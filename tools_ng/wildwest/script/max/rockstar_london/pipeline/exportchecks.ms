-- Rockstar Export Checks
-- Rockstar London
-- 25/08/2009
-- by Mark Harrison-Ball

----------------------------------------------------------------------------------------
--			TODO:
----------------------------------------------------------------------------------------
--	Collect all IPL data and test fro mising objects
--	
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

clearlistener()

filein ( RsConfigGetWildWestDir() + "script/max/rockstar_london/utils/rsl_imageutils.ms" )
filein "gta/utils/GtaUtilityFunc.ms"
filein "pipeline/export/maps/globals.ms"

global ErrorNames = #()
global ErrorObjects = #()
global WarnNames = #()
global WarnObjects = #()
global RsRetOK = true
global RsAbort = false


	

RsMapSetupGlobals()



----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
--- Search Functions Bseach Method ---
fn LookupTableComparator a b = 
	(
		if a>b then 1
		else if a<b then -1
		else 0		
	)
	
	
fn LookupTableLookup itemKey itemTable = 
	( 
		local lookupKey = itemKey --#(itemKey)
		bsearch lookupKey itemTable LookupTableComparator
	)

----------------------------------------------------------------------------------------	
----------------------------------------------------------------------------------------


	
fn CollectTXDNames =
(
	local sceneTXD = #()
	
	for obj in geometry where classof obj != XRefObject do
	(		
		if (getattrclass obj) == "Gta Object" do 
		(
			local index = getattrindex "Gta Object" "TXD"
			appendifunique sceneTXD (tolower (getattr obj index))
		)
	)
	sceneTXD	
)


fn CollectOBJNames =	
(
	local ObjNames = #()
	for obj in geometry where classof obj != XRefObject do
	(	
		appendifunique ObjNames (tolower obj.name)
	)
	ObjNames
)
	

-- Used to check the length of the maxfile name --
-- '_files.txt' gets appended to the pack data and we are limited to only 24 character including extension
-- remove extension (.ext) and you left with 20
-- remove '_files' and you left with 14
-- so need to keep name lenth to 14 characters
fn checkSceneName =
(
	local isValid = false;
	local namelength = getFilenameFile maxfilename;
	if namelength.count <= 14 then
	(
		isValid = true
	)
		
	isValid;	
)


-- Recrusive function 1 level deep
fn getFilesRecursive root pattern =
(
my_files = #()
join my_files (getFiles (root + "/"+pattern))	

dir_array = GetDirectories (root+"/*")
for d in dir_array do
join dir_array (GetDirectories (d+"/*"))

for f in dir_array do
join my_files (getFiles (f + pattern))
my_files 
)


-- Read the IDE files and sort the object name and txd
fn readIDEfiles idelist tokenIndex=
(
	local objNames = #()
	
	for filename in idelist do
	(
		-- to not collec tthe current maps objects
		if (tolower (getFilenameFile filename) !=tolower  rsmapname) then
		(
			ideFile = openfile filename mode:"rt"
			
			while(eof ideFile == false) do
			(		
				if ideLine == "end" then
					(
					exit
					)
				
				ideLine = readLine ideFile
				ideTokens = filterstring ideLine ", "
				if ideTokens.count > 7 then
				(
					append objNames (tolower ideTokens[tokenIndex])
				)	
				
			)
		)
		
	)
	objNames
)


-- Collect all valid ide ObjectName files from the level 
fn getIDEObjectfiles levelpathname=
(
	validObjNames = #()
		-- Collect all the ide files
	local idelist = getFilesRecursive levelpathname "*.ide"
	validObjNames = readIDEfiles idelist	1
)

-- Collect all valid ide TXD files from the level 
fn getIDETxdfiles levelpathname=
(
	validObjNames = #()
		-- Collect all the ide files
	local idelist = getFilesRecursive levelpathname "*.ide"
	validObjNames = readIDEfiles idelist	2
)

-- CHnged to read the IDE files instead
fn CheckDupObjNames =
(
	print "Checking Level Object Name's........."
	-- Get all the level txd's
	-- Lets get all the level objnames from the img files
	--getIDEfiles RsMapLevelDir
	levelImgObjs = #()

	levelImgObjs = getIDEObjectfiles RsMapLevelDir
	
	qsort levelImgObjs LookupTableComparator 

	-- Lets get all the level objnames from my scene
	sceneObjs = CollectOBJNames()
	
	qsort sceneObjs LookupTableComparator 
	

	local n = 0
	progressStart "Checking for Duplicate Objects..."
	for eachobj in sceneObjs do
	(
		n+=1		
		progressUpdate ( 100.0*n/sceneObjs.count)

			returnvalue = (LookupTableLookup eachobj levelImgObjs)

			if returnvalue != undefined then
			(
				append ErrorNames ("NAME ERROR: Object name "+eachobj as string+" is already referenced in another Max file belonging to this level!" )
				append ErrorObjects eachobj
				print "---------------------------"
				print ("ERROR: "+eachobj)
			)	
	)
	progressEnd()
	
)


fn CheckDupTxds =
(
	print "Checking Level TXD's........."
	
	levelImgtxds = #()
	-- Get all the level txd's
	levelImgtxds = getIDETxdfiles RsMapLevelDir
	--print levelImgtxds
	qsort levelImgtxds LookupTableComparator 
	-- get the current txd's
	scenetxds = CollectTXDNames()	
	qsort scenetxds LookupTableComparator 
	
	progressStart "Checking for Duplicate Txd's..."
	local n=0
	
	for eachtxd in scenetxds do
	(
		n+=1
		progressUpdate ( 100.0*n/scenetxds.count)
		returnvalue = (LookupTableLookup eachtxd levelImgtxds)
		if returnvalue != undefined then
		(
			--print returnvalue
			
			--print ("Warning TXD FOUND with same name "+returnvalue[1] as string+" in image " +returnvalue[2] as string)
			append ErrorNames ("TXD ERROR: txd "+eachtxd as string+" is already referenced in another Max file belonging to this level!" )
			append ErrorObjects eachtxd
		)		
	)
	progressEnd()
)

fn isExported object =
(
	local dontExport = getAttrIndex "Gta Object" "Dont Export"
	local dontAdd = getAttrIndex "Gta Object" "Dont Add To IPL"
	
	local result = not ((getAttr object dontExport) and (getAttr object dontAdd))
)

mapped fn CheckUnresolvedXrefs object =
(
	if object.unresolved then
	(
		append ErrorNames ("XREF ERROR: Object name " + object.name +" is unresolved (the object cannot be found in the max file).")
		append ErrorObjects object.name
	)
)

mapped fn CheckSmashable object =
(
	local attributeIndex = getattrindex "Gta Object" "Attribute"
	local attributeValue = getAttr object attributeIndex
	
	if attributeValue == 2 then
	(
		if object.children.count > 0 then
		(
			local glassType = true
			local hasCollision = false
			
			for i = 1 to object.children.count do
			(
				local child = object.children[i]
				
				if (getAttrClass child) == "Gta Collision" then
				(
					hasCollision = true
					
					local collisionType = getAttr child (getattrindex "Gta Collision" "Coll Type")
					local match = matchPattern collisionType pattern:"*glass*"
					
					if not match then
					(
						glassType = false
					)
				)
			)
			
			if not hasCollision then
			(
				append ErrorNames ("SMASHABLE ERROR: Object name " + object.name +" is set to Is Smashable but has no collision object/s.")
				append ErrorObjects object.name
			)
			else
			(
				if not glassType then
				(
					append ErrorNames ("SMASHABLE ERROR: Object name " + object.name +" is set to Is Smashable but it's collision object/s are not set to a smashable collision type.")
					append ErrorObjects object.name
				)
			)
		)
		else
		(
			append ErrorNames ("SMASHABLE ERROR: Object name " + object.name +" is set to Is Smashable but has no collision object/s.")
			append ErrorObjects object.name
		)
	)
)


-- Proper fucntion to get the actual triangle count from a object (poly or mesh)
fn getTriangleCount obj =
(
	local tCount = 0

	if classOf obj.baseObject == editable_poly then
	(
		for i in 1 to obj.numFaces do
		(
			local facelist = (polyOp.getFaceVerts obj i)
			if facelist != undefined then	-- Additional check for faces that are deleted
			(
				local num_verts = facelist.count				
				if num_verts > 3 then tCount += num_verts-2
					else tCount += 1
			)
		)
		return tCount
	)
	else return obj.mesh.numFaces
)

-- Will check the object mesh density 
-- Hardcoded  values
fn checkFragmentmesh obj &tricount &faceArea =
(
	-- Check if object is fragment and exportable
	-- collect all children 
	-- Collect bound info
	if getAttrClass obj == "Gta Object" then
	(
		local testMesh = snapshotAsMesh obj
		
		tricount += testMesh.faces.count --(getTriangleCount obj)
		faceArea += (meshop.getFaceArea testMesh (for f in testMesh.faces collect f.index))
		
		delete testMesh
	)
	
	for i in obj.children do
	(
		checkFragmentmesh i &tricount &faceArea
	)


)


fn checkscenefragments =
(
	print "Checking Fragment Triangle Counts........."
	
	idxFrag = getattrindex "Gta Object" "Is Fragment"
	idxDynamic = getattrindex "Gta Object" "Is Dynamic"
	dontExport = getAttrIndex "Gta Object" "Dont Export"
	animExport = getAttrIndex "Gta Object" "Has Anim" 
	clothExport = getAttrIndex "Gta Object" "Is Cloth"
	

	for i in rootnode.children where getAttrClass i == "Gta Object" do
	(
		valFrag = getattr i idxFrag 		
		valDynamic = getattr i idxDynamic
		valdontExport = getattr i dontExport
		valanimExport =  getattr i animExport
		valclothExport =  getattr i clothExport

		if valFrag == true and  valDynamic == true then --and dontExport == false then
		(
			-- Exclude objects that are not set to export
			-- Exclude objects that are not aniamtions (RayFire set pieces for example)
			-- Exclude Cloth Objects 
			if valdontExport == false and valanimExport == false and valclothExport == false then
			(
				tricount = 0
				local faceArea = 0
				checkFragmentmesh i &tricount &faceArea
				
				local highDensity = (faceArea / tricount) < 0.002
					-- Issue a Warning
				if tricount > 1000 and highDensity then
				(
					append WarnNames ("WARNING: Fragment object " + i.name +" has a total "+tricount as string+" triangles over the 1000 recomended size.")
					append WarnObjects i
				)
				-- Issue a Halt
				if tricount > 6000 and highDensity then
				(
					append ErrorNames ("ERROR: Are you out of your mind: Fragment object " + i.name +" has total "+tricount as string+" triangles!!!!!!!!!!.")
					append ErrorObjects i
				)
				-- Issue a Halt
				else if tricount > 3000 and highDensity then
				(
					append ErrorNames ("ERROR: Fragment object " + i.name +" has a total "+tricount as string+" triangles over the 3000 limit!.")
					append ErrorObjects i
				)			
				
				
				
				
			)
			--recurse object children (meshes)
			
		)
	)
)




fn recurseCheckDynamic object &hasCollision &hasOpenEdges =
(
	for i = 1 to object.children.count do
	(
		local child = object.children[i]
		
		if (getAttrClass child) == "Gta Collision" then
		(
			hasCollision = true
			
			if (classOf child) == Col_Mesh then
			(
				local collisionCopy = copy child
				
				col2Mesh collisionCopy
				local openEdges = ((meshop.getOpenEdges collisionCopy) as array).count > 0
				
				delete collisionCopy
				
				if openEdges then
				(
					hasOpenEdges = true
				)
			)
		)
		else
		(
			recurseCheckDynamic child &hasCollision &hasOpenEdges
		)
	)
)

mapped fn checkDynamicCollision object =
(
	local dynamicIndex = getattrindex "Gta Object" "Is Dynamic"
	local LODProp = (matchPattern object.name pattern:"*_L") or (matchPattern object.name pattern:"*_L_frag_")
	
	if (getAttr object dynamicIndex) and object.parent == undefined and (isExported object) and not LODProp then
	(
		local attributeIndex = getattrindex "Gta Object" "Attribute"
		local attributeValue = getAttr object attributeIndex
		
		if object.children.count > 0 then
		(
			local hasOpenEdges = false
			local hasCollision = false
			
			recurseCheckDynamic object &hasCollision &hasOpenEdges
			
			if not hasCollision and attributeValue != 4 then
			(
				append ErrorNames ("DYNAMIC ERROR: Object name " + object.name +" is set to Is Dynamic but has no collision object/s.")
				append ErrorObjects object.name
			)
			else
			(
				if hasOpenEdges then
				(
					append ErrorNames ("DYNAMIC ERROR: Object name " + object.name +" is set to Is Dynamic but it's collision object/s have open edges (hole/s).")
					append ErrorObjects object.name
				)
			)
		)
		else
		(
			if attributeValue != 4 then
			(
				append ErrorNames ("DYNAMIC ERROR: Object name " + object.name +" is set to Is Dynamic but has no collision object/s.")
				append ErrorObjects object.name
			)
		)
	)
)

mapped fn CheckTextureSize map =
(
	local powerOfTwo = true
	
	if (bit.and map.bitmap.width (map.bitmap.width - 1)) > 0 then
	(
		powerOfTwo = false
	)
	if (bit.and map.bitmap.height (map.bitmap.height - 1)) > 0 then
	(
		powerOfTwo = false
	)
	
	if not powerOfTwo then
	(
		appendIfUnique ErrorNames ("TEXTURE ERROR: Texture name " + (filenameFromPath map.bitmap.filename) +" is not a power of two " + map.bitmap.width as string + " x " + map.bitmap.height as string)
	)
)


----------------------------------------------------------------------------------------	
----------------------------------------------------------------------------------------


rollout RsMapCritAssertsCheck "Halting Export until below errors have been resolved!" width:720
(
	
	listbox lstErrors "Errors:" items:ErrorNames width:694
	button btn_help "Help" width:345 across:2
	button btnClose "Close" width:345
	
	fn getErrorObject obj =
	(
		local sel = (getnodebyname obj ignoreCase:true )
		select sel
		max zoomext sel
	)
	
	fn getErrorTxds txdName =
	(
		local errorsel = #()
		local index = getattrindex "Gta Object" "TXD"
		local validobjs = geometry as array
		
		for obj in validobjs do
		(
			if (getattrclass obj) == "Gta Object" then 
			(
				local txdNameobj = tolower (getattr obj index)
				if txdName == txdNameobj then 
				(
					append errorsel obj
				)
			)
		)
		select errorsel
		max zoomext sel
	)
	
	on lstErrors selected item do
	(
		if (matchPattern (lstErrors.selected)  pattern:"TXD ERROR*") then
		(
			getErrorTxds ErrorObjects[item]
		)
		if (matchPattern lstErrors.selected  pattern:"ERROR*") then
		(
			select ErrorObjects[item]
			max zoomext sel
		)
		
		if (matchPattern lstErrors.selected  pattern:"NAME ERROR*") or \
			(matchPattern lstErrors.selected  pattern:"XREF ERROR*") or \
			(matchPattern lstErrors.selected  pattern:"SMASHABLE ERROR*")	or \		
			(matchPattern lstErrors.selected  pattern:"DYNAMIC ERROR*") then
		(
			getErrorObject ErrorObjects[item]
		)
		
		
		
		
	)
	
	on btn_help pressed do
	(
		shellLaunch "https://mp.rockstargames.com/index.php/Export_Check_Errors" ""
	)
	
	on btnClose pressed do
	(
		DestroyDialog RsMapCritAssertsCheck
	)
)



Rollout RsExportChecks "Export Checks"
(
	--////////////////////////////////////////////////////////////
	-- interface. Current possibile list of checks subject to change
	--////////////////////////////////////////////////////////////
	button btnSelAll "Select All" across:2
	button btnChecksSelNone "Select None" 
	checkbox chkDupNames "Duplicate Names" checked:true 
	checkbox chkDupTxds "Duplicates TXDs" checked:true 
	checkBox chkUnresolved "Unresolved XRefs" checked:false
	checkBox chkSmashable "Smashable Collision" checked:false
	checkBox chkDynamicCollision "Dynamic Collision" checked:false
	checkBox chkTextureSize "Texture Size" checked:false
	checkBox chkFragmentMeshSize "Fragment Triangle count Size" checked:true
-- 	checkbox chkMissingBounds "Missing Bounds" checked:true 
	checkbox chkMiloObjects "Missing Milo Objects" checked:true 
-- 	checkbox chkNegativeVols "Negative Volumes" checked:true 	
-- 	checkbox chkSmashableGlass "Smashable Glass" checked:true 
	checkbox chkMloInstances "Mlo Instances" checked:true 
-- 	checkbox chkFragmentMass "Fragment Mass" checked:true 
	--checkbox chkSpecMaps "Check Spec Maps" checked:true -- not to be checked here.
	
	on RsExportChecks open do
	(
-- 		chkNegativeVols.enabled = false
		chkMloInstances.enabled = false
-- 		chkMissingBounds.enabled = false
		chkMiloObjects.enabled = false
-- 		chkSmashableGlass.enabled = false
-- 		chkFragmentMass.enabled = false
	)
	
	on btnSelAll pressed do
	(
		chkDupNames.checked = true
		chkDupTxds.checked = true
		chkUnresolved.checked = true
		chkSmashable.checked = true
		chkDynamicCollision.checked = true
		chkTextureSize.checked = true
		chkFragmentMeshSize.checked = true
		
-- 		chkMissingBounds.checked = true
		chkMiloObjects.checked = true
-- 		chkNegativeVols.checked = true
-- 		chkSmashableGlass.checked = true
		chkMloInstances.checked = true
-- 		chkFragmentMass.checked = true
	)

	on btnChecksSelNone pressed do
	(
		chkDupNames.checked = false
		chkDupTxds.checked = false
		chkUnresolved.checked = false
		chkSmashable.checked = false
		chkDynamicCollision.checked = false
		chkTextureSize.checked = false
		chkFragmentMeshSize.checked = false
		
-- 		chkMissingBounds.checked = false
		chkMiloObjects.checked = false
-- 		chkNegativeVols.checked = false
-- 		chkSmashableGlass.checked = false
		chkMloInstances.checked = false
-- 		chkFragmentMass.checked = false
	)

)

----------------------------------------------------------------------------------------	
----------------------------------------------------------------------------------------

fn preExportAdvanced =
(
	RsMapSetupGlobals() -- We need to call this so that globals get updated everytime we export incase we load up a different map.
	ErrorNames = #()
	ErrorObjects = #()
	WarnNames = #()
    WarnObjects = #()
	global RsRetOK = true
	global countValTimer = 10
	Print "Running Advanced Checks....."
	
	
	if checkSceneName() == false then
	(
		messagebox "Filename exceeds 14 Characters!! (Not including extension)\n\nEngine only supports 24 characters but needs to add 10 extra characters during export/build process\n\n PLEASE RENAME MAXFILE WITH SHORTER NAME"
		return false
	)
	
	local objectArray = for object in geometry where (getAttrClass object) == "Gta Object" and (classOf object) != XRefObject collect object
	local xrefArray = for object in objects where (classOf object) == XRefObject collect object
	local textureArray = getClassInstances bitmapTexture
	

	if RsExportChecks.chkDupNames.checked then CheckDupObjNames()
	if RsExportChecks.chkDupTxds.checked then CheckDupTxds()
	if RsExportChecks.chkUnresolved.checked then CheckUnresolvedXrefs xrefArray
	if RsExportChecks.chkSmashable.checked then CheckSmashable objectArray
	if RsExportChecks.chkDynamicCollision.checked then checkDynamicCollision objectArray
	if RsExportChecks.chkTextureSize.checked then CheckTextureSize textureArray
	if RsExportChecks.chkFragmentMeshSize.checked then checkscenefragments()
	


	
	if ErrorObjects.count > 0 then
	(
		CreateDialog RsMapCritAssertsCheck style:#(#style_toolwindow,#style_sysmenu)
		RsRetOK = false
	)
	else if WarnObjects.count > 0 then
	(
		RsRetOK = true
		RsAbort = false
		
		
		rollout RsMapWarnAssertsCheck "Warning: Please review these warnings!" width:720
		(

			listbox lstWarnings "Warnings:" items:WarnNames height:23
			button btnOK "Continue (10)" width:100 pos:[150,340]
			button btnCancel "Abort Export" width:100 pos:[270,340]
			button btnClose "Close" width:100 pos:[390,340]
			timer tmrCounter
			
			


			on lstWarnings selected item do (

				if (WarnObjects[item] != undefined) then (
				
					if isdeleted WarnObjects[item]== false then (

						select WarnObjects[item]
						max zoomext sel
					)
				)
			)

			-- Close Dialogie and continue export
			on btnOK pressed do (
				RsRetOK = true
				DestroyDialog RsMapWarnAssertsCheck
			)	
			
			-- Keep the dialogue open
			on btnCancel pressed do (			
				RsRetOK = false	
				RsAbort = true
				DestroyDialog RsMapWarnAssertsCheck
				
				CreateDialog RsMapWarnAssertsCheck style:#(#style_toolwindow,#style_sysmenu) modal:false
				btnOK.visible = false
				btnCancel.visible = false
				return RsRetOK
			)
			
			-- Close the dialogue
			on btnClose pressed do (			
				RsRetOK = false
				DestroyDialog RsMapWarnAssertsCheck
			)
			


			on tmrCounter tick do (
				if RsAbort == false then	(
					if countValTimer == 0 then (
						RsReturnedOK = true
						DestroyDialog RsMapWarnAssertsCheck 
					) else (

						countValTimer = countValTimer - 1
						btnOK.text = "Continue (" + (countValTimer as string) + ")"
					)
				)

			)

			on RsMapWarnAssertsCheck open do (
				countValTimer = 10
				btnOK.text = "Continue (" + (countValTimer as string) + ")"
				
			)
					
			
		)

		CreateDialog RsMapWarnAssertsCheck style:#(#style_toolwindow,#style_sysmenu) modal:true
		return RsRetOK
		
	)

	return RsRetOK
	
)


