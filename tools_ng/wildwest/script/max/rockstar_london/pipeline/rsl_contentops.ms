
fileIn "pipeline/util/xml.ms"
filein (RsConfigGetWildWestDir() + "script/max/Rockstar_London/pipeline/RSL_LevelConfig.ms")

struct RSL_ContentOps
(
	workPath = systemTools.getEnvVariable "RSG_ROOTDIR",
	
	mapsFile = (project.bin + "content\\maps.xml"),
	outputFile = (project.bin + "content\\output.xml"),
	
	mapsXml,
	outputXml,
	
	mapsLevelElement,
	outputLevelElement,
	outputLevelsElement,
	
	p4MapsEdit = false,
	p4OutputEdit = false,
	
	fn recurseMapsLevelSearch element searchString =
	(
		for i = 0 to element.ChildNodes.Count - 1 do
		(
			local currentNode = element.ChildNodes.Item[i]
			
			if currentNode.name != "#comment" and currentNode.Attributes.Item[0].value == "group" then
			(
				if (toLower currentNode.Attributes.Item[1].value) == (toLower searchString) then
				(
					mapsLevelElement = currentNode
				)
				else
				(
					recurseMapsLevelSearch currentNode searchString
				)
			)
		)
	),
	
	fn recurseOutputLevelSearch element searchString =
	(
		for i = 0 to element.ChildNodes.Count - 1 do
		(
			local currentNode = element.ChildNodes.Item[i]
			
			if currentNode.name != "#comment" and currentNode.name != "#text" then
			(
				if currentNode.Attributes.Count > 0 then
				(
					if (toLower currentNode.Attributes.Item[0].value) == (toLower searchString) then
					(
						outputLevelElement = currentNode
					)
					else
					(
						recurseOutputLevelSearch currentNode searchString
					)
				)
			)
		)
	),
	
	fn loadContentXmls =
	(
		filein (scriptsPath + "payne/pipeline/RSL_LevelConfig.ms")
		
		p4.sync(pathConfig.appendPath workPath "\\payne\\bin\\content\\...")
		
		if (getFiles mapsFile).count == 1 then
		(
			setFileAttribute mapsFile #readOnly false
			
			mapsXml = XmlDocument()
			mapsXml.init()
			mapsXml.load mapsFile
		)
		else
		(
			format "% does not exist\n" mapsFile
		)
		
		if (getFiles outputFile).count == 1 then
		(
			setFileAttribute outputFile #readOnly false
			
			outputXml = XmlDocument()
			outputXml.init()
			outputXml.load outputFile
		)
		else
		(
			format "% does not exist\n" outputXml
		)
	),
	
	fn newMapEntry mapName mapType =
	(
		local newElement = mapsXml.createelement "content"
		local attr = mapsXml.createattribute "type"
		attr.value = "map"
		newElement.Attributes.Append (attr)
		
		attr = mapsXml.createattribute "name"
		attr.value = mapName
		newElement.Attributes.Append (attr)
		
		attr = mapsXml.createattribute "definitions"
		attr.value = "true"
		newElement.Attributes.Append (attr)
		
		local instanceValue = (not (mapType == "props")) as string
		
		attr = mapsXml.createattribute "instances"
		attr.value = instanceValue
		newElement.Attributes.Append (attr)
		
		newElement
	),
	
	fn addMapContentEntry element mapType mapName =
	(
		if mapType == LevelConfig.level then
		(
			element.AppendChild (newMapEntry mapName mapType)
		)
		else
		(
			local parentElement
			
			for i = 0 to element.ChildNodes.Count - 1 do
			(
				local child = element.ChildNodes.Item[i]
				
				if child.Attributes.Item[0].value == "group" and (toLower child.Attributes.Item[1].value) == mapType then
				(
					parentElement = child
				)
			)
			
			if parentElement == undefined then
			(
				parentElement = mapsXml.createelement "content"
				local attr = mapsXml.createattribute "type"
				attr.value = "group"
				parentElement.Attributes.Append (attr)
				
				attr = mapsXml.createattribute "path"
				attr.value = mapType
				parentElement.Attributes.Append (attr)
			)
			
			parentElement.AppendChild (newMapEntry mapName mapType)
			element.AppendChild (parentElement)
		)
	),
	
	fn newOutputEntry mapName =
	(
		local parentElement = outputXml.createelement "content"
		local attr = outputXml.createattribute "type"
		attr.value = "maprpf"
		parentElement.Attributes.Append (attr)
		
		attr = outputXml.createattribute "name"
		attr.value = mapName
		parentElement.Attributes.Append (attr)
		
		local newElement = outputXml.createelement "in"
		local attr = outputXml.createattribute "group"
		attr.value = "maps"
		newElement.Attributes.Append (attr)
		
		attr = outputXml.createattribute "type"
		attr.value = "map"
		newElement.Attributes.Append (attr)
		
		attr = outputXml.createattribute "name"
		attr.value = mapName
		newElement.Attributes.Append (attr)
		
		parentElement.AppendChild newElement
		
		parentElement
	),
	
	fn addOutputContentEntry element mapType mapName =
	(
		if mapType == LevelConfig.level then
		(
			element.AppendChild (newOutputEntry mapName)
		)
		else
		(
			local parentElement
			
			for i = 0 to element.ChildNodes.Count - 1 do
			(
				local child = element.ChildNodes.Item[i]
				
				if child.Attributes.Item[0].value == "group" and (toLower child.Attributes.Item[1].value) == mapType then
				(
					parentElement = child
				)
			)
			
			if parentElement == undefined then
			(
				parentElement = outputXml.createelement "content"
				local attr = outputXml.createattribute "type"
				attr.value = "group"
				parentElement.Attributes.Append (attr)
				
				attr = outputXml.createattribute "path"
				attr.value = mapType
				parentElement.Attributes.Append (attr)
			)
			
			parentElement.AppendChild (newOutputEntry mapName)
			element.AppendChild (parentElement)
		)
	),
	
	fn getoutputLevelsElement element =
	(
		for i = 0 to element.ChildNodes.Count - 1 do
		(
			local currentNode = element.ChildNodes.Item[i]
			
			if currentNode.name != "#comment" and currentNode.name != "#text" then
			(
				if currentNode.Attributes.Item[1].value == "Levels" then
				(
					outputLevelsElement = currentNode
				)
				else
				(
					getoutputLevelsElement currentNode
				)
			)
		)
	),
	
	fn findLevelEntry =
	(
		p4MapsEdit = false
		p4OutputEdit = false
		
		if mapsXml != undefined and outputXml != undefined then
		(
			local mapType = toLower LevelConfig.imagetype
-- 			if mapType == "interiors" then
-- 			(
-- 				mapType = "Interiors"
-- 			)
			
			if mapType == "" then
			(
				mapType = LevelConfig.level
			)
			
			local mapName = toLower (getFilenameFile maxFilename)
			
			mapsLevelElement = undefined
			outputLevelElement = undefined
			outputLevelsElement = undefined
			
			recurseMapsLevelSearch mapsXml.document.DocumentElement LevelConfig.level
			
			getoutputLevelsElement outputXml.document.DocumentElement
			
			recurseOutputLevelSearch outputLevelsElement LevelConfig.level
			
			
			if mapsLevelElement == undefined then
			(
				p4MapsEdit = true
				p4.edit(pathConfig.appendPath workPath "\\payne\\bin\\content\\maps.xml")
				
				mapsLevelElement = mapsXml.createelement "content"
				local attr = mapsXml.createattribute "type"
				attr.value = "group"
				mapsLevelElement.Attributes.Append (attr)
				
				attr = mapsXml.createattribute "path"
				attr.value = LevelConfig.level
				mapsLevelElement.Attributes.Append (attr)
				
				addMapContentEntry mapsLevelElement mapType mapName
				
				local insertAfterNode
				for i = 0 to mapsXml.document.DocumentElement.ChildNodes.Count - 1 do
				(
					local currentNode = mapsXml.document.DocumentElement.ChildNodes.Item[i]
					
					if currentNode.Attributes.Item[1].value < mapsLevelElement.Attributes.Item[1].value then
					(
						insertAfterNode = currentNode
					)
					else
					(
						exit
					)
				)
				
				if insertAfterNode != undefined then
				(
					mapsXml.document.DocumentElement.InsertAfter mapsLevelElement insertAfterNode
				)
				else
				(
					mapsXml.document.DocumentElement.PrependChild mapsLevelElement
				)
				
				mapsXml.save mapsFile
				
				format "% added to maps.xml\n" LevelConfig.level
			)
			else
			(
				if mapType == LevelConfig.level then
				(
					local found = false
					
					for i = 0 to mapsLevelElement.ChildNodes.Count - 1 do
					(
						local child = mapsLevelElement.ChildNodes.Item[i]
						
						if child.Attributes.Item[0].value == "map" and (toLower child.Attributes.Item[1].value) == mapName then
						(
							found = true
						)
					)
					
					if not found then
					(
						p4MapsEdit = true
						p4.edit(pathConfig.appendPath workPath "\\payne\\bin\\content\\maps.xml")
						
						mapsLevelElement.PrependChild (newMapEntry mapName mapType)
						mapsXml.save mapsFile
						
						format "% added to % in maps.xml\n" mapName LevelConfig.level
					)
					else
					(
						format "% found in maps.xml\n" mapName
					)
				)
				else
				(
					local added = false
					
					for i = 0 to mapsLevelElement.ChildNodes.Count - 1 do
					(
						local child = mapsLevelElement.ChildNodes.Item[i]
						
						if child.Attributes.Item[0].value == "group" and (toLower child.Attributes.Item[1].value) == mapType then
						(
							local found = false
							
							for i = 0 to child.ChildNodes.Count - 1 do
							(
								local mapItem = child.ChildNodes.Item[i]
								
								if (toLower mapItem.Attributes.Item[1].value) == mapName then
								(
									found = true
									added = true
									
									format "% found in % group in maps.xml\n" mapName mapType
								)
							)
							
							if not found then
							(
								p4MapsEdit = true
								p4.edit(pathConfig.appendPath workPath "\\payne\\bin\\content\\maps.xml")
								
								child.AppendChild (newMapEntry mapName mapType)
								mapsXml.save mapsFile
								
								added = true
								
								format "% added in % group to % in maps.xml\n" mapName mapType LevelConfig.level
							)
						)
					)
					
					if not added then
					(
						p4MapsEdit = true
						p4.edit(pathConfig.appendPath workPath "\\payne\\bin\\content\\maps.xml")
						
						addMapContentEntry mapsLevelElement mapType mapName
						mapsXml.save mapsFile
						
						format "% added in % group to % in maps.xml\n" mapName mapType LevelConfig.level
					)
				)
			)
			
			if outputLevelsElement != undefined then
			(
				if outputLevelElement == undefined then
				(
					p4OutputEdit = true
					p4.edit(pathConfig.appendPath workPath "\\payne\\bin\\content\\output.xml")
					
					outputLevelElement = outputXml.createelement "content"
					local attr = outputXml.createattribute "name"
					attr.value = LevelConfig.level
					outputLevelElement.Attributes.Append (attr)
					
					attr = outputXml.createattribute "type"
					attr.value = "group"
					outputLevelElement.Attributes.Append (attr)
					
					attr = outputXml.createattribute "path"
					attr.value = LevelConfig.level
					outputLevelElement.Attributes.Append (attr)
					
					addOutputContentEntry outputLevelElement mapType mapName 
					
					local insertAfterNode
					for i = 0 to outputLevelsElement.ChildNodes.Count - 1 do
					(
						local currentNode = outputLevelsElement.ChildNodes.Item[i]
						
						if currentNode.Attributes.Item[2].value < outputLevelElement.Attributes.Item[2].value then
						(
							insertAfterNode = currentNode
						)
						else
						(
							exit
						)
					)
					
					if insertAfterNode != undefined then
					(
						outputLevelsElement.InsertAfter outputLevelElement insertAfterNode
					)
					else
					(
						outputLevelsElement.PrependChild outputLevelElement
					)
					
					format "% added to output.xml\n" LevelConfig.level
					
					outputXml.save outputFile
				)
				else
				(
					if mapType == LevelConfig.level then
					(
						local found = false
						
						for i = 0 to outputLevelElement.ChildNodes.Count - 1 do
						(
							local child = outputLevelElement.ChildNodes.Item[i]
							
							if child.Attributes.Item[0].value == "maprpf" and (toLower child.Attributes.Item[1].value) == mapName then
							(
								found = true
							)
						)
						
						if not found then
						(
							p4OutputEdit = true
							p4.edit(pathConfig.appendPath workPath "\\payne\\bin\\content\\output.xml")
							
							outputLevelElement.PrependChild (newOutputEntry mapName)
							outputXml.save outputFile
							
							format "% added to % in output.xml\n" mapName LevelConfig.level
						)
						else
						(
							format "% found in output.xml\n" mapName
						)
					)
					else
					(
						local added = false
						
						for i = 0 to outputLevelElement.ChildNodes.Count - 1 do
						(
							local child = outputLevelElement.ChildNodes.Item[i]
							
							if child.Attributes.Item[0].value == "group" and (toLower child.Attributes.Item[1].value) == mapType then
							(
								local found = false
								
								for i = 0 to child.ChildNodes.Count - 1 do
								(
									local outputItem = child.ChildNodes.Item[i]
									
									if (toLower outputItem.Attributes.Item[1].value) == mapName then
									(
										found = true
										added = true
										
										format "% found in % group in output.xml\n" mapName mapType
									)
								)
								
								if not found then
								(
									p4OutputEdit = true
									p4.edit(pathConfig.appendPath workPath "\\payne\\bin\\content\\output.xml")
									
									child.AppendChild (newOutputEntry mapName)
									outputXml.save outputFile
									
									added = true
									
									format "% added in % group to % in output.xml\n" mapName mapType LevelConfig.level
								)
							)
						)
						
						if not added then
						(
							p4OutputEdit = true
							p4.edit(pathConfig.appendPath workPath "\\payne\\bin\\content\\output.xml")
							
							addOutputContentEntry outputLevelElement mapType mapName 
							outputXml.save outputFile
							
							format "% added in % group to % in output.xml\n" mapName mapType LevelConfig.level
						)
					)
				)
			)
			else
			(
				messageBox "Failed to find output_inner element in output.xml. Nothing will be added to output.xml" title:"Error..."
			)
		)
		else
		(
			messageBox ("Failed to load content xmls\n" + mapsFile + "\n" + outputFile) title:"Error..."
		)
		if not p4MapsEdit then
		(
			setFileAttribute mapsFile #readOnly true
		)
		
		if not p4OutputEdit then
		(
			setFileAttribute outputFile #readOnly true
		)
	),
	
	fn compareMapName v1 v2 =
	(
		case of
		(
			(v1.Attributes.Item[1].value > v2.Attributes.Item[1].value):1
			(v1.Attributes.Item[1].value < v2.Attributes.Item[1].value):-1
			default:0
		)
	),
	
	fn sortMapsXml =
	(
		if mapsXml != undefined then
		(
			local childArray = #()
			
			for i = mapsXml.document.DocumentElement.childNodes.Count - 1 to 0 by -1 do
			(
				append childArray mapsXml.document.DocumentElement.childNodes.Item[i]
				
				mapsXml.document.DocumentElement.RemoveChild mapsXml.document.DocumentElement.childNodes.Item[i]
			)
			
			qSort childArray compareMapName
			
			for entry in childArray do
			(
-- 				print entry.Attributes.Item[1].Value
				
				mapsXml.document.DocumentElement.AppendChild entry
			)
			
			mapsXml.save mapsFile
		)
	),
	
	fn compareOutputName v1 v2 =
	(
		case of
		(
			(v1.Attributes.Item[2].value > v2.Attributes.Item[2].value):1
			(v1.Attributes.Item[2].value < v2.Attributes.Item[2].value):-1
			default:0
		)
	),
	
	fn sortOutputXml =
	(
		if outputXml != undefined then
		(
			getoutputLevelsElement outputXml.document.DocumentElement
			
			if outputLevelsElement != undefined then
			(
				local childArray = #()
				
				for i = outputLevelsElement.childNodes.Count - 1 to 0 by -1 do
				(
					append childArray outputLevelsElement.childNodes.Item[i]
					
					outputLevelsElement.RemoveChild outputLevelsElement.childNodes.Item[i]
				)
				
				qSort childArray compareOutputName
				
				for entry in childArray do
				(
-- 					print entry.Attributes.Item[2].Value
					
					outputLevelsElement.AppendChild entry
				)
				
				outputXml.save outputFile
			)
		)
	)
)

global RSL_Content = RSL_ContentOps()
