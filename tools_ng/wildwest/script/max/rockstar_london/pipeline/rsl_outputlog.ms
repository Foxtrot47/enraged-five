clearlistener()


-- USAGE:
--
-- Create a instance of the struct
--	logoutput = init_OutputLog ()


-- Show the log window:
--	logoutput.start()

-- Write to log:
--	logoutput.log "text" type:2

/*
fn RsCloseTimeOutButton message timeout:60 = (

	returnval = true
	global countVal = timeout
	global lblVal = message

	rollout RsTheQueryBox "Query"
	(
		label lblMsg "test"
		button btnOK "Yes" width:100 pos:[50,40]
		button btnCancel "No" width:100 pos:[160,40]
		timer tmrCount

		on btnOK pressed do (

			DestroyDialog RsTheQueryBox 
		)	

		on btnCancel pressed do (

			returnval = false
			DestroyDialog RsTheQueryBox 
		)	
		
		on tmrCount tick do (
		
			if countVal == 0 then (
				
				DestroyDialog RsTheQueryBox 
			) else (
		
				countVal = countVal - 1
				btnOK.text = "Yes (" + (countVal as string) + ")"
			)
		)
		
		on RsTheQueryBox open do (
			
			lblMsg.text = lblVal
			btnOK.text = "Yes (" + (countVal as string) + ")"
		)
	)

	CreateDialog RsTheQueryBox width:300 modal:true
	
	return returnVal
)
*/


struct init_OutputLog
(

	fn dispatchOnLogoutputClosing s e =
	(
		--init_RSL_EdgeDataEditor.onEdgeDataEditorClosing s e
		print "closing"
	),
	
	

	fn Initilizetextbox=
	(
			textbox = dotNetObject "System.Windows.Forms.richTextBox" 
			
			
	),
	fn Initilizeform =
	(
			processForm = dotNetObject "MaxCustomControls.MaxForm"	
		--Colors = (dotNetClass "System.Drawing.SystemColors")
	),
	fn Initilizetextcol =
	(
		Colors = (dotNetClass "System.Drawing.Color")
	),
	
	textbox = Initilizetextbox(),
	processForm = Initilizeform(),
	colour = Initilizetextcol(),
	
		
	fn createForm  =
		(			
			-----------------------------------------------------------------------------------------------------------------
			--									Globals
			-----------------------------------------------------------------------------------------------------------------
			DockStyle= (dotNetClass "System.Windows.Forms.DockStyle")
			SystemColors = (dotNetClass "System.Drawing.SystemColors")
			ScrollBars = (dotNetClass "System.Windows.Forms.RichTextBoxScrollBars")
			--colour = (dotNetClass "system.Drawing.Color")
			
			TableLayoutPanel = (dotNetObject "System.Windows.Forms.TableLayoutPanel")
			button = (dotNetObject "System.Windows.Forms.Button")
			button2 = (dotNetObject "System.Windows.Forms.Button")
			Drawing = (dotNetClass "System.Drawing.point")
			
			
			-----------------------------------------------------------------------------------------------------------------
			--								textbox
			-----------------------------------------------------------------------------------------------------------------

			textbox.MultiLine = true
			textbox.Backcolor = SystemColors.Control	
			textBox.ScrollBars = ScrollBars.Vertical
			textBox.ReadOnly = true
			textbox.Backcolor = SystemColors.Control
			textbox.ForeColor= colour.Black
			textbox.Dock = DockStyle.fill			
			-----------------------------------------------------------------------------------------------------------------
			--								tableLayoutPanel
			-----------------------------------------------------------------------------------------------------------------
		
			tableLayoutPanel.ColumnCount = 1
			tableLayoutPanel.RowCount = 2
			tableLayoutPanel.Dock = DockStyle.fill
			TableLayoutPanel.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" (dotNetClass "System.Windows.Forms.SizeType").Percent 100)
			TableLayoutPanel.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" (dotNetClass "System.Windows.Forms.SizeType").Absolute 30)
			
			
			-----------------------------------------------------------------------------------------------------------------
			--								Button
			-----------------------------------------------------------------------------------------------------------------
			--button.Name = "Clear Log"
			button.Text = "Clear Log"
		
			button2.Text = "close"
			button2.enabled = false
			
			--button.location = (dotNetClass "System.Drawing.point" 3 197)
			--button.Location = Drawing 3 197
			/*
			button1.Location = new System.Drawing.Point(3, 197);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 24);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
			*/
			

			--dotnet.addEventHandler processForm "FormClosing" dispatchOnLogoutputClosing
			-----------------------------------------------------------------------------------------------------------------
			--								Form
			-----------------------------------------------------------------------------------------------------------------
			
			tableLayoutPanel.Controls.Add textbox
			tableLayoutPanel.Controls.Add button
			tableLayoutPanel.Controls.Add button2
			
			
			--processForm = Initilizefrom()
			
			FormBorderStyle = (dotNetClass "System.Windows.Forms.FormBorderStyle")
			processForm.Size = dotNetObject "System.Drawing.Size" 600 400
			processForm.Name = "Output Log"
			processForm.Text = "Output Log"
			processForm.ShowInTaskbar = false
			processForm.FormBorderStyle = FormBorderStyle.SizableToolWindow
			
			
			processForm.Controls.Add tableLayoutPanel
			--processForm.ShowModeless()
			true
			--if close then processForm.Close()
		),		
	
		fn log text type:1 =
		(		
			case type of
			(
				1:	(textbox.SelectionColor = colour.black
					Header = "")
				2:	(textbox.SelectionColor = colour.red
					Header = "Error\t")
				3:	(textbox.SelectionColor = colour.Olive
					Header = "Warning\t")
			)			
			textbox.SelectedText = Header +text +"\r\n"
			
			
		),
		
		fn clear =
		(
			textbox.text = ""
		),
		
		fn start =
		(
			local x = createForm()	
			processForm.ShowModeless()			
			true

		),
		
		
		fn close timeout:60 =
		(
			
			global countVal = (timeout*60)
			
			while countVal > 0 do
			(
				print countVal
				textbox.text = countVal as string
				--button2.Text= countVal;
				countVal -=1
				
			)
			processForm.close()


			/*
			on tmrCount tick do
				(		
					if countVal == 0 then
					(						
						processForm.close()
					)
					else 
					(
				
						countVal = countVal - 1
						button.text = "Close (" + (countVal as string) + ")"
					)
				)
		
			--processForm.close()
			--return true
			*/
		)
		

	
)



