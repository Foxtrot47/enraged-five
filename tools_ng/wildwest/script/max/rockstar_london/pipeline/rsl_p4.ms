-----------------------------------------------------------
-----------			P4 Class/struct		---------------

-- Author: mark.harrison-ball@rockstargames.com
-- Edited:	6 July 2009

-- Notes:
-- Need to resolve pefore client to use the allroot paths so we can use a common clinet root across all studios.


outputlist = #()
fn getoutput idtype =
(
	
)

-- DotNet Event Handler Function --
 fn outputDataReceivedHandler obj evt =
 (
 	if evt.data != undefined then
 	(				
		append outputlist (evt.data)		
		
 	)
 )
 
 -- DotNet Process Run command Function --
fn RunRageBuilderScript arguments =
(
	outputlist = #()
	proc = dotNetObject "System.Diagnostics.Process"
	proc.EnableRaisingEvents = true
	
	proc.StartInfo.WorkingDirectory = ("c:/")
	proc.StartInfo.FileName = "p4"--project.targets[1].ragebuilder
	proc.StartInfo.UseShellExecute = false
	proc.StartInfo.RedirectStandardOutput = true
	proc.StartInfo.RedirectStandardError = false
	proc.StartInfo.CreateNoWindow = true
	  
	dotNet.AddEventHandler proc "OutputDataReceived" outputDataReceivedHandler
	
	proc.StartInfo.Arguments = (" -s "+arguments)
	proc.Start()
	proc.BeginOutputReadLine()
	
	proc.WaitForExit()

)

struct p4fn
(
	usep4 = true,	-- change this to true to use p4
	fn p4clientpath =
	(
		
	--RunRageBuilderScript "info"

	
	
	--------------------------------------------------------------------------------------
	-- Using this method for now to get p4 root. will do this properliy via dll
	--------------------------------------------------------------------------------------
	HiddenDOSCommand "p4 info > C:\p4info.ini" startpath:"c:\\"
	try(
		
	fstream = openFile "C:\p4info.ini" mode:"r"
	skipToString fstream "Client root: "
	clientpath =  (readLine fstream)
	close fstream
	)catch(
		format "!Perforce Client Deafult setting are not set up!\n"
		format "Using Payne_Dir Envrioment Variable instead\n"
		-- THis is project specfic, this need to ideally contact main user to get this set up correclty. 
		clientpath = pathConfig.stripPathToTopParent (systemTools.getEnvVariable "PAYNE_DIR")
		--clinetpath = false
		)
	clientpath
	),
	clientpath = p4clientpath(),
	--------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------
	fn GetAllFiles file = 
	(
	FileArray = getFiles file
	return FileArray
	),	
	fn runcmd cmd file =
	(
		if usep4 then
		(
			result = HiddenDOSCommand(cmd) startpath:"c:\\" 
			print cmd
		)
		else
		(
			FileArray = GetAllFiles file
			if FileArray.count !=0 then
			(
				For Item in FileArray do
				(
					if doesFileExist file do
					(
					setFileAttribute file #readOnly true
					result = HiddenDOSCommand("echo P4 inot enabled in p4.ms script") startpath:"c:\\" 
					) 
				)
			)
		)
		return result
	),
	fn edit file= 
	(
		runcmd ("p4 -s sync "+ file) file
		runcmd ("p4 -s edit "+file) file
	),	
	fn add file =
	(
		runcmd ("p4 -s add "+file) file
	),	
	fn sync file =
	(
		runcmd ("p4 -s sync "+ file) file
	),	
	fn revert file =
	(
		runcmd ("p4 -s revert -a "+file) file
	),
	fn revision file =
	(
		
	),
	fn path =
	(
		clientpath
	)
	
	
	
)

--if p4  == undefined then ( p4 = p4fn())
p4 = p4fn()

