
filein "pipeline/export/maps/globals.ms"

struct RSL_textureShiftOps
(
	levelXml = project.independent + "/intermediate/levels/" + RsMapLevel + "/" + (getFilenameFile maxFilename) + "_TextureTag.xml",
	XmlStruct,
	foundElement,
	
	scaleArray = #(1.0,0.5,0.25,0.125),
	filterArray = #("A","B","C","D"),
	
	fn loadXml =
	(
		if (getFiles levelXml).count == 1 then
		(
			XmlStruct = XmlDocument()
			XmlStruct.init()
			XmlStruct.load levelXml
			
			format "% has been loaded\n" (getFilenameFile levelXml)
		)
		else
		(
			format "% does not exist\n" levelXml
		)
	),
	
	fn loadSpecificXml xmlFile =
	(
		if (getFiles xmlFile).count == 1 then
		(
			XmlStruct = XmlDocument()
			XmlStruct.init()
			XmlStruct.load xmlFile
			
			format "% has been loaded\n" (getFilenameFile xmlFile)
		)
		else
		(
			format "% does not exist\n" xmlFile
		)
	),
	
	fn recurseElementNameSearch element searchString =
	(
		for i = 0 to element.ChildNodes.Count - 1 do
		(
			local currentNode = element.ChildNodes.Item[i]
			
			if currentNode.name == searchString then
			(
				foundElement = currentNode
			)
			else
			(
				recurseElementNameSearch currentNode searchString
			)
		)
	),
	
	fn getTextureShift texture =
	(
		local result = 0
		
		if XmlStruct != undefined then
		(
			foundElement = undefined
			recurseElementNameSearch XmlStruct.document.DocumentElement (toLower texture)
			
			if foundElement != undefined then
			(
				-- HACK TO ENSURE THAT TEXTURES GET UPDATED
				try
				(
					deleteFile (RsConfigGetTexDir() + RsRemoveSpacesFromString((toLower texture)) + ".dds")
				)
				catch()
				-- END HACK
				
				local textureScale = readValue (foundElement.Attributes.Item[0].value as stringStream)
				
				result = (findItem scaleArray textureScale) - 1
			)
			else
			(
				format "Failed to find texture:% in Xml\n" texture
			)
		)
		else
		(
			format "XmlStruct is undefined\n"
		)
		
		result
	),
	
	fn getTextureFilter texture =
	(
		local result = 0
		
		if XmlStruct != undefined then
		(
			foundElement = undefined
			recurseElementNameSearch XmlStruct.document.DocumentElement (toLower texture)
			
			if foundElement != undefined then
			(
				local textureFilter = foundElement.Attributes.Item[1].value
				
				result = (findItem filterArray textureFilter) - 1
			)
			else
			(
				format "Failed to find texture:% in textureTag Xml\n" texture
			)
		)
		else
		(
			format "XmlStruct is undefined\n"
		)
		
		result
	)
)

-- global RSL_TextureShift = RSL_textureShiftOps()
-- RSL_TextureShift.loadXml()
-- print "RSL_TextureShift.loadXml()"
-- RSL_TextureShift.getTextureShift "artex_ceiling_01_dm"