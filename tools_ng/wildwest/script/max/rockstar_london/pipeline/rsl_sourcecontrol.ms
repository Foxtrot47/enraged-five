-- SourceControl Intergration --
filein ( RsConfigGetWildWestDir() + "script/max/Rockstar_London/pipeline/RSL_LevelConfig.ms" )
filein (RsConfigGetWildWestDir() + "script/max/rockstar_london/pipeline/rsl_p4.ms")


struct SourceControl 
(
	Levelconfig = GetLevelconfig(),
	readonlyList = #(),
	fileList =#(),
	
	fn getFilesWilcard pathname = 
	(
		files = getfiles((pathname)+".*")
	),
	
	fn fixProjectPath path =
	(
		if NOT doesFileExist(path) then
		(
			-- If the project path does not have the string "/projects/" in it,
			-- then use the first 3 tokens as the path from the GAME_DIR system environment variable.

			--HiddenDOSCommand "echo %GAME_DIR% > C:/temp_game_dir.txt" startpath:"C:\\"
			--fstream = openFile "C:/temp_game_dir.txt" mode:"r"
			--gameDirPath = readLine fstream
			--close fstream
			--tokenList = filterString gameDirPath "\\"
			--path = tokenList[1] + "\\" + tokenList[2] + "\\" + tokenList[3] + "\\"
			
			-- Rewrote to use correct path
			
			--path = (systemTools.getEnvVariable "RSG_PROJDIR") +"\\"
			local game_dir = systemTools.getEnvVariable "GAME_DIR2"
			if game_dir == undefined then
			(
				return undefined
			)
			
			tokenList = filterString (game_dir) "\\"
			path = tokenList[1] + "\\" + tokenList[2] + "\\" + tokenList[3] + "\\"
			
			
		)
		return path
	),

	fn buildfileList =
	(
		-- Independent --
		append fileList (getFilesWilcard (project.independent + "/levels/"+levelconfig.level+"/"+levelconfig.imagetype+"/" +levelconfig.image))
		-- Platforms --
		for p in project.targets do
		(
			if p.enabled then	-- Test to see if our platform is enabled --
			(
			append fileList (getFilesWilcard (p.target +"/levels/"+levelconfig.level+"/"+levelconfig.imagetype+"/" +levelconfig.image))
			)
		)
	),

	mapped fn filestate files =
	(
		if  (getFileAttribute files #readOnly) then
		(
			append readonlyList files
		)
	),
	
	mapped fn fileAttribute files readonly:false =
	(
		format "Making Readonly...\n"
		print files
		setFileAttribute files #readOnly readonly	
	),
	
	mapped fn ProcessSC files =
	(
		format "Checking out...\n"
        projectPath = fixProjectPath(p4.path() + "/projects/")
		if projectPath != undefined then
		(
		print  (projectPath + pathConfig.removePathTopParent files)
		p4.edit(projectPath + pathConfig.removePathTopParent files)
		)
		else
		(
			messagebox "Warning, Game_DIR is not defined in your system paths\nPlease run the Build Button install to fix this or report this error\nPerforce Auto Checkout has been disabled!"
		)
	),
	
	fn checkout =
	(
		if Levelconfig.level != undefined then
		(
			readonlyList = #()
			fileList =#()
			buildfileList()
			print fileList
			filestate fileList			-- Build a list of whihc Files are Readonly
			--print readonlyList
			if readonlyList.count > 0 then
			(
				local mList = ""
				for f in readonlyList do (mList += "\n"+f as string)	
				if QueryBox ("Check out the Following Files\n"+mList) title:"Source Control" then
				(
					--fileAttribute readonlyList readonly:true		--
					ProcessSC readonlyList
				)
				else
				(
					if QueryBox ("Make the following Files Writable with no Furthur Checkouts?\n"+mList) title:"Source Control" then
					(
					fileAttribute fileList readonly:false
					)
				)
			)
		)
		else
		(
			messagebox("Unable to checkout max file as they do not live in a level folder under the project maps folder!")
		)
	),
	
	------------------------------------------------------------------------------------------
	--								Checkout Skydome Assets 							--	
	------------------------------------------------------------------------------------------
	-- Notes:
	-- Yes this is pretty rubbish code, but in a rush and have had no time to make a proper netframework.
	--
	fn checkOutSkydomes =
	(
		local fileList = #()
		local image = "skydomes.rpf"
		local independent = project.independent + "\\models\\cdimages\\" + image
		append fileList independent
		
		local sName = selection[1].name
		local intermediatefile = project.independent +"/intermediate/skydomes/"+sName +".idr"
		append fileList intermediatefile
		
		
		for target in project.targets do
		(
			local path = target.target + "\\models\\cdimages\\" + image
			
			if (getFiles path).count > 0 then
			(
				append fileList path
			)
		)
		
		local fileString = ""		
		for file in fileList do fileString += file + "\n"
				
		
		local okToContinue = QueryBox ("Check out the Following Files?\n" + fileString) title:"Source Control"
		
		if okToContinue then
		(
			ProcessSC fileList
			--p4.sync(pathConfig.appendPath workpath (pathConfig.removePathTopParent project.independent)+"/intermediate/skydomes/...")
			--p4.edit(pathConfig.appendPath workpath (pathConfig.removePathTopParent project.independent)+"/intermediate/skydomes/"+sName+".*")
			
		)
		else
		(
			if QueryBox ("Make the following Files Writable with no Furthur Checkouts?\n"+fileString) title:"Source Control" then
			(
				fileAttribute fileList readonly:false
			)
			else
			(
				print "ABORTING EXPORT....!"
				return false
			)
		)
		
	

		
	),
	
	------------------------------------------------------------------------------------------
	--								Checkout Vehicle Assets 							--	
	------------------------------------------------------------------------------------------
		
	fn checkOutVehicles =
	(
		local fileList = #()
		local image = "vehicles.rpf"
		
		local independent = project.independent + "\\models\\cdimages\\" + image
		append fileList independent
		
		for target in project.targets do
		(
			local path = target.target + "\\models\\cdimages\\" + image
			
			if (getFiles path).count > 0 then
			(
				append fileList path
			)
		)
		
		local fileString = ""
		
		for file in fileList do fileString += file + "\n"
		
		local okToContinue = QueryBox ("Check out the Following Files?\n" + fileString) title:"Source Control"
		
		if okToContinue then
		(
			ProcessSC fileList
		)
		
		local vName = selection[1].name
		local vehName = vName = substring vName 1 (vName.count - 5)
		local workpath = systemTools.getEnvVariable "RSG_ROOTDIR" 
		
		p4.sync(pathConfig.appendPath workpath (pathConfig.removePathTopParent project.independent)+"/intermediate/vehicles/...")
		p4.edit(pathConfig.appendPath workpath (pathConfig.removePathTopParent project.independent)+"/intermediate/vehicles/"+vehName+".*")
		
	),
	
	------------------------------------------------------------------------------------------
	--								Checkout Single File		 							--	
	------------------------------------------------------------------------------------------
	fn checkOutFile Filename =
	(
		local workpath = systemTools.getEnvVariable "RSG_ROOTDIR" 
		
		local workfile = (pathConfig.appendPath workpath (pathConfig.removePathTopParent Filename))
		p4.add(workfile)
		p4.sync(workfile)
		p4.edit(workfile)
	),
	
	------------------------------------------------------------------------------------------
	--								Add Missing Tetxures to Perforce					--	
	------------------------------------------------------------------------------------------
	fn validBitmapPath filepath =
	(
		local tokens = filterstring filepath "\\"
		local valid = false
		for i in tokens do
		(
			if tolower i == tolower (project.name+"_art") then valid = true
		)
		valid	
	),
	
	fn addMissingTextures =
	(
		local RootPath = (systemTools.getEnvVariable "RSG_ROOTDIR") 
		-- dir /b /s /a-r-d *.bmp,*.tif
		-- | p4 -x - add 
		
			
		HiddenDOSCommand("dir "+RootPath+"\\"+project.name+"\\"+(project.name+"_art\\textures\\*.bmp /b /s /a-r-d | p4 -x - add")) startpath:"c:\\" 
		HiddenDOSCommand("dir "+RootPath+"\\"+project.name+"\\"+(project.name+"_art\\textures\\*.tif /b /s /a-r-d | p4 -x - add")) startpath:"c:\\" 
		
		--p4.add(RootPath+"\\"+project.name+"\\"+(project.name+"_art\\textures\\*.bmp /b /s /a-r-d *.bmp "))
		/*
		progressStart "Adding Missing Textures to perforce..."
		local RootPath = (systemTools.getEnvVariable "RSG_ROOTDIR") 
		local bitmapPaths = #()
		for m in getClassInstances BitmapTexture do 
		(
			local filepath = (pathConfig.appendPath RootPath (pathConfig.removePathTopParent m.filename))
			if validBitmapPath filepath then
				(
			
				appendifunique bitmapPaths (getFilenamePath filepath)
				)
		)
		local cnt = 0
		for i in bitmapPaths do
		(
			cnt +=1
			progressUpdate ( 100.0*cnt/bitmapPaths.count)
			p4.add (i+"...")
			
		)
		progressEnd()
		*/
		
	)
)

sc = SourceControl()

