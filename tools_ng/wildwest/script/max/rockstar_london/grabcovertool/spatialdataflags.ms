

global SDT_GRAB_EDGE = 0
global SDT_GRAB_EDGE_MOVABLE = 1	-- ** NOT USED **
global SDT_HIGH_FALL_EDGE = 2
global SDT_COVER_EDGE = 3		-- ** NOT USED **
global SDT_COVER_CORNER = 4		-- ** NOT USED **
global SDT_RAILING = 5
global SDT_ACTOR_SYNC = 6		-- ** NOT USED **
global SDT_STAIR_TOP = 7
global SDT_VAULT_ON = 8
global SDT_VAULT_OVER = 9
global SDT_VAULT_SLIDE = 10
global SDT_STAIR_SURFACE = 11
global SDT_VAULT_JUMP = 12
global SDT_VAULT_PICKUP = 13
global SDT_VAULT_JUMPCHAIN = 14
global SDT_VAULT_HOOD_L = 18
global SDT_VAULT_HOOD_R = 19
global SDT_FLAG_SIZE = 20


function GetFlagValueFromString theString =
(
	local returnValue = -1;

	if theString == "SDT_GRAB_EDGE" then
	(
		returnValue = SDT_GRAB_EDGE
	)
	else if theString == "SDT_GRAB_EDGE_MOVABLE" then
	(
		returnValue = SDT_GRAB_EDGE_MOVABLE
	)
	else if theString == "SDT_HIGH_FALL_EDGE" then
	(
		returnValue = SDT_HIGH_FALL_EDGE
	)
	else if theString == "SDT_COVER_EDGE" then
	(
		returnValue = SDT_COVER_EDGE
	)
	else if theString == "SDT_COVER_CORNER" then
	(
		returnValue = SDT_COVER_CORNER
	)
	else if theString == "SDT_RAILING" then
	(
		returnValue = SDT_RAILING
	)
	else if theString == "SDT_ACTOR_SYNC" then
	(
		returnValue = SDT_ACTOR_SYNC
	)
	else if theString == "SDT_STAIR_TOP" then
	(
		returnValue = SDT_STAIR_TOP
	)
	else if theString == "SDT_VAULT_ON" then
	(
		returnValue = SDT_VAULT_ON
	)
	else if theString == "SDT_VAULT_OVER" then
	(
		returnValue = SDT_VAULT_OVER
	)
	else if theString == "SDT_VAULT_SLIDE" then
	(
		returnValue = SDT_VAULT_SLIDE
	)
	else if theString == "SDT_STAIR_SURFACE" then
	(
		returnValue = SDT_STAIR_SURFACE
	)
	else if theString == "SDT_VAULT_PICKUP" then
	(
		returnValue = SDT_VAULT_PICKUP
	)
	else if theString == "SDT_VAULT_JUMP" then
	(
		returnValue = SDT_VAULT_JUMP
	)
	else if theString == "SDT_VAULT_JUMPCHAIN" then
	(
		returnValue = SDT_VAULT_JUMPCHAIN
	)
	else if theString == "SDT_VAULT_HOOD_R" then
	(
		returnValue = SDT_VAULT_HOOD_R;
	)
	else if theString == "SDT_VAULT_HOOD_L" then
	(
		returnValue = SDT_VAULT_HOOD_L;
	)

	returnValue;
)
------------------------------------------------------------------------------
-- returns true false if flag is in the flag field
------------------------------------------------------------------------------
function HasNMFlag flagField flag =
(
	local result = false;
	if ( flag >= SDT_FLAG_SIZE ) or ( flag < 0 )then
	(
		format "Checking for non existant NM Flag <%>!\n" ( flag as string )
		return false;
	)

	bitField = bit.shift 1 flag
	-- If there is data here, then we have a flag
	if( (bit.and bitField flagField) != 0 ) then
	(
		result = true
	)

	result
)

function NodeHasNMFlag obj flag =
(
	local result = false;

	if ( classOf ( obj ) == Cover_Line ) then
	(
		local flagField = Cover_Line.GetFlags ( obj );
		result = HasNMFlag flagField flag;
		
	)
	else if ( classOf ( obj ) == GrabSpline ) then
	(
		local flagField = GrabSpline.GetFlags ( obj );
		result = HasNMFlag flagField flag;
	)

	result;
)

function AddNMFlag flagField flag=
(
	local tempFlag = bit.shift 1 Flag
	flagField = bit.or flagField tempFlag
	flagField;
)

function AddNMFlagToNode obj flag=
(
	if ( classOf ( obj ) == Cover_Line ) then
	(
		format "Adding NMFLag to Cover_Line Node! %\n" ( obj as string );
		local flagField = Cover_Line.GetFlags ( obj );
		flagField = AddNMFlag flagField flag;
		Cover_Line.SetFlags ( obj ) ( flagField );
		true;
	)
	else if ( classOf ( obj ) == GrabSpline ) then
	(
		format "Adding NMFLag to GrabSpline Node! %\n" ( obj as string );
		local flagField = GrabSpline.GetFlags ( obj );
		flagField = AddNMFlag flagField flag;
		GrabSpline.SetFlags ( obj ) ( flagField );
		true;
	)

	false;
)

function RemoveNMFlag flagField flag=
(
	local tempFlag = bit.shift 1 Flag
	tempFlag = bit.not tempFlag;
	flagField = bit.and flagField tempFlag
	flagField;
)

function RemoveNMFlagFromNode obj flag=
(
	if ( classOf ( obj ) == Cover_Line ) then
	(
		local flagField = Cover_Line.GetFlags ( obj );
		flagField = RemoveNMFlag flagField flag;
		Cover_Line.SetFlags ( obj ) ( flagField );
		true;
	)
	else if ( classOf ( obj ) == GrabSpline ) then
	(
		local flagField = GrabSpline.GetFlags ( obj );
		flagField = RemoveNMFlag flagField flag;
		GrabSpline.SetFlags ( obj ) ( flagField );
		true;
	)

	false;
)
