
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\GrabCoverTool\\RSV_CoverLineUtils.ms")

fn Menu_InitializeSelectedCoverLines=
(
	for obj in selection do
	(
		if ( classOf ( obj ) == Cover_Line ) then
		(
			InitializeCoverLine( obj );
			GenerateCoverHeightMeshes ( obj ) ( true );
			DrawCoverLineNormalMeshes ( obj );
		)

		if ( classOf ( obj ) == GrabSpline ) then
		(
			InitializeGrabline( obj );
			DrawCoverLineNormalMeshes ( obj );
		)
	)
)

Menu_InitializeSelectedCoverLines();
