try (destroyDialog ro_GravityWellHelper) catch ()

rollout ro_GravityWellHelper "Gravity Well Helper" width:200
(
	--==---==--==--==--==--==--==--==
	-- Data Fields

	group "Data Fields"
	(
		spinner	spn_range "Range:" range:[0.0, 40.0, 3.0] fieldWidth:96 align:#left across:2;
		checkbox	chk_range			align:#right checked:false;

		spinner	spn_nearAngle "Near Angle:" range:[0.0, 360.0, 50.0] fieldWidth:75 align:#left across:2;
		checkbox	chk_nearAngle		align:#right checked:false;

		spinner	spn_farAngle "Far Angle:" range:[0.0, 360.0, 20.0] fieldWidth:83 align:#left across:2;
		checkbox	chk_farAngle		align:#right checked:false;

		spinner	spn_desiredSpeed "Desired Speed:" range:[0.0, 1.0, 1.0] fieldWidth:58 align:#left across:2;
		checkbox	chk_desiredSpeed	align:#right checked:false;

		spinner	spn_runoutdistance "Run Out Distance:" range:[0.0, 4.0, 0.5] fieldWidth:43 align:#left across:2;
		checkbox	chk_runoutdistance	align:#right checked:false;

		spinner	spn_runOutAngle "Run Out Angle:" range:[0.0, 360.0, 180.0] fieldWidth:58 align:#left across:2;
		checkbox	chk_runOutAngle	align:#right checked:false;
	)

	button btn_ApplySelectedData "Apply Selected Fields To Selection" align:#center width:194;
	checkbutton btn_CreateGravWell "Create Gravity Well With Template" align:#center width:194;


	fn ApplyUIShitToObj obj=
	(
		if ( classOf(obj) == PayneGravityWell ) then
		(
			if ( chk_range.checked == true ) then
			(
				obj.range = spn_range.value;
			)

			if ( chk_nearAngle.checked == true ) then
			(
				obj.nearAngle = spn_nearAngle.value;
			)

			if ( chk_farAngle.checked == true ) then
			(
				obj.farAngle = spn_farAngle.value;
			)

			if ( chk_desiredSpeed.checked == true ) then
			(
				obj.desiredSpeed = spn_desiredSpeed.value;
			)

			if ( chk_runoutdistance.checked == true ) then
			(
				obj.runoutdistance = spn_runoutdistance.value;
			)

			if ( chk_runOutAngle.checked == true ) then
			(
				obj.runOutAngle = spn_runOutAngle.value;
			)
		)
	)

	fn cb_NodeAdded=
	(
		local node = callbacks.notificationParam();
		if ( classOf(node) == PayneGravityWell ) then
		(
			ApplyUIShitToObj ( node );
		)
	)

	fn RegisterNodeAddCallback=
	(
		callbacks.addScript #sceneNodeAdded "ro_GravityWellHelper.cb_NodeAdded()" id:#GRAVWELL_HELPER_CALLBACK
	)

	fn RemoveNodeAddCallbacks=
	(
		callbacks.removeScripts id:#GRAVWELL_HELPER_CALLBACK;
	)

	on btn_ApplySelectedData pressed do
	(
		for obj in selection do
		(
			ApplyUIShitToObj ( obj );
		)
	)

	on btn_CreateGravWell changed STATE do
	(
		if ( STATE == true ) then
		(
			RegisterNodeAddCallback();
			startObjectCreation PayneGravityWell;
		)
		else
		(
			RemoveNodeAddCallbacks();
		)
	)

	on ro_GravityWellHelper close do
	(
		RemoveNodeAddCallbacks();
	)
)

try
(
	createDialog ro_GravityWellHelper
)
catch
(
	-- If this craps out, we really want to make sure we are removing all the callbacks.
	MessageBox ("ERROR: Gravity Well Helper Tool Is FUCKED!");
	--callbacks.removeScripts id:#COVEREDGE_SELECTION_CALLBACKS;
	callbacks.removeScripts id:#GRAVWELL_HELPER_CALLBACK;
)