
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\GrabCoverTool\\RSV_CoverLineUtils.ms")

fn SelectParentCoverLine =
(
	local theSelected = selection[1];
	local parentCoverLine = FindCoverLineInParents ( theSelected );

	if parentCoverLine != undefined then
	(
		clearSelection();
		select ( parentCoverLine );
	)
)

SelectParentCoverLine();