
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\GrabCoverTool\\RSV_CoverLineUtils.ms")

fn FlipLineNormals=
(
	local coverLineSelected = BuildCoverLineListFromSelection();
	if coverLineSelected != undefined then
	(
		for obj in coverLineSelected do
		(
			if ( classOf(obj) == Cover_Line ) then
			(
				FlipCoverLineNormals ( obj );
				DrawCoverLineNormalMeshes ( obj );
			)
		)
	)
)

FlipLineNormals();