-- Moved startup scripts to here to keep main RS DCC startup script folder clean or added missing scripts not called on startup
filein (RsConfigGetWildWestDir() + "script\\max\\rockstar_london\\startup\\coverAttributeDefinitions.ms")
filein (RsConfigGetWildWestDir() + "script\\max\\rockstar_london\\startup\\payne_CornerPopout.ms")
filein (RsConfigGetWildWestDir() + "script\\max\\rockstar_london\\startup\\payneGravityWell.ms")
---------------------------------------------------------------------------------------------

filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\GrabCoverTool\\SpatialUtils.ms")
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\GrabCoverTool\\RSV_CoverLineUtils.ms")
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\GrabCoverTool\\SpatialDataFlags.ms")
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\GrabCoverTool\\CoverExporter.ms")

-- I'm going to move this into it's own rollout... Fuck the old tool.
-- It sucks, and there is way too much stuff in there to support multiple rollouts on one form.

try (destroyDialog ro_CoverLineTool) catch ()

rollout ro_CoverEdgeTool "Spatial Data Tool" width:325
(
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- Local Variables
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	local INITIAL_SC_RADIUS_VALUE = 2.0;
	local INITIAL_HEIGHT_VALUE = 1.0;
	local TOOL_SETTINGS_LOCATION = (RsConfigGetProjRootDir() + "Tools\\dcc\\current\\max2009\\plugcfg\\CoverEdgeTool.ini");

	local	cb_ManualAdjustMode = undefined;
	local	manualHeightModeModifiedList = undefined;

	local	settingsStruct = undefined;
	local	cornerRange = [0.0,180.0,100.0];
	local	wallWidthRange = [0.0,10.0,1.0];

	-- Max doesn't like to delete things so I have to create an array of objects I want to delete, and then delete them in the post delete callback....
	-- Holy shit.
	local	derelictChild = #();
	local	neglectingParent = #();

	local	deletrionArrayCount = 0;
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- UI Items
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	group "Line Modification"
	(
		button	btn_InitLine				"Initialize Line"		width:280;
		button	btn_flipNormal				"Flip Line Normal"			width:280;

	)
	group "Cover Modification"
	(
		-- Get undo working on all of these adjustments.
		spinner	spn_HeightAdjusterA			"Adjust Cover Height A"		range:[0.0,20.0,1.0] type:#float scale:0.01 align:#left width:280;
		spinner	spn_HeightAdjusterB			"Adjust Cover Height B"		range:[0.0,20.0,1.0] type:#float scale:0.01 align:#left width:280;
		spinner	spn_HeightAdjusterBOTH		"Adjust Both Heights"		range:[0.0,20.0,1.0] type:#float scale:0.01 align:#left width:280;
		checkbox	cbx_LinkCoverHeights		"Link Cover Heights";
		checkbutton	ckb_ManualAdjustmentMode "Manual Adjustment Mode" width:280;

		

		-- Cover lines can now generate vault and NM information as well.
		-- Should automatically apply the flags to the line when you change one of the options.
	)

	group "Gravity Wells"
	(
		button btn_GravityWells "Gravity Well Helper" width:280;
	)

	group "SD Flags"
	(
		MultiListBox mlb_FlagArray;
	)
		
	group "Corner Generation"
	(
		checkbox	cbx_CreateOnSplineEnds		"Create Corners on Leaf Nodes" align:#left;
		spinner	spn_MinWallWidth "Min Wall Width" range:wallWidthRange type:#float scale:0.1 align:#left;
		--spinner	spn_AngularDifferenceThresh	"CB Angular Difference" range:[0.0,360.0,90.0] type:#float scale:0.01
		label	lb_cornerAngle "Maximum Corner Angle" align:#left

		angle	ang_MaxCornerAngle color:blue degrees:100.0 diameter:50 range:cornerRange align:#center across:2
		spinner	spn_CornerAngle range:cornerRange type:#float scale:0.1
	
		button	btn_GenerateCorners			"Generate Cover Corners"			width:280;

	)

	group "Visibility Helpers"
	(
		button	btn_SelectParentLine	"Select Parent Line"		width:280;
		button	btn_ShowCoverEdges		"UnHide All Spatial Data Objects"	width:280;
		checkbox	ckb_FilterBasedOnUpDir	"Filter Visibility Based On Cover Up Direction" align:#center

		--label	lb_HeightMeshes		"Height Meshes" align:#left
		button	btn_ShowHeightMeshes	"Regenerate Height Meshes"		width:145		across:2;
		--button	btn_HideHeightMeshes	"Hide"		width:135		
		

		--label	lb_NormalMeshes		"Normals"		align:#left
		button	btn_ShowNormalMeshes	"Regenerate Normal Meshes"		width:145 across:2;
		--button	btn_HideNormalMeshes	"Hide"		width:135;
		
		
		spinner	spn_angleOfUp			"Angular Difference Off Up Tolerance"		range:[0.0,180,30.0] type:#float scale:0.01 align:#left width:235;

	)

	group "Import/Export"
	(
		button	btn_ValidateCoverScene	"Validate the cover scene for XML Export"	width:280;
		button	btn_ExportCover "Export Spatial Data to XML" width:280;
		button	btn_LoadCover "Reload Spatial Data From File" width:280;
		button	btn_DestroySceneSpatialData	"Destroy Scene Spatial Data"	width:280;
		editText etx_SectionName "Section Name" width:280 height:17 align:#left;
	)
	
	on etx_SectionName changed value do (
		SetSectionNameFromBox etx_SectionName.text;
	)
	
	on spn_CornerAngle changed value do
	(
		ang_MaxCornerAngle.degrees = value;
	)

	on ang_MaxCornerAngle changed value do
	(
		spn_CornerAngle.value= value;
	)

	on btn_GravityWells pressed do
	(
		filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\GrabCoverTool\\Menu_Helper_Scripts\\GravityWellHelper.ms")
		
	)

	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- Local Helper Functions
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==

	fn FilterOnUp =
	(
		return ckb_FilterBasedOnUpDir.checked ;
	)

	local TOOL_SETTINGS_FILE = RsConfigGetWildWestDir()+"script\\max\\Rockstar_London\\config\\CoverEdgeTool.ini"
	local KEY_SETTINGS_KEY = "UI_DEFAULT"

	local MAP_WINDOWPOSX = "WindowPosX";
	local MAP_WINDOWPOSY = "WindowPosY";
	local MAP_CREATEONLEAF = "CreateOnLeaf";
	local MAP_FILTERBASEDONUP = "FilterBasedOnUp";
	local MAP_ANGULARUPTOLERANCE = "AngularUpTolerance";
	local MAP_MAXCORNERANGLE = "MaxCornerAngle";
	local MAP_LINKCOVERHEIGHTS = "LinkCoverHeights";
	local MAP_MINIMUMWALLLENGTH = "MinimumWallLength";

	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- UI Save/Load Tunables

	-- Loads the tool settings from an INI File specified in TOOL_SETTINGS_LOCATION
	fn LoadToolSettings =
	(
		local theValue = GetINISetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_WINDOWPOSX;
		if theValue != "" then
		(
			local theSubValue = GetINISetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_WINDOWPOSY;
			if theSubValue != "" then
			(
				local WindowPosition = point2 ( theValue as float ) ( theSubValue as float );
				SetDialogPos ro_CoverEdgeTool WindowPosition;
			)
		)

		theValue = GetINISetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_CREATEONLEAF;
		if theValue != "" then
		(
			cbx_CreateOnSplineEnds.checked = ( theValue as booleanClass );
		)

		theValue = GetINISetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_FILTERBASEDONUP;
		if theValue != "" then
		(
			ckb_FilterBasedOnUpDir.checked = ( theValue as booleanClass );
		)

		theValue = GetINISetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_LINKCOVERHEIGHTS;
		if theValue != "" then
		(
			cbx_LinkCoverHeights.checked = ( theValue as booleanClass );
		)

		theValue = GetINISetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_ANGULARUPTOLERANCE;
		if theValue != "" then
		(
			spn_angleOfUp.value = ( theValue as float );
		)

		theValue = GetINISetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_MINIMUMWALLLENGTH;
		if theValue != "" then
		(
			spn_MinWallWidth.value = ( theValue as float );
		)

		theValue = GetINISetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_MAXCORNERANGLE;
		if theValue != "" then
		(
			spn_CornerAngle.value = ( theValue as float );
			ang_MaxCornerAngle.degrees = ( theValue as float );
		)
	)

	-- Saves the tool settings from an INI File specified in TOOL_SETTINGS_LOCATION
	fn SaveToolSettings =
	(
		SetIniSetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_WINDOWPOSX ( (GetDialogPos ro_CoverEdgeTool).x as string );
		SetIniSetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_WINDOWPOSY ( (GetDialogPos ro_CoverEdgeTool).y as string );
		SetIniSetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_CREATEONLEAF  ( cbx_CreateOnSplineEnds.checked as string );
		SetIniSetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_FILTERBASEDONUP  ( ckb_FilterBasedOnUpDir.checked as string );
		SetIniSetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_LINKCOVERHEIGHTS  ( cbx_LinkCoverHeights.checked as string );


		-- Note: Need to change ang_MaxCornerAngle along with the spinner for settings.
		SetIniSetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_ANGULARUPTOLERANCE ( spn_angleOfUp.value as string );
		SetIniSetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_MAXCORNERANGLE ( spn_CornerAngle.value as string );
		SetIniSetting TOOL_SETTINGS_FILE KEY_SETTINGS_KEY MAP_MINIMUMWALLLENGTH ( spn_MinWallWidth.value as string );
	)


	local sd_FlagTypes = #();


	function LoadSDFlags=
	(
		LoadSpatialDataFlags();
		mlb_FlagArray.items = GetSpatialDataFlags();
	)
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- System Callback functions

	local bCoverEdgeDeleted = false;

	local	derelictChild = #();
	local	neglectingParent = #();

	fn cb_NodeUnlinked =
	(
		local node = callbacks.notificationParam();
		if ( IsTempCoverObject ( node ) ) then
		(
			appendIfUnique neglectingParent node.ParentCoverLine;
			appendIfUnique derelictChild node;
		)

		if ( classOf(node) == Payne_CornerPopout ) then
		(
			if ( node.parent != undefined ) then
			(
				node.height = Cover_Line.GetEdgeHeight node.parent node.SegmentIndex node.SideIndex
				node.SegmentIndex = -1;
				node.SideIndex = -1;
				node.wirecolor = orange;
			)
		)
	)

	fn cb_PreDeleteCoverEdge=
	(
		local node = callbacks.notificationParam();

		-- Check to see if this node is a neglecting parent
		local theParentsAddress = findItem neglectingParent node;

		-- Check to see if the address is real
		if theParentsAddress > 0 then
		(
			-- Starting at the end of the list move backwards seeing if the parent
			-- has left any children we need to destroy.
			local i = derelictChild.count;

			while i > 0 do
			(
				if ( derelictChild[i] != undefined ) and ( not isDeleted ( derelictChild[i] ) ) and ( IsTempCoverObject ( derelictChild[i] ) )then
				(
					if neglectingParent[theParentsAddress] == derelictChild[i].ParentCoverLine then
					(
						delete derelictChild[i];
						deleteItem ( derelictChild ) ( i );
					)
				)
				i = i - 1;
			)

			deleteItem ( neglectingParent ) ( theParentsAddress );
		)	
	)


	fn cb_coverLineHeightMeshChanged ev nd =
	(
		local heightMesh = GetAnimByHandle (nd[1]);
		if IsTempCoverHeightMesh ( heightMesh ) then
		(
			append manualHeightModeModifiedList heightMesh.ParentCoverLine;
			AdjustLineFromHeightMesh ( heightMesh );
		)
		
	)

	-- Appends flags to an array.
	fn AppendFlagsFromObjectToArray obj &flagArray=
	(
		if( ObjectCanHaveSDFlags ( obj ) ) then
		(
			local sdFlagArray = GetSpatialDataFlags();
			for i = 1 to sdFlagArray.count do
			(
				local currentFlag = sdFlagArray[i];

				if ( ObjectHasSDFlag ( obj ) ( currentFlag ) ) then
				(
					appendIfUnique ( flagArray ) ( currentFlag );
				)
			)
		)
	)

	fn cb_CoverEdgeSelectionChanged =
	(
		if selection.count > 0 then
		(

			/**********************************************************************************************

			1.	If Every object selected has flags, go through them all and build a list of common flags
				that bind these groups together.

			***********************************************************************************************/
			mlb_FlagArray.selection = #();

			if selection.count == 1 then
			(
				local SelectionList = #();
				AppendFlagsFromObjectToArray (selection[1]) &SelectionList;

				local selectionIndexArray = #();

				for entry in SelectionList do
				(
					local index = findItem ( mlb_FlagArray.items ) ( entry );
					if ( index > 0 ) then
					(
						append selectionIndexArray index;
					)
				)
				mlb_FlagArray.selection = selectionIndexArray;
			)
			
			/**********************************************************************************************/

			-- instead check if multiple Meshes are in the selection.
			
			local UndeterminedHeight = false;
			local FoundHeightMesh = false;
			for obj in selection do
			(
				if IsTempCoverHeightMesh ( obj ) then
				(
					if ( FoundHeightMesh ) then
					(
						UndeterminedHeight = true;
					)
					else
					(
						local parentCoverLine = obj.ParentCoverLine;
						local MeshIndex = obj.MeshIndex;

						local PointAHeight = Cover_Line.GetEdgeHeight ( parentCoverLine ) ( MeshIndex ) ( 1 );
						local PointBHeight = Cover_Line.GetEdgeHeight ( parentCoverLine ) ( MeshIndex ) ( 2 );

						spn_HeightAdjusterA.value = PointAHeight;
						spn_HeightAdjusterB.value = PointBHeight;
						spn_HeightAdjusterBOTH.value = (( PointAHeight + PointBHeight ) / 2.0);
					)
				)
				else if ( classOf(obj) == Cover_line ) then
				(
					local PointAHeight = Cover_Line.GetEdgeHeight ( obj ) ( 1 ) ( 1 );
					local PointBHeight = Cover_Line.GetEdgeHeight ( obj ) ( 1 ) ( 2 );

					spn_HeightAdjusterA.value = PointAHeight;
					spn_HeightAdjusterB.value = PointBHeight;
					spn_HeightAdjusterBOTH.value = (( PointAHeight + PointBHeight ) / 2.0);
				)
			)

		)
	)

	fn EnterManualAdjustmentMode =
	(
		if manualHeightModeModifiedList == undefined then
		(
			manualHeightModeModifiedList = #();
		)

		cb_ManualAdjustMode = NodeEventCallback geometryChanged:cb_coverLineHeightMeshChanged
	)

	fn StopManualAdjustmentMode =
	(
		if cb_ManualAdjustMode != undefined then
		(
			cb_ManualAdjustMode = undefined;
			gc();
		)
		
		if manualHeightModeModifiedList != undefined then
		(
			for obj in manualHeightModeModifiedList do
			(
				if ( FilterOnUp() ) then
				(
					GenerateCoverHeightMeshes ( obj ) ( false ) angularThreshold:spn_angleOfUp.value;
				)
				else
				(
					GenerateCoverHeightMeshes ( obj ) ( false );
				)
			)
		)

		manualHeightModeModifiedList = undefined;
	)

	function ModifyFlagsOnSelection flagToChange flagState=
	(
		for obj in selection do
		(
			if ( ( classOf ( obj ) == Cover_Line ) or (classOf ( obj ) == GrabSpline ) )then
			(
				if flagState then
				(
					AddNMFlagToNode obj flagToChange;
				)
				else
				(
					RemoveNMFlagFromNode obj flagToChange;
				)
			)
		)
	)

	function ShowAllCoverObjects theNode=
	(
		if ( classOf ( theNode ) == Cover_Line ) or ( classOf ( theNode ) == Payne_CornerPopout ) then
		(
			unhide ( theNode );
		)

		for child in theNode.children do
		(
			ShowAllCoverObjects ( child );
		)
	)


	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- Event Handlers
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	on btn_flipNormal pressed do
	(

		local coverLineSelected = BuildCoverLineListFromSelection();
		if coverLineSelected != undefined then
		(

			for obj in coverLineSelected do
			(
				if ( classOf ( obj ) == Cover_Line ) then
				(
					FlipCoverLineNormals ( obj );
					DrawCoverLineNormalMeshes ( obj );
				)
			)
		)

		local grabSplineSelected = BuildGrabLineListFromSelection();
		if grabSplineSelected != undefined then
		(
			for obj in grabSplineSelected do
			(
				if ( classOf ( obj ) == GrabSpline ) then
				(
					FlipCoverLineNormals ( obj );
					DrawCoverLineNormalMeshes ( obj );
				)
			)
		)
	)

	on btn_ShowCoverEdges pressed do
	(
		ShowAllCoverObjects ( rootNode );
	)

	-- Height Adjustment UI Components
	on spn_HeightAdjusterA changed spinnerValue do
	(
		SetSelectedCoverHeights ( spinnerValue ) ( RSCL_GetSideAKey() ) ( cbx_LinkCoverHeights.checked )
	)

	on spn_HeightAdjusterB changed spinnerValue do
	(
		SetSelectedCoverHeights ( spinnerValue ) ( RSCL_GetSideBKey() ) ( cbx_LinkCoverHeights.checked )
	)

	on spn_HeightAdjusterBOTH changed spinnerValue do
	(
		SetSelectedCoverHeights ( spinnerValue ) ( RSCL_GetBothSideKey() ) ( cbx_LinkCoverHeights.checked )
	)

	on btn_ShowHeightMeshes pressed do
	(
		if selection.count > 0 then
		(
			for obj in selection do
			(
				obj = FindCoverLineInParents ( obj );
				if classOf(obj) == Cover_Line then
				(
					if ( FilterOnUp() ) then
					(
						GenerateCoverHeightMeshes ( obj ) ( true ) angularThreshold:spn_angleOfUp.value;
					)
					else
					(
						GenerateCoverHeightMeshes ( obj ) ( true );
					)
				)
			)
		)
		else
		(
			local result = yesNoCancelBox "Regenerate all Cover Height Meshes, this can be slow. Do you wish to proceed?"
			if result == #yes then
			(
				if ( FilterOnUp() ) then
				(
					RecursiveShowAllCoverLineMeshes ( rootNode ) angularDiff:spn_angleOfUp.value;
				)
				else
				(
					RecursiveShowAllCoverLineMeshes ( rootNode );
				)
			)
		)
	)

	on btn_HideHeightMeshes pressed do
	(
		if selection.count > 0 then
		(
			for obj in selection do
			(
				obj = FindCoverLineInParents ( obj );
				if classOf(obj) == Cover_Line then
				(
					DeleteLineHeightMeshes ( obj );
				)
			)
		)
		else
		(
			local result = yesNoCancelBox "Hide All Cover meshes?"
			if result == #yes then
			(
				RecursiveDestroyHeightMeshes ( rootNode );
			)
		)
	)
	
	
	on btn_ShowNormalMeshes pressed do
	(
		clearListener();
		if selection.count > 0 then
		(
			for obj in selection do
			(
				obj = FindCoverLineInParents ( obj );
				if classOf(obj) == Cover_Line then
				(
					format "DEUBG: btn_ShowNormalMeshes() Found CoverLine!\n"
					if ( FilterOnUp() ) then
					(
						DrawCoverLineNormalMeshes ( obj ) angularThreshold:spn_angleOfUp.value;
					)
					else
					(
						DrawCoverLineNormalMeshes ( obj );
					)
					
				)
			)
		)
		else
		(
			local result = yesNoCancelBox "Regenerate all Normal Meshes, this can be slow. Do you wish to proceed?"
			if result == #yes then
			(
				if ( FilterOnUp() ) then
				(
					RecursiveShowAllCoverNormalMeshes ( rootNode ) angularDiff:spn_angleOfUp.value;
				)
				else
				(
					RecursiveShowAllCoverNormalMeshes ( rootNode );
				)
			)
		)

	)

	on btn_HideNormalMeshes pressed do
	(
		if selection.count > 0 then
		(
			for obj in selection do
			(
				obj = FindCoverLineInParents ( obj );
				if classOf(obj) == Cover_Line then
				(
					DeleteNormalMeshes ( obj );
				)
			)
		)
		else
		(
			local result = yesNoCancelBox "Hide All Normal Meshes?"
			if result == #yes then
			(
				RecursiveDestroyNormalMeshes ( rootNode );
			)
		)

	)

	on btn_SelectParentLine pressed do
	(
		local theSelected = selection[1];
		local parentCoverLine = FindCoverLineInParents ( theSelected );

		if parentCoverLine != undefined then
		(
			clearSelection();
			select ( parentCoverLine );
		)
	)

	on ckb_ManualAdjustmentMode changed state do
	(
		if ( state == true ) then
		(
			EnterManualAdjustmentMode();
		)
		else
		(
			StopManualAdjustmentMode();
		)
	)

	on btn_InitLine pressed do
	(
		for obj in selection do
		(
			if ( classOf ( obj ) == Cover_Line ) then
			(
				InitializeCoverLine( obj );
				if ( FilterOnUp() ) then
				(
					GenerateCoverHeightMeshes ( obj ) ( true ) angularThreshold:spn_angleOfUp.value;
					DrawCoverLineNormalMeshes ( obj ) angularThreshold:spn_angleOfUp.value;
				)
				else
				(
					GenerateCoverHeightMeshes ( obj ) ( true );
					DrawCoverLineNormalMeshes ( obj );
				)
				
			)

			if ( classOf ( obj ) == GrabSpline ) then
			(
				InitializeGrabline( obj );
				if ( FilterOnUp() ) then
				(
					DrawCoverLineNormalMeshes ( obj ) angularThreshold:spn_angleOfUp.value;
				)
				else
				(
					DrawCoverLineNormalMeshes ( obj );
				)
			)
		)
	)

	on btn_GenerateCorners pressed do
	(
		for obj in selection do
		(
			if ( classOf ( obj ) == Cover_Line ) then
			(
				AddCornersToCoverLine ( obj ) ( ang_MaxCornerAngle.degrees ) ( cbx_CreateOnSplineEnds.checked ) ( spn_MinWallWidth.value );
			)
		)
	)

	fn DEBUG_PrintFlags obj=
	(
		if ( ObjectCanHaveSDFlags ( obj ) ) then
		(
			format "SD Object: %\n" obj.name;

			for item in mlb_FlagArray.items do
			(
				local bStateofFlag = ObjectHasSDFlag ( obj ) ( item );
				format "\t% => <%>\n" item ( bStateofFlag as string );
			)
		)
		else
		(
			format "Non-SD Object: %\n" obj.name;
		)
	)

	on mlb_FlagArray selected index do
	(

		local theFlag = mlb_FlagArray.items[index];
		local bSetFlag = mlb_FlagArray.selection[index];

		for currentNode in selection do
		(
			--format "\tOn Node<%>\n" currentNode.name;
			if ( ObjectCanHaveSDFlags ( currentNode ) ) then
			(
				--format "\t\tCan Have SD FLAG\n"
				if ( bSetFlag ) then
				(
					--format "\t\Adding Flag!\n"
					AddSDFlagToObject ( currentNode ) ( theFlag );
					
				)
				else
				(
					--format "\t\Removing Flag!\n"
					RemoveSDFlagFromObject ( currentNode ) ( theFlag );
				)
				local bStateofFlag = ObjectHasSDFlag ( currentNode ) ( theFlag );
				--format "\tFlag % State:<%>\n" theFlag (bStateofFlag as string);
			)
			else
			(
				--format "\t\tNot an SD Flag Node\n"
			)
			
			--DEBUG_PrintFlags ( currentNode );

		)
	)

	fn cb_NodeCreated=
	(
		local node = callbacks.notificationParam();
		if ( node != undefined ) then
		(
			local layer = undefined
			if( classOf( node ) == Cover_line ) then
			(
				layer = layermanager.getLayerFromName "Cover_line"
				if layer == undefined then
				(
					layer = LayerManager.newLayerFromName "Cover_line"
				)
				layer.addnode node;
			)
			else if( classOf( node ) == Payne_CornerPopout ) then
			(
				layer = layermanager.getLayerFromName "Payne_CornerPopout"
				if layer == undefined then
				(
					layer = LayerManager.newLayerFromName "Payne_CornerPopout"
				)
				layer.addnode node;
			)
			else if( classOf( node ) == GrabSpline ) then
			(
				layer = layermanager.getLayerFromName "GrabSpline"
				if layer == undefined then
				(
					layer = LayerManager.newLayerFromName "GrabSpline"
				)
				layer.addnode node;
			)
			else if( classOf( node ) == PayneGravityWell ) then
			(
				layer = layermanager.getLayerFromName "PayneGravityWell"
				if layer == undefined then
				(
					layer = LayerManager.newLayerFromName "PayneGravityWell"
				)
				layer.addnode node;
			)

		)
	)

	

	on btn_ValidateCoverScene pressed do
	(
		ValidateCoverScene();
	)

	function AddCallbacks=
	(
		callbacks.addScript #selectionSetChanged "ro_CoverEdgeTool.cb_CoverEdgeSelectionChanged()" id:#COVEREDGE_SELECTION_CALLBACKS

		-- These two callbacks handle dangling cover meshes when a parent line is deleted.
		callbacks.addScript #nodeUnlinked "ro_CoverEdgeTool.cb_NodeUnlinked()" id:#COVEREDGE_SELECTION_CALLBACKS
		callbacks.addScript #nodePreDelete "ro_CoverEdgeTool.cb_PreDeleteCoverEdge()" id:#COVEREDGE_SELECTION_CALLBACKS
		callbacks.addScript #nodeCreated "ro_CoverEdgeTool.cb_NodeCreated()" id:#COVEREDGE_SELECTION_CALLBACKS
	)

	function RemoveCallbacks=
	(
		callbacks.removeScripts id:#COVEREDGE_SELECTION_CALLBACKS
	)


	on btn_DestroySceneSpatialData pressed do
	(
		local returnVal = yesNoCancelBox "Are you sure you want to Delete all the Spatial Data in the Scene? Any unsaved or unexported work will be lost!";
		if returnVal == #yes then
		(
			RemoveCallbacks();
			PurgeSceneSpatialData();
			AddCallbacks();
		)
	)

	fn ExportSpatialData=
	(
		RecursiveDestroyHeightMeshes ( rootNode );
		RecursiveDestroyNormalMeshes ( rootNode );

		BreakUpSceneCompoundCoverLines();

		if ( ValidateCoverScene() ) then
		(
			RemoveCallbacks();
			Export_SpatialData();
			gc();
			AddCallbacks();
			messagebox "Spatial Data Export Complete!";
		)
		else
		(
			messagebox "Error Validating Cover Scene! Export Aborted.";
		)

		RecursiveShowAllCoverLineMeshes ( rootNode );
		RecursiveShowAllCoverNormalMeshes ( rootNode );
	)
	
	fn LoadSpatialData bPurgeExisting=
	(
		RemoveCallbacks();
		Import_SpatialData ( bPurgeExisting );
		gc();
		AddCallbacks();
	)

	fn LoadSpatialDataWithQuery=
	(
		local returnVal = yesNoCancelBox "Do You Want To Purge Existing Data in the Scene?";
		if returnVal == #yes then
		(
			LoadSpatialData( true );
		)
		else if returnVal == #no then
		(
			LoadSpatialData( false );
		)
	)

	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	on btn_ExportCover pressed do
	(
		ExportSpatialData();
		etx_SectionName.text = GetSectionNameForBox();
	)

	on btn_LoadCover pressed do
	(
		LoadSpatialDataWithQuery();
		etx_SectionName.text = GetSectionNameForBox();
	)
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--=='

	fn QueryExportDataBeforeClose=
	(
		local returnVal = yesNoCancelBox "Do you want export before shutting down the tool? Any unexported data will be lost!";
		if returnVal == #yes then
		(
			ExportSpatialData();
		)
	)


	on ro_CoverEdgeTool open do
	(
		LoadToolSettings();
		LoadSDFlags();

		RemoveCallbacks();
		AddCallbacks();

		RecursiveDestroyNormalMeshes ( rootNode );
		RecursiveDestroyHeightMeshes ( rootNode );

		RecursiveShowAllCoverLineMeshes ( rootNode );
		RecursiveShowAllCoverNormalMeshes ( rootNode );

	)


	on ro_CoverEdgeTool close do
	(
		RemoveCallbacks();
		SaveToolSettings();
		-- Just in case it's still running.
		StopManualAdjustmentMode();

		RecursiveDestroyNormalMeshes ( rootNode );
		RecursiveDestroyHeightMeshes ( rootNode );

		gc();
		
	)
	
)


try
(
	createDialog ro_CoverEdgeTool
)
catch
(
	-- If this craps out, we really want to make sure we are removing all the callbacks.
	MessageBox ("Error Detected In Spatial Data Tool!");
	-- print out the ecception for debugging purposes
	format "*** % ***\n" (getCurrentException());
	callbacks.removeScripts id:#COVEREDGE_SELECTION_CALLBACKS;
)
