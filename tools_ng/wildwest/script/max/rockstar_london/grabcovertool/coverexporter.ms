include "rockstar\\export\\settings.ms"
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\GrabCoverTool\\RSV_CoverLineUtils.ms")

sectionName = undefined;

function GetSectionNameForBox = 
(
	sectionName;
)

function SetSectionNameFromBox boxValue =
(
	sectionName = boxValue;
)

function FindCornersAndReparent oldLine newLine newLineIndex=
(
	if( oldLine != undefined ) and ( newLine != undefined ) then
	(
		for child in oldLine.children do
		(
			if classOf ( child ) == Payne_CornerPopout then
			(
				local segmentIndex = child.SegmentIndex;
				if segmentIndex == newLineIndex then
				(
					child.parent = newLine;
					child.SegmentIndex = 1;
				)
			)
		)
	)
)

function BreakUpCoverLine obj=
(
	if( not Cover_Line.CoverDataIsInitialized (obj) ) then
	(
		print ("Warning: BreakUpCoverLineAndExport() - Non Initialized Cover_Line trying to be exported. Skipping. <" + obj.name + ">" )
		return false;
	)

	currentTransform = obj.transform;
	currentRotation = obj.rotation;

	totalKnotCount = Cover_Line.GetKnotCount obj
	if totalKnotCount > 2 then
	(
		-- Generate an edge for every edge on this compound edge.
		for i = 2 to totalKnotCount do
		(
			--format "\t\t\t\t<Name>%</Name>\n" obj.name to:fout
			-- Knot 1 Information
			local Knot1 = Cover_Line.GetKnot (obj) (i-1)
			Knot1 = Knot1 * currentTransform;

			local Knot1SoftCoverRadius = Cover_Line.GetSoftCoverRadius (obj) (i-1)
			local Knot1HardCoverRadius = Cover_Line.GetHardCoverRadius (obj) (i-1)
			local Height1 = Cover_Line.GetEdgeHeight (obj) (i-1) (1)

			-- Knot 2 Information
			local Knot2 = Cover_Line.GetKnot (obj) (i)
			Knot2 = Knot2 * currentTransform

			
			local Knot2SoftCoverRadius = Cover_Line.GetSoftCoverRadius (obj) (i)
			local Knot2HardCoverRadius = Cover_Line.GetHardCoverRadius (obj) (i)
			local Height2 = Cover_Line.GetEdgeHeight (obj) (i-1) (2)

			local Normal = Cover_Line.GetEdgeNormal (obj) (i-1)
			Normal = (Normal) * (currentTransform.rotation);
			Normal = normalize Normal;

			local UpVector = copy currentTransform[3];
			UpVector = normalize UpVector;

			newLine = Cover_line();
			newLine.name = ( obj.name + "-" + ((i-1) as string ));

			Cover_line.Pushknot newLine Knot2;
			Cover_line.Pushknot newLine Knot1;
			Cover_line.InitCoverLine newLine;
			--CenterPivot newLine;
			Cover_line.CenterPivot newLine newLine
			Cover_line.SetEdgeHeight newLine 1 1 Height1
			Cover_line.SetEdgeHeight newLine 1 2 Height2

			Cover_line.SetHardCoverRadius newLine 1 Knot1HardCoverRadius
			Cover_line.SetHardCoverRadius newLine 2 Knot1HardCoverRadius

			Cover_line.SetSoftCoverRadius newLine 1 Knot1SoftCoverRadius
			Cover_line.SetSoftCoverRadius newLine 2 Knot1SoftCoverRadius

			Cover_line.SetEdgeNormal newLine 1 Normal
			
			FindCornersAndReparent obj newLine ( i - 1 );


			-- Tranform Objects

			-- If it's the last edge and it's a closed spline get the last edge data.
			if (i == totalKnotCount) and (Cover_Line.IsClosed obj) then
			(
				local Knot1 = Cover_Line.GetKnot (obj) (totalKnotCount)
				Knot1 = Knot1 * currentTransform
				
				local Knot1SoftCoverRadius = Cover_Line.GetSoftCoverRadius (obj) (totalKnotCount)
				local Knot1HardCoverRadius = Cover_Line.GetHardCoverRadius (obj) (totalKnotCount)

				local Knot2 = Cover_Line.GetKnot (obj) (1)
				Knot2 = Knot2 * currentTransform
				
				local Knot2SoftCoverRadius = Cover_Line.GetSoftCoverRadius (obj) (1)
				local Knot2HardCoverRadius = Cover_Line.GetHardCoverRadius (obj) (1)
				local Height1 = Cover_Line.GetEdgeHeight (obj) (i) (1)
				local Height2 = Cover_Line.GetEdgeHeight (obj) (i) (2)
				
				local Normal = Cover_Line.GetEdgeNormal (obj) (i)
				Normal = (Normal) * (currentTransform.rotation);
				Normal = normalize Normal;

				local UpVector = copy currentTransform[3];
				UpVector = normalize UpVector;
				newLine = Cover_line();
				newLine.name = ( obj.name + "-" + ((i) as string ));

				Cover_line.Pushknot newLine Knot2;
				Cover_line.Pushknot newLine Knot1;
				Cover_line.InitCoverLine newLine;
				--CenterPivot newLine;
				Cover_line.CenterPivot newLine newLine
				Cover_line.SetEdgeHeight newLine 1 1 Height1
				Cover_line.SetEdgeHeight newLine 1 2 Height2

				Cover_line.SetHardCoverRadius newLine 1 Knot1HardCoverRadius
				Cover_line.SetHardCoverRadius newLine 2 Knot1HardCoverRadius

				Cover_line.SetSoftCoverRadius newLine 1 Knot1SoftCoverRadius
				Cover_line.SetSoftCoverRadius newLine 2 Knot1SoftCoverRadius

				Cover_line.SetEdgeNormal newLine 1 Normal
				
				FindCornersAndReparent obj newLine ( i );
			)

		)
		delete obj;
	) --	if totalKnotCount > 2 then
)

function GrabLineFlagCopy sourceLine destinationLine=
(
	local bError = false;

	if ( classOf(sourceLine) != GrabSpline ) then
	(
		print ("Warning: GrabLineFlagCopy() - Source Line is not a GrabSpline. <" + sourceLine.name + ">" );
		bError = true;
	)
	else
	(
		if ( not GrabSpline.LineIsInitialized (sourceLine) ) then
		(
			print ("Warning: GrabLineFlagCopy() - Source Line is not Initialized. <" + sourceLine.name + ">" );
			bError = true;
		)
	)

	if ( classOf(destinationLine) != GrabSpline ) then
	(
		print ("Warning: GrabLineFlagCopy() - Destination Line is not a GrabSpline. <" + destinationLine.name + ">" );
		bError = true;
	)
	else
	(
		if ( not GrabSpline.LineIsInitialized (destinationLine) ) then
		(
			print ("Warning: GrabLineFlagCopy() - Destination Line is not Initialized. <" + destinationLine.name + ">" );
			bError = true;
		)
	)

	if ( not bError ) then
	(
		local aSDFlags = GetSpatialDataFlags();
		for i = 1 to aSDFlags.count do
		(
			local currentFlag = aSDFlags[i];
			if ( ObjectHasSDFlag ( sourceLine ) ( currentFlag ) ) then
			(
				AddSDFlagToObject ( destinationLine ) ( currentFlag );
			)
			else
			(
				RemoveSDFlagFromObject ( destinationLine ) ( currentFlag );
			)
		)
	)
)

function BreakUpGrabLine obj=
(
	if( (not classOf(obj) == GrabSpline ) or ( not GrabSpline.LineIsInitialized (obj) ) ) then
	(
		print ("Warning: BreakUpGrabLine() - Non Initialized GrabSpline trying to be Divided Skipping. <" + obj.name + ">" )
		return false;
	)

	currentTransform = obj.transform;
	currentRotation = obj.rotation;

	local sourceLine = obj;

	local totalEdgeCount = GrabSpline.GetEdgeCount obj
	if totalEdgeCount > 1 then
	(
		-- Generate an edge for every edge on this compound edge.
		for i = 1 to totalEdgeCount do
		(
						-- If it's the last edge and it's a closed spline get the last edge data.
			if (i == totalEdgeCount) and (GrabSpline.IsClosed obj) then
			(
				local Knot1 = GrabSpline.GetKnot (obj) (i)
				Knot1 = Knot1 * currentTransform
				
				local Knot2 = GrabSpline.GetKnot (obj) (1)
				Knot2 = Knot2 * currentTransform
				
				local Normal = GrabSpline.GetNormal (obj) (i)
				Normal = (Normal) * (currentTransform.rotation);
				Normal = normalize Normal;

				local UpVector = copy currentTransform[3];
				UpVector = normalize UpVector;
				newLine = GrabSpline();
				newLine.name = ( obj.name + "-" + ((i) as string ));

				GrabSpline.Pushknot newLine Knot2;
				GrabSpline.Pushknot newLine Knot1;
				GrabSpline.InitLine newLine;
				--CenterPivot newLine;
				GrabSpline.CenterLinePivot newLine newLine

				GrabSpline.SetNormal newLine 1 Normal

				GrabLineFlagCopy (sourceLine) (newLine);
				AddSDObjectToLayer( newLine );
			
			)
			else
			(
				--format "\t\t\t\t<Name>%</Name>\n" obj.name to:fout
				-- Knot 1 Information
				local Knot1 = GrabSpline.GetKnot (obj) (i)
				Knot1 = Knot1 * currentTransform;

				-- Knot 2 Information
				local Knot2 = GrabSpline.GetKnot (obj) (i + 1)
				Knot2 = Knot2 * currentTransform

				local Normal = GrabSpline.GetNormal (obj) (i)
				Normal = (Normal) * (currentTransform.rotation);
				Normal = normalize Normal;

				local UpVector = copy currentTransform[3];
				UpVector = normalize UpVector;

				newLine = GrabSpline();
				newLine.name = ( obj.name + "-" + ((i) as string ));

				GrabSpline.Pushknot newLine Knot2;
				GrabSpline.Pushknot newLine Knot1;
				GrabSpline.InitLine newLine;
				--CenterPivot newLine;
				GrabSpline.CenterLinePivot newLine newLine

				GrabSpline.SetNormal newLine 1 Normal

				GrabLineFlagCopy (sourceLine) (newLine);
				AddSDObjectToLayer( newLine );
			)
		)
		delete obj;
	) --	if totalKnotCount > 2 then
)

function GetIPLName=
(
	local tempArray =#();
	tempArray = filterString maxFileName "."

	tempArray[1];
)

function GetLevelName=
(
	local tempArray =#();
	tempArray = filterString maxFilePath "\\"

	local counter = 0;
	for i = 1 to tempArray.count do
	(
		local subStr = tempArray[i];
		subStr = RsUppercase subStr;

		if subStr == "MAPS" then
		(
			counter = i+1;
		)
	)

	tempArray[counter];
)

fn CleanUpLayers=
	(
		local layer = undefined;

		layer = layermanager.getLayerFromName "Cover_line"
		if layer != undefined then
		(
			layermanager.deleteLayerByName "Cover_line"
		)
		
		
		layer = layermanager.getLayerFromName "Payne_CornerPopout"
		if layer != undefined then
		(
			layermanager.deleteLayerByName "Payne_CornerPopout"
		)

		
		layer = layermanager.getLayerFromName "GrabSpline"
		if layer != undefined then
		(
			layermanager.deleteLayerByName "GrabSpline"
		)

		
		layer = layermanager.getLayerFromName "PayneGravityWell"
		if layer != undefined then
		(
			layermanager.deleteLayerByName "PayneGravityWell"
		)

		
		layer = layermanager.getLayerFromName "TempCoverHeightMesh"
		if layer != undefined then
		(
			layermanager.deleteLayerByName "TempCoverHeightMesh"
		)

		
		layer = layermanager.getLayerFromName "TempArrowMesh"
		if layer != undefined then
		(
			layermanager.deleteLayerByName "TempArrowMesh"
		)
	)

function PurgeSceneSpatialData=
(
	RecursiveDestroyHeightMeshes ( rootNode );
	RecursiveDestroyNormalMeshes ( rootNode );

	for node in rootNode.children do
	(
		for child in node.children do
		(
			if classOf( child ) == Payne_CornerPopout then
			(
				delete child;
			)
		)

		if classOf( node ) == Cover_line then
		(
			delete node;
		)
		else if classOf( node ) == Payne_CornerPopout then
		(
			delete node;
		)
		else if classOf( node ) == GrabSpline then
		(
			delete node;
		)
		else if classOf( node ) == PayneGravityWell then
		(
			delete node;
		)
	)

	CleanUpLayers();
)

function BreakUpSceneCompoundCoverLines=
(
	for obj in rootNode.children do
	(
		if classOf( obj ) == Cover_line then
		(
			BreakUpCoverLine obj;
		)
		else if classOf( obj ) == GrabSpline then
		(
			BreakUpGrabLine obj;
		)
	)
)

function Export_SpatialData=
(
	RecursiveDestroyHeightMeshes ( rootNode );
	RecursiveDestroyNormalMeshes ( rootNode );

	-- BreakUpSceneCompoundCoverLines();

	local levelName = GetLevelName();
	local projRootDir = RsConfigGetProjRootDir();
	for i = 1 to projRootDir.Count do
	(
		if projRootDir[i] == "/" then
		(
			projRootDir[i] = "\\"
		)
	)
	local fileName = projRootDir + "build\\dev\\common\\data\\levels\\" + levelName + "\\SpatialData.xml"; 
	if (sectionName == undefined or sectionName == ""  or sectionName == " ") then
	(
		local sectionName = GetIPLName();
	) 

	local ShitLine = Cover_line();
	Cover_line.SaveSceneData ShitLine fileName sectionName;
	delete ShitLine;

	RecursiveShowAllCoverLineMeshes ( rootNode );
	RecursiveShowAllCoverNormalMeshes ( rootNode );
)

function Import_SpatialData bPurgeExisting=
(
	RecursiveDestroyHeightMeshes ( rootNode );
	RecursiveDestroyNormalMeshes ( rootNode );

	if( bPurgeExisting ) then
	(
		PurgeSceneSpatialData();
	)

	local levelName = GetLevelName();
	local projRootDir = RsConfigGetProjRootDir();
	for i = 1 to projRootDir.Count do
	(
		if projRootDir[i] == "/" then
		(
			projRootDir[i] = "\\"
		)
	)
	local fileName = projRootDir + "build\\dev\\common\\data\\levels\\" + levelName + "\\SpatialData.xml"; 
	if (sectionName == undefined or sectionName == ""  or sectionName == " ") then
	(
		local sectionName = GetIPLName();
	) 

	local ShitLine = Cover_line();
	Cover_line.LoadSceneData ShitLine fileName sectionName;
	delete ShitLine;

	RecursiveShowAllCoverLineMeshes ( rootNode );
	RecursiveShowAllCoverNormalMeshes ( rootNode );
	
)
