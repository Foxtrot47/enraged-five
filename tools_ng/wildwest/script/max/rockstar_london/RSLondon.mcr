-- //rage/jimmy/dev/rage/framework/tools/wildwest/script/max/Rockstar_London/RSLondon.mcrMacros for Rockstar London Tools

include "rockstar/export/settings.ms"

-- Menu is organized alphabetical. Please keep this organized like this.

-- ///////////////////////////////////////////////////////////////////////////
-- "Character"
-- ///////////////////////////////////////////////////////////////////////////
macroscript CharacterSwapper
	category:"Character"
	ButtonText:"CharacterSwapper"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RST_CharacterSwapper.ms")
)
macroscript CharacterBatchExport
	category:"Character"
	ButtonText:"Character Batch Export"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_CharacterBatchExport.ms")
)
macroscript AmbientLighting
	category:"Character"
	ButtonText:"Ambient Lighting"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\AmbientLighting.ms")
)


-- ///////////////////////////////////////////////////////////////////////////
-- "Collision"
-- ///////////////////////////////////////////////////////////////////////////


macroscript AutoWindowTypeSize
	category:"Collision"
	ButtonText:"Auto Window Type Size"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_AutoWindowTypeSize.ms")
)

macroscript AutoSurfaceTypeAssign
	category:"Collision"
	ButtonText:"Auto Surface Type Assign"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_AutoSurfaceTypeAssign.ms")
)

macroscript CollisionOps
	category:"Collision"
	ButtonText:"Collision Ops"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_CollisionOps_DotNet.ms")
)

macroscript CollisionTagger
	category:"Collision"
	ButtonText:"Collision Tagger"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_colTagger.ms")
)

macroscript CollisionPolyChecker
	category:"Collision"
	ButtonText:"Collision Poly Checker"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_CollisionPolyChecker.ms")
)

macroscript AutoRoomID
	category:"Collision"
	ButtonText:"Auto Room ID"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\pipeline\\RSL_CollisionPrimitive.ms")
)


-- ///////////////////////////////////////////////////////////////////////////
-- "Design"
-- ///////////////////////////////////////////////////////////////////////////

macroscript PESSceneEditor
	category:"Design"
	ButtonText:"PES Scene Editor"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\Design\\RSL_PES_SceneEditor.ms")
)

macroscript PESSceneSearcher
	category:"Design"
	ButtonText:"PES Scene Searcher"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\Design\\RSL_PES_SceneSearcher.ms")
)

macroscript SimpleLevelCreator
	category:"Design"
	ButtonText:"Simple Level Creator"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\Design\\RSL_SimpleLevelCreator.ms")
)

macroscript FireBoxExporter
	category:"Design"
	ButtonText:"Fire Box Exporter"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\Design\\RSV_FireBoxExporter.ms")
)

macroscript LevelCleaner
	category:"Design"
	ButtonText:"Level Cleaner"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\Design\\RSV_LevelCleaner.ms")
)

macroscript ScriptHelper
	category:"Design"
	ButtonText:"Script Helper"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\Design\\RSV_ScriptHelper.ms")
)

macroscript TriggerBoxHelpUI
	category:"Design"
	ButtonText:"Trigger Box Help UI"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\Design\\RSV_TriggerBoxHelpUI.ms")
)


-- ///////////////////////////////////////////////////////////////////////////
-- "Helpers"
-- ///////////////////////////////////////////////////////////////////////////
macroscript ObjectTypeOps
	category:"Helpers"
	ButtonText:"Object Type Ops"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_ObjectTypeOps.ms")
)

macroscript IPLStreamExport
	category:"Helpers"
	ButtonText:"IPL Stream Export"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_IPLStreamExport.ms")
)
macroscript Toolbox
	category:"Helpers"
	ButtonText:"Toolbox"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_ToolBox.ms")
)
macroscript AlignGizmoToFace
	category:"Helpers"
	ButtonText:"Align Gizmo To Face"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_AlignGizmoToFace.ms")
)

macroscript ExplodeElements
	category:"Helpers"
	ButtonText:"Explode Elements"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_ExplodeElements.ms")
)

macroscript ExplodeToElements
	category:"Helpers"
	ButtonText:"Explode to Elements"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_ExplodeToElements.ms")
)

macroscript VehicleAnimator
	category:"Helpers"
	ButtonText:"Vehicle Animator"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\Design\\RSL_VehicleAnimator.ms")
)

macroscript MaterialLibraryOps
	category:"Helpers"
	ButtonText:"Material Library Ops"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_MaterialLibraryOps.ms")
)

-- ///////////////////////////////////////////////////////////////////////////
-- "Interior"
-- ///////////////////////////////////////////////////////////////////////////

macroscript MiloCollisionToolV1
	category:"Interior"
	ButtonText:"Milo Collision Tool V1"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\MiloTools\\MiloCollisionTool_v1.0.ms")
)

macroscript MiloInfo
	category:"Interior"
	ButtonText:"Milo Info to Listener"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\MiloTools\\RSL_MiloInfo.ms")
)

macroscript MiloOps
	category:"Interior"
	ButtonText:"Milo Operations"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\MiloTools\\RSL_MiloOps.ms")
)

-- ///////////////////////////////////////////////////////////////////////////
-- "Lighting"
-- ///////////////////////////////////////////////////////////////////////////

macroscript LightOps
	category:"Lighting"
	ButtonText:"Light Ops"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_LightOps.ms")
)

-- ///////////////////////////////////////////////////////////////////////////
-- "Misc Tools"
-- ///////////////////////////////////////////////////////////////////////////


-- ///////////////////////////////////////////////////////////////////////////
-- "Multiplayer"
-- ///////////////////////////////////////////////////////////////////////////

macroscript GameModeFileCreator
	category:"Multiplayer"
	ButtonText:"Game Mode File Creator"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_GameModeFileCreator.ms")
)

macroscript SpawnPointMarker
	category:"Multiplayer"
	ButtonText:"Spawn Point Marker"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_SpawnPointMarker.ms")
)


-- ///////////////////////////////////////////////////////////////////////////
-- "Modeling"
-- ///////////////////////////////////////////////////////////////////////////
macroscript DetachByID
	category:"Modeling"
	ButtonText:"Detach By ID"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\detachbyID.ms")
)

macroscript DecalPlanter
	category:"Modeling"
	ButtonText:"Decal Planter"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_DecalPLanter.ms")
)

macroscript mmPainter
	category:"Modeling"
	ButtonText:"MM Painter"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_MMPainter.ms")
)



-- ///////////////////////////////////////////////////////////////////////////
-- "Optimization"
-- ///////////////////////////////////////////////////////////////////////////
macroscript ObjectCleaner
	category:"Optimization"
	ButtonText:"Object Cleaner"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_ObjectCleaner.ms")
)

macroscript MeshCleaner
	category:"Optimization"
	ButtonText:"Mesh Cleaner"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_MeshCleaner.ms")
)

macroscript LightLister
	category:"Optimization"
	ButtonText:"Light Lister"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_LightLister.ms")
)

macroscript SetShadowCasting
	category:"Optimization"
	ButtonText:"Set Shadow Casting"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_SetShadowCasting.ms")
)
macroscript SceneBrowser
	category:"Optimization"
	ButtonText:"Scene Browser"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_SceneBrowser_v2.ms")
)

macroscript SceneCleaner
	category:"Optimization"
	ButtonText:"Scene Cleaner"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_SceneCleaner.ms")
)

macroscript RSLSceneStats
	category:"Optimization"
	ButtonText:"Scene Stats"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\scenestats.ms")
)

macroscript RSLTextureStats
	category:"Optimization"
	ButtonText:"Texture Stats"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\TextureStats.ms")
)
macroscript AutoLODDistance
	category:"Optimization"
	ButtonText:"Auto LOD Distance"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_AutoLODDistance.ms")
)

macroscript LodBaker
	category:"Optimization"
	ButtonText:"LOD Baker"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_LODBaker.ms")
)

macroscript LodEditor
	category:"Optimization"
	ButtonText:"LOD Editor"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_LODEditor.ms")
)

macroscript lodOptimizerTools
	category:"Optimization"
	ButtonText:"LOD Optmization Tools"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_OptimizerTools.ms")

)

macroscript TerrainBaker
	category:"Optimization"
	ButtonText:"Terrain Baker"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_TerrainBaker.ms")
)

macroscript MemoryStats
	category:"Optimization"
	ButtonText:"Scene Memory Stats"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_SceneMemoryStats.ms")
)

-- ///////////////////////////////////////////////////////////////////////////
-- "Particle"
-- ///////////////////////////////////////////////////////////////////////////
macroscript ValidateParticles
	category:"Particle"
	ButtonText:"Validate Particles"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_ValidateParticles.ms")
)

macroscript ParticleHelper
	category:"Particle"
	ButtonText:"Particle Helper"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_Particle_Helper.ms")
)
-- ///////////////////////////////////////////////////////////////////////////
-- "Prop"
-- ///////////////////////////////////////////////////////////////////////////
macroscript SetAllDynamicCollisionNonCover
	category:"Prop"
	ButtonText:"Set All Dynamic Collision Non Cover"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSV_SetAllDynamicCollisionNonCover.ms")
)

macroscript SetAllDynamicCollisionNonObstacle
	category:"Prop"
	ButtonText:"Set All Dynamic Collision Non Obstacle"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSV_SetAllDynamicCollisionNonObstacle.ms")
)

macroscript propviewer
	category:"Prop"
	ButtonText:"Prop Viewer"
(
	local propViewerFilename = RsConfigGetBinDir() + "/PropViewer_RSL/PropViewer.exe"
	shelllaunch propViewerFilename ""

)

macroscript BatchPropRender
	category:"Prop"
	ButtonText:"Batch Prop Render"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSV_PropDictionary\\RSL_BatchPropRender.ms")
)

macroscript PropPlacer
	category:"Prop"
	ButtonText:"Prop Placer"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSV_PropDictionary\\RSL_PropPlacer_v2.ms")
)

macroscript PropDictionaryEditorDotNet
	category:"Prop"
	ButtonText:"Prop Dictionary Editor"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSV_PropDictionary\\RSL_PropDictionaryEditor_v2.ms")
)

macroscript PropThumbsRenderer
	category:"Prop"
	ButtonText:"Render Props Thumbnails"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSV_PropDictionary\\RSV_QuickThumbnailRender.ms")
)

macroscript MergeProp
	category:"Prop"
	ButtonText:"Merge Prop"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_MergeProp.ms")
)

macroscript PropTxdViewer
	category:"Prop"
	ButtonText:"Prop Txd Viewer"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_PropTxdViewer.ms")
)

macroscript objectfinder
	category:"Prop"
	ButtonText:"Object Finder"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_ObjectFinder.ms")
)

macroscript NoAICover
	category:"prop"
	ButtonText:"No AI Cover"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_NoAICover.ms")
)

macroscript rebuildxrefs
	category:"prop"
	ButtonText:"Rebuild Xrefs"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_FisXrefs.ms")
)
-- ///////////////////////////////////////////////////////////////////////////
-- "Shaders"
-- ///////////////////////////////////////////////////////////////////////////
macroscript RageShaderValueEditor
	category:"Shaders"
	ButtonText:"Rage Shader Value Editor"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_RageShaderValueEditor.ms")
)

macroscript LiveShaderEditorImport
	category:"Shaders"
	ButtonText:"Live Shader Editor Import"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_LiveShaderImportOps.ms")
)

macroscript GTAToRage
	category:"Shaders"
	ButtonText:"GTA To Rage"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_GTAToRage.ms")
)

macroscript MaterialConverter
	category:"Shaders"
	ButtonText:"Material Converter"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_MaterialConverter.ms")
)


macroscript xrefmaterialtools
	category:"Shaders"
	ButtonText:"XRef Material Tools"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_XRefMaterialTools.ms")
)
-- ///////////////////////////////////////////////////////////////////////////
-- "Spatial Data"
-- ///////////////////////////////////////////////////////////////////////////
macroscript FlipLineNormals
	category:"Spatial Data"
	ButtonText:"Flip Line Normals"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\grabcovertool\\menu_helper_scripts\\FlipLineNormals.ms")
)

macroscript GravityWellHelper
	category:"Spatial Data"
	ButtonText:"Gravity Well Helper"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\grabcovertool\\menu_helper_scripts\\GravityWellHelper.ms")
)

macroscript InitializeSelectedCoverLines
	category:"Spatial Data"
	ButtonText:"Initialize Selected Cover Lines"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\grabcovertool\\menu_helper_scripts\\InitializeSelectedCoverLines.ms")
)

macroscript SelectParentCoverLine
	category:"Spatial Data"
	ButtonText:"Select Parent Cover Line"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\grabcovertool\\menu_helper_scripts\\SelectParentCoverLine.ms")
)

macroscript CoverEdge
	category:"Spatial Data"
	ButtonText:"Cover Edge"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\grabcovertool\\CoverEdge.ms")
)
-- ///////////////////////////////////////////////////////////////////////////
-- "Texture"
-- ///////////////////////////////////////////////////////////////////////////
macroscript TextureTag
	category:"Texture"
	ButtonText:"Texture Tag"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_TextureTag.ms")
)

macroscript TextureTagEdit
	category:"Texture"
	ButtonText:"Texture Tag Edit"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_TextureTagEdit.ms")
)

macroscript TextureTagMenu
	category:"Texture"
	ButtonText:"Texture Tag Menu"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_TextureTagMenu.ms")
)

macroscript TextureTag2
	category:"Texture"
	ButtonText:"Texture Tag 2"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_TextureTag_v2.ms")
)


macroscript TXDOpsLite
	category:"Texture"
	ButtonText:"TXD Ops Lite"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_TXDOpsLite.ms")

)
macroscript TXDOps2
	category:"Texture"
	ButtonText:"TXD Ops 2"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_TXDOps2.ms")

)

macroscript TXDReparent
	category:"Texture"
	ButtonText:"TXD Re-parenter"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_TXDReparent.ms")
)


macroscript ExportParents
	category:"Texture"
	ButtonText:"Export Parent TXDs"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_TXDOpsExportParent.ms")

)


macroscript BatchTXDOps
	category:"Texture"
	ButtonText:"Batch TXD Ops"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_batchTXDOptimiser.ms")

)

macroscript TXDViewer
	category:"Texture"
	ButtonText:"TXD Viewer"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_TXDViewer.ms")
)
-- ///////////////////////////////////////////////////////////////////////////
-- "Utils"
-- ///////////////////////////////////////////////////////////////////////////

macroscript MapRenderSetup
	category:"Utils"
	ButtonText:"Map Render Setup"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\createMapPlane.ms")

)

macroscript FixMyMap
	category:"Utils"
	ButtonText:"Fix My Map!"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\rs_MapFixer.ms")

)

macroscript DeleteEmptyLayers
	category:"Utils"
	tooltip:"Delete Empty Layers"
	ButtonText:"Delete Empty Layers"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\cleanuplayers.ms")
)

macroscript RSL_MM_ChannelSwap
	category:"Utils"
	ButtonText:"Micro-Movement Channel Swap"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_MM_ChannelSwap.ms")

)

-- ///////////////////////////////////////////////////////////////////////////
-- Misc tools
-- ///////////////////////////////////////////////////////////////////////////



macroscript FragTuner
	category:"Misc"
	ButtonText:"Frag Tuner"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_FragTuner.ms")
)

macroscript JoinElements
	category:"Misc"
	ButtonText:"Join Elements"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_JoinElements.ms")
)



macroscript NormalFixer
	category:"Misc"
	ButtonText:"Normal Fixer"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_NormalFixer.ms")
)

macroscript NormalOps
	category:"Misc"
	ButtonText:"Normal Ops"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_NormalOps.ms")
)


macroscript RemoveEdges
	category:"Misc"
	ButtonText:"Remove Edges"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_RemoveEdges.ms")
)

macroscript RemoveInteriorFaces
	category:"Misc"
	ButtonText:"Remove Interior Faces"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_RemoveInteriorFaces.ms")
)

macroscript RemoveVerts
	category:"Misc"
	ButtonText:"Remove Verts"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_RemoveVerts.ms")
)


macroscript FindMapInfo
	category:"Utils"
	ButtonText:"Find Map Info"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\FindMapInfo.ms")
)

macroscript ModelRenderer
	category:"Utils"
	ButtonText:"Model Renderer"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_ModelRenderer.ms")
)


macroscript PathsRemapTool
	category:"Utils"
	ButtonText:"Paths Remap"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_PathsRemapTool.ms")
)


macroscript Reporter
	category:"Utils"
	ButtonText:"Reporter"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_Reporter.ms")
)


macroscript VehicleEditor
	category:"Utils"
	ButtonText:"Vehicle Editor"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_VehicleEditor.ms")
)


macroscript OldSceneCleanup
	category:"Utils"
	ButtonText:"Old Scene Cleanup"
(
	filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSV_OldSceneCleanup.ms")
)

