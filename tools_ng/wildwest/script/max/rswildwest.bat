@ECHO OFF
REM 
REM  Builds the RS Wildwest menus.
REM
REM  Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
REM  Date:: 14 December 2009
REM

TITLE Wildwest Max Menu Builder
SET MAXRUBY=%RUBYLIB%\util\max\
SET WILDWEST=%RS_TOOLSROOT%\wildwest\script\max\


PUSHD %MAXRUBY%

cd %MAXRUBY%

ECHO Starting Ruby script...
ECHO.
ruby max_wildwest_menu.rb --wildwest=%WILDWEST%
ECHO.
POPD

ECHO.
ECHO.
ECHO The Wildwest menu builder has finished.
PAUSE

REM rswildwest.bat