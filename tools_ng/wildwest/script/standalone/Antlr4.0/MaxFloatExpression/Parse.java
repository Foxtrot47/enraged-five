import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class Parse {
    public static void main(String[] args) throws Exception {
        String inputFile = null;
        if ( args.length>0 ) inputFile = args[0];
        InputStream is = System.in;
        if ( inputFile!=null ) is = new ByteArrayInputStream(inputFile.getBytes("UTF-8"));
        ANTLRInputStream input = new ANTLRInputStream(is);
        MaxFloatExpressionLexer lexer = new MaxFloatExpressionLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MaxFloatExpressionParser parser = new MaxFloatExpressionParser(tokens);
        ParseTree tree = parser.expression(); // parse

        MotionBuilderPython eval = new MotionBuilderPython();
        String lString = eval.visit(tree);
		System.out.println(lString);
    }
}