@ECHO OFF
REM CopyMaxAssemblies.bat
REM
REM Copies 3dsmax assemblies to a specific output directory; used in Visual
REM Studio post-build events based on the build configuration.
REM
REM Example usage:
REM   CALL $(RS_TOOLSROOT)\wildwest\script\dos\CopyMaxAssemblies.bat $(SolutionName) $(TargetPath) $(PlatformName) $(MAX_DEPLOY) $(TargetFileName)
REM
REM Version history:
REM   Initial Version: GD
REM   10 April 2012:   DHM added PDB copying for debug info 
REM	  1  April 2014:   GD copying all dependencies
REM    (see https://devstar.rockstargames.com/wiki/index.php/Dev:Debugging_3dsmax_.Net_Assemblies).
REM

ECHO %1

IF %1==RSG.MaxAssemblies (
	GOTO:DoCopy
) ELSE (
	ECHO Non max configuration, not copying to max directory
	GOTO:EOF
)
:DoCopy
SET TARGET_PATH=%4


set USE_X64_TARGET=false
IF %3==x64 set USE_X64_TARGET=true
IF %3==AnyCPU set USE_X64_TARGET=true

IF "%USE_X64_TARGET%"=="true" (
	SET TARGET_PATH=%TARGET_PATH%\x64
)


ECHO copying
ECHO source path: %2
ECHO target path: %TARGET_PATH%
ECHO file name:   %5
COPY "*.dll" %TARGET_PATH%

REM Copy PDB if it exists
SET PDB_DRIVE=%~d2
SET PDB_FILENAME="*.pdb"
SET PDB_PATH=%~p2
SET PDB_SOURCE=%PDB_DRIVE%%PDB_PATH%%PDB_FILENAME% 

ECHO PDB Source: %PDB_SOURCE%%PDB_PATH%%PDB_FILENAME% 
ECHO PDB Target: %TARGET_PATH%
IF EXIST %PDB_SOURCE% COPY "%PDB_SOURCE%" "%TARGET_PATH%"

REM Done.
