rem Object Profiler bug 713461
rem To specify objects to test, create a text file X:\gta5\build\dev\common\data\displayobects.txt
rem Containing a list, 1 per line, with a maximum number of objects of 128

cd x:\gta5\build\dev
call game_xenon_beta.bat -smoketest=object_profile -level=testbed -nopopups -noidlecam