REM Copies and resizes highres flats
REM Rick Stirling March 2009
REM Edited by Stewart Wright December 2009 - New file paths


REM ConsoleRes might not exist
mkdir ConsoleRes


REM If it does, ensure files are checked out from Perforce
cd ConsoleRes
ruby x:/jimmy/tools/lib/util/perforce/p4_edit.rb ...


REM perform the copy
xcopy ..\highres\*.* . /y




REM Copy the resize tool into the folder
copy x:\jimmy\tools\dcc\current\max2009\scripts\character\Tools\LAB_Sharpen_Resize_50pc.exe .


REM for each image file (TGA, BMP, TIF, PNG, BMP), pass it to the droplet
for %%f in (*.tga *.bmp *.tif *.png *.bmp) do LAB_Sharpen_Resize_50pc.exe %CD%\%%f 

