REM Process project .ipxml files

cd /d X:\gta5\assets_ng\export\levels\gta5\_citye\downtown_01\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_citye\downtown_01\instance_placement\DT1_20.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_citye\hollywood_01\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_citye\hollywood_01\instance_placement\hw1_10.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_citye\indust_01\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_citye\indust_01\instance_placement\id1_04.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_citye\indust_02\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_citye\indust_02\instance_placement\id2_04.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_citye\port_01\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_citye\port_01\instance_placement\PO1_05.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_citye\scentral_01\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_citye\scentral_01\instance_placement\SC1_00f.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_cityw\airport_01\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_cityw\airport_01\instance_placement\ap1_01_a.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_cityw\koreatown_01\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_cityw\koreatown_01\instance_placement\KT1_16.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_cityw\sanpedro_01\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_cityw\sanpedro_01\instance_placement\sp1_01.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_cityw\santamon_01\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_cityw\santamon_01\instance_placement\sm_20.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_cityw\venice_01\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_cityw\venice_01\instance_placement\vb_34.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_hills\cityhills_01\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_hills\cityhills_01\instance_placement\ch1_01.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_hills\cityhills_02\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_hills\cityhills_02\instance_placement\ch2_01.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_hills\cityhills_03\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_hills\cityhills_03\instance_placement\ch3_01.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_hills\country_01\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_hills\country_01\instance_placement\cs1_01.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_hills\country_02\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_hills\country_02\instance_placement\cs2_01.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_hills\country_03\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_hills\country_03\instance_placement\cs3_01.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_hills\country_04\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_hills\country_04\instance_placement\cs4_01.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_hills\country_05\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_hills\country_05\instance_placement\cs5_1.ipxml

cd /d X:\gta5\assets_ng\export\levels\gta5\_hills\country_06\instance_placement
%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe --rebuild X:\gta5\assets_ng\export\levels\gta5\_hills\country_06\instance_placement\cs6_01.ipxml