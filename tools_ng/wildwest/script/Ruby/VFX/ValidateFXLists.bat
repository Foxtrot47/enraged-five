@echo OFF

CALL setenv.bat
TITLE %RS_PROJECT% : Validating VFX FXLists...
PUSHD %RS_PROJROOT%

CLS
ECHO Validating FXList %1
%RS_TOOLSRUBY% %RS_TOOLSROOT%/wildwest/script/ruby/VFX/VFXDependencyCheck.rb --fxlist %1

POPD

pause