REM Batch file to trigger Ruby script
REM Sends a texture list to a photoshop droplet
REM Rick Stirling <rick.stirling@rockstarnorth.com>


CALL setenv.bat

REM call the script with a droplet to use
%RS_TOOLSRUBY% %RS_TOOLSROOT%/wildwest/script/ruby/Texture_processing/data_send_clonedtextures_to_droplet.rb X:\gta5\tools\wildwest\script\photoshop\tonemappingADJ_mapDiffuse_LIBERTY.exe c:\DIFFUSEtextureslist.txt true

pause