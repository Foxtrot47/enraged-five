SET LOCAL_LOG_DIR=%1

PUSHD %LOCAL_LOG_DIR%
FOR /f %%i IN ('dir /B *.lgz') DO %~dp0uncompressLog.py %%i
POPD