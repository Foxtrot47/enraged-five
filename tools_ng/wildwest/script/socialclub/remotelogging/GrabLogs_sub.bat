:: Usage
::
::	GrabLogs_sub.bat <server_path> <namespace> <date_sub_string>
::
::	Examples:
::	  Grabbing PS3 logs from dev server from Oct03 2012
::		GrabLogs_sub.bat \\tk2ewrsan3-nas\rsg_userstorage_dev\ np 20121003
::
::	  Grabbing PS3 logs from dev server for all dates
::		GrabLogs_sub.bat \\tk2ewrsan3-nas\rsg_userstorage_dev\ np
::
::

SET SERVERPATHDIR=%1
SET NAMESPACE=%2
SET DATESUBSTR=%3

subst S: /D

subst S: %SERVERPATHDIR%

S:

cd S:\members\%NAMESPACE%

FOR /f %%i IN ('dir /S /B remotelogs') DO call :ProcessDir %%i


goto :eof

:ProcessDir
pushd .
echo %1
cd %1\gta5
FOR /f "tokens=5 delims=\\" %%f IN ('echo %1') do set memberID=%%f
FOR /f %%j IN ('dir /B %DATESUBSTR%*.lgz') DO copy %%j S:\mplogs\%memberID%_%%j

popd

goto :eof

pause