There are two parts to this process: Log gathering and decompression.

The logs are sent to the member namespace of the cloud from the game to the users' member folder.  Using teh GrabLogs_* scripts run on a machine that has direct access to the cloud files, you run a bat file to iterate through all of the cloud member folders and pull out all of the files.

GrabLogs_sub.bat is the main script that is called.  Usage is as follows:
:: Usage
::
::	GrabLogs_sub.bat <server_path> <namespace> <date_sub_string>
::
::	Examples:
::	  Grabbing PS3 logs from dev server from Oct03 2012
::		GrabLogs_sub.bat \\tk2ewrsan3-nas\rsg_userstorage_dev\ np 20121003
::
::	  Grabbing PS3 logs from dev server for all dates
::		GrabLogs_sub.bat \\tk2ewrsan3-nas\rsg_userstorage_dev\ np


Servers:
DEV: \\tk2ewrsan3-nas\rsg_userstorage_dev\ 
PROD: \\tk2ewrsan3-nas\rsg_cloudstorage_prod\

Namespaces:
PS3: np
XBox360: xbl



Ulitmately, it copies all of the compressed logs to s:\mplogs, renaming each file to have the user' user_id appended to the file name for organizational purposes.

All of the files in that directory should be zipped up to be copied to the local user's machine to be uncompressed.

Use UncompressLogs.bat <local_path_to_logs> to uncompress the logs into stanard text.

Summary order of operations for grabbing dev ps3 logs:
1.) Log into a machine on the TakeTWo QWEST area.  I use RSGEWRSQL3dv.
2.) Run GrabLogs_NP_DEV_ALL.bat to gather all the logs to S:\mplogs
3.) Select all the files in S:\mplogs and zip them up into a zip file
4.) Copy zip file to local machine (say to X:\gta5\logs)
5.) Run 'UncompressLogs.bat X:\gta5\logs'
