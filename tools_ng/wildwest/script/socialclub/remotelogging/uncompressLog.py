import zlib
import sys

if len(sys.argv) < 2:
	sys.exit("Missing input filename")
	
filename = sys.argv[1]
print filename

file = open(filename, 'rb')
data = file.read()
file.close()

#data = data.replace('\r\n', '\n')

unzipped_data = zlib.decompress(data)

filename = filename.replace("lgz", "log")

outFile = open(filename, 'w')
outFile.write(unzipped_data)
outFile.close()