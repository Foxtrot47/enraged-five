@ECHO OFF
REM
REM File:: quick_install.bat
REM Description:: Top-level tools quick install process.
REM
REM Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
REM date:: 24 June 2014
REM

REM in case we are calling from some other location
pushd "%~dp0"

SET POWERSHELL=powershell.exe
SET RS_PREV_TOOLSROOT=%RS_TOOLSROOT%
SET RS_TOOLSROOT=%CD%
SET RS_LOCALFILE=%RS_TOOLSROOT%\local.xml
SET RS_CONSOLE_INSTALLER=%RS_TOOLSROOT%\bin\install\installer\ConsoleInstaller.exe

:: Check whether the suer has ever installed the tools.
IF NOT EXIST %RS_LOCALFILE% (
	
	CALL %RS_TOOLSROOT%\install.bat
	GOTO END
)

REM Make sure that the batch script is being run as an admin.
CALL %RS_TOOLSROOT%\script\util\check_admin_privileges.bat
IF ERRORLEVEL 1 (
	IF "%1"=="error" (
		GOTO ERROR_END
	) ELSE (
		powershell "saps -FilePath %0 -ArgumentList error -Verb runas"
		exit /b
	)
)

REM =================================================
REM Stop RAG and SystrayRfs.
REM =================================================

ECHO Killing SystrayRFS and RAG
taskkill /IM /F SysTrayRfs.exe >NUL 2>NUL
taskkill /IM /F rag.exe >NUL 2>NUL
taskkill /IM /F ragApp.exe >NUL 2>NUL

REM =================================================
REM Run our installer executable
REM Only one now as we can read both x86 and x64 registry.
REM =================================================

SET INSTALLER_ARGS=
IF NOT "%~1"=="" (
	SET INSTALLER_ARGS=--default_branch %~1
)
IF "%~2"=="nopopups" (
	SET INSTALLER_ARGS=%INSTALLER_ARGS% --nopopups
)

ECHO Running Tools Installer
%RS_CONSOLE_INSTALLER% %INSTALLER_ARGS%

REM Installer canceled?
IF ERRORLEVEL 2 GOTO END
REM Installer error?
IF ERRORLEVEL 1 GOTO ERROR_END

REM Register File Association Extensions
REM Re-generate the REG file first, then import into Windows Registry.
SET TOOLSBIN=%RS_TOOLSROOT%\bin
SET TOOLSLIB=%RS_TOOLSROOT%\lib
SET REGISTER_EXTENSIONS_SCRIPT=%TOOLSLIB%\util\registry\generate_register_extensions.rb
SET REGISTER_EXTENSIONS=%RS_TOOLSROOT%\tmp\register_extensions.reg
ECHO Generating file associations script...
%TOOLSBIN%\ruby\bin\ruby -W0 -rrubygems -I%TOOLSLIB% %REGISTER_EXTENSIONS_SCRIPT%
ECHO Importing file associations script...
REG IMPORT %REGISTER_EXTENSIONS%
ECHO Resetting tray icons...
CALL %TOOLSBIN%\resetTray.bat -f

REM =================================================
REM Initialize Context Menu
REM =================================================

call %RS_TOOLSROOT%\bin\install\unregister_shell_extension.bat
call %RS_TOOLSROOT%\bin\install\register_shell_extension.bat

REM =================================================
REM Install Python
REM =================================================
REM %RS_TOOLSROOT%\bin\python\App\setup\SetupPython.bat 

ECHO Done.
GOTO END

:ERROR_END

ECHO Error running Rockstar Games Tools Installer.  Speak to your local tools support.
PAUSE

:END

popd

REM quick_install.bat
