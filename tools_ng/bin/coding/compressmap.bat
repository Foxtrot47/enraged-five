@echo off
setlocal
set src="%~1"
set dst=
if not "%~2"=="" set dst="%~2"
set plat=%~3
set ret=0
set cmpexe="%~dp0compressmap_x64_gen9.exe"
set cmpcmd=%cmpexe% %src% %dst% %plat%

if /i "%plat%"=="-orbis" goto:scenm
if /i "%plat%"=="-prospero" goto:scenm
goto:begin

:scenm
set cmpexe="%~dp0prospero-llvm-nm.exe"   
set cmpcmd=%cmpexe% --demangle --no-sort %src% ^| "%~dp0nm_to_cmp" %dst%

:begin
echo Starting compressmap...
ping -w 1000 -n 2 127.0.0.1>nul
if not exist %cmpexe% (
echo ERROR: compressmap tool not found [%cmpexe%]
goto :failed
)
if not exist %src% (
echo Warning: input file not found [%src%]
goto :quit
)
echo Executing %cmpcmd:|=^|%
%cmpcmd%
if %ERRORLEVEL% neq 0 (
echo ERROR: [%ERRORLEVEL%] calling [%cmpcmd:|=^|%]
goto :failed
)
goto :quit
:failed
set ret=1
:quit
echo Finished compressmap [%ret%] 
exit /b %ret%
