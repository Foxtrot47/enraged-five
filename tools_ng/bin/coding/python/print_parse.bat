@echo off
setlocal
set PARSE=
set CUSTOM=
call %1
if defined PARSE (
	echo Parse {
	echo %PARSE%
	echo }
)
if defined CUSTOM (
	echo Custom {
	echo %CUSTOM%
	echo }
)
endlocal
