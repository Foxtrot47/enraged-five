@ECHO OFF
SETLOCAL

REM syncs the files in this directory with the ones from the rage directory

REM uncomment these lines if you also want a one-stop shop for regenerating the projects too
pushd %RAGE_DIR%\base\tools\cli\parCodeGen
call build.bat
if ERRORLEVEL 1 goto :EOF
pushd %RAGE_DIR%\base\tools\cli\stripobjects
call build.bat
if ERRORLEVEL 1 goto :EOF
REM pushd %RAGE_DIR%\suite\src\actiontree
REM build_py2exe.bat

call setenv

pushd %RS_TOOLSBIN%\coding\python
p4 edit ./...
copy %RAGE_DIR%\framework\tools\bin\coding\python\*.* .
p4 revert -a ./...

echo %0 Finished!
