import os
from P4 import P4, P4Exception
from audio_logger import Logger, LogSeverity

P4SERVER = r'rsgperforce'
P4PORT = r'1666'
P4CLIENT = r'dailySync-GTAVdev_ng'
P4USER = r'svcrsgaudauto'
P4PASSWD = r'few-xSz5bxN'
P4DEPOT = r'//depot'

P4_ARGS = (
    r'-p4host '+ P4SERVER + ':' + P4PORT,
    r'-p4client '+P4CLIENT,
    r'-p4user '+P4USER,
    r'-p4passwd '+P4PASSWD,
    r'-p4depotroot '+P4DEPOT,
    )

PERFORCE_EXE_NAME = r'p4'

class P4Helper:

    def __init__(self, logger):
        self.logger = logger
        self.p4 = None
        self.changeList = None
        self.initialise_p4environment()

    def initialise_p4environment(self):
        self.logger.log("Setting P4 environment", LogSeverity.INFO)

        cmd = '%s set P4PORT=%s:%s' % (PERFORCE_EXE_NAME, P4SERVER, P4PORT)
        self.logger.log("  %s" % cmd, LogSeverity.INFO)
        if os.system(cmd) != 0:
            self.logger.log("Command failed: %s" % cmd, LogSeverity.ERROR)
            return -1

        cmd = '%s set P4CLIENT=%s' % (PERFORCE_EXE_NAME, P4CLIENT)
        self.logger.log("  %s" % cmd, LogSeverity.INFO)
        if os.system(cmd) != 0:
            self.logger.log("Command failed: %s" % cmd, LogSeverity.ERROR)
            return -1

        cmd = '%s set P4USER=%s' % (PERFORCE_EXE_NAME, P4USER)
        self.logger.log("  %s" % cmd, LogSeverity.INFO)
        if os.system(cmd) != 0:
            self.logger.log("Command failed: %s" % cmd, LogSeverity.ERROR)
            return -1

        cmd = '%s set P4PASSWD=%s' % (PERFORCE_EXE_NAME, P4PASSWD)
        self.logger.log("  %s" % cmd, LogSeverity.INFO)
        if os.system(cmd) != 0:
            self.logger.log("Command failed: %s" % cmd, LogSeverity.ERROR)
            return -1
    
        return 0

    def connect(self):

        self.p4 = P4()
        self.p4.port = P4SERVER + ':' + P4PORT
        self.p4.password = P4PASSWD
        self.p4.user = P4USER
        self.p4.client = P4CLIENT
        self.p4.connect()

        if not self.p4.connected():
            self.logger.log("Failed to connect to perforce", LogSeverity.ERROR)
            return -1

        return 0

    def create_cl(self, description):
        try:
            self.changeList = self.p4.fetch_change()
            self.changeList["Description"] = description
            return self.p4.save_change(self.changeList)[0].split()[1]

        except P4Exception, ex:
            logger.log(str(ex), LogSeverity.ERROR)
            return -1


    def checkout(self, filePath, changeListNumber):

        if not self.p4.connected() : self.p4.connect()

        # get depot path from local path
        depotPath = self.get_depot_path(filePath)

        if self.exists_in_depot(filePath):
            self.sync(depotPath)
            self.logger.log("Checking out %s" % depotPath, LogSeverity.INFO)
            try:
                self.p4.run_edit("-c", changeListNumber, depotPath)
            except P4Exception, ex:
                self.logger.log("Failed to checkout %s with error %s" % (depotPath ,str(ex)), LogSeverity.ERROR)
                return -1
        else:
            self.logger.log("Marking for add %s" % depotPath, LogSeverity.INFO)
            try:
                self.p4.run_add("-c", changeListNumber, depotPath)
            except P4Exception, ex:
                self.logger.log("Failed to checkout %s with error %s" % (depotPath, str(ex)), LogSeverity.ERROR)
                return False

        return True

    def exists_in_depot(self, filePath):
        depotPath = self.get_depot_path(filePath)
        try:
            result = self.p4.run_fstat(depotPath)
            action = result[0]['headAction'].upper()
            return action != 'DELETE'

        except P4Exception, ex:
            return False

    def submit_cl(self, changeList):
        try:
            self.p4.run("submit","-c",changeList)
        except P4Exception, ex:
            self.logger.log(str(ex), LogSeverity.ERROR)
            return False
        self.p4.disconnect()
        
        return True

    def revert_cl(self, changeList, ext, delete=False):
        self.p4.run("revert","-c",changeList, "*"+ext)

        if delete:
            self.p4.run("change", "-d", changeList)

    def sync(self, depotPath):
        try:
            self.p4.run("sync", depotPath)
        except P4Exception, ex:
            for e in ex.errors:
                self.logger.log(str(e), LogSeverity.ERROR)

    def get_depot_path(self, filePath):
        filePath = filePath.upper()
        pathP4Format = filePath.replace('\\', '/')
        pathP4FormatDepotRoot = pathP4Format.replace("X:", "//depot")
        return pathP4FormatDepotRoot