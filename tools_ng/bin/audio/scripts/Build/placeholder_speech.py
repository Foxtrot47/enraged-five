import datetime
import os

from audio_logger import Logger, LogSeverity
from P4 import P4


# SMTP email notification settings
SMTP_FROM = 'Ivona Speech Generator <ivona@rockstarnorth.com>'
SMTP_TO = (
	'Danjeli Schembri <Danjeli.Schembri@rockstarnorth.com>',
    'Alastair MacGregor <alastair.macgregor@rockstarnorth.com>',
    'Matthew Smith <matthew.smith@rockstarnorth.com>',
    'Ben Durrenberger <ben.durrenberger@rockstarnorth.com>',
    )

P4_ARGS = (
    r'-p4host rsgperforce:1666',
    r'-p4client PLACEHOLDERSPEECH',
    r'-p4user buildmanager.audio',
    r'-p4depotroot //depot',
    r'-p4passwd rockstar1A',
    )
    
# Speech gen process settings
MAX_ERRORS = 5
SPEECH_GEN_ARGS_BASE = (
    r'-workingPath x:',
    r'-project \gta5\audio\{branch}\projectSettings.xml',
	r'-databuilder X:\gta5\tools_ng\bin\audio\AudioDataBuilder\audioDataBuilder.exe'
    )

# Runtime data
NOW_START = datetime.datetime.now()
NOW_START_STR = NOW_START.strftime('%y_%m_%d__%H_%M_%S')

# Executable settings
SPEECH_GEN_WORKING_PATH = r'x:\gta5\tools_ng\bin\audio\PlaceholderSpeechGenerator\Bin'
SPEECH_GEN_EXE_NAME = r'speechgen.exe'
SPEECH_GEN_LOG_PATH = os.path.join(r'C:\Temp\Logs\Placeholder Speech', NOW_START_STR)

RUN_LOG_FILENAME = r'run_log.txt'

DEFAULT_BRANCH = 'dev_ng'

# Speech gen process runtime variables
cycle_count = 0

logger = None
use_p4_env = False
branch = None


def generate_placeholder_speech(rebuild = False, p4_env=False, no_email = False, branch_to_use=None):
    global logger
    global use_p4_env
    global branch

    use_p4_env = p4_env
    branch = branch_to_use

    if branch is None:
        loggerName="[GTA5 "+DEFAULT_BRANCH+"] Placeholder Speech Generation"
    else:
        loggerName="[GTA5 "+branch+"] Placeholder Speech Generation"

    logger = Logger(loggerName, SPEECH_GEN_LOG_PATH)

    # Run specified process
    result = run(rebuild)
    
    logger.log("Placeholder generation process finished", LogSeverity.INFO)
    
    # Send mail alert to admins
    if not no_email:
        logger.log("Mailing results...", LogSeverity.INFO)
        logger.mail_results(SMTP_FROM, SMTP_TO)
    
    logger.save(True)
    
    return result


def set_working_path(path):
    logger.log("Setting working path as %s" % path, LogSeverity.INFO)
    os.chdir(path)


def run(rebuild):
    
    logger.log("Running speech generation process...", LogSeverity.INFO)

    # Add log path to args
    log_path = os.path.join(SPEECH_GEN_LOG_PATH, 'speech_gen_process_log.txt')
    args = get_speech_gen_args() + [
        r'-logfile "%s"' % log_path,
        ]

    if rebuild:
        args += [
            r'-rebuild',
            ]

    args_str = ' '.join(args)

    # Run main speech gen process
    return run_speech_gen(args_str)


def run_speech_gen(args):
    global cycle_count
    
    set_working_path(SPEECH_GEN_WORKING_PATH)
    
    # Execute speech generator process
    cmd = '%s %s' % (SPEECH_GEN_EXE_NAME, args)
    logger.log("Executing cmd: %s" % cmd, LogSeverity.INFO)
    result = os.system(cmd)
    
    # Check for errors
    if result < 0:
        msg = '%s failed with error code %d\n' % (SPEECH_GEN_EXE_NAME, result)
        logger.log(msg, LogSeverity.ERROR)
    
    # Increment cycle count for next iteration
    cycle_count += 1
    
    # Save log after each speech generator process call
    logger.save(True)
    
    return result

    
def get_speech_gen_args():
    base_args = []
    for arg in SPEECH_GEN_ARGS_BASE:
        base_args += [
            arg.replace('{branch}', branch or DEFAULT_BRANCH)
            ]

    # Use preset P4 settings
    if use_p4_env:
        return base_args + list(P4_ARGS)

    # Use P4 settings set in environment variables
    p4 = P4()
    return base_args + [
        r'-p4host %s' % p4.port,
        r'-p4client %s' % p4.client,
        r'-p4user %s' % p4.user,
        r'-p4depotroot //depot',
        ]
