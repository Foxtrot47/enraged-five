
import xml.etree.ElementTree as et


built_waves_paths = {
    'Xbox360': r'X:\gta5\audio\dev\build\xbox360\BuiltWaves\SPEECH_PACK_FILE.xml',
    'PS3': r'X:\gta5\audio\dev\build\PS3\BuiltWaves\SPEECH_PACK_FILE.xml',
    'PC': r'X:\gta5\audio\dev\build\PC\BuiltWaves\SPEECH_PACK_FILE.xml',
    }

def run():
    
    for platform, path in built_waves_paths.items():
        
        print "Checking built waves for %s...\n" % platform
        
        root = et.parse(path).getroot()
        
        for bank_folder in root.findall('BankFolder'):
            bank_folder_name = bank_folder.attrib['name']
            for bank in bank_folder.findall('Bank'):
                bank_name = bank.attrib['name']
                voice_tag_found = False
                for tag in bank.findall('Tag'):
                    name = tag.attrib['name']
                    if name is not None and name == 'voice':
                        voice_tag_found = True
                        break
                if not voice_tag_found:
                    print "Voice tag not found in: %s/%s" % (bank_folder_name, bank_name)
        
        print "\n"
    

if __name__ == '__main__':
    run()