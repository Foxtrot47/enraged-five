
function ListWaves($pathToPackFile, $maxSize)
{
    $xml = New-Object -TypeName XML
    $xml.Load($pathToPackFile)
    
    foreach ($item in (Select-XML -Xml $xml -XPath "//Pack/BankFolder/Bank//Wave"))
    {
        if($item.Node.HasAttribute("builtSize"))
        {
            $size = $item.Node.GetAttribute("builtSize")
            if($size -gt $maxSize)
            {
                $nodeInfo = Get-XPath $item.Node
                Write-Host "${nodeInfo}: $size"
            }
        }
    }
}

function Get-XPath($n) {
  if ( $n.GetType().Name -ne 'XmlDocument' ) {
    "{0}/{1}" -f (Get-XPath $n.ParentNode), $n.Name
  }
}




Write-Host
Write-Host �Welcome to list big built waves script�
Write-Host �--------------------------------------�


$sourcePath = "X:\gta5\audio\dev_ng\build\XBOXONE\BuiltWaves\DLC_AGENT_PACK_FILE.xml"
$maxSize = 862890

Write-Host �List all waves in $sourcePath that are bigger than $maxSize�

ListWaves $sourcePath $maxSize



