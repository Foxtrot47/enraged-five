
function ReplaceAttributeContent($sourcePath)
{
    $xml = New-Object -TypeName XML
    $xml.Load($sourcePath)
    $writeXml = $false
    
    foreach ($item in (Select-XML -Xml $xml -XPath "//Objects/CarRecordingAudioSettings/@$XmlAttribute"))
    {
        $AttributeContent = $item.Node.Value
        if($item.Node.Value.Contains($findInXml))
        {
            $NewAttributeContent = $item.Node.Value.Replace($findInXml, $replaceInXmlWith)
            Write-Host "${sourcePath}: $($item.Node.Value) -> $NewAttributeContent"
            $item.Node.Value = $NewAttributeContent
            $writeXml = $true
        }
    }
    
    if($writeXml)
    {
        $targetPath = $sourcePath.Replace($sourceFolder, $targetFolder)
        New-Item -Path "$targetPath" -ItemType file -Force
        Write-Host "Write new xml to $targetPath"
        $xml.Save($targetPath)
    }
}




function GetFiles($sourcePath) 
{ 
    foreach ($item in Get-ChildItem($sourcePath))
    {
        if (Test-Path $item.FullName -PathType Container) 
        {
            GetFiles $item.FullName
        } 
        else 
        { 
            ReplaceAttributeContent($item.FullName)
        }
    } 
}




Write-Host
Write-Host �Welcome to XML content rename script�
Write-Host �------------------------------------�
$sourceFolder = "GAMEOBJECTS"
$targetFolder = "Result GAMEOBJECTS"

$XmlElement = "CarRecordingAudioSettings"
$XmlAttribute = "name"

$findInXml = "CR_"
$replaceInXmlWith = ""

$sourceFolder = $(Get-Item($sourceFolder)).FullName
$targetFolder = $(Get-Item($targetFolder)).FullName

Write-Host �Replace all occurences of $findInXml in the `"$XmlAttribute`" attribute of XML element`"$XmlElement`" in folder `"$sourceFolder`" and copy the results to `"$targetFolder`"�
Write-Host

GetFiles($sourceFolder, $targetFolder)



