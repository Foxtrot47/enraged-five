"""
Script that generates a list of contexts that require gestures.

RAVE uses the generated list of contexts to highlight the waves
which require gesture animations.

Requires XLRD python lib to be installed:

http://pypi.python.org/pypi/xlrd

Install XLRD:

 - Extract .tar.gz file locally
 - Navigate to directory in command window
 - Run: python setup.py install
"""

import os
import xlrd


INPUT_PATH = r'x:\gta5\docs\Audio\GTA5 Dialogue Category Explanations.xlsx'
OUTPUT_DIR = r'X:\gta5\tools\bin\audio\rave\data'
OUTPUT_FILENAME = r'gesture_context_list.csv'


def generate():
    
    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)
        
    file_path = os.path.join(OUTPUT_DIR, OUTPUT_FILENAME)
    
    book = xlrd.open_workbook(INPUT_PATH)
    sheet = book.sheet_by_name('Explanations')
    
    fout = open(file_path, 'w')
    
    for i in xrange(1, sheet.nrows):
        row = sheet.row_values(i)
        context_name = row[0]
        requires_gesture_flag = row[20]
        
        if requires_gesture_flag.strip().lower() in ('y', 'yes', '?'):
            fout.write("%s\n" % context_name)
    
    fout.close()


if __name__ == '__main__':
    generate()