@echo off
rem scriptfile setup
set package=%RUBYLIB%\util\ragebuilder\package.rbs
set cdimagecat=%RUBYLIB%\util\ragebuilder\concatImages.rbs
set copyfiles=%RUBYLIB%\util\ragebuilder\copyfiles.rbs

rem ragebuilder version to use
set ragebuilder=%RS_TOOLSROOT%\bin\ragebuilder_0378.exe

rem source & destination folder
set build=%RS_BUILDBRANCH%

REM work out current branch set by installer (eg dev)
for /f "tokens=*" %%a in (
'xmlstarlet sel -t -v "//local/branches/@default" %RS_TOOLSROOT%\etc\local.xml'
) do (
set branch=%%a
)

echo branch: %branch%
set audio=%RS_PROJROOT%\audio\%branch%\staging
echo audio: %audio%
