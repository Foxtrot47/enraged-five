import os
import hashlib
import shutil
import sys

PERFORCE_EXE_NAME = r'p4'

PLATFORMS = (
    'PS4',
    'XBOXONE',
    'PC',
    'PS5',
    'XBOXSERIES'
    )
ALL_PLATFORMS = 'all_platforms'

RPF_SOURCE_PATH = r'\\rockstar.t2.corp\Network\RSGEDI\Audio\Audio\RPF\dev_ng\{platform}\Audio'
RPF_MD5_PATH = r'\\rockstar.t2.corp\Network\RSGEDI\Audio\Audio\RPF\dev_ng\{platform}\md5_list.csv'
AUDIO_BUILD_PATH = r'x:\gta5\build\dev_ng\{platform}\audio'

platform = None
md5_list = {}


def run():
    global platform

    if not parse_args():
        return False

    # Validate and set platform if required
    if platform is None:
        platform = ALL_PLATFORMS
    else:
        if platform.upper() not in PLATFORMS:            
            print "Invalid platform specified: %s" % platform
            return False

    print ""

    # Get latest RPFs for all platforms
    if platform == ALL_PLATFORMS:
        for curr_platform in ALL_PLATFORMS:
            if not get_latest_rpfs(curr_platform):
                return False

    # Get latest RPFs for specified platform
    else:
        if not get_latest_rpfs(platform):
            return False

    return True


def parse_args():
    global platform

    print "Parsing input args..."

    args = sys.argv
    counter = 1

    for arg in args[1:]:
        if arg == '-platform':
            platform = args[counter+1]
            counter += 1
        elif arg[0] != '-':
            continue
        else:
            print "Invalid arg: %s" % arg
            help()
            return False

        counter += 1

    return True


def get_latest_rpfs(platform):
    print "\nGetting latest RPFs for platform: %s" % platform

    if not load_md5(platform):
        print "Failed to load MD5 file for platform: %s" % platform
        return False

    source_path = RPF_SOURCE_PATH.replace("{platform}", platform)
    if not os.path.exists(source_path):
        print "Destination path does not exist: %s" % source_path
        return False

    dest_path = AUDIO_BUILD_PATH.replace("{platform}", platform)
    if not os.path.exists(dest_path):
        print "Destination path does not exist: %s" % dest_path
        return False

    print "\nGetting latest audio RPFs..."
    sync_args = '-q %s\...' % dest_path
    if not run_p4_cmd("sync", sync_args):
        return False

    print "\nChecking out audio RPFs..."
    edit_args = '%s\...' % dest_path
    if not run_p4_cmd("edit", edit_args):
        return False

    print "\nCopying RPFs from %s to %s" % (source_path, dest_path)
    if not copy_rpfs(source_path, dest_path):
        return False

    print "\nReverting unchanged in default change list..."
    revert_args = '-a -c default %s\...' % dest_path
    if not run_p4_cmd("revert", revert_args):
        return False

    return True


def load_md5(platform):
    global md5_list

    md5_list = {}
    path = RPF_MD5_PATH.replace("{platform}", platform)
    print "Loading MD5 values: %s" % path

    if not os.path.exists(path):
        return False

    md5_file = open(path)
    for line in md5_file:
        if line:
            file, md5 = line.split(',')
            md5_list[file.lower()] = md5.replace('\n', '')

    return True


def copy_rpfs(source_path, dest_path):
    global md5_list

    if not os.path.exists(source_path):
        print "Source path does not exist: %s" % source_path
        return False

    if not os.path.exists(dest_path):
        print "Creating RPF destination path: %s" % dest_path
        os.makedirs(dest_path)

    files = [name for name in os.listdir(source_path) if not os.path.isdir(os.path.join(source_path, name))]
    dirs = [name for name in os.listdir(source_path) if os.path.isdir(os.path.join(source_path, name))]
    
    for file in files:

        file_src_path = os.path.join(source_path, file)
        file_dest_path = os.path.join(dest_path, file)

        dest_file_exists = os.path.exists(file_dest_path)

        # Check if file needs to be copied or if we already have latest version (using MD5)
        if dest_file_exists and file_src_path.lower() in md5_list:

            src_md5 = md5_list[file_src_path.lower()]
            dest_md5 = md5_for_file(file_dest_path)

            if dest_md5 == src_md5:
                # Don't need to copy file
                print "Already got latest version of %s" % file_dest_path
                continue

        print "Copying %s to %s" % (file_src_path, file_dest_path)
        shutil.copy2(file_src_path, file_dest_path)

        # Mark for add if new file
        if not dest_file_exists:
            if not run_p4_cmd("add", file_dest_path):
                return False

    # Process any sub folders
    for sub_dir in dirs:
        sub_dir_source_path = os.path.join(source_path, sub_dir)
        sub_dir_dest_path = os.path.join(dest_path, sub_dir)
        if not copy_rpfs(sub_dir_source_path, sub_dir_dest_path):
            return False

    return True


def run_p4_cmd(action, args):
    cmd = '%s %s %s' % (PERFORCE_EXE_NAME, action, args)
    print "Running Perforce cmd: %s" % cmd

    if os.system(cmd) != 0:
        print "Failed to successfully run P4 command: %s" % cmd
        return False

    return True


def md5_for_file(path, block_size=2**20):
    print "Generating MD5 for file: %s"  % path

    file = open(path)
    md5 = hashlib.md5()
    while True:
        data = file.read(block_size)
        if not data:
            break
        md5.update(data)
    return md5.hexdigest()


if __name__ == '__main__':
    run()
