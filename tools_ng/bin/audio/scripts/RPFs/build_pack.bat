@echo off
set sourcePath=%1
set destRPF=%2
set packName=%3
echo %sourcePath% to %destRPF%

p4 edit %destRPF%
%ragebuilder% %package% -pack %destRPF% -rootpath %sourcePath% -extignore .rpf -nameheapshift=1

p4 add %destRPF%