function getLatest(item)
  os.execute("%RS_TOOLSROOT%\\bin\\audio\\scripts\\reaper\\rea_p4.bat p4 sync " ..  reaper.GetMediaSourceFileName( item, "" ))
  reaper.CF_SetMediaSourceOnline(item, true)
end


local item_count = reaper.CountSelectedMediaItems(0)

  for i=0, item_count - 1 do

      local selectedItem = reaper.GetSelectedMediaItem( 0, i )
      
      local pcmSource = reaper.GetMediaItemTake_Source( reaper.GetMediaItemTake(selectedItem, 0) )
      
      
      getLatest(pcmSource)
  end


