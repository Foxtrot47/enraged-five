
 
--------------------------------------------------------
-- DEBUG
-- -----

-- Console Message
function Msg(g)
  reaper.ShowConsoleMsg(tostring(g).."\n")
end



---------------------------------------------------------
-- NUMBER
-- ------

-- Format Seconds
function Format(number)
  str = reaper.format_timestr_pos(number, "", 5)
  return str
end


function MapIntensityToScriptInt(intensity)



    --  CLUB_MUSIC_INTENSITY_LOW = 0,
     -- CLUB_MUSIC_INTENSITY_MEDIUM = 1,
     -- CLUB_MUSIC_INTENSITY_HIGH = 2,
     -- CLUB_MUSIC_INTENSITY_HIGH_HANDS = 3,


  ret = -1
  if intensity == "IDLE" then
      ret = 0
  elseif intensity == "MED_INTENSITY" then
      ret = 1
  elseif intensity == "HIGH_INTENSITY" then
      ret = 2
  elseif intensity == "HIGH_INTENSITY_HANDS_UP" then
      ret = 3
  elseif intensity == "TRANCE" then
      ret = 4
  end
  
  return ret
end

--------------------------------------------------------
-- PATHS
-- -----

-- Get Path from file name
function GetPath(str,sep)
    return str:match("(.*"..sep..")")
end


-- Check if project has been saved
function IsProjectSaved()
  -- OS BASED SEPARATOR
  if reaper.GetOS() == "Win32" or reaper.GetOS() == "Win64" then
    -- user_folder = buf --"C:\\Users\\[username]" -- need to be test
    separator = "\\"
  else
    -- user_folder = "/USERS/[username]" -- Mac OS. Not tested on Linux.
    separator = "/"
  end

  --path = reaper.GetProjectPath("") -- E:\Bureau\Projet\Audio
  --path = path:gsub("Audio", "") -- E:\Bureau\Projet\

  retval, project_path_name = reaper.EnumProjects(-1, "")
  if project_path_name ~= "" then
    
    dir = GetPath(project_path_name, separator)
    --msg(name)
    name = string.sub(project_path_name, string.len(dir) + 1)
    name = string.sub(name, 1, -5)

    name = name:gsub(dir, "")
    --file = dir .. "HTML" .. separator .. name .. " - Items List.html"
    file = dir .. name .. " - Regions List.txt"
    --msg(name)
    project_saved = true
    return project_saved
  else
    display = reaper.ShowMessageBox("You need to save the project to execute this script.", "File Export", 1)

    if display == 1 then

      reaper.Main_OnCommand(40022, 0) -- SAVE AS PROJECT

      return IsProjectSaved()

    end
  end
end


------------------------------------------------------------------
-- COLOR FUNCTIONS
-- ---------------


--------------------------------------------------------------
-- CREATE FILE
-- -----------

-- New HTML Line
function export(f, variable)
  f:write(variable)
  f:write("\n")
end

-- Create File
function create(f)
  -- CREATE THE FILE
  io.output(file)
  
  lastIntensity = 0
 
  for  itemIdx = 0,reaper.CountMediaItems(0)-1,1 do
  
  mediaItem = reaper.GetMediaItem(0, itemIdx)
  source = reaper.GetMediaItemTake_Source(reaper.GetActiveTake(mediaItem))
  sourceFile = reaper.GetMediaSourceFileName(source, "")
  
  itemStartTime = reaper.GetMediaItemInfo_Value(mediaItem, "D_POSITION")
  itemEndTime = itemStartTime + reaper.GetMediaItemInfo_Value(mediaItem, "D_LENGTH")
  
  export(f, "\n" .. sourceFile .. "\n")
  
    times = ""
    names = ""
    firstMarker = true
    i=0
    repeat
      iRetval, bIsrgnOut, iPosOut, iRgnendOut, sNameOut, iMarkrgnindexnumberOut, iColorOur = reaper.EnumProjectMarkers3(0,i)
      if iRetval >= 1 then
        if bIsrgnOut == true and iPosOut >= itemStartTime and iRgnendOut <= itemEndTime then
          start_time = math.floor(0.5 + (iPosOut - itemStartTime) * 1000) -- reaper.format_timestr(iPosOut, "")
          
          scriptId = MapIntensityToScriptInt(sNameOut)
          
          if scriptId ~= -1 then
        
        
            if firstMarker == false then
              times = times .. ", "
              names = names .. ", "
            else
             -- write a 0 marker if we don't have one
              if start_time ~= 0 then
                times = "0,"
                names = lastIntensity .. ","
              end
            end
          
            firstMarker = false
          
            times = times .. start_time  --.. end_time .. " " .. sNameOut
            names = names .. scriptId
          end
          
          lastIntensity = scriptId
          
        end -- skip -1 intensity (ie marker didn't match known list)
        i = i+1
      end
    until iRetval == 0
  
  -- Script require a final marker to tell them the length of the track
  times = times .. ", " ..  math.floor(0.5 + (itemEndTime-itemStartTime) * 1000)
  names = names .. ", " .. lastIntensity

export(f, "\t\t\t\"timems\": [" .. times .. "],")
export(f, "\t\t\t\"intype\": [" .. names .. "]")

end

  -- CLOSE FILE
  f:close() -- never forget to close the file

  Msg("Regions Lists exported to:\n" .. file .."\n")

end



----------------------------------------------------------------
-- MAIN FUNCTION
-- -------------

function main() -- local (i, j, item, take, track)

  local f = io.open(file, "w")

  -- HTML FOLDER EXIST
  if f ~= nil then
    create(f)
  end

end -- ENDFUNCTION MAIN


----------------------------------------------------------------------
-- RUN
-- ---

-- Check if there is selected Items
retval, count_markers, count_regions = reaper.CountProjectMarkers(0)

if count_regions > 0 then

  project_saved = IsProjectSaved() -- See if Project has been save and determine file paths
  if project_saved then
    main() -- Execute your main function
  end

else
  Msg("No regions in the project.")
end -- ENDIF Item in project

