<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="MaxColour">200</xsl:variable>

<xsl:template match="WaveSlots">

<xsl:variable name="MaxSlotSize">
	<xsl:for-each select="Slot[not(Name = 'TEST')]">				
		<xsl:sort select="Size/@value" data-type="number" order="descending"/>
		<xsl:if test="position()=1"><xsl:value-of select="Size/@value"/></xsl:if>
	</xsl:for-each>
</xsl:variable>
<xsl:variable name="MinSlotSize">
	<xsl:for-each select="Slot[not(Name = 'TEST')]">				
		<xsl:sort select="Size/@value" data-type="number" order="ascending"/>
		<xsl:if test="position()=1"><xsl:value-of select="Size/@value"/></xsl:if>
	</xsl:for-each>
</xsl:variable>
<html>
<head>
	<script type="text/javascript" src="X:/gta5/tools/bin/audio/rave/xsl/stats.js"></script>
	<link rel="stylesheet" type="text/css" href="X:/gta5/tools/bin/audio/rave/xsl/waveslotviewer.css" />
</head>
<body>
<h1>Total WaveSlot Size: <xsl:value-of select="format-number(sum(Slot[not(Name = 'TEST')]/Size/@value), '###,###,###')"/></h1>
	<table class="WaveSlots">
		<tr>
			<th>Slot Name</th>
			<th>Slot Size</th>
			<th>Slot Contents</th>
		</tr>
		<xsl:apply-templates select="Slot[not(Name = 'TEST')]">
				<xsl:sort select="Name" data-type="text" order="ascending"/>
				<xsl:with-param name="MinS" select="$MinSlotSize"/>
				<xsl:with-param name="MaxS" select="$MaxSlotSize"/>
		</xsl:apply-templates>
	</table>
	<h1>Test Waveslot</h1>
	<table class="WaveSlots">
		<tr>
			<th>Slot Name</th>
			<th>Slot Size</th>
			<th>Slot Contents</th>
		</tr>
		<xsl:apply-templates select="Slot[(Name = 'TEST')]">
				<xsl:sort select="Name" data-type="text" order="ascending"/>
				<xsl:with-param name="MinS" select="$MinSlotSize"/>
				<xsl:with-param name="MaxS" select="$MaxSlotSize"/>
		</xsl:apply-templates>
	</table>
</body>
</html>
</xsl:template>

<xsl:template match="Slot">
	<xsl:param name="MinS"/>
	<xsl:param name="MaxS"/>
	<xsl:variable name="nodeID" select="Name"/>
	<xsl:variable name="sizeOf" select="Size/@value"/>
	<tr class="{LoadType}" 
		onClick="toggleChildVisibility('{$nodeID}');" 
		onMouseOver="this.style.cursor='pointer'"
		style="color:RGB({floor( ( ( Size/@value - $MinS ) div ( $MaxS - $MinS ) )*255 )},0,0);">
		<td><xsl:value-of select="Name"/></td>
		<td><xsl:value-of select="format-number(Size/@value,'###,###,###')"/></td>
		<td>
			<xsl:if test="StaticBank"><xsl:value-of select="StaticBank"/></xsl:if>
			<xsl:if test="not(StaticBank)">-</xsl:if>
		</td>
		</tr>
		<tr id="{$nodeID}" class="{LoadType}" style="display:none;">
			<td style="background-color:white;"></td>
			<td class="slotInfo" colspan="2">
				<table width="100%">
					<tr><td>Size</td><td><xsl:value-of select="format-number(Size/@value,'###,###,###')"/></td></tr>
					<tr><td>MaxHeaderSize</td><td><xsl:value-of select="format-number(MaxHeaderSize/@value,'###,###,###')"/></td></tr>
					<tr><td>Type</td><td><xsl:value-of select="LoadType"/></td></tr>
					<xsl:if test="StaticBank"><tr><td>Location</td><td><xsl:value-of select="StaticBank"/></td></tr></xsl:if>
				</table>
			</td>
		</tr>
</xsl:template>

</xsl:stylesheet>
