@echo off
REM Creating a Newline variable (the two blank lines are required!)
set NLM=^


set NL=^^^%NLM%%NLM%^%NLM%%NLM%


echo %NL%Connecting to Perforce and syncing latest metadata definitions file...
p4 sync //depot/gta5/audio/dev/assets/Objects/Definitions/EventDefinitions.xml > NUL

echo %NL%Syncing latest metadata XML files...
p4 sync //depot/gta5/audio/dev/assets/Objects/Core/AI/EVENTS/... > NUL

echo %NL%Opening event files for edit...
p4 edit //depot/gta5/script/dev/shared/include/native/event_enums.sch > NUL

echo %NL%Generating event files
X:\gta5\tools\bin\rave\XSL\XSLTransform.exe X:\gta5\audio\dev\assets\Objects\Core\AI\EVENTS\DECISION_MAKERS\DECISION_MAKER_DEFAULT.xml X:\gta5\tools\bin\rave\XSL\EventDecisionMakerScriptHashGen.xslt X:\gta5\script\dev\shared\include\native\event_enums.sch

echo %NL%Reverting unchanged event files...
p4 revert -a //depot/gta5/script/dev/shared/include/native/event_enums.sch > NUL

echo %NL%Adding event files...
p4 add x:/gta5/script/dev/shared/include/native/event_enums.sch > NUL

echo %NL%Done!%NL%

PAUSE