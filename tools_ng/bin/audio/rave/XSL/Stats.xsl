<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="LCL">abcdefghijklmnopqrstuvwxyz</xsl:variable>
<xsl:variable name="UCL">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
<xsl:variable name="PlatformColour">green</xsl:variable>
<xsl:variable name="MaxColour">200</xsl:variable>
<xsl:param name="platform">UNSPECIFIED</xsl:param>
<xsl:param name="pack"></xsl:param>

<xsl:template match="BuiltWaves">
<html>
<head>
<title>
	Waves Report - <xsl:value-of select="$platform"/>
</title>

<script type="text/javascript">
	function toggleChildVisibility(node){
				var x=document.getElementById(node); 
				if(x.style.display!='none') x.style.display = 'none';
				else x.style.display = 'block';
	}
</script>

<link rel="stylesheet" type="text/css" href="file:///X:/gta5/tools/bin/audio/rave/xsl/stats.css" />
</head>

<body>
<h1>
Platform - 
<font color="cornflowerblue">
	<xsl:value-of select="$platform"/>
</font>
</h1>
<h2>
Total Size : 
<xsl:call-template name="formatNumberSize">
	<xsl:with-param name="inputNumber" select="sum(descendant::Bank/@builtSize)"/>
</xsl:call-template>
</h2>
<ul class="BuiltWaves">
<xsl:choose>
	<xsl:when test="$pack=''">
		<xsl:apply-templates select="Pack">
			<xsl:sort select="@name" data-type="text" order="ascending"/>
			<xsl:with-param name="largestChild">
				<xsl:for-each select="child::*[not(name()='Tag')]">
					<xsl:sort select="sum(descendant::Bank/@builtSize)" data-type="number" order="descending"/>
					<xsl:if test="position()=1"><xsl:value-of select="sum(descendant::Bank/@builtSize)"/></xsl:if>
				</xsl:for-each>
			</xsl:with-param>
			<xsl:with-param name="smallestChild">
				<xsl:for-each select="child::*[not(name()='Tag')]">
					<xsl:sort select="sum(descendant::Bank/@builtSize)" data-type="number" order="ascending"/>
					<xsl:if test="position()=1"><xsl:value-of select="sum(descendant::Bank/@builtSize)"/></xsl:if>
				</xsl:for-each>
			</xsl:with-param>
		</xsl:apply-templates>
	</xsl:when>
	<xsl:otherwise>
		<xsl:apply-templates select="Pack[@name=$pack]">
			<xsl:sort select="@name" data-type="text" order="ascending"/>
			<xsl:with-param name="largestChild">
				<xsl:for-each select="child::*[not(name()='Tag')]">
					<xsl:sort select="sum(descendant::Bank/@builtSize)" data-type="number" order="descending"/>
					<xsl:if test="position()=1"><xsl:value-of select="sum(descendant::Bank/@builtSize)"/></xsl:if>
				</xsl:for-each>
			</xsl:with-param>
			<xsl:with-param name="smallestChild">
				<xsl:for-each select="child::*[not(name()='Tag')]">
					<xsl:sort select="sum(descendant::Bank/@builtSize)" data-type="number" order="ascending"/>
					<xsl:if test="position()=1"><xsl:value-of select="sum(descendant::Bank/@builtSize)"/></xsl:if>
				</xsl:for-each>
			</xsl:with-param>
		</xsl:apply-templates>
	</xsl:otherwise>
</xsl:choose>
<!--<table>
<xsl:for-each select="//Wave[ancestor::Pack[contains(@name,'SCRIPTED_SPEECH')]]">
<xsl:sort select="@builtSize" data-type="number" order="descending"/>
	<tr><td><xsl:call-template name="formatNumberSize"><xsl:with-param name="inputNumber" select="@builtSize"/></xsl:call-template></td><td><xsl:for-each select="ancestor-or-self::*">/<xsl:value-of select="@name"/></xsl:for-each></td><td><xsl:for-each select="ancestor-or-self::*">/<xsl:value-of select="@timeStamp"/></xsl:for-each></td></tr>
</xsl:for-each>
</table>-->
</ul>
</body>
</html>
</xsl:template>

<xsl:template match="Pack | BankFolder | Bank | WaveFolder">
	<xsl:param name="largestChild"/>
	<xsl:param name="smallestChild"/>
	<xsl:variable name="childWaves"><xsl:value-of select="count(descendant::Wave)"/></xsl:variable>
	<xsl:variable name="nodeID">/<xsl:for-each select="ancestor-or-self::*"><xsl:value-of select="@name"/></xsl:for-each></xsl:variable>
	<xsl:variable name="sizeOf"><xsl:value-of select="sum(descendant::Wave/Chunk/@size)"/></xsl:variable>
	<xsl:variable name="sizeOfBuiltSum"><xsl:value-of select="sum(descendant::Bank/@builtSize)"/></xsl:variable>
	<xsl:variable name="sizeOfRawSum"><xsl:value-of select="sum(descendant::Wave/@sourceSize)"/></xsl:variable>
	<xsl:variable name="filePath"><xsl:if test="name()='Pack'">file:///X:/gta5/audio/dev/runtime/<xsl:if test="$platform='XBOX 360'">xbox360</xsl:if><xsl:if test="$platform='PS3'">ps3</xsl:if><xsl:if test="$platform='PC'">pc</xsl:if>/SFX/<xsl:value-of select="@name"/></xsl:if></xsl:variable>
	<xsl:variable name="wavFilePath"><xsl:if test="name()='Bank'">file:///x:/gta5/audio/dev/assets/Waves<xsl:for-each select="ancestor-or-self::*">/<xsl:value-of select="@name"/></xsl:for-each></xsl:if></xsl:variable>
	<xsl:variable name="textColour">
		<xsl:choose>
			<xsl:when test="not($smallestChild = $largestChild)">
				<xsl:choose>
					<xsl:when test="name()='WaveFolder' or name()='Bank'">
						<xsl:value-of select="floor((($sizeOf - $smallestChild) div ($largestChild - $smallestChild))*$MaxColour)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="floor((($sizeOfBuiltSum - $smallestChild) div ($largestChild - $smallestChild))*$MaxColour)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="sizesString">
		<xsl:choose>
			<xsl:when test="name()='Bank'">
				<xsl:value-of select="@builtSize"/>
			</xsl:when>
			<xsl:when test="name()='Wave'">
				<xsl:value-of select="$sizeOf"/>
			</xsl:when>
			<xsl:when test="name()='WaveFolder'">
				<xsl:value-of select="$sizeOf"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$sizeOfBuiltSum"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="iconName">
		<xsl:choose>
			<xsl:when test="name()='Pack'">file:///X:/gta5/tools/bin/audio/rave/XSL/ReportImages/RavePackIcon.png</xsl:when>
			<xsl:when test="name()='Bank'">file:///X:/gta5/tools/bin/audio/rave/XSL/ReportImages/RaveBankIcon.png</xsl:when>
			<xsl:when test="contains(name(),'Folder')">file:///X:/gta5/tools/bin/audio/rave/XSL/ReportImages/RaveFolderIcon.png</xsl:when>
			<xsl:otherwise>file:///X:/gta5/tools/bin/audio/rave/XSL/ReportImages/RaveTagIcon.png</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<li style="color:RGB({$textColour},0,0);" class="{name()}">
		<b 		
		onClick="toggleChildVisibility('{$nodeID}');" 
		onMouseOver="this.style.cursor='pointer'">
		<img src="{$iconName}"/> : <xsl:value-of select="@name"/></b> : <xsl:call-template name="formatNumberSize"><xsl:with-param name="inputNumber" select="$sizesString"/></xsl:call-template> (<xsl:value-of select="$childWaves"/> waves
			<xsl:if test="name()='Bank'">, <xsl:call-template name="formatNumberSize"><xsl:with-param name="inputNumber" select="$sizeOf"/></xsl:call-template></xsl:if>, <span class="ss"><xsl:call-template name="formatNumberSize"><xsl:with-param name="inputNumber" select="$sizeOfRawSum"/></xsl:call-template></span>)
			<xsl:if test="name()='Pack'"> : Largest Bank <xsl:call-template name="largestBank"/> : <a class='weeLink' href="{$filePath}">files</a></xsl:if>
			<xsl:if test="name()='BankFolder'"> : Largest Bank <xsl:call-template name="largestBank"/></xsl:if>
			<xsl:if test="name()='Bank'"> : <a class='weeLink' href="{$wavFilePath}">files</a></xsl:if>

		<ul id="{$nodeID}" style="display:none;">
			<xsl:for-each select="@*[not(name()='name')]">
				<xsl:sort select="name()"/>
				<li>
					<xsl:value-of select="name()"/> : 	
						<xsl:choose>
							<xsl:when test="format-number(., '###,###,###') = 'NaN'">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="formatNumberSize"><xsl:with-param name="inputNumber" select="."/></xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
				</li>
			</xsl:for-each>
			
			<xsl:if test="count(descendant::Wave) = 0">
				-
			</xsl:if>

			<xsl:apply-templates select="Tag"/>
	
			<xsl:apply-templates select="BankFolder | Pack">
				<xsl:sort select="@name" order="ascending" data-type="text"/>
				<xsl:with-param name="largestChild">
					<xsl:for-each select="child::*[not(name()='Tag')]">
						<xsl:sort select="sum(descendant-or-self::Bank/@builtSize)" data-type="number" order="descending"/>
						<xsl:if test="position()=1"><xsl:value-of select="sum(descendant-or-self::Bank/@builtSize)"/></xsl:if>
					</xsl:for-each>
				</xsl:with-param>
				<xsl:with-param name="smallestChild">
					<xsl:for-each select="child::*[not(name()='Tag')]">
						<xsl:sort select="sum(descendant-or-self::Bank/@builtSize)" data-type="number" order="ascending"/>
						<xsl:if test="position()=1"><xsl:value-of select="sum(descendant-or-self::Bank/@builtSize)"/></xsl:if>
					</xsl:for-each>
				</xsl:with-param>
			</xsl:apply-templates>
	
			<xsl:apply-templates select="Bank | WaveFolder">
				<xsl:sort select="@name" order="ascending" data-type="text"/>
				<xsl:with-param name="largestChild">
					<xsl:for-each select="child::*">
						<xsl:sort select="sum(descendant::Wave/Chunk/@size)" data-type="number" order="descending"/>
						<xsl:if test="position()=1"><xsl:value-of select="sum(descendant::Wave/Chunk/@size)"/></xsl:if>
					</xsl:for-each>
				</xsl:with-param>
				<xsl:with-param name="smallestChild">
					<xsl:for-each select="child::*[not(name()='Tag')]">
						<xsl:sort select="sum(descendant::Wave/Chunk/@size)" data-type="number" order="ascending"/>
						<xsl:if test="position()=1"><xsl:value-of select="sum(descendant::Wave/Chunk/@size)"/></xsl:if>
					</xsl:for-each>
				</xsl:with-param>
			</xsl:apply-templates>
	
			<xsl:apply-templates select="Wave">
				<xsl:sort select="@name" order="ascending" data-type="text"/>
				<xsl:with-param name="largestChild">
					<xsl:for-each select="child::Wave">
						<xsl:sort select="sum(Chunk/@size)" data-type="number" order="descending"/>
						<xsl:if test="position()=1"><xsl:value-of select="sum(Chunk/@size)"/></xsl:if>
					</xsl:for-each>
				</xsl:with-param>
				<xsl:with-param name="smallestChild">
					<xsl:for-each select="child::Wave">
						<xsl:sort select="sum(Chunk/@size)" data-type="number" order="ascending"/>
						<xsl:if test="position()=1"><xsl:value-of select="sum(Chunk/@size)"/></xsl:if>
					</xsl:for-each>
				</xsl:with-param>
			</xsl:apply-templates>
		</ul>
	</li>
</xsl:template>

<xsl:template match="Wave">
	<xsl:param name="largestChild"/>
	<xsl:param name="smallestChild"/>
	<xsl:variable name="sizeOf"><xsl:value-of select="sum(Chunk/@size)"/></xsl:variable>
	<xsl:variable name="nodeID"><xsl:for-each select="ancestor-or-self::*"><xsl:value-of select="@name"/></xsl:for-each></xsl:variable>
	<xsl:variable name="filePath">file:///x:/gta5/audio/dev/assets/Waves<xsl:for-each select="ancestor::*">/<xsl:value-of select="@name"/></xsl:for-each></xsl:variable>
	<xsl:variable name="textColour">
		<xsl:choose>
			<xsl:when test="not($smallestChild = $largestChild)">
				<xsl:value-of select="floor((($sizeOf - $smallestChild) div ($largestChild - $smallestChild))*$MaxColour)"/>
			</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<li 
		class="{name()}"
		style="color:RGB({$textColour},0,0);">
				<span 
					onClick="toggleChildVisibility('{$nodeID}');"
					onMouseOver="this.style.cursor='pointer'">
					<b><img src="file:///X:/gta5/tools/bin/audio/rave/XSL/ReportImages/RaveWaveIcon.png"/> : <xsl:value-of select="@name"/></b> : <xsl:call-template name="formatNumberSize"><xsl:with-param name="inputNumber" select="sum(Chunk/@size)"/></xsl:call-template></span> <span class="ss"><xsl:call-template name="formatNumberSize"><xsl:with-param name="inputNumber" select="@sourceSize"/></xsl:call-template></span> : <a class='weeLink' href="{$filePath}/{@name}">play</a>
		</li>
					
	<ul id="{$nodeID}" style="display:none;">

		<xsl:apply-templates select="Tag" />
	
		<xsl:for-each select="@*">
			<xsl:sort select="name()"/>
			<li>
				<xsl:value-of select="name()"/> : 	
					<xsl:choose>
					<xsl:when test="format-number(., '###,###,###') = 'NaN' or name()='nameHash' or contains(name(),'SampleRate')"><xsl:value-of select="."/></xsl:when>
					<xsl:otherwise>							
						<xsl:call-template name="formatNumberSize"><xsl:with-param name="inputNumber" select="."/></xsl:call-template>
					</xsl:otherwise>
					</xsl:choose>
			</li>
		</xsl:for-each>
		<xsl:for-each select="child::Chunk">
			<xsl:sort select="@name"/>
			<li>
				<xsl:value-of select="name()"/> : <xsl:value-of select="@name"/> : <xsl:call-template name="formatNumberSize"><xsl:with-param name="inputNumber" select="@size"/></xsl:call-template>
			</li>
		</xsl:for-each>
	</ul>

</xsl:template>

<xsl:template match="Tag">
	<li style="color:cornflowerblue">
				<img src="file:///X:/gta5/tools/bin/audio/rave/XSL/ReportImages/RaveTagIcon.png"/> : <xsl:value-of select="@name"/> : 	<xsl:value-of select="@value"/>
	</li>
</xsl:template>

<xsl:template name="formatNumberSize">
	<xsl:param name="inputNumber"/>
	<xsl:choose>
		<xsl:when test="$inputNumber &gt; 1073741824">
			<xsl:value-of select="format-number($inputNumber div 1073741824,'###,###,###.0')"/>GB
		</xsl:when>
		<xsl:when test="$inputNumber &gt; 1048576">
			<xsl:value-of select="format-number($inputNumber div 1048576,'###,###,###.0')"/>MB
		</xsl:when>
		<xsl:when test="$inputNumber &gt; 1024">
			<xsl:value-of select="format-number($inputNumber div 1024,'###,###,###.0')"/>kB
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$inputNumber"/>B
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="largestBank">
	<xsl:for-each select="descendant::Bank">
			<xsl:sort select="@builtSize" data-type="number" order="descending"/>
			<xsl:if test="position()=1"><xsl:value-of select="@name"/> (<xsl:call-template name="formatNumberSize"><xsl:with-param name="inputNumber" select="@builtSize"/></xsl:call-template>)</xsl:if>
	</xsl:for-each>
</xsl:template>



</xsl:stylesheet>