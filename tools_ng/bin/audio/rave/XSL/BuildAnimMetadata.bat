@echo off
REM Creating a Newline variable (the two blank lines are required!)
set NLM=^


set NL=^^^%NLM%%NLM%^%NLM%%NLM%

echo %NL%Deleting local *_ORIG.xml files...
del /S X:\gta5\audio\dev\assets\Objects\CORE\AI\ANIM\*_ORIG.xml > NUL

echo %NL%Connecting to Perforce and syncing latest metadata definitions file...
p4 sync //depot/gta5/audio/dev/assets/Objects/Definitions/AnimDefinitions.xml > NUL

echo %NL%Syncing latest metadata XML files...
p4 sync //depot/gta5/audio/dev/assets/Objects/Core/AI/ANIM/... > NUL

echo %NL%Deleting existing local compiled metadata files...
del /F x:\gta5\audio\dev\runtime\XBOX360\CONFIG\anim.dat* > NUL
del /F x:\gta5\audio\dev\runtime\PS3\CONFIG\anim.dat* > NUL
del /F x:\gta5\build\dev\xbox360\audio\config\anim.dat* > NUL
del /F x:\gta5\build\dev\ps3\audio\config\anim.dat* > NUL

echo %NL%Syncing latest compiled metadata files prior to edit...
p4 sync -f //depot/gta5/audio/dev/runtime/XBOX360/CONFIG/anim.dat* > NUL
p4 sync -f //depot/gta5/audio/dev/runtime/PS3/CONFIG/anim.dat* > NUL
p4 sync -f //depot/gta5/build/dev/xbox360/audio/config/anim.dat* > NUL
p4 sync -f //depot/gta5/build/dev/ps3/audio/config/anim.dat* > NUL

echo %NL%Opening existing compiled metadata files for edit...
p4 edit //depot/gta5/audio/dev/runtime/XBOX360/CONFIG/anim.dat* > NUL
p4 edit //depot/gta5/audio/dev/runtime/PS3/CONFIG/anim.dat* > NUL
p4 edit //depot/gta5/build/dev/xbox360/audio/config/anim.dat* > NUL
p4 edit //depot/gta5/build/dev/ps3/audio/config/anim.dat* > NUL

echo %NL%Compiling Xbox360 metadata...
X:\gta5\tools\bin\metadatacompiler\mc.exe -project gta5\audio\dev\projectSettings.xml -platform XENON -metadata Anim -workingpath X:\
echo %NL%%NL%Compiling PS3 metadata...
X:\gta5\tools\bin\metadatacompiler\mc.exe -project gta5\audio\dev\projectSettings.xml -platform PS3 -metadata Anim -workingpath X:\ 

echo %NL%%NL%Copying compiled metadata to the build folder...
copy x:\gta5\audio\dev\runtime\XBOX360\CONFIG\anim.dat* x:\gta5\build\dev\xbox360\audio\config > NUL
copy x:\gta5\audio\dev\runtime\PS3\CONFIG\anim.dat* x:\gta5\build\dev\ps3\audio\config > NUL

echo %NL%Reverting unchanged compiled metadata files...
p4 revert -a //depot/gta5/audio/dev/runtime/XBOX360/CONFIG/anim.dat* > NUL
p4 revert -a //depot/gta5/audio/dev/runtime/PS3/CONFIG/anim.dat* > NUL
p4 revert -a //depot/gta5/build/dev/xbox360/audio/config/anim.dat* > NUL
p4 revert -a //depot/gta5/build/dev/ps3/audio/config/anim.dat* > NUL

echo %NL%Adding any new compiled metadata files...
p4 add x:/gta5/audio/dev/runtime/XBOX360/CONFIG/anim.dat* > NUL
p4 add x:/gta5/audio/dev/runtime/PS3/CONFIG/anim.dat* > NUL
p4 add x:/gta5/build/dev/xbox360/audio/config/anim.dat* > NUL
p4 add x:/gta5/build/dev/ps3/audio/config/anim.dat* > NUL

echo %NL%Done!%NL%

PAUSE