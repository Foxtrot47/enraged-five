<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="LCL">abcdefghijklmnopqrstuvwxyz</xsl:variable>
<xsl:variable name="UCL">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
<xsl:variable name="PlatformColour">green</xsl:variable>
<xsl:variable name="MaxColour">200</xsl:variable>
<xsl:param name="platform">UNSPECIFIED</xsl:param>

<xsl:template match="BuiltWaves">
<html>
<head>
<title>
	Script Bank Sizes - <xsl:value-of select="$platform"/>
</title>

<script type="text/javascript">
	function toggleChildVisibility(node){
				var x=document.getElementById(node); 
				if(x.style.display!='none') x.style.display = 'none';
				else x.style.display = 'block';
	}
</script>

<link rel="stylesheet" type="text/css" href="file:///X:/gta5/tools/bin/audio/rave/xsl/stats.css" />
</head>

<body>
<h1>
	Script Bank Sizes - <font color="cornflowerblue"><xsl:value-of select="$platform"/></font>
</h1>
<table class="BankSizeTable">
<tr><th>Bank Size</th><th>Bank Name</th><th>Bank Path</th><th>TimeStamp</th></tr>
<xsl:for-each select="//Bank[ancestor::Pack[@name='SCRIPT']]">
	<xsl:sort select="@builtSize" data-type="number" order="descending"/>
	<xsl:variable name="bgColor"><xsl:call-template name="getBGColor"><xsl:with-param name="inputNumber" select="@builtSize"/></xsl:call-template></xsl:variable>
	<xsl:variable name="borderColor"><xsl:call-template name="getBorderColor"><xsl:with-param name="inputNumber" select="@builtSize"/></xsl:call-template></xsl:variable>
	<tr class="Size{$borderColor}" style="background-color:RGB{$bgColor};"><td><xsl:call-template name="formatNumberSize"><xsl:with-param name="inputNumber" select="@builtSize"/></xsl:call-template></td><td><xsl:value-of select="@name"/></td><td><xsl:for-each select="ancestor::*">/<xsl:value-of select="@name"/></xsl:for-each></td><td><xsl:value-of select="@timeStamp"/></td></tr>
</xsl:for-each>
</table>
</body>
</html>
</xsl:template>

<xsl:template name="formatNumberSize">
	<xsl:param name="inputNumber"/>
	<xsl:choose>
		<xsl:when test="$inputNumber &gt; 1073741824">
			<xsl:value-of select="format-number($inputNumber div 1073741824,'###,###,###.0')"/>GB
		</xsl:when>
		<xsl:when test="$inputNumber &gt; 1048576">
			<xsl:value-of select="format-number($inputNumber div 1048576,'###,###,###.0')"/>MB
		</xsl:when>
		<xsl:when test="$inputNumber &gt; 1024">
			<xsl:value-of select="format-number($inputNumber div 1024,'###,###,###.0')"/>kB
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$inputNumber"/>B
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="getBGColor">
	<xsl:param name="inputNumber"/>
	<xsl:choose>
		<xsl:when test="$inputNumber &gt; 110000">(255,200,200)</xsl:when>
		<xsl:when test="$inputNumber &gt; 90000">(255,<xsl:value-of select="round((($inputNumber - 90000) div 20000)* - 55) + 255"/>,<xsl:value-of select="round((($inputNumber - 90000) div 20000)* - 55) + 255"/>)</xsl:when>
		<xsl:otherwise>(255,255,255)</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="getBorderColor">
	<xsl:param name="inputNumber"/>
	<xsl:choose>
		<xsl:when test="$inputNumber &gt; 110000">tooBig</xsl:when>
		<xsl:when test="$inputNumber &gt; 100000">quiteBig</xsl:when>
		<xsl:otherwise>normal</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>