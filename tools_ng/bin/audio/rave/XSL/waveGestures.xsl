<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  
 <xsl:output method="html"/>

  <xsl:template match="//Pack">
    <xsl:if test="@name ='SPEECH'">
      <html>
        <head>
          <title>
            Gestured Waves
          </title>
        </head>
        <body>
          <center>
            <h2>Non-scripted speech</h2>
            <table border="1">
              <th>Wave Name</th>
              <th>Gestures Used</th>
              <xsl:for-each select="descendant::BankFolder">
                <xsl:if test="attribute::name !='SCRIPTED_SPEECH'">
                  <xsl:for-each select="child::Bank">
                    <xsl:sort select="attribute::name"/>
                    <xsl:for-each select="descendant::Wave">
                      <xsl:sort select="attribute::name"/>
                      <xsl:if test="count(child::Marker)!=0">
                        <tr>
                          <td>
                            <xsl:value-of select="../@name"/>/
                            <xsl:value-of select="attribute::name"/>
                          </td>
                          <td>
                            <xsl:for-each select="child::Marker">
                              <xsl:value-of select="attribute::name"/>
                              <br></br>
                            </xsl:for-each>
                          </td>
                        </tr>
                      </xsl:if>
                    </xsl:for-each>
                  </xsl:for-each>
                </xsl:if>
              </xsl:for-each>
            </table>
          </center>
        </body>
      </html>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>