<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:ms="urn:schemas-microsoft-com:xslt"
 xmlns:hash ="urn:rockstargames.com:xslt"
  >

  <xsl:output method="text"/>

  <ms:script language="C#" implements-prefix="hash">
    <![CDATA[public uint ComputeHash(string str)
        {
            if (str == null || str.Equals(""))
            {
                return 0;
            }

            //Convert .Net Wave name to a system string and then to a Rage hash code.
            byte[] utf8 = System.Text.Encoding.UTF8.GetBytes(str);


            // This is the one-at-a-time hash from this page:
            // http://burtleburtle.net/bob/hash/doobs.html
            // (borrowed from /soft/swat/src/swcore/string2key.cpp)
            uint key = 0;

            for (int i = 0; i < str.Length; i++)
            {
                byte character = utf8[i];
                if (character >= 'A' && character <= 'Z')
                    character += 'a' - 'A';
                else if (character == '\\')
                    character = (byte)'/';

                key += character;
                key += (key << 10);	//lint !e701
                key ^= (key >> 6);	//lint !e702
            }
            key += (key << 3);	//lint !e701
            key ^= (key >> 11);	//lint !e702
            key += (key << 15);	//lint !e701

            // The original swat code did several tests at this point to make
            // sure that the resulting value was representable as a valid
            // floating-point number (not a negative zero, or a NaN or Inf)
            // and also reserved a nonzero value to indicate a bad key.
            // We don't do this here for generality but the caller could
            // obviously add such checks again in higher-level code.
            return key;}]]>
  </ms:script>

<xsl:template match="/Objects">
#ifndef ANIM_GROUP_IDS_H
#define ANIM_GROUP_IDS_H

namespace rage
{
    class crAnimation;
}

enum AnimGroupId
{
    ANIM_GROUP_ID_INVALID = 0,
    <xsl:for-each select="child::AnimGroup">
    <xsl:value-of select="@name"/> = <xsl:value-of select="hash:ComputeHash(@name)"/>u,
    </xsl:for-each>
};

#endif // ANIM_GROUP_IDS_H
</xsl:template>


</xsl:stylesheet> 
