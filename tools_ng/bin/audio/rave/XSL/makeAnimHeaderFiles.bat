@echo off
REM Creating a Newline variable (the two blank lines are required!)
set NLM=^


set NL=^^^%NLM%%NLM%^%NLM%%NLM%


echo %NL%Connecting to Perforce and syncing latest metadata definitions file...
p4 sync //depot/gta5/audio/dev/assets/Objects/Definitions/AnimDefinitions.xml > NUL

echo %NL%Syncing latest metadata XML files...
p4 sync //depot/gta5/audio/dev/assets/Objects/Core/AI/ANIM/... > NUL

echo %NL%Opening anim header files for edit...
p4 edit //depot/gta5/src/dev/game/animation/AnimIds.h > NUL
p4 edit //depot/gta5/src/dev/game/animation/AnimGroupIds.h > NUL

echo %NL%Generating anim header files from ANIM.xml%NL%
X:\gta5\tools\bin\rave\XSL\XSLTransform.exe X:\gta5\audio\dev\assets\Objects\Core\AI\ANIM\ANIM.xml X:\gta5\tools\bin\rave\XSL\AIAnimIDs.xslt X:\gta5\src\dev\game\animation\AnimIds.h
X:\gta5\tools\bin\rave\XSL\XSLTransform.exe X:\gta5\audio\dev\assets\Objects\Core\AI\ANIM\ANIM.xml X:\gta5\tools\bin\rave\XSL\AIAnimGroupIDs.xslt X:\gta5\src\dev\game\animation\AnimGroupIds.h

echo %NL%Reverting unchanged anim header files...
p4 revert -a //depot/gta5/src/dev/game/animation/AnimIds.h > NUL
p4 revert -a //depot/gta5/src/dev/game/animation/AnimGroupIds.h > NUL

echo %NL%Adding anim header files...
p4 add x:/gta5/src/dev/game/animation/AnimIds.h > NUL
p4 add x:/gta5/src/dev/game/animation/AnimGroupIds.h > NUL

echo %NL%Done!%NL%

PAUSE