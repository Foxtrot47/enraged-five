<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
 <xsl:output method="text" encoding="ASCII" indent="no"/>
  <xsl:template match="//BankFolder">
    <xsl:if test="@name ='SCRIPTED_SPEECH'">
      <xsl:for-each select="descendant::Wave">
        <xsl:value-of select="attribute::name"/>,<xsl:choose>
          <xsl:when test="Marker[@category='G_S']">1,</xsl:when>
          <xsl:otherwise>0,</xsl:otherwise>
        </xsl:choose><xsl:choose>
          <xsl:when test="Marker[@category='G_L']">1,</xsl:when>
          <xsl:otherwise>0,</xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>