
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" />

  <xsl:template match="//BankFolder">
    <xsl:if test="@name ='SCRIPTED_SPEECH'">
       <html>
        <head>
          <title>
            Gestured Scripted Speech Waves
          </title>
        </head>
        <body>
          <center>
            <h2>Scripted Speech</h2>
              <xsl:for-each select="child::Bank">
                <xsl:sort select="attribute::name"/>
                <xsl:if test="count(descendant::Marker)!=0">
              <h3>
                Mission:
                <xsl:value-of select="attribute::name"/>
              </h3>
                  <table border="1">
                    <th>Voice</th>
                    <th>G_S</th>
                    <th>G_L</th>
                    <th>F_S</th>
                    <th>F_L</th>
                    <xsl:for-each select="descendant::Wave">
                      <xsl:if test="count(descendant::Marker)!=0">
                        <tr>
                          <td>
                            <xsl:value-of select="../@name"/>/
                            <xsl:value-of select="attribute::name"/>
                          </td>
                          <td>
                            <xsl:for-each select="child::Marker">
                              <xsl:if test="attribute::category='G_S'">
                                <xsl:value-of select="attribute::name"/>
                                <br></br>
                              </xsl:if>                                                           
                            </xsl:for-each>
                         </td>
                          <td>
                            <xsl:for-each select="child::Marker">
                              <xsl:if test="attribute::category='G_L'">
                                <xsl:value-of select="attribute::name"/>
                                <br></br>
                              </xsl:if>                                                      
                            </xsl:for-each>
                            <xsl:text>&#160;</xsl:text>
                          </td>
                          <td>
                            <xsl:for-each select="child::Marker">
                              <xsl:if test="attribute::category='F_S'">
                                <xsl:value-of select="attribute::name"/>
                                <br></br>
                              </xsl:if>
                            </xsl:for-each>
                            <xsl:text>&#160;</xsl:text>
                          </td>
                          <td>
                            <xsl:for-each select="child::Marker">
                              <xsl:if test="attribute::category='F_L'">
                                <xsl:value-of select="attribute::name"/>
                                <br></br>
                              </xsl:if>
                            </xsl:for-each>
                            <xsl:text>&#160;</xsl:text>
                          </td>
                        </tr>
                      </xsl:if>
                    </xsl:for-each>
                  </table>
              <br></br>
                </xsl:if>
            </xsl:for-each>
            <br></br>
          </center>
        </body>
      </html>
      </xsl:if>
  </xsl:template>

</xsl:stylesheet>