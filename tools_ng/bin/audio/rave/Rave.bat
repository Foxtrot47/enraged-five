@echo off
setlocal
call setenv.bat

cd /d %RS_TOOLSROOT%\bin\audio\rave
if not exist rave.exe (
echo.
echo.
echo Error: RAVE not found - please check the above configuration and see that
echo.       you have the tools and audio dirs mapped correctly in Perforce.
echo.       You should have this dir: %RS_TOOLSROOT%\bin\audio\rave
echo.
pause
exit 1
)

tasklist | find /I "rave.exe"
if %ERRORLEVEL% EQU 0 (
cls
ECHO *****************************
ECHO *** Closing Rave  ***
ECHO *****************************
taskkill /f /PID %1
)

echo.
ECHO GRABBING LATEST AUDIO TOOLS
pushd ..
p4 sync %cd%\...
popd
echo %cd%
start "" "rave.exe" -frombatch
