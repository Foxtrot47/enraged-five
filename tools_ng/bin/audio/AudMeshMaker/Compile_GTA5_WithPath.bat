echo off

call setenv.bat

REM ---------------------------------------------------------------------------------------------------
REM Set up some paths, etc
REM %1 (param 1) is the output directory (eg. X:/gta/exported_audmeshes)
REM %2 (param 2) is the audmeshes cmd file (eg. X:\GTA5\tools\bin\audgen\AudMeshMaker\Meshes_NY.lst
REM ---------------------------------------------------------------------------------------------------

set TOOLS_PATH="%RS_TOOLSBIN%\audio\AudMeshMaker"
set BUILD_DIR=%1
set CMD_FILE=%2
set GAMEPATH="%RS_BUILDBRANCH%"
set CMD_FILE_VEHICLES="%BUILD_DIR%\vehicles.lst"
set MAKE_RPF="%RS_TOOLSBIN%\audio\AudMeshMaker\MakeRPF.bat"
set AUDPARAMS="%RS_TOOLSBIN%\audio\AudMeshMaker\AudioParams.txt"
set AREAS="%RS_TOOLSBIN%\audgen\AudMeshMaker\GTA5_ResAreas.xml"

REM -----------------------------------------------------------------------------
REM Call the Compile_Project batch file
REM -----------------------------------------------------------------------------

call %RS_TOOLSBIN%\audio\AudMeshMaker\Compile_Project %TOOLS_PATH% %BUILD_DIR% %GAMEPATH% %CMD_FILE% %CMD_FILE_VEHICLES% %MAKE_RPF% %AUDPARAMS% %AREAS%


popd
