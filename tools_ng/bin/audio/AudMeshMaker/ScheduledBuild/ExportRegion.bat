echo off

call %1%\tools\bin\setenv.bat

REM -----------------------------------------------------------------------------------------------------------
REM ExportRegion.bat
REM Runs the specified game to export audmeshes in the given worldcoordinates range
REM
REM %1 = The project's Root Dir (eg. "X:\GTA\")
REM %2 = Tool executable
REM %3 = The folder to export audmeshes into
REM %4 = The ".lst" file detailing the audmesh sections & addition params for compilation
REM %5 = Additional arguments for the game (eg. the name of a specific level to load?)
REM %6 = The world coorinates as comma separated integer values ( for example: "-1000,340,-240,1045" )
REM -----------------------------------------------------------------------------------------------------------

echo -----------------------------------------------------------------------
echo 1 Root Dir = %1
echo 2 Tool executable = %2
echo 3 Audmesh export folder = %3
echo 4 Audmeshes listing file = %4
echo 5 Level-specific game param = %5
echo 6 Export region = %6
echo -----------------------------------------------------------------------

REM -------------------------------------------------------------------
REM delete old ???????_cache.dat files or we may miss some collision
REM -------------------------------------------------------------------

del %1\build\dev\pc\data\*_cache.dat

REM -------------
REM run the tool
REM -------------



pushd %1\build\dev

set COMMON_ARGS=-nopopups -audgencfg=%RS_TOOLSBIN%\audio\AudMeshMaker\audgenconfig.xml

%2 %~5 -exportRegion %6 -exportFolder "%3" -scheduledcompile -audgencmdfile=%4 %COMMON_ARGS%

popd



