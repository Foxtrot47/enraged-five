
REM ----------------------------------------------------------------------------
REM !!!!NB: I don't think we'll need this for audmeshes - it was put in for Jimmy navmeshes
REM but maybe we'll need it in the future. CW
REM
REM
REM We do not need to open a connection to Perforce, since we
REM have environment variables set up to specify the client, etc
REM
REM The parameter %1% is the full path to the NavmeshIndexMapping.dat file
REM The parameter %2% is the full path to the exported NavmeshIndexMapping.dat file
REM
REM The parameter %3% is the full path to the AudmeshIndexMapping.dat file
REM The parameter %4% is the full path to the exported AudmeshIndexMapping.dat file
REM
REM ----------------------------------------------------------------------------

echo p4host = %p4host%
echo p4port = %p4port%
echo p4user = %p4user%

REM --------------------------------------------
REM Ensure that we have the file on the client
REM --------------------------------------------

p4 sync %1
p4 sync %3

REM --------------------
REM Check the file out
REM --------------------

p4 edit %1
p4 edit %3

REM --------------------------------------
REM Copy the new file over the depot file
REM --------------------------------------

copy %2 %1
copy %4 %3

REM ------------------------
REM Check the file back in
REM ------------------------

p4 submit -d "Automated navmeshIndexMapping.dat check-in" %1
p4 submit -d "Automated audmeshIndexMapping.dat check-in" %3

REM p4 logout

REM pause
