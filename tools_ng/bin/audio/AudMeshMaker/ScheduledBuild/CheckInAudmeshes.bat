

REM -------------------------------------------------------------------------------------------------------------
REM We do not need to open a connection to Perforce, since we
REM have environment variables set up to specify the client, etc
REM
REM The parameter %1% is the full path to the audmeshes.zip file, w/o trailing slash, and minus the filename
REM The parameter %2% is the full path to the export folder where the new audmeshes.zip files are, again
REM  this path is w/o trailing backslash
REM
REM ----------------------------------------------------------------------------------

echo p4host = %p4host%
echo p4port = %p4port%
echo p4user = %p4user%

REM --------------------------------------------
REM Ensure that we have the file on the client
REM --------------------------------------------

p4 sync %1\audmeshes0.zip
p4 sync %1\audmeshes1.zip
p4 sync %1\audmeshes2.zip
p4 sync %1\audmeshes3.zip
p4 sync %1\audmeshes4.zip
p4 sync %1\audmeshes5.zip
p4 sync %1\audmeshes6.zip
p4 sync %1\audmeshes7.zip
p4 sync %1\audmeshes8.zip
p4 sync %1\audmeshes9.zip

REM --------------------
REM Check the file out
REM --------------------

p4 edit %1\audmeshes0.zip %1\audmeshes1.zip %1\audmeshes2.zip %1\audmeshes3.zip %1\audmeshes4.zip %1\audmeshes5.zip %1\audmeshes6.zip %1\audmeshes7.zip %1\audmeshes8.zip %1\audmeshes9.zip

REM --------------------------------------
REM Copy the new file over the depot file
REM --------------------------------------

copy %2\audmeshes0.zip %1\audmeshes0.zip
copy %2\audmeshes1.zip %1\audmeshes1.zip
copy %2\audmeshes2.zip %1\audmeshes2.zip
copy %2\audmeshes3.zip %1\audmeshes3.zip
copy %2\audmeshes4.zip %1\audmeshes4.zip
copy %2\audmeshes5.zip %1\audmeshes5.zip
copy %2\audmeshes6.zip %1\audmeshes6.zip
copy %2\audmeshes7.zip %1\audmeshes7.zip
copy %2\audmeshes8.zip %1\audmeshes8.zip
copy %2\audmeshes9.zip %1\audmeshes9.zip

REM ------------------------
REM Check the file back in
REM ------------------------

p4 submit -d "Automated audmeshes check-in" "%1\audmeshes*.zip"

