

REM ----------------------------------------------------------------------------
REM We do not need to open a connection to Perforce, since we
REM have environment variables set up to specify the client, etc
REM
REM The parameter %1% is the full path to the NavmeshIndexMapping.dat file
REM The parameter %2% is the full path to the exported NavmeshIndexMapping.dat file
REM ----------------------------------------------------------------------------

echo p4host = %p4host%

REM --------------------------------------------
REM Ensure that we have the file on the client
REM --------------------------------------------

p4 sync %1

REM --------------------
REM Check the file out
REM --------------------

p4 edit %1

REM --------------------------------------
REM Copy the new file over the depot file
REM --------------------------------------

copy %2 %1

REM ------------------------
REM Check the file back in
REM ------------------------

p4 submit -d "Automated NavmeshIndexMapping.dat check-in" %1


