echo off

call setenv.bat

REM -----------------------------------------------------------------------------
REM Set up some paths, etc
REM -----------------------------------------------------------------------------

set TOOLS_PATH=%1
set BUILD_DIR=%2
set GAMEPATH=%3
set CMD_FILE=%4
set CMD_FILE_VEHICLES=%5
set MAKE_RPF=%6
set COMMON_ARGS=-nopopups -audgencfg=%RS_TOOLSBIN%\audio\AudMeshMaker\audgenconfig.xml -noaudio -scheduledcompile
set AREAS=%8

echo TOOLS_PATH = %TOOLS_PATH%
echo BUILD_DIR = %BUILD_DIR%
echo GAMEPATH = %GAMEPATH%
echo CMD_FILE = %CMD_FILE%
echo CMD_FILE_VEHICLES = %CMD_FILE_VEHICLES%
echo MAKE_RPF = %MAKE_RPF%
echo AUDPARAMS = %AUDPARAMS%
echo COMMON_ARGS = %COMMON_ARGS%
echo AREAS = %AREAS%

path %RS_TOOLSBIN%;%RS_TOOLSBIN%\audgen\AudMeshMaker;%path%

pushd %GAMEPATH%

REM -----------------------------------------------------------------------------
REM Compile the audio meshes
REM ----------------------------------------------------------------------------- 



call x:\gta5\tools\bin\audio\AudMeshMaker\audmeshgenerator.exe -audiomeshgen -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE% -audioMeshParams %AUDPARAMS% %COMMON_ARGS%
call x:\gta5\tools\bin\audio\AudMeshMaker\audmeshgenerator.exe -audiomeshstitch -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE% -audioMeshParams %AUDPARAMS% %COMMON_ARGS%

REM ----------------------------------------------------
REM Create the mapping files for audmeshes :::::we're gonna try without mapping first and just have 1-1 mapping
REM ----------------------------------------------------



REM call AudMeshGenerator.exe -createmappingfile iam AudmeshIndexMapping.dat -inputpath %BUILD_DIR% -gamepath %GAMEPATH% %COMMON_ARGS%

REM --------------------------------------------------------------------------------
REM Before copying files into subfolders, make sure we don't already have a bunch
REM of old files in the subfolders. That would be OK most of the time, but if files
REM were added or removed, the distribution between folders may be different and some
REM files can end up in multiple directories (RPFs) and cause problems in the game.
REM --------------------------------------------------------------------------------

del /Q %BUILD_DIR%\0
del /Q %BUILD_DIR%\1
del /Q %BUILD_DIR%\2
del /Q %BUILD_DIR%\3
del /Q %BUILD_DIR%\4
del /Q %BUILD_DIR%\5
del /Q %BUILD_DIR%\6
del /Q %BUILD_DIR%\7
del /Q %BUILD_DIR%\8
del /Q %BUILD_DIR%\9

REM --------------------------------------------------------------------------
REM Copy "inv" files into 10 separate subfolders to try and combat the awful
REM time-complexity of dealing with RPF files in the build pipeline.
REM We will make 10 RPFs "audmeshes0.rpf" to "audmeshes9.rpf"
REM --------------------------------------------------------------------------

call x:\gta5\tools\bin\audio\AudMeshMaker\audmeshgenerator.exe -copyintosubfolders -inputpath %BUILD_DIR% %COMMON_ARGS%


REM --------------------------------------------------------------------------
REM Not entirely sure if %MAKE_RPF% will overwrite existing RPF files, or add
REM to them. So, we delete them here to be sure.
REM --------------------------------------------------------------------------

del /Q/S %BUILD_DIR%\audmeshes*.zip

REM -----------------------------------------------------------------------------
REM Add all the ".inv" files to the audmeshes.rpf
REM -----------------------------------------------------------------------------

call %MAKE_RPF% %BUILD_DIR%

REM ----------------------------------------------------------------------------------
REM Notify that the compilation is complete by creating the file "compile_complete.txt"
REM ----------------------------------------------------------------------------------

echo finished >> %BUILD_DIR%/compile_complete.txt

