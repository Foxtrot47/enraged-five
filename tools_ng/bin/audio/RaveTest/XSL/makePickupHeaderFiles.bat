@echo off
REM Creating a Newline variable (the two blank lines are required!)
set NLM=^


set NL=^^^%NLM%%NLM%^%NLM%%NLM%


echo %NL%Connecting to Perforce and syncing latest metadata definitions file...
p4 sync //depot/gta5/audio/dev/assets/Objects/Definitions/PickupDefinitions.xml > NUL

echo %NL%Syncing latest metadata XML files...
p4 sync //depot/gta5/audio/dev/assets/Objects/Core/AI/PICKUPS/... > NUL

echo %NL%Opening pickup header files for edit...
p4 edit //depot/gta5/src/dev/game/pickups/data/PickupIds.h > NUL
p4 edit //depot/gta5/src/dev/game/pickups/data/PickupIds.cpp > NUL
p4 edit //depot/gta5/script/dev/shared/include/native/pickup_enums.sch > NUL

echo %NL%Generating pickup header files from PICKUPS.xml%NL%
X:\gta5\tools\bin\rave\XSL\XSLTransform.exe X:\gta5\audio\dev\assets\Objects\Core\AI\PICKUPS\PICKUPS.xml X:\gta5\tools\bin\rave\XSL\PickupCodeIDsH.xslt X:\gta5\src\dev\game\pickups\data\PickupIds.h
X:\gta5\tools\bin\rave\XSL\XSLTransform.exe X:\gta5\audio\dev\assets\Objects\Core\AI\PICKUPS\PICKUPS.xml X:\gta5\tools\bin\rave\XSL\PickupCodeIDsCPP.xslt X:\gta5\src\dev\game\pickups\data\PickupIds.cpp
X:\gta5\tools\bin\rave\XSL\XSLTransform.exe X:\gta5\audio\dev\assets\Objects\Core\AI\PICKUPS\PICKUPS.xml X:\gta5\tools\bin\rave\XSL\PickupScriptIDs.xslt X:\gta5\script\dev\shared\include\native\pickup_enums.sch

echo %NL%Reverting unchanged pickup header files...
p4 revert -a //depot/gta5/src/dev/game/pickups/data/PickupIds.h > NUL
p4 revert -a //depot/gta5/src/dev/game/pickups/data/PickupIds.cpp > NUL
p4 revert -a //depot/gta5/script/dev/shared/include/native/pickup_enums.sch > NUL

echo %NL%Adding pickup header files...
p4 add x:/gta5/src/dev/game/pickups/data/PickupIds.h > NUL
p4 add x:/gta5/src/dev/game/pickups/data/PickupIds.cpp > NUL
p4 add x:/gta5/script/dev/shared/include/native/pickup_enums.sch > NUL

echo %NL%Done!%NL%

PAUSE