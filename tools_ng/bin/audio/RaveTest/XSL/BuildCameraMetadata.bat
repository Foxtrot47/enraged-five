@echo off
REM Creating a Newline variable (the two blank lines are required!)
set NLM=^


set NL=^^^%NLM%%NLM%^%NLM%%NLM%

echo %NL%Deleting local *_ORIG.xml files...
del /S X:\jimmy\audio\dev\assets\Objects\CORE\AI\Cameras\*_ORIG.xml > NUL

echo %NL%Connecting to Perforce and syncing latest metadata definitions file...
p4 sync //depot/jimmy/audio/dev/assets/Objects/Definitions/CameraDefinitions.xml > NUL

echo %NL%Syncing latest metadata XML files...
p4 sync //depot/jimmy/audio/dev/assets/Objects/Core/AI/CAMERAS/... > NUL

echo %NL%Deleting existing local compiled metadata files...
del /F x:\jimmy\build\dev\xbox360\data\metadata\cameras.dat* > NUL
del /F x:\jimmy\build\dev\ps3\data\metadata\cameras.dat* > NUL

echo %NL%Syncing latest compiled metadata files prior to edit...
p4 sync -f //depot/jimmy/build/dev/xbox360/data/metadata/cameras.dat* > NUL
p4 sync -f //depot/jimmy/build/dev/ps3/data/metadata/cameras.dat* > NUL

echo %NL%Opening existing compiled metadata files for edit...
p4 edit //depot/jimmy/build/dev/xbox360/data/metadata/cameras.dat* > NUL
p4 edit //depot/jimmy/build/dev/ps3/data/metadata/cameras.dat* > NUL

echo %NL%Compiling Xbox360 metadata...
X:\jimmy\bin\mc\mc.exe -project jimmy\audio\dev\projectSettings.xml -platform XENON -metadata Cameras -workingpath X:\
echo %NL%%NL%Compiling PS3 metadata...
X:\jimmy\bin\mc\mc.exe -project jimmy\audio\dev\projectSettings.xml -platform PS3 -metadata Cameras -workingpath X:\ 

echo %NL%Reverting unchanged compiled metadata files...
p4 revert -a //depot/jimmy/build/dev/xbox360/data/metadata/cameras.dat* > NUL
p4 revert -a //depot/jimmy/build/dev/ps3/data/metadata/cameras.dat* > NUL

echo %NL%Adding any new compiled metadata files...
p4 add x:/jimmy/build/dev/xbox360/data/metadata/cameras.dat* > NUL
p4 add x:/jimmy/build/dev/ps3/data/metadata/cameras.dat* > NUL

echo %NL%Done!%NL%

PAUSE