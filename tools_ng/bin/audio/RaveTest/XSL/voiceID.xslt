<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" indent="no"/>

  <xsl:template match="/">
    <xsl:apply-templates select="//Tag[@name='voice']">
      <xsl:sort select="@value"/>
    </xsl:apply-templates>
  </xsl:template>
  
  <xsl:template match="Tag">
    <xsl:if test="not(@value = preceding::Tag/@value)">
      <xsl:value-of select="@value"/>
      <xsl:text>
      </xsl:text>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>


