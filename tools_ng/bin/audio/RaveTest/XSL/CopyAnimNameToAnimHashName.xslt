<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="copy.xslt"/>
  <xsl:output media-type="text/xml" method="xml" version="1.0" encoding="UTF-8" indent="yes" />
  <xsl:output omit-xml-declaration="no"/>
  <xsl:template match="AnimNameList">
    <xsl:copy>
     <xsl:for-each select="child::AnimName">
        <xsl:value-of select="@nAnimNameHash"/> = <xsl:value-of select="@sAnimName"/>
      </xsl:for-each>
    </xsl:copy>
  </xsl:template>    
</xsl:stylesheet>