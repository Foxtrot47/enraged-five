<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:hash ="urn:rockstargames.com:xslt">

  <xsl:output method="text"/>

  <ms:script language="C#" implements-prefix="hash">
    <![CDATA[
        public int ComputeHash(string str)
        {
            if (str == null || str.Equals(""))
            {
                return 0;
            }

            //Convert .Net Wave name to a system string and then to a Rage hash code.
            byte[] utf8 = System.Text.Encoding.UTF8.GetBytes(str);


            // This is the one-at-a-time hash from this page:
            // http://burtleburtle.net/bob/hash/doobs.html
            // (borrowed from /soft/swat/src/swcore/string2key.cpp)
            uint key = 0;

            for (int i = 0; i < str.Length; i++)
            {
                byte character = utf8[i];
                if (character >= 'A' && character <= 'Z')
                    character += 'a' - 'A';
                else if (character == '\\')
                    character = (byte)'/';

                key += character;
                key += (key << 10);	//lint !e701
                key ^= (key >> 6);	//lint !e702
            }
            key += (key << 3);	//lint !e701
            key ^= (key >> 11);	//lint !e702
            key += (key << 15);	//lint !e701

            // The original swat code did several tests at this point to make
            // sure that the resulting value was representable as a valid
            // floating-point number (not a negative zero, or a NaN or Inf)
            // and also reserved a nonzero value to indicate a bad key.
            // We don't do this here for generality but the caller could
            // obviously add such checks again in higher-level code.
            return (int)key;
        }
    ]]>
  </ms:script>
  
<xsl:template match="/Objects">ENUM DECISION_MAKER_TYPE<xsl:for-each select="document('x:\gta5\audio\dev\assets\Objects\CORE\AI\EVENTS\DECISION_MAKERS\DECISION_MAKER_DEFAULT.xml')/Objects/DecisionMaker">
    DECISION_MAKER_<xsl:value-of select="@name"/> = <xsl:value-of select="hash:ComputeHash(@name)"/>,</xsl:for-each>
  <xsl:for-each select="document('x:\gta5\audio\dev\assets\Objects\CORE\AI\EVENTS\DECISION_MAKERS\DECISION_MAKER_EMPTY.xml')/Objects/DecisionMaker">
    DECISION_MAKER_<xsl:value-of select="@name"/> = <xsl:value-of select="hash:ComputeHash(@name)"/>,</xsl:for-each>
  <xsl:for-each select="document('x:\gta5\audio\dev\assets\Objects\CORE\AI\EVENTS\DECISION_MAKERS\DECISION_MAKER_PLAYER.xml')/Objects/DecisionMaker">
    DECISION_MAKER_<xsl:value-of select="@name"/> = <xsl:value-of select="hash:ComputeHash(@name)"/>,</xsl:for-each>
  <xsl:for-each select="document('x:\gta5\audio\dev\assets\Objects\CORE\AI\EVENTS\DECISION_MAKERS\DECISION_MAKER_COP.xml')/Objects/DecisionMaker">
    DECISION_MAKER_<xsl:value-of select="@name"/> = <xsl:value-of select="hash:ComputeHash(@name)"/>,
  </xsl:for-each>
    DECISION_MAKER_INVALID = 0
ENDENUM

ENUM EVENT_RESPONSE_TASK
    <xsl:for-each select="document('x:\gta5\audio\dev\assets\Objects\CORE\AI\EVENTS\DEFAULT_RESPONSE_TASKS\RESPONSE_TASK.xml')/Objects">
      <xsl:for-each select="node()">
        <xsl:if test="@name">
          <xsl:value-of select="@name"/> = <xsl:value-of select="hash:ComputeHash(@name)"/>,
    </xsl:if>
    </xsl:for-each>
    </xsl:for-each>RESPONSE_TASK_INVALID = 0
ENDENUM

ENUM EVENT_NAMES
    <xsl:for-each select="document('x:\gta5\audio\dev\assets\Objects\Definitions\EventDefinitions.xml')/TypeDefinitions/Enum">
      <xsl:if test="@name='EventType'">
        <xsl:for-each select="Value">
          <xsl:choose>
            <xsl:when test="position()=last()">
              <xsl:value-of select="."/> = <xsl:number value="position()-2" format="1"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="."/> = <xsl:number value="position()-2" format="1"/>,
    </xsl:otherwise>
    </xsl:choose>
    </xsl:for-each>
    </xsl:if>
  </xsl:for-each>
ENDENUM
</xsl:template>
</xsl:stylesheet>