<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
 <xsl:output method="text" indent="no"/>
  <xsl:template match="//BankFolder">
    <xsl:if test="@name ='PLACEHOLDER_SPEECH'">
      <xsl:for-each select="descendant::Wave">
        <xsl:value-of select="../@name"/>,<xsl:value-of select="./@name"/>,<xsl:value-of select="./@rawSize"/>,<xsl:value-of select="./@builtSize"/>,
      </xsl:for-each>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>