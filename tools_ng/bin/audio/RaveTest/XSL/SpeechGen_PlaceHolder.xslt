<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" indent="no"/>

<xsl:template match="/">
  <xsl:for-each select="descendant::Pack">
    <xsl:if test="@name='SCRIPTED_SPEECH'">
      <xsl:for-each select="descendant::WaveFolder">
        <xsl:variable name="voice" select="descendant::Tag[@name='voice']/@value "/>
        <xsl:for-each select="descendant::Wave">
          <xsl:value-of select="@name"/><xsl:text> </xsl:text><xsl:value-of select="$voice"/><xsl:text> </xsl:text><xsl:value-of select="@operation"/>,</xsl:for-each>
      </xsl:for-each>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet> 
