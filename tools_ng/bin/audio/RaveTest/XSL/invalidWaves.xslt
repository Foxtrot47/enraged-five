<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" indent="no"/>

  <xsl:strip-space elements="*"/>

  <xsl:param name="number" select="0"/>
  
  <xsl:template match="/">
    <xsl:for-each select="//Pack">
      <xsl:variable name="PackNode" select="."/>
    <xsl:if test="@name = 'SCRIPTED_SPEECH' or @name = 'SPEECH'">
      <xsl:text>
</xsl:text>
      <xsl:value-of select="@name"/>
      <xsl:text>
----------------
</xsl:text>
      <!--<xsl:call-template name="Speech">
        <xsl:with-param name="nodes" select="descendant::Wave[Tag/@name='DoNotBuild']"/>
      </xsl:call-template>-->
      <xsl:for-each select="descendant::Wave[Tag/@name='DoNotBuild']">
<xsl:value-of select="ancestor::Bank/@name"/>
<xsl:variable name="Voice" select="ancestor::WaveFolder/@name"/>
        <xsl:choose>
          <xsl:when test="not($Voice)">
            <xsl:text>, </xsl:text>
            <xsl:value-of select="parent::Bank/@name"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>, </xsl:text>
            <xsl:value-of select="$Voice"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:text>, </xsl:text>
<xsl:value-of select="@name"/>
<xsl:text>
</xsl:text>
      </xsl:for-each>

      </xsl:if>
    </xsl:for-each>
  </xsl:template>



  <xsl:template name="Speech">
    <xsl:param name="nodes"/>
    <xsl:for-each select="$nodes">
      <xsl:if test="position()>1">
        <xsl:variable name ="previous" select="position()-1"/>
        <xsl:variable name="previousName" select="$nodes[$previous]/@name"/>
        <xsl:variable name="context" select="substring(@name,0,string-length(@name)-6)"/>
        <xsl:variable name="previousContext" select="substring($previousName,0,string-length($previousName)-6)"/>
        <xsl:if test="not($context = $previousContext)">
          <xsl:value-of select="$context"/>
          <xsl:text>
</xsl:text>
        </xsl:if>
      </xsl:if>
      <xsl:if test="position()=1">
        <xsl:value-of  select="substring(@name,0,string-length(@name)-6)"/>
        <xsl:text>
</xsl:text>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>


