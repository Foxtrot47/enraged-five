<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:ms="urn:schemas-microsoft-com:xslt"
 xmlns:hash ="urn:rockstargames.com:xslt"
  >

<xsl:output method="text"/>

<xsl:template match="/Objects">

#include "Pickups/Data/PickupIds.h"

namespace PickupIds
{
  PickupHash PickupHashes[NUM_PICKUP_TYPES] =
  {
      <xsl:for-each select="child::Pickup">
      <xsl:value-of select="@name"/>_ID,
      </xsl:for-each>
  };

  char* PickupNames[NUM_PICKUP_TYPES] =
  {
      <xsl:for-each select="child::Pickup">
      "<xsl:value-of select="@name"/>",
      </xsl:for-each>
  };
}

</xsl:template>
</xsl:stylesheet> 
