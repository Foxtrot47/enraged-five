@echo off
GOTO PleaseCloseRAVE

:RAVEClosed
x:
ECHO GRABBING LATEST AUDIO TOOLS
p4 sync //depot/gta5/tools/bin/audio/...
pause
start x:\gta5\tools\bin\audio\rave\rave.exe
goto end

rem _________ Functions __________________

:PleaseCloseRAVE
tasklist | find /I "rave.exe"
if %ERRORLEVEL% EQU 0 (
cls
ECHO *****************************
ECHO *** RAVE ALREADY RUNNING  ***
ECHO *****************************
GOTO PleaseCloseRAVE
)
GOTO RAVEClosed


:end
echo exit
exit