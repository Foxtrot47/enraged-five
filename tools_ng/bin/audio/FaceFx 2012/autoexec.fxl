// -----------------------------------------------------------------------------------
// This is the autoexec.fxl file.  It resides in the same directory as 
// FaceFX Studio.  Commands in this file will be executed when 
// FxStudio.exe is run.
//
// You can also create FXL files that are run whenever you open a 
// specific FXA file.  Simply put the FXL file in the directory as the
// FXA file, and give it the same name.
//
// Currently, this file simply prints a message to the log.  Several
// commands commonly found in autoexec files are also listed below
// and can be uncommented to put into effect.
// -----------------------------------------------------------------------------------

// Tells FaceFX to use Direct Sound instead of OpenAL to play sound.
// OpenAL sounds better when scrubbing, but may cause crashes if
// you don't have the latest sound drivers installed along with the latest
// version of OpenAL:
// www.oc3ent.com/downloads/OpenALwEAX.exe
// Comment this line out if you would prefer to use OpenAL.
set -name "g_audiosystem" -v "directsound";

// Uncomment if you want FaceFX to attempt to analyze WAV files shorter 
// than half a second.  Please be aware that we can not guarantee results
// for files shorter than 0.5 seconds.
//set -n "a_audiomin" -v "0.1";

// The below python script is an example of how to automatically modify the 
// analysis text before sending it into FaceFX Studio.  You can insert text tags,
// transliterate proper nouns, or search for phrases to key gestures.
// exec -f "AnalysisTextPreprocessor.py"

// Run some additional setup from a Python script.
exec -f "OnAutoexec.py"

// Register the event custom payload manager plugin.
exec -f "EventCustomPayloadManager.py"

// Prints a message to the log.
print -message "autoexec.fxl file executed";
