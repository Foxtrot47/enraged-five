"""
Helper script to remove any plateaus in an animation after being funneled.

Owner: John Briggs

Copyright (c) 2002-2011 OC3 Entertainment, Inc.
"""
from FxStudio import issueCommand, isCurveOwnedByAnalysis, getSelectedAnimName, getSelectedAnimGroupName, getCurveNames
from FxPhonemes import PhonemeMap
from FxAnimation import Animation
from FxHelperLibrary import get_selected_animpath, split_animpath

# keys make a plateau if they differ by less than this amount
val_epsilon = 0.01
time_epsilon = 0.1

def set_curve_owned_by_user(group, anim, curve):
    """ Sets the curve in the specified animation to be owned by the user. """
    if isCurveOwnedByAnalysis(group, anim, curve):
        issueCommand('curve -edit -group "{0}" -anim "{1}" -name "{2}" -owner "user"'.format(group, anim, curve))
            
            
def set_curves_owned_by_user(group, anim, curveList):
    """ Sets the curves from the mapping in the animation to owned by user. """
    issueCommand('batch')
    for curve in curveList:
        set_curve_owned_by_user(group, anim, curve)
    issueCommand('execBatch -editedcurves')
    
    
def get_mapped_curve_list(groupName, animName):
    """ Returns a list of the curves in the animation in the mapping. """
    mappedCurves = PhonemeMap().getTargetNamesUsedInMapping()
    return [x for x in getCurveNames(groupName, animName) if x in mappedCurves]
    
    
def tweak_keys(curve):
    """ Tweaks the keys in the given curve to remove plateaus. """
    editCmd = 'key -edit -curveName "{0}" -keyIndex {1} -value {2}'.format(
        curve.name, '{0}', '{1}')
    for index, (key1, key2, key3) in enumerate(zip(curve.keys, curve.keys[1:], curve.keys[2:])):
        if key1.value > 0.0 and abs(key2.value - key1.value) < val_epsilon and abs(key2.time - key1.time) > time_epsilon:
            if abs(key3.time - key2.time) < time_epsilon:
                issueCommand(editCmd.format(index+1, key2.value * 0.8))
            elif key3.value > key2.value * 0.5 or key3.value < key2.value * 0.5 - 0.25:
                issueCommand(editCmd.format(index+1, key2.value * 0.5))
            else:
                issueCommand(editCmd.format(index+1, key2.value * 1.2))
    
    
def remove_plateaus(groupName, animName, curveList):
    """ Removes the plateaus from the listed curve in the animation. """
    anim = Animation(groupName, animName)
    for curve in [anim.findCurve(curveName) for curveName in curveList]:
        if curve:
            tweak_keys(curve)
            
            
def fix_plateaus(animpath):
    groupName, animName = split_animpath(animpath)
    curveList = get_mapped_curve_list(groupName, animName)
    set_curves_owned_by_user(groupName, animName, curveList)
    remove_plateaus(groupName, animName, curveList)
    
    
if __name__ == "__main__":
    print "Fixing plateaus..."
    animpath = get_selected_animpath()
    fix_plateaus(animpath)
    print "Finished."
    
