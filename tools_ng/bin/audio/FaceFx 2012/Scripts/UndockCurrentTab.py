# This script will undock the current tab from the main notebook in Studio and put it in a container frame.
# When the container frame is closed, the tab is re-docked back into the main notebook in Studio.

import wx
from FxStudio import *

class StudioContainer(wx.Frame):
    def __init__(self, containee, title):
        
        frame_style = wx.DEFAULT_FRAME_STYLE
        
        if wx.GetKeyState(wx.WXK_SHIFT):
            frame_style = frame_style | wx.FRAME_FLOAT_ON_PARENT
            
        wx.Frame.__init__(self, getMainWindow(), wx.ID_ANY, title, wx.DefaultPosition, (640,480), style=frame_style)
        
        # This is how to give a frame our Studio icon.
        ib = wx.IconBundle()
        ib.AddIconFromFile(getAppIconPath(), wx.BITMAP_TYPE_ANY)
        self.SetIcons(ib)
        
        self.containee = containee
        self.containee_title = title
        
        # Use some Studio theme colors.
        self.color_palette = getColorPalette()
        self.SetBackgroundColour(self.color_palette['BaseColour1'])
        self.SetForegroundColour(self.color_palette['BaseColour8'])
        
        self.Bind(wx.EVT_CLOSE, self.on_close)
        
        undockFromMainWindowNotebook(self.containee)
        self.containee.Reparent(self)
        
        # This is important! What is actually passed to connectSignal() and
        # disconnectSignal() is a method object. Each time you say self.on_app_shutdown
        # Python creates a *new* method object. In order for the connect to match up
        # with the disconnect, we need to make sure that we pass both the *same* method
        # object. Therefore we need to create only one for this instance and keep track
        # of it.
        self.appshutdown_signal_connection = self.on_app_shutdown
        
        connectSignal('appshutdown', self.appshutdown_signal_connection)
        
        self.Show(True)
        # This is required for some strange reason I'm not sure about right now. It seems that
        # when undocking / reparenting wxWidgets decides to hide the underlying window even though
        # it doesn't do that in the AudioInterface.py case.
        self.containee.Show(True)
     
    
    def __del__(self):
        disconnectSignal('appshutdown', self.appshutdown_signal_connection)
    
    
    # Closing the frame manually redocks into the notebook and destroys the frame.
    def on_close(self, event):
        self.containee.Reparent(getMainWindowNotebook())
        
        dockInMainWindowNotebook(self.containee, self.containee_title, select=True)
        
        disconnectSignal('appshutdown', self.appshutdown_signal_connection)
        
        self.Destroy()
        
    
    def on_app_shutdown(self):
        disconnectSignal('appshutdown', self.appshutdown_signal_connection)
        
        self.Destroy()
        
 
if __name__ == '__main__':

    try:
        current_tab = getSelectedTabInMainWindowNotebook()
        StudioContainer(getTabWindowInMainWindowNotebook(current_tab), current_tab).SendSizeEvent()
    except RuntimeError:
        # If there are no tabs in the notebook, simply do nothing.
        pass
 
