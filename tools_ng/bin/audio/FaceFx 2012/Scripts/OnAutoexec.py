# This file is called by autoexec.fxl which is run every time studio opens.
from FxStudio import *
import os.path

runOnceFile = getConsoleVariable("g_userdirectory") + "Settings\\runonce"
if os.path.exists(runOnceFile) == False:
    # This is the first time Studio has been run.
    if isCommandLineMode():
        warn('This appears to be FaceFX Studio\'s first run.')
        error('We still have a bit of set up left to do. Please run FaceFX Studio in normal (GUI) mode before running in command line mode.')
        setConsoleVariable('err_cli_init', '1')
    else:
        runOnceFileHandle = open(runOnceFile, "w")
        runOnceFileHandle.write("File to signal not to open intro content.")
        runOnceFileHandle.close()
        
        if displayYesNoBox("Would you be willing to help us make FaceFX Studio a better product? FaceFX Studio will prompt you to send a crash report in the event of a crash, but sometimes extra information goes a long way to helping our engineers track down an issue. In particularly rare or difficult to diagnose cases, it is helpful for our engineers to be able to contact you to discuss the issue. If you are willing to help, please answer yes to enter contact information to be sent along with crash reports.") == "yes":
            contactInfoFile = getConsoleVariable("g_userdirectory") + "Settings\\contact-info.txt"
            contactInfoPrompt = "Please enter your name and any contact information (email, phone, etc) on the single line below.\nThis information is stored in " + contactInfoFile + "\nand can be edited or removed at any time."
            contactInfo = getTextFromUser(contactInfoPrompt, "Contact Information")
            if len(contactInfo) > 0:
                contactInfoFileHandle = open(contactInfoFile, "w")
                contactInfoFileHandle.write(contactInfo)
                contactInfoFileHandle.close()
        
        sampleFile = getConsoleVariable( "g_appdirectory") + "Samples\\Slade-Maya.facefx"
        issueCommand('loadActor -file "%s";'%(sampleFile))
        selectAnimation("Default", "welcome")
        issueCommand("play;")
