<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output indent="yes" method="xml"/>

  <xsl:template match="/Objects">
    
    <Objects>
      <MixGroupList name="MixGroupList">
        <xsl:for-each select="child::MixGroupSettings">
          <MixGroup>
            <MixGroupHash>
              <xsl:value-of select="@name"/>
            </MixGroupHash>
          </MixGroup>
        </xsl:for-each>
      </MixGroupList>
    </Objects>

  </xsl:template>
</xsl:transform>