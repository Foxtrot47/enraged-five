<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:variable name="PackName">CUTSCENE</xsl:variable>
  
  <xsl:template match="/">

    <Objects>

      <xsl:for-each select="BuiltWaves/Pack">

        <xsl:if test="@name='CUTSCENE'">

          <!--xsl:for-each select="descendant::BankFolder[@name='CUTSCENES']/Bank"-->
					<xsl:for-each select="Bank">
            <xsl:variable name="StreamingSoundName">
              <xsl:value-of select="@name"/>
            </xsl:variable>

            <xsl:variable name="BankName">CUTSCENE\<xsl:value-of select="@name"/>
            </xsl:variable>

            <StreamingSound name="{$StreamingSoundName}" folder="CUTSCENE">

              <Category>CUTSCENE</Category>
              <PlayPhysically>yes</PlayPhysically>
              <StopWhenChildStops>yes</StopWhenChildStops>
              <ReleaseTime>300</ReleaseTime>

              <xsl:for-each select="Wave">

                <xsl:variable name="WaveName">
                  <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
                </xsl:variable>

                <!-- use the center channel for the duration -->
                <xsl:if test="contains($WaveName,'_CENTER')">
                  <xsl:variable name="StreamDuration">
                    <xsl:value-of select="floor(@sourceSize * 500 div @sourceSampleRate)"/>
                  </xsl:variable>
                  <Duration><xsl:value-of select="$StreamDuration"/></Duration>
                </xsl:if>
                <xsl:if test="contains($WaveName,'L_MIX')">
                  <xsl:variable name="StreamDuration">
                    <xsl:value-of select="floor(@sourceSize * 500 div @sourceSampleRate)"/>
                  </xsl:variable>
                  <Duration>
                    <xsl:value-of select="$StreamDuration"/>
                  </Duration>
                </xsl:if>

                <SoundRef>
                  <SoundId>
                    <xsl:value-of select="$PackName"/>_<xsl:value-of select="$StreamingSoundName"/>_<xsl:value-of select="$WaveName"/>
                  </SoundId>
                </SoundRef>
                
              </xsl:for-each>

            </StreamingSound>

            <xsl:for-each select="Wave">

              <xsl:variable name="WaveName">
                <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
              </xsl:variable>

              <SimpleSound name="{$PackName}_{$StreamingSoundName}_{$WaveName}" folder="CUTSCENE_COMPONENTS">

                <xsl:choose>
                <!-- Stereo mapping -->
                  <xsl:when test="contains($WaveName,'LEFT')">
                    <Pan>307</Pan>
                  </xsl:when>
                  <xsl:when test="contains($WaveName,'RIGHT')">
                    <Pan>53</Pan>
                  </xsl:when>

		<!-- Left FS Mix -->
		  <xsl:when test="contains($WaveName,'L_MIX')">
                    <Pan>275</Pan>
                  </xsl:when>
		  <xsl:when test="contains($WaveName,'R_MIX')">
                    <Pan>85</Pan>
                  </xsl:when>

                  <!-- Centre channel for LCR is speaker masked to Centre speaker -->
                  <xsl:when test="contains($WaveName,'CENTRE')">
                    <Pan>-1</Pan>
                     <SpeakerMask>
									      <FrontLeft>no</FrontLeft>
									      <FrontRight>no</FrontRight>
									      <FrontCenter>yes</FrontCenter>
									      <LFE>no</LFE>
									      <RearLeft>no</RearLeft>
									      <RearRight>no</RearRight>
									      <FrontLeftOfCenter>no</FrontLeftOfCenter>
									      <FrontRightOfCenter>no</FrontRightOfCenter>
									    </SpeakerMask>
                  </xsl:when>
                  <xsl:when test="contains($WaveName,'CENTER')">
                    <Pan>-1</Pan>
                     <SpeakerMask>
									      <FrontLeft>no</FrontLeft>
									      <FrontRight>no</FrontRight>
									      <FrontCenter>yes</FrontCenter>
									      <LFE>no</LFE>
									      <RearLeft>no</RearLeft>
									      <RearRight>no</RearRight>
									      <FrontLeftOfCenter>no</FrontLeftOfCenter>
									      <FrontRightOfCenter>no</FrontRightOfCenter>
									    </SpeakerMask>
                  </xsl:when>
                  <!-- 5.0 mapping to speakers -->
                  
                   <xsl:when test="contains($WaveName,'.LS')">
                    <Pan>-1</Pan>
                     <SpeakerMask>
									      <FrontLeft>no</FrontLeft>
									      <FrontRight>no</FrontRight>
									      <FrontCenter>no</FrontCenter>
									      <LFE>no</LFE>
									      <RearLeft>yes</RearLeft>
									      <RearRight>no</RearRight>
									      <FrontLeftOfCenter>no</FrontLeftOfCenter>
									      <FrontRightOfCenter>no</FrontRightOfCenter>
									    </SpeakerMask>
                  </xsl:when>
                     <xsl:when test="contains($WaveName,'.RS')">
                    <Pan>-1</Pan>
                     <SpeakerMask>
									      <FrontLeft>no</FrontLeft>
									      <FrontRight>no</FrontRight>
									      <FrontCenter>no</FrontCenter>
									      <LFE>no</LFE>
									      <RearLeft>no</RearLeft>
									      <RearRight>yes</RearRight>
									      <FrontLeftOfCenter>no</FrontLeftOfCenter>
									      <FrontRightOfCenter>no</FrontRightOfCenter>
									    </SpeakerMask>
                  </xsl:when>
                  <xsl:when test="contains($WaveName,'.L')">
                    <Pan>-1</Pan>
                     <SpeakerMask>
									      <FrontLeft>yes</FrontLeft>
									      <FrontRight>no</FrontRight>
									      <FrontCenter>no</FrontCenter>
									      <LFE>no</LFE>
									      <RearLeft>no</RearLeft>
									      <RearRight>no</RearRight>
									      <FrontLeftOfCenter>no</FrontLeftOfCenter>
									      <FrontRightOfCenter>no</FrontRightOfCenter>
									    </SpeakerMask>
                  </xsl:when>
                     <xsl:when test="contains($WaveName,'.R')">
                    <Pan>-1</Pan>
                     <SpeakerMask>
									      <FrontLeft>no</FrontLeft>
									      <FrontRight>yes</FrontRight>
									      <FrontCenter>no</FrontCenter>
									      <LFE>no</LFE>
									      <RearLeft>no</RearLeft>
									      <RearRight>no</RearRight>
									      <FrontLeftOfCenter>no</FrontLeftOfCenter>
									      <FrontRightOfCenter>no</FrontRightOfCenter>
									    </SpeakerMask>
                  </xsl:when>
                     <xsl:when test="contains($WaveName,'.C')">
                    <Pan>-1</Pan>
                     <SpeakerMask>
									      <FrontLeft>no</FrontLeft>
									      <FrontRight>no</FrontRight>
									      <FrontCenter>yes</FrontCenter>
									      <LFE>no</LFE>
									      <RearLeft>no</RearLeft>
									      <RearRight>no</RearRight>
									      <FrontLeftOfCenter>no</FrontLeftOfCenter>
									      <FrontRightOfCenter>no</FrontRightOfCenter>
									    </SpeakerMask>
                  </xsl:when>
                </xsl:choose>

                <WaveRef>
                  <WaveName>
                    <xsl:value-of select="$WaveName"/>
                  </WaveName>
                  <BankName>
                    <xsl:value-of select="$BankName"/>
                  </BankName>
                </WaveRef>

              </SimpleSound>

            </xsl:for-each>

          </xsl:for-each>

        </xsl:if>

      </xsl:for-each>

    </Objects>

  </xsl:template>

</xsl:transform>