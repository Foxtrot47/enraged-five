<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output indent="yes"/>
  
  <xsl:variable name="NumPositionedSoundsPerStreamingSound">
    <xsl:value-of select="1"/>
  </xsl:variable>

  <xsl:template name="AddPositionedSoundRefs">

    <xsl:param name="HowMany">1</xsl:param>
    <xsl:param name="Count">1</xsl:param>
    <xsl:param name="BaseName"></xsl:param>

    <xsl:if test="$HowMany &gt; 0">

      <SoundRef>
        <SoundId><xsl:value-of select="$BaseName"/>_<xsl:value-of select="$Count"/></SoundId>
        <ReferenceName>Positioned<xsl:value-of select="$Count"/></ReferenceName>
      </SoundRef>

      <xsl:call-template name="AddPositionedSoundRefs">
        <xsl:with-param name="HowMany" select="$HowMany - 1"/>
        <xsl:with-param name="Count" select="$Count + 1"/>
        <xsl:with-param name="BaseName" select="$BaseName"/>
      </xsl:call-template>

    </xsl:if>

  </xsl:template>


  <xsl:template name="AddStereoAndPostionedSounds">

    <xsl:param name="HowMany">1</xsl:param>
    <xsl:param name="BaseName"></xsl:param>
    <xsl:param name="BankName"></xsl:param>
    <xsl:param name="FolderName"></xsl:param>

    <MultitrackSound name="{$BaseName}_STEREO" folder="{$FolderName}">

      <EffectRoute>EFFECT_ROUTE_MUSIC</EffectRoute>
      <Category>RADIO_FRONT_END</Category>

<xsl:choose>
<xsl:when test="contains($BaseName,'_MONO_')">
<!-- special case for mono radio content - create two simple sounds for each wave -->
   
      <xsl:for-each select="Wave">

        <xsl:variable name="WaveName">
          <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
        </xsl:variable>

        <SoundRef>
          <SoundId><xsl:value-of select="$BaseName"/>_STEREO_<xsl:value-of select="$WaveName"/>_L</SoundId>
        </SoundRef>

      </xsl:for-each>


      <xsl:for-each select="Wave">

        <xsl:variable name="WaveName">
          <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
        </xsl:variable>

        <SoundRef>
          <SoundId><xsl:value-of select="$BaseName"/>_STEREO_<xsl:value-of select="$WaveName"/>_R</SoundId>
        </SoundRef>

      </xsl:for-each>
</xsl:when>
<xsl:otherwise>

      <xsl:for-each select="Wave">

        <xsl:variable name="WaveName">
          <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
        </xsl:variable>

        <SoundRef>
          <SoundId><xsl:value-of select="$BaseName"/>_STEREO_<xsl:value-of select="$WaveName"/></SoundId>
        </SoundRef>

      </xsl:for-each>
</xsl:otherwise>
</xsl:choose>

    </MultitrackSound>

    <xsl:call-template name="AddPositionedSounds">
      <xsl:with-param name="HowMany" select="$HowMany"/>
      <xsl:with-param name="BaseName" select="$BaseName"/>
      <xsl:with-param name="FolderName" select="$FolderName"/>
    </xsl:call-template>

    <!-- Start of stereo simple sounds -->
<xsl:choose>
<xsl:when test="contains($BaseName,'_MONO_')">
<!-- special case for mono radio content - create two simple sounds for each wave -->
    <xsl:for-each select="Wave">

      <xsl:variable name="WaveName">
        <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
      </xsl:variable>

      <SimpleSound name="{$BaseName}_STEREO_{$WaveName}_L" folder="{$FolderName}">

        <PlayPhysically>yes</PlayPhysically>
        <Unduckable>yes</Unduckable>
        <Uncompressible>yes</Uncompressible>
        <MuteOnUserMusic>yes</MuteOnUserMusic>     
        <Pan>307</Pan>
        <WaveRef>
          <WaveName><xsl:value-of select="$WaveName"/></WaveName>
          <BankName><xsl:value-of select="$BankName"/></BankName>
        </WaveRef>

      </SimpleSound>
    </xsl:for-each>
    <xsl:for-each select="Wave">

      <xsl:variable name="WaveName">
        <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
      </xsl:variable>

      <SimpleSound name="{$BaseName}_STEREO_{$WaveName}_R" folder="{$FolderName}">

        <PlayPhysically>yes</PlayPhysically>
        <Unduckable>yes</Unduckable>
        <Uncompressible>yes</Uncompressible>
        <MuteOnUserMusic>yes</MuteOnUserMusic>     
        <Pan>53</Pan>
        <WaveRef>
          <WaveName><xsl:value-of select="$WaveName"/></WaveName>
          <BankName><xsl:value-of select="$BankName"/></BankName>
        </WaveRef>

      </SimpleSound>
    </xsl:for-each>
</xsl:when>
<xsl:otherwise>
<!-- usual, stereo case -->
    <xsl:for-each select="Wave">

      <xsl:variable name="WaveName">
        <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
      </xsl:variable>

      <SimpleSound name="{$BaseName}_STEREO_{$WaveName}" folder="{$FolderName}">

        <PlayPhysically>yes</PlayPhysically>
        <Unduckable>yes</Unduckable>
        <Uncompressible>yes</Uncompressible>
        <MuteOnUserMusic>yes</MuteOnUserMusic>

        <xsl:choose>
          <xsl:when test="contains($WaveName,'LEFT')">
            <Pan>307</Pan>
          </xsl:when>

          <xsl:when test="contains($WaveName,'RIGHT')">
            <Pan>53</Pan>
          </xsl:when>
        </xsl:choose>

        <WaveRef>
          <WaveName><xsl:value-of select="$WaveName"/></WaveName>
          <BankName><xsl:value-of select="$BankName"/></BankName>
        </WaveRef>

      </SimpleSound>

    </xsl:for-each>
</xsl:otherwise>
</xsl:choose>
    <!-- End of stereo simple sounds -->

    <!-- Start of positioned simple sounds -->

<xsl:choose>
<xsl:when test="contains($BaseName,'_MONO_')">
<!-- special case for mono radio content - create two simple sounds for each wave -->

    <xsl:for-each select="Wave">

      <xsl:variable name="WaveName">
        <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
      </xsl:variable>

      <SimpleSound name="{$BaseName}_POS_{$WaveName}_L" folder="{$FolderName}">

        <PlayPhysically>yes</PlayPhysically>
        <MuteOnUserMusic>yes</MuteOnUserMusic>

        <WaveRef>
          <WaveName><xsl:value-of select="$WaveName"/></WaveName>
          <BankName><xsl:value-of select="$BankName"/></BankName>
        </WaveRef>

      </SimpleSound>

    </xsl:for-each>

    <xsl:for-each select="Wave">

      <xsl:variable name="WaveName">
        <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
      </xsl:variable>

      <SimpleSound name="{$BaseName}_POS_{$WaveName}_R" folder="{$FolderName}">

        <PlayPhysically>yes</PlayPhysically>
        <MuteOnUserMusic>yes</MuteOnUserMusic>

        <WaveRef>
          <WaveName><xsl:value-of select="$WaveName"/></WaveName>
          <BankName><xsl:value-of select="$BankName"/></BankName>
        </WaveRef>

      </SimpleSound>

    </xsl:for-each>
</xsl:when>
<xsl:otherwise>
<!-- usual, stereo case -->
    <xsl:for-each select="Wave">

      <xsl:variable name="WaveName">
        <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
      </xsl:variable>

      <SimpleSound name="{$BaseName}_POS_{$WaveName}" folder="{$FolderName}">

        <PlayPhysically>yes</PlayPhysically>
        <MuteOnUserMusic>yes</MuteOnUserMusic>

        <WaveRef>
          <WaveName><xsl:value-of select="$WaveName"/></WaveName>
          <BankName><xsl:value-of select="$BankName"/></BankName>
        </WaveRef>

      </SimpleSound>

    </xsl:for-each>
</xsl:otherwise>
</xsl:choose>
    <!-- End of positioned simple sounds -->

  </xsl:template>


  <xsl:template name="AddPositionedSounds">

    <xsl:param name="HowMany">1</xsl:param>
    <xsl:param name="Count">1</xsl:param>
    <xsl:param name="BaseName"></xsl:param>
    <xsl:param name="FolderName"></xsl:param>

    <xsl:if test="$HowMany &gt; 0">

      <MultitrackSound name="{$BaseName}_POS_{$Count}" folder="{$FolderName}">

        <Volume>-10000</Volume>
        <EffectRoute>EFFECT_ROUTE_POSITIONED</EffectRoute>
        <Category>RADIO_POSITIONED</Category>

<xsl:choose>
<xsl:when test="contains($BaseName,'_MONO_')">
<!-- special case for mono radio content - create two simple sounds for each wave -->

        <xsl:for-each select="Wave">

          <xsl:variable name="WaveName">
            <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
          </xsl:variable>

          <SoundRef>
            <SoundId><xsl:value-of select="$BaseName"/>_POS_<xsl:value-of select="$WaveName"/>_L</SoundId>
          </SoundRef>

        </xsl:for-each>

        <xsl:for-each select="Wave">

          <xsl:variable name="WaveName">
            <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
          </xsl:variable>

          <SoundRef>
            <SoundId><xsl:value-of select="$BaseName"/>_POS_<xsl:value-of select="$WaveName"/>_R</SoundId>
          </SoundRef>

        </xsl:for-each>
</xsl:when>
<xsl:otherwise>

        <xsl:for-each select="Wave">

          <xsl:variable name="WaveName">
            <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
          </xsl:variable>

          <SoundRef>
            <SoundId><xsl:value-of select="$BaseName"/>_POS_<xsl:value-of select="$WaveName"/></SoundId>
          </SoundRef>

        </xsl:for-each>
</xsl:otherwise>
</xsl:choose>

      </MultitrackSound>

      <xsl:call-template name="AddPositionedSounds">
        <xsl:with-param name="HowMany" select="$HowMany - 1"/>
        <xsl:with-param name="Count" select="$Count + 1"/>
        <xsl:with-param name="BaseName" select="$BaseName"/>
        <xsl:with-param name="FolderName" select="$FolderName"/>
      </xsl:call-template>

    </xsl:if>

  </xsl:template>


  <xsl:template match="/">
    <Objects>
      <xsl:for-each select="BuiltWaves/Pack[@name='Radio']/BankFolder[@name='Streams']/BankFolder">

        <xsl:variable name="PackName">RADIO</xsl:variable>
        <xsl:variable name="StationName">
          <xsl:value-of select="@name"/>
        </xsl:variable>

        <xsl:choose>

          <!-- Start of advert parsing -->
          <xsl:when test="contains(@name,'ADVERTS')">

            <xsl:for-each select="Bank">
              <xsl:variable name="StreamingSoundName">
                  <xsl:value-of select="@name"/>
                </xsl:variable>

                <xsl:variable name="BankName">
                  <xsl:value-of select="$PackName"/>\<xsl:value-of select="@name"/>
                </xsl:variable>

                <!-- Create a top-level streaming sound -->
                <StreamingSound name="{$StationName}_{$StreamingSoundName}" folder="COMPONENTS">

                  <xsl:for-each select="Wave">

                    <xsl:variable name="WaveName">
                      <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
                    </xsl:variable>

                    <xsl:if test="contains($WaveName,'LEFT')">

                      <xsl:variable name="StreamDuration">
                        <xsl:value-of select="floor(@rawSize * 500 div @rawSampleRate)"/>
                      </xsl:variable>

                      <Duration>
                        <xsl:value-of select="$StreamDuration"/>
                      </Duration>

                    </xsl:if>

                  </xsl:for-each>

                  <DopplerFactor>0</DopplerFactor>	

                  <SoundRef>
                    <SoundId><xsl:value-of select="$StationName"/>_<xsl:value-of select="$StreamingSoundName"/>_STEREO</SoundId>
                    <ReferenceName>Stereo</ReferenceName>
                  </SoundRef>

                  <xsl:variable name="BaseName"><xsl:value-of select="$StationName"/>_<xsl:value-of select="$StreamingSoundName"/><xsl:text>_POS</xsl:text></xsl:variable>

                  <xsl:call-template name="AddPositionedSoundRefs">
                    <xsl:with-param name="HowMany" select="$NumPositionedSoundsPerStreamingSound"/>
                    <xsl:with-param name="BaseName" select="$BaseName"/>
                  </xsl:call-template>

                </StreamingSound>

                <xsl:variable name="BaseName2"><xsl:value-of select="$StationName"/>_<xsl:value-of select="$StreamingSoundName"/></xsl:variable>

                <xsl:variable name="FolderName">COMPONENTS</xsl:variable>

                <xsl:call-template name="AddStereoAndPostionedSounds">
                  <xsl:with-param name="HowMany" select="$NumPositionedSoundsPerStreamingSound"/>
                  <xsl:with-param name="BaseName" select="$BaseName2"/>
                  <xsl:with-param name="BankName" select="$BankName"/>
                  <xsl:with-param name="FolderName" select="$FolderName"/>
                </xsl:call-template>

              </xsl:for-each>

          </xsl:when>
          <!-- End of advert parsing -->

          <xsl:otherwise>

            <!-- Start of station parsing -->
            <xsl:for-each select="BankFolder">

              <!-- Make sure we don't process speech banks -->
              <xsl:if test="not(contains(@name,'DJ_'))">

                <xsl:variable name="StreamingSoundFolderName"><xsl:value-of select="$StationName"/>_<xsl:value-of select="@name"/>
                </xsl:variable>

                <xsl:for-each select="Bank">

                  <xsl:variable name="StreamingSoundName">
                    <xsl:value-of select="@name"/>
                  </xsl:variable>

                  <xsl:variable name="BankName">
                    <xsl:value-of select="$PackName"/>\<xsl:value-of select="@name"/>
                  </xsl:variable>

                  <!-- Create a top-level streaming sound -->
                  <StreamingSound name="{$StationName}_{$StreamingSoundName}" folder="{$StreamingSoundFolderName}">

                    <xsl:for-each select="Wave">

                      <xsl:variable name="WaveName">
                        <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
                      </xsl:variable>

                      <xsl:if test="contains($WaveName,'LEFT')">

                        <xsl:variable name="StreamDuration">
                          <xsl:value-of select="floor(@rawSize * 500 div @rawSampleRate)"/>
                        </xsl:variable>

                        <Duration>
                          <xsl:value-of select="$StreamDuration"/>
                        </Duration>

                      </xsl:if>

                      <xsl:if test="contains($WaveName,'_MONO_')">

                        <xsl:variable name="StreamDuration">
                          <xsl:value-of select="floor(@rawSize * 500 div @rawSampleRate)"/>
                        </xsl:variable>

                        <Duration>
                          <xsl:value-of select="$StreamDuration"/>
                        </Duration>

                      </xsl:if>

                    </xsl:for-each>

                    <DopplerFactor>0</DopplerFactor>

                    <SoundRef>
                      <SoundId><xsl:value-of select="$StationName"/>_<xsl:value-of select="$StreamingSoundName"/>_STEREO</SoundId>
                      <ReferenceName>Stereo</ReferenceName>
                    </SoundRef>

                    <xsl:variable name="BaseName"><xsl:value-of select="$StationName"/>_<xsl:value-of select="$StreamingSoundName"/><xsl:text>_POS</xsl:text></xsl:variable>

                    <xsl:call-template name="AddPositionedSoundRefs">
                      <xsl:with-param name="HowMany" select="$NumPositionedSoundsPerStreamingSound"/>
                      <xsl:with-param name="BaseName" select="$BaseName"/>
                    </xsl:call-template>

                  </StreamingSound>

                  <xsl:variable name="BaseName2"><xsl:value-of select="$StationName"/>_<xsl:value-of select="$StreamingSoundName"/></xsl:variable>

                  <xsl:variable name="FolderName">COMPONENTS</xsl:variable>

                  <xsl:call-template name="AddStereoAndPostionedSounds">
                    <xsl:with-param name="HowMany" select="$NumPositionedSoundsPerStreamingSound"/>
                    <xsl:with-param name="BaseName" select="$BaseName2"/>
                    <xsl:with-param name="BankName" select="$BankName"/>
                    <xsl:with-param name="FolderName" select="$FolderName"/>
                  </xsl:call-template>

                </xsl:for-each>

              </xsl:if>

            </xsl:for-each>
            <!-- End of station parsing -->

          </xsl:otherwise>

        </xsl:choose>
      </xsl:for-each>

    </Objects>

  </xsl:template>

</xsl:stylesheet>