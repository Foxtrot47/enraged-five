<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output indent="yes" method="xml"/>
  
  <xsl:template match="/">
    <xsl:element name="Objects">
      <xsl:for-each select="BuiltWaves/Pack[@name='Radio']/BankFolder[@name='Streams']/BankFolder">
        <xsl:variable name="Name">
          <xsl:value-of select="@name"/>
        </xsl:variable>
        <xsl:choose>
          <!-- Start of advert parsing -->
          <xsl:when test="contains(@name,'ADVERTS')">
            <xsl:element name="RadioStationTrackList">
              <xsl:attribute name="name"><xsl:value-of select="$Name"/></xsl:attribute>
              <xsl:attribute name="folder">
                <xsl:value-of select="$Name"/>
              </xsl:attribute>
              <xsl:element name="Category">RADIO_TRACK_CAT_ADVERTS</xsl:element>
              <xsl:element name="HistorySpace">10</xsl:element>
                <xsl:for-each select="Bank">
                  <xsl:sort select="@name"/>
                  <xsl:element name="Track">
                    <xsl:element name="SoundRef"><xsl:value-of select="$Name"/>_<xsl:value-of select="@name"/>
                    </xsl:element>
                  </xsl:element>
                </xsl:for-each>
            </xsl:element>
        </xsl:when>
          <!-- End of advert parsing -->

          <!-- Start of station parsing -->
          <xsl:otherwise>
            <xsl:for-each select="BankFolder">
              <!-- Make sure we don't process speech banks -->
              <xsl:if test="not(contains(@name,'DJ_'))">
                <xsl:variable name="BankFolderName">
                  <xsl:value-of select="@name"/>
                </xsl:variable>
                <xsl:variable name="TrackListName">
                  <xsl:value-of select="$Name"/>_<xsl:value-of select="@name"/>
                </xsl:variable>
                <xsl:element name="RadioStationTrackList">
                  <xsl:attribute name="name">EP2_<xsl:value-of select="$TrackListName"/></xsl:attribute>
                  <xsl:attribute name="folder">
                    <xsl:value-of select="$Name"/>
                  </xsl:attribute>
                  <xsl:element name="Category">RADIO_TRACK_CAT_<xsl:value-of select="@name"/></xsl:element>
                  <xsl:element name="HistorySpace">10</xsl:element>
                 <xsl:for-each select="Bank">
                    <xsl:sort select="@name"/>
          
                    <!--<xsl:if test="not(contains(@name,'PLACEHOLDER'))">-->
                      <xsl:element name="Track">
                        <xsl:element name="SoundRef"><xsl:value-of select="$Name"/>_<xsl:value-of select="@name"/></xsl:element>
       
                        <!-- Populate context for weather -->
                        <xsl:if test="contains($BankFolderName,'WEATHER')">
                          <xsl:choose>
                            <xsl:when test="contains(@name,'FOG')">
                              <xsl:element name="Context">FOG</xsl:element>
                            </xsl:when>
                            <xsl:when test="contains(@name,'RAIN')">
                              <xsl:element name="Context">RAIN</xsl:element>
                            </xsl:when>
                            <xsl:when test="contains(@name,'SNOW')">
                              <xsl:element name="Context">SNOW</xsl:element>
                            </xsl:when>
                            <xsl:when test="contains(@name,'SUN')">
                              <xsl:element name="Context">SUN</xsl:element>
                            </xsl:when>
                            <xsl:when test="contains(@name,'THUNDER')">
                              <xsl:element name="Context">THUNDER</xsl:element>
                            </xsl:when>
                            <xsl:when test="contains(@name,'CLOUD')">
                              <xsl:element name="Context">CLOUD</xsl:element>
                            </xsl:when>
                            <xsl:when test="contains(@name,'WIND')">
                              <xsl:element name="Context">WIND</xsl:element>
                            </xsl:when>
                          </xsl:choose>
                        </xsl:if>

                      </xsl:element>

                   <!--</xsl:if>--> <!-- not contains PLACEHOLDER -->

                  </xsl:for-each>

                </xsl:element>

            </xsl:if>

            </xsl:for-each>

          </xsl:otherwise>
          <!-- End of station parsing -->

        </xsl:choose>

      </xsl:for-each>

    </xsl:element>

  </xsl:template>

</xsl:stylesheet>