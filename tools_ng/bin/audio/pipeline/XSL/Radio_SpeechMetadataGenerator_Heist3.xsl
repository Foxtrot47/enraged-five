<?xml	version="1.0" encoding="ISO-8859-1"?> <xsl:transform	
version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">

<BuiltWaves>

	<xsl:for-each select="BuiltWaves/Pack">

		<xsl:choose>

			<xsl:when test="starts-with(@name,'DLC_HEIST3')">

				<Pack name="{@name}">

					<xsl:for-each	select="BankFolder">

						<xsl:choose>

							<xsl:when	test="contains(@name,'DJ_')">

								<xsl:variable name="BankFolderName"> 
									<xsl:value-of select="@name"/> 
								</xsl:variable>

								<BankFolder name="{@name}">

									<xsl:for-each select="Bank">

										<xsl:variable name="BankName"> 
											<xsl:value-of	select="@name"/> 
										</xsl:variable>

										<Bank name="{@name}" headerSize="{@headerSize}" timeStamp="{@timeStamp}" builtSize="{@builtSize}">

											<xsl:for-each	select="Wave">

												<Wave	name="{@name}"	nameHash="{@nameHash}" sourceSize="{@sourceSize}" sourceSampleRate="{@sourceSampleRate}" builtSampleRate="{@builtSampleRate}"	builtName="{@builtName}" builtSize="{@builtSize}">

													<xsl:for-each select="Tag"> 
														<xsl:copy-of select="."/>									 
													</xsl:for-each>

													<xsl:for-each select="Marker"> 
														<xsl:copy-of select="."/> 
													</xsl:for-each>

													<xsl:for-each select="Chunk"> 
														<xsl:copy-of select="."/> 
													</xsl:for-each>

													<xsl:variable name="VariationData"> 
														<xsl:value-of select="ceiling(@sourceSize *	5 div	@sourceSampleRate)"/> 
													</xsl:variable>

													<Tag name="variationData" value="{$VariationData}" />

												</Wave>

											</xsl:for-each>

											<xsl:for-each select="Tag"> 
												<xsl:copy-of select="."/> 
											</xsl:for-each>

											<Tag name="voice" value="{$BankFolderName}_{$BankName}"/>

										</Bank>

									</xsl:for-each>

								</BankFolder>

								<xsl:for-each	select="Tag">
									<xsl:copy-of select="."/>
								</xsl:for-each>

							</xsl:when>

							<xsl:otherwise>
								<xsl:copy-of select="."/>
							</xsl:otherwise>

						</xsl:choose>

					</xsl:for-each>

					<xsl:for-each	select="Tag">
						<xsl:copy-of select="."/>
					</xsl:for-each>

				</Pack>

			</xsl:when>

			<xsl:when test="not(contains(@name,'DLC'))">
				<xsl:copy-of select="."/>
			</xsl:when>

		</xsl:choose>

	</xsl:for-each>

</BuiltWaves>

</xsl:template>

</xsl:transform>
