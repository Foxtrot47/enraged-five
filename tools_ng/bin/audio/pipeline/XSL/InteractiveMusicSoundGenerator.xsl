<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 
  <xsl:template match="/">

    <Objects>

      <xsl:for-each select="BuiltWaves/Pack">

        <xsl:if test="@name='INTERACTIVE_MUSIC'">
        	
        	<xsl:variable name="PackName">
            <xsl:value-of select="@name"/>
          </xsl:variable>

          <xsl:for-each select="BankFolder">
          	
          <xsl:variable name="FolderName">
            <xsl:value-of select="@name"/>
          </xsl:variable>
          	
	          <xsl:for-each select="Bank">
	
	            <xsl:variable name="StreamingSoundName">
	              <xsl:value-of select="@name"/>
	            </xsl:variable>
	
	            <xsl:variable name="BankName">
	            	<xsl:value-of select="$PackName"/>\<xsl:value-of select="@name"/>
	            </xsl:variable>
	
	            <StreamingSound name="{$FolderName}_{$StreamingSoundName}" folder="{$FolderName}">

                <StopWhenChildStops>yes</StopWhenChildStops>
                <Category>INTERACTIVE_MUSIC_STEMS</Category>

                <xsl:for-each select="Wave">
	
	                <xsl:variable name="WaveName">
	                  <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
	                </xsl:variable>
	
									<!-- use the left channel of the first stem for the duration -->
	                <xsl:if test="contains(@builtName,'_1_L.WAV')">
	                  <xsl:variable name="StreamDuration">
	                    <xsl:value-of select="floor(@sourceSize * 500 div @sourceSampleRate)"/>
	                  </xsl:variable>
	                  <Duration><xsl:value-of select="$StreamDuration"/></Duration>
	                </xsl:if>
	
								<!-- every stem generates a sound ref -->
								<xsl:if test="contains(@builtName,'_L.WAV')">
									<xsl:variable name="StemName">
	                  <xsl:value-of select="substring($WaveName, 0, string-length($WaveName) - 1)"/>
	                </xsl:variable>
	                <SoundRef>
	                  <SoundId><xsl:value-of select="$FolderName"/>_<xsl:value-of select="$StemName"/></SoundId>
	                </SoundRef>
	               </xsl:if> <!-- ends-with(_L) -->
	                
	              </xsl:for-each>
	
	            </StreamingSound>
	            
	            <!-- Stem multitrack sounds -->
	             <xsl:for-each select="Wave">
	             		<xsl:variable name="WaveName">
	                  <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
	                </xsl:variable>
	                <xsl:if test="contains(@builtName,'_L.WAV')">
										<xsl:variable name="StemName">
	                	  <xsl:value-of select="substring($WaveName, 0, string-length($WaveName) - 1)"/>
	               		 </xsl:variable>
	
									<MultitrackSound name="{$FolderName}_{$StemName}" folder="{$FolderName}_Components">
										
										<EffectRoute>EFFECT_ROUTE_MUSIC</EffectRoute>
										<Category>INTERACTIVE_MUSIC<xsl:value-of select="substring($StemName, string-length($StemName)-1, string-length($StemName))"/></Category>
                    <StopWhenChildStops>yes</StopWhenChildStops>
										
										<!-- go back over all waves looking for ones that this stem references -->
									
										<xsl:for-each select="../Wave">
											<xsl:variable name="ActualWaveName">
			               			 <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
			              	</xsl:variable>
			              	<xsl:if test="starts-with(@builtName, $StemName)">
			            				<SoundRef><SoundId><xsl:value-of select="$FolderName"/>_<xsl:value-of select="$ActualWaveName"/></SoundId></SoundRef>
			            			</xsl:if>
									
										</xsl:for-each>
										
									</MultitrackSound>
									</xsl:if> <!-- wave name ends with _L -->
	                
	             </xsl:for-each>
	             
	             <!-- simple sounds -->
	
	            <xsl:for-each select="Wave">
	
	              <xsl:variable name="WaveName">
	                <xsl:value-of select="substring(@builtName, 0, string-length(@builtName) - 3)"/>
	              </xsl:variable>
	
	              <SimpleSound name="{$FolderName}_{$WaveName}" folder="{$FolderName}_Components">
	
	                <xsl:choose>
	                <!-- Stereo mapping -->
	                  <xsl:when test="contains(@builtName,'_L.WAV')">
	                    <Pan>307</Pan>
	                  </xsl:when>
	                  <xsl:when test="contains(@builtName,'_R.WAV')">
	                    <Pan>53</Pan>
	                  </xsl:when>	                  
	                </xsl:choose>
	
	                <WaveRef>
	                  <WaveName>
	                    <xsl:value-of select="$WaveName"/>
	                  </WaveName>
	                  <BankName>
	                    <xsl:value-of select="$BankName"/>
	                  </BankName>
	                </WaveRef>
	
	              </SimpleSound>
	
	            </xsl:for-each> <!-- for each wave (SimpleSound generation) -->
	
	          </xsl:for-each> <!-- for each bank  -->
	          
	         </xsl:for-each> <!-- for each bank folder -->

        </xsl:if> <!-- if pack name equals interactive music -->

      </xsl:for-each> <!-- for each pack -->

    </Objects>

  </xsl:template>

</xsl:transform>