<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:transform version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output indent="yes"/>
 

<xsl:template match="/">

<Objects>

  <xsl:for-each select="BuiltWaves/Pack">

    <xsl:if test="@name='STREAMS'">

      <xsl:for-each select="BankFolder">
        <xsl:if test="@name='ARTIST_MUSIC'">
          <xsl:for-each select="BankFolder">
            <xsl:variable name="ArtistName">
              <xsl:value-of select="@name"/>
            </xsl:variable>

            <ArtistTrackList name="ArtistName">
              <name>
                <xsl:value-of select="ART_{$ArtistName}"/></name>
              <xsl:for-each select="Bank">
                <Track>
                  <ArtistTrack>
                    <xsl:value-of select="@name"/>
                  </ArtistTrack>
                </Track>
              </xsl:for-each>
            </ArtistTrackList>

            <xsl:for-each select="Bank">
              <ArtistTrack name="AT_{@name}">
                <Track>
                  <xsl:value-of select="MUS_{@name}"/>
                </Track>
                <Name>
                  <xsl:value-of select="@name"/>
                </Name>
              </ArtistTrack>
            </xsl:for-each>
          </xsl:for-each>
        </xsl:if>
      </xsl:for-each>

    </xsl:if>

  </xsl:for-each>

</Objects>

</xsl:template>

</xsl:transform>