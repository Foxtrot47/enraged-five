@echo off
setlocal
cd /d %2

set cmdline=%*

Set APP=game_win64_beta.exe
set APPCMP=game_win64_beta.cmp
Set ROOTDIR=%cd%/

echo Launching from: %2

:START
::***********************
::***** main 'loop' *****
::***********************
GOTO StartRFS
:RFSRunning
GOTO StartGame

:: - end of main 'loop'

:StartRFS
tasklist | find /I "SysTrayRfs.exe" > NUL
IF %ERRORLEVEL% EQU 1 (
	echo Starting systrayrfs.
	start %RS_TOOLSBIN%\SysTrayRfs.exe -trusted -nofocus
)
GOTO RFSRunning


:StartGame
rem killing any old instances
tasklist | find /I "game_win64_*.exe" > NUL
IF %ERRORLEVEL% EQU 1 (
	echo Killing old instances
	taskkill /F /IM game_win64_*
)

echo ***********************************
echo Launching game...
echo.

start %APP% %cmdline%

goto end

:end
timeout 30


