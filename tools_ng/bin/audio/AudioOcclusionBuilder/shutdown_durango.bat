@echo off
setlocal
cd /d %RS_BUILDBRANCH%

set cmdline=%*

set runtooldir=%DurangoXDK%\bin\
set runtool=%runtooldir%xbapp.exe

call %cd%\xbo_scripts\set_package_name.bat


title Shutting down GTA5 Xbox One...
echo.

if not exist "%runtool%" call :error "Xbox One XDK or support tools not installed as expected"
if errorlevel 1 exit %errorlevel%

echo Terminating.  If the build gets stuck here, we may need to reboot as well.
call "%runtool%" terminate %fullpackagename%

REM Not sure we actually need to reboot the console.
REM echo rebooting
REM call "%runtooldir%xbreboot" 

if not %errorlevel%==0 call :error "Failed to Shutdown Xbox One"
if not %errorlevel%==0 exit %errorlevel%

title Done shutting down GTA5 Xbox One%
echo.
pause
exit 0

:error
echo.
title Failed to shut down GTA5 Xbox One%
echo Error shutting down Xbox One game!
echo.
echo. ^> %~1
echo.
pause
exit 1
