@echo off
setlocal
cd /d %RS_BUILDBRANCH%

set cmdline=%*

set runtool=%DurangoXDK%\bin\xbapp.exe
set exename=game_durango_bankrelease
set exetype=%exename:game_durango_=%

call %cd%\xbo_scripts\set_package_name.bat
set appid=%appidprefix%%exetype%

title Running GTA5 Xbox One %exetype%...
echo.

if not exist "%runtool%" call :error "Xbox One XDK or support tools not installed as expected"
if errorlevel 1 exit %errorlevel%

if not exist "%exename%.exe" call :error "Can't find Xbox One executable [%exename%.exe]"
if errorlevel 1 exit %errorlevel%

taskkill /t /im SysTrayRfs.exe >nul 2>&1
ping -w 1000 -n 2 127.0.0.1 >nul
tasklist | find /i "SysTrayRfs.exe" >nul
if not %errorlevel%==0 start %RS_TOOLSROOT%\bin\systrayrfs.exe -trusted -nofocus
ping -w 1000 -n 2 127.0.0.1 >nul
set /p hostaddr=<%TEMP%\rfs.dat

echo.
echo Launching GTA5 Xbox One %exetype%
echo.

call %cd%\xbo_scripts\deploy_execs.bat %exetype%
call "%runtool%" launch %packagefamilyname%!%appid% %cmdline%

if not %errorlevel%==0 call :error "Failed to launch Xbox One %exetype% executable"
if not %errorlevel%==0 exit %errorlevel%

title Done running GTA5 Xbox One %exetype%
echo.
pause
exit 0

:error
echo.
title Failed running  GTA5 Xbox One %exetype%
echo Error launching Xbox One game!
echo.
echo. ^> %~1
echo.
pause
exit 1
