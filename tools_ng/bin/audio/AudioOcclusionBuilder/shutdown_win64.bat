@echo off
setlocal
cd /d %RS_BUILDBRANCH%

tasklist | find /I "game_win64_*.exe" > NUL
IF %ERRORLEVEL% EQU 1 (
	echo Killing old instances
	taskkill /F /IM game_win64_*
)

timeout 30


