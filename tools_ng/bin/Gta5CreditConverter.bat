@ECHO OFF
CALL setenv.bat
SET GAMETEXT=%RS_ASSETS%\GameText
SET EXE=%RS_TOOLSBIN%\CreditConverter.exe
SET OUTPUT=%GAMETEXT%\common\credits.txt
SET JOBCODES=%GAMETEXT%\American\americanJobTitles.txt

ECHO.
ECHO.

ECHO GAMETEXT:          %GAMETEXT%
ECHO EXE:               %EXE%
ECHO OUTPUT:            %OUTPUT%
ECHO JOBCODES:          %JOBCODES%

p4 sync %OUTPUT%
p4 edit %OUTPUT%
p4 reopen %OUTPUT%

ECHO.
ECHO.

ECHO PARAMS: %*

ECHO.
ECHO.

start %EXE% --output=%OUTPUT% --jobCodes=%JOBCODES% %*