echo off

call setenv.bat

echo EXPORTFOLDER       : %1
echo COMPILE_BATCH_FILE : %2
echo LST FILE           : %3


REM --------------------------------------------------------------------------------------------------
REM Run by NavMeshTray app when navmesh export starts.  It waits until "compile_complete.txt" exists
REM before launching the navmesh compiler.
REM
REM %1=The output directory for navmeshes
REM %2=Compile batch file
REM %3=The ".lst" navmeshes listing file to use
REM --------------------------------------------------------------------------------------------------

echo Waiting for "export_complete.txt" to exist..

REM ----------------------------------------------------------------------------------------------
REM Wait until navmesh export is complete as indicated by the "export_complete.txt" file existing
REM ----------------------------------------------------------------------------------------------

:TryAgain

IF NOT exist %1\export_complete.txt (
	goto TryAgain
)

REM -----------------------------------------
REM Reboot the xbox360 at the end of export
REM -----------------------------------------

pushd "C:\Program Files\Microsoft Xbox 360 SDK\bin\win32"
xbreboot
popd


REM -----------------------------------------------------------------------
REM Start the navmesh compilation
REM The Compile_Project.bat file creates "compile_complete.txt" at the end
REM -----------------------------------------------------------------------

call %2 %1 %3

