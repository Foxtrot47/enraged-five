echo off

echo Getting latest map data for %RS_PROJECT%

set p4user
p4 login -s

call setenv.bat

p4 sync //depot/gta5/tools_ng/bin/navgen/NavMeshMaker/GTA5_EntitySets.xml
p4 sync //depot/gta5/tools_ng/bin/navgen/NavMeshMaker/GTA5_IplGroups.xml
p4 sync //depot/gta5/tools_ng/bin/navgen/NavMeshMaker/GTA5_ResAreas.xml
p4 sync //depot/gta5/assets_ng/navmesh/
p4 sync //depot/gta5/build/dev_ng/common/data/gameconfig.xml
p4 sync //depot/%RS_PROJECT%/build/dev_ng/x64/levels/...

REM p4 sync //depot/%RS_PROJECT%/build/dev/pc/levels/...

REM pause
