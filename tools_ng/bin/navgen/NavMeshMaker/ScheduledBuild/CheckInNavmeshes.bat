

REM -------------------------------------------------------------------------------------------------------------
REM We do not need to open a connection to Perforce, since we
REM have environment variables set up to specify the client, etc
REM
REM The parameter %1% is the full path to the navmeshes.zip file, w/o trailing slash, and minus the filename
REM The parameter %2% is the full path to the export folder where the new navmeshes.zip files are, again
REM  this path is w/o trailing backslash
REM
REM ----------------------------------------------------------------------------------

echo p4host = %p4host%
echo p4port = %p4port%
echo p4user = %p4user%

REM --------------------------------------------
REM Ensure that we have the file on the client
REM --------------------------------------------

p4 sync %1\navmeshes0.zip
p4 sync %1\navmeshes1.zip
p4 sync %1\navmeshes2.zip
p4 sync %1\navmeshes3.zip
p4 sync %1\navmeshes4.zip
p4 sync %1\navmeshes5.zip
p4 sync %1\navmeshes6.zip
p4 sync %1\navmeshes7.zip
p4 sync %1\navmeshes8.zip
p4 sync %1\navmeshes9.zip

REM --------------------
REM Check the file out
REM --------------------

p4 edit %1\navmeshes0.zip %1\navmeshes1.zip %1\navmeshes2.zip %1\navmeshes3.zip %1\navmeshes4.zip %1\navmeshes5.zip %1\navmeshes6.zip %1\navmeshes7.zip %1\navmeshes8.zip %1\navmeshes9.zip

REM --------------------------------------
REM Copy the new file over the depot file
REM --------------------------------------

copy %2\navmeshes0.zip %1\navmeshes0.zip
copy %2\navmeshes1.zip %1\navmeshes1.zip
copy %2\navmeshes2.zip %1\navmeshes2.zip
copy %2\navmeshes3.zip %1\navmeshes3.zip
copy %2\navmeshes4.zip %1\navmeshes4.zip
copy %2\navmeshes5.zip %1\navmeshes5.zip
copy %2\navmeshes6.zip %1\navmeshes6.zip
copy %2\navmeshes7.zip %1\navmeshes7.zip
copy %2\navmeshes8.zip %1\navmeshes8.zip
copy %2\navmeshes9.zip %1\navmeshes9.zip

REM ------------------------
REM Check the file back in
REM ------------------------

p4 submit -d "Automated navmeshes check-in" "%1\navmeshes*.zip"

