echo off

REM ----------------------------------------------------------------
REM Delete all the files in the export folder
REM Ensure that the folder exists
REM Use with care, make sure input parameter is valid or you may
REM end up deleting your entire HD which would be annoying.
REM We will leave the .srf files, as these contain partial-recompile
REM info & a CRC check which may allow us to skip the expensive 1st
REM stage of navmesh creation (triangulation & optimisation)
REM ----------------------------------------------------------------

rename %1\rfs.dat rfs.temp_rename

del /f /q %1\*.txt
del /f /q %1\*.lst
del /f /q %1\*.tri
del /f /q %1\*.dat

del /f /q %1\*.oct
del /f /q %1\*.cvr
del /f /q /s %1\*.inv
del /f /q %1\*.img
del /f /q %1\*.rpf

del /f /q %1\*.ihn

del /f /q %1\*.iam
del /f /q %1\*.agi

rename %1\rfs.temp_rename rfs.dat

mkdir %1
