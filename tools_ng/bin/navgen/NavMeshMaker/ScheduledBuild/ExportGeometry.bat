REM echo off

call %1%\tools\bin\setenv.bat

REM -----------------------------------------------------------------------------------------------------------
REM ExportGeometry.bat
REM Runs the specified game to export navmeshes
REM
REM %1 = The project's Root Dir (eg. "X:\GTA\")
REM %2 = Tool executable
REM %3 = The folder to export navmeshes into
REM %4 = The ".lst" file detailing the navmesh sections & addition params for compilation
REM %5 = Additional arguments for the game (eg. the name of a specific level to load?)
REM -----------------------------------------------------------------------------------------------------------

echo -----------------------------------------------------------------------
echo 1 Root Dir = %1
echo 2 Tool executable = %2
echo 3 Navmesh export folder = %3
echo 4 Navmeshes listing file = %4
echo 5 Level-specific game param = %5
echo -----------------------------------------------------------------------

REM -------------------------------------------------------------------
REM delete old ???????_cache.dat files or we may miss some collision
REM -------------------------------------------------------------------

del %1\build\dev_ng\pc\data\*_cache.dat

REM -------------
REM run the tool
REM -------------

pushd %1\build\dev

set COMMON_ARGS=-nopopups -navgencfg=%RS_TOOLSBIN%\navgen\NavMeshMaker\navgenconfig.xml

%2 %~5 -exportall -exportFolder "%3" -scheduledcompile -navgencmdfile=%4 %COMMON_ARGS%

popd
