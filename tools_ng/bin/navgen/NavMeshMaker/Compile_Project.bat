echo off

call setenv.bat

REM -----------------------------------------------------------------------------
REM Set up some paths, etc
REM -----------------------------------------------------------------------------

set TOOLS_PATH=%1
set BUILD_DIR=%2
set GAMEPATH=%3
set CMD_FILE=%4
set CMD_FILE_VEHICLES=%5
set MAKE_RPF=%6
set COMMON_ARGS=-nopopups -noHangDetectThread -navgencfg=%RS_TOOLSBIN%\navgen\NavMeshMaker\navgenconfig.xml
set AREAS=%8
set COVERPOINTS=%9

echo TOOLS_PATH = %TOOLS_PATH%
echo BUILD_DIR = %BUILD_DIR%
echo GAMEPATH = %GAMEPATH%
echo CMD_FILE = %CMD_FILE%
echo CMD_FILE_VEHICLES = %CMD_FILE_VEHICLES%
echo MAKE_RPF = %MAKE_RPF%
echo AUDPARAMS = %AUDPARAMS%
echo COMMON_ARGS = %COMMON_ARGS%
echo AREAS = %AREAS%
echo COVERPOINTS = %COVERPOINTS%

path %RS_TOOLSBIN%;%RS_TOOLSBIN%\navgen\NavMeshMaker;%path%

pushd %GAMEPATH%

pause

REM -----------------------------------------------------------------------------
REM Compile the vehicles
REM Sampling parameters are not passed into NavMesMaker here, as each entry in
REM "Vehicles.lst" has a full set of params specified.
REM We don't cull behind walls for vehicles as there are exposed backface polys.
REM -----------------------------------------------------------------------------

call NavMeshGenerator.exe -sample -ignoresteepness -pedheighttest 0.6 -nonmap -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE_VEHICLES% %COMMON_ARGS%
call NavMeshGenerator.exe -merge -nonmap -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE_VEHICLES% %COMMON_ARGS%
call NavMeshGenerator.exe -analysedynamic -nonmap -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE_VEHICLES% %COMMON_ARGS%

REM -----------------------------------------------------------------------------
REM Compile the main map
REM -----------------------------------------------------------------------------

call NavMeshGenerator.exe -numthreads 0 -sample -cullbehindwalls -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE% -areas %AREAS% %COMMON_ARGS%
call NavMeshGenerator.exe -numthreads 0 -merge -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE% %COMMON_ARGS%
call NavMeshGenerator.exe -stitch -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE% %COMMON_ARGS%
call NavMeshGenerator.exe -analyse -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE% %COMMON_ARGS% -areas %AREAS% %COMMON_ARGS% -coverpoints %COVERPOINTS%
call NavMeshGenerator.exe -floodfill -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE% %COMMON_ARGS%
REM call NavMeshGenerator.exe -hierarchical -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE%

REM -----------------------------------------------------------------------------
REM Compile the audio meshes
REM ----------------------------------------------------------------------------- 

REM call AudioMeshMaker.exe -audiomesh -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE%
REM call AudioMeshMaker.exe -audiostitch -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE%

REM ----------------------------------------------------
REM Create the mapping files for navmeshes & audmeshes
REM ----------------------------------------------------

REM call NavMeshGenerator.exe -createmappingfile inv NavmeshIndexMapping.dat -inputpath %BUILD_DIR% -gamepath %GAMEPATH% %COMMON_ARGS%
REM call NavMeshGenerator.exe -createmappingfile iam AudmeshIndexMapping.dat -inputpath %BUILD_DIR% -gamepath %GAMEPATH% %COMMON_ARGS%
REM call NavMeshGenerator.exe -createmappingfile ihn NavnodeIndexMapping.dat -inputpath %BUILD_DIR% -gamepath %GAMEPATH% %COMMON_ARGS%

REM --------------------------------------------------------------------------------
REM Before copying files into subfolders, make sure we don't already have a bunch
REM of old files in the subfolders. That would be OK most of the time, but if files
REM were added or removed, the distribution between folders may be different and some
REM files can end up in multiple directories (RPFs) and cause problems in the game.
REM --------------------------------------------------------------------------------

del /Q %BUILD_DIR%\0
del /Q %BUILD_DIR%\1
del /Q %BUILD_DIR%\2
del /Q %BUILD_DIR%\3
del /Q %BUILD_DIR%\4
del /Q %BUILD_DIR%\5
del /Q %BUILD_DIR%\6
del /Q %BUILD_DIR%\7
del /Q %BUILD_DIR%\8
del /Q %BUILD_DIR%\9

REM --------------------------------------------------------------------------
REM Copy "inv" files into 10 separate subfolders to try and combat the awful
REM time-complexity of dealing with RPF files in the build pipeline.
REM We will make 10 RPFs "navmeshes0.rpf" to "navmeshes9.rpf"
REM --------------------------------------------------------------------------

call NavMeshGenerator.exe -copyintosubfolders inv -inputpath %BUILD_DIR% %COMMON_ARGS%

REM --------------------------------------------------------------------------
REM Not entirely sure if %MAKE_RPF% will overwrite existing RPF files, or add
REM to them. So, we delete them here to be sure.
REM --------------------------------------------------------------------------

del /Q/S %BUILD_DIR%\navmeshes*.zip

REM -----------------------------------------------------------------------------
REM Add all the ".inv" files to the navmeshes.rpf
REM Add all the ".inh" files to the navnodes.rpf
REM -----------------------------------------------------------------------------

call %MAKE_RPF% %BUILD_DIR%

REM ----------------------------------------------------------------------------------
REM Notify that the compilation is complete by creating the file "compile_complete.txt"
REM ----------------------------------------------------------------------------------

echo finished >> %BUILD_DIR%/compile_complete.txt

