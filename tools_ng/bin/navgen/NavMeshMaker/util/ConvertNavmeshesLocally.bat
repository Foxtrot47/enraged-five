REM    Usage: ConvertNavMeshesLocally.bat <levelname>

call setenv.bat

%RS_TOOLSLIB%\util\data_convert_file.rb %RS_EXPORT%\levels\%1\navmeshes0.zip
%RS_TOOLSLIB%\util\data_convert_file.rb %RS_EXPORT%\levels\%1\navmeshes1.zip
%RS_TOOLSLIB%\util\data_convert_file.rb %RS_EXPORT%\levels\%1\navmeshes2.zip
%RS_TOOLSLIB%\util\data_convert_file.rb %RS_EXPORT%\levels\%1\navmeshes3.zip
%RS_TOOLSLIB%\util\data_convert_file.rb %RS_EXPORT%\levels\%1\navmeshes4.zip
%RS_TOOLSLIB%\util\data_convert_file.rb %RS_EXPORT%\levels\%1\navmeshes5.zip
%RS_TOOLSLIB%\util\data_convert_file.rb %RS_EXPORT%\levels\%1\navmeshes6.zip
%RS_TOOLSLIB%\util\data_convert_file.rb %RS_EXPORT%\levels\%1\navmeshes7.zip
%RS_TOOLSLIB%\util\data_convert_file.rb %RS_EXPORT%\levels\%1\navmeshes8.zip
%RS_TOOLSLIB%\util\data_convert_file.rb %RS_EXPORT%\levels\%1\navmeshes9.zip
