echo off

call setenv.bat

REM -----------------------------------------------------------------------------
REM Set up some paths, etc
REM -----------------------------------------------------------------------------

set BUILD_DIR=%1
set CMD_FILE=%2
set TOOLS_PATH="%RS_TOOLSBIN%\navgen\NavMeshMaker"
set GAMEPATH="%RS_BUILDBRANCH%"
set MAKE_RPF="%RS_TOOLSBIN%\navgen\NavMeshMaker\MakeRPF_heightmeshes.bat"
set COMMON_ARGS=-nopopups -navgencfg=%TOOLS_PATH%\navgenconfig.xml

pushd %TOOLS_PATH%

echo TOOLS_PATH = %TOOLS_PATH%
echo BUILD_DIR = %BUILD_DIR%
echo GAMEPATH = %GAMEPATH%
echo CMD_FILE = %CMD_FILE%
echo COMMON_ARGS = %COMMON_ARGS%

path %RS_TOOLSBIN%;%RS_TOOLSBIN%\navgen\NavMeshMaker;%path%

cd %1%

REM -----------------------------------------------------------------------------
REM Compile the main map
REM -----------------------------------------------------------------------------

call NavMeshGenerator.exe -sample -aerial -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE% %COMMON_ARGS%
call NavMeshGenerator.exe -merge -aerial -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE% %COMMON_ARGS%
call NavMeshGenerator.exe -stitch -aerial -inputpath %BUILD_DIR% -gamepath %GAMEPATH% -cmdfile %CMD_FILE% %COMMON_ARGS%

REM -----------------------------------------
REM Create the mapping files for heightmeshes
REM -----------------------------------------

REM call NavMeshGenerator.exe -createmappingfile inh HeightMeshIndexMapping.dat -aerial -inputpath %BUILD_DIR% -gamepath %GAMEPATH% %COMMON_ARGS%

del /Q %BUILD_DIR%\0
del /Q %BUILD_DIR%\1
del /Q %BUILD_DIR%\2
del /Q %BUILD_DIR%\3
del /Q %BUILD_DIR%\4
del /Q %BUILD_DIR%\5
del /Q %BUILD_DIR%\6
del /Q %BUILD_DIR%\7
del /Q %BUILD_DIR%\8
del /Q %BUILD_DIR%\9

REM ----------------------------------------------------------------------------------------
REM Copy to subfolders. If we try to put all the files in a single archive, we are likely
REM to hit the limit of the RPF name heap.
REM ----------------------------------------------------------------------------------------

call NavMeshGenerator.exe -copyintosubfolders inh -aerial -inputpath %BUILD_DIR% %COMMON_ARGS%

REM --------------------------------------------------------------------------
REM Not entirely sure if data_mk_generic_zip.rb will overwrite existing zip files, or add
REM to them. So, we delete them here to be sure.
REM --------------------------------------------------------------------------

del %BUILD_DIR%\heightmeshes*.zip

REM -----------------------------------------------------------------------------
REM Add all the ".inh" files to heightmeshes*.zip
REM -----------------------------------------------------------------------------

call %MAKE_RPF% %BUILD_DIR%

REM ------------------------------------------------------------------------------------------------
REM Notify that the compilation is complete by creating the file "compile_heightmeshes_complete.txt"
REM ------------------------------------------------------------------------------------------------

echo finished >> %BUILD_DIR%/compile_heightmeshes_complete.txt

popd
