SET symbol_source=X:\gta5\titleupdate\dev

SET symbol_destination=N:\RSGEDI\Symbol\Symbol_Server\

SET LABEL="%DATE%_%TIME%"
rem Remove '.', ':' and '/' characters
set LABEL2=%LABEL::=_%
set LABEL3=%LABEL2:.=_%
set LABEL4=%LABEL3: =%
set LABEL5=%LABEL4:/=%
SET build_id=%LABEL5%

CALL %RS_TOOLSROOT%\bin\Symstore\symstore.exe add /t "GTA5" /f %symbol_source% /s %symbol_destination% /v %build_id%

PAUSE
