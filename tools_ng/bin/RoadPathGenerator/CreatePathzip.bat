echo CreatePathzip.bat

rem params
rem %1 exe name and path
rem %2 x:\gta5
rem %3 gta5 (level ID)

rem SOME ISSUES WHEN GETTING THIS TO WORK
rem GAME CONFIG pools that aren't large enough. Setting to these values works at the moment
rem DrawableStore = 60000
rem FragmentStore 14255
rem MaxLoadedInfo 65000
rem MaxLoadRequestedInfo 65000
rem ActiveLoadedInfo 65000
rem TxdStore 40000
rem fwArchetypePooledMap 62000

rem Visual studio args for gta5 main level
rem -level=gta5 -nopopups -nohangdetectthread -texlite  -nosnap -exportiplgroups=\gta5\tools\bin\navgen\NavMeshMaker\GTA5_IplGroups.xml -exportall
rem Visual studio args for heists island generation
rem -level=gta5 -nopopups -nohangdetectthread -texlite  -nosnap -exportiplgroups=\gta5\tools\bin\navgen\NavMeshMaker\GTA5_IplGroups.xml -exportall -loadMapDLCOnStart -generateIsland -extracontent=x:\gta5_dlc\mpPacks\mpHeist4\build\dev_ng\ -pathsIPL=X:\paths\paths.IPL 

rem without -pathsIPL=<file path> the tool will load the file from perforce at common\<%3>\paths.ipl
rem -generateIsland gets 1024 to be added to the generated file id's (for use when generating heist island nodes)

rem generating zip file throgh bat file (below) no longer seems to work, can just do it manually with z-zip BUT SET COMPRESSION TO STORE

rem data files are stored in assets_ng/titleupdate and read from gta5\titleupdate/dev_ng/<platform>/levels/<level>/paths.rpf

rem the exe we actually use is X:\gta5\build\dev_ng\RoadPathsGenerator_x64.exe

rem use -pathsIPL=X:\paths\paths.IPL if you want to generate data from a different file than is stored in the level arg
rem DLC_ARGS for loading data that isn't part of main gta5 level

rem FOR ISLAND HEIST
rem set DLC_ARGS=-generateIsland -loadMapDLCOnStart -extracontent=x:\gta5_dlc\mpPacks\mpHeist4\build\dev_ng\ 

echo %1
echo %2
echo %3

pushd %2\build\dev_ng

set COMMON_ARGS=-level=%3 -nopopups -nohangdetectthread -texlite -gamepath=%RS_BUILDBRANCH%
set TOOL_ARGS=-nosnap -exportall -exportiplgroups=\gta5\tools\bin\navgen\NavMeshMaker\GTA5_IplGroups.xml -pathsIPL=X:\paths\paths.IPL
set DLC_ARGS=-generateIsland -loadMapDLCOnStart -extracontent=x:\gta5_dlc\mpPacks\mpHeist4\build\dev_ng\

rem RUN TOOL
%1 %COMMON_ARGS% %TOOL_ARGS% %DLC_ARGS%

popd

IF %ERRORLEVEL% NEQ 0 GOTO SkipMakeZip

echo Intermediate files created in x:\gta5\build\dev_ng\common\data\paths\

rem GENERATE ZIP FILE
set FILTER=*.ind
set INPUT=%2/build/dev_ng/common/data/paths/
set OUTPUT=%2/assets_ng/titleupdate/dev_ng/levels/%3/paths.zip

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --uncompressed --filter=%FILTER% --output=%OUTPUT% %INPUT%

echo Zip created in %OUTPUT%

:SkipMakeZip
