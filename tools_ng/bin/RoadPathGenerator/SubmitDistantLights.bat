REM Submit DistantLights

rem params
rem %1. The project Root 
rem %2. The Level Name

set SOURCE=%1\build\dev\common\data\paths\distantlights.dat
set DEST=%1\build\dev\common\data\levels\%2\distantlights.dat

p4 sync %DEST%
p4 edit %DEST%

copy %SOURCE% %DEST%

p4 submit -d "Automated DistantLight Check-In" %DEST%

