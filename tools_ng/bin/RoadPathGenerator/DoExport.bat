echo DoExport.bat


rem ====================================================
rem rem SEE CREATEPATHZIP for most of the functionality
rem DoExport expects 3 parameters
rem 1. The project Root
rem 2. The executable to run
rem 3. the level name "gta5" "testlevel" etc...
rem ====================================================

echo off

rem params
rem echo %1
rem echo %2
rem echo %3

del /f /q X:\gta5\build\dev_ng\common\data\paths\*.*

call CheckOutPathzip.bat %1\assets_ng\titleupdate\dev_ng\levels\%3\paths.zip
call CreatePathzip.bat %2 %1 %3
echo call CheckInPathzip.bat %1\assets_ng\titleupdate\dev_ng\levels\%3\paths.zip
echo call SubmitDistantLights.bat %1 %3

echo Road Paths Build Complete

