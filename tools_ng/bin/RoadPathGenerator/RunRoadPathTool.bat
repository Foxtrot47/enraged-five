echo off

call X:\gta5\tools\bin\setenv.bat

pushd x:\gta5\tools\bin\RoadPathGenerator

rem ====================================================
rem SEE CREATEPATHZIP for most of the functionality
rem DoExport expects 4 parameters
rem 1. The project Root
rem 2. The executable to run
rem 3. the level name "gta5" "testlevel" etc...
rem ====================================================

call DoExport.bat %RS_PROJROOT% roadpathsgenerator_x64.exe gta5

popd

