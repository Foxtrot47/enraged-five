echo DoExport.bat

rem ====================================================
rem DoExport expects 3 parameters
rem 1. The project Root
rem 2. The executable to run
rem 3. the level name "gta5" "testlevel" etc...
rem ====================================================

echo off

rem params
rem echo %1
rem echo %2
rem echo %3

mkdir %1\exported_LODLights\
del /f /q %1\exported_LODLights\*.*

rem ExportLODLights.bat
pushd %1\build\dev_gen9_sga
set COMMON_ARGS=-loadMapDLCOnStart -exportall -nopopups -update=x:\gta5\titleupdate\dev_gen9_sga\ -nomtplacement -noHangDetectThread -noaudio -aggressivecleanup -nulldriver -nocloth -all_tty=fatal -all_log=fatal -lodST_log=debug3 -lodST_tty=debug3 -dlc_log=debug2 -dlc_tty=debug2 -logfile=X:\light_extraction_tool_bin.log
%2 %COMMON_ARGS% %COMMAND_ARGS_DLC% -level=%3
popd

rem CheckOutLODLightzip.bat
set DEST=%1\assets_gen9_sga\export\levels\%3\LODLights.zip
p4 sync %DEST%
p4 edit %DEST%

call CreateLODLightsZip.bat %1 %2 %3

echo LOD Light Extraction Complete
