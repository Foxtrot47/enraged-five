echo on

rem 1. The project Root
rem 2. The executable to run
rem 3. the level name "gta5" "testlevel" etc...

call X:\gta5\tools_ng\bin\setenv.bat

set FILTER=*.imap
set INPUT=%1/exported_LODLights/
set OUTPUT=%1/assets_gen9_sga/export/levels/%3/LODLights.zip

REM echo %INPUT%
REM echo %OUTPUT%
REM echo %RS_TOOLSRUBY%
REM echo %RS_TOOLSLIB%
REM echo %RS_PROJECT%

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --uncompressed --filter=%FILTER% --output=%OUTPUT% %INPUT%

IF %ERRORLEVEL% NEQ 0 GOTO SkipReport

echo Zip created: %OUTPUT%

:SkipReport
