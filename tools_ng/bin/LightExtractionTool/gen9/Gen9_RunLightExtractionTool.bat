@echo off
echo.
echo ---------------------------------------------------------------------------
echo                        LightExtraction GameTool Updater v 0.4
echo ---------------------------------------------------------------------------
echo.
xcopy X:\gta5\titleupdate\dev_gen9_sga\LightExtractionTool_win64_bankrelease.exe X:\gta5\tools_ng\bin\LightExtractionTool\gen9\LightExtractionTool_win64_bankrelease.exe
echo.
echo Select an option:
echo.
echo 1. Export default level
echo 2. Copy LODLights.zip to titleupdate
echo 3. Copy LODLights.zip to DLC - mpHeist
echo 4. Copy LODLights.zip to DLC - mpHeist4
echo 5. Copy LODLights.zip to DLC - mpVinewood
echo.
set M=1
set /P M=Type option number then press ENTER:
IF %M%==1 goto default
IF %M%==2 goto titleupdate
IF %M%==3 goto dlcMPHeist
IF %M%==4 goto dlcMPHeist4
IF %M%==5 goto dlcMPVinewood

echo Invalid option!
goto end
 
set ZIP_NAME=LODLights.zip
set COPY_SRC_DIR=X:\gta5\assets_gen9_sga\export\levels\gta5\

: default
set COMMAND_ARGS_DLC=""
call X:\gta5\tools_ng\bin\LightExtractionTool\gen9\RunLightExtractionTool.bat
goto end
 
:titleupdate
set COPY_DEST_DIR=X:\gta5\assets_gen9_disc\titleupdate\dev_gen9_sga\levels\gta5\
goto run

:dlcMPHeist
set COMMAND_ARGS_DLC=-extracontent=x:\gta5_dlc\mppacks\mpHeist\ -loadMapDLCOnStart -dlcPrefix=hei_ -exportlist=x:/exportlist.txt
set COPY_DEST_DIR=X:\gta5_dlc\mpPacks\mpHeist\assets_gen9_disc\export\levels\gta5\
goto run

:dlcMPHeist4
set COMMAND_ARGS_DLC=-extracontent=x:\gta5_dlc\mppacks\mpHeist4\ -loadMapDLCOnStart -dlcPrefix=mp4_ -exportList=x:\exportList.txt -exportListOnly -exportListMatchAny
set COPY_DEST_DIR=X:\gta5_dlc\mpPacks\mpHeist4\assets_gen9_disc\export\levels\gta5\
goto run

:dlcMPVinewood
set COMMAND_ARGS_DLC=-extracontent=x:\gta5_dlc\mppacks\mpVinewood\ -loadMapDLCOnStart -dlcPrefix=vw_
set COPY_DEST_DIR=X:\gta5_dlc\mpPacks\mpVinewood\assets_gen9_disc\export\levels\gta5\
goto run
 
 : run

call X:\gta5\tools_ng\bin\LightExtractionTool\gen9\RunLightExtractionTool.bat
xcopy %COPY_SRC_DIR%%ZIP_NAME% %COPY_DEST_DIR%%ZIP_NAME% /Y
explorer %COPY_DEST_DIR%

: end
pause