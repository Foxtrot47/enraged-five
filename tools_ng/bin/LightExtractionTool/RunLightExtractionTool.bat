echo off

call X:\gta5\tools_ng\bin\setenv.bat

pushd x:\gta5\tools_ng\bin\LightExtractionTool

REM --------------------------------
REM Sync to the current (CB) build
REM --------------------------------

REM p4 sync -f //depot/gta5/build/dev_ng/...@[CB]_GTAV_current

rem ====================================================
rem DoExport expects 4 parameters
rem 1. The project Root
rem 2. The executable to run
rem 3. the level name "gta5" "testlevel" etc...
rem ====================================================

call DoExport.bat %RS_PROJROOT% %RS_TOOLSBIN%\LightExtractionTool\LightExtractionTool_win64_bankrelease.exe gta5

popd

pause
