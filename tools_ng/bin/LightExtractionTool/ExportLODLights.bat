rem params
rem %1 x:\gta5
rem %2 exe name and path
rem %3 gta5 (level ID)

pushd %1\build\dev_ng

set COMMON_ARGS=-nopopups -rootdir %RS_BUILDBRANCH%\ -update=x:\gta5\titleupdate\dev_ng\ -extracontent=x:\gta5_dlc\mppacks\mpVinewood\ -loadMapDLCOnStart -dlcPrefix=vw_ -exportall -nopopups -nomtplacement -noHangDetectThread -noaudio -aggressivecleanup -nulldriver -nocloth -all_tty=fatal -all_log=fatal -lodST_log=debug3 -lodST_tty=debug3 -dlc_log=debug2 -dlc_tty=debug2 -logfile=X:\light_extraction_tool_bin.log

%2 %COMMON_ARGS% -level=%3

popd
