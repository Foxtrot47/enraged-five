echo DoExport.bat

rem ====================================================
rem DoExport expects 3 parameters
rem 1. The project Root
rem 2. The executable to run
rem 3. the level name "gta5" "testlevel" etc...
rem ====================================================

echo off

rem params
rem echo %1
rem echo %2
rem echo %3

mkdir %1\exported_LODLights\
del /f /q %1\exported_LODLights\*.*
call ExportLODLights.bat %1 %2 %3
call CheckOutLODLightzip.bat %1 %2 %3
call CreateLODLightsZip.bat %1 %2 %3

echo LOD Light Extraction Complete
