@echo off
setlocal enabledelayedexpansion
title Shutting down x64 game...

set PAUSE=%1

:: First ask R*TM to kill any processes it may be debugging.  taskkill
:: will not kill a process that is currently stopped in a debugger.
"%RS_TOOLSROOT%\bin\RockstarTargetManager\Rockstar Target Manager.exe" --killall=pc | findstr ^^

:: Error process name not found is not actually an error, redirect output.
taskkill /F /IM game_win64_* 2>&1

if "%PAUSE%"=="" (
    echo Shutting down x64 games complete.
    timeout 5
)

exit /b 0
