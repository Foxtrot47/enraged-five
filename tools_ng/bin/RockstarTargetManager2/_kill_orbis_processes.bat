@echo off
setlocal enabledelayedexpansion
title Shutting down PS4 game...

:: kill_orbis_processes [-nopause] [target ip addr]
:: If no address is passed, default target is used.

set target_ip=
set nopause=
set err=0
:getparam
if /i "%~1"=="-nopause" (
	set nopause=y
) else if not "%~1"=="" set target_ip=%~1
shift
if not "%1"=="" goto:getparam
if "%target_ip:~0,1%"=="-" set target_ip=

:: Put Windows executables in front of path to make sure that version of find is used
:: (as opposed to other versions like from Cygwin)
set PATH=%ComSpec:cmd.exe=%;%PATH%

:: Need to add Orbis SDK bin directory to path, since the path has spaces in it, and using
:: double quotes inside of the for used for the orbis-ctrl pkill does not seem to work
set orbis_bin_dir=%SCE_ROOT_DIR%\ORBIS\Tools\Target Manager Server\bin
if not exist "%orbis_bin_dir%" set orbis_bin_dir=C:\Program Files (x86)\SCE\ORBIS\Tools\Target Manager Server\bin
if not exist "%orbis_bin_dir%\orbis-ctrl.exe" echo.&&echo ERROR: PS4 SDK or support tools not installed as expected.&&set err=1&&goto:done

if "%target_ip%"=="" (
	pushd "%orbis_bin_dir%"
	for /f "usebackq" %%a in (`orbis-ctrl default`) do if "!target_ip!"=="" if not "%%a"=="" set target_ip=%%a
	set err=!errorlevel!
	popd
	if not "!err!"=="0" echo ERROR: Could not determine default PS4 console&&goto:done
) else (
	set target_ip=!target_ip:[=!
	set target_ip=!target_ip:]=!
	set nopause=y
)

title Shutting down PS4 game %target_ip%
echo Shutting down PS4 game %target_ip%...

:: First ask R*TM to kill any processes it may be debugging.  orbis-ctrl pkill
:: will not kill a process that is currently stopped in a debugger.
if not "%target_ip%"=="" (
	"%RS_TOOLSROOT%\bin\RockstarTargetManager\Rockstar Target Manager.exe" --kill=ps4:%target_ip% | findstr ^^
) else (
	"%RS_TOOLSROOT%\bin\RockstarTargetManager\Rockstar Target Manager.exe" --killall=ps4 | findstr ^^
)

pushd "%orbis_bin_dir%"
for /F "usebackq tokens=2" %%n in (`orbis-ctrl plist %target_ip% ^| findstr /i /c:"PID:"`) do if not "%%n"=="" orbis-ctrl pkill %%n %target_ip%
::set err=!errorlevel!
popd

:done
if not "%err%"=="0" (
	echo Shutting down PS4 games failed.
) else (
	echo Shutting down PS4 games complete.
)
title Shut down PS4 games %target_ip%

if "%nopause%"=="" (
	timeout 5
)

exit /b %err%
