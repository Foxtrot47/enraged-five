::
:: %RS_TOOLSBIN%\RockstarTargetManager\_connect_target_p.bat should not be directly modified.
::
:: This file gets automatically copied from %RS_CODEBRANCH%\rage\base\tools\ui\RockstarTargetManager\RockstarTargetManager\_connect_target_p.bat
:: when a new version of R*TM is released (see %RS_CODEBRANCH%\rage\base\tools\ui\RockstarTargetManager\CopyToTools.bat).
::

:: Need to add Prospero SDK bin directory to path, since the path has spaces in it, and using
:: double quotes inside of the for used for the prospero-ctrl pkill does not seem to work
set "sce_root=%SCE_ROOT_DIR%"
if "%sce_root%" == "" (
	set "sce_root=C:\Program Files (x86)\SCE"
)
set "prospero_bin_dir=%sce_root%\Prospero\Tools\Target Manager Server\bin\"
if not exist "%prospero_bin_dir%\prospero-run.exe" (
	echo "PS5 SDK or support tools not installed as expected"
	exit 1
)
PATH=%prospero_bin_dir%;%PATH%

if not exist "%exename%.elf" (
	echo Can't find PS5 executable [%exename%.elf]
	exit 1
)

:: If target was not specified, use the default target.
if "%target_ip%" == "" (
	for /F "usebackq tokens=1,2 delims=- " %%i in (`prospero-ctrl target get-default`) do if /i "%%i"=="hostname:" set target_ip=%%j
	if "!target_ip!" == "" (
		echo No default PS5 set
		exit 1
	)
)

set target_ip_bare=%target_ip%

prospero-ctrl target connect /force /target:%target_ip%
if errorlevel 1 (
	echo Failure connecting to target %target_ip%
	exit 1
)

:: Kill any currently running game.
call "%batch_dir%\_kill_prospero_processes.bat" [%target_ip%]
