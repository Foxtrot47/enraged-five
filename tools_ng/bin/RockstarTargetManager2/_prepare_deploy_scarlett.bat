::
:: %RS_TOOLSBIN%\RockstarTargetManager\_prepare_deploy_scarlett.bat should not be directly modified.
::
:: This file gets automatically copied from %RS_CODEBRANCH%\rage\base\tools\ui\RockstarTargetManager\RtmLib\_prepare_deploy_scarlett.bat
:: when a new version of R*TM is released (see %RS_CODEBRANCH%\rage\base\tools\ui\RockstarTargetManager\CopyToTools.bat).
::

@echo off
setlocal enabledelayedexpansion

set batch_dir=%~dp0
set target_ip=%~1
set build_dir=%~2
::set staging_dir=%~3
set build=%~3
REM set xdk=%~4
set full_package_name_to_validate=%~5

if "%staging_dir%"=="" set staging_dir=scarlett_loose

:: This can be modified to enable or disable validation of deployed files.
:: Set to 0 to disable validation, and 1 to enable validation.
set ENABLE_VALIDATION=0

goto :Main


:Copy
echo %1
if not exist "%build_dir%\%staging_dir%" md "%build_dir%\%staging_dir%"
:: If destination doesn't already exist, just use regular copy.  This is important
:: if the source and destination names aren't the same.  Otherwise xcopy will prompt
:: a selection between file or device, and there is no command line switch to
:: override this behaviour.
if not exist "%build_dir%\%staging_dir%\%~2" (
	copy /V "%~1" "%build_dir%\%staging_dir%\%~2" 2>&1
) else (
	xcopy /Q /R /V /Y "%~1" "%build_dir%\%staging_dir%\%~2" 2>&1
)
if errorlevel 1 exit 1
goto :EOF

:CopyIfNewer
echo %1
if not exist "%build_dir%\%staging_dir%\%~2" (
	copy /V "%~1" "%build_dir%\%staging_dir%\%~2" 2>&1
) else (
	xcopy /D /Q /R /V /Y "%~1" "%build_dir%\%staging_dir%\%~2" 2>&1
)
if errorlevel 1 exit 1
goto :EOF

:Validate
echo %1
fc /B "%~1" "\\%system_ip%\%full_package_name_to_validate%\%~2" > nul 2>&1
if errorlevel 1 (
	echo Files do not match!
	exit 1
)
goto :EOF



:Main

if not "%full_package_name_to_validate%" == "" (
	if not "%ENABLE_VALIDATION%" == "1" (
		exit /b 0
	)
	set Copy=call :Validate
	set CopyIfNewer=call :Validate
	echo Validating deployed files.
	for /F "usebackq tokens=2 delims=@" %%a in (`xbconnect /Q %target_ip% ^| findstr "SYSTEM:"`) do (
		set system_ip=%%a
	)
) else (
	set Copy=call :Copy
	set CopyIfNewer=call :CopyIfNewer
	echo Preparing "%build_dir%\%staging_dir%" for deployment.
	echo Copying deployment files to %staging_dir%
)

%Copy% "%TEMP%\rfs.dat" "rfs.dat"

%CopyIfNewer% "%build_dir%\game_scarlett_%build%.exe" "game_scarlett_%build%.exe"
%CopyIfNewer% "%build_dir%\game_scarlett_%build%.cmp" "game_scarlett_%build%.cmp"
%CopyIfNewer% "%build_dir%\game_scarlett_%build%.pdb" "game_scarlett_%build%.pdb"
rem %CopyIfNewer% "%build_dir%\game_scarlett_%build%.map" "game_scarlett_%build%.map"

REM TEMPORARY WORKAROUND - GDKs 210400 and 210401 (newest at time of writing) list multiple GDK versions on the executable, none of which are correct in some cases.
REM So we are just going to use the gameos.xvd and GameChat2.dll/.pdb files we have copied to scarlett_loose already in p4 until that bug is fixed.

REM if exist "%GameDK%%xdk%\gxdk\sideload\gameos.xvd" (
REM	%Copy% "%GameDK%%xdk%\gxdk\sideload\gameos.xvd" "gameos.xvd"
REM ) else (
REM	echo Required GDK for Scarlett %build% executable is not installed. [%xdk%]
REM	echo Will attempt to run with default configuration.
REM	echo.
REM )

REM set "sysext=%GameDK%%xdk%\GRDK\ExtensionLibraries\Xbox.Game.Chat.2.Cpp.API\Redist\CommonConfiguration\neutral\GameChat2"
REM if exist "!sysext!.dll" (
REM	%Copy% "!sysext!.dll" "GameChat2.dll"
REM	%Copy% "!sysext!.pdb" "GameChat2.pdb"
REM )

rem if not "%cmdline%"=="%cmdline:-enablesharedtex=%" (
rem	if not exist "%GameDK%\bin" (
rem		echo Error: GDK support not available.
rem	) else (
rem		for /F "usebackq delims=+" %%a in (`"%GameDK%\bin\xbconnect.exe" /q /s %target_ip%`) do set RSG_XBSX_ADDRESS=%%a
rem		if exist "%build_dir%\push_shared_textures.bat" (
rem			echo Syncing shared texture database, this might take a while...
rem			call "%build_dir%\push_shared_textures" xbsx -nodeploy
rem		) else (
rem			echo Error: Could not sync shared texture database.
rem			echo See that you have the latest %build_dir%\push_shared_textures.bat
rem		)
rem	)
rem )

exit /b 0
