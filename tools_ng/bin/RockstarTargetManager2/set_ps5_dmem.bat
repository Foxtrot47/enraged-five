@echo off
setlocal enabledelayedexpansion

set mindmem=4096
set maxdmem=10240

:: Ensures the PS5 DMEM is between the above minimum and maximum values; if already set within that range, DMEM is not changed and the console is left untouched
:: Usage:
:: set_ps5_dmem [target addr|target addr list filename]
:: - run directly to set the default kit (pass in -nopause to skip pause at end)
:: - pass in an IP address of a prospero kit to set it
:: - pass in the filename of a text file listing prospero IP addresses to set many kits
:: - in explorer, drop a text file containing addresses onto this script to set many kits
:: - with either of the last two uses, a .log file will be generated with the full results; realtime output won't be visible on screen


echo.&&echo Setting PS5 DMEM to minimum of %mindmem%MB / maximum of %maxdmem%MB...
set ret=0
set prevdmem=0
set currdmem=0
where prospero-ctrl>nul 2>&1
if errorlevel 1 set ret=1&&echo.&&echo ERROR: problem with prospero support&&goto:done

set sfile=%TEMP%\ps5settings.xml
set nfile=%TEMP%\ps5dmem.xml
set nopause=
if not "%~1"=="" set nopause=y
if /i "%~1"=="-nopause" set nopause=y&&shift

if exist "%~1" (
echo.>"%~dpn1.log"
for /f "usebackq tokens=1 eol=#" %%a in ("%~1") do call :setuptarget %%a >>"%~dpn1.log" 2>&1
) else (
call :setuptarget %~1
)
:done
echo.
if "%nopause%"=="" pause
exit /b %ret%

:setuptarget
set target=
if not "%~1"=="" (
	set target=/target:%~1
) else (
	for /F "usebackq tokens=1,2 delims=- " %%i in (`prospero-ctrl target get-default`) do if /i "%%i"=="hostname:" set target=%%j
	if "!target!" == "" set ret=1&&echo.&&echo ERROR: no prospero target specified and no default set&&goto:done
	set target=/target:!target!
)

prospero-ctrl settings export "%sfile%" %target%
if errorlevel 1 set ret=1&&echo.&&echo ERROR: unable to verify settings for %target:/target:=%&&goto:done

set setline=setting
for /f "usebackq tokens=1-13 delims== " %%a in (`findstr /i /c:0x78280100 "%sfile%"`) do for %%x in ("%%~b %%~c" "%%~d %%~e" "%%~f %%~g" "%%~h %%~i" "%%~j %%~k" "%%~l %%~m") do for /f "tokens=1,2*" %%y in (%%x) do (
	if /i "%%y"=="value" (
		set prevdmem=%%z
		set currdmem=%%z
		if %%z geq %mindmem% if %%z leq %maxdmem% goto:allset
		if %%z lss %mindmem% set currdmem=!mindmem!
		if %%z gtr %maxdmem% set currdmem=!maxdmem!
		set setline=!setline! %%y="!currdmem!"
	) else if /i not "%%y"=="name" if /i not "%%y"=="description" (
		set setline=!setline! %%y="%%z"
	)
)

echo.&&echo Connecting to target %target:/target:=%
prospero-ctrl target connect /force %target%
if errorlevel 1 set ret=1&&echo.&&echo ERROR: unable to connect to prospero target %target:/target:=%&&goto:done

echo ^<?xml version="1.0" encoding="UTF-8"?^>>"%nfile%"
findstr /i /c:"<settings" "%sfile%">>"%nfile%"
echo.  ^<%setline%/^>>>"%nfile%"
echo ^</settings^>>>"%nfile%"

prospero-ctrl settings import "%nfile%" %target%
if exist "%sfile%" del /f /q "%sfile%">nul
if exist "%nfile%" del /f /q "%nfile%">nul

:allset
set dmsg=[no change to console]
if %currdmem% gtr %prevdmem% set dmsg=[increased from %prevdmem%MB]
if %currdmem% lss %prevdmem% set dmsg=[decreased from %prevdmem%MB]
echo.&&echo PS5 [%target:/target:=%] DMEM is set to %currdmem%MB %dmsg%
echo.
goto:eof

