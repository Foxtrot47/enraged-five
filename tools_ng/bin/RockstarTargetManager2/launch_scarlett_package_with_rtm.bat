@echo off
setlocal enabledelayedexpansion

path %GameDK%\bin;%path%
set conntool=xbconnect.exe

where /Q %conntool%
if NOT [%errorlevel%] == [0] set ERR="Scarlett SDK or support tools not installed as expected" && goto :error

set batch_dir=%~dp0
set target_ip=
set build=
set rtm_launch_branch=dev_gen9_sga
set game_args=
set variant=

:getopts
set arg=%~1
if "%arg%"=="" goto gotopts
if /i "%arg%"=="-ipaddr" set target_ip=%~2&&shift&&goto :optparsed
if /i "%arg%"=="-build" set build=%~2&&shift&&goto :optparsed
if /i "%arg%"=="-variant" set variant=%~2&&shift&&goto :optparsed
set game_args=%game_args% %arg%
:optparsed
shift
goto getopts
:gotopts

if "%target_ip%"=="" for /f %%a in ('%conntool% /s') do if not "%%a"=="" set target_ip=%%a
if "%target_ip%"=="" set ERR=Could not find default console; check that you are properly connected. && goto :error
if "%RS_TOOLSROOT%"=="" set ERR=Unable to expand %%RS_TOOLSROOT%%, try running "setproj gta5 %rtm_launch_branch%" before executing this script. && goto :error

set rtm_dir=%RS_TOOLSROOT%\bin\RockstarTargetManager2

call %batch_dir%set_package_name.bat %variant%
set xbs_loosedir=\\%target_ip%\SystemScratch\Drives\Development\%packagefamilyname%
set launch_config=master
if NOT "%build%"=="" set launch_config=%build%
set target_path=%xbs_loosedir%\game_scarlett_%launch_config%.exe

title Launching Scarlett Package %launch_config% from %target_ip%

:: Check RTM is running first, and start it if its not
tasklist /fi "ImageName eq Rockstar Target Manager.exe" /fo csv 2>NUL | find /I "Rockstar Target Manager.exe">NUL
if NOT "%ERRORLEVEL%"=="0" (
    echo Starting RTM...
    start "" "%rtm_dir%\Rockstar Target Manager.exe" --gui
    rem Give RTM a little bit of time to launch
    ping -w 3000 -n 2 127.0.0.1 >nul
)

:: Clang builds usually have name <build>_Clang, but AppId doesn't need '_', so drop it
:: Regular builds won't be affected
set build=%build:_=%
"%rtm_dir%\Rockstar Target Manager.exe" -DXsxLooseDir=%xbs_loosedir% -DTargetPath=%target_path% -DPackageFullName=%fullpackagename% -DPackageFamilyName=%packagefamilyname% -DAppId=%devappidprefix%%build% --launch=scarlett:%target_ip% %rtm_launch_branch%/package %game_args% | findstr ^^
goto :end

:error
echo.
echo Error Launching Scarlett Package!
echo.
echo. ^> %ERR%
echo.
exit /b 1

:end
exit /b 0