@echo off
setlocal enabledelayedexpansion
set batch_dir=%~dp0

:: Respect caller's environment variables.  Only call setenv.bat if RS_BUILDBRANCH is missing.
if "%RS_BUILDBRANCH%" == "" (
	call "%batch_dir%\..\setenv.bat" > nul
)

:: Ignore whatever setenv.bat may have set for RS_TOOLSROOT.  RS_TOOLSROOT should be relative to this batchfile.
pushd "%batch_dir%\..\.."
set RS_TOOLSROOT=%cd%
popd

:: Put Windows executables in front of path to ensure that version of ping, timeout, etc are used
:: (as opposed to other versions like from Cygwin)
set PATH=%ComSpec:cmd.exe=%;%PATH%

:: Check GDK install
if "%GameDK%"=="" call :error "Scarlett GDK not installed at all"
set tooldir=%GameDK%\bin\
if not exist "%tooldir%" call :error "Scarlett GDK not installed as expected"

title Terminating Game on Scarlett

:: The next optional command line arg changes the way the batchfile exits
set pause=%1

echo.&&echo Checking default console...

"%tooldir%\xbconnect.exe" /n >nul
if not %errorlevel%==0 call :error "No default Scarlett console set"
set toolsaddr_accesskey=
for /f "usebackq" %%a in (`"%tooldir%\xbconnect.exe" /n /b`) do if !errorlevel!==0 if not "%%a"=="" set toolsaddr_accesskey=%%a
if "%toolsaddr_accesskey%"=="" call :error "No default Scarlett console set"
for /F "tokens=1* delims=+" %%a in ("%toolsaddr_accesskey%") do set toolsaddr=%%a
ping %toolsaddr% -n 1 -w 1000 >nul
if not %errorlevel%==0 call :error "Could not connect to default Scarlett %toolsaddr%"
set consoleaddr=
for /f "usebackq" %%a in (`"%tooldir%\xbconnect.exe" /q /s /ws:2`) do if !errorlevel!==0 if not "%%a"=="" set consoleaddr=%%a
if /i "%consoleaddr%"=="Refer" call :error "Problem connecting to default Scarlett console"
if "%consoleaddr%"=="" call :error "No default Scarlett console set"
set target_ip=%toolsaddr_accesskey%

if /i "%target_ip%"=="Refer" call :error "Problem connecting to Scarlett console"
title Terminating Game on Scarlett %target_ip%
ping %target_ip% -n 1 -w 1000 >nul
if not %errorlevel%==0 call :error "Could not connect to Scarlett %target_ip%"
echo.&&echo Scarlett Console Status
"%tooldir%\xbconnect.exe" %target_ip% /q /ws:2
if not %errorlevel%==0 call :error "Could not connect to Scarlett %target_ip%"

:: Walk up the directory hierarchy until we find a .tools file.
:: From that, we can then extract the RS_TOOLSROOT.
:: This has to be done here so that we can determine how to actually call R*TM.
if "%RS_TOOLSROOT%" == "" (
	pushd "%batch_dir%"
	set prev_dir=!cd!
	:check_pwd_for_dot_tools
	if exist .tools (
		goto found_dot_tools
	)
	cd ..
	set curr_dir=!cd!
	if !curr_dir! == !prev_dir! (
		call :error "RS_TOOLSROOT not set, and no .tools file in directory hierarchy.  Unable to continue"
	)
	set prev_dir=!curr_dir!
	goto check_pwd_for_dot_tools
	:found_dot_tools
	for /F "usebackq delims== tokens=1*" %%a in (`type .tools`) do set RS_TOOLSROOT=%%b
	cd %~dp0
	popd
)

:: First ask R*TM to kill any processes it may be debugging.  xbapp terminate
:: will not kill a process that is currently stopped in a debugger.
if not "%target_ip%"=="" (
	"%RS_TOOLSROOT%\bin\RockstarTargetManager\Rockstar Target Manager.exe" --kill=xsx:%target_ip% | findstr ^^
) else (
	"%RS_TOOLSROOT%\bin\RockstarTargetManager\Rockstar Target Manager.exe" --killall=xsx | findstr ^^
)

call "%RS_BUILDBRANCH%\xbs_scripts\set_package_name.bat"

echo.&&echo Checking on active games...

ping 127.0.0.1 -n 1 -w 1000 >nul

"%tooldir%\xbconnect.exe" %target_ip% /q /wt:1 >nul
if not %errorlevel%==0 goto :notrunning
echo.&&echo Terminating active games...

:: Suspend the app first so we shutdown in a structured manner.
"%tooldir%"\xbapp /x%target_ip% suspend %fullpackagename% > nul

:: Sleep for 2 seconds.
ping 127.0.0.1 -n 1 -w 2000 >nul

"%tooldir%"\xbapp /x%target_ip% terminate %fullpackagename% > nul

:notrunning
echo.&&echo Done.
echo.&&echo Completed termination of games on Scarlett %target_ip%
if not "%pause%"=="" timeout 5
exit /b 0
goto:eof

:error
echo.
title Failed running Scarlett Tool
echo ERROR:
echo. ^> %~1
echo.
if not "%pause%"=="" pause
exit /b 1