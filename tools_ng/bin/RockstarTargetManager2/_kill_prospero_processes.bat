@echo off
setlocal enabledelayedexpansion
title Shutting down PS5 game...

:: kill_prospero_processes [-nopause] [target ip addr]
:: If no address is passed, default target is used.

set target_ip=
set nopause=
set err=0
:getparam
if /i "%~1"=="-nopause" (
	set nopause=y
) else if not "%~1"=="" set target_ip=%~1
shift
if not "%1"=="" goto:getparam
if "%target_ip:~0,1%"=="-" set target_ip=

:: Put Windows executables in front of path to make sure that version of find is used
:: (as opposed to other versions like from Cygwin)
set PATH=%ComSpec:cmd.exe=%;%PATH%

:: Need to add Prospero SDK bin directory to path, since the path has spaces in it, and using
:: double quotes inside of the for used for the prospero-ctrl pkill does not seem to work
set prospero_bin_dir=%SCE_ROOT_DIR%\Prospero\Tools\Target Manager Server\bin
if not exist "%prospero_bin_dir%" set prospero_bin_dir=C:\Program Files (x86)\SCE\Prospero\Tools\Target Manager Server\bin
if not exist "%prospero_bin_dir%\prospero-ctrl.exe" echo.&&echo ERROR: PS5 SDK or support tools not installed as expected.&&set err=1&&goto:done

if "%target_ip%"=="" (
	pushd "%prospero_bin_dir%"
	for /f "usebackq tokens=1,2 delims=- " %%a in (`prospero-ctrl target get-default`) do if "!target_ip!"=="" if /i "%%a"=="hostname:" set target_ip=%%b
	set err=!errorlevel!
	popd
	if not "!err!"=="0" echo ERROR: Could not determine default PS5 console&&goto:done
) else (
	set target_ip=!target_ip:[=!
	set target_ip=!target_ip:]=!
	set nopause=y
)

title Shutting down PS5 game %target_ip%
echo Shutting down PS5 game %target_ip%...

:: First ask R*TM to kill any processes it may be debugging.  prospero-ctrl pkill
:: will not kill a process that is currently stopped in a debugger.
if not "%target_ip%"=="" (
	"%RS_TOOLSROOT%\bin\RockstarTargetManager\Rockstar Target Manager.exe" --kill=prospero:%target_ip%
) else (
	"%RS_TOOLSROOT%\bin\RockstarTargetManager\Rockstar Target Manager.exe" --killall=prospero
)

pushd "%prospero_bin_dir%"
for /F "usebackq tokens=2 delims=- " %%n in (`prospero-ctrl process list /target:%target_ip% ^| findstr /i /c:"Id:"`) do if not "%%n"=="" prospero-ctrl process kill /process:%%n /target:%target_ip%&&goto:loopcomplete
:loopcomplete
::set err=!errorlevel!
popd

:done
if not "%err%"=="0" (
	echo Shutting down PS5 games failed.
) else (
	echo Shutting down PS5 games complete.
)
title Shut down PS5 games %target_ip%

if "%nopause%"=="" (
	timeout 5
)

exit /b %err%
