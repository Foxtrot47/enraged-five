:: Internal batchfile for finding a particular command line option by scanning
:: the passed in command line and recursively checking response files.
::
:: Input
::  %find_arg%  command line arg to search for (no - prefix)
::  %platform%  durango, orbis, prospero, scarlett, or win64
::  %build%     final, profile, release, bankrelease, beta or debug
::  %instance_id%  the instance number [1-9] of this process (usually pc only)
::  %argset%    the argset index to apply [0-9]
::  %*          the command line to search
::
:: Output (printed to stdout)
::  found_arg=XXX               the argument that was found
::  found_arg_value=XXX         the value specified for the argument
::  found_arg_message=XXX       a message to say where the argument was found
::
@echo off
setlocal EnableDelayedExpansion

set prefix_list=
set platform_prefix=
for %%a in ( "durango du" "orbis or" "win64 pc" "linux gg ye lx" "yeti gg ye lx" "ggp gg ye lx" "prospero pr" "p pr" "scarlett sc" ) do (
	for /f "tokens=1,*" %%b in ( %%a ) do (
		if /i "%%b" == "%platform%" (
			set platform_prefix=%%c
			set prefix_list=%%c !prefix_list!
		)
	)
)
if "%platform_prefix%" == "" (
	rem :: Since this batchfile is called with stdout redirected, echo to stderr
	echo ERROR: _scan_cmdline.bat called with unknown %%platform%%="%platform%" 1>&2
	pause 1>&2
	exit /b 1
)

set build_prefix=
for %%a in ( "release re" "profile pf re" "final fi re" "beta be bk" "debug de bk" "bank bk" "asan as" "master ma re" ) do (
	for /f "tokens=1,*" %%b in ( %%a ) do (
		rem :: If %%b is a substring of %build%, then set build_prefix=%%c
		if /i not "%build%" == "!build:%%b=!" (
			set build_prefix=%%c
			set prefix_list=%%c !prefix_list!
		)
	)
)
if "%build_prefix%" == "" (
	echo ERROR: _scan_cmdline.bat called with unknown %%build%%="%build%" 1>&2
	pause 1>&2
	exit /b 1
)

if not "%instance_id%"=="" (
set prefix_list=i%instance_id:~0,1% %prefix_list%
)
if not "%argset%"=="" (
set argset=%argset:,= %
for %%a in (!argset!) do set prefix_list=a%%a !prefix_list!
)

goto check_prefixes_endfunc
:check_prefixes_func
	set shifted_param=%1
	if "%shifted_param:~0,1%" == "!" (
		if "%shifted_param:~3,1%" == ":" (
			for %%p in (%prefix_list%) do (
				if /i "%shifted_param:~1,2%" == "%%p" (
					set is_arg=0
					goto :eof
				)
			)
			call :check_prefixes_func %shifted_param:~4%
		)
	) else (
		if "%shifted_param:~2,1%" == ":" (
			for %%p in (%prefix_list%) do (
				if /i "%shifted_param:~0,2%" == "%%p" (
					call :check_prefixes_func %shifted_param:~3%
					goto :eof
				)
			)
			set is_arg=0
		)
	)
	goto :eof
:check_prefixes_endfunc

set found_arg=
set found_arg_value=
set found_arg_message=
set next_is_arg_value=0
goto find_arg_endfunc
:find_arg_func
	set param=%2

	if "%next_is_arg_value%" == "1" (
		set next_is_arg_value=0
        rem :: Check arg doesn't begin with \055
		if "%param:~0,1%" neq "-" (
    	    rem :: Check arg doesn't begin with \226
			if "%param:~0,1%" neq "�" (
				set found_arg_value=%param%
			)
		)
	)

	set is_arg=0
	:: Check "\055" else "\226"
	if "%param:~0,1%" == "-" (
		set is_arg=1
	) else if "%param:~0,1%" == "�" (
		set is_arg=1
	)

	if "%is_arg%" == "1" (
		call :check_prefixes_func %param:~1%
	)
	if "%is_arg%" == "1" (
		if /i "%shifted_param%" neq "%find_arg%" (
			set is_arg=0
		)
	)

	if "%is_arg%" == "1" (
		set found_arg=-%shifted_param%
		set found_arg_message=!%1!
		if "%3" == "" (
			set next_is_arg_value=1
		) else (
			set found_arg_value=%3
		)
		set found_arg_msg=!%1!
	) else if "!param:~0,1!" == "@" (
		set file=%param:~1%
		if exist "!file!" (
			set in_file=in !file!
            rem :: We need to treat " as a delimiter otherwise expanding if "%arg%" ... explodes since %arg% has a " in it.
            rem :: Quoting a " requires this horrible syntax (see http://stackoverflow.com/a/13217838).
            rem :: Two " are quoted here to avoid messing up Emacs' syntax highlighting.
			for /f tokens^=1^,2^ delims^=^=^"^" %%i in (!file!) do (call :find_arg_func in_file %%i %%j)
		)
	)
	goto :eof
:find_arg_endfunc


set "on_command_line=on command line"
for %%i in (%*) do call :find_arg_func on_command_line %%i


echo found_arg=%found_arg%
echo found_arg_value=%found_arg_value%
echo found_arg_message=%found_arg_message%
