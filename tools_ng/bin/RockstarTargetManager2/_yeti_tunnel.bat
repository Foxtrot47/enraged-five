@echo off
setlocal enabledelayedexpansion

set selfname=%~nx0

set rfsname=c:\rfs.dat
if not exist "%rfsname%" set rfsname=%TEMP%\rfs.dat
if not exist "%rfsname%" (
echo Could not find "%rfsname%".&&goto:error
)

for /f %%A in (%rfsname%) do set localip=%%A
set instanceid=
set ttyport=
set msgport=

:getopts
if "%~1"=="" goto :gotopts
if /i "%~1"=="-instanceid" set instanceid=%~2
if /i "%~1"=="-ttyport" set ttyport=%~2
if /i "%~1"=="-msgport" set msgport=%~2
shift
goto :getopts

:gotopts
:: ssh-flag port forwarding tools:
:: Rocky 31313
:: Rag 2000, 60000, 60001, 60002, 60010
:: Memvis 8798
:: RTM TTY and Message Ports are provided as arguments
:: StreamingVisualize 8799

echo.&&echo Debug1: %selfname% Digging Tunnel.....

if "%instanceid%"=="" echo Target Gamelet Id is not provided!&&goto:error

set tunnel_cmd=ggp ssh shell ^
--instance %instanceid% ^
--ssh-flag "-L31313:localhost:44750" ^
--ssh-flag "-R2000:%localip%:2000" ^
--ssh-flag "-R60000:%localip%:60000" ^
--ssh-flag "-R60001:%localip%:60001" ^
--ssh-flag "-R60002:%localip%:60002" ^
--ssh-flag "-R60010:%localip%:60010" ^
--ssh-flag "-R8798:%localip%:8798" ^
--ssh-flag "-R8799:%localip%:8799"

if not "%ttyport%"=="" (
    if not "%msgport%"=="" (
        set tunnel_cmd=%tunnel_cmd% --ssh-flag "-R%ttyport%:%localip%:%ttyport%" --ssh-flag "-R%msgport%:%localip%:%msgport%"
    ) else (
        echo Warning: %selfname% -msgport not specified. If launching from RTM, this param is required.
    )
) else (
    echo Warning: %selfname% -ttyport not specified. If launching from RTM, this param is required.
)

set tunnel_cmd=%tunnel_cmd% --ssh-flag "-vvvnNT"

start "Cloud Tunnel" %tunnel_cmd%

exit /b 0

:: -----------------------------------------------------------------------------
:: error
:: -----------------------------------------------------------------------------
:error
echo.&&echo Error: %selfname% Tunnel Digging has failed
exit /b 1
