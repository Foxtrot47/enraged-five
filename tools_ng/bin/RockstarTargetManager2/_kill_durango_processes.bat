@echo off
setlocal

set batch_dir=%~dp0

:: Respect caller's environment variables.  Only call setenv.bat if RS_BUILDBRANCH is missing.
if "%RS_BUILDBRANCH%" == "" (
	call "%batch_dir%\..\setenv.bat" > nul
)

:: Ignore whatever setenv.bat may have set for RS_TOOLSROOT.  RS_TOOLSROOT should be relative to this batchfile.
pushd "%batch_dir%\..\.."
set RS_TOOLSROOT=%cd%
popd

if not "%DurangoXDK%"=="" (
	call "%RS_BUILDBRANCH%\xbo_scripts\terminate_game.bat" %*
)
