::
:: %RS_TOOLSBIN%\RockstarTargetManager\_prepare_deploy_durango.bat should not be directly modified.
::
:: This file gets automatically copied from %RS_CODEBRANCH%\rage\base\tools\ui\RockstarTargetManager\RtmLib\_prepare_deploy_durango.bat
:: when a new version of R*TM is released (see %RS_CODEBRANCH%\rage\base\tools\ui\RockstarTargetManager\CopyToTools.bat).
::

@echo off
setlocal enabledelayedexpansion

set batch_dir=%~dp0
set target_ip=%~1
set build_dir=%~2
set build=%~3
set xdk=%~4
set full_package_name_to_validate=%~5

:: This can be modified to enable or disable validation of deployed files.
:: Set to 0 to disable validation, and 1 to enable validation.
set ENABLE_VALIDATION=0

goto :Main


:Copy
echo %1
:: If destination doesn't already exist, just use regular copy.  This is important
:: if the source and destination names aren't the same.  Otherwise xcopy will prompt
:: a selection between file or device, and there is no command line switch to
:: override this behaviour.
if not exist "%build_dir%\xbo_loose\%~2" (
	copy /V "%~1" "%build_dir%\xbo_loose\%~2" 2>&1
) else (
	xcopy /Q /R /V /Y "%~1" "%build_dir%\xbo_loose\%~2" 2>&1
)
if errorlevel 1 exit 1
goto :EOF

:CopyIfNewer
echo %1
if not exist "%build_dir%\xbo_loose\%~2" (
	copy /V "%~1" "%build_dir%\xbo_loose\%~2" 2>&1
) else (
	xcopy /D /Q /R /V /Y "%~1" "%build_dir%\xbo_loose\%~2" 2>&1
)
if errorlevel 1 exit 1
goto :EOF

:Validate
echo %1
fc /B "%~1" "\\%system_ip%\%full_package_name_to_validate%\%~2" > nul 2>&1
if errorlevel 1 (
	echo Files do not match!
	exit 1
)
goto :EOF



:Main

if not "%full_package_name_to_validate%" == "" (
	if not "%ENABLE_VALIDATION%" == "1" (
		exit /b 0
	)
	set Copy=call :Validate
	set CopyIfNewer=call :Validate
	echo Validating deployed files.
	for /F "usebackq tokens=2 delims=@" %%a in (`xbconnect /Q %target_ip% ^| findstr "SYSTEM:"`) do (
		set system_ip=%%a
	)
) else (
	set Copy=call :Copy
	set CopyIfNewer=call :CopyIfNewer
	echo Preparing "%build_dir%\xbo_loose" for deployment.
	echo Copying deployment files to xbo_loose
)

%Copy% "%TEMP%\rfs.dat" "rfs.dat"

if exist "%build_dir%\xbo_scripts\appxmanifest_%build%.xml" (
	%Copy% "%build_dir%\xbo_scripts\appxmanifest_%build%.xml" "AppxManifest.xml"
	%Copy% "%build_dir%\xbo_scripts\appdata_%build%.bin"      "appdata.bin"
) else (
	if exist "%build_dir%\xbo_scripts\appxmanifest_default.xml" (
		%Copy% "%build_dir%\xbo_scripts\appxmanifest_default.xml" "AppxManifest.xml"
	)
	if exist "%build_dir%\xbo_scripts\appdata_default.bin" (
		%Copy% "%build_dir%\xbo_scripts\appdata_default.bin"      "appdata.bin"
	)
)

%CopyIfNewer% "%build_dir%\game_durango_%build%.exe" "game_durango_%build%.exe"
%CopyIfNewer% "%build_dir%\game_durango_%build%.cmp" "game_durango_%build%.cmp"
%CopyIfNewer% "%build_dir%\game_durango_%build%.pdb" "game_durango_%build%.pdb"
%CopyIfNewer% "%build_dir%\game_durango_%build%.map" "game_durango_%build%.map"

if exist "%build_dir%\pgosweep.exe" (
	%CopyIfNewer% "%build_dir%\pgosweep.exe" "pgosweep.exe"
	%CopyIfNewer% "%build_dir%\pgort140.dll" "pgort140.dll"
)

if exist "%DurangoXDK%%xdk%\sideload\era.xvd" (
	%Copy% "%DurangoXDK%%xdk%\sideload\era.xvd" "era.xvd"
) else (
	echo Required XDK for Xbox One %build% executable is not installed. [%xdk%]
	echo Will attempt to run with default configuration.
	echo.
)

set "sysext=%PROGRAMFILES(X86)%\Microsoft SDKs\Durango.%xdk%\v8.0\ExtensionSDKs\Xbox Services API\8.0\Redist\CommonConfiguration\neutral\Microsoft.Xbox.Services"
if exist "%sysext%.dll" (
	%Copy% "%sysext%.dll" "Microsoft.Xbox.Services.dll"
	%Copy% "%sysext%.pdb" "Microsoft.Xbox.Services.pdb"
)

:: XDKs prior to 180200 used Microsoft.Xbox.GameChat.dll.  From 180200 onwards, they use GameChat2.dll.
set /a xdk_diff=%xdk%-180200
if "%xdk_diff:~0,1" == "-" (
	set "sysext=%PROGRAMFILES(X86)%\Microsoft SDKs\Durango.%xdk%\v8.0\ExtensionSDKs\Xbox GameChat API\8.0\Redist\CommonConfiguration\neutral\Microsoft.Xbox.GameChat"
	if exist "!sysext!.dll" (
		%Copy% "!sysext!.dll" "Microsoft.Xbox.GameChat.dll"
		%Copy% "!sysext!.pdb" "Microsoft.Xbox.GameChat.pdb"
	)
) else (
	set "sysext=%PROGRAMFILES(X86)%\Microsoft SDKs\Durango.%xdk%\v8.0\ExtensionSDKs\Xbox.Game.Chat.2.Cpp.API\8.0\Redist\CommonConfiguration\neutral\GameChat2"
	if exist "!sysext!.dll" (
		%Copy% "!sysext!.dll" "GameChat2.dll"
		%Copy% "!sysext!.pdb" "GameChat2.pdb"
	)
)

exit /b 0
