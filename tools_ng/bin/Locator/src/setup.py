from distutils.core import setup
import py2exe
import os

setup(
	console=['Locator.py'],
	options={
		"py2exe": {
			"excludes":["email", "email.Utils"],
			"dist_dir":os.getenv("RS_PROJROOT") + "\\tools\\bin\\Locator\\bin"
			}
		},
    zipfile="Locator.zip"
)
