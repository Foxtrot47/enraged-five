#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Localization searcher, aka LOCator
# Get it? Nah. Nobody does
# Maintained/Created by: Jeremy Tudisco



import os, sys,string,codecs, glob
import time, re, argparse, msvcrt
from colorama import init, Fore,Back

TEXT_KEY_SIZE = 16
MAX_LINES = 45
MAX_COLS = 120

# Set up window and fonty code page
init()

commentFilter = re.compile(r'\{[^}]+\}', re.UNICODE)
SystemRunVersion = False
TotalLinesOfOutput = 0
TotalResults = 0

DEFAULT_PATH = os.environ["RS_PROJROOT"] + "\\assets\\GameText\\American"



def colorCode( result, searchString ):
	if args.caseinsensitive:
		i = result.lower().find(searchString)
	else:
		i = result.find(searchString)

	# check if the result is in the string, and so, slice in the color codes
	if i != -1:
		lt = len(searchString)
		return True, result[:i] + Back.YELLOW + result[i:i+lt] + Back.RESET + result[i+lt:]
	return False, result


def findFileDirect( filePath, searchString, encoding ):
	bPrintedHeader = False
	global TotalLinesOfOutput
	global TotalResults

	with codecs.open(filePath,'r',encoding) as inputFile:
		wholeFile = inputFile.read()
		wholeFileLower = wholeFile

		# do the whole thing in lower case for insensitive searches
		if args.caseinsensitive:
			wholeFileLower = wholeFileLower.lower()

		lineCount = 0
		nextTagIndexStart = 0

		# check the file for a match
		# then back search for the string tag it applies to
		# and the string it includes
		index = wholeFileLower.find(searchString)
		while index != -1:
			tagIndexStart = max(wholeFileLower.rfind(u"[",0,index),0)
			tagIndexEnd = wholeFileLower.find(u"]", tagIndexStart)
			
			# we COULD try to snug this along as we go, but it came off.
			# now we just count from the current line all the way back to the start
			lineCount = wholeFile.count(u"\n", None, tagIndexStart+1)+1
			nextTagIndexStart = wholeFile.find(u"[", tagIndexEnd)


			lastTag = wholeFile[tagIndexStart:tagIndexEnd+1].strip()
			lastLine = wholeFile[tagIndexEnd+1:nextTagIndexStart]

			# trim comments
			if not args.includecomments:
				lastLine = commentFilter.sub(r"", lastLine)
			
			FoundTag = False
			FoundLine = False
			[FoundTag, lastTag] = colorCode(lastTag, searchString)
			[FoundLine, lastLine] = colorCode(lastLine.strip(), searchString)		

			# May have had the result be in a comment, so filter that out
			if FoundTag or FoundLine and tagIndexStart != 0:
				if not bPrintedHeader:
					print ""
					print "------------------------------------------------------"
					print filePath
					print ""
					bPrintedHeader = True
					TotalLinesOfOutput += 4
				# tilde at the front is because line numbers are currently off a bit, so it's "approximate"
				newOutput = u"~line {lineNum:>5}: {tag:<{width}}  {string}".format(lineNum=lineCount, tag=lastTag, string=lastLine.encode("utf-8", errors='replace'), width=TEXT_KEY_SIZE+2)
				print newOutput 
				TotalLinesOfOutput += (len(newOutput) / MAX_COLS)+1
				TotalResults += 1
			
				# Fake "MORE" style behavior if the user didn't use the console edition
				if SystemRunVersion and TotalLinesOfOutput % MAX_LINES == 0:
					print "-- MORE --"
					msvcrt.getch()

			index = wholeFileLower.find(searchString, nextTagIndexStart)



def findInFile( filePath, searchString ):
#	if any(word in filePath for word in ['russian', 'polish','japanese','korean']):
#		encoding = 'utf_16_le'
#	elif 'xml' in filePath:
#		encoding = 'utf-8-sig'
#	else:
#		encoding = 'cp1252'
	encodingList = ['utf-8-sig', 'utf-8', 'windows-1252']

	for encoding in encodingList:

		try:
			findFileDirect(filePath, searchString, encoding)

		except UnicodeDecodeError as e:
			#print "\n",e, "in ", filePath
			#print "\nYou may need to look at this in a hex editor\n"
			continue
		except IOError as e:
			print e, " in ", filePath
	#	except:
	#		print "Unexpected error:", sys.exc_info()[0], " in ", filePath
	#		return
		else:
			return
	print "Was unable to find an appropriate codec for %s!" % filePath

#############################################################################


parser = argparse.ArgumentParser(description="Search localization files for given keywords/phrases")
parser.add_argument('-i', '--caseinsensitive', help="case insensitive search (default is sensitive)", action='store_true')
parser.add_argument('-c', '--includecomments', help="include \{comments\} in the search results", action='store_true')
parser.add_argument('-p', '--path', help="specify a path to search all subfolders for (defaults to {})".format(DEFAULT_PATH), action='append')
parser.add_argument('searchterms', help="text to search for. If omitted, will prompt and ask the user, as well as provide --MORE-- functionality", nargs=argparse.REMAINDER)
args = parser.parse_args()

# sanitize the arguments
if args.searchterms:
	searchString = u' '.join(args.searchterms)
else:
	# no search terms? engage user-friendly mode
	os.system("mode CON: COLS={} LINES={}".format(MAX_COLS, MAX_LINES))
	searchString = raw_input("Enter string to search for: ")
	searchString = unicode(searchString)
	SystemRunVersion = True
	args.caseinsensitive = True

# apply insensitivity if desired
if args.caseinsensitive:
	searchString = searchString.lower()

# set up search path
pathList = args.path if args.path else [DEFAULT_PATH]


# actually perform the search
for path in pathList:
	for root, dirs, files in os.walk( path ):
		for curFile in files:
			if curFile.lower().endswith(".txt"):
				filePath = os.path.join(root, curFile)
				findInFile( filePath, searchString )



#All done! Display the results
Plurality = u"s"
			
if TotalResults == 0:
	TotalResults = u"No"
elif TotalResults == 1:
	Plurality = u""

print ""
print ""
print "Search Complete! {} Result{} Found".format(TotalResults, Plurality)