rem *** Used to create a Python exe 

SET OUTPATH=%RS_TOOLSROOT%\tools\bin\Locator\bin\

rem ***** check out the output files from perforce
p4 edit %OUTPATH%

rem ***** get rid of all the old files in the build folder
rd /S /Q build bin

rem ***** create the exe
c:\Python27\python setup.py py2exe

rem ***** Get unchanged stuff out of the changelist
p4 revert -a %OUTPATH%\*.*

PAUSE
