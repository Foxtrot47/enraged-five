import subprocess
import os
import json
import platform
from datetime import datetime

try:
    from tkinter import *
    from tkinter import filedialog
except ImportError:
    from Tkinter import *
    import tkFileDialog as filedialog
 
window = Tk()
 
window.title("GTAV Helper (py" + platform.python_version() + ")")
 
window.geometry('350x110')

runColour = "#c4e3ff"
stopColour = "#ffc4c4"

homeDir = os.path.join(r'C:', os.environ["HOMEPATH"])

configFile = os.path.join(homeDir, r'GTA5LauncherConfig.json')
configData = {}
configData['vs2015exe'] = r"C:/Program Files (x86)/Microsoft Visual Studio 14.0/Common7/IDE/devenv.exe"
configData['vs2017exe'] = r"C:/Program Files (x86)/Microsoft Visual Studio/2017/Professional/Common7/IDE/devenv.exe"
configData['configOptions'] = ["beta", "bankrelease"]
configData['buildConfig'] = configData['configOptions'][0]
configData['useLiveBranch'] = False
configData['orbisIps'] = []
configData['rageScriptEditor'] = r"X:/gta5/tools_ng/bin/RageScriptEditor/ragScriptEditor.exe"

def Load_Config():
    global configData
    with open(configFile, "r") as outfile:
        loadedData = json.load(outfile)
        for key, value in loadedData.items():
            configData[key] = value

def Save_Config():
    global configData
    with open(configFile, "w+") as outfile:
        json.dump(configData, outfile, indent=4, sort_keys=True)

if not os.path.exists(configFile):
    Save_Config()
else:
    Load_Config()

logDir = os.path.join(homeDir, r'Desktop/GTA5Logs')

orbisctrl = os.path.join(os.environ.get(r'SCE_ROOT_DIR'), "ORBIS/Tools/Target Manager Server/bin/orbis-ctrl")
orbisAppName = "Grand Theft Auto V"

buildConfigVar = StringVar(master=window)
buildConfigVar.set(configData['buildConfig'])
def Update_BuildConfig(*args):
    configData['buildConfig'] = buildConfigVar.get()
    Save_Config()
buildConfigVar.trace('w', Update_BuildConfig)

useLiveBranchVar = BooleanVar(master=window)
useLiveBranchVar.set(configData['useLiveBranch'])
def Update_UseLiveBranch(*args):
    configData['useLiveBranch'] = useLiveBranchVar.get()
    Save_Config()
useLiveBranchVar.trace('w', Update_UseLiveBranch)

def Resolve_DevPath(path):
    print("Resolving: " + path)
    if(useLiveBranchVar.get()):
        path = path.replace("dev_ng", "dev_ng_live")
    print("Resolved to: " + path)
    return path

def Resolve_BatchFile(platform, config):
    return Resolve_DevPath("X:/gta5/titleupdate/dev_ng/game_" + platform + "_" + config + ".bat")

def Resolve_LogPath(platform, postfix=""):
    path = os.path.join(logDir, datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + "_" + platform + "_" + buildConfigVar.get() + postfix)
    if not os.path.exists(path):
        os.makedirs(path)
    return path

def Create_TempBat(platform, config):
    # Create a tempory batch file similar to ones like "game_orbis_beta.bat"
    # This allows us to set nopause
    global homeDir
    fileName = os.path.join(homeDir, "temp.bat")
    with open(fileName, "w+") as outfile:
        outfile.write(
"""
@echo off
set batname=game_""" + platform + """_""" + config + """
set nopause=1
cd /d \"""" + Resolve_DevPath("X:/gta5/titleupdate/dev_ng/") +"""\"
call _launchgame.bat %*
"""
        )
        outfile.close()
    return fileName

def Get_LoadSlnBatPath():
    return Resolve_DevPath("X:/gta5/src/dev_ng/game/VS_Project/load_sln_unity.bat")

def Get_ScriptProjPath():
    return Resolve_DevPath("X:/gta5/script/dev_ng/singleplayer/GTA5_SP.scproj")

def Open_2015():
    Load_Config()
    subprocess.call([Get_LoadSlnBatPath(), "game_2015_unity.sln", configData['vs2015exe']])
    return

def Open_2012():
    Load_Config()
    subprocess.call([Get_LoadSlnBatPath(), "game_2012_unity.sln", configData['vs2017exe']])
    return

def Open_VSCode():
    Load_Config()
    subprocess.call("code X:/gta5/", shell=True)
    return

def Edit_Config():
    Load_Config()
    subprocess.call("code " + configFile, shell=True)

def Open_Script_Editor():
    Load_Config()   
    subprocess.Popen([configData['rageScriptEditor'], Get_ScriptProjPath()])
    return

def Run_Orbis():
    Load_Config()
    orbisIps = configData["orbisIps"]
    if len(orbisIps) == 0:
        args = [
            r'-or:logfile=' + os.path.join(Resolve_LogPath("orbis", "_DEFAULT"), r'Console.log')
        ]
        subprocess.Popen([Resolve_BatchFile("orbis", buildConfigVar.get()), ' '.join(map(str, list))])
        return

    for ip in orbisIps:
        args = [
            r'[' + ip + r']',
            r'-or:logfile=' + os.path.join(Resolve_LogPath("orbis", "_" + ip), ip + r'_Console.log')
        ]
        subprocess.Popen([Resolve_BatchFile("orbis", buildConfigVar.get()), ' '.join(map(str, list))])

def Stop_Orbis():
    Load_Config()
    orbisIps = configData["orbisIps"]
    if len(orbisIps) == 0:
        command = '"' + orbisctrl + '" ' + 'kill-app "' + orbisAppName + '"'
        subprocess.call(command, shell=True)
        return

    for ip in orbisIps:
        command = '"' + orbisctrl + '" ' + 'kill-app "' + orbisAppName + '" ' + ip
        subprocess.call(command, shell=True)




x64Button = None
x64Count = 0
x64ButtonText = StringVar()

def Update_x64Count(value=-1, add=0):
    global x64Count 
    if value >= 0: x64Count = value
    x64Count = x64Count + add
    if x64Count > 0:
        x64ButtonText.set("Run Win64 [" + str(x64Count) + "]")
    else:
        x64ButtonText.set("Run Win64")

Update_x64Count(x64Count)

def Run_x64():
    Load_Config()
    global logDir
    global x64Count
    Update_x64Count(add=1)

    batFile = Create_TempBat("win64", buildConfigVar.get())

    args = [
        r'-processinstance=' + str(x64Count),
        r'-logfile=' + os.path.join(Resolve_LogPath("x64", "_p" + str(x64Count)), r'instance' + str(x64Count) + r'_Console.log')
    ]
    
    subprocess.call(batFile + " " + " ".join(args))
    os.remove(batFile)    

def Stop_x64():
    Load_Config()
    Update_x64Count(value=0)
    os.system("taskkill /f /im game_win64_" + buildConfigVar.get() + ".bat")
    os.system("taskkill /f /im game_win64_" + buildConfigVar.get() + ".exe")

#Code/Config
ConfigFrame = Frame(window)
ConfigFrame.pack(fill=X, pady=2, padx=2)

w = Button(ConfigFrame, text="2015.sln", command=Open_2015)
w.pack(side=LEFT, padx=1)

w = Button(ConfigFrame, text="2012.sln", command=Open_2012)
w.pack(side=LEFT, padx=1)

w = Button(ConfigFrame, text="Rage Script", command=Open_Script_Editor)
w.pack(side=LEFT, padx=1)

w = Button(ConfigFrame, text="VSCode", command=Open_VSCode)
w.pack(side=LEFT, padx=1)

w = Button(ConfigFrame, text="Edit Config", command=Edit_Config)
w.pack(side=RIGHT, padx=1)

BuildFrame = Frame(window)
BuildFrame.pack(fill=X, pady=2, padx=2)

w = OptionMenu(BuildFrame, buildConfigVar, *configData['configOptions'])
w.pack(side=LEFT, padx=1)

w = Checkbutton(BuildFrame, text="Live Branch", variable=useLiveBranchVar)
w.pack(side=LEFT, padx=1)

#Run/Stop
RunStopFrame = Frame(window)
RunStopFrame.pack(fill=X, pady=2, padx=2)

w = Button(RunStopFrame, textvariable=x64ButtonText, command=Run_x64, bg=runColour)
w.pack(side=LEFT, padx=1)

w = Button(RunStopFrame, text="Stop Win64", command=Stop_x64, bg=stopColour)
w.pack(side=LEFT, padx=1)

w = Button(RunStopFrame, text="Stop Orbis", command=Stop_Orbis, bg=stopColour)
w.pack(side=RIGHT, padx=1)

w = Button(RunStopFrame, text="Run Orbis", command=Run_Orbis, bg=runColour)
w.pack(side=RIGHT, padx=1)

window.call('wm', 'attributes', '.', '-topmost', '1')
 
window.mainloop()