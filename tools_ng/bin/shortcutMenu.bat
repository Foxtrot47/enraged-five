@echo off

if not exist shortcutmenu\shortcutmenu.exe (
	cd /d "%~dp0"
	if not exist shortcutmenu\shortcutmenu.exe (
		cd /d "%RS_TOOLSROOT%\bin"
		if not exist shortcutmenu\shortcutmenu.exe (
			cd /d x:\gta5\tools_ng\bin
			if not exist shortcutmenu\shortcutmenu.exe (
				echo.
				echo Error: Could not find ShortcutMenu.exe
				echo.       Please check your environment setup, Perforce workspace mappings,
				echo.       and see that this script is being run correctly:
				echo.       %~dpfx0
				echo.
				pause
				exit 1
))))
if exist setenv.bat call setenv.bat
call shortcutmenu\shortcutmenu.bat
echo.&&echo Menu is now active in systray.
echo.&&echo This window will close automatically in a few seconds...
set /a waittime=3
:waithere
title Window will close in %waittime% seconds...
ping -n 2 -w 1000 127.0.0.1 >nul
set /a WAITTIME=%waittime%-1
if %waittime% gtr 0 goto waithere
set waittime=
