@echo off
setlocal

REM kill the shortcuts menu, rag, and systrayrfs so they are being used out of the proper location
echo.
echo Killing currently running tray icons...
taskkill /FI "STATUS eq RUNNING" /f /IM systrayrfs.exe > %TEMP%\running.txt
set /P RUNSYSTRAY=<%TEMP%\running.txt
taskkill /FI "STATUS eq RUNNING" /f /IM rag.exe > %TEMP%\running.txt
set /P RUNRAG=<%TEMP%\running.txt
taskkill /FI "STATUS eq RUNNING" /f /IM shortcutMenu.exe > %TEMP%\running.txt
set /P RUNMENU=<%TEMP%\running.txt
del /F/Q %TEMP%\running.txt

REM If you want to reset the tray icons even if they aren't running, use the -f parameter
if [%1]==[-f] (
	set RUNSYSTRAY=1
	set RUNRAG=1
	set RUNMENU=1
)

REM restore the shortcuts menu, rag, and systrayrfs from the proper location, if they were previously running
echo.
echo Restoring tray icons that were previously running...
pushd %RS_TOOLSROOT%\bin
if defined RUNSYSTRAY (
	echo %RS_TOOLSROOT%\bin\systrayrfs.exe
	start systrayrfs.exe
)

if defined RUNRAG (
	echo %RS_TOOLSROOT%\bin\rag.exe
	start rag\rag.exe
)

if defined RUNMENU (
	echo %RS_TOOLSROOT%\bin\\shortcutmenu\shortcutMenu.exe
	call shortcutmenu\shortcutmenu.bat
)
popd
endlocal