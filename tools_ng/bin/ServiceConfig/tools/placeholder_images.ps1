function GenerateImage($filepath, $width, $height, $text)
{
	Add-Type -AssemblyName System.Drawing
	
	$rect = [System.Drawing.RectangleF]::FromLTRB(0, 0, $width, $height)
	$format = [System.Drawing.StringFormat]::GenericDefault
    $format.Alignment = [System.Drawing.StringAlignment]::Center
    $format.LineAlignment = [System.Drawing.StringAlignment]::Center

	$pixelFormat = [System.Drawing.Imaging.PixelFormat]::Format24bppRgb
	$bmp = new-object System.Drawing.Bitmap $width,$height,$pixelFormat
	$font = new-object System.Drawing.Font Consolas,28
	$brushBg = [System.Drawing.Brushes]::Black
	$brushFg = [System.Drawing.Brushes]::Yellow
	
	$graphics = [System.Drawing.Graphics]::FromImage($bmp)
	$graphics.SmoothingMode = "AntiAlias"
	$graphics.FillRectangle($brushBg, 0, 0, $bmp.Width, $bmp.Height)
	$graphics.DrawString($text, $font, $brushFg, $rect, $format)
	$graphics.Dispose()
	
	$encoder = [System.Drawing.Imaging.Encoder]::ColorDepth
	$encoderParamSet = New-Object System.Drawing.Imaging.EncoderParameters(1) 
    $encoderParamSet.Param[0] = New-Object System.Drawing.Imaging.EncoderParameter($encoder, 24) 
	
	$pngCodec = [System.Drawing.Imaging.ImageCodecInfo]::GetImageEncoders() | Where{$_.MimeType -eq 'image/png'}
	
	$bmp.Save($filepath, $pngCodec, $encoderParamSet)
}

$basepath = Convert-Path .
$basepath = $basepath + '\'

$fileprefix = "activitycard" 

$textprefix = "activitycard"
$textsuffix = " placeholder"

$imgwidth = 864
$imgheight = 1040

$numimages = 4

for ($i = 0; $i -le $numimages; $i++)
{
	$number = ([string]$i).PadLeft(2,'0')
	$text = $textprefix + $number + $textsuffix
	$path = $basepath + $fileprefix + $number + ".png"
	
	Write-Output ("Generating " + $path)
	GenerateImage $path $imgwidth $imgheight $text
}
