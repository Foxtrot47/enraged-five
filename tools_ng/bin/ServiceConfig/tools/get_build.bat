@echo off

set buildconfig=Release
set mydir=%~dp0
set sourcedir=X:\rgsc\src\dev\tools\base\ui\ServiceConfig\
set sourcebindir=%sourcedir%bin\%buildconfig%\
set destbindir=%mydir%..\
set MSBUILDP=C:\Program Files (x86)\MSBuild\14.0\Bin\amd64\

:: Save current directory and change to target directory
pushd %destbindir%
:: Save value of CD variable (current directory)
set destbindir=%CD%\
:: Restore original directory
popd

:: echo ###
:: echo ### Remember to get latest on %sourcedir% if needed. Not done automatically ###
:: echo ###
p4 update //rgsc/src/dev/tools/base/ui/ServiceConfig/...

"%MSBUILDP%msbuild.exe" "%sourcedir%ServiceConfig.sln" /t:build /p:Configuration=%buildconfig% /m /verbosity:q

CALL :movefile "%sourcebindir%ServiceConfig.exe" "%destbindir%ServiceConfig.exe"
CALL :movefile "%sourcebindir%ServiceConfig.exe.config" "%destbindir%ServiceConfig.exe.config"
CALL :movefile "%sourcebindir%ServiceConfig.pdb" "%destbindir%ServiceConfig.pdb"
CALL :movefile "%sourcebindir%Newtonsoft.Json.dll" "%destbindir%Newtonsoft.Json.dll"

echo ###
echo ### done ###
echo ###
pause
EXIT /B 0

:: ###################

:movefile
SETLOCAL
set srcfile=%~1
set dstfile=%~2
p4 edit %dstfile%
xcopy /Q /Y %srcfile% %dstfile%
p4 revert -a %dstfile%
ENDLOCAL

EXIT /B 0
