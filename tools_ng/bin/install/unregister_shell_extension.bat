@ECHO OFF
REM 
REM File:: %RS_TOOLSBIN%/install/unregister_shell_extension.bat
REM Description:: Install Rockstar Games Shell Extension
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 28 November 2011
REM

CALL setenv.bat >NUL

IF /I "%PROCESSOR_ARCHITECTURE%"=="AMD64" ( 

	ECHO x64 detected, unregistering Shell Extension x64
  PUSHD %RS_TOOLSBIN%\install
  regsvr32 /u /s ShellExtension_x64.dll
  POPD

) ELSE (

	ECHO x86 detected, unregistering Shell Extension x86
  PUSHD %RS_TOOLSBIN%\install
  regsvr32 /u /s ShellExtension_Win32.dll
  POPD
	
)

REM Done.
