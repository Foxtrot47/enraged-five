@ECHO OFF
REM 
REM File:: %RS_TOOLSBIN%/install/register_shell_extension.bat
REM Description:: Install Rockstar Games Shell Extension
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 28 November 2011
REM

CALL setenv.bat >NUL

IF /I "%PROCESSOR_ARCHITECTURE%"=="AMD64" ( 

	ECHO x64 detected, registering Shell Extension x64
	regsvr32 /s %RS_TOOLSBIN%\install\ShellExtension_x64.dll

) ELSE (

	ECHO x86 detected, registering Shell Extension x86
	regsvr32 /s %RS_TOOLSBIN%\install\ShellExtension_Win32.dll
	
)

REM Done.
