@ECHO OFF

REM
REM File:: %RS_TOOLSBIN%/setenv.bat
REM Description:: Additional Tools Environment Setup, convenience for batch files
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Author:: Greg Smith <greg@rockstarnorth.com>
REM Author:: Kristopher Williams <kristopher.williams@rockstarnorth.com>
REM Date:: 23 September 2019
REM
REM This script should be invoked from other bat files using the CALL function.
REM It is intended for the convenience of batch files and processes spawned from
REM batch files, and replicates much of the secondary data available in the ruby process
REM

ECHO RUNNING SETENV FOR GEN9

SET RS_BUILDROOT=%RS_PROJROOT%\build
SET RS_CODEBRANCH=%RS_PROJROOT%\src\dev_gen9
SET RAGE_DIR=%RS_CODEBRANCH%\rage
SET RS_TOOLSSRC=%RAGE_DIR%\framework\tools\src

SET RS_ASSETS=%RS_PROJROOT%\assets_gen9
SET RS_EXPORT=%RS_ASSETS%\export
SET RS_PROCESSED=%RS_ASSETS%\processed
SET RS_ASSETS_TU=%RS_ASSETS%\titleupdate

SET RS_TOOLSCONFIG=%RS_TOOLSROOT%\etc
SET RS_TOOLSBIN=%RS_TOOLSROOT%\bin
SET RS_TOOLSDCC=%RS_TOOLSROOT%\dcc
SET RS_TOOLSLIB=%RS_TOOLSROOT%\lib
SET RS_TOOLSIRONLIB=%RS_TOOLSROOT%\ironlib
SET RS_TOOLSSCRIPT=%RS_TOOLSROOT%\script
SET RS_TOOLSRUBY=%RS_TOOLSROOT%\bin\ruby\bin\ruby.exe
SET RS_TOOLSIR=%RS_TOOLSROOT%\bin\ironruby\bin\ir64.exe
SET RS_TOOLSPYTHON=%RS_TOOLSROOT%\bin\python\App\python.exe
SET RS_TOOLSCONVERT=%RS_TOOLSIRONLIB%\lib\RSG.Pipeline.Convert.exe

SET RS_BUILDBRANCH=%RS_BUILDROOT%\dev_gen9
SET RS_SCRIPTBRANCH=%RS_PROJROOT%\script\dev_gen9
SET RAGE_CRASHDUMP_DIR=%RS_BUILDBRANCH%
SET RS_TITLE_UPDATE=%RS_PROJROOT%\titleupdate\dev_gen9

SET RAGE_3RDPARTY=X:\3rdparty\dev


ECHO ENVIRONMENT

ECHO RS_PROJECT:         %RS_PROJECT%
ECHO RS_PROJROOT:        %RS_PROJROOT%
ECHO RS_BUILDROOT:       %RS_BUILDROOT%
ECHO RS_BUILDBRANCH:     %RS_BUILDBRANCH%
ECHO RS_CODEBRANCH:      %RS_CODEBRANCH%
ECHO RS_SCRIPTBRANCH:    %RS_SCRIPTBRANCH%
ECHO RS_ASSETS:          %RS_ASSETS%
ECHO RS_EXPORT:          %RS_EXPORT%
ECHO RS_PROCESSED:       %RS_PROCESSED%

ECHO RS_TOOLSROOT:       %RS_TOOLSROOT%
ECHO RS_TOOLSBIN:        %RS_TOOLSBIN%
ECHO RS_TOOLSDCC:        %RS_TOOLSDCC%
ECHO RS_TOOLSCONFIG:     %RS_TOOLSCONFIG%
ECHO RS_TOOLSLIB:        %RS_TOOLSLIB%
ECHO RS_TOOLSSRC:        %RS_TOOLSSRC%
ECHO RS_TOOLSRUBY:       %RS_TOOLSRUBY%
ECHO RS_TOOLSIR:         %RS_TOOLSIR%
ECHO RS_TOOLSPYTHON:     %RS_TOOLSPYTHON%

ECHO RS_TOOLSCONVERT:    %RS_TOOLSCONVERT%

ECHO RAGE_DIR:           %RAGE_DIR%
ECHO RAGE_3RDPARTY:      %RAGE_3RDPARTY%
ECHO RAGE_CRASHDUMP_DIR: %RAGE_CRASHDUMP_DIR%

ECHO RS_TITLE_UPDATE:    %RS_TITLE_UPDATE%

ECHO END ENVIRONMENT

REM bin/setenv.bat
