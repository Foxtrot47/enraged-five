Push-Location $PSScriptRoot

$processes = (tasklist.exe) | Out-String

if( $processes -match "SysTrayRfs.exe" )
{
	Write-Host systrayrfs is already running
}
else
{
	Write-Host Running systrayrfs
	Start-Process -FilePath SysTrayRfs.exe
}

if( $processes -match "rag.exe" )
{
	Write-Host rag is already running
}
else
{
	Write-Host Running rag
	Start-Process -FilePath rag\rag.exe
}

if( $processes -match "shortcutMenu.exe" )
{
	Write-Host shortcutMenu is already running
}
else
{
	Write-Host Running ShortcutMenu
	Start-Process -FilePath BootStrapper\BootStrapper.exe -ArgumentList $PSScriptRoot\ShortcutMenu\ShortcutMenu.exe
}

Pop-Location
