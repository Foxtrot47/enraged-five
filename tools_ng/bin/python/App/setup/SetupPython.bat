@echo off
@echo Setting up Project Python Tools Environment
@echo -------------------------------------------

pushd "%~dp0"

rem Setup environment variables for Python.
SETX PYTHONHOME "%RS_TOOLSROOT%\bin\python\App"
SETX PYTHONPATH "%RS_TOOLSROOT%\ironlib\lib;%RS_TOOLSROOT%\techart\script\python\standalone;%RS_TOOLSROOT%\techart\script\python\packages"

rem Copy required files over to the Windows\System32 directory.
XCOPY "%RS_TOOLSROOT%\bin\python\App\python27.dll" "c:\windows\system32" /y
XCOPY "%RS_TOOLSROOT%\bin\python\App\pythoncom27.dll" "c:\windows\system32" /y
XCOPY "%RS_TOOLSROOT%\bin\python\App\pythoncomloader27.dll" "c:\windows\system32" /y
XCOPY "%RS_TOOLSROOT%\bin\python\App\pywintypes27.dll" "c:\windows\system32" /y

rem Update registry to associate .py files with our version of Python.
REG ADD HKCR\.py /ve /t REG_SZ /d py_auto_file /f
REG ADD HKCR\py_auto_file\shell\open\command /ve /t REG_SZ /d "\"%RS_TOOLSROOT%\bin\python\App\pythonw.exe\" \"%%1\"" /f
REG ADD HKCR\Applications\pythonw.exe\shell\open\command /ve /t REG_SZ /d "\"%RS_TOOLSROOT%\bin\python\App\pythonw.exe\" \"%%1\"" /f
REG ADD HKCR\Applications\python.exe\shell\open\command /ve /t REG_SZ /d "\"%RS_TOOLSROOT%\bin\python\App\python.exe\" \"%%1\"" /f
REG ADD HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.py\OpenWithProgids /v py_auto_file /t REG_NONE /f

@echo Finished.

popd
pause