@echo off
setlocal

set GFXE="gfxexport.exe"
set RORC=%RAGE_ROOT_FOLDER%\tools\base\exes\rorc.exe

set XGESUB=xgSubmit /GROUP=GFXExport /ALLOWREMOTE=OFF  /command 
set XGESUBLOCAL=xgSubmit /GROUP=GFXExport /ALLOWREMOTE=OFF /command 
set XGEWAIT=xgWait /group=GFXExport

echo -----------------------------------------------
echo Exporting
echo -----------------------------------------------
for %%i in (*.swf) do (
    echo Exporting %%i
    %XGESUB% %GFXE% -c -i DDS -qp -sd -rescale nearest -fonts -fns 48 -fts 512 -fntlst -list %%i 
)

%XGEWAIT% 

echo -----------------------------------------------
echo Making resource lists
echo -----------------------------------------------
for %%i in (*.swf) do (
    pushd %%~pni
        %XGESUBLOCAL% C:\Python25\python.exe ..\make_texlist.py %%~ni.lst textures.txt
    popd
)

%XGEWAIT% 
echo -----------------------------------------------
echo Resourcing
echo -----------------------------------------------
for %%i in (*.swf) do (
    pushd %%~pni
        %XGESUB% %RORC% textures.txt %%~ni.#td -platform pc
        %XGESUB% %RORC% textures.txt %%~ni.#td -platform xenon
        %XGESUB% %RORC% textures.txt %%~ni.#td -platform ps3 
    popd
)

%XGEWAIT% 

endlocal

pause

