SET "CDIR=%~dp1"
SET "CDIR=%CDIR:~0,-1%"
FOR %%i IN ("%CDIR%") DO SET "PARENT_DIR=%%~nxi"
set LIGHT_FILE="%~dp1%~n1.lightxml"

if "%~x1" == ".lightxml" (
	p4 edit -t binary %LIGHT_FILE%
	%RS_TOOLSROOT%/bin/anim/LightXmlFixup.exe %LIGHT_FILE%
)