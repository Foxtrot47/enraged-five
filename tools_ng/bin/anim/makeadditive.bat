@ECHO OFF
REM
REM makeadditive.bat
REM
REM David Muir <david.muir@rockstarnorth.com>
REM Luke Openshaw <luke.openshaw@rockstarnorth.com>
REM
REM Let animcombine.exe and redirecting stdout play nicely with Windows file associations.
REM

%RS_TOOLSROOT%/bin/anim/animcombine.exe -anims "%1","%1" -subtract -time 0 -out "%~d1%~p1additive_%~n1%~x1"

REM End