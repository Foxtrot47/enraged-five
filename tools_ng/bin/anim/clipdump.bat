@ECHO OFF
REM
REM clipdump.bat
REM
REM David Muir <david.muir@rockstarnorth.com>
REM Let clipdump.exe and redirecting stdout play nicely with Windows file associations.
REM

%RS_TOOLSROOT%/bin/anim/clipdump.exe -clip %1 > %1.txt

REM End