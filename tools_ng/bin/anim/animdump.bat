@ECHO OFF
REM
REM animdump.bat
REM
REM David Muir <david.muir@rockstarnorth.com>
REM
REM Let animdump.exe and redirecting stdout play nicely with Windows file associations.
REM
REM EDIT: Luke Openshaw <luke.openshaw@rockstarnorth.com>
REM Update CL params

%RS_TOOLSROOT%/bin/anim/animdump.exe -anim %1 -values -eulers -output > %1.txt -skel "%RS_TOOLSROOT%\etc\config\anim\skeletons\player.skel"

REM End