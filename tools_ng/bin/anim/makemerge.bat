@ECHO OFF
REM
REM makemerge.bat
REM
REM 
REM Let animcombine.exe and redirecting stdout play nicely with Windows file associations.
REM

set argcount=0 
for %%x in (%*) do Set /A argcount+=1

IF %argcount%==3 GOTO success
goto failure

:success
%RS_TOOLSROOT%\bin\anim\animcombine.exe -anims "%1","%2" -merge -sync -out "%3" -output -nocompression -nocompress

echo Created %3

copy "%~dpn1.clip" "%~dpn3.clip"
%RS_TOOLSROOT%\bin\anim\clipedit.exe -path "%~dp3\" -clip "%~dpn3.clip" -out "%~dpn3.clip" -clipanimation ,,,"$/%~n3%~x3"

echo Created %~dpn3.clip

goto end

:failure
echo Usage: makemerge.bat drive:\path\anim1.anim drive:\path\anim2.anim drive:\path\merged_animation.anim

:end

REM Endco