@ECHO OFF
REM
REM expressionsdump.bat
REM
REM Let expressionsdump.exe and redirecting stdout play nicely with Windows file associations.
REM

%RS_TOOLSROOT%/bin/anim/expressionsdump.exe -verbose -expr "%1" > "%1.txt"

REM End