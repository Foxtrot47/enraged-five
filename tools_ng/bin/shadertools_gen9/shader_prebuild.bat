@echo off
setlocal

:: Shader pre-build step:
::     shader_prebuild <action> <shader platform> [platform config]
::     - <action> is "build" "rebuild" or "clean"
::     - platform and config (optional) are the names used by RSM

set action=%~1
set platform=%~2
set config=%~3
set branch=%RS_BRANCHNAME%
echo.
echo Starting shader prebuild step...
echo.      Action: %action%
echo.      Branch: %branch%
echo.    Platform: %platform%
echo.      Config: %config%

if "%action%"=="" echo [No action specified]&&goto :done

:: only needed for scarlett shaders, for now..
if /i not "%platform%"=="sga_scarlett" goto :done
echo. 

:: only clean on user machines if a clean or rebuild; build machines will always clean
if /i not "%action%"=="clean" if /i not "%action%"=="rebuild" if /i not "%COMPUTERNAME:~0,3%"=="rsg" goto :done
echo Cleaning scarlett shader pdbs...
if "%branch%"=="" echo ERROR: Invalid branch - can't clean pdbs&&goto :done
:: path must match hardcoded one in sgafxc.ini
set pdbpath=%RS_PROJROOT%\shader_pdb\%branch%\scarlett
if "%config%"=="" (
	echo [%pdbpath%]
	if exist "%pdbpath%" (
		del /q /f /s "%pdbpath%\*.*" >nul
		rd /s /q "%pdbpath%" >nul
	) else (
		echo No scarlett shader pdbs to clean.&&goto :done
	)
) else (
	echo [%pdbpath%\%config%]
	if exist "%pdbpath%\%config%" (
		del /q /f "%pdbpath%\%config%\*.*" >nul
		rd /q "%pdbpath%\%config%" >nul
	) else (
		echo No scarlett shader pdbs to clean.&&goto :done
	)
)
echo Finished cleaning scarlett pdbs.


:done
echo.
echo Completed shader prebuild step.
echo.
exit /b 0
