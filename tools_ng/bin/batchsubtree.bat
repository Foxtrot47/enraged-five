@ECHO OFF

SET SCRIPT_DIR=%~dp0

REM -r means recurse
REM -p means check files out of perforce if necessary
CALL %SCRIPT_DIR%batchsub.py -rp %* *.cpp *.h *.inl

