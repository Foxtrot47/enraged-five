<?xml version="1.0"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="html"/>

    <xsl:template match="/">
	<xsl:variable name="project" select="/cruisecontrol/@project"/> 
	<xsl:if test="contains($project, 'assetbuilder') or contains($project, 'tester')">
		<xsl:variable name="logfolder" select="/cruisecontrol/integrationProperties/CCNetArtifactDirectory"/>
		<xsl:variable name="label" select="/cruisecontrol/integrationProperties/CCNetLabel"/>

	        <table class="section-table" cellpadding="2" cellspacing="0" border="0">
       	     		<tr>		       	     		
       	       			<td class="header-label"><nobr>Logs are here </nobr></td>
				<xsl:text disable-output-escaping="yes">&lt;!--[if IE]&gt;</xsl:text>

					<td class="header-data">              			
					<a>
						<xsl:attribute name="href">
							file:///<xsl:value-of select="$logfolder"/>/logfiles/<xsl:value-of select="$label"/>/
						</xsl:attribute>
						Log Files
					</a>
					</td>	
					<xsl:text disable-output-escaping="yes">&lt;!--</xsl:text> 
				<xsl:text disable-output-escaping="yes">&lt;![endif]&gt;--&gt;</xsl:text> 
              			
				<xsl:text disable-output-escaping="yes">&lt;!--[if !IE]&gt;&lt;!--&gt;</xsl:text>
					<td class="header-label"><u><xsl:value-of select="$logfolder"/>/logfiles/<xsl:value-of select="$label"/></u></td>					
				<xsl:text disable-output-escaping="yes">&lt;![endif]--&gt;</xsl:text>  
            		</tr>    
        	</table>
	</xsl:if>
    </xsl:template>

</xsl:stylesheet>
