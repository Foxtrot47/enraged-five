<?xml version="1.0"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="html"/>

    <xsl:template match="/">
   		<xsl:variable name="messages" select="/cruisecontrol//buildresults//message" />
   		<xsl:if test="count($messages) > 0">   	
			    <xsl:variable 	name="error.messages" 
						select="$messages[
									(
										(
											contains(text(), ' Exception') 
											or 
											contains(text(), 'Error ') 
											or 
											contains(text(), 'error ') 
											or 
											contains(text(), 'Error: ')
											or 
											contains(text(), 'error: ')
										) and 
											not(
												contains(text(), 'INFO_MSG:')
												or
												contains(text(), 'INFO_EMA:')
												or												
												contains(text(), 'INFO_WEB:')
												or												
												contains(text(), 'ccignore:')
												or
												contains(text(), '-werror ')
												or
												contains(text(), '-Werror ')
												or
												contains(text(), 'gcc2vs not found')
											    )
									) 
									or 
									@level='Error'
								] 
								| /cruisecontrol//builderror/message 
								| /cruisecontrol//internalerror/message" 
			    />
	        <xsl:variable name="error.messages.count" select="count($error.messages)" />
	        <xsl:variable name="warningTruncateSize" select="10000"/>
	        	        
	        <xsl:variable name="warning.messages" 
                        select="$messages[ 
                                            (
                                              (
                                                contains(text(), 'Warning ') 
                                                or 
                                                contains(text(), 'warning ') 
                                                or 
                                                contains(text(), 'Warning: ')
                                                or 
                                                contains(text(), 'warning: ')
                                              )
                                              and not(
							contains(text(), 'INFO_MSG:')
							or
							contains(text(), 'INFO_EMA:')
							or							
							contains(text(), 'INFO_WEB:')
							or							
							contains(text(), 'ccignore:')
							or
                                                        contains(text(), 'X1002')
                                                     )
                                            )
                                            or 
                                              @level='Warning'
                                          ]" />
	        <xsl:variable name="warning.messages.count" select="count($warning.messages)" />
	        <xsl:variable name="info.messages" 
                        select="$messages[
                                            (
                                              (
                                                contains(text(), 'INFO_MSG:') 
                                                or
						contains(text(), 'INFO_WEB:')                                                 
                                              )
                                              and not(
							contains(text(), 'ccignore:')
                                                     )
                                            )]" />

	        <xsl:variable name="info.messages.count" select="count($info.messages)" />

	        <xsl:variable name="total" select="count($error.messages) + count($warning.messages)"/>

	        <xsl:if test="$error.messages.count > 0">
	            <table class="section-table" cellpadding="2" cellspacing="0" border="0" width="98%">
	                <tr>
	                    <td class="sectionheader">
	                        Errors: (<xsl:value-of select="$error.messages.count"/>)
	                    </td>
	                </tr>
	                <tr>
						<td>
							<xsl:apply-templates select="$error.messages"/>
						</td>
					</tr>
	            </table>
	        </xsl:if>
	        <xsl:if test="$warning.messages.count > 0">
	            <table class="section-table" cellpadding="2" cellspacing="0" border="0" width="98%">
	                <tr>
		                <xsl:choose>
					<xsl:when test="$warning.messages.count &gt; $warningTruncateSize">
						<td class="sectionheader">
							Warnings: (<xsl:value-of select="$warning.messages.count"/>) - truncated at <xsl:value-of select="$warningTruncateSize"/> warnings.
				    		</td>
					</xsl:when>
					<xsl:otherwise>
						<td class="sectionheader">
							Warnings: (<xsl:value-of select="$warning.messages.count"/>)
				    		</td>
					</xsl:otherwise>
			    	</xsl:choose>
	                </tr>
	                
	                <tr><td><xsl:apply-templates select="$warning.messages[position() &lt; $warningTruncateSize + 1]"/></td></tr>

	            </table>
	        </xsl:if>
	        <xsl:if test="$info.messages.count > 0">
	            <table class="section-table" cellpadding="2" cellspacing="0" border="0" width="98%">
	                <tr>
	                    <td class="sectionheader">
	                        Info: (<xsl:value-of select="$info.messages.count"/>)
	                    </td>
	                </tr>
	                <tr><td><xsl:apply-templates select="$info.messages"/></td></tr>
	            </table>
	        </xsl:if>


      </xsl:if>
    </xsl:template>

    <xsl:template match="message">

	<xsl:choose>
	    <xsl:when test="contains(text(), '[colourise=black]')">
      		 <pre class="section-black">
			<xsl:value-of select="substring-before(text(),'[colourise=black]')"/>
			<xsl:value-of select="substring-after(text(),'[colourise=black]')"/>
		</pre>
	    </xsl:when>
	    <xsl:when test="contains(text(), '[colourise=grey]')">
      		 <pre class="section-grey">
			<xsl:value-of select="substring-before(text(),'[colourise=grey]')"/>
			<xsl:value-of select="substring-after(text(),'[colourise=grey]')"/>
		</pre>
	    </xsl:when>
	    <xsl:when test="contains(text(), '[colourise=red]')">
      		 <pre class="section-red">
			<xsl:value-of select="substring-before(text(),'[colourise=red]')"/>
			<xsl:value-of select="substring-after(text(),'[colourise=red]')"/>
		</pre>
	    </xsl:when>
	    <xsl:when test="contains(text(), '[colourise=green]')">
      		 <pre class="section-green">
			<xsl:value-of select="substring-before(text(),'[colourise=green]')"/>
			<xsl:value-of select="substring-after(text(),'[colourise=green]')"/>
		</pre>
	    </xsl:when>
	    <xsl:when test="contains(text(), '[colourise=blue]')">
      		 <pre class="section-blue">
			<xsl:value-of select="substring-before(text(),'[colourise=blue]')"/>
			<xsl:value-of select="substring-after(text(),'[colourise=blue]')"/>
		</pre>
	    </xsl:when>
	    <xsl:when test="contains(text(), '[colourise=yellow]')">
      		 <pre class="section-yellow">
			<xsl:value-of select="substring-before(text(),'[colourise=yellow]')"/>
			<xsl:value-of select="substring-after(text(),'[colourise=yellow]')"/>
		</pre>
	    </xsl:when>
	    <xsl:when test="contains(text(), '[colourise=orange]')">
      		 <pre class="section-orange">
			<xsl:value-of select="substring-before(text(),'[colourise=orange]')"/>
			<xsl:value-of select="substring-after(text(),'[colourise=orange]')"/>
		</pre>
	    </xsl:when>
    	    <xsl:otherwise>
		<pre class="section-error"><xsl:value-of select="text()"/></pre>
	    </xsl:otherwise>
	</xsl:choose>

    </xsl:template>

</xsl:stylesheet>