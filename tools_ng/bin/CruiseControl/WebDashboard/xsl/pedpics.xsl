<?xml version="1.0"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="html"/>

    <xsl:template match="/">
	<xsl:variable name="project" select="/cruisecontrol/@project"/> 
	<xsl:if test="contains($project, 'pedpics') or contains($project, 'pedPics')">
		<xsl:variable name="picfolder" select="/cruisecontrol/integrationProperties/CCNetArtifactDirectory"/>
		<xsl:variable name="label" select="/cruisecontrol/integrationProperties/CCNetLabel"/>

	        <table class="section-table" cellpadding="2" cellspacing="0" border="0">
       	     		<tr>
       	       			<td class="header-label"><nobr>PED PICS are here </nobr></td>
              			<td class="header-data">
				<a>
              				<xsl:attribute name="href">
                				<xsl:value-of select="$picfolder"/>/pedpics/<xsl:value-of select="$label"/>
              				</xsl:attribute>
             	 			View Ped Pics
            			</a>
              			</td>
            		</tr>    
        	</table>
	</xsl:if>
    </xsl:template>

</xsl:stylesheet>
