<?xml version="1.0"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
		<xsl:variable name="errors" select="/cruisecontrol//msbuild//error" />
		<xsl:variable name="errors.count" select="count($errors)" />
		<xsl:variable name="warnings" select="/cruisecontrol//msbuild//warning" />
		<xsl:variable name="warnings.count" select="count($warnings)" />
        <xsl:if test="$errors.count > 0">
            <table class="section-table" cellpadding="2" cellspacing="0" border="0" width="98%">
                <tr>
                    <td class="sectionheader">
                        Errors (<xsl:value-of select="$errors.count"/>)
                    </td>
                </tr>
                <tr>
					<td class="section-error">
						<xsl:apply-templates select="$errors"/>
					</td>
				</tr>
            </table>
        </xsl:if>
        <xsl:if test="$warnings.count > 0">
			<table class="section-table" cellpadding="2" cellspacing="0" border="0" width="98%">
				<tr>
					<td class="sectionheader">
						Warnings (<xsl:value-of select="$warnings.count"/>)
	                </td>
	            </tr>
	            <tr>
					<td class="section-warn">
						<xsl:apply-templates select="$warnings"/>
					</td>
				</tr>
			</table>
	    </xsl:if>
    </xsl:template>
	
	
	<xsl:template match="error">
		<xsl:variable name="max_len" select="100000"/>
		<div style="color:orangered">
			<xsl:if test="string-length(text())&gt;=$max_len">
				<xsl:if test="@file != ''" >
					<tr><td class="section-custom-error"><xsl:value-of select="position()"/>. <xsl:value-of select="normalize-space(@file)"/> (<xsl:value-of select="normalize-space(@line)"/>,<xsl:value-of select="normalize-space(@column)"/>):</td></tr>
				</xsl:if>
				<tr><td class="section-custom-error">error<xsl:value-of select="normalize-space(@code)"/>: <xsl:value-of select="normalize-space(substring(text(),1,$max_len))" />...</td></tr>
				<tr><td class="section-black">(truncated in compile-msbuild.xsl to <xsl:value-of select="$max_len"/> characters - see web page for full detail)</td></tr>
			</xsl:if>
			<xsl:if test="string-length(text())&lt;$max_len">
				<xsl:if test="@file != ''" >
					<tr><td class="section-custom-error"><xsl:value-of select="position()"/>. <xsl:value-of select="normalize-space(@file)"/> (<xsl:value-of select="normalize-space(@line)"/>,<xsl:value-of select="normalize-space(@column)"/>):</td></tr>
				</xsl:if>
				<tr><td class="section-custom-error">error<xsl:value-of select="normalize-space(@code)"/>: <xsl:value-of select="normalize-space(text())" /></td></tr>
			</xsl:if>							
		</div>
	</xsl:template>

	<xsl:template match="warning">
		<xsl:variable name="max_len" select="100000"/>
		<div style="color:blue">
			<tr><td class="section-custom-error"/></tr>
			<xsl:if test="string-length(text())&gt;=$max_len">
				<xsl:if test="@file != ''" >
					<tr><td class="section-custom-warn"><xsl:value-of select="position()"/>. <xsl:value-of select="normalize-space(normalize-space(@file))"/> (<xsl:value-of select="normalize-space(@line)"/>,<xsl:value-of select="normalize-space(@column)"/>):</td></tr>
				</xsl:if>
				<xsl:if test="@file = ''" >
					<tr><td class="section-custom-warn">warning<xsl:value-of select="normalize-space(normalize-space(@code))"/>: <xsl:value-of select="normalize-space(substring(text(),1,$max_len))" />...</td></tr>
					<tr><td class="section-black">(truncated in compile-msbuild.xsl to <xsl:value-of select="$max_len"/> characters - see web page for full detail)</td></tr>
				</xsl:if>
			</xsl:if>

			<xsl:if test="string-length(text())&lt;$max_len">
				<xsl:if test="@file != ''" >
					<tr><td class="section-custom-warn"><xsl:value-of select="position()"/>. <xsl:value-of select="normalize-space(@file)"/> (<xsl:value-of select="normalize-space(@line)"/>,<xsl:value-of select="normalize-space(@column)"/>):</td></tr>
				</xsl:if>
				<tr><td class="section-custom-warn">warning<xsl:value-of select="normalize-space(@code)"/>: <xsl:value-of select="normalize-space(text())" /></td></tr>
			</xsl:if>	
		</div>
	</xsl:template>
</xsl:stylesheet>
