<?xml version="1.0"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="html"/>

    <xsl:template match="/">
        <xsl:variable name="modification.list" select="/cruisecontrol/modifications/modification"/>

        <table class="section-table" cellpadding="2" cellspacing="0" border="0">

            <xsl:if test="/cruisecontrol/exception">
                <tr><td class="header-title" colspan="2">BUILD EXCEPTION</td></tr>
                <tr>
                    <td class="header-label" valign="top"><nobr>Error Message:</nobr></td>
                    <td class="header-data-error"><xsl:value-of select="/cruisecontrol/exception"/></td>
                </tr>
            </xsl:if>
            
            <xsl:if test="/cruisecontrol/build/@error">
                <tr><td class="header-title" colspan="2">BUILD FAILED</td></tr>
            </xsl:if>
            
            <xsl:if test="not (/cruisecontrol/build/@error) and not (/cruisecontrol/exception)">
                <tr><td class="header-title" colspan="2">BUILD SUCCESSFUL</td></tr>
            </xsl:if>

            <tr>
              <td class="header-label"><nobr>Project:</nobr></td>
              <td class="header-data"><xsl:value-of select="/cruisecontrol/@project"/></td>
            </tr>
            <tr>
                <td class="header-label"><nobr>Date of build:</nobr></td>
                <td class="header-data"><xsl:value-of select="/cruisecontrol/build/@date"/></td>
            </tr>
            <tr>
                <td class="header-label"><nobr>Running time:</nobr></td>
                <td class="header-data"><xsl:value-of select="/cruisecontrol/build/@buildtime"/></td>
            </tr>
            <tr>
              <td class="header-label"><nobr>Integration Request:</nobr></td>
              <td class="header-data">
                <xsl:value-of select="/cruisecontrol/request" />
              </td>
            </tr>

	    <xsl:if test="/cruisecontrol/build/@error">
	      	<xsl:variable name="failure-users" 		select="/cruisecontrol/integrationProperties/CCNetFailureUsers/user"/>
	    	<xsl:variable name="failure-users-count" 	select="count($failure-users)"/>
		<xsl:variable name="failure-users-max" 		select="10"/>
		<xsl:if test="$failure-users-count &gt; 0">
	        	<tr>
	      	 		<td class="header-label"><nobr><xsl:value-of select="$failure-users-count"/> Suspects:</nobr></td>
				<xsl:choose>
					<xsl:when test="$failure-users-count &lt; $failure-users-max + 1"> 
						<xsl:if test="$failure-users-count &gt; 0">
			      				<td class="header-data">
		      						<xsl:for-each select="$failure-users">
									<xsl:value-of select="text()"/><xsl:text> </xsl:text>
								</xsl:for-each>
						       </td>			
			     			</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<td class="header-data">Too many suspects to list.</td>
					</xsl:otherwise>
				</xsl:choose>
			</tr>
		</xsl:if>
	    </xsl:if>
            
            <xsl:apply-templates select="$modification.list">
                <xsl:sort select="date" order="descending" data-type="text" />
            </xsl:apply-templates>
            
        </table>
    </xsl:template>

    <!-- Last Modification template -->
    <xsl:template match="/cruisecontrol/modifications/modification">
        <xsl:if test="position() = 1">
          <tr>
            <td class="header-label"><nobr>Last changelist:</nobr></td>
            <td class="header-data" valign="top">
              <xsl:choose>
                <xsl:when test="count(url) = 1 ">
                  <a>
                    <xsl:attribute name="href">
                      <xsl:value-of select="url" />
                    </xsl:attribute>
                    <xsl:value-of select="changeNumber"/>
                  </a>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="changeNumber"/>
                </xsl:otherwise>
              </xsl:choose>
		( <xsl:value-of select="user"/> )
            </td>
          </tr>

          <tr>
            <td class="header-label"><nobr>Last changed:</nobr></td>
            <td class="header-data"><xsl:value-of select="date"/></td>
          </tr>
          <xsl:if test="string-length(issueUrl)!=0">
            <tr>
              <td class="header-label" valign="top"><nobr>Link to issue:</nobr></td>
              <td class="header-data"><A><xsl:attribute name="HREF"><xsl:value-of select="issueUrl"/></xsl:attribute><xsl:value-of select="issueUrl"/></A></td>
            </tr>
          </xsl:if>
          <tr>
            <td class="header-label" valign="top"><nobr>Last log entry:</nobr></td>
            <td class="header-data"><xsl:value-of select="comment"/></td>
          </tr>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
