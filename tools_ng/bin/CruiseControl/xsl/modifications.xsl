<?xml version="1.0"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="html"/>

    <xsl:variable name="modification.list" select="/cruisecontrol/modifications/modification"/>
	<xsl:variable name="webpage" select="/cruisecontrol/integrationProperties/CCNetProjectUrl"/>
	
    <!-- Empty cell -->
    <xsl:template name="empty">
      <td class="section-data" valign="top">&#160;</td>
    </xsl:template>
	
	<!-- Truncate cell -->
	<xsl:variable name="truncateSize" select="25"/>
	<xsl:template name="truncate">
      <td class="section-data" valign="top">Changelist truncated.  See web page for full details.</td>
    </xsl:template>

    <!-- Changlist no with p4 web link -->
    <xsl:template name="change">
      <td class="section-data" valign="top">
        <xsl:choose>
          <xsl:when test="count(url) = 1 ">
            <a>
              <xsl:attribute name="href">
                <xsl:value-of select="url" />
              </xsl:attribute>
              <xsl:value-of select="changeNumber"/>
            </a>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="changeNumber"/>
          </xsl:otherwise>
        </xsl:choose>
      </td>
    </xsl:template>

    <!-- Filename with p4web diff -->
    <xsl:template name="file">
      <td class="section-data" valign="top">
        <!-- diff against previous revision support -->
        <xsl:choose>
          <xsl:when test="count(url) = 1 ">
            <a>
              <xsl:attribute name="href">
                <xsl:variable name="url" select="url"/>
                <xsl:variable name="curr_version" select="version"/>
                <xsl:variable name="prev_version">
                  <xsl:value-of select="($curr_version)-1"/>
                </xsl:variable>
                <xsl:value-of select="substring-before($url,'&amp;')"/>&amp;cdf=<xsl:value-of select="project"/>/<xsl:value-of select="filename"/>@<xsl:value-of select="project"/>/<xsl:value-of select="filename"/>?ac=19&amp;rev1=<xsl:value-of select="$prev_version"/>&amp;rev2=<xsl:value-of select="$curr_version"/>#1
              </xsl:attribute>
              <xsl:if test="project != ''"><xsl:value-of select="project"/>/</xsl:if><xsl:value-of select="filename"/>
            </a>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test="project != ''"><xsl:value-of select="project"/>/</xsl:if><xsl:value-of select="filename"/>
          </xsl:otherwise>
        </xsl:choose>
      </td>
    </xsl:template>

    <!-- user with mailto link -->
    <xsl:template name="user">
      <td class="section-data" valign="top">
        <a>
          <xsl:attribute name="href">
            mailto:<xsl:value-of select="user"/>?subject=Re. Changelist <xsl:value-of select="changeNumber"/>&amp;body='<xsl:value-of select="comment"/>'
          </xsl:attribute>
          <xsl:value-of select="user"/>
        </a>
      </td>
    </xsl:template>

    <!-- Entry -->
    <xsl:template match="/">
		
		<table class="section-table" cellpadding="2" cellspacing="0" border="0" width="98%">
			<!-- Modifications -->
			<tr>
				<td class="sectionheader" colspan="6">
				Modifications since last build (<xsl:value-of select="count($modification.list)"/>)
				</td>
			</tr>
			
			<xsl:apply-templates select="$modification.list">
			  <!-- <xsl:sort select="date" order="descending" data-type="text" /> -->
			</xsl:apply-templates>
			
		</table>
    </xsl:template>
	
    <!-- Modifications template -->
    <xsl:template match="modification">
	
		<xsl:if test="position() = $truncateSize+1">
			<tr>
				<xsl:if test="position() mod 2=0">
				  <xsl:attribute name="class">section-oddrow</xsl:attribute>
				</xsl:if>
				<xsl:if test="position() mod 2!=0">
				  <xsl:attribute name="class">section-evenrow</xsl:attribute>
				</xsl:if>
				<xsl:call-template name="empty"/>
				<xsl:call-template name="empty"/>
				<xsl:call-template name="empty"/>
				<xsl:call-template name="truncate"/>
				<xsl:call-template name="empty"/>
				<xsl:call-template name="empty"/>
			</tr>
		</xsl:if>

		<xsl:if test="position() &gt; $truncateSize and preceding::modification[1]/changeNumber != changeNumber">
			<tr>
				<xsl:if test="position() mod 2=0">
				  <xsl:attribute name="class">section-oddrow</xsl:attribute>
				</xsl:if>
				<xsl:if test="position() mod 2!=0">
				  <xsl:attribute name="class">section-evenrow</xsl:attribute>
				</xsl:if>
				<xsl:call-template name="change"/>
				<xsl:call-template name="user"/>
				<td class="section-data" valign="top"><xsl:value-of select="@type"/></td>
				<xsl:call-template name="file"/>
				<td class="section-data" valign="top"><xsl:value-of select="comment"/></td>
				<td class="section-data" valign="top"><xsl:value-of select="date"/></td>
			</tr>
		</xsl:if>
				
		<xsl:if test="position() &gt; $truncateSize and preceding::modification[1]/changeNumber = changeNumber and preceding::modification[2]/changeNumber != changeNumber">
			<tr>
				<xsl:if test="position() mod 2=0">
				  <xsl:attribute name="class">section-oddrow</xsl:attribute>
				</xsl:if>
				<xsl:if test="position() mod 2!=0">
				  <xsl:attribute name="class">section-evenrow</xsl:attribute>
				</xsl:if>
				<xsl:call-template name="empty"/>
				<xsl:call-template name="empty"/>
				<xsl:call-template name="empty"/>
				<xsl:call-template name="truncate"/>
				<xsl:call-template name="empty"/>
				<xsl:call-template name="empty"/>
			</tr>
		</xsl:if>
		
		
		<xsl:if test="position() = 1">
			<tr>
				<xsl:if test="position() mod 2=0">
				  <xsl:attribute name="class">section-oddrow</xsl:attribute>
				</xsl:if>
				<xsl:if test="position() mod 2!=0">
				  <xsl:attribute name="class">section-evenrow</xsl:attribute>
				</xsl:if>
				<xsl:call-template name="change"/>
				<xsl:call-template name="user"/>
				<td class="section-data" valign="top"><xsl:value-of select="@type"/></td>
				<xsl:call-template name="file"/>
				<td class="section-data" valign="top"><xsl:value-of select="comment"/></td>
				<td class="section-data" valign="top"><xsl:value-of select="date"/></td>
			</tr>
		</xsl:if>
		
		<xsl:if test="position() &lt; $truncateSize and preceding::modification[1]/changeNumber != changeNumber">
			<tr>
				<xsl:if test="position() mod 2=0">
				  <xsl:attribute name="class">section-oddrow</xsl:attribute>
				</xsl:if>
				<xsl:if test="position() mod 2!=0">
				  <xsl:attribute name="class">section-evenrow</xsl:attribute>
				</xsl:if>
				<xsl:call-template name="change"/>
				<xsl:call-template name="user"/>
				<td class="section-data" valign="top"><xsl:value-of select="@type"/></td>
				<xsl:call-template name="file"/>
				<td class="section-data" valign="top"><xsl:value-of select="comment"/></td>
				<td class="section-data" valign="top"><xsl:value-of select="date"/></td>
			</tr>
		</xsl:if>
		
		<xsl:if test="position() &lt; $truncateSize and preceding::modification[1]/changeNumber = changeNumber">
			<tr>
				<xsl:if test="position() mod 2=0">
				  <xsl:attribute name="class">section-oddrow</xsl:attribute>
				</xsl:if>
				<xsl:if test="position() mod 2!=0">
				  <xsl:attribute name="class">section-evenrow</xsl:attribute>
				</xsl:if>
				<xsl:call-template name="empty"/>
				<xsl:call-template name="empty"/>
				<td class="section-data" valign="top"><xsl:value-of select="@type"/></td>
				<xsl:call-template name="file"/>
				<xsl:call-template name="empty"/>
				<xsl:call-template name="empty"/>
			</tr>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
