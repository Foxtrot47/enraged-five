<?xml version="1.0"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

    <xsl:output method="html"/>

    <xsl:template match="/">
   	<xsl:variable name="messages" select="/cruisecontrol//buildresults//message" />
	<table class="section-table">
	  <xsl:for-each select="$messages">
            <tr>
              <td>
                <xsl:attribute name="class"><xsl:value-of select="$h"/></xsl:attribute>
                <xsl:value-of select="text()"/>
       	      </td>
       	    </tr>
          </xsl:for-each>
          <br />
          <hr class="rule" />          
        </table>
    </xsl:template>

</xsl:stylesheet>