# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = %q{ruby-debug-ide}
  s.version = "0.4.17.beta3"

  s.required_rubygems_version = Gem::Requirement.new("> 1.3.1") if s.respond_to? :required_rubygems_version=
  s.authors = ["Markus Barchfeld, Martin Krauskopf, Mark Moseley, JetBrains RubyMine Team"]
  s.date = %q{2011-02-13}
  s.default_executable = %q{rdebug-ide}
  s.description = %q{An interface which glues ruby-debug to IDEs like Eclipse (RDT), NetBeans and RubyMine.
}
  s.email = %q{rubymine-feedback@jetbrains.com}
  s.executables = ["rdebug-ide"]
  s.files = ["MIT-LICENSE", "Rakefile", "bin/rdebug-ide", "lib/ruby-debug-ide.rb", "lib/ruby-debug/printers.rb", "lib/ruby-debug/helper.rb", "lib/ruby-debug/commands/catchpoint.rb", "lib/ruby-debug/commands/frame.rb", "lib/ruby-debug/commands/inspect.rb", "lib/ruby-debug/commands/condition.rb", "lib/ruby-debug/commands/variables.rb", "lib/ruby-debug/commands/load.rb", "lib/ruby-debug/commands/set_type.rb", "lib/ruby-debug/commands/enable.rb", "lib/ruby-debug/commands/control.rb", "lib/ruby-debug/commands/jump.rb", "lib/ruby-debug/commands/stepping.rb", "lib/ruby-debug/commands/pause.rb", "lib/ruby-debug/commands/threads.rb", "lib/ruby-debug/commands/breakpoints.rb", "lib/ruby-debug/commands/eval.rb", "lib/ruby-debug/command.rb", "lib/ruby-debug/xml_printer.rb", "lib/ruby-debug/event_processor.rb", "lib/ruby-debug/interface.rb", "lib/ruby-debug/version.rb", "lib/ruby-debug/processor.rb", "ext/mkrf_conf.rb"]
  s.homepage = %q{http://rubyforge.org/projects/debug-commons/}
  s.require_paths = ["lib"]
  s.required_ruby_version = Gem::Requirement.new(">= 1.8.2")
  s.rubyforge_project = %q{debug-commons}
  s.rubygems_version = %q{1.3.7}
  s.summary = %q{IDE interface for ruby-debug.}

  if s.respond_to? :specification_version then
    current_version = Gem::Specification::CURRENT_SPECIFICATION_VERSION
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rake>, [">= 0.8.1"])
    else
      s.add_dependency(%q<rake>, [">= 0.8.1"])
    end
  else
    s.add_dependency(%q<rake>, [">= 0.8.1"])
  end
end
