# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = %q{tree}
  s.version = "0.2.1"

  s.required_rubygems_version = nil if s.respond_to? :required_rubygems_version=
  s.authors = ["Anupam Sengupta"]
  s.autorequire = %q{tree}
  s.cert_chain = nil
  s.date = %q{2005-12-29}
  s.description = %q{Provides a generic tree data structure with ability to store keyed node elements in the tree. The implementation mixes in the Enumerable module.}
  s.email = %q{anupamsg@gmail.com}
  s.extra_rdoc_files = ["README"]
  s.files = ["Rakefile", "README", "lib/tree.rb", "test/person.rb", "test/testtree.rb"]
  s.require_paths = ["lib"]
  s.required_ruby_version = Gem::Requirement.new("> 0.0.0")
  s.rubygems_version = %q{1.3.7}
  s.summary = %q{Ruby implementation of the Tree data structure.}
  s.test_files = ["test/testtree.rb"]

  if s.respond_to? :specification_version then
    current_version = Gem::Specification::CURRENT_SPECIFICATION_VERSION
    s.specification_version = 1

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
    else
    end
  else
  end
end
