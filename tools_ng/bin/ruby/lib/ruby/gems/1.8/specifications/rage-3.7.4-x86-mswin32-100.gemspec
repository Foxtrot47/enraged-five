# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = %q{rage}
  s.version = "3.7.4"
  s.platform = %q{x86-mswin32-100}

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["David Muir"]
  s.date = %q{2010-11-09}
  s.description = %q{Provides script access to ragebuilder}
  s.email = %q{david.muir@rockstarnorth.com}
  s.files = ["gem/rage.so"]
  s.homepage = %q{http://www.rockstargames.com/}
  s.require_paths = ["."]
  s.rubygems_version = %q{1.3.7}
  s.summary = %q{A wrapper for ragebuilder library}

  if s.respond_to? :specification_version then
    current_version = Gem::Specification::CURRENT_SPECIFICATION_VERSION
    s.specification_version = 3
  end
end
