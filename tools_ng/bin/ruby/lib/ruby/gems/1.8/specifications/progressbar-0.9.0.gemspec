# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = %q{progressbar}
  s.version = "0.9.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Satoru Takabayashi", "Jose Peleteiro"]
  s.date = %q{2009-11-24}
  s.description = %q{Ruby/ProgressBar is a text progress bar library for Ruby. It can indicate progress with percentage, a progress bar, and estimated remaining time.}
  s.email = %q{satoru@0xcc.net}
  s.extra_rdoc_files = ["ChangeLog", "LICENSE", "README.en.rd", "README.ja.rd"]
  s.files = ["lib/progressbar.rb", "ChangeLog", "LICENSE", "README.en.rd", "README.ja.rd", "test/test.rb"]
  s.homepage = %q{http://github.com/peleteiro/progressbar}
  s.rdoc_options = ["--charset=UTF-8"]
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.3.7}
  s.summary = %q{Ruby/ProgressBar is a text progress bar library for Ruby.}
  s.test_files = ["test/test.rb"]

  if s.respond_to? :specification_version then
    current_version = Gem::Specification::CURRENT_SPECIFICATION_VERSION
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
    else
    end
  else
  end
end
