# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = %q{getopt}
  s.version = "1.4.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Daniel J. Berger"]
  s.date = %q{2009-09-05}
  s.description = %q{      The getopt library provides two different command line option parsers.
      They are meant as easier and more convenient replacements for the
      command line parsers that ship as part of the Ruby standard library.
      Please see the README for additional comments.
}
  s.email = %q{djberg96@gmail.com}
  s.extra_rdoc_files = ["README", "CHANGES", "MANIFEST"]
  s.files = ["CHANGES", "examples/example_long.rb", "examples/example_std.rb", "getopt.gemspec", "lib/getopt/long.rb", "lib/getopt/std.rb", "MANIFEST", "Rakefile", "README", "test/test_getopt_long.rb", "test/test_getopt_std.rb"]
  s.homepage = %q{http://www.rubyforge.org/projects/shards}
  s.licenses = ["Artistic 2.0"]
  s.require_paths = ["lib"]
  s.rubyforge_project = %q{shards}
  s.rubygems_version = %q{1.3.7}
  s.summary = %q{Getopt::Std and Getopt::Long option parsers for Ruby}
  s.test_files = ["test/test_getopt_long.rb", "test/test_getopt_std.rb"]

  if s.respond_to? :specification_version then
    current_version = Gem::Specification::CURRENT_SPECIFICATION_VERSION
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<test-unit>, [">= 2.0.3"])
    else
      s.add_dependency(%q<test-unit>, [">= 2.0.3"])
    end
  else
    s.add_dependency(%q<test-unit>, [">= 2.0.3"])
  end
end
