@echo off
setlocal
pushd .

if exist tools\install.bat (
	cd tools
) else if exist ..\tools\install.bat (
	cd ..\tools
) else if exist ..\..\tools\install.bat (
	cd ..\..\tools
) else if exist ..\..\..\tools\install.bat (
	cd ..\..\..tools
) else if exist ..\..\..\..\tools\install.bat (
	cd ..\..\..\..\tools
) else if exist ..\..\..\..\..\tools\install.bat (
	cd ..\..\..\..\..\tools
) else if exist ..\..\..\..\..\..\tools\install.bat (
	cd ..\..\..\..\..\..\tools
) else if exist ..\framework\tools\install.bat (
	cd ..\framework\tools
) else if exist ..\..\framework\tools\install.bat (
	cd ..\..\framework\tools
) else if exist ..\..\..\framework\tools\install.bat (
	cd ..\..\..\framework\tools
) else if exist ..\..\..\..\framework\tools\install.bat (
	cd ..\..\..\..\framework\tools
) else if exist ..\..\..\..\..\framework\tools\install.bat (
	cd ..\..\..\..\..\framework\tools
) else if exist ..\..\..\..\..\..\framework\tools\install.bat (
	cd ..\..\..\..\..\..\framework\tools
) else (
	echo Cannot find appropriate tools directory . . .
	goto :EOF
)
set RS_TOOLSROOT=%CD%
cd ..\..
set RAGE_DIR=%CD%

popd
echo RS_TOOLSROOT=%RS_TOOLSROOT%
echo RAGE_DIR=%RAGE_DIR%

if not "%1"=="" (
	if exist "%1" (
		echo Loading %1 . . .
		%1
		goto :EOF
	) else if exist "%1.sln" (
		echo Loading %1.sln . . .
		%1.sln
		goto :EOF
	) else if exist "%1_2008.sln" (
		echo Loading %1_2008.sln . . .
		%1_2008.sln
		goto :EOF
	) else if exist "%1_2005.sln" (
		echo Loading %1_2005.sln . . .
		%1_2005.sln
		goto :EOF
	)

	for %%i in (*%1*_2008.sln) do (
		echo Loading %%i . . .
		%%i
		goto :EOF
	)
	for %%i in (*%1*_2005.sln) do (
		echo Loading %%i . . .
		%%i
		goto :EOF
	)
	echo Unable to find any partial match for %1 . . .
) else (
	for %%i in (*_2008.sln) do (
		echo Loading %%i . . .
		%%i
		goto :EOF
	)
	for %%i in (*_2005.sln) do (
		echo Loading %%i . . .
		%%i
		goto :EOF
	)
	echo Unable to find any *_2005 or *_2008.sln files . . .
)
