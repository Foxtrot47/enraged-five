@echo off
set outfile=x:\installed.txt
if not "%RSG_AUTOMATION_JOB_CACHE%"=="" set outfile=%RSG_AUTOMATION_JOB_CACHE%\installed.txt
if /i not "%~1"=="-nofile" if exist "%~dp0tee.exe" call "%~f0" -nofile %*|"%~dp0tee.exe" "%outfile%"&&if /i not "%1"=="-nopause" (pause&&goto:eof) else (goto:eof)
if /i "%~1"=="-nofile" shift
setlocal enabledelayedexpansion
title Installed SDKs and Support
call :getprojbranch "%RS_CODEBRANCH:\=" "%"
set stamp=
for /f "skip=1" %%t in ('wmic os get localdatetime') do if not defined stamp set stamp=%%t
echo.&&echo ================
echo %COMPUTERNAME% %USERNAME% %stamp%
echo ================
for /f "usebackq tokens=*" %%a in (`ver`) do echo %%a
set cphy=0&&for /f "usebackq tokens=1,* delims== " %%a in (`wmic cpu get NumberOfCores /value`) do if not "%%b"=="" set cphy=%%b
set clog=0&&for /f "usebackq tokens=1,* delims== " %%a in (`wmic cpu get NumberOfLogicalProcessors /value`) do if not "%%b"=="" set clog=%%b
set scur=0&&for /f "usebackq tokens=1,* delims== " %%a in (`wmic cpu get CurrentClockSpeed /value`) do if not "%%b"=="" set scur=%%b
set smax=0&&for /f "usebackq tokens=1,* delims== " %%a in (`wmic cpu get MaxClockSpeed /value`) do if not "%%b"=="" set smax=%%b
set cname=CPU&&for /f "usebackq tokens=1,* delims== " %%a in (`wmic cpu get Name /value`) do if not "%%b"=="" set cname=%%b
set memp=0&&for /f "usebackq tokens=1,* delims== " %%a in (`wmic computersystem get TotalPhysicalMemory /value`) do if not "%%b"=="" set memp=%%b
set netadr4=?.?.?.?&&for /f "usebackq tokens=2 delims=[]" %%a in (`ping -4 -a -n 1 %COMPUTERNAME% ^| findstr [`) do set netadr4=%%a
set netname=?&&for /f "usebackq tokens=2 delims=. " %%a in (`ping -4 -a -n 1 %netadr4% ^| findstr [`) do if "!netname!"=="?" set netname=%%a
echo CPU: %NUMBER_OF_PROCESSORS%/%clog%/%cphy% %scur%/%smax% %cname%
echo MEM: %memp%
echo NET: %netadr4% [%netname%]
echo ================
if "%SCE_ROOT_DIR%"=="" set %SCE_ROOT_DIR%=%PROGRAMFILES(X86)%\SCE
echo.&&echo PS4 SDK Support Tools:
if not exist "%SCE_ROOT_DIR%\ORBIS\Tools\Target Manager Server\bin\orbis-ctrl.exe" echo.  [not installed]&&goto:skipsdktools
for /f "usebackq tokens=1,2" %%a in (`"%SCE_ROOT_DIR%\ORBIS\Tools\Target Manager Server\bin\orbis-ctrl.exe" version`) do (
set tname=          %%a
echo.  !tname:~-16!  %%b
)
set testver=
set chkfile=%SCE_ROOT_DIR%\ORBIS\Tools\Neighborhood\bin\OrbisNeighborhoodx64.dll
if exist "%chkfile%" (
for /f "usebackq tokens=1,2 delims==" %%a in (`wmic datafile where name^="%chkfile:\=\\%" get version /value`) do if /i "%%a"=="version" set testver=%%b
if "%testver%"=="" echo.     Neighborhood:  [unknown]
if not "%testver%"=="" echo.     Neighborhood:  %testver%
) else echo.     Neighborhood:  [not installed]
if exist "%SCE_ROOT_DIR%\ORBIS\Tools\Remote Viewer\bin\OrbisRemoteViewer.exe" (
set testver=
set "toolfile=%SCE_ROOT_DIR%\ORBIS\Tools\Remote Viewer\bin\OrbisRemoteViewer.exe"
for /f "usebackq tokens=1,2 delims==" %%a in (`wmic datafile where name^="!toolfile:\=\\!" get version /value`) do if /i "%%a"=="version" set testver=%%b
if "!testver!"=="" set testver=[unknown]
echo.    Remote Viewer:  !testver!
) else echo.    Remote Viewer:  [not installed]
:skipsdktools
echo.&&echo ================
echo.&&echo Available PS4 SDK versions on this PC:
if exist "%RS_CODEBRANCH%\sdk\orbis" (
set sdkdir=%RS_CODEBRANCH%\sdk\orbis
call :setsdkversion "!sdkdir!"
if not "!sdkversion!"=="?" echo.  !sdkversion!
if "%SCE_ORBIS_SDK_DIR%"=="" set SCE_ORBIS_SDK_DIR=%RS_CODEBRANCH%\sdk\orbis
)
if not "%SCE_ORBIS_SDK_DIR%"=="" if exist "%SCE_ORBIS_SDK_DIR%" (
if /i "!SCE_ORBIS_SDK_DIR:%RS_CODEBRANCH%=!"=="%SCE_ORBIS_SDK_DIR%" (
pushd "%SCE_ORBIS_SDK_DIR%\.."
for /d %%d in (*) do (
set sdkdir=!cd!\%%d
call :setsdkversion "!sdkdir!"
if not "!sdkversion!"=="?" echo.  !sdkversion!
)
popd
))
set rs3p=%RAGE_3RDPARTY%\sdk\orbis
if not exist "%rs3p%" set rs3p=x:\3rdparty\dev\sdk\orbis
if exist "%rs3p%" if "%SCE_ORBIS_SDK_DIR%"=="!SCE_ORBIS_SDK_DIR:%rs3p%=!" (
pushd ""%rs3p%"
for /d %%d in (*) do (
set sdkdir=!cd!\%%d
call :setsdkversion "!sdkdir!"
if not "!sdkversion!"=="?" echo.  !sdkversion!
)
popd
)
if "%SCE_ORBIS_SDK_DIR%"=="!SCE_ORBIS_SDK_DIR:%SCE_ROOT_DIR%=!" if exist "%SCE_ROOT_DIR%\orbis sdks" (
pushd "%SCE_ROOT_DIR%\orbis sdks"
for /d %%d in (*) do (
set sdkdir=!cd!\%%d
call :setsdkversion "!sdkdir!"
if not "!sdkversion!"=="?" echo.  !sdkversion!
)
popd
)
if "%SCE_ORBIS_SDK_DIR%"=="" goto:showfallbackps4
set sdkdir=%SCE_ORBIS_SDK_DIR%
call :setsdkversion "%sdkdir%"
:showfallbackps4
echo.&&echo Fallback PS4 SDK (default if not overridden by project):
if "%SCE_ORBIS_SDK_DIR%"=="" echo.  [unset]
if not "%SCE_ORBIS_SDK_DIR%"=="" echo.  %sdkversion%
:doneps4
echo.&&echo ================
set orbisctrl=%SCE_ROOT_DIR%\ORBIS\Tools\Target Manager Server\bin\orbis-ctrl.exe
if exist "%orbisctrl%" (
set defps4=
for /f "usebackq tokens=1" %%a in (`"%orbisctrl%" default 2^>nul`) do set defps4=%%a
if not "!defps4!"=="" (
echo.&&echo Default PS4 console:  [!defps4!]
"%orbisctrl%" info !defps4! | findstr /i /c:"SdkVersion:" /c:"IpAddress:" /c:"MacAddress:" /c:"GameLanMacAddress:" /c:"GameLanStatus:" /c:" Name:" /c:"CommsPath:"
) else (
echo.&&echo No default PS4 console set.
)
echo.&&echo ================
)
goto:getps5
goto:eof
:setsdkversion
set sdkkind=
set sdkversion=?
if "%sdkdir%"=="" goto:eof
if not "%sdkdir%"=="!sdkdir:%SCE_ROOT_DIR%=!" set sdkkind=system/global
if /i "%sdkdir:~0,11%"=="x:\3rdparty" set sdkkind=rockstar/global
if not "%sdkdir%"=="!sdkdir:%RS_PROJROOT%=!" set sdkkind=current project: %RS_PROJECT%
if not "%sdkdir%"=="!sdkdir:%RS_CODEBRANCH%=!" set sdkkind=current project/branch: %RS_PROJECT%/%RS_BRANCH%
if "%sdkkind%"=="" set sdkkind=%SCE_ORBIS_SDK_DIR%
if not exist "%sdkdir%\target\include\sdk_version.h" goto:eof
pushd "%sdkdir%\target\include"
for /f "tokens=1,2,3" %%a in (sdk_version.h) do if /i "%%a"=="#define" if /i "%%b"=="SCE_ORBIS_SDK_VERSION" set sdkversion=%%c
popd
if "%sdkversion%"=="?" goto:eof
set sdkversion=%sdkversion:(=%
set sdkversion=%sdkversion:)=%
set sdkversion=%sdkversion:u=%
set sdkversion=%sdkversion:0x0=%
set sdkversion=%sdkversion:0x=%
set sdkversion=%sdkversion:~0,-6%.%sdkversion:~-6,3%.%sdkversion:~-3%
if not "%sdkkind%"=="%sdkkind:/global=%" set sdkversion=%sdkversion%  (%sdkkind%: %~nx1)&&goto:eof
set sdkversion=%sdkversion%  (%sdkkind%)
goto:eof

:getps5
echo.&&echo PS5 SDK Support Tools:
if not exist "%SCE_ROOT_DIR%\Prospero\Tools\Target Manager Server\bin\prospero-ctrl.exe" echo.  [not installed]&&goto:skipsdktoolsps5
set testver=
set "chkfile=%SCE_ROOT_DIR%\Prospero\Tools\Target Manager Server\bin\prospero-ctrl.exe"
for /f "usebackq tokens=1,2 delims==" %%a in (`wmic datafile where name^="%chkfile:\=\\%" get version /value`) do if /i "%%a"=="version" set testver=%%b
if "%testver%"=="" echo.    prospero-ctrl:  [unknown]
if not "%testver%"=="" echo.    prospero-ctrl:  %testver%
for /f "usebackq tokens=1,2" %%a in (`"%SCE_ROOT_DIR%\Prospero\Tools\Target Manager Server\bin\prospero-ctrl.exe" diagnostics version`) do (
set tname=          %%a
if /i "!tname:~-8!"=="version:" (
echo.  !tname:~-16!  %%b
))
set testver=
set "chkfile=%SCE_ROOT_DIR%\Prospero\Tools\Target Manager\bin\ProsperoTargetManagerx64.dll"
for /f "usebackq tokens=1,2 delims==" %%a in (`wmic datafile where name^="%chkfile:\=\\%" get version /value`) do if /i "%%a"=="version" set testver=%%b
if "%testver%"=="" echo.   Target Manager:  [unknown]
if not "%testver%"=="" echo.   Target Manager:  %testver%
if exist "%SCE_ROOT_DIR%\Prospero\Tools\Remote Viewer\bin\ProsperoRemoteViewer.exe" (
set testver=
set "toolfile=%SCE_ROOT_DIR%\Prospero\Tools\Remote Viewer\bin\ProsperoRemoteViewer.exe"
for /f "usebackq tokens=1,2 delims==" %%a in (`wmic datafile where name^="!toolfile:\=\\!" get version /value`) do if /i "%%a"=="version" set testver=%%b
if "!testver!"=="" set testver=[unknown]
echo.    Remote Viewer:  !testver!
) else echo.    Remote Viewer:  [not installed]
:skipsdktoolsps5
echo.&&echo ================
echo.&&echo Available PS5 SDK versions on this PC:
if exist "%RS_CODEBRANCH%\sdk\prospero" (
set sdkdir=%RS_CODEBRANCH%\sdk\prospero
call :setsdkversionps5 "!sdkdir!"
if not "!sdkversion!"=="?" echo.  !sdkversion!
if "%SCE_PROSPERO_SDK_DIR%"=="" set SCE_PROSPERO_SDK_DIR=%RS_CODEBRANCH%\sdk\prospero
)
if not "%SCE_PROSPERO_SDK_DIR%"=="" if exist "%SCE_PROSPERO_SDK_DIR%" (
if /i "!SCE_PROSPERO_SDK_DIR:%RS_CODEBRANCH%=!"=="%SCE_PROSPERO_SDK_DIR%" (
pushd "%SCE_PROSPERO_SDK_DIR%\.."
for /d %%d in (*) do (
set sdkdir=!cd!\%%d
call :setsdkversionps5 "!sdkdir!"
if not "!sdkversion!"=="?" echo.  !sdkversion!
)
popd
))
set rs3p=%RAGE_3RDPARTY%\sdk\prospero
if not exist "%rs3p%" set rs3p=x:\3rdparty\dev\sdk\prospero
if exist "%rs3p%" if "%SCE_ORBIS_SDK_DIR%"=="!SCE_PROSPERO_SDK_DIR:%rs3p%=!" (
pushd ""%rs3p%"
for /d %%d in (*) do (
set sdkdir=!cd!\%%d
call :setsdkversionps5 "!sdkdir!"
if not "!sdkversion!"=="?" echo.  !sdkversion!
)
popd
)
if "%SCE_PROSPERO_SDK_DIR%"=="!SCE_PROSPERO_SDK_DIR:%SCE_ROOT_DIR%=!" if exist "%SCE_ROOT_DIR%\prospero sdks" (
pushd "%SCE_ROOT_DIR%\prospero sdks"
for /d %%d in (*) do (
set sdkdir=!cd!\%%d
call :setsdkversionps5 "!sdkdir!"
if not "!sdkversion!"=="?" echo.  !sdkversion!
)
popd
)
if "%SCE_PROSPERO_SDK_DIR%"=="" goto:showfallbackps5
set sdkdir=%SCE_PROSPERO_SDK_DIR%
call :setsdkversionps5 "%sdkdir%"
:showfallbackps5
echo.&&echo Fallback PS5 SDK (default if not overridden by project):
if "%SCE_PROSPERO_SDK_DIR%"=="" echo.  [unset]
if not "%SCE_PROSPERO_SDK_DIR%"=="" echo.  %sdkversion%
:doneps5
echo.&&echo ================
set prosperoctrl=%SCE_ROOT_DIR%\Prospero\Tools\Target Manager Server\bin\prospero-ctrl.exe
if exist "%prosperoctrl%" (
set defps5=
for /f "usebackq tokens=1,2 delims=-^ " %%a in (`"%prosperoctrl%" target get-default 2^>nul`) do if /i "%%a"=="hostname:" set defps5=%%b
if not "!defps5!"=="" (
echo.&&echo Default PS5 console:  [!defps5!]
"%prosperoctrl%" target info /target:!defps5! | findstr /i /c:"SdkVersion:" /c:"IpAddress:" /c:"DEVLinkSpeed:" /c:"MacAddress:" /c:"GameLanStatus:" /c:"DevkitName:" /c:"CommsPath:"
) else (
echo.&&echo No default PS5 console set.
)
echo.&&echo ================
)
goto:getxdk
goto:eof
:setsdkversionps5
set sdkkind=
set sdkversion=?
if "%sdkdir%"=="" goto:eof
if not "%sdkdir%"=="!sdkdir:%SCE_ROOT_DIR%=!" set sdkkind=system/global
if /i "%sdkdir:~0,11%"=="x:\3rdparty" set sdkkind=rockstar/global
if not "%sdkdir%"=="!sdkdir:%RS_PROJROOT%=!" set sdkkind=current project: %RS_PROJECT%
if not "%sdkdir%"=="!sdkdir:%RS_CODEBRANCH%=!" set sdkkind=current project/branch: %RS_PROJECT%/%RS_BRANCH%
if "%sdkkind%"=="" set sdkkind=%SCE_PROSPERO_SDK_DIR%
if not exist "%sdkdir%\target\include\sdk_version.h" goto:eof
pushd "%sdkdir%\target\include"
for /f "tokens=1,2,3" %%a in (sdk_version.h) do if /i "%%a"=="#define" if /i "%%b"=="SCE_PROSPERO_SDK_VERSION" set sdkversion=%%c
popd
if "%sdkversion%"=="?" goto:eof
set sdkversion=%sdkversion:(=%
set sdkversion=%sdkversion:)=%
set sdkversion=%sdkversion:u=%
set sdkversion=%sdkversion:0x0=%
set sdkversion=%sdkversion:0x=%
set sdkversion=%sdkversion:~0,-6%.%sdkversion:~-6,3%.%sdkversion:~-3%
if not "%sdkkind%"=="%sdkkind:/global=%" set sdkversion=%sdkversion%  (%sdkkind%: %~nx1)&&goto:eof
set sdkversion=%sdkversion%  (%sdkkind%)
goto:eof

:getxdk
set xdkdir=%DURANGOXDK%
setlocal
if "%XDKEDITION%"=="" (
if exist "%DURANGOXDK%\bin\xbversioninfo.exe" (
for /f "usebackq tokens=1,2* delims==" %%a in (`"%DURANGOXDK%\bin\xbversioninfo.exe" /xdk /b`) do if "!XDKEDITION!"=="" if /i %%ax==xdkeditionx set XDKEDITION=%%b
) else if exist "%DURANGOXDK%\xdk\DurangoVars.cmd" (
pushd .
call "%DURANGOXDK%\xdk\DurangoVars.cmd" XDKVS2015 >nul
popd
))
title Installed SDKs
if not "%XDKEDITION%"=="" set xdkdir=%DURANGOXDK%\%XDKEDITION%
call :setxdkversion
echo.&&echo Current (default) Xbox One XDK version:
echo.  %xdkversion%
endlocal
echo.&&echo All available Xbox One XDK versions on this PC:
call :setxdkversion
if not "!xdkversion!"=="?" echo.  !xdkversion!
pushd "%DURANGOXDK%"
for /d %%d in (*) do (
set xdkdir=!cd!\%%d
call :setxdkversion
if not "!xdkversion!"=="?" echo.  !xdkversion!
)
popd
echo.&&echo ================
goto:getgdk
:setxdkversion
set xdkversion=?
set xdkversionstr=unknown
set xdkversionmm=?
set xdkversionyyyy=?
set xdkversionqfe=
set xdkversioned=
set xdkversionfull=?
if not exist "%xdkdir%\xdk\include\shared\xdk.h" goto:eof
pushd "%xdkdir%\xdk\include\shared"
for /f "tokens=1,2,3,*" %%a in (xdk.h) do if /i "%%a"=="#define" (
if /i "%%b"=="_XDK_VER_STRING" set xdkversionstr=%%c %%d
if /i "%%b"=="_XDK_MM" call :setmonth %%c
if /i "%%b"=="_XDK_FULLYY" set xdkversionyyyy=%%c
if /i "%%b"=="_XDK_QFE_NUM" if not "%%c"=="0" set xdkversionqfe= QFE %%c
if /i "%%b"=="_XDK_VER" set xdkversion=%%c
if /i "%%b"=="_XDK_EDITION" set xdkversioned=%%c
if /i "%%b"=="_XDKVER_PRODUCTBUILDVER_FULL" set xdkversionfull=%%c
)
popd
if "%xdkversion%"=="?" goto:eof
if not "%xdkversionfull%"=="?" set xdkversion=%xdkversioned%
if not "%xdkversionmm%"=="?" set xdkversionstr=%xdkversionmm% %xdkversionyyyy% XDK%xdkversionqfe%
set xdkversion=%xdkversion: =% (%xdkversionstr:"=%)
if not "%xdkversionfull%"=="?" set xdkversion=%xdkversion% or %xdkversionfull%
goto:eof

:setmonth
if "%1"=="01" set xdkversionmm=January
if "%1"=="1" set xdkversionmm=January
if "%1"=="02" set xdkversionmm=February
if "%1"=="2" set xdkversionmm=February
if "%1"=="03" set xdkversionmm=March
if "%1"=="3" set xdkversionmm=March
if "%1"=="04" set xdkversionmm=April
if "%1"=="4" set xdkversionmm=April
if "%1"=="05" set xdkversionmm=May
if "%1"=="5" set xdkversionmm=May
if "%1"=="06" set xdkversionmm=June
if "%1"=="6" set xdkversionmm=June
if "%1"=="07" set xdkversionmm=July
if "%1"=="7" set xdkversionmm=July
if "%1"=="08" set xdkversionmm=August
if "%1"=="8" set xdkversionmm=August
if "%1"=="09" set xdkversionmm=September
if "%1"=="9" set xdkversionmm=September
if "%1"=="10" set xdkversionmm=October
if "%1"=="11" set xdkversionmm=November
if "%1"=="12" set xdkversionmm=December
goto:eof


:getgdk
set gdkdir=%GameDK%
setlocal
if "%GXDKEDITION%"=="" (
if exist "%gdkdir%\bin\xbversioninfo.exe" (
for /f "usebackq tokens=1,2* delims==" %%a in (`"%gdkdir%\bin\xbversioninfo.exe" /xdk /b`) do if "!GXDKEDITION!"=="" if /i %%ax==gxdkeditionx set GXDKEDITION=%%b
) else if exist "%gdkdir%\Command Prompts\GamingXboxVars.cmd" (
pushd .
call "%gdkdir%\Command Prompts\GamingXboxVars.cmd" GamingXboxScarlettVS2017 >nul
popd
))
title Installed SDKs
if not "%GXDKEDITION%"=="" set gdkdir=%gdkdir%\%GXDKEDITION%
call :setgxdkversion
echo.&&echo Current (default) GDK version:
echo.  %gxdkversion%
endlocal
echo.&&echo All available GDK versions on this PC:
call :setgxdkversion
if not "!gxdkversion!"=="?" echo.  !gxdkversion!
pushd "%gdkdir%"
for /d %%d in (*) do (
set gdkdir=!cd!\%%d
call :setgxdkversion
if not "!gxdkversion!"=="?" echo.  !gxdkversion!
)
popd
echo.&&echo ================
set defxbox=
set xbconn=%gdkdir%\bin\xbconnect.exe
if not exist "%xbconn%" set xbconn=%DURANGOXDK%\bin\xbconnect.exe
if exist "%xbconn%" (
for /f "usebackq tokens=1" %%a in (`"%xbconn%" /b`) do if "!defxbox!"=="" set defxbox=%%a
echo.&&echo Default Xbox console:  [!defxbox!]
"%xbconn%" /q !defxbox! | findstr /i /c:"host:" /c:"system:"
if exist "!xbconn:xbconnect=xbconfig!" "!xbconn:xbconnect=xbconfig!" /d /x:!defxbox! | findstr /i /c:"consolemode:" /c:"crashdumptype:" /c:"debugnic:" /c:"enablekerneldebugging:" /c:"enablevideocrashdumps:" /c:"extratitlememory:" /c:"profilingmode:" /c:"toolingmemoryoverflow:" /c:"debugmemorymode:" /c:"hostname:" /c:"ipaddress:" /c:"macaddress:" /c:"sandboxid:" /c:"osupdatepolicy:" /c:"defaultuser:"
echo.&&echo ================
)
goto:getdxsdk
:setgxdkversion
set gxdkversion=?
set gxdkversionstr=unknown
set gxdkversionmm=?
set gxdkversionyyyy=?
set gxdkversionqfe=
set gxdkversioned=
set gxdkversionfull=?
if not exist "%gdkdir%\gxdk\gamekit\include\gxdk.h" goto:eof
pushd "%gdkdir%\gxdk\gamekit\include"
for /f "tokens=1,2,3,*" %%a in (gxdk.h) do if /i "%%a"=="#define" (
rem if /i "%%b"=="_GXDK_VER_STRING" set gxdkversionstr=%%c %%d
if /i "%%b"=="_GXDK_MM" call :setmonthgdk %%c
if /i "%%b"=="_GXDK_FULLYY" set gxdkversionyyyy=%%c
if /i "%%b"=="_GXDK_QFE_NUM" if not "%%c"=="0" set gxdkversionqfe= QFE %%c
if /i "%%b"=="_GXDK_VER" set gxdkversion=%%c
if /i "%%b"=="_GXDK_EDITION" set gxdkversioned=%%c
if /i "%%b"=="_GXDKVER_PRODUCTBUILDVER_FULL" set gxdkversionfull=%%c
)
popd
if "%gxdkversion%"=="?" goto:eof
if not "%gxdkversionfull%"=="?" set gxdkversion=%gxdkversioned%
if not "%gxdkversionmm%"=="?" set gxdkversionstr=%gxdkversionmm% %gxdkversionyyyy%%gxdkversionqfe%
set gxdkversion=%gxdkversion: =% (%gxdkversionstr:"=%)
if not "%gxdkversionfull%"=="?" set gxdkversion=%gxdkversion% or %gxdkversionfull%
goto:eof

:setmonthgdk
if "%1"=="01" set gxdkversionmm=January
if "%1"=="02" set gxdkversionmm=February
if "%1"=="03" set gxdkversionmm=March
if "%1"=="04" set gxdkversionmm=April
if "%1"=="05" set gxdkversionmm=May
if "%1"=="06" set gxdkversionmm=June
if "%1"=="07" set gxdkversionmm=July
if "%1"=="08" set gxdkversionmm=August
if "%1"=="09" set gxdkversionmm=September
if "%1"=="10" set gxdkversionmm=October
if "%1"=="11" set gxdkversionmm=November
if "%1"=="12" set gxdkversionmm=December
goto:eof

:getdxsdk
set dxsdkdir=%DXSDK_DIR%
call :setdxsdkversion
echo.&&echo Current DirectX SDK version:
echo.  %dxsdkversion%
echo.&&echo ================
goto:getwinsdkinfo
:setdxsdkversion
set dxsdkversion=?
if not exist "%dxsdkdir%\include\dxsdkver.h" goto:eof
pushd "%dxsdkdir%\include"
for /f "tokens=1,2,3" %%a in (dxsdkver.h) do if /i "%%a"=="#define" (
if /i "%%b"=="_DXSDK_PRODUCT_MAJOR" set dxsdkversion=%%c
if /i "%%b"=="_DXSDK_PRODUCT_MINOR" set dxsdkversion2=%%c
if /i "%%b"=="_DXSDK_BUILD_MAJOR" set dxsdkversion3=%%c
if /i "%%b"=="_DXSDK_BUILD_MINOR" set dxsdkversion4=%%c
)
popd
if "%dxsdkversion%"=="?" goto:eof
set dxsdkdir=!dxsdkdir:%PROGRAMFILES(X86)%=!
set dxsdkdir=!dxsdkdir:%PROGRAMFILES%=!
set dxsdkdir=%dxsdkdir:Microsoft DirectX SDK =%
set dxsdkversion=%dxsdkversion%.%dxsdkversion2%.%dxsdkversion3%.%dxsdkversion4% %dxsdkdir:\=%
goto:eof

:getwinsdkinfo
set winsdkdir=%PROGRAMFILES(X86)%\windows kits
if exist "c:\windows kits" set winsdkdir=c:\windows kits
call :setwinsdkversion
echo.&&echo Current (default) Windows SDK version:
echo.  %winsdkversion%
echo.&&echo Available Windows SDK versions on this PC:
pushd "%PROGRAMFILES%\microsoft sdks\windows"
for /d %%k in (v6*) do set wsdkver=%%k&&echo.  !wsdkver:v=!
popd
pushd "%PROGRAMFILES(X86)%\windows kits"
for /d %%k in (7*) do echo.  %%k
for /d %%k in (8*) do echo.  %%k
popd
pushd %winsdkdir%
if exist "10\include" cd "10\include"
for /d %%k in (10*) do echo.  %%k
popd
echo.&&echo ================
goto:getyetiinfo
:setwinsdkversion
set winsdkversion=?
if exist "%winsdkdir%\10" (
pushd "!winsdkdir!\10"
for /f "tokens=1,* delims== " %%p in (sdkmanifest.xml) do if /i "%%p"=="platformidentity" set winsdkversion=%%~q
popd
)
if "!winsdkversion!"=="" set winsdkversion=?
set winsdkversion=!winsdkversion: =!
set winsdkversion=!winsdkversion:"=!
set winsdkversion=!winsdkversion:uap,version=!
if "%winsdkversion:~0,1%"=="=" set "winsdkversion=!winsdkversion:~1!"
goto:eof

:getyetiinfo
if "%GGP_SDK_PATH%"=="" goto:doneyeti
if not exist "%GGP_SDK_PATH%" goto:doneyeti
pushd "%GGP_SDK_PATH%"
set YETI_SDK_VERSION=
for /f "tokens=1,*" %%v in (VERSION) do if "!YETI_SDK_VERSION!"=="" set YETI_SDK_VERSION=%%v
echo.&&echo Current (default) GGP/Stadia SDK version:
echo.  %YETI_SDK_VERSION%
echo.&&echo ================
popd
:doneyeti

:getvsinfo
echo.
set hasvs=
for %%v in (8.0 9.0 10.0 11.0 12.0 14.0 15.0 16.0 17.0) do (
reg query HKEY_CURRENT_USER\Software\Microsoft\VisualStudio\%%v >nul 2>&1
if !errorlevel!==0 (
set hasvs=y
) else (
if exist "%PROGRAMFILES(X86)%\Microsoft Visual Studio %%v\Common7\IDE\devenv.exe" set hasvs=y
))

if "%hasvs%"=="" (
for %%v in (2012 2015 2017 2019) do (
if exist "%PROGRAMFILES(X86)%\Microsoft Visual Studio\%%v\Professional\Common7\IDE\devenv.exe" set hasvs=y
))

if "%hasvs%"=="" echo Visual Studio not installed.&&goto:donevs
echo Installed Visual Studio versions:
echo.
for %%v in ("8.0 2005" "9.0 2008" "10.0 2010" "11.0 2012" "12.0 2013" "14.0 2015" "15.0 2017" "16.0 2019" "17.0 2022") do call :checkonvs %%~v
:donevs

reg query HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Xoreax\IncrediBuild\Builder >nul 2>&1
set errlev=!errorlevel!
echo.&&echo ================
echo.
if not !errlev!==0 echo Incredibuild not installed.&&goto:doneib
echo Incredibuild info:
set ibcoord=
call :showibinfo Builder VersionText "Installed version"
call :showibinfo BuildService CoordHost "Coordinator" setvar ibcoord
if not "%ibcoord%"=="" (
set ibnetadr4=?.?.?.?&&for /f "usebackq tokens=2 delims=[]" %%a in (`ping -4 -a -n 1 %ibcoord% ^| findstr [`) do set ibnetadr4=%%a
set ibnetname=?&&for /f "usebackq tokens=2 delims=. " %%a in (`ping -4 -a -n 1 !ibnetadr4! ^| findstr [`) do if "!ibnetname!"=="?" set ibnetname=%%a
echo.           Coord details: !ibnetadr4! [!ibnetname!]
)
call :showibinfo BuildService Group "Group"
call :showibinfo BuildService AgentDescription "Description"
call :showibinfo BuildService MaxConcurrentBuilds "Concurrent builds"
call :showibinfo BuildService MinLocalCoresPerBuild "Min local cores"
call :showibinfo Builder ForceCPUCount_WhenInitiator "Initiator cores" zeroall
call :showibinfo Builder ForceCPUCount_WhenHelper "Helper cores" zeroall
call :showibinfo Builder MaxHelpers "Max cores" zeroall
call :showibinfo Builder DetectedCoreCount "Cores detected"
echo.         Cores available: %NUMBER_OF_PROCESSORS% (%PROCESSOR_REVISION%:%PROCESSOR_IDENTIFIER%)
call :showibinfo Builder HelperTimeoutSec "Helper timeout"
call :showibinfo Builder TerminateInactiveHelper "Kill inactive helpers" yesno
call :showibinfo Builder AllowDoubleTargets "Restart locally" yesno
call :showibinfo Builder AvoidLocalExec "Avoid local" yesno
call :showibinfo Builder OnlyFailLocally "Only fail locally" yesno
call :showibinfo Builder Standalone "Standalone mode" yesno
call :showibinfo Builder PredictedExecutionMode "Predicted exec mode" yesno
call :showibinfo Builder AllowParallelCustomSteps "Parallel custom steps" yesno
call :showibinfo Builder CustomStepVs10Support "Special custom steps" yesno
call :showibinfo Builder UseMSBuild "Use MSBuild" yesno
call :showibinfo Builder MSBuildMaxInstances "Max MSBuild inst"
call :showibinfo Builder ClangPathConvert "Clang path type"
call :showibinfo Builder MaxConcurrentPDBs "Max concurrent PDBs"
echo.
set ibpath=
for /f "usebackq tokens=1,2*" %%v in (`reg query HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Xoreax\IncrediBuild\Builder /v Folder`) do if !errorlevel!==0 if /i "%%v"=="Folder" set ibpath=%%x
if "%ibpath%"=="" goto:doneib
if not exist "%ibpath%" goto:doneib
set ibpkg=
for /f "usebackq delims=" %%a in (`"%ibpath%\BuildConsole.exe" /querylicense`) do (
if /i "%%a"=="packages installed:" set ibpkg=8
if not "!ibpkg!"=="" echo %%a
)
goto:doneib
:showibinfo
set ibval=
for /f "usebackq tokens=1,2*" %%v in (`reg query HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Xoreax\IncrediBuild\%~1 /v %~2 2^>^&1`) do if !errorlevel!==0 if /i "%%v"=="%~2" set ibval=%%x
if "!ibval!"=="" goto:eof
if /i "%~4"=="setvar" set %~5=!ibval!
if /i "%~4"=="yesno" (
if "!ibval!"=="0" (set ibval=no) else (set ibval=yes [!ibval!])
) else if /i "%~4"=="zeroall" if "!ibval!"=="0" set ibval=all
set ibkey=                                %~3
echo !ibkey:~-24!: !ibval!
)
goto:eof
:doneib

:doneall
echo.&&echo ================
echo.
::if /i not "%1"=="-nopause" pause
goto:eof

:checkonvs
reg query HKEY_CURRENT_USER\Software\Microsoft\VisualStudio\%1 >nul 2>&1
if !errorlevel!==0 ( reg query HKEY_CURRENT_USER\Software\Microsoft\VisualStudio\%1_Config\SplashInfo /v EnvVersion >nul 2>&1
if !errorlevel!==0 ( for /f "usebackq tokens=1,2*" %%v in (`reg query HKEY_CURRENT_USER\Software\Microsoft\VisualStudio\%1_Config\SplashInfo /v EnvVersion`) do if !errorlevel!==0 if /i "%%v"=="EnvVersion" echo Visual Studio %2 Version %%x
) else ( reg query HKEY_CURRENT_USER\Software\Microsoft\VisualStudio\%1\Profile /v BuildNum >nul 2>&1
if !errorlevel!==0 ( for /f "usebackq tokens=1,2*" %%v in (`reg query HKEY_CURRENT_USER\Software\Microsoft\VisualStudio\%1\Profile /v BuildNum`) do if !errorlevel!==0 if /i "%%v"=="BuildNum" echo Visual Studio %2 Version %%x
) else ( call :checkvsfiles %*
))) else ( call :checkvsfiles %* )
goto:eof

:checkvsfiles
set vspath=%PROGRAMFILES(X86)%\Microsoft Visual Studio\%2\Professional
if not exist "%vspath%" set vspath=%PROGRAMFILES(X86)%\Microsoft Visual Studio %1
if not exist "%vspath%\Common7\IDE\devenv.exe" goto:eof
if not exist "%vspath%\Common7\IDE\devenv.isolation.ini" (
set testver=
for /f "usebackq tokens=1,2 delims==" %%a in (`wmic datafile where name^="!vspath:\=\\!\\Common7\\IDE\\devenv.exe" get version /value`) do if /i "%%a"=="version" set testver=%%b
if "!testver!"=="" set testver=[unknown version]
echo Visual Studio %2 !testver!
goto:eof
)
set vsver=
pushd "%vspath%\Common7\IDE"
for /f "tokens=1,2* delims==" %%u in (devenv.isolation.ini) do (if /i "%%u"=="semanticversion" set vsver= v%%v)
popd
if not "!vsver!"=="" set vsver=!vsver:+=  [!]
echo Visual Studio %2!vsver!
call :checkvsi %* "%vspath%"
call :checkxbvsi %* "%vspath%"
call :checkibvsi %* "%vspath%"
call :checkts %* "%vspath%"
goto:eof

:checkts
if exist "%~3\VC\Tools\MSVC" (
echo.    Toolsets present
pushd "%~3\VC\Tools\MSVC"
for /d %%d in (*) do if exist "%%d\bin\Hostx64\x64\cl.exe" echo.      %%d
popd
)
goto:eof

:checkvsi
if exist "%~3\Common7\IDE\Extensions\SCE\SceVSI\SceVSI.dll" (
set testver=
set "vsifile=%~3\Common7\IDE\Extensions\SCE\SceVSI\SceVSI.dll"
for /f "usebackq tokens=1,2 delims==" %%a in (`wmic datafile where name^="!vsifile:\=\\!" get version /value`) do if /i "%%a"=="version" set testver=%%b
if "!testver!"=="" set testver=[unknown]
call:cleantestver !testver!
echo.         SCE VSI: !testver!
) else echo.         SCE VSI: [not installed]

if exist "%~3\Common7\IDE\Extensions\SCE\Debugger Extensions\SIEDebuggerPackage.dll" (
set testver=
set "vsifile=%~3\Common7\IDE\Extensions\SCE\Debugger Extensions\SIEDebuggerPackage.dll"
for /f "usebackq tokens=1,2 delims==" %%a in (`wmic datafile where name^="!vsifile:\=\\!" get version /value`) do if /i "%%a"=="version" set testver=%%b
if "!testver!"=="" set testver=[unknown]
call:cleantestver !testver!
echo.    SCE debugger: !testver!
) else echo.    SCE debugger: [not installed]

if exist "%~3\Common7\IDE\Extensions\SCE\Prospero Debugger Extensions\ProsperoDebuggerPackage.dll" (
set testver=
set "vsifile=%~3\Common7\IDE\Extensions\SCE\Prospero Debugger Extensions\ProsperoDebuggerPackage.dll"
for /f "usebackq tokens=1,2 delims==" %%a in (`wmic datafile where name^="!vsifile:\=\\!" get version /value`) do if /i "%%a"=="version" set testver=%%b
if "!testver!"=="" set testver=[unknown]
call:cleantestver !testver!
echo.    PS5 debugger: !testver!
) else echo.    PS5 debugger: [not installed]

if exist "%~3\Common7\IDE\Extensions\SCE\Prospero\Razor\ProsperoRazorVSPlugin.dll" (
set testver=
set "vsifile=%~3\Common7\IDE\Extensions\SCE\Prospero\Razor\ProsperoRazorVSPlugin.dll"
for /f "usebackq tokens=1,2 delims==" %%a in (`wmic datafile where name^="!vsifile:\=\\!" get version /value`) do if /i "%%a"=="version" set testver=%%b
if "!testver!"=="" set testver=[unknown]
call:cleantestver !testver!
echo.       PS5 Razor: !testver!
) else echo.       PS5 Razor: [not installed]

goto:eof

:checkxbvsi
if exist "%~3\Common7\IDE\Extensions" (
pushd "%~3\Common7\IDE\Extensions"
set gdkvsipath=
for /f "usebackq tokens=*" %%a in (`dir /s /b Microsoft.Gaming.Xbox.dll 2^>nul`) do if "!gdkvsipath!"=="" set gdkvsipath=%%a
if exist "!gdkvsipath!" (
set testver=
set "vsifile=!gdkvsipath!"
for /f "usebackq tokens=1,2 delims==" %%a in (`wmic datafile where name^="!vsifile:\=\\!" get version /value`) do if /i "%%a"=="version" set testver=%%b
if "!testver!"=="" set testver=[unknown]
call:cleantestver !testver!
echo.         GDK VSI: !testver!
) else echo.         GDK VSI: [not installed]
popd )
goto:eof

:checkibvsi
if exist "%~3\Common7\IDE\Extensions\IncredibuildExtension\IncredibuildMenu.dll" (
set testver=
set "vsifile=%~3\Common7\IDE\Extensions\IncredibuildExtension\IncredibuildMenu.dll"
for /f "usebackq tokens=1,2 delims==" %%a in (`wmic datafile where name^="!vsifile:\=\\!" get version /value`) do if /i "%%a"=="version" set testver=%%b
if "!testver!"=="" set testver=[unknown]
call:cleantestver !testver!
echo.          IB VSI: !testver!
) else echo.          IB VSI: [not installed]
goto:eof

:cleantestver
set testver=%~1
goto:eof

:getvsiver
if "%~1"=="" set vsiver=unknown&&goto:eof
if /i "%~1"=="version" set vsiver=v%~2&&goto:eof
shift
goto:getvsiver

:getprojbranch
if "%RS_PROJECT%"=="" set RS_PROJECT=%~2
if "%RS_BRANCH%"=="" set RS_BRANCH=%~4
goto:eof