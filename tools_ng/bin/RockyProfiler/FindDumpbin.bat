@echo off
setlocal enabledelayedexpansion

:: Find location of dumpbin. Reference: https://github.com/microsoft/vswhere
if exist "%programfiles(x86)%\microsoft visual studio\installer\vswhere.exe" (
    for /f "usebackq tokens=*" %%i in (`"%programfiles(x86)%\microsoft visual studio\installer\vswhere.exe" -latest -products Microsoft.VisualStudio.Product.Professional -property installationPath`) do (
      set InstallDir=%%i
    )
  )
:: Rem Start a VS dev command prompt, sets env vars so we can find dumpbin.
  if exist "!InstallDir!\VC\Auxiliary\Build\vcvars64.bat" (
    call "!InstallDir!\VC\Auxiliary\Build\vcvars64.bat" > NUL
  )
   
where dumpbin.exe
