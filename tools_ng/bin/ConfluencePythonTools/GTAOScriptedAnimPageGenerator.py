from CommonLibrary import ConfluenceController
from CommonLibrary import PerforceController
from CommonLibrary.ConfluenceController import Representation as Representation
import os
import json
from HTMLParser import HTMLParser
from xml.etree import cElementTree
import copy
import ntpath
import re
import xml.etree.ElementTree as ET
#import wx


confPageData = os.path.join(os.path.dirname(os.path.realpath(__file__)) + "\\MiscFiles\\ScriptedAnimation\\ConfPageData.xml")

parentPage = ''
pageName = ''
pageID = ''
renderFiles = []


########################################################################
# Classes for storing the info from the json
########################################################################
class HTMLtoEtreeParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.tb = cElementTree.TreeBuilder()

    def handle_starttag(self, tag, attributes):
        self.set_cdata_mode(tag)
        self.tb.start(tag, dict(attributes))

    def handle_endtag(self, tag):
        self.tb.end(tag)

    def handle_data(self, data):
        self.tb.data(str(data).encode("utf8"))

    def set_cdata_mode(self, tag):
        if tag=="ac:plain-text-body":
            self.interesting = re.compile("(?i)<\\/\\s*ac\\:plain-text-body\\s*>")
            self.cdata_tag = tag

    def close(self):
        HTMLParser.close(self)
        return self.tb.close()
    
########################################################################
#	Function to retrieve contents of a html file and call to get the structure
########################################################################
def GetStructureOfHtml(FileLocation):
    htmlText = ""
    treeStruct = None

    with open(FileLocation, 'r') as f:
        htmlText = f.readlines()
    return GetStructureOfHtmlString(str.join("", htmlText))

########################################################################
#	Function to generate a element tree structure from a html string
########################################################################
def GetStructureOfHtmlString(inputString):
    myparser = HTMLtoEtreeParser()

    myparser.feed(inputString)
    treeStruct = myparser.close()

    return treeStruct

########################################################################
#	Function to convert an element tree to a string while replacing any characters that got borked by the element tree
########################################################################
def ConvertEtreeToString(inputEtree):
    outputString = cElementTree.tostring(inputEtree).replace("&lt;", "<").replace("&gt;", ">")
    return outputString

def readConfDataFile():

	global parentPage, pageName, pageID

	if not os.path.isfile(confPageData):
		return

	tree = ET.parse(confPageData)

	root = tree.getroot()

	ParentPage = root.findall(".//ParentPage/ParentPageTitle")
	for child in ParentPage:
		parentPage = child.text

	PageName = root.findall(".//PageName/NewPageTitle")
	for child in PageName:
		pageName = child.text

	PageID = root.findall(".//PageID/NewPageID")
	for child in PageID:
		pageID = child.text

	for item in root.findall(".//RenderFiles/RenderFile"):
		renderFiles.append('{}'.format(item.attrib['Name']))


def run():

	RENDER_GALLERY_TEMPLATE = os.path.join(os.path.dirname(os.path.realpath(__file__)) + "\\MiscFiles\\ScriptedAnimation\\RenderTemplate.txt")

	RSR_MAIN_PAGE = 'Tech Art - GTAO'

	MAIN_LABEL = 'main'
	
	readConfDataFile()

	myConfluenceController = ConfluenceController.ConfluenceUploadController()

	myPerforceController = PerforceController.PerforceController()

	namespace = "RSGGTAV"

	GTAOParentPage = myConfluenceController.doesPageExist(parentPage, namespace, True)

	if GTAOParentPage is None:
		print 'No Parent Page?'
		return

	htmlContentString = GetStructureOfHtml(RENDER_GALLERY_TEMPLATE)
	htmlContentString = ConvertEtreeToString(htmlContentString)
	
	if pageID != 'None':
		renderPageId = '{}'.format(pageID)
	else:
		renderPageId = myConfluenceController.doesPageExist(pageName, namespace, False)

		if renderPageId is None:
			renderPageId = myConfluenceController.CreatePage(pageName, namespace, pageName, GTAOParentPage)
			#print renderPageId

	renderPageVersion = myConfluenceController.getPageViaId(renderPageId).version
	myConfluenceController.UpdatePage(renderPageId, renderPageVersion+1, htmlContentString, pageName, False, Representation.STORAGE)
	
	for renderFile in renderFiles:
		renderTitle = renderFile.split('/')[-1]
		attachment = myConfluenceController.postAttachmentFile(renderTitle, "".join([str(x) for x in myPerforceController.GetFileContents(renderFile)]), renderPageId, '', "render/mov")
		myConfluenceController.addLabelToContent(attachment.attId, MAIN_LABEL)
		myConfluenceController.addLabelToContent(attachment.attId, pageName)
	
if __name__ == "__main__":
    run()
