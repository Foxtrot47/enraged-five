@echo off

rem First if python is installed lets install it if its not
IF not exist %PYTHONHOME%\python.exe ( 
echo Python install missing. Please install python 2.7.15 through software centre
goto Complete
) ELSE (
rem If there is no pip installed 
IF exist %PYTHONHOME%\scripts\pip.exe (
echo Python detected. Installing required libraries
%PYTHONHOME%\scripts\pip.exe install elasticsearch==6.1.1
%PYTHONHOME%\scripts\pip.exe install requests==2.21.0
%PYTHONHOME%\scripts\pip.exe install p4python==2018.2.1743033
goto Complete
) ELSE (
echo Pip install not found. Please install python from the software centre.
goto Error
)
)

:Error
echo Error occured during set up

:Complete
pause