set EXIT_CODE=0
set ERROR_MESSAGE=
CALL setenv.bat

set LEVEL=%1
if "%LEVEL%"=="" (
	set ERROR_MESSAGE=Level input parameter missing
	set EXIT_CODE=1
	goto:END
)
echo Level: %LEVEL%

set OUTPUT_DIR=%2
if "%OUTPUT_DIR%"=="" (
	set ERROR_MESSAGE=Output directory input parameter missing
	set EXIT_CODE=1
	goto:END
)
echo Output Directory: %OUTPUT_DIR%

set SKIP_PAUSE=0
if "%3"=="nopause" (
	set SKIP_PAUSE=1
)
echo Skip Pause: %SKIP_PAUSE%
echo.

:: Set up the reports/outputs
set REPORTS=RDUGraph
echo Reports: %REPORTS%
echo.

:: Generate the reports
echo Starting to generate reports...
%RS_TOOLSBIN%\ReportGenerator\ReportGenerator.exe --level %LEVEL% --outputdir %OUTPUT_DIR% --reports %REPORTS% --nopopups --p4edit --p4submit --skiplevelload
if not ERRORLEVEL 0 (
	set ERROR_MESSAGE=Report generator exited with a non-zero exit code
	set EXIT_CODE=1
	goto:END
)

:END
:: Check if the exit code is set to something other than 0
if "%EXIT_CODE%"=="0" (
	echo All reports were successfully generated.
) else (
	if "%ERROR_MESSAGE%"=="" (
		echo An unknown error was encountered while generating the reports.
	) else (
		echo The following error was encountered while attempting to generate the reports:
		echo %ERROR_MESSAGE%
	)
)

if "%SKIP_PAUSE%"=="1" (
	goto:POST_PAUSE
)
pause
:POST_PAUSE

echo Exiting with code %EXIT_CODE%
exit /b %EXIT_CODE%