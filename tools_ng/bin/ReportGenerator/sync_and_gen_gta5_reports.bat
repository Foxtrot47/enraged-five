@ECHO OFF
::
:: Simple batch file for syncing relevant files in perforce and kicking off the generate_all_reports batch file
::
pushd %~dp0

set LOG_FILE=log.txt

:: Sync the report generation files
p4 sync generate_all_reports.bat > %LOG_FILE% 2>&1
p4 sync *.dll >> %LOG_FILE% 2>&1
p4 sync *.exe >> %LOG_FILE% 2>&1

:: Sync the game assets
p4 sync //depot/gta5/assets/characters/....xml >> %LOG_FILE% 2>&1
p4 sync //depot/gta5/assets/export/....xml >> %LOG_FILE% 2>&1
p4 sync //depot/gta5/assets/vehicles/....xml >> %LOG_FILE% 2>&1
p4 sync //depot/gta5/assets/weapons/....xml >> %LOG_FILE% 2>&1
p4 sync //depot/gta5/build/dev/... >> %LOG_FILE% 2>&1
p4 sync //depot/gta5/assets/maps/PropSwap.txt >> %LOG_FILE% 2>&1

:: Run the batch file to generate the reports
call generate_all_reports.bat gta5 X:\gta5\assets\reports nopause >> %LOG_FILE% 2>&1

popd