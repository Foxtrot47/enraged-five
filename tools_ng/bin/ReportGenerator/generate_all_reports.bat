@ECHO OFF
:: ***************************************************************************************************************
:: *** generate_all_reports.bat
:: ***
:: *** Michael T�schler <michael.taschler@rockstarnorth.com> Nov 2011
:: ***
:: *** - Script for generating all valid reports in the RSG.Model.Report library
:: *** 
:: *** Usage:
:: ***   generate_all_reports.bat <levelname> <output_directory>
:: *** 
:: ***   <level> 			- Name of the level to generate reports for (e.g. gta5)
:: ***   <output_directory> - Location where the report files should be output to (e.g. x:\gta5\assets\maps\reports)
:: ***   <nopause>			- (optional) specifies whether the script should skip the pause after completing
:: ***************************************************************************************************************

setlocal EnableDelayedExpansion

set EXIT_CODE=0
set ERROR_MESSAGE=

:: Load in project environment variables
CALL setenv.bat

:: Process the input parameters
set LEVEL=%1
if "%LEVEL%"=="" (
	set ERROR_MESSAGE=Level input parameter missing
	set EXIT_CODE=1
	goto:END
)
echo Level: %LEVEL%

set OUTPUT_DIR=%2
if "%OUTPUT_DIR%"=="" (
	set ERROR_MESSAGE=Output directory input parameter missing
	set EXIT_CODE=1
	goto:END
)
echo Output Directory: %OUTPUT_DIR%

set SKIP_PAUSE=0
if "%3"=="nopause" (
	set SKIP_PAUSE=1
)
echo Skip Pause: %SKIP_PAUSE%
echo.

:: Set up the reports/outputs
set REPORTS=Bounds2,CarGens,Characters,DuplicatedItems,IsolatedReferences,LodEntities,MapAssets,MapOptimise,MapObjects,MapSections,MapShaders,PropEntities,PropReplacements,Vehicles,VehicleTextures,Weapons,AssetTypeCountReport,ProcTypeMissingReport,HdAssetSplitCounts,BlackTexturesReport,UniqueLodChildren,WeaponAssetUsage,VehicleAssetUsage,CharacterAssetUsage,MapAssetUsage,OrphanedShadowProxies
echo Reports: %REPORTS%
echo.

:: Generate the geometry stats
echo Starting to generate geometry stats...
%RS_TOOLSBIN%\GeometryStatsGenerator\GeometryStatsGenerator.exe --output %RS_TOOLSROOT%\tmp\geometry_stats\%LEVEL%.xml --level %LEVEL%
if not ERRORLEVEL 0 (
	set ERROR_MESSAGE=Geometry stats generator exited with a non-zero exit code
	set EXIT_CODE=1
	goto:END
)

:: Generate the reports
echo Starting to generate reports...
%RS_TOOLSBIN%\ReportGenerator\ReportGenerator.exe --level %LEVEL% --outputdir %OUTPUT_DIR% --reports %REPORTS% --geomstats %RS_TOOLSROOT%\tmp\geometry_stats\%LEVEL%.xml --nopopups --p4edit --p4submit
::%RS_TOOLSBIN%\ReportGenerator\ReportGenerator.exe --level %LEVEL% --outputdir %OUTPUT_DIR% --reports %REPORTS% --geomstats %RS_TOOLSROOT%\tmp\geometry_stats\%LEVEL%.xml --nopopups --p4edit --unilog %RS_TOOLSROOT%\tools\logs\ReportGenerator.ulog
if not ERRORLEVEL 0 (
	set ERROR_MESSAGE=Report generator exited with a non-zero exit code
	set EXIT_CODE=1
	goto:END
)


:END
:: Check if the exit code is set to something other than 0
if "%EXIT_CODE%"=="0" (
	echo All reports were successfully generated.
) else (
	if "%ERROR_MESSAGE%"=="" (
		echo An unknown error was encountered while generating the reports.
	) else (
		echo The following error was encountered while attempting to generate the reports:
		echo %ERROR_MESSAGE%
	)
)

if "%SKIP_PAUSE%"=="1" (
	goto:POST_PAUSE
)
pause
:POST_PAUSE

echo Exiting with code %EXIT_CODE%
exit /b %EXIT_CODE%
