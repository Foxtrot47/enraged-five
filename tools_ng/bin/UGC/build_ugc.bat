:: This file is used to manage p4 interactions with building offline UGC

@echo OFF
echo.
echo This file is used to manage p4 interactions with building offline UGC
echo.

:: set working directory
cd X:\gta5\build\dev\common\data\ugc

:: modify attributes and delete all existing ugc files
attrib -r *.ugc /s 2>NUL
del *.ugc /s 2>NUL

echo.
echo In RAG, hit Network : Debug : Debug Live : Cloud : UGC : Save Rockstar Created
echo In RAG, hit Network : Debug : Debug Live : Cloud : UGC : Save Rockstar Verified
echo And then press continue
echo.

pause

p4 reconcile -d -a -e

echo.
echo Completed

pause



