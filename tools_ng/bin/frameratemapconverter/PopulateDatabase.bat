call setenv.bat
pushd %~dp0

setlocal EnableDelayedExpansion

SET "version=%~1"
CALL SET version=%%version:gta5_dev_gen9_sga_version_=%%
echo %version%

SET inputFolder=%~2
echo %inputFolder%

SET format=%~3
echo %format%

SET platform=%~4
echo %platform%

SET config=%~5
echo %config%

SET graphicsMode=%~6
echo %graphicsMode%

SET branch=%~7
if "!branch!"=="" SET branch=dev
echo %branch%

SET platformName=%~8
if "!platformName!"=="" SET platformName=%platform%
echo %platformName%

rem Copy performance budgets to the target directory
xcopy /y /s "X:\gta5\tools_ng\bin\RockyProfiler\PerformanceBudgetsTotal.xml" "%inputFolder%"

rem Populate database
%RS_TOOLSROOT%\bin\frameratemapconverter\ElasticSearchPopulator.exe -version "%version%" -format "%format%" -project gta5
%RS_TOOLSROOT%\bin\frameratemapconverter\XmlToDatabaseConverter.exe -data "%inputFolder%\%platform% %config%" -version "%version%" -format "%format%" -budget "%RS_PROJROOT%\gametools\release\RockyProfiler\PerformanceBudgetsTotal.xml" -branch "%branch%" -project gta5 -platform "%platformName% %config%" -mode "%graphicsMode%"

popd