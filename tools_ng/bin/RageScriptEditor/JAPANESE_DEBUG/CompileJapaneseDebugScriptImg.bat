@echo off

CALL setenv.bat


SET INPUT_SOURCE=%RS_SCRIPTBRANCH%\singleplayer\sco\JAPANESE_DEBUG

REM Define Japanese specific build branch path.

SET RS_JP_BUILDBRANCH=x:\gta5\build\Japanese

x:

PUSHD %RS_PROJROOT%
p4 login -s 2>&1
IF ERRORLEVEL 1 (
ECHO You need to log in to Perforce before you can create script.rpf files
GOTO :eof
)


PUSHD %RS_TOOLSLIB%\util


REM Delete all .sco files in the Preview folders of the Japanese branch.

SET PREVIEW_FOLDER="%RS_JP_BUILDBRANCH%\preview\"
CALL :DeleteAllScoFilesFromPreviewFolder

REM SET PREVIEW_FOLDER="%RS_BUILDROOT%\dev_network\preview\"
REM CALL :DeleteAllScoFilesFromPreviewFolder

REM SET PREVIEW_FOLDER="%RS_BUILDROOT%\release\preview\"
REM CALL :DeleteAllScoFilesFromPreviewFolder



REM Compile the script.zip in the assets\export folder
REM CALL :CompileAssetZipScript



REM Compile the platform-specific script.rpf files

::SET SourceFiles=%INPUT_SOURCE%\xbox360\*.xsc
::SET PlatformPath=xbox360
::SET EnabledPlatformToCheck=xbox360
::CALL :CompilePlatformScript

::SET SourceFiles=%INPUT_SOURCE%\ps3\*.csc
::SET PlatformPath=ps3
::SET EnabledPlatformToCheck=ps3
::CALL :CompilePlatformScript

::SET SourceFiles=%INPUT_SOURCE%\win32\*.wsc
::SET PlatformPath=pc
::SET EnabledPlatformToCheck=win32
::CALL :CompilePlatformScript

SET SourceFiles=%INPUT_SOURCE%\win64\*.ysc
SET PlatformPath=x64
SET EnabledPlatformToCheck=win64
CALL :CompilePlatformScript

::SET PlatformPath=xbox360
::SET EnabledPlatformToCheck=xbox360
::CALL :OutputLastModifiedTime

::SET PlatformPath=ps3
::SET EnabledPlatformToCheck=ps3
::CALL :OutputLastModifiedTime

::SET PlatformPath=pc
::SET EnabledPlatformToCheck=win32
::CALL :OutputLastModifiedTime

SET PlatformPath=x64
SET EnabledPlatformToCheck=win64
CALL :OutputLastModifiedTime

POPD

GOTO :eof


REM Subroutines


:DeleteAllScoFilesFromPreviewFolder

IF NOT EXIST %PREVIEW_FOLDER% GOTO :eof

IF EXIST %PREVIEW_FOLDER%\*.sco DEL %PREVIEW_FOLDER%\*.sco
::IF EXIST %PREVIEW_FOLDER%\*.csc DEL %PREVIEW_FOLDER%\*.csc
::IF EXIST %PREVIEW_FOLDER%\*.wsc DEL %PREVIEW_FOLDER%\*.wsc
IF EXIST %PREVIEW_FOLDER%\*.ysc DEL %PREVIEW_FOLDER%\*.ysc
::IF EXIST %PREVIEW_FOLDER%\*.xsc DEL %PREVIEW_FOLDER%\*.xsc

GOTO :eof



:CompileAssetZipScript
SET OUTPUT_FOLDER=%RS_EXPORT%\levels\gta5\script
SET OUTPUT_FILENAME=%OUTPUT_FOLDER%\script.zip

IF NOT EXIST %OUTPUT_FOLDER% MKDIR %OUTPUT_FOLDER%

attrib -r %OUTPUT_FILENAME%
%RS_TOOLSRUBY% data_mk_generic_zip.rb --silent --project=%RS_PROJECT% --uncompressed --output=%OUTPUT_FILENAME% %INPUT_SOURCE%\*.sco
attrib +r %OUTPUT_FILENAME%

GOTO :eof



:CompilePlatformScript
REM %RS_TOOLSRUBY% data_is_platform_enabled.rb %EnabledPlatformToCheck%
%RS_TOOLSBIN%\is_platform_enabled %EnabledPlatformToCheck%

IF NOT ERRORLEVEL 1 GOTO :eof

SET OUTPUT_FOLDER=%RS_JP_BUILDBRANCH%\%PlatformPath%\levels\gta5\script
SET OUTPUT_FILENAME=%OUTPUT_FOLDER%\script.rpf

IF NOT EXIST %OUTPUT_FOLDER% MKDIR %OUTPUT_FOLDER%

attrib -r %OUTPUT_FILENAME%
%RS_TOOLSRUBY% data_mk_generic_rpf.rb --silent --project=%RS_PROJECT% --uncompressed --output=%OUTPUT_FILENAME% %SourceFiles%
attrib +r %OUTPUT_FILENAME%


REM Copy script from dev to dev_network
SET COPY_DEST_FOLDER=%RS_BUILDROOT%\dev_network\%PlatformPath%\levels\gta5\script\
CALL :CopyScriptToDestFolder

REM Copy script from dev to release
SET COPY_DEST_FOLDER=%RS_BUILDROOT%\release\%PlatformPath%\levels\gta5\script\
CALL :CopyScriptToDestFolder

GOTO :eof


:CopyScriptToDestFolder
SET COPY_DEST_FILE=%COPY_DEST_FOLDER%\script.rpf

IF NOT EXIST %COPY_DEST_FOLDER% MKDIR %COPY_DEST_FOLDER%

IF EXIST %COPY_DEST_FILE% ATTRIB -R %COPY_DEST_FILE%
REM xcopy /Y /R %OUTPUT_FILENAME% %COPY_DEST_FOLDER%
copy /B %OUTPUT_FILENAME% %COPY_DEST_FILE% /Y
IF EXIST %COPY_DEST_FILE% ATTRIB +R %COPY_DEST_FILE%

GOTO :eof




:OutputLastModifiedTime
REM %RS_TOOLSRUBY% data_is_platform_enabled.rb %EnabledPlatformToCheck%
%RS_TOOLSBIN%\is_platform_enabled %EnabledPlatformToCheck%

IF NOT ERRORLEVEL 1 GOTO :eof

SET OUTPUT_FOLDER=%RS_JP_BUILDBRANCH%\%PlatformPath%\levels\gta5\script
SET OUTPUT_FILENAME=%OUTPUT_FOLDER%\script.rpf

FOR %%f IN (%OUTPUT_FILENAME%) DO ECHO *** %EnabledPlatformToCheck% %%f modified on %%~tf ***

GOTO :eof


