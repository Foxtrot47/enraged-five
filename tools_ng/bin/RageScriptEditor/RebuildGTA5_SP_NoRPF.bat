@echo off
setlocal

REM ***** Get the personal documents folder path *****
set key=hkcu\Software\Microsoft\windows\CurrentVersion\
set key="%key%Explorer\shell folders" /v Personal
for /f "tokens=2,*" %%A in ('reg query %key% ') do set MYDOCS=%%B

REM ***** Get the IncredBuild install path *****
set key=hklm\Software\Wow6432Node\Xoreax\IncrediBuild\Builder
set key="%key%" /v Folder
for /f "tokens=2,*" %%A in ('reg query %key% ') do set INCREDIBUILD=%%B

REM ***** Create the XML file for IncrediBuild use *****
call IncrediBuildScriptProject.exe -config Release -scproj X:\gta5\script\dev_ng\singleplayer\GTA5_SP.scproj
