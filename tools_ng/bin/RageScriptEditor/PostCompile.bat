:: POST COMPILE BATCH SCRIPT
:: Post compilation build script. Packages the scripts into one .RPF file. The debug file is script.rpf
:: The release file is script_REL.rpf. Requires one parameter.
::
:: PostCompile RELEASE|DEBUG
:: 
@echo off

IF "%1" EQU "" GOTO :missingparam
IF /I "%1" NEQ "RELEASE" IF /I "%1" NEQ "DEBUG" GOTO :missingparam

SET OUTPUT_TYPE=%1
SET SCRIPT_OUT=script.rpf

IF /I "%1" EQU "RELEASE" SET SCRIPT_OUT=script_REL.rpf

CALL setenv.bat

SET INPUT_SOURCE=%RS_PROJROOT%\script\dev\singleplayer\sco\%1

x:

PUSHD %RS_PROJROOT%
p4 login -s 2>&1
IF ERRORLEVEL 1 (

ECHO You need to log in to Perforce before you can create script.rpf files
GOTO :eof

)

PUSHD %RS_TOOLSLIB%\util


REM Delete all .sco files in the Preview folders of the Dev, Dev_network and Release branches

SET PREVIEW_FOLDER="%RS_BUILDBRANCH%\preview\"
CALL :DeleteAllScoFilesFromPreviewFolder

IF /I "%OUTPUT_TYPE%" EQU "DEBUG" (

SET PREVIEW_FOLDER="%RS_BUILDROOT%\dev_network\preview\"
CALL :DeleteAllScoFilesFromPreviewFolder

SET PREVIEW_FOLDER="%RS_BUILDROOT%\release\preview\"
CALL :DeleteAllScoFilesFromPreviewFolder

)

REM Compile the script.zip in the assets\export folder
CALL :CompileAssetZipScript



REM Compile the platform-specific script.rpf files

SET SourceFiles=%INPUT_SOURCE%\xbox360\*.xsc
SET PlatformPath=xbox360
SET EnabledPlatformToCheck=xbox360
CALL :CompilePlatformScript

SET SourceFiles=%INPUT_SOURCE%\ps3\*.csc
SET PlatformPath=ps3
SET EnabledPlatformToCheck=ps3
CALL :CompilePlatformScript

SET SourceFiles=%INPUT_SOURCE%\win32\*.wsc
SET PlatformPath=pc
SET EnabledPlatformToCheck=win32
CALL :CompilePlatformScript

SET SourceFiles=%INPUT_SOURCE%\win64\*.ysc
SET PlatformPath=x64
SET EnabledPlatformToCheck=win64
CALL :CompilePlatformScript

SET PlatformPath=xbox360
SET EnabledPlatformToCheck=xbox360
CALL :OutputLastModifiedTime

SET PlatformPath=ps3
SET EnabledPlatformToCheck=ps3
CALL :OutputLastModifiedTime

SET PlatformPath=pc
SET EnabledPlatformToCheck=win32
CALL :OutputLastModifiedTime

SET PlatformPath=x64
SET EnabledPlatformToCheck=win64
CALL :OutputLastModifiedTime

POPD

GOTO :eof


REM Subroutines


:DeleteAllScoFilesFromPreviewFolder

IF NOT EXIST %PREVIEW_FOLDER% GOTO :eof

IF EXIST %PREVIEW_FOLDER%\*.sco DEL %PREVIEW_FOLDER%\*.sco
IF EXIST %PREVIEW_FOLDER%\*.csc DEL %PREVIEW_FOLDER%\*.csc
IF EXIST %PREVIEW_FOLDER%\*.wsc DEL %PREVIEW_FOLDER%\*.wsc
IF EXIST %PREVIEW_FOLDER%\*.ysc DEL %PREVIEW_FOLDER%\*.ysc
IF EXIST %PREVIEW_FOLDER%\*.xsc DEL %PREVIEW_FOLDER%\*.xsc

GOTO :eof



:CompileAssetZipScript
SET OUTPUT_FOLDER=%RS_EXPORT%\levels\%RS_PROJECT%\script
SET OUTPUT_FILENAME=%OUTPUT_FOLDER%\script.zip

IF NOT EXIST %OUTPUT_FOLDER% MKDIR %OUTPUT_FOLDER%

attrib -r %OUTPUT_FILENAME%
%RS_TOOLSRUBY% data_mk_generic_zip.rb --silent --project=%RS_PROJECT% --uncompressed --output=%OUTPUT_FILENAME% %INPUT_SOURCE%\*.sco
attrib +r %OUTPUT_FILENAME%

FOR %%f IN (%OUTPUT_FILENAME%) DO SET filedatetime=%%~tf
echo %OUTPUT_FILENAME% was last updated at %filedatetime%.

GOTO :eof



:CompilePlatformScript
REM %RS_TOOLSRUBY% data_is_platform_enabled.rb %EnabledPlatformToCheck%
%RS_TOOLSBIN%\is_platform_enabled %EnabledPlatformToCheck%

IF NOT ERRORLEVEL 1 GOTO :eof

SET OUTPUT_FOLDER=%RS_BUILDBRANCH%\%PlatformPath%\levels\%RS_PROJECT%\script
SET OUTPUT_FILENAME=%OUTPUT_FOLDER%\%SCRIPT_OUT%

IF NOT EXIST %OUTPUT_FOLDER% MKDIR %OUTPUT_FOLDER%

attrib -r %OUTPUT_FILENAME%
%RS_TOOLSRUBY% data_mk_generic_rpf.rb --silent --project=%RS_PROJECT% --uncompressed --output=%OUTPUT_FILENAME% %SourceFiles%
attrib +r %OUTPUT_FILENAME%

IF /I "%OUTPUT_TYPE%" EQU "DEBUG" (

REM Copy script from dev to dev_network
SET COPY_DEST_FOLDER=%RS_BUILDROOT%\dev_network\%PlatformPath%\levels\%RS_PROJECT%\script\
CALL :CopyScriptToDestFolder

REM Copy script from dev to release
SET COPY_DEST_FOLDER=%RS_BUILDROOT%\release\%PlatformPath%\levels\%RS_PROJECT%\script\
CALL :CopyScriptToDestFolder

) ELSE (

FOR %%f IN (%OUTPUT_FILENAME%) DO SET filedatetime=%%~tf
echo %OUTPUT_FILENAME% was last updated at %filedatetime%.

)

GOTO :eof

REM Copy script from dev to dev_network
SET COPY_DEST_FOLDER=%RS_BUILDROOT%\dev_network\%PlatformPath%\levels\%RS_PROJECT%\script\
CALL :CopyScriptToDestFolder

REM Copy script from dev to release
SET COPY_DEST_FOLDER=%RS_BUILDROOT%\release\%PlatformPath%\levels\%RS_PROJECT%\script\
CALL :CopyScriptToDestFolder

GOTO :eof


:CopyScriptToDestFolder
SET COPY_DEST_FILE=%COPY_DEST_FOLDER%\%SCRIPT_OUT%

IF NOT EXIST %COPY_DEST_FOLDER% MKDIR %COPY_DEST_FOLDER%

IF EXIST %COPY_DEST_FILE% ATTRIB -R %COPY_DEST_FILE%
REM xcopy /Y /R %OUTPUT_FILENAME% %COPY_DEST_FOLDER%
copy /B %OUTPUT_FILENAME% %COPY_DEST_FILE% /Y
IF EXIST %COPY_DEST_FILE% ATTRIB +R %COPY_DEST_FILE%

GOTO :eof


:OutputLastModifiedTime
REM %RS_TOOLSRUBY% data_is_platform_enabled.rb %EnabledPlatformToCheck%
%RS_TOOLSBIN%\is_platform_enabled %EnabledPlatformToCheck%

IF NOT ERRORLEVEL 1 GOTO :eof

SET OUTPUT_FOLDER=%RS_BUILDBRANCH%\%PlatformPath%\levels\%RS_PROJECT%\script
SET OUTPUT_FILENAME=%OUTPUT_FOLDER%\%SCRIPT_OUT%

FOR %%f IN (%OUTPUT_FILENAME%) DO ECHO *** %EnabledPlatformToCheck% %%f modified on %%~tf ***

GOTO :eof

:missingparam
ECHO Post compilation build script. Packages the compiled scripts files into a single .RPF file.
ECHO Script file takes one parameter; RELEASE or DEBUG.
ECHO E.g.
ECHO PostCompile RELEASE
