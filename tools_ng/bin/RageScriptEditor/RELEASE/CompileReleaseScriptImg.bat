@echo off
CALL setenv.bat


SET INPUT_SOURCE=%RS_SCRIPTBRANCH%\singleplayer\sco\RELEASE

x:



PUSHD %RS_TOOLSLIB%\util

REM Delete all .sco files in the Preview folder

SET PREVIEW_FOLDER="%RS_BUILDBRANCH%\preview\"
Call :DeleteAllScoFilesFromPreviewFolder


REM Compile the script_REL.zip in the assets\export folder
CALL :CompileAssetZipScript

REM Compile the script_REL.rpf in the common folder
REM CALL :CompileCommonScript

REM Compile the platform-specific script_REL.rpf files

::SET SourceFiles=%INPUT_SOURCE%\xbox360\*.xsc
::SET PlatformPath=xbox360
::SET EnabledPlatformToCheck=xbox360
::CALL :CompilePlatformScript

::SET SourceFiles=%INPUT_SOURCE%\ps3\*.csc
::SET PlatformPath=ps3
::SET EnabledPlatformToCheck=ps3
::CALL :CompilePlatformScript

::SET SourceFiles=%INPUT_SOURCE%\win32\*.wsc
::SET PlatformPath=pc
::SET EnabledPlatformToCheck=win32
::CALL :CompilePlatformScript

SET SourceFiles=%INPUT_SOURCE%\win64\*.ysc
SET PlatformPath=x64
SET EnabledPlatformToCheck=win64
CALL :CompilePlatformScript

::SET PlatformPath=xbox360
::SET EnabledPlatformToCheck=xbox360
::CALL :OutputLastModifiedTime

::SET PlatformPath=ps3
::SET EnabledPlatformToCheck=ps3
::CALL :OutputLastModifiedTime

::SET PlatformPath=pc
::SET EnabledPlatformToCheck=win32
::CALL :OutputLastModifiedTime

SET PlatformPath=x64
SET EnabledPlatformToCheck=win64
CALL :OutputLastModifiedTime

POPD

GOTO :eof



REM Subroutines


:DeleteAllScoFilesFromPreviewFolder

IF NOT EXIST %PREVIEW_FOLDER% GOTO :eof

IF EXIST %PREVIEW_FOLDER%\*.sco DEL %PREVIEW_FOLDER%\*.sco
::IF EXIST %PREVIEW_FOLDER%\*.csc DEL %PREVIEW_FOLDER%\*.csc
::IF EXIST %PREVIEW_FOLDER%\*.wsc DEL %PREVIEW_FOLDER%\*.wsc
IF EXIST %PREVIEW_FOLDER%\*.ysc DEL %PREVIEW_FOLDER%\*.ysc
::IF EXIST %PREVIEW_FOLDER%\*.xsc DEL %PREVIEW_FOLDER%\*.xsc

GOTO :eof


:CompileAssetZipScript
SET OUTPUT_FOLDER=%RS_EXPORT%\levels\gta5\script
SET OUTPUT_FILENAME=%OUTPUT_FOLDER%\script_REL.zip

IF NOT EXIST %OUTPUT_FOLDER% MKDIR %OUTPUT_FOLDER%
attrib -r %OUTPUT_FILENAME%
%RS_TOOLSRUBY% data_mk_generic_zip.rb --silent --project=%RS_PROJECT% --uncompressed --output=%OUTPUT_FILENAME% %INPUT_SOURCE%\*.sco
attrib +r %OUTPUT_FILENAME%

GOTO :eof


:CompileCommonScript
SET OUTPUT_FILENAME=%RS_BUILDBRANCH%\common\data\script\script_REL.rpf
attrib -r %OUTPUT_FILENAME%
%RS_TOOLSRUBY% data_mk_generic_rpf.rb --silent --project=%RS_PROJECT% --uncompressed --output=%OUTPUT_FILENAME% %INPUT_SOURCE%\*.sco
attrib +r %OUTPUT_FILENAME%

GOTO :eof


:CompilePlatformScript

REM %RS_TOOLSRUBY% data_is_platform_enabled.rb %EnabledPlatformToCheck%
%RS_TOOLSBIN%\is_platform_enabled %EnabledPlatformToCheck%

IF NOT ERRORLEVEL 1 GOTO :eof

SET OUTPUT_FOLDER=%RS_BUILDBRANCH%\%PlatformPath%\levels\gta5\script
SET OUTPUT_FILENAME=%OUTPUT_FOLDER%\script_REL.rpf

IF NOT EXIST %OUTPUT_FOLDER% MKDIR %OUTPUT_FOLDER%

attrib -r %OUTPUT_FILENAME%
%RS_TOOLSRUBY% data_mk_generic_rpf.rb --silent --project=%RS_PROJECT% --uncompressed --output=%OUTPUT_FILENAME% %SourceFiles%
attrib +r %OUTPUT_FILENAME%

FOR %%f IN (%OUTPUT_FILENAME%) DO SET filedatetime=%%~tf
echo %OUTPUT_FILENAME% was last updated at %filedatetime%.

GOTO :eof


:OutputLastModifiedTime
REM %RS_TOOLSRUBY% data_is_platform_enabled.rb %EnabledPlatformToCheck%
%RS_TOOLSBIN%\is_platform_enabled %EnabledPlatformToCheck%

IF NOT ERRORLEVEL 1 GOTO :eof

SET OUTPUT_FOLDER=%RS_BUILDBRANCH%\%PlatformPath%\levels\gta5\script
SET OUTPUT_FILENAME=%OUTPUT_FOLDER%\script_REL.rpf

FOR %%f IN (%OUTPUT_FILENAME%) DO ECHO *** %EnabledPlatformToCheck% %%f modified on %%~tf ***

GOTO :eof

