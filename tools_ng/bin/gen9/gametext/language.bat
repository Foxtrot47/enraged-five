@ECHO OFF
setlocal enabledelayedexpansion

REM %1 is config file name
REM %2 is language being converted
REM %3 is rpf being created 
REM %4 is optional script parameter
REM %5 is optional suppress the ulog window

set CONFIG=%1
set LANUGAGE=%2
set RPF=%3
set TARGET=%4
set NOLOGWINDOW=%5

if not defined CONFIG (
	echo.
	title FAILED processing text
	echo *** ERROR: CONFIG not defined.
	echo.
	goto :EOF
)

REM Read the branch config
if not exist %CONFIG% ( set CONFIG=%~dp0\configs\%CONFIG%.config )
if not exist %CONFIG% (
	echo.
	title FAILED processing text
	echo *** ERROR: %CONFIG% config file not found.
	echo.
	goto :EOF
)
for /f "eol=- delims=" %%a in (%CONFIG%) do set "%%a"

REM Setup some defaults in case the above config read has missing settings.
if not DEFINED PLATFORMS          (call set PLATFORMS="ps4,ps5,xboxone,xbsx,x64")
if not DEFINED SOURCE_BRANCH      (call set SOURCE_BRANCH="dev_gen9_sga")
if not DEFINED DESTINATION_BRANCH (call set DESTINATION_BRANCH="dev_gen9_sga")
if not DEFINED ASSETS             (call set ASSETS="assets_gen9_disc")

if not DEFINED USE_FILES_IN_PATH  (call set USE_FILES_IN_PATH=False) 
if not DEFINED DEBUGGING          (call set DEBUGGING=False)

if not DEFINED IS_BRANCHED_SOURCE (call set IS_BRANCHED_SOURCE="False")
if not DEFINED IS_TITLE_UPDATE    (call set IS_TITLE_UPDATE="False")
if not DEFINED IS_TITLE_PATCH     (call set IS_TITLE_UPDATE="False")

if not DEFINED NOLOGWINDOW        (call set NOLOGWINDOW=False)
if not DEFINED GXTMAKER           (call set GXTMAKER=%~dp0gxtmaker2.exe)
if not DEFINED LOG_ROOT           (call set LOG_ROOT=%TEMP%\)
if not DEFINED ARGUMENTS          (call set ARGUMENTS=-quiet)

if /i %IS_TITLE_UPDATE%==True ( 
	set DESTINATION_ROOT=%RS_PROJROOT%\titleupdate\%DESTINATION_BRANCH%
) else (
	set DESTINATION_ROOT=%RS_PROJROOT%\build\%DESTINATION_BRANCH%
)

if /i %IS_BRANCHED_SOURCE%==True (
	if /i %IS_TITLE_PATCH%==True ( 
		set SOURCE_FOLDER="%RS_PROJROOT%\%ASSETS%\GameText\%SOURCE_BRANCH%\Patch\"
	) else ( 
		set SOURCE_FOLDER="%RS_PROJROOT%\%ASSETS%\GameText\%SOURCE_BRANCH%\%LANUGAGE%\"
	)
) else (
	if /i %IS_TITLE_PATCH%==True (
		set SOURCE_FOLDER="%RS_PROJROOT%\%ASSETS%\GameText\Patch\"
	) else (
		set SOURCE_FOLDER="%RS_PROJROOT%\%ASSETS%\GameText\%LANUGAGE%\"
	)
)

if /i %IS_TITLE_PATCH%==True ( 
	set OUTPUT=patch\data\lang\
) else ( 
	set OUTPUT=data\lang\
) 

set POSTFIX=

set text_file_list=
set text_file_list_pc=
set text_file_list_debug=
set text_file_list_platform=

echo.
echo ARGUMENTS			%ARGUMENTS%
echo LOG_ROOT           %LOG_ROOT%
echo GXTMAKER           %GXTMAKER%
echo NOLOGWINDOW        %NOLOGWINDOW%
echo LANUGAGE           %LANUGAGE%
echo RPF                %RPF%
echo TARGET             %TARGET%
echo CONFIG_FILE        %CONFIG%
echo PLATFORMS          %PLATFORMS%
echo SOURCE_BRANCH      %SOURCE_BRANCH%
echo DESTINATION_BRANCH %DESTINATION_BRANCH%
echo ASSETS             %ASSETS%
echo IS_BRANCHED_SOURCE %IS_BRANCHED_SOURCE%
echo IS_TITLE_UPDATE    %IS_TITLE_UPDATE%
echo IS_TITLE_PATCH     %IS_TITLE_PATCH%
echo DESTINATION_ROOT   %DESTINATION_ROOT%
echo SOURCE_FOLDER      %SOURCE_FOLDER%
echo OUTPUT             %OUTPUT%
echo.

REM goto: end

if /i %IS_BRANCHED_SOURCE%==True (
	set activityFeed=%RS_PROJROOT%\%ASSETS%\GameText\%SOURCE_BRANCH%\ActivityFeed\activityFeed.txt
	set errorCodes=%RS_PROJROOT%\%ASSETS%\GameText\%SOURCE_BRANCH%\errorcodes\%LANUGAGE%.txt
) else (
	set activityFeed=%RS_PROJROOT%\%ASSETS%\GameText\ActivityFeed\activityFeed.txt
	set errorCodes=%RS_PROJROOT%\%ASSETS%\GameText\errorcodes\%LANUGAGE%.txt
)

if /i %USE_FILES_IN_PATH%==True (

	if not DEFINED DEBUG_FILE_TAGS    ( call set DEBUG_FILE_TAGS=NetArmy.txt,NetCNC.txt,NetStPatricks.txt,NetJobPack.txt,Clifford.txt,NetLowriderMisc.txt )
	if not DEFINED IGNORE_FILE_TAGS   ( call set IGNORE_FILE_TAGS=NetHeists.txt,NetLowrider.txt,NetHalloween.txt,NetGangBoss.txt )

	for %%f in (%SOURCE_FOLDER%*.txt) do (
		set name=%%~nf%%~xf
		if NOT "!name!"=="!name:%LANUGAGE%=!" (
			call :processfile !name!
		)
	)

	REM echo activityFeed !activityFeed!
	if EXIST !activityFeed! (
		set text_file_list=!text_file_list! ../ActivityFeed/activityFeed.txt
		set text_file_list_debug=!text_file_list_debug! ../ActivityFeed/activityFeed.txt
	) else if %DEBUGGING%==True (
		echo "File !activityFeed! does not exist"
	)

) else (
	
	if DEFINED FILES (
		for %%f in ("%FILES:,=" "%") do (
			set name=%LANUGAGE%%%~nf%%~xf
			if exist %SOURCE_FOLDER%!name! (
				call set text_file_list=!text_file_list! !name!
				call set text_file_list_debug=!text_file_list_debug! !name!
			) else (
				echo WARNING : File !name! not found
			)
		)
	) else (
		echo.
		title FAILED processing text
		echo *** ERROR: %CONFIG% config contains no file information.
		echo.
		goto :EOF
	)

	REM Add in the PC only files from the config
	if DEFINED PC_FILES (
		for %%f in ("%PC_FILES:,=" "%") do (
			set name=%LANUGAGE%%%~nf%%~xf
			if exist %SOURCE_FOLDER%!name! (
				call set text_file_list_pc=!text_file_list_pc! !name!
			) else (
				echo WARNING : PC File !name! not found
			)
		)
	)

	REM Add in the debug only files from the config
	if DEFINED DEBUG_FILES (
		for %%f in ("%DEBUG_FILES:,=" "%") do (
			set name=%LANUGAGE%%%~nf%%~xf
			if exist %SOURCE_FOLDER%!name! (
				call set text_file_list_debug=!text_file_list_debug! !name!
			)
		)
	)
	
	if /i %IS_TITLE_PATCH%==False (

		if EXIST !activityFeed! (
			set text_file_list=!text_file_list! ../ActivityFeed/activityFeed.txt
			set text_file_list_debug=!text_file_list_debug! ../ActivityFeed/activityFeed.txt
		) else if %DEBUGGING%==True (
			echo "File !activityFeed! does not exist"
		)

	)
)

if NOT "%text_file_list%"=="" (
	echo.
	echo Files: !text_file_list!
	echo.
) else (
	echo *** ERROR: %CONFIG% config contains no file on disk.
	echo.
	goto :EOF
)
if NOT "%text_file_list_pc%"=="" (
	echo.
	echo PC Only Files: !text_file_list_pc!
	echo.
)
if NOT "%text_file_list_debug%"=="" (
	echo.
	echo Debug Files: !text_file_list_debug!
	echo.
)

REM goto: end

set logwindow=
if /i %NOLOGWINDOW%==True ( set logwindow=-nologwindow )

set err=0
set text_file_list=!text_file_list!
set logfile=%LOG_ROOT%gxtmaker-%LANUGAGE%-REL
set pconfig=release
call :Process %LANUGAGE% %RPF%_REL.rpf "%TARGET%" %DESTINATION_ROOT%

:debuggametext
if DEFINED text_file_list_debug (
	set text_file_list=!text_file_list_debug!
	set logfile=%LOG_ROOT%gxtmaker-%LANUGAGE%-dbg
	set pconfig=debug
	call :Process %LANUGAGE% %RPF%.rpf "%TARGET%" %DESTINATION_ROOT%
)

:errorcodes
if /i %IS_TITLE_PATCH%==False (
	if DEFINED errorCodes (
		echo.
		echo Produce pc error codes %errorCodes%
		echo.
		call :ProcessErrorCodes %LANUGAGE% %DESTINATION_ROOT%\x64\data\errorcodes %errorCodes%
	)
)

echo.
title Done processing text
echo Done processing text.
if not "%err%"=="0" (
echo.
title FAILED processing text
echo *** ERROR: Text processing failed - not all output files were generated
echo.           Please review the logs and correct the errors, then try again.
echo.
)

goto :end

REM %1 is file name to process
:processfile

	set name=%1

	call set IGNORE_FILE=False
	call set DEBUG_FILE=False

	if DEFINED IGNORE_FILE_TAGS (
		for %%d in ("%IGNORE_FILE_TAGS:,=" "%") do (

			if NOT "!name!"=="!name:%%~nd%%~xd=!" (
				call set IGNORE_FILE=True
				REM echo IGNORE_FILE : !name! %%~nd%%~xd !IGNORE_FILE!
			)
		)
	)
	
	if !IGNORE_FILE!==False (

		if NOT "!name!"=="!name:debug=!" (
			call set DEBUG_FILE=True
		) else if DEFINED DEBUG_FILE_TAGS (
			for %%d in ("%DEBUG_FILE_TAGS:,=" "%") do (
				if NOT "!name!"=="!name:%%~nd%%~xd=!" (
					call set DEBUG_FILE=True
					REM echo DEBUG_FILE : !name! %%~nd%%~xd !DEBUG_FILE! %DEBUG_FILE%
				)
			)
		)

		call set text_file_list_debug=!text_file_list_debug! !name!
		if !DEBUG_FILE!==False (
			if !DEBUGGING!==True ( echo ADD_FILE !name! )
			call set text_file_list=!text_file_list! !name!
		) else if !DEBUGGING!==True (
			echo DEBUG_FILE : !name!
		)

	) else if !DEBUGGING!==True (
		echo IGNORE_FILE !name!
	)
	
goto :endprocess

REM %1 is language being converted
REM %2 is target directory
REM %3 is source file
:ProcessErrorCodes

is_platform_enabled Win64

if ERRORLEVEL 1 (
	if exist %3 (
		p4 edit %2\%1.txt
		%GXTMAKER% -log %LOG_ROOT%gxtmaker-%1-errors-x64.ulog -nologwindow -platform x64 -platformkey pc -plaintext %2 %3
		p4 revert -a %2\%1.txt
	) else (
		echo WARNING %3 does not exist
	)
)

goto :endprocess

REM %1 is language being converted
REM %2 is rpf being created (based on the language)
REM %3 is optional script parameter
REM %4 is destination directory
REM %5 is platform inputs
:Process

pushd %SOURCE_FOLDER%

for %%f in ("%PLATFORMS:,=" "%") do (
	REM We need this for platform files.
	SETLOCAL ENABLEDELAYEDEXPANSION

	set DESTINATION_FILE=%4\%%~nf\%OUTPUT%%2 

	set text_file_list_platform=
	set PLATFORM_KEY=%%~nf
	if /i %%f=="x64"     (
		set text_file_list_platform=!text_file_list_pc!
		set PLATFORM_KEY=pc
	) else if /i %%f=="xboxone" (
		set PLATFORM_KEY=xb1
	) else if /i %%f=="xbsx" (
		set PLATFORM_KEY=xbsx
	) else if /i %%f=="ps4" ( 
		set PLATFORM_KEY=ps4
	) else if /i %%f=="ps5" ( 	
		set PLATFORM_KEY=ps5
	)

	is_platform_enabled %%~nf
	if /i %%f=="x64" is_platform_enabled Win64
	if /i %%f=="xboxone" is_platform_enabled XboxOne
	if /i %%f=="xbsx" is_platform_enabled xbsx
	if /i %%f=="ps4" is_platform_enabled ps4
	if /i %%f=="ps5" is_platform_enabled ps5
	if /i %3=="-all" is_platform_enabled ps4 ps5 Win64 XboxOne xbsx

	if ERRORLEVEL 1 (
		echo.
		echo Processing %LANUGAGE% %pconfig% text data for %%~nf...
		title Processing %LANUGAGE% %pconfig% text data for %%~nf
		if !DEBUGGING!==True (
			echo.
			echo Configuration:
			echo -platform %%f -platformkey !PLATFORM_KEY! !DESTINATION_FILE!
			echo !text_file_list! !text_file_list_platform!
			echo.
		)
		
		if /i %3=="-all" (
			p4 edit !DESTINATION_FILE!
		)
		%GXTMAKER% -log %logfile%-%%~nf.ulog %logwindow% %ARGUMENTS% -platform %%~nf -platformkey !PLATFORM_KEY! !DESTINATION_FILE! !text_file_list! !text_file_list_platform!
		
		if not %errorlevel%==0 set err=%errorlevel%
	)
)

popd

goto :endprocess

:end
:endprocess