@ECHO OFF

REM %1 is config file name

set CONFIG=%1

if not defined CONFIG (
	echo.
	title FAILED processing text
	echo *** ERROR: CONFIG not defined.
	echo.
	goto :EOF
)

ECHO Build all the title update game text
call %RS_TOOLSROOT%\bin\gen9\gametext\language.bat %CONFIG% american american -all True
call %RS_TOOLSROOT%\bin\gen9\gametext\language.bat %CONFIG% chinese chinese -all True
call %RS_TOOLSROOT%\bin\gen9\gametext\language.bat %CONFIG% chinesesimp chinesesimp -all True
call %RS_TOOLSROOT%\bin\gen9\gametext\language.bat %CONFIG% french french -all True
call %RS_TOOLSROOT%\bin\gen9\gametext\language.bat %CONFIG% german german -all True
call %RS_TOOLSROOT%\bin\gen9\gametext\language.bat %CONFIG% italian italian -all True
call %RS_TOOLSROOT%\bin\gen9\gametext\language.bat %CONFIG% japanese japanese -all True
call %RS_TOOLSROOT%\bin\gen9\gametext\language.bat %CONFIG% korean korean -all True
call %RS_TOOLSROOT%\bin\gen9\gametext\language.bat %CONFIG% mexican mexican -all True
call %RS_TOOLSROOT%\bin\gen9\gametext\language.bat %CONFIG% polish polish -all True
call %RS_TOOLSROOT%\bin\gen9\gametext\language.bat %CONFIG% portuguese portuguese -all True
call %RS_TOOLSROOT%\bin\gen9\gametext\language.bat %CONFIG% russian russian -all True
call %RS_TOOLSROOT%\bin\gen9\gametext\language.bat %CONFIG% spanish spanish -all True