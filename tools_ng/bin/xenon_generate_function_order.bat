@echo off

setlocal EnableDelayedExpansion

set tracepath=C:\trace
set /a maxsizeperthread=128*1024*1024
set intmax=2147483647

@echo Removing %tracepath% ...
rmdir /s /q %tracepath%

"%XEDK%\bin\win32\xbcp" /y /t hdd:\devkit\trace\* %tracepath%

for /r %tracepath% %%f in (*.pix2) do (
set /a sortsize=intmax-%%~zf
ren %%f !sortsize!_%%~nf.pix2
)

set "command="%XEDK%\bin\win32\PgoLite" /out="%RS_PROJROOT%\src\dev\game\VS_Project\xenon_function_order.txt""

set /a size=0
for /r %tracepath% %%f in (*_update_trace_*.pix2) do (
set /a size=!size!+%%~zf
if !size! gtr %maxsizeperthread% goto parse_render_traces
set command=!command! %%f
)

:parse_render_traces
set size=0
for /r %tracepath% %%f in (*_render_trace_*.pix2) do (
set /a size=!size!+%%~zf
if !size! gtr %maxsizeperthread% goto execute
set command=!command! %%f
)

:execute
%command%

goto :eof