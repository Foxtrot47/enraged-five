#
# buildlog.rb
# Build Log
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 1 April 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/cms/radiantcms'
require 'pipeline/config/projects'
require 'pipeline/log/log'
require 'pipeline/log/erblogtranslator'
require 'pipeline/os/path'

module Pipeline
	
	#
	# == Description
	# The BuildLog class is a wrapper around our +Pipeline::Log+ class.  This
	# is used by the overnight build process to add logging into our Radiant 
	# CMS system.
	#
	class BuildLog < Log
		
		#
		# BuildLog class constructor.
		#
		# The +is_root_buildlog+ argument is used by the overnight process to
		# note that a single buildlog be the 'root' buildlog (not in the sense
		# of the Log4r system, just within the BuildLog class).  The root
		# BuildLog can have a series of children which helps when writing the
		# hierarchical log data into our CMS system (or whereever it is going).
		#
		def initialize( name, additional_outputters = nil, is_root_buildlog = false )
			# Build log is never the root log (that is created by the LogSystem)
			super( name, additional_outputters, false )
			@is_root_buildlog = is_root_buildlog
			@children = []
		end
		
		#
		# Add a child BuildLog instance to be written by this instance of the
		# BuildLog.
		#
		def add_child_buildlog( child_buildlog )
			raise Exception.new( 'Object specified is not a BuildLog' ) \
				unless self.class == child_buildlog.class
			
			@children << child_buildlog
		end
		
		#
		# Write the BuildLog data into our Radiant CMS system, this only
		# happens for the root build log.
		#
		def flush()
			super() # Invoke superclass to ensure we get XML/HTML versions etc.
			return unless @is_root_buildlog
		
			# Translate this root log, creating Radiant CMS page, and fetch
			# the page ID so we can add our child log pages.
			config = Config.instance
			cms = config.cms
			radiantcms = RadiantCMS.new( cms.database_host, cms.database_port, \
				  cms.database_username, cms.database_password, cms.database_name )
			erb_path = OS::Path.combine( config.toolsbin, 'pipeline', 'log', 'erb', 'buildlog.erb' )
			translator = ErbLogTranslator.new( @xml_filename, erb_path )
			translator.translate()
			
			# Create page content
			page_id = radiantcms.find_page( 'Build Logs' )
			user_id = radiantcms.find_user( 'automate' )
			slug = "#{@name.gsub( ' ', '-' )}-#{Time.now.strftime( '%H-%M-%S' )}"
			page_id_root = radiantcms.insert_page( page_id, user_id, @name, 
				{ 'body' => translator.translated_output.gsub( /\n/, '' ) }, 
				RadiantCMS::STATUS_PUBLISHED, slug )
			
			# Translate our child logs, adding Radiant CMS page content for
			# each child log.
			@children.each do |child_buildlog|
				translator = ErbLogTranslator.new( child_buildlog.xml_filename, erb_path )
				translator.translate()
				
				slug = "#{child_buildlog.name.gsub( ' ', '-' )}-#{DateTime.now.strftime( '%Y-%m-%d-%H-%M-%S' )}"
				radiantcms.insert_page( page_id_root, user_id, child_buildlog.name,
					{ 'body' => translator.translated_output.gsub( /\n/, '' ) },
					RadiantCMS::STATUS_PUBLISHED, slug )
			end
		end
		
		#---------------------------------------------------------------------
		# Private
		#---------------------------------------------------------------------
	private
		
		attr_reader :is_root_buildlog
		attr_reader :children
	end
	
end # Pipeline module

# End of buildlogtranslator.rb
