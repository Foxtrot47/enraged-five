#
# logtranslator.rb
# Abstract XML Log Translator class definition and implementation.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 1 April 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/log/log'
require 'pipeline/util/debug'

require 'date'
require 'rexml/document'

module Pipeline
	
	#
	# == Description
	# An abstract translator class that opens a XML Log file created by the 
	# +XMLFileOutputter+ class and allows translation into another format (e.g.
	# HTML, email or PDF).
	#
	# It is expected that the translated format is saved to a file as there is
	# currently no method of fetching the translated data without first saving
	# it to a file.
	#
	class XmlLogTranslator
		#
		# == Description
		# Error exception class for translator errors.
		#
		class Error < Exception; end
		
		def initialize( filename )
			@xml_filename = filename
		end

		#---------------------------------------------------------------------
		# Abstract Public Methods
		#---------------------------------------------------------------------	
		
		#
		# Translate our XML Log element by event element, invoking our virtual
		# cb_translate_log_event method for each event element.
		#
		def translate( )
			parse()
		end

		#
		# Save the translated output to a specified file.  This must be 
		# overridden in derived classes.
		#
		#
		def save( filename )			
			raise Error.new( 'save is abstract.  Override in translator class.' )
		end

		#---------------------------------------------------------------------
		# Protected
		#---------------------------------------------------------------------	
	
	protected
		
		attr_reader :xml_filename
		attr_reader :log_name
		
		#
		# == Description
		# Class wrapper around XML log event data. 
		#
		class XmlLogEvent
			
			attr_reader :datetime
			attr_reader :message
			attr_reader :level
			attr_reader :trace_info
			
			def initialize( datetime, message, level, trace_info = nil )
				@datetime = datetime
				@message = message
				@level = level
				@trace_info = trace_info
			end
		end
		
		#
		# == Description
		# Class wrapper around XML log event trace information data.
		#
		class XmlLogEventTrace
			
			attr_reader :filename
			attr_reader :method_name
			attr_reader :line
			
			def initialize( filename, method, line )
				@filename = filename
				@method_name = method
				@line = line
			end
		end
		
		#
		# Override this method in your concrete translator classes.  This is 
		# invoked for every XML event tag within the XML tree structure.
		# 
		# +event+ is a XmlLogEvent object instance.
		#
		# See +XmlLogEvent+ class.
		#
		def cb_translate_log_event( event )
			raise Error.new( 'cb_translate_log_event is abstract.  Override in translator class.' )
		end
		
		#---------------------------------------------------------------------
		# Private
		#---------------------------------------------------------------------		
		
	private
	
		# Open and parse the XML file invoking the +cb_translate_log_event+
		# method that concrete implementations must implement.
		def parse()
			
			begin
				xmldoc = REXML::Document.new File.open( @xml_filename, 'r' )
				@log_name = xmldoc.root.attributes[ 'name' ]
				xmldoc.elements.each( 'log/event' ) do |event_elem|
					
					# Create event and trace (if available) objects.
					trace_info = nil
					if nil != event_elem.elements['trace'] then
						trace_elem = event_elem.elements['trace']
						trace_info = XmlLogEventTrace.new( trace_elem.attributes['file'],
									trace_elem.attributes['method'],
									trace_elem.attributes['line'].to_i )
					end
					event = XmlLogEvent.new( DateTime.parse( event_elem.attributes['timestamp'] ),
									event_elem.attributes['message'], 
									event_elem.attributes['type'],
									trace_info )
					
					# Invoke derived class translate event callback method.
					cb_translate_log_event( event )
				end
			rescue Exception => ex
				
				print_exception( ex )
			end
		end
	end
	
end # Pipeline module

# End of logxmltranslator.rb
