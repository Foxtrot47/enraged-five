#
# erblogtranslator.rb
# Generic ERB Xml Log Translator.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 1 April 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/log/xmllogtranslator'
require 'pipeline/util/debug'
require 'erb'

module Pipeline
	
	#
	# == Description
	# An ERB log translator class that takes an XML log and translates it
	# using an ERB template.  This template has all of the log events and
	# trace information bound and can format the log as required.
	#
	# The ERB template can use the @events array to read through all of the
	# events in the log to be translated.
	#
	class ErbLogTranslator < XmlLogTranslator
		
		attr_reader :translated_output
		attr_reader :events

		def initialize( _filename, erb_filename )
			raise Error.new( "XML file #{_filename} does not exist." ) \
				unless File.exists?( _filename )
			raise Error.new( "XML file #{_filename} is not readable." ) \
				unless File.readable?( _filename )
			raise Error.new( "ERB template #{erb_filename} does not exist." ) \
				unless File.exists?( _filename )
			raise Error.new( "ERB template #{erb_filename} is not readable." ) \
				unless File.readable?( _filename )
			
			@erb_filename = erb_filename
			@events = []
			super( _filename )
		end

		#
		# Invoke our superclass' translate method and then use ERB to process
		# our events.
		#
		def translate()
			super()
			
			# Open the ERB template, bind our scope to it and save the 
			# output to our specified output file.
			erb_template = IO.readlines( @erb_filename )
			erb_report = ERB.new( erb_template.to_s, 0, "%<>" )
			@translated_output = ( erb_report.result( binding ) )			
		end

		#
		# Our save method processes the cached events using the ERB template
		# supplied to the constructor.
		#
		def save( filename )
			begin
				
				File.open( filename, 'w' ) do |fp|
					fp.write( @translated_output )
				end			
			rescue Exception => ex
				print_exception( ex )
			end
		end
		
		#---------------------------------------------------------------------
		# Protected
		#---------------------------------------------------------------------
		
	protected
		
		#
		# Our log event callback handler simply caches all of our events and
		# trace information so that it can be processed later using an ERB
		# template.
		#
		def cb_translate_log_event( event )
			@events << event
		end
	end
		
end # Pipeline module

# End of buildlogtranslator.rb
