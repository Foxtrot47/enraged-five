#
# File:: pipeline/log/colour_console_outputter.rb
# Description:: Log4r console colour outputter.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 19 November 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/win32/console'
require 'log4r/formatter/formatter.rb'

#require 'win32console'
#require 'Win32/Console/ANSI' if ( PLATFORM =~ /win32/ )
#require 'term/ansicolor'
#include Term::ANSIColor
#include ::Win32::Console::ANSI

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline

	#
	# == Description
	#
	#
	class ColourConsoleOutputter < ::Log4r::StdoutOutputter
	
		def initialize( _name, hash = {} )
			super
			@formatter = ::Log4r::BasicFormatter::new( )
		end	

		def write( data )
		
			throw ArgumentError.new( "ColourConsoleOuputtter::write received non-LogEvent object (#{data.class})." ) \
				unless ( data.is_a?( ::Log4r::LogEvent ) )
			
			@out.write sprintf( "%*s ", 8, Log4r::LNAMES[data.level] )
			@out.write sprintf( "%s", data.name )
			@out.write ": " if ( data.tracer.nil? )
			@out.write "(#{data.tracer[0]}): " unless ( data.tracer.nil? )
			@out.write "#{data.data}\n"
		end
		
=begin	
		def initialize( _name, hash = {} )
			super
			@out = ::Win32::Console::ANSI::IO.new()
			@formatter = ::Log4r::BasicFormatter::new( )
		end
	
	private
		COLOURS = {
			# :level => escape string start
			0 => reset,
			1 => blue,
			2 => [green, bold],
			3 => [yellow, bold],
			4 => [red, bold],
			5 => [red, bold],
			:reset => reset
		}
	
		# Override the write method to include color depending on the log
		# level received.  We expect the data argument to come from our
		# AntiFormatter (otherwise its a String and cannot tell its level).
		def write( data )
			throw ArgumentError.new( "ColourConsoleOuputtter::write received non-LogEvent object (#{data.class})." ) \
				unless ( data.is_a?( ::Log4r::LogEvent ) )
			
			@out.write COLOURS[data.level]
			@out.write sprintf( "%*s ", 8, Log4r::LNAMES[data.level] )
			@out.write COLOURS[:reset]
			@out.write sprintf( "%s", data.name )
			@out.write COLOURS[:reset]
			@out.write ": " if ( data.tracer.nil? )
			@out.write "(#{data.tracer[0]}): " unless ( data.tracer.nil? )
			@out.write "#{data.data}\n"
		end
=end		
	end
	
end # Pipeline module

# pipeline/log/colour_console_outputter.rb
