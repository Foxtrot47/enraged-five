#
# File:: pipeline/log/lograge.rb
# Description:: RAGE Logging Functionality
#
# Author:: Greg Smith <greg@rockstarnorth.com>
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 26 January 2009
#

=begin

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/log/log'
require 'pipeline/win32/console'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline

	#
	# == Description
	# IO class that detects output from our RAGE Gem, stripping it out and
	# passing it to the RAGE Log object.
	#			
	#class StdOutRageLogIO < ::Win32::Console::ANSI::IO
	class StdOutRageLogIO

		# Override +write+ method with an +internal_write+ to detect RAGE
		# Gem output.  If the output doesn't contain any RAGE Gem messages
		# then we simply pass it through to our super class.
		def write( *buf )
			if ( not internal_write( *buf ) ) then
				super( buf )
			end
		end	

		#--------------------------------------------------------------------
		# Private
		#--------------------------------------------------------------------
	private
		RAGE_PREPEND = "___rage: "
		RAGE_DEBUG_PREPEND = "Debug: "
		RAGE_WARNING_PREPEND = "Warning: "
		RAGE_ERROR_PREPEND = "Error: "
		RAGE_FATAL_PREPEND = "Fatal Error: "

		def convert_buffer( buf )
			outlines = buf.split( "\n" )
			outlines.each do |outline|			
				yield( outline ) if ( block_given? )
			end
		end		
	
		#
		def internal_write( buf )
			return unless ( buf.is_a?( String ) )
			
			ret = false
			if buf.index(RAGE_PREPEND) != nil then
				
				buf = buf[RAGE_PREPEND.length...buf.length]
				ragelog = LogSystem::instance( ).ragelog
				
				convert_buffer( buf ) do |outline|	
					
					if outline.index( RAGE_ERROR_PREPEND ) != nil then 
						outline = outline[RAGE_ERROR_PREPEND.length...outline.length]
						ragelog.error(outline)
					elsif outline.index( RAGE_FATAL_PREPEND ) != nil then
						outline = outline[RAGE_FATAL_PREPEND.length...outline.length]
						ragelog.fatal(outline)
					elsif outline.index( RAGE_WARNING_PREPEND ) != nil then
						outline = outline[RAGE_WARNING_PREPEND.length...outline.length]
						ragelog.warn(outline)
					elsif outline.index( RAGE_DEBUG_PREPEND ) != nil then
						outline = outline[RAGE_DEBUG_PREPEND.length...outline.length]
						ragelog.debug(outline)
					else
						ragelog.info( outline )
					end
				end		
				ret = true
			end		
			ret		
		end	
	end

end # Pipeline module

=end

# pipeline/log/lograge.rb
