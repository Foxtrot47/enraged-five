#
# File:: %RS_TOOLSLIB%/pipeline/log/rollingdatefileoutputter.rb
# Description:: Rolling Date
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 9 April 2008
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/os/file'
require 'date'
require 'log4r/outputter/fileoutputter'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
	
	#
	# == Description
	# Log4r file outputter that maintains a daily archive of logs.  Ideal for
	# long running process', e.g. our SCM asset monitoring/rebuild scripts.
	# 
	# This is somewhat based on the Log4r::RollingFileOutputter.
	# 
	# The main log filename always contains the most-current log data.  The
	# archived files are made as the day ticks over.
	#
	class RollingDateFileOutputter < Log4r::FileOutputter

		def initialize( _name, hash={} )
			super( _name, hash )
			@count = 0
			@created_at = Time.now
			
			@filename_base = OS::Path.get_basename( @filename )
			@filename_ext = OS::Path.get_extension( @filename )
			@out.sync = false
		end
	
		#---------------------------------------------------------------------
		# Private Methods
		#---------------------------------------------------------------------
		
	private
	
		def write( data )
			roll if requires_roll
			super
		end
		
		def get_archive_filename()
			newbase = "#{@filename_base}-#{@created_at.strftime('%Y-%m-%d')}.#{@filename_ext}"
			OS::Path.combine( OS::Path.get_directory( @filename ), newbase )		
		end
		
		def requires_roll()			
			@created_at.day != Time.now.day
		end
		
		def roll()
			begin				
				@out.close()
				OS::FileUtilsEx.copy_file( @filename, get_archive_filename() )
			rescue Exception => ex
				Log4r::Logger.log_internal do
					"RollingDateFileOutputter exception: #{ex.message}"
				end
			end
			@created_at = Time.now
			@out = File.new( @filename, "w" )
			@out.sync = false
		end
	end
	
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/log/rollingdatefileoutputter.rb
