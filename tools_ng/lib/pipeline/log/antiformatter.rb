#
# File:: pipeline/log/antiformatter.rb
# Description:: Log4r anti-formatter object that doesn't do *any* formatting.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 19 November 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'log4r'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline

	#
	# == Description
	# Some custom Log4r outputters want to receive the Log4r Event objects
	# untouched.  If you use this formatter that is what the Outputter
	# class' write method will get instead of a String object.
	#
	class AntiFormatter < ::Log4r::Formatter
		
		# Format the event by just passing the event object to the outputter.
		def format( event )
			event
		end
	end
	
end # Pipeline module

# pipeline/log/antiformatter.rb
