#
# File:: pipeline/config/runlines.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 23 November 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/gui/exception_dialog'
require 'pipeline/log/log'
require 'pipeline/util/environment'
include Pipeline
require 'rexml/document'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
	
	#
	# == Description 
	# A command to run at install of uninstall of the pipeline
	# 
	class RunLine
		attr_reader :can_fail	# Can command fail boolean.
		attr_reader :run_once	# Only run once boolean.
		attr_reader :version	# Version runline appeared in config.
		attr_reader :maxonly # Only install on 3dsmax user machines
		
		def initialize( can_fail, run_once, version, max_only  = false )
			@can_fail = can_fail
			@run_once = run_once
			@version = version
			@maxonly = max_only
		end	
		
		# Return RunLine log object.
		def RunLine::log( )
			@@log = Log.new( 'runlines' ) if ( @@log.nil? )
			@@log
		end
		
		#--------------------------------------------------------------------
		# Private
		#--------------------------------------------------------------------
	private
		@@log = nil
	end
	
	#
	# == Description 
	# A dos command line to run at install of uninstall of the pipeline.
	# 	
	class RunLineCmd < RunLine
		attr_reader :cmd	# Command String.
		
		# Constructor.
		def initialize( cmd, can_fail, run_once, version, max_only = false )
			super( can_fail, run_once, version, max_only )
			@cmd = cmd
		end
		
		# Execute command.
		def run( )
			RunLine::log().info( "Runline command executing: #{@cmd}." )
			success = true
			begin
				retval = system( @cmd)
				if retval != true and @canfail == false then
					success = false
					RunLine::log().error( "Runline command failed: #{@cmd}; returned: #{retval}." )
				end	
			rescue Exception => ex
				GUI::ExceptionDialog::show_dialog( ex, 'Unhandled exception during command execution' )
				success = false
			end
			success		
		end
		
		# REXML::Element
		def RunLineCmd::from_xml( xml_node, env )
			throw ArgumentError.new( "Invalid REXML::Element object (#{xml_node.class})." ) \
				unless ( xml_node.is_a?( REXML::Element ) )
			throw ArgumentError.new( "Invalid Environment object (#{env.class})."  ) \
				unless ( env.is_a?( Pipeline::Environment ) )
			
			cmd = env.subst( xml_node.attributes['cmd'] )
			canfail = ( 0 == xml_node.attributes['canfail'].casecmp( 'true' ) ) \
				unless ( xml_node.attributes['canfail'].nil? )
			runonce = ( 0 == xml_node.attributes['runonce'].casecmp( 'true' ) ) \
				unless ( xml_node.attributes['runonce'].nil? )
			version = xml_node.attributes['version'].to_i \
				unless ( xml_node.attributes['version'].nil? )
			maxonly = ( 0 == xml_node.attributes['maxonly'].casecmp( 'true' ) ) if xml_node.attributes['maxonly'] != nil
			
			RunLineCmd.new( cmd, canfail, runonce, version )
		end	
	end
	
	#
	# == Description  
	# A ruby script line to run at install of uninstall of the pipeline
	# 	
	class RunLineScript < RunLine
		attr_reader :script
		
		# Constructor.
		def initialize( script, can_fail, run_once, version, max_only = false  )
			super( can_fail, run_once, version, max_only )
			@script = script
		end
		
		# Run script.
		def run( )
			RunLine::log().info( "Runline script executing: #{@script}." )
			success = true
			begin
				ret = Kernel.eval( @script )
				if ret != true and @canfail == false then
					success = false
					RunLine::log().error( "RunLine script failed: #{@script}; returned: #{ret}." )
				end
			rescue Exception => ex
				GUI::ExceptionDialog::show_dialog( ex, 'Unhandled exception during script evaluation' )
				success = false
			end
			success			
		end	
		
		# REXML::Element
		def RunLineScript::from_xml( xml_node, env )
			throw ArgumentError.new( "Invalid REXML::Element object (#{xml_node.class})." ) \
				unless ( xml_node.is_a?( REXML::Element ) )
			throw ArgumentError.new( "Invalid Environment object (#{env.class})."  ) \
				unless ( env.is_a?( Pipeline::Environment ) )
			
			script = env.subst( xml_node.attributes['scriptcmd'] )
			canfail = ( 0 == xml_node.attributes['canfail'].casecmp( 'true' ) )
			runonce = ( 0 == xml_node.attributes['runonce'].casecmp( 'true' ) )
			version = xml_node.attributes['version'].to_i
			maxonly = ( 0 == xml_node.attributes['maxonly'].casecmp( 'true' ) ) if xml_node.attributes['maxonly'] != nil
			
			RunLineScript.new( script, canfail, runonce, version )
		end			
	end
	
end # Pipeline module

# pipeline/config/runlines.rb
