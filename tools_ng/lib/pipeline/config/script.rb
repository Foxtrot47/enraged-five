#
# File:: script.rb
# Description:: Script configuration data.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 26 August 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
# None

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline

	#
	# == Description
	# Project branch script configuration class.
	#
	class ScriptConfig
		attr_reader :branch
		attr_reader :configurations
	
		def initialize( branch, configurations = {} )
			throw ArgumentError.new( "Invalid branch object (#{branch.class})." ) \
				unless ( branch.is_a?( Pipeline::Branch ) )
			throw ArgumentError.new( "Invalid configuration Hash." ) \
				unless ( configurations.is_a?( Hash ) )
			
			@branch = branch
			@configurations = configurations
		end
		
		def fill_env( env )
			@branch.fill_env( env )
		end
	
		def in_env( &block )
            env = Environment.new()
            fill_env( env )           
            yield env if ( block_given? )
		end
	
        #---------------------------------------------------------------------
        # Class Methods
        #---------------------------------------------------------------------	
	
		#
		# Parse script data from an XML node, either creating a new script
		# object or overwriting data in the specified script object.
		#
		# The new (or updated) Script object is returned.
		#
		# This allows us to have local.xml overwrite anything in config.xml.
		#
		def ScriptConfig::from_xml( xml_node, env, branch, script = nil )
			throw ArgumentError.new( "Invalid Script object XML specified (#{xml_node.name})." ) \
				unless ( xml_node.name == 'script' )
			throw ArgumentError.new( "Invalid Script object specified (#{script.class})." ) \
				unless ( script.nil? or script.is_a?( Pipeline::ScriptConfig ) )
			return if ( xml_node.nil? )
			
			env.push( )
			script = ScriptConfig.new( branch ) if ( script.nil? )
			script.fill_env( env )
			
			if ( not xml_node.elements['configurations'].nil? ) then
				cfg_node = xml_node.elements['configurations']
				script.configurations.clear()
				cfg_node.elements.each do |cfg_elem|
					name = cfg_elem.attributes['name']
					enabled_text = cfg_elem.attributes['enabled']
					enabled = ( "true" == enabled_text ) ? true : false
					script.configurations[name] = enabled
				end
			end
			env.pop( )
			
			script
		end
	end

end # Pipeline module

# script.rb
