#
# sharedtexturelist.rb
# Shared Texture List XML File
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 22 February 2008
#

require 'pipeline/log/log'

require 'rexml/document'
include REXML

module Pipeline
module XML

    #
    # == Description
    #
    # Represents a standard shared texture list XML file.  Currently only
    # read support is available.  Lists can be edited in 3dsmax using a similar
    # class - there is also a standard rollout UI defined for editing such 
    # lists.
    #
    class SharedTextureList
    
        #---------------------------------------------------------------------
        # Public Attributes
        #---------------------------------------------------------------------
    
    public
    
        attr_reader :filename   # Shared texture list XML filename
        attr_reader :textures   # Array of texture filenames in list
        
        #---------------------------------------------------------------------
        # Public Methods
        #---------------------------------------------------------------------  
        
    public
    
        #
        # == Description
        #
        # Class constructor taking shared texture list XML filename as a
        # parameter.  This will auto-parse the file and populate the textures
        # readonly attribute array.
        #
        def initialize( filename )
        
            @log = Log.new( 'Shared Texture List' )
        
            begin
            
                self.load( filename )
            rescue Exception => ex
            
                @log.error( "Shared Texture List Exception: #{ex.message}" )
                @log.error( "  Backtrace: #{ex.backtrace().join()}" )
                puts "Shared Texture List Exception: #{ex.message}"
                puts "  Backtrace: "
                
                ex.backtrace.each { |line|
                
                    puts "  #{line}\n"
                }
            end
        end
    
        #
        # == Description
        #
        # Load XML file as shared texture resource.
        #
        def load( filename )    
            
            @textures = Array.new()
            @filename = filename
            File.open( @filename ) do |file|
                    
                doc = Document.new( file )
                
                doc.elements.each( "shared_textures/texture" ) do |texturenode|
                
                    @textures << texturenode.attributes["filename"]
                end
            end 
        end     
    end

end # XML module
end # Pipeline module

# End of sharedtextureslist.rb
