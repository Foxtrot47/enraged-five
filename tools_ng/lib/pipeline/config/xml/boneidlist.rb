#
# boneidlist.rb
# Bone ID XML File Parser
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Edited:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# 
# Date:: 25 February 2008
#

require 'pipeline/log/log'

require 'rexml/document'
include REXML

module Pipeline
module XML

    #
    # == Description
    #
    # Represents a single Bone ID / name pair.
    #
    class BoneID
    
        #---------------------------------------------------------------------
        # Public Attributes
        #---------------------------------------------------------------------
    
    public
    
        attr_reader :id         # Bone ID (int)
        attr_reader :name       # Bone name (string)
        
        #---------------------------------------------------------------------
        # Public Methods
        #---------------------------------------------------------------------  
        
    public
    
        def initialize( id, name )
        
            @id = id
            @name = name
        end 
    end

	#
	# == Description
	#
	# Represents a standard Bone ID list XML file.
	#
	class BoneIDList
	
		#---------------------------------------------------------------------
		# Public Attributes
		#---------------------------------------------------------------------
	
	public
	
		attr_reader :filename	# Shared texture list XML filename
		attr_reader :bones		# Array of bone ID / name pairs
		
		#---------------------------------------------------------------------
		# Public Methods
		#---------------------------------------------------------------------	
		
	public
	
		#
		# == Description
		#
		# Class constructor taking shared texture list XML filename as a
		# parameter.  This will auto-parse the file and populate the textures
		# readonly attribute array.
		#
		def initialize( filename )
		
			@log = Log.new( 'Bone ID List' )
		
			begin
			
				self.load( filename )
			rescue Exception => ex
			
				@log.error( "Bone ID List Exception: #{ex.message}" )
				@log.error( "  Backtrace: #{ex.backtrace().join()}" )
				puts "Bone ID List Exception: #{ex.message}"
				puts "  Backtrace:"
				puts "  #{ex.backtrace().join( '\r\n' )}"
			end
		end
	
		#
		# == Description
		#
		# Load XML file as shared texture resource.
		#
		def load( filename )	
			
			@bones = Array.new()
			@filename = filename
			File.open( @filename ) do |file|
					
				doc = Document.new( file )
				
				doc.elements.each( "bones/bone" ) do |bone|
				
					@bones << BoneID.new( bone.attributes["id"].to_i, 
								bone.attributes["name"] )
				end
			end	
		end	
		
		#
		# == Description
		#
		# Given a bonename, return the id.
		#
		def get_bone_id( bonename )
			
			@bones.size.times { |i|
				
				if @bones[i].name == bonename then
					
					return @bones[i].id.to_i
				end
			}
			@log.error( "Bone " + bonename.to_s + " does not have an ID listed in " +  @filename.to_s )
			return false
		end	
	end

end # XML module
end # Pipeline module

# End of boneidlist.rb
