#
# File:: targets.rb
# Description:: Target class definitions.
#
# Author:: Greg Smith <greg@rockstarnorth.com>
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 20 June 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
# None

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline

	#
	# == Description 
	# Base class functionality for all targets including the independent one.
	#
	class TargetBase
		#---------------------------------------------------------------------
		# Attributes
		#---------------------------------------------------------------------
		attr_accessor :platform
		attr_accessor :project
		attr_accessor :branch
		attr_accessor :target

		#---------------------------------------------------------------------
		# Methods
		#---------------------------------------------------------------------
	
		# Virtual attribute to make sense.
		def name( )
			@platform
		end

		#
		# Fill an environment object from this object's state
		#       
		def fill_env( env )
			# Ensure we get the branch's environment.
			@branch.fill_env( env )
			
			env.add( 'platform', @platform )
			env.add( 'target', env.subst( @target ) )
		end
		
		#
		# Run a code block setting up an environment from this object
		#           
		def in_env( &block )
			env = Environment.new()
			fill_env(env)
			yield( env ) if ( block_given? )
		end     
		
	protected
	
		def initialize( platform, project, branch, target )

			@platform = platform
			@project = project
			@branch = branch
			@target = target
		end
	end

	#
	# == Description 
	# A special case target for the intermediate exported data.
	#
	class IndependentTarget < TargetBase
	
		#---------------------------------------------------------------------
		# Methods
		#---------------------------------------------------------------------
		def initialize( project, branch )
			super( 'independent', project, branch, branch.export )			
		end	
	end

	#
	# == Description 
	# The Project class contains an array attribute of ProjectTarget objects.
	# This contains the platform string and platform directory for the target
	# machine of the project along with any other per target settings.
	#
	class Target < TargetBase

		#---------------------------------------------------------------------
		# Attributes
		#---------------------------------------------------------------------
		attr_accessor :code_platform
		attr_accessor :build_label_prefix
		attr_accessor :enabled
		
		#---------------------------------------------------------------------
		# Instance Methods
		#---------------------------------------------------------------------

		def initialize( platform, project, branch, target, code_platform = '', build_label_prefix = '' )

			super( platform, project, branch, target )
			@code_platform = code_platform
			@build_label_prefix = build_label_prefix
			@enabled = false
		end

		def fill_env( env )
			super( env )
			env.add( 'code_platform', @code_platform ) unless @code_platform.nil?
			env.add( 'build_label_prefix', @build_label_prefix ) unless @build_label_prefix.nil?
		end
		
		def to_local_xml( )		
			target_elem = Element.new( 'target' )
			target_elem.attributes['platform'] = @platform
			target_elem.attributes['enabled'] = @enabled.to_s
			target_elem
		end

		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		
		#
		# Parse Target data from an XML node, either creating a new Target
		# object or overwriting data in the specified Target object.
		#
		# The new (or updated) Target object is returned.
		#
		# This allows us to have local.xml overwrite anything in config.xml.
		#
		def Target::from_xml( xml_node, project, branch, env, target = nil )
			throw ArgumentError.new( "Invalid Project object specified (#{project.class})." ) \
				unless ( project.is_a?( Pipeline::Project ) )
			throw ArgumentError.new( "Invalid Branch object specified (#{branch.class})." ) \
				unless ( branch.nil? or branch.is_a?( Pipeline::Branch ) )
			throw ArgumentError.new( "Invalid Target object specified (#{target.class})." ) \
				unless ( target.nil? or target.is_a?( Pipeline::TargetBase ) )
		
			platform = xml_node.attributes['platform']
			throw XMLParseError.new( 'Missing target platform attribute.  XML parse error?' ) \
				if ( platform.nil? )
			
			platform.downcase!
			path = xml_node.attributes['path'] unless xml_node.attributes['path'].nil?
			target = Target.new( platform, project, branch, path ) if ( target.nil? )
			
			code_platform = xml_node.attributes['code_platform'] unless xml_node.attributes['code_platform'].nil?
			build_label = xml_node.attributes['build_label'] unless xml_node.attributes['build_label'].nil?
			enabled = xml_node.attributes['enabled'] unless xml_node.attributes['enabled'].nil?
			
			target.target = path unless path.nil?
			target.enabled = ( 0 == enabled.casecmp( 'true' ) ) unless ( enabled.nil? )
			target.code_platform = code_platform unless ( code_platform.nil? )
			target.build_label = build_label unless ( build_label.nil? )
						
			target
		end
	end

end # Pipeline module

# targets.rb
