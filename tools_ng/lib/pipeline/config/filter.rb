#
# File:: pipeline/config/filter.rb
# Description:: Content filtering methods
#
# Author:: Mark Harrison-Ball <mark.harrison-ball@rockstargames.com>
# Date:: 10 September 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'rexml/document'
include REXML

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline

	#
	# == Description
	# Filter XML content nodes by path.
	#
	def Pipeline::filter_paths( node, match )
			removals = []
			node.elements.each do |element|
					type = element.attributes["type"]
					next unless ( 0 == 'level'.casecmp( type ) )
					path = element.attributes["path"]
			
					next unless path.downcase!=match.downcase
			
					removals << element
			end
			removals.each do |element|
					node.elements.delete element
			end
	end

	#
	# == Description
	# Method to filter content files by level.
	#
	def Pipeline::filter_level( level, filename, sourcedoc )
			root = sourcedoc.root
						
			if filename =~ /maps.xml$/
				filter_paths( sourcedoc.root, level )
					
			elsif filename =~ /output.xml$/
				sourcedoc.root.elements[1].elements.each do |outer|
					outer.elements.each do |inner|
						if ( 0 == 'levels'.casecmp( inner.attributes['path'] ) )
							filter_paths( inner, level )
						end
					end
				end
			elsif filename =~ /processed.xml$/
				
				sourcedoc.root.elements[1].elements.each do |outer|
					outer.elements.each do |inner|						
						if ( 0 == 'levels'.casecmp( inner.attributes['path'] ) )
							filter_paths( inner, level )
						end
					end
				end
			end
			
			## DEBUG
			## ::File.open(filename+"_", "w") { |file| sourcedoc.write(file) }
			
			return sourcedoc
	end

end # Pipeline module

# pipeline/config/filter.rb
