#
# File:: user.rb
# Description:: User type class and constants.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 26 August 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'rexml/document'
include REXML

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline

	#
	# == Description
	# UserException class for raising errors with the User class (defined
	# below).
	#
	class UserException < Exception; end

	#
	# == Description
	# User type abstraction.  A user is defined by a set of flags that 
	# determine features or priviliges they have in the pipeline system.
	#
	class User
		# used to identify if a build is taking place on the assetbuilder.
		ASSETBUILDER_USER = "builder"
	
		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		def User::from_uiname( uiname )
			@@usertypes.each_pair do |key, usertype|
				return User.new( usertype.flags ) if ( 0 == uiname.casecmp( usertype.uiname ) )
			end
			throw UserException.new( "Invalid user uiname '#{uiname}'." )
		end
		
		def User::from_name( name )
			@@usertypes.each_pair do |key, usertype|
				return User.new( usertype.flags ) if ( 0 == name.casecmp( usertype.name ) )
			end
			throw UserException.new( "Invalid user name '#{name}'." )
		end
		
		def User::from_flags( flags )
			@@usertypes.each_pair do |key, usertype|
				return User.new( usertype.flags ) if ( flags&@@pseudoUserMask == usertype.flags&@@pseudoUserMask )
			end
			throw UserException.new( "Invalid user flags '#{flags}'." )
		end
		
		def User::from_xml( xml_node )
			throw UserException.new( "Invalid XML node (#{xml_node.class}, #{xml_node.name})." ) \
				unless ( xml_node.is_a?( Element ) or ( xml_node.name != 'user' ) )
				
			flags = xml_node.attributes['flags'].to_i
			username = xml_node.attributes['username']
			email = xml_node.attributes['emailaddress']
			
			User.new( flags, username, email )
		end

		#---------------------------------------------------------------------
		# Class Attributes
		#---------------------------------------------------------------------

		#
		# Return our Hash of Usertype objects.  This Hash is defined by the 
		# Pipeline::Config class when it parses the global XML configuration
		# file.
		#
		def User::usertypes( )
		
			@@usertypes
		end

		#
		# This adds user types to our Hash.  Called by the Pipeline::Config
		# XML parser.
		#
		def User::add_usertype( name, usertype )
			throw UserException.new( "Invalid Usertype object '#{usertype.class}'." ) \
				unless ( usertype.is_a?( Usertype ) )
				
			@@usertypes[name] = usertype
		end

		@@usertypes = {}
		
		def User::set_pseudoUserMask ( pseudoUserMask )
			@@pseudoUserMask = pseudoUserMask
		end
		
		@@pseudoUserMask = 0xFFFFFFFF
		
		#---------------------------------------------------------------------
		# Instance Attributes
		#---------------------------------------------------------------------

		attr_accessor :username
		attr_accessor :emailaddress
		attr_reader :flags
		

		#---------------------------------------------------------------------
		# Instance Methods
		#---------------------------------------------------------------------
		def initialize( flags, username = '', email = '' )
		
			@@usertypes.each_pair do |key, usertype|
				areEqual = flags&@@pseudoUserMask == (usertype.flags)&@@pseudoUserMask
				if ( areEqual ) then
					# Initialise
					@flags = flags
					if ( '' == username ) then
						@username = ENV['USERNAME']
					else
						@username = username
					end
					@emailaddress = email
					break;
				end
			end
			throw UserException.new( "Invalid user flags '#{flags}'." ) \
				if ( @flags.nil? )
		end

		#
		# Equivalence operator.
		#
		def <=>( other )
			return ( self.flags <=> other.flags )
		end

		def friendlyname( )
			"<TBD>"
		end

		def to_i( )
			@flags
		end
		
		def to_s( )
			friendlyname( )
		end
		
		#
		# Local XML configuration data.
		#
		def to_local_xml( )
			user = Element.new( 'user' )
			user.add_attribute( 'username', @username )
			user.add_attribute( 'emailaddress', @emailaddress )
			user.add_attribute( 'flags', @flags.to_s )
			user
		end
		
		#
		# Automatically handle some useful utility methods, e.g. 
		# 'is_<friendly_name>'.
		#
		def method_missing( name, *args )
		
			# Generic is_<friendly_name> methods
			if ( name.to_s =~ /^is_(.*)/ ) then
				user_name = $1.downcase
				
				user_ok = false
				usertype = nil
				User::usertypes.values.each do |usertype|
					user_ok = ( 0 == usertype.name.casecmp( user_name ) )
					usertype = usertype if user_ok
					break if user_ok
				end
				throw UserException.new( "Invalid user name '#{user_name}'." ) \
					unless ( user_ok )
								
				return ( usertype.flags == ( @flags & usertype.flags ) )
			else			
				raise NameError.new( "No such method #{name.to_s} in class User.", caller )
			end
			false
		end
	end

	#
	# == Description
	# Usertype information as parsed from the global XML file.
	#
	class Usertype
		attr_reader :name
		attr_reader :uiname
		attr_reader :flags
		attr_reader :pseudo
		
		def initialize( name, uiname, flags, pseudo )
			@name = name
			@uiname = uiname
			@flags = flags.to_i
			@pseudo = pseudo
		end

		def <=>( other )
			( @flags <=> other.flags )
		end
	end

end # Pipeline module

# user.rb
