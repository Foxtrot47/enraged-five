#
# File:: pipeline/config/globals.rb
# Description:: Pipeline globals.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 16 October 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/os/path'
require 'pipeline/util/environment'
require 'pipeline/util/string'
require 'singleton'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline

	#
	# == Description
	# Globals class for system-wide globals for current tools branch.
	# This includes the project context for the current tools branch.
	#
	# The globals just come from the user-environment.  Rather than 
	# recalculate then here like we used to do.  To switch project
	# tools you need to re-run the installer anyways.
	#
	class Globals
		include Singleton
		
		attr_reader :toolsbin		# Tools binary directory.
		attr_reader :toolsdrive		# Tools root drive.
		attr_reader :toolslib		# Tools library directory.
		attr_reader :toolsproject	# Tools project context (e.g. 'jimmy', 'gta5').
		attr_reader :toolsprojectroot
		attr_reader :toolsroot		# Tools root directory.
		attr_reader :toolssrc		# Tools source directory.
		attr_reader :toolsruby		# Tools Ruby interpreter executable.
		attr_reader :toolstemp		# Tools temp directory.
		attr_reader :toolsconfig
	
        # Boolean to skip the config version check.  Required when using XGE
        # and the Ruby interpreter.
        attr_accessor :skip_version_check 
 
		# Constructor.  Initialise member data from current environment.
		def initialize( )
            @skip_version_check = false
			@toolsdrive = OS::Path::get_drive( ENV['RS_TOOLSROOT'] + "x:" )
			@toolsroot = OS::Path::normalise( ENV['RS_TOOLSROOT'] )
			@toolsbin = OS::Path::combine( @toolsroot, 'bin' )
			@toolsconfig = OS::Path::combine( @toolsroot , 'etc' )
			@toolslib = OS::Path::combine( @toolsroot, 'lib' )
			@toolsproject = OS::Path::combine( ENV['RS_PROJECT'] )
			@toolssrc = OS::Path::normalise( ENV['RS_TOOLSSRC'] )
			@toolsruby = OS::Path::combine( @toolsroot, 'bin', 'ruby', 'bin', 'ruby.exe' )
			@toolstemp = OS::Path::combine( @toolsroot, 'tmp' )
		end
		
		# Fill an environment object from this object's state
		def fill_env( env )
			env.add( 'toolsdrive', @toolsdrive )
			env.add( 'toolsroot', @toolsroot )
			env.add( 'toolsbin', @toolsbin )
			env.add( 'toolsconfig', @toolsconfig )
			env.add( 'toolslib', @toolslib )
			env.add( 'toolsproject', @toolsproject )
			env.add( 'toolssrc', @toolssrc )
			env.add( 'toolsruby', @toolsruby )
			env.add( 'toolstemp', @toolstemp )
		end

		# Run a code block setting up an environment from this object
		def in_env( )
			env = Environment.new()
			fill_env(env)			
			yield( env ) if ( block_given? )
		end
		
		# Return true iff running a development branch of the tools, 
		# false otherwise.
		def is_dev?( )
			( @toolssrc.starts_with( @toolsroot ) )
		end
		
		# Return toolsroot path to the equivalent tools branch; i.e. if 
		# currently on a development branch this will return you the
		# release path; otherwise the development branch.
		#
		# DHM FIXME 2010/06/17: this needs to be expanded.
		#
		def get_equivalent_branch_toolsroot( )
		
			if ( is_dev? ) then
				# Return release branch.
				OS::Path::combine( ENV['RS_PROJROOT'], 'tools' )
			else
				# Return TOOLSSRC - src.
				OS::Path::get_directory( ENV['RS_TOOLSSRC'] )
			end
		end
	end

end # Pipeline module

# pipeline/config/globals.rb
