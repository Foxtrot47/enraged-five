#
# File:: pipeline/installer/ccintaller.rb
# Description:: Cruise Control Server Installer
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 05 Feb 2010
#

#-----------------------------------------------------------------------------
# Globals
#-----------------------------------------------------------------------------
#$no_project_check = true

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/gui/exception_dialog'
require 'pipeline/gui/messagebox'
require 'pipeline/log/log'
require 'pipeline/os/path'
require 'pipeline/scm/perforce'
require 'pipeline/win32/envvar'
include Pipeline
include Pipeline::GUI

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
logger = Pipeline::Log.new( 'cc_server_installer' )
begin
	title = 'Tools Framework Cruise Control Install'
	conf = Pipeline::Config::instance()
	
	project = conf.projects.values[0]

	cc_enabled = (ENV['RS_REINSTALL'] != "TRUE")
	
	if ( cc_enabled ) then
		
		do_cc = true
		
		user = conf.user
		
		if (	user.is_tools() or 
				user.is_builder_client() or 
				user.is_builder_server() ) then
			
			do_cc = false
			
			title = "Post-install Cruise Control"
			msg = "Cruise Control will configure itself as a capable server on this machine.\n"
			msg += "Is that ok?\n"
			res = MessageBox::question( title, msg, [MessageBox::YES, MessageBox::NO] )			
			
			if ( MessageBox::YES == res ) then				
				do_cc = true			
			else
				logger.warn( "User specified not to auto-configure Cruise Control as a capable server on this machine." )
			end
		else 			
			
		end
		
		if do_cc then
			logger.info( "Setting machine up with capability as a Cruise Control Server." )
			
			toolsroot = OS::Path::normalise( ENV['RS_TOOLSROOT'] )
			toolsdrive = OS::Path::platform_native( OS::Path::get_drive( toolsroot ) + ':' )
			toolsbin = OS::Path::normalise( ENV['RS_TOOLSBIN'] )
			toolsconfig = OS::Path::normalise( ENV['RS_TOOLSCONFIG'] )
			toolslib = OS::Path::normalise( ENV['RS_TOOLSLIB'] )
			toolsruby = OS::Path::normalise( ENV['RS_TOOLSRUBY'] )		

			# Mmm I suppose I could do this using ruby xml xsl transform to do this in script, but for now I'm trying to write the XSL outside of the installer.
			xslexe = "#{OS::Path::combine(toolsbin,"nxslt2.exe")}"
			xsl = "#{OS::Path::combine( toolsconfig, 'cruisecontrol','general','tools_install.xsl')}"
			src = "#{OS::Path::combine( toolsroot, 'local.xml')}"
			dst = "#{OS::Path::combine( toolsconfig, 'cruisecontrol','general','tools_install.xml')}"

			cmdline = "#{xslexe} #{src} #{xsl} -o #{dst}"
			logger.info(cmdline)
			system( cmdline )
		end
	end		
	
	
rescue Exception => ex
	logger.fatal_exception( ex, "Unhandled exception during cc_server_installer: #{ex.message}" )
	ExceptionDialog::show_dialog( ex, 'Unhandled exception during cc_server_installer' )
end

# pipeline/installer/cc_server_installer.rb
