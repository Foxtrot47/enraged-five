#
# File:: pipeline/installer/qtinstaller.rb
# Description:: Qt-based tools framework installer.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Author:: Greg Smith <greg.smith@rockstarnorth.com>
# Date:: 23 November 2009
#

#----------------------------------------------------------------------------
# Globals
#----------------------------------------------------------------------------
$no_project_check = true

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/gui/messagebox'
require 'pipeline/gui/exception_dialog'
require 'pipeline/log/log'
require 'pipeline/util/incredibuild'
require 'pipeline/win32/shell32'

include Pipeline

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Installer
	
	#
	# == Description
	# Top-level framework installer wizard.  This is very basic as the base
	# class handles most things we need.  The Help button callback handler
	# is implemented here however; and just opens the users web-browser
	# to the Pipeline Installer wiki page.
	#
	
	class ToolsWizardPageUserDetails < Wx::WizardPageSimple
	
		def initialize( parent )
			
			super(parent)
			
			self.set_min_size(Wx::Size.new(350,-1))
			
			stc_user = Wx::StaticText.new(self, Wx::ID_ANY, "user name:")
			@txt_user = Wx::TextCtrl.new(self, Wx::ID_ANY, "")
			@txt_user.set_min_size(Wx::Size.new(200,-1))
			stc_email = Wx::StaticText.new(self, Wx::ID_ANY, "e-mail address:")
			@txt_email = Wx::TextCtrl.new(self, Wx::ID_ANY, "")		
			@txt_email.set_min_size(Wx::Size.new(200,-1))

			userchoices = []
			@userflag = []
			index = 0

			User::usertypes.keys.sort.each do |k|
			
				usertype = User::usertypes[k]
				next if ( usertype.pseudo )
				
				#rb = Wx::RadioButton( @rdo_type, Wx::ID_ANY, usertype.uiname )
				#@usertypes[usertype.flags] = rb
				userchoices << usertype.uiname
				@userflag << usertype.flags
				index += 1
			end			
			
			@rdo_type = Wx::RadioBox.new(self, Wx::ID_ANY, "user type:", Wx::DEFAULT_POSITION, Wx::Size.new(200,-1), userchoices, 3)

			top_sizer = Wx::FlexGridSizer.new(2,2)
			top_sizer.add(stc_user,2,Wx::ALIGN_RIGHT)
			top_sizer.add(@txt_user,3,Wx::EXPAND|Wx::ALIGN_RIGHT,1)
			top_sizer.add(stc_email,2,Wx::ALIGN_RIGHT)
			top_sizer.add(@txt_email,3,Wx::EXPAND|Wx::ALIGN_RIGHT,1)
			
			main_sizer = Wx::BoxSizer.new(Wx::VERTICAL)
			main_sizer.add(top_sizer, 0, Wx::ALL | Wx::GROW, 5)
			main_sizer.add(@rdo_type, 0, Wx::ALL | Wx::GROW, 5)
			
			set_sizer main_sizer			
		end
		
		def initialise_page()
		
			get_parent.set_label("r* tools installer: user details")
			
			c = Pipeline::Config::instance( )
			# User settings
			unless ( c.user.nil? )
			
				@txt_user.set_value(c.user.username)
				@txt_email.set_value(c.user.emailaddress)
				
				@userflag.each_index do |i|
				
					@rdo_type.set_selection(i) if @userflag[i] == c.user.flags
				end
			end			
		end
		
		def validate_page()
		
			c = Pipeline::Config::instance( )
			
			# Other fields
			c.user = User::from_flags( @userflag[@rdo_type.get_selection()] )
			
			c.user.username = @txt_user.get_value
			c.user.emailaddress = @txt_email.get_value
		end
	end	
	
	class ToolsWizardPagePerforce < Wx::WizardPageSimple

		def initialize( parent )
			
			super(parent)
			
			stc_server = Wx::StaticText.new(self, Wx::ID_ANY, "scm server:")
			@txt_server = Wx::TextCtrl.new(self, Wx::ID_ANY, "")
			@txt_server.set_min_size(Wx::Size.new(200,-1))
			stc_workspace = Wx::StaticText.new(self, Wx::ID_ANY, "scm workspace:")
			@txt_workspace = Wx::TextCtrl.new(self, Wx::ID_ANY, "")		
			@txt_workspace.set_min_size(Wx::Size.new(200,-1))
			stc_username = Wx::StaticText.new(self, Wx::ID_ANY, "scm user name:")
			@txt_username = Wx::TextCtrl.new(self, Wx::ID_ANY, "")					
			@txt_username.set_min_size(Wx::Size.new(200,-1))
			stc_rage_server = Wx::StaticText.new(self, Wx::ID_ANY, "rage scm server:")
			@txt_rage_server = Wx::TextCtrl.new(self, Wx::ID_ANY, "")
			@txt_rage_server.set_min_size(Wx::Size.new(200,-1))
			stc_rage_workspace = Wx::StaticText.new(self, Wx::ID_ANY, "rage scm workspace:")
			@txt_rage_workspace = Wx::TextCtrl.new(self, Wx::ID_ANY, "")		
			@txt_rage_workspace.set_min_size(Wx::Size.new(200,-1))
			stc_rage_username = Wx::StaticText.new(self, Wx::ID_ANY, "rage scm user name:")
			@txt_rage_username = Wx::TextCtrl.new(self, Wx::ID_ANY, "")		
			@txt_rage_username.set_min_size(Wx::Size.new(200,-1))

			top_sizer = Wx::FlexGridSizer.new(2,6)
			top_sizer.add(stc_server,2,Wx::ALIGN_RIGHT)
			top_sizer.add(@txt_server,3,Wx::EXPAND|Wx::ALIGN_RIGHT,1)
			top_sizer.add(stc_workspace,2,Wx::ALIGN_RIGHT)
			top_sizer.add(@txt_workspace,3,Wx::EXPAND|Wx::ALIGN_RIGHT,1)
			top_sizer.add(stc_username,2,Wx::ALIGN_RIGHT)
			top_sizer.add(@txt_username,3,Wx::EXPAND|Wx::ALIGN_RIGHT,1)
			top_sizer.add(stc_rage_server,2,Wx::ALIGN_RIGHT)
			top_sizer.add(@txt_rage_server,3,Wx::EXPAND|Wx::ALIGN_RIGHT,1)
			top_sizer.add(stc_rage_workspace,2,Wx::ALIGN_RIGHT)
			top_sizer.add(@txt_rage_workspace,3,Wx::EXPAND|Wx::ALIGN_RIGHT,1)
			top_sizer.add(stc_rage_username,2,Wx::ALIGN_RIGHT)
			top_sizer.add(@txt_rage_username,3,Wx::EXPAND|Wx::ALIGN_RIGHT,1)		

			set_sizer top_sizer
		end	

		def initialise_page()
		
			get_parent.set_label("r* tools installer: perforce")
			
			c = Pipeline::Config::instance( )

			@txt_server.set_value(c.sc_server)
			@txt_username.set_value(c.sc_username)
			@txt_workspace.set_value(c.sc_workspace)
			
			if ( c.user.nil? or c.user.is_programmer() ) then
			
				@txt_rage_server.set_editable( true )
				@txt_rage_username.set_editable( true )
				@txt_rage_workspace.set_editable( true )
				@txt_rage_server.set_value(c.sc_rage_server)
				@txt_rage_username.set_value(c.sc_rage_username)
				@txt_rage_workspace.set_value(c.sc_rage_workspace)
			else
			
				@txt_rage_server.set_editable( false )
				@txt_rage_username.set_editable( false )
				@txt_rage_workspace.set_editable( false )
			end			
		end		
		
		def validate_page()
		
			c = Pipeline::Config::instance( )

			c.sc_server = @txt_server.get_value
			c.sc_username = @txt_username.get_value
			c.sc_workspace = @txt_workspace.get_value
			
			if ( c.user.nil? or c.user.is_programmer() ) then
			
				c.sc_rage_server = @txt_rage_server.get_value
				c.sc_rage_username = @txt_rage_username.get_value
				c.sc_rage_workspace = @txt_rage_workspace.get_value
			end
		end		
	end
	
	class ToolsWizardPageProjectDetails < Wx::WizardPageSimple
	
		def initialize( parent )
			
			super(parent)
			
			stc_project = Wx::StaticText.new(self, Wx::ID_ANY, "project:")
			@txt_project = Wx::TextCtrl.new(self, Wx::ID_ANY, "")
			@txt_project.set_editable(false)
			
			project_sizer = Wx::BoxSizer.new(Wx::HORIZONTAL)
			project_sizer.add(stc_project)
			project_sizer.add(@txt_project)
			project_sizer.set_min_size(Wx::Size.new(250,-1))
			
			box_targets = Wx::StaticBox.new(self, Wx::ID_ANY, "targets:")
			
			@platform_chks = Hash.new()
			
      c = Pipeline::Config::instance( )
      c.platforms.values.each { |p|
       
        @platform_chks[p.name] = Wx::CheckBox.new(self, Wx::ID_ANY, p.uiname)
      }
			
			#@chk_ps3 = Wx::CheckBox.new(self, Wx::ID_ANY, "PS3")
			#@chk_360 = Wx::CheckBox.new(self, Wx::ID_ANY, "XBox 360")
			#@chk_win32 = Wx::CheckBox.new(self, Wx::ID_ANY, "Win32")
			
			target_box_sizer = Wx::StaticBoxSizer.new(box_targets,Wx::VERTICAL)
			target_sizer = Wx::BoxSizer.new(Wx::VERTICAL)
			
      c.platforms.values.each { |p|
        
        target_sizer.add(@platform_chks[p.name],3)
	    }

	    target_sizer.set_min_size(Wx::Size.new(250,-1))
			
			target_box_sizer.add(target_sizer)
			
			box_branch = Wx::StaticBox.new(self, Wx::ID_ANY, "branch:")
			
			stc_currentbranch = Wx::StaticText.new(self, Wx::ID_ANY, "current branch:")
			@txt_currentbranch = Wx::ComboBox.new(self, Wx::ID_ANY, "", Wx::DEFAULT_POSITION, Wx::DEFAULT_SIZE, [], Wx::CB_READONLY|Wx::CB_SIMPLE)
			
			evt_combobox(@txt_currentbranch.id) { |event|
			
				on_branch_changed()
			}
			
			stc_ragecode = Wx::StaticText.new(self, Wx::ID_ANY, "rage code:")
			@txt_ragecode = Wx::TextCtrl.new(self, Wx::ID_ANY, "")
			@txt_ragecode.set_editable(false)
			stc_build = Wx::StaticText.new(self, Wx::ID_ANY, "build:")
			@txt_build = Wx::TextCtrl.new(self, Wx::ID_ANY, "")
			@txt_build.set_editable(false)
			
			branch_box_sizer = Wx::StaticBoxSizer.new(box_branch,Wx::VERTICAL)
			branch_sizer = Wx::FlexGridSizer.new(2,3)
			branch_sizer.add(stc_currentbranch,2,Wx::ALIGN_RIGHT)
			branch_sizer.add(@txt_currentbranch,3,Wx::EXPAND|Wx::ALIGN_RIGHT,1)
			branch_sizer.add(stc_ragecode,2,Wx::ALIGN_RIGHT)
			branch_sizer.add(@txt_ragecode,3,Wx::EXPAND|Wx::ALIGN_RIGHT,1)
			branch_sizer.add(stc_build,2,Wx::ALIGN_RIGHT)
			branch_sizer.add(@txt_build,3,Wx::EXPAND|Wx::ALIGN_RIGHT,1)			
			branch_sizer.set_min_size(Wx::Size.new(250,-1))
						
			branch_box_sizer.add(branch_sizer)
			
			main_sizer = Wx::BoxSizer.new(Wx::VERTICAL)
			main_sizer.add(project_sizer)
			main_sizer.add(target_box_sizer)
			main_sizer.add(branch_box_sizer)
			
			set_sizer(main_sizer)
		end

		def initialise_page()
		
			get_parent.set_label("r* tools installer: project")
		
			c = Pipeline::Config::instance( )
		
			@selected_project = c.project
			@selected_project.load_config( )			
		
			@txt_project.set_value(c.project.uiname)	
			
			@txt_currentbranch.clear
			
			c.project.branches.values.each { |branch|
				
				@txt_currentbranch.append( branch.name )
			}
			
			@curr_branch = c.project.default_branch
			
      load_branch_targets()
			
			@txt_currentbranch.set_string_selection(c.project.default_branch)
			on_branch_changed()
		end
		
		def save_branch_targets()

      c = Pipeline::Config::instance()
      
      branch = c.project.branches[@curr_branch]
      
      c.platforms.values.each { |p|
       
        target = branch.targets[p.name] 
        
        target.enabled = @platform_chks[p.name].get_value() if target != nil
      }		  
		end
		
		def load_branch_targets()
		  
      c = Pipeline::Config::instance()
      
      branch = c.project.branches[@curr_branch]
      
      c.platforms.values.each { |p|
        
        chk_current = @platform_chks[p.name]
        target = branch.targets[p.name] 
        
        # Update combobox UI widgets for this project.
        if target != nil then 
        
          chk_current.set_value( target.enabled )
          chk_current.enable
        else
        
          chk_current.set_value( false )
          chk_current.disable
        end
      }
		end
		
		def on_branch_changed( )
		
			c = Pipeline::Config::instance()
			
			branchname = @txt_currentbranch.get_value()
			branch = c.project.branches[branchname]
			
			@txt_ragecode.set_value(branch.ragecode)
			@txt_build.set_value(branch.build)
			
      save_branch_targets()
      @curr_branch = branchname
      load_branch_targets()
		end		
		
		def validate_page()
		
			c = Pipeline::Config::instance()
			branch_name = @txt_currentbranch.get_value()
			
      save_branch_targets()
			
			c.project.default_branch = branch_name
		end		
	end
	
	class ToolsWizardPageAdditional < Wx::WizardPageSimple
	
		def initialize( parent )
			
			super(parent)
			
			#controls
			
			@box_additional = Wx::StaticBox.new(self, Wx::ID_ANY, "additional options:")
			@box_additional.set_min_size(Wx::Size.new(250,-1))
			@chk_use_xge = Wx::CheckBox.new(self, Wx::ID_ANY, "use xoreax xge (when installed)")
			
			@box_logging = Wx::StaticBox.new(self, Wx::ID_ANY, "logging options:")
			@box_logging.set_min_size(Wx::Size.new(250,-1))
			stc_log_level = Wx::StaticText.new(self, Wx::ID_ANY, "log level:")
			@spn_log_level = Wx::SpinCtrl.new(self, Wx::ID_ANY)
			@spn_log_level.set_range( 1, 5 )
					
			@chk_trace = Wx::CheckBox.new(self, Wx::ID_ANY, "trace info")
			@chk_mail_errors = Wx::CheckBox.new(self, Wx::ID_ANY, "mail errors")
			@chk_html_logs = Wx::CheckBox.new(self, Wx::ID_ANY, "generate html logs")
			@chk_stdout_log = Wx::CheckBox.new(self, Wx::ID_ANY, "log to STDOUT")
			
			@box_max = Wx::StaticBox.new(self, Wx::ID_ANY, "max options:")
			@box_max.set_min_size(Wx::Size.new(250,-1))
			stc_toolset = Wx::StaticText.new(self, Wx::ID_ANY, "toolset:")
			@cbo_toolset = Wx::ComboBox.new(self, Wx::ID_ANY, "", Wx::DEFAULT_POSITION, Wx::DEFAULT_SIZE, [], Wx::CB_READONLY|Wx::CB_SIMPLE)
			
			#layout
			loglevel_sizer = Wx::FlexGridSizer.new(2,3)
			loglevel_sizer.add(stc_log_level)
			loglevel_sizer.add(@spn_log_level)
			
			toolset_sizer = Wx::FlexGridSizer.new(2,1)
			toolset_sizer.add(stc_toolset)
			toolset_sizer.add(@cbo_toolset)			

			loglevel_sizer.add(@chk_trace,3)
			loglevel_sizer.add(@chk_mail_errors,3)
			loglevel_sizer.add(@chk_html_logs,3)
			loglevel_sizer.add(@chk_stdout_log,3)
			
			additional_sizer = Wx::StaticBoxSizer.new(@box_additional,Wx::VERTICAL)
			additional_sizer.add(@chk_use_xge)
			additional_sizer.set_min_size(Wx::Size.new(250,-1))
			
			logging_sizer = Wx::StaticBoxSizer.new(@box_logging,Wx::VERTICAL)
			logging_sizer.add(loglevel_sizer,3)
			logging_sizer.set_min_size(Wx::Size.new(250,-1))
			
			max_sizer = Wx::StaticBoxSizer.new(@box_max,Wx::VERTICAL)
			max_sizer.add(toolset_sizer,3)
			max_sizer.set_min_size(Wx::Size.new(250,-1))
			
			main_sizer = Wx::BoxSizer.new(Wx::VERTICAL)
			main_sizer.add(additional_sizer)
			main_sizer.add(logging_sizer)
			main_sizer.add(max_sizer)
			
			set_sizer main_sizer
		end	

		def initialise_page()
		
			get_parent.set_label("r* tools installer: additional")
			
			c = Pipeline::Config::instance( )
			
			@chk_use_xge.set_value( c.use_xge )
			@chk_mail_errors.set_value( c.logmailerrors )
			@chk_stdout_log.set_value( c.logtostdout )
			@chk_trace.set_value( c.log_trace )
			@spn_log_level.set_value( c.log_level )
			@chk_html_logs.set_value( c.log_generate_html )
			
			c.max_options.toolset_modes.each { |toolset_mode|
				
				@cbo_toolset.append( toolset_mode.to_s )
			}				
			
			@cbo_toolset.set_string_selection(c.max_options.toolset_mode.to_s)
		end	

		def validate_page()
		
			c = Pipeline::Config::instance( )
			
			c.use_xge = @chk_use_xge.get_value()
			c.logmailerrors = @chk_mail_errors.get_value()
			c.logtostdout = @chk_stdout_log.get_value()
			c.log_trace = @chk_trace.get_value()
			c.log_level = @spn_log_level.get_value()
			c.log_generate_html = @chk_html_logs.get_value()
			c.max_options.toolset_mode = @cbo_toolset.get_value().intern			
		end		
	end
	
	class ToolsWizardPageInfo < Wx::WizardPageSimple
	
		def initialize( parent )
			
			super(parent)
			
			stc_info = Wx::StaticText.new(self, Wx::ID_ANY, "The Rockstar Games Tools Framework user-setup is now complete.\n\n" \
			"Click Finish to complete the install process.")
		end

		def initialise_page()
		
			get_parent.set_label("r* tools installer: info")
		end		
		
		def validate_page()
		
		end		
	end
	
	class ToolsWizard < Wx::Wizard
	
		def initialize()
	
			super(nil)
						
			evt_wizard_finished( self.id ) { |id|
			
				c = Pipeline::Config::instance( )
				
				# Determine whether the user has enter enough information to install.
				# This handler is also invoked for the Quick Re-install option.
				if ( c.can_install == false ) then
				
					msg = 'Install failed.  You are missing critical user or Perforce information. Please re-run install.bat'
					GUI::MessageBox::error( @title, msg )
					return
				end
									
				install_ok = c.do_install( false )
				
				if ( install_ok ) then
					GUI::MessageBox::information( @title, MSG_SUCCESS )
				else
					GUI::MessageBox::error( @title, MSG_FAILURE )
				end					
			}
			
			evt_wizard_page_changed( self.id ) { | event | 
			
				self.get_current_page.initialise_page
			}			
			
			evt_wizard_page_changing( self.id ) { | event | 
			
				self.get_current_page.validate_page
			}						
		end
				
	private
		HELP_LINK = 'https://devstar.rockstargames.com/wiki/index.php/Pipeline_Installer'
		MSG_SUCCESS = 'Installation successful.'
		MSG_FAILURE = 'Installation failed.  Contact tools.'		
	end
		
end # Installer module
end # Pipeline module

#----------------------------------------------------------------------------
# Entry-Point
#----------------------------------------------------------------------------
begin
	g_AppName = OS::Path::get_basename( __FILE__ )
	log = Pipeline::Log.new( g_AppName )
	title = 'Rockstar Games Tools Framework Installer'
	
	c = Pipeline::Config::instance( )
	
	#enable all projects...
	c.projects.values.each do |project|
	
		project.load_config()
		project.enabled = true
	end	
	
	if ENV['RS_REINSTALL'] == "TRUE" then
	
		c = Pipeline::Config::instance( )
	
		# Determine whether the user has enter enough information to install.
		# This handler is also invoked for the Quick Re-install option.
		if ( c.can_install == false ) then
		
			msg = 'Install failed.  You are missing critical user or Perforce information. Please re-run install.bat'
			GUI::MessageBox::error( @title, msg )
			return
		else

			c.do_install( false )			
		end
	else	
	
		app = GUI::Application::instance( )
		#icon = OS::Path::combine( c.toolslib, 'util', 'data', 'icons', 'rs_icon.bmp' )
		#app.setWindowIcon( Qt::Icon::new( icon ) )
		
		app.do do
		
			wiz = Pipeline::Installer::ToolsWizard.new		
			
			wiz_userdetails = Pipeline::Installer::ToolsWizardPageUserDetails.new(wiz)
			wiz_perforce = Pipeline::Installer::ToolsWizardPagePerforce.new(wiz)
			wiz_project = Pipeline::Installer::ToolsWizardPageProjectDetails.new(wiz)
			wiz_additional = Pipeline::Installer::ToolsWizardPageAdditional.new(wiz)
			wiz_info = Pipeline::Installer::ToolsWizardPageInfo.new(wiz)
				
			Wx::WizardPageSimple::chain(wiz_userdetails,wiz_perforce)		
			Wx::WizardPageSimple::chain(wiz_perforce,wiz_project)
			Wx::WizardPageSimple::chain(wiz_project,wiz_additional)
			Wx::WizardPageSimple::chain(wiz_additional,wiz_info)

			wiz.get_page_area_sizer.add(wiz_userdetails)
			
			wiz.run_wizard(wiz_userdetails)
		end
	end
	
rescue Exception => ex
	puts "Unhandled exception: #{ex.message}"
	puts ex.backtrace.join( "\n" )
	
	log.error( ex, 'Unhandled exception in installer' )
	GUI::ExceptionDialog::show_dialog( ex, 'Unhandled installer exception' )
ensure
	LogSystem::instance( ).shutdown( )	
end

# pipeline/installer/qtinstaller.rb
