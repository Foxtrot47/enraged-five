#
# File:: pipeline/installer/pre_install.rb
# Description:: Pre-install Ruby script.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 5 March 2009
#
# Setup tools installation on a machine.  Can be re-run to alter/fix 
# installation.
#

#-----------------------------------------------------------------------------
# Globals
#-----------------------------------------------------------------------------
$no_project_check = true

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/gui/exception_dialog'
require 'pipeline/gui/messagebox'
require 'pipeline/log/log'
require 'pipeline/os/path'
require 'pipeline/os/start'
require 'pipeline/win32/envvar'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
# None

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

# Return clean Array of PATH environment (all previous tools paths removed).
# The paths returned are in platform native format.
def clean_environment( env )
	throw RuntimeError.new( "Invalid Array of environment data (#{env.class})." ) \
		unless ( env.is_a?( Array ) )
			
	new_env = []
	env.each do |path|
		# This is screwed up; ensure there are no existing quotes in each path,
		# nor that there are any trailing '/' as when converted to platform native
		# paths on Windows they end up '\' and if there is a " after they are escape.
		# Jeeezo that was a pain to track down. DHM 16 June 2010.
		norm_path = OS::Path::normalise( path.strip.gsub( '"', '' ) ).gsub( /\/$/, '' )
		next if ( norm_path.include?( OS::Path::combine( 'tools', 'bin' ) ) )
		
		new_env << OS::Path::platform_native( norm_path )
	end
	new_env
end

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	g_Log = Log::new( 'pre_install' )
	begin
		title = 'Tools Framework Pre-install'

		# Set local variables for our environment.
		# toolsproject = ENV['RS_PROJECT']
		toolsroot = OS::Path::normalise( ENV['RS_TOOLSROOT'] )
		toolsdrive = OS::Path::platform_native( OS::Path::get_drive( toolsroot ) + ':' )
		toolsbin = OS::Path::normalise( ENV['TOOLSBIN'] )
		toolsconfig = OS::Path::normalise( ENV['TOOLSCONFIG'] )
		toolslib = OS::Path::normalise( ENV['TOOLSLIB'] )
		toolsruby = OS::Path::normalise( ENV['TOOLSRUBY'] )
		toolstemp = OS::Path::combine( toolsroot, 'tmp' )
		FileUtils::mkdir( toolstemp ) unless ( File::directory?( toolstemp ) )
		
		# Set Windows system environment.
		Pipeline::Win32::set_envvar( 'RS_TOOLSROOT', OS::Path::platform_native( toolsroot ) )
		
		# Temporarily set up these environment variables as it is screwing up motionbuilder plugins...
		Pipeline::Win32::set_envvar( 'TOOLSDRIVE', toolsdrive )
		Pipeline::Win32::set_envvar( 'TOOLSROOT', OS::Path::platform_native( toolsroot ) )
				
		# We now always set RUBYLIB and RUBYOPT.
		Pipeline::Win32::set_envvar( 'RUBYLIB', OS::Path::platform_native( toolslib ) )
		Pipeline::Win32::set_envvar( 'RUBYOPT', 'rubygems' )
		
		#-----------------------------------------------------------------------------
		# Update user and system PATH environment
		#-----------------------------------------------------------------------------
		rubypath = OS::Path::get_directory( toolsruby )
		
		user_path = Pipeline::Win32::get_envvar( 'PATH', true )
		system_path = Pipeline::Win32::get_envvar( 'PATH', false )
				
		paths = []
		
		paths_initial_system_temp = []
		paths_initial_system_temp = system_path.split( ';' ) unless ( system_path.nil? )
		paths_initial_system = []
		paths_initial_user = []
		paths_initial_user = user_path.split( ';' ) unless ( user_path.nil? )
		
		# Normalise system paths.
		paths_initial_system_temp.each do |path|
			paths_initial_system << OS::Path::normalise( path )	
		end
		
		# Backup old PATH environments to our temp directory.  Lets play safe kids.
		# And don't overwrite this file if it already exists otherwise running
		# installer again you could lose your original path.  Argh.
		system_backup_filename = OS::Path::combine( toolstemp, 'system_path_backup.env' )
		File::open( system_backup_filename, 'w' ) do |fp|
			fp.write( system_path )	
		end	unless ( File::exists?( system_backup_filename ) )	
		user_backup_filename = OS::Path::combine( toolstemp, 'user_path_backup.env' )
		File::open( user_backup_filename, 'w' ) do |fp|
			fp.write( user_path )
		end unless ( File::exists?( user_backup_filename ) )

		# Pre-filter our PATH environment trying to remove all the old crud; i.e.
		# anything that looks like an old tools path setting.
		system_paths = clean_environment( paths_initial_system )
		user_paths = clean_environment( paths_initial_user )
		
		# Filter user PATH environment to remove any paths already in the
		# system PATH environment.
		user_paths.each do |path|
			# Skip entries that are in the system environment.
			next if ( system_paths.include?( path ) )
			paths << path
		end

		# To our filtered PATH environment we then add the two paths we really
		# want to add; %RS_TOOLSBIN% and %RS_TOOLSRUBY%.  Oh the enjoyment.
		paths << OS::Path::platform_native( toolsbin )
		paths << OS::Path::platform_native( rubypath )
		paths.uniq!
		
		# +paths+ should now be a platform native list of path environment with
		# old tools paths removed and our new ones in place.  Lets update it
		# now with the system.
		g_Log.warn( "Updating System PATH to: #{system_paths.join( ';' )}" )
		Pipeline::Win32::set_envvar( 'PATH', system_paths.join( ';' ), false, ::Win32::Registry::REG_EXPAND_SZ )
		
		g_Log.warn( "Updating User PATH to: #{paths.join( ';' )}" )
		Pipeline::Win32::set_envvar( 'PATH', paths.join( ';' ), true )

		# Run the GUI installer
		cmdline = "#{toolsruby} #{OS::Path::combine( toolslib, 'pipeline', 'installer', 'qtinstaller.rb')}"
		system( cmdline )

	rescue Exception => ex
		g_Log.exception( ex, 'Unhandled exception during pre-install' )
		GUI::ExceptionDialog::show_dialog( ex, 'Unhandled exception during pre-install' )
	end
end
	
# pipeline/installer/pre_install.rb
