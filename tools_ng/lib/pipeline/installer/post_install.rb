#
# File:: pipeline/installer/post_install.rb
# Description:: Post-install process.  Currently just sets P4 environment.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 12 March 2009
#

#-----------------------------------------------------------------------------
# Globals
#-----------------------------------------------------------------------------
$no_project_check = true

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/gui/exception_dialog'
require 'pipeline/gui/messagebox'
require 'pipeline/log/log'
require 'pipeline/os/path'
require 'pipeline/scm/perforce'
require 'pipeline/win32/envvar'
require 'pipeline/win32/registry'
include Pipeline
include Pipeline::GUI

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
g_Log = Pipeline::Log.new( 'post_install' )
begin
	title = 'Tools Framework Pre-install'
	conf = Pipeline::Config::instance()
	
	project = conf.project
	project.load_config()
	
	branch = conf.project.branches[conf.project.default_branch]
	
	user = conf.user
	
	Pipeline::Win32::set_envvar( 'RS_PROJECT', OS::Path::platform_native( project.name ) )	
	Pipeline::Win32::set_envvar( 'RS_PROJROOT', OS::Path::platform_native( project.root ) )
	
	rage_assets_path = "#{Pipeline::Globals::instance().toolsdrive}:\\rage\\assets\\"
	
	if ( user.is_programmer() or user.is_tools() ) then			
	
		Pipeline::Win32::set_envvar( 'RAGE_DIR', OS::Path::platform_native(branch.ragecode) )		
		Pipeline::Win32::set_envvar( 'RS_CODEBRANCH', OS::Path::platform_native(branch.code) )
		Pipeline::Win32::set_envvar( 'RAGE_ASSET_ROOT', OS::Path::platform_native(rage_assets_path) )
	end
	
	if ( Autodesk3dsmax::instance().is_installed? ) then
		Pipeline::Win32::set_reg_key( "HKEY_CURRENT_USER\\Software\\Rockstar\\rstMax", "AlphaName", "alpha" )
		Pipeline::Win32::set_reg_key( "HKEY_CURRENT_USER\\Software\\Rockstar\\rstMax", "ShaderName", "default" )
		Pipeline::Win32::set_reg_key( "HKEY_CURRENT_USER\\Software\\Rockstar\\rstMax", "ShaderPath", "#{OS::Path::platform_native( project.root )}\\build\\dev\\common\\shaders" )
	end
	
	Pipeline::Win32::set_envvar( 'RS_BUILDBRANCH', OS::Path::platform_native(branch.build) )
		
	p4 = SCM::Perforce.new()
	p4.port = conf.sc_server
	p4.client = conf.sc_workspace
	p4.user = conf.sc_username
	p4.connect( )
	
	if ( p4.connected? and (ENV['RS_REINSTALL'] != "TRUE")) then

		do_p4_set = true
		
		# If our user is a programmer, tools dude, or a builder then we ask 
		# whether their Perforce environment should be updated.
		if ( user.is_programmer() or user.is_tools() or 
			user.is_builder_client() or user.is_builder_server() ) then		
		
			do_p4_set = false

			title = "Post-install Perforce Environment"
			msg = "Our post-install initialises the Perforce environment so you can\n"
			msg += "use command line scripts etc.\n\n"
			msg += "This will replace your P4PORT, P4CLIENT and P4USER Perforce environment.\n"
			msg += "Is that ok?\n"
			res = MessageBox::question( title, msg, [MessageBox::YES, MessageBox::NO] )			
	
			if ( MessageBox::YES == res ) then
			
				do_p4_set = true			
			else
				g_Log.warn( "User specified not to update Perforce environment." )
			end
		end
		
		# Update Perforce envrionment.
		if do_p4_set then
		
			g_Log.info( "Setting P4PORT to #{conf.sc_server}" )
			system( "p4 set P4PORT=#{conf.sc_server}" )
			g_Log.info( "Setting P4CLIENT to #{conf.sc_workspace}" )
			system( "p4 set P4CLIENT=#{conf.sc_workspace}" )
			g_Log.info( "Setting P4USER to #{conf.sc_username}" )
			system( "p4 set P4USER=#{conf.sc_username}" )	
		end
	else
		g_Log.error( "Failed to connect to P4 server, not updating P4 environment." )
	end

	p4.disconnect( )

	# write out a local batch file that can be used to switch to this set of tool's context easily
	
	batch_filename = Pipeline::Globals::instance().toolsroot + "/script/local/switchenv.bat"

	FileUtils::mkdir_p(OS::Path.get_directory(batch_filename))
	
	File.open(batch_filename,"w+") { |f|
	
		f.write("@echo off\n")
		f.write("SET RAGE_DIR=#{OS::Path::platform_native(branch.ragecode)}\n")
		f.write("SET RAGE_ASSET_ROOT=#{OS::Path::platform_native(rage_assets_path)}\n")		
		f.write("SET RS_BUILDBRANCH=#{OS::Path::platform_native(branch.build)}\n")
		f.write("SET RS_CODEBRANCH=#{OS::Path::platform_native(branch.code)}\n")
		f.write("SET RS_PROJECT=#{OS::Path::platform_native( project.name )}\n")
		f.write("SET RS_PROJROOT=#{OS::Path::platform_native( project.root )}\n")
		f.write("SET RS_TOOLSROOT=#{OS::Path::platform_native( Pipeline::Globals::instance().toolsroot )}\n")
		f.write("IF '%1'=='nopath' GOTO Continue\n")
		f.write("SET PATH=#{Pipeline::Win32::get_envvar( 'PATH')};%PATH%\n")
		f.write(":Continue\n")
	}
	
rescue Exception => ex
	g_Log.fatal_exception( ex, "Unhandled exception during post_install: #{ex.message}" )
	ExceptionDialog::show_dialog( ex, 'Unhandled exception during post_install' )
end

# pipeline/installer/post_install.rb
