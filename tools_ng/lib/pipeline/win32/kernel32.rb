#
# File:: kernel32.rb
# Description:: Windows API kernel32.dll Function Imports / Constants
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 27 June 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
#require 'pipeline/os/path'
require 'Win32API'

module Pipeline
module Win32
module Kernel32

	# Constants
	MAX_PATH_LEN = 260

=begin
	DWORD WINAPI GetLastError(void);

	See: http://msdn.microsoft.com/en-us/library/ms679360(VS.85).aspx
=end
	GetLastError = Win32API.new( 'kernel32', 'GetLastError', '', 'L' )

=begin
	DWORD WINAPI GetTempPath(
	  __in   DWORD nBufferLength,
	  __out  LPTSTR lpBuffer
	);

	See: http://msdn.microsoft.com/en-us/library/aa364992.aspx
=end
	GetTempPath = Win32API.new( 'kernel32', 'GetTempPath', 'LP', 'L' )

=begin
	UINT WINAPI GetSystemDirectory(
	  __out  LPTSTR lpBuffer,
	  __in   UINT uSize
	);

	See: http://msdn.microsoft.com/en-us/library/ms724373(VS.85).aspx
=end
	GetSystemDirectory = Win32API.new( 'kernel32', 'GetSystemDirectory', 'PL', 'L' )

=begin
	UINT WINAPI GetSystemWindowsDirectory(
	  __out  LPTSTR lpBuffer,
	  __in   UINT uSize
	);

	See: http://msdn.microsoft.com/en-us/library/ms724403(VS.85).aspx
=end
	GetSystemWindowsDirectory = Win32API.new( 'kernel32', 'GetSystemWindowsDirectory', 'PL', 'L' )

end # Kernel32 module
end # Win32 module
end # Pipeline module

# End of kernel32.rb
