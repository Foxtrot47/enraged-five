#
# File:: depends.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 28 July 2008
#

require 'pipeline/win32/imagehlp'
require 'pipeline/win32/kernel32'

module Pipeline
module Win32
	
	#
	# == Description
	# Depends module has some utility functions to read the dependencies of
	# Win32 executable images (e.g. DLLs and EXE files).
	#
	module Depends
		
		#
		# Return an array of Win32 image filenames that are dependencies of
		# the specified image file.  An optional search path can be included,
		# if not used the system default is used.
		#
		def Depends.get_image_dependencies( filename, search_path = '' )
			depends = []
			status_routine = ::Win32::API::Callback.new( 'LPPLL', 'L' ) do
				|reason, imagename, dllname, va, parameter|
				begin
				
					case reason
						when Imagehlp::BIND_IMPORT_PROCEDURE
							depends << dllname
						when Imagehlp::BIND_IMPORT_MODULE_FAILED
							depends << dllname
						when Imagehlp::BIND_IMPORT_MODULE
							depends << dllname
					end
				rescue Exception => ex
					throw
				end
				true
			end
			
			begin
				mode = Imagehlp::BIND_NO_BOUNDS_IMPORTS | Imagehlp::BIND_NO_UPDATE | Imagehlp::BIND_ALL_IMAGES
				result = Imagehlp::BindImageEx.call( mode, filename, search_path, nil, status_routine )
				
			rescue Exception => ex
				throw
			end
			depends
		end
		
end # Depends module
end # Win32 module
end # Pipeline module

#-----------------------------------------------------------------------------
# Entry-Point (Unit Test)
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then
	begin
		filename = 'c:/3dsmax9/plugins/GTA3.dle'
		puts "Dependencies for #{filename}:"
		depends = Pipeline::Win32::Depends.get_image_dependencies( filename )
		puts depends.join( "\n\t" ) unless ( nil == depends )
	rescue Exception => ex

		puts "Exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")
	end
end

# depends.rb
