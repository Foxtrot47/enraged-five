#
# File:: stdin_nonblock_gets.rb
# Description:: Monkey-patch STDIN with non-thread-blocking 'gets' method,
#               called 'gets_wt'.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 15 June 2008
#
# References::
#   http://markmail.org/message/pqmmp24cm2bzxnii
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'Win32API' 

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
W_kbhit = Win32API.new( 'msvcrt', '_kbhit', '', 'I' )

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
def STDIN.key_wait 
	while W_kbhit.call == 0; sleep 0.1; end 
end

def STDIN.wt_gets( )
	key_wait( )
	gets( )
end 

# End of stdin_nonblock_gets.rb
