#
# File:: user32.rb
# Description:: Windows API user32.dll Function Imports / Constants
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 25 June 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'Win32API'

module Pipeline
module Win32
module User32

	# LoadIcon Constants
	IDI_APPLICATION     = 32512
	IDI_HAND            = 32513
	IDI_QUESTION        = 32514
	IDI_EXCLAMATION     = 32515
	IDI_ASTERISK        = 32516
	IDI_WINLOGO         = 32517

	# LoadImage Constants
	IMAGE_BITMAP        = 0
	IMAGE_ICON          = 1
	IMAGE_CURSOR        = 2
	LR_DEFAULTCOLOR     = 0x0000
	LR_MONOCHROME       = 0x0001
	LR_COLOR            = 0x0002
	LR_COPYRETURNORG    = 0x0004
	LR_COPYDELETEORG    = 0x0008
	LR_LOADFROMFILE     = 0x0010
	LR_LOADTRANSPARENT  = 0x0020
	LR_DEFAULTSIZE      = 0x0040
	LR_VGACOLOR         = 0x0080
	LR_LOADMAP3DCOLORS  = 0x1000
	LR_CREATEDIBSECTION = 0x2000
	LR_COPYFROMRESOURCE = 0x4000
	LR_SHARED           = 0x8000

=begin
	HICON LoadIcon(      
		HINSTANCE hInstance,
		LPCTSTR lpIconName
	);

	See: http://msdn.microsoft.com/en-us/library/ms648072(VS.85).aspx
=end		
	LoadIcon = Win32API.new( "user32", "LoadIcon", "II", "I" )
	
=begin
	HANDLE LoadImage(      
		HINSTANCE hinst,
		LPCTSTR lpszName,
		UINT uType,
		int cxDesired,
		int cyDesired,
		UINT fuLoad
	);

	See: http://msdn.microsoft.com/en-us/library/ms648045(VS.85).aspx
=end	
	LoadImage = Win32API.new( 'user32', 'LoadImage', "IPIIII", 'I' )

end # User32 module
end # Win32 module
end # Pipeline module

# End of user32.rb
