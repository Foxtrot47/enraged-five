#
# File:: registry.rb
# Description:: 
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 13 May 2010
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Win32

	def Win32::set_reg_key( key, keyname, keyvalue )
		throw ArgumentError.new( "Invalid key (#{key.class})." ) \
			unless ( key.is_a?( String ) )
		throw ArgumentError.new( "Invalid key name (#{keyname.class})." ) \
			unless ( keyname.is_a?( String ) )
		throw ArgumentError.new( "Invalid key value (#{keyvalue.class})." ) \
			unless ( keyvalue.is_a?( String ) )
		
		system( "REG ADD \"#{key}\" /v \"#{keyname}\" /f /d \"#{keyvalue}\"" )	
	end

end # Win32 module
end # Pipeline module

# registry.rb