#
# File:: imagehlp.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 28 July 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'win32/api'


module Pipeline
module Win32
module Imagehlp

	#-------------------------------------------------------------------------
	# Constants
	#-------------------------------------------------------------------------

	# BindImageEx, flags constants
	BIND_ALL_IMAGES = 0x00000004
	BIND_CACHE_IMPORTS_DLLS = 0x00000008
	BIND_NO_BOUNDS_IMPORTS = 0x00000001
	BIND_NO_UPDATE = 0x00000002

	# StatusRoutine
	BIND_OUT_OF_MEMORY = 0
	BIND_RVA_TO_VA_FAILED = 1
	BIND_NO_ROOM_IN_IMAGE = 2
	BIND_IMPORT_MODULE_FAILED = 3
	BIND_IMPORT_PROCEDURE_FAILED = 4
	BIND_IMPORT_MODULE = 5
	BIND_IMPORT_PROCEDURE = 6
	BIND_FORWARDER = 7
	BIND_FORWARDER_NOT = 8
	BIND_IMAGE_MODIFIED = 9
	BIND_EXPAND_FILE_HEADERS = 10
	BIND_IMAGE_COMPLETE = 11
	BIND_MISMATCHED_SYMBOLS = 12
	BIND_SYMBOLS_NOT_UPDATED = 13

	#-------------------------------------------------------------------------
	# Functions
	#-------------------------------------------------------------------------

=begin
	BOOL BindImageEx(
	  __in  DWORD Flags,
	  __in  PSTR ImageName,
	  __in  PSTR DllPath,
	  __in  PSTR SymbolPath,
	  __in  PIMAGEHLP_STATUS_ROUTINE StatusRoutine
	);

	Callback required:
	
	BOOL StatusRoutine(
	  __in  IMAGEHLP_STATUS_REASON Reason,
	  __in  PSTR ImageName,
	  __in  PSTR DllName,
	  __in  ULONG_PTR Va,
	  __in  ULONG_PTR Parameter
	);

	See: http://msdn.microsoft.com/en-us/library/ms679279(VS.85).aspx
=end
	BindImageEx = ::Win32::API.new( 'BindImageEx', 'LPPPK', 'L', 'imagehlp' )

end # imagehlp module
end # Win32 module
end # Pipeline module

# imagehlp.rb
