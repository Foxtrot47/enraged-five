#
# File:: kernel32.rb
# Description:: Windows API kernel32.dll Function Imports / Constants
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 27 June 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
#require 'pipeline/os/path'
require 'Win32API'

module Pipeline
module Win32
module Shell32
		
=begin
	HINSTANCE ShellExecute(      
		HWND hwnd,
		LPCTSTR lpOperation,
		LPCTSTR lpFile,
		LPCTSTR lpParameters,
		LPCTSTR lpDirectory,
		INT nShowCmd
	);

	See: http://msdn.microsoft.com/en-us/library/bb762153%28VS.85%29.aspx
=end
	ShellExecute = Win32API.new( 'shell32', 'ShellExecute', ['P','P','P','P','P','I'], 'I' )
	
end # Shell32 module
end # Win32 module
end # Pipeline module

# pipeline/win32/shell32.rb
