#
# File:: pipeline/win32/envvar.rb
# Description:: 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 3 April 2009
#
# References:
#   http://stackoverflow.com/questions/912982/use-ruby-to-permanently-ie-in-the-registry-set-environment-variables
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require "win32/registry"
require 'Win32API'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Win32

	REGISTRY_KEY_SYSTEM_ENVIRONMENT = 'SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment'
	REGISTRY_KEY_USER_ENVIRONMENT = 'Environment'
	
	#
	# == Description
	# Read environment strings.
	#
	def Win32::get_envvar( varname, user = true )
		
		if ( user ) then
			key = ::Win32::Registry::HKEY_CURRENT_USER::create( REGISTRY_KEY_USER_ENVIRONMENT )
		else
			key = ::Win32::Registry::HKEY_LOCAL_MACHINE::create( REGISTRY_KEY_SYSTEM_ENVIRONMENT ) 
		end
		
		key.each_value do |subkey, type, data|
	
			if ( ( 0 == subkey.casecmp(varname) ) and 
				 ( ( ::Win32::Registry::REG_SZ == type ) or ( ::Win32::Registry::REG_EXPAND_SZ == type ) ) )
				return data.to_s
			end
		end

		return nil
	end
	
	#
	# == Description
	# Set a system environment variable (will be maintained across reboots).
	# Requires the setx.exe executable in the configuration toolsbin/bin directory.
	#
	def Win32::set_envvar( varname, varvalue, user = true, type = ::Win32::Registry::REG_SZ )
		throw ArgumentError.new( "Invalid variable name (#{varname.class})." ) \
			unless ( varname.is_a?( String ) )

		if ( user ) then
			key = ::Win32::Registry::HKEY_CURRENT_USER::create( REGISTRY_KEY_USER_ENVIRONMENT, ::Win32::Registry::KEY_WRITE )
		else
			key = ::Win32::Registry::HKEY_LOCAL_MACHINE::create( REGISTRY_KEY_SYSTEM_ENVIRONMENT, ::Win32::Registry::KEY_WRITE )
		end
		
		ENV[varname] = varvalue.to_s
		key[varname, type] = varvalue
		
		result = 0
		SendMessageTimeout.call( HWND_BROADCAST, WM_SETTINGCHANGE, 0, 'Environment', SMTO_ABORTIFHUNG, 5000, result )
		
		true
	end
	
	#
	# == Description
	# Deletes an environment variable, through the registry
	#
	def Win32::delete_envvar( varname, user = true )	
		throw ArgumentError.new( "Invalid variable name (#{varname.class})." ) \
			unless ( varname.is_a?( String ) )
	
		system( "REG DELETE HKCU\\Environment /v #{varname} /f" ) if user
		system( "REG DELETE HKLM\\SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment /v #{varname} /f" ) unless user
	end

	#------------------------------------------------------------------------
	# Private
	#------------------------------------------------------------------------
private
	SendMessageTimeout = Win32API.new('user32', 'SendMessageTimeout', 'LLLPLLP', 'L') 
    HWND_BROADCAST = 0xffff
    WM_SETTINGCHANGE = 0x001A
    SMTO_ABORTIFHUNG = 2
    	
	
end # Win32 module
end # Pipeline module

# pipeline/win32/envvar.rb
