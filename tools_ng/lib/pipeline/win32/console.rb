#
# File:: pipeline/win32/console.rb
# Description:: Win32 colour console support.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 19 November 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
#require 'win32console'
#require 'Win32/Console/ANSI' if ( PLATFORM =~ /win32/ )
#require 'term/ansicolor'
#include Term::ANSIColor
#include ::Win32::Console::ANSI

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Win32
	
	#
	# == Description
	# Simple wrapper around Windows Console terminal output.
	#
	class Console
		
		# Print a string as an error (red).
		def Console::error( str )
			#$stdout.write red, bold, str, reset, "\n"
			$stdout.write str
		end
		
		# Print a string as a warning (yellow).
		def Console::warn( str )
			#$stdout.write dark, yellow, bold, str, reset, "\n"
			$stdout.write str
		end
		
		# Print a debug string (blue).
		def Console::debug( str )
			#$stdout.write dark, blue, str, reset, "\n"
			$stdout.write str
		end
		
		# Print an "ok" string (green)."
		def Console::ok( str )
			#$stdout.write green, str, reset, "\n"
			$stdout.write str
		end
	end
	
end # Win32 module
end # Pipeline module

# pipeline/win32/console.rb
