#
# File:: pipeline/resourcing/path.rb
# Description:: Resourcing file path conversions
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 20 June 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/targets'
require 'pipeline/os/path'

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
module Pipeline
module Resourcing

	RPF_EXTENSIONS = [ 'rpf', 'iad', 'idd', 'ibd', 'ibn', 'idd', 'idr', 'ift', 'ihm', 'ibs', 'isd', 'ist', 'itd', 'ipt', 'icd', 'cutxml' ]
	IMG_EXTENSIONS = [ 'img' ]
	REGEXP_INDEPENDENT = /[i]([A-Za-z0-9]+)/i
	
	#
	# Function that determines whether this is a core-asset stored in a ZIP
	# archive; e.g. .idr.zip, .ift.zip, .idd.zip.
	#
	def Resourcing::is_core_asset_in_zip?( filename )
	
		result = false
		ext = OS::Path::get_extension( filename )
		case ext
		when 'zip'
			# See if the ZIP extension is a container type for another core-asset type.
			secext = OS::Path::get_extension( OS::Path::remove_extension( filename ) )
			if ( secext =~ REGEXP_INDEPENDENT ) then
				result = true
			end
		end
		result
	end
	
	#
	# Function to convert an platform-independent file extension to a platform-
	# specific file extension.
	#
	# If additional converters are added (i.e. we use more than RAGE) then
	# we might need to create additional cases in this function.
	#    
	def Resourcing::convert_independent_extension_to_platform( filename, ext, target )
		throw ArgumentError.new( "Invalid target specified (#{target.class})." ) \
			unless ( target.is_a?( Pipeline::TargetBase ) )
		
		case ext
		when 'rpf'
			newext = 'rpf'
		when 'zip'
			# See if the ZIP extension is a container type for another core-asset type.
			secext = OS::Path::get_extension( OS::Path::remove_extension( filename ) )
			if ( secext =~ REGEXP_INDEPENDENT ) then
				# ZIP being used as a container for another asset type.
				case target.platform
				when 'independent'
					newext = "i#{$1}"
				when 'xbox360'
					newext = "x#{$1}"
				when 'ps3'
					newext = "c#{$1}"
				when 'win32'
					newext = "w#{$1}"
				when 'win64'
					newext = "y#{$1}"
				else
					raise Exception.new( "#{target.platform} not found" )
				end #target.platform
			else
				# ZIP -> RPF Conversion
				newext = 'rpf'
			end
		when 'ide'
			newext = 'ide'
		when 'meta'
			case target.platform
			when 'independent'
				newext = 'meta'
			when 'xbox360'
				newext = 'xmt'
			when 'ps3'
				newext = 'cmt'
			when 'win32'
				newext = 'wmt'
			when 'win64'
				newext = 'ymt'
			else
				raise Exception.new( "#{target.platform} not found" )
			end
		when 'sco'
			case target.platform
			when 'independent'
				newext = 'sco'
			when 'xbox360'
				newext = 'xsc'
			when 'ps3'
				newext = 'csc'
			when 'win32'
				newext = 'wsc'
			when 'win64'
				newext = 'ysc'
			else
				raise Exception.new( "#{target.platform} not found" )
			end
        when 'cutxml'
            newext = 'cut'
		when REGEXP_INDEPENDENT
			# 'i' prefix.  Change to 'x' for 360, 'c' for PS3.
			case target.platform
			when 'independent'
				newext = "i#{$1}"
			when 'xbox360'
				newext = "x#{$1}"
			when 'ps3'
				newext = "c#{$1}"
			when 'win32'
				newext = "w#{$1}"
			when 'win64'
				newext = "y#{$1}"
			else
				raise Exception.new( "#{target.platform} not found" )
			end #target.platform
		else
			newext = ext
		end
		newext
	end
	
	# 
	# Function to convert an platform-independent filename to a 
	# platform-dependent filename.
	#
	def Resourcing::convert_independent_filename_to_platform( filename, target )
		branch = target.branch
		
		oldext = OS::Path.get_extension( filename )
		newext = convert_independent_extension_to_platform( filename, oldext, target )
		target_filename = "#{OS::Path::remove_extension( OS::Path::remove_extension( filename ) )}.#{newext}"
		if ( branch.is_export_file?( filename ) ) then
			target_filename.sub!( branch.export, target.target )
		elsif ( branch.is_processed_file?( filename ) ) then
			target_filename.sub!( branch.processed, target.target )
		end        
		target_filename
	end

end # Resourcing module
end # Pipeline module

# pipeline/resourcing/path.rb
