#
# File:: %RS_TOOLSLIB%/pipeline/resourcing/gateways/zips_from_dir_leaves.rb
# Description:: Gateway for MB RAGE exporters to the content system
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 28 February 2012
#
#

require 'pipeline/projectutil/data_zip'
require 'pipeline/os/getopt'

include Pipeline


#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'show usage information' ],
	[ '--srcdir', '-p', OS::Getopt::REQUIRED, 'source directory' ],
	[ '--dstdir', '-o', OS::Getopt::REQUIRED, 'output directory' ],	
	[ '--ext','-e', OS::Getopt::REQUIRED, 'zip extension' ],
	[ '--rebuild', '-r', OS::Getopt::BOOLEAN, 'do a full rebuild?' ],
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

# Return accurate modified time for a directory.
def get_directory_mtime( directory, files )

  #files = OS::FindEx.find_files( OS::Path::combine( directory, "*.*" ) )

  files_most_recent = File::mtime( directory )
  filesmtime = files.collect do |filename| File::mtime( filename ); end
  
  filesmtime.each do |mtime|
    files_most_recent = mtime if ( ( mtime <=> files_most_recent ) > 0 )
  end
  
  files_most_recent
end

begin
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	g_SrcDir = ""
	g_OutputDir = ""
	g_ZipExtension = ""
	
	#-------------------------------------------------------------------------
	# Parse Command Line
	#-------------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	
	g_SrcDir = ( nil == opts['srcdir'] ) ? '' : opts['srcdir']
	g_OutputDir = ( nil == opts['dstdir'] ) ? '' : opts['dstdir']
	g_ZipExtension = ( nil == opts['ext'] ) ? 'zip' : opts['ext']
	g_ZipExtension = ( nil == opts['ext'] ) ? 'zip' : opts['ext']
	g_Rebuild = ( nil == opts['rebuild'] ) ? false : opts['rebuild']

	dirs = Pipeline::OS::FindEx.find_dirs_recurse( g_SrcDir, false )
	src_dir_tokens = g_SrcDir.split('/')
	
	dirs.each do | dir |
		dir_tokens = dir.split('/')
		dict_name = ''

		( ( src_dir_tokens.length - 1 )..( dir_tokens.length - 1 ) ).each do | i |
			dict_name += dir_tokens[i]
		end		
		next if dict_name == ''  
		
		files_for_zip =  OS::FindEx.find_files( dir + "/*.clip" )
		files_for_zip =  files_for_zip + OS::FindEx.find_files( dir + "/*.anim" )
		next if files_for_zip.count == 0
		
		outputfile = OS::Path.combine( g_OutputDir, dict_name + "." + g_ZipExtension )
		needs_zipped = false
		if not File::exist?( outputfile ) or g_Rebuild then
			needs_zipped = true 
		else
      dirmtime = get_directory_mtime( dir, files_for_zip )
      if ( ( dirmtime <=> File::mtime( outputfile ) ) > 0 )
				puts "#{outputfile} marked for creation"
				needs_zipped = true
			end
			#files_for_zip.each do | file |
			#	if ( (File::mtime( file) <=> File::mtime( outputfile )) > 0 )
			#		puts "#{outputfile} marked for creation"
			#		needs_zipped = true
			#		break
			#	end
			#end
		end
		status = ProjectUtil::data_zip_create( outputfile, files_for_zip, false ) if needs_zipped
	end
	
rescue Exception => ex

  puts "EXCEPTION: #{ex.message}"
  puts ex.backtrace().join("\n")
end