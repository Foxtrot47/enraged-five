#
# File:: %RS_TOOLSLIB%/pipeline/resourcing/gateways/all_rpf_from_content.rb
# Description:: Gateway for MB RAGE exporters to the content system
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 17 April 2012
#
#

require 'pipeline/projectutil/data_zip'
require 'pipeline/os/getopt'
require 'pipeline/projectutil/data_convert.rb'

include Pipeline


#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'show usage information' ],
	[ '--src', '-p', OS::Getopt::REQUIRED, 'source directory' ],
	[ '--rebuild', '-r', OS::Getopt::BOOLEAN, 'do a full rebuild?' ],
]

begin
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	g_Rebuild = false
	g_Source= ''

	#-------------------------------------------------------------------------
	# Parse Command Line
	#-------------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )

	g_Rebuild = ( nil == opts['rebuild'] ) ? false : opts['rebuild']
	g_Source = ( nil == opts['src'] ) ? '' : opts['src']
	
	g_Config = Pipeline::Config::instance()
	g_Project = g_Config.project
	g_Project.load_config()
		
	g_Filenames = []
	rootdirs = Pipeline::OS::FindEx.find_dirs( g_Source, false )
	rootdirs.each do | dir |
		# LPXO: Remove trailing slash, yuck, really needs fixing in data_convert.rb
		safe_dir = dir
		if OS::Path.ends_with( dir, '/' ) then
			safe_dir = dir[0..(dir.size-2)]
		end
		g_Filenames << safe_dir
	end
	results = ProjectUtil::data_convert_file(g_Filenames, g_Rebuild)
	exit( results[:success] ? 0 : 1 )
		
end