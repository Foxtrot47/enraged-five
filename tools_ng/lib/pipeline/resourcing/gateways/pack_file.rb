#
# File:: %RS_TOOLSLIB%/pipeline/resourcing/gateways/pack_file.rb
# Description:: Gateway for MB RAGE exporters to the content system
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 4 March 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/os/getopt'
require 'pipeline/config/projects'
require 'pipeline/resourcing/convert'
require 'pipeline/content/content_clip'
require 'pipeline/resourcing/util'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'show usage information' ],
	[ '--project', '-p', OS::Getopt::REQUIRED, 'project name' ],
	[ '--rootsrcdir', '-s', OS::Getopt::REQUIRED, 'root for the current scene' ],
	[ '--animsrcdir', '-s', OS::Getopt::REQUIRED, 'location of the animations for this scene' ],
	[ '--destdir', '-d', OS::Getopt::REQUIRED, 'desired compressed anim location' ],
	[ '--outputdir', '-d', OS::Getopt::REQUIRED, 'desired pack file destination' ],
	[ '--scenename', '-n', OS::Getopt::REQUIRED, 'scene name' ],
	[ '--preview', '-e', OS::Getopt::BOOLEAN, 'is this a preview export?' ],
	[ '--sectioned', '-n', OS::Getopt::BOOLEAN, 'is the data sectioned?' ]
]

begin
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------

	g_AppName = File::basename( __FILE__, '.rb' )
	g_ProjectName = ''
	g_RootSrcDir = ''
	g_AnimSrcDir = ''
	g_DestDir = ''
	g_OutputDir = ''
	g_SceneName = ''
	g_Sectioned = false
	g_Preview = false
	

	#-------------------------------------------------------------------------
	# Parse Command Line
	#-------------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] ) then
		puts OS::Getopt.usage( OPTIONS )
		exit( 1 )
	end
	
	g_ProjectName = ( nil == opts['project'] ) ? '' : opts['project']
	g_RootSrcDir = ( nil == opts['rootsrcdir'] ) ? '' : opts['rootsrcdir']
	g_AnimSrcDir = ( nil == opts['animsrcdir'] ) ? '' : opts['animsrcdir']
	g_DestDir = ( nil == opts['destdir'] ) ? '' : opts['destdir']
	g_OutputDir = ( nil == opts['outputdir'] ) ? '' : opts['outputdir']
	g_SceneName = ( nil == opts['scenename'] ) ? '' : opts['scenename']
	g_Sectioned = ( nil == opts['sectioned'] ) ? false : opts['sectioned']
	g_Preview = ( nil == opts['preview'] ) ? false : opts['preview']
	
	#-------------------------------------------------------------------------
	# Build Dictionary
	#-------------------------------------------------------------------------
	
	g_ProjectName = g_ProjectName.downcase()
	project = Pipeline::Config.instance.projects[g_ProjectName]
	
	project.load_content()
	component_group = Pipeline::Content::Group.new("generic_content_group")
	
	# Always flush the preview folder
	OS::FileUtilsEx::delete_files(project.preview + "/*")
	
	# The output directory is a combination of the standard stream and the sub dir passed into the script
	outputDir = OS::Path::combine( project.netstream, g_OutputDir )
	puts(outputDir)
	FileUtils.mkdir_p outputDir if File.directory?(outputDir) == false
	
	# Initialise the dynamic content generator
	# TODO: Add support for all pack file types if required
	cutfile = OS::Path::combine( g_RootSrcDir )
	cutfile = cutfile + ".cutbin"
	OS::FileUtilsEx.copy_file( cutfile, OS::Path::combine( outputDir, g_SceneName + ".cut" ) )
	
	if true == g_Sectioned then
		# This is a previously used method for section number determination at pack time.  There has to be a camera
		# anim for every section so this has proven to be robust
		camfiles = OS::FindEx.find_files( g_AnimSrcDir + "/*camera*.anim" )
		camfiles.each_index do | i |
			puts camfiles[i]
			clip_content = Pipeline::Content::ClipGroup.new( g_AnimSrcDir, g_DestDir, (g_SceneName + "-#{i}"), project, outputDir, i )
			component_group.add_child(clip_content)
		end
	else
		clip_content = Pipeline::Content::ClipGroup.new( g_AnimSrcDir, g_DestDir, g_SceneName, project, outputDir )
		component_group.add_child(clip_content)
	end
	
	# Initialise the converter, dynamically generate the content node and pass to the converter
	if component_group.children.size > 0
		convert = Resourcing::ConvertSystem.instance()
		convert.setup( project, nil, true, true, false)
		component_group.children.each do | content_group |
			if content_group.methods.include?( 'build' )
				content_group.build()
			end
		end
		convert.build(component_group)
	end	
	
	# Resource to the preview directory
	if true == g_Preview then
		target_content_group = Pipeline::Content::Group.new("patch_group")
		puts project.preview
		Pipeline::Resourcing::create_target_content_from_indepenent( component_group, target_content_group, project, "#{project.preview}" )
		convert.build(target_content_group)
	end
end

# %RS_TOOLSLIB%/pipeline/resourcing/gateways/pack_file.rb
