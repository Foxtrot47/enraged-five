#
# File:: %RS_TOOLSLIB%/pipeline/resourcing/gateways/rpf_from_content.rb
# Description:: Gateway for MB RAGE exporters to the content system
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 24 February 2012
#
#

require 'pipeline/projectutil/data_convert.rb'
require 'pipeline/os/getopt'

include Pipeline


#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'show usage information' ],
	[ '--rebuild', '-r', OS::Getopt::BOOLEAN, 'do a full rebuild?' ],
]
TRAILING = {
	'files' => 'files to convert.'
}

begin
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	g_Rebuild = false
	g_Src = ""
	
	#-------------------------------------------------------------------------
	# Parse Command Line
	#-------------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	
	g_Rebuild = ( nil == opts['rebuild'] ) ? false : opts['rebuild']
	g_Filenames = []
	# Here we ensure we have absolute filenames, relative filenames being
	# expanded based on the current working path.
	trailing.each_with_index do |filename, index|
		filename = File::expand_path( filename )
		if ( File::file?( filename ) ) then
			g_Filenames << OS::Path::normalise( filename )
		elsif ( File::directory?( filename ) ) then
			g_Filenames += OS::FindEx::find_files( OS::Path::combine( filename, '*.*' ) )
		end

		throw IOError.new( "File #{filename} does not exist.  Aborting convert." ) \
			unless ( File::exists?( filename ) )
	end
	

	results = ProjectUtil::data_convert_file(g_Filenames, g_Rebuild)
	exit( results[:success] ? 0 : 1 )
end