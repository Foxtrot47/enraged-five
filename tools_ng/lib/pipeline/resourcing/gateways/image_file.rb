#
# File:: %RS_TOOLSLIB%/pipeline/resourcing/gateways/image_file.rb
# Description:: Gateway for MB RAGE exporters to the content system
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Author:: David Muir <david.muir@rockstarnorth.com>
# Author:: Mike Wilson <mike.wilson@rockstarnorth.com>
# Date:: 9 July 2010
#
# Note: this really creates an RPF file now rather than an image.
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
Pipeline::Config::instance()::logtostdout = true
require 'pipeline/os/getopt'
require 'pipeline/resourcing/convert'
require 'pipeline/projectutil/data_convert'
require 'pipeline/os/path'

include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'show usage information' ],
	[ '--project', '-p', OS::Getopt::REQUIRED, 'project name' ],
	[ '--srcdir', '-s', OS::Getopt::REQUIRED, 'location of pack files to build into RPF' ],
	[ '--imagename', '-d', OS::Getopt::REQUIRED, 'name of RPF' ],
	[ '--exportsrcdir', '-e', OS::Getopt::REQUIRED, 'root directory of source anims' ],
	[ '--rebuild', '-r', OS::Getopt::BOOLEAN, 'do a full rebuild?' ],
	[ '--cutscene', '-c', OS::Getopt::BOOLEAN, 'are we processing cutscene or ingame?' ],
	[ '--all', '-a', OS::Getopt::BOOLEAN, 'Processes all objects instead of only previously existing ones.' ]
]

begin
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	g_ProjectName = ''
	g_SrcDir = ''
	g_ImageName = ''
	g_ExportSrcDir = ''
	g_Rebuild = false
	g_CutScene = false
	
	#-------------------------------------------------------------------------
	# Parse Command Line
	#-------------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	
	g_ProjectName = ( nil == opts['project'] ) ? '' : opts['project']
	g_SrcDir = ( nil == opts['srcdir'] ) ? '' : opts['srcdir']
	g_ImageName = ( nil == opts['imagename'] ) ? '' : opts['imagename']
	g_ExportDir = ( nil == opts['exportsrcdir'] ) ? '' : opts['exportsrcdir']
	g_Rebuild = ( nil == opts['rebuild'] ) ? false : opts['rebuild']
	g_CutScene = ( nil == opts['cutscene'] ) ? false : opts['cutscene']
	g_ProcessAll = ( nil == opts['all'] ) ? false : opts['all']

	puts g_SrcDir
	puts g_ExportDir
	#-------------------------------------------------------------------------
	# Build Image
	#-------------------------------------------------------------------------
	g_ProjectName = g_ProjectName.downcase()
	project = Pipeline::Config.instance.projects[g_ProjectName]
	#project.load_content(nil, Pipeline::Project::ContentType::OUTPUT | Pipeline::Project::ContentType::PLATFORM)
	project.load_content()
	
	
	ind_output_node = nil
	plat_output_list = []
	
	# Find the content nodes that corresponds to the image we want
	img_output_list = project.content.find_by_script("content.name == '" + g_ImageName + "' and content.xml_type == 'zip'" )
	
	# Loop though splitting the independent from platofrm specific content nodes
	img_output_list.each do | img_output_item |
		if img_output_item.target == project.ind_target
			ind_output_node = img_output_item
		#else
		#	plat_output_list << img_output_item
		end
	end
	
	srcdir = OS::Path::combine( project.netstream, g_SrcDir )
	
	component_group = Pipeline::Content::Group.new("anim_pack_group")
	export_dir_tokens = g_ExportDir.split('\\')
		
	# If this is a rebuild, rebuild all of the independent pack files from the source animations
	if true == g_Rebuild then
		
		OS::FileUtilsEx::delete_files(srcdir + "/*.*")
		puts "***********"
		puts srcdir
		puts "***********"
	end
	
	if g_CutScene == true
		# If its a cutscene just get the dirs, only recursive if its ingame animation
		dirs = Pipeline::OS::FindEx.find_dirs( g_ExportDir, false )
		
		dirs.each do | dir |
			dir_tokens = dir.split('/')
			
			dict_name = ''
			
			( export_dir_tokens.length..( dir_tokens.length - 1 ) ).each do | i |
				
				dict_name += dir_tokens[i]
			end

			src = (g_ExportDir + "/" + dict_name + ".cutbin")
			dst = (srcdir + "/" + dict_name + ".cut")
			
			out_of_date = false
			if g_ProcessAll == true then
				if File.exists?(src) == true and File.exists?(dst) == true then
				
					out_of_date = ( ( File::mtime( src ) <=> File::mtime( dst ) ) > 0 )
		
					if out_of_date == false then
						
						@config = Pipeline::Config::instance()
						@p4 = @config.scm()
						@p4.connect() unless @p4.connected?
						
						dst_modtime = File::mtime(dst) 
						
						month = dst_modtime.month.to_s
						month = "0" + month if dst_modtime.month < 10

						day = dst_modtime.day.to_s
						day = "0" + day if dst_modtime.day < 10
						
						hour = dst_modtime.hour.to_s
						hour = "0" + hour if dst_modtime.hour < 10 
						
						minute = dst_modtime.min.to_s
						minute = "0" + minute if dst_modtime.min < 10
						
						second = dst_modtime.sec.to_s
						second = "0" + second if dst_modtime.sec < 10
						
						#print "Acquiring changes in #{g_ExportDir}/#{dict_name}\n"
						change_records = @p4.run("changes", "#{g_ExportDir}/#{dict_name}...@#{dst_modtime.year}/#{month}/#{day}:#{hour}:#{minute}:#{second},@now")
						if change_records.size > 0 then 
						
							#A change has occurred since the last mod time.
							print "A modification in Perforce has triggered a new build.\n"
							out_of_date = true
						end
					end
				else
					#Doesn't exist; process it.
					out_of_date = true
				end
			else
				#Only process previously existing files.
				if File.exists?(src) == true and File.exists?(dst) == true then
					out_of_date = ( ( File::mtime( src ) <=> File::mtime( dst ) ) > 0 )
				else
					#Doesn't exist, but we're not processing all data so ignore it.
					out_of_date = false 
				end				
			end
			
			if ( g_Rebuild == true or out_of_date == true )
			
				print "#{dict_name} is out of date.\n"
							
				# Set a tempdir as we want to process the obj data not the unprocessed data
				tempDir = dir + "obj/"
				
				# This is ripped from pack_file.rb to section the clips
				camfiles = OS::FindEx.find_files( tempDir + "/*camera*.anim" )
				
				if camfiles == nil or camfiles.size == 0 then
					print "ERROR: No animation files match the pattern \"*camera*.anim\".  This file will not be added to the RPF.\n"
				else
					# copy the cut file from the export location to the stream dir so it can be scooped up
					OS::FileUtilsEx.copy_file(src, dst)
					
					camfiles.each_index do | i |
						puts camfiles[i]
						clip_content = Pipeline::Content::ClipGroup.new( tempDir, tempDir, (dict_name + "-#{i}"), project, srcdir, i )
						component_group.add_child(clip_content)
					end			
				end				
			end
		end
	
	else #Animation Pipeline
		
		if ( g_Rebuild == true ) then
			# If its a cutscene just get the dirs, only recursive if its ingame animation
			dirs = Pipeline::OS::FindEx.find_dirs_recurse( g_ExportDir, false )
			
			dirs.each do | dir |
				dir_tokens = dir.split('/')
				
				dict_name = ''
				
				( export_dir_tokens.length..( dir_tokens.length - 1 ) ).each do | i |
					
					dict_name += dir_tokens[i]
				end
				
				clip_content = Pipeline::Content::ClipGroup.new( dir, dir, dict_name, project, srcdir )
				component_group.add_child(clip_content)
			end
		end
	end


	# Build pack files from the content group
	if component_group.children.size > 0
		convert = Resourcing::ConvertSystem.instance()
		convert.setup( project, nil, true, true, false)
		component_group.children.each do | content_group |
			if content_group.methods.include?( 'build' )
				content_group.build()
			end
		end
		convert.build(component_group)
	end
	
	# Add all the  pack files found in the stream to the image	
	pack_file_list = Pipeline::OS::FindEx.find_files(srcdir + "/*.i*", false)
	pack_file_list = pack_file_list + Pipeline::OS::FindEx.find_files(srcdir + "/*.cut", false)
	
	
	pack_file_list.each do | pack_file |
		ind_output_node.add_input(Pipeline::Content::Zip::new( Pipeline::OS::Path.get_basename( pack_file ), Pipeline::OS::Path.get_directory( pack_file ), Pipeline::OS::Path.get_extension( pack_file ), project.ind_target, false ))
	end
	
	# Initialise the converter and pass it the image content nodes for image generation and resourcing
	convert = Resourcing::ConvertSystem.instance()
	convert.setup( project, nil, false, g_Rebuild, false)
	convert.build(ind_output_node)
	status = ProjectUtil::data_convert_file(ind_output_node.filename, g_Rebuild)
	exit( status[:success] ? 0 : 1 )
	
	#plat_output_list.each do | map_output_item |
	#	convert.build(map_output_item)	
	#end

end

# %RS_TOOLSLIB%/pipeline/resourcing/gateways/image_file.rb
