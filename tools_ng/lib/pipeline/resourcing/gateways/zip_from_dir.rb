#
# File:: %RS_TOOLSLIB%/pipeline/resourcing/gateways/zip_from_rpf.rb
# Description:: Gateway for MB RAGE exporters to the content system
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 28 February 2012
#
#

require 'pipeline/projectutil/data_zip'
require 'pipeline/os/getopt'

include Pipeline


#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'show usage information' ],
	[ '--src', '-p', OS::Getopt::REQUIRED, 'source directory' ],
	[ '--outputfile', '-o', OS::Getopt::REQUIRED, 'source directory' ],
]

begin
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	g_OutputFile = ""
	g_SrcDir = ""
	
	#-------------------------------------------------------------------------
	# Parse Command Line
	#-------------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	
	g_SrcDir = ( nil == opts['src'] ) ? '' : opts['src']
	g_OutputFile = ( nil == opts['outputfile'] ) ? '' : opts['outputfile']
	

	files_for_zip = OS::FindEx.find_files( g_SrcDir + "/*.*" )
	success = ProjectUtil::data_zip_create( g_OutputFile, files_for_zip, false )
	exit( success ? 0 : 1 )
end