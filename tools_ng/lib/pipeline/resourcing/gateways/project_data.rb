#
# File:: project_data.rb
# Description:: Gateway for MB RAGE exporters to access basic project data to
#				populate available configs
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 21 July 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/os/getopt'
require 'pipeline/config/projects'

include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'show usage information' ],
	[ '--outputfile', '-o', OS::Getopt::REQUIRED, 'location to dump project data' ]
]

begin
#-------------------------------------------------------------------------
# Entry-Point
#-------------------------------------------------------------------------
	g_OutputFile = ''
	
	
#-------------------------------------------------------------------------
# Parse Command Line
#-------------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	
	g_OutputFile = ( nil == opts['outputfile'] ) ? '' : opts['outputfile']
	
	

#-------------------------------------------------------------------------
# Dump project data
#-------------------------------------------------------------------------	
	File.delete( filename ) if File.exist?( filename )

	doc = REXML::Document.new()
	root_node = doc.add_element( "projects" )
	
	env = Pipeline::Environment.new()
	project.fill_env( env, false )
	
	Pipeline::Config.instance.projects.each_value do | project |
		proj_node = root_node.add_element( project.name )
		proj_node.add_attribute( "uiname",project.uiname )
		proj_node.add_attribute( "root",project.root )
		config = env.subst( proj.config )
	end
	
	File.open( filename,"w+" ) { |file|
		fmt = REXML::Formatters::Pretty.new()
		fmt.write(doc,file)
	}
end