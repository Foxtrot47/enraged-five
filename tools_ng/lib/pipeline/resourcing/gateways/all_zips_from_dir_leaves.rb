#
# File:: %RS_TOOLSLIB%/pipeline/resourcing/gateways/all_zips_from_dir_leaves.rb
# Description:: Gateway for MB RAGE exporters to the content system
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 17 April 2012
#
#

require 'pipeline/projectutil/data_zip'
require 'pipeline/os/getopt'

include Pipeline


#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'show usage information' ],
	[ '--src', '-p', OS::Getopt::REQUIRED, 'source directory' ],
	[ '--dst', '-o', OS::Getopt::REQUIRED, 'output directory' ],	
	[ '--ext','-e', OS::Getopt::REQUIRED, 'zip extension' ],
	[ '--rebuild', '-r', OS::Getopt::BOOLEAN, 'do a full rebuild?' ],
]

begin
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	g_ZipExtension = ""

	#-------------------------------------------------------------------------
	# Parse Command Line
	#-------------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )

	g_ZipExtension = ( nil == opts['ext'] ) ? 'zip' : opts['ext']
	g_SrcDir = ( nil == opts['src'] ) ? '' : opts['src']
	g_OutputDir = ( nil == opts['dst'] ) ? '' : opts['dst']
	g_Rebuild = ( nil == opts['rebuild'] ) ? false : opts['rebuild']
	
	g_Config = Pipeline::Config::instance()
	g_Project = g_Config.project
	g_Project.load_config()

	rootdirs = Pipeline::OS::FindEx.find_dirs( g_SrcDir, false )
	rootdirs.each do | dir |
		next if dir == g_SrcDir
		root_dir_name = OS::Path::get_trailing_directory( dir )
		full_dst_dir = OS::Path.combine( g_OutputDir, root_dir_name )
		
		commandline = "$(toolsbin)/ruby/bin/ruby.exe $(toolslib)/pipeline/resourcing/gateways/zips_from_dir_leaves.rb --srcdir=#{dir} --dstdir=#{full_dst_dir} --ext=#{g_ZipExtension}"
		commandline = commandline + " --rebuild" if g_Rebuild
		Pipeline::Globals.instance().in_env do |e|
			commandline = e.subst(commandline)
		end
		## puts "START: #{commandline}"
		## status, out, err = OS::start( commandline )
    system( commandline )

		##if status.exitstatus != 0 then
		##	puts "Zip packing for #{dir} failed."
		##end	
	end
	
rescue Exception => ex

  puts "EXCEPTION: #{ex.message}"
  puts ex.backtrace().join("\n")
end