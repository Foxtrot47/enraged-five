#
# File:: %RS_TOOLSLIB%/pipeline/resourcing/converters/converter_generic_packets.rb
# Description:: XGE Generic Converter packet classes (based off converter_rage_packets)
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 28 November 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/os/path'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Resourcing
module Converters
	
	#
	# == Description
	# This class represents a data conversion packet list which is a 
	# collection of ruby script "packets" that are sent to XGE
	# for processing.
	#
	class GenericPacketList
		attr_reader :packets
		
		# Constructor.
		def initialize( xge_folder, class_name, static_func, packet_per_item = false, maxsize = 0 )			
			@xge_folder = xge_folder
			@maxsize = maxsize
			@class_name = class_name
			@static_func = static_func
			@packet_per_item = packet_per_item
			@packets = []
		end
		
		# Create new ConversionPacket.
		def create_packet( )
			GenericPacket.new( OS::Path.combine( @xge_folder, "generic_packet#{@packets.size}.rb" ) )
		end
		
		# Add a target directory to the last ConversionPacket object.
		def add_item( item )
			# Create a new packet.
			if ( @packets.empty? ) or ( packets.last.data_size >= @maxsize ) or ( @packet_per_item ) then
				@packets << create_packet( )
			end
			@packets.last.add_item( item )
			
		end
		
		# Create the packet ruby scripts.
		def create( )
			c = Pipeline::Config::instance( )
			OS::FileUtilsEx::delete_files( OS::Path::combine( @xge_folder, '*.rb' ) )
			
			@packets.each do |packet|
				File::open( packet.filename, "w" ) do |f|
					#f.puts("require \"#{OS::Path::combine( c.toolslib, 'pipeline', 'config', 'globals')}\"" )
					f.puts("require \"#{OS::Path::combine( c.toolslib, 'pipeline', 'config', 'projects')}\"" )
					f.puts( "puts \"SETTING SKIP CHECK\"")
					f.puts( "Pipeline::Globals::instance().skip_version_check = true" )
					f.puts( "puts Pipeline::Globals::instance()")
					f.puts( "require \"#{OS::Path::combine( c.toolslib, 'pipeline', 'resourcing', 'converters', 'converter_generic_includes' )}\"" )
					packet.items.each do |item|
						f.puts( "Pipeline::Resourcing::Converters::#{@class_name}.#{@static_func.to_s}( \"#{item.dir}\", #{item.file_list.inspect} )" )
					end
				end
			end
		end
	end

	#
	# == Description
	# This class represents a single data conversion packet.  A list of these
	# are contained in the GenericPacketList class.
	#
	#
	class GenericPacket
		attr_reader :items
		attr_reader :data_size
		attr_reader :filename

		def initialize( filename )
			@items = []
			@data_size = 0
			@filename = filename
			FileUtils::mkdir_p( OS::Path::remove_filename( filename ) )
		end

		def add_item( item )
			item.file_list.each do | file |
				@data_size += File::size( file )
			end
			@items << item
		end
	end

	#
	# == Description
	# Utility class to store a generic "item" to be processed
	#
	class GenericItem		
		attr_reader :dir
		attr_reader :file_list
				
		def initialize( dir )		
			@dir = dir
		end
		
		def set_files_from_dir( wildcard = "*.*" )
			@file_list = OS::FindEx::find_files( OS::Path.combine( @dir, wildcard ) )
		end
		
		def set_files( files )
			@file_list = files
		end
	end

end # Converters module
end # Resourcing module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/resourcing/converters/convert_generic_packets.rb
