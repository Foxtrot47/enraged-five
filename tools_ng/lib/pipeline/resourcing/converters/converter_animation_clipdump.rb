#
# File:: %RS_TOOLSLIB%/pipeline/resourcing/converters/converter_animation_coalesce.rb
# Description:: Clip processor: Dumps out clips to ascii, parses them and creates clip lists to group compression types
#
# Author:: Luke Openshaw
# Date:: 22nd November 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
#require 'pipeline/config/projects'
require 'pipeline/content/content_core'
require 'pipeline/os/file'
require 'pipeline/os/path'
require 'pipeline/os/start'
require 'pipeline/projectutil/data_zip'
require 'pipeline/resourcing/converter'
require 'pipeline/resourcing/converters/converter_generic_packets'
require 'pipeline/fileformats/clipascii'
include Pipeline
include Pipeline::Resourcing

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Resourcing
module Converters
	
	#
	# == Description
	# Animation Coalescing processor.
	#
	class AnimationConverterClipDump < ChildConverterBase
		attr_reader :items
		attr_reader :use_xge
		attr_reader :conf

		def initialize( project, branch )
			super( project, branch )

			@command_list = {}
			@use_xge = ( Pipeline::Config::instance().use_xge and Incredibuild.is_installed?() )
			@conf = Pipeline::Config::instance()
			@items = []
		end

		# Build the content.
		def build( content_node_list, input_lists, &block )
			result = true
			prebuild_internal( content_node_list, input_lists )
			begin
				if @use_xge then
					result = build_xge( &block )
				else
					result = build_local( &block )
				end
			rescue BuildError => ex
				result = false
				throw
			end
			@items = []
			result
		end
		
		def AnimationConverterClipDump::processor( dir, file_list )
			clipgroup_exe = CLIP_GROUP_EXE
			Pipeline::Globals.instance().in_env do |e|
				clipgroup_exe = e.subst(clipgroup_exe)
			end
			
			clip_files_by_compression = {}
			file_list.each do | zip_file |

				clip_group_command = clipgroup_exe + " -zip #{zip_file} -outdir #{dir}"
				puts clip_group_command
				status, out, err = OS::start( clip_group_command )
				puts out
				if ( status.exitstatus != 0 ) then
					puts "ERROR: Failed to group #{zip_file}"
					exit( status.exitstatus )
				end
			end
		end
		
	#--------------------------------------------------------------------
	# Private
	#--------------------------------------------------------------------
	private
		CLIP_GROUP_EXE = '$(toolsbin)/anim/ClipGroup.exe'
		ANIM_COMBINE_EXE = '$(toolsbin)/anim/animcombine.exe'
		COMPRESSION_PROPERTY_NAME = 'Compressionfile_DO_NOT_RESOURCE'
		SKELETON_PROPERTY_NAME = 'Skelfile_DO_NOT_RESOURCE'
		ADDITIVE_PROPERTY_NAME = 'Additive_DO_NOT_RESOURCE'

		def build_local( )
			status = 0
			@command_list.each_key do | key |
				status, out, err = OS::start( @items[key] )
				if ( status.exitstatus == 0 ) then
					AnimationConverter::log().info( "Clip dump of clips for #{key} succeded." )
				else
					AnimationConverter::log().error( "Clip dump of clips for #{key} failed." )
				end
			end
			status.exitstatus == 0
		end

		def build_xge()
			result = true
			
			if ( 0 == @items.size ) then

				AnimationConverter::log().info( "Nothing to convert." )
			else
				AnimationConverter::log().info( "Creating XGE animation processing packets." )
				
				xge_folder = XGE::get_temp_dir( @project, @branch.name )
				xge_packet_folder = OS::Path::combine( xge_folder, 'anim_dump' )
				filename = OS::Path.combine( xge_packet_folder, 'anim_dump.xml' )
				@log_filename = OS::Path.combine( xge_packet_folder, 'anim_dump.log' )		
				
				xge_packets = GenericPacketList.new( xge_packet_folder, "AnimationConverterClipDump", :processor, false, 1024 )
				@items.each do |item|
					xge_packets.add_item( item )
				end				
				xge_packets.create( )
				
				AnimationConverter::log().info( "Creating XGE task" )
								
				# Create environment and tools
				options = ""
				tool = XGE::Tool.ruby( "anim_grouping", "")
				
				xge_env = XGE::Environment.new( "env_anim_clipdump" )
				xge_env.add_tool( tool )

				# Construct XGE Project
				xge_project = XGE::Project.new( "#{@project.uiname} Animation Grouping" )
				xge_taskgroup =  XGE::TaskGroup.new( "Animation Grouping", [], xge_env, xge_env.tools[0] )

				# Construct XGE TaskGroup
				xge_packets.packets.each do | packet |
					task_name = "anim_grouping"
					task = XGE::Task.new( task_name, packet.filename )
					task.caption = "#{task_name}"
					task.parameters = packet.filename

					xge_taskgroup.tasks << task
				end

				xge_project.add_task( xge_taskgroup )
				xge_project.write( filename )

				AnimationConverter::log().info( "Starting XGE animation grouping process." )
				if ( not XGE::start( filename, @log_filename ) ) then				
					AnimationConverter::log().error( "Animation Grouping failed.  Check Build Monitor for errors." )

					result = false
					if ( not @conf.user.username.downcase.include?( User::ASSETBUILDER_USER) )
						error_msg = "XGE Animation Grouping  failed.  Check #{@log_filename} for errors."						
						GUI::MessageBox::error("XGE Error Notification", error_msg)										
					end
				end
			end
			result
		end
		
		def prebuild_internal( content_node_list, input_lists )
			result = true
			
			content_node_list.each do | content_node |
				
				cache_dir = AnimationConverter::cache_dir( @branch, content_node )
				# flush the dicitonaries cache so we don't end up with mismatches of coalesced animations
				cache_dirs = OS::FindEx::find_dirs( cache_dir + "/" )
				cache_dirs.each do | dir |
					::FileUtils::rm_r( dir )
				end
				OS::FileUtilsEx::delete_files( OS::Path::combine( cache_dir, '*.*' ) )
				input_lists[content_node].each do | clip_node |
					clip_basename = OS::Path.get_basename(clip_node.filename)
					item = GenericItem.new( OS::Path.combine( cache_dir, OS::Path::remove_extension( clip_basename) ) )
					item.set_files( [clip_node.filename] )
					@items << item
				end
			end
			result
		end
	end
end # Pipeline
end # Resourcing
end # Converters