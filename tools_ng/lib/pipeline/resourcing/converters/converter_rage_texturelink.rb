#
# File:: %RS_TOOLSLIB%/pipeline/resourcing/converters/converter_rage_texturelink.rb
# Description:: Class that understands the data contained in tcl, tcs and tcp files
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'rexml/document'
include REXML

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Resourcing
module Converters
	
	#
	# == Description
	# The TextureLink class understands the data contained in tcl, tcs and tcp files
	#
	class TextureLink
		
		def initialize( )
		end
		
		def TextureLink::get_dds_inputs_from_tcl_data( env, tcl_data )
			
			dds_inputs = []
						
			tcl_document = Document::new( tcl_data )
			parent_element = tcl_document.elements['/rage__CTextureConversionSpecification/parent']
			if( parent_element != nil ) then
				tcs_pathname = parent_element.text.sub("${RS_ASSETS}", "$(assets)")#workaround for the TCS path oddness
				tcs_pathname = env.subst( tcs_pathname )
				if( !tcs_pathname.end_with?(".tcs") ) then
					tcs_pathname = tcs_pathname + ".tcs"
				end
				File::open( tcs_pathname ) do |tcs_file_stream|
					tcs_document = Document::new( tcs_file_stream )
					tcs_document.elements.each('/rage__CTextureConversionSpecification/resourceTextures/Item')  do |resource_texture_element|
						dds_pathname_element = resource_texture_element.elements['pathname']
						dds_inputs << dds_pathname_element.text
					end
				end
			end
			
			dds_inputs
		end
	end
end # Converters module
end # Resourcing module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/resourcing/converters/converter_rage_texturelink.rb
