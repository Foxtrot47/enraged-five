#
# File:: %RS_TOOLSLIB%/pipeline/resourcing/converters/converter_character.rb
# Description:: Character data converter definition.
#
# Author::Adam Munson
# Date:: 16/03/2012
#
# Content convert specifics for character assets.  This class should not be 
# used directly - use the ConvertSystem class instead.
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/content/content_core'
require 'pipeline/content/content_character'
require 'pipeline/projectutil/data_zip'
require 'pipeline/resourcing/converter'
require 'fileutils'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Resourcing
module Converters
	
	#
	# == Description
	# Map data root processor.
	#
	class CharacterConverter < ConverterBase
		include HasConverterChildren 
        				
		def initialize( project, branch )
			super( project, branch )
            @characters = []
		end
		
		# Return whether this converter can convert the specified content node.
		def can_convert?( content )
			( content.is_a?( Content::ProcessedCharacterDirectory ) )
		end
		
        # Converter prebuild step.
		def prebuild( )
		end
		
		# 
        # Build the character zip(s).
		#
		def build( &block )
			CharacterConverter::log().info( "Character converter building #{@characters.size} character zips." )
			success = true  
            
            Dir::chdir( @project.root ) do
				p4 = SCM::Perforce::new( )
				begin					
					p4.connect( )				
					CharacterConverter::log().info( "Syncing character metadata" )
					p4.run_sync( OS::Path::combine( @project.metadata,'characters', '...' ) )	
				rescue Exception => ex
					CharacterConverter::log().exception( ex, 'Metadata sync exception.' )
				ensure
					p4.disconnect( )
				end
			end
            
            @characters.each do |character| 
               
                if ( ConvertSystem::instance().rebuild ) then                
                    # If we're rebuilding then we nuke the output processed folder.
                    CharacterConverter::log().info( "Cleaning character folder for rebuild: #{character.absolute_path}" )
                    files_to_remove = []
                    files_to_remove += OS::FindEx::find_files_recurse( OS::Path::combine( character.absolute_path, '*.*' ) ) \
                        if ( File::directory?( character.absolute_path ) )
                    FileUtils::rm_r( files_to_remove )
                else
                    # Nuke all the meta files here as they are copied in again later
                    OS::FindEx.find_files(character.absolute_path + "/*.meta", false).each do |metaFile| 
                        File.delete(metaFile)
                    end 
                end
                
                character.inputs.each do |input|
                    input.inputs.each do |i|                         
                        CharacterConverter::log().info( "Processing Character: #{i.filename}" )   
                           
                        character_mtime = ::File.mtime(i.filename)
                        character_name = OS::Path::remove_extension(OS::Path::get_basename(i.filename))
                        
                        # Check timestamp of the .ped.zip file to decide if we need to go ahead and extract                         
                        if i.filename.end_with?(".ped.zip" ) then

                            needs_extract = false                        
                            ped_files = OS::FindEx.find_files(character.absolute_path + "/#{character_name}.*.zip", false)                            
                            # Just check first file found in the processed location as they all have the same timestamp
                            if ped_files.size > 0 then 
                                ped_file = ped_files.first                                                   
                                if ( (::File.mtime(ped_file) <=> character_mtime) < 0 ) then 
                                    needs_extract = true
                                end 
                            # No files in processed folder so need extracting
                            else
                                needs_extract = true
                            end        
                        
                            if ( needs_extract ) then
                                CharacterConverter::log().info( "\tNeeds Extracting" ) 
                                # Set the extracted files in the processed dir to have the same timestamp as the .ped.zip
                                ProjectUtil::data_zip_extract( i.filename, character.absolute_path, true, '*.*' ) do |filename|
                                    ::File::utime(character_mtime, character_mtime, filename)
                                end
                            end
                            
                            # Check if a meta file exists for this ped
                            file = (OS::Path::remove_extension(OS::Path::get_basename(i.filename)) + ".pso.meta")
                            metaFilename = OS::Path::combine( @project.metadata,'characters', file )                         
                            if File.exists?(metaFilename) then 
                                CharacterConverter::log().info("Copying: #{metaFilename}")
                                FileUtils.cp metaFilename, character.absolute_path 
                            end                            
                        # For shared txds just copy in if the modified time is different.  Character_name used here - but in these
                        # situations it'll be strm_peds_test for example
                        elsif i.filename.include?("_peds_")
                            needs_copying = false
                            txd_file = OS::Path::combine(character.absolute_path,"#{character_name}.itd.zip")
                            if ( File.exists?(txd_file) ) then
                                
                                if ( (::File.mtime(txd_file) <=> character_mtime) < 0 ) then 
                                    needs_copying = true                                
                                end
                            # If no file in processed dir, copy needed
                            else
                                needs_copying = true
                            end  
                            
                            if needs_copying then
                                File.delete(txd_file) if File.exists?(txd_file)
                                if File.exists?(i.filename) then 
                                    FileUtils.cp i.filename, character.absolute_path
                                    File::utime(character_mtime, character_mtime, txd_file)
                                end  
                            end
                        end                      
                    end
                end
                character.reform_inputs( )
            end
                 
            result = {}
            result[:conversion] = @characters
            result[:success] = success
		    result
        end
		
		# Clear the content from the build list.
		def clear( )
            @characters.clear()
		end
		
		# Add the content to the build list (if valid).
		def add_content( content )
			if ( content.is_a?( Content::ProcessedCharacterDirectory ) ) then
				@characters << content
				@characters.uniq!
				true
			else
				CharacterConverter::log().error( "Could not convert: #{content}." )
				false
			end
		end
        		
		# Return MapConverter log object.
		def CharacterConverter::log()
			@@log = Log.new( 'convert_character' ) if ( @@log.nil? )
			@@log
		end
  
		#--------------------------------------------------------------------
		# Private
		#--------------------------------------------------------------------
	private
		@@log = nil
	end
	
end # Converters module
end # Resourcing module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/resourcing/converters/converter_character.rb
