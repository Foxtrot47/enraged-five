#
# File:: %RS_TOOLSLIB%/pipeline/resourcing/converters/convert_rage_packets.rb
# Description:: XGE Rage Converter packet classes.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 20 April 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/os/path'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Resourcing
module Converters
	
	#
	# == Description
	# This class represents a data conversion packet list which is a 
	# collection of Ragebuilder script "packets" that are sent to XGE
	# for conversion.
	#
	# The packet list cleanly separates each target platform for conversion.
	# This ensures that Ragebuilder options and shader changes don't break
	# the data conversion.
	#
	class ConversionPacketList
		attr_reader :packets
		
		# Constructor.
		def initialize( xge_folder, maxsize = 0 )			
			@xge_folder = xge_folder
			@maxsize = maxsize
			@packets = {}
		end
		
		# Create new ConversionPacket.
		def create_packet( platform, local_only )
			packet_name = nil
			filename = nil
			if ( @packets[platform].empty? ) then
				name = '0'
				filename = OS::Path.combine( @xge_folder, "packet0_#{platform}.rbs" ) 
			else
				name = "#{@packets[platform].size}"
				filename = OS::Path.combine( @xge_folder, "packet#{@packets[platform].size}_#{platform}.rbs" )
			end

			ConversionPacket.new( name, filename, local_only )
		end
		
		# Add an item to the last ConversionPacket object.
		def add_item( item )

			throw ArgumentError::new( "Invalid item in ConversionPacket (#{item.class})." ) \
				unless ( item.is_a?( ConvertFile ) )

			packet_key = item.target.platform	
			# TEMPORARY HACK HACK HACK until XGE works with 64bit executables
			# Moved this higher up so that the comparison for local_only below wouldn't
			# evaluate incorrectly and force every Win64 asset to be resourced one-by-one.
			if ( packet_key == 'win64' ) then	
				item.local_only = true
			end
			
			@packets[packet_key] = [] unless ( @packets.has_key?( packet_key ) )
			
			# Create a new packet.
			if ( ( @packets[packet_key].empty? ) or 
				( @packets[packet_key].last.data_size >= @maxsize ) ) then
				@packets[packet_key] << create_packet( packet_key, item.local_only )
			end
			
			# Now we append onto the last packet.
			if ( @packets[packet_key].last.local_only == item.local_only ) then
				# Append if packet is of right type.
				@packets[packet_key].last.add_item( item )
			else
				# Create and append if packet is not of right type.
				@packets[packet_key] << create_packet( packet_key, item.local_only )
				@packets[packet_key].last.add_item( item )
			end
		end
		
		# Create the packet RBS scripts.
		def create( )
			c = Pipeline::Config::instance( )
			OS::FileUtilsEx::delete_files( OS::Path::combine( @xge_folder, '*.rbs' ) )
			
			@packets.each_pair do |platform, platform_packets|		
				platform_packets.each do |packet|
					File::open( packet.filename, "w" ) do |f|
						f.puts( "require(\"#{OS::Path::combine( c.toolslib, 'util', 'ragebuilder', 'convert_xge.rbs' )}\")" )
						
						packet.items.each do |item|
							
							src = item.src
							dst = item.dst
							platform = RageUtils.rage_platform(item.target.platform)
							folder = item.folder
							folder = OS::Path::combine( c.temp, item.target.platform, OS::Path::get_basename( src ) ) if (folder.nil? or folder == "")
							folder = "\"#{folder}\""
							
							f.puts( "convert(\"#{src}\",\"#{dst}\",\"#{platform}\",build,shader,shaderdb,#{folder},temporary,rebuild,options, \"#{c.toolsconfig}\")" )
						end
					end
				end
			end
		end
	end

	#
	# == Description
	# This class represents a single data conversion packet.  A list of these
	# are contained in the ConversionPacketList class.
	#
	# This class is typically not used directly; but through the 
	# ConversionPacketList class instead.
	#
	class ConversionPacket
		attr_reader :name
		attr_reader :filename
		attr_reader :items
		attr_reader :data_size
		attr_reader :target
		attr_reader :local_only	# Flag for local only execution.

		def initialize( name, filename, local_only = false )
			@name = name
			@filename = filename
			@items = []
			@data_size = 0
			@local_only = local_only
			FileUtils::mkdir_p( OS::Path::remove_filename( filename ) )
		end

		def add_item( item )
			throw ArgumentError.new( "Invalid item in ConversionPacket (#{item.class})." ) \
				unless ( item.is_a?( ConvertFile ) )

			@data_size += File::size( item.src )
			@target = item.target
			@items << item
		end
	end
    
	#
	# == Description
	# Utility class to store a content node's source and destination files
	# together with its target (see Pipeline::Target).
    #
    # DHM FIX ME: surely just a Content::File object?!
	#
	class ConvertFile		
		attr_reader :content
		attr_reader :src, :dst, :folder
		attr_accessor :local_only
		
		def initialize( content, src, dst, folder = '', local_only = false )		
			@content = content
			@src = src
			@dst = dst
			@local_only = local_only
			@folder = folder
		end
        
        def target( )
            @content.target
        end
	end

    #
    # == Description
    # Utility class to store a content node's source directory and destination
    # file together with its target (see Pipeline::Target).
    #
    # DHM FIX ME: surely just a Content::Directory object?!
    #
    class ConvertDirectory
        attr_reader :content
        attr_reader :src, :dst, :folder
        attr_accessor :local_only
        
        def initialize( content, src, dst, folder = '', local_only = false )
            @content = content
            @src = src
            @dst = dst
            @local_only = local_only
            @folder = folder
        end
        
        def target( )
            @content.target
        end
    end

end # Converters module
end # Resourcing module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/resourcing/converters/convert_rage_packets.rb
