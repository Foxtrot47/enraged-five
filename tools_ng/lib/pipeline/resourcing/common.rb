#
# File:: %RS_TOOLSLIB%/pipeline/resourcing/common.rb
# Description:: Resourcing common functionality/classes etc.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 17 September 2008
#

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Resourcing
	
	#
	# == Description
	# BuildError object exceptions can be raised by any of the converter
	# build functions.  This exception indicates that a fatal build error has
	# occurred.
	#
	# Since we inherit from Ruby's Exception class we also get access to a
	# stacktrace (see backtrace member) to pin-point the problem.
	#
	class BuildError < Exception
	
		attr_reader :content
	
		#
		# Constructor, specifying our content_node that the fatal error
		# occurred and a converter-specific error message.
		#
		def initialize( content_node, message = '' )
			super( message )
			@content = content_node
		end
	end

end # Resourcing module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/resourcing/common.rb
