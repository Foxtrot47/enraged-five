#
# File:: %RS_TOOLSLIB%/pipeline/resourcing/converter.rb
# Description:: Base converter class definition.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 13 January 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/branch'
require 'pipeline/config/project'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Resourcing
	
	#
	# == Description
	# This is the base converter class; all converters should implement this
	# abstract class.
	#
	class ConverterBase
		attr_reader :project
		attr_reader :branch
		
		def initialize( project, branch )
			throw ArgumentError.new( "Invalid project object (#{project.class})." ) \
				unless ( project.is_a?( Pipeline::Project ) )
			throw ArgumentError.new( "Invalid branch object (#{branch.class})." ) \
				unless ( branch.is_a?( Pipeline::Branch ) )
			
			@project = project
			@branch = branch
			@c = ConvertSystem::instance( )
		end
		
		# Return whether this converter can convert the specified content node.
		# Converters must implement this method.
		def can_convert?( content )
			throw NotImplementedError::new( 'Converters needs to override can_convert? method' )
		end
		
		# Add the content; queue up the content nodes to be converted by this
		# converter.
		def add_content( content )
			# nop; needs to be implemented in individual converters.
		end
		
		# Prebuild the content; typically used to sync build-time dependencies.
		# This is automatically invoked after all content has been added so
		# any internal state can be used.
		#
		# Converters can optionally implement this method.
		#
		def prebuild( )
			# nop; needs to be implemented in individual converters.
		end
		
		# Build the content; invoking block as required with the content node
		# and a boolean success flag.
		#
		# This method should return +true+ on auccess; +false+ on failure.
		#
		# Converters must implement this method.
		#
		def build( &block )
			throw NotImplementedError::new( 'Converters needs to override build method' )
		end
		
		#--------------------------------------------------------------------
		# Protected
		#--------------------------------------------------------------------
	protected
		
		# Dynamic content classes support; handle inherited classes by 
		# registering them with our system so we can drop in new ProcessorBase
		# derrivatives.
		def self.inherited( child )
			ConverterBase.registered_converter_types << child
		end
		@registered_converter_types = []
		class << self; attr_reader :registered_converter_types end
	end
	
	#
	# == Description
	# This is a converter mixin module that supports a series of child 
	# converters.  For custom converters that require to support a series of
	# child converters include this module in your root converter class.
	#
	module HasConverterChildren
		attr_reader :children
		
		def initialize_children( )
			@children = []
		end
		
		def add_child_converter( converter )
			throw RuntimeError::new( "Invalid child converter (#{converter.class})." ) \
				unless ( converter.is_a?( ChildConverterBase ) )
			@children << converter
            converter.parent = self
		end
		
		def build( &block )
			throw NotImplementedError::new( 'HasConverterChildren needs to override build method' )
		end
	end
	

	#
	# == Description
	# This is the base child converter class; all child converters should 
	# implement this abstract class.
	#
	class ChildConverterBase
		attr_reader :project
		attr_reader :branch
        attr_accessor :parent
		
		def initialize( project, branch )
			throw ArgumentError.new( "Invalid project object (#{project.class})." ) \
				unless ( project.is_a?( Pipeline::Project ) )
			throw ArgumentError.new( "Invalid branch object (#{branch.class})." ) \
				unless ( branch.is_a?( Pipeline::Branch ) )
			
			@project = project
			@branch = branch
		end
		
		# Prebuild the content.
		def prebuild( content )
		end
		
		# Build the content; invoking block as required with the content node
		# and a boolean success flag.
        #
		# Converters must implement this method.
		#
		# This method should return a Hash object optionally containing the
		# following keys:
		#  :success    => bool success flag (true successful, false otherwise)
		#  :conversion => Array of content nodes to be added for later stage conversion.
		#
		def build( content, &block )
			throw NotImplementedError::new( 'Converters needs to override build method' )
		end
	end		
	
end # Resourcing module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/resourcing/converter.rb
