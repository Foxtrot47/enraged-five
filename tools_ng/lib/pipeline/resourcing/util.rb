#
# File:: util.rb
# Description:: Resourcing utilities
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 1 September 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/resourcing/path'
require 'pipeline/os/path'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Resourcing

	# 
	# Takes content_in and fills a content container content_out with platform specific versions of content_in.
	# This can then be passed to the converter.  Useful for previewing.
	#
	# == Example Usage ==
	# %RS_TOOLSLIB%/util/data_convert_file_preview.rb
	#
    def Resourcing::create_target_content_from_indepenent( content_in, content_out, project, out_dir = nil )

        if ( ( content_in.is_a?(Pipeline::Content::Target) ) and ( content_in.target == project.ind_target ) )
            project.targets.each_value do |target|
				next unless ( target.enabled )

				name = OS::Path::get_corebasename( content_in.filename )
				ext = Pipeline::Resourcing::convert_independent_extension_to_platform( content_in.filename, content_in.extension, target ) 
				out_dir = content_in.path if ( out_dir.nil? )
								
				pack = Pipeline::Content::RPF.new( name, out_dir, ext, target )
				pack.add_input( content_in )
				content_out.add_child( pack )
            end
        elsif ( content_in.is_a?( Pipeline::Content::Group ) )
            content_in.children.each do |child|
                create_target_content_from_indepenent( child, content_out, project, out_dir )
            end
        end
    end 

end # Resourcing module
end # Pipeline module
