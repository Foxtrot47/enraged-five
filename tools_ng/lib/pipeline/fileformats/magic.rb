#
# File:: magic.rb
# Description:: Binary file magic number functions.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 9 October 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
# None

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module FileFormats

	#
	# == Description
	# Magic number functions.
	#
	class Magic
	
		#
		# From an String of characters converts them into ASCII character codes 
		# to use as a magic number.
		#
		def Magic::make( str )
			#throw ArgumentError.new( "Only expecting a 32-bit magic number, 4 characters only (got #{str.size})." ) \
			#	if ( 4 != str.size )
			# Unpack string as unsigned characters.
			str.unpack( 'C*' )
		end
		
		#
		# From an Array of unsigned chars converts them to a string.
		#
		def Magic::expand( data )
			#throw ArgumentError.new( "Only expecting a 32-bit magic number, 4 characters only." ) \
			#	if ( 4 != data.size )
		
			# Pack array as characters.
			data.pack( 'C*' )
		end
	end

end # FileFormats module
end # Pipeline module

# magic.rb
