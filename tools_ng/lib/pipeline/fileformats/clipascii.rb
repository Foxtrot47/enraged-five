#
# File:: clipascii.rb
# Description:: ASCII Clip file loader.
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 23rd November 2011
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/log/log'
require 'pipeline/math/quat'
require 'pipeline/math/vector3'
require 'pipeline/math/vector4'
require 'pipeline/math/matrix34'


#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module FileFormats

	#
	# == Description
	# Loads up clip files that have been dumped to ascci.  A workaround for the fact we can't easily mount binary clip files	
	#
	module ClipAscii
		# Loader version.
		VERSION = 1.10
		
		class PropertyType
		  NONE 		= 0
		  FLOAT 	= 1
		  INT 		= 2
		  BOOL 		= 3
		  STRING 	= 4
		  BITSET 	= 5
		  VEC3		= 6
		  VEC4 		= 7
		  QUAT		= 8
		  MATRIX	= 9
		  DATA		= 10
		  SITUATION	= 11
		end
		
		#
		# == Description
		# Represents a rutnime clip containing tags and properties.  Tags are NOT currently handled	
		# 
		class Clip
			
			attr_reader	:filename
			attr_reader	:properties
			
			def initialize( filename )
				Clip::log().info( "Loading Clip from #{filename}." )
				@filename = filename
				@properties = {}
				
				parse( )
			end
			
			def parse( )
				clipascii_file = File.new( filename )
				lines = clipascii_file.readlines
				
				property = nil
				for i in 0..lines.count
					line = lines[i]
					
					if line != nil
						if line.match( /property[0-9]/ )
							propertytokens = line.split( ' ' )
							property = Property.new( propertytokens[1].sub( '\'', '' ).sub( '\'', '' ) )

							i = i + 3
							line = lines[i]
							numattrs = line.split( ' ' )[1].to_i
							for j in 0..numattrs
								propattr = nil
								name = ''
								type = nil
								while line.strip().empty? == false and i < lines.count
									if line.match("attributename")
										name = line.split( ' ' )[1].sub( '\'', '' ).sub( '\'', '' )	
									elsif line.match("attributetype")
										type = line.split( ' ' )[2].to_i
										case type
										when PropertyType::INT # int
											i = i + 1
											value = lines[i].split( ' ' )[1].to_i
											propattr = PropertyAttributeString.new( name, type, value )
										when PropertyType::STRING # string
											i = i + 1
											value = lines[i].split( ' ' )[1].to_s
											propattr = PropertyAttributeInt.new( name, type, value )
										else
										end
									end
									i = i + 1
									line = lines[i]
								end
								property.propertyattributes[propattr.name] = propattr if propattr != nil
							end
							if property != nil
								
								properties[property.name] = property
							end
						end
					end
				end
			end
			
			def Clip::log()
				@@log = Pipeline::Log.new( 'clip_ascii' ) if ( @@log.nil? )
				@@log
			end

			#---------------------------------------------------------------------
			# Protected
			#---------------------------------------------------------------------
		private
			@@log = nil			
		end # Clip
		
		class Property
			
			attr_reader	:name
			attr_reader 	:propertyattributes
			
			def initialize( name )
				@name = name
				@propertyattributes = {}
			end
		end # Property
		
		class PropertyAttribute
					
			attr_reader	:name
			attr_reader	:type
			
			def initialize( name, type )
				@name  = name
				@type = type
			end
		end # PropertyAttribute
		
		class PropertyAttributeString < PropertyAttribute
			attr_reader	:value
		
			def initialize( name, type, value )
				super( name, type )
				@value = value
			end
		end # PropertyAttributeString
		
		class PropertyAttributeInt < PropertyAttribute
			attr_reader	:value
		
			def initialize( name, type, value )
				super( name, type )
				@value = value
			end	
		end # PropertyAttributeInt
	end # ClipAscii
end # Pipeline
end # FileFormats

#luke = Pipeline::FileFormats::ClipAscii::Clip.new("X:\\gta5\\cache\\convert\\dev\\clips\\clip\\amb@breakdown_f_emperor\\enter_ped.clip.txt")
#puts luke.properties['Compressionfile_DO_NOT_RESOURCE'].propertyattributes['Compressionfile_DO_NOT_RESOURCE'].value