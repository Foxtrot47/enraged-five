#
# File:: scenexml_attrs.rb
# Description:: SceneXml Object Attributes
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 18 November 2008
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
# None

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module FileFormats
module SceneXml

	#
	# == Description
	# Utility class for use by the ObjectDef class (and supporting classes)
	# to help maintain ObjectDef object's attributes Hash.
	#
	# +Note+: many of these are probably never used but they have been 
	# copied verbatim from our GTA3 Max plugin and categorised where that
	# information is available.
	#
	class ObjectDef
		
		MAX_CLASS_EDITABLE_MESH				= 'editable_mesh'.downcase
		MAX_CLASS_EDITABLE_POLY				= 'editable_poly'.downcase
		MAX_CLASS_XREF						= 'xref_object'.downcase
		MAX_CLASS_INTERNALREF				= 'InternalRef'.downcase
		
		MAX_SUPERCLASS_HELPER				= 'Helper'.downcase
		
		ATTR_CLASS_NONE						= ''.downcase
		ATTR_CLASS_BLOCK					= 'Gta Block'.downcase
		ATTR_CLASS_CARGEN					= 'Gta CarGen'.downcase
		ATTR_CLASS_COLLISION				= 'Gta Collision'.downcase
		ATTR_CLASS_ENTRYEXIT				= 'Gta EntryExit'.downcase
		ATTR_CLASS_GARAGEAREA				= 'Gta GarageArea'.downcase
		ATTR_CLASS_GROUP					= 'Gta Group'.downcase
		ATTR_CLASS_LODMODIFIER				= 'Gta LodModifier'.downcase
		ATTR_CLASS_MILO						= 'Gta MILO'.downcase
		ATTR_CLASS_MILOTRI					= 'Gta MILOTri'.downcase
		ATTR_CLASS_MLOPORTAL				= 'Gta MloPortal'.downcase
		ATTR_CLASS_MLOROOM					= 'Gta MloRoom'.downcase
		ATTR_CLASS_MULTIBLDG				= 'Gta MultiBldg'.downcase
		ATTR_CLASS_OBJ						= 'Gta Object'.downcase
		ATTR_CLASS_PICKUP					= 'Gta Pickup'.downcase
		ATTR_CLASS_SOUNDZONE				= 'Gta SoundZone'.downcase
		ATTR_CLASS_STUNTJUMP				= 'Gta StuntJump'.downcase
		ATTR_CLASS_TIMECYCLE				= 'Gta TimeCycle'.downcase
		ATTR_CLASS_ZONE						= 'Gta Zone'.downcase
	
		ATTR_CLASS_2DFX_AUDIO_EMITTER		= 'Gta AudioEmit'.downcase
		ATTR_CLASS_2DFX_LIGHT_SHAFT			= 'Gta LightShaft'.downcase
		ATTR_CLASS_2DFX_PROC_OBJECT			= 'Gta ProcObj'.downcase
		ATTR_CLASS_2DFX_WALK_DONT_WALK		= 'Gta WalkDontWalk'.downcase
		ATTR_CLASS_2DFX_SCRIPT				= 'Gta Script'.downcase
		ATTR_CLASS_2DFX_LADDER				= 'GtaLadderFX'.downcase
		ATTR_CLASS_2DFX_LEDGE				= 'RSLedge'.downcase
		ATTR_CLASS_2DFX_PED_QUEUE			= 'Gta Queue'.downcase
		ATTR_CLASS_2DFX_SPAWN_POINT			= 'Gta SpawnPoint'.downcase
		ATTR_CLASS_2DFX_SCROLLBARS			= 'Gta Scrollbars'.downcase
		ATTR_CLASS_2DFX_WINDOW_LEDGE		= 'Gta WindowLegde'.downcase
		ATTR_CLASS_2DFX_BUOYANCY			= 'Gta Buoyancy'.downcase
		ATTR_CLASS_2DFX_LIGHT				= 'Gta Light'.downcase
		ATTR_CLASS_2DFX_LIGHT_PHOTO			= 'Gta LightPhoto'.downcase
		ATTR_CLASS_2DFX_PARTICLE_EFFECT		= 'Gta Particle'.downcase
		ATTR_CLASS_2DFX_EXPLOSION_EFFECT	= 'Gta Explosion'.downcase
		ATTR_CLASS_2DFX_SWAYABLE_EFFECT		= 'Gta Swayable'.downcase		
		
		#----------------------------------------------------------------
		# Gta Object
		#----------------------------------------------------------------			
		ATTR_STR_OBJ_LOD_DISTANCE			= 'LOD distance'.downcase
		ATTR_STR_OBJ_TXD					= 'TXD'.downcase
		ATTR_STR_OBJ_REAL_TXD				= 'REALTXD'.downcase
		ATTR_STR_OBJ_COLLISION_GROUP		= 'Collision Group'.downcase
		ATTR_STR_OBJ_IPL_GROUP				= 'IPL Group'.downcase
		ATTR_STR_OBJ_AREA_CODE				= 'Area Code'.downcase
		ATTR_STR_OBJ_DRAW_LAST				= 'Draw last'.downcase
		ATTR_STR_OBJ_EXPORT_VERTEX_COLOURS	= 'Export Vertex Colours'.downcase
		ATTR_STR_OBJ_LEVEL_DESIGN_OBJECT	= 'Level Design Object'.downcase
		ATTR_STR_OBJ_BREAKABLE				= 'Breakable'.downcase		
		ATTR_STR_OBJ_DONT_COLLIDE_WITH_FLYER = 'Dont Collide With Flyer'.downcase
		ATTR_STR_OBJ_DONT_ADD_TO_IPL		= 'Dont Add To IPL'.downcase
		ATTR_STR_OBJ_DONT_EXPORT			= 'Dont Export'.downcase
		ATTR_STR_OBJ_IS_DYNAMIC				= 'Is Dynamic'.downcase
		ATTR_STR_OBJ_IS_FIXED				= 'Is Fixed'.downcase
		ATTR_STR_OBJ_BACKFACE_CULL			= 'Backface Cull'.downcase
		ATTR_STR_OBJ_LOW_PRIORITY			= 'Low Priority'.downcase
		ATTR_STR_OBJ_USE_FULL_MATRIX		= 'Use Full Matrix'.downcase
		ATTR_STR_OBJ_NON_STREAMED_INST		= 'Non Streamed Instance'.downcase
		ATTR_STR_OBJ_IS_UNDERWATER			= 'Is Underwater'.downcase
		ATTR_STR_OBJ_IS_TUNNEL				= 'Is Tunnel'.downcase
		ATTR_STR_OBJ_IS_TUNNEL_TRANSITION	= 'Is Tunnel Transition'.downcase
		ATTR_STR_OBJ_DOES_NOT_PROVIDE_AI_COVER = 'Does Not Provide AI Cover'.downcase
		ATTR_STR_OBJ_BREAK_USE_PIVOT		= 'Break Use Pivot'.downcase
		ATTR_STR_OBJ_MILO_PREVIEW			= 'Milo Preview'.downcase
		ATTR_STR_OBJ_IS_FRAGMENT			= 'Is Fragment'.downcase
		ATTR_STR_OBJ_IS_FRAGMENT_PROXY		= 'Is FragProxy'.downcase
		ATTR_STR_OBJ_INSTANCE				= 'Instanced'.downcase
		ATTR_STR_OBJ_DONT_CAST_SHADOWS		= 'Dont cast shadows'.downcase
		ATTR_STR_OBJ_CAST_TEXTURE_SHADOWS	= 'Cast texture shadows'.downcase
		ATTR_STR_OBJ_BLOCK_ID				= 'BlockID'.downcase
		ATTR_STR_OBJ_ATTRIBUTE				= 'Attribute'.downcase
		ATTR_STR_OBJ_IS_LADDER				= 'Is Ladder'.downcase
		ATTR_STR_OBJ_HAS_DOOR_PHYSICS		= 'Has Door Physics'.downcase
		ATTR_STR_OBJ_PACK_TEXTURES			= 'Pack Textures'.downcase
		ATTR_STR_OBJ_DOES_NOT_TOUCH_WATER	= 'Does Not Touch Water'.downcase
		ATTR_STR_OBJ_IS_FIXED_FOR_NAVIGATION = 'Is Fixed For Navigation'.downcase
		ATTR_STR_OBJ_ANIM					= 'Anim'.downcase
		ATTR_STR_OBJ_HAS_ANIM				= 'Has Anim'.downcase
		ATTR_STR_OBJ_HAS_UV_ANIM			= 'Has UvAnim'.downcase
		ATTR_STR_OBJ_DONT_AVOID_BY_PEDS		= 'Don\'t Avoid By Peds'.downcase
		ATTR_STR_OBJ_NEW_FRAG				= 'New Frag'.downcase
		ATTR_STR_OBJ_HOLD_FOR_LOD			= 'Hold for LOD'.downcase
		ATTR_STR_OBJ_INSTANCE_LOD_DISTANCE	= 'Instance LOD distance'.downcase
		ATTR_STR_OBJ_HAND_EDITED_COLLISION	= 'Hand edited collision'.downcase
		ATTR_STR_OBJ_ENABLE_AMBIENT_MULTIPLIER = 'Enable Ambient Multiplier'.downcase
		ATTR_STR_OBJ_ALWAYS_LOADED			= 'Always Loaded'.downcase
		ATTR_STR_OBJ_EDITED_BY_LEEDS		= 'Edited by Leeds'.downcase
		ATTR_STR_OBJ_SHADOW_ONLY			= 'Shadow Only'.downcase
		ATTR_STR_OBJ_DISABLE_PED_SPAWNING	= 'Disable Ped Spawning'.downcase
		ATTR_STR_OBJ_STATIC_SHADOWS			= 'Static Shadows'.downcase
		ATTR_STR_OBJ_DYNAMIC_SHADOWS		= 'Dynamic Shadows'.downcase
		ATTR_STR_OBJ_ON_ALL_DAY				= 'On all day'.downcase
		ATTR_STR_OBJ_SUPERLOD_WITH_ALPHA	= 'Superlod with Alpha'.downcase
		ATTR_STR_OBJ_TOUGH_FOR_BULLETS		= 'Tough for Bullets'.downcase
		ATTR_STR_OBJ_INSTANCE_COLLISION		= 'Instance Collision'.downcase
		ATTR_STR_OBJ_AUTHORED_ORIENTS		= 'Authored Orients'.downcase
		ATTR_STR_OBJ_DONT_ADD_TO_IDE		= 'Dont Add To IDE'.downcase
		ATTR_STR_OBJ_REVERSE_COVER_FOR_PLAYER = 'Reverse Cover For Player'.downcase
		ATTR_STR_OBJ_DAY_NIGHT_AMBIENT		= 'Day/Night Ambient'.downcase
		ATTR_STR_OBJ_PRIORITY				= 'Priority'.downcase
		ATTR_STR_OBJ_EXPORT_GEOMETRY		= 'Export Geometry'.downcase
		ATTR_STR_OBJ_ALLOW_ANY_LOD_DISTANCE	= 'Allow Any Lod Distance'.downcase
		ATTR_STR_OBJ_MODEL_GROUP			= 'Model Group'.downcase
		
		# Likely obsolete -- DHM 18 November 2008 (required for IDE)
		ATTR_STR_OBJ_LIGHT_REFLECTION		= 'Light reflection'.downcase
		ATTR_STR_OBJ_DO_NOT_FADE			= 'Do not fade'.downcase
		ATTR_STR_OBJ_NO_ZBUFFER_WRITE		= 'No Zbuffer write'.downcase
		ATTR_STR_OBJ_IS_FIRE_ESCAPE			= 'Is Fire Escape'.downcase
		
		#----------------------------------------------------------------
		# Gta Object : Time
		#----------------------------------------------------------------			
		ATTR_STR_OBJ_IS_TIME_OBJECT = 'Is time object'.downcase
		ATTR_STR_TOBJ_ALLOW_VANISH_WHILST_VIEWED = 'Allow Vanish Whilst Viewed'.downcase
		ATTR_STR_TOBJ_TIME_1		= '1'.downcase
		ATTR_STR_TOBJ_TIME_2		= '2'.downcase
		ATTR_STR_TOBJ_TIME_3		= '3'.downcase
		ATTR_STR_TOBJ_TIME_4		= '4'.downcase
		ATTR_STR_TOBJ_TIME_5		= '5'.downcase
		ATTR_STR_TOBJ_TIME_6		= '6'.downcase
		ATTR_STR_TOBJ_TIME_7		= '7'.downcase
		ATTR_STR_TOBJ_TIME_8		= '8'.downcase
		ATTR_STR_TOBJ_TIME_9		= '9'.downcase
		ATTR_STR_TOBJ_TIME_10		= '10'.downcase
		ATTR_STR_TOBJ_TIME_11		= '11'.downcase
		ATTR_STR_TOBJ_TIME_12		= '12'.downcase
		ATTR_STR_TOBJ_TIME_13		= '13'.downcase
		ATTR_STR_TOBJ_TIME_14		= '14'.downcase
		ATTR_STR_TOBJ_TIME_15		= '15'.downcase
		ATTR_STR_TOBJ_TIME_16		= '16'.downcase
		ATTR_STR_TOBJ_TIME_17		= '17'.downcase
		ATTR_STR_TOBJ_TIME_18		= '18'.downcase
		ATTR_STR_TOBJ_TIME_19		= '19'.downcase
		ATTR_STR_TOBJ_TIME_20		= '20'.downcase
		ATTR_STR_TOBJ_TIME_21		= '21'.downcase
		ATTR_STR_TOBJ_TIME_22		= '22'.downcase
		ATTR_STR_TOBJ_TIME_23		= '23'.downcase
		ATTR_STR_TOBJ_TIME_24		= '24'.downcase		
		
		#--------------------------------------------------------------------------
		# Gta Collision
		#--------------------------------------------------------------------------		
		ATTR_STR_COLLISION_AUDIO_MATERIAL = 'Audio Material'.downcase
		
		#--------------------------------------------------------------------------
		# Gta Block
		#--------------------------------------------------------------------------		
		ATTR_STR_BLOCK_BLOCK_ID		= 'BlockID'.downcase
		ATTR_STR_BLOCK_ARTIST		= 'Artist'.downcase
		ATTR_STR_BLOCK_DATE_EDITED	= 'Date Edited'.downcase
		ATTR_STR_BLOCK_IN_PROGRESS	= 'In Progress'.downcase
		ATTR_STR_BLOCK_FIRST_PASS	= 'First Pass'.downcase
		ATTR_STR_BLOCK_SECOND_PASS	= 'Second Pass'.downcase
		ATTR_STR_BLOCK_POLISHED		= 'Polished'.downcase
		ATTR_STR_BLOCK_OPTIMISED	= 'Optimised'.downcase
		ATTR_STR_BLOCK_STREAMING_WELL = 'Streaming Well'.downcase
		ATTR_STR_BLOCK_COMPLETE		= 'Complete'.downcase
		ATTR_STR_BLOCK_MEMMODEL		= 'MemModel'.downcase
		ATTR_STR_BLOCK_MEMCOLLISION	= 'MemCollision'.downcase
		ATTR_STR_BLOCK_MEMTEXTURE	= 'MemTexture'.downcase
		ATTR_STR_BLOCK_NUMMODELS	= 'NumModels'.downcase
		ATTR_STR_BLOCK_NUMXREFS		= 'NumXRefs'.downcase
		ATTR_STR_BLOCK_FREEZE		= 'Freeze'.downcase
		
		#--------------------------------------------------------------------------
		# Gta Milo
		#--------------------------------------------------------------------------		
		ATTR_STR_MILO_IS_MLO_LOD	= 'Is MLO LOD'.downcase
		ATTR_STR_MILO_DONT_EXPORT_MILO	= 'Don\'t Export Milo'.downcase
		ATTR_STR_MILO_PROPOGATE_INSTANCING = 'Propogate Instancing'.downcase
		ATTR_STR_MILO_OFFICE_POPULATION	= 'Office Population'.downcase
		ATTR_STR_MILO_ALLOW_SPRINTING = 'Allow Sprinting'.downcase
		ATTR_STR_MILO_CUTSCENE_ONLY = 'Cutscene Only'.downcase
		ATTR_STR_MILO_LOD_WHEN_LOCKED = 'LOD When Locked'.downcase
		ATTR_STR_MILO_NO_WATER_REFLECTION = 'No Water Reflection'.downcase
		ATTR_STR_MILO_DONT_DRAW_IF_LOCKED = 'Dont Draw If Locked'.downcase
		ATTR_STR_MILO_DETAIL			= 'Detail'.downcase
		ATTR_STR_MILO_PRIORITISE_DECALS	= 'Prioritise Decals/Alphas'.downcase
		
		#--------------------------------------------------------------------------
		# Gta MiloTri
		#--------------------------------------------------------------------------		
		ATTR_STR_MILOTRI_DONT_EXPORT_MILO_TRI = 'Don\'t Export Milo Tri'.downcase
		
		#--------------------------------------------------------------------------
		# Gta MloRoom
		#--------------------------------------------------------------------------		
		ATTR_STR_MLOROOM_TIMECYCLE			= 'Timecycle'.downcase
		ATTR_STR_MLOROOM_BLEND				= 'Blend'.downcase
		ATTR_STR_MLOROOM_FREEZE_PEDS		= 'Freeze Peds'.downcase
		ATTR_STR_MLOROOM_FREEZE_CARS		= 'Freeze Cars'.downcase
		ATTR_STR_MLOROOM_REDUCE_PEDS		= 'Reduce Peds'.downcase
		ATTR_STR_MLOROOM_REDUCE_CARS		= 'Reduce Cars'.downcase
		ATTR_STR_MLOROOM_NO_DIRECTIONAL_LIGHT = 'No Directional Light'.downcase
		ATTR_STR_MLOROOM_NO_EXTERIOR_LIGHT	= 'No Exterior Light'.downcase
		ATTR_STR_MLOROOM_FORCE_FREEZE		= 'Force Freeze'.downcase
		
		#--------------------------------------------------------------------------
		# Gta MloPortal
		#--------------------------------------------------------------------------
		ATTR_STR_MLOPORTAL_FADE_DISTANCE	= 'Fade Distance'.downcase
		ATTR_STR_MLOPORTAL_RED				= 'Red'.downcase
		ATTR_STR_MLOPORTAL_GREEN			= 'Green'.downcase
		ATTR_STR_MLOPORTAL_BLUE				= 'Blue'.downcase		
		ATTR_STR_MLOPORTAL_ONE_WAY			= 'One Way'.downcase
		ATTR_STR_MLOPORTAL_MLO_LINK			= 'MLO Link'.downcase
		ATTR_STR_MLOPORTAL_MIRROR			= 'Mirror'.downcase
		ATTR_STR_IGNORE_BY_MODIFIER			= 'Ignored by Modifier'.downcase		
		ATTR_STR_MLOPORTAL_EXPENSIVE_SHADERS = 'Expensive Shaders'.downcase
		ATTR_STR_MLOPORTAL_DRAW_LOW_LOD_ONLY = 'Draw Low LOD Only'.downcase
		ATTR_STR_MLOPORTAL_ALLOW_CLOSING	= 'Allow Closing'.downcase

		PROP_STR_MLOPORTAL_ATTACHED_OBJECTS	= 'PortalObjects'.downcase
		PROP_STR_MLOPORTAL_FIRST_ROOM		= 'LinkFirst'.downcase
		PROP_STR_MLOPORTAL_SECOND_ROOM		= 'LinkSecond'.downcase
		PROP_STR_MLOPORTAL_LENGTH			= '0'.downcase
		PROP_STR_MLOPORTAL_WIDTH			= '1'.downcase
		PROP_STR_MLOPORTAL_GRID				= '2'.downcase
		
		#--------------------------------------------------------------------------
		# Cargen
		#--------------------------------------------------------------------------
		ATTR_STR_MODEL				= 'Model'.downcase
		ATTR_STR_COLOUR1			= 'Colour1'.downcase
		ATTR_STR_COLOUR2			= 'Colour2'.downcase
		ATTR_STR_COLOUR3			= 'Colour3'.downcase
		ATTR_STR_COLOUR4			= 'Colour4'.downcase
		ATTR_STR_HIGH_PRIORITY		= 'High Priority'.downcase
		ATTR_STR_ALARM_CHANCE		= 'Alarm Chance'.downcase
		ATTR_STR_LOCKED_CHANCE		= 'Locked Chance'.downcase
		ATTR_STR_MIN_DELAY			= 'Min Delay'.downcase
		ATTR_STR_MAX_DELAY			= 'Max Delay'.downcase
		ATTR_STR_POLICE				= 'Police'.downcase
		ATTR_STR_FIRETRUCK			= 'Firetruck'.downcase
		ATTR_STR_AMBULANCE			= 'Ambulance'.downcase
		ATTR_STR_ALIGN_LEFT			= 'Align Left'.downcase
		ATTR_STR_ALIGN_RIGHT		= 'Align Right'.downcase
		ATTR_STR_SINGLE_PLAYER		= 'Single Player'.downcase
		ATTR_STR_NETWORK			= 'Network'.downcase
		ATTR_STR_CREATION_RULE		= 'Creation Rule'.downcase
		ATTR_STR_SCENARIO			= 'Scenario'.downcase
		
		#--------------------------------------------------------------------------
		# Vehicle Node
		#--------------------------------------------------------------------------
		ATTR_STR_VEHNOD_DISABLED	= 'Disabled'.downcase
		ATTR_STR_VEHNOD_WATER		= 'Water'.downcase
		ATTR_STR_VEHNOD_LOW_BRIDGE	= 'LowBridge'.downcase
		ATTR_STR_VEHNOD_SPEED		= 'Speed'.downcase
		ATTR_STR_VEHNOD_SPECIAL		= 'Special'.downcase
		ATTR_STR_VEHNOD_DENSITY		= 'Density'.downcase
		ATTR_STR_VEHNOD_STREETNAME	= 'Streetname'.downcase
		ATTR_STR_VEHNOD_HIGHWAY		= 'Highway'.downcase
		ATTR_STR_VEHNOD_NO_GPS		= 'NoGps'.downcase
		ATTR_STR_VEHNOD_TUNNEL		= 'Tunnel'.downcase
		ATTR_STR_VEHNOD_OPENSPACE	= 'OpenSpace'.downcase
		
		#--------------------------------------------------------------------------
		# Vehicle Link
		#--------------------------------------------------------------------------
		ATTR_STR_VEHLNK_LANES_IN	= 'Lanes In'.downcase
		ATTR_STR_VEHLNK_LANES_OUT	= 'Lanes Out'.downcase
		ATTR_STR_VEHLNK_WIDTH		= 'Width'.downcase
		ATTR_STR_VEHLNK_NARROW_ROAD	= 'Narrowroad'.downcase
		ATTR_STR_VEHLNK_GPS_BOTH_WAYS = 'GpsBothWays'.downcase
		
		#--------------------------------------------------------------------------
		# Patrol Node
		#--------------------------------------------------------------------------
		ATTR_STR_PATNOD_DURATION	= 'Patrol Duration'.downcase
		ATTR_STR_PATNOD_HEADING_X	= 'Heading X'.downcase
		ATTR_STR_PATNOD_HEADING_Y	= 'Heading Y'.downcase
		ATTR_STR_PATNOD_HEADING_Z	= 'Heading Z'.downcase
		ATTR_STR_PATNOD_ASSOCIATED_BASE = 'Associated Base'.downcase
		ATTR_STR_PATNOD_ROUTE_NAME	= 'Route Name'.downcase
		ATTR_STR_PATNOD_PATROL_TYPE	= 'Patrol Type'.downcase
		
		#--------------------------------------------------------------------------
		# Patrol Link
		#--------------------------------------------------------------------------
		
		# Currently empty.
		
		#--------------------------------------------------------------------------
		# Gta Particle / Gta Explosion
		#--------------------------------------------------------------------------		
		ATTR_STR_2DFX_PARTICLE_NAME		= 'Name'.downcase
		ATTR_STR_2DFX_PARTICLE_TRIGGER	= 'Trigger'.downcase
		ATTR_STR_2DFX_PARTICLE_ATTACH	= 'Attach'.downcase
		ATTR_STR_2DFX_PARTICLE_ATTACH_TO_ALL = 'Attach to All'.downcase
		ATTR_STR_2DFX_PARTICLE_SCALE	= 'Scale'.downcase
		ATTR_STR_2DFX_PARTICLE_PROBABILITY = 'Probability'.downcase
		ATTR_STR_2DFX_PARTICLE_COLOUR_R	= 'R'.downcase
		ATTR_STR_2DFX_PARTICLE_COLOUR_G	= 'G'.downcase
		ATTR_STR_2DFX_PARTICLE_COLOUR_B	= 'B'.downcase
		ATTR_STR_2DFX_EXPLOSION_NAME	= 'Explosion Tag'.downcase
		
		# Flags
		ATTR_STR_2DFX_PARTICLE_IGNORE_DAMAGE_MODEL = 'Ignore Damage Model'.downcase
		ATTR_STR_2DFX_PARTICLE_PLAY_ON_PARENT = 'Play On Parent'.downcase
		ATTR_STR_2DFX_PARTICLE_HAS_TINT	= 'Has Tint'.downcase
		
		#--------------------------------------------------------------------------
		# Gta Light and Gta LightPhoto
		#--------------------------------------------------------------------------		
		ATTR_STR_2DFX_LIGHT_SIZE			= 'Light size'.downcase
		ATTR_STR_2DFX_LIGHT_CORONA_SIZE		= 'Corona size'.downcase
		ATTR_STR_2DFX_LIGHT_FLASHINESS		= 'Flashiness'.downcase
		ATTR_STR_2DFX_LIGHT_CORONA_NAME		= 'Corona name'.downcase
		ATTR_STR_2DFX_LIGHT_ENABLED			= 'Enabled'.downcase
		ATTR_STR_2DFX_LIGHT_ATTACH			= 'Attach'.downcase
		ATTR_STR_2DFX_LIGHT_VOLUME_SIZE		= 'Volume Size'.downcase
		ATTR_STR_2DFX_LIGHT_VOLUME_INTENSITY	= 'Volume Intensity'.downcase
		ATTR_STR_2DFX_LIGHT_LIGHT_FADE_DISTANCE = 'Light Fade Distance'.downcase
		ATTR_STR_2DFX_LIGHT_VOLUME_FADE_DISTANCE = 'Volume Fade Distance'.downcase
		ATTR_STR_2DFX_LIGHT_CORONA_HDR_MULTIPLIER = 'Corona HDR multiplier'.downcase
		ATTR_STR_2DFX_LIGHT_SHADOW_ALPHA	= 'Shadow Alpha'.downcase
		
		PROP_STR_2DFX_LIGHT_COLOUR			= 'Colour'.downcase
		PROP_STR_2DFX_LIGHT_INTENSITY		= 'Intensity'.downcase
		PROP_STR_2DFX_LIGHT_ATTENUATION_START = 'Attenuation Start'.downcase
		PROP_STR_2DFX_LIGHT_ATTENUATION_END	= 'Attenuation End'.downcase
		PROP_STR_2DFX_LIGHT_IS_SPOT			= 'Is Spot'.downcase
		PROP_STR_2DFX_LIGHT_HOTSPOT			= 'Hotspot'.downcase
		PROP_STR_2DFX_LIGHT_FALLSIZE		= 'Fallsize'.downcase
		PROP_STR_2DFX_LIGHT_SHADOW_MAP		= 'Shadowmap'.downcase
		
		# Flags
		ATTR_STR_2DFX_LIGHT_FADE_BETWEEN_DAY_AND_NIGHT = 'Fade Between Day and Night'.downcase
		ATTR_STR_2DFX_LIGHT_DAY				= 'Day'.downcase
		ATTR_STR_2DFX_LIGHT_NIGHT			= 'Night'.downcase
		ATTR_STR_2DFX_LIGHT_IS_ELECTRIC		= 'Is Electric'.downcase
		ATTR_STR_2DFX_LIGHT_ONLY_FROM_BELOW	= 'Only From Below'.downcase
		ATTR_STR_2DFX_LIGHT_STROBE			= 'Strobe'.downcase
		ATTR_STR_2DFX_LIGHT_PLANE			= 'Plane'.downcase
		ATTR_STR_2DFX_LIGHT_CAST_STATIC_SHADOWS = 'Cast Static Object Shadow'.downcase
		ATTR_STR_2DFX_LIGHT_CAST_DYNAMIC_SHADOWS = 'Cast Dynamic Object Shadow'.downcase
		ATTR_STR_2DFX_LIGHT_CALC_FROM_SUNLIGHT = 'Calc From Sunlight'.downcase
		ATTR_STR_2DFX_LIGHT_ENABLE_BUZZING	= 'Enable Buzzing'.downcase
		ATTR_STR_2DFX_LIGHT_FORCE_BUZZING	= 'Force Buzzing'.downcase
		ATTR_STR_2DFX_LIGHT_VOLUME_DRAWING	= 'Volume Drawing'.downcase
		ATTR_STR_2DFX_LIGHT_NO_SPECULAR		= 'No Specular'.downcase
		ATTR_STR_2DFX_LIGHT_INVERT_CLAMP	= 'Invert Clamp'.downcase		
		
		#--------------------------------------------------------------------------
		# Gta Swayable
		#--------------------------------------------------------------------------		
		ATTR_STR_2DFX_SWAYABLE_BONEID		= 'Attach'.downcase
		ATTR_STR_2DFX_SWAYABLE_LOW_WIND_SPEED = 'Low Wind Sway Speed'.downcase
		ATTR_STR_2DFX_SWAYABLE_LOW_WIND_AMPLITUDE = 'Low Wind Sway Amplitude'.downcase
		ATTR_STR_2DFX_SWAYABLE_HIGH_WIND_SPEED = 'High Wind Sway Speed'.downcase
		ATTR_STR_2DFX_SWAYABLE_HIGH_WIND_AMPLITUDE = 'High Wind Sway Amplitude'.downcase
		
		#--------------------------------------------------------------------------
		# Gta Scrollbars
		#--------------------------------------------------------------------------	
		PARAM_STR_2DFX_SCROLLBARS_TYPE		= 'Type'.downcase
		PARAM_STR_2DFX_SCROLLBARS_HEIGHT	= 'Height'.downcase
		PARAM_STR_2DFX_SCROLLBARS_NUMPOINTS = 'NumPoints'.downcase
		
		#--------------------------------------------------------------------------
		# Gta Ladders
		#--------------------------------------------------------------------------	
		ATTR_STR_2DFX_LADDER_CAN_GET_OFF_AT_TOP = 'Can Get Off At Top'.downcase

		#--------------------------------------------------------------------------
		# Gta TimeCycle
		#--------------------------------------------------------------------------	
		ATTR_STR_TCYC_PERCENTAGE			= 'Percentage'.downcase
		ATTR_STR_TCYC_START_HOUR			= 'StartHour'.downcase
		ATTR_STR_TCYC_END_HOUR				= 'EndHour'.downcase
		ATTR_STR_TCYC_IDSTRING				= 'IDString'.downcase
		ATTR_STR_TCYC_RANGE					= 'Range'.downcase
		
		ATTR_DEFAULT_TCYC_PERCENTAGE		= 0.0
		ATTR_DEFAULT_TCYC_START_HOUR		= 0
		ATTR_DEFAULT_TCYC_END_HOUR			= 23
		ATTR_DEFAULT_TCYC_IDSTRING			= ''
		ATTR_DEFAULT_TCYC_RANGE				= 100.0
		
		#--------------------------------------------------------------------------
		# UNCATEGORISED
		#--------------------------------------------------------------------------	
		
		ATTR_STR_CULL_WATER			= 'Cull Water'.downcase
		ATTR_STR_ADDITIVE			= 'Additive'.downcase
		ATTR_STR_IS_SUBWAY			= 'Is Subway'.downcase
		ATTR_STR_IGNORE_LIGHTING	= 'Ignore Lighting'.downcase
		ATTR_STR_TIME_ON			= 'Time on'.downcase
		ATTR_STR_TIME_OFF			= 'Time off'.downcase
		ATTR_STR_NO_SHADOWS			= 'No Shadows'.downcase
		ATTR_STR_SMASHABLE			= 'Smashable'.downcase
		ATTR_STR_DENSITY			= 'Density'.downcase
		ATTR_STR_LENGTH				= 'Length'.downcase
		ATTR_STR_INTENSITY			= 'Intensity'.downcase
		ATTR_STR_DISTANCE_ON		= 'Distance on'.downcase
		ATTR_STR_DISTANCE_OFF		= 'Distance off'.downcase
		ATTR_STR_TYPE				= 'Type'.downcase
		ATTR_STR_START_TIME			= 'Start Time'.downcase
		ATTR_STR_END_TIME			= 'End Time'.downcase
		ATTR_STR_EFFECT				= 'Effect'.downcase
		ATTR_STR_SPEED				= 'Speed'.downcase
		ATTR_STR_SIZE				= 'Size'.downcase
		ATTR_STR_INTEREST			= 'Interest'.downcase
		ATTR_STR_LOOK_AT			= 'Look At'.downcase
		ATTR_STR_DIRECTION			= 'Direction'.downcase
		ATTR_STR_IS_GARAGE_DOOR		= 'Is Garage Door'.downcase
		ATTR_STR_GROUP				= 'Group'.downcase
		ATTR_STR_SEED				= 'Seed'.downcase
		ATTR_STR_STATUS				= 'Status'.downcase
		ATTR_STR_LOD_PARENT			= 'LOD Parent'.downcase
		ATTR_STR_IS_TREE			= 'Is Tree'.downcase
		ATTR_STR_EXPLODES_WHEN_SHOT	= 'Explodes When Shot'.downcase
		ATTR_STR_ENCLOSED			= 'Enclosed'.downcase
		ATTR_STR_STAIRS				= 'Stairs'.downcase
		ATTR_STR_1ST_PERSON			= '1st Person'.downcase
		ATTR_STR_STOP_RAINING		= 'Stop Raining'.downcase
		ATTR_STR_NO_POLICE			= 'No Police'.downcase
		ATTR_STR_IGNORE				= 'Ignore'.downcase
		ATTR_STR_LOAD_COLLISION		= 'Load Collision'.downcase
		ATTR_STR_CAN_SEE_SUBWAY		= 'Can see subway'.downcase
		ATTR_STR_POLICE_WALK		= 'Police walk'.downcase
		ATTR_STR_IN_ROOM_AUDIO		= 'In room audio'.downcase
		ATTR_STR_FEWER_PEDS			= 'Fewer Peds'.downcase
		ATTR_STR_WANTED_DROP_AMOUNT	= 'Wanted Drop Amount'.downcase
		ATTR_STR_PICKUP_TYPE		= 'Pickup Type'.downcase
		ATTR_STR_ROTATE				= 'Rotate'.downcase
		ATTR_STR_RUBBISH			= 'Rubbish'.downcase
		ATTR_STR_TITLE				= 'Title'.downcase
		ATTR_STR_INVERT_ROTATION	= 'InvertRotation'.downcase
		ATTR_STR_LEAVE_CAMERA		= 'LeaveCamera'.downcase
		ATTR_STR_EXTRA_COLOUR		= 'Extra Colour'.downcase
		ATTR_STR_IS_DOOR			= 'Is Door'.downcase
		ATTR_STR_IS_CRANE			= 'Is Crane'.downcase
		ATTR_STR_SHOP_ENTRANCE		= 'Shop Entrance'.downcase
		ATTR_STR_FADE				= 'Fade'.downcase
		ATTR_STR_LINKED				= 'Linked'.downcase
		ATTR_STR_SHOP_EXIT			= 'Shop Exit'.downcase
		ATTR_STR_LINE1				= 'Linel'.downcase
		ATTR_STR_LINE2				= 'Line2'.downcase
		ATTR_STR_LINE3				= 'Line3'.downcase
		ATTR_STR_LINE4				= 'Line4'.downcase
		ATTR_STR_WARP_CARS			= 'Warp Cars'.downcase
		ATTR_STR_WARP_BIKES			= 'Warp Bikes'.downcase
		ATTR_STR_NUM_LINES			= 'Num Lines'.downcase
		ATTR_STR_NUM_LETTERS		= 'Num Letters'.downcase
		ATTR_STR_DAY_BRIGHTNESS		= 'Day Brightness'.downcase
		ATTR_STR_NIGHT_BRIGHTNESS	= 'Night Brightness'.downcase
		ATTR_STR_DRAW_IN_WET_ONLY	= 'Draw In Wet Only'.downcase
		
		ATTR_STR_INDEX				= 'Index'.downcase
		ATTR_STR_USAGE				= 'Usage'.downcase
		ATTR_STR_SCORE				= 'Score'.downcase
		ATTR_STR_RULE				= 'Rule'.downcase
		ATTR_STR_OBJECT				= 'Object'.downcase
		ATTR_STR_SPACING			= 'Spacing'.downcase
		ATTR_STR_MIN_SCALE			= 'MinScale'.downcase
		ATTR_STR_MAX_SCALE			= 'MaxScale'.downcase
		ATTR_STR_MIN_SCALE_Z		= 'MinScaleZ'.downcase
		ATTR_STR_MAX_SCALE_Z		= 'MaxScaleZ'.downcase
		ATTR_STR_MIN_Z_OFFSET		= 'ZOffsetMin'.downcase
		ATTR_STR_MAX_Z_OFFSET		= 'ZOffsetMax'.downcase
		ATTR_STR_IS_ALIGNED			= 'IsAligned'.downcase
		ATTR_STR_GOING_UP			= 'Going Up'.downcase
		ATTR_STR_IS_TAG				= 'Is Tag'.downcase
		ATTR_STR_OPEN_TIME			= 'Open Time'.downcase
		ATTR_STR_CLOSE_TIME			= 'Close Time'.downcase
		ATTR_STR_WARP_GANG			= 'Warp Gang'.downcase
		ATTR_STR_DO_NOT_WARP		= 'Do not warp'.downcase
		ATTR_STR_SCRIPT				= 'Script'.downcase
		ATTR_STR_VISIBLE			= 'Visible'.downcase
		ATTR_STR_SCREENS_1			= 'Screens 1'.downcase
		ATTR_STR_SCREENS_2			= 'Screens 2'.downcase
		ATTR_STR_SCREENS_3			= 'Screens 3'.downcase
		ATTR_STR_SCREENS_4			= 'Screens 4'.downcase
		ATTR_STR_SCREENS_5			= 'Screens 5'.downcase
		ATTR_STR_SCREENS_6			= 'Screens 6'.downcase
		ATTR_STR_NUM_RANDOM_PEDS	= 'Num Random Peds'.downcase
		ATTR_STR_SOUND				= 'Sound'.downcase
		ATTR_STR_ACTIVE				= 'Active'.downcase
		ATTR_STR_RANDOM_OPEN_TIME	= 'Random Open Time'.downcase
		ATTR_STR_PALETTE_ID			= 'Palette ID'.downcase
		ATTR_STR_BURGLARY_HOUSE		= 'Burglary House'.downcase
		ATTR_STR_TUNNEL_TRANSITION	= 'Tunnel Transition'.downcase
		ATTR_STR_TUNNEL				= 'Tunnel'.downcase
		ATTR_STR_MILITARY_AREA		= 'Military Area'.downcase
		ATTR_STR_HAS_TUNNEL_COLOURS	= 'Has Tunnel Colours'.downcase
		ATTR_STR_EXTRA_AIR_RESISTANCE = 'Extra Air Resistance'.downcase
		ATTR_STR_FEWER_CARS			= 'Fewer Cars'.downcase
		ATTR_STR_IS_TRAFFIC_LIGHT	= 'Is Traffic Light'.downcase
		ATTR_STR_IGNORE_POPULATION_LIMIT = 'Ignore Population Limit'.downcase
		ATTR_STR_ACTIVE_DURING_DAY	= 'Active During Day'.downcase
		ATTR_STR_ACTIVE_DURING_NIGHT = 'Active During Night'.downcase
		
		ATTR_STR_PED_TYPE			= 'ped type'.downcase
		ATTR_STR_START_TIME_OVERRIDE = 'time start override'.downcase
		ATTR_STR_END_TIME_OVERRIDE	= 'time end override'.downcase
		
		ATTR_STR_WORK_TYPE			= 'Work Type'.downcase
		ATTR_STR_HOME				= 'Home'.downcase
		ATTR_STR_LEISURE_TYPE		= 'Leisure Type'.downcase
		ATTR_STR_FOOD_TYPE			= 'Food Type'.downcase
		
		#--------------------------------------------------------------------
		# Attribute/Property/Class Test Functions
		#--------------------------------------------------------------------
		
		def has_no_attr_class?( )
			is_attribute_class?( ATTR_CLASS_NONE )
		end
		
		def has_parent?( )
			( not @parent.nil? )
		end
		
		def has_name?( )
			( @name.is_a?( String ) and ( @name.size > 0 ) )
		end
		
		def export?( )
			return ( not get_attr( ATTR_STR_OBJ_DONT_EXPORT, false ) )
		end
		
		def milo_export?( )
			return ( is_milo? and not get_attr( ATTR_STR_MILO_DONT_EXPORT_MILO, false ) )
		end

		#--------------------------------------------------------------------
		# Max Classes
		#--------------------------------------------------------------------
		
		def is_editable_mesh?( )
			is_max_class?( MAX_CLASS_EDITABLE_MESH )
		end
		
		def is_editable_poly?( )
			is_max_class?( MAX_CLASS_EDITABLE_POLY )
		end
		
		def is_helper?( )
			is_max_superclass?( MAX_SUPERCLASS_HELPER )
		end
		
		def is_internalref?( )
			is_max_class?( MAX_CLASS_INTERNALREF )
		end

		def is_xref?( )
			is_max_class?( MAX_CLASS_XREF )
		end
		
		#--------------------------------------------------------------------
		# Gta Attribute Classes
		#--------------------------------------------------------------------

		def is_block?( )
			is_attribute_class?( ATTR_CLASS_BLOCK )
		end

		def is_cargen?( )
			is_attribute_class?( ATTR_CLASS_CARGEN )
		end
		
		def is_collision?( )
			is_attribute_class?( ATTR_CLASS_COLLISION )
		end

		def is_entryexit?( )
			is_attribute_class?( ATTR_CLASS_ENTRYEXIT )
		end
		
		def is_garagearea?( )
			is_attribute_class?( ATTR_CLASS_GARAGEAREA )
		end
				
		def is_group?( )
			is_attribute_class?( ATTR_CLASS_GROUP )
		end
		
		def is_lodmodifier?( )
			is_attribute_class?( ATTR_CLASS_LODMODIFIER )
		end
		
		def is_milo?( )
			is_attribute_class?( ATTR_CLASS_MILO )
		end
		
		def is_milotri?( )
			is_attribute_class?( ATTR_CLASS_MILOTRI )
		end
		
		def is_mloportal?( )
			is_attribute_class?( ATTR_CLASS_MLOPORTAL )
		end
		
		def is_mloroom?( )
			is_attribute_class?( ATTR_CLASS_MLOROOM )
		end
		
		def is_multibldg?( )
			is_attribute_class?( ATTR_CLASS_MULTIBLDG )
		end
		
		def is_object?( )
			is_attribute_class?( ATTR_CLASS_OBJ )
		end
		
		def is_pickup?( )
			is_attribute_class?( ATTR_CLASS_PICKUP )
		end

		def is_particle?( )
			is_attribute_class?( ATTR_CLASS_GTAPARTICLE )
		end
		
		def is_soundzone?( )
			is_attribute_class?( ATTR_CLASS_SOUNDZONE )
		end
		
		def is_stuntjump?( )
			is_attribute_class?( ATTR_CLASS_STUNTJUMP )
		end
		
		def is_timecycle?( )
			is_attribute_class?( ATTR_CLASS_TIMECYCLE )
		end

		def is_zone?( )
			is_attribute_class?( ATTR_CLASS_ZONE )
		end
				
		#--------------------------------------------------------------------
		# Attribute Value Classes
		#--------------------------------------------------------------------
		
		def is_time_object?( )
			return @attributes[ATTR_STR_OBJ_IS_TIME_OBJECT].value \
				if ( @attributes.has_key?( ATTR_STR_OBJ_IS_TIME_OBJECT ) )
			return false
		end

		def is_anim_object?( )
			return ( 0 != ''.casecmp( @attributes[ATTR_STR_OBJ_ANIM].value ) ) \
				if ( @attributes.has_key?( ATTR_STR_OBJ_ANIM ) )
			return false
		end	
	
		def is_fragment?( )
			return ( @attributes[ATTR_STR_OBJ_IS_FRAGMENT].value ) \
				if ( @attributes.has_key?( ATTR_STR_OBJ_IS_FRAGMENT ) )
			return false
		end
		
		def is_fragment_proxy?( )
			return ( @attributes[ATTR_STR_OBJ_IS_FRAGMENT_PROXY].value ) \
				if ( @attributes.has_key?( ATTR_STR_OBJ_IS_FRAGMENT_PROXY ) )
			return false
		end
		
		def has_audio_material?( )
			return ( 0 != ''.casecmp( @attributes[ATTR_STR_COLLISION_AUDIO_MATERIAL].value ) ) \
				if ( @attributes.has_key?( ATTR_STR_COLLISION_AUDIO_MATERIAL ) )
			return false
		end
		
		#--------------------------------------------------------------------
		# 2dfx Object Test Functions
		#--------------------------------------------------------------------		
		
		def is_2dfx?( )
			regexp_2dfx = /^is_2dfx_/
			
			# Do some Ruby magic to invoke each 'is_2dfx_*?' method returning
			# true if any return true, false otherwise.  This will ensure
			# this function is valid when 2dfx types are added/removed.
			methods.each do |method|
				return true if ( ( method =~ regexp_2dfx ) and send( method.to_sym ) )
			end
			false
		end
		
		def is_2dfx_audio_emitter?( )
			is_attribute_class?( ATTR_CLASS_2DFX_AUDIO_EMITTER )
		end
		
		def is_2dfx_light_shaft?( )
			is_attribute_class?( ATTR_CLASS_2DFX_LIGHT_SHAFT )
		end
		
		def is_2dfx_proc_object?( )
			is_attribute_class?( ATTR_CLASS_2DFX_PROC_OBJECT )
		end
		
		def is_2dfx_walk_dont_walk?( ) 
			is_attribute_class?( ATTR_CLASS_2DFX_WALK_DONT_WALK )
		end
		
		def is_2dfx_script?( )
			is_attribute_class?( ATTR_CLASS_2DFX_SCRIPT )
		end
		
		def is_2dfx_ladder?( )
			is_attribute_class?( ATTR_CLASS_2DFX_LADDER )
		end
		
		def is_2dfx_ledge?( )
			is_attribute_class?( ATTR_CLASS_2DFX_LEDGE )
		end
		
		def is_2dfx_spawn_point?( )
			is_attribute_class?( ATTR_CLASS_2DFX_SPAWN_POINT )
		end
		
		def is_2dfx_scrollbars?( )
			is_attribute_class?( ATTR_CLASS_2DFX_SCROLLBARS )
		end
		
		def is_2dfx_buoyancy?( )
			is_attribute_class?( ATTR_CLASS_2DFX_BUOYANCY )
		end
		
		def is_2dfx_light_effect?( )
			is_attribute_class?( [ATTR_CLASS_2DFX_LIGHT, ATTR_CLASS_2DFX_LIGHT_PHOTO] )
		end
		
		def is_2dfx_particle_effect?( )
			is_attribute_class?( ATTR_CLASS_2DFX_PARTICLE_EFFECT )
		end
		
		def is_2dfx_explosion_effect?( )
			is_attribute_class?( ATTR_CLASS_2DFX_EXPLOSION_EFFECT )
		end
		
		def is_2dfx_swayable_effect?( )
			is_attribute_class?( ATTR_CLASS_2DFX_SWAYABLE_EFFECT )
		end
		
		#--------------------------------------------------------------------
		# Milo Helper Functions
		#--------------------------------------------------------------------		
		
		#
		# Return true iff the object has one or more mloportal children.
		#
		def has_mloportal?( )
			@children.each do |child|
				return ( true ) if ( child.is_mloportal? )
			end
			false
		end
		
		#
		# Return number of mloportal children.
		#
		def num_mloportals( )
			portals = 0
			@children.each do |child|
				portals += 1 if ( child.is_mloportal? )	
			end
			portals
		end
		
		#
		# Return true iff the object has one or more mloroom children.
		#
		def has_mloroom?( )
			@children.each do |child|
				return ( true ) if ( child.is_mloroom? )
			end
			false
		end
		
		#
		# Return number of mloroom children.
		#
		def num_mlorooms( )
			rooms = 0
			@children.each do |child|
				rooms += 1 if ( child.is_mloroom? )
			end
			rooms
		end
		
		#
		# Return number of mlo child objects (recursive).
		#
		def num_mloobjects( node = nil )
			node = self if ( node.nil? )
			
			obj_count = 0
			node.children.each do |child|
				
				if ( child.is_mloportal? ) then
					obj_count += num_mloobjects( child )
				elsif ( child.is_mloroom? ) then
					obj_count += num_mloobjects( child )
				else
					obj_count += 1
				end
			end
			obj_count
		end
		
		#
		# Fetch attribute from object, returning default if its not available.
		#
		def get_attr( name, default_value )
			return @attributes[name].value if ( @attributes.has_key?( name ) )
			return default_value
		end
		
		#
		# Fetch parameter from object, returning default if its not available.
		#
		def get_param( name, default_value )
			return @parameters[name].value if ( @parameters.has_key?( name ) )
			return default_value
		end
		
		#
		# Fetch property from object, returning default if its not available.
		#
		def get_prop( name, default_value )
			return @properties[name].value if ( @properties.has_key?( name ) )
			return default_value
		end
		
		#--------------------------------------------------------------------
		# Private Methods
		#--------------------------------------------------------------------
	private
		#
		# Return true iff this object is of the specified Attribute class.
		#
		def is_attribute_class?( classes )
			return ( classes == @attribute_class ) if ( classes.is_a?( String ) )
			
			classes.each do |klass|
				return true if ( 0 == klass.casecmp( @attribute_class ) )
			end
			false
		end
		
		#
		# Return true iff this object is of the specified 3dsmax class.
		#
		def is_max_class?( classes )
			return ( classes == @classname ) if ( classes.is_a?( String ) )
			
			classes.each do |klass|
				return true if ( 0 == klass.casecmp( @classname ) )
			end
			false
		end
		
		#
		# Return true iff this object is of the specified 3dsmax superclass.
		#
		def is_max_superclass?( classes )
			return ( classes == @superclass ) if ( classes.is_a?( String ) )
			
			classes.each do |klass|
				return true if ( 0 == klass.casecmp( @superclass ) )
			end
			false
		end
	end
	
end # SceneXml module
end # FileFormats module
end # Pipeline module

# scenexml_attrs.rb
