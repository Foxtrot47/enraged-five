#
# File:: pipeline/gui/password_dialog.rb
# Description:: Generic password dialog.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Author:: Greg Smith <greg@rockstarnorth.com>
# Date:: 21 November 2008
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/gui/application'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module GUI
	
	class PasswordDialog < Wx::Dialog

		attr_reader :txt_password
	
		def initialize( title = TITLE_DEFAULT, prompt = PROMPT_DEFAULT )
				
			super(nil, Wx::ID_ANY, title, Wx::DEFAULT_POSITION, Wx::Size.new(300,100))
			
			stc_password = Wx::StaticText.new(self, Wx::ID_ANY, "password:")
			@txt_password = Wx::TextCtrl.new(self, Wx::ID_ANY)

			items_sizer = Wx::BoxSizer.new(Wx::HORIZONTAL)
			items_sizer.add(stc_password)
			items_sizer.add(txt_password, 1, Wx::GROW)
			
			main_sizer = Wx::BoxSizer.new(Wx::VERTICAL)
			main_sizer.add(items_sizer, 0, Wx::ALL | Wx::GROW, 5)
			main_sizer.add(create_separated_button_sizer(Wx::OK | Wx::CANCEL), 0, Wx::ALL | Wx::GROW, 5)

			set_sizer main_sizer
		end
	
		def PasswordDialog::show_dialog( title = TITLE_DEFAULT, prompt = PROMPT_DEFAULT )
		
			app = GUI::Application::instance( )
		
			output = ""
		
			app.do { |app|
		
				dlg = PasswordDialog::new( title, prompt )
				ret = dlg.show_modal()
				
				case ret
				when Wx::ID_OK
					output = dlg.txt_password.get_value
				else
					nil
				end
			}
			
			output
		end
	end
	
end # GUI module
end # Pipeline module

if ( __FILE__ == $0 ) then
	
	#-----------------------------------------------------------------------------
	# Entry-Point
	#-----------------------------------------------------------------------------
	password = Pipeline::GUI::PasswordDialog.show_dialog( 'Title', 'Prompt' )
	puts "PASSWORD: #{password}"
end

# pipeline/gui/password_dialog.rb
