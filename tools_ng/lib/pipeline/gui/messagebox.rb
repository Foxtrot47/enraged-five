#
# File:: pipeline/gui/messagebox.rb
# Description:: Generic message box utility methods.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Author:: Greg Smith <greg@rockstarnorth.com>
# Date:: 25 November 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/globals'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module GUI

	#
	# == Description
	# Provides your Ruby scripts with generic messgae box popups.  The popups
	# are invoked by a .Net executable (called dialog.exe).  This reduces the
	# requirements on custom widgets sets as Ruby gems.
	#
	class MessageBox
	
		YES		= true
		NO		= false
	
		def MessageBox::error( title, message, buttons = [] )
			
			Pipeline::Globals::instance( ).in_env do |e|
				command = "#{DIALOG} --icon error --buttons Ok \"#{message}\" \"#{title}\""
				command = e.subst( command )
				system( command )
			end
		end
		
		def MessageBox::information( title, message, buttons = [] )
			
			Pipeline::Globals::instance( ).in_env do |e|
				command = "#{DIALOG} --icon information --buttons Ok \"#{message}\" \"#{title}\""
				command = e.subst( command )
				system( command )
			end
		end
		
		def MessageBox::question( title, message, buttons = [] )
			
			Pipeline::Globals::instance( ).in_env do |e|
				command = "#{DIALOG} --icon question --buttons YesNo \"#{message}\" \"#{title}\""
				command = e.subst( command )
				system( command )
			end
		end
		
		def MessageBox::warning( title, message, buttons = [] )
		
			Pipeline::Globals::instance( ).in_env do |e|
				command = "#{DIALOG} --icon exclamation --buttons Ok \"#{message}\" \"#{title}\""
				command = e.subst( command )
				system( command )
			end
		end
		
		# Define aliases for Class methods
		class << self
			alias_method :critical, :error
			alias_method :info, :information
		end
	
		#--------------------------------------------------------------------
		# Private
		#--------------------------------------------------------------------
	private
		DIALOG = '$(toolsbin)/dialog/dialog.exe'
	end
end # GUI module
end # Pipeline module

if ( __FILE__ == $0 ) then
	
	Pipeline::GUI::MessageBox::error( 'blah title', 'test error dude' )
	Pipeline::GUI::MessageBox::information( 'blah title', 'test information' )
	res = Pipeline::GUI::MessageBox::question( 'blah title', 'test question?' )
	puts "Result: #{res}"
	
	Pipeline::GUI::MessageBox::warning( 'blah title', 'test warning dude' )
	
	# Alias	
	Pipeline::GUI::MessageBox::critical( 'blah title', 'test critical error dude' )
	Pipeline::GUI::MessageBox::info( 'blah title', 'test info dude' )
end

# pipeline/gui/messagebox.rb
