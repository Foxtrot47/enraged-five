#
# File:: pipeline/gui/exception_dialog.rb
# Description:: Generic Ruby exception dialog.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Author:: Greg Smith <greg@rockstarnorth.com>
# Date:: 26 September 2008
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module GUI

	#
	# == Description
	# Provides your Ruby scripts with a generic Exception Dialog.  The dialog
	# is invoked by a .Net executable (called dialog.exe).  This reduces the
	# requirements on custom widgets sets as Ruby gems.
	#
	class ExceptionDialog
	
		#
		# Show an exception dialog for the specified exception; message, 
		# author and email are optional but highly recommended.
		#
		def ExceptionDialog::show_dialog( ex, message = 'Unhandled exception', author = '', email = '' )
		
			c = Pipeline::Config::instance( )
			version = 'Unknown'
			if ( ex.backtrace[0] =~ /^([A-Z]{1}\:[A-Z0-9_\\\/\.]+):[0-9]+\:/i ) then
				script = $1
			end
			begin
				p4 = SCM::Perforce::new( )
				Dir::chdir( OS::Path::get_directory( script ) ) do
					p4.connect( )
					fstat = p4.run_fstat( script ).shift

          if fstat then
          
            version = "p4:#{fstat['depotFile']}##{fstat['haveRev']}" \
              if ( fstat.has_key?( 'haveRev' ) )
          end

					p4.disconnect( )
				end
			rescue Exception => ex2
				# Ignore.
			end
			
			author = 'RSGEDI Tools' if ( author.nil? or author.empty? )
			email = '*tools@rockstarnorth.com' if ( email.nil? or email.empty? )
			fixedmsg = ex.message.tr("\n", "|")
			command = "#{DIALOG} #{ARGUMENTS} \"#{message.strip}\" \"#{ex.class}\" \"#{fixedmsg}||#{ex.backtrace.join('|')}\" \"#{script}\"  \"#{version}\"  \"#{author}\" \"#{email}\""
			command = c.envsubst( command )
			puts "COMMAND: #{command}"
			system( command )
		end
		
		#--------------------------------------------------------------------
		# Private
		#--------------------------------------------------------------------
	private
		DIALOG = '$(toolsbin)/dialog/dialog.exe'
		ARGUMENTS = '--special exception'
	end

end # GUI module
end # Pipeline module

if ( __FILE__ == $0 ) then

	begin
		throw RuntimeError.new( 'Testing testing 123...' )
	rescue Exception => ex
		Pipeline::GUI::ExceptionDialog.show_dialog( ex, 'Unhandled exception', 'David Muir', 'david.muir@rockstarnorth.com' )
	end
end

# pipeline/gui/exception_dialog.rb
