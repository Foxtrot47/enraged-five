#
# File:: %RS_TOOLSLIB%/pipeline/gui/asset_dependency_dialog.rb
# Description:: Asset dependency dialog.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 21 March 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/globals'
require 'pipeline/os/start'
include Pipeline

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module GUI

	#
	# == Description
	# Provides your Ruby scripts with a generic Asset Dependency dialog.  
	# The dialog is invoked by a .Net executable (called dialog.exe).  This 
	# reduces the requirements on custom widgets sets as Ruby gems.
	#
	# The filename passed in should just be a plain text file with a list of
	# assets.  This will be expanded in future to XML to support multiple
	# lists of assets (e.g. required vs optional).
	#
	# == Example Usage
	#
	#   result = GUI::AssetDependencyDialog::show_dialog( "c:\\test.txt" )
	#   case result
	#   when AssetDependencyDialog::SYNC_AND_CONTINUE
	#      ...
	#   when AssetDependencyDialog::CONTINUE
	#      ...
	#   when AssetDependencyDialog::CANCEL
	#      ...
	#   end
	#
	class AssetDependencyDialog	
		SYNC_AND_CONTINUE = 0
		CONTINUE = 1
		CANCEL = 2
	
		#
		# Show an exception dialog for the specified exception; message, 
		# author and email are optional but highly recommended.
		#
		def AssetDependencyDialog::show_dialog( filename )
		
			command = "#{DIALOG} #{ARGUMENTS} #{filename}"
			command = Globals::instance().in_env do |e|
				e.subst( command )
			end
			status, out, err = OS::start( command )
			status.exitstatus
		end
		
		#--------------------------------------------------------------------
		# Private
		#--------------------------------------------------------------------
	private
		DIALOG = '$(toolsbin)/dialog/dialog.exe'
		ARGUMENTS = '--special asset_dependencies'
	end

end # GUI module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/gui/asset_dependency_dialog.rb
