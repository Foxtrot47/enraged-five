	#
# File:: pipeline/gui/application.rb
# Description:: Qt::Application singleton wrapper.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Author:: Greg Smith <greg@rockstarnorth.com>
# Date:: 26 November 2009
#

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
require 'singleton'
require 'wx'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module GUI
	
	class MyApp < Wx::App
	
		attr_writer :b
	
		def on_init

			@b.call
			exit_main_loop()
		end
	end
	
	#
	# == Description
	# Using this class to initialise the Qt toolkit ensures that only one
	# Qt::Application object is created per process.
	#
	class Application
		include Singleton
		
		# Qt::Application instance.
		attr_reader :app

		def initialize( &block )
		end
	
		def do( &block )

			if @app == nil then
		
				@app = MyApp.new	
				@app.b = block
				@app.main_loop		
			else
				
				block.call
			end
		end
	
		# Forward non-resolved methods to our Qt::Application object.
		def method_missing( sym, *args )
		
			#@app.send( sym, *args )
		end
	end
	
end # GUI module
end # Pipeline module

# pipeline/gui/application.rb
