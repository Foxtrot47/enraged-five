#
# File:: binary_stats.rb
# Description:: 
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 14th September 2010
#
# Passed in  :-  see OPTIONS ...
# Passed out :-  stderr contains all errors 
#                stdout for all other output. 
# Returns    :-  returns non zero upon detecting any errors 

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/file'
include Pipeline
require 'systemu'
require 'rexml/document'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ "--help", 		"-h", 	OS::Getopt::BOOLEAN, 	"display usage information." ],
]

def xml_cmp a, b
    a = REXML::Document.new(a.to_s)
    b = REXML::Document.new(b.to_s)

    normalized = Class.new(REXML::Formatters::Pretty) do
      def write_text(node, output)
	super(node.to_s.strip, output)
      end
    end
    
    normalized.new(indentation=0,ie_hack=false).write(node=a, a_normalized='')
    normalized.new(indentation=0,ie_hack=false).write(node=b, b_normalized='')

    a_normalized == b_normalized
end

def xml_diff a, b

    a = REXML::Document.new(a.to_s)
    b = REXML::Document.new(b.to_s)
        
    REXML::XPath.each(a, "//project/platform" ) do |platform_a| 
    	platform_name_a = platform_a.attributes["name"]
    	
    	puts "\t#{platform_name_a}"
	REXML::XPath.each(b, "//project/platform" ) do |platform_b| 
		platform_name_b = platform_b.attributes["name"]
		if platform_name_a == platform_name_b
			REXML::XPath.each(platform_a, "target") do |target_a|
				target_name_a = target_a.attributes["name"]
				puts "\t\t#{target_name_a}"
				REXML::XPath.each(platform_b, "target") do |target_b|
					target_name_b = target_b.attributes["name"]
					if (target_name_a == target_name_b)					
						puts "\t\t\tMatch platform #{platform_name_b} target #{target_name_b}"
						xpaths = [ 	"config/@output_path", 
								"config/forceincludes/forceinclude", 
								"config/defines", 
								"config/options", 
								"config/vc/VCCLCompilerTool", 
								"config/vc/ClCompile/Optimization", 
								"config/vc/ClCompile/InlineFunctionExpansion",
								"config/vc/ClCompile/OmitFramePointers",
								"config/vc/ClCompile/StringPooling",
								"config/vc/ClCompile/FunctionLevelLinking",								
								"config/vc/ClCompile/RuntimeLibrary",
								"config/vc/ClCompile/BufferSecurityCheck",
								"config/vc/ClCompile/ForceConformanceInForLoopScope",
								"config/vc/ClCompile/RuntimeTypeInfo",
								"config/vc/ClCompile/ProgramDataBaseFileName",
								"config/vc/ClCompile/WarningLevel",								
								"config/vc/ClCompile/DebugInformationFormat",
								"config/vc/ClCompile/CompileAs"]
								
								# "config/vc/ClCompile/TreatWarningAsError",
								#"config/vc/ClCompile/ExceptionHandling",
						xpaths.each do |xpath|
							REXML::XPath.each(target_a,xpath) do |target_a_node4|
								REXML::XPath.each(target_b,xpath) do |target_b_node4|
									puts "xpath #{xpath}\n\n\nA: #{target_a_node4}\n\n\n\n\n\n\n\nB: #{target_b_node4}\n\n\n" if target_a_node4.to_s != target_b_node4.to_s
								end
							end
						end
						
						#puts "\n#{target_a}\n#{target_b}\n" unless target_a.to_s == target_b.to_s						
#						REXML::XPath.each(target_a,"descendant-or-self::*") do |target_a_node3|							
#							puts "\t\t\t\t#{target_a_node3.name}"
#							REXML::XPath.each(target_a_node3,"@*") do |target_a_node4|
#								str = "\t\t\t\t\t#{target_a_node4.name} = #{target_a_node4.value}"
#								puts str
#							end
#						end
					end
				end				
			end						
		end
	end
    end 
end



#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------

begin
	g_AppName = File::basename( __FILE__, '.rb' )
	g_ProjectName = ''
	g_BranchName = ''
	g_Project = nil
	g_Config = Pipeline::Config.instance()

	#---------------------------------------------------------------------
	# --- PARSER COMMAND LINE                                          --- 
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] )
		puts OS::Getopt.usage( OPTIONS )
		puts ("Press Enter to continue...")
		$stdin.getc( )				
		Process.exit!( 1 )
	end	

	xmldoc_a = REXML::Document.new File.open( trailing[0], 'r' )
	xmldoc_b = REXML::Document.new File.open( trailing[1], 'r' )

	puts("Comparing #{trailing[0]} with #{trailing[1]}")
	equal = xml_cmp(xmldoc_a,xmldoc_b)
	
	if not equal
		puts("Documents differ") unless equal
		xml_diff(xmldoc_a,xmldoc_b)
	end			
	
	Process.exit! 0

rescue Exception => ex
	$stderr.puts "Error: Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	Process.exit! -1
end
