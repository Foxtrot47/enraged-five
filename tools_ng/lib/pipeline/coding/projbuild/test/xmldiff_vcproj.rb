#
# File:: binary_stats.rb
# Description:: 
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 14th September 2010
#
# Passed in  :-  see OPTIONS ...
# Passed out :-  stderr contains all errors 
#                stdout for all other output. 
# Returns    :-  returns non zero upon detecting any errors 

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/file'
include Pipeline
require 'systemu'
require 'xml'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ "--help", 		"-h", 	OS::Getopt::BOOLEAN, 	"display usage information." ],
]

def xml_cmp a, b
    a = REXML::Document.new(a.to_s)
    b = REXML::Document.new(b.to_s)

    normalized = Class.new(REXML::Formatters::Pretty) do
      def write_text(node, output)
	super(node.to_s.strip, output)
      end
    end
    
    normalized.new(indentation=0,ie_hack=false).write(node=a, a_normalized='')
    normalized.new(indentation=0,ie_hack=false).write(node=b, b_normalized='')

    a_normalized == b_normalized
end

def compare_attribute(node_a, node_b, attr)
	a = node_a.attributes[attr]		 			 	
	b = node_b.attributes[attr]
	
	if not (a and b)
#		$stderr.puts("\t\tERROR: Invalid attribute #{attr}") 
		return true
	end	
	
	if a != b
		$stderr.puts("\t\tERROR: Inequality : #{a} #{b}") 
		return false
	end
	true
end

def xml_diff a, b
 
 	root_a = a.find_first("//VisualStudioProject/Configurations")			
 	root_b = b.find_first("//VisualStudioProject/Configurations")			
 	if (root_a and root_b)
	 	nodes_a = root_a.find("Configuration") # each being a unit of work.	
	 	nodes_a.each do |node_a|
	 		config_name = node_a.attributes["Name"]
	 		
	 		if config_name.include?"SN PS3 SNC "
	 			config_name = config_name.gsub("SN PS3 SNC ","")
	 			config_name = config_name.gsub("|Win32","|PS3")
	 		end

	 		if config_name.include?"|PS3"
	 			config_name = "SN PS3 SNC #{config_name}"
	 			config_name = config_name.gsub("|PS3","|Win32")
	 		end

	 		
	 		puts config_name
		 	node_b = root_b.find_first("Configuration[@Name='#{config_name}']")	
		 	if (not node_b)
		 		puts "\t NO MATCH for Configuration[@Name='#{config_name}']"
		 		next
		 	end
		 	puts "\tmatched #{node_b.attributes["Name"]}" if node_b
		 	
		 	#puts node_b
				
			compare_attribute(node_a, node_b, "Name")
			compare_attribute(node_a, node_b, "OutputDirectory")
			compare_attribute(node_a, node_b, "IntermediateDirectory")
			compare_attribute(node_a, node_b, "ConfigurationType")
			compare_attribute(node_a, node_b, "InheritedPropertySheets")
			compare_attribute(node_a, node_b, "CharacterSet")
			
			a_VCCLCompilerTool = node_a.find_first("Tool[@Name='VCCLCompilerTool']")
			b_VCCLCompilerTool = node_b.find_first("Tool[@Name='VCCLCompilerTool']")

			if (a_VCCLCompilerTool and b_VCCLCompilerTool)
				a = a_VCCLCompilerTool
				b = b_VCCLCompilerTool
				if (a and b)
					compare_attribute(a, b, "Name")
					compare_attribute(a, b, "Optimization")
					compare_attribute(a, b, "AdditionalIncludeDirectories")
					compare_attribute(a, b, "PreprocessorDefinitions")
					compare_attribute(a, b, "MinimalRebuild")
					compare_attribute(a, b, "ExceptionHandling")
					compare_attribute(a, b, "BasicRuntimeChecks")
					compare_attribute(a, b, "RuntimeLibrary")
					compare_attribute(a, b, "EnableFunctionLevelLinking")
					compare_attribute(a, b, "ForceConformanceInForLoopScope")
					compare_attribute(a, b, "RuntimeTypeInfo")
					compare_attribute(a, b, "UsePrecompiledHeader")
					compare_attribute(a, b, "ProgramDataBaseFileName")
					compare_attribute(a, b, "WarningLevel")
					compare_attribute(a, b, "WarnAsError")
					compare_attribute(a, b, "Detect64BitPortabilityProblems")
					compare_attribute(a, b, "DebugInformationFormat")
					compare_attribute(a, b, "CompileAs")
					compare_attribute(a, b, "ForcedIncludeFiles")
				else
					$stderr.puts("Error: WTF")
				end
			else
				#$stderr.puts("Error: Missing node VCCLCompilerTool #{config_name}")
			end	

			a_VCCLX360CompilerTool = node_a.find_first("Tool[@Name='VCCLX360CompilerTool']")
			b_VCCLX360CompilerTool = node_b.find_first("Tool[@Name='VCCLX360CompilerTool']")

			if (a_VCCLX360CompilerTool and b_VCCLX360CompilerTool)
				a = a_VCCLX360CompilerTool
				b = b_VCCLX360CompilerTool
				if (a and b)
					compare_attribute(a, b, "Name")
					compare_attribute(a, b, "AdditionalOptions")
					compare_attribute(a, b, "Optimization")
					compare_attribute(a, b, "AdditionalIncludeDirectories")
					compare_attribute(a, b, "PreprocessorDefinitions")
					compare_attribute(a, b, "MinimalRebuild")
					compare_attribute(a, b, "ExceptionHandling")
					compare_attribute(a, b, "RuntimeLibrary")
					compare_attribute(a, b, "BufferSecurityCheck")
					compare_attribute(a, b, "EnableFunctionLevelLinking")
					compare_attribute(a, b, "ForceConformanceInForLoopScope")
					compare_attribute(a, b, "RuntimeTypeInfo")
					compare_attribute(a, b, "UsePrecompiledHeader")
					compare_attribute(a, b, "ProgramDataBaseFileName")
					compare_attribute(a, b, "WarningLevel")
					compare_attribute(a, b, "WarnAsError")
					compare_attribute(a, b, "DebugInformationFormat")
					compare_attribute(a, b, "CompileAs")
					compare_attribute(a, b, "ForcedIncludeFiles")
				else
					$stderr.puts("Error: WTF")
				end
			else
				#$stderr.puts("Error: Missing node VCCLX360CompilerTool #{config_name}")
			end	
				
				
			a_VCPreLinkEventTool = node_a.find_first("Tool[@Name='VCPreLinkEventTool']")
			b_VCPreLinkEventTool = node_b.find_first("Tool[@Name='VCPreLinkEventTool']")

			if (a_VCPreLinkEventTool and b_VCPreLinkEventTool)
				a = a_VCPreLinkEventTool
				b = b_VCPreLinkEventTool
				if (a and b)
					compare_attribute(a, b, "Name")
					compare_attribute(a, b, "Description")
					compare_attribute(a, b, "CommandLine")
				else
					$stderr.puts("Error: WTF")
				end
			else
				#$stderr.puts("Error: Missing node VCCLX360CompilerTool #{config_name}")
			end	


			a_VCX360LinkerTool = node_a.find_first("Tool[@Name='VCX360LinkerTool']")
			b_VCX360LinkerTool = node_b.find_first("Tool[@Name='VCX360LinkerTool']")

			if (a_VCX360LinkerTool and b_VCX360LinkerTool)
				a = a_VCX360LinkerTool
				b = b_VCX360LinkerTool
				if (a and b)
					compare_attribute(a, b, "Name")
					compare_attribute(a, b, "AdditionalDependencies")
					compare_attribute(a, b, "OutputFile")
					compare_attribute(a, b, "AdditionalLibraryDirectories")
					compare_attribute(a, b, "LinkIncremental")
					compare_attribute(a, b, "IgnoreDefaultLibraryNames")
					compare_attribute(a, b, "GenerateDebugInformation")
					compare_attribute(a, b, "ProgramDatabaseFile")
					compare_attribute(a, b, "GenerateMapFile")
					compare_attribute(a, b, "EnableCOMDATFolding")
				else
					$stderr.puts("Error: WTF")
				end
			else
				#$stderr.puts("Error: Missing node VCX360LinkerTool #{config_name}")
			end	

			a_VCX360ImageTool = node_a.find_first("Tool[@Name='VCX360ImageTool']")
			b_VCX360ImageTool = node_b.find_first("Tool[@Name='VCX360ImageTool']")

			if (a_VCX360ImageTool and b_VCX360ImageTool)
				a = a_VCX360ImageTool
				b = b_VCX360ImageTool
				if (a and b)
					compare_attribute(a, b, "Name")
					compare_attribute(a, b, "OutputFileName")
					compare_attribute(a, b, "AdditionalSections")
					compare_attribute(a, b, "TitleID")
					compare_attribute(a, b, "ProjectDefaults")
				else
					$stderr.puts("Error: WTF")
				end
			else
				#$stderr.puts("Error: Missing node VCX360ImageTool #{config_name}")
			end					

			a_VCX360DeploymentTool = node_a.find_first("Tool[@Name='VCX360DeploymentTool']")
			b_VCX360DeploymentTool = node_b.find_first("Tool[@Name='VCX360DeploymentTool']")

			if (a_VCX360DeploymentTool and b_VCX360DeploymentTool)
				a = a_VCX360DeploymentTool
				b = b_VCX360DeploymentTool
				if (a and b)
					compare_attribute(a, b, "Name")
					compare_attribute(a, b, "RemoteRoot")
					compare_attribute(a, b, "DeploymentFiles")
					compare_attribute(a, b, "DeploymentType")
				else
					$stderr.puts("Error: WTF")
				end
			else
				#$stderr.puts("Error: Missing node VCX360ImageTool #{config_name}")
			end												
			
			a_VCPostBuildEventTool = node_a.find_first("Tool[@Name='VCPostBuildEventTool']")
			b_VCPostBuildEventTool = node_b.find_first("Tool[@Name='VCPostBuildEventTool']")				

			if (a_VCPostBuildEventTool and b_VCPostBuildEventTool)
				a = a_VCPostBuildEventTool
				b = b_VCPostBuildEventTool
				if (a and b)
					compare_attribute(a, b, "Name")
					compare_attribute(a, b, "Description")
					compare_attribute(a, b, "CommandLine")
				else
					$stderr.puts("Error: WTF")
				end
			else
				#$stderr.puts("Error: Missing node VCPostBuildEventTool #{config_name}")
			end															
			
			
			a_VCLinkerTool = node_a.find_first("Tool[@Name='VCLinkerTool']")
			b_VCLinkerTool = node_b.find_first("Tool[@Name='VCLinkerTool']")

			if (a_VCLinkerTool and b_VCLinkerTool)
				a = a_VCLinkerTool
				b = b_VCLinkerTool
				if (a and b)
					compare_attribute(a, b, "Name")
					compare_attribute(a, b, "AdditionalOptions")
					compare_attribute(a, b, "AdditionalDependencies")
					compare_attribute(a, b, "OutputFile")
					compare_attribute(a, b, "AdditionalLibraryDirectories")
					compare_attribute(a, b, "LinkIncremental")
					compare_attribute(a, b, "GenerateDebugInformation")
					compare_attribute(a, b, "GenerateMapFile")
					compare_attribute(a, b, "SubSystem")
					compare_attribute(a, b, "OptimizeReferences")
					compare_attribute(a, b, "TargetMachine")
				else
					$stderr.puts("Error: WTF")
				end
			else
				#$stderr.puts("Error: Missing node VCLinkerTool #{config_name}")
			end								
	 	end
 	end
end

#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------

begin
	g_AppName = File::basename( __FILE__, '.rb' )
	g_ProjectName = ''
	g_BranchName = ''
	g_Project = nil
	g_Config = Pipeline::Config.instance()

	#---------------------------------------------------------------------
	# --- PARSER COMMAND LINE                                          --- 
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] )
		puts OS::Getopt.usage( OPTIONS )
		puts ("Press Enter to continue...")
		$stdin.getc( )				
		Process.exit!( 1 )
	end	

	if not File::file? trailing[0]			
		$stderr.puts "File not found #{trailing[0]}"
		Process.exit! -1
	end

	if not File::file? trailing[1]			
		$stderr.puts "File not found #{trailing[1]}"
		Process.exit! -1
	end

	xmldoc_a = LibXML::XML::Document::file( trailing[0] )
	xmldoc_b = LibXML::XML::Document::file( trailing[1] )

	xml_diff(xmldoc_a,xmldoc_b)
	
	Process.exit! 0

rescue Exception => ex
	$stderr.puts "Error: Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	Process.exit! -1
end
