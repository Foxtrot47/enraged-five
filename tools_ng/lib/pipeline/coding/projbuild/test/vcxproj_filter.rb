#
# File:: binary_stats.rb
# Description:: 
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 14th September 2010
#
# Passed in  :-  see OPTIONS ...
# Passed out :-  stderr contains all errors 
#                stdout for all other output. 
# Returns    :-  returns non zero upon detecting any errors 

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/file'
include Pipeline
require 'systemu'
require 'rexml/document'
require 'xml'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ "--help", 		"-h", 	OS::Getopt::BOOLEAN, 	"display usage information." ],
]

def vcxproj_filter(xmldoc, config, platform) 
 	build_config = "#{config}|#{platform}"
 	puts build_config

 	projectConfigurations = xmldoc.find("//Project/ItemGroup[@Label='ProjectConfigurations']/ProjectConfiguration[@Include!='#{build_config}']")
 	projectConfigurations.each { |node| node.remove! }
 	
 	projectConfigurations = xmldoc.find("//Project/ItemGroup[@Label='ProjectConfigurations']") 		
	projectConfigurations.each { |pc| puts pc }

	# <PropertyGroup Label="Configuration" Condition="'$(Configuration)|$(Platform)'=='Debug|PS3'">			
	propertyGroups = xmldoc.find("//Project/PropertyGroup[not(contains(@Condition, concat(\"'\",\'#{build_config}\',\"'\")))]")
 	propertyGroups.each { |node| node.remove! }	
	
 	propertyGroups = xmldoc.find("//Project/PropertyGroup") 		
	propertyGroups.each { |pg| puts pg }
	
	#<ImportGroup Label="PropertySheets" Condition="'$(Configuration)|$(Platform)'=='Debug|PS3'">
	importGroups = xmldoc.find("//Project/ImportGroup[not(contains(@Condition, concat(\"'\",\'#{build_config}\',\"'\")))]")
 	importGroups.each { |node| node.remove! }	
	
 	importGroups = xmldoc.find("//Project/ImportGroup") 		
	importGroups.each { |pg| puts pg }
	
	#<ItemDefinitionGroup Condition="'$(Configuration)|$(Platform)'=='Debug|PS3'">
	itemDefinitionGroups = xmldoc.find("//Project/ItemDefinitionGroup[not(contains(@Condition, concat(\"'\",\'#{build_config}\',\"'\")))]")
 	itemDefinitionGroups.each { |node| node.remove! }	
	
 	itemDefinitionGroups = xmldoc.find("//Project/ItemDefinitionGroup") 		
	itemDefinitionGroups.each { |pg| puts pg }
	
	customBuildCommands = xmldoc.find("//Project/ItemGroup/CustomBuild/Command[not(contains(@Condition, concat(\"'\",\'#{build_config}\',\"'\")))]")
 	customBuildCommands.each { |node| node.remove! }		

	customBuildOutputs = xmldoc.find("//Project/ItemGroup/CustomBuild/Outputs[not(contains(@Condition, concat(\"'\",\'#{build_config}\',\"'\")))]")
 	customBuildOutputs.each { |node| node.remove! }		

	customBuildAdditionalInputs = xmldoc.find("//Project/ItemGroup/CustomBuild/AdditionalInputs[not(contains(@Condition, concat(\"'\",\'#{build_config}\',\"'\")))]")
 	customBuildAdditionalInputs.each { |node| node.remove! }		

	customBuildMessages = xmldoc.find("//Project/ItemGroup/CustomBuild/Message[not(contains(@Condition, concat(\"'\",\'#{build_config}\',\"'\")))]")
 	customBuildMessages.each { |node| node.remove! }		
end

#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------

begin
	g_AppName = File::basename( __FILE__, '.rb' )
	g_ProjectName = ''
	g_BranchName = ''
	g_Project = nil
	g_Config = Pipeline::Config.instance()

	#---------------------------------------------------------------------
	# --- PARSER COMMAND LINE                                          --- 
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] )
		puts OS::Getopt.usage( OPTIONS )
		puts ("Press Enter to continue...")
		$stdin.getc( )				
		Process.exit!( 1 )
	end	

	if not File::file? trailing[0]			
		$stderr.puts "File not found #{trailing[0]}"
		Process.exit! -1
	end

	# get rid of namespaces they screw up xpath searches and make it painful
	temp_suffix = ".nonamespace.xml"
	save_suffix = ".filtered.xml"
	new_filename = "#{trailing[0]}#{temp_suffix}"
	save_filename = "#{trailing[0]}#{save_suffix}"
	
	file_contents = IO.read(trailing[0])
	file_contents = file_contents.gsub("xmlns=\"http://schemas.microsoft.com/developer/msbuild/2003\"","")
	
	file = File.new(new_filename, "w")
	file.write(file_contents)
	file.close

	xmldoc = LibXML::XML::Document::file( new_filename )
	
	if not xmldoc
		$stderr.puts "Invalid XmlDoc #{new_filename}"
		Process.exit! -1
	end	
	
	config = trailing[1]
	platform = trailing[2]
	build_config = "#{config}|#{platform}"

	puts "Filtering #{new_filename} with config #{config} platform #{platform}"
	

	vcxproj_filter(xmldoc,config, platform)
	xmldoc.save(save_filename)
	
	#xmldoc = REXML::Document.new File.open( new_filename, 'r' )
	#REXML::Document.new(new_filename)
	#xmldoc.elements.delete("//child::*[@Condition and not(contains(@Condition, concat(\"'\",\'#{build_config}\',\"'\")))]" ) 		
#	xmldoc = REXML::Document.new File.open( new_filename, 'r' )
#	num_deleted = 0
#	to_delete = []
#	xmldoc.root.each_element_with_attribute("Condition") do |element|
#		
#		if (not element.attributes["Condition"].include?("'#{build_config}'"))
#			#element.remove
#			#puts element.attributes["Condition"]
#			to_delete<<element
#			num_deleted = num_deleted + 1
#		end		
#	end
#	puts "num_deleted #{num_deleted}"
#	
#	
##	to_delete.each do |del|
#		puts "deleting #{del.xpath}"
#		xmldoc.root.delete_element(del.xpath)
#	end
#	
#	delete("//child::*[@Condition]" ) 	#[@Condition and not(contains(@Condition, concat(\"'\",\'#{build_config}\',\"'\")))]
#	#puts xmldoc
#	File.open( new_filename, 'w' ) do |file|
##		fmt = REXML::Formatters::Default.new()
#		fmt.write(xmldoc,file) 	
#	end		
		
	file_contents = IO.read(save_filename)
	file_contents = file_contents.gsub(/\n\s*\n/, "\n")
	
	file = File.new(save_filename, "w")
	file.write(file_contents)
	file.close
		
	
	#doc = REXML::Document.new( File.open( save_filename, 'r' ) ) if ( File::exists?( save_filename ) )
	#File.open( save_filename, 'w+' ) do |file|				
	#	fmt = REXML::Formatters::Default.new()
	#	fmt.write( doc, file )    						
	#end	
	
	Process.exit! 0

rescue Exception => ex
	$stderr.puts "Error: Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	Process.exit! -1
end
