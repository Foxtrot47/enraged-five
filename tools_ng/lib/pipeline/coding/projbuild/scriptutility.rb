#
# File:: scriptutility.rb
# 
# Pipeline::ScriptUtility class for retrieving data from the project generation scripts.
#
# Author:: Kevin Weinberg <kevin.weinberg@rockstarsandiego.com>
# Date:: 12 May 2010
#
#
require 'pipeline/os/file'
require 'pipeline/os/path'

module Pipeline

module ProjBuild

class ScriptUtility

	def ScriptUtility.get_templates_root()
	
		template_root = Globals::instance().toolsconfig + "/projbuild/templates"
		return template_root
	end
	
	def ScriptUtility.get_custom_root()
	
		custom_steps_root = Globals::instance().toolsroot + "/script/coding/custom_steps"
		return custom_steps_root
	end
	
	#Acquires the template to use for generating this project.
	def ScriptUtility.determine_project_template( path, project_name )
		
		
				
		templates_root = get_templates_root()
		selected_template = OS::Path::combine(templates_root, "tester") 	#Default template name if one has not been specified.
		
		archive = ScriptUtility.acquire_variable(path, 'ARCHIVE')
		if archive.nil? == false
		
			archive_entries = []
			archive_entries = archive.split(" ")
			
			archive_entries.each do |archive_entry|
			
				if archive_entry.eql?(project_name) == true
			
					#Look for the ARCHIVE_BASE variable.  
					selected_template = acquire_variable( path, "ARCHIVE_BASE" ) 					
					if selected_template.nil?
						selected_template = OS::Path::combine(templates_root, "staticlib") 	#Default template name for archives.
					else
						selected_template = OS::Path::combine(templates_root, selected_template)
					end
					
					break
				end
			end
		end
		
		testers = ScriptUtility.acquire_variable(path, 'TESTERS')
		if testers.nil? == false
	
			testers_entries = []
			testers_entries = testers.split(" ")
		
			testers_entries.each do |tester_entry|

				if tester_entry.eql?(project_name) == true
			
					#Look for the TESTER_BASE variable.  
					selected_template = acquire_variable( path, "TESTER_BASE" ) 					
					if selected_template.nil?
			
						selected_template = OS::Path::combine(templates_root, "tester") 	#Default template name.
					else
						
						selected_template = OS::Path::combine(templates_root, selected_template)
					end
					
					break
				end
			end
		end
		
		# DW : unfortunately I'm having to hack this code to get it working in the first place. 
		# - since I don't understand why the passed template doesn't get respected
		# - also if the impact of fixing this would effect other projects - sad times.
		if path.downcase.include?("vs_project_nm") 
			RageGenProjectLoader.log.info(">>>>>>>>>>>>>>>HACK : determine_project_template #{path} #{project_name}<<<<<<<<<<<<<<<<<<<")
			selected_template = OS::Path::combine(templates_root, "nm_tester") 			
		end		
		
		return selected_template
	end
	
	#Acquires the template to use to generate the given solution.
	def ScriptUtility.determine_solution_template( path )
	
		templates_root = get_templates_root()
		tester_base = acquire_variable( path, "TESTER_BASE" ) 
		if tester_base != nil and tester_base != ""
			selected_template = OS::Path::combine(templates_root, tester_base)
		else
			selected_template = OS::Path::combine(templates_root, "tester") 	#Default template name if one has not been specified.
		end

		return selected_template
	end
	
	#Parses an existing batch file (e.g. Makefile.bat) for information
	#on a given project.  
	#Assumes that the specified input_filename is parsable by this function.
	#
	def ScriptUtility.acquire_variable( input_filename, variable_name )
	
		@makefile_batch_file = input_filename	

		return "" if File.exists?(@makefile_batch_file) == false
		batch_variable_name = "%#{variable_name}%"  #Used later to prune any of these instances from the result.
		result_lines = []
		
		lines = File.readlines(@makefile_batch_file)
		lines.each do |line|
			
			next if line.index("REM") == 0   #We have commented out this line, ignore it.
			
			if line.index("call") == 0
				
				#Calling another script from here.  Parse that.
				tokens = line.split(' ')
				
				@internal_batch_file = File.dirname(input_filename)
				@internal_batch_file = File.expand_path(tokens[1], @internal_batch_file) 
				
				result = acquire_variable(@internal_batch_file, variable_name)
				
				if result != nil and result.eql?("") == false
					result.split(" ") 
					result_lines << result
				end
			end

			if line.index(variable_name) != nil
				
				line.chomp!
				tokens = line.split('=')
				
				#Validate that the variable name is on the left side of this string
				left_side_tokens = tokens[0].split(" ")
				left_side_tokens.each do |token|
				
					if token.eql?(variable_name) == true and tokens.size > 1
					
						right_side_tokens = []
						right_side_tokens = tokens[1].split(" ")
						right_side_tokens.each do |right_side_token|
							
							#Do not add tokens that append to the variable (e.g. SET FILES = %FILES% ...)
							if right_side_token.eql?(batch_variable_name) == false
								result_lines << right_side_token
							end
						end
					end
				end
			
			end	
		end

		return nil if result_lines.size == 0
		
		return result_lines.join(" ")
	end
	
	def ScriptUtility.acquire_expanded_libs( path, tester_name )
		
		libs = acquire_variable( path, 'LIBS' ) 
		
		#An override variable for certain cases where multiple solutions created from one makefile.bat have 
		#varying libraries.  Relies on the naming convention "LIBS_<tester name>".
		specific_lib_var_name = "LIBS_" + tester_name
		specific_libs = acquire_variable(path, specific_lib_var_name)
		
		if specific_libs != nil
			if libs == nil
				libs = specific_libs
			else
				libs += " " + specific_libs
			end
		end
	
		return nil if libs == nil
		libs_array = libs.split(" ")
		expanded_libs = []
		@expanded_libs_definition  = Globals::instance().toolsconfig + "/projbuild/environment_libs.bat"
		
		libs_array.each do |library|
			
			library = expand_lib_variable(path, library) 
			
			#Split these resultant libraries up.
			libraries = library.join(" ")
			library = convert_paths_in_line(libraries)
			libraries = library.split(" ")
			
			expanded_libs.concat(libraries)
		end
		
		return expanded_libs
	end
	
	def ScriptUtility.expand_lib_variable( path, lib_name )

		expanded_libs = []
		
		if lib_name.index('%') == 0 and lib_name.rindex('%') == lib_name.size-1
				
			trimmed_library = lib_name[1...lib_name.size-1]
			
			#First, acquire the variable of the library in the project's script.
			expanded_lib = acquire_variable( path, trimmed_library)
			
			#Cross-reference the value of this variable with the common library definitions.
			if expanded_lib == nil
			
				expanded_lib = acquire_variable(@expanded_libs_definition, trimmed_library)
			end
			
			if expanded_lib != nil
				
				#Again, the common library definitions may have references to other common definitions.
				#Expand them out entirely.
				
				expanded_array = expanded_lib.split(" ")
				expanded_array.each do |expanded_var|
				
					expanded_lib = expand_lib_variable( path, expanded_var )
					expanded_libs << expanded_lib
				end
			end
			
			if expanded_lib == nil
			
				expanded_libs << lib_name
			end
		else

			expanded_libs << lib_name
		end
		
		return expanded_libs
	end
	
	@@dir_tokens = ["$(RAGE_DIR)", "%RAGE_DIR%"] #Tokens to be replaced by a relative path to the RAGE directory.
	
	def ScriptUtility.convert_paths_in_line( line )
		
		if ENV["RAGE_DIR"] == nil
			return line
		end

		@@dir_tokens.each do |token|
			line.gsub!(token, ENV["RAGE_DIR"])
		end
		
		return line
	end
	
			
	@@include_defs = nil
		
	#Acquire a string-string hash mapping a name to an include path. 
	def ScriptUtility.find_include_definition( name )
	
		if @@include_defs == nil 
		
			@include_definitions_file = Globals::instance().toolsconfig + "/projbuild/include_definitions.xml"
			if File.exists?(@include_definitions_file) == false
				return name
			end
			
			xml = File.read(@include_definitions_file)
			doc = REXML::Document.new(xml)

			@@include_defs = Hash.new
			doc.elements.each('IncludePaths/Include') do |include_node|
			
				if include_node.attributes["name"].nil? == false and include_node.attributes["path"].nil? == false
				
					@@include_defs[include_node.attributes["name"].downcase] = include_node.attributes["path"]
				end
			end
		end
		
		if @@include_defs != nil and @@include_defs[name.downcase] != nil

			return @@include_defs[name.downcase] 
		else

			return name
		end
	end
	
end

end #module ProjBuild

end #module Pipeline
