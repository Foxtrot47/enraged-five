#
# custom.rb
# ProjBuilder Custom Configuration Utility - Shared Functions
# 
# Created a file configuration XML node based on a template custom file specified.
#
# Kevin Weinberg <kevin.weinberg@rockstarsandiego.com>
#

module Pipeline

module ProjBuild

class CustomBuildStepShared

	def CustomBuildStepShared.scan_dependencies( path, file, dependencies )
		
		return if File.exist?(file) == false
		
		#Recursively scan for include paths to determine a set of dependencies for this object.
		files = File.readlines(file)
		
		current_file_dir = File.dirname(file)
		
		files.each do |line|
			
			line.downcase!
			line.strip!
			
			index = line.index("#include")
			index = line.index(".include") if index == nil
			if index != nil
				
				split_paths = line.split("\"")
				
				next if split_paths.size == 0
				
				dependency_path = split_paths[1]

				next if dependency_path.nil? or dependency_path.eql?("")
				
				dependency_path = OS::Path::combine(current_file_dir, dependency_path) if current_file_dir != nil
				dependency_path = File.expand_path(dependency_path)
				dependency_path = OS::Path::normalise(dependency_path)
				dependency_path = OS::Path::make_relative(dependency_path, path)
				
				if dependency_path.index("/") == 0 or dependency_path.index("\\") == 0
					next
				end
				
				#Do not allow duplicate paths in the dependency list.
				found = false
				dependencies.each do |dependency|
				
					if dependency.eql?(dependency_path) == true
						found = true
						break
					end
				
				end 
				
				if found == false
				
					dependencies.insert(dependencies.size, dependency_path)
					scan_dependencies(path, dependency_path, dependencies)
				end
			end
		end
		
	end

end #class CustomBuildStep
end #module ProjBuild
end #module Pipeline