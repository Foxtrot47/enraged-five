#
# custom.rb
# ProjBuilder Custom Configuration Utility
# 
# Created a file configuration XML node based on a template custom file specified.
#
# Kevin Weinberg <kevin.weinberg@rockstarsandiego.com>
#

require 'pipeline\coding\projbuild\custom\custom_build_shared.rb'

module Pipeline

module ProjBuild

class CustomBuildStep

	def CustomBuildStep.get_file_configuration(custom_build_file, file_path, configuration_name)
		
		return nil if File.exists?(custom_build_file) == false
		
		dependencies = []
		CustomBuildStepShared.scan_dependencies(File.dirname(file_path), file_path, dependencies)
		dependencies_str = dependencies.join(";")
		
		configuration_name = configuration_name.to_s
		
		file_configuration = ""
		File.readlines(custom_build_file).each do |line|
			file_configuration += line
		end
		
		file_configuration.gsub!("$(ADDITIONAL_DEPENDENCIES)", dependencies_str)
		
		return file_configuration
	end
	
end #class CustomBuildStep
end #module ProjBuild
end #module Pipeline