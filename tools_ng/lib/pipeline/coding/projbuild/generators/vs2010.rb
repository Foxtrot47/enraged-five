require 'active_support/ordered_hash'
require 'xml'
require 'pipeline/os/path'
require 'pipeline/os/file'
require 'pipeline/coding/projbuild/project'
require 'pipeline/coding/projbuild/generators/vsutility'
require 'pipeline/coding/projbuild/generators/vs2010_sln'
require 'pipeline/coding/projbuild/generators/vs2010_generator'
require 'pipeline/coding/projbuild/generators/vs2010_projloader'
require 'pipeline/coding/projbuild/generators/vs2010_projgenerator'

module Pipeline

  module ProjBuild

    class ItemGroup

      # Used for creating XML such as:
      # <ItemGroup Label="ProjectConfigurations">
      #
      attr_reader :label_name
	
      ITEM_GROUP = "ItemGroup"
      def initialize(label_name = nil)
        @label_name = label_name
      end
	
      def create_node()
	
        xml_node = XML::Node.new(ITEM_GROUP)
        xml_node.attributes["Label"] = label_name if label_name != nil
		
        xml_node
      end
    end

    class PropertyGroup

      # Used for creating XML such as:
      # <PropertyGroup Condition="'$(Configuration)|$(Platform)'=='RscDebug|Win32'" Label="Configuration">
      # ...
      # </PropertyGroup>
      attr_reader :label, :condition
      PROPERTY_GROUP = "PropertyGroup"
	
      def initialize(label, condition = nil)
	
        @label = label
        @condition = condition
      end
	
      def create_node()
		
        xml_node = XML::Node.new(PROPERTY_GROUP)
        xml_node.attributes["Label"] = @label if @label != nil
        xml_node.attributes["Condition"] = @condition if @condition != nil
		
        xml_node
      end
    end

    class ProjectConfiguration

      # Used for creating XML such as:
      #  <ProjectConfiguration Include="BankRelease|Win32">
      #  	<Configuration>BankRelease</Configuration>
      #  	<Platform>Win32</Platform>
      #  </ProjectConfiguration>
	
      attr_reader :configuration, :platform, :name
	
      def initialize(configuration, platform, name)
        @configuration = configuration
        @platform = platform
        @name = name
      end
	
      def create_node()
		
        projconfig_node = XML::Node.new("ProjectConfiguration")
        projconfig_node.attributes["Include"] = "#{@name}"
		
        configuration_node = XML::Node.new("Configuration")
        configuration_node << @configuration.to_s
        projconfig_node << configuration_node
			
        platform_node = XML::Node.new("Platform")
        platform_node << @platform
        projconfig_node << platform_node
		
        projconfig_node
      end
    end

    class GlobalProperties

      # Used for creating XML such as:
      #   <PropertyGroup Label="Globals">
      #		<ProjectName>RageCore</ProjectName>
      #		<ProjectGuid>{A332A45A-30D2-4007-96DD-E20799ED8CC6}</ProjectGuid>
      #	</PropertyGroup>
      attr_reader :project_name, :project_guid
	
      GLOBALS = "Globals"
	
      def initialize(name, guid)
	
        @project_name = name
        @project_guid = guid.gsub(/\s+/, "")
      end
	
      def create_node()
		
        property_group = PropertyGroup.new(GLOBALS)
		
        property_group_node = property_group.create_node()
		
        project_name_node = XML::Node.new("ProjectName")
        project_name_node << @project_name
        property_group_node << project_name_node
			
        project_guid_node = XML::Node.new("ProjectGuid")
        project_guid_node << @project_guid
        property_group_node << project_guid_node
			
        property_group_node
      end
    end

    class ImportProject

      # Used for creating XML such as:
      # <Import Project="$(VCTargetsPath)\Microsoft.Cpp.Default.props" />
      attr_reader :project_file, :label, :condition

      def initialize(project_file, label = nil, condition = nil)
		
        @project_file = project_file
        @label = label
        @condition = condition
      end
	
      def create_node()
		
        default_node = XML::Node.new("Import")
        default_node.attributes["Project"] = @project_file if @project_file != nil
        default_node.attributes["Label"] = @label if @label != nil
        default_node.attributes["Condition"] = @condition if @condition != nil
		
        default_node
      end
    end

    class ImportGroup

      #Used for creating XML such as:
      #<ImportGroup Condition="'$(Configuration)|$(Platform)'=='RscDebug|Win32'" Label="PropertySheets">
      #  <Import Project="$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props" Condition="exists('$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props')" Label="LocalAppDataPlatform" />
      #  <Import Project="$(VCTargetsPath)Microsoft.CPP.UpgradeFromVC71.props" />
      #</ImportGroup>
	
      attr_reader :condition, :label
	
      def initialize(label, condition = nil, projects = nil)
	
        @label = label
        @condition = condition
        @projects = projects 	#An array of projects to be imported.
      end
	
      def create_node
		
        group_node = XML::Node.new("ImportGroup")
        group_node.attributes["Label"] = @label
        group_node.attributes["Condition"] = @condition if @condition != nil
		
        if @projects != nil
          @projects.each do |project|

            import_project_node = project.create_node()
            group_node << import_project_node
          end
        end
		
        group_node
      end
    end

    class ConfigurationInfo
	
      #Used to create XML such as:
      #   <PropertyGroup Condition="'$(Configuration)|$(Platform)'=='RscDebug|Win32'" Label="Configuration">
      #		<ConfigurationType>StaticLibrary</ConfigurationType>
      #		<CharacterSet>MultiByte</CharacterSet>
      #	</PropertyGroup>
      attr_reader :configuration, :config_type, :character_set
	
      CONFIGURATION = "Configuration"
      CONFIGURATION_TYPE = "ConfigurationType"
      CHARACTER_SET = "CharacterSet"
      PLATFORM_TOOLSET = "PlatformToolset"
			EXCEPTIONS_AND_RTTI = "ExceptionsAndRtti"
	
      def initialize(configuration_name, config_type, character_set, platform_toolset = nil, exceptions_and_rtti = nil )
	
        @configuration_name = configuration_name
        @config_type = config_type
        @character_set = character_set
				@platform_toolset = platform_toolset
				@exceptions_and_rtti = exceptions_and_rtti
      end

      def create_node()
	
        configuration_condition = "'$(Configuration)|$(Platform)'=='#{@configuration_name}'"
        property_group = PropertyGroup.new(CONFIGURATION, configuration_condition)
		
        property_group_node = property_group.create_node()
		
        config_type_node = XML::Node.new(CONFIGURATION_TYPE)
        config_type_node << @config_type
        property_group_node << config_type_node
			
        character_set_node = XML::Node.new(CHARACTER_SET)
        character_set_node << @character_set
        property_group_node << character_set_node

				if (@platform_toolset)
	        platform_toolset = XML::Node.new(PLATFORM_TOOLSET)
		      platform_toolset << @platform_toolset
			    property_group_node << platform_toolset				
				end
				
				if @exceptions_and_rtti
	        exceptions_and_rtti = XML::Node.new(EXCEPTIONS_AND_RTTI)
		      exceptions_and_rtti << @exceptions_and_rtti
			    property_group_node << exceptions_and_rtti
				end

        property_group_node
      end
    end

    class ProjectFileVersion
	
      PROJECT_FILE_VERSION_NODE = "_ProjectFileVersion"
      PROJECT_VERSION = "10.0.30319.1"
	
      def initialize()
		
        @version = PROJECT_VERSION
      end
	
      def create_node()
		
        xml_node = XML::Node.new(PROJECT_FILE_VERSION_NODE)
        xml_node << @version
		
        xml_node
      end
    end

    class ItemDefinitionGroup

      ITEM_DEFINITION_GROUP = "ItemDefinitionGroup"
	
      def initialize(condition)
	
        @condition = condition
      end
	
      def create_node()
		
        xml_node = XML::Node.new(ITEM_DEFINITION_GROUP)
        xml_node.attributes["Condition"] = @condition
		
        xml_node
      end
    end

    class VisualStudioFilter

      def initialize(path, guid)
	
        @path = path
        @unique_identifier = guid
      end
	
      def create_node()
		
        xml_node = XML::Node.new("Filter")
        xml_node.attributes["Include"] = @path
		
        id_node = XML::Node.new("UniqueIdentifier")
        id_node << @unique_identifier
			
        xml_node << id_node
        xml_node
      end
    end

    # -----------------------------------------
    # -------- DW : DEBUGGING SUPPORT ---------
    # debugging stages - gives a hierarchical feel to a program along with trace info
    # - I find this required when code is not modular.
    class DebugTracer
      attr_accessor :enabled
      MAX_STAGES = 6

      def initialize(max_stages = MAX_STAGES)
        @max_stages = max_stages
        @enabled = true # switch this shit off if you like.
        @s=[]
        clear_stages(0)
      end

      def clear_stages(stage)
        stage.upto(@max_stages-1) { |i| @s[i] = 0 }
      end

      def next_stage(stage)
        @s[stage-1] += 1
        clear_stages(stage)
        stage_str()
      end

      def stage_str()
        str = ""
        1.upto(@max_stages-1) { |i| str += "  " if @s[i] != 0 }        # padding
        str += @s[0].to_s
        1.upto(@max_stages-1) { |i| str += ".#{@s[i]}" if @s[i] != 0 } # each number
        str
      end
    end
    # -------- END DEBUGGING SUPPORT ---------
    # ----------------------------------------

  end #module ProjBuild

end #module Pipeline