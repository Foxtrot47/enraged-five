require 'xml'
require 'pipeline/coding/projbuild/project'
require 'pipeline/coding/projbuild/xml/include'

module Pipeline

module ProjBuild

class ProjBuildImporter

	@@log = nil 
	
	def ProjBuildImporter.log

		@@log = Log.new( 'projbuildimporter' ) if @@log == nil
		@@log
	end

	def import( path )
	
		if File.exists?(path) == false then
	
			ProjBuildImporter.log.debug("couldn't open #{path}")
			return false 
		end
	
		ProjBuildImporter.log.debug("checking #{path} with internal project loader")
		
		parser = XML::Parser.file(path)
		doc = parser.parse

		if doc.root.name != "rproj" or doc.root["Version"] != "1.00" then

			ProjBuildImporter.log.debug("#{path} not in the right format")
			return false 		
		end
		
		ProjBuildImporter.log.debug("parsing #{path} with internal project loader")
		
		project_node = doc.root.find_first("project")
		project_include = project_node["xinclude"]
		
		if project_include != nil
		
			ProjBuildImporter.log.debug("yay!")
		
			project_node.attributes.get_attribute("xinclude").remove!		
		
			Pipeline::Globals.instance().in_env do |e|
			
				project_include = e.subst(project_include)
			end
			
			ProjBuildImporter.log.debug(project_include)
			
			xml_include = XMLUtil::Include.instance()			
			xml_include.merge_file(project_node,project_include) 
		end
		
		@curr_project = Info::Project.new(OS::Path.get_basename(path))
		@curr_project.name = project_node.attributes['name']
		@curr_project.guid = project_node.attributes['guid']
		@curr_project.path = project_node.attributes['path']		
	#	@curr_project.config.root_node.attributes["type"] = project_node.attributes['type']
		
		ProjBuildImporter.log.debug("importing tree")
		
		import_tree(@curr_project,project_node)
			
		return true, @curr_project
	end

	def import_configurable( configurable, xml_node )
	
		xml_config = xml_node.find_first("config")		
		configurable.platforms.clear
		
		configurable.config.xml_data = xml_config.copy(true) if xml_config != nil

		xml_node.find("platform").each { |xml_platform|
		
			platform_sym = xml_platform.attributes["name"].intern

			new_platform = Info::Platform.new(platform_sym)
			xml_config = xml_platform.find_first("config")
			new_platform.config.xml_data = xml_config.copy(true) if xml_config != nil
			
			configurable.platforms[platform_sym] = new_platform
			
			xml_platform.find("target").each { |xml_target|
			
				target_name = xml_target.attributes["name"]
				xml_config = xml_target.find_first("config")
				configurable.platforms[platform_sym].get_target(target_name).xml_data = xml_config.copy(true) if xml_config != nil
			}			
		}
	end
	
protected
	
	def import_tree( filter, xml_node )
	
		ProjBuildImporter.log.debug("import node #{filter.name}")
	
		import_configurable(filter,xml_node)
		
		xml_node.find("filter").each { |xml_filter|
		
			new_filter = Info::Filter.new(xml_filter["name"])
			new_filter.path = xml_filter.attributes["path"]		
			
			import_configurable(new_filter,xml_filter)
			import_tree(new_filter,xml_filter)
			
			filter.filters << new_filter
		}				
		
		xml_node.find("file").each { |xml_file|
					
			new_file = Info::File.new(xml_file["path"])
			import_configurable(new_file,xml_file)
			filter.files << new_file
		}
	end
end

class ProjBuildExporter

	def export( path, project )
		
		@path = File.expand_path(path)
		
		#we can strip out unrequired configuration here...
		#...i.e. custom build steps that are the same as the wildcard default...
		
		project.rationalise
		
		#make the root project path relative...	
		#project.path = OS::Path::get_directory(path.gsub(project.path,""))
		
		doc = XML::Document.new()
		
		doc.root = XML::Node.new("rproj")
		doc.root.attributes["Version"] = "1.00"
		export_project(project,doc.root)
		export_solution(doc.root)
		
		doc.save(path)
	
		true
	end
	
protected

	def export_config( config, xml_parent )
	
		return 0 if (config == nil)
		return 0 if (config.xml_data == nil)
	
		xml_parent << config.xml_data.copy(true)
	end

	def export_configurable( configurable, xml_parent )
		
		export_config(configurable.config,xml_parent)
		
		return 0 if configurable.platforms == nil
		
		configurable.platforms.each { |platform_sym, platform|
		
			if platform.targets.size == 0 then
		
				next if platform.config == nil
				next if platform.config.is_null
			end
		
			xml_platform = XML::Node.new("platform")
			xml_platform.attributes["name"] = platform_sym.to_s
				
			export_config(platform.config,xml_platform)
			
			platform.targets.each { |target_name, config|
			
				xml_config = XML::Node.new("target")	
				xml_config.attributes["name"] = target_name
				
				export_config(config,xml_config)
				
				xml_platform << xml_config
			}
		
			xml_parent << xml_platform
		}
	end

	def export_file( file, xml_parent )
		
		xml_node = XML::Node.new("file")
		xml_node.attributes["path"] = OS::Path.make_relative(file.path,OS::Path.remove_filename(@path))
		
		export_configurable(file,xml_node)
		
		xml_parent << xml_node		
	end
	
	def add_filter_info( filter, xml_parent )
		
		xml_parent.attributes["name"] = filter.name
		xml_parent.attributes["path"] = OS::Path.make_relative(filter.path,OS::Path.remove_filename(@path))

		export_configurable(filter,xml_parent)
		
		filter.filters.each { |child_filter|
			
			export_filter(child_filter,xml_parent)
		}
		
		filter.files.each { |child_file|
			
			export_file(child_file,xml_parent)
		}	
	end
	
	def export_filter( filter, xml_parent )
	
		xml_node = XML::Node.new("filter")	
		add_filter_info(filter,xml_node)
		
		xml_parent << xml_node
	end

	def export_project( project, xml_parent )
	
		xml_node = XML::Node.new("project")
		xml_node.attributes["guid"] = project.guid
		add_filter_info(project,xml_node)
	
		xml_parent << xml_node
	end
	
	def export_solution( xml_parent )
	
		proj_build = ProjBuildData::instance()
	
		xml_node = XML::Node.new("solution")

		proj_build.sln_platforms.each_pair { |sln_platform, sln_targets|
		
			xml_platform = XML::Node.new("platform")
			xml_platform.attributes["name"] = sln_platform
			xml_node << xml_platform
		
			sln_targets.each_pair { |sln_target, projectconfigs|

				xml_target = XML::Node.new("target")
				xml_target.attributes["name"] = sln_target
				xml_platform << xml_target
			
				projectconfigs.each_pair { |guid, projectconfig|	
			
					xml_project = XML::Node.new("projectconfig")
					xml_project.attributes["guid"] = guid
					xml_project.attributes["platform"] = projectconfig.platform.to_s
					xml_project.attributes["target"] = projectconfig.target
					xml_target << xml_project
				}
			}
		}
	
		xml_parent << xml_node		
	end
end

class ProjBuildGenerator
	@@unity_build_filenames	= nil # if true filenames that are created have "_unity" in them.
	def set_unity_build_filenames( set )
		@@unity_build_filenames = set
	end

	def get_unity_build_filenames( )
		@@unity_build_filenames
	end
	
	def export( path, project, options = nil )
	
		ProjBuildExporter.new().export(path,project)
	end

	def import( path, options = nil )		
	
		ProjBuildImporter.new().import(path)
	end
end

end #module ProjBuild

end #module Pipeline
