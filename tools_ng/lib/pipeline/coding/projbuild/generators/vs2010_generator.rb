require 'active_support/ordered_hash'
require 'xml'
require 'pipeline/os/path'
require 'pipeline/os/file'
require 'pipeline/coding/projbuild/project'
require 'pipeline/coding/projbuild/generators/vsutility'

module Pipeline

module ProjBuild


class VS2010Generator
	
      @@log = nil

      GENERATOR_ID = "2010"
	
      @@unity_build_filenames	= nil # if true filenames that are created have "_unity" in them.
      def set_unity_build_filenames( set )
        @@unity_build_filenames = set
      end

      def get_unity_build_filenames( )
        @@unity_build_filenames
      end

      def self.get_filters_filename( basename )
        (@@unity_build_filenames and not basename.downcase.include?("shaders")) ? "#{basename}_#{GENERATOR_ID}_unity.vcxproj.filters" : "#{basename}_#{GENERATOR_ID}.vcxproj.filters"
      end
	
      def self.get_project_filename( basename )
        #VS2010Generator.log.info("unity_build_filenames #{@@unity_build_filenames}")
        (@@unity_build_filenames and not basename.downcase.include?("shaders")) ? "#{basename}_#{GENERATOR_ID}_unity.vcxproj" : "#{basename}_#{GENERATOR_ID}.vcxproj"
      end

      def self.get_project_suffix(basename)
        (@@unity_build_filenames and not basename.downcase.include?("shaders")) ? "_#{GENERATOR_ID}_unity" : "_#{GENERATOR_ID}"
      end

      def self.get_solution_filename( basename )
        (@@unity_build_filenames and not basename.downcase.include?("shaders")) ? "#{basename}_#{GENERATOR_ID}_unity.sln" 	: "#{basename}_#{GENERATOR_ID}.sln"
      end

      def VS2010Generator.log

        @@log = Log.new( 'VS2010Generator' ) if @@log == nil
        @@log
      end

      def initialize()
	
        @log = VS2010Generator.log
      end
	
      def export( path, project, options = nil )

        VS2010Generator.log.info("Exporting #{path} for Visual Studio 2010")
	
        if options != nil and options[:type] == :solution then
          return VS2010VSSlnExporter.new().export(path,project)
        else
          project_name = VS2010Generator.get_project_filename(path)
          filters_name = VS2010Generator.get_filters_filename(path)
          return VS2010ProjectGenerator.new(get_unity_build_filenames()).export(project_name, filters_name, project)
        end
			
        false
      end
	
      def import( path, options = nil )
	
        case OS::Path::get_extension(path).downcase
		
        when 'sln':
		
            @log.info("Importing solution #{path}.")
		
          #	loader = VS2010SlnLoader.new()
          return nil #loader.import(path)
			
        when 'vcproj':
			
            @log.info("Importing project #{path}.")
			
          loader = VS2010ProjectGenerator.new(get_unity_build_filenames())
          return loader.import(path)
        end
		
        false
      end
    end

	end #module ProjBuild

end #module Pipeline