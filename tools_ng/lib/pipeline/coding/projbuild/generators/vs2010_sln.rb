require 'active_support/ordered_hash'
require 'xml'
require 'pipeline/os/path'
require 'pipeline/os/file'
require 'pipeline/coding/projbuild/project'
require 'pipeline/coding/projbuild/generators/vsutility'
require 'pipeline/coding/projbuild/generators/vs2010'
require 'pipeline/coding/projbuild/generators/vs2010_generator'

module Pipeline

module ProjBuild


class VS2010VSSlnExporter < VSProjShared

	VS_VERSION = "11.00"
	VS_VERSION_YEAR = "2010"

	@@log = nil 

	#-----------------------------------------------------------------------------
	#
	def VS2010VSSlnExporter.log

		@@log = Log.new( 'vs2010vsslnexporter' ) if @@log == nil
		@@log
	end

	#-----------------------------------------------------------------------------
	#
	def initialize()
		@log = VS2010VSSlnExporter.log
		config = Pipeline::Config.instance
	end

	#-----------------------------------------------------------------------------
	#
	def write_identifying_character_sequence(file)
      file.putc(0xef)
			file.putc(0xbb)
			file.putc(0xbf)
			file.write("\n")
	end

	#-----------------------------------------------------------------------------
	#
  def write_dep_guids(proj_build,project,file)
      dep_guids = ActiveSupport::OrderedHash.new()
			project.dep_guids.each do |dep_guid|

				dep_guids[dep_guid] = true
				dep_project = proj_build.dependent_projects[dep_guid]
				file.write("\t\t#{dep_project.guid} = #{dep_project.guid}\n")
			end
			dep_guids
	end

	#-----------------------------------------------------------------------------
	#
  def write_header(file,sln_guid,project,main_filename,proj_build)
			
			write_identifying_character_sequence(file)

			file.write("Microsoft Visual Studio Solution File, Format Version #{VS_VERSION}\n")
			file.write("# Visual Studio #{VS_VERSION_YEAR}\n")
			file.write("Project(\"#{sln_guid}\") = \"#{project.name}\", \"#{main_filename}\", \"#{project.guid}\"\n")
			file.write("\tProjectSection(ProjectDependencies) = postProject\n")

			dep_guids = write_dep_guids(proj_build,project,file)

			file.write("\tEndProjectSection\n")			
			file.write("EndProject\n")
      dep_guids
  end
  
	#-----------------------------------------------------------------------------
	#
  def	write_dependent_projects(proj_build,file,project,sln_guid)
    
     proj_build.dependent_projects.keys.each do |dep_guid|
			
				next if dep_guid == project.guid
				dep_project = proj_build.dependent_projects[dep_guid]

				filename = OS::Path.combine(dep_project.path,dep_project.name)							
				filename = OS::Path.dos_format(VS2010Generator.get_project_filename(filename))

        # DW : some implicit naming is required by msbuild it seems : sadly this make the VS2010 project less neat
        # we should have made the unity builds different configurations anyway and this wouldnt be a problem!!!... naebody listens...
        project_name = dep_project.name
        project_name += VS2010Generator.get_project_suffix(project_name)

				file.write("Project(\"#{sln_guid}\") = \"#{project_name}\", \"#{filename}\", \"#{dep_project.guid}\"\n")
				file.write("EndProject\n")
     end
  end      

	#-----------------------------------------------------------------------------
	#
	def write_pre_solution(proj_build,file)
			file.write("Global\n")
			file.write("\tGlobalSection(SolutionConfigurationPlatforms) = preSolution\n")
			proj_build.sln_platforms.each do |sln_platform,targets|
				targets.each do |sln_target,proj_cfg|
					platform_out = VSProjShared.get_platform_string(sln_platform)
					config = "#{sln_target}|#{platform_out}"
					file.write("\t\t#{config} = #{config}\n")
				end
			end
			file.write("\tEndGlobalSection\n")
	end

	#-----------------------------------------------------------------------------
	#
	def write_post_solution(proj_build,file,dep_guids,project)
			file.write("\tGlobalSection(ProjectConfigurationPlatforms) = postSolution\n")
			proj_build.sln_platforms.each do |sln_platform,targets|

				targets.each do |sln_target,proj_cfgs|

					#First process the start up project.
					proj_cfg = proj_cfgs[project.guid]
					prj_cfg = VSUtility.create_config_name(proj_cfg.platform,proj_cfg.target,true)
					sln_cfg = "#{sln_target}|#{VSProjShared.get_platform_string(sln_platform)}"
					config_out = "#{project.guid}.#{sln_cfg}"

					file.write("\t\t#{config_out}.ActiveCfg = #{prj_cfg}\n")
					file.write("\t\t#{config_out}.Build.0 = #{prj_cfg}\n")

					#Process its dependent projects.
					proj_build.dependent_projects.keys.each do |guid|

						next if proj_cfgs[guid] == nil
						proj_cfg = proj_cfgs[guid]

						prj_cfg = VSUtility.create_config_name(proj_cfg.platform,proj_cfg.target,true)
						sln_cfg = "#{sln_target}|#{VSProjShared.get_platform_string(sln_platform)}"
						config_out = "#{guid}.#{sln_cfg}"

						file.write("\t\t#{config_out}.ActiveCfg = #{prj_cfg}\n")
						file.write("\t\t#{config_out}.Build.0 = #{prj_cfg}\n") if dep_guids[guid] or guid == project.guid
					end
				end
			end

			file.write("\tEndGlobalSection\n")
	end

	#-----------------------------------------------------------------------------
	#
	def write_footer(file)
			file.write("\tGlobalSection(SolutionProperties) = preSolution\n")
			file.write("\t\tHideSolutionNode = FALSE\n")
			file.write("\tEndGlobalSection\n")
			file.write("EndGlobal\n")
	end

	#-----------------------------------------------------------------------------
	#
	def export(path, project)

		path = VS2010Generator.get_solution_filename(path)		
		sln_guid = "{8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942}"
		proj_build = ProjBuildData::instance()
				
		File.open(path,"w+") do |file|
		
			main_filename = VS2010Generator.get_project_filename(project.name)
		
      dep_guids = write_header(file,sln_guid,project,main_filename,proj_build)
				
      write_dependent_projects(proj_build,file,project,sln_guid)

			write_pre_solution(proj_build,file)

			write_post_solution(proj_build,file,dep_guids,project)

			write_footer(file)
		end
		
		revert_unchanged(path)
		
		true
	end
end

end #module ProjBuild

end #module Pipeline