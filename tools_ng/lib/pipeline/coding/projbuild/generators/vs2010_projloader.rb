require 'active_support/ordered_hash'
require 'xml'
require 'pipeline/os/path'
require 'pipeline/os/file'
require 'pipeline/coding/projbuild/project'
require 'pipeline/coding/projbuild/generators/vsutility'

module Pipeline

module ProjBuild

class VS2010VSProjLoader  < VSProjShared

      @@log = nil

      def VS2010VSProjLoader.log

        @@log = Log.new( 'VS2010VSProjLoader' ) if @@log == nil
        @@log
      end

      def initialize()

        @log = VS2010VSProjLoader.log
      end

      def import( path, project_path = nil )

        if File.exists?(path) == false then

          @log.debug("couldn't open #{path}")
          return false
        end

        @log.info("checking #{path} with vs2010 project loader")

        data = nil

        #parser = XML::Parser.file(path)
        #doc = parser.parse

        #	if doc.root.attributes["Version"] != "9.00" then
        #
        #		@log.debug("#{path} not in the correct format")
        #		return false
        #	end


        #strip_unused_info(doc)

        puts path
        doc = LibXML::XML::Document::file(path)

        @log.info("parsing #{path} with vs2010 project loader")

        globals = doc.find_first("//Project/PropertyGroup[@Label=\"Globals\"]")

        projectname_node = doc.root.find_first("//Project/@Name")

        @log.error("Can't find Name in project") if projectname_node.nil?
        proj_name = projectname_node.value

        @log.info("project name = #{proj_name}")

        @curr_project = Info::Project.new(OS::Path.get_basename(path))
        @curr_project.name = proj_name
        @curr_project_path = project_path if project_path != nil
        @curr_project_path = OS::Path.normalise(OS::Path.get_directory(path)) unless project_path != nil

        #files
        files_nodes = doc.find("//Project/ItemGroup/ClCompile")
        @log.error("Error: Cant find //Project/ItemGroup/ClCompile") if files_nodes.nil?
        import_proj_files(files_nodes,@curr_project)

        #root config
        configs_nodes = doc.find("//Project/ItemDefinitionGroup")
        @log.error("Error: Cant find //Project/ItemDefinitionGroup") if configs_nodes.nil?
        import_root_configs(configs_nodes)

        @curr_project.guid = doc.root.attributes["ProjectGUID"]
        @curr_project.config.xml_data = XML::Node.new("config")

        @log.debug("parsed #{path}")

        return true, @curr_project
      end

      protected

      def strip_unused_info( doc )

        doc.find("/VisualStudioProject/Configurations/Configuration/Tool").each { |xml_node|

          case xml_node.attributes["Name"]

          when "VCWebDeploymentTool":

              xml_node.remove!
          end
        }
      end

      def import_file_config_hierarchy( xml_in )

        xml_out = nil

        if xml_in.name == "FileConfiguration" then

          xml_out = XML::Node.new("config")
          clone_attributes(xml_in,xml_out)
        elsif xml_in.name == "Tool" then

          xml_out = XML::Node.new(xml_in.attributes["Name"])
          clone_attributes(xml_in,xml_out)
        elsif xml_in.name == "custombuild" then

          xml_out = XML::Node.new(xml_in.attributes["Name"])
          clone_attributes(xml_in,xml_out)
        end

        #		if xml_out == nil then
        #
        #			ProjBuilder.log.debug("Not loaded!")
        #			ProjBuilder.log.debug(xml_in)
        #		end

        if xml_out != nil and xml_in.children? then

          xml_in.children.each { |child|

            xml_new = import_file_config_hierarchy(child)
            xml_out << xml_new if xml_new != nil
          }
        end

        xml_out
      end

      def import_proj_files( xml_nodes, filter_node )

        xml_nodes.each do |xml_file_node|

          file_path = xml_file_node.attributes["Include"]
          file_path = File.expand_path(file_path,@curr_project_path)

          new_file = Info::File.new(file_path)

          # DW : is this valid in VS2010 ?
          #
          #			xml_file_node.find("FileConfiguration").each do |xml_file_config_node|
          #
          #				platform, target = VSProjShared.parse_config_name(xml_file_config_node.attributes["Name"])
          #				next if (platform == nil or target == nil)
          #				config = new_file.platforms[platform].get_target(target)
          #				config.xml_data = import_file_config_hierarchy(xml_file_config_node)
          #			end

          filter_node.files << new_file
        end

        # DW : is this valid in VS2010 ?
        #		xml_node.find("Filter").each do |xml_filter_node|
        #
        #			new_filter_node = Info::Filter.new(xml_filter_node.attributes["Name"])
        #			filter_node.filters << new_filter_node
        #
        #			import_proj_files(xml_filter_node,new_filter_node)
        #		end
      end

      def import_config_hierarchy( xml_in, doc, platform, target )

        xml_out = nil

        if xml_in.name == "Configuration" then

          doc.root = XML::Node.new("config")
          xml_out = XML::Node.new("vc")
          doc.root << xml_out

          case xml_in["ConfigurationType"]

          when "1": doc.root["type"] = :exe.to_s
          when "2": doc.root["type"] = :dll.to_s
          when "4": doc.root["type"] = :library.to_s

          end

          doc.root["output_path"] = xml_in["OutputDirectory"].downcase if xml_in["OutputDirectory"]

        elsif xml_in.name == "Tool" or xml_in.name == "custombuild"

          found = false

          #ProjBuilder.log.info("#{xml_in['Name']}")

          if xml_in["Name"] == "VCCLCompilerTool" or xml_in["Name"] == "VCCLX360CompilerTool" then

            #ProjBuilder.log.info("we are here A")

            if xml_in["AdditionalIncludeDirectories"] then

              found = true
              xml_incpaths = create_xml_path(doc.root,"includepaths")
              dirs = xml_in["AdditionalIncludeDirectories"].split(";")
              xml_in.attributes.get_attribute("AdditionalIncludeDirectories").remove!

              dirs.each { |dir|

                xml_incpath = XML::Node.new("includepath")
                xml_incpath["path"] = dir

                xml_incpaths << xml_incpath
              }
            end

            if xml_in["PreprocessorDefinitions"] then

              found = true
              xml_defines = create_xml_path(doc.root,"defines")
              defines = xml_in["PreprocessorDefinitions"].split(";")
              xml_in.attributes.get_attribute("PreprocessorDefinitions").remove!

              defines.each { |define|

                next if define == ""

                xml_define = XML::Node.new("define")
                xml_define["value"] = define

                xml_defines << xml_define
              }
            end

            if xml_in["ForcedIncludeFiles"] then

              found = true
              xml_forceincludes = create_xml_path(doc.root,"forceincludes")
              forceincludes = xml_in["ForcedIncludeFiles"].split(";")
              xml_in.attributes.get_attribute("ForcedIncludeFiles").remove!

              i = 0

              forceincludes.each { |forceinclude|

                xml_forceinclude = XML::Node.new("forceinclude")
                xml_forceinclude["path"] = forceinclude
                xml_forceinclude["order"] = i.to_s

                i = i + 1

                xml_forceincludes << xml_forceinclude
              }
            end
          end

          if (xml_in.attributes["Name"] != nil and xml_in.attributes.length > 1) then
            #ProjBuilder.log.info("clone_attributes")

            xml_out = XML::Node.new(xml_in.attributes["Name"])
            clone_attributes(xml_in,xml_out)
          end
        elsif xml_in.name == "DebuggerTool" then

          xml_out = XML::Node.new("DebuggerTool")
          clone_attributes(xml_in,xml_out)
        end

        #		if xml_out == nil then
        #
        #			ProjBuilder.log.debug("Not loaded!")
        #			ProjBuilder.log.debug(xml_in)
        #		end

        if xml_out != nil and xml_in.children? then

          xml_in.children.each { |child|

            xml_new = import_config_hierarchy(child,doc,platform,target)
            xml_out << xml_new if xml_new != nil
          }
        end

        xml_out
      end

      def import_root_config( xml_node )

        node = xml_node.attributes["Condition"]
        @log.error("Error: Bad node?") if node.nil?

        name_trimmed = node.gsub("'$(Configuration)|$(Platform)'=='","")
        name_trimmed.gsub!("'","")
        #@log.info("name_trimmed #{name_trimmed}")
        platform, target = VSProjShared.parse_config_name(name_trimmed)
        @log.info("import_root_config #{platform} #{target}")

        return nil if (platform == nil or target == nil)

        config = @curr_project.platforms[platform].get_target(target)

        doc = XML::Document.new()
        import_config_hierarchy(xml_node,doc,platform,target)

        config.xml_data = doc.root
      end

      def import_root_configs( xml_nodes )

        xml_nodes.each do |xml_config_node|

          import_root_config(xml_config_node)
        end
      end
    end

end #module ProjBuild

end #module Pipeline