#
# File:: vsutility.rb
# 
# Shared class functions for parsing, loading and creating
# 	Visual Studio projects.  Commonly accessible to all generators.
#
# Author:: Kevin Weinberg <kevin.weinberg@rockstarsandiego.com
# Date:: 22 September 2010
#
#
require 'pipeline/os/path'
require 'pipeline/os/file'
require 'xml'

module Pipeline

module ProjBuild

class VSUtility
	
	def VSUtility.create_guid( guid_filename, p4 )
	
		#If the GUID file does not exist, attempt to create it and add it to Perforce.
		if File.exists?(guid_filename) == false
		
			print "Creating new .guid file #{guid_filename}...\n"
			system("#{Globals::instance().toolsbin}\\coding\\uuidgen.exe -o#{guid_filename}")
			full_path = File.expand_path(guid_filename)
			
			if p4.nil? == false
				puts "p4 Adding #{full_path}"
				p4.run_add(full_path)
			end
			
			#Verify that the guid file has been created.  Otherwise, we have failed to create this project properly.
			if File.exists?(guid_filename) == false
				RageProjBuilderLoader.log.error("#{guid_filename} does not exist.  Can not generate project file!")
				return nil
			end	
		end
		guid = "#{File.readlines(guid_filename)[0]}"
		guid.chomp!		#Remove whitespace and newlines from the guid.
		guid = "{#{guid.upcase}}"
		
		return guid
	end
	
	def VSUtility.create_guid()
	
		temp_file = OS::Path.combine(Globals::instance().toolstemp, "temp.guidx")
		system("#{Globals::instance().toolsbin}\\coding\\uuidgen.exe -o#{temp_file}")
		
		guid = "#{File.readlines(temp_file)[0]}"
		guid.chomp!		#Remove whitespace and newlines from the guid.
		guid = "{#{guid.upcase}}"
		
		guid
	end
	
	#Parses the XML-formatted GUID file for particular information to generate
	#the project file.
	#
	def VSUtility.get_guid( guid_filename, path )
	
		#If the GUID file does not exist, attempt to create it and add it to Perforce.
		doc = nil
		if File.exists?(guid_filename) == false
		
			#Create the GUID file template.
			doc = XML::Document.new()		
			doc.encoding = XML::Encoding::UTF_8   #Used to create the proper XML header.
		
			project_node = XML::Node.new("Project")
			doc.root = project_node
			
			filters_node = XML::Node.new("Filters")
			project_node << filters_node

		else
		
			parser = XML::Parser.file(guid_filename)
			doc = parser.parse
		end
		
		filter_node = doc.root.find_first("Filters/Filter[@Path='#{path}']")
		
		guid = nil
		if filter_node == nil
			
			guid = create_guid()
			
			#Create this node and update the .guidx file.
			filters_node = doc.root.find_first("Filters")
			
			if filters_node == nil
				filters_node = XML::Node.new("Filters")
				doc.root << filters_node
			end
			
			new_filter_node = XML::Node.new("Filter")
			new_filter_node.attributes["Path"] = path
			new_filter_node.attributes["GUID"] = guid
			
			filters_node << new_filter_node
			
			#Save this XML and then convert it to a human-readable XML for easier differentiating.
			doc.save(guid_filename)
			VsXml::convert_to_vsxml(guid_filename, guid_filename, true)
		else
			
			#Parse it.
			guid = filter_node.attributes["GUID"]
		end
		
		return guid
	end
	
	WINDOWS_32_BIT_PLATFORM_NAME = "Win32"
	WINDOWS_64_BIT_PLATFORM_NAME = "x64"
	XBOX_360_PLATFORM_NAME = "Xbox 360"
	PLAYSTATION_3_PLATFORM_NAME = "Win32"  #Note that we build the Win32 platform specifically for the PS3, which has its own configurations.
	PLAYSTATION_3_PLATFORM_NAME_VS2010 = "PS3"  # DW now supported  in VS2010
	PSP2_PLATFORM_NAME = "Win32"  #Note that we build the Win32 platform specifically for the PSP2, which has its own configurations.
	PSP2_PLATFORM_NAME_VS2010 = "PSP2"
	def VSUtility.get_platform_name( platform, vs2010 = false )
	
		platform_name = case platform
		when :win32
			WINDOWS_32_BIT_PLATFORM_NAME
		when :win64
			WINDOWS_64_BIT_PLATFORM_NAME
		when :xbox360
			XBOX_360_PLATFORM_NAME
		when :ps3
			if vs2010==true
				PLAYSTATION_3_PLATFORM_NAME_VS2010
			else
				PLAYSTATION_3_PLATFORM_NAME				
			end
		when :psp2
			if vs2010==true
				PSP2_PLATFORM_NAME_VS2010
			else
				PSP2_PLATFORM_NAME				
			end
		else
			"#{platform.to_s}"
		end
	
		platform_name
	end

	def VSUtility.create_project_configuration_configuration( platform, target, vs2010 = false )
		if platform==:ps3
			if (vs2010==true)
				return "#{target}"
			else
				return "SN PS3 SNC #{target}"
			end
		end
		target.to_s
	end

	def VSUtility.create_config_name( platform, target, vs2010 = false  )
	
		config_name = case platform
		when :win32
			"#{target}|#{WINDOWS_32_BIT_PLATFORM_NAME}"
		when :win64
			"#{target}|#{WINDOWS_64_BIT_PLATFORM_NAME}"
		when :xbox360
			"#{target}|#{XBOX_360_PLATFORM_NAME}"
		when :ps3
			if vs2010==true
				"#{target}|#{PLAYSTATION_3_PLATFORM_NAME_VS2010}"
			else
				"SN PS3 SNC #{target}|#{PLAYSTATION_3_PLATFORM_NAME}"				
			end		
			
		when :psp2
			if vs2010==true
				"#{target}|#{PSP2_PLATFORM_NAME_VS2010}"
			else
				"SN PSP2 SNC #{target}|#{PSP2_PLATFORM_NAME}"			
			end
		else
			"#{target}|#{platform.to_s}"
		end
	
		config_name
	end	
		
	def VSUtility.create_config_type( config )
	
		return "1" if config == nil
		return "1" if config.xml_data == nil
		return "1" if config.xml_data.attributes["type"] == nil
	
		type = config.xml_data.attributes["type"].intern
	
		ret = case type
		
		when :library
			"4"
		when :dll
			"2"
		when :exe
			"1"
		else "1"
		end
		
		ret
	end
	
	APPLICATION = "Application"
	DLL = "DynamicLibrary"
	LIBRARY = "StaticLibrary"
		
	def VSUtility.create_2010_config_type( config )
		
		return LIBRARY if config == nil
		return LIBRARY if config.xml_data == nil
		return LIBRARY if config.xml_data.attributes["type"] == nil
	
		type = config.xml_data.attributes["type"].intern
	
		ret = case type
		
		when :library
			LIBRARY
		when :dll
			DLL
		when :exe
			APPLICATION
		else 
			APPLICATION
		end
		
		ret
	end
	
	MULTIBYTE = "MultiByte"
	UNICODE = "Unicode"
	NOTSET = "NotSet"
	
	def VSUtility.create_2010_character_set( config )
		return MULTIBYTE if config == nil
		return MULTIBYTE if config.xml_data == nil
		return MULTIBYTE if config.xml_data.attributes["character_set"] == nil
		
		character_set = config.xml_data.attributes["character_set"].intern
		
		ret = case character_set
		
		when :notset
			NOTSET
		when :unicode
			UNICODE
		when :multibyte
			UNICODE
		else 
			MULTIBYTE
		end
	
		ret
	end

  def VSUtility.create_2010_platform_toolset( config, platform_sym, target_sym )
		return "SNC" if platform_sym.to_s.downcase == "ps3"
	end

  def VSUtility.create_2010_exceptions_and_rtti( config, platform_sym, target_sym )
		"NoExceptsWithRtti" if platform_sym.to_s.downcase == "ps3"
	end

	def VSUtility.get_library_dependencies( config, target_sym )
	
		#Determine the list of library dependencies.
		additional_deps = Array.new()
		libs_node = config.find("libs/lib")
			
		if libs_node != nil then
		
			libs_node.each { |lib_node|
				
				configurations_node = lib_node.find("configuration")
				
				if configurations_node.size > 0
					
					configurations_node.each { |config_node|
					
						if( target_sym.eql?(config_node["name"]) )
						
							additional_deps.push(lib_node["path"])
							break
						end
					}
				
				else
					additional_deps.push(lib_node["path"])
				end
			}		
		end
		
		additional_deps
	end
	
	def VSUtility.get_force_includes( config )
		
		i = 0
		
		forceinclude_nodes = config.xml_data.find("forceincludes/forceinclude")
		# DHM and RMS 2010/11/22 - don't do a seperate query to get the size for array preallocation, 
		# as libxml_ruby has issues without freeing the node set explicitly.
		forceincludes = Array.new(forceinclude_nodes.size) 
	
		forceinclude_nodes.each do |forceinclude_node|
			
			forcei = nil
			forcei = i = forceinclude_node["order"].to_i if forceinclude_node["order"] != nil

			if forceincludes[i] != nil then
			
				0.upto(forceincludes.size - 1) { |j|
				
					if forceincludes[j] == nil then
					
						i = j 
						break
					end
				}
			end
					
			if forcei then
			
				forceincludes[i] = forceincludes[forcei]
				forceincludes[forcei] = forceinclude_node["path"]
			else
					
				forceincludes[i] = forceinclude_node["path"]
			end
			
			i = i + 1
		end
		forceinclude_nodes = nil
			
		forceincludes	
	end
end

end #module ProjBuild

end #module Pipeline
