require 'xml'
require 'pipeline/coding/projbuild/project'
require 'pipeline/coding/projbuild/config'
require 'pipeline/coding/projbuild/generators/vsshared'
require 'pipeline/coding/xml2vsxml'
require 'pipeline/os/path'

module Pipeline

module ProjBuild

class CSharpGenerator
	
	@@log = nil 
	@@unity_build_filenames	= nil # if true filenames that are created have "_unity" in them.
	def set_unity_build_filenames( set )
		@@unity_build_filenames = set
	end

	def get_unity_build_filenames( )
		@@unity_build_filenames
	end


	def CSharpGenerator.log

		@@log = Log.new( 'CSharpGenerator' ) if @@log == nil
		@@log
	end

	def initialize()
	
		@log = CSharpGenerator.log
			
		config = Pipeline::Config.instance

		@p4 = SCM::Perforce.new()
		@p4.port = config.sc_server
		@p4.client = config.sc_workspace
		@p4.user = config.sc_username
		@p4.connect()
		
	end
	
	def export( path, project, options = nil )
		
		return true
	end
	
	def import( path, options = nil )
	
		return scan_csproj(path)
	end
	
	
	def scan_csproj( recurse_dir )

		project_files = []		
		project_files = OS::FindEx::find_files_recurse( recurse_dir )
		
		full_path = ENV['RAGE_DIR'];
		if full_path.nil?
			print "Unable to find RAGE_DIR to process C# projects.\n"
			return false
		end
		
		full_path = OS::Path::combine(full_path, "base/tools/libs/shared/CommonAssemblyInfo.cs")
		
		if File.exist?(full_path) == false
			
			print "Unable to find #{full_path} to link to CSharp projects!\n"
			return false
		end

		project_files.each do |file|
			
			lines = File.readlines(file)
			@p4.run_edit(file)
			
			new_file = File.new(file, "w")
			lines.each do |line|
				
				if line.index("Include") != nil and line.index("CommonAssemblyInfo.cs") != nil
				
					rel_path = OS::Path::make_relative(full_path, File.dirname(file))
					
					rel_path = rel_path.gsub("/", "\\")
					
					line = line.gsub(/\".*framework\\tools\\etc\\projbuild\\CommonAssemblyInfo.cs\"/, "\"#{rel_path}\"")
				end
				
				new_file.write(line)
			end
			
			new_file.close
		end
		
		return true
	end
end

end #module ProjBuild

end #module Pipeline
