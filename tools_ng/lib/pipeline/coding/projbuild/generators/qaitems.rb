require 'xml'
require 'pipeline/coding/projbuild/project'
require 'pipeline/coding/xml2vsxml'
require 'pipeline/coding/projbuild/generators/vsshared'
require 'pipeline/coding/projbuild/scriptutility'
require 'pipeline/os/file'

module Pipeline

module ProjBuild

class QAItemsGeneratorExporter < VSProjShared

	@@log = nil 

	def QAItemsGeneratorExporter.log

		@@log = Log.new( 'qaitemsgenerator' ) if @@log == nil
		@@log
	end
		
	def initialize()
	
		config = Pipeline::Config.instance
	
		@p4 = SCM::Perforce.new()
		@p4.port = config.sc_server
		@p4.client = config.sc_workspace
		@p4.user = config.sc_username
		@p4.connect()
		
		@xproj = []
		@libs = []
		@directories = []
		@all_qaitems_file = nil
		
		@qa_items_vcproj_name = "qaitems"
	end
	
	def process_directory( path ) 
		
		directories = []
		directories = OS::FindEx.find_dirs_recurse(path)

		directories.each do |directory|

			lib_name = File.basename(directory)
			
			guid_files = OS::Path::combine(directory, lib_name + ".guid")
			no_qa_items_file = OS::Path::combine(directory, lib_name + ".no_qaitems")
			if File.exist?(guid_files) == true and File.exist?(no_qa_items_file) == false
				@libs << lib_name
			end
			
			#Look for any QA Items and append any results
			source_files = OS::FindEx::find_files( OS::Path::combine( directory, "*.cpp") )

			grep_exe = get_egrep_exe_path( )
			source_files.each do |source_file|
			
				result = system(grep_exe + ' -h "^QA_ITEM" ' + source_file + ' >> ' + @all_qaitems_file)
			end
			
		end
	end
	
	def import( path, options )	
	
		@root_directory = File.dirname(path)
		
		#TODO:  Is there a need to generate this file.  The old process doesn't appear to use this
		#beyond deleting it once it's been generated.
		@all_qaitems_file = OS::Path::combine(@root_directory, "qaitemstemp.cpp")
		@all_qaitems_file.gsub!("/", "\\")
 		
		@xproj_var = ScriptUtility.acquire_variable(path, "XPROJ")
		if @xproj_var != nil
			@xproj = @xproj_var.split(" ")
		end
		
		@libs_var = ScriptUtility.acquire_variable(path, "LIBS")
		if @libs_var != nil
			@libs = @libs_var.split(" ")
		end
		
		@directories_var = ScriptUtility.acquire_variable(path, "DIRECTORY")
		if @directories_var != nil
			
			@directories_var.gsub!("$(RAGE_DIR)", ENV["RAGE_DIR"])
			@directories = @directories_var.split(" ")
		end
		
		@directories.each do |directory|
			
			print "Gathering QA items from #{directory}...\n"
			process_directory( directory )
		end
		
		#TODO:  Since project generation is only set up, the LIBS environment variable
		#is not being used.  Once the solution management side the ProjBuilder is created
		#attach the LIBS environment variable.
		
		#Generate the project.
		ragegen = RageGenGenerator.new()	
		
		#Output is nil; it is not used since we deduce what to generate from the existing paths.
		result = ragegen.import(path, options)
		
		if File.exists?(@all_qaitems_file) == true
			File.delete(@all_qaitems_file)
		end
		 
		return result, nil
	end

end

class QAItemsGenerator
	
	@@unity_build_filenames	= nil # if true filenames that are created have "_unity" in them.
	def set_unity_build_filenames( set )
		@@unity_build_filenames = set
	end

	def get_unity_build_filenames( )
		@@unity_build_filenames
	end
	
	def export( path, project, options = nil )
		
		true
	end
	
	def import( path, options = nil )
		
		loader = QAItemsGeneratorExporter.new()

		result = loader.import(path, options)

		return result
	end
end

end #module ProjBuild

end #module Pipeline