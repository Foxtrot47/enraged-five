require 'active_support/ordered_hash'
require 'xml'
require 'pipeline/os/path'
require 'pipeline/os/file'
require 'pipeline/coding/projbuild/project'
require 'pipeline/coding/projbuild/generators/vsutility'
require 'pipeline/coding/projbuild/generators/vs2010_sln'
require 'pipeline/coding/projbuild/generators/vs2010_generator'
require 'pipeline/coding/projbuild/generators/vs2010_projloader'

module Pipeline
module ProjBuild

# DW : Handy Links
# http://blogs.msdn.com/b/visualstudio/archive/2010/05/14/a-guide-to-vcxproj-and-props-file-structure.aspx 
# http://blogs.msdn.com/b/vcblog/archive/2010/03/02/visual-studio-2010-c-project-upgrade-guide.aspx

		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		# VS2010ProjectGenerator
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
    class VS2010ProjectGenerator

      PROPERTY_SHEETS_LABEL = "PropertySheets"
      DEFAULT_CPP_PROPERTIES = "$(VCTargetsPath)\\Microsoft.Cpp.Default.props"
      CPP_PROPERTIES = "$(VCTargetsPath)\\Microsoft.Cpp.props"
      UPGRADE_VCPROJ_PROPERTIES = "$(VCTargetsPath)Microsoft.CPP.UpgradeFromVC71.props"
      CPP_TARGETS = "$(VCTargetsPath)\\Microsoft.Cpp.targets"

      USER_MACROS_LABEL = "UserMacros"
      CLCOMPILE_TAG = "ClCompile"
      LINK_TAG = "Link"
      LIBRARY_TAG = "Lib"
      VS2010_PP_SYMBOL="__VS2010"

      COMPILE_EXTENSIONS = [ ".c", ".cpp" ]
      INCLUDE_EXTENSIONS = [ ".h", ".hpp", ".inl" ]

      NONE_TASK = "None"
      INCLUDE_TASK = "ClInclude"
      COMPILE_TASK = "ClCompile"
      CUSTOM_BUILD_TASK = "CustomBuild"

      VERSION_SUFFIX = "_2010"
      CRLF = "&#x0D;&#x0A;"

      CURRENT_TOOLS_VERSION = "4.0"	  #This corresponds to the .NET Framework.
      DEFAULT_XML_NAMESPACE = "http://schemas.microsoft.com/developer/msbuild/2003"

      def initialize( unity_build_filenames )
      	@unity_build_filenames = unity_build_filenames
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def import( path )
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      # --- This is a high level entry point for exporting ---
      # DW - from the internal representation of the project resident in memory
      # export this as a MSBUILD VS2010 vcxproj.
      # This has now been modularised - you dont want to see it as it was before.
      def export( path, filters_filename, project )

				@custom_file_nodes = []

        VS2010_Export_Trace_Log(1,"VS2010 EXPORT : path #{path}")

        File.open(path,"w+") do |file| # DW - I added this - it wasn't clear to me if there was any perforce interaction,
          # but THIS is the perforce interaction here, yes indeed! via the a monkey patched File class see Perforce_file.rb
          # it has caused a fair bit of confusion, but seems to do the job.

          store_to_class(path, filters_filename, project)	# not sure what this is about yet
          project_node = create_project_node(project) # Create the initial project node.

          VS2010_Export_Trace_Log(1,"create_doc")
          doc = create_doc(project_node) # just create the xml doc attaching the project node to the root

          VS2010_Export_Trace_Log(1,"fill_project_node")
          fill_project_node(project, project_node) # convert internal rep and write to project node - already attached to xml doc

          VS2010_Export_Trace_Log(1,"export_project_filters")
          export_project_filters( project ) #Generate the .filters file which contains specific information how the SolutionExplorer displays the file structure, such as filters.

          VS2010_Export_Trace_Log(1,"saving temp.vcxproj")
          temporary_filename = OS::Path.combine(Globals::instance().toolstemp,"temp.vcxproj")
          doc.save(temporary_filename)

          VS2010_Export_Trace_Log(1,"VsXml::convert_to_vsxml")
          res = VsXml::convert_to_vsxml(temporary_filename, path, true)

	# hacked in a fix for unity only settings
	is_unity = path.downcase.include? "_unity"

	lines = File.readlines(path) 
	new_lines = []
	lines.each do |line|
		if line.include?("UNITY_ONLY")
			if (is_unity)
				line =~ /.*(END_UNITY_ONLY).*/i
				line = line.gsub($1,"") if $1
				line =~ /.*(UNITY_ONLY).*/i
				line = line.gsub($1,"") if $1
			else
				line =~ /.*(UNITY_ONLY.*END_UNITY_ONLY).*/i
				line = line.gsub($1,"") if $1
			end									
		end
		new_lines << line
	end	

	File.open(path, 'w') {|f| f.write(new_lines) }




          return res
        end

        nil
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      # stores some variables to the class ( dont know why yet )
      def store_to_class(path, filters_filename, project)
        @filters_filename = filters_filename
        @current_project = project
        @save_folder = File.expand_path(OS::Path::get_directory(path))
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_project_node( project )
        xml_node = XML::Node.new("Project")
        xml_node.attributes["DefaultTargets"] = "Build"
        xml_node.attributes["ToolsVersion"] = CURRENT_TOOLS_VERSION
        xml_node.attributes["xmlns"] = DEFAULT_XML_NAMESPACE
        xml_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      # create the xml doc for the VS2010 project file
      def create_doc(project_node)
        doc = XML::Document.new()
        doc.encoding = XML::Encoding::UTF_8   #Used to create the proper XML header.
        doc.root = project_node
        doc
      end

      #--------------------------------------------------------------------------------------------------------------------------
      # from here on as you should now be able to see - now that this is modular! - we create an XML doc and add nodes into it.
      def fill_project_node( project, project_node )

        VS2010_Export_Trace_Log(1,"create_project_configurations")
        project_node << create_project_configurations_and_make_hash_of_all_configurations(project)

        VS2010_Export_Trace_Log(1,"create_global_properties_node")
        project_node << create_global_properties_node(project)

        VS2010_Export_Trace_Log(1,"create_default_cpp_node")
        project_node << create_default_cpp_node()

        VS2010_Export_Trace_Log(1,"create_configuration_info_nodes")
        nodes = create_configuration_info_nodes(project)
        nodes.each do |node|
          VS2010_Export_Trace_Log(2,"create_configuration_info_nodes : node added")
          project_node << node
        end

        VS2010_Export_Trace_Log(1,"create_defaults_node")
        project_node << create_defaults_node()
        VS2010_Export_Trace_Log(1,"create_import_group_node")
        project_node << create_import_group_node()

        VS2010_Export_Trace_Log(1,"create_project_import_group_nodes")
        nodes = create_project_import_group_nodes(project)
        nodes.each do |node|
          VS2010_Export_Trace_Log(2,"create_project_import_group_nodes : node added")
          project_node << node
        end

        VS2010_Export_Trace_Log(1,"create_user_macros_node")
        project_node << create_user_macros_node()

        VS2010_Export_Trace_Log(1,"create_project_attributes_group_node")
        project_node << create_project_attributes_group_node(project)

        VS2010_Export_Trace_Log(1,"create_specific_configuration_options")
        nodes = create_specific_configuration_options(project)
        nodes.each do |node|
          VS2010_Export_Trace_Log(2,"create_specific_configuration_options : node added")
          project_node << node
        end

        VS2010_Export_Trace_Log(1,"files_export")
        nodes = files_export(project)
        nodes.each do |node|
          VS2010_Export_Trace_Log(2,"files_export : node added")
          project_node << node
        end

        VS2010_Export_Trace_Log(1,"create_targets_project")
        project_node << create_targets_project()

#        VS2010_Export_Trace_Log(1,"create_custom_targets_hack")
#        nodes = create_custom_targets_hack()
#        nodes.each do |node|
#          VS2010_Export_Trace_Log(2,"custom target hack : node added")
#          project_node << node
#        end

        VS2010_Export_Trace_Log(1,"export_project_references")        
        project_refs_group = export_project_references( project ) #Export all project references for this file.
        project_node << project_refs_group if project_refs_group != nil

        VS2010_Export_Trace_Log(1,"create_extension_targets_node")
        project_node << create_extension_targets_node()
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      # creates a node for the VS2010 vcxproj
			# this also stores a hash of all project configurations in the class
      def create_project_configurations_and_make_hash_of_all_configurations(project)
        project_configurations_group = ItemGroup.new("ProjectConfigurations")
        project_configurations_group_node = project_configurations_group.create_node

        @platforms = ActiveSupport::OrderedHash.new()

        project.platforms.each { |platform_sym,platform|
          next unless platform_valid(platform_sym)
          @platforms[platform_sym] = ActiveSupport::OrderedHash.new()

          platform.targets.each { |target_sym,target|
            next unless target_valid(target_sym)

            @platforms[platform_sym][target_sym] = 1

            project_configurations_group_node << create_project_configuration_node(platform,target,platform_sym,target_sym)
          }
        }
        project_configurations_group_node
      end

	#-----------------------------------------------------------------------------------------------------------------------------
	#
	def create_project_configuration_node(platform,target,platform_sym,target_sym)
		platform_name = VSUtility.get_platform_name(platform_sym,true)
		VS2010_Export_Trace_Log(2,"The platform is called #{platform_name}")
		configuration_name = VSUtility.create_config_name(platform_sym,target_sym,true)

		target_name = VSUtility.create_project_configuration_configuration( platform_sym, target_sym, true )
		project_configuration = ProjectConfiguration.new(target_name, platform_name, configuration_name)
		project_configuration.create_node()
	end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_global_properties_node(project)
        global_properties = GlobalProperties.new(project.name, project.guid)
        global_properties_node = global_properties.create_node()
        global_properties_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      # creates an array of nodes for the VS2010 vcxproj
      def create_configuration_info_nodes(project)
        nodes = []
        project.platforms.each do |platform_sym,platform|
          next unless platform_valid(platform_sym)
          platform_config = Info::Config.merge(project.config,platform.config)

          platform.targets.each do |target_sym,target|

            next unless target_valid(target_sym)
            target_config       = Info::Config.merge(platform_config, target)

            nodes << create_project_configuration_info_node(platform_sym,target_sym,target_config)
          end
        end
        nodes
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
		  def create_project_configuration_info_node(platform_sym,target_sym,target_config)
					configuration_name  = VSUtility.create_config_name(platform_sym,target_sym,true)
					config_type         = VSUtility.create_2010_config_type( target_config )
					character_set       = VSUtility.create_2010_character_set( target_config )
					toolset					    = VSUtility.create_2010_platform_toolset( target_config,platform_sym,target_sym )
					exceptions_and_rtti = VSUtility.create_2010_exceptions_and_rtti( target_config,platform_sym,target_sym )

					project_configuration = ConfigurationInfo.new(configuration_name, config_type, character_set, toolset, exceptions_and_rtti)
					project_configuration.create_node()
			end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_project_import_group_nodes(project)
        nodes = []
        projects = Array.new()

        user_props_project = ImportProject.new("$(UserRootDir)\\Microsoft.Cpp.$(Platform).user.props", "LocalAppDataPlatform", "exists('$(UserRootDir)\\Microsoft.Cpp.$(Platform).user.props')")
        projects << user_props_project

        upgrade_project = ImportProject.new(UPGRADE_VCPROJ_PROPERTIES)
        projects << upgrade_project

        project.platforms.each { |platform_sym,platform|
          next unless platform_valid(platform_sym)
          platform.targets.each { |target_sym,target|
            next unless target_valid(target_sym)
            configuration_name = VSUtility.create_config_name(platform_sym,target_sym,true)

            config_import_group = ImportGroup.new(PROPERTY_SHEETS_LABEL, conditional_config(configuration_name), projects)
            config_import_group_node = config_import_group.create_node()
            nodes << config_import_group_node
          }
        }
        nodes
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_specific_configuration_options_platform( project, platform_sym, platform )
        nodes = []
        platform_config = Info::Config.merge( project.config, platform.config )

        platform.targets.each do |target_sym,target|
          next unless target_valid(target_sym)
          new_node = create_specific_configuration_options_platform_target(platform_config, project, platform_sym, platform, target_sym, target )
          VS2010_Export_Trace_Log(2, "new_node #{new_node}")
          nodes << new_node
        end
        nodes
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      # Specific Configuration Options
      def create_specific_configuration_options(project)
        nodes = []
        project.platforms.each do |platform_sym,platform|
          next unless platform_valid(platform_sym)
          nodes += create_specific_configuration_options_platform(project,platform_sym,platform)
        end
        nodes
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_targets_project()
        targets_project = ImportProject.new(CPP_TARGETS)
        targets_project_node = targets_project.create_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
			def create_custom_targets_hack()
				nodes = []
				@custom_file_nodes.each do |custom_file_node|
					 target_node = XML::Node.new("Target")
					 target_node.attributes["Name"] = "HackCustomBuild"
					 target_node.attributes["BeforeTargets"] = "ClCompile"
					 exec_node = XML::Node.new("Exec")

					 include = custom_file_node["Include"]
					 command = custom_file_node.find_first("Command").content
					 command = command.gsub("&&", "&quot;&quot;")
					 command = command.gsub("%(Filename)%(Extension)", include)

					 exec_node.attributes["Command"] = command
					 target_node << exec_node
					 nodes << target_node
				end
				nodes
			end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_extension_targets_node()
        extension_targets = ImportGroup.new("ExtensionTargets")
        extension_targets_node = extension_targets.create_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def	files_export(project)
        nodes = []
        @item_groups =  ActiveSupport::OrderedHash.new()
        @item_groups[INCLUDE_TASK] = ItemGroup.new().create_node()
        @item_groups[COMPILE_TASK] = ItemGroup.new().create_node()
        @item_groups[CUSTOM_BUILD_TASK] = ItemGroup.new().create_node()
        @item_groups[NONE_TASK] = ItemGroup.new().create_node()

	vs2010 = true

        export_files(project, project.path)

        @item_groups.keys.each do |key|
          item_group = @item_groups[key]
          nodes << item_group
        end
        nodes
      end

      #The Custom Build Steps instantiated are currently assuming Visual Studio 2005/2008 macros.
      #Convert these macros into what 2010 uses.
      def convert_macros( line )
        return line if line == nil

        line = line.gsub("$(InputDir)", "%(RootDir)%(Directory)")
        line.gsub!("$(InputName)", "%(Filename)")
        line.gsub!("&quot;", "\"")
        line.gsub!("&amp;", "&")

        line
      end

			#--------------------------------------------------------------------------------------------------------
			# <CustomBuildAfterTargets>ClCompile</CustomBuildAfterTargets>
		  # <CustomBuildBeforeTargets>Link</CustomBuildBeforeTargets>
			# DW:
			def create_custom_build_before_after(configuration_name)
					node = conditional_node("CustomBuildBeforeTargets","InitializeBuildStatus",configuration_name)
			end


      # Creates a Visual Studio 2010-formatted custom build XML node such as:
      #<CustomBuildStep>
      #  <Command Condition="'$(Configuration)|$(Platform)'=='Release|Win32'">TestCommand</Command>
      #  <Outputs Condition="'$(Configuration)|$(Platform)'=='Release|Win32'">include.h;more.h;hello.h</Outputs>
      #  <AdditionalInputs Condition="'$(Configuration)|$(Platform)'=='Release|Win32'">another_exe.exe</AdditionalInputs>
      #  <Message Condition="'$(Configuration)|$(Platform)'=='Release|Win32'">Test Description</Message>
      #</CustomBuildStep>
      #
      # Unfortunately Visual Studio couldn't have picked a more confusing, bloated XML schema for this, requiring a lot
      # more XML than really necessary.
      #
      CUSTOM_BUILD_STEP_ELEMENT_NAME = "CustomBuildStep"
      def convert_to_2010_custom_build_step( file_node, find_node, configuration_name )

        custom_build_step_node = XML::Node.new(CUSTOM_BUILD_STEP_ELEMENT_NAME)

				command = ''
				outputs = ''
				dependencies = ''
        if find_node["CommandLine"] != nil
          	command = convert_macros(find_node["CommandLine"])
					
		# DW: I don't know why this needs to be done yet - this used to use 'remoteroot' - a mistake by the original author?
		if configuration_name.downcase.include?("ps3")
			command = command.gsub("$(InputFileName)", "%(Filename)%(Extension)")
			custom_build_step_node << conditional_node("Command",command,configuration_name)
		else
		  	custom_build_step_node << conditional_node("Command",command,configuration_name)
		end
        end

        if find_node["Outputs"] != nil
          outputs = convert_macros(find_node["Outputs"]) 
          custom_build_step_node << conditional_node("Outputs","#{outputs};%(Outputs)",configuration_name)
        end

        if find_node["AdditionalDependencies"] != nil
          dependencies = convert_macros(find_node["AdditionalDependencies"])
          dependencies = dependencies.gsub(" ","") # another VS2010 quirk , spaces in here is a show stopper.
          dependencies = "" # TODO remove this hack once this the project builder goes live - programmers will have to fix their dodgy dependencies!
          custom_build_step_node << conditional_node("AdditionalInputs","#{dependencies};%(AdditionalInputs)",configuration_name)
        end

        if find_node["Description"] != nil
          description = convert_macros(find_node["Description"])
          custom_build_step_node << conditional_node("Message","#{description} Command #{command.gsub("&&","")} Outputs #{outputs.gsub("&&","")} Dependencies #{dependencies.gsub("&&","")}",configuration_name)
        end

        custom_build_step_node
      end

      def process_custom_build(file, file_config)

        #Initialize custom build data.
        custom_builds = ActiveSupport::OrderedHash.new()

        @platforms.keys.each { |platform|

          custom_builds[platform] = ActiveSupport::OrderedHash.new()
        }


        #Per Wildcard

        #Per Wildcard, Per Platform

        @platforms.keys.each { |platform|

          next if @current_project.platforms[platform].config == nil
          files_nodes = @current_project.platforms[platform].config.find("file")

          next if files_nodes == nil

          files_nodes.each { |file_node|

            next if file_node["extension"] == nil

            if OS::Path.get_extension(file.path) == file_node["extension"] then

              find_node = file_node.find_first("custombuild")

              @platforms[platform].keys.each { |target|

                configuration_name = VSUtility.create_config_name(platform.sym,target,true)

                custom_builds[platform][target] = convert_to_2010_custom_build_step(custom_builds[platform][target], find_node, configuration_name)
              }
            end
          }
        }

        #Per Wildcard, Per Platform, Per Target

        #Per File

        find_node = file_config.find_first("custombuild")

        if find_node != nil

          file.platforms.values.each { |platform|

            if @platforms.has_key?(platform.sym) == true then

              @platforms[platform.sym].keys.each { |target|

                configuration_name = VSUtility.create_config_name(platform.sym, target,true)

                custom_builds[platform.sym][target] = convert_to_2010_custom_build_step(custom_builds[platform.sym][target], find_node, configuration_name)
              }
            end
          }
        end

        #Per File, Per Platform

        file.platforms.values.each { |platform|

          next if platform.config.nil?

          find_node = platform.config.find_first("custombuild")

          if find_node != nil then

            if @platforms.has_key?(platform.sym) == true then

              @platforms[platform.sym].keys.each { |target|

                configuration_name = VSUtility.create_config_name(platform.sym, target,true)

                custom_builds[platform.sym][target] = convert_to_2010_custom_build_step(custom_builds[platform.sym][target], find_node, configuration_name)
              }
            end
          end
        }

        #Per File, Per Platform, Per Target

        ###########################################################################################################
        ###########################################################################################################

        #Processing has been finshed; write the output nodes.
        custom_build_xml = XML::Node.new("CustomBuild")
        custom_build_xml.attributes["Include"] = file.path.gsub("/", "\\")

        has_custom_build_steps = false

        #Custom Build Steps

				# DW : is this required?
#        file_type_node = XML::Node.new("FileType")
 #       file_type_node << "Document"
  #      custom_build_xml << file_type_node

        custom_builds.keys.each { |platform_sym|
          targets = custom_builds[platform_sym]

          targets.each { |target_sym,custom_build|

            custom_build.each_element { |element|

              has_custom_build_steps = true
              custom_build_xml << element.copy(true)
            }

          }
        }

        if has_custom_build_steps == true
          return custom_build_xml
        else
          return nil
        end
      end

      #-----------------------------------------------------------------------------------------------------------------------------------------------------------------
      #
      def process_precompiled_headers(file, file_config)

        fileconfigs = ActiveSupport::OrderedHash.new()
        precomps = ActiveSupport::OrderedHash.new()

        @platforms.keys.each { |platform|

          precomps[platform] = ActiveSupport::OrderedHash.new()
          fileconfigs[platform] = ActiveSupport::OrderedHash.new()
        }

        fileconfigs.keys.each { |platform_sym|
          targets = fileconfigs[platform_sym]

          @platforms[platform_sym].keys.each { |target|

            precomps[platform_sym][target] = false
          }
        }

        #Global
        if OS::Path.get_filename(file_config.root_node["precompiled_header_source"]) == OS::Path.get_filename(file.path) then

          fileconfigs.keys.each { |platform_sym|
            targets = fileconfigs[platform_sym]

            @platforms[platform_sym].keys.each { |target|

              precomps[platform_sym][target] = true
            }
          }
        end

        #Per Platform

        @current_project.platforms.each { |platform_sym,platform|
          next unless platform_valid(platform_sym)
          platform_config = Info::Config.merge(@current_project.config, platform.config)
          platform_file_config = Info::Config.merge(platform_config, file_config)

          next if platform_file_config.root_node["precompiled_header_source"] == nil

          if OS::Path.get_filename(platform_file_config.root_node["precompiled_header_source"]) == OS::Path.get_filename(file.path) then

            @platforms[platform_sym].keys.each { |target|

              precomps[platform_sym][target] = true
            }
          end
        }

        #Precompiled Headers
        #    <ClCompile Include="..\..\vector\vec.cpp">
        #		<PrecompiledHeader Condition="'$(Configuration)|$(Platform)'=='Release|Win32'">Create</PrecompiledHeader>
        #		<PrecompiledHeaderFile Condition="'$(Configuration)|$(Platform)'=='Release|Win32'">test_pch.h</PrecompiledHeaderFile>
        #	 </ClCompile>
        pch_xml = XML::Node.new(CLCOMPILE_TAG)
        pch_xml.attributes["Include"] = file.path.gsub("/", "\\")
        has_pch_nodes = false

        precomps.each { |platform_sym,targets|

          targets.each { |target_sym,enabled|

            if enabled then

              has_pch_nodes = true
              configuration_name = VSUtility.create_config_name(platform.sym, target,true)
              pch_xml << conditional_node("PrecompiledHeader","Create",configuration_name)

              if platform_sym == :ps3 then
                pch_xml << conditional_node("PrecompiledHeaderFile","$(InputName).h.gch/$(ProjectName)_$(ConfigurationName).h.gch",configuration_name)
              end

            end
          }
        }

        if has_pch_nodes == true
          return pch_xml
        else
          return nil
        end
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def export_files( filter, path )

        filter_config = Info::Config.merge(@current_project.config, filter.config)
				
        @file_task_map = Hash.new()
        @file_task_map[INCLUDE_TASK] = INCLUDE_EXTENSIONS
        @file_task_map[COMPILE_TASK] = COMPILE_EXTENSIONS

        filter.files.each { |file|

          file_config = Info::Config.merge(filter_config,file.config)

          #Determine whether this file should be part of the Include or Compile item groups.
          #Default to "None" -- a valid item group for files that have no actions associated with them. (e.g. readme.txt)
          current_task = NONE_TASK
          xml_file_node = nil

          #Process the custom build steps or precompiled headers if one exists for this file.
          #Otherwise, process this file by extensions as to whether it should receive a compile or include task.
          custom_file_node = process_custom_build(file, file_config)
          
          # DW : Store the custom file node so we can hack these to work later...          
          @custom_file_nodes << custom_file_node if custom_file_node
          
          
          pch_file_node = process_precompiled_headers(file, file_config)
          exclude_from_build_node = process_excluded_from_build(file, file_config)

          if custom_file_node != nil
            file.has_custom_build_steps = true
            current_task = CUSTOM_BUILD_TASK
            xml_file_node = custom_file_node
          elsif exclude_from_build_node != nil
            current_task = COMPILE_TASK
            xml_file_node = exclude_from_build_node
          elsif pch_file_node != nil

            current_task = COMPILE_TASK
            xml_file_node = pch_file_node
          else

            extension = File.extname(file.path)
            @file_task_map.each_pair { |key, values|

              if values.include?(extension) == true
                current_task = key
                break
              end
            }

            xml_file_node = XML::Node.new(current_task)

            out_path = OS::Path.combine(path,file.path)
            out_path = File.expand_path(out_path)
            out_path = OS::Path.make_relative(out_path,@save_folder)
            out_path = OS::Path.dos_format(out_path)

            xml_file_node.attributes["Include"] = out_path
          end

          @item_groups[current_task] << xml_file_node
        }

	filter.filters.compact!

        filter.filters.each { |child_filter|

          new_path = OS::Path.combine(path,child_filter.path)
          export_files(child_filter,new_path)
        }
      end

			#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
      #
      def export_project_references( project )

        item_group = nil

        #VS2010Generator.log.info("export_project_references #{project.config.xml_data}")

        project.config.xml_data.find("ProjectReferences/ProjectReference").each { |reference_node|

          #VS2010Generator.log.info("reference_node #{reference_node}")

          if item_group == nil
            item_group = ItemGroup.new()
            item_group = item_group.create_node()
          end

          project_path = reference_node["path"] # Dw - this looks just wrong?... but I dunno

          project_path = reference_node["Include"] if project_path.nil?
	  
          project_path = File.expand_path(project_path)

          if File.exists?(project_path) == true

            project_path = OS::Path.make_relative(project_path, project.path)

            project_reference_node = XML::Node.new("ProjectReference")
            project_reference_node["Include"] = project_path
            project_condition = reference_node["Condition"]
            #project_reference_node["Condition"] = project_condition if project_condition

            #Project References require the GUID of the project.
            #Open the project XML, parse it and acquire the GUID via an XPATH query.
            #NOTE: Opening up the xguid files is a likely candidate for an alternative,
            #but not all 2010 projects are guaranteed to have that file.

            VS2010_Export_Trace_Log(2,"Parsing: #{project_path}")
            doc = XML::Document.file(project_path)
            context = XML::XPath::Context.new(doc)
            context.register_namespace('xmlns', DEFAULT_XML_NAMESPACE)

            project_guid_node = context.find("//xmlns:Project/xmlns:PropertyGroup[@Label='Globals']/xmlns:ProjectGuid").first
            project_guid = project_guid_node.find_first("text()")

            guid_node = XML::Node.new("Project")
            guid_node << project_guid.copy(true)
            project_reference_node << guid_node

            reference_output_node = XML::Node.new("ReferenceOutputAssembly")
            reference_output_node << "false"
            project_reference_node << reference_output_node

            item_group << project_reference_node
          else
            print "Unable to find project #{project_path}!  This project will be excluded.\n"
          end
        }

        return item_group
      end

    end #end class

		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		# SIMPLE NODE CREATION	( partial class )
		class VS2010ProjectGenerator
      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_filters_node( )
        xml_node = XML::Node.new("Project")
        xml_node.attributes["ToolsVersion"] = CURRENT_TOOLS_VERSION
        xml_node.attributes["xmlns"] = DEFAULT_XML_NAMESPACE
        xml_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      # creates a node for the VS2010 vcxproj
      def create_default_cpp_node()
        defaults = ImportProject.new(DEFAULT_CPP_PROPERTIES)
        defaults_node = defaults.create_node()
        defaults_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      # creates a node for the VS2010 vcxproj
      def create_defaults_node()
        #<Import Project="$(VCTargetsPath)\Microsoft.Cpp.props" />
        defaults = ImportProject.new(CPP_PROPERTIES)
        defaults_node = defaults.create_node()
        VS2010_Export_Trace_Log(2,"defaults_node #{defaults_node}")
        defaults_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_import_group_node()
        extension_projects = Array.new()  #Intentionally left empty.
        import_group = ImportGroup.new("ExtensionSettings", nil, extension_projects)
        import_group_node = import_group.create_node()
        VS2010_Export_Trace_Log(2,"import_group_node #{import_group_node}")
        import_group_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_user_macros_node()
        user_macros = PropertyGroup.new(USER_MACROS_LABEL)
        user_macros_node = user_macros.create_node
        VS2010_Export_Trace_Log(2,"user_macros_node #{user_macros_node}")
        user_macros_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_project_file_version_node()
				project_file_version = ProjectFileVersion.new()
				project_file_version_node = project_file_version.create_node
        VS2010_Export_Trace_Log(2,"project_file_version_node #{project_file_version_node}")
        project_file_version_node
      end

		end #end class

		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		# Excluded from build nodes
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		class VS2010ProjectGenerator
      #-----------------------------------------------------------------------------------------------------------------------------------------------------------------
      #	DW : trying to fathom this out - the idea in my mind is to create the file node that has excluded from build in it - for each platform / target
      def process_excluded_from_build(file, file_config)

        return nil unless file.excluded_from_build

        clcompile_xml_node = create_clcompile_node(file)

        #Iterate through every configuration
        @current_project.platforms.each do |platform_sym,platform|
          next unless platform_valid(platform_sym)

          platform.targets.each do |target_sym,target|
            next unless target_valid(target_sym)
            configuration_name = VSUtility.create_config_name(platform_sym, target_sym,true)
            clcompile_xml_node << conditional_node("ExcludedFromBuild","true",configuration_name)
          end
        end

        clcompile_xml_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------------------------------------------
      #
      def create_clcompile_node(file)
        clcompile_xml = XML::Node.new(CLCOMPILE_TAG)
        clcompile_xml.attributes["Include"] = file.path.gsub("/", "\\")
        clcompile_xml
      end

		end #end class


		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		# Debug & utils
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		class VS2010ProjectGenerator
      #-----------------------------------------------------------------------------------------------------------------------------
      # quick hacked in log helper method... makes it clear what this process does by prepending numerical stages.
      def VS2010_Export_Trace_Log( depth, str )
        #@debug_tracer = DebugTracer.new() unless @debug_tracer
        #VS2010Generator.log.info("VS2010 EXPORT : #{@debug_tracer.next_stage(depth)} #{str}") if @debug_tracer.enabled
      end

			#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
      # For debugging really...
      def platform_valid(platform)
      	true
        #platform.to_s.downcase == "ps3" # win32 is used by ps3?       
      end

			#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
			#
      def target_valid(target)
        true
        #target.to_s.downcase == "beta"# "beta"
      end

			#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
      #libxml - xpath quirk - temp fix.
      def make_node_xpath_searchable(node)
        doc = XML::Document.new()
        doc.root = node
      end

			#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
      #libxml - xpath quirk - temp fix.
      def unmake_node_xpath_searchable(node)
        node.remove!
      end

			#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
      #
			def conditional_config(configuration_name)
				"'$(Configuration)|$(Platform)'=='#{configuration_name}'"
			end

			#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
      #
			def conditional_node( name, content, configuration_name )
				node = XML::Node.new(name)
				node.attributes["Condition"] = conditional_config(configuration_name)
				node << content if content
				node
			end

		end #end class

		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		# PROJECT ATTRIBUTES GROUP NODE
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		class VS2010ProjectGenerator
      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_project_attributes_group_node(project)

        project_attributes_group = PropertyGroup.new(nil)
        project_attributes_group_node = project_attributes_group.create_node

	#Add the project file version node.
        VS2010_Export_Trace_Log(2,"create_project_file_version_node")
	project_attributes_group_node << create_project_file_version_node()

	#Iterate through every configuration to create the input and output directories.
	project.platforms.each do |platform_sym,platform|
          next unless platform_valid(platform_sym)
		platform_config = Info::Config.merge(project.config,platform.config)

		platform.targets.each do |target_sym,target|
        	    next unless target_valid(target_sym)
			target_config = Info::Config.merge(platform_config,target)
			configuration_name = VSUtility.create_config_name(platform_sym,target_sym,true)

			VS2010_Export_Trace_Log(2,"create_output_directory_node #{platform_sym} #{target_sym}")
			project_attributes_group_node << create_output_directory_node(target_config,configuration_name)
			VS2010_Export_Trace_Log(2,"create_intermediate_directory_node #{platform_sym} #{target_sym}")
			project_attributes_group_node << create_intermediate_directory_node(configuration_name)

			is_library = (platform_config.root_node.attributes["type"] != nil) and (platform_config.root_node.attributes["type"].intern == :library)

			if not is_library and platform_sym == :win32
				#puts "Creating hardcoded paths for include and libraries"
				project_attributes_group_node << create_include_path_node_hardcoded_win32(configuration_name) 
				project_attributes_group_node << create_library_path_node_hardcoded_win32(configuration_name)
			else

				VS2010_Export_Trace_Log(2,"create_include_path_node #{platform_sym} #{target_sym}")
				node = create_include_path_node(target_config,configuration_name)
				project_attributes_group_node << node if node

				VS2010_Export_Trace_Log(2,"create_library_path_node #{platform_sym} #{target_sym}")
				node = create_library_path_node(target_config,configuration_name)
				project_attributes_group_node << node if node
			end
			
			if (platform_sym.to_s.include?("360") and is_library)
				VS2010_Export_Trace_Log(2,"create_output_file_node #{platform_sym} #{target_sym}")
				project_attributes_group_node << create_output_file_node(configuration_name)
			end

			VS2010_Export_Trace_Log(2,"create_custom_build_before_after #{platform_sym} #{target_sym}")
			project_attributes_group_node << create_custom_build_before_after(configuration_name)

			# DW : This seemed to be missing - I'm hacking it in for 360 only		
			is_library = (platform_config.root_node.attributes["type"] != nil) and (platform_config.root_node.attributes["type"].intern == :library)
			if (platform_sym.to_s.include?("360") and not is_library)
				VS2010_Export_Trace_Log(2,"add_360_project_attributes_group_nodes #{platform_sym} #{target_sym}")
				add_360_project_attributes_group_nodes(project_attributes_group_node, configuration_name,platform_sym,target_sym)
			end

			if (platform_sym.to_s.downcase.include?("ps3") and is_library)
				VS2010_Export_Trace_Log(2,"add_ps3_project_attributes_group_nodes #{platform_sym} #{target_sym}")
				add_ps3_project_attributes_group_nodes(project_attributes_group_node, configuration_name)
			end

          	end
        end
        project_attributes_group_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_output_directory_node( target_config, configuration_name )
        #Output Directory
	out_directory_node = conditional_node( "OutDir", nil, configuration_name )

	# DW the trailing slash is a fickle matter
	
	# DW : needs to be unique across unity / non unity 
	unity_suffix = ""
	if (@unity_build_filenames)
		VS2010_Export_Trace_Log(2,"out_directory_node will has a unity suffix")
		unity_suffix = "_unity"
	end	
	
        if target_config.xml_data["output_path"] != nil
          out_directory_node << "#{target_config.xml_data['output_path']}#{unity_suffix}#{VERSION_SUFFIX}\\"
        else
          out_directory_node << "$(Platform)_$(Configuration)#{unity_suffix}#{VERSION_SUFFIX}\\"
        end
        VS2010_Export_Trace_Log(2,"out_directory_node #{out_directory_node}")
        out_directory_node
      end
      
      
      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_include_path_node( target_config, configuration_name )
	include_path_node = conditional_node( "IncludePath", nil, configuration_name )

        if target_config.xml_data["include_path"] != nil
          include_path_node << "#{target_config.xml_data['include_path']}"
        else
          return nil
        end
        VS2010_Export_Trace_Log(2,"include_path_node #{include_path_node}")
        include_path_node
      end      
      
      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_library_path_node( target_config, configuration_name )
	library_path_node = conditional_node( "LibraryPath", nil, configuration_name )

        if target_config.xml_data["library_path"] != nil
          library_path_node << "#{target_config.xml_data['library_path']}"
        else
          return nil
        end
        VS2010_Export_Trace_Log(2,"library_path_node #{library_path_node}")
        library_path_node
      end          
      
      
      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_output_file_node( configuration_name )
        #Output File
	out_directory_node = conditional_node( "OutputFile", "$(OutDir)$(ProjectName).lib", configuration_name )
        VS2010_Export_Trace_Log(2,"out_directory_node #{out_directory_node}")
        out_directory_node
      end


      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_intermediate_directory_node(configuration_name)
        #Intermediate Directory
	intermediate_directory_node = conditional_node( "IntDir", "$(OutDir)", configuration_name )
        VS2010_Export_Trace_Log(2,"intermediate_directory_node #{intermediate_directory_node}")
        intermediate_directory_node
      end
      
      #-----------------------------------------------------------------------------------------------------------------------------
      # Interim fix for DX builds
      def create_include_path_node_hardcoded_win32(configuration_name)
	include_path_node = conditional_node( "IncludePath", "$(IncludePath);$(DXSDK_DIR)/Include", configuration_name )
        VS2010_Export_Trace_Log(2,"include_path_node #{include_path_node}")
        include_path_node
      end
      
      #-----------------------------------------------------------------------------------------------------------------------------
      # Interim fix for DX builds
      def create_library_path_node_hardcoded_win32(configuration_name)
	library_path_node = conditional_node( "LibraryPath", "$(LibraryPath);$(DXSDK_DIR)/Lib/x86", configuration_name )
        VS2010_Export_Trace_Log(2,"library_path_node #{library_path_node}")
        library_path_node
      end      
      
end #end class


		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		# LIBRARY NODE
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		class VS2010ProjectGenerator
			#-----------------------------------------------------------------------------------------------------------------------------
			#
			def create_library_node(target_config, additional_library_deps, platform_sym)

				library_node = XML::Node.new("Lib")
				library_source = target_config.xml_data.find_first("vc/#{LIBRARY_TAG}")

				library_node = library_source.copy(true) if library_source != nil
				library_node = clean_xml_copy(library_source) if library_source != nil

				# DW I can't understand this... it create two lib dependencies nodes... actually it needs to in the case of binkxenon.lib at least
								
				library_node << create_library_additional_dependencies_node(library_source,additional_library_deps) #unless platform_sym.to_s.downcase.include?("ps3")				
				
				# DW - this confuses me - why was this done? -
				if (library_source)
					library_node << create_library_additional_options_node() unless (library_source.find_first("AdditionalOptions")) #unless (platform_sym == :ps3 or library_source.find_first("AdditionalOptions"))
				end
				#library_node << create_library_output_file_node() # DW : not sure if this is required?

				module_definition_node = create_library_module_definition_node(target_config)
				library_node << module_definition_node if module_definition_node
				VS2010_Export_Trace_Log(5, "create_library_node #{library_node}")
				library_node
			end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_library_additional_dependencies_node(library_source,additional_library_deps)
        additional_dependencies_node = nil
        additional_dependencies_node = library_source.find_first("AdditionalDependencies") if library_source != nil

	

        if additional_dependencies_node == nil
          create_element("AdditionalDependencies", additional_library_deps.join(";"))
        else
          copied_dep_node = additional_dependencies_node.copy(true)
          copied_dep_node << ";" + additional_library_deps.join(";") if additional_library_deps.size > 0
        end
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_library_additional_options_node()
        #additional_options_node = XML::Node.new("AdditionalOptions","\/ignore:4221 %(AdditionalOptions)")
        additional_options_node = XML::Node.new("AdditionalOptions","%(AdditionalOptions)")
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_library_output_file_node()
        output_file_node = XML::Node.new("OutputFile","$(OutDir)$(ProjectName).lib")
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      # Module Definition File
      def create_library_module_definition_node(target_config)
        module_definition_file = nil
        module_definition_file = target_config.xml_data["module_definition_file"] if target_config.xml_data != nil
        return create_element("ModuleDefinitionFile", module_definition_file) if module_definition_file != nil
        nil
      end

		end #end class


		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		# LINKER NODE
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		class VS2010ProjectGenerator
      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_linker_node(target_config, additional_library_deps)
        #Executable Configuration
        #TODO:  Xbox 360 Executable Support -- this iteration of the 2010 generator was done before 360 XDK support.
        link_source = target_config.xml_data.find_first("vc/#{LINK_TAG}")
        linker_node = XML::Node.new("Link")
        make_node_xpath_searchable(linker_node)
        linker_node = clean_xml_copy(link_source) if link_source != nil

        VS2010_Export_Trace_Log(4, "create_linker_output_file_node")
        linker_output_node = create_linker_output_file_node(target_config, link_source)
        linker_node << linker_output_node if linker_output_node

        VS2010_Export_Trace_Log(4, "create_linker_additional_dependencies_node")
        linker_additional_dependencies_node = create_linker_additional_dependencies_node(additional_library_deps, link_source)
        linker_node << linker_additional_dependencies_node if linker_additional_dependencies_node

	# DW : added these - these settings were missings - although not sure yet which builds require map file generation - presumably all.
        VS2010_Export_Trace_Log(4, "create_linker_map_filename_node")
        linker_map_filename_node = create_linker_map_filename_node()                      
        linker_node << linker_map_filename_node if linker_map_filename_node
                
        #VS2010_Export_Trace_Log(4, "create_generate_map_file_node")
        #linker_generate_map_file_node = create_generate_map_file_node()
        #linker_node << linker_generate_map_file_node if linker_generate_map_file_node        
        
        VS2010_Export_Trace_Log(4, "Add VCLinkerTool")
        # <Tool Name="VCLinkerTool" AdditionalOptions="-strip-unused-data -Wl,--disable-warning=134" AdditionalDependencies="-lm_stub" OutputFile="$(RS_BUILDBRANCH)/game_psp2_profile.self" LinkIncremental="0" AdditionalLibraryDirectories="&quot;$(RAGE_DIR)\lib&quot;;&quot;$(SCE_PSP2_SDK_DIR)\target\lib&quot;" GenerateManifest="false"/>
        translate_and_insert_configuration_node(linker_node, nil, target_config, "Link", "VCLinkerTool", ["AdditionalOptions", "AdditionalDependencies", "OutputFile", "AdditionalLibraryDirectories", "GenerateMapFile"] )
        
        VS2010_Export_Trace_Log(4, "Add VCX360LinkerTool")
        #<Tool Name="VCX360LinkerTool" AdditionalDependencies="xboxkrnl.lib" OutputFile="$(RS_BUILDBRANCH)/game_xenon_profile.xex" LinkIncremental="1" AdditionalLibraryDirectories="&quot;$(RAGE_DIR)\lib&quot;" IgnoreDefaultLibraryNames="xapilib.lib" GenerateDebugInformation="true" OptimizeReferences="2" EnableCOMDATFolding="2"/>
        translate_and_insert_configuration_node(linker_node, nil, target_config, "Link", "VCX360LinkerTool", [ "AdditionalDependencies", "OutputFile", "LinkIncremental", "AdditionalLibraryDirectories", "IgnoreDefaultLibraryNames", "GenerateDebugInformation", "OptimizeReferences", "EnableCOMDATFolding", "GenerateMapFile"] )

        VS2010_Export_Trace_Log(4, "append_linker_node_settings")
        append_linker_node_settings(linker_node)

        unmake_node_xpath_searchable(linker_node)

        linker_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_linker_map_filename_node()
      	return create_element("MapFileName", "$(TargetDir)$(TargetName).map")
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_generate_map_file_node()
      	return create_element("GenerateMapFile", "")
      end
	
      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_linker_output_file_node(target_config, link_source)
        #An available override in the project definition for the outputted file name.
        #If an OutputFile attribute is available in the VCLinkerTool already, then
        #we replace only the filename.  If not, use the OutputFileName as a full path.
        if target_config.xml_data.attributes["output_file_name"] != nil

          outputfile_node = nil
          outputfile_node = link_source.find_first("OutputFile") if link_source != nil
          if outputfile_node != nil

            output_file_name = target_config.xml_data.attributes["output_file_name"]
            output_file_path = File.dirname( outputfile_node.find_first("text()") )
            output_file_path = OS::Path.combine(output_file_path, output_file_name)

            return create_element("OutputFile", output_file_path)
          else
            return create_element("OutputFile", target_config.xml_data.attributes["output_file_name"])
          end
        end
        nil
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_linker_additional_dependencies_node(additional_library_deps, link_source)
        additional_dependencies_node = nil
        additional_dependencies_node = link_source.find_first("AdditionalDependencies") if link_source != nil

        if additional_dependencies_node == nil
          VS2010_Export_Trace_Log(4,"additional_dependencies_node == nil")
          return create_element("AdditionalDependencies", additional_library_deps.join(";"))
        else
          VS2010_Export_Trace_Log(4,"additional_dependencies_node != nil")
          copied_dep_node = additional_dependencies_node.copy(true)
          copied_dep_node << " " + additional_library_deps.join(" ") if additional_library_deps.size > 0
          return copied_dep_node
        end
        nil
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      # I haven;t the foggiest where these settings come from so this is a dodgy hack - and is done by hand - if somebody knows the origin of such settings please do fix! - butI don;t have the time to investigate!
      def append_linker_node_settings(linker_node)
        append_or_create_node(linker_node,"AdditionalLibraryDirectories",";%(AdditionalLibraryDirectories)")
        append_or_create_node(linker_node,"IgnoreSpecificDefaultLibraries",";%(IgnoreSpecificDefaultLibraries)")
      end

		end #end class


		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		# Compile Node
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		class VS2010ProjectGenerator
      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_compile_node( platform_config, target_config,platform_sym )
        #C++ Compilation
        compile_node = XML::Node.new(CLCOMPILE_TAG)
        xml_source = target_config.xml_data.find_first("vc/#{CLCOMPILE_TAG}")
        compile_node = clean_xml_copy(xml_source) if xml_source != nil
        make_node_xpath_searchable(compile_node)

        VS2010_Export_Trace_Log(4, "create_compile_node_additional_include_directories")
        compile_node << create_compile_node_additional_include_directories(target_config)
        VS2010_Export_Trace_Log(4, "create_compile_node_preprocessor_defines")
	if (platform_sym == :ps3)
		compile_node << create_compile_node_preprocessor_defines_ps3(target_config)
	else
		compile_node << create_compile_node_preprocessor_defines(target_config)
	end
        VS2010_Export_Trace_Log(4, "create_compile_node_force_includes")
        compile_node <<  create_compile_node_force_includes(target_config)

        VS2010_Export_Trace_Log(4, "create_compile_node_precompiled_header")
        nodes = create_compile_node_precompiled_header(target_config,xml_source)
        nodes.each do |node|
          compile_node << node
        end

        VS2010_Export_Trace_Log(4, "create_compile_node_warning_level")
        warning_level_node = create_compile_node_warning_level(target_config)
        compile_node << warning_level_node

        VS2010_Export_Trace_Log(4, "create_compile_node_warning_as_errors")
        warning_as_errors_node = create_compile_node_warning_as_errors(target_config)
        compile_node << warning_as_errors_node

	if (platform_sym == :ps3)
	        VS2010_Export_Trace_Log(4, "create_disable_specific_warnings_ps3")
	        disable_specific_warnings = create_disable_specific_warnings_ps3(target_config)
	        compile_node << disable_specific_warnings
	end
	
        VS2010_Export_Trace_Log(4, "create_compile_node_warning_as_errors")
        warning_as_errors_node = create_compile_node_warning_as_errors(target_config)
        compile_node << warning_as_errors_node
	
#        VS2010_Export_Trace_Log(4, "create_compile_node_optimization_level")
#        optimization_level_node = create_compile_node_optimization_level(target_config)
#        compile_node << optimization_level_node if optimization_level_node

	is_library = (platform_config.root_node.attributes["type"] != nil) and (platform_config.root_node.attributes["type"].intern == :library)
	
        #Library Configuration
        if not is_library
          VS2010_Export_Trace_Log(4, "This project is not a library.")
          VS2010_Export_Trace_Log(4, "translate_and_insert_configuration_node VCCLCompilerTool")
          ##################################################################################################################################
          # <Tool Name="VCCLCompilerTool" AdditionalOptions="-Os -g -Xquit=1 -Xc+=rtti -Xc-=exceptions" AdditionalIncludeDirectories="&quot;$(RAGE_DIR)\stlport\STLport-5.0RC5\stlport&quot;;..;%RAGE_DIR%\base\src;%RAGE_DIR%\suite\src;%RAGE_DIR%\script\src;%RAGE_DIR%\framework\src;%RAGE_DIR%\stlport\STLport-5.0RC5\src" PreprocessorDefinitions="SN_TARGET_PSP2;;NDEBUG;__SNC__" ProgramDataBaseFileName="$(RS_BUILDBRANCH)/game_psp2_final.pdb" DebugInformationFormat="0" CompileAs="0" ForcedIncludeFiles="$(RAGE_DIR)\base\src\forceinclude\psp2_final.h;basetypes.h;game_config.h"/>
          translate_and_insert_configuration_node(compile_node, nil, target_config, "ClCompile", "VCCLCompilerTool", ["AdditionalOptions", "ProgramDataBaseFileName"] )

          # for 360 this needs supported - got to work out the translations if required first
          #<Tool Name="VCCLX360CompilerTool" AdditionalOptions="/GF /callcap /QVMX128" Optimization="2" InlineFunctionExpansion="1" EnableIntrinsicFunctions="true" AdditionalIncludeDirectories="&quot;$(RAGE_DIR)\stlport\STLport-5.0RC5\stlport&quot;;..;%RAGE_DIR%\base\src;%RAGE_DIR%\suite\src;%RAGE_DIR%\script\src;%RAGE_DIR%\framework\src;%RAGE_DIR%\stlport\STLport-5.0RC5\src" PreprocessorDefinitions=";WIN32;NDEBUG;_WINDOWS" ExceptionHandling="0" RuntimeLibrary="0" BufferSecurityCheck="false" EnableFunctionLevelLinking="false" ForceConformanceInForLoopScope="true" RuntimeTypeInfo="true" UsePrecompiledHeader="0" ProgramDataBaseFileName="$(RS_BUILDBRANCH)/game_xenon_profile.pdb" WarningLevel="4" WarnAsError="true" DebugInformationFormat="3" CompileAs="0" ForcedIncludeFiles="$(RAGE_DIR)/base/src/forceinclude/xenon_profile.h;basetypes.h;game_config.h"/>
          translate_and_insert_configuration_node(compile_node, nil, target_config, "ClCompile", "VCCLX360CompilerTool", ["AdditionalOptions", "Optimization", "InlineFunctionExpansion", "EnableIntrinsicFunctions", "AdditionalIncludeDirectories", "PreprocessorDefinitions", "ExceptionHandling", "RuntimeLibrary", "BufferSecurityCheck", "EnableFunctionLevelLinking", "ForceConformanceInForLoopScope", "RuntimeTypeInfo", "UsePrecompiledHeader", "ProgramDataBaseFileName", "WarningLevel", "WarnAsError", "DebugInformationFormat", "CompileAs", "ForcedIncludeFiles", "StringPooling"] )

          VS2010_Export_Trace_Log(4, "append_compile_node_settings")
          append_compile_node_settings(compile_node)
	else
          VS2010_Export_Trace_Log(4, "This project is a library.")

          VS2010_Export_Trace_Log(4, "append_compile_node_settings")
          append_compile_node_settings(compile_node)
        end

        unmake_node_xpath_searchable(compile_node)
        compile_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_compile_node_additional_include_directories(target_config)
        #Additional Include Directories
        #First, take the include paths specified by the configuration template.  Then combine with the
        #project definition itself.
        inc_paths = []
        target_config.xml_data.find("includepaths/includepath").each { |include_path| inc_paths << include_path.attributes["path"] }
        inc_paths << "%(AdditionalIncludeDirectories)"
        
        # remove any duplicates
        inc_paths.each do |path|        	
        	path = path.strip
        end
        
        inc_paths.uniq!
        
        create_element("AdditionalIncludeDirectories", inc_paths.join(";"))
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      # DW: dont get confused with USerPreProcessorDefinitions and PreprocessorDefinitions - for some reason it needs split out for PS3
      def create_compile_node_preprocessor_defines_ps3(target_config)
        #Preprocessor Definitions
        defines = []
        target_config.xml_data.find("defines/define").each { |define_node| defines << define_node["value"] }
        defines << "#{VS2010_PP_SYMBOL};%(PreprocessorDefinitions)"
        create_element("PreprocessorDefinitions", defines.join(";"))
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      # DW: dont get confused with USerPreProcessorDefinitions and PreprocessorDefinitions - for some reason it needs split out for PS3
      def create_compile_node_preprocessor_defines(target_config)
        #Preprocessor Definitions
        defines = []
        target_config.xml_data.find("defines/define").each { |define_node| defines << define_node["value"] }
        defines << "#{VS2010_PP_SYMBOL};%(PreprocessorDefinitions)"
        create_element("PreprocessorDefinitions", defines.join(";"))
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_compile_node_force_includes(target_config)
        #Force Includes
        forceincludes = VSUtility.get_force_includes(target_config)
        forceincludes << "%(ForcedIncludeFiles)"
        create_element("ForcedIncludeFiles", forceincludes.join(";"))
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_compile_node_optimization_level(target_config)
				defines = []
        target_config.xml_data.find("vc/VCCLCompilerTool").each do |compile_settings|
						options = compile_settings.attributes["AdditionalOptions"]
						if (options)
							if (options.include?("-Od"))
								return create_element("OptimizationLevel", "Leveld")
							end
						end
				end
				nil
      end


      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_compile_node_precompiled_header(target_config, xml_source)
        nodes = []
        #Precompiled headers.
        platform_precompiled_header = nil
        platform_precompiled_header = target_config.xml_data["UsePrecompiledHeader"]  if target_config.xml_data != nil

        did_set_pch = false

        if platform_precompiled_header != nil then

          nodes << create_element("PrecompiledHeaderFile", platform_precompiled_header)

          if create_precompiled_header == true
            nodes << create_element("PrecompiledHeader", "Create")
            did_set_pch = true
          else
            nodes << create_element("PrecompiledHeader", "Use")
            did_set_pch = true
          end
        else

          #Do not use precompiled headers.
          if xml_source != nil and xml_source.find("PrecompiledHeaderFile") == nil

            nodes << create_element("PrecompiledHeaderFile", " ")
            did_set_pch = true
          end

          if xml_source != nil and xml_source.find("PrecompiledHeader") == nil

            nodes << create_element("PrecompiledHeader", "NotUsing")
            did_set_pch = true
          end

          if not did_set_pch
            nodes << create_element("PrecompiledHeader", "NotUsing")
            nodes << create_element("PrecompiledHeaderFile","")
          end
        end
        nodes
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_compile_node_warning_level(target_config)
        warning_level = nil
        warning_level = target_config.xml_data["warning_level"] if target_config.xml_data != nil

        if warning_level != nil
          if warning_level == "0"
            return create_element("WarningLevel", "TurnOffAllWarnings")
          else
            return create_element("WarningLevel", "Level" + warning_level)
          end
        end
        nil
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_compile_node_warning_as_errors(target_config)
        warnings_as_errors = nil
        warnings_as_errors = target_config.xml_data["warnings_as_errors"] if target_config.xml_data != nil

        if warnings_as_errors != nil
          if warnings_as_errors == "false" or warnings_as_errors == "0"
            return create_element("WarnAsError", "false")
          else
            return create_element("WarnAsError", "true")
          end
        end
        nil
      end
      
      
      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_disable_specific_warnings_ps3(target_config)
	#disable_specific_warnings = nil
	#disable_specific_warnings = target_config.xml_data["disable_specific_warnings"] if target_config.xml_data != nil
	#
	#if disable_specific_warnings != nil
	#    return create_element("DisableSpecificWarnings", disable_specific_warnings.to_s)
	#end
	#DW - FFS I hardcoded this because it's way too complex to update the template with new fields, as the template is written in VS2008 for VS2010
	return create_element("DisableSpecificWarnings", "552;178")
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def append_compile_node_settings(compile_node)
        append_or_create_node(compile_node,"AdditionalOptions"," %(AdditionalOptions)")
      end

		end #end class




		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		# 360 / PS3 specific Nodes
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		class VS2010ProjectGenerator
      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def add_360_project_attributes_group_nodes(project_attributes_group_node, configuration_name, platform_sym, target_sym)
        
	output_file_node = conditional_node("OutputFile","$(OutDir)$(TargetName)$(TargetExt)",configuration_name) #"$(RS_BUILDBRANCH)/game_xenon_#{target_sym}.exe",configuration_name) 
	project_attributes_group_node << output_file_node
	VS2010_Export_Trace_Log(2,"output_file_node #{output_file_node}")

        link_incremental_node = conditional_node("LinkIncremental","false",configuration_name)
        project_attributes_group_node << link_incremental_node
        VS2010_Export_Trace_Log(2,"link_incremental_node #{link_incremental_node}")

        image_xex_output_node = conditional_node("ImageXexOutput", "$(RS_BUILDBRANCH)/game_xenon_#{target_sym}.xex", configuration_name)
				project_attributes_group_node << image_xex_output_node
        VS2010_Export_Trace_Log(2,"image_xex_output_node #{image_xex_output_node}")

        remote_root_node = conditional_node("RemoteRoot","xe:\\$(ProjectName)",configuration_name)
				project_attributes_group_node << remote_root_node
        VS2010_Export_Trace_Log(2,"remote_root_node #{remote_root_node}")
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def add_ps3_project_attributes_group_nodes(project_attributes_group_node, configuration_name)

				target_node = conditional_node("TargetName","$(ProjectName)",configuration_name)
				project_attributes_group_node << target_node
				VS2010_Export_Trace_Log(2,"target_node #{target_node}")
      end

		end
		# END NODE CREATION
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------


		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		# XML MANIPULATION - should belong in helper?
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		class VS2010ProjectGenerator
      #-----------------------------------------------------------------------------------------------------------------------------
      # DW : Err you don't need this really XML::Node.new(name, value) would suffice
      def create_element(node_name, node_value)
        xml_node = XML::Node.new(node_name)
        xml_node << node_value
        xml_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def clean_xml_copy(xml_source)
        xml_dest = XML::Node.new(xml_source.name) #xml_source.copy(true)

        xml_source.each_element do |element|
          #VS2010Generator.log.info("element = #{element}")
          xml_dest << element.copy(true)
        end

        xml_dest
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def append_or_create_node(node,setting,append)
        find_node = node.find_first(setting)
        if find_node
          find_node.content = find_node.content + append
        else
          node << create_element(setting,append)
        end
      end
		end #end class


		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		# Translations
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		class VS2010ProjectGenerator
			#-----------------------------------------------------------------------------------------------------------------------------
      # DW - takes a node ~( which may already be partly populated) and potentially adds it to a parent node
      # whilst doing so it translates attributes of the temporary VS2008 project which is held in the internal memory representation
      # - eventually this might go
      # it then translates attribute values and element names so that it it VS2010 compliant.
      def translate_and_insert_configuration_node(new_node, configuration_group_node, target_config, section, section_internal_name, attribute_names, optional_element_rename = {} )

        new_node = XML::Node.new(section) if new_node.nil?

        attribute_names.each do |attribute_name|
	  
          xpath = "vc/#{section_internal_name}/@#{attribute_name}"
          node = target_config.xml_data.find_first(xpath)

          if node
                      
            # DW : reclutantly a specific fix for line feeds that go missing when we use lib xml, AFAIK VS2010 requires concatenated commands to be line seperated by linefeeds not carriage returns.
            # this this also fails later the only solution is to use the DOS command concatenation &&
	    if node.value.include?"compressmap"
            	node.value = node.value.gsub("\n"," &amp;&amp; ")     # "&#13;&amp;#10;")
            end
          
            # translate values... sigh
            value = translate_values(section,attribute_name,node.value)
	    	    
            element_to_concat = nil
            new_node.each { |element| element_to_concat = element if (element.name == attribute_name) }

            if (element_to_concat and value)
              element_to_concat.content = "#{value};#{element_to_concat.content}".chomp(";")
            else
              new_node << XML::Node.new(attribute_name, value)
            end
          end
        end

        rename_element_names(new_node,optional_element_rename)

        configuration_group_node << new_node if configuration_group_node
      end

			#-----------------------------------------------------------------------------------------------------------------------------------
      # DW - transform 2008 element names into 2010
      def rename_element_names(new_node, optional_element_rename = {} )
        # 2008 name followed by 2010 name
        rename = {	"IgnoreDefaultLibraryNames" 	=> "IgnoreSpecificDefaultLibraries",
			"WarnAsError" 			=> "TreatWarningAsError",
			"UsePrecompiledHeader" 		=> "PrecompiledHeader",
			"EnableFunctionLevelLinking" 	=> "FunctionLevelLinking", 				
		 }

        rename.merge!(optional_element_rename)

        new_node.each do |element|
          if rename.has_key?(element.name)
            new_name = rename[element.name]
            VS2010_Export_Trace_Log(4,"rename_element_name #{element.name} => #{new_name}")
            element.name = new_name
          end
        end
      end

      #----------------------------------------------------------------------------------------------------------------------------------
      # DW - given a section, attribute name and value of a VS2008 node, translate this value into a VS2010 compliant enumeration string.
      # THIS IS NOT COMPLETE - when I get the definitive values from a MSDN link it will be posted here... I need to be VERY careful to get this right too.
			#  VS2010 : http://msdn.microsoft.com/en-us/library/ee862477.aspx
			#  VS2008 : ??????????
      #  ____  _____   ___    _________     ______    ___   ____    ____  _______  _____     ________  _________  ________
      # |_   \|_   _|.'   `. |  _   _  |  .' ___  | .'   `.|_   \  /   _||_   __ \|_   _|   |_   __  ||  _   _  ||_   __  |
      #   |   \ | | /  .-.  \|_/ | | \_| / .'   \_|/  .-.  \ |   \/   |    | |__) | | |       | |_ \_||_/ | | \_|  | |_ \_|
      #   | |\ \| | | |   | |    | |     | |       | |   | | | |\  /| |    |  ___/  | |   _   |  _| _     | |      |  _| _
      #  _| |_\   |_\  `-'  /   _| |_    \ `.___.'\\  `-'  /_| |_\/_| |_  _| |_    _| |__/ | _| |__/ |   _| |_    _| |__/ |
      # |_____|\____|`.___.'   |_____|    `.____ .' `.___.'|_____||_____||_____|  |________||________|  |_____|  |________|

      def translate_values( section, attribute_name, value )

        translations  = { "Link" => {
            "LinkIncremental" =>      { "0" => "Default", "1" => "false", "2" => "true" }, # complete
            "EnableCOMDATFolding" =>  { "0" => "Default", "1" => "false", "2" => "true" }, # complete
            "OptimizeReferences" =>   { "0" => "Default", "1" => "false", "2" => "true" }, # complete
          },

          "ClCompile" => {
            "Optimization" =>            { "0" => "Disabled", "1" => "MinSpace", "2" => "MaxSpeed", "3" => "Full" }, # complete
            "InlineFunctionExpansion" => { "0" => "Disabled", "1" => "OnlyExplicitInline", "2" => "AnySuitable" }, # complete
            "ExceptionHandling" =>       { "0" => "false", "1" => "Async", "2" => "Sync", "3" => "SyncCThrow" }, # complete
            "RuntimeLibrary" =>          { "0" => "MultiThreaded", "1" => "MultiThreadedDebug", "2" => "MultiThreadedDLL", "3" => "MultiThreadedDebugDLL" }, # complete
            "UsePrecompiledHeader" =>    { "0" => "NotUsing", "1" => "Create", "2" => "Use"}, # complete
            "WarningLevel" =>            { "0" => "Level0", "1" => "Level1", "2" => "Level2", "3" => "Level3", "4" => "Level4", "5" => "EnableAllWarnings"}, # complete
            "DebugInformationFormat" =>  { "0" => "OldStyle", "1" => "EditAndContinue", "3" => "ProgramDatabase", }, #????????????? NOT VERIFIED ??????????????
            "CompileAs" =>               { "0" => "Default", "1" => "CompileAsC", "2" => "CompileAsCpp" }, # complete
          },
          "Deploy" => {
						"DeploymentType" =>          { "0" => "CopyToHardDrive"},  # Not documented
          },
        }
        if translations.has_key?(section) and translations[section].has_key?(attribute_name)
          if translations[section][attribute_name].has_key?(value)
            new_value = translations[section][attribute_name][value]
            VS2010_Export_Trace_Log(4,"translate_value #{value} => #{new_value}")
            return  new_value
          else
            VS2010_Export_Trace_Log(4,"***SERIOUS ERROR*** - THE TRANSLATIONS TABLE IS DEFICIENT OF THE TRANSLATION OF VALUE #{section},#{attribute_name},#{value}")
          end
        end
        value
      end

		end #end class


		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		# create_specific_configuration_options_platform_target
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		class VS2010ProjectGenerator
      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_specific_configuration_options_platform_target(platform_config, project, platform_sym, platform, target_sym, target )

        VS2010_Export_Trace_Log(3,"create_specific_configuration_options #{platform_sym}")
        target_config = Info::Config.merge(platform_config,target)

        additional_library_deps = VSUtility.get_library_dependencies(platform_config, target_sym)
        additional_library_deps << "%(AdditionalDependencies)"

        configuration_group_node = create_configuration_group_node(platform_sym,target_sym)
        VS2010_Export_Trace_Log(3,"configuration_group_node : #{configuration_group_node}")

        VS2010_Export_Trace_Log(3,"create_prebuild_step")
        prebuild_step = create_prebuild_step(target_config)
        configuration_group_node << prebuild_step if prebuild_step

        VS2010_Export_Trace_Log(3,"create_prelink_step")
        prelink_step = create_prelink_step(target_config)
        configuration_group_node << prelink_step if prelink_step

        is_library = (platform_config.root_node.attributes["type"] != nil) and (platform_config.root_node.attributes["type"].intern == :library)
        #Library Configuration
        if is_library
          VS2010_Export_Trace_Log(3,"create_library_node")
          library_node = create_library_node(target_config, additional_library_deps, platform_sym)
          configuration_group_node << library_node if library_node
        else
          VS2010_Export_Trace_Log(3,"create_linker_node")
          linker_node = create_linker_node(target_config, additional_library_deps)
          configuration_group_node << linker_node if linker_node

          VS2010_Export_Trace_Log(3,"process VCPostBuildEventTool")
          # <Tool Name="VCPostBuildEventTool" Description="Building symbol file..." CommandLine="&quot;$(SCE_PSP2_SDK_DIR)\host_tools\build\bin\psp2bin&quot; -dsy -c &quot;$(TargetPath)&quot; &gt; &quot;$(TargetDir)$(TargetName).sym&quot;&#13;&#10;$(RS_TOOLSROOT)\bin\coding\compressmap &quot;$(TargetDir)$(TargetName).sym&quot;"/>
         # puts "******************TARGET CONFIG**********************#{target_config}*************TARGET CONFIG************************" if target_config.to_s.downcase.include?"compress"
          translate_and_insert_configuration_node(nil, configuration_group_node, target_config, "PostBuildEvent", "VCPostBuildEventTool", ["Description", "CommandLine"], { "Description" => "Message", "CommandLine" => "Command", } )
	#puts "******************configuration_group_node**********************#{configuration_group_node}*************configuration_group_node************************" if target_config.to_s.downcase.include?"compress"

          VS2010_Export_Trace_Log(3,"process VCPreLinkEventTool")
          #<Tool Name="VCPreLinkEventTool" Description="Resetting symbol file..." CommandLine="echo Placeholder &gt; &quot;$(TargetDir)$(TargetName).cmp&quot;"/>
          translate_and_insert_configuration_node(nil, configuration_group_node, target_config, "PreLinkEvent", "VCPreLinkEventTool", ["Description", "CommandLine" ], { "Description" => "Message", "CommandLine" => "Command", } )

          VS2010_Export_Trace_Log(3,"process VCX360ImageTool")
          #<Tool Name="VCX360ImageTool" OutputFileName="$(RS_BUILDBRANCH)/game_xenon_profile.xex" AdditionalSections="54540840=..\..\..\..\xlast\Fuzzy.spa" TitleID="0x54540840" ProjectDefaults=""/>
          translate_and_insert_configuration_node(nil, configuration_group_node, target_config, "ImageXex", "VCX360ImageTool", [ "OutputFileName", "AdditionalSections", "TitleID", "ProjectDefaults" ] )

          VS2010_Export_Trace_Log(3,"process VCX360DeploymentTool")
          #<Tool Name="VCX360DeploymentTool" ExecutionBucket="85313600" RemoteRoot="xe:\$(ProjectName)" DeploymentFiles="$(RemoteRoot)=$(ImagePath);$(RemoteRoot)=$(TEMP)\rfs.dat;$(RemoteRoot)=$(TargetDir)$(TargetName).cmp;" DeploymentType="0"/>
          translate_and_insert_configuration_node(nil, configuration_group_node, target_config, "Deploy", "VCX360DeploymentTool", [ "ExecutionBucket", "RemoteRoot", "DeploymentFiles", "DeploymentType" ] )

          # DW : even though this is a library setting - don;t use it for libs since they are generated via the rageprojbuilder format which is in turn driven by configurations.xml which sets this out in the new format of node PreBuildEvent - I didn't write this.
          VS2010_Export_Trace_Log(3,"process VCPreBuildEventTool")
          translate_and_insert_configuration_node(nil, configuration_group_node, target_config, "PreBuildEvent", "VCPreBuildEventTool", [ "CommandLine"], { "CommandLine" => "Command", } )

          # DW : once that is done - check what is required to be supported for PS3 - I think no more is required though
        end


        VS2010_Export_Trace_Log(3, "create_compile_node")
        compile_node = create_compile_node(platform_config,target_config,platform_sym)
        configuration_group_node << compile_node if compile_node

        configuration_group_node
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_prebuild_step(target_config)
        #Pre-Build Event:
        #     <PreBuildEvent>
        #      <Command>Test</Command>
        #    </PreBuildEvent>
        #    <PreBuildEvent>
        #      <Message>tests</Message>
        #    </PreBuildEvent>
        config_prebuild = target_config.xml_data.find_first("prebuild")

        if config_prebuild then

          cmdlines = Array.new()
          prebuild_step = XML::Node.new("PreBuildEvent")
          config_prebuild.find("buildstep").each { |buildstep|

            cmdlines << buildstep.attributes["cmd"]
          }

          command = XML::Node.new("Command")
          command << cmdlines.join(CRLF)
          prebuild_step << command
          VS2010_Export_Trace_Log(4, "prebuild_step #{prebuild_step}")
          return prebuild_step
        end
        nil
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_prelink_step(target_config)
        #Pre-Link Event:
        #    <PreLinkEvent>
        #      <Command>Test Command</Command>
        #    </PreLinkEvent>
        #    <PreLinkEvent>
        #      <Message>Test Description</Message>
        #    </PreLinkEvent>
        config_prelink = target_config.xml_data.find_first("prelink")

        if config_prelink then

          prelink_event = XML::Node.new("PreLinkEvent")
          cmdlines = Array.new()

          config_prelink.find("buildstep").each { |buildstep|

						cmd = buildstep.attributes["cmd"]
						if cmd.include?("stripobjects.exe")
							# DW - A hack - since I've been working on this damn project builder for over a month now I've no choice but to try to get it going and retrospectively come back and undo hacks!
							# DW - I should add I never wrote this in the first place..
							cmd = "$(RS_TOOLSROOT)/bin/coding/python/stripobjects.exe %(FullPath) $(OutDir)"
						end
            cmdlines << cmd
          }

          command = XML::Node.new("Command")
          command << cmdlines.join(CRLF)
          prelink_event << command
         # configuration_group_node << prelink_event

          if (config_prelink.attributes["description"] != nil)

            message = XML::Node.new("Message")
            message << config_prelink.attributes["description"]
            prelink_event << message
            VS2010_Export_Trace_Log(5, "prelink_event #{prelink_event}")
            return prelink_event
          end
        end
        nil
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
      def create_configuration_group_node(platform_sym,target_sym)
        configuration_name = VSUtility.create_config_name(platform_sym,target_sym,true)
        configuration_group = ItemDefinitionGroup.new(conditional_config(configuration_name))
        configuration_group_node = configuration_group.create_node()
        VS2010_Export_Trace_Log(5, "configuration_group_node #{configuration_group_node}")
        configuration_group_node
      end

		end #end class

		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		# Export Filters
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		#-----------------------------------------------------------------------------------------------------------------------------
		class VS2010ProjectGenerator
			#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
      #
      def export_project_filters( project )

				filters_node = create_filters_node()

				doc = create_filters_doc(filters_node)
				document_root = doc.root

        filters_item_group = ItemGroup.new()
        filters_item_group_node = filters_item_group.create_node()

        @guid_filename = OS::Path::combine(@current_project.path, @current_project.name) + ".xguid"

        #Reset the item groups for the files based on their task.
        @item_groups = ActiveSupport::OrderedHash.new()
        @item_groups[INCLUDE_TASK] = ItemGroup.new().create_node()
        @item_groups[COMPILE_TASK] = ItemGroup.new().create_node()
        @item_groups[CUSTOM_BUILD_TASK] = ItemGroup.new().create_node()
        @item_groups[NONE_TASK] = ItemGroup.new().create_node()

        #Recursively export the filters.
        export_filters_file( project, project.path, nil, filters_item_group_node )

        #Append the list of filters to the XML Document.
        document_root << filters_item_group_node

        #Append the list of files, containing their filtered information.
        @item_groups.values.each do |item_group|
          document_root << item_group if item_group.children.size > 0
        end

        temporary_filename = OS::Path.combine(Globals::instance().toolstemp,"temp.vcxproj")
        doc.save(temporary_filename)

        return VsXml::convert_to_vsxml(temporary_filename, @filters_filename, true)
      end

			#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
      #
			def create_filters_doc(filters_node)
        doc = XML::Document.new()
        doc.encoding = XML::Encoding::UTF_8   #Used to create the proper XML header.
        doc.root = filters_node
				doc
			end

			#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
      #
      def export_filters_file( filter, project_path, filter_root, filter_item_node )

        #Create the Filters.
        filter_config = Info::Config.merge(@current_project.config,filter.config)

        current_filter_root = nil
        if filter_root.nil? or filter_root.empty?
          current_filter_root = filter.name if filter.name != @current_project.name
        else
          current_filter_root = OS::Path.combine(filter_root, filter.name)
          current_filter_root.gsub!("/", "\\")
        end

        #Enumerate all of the filters in this particular node.
        filters_array = Array.new()
        filter.filters.each { |child_filter|

          child_filter_path = nil
          if current_filter_root.nil?
            child_filter_path = child_filter.name
          else
            child_filter_path = OS::Path.combine(current_filter_root, child_filter.name)
            child_filter_path.gsub!("/", "\\")
          end

          guid = VSUtility.get_guid(@guid_filename, child_filter_path)  #Specify a full path to get the proper filter UUID.

          new_filter = VisualStudioFilter.new(child_filter_path, guid)
          new_filter_node = new_filter.create_node()
          filter_item_node << new_filter_node
        }

        @file_task_map = Hash.new()
        @file_task_map[INCLUDE_TASK] = INCLUDE_EXTENSIONS
        @file_task_map[COMPILE_TASK] = COMPILE_EXTENSIONS

        #Enumerate the files in this particular filter and add XML based on what they're filtered by.
        filter.files.each { |file|

          file_config = Info::Config.merge(filter_config,file.config)

          if file.has_custom_build_steps == true

            current_task = CUSTOM_BUILD_TASK
          else

            extension = File.extname(file.path)
            current_task = NONE_TASK
            @file_task_map.each_pair { |key, values|

              if values.include?(extension) == true
                current_task = key
                break
              end
            }
          end

					@item_groups[current_task] << create_filters_xml_file(file,current_task,current_filter_root)
        }

        filter.filters.each { |child_filter|

          new_path = OS::Path.combine(@current_project.path, child_filter.path)

          export_filters_file(child_filter, new_path, current_filter_root, filter_item_node)
        }
      end

      #-----------------------------------------------------------------------------------------------------------------------------
      #
			def create_filters_xml_file(file,current_task,current_filter_root)

          xml_file = XML::Node.new(current_task)

          out_path = OS::Path.combine(@current_project.path,file.path)
          out_path = File.expand_path(out_path)
          out_path = OS::Path.make_relative(out_path,@save_folder)
          out_path = OS::Path.dos_format(out_path)

          xml_file.attributes["Include"] = out_path

          #Add a filter here.
          filter_node = XML::Node.new("Filter")
          filter_node << current_filter_root   #Does this need to be a full path to the filter?

          xml_file << filter_node
					xml_file
			end

		end #end class


end #module ProjBuild
end #module Pipeline