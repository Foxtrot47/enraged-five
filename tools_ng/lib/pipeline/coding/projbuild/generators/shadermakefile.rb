require 'xml'
require 'pipeline/coding/projbuild/project'
require 'pipeline/coding/xml2vsxml'
require 'pipeline/coding/projbuild/generators/vsshared'
require 'pipeline/coding/projbuild/scriptutility'

module Pipeline

module ProjBuild

class ShaderMakefileExporter < VSProjShared

	attr_accessor :unity_build_filenames	# if true filenames that are created have "_unity" in them.

	@@log = nil 

	def ShaderMakefileExporter.log

		@@log = Log.new( 'shadermakefileexporter' ) if @@log == nil
		@@log
	end
		
	def initialize()
	
		config = Pipeline::Config.instance
	
		@p4 = SCM::Perforce.new()
		@p4.port = config.sc_server
		@p4.client = config.sc_workspace
		@p4.user = config.sc_username
		@p4.connect()
	end


	def import( path, options )	

		output_format = options[:out_format]
		old_working_directory = Dir.getwd()
		working_directory = File.dirname(path)		
		
		preload_list_name = "preload.list"
		preload_list_file_path = OS::Path::combine(working_directory, preload_list_name)
		
		if File.exist?(preload_list_file_path) == false
			print "Unable to find preload.list file: #{preload_list_file_path}\n"
			return false
		end
		
		if ENV["RAGE_ASSET_ROOT"] == nil
			ENV["RAGE_ASSET_ROOT"] = 'T:\\rage\\assets'  
		end

		shader_path = ScriptUtility.acquire_variable(path, "SHADERPATH")

		archive_name = ScriptUtility.acquire_variable(path, "ARCHIVE")
		if archive_name.nil?
			print "Unable to acquire \"ARCHIVE\" attribute!\"\n"
			return false
		end

		archive = OS::Path::combine(working_directory, archive_name)
	
		#Create the preload list.
		
		#Re-write the project file with the replacement changes.
		temp_makefile_txt = OS::Path::combine(working_directory, "makefile_temp.txt")
		archive_makefile = "#{archive}.mk"
		file = File.new(temp_makefile_txt, 'w')
			
		#First, write any .fxh to be included in this project.
		file.write("Includes {\n")
		fxh_files = []
		fxh_files = OS::FindEx::find_files(  OS::Path::combine(working_directory, "*.fxh") )

		fxh_files.each do |fxh_file|
			fxh_file_basename = File.basename(fxh_file)
			file.write( fxh_file_basename + "\n")
		end
		
		file.write("}\n")
		
		#Next, write the contents of preload.list into this file.
		#First line to add is the name of the preloaded list.
		file.write(preload_list_name + "\n")
		
		preload_list_file = File.new(preload_list_file_path)
		lines = preload_list_file.readlines
		lines.each do |line|
			file.write(line)
		end
		preload_list_file.close
		
		file.close
		
		edit_in_p4(@p4, archive_makefile)
		
		shader_makefile_exe = Globals::instance().toolsbin + "\\coding\\make_shader_makefile.exe"
		result = system("#{shader_makefile_exe} #{preload_list_file_path} #{archive_makefile} #{shader_path}")
		if result == false
			print "Unable to execute make shader file program in #{shader_makefile_exe}.\n"
			return false
		end

		Dir.chdir(working_directory)
		
		result = system(Globals::instance().toolsbin + "\\coding\\makemakeproj.exe #{archive_name} #{temp_makefile_txt} #{output_format.to_s}")
		
		# After the project has been created, check it out in source control.
		if @p4.nil? == false
		
			project_filename = OS::Path.combine(working_directory, archive_name)
			project_filename += VSProjShared.get_project_suffix(output_format)
			
			edit_in_p4(@p4, project_filename)
		end
		
		Dir.chdir(old_working_directory)
		if result == false
			print "Failed executing makemakeproj program. Exit code: #{$?}\n"
			return false
		end
		
		if File.exist?(temp_makefile_txt) == true
			File.delete(temp_makefile_txt)
		end
		
		if result == false
			print "Failed executing make shader file program.\n"
			return false
		end	
		
		return true, nil
	end

end

class ShaderMakefileGenerator
	
	@@unity_build_filenames	= nil # if true filenames that are created have "_unity" in them.
	def set_unity_build_filenames( set )
		@@unity_build_filenames = set
	end

	def get_unity_build_filenames( )
		@@unity_build_filenames
	end

	def export( path, project, options = nil )
		
		true
	end
	
	def import( path, options = nil )
		
		return false if options == nil

		case options[:type]
		
		when :solution: 
			
			loader = VS2005SlnLoader.new()
			return loader.import(path)
		
		when :project:

			loader = ShaderMakefileExporter.new()

			return loader.import(path, options)
		end
		
		true
	end
end

end #module ProjBuild

end #module Pipeline