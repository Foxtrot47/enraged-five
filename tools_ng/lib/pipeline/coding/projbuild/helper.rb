#
# File:: helper.rb
#			replacement for the old rage batch file projbuilderhelper.bat that rebuilt all
#			rage projects		
#
# Pipeline::	
#
# Author:: Greg Smith <greg@rockstarnorth.com>
# Date:: 12 February 2010
#
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/os/path'
require 'pipeline/os/file'
require 'pipeline/coding/projbuild'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

module Pipeline

module ProjBuild

class ProjBuilderHelper

	attr_writer :in_format, :out_format, :in_options, :out_options, :out_type, :unity_build_enabled

	def initialize()
	
		@config = Pipeline::Config.instance
		@config.project.load_config
		
		@log = Pipeline::LogSystem.instance().rootlog
		@root_path = ''
		@in_format = :rageprojbuilder		
		@out_format = nil
		@in_options = Hash.new()
		@out_options = Hash.new()
		@out_type = :project
		@unity_build_enabled = nil
	end
	
	def set_root( root_path )
	
		@root_path = root_path.downcase
	end
	
	def build( path, projectname = nil )
		
		path = OS::Path.combine(@root_path,path) if path.index(@root_path) == nil
		
		return nil if OS::FileEx.directory?(path) == false

		@log.info("Building #{path}")
		
		FileUtils.cd(path) {
		
			projectname = path.split("/").last if projectname == nil
		
			input = nil
		
			search_path = OS::Path.combine(path,"#{projectname}.txt")
			files = OS::FindEx.find_files(search_path)
			
			if files.size == 0 then

				search_path = OS::Path.combine(path,"makefile.txt")
				files = OS::FindEx.find_files(search_path)			
			end			
			
			if files.size == 0 then

				search_path = OS::Path.combine(path,"*.txt")
				files = OS::FindEx.find_files(search_path)			
			end
			
			input = files.first			
						
			
			# command line crazyness - I'd rather not have to support this TBH it makes it more complex than it needs to be IMO. - DW
			# the idea is that if you specify a full commandline override it will not obey the project.xml
			if not (@out_format.nil? or @unity_build_enabled.nil?)
				
				@log.info("Calling projbuilder in #{path} on #{input} for compiler #{@out_format} unity=#{@unity_build_enabled}")
												
				projbuild = Pipeline::ProjBuild::ProjBuilder.new( @out_format.downcase=="vs2010" )				
				
				#print "Unity Build Enabled (cmdline): #{@unity_build_enabled}\n"
				@in_options[:out_format] = @out_format
				@in_options[:unity_build_enabled] = @unity_build_enabled
				@out_options[:in_format] = @in_format
				@out_options[:unity_build_enabled] = @unity_build_enabled
				
				ret = projbuild.import(input,@in_format,@in_options)			
				projbuild.export(projectname,@out_format,@out_options)
			else							

				if ( @config.project.codeconfigs.count == 0 )
					@log.error("No code configurations have been declared in your local installer setup.  Please make sure the project.xml is updated.")
					return
				end
				
				idx = 1
				
				num_configs = @config.project.codeconfigs.length
				
				@log.info("\n=======================================================================================================\n#{projectname}\n=======================================================================================================")
				
				@config.project.codeconfigs.each do |codeconfig|

					@out_format = codeconfig.compiler	
					sep = "-------------------------------------------------------------------------------------------------------"
					@log.info("\n\n\n#{sep}\nBUILD #{idx}/#{num_configs} : #{projectname} \t\t\t\t\t\t\t#{Time.now}\n\t\t\tVS Version:#{@out_format}\n\t\t\tUnity:#{codeconfig.unity_build.enabled}\n\t\t\tWorking dir:#{path}\n\t\t\tMakefile:#{input}\n\t\t\t")

					projbuild = Pipeline::ProjBuild::ProjBuilder.new( @out_format.to_s.downcase=="vs2010" )				
					
					@in_options[:out_format] = @out_format
					@in_options[:unity_build_enabled] = codeconfig.unity_build.enabled
					@out_options[:in_format] = @in_format
					@out_options[:unity_build_enabled] = codeconfig.unity_build.enabled

					ret = projbuild.import(input,@in_format,@in_options)			
					projbuild.export(projectname,@out_format,@out_options)
					
					idx = idx + 1
				end
			end
		}	
		
		
		@log.info("Building #{path} complete\n\n\n\n\n\n")
	end
	
	def build_in( path )
	
		full_path = OS::Path.combine(@root_path,path)
	
		Dir.open(full_path).each { |item|
		
			next if item == "." or item == ".."
		
			new_path = OS::Path.combine(full_path,item)
		
			if OS::FileEx.directory?(new_path) then
			
				build(new_path)
			end
		}
	end
	
protected
	
end

end #module ProjBuild

end #module Pipeline
