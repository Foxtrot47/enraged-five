#
# File:: convert.rb
#			Command line wrapper for the projbuild system 
#
# Pipeline::
#
# Author:: Greg Smith <greg@rockstarnorth.com>
# Date:: 5 February 2010
#
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/coding/projbuild.rb'
require 'pipeline/os/getopt'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline

module ProjBuild

class ProjConvert

	def initialize()
		config = Pipeline::Config.instance
	
		@p4 = SCM::Perforce.new()
		@p4.port = config.sc_server
		@p4.client = config.sc_workspace
		@p4.user = config.sc_username
		@p4.connect()
		
		#TODO: Expose this globally to the CLI.
		@submit_p4_files = false
	end
	
	def copy_source_control_files(project_path, p4)
		projbuild_root = Globals::instance().toolsconfig + "/projbuild"
		src_vspscc_file = OS::Path::combine(projbuild_root, "template.vspscc")
		src_vssscc_file = OS::Path::combine(projbuild_root, "template.vssscc")
		
		vspscc_file = project_path + ".vspscc"
		if File.exists?(vspscc_file) == false and File.exists?(src_vspscc_file) == true
			OS::FileUtilsEx::copy_file(src_vspscc_file, vspscc_file)
		end
		
		vssscc_file =  project_path + ".vssscc"
		if File.exists?(vssscc_file) == false and File.exists?(src_vssscc_file) == true
			OS::FileUtilsEx::copy_file(src_vssscc_file, vssscc_file)
		end
		
		#Add to source control.
		if p4 != nil
			edit_in_p4(p4, vspscc_file)
			edit_in_p4(p4, vssscc_file)
		end
	end
	
	def execute( input, in_generator_type, out_generator_type, options)
		
		projbuild = Pipeline::ProjBuild::ProjBuilder.new( out_generator_type.to_s.downcase=="vs2010" )		
		@log = Pipeline::LogSystem.instance().rootlog
		
		if File.exist?(input) == true

			print "Processing #{input}...\n";
			
			result = projbuild.import(input, in_generator_type, options)
			if result == true
				
				print "Exporting #{input}...\n"
				result = projbuild.export(input, out_generator_type, options)
			end
			
			if @submit_p4_files == true
				#Copy any source control files related to projects.
				copy_source_control_files(output_path, @p4)
			end
			
			#If there is a post_makefile.bat in this directory, execute it.
			
			working_directory = File.dirname(input)
			post_makefile_path = OS::Path::combine(working_directory, "post_makefile.bat")
			if File.exist?(post_makefile_path) == true
			
				original_working_directory = Dir.getwd()
				Dir.chdir( working_directory ) 
			
				print "Calling post makefile process...\n"
				system("call " + post_makefile_path)
				
				Dir.chdir(original_working_directory)
			end
			
			revert_unchanged(input)			
			
			return result
		end
		
		return false		
	end
	
	# revert unchanged files
	def revert_unchanged(input)	
		# Revert if unchanged
		fd = File.expand_path(input)
		scm = Pipeline::Config::instance().scm
		ragescm = Pipeline::Config::instance().ragescm
	
		reverted = nil
		FileUtils.cd(Pipeline::OS::Path.get_directory(fd)) do
			if ( scm.exists?( fd ) ) then
				reverted = scm.run_revert("-a", fd)
			elsif ( ragescm.exists?( fd ) ) then
				reverted = ragescm.run_revert("-a", fd)
			end
		end

		@log.info("Reverted #{input} - it has not changed.") if reverted and reverted.length > 0									
	end	

end

end #module ProjBuild

end #module Pipeline