#
# File:: tools_get_latest.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 7 August 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/path'
require 'pipeline/scm/perforce'

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
module Pipeline
module ProjectUtil

	#
	# Fetches the latest global tools data and enabled projects tools data.
	# Automatically re-running the install process.
	#
	# This should be run by daemon-like tools that are resident for long 
	# periods of time, maybe nightly or at the start of each 'update' or 
	# 'tick'.
	#
	# == Example Usage
	#	 files_synced = Pipeline::ProjectUtil::tools_get_latest( ) do 
	#       |client_file, synced, errors|
	#	    puts "Sync: #{client_file}"
	#    end
	#
	def ProjectUtil.tools_get_latest( force = false, &block )
		
		c = Pipeline::Config::instance()
		p4 = c.scm()
		p4.connect() unless p4.connected?
		
		args = []
		args << '-f' if force
		
		# Build up series of paths to pass to Perforce sync command
		args << "#{c.toolsroot}/..."
		
		files_synced = p4.run_sync_with_block( args ) do
			|client_file, synced, errors|
			
			yield( client_file, synced, errors ) if ( block_given? )
		end

		files_synced
	end

	def ProjectUtil.tools_config_get_latest( force = false, &block )
		
		c = Pipeline::Config::instance()
		p4 = c.scm()
		p4.connect() unless p4.connected?
		
		args = []
		args << '-f' if force
		
		# Build up series of paths to pass to Perforce sync command
		args << "#{c.toolsconfig}/..."
		
		files_synced = p4.run_sync_with_block( args ) do
			|client_file, synced, errors|
			
			yield( client_file, synced, errors ) if ( block_given? )
		end

		files_synced
	end

	def ProjectUtil.tools_lib_get_latest( force = false, &block )
		
		c = Pipeline::Config::instance()
		p4 = c.scm()
		p4.connect() unless p4.connected?
		
		args = []
		args << '-f' if force
		
		# Build up series of paths to pass to Perforce sync command
		args << "#{c.toolslib}/..."
		
		files_synced = p4.run_sync_with_block( args ) do
			|client_file, synced, errors|
			
			yield( client_file, synced, errors ) if ( block_given? )
		end

		files_synced
	end

	def ProjectUtil.tools_lib_util_ragebuilder_get_latest( force = false, &block )
		
		c = Pipeline::Config::instance()
		p4 = c.scm()
		p4.connect() unless p4.connected?
		
		args = []
		args << '-f' if force
		
		# Build up series of paths to pass to Perforce sync command
		args << "#{c.toolslib}/util/ragebuilder/..."
		
		files_synced = p4.run_sync_with_block( args ) do
			|client_file, synced, errors|
			
			yield( client_file, synced, errors ) if ( block_given? )
		end

		files_synced
	end

	def ProjectUtil.tools_bin_get_latest( force = false, &block )
		
		c = Pipeline::Config::instance()
		p4 = c.scm()
		p4.connect() unless p4.connected?
		
		args = []
		args << '-f' if force
		
		# Build up series of paths to pass to Perforce sync command
		args << "#{c.toolsbin}/..."

		puts args
		
		files_synced = p4.run_sync_with_block( args ) do
			|client_file, synced, errors|
			
			yield( client_file, synced, errors ) if ( block_given? )
		end

		files_synced
	end
	
end # ProjectUtil
end # Pipeline

# tools_get_latest.rb
