#
# File:: data_texture.rb
# Description:: Texture processing utility functions for the pipeline
#
# Author:: Greg Smith <greg@rockstarnorth.com>
# Date:: 21 March 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module ProjectUtil

  def make_sure_tcs_exists( p4, changelist, tcsfilename, templatepath, skipprocessing = nil, recreate = false )

		c = Pipeline::Config.instance( )

    templatepath = OS::Path.normalise(templatepath)
    templatepath.gsub!(c.project.assets, "")

		p4.run_sync( tcsfilename ) if p4 != nil

		if (not File::exists?( tcsfilename ) or recreate) then

      #changelist = p4.create_changelist( changelist ) if p4 != nil

			tcs_dir = OS::Path::get_directory( tcsfilename )
			FileUtils::mkdir_p( tcs_dir ) \
				unless ( File::directory?( tcs_dir ) )

			File.open( tcsfilename, 'w+' ) do |file|

				file.write("<?xml version=\"1.0\" encoding = \"Windows-1252\"?>\n")
				file.write("<rage__CTextureConversionSpecification>\n")
				file.write("<skipProcessing value=\"false\"/>\n") if skipprocessing != nil
				file.write("<parent>${RS_ASSETS}#{templatepath}</parent>\n")
				file.write("</rage__CTextureConversionSpecification>\n")
			end

      p4.run_edit_or_add( tcsfilename ) if p4 != nil
      #p4.run_reopen( '-c', changelist, tcsfilename ) if p4 != nil
		end

		changelist
  end


  def make_sure_tcs_exists_with_asset_base( p4, changelist, tcsfilename, templatepath, skipprocessing = nil, recreate = false )

		c = Pipeline::Config.instance( )

    templatepath = OS::Path.normalise(templatepath)
    templatepath.gsub!(c.project.assets, "")

		p4.run_sync( tcsfilename ) if p4 != nil

		if (not File::exists?( tcsfilename ) or recreate) then

      #changelist = p4.create_changelist( changelist ) if p4 != nil

			tcs_dir = OS::Path::get_directory( tcsfilename )
			FileUtils::mkdir_p( tcs_dir ) \
				unless ( File::directory?( tcs_dir ) )

			File.open( tcsfilename, 'w+' ) do |file|

				file.write("<?xml version=\"1.0\" encoding = \"Windows-1252\"?>\n")
				file.write("<rage__CTextureConversionSpecification>\n")
				file.write("<skipProcessing value=\"false\"/>\n") if skipprocessing != nil
				file.write("<parent>$(assets)#{templatepath}</parent>\n")
				file.write("</rage__CTextureConversionSpecification>\n")
			end

      p4.run_edit_or_add( tcsfilename ) if p4 != nil
      #p4.run_reopen( '-c', changelist, tcsfilename ) if p4 != nil
		end

		changelist
  end

	#
	# == Description
	# Create an uncompressed texture pipeline TCS file for a specific texture.
	# The location for the TCS file is under $(assets)/metadata/textures 
	# relative to the location in $(art).
	#
	def make_sure_uncompressed_tcs_exists( p4, changelist, texture )

		c = Pipeline::Config.instance( )
		assets = c.project.assets
		
		tcs_file = OS::Path.remove_extension( texture )
		tcs_file.gsub!( c.project.art, "" )
		tcs_file = OS::Path::combine( assets, 'metadata/textures', tcs_file) + ".tcs"
	
		p4.run_sync( tcs_file )
	
		if (not File::exists?( tcs_file ) ) then
	
			tcs_dir = OS::Path::get_directory( tcs_file )
			FileUtils::mkdir_p( tcs_dir ) \
				unless ( File::directory?( tcs_dir ) )
		
			File.open( tcs_file, 'w+' ) do |file|

				file.write("<?xml version=\"1.0\" encoding = \"Windows-1252\"?>\n")
				file.write("<rage__CTextureConversionSpecification>\n")
				file.write("\t<skipProcessing value=\"true\"/>\n")
				file.write("</rage__CTextureConversionSpecification>\n")
			end
		end
	
		p4.run_edit_or_add( tcs_file )
		p4.run_reopen( '-c', changelist, tcs_file )
	
		tcs_file
	end

	#
	# == Description
	# Create an compressed texture pipeline TCS file for a specific texture.
	# The location for the TCS file is under $(assets)/metadata/textures 
	# relative to the location in $(art).
	#
	def make_sure_compressed_tcs_exists( p4, changelist, texture )

		c = Pipeline::Config.instance( )
		assets = c.project.assets
		
		tcs_file = OS::Path.remove_extension( texture )
		tcs_file.gsub!( c.project.art, "" )
		tcs_file = OS::Path::combine( assets, 'metadata/textures', tcs_file) + ".tcs"
	
		p4.run_sync( tcs_file )
	
		if (not File::exists?( tcs_file ) ) then
	
			tcs_dir = OS::Path::get_directory( tcs_file )
			FileUtils::mkdir_p( tcs_dir ) \
				unless ( File::directory?( tcs_dir ) )
		
			File.open( tcs_file, 'w+' ) do |file|

				file.write("<?xml version=\"1.0\" encoding = \"Windows-1252\"?>\n")
				file.write("<rage__CTextureConversionSpecification>\n")
				file.write("<skipProcessing value=\"false\"/>\n")
				file.write("<parent>${RS_ASSETS}/metadata/textures/templates/globals/Default.tcp</parent>\n")
				file.write("</rage__CTextureConversionSpecification>\n")
			end
		end
	
		p4.run_edit_or_add( tcs_file )
		p4.run_reopen( '-c', changelist, tcs_file )
	
		tcs_file
	end
	
end # ProjectUtil module
end # Pipeline module
