#
# File:: %RS_TOOLSLIB%/pipeline/projectutil/data_rpf.rb
# Description:: RPF functions.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 24 June 2011 (refactor data_extract.rb)
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/log/log'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_rpf_sort'
require 'pipeline/util/rage'
include Pipeline

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module ProjectUtil

	@@rpf_log = Pipeline::Log::new( 'data_rpf' )
	RPF_BUILD_RBS = "$(toolslib)/util/ragebuilder/build_rpf.rbs"
	
	#
	# == Description
	# Extract the content of an RPF file to a destination directory.  This
	# is no longer supported.
	#
	def ProjectUtil::data_extract_rpf( filename, destination, overwrite = false, filter = '*.*', &block )
		throw RuntimeError::new( "Extracting files from an RPF is no longer supported." )
	end
	
	#
	# == Description
	# Create a RPF file; from the specific file list.
	#
	# file_list is an Array; of either file path Strings or Hash objects 
	# containing two keys, :src, :dst specifying source file and destination
	# path in the zip file.
	#
	# == Example Usage
	# See %RS_TOOLSLIB%/util/data_mk_generic_rpf.rb
	#
	def ProjectUtil::data_rpf_create( filename, file_list, compressed = true, target = nil, &block )
	
		c = Pipeline::Config::instance()
		p = c.projects[ENV['RS_PROJECT']]
		r = RageConvertTool::parse( p )
		if (target == nil) then
			target = p.targets['win32'] if ( p.targets.has_key?( 'win32' ) )
			target = p.targets['win64'] if ( p.targets.has_key?( 'win64' ) )
		end	
			
        # Sort our file list.
        sorted_file_list = ProjectUtil::data_rpf_sort_filelist( file_list )
  
		# Generate our little text input file; for the build_rpf.rbs script.
		filelist_dir = OS::Path::combine( c.temp, 'data_rpf' )
		filelist = OS::Path::combine( filelist_dir, OS::Path::get_basename( filename ) + ".txt" )
		FileUtils::mkdir_p( filelist_dir ) unless ( File::directory?( filelist_dir ) )
		
		File::open( filelist, 'w' ) do |fp|
			sorted_file_list.each do |path|
				if ( path.is_a?( String ) and File::file?( path ) ) then
					fp.puts( "#{path}\t#{OS::Path::get_filename(path)}" )
					yield( path ) if ( block_given? )
				elsif ( path.is_a?( Hash ) and path.has_key?( :src ) and path.has_key?( :dst ) )
					fp.puts( "#{path[:src]}\t#{path[:dst]}" )
					yield( path ) if ( block_given? )
				else
					@@rpf_log.error( "Misformed entry in file_list (#{path.class})." )
				end
			end
		end
		
		platform_name = RageUtils.rage_platform( target.platform )
		
		# Invoke Ragebuilder to package up our RPF file.
		rbcmd = ""
		p.in_env do |env|
			rbcmd += env.subst( "#{r[target.platform].path} " )
			rbcmd += env.subst( "#{c.envsubst( RPF_BUILD_RBS )} " )
            rbcmd += "-nopopups "
			rbcmd += env.subst( "-build #{p.build} " )
			rbcmd += env.subst( "-shader #{p.shaders} " )
			rbcmd += env.subst( "-shaderdb #{OS::Path::combine( p.shaders, 'db' )} " )
			rbcmd += env.subst( "-output #{filename} " )
			rbcmd += env.subst( "-platform #{platform_name} " )
			rbcmd += env.subst( "-filelist #{filelist} " )
		end
		
		@@rpf_log.info( "Building RPF: #{rbcmd}." )
		res = nil
		begin
			res, sout, serr = OS::start( rbcmd )
		rescue Exception => ex
			puts "Exception: #{ex.message}"
			puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"

			@@rpf_log.warn( "OS::start hit an execption, it will now run the command with system()." )
			# Gracefully handle exceptions in systemu itself ( NOT WHAT IT IS RUNNING - don't be confused. )
			system( command_line )
			res = 0 # force it to be happy, system doesn't return a code for the process that run anyway.
		end
		FileUtils::rm( filelist )
		
		( 0 == res )
	end

	#
	# == Description
	# Append files to an existing RPF file.  This is no longer supported.
	#
	def ProjectUtil::data_append_rpf( filename, append_files )
		throw RuntimeError::new( "Appending files to an RPF is no longer supported." )
	end

end # ProjectUtil module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/projectutil/data_rpf.rb
