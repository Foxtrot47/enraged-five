#
# File:: builder_assettest_tracingstats_append.rb
# Description:: 
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 22 January 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'rexml/document'
require 'rexml/formatters/pretty'
include REXML

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
	module ProjectUtil
		#
		# == Description
		# populate the dest xmldoc with the tracing stat elements
		# Ensure the xml document is in the form required.
		def ProjectUtil::tracing_stat_xmldoc_prepare(dest_xmldoc,project_project,project_branch,project_platform)
			#------------Create dest xml doc XML structure------
			root = dest_xmldoc.elements['report']
			root = dest_xmldoc.add_element("report") if not root	
			
			if not root.elements['project'] then
				elem = root.add_element("project")
				elem.add_attribute('project',project_project)
				elem.add_attribute('branch',project_branch)
				elem.add_attribute('platform',project_platform)				
			end		
			
			root.add_element("tracing_stats") if not root.elements['tracing_stats']
		end # tracing_stat_xmldoc_prepare
		
		#
		# == Description
		# populate the dest xmldoc with the tracing stat elements
		def ProjectUtil::append_tracing_stat(dest_xmldoc,ts_elem,timestamp_date,timestamp_time)
			root = dest_xmldoc.elements['report']
			
			#------------Stuff in new data--------------				
			new_elem = root.elements['tracing_stats'].add(ts_elem.dup)
			#------------Make sure its timestamped------				
			new_elem.add_attribute("time",timestamp_time)
			new_elem.add_attribute("date",timestamp_date)			
		end # append_tracing_stat
		
		#
		# == Description
		# Process and individual file for builder_assettest_tracingstats_append
		def ProjectUtil::builder_assettest_tracingstats_append_single_file(src_filename,dest_directory)			
			begin
				return if not File.exists?(src_filename)
				
				FileUtils::mkdir_p( dest_directory ) unless ( File::directory?( dest_directory ) )					
				
				#----------- Open Source xml -------------------------						
				src_file	= File.new( src_filename )
				src_xmldoc	= REXML::Document.new( src_file )
				
				#----------- Read timestamp --------------------------
				timestamp_node	= src_xmldoc.root.elements['timestamp']
				timestamp_date	= timestamp_node.attributes['date']
				timestamp_time	= timestamp_node.attributes['time']
				
				#------------Read project details---------------------
				project_node	= src_xmldoc.root.elements['project']
				project			= project_node.attributes['project']
				branch			= project_node.attributes['branch']	
				platform		= project_node.attributes['platform']	
				
				#------------ Goto Tracing stats node ----------------
				tracingstat_node = src_xmldoc.root.elements['tracing_stats']			
				
				tracingstat_node.elements.each do |tracingstat_elem|
					#------------ Create dest filename ---------------
					filename		= "#{project}_#{branch}_#{platform}_tracing_stats_history_#{tracingstat_elem.name}.xml"
					dest_filename	= OS::Path.combine(dest_directory,filename)
				
					#------------ Read or create dest xml doc --------
					dest_xmldoc = REXML::Document.new
					dest_xmldoc << REXML::XMLDecl.new	
					dest_xmldoc = REXML::Document.new( File.open( dest_filename, 'r' ) ) if ( File::exists?( dest_filename ) )
					
					#------------ Prepare dest xml doc ---------------
					tracing_stat_xmldoc_prepare(dest_xmldoc,project,branch,platform)
					
					#------------ Append to dest xml doc -------------
					append_tracing_stat(dest_xmldoc,tracingstat_elem,timestamp_date,timestamp_time)
					
					#------------ Save dest xml doc ------------------
					File.open( dest_filename, 'w+' ) do |file|				
						fmt = REXML::Formatters::Pretty.new()
						fmt.write( dest_xmldoc, file )    						
					end
				end
			rescue Exception => ex
				puts "builder_assettest_tracingstats_append_single_file has quit unexpectedly:"
				puts "\t#{ex.message}"
				ex.backtrace.each do |m| puts "\t#{m}"; end
			end						
		end # builder_assettest_tracingstats_append_single_file
		
		#
		# == Description
		# Takes a bunch of source xml files ( tracing stat processed files ) 		
		# and appends then to existing ( or creates new ) files in the passed directory.
		# inserting a timestamp as it goes.
		def ProjectUtil::builder_assettest_tracingstats_append( src_files, dest_directory)	
			begin
				src_files.each do |scr_file| 
					throw IOError.new( "File #{scr_file} does not exist.  Aborting convert." ) unless ( File::exists?( scr_file ) )										
					builder_assettest_tracingstats_append_single_file(scr_file, dest_directory)
				end	
			rescue Exception => ex
				puts "builder_assettest_tracingstats_append has quit unexpectedly:"
				puts "\t#{ex.message}"
				ex.backtrace.each do |m| puts "\t#{m}"; end
			end
		end	# builder_assettest_tracingstats_append
	end # ProjectUtil module
end # Pipeline module

# builder_assettest_tracingstats_append.rb