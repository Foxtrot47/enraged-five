#
# File:: %RS_TOOLSLIB%/pipeline/projectutil/data_extract.rb
# Description:: Extract data functions from game archives.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 5 November 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/projectutil/data_rpf'
require 'pipeline/projectutil/data_zip'

# Implementation is in 'data_rpf' and 'data_zip'.  This file is obsolete.

# %RS_TOOLSLIB%/pipeline/projectutil/data_extract.rb
