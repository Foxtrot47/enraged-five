#
# File:: data_search.rb
# Description:: Functions for searching through and getting information
#				from data.
#
# Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
# Date:: 22 July 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/os/file'
require 'pipeline/resourcing/path'


#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
	module ProjectUtil
		
		#-------------------------------------------------------------------------
		# Functions
		#-------------------------------------------------------------------------
		
		#
		# == Description
		# A function which returns a list of all the map files (IDE, IPL, IMG/RPF, etc.)
		# for a particular level.  If the level is given as "generic" it returns all the
		# map files for the game.
		#
		# === Example Usage
		# The code below would return a list of all the map files for Jimmy Alpine.
		#
		# p = Pipeline::Config.instance.projects[ "jimmy" ]
		# file_list = ProjectUtil::data_find_maps_levels( p, "alpine" )
		#
		def ProjectUtil::data_find_maps_levels(project, level)
			#throw ArgumentError.new( "Project #{project.name} does not have levels!" ) unless project.has_levels
			
			if ( level.eql?( "generic" ) ) then
				# If the level is generic then search everywhere, excluding files in the generic directory
				file_list = OS::FindEx::find_files_recurse( OS::Path::combine( project.independent, 'levels', '*' ) )
				file_list.delete_if{ |file| OS::Path::get_trailing_directory( file ) == "generic" }
			else
				# If we have a specific level then just search this level
				file_list = OS::FindEx::find_files_recurse( OS::Path::combine( project.independent, 'levels', level, '*.*' ) )
			end
			
			# Remove non-map files from the list
			file_list.delete_if{ |file| OS::Path::get_trailing_directory( file ).include?( "_prop" ) }
			file_list.delete_if{ |file| OS::Path::get_trailing_directory( file ).include?( "interiors" ) }
			file_list.delete_if{ |file| OS::Path::get_basename( file ).include?( "navmeshes" ) }
			file_list.delete_if{ |file| OS::Path::get_basename( file ).include?( "navnodes" ) }
			file_list.delete_if{ |file| OS::Path::get_basename( file ).include?( "paths" ) }
			file_list.delete_if{ |file| OS::Path::get_basename( file ).include?( "patrol" ) }
			file_list.delete_if{ |file| OS::Path::get_basename( file ).include?( "radar" ) }
			
			# Return the list
			file_list
		end #data_find_maps_levels
		
		
		#
		# == Description
		# A function which takes a hash with items in the game as keys and arrays
		# as values.  It then searches the given IPL for references to the given items 
		# and creates a new hash containing found items with a pair giving the file name
		# and how many times it was found.
		#
		# Returns the new hash.
		#
		# === Example Usage
		# Suppose that the item "tools_test" appears 10 times in alp_lake.ipl.  We
		# should then get the following from this piece of code:
		#
		# ipl = "X:\jimmy\build\dev\independent\levels\alpine\alp_lake.ipl"
		# items = { "tools_test" => [] }
		# items = ProjectUtil::data_search_object_in_IPL(ipl, items)
		#	==> { "tools_test" => ["alp_lake - 10\n"] }
		#
		def ProjectUtil::data_search_object_in_IPL(ipl, items)
			# Set up variables
			logStr = ""
			count = 0
			
			# Create a temporary hash to store info about only this IPL.
			# Values are pairs where the first object is a string about 
			# this item and the second object is a usage count.
			found_items = {}
			
			# Parse the IPL
			ipl_objects = []
			File.open(ipl, 'r') do
				text = IO.readlines( ipl )
				# Take out version info which confuses the parser
				start = text.index( "vers\n" )
				finish = text.index( "end\n" )
				text.each_index { |i| if i >= start and i <= finish then text.delete_at(i) end }
				# Get objects
				start = text.index( "inst\n" ) + 1
				finish = text.index( "end\n" ) - 1
				ipl_objects = text[start..finish]
			end
			
			# Go through the list of objects
			ipl_objects.each do |line|
				# Parse each line in the list for the object name
				objName = line.split(',')[0]
				objName.downcase!
				
				if objName
					# If we have found an object name, check it against the given items
					items.each_key do |key|
						if objName.include?( key )
							# If this is a new map, add its name to the info string
							unless found_items.has_key?( key )
								mapName = OS::Path::get_basename( ipl )
								found_items[key] = [mapName, 1]
							end
							# Update the count
							found_items[key][1] += 1
						end #objName.include?
					end #items.each
				else
					throw ParseError.new( "Object not found in this line of the IPL." )
				end #if objName
			end #ipl_objects.each
			
			# Return the updated hash
			found_items
		end #data_search_object_in_IPL
		
	end # ProjectUtil module
end # Pipeline module

# data_search.rb