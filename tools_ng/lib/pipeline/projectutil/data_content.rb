#
# File:: %RS_TOOLSLIB%/pipeline/projectutil/data_content.rb
# Description:: Data content node methods.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 21 June 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/content/content_core'
require 'pipeline/content/treecore'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module ProjectUtil
	
	#
	# Return an Array of Content node for all the filenames passed into the 
	# function.  Entries will have nil if they have no corresponding entry in 
	# the content tree.
	#
	def ProjectUtil::data_content_for_files( project, filenames )
		throw ArgumentError::new( "Invalid project (#{project.class})." ) \
			unless ( project.is_a?( Project ) )
		throw ArgumentError::new( "Invalid filenames Array or String (#{filenames.class})." ) \
			unless ( filenames.is_a?( String ) or filenames.is_a?( Array ) )
		project.load_config( )
		project.load_content( )
	
		# Ensure internally filenames is always an Array.
		filenames = [ filenames ] if ( filenames.is_a?( String ) )
		content_nodes = []
	
		filenames.each do |filename|
			filename = OS::Path::normalise( filename )
			basename = OS::Path::get_basename( filename )
            
			content_files = project.content.find( ) do |content|
                if ( content.is_a?( Content::Directory ) ) then
                    content_normalised = OS::Path::normalise( content.absolute_path )
                    filename_normalised = OS::Path::normalise( filename )
                    ( filename_normalised.starts_with( content_normalised ) )
                else
				    ( 0 == content.name.casecmp( basename ) )
                end
			end
			content_node = nil
			content_files.each do |content|
    
                if ( content.is_a?( Content::File ) ) then
                    next unless ( 0 == filename.casecmp( content.filename ) )
                    content_node = content
                    break
                elsif ( content.is_a?( Content::Directory ) ) then
                	srcdir = ''
                	if File.file?( filename ) then
                		srcdir = OS::Path.get_directory( filename )
                	else
                		srcdir = filename  
                	end
                    next unless ( srcdir == content.absolute_path )
                    content_node = content
				    break
                end
			end
            
			# May append nil, if not found.
			content_nodes << content_node
		end
		content_nodes
	end
	
	#
	# Return an Array of filenames that represent the output of the content 
	# node (optionally recursive).
	#
	def ProjectUtil::data_content_get_output_files( project, branch, content, recurse = false )
		throw ArgumentError::new( "Invalid project object (#{project.class})." ) \
            unless ( project.is_a?( Project ) )
		throw ArgumentError::new( "Invalid branch object (#{branch.class})." ) \
            unless ( branch.is_a?( Branch ) )
        throw ArgumentError::new( "Invalid content node (#{content.class})." ) \
			unless ( content.is_a?( Content::Base ) )
		
		files = []
        if ( content.outputs.size > 0 ) then
		    content.outputs.each do |output_node|  
			files << output_node.filename if ( output_node.is_a?( Content::File ) )
                files += data_content_get_output_files( project, branch, output_node, true ) if ( recurse )
		    end
        else
            if content.is_a?( Content::Directory )
                   # HACK: Assume resulting RPF has the same name as the input content directory
                  path = OS::Path.combine( content.path, content.name + ".zip")
            else
                  path = content.filename
            end
            files += ProjectUtil::data_content_platform_filenames( project, branch, path )
        end
		
		# Recurse to children.
		if ( content.class <= Content::SupportChildren ) then
			content.children.each do |child|
				# JWR - B* 692193 - prevent scene xml files making it into the platform data
				next if child.is_a?( Pipeline::Content::MapSceneXml )
				
				files += data_content_get_output_files( project, branch, child, recurse )
			end
		end
		files.uniq
	end
	
	#
	# Return an Array of filenames that represent the input of the content
	# node (recursive).
	#
	def ProjectUtil::data_content_get_input_files( content )
		throw ArgumentError::new( "Invalid content node (#{content.class})." ) \
			unless ( content.is_a?( Content::Base ) )
		
		files = []		
		content.inputs.each do |input_node|
			files << input_node.filename if ( input_node.is_a?( Content::File ) )
		end
		
		# Recurse to children.
		if ( content.class <= Content::SupportChildren ) then
			content.children.each do |child|
				files += data_content_get_input_files( child )
			end
		end
		files.uniq
	end
	

	#
    # ********
    # DHM WARNING:: Duplicate of data_convert function so save a circular
    #               dependency
    # *******
    #
    #
	# Return a platform file for an independent filename(s).  The filenames
	# argument can either be a String filename or Array of String filenames.
	#
	# Returns either an Array of filenames (when multiple files or target is
	# nil) or a single filename when a single filename and target is specified.
	#
	# === Example
	# DHM TODO
	#
	def ProjectUtil::data_content_platform_filenames( project, branch, filenames, target = nil )
		throw ArgumentError.new( "Invalid project specified (#{project.class})." ) \
			unless ( project.is_a?( Pipeline::Project ) )
		throw ArgumentError.new( "Invalid branch specified (#{branch.class})." ) \
			unless ( branch.is_a?( Pipeline::Branch ) )
		throw ArgumentError.new( "Invalid target specified (#{target.class})." ) \
			if ( ( not target.nil? ) and ( not target.is_a?( Pipeline::Target ) ) )
		
		results = []	
		if ( filenames.is_a?( String ) ) then
			filename = OS::Path::normalise( filenames )
			throw RuntimeError.new( "Invalid export or processed file: #{filename}." ) \
				unless ( branch.is_export_file?( filename ) or branch.is_processed_file?( filename ) )
			
			if ( target.nil? ) then
				
				results = []
				branch.targets.each_pair do |platform, target|
					next unless ( target.enabled )
					
					target.in_env() do |env|
						results << env.subst( Resourcing::convert_independent_filename_to_platform( filename, target ) )
					end
				end
				return ( results )
			else
				target.in_env() do |env|
					
					return env.subst( Resourcing::convert_independent_filename_to_platform( filename, target ) )
				end
			end
		elsif ( filenames.is_a?( Array ) ) then
			
			filenames.each do |filename|
				filename = OS::Path::normalise( filename )
				inner = ProjectUtil::data_convert_platform_filenames( project, branch, filename, target )
								
				if ( inner.is_a?( Array ) )
					results += inner
				else
					results << inner
				end
			end
		else
			
			throw ArgumentError.new( "Invalid export or processed filenames object (#{filenames.class})." )
		end
		return ( results )
	end 
 
end # ProjectUtil module
end # Pipeline module

=begin
if ( __FILE__ == $0 ) then
    begin
        require 'pipeline/config/projects'
        include Pipeline
        c = Pipeline::Config::instance
        p = c.project
        p.load_config
        p.load_content
        content = p.content.find_by_script( "'cs1_01' == content.name and content.is_a?( MapZip )" )

        content.each do |c|
    
            puts "CONTENT: #{c}"
            outputs = ProjectUtil::data_content_get_output_files( p, p.branches['dev'], c, true )
            outputs.each do |output|
                puts "\tOUTPUT: #{output}"
            end
        end
    rescue Exception => ex

        puts "Exception :#{ex.message}"
        puts ex.backtrace.join("\n")
    end
end
=end

# %RS_TOOLSLIB%/pipeline/projectutil/data_content.rb
