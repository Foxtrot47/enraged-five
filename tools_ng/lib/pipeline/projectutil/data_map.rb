#
# File:: %RS_TOOLSLIB%/pipeline/projectutil/data_map.rb
# Description:: Map data Ruby functions
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 2 December 2010
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/project'
require 'pipeline/config/projects'
require 'pipeline/content/content_core'
require 'pipeline/content/content_maps'
require 'pipeline/os/file'
require 'pipeline/os/path'
require 'pipeline/os/start'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module ProjectUtil

	# Constants	
	MAP_DATA_META_SERIALISER = '$(toolsbin)/MapExport/MapExportIDEIPL.exe'
	MAP_DATA_VALIDATION = '$(toolsbin)/MapExport/MapExportValidation.exe'
	
	#
	# Yield a user-block for each map RPF in our content tree.
	#
	def ProjectUtil::data_map_for_each( project, level = nil, group_path = nil, &block )
	
		throw ArgumentError.new( "Invalid project object (#{project.class})." ) \
			unless ( project.is_a?( Pipeline::Project ) )

		project.load_content()
		root_content = project.content.find_by_script( "content.name == 'output_inner'" ).shift
		root_content = root_content.find_by_script( "content.is_a?( Pipeline::Content::Level ) and content.name == '#{level}'" ).shift \
			if ( not level.nil? and level.is_a?( String ) )
		root_content = root_content.find_by_script( "content.is_a?( Pipeline::Content::Group ) and content.path.ends_with('#{group_path}')" ).shift \
			if ( not group_path.nil? and group_path.is_a?( String ) )
		if ( root_content.nil? and not level.nil? ) then
			throw RuntimeError::new( "Failed to find level: #{level}." )
		elsif ( root_content.nil? and not group_path.nil? ) then
			throw RuntimeError::new( "Failed to find group path: #{group_path}." )
		end
		
		mapzips = root_content.find_by_script( "content.is_a?( Pipeline::Content::MapZip )" )
		if ( block_given? ) then
			mapzips.each do |mapzip|
				yield mapzip
			end
		end
		mapzips
	end
	
	#
	# Export META files from a SceneXml input file.
	#
	def ProjectUtil::data_map_export_meta( input_filename, ityp_filename, imap_filename, definitions, instances, streamdir, builddir )
		
		c = Pipeline::Config::instance()
		opts = "--iplstream #{streamdir} --build #{builddir}"
		command_line = c.envsubst( "#{MAP_DATA_META_SERIALISER} #{opts} --nopopups " )
		command_line += c.envsubst( "--ityp #{ityp_filename} " ) if ( definitions )
		command_line += c.envsubst( "--imap #{imap_filename} " ) if ( instances )
		command_line += c.envsubst( "#{input_filename}" )
		
		# Delete old meta files in stream.
		File::delete( ityp_filename ) if ( File::exists?( ityp_filename ) )
		File::delete( imap_filename ) if ( File::exists?( imap_filename ) )
		OS::FileUtilsEx::delete_files( OS::Path::combine( streamdir, '*.imap' ) )
		meta_stream_files = OS::FindEx::find_files( OS::Path::combine( streamdir, '*.ityp' ) )
		meta_stream_files += OS::FindEx::find_files( OS::Path::combine( streamdir, '*.imap' ) )
		meta_stream_files.each do |m|
			File::delete( m ) if ( File::file?( m ) )	
		end
		
		# Invoke the serialiser and then return the META filename if successful.
		files = []
		system( command_line )
		meta_stream_files = OS::FindEx::find_files( OS::Path::combine( streamdir, '*.ityp' ) )
		meta_stream_files += OS::FindEx::find_files( OS::Path::combine( streamdir, '*.imap' ) )
		meta_stream_files 
	end
	
	#
	# Export IDE files from a SceneXml input file.
	#
	def ProjectUtil::data_map_export_ide( input_filename, ide_filename, builddir )
		
		c = Pipeline::Config::instance()
		opts = "--build #{builddir}"
		command_line = c.envsubst( "#{MAP_DATA_META_SERIALISER} #{opts} --nopopups " )
		command_line += c.envsubst( "--ide #{ide_filename} " )
		command_line += c.envsubst( "#{input_filename}" )
				
		# Invoke the serialiser and then return the IDE filename if successful.
		system( command_line )
		ide_filename
	end
	
	#
	# Validate a Map; using the standalone validation executable.
	#
	def ProjectUtil::data_map_validate( map )
		throw ArgumentError::new( "Invalid Map content node (#{map.class})." ) \
			unless ( map.is_a?( Content::MapZip ) )
		
		c = Pipeline::Config::instance( )
		p = c.projects[ENV['RS_PROJECT']]
		ulog = OS::Path::combine( c.toolsroot, 'logs', 'map_validation', map.name + '.ulog' ) 
		xml =  OS::Path::replace_ext( map.filename, 'xml' )
		if ( not File::exists?( xml ) ) then
			return -1
		end
		
		command_line = c.envsubst( "#{MAP_DATA_VALIDATION} " )
		command_line += " --nopopups "
		command_line += c.envsubst( " --build #{p.export} " )
		command_line += c.envsubst( " --unilog #{ulog} " )
		command_line += c.envsubst( xml )
		
		status, out, err = OS::start( command_line )
		if ( status.to_i < 2 ) then
			FileUtils::rm( ulog )
		end
		status
	end
		
end # ProjectUtil module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/projectutil/data_map.rb
