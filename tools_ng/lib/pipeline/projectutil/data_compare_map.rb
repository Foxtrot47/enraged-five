#
# File:: data_compare_map.rb
# Description:: Functions for comparing the map data of two projects.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
#
# Date:: 13 November 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/projectutil/data_compare'
require 'pipeline/projectutil/data_remove'

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
module Pipeline
module ProjectUtil
	
	#
	# == Description
	# Functions which compare the map data of two projects, one root 
	# control project (e.g. GTA4) and another episodic project (e.g. GTA4 E1).  
	# The platform data for a single target is extracted for both projects and 
	# GNU diff is used to determine if they are different.  
	#
	# Files which are different are put into the resultant output data which
	# is then re-constructed into the same format as the input data (e.g. RPFs
	# or CD Images archives).
	#
	# An optional code block can be given to process changed files.  For image
	# files the image file is re-constructed so it contains only different files
	# and then the filename is passed to the block.
	#
	# == Example Usage
	# DHM TODO
	#
	
	#---------------------------------------------------------------------
	# Comparison used in data_compare_platform_episodic.rb
	#---------------------------------------------------------------------
	def ProjectUtil::data_compare_episodic_map_data( root_branch, ep_branch, target, output_dir, image = nil, &block )
		throw RuntimeError.new( "Invalid root project branch, #{root_proj_branch.class}." ) \
		unless ( root_branch.is_a?( Pipeline::Branch ) )
		throw RuntimeError.new( "Invalid episodic project branch, #{ep_proj_branch.class}." ) \
		unless ( ep_branch.is_a?( Pipeline::Branch ) )
		throw RuntimeError.new( "Invalid project target, #{target}." ) \
		unless ( root_branch.targets.has_key?( target ) and ( ep_branch.targets.has_key?( target ) ) )
		
		c = Pipeline::Config::instance( )
		r = RageUtils.new( root_branch.project, root_branch.name )
		t = root_branch.targets[target]
		
		root_platform = ''
		ep_platform = ''
		root_branch.targets[target].in_env do |e|
			root_platform = e.subst( '$(target)' )
		end
		ep_branch.targets[target].in_env do |e|
			ep_platform = e.subst( '$(target)' )
		end
		
		root_independent_files = []
		ep_independent_files = []
		if ( image.nil? )			
			root_independent_files = OS::FindEx::find_files_recurse( OS::Path::combine( root_branch.independent, 'data', 'maps', '*.*' ) )
			ep_independent_files = OS::FindEx::find_files_recurse( OS::Path::combine( ep_branch.independent, 'data', 'maps', '*.*' ) )
		else
			root_independent_files = OS::FindEx::find_files_recurse( OS::Path::combine( root_branch.independent, 'data', 'maps', image ) )
			ep_independent_files = OS::FindEx::find_files_recurse( OS::Path::combine( ep_branch.independent, 'data', 'maps', image ) )
		end
		
		# Loop through all files in the episodic project, comparing to our root
		# project.
		ep_independent_files.each do |filename|
			puts "Processing: #{filename}"
			
			# Fetch episodic project filenames
			ep_independent_filename = filename
			ep_platform_filename = ep_independent_filename.sub( ep_branch.independent, ep_platform )
			ep_platform_filename = Resourcing::convert_independent_filename_to_platform( ep_platform_filename, t )
			
			# Fetch root project filenames
			root_independent_filename = ep_independent_filename.sub( ep_branch.independent, root_branch.independent )
			root_platform_filename = root_independent_filename.sub( root_branch.independent, root_platform )
			root_platform_filename = Resourcing::convert_independent_filename_to_platform( root_platform_filename, t )
			
			unless ( ::File::exist?( root_platform_filename ) )
				# User-block process new episodic files (those not present in the root project).
				yield( ep_independent_filename, ep_independent_filename ) if ( block_given? )
				next
			end
			
			ProjectUtil::data_compare_file( root_branch.project, root_branch, target, r, ep_independent_filename, root_independent_filename, output_dir ) do |new_filename|
				# User-block process different non-image episodic files.
				yield( filename, new_filename ) if ( block_given? )
			end
		end
	end # data_compare_episodic_map_data
	
	
	#---------------------------------------------------------------------
	# Comparison used in data_compare_episodic_disc.rb
	#
	# Also has an option for turning output logging on or off.  When true,
	# this will generate a text file (compare_map_output.txt) in the output 
	# directory, with a list of all the removed files.
	#---------------------------------------------------------------------	
	def ProjectUtil::data_compare_episodic_map_data_disc( root_branch, ep_branch, target, output_dir, log_output = false, image = nil, &block )
		
		# Check that given branches are valid
		throw RuntimeError.new( "Invalid root project branch, #{root_proj_branch.class}." ) \
		unless ( root_branch.is_a?( Pipeline::Branch ) )
		throw RuntimeError.new( "Invalid episodic project branch, #{ep_proj_branch.class}." ) \
		unless ( ep_branch.is_a?( Pipeline::Branch ) )
		throw RuntimeError.new( "Invalid project target, #{target}." ) \
		unless ( root_branch.targets.has_key?( target ) and ( ep_branch.targets.has_key?( target ) ) )
		
		c = Pipeline::Config::instance( )
		r = RageUtils.new( root_branch.project, root_branch.name )
		t = root_branch.targets[target]
		
		# If output file is specified, then create path and remove any existing file
		map_log = ""
		unless log_output == false then
			map_log = OS::Path::combine( output_dir, "compare_map_output.txt" )
			::FileUtils::rm( map_log ) if File.exist?(map_log)
		end
		
		# Set target platform
		root_platform = ''
		ep_platform = ''
		root_branch.targets[target].in_env do |e|
			root_platform = e.subst( '$(target)' )
		end
		ep_branch.targets[target].in_env do |e|
			ep_platform = e.subst( '$(target)' )
		end
		
		root_independent_files = []
		ep_independent_files = []
		ep_branch.independent = "x:/gta_e1/build/independent"
		
		# If no image is given then get all images
		if ( image.nil? )	
			root_independent_files = OS::FindEx::find_files_recurse( OS::Path::combine( root_branch.independent, 'data', 'maps', '*.img*' ) )
			ep_independent_files = OS::FindEx::find_files_recurse( OS::Path::combine( ep_branch.independent, 'data', 'maps', '*.img*' ) )
		else	# Otherwise just get the given image
			root_independent_files = OS::FindEx::find_files_recurse( OS::Path::combine( root_branch.independent, 'data', 'maps', image ) )
			ep_independent_files = OS::FindEx::find_files_recurse( OS::Path::combine( ep_branch.independent, 'data', 'maps', image ) )
		end
		
		# Loop through all the files in the root project and compare
		# them to the episodic project
		root_independent_files.each do |filename|
			puts "Processing: #{filename}"
			
			# Fetch root project filenames
			root_independent_filename = filename
			root_platform_filename = root_independent_filename.sub( root_branch.independent, root_platform )
			root_platform_filename = Resourcing::convert_independent_filename_to_platform( root_platform_filename, t )
			
			# Fetch episodic project filenames
			ep_independent_filename = root_independent_filename.sub( root_branch.independent, ep_branch.independent )
			ep_platform_filename = ep_independent_filename.sub( ep_branch.independent, ep_platform )
			ep_platform_filename = Resourcing::convert_independent_filename_to_platform( ep_platform_filename, t )
			
			unless ( ::File::exist?( ep_independent_filename ) )
				# if the episodic file doesn't exist, use the given block to process the root file
				yield( root_independent_filename, root_independent_filename ) if ( block_given? )
				next
			end
			
			# If the episodic file exists, produce a new root image without any of the episodic
			# items in it.
			ProjectUtil::data_cull_image( ep_branch.project, ep_branch, target, r, root_independent_filename, ep_independent_filename, output_dir, map_log ) do |new_filename|
				yield( filename, new_filename ) if ( block_given? )
			end
		end #loop through root files		
	end	#data_compare_episodic_map_data_disc
	
end # ProjectUtil module
end # Pipeline module

# data_compare_map.rb
