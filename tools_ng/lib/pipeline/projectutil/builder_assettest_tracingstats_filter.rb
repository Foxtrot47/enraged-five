#
# File:: builder_assettest_tracingstats_filter.rb
# Description:: 
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 22 January 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'rexml/document'
require 'rexml/formatters/pretty'
include REXML
require 'win32ole'
require 'pipeline/os/path'
require 'time'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
	module ProjectUtil
		#
		# == Description
		# populate the dest xmldoc with the tracing stat elements
		# Ensure the xml document is in the form required.
		def ProjectUtil::tracing_stat_xmldoc_prepare_filtered(dest_xmldoc)
			#------------Create dest xml doc XML structure------
			root = dest_xmldoc.elements['report']
			root = dest_xmldoc.add_element("report") if not root	
			
			root.add_element("tracing_stats") if not root.elements['tracing_stats']
		end # tracing_stat_xmldoc_prepare
	
		#
		# == Description
		# 		
		def ProjectUtil::append_tracing_stat_filtered(project, branch, platform, dest_xmldoc, src_xmldoc, filter_project, filter_branch, filter_platform, filter_group_name, filter_string, filter_alias, date_from, date_to, replace_strings, pre_quailfy_alias = false, contains_multiple_tests = false)				
			throw ArgumentError.new( 'filter_group_name CANNOT be WILDCARD or empty.' + project) if filter_group_name=='' or filter_group_name=='*'
			
			return if filter_project.length>0	and project.casecmp(filter_project)!=0
			return if filter_branch.length>0	and branch.casecmp(filter_branch)!=0
			return if filter_platform.length>0  and platform.casecmp(filter_platform)!=0					
				
			full_filter_name	= filter_string
			
			strs = filter_string.split(".")
			attribute_name		= strs[-1]											
			
			strs.delete_at(-1)											
			filter_name			= ''
			strs.each { |str| filter_name += str + '.' }	
			filter_name.chop!
			
			split_filters = filter_name.split("..")
			wild = split_filters.length >=2
			
			# there WILL be a better of doing this, in many ways too, the wildcards are not TRUE wildcards!
			regexp_string = filter_name.dup
			10.times { regexp_string = regexp_string.gsub('..','.(.*).') }# /\.\./ # nb. The dots are actually interpretted as metacharacters in the regexp.. a slight annoyance that could be fixed. with escaping... but it aint too easy actually.
			regexp_filter = Regexp.new(regexp_string)
						 			
			dest_elem	= dest_xmldoc.root.elements['tracing_stats']	
			ts_node		= src_xmldoc.root.elements['tracing_stats']			
						
			ts_node.elements.each(filter_group_name) do |ts_group_elem|
				
				date = ts_group_elem.attributes['date']
				time = ts_group_elem.attributes['time']
				
				# Date filtering 
				ts_time = Time.parse(date) 								
				next if date_from and ts_time<Time.parse(date_from)				 
				next if date_to and ts_time>Time.parse(date_to)
				
				ts_group_elem.elements.each do |ts_elem|	
					if (ts_elem.name =~ regexp_filter) then 
						time_string = date+" "+time unless contains_multiple_tests
						time_string = date if contains_multiple_tests
						
						# append or create?
						elem = nil					
						dest_elem.elements.each('tracing_stat') do |search_dest_elem|
							if (search_dest_elem.attributes['time_date']==time_string) then
								elem = search_dest_elem
								break
							end
						end	
											
						if elem == nil then
							elem = dest_elem.add_element("tracing_stat") 
							elem.add_attribute('time_date',time_string)					
						end
						
						val = ts_elem.attributes[attribute_name]
						pre_qual_str = "#{project}.#{branch}.#{platform}"
						pre_qualifier = pre_quailfy_alias ? pre_qual_str : ''
						elem_name = "#{pre_qualifier}.#{filter_alias}"											if		(filter_alias.length>0 and not wild)
						elem_name = "#{pre_qual_str}.#{filter_group_name}.#{ts_elem.name}.#{attribute_name}"	unless  (filter_alias.length>0 and not wild)
						
						# make the fields in the xls more user firendly if required
						replace_strings.each do |replace_string|
							str = replace_string.split(',')
							str << '' if str.length==1 
							elem_name.sub!(str[0],str[1])
						end
						
						new_elem = elem.add_element(elem_name)		

						new_elem.add_text(val)
					end # end regexp match
				end
			end			
		end # append_tracing_stat_filtered
						
		#
		# == Description
		# Process and individual file for builder_assettest_tracingstats_filter
		def ProjectUtil::builder_assettest_tracingstats_filter_single_file(src_filename, dest_filename, filter, date_from, date_to, replace_strings, contains_multiple_tests = false)		
			begin
				puts "File not found #{src_filename}" and return '' if not File.exists?(src_filename)
				dest_directory = OS::Path.get_directory(dest_filename)
				FileUtils::mkdir_p( dest_directory ) unless ( File::directory?( dest_directory ) )					
				
				puts "using : src_filename #{src_filename}"
				
				#----------- Open Source xml -------------------------						
				src_file = File.new( src_filename )
				src_xmldoc = REXML::Document.new( src_file )
				
				#------------Read project details---------------------
				project_node	= src_xmldoc.root.elements['project']
				project			= project_node.attributes['project']
				branch			= project_node.attributes['branch']	
				platform		= project_node.attributes['platform']
				
				#------------ Goto Tracing stats node ----------------
				tracingstat_node = src_xmldoc.root.elements['tracing_stats']			
				
				#------------ Read or create dest xml doc --------
				dest_xmldoc = REXML::Document.new
				dest_xmldoc << REXML::XMLDecl.new	
				dest_xmldoc = REXML::Document.new( File.open( dest_filename, 'r' ) ) if ( File::exists?( dest_filename ) )
												
				#------------ Filtrate! --------				
				filter.split(';').each do |filter_string|				
					#------------ Prepare dest xml doc ---------------
					tracing_stat_xmldoc_prepare_filtered(dest_xmldoc)
				
					#------------ Prepare filter string -----------------
					# idea : () parenthesis have contents removed. [] have contexts kept.
					# , seperates alias out 
					# first there dots required for project,branch,platform (... is empty), *.*.* is also valid, but *immy* does not equate to jimmy
						
					# strip out aliases if we can 
					aliases = filter_string.split(',')
					use_alias = aliases.length==2
					filter_alias = use_alias ? aliases[1] : ''
					
					# get all the substrings of the filter
					filter_substrings = aliases[0].split('.')				
					throw Exception.new( 'Invalid filter string in stat.') if filter_substrings.length<4										
					
					# Take out all *s and store fixed fields in variables
					filter_substrings.collect!{|a| a=='*' ? '' : a }					
					filter_project,filter_branch,filter_platform,filter_group_name 	= filter_substrings[0],filter_substrings[1],filter_substrings[2],filter_substrings[3]
					
					replace2 = "#{filter_project}.#{filter_branch}.#{filter_platform}."
					replace1 = "#{replace2}#{filter_group_name}."					
					
					filter_string_stripped = aliases[0].sub(replace1,"")
					
					# handy shorthand for stats.
					# hides the complexities of the tracing stat sampling technique.
					# however some aliases will possible be exposed.
					possible_alias =  aliases[0].sub(replace2,"")
					stat_func = ["MAX","MIN","MEAN"]
										
					if filter_string_stripped.chomp!(".*") then						

						filter_string_stripped.gsub!('*','')
						
						#------------ Append to dest xml doc -------------
						base_name = filter_string_stripped.dup
						base_alias = filter_alias.dup		
						
						stat_func.each do |stat_func_type|
							stat_func_type_lower = stat_func_type.downcase
							filter_alias="#{base_alias}.#{stat_func_type}"			if		use_alias
							filter_alias="#{base_name}.#{stat_func_type}"			unless  use_alias
							filter_string_stripped="#{base_name}.#{stat_func_type_lower}Sampled.#{stat_func_type_lower}" 
							#------------ Append 3x to dest xml doc -------------
							append_tracing_stat_filtered(project, branch, platform, dest_xmldoc, src_xmldoc, filter_project, filter_branch, filter_platform, filter_group_name, filter_string_stripped, filter_alias, date_from, date_to,replace_strings,false,contains_multiple_tests)																	
						end	
					else
						
						filter_string_stripped.gsub!('*','')
						
						stat_func.each do |stat_func_type|
							stat_func_type_lower = stat_func_type.downcase
							if filter_string_stripped.chomp!(".#{stat_func_type}") then	
								filter_string_stripped+=".#{stat_func_type_lower}Sampled.#{stat_func_type_lower}"	
								filter_alias=possible_alias
								use_alias=true
								break
							end						
						end

						pre_quailfy_alias = true
						
						#------------ Append to dest xml doc -------------
						append_tracing_stat_filtered(project, branch, platform, dest_xmldoc, src_xmldoc, filter_project, filter_branch, filter_platform, filter_group_name, filter_string_stripped, filter_alias, date_from, date_to, replace_strings, pre_quailfy_alias, contains_multiple_tests)				
					end
				end

				#------------ Save dest xml doc ------------------
				File.open( dest_filename, 'w+' ) do |file|				
					fmt = REXML::Formatters::Pretty.new()
					fmt.write( dest_xmldoc, file )    						
				end		
			rescue Exception => ex
				puts "builder_assettest_tracingstats_filter_single_file has quit unexpectedly:"
				puts "\t#{ex.message}"
				ex.backtrace.each do |m| puts "\t#{m}"; end
			end		
		
			'' # return some output if we want 				
		end # builder_assettest_tracingstats_filter_single_file
		
		#
		# == Description
		# Takes a bunch of source xml files ( tracing stat processed files ) 		
		# and appends then to existing ( or creates new ) files in the passed directory.
		# inserting a timestamp as it goes.
		
		# class repository for Excel constants involved in Win32OLE calls.
		class ExcelConst
			def ExcelConst::filled=(val)
				@@filled = val
			end
			def ExcelConst::filled()
				@@filled
			end
		private
			@@filled = false
		end		

		# class repository for MSO constants involved in Win32OLE calls.
		class MSOConst
			def MSOConst::filled=(val)
				@@filled = val
			end
			def MSOConst::filled()
				@@filled
			end
			private
			@@filled = false
		end				
		
		def ProjectUtil::builder_assettest_tracingstats_filter( chart_title, src_files, replace_strings, dest_filename, filter, date_from, date_to, save_jpg, save_xls, save_xml, chart_axis_y, contains_multiple_tests = false)	
			output = ''
			begin							
				# Always start with a blank file if we can.				
				File.delete( dest_filename ) if File::exists?( dest_filename )
				
				src_files.each do |scr_file| 
#					throw IOError.new( "File #{scr_file} does not exist.  Aborting convert." ) unless ( File::exists?( scr_file ) )										
					puts "File #{scr_file} does not exist.  Aborting convert."  unless ( File::exists?( scr_file ) )
					output += builder_assettest_tracingstats_filter_single_file(scr_file,dest_filename, filter, date_from, date_to, replace_strings, contains_multiple_tests)
				end	

				if File::exists?( dest_filename ) then 
					begin		
						puts "	 => #{dest_filename}"
							
						begin
							excel = WIN32OLE.connect("excel.application") 							
						rescue
							puts " * A new instance of Excel has been started, if you see this message more than once it may be a leaking process! * "
							excel = WIN32OLE.new("excel.application") if excel==nil 
						end
						
						excel.DisplayAlerts = false
						WIN32OLE.const_load(excel, ExcelConst) if not ExcelConst::filled
						ExcelConst::filled = true
						#WIN32OLE.const_load('Microsoft Office 9.0 Object Library', MSOConst) if not MSOConst::filled						
						#MSOConst::filled = true
						#excel.visible = true     # in case you want to see what happens 
						workbook	= excel.Workbooks.OpenXML(dest_filename,nil,ExcelConst::XlXmlLoadImportToList)
						worksheet	= workbook.Worksheets(1)								
						worksheet.Activate														
						chart						= workbook.Charts.Add	
						chart.HasTitle				= true
						chart_name					= dest_filename if not chart_name
						chart.ChartTitle.Text		= chart_title
						chart.ChartType				= ExcelConst::XlLineMarkers 
						chart.ChartTitle.Font.Name	= 'Verdana'
						chart.ChartTitle.Font.Size	= 12
						chart.ChartTitle.Font.Bold	= true
						
						chart.Legend.Font.Size = 8	
						
						#workbook.WebOptions.ScreenSize = msoScreenSize800x600
						#workbook.WebOptions.PixelsPerInch = 120
				
						axis						= chart.Axes(1) #XL_CATEGORY = 1XL_VALUE = 2XL_SERIESAXIS = 3					
						axis.HasTitle				= true
						axis.AxisTitle.Text			= "Date"					

						if (chart_axis_y) then 
							axis				= chart.Axes(2)					
							axis.HasTitle		= true
							axis.AxisTitle.Text = chart_axis_y					
						end
						
						chart.SetSourceData(worksheet.range("a1").currentregion,ExcelConst::XlColumns)	
						
						xls_filename = OS::Path.replace_ext(dest_filename,'xls')
						jpg_filename = OS::Path.replace_ext(dest_filename,'jpg')
						chart.Export(jpg_filename, "JPG") if save_jpg
		
						workbook.saveas(xls_filename) if save_xls
						workbook.close	
						
					rescue Exception => ex
						puts "WIN32OLE Excel stuff has quit unexpectedly:"
						puts "\t#{ex.message}"
						ex.backtrace.each do |m| puts "\t#{m}"; end
						excel.Quit()
					ensure
						excel.Quit() unless excel.nil?
					end	# end begin 1
					
					excel.Quit() unless excel.nil?
					
					File.delete( dest_filename ) if not save_xml					
				end # end if file exists
							
				rescue Exception => ex
					puts "builder_assettest_tracingstats_filter has quit unexpectedly:"
					puts "\t#{ex.message}"
					ex.backtrace.each do |m| puts "\t#{m}"; end
				end	#end begin 2				
			output
		end	# builder_assettest_tracingstats_filter
	end # ProjectUtil module
end # Pipeline module

# builder_assettest_tracingstats_filter.rb