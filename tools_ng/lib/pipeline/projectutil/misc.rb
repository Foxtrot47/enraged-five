#
# File:: misc.rb
# Description:: Miscellaneous ProjectUtil module functions.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 5 November 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/config/branch'
require 'pipeline/config/targets'
require 'pipeline/content/content_core'
require 'pipeline/os/start'
require 'pipeline/projectutil/data_convert'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module ProjectUtil

	#-------------------------------------------------------------------------
	# Constants
	#-------------------------------------------------------------------------
	
	#-------------------------------------------------------------------------
	# Functions
	#-------------------------------------------------------------------------

	# == Description		
	# Replaces characters that are deemed unsafe (when used within HTML/XML documents or otherwise) whatever, ones that use unicode utf-8 character encoding.
	# the replacement is a question mark '?' character.
	# see http://www.tony-franks.co.uk/UTF-8.htm sorry I still can't find the definitive specification for utf-8.
	def ProjectUtil::replace_invalid_utf8_chars(str, char = '?')				
		str.gsub(/[\x7f-\xff|\x00-\x09|\x0B-\x0C|\x0E-\x1f]/,char)		
	end
	
	#
	# == Description
	# Return the project and branch objects for an Array of filenames.  If the
	# files belong to multiple projects or branches a RuntimeError exception 
	# is raised.
	#
	# The optional block is executed with the project, branch and filename
	# string.
	#
	# === Example Usage
	# DHM TODO
	#
	def ProjectUtil::get_project_from_filenames( filenames, &block )
		raise ArgumentError.new( "Invalid Array of filenames (#{filenames.class})." ) \
			unless ( filenames.is_a?( Array ) )

		config = Pipeline::Config::instance()
		conv_project = nil
		conv_branch = nil

		filenames.each do |filename|
			throw IOError.new( "File #{filename} does not exist or is not readable." ) \
				unless ( ::File::exists?( filename ) and ::File::readable?( filename ) )		
			filename = OS::Path::normalise( File::expand_path( filename ) )
		
			config.projects.each_pair do |name, project|
				next unless ( filename.starts_with( project.root ) )
				
				project.load_config( )
				project.branches.each_pair do |branch_name, branch|
					next unless ( branch.is_export_file?( filename ) or branch.is_processed_file?( filename ) )
				
					independent_file = Content::from_filename( project, branch_name, filename )
					branch.targets.each_pair do |platform, target|						
						next unless ( target.enabled )
						
						target_filename = ProjectUtil::data_convert_platform_filenames( project, branch, filename, target )
						
						conv_project = project
						conv_branch = branch
						
						# Call user-block
						yield( conv_project, conv_branch, filename ) \
							if ( block_given? and ( not ( conv_project.nil? or conv_branch.nil? ) ) )
					end
				end
			end
		end
		
		# Return project and branch.
		[ conv_project, conv_branch ]
	end

end # ProjectUtil module
end # Pipeline module

# misc.rb
