#
# File:: %RS_TOOLSLIB%/pipeline/projectutil/data_rpf_sort.rb
# Description:: RPF asset sorting functions.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 9 February 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/log/log'
require 'pipeline/os/path'
require 'pipeline/util/time'
include Pipeline

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module ProjectUtil

    # Platform patterns for particular files we look for during sorting.
    PATTERN_MATCH_ITYP = '*.?typ'
    PATTERN_MATCH_IMAP = '*.?map'
    PATTERN_MATCH_ITD = '*.?td'
    PATTERN_MATCH_IBN = '*.?bn'
    PATTERN_MATCH_CLIP = '*.?cd'
    PATTERN_MATCH_MANIFEST = '*_manifest.?mf'
    PATTERN_PHYSICS = '.?bd'
    PATTERN_ITD = '.?td'
    PATTERN_IDR = '.?dr'
    PATTERN_IDD = '.?dd'
    PATTERN_IFT = '.?ft'

    #
    # == Description
    # Returns a sorted list of files for the RPF.
    #
    def ProjectUtil::data_rpf_sort_filelist( file_list )
    
        # Determine if we have an embedded ITYP; if so we don't use our fallback
        # sorting mechanism.
        has_ityp = false
        ityp_filename = nil
        file_list.each do |item|
            if ( item.is_a?( Hash ) and File::fnmatch?( PATTERN_MATCH_ITYP, item[:src], File::FNM_CASEFOLD ) ) then
                has_ityp = true
                ityp_filename = item[:src]
            elsif ( item.is_a?( String ) and File::fnmatch?( PATTERN_MATCH_ITYP, item, File::FNM_CASEFOLD ) )
                has_ityp = true
                ityp_filename = item
            end
        end
        if ( has_ityp ) then
            drive = OS::Path::get_drive( ityp_filename )
            parts = OS::Path::get_parts( ityp_filename )
            parts.delete_at( 0 ) # drive
            parts.delete_at( parts.size - 2 ) # remove platform path
            ityp_filename = OS::Path::replace_ext( OS::Path::combine( "#{drive}:", *parts ), 'ityp' )
            @@rpf_log.info( "Platform ITYP found; RPF will be sorted according to asset dependencies." )
        else
            @@rpf_log.info( "Platform ITYP not found; RPF will be sorted according to naming asset name/type." )
        end
    
        sorted_file_list = []
        sort_time = Util::time do 
            
            if ( not has_ityp ) then
                # Default sorting algorithm where we match up asset names; ordering
                # as: TXD, +HD TXD, DRAW/FRAG/DRAWDICT.
            
                # Sort file_list for those zips that don't have an ITYP.
                sorted_file_list = file_list.sort do |a, b|
                    source_filename = a unless ( a.is_a?( Hash ) )
                    source_filename = a[:src] if ( a.is_a?( Hash ) )
                    source_base_split = OS::Path::get_basename( source_filename ).split( '+' )
                    source_base = source_base_split[0]
                
                    comp_filename = b unless ( b.is_a?( Hash ) )
                    comp_filename = b[:src] if ( b.is_a?( Hash ) )
                    comp_base_split = OS::Path::get_basename( comp_filename ).split( '+' )
                    comp_base = comp_base_split[0]

                    if ( ( comp_base.is_a?( String ) and source_base.is_a?( String ) ) and 
                         0 == comp_base.casecmp( source_base ) )
                        # Same asset name; TXD vs HD TXD or different asset type.
                        if ( File::fnmatch?( PATTERN_MATCH_ITD, source_filename, File::FNM_CASEFOLD ) and
                             File::fnmatch?( PATTERN_MATCH_ITD, comp_filename, File::FNM_CASEFOLD ) )
                            # Both TXDs, so one is HD which we put after.
                            if ( source_base_split.size > 1 ) then
                                1 # Source HD TXD, Comp TXD, Comp First.
                            else
                                -1 # Comp HD TXD, Source TXD, Source First.
                            end
                        elsif ( File::fnmatch?( PATTERN_MATCH_ITD, source_filename, File::FNM_CASEFOLD ) )
                            -1
                        elsif ( File::fnmatch?( PATTERN_MATCH_ITD, comp_filename, File::FNM_CASEFOLD ) )
                            1
                        else
                            0
                        end
                    else
                        # Filename comparison.
                        ( source_filename <=> comp_filename )
                    end
                end            
            else
                # ITYP present; use the archetype definitions to order our data
                # as that specifies the data dependencies.
                sorted_file_list = ProjectUtil::data_rpf_sort_filelist_with_ityp(
                    file_list, ityp_filename )
            end
        end
        @@rpf_log.error( "RPF sort issues; input (#{file_list.size}) and output (#{sorted_file_list.size}) file list size mismatch." ) \
            unless ( sorted_file_list.size() == file_list.size() )
        @@rpf_log.info( "RPF sort time: #{sort_time}s." )
    
        # DHM REMOVE ME
=begin
        begin
            if ( sorted_file_list.size() != file_list.size() ) then
                File::open( 'x:/unsorted_rpf.txt', 'w' ) do |fp|
                    filelist = file_list.collect do |item|
                        if ( item.is_a?( Hash ) ) then
                            item[:dst]
                        elsif ( item.is_a?( String ) )
                            item
                        else
                            "*** WTF: #{item}"
                        end
                    end
                    fp.write( filelist.join("\n") )
                end
                File::open( 'x:/sorted_rpf.txt', 'w' ) do |fp|
                    filelist = sorted_file_list.collect do |item|
                        if ( item.is_a?( Hash ) ) then
                            item[:dst]
                        elsif ( item.is_a?( String ) )
                            item
                        else
                            "*** WTF: #{item}"
                        end
                    end
                    fp.write( filelist.join("\n") )
                end
            end
        rescue Exception => ex
            puts "EX: #{ex.message}"
            puts ex.backtrace.join("\n")
        end
=end
        # END DHM REMOVE ME
    
        sorted_file_list
    end

    #
    # == Description
    # Returns a sorted file list for RPF construction based on an input
    # ITYP file specifying asset dependencies.
    #
    def ProjectUtil::data_rpf_sort_filelist_with_ityp( file_list_unsorted, ityp_filename )

        if ( not File::exists?( ityp_filename ) ) then
            @@rpf_log.warn( "ITYP file for sorting input not found (#{ityp_filename})." )
            
            file_list_unsorted
        else
            # Parse the ITYP file creating a Hash of dependencies; based on
            # object name.
            assets = {}
            File.open( ityp_filename ) do |fp|
                doc = REXML::Document::new( fp )
                doc.elements.each( 'CMapTypes/archetypes/Item' ) do |item|
                    # For each archetype; determine its dependency files and
                    # maintain a list of them.
                        
                    drawable_or_fragment = ''
                    drawable_dictionary = nil
                    texture_dictionary = ''
                    physics_dictionary = ''
                                    
                    # Process each Archetype's elements to determine its 
                    # dependencies and the type of asset.
                    item.elements.each do |elem|
                        case elem.name
                        when 'name'
                            drawable_or_fragment = elem.text
                        when 'drawableDictionary'
                            drawable_dictionary = elem.text unless ( elem.text.nil? )
                        when 'textureDictionary'
                            texture_dictionary = elem.text unless ( elem.text.nil? )
                        when 'physicsDictionary'
                            physics_dictionary = elem.text unless ( elem.text.nil? )
                        end
                    end
                    
                    if ( not drawable_dictionary.nil? ) then
                        # Drawable dictionary is the core asset.
                        assets[drawable_dictionary] = {} unless ( assets.has_key?( drawable_or_fragment ) )
                        assets[drawable_dictionary][:txd] = texture_dictionary unless ( texture_dictionary.empty? )
                        assets[drawable_dictionary][:txd_hd] = "#{texture_dictionary}+hi" unless ( texture_dictionary.empty? )
                    else
                        # Drawable/Fragment case.
                        assets[drawable_or_fragment] = {} unless ( assets.has_key?( drawable_or_fragment ) )
                        assets[drawable_or_fragment][:txd] = texture_dictionary unless ( texture_dictionary.empty? )
                        assets[drawable_or_fragment][:txd_hd] = "#{texture_dictionary}+hi" unless ( texture_dictionary.empty? )
                        assets[drawable_or_fragment][:txd_drhd] = "#{drawable_or_fragment}+hidr" # In case IDR produces HD TXD.
                        assets[drawable_or_fragment][:txd_frhd] = "#{drawable_or_fragment}+hifr" # In case IFT produces HD TXD.
                        assets[drawable_or_fragment][:physics] = physics_dictionary unless ( physics_dictionary.empty? )
                    end
                end
            end
            
            file_list = file_list_unsorted.dup
            sorted_file_list = []
            
            # MANIFEST
            manifest_files = []
            file_list.each do |item|            
                if ( item.is_a?( String ) and File::fnmatch?( PATTERN_MATCH_MANIFEST, item, File::FNM_CASEFOLD ) ) 
                    manifest_files << item
                elsif ( item.is_a?( Hash ) and File::fnmatch?( PATTERN_MATCH_MANIFEST, item[:src], File::FNM_CASEFOLD ) )
                    manifest_files << item
                end
            end
            manifest_files.sort! do |a, b|
                a[:src] <=> b[:src]
            end
            manifest_files.each do |manifest|
                file_list.delete( manifest )
                sorted_file_list << manifest
            end
            
            # ITYP
            ityp_files = []
            file_list.each do |item|            
                if ( item.is_a?( String ) and File::fnmatch?( PATTERN_MATCH_ITYP, item, File::FNM_CASEFOLD ) ) 
                    ityp_files << item
                elsif ( item.is_a?( Hash ) and File::fnmatch?( PATTERN_MATCH_ITYP, item[:src], File::FNM_CASEFOLD ) )
                    ityp_files << item
                end
            end
            ityp_files.sort! do |a, b|
                a[:src] <=> b[:src]
            end
            ityp_files.each do |ityp|
                file_list.delete( ityp )
                sorted_file_list << ityp
            end
            
            # IMAPs
            imap_files = []
            file_list.each do |item|
                if ( item.is_a?( String ) and File::fnmatch?( PATTERN_MATCH_IMAP, item, File::FNM_CASEFOLD ) ) 
                    imap_files << item
                elsif ( item.is_a?( Hash ) and File::fnmatch?( PATTERN_MATCH_IMAP, item[:src], File::FNM_CASEFOLD ) )
                    imap_files << item
                end
            end
            imap_files.sort! do |a, b|
                # Handle IMAP groups with '_lod' IMAP files being a dependency
                # for the regular HD group IMAP.  E.g. <group>_lod.imap is
                # a dependency for <group>.imap.                
                if ( File::fnmatch?( '*_lod.?map', a[:src], File::FNM_CASEFOLD ) ) then 
                
                    basename = OS::Path::get_basename( b[:src] )                
                    if ( a[:dst].downcase.starts_with( basename ) ) then
                        # LOD first.
                        -1
                    else
                        # Default name order.
                        a[:src] <=> b[:src]
                    end
                elsif ( File::fnmatch?( '*_lod.?map', b[:src], File::FNM_CASEFOLD ) ) then
                
                    basename = OS::Path::get_basename( a[:src] )                    
                    if ( b[:dst].downcase.starts_with( basename ) ) then
                        # LOD first.
                        1
                    else
                        # Default name order.
                        a[:src] <=> b[:src]
                    end
                else
                    # Default name order.
                    a[:src] <=> b[:src]
                end
            end
            imap_files.each do |imap|
                file_list.delete( imap )
                sorted_file_list << imap
            end
            
            # Physics
            physics_files = []
            file_list.each do |item|
                if ( item.is_a?( String ) and File::fnmatch?( PATTERN_MATCH_IBN, item, File::FNM_CASEFOLD ) ) 
                    physics_files << item
                elsif ( item.is_a?( Hash ) and File::fnmatch?( PATTERN_MATCH_IBN, item[:src], File::FNM_CASEFOLD ) )
                    physics_files << item
                end
            end
            physics_files.sort! do |a, b|
                a[:src] <=> b[:src]
            end
            physics_files.each do |physics|
                file_list.delete( physics )
                sorted_file_list << physics
            end
                
            # Clip dictionaries
            clip_files = []
            file_list.each do |item|
                if ( item.is_a?( String ) and File::fnmatch?( PATTERN_MATCH_CLIP, item, File::FNM_CASEFOLD ) ) 
                    clip_files << item
                elsif ( item.is_a?( Hash ) and File::fnmatch?( PATTERN_MATCH_CLIP, item[:src], File::FNM_CASEFOLD ) )
                    clip_files << item
                end
            end
            clip_files.sort! do |a, b|
                a[:src] <=> b[:src]
            end
            clip_files.each do |clip|
                file_list.delete( clip )
                sorted_file_list << clip
            end
            
            # Assets
            assets.each_pair do |asset, dependencies|
            
                if ( dependencies.has_key?( :physics ) ) then
                    pattern = "**/#{dependencies[:physics]}#{PATTERN_PHYSICS}"                
                    file_list.each do |item|                        
                        if ( item.is_a?( String ) and File::fnmatch?( pattern, item, File::FNM_CASEFOLD ) )
                            file_list.delete( item ) 
                            sorted_file_list << item
                            break
                        elsif ( item.is_a?( Hash ) and File::fnmatch?( pattern, item[:src], File::FNM_CASEFOLD ) )
                            file_list.delete( item ) 
                            sorted_file_list << item
                            break
                        end
                    end
                end
                if ( dependencies.has_key?( :txd ) ) then
                    pattern = "**/#{dependencies[:txd]}#{PATTERN_ITD}"     
                    
                    file_list.each do |item|  
                        if ( item.is_a?( String ) and File::fnmatch?( pattern, item, File::FNM_CASEFOLD ) )
                            file_list.delete( item ) 
                            sorted_file_list << item
                            break
                        elsif ( item.is_a?( Hash ) and File::fnmatch?( pattern, item[:src], File::FNM_CASEFOLD ) )
                            file_list.delete( item ) 
                            sorted_file_list << item
                            break
                        end
                    end
                end
                if ( dependencies.has_key?( :txd_hd ) ) then
                    pattern = "**/#{dependencies[:txd_hd]}#{PATTERN_ITD}"     
                    
                    file_list.each do |item|  
                        if ( item.is_a?( String ) and File::fnmatch?( pattern, item, File::FNM_CASEFOLD ) )
                            file_list.delete( item ) 
                            sorted_file_list << item
                            break
                        elsif ( item.is_a?( Hash ) and File::fnmatch?( pattern, item[:src], File::FNM_CASEFOLD ) )
                            file_list.delete( item ) 
                            sorted_file_list << item
                            break
                        end
                    end
                end
                if ( dependencies.has_key?( :txd_drhd ) ) then
                    pattern = "**/#{dependencies[:txd_drhd]}#{PATTERN_ITD}"     
                    
                    file_list.each do |item|  
                        if ( item.is_a?( String ) and File::fnmatch?( pattern, item, File::FNM_CASEFOLD ) )
                            file_list.delete( item ) 
                            sorted_file_list << item
                            break
                        elsif ( item.is_a?( Hash ) and File::fnmatch?( pattern, item[:src], File::FNM_CASEFOLD ) )
                            file_list.delete( item ) 
                            sorted_file_list << item
                            break
                        end
                    end
                end
                if ( dependencies.has_key?( :txd_frhd ) ) then
                    pattern = "**/#{dependencies[:txd_frhd]}#{PATTERN_ITD}"     
                    
                    file_list.each do |item|  
                        if ( item.is_a?( String ) and File::fnmatch?( pattern, item, File::FNM_CASEFOLD ) )
                            file_list.delete( item ) 
                            sorted_file_list << item
                            break
                        elsif ( item.is_a?( Hash ) and File::fnmatch?( pattern, item[:src], File::FNM_CASEFOLD ) )
                            file_list.delete( item ) 
                            sorted_file_list << item
                            break
                        end
                    end
                end
                
                # Added dependencies so now add the asset.
                pattern_idd = "**/#{asset}#{PATTERN_IDD}"
                pattern_idr = "**/#{asset}#{PATTERN_IDR}"
                pattern_ift = "**/#{asset}#{PATTERN_IFT}"
                file_list.each do |item|
                    filename = ''
                    if ( item.is_a?( String ) )
                        filename = item
                    elsif ( item.is_a?( Hash ) )
                        filename = item[:src]
                    end
                    
                    if ( File::fnmatch?( pattern_idd, filename, File::FNM_CASEFOLD ) or
                            File::fnmatch?( pattern_idr, filename, File::FNM_CASEFOLD ) or
                            File::fnmatch?( pattern_ift, filename, File::FNM_CASEFOLD ) ) then
                        file_list.delete( item ) 
                        sorted_file_list << item
                        break
                    end
                end
            end
               
            # Mop up the rest.
            file_list.each do |mop|
                sorted_file_list << mop
            end
                 
            sorted_file_list
        end
    end

end # ProjectUtil module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/projectutil/data_rpf_sort.rb
