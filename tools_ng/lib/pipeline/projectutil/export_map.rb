#
# File:: export_map.rb
# Description:: Utility functions to ease map export functions from Ruby.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 11 December 2008
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/fileformats/scenexml'
require 'pipeline/fileformats/scenexml_ide'
require 'pipeline/fileformats/scenexml_ipl'
require 'pipeline/os/path'

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------
module Pipeline
module ProjectUtil
	
	#
	# == Description
	# Exception raised when there was an exception during map export.
	#
	class ExportMapException < Exception; end
	
	#
	# Validate a SceneXml object.
	#
	def ProjectUtil::export_map_scenexml_validate( scenexml, &block )
		throw ArgumentError.new( "Invalid SceneXml::Scene object (#{scenexml.class})." ) \
			unless ( scenexml.is_a?( Pipeline::FileFormats::SceneXml::Scene ) )
				
		throw NotImplementedError.new( )
	end
	
	#
	# Export a map IDE file from a SceneXml description file.
	#
	# The user-specified block is called with the SceneXml::Scene and output
	# filename String as parameters.
	#
	def ProjectUtil::export_map_ide( scenexml, filename, &block )
		throw ArgumentError.new( "Invalid SceneXml::Scene object (#{scenexml.class})." ) \
			unless ( scenexml.is_a?( Pipeline::FileFormats::SceneXml::Scene ) )
		
		begin
			converter = SceneXml::IDESerialiser::new( scenexml )
			converter.write( filename )
			
			yield( scenexml, filename ) if ( block_given? )
			
		rescue Exception => ex
			SceneXml::IDESerialiser::log().exception( ex, 'Unhandle IDESerialiser exception' )
			
			puts "Unhandled exception: #{ex.message}"
			puts ex.backtrace.join( "\n" )
			throw ExportMapException.new( ex.message )
		end
	end
	
	#
	# Export an IPL file set from a map SceneXml description file.
	#
	# The user-specified block is called with the SceneXml::Scene and output
	# filename String as parameters.
	#
	def ProjectUtil::export_map_ipls( scenexml, path, &block )
		throw ArgumentError.new( "Invalid SceneXml::Scene object (#{scenexml.class})." ) \
			unless ( scenexml.is_a?( Pipeline::FileFormats::SceneXml::Scene ) )
		
		begin
			converter = SceneXml::IPLSerialiser::new( scenexml )
			converter.write( path )
			
			yield( scenexml, path ) if ( block_given? )
			
		rescue Exception => ex
			SceneXml::IPLSerialiser::log().exception( ex, 'Unhandled IPLSerialiser exception' )

			puts "Unhandled exception: #{ex.message}"
			puts ex.backtrace.join( "\n" )
			throw ExportMapException.new( ex.message )
		end
	end
	
end # ProjectUtil module
end # Pipeline module

#----------------------------------------------------------------------------
# Entry
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
	require 'pipeline/gui/log_window'
	Pipeline::GUI::LogWindow::show_dialog( ) do
		begin
			include Pipeline
			include Pipeline::FileFormats
			filename = ARGV[0].nil? ? 'x:/home/SceneXml/test/manhat09.xml' : ARGV[0] 
			output_ide = OS::Path::combine( OS::Path::get_directory( filename ), OS::Path::get_basename( filename ) ) + '_test.ide'
			output_ipl = OS::Path::get_directory( filename )
			puts "Filename:   #{filename}"
			puts "Output IDE: #{output_ide}"
			puts "Output IPL: #{output_ipl}"
			
			start = Time.now
			scene = SceneXml::Scene::new( filename )
			puts "SceneXml load      : #{Time.now - start}s."
			
			start = Time.now
			##ProjectUtil::export_map_ide( scene, output_ide )
			puts "SceneXml IDE export: #{Time.now - start}s."
			
			start = Time.now
			ProjectUtil::export_map_ipls( scene, output_ipl )
			puts "SceneXml IPL export: #{Time.now - start}s."
		rescue Exception => ex

			Pipeline::LogSystem::instance().rootlog.error( ex.inspect )
			ex.backtrace.each do |m| Pipeline::LogSystem::instance().rootlog.error( m ); end
			
			puts "Unhandled exception: #{ex.message}"
			puts ex.backtrace.join( "\n" )
		end
	end
end

# export_map.rb
