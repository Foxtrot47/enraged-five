#
# File:: data_get_latest.rb
# Description:: Function to fetch latest data for a project.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 6 August 2008
#
# NOTE:
#   Some of these functions take a block to provide status updates to the user.
#   Although from Perforce we don't know when each file is fetched at least we
#   can give users some feedback during long operations.
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/project'
require 'pipeline/scm/perforce'
require 'pipeline/os/path'
require 'pipeline/os/start'

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

module Pipeline
module ProjectUtil

	#
	# Fetches all the latest data for a project.
	#
	# By default the sync will not force overwrite files.
	# The block is invoked after each individual file synced.
	# An Array of all the files synced is returned.
	#
	def ProjectUtil::data_get_latest_all( project, branch = nil, force = false, &block )
		raise ArgumentError.new( "Invalid project." ) \
			unless ( project.is_a?( Pipeline::Project ) )
		branch = project.default_branch if ( branch.nil? )
		raise ArgumentError.new( "Invalid branch string (#{branch}, #{branch.class})." ) \
			unless ( branch.is_a?( String ) and project.branches.has_key?( branch ) )
	
		project.load_config()
		p4 = project.scm()
		p4.connect() unless ( p4.connected? )
		
		args = []
		args << '-f' if force
		
		files_synced = p4.run_sync_with_block( args, "#{project.branches[branch].build}/..." ) do
			|client_file, synced, errors|

			yield( client_file, synced, errors )
		end
		
		files_synced
	end

	#
	# Fetches the latest common and platform data for a project.
	#
	# By default the sync will not force overwrite files.
	# The block is invoked after each individual file synced.
	# An Array of all the files synced is returned.
	#
	def ProjectUtil::data_get_latest_build( project, branch = nil, force = false, &block )

		ProjectUtil::data_get_latest_build_labelled( project, branch, '', force, &block )
	end
	

	
	
	#
	# Fetches the latest common and platform data for a project, based on a 
	# specific label or #head if a specific label is not given.
	#
	# By default the sync will fetch the HEAD revision.
	# By default the sync will not force overwrite files.
	# The block is invoked after each individual file synced.
	# An Array of all the files synced is returned.
	#
	def ProjectUtil::data_get_latest_build_labelled( project, branch = nil, label = '', force = false, &block )
		raise ArgumentError.new( "Invalid project." ) \
		unless ( project.is_a?( Pipeline::Project ) )
		branch = project.default_branch if ( branch.nil? )
		raise ArgumentError.new( "Invalid branch string (#{branch}, #{branch.class})." ) \
		unless ( branch.is_a?( String ) and project.branches.has_key?( branch ) )
		
		project.load_config()
		p4 = project.scm()
		p4.connect() unless ( p4.connected? )
		
		files_synced = []
		args = []
		args << '-f' if force
		if ( '' == label )
			label = '#head'
		else
			label = "@#{label}"
		end
		build_path = project.branches[branch].build + label
		common_path = OS::Path.combine( project.branches[branch].common, "...#{label}" )
		
		# Get executables
		files_synced += p4.run_sync_with_block( args, build_path ) do
			|client_file, synced, errors|
			
			yield( client_file, synced, errors ) if ( block_given? )
		end
		
		# Get common data (recursive)
		files_synced += p4.run_sync_with_block( args, common_path ) do
			|client_file, synced, errors|
			
			yield( client_file, synced, errors ) if ( block_given? )
		end
		
		# Get platform data for enabled targets (recursive)
		project.branches[branch].targets.each do |name, target|
			next unless target.enabled
			
			path = ''
			target.in_env do |e|
				path = e.subst( "$(target)/...#{label}" )
			end
			
			p4.run_sync( args, path )
		end
		
		files_synced		
	end	
	
	#
	# Fetches the latest platform data for a project, based on a 
	# specific label or #head if a specific label is not given.
	#
	# By default the sync will fetch the HEAD revision.
	# By default the sync will not force overwrite files.
	# The block is invoked after each individual file synced.
	# An Array of all the files synced is returned.
	#
	def ProjectUtil::data_get_latest_platform_build_labelled( project, branch = nil, label = '', force = false, &block )
	raise ArgumentError.new( "Invalid project." ) \
		unless ( project.is_a?( Pipeline::Project ) )
		branch = project.default_branch if ( branch.nil? )
	raise ArgumentError.new( "Invalid branch string (#{branch}, #{branch.class})." ) \
		unless ( branch.is_a?( String ) and project.branches.has_key?( branch ) )
		
		project.load_config()
		p4 = project.scm()
		p4.connect() unless ( p4.connected? )
		
		files_synced = []
		args = []
		args << '-f' if force
		if ( '' == label )
			label = '#head'
		else
			label = "@#{label}"
		end

		# Get platform data for enabled targets (recursive)
		project.branches[branch].targets.each do |name, target|
			next unless target.enabled
			
			path = ''
			target.in_env do |e|
				path = e.subst( "$(target)/...#{label}" )
			end
			
			p4.run_sync( args, path )
		end
		
		files_synced		
	end


	#
	# Fetches the latest common and platform data for a project, based on a 
	# specific label or #head if a specific label is not given.
	#
	# By default the sync will fetch the HEAD revision.
	# By default the sync will not force overwrite files.
	# The block is invoked after each individual file synced.
	# An Array of all the files synced is returned.
	#
	def ProjectUtil::data_get_latest_build_labelled( project, branch = nil, label = '', force = false, &block )
		raise ArgumentError.new( "Invalid project." ) \
			unless ( project.is_a?( Pipeline::Project ) )
		branch = project.default_branch if ( branch.nil? )
		raise ArgumentError.new( "Invalid branch string (#{branch}, #{branch.class})." ) \
			unless ( branch.is_a?( String ) and project.branches.has_key?( branch ) )

		project.load_config()
		p4 = project.scm()
		p4.connect() unless ( p4.connected? )
		
		files_synced = []
		args = []
		args << '-f' if force
		if ( '' == label )
			label = '#head'
		else
			label = "@#{label}"
		end
		build_path = project.branches[branch].build + label
		common_path = OS::Path.combine( project.branches[branch].common, "...#{label}" )
		
		# Get executables
		files_synced += p4.run_sync_with_block( args, build_path ) do
			|client_file, synced, errors|

			yield( client_file, synced, errors ) if ( block_given? )
		end
		
		# Get common data (recursive)
		files_synced += p4.run_sync_with_block( args, common_path ) do
			|client_file, synced, errors|

			yield( client_file, synced, errors ) if ( block_given? )
		end
		
		# Get platform data for enabled targets (recursive)
		project.branches[branch].targets.each do |name, target|
			next unless target.enabled
			
			path = ''
			target.in_env do |e|
				path = e.subst( "$(target)/...#{label}" )
			end
			
			files_synced += p4.run_sync_with_block( args, path ) do
				|client_file, synced, errors|

				yield( client_file, synced, errors ) if ( block_given? )
			end
		end
		
		files_synced		
	end
	

	#
	# Fetches the latest shaders for a project.
	#
	# By default the sync will not force overwrite files.
	# The block is invoked after each individual file synced.
	# An array of all the files synced is returned.
	#
	def ProjectUtil::data_get_latest_build_shaders( project, branch = nil, force = false, &block )
		raise ArgumentError.new( "Invalid project." ) \
			unless ( project.is_a?( Pipeline::Project ) )
		branch = project.default_branch if ( branch.nil? )
		raise ArgumentError.new( "Invalid branch string (#{branch}, #{branch.class})." ) \
			unless ( branch.is_a?( String ) and project.branches.has_key?( branch ) )
	
		project.load_config()
		p4 = project.scm()
		p4.connect() unless ( p4.connected? )
		
		args = []
		args << '-f' if force
		
		# Get shaders
		files_synced = p4.run_sync_with_block( args, "#{project.branches[branch].shaders}/..." ) do
			|client_file, synced, errors|
			
			yield( client_file, synced, errors ) if ( block_given? )
		end
		
		files_synced
	end


	#
	# Fetches the latest assets/metadata folder for a project.
	#
	# By default the sync will not force overwrite files.
	# The block is invoked after each individual file synced.
	# An array of all the files synced is returned.
	#
	def ProjectUtil::data_get_latest_assets_metadata( project, branch = nil, force = false, &block )
		raise ArgumentError.new( "Invalid project." ) \
			unless ( project.is_a?( Pipeline::Project ) )
		branch = project.default_branch if ( branch.nil? )
		raise ArgumentError.new( "Invalid branch string (#{branch}, #{branch.class})." ) \
			unless ( branch.is_a?( String ) and project.branches.has_key?( branch ) )
	
		project.load_config()
		p4 = project.scm()
		p4.connect() unless ( p4.connected? )
		
		args = []
		args << '-f' if force
		
		# Get files
		files_synced = p4.run_sync_with_block( args, "#{project.root}/assets/metadata/..." ) do
			|client_file, synced, errors|
                        
			yield( client_file, synced, errors ) if ( block_given? )
		end
		
		files_synced		
	end


	#
	# Fetches the latest assets/maps/ParentTxds.xml folder for a project.
	#
	# By default the sync will not force overwrite files.
	# The block is invoked after each individual file synced.
	# An array of all the files synced is returned.
	#
	def ProjectUtil::data_get_latest_assets_maps_parenttxds( project, branch = nil, force = false, &block )
		raise ArgumentError.new( "Invalid project." ) \
			unless ( project.is_a?( Pipeline::Project ) )
		branch = project.default_branch if ( branch.nil? )
		raise ArgumentError.new( "Invalid branch string (#{branch}, #{branch.class})." ) \
			unless ( branch.is_a?( String ) and project.branches.has_key?( branch ) )
	
		project.load_config()
		p4 = project.scm()
		p4.connect() unless ( p4.connected? )
		
		args = []
		args << '-f' if force
		
		# Get files
		files_synced = p4.run_sync_with_block( args, "#{project.root}/assets/maps/parentTxds.xml" ) do
			|client_file, synced, errors|
                        
			yield( client_file, synced, errors ) if ( block_given? )
		end
		
		files_synced		
	end


	#
	# Fetches the latest assets/processed folder for a project.
	#
	# By default the sync will not force overwrite files.
	# The block is invoked after each individual file synced.
	# An array of all the files synced is returned.
	#
	def ProjectUtil::data_get_latest_assets_processed( project, branch = nil, force = false, &block )
		raise ArgumentError.new( "Invalid project." ) \
			unless ( project.is_a?( Pipeline::Project ) )
		branch = project.default_branch if ( branch.nil? )
		raise ArgumentError.new( "Invalid branch string (#{branch}, #{branch.class})." ) \
			unless ( branch.is_a?( String ) and project.branches.has_key?( branch ) )
	
		project.load_config()
		p4 = project.scm()
		p4.connect() unless ( p4.connected? )
		
		args = []
		args << '-f' if force
		
		# Get files
		files_synced = p4.run_sync_with_block( args, "#{project.root}/assets/processed/..." ) do
			|client_file, synced, errors|
                        
			yield( client_file, synced, errors ) if ( block_given? )
		end
		
		files_synced		
	end

	#
	# Fetches the latest assets folder for a project.
	#
	# By default the sync will not force overwrite files.
	# The block is invoked after each individual file synced.
	# An array of all the files synced is returned.
	#
	def ProjectUtil::data_get_latest_assets( project, branch = nil, force = false, &block )
		raise ArgumentError.new( "Invalid project." ) \
			unless ( project.is_a?( Pipeline::Project ) )
		branch = project.default_branch if ( branch.nil? )
		raise ArgumentError.new( "Invalid branch string (#{branch}, #{branch.class})." ) \
			unless ( branch.is_a?( String ) and project.branches.has_key?( branch ) )
	
		project.load_config()
		p4 = project.scm()
		p4.connect() unless ( p4.connected? )
		
		args = []
		args << '-f' if force
		
		# Get files
		files_synced = p4.run_sync_with_block( args, "#{project.root}/assets/..." ) do
			|client_file, synced, errors|
                        
			yield( client_file, synced, errors ) if ( block_given? )
		end
		
		files_synced		
	end

	#
	# Fetches the latest common build directory for a project.
	#
	# By default the sync will not force overwrite files.
	# The block is invoked after each individual file synced.
	# An array of all the files synced is returned.
	#
	def ProjectUtil::data_get_latest_build_common( project, branch = nil, force = false, &block )
		raise ArgumentError.new( "Invalid project." ) \
			unless ( project.is_a?( Pipeline::Project ) )
		branch = project.default_branch if ( branch.nil? )
		raise ArgumentError.new( "Invalid branch string (#{branch}, #{branch.class})." ) \
			unless ( branch.is_a?( String ) and project.branches.has_key?( branch ) )
	
		project.load_config()
		p4 = project.scm()
		p4.connect() unless ( p4.connected? )
		
		args = []
		args << '-f' if force
		
		# Get files
		files_synced = p4.run_sync_with_block( args, "#{project.branches[branch].common}/..." ) do
			|client_file, synced, errors|
                        
			yield( client_file, synced, errors ) if ( block_given? )
		end
		
		files_synced		
	end
        
	#
	# Fetches the latest independent build directory for a project.
	#
	# By default the sync will not force overwrite files.
	# The block is invoked after each individual file synced.
	# An array of all the files synced is returned.
	#
	def ProjectUtil::data_get_latest_build_independent( project, branch = nil, force = false, &block )
		raise ArgumentError.new( "Invalid project." ) \
			unless ( project.is_a?( Pipeline::Project ) )
		branch = project.default_branch if ( branch.nil? )
		raise ArgumentError.new( "Invalid branch string (#{branch}, #{branch.class})." ) \
			unless ( branch.is_a?( String ) and project.branches.has_key?( branch ) )
	
		project.load_config()
		p4 = project.scm()
		p4.connect() unless ( p4.connected? )
		
		args = []
		args << '-f' if force
		
		# Get files
		files_synced = p4.run_sync_with_block( args, "#{project.branches[branch].independent}/..." ) do
			|client_file, synced, errors|
			                        
			yield( client_file, synced, errors ) if ( block_given? )
		end
		
		files_synced		
	end        


	#
	# Return an Array of Perforce label strings for a given project that label
	# its stable builds.
	#
	def ProjectUtil::data_build_labels( project, branch = nil )
		raise ArgumentError.new( "Invalid project." ) \
			unless ( project.is_a?( Pipeline::Project ) )
		branch = project.default_branch if ( branch.nil? )
		raise ArgumentError.new( "Invalid branch string (#{branch}, #{branch.class})." ) \
			unless ( branch.is_a?( String ) and project.branches.has_key?( branch ) )
		
		project.load_config()
		p4 = project.scm()
		p4.connect() unless ( p4.connected? )
			
		labels = []
		labels_array = p4.run_labels( "#{project.branches[branch].build}/..." )
		labels_array.each do |label_hash|
			next unless label_hash.has_key?( 'label' )
			
			labels << label_hash['label']
		end 
		labels.sort()
	end

end # ProjectUtil module
end # Pipeline module

# data_get_latest.rb
