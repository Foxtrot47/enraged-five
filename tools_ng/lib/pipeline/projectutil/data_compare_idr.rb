#
# File:: data_compare_idr.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 18 November 2008
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/projectutil/data_compare'

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------
module Pipeline
module ProjectUtil
	
	# Simple real number regexp (enough for our purposes)
	REGEXP_FLOAT = /([-+]?[0-9]+?\.[0-9]+)/
	
	#
	# == Description
	# This function compares two .type files from drawables (typically called
	# 'entity.type').  These files are ascii and contain some information
	# about a Rage drawable, including shader info, bound info.  During the
	# comparison for GTA4 vs. E1 these files were coming back with some 
	# differences due to floating-point values being ever-so-slightly
	# different.
	#
	# This function takes those floating-point values into account.
	#
	# This function implements a very naive diffing algorithm to determine
	# differences - splitting each line by space, if its a float then it
	# will do some epsilon comparison otherwise it just attempts to match it.
	#
	# === Example Usage
	# DHM TODO
	#
	def ProjectUtil::data_compare_idr_type( file1, file2, epsilon = 0.003, &block )
		
		diff_status = data_compare_raw( file1, file2 )
		if ( 0 == diff_status )
			return ( DATA_COMPARE_EQUAL )
		end
		
		file1_data = []
		file2_data = []
		File::open( file1 ) do |fp1|
			file1_data = fp1.readlines( )
		end		
		File::open( file2 ) do |fp2|
			file2_data = fp2.readlines( )		
		end
		
		if ( file1_data.size != file2_data.size )
			yield( file1 ) if ( block_given? )
			return ( DATA_COMPARE_EQUIVALENT )
		end
		
		file1_data.each_with_index do |file1_line, index|
			file2_line = file2_data[index]
			
			# Tokenise each line.
			file1_toks = file1_line.split( ' ' )
			file2_toks = file2_line.split( ' ' )
			
			# Process tokens.
			file1_toks.each_with_index do |file1_tok, tok_index|
				file2_tok = file2_toks[tok_index]
				
				file1_md = file1_tok.match( REGEXP_FLOAT )
				file2_md = file2_tok.match( REGEXP_FLOAT )
				
				if ( not ( file1_md.nil? and file2_md.nil? ) )
					# Try float data
					float1 = file1_md[0].to_f
					float2 = file2_md[0].to_f
					if ( float1 <= ( float2 - epsilon ) or
						 float1 >= ( float2 + epsilon ) ) then
						 yield( file1 ) if ( block_given? )
						 return ( DATA_COMPARE_DIFFERENT )
					end
					
				else
					# Else compare as String data
					if ( file1_tok != file2_tok ) then
						yield( file1 ) if ( block_given? )
						return ( DATA_COMPARE_DIFFERENT )
					end				
				end
			end
		end
		
		return ( DATA_COMPARE_EQUIVALENT )
	end
	
end # ProjectUtil module
end # Pipeline module

if ( __FILE__ == $0 ) then
	include Pipeline
	
	ProjectUtil::data_compare_idr_type( 'X:\\compare_test\\ep\\nj_01\\nj_carparkmid\\entity.type', 'X:\\compare_test\\root\\nj_01\\nj_carparkmid\\entity.type' )
	
end

# data_compare_idr.rb
