#
# File:: data_hash.rb
# Description:: Data hashing functions.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 3 December 2008
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/os/start'

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------
module Pipeline
module ProjectUtil
	
	HASH_STRING_U32 = 0		# atStringHash algorithm
	HASH_STRING_S16 = 1		# atHash16 algorithm
	HASH_STRING_U16 = 2		# atHash16U algorithm
	
	#
	# == Description
	# Hashes a String using the RAGE string hashing functions (located in an 
	# external executable).
	#
	# Three different hashing algorithms are available:
	#   HASH_STRING_U32 - atStringHash
	#   HASH_STRING_S16 - atHash16
	#   HASH_STRING_U16 - atHash16U
	#
	# === Example Usage
	#
	#    ProjectUtil::hash( 'hello world' ) => 1045060183
	#    ProjectUtil::hash( 'hello world', HASH_STRING_U32 ) => 1045060183
	#
	def ProjectUtil::hash( str, algorithm = HASH_STRING_U32 )
		c = Pipeline::Config::instance()
		hash_exe = c.envsubst( OS::Path::combine( '$(toolsbin)', 'stringhash.exe' ) )
		cmdline = ""
		case algorithm
		when HASH_STRING_S16
			cmdline = "#{hash_exe} \"#{str.to_s}\" -short"
		when HASH_STRING_U16
			cmdline = "#{hash_exe} \"#{str.to_s}\" -ushort"
		else
			# Default of U32 - HASH_STRING_U32
			cmdline = "#{hash_exe} \"#{str.to_s}\""
		end
		
		status, stdout, stderr = OS::start( cmdline )
		if ( status.exited? and ( 0 == status.exitstatus ) ) then
			return ( stdout.strip.to_i )
		else
			return ( nil )
		end
	end
	
end # ProjectUtil module
end # Pipeline module

# data_hash.rb
