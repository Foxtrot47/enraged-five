#
# File:: data_compare_cutscene.rb
# Description:: Functions for comparing the cutscene data of two projects.
#
# Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
# Date:: 19 June 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/projectutil/data_remove'
require 'pipeline/projectutil/data_extract'

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
module Pipeline
	module ProjectUtil
		
		#
		# == Description
		# The cuts.img file is removed from the root project and replaced with
		# a dummy file.
		#
		# An optional code block can be given to process changed files.  
		#
		# == Example Usage
		# The code below calls the comparison, then prints the name of the new file
		#
		# ProjectUtil::data_compare_episodic_cutscene_data( g_RootBranch, g_EpBranch, g_TargetName, g_OutputDir ) do |filename, new_filename|
		#	puts filename
		# end #data_compare_episodic_cutscene_data
		#
		
		def ProjectUtil::data_compare_episodic_cuts_data( root_branch, ep_branch, target, output_dir, template_filename, &block )
			throw RuntimeError.new( "Invalid root project branch, #{root_proj_branch.class}." ) unless ( root_branch.is_a?( Pipeline::Branch ) )
			throw RuntimeError.new( "Invalid episodic project branch, #{ep_proj_branch.class}." ) unless ( ep_branch.is_a?( Pipeline::Branch ) )
			throw RuntimeError.new( "Invalid project target, #{target}." ) unless ( root_branch.targets.has_key?( target ) and ( ep_branch.targets.has_key?( target ) ) )
				
			c = Pipeline::Config::instance( )
			r = RageUtils.new( root_branch.project, root_branch.name )
			t = root_branch.targets[target]
			
			root_platform = ''
			ep_platform = ''
			root_branch.targets[target].in_env do |e|
				root_platform = e.subst( '$(target)' )
			end
			ep_branch.targets[target].in_env do |e|
				ep_platform = e.subst( '$(target)' )
			end
			
			# Define paths for files
			root_cuts = OS::Path::combine( root_branch.independent, 'anim', 'cuts.img' )
				
			# Replace cuts.img with a dummy file
			puts "\nReplacing cuts.img with a placeholder..."
			ProjectUtil::data_dummy_cuts( root_branch.project, root_branch, t, r, root_cuts, template_filename, output_dir ) do |new_filename|
				# Use the given block to process the resulting file
				yield( root_cuts, new_filename ) if ( block_given? )
			end
			
		end # data_compare_episodic_cuts_data
		
		
		#
		# == Description
		# The cutsprops.img file from the root project is compared with
		# the cutscene files from the episodic project and any files which are not
		# used in the episodic project are removed.
		#
		# An optional code block can be given to process changed files.  
		#
		# == Example Usage
		# The code below calls the comparison, then prints the name of the new file
		#
		# ProjectUtil::data_compare_episodic_cutsprops_data( g_RootBranch, g_EpBranch, g_TargetName, g_OutputDir ) do |filename, new_filename|
		#	puts filename
		# end #data_compare_episodic_cutsprops_data
		#
		
		def ProjectUtil::data_compare_episodic_cutsprops_data( root_branch, ep_branch1, ep_branch2, target, output_dir, &block )
			throw RuntimeError.new( "Invalid root project branch, #{root_branch.class}." ) unless ( root_branch.is_a?( Pipeline::Branch ) )
			throw RuntimeError.new( "Invalid episodic project branch, #{ep_branch1.class}." ) unless ( ep_branch1.is_a?( Pipeline::Branch ) )
			throw RuntimeError.new( "Invalid episodic project branch, #{ep_branch2.class}." ) unless ( ep_branch2.is_a?( Pipeline::Branch ) )
			throw RuntimeError.new( "Invalid project target, #{target}." ) unless ( root_branch.targets.has_key?( target ) and ( ep_branch1.targets.has_key?( target ) ) and ( ep_branch2.targets.has_key?( target ) ) )
			
			c = Pipeline::Config::instance( )
			r = RageUtils.new( root_branch.project, root_branch.name )
			t = root_branch.targets[target]
=begin			
			root_platform = ''
			ep_platform = ''
			root_branch.targets[target].in_env do |e|
				root_platform = e.subst( '$(target)' )
			end
			ep_branch.targets[target].in_env do |e|
				ep_platform = e.subst( '$(target)' )
			end
=end			
			# Define paths for files
			root_cutsprops = OS::Path::combine( root_branch.independent, 'anim', 'cutsprops.img' )
			ep_cuts1 = OS::Path::combine( ep_branch1.independent, 'anim', 'cuts.img' )
			ep_cutsprops1 = OS::Path::combine( ep_branch1.independent, 'anim', 'cutsprops.img' )
			ep_cuts2 = OS::Path::combine( ep_branch2.independent, 'anim', 'cuts.img' )
			ep_cutsprops2 = OS::Path::combine( ep_branch2.independent, 'anim', 'cutsprops.img' )
			
			# Remove unused files in cutsprops.img
			puts "\nRemoving unused files in cutsprops.img..."
			ProjectUtil::data_cull_cutsprops( root_branch.project, root_branch, target, r, root_cutsprops, ep_cuts1, ep_cutsprops1, ep_cuts2, ep_cutsprops2, output_dir ) do |new_filename|
				# Use the given block to process the resulting file
				yield( root_cutsprops, new_filename ) if ( block_given? )
			end
			
		end # data_compare_episodic_cutsprops_data
		
	end # ProjectUtil module
end # Pipeline module

# data_compare_cutscene.rb
