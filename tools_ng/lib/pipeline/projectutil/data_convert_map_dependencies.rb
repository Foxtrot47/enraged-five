#
# File:: %RS_TOOLSLIB%/pipeline/projectutil/data_convert_map_dependencies.rb
# Description:: Functions to determine map dependencies.
#
# Author:: Jonny Rivers <jonathan.rivers@rockstarnorth.com>
# Date:: 4 July 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/project'
require 'pipeline/content/content_core'
require 'pipeline/content/treecore'

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

module Pipeline
module ProjectUtil
	
	# Given a mapzip node, append the mapzip filename of any inter-container LOD dependencies to dependency_filenames
	def ProjectUtil::get_lod_dependency_files( mapzip_content, dependency_filenames )
		
		first_map_content = ProjectUtil::get_first_map_content( mapzip_content )
			
		if( first_map_content != nil ) then
			if( (first_map_content.maptype == :container) or (first_map_content.maptype == :container_props) ) then
				ProjectUtil::all_lod_parent_dependency_files( first_map_content, dependency_filenames )
			elsif (first_map_content.maptype == :container_lod) then
				ProjectUtil::all_lod_child_sibling_dependency_files( first_map_content, dependency_filenames )
			end
		end
	end
	
	# Given a mapzip node, append the mapzip node of any inter-container LOD dependencies to dependency_nodes
	def ProjectUtil::get_lod_dependency_nodes( mapzip_content, dependency_nodes )
		
		first_map_content = ProjectUtil::get_first_map_content( mapzip_content )
			
		if( first_map_content != nil ) then
			if( (first_map_content.maptype == :container) or (first_map_content.maptype == :container_props) ) then
				ProjectUtil::all_lod_parent_dependency_nodes( first_map_content, dependency_nodes )
			elsif (first_map_content.maptype == :container_lod) then
				ProjectUtil::all_lod_child_sibling_dependency_nodes( first_map_content, dependency_nodes )
			end
		end
	end
	
	# Get the first map node that is an input to mapzip_content of type (:container ; :container_props ; :container_lod), or nil
	def ProjectUtil::get_first_map_content( mapzip_content )
		mapzip_content.inputs.each do |map_content|
			if( map_content.is_a?( Content::Map ) and ((map_content.maptype == :container) or (map_content.maptype == :container_props) or (map_content.maptype == :container_lod))) then
				return map_content
			end
		end
			
		return nil
	end
	
	# Given a map node, append the mapzip filename of any inter-container parent LOD dependencies to dependency_filenames
	def ProjectUtil::all_lod_parent_dependency_files( map_content, dependency_filenames )
		parent_map_content = map_content.parent
			
		parent_map_content.children.each do |sibling_map_content|
			next unless sibling_map_content.is_a?( Content::Map )
				
			if( sibling_map_content.maptype == :container_lod ) then
				sibling_map_content.outputs.each do |sibling_mapzip_content|
					next unless sibling_mapzip_content.is_a?(Content::MapZip)
					
					dependency_filenames << sibling_mapzip_content.filename
				end
			end
		end
	end
	
	# Given a map node, append the mapzip filename of any inter-container child/sibling LOD dependencies to dependency_filenames
	def ProjectUtil::all_lod_child_sibling_dependency_files( map_content, dependency_filenames )
		parent_map_content = map_content.parent
			
		parent_map_content.children.each do |sibling_map_content|
			next unless sibling_map_content.is_a?( Content::Map )
				
			#we also scoop up any siblings as some regions have multiple lod containers
			if( (sibling_map_content.maptype == :container) or (sibling_map_content.maptype == :container_props) or (sibling_map_content.maptype == :container_lod) ) then
				sibling_map_content.outputs.each do |sibling_mapzip_content|
					next unless sibling_mapzip_content.is_a?(Content::MapZip)
					
					dependency_filenames << sibling_mapzip_content.filename
				end
			end
		end
	end
	
	# Given a map node, append the mapzip node of any inter-container parent LOD dependencies to dependency_nodes
	def ProjectUtil::all_lod_parent_dependency_nodes( map_content, dependency_nodes )
		parent_map_content = map_content.parent
			
		parent_map_content.children.each do |sibling_map_content|
			next unless sibling_map_content.is_a?( Content::Map )
				
			if( sibling_map_content.maptype == :container_lod ) then
				sibling_map_content.outputs.each do |sibling_mapzip_content|
					next unless sibling_mapzip_content.is_a?(Content::MapZip)
					
					dependency_nodes << sibling_mapzip_content
				end
			end
		end
	end
	
	# Given a map node, append the mapzip node of any inter-container child/sibling LOD dependencies to dependency_nodes
	def ProjectUtil::all_lod_child_sibling_dependency_nodes( map_content, dependency_nodes )
		parent_map_content = map_content.parent
			
		parent_map_content.children.each do |sibling_map_content|
			next unless sibling_map_content.is_a?( Content::Map )
				
			#we also scoop up any siblings as some regions have multiple lod containers
			if( (sibling_map_content.maptype == :container) or (sibling_map_content.maptype == :container_props) or (sibling_map_content.maptype == :container_lod) ) then
				sibling_map_content.outputs.each do |sibling_mapzip_content|
					next unless sibling_mapzip_content.is_a?(Content::MapZip)
					
					dependency_nodes << sibling_mapzip_content
				end
			end
		end
	end
	
end # ProjectUtil module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/projectutil/data_convert_map_dependencies.rb
