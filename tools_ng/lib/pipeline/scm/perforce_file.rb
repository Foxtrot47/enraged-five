#
# File:: perforce_file.rb
# 
# Pipeline::Overloads file open so that any files that are opened for write are checked out of perforce if they've
#			already been added
#
# Author:: Greg Smith <greg@rockstarnorth.com>
# Date:: 13 June 2010
#
#

class File

	alias :old_new :initialize
	
	def initialize(fd, mode_string="r", args = nil)	
		
		if fd and Pipeline::OS::Path.get_extension(fd).downcase != "log" and File.exists?(fd) and (mode_string.index("w") != nil) then
		
			fd = File.expand_path(fd)
			scm = Pipeline::Config::instance().scm
			ragescm = Pipeline::Config::instance().ragescm
				
			FileUtils.cd(Pipeline::OS::Path.get_directory(fd)) {
			
					if ( scm.exists?( fd ) ) then
						scm.run_edit(fd)
						scm.run_reopen( '-t', "+w", fd ) if (mode_string.include?("w"))
					elsif ( ragescm.exists?( fd ) ) then
						ragescm.run_edit(fd)
						ragescm.run_reopen( '-t', "+w", fd ) if (mode_string.include?("w"))
					end										
			}
		end
	
		old_new(fd,mode_string,args)
	end
end