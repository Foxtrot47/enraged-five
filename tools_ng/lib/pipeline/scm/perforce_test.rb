#p4 test - DW 28/04/11

require "pipeline/scm/perforce"
require 'win32/registry'
require 'p4'

#-------------------------------------------------------------------------------
def p4_works()
	# This works
	FileUtils.cd("c:\\") { p4 = MyPerforce.new(); p4.connect() }
end

#-------------------------------------------------------------------------------
def p4_fails()
	# This doesn't work
	p4 = MyPerforce.new()
	FileUtils.cd("c:\\") { p4.connect() }
end

class MyPerforce < P4
	# Class constructor.
		def initialize( )
			super()
			self.exception_level = P4::RAISE_ERRORS
		end

		#
		# We override the base class' connect method to handle our Perforce
		# username password.  If the user has already connected (e.g. with
		# P4V, P4Win) they will have a ticket on the machine with a password
		# token.  We can use this password token if its available, otherwise
		# we have to popup a password dialog (erk).
		#
		def connect( )
			begin
				# Connect to our server
				result = super( ) unless ( self.connected?() )

				# Determine if we have a ticket
				begin
					login_result = self.run_login( '-s' ).shift( )
				rescue P4Exception => ex

					begin
						c = Pipeline::Config::instance()
						command = c.envsubst( "#{DIALOG} #{ARGUMENTS} #{self.port} #{self.user}" )
						if ( system( command ) ) then
							# Our custom dialog does the login for us; so we just check the status again.
							self.run_login( '-s' ).shift( )
						else
							Perforce::log().error( "#{self.port} User #{self.user} login failed.  Password incorrect?" )
							throw P4Exception.new( 'Login failed.' )
						end
					rescue P4Exception => ex
						result = false
					end
				end
				result

			rescue P4Exception => ex
				ex.backtrace.each do |err| Perforce::log().error( err ) end
			end
		end
end

#-------------------------------------------------------------------------------
def p4_test(dir)
	begin
		puts "\n\n\n**** #{dir} ****"

		p4 = SCM::Perforce.new()

		puts "\n\t=== p4 connect ==="
		ret = p4.connect()
		puts "\t\tconnect #{ret}"

		puts "\n\t=== p4 connected? ==="
		ret = p4.connected?
		puts "\t\tconnected #{ret}"

		puts "\n\t=== p4 login -s ==="
		ret = p4.run_login('-s')
		puts "\t\tlogin -s #{ret}"

		puts "\n\t=== p4 inspect ==="
		puts "#\t\t#{p4.inspect}"

		puts "\n\t=== p4 login -s (commandline) ==="
		ret = `p4 login -s`
		ret.each { |r| puts "\t\t#{r}" }

		puts "\n\t=== p4 set (commandline) ==="
		ret = `p4 set`
		ret.each { |r| puts "\t\t#{r}" }	

		puts "\n\t=== p4 info ==="
		ret = p4.run_info
		ret.each { |r| puts "\t\t#{r}" }

		puts "\n\t=== p4 info (commandline) ==="
		ret = `p4 info`
		ret.each { |r| puts "\t\t#{r}" }

		puts "\n\t=== Registry ==="
		::Win32::Registry::HKEY_CURRENT_USER.open( 'Software\\Perforce\\environment' ) do |key|
			key.each_value do |subkey, type, value|
				puts "\t\t#{subkey} #{type} #{value}"			
			end
		end
	rescue Exception => ex
		puts( ex, "Unhandled exception during __FILE__ #{ex.message}" )
	end
end

if __FILE__ == $0

	puts "\n\t This fails on als machine"
	p4_fails()
	
	puts "\n\t This works on als machine"
	p4_works()

	worked = false
	
	dir = "#{ENV["RS_CODEBRANCH"]}/game"
	if (File.exist?(dir))
		FileUtils.cd(dir) do
			p4_test(dir)	
			worked = true
		end
	end
	
	dir = "#{ENV["RS_CODEBRANCH"]}/rage"
	if (File.exist?(dir))
		FileUtils.cd(dir) do
			p4_test(dir)	
			worked = true
		end	
	end
	
	p4_test("current dir") if not worked
	
end

