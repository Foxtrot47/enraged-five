#
# File:: perforce_helper.rb
# Description:: Perforce helper functions.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 1 December 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/project'
require 'pipeline/scm/perforce'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

module Pipeline
module SCM
		
	#
	# == Description
	# Monkey-patch some utility functions into our SCM::Perforce class.
	#
	class Perforce

		#
		# Return a Hash describing the user as known by the Perforce server.
		# Will return nil if the user is invalid.
		#
		# The parameter may be a String username (e.g. 'david.muir') or an
		# Integer changelist number.
		#
		def get_user( username_or_changelist )
			if ( username_or_changelist.is_a?( String ) ) then
				user = self.run_user( '-o', username_or_changelist ).shift
				return ( user )
			elsif ( username_or_changelist.is_a?( Integer ) ) then
				changelist = self.run_describe( username_or_changelist ).shift
				return ( get_user( changelist['user'] ) ) if ( changelist.has_key?( 'user' ) )
			end	
			nil
		end

		#
		# Return a String of the user's email address as known by the Perforce
		# server.  Will return nil if the user is invalid.
		#
		# The parameter may be a String username (e.g. 'david.muir') or an
		# Integer changelist number.
		#
		def get_user_email( username_or_changelist )

			user = self.get_user( username_or_changelist )
			return ( user['Email'] ) if ( user.is_a?( Hash ) and user.has_key?( 'Email' ) )
			nil
		end

		#
		# Return a String of the user's fullname as known by the Perforce
		# server.  Will return nil if the user is invalid.
		#
		# The parameter may be a String username (e.g. 'david.muir') or an
		# Integer changelist number.
		#
		def get_user_fullname( username_or_changelist )

			user = self.get_user( username_or_changelist )
			return ( user['FullName'] ) if ( user.is_a?( Hash ) and user.has_key?( 'FullName' ) )
			nil
		end
	end
		
end # SCM module
end # Pipeline module

# perforce_helper.rb
