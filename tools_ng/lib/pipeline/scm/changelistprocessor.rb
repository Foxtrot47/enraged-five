#
# File:: changelistprocessor.rb
# Description:: ChangeListProcessor base class
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 6 August 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/scm/perforce'

module Pipeline
module SCM

	#
	# == Description
	# The ChangeListProcessor class fetches information about a changelist and
	# can then analyse the changelist to do some custom action.
	#
	# == Example Usage
	#
	#    p4 = SCM::Perforce( ... )
	#    ...
	#    p4.connect()
	#
	#    ChangeListProcessor.process( p4, 3 ) do |cl_hash|
	#       cl_hash.each_pair do |k,v|
	#         puts "#{k} => #{v}"
	#       end
	#    end	
	#
	class ChangeListProcessor
		
		#
		# Process a changelist given a Perforce instanceand changelist number.
		# The block will be called with a single parameter being the result 
		# Array of Hash objects.
		#
		def ChangeListProcessor.process( p4, changelist, &block )	
			p4.connect() unless p4.connected?
		
			# Fetch changelist hash from Perforce
			changelist = p4.run_describe( changelist.to_s )
		
			# Yield
			yield changelist if ( block_given? )
		end
	end

end # SCM module
end # Pipeline module

# changelistprocessor.rb
