#
# File:: pipeline/scm/GUI/p4_history_dialog.rb
# Description:: 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 18 November 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'wx'

#----------------------------------------------------------------------------
# Monkey-Patching
#----------------------------------------------------------------------------

# Monkey patch the P4::Revision class to allow public access to the 
# attributes member.  This class varies from our documentation quite a bit.
# Ah well.  Thank the Ruby stars for Monkey-Patching goodness.
class P4
	class Revision
		attr_reader :attributes
	end
end

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module SCM
module GUI
	
	#
	# == Description
	# Perforce file revision history dialog.
	#
	class HistoryDialog < Wx::Dialog
		attr_reader :depotfile
		attr_reader :revisions
		attr_reader :cbhandler
		
		# Constructor
		def initialize( depotfile, cbhandler )
			throw ArgumentError.new( "Invalid P4::DepotFile object (#{depotfile.class})." ) \
				unless ( depotfile.is_a?( P4::DepotFile ) )	
			throw ArgumentError.new( "Invalid callback handler Proc object (#{cbhandler.class})." ) \
				unless ( cbhandler.is_a?( Proc ) )
			
			@index = -1
			@revisions = []
			@depotfile = depotfile
			@cbhandler = cbhandler
			super( nil, Wx::ID_ANY, TITLE, Wx::DEFAULT_POSITION,
				Wx::Size::new( WIDTH, HEIGHT ),
				Wx::DEFAULT_DIALOG_STYLE | Wx::RESIZE_BORDER | Wx::CLIP_CHILDREN )
			
			#--------------------------------------------------------------------
			# WIDGETS
			#--------------------------------------------------------------------
			@lbHistory = Wx::ListCtrl::new( self, Wx::ID_ANY, Wx::DEFAULT_POSITION,
				Wx::DEFAULT_SIZE, Wx::LC_REPORT | Wx::LC_SINGLE_SEL )
			@btnSelect = Wx::Button::new( self, Wx::ID_ANY, '&Select' )
			@btnCancel = Wx::Button::new( self, Wx::ID_ANY, '&Cancel' )
			
			#--------------------------------------------------------------------
			# LAYOUT
			#--------------------------------------------------------------------
			vert_layout = Wx::BoxSizer::new( Wx::VERTICAL )
			horz_layout = Wx::BoxSizer::new( Wx::HORIZONTAL )
			horz_layout.add( @btnSelect, 0, Wx::GROW | Wx::ALL, 2 )
			horz_layout.add( @btnCancel, 0, Wx::GROW | Wx::ALL, 2 )
			
			vert_layout.add( @lbHistory, 2, Wx::GROW | Wx::ALL, 2 )
			vert_layout.add( horz_layout, 0, Wx::GROW | Wx::ALL, 2 )
			set_sizer( vert_layout )
			
			setup_view( )
			
			#--------------------------------------------------------------------
			# EVENT HANDLERS
			#--------------------------------------------------------------------
			
			# ListCtrl Selection Change Events
			evt_list_item_selected( @lbHistory.get_id() ) do |event|
				@index = event.index				
				@btnSelect.enable() if ( @lbHistory.get_selected_item_count() > 0 )
				@btnSelect.disable() if ( 0 == @lbHistory.get_selected_item_count() )
			end
			evt_list_item_deselected( @lbHistory.get_id() ) do |event|
				@index = -1
				@btnSelect.enable() if ( @lbHistory.get_selected_item_count() > 0 )
				@btnSelect.disable() if ( 0 == @lbHistory.get_selected_item_count() )
			end
			
			# 'Select' Button Click Event
			evt_button( @btnSelect.get_id() ) do |event|
				# Get selected revision number from the ListCtrl.				
				throw RuntimeError.new( "Internal error: no revision selected." ) \
					if ( -1 == @index )
				if ( @cbhandler.call( @revisions[@index] ) )
					exit( 0 )
				end
			end
			
			# 'Cancel' Button Click Event
			evt_button( @btnCancel.get_id() ) do |event|
				exit( 1 )
			end
		end
		
		#--------------------------------------------------------------------
		# Private
		#--------------------------------------------------------------------
	private
		TITLE = 'Perforce File History'
		WIDTH = 800
		HEIGHT = 480
		
		def setup_view( )
			@lbHistory.insert_column( 0, 'Revision', Wx::LIST_FORMAT_LEFT, 50 )
			@lbHistory.insert_column( 1, 'Filename', Wx::LIST_FORMAT_LEFT, 120 )
			@lbHistory.insert_column( 2, 'Changelist' )
			@lbHistory.insert_column( 3, 'Date' )
			@lbHistory.insert_column( 4, 'User' )
			@lbHistory.insert_column( 5, 'Action', Wx::LIST_FORMAT_LEFT, 50 )
			@lbHistory.insert_column( 6, 'Description', Wx::LIST_FORMAT_LEFT, 300 )		
			
			@depotfile.each_revision do |rev|
				#puts "REV: #{rev.class} #{rev.inspect}"
				
				index = @revisions.size
				@lbHistory.insert_item( index, '' )
				@lbHistory.set_item( index, 0, rev.attributes['rev'].to_s )
				@lbHistory.set_item( index, 1, rev.depot_file.to_s )
				@lbHistory.set_item( index, 2, rev.attributes['change'].to_s )
				@lbHistory.set_item( index, 3, rev.attributes['time'].to_s )
				@lbHistory.set_item( index, 4, rev.attributes['user'].to_s )
				@lbHistory.set_item( index, 5, rev.attributes['action'].to_s )
				@lbHistory.set_item( index, 6, rev.attributes['desc'].to_s )
				
				@revisions << rev
			end
			
			@btnSelect.enable() if ( @lbHistory.get_selected_item_count() > 0 )
			@btnSelect.disable() if ( 0 == @lbHistory.get_selected_item_count() )
		end		
	end
	
	#
	# == Description
	# Simple Wx::App that displays the History dialog above and executes the
	# user-specified block for the revision that was selected.
	#
	class HistoryDialogApp < Wx::App
		attr_reader :depotfile
		attr_reader :cbhandler
		
		# Constructor.
		def initialize( depotfile, cbhandler )
			throw ArgumentError.new( "Invalid callback handler Proc object (#{cbhandler.class})." ) \
				unless ( cbhandler.is_a?( Proc ) )
			super()
			@depotfile = depotfile
			@cbhandler = cbhandler
		end
		
		# Application initialisation callback.
		def on_init( )
			dialog = HistoryDialog::new( @depotfile, @cbhandler )
			dialog.show( )
		end
	end
	
end # GUI module
end # SCM module
end # Pipeline module

# pipeline/scm/GUI/p4_history_dialog.rb
