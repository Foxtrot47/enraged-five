#
# File:: constants.rb
# Description:: Additional math constants.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 11 December 2008
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
# None

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Math

	# Square-root of 2.
	M_SQRT2 = ( 1.4142135623730950488016887242097 )

	# Square-root of 1/2.
	M_SQRT1_2 = ( 0.70710678118654752440084436210485 )

	# Square-root of 3.
	M_SQRT3 = ( 1.7320508075688772935274463415059 )
	
end # Math module
end # Pipeline module

# constants.rb
