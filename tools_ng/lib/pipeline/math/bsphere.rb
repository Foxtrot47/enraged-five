#
# File:: bsphere.rb
# Description:: Bounding Sphere Class
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 28 February 2008
# 

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/math/vector3'
require 'pipeline/math/bsphere'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Math

	#
	# == Description
	# Class representing a Bounding Sphere.
	#
	class BoundingSphere
	
		attr_reader :centre	# Sphere centre point (Vector3 object)
		attr_reader :radius	# Sphere radius
		
		# Class constructor taking sphere centre and radius to define the
		# sphere.
		def initialize( centre, radius )
			raise ArgumentError.new( "Sphere centre must be a Vector3 object. ") \
				unless ( centre.is_a?( Vector3 ) )
					
			@centre = centre
			@radius = radius
		end
		
		#
		# Return a bounding sphere from a bounding box.  This is used by the SceneXml
		# IDE serialiser.
		#
		def BoundingSphere::from_bbox( bbox )
			throw ArgumentError.new( "Invalid BoundingBox3 object (#{bbox.class})." ) \
				unless ( bbox.is_a?( Pipeline::Math::BoundingBox3 ) )
			
			distx = ( bbox.max.x - bbox.min.x ) / 2.0
			disty = ( bbox.max.y - bbox.min.y ) / 2.0
			distz = ( bbox.max.z - bbox.min.z ) / 2.0
			dista = ::Math::sqrt( ( distx * distx ) + ( disty * disty ) )
			
			posx = bbox.min.x + distx
			posy = bbox.min.y + disty
			posz = bbox.min.z + distz
			
			pos = Vector3.new( posx, posy, posz )
			radius = ::Math::sqrt( ( dista * dista ) + ( distz * distz ) )
			
			BoundingSphere.new( pos, radius )
		end
	end

end # Math module
end # Pipeline module

# bsphere.rb
