#
# angles.rb
# Useful angles functions / constants etc.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# File:: 10 March 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'mathn'

module Pipeline
module Math

	#-------------------------------------------------------------------------
	# Constants
	#-------------------------------------------------------------------------

	# Degrees to Radians angle conversion constant.
	DEG_TO_RAD = (::Math::PI / 180.0)
	# Radians to Degrees angle conversion constant.
	RAD_TO_DEG = (180.0 / ::Math::PI)

end # Math module
end # Pipeline module

# End of angles.rb
