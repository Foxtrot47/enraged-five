#
# defaultidefile.rb
# Game Default IDE File Loader Class
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 4 March 2008
# 

require 'pipeline/game/idefile'
require 'pipeline/game/defaultidedefs'
require 'pipeline/os/path'

module Pipeline
module Game

	#
	# == Description
	#
	# Game Default IDE file loader class.
	#
	class DefaultIDEFile < IDEFileBase
	
		attr_reader :weapon_definitions			# Array of WeaponIDEDef objects
		
		def initialize( path )
		
			super( path, nil )
			@weapon_definitions = Array.new
			state = STATE_NONE
			
			File.open( path ) do |file|
			
				begin
					file.each do |linex|
	
						line = linex.strip
						# Skip empty lines
						next if 0 == line.size
	
						# Skip comment lines
						next if line.starts_with( '#' )
	
						# See if we need to move to next state
						changed_state = false
						TAGS.each do |tag, id|
							state = id if tag == line
							changed_state = true if tag == line
						end
						next if changed_state
						
						case state
						when STATE_WEAP
							@weapon_definitions << WeaponIDEDef.parse( line, self )
						
						when STATE_HIER
						
						when STATE_AMAT
                        
                        when STATE_2DFX
						
						end
					end
				ensure
					file.close()
				end
			end
		end
		
		#
		# == Description
		#
		# Return the total number of definitions that have been parsed in this
		# IDE file.
		#
		def total_definitions
		
			( 	@weapon_definitions.size )
		end
		
		#---------------------------------------------------------------------
		# Private 
		#---------------------------------------------------------------------
	
	private
	
		STATE_NONE = -1
		STATE_OBJ = 0
		STATE_WEAP = 1
		STATE_HIER = 2
		STATE_AMAT = 3
		STATE_2DFX = 4
	
		TAGS = { 	'objs' 	=> STATE_OBJ, 	# Objects
					'weap' 	=> STATE_WEAP, 	# Weapon objects
                    'hier'  => STATE_HIER,
                    'amat'  => STATE_AMAT,
                    '2dfx'  => STATE_2DFX, 
					'end' 	=> STATE_NONE }	# Tag end token
	end

end # Game module
end # Pipeline module

# End of mapidefile.rb
