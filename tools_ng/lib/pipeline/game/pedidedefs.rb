#
# vehicleidedefs.rb
# Game IDE Vehicle Object Definition Classes
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 3 March 2008
# 

require 'pipeline/game/idefile'

module Pipeline
module Game

	#
	# == Description
	#
	#
	class PedIDEPedDef < IDEDefBase
	
		attr_reader :props
		attr_reader :type
		attr_reader :anim_group
		attr_reader :gesture_group
		attr_reader :gesture_phone_group
		attr_reader :facial_group
		attr_reader :viseme_group
		attr_reader :flags
		attr_reader :anim_file
		attr_reader :blendshape_file
		attr_reader :radio1
		attr_reader :radio2
		attr_reader :audio_type
		attr_reader :first_voice
		attr_reader :last_voice
		
		def PedIDEPedDef.parse( line, idefile )
		
			parts = line.split( ',' )
			parts.each do |part|
				part.strip!()
			end

			name = parts[0]
			props = parts[1]
			anim_group = parts[2]
			gesture_group = parts[3]
			gesture_phone_group = parts[4]
			facial_group = parts[5]
			viseme_group = parts[6]
			flags = parts[7].to_i
			anim_file = parts[8]
			blendshape_file = parts[9]
			radio1 = parts[10].to_i
			radio2 = parts[11].to_i
			audio_type= parts[12]
			first_voice = parts[13]
			last_voice = parts[14]
			
			PedIDEPedDef.new( name, idefile, props, anim_group, gesture_group,
				gesture_phone_group, facial_group, viseme_group, flags,
				anim_file, blendshape_file, radio1, radio2, audio_type,
				first_voice, last_voice )		
		end
		
		def initialize( name, idefile, props, anim_group, gesture_group,
				gesture_phone_group, facial_group, viseme_group, flags,
				anim_file, blendshape_file, radio1, radio2, audio_type,
				first_voice, last_voice )
		
			super( name, idefile )
			@props = props
			@anim_group = anim_group
			@gesture_group = gesture_group
			@gesture_phone_group = gesture_phone_group
			@facial_group = facial_group
			@viseme_group = viseme_group
			@flags = flags
			@anim_file = anim_file
			@blendshape_file = blendshape_file
			@radio1 = radio1
			@radoi2 = radio2
			@audio_type = audio_type
			@first_voice = first_voice
			@last_voice = last_voice
		end
	end

	#
	# == Description
	#
	#
	class PedIDEAudioGroupDef < IDEDefBase
	
		attr_reader :audio_group
		attr_reader :head_index
		
		def PedIDEAudioGroupDef.parse( line, idefile )
		
			parts = line.split( ',' )
			parts.each do |part|
				part.strip!()
			end

			name = parts[0]
			audio_group = parts[1]
			head_index = parts[2].to_i
			
			PedIDEAudioGroupDef.new( name, idefile, audio_group, head_index )
		end
		
		def initialize( name, idefile, audio_group, head_index )
		
			super( name, idefile )
			@audio_group = audio_group
			@head_index = head_index
		end
	end
	
end # Game module
end # Pipeline module

# End of pedidedefs.rb
