#
# File:: iplfile.rb
# Description:: Game IPL Loader base classes
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 28 February 2008
# 

module Pipeline
module Game

	#
	# == Description
	# Game IPL file loader class.
	#
	class IPLFileBase
	
		attr_reader :filename
	
		def initialize( path )
		
			@filename = path
		end
	end

	#
	# == Description
	#
	#
	class IPLInstBase
	
		attr_reader :name
		
		def initialize( name )
			@name = name
		end
	end
	
end # Game module
end # Pipeline module

# End of iplfile.rb
