#
# File:: mapipldefs.rb
# Description:: Game IPL Instance Objects Classes
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 28 February 2008
# 

require 'pipeline/game/iplfile'
require 'pipeline/math/vector'
require 'pipeline/math/quat'

module Pipeline
module Game

	#
	# == Description
	# Object instance class.
	#
	class MapIPLInst < IPLInstBase
	
		attr_reader :flags
		attr_reader :position	# Position vector
		attr_reader :rotation	# Rotation quaternion
		attr_reader :lod_index
		attr_reader :block_index
		attr_reader :lod_distance
		attr_reader :definition	# IDEDef this is an instance of
		
		def MapIPLInst.parse( line )
			
			parts = line.split( ',' )
			parts.each do |part|
				part.strip!()
			end
			
			name = parts[0]
			flags = parts[1].to_i
			pos = Math::Vector.new # parts[2..4]
			rot = Math::Quat.new # parts[5..8]
			lod_index = parts[9].to_i
			block_index = parts[10].to_i
			lod_distance = parts[11].to_f
			
			MapIPLInst.new( name, flags, pos, rot, lod_index, block_index, lod_distance )
		end
		
		def initialize( name, flags, pos, rot, lod_index, block_index, lod_distance )
		
			super( name )
			@flags = flags
			@position = pos
			@rotation = rot
			@lod_index = lod_index
			@block_index = block_index
			@lod_distance = lod_distance
		end
	end
	
end # Game module
end # Pipeline module

# End of mapipldefs.rb
