#
# mapidefile.rb
# Game Map IDE File Loader Class
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 28 February 2008
# 

require 'pipeline/game/idefile'
require 'pipeline/game/mapidedefs'
require 'pipeline/os/path'
require 'tempfile'

module Pipeline
module Game

	#
	# == Description
	#
	# Game Map IDE file loader class.
	#
	class MapIDEFile < IDEFileBase
	
		attr_reader :object_definitions			# Array of IDEObjDef objects
		attr_reader :time_object_definitions	# Array of IDETimeObjDef objects
		attr_reader :anim_definitions			# Array of IDEAnimObjDef objects
		attr_reader :time_anim_definitions		# Array of IDETimeAnimObjDef objects
		attr_reader :milo_object_definitions	# Array of IDEMiloObjDef objects
		attr_reader :fx2d_definitions			# Array of MapIDE2DFXDef objects
		attr_reader :parent_txd_definitions		# Array of IDEParentTxdDef objects
		
		def initialize( path )
		
			super( path, OS::Path.replace_ext( path, 'img' ) )
			@object_definitions = Array.new
			@time_object_definitions = Array.new
			@anim_definitions = Array.new
			@time_anim_definitions = Array.new
			@milo_object_definitions = Array.new
			@fx2d_definitions = Array.new
			@parent_txd_definitions = Array.new
			state = STATE_NONE
			
			File.open( path ) do |file|
			
				begin
					file.each do |linex|
	
						line = linex.strip
						# Skip empty lines
						next if 0 == line.size
	
						# Skip comment lines
						next if '#' == line[0]
	
						# See if we need to move to next state
						changed_state = false
						TAGS.each do |tag, id|
							state = id if tag == line
							changed_state = true if tag == line
						end
						next if changed_state
						
						case state
						when STATE_OBJ
							@object_definitions << MapIDEObjDef.parse( line, self )
						
						when STATE_TOBJ
							@time_object_definitions << MapIDETimeObjDef.parse( line, self )
						
						when STATE_TREE
						when STATE_PATH
						when STATE_ANIM
							@anim_definitions << MapIDEAnimObjDef.parse( line, self )
						
						when STATE_TANM
							@time_anim_definitions << MapIDETimeAnimObjDef.parse( line, self )
						
						when STATE_MILO
							@milo_object_definitions << MapIDEMiloObjDef.parse( line, self )
						
						when STATE_2DFX
							@fx2d_definitions << MapIDE2DFXDef.parse( line, self )
						
						when STATE_AMAT
						when STATE_TXDP
							@parent_txd_definitions << IDEParentTxdDef.parse( line, self )
							
						end
					end
				ensure
					file.close()
				end
			end
		end
		
		#
		# == Description
		#
		# Return the total number of definitions that have been parsed in this
		# IDE file.
		#
		def total_definitions
		
			( 	@object_definitions.size + 
				@time_object_definitions.size + 
				@milo_object_definitions.size +
				@parent_txd_definitions.size )
		end
		
		#---------------------------------------------------------------------
		# Private 
		#---------------------------------------------------------------------
	
	private
	
		STATE_NONE = -1
		STATE_OBJ  = 0
		STATE_TOBJ = 1
		STATE_TREE = 2
		STATE_PATH = 3
		STATE_ANIM = 4
		STATE_TANM = 5
		STATE_MILO = 6
		STATE_2DFX = 7
		STATE_AMAT = 8
		STATE_TXDP = 9
	
		TAGS = { 	'objs' 	=> STATE_OBJ, 	# Objects
					'tobj' 	=> STATE_TOBJ, 	# Time objects
					'tree'	=> STATE_TREE, 	# Trees
					'path'	=> STATE_PATH,
					'anim'	=> STATE_ANIM, 
					'tanm'	=> STATE_TANM, 
					'mlo'	=> STATE_MILO, 	# MILOs
					'2dfx'	=> STATE_2DFX, 	# 2D effects
					'amat'	=> STATE_AMAT,	# Audio materials
					'txdp'	=> STATE_TXDP,
					'end' 	=> STATE_NONE }	# Tag end token
	end

end # Game module
end # Pipeline module

# End of mapidefile.rb
