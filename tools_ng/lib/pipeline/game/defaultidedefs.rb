#
# mapidedefs.rb
# Game IDE Default Object Definition Classes
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 4 March 2008
# 

require 'pipeline/game/idefile'

module Pipeline
module Game

	#
	# == Description
	#
	# Weapon IDE Definition.
	#
	class WeaponIDEDef < IDEDefBase
	
		attr_reader :txd_name
		attr_reader :anim_dict
		attr_reader :num_lods
		attr_reader :lod_dist
		
		def WeaponIDEDef.parse( line, idefile )
			
			parts = line.split( ',' )
			parts.each do |part|
				part.strip!()
			end
			
			name = parts[0].downcase
			txd_name = parts[1].downcase
			anim_dict = parts[2].downcase != 'null' ? parts[2].downcase : nil
			num_lods = parts[3].to_i
			lod_dist = parts[4].to_f
			
			WeaponIDEDef.new( name, idefile, txd_name, anim_dict, num_lods, lod_dist )
		end
		
		def initialize( name, idefile, txd_name, anim_dict, num_lods, lod_dist )
		
			super( name, idefile )
			@txd_name = txd_name
			@anim_dict = anim_dict
			@num_lods = num_lods
			@lod_dist = lod_dist
		end			
	end

end # Game module
end # Pipeline module

# End of defaultidedefs.rb
