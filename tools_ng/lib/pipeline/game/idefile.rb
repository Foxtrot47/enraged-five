#
# idefile.rb
# Game Base IDE File Loader Class
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 28 February 2008
# 

module Pipeline
module Game

	#
	# == Description
	#
	# IDE File base class.
	#
	class IDEFileBase
	
		attr_reader :filename		# Fullpath to IDE File
		attr_reader :image_filename	# Fullpath to associated IMG File
		
		def initialize( filename, image_filename )		
			@filename = filename
			@image_filename = image_filename
		end
	end

	#
	# == Description
	#
	# IDE Definition base class.
	#
	class IDEDefBase
	
		attr_reader :name			# Definition name
		attr_reader :idefile		# IDEFile Object reference (owning this def)
		
		def initialize( name, idefile )
			@name = name.downcase
			@idefile = idefile
		end
	end

	#
	# == Description
	#
	# Parent Txd definition.
	#
	class IDEParentTxdDef < IDEDefBase
	
		attr_reader :parent_txd
		
		def txd
		
			@name
		end
		
		def IDEParentTxdDef.parse( line, idefile )
		
			parts = line.split( ',' )
			parts.each do |part|
				part.strip!()
			end
			
			txd = parts[0]
			parent_txd = parts[1]
			
			IDEParentTxdDef.new( txd, idefile, parent_txd )
		end
		
		def initialize( txd, idefile, parent_txd )
		
			super( txd, idefile )
			@parent_txd = parent_txd.downcase
		end
	end

end # Game module
end # Pipeline module

# End of idefile.rb
