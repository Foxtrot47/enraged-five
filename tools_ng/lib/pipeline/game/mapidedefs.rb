#
# mapidedefs.rb
# Game IDE Map Object Definition Classes
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 28 February 2008
# 

require 'pipeline/game/idefile'
require 'pipeline/math/bbox'
require 'pipeline/math/bsphere'
require 'pipeline/math/vector'
require 'pipeline/math/quat'

module Pipeline
module Game

	#
	# == Description
	#
	# Object definition class.
	#
	class MapIDEObjDef < IDEDefBase
	
		attr_reader :txd_name
		attr_reader :dd_name
		attr_reader :lod_distance
		attr_reader :bbox
		attr_reader :bsphere
		attr_reader :flags
		attr_reader :special
		
		def MapIDEObjDef.parse( line, idefile )
			
			parts = line.split( ',' )
			parts.each do |part|
				part.strip!()
			end
			
			name = parts[0].downcase
			txd_name = parts[1]
			lod_distance = parts[2]
			flags = parts[3]
			special = parts[4]
			bbox = nil #Math::BoundingBox.new # parts[5..7, 8..10]
			bsphere = nil #Math::BoundingSphere.new # parts[11..14]
			dd_name = 'null' != parts[15] ? parts[15] : nil
			
			MapIDEObjDef.new( name, idefile, txd_name, dd_name, lod_distance, 
							bbox, bsphere, flags, special )
		end
		
		def initialize( name, idefile, txd_name, dd_name, lod_distance, 
						  bbox, bsphere, flags, special )
		
			super( name, idefile )
			@txd_name = txd_name.downcase
			@dd_name = dd_name == nil ? nil : dd_name.downcase
			@lod_distance = lod_distance
			@bbox = bbox
			@bsphere = bsphere
			@flags = flags
			@special = special
		end		
	end

	#
	# == Description
	#
	# Time object definition class.
	#	
	class MapIDETimeObjDef < MapIDEObjDef
	
		attr_reader :time_on
		attr_reader :time_off
	
		def MapIDETimeObjDef.parse( line, idefile )
			
			parts = line.split( ',' )
			parts.each do |part|
				part.strip!()
			end

			name = parts[0]
			txd_name = parts[1]
			lod_distance = parts[2].to_f
			flags = parts[3].to_i
			special = parts[4].to_i
			bbox = nil #Math::BoundingBox.new() # parts[5..10]
			bsphere = nil #Math::BoundingSphere.new() # parts[11..14]
			dd_name = 'null' != parts[15] ? parts[15] : nil
			time_on = parts[16].to_i
			time_off = parts[17].to_i
			
			MapIDETimeObjDef.new( name, idefile, txd_name, dd_name, lod_distance, bbox, 
							   bsphere, flags, special, time_on, time_off ) 
		end
		
		def initialize( name, idefile, txd_name, dd_name, lod_distance, 
						  bbox, bsphere, flags, special, time_on, time_off )
		
			super( name, idefile, txd_name, dd_name, lod_distance, bbox, bsphere,
					flags, special )
			@time_on = time_on
			@time_off = time_off
		end
	end

	#
	# == Description
	#
	# Map Anim object definition.
	#	
	class MapIDEAnimObjDef < IDEDefBase
	
		attr_reader :txd_name				# Texture Dictionary
		attr_reader :anim_filename			# Animation Dictionary
		attr_reader :lod_dist
		attr_reader :bbox
		attr_reader :bsphere
		attr_reader :flags
		attr_reader :special_attributes
		attr_reader :dd_name
		
		def MapIDEAnimObjDef.parse( line, idefile )
			
			parts = line.split( ',' )
			parts.each do |part|
				part.strip!()
			end
			
			name = parts[0]
			txd_name = parts[1]
			anim_filename = parts[2]
			lod_dist = parts[3].to_f
			flags = parts[4].to_i
			special_attributes = parts[5].to_i			
			bbox = nil #Math::BoundingBox.new()	# parts[6..11]
			bsphere = nil #Math::BoundingSphere.new() # parts[12..15]
			dd_name = 'null' != parts[16] ? parts[16] : nil
			
			MapIDEAnimObjDef.new( name, idefile, txd_name, anim_filename, 
				lod_dist, bbox, bsphere, flags, special_attributes, dd_name )
		end
		
		def initialize( name, idefile, txd_name, anim_filename, lod_dist, 
			bbox, bsphere, flags, special_attributes, dd_name )
			
			super( name, idefile )
			@txd_name = txd_name.downcase
			@anim_filename = anim_filename.downcase
			@lod_dist = lod_dist
			@bbox = bbox
			@bsphere = bsphere
			@flags = flags
			@special_attributes = special_attributes
			@dd_name = dd_name
		end
	end


	#
	# == Description
	#
	# Animated time object class.
	#
	class MapIDETimeAnimObjDef < MapIDEAnimObjDef

		attr_reader :time_on
		attr_reader :time_off
		
		def MapIDETimeAnimObjDef.parse( line, idefile )
			
			parts = line.split( ',' )
			parts.each do |part|
				part.strip!()
			end
			
			name = parts[0]
			txd_name = parts[1]
			anim_filename = parts[2]
			lod_dist = parts[3].to_f
			flags = parts[4].to_i
			special_attributes = parts[5].to_i			
			bbox = nil #Math::BoundingBox.new()	# parts[6..11]
			bsphere = nil #Math::BoundingSphere.new() # parts[12..15]
			dd_name = 'null' != parts[16] ? parts[16] : nil
			time_on = parts[17]
			time_off = parts[18]
			
			MapIDETimeAnimObjDef.new( name, idefile, txd_name, anim_filename, 
				lod_dist, bbox, bsphere, flags, special_attributes, dd_name,
				time_on, time_off )			
		end
		
		def initialize( name, idefile, txd_name, anim_filename, lod_dist, 
				bbox, bsphere, flags, special_attributes, dd_name,
				time_on, time_off )
			
			super( name, idefile, txd_name, anim_filename, lod_dist, 
					bbox, bsphere, flags, special_attributes, dd_name )
			@time_on = time_on
			@time_off = time_off
		end	
	end


	#
	# == Description
	#
	# MILO definition.
	#	
	class MapIDEMiloObjDef < IDEDefBase
	
		attr_reader :flags
		attr_reader :num_rooms
		attr_reader :num_portals
		attr_reader :num_objects
		attr_reader :detail_dist
		attr_reader :lod1_dist
		attr_reader :lod2_dist
		
		def MapIDEMiloObjDef.parse( line, idefile )
			
			parts = line.split( ',' )
			parts.each do |part|
				part.strip!()
			end
			
			name = parts[0]
			flags = parts[1].to_i
			nrooms = parts[2].to_i
			nportals = parts[3].to_i
			nobjs = parts[4].to_i
			detail_dist = parts[5].to_f
			lod1_dist = parts[6].to_f
			lod2_dist = parts[7].to_f
			
			MapIDEMiloObjDef.new( name, idefile, flags, nrooms, nportals, nobjs, 
							detail_dist, lod1_dist, lod2_dist )
		end
		
		def initialize( name, idefile, flags, nrooms, nportals, nobjs, detail_dist,
						  lod1_dist, lod2_dist )
			
			super( name, idefile )
			@flags = flags
			@num_rooms = nrooms
			@num_portals = nportals
			@num_objects = nobjs
			@detail_dist = detail_dist
			@lod1_dist = lod1_dist
			@lod2_dist = lod2_dist
		end
	end		

	#
	# == Description
	#
	# 2DFX definition.
	#	
	class MapIDE2DFXDef < IDEDefBase
		class UnknownType < Exception; end
	
		attr_reader :position
		attr_reader :rotation

		def MapIDE2DFXDef.parse( line, idefile )
			
			parts = line.split( ',' )
			parts.each do |part|
				part.strip!()
			end
			
			name = parts[0].downcase
			position = Math::Vector.new() # parts[1..3]
			type = parts[4].to_i
			rotation = Math::Vector.new() # parts[5..8]
			
			case type
			when ET_LIGHT
				parse_light( name, idefile, position, rotation, parts )
			when ET_PARTICLE
				return MapIDE2DFXDef.new( name, idefile, position, rotation )
			when ET_EXPLOSION
				return MapIDE2DFXDef.new( name, idefile, position, rotation )
			when ET_ESCALATOR
				return MapIDE2DFXDef.new( name, idefile, position, rotation )
			when ET_PROCEDURALOBJECTS
				return MapIDE2DFXDef.new( name, idefile, position, rotation )
			when ET_COORDSCRIPT
				return MapIDE2DFXDef.new( name, idefile, position, rotation )
			when ET_LADDER
				return MapIDE2DFXDef.new( name, idefile, position, rotation )
			when ET_AUDIOEFFECT
				return MapIDE2DFXDef.new( name, idefile, position, rotation )
			when ET_SPAWN_POINT
				return MapIDE2DFXDef.new( name, idefile, position, rotation )
			when ET_LIGHT_SHAFT
				return MapIDE2DFXDef.new( name, idefile, position, rotation )
			when ET_SCROLLBAR
				return MapIDE2DFXDef.new( name, idefile, position, rotation )
			when ET_SWAYABLE
				return MapIDE2DFXDef.new( name, idefile, position, rotation )
			when ET_BUOYANCY
				return MapIDE2DFXDef.new( name, idefile, position, rotation )
			when ET_WALKDONTWALK
				return MapIDE2DFXDef.new( name, idefile, position, rotation )
			when ET_LEDGE
				return MapIDE2DFXDef.new( name, idefile, position, rotation )
			else
				raise UnknownType.new( "Unknown 2DFX Type: #{type}" )
			end # End type switch
		end

		#---------------------------------------------------------------------
		# Protected
		#---------------------------------------------------------------------
	
	protected
		
		def initialize( name, idefile, position, rotation )
			
			super( name, idefile )
			@position = position
			@rotation = rotation
		end
		
		#---------------------------------------------------------------------
		# Private
		#---------------------------------------------------------------------
	
	private
	
		def MapIDE2DFXDef.parse_light( name, idefile, position, rotation, parts )
			type = parts[9]
			colour = nil # parts[10..13]
			corona_name = parts[14].gsub( '"', '' )
			shadow_name = parts[15].gsub( '"', '' )
			lod_distance = parts[16].to_f
			size = parts[17].to_s
			corona_size = parts[18].to_f
			shadow_size = parts[19].to_f
			shadow_alpha = parts[20].to_i
			flashyness = parts[21].to_i
			reflection_type = parts[22].to_i
			lens_flare_type = parts[23].to_i
			flags = parts[24].to_i
			cone_inner = parts[25].to_i
			cone_outer = parts[26].to_i
			intensity = parts[27].to_f
			volume_intensity = parts[28].to_f
			volume_intensity = parts[29].to_f
			volume_size = parts[30].to_f
			atten_start = parts[31].to_f
			atten_end = parts[32].to_f
			direction = Math::Vector.new( parts[33].to_f, parts[34].to_f, parts[35].to_f )
			up = Math::Vector.new( parts[36].to_f, parts[37].to_f, parts[38].to_f )
			attach = parts[39].to_i
			light_fade = parts[40].to_f
			volume_fade = parts[41].to_f
			corona_hdr_multiplier = parts[42].to_f
						
			if ( 'null' == shadow_name or '' == shadow_name )
				shadow_name = nil
			end
			if ( 'null' == corona_name or '' == corona_name )
				corona_name = nil
			end
			
			return MapIDELight2DFXDef.new( name, idefile, position, rotation,
						corona_name, shadow_name, lod_distance, size, 
						corona_size, shadow_size, shadow_alpha, flashyness,
						reflection_type, lens_flare_type, flags,
						cone_inner, cone_outer, intensity, volume_intensity,
						volume_size, atten_start, atten_end, direction, up,
						attach, light_fade, volume_fade, corona_hdr_multiplier )			
		end
	
		ET_LIGHT				= 0
		ET_PARTICLE				= 1
		ET_EXPLOSION			= 2
		ET_ESCALATOR			= 10
		ET_PROCEDURALOBJECTS	= 12
		ET_COORDSCRIPT			= 13
		ET_LADDER				= 14
		ET_AUDIOEFFECT			= 15
		ET_SPAWN_POINT			= 17
		ET_LIGHT_SHAFT			= 18
		ET_SCROLLBAR			= 19
		ET_SWAYABLE				= 21
		ET_BUOYANCY				= 22
		ET_WALKDONTWALK			= 23		
		ET_LEDGE				= 24
	end	

	class MapIDELight2DFXDef < MapIDE2DFXDef
	
		attr_reader :corona_name
		attr_reader :shadow_name
		attr_reader :lod_distance
		attr_reader :size
		attr_reader :corona_size
		attr_reader :shadow_size
		attr_reader :shadow_alpha
		attr_reader :flashyness
		attr_reader :reflection_type
		attr_reader :lens_flare_type
		attr_reader :flags
		attr_reader :cone_inner
		attr_reader :cone_outer
		attr_reader :intensity
		attr_reader :volume_intensity
		attr_reader :volume_size
		attr_reader :atten_start
		attr_reader :atten_end
		attr_reader :direction
		attr_reader :up
		attr_reader :attach
		attr_reader :light_fade
		attr_reader :volume_fade
		attr_reader :corona_hdr_multiplier
	
		def initialize( name, idefile, position, rotation,
						corona_name, shadow_name, lod_distance, size, 
						corona_size, shadow_size, shadow_alpha, flashyness,
						reflection_type, lens_flare_type, flags,
						cone_inner, cone_outer, intensity, volume_intensity,
						volume_size, atten_start, atten_end, direction, up,
						attach, light_fade, volume_fade, corona_hdr_multiplier )
			super( name, idefile, position, rotation )
		end
	end
	
	class MapIDEParticle2DFXDef < MapIDE2DFXDef
	
		def initialize( name, idefile, position, rotation )
			super( name, idefile, position, rotation )
		end
	end
	
	class MapIDEExplosion2DFXDef < MapIDE2DFXDef
	
		def initialize( name, idefile, position, rotation )
			super( name, idefile, position, rotation )
		end
	end
	
	class MapIDEEscalator2DFXDef < MapIDE2DFXDef
	
		def initialize( name, idefile, position, rotation )
			super( name, idefile, position, rotation )
		end
	end	
	
	class MapIDEProcedural2DFXDef < MapIDE2DFXDef
	
		def initialize( name, idefile, position, rotation )
			super( name, idefile, position, rotation )
		end
	end
	
	class MapIDECoordScript2DFXDef < MapIDE2DFXDef
	
		def initialize( name, idefile, position, rotation )
			super( name, idefile, position, rotation )
		end
	end
	
	class MapIDELadder2DFXDef < MapIDE2DFXDef
	
		attr_reader :bottom	# Vector
		attr_reader :top	# Vector
		attr_reader :normal	# Vector
		attr_reader :can_get_off_at_top # Bool
	
		def initialize( name, idefile, position, rotation, 
						bottom, top, normal, can_get_off_at_top )
			super( name, idefile, position, rotation )
			@bottom = bottom
			@top = top
			@normal = normal
			@can_get_off_at_top = can_get_off_at_top
		end
	end
	
	class MapIDEAudioEffect2DFXDef < MapIDE2DFXDef
	
		def initialize( name, idefile, position, rotation )
			super( name, idefile, position, rotation )
		end
	end
	
	class MapIDESpawnPoint2DFXDef < MapIDE2DFXDef
	
		def initialize( name, idefile, position, rotation )
			super( name, idefile, position, rotation )
		end
	end
	
	class MapIDELightShaft2DFXDef < MapIDE2DFXDef
	
		def initialize( name, idefile, position, rotation )
			super( name, idefile, position, rotation )
		end
	end	

	class MapIDEScrollbar2DFXDef < MapIDE2DFXDef
	
		def initialize( name, idefile, position, rotation )
			super( name, idefile, position, rotation )
		end
	end
	
	class MapIDESwayable2DFXDef < MapIDE2DFXDef
	
		def initialize( name, idefile, position, rotation )
			super( name, idefile, position, rotation )
		end
	end

	class MapIDEBuoyancy2DFXDef < MapIDE2DFXDef
	
		def initialize( name, idefile, position, rotation )
			super( name, idefile, position, rotation )
		end
	end
	
	class MapIDEWalkDontWant2DFXDef < MapIDE2DFXDef
	
		def initialize( name, idefile, position, rotation )
			super( name, idefile, position, rotation )
		end
	end

end # Game module
end # Pipeline module

# End of mapidedefs.rb
