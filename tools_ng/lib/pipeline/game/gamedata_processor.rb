#
# File:: gamedata_processor.rb
# Description:: Project's game data processor to walk specifc parts of game 
#               data files.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 18 August 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/game/gamedataloader'
require 'pipeline/util/rage'

#-----------------------------------------------------------------------------
# Code
#-----------------------------------------------------------------------------
module Pipeline
module Game

	#
	# == Description
	# A project's game data processor.  The processor is initialised with a 
	# project (Pipeline::Project) object and can then invoke user-code for each 
	# interesting piece of game data for that project (e.g. each IPL file, IDE, 
	# Image, RPF etc).
	#
	# The GameDataProcessor extracts any neccessary assets into a temporary
	# location and specifies filenames etc. to the user-defined code block
	# for custom processing.
	#
	# == Example Usage
	#
	#    p = Pipeline::Config.instance.projects['gta4']
	#    dataprocessor = Pipeline::Game::GameDataProcessor.new( p )
	#    dataprocessor.walk_images() do |image_filename, mount_point, ide_obj, ipl_obj|
	#       # Custom code here...
	#    end
	#
	class GameDataProcessor
		attr_reader :project
		attr_reader :game_data
	
		IMAGE_MOUNT = "image:/"
	
		def initialize( project, verbose = false )
			raise ArgumentError.new( "Invalid project (#{project.class})." ) \
				unless ( project.is_a?( Pipeline::Project ) )
			@project = project
			@project.load_config( )
			@r = Pipeline::RageUtils.instance()
			@game_data = GameDataLoader.new( @project )
		end
		
		#
		# Walks our loaded map game data, mounting the map image and then 
		# invoking the passed block with the following arguments:
		#   image_filename - absolute filename of image mounted
		#   mount_point - image mount point
		#   ide_obj - associated IDE file
		#   ipl_obj - associated IPL file (can be nil)
		#
		# The image is unmounted after the yield automatically.
		#
		def walk_map_images( verbose = false, &block )
			return unless ( block_given? )

			@game_data.load_ides( verbose )
			@game_data.load_ipls( verbose )

			@game_data.project_ides.each_pair do |ide_filename, ide_obj|
				@r.image.mount( ide_obj.image_filename, IMAGE_MOUNT )
				
				ipl_filename = OS::Path::replace_ext( ide_filename, 'ipl' )
				ipl_obj = nil
				ipl_obj = @game_data.project_ipls[ipl_filename] if ( @game_data.project_ipls.has_key?( ipl_filename ) )
				
				# Invoke callee's block
				yield( ide_obj.image_filename, IMAGE_MOUNT, ide_obj, ipl_obj )
				
				@r.image.unmount( ide_obj.image_filename )
			end
		end
	end

end # Game module
end # Pipeline module

if ( __FILE__ == $0 ) then

	c = Pipeline::Config.instance()
	p = c.projects['jimmy']
	gdp = Pipeline::Game::GameDataProcessor.new( p, false )
	gdp.walk_images() do |image_filename, mount_point, ide_obj, ipl_obj|
		puts "#{image_filename} #{mount_point} #{ide_obj} #{ipl_obj}"
	end
end

# gamedata_processor.rb
