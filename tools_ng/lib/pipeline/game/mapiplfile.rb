#
# File:: mapiplfile.rb
# Description:: Game Map IDE File Loader Class
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 15 April 2008
# 

require 'pipeline/game/iplfile'
require 'pipeline/game/mapipldefs'
require 'pipeline/os/path'

module Pipeline
module Game
	
	#
	# == Description
	#
	#
	class MapIPLFile < IPLFileBase
		
		attr_reader :instances
		attr_reader :culls
		attr_reader :paths
		attr_reader :garages
		attr_reader :entryexits
		attr_reader :pickups
		attr_reader :stuntjumps
		attr_reader :timecycles
		attr_reader :soundzones
		attr_reader :blocks
		attr_reader :multibuildings
		attr_reader :vehicle_nodes
		attr_reader :vehicle_links
		attr_reader :milo_plus
		attr_reader :fx2d
		attr_reader :lod_modifiers
		attr_reader :slow_zones
		
		def initialize( path )
			super( path )

			@instances = Array.new
			@culls = Array.new
			@paths = Array.new
			@garages = Array.new
			@entryexits = Array.new
			@pickups = Array.new
			@stuntjumps = Array.new
			@timecycles = Array.new
			@soundzones = Array.new
			@blocks = Array.new
			@multibuildings = Array.new
			@vehicle_nodes = Array.new
			@vehicle_links = Array.new
			@milo_plus = Array.new
			@fx2d = Array.new
			@lod_modifiers = Array.new
			@slow_zones = Array.new
			
			if ( ( OS::Path.get_drive( path ).size > 1 ) &&
			      !File.exists?( path ) )
				
				open_mounted_file( path )
			else
				open_local_file( path )
			end
		end

		#---------------------------------------------------------------------
		# Private Methods
		#---------------------------------------------------------------------
	
	private
		# This private method is called by the constructor when the file is
		# determined to be located on a rage mounted image/pack file.  The
		# file is copied to a temporary area and then open (using regular
		# open_local_file(), and the IPL is then deleted.
		def open_mounted_file( path )
		
			r = RageUtils.instance()
			filename = OS::Path.get_filename( path )
			tf = Tempfile.new( filename )
			tf.flush
			tf.close
			
			r.util.copy_file( path, tf.path )
			
			open_local_file( tf.path )
			
			File.delete( tf.path )
		end
	
		# This private method is called by the constructor to open a local
		# file.
		def open_local_file( path )
			
			state = STATE_NONE
			File.open( path ) do |file|
				
				begin			
					file.each do |linex|
	
						line = linex.strip
						# Skip empty lines
						next if 0 == line.size
	
						# Skip comment lines
						next if '#' == line[0]
	
						# See if we need to move to next state
						changed_state = false
						TAGS.each do |tag, id|				
							state = id if tag == line
							changed_state = true if tag == line
						end
						next if changed_state
						
						case state
						when STATE_INST
							@instances << MapIPLInst.parse( line )
						
						when STATE_CULL
							@culls << nil

						when STATE_PATH
							@paths << nil
							
						when STATE_GRGE
							@garages << nil
							
						when STATE_ENEX
							@entryexits << nil
							
						when STATE_PICK
							@pickups << nil
							
						when STATE_JUMP
							@stuntjumps << nil
							
						when STATE_TCYC
							@timecycles << nil
							
						when STATE_AUZO
							@soundzones << nil
							
						when STATE_BLOK
							@blocks << nil
							
						when STATE_MULT
							@multibuildings << nil
							
						when STATE_VNOD
							@vehicle_nodes << nil
							
						when STATE_LINK
							@vehicle_links << nil
							
						when STATE_MLOPLUS
							@milo_plus << nil
							
						when STATE_2DFX
							@fx2d << nil
							
						when STATE_LODM
							@lod_modifiers << nil
							
						when STATE_SLOW
							@slow_zones << nil
						end
					end
				ensure
					file.close()
				end
			end			
		end

		#---------------------------------------------------------------------
		# Private Constants
		#---------------------------------------------------------------------
	
	private
		
		STATE_NONE = -1
		STATE_INST = 0
		STATE_CULL = 1
		STATE_PATH = 2
		STATE_GRGE = 3
		STATE_ENEX = 4
		STATE_PICK = 5
		STATE_JUMP = 6
		STATE_TCYC = 7
		STATE_AUZO = 8
		STATE_BLOK = 9
		STATE_MULT = 10
		STATE_VNOD = 11
		STATE_LINK = 12
		STATE_MLOPLUS = 13
		STATE_2DFX = 14
		STATE_LODM = 15
		STATE_SLOW = 16
	
		TAGS = { 	'inst' 	=> STATE_INST, 	# Instances
					'cull'	=> STATE_CULL,
					'path'	=> STATE_PATH,
					'grge'	=> STATE_GRGE,
					'enex'	=> STATE_ENEX,
					'pick'	=> STATE_PICK,
					'jump'	=> STATE_JUMP,
					'tcyc'	=> STATE_TCYC,
					'auzo'	=> STATE_AUZO,
					'blok'	=> STATE_BLOK,
					'mult'	=> STATE_MULT,
					'vnod'	=> STATE_VNOD,
					'link'	=> STATE_LINK,
					'mlo+'	=> STATE_MLOPLUS,
					'2dfx'	=> STATE_2DFX,
					'lodm'	=> STATE_LODM,
					'slow'	=> STATE_SLOW,
					'end' 	=> STATE_NONE }	# Tag end token			
    end
	
end # Game module
end # Pipeline module

# End of mapiplfile.rb
