#
# idefile.rb
# Game IDE File Loader Class
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 3 March 2008
# 

require 'pipeline/game/idefile'
require 'pipeline/game/vehicleidedefs'
require 'pipeline/os/path'
require 'pipeline/util/string'

module Pipeline
module Game

	#
	# == Description
	#
	# Vehicles IDE file loader class.
	#
	class VehicleIDEFile < IDEFileBase
	
		attr_reader :vehicle_definitions
		attr_reader :parent_txd_definitions
	
		def initialize( filename, image_filename )
		
			super( filename, image_filename )
			@vehicle_definitions = Array.new
			@parent_txd_definitions = Array.new
			state = STATE_NONE
			
			File.open( @filename ) do |file|
			
				begin
					file.each do |linex|
	
						line = linex.strip
						# Skip empty lines
						next if 0 == line.size
	
						# Skip comment lines
						next if line.starts_with( '#' )
		
						# See if we need to move to next state
						changed_state = false
						TAGS.each do |tag, id|
							state = id if tag == line
							changed_state = true if tag == line
						end
						next if changed_state
						
						case state
						when STATE_CARS
							@vehicle_definitions << VehicleIDECarDef.parse( line, self )

						when STATE_TXDP
							@parent_txd_definitions << IDEParentTxdDef.parse( line, self )
							
						end
					end
				ensure
					file.close()
				end
			end
		end
		
		#---------------------------------------------------------------------
		# Private 
		#---------------------------------------------------------------------
	
	private
	
		STATE_NONE = -1
		STATE_CARS = 1
		STATE_TXDP = 2
	
		TAGS = { 	'cars' 	=> STATE_CARS, 	# Cars
					'txdp'	=> STATE_TXDP,	# Parent TXDs
					'end' 	=> STATE_NONE }	# Tag end token
	end

end # Game module
end # Pipeline module

#-----------------------------------------------------------------------------
# Unit Tests
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

	Test::Unit::UI::Console::TestRunner.run( Pipeline::Game::VehicleIDEFileTests )	

end

# End of vehicleidefile.rb

