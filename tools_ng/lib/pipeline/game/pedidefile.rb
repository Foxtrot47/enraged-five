#
# pedidefile.rb
# Game IDE Ped File Loader Class
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 3 March 2008
# 

require 'pipeline/game/idefile'
require 'pipeline/game/pedidedefs'
require 'pipeline/os/path'
require 'pipeline/util/string'

module Pipeline
module Game

	#
	# == Description
	#
	# Ped IDE file loader class.
	#
	class PedIDEFile < IDEFileBase
	
		attr_reader :ped_player
		attr_reader :ped_definitions
		attr_reader :ped_audio_groups
	
		def initialize( filename, image_filename )
		
			super( filename, image_filename )
			@ped_definitions = Array.new
			@ped_audio_groups = Array.new
			state = STATE_NONE
			
			File.open( @filename ) do |file|
			
				begin
					file.each do |linex|
	
						line = linex.strip
						# Skip empty lines
						next if 0 == line.size
	
						# Skip comment lines
						next if line.starts_with( '#' )
	
						# See if we need to move to next state
						changed_state = false
						TAGS.each do |tag, id|
							state = id if tag == line
							changed_state = true if tag == line
						end
						next if changed_state
						
						case state
						when STATE_CARS
							@ped_definitions << PedIDEPedDef.parse( line, self )

						when STATE_AGRPS
							@ped_audio_groups << PedIDEAudioGroupDef.parse( line, self )
							
						end
					end
				ensure
					file.close()
				end
			end
			# Player ped is our first defined ped
			@ped_player = @ped_definitions[0]
			@ped_definitions.delete_at( 0 )
		end
		
		#---------------------------------------------------------------------
		# Private 
		#---------------------------------------------------------------------
	
	private
	
		STATE_NONE = -1
		STATE_CARS = 1
		STATE_AGRPS = 2
	
		TAGS = { 	'peds' 	=> STATE_CARS, 	# Cars
					'agrps'	=> STATE_AGRPS,	# Ped audio groups
					'end' 	=> STATE_NONE }	# Tag end token
	end

end # Game module
end # Pipeline module

# End of pedidefile.rb
