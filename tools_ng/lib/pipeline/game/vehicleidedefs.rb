#
# vehicleidedefs.rb
# Game IDE Vehicle Object Definition Classes
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 3 March 2008
# 

require 'pipeline/game/idefile'

module Pipeline
module Game

	#
	# == Description
	#
	# Vehicle IDE file loader class.
	#
	#
	class VehicleIDECarDef < IDEDefBase
	
		attr_reader :txd_name
		attr_reader :type
		attr_reader :handling_id
		attr_reader :game_name
		attr_reader :anims
		attr_reader :anims2
		attr_reader :frequency
		attr_reader :max_num
		attr_reader :radius_front
		attr_reader :radius_rear
		attr_reader :default_dirt_level
		attr_reader :lod_mult
		attr_reader :swankness
		attr_reader :flags
		
		def VehicleIDECarDef.parse( line, idefile )
		
			parts = line.split( ',' )
			parts.each do |part|
				part.strip!()
			end

			name = parts[0]
			txd_name = parts[1]
			type = parts[2]
			handling_id = parts[3]
			game_name = parts[4]
			anims = parts[5]
			anims2 = parts[6]
			frequency = parts[7].to_i
			max_num = parts[8].to_i
			radius_front = parts[9].to_f
			radius_rear = parts[10].to_f
			default_dirt_level = parts[11]
			lod_mult = parts[12]
			swankness = parts[13].to_f
			flags = parts[14]
			
			VehicleIDECarDef.new( name, idefile, txd_name, type, handling_id, game_name,
				anims, anims2, frequency, max_num, radius_front, radius_rear, 
				default_dirt_level, lod_mult, swankness, flags )		
		end
		
		def initialize( name, idefile, txd_name, type, handling_id, game_name,
				anims, anims2, frequency, max_num, radius_front, radius_rear, 
				default_dirt_level, lod_mult, swankness, flags )
		
			super( name, idefile )
			@txd_name = txd_name
			@type = type
			@handling_id = handling_id
			@game_name = game_name
			@anims = anims
			@anims2 = anims2
			@frequency = frequency
			@max_num = max_num
			@radius_front = radius_front
			@radius_rear = radius_rear
			@default_dirt_level = default_dirt_level
			@lod_mult = lod_mult
			@swankness = swankness
			@flags = flags
		end
	end
	
end # Game module
end # Pipeline module

# End of vehicleidedefs.rb
