#
# File:: %RS_TOOLSLIB%/pipeline/content/content_dependency.rb
# Description:: Content system dependency content tree node classes.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 22 March 2011
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/content/treecore'
require 'pipeline/os/path'
require 'pipeline/scm/perforce'
require 'rexml/document'
include REXML

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Content
	
	#
	# == Description
	# The DependencyFile content node class represents a specific-file 
	# dependency within the content system.
	#
	class DependencyFile < File
		XML_CONTENT_TYPE = 'dependency_file'
		
		def initialize( filename )
			name = OS::Path::get_basename( filename )
			path = OS::Path::get_directory( filename )
			ext = OS::Path::get_extension( filename )
			super( "#{name}_file_dependency", path, ext )
		end
		
		# Determine if there are dependencies that are out of date.
		def outdated?( )
			results = do_sync( :preview )
			( results.size() > 0 )
		end
		
		# Sync the dependency.
		def sync( force = false, p4 = nil )
			do_sync( force ? :force : :normal, p4 )
		end
		
		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		def DependencyFile::from_xml( xml_node, path, env, target )
			filename = env.subst( xml_node.attributes['path'] )
			DependencyFile::new( filename )
		end
		
		def DependencyFile::from_filename( filename )
			DependencyFile::new( filename )
		end
		
		#---------------------------------------------------------------------
		# Private Methods
		#---------------------------------------------------------------------
	private	
		def do_sync( mode = :normal, p4 = nil, preview = false )
			
			results = []
			was_connected = p4.nil? ? false : p4.connected?
			p4 = SCM::Perforce::new( ) if ( p4.nil? )
			Dir::chdir( @path ) do 
				p4.connect( )
				case mode
				when :normal
					results = p4.run_sync( OS::Path::combine( @path, '...' ) )
				when :force
					results = p4.run_sync( '-f', OS::Path::combine( @path, '...' ) )
				when :preview
					results = p4.run_sync( '-n', OS::Path::combine( @path, '...' ) )
				end
				p4.disconnect( ) unless ( was_connected )
			end
			results
		end
	end
	
	#
	# == Description
	# The DependencyDirectory content node class represents an entire directory
	# (recursive) dependency within the content system.
	#
	class DependencyDirectory < File
		XML_CONTENT_TYPE = 'dependency_directory'
		
		def initialize( path )
			
			super( 'directory_dependency', path, '' )
		end
		
		# Determine if there are dependencies that are out of date.
		def outdated?( )
			results = do_sync( :preview )
			( results.size() > 0 )
		end
		
		# Sync the dependency.
		def sync( force = false, p4 = nil )
			do_sync( force ? :force : :normal, p4 )
		end
		
		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		def DependencyDirectory::from_xml( xml_node, path, env, target )
			path = env.subst( xml_node.attributes['path'] )
			DependencyDirectory::new( path )
		end
		
		def DependencyDirectory::from_filename( path )
			DependencyFileDependencyDirectory::new( OS::Path::get_directory( path ) )
		end
		
		#---------------------------------------------------------------------
		# Private Methods
		#---------------------------------------------------------------------
	private
		def do_sync( mode = :normal, p4 = nil, preview = false )
			
			results = []
			was_connected = p4.nil? ? false : p4.connected?
			p4 = SCM::Perforce::new( ) if ( p4.nil? )
			Dir::chdir( @path ) do 
				p4.connect( )
				case mode
				when :normal
					results = p4.run_sync( filename )
				when :force
					results = p4.run_sync( '-f', filename )
				when :preview
					results = p4.run_sync( '-n', filename )
				end
				p4.disconnect( ) unless ( was_connected )
			end
			results
		end
	end
	
end # Content module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/content/content_dependency.rb
