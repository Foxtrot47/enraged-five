#
# File:: content_cutscene.rb
# Description:: Content system cutscene tree nodes.
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 18 November 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/content/content_map'
require 'rexml/document'
include REXML


module Pipeline
	module Content
        
        #
        # This is derived from the MapGeo stuff.  Cutscene props are exported individually (each prop has it's own max file) but
		# I want to leave the option open for them being exported collectively like in-game props are.  
        #
        class CutscenePropGeo < MapGeo
			
			#
			#
			#
			def initialize( srcfile, project )
                super(srcfile, project, "", false, "cutsprops")
			end
			

		end
		
	end
end