#
# File:: %RS_TOOLSLIB%/pipeline/content/converter_character.rb
# Description:: Content system character content tree node class implementation for a ped.
#
# Author::Adam Munson
# Date:: 16/03/2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/content/treecore'
require 'pipeline/content/content_core'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Content

    #
    # == Description
    #
    class ProcessedCharacterDirectory < Directory
		XML_CONTENT_TYPE = 'processed_character_directory'
    
        def initialize( name, path, target )
            super( name, path, target )
            @xml_type = XML_CONTENT_TYPE
        end
        
        # Rebuild the inputs Array by stating disk.
        def reform_inputs( )
            @inputs.clear( )
            
            files = OS::FindEx::find_files_recurse( OS::Path::combine( @absolute_path, '*.zip' ) )
            files += OS::FindEx::find_files( OS::Path::combine( @absolute_path, '*.meta' ) )
            files.each do |filename|
                add_input( Content::File::from_filename( filename ) )
            end
            Content::Parser::log( ).warn( "No inputs for directory node: #{self}." ) \
                unless ( @inputs.size > 0 )
        end
    end
    
    #
    # == Description
    #
    class ProcessedStreamedCharacterDirectory < ProcessedCharacterDirectory
		XML_CONTENT_TYPE = 'processed_streamed_character_directory'
    
        def initialize( name, path, target )
            super( name, path, target )
            @xml_type = XML_CONTENT_TYPE
        end
        
		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def ProcessedStreamedCharacterDirectory::from_xml( xml_node, path, env, target )

			name = env.subst( xml_node.attributes['name'] )
			path = OS::Path.combine( path, xml_node.attributes['path'] )

			ProcessedStreamedCharacterDirectory::new( name, path, target )
		end  
    end
    
    #
    # == Description
    #
    class ProcessedComponentCharacterDirectory < ProcessedCharacterDirectory
		XML_CONTENT_TYPE = 'processed_component_character_directory'
    
        def initialize( name, path, target )
            super( name, path, target )
            @xml_type = XML_CONTENT_TYPE
        end
        
		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def ProcessedComponentCharacterDirectory::from_xml( xml_node, path, env, target )

			name = env.subst( xml_node.attributes['name'] )
			path = OS::Path.combine( path, xml_node.attributes['path'] )

			ProcessedComponentCharacterDirectory::new( name, path, target )
		end  
    end
    
    #
    # == Description
    #
    class ProcessedCutsceneCharacterDirectory < ProcessedCharacterDirectory
		XML_CONTENT_TYPE = 'processed_cutscene_character_directory'
    
        def initialize( name, path, target )
            super( name, path, target )
            @xml_type = XML_CONTENT_TYPE
        end
        
		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def ProcessedCutsceneCharacterDirectory::from_xml( xml_node, path, env, target )

			name = env.subst( xml_node.attributes['name'] )
			path = OS::Path.combine( path, xml_node.attributes['path'] )

			ProcessedCutsceneCharacterDirectory::new( name, path, target )
		end  
    end

end # Content module
end # Pipeline module
