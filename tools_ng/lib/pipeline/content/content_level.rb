#
# File:: %RS_TOOLSLIB%/pipeline/content/content_level.rb
# Description:: Content system level-related content tree node classes.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 23 March 2011
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/content/treecore'
require 'pipeline/content/content_core'
require 'pipeline/os/path'
require 'rexml/document'
include REXML

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Content

	#
	# == Description
	# 
	#
	class LevelBitmap < File
		XML_CONTENT_TYPE = 'level_image'
		
		attr_reader :min_x
		attr_reader :max_x
		attr_reader :min_y
		attr_reader :max_y
		
		def initialize( filename, min_x, min_y, max_x, max_y )
			name = OS::Path::get_basename( filename )
			ext = OS::Path::get_extension( filename )
			path = OS::Path::get_directory( filename )
			super( name, path, ext )
		end
		
		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		def LevelBitmap::from_xml( xml_node, path, env, target )
						
			filename = env.subst( xml_node.attributes['path'] )
			min_x = xml_node.attributes['min_x'].to_f
			min_y = xml_node.attributes['min_y'].to_f
			max_x = xml_node.attributes['max_x'].to_f
			max_y = xml_node.attributes['max_y'].to_f
			
			LevelBitmap::new( filename, min_x, min_y, max_x, max_y )
		end
	end
	
end # Content module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/content/content_level.rb