#
# File:: pipeline/content/content_weapons.rb
# Description:: Custom weapons content system nodes.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 4 August 2008 
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/content/treecore'
require 'pipeline/content/content_core'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Content

	#
	# == Description
	# Weapons ZIP archive.  Simple type so we can find it easily in our
	# exporter.
	#
	class WeaponZIP < Zip
		XML_CONTENT_TYPE = 'weaponszip'
		
		def initialize( name, path, target )
			super( name, path, 'zip', target )
			@xml_type = XML_CONTENT_TYPE
		end 
		
		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		
		#
		# Parse an XML node for the weapon ZIP file representation.
		#
		def WeaponZIP::from_xml( node, path, env, target )
			
			name = env.subst( node.attributes['name'] )
			zip = WeaponZIP.new( name, path, target )
			zip
		end
	end

end # Content module
end # Pipeline module

# pipeline/content/content_weapons.rb
