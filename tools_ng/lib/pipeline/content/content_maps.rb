#
# File:: %RS_TOOLSLIB%/pipeline/content/content_maps.rb
# Description:: Content system map content tree node class implementation.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Author:: Greg Smith <greg@rockstarnorth.com>
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 12 June 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/content/treecore'
require 'rexml/document'
include REXML

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Content

	#
	# == Description
	# Map content node class.  These objects represent map content data.  They
	# typically have upto three input File nodes that represent its IDE, IPL
	# and IMG files.
	#
	# The map content node now has a subtype which gives more high-level 
	# information about the type of assets used; this is currently not
	# required by the map exporter but is used in higher-level tools (e.g.
	# Workbench for statistics purposes).
	#
	# The valid map maptypes are:
	#   :unknown, :props, :interior, 
	#   :container, :container_props, :container_lod, :container_occl
	#
	class Map < Base

		attr_reader :path
		attr_reader :prefix				# Object name prefix (default '')
		attr_reader :exportdefinitions
		attr_reader :exportinstances
		attr_reader :exportdata
		attr_reader :pedantic			# Map pedantic flag
		attr_reader :maptype			# Map subtype (see above for valid symbols)

		XML_CONTENT_TYPE = 'map'
	
		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def initialize( name, path, prefix, defs, insts, data = true, pedantic = true, maptype = :unknown )
			super( name )
			@xml_type = XML_CONTENT_TYPE
			@path = path
			@prefix = prefix
			@exportdefinitions = defs
			@exportinstances = insts
			@exportdata = data
			@pedantic = pedantic
			@maptype = maptype
		end
	
		def to_s()
			"#{super()} defs:#{@exportdefinitions} inst:#{@exportinstances} data:#{@exportdata}"
		end
	
		#
		# Fill an environment object from this content object's state.
		#
		def fill_env( env )
			super( env )
			env.add( 'exportdefinitions', @exportdefinitions ) if ( @exportdefinitions )
			env.add( 'exportinstances', @exportinstances ) if ( @exportinstances )
			env.add( 'exportdata', @exportdata ) if ( @exportdata )
		end

		def to_xml( )
			node = super( )
			node.attributes['prefix'] = @prefix
			node.attributes['path'] = @path
			node.attributes['definitions'] = @exportdefinitions.to_s
			node.attributes['instances'] = @exportinstances.to_s
			node.attributes['data'] = @exportdata.to_s
			node.attributes['subtype'] = @subtype.to_s
			node.attributes['pedantic'] = @pedantic.to_s
			node
		end
		
		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
   
		#
		# Parse an XML node to create our Map content tree node.  This
		# automatically creates upto 3 File input nodes depending on what the
		# map will export (IDE, IPL and IMG files).
		#
		def Map::from_xml( xml_node, path, env, target )
		
			name = env.subst( xml_node.attributes['name'] )
			path = OS::Path.combine( path, xml_node.attributes['path'] )
			defs = ( 'true' == xml_node.attributes['definitions'] ) ? true : false
			inst = ( 'true' == xml_node.attributes['instances'] ) ? true : false
			data = ( 'false' == xml_node.attributes['data'] ) ? false : true
			pedantic = ( 'false' == xml_node.attributes['pedantic'] ) ? false : true
			
			# Object name prefix (default empty string)
			prefix = '' 
			prefix = xml_node.attributes['prefix'] unless ( xml_node.attributes['prefix'].nil? )
		
			# Map subtype 
			maptype = :unknown
			if ( not xml_node.attributes['maptype'].nil? ) then
				maptype_str = xml_node.attributes['maptype']
				case maptype_str
				when 'container'
					maptype = :container
				when 'container_props'
					maptype = :container_props
				when 'container_lod'
					maptype = :container_lod
				when 'container_occl'
					maptype = :container_occl
				when 'props'
					maptype = :props
				when 'interior'
					maptype = :interior
				else
					maptype = :unknown
				end
			end
		
			Map::new( name, path, prefix, defs, inst, data, pedantic, maptype )
		end
	end
	
	#
	# == Description
	# Map export ZIP file content node; there is a one-to-one mapping of MapZip
	# and Map content nodes.
	#
	class MapZip < Zip
		include SupportChildren
		
		attr_reader :viaboundsprocessor
		
		XML_CONTENT_TYPE = 'mapzip'
		
		#-----------------------------------------------------------------
		# Methods
		#-----------------------------------------------------------------
		def initialize( name, path, target, viaboundsprocessor )			
			super( name, path, 'zip', target, true )
			init_SupportChildren( )
			@xml_type = XML_CONTENT_TYPE
			@viaboundsprocessor = viaboundsprocessor
		end
		
		# Post-load inputs; after inputs are resolved we validate them.
		def post_load_input( )			
			# SceneXml file.
			add_child( MapSceneXml::new( @name, @path, @target ) )
		end
		
		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def MapZip::from_xml( xml_node, path, env, target )
			
			name = env.subst( xml_node.attributes['name'] )
			path = OS::Path::combine( path, xml_node.attributes['path'] )
			viaboundsprocessor = ( 'true' == xml_node.attributes['viaboundsprocessor'] ) ? true : false
			
			MapZip::new( name, path, target, viaboundsprocessor )
		end
	end
	
	#
	# == Description
	# Map SceneXml file content node.  These are implicitly created by tghe 
	# MapZip nodes and do not need to appear in the content XML.
	#
	class MapSceneXml < Target
		XML_CONTENT_TYPE = 'mapxml'
		
		def initialize( name, path, target )
			super( name, path, 'xml', target )
			@xml_type = XML_CONTENT_TYPE
		end
		
		#--------------------------------------------------------------------
		# Class Methods
		#--------------------------------------------------------------------
		def MapSceneXml::from_xml( xml_node, path, env, target )
			name = env.subst( xml_node.attributes['name'] )
			path = OS::Path::combine( path, xml_node.attributes['path'] )
			MapSceneXml::new( name, path, target )
		end
	end
	
	
	#
	# == Description
	# Map export ZIP file content node; there is a one-to-one mapping of MapZip
	# and Map content nodes.
	#
	class MapCombineZip < Target
		XML_CONTENT_TYPE = 'mapcombinezip'
		
		#-----------------------------------------------------------------
		# Methods
		#-----------------------------------------------------------------
		def initialize( name, path, target )			
			super( name, path, 'zip', target )
			@xml_type = XML_CONTENT_TYPE
            @compress = true
		end
		
		# Post-load inputs; after inputs are resolved we validate them.
		def post_load_input( )
		end
		
		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def MapCombineZip::from_xml( xml_node, path, env, target )
			
			name = env.subst( xml_node.attributes['name'] )
			path = OS::Path::combine( path, xml_node.attributes['path'] )
			
			MapCombineZip::new( name, path, target )
		end
	end
	
	#
	# == Description
	# Processed Map ZIP file; through our new pipeline stage.  There is a many-
	# to-one mapping of MapZip to ProcessedMapZip content nodes.
	#
	class ProcessedMapZip < Target
		include SupportChildren
		XML_CONTENT_TYPE = 'processedmapzip'
		attr_reader :exportdefinitions
		attr_reader :exportinstances
		
		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def initialize( name, path, target )
			super( name, path, 'zip', target )
			init_SupportChildren( )
			@xml_type = XML_CONTENT_TYPE
		end
		
		# Post-load inputs; after inputs are resolved we validate them.
		def post_load_input( )
			throw RuntimeError.new( "ProcessedMapZip has no inputs." ) \
				unless ( @inputs.is_a?( Array ) and @inputs.size > 0 )
			@inputs.each do |input|
				throw RuntimeError.new( "ProcessedMapZip '#{@name}' has invalid input '#{input.name}' (not MapZip)." ) \
					unless ( input.is_a?( Content::MapZip ) )

				@exportdefinitions = true if ( input.inputs[0].exportdefinitions )
				@exportinstances = true if ( input.inputs[0].exportinstances )
			end
		end
		
		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def ProcessedMapZip::from_xml( xml_node, path, env, target )
			
			name = env.subst( xml_node.attributes['name'] )
			path = OS::Path.combine( path, xml_node.attributes['path'] )
			
			ProcessedMapZip::new( name, path, target )
		end
	end
	
end # Content module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/content/content_maps.rb
