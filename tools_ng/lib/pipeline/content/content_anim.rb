#
# File:: content_anim.rb
# Description:: Content system anim content tree node class implementation.
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 26 June 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/content/treecore'
require 'pipeline/content/content_core'
require 'rexml/document'
include REXML

module Pipeline
module Content

	#
	# == Description
	# Processed Anim directory class; through our new pipeline stage.  
	#
	#
	class ProcessedAnimationDir < Directory
		include SupportChildren
		XML_CONTENT_TYPE = 'processedanimdir'
		
		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def initialize( name, path, target )
			super( name, path, target )
			init_SupportChildren( )
			@xml_type = XML_CONTENT_TYPE
		end

		# Post-load inputs; after inputs are resolved we validate them.
		def post_load_input( )
			throw RuntimeError.new( "ProcessedAnimationDir has no inputs." ) \
				unless ( @inputs.is_a?( Array ) and @inputs.size > 0 )
		end
		
		def inputs( )
			@inputs
            	end
            	
            	# Because this content sits in the intermediate portion of the pipeline
            	# we need to set it's inputs as the node from output.xml on the way in
            	# and then reform the inputs to the files that will be passed ot the rage
            	# converter.
            	def reform_inputs()
            		@inputs.clear
            		files = OS::FindEx::find_files( OS::Path::combine( @absolute_path, '*.*' ) )
			files.each do |filename|
				add_input( Content::File::from_filename( filename ) )
			end
			Content::Parser::log( ).warn( "No inputs for directory node: #{self}." ) \
			unless ( @inputs.size > 0 )
			@inputs_processed = true
            	end
            	
            	# Explicitly reform the inputs.  For preview
            	def reform_inputs_from( file_list )
            		@inputs.clear
            		file_list.each do | file |
            			puts "ADDING INPUT: #{file}"
            			add_input( Content::File::from_filename( file ) )
            		end
            		Content::Parser::log( ).warn( "No inputs for directory node: #{self}." ) \
			unless ( @inputs.size > 0 )
			@inputs_processed = true
            	end

		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def ProcessedAnimationDir::from_xml( xml_node, path, env, target )

			name = env.subst( xml_node.attributes['name'] )
			path = OS::Path.combine( path, xml_node.attributes['path'] )

			ProcessedAnimationDir::new( name, path, target )
		end
	end

	#
	# == Description
	# Processed Anim ZIP file; through our new pipeline stage.  
	#
	#
	class ProcessedAnimationZip < Target
		include SupportChildren
		XML_CONTENT_TYPE = 'processedanimzip'
		
		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def initialize( name, path, target )
			super( name, path, 'zip', target )
			init_SupportChildren( )
			@xml_type = XML_CONTENT_TYPE
		end

		# Post-load inputs; after inputs are resolved we validate them.
		def post_load_input( )
			throw RuntimeError.new( "ProcessedAnimatopmZip has no inputs." ) \
				unless ( @inputs.is_a?( Array ) and @inputs.size > 0 )
		end

		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def ProcessedAnimationZip::from_xml( xml_node, path, env, target )

			name = env.subst( xml_node.attributes['name'] )
			path = OS::Path.combine( path, xml_node.attributes['path'] )

			ProcessedAnimationZip::new( name, path, target )
		end
	end

	#
	# == Description
	# Networks ZIP file;  A zip file containing textual network data (IMVF)
	#
	#
	class NetworksZip < Target
		include SupportChildren
		XML_CONTENT_TYPE = 'networkszip'
		
		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def initialize( name, path, target )
			super( name, path, 'zip', target )
			init_SupportChildren( )
			@xml_type = XML_CONTENT_TYPE
		end

		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def NetworksZip::from_xml( xml_node, path, env, target )

			name = env.subst( xml_node.attributes['name'] )
			path = OS::Path.combine( path, xml_node.attributes['path'] )

			NetworksZip::new( name, path, target )
		end
	end

	#
	# == Description
	# Processed Networks ZIP file;  A zip file containing game-ready network data (MRF)
	#
	#
	class ProcessedNetworksZip < Target
		include SupportChildren
		XML_CONTENT_TYPE = 'processednetworkszip'
		
		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def initialize( name, path, target )
			super( name, path, 'zip', target )
			init_SupportChildren( )
			@xml_type = XML_CONTENT_TYPE
		end

		# Post-load inputs; after inputs are resolved we validate them.
		def post_load_input( )
			throw RuntimeError.new( "ProcessedNetworksZip has no inputs." ) \
				unless ( @inputs.is_a?( Array ) and @inputs.size > 0 )
		end

		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def ProcessedNetworksZip::from_xml( xml_node, path, env, target )

			name = env.subst( xml_node.attributes['name'] )
			path = OS::Path.combine( path, xml_node.attributes['path'] )

			ProcessedNetworksZip::new( name, path, target )
		end
	end

    #
	# == Description
	# Represents a group of anim dictionarys
	# 
	#
    class AnimGroup < Group
        attr_reader :p
        attr_reader :asset_list
        attr_reader :weld_name
        attr_reader :stream_src

        def initialize( srcdir, weldname, project, streamsrc )
            super( weldname )

            @p = project
            @asset_list = []
            @weld_name = weldname
            @stream_src = streamsrc

            populate_lists( srcdir )

        end
        
        def populate_lists( srcdir )     
             @asset_list = OS::FindEx.find_files( srcdir + "/*.anim" )
        end

        def build()
            pack_content = RPF.new( @weld_name, @stream_src, "iad", @p.ind_target )
            @asset_list.each do | file | 
                pack_content.add_input( File.new( OS::Path.get_basename( file ), OS::Path.get_directory( file ), OS::Path.get_extension( file ) ) ) if ::File.exist?( file ) 
            end     
            add_child( pack_content )
        end
    end
    
    
    #
	# == Description
	# Represents a single animation dictionary
	# 
	#
    class AnimDict < RPF
       
        
        XML_CONTENT_TYPE = 'animdict'
        
        def initialize( name, path, target )
            weldname = name.replace_c('/', '')
            super( weldname, path, "iad", target )
			@xml_type = XML_CONTENT_TYPE
            
        end
    
        
    
    end
    

    
end # Content module
end # Pipeline module

# End of content_anim.rb
