#
# File:: pipeline/content/content_vehicles.rb
# Description:: Custom vehicles content system nodes.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 4 August 2008 
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/content/treecore'
require 'pipeline/content/content_core'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Content

    #
    # == Description
    # Vehicle directory; we have some additional properties for the max ui.
    #
    class VehiclesDirectory < Directory
        XML_CONTENT_TYPE = 'vehiclesdirectory'
		attr_reader :sharedtxd
		attr_reader :sharedtxdinput
		attr_reader :friendly        
    
        def initialize( name, path, target, sharedtxd, sharedtxdinput, friendly )
            super( name, path, target )
            @xml_type = XML_CONTENT_TYPE   
			@sharedtxd = sharedtxd
			@sharedtxdinput = sharedtxdinput
			@friendly = friendly         
        end
		
		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		
		#
		# Parse an XML node for the weapon image file representation.
		#
		def VehiclesDirectory::from_xml( node, path, env, target )
			
			name = env.subst( node.attributes['name'] )
			sharedtxd = env.subst( node.attributes['sharedtxd'].nil? ? '' : node.attributes['sharedtxd'] )
			sharedtxdinput = env.subst( node.attributes['sharedtxdinput'].nil? ? '' : node.attributes['sharedtxdinput'] )
			friendly = env.subst( node.attributes['friendlyname'].nil? ? '' : node.attributes['friendlyname'] )
			VehiclesDirectory::new( name, path, target, sharedtxd, sharedtxdinput, friendly )            
		end
    end

	#
	# == Description
	# Vehicles image.  Simple type so we can find it easily in our exporter.
	#
	class VehiclesImage < Zip
		attr_reader :sharedtxd
		attr_reader :sharedtxdinput
		attr_reader :friendly
		XML_CONTENT_TYPE = 'vehicleszip'
		
		def initialize( name, path, target, sharedtxd, sharedtxdinput, friendly )
			super( name, path, 'zip', target )
			@xml_type = XML_CONTENT_TYPE
			@sharedtxd = sharedtxd
			@sharedtxdinput = sharedtxdinput
			@friendly = friendly
		end
		
		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		
		#
		# Parse an XML node for the weapon image file representation.
		#
		def VehiclesImage::from_xml( node, path, env, target )
			
			name = env.subst( node.attributes['name'] )
			sharedtxd = env.subst( node.attributes['sharedtxd'].nil? ? '' : node.attributes['sharedtxd'] )
			sharedtxdinput = env.subst( node.attributes['sharedtxdinput'].nil? ? '' : node.attributes['sharedtxdinput'] )
			friendly = env.subst( node.attributes['friendlyname'].nil? ? '' : node.attributes['friendlyname'] )
			image = VehiclesImage.new( name, path, target, sharedtxd, sharedtxdinput, friendly )            
			image
		end
	end
	
	
	#
	# == Description
	# Map export ZIP file content node; there is a one-to-one mapping of VehicleModZip
	# and Map content nodes.
	#
	class VehicleModZip < Zip
		include SupportChildren
		
		XML_CONTENT_TYPE = 'vehiclemodzip'
		
		#-----------------------------------------------------------------
		# Methods
		#-----------------------------------------------------------------
		def initialize( name, path, target )			
			super( name, path, 'zip', target, true )
			init_SupportChildren( )
			@xml_type = XML_CONTENT_TYPE
		end
		
		# Post-load inputs; after inputs are resolved we validate them.
		def post_load_input( )			
			# SceneXml file.
			add_child( MapSceneXml::new( @name, @path, @target ) )
		end
		
		#-----------------------------------------------------------------
		# Class Methods
		#-----------------------------------------------------------------
		def VehicleModZip::from_xml( xml_node, path, env, target )
			
			name = env.subst( xml_node.attributes['name'] )
			path = OS::Path::combine( path, xml_node.attributes['path'] )
			
			VehicleModZip::new( name, path, target )
		end
	end

end # Content module
end # Pipeline module

# pipeline/content/content_vehicles.rb
