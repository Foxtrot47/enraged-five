#
# File:: %RS_TOOLSLIB%/pipeline/content/content_core.rb
# Description:: Content system core content tree node class implementation.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 12 June 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/content/treecore'
require 'pipeline/os/path'
require 'rexml/document'
include REXML

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Content

	#
	# == Description
	# Function to create Content tree nodes based on a filename.  The project
	# and branch need to be specified as all Target-based content nodes require
	# this information to determine the correct target object.
	#
	# This function will return a File, Target, RPF or Image content node.
	#
	def Content::from_filename( project, branch_name, filename )
	
		ext = OS::Path::get_extension( filename )
		case ext
		when 'zip'
			return Zip::from_filename( project, branch_name, filename )
		when 'rpf'
			# Uncompressed
			return RPF::from_filename( project, branch_name, filename, true, false )
		when 'ibd', 'idd', 'idr', 'itd', 'ipt'
			return RPF::from_filename( project, branch_name, filename )
		else
			# Determine if we are a regular file or a target file.
			# Try independent platform
			project.branches[branch_name].ind_target.in_env() do |env|
				next unless ( filename.starts_with( env.subst( '$(export)' ) ) )
                   
                ind_target = project.branches[branch_name].ind_target
                if ( ::File::file?( filename ) ) then
					name = OS::Path::get_basename( filename )
					path = OS::Path::get_directory( filename )
					ext = OS::Path::get_extension( filename )
					return Target::new( name, path, ext, ind_target )
				elsif ( ::File::directory?( filename ) ) then
                    name = OS::Path::get_basename( filename )
                    path = OS::Path::get_directory( filename )
                    return Directory::new( name, path, ind_target )
                end
			end
			
			# Try non-independent platforms			
			project.branches[branch_name].targets.each_pair do |platform, target|
				target_dir = '$(target)'
				target.in_env() do |env|
					target_dir = env.subst( target_dir )
				end
				match = true
				filename_parts = OS::Path::get_parts( filename )
				target_parts = OS::Path::get_parts( target_dir )
				target_parts.each_with_index do |part, index|
					if ( 0 != part.casecmp( filename_parts[index] ) ) then
						match = false
						break
					end
				end				
				next unless ( match )
				
				name = OS::Path::get_basename( filename )
				path = OS::Path::get_directory( filename )
				ext = OS::Path::get_extension( filename )
				return Target.new( name, path, ext, target )
			end
	
			# Otherwise we just create a regular File content node
			return Content::File::from_filename( filename )
		end
	end

	#
	# == Description
	# The basic File content type.  Such nodes are often the inputs for most
	# other content nodes.  If the file has a specific platform target then
	# the Target class should be used instead.
	#
	# Read and write access is available for the :path and :extension members
	# so our content tree can be updated by the conversion process later.
	#
	class File < Base        
		attr_reader :path
		attr_writer :path
		attr_reader :extension
		
		XML_CONTENT_TYPE = 'file'
		
		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		def initialize( name, path, ext )
					
			raise Exception.new("no extension for target file #{name} #{path}") if ext == nil
					
			super( name )
			@xml_type = XML_CONTENT_TYPE
			@path = ( nil == path ) ? '' : path
			@extension = ext
		end
		
		def extension=(ext)
			raise Exception.new("no extension for target file #{@name} #{@path}") if ext == nil
			@extension = ext
		end

		#
		# A file asset is outdated if it doesn't exist on disk or any of its
		# inputs are outdated or more recent than self.
		#
		def outdated?( env )
			full_filename = env.subst( filename )
			self_mtime = ::File.mtime( full_filename ) if ::File.exists?( full_filename )
			return true unless ::File.exists?( full_filename )
			
			@inputs.each do |input|
				# Check if input is outdated
				return true if input.outdated?( env )
			
				if ( input.is_a?( File ) ) then                 
					input_filename = env.subst( input.filename )
					input_mtime = ::File.mtime( input_filename ) if ::File.exists?( input_filename )
					#return true unless ::File.exists?( input_filename )
				
					# Check if input is more recent than self
					#puts "CHECK MTIME #{input_mtime} #{self_mtime} #{input_mtime <=> self_mtime}"
					case ( input_mtime <=> self_mtime )
						when -1, 0
							return false
						when 1
							return true
					end
				elsif ( input.is_a?( Group ) ) then
				
					return false
				elsif ( input.is_a?( Unresolved ) ) then
				
					return false
				else            
					return false
				end
			end
			false
		end

		def to_s( )
			"#{super()} path:#{@path} ext:#{@extension}"
		end

		def filename( env = nil )
			"#{OS::Path.combine( @path, @name )}.#{@extension}"
		end
		
		def fill_env( env )
			super( env )
			env.add( 'path', @path ) unless ( nil == @path )
			env.add( 'extension', @extension )
		end
		
		def to_xml( )
			node = super( )
			node.attributes['path'] = @path
			node.attributes['extension'] = @extension
			node
		end
		
		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		def File::from_xml( xml_node, path, env, target )
						
			name = env.subst( xml_node.attributes['name'] )
			extension = env.subst( xml_node.attributes['extension'] )
			file = File.new( name, path, extension )
			
			return ( file )
		end 
		
		def File::from_filename( filename )
		
			name = OS::Path::get_basename( filename )
			path = OS::Path::get_directory( filename )
			ext = OS::Path::get_extension( filename )
			File.new( name, path, ext )
		end
	end
  
	#
	# == Description
	# Target content node.
	#
	class Target < File
	
		attr_reader :target
		
		XML_CONTENT_TYPE = 'target'

		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		def initialize( name, path, ext, target )
			super( name, path, ext )
			@xml_type = XML_CONTENT_TYPE
			@target = target
		end

		#
		# A target asset is outdated if any of its inputs are outdated or more 
		# recent than self.
		#
		def outdated?( env )        
			full_filename = env.subst( filename )
			self_mtime = ::File.mtime( full_filename ) if ::File.exists?( full_filename )
			return true unless ::File.exists?( full_filename )
			
			@inputs.each do |input|
				# Check if input is outdated
				return true if input.outdated?( env )
			
				if ( input.is_a?( File ) ) then                 
					input_filename = env.subst( input.filename )
					input_mtime = ::File.mtime( input_filename ) if ::File.exists?( input_filename )
					return true unless ::File.exists?( input_filename )
				
					# Check if input is more recent than self
					#puts "CHECK MTIME #{input_mtime} #{self_mtime} #{input_mtime <=> self_mtime}"
					case ( input_mtime <=> self_mtime )
						when -1, 0
							return false
						when 1
							return true
					end
				elsif ( input.is_a?( Group ) ) then
				
					return false
				elsif ( input.is_a?( Unresolved ) ) then
				
					return false
				else            
					return false
				end
			end
			false
		end
		
		def to_s( )
					
			if @target == nil then
				"#{super()} target:nil"
			else
				"#{super()} target:#{@target.platform}"
			end
		end

		def filename( )
			@target.in_env do |env|
				filepath = super()
				return env.subst( filepath )
			end
		end

		def fill_env( env )
			super( env )
			env.add( 'path', @path )
		end
		 
		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		def Target.from_xml( xml_node, path, env, target )
						
			name = env.subst( xml_node.attributes['name'] )
			extension = env.subst( xml_node.attributes['extension'] )
						
			raise Exception.new( "No extension for target file #{name} #{extension} #{env} #{xml_node}" ) \
				if ( extension.nil? )
			
			target = Target.new( name, path, extension, target )
			
			return ( target )
		end
		
		# Create new Target node.
		def Target::from_filename( filename, target )
		
			name = OS::Path::get_basename( filename )
			path = OS::Path::get_directory( filename )
			ext = OS::Path::get_extension( filename )
			Target::new( name, path, ext, target )
		end
	end 

	#
	# == Description
	# Content node representing a generic PKWare-ZIP file.
	#
	class Zip < Target
		XML_CONTENT_TYPE = 'zip'
		attr_reader :compress
		
		def initialize( name, path, ext, target, compress = true )
			super( name, path, ext, target )
			@xml_type = XML_CONTENT_TYPE
			@compress = compress
		end
		
		def to_s( )
			"#{super()} compress:#{@compress.to_s}"
		end
		
		def fill_env( env )
			super( env )
			env.add( 'compress', @compress )
		end
		
		def to_xml( )
			node = super( )
			node.attributes['compress'] = @compress.to_s
			node
		end
		
		#--------------------------------------------------------------------------
		# Class Methods
		#--------------------------------------------------------------------------
		
		#
		#
		#
		def Zip::from_xml( xml_node, path, env, target )
			
			name = env.subst( xml_node.attributes['name'] )
			extension = env.subst( xml_node.attributes['extension'] ) unless ( xml_node.attributes['extension'].nil? )
			compress = ( 'false' == xml_node.attributes['compress'] ) ? false : true
			
		extension = 'zip' if ( extension.nil? )
			Zip.new( name, path, extension, target, compress )
		end		
		
		#
		# Create an Zip content node from a filename with a target.
		#
		def Zip::from_filename_and_target( filename, target, compress = true )
			# Normalise filename before attempting to to string comparisons.
			filename = OS::Path::normalise( filename )
			
			name = OS::Path::get_basename( filename )
			path = OS::Path::get_directory( filename )
			ext = OS::Path::get_extension( filename )
			
			Zip::new( name, path, ext, target, compress )
		end
		
		#
		# Create a Zip content node from a filename.
		#
		def Zip::from_filename( project, branch_name, filename, compress = true )
			throw ArgumentError.new( "Project does not have branch called #{branch_name}." ) \
				unless ( project.branches.has_key?( branch_name ) )
			
			# Normalise filename before attempting to to string comparisons.
			filename = OS::Path::normalise( filename )
			
			# Try independent platform
			branch = project.branches[branch_name]
			branch.ind_target.in_env() do |env|
				if ( branch.is_export_file?( filename ) or 
				     branch.is_processed_file?( filename ) ) then
					name = OS::Path::get_basename( filename )
					path = OS::Path::get_directory( filename )
					ext = OS::Path::get_extension( filename )
					return Zip::new( name, path, ext, branch.ind_target, compress )
				end
			end
			
			# Try non-independent platforms
			branch.targets.each_pair do |platform, target|
				target_dir = '$(target)'
				target.in_env() do |env|				
					target_dir = env.subst( target_dir )
				end
				
				match = true
				filename_parts = OS::Path::get_parts( filename )
				target_parts = OS::Path::get_parts( target_dir )
				target_parts.each_with_index do |part, index|
					if ( 0 != part.casecmp( filename_parts[index] ) ) then
						match = false
						break
					end
				end				
				next unless ( match )
				
				name = OS::Path::get_basename( filename )
				path = OS::Path::get_directory( filename )
				ext = OS::Path::get_extension( filename )
				return Zip::new( name, path, ext, target, compress )			
			end
			
			name = OS::Path::get_basename( filename )
			path = OS::Path::get_directory( filename )
			ext = OS::Path::get_extension( filename )
			return Zip::new( name, path, ext, branch.ind_target, compress )
		end		
	end
	
	
	#
	# == Description
	# RPF content node.
	#
	class RPF < Target
		attr_reader :stack_path
		attr_reader :compress		# Default: true, compress RPF content.
		
		XML_CONTENT_TYPE = 'rpf'
		
		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		def initialize( name, path, ext, target, stack_path = true, compress = true )
			super( name, path, ext, target )
			
			@stack_path = stack_path    # Allows us to pass groups to the pack file generator without it automatically creating a directory hierarchy in the packfile
			@compress = compress
			@xml_type = XML_CONTENT_TYPE
		end
		
		def to_s()
			"#{super} stackpath:#{@stack_path} compress:#{@compress}"
		end
		
		def to_xml( )
			node = super( )
			node.attributes['compress'] = @compress.to_s
			node.attributes['stack_path'] = @stack_path.to_s
			node
		end
		
		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		
		#
		#
		#
		def RPF::from_xml( xml_node, path, env, target )
		
			name = env.subst( xml_node.attributes['name'] )
			extension = env.subst( xml_node.attributes['extension'] ) unless ( xml_node.attributes['extension'].nil? )
			stackpath = ( 'false' ==  xml_node.attributes["stackpath"] ) ? false : true
			compress = ( 'false' == xml_node.attributes['compress'] ) ? false : true
						
			stackpath = true if ( stackpath.nil? )
			extension = 'rpf' if ( extension.nil? )
			rpf = RPF.new( name, path, extension, target, stackpath, compress )
					
			rpf
		end

		#
		# Create an RPF content node from a filename with a target.
		#
		def RPF::from_filename_and_target( filename, target, stack_path = true, compress = true )
			# Normalise filename before attempting to to string comparisons.
			filename = OS::Path::normalise( filename )
			
			name = OS::Path::get_basename( filename )
			path = OS::Path::get_directory( filename )
			ext = OS::Path::get_extension( filename )
			
			RPF::new( name, path, ext, target, stack_path, compress )
		end
		
		#
		# Create a RPF content node from a filename.
		#
		def RPF::from_filename( project, branch_name, filename, stack_path = true, compress = true )
			throw ArgumentError.new( "Project does not have branch called #{branch_name}." ) \
				unless ( project.branches.has_key?( branch_name ) )
			
			# Normalise filename before attempting to to string comparisons.
			filename = OS::Path::normalise( filename )
			branch = project.branches[branch_name]
			
			# Try export and processed platform
			if ( branch.is_export_file?( filename ) or branch.is_processed_file?( filename ) ) then
				name = OS::Path::get_basename( filename )
				path = OS::Path::get_directory( filename )
				ext = OS::Path::get_extension( filename )
				return ( RPF::new( name, path, ext, branch.ind_target, stack_path, compress ) )
			end
			
			# Try non-independent platforms
			project.branches[branch_name].targets.each_pair do |platform, target|
				target_dir = '$(target)'
				target.in_env() do |env|				
					target_dir = env.subst( target_dir )
				end
				
				match = true
				filename_parts = OS::Path::get_parts( filename )
				target_parts = OS::Path::get_parts( target_dir )
				target_parts.each_with_index do |part, index|
					if ( 0 != part.casecmp( filename_parts[index] ) ) then
						match = false
						break
					end
				end				
				next unless ( match )
				
				name = OS::Path::get_basename( filename )
				path = OS::Path::get_directory( filename )
				ext = OS::Path::get_extension( filename )
				return RPF.new( name, path, ext, target , stack_path, compress )
			end
			
			throw RuntimeError.new( "No Content::RPF created." )
		end
	end
	
    #
    # == Description
    # Directory content-node.
    #
    class Directory < Base 
		attr_reader :path
	    attr_reader :target
        XML_CONTENT_TYPE = 'directory'
		
		#---------------------------------------------------------------------
		# Instance Methods
		#---------------------------------------------------------------------
		def initialize( name, path, target )
			super( name )
            @xml_type = XML_CONTENT_TYPE
			@path = path.nil? ? '' : path
            target.in_env do |e|
                @absolute_path = e.subst( OS::Path::combine( path, name ) )
		    end
            @target = target
            @inputs_processed = false
        end

        # Override the Base::inputs method; we fstat our directory contents
        # and cache them when the inputs are first read.  This means we're
        # not hitting the disk on content-tree load.
        def inputs( )
            return @inputs if ( @inputs_processed )
            
            files = OS::FindEx::find_files( OS::Path::combine( @absolute_path, '*.*' ) )
            files.each do |filename|
                add_input( Content::File::from_filename( filename ) )
            end
            Content::Parser::log( ).warn( "No inputs for directory node: #{self}." ) \
                unless ( @inputs.size > 0 )
            @inputs_processed = true
            @inputs
        end

        def path( )
			@target.in_env do |env|
				env.subst( @path )
			end
        end
        
        def absolute_path( )
            @target.in_env do |env|
                env.subst( @absolute_path )
            end
        end

		def to_s( )
			"#{super()} path:#{@path}"
		end

		def fill_env( env )
			super( env )
			env.add( 'path', @path ) unless ( @path.nil? )
		end
		
		def to_xml( )
			node = super( )
			node.attributes['path'] = @path
			node
		end
		
		# Post-load inputs; after inputs are resolved we validate them.
        ## DHM No longer need to do this as inputs are determined when first
        ##     read rather than at content-tree load.
		def post_load_input( )			
			# Stat directory for inputs; not really sure this is the
            # right place to do this because post-load might not have any
            # files... eeeeek..... DHM FIX ME
           
           # files = OS::FindEx::find_files( OS::Path::combine( absolute_path, '*.*' ) )
           # files.each do |filename|
           #     puts "\tAdd Input: #{filename}"
           #     add_input( Content::File::from_filename( filename ) )
           # end
           # Content::Parser::log( ).warn( "No inputs for directory node: #{self}." ) \
           #     unless ( @inputs.size > 0 )
		end	
  
		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		def Directory::from_xml( xml_node, path, env, target )
        	name = env.subst( xml_node.attributes['name'] )
			Directory::new( name, path, target )
		end
    end

end # Content module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/content/content_core.rb
