#
# File:: content_expression.rb
# Description:: Content system for expression dictionary management.
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 14 September 2009
# Modified:: Mike Wilson <mike.wilson@rockstartoronto.com>
# Date:: 8 June 2011

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------


module Pipeline
	module Content
		class ExpressionGroup < Group
			
			 attr_reader :p
			 attr_reader :asset_list
			 attr_reader :stream_src
			 attr_reader :dictname
			
			
			def initialize( srcdir, dictname, project, stream_src )
				super( dictname )
                
				@p = project
				@asset_list = []
				@stream_src = stream_src
				@dictname = dictname
				
				populate_lists( srcdir )
			end
				
			def populate_lists( srcdir ) 
				print srcdir
				@asset_list = OS::FindEx.find_files( srcdir + "/*.expr" )
			end
				
			def build()
				 pack_content = Zip::new( @dictname, @stream_src, "ied.zip", @p.ind_target, false )
				 @asset_list.each do | file | 
					 pack_content.add_input( File.new( OS::Path.get_basename( file ), OS::Path.get_directory( file ), OS::Path.get_extension( file ) ) ) if ::File.exist?( file ) 
				 end     
				 add_child( pack_content )
			end
			
		end
	
	end	
end