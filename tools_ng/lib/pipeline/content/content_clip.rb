#
# File:: content_clip.rb
# Description:: Content system clip content tree node class implementation for a given clip.
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 16 October 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/content/content_core'
require 'rexml/document'
include REXML

module Pipeline
	module Content
        
        #
        #
        #
        class ClipGroup < Group
            attr_reader :p
            attr_reader :asset_list
            attr_reader :weld_name
            attr_reader :stream_src
			attr_reader :sectioned
            
            def initialize( srcdir, destdir, weldname, project, streamsrc, sectionidx=-1 )
                super( weldname )
                
                @p = project
                @asset_list = []
                @weld_name = weldname
                @stream_src = streamsrc
				@dest_dir = destdir.gsub( "\\", "/" )
				@sectionidx = sectionidx
                
				
                populate_lists( srcdir, destdir )
				
            end
			
            
            def populate_lists( srcdir, destdir )
				# If we are dealing with sectioned data then only look for anims and clips
				# for the section index provided
				if @sectionidx != -1 then
					anim_list = OS::FindEx.find_files( srcdir + "/*-#{@sectionidx}.anim" )
					clip_list = OS::FindEx.find_files( srcdir + "/*-#{@sectionidx}.clip" )
				else
					anim_list = OS::FindEx.find_files( srcdir + "/*.anim" )
					clip_list = OS::FindEx.find_files( srcdir + "/*.clip" )
				end
				@asset_list = anim_list
				@asset_list = @asset_list + clip_list
            end
            
            def build()
                pack_content = Zip::new( @weld_name, @stream_src, "icd.zip", @p.ind_target, false )
                @asset_list.each do | file | 
                    pack_content.add_input( File.new( OS::Path.get_basename( file ), OS::Path.get_directory( file ), OS::Path.get_extension( file ) ) ) if ::File.exist?( file ) 
                end     
                add_child( pack_content )
            end
        end
		
	    #
		# == Description
		# Represents a single clip dictionary
		# 
		#
		class ClipDict < Zip
			
			
			XML_CONTENT_TYPE = 'clipdict'
			
			def initialize( name, path, target )
				weldname = name.replace_c('/', '')
				super( weldname, path, "icd.zip", target )
				@xml_type = XML_CONTENT_TYPE
				
			end
			
		end
        
    end # module pipeline
end # module content
