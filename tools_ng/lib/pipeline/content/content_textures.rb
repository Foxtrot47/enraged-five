#
# File:: content_textures.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 5 August 2008
#

module Pipeline
module Content

	#
	# == Description
	# DDS texture file.
	#
	class Texture < File
		XML_CONTENT_TYPE = 'dds'
		
		def initialize( name, path )
			super( name, path, 'dds' )
			@xml_type = XML_CONTENT_TYPE
		end
				
		def Texture::from_xml( xml_node, path, env, target )
			name = env.subst( xml_node.attributes['name'] )
			file = File.new( name, path, 'dds' )
			
			return ( file )
		end
	end

	#
	# == Description
	# Shared texture list file content node.  This is a non-target XML file
	# that can be edited by Max.
	#
	class SharedTextureList < Base
		XML_CONTENT_TYPE = 'texturelist'
		attr_reader :path
		
		def initialize( name, path )
			super( name )
			@xml_type = XML_CONTENT_TYPE
			@path = path			
		end
				
		def to_s( )
			"#{super} path:#{@path}"
		end
		
		def fill_env( env )
			super( env )
			env.add( 'path', @path )
		end
		
		def to_xml( )
			node = super( )
			node.attributes['path'] = @path
			node
		end
		
		def SharedTextureList.from_xml( xml_node, path, env, target )
		
			name = env.subst( xml_node.attributes['name'] )
			path = OS::Path.combine( path, xml_node.attributes['path'] )
						
			SharedTextureList.new( name, path )
		end
	end

end # Content module
end # Pipeline module

# content_textures.rb
