#
# File:: %RS_TOOLSLIB%/pipeline/util/time.rb
# Description:: Time functions, timing code execution etc.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 15 March 2011
#

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------
module Pipeline
module Util
	
	#
	# == Description
	# Function to return the elapsed time (in seconds) of the specified code
	# block.
	#
	# == Example Usage
	#
	#    duration = Util::time() do
	#       ... code here ...
	#    end
	#    puts "Elapsed time: #{duration} seconds."
	#
	def Util::time( &block )
		
		start = Time::now( )
		yield if ( block_given? )
		Time::now() - start
	end
	
end # Util module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/util/time.rb
