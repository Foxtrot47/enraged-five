def ParamCToRuby ctype

	case ctype
	
		when "bool"
			"i"
		when "s32"
			"i"
		when "int"
			"i"
		when "char*"
			"p"
		when "float"
			"n"
		when "void"
			"v"
	
	end
end

def ConvertFile inputfile

	File.open(inputfile) { |aFile|
	
		aFile.each_line { |aLine|
	
				if aLine =~ /DLLEXPORT/ then
	
					aLineTokens = aLine.split(/[()]/)
					aLineTokensCalling = aLineTokens[0].split(" ")
					aLineTokensParameters = aLineTokens[1].split(",")
					
					functionName = aLineTokensCalling[aLineTokensCalling.size - 1]
					functionRubyName = functionName[2...3].downcase! + functionName[3...functionName.size]
					
					retValue = aLineTokensCalling[1] if aLineTokensCalling[1] != "const"
					retValue = aLineTokensCalling[2] if aLineTokensCalling[1] == "const"
					
					retValue = ParamCToRuby retValue
				
					aLineTokensParameters.collect! { |aParam|
					
						aParamTokens = aParam.split(" ")
						
						inValue = aParamTokens[0] if aParamTokens[0] != "const"
						inValue = aParamTokens[1] if aParamTokens[0] == "const"
						
						ParamCToRuby inValue
					}
				
					print("@")
					print(functionRubyName)
					print(" = Win32API.new(\"ragebuilder\",\"")
					print(functionName)
					print("\",[")
					
					if aLineTokensParameters.size > 0 then
					
						0.upto(aLineTokensParameters.size - 2) { |x|
						
							print("'" + aLineTokensParameters[x] + "'")
							print(",")
						}
						
						print("'" + aLineTokensParameters[aLineTokensParameters.size - 1] + "'")
					end
				
					print("],'" + retValue + "')\n")
					
				end
		}
	}
end

def ConvertFileToMethod inputfile

	File.open(inputfile) { |aFile|
	
		aFile.each_line { |aLine|
	
				if aLine =~ /DLLEXPORT/ then
	
					aLineTokens = aLine.split(/[()]/)
					aLineTokensCalling = aLineTokens[0].split(" ")
					aLineTokensParameters = aLineTokens[1].split(",")
					
					functionName = aLineTokensCalling[aLineTokensCalling.size - 1]
					functionRubyName = functionName[2...3].downcase! + functionName[3...functionName.size]
					
					retValue = aLineTokensCalling[1] if aLineTokensCalling[1] != "const"
					retValue = aLineTokensCalling[2] if aLineTokensCalling[1] == "const"
					
					retValue = ParamCToRuby retValue
				
					aLineTokensParameters.collect! { |aParam|
					
						aParamTokens = aParam.split(" ")
						
						aParamTokens[aParamTokens.size - 1]
					}
				
					print("def " + functionRubyName + "(")
					
					if aLineTokensParameters.size > 0 then
					
						0.upto(aLineTokensParameters.size - 2) { |x|
						
							print(aLineTokensParameters[x])
							print(",")
						}
						
						print(aLineTokensParameters[aLineTokensParameters.size - 1])
						
					end
				
					print(")\n")
					
					print("\t@" + functionRubyName + ".Call(")

					if aLineTokensParameters.size > 0 then
					
						0.upto(aLineTokensParameters.size - 2) { |x|
						
							print(aLineTokensParameters[x])
							print(",")
						}
						
						print(aLineTokensParameters[aLineTokensParameters.size - 1])
						
					end
					
					print(")\n")
					print("end\n")
					
				end
		}
	}
end

ConvertFile "x:/dev/tools/ragebuilder/scriptgta.h"
ConvertFile "x:/dev/tools/ragebuilder/scriptgeneral.h"
ConvertFile "x:/dev/tools/ragebuilder/scriptimage.h"
ConvertFile "x:/dev/tools/ragebuilder/scriptpack.h"
ConvertFile "x:/dev/tools/ragebuilder/scriptrage.h"