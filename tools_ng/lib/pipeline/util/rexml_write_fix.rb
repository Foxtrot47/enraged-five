#
# File:: rexml_write_fix.rb
# Description:: Fixes a bug in REXML::Document.write in Ruby 1.8.6.
#
# Date:: 26 June 2008
#
# References:
#   http://www.germane-software.com/projects/rexml/ticket/115
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'rexml/document'

module REXML

	class Document

		def write( output=$stdout, indent=-1, trans=false, ie_hack=false )
			if xml_decl.encoding != "UTF-8" && !output.kind_of?(Output)
				output = Output.new( output, xml_decl.encoding )
			end
			formatter = if indent > -1
				if trans
					REXML::Formatters::Transitive.new( indent, ie_hack )
				else
					REXML::Formatters::Pretty.new( indent, ie_hack )
				end
			else
				REXML::Formatters::Default.new( ie_hack )
			end
			formatter.write( self, output )
		end
	end
end

# End of rexml_write_fix.rb
