#
# File:: incredibuild.rb
# Description:: Xoreax IncrediBuild application interface.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 29 July 2008
#

require 'pipeline/os/path'
require 'pipeline/util/application'
require 'win32/registry'

module Pipeline

    #
    # == Description
    # Xoreax IncrediBuild application interface, to determine installed versions,
    # install directories etc.
	#
    class Incredibuild < IApplication
               
        #---------------------------------------------------------------------
        # Class Methods
        #---------------------------------------------------------------------      
        
        def Incredibuild.is_installed?()            
            return ( versions.size > 0 )
        end
        
        def Incredibuild.versions()
            
            versions = []
			begin
				::Win32::Registry::HKEY_LOCAL_MACHINE.open( REGISTRY_KEY ) do |key|
					key.each_value do |subkey, type, value|
						next unless ( subkey =~ REGEXP_VERSION )
						
						versions << value
					end
				end
			rescue ::Win32::Registry::Error
			end
            versions
        end
        
        def Incredibuild.latest_version()

            installed_versions = Incredibuild.versions()
            return installed_versions[0] if installed_versions.size > 0
            return nil
        end
        
		# Return Hash, version String key to String directory path.
        def Incredibuild.installdirs()        
            return nil unless is_installed?
            
            installdirs = {}
            begin
				::Win32::Registry::HKEY_LOCAL_MACHINE.open( REGISTRY_KEY ) do |key|
					key.each_value do |subkey, type, value|
						next unless ( subkey =~ REGEXP_INSTALLDIR )
						
						installdirs[latest_version()] = OS::Path.normalise( value )
					end
				end
			rescue ::Win32::Registry::Error
			end
            
            installdirs
        end
        
        #---------------------------------------------------------------------
        # Private Constants
        #---------------------------------------------------------------------
        
    private
        
        REGISTRY_KEY = 'Software\\Xoreax\\IncrediBuild\\Builder'
		REGEXP_VERSION = /^VersionText/
        REGEXP_INSTALLDIR = /^Folder/
    end
    
end # Pipeline module

if ( __FILE__ == $0 ) then

	versions = Pipeline::Incredibuild.versions()
	versions.each do |ver|
		puts "Version: #{ver}"
	end
	dirs = Pipeline::Incredibuild.installdirs()
	dirs.each_pair do |ver, dir|
		puts "Ver: #{ver} install at: #{dir}"
	end
	
	puts "IncrediBuild latest version: #{Pipeline::Incredibuild.latest_version()}"

end

# End of incredibuild.rb
