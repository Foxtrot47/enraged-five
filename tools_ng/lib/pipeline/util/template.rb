#
# File:: %RS_TOOLSLIB%/pipeline/template.rb
# Description:: ERB template helper processing class.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 17 March 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'erb'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Util
	
	#
	# == Description ==
	# ERB template processing class.
	#
	# === Template Arguments ===
	#   filename: output filename
	#   template: input ERB template filename
	#   args:     Hash object for user-defined arguments
	#
	class Template
		
		# Process method so it can be executed multiple times with different 
		# arguments.
		def Template::process( template, output, args = {} )
			
			template_text = File::read( template )
			erb_template = ERB::new( template_text )
			result = erb_template.result( binding )
			
			File::open( output, 'w' ) do |fp|
				fp.write( result )
			end
		end
	end
	
end # Util module
end # Pipeline module
