#
# float.rb
# Additional Float class functions.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 2 March 2008
#

class Float

	#
	#
	#
	alias_method :orig_to_s, :to_s
	
	#
	# Neatly pretty-print with specified number of places.
	#
	def to_s( arg = nil )
		if arg.nil?
			orig_to_s
		else
			if ( self == self.truncate.to_f.truncate )
				sprintf( "%.1f", self )
			else
				sprintf( "%.#{arg}f", self )
			end
		end
	end

end

# End of float.rb
