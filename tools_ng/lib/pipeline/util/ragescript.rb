#
# File:: pipeline/util/ragescript.rb
# Description:: RAGE Script Interface
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 24 September 2009
#
# Heavily based on Marissa's MaxScript interface.
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/os/file'
require 'pipeline/os/path'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Util	

	#
	# == Description
	# Simple interface for working with and parsing RAGE script files.
	#
	class RageScript
		#---------------------------------------------------------------------
		# Constants
		#---------------------------------------------------------------------	
		SCRIPT_EXTENSIONS = [ 'sc', 'sch' ]

		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------
		
		#
		# == Description
		# Determines whether this is a valid MaxScript file.  Only
		# accepts .sc or .sch files.
		#
		# == Example Usage
		#
		#	 RageScript::valid_file?( "x:/toolstest.sc" )
		#	 => true
		#	 RageScript::valid_file?( 'x:/toolstest.sch' )
		#    => true
		#	 RageScript::valid_file?( "x:/toolstest.txt" )
		#	 => false
		#
		def RageScript::valid_file?( filename )
			valid = false
			
			if (File.exist? filename)
				case OS::Path::get_extension( filename )
				when 'sc'
					valid = true
				when 'sch'
					valid = true
				end #case OS::Path::get_extension( filename )
			end #if (File.exist? filename)
			
			valid
		end #valid_file?
		
		
		#
		# == Description
		# Determines whether a given line is a comment.
		#
		# == Example Usage
		# DHM TODO
		#
		def RageScript::line_is_comment?( line )
			comment = false			
			l = line.strip
			
			# Are the first two characters of this line "/"?
			comment = true if ( 47 == ( l[0] and l[1] ) )			
			comment
		end #line_is_comment?
		
		#
		# == Description
		# Returns the contents of MaxScript file.  Also tests to make
		# sure that the file is valid and escapes all doublequotes.
		#
		# == Example Usage
		#	 Maxscript.get_lines( "x:/toolstest.ms" )
		#	 => ["line1", "line2", etc.]
		#
		def RageScript::get_lines( filename )
			throw ArgumentError.new("File #{filename} is not a valid RAGE Script file.") \
				unless RageScript::valid_file?( filename )
			
			# Get the text
			text = []
			File.open(filename, 'r') do
				text = IO.readlines( filename )
			end
			
			# Escape any double quotes
			text.each { |line| line.tr!('"', '\"') }
			
			text
		end #get_lines				
	end # RageScript class

end # Util module
end # Pipeline module

# pipeline/util/ragescript.rb
