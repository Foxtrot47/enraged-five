#
# File:: %RS_TOOLSLIB%/pipeline/util/rubyzip2_mtime_fix.rb
# Description:: Monkey-patch rubyzip2 for modified time fix.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 23 June 2011
#
# References: http://perso.wanadoo.es/jgoizueta/dev/goi/zipx.pdf
#

module Zip

	class ZipOutputStream
		def put_next_entry(entry, level = Zlib::DEFAULT_COMPRESSION)
			raise ZipError, "zip stream is closed" if @closed
			newEntry = entry.kind_of?(ZipEntry) ? entry : (entry.respond_to?(:__getobj__) ? entry.__getobj__ :
			ZipEntry.new(@fileName, entry.to_s))
			init_next_entry(newEntry)
			@currentEntry=newEntry
		end
	end

  class ZipEntry
    def time
      if @extra["UniversalTime"] && @extra["UniversalTime"].mtime
        @extra["UniversalTime"].mtime
      else
        @time
      end
    end
  end
  
  class ZipExtraField
		class UniversalTime
			def pack_for_local
				s = [@flag || 0].pack("C")
				@flag & 1 != 0 and s << [@mtime.to_i].pack("V")
				@flag & 2 != 0 and s << [@atime.to_i].pack("V")
				@flag & 4 != 0 and s << [@ctime.to_i].pack("V")
				s
			end
			
			def pack_for_c_dir
				s = [@flag || 0].pack("C")
				@flag & 1 == 1 and s << [@mtime.to_i].pack("V")
				s
			end
		end
	end
end

# %RS_TOOLSLIB%/pipeline/util/rubyzip2_mtime_fix.rb
