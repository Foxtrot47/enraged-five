#
# File:: rage_pack.rb
# Description:: This provides a compatibility layer for RPF construction
#               methods normally provided by the RAGE Gem.  This class
#               uses the corresponding project, branch and target's
#               ragebuilder executable to build the RPF.
#
#               It was created to support GTA4 E2 on the Perforce transition
#               as that project requires 1.9.0 RAGE but has different versions
#               for 360 and PS3 platforms.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 10 November 2008
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
# None

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Util
	
	#
	# == Description
	# RAGE Gem compatibility class that replicates RageUtil::pack 
	# functionality to if we do not have an appropriate RAGE Gem we can still
	# build image files.
	#
	# Or in the case of GTA4/E1/E2 we can easily build image files for multiple
	# platforms.
	#
	# Note: this only exposes a subset of the full RAGE Gem functionality,
	# but should be enough for converts or other simple RPF building scripts.
	#
	class RagebuilderPack
		attr_reader :project
		attr_reader :branch
		attr_reader :target
		attr_reader :files
			
		def initialize( project, branch, target )
			@project = project
			@branch = branch
			@target = target
		end
		
		# Virtual target attribute writer so we can refresh the ragebuilder 
		# executable filename if the user or convert_rage updates the target.
		def target=( val )
			throw ArgumentError.new( "Invalid target object." ) \
			unless ( val.is_a?( Pipeline::Target ) )
			
			@target = val
		end
		
		def start( )
			@files = []
			@compressed = true
		end
		
		def start_uncompressed( )
			@files = []
			@compressed = false
		end
		
		def add( filename )
			@files << filename
		end
		
		def save( filename )
			
			filepath = OS::Path::get_directory( filename )
			FileUtils::mkdir_p( filepath ) unless ( ::File::directory?( filepath ) )
			
			# Create temporary Ragebuilder script.
			File.open( TEMP_RBS_SCRIPT, "w+" ) do |f|

				f << "start_pack()\n" if ( @compressed )
				f << "start_pack_uncompressed()\n" unless ( @compressed )
				
				@files.each do |file|					
					f << "add_to_pack(\"#{file}\")\n"
				end
				
				f << "save_pack(\"#{filename}\")\n"
				f << "close_pack()\n"
			end
			
			tools = RageConvertTool.parse( @project )
			cmdline = tools[@target.platform].path + " #{TEMP_RBS_SCRIPT} " + "-nopopups"
			Kernel.system( cmdline )
		end
		
		def close( )
			# Ignore
		end
	
		#--------------------------------------------------------------------
		# Private
		#--------------------------------------------------------------------
	private
		TEMP_RBS_SCRIPT = OS::Path::combine( Config::instance().temp, 'rage_pack.rbs' )
	end
	
end # Util module
end # Pipeline module

# rage_image.rb
