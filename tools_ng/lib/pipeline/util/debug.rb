#
# debug.rb
# Ruby debug helpers
#
# Author:: David Muir <david.muir@rockstarnorth.com>
#

#
# == Description
# Exception class raised when an assert fails.  To continue after asserts you
# need to catch such exceptions and recover in your code.
#
#
class AssertFail < StandardError
end

#
# == Description
# We extend the root Object class with an additional assert method.
#
#
class Object
	
	#
	# == Description
	# Assert method to test condition being true.  If cond is false then an
	# AssertFail exception is raised.  In order to see the assert messages
	# run ruby with the -d flag.
	#
	# == Example Usage
	#   5.assert( false, "assert message" )
	#
	def assert( cond, message = "Assert failure." )
	
		if $DEBUG
			raise AssertFail.new( message ) unless cond
		end
	end
	
	#
	# == Description
	# Pretty-print an exception message and stacktrace to stdout.
	#
	# == Example Usage
	#   begin
	#     # Do something...
	#   rescue Exception => ex
	#	  print_exception( ex )
	#   end
	#
	def print_exception( ex, prefix = 'Unhandled exception:' )
		print "#{prefix} #{ex.message}\n"
		print "Stacktrace:\n\r\t#{ex.backtrace.join("\n\r\t")}"
		print "\n\n"
	end
end

# End of debug.rb
