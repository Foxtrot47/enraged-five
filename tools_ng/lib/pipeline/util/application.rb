#
# File:: application.rb
# Description:: Application interface, installed versions etc.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 29 July 2008
#

module Pipeline

    #
    # == Description
    # Application abstract interface defining a few class methods for retrieving
	# information about locally installed applications.
    #
    class IApplication
        
		#
		# Return whether the application is installed on the local machine.
		#
        def IApplication.is_installed?()            
            throw NotImplementedError.new()
        end
        
        #
        # Return an array of installed application versions (as floats).
        #
        def IApplication.versions()
			throw NotImplementedError.new()
        end
        
        #
        # Return the latest installed version of the application on the local
        # machine - nil if no versions are installed.
        #
        def IApplication.latest_version()
			throw NotImplementedError.new()
		end
        
        #
        # Return a hash of installed versions (key) and their install locations
        # (string path) for all installed application versions.
        #
        def IApplication.installdirs()
            throw NotImplementedError.new()
        end
    end
    
end # Pipeline module

# End of autodesk3dsmax.rb
