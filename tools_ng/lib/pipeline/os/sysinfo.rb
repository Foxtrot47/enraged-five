#
# Filename:: pipeline/os/sysinfo.rb
# Description:: Windows system information.  At the moment just used to store system type (x64, x86)
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
#
# 

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/os/path'

module Pipeline
module OS

	#
	# == Description
	# System architecture information.  May be expanded in the future.
	#
	class WinSystemInfo			
		#---------------------------------------------------------------------
		# Class Methods
		#---------------------------------------------------------------------      
					
		# Return system architecture as a symbol (e.g. :x86, :x64)
		def WinSystemInfo::SysType(  )			
			if ( not ENV[PROGRAM_FILESX86].nil? ) then
				:x64
			else
				:x86
			end
		end
		
		#---------------------------------------------------------------------
		# Public Constants
		#--------------------------------------------------------------------- 
	private     
		# We would have used the %PROCESSOR_ARCHITECTURE% value but for 32-bit
		# processes like ruby.exe it is set to x86.  Useful.
		
		PROGRAM_FILESX86 = 'ProgramFiles(x86)'
		X86 = 'x86'	
		X64 = 'AMD64'
	end		
end # OS module
end # Pipeline module

if ( __FILE__ == $0 ) then

	puts "Current system architecture: #{Pipeline::OS::WinSystemInfo::SysType()}"	
end

# pipeline/os/sysinfo.rb
