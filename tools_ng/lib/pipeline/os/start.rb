#
# File:: %RS_TOOLSLIB%/pipeline/os/start.rb
# Description:: Start process utilities
# 
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 30 January 2008
#
# Requirements:
#  * POpen4 Gem
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/log/log'
require 'systemu'

#-----------------------------------------------------------------------------
# Monkey-Patching
#-----------------------------------------------------------------------------
module Process
    class Status
        def initialize( exitstatus )
            @exitstatus = exitstatus
        end
    end
end

#-----------------------------------------------------------------------------
# Pipeline
#-----------------------------------------------------------------------------
module Pipeline
module OS

    # ProcessStatus class made to look like Process::Status.
    class ProcessStatus
        attr_reader :exitstatus
        
        def initialize( exitstatus )
            @exitstatus = exitstatus
        end
    end

    #
    # == Description
    #
    # start invokes a new child process returning status, stdin and stdout.
	#
	# If a block is given a child process ID is available as its parameter.
    #
    # == Example Usage
    #
    #    status, stdout, stderr = Pipeline::OS.start( cmd )
	#    puts "stdout     : #{ stdout.strip }"
    #    puts "stderr     : #{ stderr.strip }"
    #    puts "status     : #{ status.inspect }"
    #    puts "exitstatus : #{ status.exitstatus }"
    #    
    def OS::start( command )
        config = Pipeline::Config::instance( )
        if ( config.user.username.include?( "'" ) ) then
            res = system( command )
            status = ProcessStatus::new( res ? 0 : 1 )
            [ status, '', '' ]
        else
            systemu( command )
        end
    end
	
end # OS module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/os/start.rb
