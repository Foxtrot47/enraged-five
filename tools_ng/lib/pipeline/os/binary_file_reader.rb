#
# File:: binary_file_reader.rb
# Description:: Binary File Reader helper module.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 9 October 2008
#
# References:
#   http://davidtran.doublegifts.com/blog/?cat=3
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
# None

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module OS

	#
	# == Description
	# The BinaryFileReader class presents functions to read binary data
	# chunks from little-endian or big-endian based files.
	#
	module BinaryFileReader
		#
		# Read binary chunk from IO device in little-endian format.
		#
		def BinaryFileReader::read_io_ledw( io, size )
			format2bytes, format4bytes = %w[v V]
			case size
			when 4
				io.read( size ).unpack( format4bytes ).join().to_i
			when 2
				io.read( size ).unpack( format2bytes ).join().to_i
			else
				throw RuntimeError.new( "Size of #{size} not implemented." )
			end
		end
		
		#
		# Read binary chunk from IO device in big-endian format.
		#
		def BinaryFileReader::read_io_bedw( io, size )
			format2bytes, format4bytes = %w[n N]
			case size
			when 4
				io.read( size ).unpack( format4bytes ).join().to_i
			when 2
				io.read( size ).unpack( format2bytes ).join().to_i
			else
				throw RuntimeError.new( "Size of #{size} not implemented." )
			end
		end
		
		#
		# Read binary chunk from IO device as little-endian float.
		#
		def BinaryFileReader::read_io_lefloat( io )
			io.read( 4 ).unpack( 'e' ).join()
		end
		
		#
		# Read binary chunk from IO device as native float.
		#
		def BinaryFileReader::read_io_nfloat( io )
			io.read( 4 ).unpack( 'f' ).join()
		end
	end

end # OS module
end # Pipeline module

# dds_raw.rb
