#
# File:: p4ruby_test.rb
# Description:: P4 Ruby Bindings Gem Unit Tests
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 20 June 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'p4'
require 'test/unit' 

module Pipeline
module Test

	#
	# == Description
	# Some simple unit tests for p4ruby bindings.  Essentially tests that the
	# gem has successfully been installed on your system so might be useful
	# for debugging some issues with source control later on.
	#
	class P4Ruby < ::Test::Unit::TestCase
	
		def setup()
			assert( P4.is_a?( Class ) )
			@p4 = P4.new( )
		end
		
		def teardown()
			@p4.disconnect() if ( @p4.connected?() )
		end
		
		def test_p4ruby_connection()
			assert_equal( false, @p4.connected?() )
			@p4.connect()
			assert_equal( true, @p4.connected?() )
			assert( @p4.cwd().is_a?( String ) )
			assert( @p4.cwd().size() > 0 )
		end
		
		def test_p4ruby_install()
			assert( P4.identify().is_a?( String ), "P4 version information not available." )
			assert( P4.identify().size() > 0, "P4 version information incorrect." )
		end
	end

end # Test module
end # Pipeline module

# End of p4ruby_test.rb
