#
# File:: monitor_test.rb
# Description:: Perforce Monitor Unit Tests
#
# Author:: David Muir <david.muir@rockstarrnoth.com>
# Date:: 5 March 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/scm/perforce'
require 'pipeline/scm/monitor'
include Pipeline
require 'test/unit' 

module Pipeline
module Test

	#
	# == Description
	# Perforce Monitor class unit test cases.
	#
	class MonitorTest < ::Test::Unit::TestCase
	
		PROJECT_NAME = 'jimmy'
		MAX_CHANGELIST = 9999999

		def setup( )
			@project = Pipeline::Config::instance().projects[PROJECT_NAME]
			@project.load_config()			

			@p4 = @project.scm()
			@p4.connect( )
			@monitor = SCM::Monitor::from_p4( @p4, "#{@project.build}/...", 'test.xml' )
			assert( @monitor, "Monitor creation failed." )
		end

		def teardown( )
		end

		def test_poll_once( )
			puts "TEST_POLL_ONCE"

			changelists = @p4.run_changes( '-ssubmitted', '-L', "#{@project.build}/...\#have,@#{MAX_CHANGELIST}" )
			if ( changelists.size > 0 ) then
				@monitor.poll_once() do |cl, sync_output, skipped|
					puts "Fetched: #{cl}"
					assert_equal( cl, changelists.last, "Incorrect changelist fetched." )
				end	
			else
				assert_not_nil( nil, "No changelists to fetch under #{@project.build}/..." )
			end
		end

		def test_multi( )
			puts "TEST_MULTI"

			changelists = @p4.run_changes( '-ssubmitted', '-L', "#{@project.build}/...\#have,@#{MAX_CHANGELIST}" )
			if ( changelists.size > 0 ) then
				num_cl = 10
				while ( num_cl > 0 ) do
					changelists = @p4.run_changes( '-ssubmitted', '-L', "#{@project.build}/...\#have,@#{MAX_CHANGELIST}" )
					@monitor.poll_once() do |cl, sync_output, skipped|
						puts "Fetched: #{cl}"
						assert_equal( cl, changelists.last, "Incorrect changelist fetched." )
					end
					num_cl -= 1
				end
			else
				assert_not_nil( nil, "No changelists to fetch under #{@project.build}/..." )
			end
		end
	end

end # Test module
end # Pipeline module

# monitor_test.rb
