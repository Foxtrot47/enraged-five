#
# File:: perforce_helper_test.rb
# Description:: Perforce SCM class helper unit tests
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 1 December 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/scm/perforce'
require 'pipeline/scm/perforce_helper'
require 'test/unit'

#-----------------------------------------------------------------------------
# Code
#-----------------------------------------------------------------------------

module Pipeline
	module Test
		
		class PerforceTest < ::Test::Unit::TestCase
			
			def setup
				@config = Config::instance()
				@p4 = Pipeline::SCM::Perforce.new()
				@p4.user = @config.sc_username
				@p4.client = @config.sc_workspace
				@p4.port = @config.sc_server
				@p4.connect()
				
				assert( @p4.connected?, 'Failed to connect to Perforce server' )
			end
			
			def teardown
				@p4.disconnect()
			end
			
			def test_email
				assert_equal( 'david.muir@rockstarnorth.com', @p4.get_user_email( 'david.muir' ) )
				assert_equal( 'tim.gilbert@rockstarnorth.com', @p4.get_user_email( 1247729 ) )
			end
		end
		
	end # Test module
end # Pipeline module

# End of project_test.rb
