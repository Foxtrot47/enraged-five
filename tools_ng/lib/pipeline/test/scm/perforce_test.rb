#
# File:: perforce_test.rb
# Description:: Perforce SCM class unit tests
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 20 March 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/config/projects'
require 'pipeline/scm/perforce'
require 'test/unit'  

#-----------------------------------------------------------------------------
# Code
#-----------------------------------------------------------------------------

module Pipeline
module Test
    
    class PerforceTest < ::Test::Unit::TestCase
    
		def setup( )
			@config = Config::instance()
			@p4 = Pipeline::SCM::Perforce.new()
			@p4.user = @config.sc_username
            @p4.client = @config.sc_workspace
            @p4.port = @config.sc_server
			@p4.connect()
			
			assert( @p4.connected?, 'Failed to connect to Perforce server' )
        end
		
		def teardown( )
			@p4.disconnect()
        end
		
		def test_sync( )
			@p4.run_sync( '//depot/tools_release/...' )
		end
		
        def test_mapping( )
			depot1 = '//depot/test_file.txt'
			depot2 = '//depot/jimmy/src/dev/test_file.txt'
			local1 = 'x:/test_file.txt'
			local2 = 'x:/jimmy/src/dev/test_file.txt'
	
			assert_equal( local1, @p4.depot2local( depot1 ), 'depot2local failed' )
			assert_equal( local2, @p4.depot2local( depot2 ), 'depot2local failed' )	
			assert_equal( depot1, @p4.local2depot( local1 ), 'local2depot failed' )
			assert_equal( depot2, @p4.local2depot( local2 ), 'local2depot failed' )
        end
    end
    
end # Test module
end # Pipeline module

# End of project_test.rb
