#
# File:: connection_test.rb
# Description:: Perforce connection test script.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 3 September 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/scm/perforce'
include Pipeline

def create_changelist( p4, desc )
	spec = p4.fetch_change()
	spec[ 'Files' ] = []
	spec[ 'Jobs' ] = []
	spec[ 'Description' ] = desc
	out = p4.save_change( spec ).shift
	if ( out =~ /Change (\d+) created./ )
		cl = $1
		return ( cl )
	end
	throw P4Exception.new( 'Error creating pending changelist.' )
end

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

	begin
		p4a = SCM::Perforce_Connected.new()
		p4b = SCM::Perforce_Connected.new()

		p4b.port = 'localhost:1666'
		p4b.user = 'david.muir'
		p4b.client = 'david.muir'
		p4b.connect( )

		p4a.port = 'rsgedip4d01:1666'
		p4a.user = 'david.muir'
		p4a.client = 'david.muir'
		p4a.connect( )

		create_changelist( p4a, 'Server A Changelist' )
		create_changelist( p4b, 'Server B Changelist' )

		p4a.disconnect( )
		p4b.disconnect( )

	rescue Exception => ex

		puts "Unhandled exception: #{ex.message}"
		puts "Call stack:"
		puts ex.backtrace.join( "\n\t" )
	end
end

# connection_test.rb
