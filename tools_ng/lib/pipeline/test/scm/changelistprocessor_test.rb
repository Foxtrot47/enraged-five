#
# File:: changelistprocessor_test.rb
# Description:: Unit tests for change list processor.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 6 August 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/scm/perforce'
require 'pipeline/scm/changelistprocessor'
require 'test/unit'

module Pipeline
module Test

	#
	# == Description
	# Unit tests for the ChangeListProcessor class.
	#
	class ChangeListProcessorTests < ::Test::Unit::TestCase
	
		def setup()
			@p4 = Pipeline::SCM::Perforce.new()
			@p4.exception_level = P4::RAISE_ERRORS
			@p4.user = 'david.muir'
			@p4.client = 'david.muir'
			@p4.port = 'rsgedip4d01:1666'
			@p4.connect()
			
			assert( @p4.connected?, 'Perforce hasn\'t connected.' )
		end
		
		def teardown()
			@p4.disconnect()
			assert( ( not @p4.connected? ), 'Perforce hasn\'t disconnected.' )
		end
		
		def test_processor_block()
		
			Pipeline::SCM::ChangeListProcessor.process( @p4, 22 ) do |changelist|
				assert( changelist.is_a?( Array ), "Invalid changelist type (#{changelist.class})." )
				
				changelist.each do |cl|
					assert( cl.is_a?( Hash ), "Invalid changelist subtype (#{cl.class})." )
					
					cl.each_pair do |k,v|
						puts "#{k} => #{v}"
					end
				end
			end
		end
	end

end # Test module
end # Pipeline module

# changelistprocessor_test.rb
