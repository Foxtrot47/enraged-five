#
# File:: alienbrain_test.rb
# Description:: Alienbrain abstraction class unit tests
# Author:: Greg Smith <greg@rockstarnorth.com>
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 14/3/2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/scm/alienbrain'
require 'test/unit'

#-----------------------------------------------------------------------------
# Code
#-----------------------------------------------------------------------------

module Pipeline
module Test
    
    class AlienbrainTest < ::Test::Unit::TestCase
    
		def setup			
			@ab = Alienbrain.new('Glenfiddich', 'tools_release', 'david.muir', '', false)
        end
		
		def teardown
			@ab = nil
        end
		
		def test_exists
			
			assert( @ab.exists( 'tools_release/gta_bin' ), 
				'Directory tools_release/gta_bin should exist' )
			assert( @ab.exists( 'tools_release/gta_bin/gta3.att' ), 
				'File tools_release/gta_bin/gta3.att should exist' )
        end
		
		def test_local_to_workspace_path
			
			assert_equal( '\\Workspace\\tools_release\\gta_bin', 
				@ab.local_to_workspace_path( 'x:/tools_release/gta_bin' ) )
			assert_equal( '\\Workspace\\tools_release\\gta_bin\\attribedit.exe',
				@ab.local_to_workspace_path( 'x:/tools_release/gta_bin/attribedit.exe' ) )
        end
		
		def test_workspace_to_local_path
			
			assert_equal( 'x:/tools_release/gta_bin/', 
				@ab.workspace_to_local_path( '\\Workspace\\tools_release\\gta_bin' ) )
			assert_equal( 'x:/tools_release/gta_bin/attribedit.exe',
				@ab.workspace_to_local_path( '\\Workspace\\tools_release\\gta_bin\\attribedit.exe' ) )
        end
		
        def test_get_latest
			
            files = @ab.get_latest( 'tools_release/gta_bin', false )
			puts "Files fetched (#{files.size}):"
			files.each do |file|
				puts "\t#{file}"
            end
        end
		
		def test_check_in_out
			
            #puts 'Checking out tools_release/bin/pipeline/scm/Test.txt...'
            #ab.checkout( 'tools_release/bin/pipeline/scm/Test.txt', false, "Test checkout" )
    
            #puts 'Checking in tools_release/bin/pipeline/scm/Test.txt...'
            #ab.checkin( 'tools_release/bin/pipeline/scm/Test.txt' )
        end
		
		def test_search
			
			files = @ab.search( 'tools_release/gta_bin', Alienbrain::AB_FLAG_SEARCH_NORMAL )
			files.each do |path|
				puts path
            end
        end
    end 
    
end # Test module
end # Pipeline module

# End of alienbrain_test.rb
