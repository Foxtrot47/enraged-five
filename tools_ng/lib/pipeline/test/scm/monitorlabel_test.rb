#
# File:: monitorlabel_test.rb
# Description:: Perforce MonitorLabel Unit Tests
#
# Author:: Derek Ward <derek.ward@rockstarrnoth.com>
# Date:: 5 March 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/scm/perforce'
require 'pipeline/scm/monitor_label'
include Pipeline
require 'test/unit' 

module Pipeline
module Test

	#
	# == Description
	# Perforce MonitorLabel class unit test cases.
	#
	class MonitorLabelTest < ::Test::Unit::TestCase
	
		PROJECT_NAME		= 'jimmy'
		BRANCH_NAME         = 'dev'
		MONITOR_LABEL_SLEEP = 60
		LABEL				= "LabelMonitor_Test_Label"
		XML					=  'test.xml'

		def setup( )
			@project = Pipeline::Config::instance().projects[PROJECT_NAME]
			@project.load_config()	
			
			@branch = BRANCH_NAME

			@p4 = @project.scm()
			@p4.connect( )

			@monitor = SCM::MonitorLabel::from_p4( @p4, "#{@project.branches[@branch].build}/Testfolder/...", XML, LABEL, MONITOR_LABEL_SLEEP )
			assert( @monitor, "Monitor creation failed." )
		end

		def teardown( )
		end

		def test_label_exists( )
			puts "checking if label exists"
			if @monitor.label_exists then 
				puts "The label exists #{@monitor.label}\n"
			else
				puts "The label does not exist #{@monitor.label}\n" 
			end			
		end	# test_label_exists
		
		def test_is_synced_to_label( )
			if @monitor.is_synced_to_label then 
				puts "Good : The monitor is synced to its label #{@monitor.label}\n"
			else
				puts "Warning : The monitor is not synced to its label #{@monitor.label}\n" 
			end	
		end # test_is_synced_to_label
			
		def test_poll( )
			@monitor.poll() do |changelist, sync_output, skipped|
				puts "Monitor poll yielded"
			end # monitor poll
		end # test_poll			
					
	end # class MonitorLabelTest	
	
end # Test module
end # Pipeline module

# monitorlabel_test.rb
