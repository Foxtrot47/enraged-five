#
# File:: email_test.rb
# Description:: Email utility class unit tests.
#
# Author:: David Muir <dhm@davidmuir.co.uk>
# Date:: 13 September 2008
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/util/email'
require 'test/unit'

#----------------------------------------------------------------------------
# Code
#----------------------------------------------------------------------------
module Pipeline
module Util
module Test

	#
	# == Description
	# Email class methods test cases.
	#
	class EmailTest < ::Test::Unit::TestCase
		
		EMAIL_OPTIONS = {
			:server => 'rsgediexg1.rockstar.t2.corp',
			:port => 25,
			:username => nil,
			:password => nil,
			:domain => 'rockstarnorth.com',
			:auth_mode => :plain
		}

		def test_instance_methods( )
		
			@email = Util::Email.new( EMAIL_OPTIONS )
			@email.send( { :address => 'test@rockstarnorth.com', :alias => 'Email Test' },
						 { :address => 'david.muir@rockstarnorth.com', :alias => 'David Muir' },
						 'Testing Email',
						 "Testing body plain text (#{Time.now})" )
						 
			@email.send( { :address => 'test@rockstarnorth.com', :alias => 'Email Test' },
						 { :address => 'david.muir@rockstarnorth.com', :alias => 'David Muir' },
						 'Testing Email',
						 "<html><body><h3>Heading</h3><p>The time: #{Time.now}</p></body></html>",
						 'text/html' )

			@email.send( { :address => 'test@rockstarnorth.com', :alias => 'Email Test' },
						 [ { :address => 'david.muir@rockstarnorth.com', :alias => 'David Muir' },
						   { :address => 'derek.ward@rockstarnorth.com', :alias => 'Derek Ward' } ],
						 'Testing Multiple Recipients Email',
						 "<html><body><h3>Heading</h3><p>The time: #{Time.now}</p></body></html>",
						 'text/html' )

			# Multipart test
			filename = 'x:/pipedev/src/dev/GTA tools/helpdocs/relnotes/html/rslogo.jpg'
			if ( File::exists?( filename ) and File::readable?( filename ) ) then
				html_part = Util::EmailFactory::create_html_mail( "<html><body><h3>HTML Email Test</h3><p>The time: #{Time.now}</p></body></html>" )
				text_part = Util::EmailFactory::create_text_mail( "The time: #{Time.now}" )
				attachment = Util::EmailFactory::create_attachment_from_file( filename )
				
				parent = TMail::Mail.new()
				parent.mime_version = '1.0'
				parent.set_content_type( 'multipart', 'alternative', { 'boundary' => TMail::new_boundary() } )
				parent.parts << html_part
				parent.parts << text_part

				@email.send_multipart( { :address => 'test@rockstarnorth.com', :alias => 'Email Test' },
							 { :address => 'david.muir@rockstarnorth.com', :alias => 'David Muir' },
							 'Testing Email',
							 [ parent, attachment ] )

				@email.send( { :address => 'test@rockstarnorth.com', :alias => 'Email Test' },
							[ { :address => 'david.muir@rockstarnorth.com', :alias => 'David Muir' },
							  { :address => 'derek.ward@rockstarnorth.com', :alias => 'Derek Ward' } ],
							'Testing Multiple Recipients Multipart Email',
							[ parent, attachment ] )
			end
		end
		
		def test_class_methods( )
		end
	end

end # Test module
end # Util module
end # Pipeline module

# email_test.rb
