#
# debug_test.rb
# Debug functions unit tests
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 11 March 2008
#

require 'pipeline/util/debug'
require 'test/unit'

module Pipeline
module Test

	class DebugTest < ::Test::Unit::TestCase
	
		def setup
			# Force Ruby into debug mode
			$DEBUG = 1
		end
	
		def test_assert_false
				
			assert_raise AssertFail do
				5.assert( false, "testing - ignore message in unit test." )
			end
			assert_nothing_raised do
				5.assert( true, "AssertFail not caught." )
			end
		end
		
		def test_assert_true
				
			assert_nothing_raised do
				5.assert( true, "testing" )
			end
			assert_nothing_raised do
				'hello'.assert( true, "AssertFail not raised." )
			end
			assert_raise AssertFail do
				'testing'.assert( false, "testing - ignore message in unit test." )
			end
		end	
	end
		
end # Test module
end # Pipeline module

# End of debug_test.rb
