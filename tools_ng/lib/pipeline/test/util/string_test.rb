#
# string_test.rb
# String class extensions unit tests
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 10 March 2008
#

require 'pipeline/util/string'
require 'test/unit'

module Pipeline
module Test

	#
	# == Description
	# Test cases for additional string methods declared above.
	#
	class StringTest < ::Test::Unit::TestCase
	
		def test_mgsub
		
			assert_equal( TEST_01_RESULT, TEST_01_SOURCE.mgsub( TEST_01_REGEXP ), 
				"mgsub TEST_01 failed." )
			assert_equal( TEST_02_RESULT, TEST_02_SOURCE.mgsub( TEST_02_REGEXP ), 
				"mgsub TEST_02 failed." )		
		end
		
		def test_mgsubex
		
			assert_equal( TEST_01_RESULT, TEST_01_SOURCE.mgsub!( TEST_01_REGEXP ),
				"mgsub! TEST_01 failed." )
			assert_equal( TEST_02_RESULT, TEST_02_SOURCE.mgsub!( TEST_02_REGEXP ), 
				"mgsub! TEST_02 failed." )	
		end
	
		def test_starts_with
		
			assert( TEST_03_SOURCE.starts_with( TEST_03_SOURCE ) )
			assert( TEST_03_SOURCE.starts_with( TEST_03_STARTS ) )
			assert( !TEST_03_SOURCE.starts_with( TEST_04_ENDS ) )
		end
	
		def test_ends_with
		
			assert( TEST_04_SOURCE.ends_with( TEST_04_SOURCE ) )
			assert( TEST_04_SOURCE.ends_with( TEST_04_ENDS ) )
			assert( !TEST_04_SOURCE.ends_with( TEST_03_STARTS ) )
		end
	
		#---------------------------------------------------------------------
		# Private Constants (Test Data)
		#---------------------------------------------------------------------
	
	private
	
		TEST_01_SOURCE = 'GO HOME!'
		TEST_01_REGEXP = [[/.*GO/i, 'Home'], [/home/i, 'is where the heart is']]
		TEST_01_RESULT = 'Home is where the heart is!'
		
		TEST_02_SOURCE = 'Here is number #123'
		TEST_02_REGEXP = [[/[a-z]/i, '#'], [/#/, 'P']]
		TEST_02_RESULT = '#### ## ###### P123'
		
		TEST_03_SOURCE = 'teststartswith'
		TEST_03_STARTS = 'tests'
		
		TEST_04_SOURCE = 'testsendwith'
		TEST_04_ENDS   = 'with'
	
	end
		
end # Test module
end # Pipeline module

# End of string_test.rb
