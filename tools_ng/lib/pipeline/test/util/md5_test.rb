#
# File:: md5_test.rb
# Description:: MD5 utility class/function unit tests.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 15 September 2008
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/util/md5'
require 'digest/md5'
require 'test/unit'

#----------------------------------------------------------------------------
# Code
#----------------------------------------------------------------------------
module Pipeline
module Util
module Test

	#
	# == Description
	# MD5 class methods test cases.
	#
	class MD5Test < ::Test::Unit::TestCase
		FILE1 = 'x:/Tools/dev/GTA tools/helpdocs/relnotes/html/rslogo.jpg'
		
		def test_md5
			md5_check = nil
			File.open( FILE1, 'rb' ) do |fp|
				data = fp.read()
				md5_check = Digest::MD5::hexdigest( data )
			end
			puts "MD5 check: #{md5_check}"
			
			md5 = Util::MD5::hexdigest( FILE1 )
			puts "MD5: #{md5}"
			
			assert_equal( md5_check, md5, 'MD5 checksums do not match!' )
		end
	end

end # Test module
end # Util module
end # Pipeline module

# md5_test.rb
