#
# File:: environment_test.rb
# Description:: Environment class unit tests
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 11 June 2008
#

require 'pipeline/util/environment'
require 'test/unit'

module Pipeline
module Test

	#
	# == Description
	# Environment class unit tests.
	#
	class EnvironmentTest < ::Test::Unit::TestCase
	
		def setup
			@env = Environment.new()
		end
	
		def teardown
			@env = nil
		end
	
		def test_environment_stack
			
			assert_raise EnvironmentException do
				@env.lookup( 'test' )
			end
			@env.add( 'test', 'test_depth0' )
			assert_equal( 'test_depth0', @env.lookup( 'test' ) )
			@env.push()
			@env.add( 'test', 'test_depth1' )
			assert_equal( 'test_depth1', @env.lookup( 'test' ) )
			
			@env.pop()
			assert_equal( 'test_depth0', @env.lookup( 'test' ) )
			@env.clear()
			assert_raise EnvironmentException do
				@env.lookup( 'test' )
			end
		end
	end
		
end # Test module
end # Pipeline module

# End of debug_test.rb
