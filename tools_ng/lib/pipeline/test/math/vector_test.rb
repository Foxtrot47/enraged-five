#
# vector_test.rb
# Vector class unit test cases
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 11 March 2008
#

require 'pipeline/math/vector'
require 'test/unit'

module Pipeline
module Test

    #
    # == Description
    # Vector class test cases.
    # 
    class VectorTests < ::Test::Unit::TestCase

        def setup        
            @v1 = Math::Vector.new( 3.0, 4.0, 5.0 )
            @v2 = Math::Vector.new( 5.0, 3.0, 4.0 )
            @v3 = Math::Vector.new( 0.0, 0.0, 0.0 )
            @v4 = Math::Vector.new( 1.0, 1.0, 1.0 )
        end

        def test_op_sqbrkt
            assert_equal( @v1[0], 3.0, 'x-component failed.' )
            assert_equal( @v1[1], 4.0, 'y-component failed.' )
            assert_equal( @v1[2], 5.0, 'z-component failed.' )
        end
        
        def test_magnitude
            assert_equal( ::Math::sqrt(50.0), @v1.magnitude, 'magnitude call failed' )
            assert_equal( ::Math::sqrt(50.0), @v1.length, 'length call failed' )
            assert_equal( ::Math::sqrt(50.0), @v2.magnitude, 'magnitude call failed' )
            assert_equal( ::Math::sqrt(50.0), @v2.length, 'length call failed' )
            assert_equal( 0.0, @v3.magnitude, 'magnitude call failed' )
            assert_equal( 0.0, @v3.length, 'length call failed' )
            assert_equal( ::Math::sqrt(3.0), @v4.magnitude, 'magnitude call failed' )
            assert_equal( ::Math::sqrt(3.0), @v4.length, 'length call failed' )
        end
        
        def test_inverse_magnitude
        	# DHM TODO
        end
        
        def test_normalise
        	# DHM TODO
        end
        
        def test_scale
        	# DHM TODO
        end
        
        def test_dot
        	# DHM TODO
        end
        
        def test_tos
            assert_equal( '[ 3.0, 4.0, 5.0 ]', @v1.to_s(1) )
        end
    end
	
end # Test module
end # Pipeline module

# End of vector_test.rb
