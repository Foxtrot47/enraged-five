#
# File:: quat_test.rb
# Description:: Quaternion class unit tests
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 11 April 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/math/quat'
require 'pipeline/math/vector'
require 'pipeline/math/angles'
require 'test/unit'

module Pipeline
module Test
	
	#
	# == Description
	# Unit test case class for our Quaternion class.
	#
	class QuatTest < ::Test::Unit::TestCase
		
		def setup
			@x_axis = Math::Vector.new( 1.0, 0.0, 0.0 )
			@y_axis = Math::Vector.new( 0.0, 1.0, 0.0 )
			@z_axis = Math::Vector.new( 0.0, 0.0, 1.0 )
			@q1 = Math::Quat.from_rotation( @x_axis, 0.0 )
			@q2 = Math::Quat.from_rotation( @y_axis, Math::DEG_TO_RAD * 90.0 )
        end
		
		def test_constructor
        end
		
		def test_magnitude
			puts @q1.magnitude
			puts @q2.magnitude
        end
		
		def test_normalise
			
        end
		
		def test_scale
			
        end
    end
	
end # Test module
end # Pipeline module

# End of quat_test.rb
