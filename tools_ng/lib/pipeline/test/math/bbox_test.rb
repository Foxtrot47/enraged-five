#
# bbox_test.rb
# Boundingbox class unit test cases
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 11 March 2008
#

require 'pipeline/math/bbox'
require 'test/unit'

module Pipeline
module Test

    #
    # == Description
    # 3D Bounding Box class test cases.
    # 
    class BoundingBoxTests < ::Test::Unit::TestCase
    
        def test_constructor
            assert_nothing_raised do
                bbox = Math::BoundingBox3.new( Math::Vector.new( 0.0, 0.0, 0.0 ), Math::Vector.new( 1.0, 1.0, 1.0 ) )
                assert_not_nil( bbox )
            end
            assert_raise Math::BoundingBox3::Invalid do
                Math::BoundingBox3.new( Math::Vector.new( 0.0, 0.0, 0.0 ), 2.01 )
            end
            assert_raise Math::BoundingBox3::Invalid do
                Math::BoundingBox3.new( 2.01, Math::Vector.new( 0.0, 0.0, 0.0 ) )
            end
            assert_raise Math::BoundingBox3::Invalid do
                bbox = Math::BoundingBox3.new( 3.0, 2.01 )
            end
        end
        
        def test_contains
            bbox = nil
            assert_nothing_raised do
                bbox = Math::BoundingBox3.new( Math::Vector.new( 0.0, 0.0, 0.0 ), Math::Vector.new( 1.0, 1.0, 1.0 ) )
            end
            assert_not_nil( bbox )
            
            assert_raise ArgumentError do
                bbox.contains( nil )
            end
            assert_raise ArgumentError do
                bbox.contains( 3.0 )
            end
            assert_raise ArgumentError do
                bbox.contains( "helpme" )
            end
            assert( bbox.contains( Math::Vector.new( 0.0, 0.0, 0.0 ) ) )
            assert( bbox.contains( Math::Vector.new( 1.0, 1.0, 1.0 ) ) )
            assert( bbox.contains( Math::Vector.new( 0.5, 0.5, 0.5 ) ) )
            assert( !bbox.contains( Math::Vector.new( -0.1, 0.5, 0.5 ) ) )
            assert( !bbox.contains( Math::Vector.new( 0.5, -0.1, 0.5 ) ) )
            assert( !bbox.contains( Math::Vector.new( 0.5, 0.5, -0.1 ) ) )
            assert( !bbox.contains( Math::Vector.new( 3.1, 0.5, 0.5 ) ) )
            assert( !bbox.contains( Math::Vector.new( 0.5, 3.1, 0.5 ) ) )
            assert( !bbox.contains( Math::Vector.new( 0.5, 0.5, 3.1 ) ) )
        end
    end
    
end # Test module
end # Pipeline module

# End of bbox_test.rb
