#
# bsphere_test.rb
# Bounding sphere unit test cases
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 11 March 2008
#

require 'pipeline/math/bsphere'
require 'test/unit'

module Pipeline
module Test

	#
	# == Description
	# BoundingSphere class test cases.
	#
	class BoundingSphereTests < ::Test::Unit::TestCase
		
		def setup
		end
	
		def test_constructor
			assert_raise ArgumentError do
				sph = Math::BoundingSphere.new( '13.0', 4.5 )				
			end
			assert_raise ArgumentError do
				sph = Math::BoundingSphere.new( nil, 3.6 )
			end
			assert_nothing_raised do
				sph = Math::BoundingSphere.new( Math::Vector.new( 0.0, 0.0, 0.0 ), 3.2 )
			end
		end
	end
	
end # Test module
end # Pipeline module

# End of bsphere_test.rb
