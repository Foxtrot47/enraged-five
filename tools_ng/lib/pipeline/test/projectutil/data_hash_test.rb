#
# File:: data_hash_test.rb
# Description:: ProjectUtil Hash function unit tests.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 3 December 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/projectutil/data_hash'
include Pipeline
require 'test/unit'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Test
module ProjectUtil
	
	class HashTests < ::Test::Unit::TestCase
		
		STR_TEST_1 = 'hello world'
		
		def test_hash_u32( )
			assert_equal( 1045060183, Pipeline::ProjectUtil::hash( STR_TEST_1 ) )
			assert_equal( 1045060183, Pipeline::ProjectUtil::hash( STR_TEST_1, Pipeline::ProjectUtil::HASH_STRING_U32 ) )
		end
		
		def test_hash_s16( )
			assert_equal( 15930, Pipeline::ProjectUtil::hash( STR_TEST_1, Pipeline::ProjectUtil::HASH_STRING_S16 ) )
		end
		
		def test_hash_u16( )
			assert_equal( 32766, Pipeline::ProjectUtil::hash( STR_TEST_1, Pipeline::ProjectUtil::HASH_STRING_U16 ) )
		end
	end
	
end # ProjectUtil module
end # Test module
end # Pipeline module

# data_hash_test.rb
