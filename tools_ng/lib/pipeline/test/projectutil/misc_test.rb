#
# File:: misc_test.rb
# Description:: Misc ProjectUtil function unit tests.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 28 November 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/projectutil/misc'
include Pipeline
require 'test/unit'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Test
module ProjectUtil
	
	class ProjectFromFilenamesTest < ::Test::Unit::TestCase
		
		def test_1( )
			files = [ 'x:/jimmy/build/dev/independent/models/plantsmgr.idd' ]
			proj, branch = Pipeline::ProjectUtil::get_project_from_filenames( files )
			
			assert_equal( 'jimmy', proj.name )
			assert_equal( 'dev', branch.name )
		end
	end
	
end # ProjectUtil module
end # Test module
end # Pipeline module

# misc_test.rb
