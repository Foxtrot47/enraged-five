#
# projects_test.rb
# Unit tests
#
# Author::
# Date::
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/util/environment'
require 'test/unit'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Test
	
	#
	# == Description
	# Project class unit test cases.
	#
	class ProjectTest < ::Test::Unit::TestCase
	
		#---------------------------------------------------------------------
		# Constants
		#---------------------------------------------------------------------
		PROJECT_NAME = 'jimmy'
		PROJECT_UINAME = 'Jimmy'
		PROJECT_ROOT = 'x:/jimmy'
	
		def setup( )
			@c = Pipeline::Config.instance()
		
			@project = Pipeline::Project.new( 'jimmy', 'Jimmy', 'x:/jimmy', 
						OS::Path.combine( Globals::instance().toolsroot, 'jimmy', 'bin', 'config.xml' ), 
						'jimmy' )
			assert( @project.is_a?( Pipeline::Project ), 'Invalid Project class created.' )
			assert( !@project.loaded_config, 'Config loaded before being requested to load.' )
			@project.load_config( true )
			assert( @project.loaded_config, 'Config not loaded after request.' )
		end
		
		def test_data( )
			assert_equal( PROJECT_NAME, @project.name, 'Invalid project name.' )
			assert_equal( PROJECT_UINAME, @project.uiname, 'Invalid project uiname.' )
			assert_equal( PROJECT_ROOT, @project.root, 'Invalid project root.' )
		end
		
		def test_branch_data( )
			assert( @project.branches.is_a?( Hash ), 'Invalid project branches Hash.' )
		
			@project.branches.each_pair do |branch_name, branch|
				assert( branch.is_a?( Pipeline::Branch ), "Invalid branch defined #{branch.class}." )
			
				if ( 'dev_migrate' == branch_name ) then
					assert_equal( 'x:/jimmy/build/dev_migrate', branch.build ) 
					assert_equal( 'x:/jimmy/build/dev_migrate/independent', branch.independent ) 
					assert_equal( 'x:/jimmy/build/dev_migrate/common', branch.common ) 
					assert_equal( 'x:/jimmy/build/dev_migrate/common/shaders', branch.shaders ) 
					assert_equal( 'x:/jimmy/jimmy_art', branch.art )
				
				elsif ( 'dev' == branch_name ) then
					assert_equal( 'x:/jimmy/build/dev', branch.build )
					assert_equal( 'x:/jimmy/build/dev/independent', branch.independent ) 
					assert_equal( 'x:/jimmy/build/dev/common', branch.common ) 
					assert_equal( 'x:/jimmy/build/dev/common/shaders', branch.shaders ) 
					assert_equal( 'x:/jimmy/jimmy_art', branch.art )
				
				end
			
				# Test targets
				branch.targets.each_pair do |platform, target|
					assert_equal( branch.object_id, target.branch.object_id, 'Invalid target branch.' )
				end
			end
		end
		
		def test_default_branch_data( )
			assert( @project.default_branch.is_a?( String ), 'No project default branch set.' )
			
			# Test our default data (for compatibility)
			assert_equal( 'x:/jimmy/build/dev', @project.build, 'Error with default branch build path.' )
			assert_equal( 'x:/jimmy/build/dev/independent', @project.independent, 'Error with default branch independent path.' )
			assert_equal( 'x:/jimmy/build/dev/common', @project.common, 'Error with default branch common path.' )
			assert_equal( 'x:/jimmy/build/dev/common/shaders', @project.shaders, 'Error with default branch shaders path.' )
			assert_equal( 'x:/jimmy/src/dev', @project.code, 'Error with default branch code path.' )
			assert_equal( 'x:/rage/dev_north_gta', @project.ragecode, 'Error with default branch ragecode path.' )
			assert_equal( 'x:/jimmy/jimmy_art', @project.art, 'Error with default branch art path.' )
			
			# Test Independent Target (for compatibility)
			assert( @project.ind_target.is_a?( Pipeline::IndependentTarget ), 'Invalid default branch independent target.' )
			assert( @project.ind_target.branch.is_a?( Pipeline::Branch ), 'Invalid default branch independent target branch.' )
			assert( @project.ind_target.project.is_a?( Pipeline::Project ), 'Invalid default branch independent target project.' )
			assert_equal( @project, @project.ind_target.project, 'Default branch independent target project mismatch.' )
			assert_equal( @project.branches[@project.default_branch], @project.ind_target.branch, 'Default branch independent target branch mismatch.' )
			
			# Test Targets data (for compatibility)
			assert( @project.targets.is_a?( Hash ), 'Invalid default branch targets Array.' )
			assert_equal( 'independent', @project.ind_target.platform, 'Invalid default branch independent target platform.' )
			assert( ( @project.targets.size > 0 ), 'No default branch targets, XML parse error?' )
			
			has_ps3 = false
			has_xbox360 = false
			@project.targets.each_pair do |platform, target|
				has_ps3 = true if ( 'ps3' == platform )
				has_xbox360 = true if ( 'xbox360' == platform )
			
				assert( target.branch.is_a?( Pipeline::Branch ), 'Invalid default branch target branch.' )
				assert( target.branch.name == @project.default_branch )
				assert( target.is_a?( Pipeline::Target ), 'Invalid default branch target.' )
			end
			assert( has_ps3, 'No PS3 target defined.' )
			assert( has_xbox360, 'No xbox360 target defined.' )
		end
		
		def test_reset( )
		end
		
		#
		# Project environment testing.  This is a large one as it ensures we
		# have compatibility with tools created before the branch support was
		# added to the project configuration.
		#
		def test_env( )
		
			env = Environment.new()
			@project.fill_env( env )
			env.list()
		end
		
		def test_ind_target( )
		end
		
		def test_targets( )
		end
		
		def test_file_test( )
		end
	end		
	
end # Test module
end # Pipeline module

# End of project_test.rb
