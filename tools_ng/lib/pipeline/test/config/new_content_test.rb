require 'pipeline/config/projects'
require 'pipeline/content/parser'
require 'pipeline/content/content_anim'
require 'pipeline/util/rage'
require 'pipeline/resourcing/content_rpfbuilder'

module Pipeline
        
    
    proj = Pipeline::Config.instance.projects["jimmy"]
	proj.load_config()
    
  
	cp = Content::Parser.new(proj)
	rootcontent = cp.parse_xml("X:/pipedev/jimmy/bin/content/anim.xml")
    
    #pack = Rage::Pack.instance()
    #rb = Rage::Builder.instance()
    #rb.init()
    #rb.shutdown()
   
    rootcontent.children.each do |child| 
        
       
  
        child.children.each do |anim|
            #crpf = Resourcing::ContentRPFBuilder.new()
            #crpf.build( proj, anim )
             Resourcing::ContentRPFBuilder::build( proj, anim )
            #print(anim.to_s + "\n")
            #anim.children.each do |anim2|
             #   print("\t" + anim2.to_s + "\n")
           # end
            
            #print(anim.to_s + "\n")
        end
   
 
     end
    #print(rootcontent.to_s + "\n")

    print("Completed\n")
end