#
# File:: levels_test.rb
# Description:: Levels/Level classes unit tests
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 26 November 2008
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/levels'
require 'test/unit'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Test
	
	#
	# == Description
	# Levels and Level classes unit tests.
	#
	class LevelsTest < ::Test::Unit::TestCase
		
		def setup( )
			@c = Pipeline::Config::instance
			@p = @c.projects['jimmy']
			@p.load_config()
			@b = @p.branches['dev']
			
			@levels = Levels.new( @p, @b, 'x:/jimmy/build/dev/common/data/levels.xml' )
		end
		
		def test_print( )
			@levels.levels.each_pair do |k, v|
				puts "LEVEL: #{k}"
				v.dat.each_pair do |d, files|
					puts "\t#{d} (#{files.size})"
					files.each do |filename|
						puts "\t\t#{filename}"	
					end
				end
			end
		end
	end
	
end # Test module
end # Pipeline module

# levels_test.rb
