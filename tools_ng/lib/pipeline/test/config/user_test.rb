#
# File:: user_test.rb
# Description:: User class unit tests.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 26 August 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/user'
require 'test/unit'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Test

	#
	# == Description
	# Pipeline::User unit test cases.
	#
	class UserTests < ::Test::Unit::TestCase
		def setup( )
			assert_nothing_raised do 
				@user1 = Pipeline::User.new( 'maps' )
			end
			assert_nothing_raised do
				@user2 = Pipeline::User.new( User::FLAG_TOOLS )
			end
		end
		
		def test_is_methods( )
		
			assert( @user1.is_artist() )
			assert( @user1.is_maps() )
			assert( ( not @user1.is_programmer() ) )
			assert( ( not @user1.is_tester() ) )
			assert( ( not @user1.is_designer() ) )
			assert( ( not @user1.is_tools() ) )
			
			assert( @user2.is_artist() )
			assert( @user2.is_maps() )
			assert( @user2.is_programmer() )
			assert( @user2.is_tester() )
			assert( @user2.is_designer() )
		end
	end

end # Test module
end # Pipeline module

# user_test.rb
