#
# File:: config_test.rb
# Description:: Pipeline::Config unit tests
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 7 April 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'test/unit'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Test
	
	class ProjectsTest < ::Test::Unit::TestCase
	
		def setup
			@c = Pipeline::Config.instance
			@c.projects.each_value do |proj|
				assert( proj.is_a?( Pipeline::Project ), "Invalid project in Config array #{proj.class}." )
			
				# Load project configs for those that exist
				proj.load_config() if File.exists?( OS::Path.combine( proj.root, "bin/config.xml" ) )
				next unless proj.loaded_config
	
				puts "Loaded project: #{proj.uiname}."
	
				puts "Name: #{proj.uiname}"
				puts "Project root: #{proj.root}"
				puts "Number of targets: #{proj.targets.length}"
				puts "Targets:"
				proj.targets.each_value do |target| 
					puts "Target: #{target.platform} #{target.enabled} #{target.target}" 
				end			
			end
		end
		
		def test_applications( )
		
			assert( @c.use_xge, 'Use XGE default error.' )
		end
	
		def test_environment( )
			assert( "#{@c.toolsbin}" == @c.envsubst( "$(toolsbin)" ), 
					"$(toolsbin) is not defined correctly." )
			assert( "#{@c.toolsroot}" == @c.envsubst( "$(toolsroot)" ), 
					"$(toolsroot) is not defined correctly." )
		end
			
		def test_log( )
		
			# log_level
			assert( @c.log_level.is_a?( Fixnum ), "Invalid log level, maybe not set in XML parsing? (#{@c.log_level})." )
			assert( @c.log_level >= 0, 'Log level below minimum.' )
			assert( @c.log_level <= 7, 'Log level above maximum.' )
		
			# boolean options
			assert( ( @c.log_trace.is_a?( FalseClass ) or @c.log_trace.is_a?( TrueClass ) ), 
				"Invalid log_trace, maybe not set in XML parsing? (#{@c.log_trace})." )
			assert( ( @c.logmailerrors.is_a?( FalseClass ) or @c.logmailerrors.is_a?( TrueClass ) ), 
				"Invalid logmailerrors, maybe not set in XML parsing? (#{@c.logmailerrors})." )
			assert( ( @c.logtostdout.is_a?( FalseClass ) or @c.logtostdout.is_a?( TrueClass ) ), 
				"Invalid logtostdout, maybe not set in XML parsing? (#{@c.logtostdout})." )
			assert( ( @c.log_generate_html.is_a?( FalseClass ) or @c.log_generate_html.is_a?( TrueClass ) ), 
				"Invalid log_generate_html, maybe not set in XML parsing? (#{@c.log_generate_html})." )
		end

		def test_user( )
		end
	end
	
end # Test module
end # Pipeline module

# End of config_test.rb
