#
# log_test.rb
# Unit tests for Pipeline logging system.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 10 March 2008
#

require 'pipeline/log/log'
require 'test/unit'

module Pipeline
module Test
	
	class LogTest < ::Test::Unit::TestCase
	
		def test_main
		
			log = Pipeline::Log.new("Unit Test Log")
	
			# Now log some stuff...
			log.debug( "Created logger." )
			log.warn( "Erm, not doing anything" )
			log.error( "this is an error" )
			#log.fatal( "fatal exception" )
			#log.flush()
		end
	end
	
end # Test module
end # Pipeline module

# End of log_test.rb
