#
# logxml_test.rb
# Log XML Outputter Unit Tests
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 10 March 2008
#

require 'pipeline/log/log'
require 'pipeline/log/logxml'
require 'test/unit'

module Pipeline
module Test
	
	class LogXMLTests < ::Test::Unit::TestCase
	
		def setup
			name = 'LogXMLTests'
			@log = ::Pipeline::Log.new( name, [ XMLFileOutputter.new(name) ] )
		end
		
		def test_levels
			@log.error( "test_error" )
			@log.info( "test_info" )
		end
	end
	
end # Test module
end # Pipeline module

# End of logxml_test.rb
