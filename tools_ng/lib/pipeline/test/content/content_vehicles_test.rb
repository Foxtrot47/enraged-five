#
# File:: content_vehicles_test.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 4 August 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/util/environment'
include Pipeline
require 'test/unit'

module Pipeline
module Test

	#
	# == Description
	# Vehicles content unit tests.
	#
	class VehicleContentTest < ::Test::Unit::TestCase
	
		CONTENT = 	Pipeline::Project::ContentType::VEHICLES | 
					Pipeline::Project::ContentType::OUTPUT |
					Pipeline::Project::ContentType::PLATFORM
		#CONTENT  =	Pipeline::Project::ContentType::ALL
	
		def setup()
			@p = Config::instance.projects['jimmy']
			@p.load_config( )
			@p.load_content( CONTENT )
			assert( @p.content.is_a?( Pipeline::Content::Base ) )
		end
		
		def test_find_vehicles( )
			@vehicles_group = @p.content.find_first_group( 'vehicles' )
			@vehicles_inner_group = @p.content.find_first_group( 'vehicles_inner' )
			@vehicles_image = @p.content.find_by_script( "'vehiclesimage' == content.xml_type and 'vehicles' == content.name" )
		
			assert( @vehicles_group.is_a?( Pipeline::Content::Group ) )
			assert( @vehicles_inner_group.is_a?( Pipeline::Content::Group ) )
			assert( @vehicles_image.is_a?( Pipeline::Content::VehiclesImage ) )
		end
		
		def test_print()
			@p.content.pretty_print( true )
		end
	end

end # Test module
end # Pipeline module

# content_vehicles_test.rb
