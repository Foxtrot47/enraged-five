#
# File:: content_weapons_test.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 4 August 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/util/environment'
include Pipeline
require 'test/unit'

module Pipeline
module Test

	#
	# == Description
	# Vehicles content unit tests.
	#
	class WeaponsContentTest < ::Test::Unit::TestCase
	
		CONTENT = 	Pipeline::Project::ContentType::WEAPONS | 
					Pipeline::Project::ContentType::OUTPUT |
					Pipeline::Project::ContentType::PLATFORM
	
		def setup()
			@p = Config::instance.projects['jimmy']
			@p.load_config( )
			@p.load_content( CONTENT )
			assert( @p.content.is_a?( Pipeline::Content::Base ) )
		end
		
		def test_find_weapons( )
			@weapons_group = @p.content.find_first_group( 'weapons' )
			@weapons_inner_group = @p.content.find_first_group( 'weapons_inner' )
			@weapons_image = @p.content.find_by_script( "'weaponsimage' == content.xml_type and 'weapons' == content.name" )
		
			assert( @weapons_group.is_a?( Pipeline::Content::Group ) )
			assert( @weapons_inner_group.is_a?( Pipeline::Content::Group ) )
			assert( @weapons_image.is_a?( Pipeline::Content::WeaponsImage ) )
		end
		
		def test_print()
			@p.content.pretty_print( true )
		end
	end

end # Test module
end # Pipeline module

# content_weapons_test.rb
