#
# File:: content_platform_test.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 16 August 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/content/treecore'
require 'pipeline/content/content_core'
require 'pipeline/util/environment'
require 'test/unit'

module Pipeline
module Test

	#
	# == Description
	# Platform content sub-tree unit tests.
	#
	class ContentPlatformTest < ::Test::Unit::TestCase
		
		def setup( )
			@p = Config::instance.projects['jimmy']
			@p.load_config( )
			@p.load_content( )
			assert( @p.content.is_a?( Pipeline::Content::Base ) )
			assert( @p.ind_target )
			
			@platform = @p.content.find_first_group( 'output' )
			assert( @platform.is_a?( Pipeline::Content::Group ) )
			
			@output_360 = @p.content.find_first_group( 'output_xbox360' )
			assert( @output_360.is_a?( Pipeline::Content::Group ) )
	
			@output_ps3 = @p.content.find_first_group( 'output_ps3' )
			assert( @output_ps3.is_a?( Pipeline::Content::Group ) )
		end
		
		def test_print( )
			@output_360.pretty_print( true )
		end
	end

end # Test module
end # Pipeline module

# End of content_test.rb
