#
# File:: content_test.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 16 June 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/content/treecore'
require 'pipeline/content/content_core'
require 'pipeline/util/environment'
require 'test/unit'

module Pipeline
module Test

	#
	# == Description
	# Content system unit tests.
	#
	class ContentTest < ::Test::Unit::TestCase
		
		BRANCH_NAME = 'dev_migrate'
		
		def setup( )
			@p = Config::instance.projects['jimmy']
			@p.load_config( )
			@p.load_content( BRANCH_NAME )
			assert( @p.content.is_a?( Pipeline::Content::Base ) )
			
			@output_360 = @p.content.find_first_group( 'output_xbox360' )
			assert( @output_360.is_a?( Pipeline::Content::Group ), "360 output incorrectly defined as #{@output_360.class}" )
	
			@output_ps3 = @p.content.find_first_group( 'output_ps3' )
			assert( @output_ps3.is_a?( Pipeline::Content::Group ), "PS3 output incorrectly defined as #{@output_360.class}" )
		end
		
		def test_reset_reload( )
			assert( @p.content.is_a?( Pipeline::Content::Base ) )
			@p.reset( )
			assert_equal( nil, @p.content )
			@p.load_content( BRANCH_NAME )
			assert( @p.content.is_a?( Pipeline::Content::Base ) )
			@p.load_content( BRANCH_NAME, true )
			assert( @p.content.is_a?( Pipeline::Content::Base ) )
		end
		
		def test_outdated_360( )
		
			env = Environment.new
			target = @p.branches[BRANCH_NAME].targets['xbox360']
			target.fill_env( env )
		
			env.list()
		
			outdated_files = []
			puts "XBOX360 Outdated Files:"
			@output_360.walk do |c|
				next unless ( c.is_a?( Pipeline::Content::File ) )
				
				puts "OUTDATED: #{c.filename}" if c.outdated?( env )
				outdated_files << c.filename if c.outdated?( env )
			end
			assert( outdated_files.size > 0, "No outdated files." )
		end
		
		def test_outdated_ps3( )
				
			env = Environment.new
			target = @p.branches[BRANCH_NAME].targets['ps3']
			target.fill_env( env )
		
			outdated_files = []
			puts "PS3 Outdated Files:"
			@output_ps3.walk do |c|
				next unless ( c.is_a?( Pipeline::Content::File ) )
				
				puts "OUTDATED: #{c.filename}" if c.outdated?( env )
				outdated_files << c.filename if c.outdated?( env )
			end
			assert( outdated_files.size > 0, "No outdated files." )
		end
	end

	#
	# == Description
	# Custom content tree tests (saves loading a whole project tree).
	#
	class ContentTestCustom < ::Test::Unit::TestCase
	
		def setup( )
		
			@contenttree = Pipeline::Content::Group.new( 'root', 'x:/directory' )
			@contenttree.add_child( Pipeline::Content::File.new( 'test', 'textfiles', 'txt' ) )
			@contenttree.add_child( Pipeline::Content::File.new( 'test2', 'textfiles', 'txt' ) )
			
			subgroup = Pipeline::Content::Group.new( 'subgroup', 'sub' )
			subgroup.add_child( Pipeline::Content::File.new( 'blahfile', '', 'bmp' ) )
			subgroup.add_child( Pipeline::Content::File.new( 'blahfile2', '', 'bmp' ) )
			@contenttree.add_child( subgroup )
			
			subsubgroup = Pipeline::Content::Group.new( 'subsubgroup', 'subsub' )
			subsubgroup.add_child( Pipeline::Content::File.new( 'subfile1', '', 'img' ) )
			subsubgroup.add_child( Pipeline::Content::File.new( 'subfile2', '', 'img' ) )
			subsubgroup.add_child( Pipeline::Content::File.new( 'subfile3', '', 'img' ) )
			subgroup.add_child( subsubgroup )
			
			subgroup2 = Pipeline::Content::Group.new( 'subgroup2', 'sub2' )
			subgroup2.add_child( Pipeline::Content::File.new( 'subfile3333', '', 'img' ) )
			@contenttree.add_child( subgroup2 )
		end
		
		def teardown( )
			
			@contenttree.pretty_print( )
		end
		
		def test_find_first( )
		
			file = @contenttree.find_first( 'hjshjfile', 'file' )
			assert_equal( nil, file, 'Non-existance file found!!' )
		
			file = @contenttree.find_first( 'blahfile', 'file' )
			assert( file.is_a?( Pipeline::Content::File ), "Did not find correct node type: #{file.class}" )
			assert_equal( 'blahfile', file.name )
			assert_equal( 'bmp', file.extension )
			
			file = @contenttree.find_first( 'blahfile2' )
			assert( file.is_a?( Pipeline::Content::File ), "Did not find correct node type: #{file.class}" )
			assert_equal( 'blahfile2', file.name )
			assert_equal( 'bmp', file.extension )

			file = @contenttree.find_first( 'subfile2' )
			assert( file.is_a?( Pipeline::Content::File ), "Did not find correct node type: #{file.class}" )
			assert_equal( 'subfile2', file.name )
			assert_equal( 'img', file.extension )
			
			file = @contenttree.find_first( 'subfile3333' )
			assert( file.is_a?( Pipeline::Content::File ), "Did not find correct node type: #{file.class}" )
			assert_equal( 'subfile3333', file.name )
			assert_equal( 'img', file.extension )
		end
		
		def fhfhtest_find_first_group( )
		
			group = @contenttree.find_first_group( 'hakdsjk' )
			assert_equal( nil, group, 'Non-existance group found!!' )
			
			group = @contenttree.find_first_group( 'subgroup' )
			assert_equal( 'subgroup', group.name )
			assert_equal( 'group', group.xml_type )
			assert_equal( 2, group.children.size() )
			
			group = @contenttree.find_first_group( 'subsubgroup ' )
			assert_equal( 'subgroup', group.name )
			assert_equal( 'group', group.xml_type )
			assert_equal( 3, group.children.size() )
		end
	end

end # Test module
end # Pipeline module

# End of content_test.rb
