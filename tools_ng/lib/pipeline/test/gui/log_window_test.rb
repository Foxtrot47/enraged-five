#
# File:: log_window_test.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 16 January 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/gui/log_window'
require 'pipeline/log/log'
require 'test/unit'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------

module Pipeline
module Test
module GUI

	#
	# == Description
	# Log window test cases.
	#
	class LogWindowTest < ::Test::Unit::TestCase
	
		def test_autopopup( )
			Pipeline::GUI::LogWindow::show_dialog( true ) do
				Pipeline::LogSystem::instance().rootlog.error( "Testing in block" )
				Pipeline::LogSystem::instance().rootlog.warn( "Testing in block" )
				Pipeline::LogSystem::instance().rootlog.info( "Testing in block" )
				Pipeline::LogSystem::instance().rootlog.debug( "Testing in block" )
				
				#Pipeline::LogSystem::instance().rootlog.error( "Testing in block2" )
				Pipeline::LogSystem::instance().rootlog.warn( "Testing in block2" )
				Pipeline::LogSystem::instance().rootlog.info( "Testing in block2" )
				Pipeline::LogSystem::instance().rootlog.debug( "Testing in block2" )
			end	
		end

		def test_normal( )
			Pipeline::GUI::LogWindow::show_dialog( ) do
				Pipeline::LogSystem::instance().rootlog.error( "Testing in block" )
				Pipeline::LogSystem::instance().rootlog.warn( "Testing in block" )
				Pipeline::LogSystem::instance().rootlog.info( "Testing in block" )
				Pipeline::LogSystem::instance().rootlog.debug( "Testing in block" )
				
				Pipeline::LogSystem::instance().rootlog.error( "Testing in block2" )
				Pipeline::LogSystem::instance().rootlog.warn( "Testing in block2" )
				Pipeline::LogSystem::instance().rootlog.info( "Testing in block2" )
				Pipeline::LogSystem::instance().rootlog.debug( "Testing in block2" )
			end
		end
	end

end # GUI module
end # Test module
end # Pipeline module

# log_window_test.rb
