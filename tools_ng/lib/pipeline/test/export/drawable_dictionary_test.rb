#
# File:: pipeline/test/export/drawable_dictionary_test.rb
# Description:: Drawable Dictionary Class Unit Test Cases
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 6 August 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/export/collada'
require 'pipeline/export/drawable_dictionary'
require 'pipeline/export/materials'
require 'pipeline/os/path'
require 'pipeline/util/rage'
include Pipeline::Export
include Pipeline
require 'test/unit'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Test
module Export

	#
	# == Description
	# Unit test cases for Pipeline::Export::DrawableDictionary class.
	#
	class DrawableDictionaryTest < ::Test::Unit::TestCase

		FILE_OUTPUT = '$(toolsbin)/pipeline/test/data/collada/'
		FILE_DAEDOCS = [
			'$(toolsbin)/pipeline/test/data/collada/Cablesnow.DAE',
			'$(toolsbin)/pipeline/test/data/collada/alpRestSkihut.DAE'
		]

		def setup( )
			@c = Pipeline::Config::instance()
			@output = @c.envsubst( FILE_OUTPUT )
			@drawdict_name = 'test_drawdict'
			@doc_files = []
			FILE_DAEDOCS.each do |filename|
				@doc_files << @c.envsubst( filename )
			end
			assert( File::directory?( @output ), "Output directory #{FILE_OUTPUT} does not exist." )
		
			@project = Pipeline::Config::instance().projects['jimmy']
			assert( @project.enabled, "Project #{@project.uiname} is not enabled." )
			@r = RageUtils.new( @project )

			Collada::init( )
			@docs = []
			@doc_files.each do |file|
				assert( File::exists?( file ), "Input file #{file} does not exist." )
				doc = ColladaDocument.new( file )
				doc.pre_process( )
				@docs << doc
			end
		end

		def teardown( )
			@docs.each do |doc| doc.close(); end
			Pipeline::Export::Collada::shutdown( )
		end

		def test_export_all( )
			drawdict = DrawableDictionary.new( @drawdict_name, @docs )
			drawdict.export_all( @output )
			drawdict.pack( @r, @output )

			output_pack = OS::Path::combine( @output, @drawdict_name + '.idd' )
			assert( File::exists?( output_pack ), "Output pack #{output_pack} does not exist." )
		end
	end

end # Export module
end # Test module
end # Pipeline module

# pipeline/test/export/drawable_dictionary_test.rb
