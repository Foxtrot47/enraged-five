#
# File:: static_collision_test.rb
# Description:: Static collision export unit tests
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 22 July 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/export/collada'
require 'pipeline/export/static_collision'
require 'pipeline/os/path'
require 'pipeline/util/rage'
include Pipeline::Export
include Pipeline
require 'test/unit'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Test
module Export
	
	#
	# == Description
	# Unit test cases for Pipeline::Export::Geometry class.
	#
	class StaticCollisionTest < ::Test::Unit::TestCase
		
		FILE_OUTPUT = '$(toolsbin)/pipeline/test/data/collada/'
		FILE_DAEDOC1 = '$(toolsbin)/pipeline/test/data/collada/static_collision01.DAE'
		FILE_SECTIONS1 = '$(toolsbin)/pipeline/test/data/collada/static_collision01.xml'
		
		def setup( )
			@c = Pipeline::Config::instance()
			@filename = @c.envsubst( FILE_DAEDOC1 )
			@sections_filename = @c.envsubst( FILE_SECTIONS1 )
			@output = @c.envsubst( FILE_OUTPUT )
			assert( File::directory?( @output ), "Output directory #{FILE_OUTPUT} does not exist." )
			assert( File::exists?( @filename ), "Input file #{FILE_DAEDOC1} does not exist.  Tests will fail." )
			assert( File::exists?( @sections_filename ), "Input file #{FILE_SECTIONS1} does not exist.  Tests will fail." )			

			@project = @c.projects['jimmy']
			assert( @project.enabled, "Project #{@project.uiname} is not enabled." )
			@r = RageUtils.new( @project )

			Pipeline::Export::Collada::init( )
			@file = ColladaDocument::new( @filename )
			@file.pre_process( )
			assert( @file, "Failed to create ColladaDocument." )
		end
		
		def teardown( )
			@file.close( ) if ( @file.is_a?( Pipeline::Export::ColladaDocument ) )
			Pipeline::Export::Collada::shutdown( )
		end

		def test_export_all( )
			static_coll = StaticCollision.new( @file, @sections_filename )
			static_coll.export_all( @output ) do |file, status, pct|
				puts "File: #{file} #{status} #{pct}"
				assert( status, "Export of #{file} failed." )
				assert( File::exists?( file ), "Export of #{file} didn't produce the output file." )
				assert( pct >= 0.0, "Block pct argument out of range (#{pct})." )
				assert( pct <= 1.0, "Block pct argument out of range (#{pct})." )
			end
			static_coll.pack( @r, @output )
		end
	end

end # Export module
end # Test module
end # Pipeline module

# static_collision_test.rb
