#
# File:: pipeline/test/export/util/geometry_test.rb
# Description:: Geometry utility method test cases.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 22 June 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/export/collada'
require 'pipeline/export/materials'
require 'pipeline/export/util/geometry'
require 'pipeline/os/path'
include Pipeline::Export::Util
include Pipeline::Export
include Pipeline
require 'test/unit'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Test
module Export

	#
	# == Description
	# Unit test cases for Pipeline::Export::Geometry class.
	#
	class GeometryTest < ::Test::Unit::TestCase
	
		FILE_OUTPUT = '$(toolsbin)/pipeline/test/data/collada/'
		FILE_DAEDOC1 = '$(toolsbin)/pipeline/test/data/collada/Cablesnow.DAE'

		def setup( )
			@filename = Pipeline::Config::instance().envsubst( FILE_DAEDOC1 )
			@output = Pipeline::Config::instance().envsubst( FILE_OUTPUT )
			assert( File::directory?( @output ), "Output directory #{FILE_OUTPUT} does not exist." )
			assert( File::exists?( @filename ), "Input file #{FILE_DAEDOC1} does not exist.  Tests will fail." )
		
			Collada::init( )
			@file = ColladaDocument::new( @filename )
			@file.pre_process( )
			assert( @file, "Failed to create ColladaDocument." )
		end

		def teardown( )
			# Delete created test data
			@output_file = OS::Path::combine( @output, OS::Path::get_basename( FILE_DAEDOC1 ) ) + '.mesh'
			File::delete( @output_file ) if ( File::exists?( @output_file ) )

			@file.close( )

			Collada::shutdown( )
		end

		def test_triangulate( )
			Export::Util::Geometry::triangulate_all!( @file )
		end

		def test_splitonmaterial( )
			Export::Util::Geometry::split_geometry_on_material( @file )
		end
	
		def test_export_all( )
			@output_file = OS::Path::combine( @output, OS::Path::get_basename( FILE_DAEDOC1 ) ) + '.mesh'
			assert( (not File::exists?( @output_file )), "Output mesh (#{@output_file}) already exists." )
			Export::Util::Geometry::triangulate_all!( @file )
			Export::Util::Geometry::export_all( @file, @output )

			assert( File::exists?( @output_file ), "Output mesh #{@output_file} was not created." )
		end
	end

end # Export module
end # Test module
end # Pipeline module

# pipeline/test/export/util/geometry_test.rb
