#
# File:: pipeline/test/export/drawable_test.rb
# Description:: Drawable Class Unit Test Cases
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 26 June 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/export/collada'
require 'pipeline/export/drawable'
require 'pipeline/export/materials'
require 'pipeline/os/path'
require 'pipeline/util/rage'
include Pipeline::Export
include Pipeline
require 'test/unit'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Test
module Export
	
	#
	# == Description
	# Unit test cases for Pipeline::Export::Drawable class.
	#
	class DrawableTest < ::Test::Unit::TestCase
		
		FILE_OUTPUT = '$(toolsbin)/pipeline/test/data/collada/'
		FILE_DAEDOC1 = '$(toolsbin)/pipeline/test/data/collada/Cablesnow.DAE'
		FILE_DAEDOC2 = '$(toolsbin)/pipeline/test/data/collada/alpRestSkihut.DAE'
		
		def setup( )
			@filename = Pipeline::Config::instance().envsubst( FILE_DAEDOC1 )
			@output = Pipeline::Config::instance().envsubst( FILE_OUTPUT )
			assert( File::directory?( @output ), "Output directory #{FILE_OUTPUT} does not exist." )
			assert( File::exists?( @filename ), "Input file #{FILE_DAEDOC1} does not exist.  Tests will fail." )
			
			@project = Pipeline::Config::instance().projects['jimmy']
			assert( @project.enabled, "Project #{@project.uiname} is not enabled." )
			@r = RageUtils.new( @project )

			Collada::init( )
			@file = ColladaDocument::new( @filename )
			@file.pre_process( )
			assert( @file, "Failed to create ColladaDocument." )
		end
		
		def teardown( )	
			@file.close( )
			Collada::shutdown( )
		end
		
		def test_export_all( )
			drawable_export = Drawable.new( @file )
			drawable_export.export_all( @output )
			drawable_export.pack( @r, @output )

			entity_file = OS::Path::combine( @output, OS::Path::get_basename( @filename ), 'entity.type' )
			assert( File::exists?( entity_file ), "Output entity.type #{entity_file} does not exist." )

			output_mesh = OS::Path::combine( @output, OS::Path::get_basename( @filename ), OS::Path::get_basename( @filename ) ) + '.mesh'
			assert( File::exists?( output_mesh ), "Output mesh #{output_mesh} does not exist." )

			output_pack = OS::Path::combine( @output, OS::Path::get_basename( @filename ) + '.idr' )
			assert( File::exists?( output_pack ), "Output pack #{output_pack} does not exist." )
		end
	end
	
end # Export module
end # Test module
end # Pipeline module

# pipeline/test/export/drawable_test.rb
