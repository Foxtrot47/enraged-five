#
# File:: attr_data_store_test.rb
# Description:: 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 22 June 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/export/attr_data_store'
include Pipeline::Export
require 'test/unit'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Test

	#
	# == Description
	# Unit test cases for Pipeline::Export::AttrDataStore class.
	# 
	class AttrDataStoreTest < ::Test::Unit::TestCase
		
		FILE_OUTPUT = '$(toolsbin)/pipeline/test/data/collada/'
		FILE_DAEDOC1 = '$(toolsbin)/pipeline/test/data/collada/Cablesnow.DAE'

		def setup( )
			@filename = Pipeline::Config::instance().envsubst( FILE_DAEDOC1 )
			@output = Pipeline::Config::instance().envsubst( FILE_OUTPUT )
			assert( File::directory?( @output ), "Output directory #{FILE_OUTPUT} does not exist." )
			assert( File::exists?( @filename ), "Input file #{FILE_DAEDOC1} does not exist.  Tests will fail." )
			
			Pipeline::Export::Collada::init( )
			@file = Pipeline::Export::ColladaDocument::new( @filename )
			assert( @file, "Failed to create ColladaDocument." )
		end

		def teardown( )
			Pipeline::Export::Collada::shutdown( )
		end
	
		def test_single_attr( )
			@file.walk do |node|
				next unless ( AttrDataStore::has_attr_class( node, 'Gta Object' ) )

				assert( AttrDataStore::has_attr_class( node, 'Gta Object' ), "Wrong attribute class." )
				assert_equal( 299.0, AttrDataStore::get_attr_value( node, 'LOD distance' )	)			
			end
		end

		def test_multiple_attrs( )
			@file.walk do |node|
				next unless ( AttrDataStore::has_attr_class( node, 'Gta Object' ) )
				
				attrs = AttrDataStore::get_attrs( node )
				assert( attrs.is_a?( Hash ), "Attributes container is not a Hash object." )
				attrs.keys.sort.each do |k|
					assert_not_equal( nil, attrs[k], "Attribute #{k} is nil." )
				end
			end
		end
	end

end # Test module
end # Pipeline module

# attr_data_store_test.rb
