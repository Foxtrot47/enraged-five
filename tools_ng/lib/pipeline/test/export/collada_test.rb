#
# File:: collada_test.rb
# Description:: Collada and ColladaDocument unit tests.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 22 July 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/export/collada'
require 'pipeline/os/path'
include Pipeline::Export
include Pipeline
require 'test/unit'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Test
module Export

	# Shared test case data.
	module SharedData
		FILE_DAEDOC1 = Pipeline::Config::instance().envsubst( '$(toolsbin)/pipeline/test/data/collada/Cablesnow.DAE' )
		FILE_DAEDOC2 = Pipeline::Config::instance().envsubst( '$(toolsbin)/pipeline/test/data/collada/static_collision01.DAE' )
		FILE_DAEDOC3 = Pipeline::Config::instance().envsubst( '$(toolsbin)/pipeline/test/data/collada/Alp_Rest03.DAE' )
		FILE_DAEDOC4 = Pipeline::Config::instance().envsubst( '$(toolsbin)/pipeline/test/data/collada/Alprestcable02.DAE' )
	end

	#
	# == Description
	# Collada class unit tests.
	#
	class ColladaTest < ::Test::Unit::TestCase
		include SharedData
		
		def setup( )
			assert( File::exists?( FILE_DAEDOC1 ), "Input file #{FILE_DAEDOC1} does not exist." )
			assert( File::exists?( FILE_DAEDOC2 ), "Input file #{FILE_DAEDOC2} does not exist." )
			assert( File::exists?( FILE_DAEDOC3 ), "Input file #{FILE_DAEDOC3} does not exist." )
			assert( File::exists?( FILE_DAEDOC4 ), "Input file #{FILE_DAEDOC4} does not exist." )
		
			assert( Collada::init(), "COLLADA failed to initialise." )
		end

		def teardown( )
			assert( Collada::shutdown(), "COLLADA failed to shutdown." )
		end

		def test_version( )
			assert_equal( Collada::EXPECTED_VERSION, Collada::version(), "COLLADA version check failed." )
		end

		def test_has_geometry( )
		end

		def test_has_controller( )
		end

		def test_has_local_mesh( )
		end

		def test_has_local_collision( )
		end

		def test_get_rage_technique( )
		end
	end

	#
	# == Description
	# ColladaDocument class unit tests.
	#
	class ColladaDocumentTest < ::Test::Unit::TestCase
		include SharedData

		def setup( )
			assert( File::exists?( FILE_DAEDOC1 ), "Input file #{FILE_DAEDOC1} does not exist." )
			assert( File::exists?( FILE_DAEDOC2 ), "Input file #{FILE_DAEDOC2} does not exist." )
			assert( File::exists?( FILE_DAEDOC3 ), "Input file #{FILE_DAEDOC3} does not exist." )
			assert( File::exists?( FILE_DAEDOC4 ), "Input file #{FILE_DAEDOC4} does not exist." )
			
			assert( Collada::init(), "COLLADA failed to initialise." )
		end
		
		def teardown( )
			assert( Collada::shutdown(), "COLLADA failed to shutdown." )
		end

		# Test case for method_missing functionality; needs to pass calls
		# down to the underlying document.
		def test_method_missing()
		end
	end

end # Export module
end # Test module
end # Pipeline module

# collada_test.rb
