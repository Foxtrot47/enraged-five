#
# mapidefile_test.rb
# Map IDE File Loader test cases
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 11 March 2008
#

require 'pipeline/game/mapidefile'
require 'test/unit'

module Pipeline
module Test

	#
	# == Description
	# Unit test cases for MapIDEFile class.
	#
	class MapIDEFileTests < ::Test::Unit::TestCase
	
		def setup
			
			@ide1 = Pipeline::Game::MapIDEFile.new( DATA_FILE_1 )
			@ide2 = Pipeline::Game::MapIDEFile.new( DATA_FILE_2 )
		end
		
		def test_main
	
			assert( @ide1.object_definitions.size > 0 )
			puts "BRONX_E2"
			puts "Num object defs:        #{@ide1.object_definitions.size}"
			puts "Num time object defs:   #{@ide1.time_object_definitions.size}"
			puts "Num anim object defs:   #{@ide1.anim_definitions.size}"
			puts "Num tanim objects defs: #{@ide1.time_anim_definitions.size}"
			puts "Num MILO object defs:   #{@ide1.milo_object_definitions.size}"
			puts "Num 2DFX object defs:   #{@ide1.fx2d_definitions.size}"
			puts "Num Parent TXD defs:    #{@ide1.parent_txd_definitions.size}"
			
			assert( @ide2.object_definitions.size > 0 )
			puts "MANHAT_09"
			puts "Num object defs:        #{@ide2.object_definitions.size}"
			puts "Num time object defs:   #{@ide2.time_object_definitions.size}"
			puts "Num anim object defs:   #{@ide2.anim_definitions.size}"
			puts "Num tanim objects defs: #{@ide2.time_anim_definitions.size}"
			puts "Num MILO object defs:   #{@ide2.milo_object_definitions.size}"
			puts "Num 2DFX object defs:   #{@ide1.fx2d_definitions.size}"
			puts "Num Parent TXD defs:    #{@ide2.parent_txd_definitions.size}"
			
			@ide2.anim_definitions.each do |animdef|
			
				puts "Anim: #{animdef.name} #{animdef.txd_name} #{animdef.anim_filename}"
			end
		end
		
		#---------------------------------------------------------------------
		# Private Data
		#---------------------------------------------------------------------
		
	private
	
		DATA_FILE_1 = 'x:/gta/build/independent/data/maps/east/bronx_e2.ide'
		DATA_FILE_2 = 'x:/gta/build/independent/data/maps/manhat/manhat09.ide'
	
	end	
		
end # Test module
end # Pipeline module

# End of mapidefile_test.rb
