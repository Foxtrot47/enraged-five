#
# iplfile_test.rb
# IPL File Loader Test Cases
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 11 March 2008
#

require 'pipeline/game/iplfile'
require 'test/unit'

module Pipeline
module Test
	
	#
	# == Description
	# Unit test cases for IDEFile class.
	#
	class IPLFileTests < ::Test::Unit::TestCase
	
		def setup
			
			@ipl1 = Pipeline::Game::IPLFile.new( DATA_FILE_1 )
			@ipl2 = Pipeline::Game::IPLFile.new( DATA_FILE_2 )
		end
		
		def test_main
	
			assert( @ipl1.instances.size > 0 )
			puts "BRONX_E2"
			puts "Num insts: #{@ipl1.instances.size}"
			@ipl1.instances.each do |inst|
			
				puts "\tInst: #{inst.name} flags:0x#{inst.flags.to_s(16)}"
			end
			assert( @ipl2.instances.size > 0 )
			puts "MANHAT_09"
			puts "Num insts: #{@ipl2.instances.size}"
			@ipl2.instances.each do |inst|
			
				puts "\tInst: #{inst.name} flags:0x#{inst.flags.to_s(16)}"
			end			
		end
		
		#---------------------------------------------------------------------
		# Private Data
		#---------------------------------------------------------------------
		
	private
	
		DATA_FILE_1 = 'x:/gta/build/independent/data/maps/east/bronx_e2.ipl'
		DATA_FILE_2 = 'x:/gta/build/independent/data/maps/manhat/manhat09.ipl'
	end
	
end # Test module
end # Pipeline module

# End of iplfile_test.rb
