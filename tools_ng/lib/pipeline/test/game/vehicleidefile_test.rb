#
# vehicleidefile_test.rb
# Vehicles IDE File Loader test cases
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 11 March 2008
#

require 'pipeline/game/vehicleidefile'
require 'test/unit'

module Pipeline
module Test

	#
	# == Description
	# Unit test cases for VehicleIDEFile class.
	#
	class VehicleIDEFileTests < ::Test::Unit::TestCase
	
		def setup
			
			@ide1 = Pipeline::Game::VehicleIDEFile.new( DATA_FILE_IDE_1, DATA_FILE_IMG_1 )
		end
		
		def test_main
	
			assert( @ide1.vehicle_definitions.size > 0 )
			puts "VEHICLES"
			puts "\tNum vehicle defs: #{@ide1.vehicle_definitions.size}"
		end
		
		#---------------------------------------------------------------------
		# Private Data
		#---------------------------------------------------------------------
		
	private
	
		DATA_FILE_IDE_1 = 'x:/gta/build/common/data/vehicles.ide'
		DATA_FILE_IMG_1 = 'x:/gta/build/independent/data/vehicles.ide'
	
	end
		
end # Test module
end # Pipeline module

# End of vehicleidefile_test.rb
