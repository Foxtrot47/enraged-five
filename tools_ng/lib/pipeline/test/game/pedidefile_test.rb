#
# pedidefile_test.rb
# Peds IDE File Loader test cases
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 11 March 2008
#

require 'pipeline/game/pedidefile'
require 'test/unit'

module Pipeline
module Test

	#
	# == Description
	# Unit test cases for PedIDEFile class.
	#
	class PedIDEFileTests < ::Test::Unit::TestCase
	
		def setup
			
			@ide1 = Pipeline::Game::PedIDEFile.new( DATA_FILE_IDE_1, DATA_FILE_IMG_1 )
		end
		
		def test_main
	
			assert( @ide1.ped_definitions.size > 0 )
			puts "VEHICLES"
			puts "\tNum ped defs: #{@ide1.ped_definitions.size}"
			puts "\tNum agrps defs: #{@ide1.ped_audio_groups.size}"
		end
		
		#---------------------------------------------------------------------
		# Private Data
		#---------------------------------------------------------------------
		
	private
	
		DATA_FILE_IDE_1 = 'x:/gta/build/common/data/peds.ide'
		DATA_FILE_IMG_1 = 'x:/gta/build/independent/data/componentpeds.ide'
	
	end
		
end # Test module
end # Pipeline module

# End of pedidefile_test.rb
