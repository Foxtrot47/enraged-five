#
# defaultidefile_test.rb
# Default IDE File Loader Test Cases
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 11 March 2008
#

require 'pipeline/game/defaultidefile'
require 'test/unit'

module Pipeline
module Test
	
	#
	# == Description
	# Unit test cases for MapIDEFile class.
	#
	class DefaultIDEFileTests < ::Test::Unit::TestCase
	
		def setup
			
			@ide1 = Pipeline::Game::DefaultIDEFile.new( DATA_FILE_1 )
		end
		
		def test_main
	
			assert( @ide1.weapon_definitions.size > 0 )
			puts "DEFAULT"
			puts "Num weapon defs:      #{@ide1.weapon_definitions.size}"
		end
		
		#---------------------------------------------------------------------
		# Private Data
		#---------------------------------------------------------------------
		
	private
	
		DATA_FILE_1 = 'x:/gta/build/common/data/default.ide'
	end
	
end # Test module
end # Pipeline module

# End of defaultidefile_test.rb
