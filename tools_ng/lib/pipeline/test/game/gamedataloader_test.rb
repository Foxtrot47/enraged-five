#
# gamedataloader_test.rb
# Game Data Loader class unit tests
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 10 March 2008
#

require 'pipeline/game/gamedataloader'
require 'test/unit'

module Pipeline
module Test

    #
    # == Description
    # GameDataLoader class test cases.
    #
    class GameDataLoaderTests < ::Test::Unit::TestCase
    
        def setup
        
            @project = Config.instance.projects['gta4']
            @project.load_config()
            @gamedata = Game::GameDataLoader.new( @project )
            @gamedata.load_ides()
            @gamedata.load_ipls()
        end
        
        def test_ides_ipls
        
            assert( @gamedata.project_ides.size > 0, 'No IDE files loaded for project.' )
            assert( @gamedata.project_ipls.size > 0, 'No IPL files loaded for project.' )
        end
    end	
	
end # Test module
end # Pipeline module

# End of gamedataloader_test.rb
