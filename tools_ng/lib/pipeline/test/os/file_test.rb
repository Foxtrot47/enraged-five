#
# File:: file_test.rb
# Description:: File unit tests
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 28 July 2008
#

require 'pipeline/os/file'
include Pipeline::OS
require 'test/unit'

module Pipeline
module Test

	#
	# == Description
	# FindEx unit tests.
	#
	class FindExTests < ::Test::Unit::TestCase
		
		def test_find_files_recurse()
			assert_equal( [], FindEx.find_files_recurse( 'c:/windows/windows/*.*' ) )
			files = FindEx.find_files_recurse( 'c:/windows/*.dll' )
			files.each do |f|
				assert( ( not File.directory?( f ) ), 'Directory returned by FindEx::find_files_recurse' )
			end
			assert( files.size > 0 )
		end
		
		def test_find_files()
			assert_equal( [], FindEx.find_files( 'c:/windows/windows/*.*' ) )
			files = FindEx.find_files( 'c:\windows\*.bmp' )
			files.each do |f|
				assert( ( not File.directory?( f ) ), 'Directory returned by FindEx::find_files' )
			end
			assert( files.size > 0 )
		end
		
		def test_find_dirs()
			assert_equal( [], FindEx.find_dirs( 'c:/windows/windows' ) )
			assert( FindEx.find_dirs( 'c:/windows' ).size > 0 )
		end
	end

end # Test module
end # Pipeline module

# file_test.rb
