#
# path_test.rb
# Unit tests for Pipeline::OS::Path class
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 10 March 2008
#

require 'pipeline/os/path'
require 'test/unit'

module Pipeline
module Test

	#-------------------------------------------------------------------------
	# Unit Test Classes
	#-------------------------------------------------------------------------
	class PathTest < ::Test::Unit::TestCase
	
		#---------------------------------------------------------------------
		# Test Path Constants (don't need to exist as its just string changes)
		#---------------------------------------------------------------------	
	
		TEST_PATH_01 = 'X:\\test\\path.txt'
		TEST_PATH_02 = 'Z:\\test.txt'
		TEST_PATH_03 = 'X:\\tools_release'
		TEST_PATH_04 = 'X:\\tools_release\\'
		TEST_PATH_05 = 'z:/tools_release/'
		TEST_PATH_06 = 'file.txt'
		TEST_PATH_07 = '\\shared_unc1\test_file.txt'
		TEST_PATH_08 = '//SHARED_UNC1/test_file.txt'
		TEST_PATH_09 = 'c:\\windows\\notepad.exe'
	
		#---------------------------------------------------------------------
		# Test Path Constants (normalised)
		#---------------------------------------------------------------------		
	
		TEST_PATH_01_NORMALISED = 'x:/test/path.txt'
		TEST_PATH_02_NORMALISED = 'z:/test.txt'
		TEST_PATH_03_NORMALISED = 'x:/tools_release'
		TEST_PATH_04_NORMALISED = 'x:/tools_release/'
		TEST_PATH_07_NORMALISED = '//shared_unc1/test_file.txt'
		TEST_PATH_08_NORMALISED = '//shared_unc1/test_file.txt'
		
	
		#---------------------------------------------------------------------
		# Test Path Constants (stripDrive)
		#---------------------------------------------------------------------		
	
		TEST_PATH_01_STRIPDRIVE = 'test/path.txt'
		TEST_PATH_02_STRIPDRIVE = 'test.txt'	
	
		#---------------------------------------------------------------------
		# Test Path Constants (combine)
		#---------------------------------------------------------------------	
		
		TEST_PATH_03_06_COMBINE	= 'x:/tools_release/file.txt'
		TEST_PATH_04_06_COMBINE	= 'x:/tools_release/file.txt'
		TEST_PATH_05_06_COMBINE	= 'z:/tools_release/file.txt'

		#---------------------------------------------------------------------
		# Test Path Constants (get_directory)
		#---------------------------------------------------------------------	
		
		TEST_PATH_01_DIRECTORY	= 'x:/test'
		TEST_PATH_02_DIRECTORY	= 'z:/'
		TEST_PATH_07_DIRECTORY	= '//shared_unc1'
		TEST_PATH_08_DIRECTORY	= '//shared_unc1'
		
		#---------------------------------------------------------------------
		# Test Methods
		#---------------------------------------------------------------------		
	
		def test_replace_ext
			
			assert_equal( 'filename.name', OS::Path.replace_ext( 'filename.ext', 'name' ) )
			assert_equal( 'filename.ide', OS::Path.replace_ext( 'filename.ext', 'ide' ) )
		end
	
		def test_get_basename

			assert_equal( 'filename', OS::Path::get_basename( 'x:/filename' ) )
			assert_equal( 'filename', OS::Path::get_basename( 'x:/filename.txt' ) )
			assert_equal( 'filename', OS::Path::get_basename( 'x:/directory/filename' ) )
			assert_equal( 'filename', OS::Path::get_basename( 'x:/directory/filename.txt' ) )
			assert_equal( OS::Path::get_filename( 'x:/filename' ), OS::Path::get_basename( 'x:/filename' ) )
			assert_not_equal( OS::Path::get_filename( 'x:/filename.txt' ), OS::Path::get_basename( 'x:/filename.txt' ) )
		end

		def test_get_directory
		
			assert_equal( TEST_PATH_01_DIRECTORY, OS::Path.get_directory( TEST_PATH_01 ),
					"TEST_PATH_01 failed get_directory test." )
			assert_equal( TEST_PATH_02_DIRECTORY, OS::Path.get_directory( TEST_PATH_02 ),
					"TEST_PATH_02 failed get_directory test." )
			assert_equal( TEST_PATH_07_DIRECTORY, OS::Path.get_directory( TEST_PATH_07 ),
					"TEST_PATH_07 failed get_directory test." )
			assert_equal( TEST_PATH_08_DIRECTORY, OS::Path.get_directory( TEST_PATH_08 ),
					"TEST_PATH_08 failed get_directory test." )
		end
	
		def test_normalise
		
			assert_equal( TEST_PATH_01_NORMALISED, OS::Path.normalise( TEST_PATH_01 ),
					"TEST_PATH_01 failed normalise test." )
			assert_equal( TEST_PATH_02_NORMALISED, OS::Path.normalise( TEST_PATH_02 ),
					"TEST_PATH_02 failed normalise test." )
			assert_equal( TEST_PATH_03_NORMALISED, OS::Path.normalise( TEST_PATH_03 ),
					"TEST_PATH_03 failed normalise test." )
			assert_equal( TEST_PATH_04_NORMALISED, OS::Path.normalise( TEST_PATH_04 ),
					"TEST_PATH_04 failed normalise test." )
		end
		
		def test_platform_native
			
			assert_equal( TEST_PATH_09, OS::Path::platform_native( TEST_PATH_09 ) )
			assert_not_equal( TEST_PATH_01, OS::Path::platform_native( TEST_PATH_01 ) )
		end
		
		def test_normalise_unc
		
			assert_equal( TEST_PATH_07_NORMALISED, OS::Path.normalise( TEST_PATH_07 ),
					"TEST_PATH_07 failed normalise test." )
			assert_equal( TEST_PATH_08_NORMALISED, OS::Path.normalise( TEST_PATH_08 ),
					"TEST_PATH_08 failed normalise test." )
		end
		
		def test_strip_drive
		
			assert_equal( TEST_PATH_01_STRIPDRIVE, OS::Path.strip_drive( TEST_PATH_01 ),
					"TEST_PATH_01 failed stripdrive test." )
			assert_equal( TEST_PATH_02_STRIPDRIVE, OS::Path.strip_drive( TEST_PATH_02 ),
					"TEST_PATH_02 failed stripdrive test." )
		end
		
		def test_combine
		
			assert_equal( TEST_PATH_03_06_COMBINE, OS::Path.combine( TEST_PATH_03, TEST_PATH_06 ),
					"TEST_PATH_03 and TEST_PATH_06 failed combine test." )
				
			assert_equal( TEST_PATH_04_06_COMBINE, OS::Path.combine( TEST_PATH_04, TEST_PATH_06 ),
					"TEST_PATH_04 and TEST_PATH_06 failed combine test." )
			assert_equal( TEST_PATH_05_06_COMBINE, OS::Path.combine( TEST_PATH_05, TEST_PATH_06 ),
					"TEST_PATH_05 and TEST_PATH_06 failed combine test." )
		end
		
		def test_ends_with
		
			assert( OS::Path.ends_with( TEST_PATH_01, ".txt" ), 
					"TEST_PATH_01 failed ends_with test." )
			assert( !OS::Path.ends_with( TEST_PATH_01, ".tx" ),
					"TEST_PATH_01 failed ends_with test." )
					
			assert( OS::Path.ends_with( TEST_PATH_02, "test.txt" ), 
					"TEST_PATH_02 failed ends_with test." )
			assert( !OS::Path.ends_with( TEST_PATH_02, "X:/" ),
					"TEST_PATH_02 failed ends_with test." )

			assert( !OS::Path.ends_with( TEST_PATH_03, "\\" ),
					"TEST_PATH_03 failed ends_with test." )
			assert( !OS::Path.ends_with( TEST_PATH_03, "/" ),
					"TEST_PATH_03 failed ends_with test." )
			assert( !OS::Path.ends_with( TEST_PATH_04, "\\" ),
					"TEST_PATH_04 failed ends_with test." )
			assert( OS::Path.ends_with( TEST_PATH_04, "/" ),
					"TEST_PATH_04 failed ends_with test." )
			assert( OS::Path.ends_with( TEST_PATH_05, "/" ),
					"TEST_PATH_05 failed ends_with test." )
			assert( !OS::Path.ends_with( TEST_PATH_05, "\\" ),
					"TEST_PATH_05 failed ends_with test." )
		end
		
		def test_windows_dirs
		
			tempdir = OS::Path.get_temp_directory()
			windir = OS::Path.get_windows_directory()
			sysdir = OS::Path.get_windows_system_directory()
			assert( tempdir.is_a?( String ) )
			assert( windir.is_a?( String ) )
			assert( sysdir.is_a?( String ) )
			
			puts "Temp Path:    #{OS::Path.get_temp_directory()}"
			puts "Windows Path: #{OS::Path.get_windows_directory()}"
			puts "System Path:  #{OS::Path.get_windows_system_directory()}"
		end		
	end
	
end # Test module
end # Pipeline module

# End of log_test.rb
