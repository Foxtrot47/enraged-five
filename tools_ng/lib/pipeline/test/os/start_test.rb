#
# start_test.rb
# 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 10 March 2008
#

require 'pipeline/os/start'
require 'test/unit'

module Pipeline
module Test
		
	#
    # == Description
    # Unit test class for the Pipeline::start function.
    #
    class StartTest < ::Test::Unit::TestCase
    
        #
        # Test a command that exists, pipe some input to stdin, read
        # stdout and stderr, asserting exit status.
        #
        def test_start1
		
			status, stdout, stderr = Pipeline::OS.start( "cmd" )
            puts "status     : #{ status.inspect }"
            puts "exitstatus : #{ status.exitstatus }"
			puts "stdout     : #{ stdout.strip }"
			puts "stderr     : #{ stderr.strip }"
            
            assert( 0 == status.exitstatus, "Exit status incorrect." )
        end
        
        #
        # Test a command that does not exist, read stdout, stderr and
        # asserting on exit status.
        #
        def test_start2

            status, stdout, stderr = Pipeline::OS.start( "cmd22" )
            puts "status     : #{ status.inspect }"
            puts "exitstatus : #{ status.exitstatus }"
			puts "stdout     : #{ stdout.strip }"
			puts "stderr     : #{ stderr.strip }"
    
            assert( 0 != status.exitstatus, "Exit status incorrect." )
        end
		
		#
		# Test block version.
		#
		def test_start3
		
			status, stdout, stderr = Pipeline::OS.start( "cmd" ) do |pid|
				puts "PID: #{pid}"
			end
			puts "status     : #{ status.inspect }"
		end
    end	
		
end # Test module
end # Pipeline module

# End of start_test.rb
