#
# getopt_test.rb
# Getopt unit tests
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 10 March 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/os/getopt'
require 'test/unit'

#-----------------------------------------------------------------------------
# Code
#-----------------------------------------------------------------------------

module Pipeline
module Test
		
	#
	# == Description
	#
	# Command line argument processor test cases.  These tests screw with ARGV,
	# although it attempts to restore in the teardown function.
	#
	class GetoptTests < ::Test::Unit::TestCase
   	
		def setup
		
			# Backup ARGV before we screw with it...
			@argv_copy = Array.new
			@argv_copy.replace( ARGV )
		end
		
		def teardown
		
			# Restore ARGV after screwing with it...
			ARGV.replace( @argv_copy )
		end
	
		def test_getopts
	
			ARGV.clear()
			ARGV << "--version" << "-h" << "test_file.txt"
			assert_equal( ARGV.size, 3, "ARGV set failed." )
			
			opts, targs = OS::Getopt.getopts( 
				[ [ '--version', '-v', ::Getopt::BOOLEAN ],
				[ '--help', '-h', ::Getopt::BOOLEAN ],
				[ '--recurse', '-r', ::Getopt::BOOLEAN ],
				[ '--dimensions', '-d', ::Getopt::BOOLEAN ],
				[ '--compression', '-c', ::Getopt::BOOLEAN ],
				[ '--filter', '-f', ::Getopt::REQUIRED ] ] )
			
			assert_equal( opts['h'], true, "Short option failed." )
			assert_equal( opts['help'], true, "Short option failed." )
			assert_equal( opts['v'], true, "Long option failed." )
			assert_equal( opts['version'], true, "Long option failed." )
			assert_equal( opts['verbose'], nil, "Unspecified option failed." )
			assert_equal( targs.size, 1, "Trailing arguments size failed." )
			assert_equal( targs[0], "test_file.txt", "Trailing arguments value failed." )
		end
		
		def test_usage

			ARGV.clear()
			ARGV << "--version" << "-h" << "test_file.txt"
			assert_equal( ARGV.size, 3, "ARGV set failed." )
			
			message = OS::Getopt.usage( 
				[ [ '--version', '-v', ::Getopt::BOOLEAN, 'Display version information.' ],
				[ '--help', '-h', ::Getopt::BOOLEAN, 'Display usage information with a really really long description string yeah woohooo!' ],
				[ '--recurse', '-r', ::Getopt::BOOLEAN, 'Recurse into sub-directories.' ],
				[ '--dimensions', '-d', ::Getopt::BOOLEAN, 'Display image dimensions.' ],
				[ '--compression', '-c', ::Getopt::BOOLEAN, 'Display image compression algorithm.' ],
				[ '--filter', '-f', ::Getopt::REQUIRED, 'File filter (e.g. *.dds).' ] ],
				{ 
					'files' => 'List of files or paths to search for files (based on filter option).'
				} )
			puts "\n#{message}"
			
			assert( message.size > 0, 'Usage message is empty' )		
        end
	end
		
end # Test module
end # Pipeline module

# End of getopt_test.rb
