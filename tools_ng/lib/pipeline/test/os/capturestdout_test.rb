#
# capturestdout_test.rb
# Unit tests for capture stdout function
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 10 March 2008
#

require 'pipeline/os/capturestdout'
require 'test/unit'

module Pipeline
module Test

    class CaptureStdOutTest < ::Test::Unit::TestCase
    
        def test_helloworld
    
            out1 = Pipeline::OS.capture_stdout do
                puts STRING_TEST1
            end
            
            puts "Captured output: \"#{out1.strip}\""
            out2 = puts STRING_TEST1
            
            assert( 0 == ( STRING_TEST1 <=> out1.strip ), "STDOUT not captured." )
            assert( nil == out2, "STDOUT not restored." )
        end
        
        #---------------------------------------------------------------------
        # Private Constants
        #---------------------------------------------------------------------
    private    
        STRING_TEST1    = "Hello, World!"
    
    end
	
end # Test module
end # Pipeline module

# End of log_test.rb
