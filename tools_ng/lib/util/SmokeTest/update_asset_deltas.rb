# File:: update_asset_deltas.rb
# Description:: updates asset deltas spreadsheet. 
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 28 June 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'win32ole'
require 'pipeline/os/path'
require 'time'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'fileutils'
include FileUtils
require 'dl'
include Pipeline
require 'pipeline/log/log'
require 'util/ExcelTools/excel_tools'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS =	[
			[ '--xlsfilename', 	'-x', Getopt::REQUIRED, 'destination xls filename' ],
			[ '--datafilename', 	'-d', Getopt::REQUIRED, 'source filename from which data is extracted for insertion' ],
			[ '--disablecheckin', 	'-c', Getopt::BOOLEAN,	'prevent checkin ( for development )' ],			
			[ '--row', 		'-r', Getopt::REQUIRED,	'the row of the dest xls file to update' ],			
		]

INFO			= "[colourise=black]INFO_MSG: "
INFO_BLUE		= "[colourise=blue]INFO_MSG: "


#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

	#-----------------------------------------------------------------------------
	# Code
	#-----------------------------------------------------------------------------
	begin
		#-------------------------------------------------------------------------
		# Entry-Point
		#-------------------------------------------------------------------------

		g_AppName = File::basename( __FILE__, '.rb' )
		g_xls_filename = ''
		g_Config = Pipeline::Config.instance()
		g_Log = Log.new( 'exceltools' )

		#-------------------------------------------------------------------------
		# Parse & validate Command Line
		#-------------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			response = message_box( "#{g_AppName} will exit.",g_AppName, BUTTONS_OK, ICON_QUESTION)
			exit( 1 )
		end

		if not opts['row']
			$stderr.puts "Error : Specify a row in the dest xls to update."
			exit( 2 )
		end

		g_xls_filename 		= ( nil == opts['xlsfilename'] ) 	? nil  	: opts['xlsfilename']
		g_enable_checkin	= ( nil == opts['disablecheckin'] ) 	? true 	: false
		g_row			= opts['row'].to_i
		
		#-------------------------------------------------------------------------
		# --- Sync, check out file ---
		#-------------------------------------------------------------------------

		puts "#{INFO_BLUE} Creating p4"
		p4 	= SCM::Perforce::create( g_Config.sc_server, g_Config.sc_username, g_Config.sc_workspace )
		
		puts "#{INFO_BLUE} Connecting to p4 servers"
		raise Exception if not p4
		p4.connect( )
		raise Exception if not p4.connected?
	
		puts "#{INFO_BLUE} Creating CL"
		change_id = p4.create_changelist( "Automated Build of 'asset deltas' via Cruise Control @ #{Time.now} on #{ENV["COMPUTERNAME"]}." )
		raise Exception if change_id.nil?
		puts "#{INFO_BLUE}Changelist #{change_id} is created"

		puts "#{INFO_BLUE} Syncing to XLS file."
		depot_filename = p4.local2depot(g_xls_filename)
		p4.run_sync( depot_filename )
				
		depot_filename = p4.local2depot(g_xls_filename)
		p4.run_sync( depot_filename )
			
		3.times {
			fstat = p4.run_fstat( depot_filename ).shift		
			otherOpen = fstat["otherOpen"]
			if (otherOpen != nil)
				otherOpen.each do |oo|
					if (oo.downcase.include?('builder'))
						puts "#{INFO_BLUE} Sleeping builder has the shared XLS file checked out!"
						Kernel.sleep 60 # wait for lock to be freed
					end
				end
			else
				break
			end
		}							
		
		puts "#{INFO_BLUE}Checking out #{depot_filename} in CL #{change_id.to_s}"
		puts "Checking out #{depot_filename} in CL #{change_id.to_s}"
		p4.run_edit( '-c', change_id.to_s, depot_filename )	
		p4.run_reopen( '-c', change_id.to_s, depot_filename )		
		
		
		#-------------------------------------------------------------------------
		# --- Append the row ---
		#-------------------------------------------------------------------------

		if (trailing)

			#-------------------------------------------------------------------------
			# --- Search for data in log.
			#-------------------------------------------------------------------------
			trailing.each do |data_filename|
			
				idx = 0			
				vals = [ 0,0,0,0,0,0 ]

				puts "#{INFO_BLUE} opening #{data_filename}"
				File.open(data_filename) do |file| 
					file.each_line do |line|
						if line =~ /^\s*(-?\d*)\s*(-?\d*)\s*\*\*\*\sTOTAL\s\*\*\*\s*$/i						
							vals[idx] = $1 							
							idx = idx + 1
							vals[idx] = $2
							idx = idx + 1
							puts "#{INFO_BLUE} Matched #{line}, retrieved values #{$1} and #{$2} for row #{g_row}"
						end					
					end
				end

				puts "#{INFO_BLUE} vals"

				vals.each_with_index do |val,index|
					col = index+2
					puts "#{INFO_BLUE} setting #{g_xls_filename} (#{col},#{g_row}) = #{val}"
					ExcelTools::SetCell(g_xls_filename, g_row, index+2, val)	
				end			
				
				g_row = g_row + 1
			end
		else
			$stderr.puts "No data filenames passed as trailing args"
			exit( 4 )
		end		
		

		#-------------------------------------------------------------------------
		# --- Submit changes
		#-------------------------------------------------------------------------

		puts "#{INFO_BLUE}Reverting unchanged files #{change_id}"
		p4.run_revert( '-a', '-c', change_id.to_s, '//...')

		files = p4.run_opened( '-c', change_id.to_s )
		raise Exception if files.nil?

		puts "#{INFO_BLUE}There are #{files.size} files to submit."
		files.each do |file|
			puts "#{INFO_BLUE}#{file['depotFile']} has been updated and will be submitted."			
		end

		if ( g_enable_checkin )
			if ( files.size > 0 )		
				puts "#{INFO_BLUE}Submitting file currently in #{change_id}"	
				submit_result = p4.run_submit( '-c', change_id.to_s )
				puts submit_result.to_s
			elsif ( 0 == files.size )
				puts "#{INFO_BLUE}Deleting #{change_id} no files changed."
				p4.run_change('-d', change_id.to_s)
			end
		else
			puts "#{INFO_BLUE}Checkin is disabled the CL is pending."	
		end

	rescue Exception => ex

		puts "#{g_AppName} unhandled exception: #{ex.message}"
		puts "Call stack:"
		puts ex.backtrace.join( "\n\t" )
	end		

# update_asset_deltas.rb