#
# File:: check_files.rb
# Description:: checek if a number of files exist and issues a big fat error if any don't.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 19th June 2012
#
# Passed in  :-  see OPTIONS ...
# Passed out :-  stderr contains all errors 
#                stdout for all other output. 
# Returns    :-  returns non zero upon detecting any non existent files

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/file'
include Pipeline
require 'systemu'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ "--help", 		"-h", 	OS::Getopt::BOOLEAN, 	"display usage information." ],
]

TRAILING_DESC = 'filenames to check separated by spaces.'

INFO			= "[colourise=black]INFO_MSG: "
INFO_BLUE		= "[colourise=blue]INFO_MSG: "
	
#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------

begin
	g_AppName = File::basename( __FILE__, '.rb' )
	g_ProjectName = ''
	g_BranchName = ''
	g_Project = nil
	g_Config = Pipeline::Config.instance()

	#---------------------------------------------------------------------
	# --- PARSER COMMAND LINE                                          --- 
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] )
		puts OS::Getopt.usage( OPTIONS )
		puts ("Press Enter to continue...")
		$stdin.getc( )				
		Process.exit!( 1 )
	end	

	if ( 0 == trailing.size ) then
		puts 'No trailing file arguments specified.  Exiting.'
		puts OS::Getopt.usage( OPTIONS, { 'files' => TRAILING_DESC } )
		Process.exit!( 2 )
	end

	filenames 		= trailing

	puts "#{INFO_BLUE} checking files exist #{filenames}"
	errors = 0
	filenames.each do |filename|
		if (File.exist?(filename))
			puts "#{INFO_BLUE} #{filename} exists."
		else
			puts "#{INFO_BLUE} #{filename} DOES NOT EXIST "
			$stderr.puts "#{filename} DOES NOT EXIST"
			errors += 1
		end		
	end
	
	if errors > 0 
		puts "#{INFO_BLUE} The ruby process is returning -1 : errors #{errors}"
		Process.exit! -1 
	end

	puts "#{INFO_BLUE} all filenames exist this script will return 0"
	Process.exit! 0

rescue Exception => ex
	$stderr.puts "Error: Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	Process.exit! -1
end