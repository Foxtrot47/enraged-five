#
# File:: clip_convert.rb
# Description:: Converts all clip files by touching them with clip_edit tool.
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 16 September 2010
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/gui/exception_dialog'
require 'pipeline/win32/kernel32'
require 'pipeline/os/path'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/config/globals'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
    [ '--project', '-p', Getopt::REQUIRED, 'project (e.g. gta5).' ],
    [ '--help', '-h', Getopt::BOOLEAN, 'display usage information.' ]
]

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_ProjectName = ''
		g_Log = Log.new( g_AppName )
		
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		
		g_ProjectName = ( nil == opts['project'] ) ? '' : opts['project']
		g_Project = Pipeline::Config.instance( ) .projects[ g_ProjectName ]
		g_Project.load_config( )
		
		anim_export_folder = OS::Path::combine( g_Project.branches['dev'].art, "anim", "export_mb" )
		clip_list = OS::FindEx.find_files_recurse( anim_export_folder + "/*.clip" )
		clip_edit_tool = OS::Path::combine( Globals::instance().toolsbin, "anim", "clipedit.exe")
		#puts clip_list
		 
		
		clip_list.each_with_index do | filename, index |
				puts ("Processing: " + filename)
				#puts (clip_edit_tool + "-clip " + filename + " -out " + filename)
				Pipeline::OS.start( clip_edit_tool + " -clip \"" + filename + "\" -out \"" + filename + "\"" )
		end
		
		
	rescue Exception => ex
		g_Log.exception( ex, "Unhandle exception during convert" )
		ex.backtrace.each do |m| g_Log.error( m ); end
		GUI::ExceptionDialog::show_dialog( ex, "#{g_AppName} unhandled exception" )

		# Only require Enter press when an exception has occurred.
		puts "\nPress Enter or close this window to continue..."
		$stdin.gets( )    
	end
end

