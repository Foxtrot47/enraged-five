require 'pipeline/os/file'
require 'pipeline/os/path'
include Pipeline

# cutscene_fbx_counter.rb
# Author: Mike Wilson <mike.wilson@rockstartoronto.com>
#
# List all fbx files under a dir, excluding any in 'stem' directories. print the count.
#

source_files = OS::FindEx::find_files_recurse( OS::Path::combine( 'X:/gta5/art/animation/cutscene', '*.fbx' ) )
count = 0
source_files.each do |source_file|
	directory = OS::Path::get_directory( source_file )
	if directory.index('_stems') == nil
		count += 1
		#puts directory
		puts OS::Path::get_filename( source_file )	
	end
end

puts "\r\n#{count} fbx files within 'X:/gta5/art/animation/cutscene'"