require 'pipeline/os/getopt'
require 'pipeline/config/projects'
include Pipeline

def run( checkout_files, rebuild_zips )

	c = Pipeline::Config::instance()
	p = c.projects[ENV['RS_PROJECT']]
	p.load_config()
	
	# Sync our source data and work out what will actually need to be checked out
	p4 = c.scm()
	p4.connect() unless p4.connected?

	src_dir = "#{p.art}/anim/export_mb/"
	dst_dir = "#{p.export}/anim/ingame/"

	
	puts "Syncing: #{dst_dir}"
	p4.run_sync( OS::Path.combine( dst_dir, '...' ) )
	puts "Sync complete"
	puts "Syncing: #{src_dir}"
	p4.run_sync( OS::Path.combine( src_dir, '...' ) )
	puts "Sync complete"

	
	if checkout_files then 
		puts "Checking out dictionaries..."
		zip_files =  OS::FindEx.find_files_recurse( dst_dir + "/*.icd.zip" )
		if zip_files.count > 0 then
			changelistname = 'In-game Animation Dictionaries'
			cl = p4.create_changelist( changelistname )
			zip_files.each do | clip_dictionary |
				p4.run_edit_or_add( '-c',cl.to_s, clip_dictionary  ) 
				clip_dictionary.gsub!( '@', '%40' )
				p4.run_reopen( '-t', '+l', clip_dictionary )
			end
			
		end
		puts "Check out complete."
	end

	
	puts "Building zips..." unless rebuild_zips
	puts "Rebuilding zips..." if rebuild_zips
	commandline = "$(toolsbin)/ruby/bin/ruby.exe $(toolslib)/pipeline/resourcing/gateways/all_zips_from_dir_leaves.rb --src=#{src_dir} --dst=#{dst_dir} --ext=icd.zip"
	commandline += " --rebuild" if rebuild_zips
	Pipeline::Globals.instance().in_env do |e|
		commandline = e.subst(commandline)
	end
	status, out, err = OS::start( commandline )
	
	if status.exitstatus == 0 then
		puts "Build successful."
	else
		puts "Build failed."
		puts "#{out}"
	end
	
	if checkout_files then 
		puts "Reverting unchanged dictionaries..."
		p4.run_revert("-a",OS::Path.combine( dst_dir, '...' ) )
	end
end

if ( __FILE__ == $0 ) then
	#-----------------------------------------------------------------------------
	# Constants
	#-----------------------------------------------------------------------------
	OPTIONS = [
		[ '--checkout', '-c', OS::Getopt::BOOLEAN, 'checkout files form perforce' ],
		[ '--rebuildzips', '-r', OS::Getopt::BOOLEAN, 'Rebuild all zips.' ],
	]

	
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	begin
		opts, trailing = OS::Getopt.getopts( OPTIONS )
	
		g_CheckoutFiles = ( nil == opts['checkout'] ) ? false : opts['checkout']
		g_RebuildZips = ( nil == opts['rebuildzips'] ) ? false : opts['rebuildzips']
		run( g_CheckoutFiles, g_RebuildZips )

	rescue Exception => ex
		puts(ex)
		puts "Error: Unhandled exception {ex}."
		puts "Press Enter or close this window to continue..."
		STDIN.gets
	end
end
