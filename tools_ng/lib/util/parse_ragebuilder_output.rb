#
# File:: parse_ragebuilder_output.rb
# Description:: 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 10 June 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/log/log'
require 'pipeline/os/getopt'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', OS::Getopt::REQUIRED, 'display usage information.' ]
]
TRAILING_DESC = { 'files' => 'Ragebuilder log files to parse' }
REGEXP_CONTEXT = /^Input file: (.*)$/i
REGEXP_ERROR = /^(.*): Error : (.*)$/i

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

# Parse file for Ragebuilder errors (including context in output)
def parse( filename )
	text = []
	context = ''
	File::open( filename, 'r' ).each do |line|
		if ( line =~ REGEXP_CONTEXT ) then
			context = $1
		elsif ( line =~ REGEXP_ERROR ) then
			puts "Error converting #{context}: #{$2} (#{$1})"
		end
	end
end

#----------------------------------------------------------------------------
# Entry-Point
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

	g_AppName = File::basename( __FILE__, '.rb' )
	g_Log = Log.new( g_AppName )
	g_Config = Pipeline::Config::instance( )
	g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
	
	g_Filenames = []
	# Here we ensure we have absolute filenames, relative filenames being
	# expanded based on the current working path.
	g_Trailing.each_with_index do |filename, index|
		filename = File::expand_path( filename )
		if ( File::file?( filename ) ) then
			g_Filenames << OS::Path::normalise( filename )
		elsif ( File::directory?( filename ) ) then
			# Here we find all files in the directory and recurse if asked.
			if ( g_Recursive ) then
				g_Filenames += OS::FindEx::find_files_recurse( OS::Path::combine( filename, '*.*' ) )
			else
				g_Filenames += OS::FindEx::find_files( OS::Path::combine( filename, '*.*' ) )
			end	
		end
		
		throw IOError.new( "File #{filename} does not exist.  Aborting convert." ) \
			unless ( File::exists?( filename ) )
	end

	# Iterate through all our files doing our parse.
	g_Filenames.each do |filename|
		parse( filename )
	end
end

# parse_ragebuilder_output.rb
