#
# File:: %RS_TOOLSLIB%/util/data_convert_file_preview.rb
# Description:: Attempt to intelligently convert a file, given only the local
#               independent path.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 20 December 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
Pipeline::Config::instance()::logtostdout = true
require 'pipeline/config/project'
require 'pipeline/gui/exception_dialog'
require 'pipeline/log/log'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_convert'
require 'pipeline/resourcing/convert'
require 'pipeline/resourcing/path'
require 'pipeline/resourcing/util'
require 'pipeline/util/environment'
require 'pipeline/util/rage'
require 'pipeline/util/string'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--branch', '-b', OS::Getopt::REQUIRED, 'project branch.' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING = {
	'files' => 'files to convert and put into preview directory.'
}

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
begin
	g_AppName = OS::Path::get_filename( __FILE__ )
	g_Config = Pipeline::Config::instance( )
	g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
	g_Log = Log.new( g_AppName )
	
	g_Project = g_Config.project
	g_Project.load_config()
	g_BranchName = g_Opts['branch'].nil? ? g_Project.default_branch : g_Opts['branch']
	g_Branch = g_Project.branches[g_BranchName]
	
	
	if ( g_Opts['help'] ) then
		puts OS::Getopt::usage( OPTIONS, TRAILING )
		exit( 1 )
	end

	g_Filenames = []
	# Here we ensure we have absolute filenames, relative filenames being
	# expanded based on the current working path.
	g_Trailing.each_with_index do |filename, index|
		filename = File::expand_path( filename )
		next unless ( File::file?( filename ) )
		
		g_Filenames << OS::Path::normalise( filename )
	end
	
	# Log all files to be converted.
	g_Filenames.each do |filename|
		g_Log.info( "File #{filename} will be converted." )
	end

	g_Content = Content::Group::new( "input" )
	g_Preview = Content::Group::new( "preview" )
	g_Filenames.each do |filename|
		node = Content::Zip::from_filename_and_target( filename, g_Project.ind_target )
		g_Content.add_child( node )
	end
	Resourcing::create_target_content_from_indepenent( g_Content, g_Preview, g_Project, g_Branch.preview )
	ConvertSystem::instance()::setup( g_Project, g_BranchName, false, true, false )
	result = false
	if ( g_Preview.children.size() > 0 ) then
		result = ConvertSystem::instance()::build( g_Preview )
	end
	
	g_Log.info( "Final result: #{result}." )
	exit( result ? 0 : 1 )

rescue Pipeline::ProjectUtil::ConvertException => ex
    g_Log.exception( ex, 'Conversion exception during convert' )
	ex.backtrace.each do |m| g_Log.error( m ); end
    filenames = ex.filenames.join("\n")
    ex.filenames.each do |filename| 
        g_Log.error( "File: '#{filename}'." )
    end
    GUI::ExceptionDialog::show_dialog( ex, "Conversion exception during convert with files:\n#{filenames}." )
	exit( 1 )

rescue SystemExit => ex
	exit( ex.status )

rescue Exception => ex
	g_Log.exception( ex, "Unhandle exception during convert" )
	ex.backtrace.each do |m| g_Log.error( m ); end
	GUI::ExceptionDialog::show_dialog( ex, "#{g_AppName} unhandled exception" )

	# Only require Enter press when an exception has occurred.
	puts "\nPress Enter or close this window to continue..."
	$stdin.gets( )
	exit( 1 )
end

# %RS_TOOLSLIB%/util/data_convert_file_preview.rb
