#
# File:: %RS_TOOLSLIB%/util/data_extract_zip.rb
# Description:: Extract PKWare Zip file contents to disk.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 23 June 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_zip'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--project', '-p', Getopt::REQUIRED, 'OBSOLETE.' ],
    [ '--branch', '-b', Getopt::REQUIRED, 'OBSOLETE' ],
    [ '--output', '-o', OS::Getopt::REQUIRED, 'output directory (root for Zip extraction).' ],
    [ '--overwrite', '-f', OS::Getopt::BOOLEAN, 'overwrite files if required.' ],
	[ '--showlog', '-l', OS::Getopt::BOOLEAN, 'display log window.' ],
    [ '--debug', '-d', OS::Getopt::BOOLEAN, 'debug mode toggle.' ],
    [ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING_DESC = { 
    'files' => 'ZIP files to extract.'
}

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_ProjectName = ''
		g_Project = nil
		g_Branch = ''
		g_Config = Pipeline::Config.instance( ) 
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
			exit( 1 )
		end
		g_Debug = opts['debug'].nil? ? false : true
		Pipeline::Config::instance()::log_trace = g_Debug

		# Force log output
		Pipeline::Config::instance()::log_level = 2
		Pipeline::Config::instance()::log_level = 1 if ( g_Debug )
		g_Log = Log.new( g_AppName )

		g_ProjectName = ( nil == opts['project'] ) ? ENV['RS_PROJECT'] : opts['project']
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) then
			puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
			exit( 3 )
		end 
		g_Project.load_config( )
		g_BranchName = ( nil == opts['branch'] ) ? g_Project.default_branch : opts['branch']
		if ( not g_Project.branches.has_key?( g_BranchName ) ) then
			puts "\nError project: #{g_ProjectName} does not have a branch called #{g_BranchName}."
			exit( 4 )
		end
		g_OutputDir = ( nil == opts['output'] ) ? nil : File::expand_path( opts['output'] )
		if ( g_OutputDir.nil? ) then
			puts "\nError no output directory specified."
			puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
			exit( 5 )
		end
		g_Overwrite = ( nil == opts['overwrite'] ) ? false : true
		g_ShowLog = opts['showlog'].nil? ? false : true
		
		#---------------------------------------------------------------------
		# Process RPF Filenames
		#---------------------------------------------------------------------
		FileUtils::mkdir_p( g_OutputDir ) unless ( File::directory?( g_OutputDir ) )
		
		trailing.each do |rpf_filename|
			
			rpf_filename = File::expand_path( rpf_filename )
			g_Log.info( "Extracting ZIP: #{rpf_filename}" )
			ProjectUtil::data_zip_extract( rpf_filename, g_OutputDir, g_Overwrite ) do |filename|
				g_Log.info( "  #{filename}" )
			end
		end
		
	rescue SystemExit => ex
		exit( ex.status )
	
	rescue Exception => ex
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")      
	end 	
end

# %RS_TOOLSLIB%/util/data_extract_rpf.rb
