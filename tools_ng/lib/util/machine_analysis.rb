#
# File:: MachineAnalysis.rb
# Description:: Checks the state of a machine. 
#		Including harware, software and it's configuration.
#		erroring if s/w is not up to date and reporting versions fo installed s/w.
#		Primarily for use to support multiple Cruise control machines.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 14th July 2010
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
include Pipeline
require 'rbconfig'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ '--help', 		'-h', 	OS::Getopt::BOOLEAN, 	'show usage information' ],
	[ '--cruisecontrol', 	'-c', 	OS::Getopt::BOOLEAN, 	'make output pretty for cruise control' ],
	[ '--project', 		'-p', 	OS::Getopt::REQUIRED, 	'specify project key (e.g. gta5, jimmy)' ],
	[ '--branch', 		'-b', 	OS::Getopt::REQUIRED, 	'specify project branch (e.g. dev, dev_migrate)' ]

]

INFO		= "[colourise=black]INFO_MSG: "
INFO_BLUE	= "[colourise=blue]INFO_MSG: "
INDENT		= "  "
PAD		= 22

#----------------------------------------------------------------------------------------------
def check( name, val, acceptable_values = [], unacceptable_values = [] )
	ok = true
	cc_str = $cc ? "#{INFO_BLUE}" : "" 
	output = "#{INDENT}#{cc_str}#{name.ljust(PAD," ")}#{val}"

	if ( (acceptable_values.length > 0) and (not acceptable_values.include?(val) ) )
		ok = false 
		$stderr.puts "#{output} : Error : Not an acceptable value."
	end

	if ( unacceptable_values.include?(val) )
		ok = false 
		$stderr.puts "#{output} : Error : Unacceptable value."
	end

	puts output 

	$num_errors += 1 if not ok

	ok
end

#----------------------------------------------------------------------------------------------
def header( name )
	cc_str = $cc ? "#{INFO_BLUE}" : "" 
	puts "#{cc_str}#{name}"
end

#----------------------------------------------------------------------------------------------
def check_ruby_version()
	header( "Ruby" )
	check( "RUBY_VERSION", 		RUBY_VERSION )
	check( "RUBYLIB", 		ENV['RUBYLIB'] )
end

#----------------------------------------------------------------------------------------------
def check_dot_net()
	dot_net_version_x86, dot_net_version_x64 = "not installed", "not installed"
	dot_net_versions = [ "1.0.3705", "1.1.4322", "2.0.50727", "3.0", "3.5", "4.0.30319" ]

	dot_net_versions.each { | version | dot_net_version_x86 = version if File.exist? "#{ENV['WINDIR']}/Microsoft.Net/Framework/v#{version}/mscorlib.dll" }		
	dot_net_versions.each { | version | dot_net_version_x64 = version if File.exist? "#{ENV['WINDIR']}/Microsoft.Net/Framework64/v#{version}/mscorlib.dll" }

	check( ".Net(x86)",			dot_net_version_x86 )
	check( ".Net(x64)",			dot_net_version_x64 )
end

#----------------------------------------------------------------------------------------------
def check_os()
	header( "OS" )
	check( "OS" ,  			ENV['OS'] )
	check( "RUBY_PLATFORM",  	RUBY_PLATFORM )
	check( "target_cpu",  		Config::CONFIG['target_cpu'] )
	check( "target_os",  		Config::CONFIG['target_os'] )
	check( "host_cpu",  		Config::CONFIG['host_cpu'] )
	check( "host_os",  		Config::CONFIG['host_os'] )

	check_dot_net()
end

#----------------------------------------------------------------------------------------------
def check_basic()
	header( "Basic")
	check( "COMPUTERNAME", 		ENV['COMPUTERNAME'] )
	check( "USERNAME", 		ENV['USERNAME'] )
	check( "RS_PROJECT", 		ENV['RS_PROJECT'] )
end

#----------------------------------------------------------------------------------------------
def check_sw()
	header( "Software")
	check( "XEDK", 			ENV['XEDK'] )
end

#----------------------------------------------------------------------------------------------
def check_hw()
	header( "Hardware")
	check( "NUMBER_OF_PROCESSORS", 	ENV['NUMBER_OF_PROCESSORS'])
end

#----------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------
begin

	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------

	g_AppName = File::basename( __FILE__, '.rb' )
	g_ProjectName = ''
	g_BranchName = ''
	g_Project = nil
	g_Config = Pipeline::Config.instance()
	
	$num_errors	= 0

	#-------------------------------------------------------------------------
	# Parse Command Line
	#-------------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] ) then
		puts OS::Getopt.usage( OPTIONS )
		Process.exit( 1 )
	end

	g_ProjectName = ( nil == opts['project'] ) ? '' : opts['project']
	project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
	if ( not project_exists ) then
		puts OS::Getopt.usage( OPTIONS )
		puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
		exit( 2 )
	end
	g_Project = g_Config.projects[ g_ProjectName ]
	g_Project.load_config( )
	if ( not g_Project.enabled ) then
		puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
		exit( 3 )
	end
	g_BranchName = ( nil == opts['branch'] ) ? g_Project.default_branch : opts['branch']
	if ( not g_Project.branches.has_key?( g_BranchName ) ) then
		puts "\nError project: #{g_ProjectName} does not have branch #{g_BranchName} defined."
		exit( 4 )
	end

	$cc = opts['cruisecontrol']

	#-------------------------------------------------------------------------
	# Analysis
	#-------------------------------------------------------------------------

	check_basic()
	check_hw()
	check_os()
	check_ruby_version()
	check_sw()
	
rescue Exception => ex

	puts "#{g_AppName} unhandled exception: #{ex.message}"
	puts "Call stack:"
	puts ex.backtrace.join( "\n\t" )
end

Process.exit $num_errors

# End of MachineAnalysis.rb
