#
# File:: ping.rb
# Description:: Ping an IP address.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 16th October 2009
#

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'ping'
require 'pipeline/os/getopt'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ "--help", "-h", Getopt::BOOLEAN, "display usage information." ],
	[ "--retries", "-r", Getopt::REQUIRED, "Number of retries." ],
	[ "--ip_address", "-ip", Getopt::REQUIRED, "IP Address." ],
]

NUM_PINGS	= 1
IP			= "127.0.0.1"

#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------
begin
	#---------------------------------------------------------------------
	# Parse Command Line.
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] )
		puts OS::Getopt.usage( OPTIONS )
		puts ("Press Enter to continue...")
		$stdin.getc( )				
		Process.exit!(1)
	end	
	
	num_pings = opts['retries'] ? opts['retries'].to_i	: NUM_PINGS
	ip_address = opts['ip_address']	? opts['ip_address'] : IP

	#---------------------------------------------------------------------
	# Perform pings.
	#---------------------------------------------------------------------
	puts "Pinging #{ip_address}"
#	if Ping.pingecho(ip_address, num_pings)
#		puts "ping succeeded"
#		Process.exit!(0)
#	end

	result = `ping -n #{num_pings} #{ip_address}`
	if ($?.exitstatus == 0) 
		puts "ping succeeded"
		Process.exit!(0)
	end



	$stderr.puts "ping #{ip_address} failed after #{num_pings} retries."

rescue Exception => ex
	$stderr.puts "Unhandled exception: #{ex.message}"
	$stderr.puts "Inspect #{ex.inspect}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	Process.exit!(-1)
end

Process.exit!(-1)
