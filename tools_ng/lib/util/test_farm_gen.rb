#
# File:: test_farm_gen.rb
# Description:: Generate commandlines for test farm
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 30th July 2012
#



#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/log/log'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'xml'
include Pipeline


#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ "--help", "-h", Getopt::BOOLEAN, "display usage information." ]
]

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 )

	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------

	begin
				
		g_AppName = File::basename( __FILE__, '.rb' )
		
		log = Log.new( __FILE__ )

		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end

		tests = []
		
		trailing.each do |config_filename|
		
			if File.exists?( config_filename )
				
				xmldoc = LibXML::XML::Document::file( config_filename )
				nodes = xmldoc.find("//testfarm/test")

				nodes.each do |node|
					tests << { 
							:name 		=> 	node['name'], 
							:desc 		=> 	node['desc'],
						   	:split_axis	=> 	node['split_axis'],
						   	:dest_dir	=> 	node['dest_dir'],
						   	:min_x		=> 	node['min_x'].to_i,
						   	:max_x		=> 	node['max_x'].to_i,
						   	:min_y		=> 	node['min_y'].to_i,
						   	:max_y		=> 	node['max_y'].to_i,
						   	:grid_dim	=> 	node['grid_dim'].to_i, 
						   	:farm_size	=>	node['farm_size'].to_i,
						   	:commandline 	=> 	node['commandline'],
						   	:run_script	=>	node['run_script']
						 }					
				end				
			end
		end
		
		tests.each_with_index do |t,idx|
			job = "Test \##{idx+1} FarmSize(#{t[:farm_size]}): #{t[:name]} : split in (#{t[:split_axis]}) axis (#{t[:min_x]},#{t[:min_y]}) to (#{t[:max_x]},#{t[:max_y]}) sample step #{t[:grid_dim]} => #{t[:dest_dir]}"
			log.info(job)
			
			# delete existing batch files of this name
			filelist = OS::FindEx.find_files( OS::Path.combine(t[:dest_dir],"#{t[:name]}*.bat"), false )
			filelist.each do |f|
				File.delete(f) 
			end
			
			# calculate how to split across machines...
			range_x = t[:max_x]-t[:min_x]
			range_y = t[:max_y]-t[:min_y]						
			sx = t[:min_x]
			sy = t[:min_y]
			
			if (t[:split_axis] == "x")
				lines = range_x.to_f / t[:grid_dim].to_f
				lines = lines.ceil
				num_lines_each = lines/t[:farm_size]
				num_lines_each = num_lines_each.floor								
				ex = sx + (num_lines_each * t[:grid_dim])				
				ey = t[:max_y]
			else
				lines = range_y.to_f / t[:grid_dim].to_f
				lines = lines.ceil
				num_lines_each = lines/t[:farm_size]
				num_lines_each = num_lines_each.floor								
				ex = t[:max_x]								
				ey = sy + (num_lines_each * t[:grid_dim])
			end
			log.info("\t\##{num_lines_each} sample lines each")
									
			# write batch script to run
			t[:farm_size].to_i.times do |i|				
				filename = "#{t[:dest_dir]}\\#{t[:name]}#{i+1}.bat"				
				
				commandline = "@#{t[:dest_dir]}\\#{t[:name]}#{i+1}_skip.rsp "
				commandline += t[:commandline]
				commandline += " -grid_start_x=#{sx}"
				commandline += " -grid_start_y=#{sy}"
				commandline += " -grid_end_x=#{ex}"
				commandline += " -grid_end_y=#{ey}"
				commandline += " -grid_size=#{t[:grid_dim]}"
				
				this_job = "#{filename}:(#{sx},#{sy})=>(#{ex},#{ey}):grid_size=#{t[:grid_dim]}"
				log.info("\t#{this_job}")				
				
				File.open(filename, 'w+') do |f| 
					f.write("@ECHO off\n")
					f.write("TITLE \"#{this_job}\" \n")
					f.write("ECHO.\n")
					f.write("ECHO **********************************************************************************************\n")
					f.write("ECHO \"#{this_job}\"\n")
					f.write("ECHO     - #{t[:desc]}\n")
					f.write("ECHO **********************************************************************************************\n\n")
					f.write("ECHO.\n")
					f.write("CALL setenv.bat\n")
					f.write("PUSHD %RS_BUILDBRANCH%\n")
					f.write("CALL #{t[:run_script]} #{commandline}\n")
					f.write("POPD\n")
					f.write("ECHO =======================================\n")
					f.write("ECHO COMPLETED\n")
					f.write("ECHO =======================================\n")
				end
								
				retry_script = filename.gsub(t[:name],"#{t[:name]}_retry")
				skip_filename = "#{t[:dest_dir]}\\#{t[:name]}#{i+1}_skip.rsp"
				
				File.open(retry_script, 'w+') do |f| 
					f.write("@ECHO off\n")
					f.write("TITLE \"RETRY #{this_job}\" \n")
					
					f.write("ECHO .\n")
					f.write("ECHO https://devstar.rockstargames.com/wiki/index.php/Test_Farm\n")
					f.write("ECHO .\n")

					f.write("ECHO .\n")
					f.write("ECHO **********************************************************************************************\n")
					f.write("ECHO \"RETRY #{this_job}\"\n")
					f.write("ECHO \"This is script that will retry with skip a capture from the game.\"\n")
					f.write("ECHO     - #{t[:desc]}\n")
					f.write("ECHO **********************************************************************************************\n\n")
					f.write("ECHO .\n")
					f.write("CALL setenv.bat\n")
					f.write("PUSHD %RS_BUILDBRANCH%\n")
					
					commandline =~ /(.*)-logfile=(\S*)\s*/i
					console_log_filename = $2
					f.write("CALL %RS_TOOLSBIN%\\testermonitor\\testermonitor.exe --log_timeout 500000 --launch_script #{filename} --skip_filename #{skip_filename} --console_log #{console_log_filename}\n")
					f.write("POPD\n")
					f.write("ECHO =======================================\n")
					f.write("ECHO COMPLETED\n")
					f.write("ECHO =======================================\n")
				end								
				
				if (t[:split_axis] == "x")				
					sx = ex
					ex = sx + (num_lines_each * t[:grid_dim])
					ex = t[:max_x] if ex > t[:max_x]
				else
					sy = ey
					ey = sy + (num_lines_each * t[:grid_dim])				
					ey = t[:max_y] if ey > t[:max_y]
				end
			end
		end							
		
	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
	
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")		
	ensure

		puts "Press Enter to continue..."
		$stdin.getc( )	
	end	
end

# test_farm_gen.rb
