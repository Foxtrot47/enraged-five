#
# File:: %RS_TOOLSLIB%/util/data_set_platform_enabled.rb
# Description:: Set whether platforms are enabled for the current projects, default branch.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 3 June 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--project', '-p', Getopt::REQUIRED, 'project.' ],
	[ '--branch', '-b', Getopt::REQUIRED, 'branch.' ],
	[ '--ps3', '-c', Getopt::OPTIONAL, 'enable PS3 platform.' ],
	[ '--xbox360', '-x', Getopt::OPTIONAL, 'enable Xbox360 platform.' ],
	[ '--win32', '-w', OS::Getopt::OPTIONAL, 'enable Win32 platform.' ]
]

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_ProjectName = ENV['RS_PROJECT']
		g_Project = nil
		g_Branch = ''
		g_Config = Pipeline::Config.instance( ) 
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 2 )
		end
		
		g_ProjectName = ( opts['project'].nil? ) ? ENV['RS_PROJECT'] : opts['project']
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			puts OS::Getopt.usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 3 )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) then
			puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
			exit( 4 )
		end		
		g_Project.load_config( )
		g_BranchName = ( nil == opts['branch'] ) ? g_Project.default_branch : opts['branch']
		if ( not g_Project.branches.has_key?( g_BranchName ) ) then
			puts "\nError project: #{g_ProjectName} does not have a branch called #{g_BranchName}."
			exit( 5 )
		end
		g_Branch = g_Project.branches[g_BranchName]
		
		if ( opts.has_key?( 'xbox360' ) and g_Branch.targets.has_key?( 'xbox360' )) then
			g_Branch.targets['xbox360'].enabled = opts['xbox360']
		end
		if ( opts.has_key?( 'ps3' ) and g_Branch.targets.has_key?( 'ps3' )) then
			g_Branch.targets['ps3'].enabled = opts['ps3']
		end
		if ( opts.has_key?( 'win32' ) and g_Branch.targets.has_key?( 'win32' ) ) then
			g_Branch.targets['win32'].enabled = opts['win32'] 
		end
		if ( opts.has_key?( 'win64' ) and g_Branch.targets.has_key?( 'win64' ) ) then
			g_Branch.targets['win64'].enabled = opts['win64'] 
		end
		
		g_Project.save_locals( )
		
	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")  
	end
end

# %RS_TOOLSLIB%/util/data_is_platform_enabled.rb
