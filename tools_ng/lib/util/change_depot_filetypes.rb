# File:: change_depot_filetypes.rb
# Description:: changes the filetype of depot files of +s10w to +s16w
# 
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 17th September 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/os/path'
require 'pipeline/os/getopt'
require 'pipeline/log/log'

include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS =	[
			[ '--checkin', 	'-dc', 	Getopt::BOOLEAN,	'checkin ( for development )' ],			
		]

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

		
if ( __FILE__ == $0 ) then

	error_count = 0

	#-----------------------------------------------------------------------------
	# Code
	#-----------------------------------------------------------------------------
	begin
		#-------------------------------------------------------------------------
		# Entry-Point
		#-------------------------------------------------------------------------

		g_AppName = File::basename( __FILE__, '.rb' )
		g_Config = Pipeline::Config.instance()
		g_Log = Log.new( g_AppName )

		#-------------------------------------------------------------------------
		# Parse & validate Command Line
		#-------------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			response = message_box( "#{g_AppName} will exit.",g_AppName, BUTTONS_OK, ICON_QUESTION)
			Process.exit!( 1 )
		end

		g_enable_checkin	= ( nil == opts['checkin'] ) 	? false : true

		p4 = SCM::Perforce::create( g_Config.sc_server, 	g_Config.sc_username, g_Config.sc_workspace )
		p4.connect( )
		raise Exception if not p4.connected?
		
		change_id = p4.create_changelist( "S10w ->S16w (#{trailing}) @ #{Time.now} on #{ENV["COMPUTERNAME"]}\n" )
		raise Exception if change_id.nil?
		
		trailing.each do |depot_path|
			puts "Sync"
			p4.run_sync( depot_path ) 
			
			puts "edit all (to get type)"
			p4.run_edit( '-c', change_id.to_s, depot_path )
			p4.run_reopen( '-c', change_id.to_s, depot_path )			
			files = p4.run_opened( '-c', change_id.to_s )
			
			puts "fstat #{depot_path}"
			files = p4.run_fstat('-F','type~=S10w','-F','headAction!=delete', depot_path)
			puts "Num files #{files.length}"
			files.each do |file|
				file_depot_path = file['depotFile']
				#puts " Edit #{file_depot_path}"
				p4.run_edit( '-c', change_id.to_s, file_depot_path )	
				p4.run_reopen( '-c', change_id.to_s, file_depot_path )
				p4.run_reopen( '-t', "+S16w", '-c', change_id.to_s, file_depot_path )					
			end
			
			puts "Revert unchanged"
			p4.run_revert( '-a', '-c', change_id.to_s, '//...')
			files = p4.run_opened( '-c', change_id.to_s )
			puts "#{files.length} Files in CL #{change_id}"
		end
		
		if (g_enable_checkin)
			puts ("Press a key to checkin.")
			STDIN.getc
			submit_result = p4.run_submit( '-c', change_id.to_s )
			puts "Submit result : #{submit_result.to_s}"
		end

	rescue Exception => ex

		puts "#{g_AppName} unhandled exception: #{ex.message}"
		puts "Call stack:"
		puts ex.backtrace.join( "\n\t" )
	end	

	Process.exit! error_count
end