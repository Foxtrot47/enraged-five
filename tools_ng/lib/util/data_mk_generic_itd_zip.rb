#
# File:: %RS_TOOLSLIB%/util/data_mk_generic_itd_zip.rb
# Description:: Script to create a generic RAGE RPF file from a set of
#               files or directory contents.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 12 July 2011
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_texture'
require 'pipeline/projectutil/data_zip'

include Pipeline
include Pipeline::ProjectUtil

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
  [ '--project', '-p', OS::Getopt::REQUIRED, 'project (e.g. %RS_PROJECT%, jimmy).' ],
  [ '--branch', '-b', OS::Getopt::REQUIRED, 'project branch (e.g. dev, dev_migrate).' ],
  [ '--output', '-o', OS::Getopt::REQUIRED, 'output ZIP filename.' ],
  [ '--uncompressed', '-u', OS::Getopt::BOOLEAN, 'create uncompressed RPF.' ],
  [ '--flatten', '-f', OS::Getopt::BOOLEAN, 'flatten directory structure.' ],
  [ '--recursive', '-r', OS::Getopt::BOOLEAN, 'recursive operation, expects directory.' ],
  [ '--filter', '-f', OS::Getopt::REQUIRED, 'file filter for path arguments.' ],
  [ '--silent', '-s', OS::Getopt::BOOLEAN, 'silent mode, reduces logging.' ],
  [ '--debug', '-d', OS::Getopt::BOOLEAN, 'debug mode toggle.' ],
  [ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ],
  [ '--metadata', '-m', OS::Getopt::REQUIRED, 'texture metadata location (tcs files).' ],
  [ '--template', '-t', OS::Getopt::REQUIRED, 'texture template for new tcs files' ],
  [ '--regentcs', '-z', OS::Getopt::BOOLEAN, 'force regeneration of texture templates' ]
]
TRAILING_DESC = {
  'files' => 'list of files/directories to include/search for files to include (can include wildcards, e.g. x:/assets/*.dds).'
}
DEFAULT_FILTER = '*.*'

CL_DESCRIPTION = 'Automatic tcs creation for itd'

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

  #-------------------------------------------------------------------------
  # Entry-Point
  #-------------------------------------------------------------------------

  begin
    # Force output to stdout for console scripts.
    Pipeline::Config::instance()::logtostdout = true

    g_app_name = File::basename( __FILE__, '.rb' )
    g_project_name = ''
    g_project = nil
    g_branch = ''
    g_config = Pipeline::Config.instance( )

    #---------------------------------------------------------------------
    # Parse Command Line
    #---------------------------------------------------------------------
    opts, trailing = OS::Getopt.getopts( OPTIONS )
    if ( opts['help'] ) then
      puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
      exit( 1 )
    end
    g_debug = opts['debug']
		g_silent = opts['silent']
		Pipeline::Config::instance()::log_level = 5 if ( g_silent )
		Pipeline::Config::instance()::log_level = 2 unless ( g_silent )
		Pipeline::Config::instance()::log_level = 1 if ( g_debug )
    Pipeline::Config::instance()::log_trace = g_debug
    g_log = Log.new( g_app_name )

    g_project_name = ( nil == opts['project'] ) ? ENV['RS_PROJECT'] : opts['project']
    project_exists = ( g_config.projects.has_key?( g_project_name ) )
    if ( not project_exists ) then
      puts OS::Getopt.usage( OPTIONS )
      puts "\nError project: #{g_project_name} does not exist or its configuration is unreadable."
      exit( 2 )
    end
    g_project = g_config.projects[ g_project_name ]
    g_project.load_config( )
    g_branch_name = ( nil == opts['branch'] ) ? g_project.default_branch : opts['branch']
    branch_exists = ( g_project.branches.has_key?( g_branch_name ) )
    if ( not branch_exists ) then
      puts OS::Getopt.usage( OPTIONS )
      puts "\nError project: #{g_project_name} does not have branch named #{g_branch_name} defined."
      exit( 4 )
    end
    g_Output = ( nil == opts['output'] ) ? nil : ::File::expand_path( opts['output'] )
    if ( g_Output.nil? ) then
      puts 'No output itd file specified.'
      puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
      exit( 2 )
    end
    g_Filter = ( nil == opts['filter'] ) ? DEFAULT_FILTER : opts['filter']
		g_Flatten = opts['flatten']
		g_Recursive = opts['recursive']
    g_Uncompressed = opts['uncompressed']
    g_regentcs = opts['regentcs']
    g_metadata_path = opts['metadata']
    g_metadata_template = opts['template']
    g_paths = []
    trailing.each do |trail|
      g_paths << OS::Path.normalise( trail )
    end
    if ( 0 == g_paths.size ) then
      puts 'No input files or paths specified.'
      puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
      exit( 3 )
    end

    #---------------------------------------------------------------------
    # Check output
    #---------------------------------------------------------------------
		if ( ::File::exists?( g_Output ) and ( not ::File::writable?( g_Output ) ) ) then
			g_log.error( "Output file: #{g_Output} is not writable.  Aborting." )
			exit( 4 )
		end

		# Construct Array of files to pack into zip.
    g_FileList = []
		g_paths.each do |path|
			if ( File::file?( path ) ) then			
				entry = {}
				entry[:src] = path
				entry[:dst] = OS::Path::get_filename( path )
				g_FileList << entry

			elsif ( File::directory?( path ) ) then
				files = OS::FindEx::find_files( OS::Path::combine( path, g_Filter ) ) unless ( g_Recursive )
				files = OS::FindEx::find_files_recurse( OS::Path::combine( path, g_Filter ) ) if ( g_Recursive )
			
				files.each do |file|
					if ( g_Flatten ) then
						entry = {}
						entry[:src] = file
						entry[:dst] = OS::Path::get_filename( file )
						g_FileList << entry
					else
						entry = {}
						entry[:src] = file
						entry[:dst] = file.sub( path, '' )
						# Sometimes removing path would leave a slash before filename
						if ( entry[:dst].start_with?("/") ) then
							entry[:dst].sub!( '/', '' )
						end
						g_FileList << entry
					end
				end
			end
		end

		#---------------------------------------------------------------------
    # Make sure tcs files exist
    #---------------------------------------------------------------------

    txdname = OS::Path::get_corebasename( g_Output )
    temp_dir = OS::Path::combine(Globals::instance().toolstemp,"metadata",txdname)

    OS::FileUtilsEx.delete_files(OS::Path.combine(temp_dir,"*.*"))

		g_perforce = Pipeline::SCM::Perforce.new( )
		g_perforce.connect( )

		g_changelist_number = CL_DESCRIPTION

		g_TclFileList = []
    g_FileList.each do |filepath|

      tcsname = OS::Path::get_basename( filepath[:src] ) + ".tcs"
      tcsname = OS::Path::combine( g_metadata_path, tcsname )

      tclname = OS::Path::get_basename( filepath[:src] ) + ".tcl"
      tclname = OS::Path::combine( temp_dir, tclname )

      g_changelist_number = make_sure_tcs_exists(g_perforce,g_changelist_number,tcsname,g_metadata_template,false,g_regentcs)
      make_sure_tcs_exists(nil,nil,tclname,OS::Path.remove_extension(tcsname))

      g_TclFileList << tclname
    end
		
    FileUtils::rm( g_Output ) if ( File::exists?( g_Output ) )
		ProjectUtil::data_zip_create( g_Output, g_FileList + g_TclFileList ) 

  rescue Exception => ex
    exit( ex.status ) if ( 'exit' == ex.message )

    puts "\n#{g_app_name} unhandled exception: #{ex.message}"
    puts ex.backtrace.join("\n\t")
  end
end

# %RS_TOOLSLIB%/util/data_mk_generic_itd_zip.rb
