@ECHO OFF
REM 
REM Starts disktools for GTA E2
REM

TITLE Disktools E2
SET TOOLSBIN=x:\pipedev\tools
SET DISKTOOLS=\util\Disktools

PUSHD %TOOLSBIN%\%DISKTOOLS%
ruby disktools.rb --xlsfilename=X:\Pipedev\tools\util\DiskTools\run3.xls --checkthruput=1
POPD

ECHO Disk tools has quit.
PAUSE
