#
# File:: restart_machine.rb
# Description:: Restarts the machine this script is run on.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 16th October 2009
#


#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/log/log'
require 'pipeline/os/getopt'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ "--help", "-h", Getopt::BOOLEAN, "display usage information." ],
]

#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------
begin
	#---------------------------------------------------------------------
	# Parse Command Line.
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] )
		puts OS::Getopt.usage( OPTIONS )
		puts ("Press Enter to continue...")
		$stdin.getc( )				
		exit( 1 )
	end	

	#---------------------------------------------------------------------
	# Kick off restart.
	#---------------------------------------------------------------------	
	system 'start shutdown.exe /r /f /c Rebooting'

rescue
	$stderr.puts "Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	exit -1
end

exit 0