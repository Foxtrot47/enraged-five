#
# File:: envtest.rb
# Description:: tests an environment variable before running a command.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 09th September 2011
#
# Passed in  :-  see OPTIONS ...
# Passed out :-  stderr contains all errors 
#                stdout for all other output. 
# Returns    :-  returns non zero upon detecting any errors 

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/file'
include Pipeline
require 'systemu'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ "--help", 		"-h", 	OS::Getopt::BOOLEAN, 	"display usage information." ],
	[ '--env', 		'-e', 	OS::Getopt::REQUIRED, 	'The environment variable and value to test' ],
	[ '--cmd', 		'-c', 	OS::Getopt::REQUIRED, 	'The command to execute if the environment checks pass' ],
]

INFO = "[colourise=black]INFO_MSG: "


#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------

begin
	g_AppName = File::basename( __FILE__, '.rb' )
	g_ProjectName = ''
	g_BranchName = ''
	g_Project = nil
	g_Config = Pipeline::Config.instance()

	#---------------------------------------------------------------------
	# --- PARSER COMMAND LINE                                          --- 
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] )
		puts OS::Getopt.usage( OPTIONS )
		puts ("Press Enter to continue...")
		$stdin.getc( )				
		Process.exit!( 1 )
	end	

	env = opts['env'] ? opts['env'] : nil
	cmd = opts['cmd'] ? opts['cmd'] : nil
	
	if (not cmd)
		$stderr.puts("Error: no cmd")
		Process.exit! -1
	end

	execute = true
	
	puts("Envtest command #{cmd} with env #{env}")
	
	if (env)		
		tests = env.split(",")
				
		tests.each do |test|
			if test.include?"="
				split = test.split("=")
				if (split.length==2)
					env_var =  split[0]
					value   =  split[1]
															
					if (	ENV[env_var] == nil and 
						ENV[env_var.upcase] == nil and 
						ENV[env_var.downcase] == nil)
						
						puts("Env var #{env_var} is not set")
						execute = false		
						
					else
						if 	(ENV[env_var] != value and 
							 ENV[env_var.upcase] != value and 
							 ENV[env_var.downcase] != value )
							 
							puts("Env var #{env_var} is #{ENV[env_var]} not #{value}")
							execute = false
						else
							puts("Env var #{env_var} is #{ENV[env_var]} as required.")					
						end
					end					
				else
					$stderr.puts "Bad format of env: #{test}"
				end
			else
				if 	(not ENV[test] and 
					 not ENV[test.upcase] and 
					 not ENV[test.downcase] )
					 
					execute = false 
					puts("Env var #{test} is not set")
					
				end
			end			
		end
	
	else
		puts("Warning: no env specified - command will just run")
	end

	if (execute)
#		Kernel.exec(cmd)
		error_count = 0
		
		puts "Executing #{cmd}...\n"
		status, stdout, stderr = systemu(cmd)
		status = $?
		puts "\n...Completed with status #{status} #{stderr.length} errors"

		$stderr.puts( cmd ) if ( stderr.length > 0 )

		stderr.each do |err|
			error_count += 1
			$stderr.puts "Error: #{err}"
		end

		stdout.each do |out|									
			puts out
		end

		Process.exit! -1 if error_count > 0

		Process.exit! status.to_i
		
	else
		puts("Command will not execute")
	end	
	
	Process.exit! 0

rescue Exception => ex
	$stderr.puts "Error: Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	Process.exit! -1
end
