#
# File:: kill_process.rb
# Description:: Kills all instances of passed process names.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 16th October 2009
#
# Pass in a list of process names.
# Returns 0 if no errors occur.
# Returns -1 if there where errors.

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'sys/proctable'
require 'pipeline/log/log'
require 'pipeline/os/getopt'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ "--help", "-h", Getopt::BOOLEAN, "display usage information." ],
	[ "--process_name", "-p", Getopt::REQUIRED, "Process name." ],
]
PROCESS_EXIT_SLEEP = 5

#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------
begin
	#---------------------------------------------------------------------
	# Parse Command Line.
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] )
		puts OS::Getopt.usage( OPTIONS )
		puts ("Press Enter to continue...")
		$stdin.getc( )				
		exit( 1 )
	end	

	process_name = opts['process_name'] ? opts['process_name'].downcase : nil
	ret = 0

	#---------------------------------------------------------------------
	# Kill process.
	#---------------------------------------------------------------------
	if (process_name)
		puts "Searching for process named: #{process_name}."

		num_found = 0

		#---------------------------------------------------------------------
		# Kill ALL and EVERY instance of this process.
		#---------------------------------------------------------------------	
		Sys::ProcTable.ps.each do |ps|
		if ps.name.downcase == process_name
			begin

  				puts "Killing process named: #{process_name}."

				Process.kill('KILL', ps.pid)

				num_found += 1

			rescue Exception => ex
				$stderr.puts "Exception killing process #{ps.name} #{ps.pid} #{ex.message}."
				ret = -1
			end
		end
		end

		#---------------------------------------------------------------------
		# Give the processes a chance to exit.
		#---------------------------------------------------------------------			  
		Kernel.sleep(PROCESS_EXIT_SLEEP) if (num_found>0)

		#---------------------------------------------------------------------
		# Check the process no longer exists.
		#---------------------------------------------------------------------		  
		Sys::ProcTable.ps.each do |ps|
		if ps.name.downcase == process_name
			$stderr.puts "Error: The process #{process_name} still exists."
			ret = -1
		end
		end

		puts "Killed #{num_found} processes named: #{process_name}"

	end
rescue
	$stderr.puts "Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	exit -1
end

exit ret