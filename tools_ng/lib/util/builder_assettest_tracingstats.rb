#
# File:: builder_assettest_tracingstats_append.rb
# Description:: append the xml file of tracing stats to historical log file(s).
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 22 January 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/gui/exception_dialog'
require 'pipeline/gui/log_window'
require 'pipeline/log/log'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_convert'
require 'pipeline/resourcing/path'
require 'pipeline/util/environment'
require 'pipeline/util/rage'
require 'pipeline/util/string'
include Pipeline
require 'wx'
require 'pipeline/projectutil/builder_assettest_tracingstats_append'
require 'pipeline/projectutil/builder_assettest_tracingstats_filter'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--help',			'-h',	OS::Getopt::BOOLEAN,	'display usage information.' ],
	[ '--append',		'-a',	OS::Getopt::BOOLEAN,	'action is to append.' ],
	[ '--filter',		'-f',	OS::Getopt::OPTIONAL,	'action is to filter, a ; separated string with the filter criteria.' ],
	[ '--datefrom',		'-df',	OS::Getopt::OPTIONAL,	'date range FROM' ],
	[ '--dateto',		'-dt',	OS::Getopt::OPTIONAL,	'date range TO' ],
	[ '--dest',			'-d',	OS::Getopt::REQUIRED,	'where to put results.' ],	
	[ '--nojpg',		'-nj',	OS::Getopt::BOOLEAN,	'Prevent JPG creation.' ],	
	[ '--noxml',		'-nx',	OS::Getopt::BOOLEAN,	'Prevent XML creation.' ],	
	[ '--noxls',		'-ns',	OS::Getopt::BOOLEAN,	'Prevent XLS creation.' ],
	[ '--charttitle',	'-cn',	OS::Getopt::OPTIONAL,	'Specify chart title' ],
	[ '--chartaxisy',	'-cay',	OS::Getopt::OPTIONAL,	'Specify chart axis name for Y ( values).' ]
]

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

begin
	g_AppName = File::basename( __FILE__, '.rb' )
	g_Log = Log.new( g_AppName )
	g_Config = Pipeline::Config::instance( )
	g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
	
	dest_filename = File::expand_path( g_Opts['dest'] )
	dest_filename = OS::Path::normalise( dest_filename )
	
	# Here we ensure we have absolute filenames, relative filenames being 
	# expanded based on the current working path.
	g_Trailing.each_with_index do |filename, index|
		puts filename
		filename = File::expand_path( filename )
		g_Trailing[index] = OS::Path::normalise( filename )
		
		throw IOError.new( "File #{filename} does not exist.  Aborting convert." ) \
		unless ( File::exists?( filename ) )
	end
	
	date_from	= 	g_Opts['datefrom']
	date_to		= 	g_Opts['dateto']
	filter		=   g_Opts['filter']
	
	save_jpg	=	true
	save_xml	=	true
	save_xls	=	true
	
	save_jpg	=	false if g_Opts['nojpg']
	save_xml	=	false if g_Opts['noxml']
	save_xls	=	false if g_Opts['noxls']
	
	chart_title  =   g_Opts['charttitle']
	chart_axis_y =   g_Opts['chartaxisy']
	
	ProjectUtil::builder_assettest_tracingstats_append( g_Trailing, dest_filename ) if g_Opts['append']
	ProjectUtil::builder_assettest_tracingstats_filter( chart_title, g_Trailing, dest_filename, filter, date_from, date_to, save_jpg, save_xls, save_xml, chart_axis_y ) if g_Opts['filter']
	
rescue Exception => ex
	g_Log.exception( ex, "Unhandle exception during convert" )
	ex.backtrace.each do |m| g_Log.error( m ); end
	GUI::ExceptionDialog::show_dialog( ex, "#{g_AppName} unhandled exception" )
	
	# Only require Enter press when an exception has occurred.
	puts "\nPress Enter or close this window to continue..."
	$stdin.gets( )
end

# builder_assettest_tracingstats_append.rb
