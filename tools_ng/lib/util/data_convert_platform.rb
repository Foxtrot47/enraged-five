#
# File:: data_convert_platform.rb
# Description:: Convert project independent data to platform data.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 7 August 2008
#


#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/content/treecore'
require 'pipeline/content/content_core'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/projectutil/data_convert.rb'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
    [ '--project', '-p', Getopt::REQUIRED, 'project data to convert (e.g. jimmy, gta4_jpn).' ],
    [ '--branch', '-b', Getopt::REQUIRED, 'project branch to convert (e.g. dev, dev_migrate).' ],
    [ '--rebuild', '-r', Getopt::BOOLEAN, 'force rebuild of data.' ],
    [ '--temp', '-t', Getopt::BOOLEAN, 'output to temporary location (does not overwrite existing build data).' ],
    [ '--help', '-h', Getopt::BOOLEAN, 'display usage information.' ]
]

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

    #-------------------------------------------------------------------------
    # Entry-Point
    #-------------------------------------------------------------------------

    begin
        g_AppName = File::basename( __FILE__, '.rb' )
        g_ProjectName = ''
        g_Project = nil
        g_Branch = ''
        g_Config = Pipeline::Config.instance( ) 
    
        #---------------------------------------------------------------------
        # Parse Command Line
        #---------------------------------------------------------------------
        opts, trailing = OS::Getopt.getopts( OPTIONS )
        if ( opts['help'] ) then
            puts OS::Getopt.usage( OPTIONS )
            exit( 1 )
        end
        
        g_ProjectName = ( nil == opts['project'] ) ? '' : opts['project']
        project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
        if ( not project_exists ) then
            puts OS::Getopt.usage( OPTIONS )
            puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
            exit( 2 )
        end
        g_Project = g_Config.projects[ g_ProjectName ]
        if ( not g_Project.enabled ) then
            puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
            exit( 3 )
        end
        g_Project.load_config( )
        g_BranchName = ( nil == opts['branch'] ) ? g_Project.default_branch : opts['branch']
        branch_exists = ( g_Project.branches.has_key?( g_BranchName ) )
        if ( not branch_exists ) then
            puts OS::Getopt.usage( OPTIONS )
            puts "\nError project: #{g_ProjectName} does not have branch named #{g_BranchName} defined."
            exit( 4 )
        end
        g_Rebuild = opts['rebuild']
        g_Temp = opts['temp']

        puts "Converting independent data for #{g_Project.uiname}..."
        puts "Branch: #{g_BranchName}"
        puts "Rebuild: #{g_Rebuild}" if ( g_Rebuild )
		
		
		# Find all local independent files
		independent_wildcard = OS::Path::combine( g_Project.branches[g_BranchName].independent, '*.*' )
		independent_files = OS::FindEx::find_files_recurse( independent_wildcard )
		puts "\nNumber of independent files: #{independent_files.size}."
		
		# Convert these files
		converted_files = ProjectUtil::data_convert_file( independent_files, g_Rebuild ) do |content, success|
		
			next unless ( content.is_a?( Pipeline::Content::File ) )
			
			puts "Converted: #{content.filename}" if success
			puts "Failed to convert: #{content.filename}" unless success
		end
		puts "\nConverted #{converted_files.size} files."
        puts "Done."
        
    rescue Exception => ex
        exit( ex.status ) if ( 'exit' == ex.message )
    
        puts "\n#{g_AppName} unhandled exception: #{ex.message}"
        puts ex.backtrace.join("\n\t")      
    ensure

        puts "Press Enter key to continue..."
        $stdin.getc( )  
    end 
end

# data_convert_platform.rb
