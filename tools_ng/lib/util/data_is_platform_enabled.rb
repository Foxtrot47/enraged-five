#
# File:: %RS_TOOLSLIB%/util/data_is_platform_enabled.rb
# Description:: Set process exit code based on whether the specified platform is enabled.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 3 March 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/gui/log_window'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_extract'
require 'pipeline/util/rage'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--project', '-p', Getopt::REQUIRED, 'project data to convert (e.g. jimmy, gta4_jpn).' ],
    [ '--branch', '-b', Getopt::REQUIRED, 'project branch to convert (e.g. dev, dev_migrate).' ],
    [ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING_DESC = { 
    'targets' => 'list of targets to check.'
}

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_ProjectName = ''
		g_Project = nil
		g_Branch = ''
		g_Config = Pipeline::Config.instance( ) 
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
			exit( 2 )
		end
		g_Debug = opts['debug'].nil? ? false : true
		Pipeline::Config::instance()::log_trace = g_Debug
		
		g_ProjectName = ( opts['project'].nil? ) ? ENV['RS_PROJECT'] : opts['project']
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 3 )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		g_Project.load_config( )
		g_BranchName = ( nil == opts['branch'] ) ? g_Project.default_branch : opts['branch']
		if ( not g_Project.branches.has_key?( g_BranchName ) ) then
			puts "\nError project: #{g_ProjectName} does not have a branch called #{g_BranchName}."
			exit( 5 )
		end
		g_Branch = g_Project.branches[g_BranchName]
		
		enabled = true
		trailing.each do |target|
			if ( not g_Branch.targets.has_key?( target ) ) then
				puts "\nError: target #{target} not available for this project."
				exit( 6 )
			end
			enabled = ( enabled and g_Branch.targets[target].enabled )
		end
		exit( 0 ) unless ( enabled )
		exit( 1 ) if ( enabled )
		
	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")  
	end
end

# %RS_TOOLSLIB%/util/data_is_platform_enabled.rb
