#
# File:: 
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 31st October 2011
#
# Passed in  :-  files to process
# Passed out :-  stderr contains all errors 
#                stdout for all other output. 
# Returns    :-  returns non zero upon detecting any errors 

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'rexml/document'
include Pipeline
require 'systemu'
require 'fileutils'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ "--help", 			"-h", OS::Getopt::BOOLEAN, 	"display usage information." ],
]

TRAILING_DESC 		= 'File paths separated by spaces (e.g. x:/gta5/src/dev).'

INFO			= "" # comment back in for a verbose report "[colourise=grey]INFO_MSG: "
INFO_BLACK		= "[colourise=black]INFO_MSG: "
INFO_BLUE		= "[colourise=blue]INFO_MSG: "
INFO_GREEN		= "[colourise=green]INFO_MSG: "

#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------
if __FILE__ == $0

	error_count = 0

	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_ProjectName = ''
		g_BranchName = ''
		g_Project = nil
		g_Config = Pipeline::Config.instance()

		#---------------------------------------------------------------------
		# Parse Command Line.
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		
		if ( opts['help'] )
			puts OS::Getopt.usage( OPTIONS )
			puts ("Press Enter to continue...")
			$stdin.getc( )				
			Process.exit! 1
		end

		if ( ( 0 == trailing.size ) ) then
			$stderr.puts 'No trailing file arguments specified. Exiting.'
			puts OS::Getopt.usage( OPTIONS, { 'files' => TRAILING_DESC } )
			Process.exit( 2 )
		end				

		files = []		
		trailing.each do |wildcard|
			files_found = OS::FindEx::find_files_recurse( wildcard )	

			files_found.each do |filename|
				files << filename if File.exist? filename
			end				
		end	
		
		if ( files.length > 0 )
			# process files
			files.each do |filename|
				cmd = '"C:\\Program Files (x86)\\Microsoft Visual Studio 9.0\\VC\\bin\\cl.exe" /O2 /Ob2 /Oi /Ot /I "X:\\gta5\\src\\dev\\rage\\stlport\\STLport-5.0RC5\\stlport" /I "X:\\gta5\\src\\dev\\rage\\base\\src" /I "X:\\gta5\\src\\dev\\rage\\suite\\src" /I "x:\\gta5\\src\\dev\\game\\vs_project2_lib\\.." /I "X:\\gta5\\src\\dev\\rage\\naturalmotion\\include" /I "X:\\gta5\\src\\dev\\rage\\naturalmotion\\src" /I "X:\\gta5\\src\\dev\\rage\\script\\src" /I "X:\\gta5\\src\\dev\\rage\\framework\\src" /D "WIN32" /D "_LIB" /D "_MBCS" /FD /MT /Gy /GR /Fo"xenon_beta_2008\\" /W4 /WX /c /Zi /FI " X:\\gta5\src\\dev\\rage\\base\\src\\forceinclude\\xenon_beta.h" /FI "game_config.h" /FI  "basetypes.h"  /GF /I "C:\\Program Files (x86)\\Microsoft Xbox 360 SDK\\include\\xbox" /EP /C ' + "#{filename} > #{filename}.pp"
				puts "INFO_MSG: #{cmd}"
				status, stdout, stderr = systemu(cmd)						

				# --- interpret output ---
				$stderr.puts  "\n\n\n#{stderr.length} ERRORS FOUND in parcodegen_ci_pre_convert.rb!\n\n\n" if (stderr.length > 0)					
				stderr.each { |err| $stderr.puts "#{err}"; error_count += 1 }
				stdout.each do |out| 
					puts("\t\t#{cmd} : #{out}") 
				end				
			end										
			
		end

	rescue Exception => ex
		error "Unhandled exception: #{ex.message}"
		error "Backtrace:"
		ex.backtrace.each { |m| error "\t#{m}" }
		Process.exit -1
	end

	ret_code = ( error_count > 0 ) ? -1 : 0

	Process.exit! ret_code

end #if __FILE__ == $0