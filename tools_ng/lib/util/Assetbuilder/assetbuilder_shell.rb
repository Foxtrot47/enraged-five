#
# File:: assetbuilder_shell.rb
# Description:: A remote (or local ) shell to an assetbuilder server.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com> David Muir <david.muir@rockstarnorth.com>
# Date:: 07 June 2010
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require "source/builder/shell/console"
require "source/builder/shell/command"
require "source/builder/shell/command_queue"
require 'pipeline/os/getopt'
include Assetbuild::Builder::Shell

OPTIONS = [
	[ '--help',	'-h', Pipeline::OS::Getopt::BOOLEAN,	'show usage information' ],
	[ '--server',	'-s', Pipeline::OS::Getopt::REQUIRED,	'specify server IP eg. RSGEDIABLD1' ],
	[ '--port',	'-p', Pipeline::OS::Getopt::REQUIRED,	'specify port eg. 8787' ],
	[ '--command',	'-c', Pipeline::OS::Getopt::OPTIONAL,	'command to run then exit' ],
]

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
begin

	g_AppName = File::basename( __FILE__, '.rb' )

	opts, trailing = Pipeline::OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] ) then
		puts Pipeline::OS::Getopt.usage( OPTIONS )
		Process.exit!
	end

	server 	= (opts['server'] == nil) ? ENV["COMPUTERNAME"] : opts['server']
	port 	= (opts['port'] == nil) ? CommandQueue::DEFAULT_PORT : opts['port']
	command = (opts['command'] == nil) ? nil : opts['command']

	if ( __FILE__ == $0 ) then	
		puts "Establishing remote shell to server #{server} on port #{port}"
		shell = Console.new( nil, server, port )
		
		if (command)
			puts "Running command #{command} on assetbuilder"
			result = shell.run_command( command )
			puts "Running command #{command} returned #{result}"
			Process.exit! result.to_i
		end
		
		shell.start
	end

rescue Exception => ex

	puts "#{g_AppName} unhandled exception: #{ex.message}"
	puts "Call stack:"
	puts ex.backtrace.join( "\n\t" )
	Process.exit -1
end

Process.exit 0 

# assetbuilder_shell.rb
