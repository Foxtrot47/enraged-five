#
# File:: config.rb
# Description:: Builder application configuration class.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 20 October 2008
#
# == Description
# This file contains the Builder configuration classes which can parse
# generic configuration details from an XML file.  It is expected that each
# builder application will require to inherit from ConfigBase their own
# configuration class to provide builder-specific details.
#
# The configuration data is fully project, branch and target environment
# aware.  All string configuration data etc can specify environment variables
# using the $(...) syntax and it will be expanded for the project, branch and
# target as appropriate.
#
# == Example Usage
#
#   config = ConfigBase::from_xml( 'x:/jimmy/bin/assetbuild.xml' )
#
#   <config  sleep="30" max_changelists="10">
#     <report local_path="..." web_path="...">
#       <xslt_templates>
#         <xslt name="html_email" filename="..." />
#         <xslt name="html_email" filename="..." />
#         <xslt name="html_email" filename="..." />
#       <xslt_templates>
#       <emails>
#         <address name="maintainer" address="..." alias="..." />
#         <address name="list" address="..." alias="..." />
#         <address name="from" address="..." alias="..." />
#       </emails>
#     </report>
#   </config>
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/os/path'
require 'pipeline/util/environment'
require 'rexml/document'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module Builder
module Config
	
	#
	# == Description
	# Object that represents the email configuration data.  The email 
	# configuration data contains email addresses, aliases and XSLT template
	# filenames for email.
	#
	# === Example XML
	# Below is an example XML snippet of email configuration data:
	#   <email_config>
	#      <xslt name="email_xslt" filename="$(toolsbin)/util/RSNAssetBuilder/data/report_email.xslt" />
	#	   <email name="maintainer" alias="David Muir" address="david.muir@rockstarnorth.com" />
	#      <email name="list" alias="Asset Builder List" address="david.muir@rockstarnorth.com,luke.openshaw@rockstarnorth.com" />
	#   </email_config>
	#
	class EmailConfig
		XML_NODE_NAME = 'email_config'
		
		attr_reader :xslt_templates	# Hash of XSLT templates within data.
		attr_reader :emails		
		
		def initialize( addresses )
			@addresses = addresses
		end
		
		def pretty_print( indent = 0, indent_char = "\t" )
			(indent).times do print indent_char; end
			print "Emails"
			
			@addresses.each do |address|
				address.pretty_print( indent+1, indent_char )	
			end
		end
		
		#
		# Parse an XML node and return an EmailConfig object.
		#
		def EmailConfig::from_xml( xml_node, env )
			throw ArgumentError.new( "Invalid XML node (#{xml_node.name})." ) \
				unless ( xml_node.name == XML_NODE_NAME )
			
			xml_node.elements.each( XML_NODE_EMAIL ) do |xml_email_node|
			end
			xml_node.elements.each( XML_NODE_XSLT ) do |xml_xslt_node|

			end
		end

		#--------------------------------------------------------------------
		# Private
		#--------------------------------------------------------------------
	private
		XML_NODE_EMAIL = 'email'
		XML_NODE_XSLT = 'xslt'

		def EmailConfig::parse_xslt( xml_node, env )
		end

		def EmailConfig::parse_email( xml_node, env )
		end
	end
	
end # Config module
end # Builder module
end # Pipeline module

# config.rb
