#
# File:: command_queue.rb
# Description:: Builder CommandClient class.  
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com> David Muir <david.muir@rockstarnorth.com>
# Date:: 02-06-2010
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require "source/builder/shell/command"
require "source/builder/shell/shell"
require 'pipeline/util/thread'
require 'drb'
require 'drb/acl'
require 'rinda/tuplespace'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Assetbuild
module Builder
module Shell
			
	#
	# == Description
	# The module implements the shell command queue client which should be
	# included by the various Shell implementations we will have.  This will
	# allow the Shell to send commands to be queued to the builder application
	# which will fetch them via the ShellCommandQueue module.
	#
	module CommandClient
		
		#
		# Shutdown the CommandClient, stopping the DRb service and stopping
		# our queue thread.
		#
		def shutdown( )
			DRb::stop_service( )
			@active = false
			@queue_thread.join( )
		end
		
		#
		# Return command items in the queue, and/or invoke a user block for 
		# each item in the queue.
		#
		def queue_walk( &block )
			items = @ts.read_all( [ :Command, nil, nil, nil, nil, nil ] )
			items.each do |item|
				yield( item ) if ( block_given? )
			end
			items
		end
		
		#
		# Return result items in the queue, and/or invoke a user block for
		# each item in the result queue.
		#
		def result_walk( machine = ENV['MACHINENAME'], user = ENV['USERNAME'], script = $0, process_num = $$, &block )
			items = @ts.read_all( [ :CommandResult, machine, user, script, process_num, CommandResult ] )
			items.each do |item|
				yield( item ) if ( block_given? )
			end
			items
		end
		
		#--------------------------------------------------------------------------
		# Protected
		#--------------------------------------------------------------------------
		protected
		#
		# Start the shell's internal loop of processing commands.
		#
		# The method yields to the user-block after a command result has been posted
		# to our distributed Tuplespace and requires processing.  It is then up to the 
		# shell class to process the CommandResult as required.
		#
		# As the CommandResult objects are taken from the Tuplespace they shall not
		# be available for other shells to process.
		#
		def start_result_processing( client = ENV['COMPUTERNAME'], user = ENV['USERNAME'], script = $0, process_num = $$, &block )
			
			@queue_thread = Thread.new do
				Thread.current[:name] = THREAD_NAME		
				while ( @active )
					
					result = @ts.take( [ :CommandResult, client, user, script, process_num, nil ] )					
					puts "CLIENT DEQUEUE COMMAND RESULT : #{client} #{user} #{script} #{process_num} #{result.command.pretty_string} #{result.result}" if @print_queue_activity
					
					# Invoke the user-block with the CommandResult object.
					yield( result[-1] ) if ( block_given? )
				end
			end
		end
		
		#--------------------------------------------------------------------------
		# Private
		#--------------------------------------------------------------------------
		private
		THREAD_NAME_DRB = '__BuilderFramework_DRbCommandClient'
		THREAD_NAME = '__BuilderFramework_CommandClient'
		
		#
		# Initilaise the client DRb connection and reference to TupleSpace.
		#
		def init_client( server = ENV['COMPUTERNAME'], port = ShellCommandQueue::DEFAULT_PORT )
			@server = server
			@port = port
			@print_queue_activity = false
			@remote = ( server != ENV['COMPUTERNAME'] )
			
			DRb::start_service()
			DRb::thread[:name] = THREAD_NAME_DRB
			@ts = Rinda::TupleSpaceProxy.new( DRbObject.new_with_uri( "druby://#{@server}:#{@port}" ) )		
			
			puts "Client Connected : #{ENV['COMPUTERNAME']} #{ENV['USERNAME']} #{$0} #{$$}"
			puts "Remote Shell" if @remote
		end
		
		#
		# Queue a ShellCommand object to our distributed queue.
		#
		def enqueue_command( command, client = ENV['COMPUTERNAME'], user = ENV['USERNAME'], script = $0, process_num = $$ )
			throw ArgumentError.new( "Invalid Command object (#{command.class})." ) \
				unless ( command.is_a?( Command ) )
			
=begin			
			# just for debugging - shows queue ops on *client*
			if (command.split[0] == "console_toggle_queue_activity")
				@print_queue_activity = !@print_queue_activity
				return
			end
=end			
			if ( @remote and ( command.name == "exit" or command.name == "q" ) )
				@active = false
				return
			end

			puts "CLIENT ENQUEUE COMMAND:\n \t#{client}\n \t#{user}\n \t#{script}\n \t#{process_num}\n \t#{command}\n \t#{command.pretty_string}\n" if @print_queue_activity
			@ts.write( [ :Command, client, user, script, process_num, command ] )
		end
	end			
end # Shell module
end # Builder module
end # Pipeline module

# command_client.rb
