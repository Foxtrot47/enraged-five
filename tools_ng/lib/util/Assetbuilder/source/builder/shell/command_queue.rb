#
# File:: command_queue.rb
# Description:: 
# Author:: Derek Ward <derek.ward@rockstarnorth.com> David Muir <david.muir@rockstarnorth.com>
# Date::02 June 2010
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require "source/builder/shell/command"
require "source/builder/shell/shell"
require 'pipeline/config/projects'
require 'pipeline/util/thread'
require 'drb'
require 'drb/acl'
require 'rinda/tuplespace'
require 'socket'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Assetbuild
module Builder
module Shell
	
	#
	# == Description
	# This class represents a builder's command queue.  There are no methods
	# to enqueue commands onto the queue as this will be done by remote
	# objects.
	#
	module CommandQueue
		
		DEFAULT_PORT = 10001

		# customise as required per studio - or write some code to read it in from file.
		PERMITTED_MACHINES = [	ENV['COMPUTERNAME'],  
					            "EDIW-DWARD1", 
					            "EDIW-GSMITH", 
					            "EDIW-DMUIR3"]
		
		def shutdown( )
			DRb::stop_service( )
			@active = false
		end
		
		protected
		def init_queue( commands, server = ENV["COMPUTERNAME"], port = DEFAULT_PORT )
			@active = true
			@queue = Queue.new( )
			@port = port
			@pool = ThreadPool.new( 10 )
			@ts = Rinda::TupleSpace.new( )
			@commands = commands
			@print_queue_activity = false

			config = Pipeline::Config.instance
			config.project.load_config		
			branch = @config.project.default_branch

			if ( branch == 'dev_migrate' ) then
				port += 1
			elsif ( branch == 'dev_japanese' ) then
				port += 2
			end
						
			setup_drb( server, port)
		end
		
		#
		# Setup Access control List
		def setup_access_control_list()
			# DW - Ack looks like ONLY IP addresses are permitted here :( 
			# todo - drive with a config file
			# NEVERTHELESS YOU SHOULD MAKE THESE MACHINES USE STATIC IP ADDRESSES
			# Need to convert host names into IP octet due to DHCP - otherwise it's not secure and could stop working.

			acl_cmds = %w(deny all)

			# DNS lookup.
			PERMITTED_MACHINES.each do |machine|
				begin
					addr_info = Socket.getaddrinfo( machine, nil )
					if ( addr_info )
						ip = addr_info[0][3]
						puts "Access granted to machine #{machine} ( #{ip} )"
						acl_cmds << "allow" 
						acl_cmds << ip								
					else
						puts "Error: No socket info for machine #{machine} it will not be permitted access."
					end
				rescue
					puts "Failed to get address info for #{machine}"
				end
			end	

			acl = ACL.new( acl_cmds )
			DRb::install_acl( acl )
		end
		
		#
		# Setup the Distributed Ruby thread.		
		def setup_drb( server, port)
			begin	
				setup_access_control_list()
				DRb::start_service( "druby://#{server}:#{@port}", @ts )
				DRb::thread[:name] = THREAD_NAME_DRB
			rescue Exception => ex	
				puts "Unhandled exception during ACL and DRb setup: #{ex.inspect()}"
				puts ex.backtrace.join( "\n" )
			end	
		end
		
		#
		# Start the builder application internal loop of processing commands.
		# The method yields to the user-block after a command has been dequeued
		# and requires processing.  It is then up to the builder application
		# to process the command as required.
		#
		def start_queue_processing( &block )
			
			@queue_thread = Thread.new() do 
				Thread.current[:name] = THREAD_NAME
				while ( @active )
					begin												
						# Free our queue processing by utilising a thread pool
						# for running the actual command code.  This requires
						# that the commands are written in a thread safe manner.
						@pool.process do							

							command_obj 		= @ts.take( [ :Command, nil, nil, nil, nil, nil ] )						
							
							command_symbol		= command_obj[0]
							sender_host		= command_obj[1]
							sender_user		= command_obj[2]
							sender_script		= command_obj[3]
							sender_process_num	= command_obj[4]
							command			= command_obj[5]
							puts "SERVER DEQUEUE COMMAND:\n \t#{sender_host}\n \t#{sender_user}\n \t#{sender_script}\n \t#{sender_process_num}\n \t#{command}\n \t#{command.pretty_string}\n" if @print_queue_activity
							
							# Invoke builder code to execute the command.
							status = run_command( command )							
														
							throw RuntimeError.new( "Invalid command function, must return CommandResultCompleted or CommandResultError object." ) \
							unless ( status.is_a?( CommandResultCompleted ) )
														
							# Yield to user code block (if specified)							
							yield( cmdinst, status ) if ( block_given? )
														
							# Post CommandResult object for originator to take.
							puts "returning result to #{sender_host} #{sender_user} #{sender_script} #{sender_process_num}" if @print_queue_activity
							enqueue_command_result( status, sender_host, sender_user, sender_script, sender_process_num )
						end
						
					rescue Exception => ex
						
						puts "Unhandled exception during queue processing: #{ex.inspect()}"
						puts ex.backtrace.join( "\n" )
					end
				end
			end
		end
		
		#
		# If the command is supported run the command, invoking a Proc in the engine.
		#		
		def run_command( command )			
			@commands.each do |cmd|		
				if ( command.name == cmd.name or cmd.shortcuts.include?(command.name) )
					status = cmd.proc.call( command.parameters.join(" ") )
					status.command = command
					throw RuntimeError.new( "Invalid command function, must return CommandResultCompleted or CommandResultError object." ) \
						unless ( status.is_a?( CommandResultCompleted ) )
							
					return status
				end
			end
			
			puts "Unknown command '#{command.name}'"
			nil
		end
		
		#
		# Enqueue a CommandStatus message.
		#
		def enqueue_command_result( result, host = ENV['COMPUTERNAME'], user = ENV['USERNAME'], script = $0, process_num = $$ )
			throw RuntimeError.new( "Invalid CommandResult message (#{result.class})." ) \
			unless ( result.is_a?( CommandResult ) )
			
			puts "SERVER ENQUEUE COMMAND RESULT:\n\t#{host}\n\t#{user}\n\t#{script}\n\t#{process_num}\n\t#{result.result}\n" if @print_queue_activity
			@ts.write( [ :CommandResult, host, user, script, process_num, result ] )
		end
		
		#
		# Return whether we have any outstanding commands in the queue.
		#
		def commands?( )
			queue = @ts.read_all( [ :Command, nil,  nil , nil, nil, nil ] )
			( queue.size > 0 )
		end
		
		#--------------------------------------------------------------------------
		# Private
		#--------------------------------------------------------------------------
		private
		THREAD_NAME_DRB = '__BuilderFramework_DRbCommandQueue'
		THREAD_NAME = '__BuilderFramework_CommandQueue'
	end

end # Shell module
end # Builder module
end # Pipeline module

# command_queue.rb
