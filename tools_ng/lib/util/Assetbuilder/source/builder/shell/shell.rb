#
# File:: shell.rb
# Description:: Core builder shell classes and modules.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 13 Novemeber 2008
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require "source/builder/shell/command"
require "source/builder/shell/command_client"
require "source/builder/shell/command_queue"

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Assetbuild
module Builder
module Shell
	
	#
	# == Description
	# Shell base class.  The shell base class initialises a DRb session for
	# the client, setting up a command queue connection and a method to
	# enqueue commands (which the specific shell implementation must use
	# to add commands).
	#
	class ShellBase
		include CommandClient
		
		def initialize( server = ENV['COMPUTERNAME'], port = CommandServer::DEFAULT_PORT )
			init_client( server, port )
		end
		
		def ShellBase::log( )
			@@shell_log = Log.new( 'shell_base' ) if ( @@shell_log.nil? )
			@@shell_log
		end
		
	private
		@@shell_log = nil
	end

end # Shell module
end # Builder module
end # Pipeline module

# shell.rb
