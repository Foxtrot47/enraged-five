#
# File:: shell_commands.rb
# Description:: Builder shell command classes.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 14 Novemeber 2008
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'shellwords'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Assetbuild
module Builder
module Shell
	
	#
	# == Exception
	# Exception indicating an invalid command.
	#
	class InvalidCommand < Exception; end
	
	#
	# == Description
	#
	class Param
		attr_reader :name		# Parameter name.
		attr_reader :valuetype	# Ruby class indicating type (e.g. String, Fixnum)
		
		NONE = []
		
		def initialize( name, type )
			@name = name
			@valuetype = type
		end
	end

	#
	# == Description
	# The Command class fully represents a user or pre-defined shell command
	# that the console may execute.  The Ruby Proc object is typically defined
	# in user code to carry out the necessary command actions.
	#
	class Command
		attr_reader :name		# String command name
		attr_reader :shortcuts	# Array of shortcut keys for command (e.g. 's', '?')
		attr_reader :help		# String help information (optional: for shell help)
		attr_reader :parameters	# Array of Param objects
		attr_reader :proc		# Ruby Proc object for user-routine executed
		
		NO_SHORTCUTS = []
		
		def initialize( name, shortcuts, help, params, proc )
			throw ArgumentError.new( "Invalid name String, #{name.class}." ) \
				unless ( name.is_a?( String ) )
			throw ArgumentError.new( "Invalid Array of shortcuts, #{shortcuts.class}." ) \
				unless ( shortcuts.nil? or shortcuts.is_a?( Array ) )
			throw ArgumentError.new( "Invalid params Array, #{params.class}." ) \
				unless ( params.nil? or params.is_a?( Array ) )
			throw ArgumentError.new( "Invalid proc Proc object, #{proc.class}." ) \
				unless ( proc.nil? or proc.is_a?( Proc ) )
				
			@name = name
			@shortcuts = shortcuts
			@help = help
			@parameters = params
			@proc = proc
		end
		
		def pretty_string
			"#{@name} #{parameters.join(" ")}"
		end
	end	
		
	#------------------------------------------------------------------------
	# Command Function Result Classes
	#------------------------------------------------------------------------
	
	#
	# == Description
	# Base class for command results.
	#
	class CommandResult
		attr_reader :result			# Variant, Proc object return value.

		def initialize( result )
			@result = result
		end
	end
	
	#
	# == Description
	# This class represents the result of a command executed by a builder 
	# application.  It includes a reference to the CommandInst and the
	# return value of the user-defined command function.
	#
	class CommandResultCompleted < CommandResult
		attr_accessor :command	# Command object.
		
		def initialize( result, command = nil )
			super( result )
			@command = command
		end
	end
	
	#
	# == Description
	# This class represents a command error.
	#
	class CommandResultError < CommandResultCompleted
		
		def initialize( result, command = nil )
			super( result, command )
		end
	end
	
end # Shell module
end # Builder module
end # Pipeline module

# shell_commands.rb
