#
# File:: reportwriter.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 28 July 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require "source/builder/reporting/email_exception"
require 'pipeline/config/projects'
require 'pipeline/util/email'
require 'pipeline/util/environment'
require 'systemu'

#-----------------------------------------------------------------------------
# Code
#-----------------------------------------------------------------------------
module Pipeline
module Builder

	#
	# == Description
	# Base report writer class.
	#
	class ReportWriter
		#
		# The report argument can either be a serialised XML document filename
		# or a REXML::Document object.
		#
		def initialize( report )
			raise ArgumentError.new( "Report is not a valid filename or REXML::Document." ) \
				unless ( report.is_a?( REXML::Document ) or ::File::exists?( report ) )			
			@report = report
		end
	end

	#
	# == Description
	# XSLT report transformation.
	#
	class XSLTReportWriter < ReportWriter
		def initialize( report )
			super( report )			
			@xslt_processor = Config::instance().envsubst( XSLT_PROCESSOR )
		end
		
		#
		# Run the XSLT processor template on the report XML, returning the
		# stdout if no filename given or the processor's exit status 
		# (Process::Status object) if a filename is given.
		#
		def write( template, filename = nil )
			
			begin
				# apologies for this code... to rushed to be able to do it properly. There are no doubt better ways.
				if ( @report.is_a?( REXML::Document ) ) then									
					aFile2 = File.new("c:/hackxml.xml", File::CREAT|File::TRUNC|File::RDWR)
					aFile2.syswrite(@report.to_s()) if aFile2
				end				
				
				if ( filename.nil? ) then
					
					report_command = "#{@xslt_processor} #{@report} #{template}" \
					unless ( @report.is_a?( REXML::Document ) )
					report_command = "#{@xslt_processor} - #{template} < c:/hackxml.xml" \
					if ( @report.is_a?( REXML::Document ) )										
					
					status, stdout, stderr = systemu( report_command )
					
					stdout	
				else
					
					report_command = "#{@xslt_processor} #{@report} #{template} -o #{filename}" \
					unless ( @report.is_a?( REXML::Document ) )
					report_command = "#{@xslt_processor} - #{template} < c:/hackxml.xml" \
					if ( @report.is_a?( REXML::Document ) )
					
					puts report_command
					
					status, stdout, stderr = systemu( report_command )
					
					status
				end
			rescue Exception => ex
				Pipeline::Builder::EmailException::send("#{$PROGRAM_NAME}",ex, Config::instance())
				puts "XSLTReportWriter exception: #{ex.message}"
				puts ex.backtrace.join()
			end
		end
		
		private
		XSLT_PROCESSOR = '$(toolsbin)/nxslt2.exe'
	end
	
	#
	# == Description
	# Format report for email, using XSLT, and then send it.
	#
	class EmailReportWriter < XSLTReportWriter
		def initialise( report )
			super( report )
		end
		
		def write( template, subject, from, to, reply_to, 
				from_alias = '', to_alias = '', reply_to_alias = '' )
			@c = Config::instance()
			output = super( template )
			
			send_email( @c.mailserver, @c.mailport, subject, output, 
				from, to, reply_to, 
				from_alias, to_alias, reply_to_alias )
		end
		
		#---------------------------------------------------------------------
		# Private Methods
		#---------------------------------------------------------------------
		private
		#
		# Send email.
		#
		def send_email( server, port, subject, body, from, to, reply_to = '', 
				from_alias = '', to_alias = '', reply_to_alias = '' )
			
			begin				
				msgstr = "From: #{from_alias} <#{from}>\n"
				msgstr << "To: #{to_alias}\n"
				msgstr << "Reply-To: #{reply_to_alias} <#{reply_to}>\n" unless ( '' == reply_to )
				msgstr << "Subject: #{subject}\n"
				msgstr << "Date: #{Time.now}\n\n"
				msgstr << body
				
				Net::SMTP.start( server, port ) do |smtp|
					smtp.send_message( msgstr, from, to )
				end
			rescue Exception => ex
				Pipeline::Builder::EmailException::send("#{$PROGRAM_NAME}",ex, Config::instance())
				puts "Exception sending email: #{ex.message}"
				puts ex.backtrace.join("\n")
			end
		end
	end
	
	#
	# == Description
	# Format report for HTML email, using XSLT, and then send it.
	#
	class HTMLEmailReportWriter < XSLTReportWriter
		
		def initialise( report )
			super( report )
		end
		
		def write( template, subject, from, to, reply_to, 
				from_alias = '', to_alias = '', reply_to_alias = '' )
			@c = Config::instance()
			output = super( template )
			
			send_email( @c.mailserver, @c.mailport, subject, output, 
				from, to, reply_to, 
				from_alias, to_alias, reply_to_alias )
		end	
		
		#---------------------------------------------------------------------
		# Private Methods
		#---------------------------------------------------------------------
		private
		#
		# Send email.
		#
		def send_email( server, port, subject, body, from, to, reply_to = '', 
				from_alias = '', to_alias = '', reply_to_alias = '' )
			
			begin
				from = { :address => from } if ( '' == from_alias )
				to = { :address => to } if ( '' == to_alias )
				replyto = { :address => reply_to } if ( '' == reply_to_alias )
				from = { :address => from, :alias => from_alias } unless ( '' == from_alias )
				to = { :address => to, :alias => to_alias } unless ( '' == to_alias )
				replyto = { :address => reply_to, :alias => reply_to_alias } unless ( '' == reply_to_alias )
				
				html_email = Pipeline::Util::EmailFactory::create_html_mail( body )
				Pipeline::Util::Email::send( server, port.to_i, nil, nil, nil, nil,
					from, to, subject, body, 'text/html', replyto )
				
			rescue Exception => ex								
				#Pipeline::Builder::EmailException::send("#{$PROGRAM_NAME}",ex, Config::instance())
				puts "Exception sending email: #{ex.message}"
				puts ex.backtrace.join("\n")
			end
		end
	end
end # Builder module
end # Pipeline module

# report_writer.rb
