#
# File:: report.rb
# Description:: AutoBuild Report generator.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 25 June 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require "source/builder/reporting/report"
require "source/builder/reporting/report_log"
require 'rexml/document'
require 'rexml/formatters/pretty'
include REXML

#-----------------------------------------------------------------------------
# Code
#-----------------------------------------------------------------------------
module Pipeline
module Builder

	#
	# == Description
	# Changelist-triggered build report.
	#
	class ReportChangelist < ReportBase
		include ReportLog
		include ReportXml
	
		attr_reader :skipped		# Did we skip to latest?
		attr_reader :changelist		# Perforce Changelist synced to Hash
		attr_reader :sync_output	# Perforce sync output Hash
		
		def initialize( project, branch, output, skipped = false, changelist = {}, sync_output = [] )
			throw ArgumentError.new( "changelist object must be a Hash (#{changelist.class})" ) \
				unless ( changelist.is_a?( Hash ) )
			throw ArgumentError.new( "sync_output object must be a Array (#{sync_output.class})" ) \
				unless ( sync_output.is_a?( Array ) )
			super( project, branch, output )
			init_log( Pipeline::LogSystem::instance.rootlog, self )
			@skippped = skipped
			@changelist = changelist
			@sync_output = sync_output
		end
		
		def close( )
			super( )
			shutdown_log( )
		end
	
		#
		# Return a REXML::Document object containing the full report.
		#
		def to_xml( serialise_output = false )
			xmldoc = super( serialise_output )
			root = xmldoc.root

			# Changelist Report Customisation
			changelist = root.add_element( 'changelist' )
			changelist.add_attribute( 'id', @changelist['change'] )
			changelist.add_attribute( 'user', @changelist['user'] )
			changelist.add_attribute( 'time', Time.at(@changelist['time'].to_i).to_s ) if ( @changelist.has_key?( 'time' ) )
			changelist.add_attribute( 'client', @changelist['client'] )
			
			desc = changelist.add_element( 'description' )
			desc.text = @changelist['desc']
			
			filelist = changelist.add_element( 'files' )
			@sync_output.each do |filehash|
				felem = filelist.add_element( 'file' )
				felem.add_attribute( 'filename', OS::Path.normalise( filehash['depotFile'] ) )
				felem.add_attribute( 'revision', filehash['rev'] )
				felem.add_attribute( 'action', filehash['action'] )
			end
									
			xmldoc
		end
	end

end # Builder module
end # Pipeline module

# End of builder_report.rb
