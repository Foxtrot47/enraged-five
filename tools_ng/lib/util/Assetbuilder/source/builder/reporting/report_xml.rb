#
# File:: report_xml.rb
# Description:: Define the ReportXml module for base XML report functionality.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 8 April 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/log/log'
require 'rexml/document'
include REXML

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Builder
module Reporting
			
	#
	# == Description
	# The ReportXml module provides some base function implementations for
	# XML report writing.
	#
	# The XML Document is constructed and serialised using the LibXML2 gem.
	#
	module ReportXml

		#
		# Close report XML file.
		#
		def close( )
			ReportXml::log().debug( "Closing XML Document Report." )
		end

		#
		# Save the XML representation of this report to filename.
		#
		def save( filename, serialise_output = false )
			
			FileUtils::mkdir_p( OS::Path::get_directory( filename ) ) \
				unless ( File::directory?( filename ) )
			
			ReportXml::log().debug( "Saving XML Document Report (#{filename})." )
			xmldoc = self.to_xml( serialise_output )
			File.open( filename, 'w' ) do |fp|
				xmldoc.write( fp, 2 )
			end
		end	

		#
		# Return a XML::Document (libxml2) object containing the full report.
		# In general, this needs to be overridden by sub-classes.
		#
		def to_xml( serialise_output = false )
			ReportXml::log().debug( "Constructing XML Document Report (output: #{serialise_output})." )
			
			xmldoc = Document.new
			xmldoc << XMLDecl.new
			xmldoc << Element.new( 'report' )
			
			add_output_to_xml( xmldoc ) if ( serialise_output )
			
			xmldoc.root << title = Element.new( 'project' )
			title.add_attribute( 'project', @project.uiname ) if ( defined? @project and @project.is_a?( Pipeline::Project ) )
			title.add_attribute( 'branch', @branch ) if ( defined? @branch and @branch.is_a?( String ) )
			
			xmldoc.root << additionalelem = Element.new( 'additional' )
			@additional.each do |info|
				additionalelem << infoelem = Element.new( 'info' )
				infoelem['text'] = info
			end
			
			# Summary Information
			xmldoc.root << summary = Element.new( 'summary' )
			summary.add_attribute( 'errors', @error_count.to_s )
			summary.add_attribute( 'warnings', @warning_count.to_s )
			
			# Errors List
			xmldoc.root << errors = Element.new( 'errors' )
			@errors.each_pair do |proj, errs|
				errs.each do |err|
					errors << err.to_xml( 'error' )
				end
			end
			# Warnings List
			xmldoc.root << warnings = Element.new( 'warnings' )
			@warnings.each_pair do |proj, errs|
				errs.each do |err|
					warnings << err.to_xml( 'warning' )
				end
			end
			# Info Message List
			xmldoc.root << infos = Element.new( 'infos' )
			@infos.each_pair do |proj, info|
				info.each do |info|
					infos <<  info.to_xml( 'info' )
				end
			end
			
			xmldoc
		end
	
		#--------------------------------------------------------------------
		# Class Methods
		#--------------------------------------------------------------------		
		
		def ReportXml::log( )
			@@log = Log.new( 'report_xml' ) if ( @@log.nil? )
			@@log
		end

		#--------------------------------------------------------------------
		# Protected Methods
		#--------------------------------------------------------------------
	protected
		#
		# Serialise each output line into the xmldoc passed in
		#		
		def add_output_to_xml( xmldoc )
			return if ( @output.nil? or 0 == @output.size )
			throw ArgumentError.new( "Invalid XML::Document object (#{xmldoc.class})" ) \
				unless ( xmldoc.is_a?( XML::Document ) )
			
			ReportXml::log().debug( 'Constructing output XML to add to root document.' )
			xmldoc.root << output = XML::Node.new( 'output' )
			@output.each do |line|
				if (line =~ /\S/ ) then
					output << element = XML::Node.new( 'line' )
					element['text'] = line
				end
			end											
		end
		
		#--------------------------------------------------------------------
		# Private
		#--------------------------------------------------------------------
	private
		@@log = nil
	end
			
end # Reporting module
end # Builder module
end # Pipeline module

# report_xml.rb
