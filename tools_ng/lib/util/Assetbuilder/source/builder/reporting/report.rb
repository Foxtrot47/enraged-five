#
# File:: report.rb
# Description:: AutoBuild Report base classes and common modules.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 25 June 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
# None

#-----------------------------------------------------------------------------
# Code
#-----------------------------------------------------------------------------
module Pipeline
module Builder

	#
	# == Description
	# A ReportMessage object are contained in an IReport object's errors and
	# warnings arrays.
	#
	class ReportMessage			
		attr_reader  :lineno, :errorcode, :message, :filename, :context
		
		def initialize( message, filename = '', line = -1, errorcode = '', context = '' )
			@message = message
			@filename = filename
			@lineno = line
			@errorcode = errorcode
			@context = context
		end
			
		def to_xml( name = 'message' )
			
			elem = Element.new( name )
			elem.add_attribute( 'message', @message )
			elem.add_attribute( 'filename', @filename ) unless ( @filename == '' )
			elem.add_attribute( 'lineno', @lineno.to_s ) unless ( @lineno.nil? )
			elem.add_attribute( 'errorcode', @errorcode ) unless ( @errorcode == '' )
			elem.add_attribute( 'context', @context ) unless ( @context == '' )
                        
			elem
		end
	end

	#
	# == Description
	# Abstract builder report class.
	#
	class ReportBase
		attr_reader :project		# Project (Pipeline::Project object)
		attr_reader :branch			# Project branch (String)
		attr_reader :output			# typically the tty output.
		attr_reader :additional		# Additional information to be reported (e.g. resolved or reverted files).
		
		# Log report information.
		attr_reader :errors			# Hash of Array of error ReportMessage objects
		attr_reader :error_count	# Number of errors
		attr_reader :warnings		# Hash of Array of warnings ReportMessage objects
		attr_reader :warning_count	# Number of warnigns
		attr_reader :infos			# Hash of Array of info ReportMessage objects
		
		def initialize( project, branch, output = nil )
			throw ArgumentError.new( "project object must be a Pipeline::Project object (#{project.class})" ) \
			unless ( Pipeline::Project == project.class )
			
			@project = project
			@branch = branch
			@output = output
			@additional = []
			@errors = {}
			@error_count = 0
			@warnings = {}
			@warning_count = 0
			@infos = {}		
		end
		
		#
		# Manually add an error message.
		#
		def add_error( error, proj = 'root' )
			throw ArgumentError.new( "Invalid error message object (#{error.class})" ) \
			unless ( ReportMessage == error.class )
			
			@errors[proj] = [] unless @errors.has_key?( proj )
			@errors[proj] << error
			@error_count += 1
		end
		
		#
		# Manually add a warning message.
		#
		def add_warning( warning, proj = 'root' )
			throw ArgumentError.new( "Invalid warning message object (#{warning.class})" ) \
			unless ( ReportMessage == warning.class )
			
			@warnings[proj] = [] unless @warnings.has_key?( proj )
			@warnings[proj] << warning
			@warning_count += 1
		end
		
		#
		# Manually add an info message.
		#
		def add_info( info, proj = 'root' )
			throw ArgumentError.new( "Invalid info message object (#{info.class})" ) \
			unless ( ReportMessage == info.class )
			
			@infos[proj] = [] unless @infos.has_key?( proj )
			@infos[proj] << info
		end
		
		#---------------------------------------------------------------------
		# Report class registration
		#---------------------------------------------------------------------		
		def self.inherited( child )
			@registered_reports = [] if ( ( not defined? @registered_reports ) or @registered_reports.nil? )
			ReportBase.registered_reports << child
		end
		
		class << self
			attr_reader :registered_reports
		end		
	end
	
	#
	# == Description
	# The ReportXml module provides some base function implementations for
	# XML report writing.
	#
	module ReportXml
		
		#
		# Close report.
		#
		def close( )
		end

		#
		# Save the XML representation of this report to filename.
		#
		def save( filename, serialise_output = false )
			
			FileUtils::mkdir_p( OS::Path::get_directory( filename ) ) \
			unless ( File::directory?( filename ) )
			
			xmldoc = self.to_xml( serialise_output )
			File.open( filename, 'w' ) do |fp|
				xmldoc.write( fp, 2 )
			end
		end		
		
		#
		# Return a REXML::Document object containing the full report.  This needs to
		# be implemented by sub-classes.
		#
		def to_xml( serialise_output = false )		
			xmldoc = Document.new
			xmldoc << XMLDecl.new
			root = xmldoc.add_element( 'report' )
			
			add_output_to_xml( xmldoc ) if ( serialise_output )
			
			title = root.add_element( 'project' )
			title.add_attribute( 'project', @project.uiname )
			title.add_attribute( 'branch', @branch ) 
			
			additionalelem = root.add_element( 'additional' )
			@additional.each do |info|
				infoelem = additionalelem.add_element( 'info' )
				infoelem.add_attribute( 'text', info )
			end
			
			# Summary Information
			summary = root.add_element( 'summary' )
			summary.add_attribute( 'errors', @error_count.to_s )
			summary.add_attribute( 'warnings', @warning_count.to_s )
			errors = root.add_element( 'errors' )
			warnings = root.add_element( 'warnings' )
			infos = root.add_element( 'infos' )
			
			# Errors List
			@errors.each_pair do |proj, errs|
				errs.each do |err|
					errors.add_element( err.to_xml( 'error' ) )
				end
			end
			# Warnings List
			@warnings.each_pair do |proj, errs|
				errs.each do |err|
					warnings.add_element( err.to_xml( 'warning' ) )
				end
			end
			# Info Message List
			@infos.each_pair do |proj, info|
				info.each do |info|
					infos.add_element( info.to_xml( 'info' ) )
				end
			end
	
			xmldoc
		end
		
		#--------------------------------------------------------------------
		# Protected Methods
		#--------------------------------------------------------------------
	protected
		#
		# Serialise each output line into the xmldoc passed in
		#		
		def add_output_to_xml( xmldoc )
			return if ( @output.nil? or 0 == @output.size )
      throw ArgumentError.new( "Invalid REXML object (#{xmldoc.class})" ) \
        unless ( xmldoc.is_a?(REXML::Document) )
			
			root = xmldoc.elements['report'] 
			output = root.add_element( 'output' )
			@output.each do |line|
				if (line =~ /\S/ ) then
					element = output.add_element( 'line' )
					element.add_attribute( 'text', line )								
				end
			end											
		end
	end
	
end # Builder module
end # Pipeline module

# report.rb
