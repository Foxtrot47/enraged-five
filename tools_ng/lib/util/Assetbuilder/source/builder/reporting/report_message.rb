#
# File:: pipeline/builder/reporting/report_message.rb
# Description:: 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 8 April 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'rexml/document'
include REXML

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Builder
module Reporting
			
	#
	# == Description
	# A ReportMessage object is contained in a ReportBase object's errors and
	# warnings containers.  These object represent individual report messages
	# from a builder application.
	#
	class ReportMessage
		attr_reader :lineno
		attr_reader :errorcode
		attr_reader :message
		attr_reader :filename
		attr_reader :context
		
		#
		#
		#
		def initialize( message, filename = '', line = -1, errorcode = '', context = '' )
			@message = message
			@filename = filename
			@lineno = line
			@errorcode = errorcode
			@context = context
		end
		
		#
		#
		#
		def to_xml( name = 'message' )
		
			elem = Element.new( name )
			elem.add_attribute( 'message', @message )
			elem.add_attribute( 'filename', @filename ) unless ( @filename == '' )
			elem.add_attribute( 'lineno', @lineno.to_s ) unless ( @lineno.nil? )
			elem.add_attribute( 'errorcode', @errorcode ) unless ( @errorcode == '' )
			elem.add_attribute( 'context', @context ) unless ( @context == '' )
			elem
		end
	end
			
end # Reporting module
end # Builder module
end # Pipeline module

# pipeline/builder/reporting/report_message.rb
