#
# File:: Email_Exception.rb
# Description:: Handy mailer util to email maintainers upon expectional program behaviour.
#
# Author(s):: Derek Ward <derek.ward@rockstarnorth.com> 
# Date:: 14 May 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

module Pipeline
module Builder
	#
	# == Description
	# EmailException - emails maintainers details of program difficulties or exceptional behaviour.
	# eg.
	#
	#	begin
	#	    throw tungsten carbide drill bit
	#   rescue Exception => ex	
	#		Pipeline::Builder::EmailException::send("#{$PROGRAM_NAME}",ex, @config)
	#	end
	#
	class EmailException
		
		EMAIL_OPTIONS = {
			:server => Pipeline::Config::instance().mailserver,
			:port => Pipeline::Config::instance().mailport.to_i,
			:username => nil,
			:password => nil,
			:domain => Pipeline::Config::instance().maildomain,
			:auth_mode => :plain
		}
		
		def EmailException::send( subject, ex, config )
			msg = "#{Time.now.to_s}\n\n"
			msg += " INSPECT\n -------\n    #{ex.inspect} \n\n BACKTRACE\n ---------\n"
			ex.backtrace.each  { |m| msg += "    #{m}\n" }
			@email = Pipeline::Util::Email.new( EMAIL_OPTIONS )
			
			email_key_value = config.report.email.addresses[:maintainer]
			
			@email.send( { :address => 'exceptionEmailer@rockstarnorth.com', :alias => "#{$PROGRAM_NAME} Exception" },
				{ :address => email_key_value, :alias => 'Maintainers' },
				"#{subject} | #{ex.backtrace[0]} | #{ex.inspect}", msg )
		end	
	end		
end # Builder module
end # Pipeline module
