#
# File:: report.rb
# Description:: AutoBuild Report generator.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 25 June 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require "source/builder/reporting/report"
require "source/builder/reporting/report_log"
require 'rexml/document'
require 'rexml/formatters/pretty'
include REXML

#-----------------------------------------------------------------------------
# Code
#-----------------------------------------------------------------------------
module Pipeline
module Builder

	#
	# == Description
	# Command output report generator/parser abstract class containing some
	# useful utilities and common code.
	#
	class ReportRebuild < ReportBase
		include ReportLog
		include ReportXml
		
		attr_reader :message
		
		def initialize( project, branch, output, message, files = [] )
			throw ArgumentError.new( "message must be a String object (#{message.class})" ) \
				unless ( message.is_a?( String ) )
			throw ArgumentError.new( "files must be an Array object (#{files.class})" ) \
				unless ( files.is_a?( Array ) )
			super( project, branch, output )
			init_log( Pipeline::LogSystem::instance.rootlog, self )
			@message = message
			@files = files
		end
		
		def close( )
			super( )
			shutdown_log( )
		end
		
		#
		# Return a REXML::Document object containing the full report.
		#
		def to_xml( serialise_output = false )
			xmldoc = super( serialise_output )
			root = xmldoc.root
			
			# Rebuild Report Customisation
			rebuild = root.add_element( 'rebuild' )
			rebuild.add_attribute( 'message', @message )
			fileselem = rebuild.add_element( 'files' )
			@files.each do |filename|
				fileelem = fileselem.add_element( 'file' )
				fileelem.add_attribute( 'filename', filename )
			end
			
			xmldoc
		end
	end

end # Builder module
end # Pipeline module

# End of builder_report.rb
