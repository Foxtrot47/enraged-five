#
# File:: report_ragebuilder.rb
# Description:: Ragebuilder report parser.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 4 December 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require "source/builder/reporting/report_changelist"
require "source/builder/reporting/report_rebuild"
require "pipeline/util/rexml_write_fix"
include Pipeline

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module AssetBuild

=begin	

# Kept around for posterity, at least for now... since cannot easily put in place the same functionality with an external parser..

	#
	# == Description
	# Ragebuilder output report parser functionality.  This module exposes
	# Ragebuilder error and warning parsing functions to the Changelist and 
	# Rebuild report classes defined below.
	#
	module RagebuilderParser		
		def parse_internal( text )
			return if ( text.nil? or ( 0 == text.size ) )
			
			curr_proj = ''
			filename = ''
			@errors[curr_proj] = [] unless ( @errors.has_key?( curr_proj ) )
			@warnings[curr_proj] = [] unless ( @warnings.has_key?( curr_proj ) )
			context = ''
			text.each do |line|
				
				if ( line =~ REGEXP_PLATFORM )
					context = $1.strip
				
				elsif ( line =~ REGEXP_FILENAME )
					filename = $1
				
				elsif ( line =~ REGEXP_ERROR )
					@errors[curr_proj] << Builder::ReportMessage.new( $1, filename, -1, '', context )
					@error_count += 1
										
				elsif ( line =~ REGEXP_WARNING )
					@warnings[curr_proj] << Builder::ReportMessage.new( $1, filename, -1, '', context )
					@warning_count += 1
				end
			end
		end
		
		#---------------------------------------------------------------------
		# Private Constants
		#---------------------------------------------------------------------
	private
		REGEXP_ERROR	= /Error: (.*)/i
		REGEXP_WARNING	= /Warning: (.*)/i
		REGEXP_PLATFORM	= /setting platform (.*)/i
		REGEXP_FILENAME = /converting ([A-Za-z0-9\.\\\/\:_]+)/
	end
=end
		
	#
	# == Description
	# Rebuild report capable of parsing Ragebuilder output.
	#
	class ReportRebuildRagebuilder < Builder::ReportRebuild
		
		def initialize( project, branch, message, files = [] )
			super( project, branch, nil, message, files )
		end
		
		def set_output_errors_warnings( output, errors, warnings )
			@output			= output
			
			curr_proj = ''
			filename = ''
			@errors[curr_proj] = [] unless ( @errors.has_key?( curr_proj ) )
			@warnings[curr_proj] = [] unless ( @warnings.has_key?( curr_proj ) )
			context = ''
			
			errors.each do |line|			
				@errors[curr_proj]  << Builder::ReportMessage.new( line, filename, -1, '', context )
			end
			
			warnings.each do |line|			
				@warnings[curr_proj]  << Builder::ReportMessage.new( line, filename, -1, '', context )
			end

			@warning_count  = warnings 	? @warnings[curr_proj].length : 0 
			@error_count  	= errors 	? @errors[curr_proj].length : 0 
		end		
				
		def save( filename, serialise_output = false, url = nil )
			
			FileUtils::mkdir_p( OS::Path::get_directory( filename ) ) \
			unless ( File::directory?( filename ) )
			
			xmldoc = self.to_xml( serialise_output )
			xmldoc.root.attributes['url'] = url unless ( url.nil? )
			File.open( filename, 'w' ) do |fp|
				xmldoc.write( fp, 2 )
			end
		end
	end
	
end # CodeBuild module

# End of report_incredibuild.rb
