#
# File:: report.rb
# Description:: 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 8 April 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require "source/builder/reporting/report_message"
require 'pipeline/log/log'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Builder
module Reporting

	#
	# == Description
	# Abstract builder report class that does not contain project and branch
	# information.
	#
	class ReportBaseSimple
		
		attr_reader :output			# typically the tty output.
		attr_reader :additional		# Additional information to be reported (e.g. resolved or reverted files).
		
		# Log report information.
		attr_reader :errors			# Hash of Array of error ReportMessage objects
		attr_reader :error_count	# Number of errors
		attr_reader :warnings		# Hash of Array of warnings ReportMessage objects
		attr_reader :warning_count	# Number of warnigns
		attr_reader :infos			# Hash of Array of info ReportMessage objects
		
		def initialize( output = nil )
			@output = output
			@additional = []
			@errors = {}
			@error_count = 0
			@warnings = {}
			@warning_count = 0
			@infos = {}		
		end
		
		#
		# Manually add an error message.
		#
		def add_error( error, proj = 'root' )
			throw ArgumentError.new( "Invalid error message object (#{error.class})" ) \
			unless ( ReportMessage == error.class )
			
			@errors[proj] = [] unless @errors.has_key?( proj )
			@errors[proj] << error
			@error_count += 1
		end
		
		#
		# Manually add a warning message.
		#
		def add_warning( warning, proj = 'root' )
			throw ArgumentError.new( "Invalid warning message object (#{warning.class})" ) \
			unless ( ReportMessage == warning.class )
			
			@warnings[proj] = [] unless @warnings.has_key?( proj )
			@warnings[proj] << warning
			@warning_count += 1
		end
		
		#
		# Manually add an info message.
		#
		def add_info( info, proj = 'root' )
			throw ArgumentError.new( "Invalid info message object (#{info.class})" ) \
			unless ( ReportMessage == info.class )
			
			@infos[proj] = [] unless @infos.has_key?( proj )
			@infos[proj] << info
		end
		
		#---------------------------------------------------------------------
		# Report class registration
		#---------------------------------------------------------------------		
		def self.inherited( child )
			@registered_reports = [] if ( ( not defined? @registered_reports ) or @registered_reports.nil? )
			ReportBaseSimple.registered_reports << child
		end
		
		class << self
			attr_reader :registered_reports
		end	
	end
	
	#
	# == Description
	# Abstract builder report class that expands on ReportBaseSimple to include
	# project and branch information.
	#
	class ReportBase < ReportBaseSimple
		attr_reader :project		# Project (Pipeline::Project object)
		attr_reader :branch			# Project branch (String)
				
		def initialize( project, branch, output = nil )
			throw ArgumentError.new( "project object must be a Pipeline::Project object (#{project.class})" ) \
				unless ( project.is_a?( Pipeline::Project ) )
			super( output )
			@project = project
			@branch = branch
		end
	end

end # Reporting module
end # Builder module
end # Pipeline module

# report.rb
