#
# File:: report_log.rb
# Description:: Log listener class inherited from IReport.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 29 July 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/log/log'
gem 'log4r'

module Pipeline
module Builder

	#
	# == Description
	# This report module mixin is useful for capturing our log system messages
	# to the ReportBase errors, warnings and infos arrays.
	#
	module ReportLog
		#attr_reader :listen_log
		attr_reader :report
	
		def init_log( log, report )
			#@listen_log = log
			@report = report
			#install_listener( )
		end
		
		def shutdown_log( )
			#uninstall_listener( )
		end
		
		#---------------------------------------------------------------------
		# Private Methods
		#---------------------------------------------------------------------
	private		
		def install_listener()
			@listener = ReportLogListener.new( LISTENER_NAME, @report )
			@listen_log.add( @listener )
		end
		
		def uninstall_listener()
			@listen_log.remove( @listener )
		end
		
		#---------------------------------------------------------------------
		# Private Constants
		#---------------------------------------------------------------------
	private
		LISTENER_NAME = 'report_log_listener'
	end

	#
	# ReportLog log system listener.  Is installed as an outputter to the root
	# log by the ReportLog module.
	#
	class ReportLogListener < Log4r::Outputter
		def initialize( _name, report, hash={} )
			super( _name, hash )
			@report = report
		end

		# Log event handler, here we simply add data into our report.
		def canonical_log( event )
			synch do
				case Log4r::LNAMES[event.level]
					when 'Fatal', 'Error'
						@report.add_error( event_to_reportmessage( event ) )
					when 'Warn'
						@report.add_warning( event_to_reportmessage( event ) )
					when 'Debug', 'Info'
						@report.add_info( event_to_reportmessage( event ) )
				end
			end
		end

		#-----------------------------------------------------------------
		# Private Methods
		#-----------------------------------------------------------------
	private

		def parse_trace_info( event )

			trace_info = {}
			matches = /^(.*)\:([0-9]+):in .([A-Za-z0-9_]+)./.match( event.tracer[0] )
			trace_info[:filename] = matches[1]
			trace_info[:lineno] = matches[2]
			trace_info[:function] = matches[3]

			# Return our hash with trace information
			trace_info
		end	

		def event_to_reportmessage( event )

			if ( not event.tracer.nil? ) then
				ti = parse_trace_info( event )

				return ( ReportMessage.new( event.data, ti[:filename], ti[:lineno] ) )
			else
				return ( ReportMessage.new( event.data ) )
			end
		end
	end

end # Builder module
end # Pipeline module

# report_log.rb
