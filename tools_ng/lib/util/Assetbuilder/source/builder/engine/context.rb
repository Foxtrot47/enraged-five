#
# File:: engine_context.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 24 March 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/branch'
require 'pipeline/config/targets'
require 'pipeline/util/environment'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------

module Pipeline
module Builder
module Engine

	#
	# == Description
	# The Context module is a common container for some Builder Engine
	# specific data.  Rather than have to duplicate project, branch, target
	# and the context Symbol objects within each builder Engine class just
	# include this module and initialise it.
	#
	# The EngineContext module automatically initialises and maintains
	# an environment (@env) for you to use in your engine also.
	#
	# === Example Usage
	#
	#    class Engine < EngineBase
	#	    include EngineContext
	#
	#       def initialize( p, b, t )
	#		   init_context( :live, p, b, t )
	#		end
	#
	#		def cb_on_context_change( )
	#			puts "CONTEXT HAS CHANGED"
	#		end
	#    end
	#    
	module Context

		attr_reader :dev_context
		attr_reader :project
		attr_reader :branch
		attr_reader :target
		attr_reader :env

		def init_context( context, project, branch, target = nil )
			throw ArgumentError.new( "Invalid context Symbol (#{context.class})." ) \
				unless ( context.is_a?( Symbol ) )
			throw ArgumentError.new( "Invalid project object (#{project.class})." ) \
				unless ( project.is_a?( Pipeline::Project ) )
			throw ArgumentError.new( "Invalid branch object (#{branch.class})." ) \
				unless ( branch.is_a?( Pipeline::Branch ) )
			throw ArgumentError.new( "Invalid target object (#{target.class})." ) \
				unless ( target.nil? or target.is_a?( Pipeline::TargetBase ) )

			@env = Environment.new
			@project = project
			@branch = branch
			@target = target
			@dev_context = context
			update_env( )
		end

		#
		# Update the current development context.
		#
		# === Example Usage
		#   set_context( :development )
		#
		#   set_context( :development ) do |old_ctx, new_ctx|
		#		# User-code block here
		#   end
		#
		def set_context( context, &block )
			throw ArgumentError.new( "Invalid context Symbol (#{context.class})." ) \
				unless ( context.is_a?( Symbol ) )
			
			old = @dev_context
			@dev_context = context
			update_env( )

			yield( old, @dev_context ) if ( block_given? )
		end

	private
		def update_env( )
			@env.clear( )
			@target.fill_env( @env ) unless ( @target.nil? )
			@branch.fill_env( @env ) if ( @target.nil? )
			@env.add( 'dev_context', @dev_context.to_s )
		end
	end

end # Engine module
end # Builder module
end # Pipeline module

# context.rb
