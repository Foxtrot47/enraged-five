#
# File:: scheduler.rb
# Description:: Builder Framework Engine Scheduler Component.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 26 March 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'rufus-scheduler'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
module Pipeline
module Builder
module Engine

	#
	# == Description
	# The Scheduler module is a common feature for the builder framework.
	# It provides the included class with a Rufus-Scheduler object.
	#
	# === Example Usage
	# DHM TODO
	#
	module Scheduler

		#
		# Initialise the scheduler.
		#
		def init_scheduler( )
			@scheduled_jobs = []
			@scheduler = Rufus::Scheduler::PlainScheduler.start_new( :thread_name => '__Engine_Scheduler' )
		end

		#
		# Print job information to STDOUT.		
		#
		def print_scheduler_jobs( )
			puts "Scheduled Jobs:"
			@scheduled_jobs.each_with_index do |job, index|
				if ( job.is_a?( Rufus::Scheduler::CronJob ) ) then
					puts "\t#{index} CRON #{job.cron_line.original}"
					puts "\t\tLast: #{job.last}"
					puts "\t\tNext: #{job.cron_line.next_time}"
				elsif ( job.is_a?( Rufus::Scheduler::Job ) ) then
					puts "\t#{index} #{job.t}"
					puts "\t\tLast: #{job.last}"
					puts "\t\tNext: #{job.cron_line.next_time}"
				end
			end		
		end
	end

end # Engine module
end # Builder module
end # Pipeline module

# scheduler.rb
