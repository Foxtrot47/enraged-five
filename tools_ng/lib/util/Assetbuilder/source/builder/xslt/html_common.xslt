<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--
    File:: html_common.xslt
    Description:: Common HTML XSLT markup templates.
    
    Author:: David Muir <david.muir@rockstarnorth.com>
    Author:: Derek Ward <derek.ward@rockstarnorth.com>
    Date:: 3 December 2008
    
    In your custom builder application XSLT report templates use the
    XSLT xsl:include or xsl:import tags to incorporate this into
    your stylesheet.
    
    E.g.
      <xsl:include href="pipeline/builder/html_common.xslt" />
      <xsl:import href="pipeline/builder/html_common.xslt" />
  -->

  <!--
    Template: report_header
    Description: Print HTML page header information.
  -->
  <xsl:template name="report_header">
    <xsl:param name="title" />
    <xsl:param name="project" />
    <xsl:param name="branch" />
    <head>
      <title>
        <xsl:choose>
          <xsl:when test="$branch">
            <xsl:value-of select="$title" />: <xsl:value-of select="$project"/> - <xsl:value-of select="$branch"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$title" />: <xsl:value-of select="$project"/>
          </xsl:otherwise>
        </xsl:choose>
      </title>
      <meta HTTP-EQUIV="pragma" CONTENT="no-cache" />
      <link REL="Stylesheet" HREF="http://rsgedibug1/toolstats/css/global.css" TYPE="text/css" MEDIA="screen" />
    </head>
  </xsl:template>

  <!--
    Template: report_title
    Description: 
  -->
  <xsl:template name="report_title">
    <xsl:param name="title" />
    <xsl:param name="project" />
    <xsl:param name="branch" />
    <h3>
      <xsl:choose>
        <xsl:when test="$branch">
          <xsl:value-of select="$title" />: <xsl:value-of select="$project"/> - <xsl:value-of select="$branch"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$title" />: <xsl:value-of select="$project"/>
        </xsl:otherwise>
      </xsl:choose>
    </h3>
  </xsl:template>

  <!--
    Template: changelist_summary
    Description: Print a table summary of the Perforce Changelist.
  -->
  <xsl:template name="changelist_summary">
    <xsl:param name="changelist" />
    <table>
      <tr>
        <th>ID</th>
        <td class="borderless">
          <a>
            <xsl:attribute name="href">
              http://10.11.16.17:8080/@md=d&amp;cd=/&amp;c=7FM@/<xsl:value-of select="$changelist/@id" />?ac=10
            </xsl:attribute>
            <xsl:value-of select="$changelist/@id"/>
          </a>          
        </td>
      </tr>
      <tr>
        <th>Time</th>
        <td class="borderless">
          <xsl:value-of select="$changelist/@time"/>
        </td>
      </tr>
      <tr>
        <th>User</th>
        <td class="borderless">
          <xsl:value-of select="$changelist/@user"/>
        </td>
      </tr>
      <tr>
        <th>Description</th>
        <td class="borderless">
          <xsl:value-of select="$changelist/description"/>
        </td>
      </tr>
    </table>
  </xsl:template>


  <!--
    Template: changelist_summary
    Description: Print a one line table summary of the Perforce Changelist.
  -->
  <xsl:template name="changelist_summary_one_liner">
    <xsl:param name="changelist" />
    <table>
      <tr>
        <th>ID</th>
        <td>
          <a>
            <xsl:attribute name="href">
              http://10.11.16.17:8080/@md=d&amp;cd=/&amp;c=7FM@/<xsl:value-of select="$changelist/@id" />?ac=10
            </xsl:attribute>
            <xsl:value-of select="$changelist/@id"/>
          </a>
        </td>
        <th>User</th>
        <td>
          <xsl:value-of select="$changelist/@user"/>
        </td>
        <th>Description</th>
        <td>
          <xsl:value-of select="$changelist/description"/>
        </td>
      </tr>
    </table>
  </xsl:template>
   
  <!--
    Template: changelist_files
    Description: Print a table list of the Perforce Changelist files.
  -->
  <xsl:template name="changelist_files">
    <xsl:param name="file_count">5</xsl:param>
    <xsl:param name="file_list" />
    <xsl:for-each select="$file_list">
      <table>
        <tr>
          <th>Filename</th>
          <th>Revision</th>
          <th>Action</th>
        </tr>
        <xsl:choose>
          <xsl:when test="count(file) &gt; $file_count">
            <!-- Only do first $file_count files -->
            <xsl:for-each select="file[position() &lt; ($file_count + 1)]">
              <tr>
                <td class="borderless">
                  <a>
                    <xsl:attribute name="href">
                      http://10.11.16.17:8080/@md=d&amp;cd=/&amp;c=0lc@<xsl:value-of select="@filename" />?ac=22
                    </xsl:attribute>
                    <xsl:value-of select="@filename" />
                  </a>
                </td>
                <td class="borderless">
                  <xsl:value-of select="@revision"/>
                </td>
                <td class="borderless">
                  <xsl:value-of select="@action"/>
                </td>
              </tr>
            </xsl:for-each>
            <tr>
              <td class="borderless">...</td>
              <td class="borderless">...</td>
              <td class="borderless">...</td>
            </tr>
          </xsl:when>
          <xsl:otherwise>
            <!-- Include all files since we have less than $file_count -->
            <xsl:for-each select="file">
              <tr>
                <td class="borderless">
                  <a>
                    <xsl:attribute name="href">
                      http://10.11.16.17:8080/@md=d&amp;cd=/&amp;c=0lc@<xsl:value-of select="@filename" />?ac=22
                    </xsl:attribute>
                    <xsl:value-of select="@filename" />
                  </a>
                </td>
                <td class="borderless">
                  <xsl:value-of select="@revision"/>
                </td>
                <td class="borderless">
                  <xsl:value-of select="@action"/>
                </td>
              </tr>
            </xsl:for-each>
          </xsl:otherwise>
        </xsl:choose>
      </table>
    </xsl:for-each>
  </xsl:template>

  <!--
    Template: generic_filelist
    Description: Print a table list of files.
  -->
  <xsl:template name="generic_filelist">
    <xsl:param name="file_count">5</xsl:param>
    <xsl:param name="file_list" />
    <xsl:for-each select="$file_list">
      <table>
        <tr>
          <th>Filename</th>
        </tr>
        <xsl:choose>
          <xsl:when test="count(file) &gt; $file_count">
            <!-- Only do first $file_count files -->
            <xsl:for-each select="file[position() &lt; ($file_count + 1)]">
              <tr>
                <td class="borderless">
                  <xsl:value-of select="@filename"/>
                </td>
              </tr>
            </xsl:for-each>
            <tr>
              <td class="borderless">...</td>
            </tr>
          </xsl:when>
          <xsl:otherwise>
            <!-- Include all files since we have less than $file_count -->
            <xsl:for-each select="file">
              <tr>
                <td class="borderless">
                  <xsl:value-of select="@filename"/>
                </td>
              </tr>
            </xsl:for-each>
          </xsl:otherwise>
        </xsl:choose>
      </table>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
