<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--
    File:: html_messages.xslt
    Description:: Builder Message HTML XSLT markup templates.
    
    Author:: David Muir <david.muir@rockstarnorth.com>
    Author:: Derek Ward <derek.ward@rockstarnorth.com>
    Date:: 19 December 2008
    
    In your custom builder application XSLT report templates use the
    XSLT xsl:include or xsl:import tags to incorporate this into
    your stylesheet.
    
    E.g.
      <xsl:include href="x:/pipedev/tools/pipeline/builder/html_messages.xslt" />
      <xsl:import href="x:/pipedev/tools/pipeline/builder/html_messages.xslt" />
  -->

  <!--
    Template: report_errors
    Description: Print HTML table of error messages.
  -->
  <xsl:template name="report_errors">
    <xsl:param name="error_list" />
    <xsl:choose>
      <xsl:when test="count( $error_list/error ) = 0">
        <p>No errors reported.</p>
      </xsl:when>
      <xsl:otherwise>
        <table>
          <tr>
            <th>Message</th>
            <th>Filename</th>
            <th>Line</th>
          </tr>
          <xsl:for-each select="$error_list/error">
            <tr>
              <td>
                <xsl:value-of select="@message" />
              </td>
              <td>
                <xsl:value-of select="@filename" />
              </td>
              <td>
                <xsl:value-of select="@lineno" />
              </td>
            </tr>
          </xsl:for-each>
        </table>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--
    Template: report_warnings
    Description: Print HTML table of warning messages.
  -->
  <xsl:template name="report_warnings">
    <xsl:param name="warnings_list" />
    <xsl:choose>
      <xsl:when test="count( $warnings_list/warning ) = 0">
        <p>No warnings reported.</p>
      </xsl:when>
      <xsl:otherwise>
        <table>
          <tr>
            <th>Message</th>
            <th>Filename</th>
            <th>Line</th>
          </tr>
          <xsl:for-each select="$warnings_list/warning">
            <tr>
              <td>
                <xsl:value-of select="@message" />
              </td>
              <td>
                <xsl:value-of select="@filename" />
              </td>
              <td>
                <xsl:value-of select="@lineno" />
              </td>
            </tr>
          </xsl:for-each>
        </table>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--
    Template: report_info
    Description: Print HTML table of info messages.
  -->
  <xsl:template name="report_info">
    <xsl:param name="info_list" />
    <xsl:choose>
      <xsl:when test="count( $info_list/info ) = 0">
      </xsl:when>
      <xsl:otherwise>
        <table>
          <tr>
            <th>Message</th>
            <th>Filename</th>
            <th>Line</th>
          </tr>
          <xsl:for-each select="$info_list/info">
            <tr>
              <td>
                <xsl:value-of select="@message" />
              </td>
              <td>
                <xsl:value-of select="@filename" />
              </td>
              <td>
                <xsl:value-of select="@lineno" />
              </td>
            </tr>
          </xsl:for-each>
        </table>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
