<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes"/>
  <xsl:include href="file://x:/gta5/tools/lib/util/assetbuilder/source/builder/xslt/html_common.xslt" />
  <xsl:include href="file://x:/gta5/tools/lib/util/assetbuilder/source/builder/xslt/html_messages.xslt" />
  <xsl:template match="/">

    <html>
      <xsl:call-template name="report_header">
        <xsl:with-param name="title">Asset Build Report</xsl:with-param>
        <xsl:with-param name="project" select="report/project/@project" />
        <xsl:with-param name="branch" select="report/project/@branch" />
      </xsl:call-template>

      <body>
        <xsl:call-template name="report_title">
          <xsl:with-param name="title">Asset Build Report</xsl:with-param>
          <xsl:with-param name="project" select="report/project/@project" />
          <xsl:with-param name="branch" select="report/project/@branch" />
        </xsl:call-template>

        <a>
          <xsl:attribute name="href">
            <xsl:value-of select="/report/@url"/>
          </xsl:attribute>
          Full Build Report
        </a>

        <!-- Changelist Report -->
        <xsl:if test="count(report/changelist) &gt; 0">
          <h3>Changelist</h3>
          <xsl:call-template name="changelist_summary">
            <xsl:with-param name="changelist" select="report/changelist" />
          </xsl:call-template>

          <h3>Files</h3>
          <xsl:call-template name="changelist_files">
            <xsl:with-param name="file_count">5</xsl:with-param>
            <xsl:with-param name="file_list" select="report/changelist/files" />
          </xsl:call-template>
        </xsl:if>

        <!-- Rebuild Report -->
        <xsl:if test="count(report/changelist) = 0">
          <h3>Description</h3>
          <p>
            <xsl:value-of select="report/rebuild/@message" />
          </p>
          
          <h3>Files</h3>
          <xsl:call-template name="generic_filelist">
            <xsl:with-param name="file_list" select="report/rebuild/files" />
          </xsl:call-template>
        </xsl:if>

        <h3>Errors</h3>
        <xsl:call-template name="report_errors">
          <xsl:with-param name="error_list" select="report/errors" />
        </xsl:call-template>
      </body>
    </html>

  </xsl:template>
</xsl:stylesheet>
