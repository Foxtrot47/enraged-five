<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes"/>
  <xsl:include href="file://x:/gta5/tools/lib/util/assetbuilder/source/builder/xslt/html_common.xslt" />
  <xsl:include href="file://x:/gta5/tools/lib/util/assetbuilder/source/builder/xslt/html_messages.xslt" />
  <xsl:template match="/">

    <html>
      <xsl:call-template name="report_header">
        <xsl:with-param name="title">Asset Build Report</xsl:with-param>
        <xsl:with-param name="project" select="report/project/@project" />
        <xsl:with-param name="branch" select="report/project/@branch" />
      </xsl:call-template>

      <body>
	<img src="http://rsgedibug1/toolstats/images/rockstar.png" />
        <xsl:call-template name="report_title">
          <xsl:with-param name="title">Asset Build Report</xsl:with-param>
          <xsl:with-param name="project" select="report/project/@project" />
          <xsl:with-param name="branch" select="report/project/@branch" />
        </xsl:call-template>

	<h2>Summary</h2>
	<table>
	  <tr>
	    <th>Project</th>
	    <td><xsl:value-of select="report/project/@project"/></td>
	  </tr>
	  <tr>
	    <th>Branch</th>
	    <td><xsl:value-of select="report/project/@branch"/></td>
	  </tr>
	  <tr>
	    <th>Errors:</th>
	    <td><xsl:value-of select="report/summary/@errors"/></td>
	   </tr>
	   <tr>
	     <th>Warnings:</th>
	     <td><xsl:value-of select="report/summary/@warnings"/></td>
	   </tr>
	 </table>

        <!-- Changelist Report -->
        <xsl:if test="count(report/changelist) &gt; 0">
          <h3>Changelist</h3>
          <xsl:call-template name="changelist_summary">
            <xsl:with-param name="changelist" select="report/changelist" />
          </xsl:call-template>

          <h3>Files</h3>
          <xsl:call-template name="changelist_files">
            <xsl:with-param name="file_count">5</xsl:with-param>
            <xsl:with-param name="file_list" select="report/changelist/files" />
          </xsl:call-template>
        </xsl:if>
        
        <!-- Rebuild Report -->
        <xsl:if test="count(report/changelist) = 0">
          <xsl:call-template name="generic_filelist">
            <xsl:with-param name="file_list" select="report/rebuild/files" />
          </xsl:call-template>
        </xsl:if>
        
	<h2>Errors</h2>
        <xsl:call-template name="report_errors">
          <xsl:with-param name="error_list" select="report/errors" />
        </xsl:call-template>
        
        <h2>Warnings</h2>
        <xsl:call-template name="report_warnings">
          <xsl:with-param name="warnings_list" select="report/warnings" />
        </xsl:call-template>
    	
        <h3>Full Build Output</h3>
        <pre>
          <xsl:for-each select="report/output/line">
            <xsl:value-of select="@text" />
            <xsl:text>
            </xsl:text>
          </xsl:for-each>
        </pre>    	
    	
	<h2>Additional Output</h2>
        <xsl:call-template name="report_info">
          <xsl:with-param name="info_list" select="report/infos" />
        </xsl:call-template>
        
      </body>
    </html>

  </xsl:template>
</xsl:stylesheet>
