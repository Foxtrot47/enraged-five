#
# File:: AssetBuilder.rb
# Description:: Monitors project's Perforce repositories
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 24 July 2008
#

# A development thing to switch the directory where we 'require' files from - for the purposes of development.
# developed in tools release since this is the usual much easier way when developing a Cruise Control project.

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
include Pipeline

require 'source/config'
require 'source/engine'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'show usage information' ],
	[ '--project', '-p', OS::Getopt::REQUIRED, 'specify project key (e.g. gta4, jimmy)' ],
	[ '--branch', '-b', OS::Getopt::REQUIRED, 'specify project branch (e.g. dev, dev_migrate)' ]
]

begin

	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------

	g_AppName = File::basename( __FILE__, '.rb' )
	g_ProjectName = ''
	g_BranchName = ''
	g_Project = nil
	g_Config = Pipeline::Config.instance()

	#-------------------------------------------------------------------------
	# Parse Command Line
	#-------------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] ) then
		puts OS::Getopt.usage( OPTIONS )
		exit( 1 )
	end

	g_ProjectName = opts['project'].nil? ? ENV['RS_PROJECT'] : opts['project']
	project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
	if ( not project_exists ) then
		puts OS::Getopt.usage( OPTIONS )
		puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
		exit( 2 )
	end
	g_Project = g_Config.projects[ g_ProjectName ]
	g_Project.load_config( )
	g_Project.load_content( )
	if ( not g_Project.enabled ) then
		puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
		exit( 3 )
	end
	g_BranchName = ( nil == opts['branch'] ) ? g_Project.default_branch : opts['branch']
	if ( not g_Project.branches.has_key?( g_BranchName ) ) then
		puts "\nError project: #{g_ProjectName} does not have branch #{g_BranchName} defined."
		exit( 4 )
	end
	g_Branch = g_Project.branches[g_BranchName]
	
	g_BuildConfig = AssetBuild::Configuration.new( g_Project, g_Branch )
		
	#-------------------------------------------------------------------------
	# Build Engine
	#-------------------------------------------------------------------------
	puts "#{g_AppName}"
	puts "Project: #{g_ProjectName}"
	puts "Branch:  #{g_BranchName}"
	builder = AssetBuild::Engine.new( g_Project, g_Branch, g_BuildConfig )
	builder.start( )
	
rescue Exception => ex

	puts "#{g_AppName} unhandled exception: #{ex.message}"
	puts "Call stack:"
	puts ex.backtrace.join( "\n\t" )
end

# End of AssetBuild.rb
