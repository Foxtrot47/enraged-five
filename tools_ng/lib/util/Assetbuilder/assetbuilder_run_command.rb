#
# File:: assetbuilder_run_command.rb
# Description:: 
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com> David Muir <david.muir@rockstarnorth.com>
# Date:: 07 June 2010
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require "source/builder/shell/console"
include Assetbuild::Builder::Shell

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 and ARGV.length > 0 ) then	
	console = Console.new( )
	console.run_command( ARGV.join(" ") )
end

# assetbuilder_run_command.rb
