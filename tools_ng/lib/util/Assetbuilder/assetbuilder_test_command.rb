#
# File:: test_command.rb
# Description:: 
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com> David Muir <david.muir@rockstarnorth.com>
# Date:: 07 June 2010
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require "source/builder/shell/console"
require "source/builder/shell/command"
require "source/builder/shell/command_queue"
include Assetbuild::Builder::Shell

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then	
	console = Console.new( )
	console.run_command( "help" )
end

# test_command.rb
