#
# File:: print_content_tree.rb
# Description:: Pretty print project's content tree to stdout.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 11 August 2008
#



#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ "--project", "-p", Getopt::REQUIRED, "project data to convert (e.g. jimmy, gta4_jpn)." ],
	[ "--group", "-g", Getopt::REQUIRED, "content group to find and print (optional)." ],
	[ "--help", "-h", Getopt::BOOLEAN, "display usage information." ]
]

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------

	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_ProjectName = ''
		g_Group = ''
		g_Project = nil
		g_Config = Pipeline::Config.instance( )	
	
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
		
		g_ProjectName = ( nil == opts['project'] ) ? '' : opts['project']
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			puts OS::Getopt.usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) then
			puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
			exit( 3 )
		end	
		g_Group = ( nil == opts['group'] ) ? '' : opts['group']
				
		g_Project.load_config()
		g_Project.load_content()
		
		group = g_Project.content.find_first_group( g_Group )
		group.pretty_print( )
		
	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
	
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")		
	ensure

		puts "Press Enter to continue..."
		$stdin.getc( )	
	end	
end

# print_content_tree.rb
