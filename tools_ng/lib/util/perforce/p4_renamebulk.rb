#
# File:: %RS_TOOLSLIB%/util/perforce/p4_renamebulk.rb
# Description:: Perforce bulk rename utility; uses 'p4 move'.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 14 September 2011
#
# For example usage: %RS_TOOLSROOT%/script/util/perforce/rename_carrecs.bat.
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/scm/perforce'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [	
	[ '--source_regexp', '-s', OS::Getopt::REQUIRED, 'source file regular expression.' ],
	[ '--dest_regexp', '-d', OS::Getopt::REQUIRED, 'destination file regular expression.' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ],
]
TRAILING = { 'file' => 'Perforce filespec to rename.' }

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------
# None

#----------------------------------------------------------------------------
# Entry
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
	g_AppName = OS::Path::get_basename( __FILE__ )
	g_Config = Pipeline::Config.instance( )
	g_Log = Log::new( g_AppName )	
	begin
		#--------------------------------------------------------------------
		# Parse Command Line
		#--------------------------------------------------------------------
		g_Options, g_Trailing = OS::Getopt.getopts( OPTIONS )
		if ( g_Options['help'] ) then
			puts OS::Getopt::usage( OPTIONS )
			exit( 1 )
		end

		#--------------------------------------------------------------------
		# Parse Command Line : Regular Expressions
		#--------------------------------------------------------------------
		g_SourceRegExp = Regexp::new( g_Options['source_regexp'] )
		if ( g_SourceRegExp.nil? ) then
			g_Log.error( "Source regular expression missing." )
			puts OS::Getopt::usage( OPTIONS, TRAILING )
			exit( 2 )			
		end
		
		g_DestRegExp = g_Options['dest_regexp']
		if ( g_DestRegExp.nil? ) then
			g_Log.error( "Destination regular expression missing." )
			puts OS::Getopt::usage( OPTIONS, TRAILING )
			exit( 3 )			
		end
	
		Dir::chdir( g_Config.project.root )
		g_Perforce = SCM::Perforce::new( )
		g_Perforce.connect( )
		g_CL = g_Perforce.create_changelist( "Perforce Bulk rename: #{g_Options['source_regexp']} ==> #{g_DestRegExp}" )
		
		g_FileCount = 0
		g_Trailing.each do |filespec|
			g_Perforce.run_sync( filespec )
			
			fstat = g_Perforce.run_fstat( filespec )
			fstat.each do |fstat|
				next if ( fstat['headAction'] == 'delete' )
				next if ( fstat['headAction'] == 'move/delete' )
				
				next unless ( fstat['depotFile'] =~ g_SourceRegExp )
				
				source_depotfile = fstat['depotFile']
				source_filename = fstat['clientFile']
				source_directory = OS::Path::get_directory( source_filename )
				destination_filename = Kernel::eval( g_DestRegExp )
				destination_path = OS::Path::combine( source_directory, destination_filename )
				
				g_Perforce.run_edit( source_depotfile )
				g_Perforce.run_move( '-c', g_CL.to_s, source_depotfile, destination_path )

				g_Log.info( "Moving: #{source_filename} ==> #{destination_path}" )
				g_FileCount += 1
			end
		end
		g_Log.info( "#{g_FileCount} files renamed in CL\##{g_CL}." )
		
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
		g_Log.exception( ex, 'Unhandled exception' )
		puts "Unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n")
		exit -1
	end
end

# %RS_TOOLSLIB%/util/perforce/p4_renamebulk.rb
