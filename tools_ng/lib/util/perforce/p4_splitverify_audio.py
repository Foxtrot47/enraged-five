#!/usr/bin/python
#
# 

"""A P4Python script that splits the verify process over the root directories of each depot

$Id$

#*******************************************************************************
# Copyright (c) 2009, Perforce Software, Inc.  All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1.  Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# 2.  Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL PERFORCE SOFTWARE, INC. BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#*******************************************************************************

"""

from __future__ import with_statement
import P4
import sys
import datetime
import time

p4 = P4.P4()
# assumes that the P4 enviroment is set correctly via environment variables or P4CONFIG
# otherwise, set the P4 environment here
p4.port = "rsgedip4d1:1666"
p4.user = "svcrsgediperforce"

def formatTD(td):		
	days = td.seconds // (60*60*24)
	hours = ( td.seconds % (60*60*24) ) // 3600
	minutes = (td.seconds % 3600) // 60
	seconds = td.seconds % 60
	return '%s days, %sh %sm %ss' % (days, hours, minutes, seconds) 

def main( file ):
	try:
		p4.connect()
		
		time_start = datetime.datetime.now()
		print >> file, time_start.strftime( "[%H:%M:%S - %d/%m/%Y]" ) + ": P4 Verify Start"

		dirs = p4.run_dirs("//*/*")
		result = []
		for d in dirs:
			pattern = d['dir'] + "/..."			
			if ( 0 == cmp( pattern, "//depot/audio/..." ) ):
				print >> file, datetime.datetime.now().strftime( "[%H:%M:%S - %d/%m/%Y]" ) + ": Verifying %s" % pattern
				result += ( p4.run_verify("-q",  pattern))
		
		if len(result) > 0:
			print >> file, "Verify reported errors:"
			for r in result:
				print >> file, r

		time_end = datetime.datetime.now()
		print >> file, time_end.strftime( "[%H:%M:%S - %d/%m/%Y]" ) + ": P4 Verify End"
		
		delta = (time_end-time_start)
		print >> file, "Verify took: " + formatTD(delta)

		p4.disconnect()
		
	except P4.P4Exception, err:
		print >> file, err

if __name__ == "__main__":
	if len( sys.argv ) > 1:
		filename = sys.argv[1]
		with open( filename, "w" ) as hFile:
			main(hFile)
	else:
		main(sys.stdout)
	print "verify complete"



