#
# File:: p4_standardize_filetypes.rb
# Description:: Standardizes the filetypes in Perforce.  This will accrue a changelist 
# that contains filetype changes.  Changes will be checked out in the default changelist.
#
# Date:: 12 September 2011
#
#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/scm/perforce'
include Pipeline

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
begin

	# Configure and connect to Perforce.
	config = Pipeline::Config.instance	
	@p4 = SCM::Perforce.new()
	@p4.port = config.sc_server
	@p4.client = config.sc_workspace
	@p4.user = config.sc_username
	@p4.connect()
	
	common_file_paths = Array.new()
	common_file_paths << "#{ENV["RS_PROJROOT"]}/assets/export/..."
	common_file_paths << "#{ENV["RS_BUILDBRANCH"]}..."
	common_file_info = []
	
	standardtypes = Array.new()
	standardtypes << [ '.zip', 'binary', 'F' ]
	standardtypes << [ '.rpf', 'ubinary', nil ]			#Note the 'ubinary' is an old name and a synonym of "binary+F"

	common_file_paths.each do |common_path|
		
		fstat_records = @p4.run('fstat', common_path)
		fstat_records.each do |fstat_record|
			
			if fstat_record['headAction'] != 'delete' then
			
				standardtypes.each do |standardtype|
					
					if fstat_record['depotFile'].index(standardtype[0]) != nil  
					
						basetype, modifiers = fstat_record['headType'].split( '+' ) 
						
						if basetype != standardtype[1] or modifiers != standardtype[2] then
						
							current_filetype = basetype
							if standardtype[1] != nil then
								current_filetype = standardtype[1]
							end
							
							current_modifiers = modifiers
							if standardtype[2] != nil then
								current_modifiers = standardtype[2]
							end
							
							filetype = "#{current_filetype}+#{current_modifiers}" 	#Expected Perforce format for filetypes.
							print "Checking out #{fstat_record['depotFile']} with #{filetype}...\n"
							@p4.run('edit', '-t', filetype, fstat_record['depotFile'])
						end
						
					end
				end
			
			end
		end
	end
	

	0
end

# %RS_TOOLSLIB%/util/perforce/p4_standardize_filetypes.rb