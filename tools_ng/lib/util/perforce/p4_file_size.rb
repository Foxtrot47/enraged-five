#
# File:: %RS_TOOLSLIB%\util\perforce\p4_file_size.rb
# Description:: 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 16 August 2010
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/scm/perforce'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--ignore', '-i', OS::Getopt::REQUIRED, 'ignore paths.' ],
	[ '--kilobytes', '-k', OS::Getopt::BOOLEAN, 'change units to kilobytes.' ],
	[ '--all', '-a', OS::Getopt::BOOLEAN, 'calculate sizes for all revisions.' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING = { 'paths' => 'paths to produce file data sizes for.' }

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
# Entry-Point
#----------------------------------------------------------------------------

begin
	g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
	
	g_Kilobytes = g_Opts['kilobytes'].nil? ? false : true
	g_Megabytes = ( not g_Kilobytes )
	g_AllRevs = g_Opts['all'].nil? ? false : true
	g_InitialDir = Dir::pwd()
	
	if ( g_Opts['help'] or ( 0 == g_Trailing.size ) ) then
		puts OS::Getopt::usage( OPTIONS )
		exit( 1 )
	end
	
	path_sizes = {}
	g_Trailing.each do |path|
		puts "Processing: #{path}"
		path = OS::Path::normalise( File::expand_path( path ) )	
		
		oldpath = Dir::pwd()
		Dir::chdir( path )
		p4 = SCM::Perforce.new()
		p4.connect()
		
		fstat = p4.run_fstat( '-Ol', "#{path}/..." ) if ( not g_AllRevs )
		fstat = p4.run_fstat( '-Ol', '-Of', "#{path}/..." ) if ( g_AllRevs ) 
		
		size = 0
		fstat.each do |st|
			next unless ( st.has_key?( 'fileSize' ) )
			size += st['fileSize'].to_i
		end
		path_sizes[path] = size
		
		Dir::chdir( oldpath )
	end
	
	#--
	# Output
	#--
	path_sizes.each_pair do |path, size|
		puts "Path: #{path} => #{size / 1024.0} KB" if ( g_Kilobytes )
		puts "Path: #{path} => #{size / 1024.0 / 1024.0} MB" if ( g_Megabytes )
	end
	
rescue SystemExit => ex
	exit( ex.status )

rescue Exception => ex
	puts "Unhandled exception: #{ex.message}."
	ex.backtrace.each do |m| puts m; end
end

# %RS_TOOLSLIB%\util\perforce\p4_file_size.rb
