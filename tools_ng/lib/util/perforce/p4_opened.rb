#
# File:: p4_opened.rb
# Description:: 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 15 July 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/gui/log_window'
require 'pipeline/log/log'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/scm/perforce'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--show', '-s', OS::Getopt::BOOLEAN, 'show opened files.' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING_DESC = { 'paths' => 'Perforce depot path to check for opened files.' }

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then
	
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	
	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_Config = Pipeline::Config.instance( )
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
			exit( -1 )
		end
		if ( 0 == trailing.size ) then
			puts 'No trailing path arguments specified.  Exiting.'
			puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
			exit( -2 )
		end
		
		# Connect to Perforce
		exitcode = 0
		GUI::LogWindow::show_dialog( true, LogSystem::instance().rootlog, 'P4 Checked-Out Log' ) do

			p4 = Pipeline::SCM::Perforce.new( )
			p4.user = g_Config.sc_username
			p4.client = g_Config.sc_workspace
			p4.port = g_Config.sc_server
			p4.connect( )
			if ( not p4.connected? ) then
				puts 'Failed to connect to Perforce server.  Check installer config.'
				puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
				exit( -3 )
			end
			
			# Normalise user-paths
			user_paths = []
			trailing.each do |trailing|
				user_paths << OS::Path.normalise( trailing )
			end
			
			# Fetch opened files from p4 opened command.
			opened_files = []
			user_paths.each do |path|
				opened_files += p4.run_opened( path )
			end	
			opened_files.each do |opened|
				LogSystem::instance().rootlog.warn( "File: #{opened['depotFile']} is checked-out." )
			end	
			
			if ( opened_files.size > 0 ) then
				LogSystem::instance().rootlog.error( 'Files are checked-out.' )
				exitcode = 1 
			end
			
			# Disconnect
			p4.disconnect( )
		end
		exit( exitcode )

	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")		
	end	
end

# p4_opened.rb
