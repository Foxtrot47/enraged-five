#
# File:: %RS_TOOLSLIB%/util/perforce/p4_submit.rb
# Description:: Simple utility script to checkin a file given an absolute path.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 2 September 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ "--port", "-p", Getopt::REQUIRED, "(optional) Perforce server port to use." ],
	[ "--client", "-c", Getopt::REQUIRED, "(optional) Perforce client to use." ],
	[ "--description", "-d", Getopt::REQUIRED, "changelist description (requires quotes)." ],
	[ "--help", "-h", Getopt::BOOLEAN, "display usage information." ]
]
TRAILING_DESC = 'absolute file paths separated by spaces (local syntax).'

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------

	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_Config = Pipeline::Config.instance( )
	
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS, { 'files' => TRAILING_DESC } )
			exit( 1 )
		end
		if ( 0 == trailing.size ) then
			puts 'No trailing file arguments specified.  Exiting.'
			puts OS::Getopt.usage( OPTIONS, { 'files' => TRAILING_DESC } )
			exit( 2 )
		end

		# Connect to Perforce
		trailing.each do |path|
		
			path = OS::Path::normalise( path )
			Dir::chdir( OS::Path::platform_native( path ) ) do
			
				p4 = Pipeline::SCM::Perforce.new( )
				p4.port = opts['port'] unless ( opts['port'].nil? )
				p4.client = opts['client'] unless ( opts['client'].nil? )
				p4.connect( )
				puts "SERVER: #{p4.port} (#{Dir::pwd})"

				if ( not p4.connected? ) then
					puts 'Failed to connect to Perforce server.  Check installer config.'
					puts OS::Getopt.usage( OPTIONS, { 'files' => TRAILING_DESC } )
					exit( 3 )
				end

				change_id = p4.create_changelist( opts['description'].gsub( "$", "\n" ) )
				depot_path = nil
				path_where = p4.run_where( path ) 
				path_where.each do |where|
					next if ( where.has_key?( 'unmap' ) )
					depot_path = where['depotFile']
					break
				end
				
				p4.run_reopen( '-c', change_id.to_s, depot_path )

				cl = p4.run_describe( change_id.to_s ).shift
				if ( cl['depotFile'].is_a?( Array ) and cl['depotFile'].size > 0 ) then
					p4.run_submit( '-c', change_id )
				else
					p4.delete_changelist( change_id.to_s )
				end			

				# Disconnect	
				p4.disconnect( )
			end
		end
		
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
	
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")		
	end	
end

# %RS_TOOLSLIB%/util/perforce/p4_submit.rb
