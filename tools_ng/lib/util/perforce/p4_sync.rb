#
# File:: util/perforce/p4_sync.rb
# Description:: Perforce sync wrapper.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 9 March 2009
#

#-----------------------------------------------------------------------------
# Globals
#-----------------------------------------------------------------------------
$no_project_check = false
$no_network_check = false
#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/log/log'
require 'pipeline/os/getopt'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ "--debug", "-d", OS::Getopt::BOOLEAN, 'display debug log messages.' ],
	[ "--help", "-h", OS::Getopt::BOOLEAN, 'display usage information.' ],
	[ "--noprojectcheck", "-p", OS::Getopt::BOOLEAN, 'disabled project toolchain update check.' ]
]
TRAILING_DESC = 'Perforce file paths separated by spaces (e.g. //depot/...).'

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	g_AppName = File::basename( __FILE__, '.rb' )

	#---------------------------------------------------------------------
	# Parse Command Line
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] ) then
		puts OS::Getopt.usage( OPTIONS, { 'files' => TRAILING_DESC } )
		exit( 1 )
	end
	if ( 0 == trailing.size ) then
		puts 'No trailing file arguments specified.  Exiting.'
		puts OS::Getopt.usage( OPTIONS, { 'files' => TRAILING_DESC } )
		exit( 2 )
	end
	g_Debug = opts['debug'].nil? ? false : true

	$no_project_check = opts['noprojectcheck'].nil? ? false : true 
	$no_network_check = $no_project_check
	
	g_Config = Pipeline::Config.instance( )
	g_Config::log_trace = g_Debug
	g_Config::logtostdout = true
	g_Config::log_level = 2 if ( g_Config::log_level > 2 )
	g_Config::log_level = 1 if ( g_Debug )
	g_Log = Log::new( g_AppName )
				
	begin
		# Connect to Perforce
		p4 = Pipeline::SCM::Perforce.new( )
		p4.user = g_Config.sc_username
		p4.client = g_Config.sc_workspace
		p4.port = g_Config.sc_server
		p4.connect( )
		if ( not p4.connected? ) then
			puts 'Failed to connect to Perforce server.  Check installer config.'
			puts OS::Getopt.usage( OPTIONS, { 'files' => TRAILING_DESC } )
			exit( 3 )
		end
		
		all_errors = []
		trailing.each do |path|
			g_Log.info( "Syncing #{path}" )
			p4.run_sync_with_block( path ) do |file, success, errors|
				if ( not success ) then
					all_errors += errors
					next
				end
				g_Log.info( "Sync #{file}" )
			end
		end

		# Disconnect.
		p4.disconnect( )
		
		# Repeat all errors; return appropriate error code.
		g_Log.info( "All sync errors listed below." ) if ( all_errors.size > 0 )
		all_errors.each do |error|
			g_Log.error( error )	
		end
		exit( 1 ) if ( all_errors.size > 0 )
		exit( 0 ) if ( 0 == all_errors.size )
		
	rescue SystemExit => ex
		exit( ex.status )
		
	rescue Exception => ex
		g_Log.exception( ex, "\n#{g_AppName} unhandled exception" )
	end	
end

# util/perforce/p4_sync.rb
