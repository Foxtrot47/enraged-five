#
# File:: p4_label_invalid_files.rb
# Description:: Generates a list of files that have revisions purged on the server at the particular label.
#
# Date:: 12 September 2011
#
#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/scm/perforce'
include Pipeline


OPTIONS = [
	[ '--label', '-l', OS::Getopt::REQUIRED, 'The label name.' ],
	[ '--verbose', '-q', OS::Getopt::BOOLEAN, 'Enables debug output.' ],
]
TRAILING = {}


def FindInvalidFiles (label_name, search_paths)

	invalid_files = []
	
	search_paths.each do |file_path|
		fstat_records = @p4.run('fstat', "#{file_path}@#{label_name}")

		fstat_records.each do |fstat_record|
		
			if fstat_record['headAction'] != 'delete' then
				
				basetype, modifiers = fstat_record['headType'].split( '+' ) 
			
				if modifiers != nil and not modifiers.empty? then
				
					index = modifiers.index(/S[0-9]+/)
					
					serverCountExp = /(S)([0-9]+)/
					serverCount = serverCountExp.match(modifiers)
					
					if serverCount != nil and serverCount.length > 2 then
						serverCount = serverCount[2].to_i	
					
						#Find out what the real head revision is.  Note that the "head revision" of fstat_records is
						#the head revision at the label.
						head_fstat_records = @p4.run('fstat', "#{fstat_record['depotFile']}")
						head_rev = head_fstat_records[0]['headRev'].to_i

						label_file_rev = fstat_record['headRev'].to_i						
						if label_file_rev < (head_rev - serverCount) then
						
							print "Invalid file found: #{fstat_record['depotFile']}.\n"
							invalid_files << fstat_record['depotFile']
						end
					end
				end
			end
		end
	end

	invalid_files
end

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
begin

	g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
	
	if nil == g_Opts['label'] then
		abort("Label was not specified.  Aborting...")
	end
	
	label_name = g_Opts['label']
	
	# Configure and connect to Perforce.
	config = Pipeline::Config.instance	
	@p4 = SCM::Perforce.new()
	@p4.port = config.sc_server
	@p4.client = config.sc_workspace
	@p4.user = config.sc_username
	@p4.connect()
	
	common_file_paths = Array.new()
	common_file_paths << "#{ENV["RS_BUILDBRANCH"]}..."
	common_file_info = []
	
	invalid_files = FindInvalidFiles(label_name, common_file_paths)
	
	print "There are #{invalid_files.count} files that are invalid in label #{label_name}\n"
	invalid_files.each do |invalid_file|
	
		print "\t#{invalid_file}\n"
	end

	0
end

# %RS_TOOLSLIB%/util/p4_find_invalid_labels.rb