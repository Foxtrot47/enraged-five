#
# File:: p4_edit.rb
# Description:: Simple utility script to checkout a file given an absolute path.
#               The file is put into a new changelist, description is optional.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 2 September 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ "--description", "-d", OS::Getopt::REQUIRED, "changelist description (requires quotes)." ],
	[ "--help", "-h", OS::Getopt::BOOLEAN, "display usage information." ]
]
TRAILING_DESC = 'absolute file paths separated by spaces.'

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------

	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_Config = Pipeline::Config.instance( )
	
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS, { 'files' => TRAILING_DESC } )
			exit( 1 )
		end
		if ( 0 == trailing.size ) then
			puts 'No trailing file arguments specified.  Exiting.'
			puts OS::Getopt.usage( OPTIONS, { 'files' => TRAILING_DESC } )
			exit( 2 )
		end
	
		# Connect to Perforce
		p4 = Pipeline::SCM::Perforce.new( )
		p4.user = g_Config.sc_username
		p4.client = g_Config.sc_workspace
		p4.port = g_Config.sc_server
		p4.connect( )
		if ( not p4.connected? ) then
			puts 'Failed to connect to Perforce server.  Check installer config.'
			puts OS::Getopt.usage( OPTIONS, { 'files' => TRAILING_DESC } )
			exit( 3 )
		end
	
		# Create changelist
		description = opts['description'] ? opts['description'].sub!( /^=/, '' ) : ''
		change_id = p4.create_changelist( description )
		puts "Changelist #{change_id} created:"
		# Open files for edit
		files_added = false
		trailing.each do |filename|
			result = p4.run_edit( '-c', change_id.to_s, filename ).shift
			
			if ( result.nil? ) then
				puts "\t#{filename} failed to open for edit."
			elsif ( result.kind_of? String and result.index("reopen") != nil ) then
				puts "\t#{filename} is already opened."
			elsif ( result.has_key?( 'clientFile' ) )
				files_added = true
				puts "\t#{filename} opened for edit."
			end
		end
		
		# Delete our changelist if nothing was added.
		if ( not files_added ) then
			puts "Deleting changelist as nothing was opened for edit."
			p4.delete_changelist( change_id )
		end
		
		# Disconnect
		p4.disconnect( )
		
	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
	
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")		
	end	
end

# p4_edit.rb
