#
# File:: util/perforce/p4_typemap_sync.rb
# Description:: Script to sync Perforce files with server typemap.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 9 November 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/scm/perforce'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------

OPTIONS = [
	[ '--verbose', '-v', OS::Getopt::BOOLEAN, 'verbose logging to stdout.' ],
	[ "--help", "-h", OS::Getopt::BOOLEAN, "display usage information." ]
]

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

# Print usage information to stdout.
def print_usage( )
	puts OS::Getopt.usage( OPTIONS, TRAILING )
end

# Process a single Perforce filespec.
def process_typemap( p4, typemap, log )

	changelist_desc = "Automated Perforce Typemap sync."
	changelist_id = p4.create_changelist( changelist_desc )

	# Loop through our typemap, then fstat and iterate through all files
	# that the typemap specifies for each line.
	typemap.each do |map|
		type, path = map.split( ' ' )
		map_type, map_modifiers = type.split( '+' )
		map_type = '' if ( map_type.nil? )
		map_modifiers = '' if ( map_modifiers.nil? )
		
		# Check if typemap files are in workspace, if not move on.
		where = p4.run_where( path ).shift
		next if ( where.nil? )
		
		# -Rc returns only files within the client view.
		fstat = p4.run_fstat( '-Rc', path )

		# Iterate through all of our files.  Checking their headType
		# against the fetched server typemap.
		fstat.each do |filestat|
			filename = filestat['depotFile']
			where = p4.run_where( filename ).shift
			if ( where.has_key?( 'unmap' ) ) then
				# This should never happen now because of the -Rc above.
				log.error( "Skipping #{filename} as its not in client view." )
				next
			end
						
			current_type, current_modifiers = filestat['headType'].split( '+' )
			current_type = '' if ( current_type.nil? )
			current_modifiers = '' if ( current_modifiers.nil? )

			if ( ( 0 == map_type.casecmp( current_type ) or map_type.nil? ) and
				 ( 0 == map_modifiers.casecmp( current_modifiers ) or map_modifiers.nil? ) ) then

				log.info( "Type and modifiers already in sync for #{filename}." )
				next
			end

			set_type = "#{map_type}+#{map_modifiers}"
			p4.run_sync( filename )
			
			log.info( "Set type and modifiers of #{filename} to #{set_type}." )
			if ( filestat.has_key?( 'action' ) ) then
				p4.run_reopen( '-c', changelist_id, '-t', set_type, filename )
			else
				p4.run_edit( '-c', changelist_id, '-t', set_type, filename )
			end
		end
	end
	
	# Revert unchanged files.
	unchanged = p4.run_revert( '-c', changelist_id, '-a' )
	unchanged.each do |file|
		log.info( "Unchanged file  #{file['depotFile']} reverted." )
	end
	
	# Revert our changelist if its empty.
	changelist = p4.run_opened( '-c', changelist_id ).shift
	if ( changelist.nil? ) then
		log.info( "Reverting empty changelist: #{changelist_id}." )
		p4.delete_changelist( changelist_id )
	end
end

#----------------------------------------------------------------------------
# Entry
#----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then
	
	#--------------------------------------------------------------------------
	# Entry-Point
	#--------------------------------------------------------------------------
	
	begin
		g_AppName = OS::Path::get_basename( __FILE__ )
		
		#------------------------------------------------------------------------
		# Parse Command Line
		#------------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
		g_Verbose = opts['verbose']
		Pipeline::Config::instance().logtostdout = g_Verbose
		g_Log = Log::new( g_AppName )
		
		# Connect to Perforce
		p4 = SCM::Perforce.new( )
		p4.connect( )
		if ( not p4.connected? ) then
			puts 'Failed to connect to Perforce server.  Check installer config.'
			print_usage( )
			exit( 2 )
		end
		
		# Fetch Perforce Typemap
		typemap = p4.run_typemap( '-o' ).shift
		if ( not typemap.has_key?( 'TypeMap' ) ) then
			puts 'Server typemap empty.  Check server config.  Exiting.'
			print_usage( )
			exit( 3 )
		end
		typemap = typemap['TypeMap']
		
		# Process server's typemap.
		g_Log.info( "Processing typemap" )
		process_typemap( p4, typemap, g_Log )
		
		# Disconnect
		p4.disconnect( )
		
	rescue SystemExit => ex
		exit( ex.status )
	
	rescue Exception => ex
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")
		
	end	
end

# util/perforce/p4_typemap_sync.rb
