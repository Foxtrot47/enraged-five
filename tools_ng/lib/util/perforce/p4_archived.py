#!/usr/local/bin/python

import P4
import sys

if len(sys.argv) < 3:
	print "Usage: archived.py <port> <user> <files...>"
	sys.exit(1)

p4 = P4.P4()
p4.port = sys.argv[1]
p4.user = sys.argv[2]
files = sys.argv[3]

p4.exception_level = P4.P4.RAISE_ERROR
p4.connect()

for fstat in p4.run_fstat("-Oa", files):
	message = "is not archived"
	if "attr-archiveDate" in fstat:
		message = "is archived"
	
	print "%s %s" % (fstat["depotFile"], message)

p4.disconnect()
