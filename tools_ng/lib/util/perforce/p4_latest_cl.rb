#
# File:: util/perforce/p4_latest_cl.rb
# Description:: Perforce get latest changelist user has synced to.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 12th March 2010
#


#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/start'
require 'pipeline/scm/perforce'
require 'pipeline/scm/perforce_helper'
			
#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ '--source_port', '-p', OS::Getopt::REQUIRED, 'source Perforce server (and port).' ],
	[ '--source_client', '-c', OS::Getopt::REQUIRED, 'source client/workspace name.' ],
	[ '--source_user', '-u', OS::Getopt::REQUIRED, 'source username.' ],
	[ '--source_path', '-s', OS::Getopt::REQUIRED, 'source path mapping.' ], 	
	[ "--debug", "-d", OS::Getopt::BOOLEAN, 'display debug log messages.' ],
	[ "--help", "-h", OS::Getopt::BOOLEAN, 'display usage information.' ],
	[ '--nocheck', "-nc", OS::Getopt::BOOLEAN, 'dont do a check to see what would sync after having got this changelist.' ]
]
TRAILING_DESC = 'Perforce file paths separated by spaces (e.g. //depot/...).'

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

def SetResult(cl)
	puts "CHANGELIST=#{cl}"
end
	
#----------------------------------------------------------------------------
# Entry
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
	begin
		g_AppName = OS::Path::get_basename( __FILE__ )
		g_Config = Pipeline::Config.instance( )
		g_Source = {}	# Source Perforce server settings
		
		#--------------------------------------------------------------------
		# Parse Command Line
		#--------------------------------------------------------------------
		g_Options, g_Trailing = OS::Getopt.getopts( OPTIONS )		
		g_Debug = g_Options['debug'].nil? ? false : true			
		g_Check = g_Options['nocheck'].nil? ? true : false
		
		# Force log output
		g_Config::log_trace = g_Debug
		g_Config::logtostdout = true
		g_Config::log_level = 2
		g_Config::log_level = 1 if ( g_Debug )
		g_Log = Log::new( g_AppName )
		
		if ( g_Options['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
				
		#--------------------------------------------------------------------
		# Parse Command Line : Source Arguments and Validate
		#--------------------------------------------------------------------
		g_Source[:port] = g_Options['source_port']
		g_Source[:client] = g_Options['source_client']
		g_Source[:user] = g_Options['source_user']
		g_Source[:path] = g_Options['source_path']
				
		# Validate
		g_Source.each_pair do |k, v|
			next unless ( v.nil? )
			g_Log.error( "source settings incomplete (#{k})." )
			puts OS::Getopt::usage( OPTIONS, TRAILING )
			exit( 2 )
		end
		
		#--------------------------------------------------------------------
		# Changes
		#--------------------------------------------------------------------
		
		# Create Perforce connection objects.
		g_Source[:p4] = SCM::Perforce.new( )
		g_Source[:p4].port = g_Source[:port]
		g_Source[:p4].client = g_Source[:client]
		g_Source[:p4].user = g_Source[:user]
		g_Source[:p4].connect( )
		
		filespec = g_Trailing
				
		g_Log.info( "Determine latest changelist id on filespec #{filespec}" )
		
		# DW - the intention here is to use the filespec trailing '#have' but now I'm reading stuff that seems to derise it's use.
		# i.e. p4 changes -m1 //...#have
		# see http://kb.perforce.com/UserTasks/ManagingFile..Changelists/DeterminingC..OfWorkspace
		# and http://stackoverflow.com/questions/47007/determining-the-last-changelist-synced-to-in-perforce		
		# and then try to get it working in the API ... I couldn't seem to satisfy its syntax!
		
		# get changes
		change = g_Source[:p4].run_changes( "-m1", "#{filespec}@#{g_Source[:p4].client}" )			
		
		if (change.length > 0)
			changelist_id = change[0]['change'] 			

			# http://kb.perforce.com/UserTasks/ManagingFile..Changelists/DeterminingC..OfWorkspace
			if (g_Check)				
				sync_output = g_Source[:p4].run_sync( "-n", "#{filespec}@#{changelist_id}" )
				if (sync_output.length > 0)		
					sync_output.each { |err| g_Log.error( "#{err['change']} #{err['clientFile']}" ) } if (g_Debug)					 
					g_Log.error( " Error: (run will --debug) See http://kb.perforce.com/UserTasks/ManagingFile..Changelists/DeterminingC..OfWorkspace and http://stackoverflow.com/questions/47007/determining-the-last-changelist-synced-to-in-perforce" )
				end
			end
			
			SetResult(changelist_id)
		else
			SetResult("-1")
			g_Log.error( "no changelist id found" )
			exit -1
		end
			
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
		g_Log.exception( ex, 'Unhandled exception' )
		puts "Unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n")
		puts "Press any key to continue..."
		$stdin.gets()
		exit -1
	end
	
	exit 0
end

exit -1

