# Show if a file is archived or not

require "P4"

# retrieve P4PORT and P4USER and the file name from the command line

if (ARGV.length < 3)
	print "Usage: archived.rb <port> <user> <filename>"
	exit
end

p4 = P4.new
p4.port = ARGV[0]
p4.user = ARGV[1]
files = ARGV[2]

p4.exception_level = P4::RAISE_ERRORS
p4.connect

p4.run_fstat("-Oa", files).each do
	|fstat|
	print "#{fstat["depotFile"]} is #{"not" if !fstat.has_key?("attr-archiveDate")} archived\n"
end

p4.disconnect
