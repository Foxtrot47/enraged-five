#
# File:: %RS_TOOLSLIB%/util/perforce/p4_remove_modifiers.rb
# Description:: Remove Perforce file modifiers.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 18 May 2010
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
Pipeline::Config::instance()::logtostdout = true
require 'pipeline/log/log'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/scm/perforce'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING_DESC = { 'paths' => 'Perforce depot paths to stat.' }


#----------------------------------------------------------------------------
# Entry-Point
#----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

	g_AppName = OS::Path::get_basename( __FILE__ )
	g_Config = Pipeline::Config.instance( )
	g_Log = Log::new( g_AppName )

	begin
		g_Options, g_Trailing = OS::Getopt.getopts( OPTIONS )
		if ( g_Options['help'] ) then
			puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
			exit( 1 )
		end
		if ( 0 == g_Trailing.size ) then
			puts 'No trailing path arguments specified.  Exiting.'
			puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
			exit( 2 )
		end
		
		# Connect to Perforce based.
		g_Perforce = SCM::Perforce::new
		g_Perforce.connect()
		g_Description = "Automated file modifier removal (#{g_AppName})." 
		g_Changelist = g_Perforce.create_changelist( g_Description )
		
		# Process each Perforce depot path specified.
		g_Trailing.each do |p4path|
			g_Log.info( p4path )
			
			g_Perforce.run_sync( p4path )
			fstat = g_Perforce.run_fstat( p4path )
			fstat.each do |stat|
				next unless ( stat.has_key?( 'headType' ) )
				next unless ( stat['headType'].include?( '+' ) )
			
				basetype, modifiers = stat['headType'].split( '+' ) 
				g_Log.info( "#{stat['depotFile']}: #{basetype} removing '#{modifiers}'" )
			
				g_Perforce.run_edit( '-c', g_Changelist.to_s, '-t', basetype, stat['depotFile'] )
			end
		end
		
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
		g_Log.exception( ex, "Unhandled exception" )
		exit( -1 )
	end
end

# %RS_TOOLSLIB%/util/perforce/p4_remove_modifiers.rb
