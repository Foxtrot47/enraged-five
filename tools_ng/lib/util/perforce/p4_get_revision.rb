#
# File:: lib/util/perforce/p4_get_revision.rb
# Description:: Fetch revision of file at a temporary location (like Alienbrain).
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 18 November 2009
#
# Uses p4ruby directly rather than through our Perforce class wrapper.
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/scm/gui/p4_history_dialog'
require 'pipeline/scm/perforce_util'
include Pipeline
require 'p4'
require 'wx'

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--port', '-p', OS::Getopt::REQUIRED, 'source Perforce server (and port).' ],
	[ '--client', '-c', OS::Getopt::REQUIRED, 'source client/workspace name.' ],
	[ '--user', '-u', OS::Getopt::REQUIRED, 'source username.' ],
	[ '--debug', '-d', OS::Getopt::BOOLEAN, 'include debug output.' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING = { 'filenames' => 'filenamse in Perforce depot syntax.' }

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------
# None

#----------------------------------------------------------------------------
# Entry
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	begin
		g_AppName = OS::Path::get_basename( __FILE__ )
		g_Config = Pipeline::Config.instance( )
		
		#--------------------------------------------------------------------
		# Parse Command Line
		#--------------------------------------------------------------------
		g_Options, g_Trailing = OS::Getopt.getopts( OPTIONS )
		g_Debug = g_Options['debug'].nil? ? false : true
		g_Port = g_Options['port']
		g_Client = g_Options['client']
		g_User = g_Options['user']
		
		# Force log output
		g_Config::log_trace = g_Debug
		g_Config::logtostdout = true
		g_Config::log_level = 2
		g_Config::log_level = 1 if ( g_Debug )
		g_Log = Log::new( g_AppName )
		
		if ( g_Options['help'] ) then
			puts OS::Getopt.usage( OPTIONS, TRAILING )
			exit( 1 )
		end
		
		# Server connection
		p4 = P4::new()
		p4.prog = g_AppName
		p4.port = g_Port
		p4.client = g_Client
		p4.user = g_User
		p4.connect( ) unless ( p4.connected?() )
		
		# Create new workspace
		g_NewClientName = "#{g_User}_get_revision_temp"
		filename = g_Trailing[0]
		where = p4.run_where( filename ).shift()
		target = OS::Path::get_directory( where['clientFile'].gsub( g_Client, g_NewClientName ) )
		target = OS::Path::combine( target, '_Get', OS::Path::get_filename( filename ) )
				
		depotfile = p4.run_filelog( '-L', filename ).shift
		handler = Proc::new do |rev|
			begin
				localfile = SCM::PerforceUtil::get_file_revision_temp( g_Log, p4, depotfile, rev )
				g_Log.info( "File: #{localfile} written." )
			
			rescue P4Exception => ex
				g_Log.exception( ex, 'Unhandled Perforce exception' )
			rescue Exception => ex
				g_Log.exception( ex, 'Unhandled exception' )
			ensure
				# Ensure we end up back with default workspace.
				p4.disconnect( )
				p4.connect( )
			end
		end
		SCM::GUI::HistoryDialogApp::new( depotfile, handler ).main_loop( )
		
	rescue P4Exception => ex
		puts "Unhandled P4 exception: #{ex.message}."
		puts ex.backtrace.join( "\n" )
		
	rescue SystemExit => ex
		exit( ex.status )
	
	rescue Exception => ex
		puts "Unhandled exception: #{ex.message}."
		puts ex.backtrace.join( "\n" )
	end
end

# lib/util/perforce/p4_get_revision.rb
