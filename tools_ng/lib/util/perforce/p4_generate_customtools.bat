@ECHO OFF
REM File:: p4_generate_customtools.bat
REM Description:: Reconstruct the custom tools XML and text input files for 
REM               Perforce UI tools (P4V and P4Win).
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 17 June 2010
REM

CALL setenv.bat
SET SCRIPT=%RS_TOOLSLIB%\util\file_template_processor.rb

X:
PUSHD %RS_TOOLSBIN%
ECHO P4V Custom Tools
%RS_TOOLSRUBY% %SCRIPT% --native --input=%RS_TOOLSLIB%/util/perforce/P4V/customtools_framework_integrate.xml.in --output=%RS_TOOLSLIB%/util/perforce/P4V/customtools_framework_integrate.xml

ECHO P4Win Custom Tools (for all the cool kids)
%RS_TOOLSRUBY% %SCRIPT% --native --input=%RS_TOOLSLIB%/util/perforce/P4Win/customtools_framework_integrate.txt.in --output=%RS_TOOLSLIB%/util/perforce/P4Win/customtools_framework_integrate.txt
%RS_TOOLSRUBY% %SCRIPT% --native --input=%RS_TOOLSLIB%/util/perforce/P4Win/customtools_shelving.txt.in --output=%RS_TOOLSLIB%/util/perforce/P4Win/customtools_shelving.txt
POPD

PAUSE
