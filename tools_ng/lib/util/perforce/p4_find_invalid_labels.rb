#
# File:: p4_find_invalid_labels.rb
# Description:: Generates a list of labels that have revisions purged on the server.
#
# Date:: 12 September 2011
#
#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/scm/perforce'
include Pipeline


OPTIONS = [
	[ '--labelfilter', '-l', OS::Getopt::REQUIRED, 'The label filter for acquiring a list of labels.' ],
	[ '--delete', '-d', OS::Getopt::BOOLEAN, 'Deletes any old labels.' ],
	[ '--fast', '-f', OS::Getopt::BOOLEAN, 'If this option is specified, the script will only look at executables to determine whether labels are invalid.' ],
	[ '--verbose', '-q', OS::Getopt::BOOLEAN, 'Enables debug output.' ],
]
TRAILING = {}

def IsLabelValid (label_name, search_paths)
	
	search_paths.each do |file_path|
		fstat_records = @p4.run('fstat', "#{file_path}@#{label_name}")

		fstat_records.each do |fstat_record|
		
			if fstat_record['headAction'] != 'delete' then
				
				basetype, modifiers = fstat_record['headType'].split( '+' ) 
			
				if modifiers != nil and not modifiers.empty? then
				
					index = modifiers.index(/S[0-9]+/)
					
					serverCountExp = /(S)([0-9]+)/
					serverCount = serverCountExp.match(modifiers)
					
					if serverCount != nil and serverCount.length > 2 then
						serverCount = serverCount[2].to_i	
					
						#Find out what the real head revision is.  Note that the "head revision" of fstat_records is
						#the head revision at the label.
						head_fstat_records = @p4.run('fstat', "#{fstat_record['depotFile']}")
						head_rev = head_fstat_records[0]['headRev'].to_i

						label_file_rev = fstat_record['headRev'].to_i
						if label_file_rev < (head_rev - serverCount) then
							return false
						end
					end
				end
			end
		end
	end

	return true
end

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
begin

	g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
	
	if nil == g_Opts['labelfilter'] then
		abort("Label filter was not specified.  Aborting...")
	end
	
	label_filter = g_Opts['labelfilter']
	verbose = g_Opts['verbose'].nil? ? false : true
	fast = g_Opts['fast'].nil? ? false : true
	delete_labels = g_Opts['delete'].nil? ? false : true 
	
	# Configure and connect to Perforce.
	config = Pipeline::Config.instance	
	@p4 = SCM::Perforce.new()
	@p4.port = config.sc_server
	@p4.client = config.sc_workspace
	@p4.user = config.sc_username
	@p4.connect()
	
	log = Log.new( 'InvalidPerforceLabels' )
	
	deprecated_labels = []
	
	common_file_paths = Array.new()
	common_file_paths << "#{ENV["RS_BUILDBRANCH"]}\\..." unless fast == true
	common_file_paths << "#{ENV["RS_BUILDBRANCH"]}\\*" if fast == true	#To test only executables.

	
	labels_record = @p4.run('labels', '-e', label_filter)	
	if labels_record.count == 0 then
		log.error("No labels were found matching the specified filter.")
		
	else
	
		labels_record.each do |record|
		
			label_name = record['label']
			
			log.debug("Checking #{label_name}...")
				
			if IsLabelValid(label_name, common_file_paths) == false then
				log.debug("Label #{label_name} is invalid.")
				deprecated_labels << label_name
			end		
		end
		
		log.info("INFO_MSG: There are #{deprecated_labels.count} invalid labels :")
		deprecated_labels.each do |deprecated_label|
		
			log.info( "INFO_MSG: \t#{deprecated_label}")
		end
		
		if delete_labels == true then			
			
			label_definition_dir = OS::Path.combine(ENV["RS_PROJROOT"], "labels")	
			if ( File.exists?(label_definition_dir) == false ) then
				Dir.mkdir(label_definition_dir)
			end
			
			deprecated_labels.each do |deprecated_label|
			
				label_definition_path = OS::Path.combine(label_definition_dir, "#{deprecated_label}.txt")
				label_definition_file = File.new(label_definition_path, "w")
				if label_definition_file == nil then
					log.error("Unable to create file #{label_definition_path}\n")
					next
				end
				
				log.info("Generating label definition file at #{label_definition_path}...")
				
				label_definition_file.write("Label Definition for #{deprecated_label}\n\n")
				
				#If we are deleting a label, then we are going to stash its label list in case we need to access it again.
				fstat_records = @p4.run('fstat', '-Ofp', "//...@#{deprecated_label}")
				fstat_records.each do |fstat_record|
				
					line = "#{fstat_record['depotFile']}##{fstat_record['headRev']}\n"
					label_definition_file.write(line)
				
				end
				
				log.info("Deleting label #{deprecated_label}...")
				@p4.run('label', '-d', deprecated_label)

			end
		end
	end

	0
end

# %RS_TOOLSLIB%/util/p4_find_invalid_labels.rb