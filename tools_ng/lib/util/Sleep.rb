#
# File:: sleep.rb
# Description:: Sleeps for set duration in seconds.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 16th October 2009
#


#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/log/log'
require 'pipeline/os/getopt'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ "--help", "-h", Getopt::BOOLEAN, "display usage information." ],
	[ "--seconds", "-s", Getopt::REQUIRED, "Number of seconds to sleep." ],
]

#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------
begin
	#---------------------------------------------------------------------
	# Parse Command Line.
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] )
		puts OS::Getopt.usage( OPTIONS )
		puts ("Press Enter to continue...")
		$stdin.getc( )				
		exit( 1 )
	end	
	
	duration = opts['seconds'] ? opts['seconds'].to_i	: 0

	#---------------------------------------------------------------------
	# Thread sleep.
	#---------------------------------------------------------------------
	Kernel.sleep duration

rescue
	$stderr.puts "Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	exit -1
end

exit 0