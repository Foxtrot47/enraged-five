# File:: buildp4_test.rb
# Description:: A quick test script.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 18th October 2010
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'util/buildp4'

error_count = 0

#-----------------------------------------------------------------------------
# Code
#-----------------------------------------------------------------------------
begin
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------

	g_Config = Pipeline::Config.instance()

	build_p4 = BuildP4.new("//depot/gta5/tools/lib/data/build_p4.txt", 
				"test buildp4 description", 
				"echo test2 > x:/gta5/tools/lib/data/build_p4.txt", 
				false, 
				true,
				true)
		
	error_count = build_p4.run( g_Config )

rescue Exception => ex

	puts "Call stack:"
	puts ex.backtrace.join( "\n\t" )
end	


Process.exit! error_count

# buildp4_test.rb