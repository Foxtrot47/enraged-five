#
# File:: util/registry/generate_register_extensions.rb
# Description:: Generate $(toolsbin)/bin/register_extensions.reg file for 
#               current branch.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 16 November 2009
#
# This is currently very simple as it only requires the root configuration
# data environment; no project data required.
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/util/environment'
include Pipeline
require 'progressbar'

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
INPUT_FILE = '$(toolslib)/util/registry/register_extensions.reg.in'
OUTPUT_FILE = '$(toolsroot)/tmp/register_extensions.reg'

#----------------------------------------------------------------------------
# Monkey-Patching
#----------------------------------------------------------------------------
module Pipeline
	
	# Monkey-patch base Config class to allow direct access to the 
	# environment object.
	class Config
		attr_reader :environment
	end

	# Monkey-patch Environment class to allow direct access to the symbol
	# table storage.
	class Environment
		attr_reader :symbol_table
	end
end # Pipeline module

#----------------------------------------------------------------------------
# Entry-Point
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	begin
		g_Config = Pipeline::Config::instance( )
		
		# We copy the root environment but ensure all paths are in the native
		# OS format so we don't have '/' in the registry.
		g_Environment = Environment::new( )
		g_Config.environment.symbol_table.each do |var, val|
			g_Environment.add( var, OS::Path::platform_native( val ).gsub( '\\', '\\\\\\\\\\' ) )
		end
		g_Environment.add( 'arg', '\"%1\"' )
		
		g_Environment.list( )
		
		input_file = g_Environment.subst( INPUT_FILE )
		output_file = g_Environment.subst( OUTPUT_FILE )
		
		data = []
		File::open( input_file, 'r' ) do |fp|
			data = fp.readlines
		end
		pbar = ProgressBar::new( "Generate Reg", data.size )
		File::open( output_file, 'w' ) do |fp|
			data.each_with_index do |line, index|
				if ( line.include?( '[' ) ) then
					fp.puts( line )
				else
					fp.puts( g_Environment.subst( line.gsub( '\\', '\\\\\\' ) ) )
				end
				pbar.inc( )
			end
		end
		pbar.finish( )
		
	rescue SystemExit => ex
		exit( ex.status )
		
	rescue Exception => ex
		puts "Unhandled exception: #{ex.message}."
		puts ex.backtrace.join( "\n" )
	end
end

# util/registry/generate_register_extensions.rb
