@ECHO OFF
REM File:: generate_register_extensions.bat
REM Description:: Re-construct the register_extensions.reg file for the
REM               current environment.
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 25 November 2009
REM
call setenv.bat >NUL
SET SCRIPT=%RS_TOOLSLIB%\util\registry\generate_register_extensions.rb

REM Clean up very old keys for folder conversions with old pipeline.
REM For some reason deletes in .reg file don't work correctly.
REG DELETE HKEY_CLASSES_ROOT\Folder\shell\convert /f
REG DELETE HKEY_CLASSES_ROOT\Folder\shell\convert_rave /f
REG DELETE HKEY_CLASSES_ROOT\Folder\shell\convert_rec /f

PUSHD %RS_TOOLSBIN%
%RS_TOOLSRUBY% %SCRIPT%
POPD

PAUSE
