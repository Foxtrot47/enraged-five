#
# File:: p4_speed_test.rb
# Description:: Runs a load of p4 commands to get an idea about speed.

# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 30th August 2011
#
# Passed in  :-  see OPTIONS ...
# Passed out :-  stderr contains all errors 
#                stdout for all other output. 
# Returns    :-  returns non zero upon detecting any errors 

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
#require 'pipeline/os/getopt'
require 'pipeline/log/log'
include Pipeline

INFO			= "[colourise=black]INFO_MSG: "
INFO_BLUE		= "[colourise=blue]INFO_MSG: "
MSG_PREFIX_EMAIL	= "[colourise=blue]INFO_EMA: "
MSG_PREFIX_WEB		= "[colourise=blue]INFO_WEB: "
INFO_GREEN		= "[colourise=green]INFO_MSG: "
INFO_ORANGE		= "[colourise=orange]INFO_MSG: "
P4LOOPS 		= 100
RUBYLOOPS 		= 10000000
FILENAME 		= "x:/gta5/assets/export/levels/gta5/vehicles.zip" # 500 meg of fun!

def test_ruby()
	RUBYLOOPS.times do 
		str = "this is a test"
		str = str.split
		str = str.join(" ")
		length = str.length		
	end
end

# Helper method...
# puts the msg and log it too.
def log_info( msg , build_log )
	msg = "#{Time.now.strftime('%Y-%m-%d %H:%M:%S')} #{msg}"
	build_log.info( msg )
	puts msg
end

def test_p4(g_Config)

	p4 = SCM::Perforce::create( g_Config.sc_server, g_Config.sc_username, g_Config.sc_workspace )
	p4.connect( )
	raise Exception if not p4.connected?	

	change_id = p4.create_changelist( "Create changelist\n" )	
	raise Exception if change_id.nil?

	P4LOOPS.times do 
		p4.run_sync(FILENAME)
		p4.run_edit( '-c', change_id.to_s, FILENAME )
		fstat = p4.run_fstat( FILENAME ).shift		
		p4.run_revert( '-a', '-c', change_id.to_s, '//...')
		p4.run_opened( '-c', change_id.to_s )
	end	

	p4.run_change('-d', change_id.to_s)
end

def test_p4_2(g_Config)

	build_log = Log.new( 'build')

	p4 = SCM::Perforce::create( g_Config.sc_server, g_Config.sc_username, g_Config.sc_workspace )
	p4.connect( )
	raise Exception if not p4.connected?	

	change_id = p4.create_changelist( "Create changelist\n" )	
	raise Exception if change_id.nil?

	P4LOOPS.times do			
		if (not File.exist?(FILENAME))
			next
		end

		log_info("Checkout: #{FILENAME} CL #{change_id.to_s}",build_log)
		p4.run_edit( FILENAME )
		p4.run_reopen( '-t', '+S10w', '-c', change_id.to_s, FILENAME )
	end
					
	p4.run_change('-d', change_id.to_s)
end


def test_log()

	build_log = Log.new( 'test_log_speed')
	change_id = 1000

	P4LOOPS.times do			
		log_info("test test test test",build_log)
	end
end




#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------

begin
	g_Config = Pipeline::Config.instance()

	# ---- create a p4 connection and a CL ----
	now = Time.now.to_f   
		test_p4(g_Config)	
	endd = Time.now.to_f   
	dur1 = endd - now 		
		
	now = Time.now.to_f   
		test_ruby()	
	endd = Time.now.to_f   
	dur2 = endd - now 							

	now = Time.now.to_f   
		test_log()	
	endd = Time.now.to_f   
	dur3 = endd - now 	
	
	
	puts "#{ENV['COMPUTERNAME']} p4 test took #{dur1}"
	puts "#{ENV['COMPUTERNAME']} ruby test took #{dur2}"	
	puts "#{ENV['COMPUTERNAME']} log test took #{dur3}"							
		
	
	Process.exit! 0

rescue Exception => ex
	$stderr.puts "Error: Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	Process.exit! -1
end
