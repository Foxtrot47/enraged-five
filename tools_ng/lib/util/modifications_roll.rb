#
# File:: roll_modifications.rb
# Description:: 

# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 08th September 2011
#
# Passed in  :-  see OPTIONS ...
# Passed out :-  stderr contains all errors 
#                stdout for all other output. 
# Returns    :-  returns non zero upon detecting any errors 

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/file'
require 'pipeline/util/email'
require 'systemu'
require 'rexml/document'
require 'fileutils'

include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ "--help", 		"-h", 	OS::Getopt::BOOLEAN, 	"display usage information." ],
]

# -- output prefixes ---
INFO				= "[colourise=black]INFO_MSG: "
INFO_BLUE			= "[colourise=blue]INFO_MSG: "

#=================================================================================================================
#
# CodebuilderStatsAnalyser : a class that analyses a database of code stats gathered from a smoketest of the game.
#
class MergeModifications

	#---------------------------------------------------------------------------------------------------------------------------
	#
	# Constructor
	#
	def initialize()
	end
	
	@@log = nil 
	def MergeModifications.log
		@@log = Log.new( 'merge_modifications' ) if @@log == nil
		@@log
	end	

	#---------------------------------------------------------------------------------------------------------------------------
	#
	# Control logic
	#
	def merge(src_filename, dst_filename)

		if File.exist?(src_filename)
			File.open(src_filename) do |file|
				doc_src = REXML::Document.new(file)			
				src_array_of_modifications = doc_src.elements["//ArrayOfModification"]
								
				if File.exist?(dst_filename)
					File.open(dst_filename) do |file|
						doc = REXML::Document.new(file)			
						modifications = doc.elements.to_a( "//ArrayOfModification" )
						modifications.each do |mod|
							src_array_of_modifications.add_element(mod)
							MergeModifications.log.info("#{INFO_BLUE} Modfication #{mod}")
						end						
					end	
				end
				
				#FileUtils::mkdir_p( OS::Path::get_directory( dst_filename ) ) unless ( File::directory?( dst_filename ) )
			
				File.open( dst_filename, 'w' ) do |fp|
					doc_src.write( fp, 2 )
				end							
			end
		else
			MergeModifications.log.error "#{MSG_PREFIX} #{src_filename} not found"
			return
		end			
	end

	#<!-- Start of the group of modifications (even if just one). -->
	#<ArrayOfModification xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
	#  <!-- Start of one modification. -->
	#  <Modification>
	#    <!-- The change number. -->--filename=
	#    <ChangeNumber>... value ...</ChangeNumber>
	#    <!-- The comment. -->
	#    <Comment>... value ...</Comment>
	#    <!-- The user's email address. -->
	#    <EmailAddress>... value ...</EmailAddress>
	#    <!-- The affected file name. -->
	#    <FileName>... value ...</FileName>
	#    <!-- The affect file's folder name. -->
	#    <FolderName>... value ...</FolderName>
	#    <!-- The change timestamp, in yyyy-mm-ddThh:mm:ss.nnnn-hhmm format -->
	#    <ModifiedTime>... value ...</ModifiedTime>
	#    <!-- The operation type. -->
	#    <Type>... value ...</Type>
	#    <!-- The user name. -->
	#    <UserName>... value ...</UserName>
	#    <!-- The related URL. -->	
	#    <Url>... value ...</Url>
	#    <!-- The file version. -->
	#    <Version>... value ...</Version>
	#    <!-- End of modification. -->
	#  </Modification>
	# <!-- End of the group of modifications. -->
	#</ArrayOfModification>
	#Pasted from <http://confluence.public.thoughtworks.org/display/CCNET/Modification+Writer+Task> 	

end # end class MergeModifications
 
#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 )
	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_ProjectName = ENV['RS_PROJECT']
		g_BranchName = ''
		g_Project = nil
		g_Config = Pipeline::Config.instance()

		#--------------------------------------------------------------------
		# --- PARSE COMMAND LINE                                          --- 
		#--------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] )
			puts OS::Getopt.usage( OPTIONS )
			puts ("Press Enter to continue...")
			$stdin.getc( )				
			Process.exit!( 1 )
		end	

		src_filename = trailing[0]
		dst_filename = trailing[1]
		
		if not src_filename
			$stderr.puts "Error: no src filename" 
			Process.exit! 1
		end
		
		if not dst_filename
			$stderr.puts "Error: no dst filename" 
			Process.exit! 2
		end

		merge_mods = MergeModifications.new()
		merge_mods.merge(src_filename, dst_filename)

		Process.exit! 0

	rescue Exception => ex
		$stderr.puts "Error: Unhandled exception: #{ex.message}"
		$stderr.puts "Backtrace:"
		ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
		Process.exit! -1
	end
end
