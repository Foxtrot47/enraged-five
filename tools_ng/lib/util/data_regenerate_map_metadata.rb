#
# File:: %RS_TOOLSLIB%/util/data_regenerate_map_metadata.rb
# Description:: Regenerate all map data metafiles from SceneXml.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 2 December 2010
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/log/log'
require 'pipeline/projectutil/data_convert'
require 'pipeline/projectutil/data_zip'
require 'pipeline/projectutil/data_map'
require 'pipeline/scm/perforce'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ '--project', '-p', Getopt::REQUIRED, 'project data to fetch (e.g. jimmy, gta5).' ],
	[ '--continue', '-c', Getopt::REQUIRED, 'continue from a particular map zip (e.g. vb_39).' ], 
	[ '--level', '-l', Getopt::REQUIRED, 'process a single level map data (e.g. mdtestbed).' ],
	[ '--group', '-g', Getopt::REQUIRED, 'process a group (folder) of map data (e.g. _citye, downtown_01).' ],
	[ '--map', '-m', Getopt::REQUIRED, 'process a single map (e.g. v_int_1).' ],
	[ '--list', '-l', Getopt::REQUIRED, 'process a list of files (e.g. list.txt).' ],	
	[ '--nop4', '-n', Getopt::BOOLEAN, 'disable Perforce integration.' ],
]
MOUNT = 'pack:/'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	g_AppName = OS::Path::get_basename( __FILE__ )
	g_Log = Pipeline::Log.new( g_AppName )
	g_Config = Pipeline::Config::instance()
	begin
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
        g_ProjectName = ( nil == opts['project'] ) ? ENV['RS_PROJECT']: opts['project']
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			puts OS::Getopt.usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) then
			puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
			exit( 3 )
		end
		g_Project.load_config( )
		g_ContinueMap = opts['continue'].nil? ? nil : opts['continue']
		g_Continue = ( not g_ContinueMap.nil? )
		g_Level = opts['level'].nil? ? nil : opts['level']
		g_Group = opts['group'].nil? ? nil : opts['group']
		g_Map = opts['map'].nil? ? nil : opts['map']
		g_ListFile = opts['list'].nil? ? nil : opts['list']
		g_UseP4 = opts['nop4'].nil? ? true : false
		
		g_List = []
		if (g_ListFile) then
			f = File.open(g_ListFile)
			g_List = f.readlines.map{ |line| line.chomp.downcase }
			f.close
		end
		
		Dir::chdir( g_Project.export )
		if ( g_UseP4 ) then
			g_Perforce = SCM::Perforce.new
			g_Perforce.connect()
			cl = g_Perforce.create_changelist( "Map metadata regeneration." )
		end
		g_Files = []
	
		# Now for the main processing loop...
		ProjectUtil::data_map_for_each( g_Project, g_Level, g_Group ) do |mapzip|
		
			if ( g_Continue and 
				 ( 0 == OS::Path::get_basename( mapzip.name ).casecmp( g_ContinueMap ) ) ) then
				g_Continue = false
			elsif ( g_Continue ) then
				g_Log.info( "Skipping #{OS::Path::get_basename( mapzip.name )}..." )
				next
			end
			if ( not g_Map.nil? ) then
				next unless ( g_Map == mapzip.name )
			end
		
			if ( not g_ListFile.nil? ) then
				index = g_List.index(OS::Path::get_basename( mapzip.name.downcase ))
				next unless ( not index.nil? ) 
				g_Log.info( "List selected #{OS::Path::get_basename( mapzip.name )}..." )
			end
		
			zip_file = mapzip.filename
			next unless ( File::exists?( zip_file ) )
			next unless ( mapzip.is_a?( Pipeline::Content::MapZip ) )
			next unless ( mapzip.inputs[0].is_a?( Pipeline::Content::Map ) )
			
			definitions = mapzip.inputs[0].exportdefinitions
			instances = mapzip.inputs[0].exportinstances
						
			build_dir = g_Project.build
			stream_dir = OS::Path::combine( g_Config.temp, 'meta_export', OS::Path::get_basename( zip_file ) )
			FileUtils::mkdir_p( stream_dir ) unless ( ::File::directory?( stream_dir ) )
			
			scenexml = OS::Path::get_basename( zip_file ) + '.xml'
			xml_file = OS::Path::replace_ext( zip_file, 'xml' )
		
			FileUtils::mkdir_p( stream_dir ) unless ( ::File::directory?( stream_dir) )
					
			ityp_dst = OS::Path::combine( stream_dir, OS::Path::get_basename( zip_file ) + '.ityp' )
			imap_dst = OS::Path::combine( stream_dir, OS::Path::get_basename( zip_file ) + '.imap' )

			if ( g_UseP4 and g_Perforce.exists?( zip_file ) ) then
				g_Perforce.run_edit( '-t', 'binary+F', zip_file )
				g_Perforce.run_reopen( '-t', 'binary+Fl', '-c', cl.to_s, zip_file )
			elsif ( not File::writable?( zip_file ) )
				FileUtils::chmod( 0666, zip_file )
			end
			
			filenames = ProjectUtil::data_map_export_meta( xml_file, ityp_dst, imap_dst, definitions, instances, stream_dir, build_dir )
			g_Files << zip_file
			
			if ( filenames.nil? ) then
				g_Log.error( "No META files generated by map export for #{OS::Path::get_basename( zip_file )}." )
			else
				# Extract zip and remove any old IMAP/ITYP files from the list
				# of input files.
				temp_dir = OS::Path::combine( g_Config.temp, 'map_metadata_append' )
				extracted_files = ProjectUtil::data_zip_extract( zip_file, temp_dir, true )
                extracted_files.delete_if do |item|
					0 == 'imap'.casecmp( OS::Path::get_extension( item ) ) or
					0 == 'ityp'.casecmp( OS::Path::get_extension( item ) )
				end
				ProjectUtil::data_zip_create( zip_file, extracted_files + filenames, true )
			end
		end
		#ProjectUtil::data_convert_file( g_Files, true )
	
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")    
	end
end

# %RS_TOOLSLIB%/util/data_regenerate_map_metadata.rb
