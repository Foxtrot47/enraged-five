#
# File:: time.rb
# Desc:: Equivalent to the Bash Shell's builtin `time` command for timing the
#        execution of scripts, except we only get realtime figures at the 
#        moment and not cpu times.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 29 February 2008
#
# == Example Usage
#
# From a command line:
#
#    time.rb date /T
#
#    29/02/2008
#
#    real    0m0.016s
#
#
# For timing Ruby scripts:
#
#    time.rb ruby scriptname.rb
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/os/getopt'
require 'pipeline/os/start'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
		[ '--help', '-h', Getopt::BOOLEAN, 'show this usage information.' ], 
		[ '--exit', '-e', Getopt::BOOLEAN, 'show process exit status.' ]
	]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

def print_usage
	usage_message = OS::Getopt::usage( OPTIONS, 
		{ 'command_line' => 'Command line to time.' } )
	puts usage_message
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------

begin
	
	opt, args = OS::Getopt.getopts( OPTIONS )
	
	# Check our builtin options (all one of them)
	if ( opt['help'] ) then
		print_usage( )
		
	else
		# Attempt to execute the first trailing argument specifying the others
		# as arguments to that script/program.
		time_start = Time.now()

		command = args.join( ' ' )
		result = system( command )
		puts "\nstatus\t#{$?.exitstatus}" if opt['exit'] && result
		puts "\nstatus\tcommand failed" if opt['exit'] && !result

		time_duration = Time.now() - time_start
		parts = time_duration.divmod( 60 )

		puts "\nreal\t#{parts[0]}m#{parts[1]}s\n" unless opt['exit']
		puts "real\t#{parts[0]}m#{parts[1]}s\n" if opt['exit']
	end

rescue OS::Getopt::Error => ex
	puts "Error parsing command line arguments: #{ex.message}"
	print_usage()
	
rescue Exception => ex

	puts "Unhandled exception: #{ex.message}"
	puts "\t#{ex.backtrace.join('\n\r')}"
end

# End of time.rb
