#
# File:: spit_error.rb
# Description:: Restarts the machine this script is run on.
#
# - Takes an input filename, parses it line by line piping what is deemed to be an error to stderr.
# - Further, everything that is not an error is piped to stdout.
# - File contents that are deemed warnings are wrapped with a warning syntax.
# - This is used by the asset tester.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 16th October 2009
#
# Passed in  :-  filename to parse 
# Passed out :-  stderr contains all errors 
#                stdout for all other contents of file. 
# Returns    :-  returns non zero upon detecting any errors 

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
#require 'pipeline/log/log'
require 'pipeline/os/getopt'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ "--help", "-h", Getopt::BOOLEAN, "display usage information." ],
	[ "--callstack", "-c", Getopt::BOOLEAN, "display callstacks." ],
	[ "--filename", "-f", Getopt::REQUIRED, "Filename to parse." ]
]

REGEXP_EXCEPTION 		= /^(.+)(\*\*\* EXCEPTION |Exception:|detected an interrupt)(.+)$/i
REGEXP_FATAL_ERROR 		= /^(.+)(Fatal Error)(.+)$/i
REGEXP_BASIC_ERROR		= /^(.+)(Error\s:|Error:\s)(.+)$/i
REGEXP_WARNING			= /^(.+)(Warning:\s)(.+)$/i
REGEXP_LOADED_OK		= /^(.*)Game init took(.+)$/i
REGEXP_CALLSTACK		= /^(.+)\[35m(.+)$/  
REGEXP_CALLSTACK_EXCLUSION 	= /^(.+)(- notfound|sysIpcThreadWrapper|CommonMain|ExceptMain|Main\(|rage::diagAssertHelper|rage::diagLog|mainCRTStartup)(.+)$/

#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------

lines_read 	= 0
error_count     = 0 
warning_count   = 0
loaded_ok 	= false
colourise_callstack = "[colourise=grey]"

begin
	#---------------------------------------------------------------------
	# Parse Command Line.
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] )
		puts OS::Getopt.usage( OPTIONS )
		puts ("Press Enter to continue...")
		$stdin.getc( )				
		exit( 1 )
	end	
	
	#---------------------------------------------------------------------
	# Parse file.
	#---------------------------------------------------------------------	
	if ( opts['filename'] )
		filename = opts['filename']

		lines = []
		file = File.new(filename, "r")
		while (line = file.gets)
			lines << line 
		end
		file.close

		lines.each_with_index do |line,line_idx|
			lines_read += 1

			is_exception 	= line =~ REGEXP_EXCEPTION 
			is_fatal_error 	= line =~ REGEXP_FATAL_ERROR
			is_basic_error 	= line =~ REGEXP_BASIC_ERROR
			is_warning 	= line =~ REGEXP_WARNING
			is_callstack 	= line =~ REGEXP_CALLSTACK
			is_loaded_ok 	= line =~ REGEXP_LOADED_OK
			is_error	= true if (is_exception) 
			is_error	= true if (is_fatal_error)
			is_error	= true if (is_basic_error)	

			is_callstack = false if ( is_callstack and line =~ REGEXP_CALLSTACK_EXCLUSION )
			is_callstack = false unless opts['callstack']

			callstack_warning = ""
			callstack_prefix = ""

			if (is_callstack and not (is_error or is_warning) )
				callstack_colourise = colourise_callstack
				callstack_prefix = "        ***CALLSTACK*** "
				lines[line_idx..-1].each do |search_line|									
					is_warning = ( search_line =~ REGEXP_WARNING )
		
					is_error   = ( 	search_line =~ REGEXP_EXCEPTION or 
							search_line =~ REGEXP_FATAL_ERROR or 
							search_line =~ REGEXP_BASIC_ERROR )
				
					if (is_error)
						break
					elsif (is_warning)
						callstack_warning = "Warning:"
						break
					end
				end								
				if (not is_error and (not is_warning) )
					is_error = true
				end
			end

			if ( is_error)
				$stderr.puts "#{callstack_colourise}#{callstack_prefix}#{lines_read}: #{line}"
				error_count += 1 unless (is_callstack)
			elsif ( is_warning )
				puts "#{callstack_colourise}#{callstack_prefix}#{lines_read}: #{callstack_warning} #{line}"
				warning_count += 1 unless (is_callstack)
			else
				puts "#{callstack_colourise}#{callstack_prefix}#{lines_read}: #{line}"
			end

			loaded_ok = true if (is_loaded_ok)			
		end


		puts "Read #{lines_read} lines."
		puts "Detected #{error_count} errors."
		puts "Detected #{warning_count} warnings."
		puts "Load ok" if loaded_ok
		puts "Load failed" unless loaded_ok
	end
rescue Exception => ex
	$stderr.puts "Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	exit -1
end

$stderr.puts("The game did not reach a point in the code where #{REGEXP_LOADED_OK.to_s} was output.") unless loaded_ok
$stderr.puts("It's possible the devkit has actually died or an exception has been hit - manual intervention required on assettester machine") if ((not loaded_ok) and error_count == 0)

exit -2 unless loaded_ok
exit -1 if (error_count > 0) 

exit 0
