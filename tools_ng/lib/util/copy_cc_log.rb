#
# File:: copy_cc_log.rb
# Description:: Copies the LATEST logfile into a folder and renames it so it can be suqsequently published by another script
#		this gives us the ability to know the status of a build when it is published.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 09th September 2011
#
# Passed in  :-  see OPTIONS ...
# Passed out :-  stderr contains all errors 
#                stdout for all other output. 
# Returns    :-  returns non zero upon detecting any errors 

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/file'
require 'fileutils'
include Pipeline
require 'systemu'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ "--help", 		"-h", 	OS::Getopt::BOOLEAN, 	"display usage information." ],
	[ '--logfiles', 	'-l', 	OS::Getopt::REQUIRED, 	'The logfiles' ],
	[ '--dest', 		'-d', 	OS::Getopt::REQUIRED, 	'The dest filename' ],
]

INFO = "[colourise=black]INFO_MSG: "


#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------

begin
	g_AppName = File::basename( __FILE__, '.rb' )
	g_ProjectName = ''
	g_BranchName = ''
	g_Project = nil
	g_Config = Pipeline::Config.instance()

	#---------------------------------------------------------------------
	# --- PARSER COMMAND LINE                                          --- 
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] )
		puts OS::Getopt.usage( OPTIONS )
		puts ("Press Enter to continue...")
		$stdin.getc( )				
		Process.exit!( 1 )
	end	

	logfiles = opts['logfiles'] ? opts['logfiles'] : nil
			
	if (not logfiles)
		$stderr.puts("Error: no logfiles filespec")
		Process.exit! -1
	end
 
 	dest = opts['dest'] ? opts['dest'] : nil
 			
 	if (not dest)
 		$stderr.puts("Error: no dest")
 		Process.exit! -1
 	end
 
	logfiles = File::expand_path( logfiles )
	filenames = OS::FindEx::find_files( logfiles  )

	if filenames
		sorted_filenames = filenames.sort_by {|filename| File.mtime(filename) } 
		sorted_filenames.reverse!
		newest_filename = sorted_filenames[0]
		
		if newest_filename
			FileUtils.cp(newest_filename,dest)
			puts "#{INFO} Copying #{newest_filename} to #{dest} #{File.mtime(newest_filename)}"
		end
	end
	
	Process.exit! 0

rescue Exception => ex
	$stderr.puts "Error: Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	Process.exit! -1
end
