#
# File:: %RS_TOOLSLIB%/util/data_convert_file.rb
# Description:: Attempt to intelligently convert a file, given only the local
#               independent path.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 26 September 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/gui/exception_dialog'
require 'pipeline/gui/log_window'
require 'pipeline/log/log'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_convert'
require 'pipeline/projectutil/data_content'
require 'pipeline/resourcing/path'
require 'pipeline/util/environment'
require 'pipeline/util/rage'
require 'pipeline/util/string'
include Pipeline
require 'wx'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--rebuild', '-b', OS::Getopt::BOOLEAN, 'force rebuild.' ],
	[ '--recursive', '-r', OS::Getopt::BOOLEAN, 'recurse down specified directory.' ],
    [ '--no-content', '-n', OS::Getopt::BOOLEAN, 'do not load content tree.' ],
	[ '--showlog', '-l', OS::Getopt::BOOLEAN, 'display log window.' ],
	[ '--debug', '-d', OS::Getopt::BOOLEAN, 'debug information.' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING = {
	'files' => 'files to convert.'
}

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------

begin
	g_AppName = OS::Path::get_filename( __FILE__ )
	g_Config = Pipeline::Config::instance( )
	g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )

	g_Rebuild = g_Opts['rebuild'].nil? ? false : true
	g_Recursive = g_Opts['recursive'].nil? ? false : true
	g_ShowLog = g_Opts['showlog'].nil? ? false : true
	g_Debug = g_Opts['debug'].nil? ? false : true
    g_LoadContent = g_Opts['no-content'].nil? ? true : false
	
	# Force log output
	Pipeline::Config::instance()::log_trace = g_Debug
	Pipeline::Config::instance()::log_level = 1 if ( g_Debug )
	g_Log = Log.new( g_AppName )
	
	if ( g_Opts['help'] ) then
		puts OS::Getopt::usage( OPTIONS, TRAILING )
		exit( 1 )
	end

	g_Log.info( "Rebuild: #{g_Rebuild}." )
	g_Filenames = []
	# Here we ensure we have absolute filenames, relative filenames being
	# expanded based on the current working path.
	g_Trailing.each_with_index do |filename, index|
		filename = File::expand_path( filename )
		if ( File::file?( filename ) ) then
			g_Filenames << OS::Path::normalise( filename )
		elsif ( File::directory?( filename ) ) then
			content_node = ProjectUtil::data_content_for_files( g_Config.project, filename ).shift
			if nil == content_node then 
				# Here we find all files in the directory and recurse if asked.
				if ( g_Recursive ) then
					g_Filenames += OS::FindEx::find_files_recurse( OS::Path::combine( filename, '*.*' ) )
				else            
					g_Filenames += OS::FindEx::find_files( OS::Path::combine( filename, '*.*' ) )
				end
			else
				# If the directory is content, pass it directly through to the conversion system
				g_Filenames << filename
			end
		end
		
		throw IOError.new( "File #{filename} does not exist.  Aborting convert." ) \
			unless ( File::exists?( filename ) )
	end
	
	# Log all files to be converted.
	g_Filenames.each do |filename|
		g_Log.info( "File #{filename} will be converted." )
	end

	# Now that we have a set of files to convert, we need to find the associated
	# project which we do based on the root path of the file.  If we cannot
	# find the project then we raise an exception so its displayed to the user.
	#
	# Or rather thats we used to have to do, now we just call a convert function
	# and that does all the hard work for us.  This will now also handle XGE
	# if its installed and enabled, falling back to local convert.  Everyone
	# WINS!
	results = {}
	if ( g_ShowLog ) then
		Pipeline::GUI::LogWindow::show_dialog( ) do
			results = ProjectUtil::data_convert_file( g_Filenames, g_Rebuild, false, g_LoadContent )
		end
	else
		results = ProjectUtil::data_convert_file( g_Filenames, g_Rebuild, false, g_LoadContent )
	end 
	exit( results[:success] ? 0 : 1 )
    
rescue Pipeline::ProjectUtil::ConvertException => ex
    g_Log.exception( ex, 'Conversion exception during convert' )
	ex.backtrace.each do |m| g_Log.error( m ); end
    filenames = ex.filenames.join("\n")
    ex.filenames.each do |filename| 
        g_Log.error( "File: '#{filename}'." )
    end
    GUI::ExceptionDialog::show_dialog( ex, "Conversion exception during convert with files:\n#{filenames}." )

rescue SystemExit => ex
	exit( ex.status )

rescue Exception => ex

	g_Log.exception( ex, "Unhandle exception during convert" )
	ex.backtrace.each do |m| g_Log.error( m ); end
	GUI::ExceptionDialog::show_dialog( ex, "#{g_AppName} unhandled exception" )

	# Only require Enter press when an exception has occurred.
	puts "\nPress Enter or close this window to continue..."
	$stdin.gets( )
end

# %RS_TOOLSLIB%/util/data_convert_file.rb
