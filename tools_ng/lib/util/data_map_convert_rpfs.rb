#
# File:: %RS_TOOLSLIB%/util/data_map_convert_rpfs.rb
# Description:: Convert all map RPFs from independent to platform.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 23 December 2010
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/log/log'
require 'pipeline/projectutil/data_convert'
require 'pipeline/projectutil/data_map'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ '--project', '-p', Getopt::REQUIRED, 'project data to fetch (e.g. jimmy, gta5).' ],
]
MOUNT = 'pack:/'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	g_AppName = OS::Path::get_basename( __FILE__ )
	g_Log = Pipeline::Log.new( g_AppName )
	g_Config = Pipeline::Config::instance()
	begin
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
		g_ProjectName = ( nil == opts['project'] ) ? '' : opts['project']
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			puts OS::Getopt.usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) then
			puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
			exit( 3 )
		end
		
		# Now for the main processing loop...
		g_Files = []
		ProjectUtil::data_map_for_each( g_Project ) do |maprpf|
			next unless ( File::exists?( maprpf.filename ) )	
			
			g_Log.info( "Processing #{maprpf.filename}..." )
			g_Files << maprpf.filename
		end
		ProjectUtil::data_convert_file( g_Files, true )
		
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")    
	end
end

# %RS_TOOLSLIB%/util/data_map_convert_rpfs.rb
