#
# File:: %RS_TOOLSLIB%/util/data_is_labelled.rb
# Description:: 
#
# Author:: Mike Wilson <mike.wilson@rockstartoronto.com>
# Date:: 04 April 2012
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_zip'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--tools', '-t', OS::Getopt::BOOLEAN, 'tools diff' ],
	[ '--build', '-b', OS::Getopt::BOOLEAN, 'build diff' ]
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

# Diff folders and return how many differ
def diff_folders( p4, log, path1, path2 )

	count = 0
	results = p4.run("diff2", "-q", path1, path2)
	results.each do |entry|
		count += 1
		log.info( "#{entry["depotFile"]} #{entry["rev"]} - #{entry["depotFile2"]} #{entry["rev2"]}" )
	end

	count
end

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	begin
		g_AppName = OS::Path::get_filename( __FILE__ )
		g_Log = Log.new( g_AppName )

		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt::getopts( OPTIONS )
		diff_tools = opts['tools'].nil? ? false : true
		diff_build = opts['build'].nil? ? false : true

		project = Pipeline::Config::instance( ).project
		project.load_config( )
		p4 = project.scm( )
		p4.connect( )
		if ( not p4.connected? ) then
			g_Log.warn( "Failed to connect to Perforce server.  Check installer config." )
			exit( 1 )
		end
		
		label_name = nil
		if diff_tools == true 
			label_name = project.labels['tools_current']
		elsif diff_build == true  
			label_name = project.labels['current']
		end
		
		g_Log.info("Targeted label: #{label_name}\n")
		
		if ( diff_tools ) then
			count = diff_folders( p4, g_Log, "#{Pipeline::Config::instance.toolsroot}...#have", "#{Pipeline::Config::instance.toolsroot}...@#{label_name}")
			g_Log.info( "\t#{count} tools files differ" )
		end

		if ( diff_build ) then
			#count = diff_folders( p4, g_Log, "#{Globals::instance().toolsroot}\\...#have", "#{Globals::instance().toolsroot}\\...@{label_name}")
			#g_Log.info( "\t#{count} build files differ" )
		end

		p4.disconnect( )

		exit( (count > 0) ? 0 : 1 )

	rescue SystemExit => ex
		exit( ex.status )
	
	rescue Exception => ex
		
		puts "Unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")      
	end 	
end

# %RS_TOOLSLIB%/util/data_is_labelled_rpf.rb
