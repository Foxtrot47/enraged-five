#
# File:: %RS_TOOLSLIB%/util/data_convert_file_preview.rb
# Description:: Attempt to intelligently convert a file, given only the local
#               independent path.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 20 December 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
Pipeline::Config::instance()::logtostdout = true
require 'pipeline/config/project'
require 'pipeline/gui/exception_dialog'
require 'pipeline/log/log'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/resourcing/convert'
require 'pipeline/resourcing/path'
require 'pipeline/resourcing/util'
require 'pipeline/util/environment'
require 'pipeline/util/rage'
require 'pipeline/util/string'
require 'pipeline/projectutil/data_convert'
include Pipeline
require 'wx'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'show usage information' ],
	[ '--branch', '-b', OS::Getopt::REQUIRED, 'project branch.' ],
    [ '--no-content', '-n', OS::Getopt::BOOLEAN, 'do not load content tree.' ],
]
TRAILING = {
	'files' => 'files to convert and put into preview directory.'
}

g_Filenames = []

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
begin
	g_AppName = OS::Path::get_filename( __FILE__ )
	g_Config = Pipeline::Config::instance( )
	g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )

	g_Log = Log.new( g_AppName )
	g_Project = g_Config.project
	g_Project.load_config()
	g_BranchName = g_Opts['branch'].nil? ? g_Project.default_branch : g_Opts['branch']
	g_Branch = g_Project.branches[g_BranchName]
    g_LoadContent = g_Opts['no-content'].nil? ? true : false
 
	if ( g_Opts['help'] ) then
		puts OS::Getopt::usage( OPTIONS, TRAILING )
		exit( 1 )
	end
	
	# Here we ensure we have absolute filenames, relative filenames being
	# expanded based on the current working path.
	g_Trailing.each_with_index do |filename, index|
		filename = File::expand_path( filename )
		next unless ( File::file?( filename ) )

		g_Filenames << OS::Path::normalise( filename )
	end
	
	# Log all files to be converted.
	g_Filenames.each do |filename|
		g_Log.info( "File #{filename} will be converted." )
	end
	
	ProjectUtil::data_convert_file( g_Filenames, false, true, g_LoadContent )


rescue Pipeline::ProjectUtil::ConvertException => ex
    g_Log.exception( ex, 'Conversion exception during convert' )
	ex.backtrace.each do |m| g_Log.error( m ); end
    filenames = ex.filenames.join("\n")
    ex.filenames.each do |filename| 
	g_Log.error( "File: '#{filename}'." )
    end
    GUI::ExceptionDialog::show_dialog( ex, "Conversion exception during convert with files:\n#{filenames}." )

rescue SystemExit => ex
	exit( ex.status )

rescue Exception => ex
	g_Log.exception( ex, "Unhandle exception during convert" )
	ex.backtrace.each do |m| g_Log.error( m ); end
	GUI::ExceptionDialog::show_dialog( ex, "#{g_AppName} unhandled exception with files:\n#{g_Filenames}." )

	# Only require Enter press when an exception has occurred.
	puts "\nPress Enter or close this window to continue..."
	$stdin.gets( )
end

# %RS_TOOLSLIB%/util/data_convert_file_preview.rb
