#
# File:: data_get_props_usage.rb
# Description::  Outputs usage on props in a given file.  Takes an optional
# output file in the commandline.
#
# Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
# Date:: 22 July 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_get_usage'
require 'pipeline/util/string'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--proj', '-p', OS::Getopt::REQUIRED, 'project' ],
	[ '--map', '-m', OS::Getopt::REQUIRED, 'name of the map to process' ],
	[ '--output', '-o', OS::Getopt::REQUIRED, 'output file' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information' ]
]

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
	begin
		g_AppName = OS::Path::get_basename( __FILE__ )
		g_Log = Log.new( g_AppName )
		g_Config = Pipeline::Config::instance( )
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
		
		if ( g_Opts['help'] )
			puts OS::Getopt::usage( OPTIONS )
			exit( 1 )
		end
		
		g_ProjectName = ( nil == g_Opts['proj'] ) ? '' : g_Opts['proj']
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) 
			puts OS::Getopt::usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end

		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) 
			puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
			exit( 4 )
		end
		
		g_Project.load_config( )
		
		# Set the target
		g_TargetName = ( nil == g_Opts['target'] ) ? 'xbox360' : g_Opts['target']
		
		# Set the output file, if none is given then use a blank string
		g_OutputFile = ( nil == g_Opts['output'] ) ? "" : OS::Path::normalise(g_Opts['output'])
		
		# If the map file exists, check that it's valid
		g_Project.load_content(nil, Pipeline::Project::ContentType::MAPS | Pipeline::Project::ContentType::OUTPUT)
		
		g_MapName = ( nil == g_Opts['map'] ) ? nil : g_Opts['map']
		throw ArgumentError.new( "Map name argument not given." ) if ( g_MapName == nil )
		
		map = g_Project.content.find_by_script("content.name == '" + g_MapName + "' and content.xml_type == 'map'")
		throw ArgumentError.new( "Map name #{g_MapName} was not found in project #{g_ProjectName}." ) if map.empty?
		throw ArgumentError.new( "Map name #{g_MapName} does not appear to be a props file." ) if map[0].exportinstances
		
		# Set IDE filename from map name
		map_rpf = g_Project.content.find_by_script("content.name == '" + g_MapName + "' and content.xml_type == 'maprpf'")
		throw RuntimeError.new( "Unable to find RPF config info for map #{g_MapName}." ) if map_rpf.empty?
		
		g_IDE = OS::Path::replace_ext( OS::Path::normalise( map_rpf[0].filename ), "ide" )
		

		#---------------------------------------------------------------------
		# Get data
		#---------------------------------------------------------------------
		start_time = Time.now()
		
		# If an output file has been given then make the output directory if it doesn't already exist
		unless g_OutputFile.empty?
			g_OutputDir = OS::Path::get_directory( g_OutputFile )
			::FileUtils::mkdir_p( g_OutputDir ) unless ::File::directory?( g_OutputDir )
		end
		
		# Get usage data on the IDE
		puts "\n\nSearching #{g_ProjectName} for the props in #{g_MapName}...\n\n"
		
		ProjectUtil::data_get_usage_IDE( g_Project, g_TargetName, g_OutputFile, g_IDE )
		
		puts "Time taken: #{Time.now() - start_time}s"
	end
end

# data_get_props_usage.rb