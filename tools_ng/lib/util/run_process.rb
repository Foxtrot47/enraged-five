#
# File:: run_process.rb
# Description:: runs a process only if not already running ( invented for rag.exe launch ) 
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 23rd April 2012
#
# Pass in the executable from which the process name will be derived.
# Returns 0 if no errors occur.
# Returns -1 if there where errors.

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'sys/proctable'
require 'pipeline/log/log'
require 'pipeline/os/getopt'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ "--help", "-h", Getopt::BOOLEAN, "display usage information." ],
	[ "--executable", "-p", Getopt::REQUIRED, "Process name." ],
]
PROCESS_EXIT_SLEEP = 5

#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------
begin
	#---------------------------------------------------------------------
	# Parse Command Line.
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] )
		puts OS::Getopt.usage( OPTIONS )
		puts ("Press Enter to continue...")
		$stdin.getc( )				
		exit( 1 )
	end	

	executable = opts['executable'] ? opts['executable'].downcase : nil
	#---------------------------------------------------------------------
	# Run process.
	#---------------------------------------------------------------------
	if (executable)
		executable =~ /.*\\(.*)/i
		process_name = $1
		puts "Searching for process named: '#{process_name}'"

		Sys::ProcTable.ps.each do |ps|
			if ps.name.downcase == process_name
				puts "INFO_MSG: Process #{process_name} found, exiting with ret code 0"
				Process.exit!(0)
			end
		end

		# no fork under windows - will this do???		
		env = Environment.new()
		run = env.subst(executable)
		puts "Running #{run} in new thread"
		Thread.new { system "#{run}" }
	end
rescue Exception => ex
	$stderr.puts "Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	exit -1
end

Process.exit! 0