#
# File:: %RS_TOOLSLIB%/util/ragebuilder/zip_pack_test.rb
# Description:: Builds a destination zip file from a source directory; the
#               source directory should have subdirectories for the zip 
#				extension, and then subdirs for each asset name containing the
#               raw files to pack, see //depot/gta5/assets/unittests/tools_test/...
#				as an example.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 13 December 2010
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/log/log'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/util/rage'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------

OPTIONS = [
	[ '--project', '-p', OS::Getopt::REQUIRED, 'project override (%RS_PROJECT% default).' ],
	[ '--source_folder', '-s', OS::Getopt::REQUIRED, 'source folder to get assets from' ],
	[ '--dest_zip', '-d', OS::Getopt::REQUIRED, 'destination zip file to write.' ],
    [ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then
	begin		
		g_AppName = OS::Path::get_basename( __FILE__ )
		g_Log = Log.new( g_AppName )
		g_Config = Pipeline::Config.instance( ) 
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
		g_ProjectName = opts['project'].nil? ? ENV['RS_PROJECT'] : opts['project']
		g_Project = g_Config.projects[g_ProjectName] 
		g_Project.load_config( )
		
		g_SourceDir = opts['source_folder']
		if ( g_SourceDir.nil? ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 2 )
		end
		
		g_Destzip = opts['dest_zip']
		if ( g_Destzip.nil? ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 3 )
		end
		
		g_R = RageUtils::new( g_Project )
		
		# Pack the RAW asset zips.
		g_AssetTypeDirs = OS::FindEx::find_dirs( g_SourceDir )
		g_AssetTypeDirs.each do |asset_type|
				
			asset_ext = OS::Path::get_parts( asset_type ).last
			asset_folders = OS::FindEx::find_dirs( asset_type )
			asset_folders.each do |asset_folder|
			
				asset_name = OS::Path::get_parts( asset_folder ).last
				output_zip = OS::Path::combine( g_SourceDir, "#{asset_name}.#{asset_ext}" )
				g_Log.info( "Building Asset: #{output_zip}" )
			
				g_R.pack.start_uncompressed( )
				asset_files = OS::FindEx::find_files( OS::Path::combine( asset_folder, '*.*' ) )
				asset_files.each do |asset_file|
					
					g_Log.info( "\tFile: #{asset_file}" )
					g_R.pack.add( asset_file, OS::Path::get_filename( asset_file ) )
				end
				g_R.pack.save( output_zip )
				g_R.pack.close( )
			end	
		end
	
		# Pack our destination zip.
		g_R.pack.start_uncompressed( )
		g_Files = OS::FindEx::find_files( OS::Path::combine( g_SourceDir, '*.*' ) )
		g_Files.each do |file|
			
			g_R.pack.add( file, OS::Path::get_filename( file ) )
		end
		g_R.pack.save( g_Destzip )
		g_R.pack.close( )
	
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
		puts "Unhandled exception: #{ex.message}"
		puts ex.backtrace.join( "\n" )
	end
end

# %RS_TOOLSLIB%/util/ragebuilder/zip_pack_test.rb
