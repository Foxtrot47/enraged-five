#
# File:: %RS_TOOLSLIB%/util/ragebuilder/rpf_pack_test.rb
# Description:: Builds a destination RPF file from a source directory; the
#               source directory should have subdirectories for the RPF 
#				extension, and then subdirs for each asset name containing the
#               raw files to pack, see //depot/gta5/assets/unittests/tools_test/...
#				as an example.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 13 December 2010
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/log/log'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_convert'
require 'pipeline/util/rage'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------

OPTIONS = [
	[ '--project', '-p', OS::Getopt::REQUIRED, 'project override (%RS_PROJECT% default).' ],
	[ '--ragebuilder_binary', '-r', OS::Getopt::REQUIRED, 'Ragebuilder executable to use.' ],
	[ '--source', '-s', OS::Getopt::REQUIRED, 'Source asset to convert.' ],
    [ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

def convert( project, log, binary, options, source_file, platform, target )

	c = Pipeline::Config::instance
	destination_file = ProjectUtil::data_convert_platform_filenames( project, project.branches[project.default_branch], source_file, target )
	
	rage_platform = RageUtils.rage_platform( platform )
	cmdline = binary
	cmdline += " #{OS::Path::combine( c.toolslib, 'util', 'ragebuilder', 'convert_file.rbs' )} -nopopups "
	
	cmdline += "-rebuild true "
	cmdline += "-build #{project.build} "
	cmdline += "-shader #{project.shaders} "
	cmdline += "-shaderdb #{OS::Path::combine( project.shaders, 'db' )} "
	cmdline += "-src #{source_file} "
	cmdline += "-dst #{destination_file} "
	cmdline += "-platform #{rage_platform} "
	cmdline += "-folder #{OS::Path::combine(c.temp, 'local')} "
	cmdline += "-temporary false "
	cmdline += "-output "
	cmdline += "-nologging 1 "
	cmdline += "#{options} "
	cmdline += "-toolsconfig #{c.toolsconfig} "
	
	log.info( "Convert: #{cmdline}" )
	system( cmdline )
end

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then
	begin		
		g_AppName = OS::Path::get_basename( __FILE__ )
		g_Log = Log.new( g_AppName )
		g_Config = Pipeline::Config.instance( ) 
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
		g_ProjectName = opts['project'].nil? ? ENV['RS_PROJECT'] : opts['project']
		g_Project = g_Config.projects[g_ProjectName] 
		g_Project.load_config( )
		
		g_Ragebuilder = opts['ragebuilder_binary']
		if ( g_Ragebuilder.nil? ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 2 )
		end
				
		g_Source = opts['source']
		if ( g_Source.nil? ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 3 )
		end
		
		g_RageConvert = RageConvertTool::parse( g_Project )
		g_Project.targets.each_pair do |platform, target|
			next unless ( target.enabled )
		
			g_Log.info( "Converting for target: #{platform}" )
			
			tool = g_RageConvert[platform]			
			convert( g_Project, g_Log, g_Ragebuilder, tool.options, g_Source, platform, target )
		end
		
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
		puts "Unhandled exception: #{ex.message}"
		puts ex.backtrace.join( "\n" )
	end
end

# %RS_TOOLSLIB%/util/ragebuilder/rpf_pack_test.rb
