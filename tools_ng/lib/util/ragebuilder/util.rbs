--
-- File:: tools/util/ragebuilder/util.rbs
-- Description:: Ragebuilder Script utilities
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 27 August 2009
--

local materialsLoaded = false

--
-- name: normalisePath
-- desc: Normalise path String to backward slashes.
--
function normalisePath( sPath )
	if (sPath == nil) then
		return nil
	end
	
	s = string.gsub(sPath , "/", "\\")
	s = string.lower(s)
	return s
end

--
-- name: normalisePathForward
-- desc: Normalise path String top forward slashes.
--
function normalisePathForward( sPath )
	if (sPath == nil) then
		return nil
	end
	
	s = string.gsub(sPath , "\\", "/")
	s = string.lower(s)
	return s
end

--
-- name: extractFileParam
-- desc:
--
function extractFileParam(sParam)
	local sValue = normalisePath(get_param(sParam))
	if (sValue == nil) then
		return nil
	end
	display(sValue)
	return string.sub(sValue, 2, -2)
end

--
-- name: registerTextureNames
-- desc:
--
function registerTextureNames()

	register_texturename("MIRROR_RT")
	register_texturename("PHONE_SCREEN")
	register_texturename("script_rt")
	register_texturename("vehicle_generic_carbody_envmap")
	
end

--
-- name: loadMaterials
-- desc: Load material information from the materials/procedural data files.
--
function loadMaterials( dirBuild, materials, procedurals )

	if (materialsLoaded == false) then	
	
		matRoot = dirBuild
		
		if ( materials == nil ) then
		    -- Default Materials Datafile.
		    materials = normalisePath( matRoot ) .. "/common/data/materials/materials.dat"
		end
		
		if (load_materialdefs(materials) == 0) then
			error("failed to load external materials")
			return false
		end
		
		if ( procedurals == nil ) then
		    procedurals = normalisePath( matRoot ) .. "/common/data/materials/procedural.meta"
		end
					
		if (load_proceduraldefs(procedurals) == 0) then
			error("failed to load internal procedurals")
			return false
		end
				
		materialsLoaded = true
	end
	
	return true
end

--
-- name: getParam
-- desc: Get additional script parameters from the Ragebuilder command line.
--
function getParam( options, key )

	index = string.find(options, key .. "!")
	
	if index == nil then
	
		return nil
	
	end
	
	value = string.sub(options,index + string.len(key) + 1)
	index = string.find(value, "#")
	
	if index ~= nil then
	
		value = string.sub(value,0,index - 1)
	end

	return value

end

--
-- name: getIdeFileList
-- desc:
--
function getIdeFileList( ideFilename )

	load_definitions(ideFilename)

	objectCount = get_num_definitions()

	ddnames = {}
	txdnames = {}
	txdobjs = {}

	for i = 1, objectCount, 1 do

		objectName = get_object_name(i)

		splitVal = string.find(objectName,"-")
		objectName = string.lower(string.sub(objectName,1,splitVal - 1))

		txdName = string.lower(get_object_txdname(i))
		ddName = string.lower(get_dd_name(i))

		filename = ""

		if ddName == "null" then

			filename = objectName .. ".idr"

		else

			filename = ddName .. ".idd"

			if ddnames[ddName] == nil then

				ddnames[ddName] = 1

			else

				txdName = "null"

			end

		end

		if txdName ~= "null" then

			txdName = txdName .. ".itd"

			if txdnames[txdName] == nil then

				txdnames[txdName] = { filename }

			else 

				table.insert(txdnames[txdName],filename)

			end

		end

	end

	filelist = {}

	for key, value in txdnames do

		table.insert(filelist,key)

		for key2, value2 in value do

			table.insert(filelist,value2)

		end

	end

	return filelist	
end


--
-- name: findIdeFile
-- desc:
--
function findIdeFile( mapName, rootDir )

	searchfile = rootDir .. "/" .. mapName .. ".ide"
	
	if file_exists(searchfile) then 
		return searchfile
	end

	foundDirs = find_dirs(rootDir)

	for key, value in foundDirs do

		foundDir = rootDir .. "/" .. value
		
		retval = findIdeFile(mapName, foundDir)
		
		if retval ~= "" then
		
			return retval
		
		end

	end	

	return ""

end

--
-- name: getOptimisedFileOrder
-- desc:
--
function getOptimisedFileOrder( image_filename )

	files = {}

	image_directory = remove_name_from_path( image_filename )
	image_basename = remove_extension_from_path( get_filename_from_path( image_filename ) )
	idefilename = findIdeFile( image_basename, image_directory )

	if ( "" == idefilename ) then

		print( "Optimising Image File Order:" )

		-- do some sorting if this isnt a map image

		objnames = {}
		addedfiles = {}
		local filelist = find_files("image:/*.*",true,false)
		
		progress_size(table.getn(filelist))
		
		count = 0
		
		for key, value in filelist do	
		
			progress_pos(count)
			count = count + 1
		
			objname = remove_extension_from_path(value)
		
			if objnames[objname] == nil then

				objnames[objname] = 1

				tdfilename = objname .. ".itd"
				if file_exists("image:/" .. tdfilename) then 			
					addedfiles[tdfilename] = 1
					table.insert(files,tdfilename)
				end
				
				drfilename = objname .. ".idr"
				if file_exists("image:/" .. drfilename) then 
					addedfiles[drfilename] = 1
					table.insert(files,drfilename)
				end
				
				ddfilename = objname .. ".idd"
				if file_exists("image:/" .. ddfilename) then 
					addedfiles[ddfilename] = 1
					table.insert(files,ddfilename)
				end

				ftfilename = objname .. ".ift"				
				if file_exists("image:/" .. ftfilename) then 
					addedfiles[ftfilename] = 1
					table.insert(files,ftfilename)
				end
			end		
		end
		
		for key, value in filelist do	
		
			if addedfiles[value] ~= 1 then
			
				addedfiles[value] = 1
				table.insert(files,value)
			end
		end
		
	else

		print( "Optimising MAP Image File Order:" )
		filesCheck = {}

		idefiles = getIdeFileList( idefilename ) 
		otherfiles = find_files("image:/*.*",true,false)

		for key, value in otherfiles do 

			value = string.lower(value)

			extval = get_extension_from_path(value)

			if extval == ".iad" or extval == ".ibd" or extval == ".ibn" or extval == ".ipl" then 

				if filesCheck[value] == nil then

					filesCheck[value] = 1
					table.insert(files,value)
				end 
			end
		end

		for key, value in idefiles do 

			value = string.lower(value)

			-- ifts come through as idrs from the getIdeFileList function
			-- so if you cant find an idr assume it is an ift

			if file_exists("image:/" .. value) == false then 

				value = remove_extension_from_path(value) .. ".ift"				

			end

			if file_exists("image:/" .. value) == true then 

				if filesCheck[value] == nil then

					filesCheck[value] = 1
					table.insert(files,value)

				end 				
			end

		end

		for key, value in otherfiles do 

			value = string.lower(value)

			if filesCheck[value] == nil then

				filesCheck[value] = 1
				table.insert(files,value)

			end 		
		end		
	end
	
	return files
end

--
-- name: split a string using supplied pattern (http://lua-users.org/wiki/SplitJoin)
-- desc:
--
function string:split(delimiter)
  local result = { }
  local from  = 1
  local delim_from, delim_to = string.find( self, delimiter, from  )
  while delim_from do
    table.insert( result, string.sub( self, from , delim_from-1 ) )
    from  = delim_to + 1
    delim_from, delim_to = string.find( self, delimiter, from  )
  end
  table.insert( result, string.sub( self, from  ) )
  return result
end