#
# File:: data_compare_episodic_cutsprops.rb
# Description:: Performs cutsprops comparisons for the E1/E2 disc build.
#
# Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
# Date:: 23 June 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_compare_cutscene'
require 'pipeline/util/string'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--rootproj', '-r', OS::Getopt::REQUIRED, 'root control project' ],
	[ '--rootbranch', '-b', OS::Getopt::REQUIRED, 'root control project branch' ],
	[ '--epproj1', '-e', OS::Getopt::REQUIRED, 'episodic project' ],
	[ '--epbranch1', '-n', OS::Getopt::REQUIRED, 'episodic project branch' ],
	[ '--epproj2', '-e', OS::Getopt::REQUIRED, 'episodic project' ],
	[ '--epbranch2', '-n', OS::Getopt::REQUIRED, 'episodic project branch' ],
	[ '--image', '-i', OS::Getopt::REQUIRED, 'single image name to process' ],
	[ '--output', '-o', OS::Getopt::REQUIRED, 'root output directory' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information' ]
]

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
	begin
		g_AppName = OS::Path::get_basename( __FILE__ )
		g_Log = Log.new( g_AppName )
		g_Config = Pipeline::Config::instance( )
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
		
		if ( g_Opts['help'] ) then
			puts OS::Getopt::usage( OPTIONS )
			exit( 1 )
		end
		
		g_RootProjectName = ( nil == g_Opts['rootproj'] ) ? '' : g_Opts['rootproj']
		root_project_exists = ( g_Config.projects.has_key?( g_RootProjectName ) )
		if ( not root_project_exists ) then
			puts OS::Getopt::usage( OPTIONS )
			puts "\nError root project: #{g_RootProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_EpProjectName1 = ( nil == g_Opts['epproj1'] ) ? '' : g_Opts['epproj1']
		ep_project_exists = ( g_Config.projects.has_key?( g_EpProjectName1 ) )
		if ( not ep_project_exists ) then
			puts OS::Getopt::usage( OPTIONS )
			puts "\nError episodic project: #{g_EpProjectName1} does not exist or its configuration is unreadable."
			exit( 3 )
		end
		g_EpProjectName2 = ( nil == g_Opts['epproj2'] ) ? '' : g_Opts['epproj2']
		ep_project_exists = ( g_Config.projects.has_key?( g_EpProjectName2 ) )
		if ( not ep_project_exists ) then
			puts OS::Getopt::usage( OPTIONS )
			puts "\nError episodic project: #{g_EpProjectName2} does not exist or its configuration is unreadable."
			exit( 3 )
		end
		
		g_RootProject = g_Config.projects[ g_RootProjectName ]
		g_EpProject1 = g_Config.projects[ g_EpProjectName1 ]
		g_EpProject2 = g_Config.projects[ g_EpProjectName2 ]
		if ( not g_RootProject.enabled ) then
			puts "\nError root project: #{g_RootProjectName} is not enabled on this machine.  Re-run installer."
			exit( 4 )
		end
		if ( not g_EpProject1.enabled ) then
			puts "\nError episodic project: #{g_EpProjectName1} is not enabled on this machine.  Re-run installer."
			exit( 5 )
		end
		if ( not g_EpProject2.enabled ) then
			puts "\nError episodic project: #{g_EpProjectName2} is not enabled on this machine.  Re-run installer."
			exit( 5 )
		end
		
		g_RootProject.load_config( )
		g_EpProject1.load_config( )
		g_EpProject2.load_config( )
		
		g_RootBranchName = ( nil == g_Opts['rootbranch'] ) ? g_RootProject.default_branch : g_Opts['rootbranch']
		if ( not g_RootProject.branches.has_key?( g_RootBranchName ) ) then
			puts "\nError root project: #{g_RootProjectName} does not have a branch called #{g_RootBranchName}."
			exit( 6 )
		end
		g_RootBranch = g_RootProject.branches[ g_RootBranchName ]
		
		g_EpBranchName1 = ( nil == g_Opts['epbranch1'] ) ? g_EpProject1.default_branch : g_Opts['epbranch1']
		if ( not g_EpProject1.branches.has_key?( g_EpBranchName1 ) ) then
			puts "\nError episodic project: #{g_EpProjectName1} does not have a branch called #{g_EpBranchName1}."
			exit( 7 )
		end
		g_EpBranch1 = g_EpProject1.branches[ g_EpBranchName1 ]
		
		g_EpBranchName2 = ( nil == g_Opts['epbranch2'] ) ? g_EpProject2.default_branch : g_Opts['epbranch2']
		if ( not g_EpProject2.branches.has_key?( g_EpBranchName2 ) ) then
			puts "\nError episodic project: #{g_EpProjectName2} does not have a branch called #{g_EpBranchName2}."
			exit( 7 )
		end
		g_EpBranch2 = g_EpProject2.branches[ g_EpBranchName2 ]
		
		g_TargetName = ( nil == g_Opts['target'] ) ? 'xbox360' : g_Opts['target']
		g_OutputDir = ( nil == g_Opts['output'] ) ? File::expand_path( '.' ) : OS::Path::normalise(g_Opts['output'])
		g_Image = ( nil == g_Opts['image'] ) ? nil : g_Opts['image']
		
		puts "Project 1: #{g_RootProject.uiname} #{g_RootBranch.name}"
		puts "Project 2: #{g_EpProject1.uiname} #{g_EpBranch1.name}"
		puts "Project 2: #{g_EpProject2.uiname} #{g_EpBranch2.name}"
		
		#---------------------------------------------------------------------
		# Comparison
		#---------------------------------------------------------------------
		start_time = Time.now()
		
		#Make the output directory if it doesn't already exist
		::FileUtils::mkdir_p( g_OutputDir ) unless ( ::File::directory?( g_OutputDir ) )
		
		#Compare cutscene data
		puts "\nComparing cutsprops data...\n\n"
		ProjectUtil::data_compare_episodic_cutsprops_data( g_RootBranch, g_EpBranch1, g_EpBranch2, g_TargetName, g_OutputDir ) do |filename, new_filename|
			
			# Copy files from the comparison to the output directory
			dst_filename = OS::Path::combine( 'x:', 'gta_e1', 'build', 'dev_disc', 'xbox360', 'independent', 'anim', OS::Path::get_filename( new_filename ) )
			dst_path = OS::Path::get_directory( dst_filename )
			FileUtils::mkdir_p( dst_path ) unless ( ::File::directory?( dst_path ) )
			
			puts "Copying file: #{filename}"
			puts "\tto #{dst_filename}"
			puts "FILENAME: #{filename}"
			puts "NEW     : #{new_filename}"
			puts "DST     : #{dst_filename}"
			if ( ::File::exists?( new_filename ) ) then
				FileUtils::rm(dst_filename) if ::File::exists?( dst_filename )
				FileUtils::cp( new_filename, dst_filename, :preserve => true )
				FileUtils::chmod( 0666, dst_filename )
			else
				
				puts "WARNING: #{filename} does not exist for copying."
			end
		end #data_compare_episodic_cutscene_data
		
		puts "Time taken: #{Time.now() - start_time}s"
	end
end

# data_compare_episodic_cutsprops.rb