@ECHO OFF
REM data_compare_gta4_instances_to_text.bat
REM David Muir <david.muir@rockstarnorth.com>
REM 11 August 2009
REM

SET XSLT_TOOL=%TOOLSBIN%\bin\nxslt2.exe
SET XSLT_SRC=%TOOLSBIN%\util\compare\data\gta4_episode_instances.xslt
SET SRC_DIR=x:\streamE2\maps
SET OUT_DIR=x:\streamE2\maps

ECHO Converting XML to TXT...

FOR /F "usebackq" %%A IN (`dir /b %SRC_DIR%\*_instances.xml`) DO (
	ECHO Processing %SRC_DIR%\%%A...
	
	%XSLT_TOOL% %SRC_DIR%\%%A %XSLT_SRC% -o %OUT_DIR%\%%A.txt
)



REM X:\streamE2\maps>nxslt2 bronx_e_episode_instances.xml x:\pipedev\tools\util\comp
REM are\data\gta4_episode_instances.xslt -o bronx_e_episode_instances.txt


ECHO Done.
PAUSE
