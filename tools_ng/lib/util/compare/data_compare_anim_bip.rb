#
# File:: data_compare_anim_clip.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 23 January 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/gui/log_window'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ],
	[ '--debug', '-d', OS::Getopt::BOOLEAN, 'include log tracing information.' ],
	[ '--bips', '-b', OS::Getopt::REQUIRED, 'BIP root folder.' ],
	[ '--anim', '-a', OS::Getopt::REQUIRED, 'Anim root folder,' ],
]

#----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_ProjectName = ''
		g_Project = nil
		g_Branch = ''
		g_Config = Pipeline::Config.instance( ) 
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
		g_Debug = opts['debug'].nil? ? false : true
		puts "DEBUG: #{g_Debug}"
		Pipeline::Config::instance()::log_trace = g_Debug
		
		g_PathBIP = OS::Path::normalise( opts['bips'] )
		if ( not File::directory?( g_PathBIP ) ) then
			puts "Error: invalid BIP root directory specified."
			puts OS::Getopt::usage( OPTIONS )
			exit( 2 )
		end
		g_PathAnim = OS::Path::normalise( opts['anim'] )
		if ( not File::directory?( g_PathAnim ) ) then
			puts "Error: invalid ANIM root directory specified."
			puts OS::Getopt::usage( OPTIONS )
			exit( 3 )
		end		

		# Force log output
		Pipeline::Config::instance()::log_level = 2
		Pipeline::Config::instance()::log_level = 1 if ( g_Debug )
		g_Log = Log.new( g_AppName )

		#---------------------------------------------------------------------
		# Process directory
		#---------------------------------------------------------------------
		GUI::LogWindow::show_dialog( ) do
			anim_files = OS::FindEx::find_files_recurse( OS::Path::combine( g_PathAnim, '*.anim' ) )
			anim_files.each do |filename|
				# Check we have a BIP or XAF
				bip_path = OS::Path::replace_ext( filename.sub( g_PathAnim, g_PathBIP ), 'bip' )
				xaf_path = OS::Path::replace_ext( filename.sub( g_PathAnim, g_PathBIP ), 'xaf' )
				g_Log.error( "BIP/XAF file #{bip_path}/.xaf does not exist for ANIM file #{filename}." ) \
					if ( not ( File::exists?( bip_path ) || File::exists?( xaf_path ) ) )
			end

			bip_files = OS::FindEx::find_files_recurse( OS::Path::combine( g_PathBIP, '*.bip' ) )
			bip_files.each do |filename|
				# Check we have an ANIM
				anim_path = OS::Path::replace_ext( filename.sub( g_PathBIP, g_PathAnim ), 'anim' )
				g_Log.error( "ANIM file #{anim_path} does not exist for BIP file #{filename}." ) \
					if ( not File::exists?( anim_path ) )
			end

			xaf_files = OS::FindEx::find_files_recurse( OS::Path::combine( g_PathBIP, '*.xaf' ) )
			xaf_files.each do |filename|
				# Check we have an ANIM
				anim_path = OS::Path::replace_ext( filename.sub( g_PathBIP, g_PathAnim ), 'anim' )
				g_Log.error( "ANIM file #{anim_path} does not exist for XAF file #{filename}." ) \
					if ( not File::exists?( anim_path ) )
			end
		end

	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")      
	end 
end

# data_compare_anim_clip.rb
