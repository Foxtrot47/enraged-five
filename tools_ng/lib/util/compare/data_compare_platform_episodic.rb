#
# File:: data_compare_platform_episodic.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 4 November 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_compare_map'
require 'pipeline/util/string'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--rootproj', '-r', OS::Getopt::REQUIRED, 'root control project' ],
	[ '--rootbranch', '-b', OS::Getopt::REQUIRED, 'root control project branch' ],
	[ '--epproj', '-e', OS::Getopt::REQUIRED, 'episodic project' ],
	[ '--epbranch', '-n', OS::Getopt::REQUIRED, 'episodic project branch' ],
	[ '--image', '-i', OS::Getopt::REQUIRED, 'single image name to process' ],
	[ '--output', '-o', OS::Getopt::REQUIRED, 'root output directory' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information' ]
]

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
	begin
		g_AppName = OS::Path::get_basename( __FILE__ )
		g_Log = Log.new( g_AppName )
		g_Config = Pipeline::Config::instance( )
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
		
#		FOR DEBUGGING...
#		g_Opts = {}
#		g_Opts['rootproj'] = 'gta4'
#		g_Opts['epproj'] = 'gta_e1'
#		g_Opts['output'] = 'x:/compare_output'
		
		if ( g_Opts['help'] ) then
			puts OS::Getopt::usage( OPTIONS )
			exit( 1 )
		end
		
		g_RootProjectName = ( nil == g_Opts['rootproj'] ) ? '' : g_Opts['rootproj']
		root_project_exists = ( g_Config.projects.has_key?( g_RootProjectName ) )
		if ( not root_project_exists ) then
			puts OS::Getopt::usage( OPTIONS )
			puts "\nError root project: #{g_RootProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_EpProjectName = ( nil == g_Opts['epproj'] ) ? '' : g_Opts['epproj']
		ep_project_exists = ( g_Config.projects.has_key?( g_EpProjectName ) )
		if ( not ep_project_exists ) then
			puts OS::Getopt::usage( OPTIONS )
			puts "\nError episodic project: #{g_EpProjectName} does not exist or its configuration is unreadable."
			exit( 3 )
		end
		
		g_RootProject = g_Config.projects[ g_RootProjectName ]
		g_EpProject = g_Config.projects[ g_EpProjectName ]
		if ( not g_RootProject.enabled ) then
			puts "\nError root project: #{g_RootProjectName} is not enabled on this machine.  Re-run installer."
			exit( 4 )
		end
		if ( not g_EpProject.enabled ) then
			puts "\nError episodic project: #{g_EpProjectName} is not enabled on this machine.  Re-run installer."
			exit( 5 )
		end
		
		g_RootProject.load_config( )
		g_EpProject.load_config( )
		
		g_RootBranchName = ( nil == g_Opts['rootbranch'] ) ? g_RootProject.default_branch : g_Opts['rootbranch']
		if ( not g_RootProject.branches.has_key?( g_RootBranchName ) ) then
			puts "\nError root project: #{g_RootProjectName} does not have a branch called #{g_RootBranchName}."
			exit( 6 )
		end
		g_RootBranch = g_RootProject.branches[ g_RootBranchName ]
		
		g_EpBranchName = ( nil == g_Opts['epbranch'] ) ? g_EpProject.default_branch : g_Opts['epbranch']
		if ( not g_EpProject.branches.has_key?( g_EpBranchName ) ) then
			puts "\nError episodic project: #{g_EpProjectName} does not have a branch called #{g_EpBranchName}."
			exit( 7 )
		end
		g_EpBranch = g_EpProject.branches[ g_EpBranchName ]
		
		g_TargetName = ( nil == g_Opts['target'] ) ? 'xbox360' : g_Opts['target']
		g_OutputDir = ( nil == g_Opts['output'] ) ? File::expand_path( '.' ) : g_Opts['output']
		g_Image = ( nil == g_Opts['image'] ) ? nil : g_Opts['image']
				
		#---------------------------------------------------------------------
		# Comparison
		#---------------------------------------------------------------------
		
		start_time = Time.now()
		p4 = g_EpProject.scm()
		ProjectUtil::data_compare_episodic_map_data( g_RootBranch, g_EpBranch, g_TargetName, g_OutputDir, g_Image ) do |filename, new_filename|
			
			filename_parts = OS::Path::get_directory( filename ).split( '/' )
			dst_filename = OS::Path::combine( g_EpBranch.build, 'independent', 'data', 'maps', filename_parts.last, OS::Path::get_filename( new_filename ) )
			dst_path = OS::Path::get_directory( dst_filename )
			FileUtils::mkdir_p( dst_path ) unless ( ::File::directory?( dst_path ) )
			
			p4.connect() unless ( p4.connected? )
			p4.run_edit( dst_filename )

			puts "Copying file: #{filename}"
			puts "\tto #{dst_filename}"
			
			puts "FILENAME: #{filename}"
			puts "NEW     : #{new_filename}"
			puts "DST     : #{dst_filename}"
			if ( ::File::exists?( new_filename ) ) then
				
				FileUtils::cp( new_filename, dst_filename, :preserve => true )
				FileUtils::chmod( 0666, dst_filename )
			else
				
				puts "WARNING: #{filename} does not exist for copying."
			end
		end #end of data_compare_episodic_map_data call
		
		p4.disconnect( )
		puts "Time taken: #{Time.now() - start_time}s"
	
	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
	
		puts "Unhandled exception: #{ex.message}"
		puts ex.backtrace.join( "\n" )
	end	
end

# data_compare_platform_episodic.rb
