#
# File:: data_compare_merge_ide_ipl.rb
# Description::  Performs map comparisons on IDEs/IPLs for the E1/E2 disc build.
#
# Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
# Date:: 12 August 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_compare_merge'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--rootproj', '-r', OS::Getopt::REQUIRED, 'root control project' ],
	[ '--rootbranch', '-b', OS::Getopt::REQUIRED, 'root control project branch' ],
	[ '--epproj', '-e', OS::Getopt::REQUIRED, 'episodic project' ],
	[ '--epbranch', '-n', OS::Getopt::REQUIRED, 'episodic project branch' ],
	[ '--output', '-o', OS::Getopt::REQUIRED, 'root output directory' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information' ]
]

# Files which should be ignored during processing
EXCLUDE_FILES = ['public_3']

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
	begin
		g_AppName = OS::Path::get_basename( __FILE__ )
		g_Log = Log.new( g_AppName )
		g_Config = Pipeline::Config::instance( )
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
		
		if ( g_Opts['help'] ) then
			puts OS::Getopt::usage( OPTIONS )
			exit( 1 )
		end
		
		g_RootProjectName = ( nil == g_Opts['rootproj'] ) ? '' : g_Opts['rootproj']
		root_project_exists = ( g_Config.projects.has_key?( g_RootProjectName ) )
		if ( not root_project_exists ) then
			puts OS::Getopt::usage( OPTIONS )
			puts "\nError root project: #{g_RootProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_EpProjectName = ( nil == g_Opts['epproj'] ) ? '' : g_Opts['epproj']
		ep_project_exists = ( g_Config.projects.has_key?( g_EpProjectName ) )
		if ( not ep_project_exists ) then
			puts OS::Getopt::usage( OPTIONS )
			puts "\nError episodic project: #{g_EpProjectName} does not exist or its configuration is unreadable."
			exit( 3 )
		end
		
		g_RootProject = g_Config.projects[ g_RootProjectName ]
		g_EpProject = g_Config.projects[ g_EpProjectName ]
		if ( not g_RootProject.enabled ) then
			puts "\nError root project: #{g_RootProjectName} is not enabled on this machine.  Re-run installer."
			exit( 4 )
		end
		if ( not g_EpProject.enabled ) then
			puts "\nError episodic project: #{g_EpProjectName} is not enabled on this machine.  Re-run installer."
			exit( 5 )
		end
		
		g_RootProject.load_config( )
		g_EpProject.load_config( )
		
		g_RootBranchName = ( nil == g_Opts['rootbranch'] ) ? g_RootProject.default_branch : g_Opts['rootbranch']
		if ( not g_RootProject.branches.has_key?( g_RootBranchName ) ) then
			puts "\nError root project: #{g_RootProjectName} does not have a branch called #{g_RootBranchName}."
			exit( 6 )
		end
		g_RootBranch = g_RootProject.branches[ g_RootBranchName ]
		
		g_EpBranchName = ( nil == g_Opts['epbranch'] ) ? g_EpProject.default_branch : g_Opts['epbranch']
		if ( not g_EpProject.branches.has_key?( g_EpBranchName ) ) then
			puts "\nError episodic project: #{g_EpProjectName} does not have a branch called #{g_EpBranchName}."
			exit( 7 )
		end
		g_EpBranch = g_EpProject.branches[ g_EpBranchName ]
		
		# Set the output directory, if given (the default is the root)
		g_OutputDir = ( nil == g_Opts['output'] ) ? File::expand_path( '.' ) : OS::Path::normalise(g_Opts['output'])
		
		puts "Project 1: #{g_RootProject.uiname} #{g_RootBranch.name}"
		puts "Project 2: #{g_EpProject.uiname} #{g_EpBranch.name}"
		
		#---------------------------------------------------------------------
		# Comparison
		#---------------------------------------------------------------------
		start_time = Time.now()
		
		# Make the output directory if it doesn't already exist
		::FileUtils::mkdir_p( g_OutputDir ) unless ( ::File::directory?( g_OutputDir ) )
		
		# Set up and start the builder
		new_rpf = OS::Path::combine( g_OutputDir, "xbox360.rpf" )
		
		# Compare packed xbox data
		puts "\nComparing map IDEs and IPLs...\n\n"
		
		ProjectUtil::data_compare_merge_ide_ipl( g_RootBranch, g_EpBranch, g_OutputDir, EXCLUDE_FILES ) do |merge_dir, renamed_list|
			# Get merged files and iterate through them
			merge_files = OS::FindEx::find_files_recurse( OS::Path::combine( merge_dir, '*.*' ) )
			
			merge_files.each do |filename|
				# Copy files from the comparison to the output directory
				dst_filename = filename.gsub( merge_dir, OS::Path::combine( 'x:', 'gta_e1', 'build', 'dev_disc', 'xbox360', 'independent', 'data', 'maps' ) )
				dst_path = OS::Path::get_directory( dst_filename )
				FileUtils::mkdir_p( dst_path ) unless ( ::File::directory?( dst_path ) )
				
				#puts "Copying file: #{filename}"
				#puts "\tto #{dst_filename}"
				puts "FILENAME: #{filename}"
				puts "NEW     : #{filename}"
				puts "DST     : #{dst_filename}"
				if ( ::File::exists?( filename ) ) then
					FileUtils::rm( dst_filename) if ::File::exists?( dst_filename )
					FileUtils::cp( filename, dst_filename, :preserve => true )
					FileUtils::chmod( 0666, dst_filename )
				else
					
					puts "WARNING: #{filename} does not exist for copying."
				end
			end #merge_files.each
			
			puts "\nWrote renamed file list to #{renamed_list}."
		end #data_compare_merge_ide_ipl
		
		# Copy over episodic-only files
		puts "Post-Processing: copying episodic-only files..."
		ep_files = OS::FindEx::find_files( OS::Path::combine( g_EpBranch.independent, 'data', 'maps', 'props', '*.ide' ) )
		ep_files.concat( OS::FindEx::find_files( OS::Path::combine( g_EpBranch.independent, 'data', 'maps', 'props', '*.ipl' ) ) )
		ep_files.concat( OS::FindEx::find_files( OS::Path::combine( g_EpBranch.independent, 'data', 'maps', 'interiors', '*.ide' ) ) )
		ep_files.concat( OS::FindEx::find_files( OS::Path::combine( g_EpBranch.independent, 'data', 'maps', 'interiors', '*.ipl' ) ) )
		ep_files.delete_if { |file| EXCLUDE_FILES.include?( OS::Path::get_basename( file ) ) }
		ep_files.each do |new_filename|
			# Construct destination path
			dst_filename = new_filename.gsub( OS::Path::combine( g_EpBranch.independent, 'data', 'maps' ), OS::Path::combine( 'x:', 'gta_e1', 'build', 'dev_disc', 'xbox360', 'independent', 'data', 'maps' ) )
			file = OS::Path::get_filename( dst_filename )
			dst_filename = OS::Path::combine( OS::Path::get_directory( dst_filename ), 'extra', file )
			dst_path = OS::Path::get_directory( dst_filename )
			FileUtils::mkdir_p( dst_path ) unless ( ::File::directory?( dst_path ) )
			
			# Copy episodic files to output directory
			puts "Copying file: #{new_filename}"
			puts "\tto #{dst_filename}"
			puts "FILENAME: #{new_filename}"
			puts "NEW     : #{new_filename}"
			puts "DST     : #{dst_filename}"
			if ( ::File::exists?( new_filename ) ) then
				FileUtils::rm(dst_filename) if ::File::exists?( dst_filename )
				FileUtils::cp( new_filename, dst_filename, :preserve => true )
				FileUtils::chmod( 0666, dst_filename )
			else
				
				puts "WARNING: #{filename} does not exist for copying."
			end
		end #ep_files.each
		
		puts "Time taken: #{Time.now() - start_time}s"
	end
end

# data_compare_merge_ide_ipl.rb