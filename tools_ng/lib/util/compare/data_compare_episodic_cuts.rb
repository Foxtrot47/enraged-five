#
# File:: data_compare_episodic_cuts.rb
# Description:: Performs cutscene comparisons for the E1/E2 disc build.
#
# Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
# Date:: 23 June 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_compare_cutscene'
require 'pipeline/util/string'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--rootproj', '-r', OS::Getopt::REQUIRED, 'root control project' ],
	[ '--rootbranch', '-b', OS::Getopt::REQUIRED, 'root control project branch' ],
	[ '--epproj', '-e', OS::Getopt::REQUIRED, 'episodic project' ],
	[ '--epbranch', '-n', OS::Getopt::REQUIRED, 'episodic project branch' ],
	[ '--image', '-i', OS::Getopt::REQUIRED, 'single image name to process' ],
	[ '--output', '-o', OS::Getopt::REQUIRED, 'root output directory' ],
	[ '--template', '-p', OS::Getopt::REQUIRED, 'template for .iad files' ],
	[ '--target', '-t', OS::Getopt::REQUIRED, 'target platform' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information' ]
]

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
	begin
		g_AppName = OS::Path::get_basename( __FILE__ )
		g_Log = Log.new( g_AppName )
		g_Config = Pipeline::Config::instance( )
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
		
		if ( g_Opts['help'] ) then
			puts OS::Getopt::usage( OPTIONS )
			exit( 1 )
		end
		
		g_RootProjectName = ( nil == g_Opts['rootproj'] ) ? '' : g_Opts['rootproj']
		root_project_exists = ( g_Config.projects.has_key?( g_RootProjectName ) )
		if ( not root_project_exists ) then
			puts OS::Getopt::usage( OPTIONS )
			puts "\nError root project: #{g_RootProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_EpProjectName = ( nil == g_Opts['epproj'] ) ? '' : g_Opts['epproj']
		ep_project_exists = ( g_Config.projects.has_key?( g_EpProjectName ) )
		if ( not ep_project_exists ) then
			puts OS::Getopt::usage( OPTIONS )
			puts "\nError episodic project: #{g_EpProjectName} does not exist or its configuration is unreadable."
			exit( 3 )
		end
		
		g_RootProject = g_Config.projects[ g_RootProjectName ]
		g_EpProject = g_Config.projects[ g_EpProjectName ]
		if ( not g_RootProject.enabled ) then
			puts "\nError root project: #{g_RootProjectName} is not enabled on this machine.  Re-run installer."
			exit( 4 )
		end
		if ( not g_EpProject.enabled ) then
			puts "\nError episodic project: #{g_EpProjectName} is not enabled on this machine.  Re-run installer."
			exit( 5 )
		end
		
		g_RootProject.load_config( )
		g_EpProject.load_config( )
		
		g_RootBranchName = ( nil == g_Opts['rootbranch'] ) ? g_RootProject.default_branch : g_Opts['rootbranch']
		if ( not g_RootProject.branches.has_key?( g_RootBranchName ) ) then
			puts "\nError root project: #{g_RootProjectName} does not have a branch called #{g_RootBranchName}."
			exit( 6 )
		end
		g_RootBranch = g_RootProject.branches[ g_RootBranchName ]
		
		g_EpBranchName = ( nil == g_Opts['epbranch'] ) ? g_EpProject.default_branch : g_Opts['epbranch']
		if ( not g_EpProject.branches.has_key?( g_EpBranchName ) ) then
			puts "\nError episodic project: #{g_EpProjectName} does not have a branch called #{g_EpBranchName}."
			exit( 7 )
		end
		g_EpBranch = g_EpProject.branches[ g_EpBranchName ]
		
		g_Template = OS::Path::normalise(g_Opts['template'])
		unless File.exist?(g_Template) then
			puts "\nError template file: #{g_Template} does not exist."
			exit( 8 )
		end
		
		g_TargetName = ( nil == g_Opts['target'] ) ? 'xbox360' : g_Opts['target']
		g_OutputDir = ( nil == g_Opts['output'] ) ? File::expand_path( '.' ) : OS::Path::normalise(g_Opts['output'])
		g_Image = ( nil == g_Opts['image'] ) ? nil : g_Opts['image']
		
		puts "Project 1: #{g_RootProject.uiname} #{g_RootBranch.name}"
		puts "Project 2: #{g_EpProject.uiname} #{g_EpBranch.name}"
		
		#---------------------------------------------------------------------
		# Comparison
		#---------------------------------------------------------------------
		start_time = Time.now()
		
		#Make the output directory if it doesn't already exist
		::FileUtils::mkdir_p( g_OutputDir ) unless ( ::File::directory?( g_OutputDir ) )
		
		#Compare cutscene data
		puts "\nComparing cutscene data...\n\n"
		ProjectUtil::data_compare_episodic_cuts_data( g_RootBranch, g_EpBranch, g_TargetName, g_OutputDir, g_Template ) do |filename, new_filename|
			
			# Copy files from the comparison to the output directory
			dst_filename = OS::Path::combine( g_OutputDir, 'independent', 'anim', OS::Path::get_filename( new_filename ) )
			dst_path = OS::Path::get_directory( dst_filename )
			FileUtils::mkdir_p( dst_path ) unless ( ::File::directory?( dst_path ) )
			
			puts "Copying file: #{filename}"
			puts "\tto #{dst_filename}"
			puts "FILENAME: #{filename}"
			puts "NEW     : #{new_filename}"
			puts "DST     : #{dst_filename}"
			if ( ::File::exists?( new_filename ) ) then
				FileUtils::rm(dst_filename) if ::File::exists?( dst_filename )
				FileUtils::cp( new_filename, dst_filename, :preserve => true )
				FileUtils::chmod( 0666, dst_filename )
			else
				
				puts "WARNING: #{filename} does not exist for copying."
			end
		end #data_compare_episodic_cutscene_data
		
		puts "Time taken: #{Time.now() - start_time}s"
	end
end

# data_compare_episodic_cuts.rb