<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes"/>
  <xsl:template match="/">

    GTA4 Episode 1: The Lost and Damned
    Map Objects

    -------------------------------------------------------------------------
    New Objects
    -------------------------------------------------------------------------
    <xsl:for-each select="episodic_instances/episode_1/instances_new/instance">
      <xsl:value-of select="@name"/> at 
      <xsl:value-of select="@tx"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="@ty"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="@tz"/>
      <xsl:text> 

      </xsl:text>
    </xsl:for-each>
    -------------------------------------------------------------------------

    -------------------------------------------------------------------------
    Removed Objects
    -------------------------------------------------------------------------
    <xsl:for-each select="episodic_instances/episode_1/instances_removed/instance">
      <xsl:value-of select="@name"/> at
      <xsl:value-of select="@tx"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="@ty"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="@tz"/>
      <xsl:text> 

      </xsl:text>
    </xsl:for-each>

    -------------------------------------------------------------------------


    GTA4 Episode 2: The Ballad of Gay Tony
    Map Objects

    -------------------------------------------------------------------------
    New Objects
    -------------------------------------------------------------------------
    <xsl:for-each select="episodic_instances/episode_1/instances_new/instance">
      <xsl:value-of select="@name"/> at
      <xsl:value-of select="@tx"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="@ty"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="@tz"/>
      <xsl:text> 

      </xsl:text>
    </xsl:for-each>


    -------------------------------------------------------------------------
    Replaced Objects
    -------------------------------------------------------------------------
    <xsl:for-each select="episodic_instances/episode_2/instances_removed/instance">
      <xsl:value-of select="@name"/> at
      <xsl:value-of select="@tx"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="@ty"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="@tz"/>
      <xsl:text> 

      </xsl:text>
    </xsl:for-each>

    -------------------------------------------------------------------------

  </xsl:template>
</xsl:stylesheet>