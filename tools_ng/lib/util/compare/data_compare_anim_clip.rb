#
# File:: data_compare_anim_clip.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 23 January 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/gui/log_window'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ],
	[ '--debug', '-d', OS::Getopt::BOOLEAN, 'include log tracing information.' ]
]
TRAILING_DESC = { 
	'directory' => 'root directory to compare for anim and clip files.'
}

#----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_ProjectName = ''
		g_Project = nil
		g_Branch = ''
		g_Config = Pipeline::Config.instance( ) 
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
			exit( 1 )
		end
		g_Debug = opts['debug'].nil? ? false : true
		puts "DEBUG: #{g_Debug}"
		Pipeline::Config::instance()::log_trace = g_Debug
		
		if ( trailing.size != 1 ) then
			puts "Only one path supported, #{trailing.size} given."
			puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
			exit( 2 )
		end
		g_Path = OS::Path::normalise( trailing[0] )

		# Force log output
		Pipeline::Config::instance()::log_level = 2
		Pipeline::Config::instance()::log_level = 1 if ( g_Debug )
		puts "LEVEL: #{Pipeline::Config::instance()::log_level}"	
		g_Log = Log.new( g_AppName )

		#---------------------------------------------------------------------
		# Process directory
		#---------------------------------------------------------------------
		GUI::LogWindow::show_dialog( ) do
			files = OS::FindEx::find_files_recurse( OS::Path::combine( g_Path, '*.*' ), true )
			g_Log.info( "#{files.size} files found." )

			files.each_with_index do |filename, i|
				g_Log.debug( "Process File: #{filename}" )
				
				puts "File: #{i} of #{files.size}"
				search_filename = nil
				ext = OS::Path::get_extension( filename )
				directory = OS::Path::get_directory( filename )
				basename = OS::Path::get_basename( filename )
				
				# If we have a .clip find a .anim, or vice versa.
				case ext
				when 'clip'
					search_filename = OS::Path::combine( directory, basename + '.anim' )
				when 'anim'
					search_filename = OS::Path::combine( directory, basename + '.clip' )
				end

				if ( search_filename.nil? ) then					
					# Skip to next filename as we only verify clip/anim pairs.
					g_Log.debug( "Skipping #{filename} as its not a .clip or .anim." )
				else
					g_Log.debug( "Searching for #{search_filename}..." )
					g_Log.error( "#{search_filename} does not exist for #{filename}!" ) \
						unless ( files.index( search_filename ) )
				end
			end
		end

	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")      
	end 
end

# data_compare_anim_clip.rb
