#
# File:: data_compare_gta4_episode_ipls.rb
# Description:: Compare GTA4/E1/E2 IPL files to find added/removed objects.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 30 July 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/gui/log_window'
require 'pipeline/math/vector2'
require 'pipeline/math/vector3'
require 'pipeline/math/quat'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/util/environment'
require 'pipeline/util/rage'
include Pipeline
include Pipeline::Math
require 'REXML/document'
require 'tempfile'
include REXML
require 'pipeline/util/rexml_write_fix'

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ],
	[ '--debug', '-d', OS::Getopt::BOOLEAN, 'include log tracing information.' ],
	[ '--map', '-m', OS::Getopt::REQUIRED, 'only run for the specific map.' ],
	[ '--output', '-o', OS::Getopt::REQUIRED, 'output directory.' ]
]
MOUNT_POINT = 'image:/'

#----------------------------------------------------------------------------
# Globals
#----------------------------------------------------------------------------
$g_Log = nil
$g_ProjGTA4 = nil
$g_ProjE1 = nil
$g_ProjE2 = nil
$g_EnvGTA4 = Environment.new()
$g_EnvE1 = Environment.new()
$g_EnvE2 = Environment.new()
$g_MapIndPath = '$(independent)/data/maps/'
$g_MapArtPath = '$(art)/models/'

#----------------------------------------------------------------------------
# Classes
#----------------------------------------------------------------------------

#
# == Description
# IPL instance class, used to store IPL instance parameters for later 
# reporting.
#
class Inst
	attr_reader :name			# Object name
	attr_reader :pos			# Object position
	attr_reader :rot			# Object rotation
	attr_reader :ipl_filename	# Source of instance

	EPSILON = 0.01

	# Class constructor.
	def initialize( name, pos, rot, ipl_filename )
		@name = name
		@pos = pos
		@rot = rot
		@ipl_filename = ipl_filename
	end

	def feq( f1, f2 )
		return true if ( ( ( f1 + EPSILON ) >= f2 ) and ( ( f1 - EPSILON ) <= f2 ) ) 
		false
	end

	# Equality operator.
	def ==( other )
		result = true
		if ( not other.is_a?( Inst ) ) then
			result = false
		elsif ( 0 != @name.casecmp( other.name ) ) then
			result = false
		elsif ( not ( feq( @pos.x, other.pos.x ) and feq( @pos.y, other.pos.y ) and feq( @pos.z, other.pos.z ) ) ) then
			result = false 
		elsif ( not ( feq( @rot.x, other.rot.x ) and feq( @rot.y, other.rot.y ) and feq( @rot.z, other.rot.z ) and feq( @rot.w, other.rot.w ) ) ) then
			result = false
		else
			result = true
		end
		result
	end

	# Return REXML::Element representation.
	def to_xml( )
		inst_elem = Element.new( 'instance' )
		inst_elem.attributes << Attribute.new( 'name', @name )
		inst_elem.attributes << Attribute.new( 'tx', @pos.x.to_s )
		inst_elem.attributes << Attribute.new( 'ty', @pos.y.to_s )
		inst_elem.attributes << Attribute.new( 'tz', @pos.z.to_s )
		inst_elem.attributes << Attribute.new( 'qx', @rot.x.to_s )
		inst_elem.attributes << Attribute.new( 'qy', @rot.y.to_s )
		inst_elem.attributes << Attribute.new( 'qz', @rot.z.to_s )
		inst_elem.attributes << Attribute.new( 'qw', @rot.w.to_s )
		inst_elem.attributes << Attribute.new( 'ipl_filename', @ipl_filename )
		inst_elem
	end

	# Constructor (from String IPL file line)
	def Inst::from_s( line, ipl_filename )
		# <name>, <flags>, <x>, <y>, <z>, <qx>, <qy>, <qz>, <qw>, ...
		parts = line.split( ',' )
		name = parts[0]
		pos = Vector3.new( parts[2].to_f, parts[3].to_f, parts[4].to_f )
		rot = Quat.new( parts[5].to_f, parts[6].to_f, parts[7].to_f, parts[8].to_f )
		Inst.new( name, pos, rot, ipl_filename )
	end
end

#
# == Description
# CarGen class, used to store IPL car gen parameters for later 
# reporting.
#
class CarGen
	attr_reader :pos			# Object position (Vector3)
	attr_reader :vec1x
	attr_reader :vec1y
	attr_reader :vec2len
	attr_reader :modelid
	attr_reader :colour1
	attr_reader :colour2
	attr_reader :colour3
	attr_reader :colour4
	attr_reader :flags
	attr_reader :alarm_chance
	attr_reader :locked_chance
	attr_reader :ipl_filename	# Source of instance
	
	EPSILON = 0.01
	
	# Class constructor.
	def initialize( pos, vec1x, vec1y, vec2len, modelid, col1, col2, col3, col4, flags, alarm, locked, ipl_filename )
		@pos = pos
		@vec1x = vec1x
		@vec1y = vec1y
		@vec2len = vec2len
		@modelid = modelid
		@colour1 = col1
		@colour2 = col2
		@colour3 = col3
		@colour4 = col4
		@flags = flags
		@alarm_chance = alarm
		@locked_chance = locked
		@ipl_filename = ipl_filename
	end
	
	def feq( f1, f2 )
		return true if ( ( ( f1 + EPSILON ) >= f2 ) and ( ( f1 - EPSILON ) <= f2 ) ) 
		false
	end
	
	# Equality operator.
	def ==( other )
		result = true
		if ( not other.is_a?( CarGen ) ) then
			result = false
		elsif ( not ( feq( @pos.x, other.pos.x ) and feq( @pos.y, other.pos.y ) and feq( @pos.z, other.pos.z ) ) ) then
			result = false 
		elsif ( not ( feq( @vec1x, other.vec1x ) and feq( @vec1y, other.vec1y ) and feq( @vec2len, other.vec2len ) ) ) then
			result = false
		elsif ( ( @colour1 != other.colour1 ) or ( @colour2 != other.colour2 ) or ( @colour3 != other.colour3 ) or ( @colour4 != other.colour4 ) ) then
			result = false
		elsif ( flags != other.flags ) then
			result = false
		elsif ( not ( feq( @alarm_chance, other.alarm_chance ) and feq( @locked_chance, other.locked_chance ) ) ) then
			result = false 
		else
			result = true
		end
		result
	end
	
	# Return REXML::Element representation.
	def to_xml( )
		inst_elem = Element.new( 'cargen' )
		inst_elem.attributes << Attribute.new( 'tx', @pos.x.to_s )
		inst_elem.attributes << Attribute.new( 'ty', @pos.y.to_s )
		inst_elem.attributes << Attribute.new( 'tz', @pos.z.to_s )
		inst_elem.attributes << Attribute.new( 'ipl_filename', @ipl_filename )
		inst_elem
	end
	
	# Constructor (from String IPL file line)
	def CarGen::from_s( line, ipl_filename )
		# <x>, <y>, <z>, vec1x, vec1y, vec2length, modelid, colour1, colour2, colour3, colour4, flags, alarmChance, lockedChance
		parts = line.split( ',' )
		pos = Vector3.new( parts[0].to_f, parts[1].to_f, parts[2].to_f )
		vec1x = parts[3].to_f
		vec1y = parts[4].to_f
		vec2len = parts[5].to_f
		modelid = parts[6].to_i
		col1 = parts[7].to_i
		col2 = parts[8].to_i
		col3 = parts[9].to_i
		col4 = parts[10].to_i
		flags = parts[11].to_i
		alarm = parts[12].to_f
		locked = parts[13].to_f
		CarGen.new( pos, vec1x, vec1y, vec2len, modelid, col1, col2, col3, col4, flags, alarm, locked, ipl_filename )
	end
end


#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

# Extract IPL base filename from Tempfile IPL filename.
def extract_ipl_filename( ipl_filename )
	ipl_file = OS::Path::get_filename( ipl_filename )
	ipl_file.slice( 0, ipl_file.index( '.' ) )
end

# Return Array of Inst and CarGen objects for an IPL file.
def get_map_ipl_instances_from_ipl( ipl_filename )

	throw RuntimeError.new() if ( OS::Path::get_extension( ipl_filename ) == 'img' )

	insts = []
	File::open( ipl_filename ) do |fp|
		text = fp.readlines( )
			
		in_insts = false
		in_cars = false
		text.each do |line|
			
			in_insts = true if ( 0 == 'inst'.casecmp( line.strip ) )
			in_cars = true if ( 0 == 'cars'.casecmp( line.strip ) )
			in_insts = false if ( 0 == 'end'.casecmp( line.strip ) )
			in_cars = false if ( 0 == 'end'.casecmp( line.strip ) )
			next unless ( in_insts or in_cars )
			next if ( 0 == 'inst'.casecmp( line.strip ) )
			next if ( 0 == 'cars'.casecmp( line.strip ) )
			
			ipl_file = extract_ipl_filename( ipl_filename )
			insts << Inst::from_s( line, ipl_file ) if ( in_insts )
			insts << CarGen::from_s( line, OS::Path::get_basename( ipl_file ) ) if ( in_cars )
		end
	end
	puts "IPL: #{ipl_filename} [#{insts.size}]"
	insts
end

# Return Array of Inst files within an IMG archive.
def get_map_ipl_instances_from_img( r, image, root_image = nil )
	
	# Find root project IPLs.  We need to fall back to these in case
	# the episodic IMG doesn't contain the same number of IPLs.
	root_ipl_files = {}
	if ( not root_image.nil? ) then	
		r.image.mount( root_image, MOUNT_POINT )
		root_files = r.find_files( OS::Path::combine( MOUNT_POINT, '*.ipl' ) )
		root_files.each do |img_filename|
			
			root_ipl_filename = OS::Path::combine( OS::Path::get_temp_directory(), img_filename )
			r.util.copy_file( OS::Path::combine( MOUNT_POINT, img_filename ), root_ipl_filename )
			root_ipl_basename = extract_ipl_filename( root_ipl_filename )
			root_ipl_files[root_ipl_basename] = root_ipl_filename
		end
		r.image.unmount( root_image )
	end

	r.image.mount( image, MOUNT_POINT )
	files = r.find_files( OS::Path::combine( MOUNT_POINT, '*.ipl' ) )
	ipl_files = {}
	files.each do |ipl_filename|
		ipl_basename = extract_ipl_filename( ipl_filename )
		ipl_files[ipl_basename] = ipl_filename
	end

	insts = []
	# Copy file to temp location, parse, then delete.
	ipl_files.each_value do |img_filename|
		tf = Tempfile.new( OS::Path::get_filename( img_filename ) )
		tf.flush
		tf.close
		ipl_filename = tf.path
		
		r.util.copy_file( OS::Path::combine( MOUNT_POINT, img_filename ), ipl_filename )
		insts += get_map_ipl_instances_from_ipl( ipl_filename )
	end
	root_ipl_files.each_pair do |ipl_basename, ipl_filename|
		next if ( ipl_files.has_key?( ipl_basename ) ) # Skip if our episodic img had this file.
		
		# Otherwise parse the root project's file for instances
		puts "Parsing root project IPL for: #{ipl_basename}"
		insts += get_map_ipl_instances_from_ipl( ipl_filename )
	end

	r.image.unmount( image )

	insts
end

# Return Array of Inst objects for a map.
def get_map_ipl_instances( r, ipl_filename, image, root_image = nil )
	insts = []
	
	# Get inst objects from static IPL file
	insts += get_map_ipl_instances_from_ipl( ipl_filename )
	# Get inst objects from stream IPL files.
	insts += get_map_ipl_instances_from_img( r, image, root_image )
	puts "INSTS: #{insts.size}"
	insts
end

# Return 2-element Array of Arrays of Inst/CarGen objects that differ between 
# control_list and episode_list.
# First list is new Instances in episode, second list is removed instances in episode.
def compare_map_ipl_instances( control_list, episode_list )

	new_list = []
	removed_list = []
	episode_list.each do |episode_inst|
		if ( nil == control_list.index( episode_inst ) ) then
			new_list << episode_inst
		end
	end
	control_list.each do |control_inst|
		if ( nil == episode_list.index( control_inst ) ) then
			removed_list << control_inst
		end
	end
	[ new_list, removed_list ]
end

# Serialise Array of Inst objects to XML node.
def write_inst_list( xml_node, inst_list )
	inst_list.each do |inst|
		xml_node << inst.to_xml()
	end
end

# Process a single map image (e.g. manhat/manhat09)
def process_map( r, mapname, outputdir, log )
	
	gta4_img = OS::Path::combine( $g_EnvGTA4.subst( $g_MapIndPath ), mapname ) + '.img'
	gta4_ipl = OS::Path::combine( $g_EnvGTA4.subst( $g_MapIndPath ), mapname ) + '.ipl'
	e1_img = OS::Path::combine( $g_EnvE1.subst( $g_MapIndPath ), mapname ) + '.img'
	e1_ipl = OS::Path::combine( $g_EnvE1.subst( $g_MapIndPath ), mapname ) + '.ipl'
	e2_img = OS::Path::combine( $g_EnvE2.subst( $g_MapIndPath ), mapname ) + '.img'
	e2_ipl = OS::Path::combine( $g_EnvE2.subst( $g_MapIndPath ), mapname ) + '.ipl'
	
	gta4_insts = []
	e1_insts = []
	e1_new_insts = []
	e1_removed_insts = []
	e2_insts = []
	e2_new_insts = []
	e2_removed_insts = []

	log.info( "Processing #{mapname}..." )
	if ( not ( File::exists?( e1_img ) or File::exists?( e2_img ) ) ) then
		log.warn( 'Ignoring map because it doesn\'t exist in either episodic project.' )
		return []
	end
	
	if ( File::exists?( gta4_img ) and File::exists?( gta4_ipl ) ) then
		gta4_insts = get_map_ipl_instances( r, gta4_ipl, gta4_img )
	end
	if ( File::exists?( e1_img ) and File::exists?( e1_ipl ) ) then
		e1_insts = get_map_ipl_instances( r, e1_ipl, e1_img, gta4_img )
		data = compare_map_ipl_instances( gta4_insts, e1_insts )
		e1_new_insts = data[0]
		e1_removed_insts = data[1]
	end	
	if ( File::exists?( e2_img ) and File::exists?( e2_ipl ) ) then
		e2_insts = get_map_ipl_instances( r, e2_ipl, e2_img, gta4_img )
		data = compare_map_ipl_instances( gta4_insts, e2_insts )
		e2_new_insts = data[0]
		e2_removed_insts = data[1]
	end
	
	# Write out XML data for loading in MaxScript later.
	xml_file = OS::Path::combine( outputdir, OS::Path::get_basename( mapname ) ) + '_episode_instances.xml'
	xml_doc = Document.new( )
	xml_doc << XMLDecl.new( )
	
	xml_doc << root = Element.new( 'episodic_instances' )
	root << e1_elem = Element.new( 'episode_1' )
	root << e2_elem = Element.new( 'episode_2' )
	
	e1_elem << e1_new_elem = Element.new( 'instances_new' )
	e1_elem << e1_removed_elem = Element.new( 'instances_removed' )
	e2_elem << e2_new_elem = Element.new( 'instances_new' )
	e2_elem << e2_removed_elem = Element.new( 'instances_removed' )
	
	write_inst_list( e1_new_elem, e1_new_insts )
	write_inst_list( e1_removed_elem, e1_removed_insts )
	write_inst_list( e2_new_elem, e2_new_insts )
	write_inst_list( e2_removed_elem, e2_removed_insts )
	
	File::open( xml_file, 'w' ) do |fp|
		fmt = REXML::Formatters::Pretty.new()
		fmt.write( xml_doc, fp )  
	end
end

#
# Process all map images.
#
def process_all( r, outputdir, log )
	log.info( 'Processing all maps...' )
	gta4_path = $g_EnvGTA4.subst( $g_MapIndPath ) + "*.img"
	
	# Find all GTA4 map img files (get relative so they can be passed
	# straight into the +process_map+ function.
	map_files = OS::FindEx::find_files_recurse( gta4_path, true )
	log.info( "#{map_files.size} maps to analyse." )
	map_files.each do |mapname|
		process_map( r, mapname.sub( '.img', '' ), outputdir, log )
	end
end

#----------------------------------------------------------------------------
# Entry-Point
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

	begin
		g_AppName = OS::Path::get_basename( __FILE__ )
		g_Config = Pipeline::Config.instance( ) 
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		g_Options, g_Trailing = OS::Getopt::getopts( OPTIONS )
		if ( g_Options['help'] ) then
			puts OS::Getopt::usage( OPTIONS )
			exit( 1 )
		end
		g_Debug = g_Options['debug'].nil? ? false : true
		puts "DEBUG: #{g_Debug}" if ( g_Debug )
		g_Config::log_trace = g_Debug
		g_OutputDir = g_Options['output'].nil? ? OS::Path::get_directory( __FILE__ ) : OS::Path::normalise( g_Options['output'] )
		
		# See if we have a single map to process
		g_Map = g_Options['map'].nil? ? nil : g_Options['map']
		
		# Force log output
		Pipeline::Config::instance()::log_level = 2
		Pipeline::Config::instance()::log_level = 1 if ( g_Debug )
		puts "LEVEL: #{Pipeline::Config::instance()::log_level}" if ( g_Debug )
		$g_Log = Log.new( g_AppName )
		$g_Log.info( "Output directory: #{g_OutputDir}" )
				
		#-----------------------------------------------------------------
		# Initialise our global data
		#-----------------------------------------------------------------
		$g_ProjGTA4 = g_Config.projects['gta4']
		$g_ProjGTA4.load_config()
		$g_ProjGTA4.fill_env( $g_EnvGTA4 ) 
		$g_Log.info( "#{$g_ProjGTA4.uiname} configuration loaded." )
		$g_ProjE1 = g_Config.projects['gta_e1']
		$g_ProjE1.load_config()
		$g_ProjE1.fill_env( $g_EnvE1 ) 
		$g_Log.info( "#{$g_ProjE1.uiname} configuration loaded." )
		$g_ProjE2 = g_Config.projects['gta_e2']		
		$g_ProjE2.load_config()	
		$g_ProjE2.fill_env( $g_EnvE2 ) 	
		$g_Log.info( "#{$g_ProjE2.uiname} configuration loaded." )
		
		r = RageUtils.new( g_Config.projects['jimmy'] )
		
		if ( not g_Map.nil? ) then
			process_map( r, g_Map, g_OutputDir, $g_Log )
		else
			process_all( r, g_OutputDir, $g_Log )
		end
		$g_Log.info( "Done." )
		
	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")
	end 
end

# data_compare_gta4_episode_ipls.rb
