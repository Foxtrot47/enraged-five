#
# File:: export_map_scenexml_batch.rb
# Description:: Invoke batch map SceneXml export for a project
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 30 January 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/log/log'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ '--project', '-p', Getopt::REQUIRED, 'project data to fetch (e.g. jimmy, gta4_jpn).' ],
	[ '--path', '-a', Getopt::REQUIRED, 'project path to find max files.' ],
]

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_ProjectName = ''
		g_Project = nil
		g_Config = Pipeline::Config.instance( ) 
		g_Log = Pipeline::Log.new( g_AppName )

		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
		
		g_ProjectName = ( nil == opts['project'] ) ? '' : opts['project']
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			puts OS::Getopt.usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) then
			puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
			exit( 3 )
		end
		g_Path = ( nil == opts['path'] ) ? g_Project.root : OS::Path::normalise( opts['path'] )
		
		# Find all max files under project root (could use content system!)
		maxfiles = OS::FindEx::find_files_recurse( OS::Path::combine( g_Path, "*.max" ) )
		Dir::chdir( "x:\\pipedev\\tools\\dcc\\current\\max2009\\scripts\\pipeline\\export\\maps\\" ) do
			g_Log.info( "Current directory: #{Dir.pwd}" )
			g_Log.info( "Starting batch export with #{maxfiles.size} files..." )
				maxfiles.each do |filename|		
					g_Log.info( "Starting 3dsmax for #{filename}..." )
					system( "c:\\3dsmax2009\\3dsmax.exe", filename, "-silent", "-U", "MAXScript", "scenexml_batch.ms" )
				g_Log.info( "Done." )
			end
			g_Log.info( "Finished." )
		end		

	rescue Exception => ex
        exit( ex.status ) if ( 'exit' == ex.message )
		
        puts "\n#{g_AppName} unhandled exception: #{ex.message}"
        puts ex.backtrace.join("\n\t")      
	end
end

# export_map_scenexml_batch.rb
