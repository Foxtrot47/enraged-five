#
# File:: util/data_file_sizes.rb
# Description:: "Somewhat" equivalent to the GNU 'du' utility.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 1 September 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--ignore', '-i', OS::Getopt::REQUIRED, 'ignore paths.' ],
	[ '--kilobytes', '-k', OS::Getopt::BOOLEAN, 'change units to kilobytes.' ],
	[ '--report', '-r', OS::Getopt::REQUIRED, 'write output report to file (tab delimited).' ],
	[ '--debug', '-d', OS::Getopt::BOOLEAN, 'debug information.' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING = { 'paths' => 'paths to produce file data sizes for.' }

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

# Return the size (in bytes) of a path name (recursively)
def get_size( path )

	throw ArgumentError.new( "Invalid path #{path}." ) \
		unless ( File::exists?( path ) )
	sizes = 0

	if ( File::directory?( path ) ) then
		size = 0
		files = OS::FindEx::find_files_recurse( OS::Path::combine( path, '*.*' ) )
		files.each do |filename|
			size += File::size( filename )
		end
		sizes = size

	elsif ( File::file?( path ) ) then
		sizes = File::size?( path )
	end
	sizes
end

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
begin
	g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
	
	g_Kilobytes = g_Opts['kilobytes'].nil? ? false : true
	g_Megabytes = ( not g_Kilobytes )
	g_Debug = g_Opts['debug'].nil? ? false : true
	
	if ( g_Opts['help'] or ( 0 == g_Trailing.size ) ) then
		puts OS::Getopt::usage( OPTIONS )
		exit( 1 )
	end
	
	g_Trailing.each do |filename|
		filename = OS::Path::normalise( filename )
		if ( not File::exists?( filename ) ) then
			puts "ERROR: path #{filename} does not exist. Ignoring."
			next
		end

		fullpath = File::expand_path( filename )
		if ( File::directory?( fullpath ) ) then
			puts "#{filename}\t#{get_size( fullpath ) / 1024}" if ( g_Kilobytes )
			puts "#{filename}\t#{get_size( fullpath ) / 1024 / 1024}" if ( g_Megabytes )
		elsif ( File::file?( fullpath ) ) then
			puts "#{filename}\t#{get_size( fullpath ) / 1024}" if ( g_Kilobytes )
			puts "#{filename}\t#{get_size( fullpath ) / 1024 / 1024}" if ( g_Megabytes )
		end
	end

rescue SystemExit => ex
	exit( ex.status )
	
rescue Exception => ex
	puts "Unhandled exception: #{ex.message}."
	ex.backtrace.each do |m| puts m; end
end

# util/data_file_sizes.rb
