#
# File:: file_get_maxscript_usage.rb
# Description::  Outputs usage on MaxScript (.ms) files.  If no tools root
#				 is given then it just checks the system settings.
# NOTE THAT FOR NOW THIS ONLY REPORTS ON UNUSED ITEMS.
#
# Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
# Date:: 5 August 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/util/string'
require 'pipeline/projectutil/file_get_usage.rb'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--output', '-o', OS::Getopt::REQUIRED, 'output file' ],
	[ '--toolsroot', '-t', OS::Getopt::REQUIRED, 'optional tools root to check' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information' ]
]

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
	begin
		g_AppName = OS::Path::get_basename( __FILE__ )
		g_Log = Log.new( g_AppName )
		g_Config = Pipeline::Config::instance( )
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
		
		if ( g_Opts['help'] )
			puts OS::Getopt::usage( OPTIONS )
			exit( 1 )
		end 
		
		# Set the output file, if none is given then use a blank string
		g_OutputFile = ( nil == g_Opts['output'] ) ? "" : OS::Path::normalise(g_Opts['output'])
		
		# Set the tools root, if none is given then use the system settings
		g_ToolsRoot = ( g_Opts['toolsroot'].nil? ) ? Pipeline::Config::instance().toolsroot : OS::Path::normalise(g_Opts['toolsroot'])
		
		#---------------------------------------------------------------------
		# Get data
		#---------------------------------------------------------------------
		start_time = Time.now()
		
		# If an output file has been given then make the output directory if it doesn't already exist
		unless g_OutputFile.empty?
			g_OutputDir = OS::Path::get_directory( g_OutputFile )
			::FileUtils::mkdir_p( g_OutputDir ) unless ::File::directory?( g_OutputDir )
		end
		
		# Get usage data
		puts "\n\nGetting usage data on MaxScript files...\n\n"
		
		ProjectUtil::file_get_unused_maxscript( g_ToolsRoot, g_OutputFile )
		
		puts "Time taken: #{Time.now() - start_time}s"
	end
end

# file_get_maxscript_usage.rb