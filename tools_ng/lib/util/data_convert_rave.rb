#
# File:: util/data_convert_rave.rb
# Description:: RAVE XML Data Conversion
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 13 November 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/projectutil/rave/data_convert'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--showlog', '-l', OS::Getopt::BOOLEAN, 'display log window.' ],
	[ '--debug', '-d', OS::Getopt::BOOLEAN, 'debug information.' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING = {
	'files' => 'RAVE XML files to convert.'
}

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
# Entry-Point
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	g_AppName = OS::Path::get_basename( __FILE__ )
	g_Config = Pipeline::Config::instance( )
	g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
	
	# Options
	g_ShowLog = g_Opts['showlog'].nil? ? false : true
	g_Debug = g_Opts['debug'].nil? ? false : true
	
	# Force log output
	g_Config::logtostdout = true
	g_Config::log_trace = g_Debug
	g_Config::log_level = 2
	g_Config::log_level = 1 if ( g_Debug )
	g_Log = Log.new( g_AppName )
	
	if ( g_Opts['help'] ) then
		puts OS::Getopt::usage( OPTIONS, TRAILING )
		exit( 1 )
	end
	
	begin
		g_Filenames = []
		g_Trailing.each_with_index do |filename, index|
			filename = File::expand_path( filename )
			g_Filenames << filename
		end
		
		# Log all files to be converted.
		g_Filenames.each do |filename|
			g_Log.info( "File #{filename} will be converted." )	
		end
		
		if ( g_ShowLog ) then
			Pipeline::GUI::LogWindow::show_dialog( ) do
				ProjectUtil::RAVE::data_convert_file( g_Filenames )	
			end
		else
			ProjectUtil::RAVE::data_convert_file( g_Filenames )
		end
		
	rescue SystemExit => ex
		exit( ex.status )
		
	rescue Exception => ex
		g_Log.exception( ex, "Unhandled exception during RAVE convert" )
		ex.backtrace.each do |m| g_Log.error( m ); end
		GUI::ExceptionDialog::show_dialog( ex, "#{AppName} unhandled exception" )
		
		# Only require Enter press when an exception has occurred.
		puts "\nPress Enter or close this window to continue..."
		$stdin.gets( )
	end
end

# util/data_convert_rave.rb
