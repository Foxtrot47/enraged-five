#
# File:: data_delete_tree.rb
# Description:: Delete a complete directory tree structure.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 17 July 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/log/log'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--recursive', '-r', OS::Getopt::BOOLEAN, 'recursively delete stuff.' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING_DESC = { 
    'paths' => 'files and directories to delete (recursive).'
}

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

	g_AppName = File::basename( __FILE__, '.rb' )
	g_Log = Log.new( g_AppName )
	g_Config = Pipeline::Config::instance( )

begin
	g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
	g_Recursive = g_Opts['recursive'].nil? ? false : true

	
	if ( g_Opts['help'] ) then
		puts OS::Getopt::usage( OPTIONS, TRAILING_DESC )
		exit( 1 )
	end
	if ( 0 == g_Trailing.size ) then
		puts "No files/paths specified.  Aborting."
		puts OS::Getopt::usage( OPTIONS, TRAILING_DESC )
		exit( 2 )
	end

	g_Filenames = []
	# Here we ensure we have absolute filenames, relative filenames being
	# expanded based on the current working path.
	g_Trailing.each_with_index do |filename, index|
		filename = File::expand_path( filename )
		g_Filenames << filename
	end

	# Delete
	g_Filenames.each do |filename|

		begin
			if ( g_Recursive ) then
				puts "Note: #{filename}"
				FileUtils::rm_rf( filename )
			else
				FileUtils::rm( filename )
			end
		rescue Exception => ex
			puts "Error: #{ex.message}"
		end
	end
	
rescue Exception => ex
	exit( ex.status ) if ( 'exit' == ex.message )

	g_Log.exception( ex, "Unhandle exception during delete" )
	ex.backtrace.each do |m| g_Log.error( m ); end

	puts "Unhandled exception during delete: #{ex.message}"
	ex.backtrace.each do |m| puts m; end

	# Only require Enter press when an exception has occurred.
	puts "\nPress Enter or close this window to continue..."
	$stdin.gets( )
	
	exit( 1 ) # show failure.
end
end

# data_convert_file.rb
