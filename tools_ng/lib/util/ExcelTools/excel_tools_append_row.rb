# File:: excel_append.rb
# Description:: adds a line of text to an XLS MS Excel spreadsheet
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 07 October 2010
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'win32ole'
require 'pipeline/os/path'
require 'time'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'fileutils'
include FileUtils
require 'dl'
include Pipeline
require 'pipeline/log/log'
require 'util/ExcelTools/excel_tools'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS =	[
			[ '--xlsfilename', 	'-x', Getopt::REQUIRED, 'destination xls filename' ],
			[ '--datafilename', 	'-d', Getopt::REQUIRED, 'source filename from which data is extracted for insertion' ],
			[ '--versionfilename', 	'-v', Getopt::REQUIRED, 'filename from which version is extracted for insertion' ],
			[ '--ps3bin', 		'-p', Getopt::REQUIRED, 'where the ps3bin.exe executable exists' ],
			[ '--bin', 		'-b', Getopt::REQUIRED, 'the binary' ]			
		]

INFO			= "[colourise=black]INFO_MSG: "
INFO_BLUE		= "[colourise=blue]INFO_MSG: "

# keys that appear as output from ps3bin -dsi ( update if ever changes )

KEY_TEXT		= "Text (Code)"
KEY_DATA		= "Data"
KEY_RODATA		= "RO-Data"
KEY_BSS			= "BSS (Uninitialised data)"
KEY_TOTAL		= "Total"

KEYS 			= [ KEY_TEXT, KEY_DATA, KEY_RODATA, KEY_BSS, KEY_TOTAL ]

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

	#-----------------------------------------------------------------------------
	# Code
	#-----------------------------------------------------------------------------
	begin
		#-------------------------------------------------------------------------
		# Entry-Point
		#-------------------------------------------------------------------------

		g_AppName = File::basename( __FILE__, '.rb' )
		g_xls_filename = ''
		g_Config = Pipeline::Config.instance()
		g_Log = Log.new( 'exceltools' )

		puts g_Config.sc_server
		#-------------------------------------------------------------------------
		# Parse Command Line
		#-------------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			response = message_box( "#{g_AppName} will exit.",g_AppName, BUTTONS_OK, ICON_QUESTION)
			exit( 1 )
		end


		g_xls_filename 		= ( nil == opts['xlsfilename'] ) ? '' : opts['xlsfilename']
		g_data_filename 	= ( nil == opts['datafilename'] ) ? '' : opts['datafilename']
		g_version_filename 	= ( nil == opts['versionfilename'] ) ? 'X:\gta5\build\dev\common\data\version.txt' : opts['versionfilename']
		g_ps3bin_filename 	= ( nil == opts['ps3bin'] ) ? 'X:\ps3sdk\dev\usr\local\340_001\cell\host-win32\sn\bin\ps3bin.exe' : opts['ps3bin']
		g_bin_filename 		= ( nil == opts['bin'] ) ? nil : opts['bin']

		if g_data_filename and not File.exist? g_data_filename
			$stderr.puts "Error : #{g_data_filename} does not exist."
			exit( 2 )
		end

		if g_version_filename and not File.exist? g_version_filename
			$stderr.puts "Error : #{g_version_filename} does not exist."
			exit( 3 )
		end

		if g_ps3bin_filename and not File.exist? g_ps3bin_filename 
			$stderr.puts "Error : #{g_ps3bin_filename} does not exist."
			exit( 4 )
		end

		if g_bin_filename and not File.exist? g_bin_filename 
			$stderr.puts "Error : #{g_bin_filename} does not exist."
			exit( 5 )
		end

		#
		# --- Check out file ---
		#
		puts "#{INFO_BLUE} Creating p4"
		p4 	= SCM::Perforce::create( g_Config.sc_server, g_Config.sc_username, g_Config.sc_workspace )
		p4_rage = SCM::Perforce::create( g_Config.sc_rage_server, g_Config.sc_rage_username, g_Config.sc_rage_workspace )
		
		puts "#{INFO_BLUE} Connecting to p4 servers"
		raise Exception if not p4
		p4.connect( )
		raise Exception if not p4.connected?

		raise Exception if not p4_rage
		p4_rage.connect( )
		raise Exception if not p4_rage.connected?

		if (g_version_filename)
			puts "#{INFO_BLUE} Syncing to version file."
			depot_filename = p4.local2depot(g_version_filename)
			p4.run_sync( g_version_filename )
		end

		if (g_ps3bin_filename)
			puts "#{INFO_BLUE} Syncing to ps3bin file."
			depot_filename = p4_rage.local2depot( g_ps3bin_filename )
			p4_rage.run_sync( g_ps3bin_filename )
		end
			
		puts "#{INFO_BLUE} Creating CL"
		change_id = p4.create_changelist( "Automated Build of Game Stats @ #{Time.now}" )
		raise Exception if change_id.nil?
		puts "#{INFO_BLUE}Changelist #{change_id} is created"

		puts "#{INFO_BLUE} Syncing to memory XLS file."
		depot_filename = p4.local2depot(g_xls_filename)
		p4.run_sync( depot_filename )
		
		puts "#{INFO_BLUE}Checking out #{depot_filename} in CL #{change_id.to_s}"
		puts "Checking out #{depot_filename} in CL #{change_id.to_s}"
		p4.run_edit( '-c', change_id.to_s, depot_filename )			
		
		#
		# --- Append the row ---
		#
		puts "AppendRow to #{g_xls_filename}"
		if (g_data_filename)
			# --- Search for data ---
			buildstate_regexp = [ ]

			puts "#{INFO_BLUE} building regexps"
			
			buildstate_strings = [ 	"Date", "Build", 
						"CL Game", "CL Rage", 
						"Text Size", "Data Size", "RO-Data Size", "BSS Size", "Total", 
						"GH Total", "GH Used", "GH Left", 
						"Default", "Animation", "Streaming", "World", "Gameplay", "FX", "Rendering", "Physics", "Audio", "Network", "System", "Scaleform", "Bounds", "Pools", "AI", "Process", "Pathfinding"
					     ]

			buildstate_strings.each do |str|
				buildstate_regexp << Regexp.new("$[AssetTest] BuildState:#{str}=(.*)^")
			end

			buildstate_regexp.each do |regexp|
				puts "#{INFO_BLUE} Regexp #{regexp.to_s}"	
			end

			puts "#{INFO_BLUE} opening #{g_data_filename}"
			File.open(g_data_filename) do |file| 
				buildstate_regexp.each_with_index do |regexp, idx|
					file.each_line do |line|					
						if line =~ regexp
							puts "#{INFO_BLUE} Matched #{line}"
							trailing[idx] = $1 
							break
						end
					end					
				end
			end
	
			#--- derive missing data ---
			trailing[0] = Time.now.strftime("%d/%m/%Y") if trailing[0].nil?
			
			if trailing[1].nil?
				puts "#{INFO_BLUE} Opening #{g_version_filename}"
				File.open(g_version_filename) do |file|
					
					capture = false
					file.each_line do |line|
						if capture 
							puts "#{INFO_BLUE} Version read #{line}"
							trailing[1] = line
							break
						end
						puts line 
						capture = true if line.include?"[VERSION_NUMBER]"								
					end
				end	
			end

			filespec = "//..."

			if (trailing[2].nil?)	
				puts "#{INFO_BLUE} Deriving latest CL on P4"				
				change = p4.run_changes( "-m1", "#{filespec}@#{p4.client}" )			
				trailing[2] = change[0]['change'] if (change.length > 0)
				puts "#{INFO_BLUE} CL game #{trailing[2]}"
			end
			
			if (trailing[3].nil?)					
				puts "#{INFO_BLUE} Deriving latest CL on P4 for Rage"
				change = p4_rage.run_changes( "-m1", "#{filespec}@#{p4_rage.client}" )			
				trailing[3] = change[0]['change'] if (change.length > 0)
				puts "#{INFO_BLUE} CL rage #{trailing[3]}"
			end

			if (	trailing[4].nil? or 
				trailing[5].nil? or 
				trailing[6].nil? or 
				trailing[7].nil? or 
				trailing[8].nil?)

				cmd = "#{g_ps3bin_filename} #{g_bin_filename} -dsi"
				puts "#{INFO_BLUE} Executing #{cmd}...\n"
				status, stdout, stderr = systemu(cmd)
				puts "#{INFO_BLUE} \n...Completed"
				
				$stderr.puts( cmd ) if ( stderr.length > 0 )
		
				stderr.each do |err|
					error_count += 1
					$stderr.puts "Error: #{err}"
				end
		
				stdout.each_with_index do |out, idx|									
					puts out
					if (idx==1)
						vals = out.split
					
						hash = {}
						KEYS.each_with_index do |key,idx|
							puts "#{INFO_BLUE} Bin stats : #{key} = #{vals[idx]}" 
							hash[key] = vals[idx]
						end

						trailing[4] = hash[KEY_TEXT]
						trailing[5] = hash[KEY_DATA]
						trailing[6] = hash[KEY_RODATA]
						trailing[7] = hash[KEY_BSS]
						trailing[8] = hash[KEY_TOTAL]
					end
				end
			end

			buildstate_strings.each_with_index do |str, idx|
				trailing[idx] = "0" if trailing[idx].nil? 
				puts "#{INFO_BLUE} #{str} #{trailing[idx]}"
			end	
		end		
		

		# -- finally add this row to the spreadsheet ---
		ExcelTools::AppendRow(g_xls_filename, trailing)

	rescue Exception => ex

		puts "#{g_AppName} unhandled exception: #{ex.message}"
		puts "Call stack:"
		puts ex.backtrace.join( "\n\t" )
	end
=begin

		#
		# --- Submit changes ---
		#
		puts "#{INFO_BLUE}Reverting unchanged files #{@change_id}"
		p4.run_revert( '-a', '-c', change_id.to_s, '//...')

		files = p4.run_opened( '-c', change_id.to_s )
		raise Exception if files.nil?

		puts "#{INFO_BLUE}There are #{files.size} files to submit."
		files.each do |file|
			puts "#{INFO_BLUE}#{file['depotFile']} has been updated and will be submitted."			
		end

		if ( files.size > 0 )		
			puts "#{INFO_BLUE}Submitting File currently in #{@change_id}"	
			submit_result = p4.run_submit( '-c', change_id.to_s )
			puts submit_result.to_s
		elsif ( 0 == files.size )
			puts "#{INFO_BLUE}Deleting #{change_id} no files changed."
			p4.run_change('-d', change_id.to_s)
		end

	rescue Exception => ex

		puts "#{g_AppName} unhandled exception: #{ex.message}"
		puts "Call stack:"
		puts ex.backtrace.join( "\n\t" )
	end		

# Exceltools.rb
=end