#
# File:: integrate_tools.rb
# Description:: Copy across required data massaging tools from the head of rage and submit them into our local tool chain
#				Not sure if we're going to use this but its here if we need it
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 5 March 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/scm/perforce'
include Pipeline

srcpath = "X:/jimmy/src/dev/rage/tools/"
destpath = "X:/pipedev/tools/dcc/current/motionbuilder2009/"
description = "Integrating RAGE cutscene pipeline tools into North tool chain"

c = Pipeline::Config.instance
p4_local = SCM::Perforce::create( c.sc_server, c.sc_username, c.sc_workspace )
p4_rage = SCM::Perforce::create( c.sc_rage_server, c.sc_rage_username, c.sc_rage_workspace )
#p4 = Perforce::create( "rsgedip4proxy01:1667", "luke.openshaw", "LUKEOPENSHAW-001-NEW" )

change_id = p4_local.create_changelist( description )

p4_rage.get_latest( "//rage/jimmy/dev/rage/tools/base/..." )
p4_rage.get_latest( "//rage/jimmy/dev/rage/tools/motionbuilder/..." )
p4_local.run_edit( '-c', change_id, "//depot/pipedev/tools/dcc/current/motionbuilder2009/tools/..." )
file_list = [ "base/moduleSettings.xml",
			"base/exes/CutScene/",
			"base/exes/animcombine.exe",
			"base/exes/animcompare.exe",
			"base/exes/animcompress.exe",
			"base/exes/animconcat.exe",
			"base/exes/animdump.exe",
			"base/exes/animedit.exe",
			"base/exes/clipcombine.exe",
			"base/exes/clipdump.exe",
			"base/exes/clipedit.exe" ]
			
file_list.each do | file |
	print( OS::Path::combine( srcpath, file ) + " to " + OS::Path::combine(destpath, file ) + "\n" )
	OS::FileUtilsEx.copy_file( OS::Path::combine( srcpath, file ), OS::Path::combine(destpath, file ) )
end

