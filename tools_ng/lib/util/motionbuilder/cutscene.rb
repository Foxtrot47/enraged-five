#
# File:: util/motionbuilder/cutscene.rb
# Description:: Ruby replacement for the crap that is 'MSBuild'
#
# Author:: Mike Wilson <mike.wilson@rockstarnorth.com>
# Edits:: Kevin Weinberg
# Date:: 19 May 2010
# Updated:: 04 June 2010
# Updated:: 15 Match 2012
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require "rexml/document"
require 'pipeline/log/log'
require 'pipeline/util/environment'
require 'pipeline/os/getopt'
require 'pipeline/gui/exception_dialog'
require 'pipeline/util/incredibuild'
require 'pipeline/util/incredibuild_xge'
require 'pipeline/util/RSNProfiler.rb'
require 'pipeline/scm/perforce'

include REXML
include Pipeline
include Util

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
$sectionmethod = -1.to_s()
$sectionduration = 0.0.to_s()
COMMONARGS = "-nopopups -usevsoutput -hideAlloc -ulog"
EXECUTECOMMANDS = true

OPTIONS = [
	[ '--build', '-b', OS::Getopt::BOOLEAN, 'build all.' ],
	[ '--inputDir', '-id', Getopt::REQUIRED, 'input dir.' ],
	[ '--sectionMethod', '-sm', Getopt::REQUIRED, 'section method.' ],
	[ '--sectionDuration', '-sd', Getopt::REQUIRED, 'section duration.' ],
	[ '--sceneName', '-sn', Getopt::REQUIRED, 'scene name.' ],
	[ '--saveStreamOnly', '-dsc', OS::Getopt::BOOLEAN, 'dont save scene name cutxml/cutbin (cutfsection).' ],
	]

#----------------------------------------------------------------------------
# Modules
#----------------------------------------------------------------------------
module Cutscene
	
	#-------------------------------------------------------------------------
	# Attributes
	#-------------------------------------------------------------------------	
	
	#Build all intermediate files and cut file
	def Cutscene::build_all( inputDir, saveStreamOnly)
		
		toolsConfigDirectory = Globals::instance().toolsconfig
		if toolsConfigDirectory.nil? 
			message = "Unable to find RS_TOOLSROOT environment variable.  Please install the project framework.\n"
			puts message
			Cutscene::log().error( message )
			return -1
		end
		
		@config = Pipeline::Config::instance()
		@project = @config.projects[ENV['RS_PROJECT']]
		@project.load_config()
		
		env = Environment.new
		@project.fill_env( env )
		
		@timer = RSNProfiler.new("CutsceneTimer")
		
		#Need this incase there is no toybox so we can still concat
		OS::FileUtilsEx.copy_file("#{inputDir}/data.cutxml", "#{inputDir}/data_stream.cutxml")	
		
		Cutscene::log_spacing()
			
		#StreamCutfile - modify the cutfile to optimise for streaming using the toybox folder
		#Buffer is only applied to data from the toybox folder
		commandline = "$(toolsbin)/anim/cutscene/cutfstream.exe -inputDir \"#{inputDir}/toybox\" -destDir \"#{inputDir}\" -cutFile \"#{inputDir}/data_stream.cutxml\" -timeBuffer 0 -output -hideAlloc -ulog"
		commandline = env.subst( commandline )
		
		if EXECUTECOMMANDS then
			status, sout, serr = OS::start( commandline )
			Cutscene::log().info( "#{commandline}" )
			case status.exitstatus
			when 0
				puts "STREAMING ENABLED.."
				Cutscene::log().info( "STREAMING ENABLED." )
				Cutscene::log().info( "CUTSCENE - cutfstream.exe exited ok." )
				sout.each do |m| Cutscene::log().info( m ); end

			when 1
				Cutscene::log().info( "STREAMING DISABLED." )
				Cutscene::log().info( "CUTSCENE - cutfstream.exe exited ok." )
				sout.each do |m| Cutscene::log().info( m ); end

			else
				Cutscene::log().error( "Error invoking CUTSCENE - cutfstream.exe: #{status.exitstatus}." )
				sout.each do |m| Cutscene::log().info( m ); end
				serr.each do |m| Cutscene::log().error( m ); end
				return -1
			end
		end
		
		Cutscene::log_spacing()	

		#SectionCutfile - Crop the cut file data to the time range, and save it as *.cutbin
		commandline = "$(toolsbin)/anim/cutscene/cutfsection_n.exe -inputDir \"#{inputDir}\" -cutsceneFile \"#{inputDir}/data_stream.cutxml\" -method #{$sectionmethod} -duration #{$sectionduration} #{COMMONARGS}"
		if ( saveStreamOnly == true ) then
			commandline = commandline + " -saveStreamOnly"
		end
		commandline = env.subst( commandline )
		
		if EXECUTECOMMANDS then
			status, sout, serr = OS::start( commandline )
			Cutscene::log().info( "#{commandline}" )
			case status.exitstatus
			when 0
				Cutscene::log().info( "CUTSCENE - cutfsection.exe exited ok." )
				sout.each do |m| Cutscene::log().info( m ); end
			else
				Cutscene::log().error( "Error invoking CUTSCENE - cutfsection.exe: #{status.exitstatus}." )
				sout.each do |m| Cutscene::log().info( m ); end
				serr.each do |m| Cutscene::log().error( m ); end
				
				return -1
			end
		end
		
		@timer.end
		Cutscene::log().info("Total Time: #{@timer.elapsed_seconds()} seconds.")	
		return 0
	end
	
	def Cutscene::create_log( scene_name )
		@@log = Log.new( "#{scene_name}_cutscene" ) if ( @@log.nil? )
		@@log
	end
	
	#Return module-wide log object.
	def Cutscene::log()
		@@log
	end
	
	#Formatting for log so its more readable
	def Cutscene::log_spacing()
		Cutscene::log().info( "" )
	end
	
#------------------------------------------------------------------------
# Private
#------------------------------------------------------------------------
private
	@@log = nil
end

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
    #-------------------------------------------------------------------------
    # Entry-Point
    #-------------------------------------------------------------------------
    begin
		opts, trailing = OS::Getopt::getopts( OPTIONS )
		
		build = opts['build'].nil? ? false : true
		inputDir = opts['inputDir'].nil? ? "" : opts['inputDir']
		$sectionmethod = opts['sectionMethod'].nil? ? "-1" : opts['sectionMethod']
		$sectionduration = opts['sectionDuration'].nil? ? "0.0" : opts['sectionDuration']
		scene_name = opts['sceneName'].nil? ? "" : opts['sceneName']
		saveStreamOnly = opts['saveStreamOnly'].nil? ? false : true
		
		result=0
		
		Cutscene::create_log( scene_name )
		
		if build then
			result = Cutscene.build_all( inputDir, saveStreamOnly )
		end
		
		exit(result)
		
	rescue SystemExit
		#puts result
		exit(result)
		
	rescue Exception => ex
	
		Cutscene::log().exception( ex, "Unhandle exception during convert" )
		ex.backtrace.each do |m| Cutscene::log().error( m ); end
		GUI::ExceptionDialog::show_dialog( ex, "unhandled exception" )
	
		# Only require Enter press when an exception has occurred.
		puts "\nPress Enter or close this window to continue..."
		$stdin.gets( ) 
	end
end
