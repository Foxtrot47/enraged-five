#
# File:: data_mk_generic_rpf.rb
# Description:: Script to create a generic RAGE RPF file from a set of
#               files or directory contents.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 3 September 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_rpf'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
    [ '--project', '-p', OS::Getopt::REQUIRED, 'project data to convert (e.g. jimmy, gta4_jpn).' ],
    [ '--branch', '-b', OS::Getopt::REQUIRED, 'project branch to convert (e.g. dev, dev_migrate).' ],
    [ '--output', '-o', OS::Getopt::REQUIRED, 'output itd filename.' ],
	[ '--uncompressed', '-u', OS::Getopt::BOOLEAN, 'create uncompressed RPF.' ],
	[ '--flatten', '-f', OS::Getopt::BOOLEAN, 'flatten directory structure.' ],
	[ '--recursive', '-r', OS::Getopt::BOOLEAN, 'recursive operation, expects directory.' ],
    [ '--filter', '-f', OS::Getopt::REQUIRED, 'file filter for path arguments.' ],
	[ '--silent', '-s', OS::Getopt::BOOLEAN, 'silent mode, reduces logging.' ],
    [ '--debug', '-d', OS::Getopt::BOOLEAN, 'debug mode toggle.' ],
    [ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING_DESC = { 
    'files' => 'list of files/directories to include/search for files to include (can include wildcards, e.g. x:/assets/*.dds).'
}
DEFAULT_FILTER = '*.*'

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

    #-------------------------------------------------------------------------
    # Entry-Point
    #-------------------------------------------------------------------------

    begin
        # Force output to stdout for console scripts.
        Pipeline::Config::instance()::logtostdout = true
        
        g_AppName = File::basename( __FILE__, '.rb' )
        g_ProjectName = ''
        g_Project = nil
        g_Branch = ''
        g_Config = Pipeline::Config.instance( ) 
    
        #---------------------------------------------------------------------
        # Parse Command Line
        #---------------------------------------------------------------------
        opts, trailing = OS::Getopt.getopts( OPTIONS )
        if ( opts['help'] ) then
            puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
            exit( 1 )
        end
        g_Debug = opts['debug']
		g_Silent = opts['silent']
		Pipeline::Config::instance()::log_level = 5 if ( g_Silent )
		Pipeline::Config::instance()::log_level = 2 unless ( g_Silent )
		Pipeline::Config::instance()::log_level = 1 if ( g_Debug )
        Pipeline::Config::instance()::log_trace = g_Debug
        g_Log = Log.new( g_AppName )
        
        g_ProjectName = ( nil == opts['project'] ) ? ENV['RS_PROJECT'] : opts['project']
        project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
        if ( not project_exists ) then
            puts OS::Getopt.usage( OPTIONS )
            puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
            exit( 2 )
        end
        g_Project = g_Config.projects[ g_ProjectName ]
        g_Project.load_config( )
        g_BranchName = ( nil == opts['branch'] ) ? g_Project.default_branch : opts['branch']
        branch_exists = ( g_Project.branches.has_key?( g_BranchName ) )
        if ( not branch_exists ) then
            puts OS::Getopt.usage( OPTIONS )
            puts "\nError project: #{g_ProjectName} does not have branch named #{g_BranchName} defined."
            exit( 4 )
        end
        g_Output = ( nil == opts['output'] ) ? nil : ::File::expand_path( opts['output'] )
        if ( g_Output.nil? ) then
            puts 'No output itd file specified.'
            puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
            exit( 2 )
        end
        g_Filter = ( nil == opts['filter'] ) ? DEFAULT_FILTER : opts['filter']
		g_Flatten = opts['flatten']
		g_Recursive = opts['recursive']
        g_Paths = []
        trailing.each do |trail|
            g_Paths << OS::Path.normalise( trail )
        end
        if ( 0 == g_Paths.size ) then
            puts 'No input files or paths specified.'
            puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
            exit( 3 )
        end
        
        #---------------------------------------------------------------------
        # Check output
        #---------------------------------------------------------------------
		if ( ::File::exists?( g_Output ) and ( not ::File::writable?( g_Output ) ) ) then
			g_Log.error( "Output file: #{g_Output} is not writable.  Aborting." )
			exit( 4 )
		end
		
        #---------------------------------------------------------------------
        # Process input
        #---------------------------------------------------------------------
		if ( ::File::exists?( g_Output ) ) then
			time_last_moded = File::mtime( g_Output ) 
		else	
			time_last_moded = Time.new
        end
		
		file_list = []
		g_Paths.each do |path|
			if ( ::File::file?( path ) ) then
                g_Log.info( "Adding file: #{path}" )
				# Just add the file at the RPF root
				entry = {}
				entry[:src] = path
				entry[:dst] = OS::Path::get_filename( path )
				file_list << entry
            
            else
                g_Log.info( "Adding files from directory: #{path}" )
				files = []
				if ( g_Recursive ) then
					files = OS::FindEx::find_files_recurse( OS::Path::combine( path, g_Filter ) )
				else
					files = OS::FindEx::find_files( OS::Path.combine( path, g_Filter ) )
				end
				files.each do |filename|
				
					if ( g_Flatten ) then
						entry = {}
						entry[:src] = filename
						entry[:dst] = OS::Path::get_filename( filename )	
						file_list << entry
					else
						relative_filename = filename.sub( path, '' )
						entry = {}
						entry[:src] = filename
						entry[:dst] = relative_filename
						file_list << entry
					end
                end
            end
		end
		
		compress = opts['uncompressed'].nil? ? true : false
		ProjectUtil::data_rpf_create( g_Output, file_list, compress )
   
		time_moded_after_save = File.mtime(g_Output)
		
		if (time_moded_after_save == time_last_moded)
			g_Log.error("\n\n#------------- ERROR: Couldn't update the RPF file ------------#")
		else
			g_Log.info("\n\nSuccessfully updated the RPF file")
			puts time_moded_after_save
		end	
		puts "\n"
     
    rescue Exception => ex
        exit( ex.status ) if ( 'exit' == ex.message )
    
        puts "\n#{g_AppName} unhandled exception: #{ex.message}"
        puts ex.backtrace.join("\n\t")      
    end 
end

# data_mk_generic_rpf.rb
