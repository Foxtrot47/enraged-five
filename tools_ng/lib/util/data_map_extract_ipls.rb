#
# File:: %RS_TOOLSLIB%/util/data_map_extract_ipls.rb
# Description:: Extract all map metadata ITYP/IMAP files from ZIPs
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 17 AUgust 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/log/log'
require 'pipeline/projectutil/data_zip'
require 'pipeline/projectutil/data_map'
require 'pipeline/scm/perforce'
require 'pipeline/util/rage'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( $0 == __FILE__ ) then
	
	g_AppName = OS::Path::get_basename( __FILE__ )
	g_Log = Pipeline::Log.new( g_AppName )
	g_Config = Pipeline::Config::instance()
	begin
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		g_ProjectName = ENV['RS_PROJECT']
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			puts OS::Getopt.usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) then
			puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
			exit( 3 )
		end
						
		# Now for the main processing loop...
		ProjectUtil::data_map_for_each( g_Project ) do |mapzip|
			
			zip_file = mapzip.filename
			next unless ( File::exists?( zip_file ) )
			next unless ( mapzip.is_a?( Pipeline::Content::MapZip ) )
			next unless ( mapzip.inputs[0].is_a?( Pipeline::Content::Map ) )
			
			stream_dir = OS::Path::combine( g_Config.temp, 'map_ipls_extract', OS::Path::get_basename( zip_file ) )
			FileUtils::mkdir_p( stream_dir ) unless ( ::File::directory?( stream_dir ) )
			
			ProjectUtil::data_zip_extract( mapzip.filename, stream_dir, true, '*.ipl' ) do |filename|
				puts "Extracting: #{filename}..."
			end
		end
		
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")    
	end
end

# %RS_TOOLSLIB%/util/data_map_extract_ipls.rb
