#
# File:: copy_latest_ragebuilder.rb
# Description:: Copies the last modified version of ragebuilder to a version that has it's filename altered with a suffix.
# for use with ragebuilder unit-test pipeline - it permits the latest version built to be checked in for testing without actaully
# screwing up the live version of ragebuilder.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 26th November 2010
#
# Passed in  :-  see OPTIONS ...
# Passed out :-  stderr contains all errors 
#                stdout for all other output. 
# Returns    :-  returns non zero upon detecting any errors 

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/file'
include Pipeline
require 'systemu'
require 'rexml/document'
require 'find'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ "--help", 		"-h", 	OS::Getopt::BOOLEAN, 	"display usage information." ],
	[ '--suffix', 		'-s', 	OS::Getopt::REQUIRED, 	'the suffix to replace the version number with' ],
	[ '--dir', 		'-d', 	OS::Getopt::REQUIRED, 	'the directory in which to find ragebuilder executables' ]
]

INFO			= "[colourise=black]INFO_MSG: "
INFO_BLUE		= "[colourise=blue]INFO_MSG: "


#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------

begin
	g_AppName = File::basename( __FILE__, '.rb' )
	g_ProjectName = ''
	g_BranchName = ''
	g_Project = nil
	g_Config = Pipeline::Config.instance()

	#---------------------------------------------------------------------
	# --- PARSER COMMAND LINE                                          --- 
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] )
		puts OS::Getopt.usage( OPTIONS )
		puts ("Press Enter to continue...")			
		Process.exit!( 1 )
	end
	
	if ( not opts['dir'])
		$stderr.puts("dir not specified in commandline to #{g_AppName}")
		puts OS::Getopt.usage( OPTIONS )					
		Process.exit!( 1 )		
	end
	
	if ( not opts['suffix'])
		$stderr.puts("suffix not specified in commandline to #{g_AppName}")
		puts OS::Getopt.usage( OPTIONS )					
		Process.exit!( 1 )		
	end	
	
	suffix 	= opts['suffix']
	dir 	= opts['dir']

	newest_time,newest_filename,newest_version = nil, nil, nil

	if File.directory?(dir)
		Find.find(dir) do |filename|		
			if (filename =~ /ragebuilder_(\d.*.*).exe/)				
				version = $1
				puts "#{INFO_BLUE} Ragebuilder version matched #{filename} version #{version}"
				if (File.exist? filename)					
					modtime = File.mtime(filename)
					if newest_time.nil?
						newest_time 	= modtime
						newest_filename = filename
						newest_version 	= version
					else
						if modtime > newest_time	
							newest_time 	= modtime
							newest_filename = filename
							newest_version 	= version
						end
					end				
				end
			end
		end
	else
		$stderr.puts("Error: Directory does not exist #{dir} #{g_AppName}")
	end
	
	puts ("#{INFO_BLUE} #{newest_filename} is version #{newest_version} and was modified @ #{newest_time} which is the newest file")
	out_filename = newest_filename.sub(newest_version,suffix)
	
	puts ("#{INFO_BLUE} Copying #{newest_filename} to #{out_filename}")
	FileUtils.copy(newest_filename,out_filename)
	
	Process.exit! 0

rescue Exception => ex
	$stderr.puts "Error: Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	Process.exit! -1
end
