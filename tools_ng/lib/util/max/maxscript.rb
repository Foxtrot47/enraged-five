#
# maxscript.rb
# Maxscript module.  No shit huh. 
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# 
# Date:: 30/04/2010
#
module Maxscript
	class File	
		attr_reader	:lines
		attr_reader	:filename

		#
		# == Description
		# 
		# Set members
		# 
		def initialize(filename)
			@filename = filename
			@lines = []
		end

		#
		# == Description
		# 
		# parse file and load into array
		# 
		def load()
			::File.open(@filename) do |mxsfile|
				begin
					mxsfile.each do |currline|
						@lines.push(currline)
					end
				ensure 
					mxsfile.close()
				end
			end
		end

		#
		# == Description
		# 
		# save array of lines back out as maxscript
		# 
		def save( outfile )
			::File.open(outfile, "w+") do |mxsfile|
				begin
					@lines.each do |line|
						mxsfile.write(line)
					end
				ensure
					mxsfile.close()
				end
			
			end
		end

		#
		# == Description
		# 
		# replace string patterns in include and filein paths
		# 
		def modify_include_strings( target, new )
			for i in 0..lines.size do
				tokens = []
				tokens = lines[i].split(' ') if lines[i] != nil
				
				if tokens.size > 0 then
					lines[i].sub!(target, new) if tokens[0].downcase == "include" or tokens[0].downcase == "filein" 				
				end
				
			end
		end
	end
	
end


