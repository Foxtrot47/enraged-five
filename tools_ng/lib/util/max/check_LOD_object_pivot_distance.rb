#
# File:: %RS_TOOLSLIB%/util/max/check_LOD_object_pivot_distance.rb
# Description:: Gets a list of scene xml files based on the project, level, group and map
#				input and then runs a c# tool to which runs through the scene xml files to
#				find the distance between the pivot and bounding box centres
#
# Author:: Adam Munson <adam.munson@rockstarnorth.com>
# Date:: 13 July 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_map'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ '--project', '-p', Getopt::REQUIRED, 'project data to fetch (e.g. jimmy, gta5).' ],
	[ '--level', '-l', Getopt::REQUIRED, 'process a single level map data (e.g. mdtestbed).' ],
	[ '--group', '-g', Getopt::REQUIRED, 'process a group (folder) of map data (e.g. _citye, downtown_01).' ],
	[ '--map', '-m', Getopt::REQUIRED, 'process a single map (e.g. v_int_1).' ],
	[ '--threshold', '-t', Getopt::REQUIRED, 'threshold value for object pivot distance check (e.g 0.3 for 30%)' ],
]

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	g_AppName = OS::Path::get_basename( __FILE__ )
	g_Config = Pipeline::Config::instance()
	begin
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
        g_ProjectName = ( nil == opts['project'] ) ? ENV['RS_PROJECT']: opts['project']
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			puts OS::Getopt.usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) then
			puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
			exit( 3 )
		end
		g_Project.load_config( )
		g_Level = opts['level'].nil? ? nil : opts['level']
		g_Group = opts['group'].nil? ? nil : opts['group']
		g_Map = opts['map'].nil? ? nil : opts['map']
		g_Threshold = opts['threshold'].nil? ? nil : opts['threshold']

		g_Files = []
	
		# Now for the main processing loop...
		ProjectUtil::data_map_for_each( g_Project, g_Level, g_Group ) do |mapzip|
		
			if ( not g_Map.nil? ) then
				next unless ( g_Map == mapzip.name )
			end
		
			zip_file = mapzip.filename
			next unless ( File::exists?( zip_file ) )
			next unless ( mapzip.is_a?( Pipeline::Content::MapZip ) )
			next unless ( mapzip.inputs[0].is_a?( Pipeline::Content::Map ) )
			
			xml_file = OS::Path::replace_ext( zip_file, 'xml' )
			g_Files << xml_file
			
		end
		
		# Construct the command line to run, with trailing scene xml filenames
		cmdLine = "#{ENV['RS_TOOLSBIN']}/MapExport/MapPivotDistanceCheck.exe"
		# Threshold is optional, defaults to 0.2 in the c# tool if not specified
		if ( g_Threshold != nil )  then
			cmdLine += "-threshold #{g_Threshold}"
		end
		
		g_Files.each do |file|
			cmdLine += " " + file
		end
		system(cmdLine)
	
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")    
	end
end
