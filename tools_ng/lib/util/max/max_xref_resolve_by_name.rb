#
# File:: %RS_TOOLSLIB%/util/max/max_xref_resolve_by_name.rb
# Description:: Patches the user's 3dsmax.ini file with the following:
#
#   [XRefOptions]
#   ResolveByNameFirst=1
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 24 March 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/log/log'
require 'pipeline/os/path'
require 'pipeline/util/autodesk3dsmax'
require 'pipeline/util/environment'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------

# INI file path, e.g. C:/Users/david.muir/AppData/Local/Autodesk/3dsmax/2009 - 32-bit/enu
INI_PATH = '$(localappdata)/Autodesk/3dsmax/$(version) - $(arch)/enu/3dsmax.ini'
START_VERSION  = 11.0
X86_ARCH = '32bit'
X64_ARCH = '64bit'
RESOLVE_BY_NAME_FIRST_REGEXP = /ResolveByNameFirst=1/
PATCH = [ '', '[XRefOptions]', 'ResolveByNameFirst=1' ]

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

def process_version( log, env, version, arch )
	
	env.push( )
	env.add( 'version', version.to_s )
	env.add( 'arch', arch )
			
	ini_path = env.subst( INI_PATH )
	log.info( "Processing 3dsmax version #{version} #{arch}..." )
	log.info( "\tPath: #{ini_path}" )
			
	if ( File::exists?( ini_path ) and File::readable?( ini_path ) ) then
		patch_file( log, ini_path )
	else
		log.error( "Patching failed: #{ini_path} does not exist or is not readable." )
	end
			
	env.pop( )
end

# Actually patch the file if required.
def patch_file( log, filename )
	
	resolve_by_name_first = false
	lines =File::readlines( filename )
	lines.each do |line|
		resolve_by_name_first = ( line =~ RESOLVE_BY_NAME_FIRST_REGEXP )
		break if ( resolve_by_name_first )
	end
		
	if ( resolve_by_name_first ) then
		log.info( "\tNo patching #{OS::Path::get_filename(filename)} required." )
	else
		log.info( "\tPatching #{OS::Path::get_filename(filename)}" )
		
		FileUtils::cp( filename, "#{filename}.bak" )
		File::open( filename, 'a' ) do |fp|
			PATCH.each do |patch|
				fp.puts( patch )
			end
		end
	end
end

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
	g_Log = Log::new( 'max_xref_resolve_by_name' )
	begin
		# Iterate through all versions of 3dsmax that we know about thats
		# greater than the START_VERSION.
		env = Environment::new( ) 
		env.add( 'localappdata', ENV['LOCALAPPDATA'] )
		
		versions = Autodesk3dsmax::instance().installdirs( )
		versions[:x86].each_pair do |version, installpath|			
			next if ( version.to_f < START_VERSION )
			ver = Autodesk3dsmax::VERSION_DIRS[version.to_f].gsub( 'max', '' )
			process_version( g_Log, env, ver, X86_ARCH )
		end
		versions[:x64].each_pair do |version, installpath|		
			next if ( version.to_f < START_VERSION )
			ver = Autodesk3dsmax::VERSION_DIRS[version.to_f].gsub( 'max', '' )
			process_version( g_Log, env, ver, X64_ARCH )
		end
		
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
		g_Log.exception( ex, "Unhandled exception patching 3dsmax.ini." )
		puts "Unhandled exception patching 3dsmax.ini: #{ex.message}."
		puts ex.backtrace.join( "\n" )
	end
end

# %RS_TOOLSLIB%/util/max/max_xref_resolve_by_name.rb
