require 'pipeline/os/path'
require 'pipeline/os/file'
include Pipeline

# Return accurate modified time for a directory.
def get_directory_mtime( directory )

  files = OS::FindEx.find_files( OS::Path::combine( directory, "*.*" ) )

  files_most_recent = File::mtime( directory )
  puts "DIR MTIME: #{files_most_recent}"
  
  filesmtime = files.collect do |filename| File::mtime( filename ); end
  
  filesmtime.each do |mtime|
    puts "\tMTIME: #{mtime}"
    files_most_recent = mtime if ( ( mtime <=> files_most_recent ) > 0 )
  end
  
  files_most_recent
end

begin
  puts "Directory: #{ARGV[0]}"
  mtime = get_directory_mtime( ARGV[0] )
  puts "mtime: #{mtime}"

rescue Exception => ex

  puts "EX: #{ex.message}"
  puts ex.backtrace().join("\n")
end
