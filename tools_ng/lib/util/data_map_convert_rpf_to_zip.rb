#
# File:: %RS_TOOLSLIB%/util/data_map_convert_rpf_to_zip.rb
# Description:: Convert all map RPF data to ZIPs (and move SceneXml out).
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 9 February 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/log/log'
require 'pipeline/projectutil/data_convert'
require 'pipeline/projectutil/data_extract'
require 'pipeline/projectutil/data_map'
require 'pipeline/scm/perforce'
require 'pipeline/util/rage'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--project', '-p', Getopt::REQUIRED, 'project data to fetch (e.g. jimmy, gta5).' ],
	[ '--continue', '-c', Getopt::REQUIRED, 'continue from a particular map RPF (e.g. vb_39).' ], 	
]
MOUNT = 'pack:/'

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	g_AppName = OS::Path::get_basename( __FILE__ )
	g_Log = Pipeline::Log.new( g_AppName )
	g_Config = Pipeline::Config::instance()
	begin
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
		g_ProjectName = ( nil == opts['project'] ) ? '' : opts['project']
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			puts OS::Getopt.usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) then
			puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
			exit( 3 )
		end
		g_ContinueMap = opts['continue'].nil? ? nil : opts['continue']
		g_Continue = ( not g_ContinueMap.nil? )
		
		g_R = RageUtils::new( g_Project )
		
		Dir::chdir( g_Project.export )
		g_Perforce = SCM::Perforce.new
		g_Perforce.connect()
		g_Files = trailing
		
		# Now for the main processing loop...
		trailing.each do |filename|
			rpf_file = OS::Path::replace_ext( filename, 'rpf' )
			zip_file = OS::Path::replace_ext( filename, 'zip' )
			next unless ( File::exists?( rpf_file ) )
			
			# Extract RPF.
			g_Log.info( "Extracting: #{rpf_file}" )
			output_dir = OS::Path::combine( g_Config.temp, OS::Path::get_basename( rpf_file ) )
			files = ProjectUtil::data_extract_rpf( g_R, rpf_file, output_dir, true )
			scenexml_file = OS::Path::get_basename( rpf_file ) + '.xml'
			scenexml_src = nil
			scenexml_dst = OS::Path::replace_ext( zip_file, 'xml' )
			
			# Create ZIP; excluding the SceneXml file, copy it to destination.
			g_Log.info( "Building: #{zip_file}" )
			g_R.zip.start( )
			files.each do |file|
				if ( file.ends_with( scenexml_file ) ) then
					# Copy SceneXml file to same place as destination.
					scenexml_src = file
				elsif ( file.ends_with( '_files.txt' ) ) then
					next # skip as old metadata file for RPF4
				else
					# Otherwise we add the file to the ZIP.
					g_R.zip.add( file, OS::Path::get_filename( file ) )
				end
			end
			g_R.zip.save( zip_file )
			g_R.zip.close( )
			FileUtils::cp( scenexml_src, scenexml_dst )
			
			g_Perforce.run_edit_or_add( scenexml_dst )
			g_Perforce.run_edit_or_add( zip_file )
		end
		
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")    
	end
end

# %RS_TOOLSLIB%/util/data_map_convert_rpf_to_zip.rb
