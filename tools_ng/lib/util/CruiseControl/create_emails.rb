#
# File:: create_emails.rb
# Description:: Generates a XML document formatted for Cruise Control
# listing all e-mails registered with the Perforce server.
#
# Date:: 19 August 2011
#
#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/scm/perforce'
include Pipeline

OPTIONS = [
	[ '--output', '-o', OS::Getopt::REQUIRED, 'Output file.' ],
	[ '--nosubmit', '-s', OS::Getopt::BOOLEAN, 'Disables submission.' ],
]
TRAILING = {}

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
begin
	g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )

	tools_root = ENV["RS_TOOLSROOT"]
	
	output_file = ( nil == g_Opts['output'] ) ? "#{tools_root}\\etc\\CruiseControl\\general\\standardemails.xml" : g_Opts['output']
	submit = g_Opts['nosubmit'].nil? ? true : false	

	# Configure and connect to Perforce.
	config = Pipeline::Config.instance	
	@p4 = SCM::Perforce.new()
	@p4.port = config.sc_server
	@p4.client = config.sc_workspace
	@p4.user = config.sc_username
	@p4.connect()
	
	edit_output = @p4.run_edit(output_file)
	
	email_file = File.new(output_file, "w")
	if email_file == nil then
		print "Unable to create file #{email_file}\n"
		return -1
	else

		output_fstat  = @p4.run_fstat(output_file)
		depotFile = output_fstat[0]["depotFile"]
		
		#Create a new changelist
		changelist = @p4.fetch_change
		changelist[ "Files" ] = [ depotFile ]
		changelist[ "Description" ] = "Updated Cruise Control Emails"

		users_output = @p4.run("users")
		users_output.each do |user|
		
			if (user.has_key?("User") and user.has_key?("Email")) then	
			
				#print "#{user["User"]}\n"
				#print "#{user["Email"]}\n"
				
				email_file.write("<user name=\"#{user["User"]}\" address=\"#{user["Email"]}\" />\n")
			end		
		end
		
		email_file.close
	
		revert_records = @p4.run_revert("-a", "-n", depotFile)
		@p4.run_revert("-a", depotFile)
		
		if revert_records.count > 0 and revert_records[0]["depotFile"] == depotFile then
			print "INFO_MSG: No change was found.  The file will not be submitted.\n"
		else
			if submit then
				print "INFO_MSG: Submitting #{output_file}...\n"
				submit_info = @p4.run_submit( changelist )
			else
				print "INFO_MSG: The e-mail file has been successfully generated at #{output_file}.\n"
			end	
		end

	end
		
	0
end

# %RS_TOOLSLIB%/util/create_emails.rb