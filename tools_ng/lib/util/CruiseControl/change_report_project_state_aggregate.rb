#
# File:: change_report_project_state_aggregate.rb
# Description:: Aggregate several project_state.xml into a cross project / cross machine report.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 11th March 2010
#
# Passed in  :-  see OPTIONS ...
# Passed out :-  
#                
# Returns    :-  

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/os/getopt'
require 'pipeline/os/file'
include Pipeline
require 'xml'
require 'util/cruisecontrol/change_report_history'
require 'util/cruisecontrol/change_report_project_state'
require 'active_support/ordered_hash'
require 'time'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

class String

 def rtrim(character)
    ret = self
    if self[-1, 1] == character.to_s
      ret = self[0, self.length - 1]
    end
    return ret
 end
  
 def ltrim(character)
    ret = self
    if self[0, 1] == character.to_s
      ret = self[1, self.length]
    end
    return ret
  end  
  
end

# class to store details about a Cruise Control project
class CCProject
	attr_accessor :name
	attr_accessor :status
	attr_accessor :url
	
	def initialize(name, url)
		@name = name
		@url = url
		@status = nil
	end
end

# The Change Report Project State Aggregate class
class ChangeReportProjectStateAggregate

	#------------------------------------------------------------------------------------------
	# constants
	PROJECT_CHANGE_XML_KEYS = ["project_name","run_time","status","build_date","build_duration", "details", "log_name", "swizzler" ]
	CHANGE_XML_KEYS 	= ["change_number","comment","buddy","user","url","sort_date","modified_date","last_build_date","turnaround"]	

	BLANK = "-"
	RUNNING_WORK_UNIT_STATUS = "running"
	PENDING_WORK_UNIT_STATUS = "pending"
	UNKNOWN_WORK_UNIT_STATUS = "unknown"

	REGEXP_TOOLBUILDER_TARGET_XML = /^[^\\]+\\toolbuilder_target_(.*).xml/i

	#------------------------------------------------------------------------------------------
	#
	def initialize(common, categories, sub_categories, poll_interval, auto_refresh, title, webserver, prev_report, next_report, events_filename = nil)
		@run_time = Time.now.strftime("%H:%M:%S %d/%m")
		@projects = []	
		@common = common.rtrim("_").ltrim("_")
		@categories = categories.split(" ")
		@sub_categories = sub_categories.split(" ")
		@poll_interval = poll_interval
		@auto_refresh = auto_refresh
		@events_filename = events_filename
		@prev_report = prev_report
		@next_report = next_report	
		@webserver = webserver
		@title = title
	end

	#------------------------------------------------------------------------------------------
	# sync to a file
	def sync_file(filename)
		begin
			@config = Pipeline::Config.instance
			@config.project.load_config						
			@p4 = SCM::Perforce.new()
			@p4.port = @config.sc_server
			@p4.client = @config.sc_workspace
			@p4.user = @config.sc_username
			@p4.connect()		
			@p4.run_sync( filename )
		rescue Exception => ex		
			ChangeReportProjectStateAggregate::log().error("exception #{ex}")										
		end	
	end								

	#------------------------------------------------------------------------------------------
	# read the source xml file and dervie from it a bunch of xml docs that should be read
	# so that this process is automatic! - it will sync also to keep up to date.
	def get_toolbuilder_xmldocs(targets_filename)
		
		xmldocs = []
		
		sync_file( targets_filename )

		src_xmldoc = LibXML::XML::Document::file( targets_filename )
		nodes = src_xmldoc.find("*/@href")
						
		if (nodes)
			ChangeReportProjectStateAggregate::log().error("Nae nodes.") if nodes.length <= 0 

			nodes.each do |node|			

				if node.value =~ REGEXP_TOOLBUILDER_TARGET_XML
					# DW - ack.. a minor assumption about the project branch - I hate doing stuff like this as it may one day break.
					new_filename = "toolbuilder_#{ENV["RS_PROJECT"]}_dev_#{$1}/project_state.xml"
										
					if (File.exist?(new_filename))
						xmldoc = LibXML::XML::Document::file( new_filename )
						if xmldoc
							xmldocs << xmldoc
						else
							ChangeReportProjectStateAggregate::log().error("Not valid xmldoc #{new_filename} - ignored.")					
						end
					else
						ChangeReportProjectStateAggregate::log().error("Filename #{new_filename} doesnt exist.")
					end								
				end
			end
		else
			ChangeReportProjectStateAggregate::log().error("Nodes are nil")
		end	

		xmldocs
	end

	#------------------------------------------------------------------------------------------
	# create an aggregate report of the filenames passed in.
	def create( filenames )
		begin 		
			ChangeReportProjectStateAggregate::log().debug("================= #{@run_time} =========================================================")
		
			# load all xmldocs
			xmldocs  = []
			filenames.each do |filename|
				if (File::file?(filename) )
					if ( filename.downcase.include?("toolbuilder"))
						xmldocs += get_toolbuilder_xmldocs(filename)	
					else
						xmldoc = LibXML::XML::Document::file( filename )				
						if (xmldoc)
							xmldocs << xmldoc														
						else
							ChangeReportProjectStateAggregate::log().error("Not valid xmldoc #{filename} - ignored.")					
						end
					end
				else
					ChangeReportProjectStateAggregate::log().error("No file #{filename} - ignored.") 
				end
			end
			
			create_master_changes(xmldocs)				
			fill_blanks()
			
			calculate_project_status()

		rescue Exception => ex		
			ChangeReportProjectStateAggregate::log().error("exception #{ex}")										
		end

		true
	end
	
	#------------------------------------------------------------------------------------------
	# Determine the last status of each project that has built.
	# - this ignores running / pedning / unknown project states.
	def calculate_project_status()
		
		@projects.each do |project|
					
			biggest_cl_num = 0
			
			@master_changes.each_pair do |change_number, change|
				cl = change_number.to_i
				if (cl>biggest_cl_num)
					
					keys = change["project"].keys
					if keys.length > 0
						keys.each do |key|
							if (key.include? project.name)
								status = change["project"][key]["status"]
								
								if status != "running" and status != "pending" and status != "Pending" and status != "unknown"
									project.status = status
									biggest_cl_num=cl									 
								end
							end
						end
					end					
				end
			end													
		end
	end
	
	#------------------------------------------------------------------------------------------
	# Create master list of changes - inside which there are details of the state for each project
	def create_master_changes( xmldocs )
		@master_changes = {}

		xmldocs.each do |xmldoc|
			project_state = xmldoc.find_first("//project_state")

			if (project_state)
				project_name = project_state.attributes["project_name"]				
				run_time = project_state.attributes["run_time"]	
				project_url = project_state.attributes["project_url"]	
				@projects << CCProject.new(project_name,project_url)

				workunits = project_state.find("work_units/work_unit")
				ChangeReportProjectStateAggregate::log().info("Reading #{workunits.length} workunits.") 

				swizzler = "0"
				
				sorted_workunits = workunits.to_a				
				sorted_workunits.sort! { |a,b| b.attributes["build_date"] <=> a.attributes["build_date"] } 
				
				sorted_workunits.each do |workunit|
					status 		= workunit.attributes["status"]
					build_date 	= workunit.attributes["build_date"]
					build_duration 	= workunit.attributes["build_duration"]
					details 	= workunit.attributes["details"] ? workunit.attributes["details"] : ""
					log_name 	= workunit.attributes["log_name"]
					last_build_date = BLANK
					turnaround	= BLANK
					
					# change the build date to be a build completed
					build_completed = Time.parse(build_date.to_s)
					if (build_duration.to_s =~ /(\d\d):(\d\d):(\d\d)/i)
						duration_hours = $1.to_i
						duration_mins = $2.to_i
						duration_secs = $3.to_i

						short_duration = ""											
						short_duration += "#{duration_hours} hrs" if duration_hours > 0
						short_duration += " #{duration_mins} mins" if duration_mins > 0
						short_duration += " #{duration_secs} secs" if duration_secs > 0
						
						build_duration = short_duration
						
						total_seconds = duration_secs + ( duration_mins * 60 ) + ( duration_hours * 60 * 60 )
						
						build_completed += total_seconds
						build_date = build_completed.to_s
					end
					
					changes = workunit.find("changes/change")
					changes.each do |change|
						number 		= change.attributes["number"]
						comment 	= change.attributes["comment"]
						buddy 		= change.attributes["buddy"]
						user 		= change.attributes["user"]
						url 		= change.attributes["url"]
						sort_date 	= change.attributes["modified_date"]
						modified_date 	= change.attributes["modified_date"]
						
						status_lower = status.to_s.downcase
						last_build_date_valid = (status_lower!=RUNNING_WORK_UNIT_STATUS and
									status_lower!=PENDING_WORK_UNIT_STATUS and 
									status_lower!=UNKNOWN_WORK_UNIT_STATUS)
						last_build_date = build_date if last_build_date_valid
						
						if (not @master_changes[number])							
							@master_changes[number] = { 	
											"change_number" => number,
											"comment" => comment,
											"buddy" => buddy,
											"user" => user.downcase,
											"url" => url,
											"sort_date" => sort_date,
											"modified_date" => modified_date,
											"last_build_date" => last_build_date,
											"turnaround" => turnaround,
											"project" => {}
										  }	
						end
						
						# calculate the last build date
						stored_last_build_date = @master_changes[number]["last_build_date"].to_s
						if (last_build_date_valid)
							this_time = Time.parse(last_build_date.to_s)
							stored_time = Time.parse(stored_last_build_date)
							mod_time = Time.parse(@master_changes[number]["modified_date"])
							if (stored_last_build_date==BLANK or this_time >= stored_time)
								@master_changes[number]["last_build_date"] = last_build_date 
								
								# recalc turnaround	
								difference = this_time - mod_time

								seconds    =  difference % 60
								difference = (difference - seconds) / 60
								minutes    =  difference % 60
								difference = (difference - minutes) / 60
								hours      =  difference % 24
								difference = (difference - hours)   / 24
								days       =  difference % 7
								weeks      = (difference - days)    /  7								

								turnaround = ""											
								turnaround += " #{weeks.to_i} weeks" if weeks > 0
								turnaround += " #{days.to_i} days" if days > 0
								turnaround += " #{hours.to_i} hrs" if hours > 0
								turnaround += " #{minutes.to_i} mins" if minutes > 0
								turnaround += " #{seconds.to_i} secs" if seconds > 0

								@master_changes[number]["turnaround"] = turnaround
							end														
						end
						
						@master_changes[number]["project"][project_name] = {					
													"project_name" => project_name, 
													"run_time" => run_time,
													"status" => status,
													"build_date" => build_date,
													"build_duration" => build_duration,
													"details" => details,
													"log_name" => log_name,
													"swizzler" => swizzler,
											        }												
					end				
					
					if (status!= "pending" && status!="running" && status!="Pending" && status != "unknown")
					
						if swizzler == "1"
							swizzler = "0" 
						else
							swizzler = "1" 
						end			

					end									
					
				end
			else
				ChangeReportProjectStateAggregate::log().error("Bad xml format.") 
			end			
		end
	end # create_master_changes
	
	#------------------------------------------------------------------------------------------
	# some project might not have an entry for a change - in which case a blank is inserted
	# might make assumptions here - so may flag this entry as a 'guessed' entry
	def fill_blanks()
		num_projects = @projects.length
		@master_changes.each_pair do |change_number, change|
			keys = change["project"].keys
			if keys.length > 0
				statuses= []
				keys.each do |key|
					statuses << change["project"][key]["status"]
				end
				
				unknown_state = "unknown"
				unknown_state = "Pending" if (statuses.include? "running" or statuses.include? "pending")
			
				src = change[keys[0]] # to be used to replicate it;s staus across other nodes - if pending?
				@projects.each do |project|
					if (not keys.include? project.name )
						#ChangeReportProjectStateAggregate::log().info("project created for #{change_number}, #{project.name}")
						change["project"][project.name] = { 	
										"project_name" => 	project.name, 
										"run_time" => 		"unknown",
										"status" => 		unknown_state,
										"build_date" => 	"unknown",
										"build_duration" => 	"unknown",
										"details" => 		"",
										"log_name" =>		"unknown",
										"swizzler" =>		"unknown",
									  }
					end
				end				
			else
				ChangeReportProjectStateAggregate::log().error("Empty change found") 
			end
		end
	end
		
	#------------------------------------------------------------------------------------------
	# save file
	def save( filename )
		#FileUtils::mkdir_p( OS::Path::get_directory( filename ) ) unless ( File::directory?( filename ) )

		ChangeReportProjectState::log().info( "Saving (#{filename})." )
		xmldoc = to_xml( )
		xmldoc.save( filename)
	end	

	#------------------------------------------------------------------------------------------
	# get xmldoc
	def to_xml(  )			
		xmldoc = XML::Document.new
		xmldoc.encoding = XML::Encoding::UTF_8
		node = XML::Node.new( 'project_state_aggregate' )
		node.attributes["run_time"] = @run_time
		node.attributes["common"] = @common
		node.attributes["poll_interval"] = @poll_interval.to_s
		node.attributes["auto_refresh"] = @auto_refresh.to_s		
		
		node.attributes["title"] = @title? @title.to_s : "" 
		node.attributes["webserver"] = @webserver? @webserver.to_s : "" 
		node.attributes["prev_report"] = @prev_report? @prev_report.to_s : ""			
		node.attributes["next_report"] = @next_report? @next_report.to_s : "" 		
		xmldoc.root = node 				
		xmldoc.root << projects_xml_node()		
		xmldoc.root << changes_xml_node()
		if @events_filename
			xmldoc.root << events_xml_node() 
		end

		xmldoc				
	end	

	#------------------------------------------------------------------------------------------
	# log accessor
	def self.log( )
		@@log = Log.new( 'change_report_project_state' ) if ( @@log.nil? )
		@@log
	end	
	
protected

	#------------------------------------------------------------------------------------------
	def events_xml_node( )
		node = XML::Node.new( 'events' )

		xmldoc = LibXML::XML::Document::file( @events_filename )				
		if (xmldoc)

			events = xmldoc.find_first("//events")

			if (events)
				event_nodes = events.find("event")
				ChangeReportProjectStateAggregate::log().info("Reading #{event_nodes.length} events.") 
				
				sorted_events = event_nodes.to_a				
				sorted_events.sort! { |a,b| b.attributes["sort_date"] <=> a.attributes["sort_date"] } 

				sorted_events.each do |event|
					status 		= event.attributes["status"]
					build_date 	= event.attributes["build_date"]
					sort_date 	= event.attributes["sort_date"]
					build_completed = event.attributes["build_completed"]
					build_duration 	= event.attributes["build_duration"]
					project 	= event.attributes["project"]
					description 	= event.attributes["description"]
					cc_project_name = event.attributes["cc_project_name"]

					event_node = XML::Node.new( 'event' )
					event_node.attributes["project"]		= project ? project : "-"
					event_node.attributes["description"] 		= description ? description : "-"
					event_node.attributes["status"] 		= status ? status : "-"
					event_node.attributes["build_date"] 		= build_date ? build_date : "-"
					event_node.attributes["sort_date"] 		= sort_date ? sort_date : "-"
					event_node.attributes["build_completed"] 	= build_completed ? build_completed : "-"
					event_node.attributes["build_duration"] 	= build_duration ? build_duration : "-"
					event_node.attributes["cc_project_name"]	= cc_project_name ? cc_project_name : "-"
					node << event_node
				end
			else
				ChangeReportProjectStateAggregate::log().error("Not valid xmldoc : no events #{filename} - ignored.")
			end
		else
			ChangeReportProjectStateAggregate::log().error("Not valid xmldoc #{filename} - ignored.")					
		end

		node
	end



	#------------------------------------------------------------------------------------------
	def project_xml_node( project )
		node = XML::Node.new( 'project' )
		
		node.attributes["name"] = project.name
		node.attributes["project_url"] = project.url
		node.attributes["status"] = project.status
		
		short_name = project.name.sub(@common,"")
		num_set = 0		

		category = "-"
		@categories.each do |cat|			
			if (project.name.include? cat)
				category = cat 
				num_set += 1
				break
			end
		end
		node.attributes["category"] = category

		sub_category = "-"
		@sub_categories.each do |sub_cat|			
			if (project.name.include? sub_cat)
				sub_category = sub_cat 
				num_set += 1
				break
			end
		end
		node.attributes["sub_category"] = sub_category

		# set a name at least for something
		if (num_set == 0)
			short_name = short_name.rtrim("_").ltrim("_")
			split_name = short_name.split("_")

			if (split_name.length > 1)		
				node.attributes["category"] = split_name[0]
				node.attributes["sub_category"] = split_name[1..-1].join("|")
			else
				node.attributes["sub_category"] = short_name
			end
		end

		node.attributes["short_name"] = short_name
		node
	end

	#------------------------------------------------------------------------------------------
	def change_project_xml_node(project_name, project)
		node = XML::Node.new( 'project' )
		PROJECT_CHANGE_XML_KEYS.each { |key| node.attributes[key] = project[key] }
		
		# specify an order that can be used for the projects as we find them in the projects array - better than sorting as that is not desired.
		order = "0"
		@projects.each_with_index do |project,idx|
			order = idx.to_s if project.name == project_name
		end
				
		node.attributes["order"] = order
		node.attributes["build_date"] = Time.parse(node.attributes["build_date"].to_s).strftime("%H:%M:%S %d/%m") if node.attributes["build_date"].to_s != BLANK		
		
		node	
	end

	#------------------------------------------------------------------------------------------
	def change_xml_node(change)
		node = XML::Node.new( 'change' )
		CHANGE_XML_KEYS.each { |key| node.attributes[key] = change[key] }
		node.attributes["last_build_date"] = Time.parse(node.attributes["last_build_date"].to_s).strftime("%H:%M:%S %d/%m") if node.attributes["last_build_date"].to_s != BLANK
		node.attributes["modified_date"] = Time.parse(node.attributes["modified_date"].to_s).strftime("%H:%M:%S %d/%m") if node.attributes["modified_date"].to_s != BLANK		
		
		change["project"].each_pair { |project_name,project| node << change_project_xml_node(project_name,project) }
		node
	end

	#------------------------------------------------------------------------------------------
	def projects_xml_node()
		projects_node = XML::Node.new( 'projects' )
		@projects.each { |project| projects_node << project_xml_node(project) }
		projects_node
	end

	#------------------------------------------------------------------------------------------
	def changes_xml_node()
		changes_node = XML::Node.new( 'changes' )		
		@master_changes.each_pair { |change_number,change| changes_node << change_xml_node(change) }
		changes_node
	end
	
	@@log = nil		
end # ChangeReportProjectStateAggregate

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

	OPTIONS = [
		[ "--help", 			"-h", 	OS::Getopt::BOOLEAN, 	"display usage information." ],
		[ '--dest_filename', 		'-d', 	OS::Getopt::OPTIONAL, 	'filename to output results to. eg. project_state.xml' ],
		[ '--common', 			'-x', 	OS::Getopt::OPTIONAL, 	'common aspect of project names' ],
		[ '--categories', 		'-w', 	OS::Getopt::OPTIONAL, 	'recognised categories' ],
		[ '--sub_categories', 		'-s', 	OS::Getopt::OPTIONAL, 	'recognised sub categories' ],
		[ '--poll_interval', 		'-p', 	OS::Getopt::OPTIONAL, 	'poll interval this is run with' ],
		[ '--auto_refresh', 		'-a', 	OS::Getopt::OPTIONAL, 	'desired auto refresh of report' ],
		[ '--events', 			'-e', 	OS::Getopt::OPTIONAL, 	'events filename' ],		
		[ '--title', 			'-nr', 	OS::Getopt::REQUIRED, 	'title' ],
		[ '--webserver', 		'-nr', 	OS::Getopt::REQUIRED, 	'webserver' ],
		[ '--next_report', 		'-nr', 	OS::Getopt::REQUIRED, 	'next report' ],
		[ '--prev_report', 		'-pr', 	OS::Getopt::REQUIRED, 	'prev report' ],
	]

	begin
		#Disable the version check for this script in particular; prevents
		#Perforce prompts from appearing should the server go down for a brief moment.
		Globals::instance().skip_version_check = true
		
		g_AppName = File::basename( __FILE__, '.rb' )
	
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, g_Trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
		g_DestFilename 	= opts['dest_filename'].nil? ? "project_state_aggregate.xml" : opts['dest_filename']
		g_Common 	= opts['common'].nil? ? "" : opts['common']
		g_Categories 	= opts['categories'].nil? ? "" : opts['categories']					
		g_SubCategories = opts['sub_categories'].nil? ? "" : opts['sub_categories']					
		g_AutoRefresh  	= opts['auto_refresh'].nil? ? (60*5) : opts['auto_refresh']					
		g_PollInterval  = opts['poll_interval'].nil? ? 30 : opts['poll_interval']					
		g_EventsFile  	= opts['events'].nil? ? nil : opts['events']					
		g_Title  	= opts['title'].nil? ? "" : opts['title']					
		g_Webserver  	= opts['webserver'].nil? ? "No webserver specified" : opts['webserver']					
		g_PrevReport  	= opts['prev_report'].nil? ? "" : opts['prev_report']					
		g_NextReport  	= opts['next_report'].nil? ? "" : opts['next_report']							
		
		g_Filenames = []

		g_Trailing.each_with_index do |filename, index|				
			filename = File::expand_path( filename )
			g_Filenames += OS::FindEx::find_files( filename )
			
			puts "FILES: #{filename}"
		end
		
		g_Filenames.each do |f|
			puts "File: '#{f}'"
		end

		if g_Filenames.length == 0
			ChangeReportProjectStateAggregate::log().error("No source report files files found")  		
			Process.exit! -1
		else
			g_Filenames.each do |fn|
				ChangeReportProjectStateAggregate::log().info("Source file: #{fn}") 
			end
		end
		
		ChangeReportProjectStateAggregate::log().info("Previous report #{g_PrevReport}")
		ChangeReportProjectStateAggregate::log().info("Next Report #{g_NextReport}")
		
		change_report_project_state_aggregate = ChangeReportProjectStateAggregate.new(g_Common, g_Categories, g_SubCategories, g_PollInterval, g_AutoRefresh, g_Title, g_Webserver, g_PrevReport, g_NextReport, g_EventsFile )
		if ( change_report_project_state_aggregate.create( g_Filenames ) )
			change_report_project_state_aggregate.save( g_DestFilename  )
		end
		
		Process.exit! 0 
						
	rescue Exception => ex
		puts "Unhandled exception: #{ex.message}"
		puts ex.backtrace().join("\n")	
	end 	
end

# %RS_TOOLSLIB%/util/CruiseControl/change_report_project_state_aggregate.rb