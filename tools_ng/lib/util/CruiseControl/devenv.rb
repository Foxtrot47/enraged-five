#
# File:: devenv.rb
# Description:: 
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 15th November 2011
# Executes visual studio devenv.exe process and gets errors and return code - since normally running this is doen in background we have to wait for this background process to complete.
#
# 
# Passed in  :-  see OPTIONS ...
# Passed out :-  
#                
# Returns    :-  

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/os/path'
require 'pipeline/os/getopt'
require 'pipeline/log/log'
include REXML
require 'fileutils'  
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

INFO_BLUE = "[colourise=blue]INFO_MSG: "
VS2008 = "C:/Program Files (x86)/Microsoft Visual Studio 9.0/Common7/IDE/devenv.exe"		
VS2010 = "C:/Program Files (x86)/Microsoft Visual Studio 10.0/Common7/IDE/devenv.exe"		

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

	OPTIONS = [
		[ "--help", 		"-h", 	OS::Getopt::BOOLEAN, 	"display usage information." ],
		[ "--devenv", 		"-d", 	OS::Getopt::OPTIONAL, 	"dev env executable. eg. C:/Program Files (x86)/Microsoft Visual Studio 10.0/Common7/IDE/devenv.exe" ],
		[ "--sln", 		"-s", 	OS::Getopt::REQUIRED, 	"solution . eg. WorkBench.sln " ],
		[ "--build_type", 	"-bt", 	OS::Getopt::OPTIONAL, 	"rebuild or build " ],
		[ "--cfg", 		"-c", 	OS::Getopt::REQUIRED, 	"configuration eg. Release|x86." ],
		[ "--out_file", 	"-o", 	OS::Getopt::OPTIONAL, 	"outfile filename eg. devenv.txt" ],
		[ "--vs2008", 		"-v8", 	OS::Getopt::BOOLEAN, 	"build with Visual Studio 2008, default is vs2010" ],
	]

	begin
		g_AppName = File::basename( __FILE__, '.rb' )	
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, g_Trailing = OS::Getopt.getopts( OPTIONS )

		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			Process.exit!( 3 )
		end	
		
		if ( not opts['sln'] ) then
			$stderr.puts("Error : #{g_AppName} : no sln specified.")
			Process.exit!( 1 )
		end	
		
		if ( not opts['cfg'] ) then
			$stderr.puts("Error : #{g_AppName} : no build configuration specified.")
			Process.exit!( 2 )
		end			

		if (opts['vs2008'])
			devenv 		= opts['devenv'] ? opts['devenv'] : VS2008
		else
			devenv 		= opts['devenv'] ? opts['devenv'] : VS2010
		end
		
		sln 		= opts['sln'] ? opts['sln'] : "X:/gta5/src/dev/rage/framework/tools/src/ui/Workbench/WorkBench.sln"
		config 		= opts['cfg'] ? opts['cfg'] : "Release|x86"		
		out_file 	= opts['out_file'] ? opts['out_file'] : "devenv.txt"
		build_type	= opts['build_type'] ? opts['build_type'].gsub("/","") : "Rebuild"
		
		cmd		= "\"#{devenv}\" #{sln} /#{build_type} \"#{config}\" /Out #{out_file}"
		
		puts "#{cmd}"
		
		pid = IO.popen(cmd)	# the process runs in the background, hence all this jiggery pokery.
		
		puts "Process id #{pid} has started"
		pid.close # waits for the process to complete... simple!
		status = $?

		# spit out the file contents, _trusting_ that errors will be picked up by regex match rather than stream... pah.		
		File.open(out_file, "r") do |file|
			while (line = file.gets)
		  		puts "#{line}"
		  	end
		end

		puts "Process id #{pid} returned #{status}"
			
		if (File.exist?(out_file))
			FileUtils.mv(out_file, "#{out_file}.old") 
		end

		Process.exit! status		
	rescue Exception => ex
		$stderr.puts "Error: Unhandled exception: #{ex.message}"
		puts ex.backtrace().join("\n")
		Process.exit! -1
	end 	
	
	Process.exit! 0
end

# %RS_TOOLSLIB%/util/CruiseControl/devenv.rb