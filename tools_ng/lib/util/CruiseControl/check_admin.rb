#
# File:: check_admin.rb
# Description:: Is the user 'admin' for Cruise Control
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 16th October 2009
#

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/log/log'
require 'pipeline/os/getopt'

#
# DW - Do not edit this script this is mild security to prevent accidental or malicious launching of admin tasks in Cruise Control.
# Feel free to beef up the security if you wish!
#

authorised_usernames = ['derek','klaas.shilstra','greg.smith','david.muir','buildernorth','buildersan']
username =  ENV['USERNAME']
ret = authorised_usernames.include?(username) ? 0 : 1
if ret != 0
	$stderr.puts "Error - Access Denied : username ('#{username}') is not authorised to run this. Please ask #{authorised_usernames[0]} if you really need this." 
	$stderr.puts "Oh go on then have access for now..."
	ret = 0
end

exit ret