#
# File:: change_report_project_state_aggregate_poll.rb
# Description:: runs the batch file to generate xml & html project aggregate reports.
#    
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 11th March 2010
#
# Passed in  :-  see OPTIONS ...
# Passed out :-  
#                
# Returns    :-  

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/file'
include Pipeline
require 'systemu'
require 'xml'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	OPTIONS = [
		[ "--help", 		"-h", 	OS::Getopt::BOOLEAN, 	"display usage information." ],
		[ '--run', 		'-r', 	OS::Getopt::REQUIRED, 	'filename to execute' ],
		[ '--poll_interval', 	'-i', 	OS::Getopt::OPTIONAL, 	'seconds to wait till new poll' ],
		[ '--auto_refresh', 	'-a', 	OS::Getopt::OPTIONAL, 	'seconds to autorefresh report' ],
	]

	begin
		g_AppName = File::basename( __FILE__, '.rb' )	
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, g_Trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end

		g_Run 	= opts['run']
		g_PollInterval 	= opts['poll_interval'].nil? 	? 60 			: opts['poll_interval'].to_i		
		g_AutoRefresh 	= opts['auto_refresh'].nil? 	? (60*5) 		: opts['auto_refresh'].to_i

		g_Run += " --poll_interval #{g_PollInterval}"
		g_Run += " --auto_refresh #{g_AutoRefresh}"

		# Poll loop - can run just once.
		iterations = 0
		begin			
			puts("\n=== Run\##{iterations}. #{Time.now} #{g_Run} ===")
			status, stdout, stderr = systemu(g_Run)
			#puts("#{g_Run} returned #{status}")
			#if (status!=0)
			
				if (stderr.length > 0)
					$stderr.puts "\n\n\n#{stderr.length} ERRORS FOUND!\n\n\n"
				end
				stderr.each do |err|
					$stderr.puts("Error: #{err}")
				end
				
				stdout.each do |out|
					puts("#{g_Run} : #{out}")
				end				
			#end		
		
			if (g_PollInterval>=0)
				puts(" Sleep #{g_PollInterval} seconds...\n")
				Kernel.sleep(g_PollInterval)
			end
			iterations += 1
		end while (g_PollInterval>=0)
				
	rescue Exception => ex
		puts "Unhandled exception: #{ex.message}"
		puts ex.backtrace().join("\n")
	end 	
end

# %RS_TOOLSLIB%/util/CruiseControl/change_report_project_state_aggregate_poll.rb