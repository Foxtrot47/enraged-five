#
# File:: cctray_update.rb
# Description:: 
#
# https://devstar.rockstargames.com/wiki/index.php?title=Tool_Builder&action=edit&redlink=1
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 20th June 2011
#
# Passed in  :-  see OPTIONS ...
# Passed out :-  
#                
# Returns    :-  

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/os/path'
require 'pipeline/os/getopt'
require 'pipeline/log/log'
require 'xml'
require "rexml/document"
include REXML

include Pipeline
require 'sys/proctable'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

INFO_BLUE		= "[colourise=blue]INFO_MSG: "
HELP_URL		= "https://devstar.rockstargames.com/wiki/index.php?title=Tool_Builder&action=edit&redlink=1"
PROCESS_EXIT_SLEEP 	= 5
PROCESS_NAME		= "cctray.exe"

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

	OPTIONS = [
		[ "--help", 		"-h", 	OS::Getopt::BOOLEAN, 	"display usage information." ],
		[ '--cctray', 		'-c', 	OS::Getopt::OPTIONAL, 	"cctray file to update" ]
	]

	begin
		g_AppName = File::basename( __FILE__, '.rb' )	
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, g_Trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] )
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
		
		@config = Pipeline::Config.instance
		@config.project.load_config			
		
		cctray_type = opts['cctray']

		g_CCtrayFile = Pipeline::OS::Path.combine( @config.toolsconfig, 'CruiseControl', 'cctray', "cctray-settings-#{cctray_type}.xml" )		
		
		filename = File::expand_path( g_CCtrayFile )
		if (File.exist? filename)
			g_CCtrayFile = filename 
		else
			g_CCtrayFile = nil
			$stderr.puts("Error: filename #{filename} not found")
			Process.exit!(1)
		end

			
		@p4 = SCM::Perforce.new()
		@p4.port = @config.sc_server
		@p4.client = @config.sc_workspace
		@p4.user = @config.sc_username
		@p4.connect()
		@p4.run_sync( filename )														

		process_name = PROCESS_NAME.sub(".exe","_#{cctray_type}.exe")

		#---------------------------------------------------------------------
		# Kill process.
		#---------------------------------------------------------------------
		
		if (process_name)
			puts "Searching for process named: #{process_name}."

			num_found = 0

			#---------------------------------------------------------------------
			# Kill ALL and EVERY instance of this process.
			#---------------------------------------------------------------------	
			Sys::ProcTable.ps.each do |ps|
				if ps.name.downcase == process_name
					begin

						puts "Killing process named: #{process_name}."

						Process.kill('KILL', ps.pid)

						num_found += 1

					rescue Exception => ex
						$stderr.puts "Exception killing process #{ps.name} #{ps.pid} #{ex.message}."
						ret = -1
					end
				end	
			end
			
			#---------------------------------------------------------------------
			# Give the processes a chance to exit.
			#---------------------------------------------------------------------			  
			Kernel.sleep(PROCESS_EXIT_SLEEP) if (num_found>0)

			#---------------------------------------------------------------------
			# Check the process no longer exists.
			#---------------------------------------------------------------------		  
			Sys::ProcTable.ps.each do |ps|
				if ps.name.downcase == process_name
					$stderr.puts "Error: The process #{process_name} still exists."
					ret = -1
				end
			end
			puts "Killed #{num_found} processes named: #{process_name}"
		end			

		#cctray_exe_path = Pipeline::OS::Path.combine( @config.toolsbin, 'CruiseControl', process_name )
		#cctray_settings_path = Pipeline::OS::Path.combine( @config.toolsconfig, 'CruiseControl', 'cctray', "cctray-settings-#{cctray_type}.xml" )

		#system "start #{cctray_exe_path} #{cctray_settings_path}"
		#puts "Running #{cctray_exe_path} #{cctray_settings_path}"
		Process.exit! 0

	rescue Exception => ex
		puts "Unhandled exception: #{ex.message}"
		puts ex.backtrace().join("\n")
	end 	
end

# %RS_TOOLSLIB%/util/CruiseControl/cctray_update.rb