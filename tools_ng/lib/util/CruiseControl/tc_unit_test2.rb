#
# File:: unit_test.rb
# Description:: Handy example of how unit testing works, for use in testing Cruise Control
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 25th January 2010
#

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require "test/unit"

class SimpleNumber
	
	def initialize( num )
		raise unless num.is_a?(Numeric)
		@x = num
	end
	
	def add( y )
		@x + y
	end
	
	def multiply( y )
		@x * y
	end
	
end

class TestSimpleNumber < Test::Unit::TestCase
	
	def test_simple
		assert_equal(4, SimpleNumber.new(2).add(2) )
		assert_equal(4, SimpleNumber.new(2).multiply(2) )
	end
	
	def test_typecheck
		assert_raise( RuntimeError ) { SimpleNumber.new('a') }
	end
	
	def test_failure
		assert_equal(3, SimpleNumber.new(2).add(2), "Adding doesn't work" )
	end
	
end
