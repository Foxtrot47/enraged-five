#
# File:: change_report_history.rb
# Description:: builds a history xml file from the log files in a cruise control artifact directory
# 		can poll the directory for changes, although intended use is to have polling loop 
#               controlled at a higher level and the ChangeReportHistory script be called
#    
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 11th March 2010
#
# Passed in  :-  see OPTIONS ...
# Passed out :-  
#                
# Returns    :-  

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/file'
require 'pipeline/util/email'
include Pipeline
require 'systemu'
require 'xml'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

EMAIL_SPLIT_REGEXP = /[,|;]/
LIMITED_MODIFICATIONS = 1 #  most likely will want this limited since the xml file will grow way way too big with modifications


#log filenames to read. eg. N:\RSGEDI\Builders\CruiseControl\live\codebuilder_gta5_dev_ps3_build_bankrelease_new\buildlogs\log*.xml
		
# The Change Report History class
# - more optimisations are possible here with timestamps
class ChangeReportHistory

	def initialize()
	end

	# create within the class an xml representation of the history.
	def create( filenames, prev_filename, error_detect, custom_error_classify, ignore_path = nil )
		Pipeline::OS::Path::set_downcase_on_normalise( false )	
		changed = 0
		existing_filenames = {}
		filenames_in_xmldoc = {}
		previous_xmldoc = nil
		xml_node_files = []
		project_url = nil
		
		@ignore_path = ignore_path
		
		ChangeReportHistory::log().info("================= change report history started #{Time.now} =========================================================")
		ChangeReportHistory::log().info("Reading #{filenames.length} files, using prev filename of #{prev_filename}")	

		#Pipeline::OS::Path::set_downcase_on_normalise( true )		
		filenames.each { |filename| existing_filenames[OS::Path.get_filename(filename)] = "" if File::file?(filename) }
		#Pipeline::OS::Path::set_downcase_on_normalise( false )	
		
		ChangeReportHistory::log().info("#{existing_filenames.length} existing filenames")

		# cache the previous filenames in an array so that we can quickly determine if there are any changes. ( there may be quicker / better ways of doing this. )
		if prev_filename and File::file?(prev_filename)
			previous_xmldoc = LibXML::XML::Document::file( prev_filename )			
			history_node 	= previous_xmldoc.find_first("//history")									
			
			if (history_node.nil?)
				ChangeReportHistory::log().error("Error: Corrupt History file : #{prev_filename}")
				File.Delete(prev_filename)
				return 0
			end			
			
			xml_node_files 	= previous_xmldoc.find_first("//history/files")
			
			if (xml_node_files.nil?)
				ChangeReportHistory::log().error("Error: Corrupt History file : #{prev_filename}")
				File.Delete(prev_filename)
				return 0
			end
			
			nodes = previous_xmldoc.find("//history/files/file")
			
			if (nodes)
				nodes.each do |node| 	
					filename = node["name"]
					if (existing_filenames[filename] or existing_filenames[filename.downcase])				
						filenames_in_xmldoc[filename] = ""
					else
						node.remove!
						changed += 1
						ChangeReportHistory::log().info("#{filename} purged")
					end
				end
			end			
		else
			ChangeReportHistory::log().info("Previous file doesnt exist - which is ok.")
			previous_xmldoc 		= XML::Document.new()
			previous_xmldoc.encoding 	= XML::Encoding::UTF_8			
			previous_xmldoc.root 		= XML::Node.new( 'history' )				
			previous_xmldoc.root.attributes['project_url'] = ""	
			xml_node_files 			= XML::Node.new( 'files' ) 
			previous_xmldoc.root << xml_node_files						
		end

		# Get which filenames are new
		new_filenames = {}
		existing_filenames.each { |key,val| new_filenames[key] = "" unless filenames_in_xmldoc.has_key?(key) }

		# Add the new filenames
		len = new_filenames.length
		new_filenames.each_with_index do |(filename,dummy),idx|
			begin		
				ChangeReportHistory::log().info("Processing new file #{idx+1}/#{len} #{filename}")
				
				# the element to create
				element = XML::Node.new( 'file' )
				element.attributes['name'] = filename				

				# The xmldoc to parse
				xmldoc 		= LibXML::XML::Document::file( filename )				
				project_name    = xmldoc.find_first("//cruisecontrol/@project")
				modifications 	= xmldoc.find_first("//cruisecontrol/modifications")
				status 		= xmldoc.find_first("//cruisecontrol/integrationProperties/CCNetIntegrationStatus")				
				project_url 	= xmldoc.find_first("//cruisecontrol/integrationProperties/CCNetProjectUrl")
				last_cl		= xmldoc.find_first("//cruisecontrol/integrationProperties/LastChangeNumber").content
				build_date  	= xmldoc.find_first("//cruisecontrol/build/@date")
				build_duration  = xmldoc.find_first("//cruisecontrol/build/@buildtime")								

				# setup the new element
				@project_url = project_url ? project_url.content.sub("ViewProjectReport.aspx","") : "unknown"
				element.attributes['status'] 		= status ? status.content 		: "unknown"
				element.attributes['build_date'] 	= build_date ? build_date.value 	: "unknown"
				element.attributes['build_duration'] 	= build_duration ? build_duration.value : "unknown"								
				project_name = project_name ? project_name.value : "unknown"

				# Look for specific errors and send to specific users
				error_detection(error_detect, xmldoc, build_date, filename, project_url, project_name, last_cl, element) if (error_detect)

				custom_error_classification(custom_error_classify, xmldoc, build_date, filename, project_url, project_name, last_cl, element) if (custom_error_classify)

				if (modifications)

					# Todo : DW - remove superfluous fields and long comments!
					modifications_element = XML::Node.new( 'modifications' )
					mods =  modifications.find("modification")
					changes = {}
					mods.each do |m|

						next if ignore_mod(m)

						node = m.find_first("changeNumber")
						change_number = node.content

						if changes[change_number.to_s].nil?
							changes[change_number] = 1 
						else
							changes[change_number] += 1
						end			
						
						if changes[change_number].to_i <= LIMITED_MODIFICATIONS
							ChangeReportHistory::log().info("CL #{change_number}")
							modifications_element << m.copy(true) 						
						end
					end				

					element << modifications_element																										
				else
					ChangeReportHistory::log().error("error no modifications found in #{filename}")
				end

				xml_node_files << element
				changed +=1				
	
			rescue Exception => ex
				$stderr.puts "Error: Unhandled exception: #{ex.message}"
				$stderr.puts ex.backtrace().join("\n")
				$stderr.puts "This exception handler is here because corrupt XML file have been known to occur in which case it should be safe to ignore them, rather than hit to top level exception handler."
			end 										
		end		
		
		if (changed>0)
			ChangeReportHistory::log().info("Saving #{changed} changes in #{prev_filename}")								
			previous_xmldoc.root.attributes['project_url'] = @project_url if project_url
			
			
			if (File.exist?(prev_filename))
				modTimeBefore = File.mtime(prev_filename) 		
				previous_xmldoc.save(prev_filename)
				modTimeAfter = File.mtime(prev_filename)
				if (modTimeBefore==modTimeAfter)
				
					ChangeReportHistory::log().info("Deleting file as it didn't update!")
					# delete the file, there is something wrong with it.
					File.Delete(prev_filename)
				end
			else			
			
				previous_xmldoc.save(prev_filename)
			end			
			
			ChangeReportHistory::log().info("Saved #{prev_filename}")
		end		

		ChangeReportHistory::log().info("================= change report history finished #{Time.now} =========================================================")
		
		Pipeline::OS::Path::set_downcase_on_normalise( true )	
		
		return changed		
	end
			
	#------------------------------------------------------------------------------------------
	# return true if this modification should be ignored
	def ignore_mod(mod)		
		node = mod.find_first("project")		
		folder = mod.find_first("project").content if node
		folder = mod.find_first("FolderName").content unless folder
		return true if @ignore_path and folder and folder.include?(@ignore_path) 
		return false
	end
	
	#
	# read from file the email addresses
	def get_emails_from_files(email_addresses)
		email_addresses_from_file = []
		email_addresses.each do |file|
			file_search = file.split"|"
			if file_search.length==2
				file 	= file_search[0]
				search	= file_search[1]
				if (file and search and File.exist?(file))
					xml = File.read(file)
					doc = REXML::Document.new(xml)
					nodes = doc.find("//cb:config-template/cb:define/@#{search}")					
					nodes.each { |node| email_addresses_from_file << node.content.split(EMAIL_SPLIT_REGEXP) if node.content }
				end
			end
		end	
		email_addresses_from_file
	end
	
	
	# special error code needing identified and promoted to the details field
	def custom_error_classification(custom_error_classify, xmldoc, build_date, filename_short, project_url, project_name, last_cl, element)
		cmds = custom_error_classify.split(",")
		cmds.each do |cmd|				
			err_cmd = cmd.split(" ")

			title  		= err_cmd[0]					
			detail_out  	= err_cmd[1]
			regexp 		= Regexp.new(err_cmd[2])

			errors = xmldoc.find("//cruisecontrol/build/buildresults/message[@level='Error']")

			matched_errors = []
			errors.each do |error|
				if (error.content =~ regexp)
					puts "     ***Err discovered :#{error.content}"
					matched_errors << error.content	
					element.attributes['details'] = detail_out
					# break here?
				end
			end			
		end
	end		
	
	
	# custom errors detected and emailed to specific users 
	# these error might make it to the report history too since they have specific value.
	def error_detection(error_detect, xmldoc, build_date, filename_short, project_url, project_name, last_cl, element)
		err_cmd = error_detect.split(" ")

		title  		= err_cmd[0]					
		regexp 		= Regexp.new(err_cmd[1])
		email  		= err_cmd[2]	
		stale_age	= err_cmd[3].to_i

		emails = []					
		email_addresses = email.split(EMAIL_SPLIT_REGEXP)
		
		# read from file the email addresses
		email_addresses_from_file = get_emails_from_files(email_addresses)	
		email_addresses += email_addresses_from_file
				
		email_addresses.each { |email_addr| emails << { :address => email_addr } }

		errors = xmldoc.find("//cruisecontrol/build/buildresults/message[@level='Error']")

		matched_errors = []
		errors.each do |error|
			if (error.content =~ regexp)
				puts "     ***Err discovered :#{error.content}"
				matched_errors << error.content						
			end
		end

		build_time = Time.parse(build_date.value)								
		time_diff = Time.now-build_time					
		if (matched_errors.length>0)					
			if (time_diff > stale_age)
				puts "    *** Silenced error - it was too long ago ***"
			else
				puts "Error detected #{time_diff.to_s} secs ago"
			
				begin
				
					email_options = {
						:server => Pipeline::Config::instance().mailserver,
						:port => Pipeline::Config::instance().mailport.to_i,
						:username => nil,
						:password => nil,
						:domain => Pipeline::Config::instance().maildomain,
						:auth_mode => :plain	
					}

					email = Util::Email.new( email_options )												

					email.send( 	{ 	:address => "do_not_reply@#{email_options['domain']}", 
								:alias => title },
								emails,						   
								title + ": #{project_name}",
								error_html(filename_short, project_url, build_time, matched_errors, project_name, errors.length, last_cl),
								'text/html' )																		
				rescue Exception => ex
					$stderr.puts "Error: Unhandled exception: #{ex.message}"
					$stderr.puts ex.backtrace().join("\n")
				end 	
			end
			
			# add to history since these errors are of interest.
			error_string = ""
			matched_errors.each { |err| error_string +="#{err}\n" }
			element.attributes['details'] = error_string
		end		
	end	
		
	# returns an html string for composing an html report email.
	def error_html(filename_short, project_url, build_time, matched_errors, project_name, errors_length, last_cl)
	
		error_string = ""
		matched_errors.each_with_index { |err, idx| error_string +="<pre style=\"color: darkred\">#{idx+1}. #{err}</pre>\n" }
		title = "Cruise Control - Rockstar Custom Error Report"
		bg_col = "#999"
		fg_col = "#FFF"	
	
		"<html>
			<body>						
				<table border=\"1\" width=\"100%\">
					<tr><td colspan=\"2\" align=\"center\" style=\"background-color: #{bg_col}; color: #{fg_col}; font-size: 16pt\">#{title}</td></tr>	
					<tr>
						<td style=\"background-color: #{bg_col}; color: #{fg_col}\">Project</td>
						<td>
							<a href=\"#{@project_url}/build/#{filename_short}/ViewBuildReport.aspx\">#{project_name}</a>
						</td>
					</tr>
					<tr>
						<td style=\"background-color: #{bg_col}; color: #{fg_col}\">Build Time</td>
						<td>#{build_time.strftime("%H:%M:%S %d-%m-%Y")}</td>						
					</tr>
					<tr>
						<td style=\"background-color: #{bg_col}; color: #{fg_col}\">Detection Time</td>
						<td>#{Time.now.strftime("%H:%M:%S %d-%m-%Y")}</td>
					</tr>
					<!-- removed as it can be ambiguous <tr>
						<td style=\"background-color: #{bg_col}; color: #{fg_col}\">Last Changelist</td>
						<td>#{last_cl}</td>
					</tr> -->					
					<tr>
						<td style=\"background-color: #{bg_col}; color: #{fg_col}\">Errors</td>
						<td style>#{matched_errors.length} reported of #{errors_length} total errors</td>
					</tr>
				</table>
				<br/>
				<table>
					<tr>
						<td>#{error_string}</td>
					</tr>
				</table>				
				
			</body>
		</html>"
	end
	
	def self.log( )
		@@log = Log.new( 'change_report' ) if ( @@log.nil? )
		@@log
	end	

	@@log = nil		
end

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	OPTIONS = [
		[ "--help", 		"-h", 	OS::Getopt::BOOLEAN, 	"display usage information." ],
		[ '--dest_filename', 	'-d', 	OS::Getopt::OPTIONAL, 	'filename to output results to. eg. history.xml' ],
		[ '--src_wildcard', 	'-w', 	OS::Getopt::OPTIONAL, 	'filename wildcard to read to. eg. log*.xml <- default' ],
		[ '--prev_filename', 	'-p', 	OS::Getopt::OPTIONAL, 	'filename that contains a previous history so that no unneccessary processing is required. eg. history.xml - it can be the same as the dest filename.' ],
		[ '--poll_interval', 	'-i', 	OS::Getopt::OPTIONAL, 	'seconds to wait till new poll' ],
		[ '--error_detect', 	'-e', 	OS::Getopt::OPTIONAL, 	'error regexp to detect with emails to inform' ],
		[ '--custom_error_classify', 	'-c', 	OS::Getopt::OPTIONAL, 	'error regexp to classify types of error' ],
		
	]

	begin
		g_AppName = File::basename( __FILE__, '.rb' )	
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, g_Trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end

		g_DestFilename 		= opts['dest_filename'].nil? 	? "history.xml" 	: opts['dest_filename']
		g_PrevFilename 		= opts['prev_filename'].nil? 	? nil 			: opts['prev_filename']
		g_Wildcard 		= opts['src_wildcard'].nil? 	? "log*.xml" 		: opts['src_wildcard']
		g_PollInterval 		= opts['poll_interval'].nil? 	? -1 			: opts['poll_interval'].to_i		
		g_ErrorDetect		= opts['error_detect'].nil? 	? nil 			: opts['error_detect']
		g_CustomErrorClassify	= opts['custom_error_classify'].nil? 	? nil 		: opts['custom_error_classify']
		

		# Poll loop - can run just once.
		begin			
			g_Filenames = []

			Pipeline::OS::Path::set_downcase_on_normalise( false )
			g_Trailing.each_with_index do |filename, index|				
				filename = File::expand_path( filename )
				g_Filenames += OS::FindEx::find_files( OS::Path::combine( filename, g_Wildcard ) )
			end
			Pipeline::OS::Path::set_downcase_on_normalise( true )

			# Convert
			change_report_history = ChangeReportHistory.new()
			
			changed = change_report_history.create( g_Filenames, g_PrevFilename, g_ErrorDetect, g_CustomErrorClassify )
			ChangeReportHistory::log().info("#{changed} changes detected")
			
			if changed > 0 				
				ChangeReportHistory::log().info("No updates made.")
			end
			
			if (g_PollInterval>=0)
				ChangeReportHistory::log().debug("Sleeping #{g_PollInterval} seconds...")
				Kernel.sleep(g_PollInterval)
			end
		end while (g_PollInterval>=0)
				
	rescue Exception => ex
		puts "Unhandled exception: #{ex.message}"
		puts ex.backtrace().join("\n")
	end 	
end

# %RS_TOOLSLIB%/util/CruiseControl/change_report_history.rb