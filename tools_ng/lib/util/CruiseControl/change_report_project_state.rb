#
# File:: change_report_project_state.rb
# Description:: Takes a history, modifications & pending modifications xml file and produces an aggregate XML report of them.
# Where there is overlap between each file ( which is possible due to the fact there is no clean up of modifications.xml - and nor should there be ) this overlap is resolved
# Hence the reason for doing this in script not XSLT.
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 11th March 2010
#
# Passed in  :-  see OPTIONS ...
# Passed out :-  
#                
# Returns    :-  

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/os/getopt'
require 'pipeline/os/file'
include Pipeline
require 'xml'
require 'util/cruisecontrol/change_report_history'
require 'time'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------


# The Change Report Project State class (aggregate)
class ChangeReportProjectState

	#------------------------------------------------------------------------------------------
	# constants
	REGEXP_BUDDY = /(.*)Buddy(\s*):(\s*)(.*)$/i
	CHANGE_KEYS = ["number","comment","buddy","user","url","modified_date"]
	BLANK = "-"
	RUNNING_WORK_UNIT_STATUS = "running"
	PENDING_WORK_UNIT_STATUS = "pending"

	#------------------------------------------------------------------------------------------
	#
	def initialize()
		@run_time = Time.now.strftime("%Y-%m-%d %H:%M:%S")
	end

	#------------------------------------------------------------------------------------------
	# create an aggregate report of the filenames passed in.
	def create( project_name, history_filename, mod_filename, pending_mod_filename, ignore_path)
		
		ChangeReportProjectState::log().debug("================= #{project_name} #{@run_time} =========================================================")
		
		# these work units is what will ultimately be written to a report.
		@xml_node_work_units    = XML::Node.new( 'work_units' )		
		@project_name = project_name
		@all_history_changes = []
		@project_url = "unknown"
		@ignore_path = ignore_path
		
		#========================================================================================
		# Add history xml work units.
		if (File::file?(history_filename) )			
			history_xmldoc 		= LibXML::XML::Document::file( history_filename )
			
			node = history_xmldoc.find_first("//history")			
			@project_url = node.attributes["project_url"] if node			
			
			history_nodes		= history_xmldoc.find("//history/files/file") # each being a unit of work.			
			history_nodes.each { |node| add_history_work_unit( node ) }			
		else
			ChangeReportProjectState::log().error("No file #{history_filename}") 		 	
		end
		
		#========================================================================================
		# Add running xml work units. - dont add if already processed in history.
		mod_filenames = mod_filename.split(",")
		
		mod_filenames.each do |mod_filename|
			if (File::file?(mod_filename) ) 
				mod_xmldoc 		= LibXML::XML::Document::file( mod_filename )
				mod_nodes 		= mod_xmldoc.find("//ArrayOfModification")			
				mod_nodes.each { |node| add_running_work_unit( node ) }
				break
			else
				ChangeReportProjectState::log().error("No file #{mod_filename}") 				
			end
		end
		
		#========================================================================================
		# Add pending xml work units - each CL is a potential workunit.		
		if (File::file?(pending_mod_filename) )
			pending_mod_xmldoc 	= LibXML::XML::Document::file( pending_mod_filename )							
			pending_mod_nodes 	= pending_mod_xmldoc.find("//ArrayOfModification")			
			pending_mod_nodes.each { |node| add_pending_work_units( node ) }
		else			
			ChangeReportProjectState::log().error("No file #{pending_mod_filename}") 	 
		end
						
		true
	end
	
	#------------------------------------------------------------------------------------------
	# save file
	def save( filename )
		#FileUtils::mkdir_p( OS::Path::get_directory( filename ) ) unless ( File::directory?( filename ) )

		ChangeReportProjectState::log().info( "Saving (#{filename})." )
		xmldoc = to_xml( )
		xmldoc.save( filename)
	end	

	#------------------------------------------------------------------------------------------
	# get xmldoc
	def to_xml(  )
		xmldoc = XML::Document.new
		xmldoc.encoding = XML::Encoding::UTF_8
		xmldoc.root = XML::Node.new( 'project_state' )
		xmldoc.root.attributes['project_name'] = @project_name
		xmldoc.root.attributes['run_time'] = @run_time
		xmldoc.root.attributes['project_url'] = @project_url
		xmldoc.root << @xml_node_work_units
		xmldoc				
	end	

	#------------------------------------------------------------------------------------------
	# log accessor
	def self.log( )
		@@log = Log.new( 'change_report_project_state' ) if ( @@log.nil? )
		@@log
	end	
	
protected

	#------------------------------------------------------------------------------------------
	# create a hash of the essential parts of modification node we are interested in.
	def get_change( mod, changeNumber )

		node = mod.find_first("comment")
		node = mod.find_first("Comment") if ( not node )
		comment = node ? node.content : BLANK

		node = mod.find_first("user")
		node = mod.find_first("UserName")  if ( not node )
		user = node ? node.content : BLANK

		node  = mod.find_first("url")
		node = mod.find_first("Url")  if ( not node )
		url = node ? node.content : BLANK

		node = mod.find_first("ModifiedTime")
		node = mod.find_first("date")  if ( not node )
		node = mod.find_first("Date")  if ( not node )					
		if node  		
			modified_date = node.content			
			time = Time.parse(modified_date)

			#bug fix for timezones across p4 servers ( bit o a hack I'm afraid for now )
#			if (url.include?("rsgedip4p1"))				
#				time = time + (60 * 60 * 8) # ( Daylight saving change between Edinburgh and San Diego				
#			end			
			modified_date = time.strftime("%Y-%m-%d %H:%M:%S")		
		else
			modified_date = BLANK
		end
		

		buddy = (comment =~REGEXP_BUDDY)? $4 : BLANK

		{
			"number" => changeNumber,
			"comment" => comment, 
			"buddy" => buddy,
		 	"user" => user,
			"url" => url,
			"modified_date" => modified_date,
		} 
	end

	#------------------------------------------------------------------------------------------
	# create an xml node of a change from the custom hash format as return by get_change() above.
	def get_change_node( change )
		change_node = XML::Node.new( 'change' )
		CHANGE_KEYS.each { |key| change_node.attributes[key] = change[key] }
		change_node
	end

	#------------------------------------------------------------------------------------------
	# return true if this modification should be ignored
	def ignore_mod(mod)
		node = mod.find_first("project")		
		folder = mod.find_first("project").content if node
		folder = mod.find_first("FolderName").content unless folder
		return true if @ignore_path and folder and folder.include?(@ignore_path) 
		return false
	end

	#------------------------------------------------------------------------------------------
	# add a history workunit, storing a history of what has been added ( so that running workunits will not be considered running if in history )
	def add_history_work_unit( node )
		
		mods = node.find("modifications/modification")

		changes = {}
		changes_node = XML::Node.new( 'changes' )	
		
		mods.each do |mod|
		
			next if ignore_mod(mod)
					
			changeNumber = mod.find_first("changeNumber").content
			changes[changeNumber] = get_change( mod, changeNumber ) if not changes[changeNumber]
			@all_history_changes << changeNumber if not @all_history_changes.include?(changeNumber)
		end

		changes.each_pair do |change_id,change|					
			changes_node << get_change_node(change)
		end

		work_unit    = XML::Node.new( 'work_unit' )
		work_unit.attributes['status'] 		= node.attributes['status']
		work_unit.attributes['build_date'] 	= node.attributes['build_date']
		work_unit.attributes['build_duration'] 	= node.attributes['build_duration']
		work_unit.attributes['details'] 	= node.attributes['details'] if node.attributes['details']
		work_unit.attributes['log_name'] 	= node.attributes['name']
		work_unit << changes_node

		@xml_node_work_units << work_unit	
	end
	
	#------------------------------------------------------------------------------------------
	# add a pending workunit
	# very tricky code - sorry! I will come back to try to make clearer
	def add_pending_work_units( node )	

		mods = node.find("Modification")			

		changes = {}
		mods.each do |mod|
		
			next if ignore_mod(mod)

			changeNumber = mod.find_first("ChangeNumber").content			
			changes[changeNumber] = get_change( mod, changeNumber ) if ( not changes[changeNumber] )
		end

		changes.each_pair do |change_id, change|				
			change_node = get_change_node(change)						

			changes_node = XML::Node.new( 'changes' )
			changes_node << change_node
			
			work_unit = XML::Node.new( 'work_unit' )
			work_unit.attributes['status'] 		= PENDING_WORK_UNIT_STATUS 		
			work_unit.attributes['build_date'] 	= change_node.attributes["modified_date"]			
			work_unit.attributes['build_duration'] 	= BLANK	
			work_unit.attributes['details'] 	= BLANK	
			work_unit.attributes['log_name'] 	= BLANK
			work_unit << changes_node

			@xml_node_work_units << work_unit
		end							
	end

	#------------------------------------------------------------------------------------------
	# add a running work unit.
	def add_running_work_unit( node )

		mods = node.find("Modification")			

		changes_node = XML::Node.new( 'changes' )
		changes = {}
		mods.each do |mod|
		
			next if ignore_mod(mod)
		
			changeNumber = mod.find_first("ChangeNumber").content
			if ( not changes[changeNumber] )
				if (@all_history_changes and @all_history_changes.include?(changeNumber) ) # handles files already processed.
					# skipped since this running change has now been processed
				else
					changes[changeNumber] = get_change( mod, changeNumber )
				end
			end
		end

		work_unit    = XML::Node.new( 'work_unit' )
		work_unit.attributes['status'] = RUNNING_WORK_UNIT_STATUS
		work_unit.attributes['build_date'] 	= @run_time
		work_unit.attributes['build_duration'] 	= BLANK
		work_unit.attributes['details'] 	= BLANK
		work_unit.attributes['log_name'] 	= BLANK

		changes.each_pair do |change_id,change|					
			changes_node << get_change_node(change)
		end

		work_unit << changes_node

		@xml_node_work_units << work_unit					
	end

	@@log = nil		
end

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

	OPTIONS = [
		[ "--help", 			"-h", 	OS::Getopt::BOOLEAN, 	"display usage information." ],
		[ '--dest_filename', 		'-d', 	OS::Getopt::OPTIONAL, 	'filename to output results to. eg. project_state.xml' ],
		[ '--src_history', 		'-h', 	OS::Getopt::OPTIONAL, 	'the history file. eg. history.xml'],
		[ '--src_modifications',	'-m', 	OS::Getopt::OPTIONAL, 	'the modifications file eg. modifications.xml' ],
		[ '--src_modifications_pending','-p', 	OS::Getopt::OPTIONAL, 	'the pending modifications file eg. modificationsPending.xml' ],
		[ '--poll_interval', 		'-i', 	OS::Getopt::OPTIONAL, 	'seconds to wait till new poll' ],
		[ '--poll_history_changes', 	'-c', 	OS::Getopt::BOOLEAN, 	'poll for a change in history before rebuilding the report' ],
		[ '--wildcard', 		'-w', 	OS::Getopt::OPTIONAL, 	"wildcard of reports to build history from eg. 'log*.xml' ( default )" ],
		[ '--project_name', 		'-n', 	OS::Getopt::OPTIONAL, 	'the project name' ],
		[ '--error_detect', 		'-e', 	OS::Getopt::OPTIONAL, 	'error regexp to detect with emails to inform' ],
		[ '--custom_error_classify', 	'-c', 	OS::Getopt::OPTIONAL, 	'error regexp to classify particular errors : cannot be used in conjunction with error_detect option' ],		
		[ '--ignore_path', 		'-id', 	OS::Getopt::OPTIONAL, 	'description if found in CL will be ignored.' ],
		
	]

	begin
		g_AppName = File::basename( __FILE__, '.rb' )
	
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, g_Trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
		g_DestFilename 			= opts['dest_filename'].nil? 			? "project_state.xml" 	: opts['dest_filename']
		g_HistoryFilename 		= opts['src_history'].nil? 			? nil 			: opts['src_history']
		g_ModificationsFilename 	= opts['src_modifications'].nil? 		? nil 			: opts['src_modifications']
		g_PendingModificationsFilename 	= opts['src_modifications_pending'].nil? 	? nil 			: opts['src_modifications_pending']
		g_Wildcard			= opts['wildcard'].nil?				? "log*.xml"		: opts['wildcard']
		g_ProjectName			= opts['project_name'].nil?			? "Cruise Control project" : opts['project_name']
		g_ErrorDetect			= opts['error_detect'].nil? 			? nil 			: opts['error_detect']
		g_CustomErrorClassify		= opts['custom_error_classify'].nil? 		? nil 			: opts['custom_error_classify']
		g_IgnorePath			= opts['ignore_path'].nil? 			? nil 			: opts['ignore_path']

		g_Filenames = []

		Pipeline::OS::Path::set_downcase_on_normalise( false )
		g_Trailing.each_with_index do |filename, index|	
			filename = File::expand_path( filename )
			path = OS::Path::combine( filename, g_Wildcard )
			ChangeReportProjectState::log().info("looking for logfiles in #{path}")
			g_Filenames += OS::FindEx::find_files( path )
		end	
		Pipeline::OS::Path::set_downcase_on_normalise( true )

		ChangeReportProjectState::log().info("No log files found") if g_Filenames.length == 0 		

		change_report_history = ChangeReportHistory.new()
		if ( change_report_history.create( g_Filenames, g_HistoryFilename, g_ErrorDetect, g_CustomErrorClassify, g_IgnorePath ) > 0 )
			#change_report_history.save( g_HistoryFilename )
		end

		#OK even if the history doesn't update there is still a possibility the modifications and / or the pending modifications will be updated, so we run regardless.
		change_report_project_state = ChangeReportProjectState.new()
		if ( change_report_project_state.create( g_ProjectName, g_HistoryFilename, g_ModificationsFilename, g_PendingModificationsFilename, g_IgnorePath ) )
			change_report_project_state.save( g_DestFilename )
		end
						
	rescue Exception => ex
		puts "Unhandled exception: #{ex.message}"
		puts ex.backtrace().join("\n")	
	end 	
end

# %RS_TOOLSLIB%/util/CruiseControl/change_report_project_state.rb