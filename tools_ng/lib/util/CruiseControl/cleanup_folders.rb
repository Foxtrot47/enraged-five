require 'fileutils'

def cleanupbacklog( directory, historycnt )
   result = Array.new
   Dir.glob("#{directory}/*") do |file|
     if File.directory? file
       result << file
     end
   end
   result = result.sort()
   result = result.reverse()
   if result.size > historycnt
	numtoremove = result.size - historycnt
	removeid = result.size-1
	while numtoremove > 0
	  puts "removing " + result[removeid]
	  FileUtils.rm_rf(result[removeid])
	  removeid = removeid - 1
	  numtoremove = numtoremove - 1
	end
   end
   puts "keeping folders:"
   puts result
   result
end


puts "\n********************************************\n"
puts "usage: <path to look in to cleanup> <number of old folders to keep>"
puts "e.g. N:\\RSGLDS\\Builders\\CruiseControl\\live\\codebuilder_jimmy_dev_ps3_rebuild_bankrelease\\builds 5"
puts "********************************************\n"
changepath = ARGV[0].gsub("\\","/")
puts "arg0: " + changepath
puts "arg1: " + ARGV[1]
puts "\n"
cleanupbacklog(changepath, ARGV[1].to_i)