#
# File:: %RS_TOOLSLIB%/util/file_template_processor.rb
# Description:: Generic template file processor for environment substitution.
#               Process an input file line-by-line substituting in the current
#               tools branch extended environment variable where applicable.
#               Nice.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 17 June 2010
#
# This is a better version of the registry extensions template processor;
# which should be able to replace it as its more generic.
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
Pipeline::Config::instance()::logtostdout = true
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/scm/perforce'
require 'pipeline/util/environment'
include Pipeline
require 'progressbar'

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--input', '-i', OS::Getopt::REQUIRED, 'input file to process as template.' ],
	[ '--output', '-o', OS::Getopt::REQUIRED, 'output file.' ],
	[ '--native', '-n', OS::Getopt::BOOLEAN, 'use platform native paths in environment.' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]

#----------------------------------------------------------------------------
# Monkey-Patching
#----------------------------------------------------------------------------
module Pipeline
	
	# Monkey-patch base Config class to allow direct access to the 
	# environment object.
	class Config
		attr_reader :environment
	end
	
	# Monkey-patch Environment class to allow direct access to the symbol
	# table storage.
	class Environment
		attr_reader :symbol_table
	end
end # Pipeline module

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

# Fill environment object with some additional variables.
def fill_env( log, config, env )
	env.push( )
	
	# Determine Perforce mapping.
	current_dir = Dir::pwd( )
	begin
		Dir::chdir( config.toolsroot )
		p4 = SCM::Perforce.new( )
		p4.connect( )
		p4_toolsroot = OS::Path::get_directory( p4.local2depot( OS::Path::combine( config.toolsroot, 'config.xml' ) ) )
		log.info( "RS_TOOLSROOT: #{config.toolsroot}" )
		log.info( "P4 TOOLSROOT: #{p4_toolsroot}" )
		p4.disconnect( )
		
		equivalent_toolsroot = Pipeline::Globals::instance( ).get_equivalent_branch_toolsroot()
		Dir::chdir( equivalent_toolsroot )
		p4_ = SCM::Perforce.new( )
		p4_.connect( )
		p4_equivalent_toolsroot = OS::Path::get_directory( p4_.local2depot( OS::Path::combine( equivalent_toolsroot, 'config.xml' ) ) )
		log.info( "EQ TOOLSROOT: #{equivalent_toolsroot}" )
		log.info( "EQ P4 TOOLSROOT: #{p4_equivalent_toolsroot}" )
		p4_.disconnect( )
		
		if ( Pipeline::Globals::instance().is_dev?() ) then
			env.add( 'p4_toolsroot_dev', p4_toolsroot )
			env.add( 'p4_toolsroot_rel', p4_equivalent_toolsroot )
		else
			env.add( 'p4_toolsroot_dev', p4_equivalent_toolsroot )
			env.add( 'p4_toolsroot_rel', p4_toolsroot )
		end
	ensure
		Dir::chdir( current_dir )
	end		
end

#----------------------------------------------------------------------------
# Entry-Point
#----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then
	g_Log = Log::new( OS::Path::get_basename( __FILE__ ) )
	g_Config = Pipeline::Config::instance( )
	g_Project = g_Config.projects[ENV['RS_PROJECT']]
	g_Project.load_config( )
	g_Opts, g_Trail = OS::Getopt::getopts( OPTIONS )
	
	begin
		# Option Validation.
		if ( g_Opts['help'] ) then
			puts OS::Getopt::usage( OPTIONS )
			exit( 1 )
		end
		
		g_Env = Environment::new( )
		g_Project.fill_env( g_Env )
		
		if ( g_Opts['native'] ) then
			# We copy the root environment but ensure all paths are in the native
			# OS format so we don't have '/' in the registry.
			g_Env.symbol_table.each do |var, val|
				next unless ( val.include?( '\\' ) or val.include?( '/' ) )				
				g_Env.symbol_table[var] = OS::Path::platform_native( val ).gsub( '\\', '\\\\\\\\\\' )
			end
		else
			# We don't manipulate the environment and leave our environment as-is.
		end
		fill_env( g_Log, g_Config, g_Env )
		g_Env.list( )
		
		# Input File Processing
		input_file = g_Opts['input']
		output_file = g_Opts['output']
		
		data = []
		g_Log.info( "Processing template: #{input_file}..." )
		File::open( input_file, 'r' ) do |fp|
			data = fp.readlines	
		end
		
		pbar = ProgressBar::new( "Generating", data.size )
		File::open( output_file, 'w' ) do |fp|
			data.each_with_index do |line, index|
				if ( line.include?( '[' ) ) then
					fp.puts( line )
				else
					fp.puts( g_Env.subst( line.gsub( '\\', '\\\\\\' ) ) )
				end
				pbar.inc( )
			end
		end
		pbar.finish( )
		
	rescue SystemExit => ex
		exit( ex.status )
		
	rescue Exception => ex
		g_Log.exception( ex, 'Unhandled exception' )
	end
end

# %RS_TOOLSLIB%/util/file_template_processor.rb
