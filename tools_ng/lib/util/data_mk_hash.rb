#
# File:: data_mk_hash.rb
# Description:: Create RAGE hash code from a String.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 6 January 2010
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/util/rage'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--help', '-h', Getopt::BOOLEAN, 'display usage information.' ],
	[ '--algorithm', '-a', Getopt::REQUIRED, 'set hashing algorithm (u32, u16, s16, s32).' ],
]
TRAILING = {
	'string' => 'string to hash'
}


#
# Monkey patch integer class
# Copied from Derek Ward's enum_maker.rb
class Integer
	def to_s32
		self >= (2**31) ? self - (2**32) : self
	end
end


#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

	begin
		options, trailing = OS::Getopt::getopts( OPTIONS )
		if ( options['help'] ) then
			puts OS::Getopt::usage( OPTIONS, TRAILING )
			exit( 1 )
		end
		
		data = trailing.join( ' ' )
		algorithm = options['algorithm'].to_sym if ( options.has_key?( 'algorithm' ) )
		algorithm = :u32 if ( algorithm.nil? )
	
		g = Globals::instance( )
		p = Pipeline::Config::instance().projects[g.toolsproject]
		r = RageUtils::new( p )
	
		case algorithm
		when :u16
			puts r.rage.atHash16U( data ).to_s
		when :s16
			puts r.rage.atHash16( data ).to_s
		when :s32
			puts r.rage.atStringHash( data ).to_s32().to_s
		else
			puts r.rage.atStringHash( data ).to_s
		end
	
	rescue Exception => ex
	
		puts "Unhandled exception: #{ex.message}."
		puts ex.backtrace.join( "\n" )
	end
end

# data_mk_hash.rb
