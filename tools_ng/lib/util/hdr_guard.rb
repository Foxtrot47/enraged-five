require 'find'

#"X:/gta5/src/dev/game/ai",
# "X:/gta5/src/dev/rage"

dirs = ["X:/gta5/src/dev/rage/base/src/vector"] 

#dirs = ["X:/gta5/src/dev/rage/framework/src/fwaudio", "X:/gta5/src/dev/rage/framework/src/fwcontrol", "X:/gta5/src/dev/rage/framework/src/fwdebug", "X:/gta5/src/dev/rage/framework/src/fwdrawlist", "X:/gta5/src/dev/rage/framework/src/fwevent", "X:/gta5/src/dev/rage/framework/src/fwmaths", "X:/gta5/src/dev/rage/framework/src/fwnet"] 
#dirs = ["X:/gta5/src/dev/rage/framework/src/fwrenderer" ] 
#dirs = ["X:/gta5/src/dev/rage/framework/src/fwpheffects", "X:/gta5/src/dev/rage/framework/src/fwscene", "X:/gta5/src/dev/rage/framework/src/fwscript", "X:/gta5/src/dev/rage/framework/src/fwsys"] 
#dirs = ["X:/gta5/src/dev/rage/framework/src/fwtl", "X:/gta5/src/dev/rage/framework/src/fwutil", "X:/gta5/src/dev/rage/framework/src/fwvehicleai", "X:/gta5/src/dev/rage/framework/src/phframework", "X:/gta5/src/dev/rage/framework/src/streaming", "X:/gta5/src/dev/rage/framework/src/timecycle", "X:/gta5/src/dev/rage/framework/src/vfx"] 

excludes = ["none"]
header_guard = "\#pragma once // Added by hdr_guard.rb "
files_found = 0
files_changed = 0

# monkey patch string class
class String  
	def starts_with?(characters)      
		self.match(/^#{characters}/) ? true : false  
	end
end

# Entry Point
begin
	for dir in dirs
	  Find.find(dir) do |path|
	    if FileTest.directory?(path)
	      if excludes.include?(File.basename(path))
		Find.prune
	      else
		next
	      end
	    else      
	      if ( File.extname(path) == ".h"and not File.basename(path).include?("pch") )		
		
		contents = File.read(path) 
		files_found += 1
		if ( not contents.starts_with?(header_guard))
			files_changed += 1
			File.chmod(0777, path)
			File.open(path, 'w+') do |f|	
				f.write(header_guard)
				f.write("\n")
				f.write(contents)
				puts "=> #{path} : prepended with hdr guard."
			end	
		else
			puts "=> #{path} : already prepended with hdr guard."
		end
		
	      end
	    end
	  end
	end
rescue Exception => ex
	$stderr.puts "Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	exit -1
end

puts "Finished : #{files_changed}/#{files_found} files modified. Press a key"
STDIN.getc

