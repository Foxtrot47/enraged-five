#
# File:: export_static_collision.rb
# Description:: Export static collision from a Collada DAE file.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 26 June 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/export/collada'
require 'pipeline/export/static_collision'
require 'pipeline/gui/exception_dialog'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/util/rage'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--project', '-p', Getopt::REQUIRED, 'project data to convert (e.g. jimmy, gta4_jpn).' ],
	[ '--branch', '-b', Getopt::REQUIRED, 'project branch to convert (e.g. dev, dev_migrate).' ],
	[ '--output', '-o', OS::Getopt::REQUIRED, 'output directory for mesh files.' ],
	[ '--xge', '-x', OS::Getopt::BOOLEAN, 'use XGE (if available).' ],
    [ '--debug', '-d', OS::Getopt::BOOLEAN, 'debug mode toggle.' ],
    [ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING_DESC = { 
    'collada_file' => 'Collada DAE file to export static collision for.'
}

#----------------------------------------------------------------------------
# Entry-Point
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_Options, g_Trailing = OS::Getopt::getopts( OPTIONS )
		
		#--------------------------------------------------------------------
		# Parse Command Line
		#--------------------------------------------------------------------
		if ( g_Options['help'] ) then
			puts OS::Getopt::usage( OPTIONS, TRAILING_DESC )
			exit( 1 )
		end
		g_Debug = g_Options['debug'].nil? ? false : true
		
		# Force log output
		Pipeline::Config::instance()::log_level = 2
		Pipeline::Config::instance()::log_level = 1 if ( g_Debug )
		Pipeline::Config::instance()::logtostdout = g_Debug
		Pipeline::Config::instance()::log_trace = g_Debug
		g_Log = Log.new( g_AppName )
		
		# Get output directory
		if ( g_Options['output'].nil? ) then
			puts OS::Getopt::usage( OPTIONS, TRAILING_DESC )
			exit( 2 )
		end
		g_OutputDir = g_Options['output']
		
		if ( 0 == g_Trailing.size ) then
			g_Log.error( "No static collision files to export.  Aborting." )
			puts OS::Getopt::usage( OPTIONS, TRAILING_DESC )
			exit( 3 )	
		end
		
		#--------------------------------------------------------------------
		# Process Collada Geometry Files
		#--------------------------------------------------------------------
		Pipeline::Export::Collada::init( )
		
		FileUtils::mkdir_p( g_OutputDir ) unless ( File::directory?( g_OutputDir ) )
		g_Log.info( "Exporting #{g_Trailing.size} Collada files to #{g_OutputDir}." )
		g_Trailing.each do |collada_file|
			g_Log.info( "Exporting: #{collada_file}..." )
			
			file = Export::ColladaDocument::new( collada_file )
			coll = Export::StaticCollision::new( file )
			coll.export_all( g_OutputDir )
		end
		
		Pipeline::Export::Collada::shutdown( )
		
	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
		GUI::ExceptionDialog::show_dialog( ex )		
	end
end

# export_static_collision.rb
