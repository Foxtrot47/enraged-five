#
# File:: util/export/export_drawables.rb
# Description:: Export drawables from a series of Collada DAE files.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 29 May 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/export/collada'
require 'pipeline/export/drawable'
require 'pipeline/gui/exception_dialog'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/util/incredibuild_xge'
require 'pipeline/util/rage'
include Pipeline::Export
include Pipeline::Util
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--project', '-p', Getopt::REQUIRED, 'project data to convert (e.g. jimmy, gta4_jpn).' ],
	[ '--branch', '-b', Getopt::REQUIRED, 'project branch to convert (e.g. dev, dev_migrate).' ],
	[ '--output', '-o', OS::Getopt::REQUIRED, 'output directory for mesh files.' ],
	[ '--xge', '-x', OS::Getopt::BOOLEAN, 'use XGE (if available).' ],
    [ '--debug', '-d', OS::Getopt::BOOLEAN, 'debug mode toggle.' ],
    [ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING_DESC = { 
    'collada_files' => 'Collada DAE files to export geometry for.'
}

#----------------------------------------------------------------------------
# Classes
#----------------------------------------------------------------------------
# None

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------
def create_log( name, debug = false )
	Pipeline::Config::instance()::log_level = 2
	Pipeline::Config::instance()::log_level = 1 if ( debug )
	Pipeline::Config::instance()::logtostdout = debug
	Pipeline::Config::instance()::log_trace = debug
	Log.new( name )
end

# Print usage information to stdout and exit process.
def print_usage_and_exit( exit_code = 1, error = '' )
	puts OS::Getopt::usage( OPTIONS, TRAILING_DESC )
	puts error unless ( error.empty? )
	exit( exit_code )
end

#----------------------------------------------------------------------------
# Entry-Point
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	begin
		g_AppName = OS::Path::get_basename( __FILE__ )
		g_ProjectName = ''
		g_Project = nil
		g_Branch = ''
		g_Config = Pipeline::Config.instance( ) 
		
		#--------------------------------------------------------------------
		# Parse Command Line
		#--------------------------------------------------------------------
		g_Options, g_Trailing = OS::Getopt::getopts( OPTIONS )
		if ( g_Options['help'] ) then
			print_usage_and_exit( 1 )
		end
		@debug = g_Options['debug'].nil? ? false : true
		
		# Force log output
		g_Log = create_log( g_AppName, @debug )
		
		# Get project and branch information
		g_ProjectName = ( nil == g_Options['project'] ) ? '' : g_Options['project']
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			print_usage_and_exit( 1, "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable." )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) then
			print_usage_and_exit( 2, "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer." )
		end 
		g_Project.load_config( )
		g_BranchName = ( nil == g_Options['branch'] ) ? g_Project.default_branch : g_Options['branch']
		if ( not g_Project.branches.has_key?( g_BranchName ) ) then
			print_usage_and_exit( 3, "\nError project: #{g_ProjectName} does not have a branch called #{g_BranchName}." )
		end
		# Get output directory
		if ( g_Options['output'].nil? ) then
			print_usage_and_exit( 4, "No output directory defined.  Aborting." )
		end
		g_OutputDir = g_Options['output']
		g_UseXGE = g_Options['xge'] and Pipeline::Config::instance().use_xge
		
		if ( 0 == g_Trailing.size ) then
			g_Log.error( "No drawable files to export.  Aborting." )
			print_usage_and_exit( 5, "No drawable files to export.  Aborting." )
		end

		#--------------------------------------------------------------------
		# Process Collada Geometry Files
		#--------------------------------------------------------------------
		g_RageUtil = RageUtils.new( g_Project, g_BranchName )
		g_Log.info( "Drawable export using XGE: #{g_UseXGE}." )
		Collada::init( )

		FileUtils::mkdir_p( g_OutputDir ) unless ( File::directory?( g_OutputDir ) )
		g_Log.info( "Exporting #{g_Trailing.size} Collada files to #{g_OutputDir}." )
		
		if ( g_UseXGE ) then
			#----------------------------------------------------------------
			# XGE enabled codepath.  Export and pack in parallel.
			#----------------------------------------------------------------
			xge_folder = XGE::get_temp_dir( g_Project, g_BranchName )
			xge_packet_folder = OS::Path::combine( xge_folder, 'export' )
			xge_config = OS::Path::combine( xge_packet_folder, 'export_drawables.xml' )
			xge_log = OS::Path::combine( xge_packet_folder, 'export_drawables.log' )		

			# Construct XGE Environments and Tools
			xge_env = XGE::Environment.new( 'default' )
			xge_env.add_variable( 'BinVar', g_Config.toolsbin )
			# DHM FIX ME: false ==> true
			xge_env.add_tool( XGE::Tool::ruby( 'Export Drawable', File::expand_path( __FILE__ ), "--project=#{g_ProjectName} --branch=#{g_BranchName}", false ) )
			
			# Construct XGE Tasks
			xge_taskgroup = XGE::TaskGroup.new( 'Drawable Export', [], xge_env, xge_env.tools[0] )			
			g_Trailing.each do |collada_file|
				if ( File::directory?( collada_file ) ) then
					next
				elsif ( not File::exists?( collada_file ) ) then
					g_Log.error( "File #{collada_file} does not exist.  Ignoring." )
					next
				end

				filename = OS::Path::get_filename( collada_file )
				filesize = File::size( collada_file )
				task_name = "Drawable Export #{filename} [#{filesize/1024}KB]"

				xge_task = XGE::Task.new( task_name, collada_file )
				xge_taskgroup.tasks << xge_task
			end	
			
			# Ensure we have work to do, otherwise exit.
			if ( 0 == xge_taskgroup.tasks.size ) then
				g_Log.error( "No COLLADA drawables to export.  Aborting." )
				print_usage_and_exit( 6, "No COLLADA drawables to export.  Aborting." )
			end

			# Construct XGE Project	
			xge_project = XGE::Project.new( "#{g_Project.uiname} XGE Drawable Export" )
			xge_project.add_task( xge_taskgroup )
			xge_project.write( xge_config )

			XGE::start( xge_config, xge_log )
		else
			#----------------------------------------------------------------
			# Non-XGE enabled codepath.  Export and pack sequentially.
			#----------------------------------------------------------------
			g_Trailing.each do |collada_file|
				g_Log.info( "Exporting: #{collada_file}..." )
				
				file = ColladaDocument.new( collada_file )
				file.pre_process( )
				drawable = Drawable.new( file )
				drawable.export_all( g_OutputDir )
				drawable.pack( g_RageUtil, g_OutputDir )
				file.close( )
			end
		end
		g_Log.info( "Collada file export complete." )

		Collada::shutdown( )
	
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
		GUI::ExceptionDialog::show_dialog( ex )		
	end
end

# util/export/export_drawables.rb
