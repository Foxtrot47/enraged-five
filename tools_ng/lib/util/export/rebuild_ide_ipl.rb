#
# File:: %RS_TOOLSLIB%/util/export/rebuild_ide_ipl.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Author:: Greg Smith <greg@rockstarnorth.com>
# Date:: 29 April 2010
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
Pipeline::Config::instance()::logtostdout = true
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/os/start'
require 'pipeline/projectutil/data_extract'
require 'pipeline/projectutil/data_convert'
require 'pipeline/util/rage'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--project', '-p', Getopt::OPTIONAL, 'project data to convert (e.g. jimmy, gta4_jpn).' ],
	[ '--branch', '-b', Getopt::OPTIONAL, 'project branch to convert (e.g. dev, dev_migrate).' ],
    [ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING = { 
    'map(s)' => 'Map (maprpf) names to re-export (checked in output content).'
}
PACK_MOUNT = 'pack:/'

$changelist = nil
$convertfilenames = Array.new()

#----------------------------------------------------------------------------
# Classes
#----------------------------------------------------------------------------
# None

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

# Create and return our main script log object.
def create_log( name, debug = false )

	Pipeline::Config::instance()::log_level = 2
	Pipeline::Config::instance()::log_level = 1 if ( debug )
	Pipeline::Config::instance()::log_trace = debug
	Log.new( name )
end

# Print usage information to stdout and exit process.
def print_usage_and_exit( exit_code = 1, error = '' )
	puts OS::Getopt::usage( OPTIONS, TRAILING )
	puts error unless ( error.empty? )
	exit( exit_code )
end

# Open and extract maprpf and re-export our IDE/IPL files; then repack.
def export_ide_ipl( p, r, maprpf )
	
	c = Pipeline::Config::instance()
	map_base = OS::Path::get_basename( maprpf.filename )
	dest_path = OS::Path::combine( c.temp, map_base )
	exe = OS::Path::combine( c.toolsbin, 'MapExportIDEIPL.exe' )
	stream_dir = dest_path
	scenexml_filename = OS::Path::combine( dest_path, "#{map_base}.xml" )
		
	ipl_filename = nil
	ide_filename = nil
		
	maprpf.children.each { |child|
	
		if child.class == Content::Target and child.extension.downcase == "ipl" then
	
			p.in_env { |e| 
	
				ipl_filename = e.subst(child.path + child.name + "." + child.extension)
			}
		elsif child.class == Content::Target and child.extension.downcase == "ide" then
	
			p.in_env { |e| 
	
				ide_filename = e.subst(child.path + child.name + "." + child.extension)
			}
		end
	}
	
	if ipl_filename != nil then
		
		$changelist = p.scm.create_changelist( "regenerate main ipl files" ) if $changelist == nil
		p.scm.run_edit_or_add( '-c', $changelist.to_s, ipl_filename )
		
		if File.writable?(ipl_filename) then

			$convertfilenames << ipl_filename
			files = ProjectUtil::data_extract_rpf( r, maprpf.filename, dest_path, true )
				
			command = "#{exe} --build #{p.independent} --ipl6 #{ipl_filename} --iplstream #{stream_dir} #{scenexml_filename}"
			puts(command)
			system( command )		
		end			
	end
	
	#x:/gta5/src/dev/rage/framework/tools/bin/MapExportIDEIPL.exe --build x:/gta5/build/dev/independent/ --ide x:/gta5/build/dev/independent//Levels/gta5/_cityw/venice_01//vb_21.ide --ipl6 x:/gta5/build/dev/independent//Levels/gta5/_cityw/venice_01//vb_21.ipl --iplstream x:/streamGTA5/maps/vb_21 --log x:/streamGTA5/maps/vb_21/vb_21_export.log  x:/streamGTA5/maps/vb_21/vb_21.xml
end

#----------------------------------------------------------------------------
# Entry-Point
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

	begin
	
		g_AppName = OS::Path::get_basename( __FILE__ )
		g_ProjectName = nil
		g_Project = nil
		g_Branch = ''
		g_Config = Pipeline::Config.instance( ) 
		
		#--------------------------------------------------------------------
		# Parse Command Line
		#--------------------------------------------------------------------
		g_Options, g_Trailing = OS::Getopt::getopts( OPTIONS )
		
		if ( g_Options['help'] ) then
		
			print_usage_and_exit( 1 )
		end
		
		# Force log output
		$log = create_log( g_AppName, @debug )
		
#		if ( 0 == g_Trailing.size ) then
#			$log.error( "No maprpf names specified.  Aborting." )
#			print_usage_and_exit( 5, "No maprpf names specified.  Aborting." )
#		end	
		
		@debug = g_Options['debug'].nil? ? false : true
		
		g_Project = g_Config.project
		
		# Get project and branch information
		
		g_ProjectName = g_Options['project'] if nil != g_Options['project']
		
		if g_ProjectName != nil then
		
			project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
			
			if ( not project_exists ) then
			
				print_usage_and_exit( 1, "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable." )
			end
			
			g_Project = g_Config.projects[ g_ProjectName ]
		end
		
		if ( not g_Project.enabled ) then
		
			print_usage_and_exit( 2, "\nError project: #{g_Project.name} is not enabled on this machine.  Re-run installer." )
		end 
		
		g_Project.load_config( )	
		g_BranchName = ( nil == g_Options['branch'] ) ? g_Project.default_branch : g_Options['branch']
		
		if ( not g_Project.branches.has_key?( g_BranchName ) ) then
		
			print_usage_and_exit( 3, "\nError project: #{g_Project.name} does not have a branch called #{g_BranchName}." )
		end
		
		g_Project.load_content( g_BranchName, Project::ContentType::MAPS | Project::ContentType::OUTPUT )
		r = RageUtils.new( g_Project )
		
		if ( 0 == g_Trailing.size ) then		
		
			#do all maps
			
			map_group = g_Project.content.find_first( "output", "group" )
			map_group = map_group.find_first( "gta5", "level" )
			
			map_group.walk { |item|
			
				if item.xml_type == "maprpf" then
				
					export_ide_ipl( g_Project, r, item )
				end
			}
			
			
		else
			# Process the maprpfs.
			g_Trailing.each do |maprpf_name|
			
				$log.info( "Processing #{maprpf_name}..." )
				maprpf = g_Project.content.find_first( maprpf_name, 'maprpf' )
				
				if ( maprpf.nil? ) then
					$log.error( "Map #{maprpf_name} not found.  Continuing..." )
					next
				end
				export_ide_ipl( g_Project, r, maprpf )
			end
		end
		
		ProjectUtil::data_convert_file( $convertfilenames, true )
		
	rescue SystemExit => ex
		exit( ex.status )
		
	rescue Exception => ex
	
		$log.exception( ex, "Unhandled exception" )
	end
end

# %RS_TOOLSLIB%/util/export/rebuild_ide_ipl.rb
