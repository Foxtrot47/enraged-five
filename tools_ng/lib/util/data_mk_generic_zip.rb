#
# File:: %RS_TOOLSLIB%/util/data_mk_generic_zip.rb
# Description:: Script to create a generic PKware-ZIP file from a set of
#               files or directory contents.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 23 June 2011
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_zip'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ '--project', '-p', OS::Getopt::REQUIRED, 'OBSOLETE' ],
	[ '--output', '-o', OS::Getopt::REQUIRED, 'output ZIP filename.' ],
	[ '--uncompressed', '-u', OS::Getopt::BOOLEAN, 'create uncompressed ZIP.' ],
	[ '--flatten', '-f', OS::Getopt::BOOLEAN, 'flatten directory structure.' ],
	[ '--recursive', '-r', OS::Getopt::BOOLEAN, 'recursive operation, expects directory.' ],
	[ '--filter', '-f', OS::Getopt::REQUIRED, 'file filter for path arguments (multiple separated by commas).' ],
	[ '--silent', '-s', OS::Getopt::BOOLEAN, 'silent mode, reduces logging.' ],
	[ '--debug', '-d', OS::Getopt::BOOLEAN, 'debug mode toggle.' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
TRAILING_DESC = { 
	'files' => 'list of files/directories to add to zip.'
}
DEFAULT_FILTER = '*.*'

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then
    begin
        # Force output to stdout for console scripts.
        Pipeline::Config::instance()::logtostdout = true
        
        g_AppName = OS::Path::get_basename( __FILE__ )
        g_Config = Pipeline::Config.instance( ) 
    
        #---------------------------------------------------------------------
        # Parse Command Line
        #---------------------------------------------------------------------
        opts, trailing = OS::Getopt.getopts( OPTIONS )
        if ( opts['help'] ) then
            puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
            exit( 1 )
        end
        g_Debug = opts['debug']
        g_Silent = opts['silent']
        Pipeline::Config::instance()::log_level = 5 if ( g_Silent )
        Pipeline::Config::instance()::log_level = 2 unless ( g_Silent )
        Pipeline::Config::instance()::log_level = 1 if ( g_Debug )
        Pipeline::Config::instance()::log_trace = g_Debug
        g_Log = Log.new( g_AppName )
        g_Output = ( nil == opts['output'] ) ? nil : ::File::expand_path( opts['output'] )
        if ( g_Output.nil? ) then
            g_Log.error( 'No output ZIP file specified.' )
            puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
            exit( 2 )
        end
        g_Filters = ( nil == opts['filter'] ) ? [DEFAULT_FILTER] : opts['filter'].split(',')
        g_Flatten = opts['flatten']
        g_Recursive = opts['recursive']
        g_Compress = ( not opts['uncompressed'] )
        g_Paths = []
        trailing.each do |trail|
            g_Paths << OS::Path.normalise( trail )
        end
        if ( 0 == g_Paths.size ) then
            puts 'No input files or paths specified.'
            puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
            exit( 3 )
        end
        
        #---------------------------------------------------------------------
        # Check output
        #---------------------------------------------------------------------
        if ( ::File::exists?( g_Output ) and ( not ::File::writable?( g_Output ) ) ) then
          g_Log.error( "Output file: #{g_Output} is not writable.  Aborting." )
          exit( 4 )
        end
    
        #---------------------------------------------------------------------
        # Process input
        #---------------------------------------------------------------------
        if ( ::File::exists?( g_Output ) ) then
          time_last_moded = File::mtime( g_Output ) 
        else  
          time_last_moded = Time.new
        end
        
		# Construct Array of files to pack into zip.
        g_FileList = []
		g_Paths.each do |path|
			if ( File::file?( path ) ) then	
            
                # Ensure the file path adheres to any filters.
                g_Filters.each do |filter|
                    next unless ( File::fnmatch( filter, path ) )

				    entry = {}
				    entry[:src] = path
				    entry[:dst] = OS::Path::get_filename( path )
				    g_FileList << entry
                end            		

			elsif ( File::directory?( path ) ) then
                files = []
                g_Filters.each do |filter|
				    files += OS::FindEx::find_files( OS::Path::combine( path, filter ) ) unless ( g_Recursive )
				    files += OS::FindEx::find_files_recurse( OS::Path::combine( path, filter ) ) if ( g_Recursive )
	            end		
   
				files.each do |file|
					if ( g_Flatten ) then
						entry = {}
						entry[:src] = file
						entry[:dst] = OS::Path::get_filename( file )
						g_FileList << entry
					else
						entry = {}
						entry[:src] = file
						entry[:dst] = file.sub( path, '' )
						# Sometimes removing path would leave a slash before filename
						if ( entry[:dst].start_with?("/") ) then
							entry[:dst].sub!( '/', '' )
						end
						g_FileList << entry
					end
				end
			end
		end
		
        FileUtils::rm( g_Output ) if ( File::exists?( g_Output ) )
		ProjectUtil::data_zip_create( g_Output, g_FileList, g_Compress )        

		time_moded_after_save = File.mtime( g_Output )
		if ( time_moded_after_save == time_last_moded ) then
			g_Log.error("\n\n#------------- ERROR: Couldn't update the ZIP file ------------#")
		else
			g_Log.info("\n\nSuccessfully updated the ZIP file")
			puts time_moded_after_save
		end 
		puts "\n"
     
	rescue SystemExit => ex
		exit( ex.status )
	 
	rescue Exception => ex
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
        puts ex.backtrace.join("\n\t")
        exit( 1 )
    end 
end

# %RS_TOOLSLIB%/util/data_mk_generic_zip.rb
