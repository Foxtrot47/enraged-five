#
# File:: map_inst_count.rb
# Description:: Game map world instance counter script.
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 15 April 2008
#

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

require 'pipeline/config/projects'
require 'pipeline/game/gamedataloader'
require 'pipeline/game/mapidefile'
require 'pipeline/game/vehicleidefile'
require 'pipeline/game/pedidefile'
require 'pipeline/game/defaultidefile'
require 'pipeline/game/iplfile'
require 'pipeline/os/path'
require 'pipeline/util/debug'
require 'pipeline/util/float'
require 'pipeline/util/rage'
require 'pipeline/util/string'
include Pipeline

require 'find'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

IMAGE_MOUNT 	= 'image:/'

#-----------------------------------------------------------------------------
# Globals
#-----------------------------------------------------------------------------

$script_definition_count = {
	:object => 0,
	:time_object => 0,
	:anim => 0,
	:time_anim => 0,
	:milo => 0
}
$script_map_instance_count = {}		# Key: map name, Value: instance count

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

#
# Process maps IDE/IPL data to count object assets.
#
def process_maps( project, r )

	gamedata = Game::GameDataLoader.new( project )
	gamedata.load_ides( true )
	gamedata.load_ipls( true )
	
	# Loop through all our maps, all our definitions...
	count = 0
	total = gamedata.project_ides.size
	gamedata.project_ides.each do |idefilename, idefile|
	 	
		count += 1
		progress = 100.0 * ( count / total.to_f )
		
		puts "Processing IDE: #{idefile.filename} [#{count} of #{total} - #{progress.to_s(2)}%]"

		$script_definition_count[:object] += idefile.object_definitions.size
		$script_definition_count[:time_object] += idefile.time_object_definitions.size
		$script_definition_count[:anim] += idefile.anim_definitions.size
		$script_definition_count[:time_anim] += idefile.time_anim_definitions.size
		$script_definition_count[:milo] += idefile.milo_object_definitions.size
	end
		
	# Loop through all our maps, all our instances...
	count = 0
	total = gamedata.project_ipls.size
	gamedata.project_ipls.each do |iplfilename, iplfile|
	 
		count += 1
		progress = 100.0 * ( count / total.to_f )
		
		puts "Processing IPL: #{iplfile.filename} [#{count} of #{total} - #{progress.to_s(2)}%]"

		# Instances
		map_name = OS::Path.get_filename( iplfilename )
		$script_map_instance_count[map_name] = 0 \
		  unless $script_map_instance_count.has_key?( map_name )
		
		$script_map_instance_count[map_name] += iplfile.instances.size
	end
end

#-----------------------------------------------------------------------------
# Entry Point
#-----------------------------------------------------------------------------

begin
	project = Pipeline::Config.instance.projects['gta4']
	project.load_config()
	r = RageUtils.instance

	process_maps( project, r )
rescue Exception => ex

	print_exception( ex )
ensure

	# Total the count
	def_count = 0
	$script_definition_count.each do |map, c| def_count += c; end
	inst_count = 0
	$script_map_instance_count.each do |map, c| inst_count += c; end
	
	puts "-------------------------------------------------------------------"
	puts "Total number of object definitions: #{def_count}"
	puts "Total number of object instances: #{inst_count}"
	puts "-------------------------------------------------------------------"
	puts "Definition category counts:"
	$script_definition_count.each do |k, c|
		puts "\t#{k} => #{c}"
    end
	
	puts "Map section object instances:"
	$script_map_instance_count.each do |map, c|
		puts "\t#{map} => #{c}"
    end
end

# End of map_inst_count.rb
