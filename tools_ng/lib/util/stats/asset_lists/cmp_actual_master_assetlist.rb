#
# cmp_actual_master_assetlist.rb
# Compare Actual and Master Asset Lists
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 3 March 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/os/path'
include Pipeline
require 'util/asset_lists/asset_lists_common'

# Rubygems
require 'progressbar'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

REGEXP_ANIM_CODE = /(.+);(.+)/
REGEXP_MODEL_CODE = /(.+)=(.+)/

#-----------------------------------------------------------------------------
# Globals
#-----------------------------------------------------------------------------

# Global hashes map asset name to asset source (e.g. anim name to IAD name for
# the anim lists).
# Global arrays just contain the asset name - as they can be used in multiple
# sources and don't want to confuse the issue.

$master_models_list = Hash.new 	# IDR, IFT
$master_iad_list = Hash.new
$master_anim_list = Hash.new
$master_itd_list = Hash.new
$master_dds_list = Hash.new
$master_sco_list = Hash.new
$master_cut_list = Hash.new

$actual_models_list = Hash.new 	# IDR, IFT
$actual_iad_list = Hash.new
$actual_anim_list = Hash.new
$actual_itd_list = Hash.new
$actual_dds_list = Hash.new
$actual_sco_list = Hash.new
$actual_cut_list = Hash.new

$unused_models_list = Hash.new	# IDR, IFT
$unused_iad_list = Hash.new
$unused_anim_list = Hash.new
$unused_itd_list = Hash.new
$unused_itd_list_filtered = Hash.new
$unused_dds_list = Hash.new
$unused_sco_list = Hash.new
$unused_cut_list = Hash.new

$used_models_list = Hash.new	# IDR, IFT 
$used_iad_list = Hash.new
$used_anim_list = Hash.new
$used_itd_list = Hash.new
$used_dds_list = Hash.new
$used_sco_list = Hash.new
$used_cut_list = Hash.new

$missing_models_list = Hash.new	# IDR, IFT 
$missing_iad_list = Hash.new
$missing_anim_list = Hash.new
$missing_itd_list = Hash.new
$missing_dds_list = Hash.new
$missing_sco_list = Hash.new
$missing_cut_list = Hash.new

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

#
# Read list of IAD and ANIM files from input file provided by Phil H.  
# These lists are output from the game code using RAG.
#
def read_iad_anims_from_code( filename )

	File.open( filename ) do |fp|	
		begin
			fp.each do |line|			
				if ( line =~ REGEXP_ANIM_CODE )					
					iad = $1.downcase
					anim = $2.downcase
					
					add_asset_ref( $actual_iad_list, iad, 'code' )
					add_asset_ref( $actual_anim_list, anim, 'code' )
				end
			end
		rescue Exception => ex		
			puts "Exception: #{ex.message}"
			puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
		ensure		
			fp.close()
		end
	end
end

#
# Read list of models from input file provided by Graeme W.  This list 
# provides all the models referenced by script SC files.
#
def read_models_from_scripts( filename )

	File.open( filename ) do |fp|	
		begin		
			fp.each do |line|			
				if ( line =~ REGEXP_MODEL_CODE )				
					key = $1.downcase
					add_asset_ref( $actual_models_list, key, 'script' )
				end
			end
		rescue Exception => ex		
			puts "Exception: #{ex.message}"
			puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"		
		ensure			
			fp.close()
		end
	end
end


#
# Compare an actual and master list populating the used and unused lists
# as appropriate.
#
def cmp_actual_master_list( name, actual_hash, master_hash, used_list, unused_list, missing_master_list )

	size = master_hash.size + actual_hash.size
	pbar = ProgressBar.new( name, size )

	actual_list = actual_hash.keys
	actual_list.sort!()
	actual_list.each_with_index do |item, index|
		actual_list[index] = item.downcase
	end
	
	master_list = master_hash.keys
	master_list.sort!()
	master_list.each_with_index do |item, index|
		master_list[index] = item.downcase
	end
	
	# Loop through master list adding to used and unused
	master_list.each do |item|
	
		# Check item is in our actual list, if not its unused.
		if ( !actual_list.include?( item ) and !unused_list.include?( item ) )
			add_asset_refs( unused_list, item, master_hash[item] )
		else
			# Otherwise it used
			add_asset_refs( used_list, item, master_hash[item] )
		end
		pbar.inc
	end
	
	# Loop through actual list adding to missing if not in master list.
	actual_list.each do |item|
	
		it = item.downcase
		if ( !master_list.include?( it ) and !missing_master_list.include?( it ) )
			add_asset_refs( missing_master_list, it, actual_hash[it] )
		end
		
		pbar.inc
	end
	puts ""
end

#
# Filter a list by removing entries with substrings
#
def filter_list( list, outlist, filter )
	
	outline = Hash.new
	list.each do |key, item|
	
		outlist[key] = Array.new
		item.each do |it|
			
			endswith_filter = false
			filter.each do |filter_string|
				endswith_filter = true if it.ends_with( filter_string )
			end
			outlist[key] << it unless endswith_filter
		end
	end
end

#-----------------------------------------------------------------------------
# Entry Point
#-----------------------------------------------------------------------------

begin

	# Data Load Lists : Master
	puts "Loading Master Lists..."
	read_list_from_file( $master_iad_list, OS::Path.combine( MASTER_PATH, 'iad_master_list.txt' ) )
	read_list_from_file( $master_anim_list, OS::Path.combine( MASTER_PATH, 'anim_master_list.txt' ) )
	read_list_from_file( $master_itd_list, OS::Path.combine( MASTER_PATH, 'itd_master_list.txt' ) )
	read_list_from_file( $master_dds_list, OS::Path.combine( MASTER_PATH, 'dds_master_list.txt' ) )
	read_list_from_file( $master_sco_list, OS::Path.combine( MASTER_PATH, 'sco_master_list.txt' ) )
	read_list_from_file( $master_models_list, OS::Path.combine( MASTER_PATH, 'idr_master_list.txt' ) )
	read_list_from_file( $master_models_list, OS::Path.combine( MASTER_PATH, 'ift_master_list.txt' ) )
	read_list_from_file( $master_cut_list, OS::Path.combine( MASTER_PATH, 'cut_master_list.txt' ) )

	# Data Load Lists : Actual
	puts "Loading Actual Lists..."
	read_list_from_file( $actual_iad_list, OS::Path.combine( ACTUAL_PATH, 'iad_actual_list.txt' ) )
	read_list_from_file( $actual_anim_list, OS::Path.combine( ACTUAL_PATH, 'anim_actual_list.txt' ) )
	read_list_from_file( $actual_itd_list, OS::Path.combine( ACTUAL_PATH, 'itd_actual_list.txt' ) )
	read_list_from_file( $actual_dds_list, OS::Path.combine( ACTUAL_PATH, 'dds_actual_list.txt' ) )
	read_list_from_file( $actual_sco_list, OS::Path.combine( ACTUAL_PATH, 'sco_actual_list.txt' ) )
	read_list_from_file( $actual_models_list, OS::Path.combine( ACTUAL_PATH, 'models_actual_list.txt' ) )
	read_list_from_file( $actual_cut_list, OS::Path.combine( ACTUAL_PATH, 'cut_actual_list.txt' ) )
	read_models_from_scripts( OS::Path.combine( ACTUAL_PATH, 'AllModelsUsedByScripts.txt' ) )
	read_iad_anims_from_code( OS::Path.combine( ACTUAL_PATH, 'anim_actual_code_list.txt' ) )
	
	# Manual Load Lists : Actual
	puts "Loading Actual (manual) Lists..."
	read_list_from_file( $actual_itd_list, OS::Path.combine( MANUAL_PATH, 'itd_manual_list.txt' ) )
	read_list_from_file( $actual_sco_list, OS::Path.combine( MANUAL_PATH, 'sco_manual_list.txt' ) )
    read_list_from_file( $actual_anim_list, OS::Path.combine( MANUAL_PATH, 'anim_manual_list.txt' ) )
	read_list_from_file( $actual_dds_list, OS::Path.combine( MANUAL_PATH, 'dds_manual_list.txt' ) )
	read_list_from_file( $actual_dds_list, OS::Path.combine( MANUAL_PATH, 'dds_manual_list_autogen.txt' ) )
	read_list_from_file( $actual_dds_list, OS::Path.combine( MANUAL_PATH, 'dds_manual_list_vehtex.txt' ) )
	read_list_from_file( $actual_models_list, OS::Path.combine( MANUAL_PATH, 'models_manual_list.txt' ) )
	read_list_from_file( $actual_models_list, OS::Path.combine( MANUAL_PATH, 'models_manual_list_peds.txt' ) )
	read_list_from_file( $actual_cut_list, OS::Path.combine( MANUAL_PATH, 'cut_manual_list.txt' ) )

	# Let's Compare Lists
	puts "Comparing Lists..."
	cmp_actual_master_list( 'iad', $actual_iad_list, $master_iad_list, $used_iad_list, $unused_iad_list, $missing_iad_list )
	cmp_actual_master_list( 'anim', $actual_anim_list, $master_anim_list, $used_anim_list, $unused_anim_list, $missing_anim_list )
	cmp_actual_master_list( 'itd', $actual_itd_list, $master_itd_list, $used_itd_list, $unused_itd_list, $missing_itd_list )
	cmp_actual_master_list( 'dds', $actual_dds_list, $master_dds_list, $used_dds_list, $unused_dds_list, $missing_dds_list )
	cmp_actual_master_list( 'sc', $actual_sco_list, $master_sco_list, $used_sco_list, $unused_sco_list, $missing_sco_list )
	cmp_actual_master_list( 'models', $actual_models_list, $master_models_list, $used_models_list, $unused_models_list, $missing_models_list )
	cmp_actual_master_list( 'cut', $actual_cut_list, $master_cut_list, $used_cut_list, $unused_cut_list, $missing_cut_list )

	filter_list( $unused_itd_list, $unused_itd_list_filtered, ['empty'] )
	
rescue Exception => ex

	puts "Exception: #{ex.message}"
	puts "  Stacktrace: #{ex.backtrace.join('\n\r')}"

ensure

	# DEBUG
	puts "--------------------------------------------------------------------------------"
	puts "Summary : Master Lists"
	puts "--------------------------------------------------------------------------------"
	puts "\n"
	puts "IAD  : #{$master_iad_list.size}"
	puts "ANIM : #{$master_anim_list.size}"
	puts "ITD  : #{$master_itd_list.size}"
	puts "DDS  : #{$master_dds_list.size}"
	puts "SCO  : #{$master_sco_list.size}"
	puts "Model: #{$master_models_list.size} (IDR, IFT, IDD)"
	puts "CUT  : #{$master_cut_list.size}"
	puts "\n\n"
	puts "--------------------------------------------------------------------------------"
	puts "Summary : Actual Lists"
	puts "--------------------------------------------------------------------------------"
	puts "\n"
	puts "IAD  : #{$actual_iad_list.size}"
	puts "ANIM : #{$actual_anim_list.size}"
	puts "ITD  : #{$actual_itd_list.size}"
	puts "DDS  : #{$actual_dds_list.size}"
	puts "SCO  : #{$actual_sco_list.size}"
	puts "Model: #{$actual_models_list.size} (IDR, IFT, IDD)"
	puts "CUT  : #{$actual_cut_list.size}"
	# END DEBUG

	# Write out unused lists
	write_sorted_justified_list( $unused_iad_list, 'asset_list_data/iad_unused_list.txt' )
	write_sorted_justified_list( $unused_anim_list, 'asset_list_data/anim_unused_list.txt' )
	write_sorted_justified_list( $unused_itd_list, 'asset_list_data/itd_unused_list.txt' )
	write_sorted_justified_list( $unused_itd_list_filtered, 'asset_list_data/itd_unused_list_filtered.txt' )
	write_sorted_justified_list( $unused_dds_list, 'asset_list_data/dds_unused_list.txt' )
	write_sorted_justified_list( $unused_sco_list, 'asset_list_data/sco_unused_list.txt' )
	write_sorted_justified_list( $unused_models_list, 'asset_list_data/models_unused_list.txt' )
	write_sorted_justified_list( $unused_cut_list, 'asset_list_data/cut_unused_list.txt' )
	
	# Write out missing lists
	write_sorted_justified_list( $missing_iad_list, 'asset_list_data/iad_missing_list.txt' )
	write_sorted_justified_list( $missing_anim_list, 'asset_list_data/anim_missing_list.txt' )
	write_sorted_justified_list( $missing_itd_list, 'asset_list_data/itd_missing_list.txt' )
	write_sorted_justified_list( $missing_dds_list, 'asset_list_data/dds_missing_list.txt' )
	write_sorted_justified_list( $missing_sco_list, 'asset_list_data/sco_missing_list.txt' )
	write_sorted_justified_list( $missing_models_list, 'asset_list_data/models_missing_list.txt' )
	write_sorted_justified_list( $missing_cut_list, 'asset_list_data/cut_missing_list.txt' )
end

# End of cmp_actual_master_assetlist.rb
