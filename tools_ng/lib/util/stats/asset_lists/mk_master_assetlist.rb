#
# mk_master_assetlist.rb
# Make Master Asset Lists
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 28 February 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/util/debug'
require 'pipeline/util/rage'
require 'pipeline/util/string'
include Pipeline
require 'util/asset_lists/asset_lists_common'
require 'find'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

REGEXP_ANIM = /([A-Za-z_0-9 ]+)_uv_[0-9]+/

#-----------------------------------------------------------------------------
# Globals
#-----------------------------------------------------------------------------

# Output Lists
$master_iad_list = Hash.new     # Key: asset name, Value: array of references
$master_anim_list = Hash.new    # Key: asset name, Value: array of references
$master_itd_list = Hash.new     # Key: asset name, Value: array of references 
$master_dds_list = Hash.new     # Key: asset name, Value: array of references
$master_idr_list = Hash.new     # Key: asset name, Value: array of references
$master_idd_list = Hash.new     # Key: asset name, Value: array of references
$master_ift_list = Hash.new     # Key: asset name, Value: array of references
$master_sco_list = Hash.new     # Key: asset name, Value: array of references
$master_cut_list = Hash.new     # Key: asset name, Value: array of references

# Additional Lists
$empty_itd_list = Array.new     # Array of empty (no DDS) texture dictionaries

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

#
# Recursively process directory for image files.
#
def process_directory( path, r )

	Find.find( path ) do |subpath|

		if FileTest.file?( subpath ) 

			Find.prune if subpath.ends_with( 'radar.img' )
			Find.prune if subpath.ends_with( 'playerped.rpf' )
		
			process_asset( subpath, OS::Path.get_directory(subpath), r )
		end
	end
end

#
# Process other asset
#
def process_asset( path, container, r )

	ext = OS::Path.get_extension( path )
	case ext
	
	# IMG
	when 'img'
		puts "Processing IMG: #{path}"
		r.image.mount( path, IMAGE_MOUNT )
		files = r.find_files( "#{IMAGE_MOUNT}*.*" )
		files.each do |file|
		
			process_asset( "#{IMAGE_MOUNT}#{file}", path, r )
		end
		r.image.unmount( path )
	
	# RPF
	when 'rpf'
		puts "Processing RPF: #{path}"
		process_rpf( path, container, r )
	
	# SCO
	when 'sco'
		puts "Processing SCO: #{path}"
		asset = OS::Path.get_basename(path)
		add_asset_ref( $master_sco_list, asset, container )
	
	# Drawable
	when 'idr'
		puts "Processing IDR: #{path}"
		asset = OS::Path.get_basename(path)
		add_asset_ref( $master_idr_list, asset, container )
		process_rpf( path, container, r )
	
	# Fragment
	when 'ift'
		puts "Processing IFT: #{path}"
		asset = OS::Path.get_basename(path)
		add_asset_ref( $master_ift_list, asset, container )
		process_rpf( path, container, r )
	
	# Drawable Dictionary
	when 'idd'
		puts "Processing IDD: #{path}"
		asset = OS::Path.get_basename(path)
		add_asset_ref( $master_idd_list, asset, container )
		process_idd( path, container, r )
		
	# Texture
	when 'dds'
		puts "Processing DDS: #{path}"
		asset = OS::Path.get_basename(path)
		add_asset_ref( $master_dds_list, asset, container )
	
	# Texture Dictionary
	when 'itd'
		puts "Processing ITD: #{path}"
		asset = OS::Path.get_basename(path)
		has_textures = process_itd( path, container, r )
		
		add_asset_ref( $master_itd_list, asset, container ) if has_textures
		add_asset_ref( $master_itd_list, asset, "#{container}-EMPTY" ) unless has_textures
		
	# Animation
	when 'anim'
		puts "Processing ANIM: #{path}"
		anim_basename = OS::Path.get_basename(path)
		
		# Mental Map ANIM processing to not add UV anims to the master list,
		# but ensure their core name is included i.e. excluding the _uv_[0-9]+
		# suffix.
		if ( anim_basename =~ REGEXP_ANIM ) then
			anim = $1.downcase
			add_asset_ref( $master_anim_list, anim, container )
		else
			anim = anim_basename
			add_asset_ref( $master_anim_list, anim, container )
		end		
	
	# Animation Dictionary
	when 'iad'
		puts "Processing IAD: #{path}"
		asset = OS::Path.get_basename(path)
		add_asset_ref( $master_iad_list, asset, container )

		process_rpf( path, container, r )
	
	# Cutscene File
	when 'cut'
	   puts "Processing CUT: #{path}"
	   asset = OS::Path.get_basename(path)
	   add_asset_ref( $master_cut_list, asset, container )
	
	end
end

#
# Mount RPF, and recursively invoke process_asset on its contents.  This 
# functionality is generic so its been extracted out of the process_asset
# function to prevent it being repeated.
#
def process_rpf( path, container, r )
	
	r.pack.mount( path, PACK_MOUNT )
	files = r.find_files( "#{PACK_MOUNT}*.*" )
	files.each do |file|
		
		process_asset( "#{PACK_MOUNT}#{file}", "#{container}-#{path}", r )
	end
	r.pack.unmount( path )
end

#
# Identical to process_rpf above except we are maintaining a list of 
# ITDs that are 'empty' of textures (DDS files)
# 
def process_itd( path, container, r )

	# Ignore componentpeds.img texture dictionaries
	return if container =~ /componentpeds/
	# Ignore pedprops.img texture dictionaries	
	return if container =~ /pedprops/

	r.pack.mount( path, PACK_MOUNT )
	files = r.find_files( "#{PACK_MOUNT}*.*" )
	has_textures = false
	files.each do |file|
		
		has_textures = ( file.ends_with( ".dds" ) or has_textures )
		process_asset( "#{PACK_MOUNT}#{file}", "#{container}-#{path}", r )
	end
	r.pack.unmount( path )
	
	$empty_itd_list << "#{container}-#{path}" unless has_textures
	has_textures
end

#
# Process a Drawable Dictionary, recurse through its directories and ensure
# each has a mesh: that forms the drawable name.
#
def process_idd( path, container, r )

	r.pack.mount( path, PACK_MOUNT )
	dirs = r.find_dirs( "#{PACK_MOUNT}" )
	dirs.each do |dir|
	
        add_asset_ref( $master_idr_list, dir, path )
	end
	
	# Process any files, just in case
	files = r.find_files( "#{PACK_MOUNT}*.*" )
	files.each do |file|
	
		process_asset( "#{PACK_MOUNT}#{file}", "#{container}-#{path}", r )
	end
	
	r.pack.unmount( path )
end

#-----------------------------------------------------------------------------
# Entry Point
#-----------------------------------------------------------------------------

begin
    # Ensure MASTER_PATH exists
    Dir.mkdir( ROOT_PATH ) unless File.directory?( ROOT_PATH )
    Dir.mkdir( MASTER_PATH ) unless File.directory?( MASTER_PATH )
    
	project = Pipeline::Config.instance.projects['gta4']
	project.load_config()
	r = RageUtils.instance
	
	# Process root directory
	process_directory( PROJECT_ROOT, r )
	# Process common script images
	script_dir = OS::Path.combine( COMMON_ROOT, 'data', 'cdimages' )
	process_asset( OS::Path.combine( script_dir, 'script_rel.img' ), script_dir, r )
	process_asset( OS::Path.combine( script_dir, 'script_network_rel.img' ), script_dir, r )
	process_asset( OS::Path.combine( script_dir, 'carrec.img' ), script_dir, r )

rescue Exception => ex

	puts "Unhandled exception: #{ex.message}"
	puts "Stacktrace:\n\r\t#{ex.backtrace.join("\n\r\t")}"
ensure

	puts "Number of IAD:  #{$master_iad_list.size}"
	puts "Number of ANIM: #{$master_anim_list.size}"
	puts "Number of ITD:  #{$master_itd_list.size}"
	puts "Number of DDS:  #{$master_dds_list.size}"
	puts "Number of IDR:  #{$master_idr_list.size}"
	puts "Number of IDD:  #{$master_idd_list.size}"
	puts "Number of IFT:  #{$master_ift_list.size}"
	puts "Number of SCO:  #{$master_sco_list.size}"
	puts "Number of CUT:  #{$master_cut_list.size}"
	puts "\nNumber of empty ITD: #{$empty_itd_list.size}"
	
	write_list( $master_iad_list, OS::Path.combine( MASTER_PATH, 'iad_master_list.txt' ) )
	write_list( $master_anim_list, OS::Path.combine( MASTER_PATH, 'anim_master_list.txt' ) )
	write_list( $master_itd_list, OS::Path.combine( MASTER_PATH, 'itd_master_list.txt' ) )
	write_list( $master_dds_list, OS::Path.combine( MASTER_PATH, 'dds_master_list.txt' ) )
	write_list( $master_idr_list, OS::Path.combine( MASTER_PATH, 'idr_master_list.txt' ) )
	write_list( $master_idd_list, OS::Path.combine( MASTER_PATH, 'idd_master_list.txt' ) )
	write_list( $master_ift_list, OS::Path.combine( MASTER_PATH, 'ift_master_list.txt' ) )
	write_list( $master_sco_list, OS::Path.combine( MASTER_PATH, 'sco_master_list.txt' ) )
	write_list( $master_cut_list, OS::Path.combine( MASTER_PATH, 'cut_master_list.txt' ) )
	write_array_list( $empty_itd_list, OS::Path.combine( ROOT_PATH, 'itd_empty_list.txt' ) )
end

# End of mk_master_assetlist.rb
