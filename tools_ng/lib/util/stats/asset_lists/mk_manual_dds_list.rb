#
# mk_manual_dds_list.rb
# Make manual DDS list from list of manual ITD files (e.g. script ITD files)
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 8 March 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/util/rage'
require 'pipeline/util/string'
include Pipeline
require 'util/asset_lists/asset_lists_common'
require 'find'

#-----------------------------------------------------------------------------
# Globals
#-----------------------------------------------------------------------------

$manual_dds_list = Hash.new
$manual_itd_list = Hash.new
$num_itds = 0

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

def process_itd_list( r )
	packdone = Array.new
	
	Find.find( INDEPENDENT_ROOT ) do |path|
		
		if FileTest.file?( path )
			# Only process ITD files
			Find.prune unless path.ends_with( '.itd' )
				
			$manual_itd_list.each do |asset, references|
				next if asset != OS::Path.get_basename( asset )
				next if packdone.include?( path )
				
				r.pack.mount( path, PACK_MOUNT )
				files = r.find_files( "#{PACK_MOUNT}*.dds" )
				files.each do |file|
					add_asset_ref( $manual_dds_list, OS::Path.get_basename( file ), 'itd_manual_list' )					
				end
				r.pack.unmount( path )
				
				packdone << path
			end
		end
	end
	
	$num_itds = packdone.size
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------

begin
	project = Pipeline::Config.instance.projects['gta4']
	project.load_config()
	r = RageUtils.instance
	
	read_list_from_file( $manual_itd_list, OS::Path.combine( MANUAL_PATH, 'itd_manual_list.txt' ) )
	process_itd_list( r )
	
rescue Exception => ex	
	puts "Exception: #{ex.message}"
	puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
ensure
	
	puts "Number of ITD processed: #{$num_itds}"
	puts "Number of DDS found: #{$manual_dds_list.size}"
	write_list( $manual_dds_list, OS::Path.combine( MANUAL_PATH, 'dds_manual_list_autogen.txt' ) )
end

# End of mk_manual_dds_list.rb
