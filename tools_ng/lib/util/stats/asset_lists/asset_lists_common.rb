#
# asset_lists_common.rb
# Common Asset Lists Functions
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 7 March 2008
# 

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

ROOT_PATH       = 'asset_list_data'
MASTER_PATH 	= 'asset_list_data/master'
ACTUAL_PATH 	= 'asset_list_data/actual'
MANUAL_PATH 	= 'asset_list_data/manual'
PROJECT_ROOT 	= 'x:/gta/build/independent'
INDEPENDENT_ROOT= 'x:/gta/build/independent'
COMMON_ROOT		= 'x:/gta/build/common'
IMAGE_EXTENSION = 'img'
IMAGE_MOUNT 	= 'image:/'
PACK_MOUNT		= 'pack:/'

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

#
# Add an asset to the specified list with the reference (initialises the
# array of references if it doesn't exist, and doesn't add the reference
# if it already has been included).
#
def add_asset_ref( list, asset, ref )

	raise ArgumentError.new( "Attempt to add empty asset!" ) if 0 == asset.size
	raise ArgumentError.new( "Attempt to add empty reference for asset #{asset}!" ) if 0 == ref.size

    list[asset] = Array.new unless list.has_key?( asset )
    list[asset] << ref unless list[asset].include?( ref )
end

#
# Add an asset to the specified list with the references (initialises the
# array of references if it doesn't exist, and doesn't add the reference
# if it already has been included).
#
def add_asset_refs( list, asset, refs )
	
	raise ArgumentError.new( "Attempt to add empty asset!" ) if 0 == asset.size
	raise ArgumentError.new( "Attempt to add nil reference array for asset #{asset}!" ) if nil == refs
	raise ArgumentError.new( "Attempt to add empty reference for asset #{asset}!" ) if 0 == refs.size
	
	list[asset] = Array.new unless list.has_key?( asset )
	refs.each do |ref|
		list[asset] << ref unless list[asset].include?( ref )
	end
end

#
# Writes out one of our master lists into a text file.
#
def write_list( list, filename )

	puts "Writing #{list.size} entries to #{filename}"
	File.open( filename, 'w' ) do |out|
	
		list.each do |key, references|
            out.write( "#{key};" )
            references.each_with_index do |ref, index|
                out.write( "#{ref};" ) unless index == (references.size - 1)
                out.write( "#{ref}" ) if index == (references.size - 1)
			end
			out.write( "\n" )
		end
	end
end

#
# Read generic output list from an input file written by the 
# `mk_master_assetlist.rb` or `mk_actual_assetlist.rb` scripts.
#
def read_list_from_file( list, filename )

	File.open( filename ) do |fp|
		begin
		
			fp.each do |line|
			
                parts = line.split( ';' )
                key = parts[0].downcase
                parts.each_with_index do |part, index|
                    next if 0 == index
                    add_asset_ref( list, key, part.downcase )
                end
			end
		rescue Exception => ex
		
			puts "Exception: #{ex.message}"
			puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
		ensure
		
			fp.close()
		end
	end
end

#
# Writes out an asset list into a text file, sorting by hash value.
#
def write_sorted_justified_list( list, filename )

	# Sort by container reference
	sorted_list = list.sort do |key, value| 
		
		key[0] = '' if nil == key[0]
		value[0] = '' if nil == value[0]
		key[0]<=>value[0] 
	end

	File.open( filename, 'w' ) do |out|
		begin
	
			out.write( sprintf( "%40s\t%s\n", 'Asset', 'Reference' ) )
			sorted_list.each do |arr|
                if Array == arr[1].class then
                	arr[1].each do |item|
                		out.write( sprintf( "%40s\t%s\n", arr[0].to_s, item.to_s ) )
                	end
                else
                	out.write( sprintf( "%40s\t", arr[0].to_s ) )
                	out.write( arr[1].to_s )
                end
			end
		rescue Exception => ex
		
			puts "Exception: #{ex.message}"
			puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
		
		ensure
			out.close()
		end
	end
end

#
# Writes out a simple array list into a text file
#
def write_array_list( list, filename )

    File.open( filename, 'w' ) do |out|
    
        list.each do |item|
            out.write( "#{item}\n" )             
        end
    end
end

#
# Prints an asset list to stdout.
#
def print_list( list )

	if ( Array == list.class )
	
		list.each do |value|
		
			puts value.to_s
		end
	elsif ( Hash == list.class )
	
		list.each do |key, value|
		
			puts "#{key}\t\t=>\t\t#{value}"
		end	
	end
end

# End of asset_lists_common.rb
