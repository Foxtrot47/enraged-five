#
# cmp_anim_code_data_master_assetlist.rb
# Compare Anim Code and Data Master Asset Lists
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 4 March 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/os/path'
include Pipeline

#-----------------------------------------------------------------------------
# Globals
#-----------------------------------------------------------------------------

MASTER_PATH = 'x:/tools_release/bin/asset_list_data/master'
ACTUAL_PATH = 'x:/tools_release/bin/asset_list_data/actual'
REGEXP_HASH = /(.+);(.+)/
REGEXP_ANIM_CODE = /(.+);(.+)/

# Global hashes map asset name to asset source (e.g. anim name to IAD name for
# the anim lists).
# Global arrays just contain the asset name - as they can be used in multiple
# sources and don't want to confuse the issue.

$master_iad_code_list = Hash.new
$master_iad_data_list = Hash.new
$master_anim_code_list = Hash.new
$master_anim_data_list = Hash.new
$iad_match_list = Hash.new
$iad_nomatch_list = Hash.new
$anim_match_list = Hash.new
$anim_nomatch_list = Hash.new

#
# Read list of IAD and ANIM files from input file provided by Jonathon A.  
# These lists are output from the game code using RAG.
#
def read_iad_anims_from_code( filename )

	File.open( filename ) do |fp|
	
		begin
		
			fp.each do |line|
			
				if ( line =~ REGEXP_ANIM_CODE )
					
					$master_iad_code_list[$1.downcase] = 'code' unless $master_iad_code_list.has_key?( $1 )
					$master_anim_code_list[$2.downcase] = 'code' unless $master_anim_code_list.has_key?( $2 )
				end
			end
		rescue Exception => ex
		
			puts "Exception: #{ex.message}"
			puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
		ensure
		
			fp.close()
		end
	end
end


#
# Read generic output list from an input file written by the 
# `mk_master_assetlist.rb` or `mk_actual_assetlist.rb` scripts.
#
def read_list_from_file( list, filename )

	File.open( filename ) do |fp|
		begin
		
			fp.each do |line|
			
				if ( line =~ REGEXP_HASH )
					
					list[$1.downcase] = $2.downcase if !list.has_key?( $1 )
				end
			end
		rescue Exception => ex
		
			puts "Exception: #{ex.message}"
			puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
		ensure
		
			fp.close()
		end
	end
end


#
# Compare an actual and master list populating the used and unused lists
# as appropriate.
#
def cmp_actual_master_list( data_hash, code_hash, match_list, nomatch_list )

	data_list = data_hash.keys
	data_list.sort!()
	data_list.each_with_index do |item, index|
		data_list[index] = item.downcase
	end
	
	code_list = code_hash.keys
	code_list.sort!()
	code_list.each_with_index do |item, index|
		code_list[index] = item.downcase
	end
		
	# Loop through data list adding to match or nomatch list depending on its
	# occurrence in code list
	data_list.each do |item|
	
		puts "#{item} DATA DATA" if ( data_hash[item].nil? )
	
		match_list[item] = data_hash[item] if code_list.include?( item )
		nomatch_list[item] = data_hash[item] unless code_list.include?( item )
	end
	code_list.each do |item|
	
		puts "#{item} CODE DATA" if ( code_hash[item].nil? )
		
		match_list[item] = code_hash[item] if data_list.include?( item )
		nomatch_list[item] = code_hash[item] unless data_list.include?( item )
	end
end

#
# Writes out one of our asset lists into a text file.
#
def write_list( list, filename )

	# Sort by container reference
	sorted_list = list.sort do |key, value| 
		
		key[1] = '' if nil == key[1]
		value[1] = '' if nil == value[1]
		key[1]<=>value[1] 
	end

	File.open( filename, 'w' ) do |out|
		begin
	
			out.write( sprintf( "%40s %s\n", 'Asset', 'Reference' ) )
			sorted_list.each do |arr|
		
				out.write( sprintf( "%40s %s\n", arr[0].to_s, arr[1].to_s ) )
			end
		rescue
		
		ensure
			out.close()
		end
	end
end

#
# Prints an asset list to stdout.
#
def print_list( list )

	if ( Array == list.class )
	
		list.each do |value|
		
			puts value.to_s
		end
	elsif ( Hash == list.class )
	
		list.each do |key, value|
		
			puts "#{key}\t\t=>\t\t#{value}"
		end	
	end
end

#-----------------------------------------------------------------------------
# Entry Point
#-----------------------------------------------------------------------------

# Read IAD and ANIM from code master (provided by Jonathon's RAG button)
read_iad_anims_from_code( OS::Path.combine( MASTER_PATH, 'anim_master_code_list.txt' ) )

# Read IAD and ANIM from data master
read_list_from_file( $master_iad_data_list, OS::Path.combine( MASTER_PATH, 'iad_master_list.txt' ) )
read_list_from_file( $master_anim_data_list, OS::Path.combine( MASTER_PATH, 'anim_master_list.txt' ) )

#print_list( $master_iad_data_list )

cmp_actual_master_list( $master_iad_data_list, $master_iad_code_list, $iad_match_list, $iad_nomatch_list )
cmp_actual_master_list( $master_anim_data_list, $master_anim_code_list, $anim_match_list, $anim_nomatch_list )

write_list( $iad_match_list, 'asset_list_data/iad_match.txt' )
write_list( $iad_nomatch_list, 'asset_list_data/iad_nomatch.txt' )
write_list( $anim_match_list, 'asset_list_data/anim_match.txt' )
write_list( $anim_nomatch_list, 'asset_list_data/anim_nomatch.txt' )


