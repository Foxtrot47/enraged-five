#
# mk_actual_assetlist.rb
# Make Actual Asset Lists
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 29 February 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/config/projects'
require 'pipeline/game/gamedataloader'
require 'pipeline/game/mapidefile'
require 'pipeline/game/vehicleidefile'
require 'pipeline/game/pedidefile'
require 'pipeline/game/defaultidefile'
require 'pipeline/game/iplfile'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/os/start'
require 'pipeline/util/debug'
require 'pipeline/util/float'
require 'pipeline/util/rage'
require 'pipeline/util/string'
include Pipeline
require "util/asset_lists/asset_lists_common.rb"

require 'find'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

IMAGE_CUTPROPS 	= 'cutsprops:/'
SCRIPT_SOURCES	= 'x:/gta/gta_rage_scripts'
SCRIPT_INCLUDES	= [ 'x:\tools_release\gta_bin\ragescriptcompiler\include_scripts',
					'x:\gta\gta_rage_scripts\include_scripts', 
					'x:\gta\gta_rage_scripts\network\_headers', 
					'x:\gta\gta_rage_scripts\globals', 
					'x:\gta\gta_rage_scripts\headers', 
					'x:\gta\gta_rage_scripts\contacts', 
					'x:\gta\gta_rage_scripts\demo', 
					'x:\gta\gta_rage_scripts\network', 
					'x:\gta\gta_rage_scripts\ambient\headers', 
					'x:\tools_release\gta_bin\ragescriptcompiler\shared_headers', 
					'x:\tools_release\gta_bin\ragescriptcompiler\shared_scripting_tools', 
					'x:\gta\gta_rage_scripts\console\headers', 
					'x:\gta\gta_rage_scripts\cellphone\headers', 
					'x:\gta\gta_rage_scripts\shops\headers', 
					'x:\gta\gta_rage_scripts\activities\headers', 
					'x:\gta\gta_rage_scripts\computer\headers', 
					'x:\gta\gta_rage_scripts\random_interactions\headers', 
					'x:\gta\gta_rage_scripts\project', 
					'x:\gta\gta_rage_scripts\friends', 
					'x:\gta\gta_rage_scripts\friends\headers', 
					'x:\gta\gta_rage_scripts\rsc\headers' ]

SCRIPT_PREPROC	= 'x:/tools_release/bin/util/script_preproc.exe'

STATE_CUTS_NONE = 0
STATE_CUTS_MODELS = 1
STATE_CUTS_HEADER = 2
STATE_CUTS_LIGHTS = 3
STATE_CUTS_EFFECTS = 4

REGEXP_GRCTEXTURE 	= /grcTexture[ \t]+(.+)/ 
REGEXP_SPS			= /([a-z0-9_\-]+\.sva)/i

# Script Parsing RegExp objects
REGEXP_COMMENT_LINES = /^[ \t]*\/\/.*/i
REGEXP_USING_LINES = /^USING \"([A-Za-z0-9_\.]+)\"/i
REGEXP_TEXTURE = /REGISTER_TEXTURE_USE[ \t]*\([ \t]*\"(.+)\"[ \t]*,[ \t]*\"(.+)\"/i
REGEXP_ANIMATION = /REGISTER_ANIM_USE[ \t]*\([ \t]*\"(.+)\"[ \t]*,[ \t]*\"(.+)\"/i
REGEXP_INIT_CUTSCENE = /INIT_CUTSCENE[ \t]*\([ \t]*\"(.+)\"/i
REGEXP_START_CUTSCENE_NOW = /START_CUTSCENE_NOW[ \t]*\([ \t]*\"(.+)\"/i
REGEXP_CLEAR_NAMED_CUTSCENE = /CLEAR_NAMED_CUTSCENE[ \t]*\([ \t]*\"(.+)\"/i

# CUT Files RegExp
REGEXP_CUTS_LIGHTS = /(.*)/
REGEXP_CUTS_EFFECTS = /(.*):(.*) -1 -1 (.*) (.*)/

#-----------------------------------------------------------------------------
# Globals
#-----------------------------------------------------------------------------

$script_processed_headers = Array.new
$script_register_texture_use = 0
$script_register_anim_use = 0
$script_cut_use = 0

$actual_iad_list = Hash.new     # Key: asset name, Value: array of references
$actual_anim_list = Hash.new    # Key: asset name, Value: array of references
$actual_itd_list = Hash.new     # Key: asset name, Value: array of references
$actual_dds_list = Hash.new     # Key: asset name, Value: array of references
$actual_models_list = Hash.new  # Key: asset name, Value: array of references
$actual_sco_list = Hash.new     # Key: asset name, Value: array of references
$actual_cut_list = Hash.new     # Key: asset name, Value: array of references

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

#
# Return all textures from path (in .SVA files)
#
def get_textures_for_path( path, r )

	textures = Array.new
	sva_files_used = Array.new
	entity_type = r.find_files( "#{path}/entity.type" )
	entity_type.each do |entity|
    
    	r.util.copy_file( "#{path}/#{entity}", 'c:/temp/entity.type' )
    	File.open( 'c:/temp/entity.type' ) do |fp|
			begin				
				fp.each do |line|
					if ( line =~ REGEXP_SPS ) then						
						sva_files_used << $1.downcase
					end
				end
			rescue Exception => ex				
				puts "Exception: #{ex.message}"
				puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
			ensure
				fp.close
				File.delete( 'c:/temp/entity.type' )
			end
     	end
  	end
  
	sva_files = r.find_files( "#{path}/*.sva" )	
	sva_files.each do |sva|
	
		puts "Error: SVA being ignored: #{sva.downcase}" unless sva_files_used.include?( sva.downcase )
		puts "List: #{sva_files.join(';')}" unless sva_files_used.include?( sva.downcase )
		next unless sva_files_used.include?( sva.downcase )
	
		r.util.copy_file( "#{path}/#{sva}", 'c:/temp/sva.tmp' )
		File.open( 'c:/temp/sva.tmp' ) do |fp|
			begin
			
				fp.each do |line|
				
					if ( line =~ REGEXP_GRCTEXTURE ) then
					
						textures << $1 unless textures.include?( $1 )
					end
				end
			rescue Exception => ex
				
				puts "Exception: #{ex.message}"
				puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
			ensure

				fp.close()
				File.delete( 'c:/temp/sva.tmp' )
			end		
		end
	end
	
	# Now look for light IDE files (to parse 2DFX section)
	ide_files = r.find_files( "#{path}/*.ide" )
	ide_files.each do |idefile|
		
		r.util.copy_file( "#{path}/#{idefile}", 'c:/temp/drawable.ide' )
		idefileobj = Game::MapIDEFile.new( 'c:/temp/drawable.ide' )
		
		# 2DFX Object Definitions
		idefileobj.fx2d_definitions.each do |fx|
		
			puts "SHADOW DDS: #{fx.shadow_name}" if ( Game::MapIDELight2DFXDef == fx.class ) and ( nil != fx.shadow_name )
            textures << fx.shadow_name if ( Game::MapIDELight2DFXDef == fx.class ) and ( nil != fx.shadow_name )
		end
	end
	
	textures
end

#
# Process object definition (image mounted already)
#
def process_objdef( objdef, r )
	
	# Process the object definition by fetching all textures referenced
	# by the model, and adding the TXD name for the definition to
	# our actual list :)
	if ( nil != objdef.dd_name ) then
	
		r.pack.mount( "#{IMAGE_MOUNT}#{objdef.dd_name}.idd", PACK_MOUNT )
		textures = get_textures_for_path( "#{PACK_MOUNT}#{objdef.name}", r )
		textures.each do |tex|
			add_asset_ref( $actual_dds_list, tex, objdef.idefile.filename )
		end
		r.pack.unmount( "#{IMAGE_MOUNT}#{objdef.dd_name}.idd" )
		
	else
		
		# Try IDR then IFT
		files = r.find_files( "#{IMAGE_MOUNT}#{objdef.name}.idr" )
		if ( files.size > 0 ) then
			
			r.pack.mount( "#{IMAGE_MOUNT}#{objdef.name}.idr", PACK_MOUNT )
			textures = get_textures_for_path( PACK_MOUNT, r )
			textures.each do |tex|
                add_asset_ref( $actual_dds_list, tex, objdef.idefile.filename )
			end
			r.pack.unmount( "#{IMAGE_MOUNT}#{objdef.name}.idr" )
		else
		
			files = r.find_files( "#{IMAGE_MOUNT}#{objdef.name}.ift" )
			if ( files.size > 0 ) then
	
				r.pack.mount( "#{IMAGE_MOUNT}#{objdef.name}.ift", PACK_MOUNT )
				textures = get_textures_for_path( PACK_MOUNT, r )
				textures.each do |tex|
                    add_asset_ref( $actual_dds_list, tex, objdef.idefile.filename )	
				end
				r.pack.unmount( "#{IMAGE_MOUNT}#{objdef.name}.ift" )
			end
		end
	end
end


#
# Process maps IDE/IPL data to find referenced assets.
#
def process_maps( project, r )

	gamedata = Game::GameDataLoader.new( project )
	gamedata.load_ides( true )
	
	# Loop through all our maps, all our definitions...
	count = 0
	total = gamedata.project_ides.size
	gamedata.project_ides.each do |idefilename, idefile|
	 
		count += 1
		progress = 100.0 * ( count / total.to_f )
	
		
		# DEBUG DEBUG
		#next unless idefilename.ends_with( 'manhat09.ide' )
		#next unless idefile.filename.ends_with( 'rubbish.ide' )	
		#puts "# objs: #{idefile.object_definitions.size}"
		#puts "# tobjs: #{idefile.time_object_definitions.size}"
		# END DEBUG
		
		puts "Processing IDE: #{idefile.filename} [#{count} of #{total} - #{progress.to_s(2)}%]"
	
	
		# Mount IDEFile's Image
		r.image.mount( idefile.image_filename, IMAGE_MOUNT )
	
		# Object definitions
		idefile.object_definitions.each do |objdef|
		
			add_asset_ref( $actual_models_list, objdef.name, idefile.filename )
			add_asset_ref( $actual_itd_list, objdef.txd_name, idefile.filename )

			process_objdef( objdef, r )
		end
		# Time Object definitions
		idefile.time_object_definitions.each do |tobjdef|
		
            add_asset_ref( $actual_models_list, tobjdef.name, idefile.filename )
            add_asset_ref( $actual_itd_list, tobjdef.txd_name, idefile.filename )

			process_objdef( tobjdef, r )
		end
		# Anim Object definitions
		idefile.anim_definitions.each do |animdef|
		
            add_asset_ref( $actual_anim_list, animdef.name, idefile.filename )
            add_asset_ref( $actual_models_list, animdef.name, idefile.filename )
            add_asset_ref( $actual_iad_list, animdef.anim_filename, idefile.filename )
            add_asset_ref( $actual_itd_list, animdef.txd_name, idefile.filename )
			
			# Get textures for anim model
			process_objdef( animdef, r )
		end
		# Time Anim Object definitions
		idefile.time_anim_definitions.each do |tanimdef|
		
            add_asset_ref( $actual_anim_list, tanimdef.name, idefile.filename )
            add_asset_ref( $actual_models_list, tanimdef.name, idefile.filename )
            add_asset_ref( $actual_iad_list, tanimdef.anim_filename, idefile.filename )
            add_asset_ref( $actual_itd_list, tanimdef.txd_name, idefile.filename )
			
			# Get textures for anim model
			process_objdef( tanimdef, r )
		end
		# 2DFX Object Definitions
		idefile.fx2d_definitions.each do |fx|
		
            add_asset_ref( $actual_models_list, fx.name, idefile.filename )
            if ( Game::MapIDELight2DFXDef == fx.class and nil != fx.shadow_name )
            	add_asset_ref( $actual_dds_list, fx.shadow_name, idefile.filename )
            end
		end
		# Parent TXD Definitions
		idefile.parent_txd_definitions.each do |txd|
		
            add_asset_ref( $actual_itd_list, txd.name, idefile.filename )
            add_asset_ref( $actual_itd_list, txd.parent_txd, idefile.filename )
		end
		
		# Unmount IDEFile's Image
		r.image.unmount( idefile.image_filename )
	end
end

#
# Find a filename within a directory tree structure
#
def find_script_file( filename )
	
	SCRIPT_INCLUDES.each do |inc_path|
		Find.find( inc_path ) do |path|
	
			# Don't recurse through include directories, the script compiler
			# wouldn't do that... so let us not do it too.
			Find.prune if FileTest.directory?( path ) && path.downcase != inc_path.downcase
	
			pathfilename = OS::Path.get_filename( path )
			return path if FileTest.file?( path ) and pathfilename.downcase == filename.downcase
		end
	end
	Find.find( SCRIPT_SOURCES ) do |path|

		pathfilename = OS::Path.get_filename( path )
		return path if FileTest.file?( path ) and pathfilename.downcase == filename.downcase
	end
	
	false
end

#
# Process a single script file for used assets.
#
def process_script_for_assets( script_filename, container, r )

	ext = OS::Path.get_extension( script_filename )
	puts "Processing #{ext.upcase}: #{script_filename}"
	
	script_basename = OS::Path.get_basename( script_filename ).downcase
	add_asset_ref( $actual_sco_list, script_basename, container )

	# Use our custom preprocessor to remove comment string from the SC files.
	input_file = "c:/temp/#{script_basename}.input.#{ext.downcase}"
	output_file = "c:/temp/#{script_basename}.output.#{ext.downcase}"
	
	OS::FileUtilsEx.copy_file( script_filename, input_file )	
	command = "#{SCRIPT_PREPROC} < #{input_file} > #{output_file}"
	system( command )

	File.open( output_file ) do |fp|
	
		begin
			fp.each do |line|

				# Skip blank lines
				next if ( 0 == line.size )
				# Skip comment lines
				next if ( line =~ REGEXP_COMMENT_LINES )
		
				# Load and parse headers for assets
				if ( line =~ REGEXP_USING_LINES ) then
				
					header_file = line[/\"([A-Za-z0-9_\.]+)\"/][/([A-Za-z0-9_\.]+)/]
					sch_path = find_script_file( header_file )
					
					puts "Header #{sch_path} not found." if !sch_path
					
					if ( nil != sch_path and String == sch_path.class ) then
						
						sch_path = OS::Path.normalise( sch_path )
						# Skip if already processed SCH
						next if $script_processed_headers.include?( sch_path )
							
						$script_processed_headers << sch_path.downcase
						process_script_for_assets( sch_path, container, r )
					end	
				# Otherwise lets look for some script commands that we are
				# interested in.			
				elsif ( line =~ REGEXP_TEXTURE ) then
				
				    add_asset_ref( $actual_itd_list, $1, script_filename )
				    add_asset_ref( $actual_dds_list, $2, script_filename )
					$script_register_texture_use += 1
			
				elsif ( line =~ REGEXP_ANIMATION ) then
				
				    add_asset_ref( $actual_iad_list, $1, script_filename )
				    add_asset_ref( $actual_anim_list, $2, script_filename )
					$script_register_anim_use += 1
		
                elsif (( line =~ REGEXP_INIT_CUTSCENE ) or 
                       ( line =~ REGEXP_START_CUTSCENE_NOW ) or 
                       ( line =~ REGEXP_CLEAR_NAMED_CUTSCENE )) then
			
                    add_asset_ref( $actual_cut_list, $1, script_filename )
                    $script_cut_use += 1
			
				end
			end
		rescue Exception => ex
			
			print_exception( ex )
		ensure
			fp.close()
		end
	end
end

#
# Process a single script image for assets.
#
def process_script_image( image, r )

	r.image.mount( image, IMAGE_MOUNT )
	
	files = r.find_files( "#{IMAGE_MOUNT}*.sco" )
	# For each SCO (Script Object), find its source file in our script sources
	# root directory.  Open it and parse its contents and any headers it 
	# refereces.
	files.each do |file|
	
		sc_filename = OS::Path.replace_ext( file, 'sc' )
		sc_path = find_script_file( sc_filename )
		
		puts "Source #{sc_filename} not found." if !sc_path
		process_script_for_assets( sc_path, sc_path, r ) if sc_path
	end	
	
	r.image.unmount( image )
end

#
# Process cut files for cutscene model names
#
def process_cuts( image, r )

	cutprops_image = OS::Path.combine( INDEPENDENT_ROOT, 'anim', 'cutsprops.img' )
	r.image.mount( image, IMAGE_MOUNT )
	r.image.mount( cutprops_image, IMAGE_CUTPROPS )
	files = r.find_files( "#{IMAGE_MOUNT}*.cut" )

	begin
	
		# For each CUT file, find its [MODELS] section and extract the model names,
		# and some ANIM and IAD file names.
		files.each do |file|
		
			puts "Processing CUT: #{file}"
			r.util.copy_file( "#{IMAGE_MOUNT}#{file}", 'c:/temp/temp.cut' )
			File.open( 'c:/temp/temp.cut' ) do |fp|
				begin
				
					state = STATE_CUTS_NONE
					num_sections = 0
					fp.each do |line|
					
						# Skip empty lines
						next if 0 == line.size
					
						if ( line =~ /\[CUTSCENE_HEADER\]/ )
						
							state = STATE_CUTS_HEADER
						elsif ( line =~ /\[\/CUTSCENE_HEADER\]/ )
						
							state = STATE_CUTS_NONE
						elsif ( line =~ /\[MODELS\]/ )
							
							state = STATE_CUTS_MODELS
						elsif ( line =~ /\[\/MODELS\]/ )
							
							state = STATE_CUTS_NONE
						elsif ( line =~ /\[LIGHTS\]/ )
							
							state = STATE_CUTS_LIGHTS
						elsif ( line =~ /\[\/LIGHTS\]/ )
						
							state = STATE_CUTS_NONE
						elsif ( line =~ /\[EFFECTS\]/ )
							
							state = STATE_CUTS_EFFECTS
						elsif ( line =~ /\[\/EFFECTS\]/ )
						
							state = STATE_CUTS_NONE
						elsif STATE_CUTS_HEADER == state
					
							parts = line.split( ' ' )
							next if parts.size < 1
							# Get number of sections
							num_sections = parts.size - 1 if parts.size > 1
								
							# Anim Dict References (auto-generated from number of sections)
							for index in 0..(num_sections-1)
							
								filename = OS::Path.get_basename( file )
								ext = OS::Path.get_extension( file )
								add_asset_ref( $actual_iad_list, "#{filename}_#{index}".downcase, "#{image}-#{file}" )
								add_asset_ref( $actual_anim_list, "camera_#{index}", "#{image}-#{file}" )
							end	
							
						elsif STATE_CUTS_MODELS == state
						
							parts = line.split( ' ' )
							next if parts.size < 1
								
							# Model Reference
							name = parts[1].downcase
							add_asset_ref( $actual_models_list, name, "#{image}-#{file}" )
							add_asset_ref( $actual_itd_list, name, "#{image}-#{file}" )
							
							# Texture References for Model (in cutprops.img)
							# Find IDR/IFT
							files = r.find_files( "#{IMAGE_CUTPROPS}#{name}.idr" )
							if ( files.size > 0 ) then
							
								r.pack.mount( "#{IMAGE_CUTPROPS}#{name}.idr", PACK_MOUNT )
								textures = get_textures_for_path( PACK_MOUNT, r )
								textures.each do |txd|
	                                add_asset_ref( $actual_dds_list, txd.downcase, "#{image}-#{file}" )
								end
								r.pack.unmount( "#{IMAGE_CUTPROPS}#{name}.idr" )
							end
							files = r.find_files( "#{IMAGE_CUTPROPS}#{name}.ift" )
							if ( files.size > 0 ) then
							
								r.pack.mount( "#{IMAGE_CUTPROPS}#{name}.ift", PACK_MOUNT )
								textures = get_textures_for_path( PACK_MOUNT, r )
								textures.each do |txd|
	                                add_asset_ref( $actual_dds_list, txd.downcase, "#{image}-#{file}" )
								end
								r.pack.unmount( "#{IMAGE_CUTPROPS}#{name}.ift" )
							end
							
							# Find IDD
							files = r.find_files( "#{IMAGE_CUTPROPS}#{name}.idd" )
							if ( files.size > 0 ) then
							
								r.pack.mount( "#{IMAGE_CUTPROPS}#{name}.idd", PACK_MOUNT )
								dirs = r.find_dirs( PACK_MOUNT )
								dirs.each do |dir|
									textures = get_textures_for_path( "#{PACK_MOUNT}#{dir}", r )
									textures.each do |txd|
	                                    add_asset_ref( $actual_dds_list, txd.downcase, "#{image}-#{file}" )
									end
								end
								r.pack.unmount( "#{IMAGE_CUTPROPS}#{name}.idd" )
							end
							
							# Anim References (incl. camera_x anims)
							parts.each_with_index do |part, index|
							
								next if index <= 1
								add_asset_ref( $actual_anim_list, part.downcase, "#{image}-#{file}" )
							end
							
						elsif STATE_CUTS_LIGHTS == state
						
							# Anim references (basic)
							if ( line =~ REGEXP_CUTS_LIGHTS ) then
								name = $1.downcase
								# BUG : this can sometimes get empty references!  For blank lines.
								add_asset_ref( $actual_anim_list, name, "#{image}-#{file}" ) if name.size > 0
							end
							
						elsif STATE_CUTS_EFFECTS == state
						
							# Anim references (complex as only when effect has duration)
							# Looking for: <name>:<anim> -1 -1 <a> <b> 
							#   where a != b for effect duration
							if ( line =~ REGEXP_CUTS_EFFECTS ) then
								
								anim = $2.downcase
								add_asset_ref( $actual_anim_list, anim, "#{image}-#{file}" )
							end						
						end
					end
				rescue Exception => ex
				
					puts "Exception: #{ex.message}"
					puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
				ensure
					fp.close()
				end
			end
		end
	rescue Exception => ex
	
		print_exception( ex )
	end
	r.image.unmount( cutprops_image )
	r.image.unmount( image )
end

#
# Process generic (ped/veh) definition (image mounted already)
#
def process_genericdef( name, idefilename, r )

	# Process the vehicle definition by fetching all textures referenced
	# by the model, and adding the texture names for the definition to
	# our actual list :)
		
	# Try IDR then IFT
	files = r.find_files( "#{IMAGE_MOUNT}#{name}.idr" )
	if ( files.size > 0 ) then
		
		r.pack.mount( "#{IMAGE_MOUNT}#{name}.idr", PACK_MOUNT )
		textures = get_textures_for_path( PACK_MOUNT, r )
		textures.each do |tex|
            add_asset_ref( $actual_dds_list, tex, idefilename )
		end
		r.pack.unmount( "#{IMAGE_MOUNT}#{name}.idr" )
	else
	
		files = r.find_files( "#{IMAGE_MOUNT}#{name}.ift" )
		if ( files.size > 0 ) then

			r.pack.mount( "#{IMAGE_MOUNT}#{name}.ift", PACK_MOUNT )
			textures = get_textures_for_path( PACK_MOUNT, r )
			textures.each do |tex|
                add_asset_ref( $actual_dds_list, tex, idefilename )
			end
			r.pack.unmount( "#{IMAGE_MOUNT}#{name}.ift" )
		end
	end
end

#
# Process vehicles IDE file
#
def process_vehicles_ide( filename, image, r )

	r.image.mount( image, IMAGE_MOUNT )

	vehicleide = Game::VehicleIDEFile.new( filename, image )
	vehicleide.vehicle_definitions.each do |vehicledef|
	
		name = vehicledef.name.downcase
		txd_name = vehicledef.txd_name.downcase
		add_asset_ref( $actual_models_list, name, filename )
		add_asset_ref( $actual_itd_list, txd_name, filename )
		
		process_genericdef( vehicledef.name, filename, r )
	end
	vehicleide.parent_txd_definitions.each do |txd|
	
		txd_name = txd.name.downcase
		add_asset_ref( $actual_itd_list, txd_name, filename )
	end
	
	r.image.unmount( image )
end


#
# Process ped IDE file
#
def process_peds_ide( filename, image, r )

    r.image.mount( image, IMAGE_MOUNT )

	pedide = Game::PedIDEFile.new( filename, image )
	pedide.ped_definitions.each do |peddef|
	
		name = peddef.name.downcase
		prop = peddef.props.downcase
		add_asset_ref( $actual_models_list, name, filename )
		add_asset_ref( $actual_itd_list, name, filename )
		add_asset_ref( $actual_itd_list, prop, filename )
		
		process_genericdef( name, filename, r )
	end
	
	r.image.unmount( image )
end

#
# Process default IDE file
#
def process_default_ide( filename, image, r )

    r.image.mount( image, IMAGE_MOUNT )

	defaultide = Game::DefaultIDEFile.new( filename )
	defaultide.weapon_definitions.each do |weapon|
	
		name = weapon.name
		txd = weapon.txd_name
		anim_dict = weapon.anim_dict
		
		add_asset_ref( $actual_models_list, name, filename )
		add_asset_ref( $actual_itd_list, txd, filename )
		add_asset_ref( $actual_iad_list, anim_dict, filename ) unless anim_dict.nil?
		
		process_genericdef( name, filename, r )
	end
	
	r.image.unmount( image )
end

#-----------------------------------------------------------------------------
# Entry Point
#-----------------------------------------------------------------------------

begin
    # Ensure ACTUAL_PATH exists
    Dir.mkdir( ROOT_PATH ) unless File.directory?( ROOT_PATH )
    Dir.mkdir( ACTUAL_PATH ) unless File.directory?( ACTUAL_PATH )

	project = Pipeline::Config.instance.projects['gta4']
	project.load_config()
	r = RageUtils.instance

	script_dir = OS::Path.combine( COMMON_ROOT, 'data', 'cdimages' )
	process_script_image( OS::Path.combine( script_dir, 'script_rel.img' ), r )
	process_script_image( OS::Path.combine( script_dir, 'script_network_rel.img' ), r )
	process_maps( project, r )
	process_cuts( OS::Path.combine( INDEPENDENT_ROOT, 'anim', 'cuts.img' ), r )
	process_vehicles_ide( OS::Path.combine( COMMON_ROOT, 'data', 'vehicles.ide' ), 
		OS::Path.combine( INDEPENDENT_ROOT, 'models', 'cdimages', 'vehicles.img' ), r )
	process_peds_ide( OS::Path.combine( COMMON_ROOT, 'data', 'peds.ide' ), 
		OS::Path.combine( INDEPENDENT_ROOT, 'models', 'cdimages', 'componentpeds.img' ), r )
	process_default_ide( OS::Path.combine( COMMON_ROOT, 'data', 'default.ide' ), 
	    OS::Path.combine( INDEPENDENT_ROOT, 'models', 'cdimages', 'weapons.img' ), r )
rescue Exception => ex

	print_exception( ex )
ensure

	# DEBUG
	puts "Number of parsed script headers: #{$script_processed_headers.size}"
	puts "Number of Script REGISTER_TEXTURE_USE: #{$script_register_texture_use}"
	puts "Number of Script REGISTER_ANIM_USE: #{$script_register_anim_use}"
	puts "Number of Script CUT references: #{$script_cut_use}"
	# END DEBUG
	
	puts "Number of IAD:     #{$actual_iad_list.size}"
	puts "Number of ANIM:    #{$actual_anim_list.size}"
	puts "Number of ITD:     #{$actual_itd_list.size}"
	puts "Number of DDS:     #{$actual_dds_list.size}"
	puts "Number of MODELS:  #{$actual_models_list.size}"
	puts "Number of SCO:     #{$actual_sco_list.size}"
	puts "Number of CUT:     #{$actual_cut_list.size}"
	
	write_list( $actual_iad_list, OS::Path.combine( ACTUAL_PATH, 'iad_actual_list.txt' ) )
	write_list( $actual_anim_list, OS::Path.combine( ACTUAL_PATH, 'anim_actual_list.txt' ) )
	write_list( $actual_itd_list, OS::Path.combine( ACTUAL_PATH, 'itd_actual_list.txt' ) )
	write_list( $actual_dds_list, OS::Path.combine( ACTUAL_PATH, 'dds_actual_list.txt' ) )
	write_list( $actual_models_list, OS::Path.combine( ACTUAL_PATH, 'models_actual_list.txt' ) )
	write_list( $actual_sco_list, OS::Path.combine( ACTUAL_PATH, 'sco_actual_list.txt' ) )
	write_list( $actual_cut_list, OS::Path.combine( ACTUAL_PATH, 'cut_actual_list.txt' ) )
end

# End of mk_actual_assetlist.rb
