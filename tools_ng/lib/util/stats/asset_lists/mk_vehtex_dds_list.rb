#
# mk_vehtex_dds_list.rb
# Make manual DDS list from vehtex files
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 8 March 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/util/rage'
require 'pipeline/util/string'
include Pipeline
require 'util/asset_lists/asset_lists_common'
require 'find'

#-----------------------------------------------------------------------------
# Globals
#-----------------------------------------------------------------------------

$manual_dds_list = Hash.new

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

def process
	filedone = Array.new
	
	Find.find( 'x:/gta/gta_art/vehicles' ) do |path|
		
		if FileTest.file?( path )
			# Only process .VEHTEX files
			Find.prune unless path.ends_with( '.vehtex' )
			# Only process files once
			Find.prune if filedone.include?( path )
			
			puts "Processing VEHTEX: #{path}"
			File.open( path ) do |fp|
				
				fp.each do |line|
					
					if ( line =~ /\"(.*)\"/ ) then
						dds = OS::Path.get_basename($1.downcase)
						add_asset_ref( $manual_dds_list, dds, "shared_vehicle_texture-#{path}" )
					end
				end
			end
		end
	end
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------

begin
	process	
	
rescue Exception => ex	
	puts "Exception: #{ex.message}"
	puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
ensure
	
	puts "Number of DDS found: #{$manual_dds_list.size}"
	write_list( $manual_dds_list, OS::Path.combine( MANUAL_PATH, 'dds_manual_list_vehtex.txt' ) )
end

# End of mk_manual_dds_list.rb
