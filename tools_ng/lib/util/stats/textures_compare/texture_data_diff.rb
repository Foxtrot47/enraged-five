#
# File:: texture_data_diff.rb
# Description:: Extract textures from two projects map data and then compare
#               them.  Textures from the master project are only overwritten in
#               the output directory if their main mipmap is different.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 16 October 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/fileformats/dds_raw'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/util/pack_stack_manager'
require 'pipeline/util/rage'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--master_project', '-m', OS::Getopt::REQUIRED, 'Master project key (e.g. gta4).' ],
	[ '--master_input', '-i', OS::Getopt::REQUIRED, 'Master project input path (e.g. x:/gta/build/independent/data/maps).' ],
	[ '--episodic_project', '-e', OS::Getopt::REQUIRED, 'Episodic project key (e.g. gta_e1).' ],
	[ '--episodic_input', '-f', OS::Getopt::REQUIRED, 'Episodic project input path (e.g. x:/gta_e1/build/independent/data/maps).' ],
	[ '--output', '-o', OS::Getopt::REQUIRED, 'Output directory for textures.' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'Display this usage information.' ]
]
EXTENSIONS = [ 'img', 'idd', 'idr', 'ift', 'itd', 'rpf' ]
MOUNT_POINT = 'pack:/'

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

#
# Find and process all project independent files.
#
def extract_project_textures( project, branch, input, output, r )

	packstack = Util::PackStackManager.new( r, MOUNT_POINT )

	input_wildcard = OS::Path::combine( input, '*.*' )
	project_independent_files = OS::FindEx::find_files_recurse( input_wildcard )
	project_independent_files.each do |filename|
		extract_file_textures( project, branch, packstack, filename, output, r )
	end
end

#
# Process a single project file.  This determines the file type (on extension)
# and filters it into the corresponding process_file_* function.
#
def extract_file_textures( project, branch, packstack, filename, output, r )

	ext = OS::Path::get_extension( filename )
	case ext
	when 'img'
		extract_file_pack_textures( project, branch, packstack, filename, output, r )
		
	when 'idr', 'ift', 'itd', 'rpf'
		extract_file_pack_textures( project, branch, packstack, filename, output, r )
		
	end
end

#
# Mount our pack file and process its contents.  The PackStackManager allows
# us to do this recursively and handles all of the mounting/unmounting for us.
#
def extract_file_pack_textures( project, branch, packstack, filename, output, r )

	packs_dir = OS::Path::combine( output, 'packs' )
	FileUtils::mkdir_p( output ) unless ( ::File::directory?( output ) )
	FileUtils::mkdir_p( packs_dir ) unless ( ::File::directory?( packs_dir ) )
	
	packstack.push( filename )
	pack_files = r.util.find_files( OS::Path::combine( MOUNT_POINT, '*.*' ), false, true ).split( ',' )
	pack_files.each do |pack_filename|
		# Skip files that won't contain textures.
		ext = OS::Path::get_extension( pack_filename )
		next unless ( EXTENSIONS.include?( ext ) or ( 0 == ext.casecmp( 'dds' ) ) )
	
		if ( EXTENSIONS.include?( ext ) ) then	
			# Process files that might...
			dst = OS::Path::combine( packs_dir, OS::Path::get_filename( pack_filename ) )
			r.util.copy_file( pack_filename, dst ) unless ( ::File::exists?( dst ) )
			
			extract_file_pack_textures( project, branch, packstack, dst, output, r )

		else
			# DDS
			FileUtils::mkdir_p( output ) unless ( ::File::directory?( output ) )
	
			dst = OS::Path::combine( output, OS::Path::get_filename( pack_filename ) )
			r.util.copy_file( pack_filename, dst ) unless ( ::File::exists?( dst ) )
			
		end
	end	
	packstack.pop( )
end

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

	begin
        # Force output to stdout for console scripts.
        Pipeline::Config::instance()::logtostdout = true
		
		g_AppName = File::basename( __FILE__, '.rb' )
		g_Log = Log.new( g_AppName )
		g_MasterProjectName = ''
		g_EpisodicProjectName = ''
		g_MasterProject = nil
		g_EpisodicProject = nil
		g_Output = ''
		g_Config = Pipeline::Config::instance()
		g_Options, g_Trailing = OS::Getopt::getopts( OPTIONS )
		if ( g_Options['help'] ) then
			puts OS::getopt::usage( OPTIONS )
			exit( 1 )
		end
		
		#---------------------------------------------------------------------
		# Options Processing
		#---------------------------------------------------------------------
		
		g_MasterProjectName = ( nil == g_Options['master_project'] ) ? '' : g_Options['master_project']
		if ( not g_Config.projects.has_key?( g_MasterProjectName ) ) then
			puts OS::Getopt::usage( OPTIONS )
			puts "\nError master project #{g_MasterProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_MasterProject = g_Config.projects[g_MasterProjectName]
		if ( not g_MasterProject.enabled ) then
			puts "\nError master project #{g_MasterProjectName} is not enabled on this machine, re-run installer."
			exit( 3 )
		end
		g_MasterProjectInput = ( nil == g_Options['master_input'] ) ? g_MasterProject.independent : g_Options['master_input']
		
		g_EpisodicProjectName = ( nil == g_Options['episodic_project'] ) ? '' : g_Options['episodic_project']
		if ( not g_Config.projects.has_key?( g_EpisodicProjectName ) ) then
			puts OS::Getopt::usage( OPTIONS )
			puts "\nError episodic project #{g_EpisodicProjectName} does not exist or its configuration is unreadable."
			exit( 4 )
		end
		g_EpisodicProject = g_Config.projects[g_EpisodicProjectName]
		if ( not g_EpisodicProject.enabled ) then
			puts "\nError episodic project #{g_EpisodicProjectName} is not enabled on this machine, re-run installer."
			exit( 5 )
		end
		g_EpisodicProjectInput = ( nil == g_Options['episodic_input'] ) ? g_EpisodicProject.independent : g_Options['episodic_input']
		
		g_Output = ( nil == g_Options['output'] ) ? nil : ::File::expand_path( g_Options['output'] )
		if ( g_Output.nil? ) then
			puts 'No output path specified.'
			puts OS::Getopt::usage( OPTIONS )
			exit( 6 )
		end
		
		#---------------------------------------------------------------------
		# Project Processing
		#---------------------------------------------------------------------

		g_MasterTemp = ::File::expand_path( 'master_temp' )
		g_EpisodicTemp = ::File::expand_path( 'episodic_temp' )
		FileUtils::mkdir_p( g_MasterTemp )
		FileUtils::mkdir_p( g_EpisodicTemp )
		
		# Both projects must use the same Rage gem or we are rubber ducked.
		r = RageUtils::new( g_MasterProject, g_MasterProject.default_branch )
		
		# Extract the texture data.
		## extract_project_textures( g_MasterProject, g_MasterProject.default_branch, g_MasterProjectInput, g_MasterTemp, r )
		## extract_project_textures( g_EpisodicProject, g_EpisodicProject.default_branch, g_EpisodicProjectInput, g_EpisodicTemp, r )
		
		# Combine the texture data, replacing master textures when the episodic
		# textures have a different main image / top-level mipmap.
		
		# Copy master texture data to output directory.
		master_total = 0
		FileUtils::mkdir_p( g_Output )
		Dir::open( g_MasterTemp ) do |dir|
			dir.each do |filename|
				next unless ( filename.ends_with( '.dds' ) )
				src = OS::Path::combine( g_MasterTemp, filename )
				dst = OS::Path::combine( g_Output, filename )
				master_total += 1
				next if ( ::File::exists?( dst ) )
				
				puts "Copy #{src} to #{dst}..."
				FileUtils::cp( src, dst )
			end
		end
		
		# Loop through the episodic textures copying to output directory if the
		# main mipmap is different.
		episodic_total = 0
		episodic_copied_no_master = 0
		episodic_copied_image_diff = 0
		master_kept = 0
		Dir::open( g_EpisodicTemp ) do |dir|
			dir.each do |filename|
				next unless ( filename.ends_with( '.dds' ) )
				episodic_total += 1
				src = OS::Path::combine( g_EpisodicTemp, filename )
				dst = OS::Path::combine( g_Output, filename )
				
				# If the destination texture doesn't exist, then copy it.
				if ( not ::File::exists?( dst ) ) then
					puts "Copy #{src} to #{dst} as it doesn't exist."
					episodic_copied_no_master += 1
					FileUtils::cp( src, dst )
				
				else
					dds_src = FileFormats::DDSFileRaw::from_filename( src )
					dds_dst = FileFormats::DDSFileRaw::from_filename( dst )
					
					if ( dds_src.imagedata != dds_dst.imagedata ) then
						puts "Copy #{src} to #{dst} as their main image data is different."
						episodic_copied_image_diff += 1
						FileUtils::cp( src, dst )
						
					else
						puts "Not copying #{src} as their main image data is the same."
						master_kept += 1
					
					end
				end
			end
		end
		# Print summary info
		puts "Total master textures:                   #{master_total}"
		puts "Total episodic textures:                 #{episodic_total}"
		puts "\nEpisodic textures of which"
		puts "  Master textures kept:                  #{master_kept}"
		puts "  Episodic textures copied (no master):  #{episodic_copied_no_master}"
		puts "  Episodic textures copied (image diff): #{episodic_copied_image_diff}"
		
	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join( "\n" )
	end
end

# texture_data_diff.rb
