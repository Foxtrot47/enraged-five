#
# File:: data_compare_textures.rb
# Description:: Compares two sets of textures highlighting differences.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 7 October 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/os/start'
require 'pipeline/util/diff'
require 'pipeline/util/string'
include Pipeline
require 'progressbar'

require 'common'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--output', '-i', OS::Getopt::REQUIRED, 'simple texture differences output.' ],
	[ '--csv', '-i', OS::Getopt::REQUIRED, 'complete CSV output.' ],
	[ '--input', '-i', OS::Getopt::REQUIRED, 'GNU diff output file.' ],
    [ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
DDSINFO_CMD = 'x:/pipedev/tools/bin/ddsinfo.exe -c -d'
FILES_DIFF_REGEXP = /^Files (.+) and (.+) differ$/
FILES_DDS_REGEXP  = /(.*) ([0-9]+) x ([0-9]+) x ([0-9]+) (.*)$/

#-----------------------------------------------------------------------------
# Classes
#-----------------------------------------------------------------------------



#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

def get_texture_info( filename )
	throw IOError.new( "File does not exist or is not readable: #{filename}." ) \
		unless ( File::exists?( filename ) and File::readable?( filename ) )
		
	texture = nil
	stat, out, err = OS::start( "#{DDSINFO_CMD} #{filename}" )
	out.each do |dds_line|
		if ( dds_line =~ FILES_DDS_REGEXP ) then
			texture = TextureInfo.new( filename, $2, $3, $4, $5 )
			break
		end
	end	
	texture
end

def diff_directories( input, output, csv )
	diffs = []

	stdout = nil
	File.open( input ) do |fp|
		stdout = fp.readlines()
	end
	
	fp_csv = File.open( csv, 'w' )
	fp_output = File.open( output, 'w' )
	
	stdout.each do |line|
		next if ( line.starts_with( 'Only in' ) )
		next unless ( line.starts_with( 'Files' ) )

		begin
			if ( line =~ FILES_DIFF_REGEXP ) then
				
				t1 = get_texture_info( $1 )
				t2 = get_texture_info( $2 )
				diffs << TextureDiff.new( t1, t2 )
				
				# Output CSV
				fp_csv.puts( diffs.last.to_csv() )
			
				# Output simple (if width, height, depth or compression are different)
				if ( ( t1.width != t2.width ) or ( t1.height != t2.height ) or ( t1.depth != t2.depth ) or
					( t1.compression != t2.compression ) ) then
					fp_output.puts( diffs.last.to_csv() )
				end
			end
		rescue Exception => ex
			puts "\n\n\n"
			puts "Unhandled exception processing: #{line}"
			puts ex.backtrace.join("\n")
			puts "\n\n\n"
		end
	end	
	
	fp_output.close()
	fp_csv.close()
	diffs
end

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

   begin
        # Force output to stdout for console scripts.
        Pipeline::Config::instance()::logtostdout = true
        
        g_AppName = File::basename( __FILE__, '.rb' )
        g_Log = Log.new( g_AppName )
        g_Config = Pipeline::Config.instance( ) 
    
        #---------------------------------------------------------------------
        # Parse Command Line
        #---------------------------------------------------------------------
        opts, trailing = OS::Getopt.getopts( OPTIONS )
        if ( opts['help'] ) then
            puts OS::Getopt.usage( OPTIONS )
            exit( 1 )
        end
		g_Output = ( nil == opts['output'] ) ? nil : ::File::expand_path( opts['output'] )
		if ( g_Output.nil? ) then
			puts 'No output file specified.'
			puts OS::Getopt.usage( OPTIONS )
			exit( 2 )
		end
		g_Input = ( nil == opts['input'] ) ? nil : ::File::expand_path( opts['input'] )
		if ( g_Input.nil? ) then
			puts 'No GNU diff input file specified.'
			puts OS::Getopt.usage( OPTIONS )
			exit( 3 )
		end
		g_Csv = ( nil == opts['csv'] ) ? nil : ::File::expand_path( opts['csv'] )
		if ( g_Csv.nil? ) then
			puts 'No CSV output file specified.'
			puts OS::Getopt.usage( OPTIONS )
			exit( 4 )
		end
		
        #---------------------------------------------------------------------
        # Diff directories
        #---------------------------------------------------------------------
        diffs = diff_directories( g_Input, g_Output, g_Csv )
		
    rescue Exception => ex
        exit( ex.status ) if ( 'exit' == ex.message )
    
        puts "\n#{g_AppName} unhandled exception: #{ex.message}"
        puts ex.backtrace.join("\n\t")      
    end 
end

# data_compare_textures.rb
