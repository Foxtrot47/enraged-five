#
# File:: data_compare_textures.rb
# Description:: Compares two sets of textures highlighting differences.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 7 October 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/util/pack_stack_manager'
require 'pipeline/util/rage'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
    [ '--project', '-p', OS::Getopt::REQUIRED, 'project data to convert (e.g. jimmy, gta4_jpn).' ],
    [ '--branch', '-b', OS::Getopt::REQUIRED, 'project branch to convert (e.g. dev, dev_migrate).' ],
	[ '--input', '-i', OS::Getopt::REQUIRED, 'path to get independent files (optional, to restrict set).' ],
    [ '--output', '-o', OS::Getopt::REQUIRED, 'output path.' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
EXTENSIONS = [ 'img', 'idd', 'idr', 'ift', 'itd', 'rpf' ]
MOUNT_PACK = 'pack:/'

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

#
# Find and process all project independent files.
#
def process_project( project, branch, input, output, r )

	packstack = Util::PackStackManager.new( r, MOUNT_PACK )

	input_wildcard = OS::Path::combine( input, '*.*' )
	project_independent_files = OS::FindEx::find_files_recurse( input_wildcard )
	project_independent_files.each do |filename|
		process_file( project, branch, packstack, filename, output, r )
	end
end

#
# Process a single project file.  This determines the file type (on extension)
# and filters it into the corresponding process_file_* function.
#
def process_file( project, branch, packstack, filename, output, r )

	ext = OS::Path::get_extension( filename )
	case ext
	when 'img'
		process_file_pack( project, branch, packstack, filename, output, r )
		
	when 'idr', 'ift', 'itd', 'rpf'
		process_file_pack( project, branch, packstack, filename, output, r )
		
	end
end

#
# Mount our pack file and process its contents.  The PackStackManager allows
# us to do this recursively and handles all of the mounting/unmounting for us.
#
def process_file_pack( project, branch, packstack, filename, output, r )

	packs_dir = OS::Path::combine( output, 'packs' )
	FileUtils::mkdir_p( output ) unless ( ::File::directory?( output ) )
	FileUtils::mkdir_p( packs_dir ) unless ( ::File::directory?( packs_dir ) )
	
	packstack.push( filename )
	pack_files = r.util.find_files( OS::Path::combine( MOUNT_PACK, '*.*' ), false, true ).split( ',' )
	pack_files.each do |pack_filename|
		# Skip files that won't contain textures.
		ext = OS::Path::get_extension( pack_filename )
		next unless ( EXTENSIONS.include?( ext ) or ( 0 == ext.casecmp( 'dds' ) ) )
	
		if ( EXTENSIONS.include?( ext ) ) then	
			# Process files that might...
			dst = OS::Path::combine( packs_dir, OS::Path::get_filename( pack_filename ) )
			r.util.copy_file( pack_filename, dst )
			
			process_file( project, branch, packstack, dst, output, r )

		else
			# DDS
			textures_dir = OS::Path::combine( output, 'textures' )
			FileUtils::mkdir_p( textures_dir ) unless ( ::File::directory?( textures_dir ) )
	
			dst = OS::Path::combine( textures_dir, OS::Path::get_filename( pack_filename ) )
			r.util.copy_file( pack_filename, dst )
			
		end
	end	
	packstack.pop( )
end

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

   begin
        # Force output to stdout for console scripts.
        Pipeline::Config::instance()::logtostdout = true
        
        g_AppName = File::basename( __FILE__, '.rb' )
        g_Log = Log.new( g_AppName )
        g_ProjectName = ''
        g_Project = nil
        g_Branch = ''
        g_Config = Pipeline::Config.instance( ) 
    
        #---------------------------------------------------------------------
        # Parse Command Line
        #---------------------------------------------------------------------
        opts, trailing = OS::Getopt.getopts( OPTIONS )
        if ( opts['help'] ) then
            puts OS::Getopt.usage( OPTIONS )
            exit( 1 )
        end
		
        g_ProjectName = ( nil == opts['project'] ) ? '' : opts['project']
        project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
        if ( not project_exists ) then
            puts OS::Getopt.usage( OPTIONS )
            puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
            exit( 2 )
        end
        g_Project = g_Config.projects[ g_ProjectName ]
        if ( not g_Project.enabled ) then
            puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
            exit( 3 )
        end
        g_Project.load_config( )
        g_BranchName = ( nil == opts['branch'] ) ? g_Project.default_branch : opts['branch']
        branch_exists = ( g_Project.branches.has_key?( g_BranchName ) )
        if ( not branch_exists ) then
            puts OS::Getopt.usage( OPTIONS )
            puts "\nError project: #{g_ProjectName} does not have branch named #{g_BranchName} defined."
            exit( 4 )
        end
        g_Output = ( nil == opts['output'] ) ? nil : ::File::expand_path( opts['output'] )
        if ( g_Output.nil? ) then
            puts 'No output path specified.'
            puts OS::Getopt.usage( OPTIONS )
            exit( 5 )
        end
		g_Input = ( nil == opts['input'] ) ? g_Project.branches[g_BranchName].independent : ::File::expand_path( opts['input'] )
		
        #---------------------------------------------------------------------
        # Process project files
        #---------------------------------------------------------------------
        r = RageUtils::new( g_Project, g_BranchName )
		process_project( g_Project, g_BranchName, g_Input, g_Output, r )
        
    rescue Exception => ex
        exit( ex.status ) if ( 'exit' == ex.message )
    
        puts "\n#{g_AppName} unhandled exception: #{ex.message}"
        puts ex.backtrace.join("\n\t")      
    end 
end

# data_compare_textures.rb
