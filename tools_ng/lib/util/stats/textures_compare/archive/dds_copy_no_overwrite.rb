#
# File:: dds_copy.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 13 October 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/log/log'
require 'pipeline/os/getopt'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--output', '-i', OS::Getopt::REQUIRED, 'output directory for texture files.' ],
	[ '--input', '-i', OS::Getopt::REQUIRED, 'input directory to iterate.' ],
    [ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

   begin
        # Force output to stdout for console scripts.
        Pipeline::Config::instance()::logtostdout = true
        
        g_AppName = File::basename( __FILE__, '.rb' )
        g_Log = Log.new( g_AppName )
        g_Config = Pipeline::Config.instance( ) 
    
        #---------------------------------------------------------------------
        # Parse Command Line
        #---------------------------------------------------------------------
        opts, trailing = OS::Getopt.getopts( OPTIONS )
        if ( opts['help'] ) then
            puts OS::Getopt.usage( OPTIONS )
            exit( 1 )
        end
		g_OutputDir = ( nil == opts['output'] ) ? nil : ::File::expand_path( opts['output'] )
		if ( g_OutputDir.nil? ) then
			puts 'No output directory specified.'
			puts OS::Getopt.usage( OPTIONS )
			exit( 2 )
		end
		g_InputDir = ( nil == opts['input'] ) ? nil : ::File::expand_path( opts['input'] )
		if ( g_InputDir.nil? ) then
			puts 'No input directory specified.'
			puts OS::Getopt.usage( OPTIONS )
			exit( 3 )
		end
		
        #---------------------------------------------------------------------
        # Diff directories
        #---------------------------------------------------------------------
        skip = 0
		Dir::open( g_InputDir ) do |dir|
			dir.each do |filename|
				source = OS::Path::combine( g_InputDir, filename )
				target = OS::Path::combine( g_OutputDir, filename )
				
				skip += 1 if ( ::File::exists?( target ) )
				next if ( ::File::exists?( target ) )
				
				puts "Copying #{source} to #{target}"
				FileUtils::cp( source, target )
			end
		end
		puts "Skipped #{skip} files." 
		
    rescue Exception => ex
        exit( ex.status ) if ( 'exit' == ex.message )
    
        puts "\n#{g_AppName} unhandled exception: #{ex.message}"
        puts ex.backtrace.join("\n\t")      
    end 
end


# dds_copy.rb
