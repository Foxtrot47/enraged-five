#
# File:: common.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 8 October 2008
#

class TextureInfo
	attr_reader :filename
	attr_reader :width, :height, :depth
	attr_reader :compression
	
	def initialize( filename, w, h, d, comp )
		@filename = filename
		@width = w
		@height = h
		@depth = d
		@compression = comp
	end
	
	def to_csv( )
		"#{@filename},#{@width},#{@height},#{@depth},#{@compression}"
	end
end

class TextureDiff
	attr_reader :texture1
	attr_reader :texture2
	
	def initialize( t1, t2 )
		@texture1 = t1
		@texture2 = t2
	end
	
	def to_csv( )
		"#{@texture1.to_csv()},#{@texture2.to_csv()}"
	end
end

# common.rb
