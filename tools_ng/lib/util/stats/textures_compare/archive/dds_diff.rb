#
# File:: dds_diff.rb
# Description:: DDS RGBA image data compare.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 9 October 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/fileformats/dds_raw'
require 'pipeline/os/getopt'
require 'pipeline/util/diff'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--input', '-i', OS::Getopt::REQUIRED, 'GNU diff output file.' ],
	[ '--identical', '-a', OS::Getopt::REQUIRED, 'identical output file data.' ],
	[ '--main', '-b', OS::Getopt::REQUIRED, 'main image data differs output file data.' ],
	[ '--mipmap', '-c', OS::Getopt::REQUIRED, 'mipmap image data differs output file data.' ],
    [ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
FILES_DIFF_REGEXP = /^Files (.+) and (.+) differ$/
FILES_DDS_REGEXP  = /(.*) ([0-9]+) x ([0-9]+) x ([0-9]+) (.*)$/

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

def diff_dds_files( input, identical, main_differ, mipmap_differ )

	stdout = nil
	File.open( input ) do |fp|
		stdout = fp.readlines()
	end
	
	begin
		fp_identical = File::open( identical, 'w' )
		fp_differ = File::open( main_differ, 'w' )
		fp_mipmap = File::open( mipmap_differ, 'w' )
		
		stdout.each do |line|
			next if ( line.starts_with( 'Only in' ) )
			next unless ( line.starts_with( 'Files' ) )

			begin
				if ( line =~ FILES_DIFF_REGEXP ) then
					
					dds1 = FileFormats::DDSFileRaw::from_filename( $1 )
					dds2 = FileFormats::DDSFileRaw::from_filename( $2 )
					
					if ( dds1.mipimagedata.size != dds2.mipimagedata.size ) then
						fp_mipmap.puts "#{$1} and #{$2} have different number of mipmaps (#{dds1.header.dwMipMapCount}, #{dds2.header.dwMipMapCount})."
					
					elsif ( dds1.imagedata != dds2.imagedata ) then
						fp_differ.puts "#{$1} and #{$2} have different DDS main image data."
						
					else
						# Compare mipmaps
						differ = false
						dds1.mipimagedata.each_with_index do |dds1_mipdata, index|
							if ( dds1_mipdata != dds2.mipimagedata[index] ) then
								fp_mipmap.puts "#{$1} and #{$2} have different mip image data, mip level: #{index}."
								differ = true
							end
						end
					
						fp_identical.puts "#{$1} and #{$2} are identical." unless ( differ )
					end
				end
			rescue Exception => ex
				puts "\n\n\n"
				puts "Unhandled exception processing: #{ex.message} #{line}"
				puts ex.backtrace.join("\n")
				puts "\n\n\n"
			end
		end
	
	ensure
		fp_identical.close()
		fp_differ.close()
		fp_mipmap.close()
	end
end

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

   begin
        # Force output to stdout for console scripts.
        Pipeline::Config::instance()::logtostdout = true
        
        g_AppName = File::basename( __FILE__, '.rb' )
        g_Log = Log.new( g_AppName )
        g_Config = Pipeline::Config.instance( ) 
    
        #---------------------------------------------------------------------
        # Parse Command Line
        #---------------------------------------------------------------------
        opts, trailing = OS::Getopt.getopts( OPTIONS )
        if ( opts['help'] ) then
            puts OS::Getopt.usage( OPTIONS )
            exit( 1 )
        end
		g_Input = ( nil == opts['input'] ) ? nil : ::File::expand_path( opts['input'] )
		if ( g_Input.nil? ) then
			puts 'No GNU diff input file specified.'
			puts OS::Getopt.usage( OPTIONS )
			exit( 2 )
		end
		g_OutputIdentical = ( nil == opts['identical'] ) ? nil : ::File::expand_path( opts['identical'] )
		if ( g_OutputIdentical.nil? ) then
			puts 'No identical output file specified.'
			puts OS::Getopt.usage( OPTIONS )
			exit( 3 )
		end
		g_MainDiffer = ( nil == opts['main'] ) ? nil : ::File::expand_path( opts['main'] )
		if ( g_MainDiffer.nil? ) then
			puts 'No main output file specified.'
			puts OS::Getopt.usage( OPTIONS )
			exit( 4 )
		end
		g_MipmapDiffer = ( nil == opts['mipmap'] ) ? nil : ::File::expand_path( opts['mipmap'] )
		if ( g_MipmapDiffer.nil? ) then
			puts 'No mipmap output file specified.'
			puts OS::Getopt.usage( OPTIONS )
			exit( 5 )
		end
				
        #---------------------------------------------------------------------
        # Diff directories
        #---------------------------------------------------------------------
        diffs = diff_dds_files( g_Input, g_OutputIdentical, g_MainDiffer, g_MipmapDiffer )
		
    rescue Exception => ex
        exit( ex.status ) if ( 'exit' == ex.message )
    
        puts "\n#{g_AppName} unhandled exception: #{ex.message}"
        puts ex.backtrace.join("\n\t")      
    end 
end

=begin
dds1 = FileFormats::DDSFileRaw::from_filename( 'x:/gta4_e1_map_textures/textures/ab_brick03.dds' )
dds2 = FileFormats::DDSFileRaw::from_filename( 'x:/gta4_map_textures/textures/ab_brick03.dds' )

puts "DDS1 filesize:  #{dds1.filesize}"
puts "DDS2 filesize:  #{dds2.filesize}"
puts "DDS1 size:      #{dds1.imagedata.size}"
puts "DDS2 size:      #{dds2.imagedata.size}"
puts "Are different?: #{dds1.imagedata != dds2.imagedata ? 'yes' : 'no'}"

puts "Size:   #{dds.header.dwSize}"
puts "Width:  #{dds.header.dwWidth}"
puts "Height: #{dds.header.dwHeight}"
puts "Pitch:  #{dds.header.dwPitchOrLinearSize}"
puts "Depth:  #{dds.header.dwDepth}"
puts "Mips:   #{dds.header.dwMipMapCount}"
puts "RMask:  #{dds.header.ddpfPixelFormat.dwRBitMask}"
puts "RMask:  #{dds.header.ddpfPixelFormat.dwGBitMask}"
puts "RMask:  #{dds.header.ddpfPixelFormat.dwBBitMask}"
puts "AMask:  #{dds.header.ddpfPixelFormat.dwRGBAAlphaBitMask}"
puts "CC:     #{dds.header.ddpfPixelFormat.dwFourCC}"
puts "Caps1:  #{dds.header.ddsCaps.dwCaps1}"
puts "Caps2:  #{dds.header.ddsCaps.dwCaps2}"
puts "Data size: #{dds.imagedata.size}"
=end

# dds_diff.rb
