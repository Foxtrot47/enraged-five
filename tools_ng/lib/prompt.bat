@ECHO OFF

REM Detect Number of Arguments
SET ARGC=0
FOR %%x IN (%*) DO SET /A ARGC+=1

REM Detect CPU Architecture
IF /I "%PROCESSOR_ARCHITECTURE%"=="AMD64" ( 
  GOTO X64
) ELSE (
  GOTO X86
)

REM X64 Architecture Paths
:X64
	ECHO x64 detected, setting PATH and RUBYLIB appropriately.
  SET RUBYLIB=%RS_TOOLSLIB%
  SET RUBYOPT=rubygems
  
  GOTO GO

REM X86 Architecture Paths
:X86
	ECHO x86 detected, setting PATH and RUBYLIB appropriately.
  SET RUBYLIB=%RS_TOOLSLIB%
  SET RUBYOPT=rubygems

  GOTO GO

:GO
REM If we have no arguments then we invoke CMD.
IF "%ARGC%"=="0" (
  CALL CMD
  GOTO EOF
)
REM Otherwise we invoke CMD to invoke those arguments.
CALL CMD /C %*

:EOF
