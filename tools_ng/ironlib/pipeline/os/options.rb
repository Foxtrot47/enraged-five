#
# File:: %RS_TOOLSLIB%/pipeline/os/options.rb
# Description:: Command line parsing helper class; utilising the 
#               RSG.Pipeline.Services.CommandOptions class for common options.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 13 June 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Pipeline.Services.dll'
include RSG::Base::OS
include RSG::Pipeline::Services
require 'pipeline/monkey/array'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
module Pipeline
module OS

    #
    # == Description
    # Command-line options helper class; automatically parses command line
    # options and gives accessibility to them.
    #
    # Users can define recognised options for their script; see example below. 
    # 
    # == Example
    #    opt_input = LongOption::new( 'input', LongOption::ArgType.Required, 'i', 'Specify output filename.' )
    #    opt_output = LongOption::new( 'output', LongOption::ArgType.Required, 'o', 'Specify input filename.' )
    #    opt_options = [ opt_input, opt_output ]
    #    options = Options::new( opt_options )
    #
    class Options
        attr_reader :argc
        attr_reader :argv
        attr_reader :command_options
    
        # Constructor; initialise Options instance passing in a Ruby Array of
        # LongOption objects.
        def initialize( options )
            @argv = ARGV
            @argc = @argv.size
        
            argv_array = @argv.to_clr( System::String )
            options_array = options.to_clr( LongOption )
            
            @command_options = CommandOptions::new( argv_array, options_array )
        end
    
        # Return whether user has requested help usage information.
        def show_help?( )
            ( @command_options.ShowHelp )
        end
    
        # Return whether user has requested popups or not.
        def show_popups?( )
            ( not @command_options.NoPopups )
        end
    
        # Return whether we have an option defined on the command line.
        def has_option?( opt )
            ( @command_options.ContainsOption( opt ) )
        end
    
        # Return value of an option (or nil) on the command line.
        def get( opt )
            if ( has_option?( opt ) ) then
                ( @command_options[opt] )
            else
                nil
            end
        end
        
        # Return Array of Ruby String trailing arguments.
        def trailing( )
            @command_options.TrailingArguments.map { |t| t.to_s }
        end
        
        # Determine whether a bool property is enabled.
        def is_enabled?( opt )
            if ( has_option?( opt ) ) then
                ( true == @command_options[opt] )
            else
                false
            end
        end

        # Determine whether a bool property is disabled.
        def is_disabled?( opt )
            ( not is_enabled?( opts, opt ) )
        end
        
        # Return usage string.
        def usage( )
            ( @command_options.GetUsage() )
        end
    end

end # OS module
end # Pipeline module

# %RS_TOOLSLIB%/pipeline/os/options.rb
