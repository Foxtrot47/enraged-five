#
# File:: %RS_TOOLSLIB%/pipeline/monkey/hash.rb
# Description:: IronRuby/.Net Hash class monkey-patched methods.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 20 June 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'mscorlib'
require 'System.Core'
include System::Collections::Generic

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

class Hash

    # Return a .Net CLR Dictionary object.
    def to_clr( key_cast = System::Object, value_cast = System::Object )    
        
        d = System::Collections::Generic::Dictionary[key_cast, value_cast]::new()
        self.each_pair { |k,v| d[k] = v }
        d
    end
    
    # Return a Ruby Hash from a .Net CLR Dictionary object.
    def Hash::from_clr( d )
        h = Hash[*d.collect { |x| [x.key,x.value] }.flatten]
        h.each { |k,v| puts k,v }
        h
    end
end

# %RS_TOOLSLIB%/pipeline/monkey/hash.rb
