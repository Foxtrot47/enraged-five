#
# File:: %RS_TOOLSLIB%/pipeline/monkey/object.rb
# Description:: IronRuby/.Net Object class monkey-patched methods.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 30 July 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'mscorlib'

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

class Object

    # Return IEnumerable of type.
    def to_seq( type = Object )
        System::Linq::Enumerable.method(:of_type).of(type).call(self.to_a)
    end
end

# %RS_TOOLSLIB%/pipeline/monkey/array.rb
