#
# File:: %RS_TOOLSLIB%/pipeline/monkey/array.rb
# Description:: IronRuby/.Net Array class monkey-patched methods.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 20 June 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'mscorlib'
require 'System.Core'
include System::Collections::Generic

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

class Array

    # Return a .Net CLR Array object.
    def to_clr( cast = System::Object )    
        System::Array[cast]::new( self.map { |element| element } )
    end
    
    # Return a Ruby Array from a .Net CLR Array object.
    def Array::from_clr( a )
        ar = []
        a.each { |v| ar<<v }
        ar
    end
end

# %RS_TOOLSLIB%/pipeline/monkey/array.rb
