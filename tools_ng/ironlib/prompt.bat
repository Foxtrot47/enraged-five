@ECHO OFF

REM Detect Number of Arguments
SET ARGC=0
FOR %%x IN (%*) DO SET /A ARGC+=1

REM Detect CPU Architecture
IF /I "%PROCESSOR_ARCHITECTURE%"=="AMD64" ( 
  GOTO X64
) ELSE (
  GOTO X86
)

REM X64 Architecture Paths
:X64
	ECHO x64 detected, setting PATH and RUBYLIB appropriately.
  SET PATH=%PATH%;C:\Windows\system32;C:\Windows;C:\Windows\system32\wbem;c:\program files (x86)\perforce;c:\program files (x86)\xoreax\incredibuild;%RS_TOOLSROOT%\bin\ironruby\bin
  SET RUBYLIB=%RS_TOOLSROOT%\ironlib;%RS_TOOLSROOT%\ironlib\lib;C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0
  
  GOTO GO

REM X86 Architecture Paths
:X86
	ECHO x86 detected, setting PATH and RUBYLIB appropriately.
  SET PATH=%PATH%;C:\Windows\system32;C:\Windows;C:\Windows\system32\wbem;c:\program files\perforce;c:\program files\xoreax\incredibuild;%RS_TOOLSROOT%\bin\ironruby\bin
  SET RUBYLIB=%RS_TOOLSROOT%\ironlib;%RS_TOOLSROOT%\ironlib\lib;C:\Program Files\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0

  GOTO GO

:GO
REM If we have no arguments then we invoke CMD.
IF "%ARGC%"=="0" (
  CALL CMD
  GOTO EOF
)
REM Otherwise we invoke CMD to invoke those arguments.
CALL CMD /C %*

:EOF
