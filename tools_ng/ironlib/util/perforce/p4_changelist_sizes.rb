#
# File:: %RS_TOOLSLIB%/util/perforce/p4_changelist_sizes.rb
# Description:: Script to determine the client-sizes of files from a list of 
#               changelists.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 16 July 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::SourceControl::Perforce

require 'mscorlib'
require 'System.Core'
using_clr_extensions System::Linq
include System
include System::Collections::Generic
include System::IO

require 'pipeline/monkey/array'
require 'pipeline/monkey/object'
require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
    LongOption::new( 'port', LongOption::ArgType.Required, 'p', 'Perforce port (host:port syntax).' ),
    LongOption::new( 'kilobytes', LongOption::ArgType.None, 'k', 'Sizes reported in kilobytes (default: bytes).' ),
    LongOption::new( 'megabytes', LongOption::ArgType.None, 'm', 'Sizes reported in megabytes (default: bytes).' ),
    LongOption::new( 'gigabytes', LongOption::ArgType.None, 'g', 'Sizes reported in gigabytes (default: bytes).' ),
    LongOption::new( 'yesterday', LongOption::ArgType.None, 'y', 'Generate report for yesterday.' ),
    LongOption::new( 'today', LongOption::ArgType.None, 't', 'Generate report for today.' ),
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

# Return Changelist objects from a set of Change records.
def get_changelists_from_changes( log, p4, change_records )

    changelists = []
    change_records.each do |change_record|
        changelist = Changelist::Create( p4, change_record['change'].to_i )
        changelists << changelist[0]
    end
    
    changelists
end

# Return the size of the changelist.
def changelist_size( log, p4, cl )
        
    sum = 0
    cl.FileSizes.each do |size|
        sum += size
    end
    sum
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    g_Log = LogFactory.method(:CreateUniversal).of(UniversalLog).call( 'p4_changelist_sizes' )
    LogFactory.CreateApplicationConsoleLogTarget( )
        
    g_Options = OS::Options::new( OPTIONS )
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            puts "#{__FILE__}"
            puts "Usage:"
            puts g_Options.usage()
            exit( 1 )
        end
        
        # Perforce initialisation.
        g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )
        g_P4 = P4::new( )
        g_P4.Port = g_Options.get( 'port' ) unless g_Options.get( 'port' ).nil?
        g_P4.CallingProgram = __FILE__
        g_P4.Connect( )
        g_TotalSize = 0
        
        # Query Perforce for submitted changelists based on options.
        filespecs = []
        if ( g_Options.is_enabled?( 'yesterday' ) ) then
        
            today = System::DateTime::Today( )
            yesterday = today - System::TimeSpan::new( 1, 0, 0, 0 )
            g_Options.trailing.each do |path|
                filespecs << "#{path}/...@#{yesterday.ToString('yyyy/MM/dd')},@#{today.ToString('yyyy/MM/dd')}"
            end
        
        elsif ( g_Options.is_enabled?( 'today' ) ) then
            
            today = System::DateTime::Today( )
            tomorrow = today + System::TimeSpan::new( 1, 0, 0, 0 )
            g_Options.trailing.each do |path|
                filespecs << "#{path}/...@#{today.ToString('yyyy/MM/dd')},@#{tomorrow.ToString('yyyy/MM/dd')}"
            end
        else
            g_Options.trailing.each do |path|
                filespecs << path
            end
        end
        
        # Analyse each filespec path.
        filespecs.each do |path|
            
            change_records = g_P4.Run( 'changes', true, '-s', 'submitted', '-i', path )
            changes = get_changelists_from_changes( g_Log, g_P4, change_records )
            changes.each do |change|                
                g_TotalSize += changelist_size( g_Log, g_P4, change )
            end
            
            kb = (g_TotalSize / 1024.0)
            mb = (g_TotalSize / (1024.0 * 1024.0))
            gb = (g_TotalSize / (1024.0 * 1024.0 * 1024.0))
            
            g_Log.message( "Analysed #{changes.size} changelists." )
            g_Log.message( "Total data size: #{g_TotalSize} bytes." )
            g_Log.message( "Total data size: #{kb} kilobytes." ) \
                if ( g_Options.is_enabled?( 'kilobytes' ) )
            g_Log.message( "Total data size: #{mb} megabytes." ) \
                if ( g_Options.is_enabled?( 'megabytes' ) )
            g_Log.message( "Total data size: #{gb} gigabytes." ) \
                if ( g_Options.is_enabled?( 'gigabytes' ) )
        end
        
        # Disconnect.
        g_P4.Disconnect( )
    
    rescue SystemExit => ex
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during data_convert_file." )

        puts "Exception during data_convert_file: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        unless ( g_Options.is_enabled?( 'nopopups' ) ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/perforce/p4_changelist_sizes.rb
