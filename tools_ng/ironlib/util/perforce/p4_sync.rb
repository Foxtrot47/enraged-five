#
# File:: %RS_TOOLSLIB%/util/perforce/p4_sync.rb
# Description:: Script to perform a p4 sync operation for a particular set of files.
#
# Author:: Michal T�schler <michael.taschler@rockstarnorth.com>
# Date:: 04 April 2013
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::SourceControl::Perforce

require 'mscorlib'
require 'System.Core'
using_clr_extensions System::Linq
include System
include System::Collections::Generic
include System::IO

require 'pipeline/monkey/array'
require 'pipeline/monkey/object'
require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'

ULOG_VIEWER = '$(toolsbin)/UniversalLogViewer/UniversalLogViewer.exe'
ULOG_PATH = '$(toolsroot)/logs'

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
	LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.CreateUniversalLog( 'p4_sync' )
    
    begin
        g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )
        g_Branch = g_Config.Project.DefaultBranch
        
        # Perforce initialisation.
        g_P4 = P4::new( )
        g_P4.CallingProgram = __FILE__
        g_P4.Connect( )
        
        # Run the sync command.
        g_results = g_P4.Run( "sync", true, *ARGV )
        
        g_results.Errors.each do |error|
            g_Log.Error( error )
        end
        
        g_results.Warnings.each do |warning|
            g_Log.Warning( warning )
        end
        
        # Disconnect.
        g_P4.Disconnect( )
    
        if ( g_Log.HasErrors ) 
            command = "#{g_Branch.Environment.Subst(ULOG_VIEWER)} #{LogFactory.ApplicationLogFilename}"
            g_Log.Message( "Displaying Universal Logs: #{command}" )
            LogFactory.FlushApplicationLog()
            system( command )
        end
        
        exit( g_Log.HasErrors ? 1 : 0 )
    
    rescue SystemExit => ex
        LogFactory.ApplicationShutdown()
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during data_convert_file." )

        puts "Exception during p4_sync: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
            g_Config.EmailAddress, AUTHOR, EMAIL )
        dlg.ShowDialog( )
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/perforce/p4_sync.rb
