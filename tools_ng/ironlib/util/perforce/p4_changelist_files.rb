#
# File:: %RS_TOOLSLIB%/util/perforce/p4_changelist_files.rb
# Description:: Script to output changelist files.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 30 August 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::SourceControl::Perforce

require 'mscorlib'
require 'System.Core'
using_clr_extensions System::Linq
include System
include System::Collections::Generic
include System::IO

require 'pipeline/monkey/array'
require 'pipeline/monkey/object'
require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
    LongOption::new( 'status', LongOption::ArgType.Required, 's', 
        'Changelist status (pending, submitted).' ),
    LongOption::new( 'directory', LongOption::ArgType.None, 'p', 
        'Collapse by leaf directory.' )
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

# Return Changelist objects from a set of Change records.
def get_changelists_from_changes( log, p4, change_records )

    changelists = []
    change_records.each do |change_record|
        changelist = Changelist::Create( p4, change_record['change'].to_i )
        changelists << changelist[0]
    end
    
    changelists
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    g_Log = LogFactory.method(:CreateUniversal).of(UniversalLog).call( 'p4_changelist_sizes' )
    LogFactory.CreateApplicationConsoleLogTarget( )
        
    g_Options = OS::Options::new( OPTIONS )
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            puts "#{__FILE__}"
            puts "Usage:"
            puts g_Options.usage()
            exit( 1 )
        end
        
        # Perforce initialisation.
        g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )
        g_Status = g_Options.has_option?( 'status' ) ? g_Options.get( 'status' ) : 'submitted'
        g_P4 = P4::new( )
        g_P4.Port = g_Options.get( 'port' ) unless g_Options.get( 'port' ).nil?
        g_P4.CallingProgram = __FILE__
        g_P4.Connect( )
        
        g_DirectoryMode = g_Options.is_enabled?( 'directory' )
        g_Filespecs = g_Options.trailing
  
        if ( not g_DirectoryMode ) then

            # Analyse each filespec path.
            g_Filespecs.each do |path|
            
                change_records = g_P4.Run( 'changes', true, '-s', g_Status, '-i', path )
                changes = get_changelists_from_changes( g_Log, g_P4, change_records )
                changes.each do |change|

                  g_Log.message( "Changelist: #{change.Number} by #{change.Username}." )
                  change.Files.each do |filename|
                    g_Log.Message("\t#{filename}" )
                  end
               end
            end
        
        else
            # Directory mode; useful for ingame animation for example.
	        filelist = []
	        g_Filespecs.each do |path|

                change_records = g_P4.Run( 'changes', true, '-s', g_Status, '-i', path )
                changes = get_changelists_from_changes( g_Log, g_P4, change_records )
                changes.each do |change|
                
                    g_Log.message( "Changelist: #{change.Number} by #{change.Username}." )
		            change.Files.each do |filename|
			            index = filename.rindex( '/' )
			            filelist << filename[0..index-1]
		            end
                end
	        end
	        filelist.uniq!
	        filelist.sort!
	        filelist.each do |filename|
		        g_Log.Message( filename )
	        end
        end

        # Disconnect.
        g_P4.Disconnect( )
    
    rescue SystemExit => ex
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during data_convert_file." )

        puts "Exception during data_convert_file: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        unless ( g_Options.is_enabled?( 'nopopups' ) ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/perforce/p4_changelist_files.rb
