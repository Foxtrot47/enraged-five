@ECHO OFF
REM 
REM File:: %RS_TOOLSLIB%/util/perforce/p4_changelist_sizes_yesterday.bat
REM Description:: Generate changelist size report for yesterday.
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 16 July 2012
REM

CALL setenv.bat > NUL
CALL %RS_TOOLSROOT%/ironlib/prompt.bat %RS_TOOLSIRONLIB%\util\perforce\p4_changelist_sizes.rb --today --megabytes --gigabytes %1 %2 %3 %4

PAUSE

REM %RS_TOOLSLIB%/util/perforce/p4_changelist_sizes_yesterday.bat
