#
# File:: %RS_TOOLSLIB%/util/vfx/data_mk_rmptfx_ipt.rb
# Description:: Build platform-independent RMPTFX IPT file.
# 
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 18 October 2012 (AP3, IronRuby)
# Date:: 3 June 2008 (original)
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.Pipeline.Core.dll'
require 'RSG.Pipeline.Content.dll'
require 'RSG.Pipeline.Services.dll'
require 'RSG.SourceControl.Perforce.dll'
include RSG::Base::Collections
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::Pipeline::Services::Platform::Texture
include RSG::SourceControl::Perforce

require 'mscorlib'
require 'System.Core'
include System::Collections::Generic
include System::IO
using_clr_extensions System::Linq

require 'pipeline/monkey/object'
require 'pipeline/monkey/string'
require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

AUTHOR = 'RSGEDI Tools'
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [
]
CL_DESCRIPTION = 'Automatic VFX texture asset templates.'
DEFAULT_TEMPLATE = '${RS_ASSETS}/metadata/textures/templates/fx/Particle_Diffuse'
TCS_DIRECTORY = '$(metadata)/textures/vfx/rmptfx'
DDS_DIRECTORY = '$(art)/vfx/rmptfx/textures'

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

# Create texture TCS/TCL data as required.
def create_tcsdata( log, p4, texture_filename, cl, template, metadata_path )

    texture_basename = Path::GetFileNameWithoutExtension( texture_filename )
 
    tcsname = Path::ChangeExtension( texture_basename, ".tcs" )
	tcspath = Path::Combine( metadata_path, tcsname )
    tcsdir = Path::GetDirectoryName( tcspath )
        
    Directory::CreateDirectory( tcsdir ) unless ( Directory::Exists( tcsdir ) )
    
	source_textures = [].to_clr( System::String )
    if ( not System::IO::File::Exists( tcspath ) ) then
        log.Message( "Creating TCS: #{tcspath}" )
		p4.Run( "add", false, tcspath )
        p4.Run( "reopen", false, ['-c', cl.to_s, tcspath].to_clr(System::String) )
		SpecificationFactory::Create( tcspath, template, source_textures, texture_filename, false )
	end
end

# Clean up empty changelists with a particular description.
def cleanup_changelists( log, p4, config )
    changes = p4.Run( 'changes', false, ['-L', '-u', config.Username, '-s', 'pending', '-c', p4.Client].to_clr(System::String) )
    changes.each do |change|
        next unless ( change['desc'].include?( CL_DESCRIPTION ) )
        
        ## log.Warning( "Removing empty changelist #{change['change']}." )
        p4.Run( "change", false, ['-d', change['change'].to_s].to_clr(System::String) )
    end
end

# Print usage information to stdout.
def usage( options )
	puts "#{__FILE__}"
	puts "Usage:"
	puts options.usage()
	exit( 1 )
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

	# Initialise log and console output.
  LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog;
		
	g_Options = OS::Options::new( OPTIONS )
	begin
		if ( g_Options.show_help? )
			usage( g_Options )
		end
	
		g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )
		g_BranchName = g_Options.has_option?( 'branch' ) ? g_Options.get( 'branch' ) : g_Config.Project.DefaultBranchName
		g_Branch = g_Config.Project.Branches[g_BranchName] if ( g_Config.Project.Branches.ContainsKey( g_BranchName ) )
		if ( g_Branch.nil? ) then
			g_Log.Error( "Invalid branch '#{g_BranchName}' for project '#{g_Config.Project.FriendlyName}'." )
			exit( 1 )
		end
 
    g_P4 = g_Config.Project.SCMConnect()
    g_Changelist = g_P4.CreatePendingChangelist( CL_DESCRIPTION )
    g_ChangelistNumber = g_Changelist.Number

    # Loop through all DDS files in the source texture tree; updating
    # TCS files as required.
    g_MetadataDir = g_Branch.Environment.Subst( TCS_DIRECTORY )
    g_TextureDir = g_Branch.Environment.Subst( DDS_DIRECTORY )
    g_TextureFiles = Directory::GetFiles( g_TextureDir, '*.dds' )
    g_TextureFiles.each do |texture_filename|
        create_tcsdata( g_Log, g_P4, texture_filename, g_ChangelistNumber, DEFAULT_TEMPLATE, g_MetadataDir )
    end
    
    # Perforce revert unchanged; delete changelist if empty.
    g_P4.Run( "revert", false, ['-a', '-c', g_ChangelistNumber.to_s].to_clr(System::String) )
    cleanup_changelists( g_Log, g_P4, g_Config )
    g_Log.Message( "Done." )
    g_P4.Disconnect( )
  
	rescue SystemExit => ex
		exit( ex.status )    

	rescue Exception => ex

		g_Log.Exception( ex, "Exception during #{__FILE__}." )

		puts "Exception during #{__FILE__}: #{ex.message}"
		puts ex.backtrace.join("\n")
		
		unless ( g_Options.is_enabled?( 'nopopups' ) ) then
			dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
				g_Config.EmailAddress, AUTHOR, EMAIL )
			dlg.ShowDialog( )
		end
		
		exit( 1 )
	end
end

# data_mk_rmptfx_ipt.rb
