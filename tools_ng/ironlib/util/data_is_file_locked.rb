#
# File:: %RS_TOOLSLIB%/util/data_is_file_locked.rb
# Description:: Determine if a file is locked; setting return code.
#                 0 - no files listed are locked
#                 1 - at least one file listed is locked
#                 2 - argument error
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 31 August 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'mscorlib'
require 'System.Core'
include System::IO
include RSG::Base::Logging
include RSG::Base::OS

using_clr_extensions RSG::Base::Extensions

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
    begin
        exit( 2 ) if ( 0 == ARGV.size )
    
        is_locked = false
        ARGV.each do |filename|
            
            begin
                fp = System::IO::File::Open( filename, FileMode.Open, 
                    FileAccess.ReadWrite, FileShare.None )
                fp.Close()
            
            rescue IOException => ex            
                is_locked = ex.IsFileLocked()
            end
        end
        exit( is_locked ? 1 : 0 )
    
    rescue SystemExit => ex
        exit( ex.status )
    
    rescue Exception => ex
        ConsoleLog::new( )
        Log::Log__Error( "Unhandled error." )
        Log::Log__Error( ex.backtrace.join("\n" ) )
    end
end

# %RS_TOOLSLIB%/util/data_is_file_locked.rb
