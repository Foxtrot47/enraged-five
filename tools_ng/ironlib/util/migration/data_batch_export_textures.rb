#
# File:: %RS_TOOLSLIB%/util/migration/data_batch_export_textures.rb
# Description:: Batch texture export script; using the Texture Exporter Processor.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 19 February 2013
#
# Usage:
#   %RS_TOOLSIR% data_batch_export_textures.rb [--force] --output <outdir> <input_files>
#
#   <outdir>      - Output directory for exported textures.
#   <input_files> - SceneXml filenames to load for source textures.
#   force         - Force texture export.
#
# Example:
#   
#   %RS_TOOLSIR% data_batch_export_textures.rb 
#     --output x:\gta5\assets\maps\textures
#     x:\gta5\assets\export\levels\gta5\_citye\downtown_01\*.xml
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.Pipeline.Core.dll'
require 'RSG.Pipeline.Content.dll'
require 'RSG.Pipeline.Engine.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::Pipeline::Core
include RSG::Pipeline::Content
include RSG::Pipeline::Engine

require 'mscorlib'
require 'System.Core'
include System::Collections::Generic
include System::IO
using_clr_extensions System::Linq

require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
    LongOption::new( 'output', LongOption::ArgType.Required, 'o', 
        'Specify output directory for textures.' ),
    LongOption::new( 'force', LongOption::ArgType.None, 'r',
        'Force texture export.' )    
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
def usage( options )
    puts "#{__FILE__}"
    puts "Usage:"
    puts options.usage()
    exit( 1 )
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog
    g_Options = OS::Options::new( OPTIONS )
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            usage( g_Options )
        end
    
        g_Config = g_Options.command_options.Config
        g_Project = g_Options.command_options.Project
        g_BranchName = g_Options.has_option?( 'branch' ) ? g_Options.get( 'branch' ) : g_Project.DefaultBranchName
        g_Branch = g_Project.Branches[g_BranchName] if ( g_Project.Branches.ContainsKey( g_BranchName ) )
        if ( g_Branch.nil? ) then
            g_Log.Error( "Invalid branch '#{g_BranchName}' for project '#{g_Project.UIName}'." )
            exit( 1 )
        end
        
        g_Output = g_Options.has_option?( 'output' ) ? g_Options.get('output') : nil
            
        if ( g_Output.nil? ) then
            g_Log.Error( "No output directory specified.  Aborting." )
            usage( g_Options )
        end
        
        if ( not System::IO::Directory::Exists( g_Output ) ) then
            System::IO::Directory::CreateDirectory( g_Output )
        end
        
        # Engine initialisation.
        g_EngineFlags = EngineFlags.Default
        g_EngineFlags |= EngineFlags.Rebuild if ( g_Options.is_enabled?( 'force' ) )
        g_InputScene = g_Options.get( 'input' )
        g_OutputDirectory = g_Options.get( 'output' )
             
        g_Engine = Engine::new( g_Options.command_options, g_EngineFlags )
        g_BuildTime = nil
    
        g_Tree = Factory::CreateEmptyTree( g_Branch )
        
        # Loop through our SceneXml input files.
        g_BuildInput = BuildInput::new( g_Branch )
        g_SceneFiles = []
        g_Options.trailing.each do |input|
        
            if ( File::exists?( input ) ) then
                g_SceneFiles << input
            else
                # globpath doesn't handle '\'! 
                globpath = input.gsub( '\\', '/' )
                Dir.glob( globpath ).each do |i|
                    g_SceneFiles << i
                end
            end
        end
        # Process scenes.
        g_SceneFiles.each do |input|
        
            pb = ProcessBuilder::new( "RSG.Pipeline.Processor.Texture.TextureExporterProcessor",
            g_Engine.Processors, g_Tree )
        
            pb.Inputs.Add( g_Tree.CreateFile( input, false ) )
            pb.Outputs.Add( g_Tree.CreateDirectory( g_Output ) )
    
            g_BuildInput.RawProcesses.Add( pb.ToProcess() )
        end
    
        g_Outputs = g_Engine.Build( g_BuildInput )
       
        g_BuildTime = g_Outputs[3]
        g_Log.Message( "Texture Export Took: #{g_BuildTime}" )
        exit( g_Outputs[0] ? 0 : 1 )
    
    rescue SystemExit => ex
    
        LogFactory.ApplicationShutdown()
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/migration/data_batch_export_textures.rb
