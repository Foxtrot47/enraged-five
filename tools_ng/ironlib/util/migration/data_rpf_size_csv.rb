#
# File:: %RS_TOOLSLIB%/util/migration/data_rpf_size_csv.rb
# Description:: Generate CSV of RPF file sizes for a particular Perforce filespec.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 4 March 2013
#
# Usage:
#   %RS_TOOLSIR% data_rpf_size_csv.rb <filespec>
#
#   <filespec>   - Perforce filespec.
#
# Example:
#
#   %RS_TOOLSIR% data_rpf_size_csv.rb //depot/gta5/build/dev/....rpf@GTA5_[ALL]_version_304.1
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.Pipeline.Core.dll'
require 'RSG.Pipeline.Content.dll'
require 'RSG.Pipeline.Services.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::Pipeline::Core
include RSG::Pipeline::Content

require 'mscorlib'
require 'System.Core'
include System::Collections::Generic
include System::IO
using_clr_extensions System::Linq

require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
def usage( options )
    puts "#{__FILE__}"
    puts "Usage:"
    puts options.usage()
    exit( 1 )
end

def process( log, p4, input )

    file_stats = {}

    log.Message( "Processing: #{input}" )
    result = p4.run( 'fstat', false, '-Ol',  '-T', 'fileSize,clientFile', input )
    if ( result.HasErrors() ) then
        log.Error( "Perforce errors returned: " )
        result.Errors.each do |error|
            log.Error( error )
        end
    else
    
        result.Records.each do |record|        
            filename = record['clientFile']
            filesize = record['fileSize']
            basename = Path::GetFileNameWithoutExtension( filename )
            
            file_stats[basename] = {} unless ( file_stats.has_key?( basename ) )
            
            if ( filename.include?( 'ps3' ) ) then
                file_stats[basename][:ps3] = filesize
            elsif ( filename.include?( 'xbox360' ) ) then
                file_stats[basename][:xbox360] = filesize
            elsif ( filename.include?( 'pc' ) ) then
                file_stats[basename][:pc] = filesize
            elsif ( filename.include?( 'x64' ) ) then
                file_stats[basename][:x64] = filesize
            end            
        end
    end
    
    puts "File,PS3,Xbox360"
    file_stats.each_pair do |k,v|
        puts "#{k},#{v[:ps3]},#{v[:xbox360]}"
    end
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog
    g_Options = OS::Options::new( OPTIONS )
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            usage( g_Options )
        end
    
        g_Config = g_Options.command_options.Config
        g_Project = g_Options.command_options.Project
        g_BranchName = g_Options.has_option?( 'branch' ) ? g_Options.get( 'branch' ) : g_Project.DefaultBranchName
        g_Branch = g_Project.Branches[g_BranchName] if ( g_Project.Branches.ContainsKey( g_BranchName ) )
        if ( g_Branch.nil? ) then
            g_Log.Error( "Invalid branch '#{g_BranchName}' for project '#{g_Project.UIName}'." )
            exit( 1 )
        end
                
        begin
            p4 = g_Project.SCMConnect( )
            g_Options.trailing.each do |input|
                process( g_Log, p4, input )
            end
        ensure
            p4.Disconnect( )
        end
    
        g_Log.Message( "Done." )
    
    rescue SystemExit => ex
    
        LogFactory.ApplicationShutdown()
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/migration/data_batch_export_textures.rb
