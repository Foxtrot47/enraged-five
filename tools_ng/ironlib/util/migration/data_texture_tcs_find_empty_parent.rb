#
# File:: %RS_TOOLSLIB%/util/migration/data_texture_tcs_find_empty_parent.rb
# Description:: Find empty 'parent' tags in TCS files.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 19 February 2013
#
# Usage:
#   %RS_TOOLSIR% data_texture_tcs_find_empty_parent.rb <input_path>
#
#   <input_path>  - input directory to scan for TCS files with empty parents.
#
# Example:
#   
#   %RS_TOOLSIR% data_texture_tcs_find_empty_parent.rb 
#     x:\gta5\assets\metadata\textures\maps
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.Pipeline.Core.dll'
require 'RSG.Pipeline.Content.dll'
require 'RSG.Pipeline.Engine.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::Pipeline::Core
include RSG::Pipeline::Content
include RSG::Pipeline::Engine

require 'mscorlib'
require 'System.Core'
require 'System.Xml'
require 'System.Xml.Linq'
include System::Collections::Generic
include System::IO
include System::Xml::Linq
include System::Xml::XPath
using_clr_extensions System::Linq
using_clr_extensions System::Xml::Linq
using_clr_extensions System::Xml::XPath

require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
def usage( options )
    puts "#{__FILE__}"
    puts "Usage:"
    puts options.usage()
    exit( 1 )
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog
    g_Options = OS::Options::new( OPTIONS )
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            usage( g_Options )
        end
    
        g_Config = g_Options.command_options.Config
        g_Project = g_Options.command_options.Project
        g_BranchName = g_Options.has_option?( 'branch' ) ? g_Options.get( 'branch' ) : g_Project.DefaultBranchName
        g_Branch = g_Project.Branches[g_BranchName] if ( g_Project.Branches.ContainsKey( g_BranchName ) )
        if ( g_Branch.nil? ) then
            g_Log.Error( "Invalid branch '#{g_BranchName}' for project '#{g_Project.UIName}'." )
            exit( 1 )
        end
        
        g_Directories = []
        g_Options.trailing.each do |input|
        
            if ( File::exists?( input ) ) then
                g_Directories << input
            else
                # globpath doesn't handle '\'! 
                globpath = input.gsub( '\\', '/' )
                Dir.glob( globpath ).each do |i|
                    g_Directories << i
                end
            end
        end
        # Process scenes.
        tcs_files_scanned = 0
        tcs_files_empty_parent = 0
        g_Directories.each do |input|
        
            tcs_files = Directory::GetFiles( input, "*.tcs", System::IO::SearchOption.AllDirectories )
            g_Log.Message( "Processing: #{input} [#{tcs_files.Length}]" )
            tcs_files.each do |tcs_input|
            
                begin
                    tcsxml = XDocument::Load( tcs_input )
                    parent_elem = tcsxml.Root.XPathSelectElement( 'parent' )
                
                    if ( nil != parent_elem ) then
                        if ( parent_elem.Value.Length == 0 ) then
                            g_Log.Error( "Template: #{tcs_input} has empty parent!" ) 
                            tcs_files_empty_parent += 1
                        end
                    else
                        g_Log.Error( "Template: #{tcs_input} has no parent element!" )
                        tcs_files_empty_parent += 1
                    end
                rescue 
                    g_Log.Error( "Template: #{tcs_input} is invalid XML." )
                    tcs_files_empty_parent += 1
                end
                tcs_files_scanned += 1
            end
        end
        
        g_Log.Message( "Processed #{tcs_files_scanned}; #{tcs_files_empty_parent} have empty parents." )
    
    rescue SystemExit => ex
    
        LogFactory.ApplicationShutdown()
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/migration/data_texture_tcs_find_empty_parent.rb
