#
# File:: %RS_TOOLSLIB%/util/migration/data_batch_update_texture_tcls.rb
# Description:: Batch update packed TCLs in map zips.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 21 February 2013
#
# Usage:
#   %RS_TOOLSIR% data_batch_update_texture_tcls.rb --from <from> --to <to> <input_files>
#
#   <from>        - TCS old path (e.g. x:/gta5/assets/metadata/maps_half/)
#   <to>          - TCS new path (e.g. x:/gta5/assets/metadata/maps/)
#   <input_files> - List of zip files to recursively fix up.
#
# Example: (migrate downtown_01 data from 'maps_half' to 'maps')
#
#   %RS_TOOLSIR% data_batch_update_texture_tcls.rb 
#     --from ${RS_ASSETS}/metadata/textures/maps_half 
#     --to ${RS_ASSETS}/metadata/textures/maps 
#     x:\gta5\assets\export\levels\gta5\_citye\downtown_01\*.zip
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.Pipeline.Core.dll'
require 'RSG.Pipeline.Content.dll'
require 'RSG.Pipeline.Services.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::Pipeline::Core
include RSG::Pipeline::Content
include RSG::Pipeline::Services
include RSG::Pipeline::Services::Platform::Texture

require 'mscorlib'
require 'System.Core'
include System::Collections::Generic
include System::IO
using_clr_extensions System::Linq

require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
    LongOption::new( 'from', LongOption::ArgType.Required, 's', 
        'TCS old path (e.g. x:/gta5/assets/metadata/maps_half/).' ),
    LongOption::new( 'to', LongOption::ArgType.Required, 't',
        'TCS new path (e.g. x:/gta5/assets/metadata/maps/).' )
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
def usage( options )
    puts "#{__FILE__}"
    puts "Usage:"
    puts options.usage()
    exit( 1 )
end

# Process a top-level zip; migrate all TCL parent's from -> to prefix paths.
def process_zip( log, project, filename, from, to )
    destination = Path::Combine( project.Config.ToolsTemp, 'texture_migrate', \
        Path::GetFileNameWithoutExtension( filename ) )
    log.Message( "Processing #{filename} (#{destination})" )
    
    # Horrible hack because we have overloaded methods.
    result = Zip::GetFileList( filename )
    
    if ( not Zip::ExtractAll( filename, destination, true, nil ) ) then
        log.Error( "Failed to read zip: #{filename}.  Ignoring." )
        return
    end    
        
    zip_file_contents = result[1]
    zip_file_contents.each do |asset_filename|
        ext = Path::GetExtension( asset_filename )
        next unless ( '.zip' == ext )
    
        full_asset_filename = Path::Combine( destination, asset_filename )
        result = Zip::GetFileList( full_asset_filename )
        if ( not result[0] ) then
            log.Error( "Failed to read zip: #{full_asset_filename}.  Ignoring." )
            next
        end
        asset_files = result[1]
        tcl_files = asset_files.Where( lambda { |f| ".tcl" == Path::GetExtension( f ) } )
        next unless ( tcl_files.Count() > 0 )
        
        log.Message( "\tProcessing #{asset_filename} (#{tcl_files.Count()} TCL files)." )
        asset_destination = Path::Combine( destination, "EXTRACT_#{asset_filename}" )
        if ( not Zip::ExtractAll( full_asset_filename, asset_destination, true, nil ) ) then
            log.Error( "Failed to read zip: #{full_asset_filename}.  Ignoring." )
            next
        end
        
        asset_files.each do |asset_file|
            ext = Path::GetExtension( asset_file )    
            next unless ( '.tcl' == ext )
            
            full_asset_file_path = Path::Combine( asset_destination, asset_file )
            old_parent = SpecificationFactory::ReadParent( full_asset_file_path )
            new_parent = old_parent.Replace( from, to )
            SpecificationFactory::CreateLink( full_asset_file_path, new_parent )
            
            log.Message( "\t\tMigrating #{asset_file} to #{new_parent.Replace('{', '{{').Replace('}','}}')}." )
        end
        
        # Repackage up zip; TCLs edited inline.
        full_asset_files = Array::from_clr( asset_files.Select( lambda { |f| Path::Combine( asset_destination, f ) } ) ).to_clr( System::String )
        System::IO::File::Delete( full_asset_filename )
        if ( not Zip::Create( full_asset_filename, full_asset_files ) ) then
            log.Error( "\tFailed to re-construct zip: #{asset_filename}." )
            next
        end
    end
    
    # Repackage up main zip.
    full_zip_file_contents = Array::from_clr( zip_file_contents.Select( lambda { |f| Path::Combine( destination, f ) } ) ).to_clr( System::String )
    System::IO::File::Delete( filename )
    if ( not Zip::Create( filename, full_zip_file_contents ) ) then
        log.Error( "Failed to re-construct zip: #{filename}." )
    end
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog
    g_Options = OS::Options::new( OPTIONS )
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            usage( g_Options )
        end
    
        g_Config = g_Options.command_options.Config
        g_Project = g_Options.command_options.Project
        g_BranchName = g_Options.has_option?( 'branch' ) ? g_Options.get( 'branch' ) : g_Project.DefaultBranchName
        g_Branch = g_Project.Branches[g_BranchName] if ( g_Project.Branches.ContainsKey( g_BranchName ) )
        if ( g_Branch.nil? ) then
            g_Log.Error( "Invalid branch '#{g_BranchName}' for project '#{g_Project.UIName}'." )
            exit( 1 )
        end
        
        g_From = g_Options.has_option?( 'from' ) ? g_Options.get('from') : nil
        g_To = g_Options.has_option?( 'to' ) ? g_Options.get('to') : nil
        
        # Loop through each of our input files.
        g_ZipFiles = []
        g_Options.trailing.each do |input|
        
            if ( File::exists?( input ) ) then
                g_ZipFiles << input
            else
                # globpath doesn't handle '\'! 
                globpath = input.gsub( '\\', '/' )
                Dir.glob( globpath ).each do |i|
                    g_ZipFiles << i
                end
            end
        end
        # Process each zip file.
        g_ZipFiles.each do |input|
            process_zip( g_Log, g_Project, input, g_From, g_To )
        end        
    
    rescue SystemExit => ex
    
        LogFactory.ApplicationShutdown()
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/migration/data_batch_update_texture_tcls.rb
