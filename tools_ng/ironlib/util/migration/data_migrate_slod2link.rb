#
# File:: %RS_TOOLSLIB%/util/migration/data_migrate_slod2link.rb
# Description:: Migrate SLOD2 link attribute from old tree to new tree.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 8 July 2013
#
# Usage:
#   %RS_TOOLSIR% data_migrate_slod2link.rb
#
# Example:
#   
#   %RS_TOOLSIR% data_migrate_slod2link.rb 
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS

require 'mscorlib'
require 'System.Core'
require 'System.Xml'
require 'System.Xml.Linq'
include System::Collections::Generic
include System::IO
include System::Xml
include System::Xml::Linq
using_clr_extensions System::Linq
using_clr_extensions System::Xml::Linq
using_clr_extensions System::Xml::XPath

require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
    LongOption::new( 'output', LongOption::ArgType.Required, 'o', 
        'Specify output directory for textures.' ),
    LongOption::new( 'force', LongOption::ArgType.None, 'r',
        'Force texture export.' )    
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
def usage( options )
    puts "#{__FILE__}"
    puts "Usage:"
    puts options.usage()
    exit( 1 )
end

def set_slod2link( xmldoc, slod2links )

    xmldoc.Root.XPathSelectElements( '//node' ).each do |node|
        next unless ( node.XPathSelectElement( 'data[@key="path"]' ) )
    
        if ( node.XPathSelectElement( 'data[@key="slod2link"]' ) )
            puts "Skipping; already has SLOD2 Link set." 
            next
        end
    
        pathname = node.XPathSelectElement( 'data[@key="path"]' )
        name = Path::GetFileNameWithoutExtension( pathname ).ToLower()
        
        next unless ( slod2links.has_key?( name ) )
        
        puts "Set SLOD2 Link: #{name} => #{slod2links[name]}."
        linkElem = XElement::new( XName::Get( 'data' ),
            XAttribute::new( XName::Get( 'key' ), 'slod2link' ),
            slod2links[name]
        )
        node.Add( linkElem )
    end
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog
    g_Options = OS::Options::new( OPTIONS )
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            usage( g_Options )
        end
    
        g_Config = g_Options.command_options.Config
        g_Project = g_Options.command_options.Project
        g_BranchName = g_Options.has_option?( 'branch' ) ? g_Options.get( 'branch' ) : g_Project.DefaultBranchName
        g_Branch = g_Project.Branches[g_BranchName] if ( g_Project.Branches.ContainsKey( g_BranchName ) )
        if ( g_Branch.nil? ) then
            g_Log.Error( "Invalid branch '#{g_BranchName}' for project '#{g_Project.UIName}'." )
            exit( 1 )
        end
        
        maps_xml = Path::Combine( g_Config.ToolsConfig, "content", "maps.xml" )
        xmldoc = XDocument::Load( maps_xml )
        
        citye_doc = XDocument::Load( 'x:\\gta5\\tools_ng\\etc\\content\\content_level_gta5_citye.xml' )
        cityw_doc = XDocument::Load( 'x:\\gta5\\tools_ng\\etc\\content\\content_level_gta5_cityw.xml' )
        hills_doc = XDocument::Load( 'x:\\gta5\\tools_ng\\etc\\content\\content_level_gta5_hills.xml' )
        
        slod2links = {}
        xmldoc.Root.XPathSelectElements( '//content' ).each do |content|
            next unless ( content.Attribute( XName::Get( 'type' ) ) and 'map' == content.Attribute( XName::Get( 'type' ) ).Value )
            
            # Have a raw map node; see if it has a SLOD2 Link Attribute.
            next if ( content.Attribute( XName::Get( 'slod2link' ) ).nil? )
            
            # Find AP3 node; and add the SLOD2 link parameter if not already there.
            mapname = content.Attribute( XName::Get( 'name' ) ).Value.ToLower()
            hasSLOD2Link = ( 'true' == content.Attribute( XName::Get( 'slod2link' ) ).Value )
            slod2links[mapname] = hasSLOD2Link
        end
        
        set_slod2link( citye_doc, slod2links )
        set_slod2link( cityw_doc, slod2links )
        set_slod2link( hills_doc, slod2links )
        
        citye_doc.Save( 'x:\\gta5\\tools_ng\\etc\\content\\content_level_gta5_citye.xml' )
        cityw_doc.Save( 'x:\\gta5\\tools_ng\\etc\\content\\content_level_gta5_cityw.xml' )
        hills_doc.Save( 'x:\\gta5\\tools_ng\\etc\\content\\content_level_gta5_hills.xml' )
        
        
    rescue SystemExit => ex
    
        LogFactory.ApplicationShutdown()
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/migration/data_batch_export_textures.rb
