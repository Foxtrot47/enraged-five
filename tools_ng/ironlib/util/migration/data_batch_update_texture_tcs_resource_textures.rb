#
# File:: %RS_TOOLSLIB%/util/migration/data_batch_update_texture_tcs_resource_textures.rb
# Description:: Batch texture TCS update script; populates resourceTextures
#               if it doesn't exist.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 21 February 2013
#
# Usage:
#   %RS_TOOLSIR% data_batch_update_texture_tcs_resource_textures.rb --tcs <tcsdir> --textures <texturedir> --extension <ext>
#
#   <tcs>        - Root directory of TCS directory.
#   <texturedir> - Root directory of resource texture directory.
#   <ext>        - Resource texture extension (e.g. .dds, .tif).
#
# Example:
#
#   %RS_TOOLSIR% data_batch_update_texture_tcs_resource_textures.rb 
#      --tcs x:\gta5\assets\metadata\textures\pedshared\component\comp_peds_marine 
#      --textures x:\gta5\assets\characters\textures\shared\component\comp_peds_marine 
#      --extension .dds
#
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.Pipeline.Core.dll'
require 'RSG.Pipeline.Content.dll'
require 'RSG.Pipeline.Services.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::Pipeline::Core
include RSG::Pipeline::Content
include RSG::Pipeline::Services
include RSG::Pipeline::Services::Platform::Texture

require 'mscorlib'
require 'System.Core'
include System::Collections::Generic
include System::IO
using_clr_extensions System::Linq

require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
    LongOption::new( 'tcs', LongOption::ArgType.Required, 's', 
        'Root directory of TCS directory.' ),
    LongOption::new( 'textures', LongOption::ArgType.Required, 't',
        'Root directory of resource texture directory.' ),
    LongOption::new( 'extension', LongOption::ArgType.Required, 'e',
        'Resource texture file extension (.tif, .dds).' ) 
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
def usage( options )
    puts "#{__FILE__}"
    puts "Usage:"
    puts options.usage()
    exit( 1 )
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog
    g_Options = OS::Options::new( OPTIONS )
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            usage( g_Options )
        end
    
        g_Config = g_Options.command_options.Config
        g_Project = g_Options.command_options.Project
        g_BranchName = g_Options.has_option?( 'branch' ) ? g_Options.get( 'branch' ) : g_Project.DefaultBranchName
        g_Branch = g_Project.Branches[g_BranchName] if ( g_Project.Branches.ContainsKey( g_BranchName ) )
        if ( g_Branch.nil? ) then
            g_Log.Error( "Invalid branch '#{g_BranchName}' for project '#{g_Project.UIName}'." )
            exit( 1 )
        end
        
        g_TCS = g_Options.has_option?( 'tcs' ) ? g_Options.get('tcs') : nil
        g_TextureDirectory = g_Options.has_option?( 'textures' ) ? g_Options.get('textures') : nil
        g_Extension = g_Options.has_option?( 'extension' ) ? g_Options.get( 'extension' ) : '.dds'
        
        g_Log.Message( "TCS Directory:     #{g_TCS}" )
        g_Log.Message( "Texture Directory: #{g_TextureDirectory}" )
        
        if ( g_TCS.nil? ) then
            g_Log.Error( "No TCS directory specified.  Aborting." )
            usage( g_Options )
        end
        if ( not System::IO::Directory::Exists( g_TCS ) ) then
            g_Log.Error( "TCS directory does not exist.  Aborting." )
            usage( g_Options )
        end
        if ( g_TextureDirectory.nil? ) then
            g_Log.Error( "Resource texture directory specified.  Aborting." )
            usage( g_Options )
        end
        if ( not System::IO::Directory::Exists( g_TextureDirectory ) ) then
            g_Log.Error( "Resource texture directory does not exist.  Aborting." )
            usage( g_Options )
        end
        
        total = 0
        processed = 0
        skipped = 0
        tcs_files = Directory::GetFiles( g_TCS, "*.tcs" )
        begin
            p4 = g_Project.SCMConnect( )
        
            tcs_files.each do |tcs_filename|
        
                total += 1
                next if ( SpecificationFactory::HasResourceTexturesData( tcs_filename ) )
            
                processed += 1
                g_Log.Message( "Processing: #{tcs_filename}" )
            
                basename = Path::GetFileNameWithoutExtension( tcs_filename )
                resource_texture = Path::Combine( g_TextureDirectory, basename + g_Extension )
                            
                if ( not System::IO::File::Exists( resource_texture ) ) then
                    ##g_Log.Error( "Resource texture #{resource_texture} doesn't exist.  Skipping." )
                    skipped += 1
                    next
                end            
            
                p4.Run( 'edit', false, tcs_filename )
                SpecificationFactory::UpdateResourceTextures( tcs_filename, 
                    [].to_clr( System::String ), resource_texture )
            end 
        ensure
            p4.Disconnect( )
        end
    
        g_Log.Message( "#{processed}/#{total} TCS files processed (#{skipped} skipped)." )
    
    rescue SystemExit => ex
    
        LogFactory.ApplicationShutdown()
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/migration/data_batch_export_textures.rb
