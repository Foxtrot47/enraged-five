

require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.Pipeline.Core.dll'
require 'RSG.Pipeline.Content.dll'
require 'RSG.Pipeline.Engine.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::Pipeline::Core
include RSG::Pipeline::Content
include RSG::Pipeline::Engine

require 'System.Xml'
require 'mscorlib'
require 'System.Core'
include System::Collections::Generic
include System::IO
include System::Diagnostics
include System::Xml

using_clr_extensions System::Linq

require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [
LongOption::new( 'output', LongOption::ArgType.Required, 'o', 
        'Nothing!!' )
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
def usage( options )
    puts "#{__FILE__}"
    puts "Usage:"
    puts options.usage()
    exit( 1 )
end

def CreatePropAssetXMLFile(filename)

	xml_writer_settings = XmlWriterSettings.new()
	xml_writer_settings.Indent = true
	xml_writer_settings.IndentChars = "\t"
    	
	xml_writer = XmlWriter.Create(filename, xml_writer_settings)
	xml_writer.WriteStartDocument()
	xml_writer.WriteStartElement("Assets")

	return xml_writer
end

def AddStreamedPedPropAsset(xml_writer, prop_folder, prop_name, output_zip)
	xml_writer.WriteStartElement("ZipArchive")
	xml_writer.WriteAttributeString("path", output_zip)
	xml_writer.WriteStartElement("Directory")
	xml_writer.WriteAttributeString("path", prop_folder)
	xml_writer.WriteAttributeString("destination", prop_name)
	xml_writer.WriteEndElement()
	xml_writer.WriteEndElement()
end

def AddPedPropAsset(xml_writer, texture_file, drawable_file, output_zip)
	xml_writer.WriteStartElement("ZipArchive")
	xml_writer.WriteAttributeString("path", output_zip)
	xml_writer.WriteStartElement("File")
	xml_writer.WriteAttributeString("path", texture_file)
	xml_writer.WriteEndElement()
	xml_writer.WriteStartElement("File")
	xml_writer.WriteAttributeString("path", drawable_file)
	xml_writer.WriteEndElement()
	xml_writer.WriteEndElement()
end

if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog
    g_Options = OS::Options::new( OPTIONS )

    begin
        if ( g_Options.is_enabled?( 'help' ) )
            usage( g_Options )
        end
    
        g_Config = g_Options.command_options.Config
        g_Project = g_Options.command_options.Project
        g_BranchName = g_Options.has_option?( 'branch' ) ? g_Options.get( 'branch' ) : g_Project.DefaultBranchName
        g_Branch = g_Project.Branches[g_BranchName] if ( g_Project.Branches.ContainsKey( g_BranchName ) )
        if ( g_Branch.nil? ) then
            g_Log.Error( "Invalid branch '#{g_BranchName}' for project '#{g_Project.UIName}'." )
            exit( 1 )
        end

        g_pedPropDirectory = Path::Combine( g_Branch.export, "models/cdimages/pedprops" )
		g_streamPedPropDirectory = Path::Combine( g_Branch.export, "models/cdimages/streamedpedprops" )

		g_pedPropZipFile = Path::Combine( g_Branch.export, "models/cdimages/pedprops.zip" )
		g_streamedPedPropZipFile = Path::Combine( g_Branch.export, "models/cdimages/streamedpedprops.zip" )
		
		g_assetFilename = Path::Combine( g_Branch.export, "models/cdimages/tempZip/propConversion.xml" )

		xml_writer = CreatePropAssetXMLFile( g_assetFilename )

		# Ped Props Conversion
		#---------------------------------------------------------------------------------
		g_tempExtract = Path::Combine( g_Branch.export, "models/cdimages/tempZip/pedprops" )
		puts "Extracting: #{g_pedPropZipFile}"

		if (not Zip::ExtractAll( g_pedPropZipFile, g_tempExtract, true, nil ) ) then
			g_Log.Error( "Failed to read zip: #{g_pedPropZipFile}.  Ignoring." )
		end

		Directory.GetFiles( g_tempExtract, "*.idd.zip").each do |drawable|
			baseName = File.basename( drawable, ".idd.zip" )
			
			drawable_file = drawable
			texture_file = Path::Combine( g_tempExtract, baseName + ".itd.zip" )

			output_zip = Path::Combine( g_pedPropDirectory, baseName + ".prop.zip" )
			System::IO::File::Delete( output_zip )

			AddPedPropAsset( xml_writer, texture_file, drawable_file, output_zip )

			puts "Creating entry for: #{output_zip}"
		end
		#---------------------------------------------------------------------------------

		# Streamed Ped / Cutscene Props Conversion
		#---------------------------------------------------------------------------------
		g_tempExtract = Path::Combine(g_Branch.export, "models/cdimages/tempZip/streamedpedprops")
		puts "Extracting: #{g_streamedPedPropZipFile}"

		if (not Zip::ExtractAll( g_streamedPedPropZipFile, g_tempExtract, true, nil )) then
			g_Log.Error( "Failed to read zip: #{g_streamedPedPropZipFile}.  Ignoring." )
		end

		Dir::open( g_tempExtract ) do |dir|
			dir.each do |filename|
				next if ( '.' == filename )
				next if ( '..' == filename )
				
				output_zip = Path::Combine( g_streamPedPropDirectory, filename + ".prop.zip" )
				source_folder = Path::Combine( g_tempExtract, filename)
				System::IO::File::Delete( output_zip )

				AddStreamedPedPropAsset(xml_writer, source_folder, filename, output_zip)

				puts "Creating entry for: #{output_zip}"
			end
		end
		#---------------------------------------------------------------------------------
		
		# Write out the asset XML file
		xml_writer.WriteEndDocument()
		xml_writer.Close()

		# Execute the data_asset_pack to zip up all the prop files
		ir_exe = g_Config.ToolsIronRuby
		args ="$(toolsroot)/ironlib/util/data_asset_pack.rb"
		args = args + " --rebuild"
		args = args + " #{g_assetFilename}"

		ir_exe = g_Config.Environment.Subst(ir_exe)
		args = g_Config.Environment.Subst(args)
		g_Log.Message("Asset pack command #{ir_exe} #{args}")
		startInfo = ProcessStartInfo::new()
		startInfo.Arguments = args
		startInfo.FileName = ir_exe
		startInfo.UseShellExecute = false

		proc = System::Diagnostics::Process.Start(startInfo)
		proc.WaitForExit()
		exit_code = proc.ExitCode

		g_Log.Error("Errors creating prop files") if (exit_code != 0)

    rescue SystemExit => ex
    
        LogFactory.ApplicationShutdown()
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        exit( 1 )
    end
end