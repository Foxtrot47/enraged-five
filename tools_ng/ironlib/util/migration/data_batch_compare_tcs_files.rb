#
# File:: %RS_TOOLSLIB%/util/migration/data_batch_compare_tcs_files.rb
# Description:: Batch TCS file comparison; validates:
#                 1. parent basenames match
#                 2. imageSplitHD property
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 12 March 2013
#
# Usage:
#   %RS_TOOLSIR% data_batch_compare_tcs_files.rb
#
# Example:
#   
#   %RS_TOOLSIR% data_batch_compare_tcs_files.rb 
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.Pipeline.Core.dll'
require 'RSG.Pipeline.Services.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::Pipeline::Services

require 'mscorlib'
require 'System.Core'
require 'System.Xml'
require 'System.Xml.Linq'
include System::Collections::Generic
include System::IO
include System::Xml::Linq
using_clr_extensions System::Linq
using_clr_extensions System::Xml::Linq
using_clr_extensions System::Xml::XPath

require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [  
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
def usage( options )
    puts "#{__FILE__}"
    puts "Usage:"
    puts options.usage()
    exit( 1 )
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog
    g_Options = OS::Options::new( OPTIONS )
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            usage( g_Options )
        end
    
        g_Config = g_Options.command_options.Config
        g_Project = g_Options.command_options.Project
        g_BranchName = g_Options.has_option?( 'branch' ) ? g_Options.get( 'branch' ) : g_Project.DefaultBranchName
        g_Branch = g_Project.Branches[g_BranchName] if ( g_Project.Branches.ContainsKey( g_BranchName ) )
        if ( g_Branch.nil? ) then
            g_Log.Error( "Invalid branch '#{g_BranchName}' for project '#{g_Project.UIName}'." )
            exit( 1 )
        end
        
        g_MapsTCS = Path::Combine( g_Branch.Assets, 'metadata', 'textures', 'maps' )
        g_MapsHalfTCS = Path::Combine( g_Branch.Assets, 'metadata', 'textures', 'maps_half' )
        
        g_Log.Profile( "Finding all Maps Half TCS files." )
        g_MapsHalfTCSFiles = Directory::GetFiles( g_MapsHalfTCS, '*.tcs', SearchOption.AllDirectories )
        #g_MapsHalfTCSFiles.Sort( )
        g_Log.ProfileEnd( )
        g_MismatchedTCS = 0
        g_TotalTCS = 0
        g_MapsHalfTCSFiles.each do |maps_half_tcs|
        
            g_Log.Message( "Comparing: #{maps_half_tcs}..." )
                
            g_TotalTCS += 1
            tcs_filename = Path::GetFileName( maps_half_tcs )
            maps_tcs = Path::Combine( g_MapsTCS, tcs_filename )
                        
            # Skip if we don't have a maps equivalent.
            next unless ( System::IO::File::Exists( maps_tcs ) ) 

            # USING RAW XML
            maps_spec_doc = XDocument::Load( maps_tcs )
            maps_half_spec_doc = XDocument::Load( maps_half_tcs )
      
            maps_parent = ''
            maps_half_parent = ''
      
            maps_parent_elem = maps_spec_doc.Root.XPathSelectElement( 'parent' )
            maps_parent = Path::GetFileNameWithoutExtension( maps_parent_elem.Value ) unless ( maps_parent_elem.nil? )
            
            maps_half_parent_elem = maps_half_spec_doc.Root.XPathSelectElement( 'parent' )
            maps_half_parent = Path::GetFileNameWithoutExtension( maps_half_parent_elem.Value ) unless ( maps_half_parent_elem.nil? )
            
            maps_split_hd = 'false'
            maps_half_split_hd = 'false'
            
            maps_split_hd_elem = maps_spec_doc.Root.XPathSelectElement( 'imageSplitHD' )
            maps_half_split_hd_elem = maps_half_spec_doc.Root.XPathSelectElement( 'imageSplitHD' )
            
            maps_split_hd_attr = maps_split_hd_elem.Attribute( XName::Get( "value" ) ) unless ( maps_split_hd_elem.nil? )
            maps_split_hd = maps_split_hd_attr.Value unless ( maps_split_hd_attr.nil? )
            
            maps_half_split_hd_attr = maps_half_split_hd_elem.Attribute( XName::Get( "value" ) ) unless ( maps_half_split_hd_elem.nil? )
            maps_half_split_hd = maps_half_split_hd_attr.Value unless ( maps_half_split_hd_attr.nil? )
                   
            # Match vars
            imageSplitHDMismatch = ( 0 != System::String::Compare( maps_split_hd, maps_half_split_hd, true ) )
            parentMismatch = ( 0 != System::String::Compare( maps_parent, maps_half_parent ) )
      
            # USING SPECIFICATION CLASS: target-sensitive.
=begin
            maps_spec = RSG::Pipeline::Services::Platform::Texture::Specification::new( 
                g_Branch.Targets.First().Value, maps_tcs )
            maps_half_spec = RSG::Pipeline::Services::Platform::Texture::Specification::new( 
                g_Branch.Targets.First().Value, maps_half_tcs )
            
            maps_parent = Path::GetFileNameWithoutExtension( maps_spec.ImmediateParent )
            maps_half_parent = Path::GetFileNameWithoutExtension( maps_half_spec.ImmediateParent )
            
            # Match vars.
            imageSplitHDMismatch = ( maps_spec.ImageSplitHD != maps_half_spec.ImageSplitHD )
            parentMismatch = ( 0 != System::String::Compare( maps_parent, maps_half_parent, true ) )
=end        
            
            if ( imageSplitHDMismatch or parentMismatch ) then
                g_MismatchedTCS += 1
                if ( imageSplitHDMismatch ) then
                    g_Log.Error( "\timageSplitHD mismatch." )
                end     
                if ( parentMismatch ) then
                    g_Log.Error( "\tparent mismatch: #{maps_half_parent} : #{maps_parent}" )
                end
            end
        end
        g_Log.Message( "#{g_MismatchedTCS}/#{g_TotalTCS} mismatched TCS files." )
    
    rescue SystemExit => ex
    
        LogFactory.ApplicationShutdown()
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/migration/data_batch_compare_tcs_files.rb
