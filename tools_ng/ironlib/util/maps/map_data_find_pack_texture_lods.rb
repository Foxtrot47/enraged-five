#
# File:: %RS_TOOLSLIB%/Util/test_map_unique_hashes.rb
# Description:: 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 22 February 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'lib/RSG.Base.dll'
require 'lib/RSG.Base.ConfigParser.dll'
require 'lib/RSG.ManagedRage.dll'
require 'lib/RSG.SceneXml.dll'
include RSG::Base::ConfigParser
include RSG::Base::Logging
include RSG::ManagedRage
include RSG::SceneXml

require 'mscorlib'
require 'System.Core'
include System

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

#
# Yield a user-block for each map RPF in our content tree.
#
def data_map_for_each( &block )
	
    config = ConfigGameView::new()
	maps = config.Content.Root.FindAll( "map" )
    if ( block_given? ) then
        maps.each do |map|
            yield map
        end
    end
	maps
end

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( $0 == __FILE__ ) then

    begin
        consoleLog = ConsoleLog::new()
        
        container_names = {}
        containers = {}
        containers16 = {}
        entities = {}
        scenes = SceneCollection::new()
        clashes = 0
        data_map_for_each do |map|
            scene = scenes.GetScene( map.Name )
            next if ( scene.nil? )
                        
            fp = File::open( "c:\\#{map.Name}.txt", 'w' )
            
            entities[hash] = {}
            scene.Walk( RSG::SceneXml::Scene::WalkMode.BreadthFirst ).each do |object|
                next if ( object.IsRefObject() )
                next if ( object.DontExport() or object.DontExportIDE() )
                next if ( ( RSG::SceneXml::ObjectDef::LodLevel.HD == object.LODLevel ) or ( RSG::SceneXml::ObjectDef::LodLevel.ORPHANHD == object.LODLevel ) )
                
                if ( object.GetAttribute( "Pack Textures", false ) )
                    fp.puts( object.Name )
                    Log::Log__Warning( "Object: #{object.Name}." )
                end
            end
            fp.close()
            
            scenes.UnloadScene( map.Name )
        end
        Log::Log__Message( "Done." )
        
    rescue Exception => ex
    
        puts "Exception: #{ex.message}"
        puts ex.backtrace.join( "\n" )
    end
end
