#
# File:: %RS_TOOLSLIB%/Util/test_map_unique_hashes.rb
# Description:: 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 22 February 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'lib/RSG.Base.dll'
require 'lib/RSG.Base.ConfigParser.dll'
require 'lib/RSG.ManagedRage.dll'
require 'lib/RSG.SceneXml.dll'
include RSG::Base::ConfigParser
include RSG::Base::Logging
include RSG::ManagedRage
include RSG::SceneXml

require 'mscorlib'
require 'System.Core'
include System

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

#
# Yield a user-block for each map RPF in our content tree.
#
def data_map_for_each( &block )
	
    config = ConfigGameView::new()
	maps = config.Content.Root.FindAll( "map" )
    if ( block_given? ) then
        maps.each do |map|
            yield map
        end
    end
	maps
end

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( $0 == __FILE__ ) then

    begin
        consoleLog = ConsoleLog::new()
        puts "Hello Asset Pipeline"
        
        Log::Log__Message( 'Testing logging...' )
        Log::Log__Error( 'Testing error...' )
        Log::Log__Debug( 'Testing debug...' )
        Log::Log__Warning( "Testing warning..." )
        
        container_names = {}
        containers = {}
        containers16 = {}
        entities = {}
        scenes = SceneCollection::new()
        clashes = 0
        data_map_for_each do |map|
            scene = scenes.GetScene( map.Name )
            next if ( scene.nil? )
                        
            # 16-bits for the container ID.
            hash = scene.Hash32 #( StringHashUtil::atStringHash( map_name, seed ) ) # & 0x00000FFF )
            if ( containers.has_key?( hash ) ) then
                Log::Log__Error( "Container #{map.Name} (#{map_name}) hash collision with #{container_names[hash]} #{containers[hash]} (#{hash})." )
                clashes += 1
            else
              #  puts "Scene: #{scene.Filename}"
                containers[hash] = map.Name
                container_names[hash] = map.Name
            end
=begin
                entities[hash] = {}
                scene.Walk( RSG::SceneXml::Scene::WalkMode.BreadthFirst ).each do |object|
                    next unless ( object.IsRefObject() )
                    next if ( object.DontExport() or object.DontExportIPL() )
                
                    # 32-bit archetype name.
                    archetype_name = object.GetObjectName( )
                    archetype_hash = StringHashUtil::atStringHash( archetype_name, 0 )
                    entities[hash][archetype_hash] = {} unless ( entities[hash].has_key?( archetype_hash ) ) 
                    
                    # 16-bit entity hash.
                    #entity_name_hash = StringHashUtil::atStringHash( object.Name, 1 )
                    #entity_hash = ( StringHashUtil::atStringHash( object.AttributeGuid.to_s, entity_name_hash ) & 0x0000FFFF )
                    entity_hash = ( StringHashUtil::atStringHash( object.AttributeGuid.to_s, hash_entity_seed ) )
                    if ( entities[hash][archetype_hash].has_key?( entity_hash ) ) then
                        Log::Log__Error( "Entity #{object.name} hash collision with #{entities[hash][archetype_hash][entity_hash]} (#{entity_hash})." )
                        $stdin.gets
                    else
                        puts "\tObject: #{object.Name} #{entity_hash}"
                        entities[hash][archetype_hash][entity_hash] = object
                    end
                end
            end
=end
            scenes.UnloadScene( map.Name )
        end
        Log::Log__Message( "#{clashes} hash collisions." )
        
    rescue Exception => ex
    
        puts "Exception: #{ex.message}"
        puts ex.backtrace.join( "\n" )
    end
    $stdin.gets
end
