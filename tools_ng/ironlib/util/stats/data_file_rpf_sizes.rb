#
# File:: %RS_TOOLSIRONLIB%/util/stats/data_file_rpf_sizes.rb
# Description:: Iterates over directory for RPF files reporting on asset sizes.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 27 October 2010
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.ManagedRage.dll'
require 'RSG.Platform.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::ManagedRage
include RSG::Platform

require 'mscorlib'
require 'System.Core'
include System::Collections::Generic
include System::IO
using_clr_extensions System::Linq

require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
    LongOption::new( 'output', LongOption::ArgType.Required, 'o', 
        'Specify output filename.' ), 
    LongOption::new( 'compare', LongOption::ArgType.None, 'c', 
        'Compare two paths specified.' ), 
]

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

# Return Hash of base-filename keys to Hash data.
def get_rpf_sizes_from( log, path, recurse, strip_path )

    data = {}
    pack = Packfile::new()
       
    rpf_files = System::IO::Directory::GetFiles( path, '*.rpf', recurse ? 
        SearchOption.AllDirectories : SearchOption.TopDirectoryOnly )
    rpf_files.each do |filename|
            
        log.Message( "Processing #{filename}..." )

        # Strip path is useful for comparisons.
        rpf_key = filename unless ( strip_path )
        rpf_key = Path::GetFileName( filename ) if ( strip_path )
        
        data[rpf_key] = {}
        pack.Load( filename )
                
        # Get file and resource counts.
        count_file = pack.Entries.Where( lambda { |e| e.IsFile } ).Count( )
        count_res = pack.Entries.Where( lambda { |e| e.IsResource } ).Count( )
        data[rpf_key][:resources] = count_res
        data[rpf_key][:files] = count_file
                
        System::Enum::GetValues( FileType.to_clr_type ).each do |ft|
            data[rpf_key][ft] = 0
        end
                
        pack.Entries.each do |entry|
            filetypes = RSG::Platform::Filename::GetFileTypes( entry.Name )
            type = filetypes[0]
            data[rpf_key][type] += 1
        end
                
        pack.Close( )
    end
    pack.Dispose()
    data
end

# Write data out.
def write_data( data, filename )

	# Write out report data based on g_FileTypes hash.
	# Sorting by descending file sizes.
	File::open( filename, 'w' ) do |fp|
		
        # Header
        header = "Filename,Resource Count,File Count"
        System::Enum::GetValues( FileType.to_clr_type ).each do |ft|
            header += ",#{ft}"
        end
        fp.write "#{header}\n"
            
        # Data
        data.each_pair do |filename, d|
            fp.write "#{filename},"
            fp.write "#{d[:resources]},"
            fp.write "#{d[:files]},"
            System::Enum::GetValues( FileType.to_clr_type ).each do |ft|
                fp.write "#{d[ft]},"
            end
            fp.write "\n"
        end
	end
end

# Write comparison data out.
def write_comparison_data( data1, data2, filename )

	# Write out report data based on g_FileTypes hash.
	# Sorting by descending file sizes.
	File::open( filename, 'w' ) do |fp|
		
        # Header
        header = "Filename,Resource Count 1,Resource Count 2,File Count 1,File Count 2"
        System::Enum::GetValues( FileType.to_clr_type ).each do |ft|
            header += ",#{ft} 1, #{ft} 2"
        end
        fp.write "#{header}\n"
            
        # Data
        data1.keys.each do |filename|
            d1 = data1.has_key?( filename ) ? data1[filename] : nil
            d2 = data2.has_key?( filename ) ? data2[filename] : nil
            next if ( d1.nil? or d2.nil? )
        
            fp.write "#{filename},"
            fp.write "#{d1[:resources]},#{d2[:resources]},"
            fp.write "#{d1[:files]},#{d2[:files]},"
            System::Enum::GetValues( FileType.to_clr_type ).each do |ft|
                fp.write "#{d1[ft]},#{d2[ft]},"
            end
            fp.write "\n"
        end
	end
end

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog
    g_Options = OS::Options::new( OPTIONS )
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            puts "#{__FILE__}"
            puts "Usage:"
            puts g_Options.usage()
            exit( 1 )
        end
    
        g_Config = g_Options.command_options.Config
        g_Project = g_Options.command_options.Project
        g_BranchName = g_Options.has_option?( 'branch' ) ? g_Options.get( 'branch' ) : g_Project.DefaultBranchName
        g_Branch = g_Project.Branches[g_BranchName] if ( g_Project.Branches.ContainsKey( g_BranchName ) )
        if ( g_Branch.nil? ) then
            g_Log.Error( "Invalid branch '#{g_BranchName}' for project '#{g_Project.UIName}'." )
            exit( 1 )
        end
	
        g_Comparison = g_Options.has_option?( 'compare' ) ? true : false
	    g_Output = g_Options.get( 'output' )
	    if ( g_Output.nil? ) then
		    g_Log.Error "\nError output path not set.  Provide --output with filename option."
		    exit( 4 )
	    end
	        
        g_Paths = []
        g_Options.trailing.each do |filename|
            g_Paths << System::IO::Path.GetFullPath( filename )
        end
        
        if ( g_Comparison ) then
            # Comparison mode; ensure two paths, generate data and write 
            # compare CSV.
            if ( 2 != g_Paths.size() ) then
                g_Log.Error( "Comparison mode requires two trailing paths only." )
                puts g_Options.usage()
                exit( 1 )
            end
            
            g_Data1 = get_rpf_sizes_from( g_Log, g_Paths[0], true, true )
            g_Data2 = get_rpf_sizes_from( g_Log, g_Paths[1], true, true )
            write_comparison_data( g_Data1, g_Data2, g_Output )           
            
        else
            # Single amount of data; parse and write.
            g_Data = {}
            g_Paths.each do |path|
        
                data = get_rpf_sizes_from( g_Log, path, true, false )
                g_Data.merge!( data )
            end
            write_data( g_Data, g_Output )
        end
        
    rescue SystemExit => ex
        LogFactory.ApplicationShutdown()
        exit( ex.status )   
	
    rescue Exception => ex
        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        if ( g_Options.show_popups? ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end
        
        exit( 1 )
    end
end

# %RS_TOOLSIRONLIB%/util/stats/data_file_rpf_sizes.rb
