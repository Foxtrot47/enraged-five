#
# File:: %RS_TOOLSLIB%/util/data_asset_pack.rb
# Description:: Entry-point into our asset build pipeline for packing assets 
#               (no conversion).
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 24 October 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.Pipeline.Core.dll'
require 'RSG.Pipeline.Content.dll'
require 'RSG.Pipeline.Engine.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::Pipeline::Core
include RSG::Pipeline::Content
include RSG::Pipeline::Engine

require 'mscorlib'
require 'System.Core'
include System::Collections::Generic
include System::IO

require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
    LongOption::new( 'rebuild', LongOption::ArgType.None, 'f', 
        'Rebuild data; ignoring timestamp information.' ), 
    LongOption::new( 'no-sync', LongOption::ArgType.Required, 's', 
        'Disable engine Perforce dependency sync.' ),
    LongOption::new( 'no-xge', LongOption::ArgType.None, 'x', 
        'Disable Xoreax XGE parallel processing engine.' ),
]
ASSET_PACK_PROCESSOR = 'RSG.Pipeline.Processor.Common.AssetPackProcessor'

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
# None

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    LogFactory.Initialize()
    g_Log = LogFactory.method(:CreateUniversal).of(UniversalLog).call( 'data_asset_pack' )
    LogFactory.CreateApplicationConsoleLogTarget( )        
    g_Options = OS::Options::new( OPTIONS )
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            puts "#{__FILE__}"
            puts "Usage:"
            puts g_Options.usage()
            puts "Trailing arguments specify Asset Pack Processor XML files."
            exit( 1 )
        end
    
        g_Project = g_Options.command_options.Project
        g_BranchName = g_Options.has_option?( 'branch' ) ? g_Options.get( 'branch' ) : g_Project.DefaultBranchName
        g_Branch = g_Project.Branches[g_BranchName] if ( g_Project.Branches.ContainsKey( g_BranchName ) )
        if ( g_Branch.nil? ) then
            g_Log.Error( "Invalid branch '#{g_BranchName}' for project '#{g_Project.UIName}'." )
            exit( 1 )
        end
        
        # Engine initialisation.
        g_EngineFlags = EngineFlags.Default
        g_EngineFlags |= EngineFlags.Rebuild if ( g_Options.is_enabled?( 'rebuild' ) )
        g_EngineFlags &= ~EngineFlags.SyncDependencies if ( g_Options.is_enabled?( 'no-sync' ) )
        g_EngineFlags &= ~EngineFlags.XGE if ( g_Options.is_enabled?( 'no-xge' ) )
        g_Processor = ASSET_PACK_PROCESSOR
        
        if ( not (g_EngineFlags.HasFlag(EngineFlags.XGE)) ) then
            g_Log.Warning( "Xoreax XGE parallel processing engine disabled." )
        end
        
        g_Log.Message( "Engine Flags from user options: #{g_EngineFlags}" )
         g_Engine = Engine::new( g_Options.command_options, g_EngineFlags )
        g_BuildTime = nil
                
        # Construct an Asset Pack Process.
        build_input = BuildInput::new( g_Branch )
        tree = Factory::CreateEmptyTree( g_Branch ) 
        pb = ProcessBuilder::new( g_Processor, g_Engine.Processors, tree )
                
        # Handle multiple XML inputs.
        g_Options.trailing.each do |trailing|
            pb.Inputs.Add( tree.CreateFile( trailing, false ) )
        end
        process = pb.ToProcess( )
        build_input.RawProcesses.Add( process )
        
        g_Outputs = g_Engine.Build( build_input ) 
    
        g_BuildTime = g_Outputs[2]
        g_Log.Message( "Asset Pack Took: #{g_BuildTime}" )
        exit( g_Outputs[0] ? 0 : 1 )
    
    rescue SystemExit => ex
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during data_convert_file: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        unless ( g_Options.is_enabled?( 'nopopups' ) ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/data_asset_pack.rb
