#
# File:: %RS_TOOLSLIB%/Util/anim/ingame/zip_animations.rb
# Description:: 
#
# Author:: Fraser Tomison <fraser.tomison@rockstarnorth.com>
# Date:: 02 October 2014
#
#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::SourceControl::Perforce

require 'mscorlib'
require 'System.Core'
require 'System.Xml'
include System::Collections::Generic
include System::IO
include System::Diagnostics
include System::Xml

require 'pipeline/os/options'
require 'pipeline/monkey/array'
require 'fileutils'

include Pipeline

def run( config, src_dir, out_file )

	p4 = config.Project.SCMConnect()
	begin
		dirname = File.dirname(out_file)
		unless File.directory?(dirname)
			FileUtils.mkdir_p(dirname)
		end
		
		args = ['-l', (Path.Combine( src_dir, '...' ) + '#>have')]
		changeRecordSet = p4.Run( 'changes', System::Array[System::String].new(args) )
	
		outFile = File.new(out_file, "w")
	
		changeRecordSet.each do |set|
			outFile.puts( 'Change - ' + set.fields["change"] + ' - ' + set.fields["user"] + '@' + set.fields["client"])
		
			args = ['-s', set.fields["change"]]
			describeRecordSet = p4.Run( 'describe', System::Array[System::String].new(args) )
		
			outFile.puts( describeRecordSet.records[0].fields["desc"] )
		
			outFile.puts( "" )
		end
	
		outFile.close
	
	ensure
		p4.Disconnect()
	end
end

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------	
if ( __FILE__ == $0 ) then
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	begin
		g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )
		g_Config.DLCProjects.each do | pair |
			export_path = "#{pair.Value.Root}\\"
			input_path = "#{pair.Value.DefaultBranch.Assets}\\cuts\\"
			if (Directory.Exists( input_path ) == false) then
				next
			end
			
			time = Time.new
			out_file = export_path + "build_changelists\\changes_" + time.month.to_s + "_" + time.day.to_s + "_" + time.hour.to_s + "_" + time.min.to_s + ".txt"
			run( g_Config, input_path, out_file)
		end

	rescue Exception => ex
		puts "Exception during cutscene_changes_dlc: #{ex.message}"
			puts ex.backtrace.join("\n")
			
			exit( 1 )
		
	end
end