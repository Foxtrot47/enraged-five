#
# File:: %RS_TOOLSLIB%/Util/anim/cutscene/zip_animations.rb
# Description:: 
#
# Author:: Mike Wilson <mike.wilson@rockstarnorth.com>
# Date:: 15 January 2013
#
#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::SourceControl::Perforce

require 'mscorlib'
require 'System.Core'
require 'System.Xml'
include System
include System::Collections::Generic
include System::IO
include System::Diagnostics
include System::Xml

require 'pipeline/os/options'
require 'pipeline/monkey/array'

require 'RSG.ManagedRage.dll'
include RSG::ManagedRage::ClipAnimation

include Pipeline
#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [ 
    LongOption::new( 'rebuild', LongOption::ArgType.None, 'f', 'Rebuild data; ignoring timestamp information.' ), 
    LongOption::new( 'checkout', LongOption::ArgType.None, 'c', 'Checkout data from p4.' ), 
    LongOption::new( 'nosync', LongOption::ArgType.Optional, 's', 'Prevents automatic syncing.' ), 
    LongOption::new( 'manifest', LongOption::ArgType.Optional, 'm', 'Manifest file override.' ), 
    LongOption::new( 'synccl', LongOption::ArgType.Optional, 'e', 'Sync specific changelists.' ),
	
]

def get_latest_time_from_dir(src_dir)

    time = DateTime::new(2011,1,1)
    files = Directory.GetFiles(src_dir, "*.*", SearchOption.AllDirectories)

    files.each do | file |
        if(System::IO::File.GetLastWriteTime(file).CompareTo(time) > 0) then
            time = System::IO::File.GetLastWriteTime(file)
        end
    end

    return time
end

def checkout_files( p4, changelist, file_mapping, log)

	main_args = ['-c', changelist.Number.to_s] 
	second_args = ['-t', '+l']
	file_mapping.each do | zip_dictionary |
		if p4.Exists(zip_dictionary.DepotFilename)
			log.Message("Marking for edit #{zip_dictionary.DepotFilename}")
			args = main_args + [zip_dictionary.DepotFilename] 
			args = System::Array[System::String].new(args)

			ret = p4.Run('edit', args)
		else 
			log.Message("Marking for add #{zip_dictionary.DepotFilename}")
			args = main_args + ['-f', '-tbinary+Flw', zip_dictionary.LocalFilename] 
			args = System::Array[System::String].new(args)

			ret = p4.Run('add', args)
		end

		
		# TODO: Handle errors better.  Do we want to bail if there are any?
		ret.Messages.each do | msg |
			log.Message(msg)
		end
		log.Message("about to re-open #{zip_dictionary}")
		args = second_args + [zip_dictionary.DepotFilename]
		args = System::Array[System::String].new(args)
		ret = p4.Run('reopen', args)
		ret.Messages.each do | msg |
			log.Message(msg)
		end
	end
	
end

def generate_master_icd_list(config, rebuild, log, master_list_filename, src_dir, dest_dir, concat_list_dir )
	result = true
	System::IO::File.Delete(master_list_filename) if System::IO::File.Exists(master_list_filename)
	directory_list = Directory.GetDirectories(src_dir)
    
    MClip.Init()
	
	xml_writer_settings = XmlWriterSettings.new()
	xml_writer_settings.Indent = true
	xml_writer_settings.IndentChars = "\t"
    	
	xml_writer = XmlWriter.Create(master_list_filename, xml_writer_settings)
	xml_writer.WriteStartDocument()
	xml_writer.WriteStartElement("Assets")
    
    dest_dir = dest_dir.Replace('\\', '/')
    #sceneSplit = dest_dir.Split("/")
    
    #get all names of the concat lists to ignore the concat folders
    concat_list = Directory.GetFiles(concat_list_dir, "*.concatlist")
    
	directory_list.each do | dir |
    
        # TEMP TO NOT BUILD ZIPS FOR CONCAT DIRS
        skip_dir = false
        concat_list.each do | list |
        
            xml_doc = XmlDocument.new()
            
            xml_doc.Load(list)
            
            xml_node = xml_doc.SelectSingleNode("/RexRageCutscenePartFile/path")

            if(Path.GetFileName(xml_node.InnerText).ToLower() == Path.GetFileName(dir).ToLower()) then
                puts "SKIPPING: " + Path.GetFileName(dir)
                skip_dir = true
                break
            end
        end
        
        if(skip_dir == true) then 
            next
        end
        # TEMP TO NOT BUILD ZIPS FOR CONCAT DIRS
		
		clip_list = Directory.GetFiles(dir, ("*.clip"));
        
        # Skip empty folders
        if(clip_list.Length == 0) then
            next
        end

        found_property = false
        clip_list.each do | clip |
            rageClip = MClip::new(0) # this is MClip.ClipType.Normal
            if(rageClip.Load(clip) == true) then
                fbx_property = rageClip.FindProperty("FBXFile_DO_NOT_RESOURCE")
                if (fbx_property != nil) then
                    found_property = true
                    fbx_file = (fbx_property.GetPropertyAttributes()[0]).GetString()
                    fbx_file = config.Project.DefaultBranch.Environment.Subst(fbx_file)
                    fbx_file = fbx_file.Replace('\\','/')
                    fbx_file = fbx_file.ToLower();
                    
                    sceneSplit = dest_dir.Split("/")
                    
                    # HACK FOR FBX FILES WITH ASSETS PATH FOR FBX 
                    if fbx_file.StartsWith("x:/gta5/art/animation/cutscene/!!scenes") == true then
                    	tempDestDir = System::String.Format("x:/gta5/art/animation/cutscene/!!scenes")
                    	sceneSplit = tempDestDir.Split("/")
                    end
                    
                    assetSplit = fbx_file.Split('/');

                    missionName = ""
                               
                    begin
                        missionName = assetSplit[sceneSplit.Length]
                        
                        # We have some clips that have x:\depot\gta5, this messes up the mapping
                        if missionName == "!!scenes" then
                        	log.Warning("Clip contains mission !!scenes - " + fbx_file + " - " + clip)
                        	break;
                        end
                        
                    rescue Exception => ex
                        log.Error("Unable to generate zip for '" + Path.GetFileName(dir) + "' - " + ex.message + " - " + fbx_file + " | " + dest_dir + " | " + clip)
                        break
                    end
                    
                    zipFileName = System::String.Format("$(export)/anim/cutscene/{0}/{1}.icd.zip", missionName, Path.GetFileName(dir))
                    zipFileName = config.Project.DefaultBranch.Environment.Subst(zipFileName)

                    # check if the data is newer than the zip
                    if(rebuild == false) then
                        if(System::IO::File.Exists(zipFileName)) then
                            zipTime = System::IO::File.GetLastWriteTime(zipFileName)
                            dirtime = System::IO::Directory.GetLastWriteTime(dir)
                            fileTime = get_latest_time_from_dir(dir)
                    
                            if (zipTime.CompareTo(fileTime) > 0 && zipTime.CompareTo(dirtime) > 0) then
                                break;
                            end
                        end
                    end
                    
                    xml_writer.WriteStartElement("ZipArchive")
		            xml_writer.WriteAttributeString("path", zipFileName)
		            xml_writer.WriteStartElement("Directory")
		            xml_writer.WriteAttributeString("path", dir)
		            xml_writer.WriteEndElement()
                    
                    firstlvl_sub = Directory.GetDirectories(dir)
                    firstlvl_sub.each do | first_sub |
                        if(Path.GetFileName(first_sub).ToLower() != "obj" && Path.GetFileName(first_sub).ToLower() != "toybox") then
                            xml_writer.WriteStartElement("Directory")
                            xml_writer.WriteAttributeString("path", dir + "\\" + Path.GetFileName(first_sub))
                            xml_writer.WriteAttributeString("destination", "\\" + Path.GetFileName(first_sub))
                            xml_writer.WriteEndElement();
                        end
                        
                        secondlvl_sub = Directory.GetDirectories(first_sub)
                        secondlvl_sub.each do | second_sub |
                            if(Path.GetFileName(second_sub).ToLower() != "toybox") then
                                xml_writer.WriteStartElement("Directory")
                                xml_writer.WriteAttributeString("path", first_sub + "\\" + Path.GetFileName(second_sub))
                                xml_writer.WriteAttributeString("destination", Path.GetFileName(first_sub) + "\\" + Path.GetFileName(second_sub))
                                
                                xml_writer.WriteEndElement();
                            end
                        end
                    end
		            xml_writer.WriteEndElement() 
                    break   
                end
            end
            rageClip.Dispose()
        end
        
        if(found_property != true) then
            log.Warning("No clips contain property FBXFile_DO_NOT_RESOURCE - '" + Path.GetFileName(dir) + "'")
        end
	end
	xml_writer.WriteEndDocument()
    xml_writer.Close()
	
	return result
end

def run( config, rebuild, checkout, log, no_sync, manifest_file_path, no_popups, sync_cl )
	
	src_dir = config.Project.DefaultBranch.Environment.Subst("$(art)/animation/cutscene/!!scenes")
	dest_dir = config.Project.DefaultBranch.Environment.Subst("$(export)/anim/cutscene/")
	asset_dir = config.Project.DefaultBranch.Environment.Subst("$(assets)/cuts")
	concat_list_dir = config.Project.DefaultBranch.Environment.Subst("$(assets)/cuts/!!Cutlists")
	sync_cl_file = config.Project.DefaultBranch.Environment.Subst("$(toolsroot)/etc/config/cutscene/syncchangelists.txt")

	if manifest_file_path != nil then
	
		manifest_directory = Path.GetDirectoryName(manifest_file_path)
		if Directory.Exists(manifest_directory) == false then
			Directory.CreateDirectory(manifest_directory)
		end
		
		master_list_filename = manifest_file_path
	else
		master_list_filename = config.Project.DefaultBranch.Environment.Subst("$(export)/anim/cutscene/master_icd_list.xml")
	end
	
	p4 = config.Project.SCMConnect()
	begin
	
		if not no_sync then
			log.Message("Syncing: #{dest_dir}")
			p4.Run( 'sync', true, Path.Combine( dest_dir, '...' ) )
			log.Message("Sync complete")

			if not sync_cl then
				log.Message("Syncing: #{asset_dir}")
				p4.Run( 'sync', Path.Combine( asset_dir, '...' ) )
				log.Message("Sync complete")
			else
				changelistnumbers = System::IO::File.ReadAllLines(sync_cl_file)
				changelistnumbers.each do | change |
					log.Message("Syncing CL: #{change}")
					p4.Run( 'sync', System::String.new('@=' + change) )
					log.Message("Sync complete")
				end
			end

        		log.Message("Syncing: #{concat_list_dir}")
        		p4.Run( 'sync', Path.Combine( concat_list_dir, '...' ) )
			log.Message("Sync complete")
		end
		
		zip_files = []
		changelist = nil
		if checkout then 
			log.Message("Checking out dictionaries...")
			zip_files =  Directory.GetFiles( dest_dir, "*.icd.zip", SearchOption.AllDirectories )

			file_mapping = FileMapping.Create(p4, System::Array[System::String].new(zip_files))

			if file_mapping.count > 0 then
				changelistname = 'Cutscene Animation Dictionaries [Asset Pipeline 3.0]'
				changelist = p4.CreatePendingChangelist(changelistname)
				checkout_files(p4, changelist, file_mapping, log)
			end
			log.Message("Check out complete.")
		end

		if(generate_master_icd_list(config, rebuild, log, master_list_filename, asset_dir, src_dir, concat_list_dir)) then
			log.Message("Zipping clip dictionaries...")
			ir_exe = config.ToolsRoot + "\\ironlib\\lib\\RSG.Pipeline.Convert.exe"
			args = ""
			args = args + " --rebuild" if rebuild == true
			args = args + " #{master_list_filename}"

			ir_exe = config.Environment.Subst(ir_exe)
			args = config.Environment.Subst(args)
			log.Message("Asset pack command #{ir_exe} #{args}")
			startInfo = ProcessStartInfo::new()
			startInfo.Arguments = args
			startInfo.FileName = ir_exe
			startInfo.UseShellExecute = false

			proc = System::Diagnostics::Process.Start(startInfo)
			proc.WaitForExit()
			exit_code = proc.ExitCode
			log.Error("Errors zipping animations, see log") if (exit_code != 0)
		else
			log.Error("Errors generating master icd list") if (exit_code != 0)
		end

		if checkout then
			post_zip_files = Directory.GetFiles( dest_dir, "*.icd.zip", SearchOption.AllDirectories )
			new_zip_files = Array::from_clr(post_zip_files).reject { |zip_file| zip_files.include?(zip_file) }

			if(new_zip_files.count > 0) then
				file_mapping = FileMapping.Create(p4, System::Array[System::String].new(new_zip_files))
				if file_mapping.count > 0 then
					log.Message("Adding new zip files")
					log.Message("New files: #{new_zip_files.inspect}")
					if changelist == nil then
						changelistname = 'Cutscene Animation Dictionaries [Asset Pipeline 3.0]'
						changelist = p4.CreatePendingChangelist(changelistname)
					end
					checkout_files(p4, changelist, file_mapping, log)

				end
			end
			log.Message("Reverting unchanged dictionaries...")
			args = ['-a', Path.Combine( dest_dir, '...' )]
			p4.Run('revert', System::Array[System::String].new(args))
		end
	ensure
		p4.Disconnect()
	end
	
end

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------	
if ( __FILE__ == $0 ) then
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	LogFactory.Initialize( )
	LogFactory.CreateApplicationConsoleLogTarget( )
    	g_Log = LogFactory.ApplicationLog
	begin
	
		g_Options = OS::Options::new( OPTIONS )
		
		g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )
		g_Rebuild = ( g_Options.is_enabled?( 'rebuild' ) )
		g_Checkout = ( g_Options.is_enabled?( 'checkout' ) )
		g_NoSync = (g_Options.has_option?( 'nosync' ))
		g_ManifestFile = (g_Options.get( 'manifest' ))
		g_NoPopups = ( g_Options.is_enabled?( 'nopopups' ) )
		g_SyncCL = (g_Options.has_option?( 'synccl' ))
		
		# Initialise log and console output.
		run( g_Config, g_Rebuild, g_Checkout, g_Log, g_NoSync, g_ManifestFile, g_NoPopups, g_SyncCL )
		
		LogFactory.ApplicationShutdown()
		LogFactory.ShowUniversalLogViewer(LogFactory.ApplicationLogFilename()) if ((not g_NoPopups) and g_Log.HasErrors())
		
	rescue Exception => ex
		g_Log.Exception( ex, "Exception during #{__FILE__}: #{ex.message}")
        	
        	LogFactory.ApplicationShutdown()
			LogFactory.ShowUniversalLogViewer(LogFactory.ApplicationLogFilename()) if ((not g_NoPopups) and g_Log.HasErrors())
	end
end
