#
# File:: %RS_TOOLSLIB%/Util/anim/ingame/zip_animations.rb
# Description:: 
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 09 October 2012
#
#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::SourceControl::Perforce

require 'mscorlib'
require 'System.Core'
require 'System.Xml'
include System::Collections::Generic
include System::IO
include System::Diagnostics
include System::Xml

require 'pipeline/os/options'
require 'pipeline/monkey/array'
require 'fileutils'

include Pipeline
#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [ 
	LongOption::new( 'src_dir', LongOption::ArgType.Required, 's', 'Source directory to sync' ), 
	LongOption::new( 'out_file', LongOption::ArgType.Required, 'c', 'Output file for CL info' ), 
]

def run( config, src_dir, out_file )

	p4 = config.Project.SCMConnect()
	begin
		dirname = File.dirname(out_file)
		unless File.directory?(dirname)
			FileUtils.mkdir_p(dirname)
		end
		
		args = ['-l', (Path.Combine( src_dir, '...' ) + '#>have')]
		changeRecordSet = p4.Run( 'changes', System::Array[System::String].new(args) )
	
		outFile = File.new(out_file, "w")
	
		changeRecordSet.each do |set|
			outFile.puts( 'Change - ' + set.fields["change"] + ' - ' + set.fields["user"] + '@' + set.fields["client"])
		
			args = ['-s', set.fields["change"]]
			describeRecordSet = p4.Run( 'describe', System::Array[System::String].new(args) )
		
			outFile.puts( describeRecordSet.records[0].fields["desc"] )
		
			outFile.puts( "" )
		end
	
		outFile.close
	
	ensure
		p4.Disconnect()
	end
end

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------	
if ( __FILE__ == $0 ) then
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	begin
		g_Options = OS::Options::new( OPTIONS )
		
		g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )
		g_SrcDir = g_Options.get('src_dir')
		g_OutFile = g_Options.get('out_file')

		g_SrcDir = g_Config.Project.DefaultBranch.Environment.Subst(g_SrcDir)
		
		run( g_Config, g_SrcDir, g_OutFile )

	rescue Exception => ex
		puts "Exception during cutscene_changes: #{ex.message}"
			puts ex.backtrace.join("\n")
			
			exit( 1 )
		
	end
end