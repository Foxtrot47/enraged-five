#
# File:: %RS_TOOLSLIB%/Util/anim/cutscene/process_animations_dlc.rb
# Description:: 
#
# Author:: Mike Wilson <mike.wilson@rockstarnorth.com>
# Date:: 23 April 2014
#
#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'

require 'mscorlib'
require 'System.Core'

include System
include System::Collections::Generic
include System::IO

g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )

require "#{g_Config.ToolsRoot}/ironlib/util/anim/process_animations.rb"

require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------	
if ( __FILE__ == $0 ) then
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	begin
		g_Options = OS::Options::new( OPTIONS )
		
		g_Rebuild = ( g_Options.is_enabled?( 'rebuild' ) )
		g_NoPopups = ( g_Options.is_enabled?( 'nopopups' ) )
		
		g_Config.DLCProjects.each do | pair |
			export_path = "#{pair.Value.DefaultBranch.Export}/anim/cutscene/"
			
			# Subst for consistency to match usage in the run function down below
			final_path = pair.Value.DefaultBranch.Environment.Subst(export_path)

			if (Directory.Exists( export_path ) == false) then
				next
			end
			
			if (Directory.GetDirectories(final_path).Length == 0) then
				next
			end

			# run function from process_animations.rb
			run( g_Config, g_Rebuild, export_path, g_NoPopups)
		end

	rescue Exception => ex
		puts "Exception during data_convert_file: #{ex.message}"
        	puts ex.backtrace.join("\n")
        	
        	exit( 1 )
	end
end