#
# File:: %RS_TOOLSLIB%/Util/anim/ingame/process_animations.rb
# Description:: 
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 218 September 2012
#
#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows

require 'mscorlib'
require 'System.Core'
include System::Collections::Generic
include System::IO
include System::Diagnostics

require 'pipeline/os/options'
include Pipeline
#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [ 
    LongOption::new( 'rebuild', LongOption::ArgType.None, 'f', 'Rebuild data; ignoring timestamp information.' ), 
    LongOption::new( 'src_dir', LongOption::ArgType.Required, 's', 'Source directory.' ), 
	LongOption::new( 'supportcore', LongOption::ArgType.None, 'c', 'Handle cutscenes in core' ), 
	LongOption::new( 'supportdlc', LongOption::ArgType.None, 'd', 'Handle cutscenes in dlcs' ), 
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
def run( config, rebuild, src_dir, no_popups)

	src_dir = config.Project.DefaultBranch.Environment.Subst(src_dir)
	anim_dirs = Directory.GetDirectories(src_dir)
	
	puts "Processing clip dictionaries..."
	ir_exe = config.ToolsRoot + "\\ironlib\\lib\\RSG.Pipeline.Convert.exe"
	args =""
	args = args + " --branch " + config.Project.DefaultBranchName
	args = args + " --rebuild" if rebuild == true
	args = args + " --nopopups" if no_popups == true
	
	anim_dirs.each do | dir |
		args = args + " #{dir}"
	end
	
	ir_exe = config.Environment.Subst(ir_exe)
	args = config.Environment.Subst(args)
	
	startInfo = ProcessStartInfo::new()
	startInfo.Arguments = args
	startInfo.FileName = ir_exe
	startInfo.UseShellExecute = false

	proc = System::Diagnostics::Process.Start(startInfo)
	proc.WaitForExit()
	exit_code = proc.ExitCode;
        puts "Errors build animations, see log" if (exit_code != 0)
	
end


#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------	
if ( __FILE__ == $0 ) then
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	begin
		g_Options = OS::Options::new( OPTIONS )
		
		g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )
		g_Rebuild = ( g_Options.is_enabled?( 'rebuild' ) )
		g_Dir = g_Options.get('src_dir')
		g_NoPopups = ( g_Options.is_enabled?( 'nopopups' ) )
		
		run( g_Config, g_Rebuild, g_Dir, g_NoPopups)

	rescue Exception => ex
		puts "Exception during data_convert_file: #{ex.message}"
        	puts ex.backtrace.join("\n")
        	
        	exit( 1 )
		
	end
end