#
# File:: email.rb
# Description:: Emails users
#		- fires of an email to users
# Author:: Derek Ward <derek@rockstarnorth.com>
# Date:: 29 April 2013
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'RSG.Base.dll'
require 'RSG.Pipeline.Core.dll'
require 'mscorlib'
require 'System.Core'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::Pipeline::Core

require 'pipeline/os/options'
include Pipeline

require 'fileutils'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
    LongOption::new( 'recipients', LongOption::ArgType.Required, 'r', 
        'Specify recipients.' ), 
    LongOption::new( 'subject', LongOption::ArgType.Required, 's', 
        'Specify subject.' ), 
    LongOption::new( 'body', LongOption::ArgType.Required, 'b', 
        'Specify Body' ), 
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then 

    g_Options = OS::Options::new( OPTIONS )
    
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            puts "#{__FILE__}"
            puts "Usage:"
            puts g_Options.usage()
            exit( 1 )
        end
    
    	exit( 0 ) if ( 0 == ARGV.size )
    
    	LogFactory.CreateApplicationConsoleLogTarget( )
    	g_Log = LogFactory.ApplicationLog    
    	g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )

	subject = g_Options.has_option?( 'subject' ) ? g_Options.get( 'subject' ) : "no subject was set"
	body = g_Options.has_option?( 'body' ) ? g_Options.get( 'body' ) : ""    
	recipients = g_Options.has_option?( 'recipients' ) ? g_Options.get( 'recipients' ) : nil
	
	if (recipients == nil)	
		g_Log.Warn("No recipients, no email will be sent")
		exit 1
	end
	
 	recipients = recipients.split(/[\s,;]/)

        g_Options.trailing.each do |more_body|
		body += "\n" + more_body
        end            
               	
       	g_Log.Message("Sending Email : {0} : {1}", subject, recipients.join(","))

       	recipients = recipients.to_clr( System::String )
       	attachments = [].to_clr( System::String )
       	
        RSG::Base::Configuration::Email::Email.SendEmail(g_Config, recipients, subject, body, attachments)
       	g_Log.Message("Sent")
       
        exit 0

    rescue SystemExit => sex

        LogFactory.ApplicationShutdown()
        exit( sex.status )   

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        if ( g_Options.show_popups? ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end
                
        exit( -1 )
    end
end

# email.rb
