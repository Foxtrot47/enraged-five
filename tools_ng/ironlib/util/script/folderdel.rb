#
# File:: folderdel.rb
# Description:: Temp script for script to delete SCO files from a directory.

# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 4 September 2013
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Windows.dll'
require 'System.Core'
require 'mscorlib'

require 'RSG.Base.Windows.dll'
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
require 'pipeline/os/options'
include Pipeline

include System::Collections::Generic
include System::IO
using_clr_extensions System::Linq


#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
    LongOption::new( 'input', LongOption::ArgType.Required, 's', 'input text file' )
]


#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then 
    
    g_Options = OS::Options::new( OPTIONS )    
    begin        
        retcode = 0
            
        if ( g_Options.is_enabled?( 'help' ) )
            puts "#{__FILE__}"
            puts "Usage:"
            puts g_Options.usage()
            exit( 1 )
        end
    
    	LogFactory.CreateApplicationConsoleLogTarget( )
    	g_Log = LogFactory.CreateUniversalLog("folderdel")
	    g_LogFile = LogFactory.CreateUniversalLogFile("folderdel", g_Log) # as UniversalLogFile
	
        input = g_Options.get( 'input' ) unless g_Options.get( 'input' ).nil?
        if ( not System::IO::File::Exists( input ) ) then
            g_Log.Error( "Input file '#{input}' does not exist.  Aborting." )
            exit 1
        end
        
        basenames = System::IO::File::ReadLines( input )
        basenames = basenames.Select( lambda { |basename| basename.Split( ' ' ).First() } ).to_a.to_clr( System::String )
     
        folders = []
        g_Options.trailing.each do |path|
            folders << System::IO::Path.GetFullPath( path )
        end
        folders = folders.to_clr( System::String )
        
        folders.each do |folder|
            files = System::IO::Directory::GetFiles( folder, '*.*' )
            files.each do |filename|
                
                basename = System::IO::Path::GetFileNameWithoutExtension( filename )
                if ( basenames.Contains( basename ) ) then
                    g_Log.Message( "Deleting: #{filename}." )
                    System::IO::File::Delete( filename )
                end
            end
        end

    rescue SystemExit => sex

        LogFactory.ApplicationShutdown()
        exit( sex.status )   

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        if ( g_Options.show_popups? ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end
                
        exit( -1 )
    end
end

# binarydiff.rb

