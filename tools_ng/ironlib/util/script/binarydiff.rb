#
# File:: binarydiff.rb
# Description:: Binary diff folders - used for script object file comparison.

# Author:: Derek Ward <derek@rockstarnorth.com>
# Date:: 18 March 2013
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'RSG.Base.Configuration.dll'
require 'RSG.Base.dll'
require 'System.Core'
require 'mscorlib'
require 'RSG.Pipeline.Core.dll'

require 'RSG.Base.Windows.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::Pipeline::Core
require 'pipeline/os/options'
include Pipeline

require 'fileutils'


#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
    LongOption::new( 'source_folder', LongOption::ArgType.Required, 's', 'source_folder' ),
    LongOption::new( 'compare_folder', LongOption::ArgType.Required, 'c', 'compare_folder' ),
    LongOption::new( 'wildcard', LongOption::ArgType.Required, 'w', 'wildcard' ),             
    LongOption::new( 'email', LongOption::ArgType.Required, 'e', 'email' ),
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then 

    puts "#{$0} #{$*}"
    hdr = "INFO_MSG: "
    
    g_Options = OS::Options::new( OPTIONS )
    
    begin
        
        retcode = 0
            
        if ( g_Options.is_enabled?( 'help' ) )
            puts "#{__FILE__}"
            puts "Usage:"
            puts g_Options.usage()
            exit( 1 )
        end
    
    	LogFactory.CreateApplicationConsoleLogTarget( )
    	g_Log = LogFactory.CreateUniversalLog("binarydiff")

    	g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )

       	source_folder = g_Options.get( 'source_folder' ) unless g_Options.get( 'source_folder' ).nil?
       	compare_folder = g_Options.get( 'compare_folder' ) unless g_Options.get( 'compare_folder' ).nil?
       	wildcard = g_Options.get( 'wildcard' ) unless g_Options.get( 'wildcard' ).nil?
        email =  g_Options.get( 'email' ) unless g_Options.get( 'email' ).nil?
        
	g_Log.Message("#{hdr} ")
	g_Log.Message("#{hdr}******************")
	g_Log.Message("#{hdr}*** BinaryDiff ***")
	g_Log.Message("#{hdr}******************")
	g_Log.Message("#{hdr} ")
	g_Log.Message("#{hdr}Source folder        : #{source_folder}")
	g_Log.Message("#{hdr}Compare folder       : #{compare_folder}")
    	g_Log.Message("#{hdr}Wildcard             : #{wildcard}");
    	g_Log.Message("#{hdr} ")             	      
       
        if (!File.directory? source_folder)
        	g_Log.Warning("directory doesnt exist #{source_folder}" )
        	exit ( 0 )
        end

        if (!File.directory? compare_folder)
        	g_Log.Warning("compare folder doesnt exist #{compare_folder}" )
        	exit ( 0 )
        end
               
        files = System::IO::Directory.GetFiles(source_folder, wildcard)
        if (files == nil || files.Length==0)
        	g_Log.Warning("no files found in #{source_folder}" )
        	exit ( 0 )
        end
        
        files_differ = []
        files_identical = []
        files_compared = []
        num_files = files.size
        i = 1
        g_Log.Message("#{hdr}------ Comparing #{num_files} files --------")	        
        files.each do |file|
        	filename = File.basename(file)
        	compare_with = "#{compare_folder}\\#{filename}"
        	               
        	if (File.exists?(compare_with))
        		#
        		# Does a filesize comnparison then (if required) reads streams in blocks until files differ.
        		# - that seems perfectly good and is not memory hungry.
        		#
	        	result = FileUtils.compare_file(file, compare_with)
	        	if (result)
		        	g_Log.Message("#{hdr}(#{i}/#{num_files}) Identical : #{File.basename(file)}")   
		        	files_identical << compare_with
	        	else
		        	g_Log.Message("#{hdr}(#{i}/#{num_files}) Differs   : #{File.basename(file)}")        	
		        	files_differ << compare_with
	        	end
	        	files_compared << compare_with
        	else
        		g_Log.Error("#{compare_with} does not exist.")
        	end
        	i=i+1
        end

    	g_Log.Message("#{hdr} ")
	g_Log.Message("#{hdr}------ Differing files --------")	
	files_differ.each do |file|
		g_Log.Message("#{hdr}#{File.basename(file)}")	
	end
        g_Log.Message("#{hdr}-------------------------------")
    	g_Log.Message("#{hdr} ")    	
	g_Log.Message("#{hdr}---------- Summary ------------")	
	g_Log.Message("#{hdr}Compared files  : #{files_compared.size}")	        	    
	g_Log.Message("#{hdr}Identical files : #{files_identical.size}")	        
	g_Log.Message("#{hdr}Differing files : #{files_differ.size}")	   	
        g_Log.Message("#{hdr}-------------------------------")
    	g_Log.Message("#{hdr} ")    	
    	
    	retcode = -1 if (files_differ.size>0)
    	
    	if (email=="true" && retcode == -1)
    		g_Log.Error("Flushing logfile with error, retcode is #{retcode}")
                LogFactory.FlushApplicationLog()
                
		subject = "Binary differences detected in #{source_folder}\\#{wildcard}"
		body = "See attached log file"		
		recipients = []
		#recipients << "*GTAScripting@rockstarnorth.com"
		recipients << "graeme.williamson@rockstarnorth.com"
		recipients << "ben.rollinson@rockstarnorth.com"
		recipients << "kenneth.ross@rockstarnorth.com"
		recipients << "robert.bray@rockstarnorth.com"
		recipients << "ross.wallace@rockstarnorth.com"
		recipients << "matthew.booton@rockstarnorth.com"
		recipients << "keith.mcleman@rockstarnorth.com"
		recipients << "robert.wright@rockstarnorth.com"
		recipients << "derek.ward@rockstarnorth.com"
		recipients << "liam.anderson@rockstarnorth.com"

		g_Log.Message("Sending Email : {0} : {1} with logfile {2}", subject, recipients.join(","), LogFactory.ApplicationLogFilename)

		recipients = recipients.to_clr( System::String )
		attachments = [System::Net::Mail::Attachment.new(LogFactory.ApplicationLogFilename)]
		attachments = attachments.to_clr( System::Net::Mail::Attachment )
		RSG::Base::Configuration::Email::Email.SendEmail(g_Config, recipients, subject, body, attachments)    	
	end    	
    	
       	g_Log.Message("#{hdr}Binary diff exiting with #{retcode}")
        exit retcode

    rescue SystemExit => sex

        LogFactory.ApplicationShutdown()
        exit( sex.status )   

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        if ( g_Options.show_popups? ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end
                
        exit( -1 )
    end
end

# binarydiff.rb
