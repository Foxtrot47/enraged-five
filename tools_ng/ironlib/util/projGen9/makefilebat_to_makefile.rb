#
# File:: %RS_TOOLSLIB%/util/makefile_bat_to_makefile.rb
# Description:: ironruby script that recursively scans for makefile.bat upon finding them produces a template
# .makefile which can be built by PG3
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 08th October 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'mscorlib'
require 'System.Core'
require 'pathname'

include System::IO
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
require 'pipeline/os/options'
include Pipeline

using_clr_extensions RSG::Base::Extensions

module MakefileBat2Makefile

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

# Order matters, slndefs should be converted after makefiles.
MAKEFILE_SEARCHES 	= [ "makefile.bat" ]
RECOGNISED_SETTINGS	= ["ARCHIVE", "FILES", "HEADONLY", "CUSTOM", "TESTERS", "LIBS", "SAMPLE_LIBS", "XPROJ" ] # there are more

AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'

OPTIONS = [ 
    LongOption::new( 'nonrecursive', LongOption::ArgType.None, 'd', 'Do not recurse.' ),
    LongOption::new( 'exclude', LongOption::ArgType.Required, 'x', 'Exclusion Regex' ) 
]   

class Utility
	#------------------------------------------------------------------
	# search routine
	def Utility::search_files( searches, list, searchOptions, exclusionRegex, log )
		searches.each do |search|
			files = Directory::GetFiles(Dir.pwd, search, searchOptions )
			files = files.collect { |f| File.expand_path(f) }

			excluded = 0
			if exclusionRegex # delete_if doesnt work!!!!			
				newfiles = []	
				files.each do |f|				
					if (f=~exclusionRegex)
						excluded += 1
						log.Message "    excluding #{f}"
					else
						newfiles << f
					end
				end
				files = newfiles
			end		

			list.concat files
			log.Message "- Discovered #{files.length} x #{search} : #{excluded} excluded" if (files.length>0)		
		end
	end
end

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
    # Initialise log and console output.
    g_Log = LogFactory.method(:CreateUniversal).of(UniversalLog).call( 'findMakefiles' )
    LogFactory.CreateApplicationConsoleLogTarget( )
        
    g_Options = OS::Options::new( OPTIONS )

    begin
                
        if ( g_Options.is_enabled?( 'help' ) )
            g_Log.Message "#{__FILE__}"
            g_Log.Message "Usage:"
            g_Log.Message g_Options.usage()
            exit( 1 )
        end
        
        g_Recurse = g_Options.get( 'nonrecursive' ) ? false : true
        searchOptions = g_Recurse ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly
        
        g_ExclusionRegex = nil
        
        if g_Options.has_option?( 'exclude' )
        	regex = g_Options.get( 'exclude' )
        	g_ExclusionRegex = Regexp.new( regex, 'i')
        	g_Log.Message " "
        	g_Log.Message "Excluding #{regex}"
        	g_Log.Message " "
        end

    	makefileBats = []

	#
	# Search recursively for specific files.
	#
	
	g_Log.Message " "
	g_Log.Message "--------------------- Recursive Directory Search --------------------" if g_Recurse
	g_Log.Message "------------------- Non Recursive Directory Search ------------------" unless g_Recurse
	g_Log.Message "  Searching within #{Dir.pwd}..."
	
	Utility::search_files(MAKEFILE_SEARCHES, makefileBats, searchOptions, g_ExclusionRegex, g_Log)
	
	g_Log.Message "---------------------------------------------------------------------"


	g_Log.Message " "
	g_Log.Message "------------------------ Read makefile.bats -------------------------"

	# Build hash (filename) of hash(makefile bat keys) of arrays(each setting)
	makefilebat_settings = {}

	makefileBats.each do |makefile_bat|

		makefilebat_settings[makefile_bat] = {}

		cur_hash = makefilebat_settings[makefile_bat]
		RECOGNISED_SETTINGS.each { |setting| cur_hash[setting] = [] }

		f = File.open(makefile_bat)
		lines = f.readlines
		lines.each do |line|
			if (/\s*set\s+(.*)\s*=\s*(.*)\s*/.match(line))
				key = $1
				val = $2
				vals = val.split(' ')
				if (cur_hash.has_key? key)
					RECOGNISED_SETTINGS.each do |rs|
						vals.delete_if { |v| v.include? rs } # we dont need these - assumes we are not redefining the setting entirely 
					end
				
					cur_hash[key].concat vals
				else
					g_Log.Warning("#{makefile_bat} : Unhandled makefilebat setting #{key}")
				end
			end
		end
	end
	g_Log.Message "---------------------------------------------------------------------"

	g_Log.Message " "
=begin	
	g_Log.Message "-------------------------- Display makefile.bats --------------------"
	makefilebat_settings.each_pair do |filename, settings|
		g_Log.Message(" #{filename}")
		
		settings.each_pair do |setting_key, setting_values|
			g_Log.Message ("    #{setting_key} : #{setting_values.join(' ')}") if setting_values.length > 0
		end
	end
	g_Log.Message "---------------------------------------------------------------------"
	g_Log.Message " "
=end

	g_Log.Message "-------------------------- Make new .makefiles ----------------------"
	makefilebat_settings.each_pair do |filename, settings|
		g_Log.Message("#{filename}")
		dir = File.dirname(filename)
		
		#
		# Worth considering is whether we run the makefile.bat in order to retrieve any environment settings - eg. %RAGE_CORE_LIBS%?
		# although that might be that straightforward since we want to keep some environment settings unexpanded.
		#
		
		settings["TESTERS"].each do |tester|
			makefile_filename = File.join(dir,tester+".makefile")
			g_Log.Message ("    #{makefile_filename}");
			
			if (File.exists?makefile_filename)
				g_Log.Warning("#{makefile_filename} exists - writing to #{makefile_filename}.test instead")
				makefile_filename = makefile_filename + ".test"
			end
			
			File.open(makefile_filename, 'w') do |f| 
				f.puts("Project #{tester}\n") 
				f.puts("ConfigurationType exe\n")
				f.puts("RootDirectory .\n") 
				f.puts("Files {") 
					settings["FILES"].each do |file|
						f.puts("\t#{file}")
					end
				f.puts("}") 

				if (settings["LIBS"].include?("%RAGE_CORE_LIBS%"))
					f.puts('Include %RAGE_DIR%/base/src/vcproj/ragecore/RageCore.libdefs') 
				end

				# Only do this if you want a solution for each makefile
				#f.puts('Include %RAGE_DIR%/base/src/vcproj/ragecore/RageCoreSample.slnTemplate') 				
			end
			
			#
			# Todo - add p4 edit?
			#
	#end
			
			
		end
	end

	g_Log.Message "---------------------------------------------------------------------"
	
	g_Log.Message " "

    rescue SystemExit => ex
        exit( ex.status )
    
    rescue Exception => ex
        ConsoleLog::new( )
        Log::Log__Error( "Unhandled error in makefilebat_to_makefile.rb" )
        Log::Log__Error( ex.backtrace.join("\n" ) + "\n" + ex.Message )
    end
end

end # module MakefileBat2Makefile