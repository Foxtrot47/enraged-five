#
# File:: %RS_TOOLSLIB%/util/data_asset_pack.rb
# Description:: Entry-point into our asset build pipeline for packing assets 
#               (no conversion).
#
# Author:: Gunnar Droege <gunnar.droege@rockstarnorth.com>
# Date:: 14/4/2014
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'mathn'
require 'RSG.Rage.dll'

include RSG::Rage::Entity

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

def IsOnLine(endPoint1, endPoint2, checkPoint)
	return	((checkPoint.y - endPoint1.y) / (checkPoint.x - endPoint1.x) ==
		     (endPoint2.y - endPoint1.y) / (endPoint2.x - endPoint1.x)) &&
			((checkPoint.y - endPoint1.y) / (checkPoint.z - endPoint1.z) ==
		     (endPoint2.y - endPoint1.y) / (endPoint2.z - endPoint1.z)) &&
			((checkPoint.x - endPoint1.x) / (checkPoint.z - endPoint1.z) ==
		     (endPoint2.x - endPoint1.x) / (endPoint2.z - endPoint1.z));
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------

file = File.new("output.txt", "w+")
if file==nil then
	puts "Counldn't create output file!"
end

foundProbs = false


ARGV.each do |arg|

	if !File.exists?(arg) then
		puts "File #{arg} does nae exist."
		next
	end

	myModel = Model::new("something", 0,255)
	myModel.LoadMesh arg
	
	file.puts "Loaded mesh #{arg}"

	myModel.Mesh.Mtls.each_with_index do |mtl, mtlIndex|
		mtl.Prims.each_with_index do |prim,primIndex|
			case prim.Type
				when RSG::Rage::Mesh::PrimType::mshTRIANGLES
					triangles = prim.Idx.each_slice(3).to_a
					triangles.each_with_index do |tri,triIndex|
						vert1 = mtl.Verts[tri[0]]
						vert2 = mtl.Verts[tri[1]]
						vert3 = mtl.Verts[tri[2]]
						if IsOnLine(vert1.Pos, vert2.Pos, vert3.Pos) then
							file.puts "Two vertices of triangle #{tri} at index #{triIndex} in material #{mtlIndex} are on a straight line:"
							file.puts "\t #{vert1.Pos}\n\t #{vert2.Pos}\n\t #{vert3.Pos}"
						end
					end
				else
					puts "Only supporting TRIANGLES type so far. this prim has #{prim.Type}"
					next
				end
		end
	end
end

if foundProbs then
	puts "Errors found, please check output.txt in the directory of this script."
else
	puts "No errors found."
end
