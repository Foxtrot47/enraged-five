#
# File:: %RS_TOOLSLIB%/util/test/test_shader_presets.rb
# Description:: Test shader presets.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 20 February 2013
#
# Usage:
#   %RS_TOOLSIR% test_shader_presets.rb
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.Pipeline.Services.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::Pipeline::Services

require 'mscorlib'
require 'System.Core'
include System::Collections::Generic
include System::IO
using_clr_extensions System::Linq

require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
]

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
    
	# Initialise log and console output.
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog
    g_Options = OS::Options::new( OPTIONS )
	begin

		presets = RSG::Pipeline::Services::Materials::ShaderPreset::Create( g_Options.command_options.Branch )
		presets.each do |kvp|
			puts "#{kvp.Key} => {"
			puts "\tName:        #{kvp.Value.Name}"
			puts "\tFilename:    #{kvp.Value.Filename}"
			puts "\tShader:      #{kvp.Value.ShaderName}"
			puts "\tDraw Bucket: #{kvp.Value.DrawBucket}"
			puts "}"
		end

    rescue SystemExit => ex
    
        LogFactory.ApplicationShutdown()
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        exit( 1 )
    end
end

