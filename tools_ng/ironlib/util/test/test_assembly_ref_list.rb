#
# File:: %RS_TOOLSLIB%/util/test/test_assembly_ref_list.rb
# Description:: List all assemblies referenced by another.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 3 July 2013
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
include RSG::Base
include RSG::Base::Logging
include RSG::Base::Logging::Universal

require 'mscorlib'
require 'System.Core'
include System::IO
include System::Reflection
using_clr_extensions System::Linq
using_clr_extensions RSG::Base::Extensions

require 'pipeline/os/options'
include Pipeline

OPTIONS = [ 
    LongOption::new( 'local_only', LongOption::ArgType.None, 'l', 
        'Only consider RSG.* assemblies.' ),
]

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    LogFactory.Initialize()
	g_Log = LogFactory.method(:CreateUniversal).of(UniversalLog).call( 'test_assembly_ref_list' )
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Options = OS::Options::new( OPTIONS )
    begin
        
		g_LocalOnly = g_Options.has_option?( 'local_only' ) ? true : false
        g_Options.trailing.each do |filename|
            
			g_Log.Message("#{filename} References: " )
			assembly = Assembly.ReflectionOnlyLoadFrom( filename )
			refs = assembly.GetReferencedAssemblies() unless ( g_LocalOnly )
			refs = assembly.GetReferencedAssemblies().Where( lambda { |a| a.Name.StartsWith( "RSG." ) } ) if ( g_LocalOnly )

			refs.each_with_index do |ref_assembly, index|
				g_Log.Message( "\t#{index} => #{ref_assembly.Name}" )
			end			
        end
                
    rescue SystemExit => ex
        exit( ex.status )    

    rescue Exception => ex
        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        unless ( g_Options.is_enabled?( 'nopopups' ) ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/test/test_assembly_ref_list.rb
