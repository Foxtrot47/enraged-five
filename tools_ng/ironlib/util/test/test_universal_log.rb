#
# File:: %RS_TOOLSLIB%/util/test/test_universal_log.rb
# Description:: Test Universal Log leak.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 9 November 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.Pipeline.Core.dll'
require 'RSG.Pipeline.Content.dll'
require 'RSG.Pipeline.Engine.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::Pipeline::Core
include RSG::Pipeline::Content
include RSG::Pipeline::Engine

require 'mscorlib'
require 'System.Core'
include System::Collections::Generic
include System::IO

require 'pipeline/monkey/array'
require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
def log_sublog()

    sublog = LogFactory.CreateUniversalLog( 'sublog' )
    sublogfile = LogFactory.CreateUniversalLogFile( sublog )
    
    500.times do
		sublog.ToolError( "Test internal-tool error" )
        sublog.Debug( "*** DEBUG TEST ***" )
		10.times do
            sublog.Message( "sublog blah info" * 40 )
        end
        sublog.Error( "sublog blah" * 50 )
    end
    
    LogFactory.CloseUniversalLogFile( sublogfile )
    LogFactory.CloseUniversalLog( sublog )
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog
    LogFactory.SetLogLevel( RSG::Base::Logging::LogLevel.Info )

    g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )
    begin
        200.times do 
            g_Log.Error( "Blah fucking blah" * 50 )
			g_Log.ToolError( "Test internal-tool error" )
            g_Log.Warning( "Blah fucking blah" * 50 )
            log_sublog()
        end
    
    rescue SystemExit => ex
        puts "Any key to continue.."
        $stdin.gets
        exit( ex.status )    

    rescue Exception => ex

        g_Log.ToolException( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        unless ( g_Options.is_enabled?( 'nopopups' ) ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/test/test_test_universal_log.rb
