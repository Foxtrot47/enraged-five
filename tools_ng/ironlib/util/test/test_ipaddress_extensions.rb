#
# File:: %RS_TOOLSLIB%/util/test/test_ipaddress_extensions.rb
# Description:: 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 23 November 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
include RSG::Base
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::Net

require 'mscorlib'
require 'System.Core'
include System::Net
include System::Net::Sockets
using_clr_extensions RSG::Base::Extensions

require 'pipeline/monkey/array'
require 'pipeline/os/options'
include Pipeline

OPTIONS = [ 
]

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.    
    g_Log = LogFactory.CreateUniversalLog( 'test_ipaddress_extensions' )
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Options = OS::Options::new( OPTIONS )
    begin
        ip = nil
        host = Dns.GetHostEntry( Dns.GetHostName() )
        host.AddressList.each do |ip|
            next unless ( AddressFamily.InterNetwork == ip.AddressFamily )
            
            puts "IP:     #{ip}"
            g_Options.command_options.Config.Studios.each do |studio|
                puts "Studio: #{studio.FriendlyName}"
            
                puts "\tSubnet Range: #{studio.SubnetRange}"
                        
                subnetAndMask = SubnetMask.Create(studio.SubnetRange)
                puts "\tSubnet:       #{subnetAndMask.Key}"
                puts "\tSubnet Mask:  #{subnetAndMask.Value}"
            
                puts "\tIs My Studio: #{ip.IsInSameSubnet(subnetAndMask.Key, subnetAndMask.Value)}"
            end
            puts "-" * 80
        end        
        
    rescue SystemExit => ex
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        unless ( g_Options.is_enabled?( 'nopopups' ) ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/test/test_exception_dialog.rb
