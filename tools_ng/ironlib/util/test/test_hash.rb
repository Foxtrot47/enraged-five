#
# File:: %RS_TOOLSLIB%/util/test/test_hash.rb
# Description:: 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 10 April 2013
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.ManagedRage.dll'
include RSG::Base
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::ManagedRage

require 'mscorlib'
require 'System.Core'
include System::IO
using_clr_extensions RSG::Base::Extensions

require 'pipeline/monkey/array'
require 'pipeline/os/options'
include Pipeline

OPTIONS = [ 
]

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    g_Log = LogFactory.method(:CreateUniversal).of(UniversalLog).call( 'test_hash' )
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Options = OS::Options::new( OPTIONS )
    begin
        
        mr_sw = System::Diagnostics::Stopwatch::new
        base_sw = System::Diagnostics::Stopwatch::new
        g_Options.trailing.each do |filename|
            
            sr = StreamReader::new( filename )
            while ( (line = sr.ReadLine()) != nil ) do
            
                mr_sw.Start()
                mr_atStringHash = StringHashUtil::atStringHash( line, 0 )
                mr_atHash16 = StringUtil::atHash16( line )
                mr_atHash16U = StringUtil::atHash16U( line )
                mr_sw.Stop()
                                
                base_sw.Start()
                base_atStringHash = RSG::Base::Security::Cryptography::OneAtATime::ComputeHash( line )
                base_atHash16 = RSG::Base::Security::Cryptography::Hash16::ComputeHash( line )
                base_atHash16U = RSG::Base::Security::Cryptography::Hash16U::ComputeHash( line )
                base_sw.Stop()
                
                g_Log.Error( "atStringHash mismatch '#{line}'; ManagedRage: #{mr_atStringHash}, Base: #{base_atStringHash}." ) \
                    unless ( mr_atStringHash == base_atStringHash )
                
                g_Log.Error( "atHash16 mismatch '#{line}'; ManagedRage: #{mr_atHash16}, Base: #{base_atHash16}." ) \
                    unless ( mr_atHash16 == base_atHash16 )

                g_Log.Error( "atHash16U mismatch '#{line}'; ManagedRage: #{mr_atHash16U}, Base: #{base_atHash16U}." ) \
                    unless ( mr_atHash16U == base_atHash16U )                
                
                puts line
                
            end
            sr.Close( )
        end
        
        g_Log.Message( "ManagedRage Time: #{mr_sw.ElapsedMilliseconds}ms" )
        g_Log.Message( "RSG.Base Time:    #{base_sw.ElapsedMilliseconds}ms" )
        
    rescue SystemExit => ex
        exit( ex.status )    

    rescue Exception => ex
        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        unless ( g_Options.is_enabled?( 'nopopups' ) ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/test/test_valid_email.rb
