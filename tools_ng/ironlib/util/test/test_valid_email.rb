#
# File:: %RS_TOOLSLIB%/util/test/test_ipaddress_extensions.rb
# Description:: 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 23 November 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Pipeline.Services.dll'
include RSG::Base
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Pipeline::Services

require 'mscorlib'
require 'System.Core'
include System::Net
include System::Net::Sockets
using_clr_extensions RSG::Base::Extensions

require 'pipeline/monkey/array'
require 'pipeline/os/options'
include Pipeline

OPTIONS = [ 
]

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    g_Log = LogFactory.method(:CreateUniversal).of(UniversalLog).call( 'test_valid_email' )
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Options = OS::Options::new( OPTIONS )
    begin
    	valid_email_address0 = "luke.openshaw@rockstarnorth.com"
    	valid_email_address1 = "*Tools@rockstarnorth.com"
    	invalid_email_address0 = "@rockstarnorth.com"
    	invalid_email_address1 = "#test@rockstarnorth.com"
        
        puts "#{valid_email_address0} is valid: #{Email.IsValidEmail(valid_email_address0)}"
        puts "#{valid_email_address1} is valid: #{Email.IsValidEmail(valid_email_address1)}"
        puts "#{invalid_email_address0} is valid: #{Email.IsValidEmail(invalid_email_address0)}"
        puts "#{invalid_email_address1} is valid: #{Email.IsValidEmail(invalid_email_address1)}"
        
        
    rescue SystemExit => ex
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        unless ( g_Options.is_enabled?( 'nopopups' ) ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/test/test_valid_email.rb
