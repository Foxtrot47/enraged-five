#
# File:: %RS_TOOLSLIB%/util/test/test_incredibuild_status.rb
# Description:: 
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 27 November 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Interop.Incredibuild.dll'
include RSG::Base
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Interop::Incredibuild

require 'mscorlib'
require 'System.Core'
using_clr_extensions RSG::Base::Extensions

require 'pipeline/monkey/array'
require 'pipeline/os/options'
include Pipeline

OPTIONS = [ 
]

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    g_Log = LogFactory.CreateUniversalLog( 'test_incredibuild_status' )
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Options = OS::Options::new( OPTIONS )
    begin
        g_Log.Message( "Incredibuild installed:  #{Incredibuild.IsInstalled}." )
        g_Log.Message( "Incredibuild version:    #{Incredibuild.Version}." )
        g_Log.Message( "Incredibuild installdir: #{Incredibuild.InstallDirectory}." )
        g_Log.Message( "Agent enabled:           #{Incredibuild.IsAgentEnabled}." )
        g_Log.Message( "Agent standalone mode:   #{Incredibuild.IsAgentStandalone}." )
        g_Log.Message( "Agent forced CPU count:  #{Incredibuild.AgentForceCPUCount}." )
        
    rescue SystemExit => ex
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        unless ( g_Options.is_enabled?( 'nopopups' ) ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/test/test_exception_dialog.rb
