#
# File:: %RS_TOOLSLIB%/util/test/test_exception_dialog.rb
# Description:: Exception dialog test script.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 9 September 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.Pipeline.Core.dll'
require 'RSG.Pipeline.Content.dll'
require 'RSG.Pipeline.Engine.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::Pipeline::Core
include RSG::Pipeline::Content
include RSG::Pipeline::Engine

require 'mscorlib'
require 'System.Core'
require 'PresentationFramework'
include System::Collections::Generic
include System::IO
include System::Windows

require 'pipeline/monkey/array'
require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------
# None

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.
    g_Log = LogFactory.method(:CreateUniversal).of(UniversalLog).call( 'test_exception_dialog' )
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Options = OS::Options::new( OPTIONS )
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            puts "#{__FILE__}"
            puts "Usage:"
            puts g_Options.usage()
            exit( 1 )
        end
                
        result = RSG::Base::Windows::Dialogs::CustomMessageBox::Show(
            "Test Message", "Test Caption",
            System::Windows::MessageBoxButton.YesNoCancel, System::Windows::MessageBoxImage.Asterisk,
            System::Windows::MessageBoxResult.Cancel, 3 
        )
        puts "RESULT: #{result}"
        
        ex1 = System::IO::FileNotFoundException::new( "c:\\test.xml" )
        ex2 = System::ArgumentException::new( "test_arg1" )  
        
        throw System::AggregateException::new( [ex1,ex2].to_clr( System::Exception ) )
    
    rescue SystemExit => ex
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        unless ( g_Options.is_enabled?( 'nopopups' ) ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/test/test_exception_dialog.rb
