#
# File:: %RS_TOOLSLIB%/util/test/test_bugstar_model.rb
# Description:: Bugstar model testing.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 10 December 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.Model.Bugstar.dll'
include RSG::Base
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Model::Bugstar
include RSG::Model::Bugstar::Organisation

require 'mscorlib'
require 'System.Core'
include System::Net
include System::Net::Sockets
using_clr_extensions System::Linq
using_clr_extensions RSG::Base::Extensions

require 'pipeline/monkey/array'
require 'pipeline/os/options'
include Pipeline

AUTHOR = 'RSGEDI Tools'
EMAIL = '*tools@rockstarnorth.com'
OPTIONS = [ 
]

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    # Initialise log and console output.    
    g_Log = LogFactory.CreateUniversalLog( __FILE__ )
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Options = OS::Options::new( OPTIONS )
    begin
        bugstarConfig = ConfigFactory.CreateBugstarConfig()
    
        bugstar = BugstarConnection::new()
        bugstar.Login( 'mapautoexport', 'rockstat1A', bugstarConfig.ExternalDomain )
    
        # Output the available Bugstar Projects.
        projects = Project.GetProjects( bugstar )
        projects.each do |project|
            g_Log.Message( "Project: #{project.Name}" )
        end
        
        # Get current Project.
        project = Project.GetProjectById( bugstar, bugstarConfig.ProjectId )
        g_Log.Message( "Current Project: #{project.Name}" )
        
        # Get current User.
        user = project.CurrentUser
        g_Log.Message( "Current User:" )
        g_Log.Message( "\tId:        #{user.Id}" )
        g_Log.Message( "\tUsername:  #{user.UserName}" )
        g_Log.Message( "\tJob Title: #{user.JobTitle}" )
        g_Log.Message( "\tEmail:     #{user.Email}" )
  
        # Create attachment.
        ab = AttachmentBuilder::new( project )
        attachment = ab.ToAttachment( bugstar, 'x:\\test.ulog' )
        g_Log.Message( "Attachment Id: #{attachment.Id}" )
        
        # Create Bug test.
        bugbuilder = BugBuilder::new( project )
        bugbuilder.Summary = 'Test bug summary'
        bugbuilder.Description = 'Test bug description'
        bugbuilder.DeveloperId = user.Id
        bugbuilder.TesterId = user.Id
        bugbuilder.Tags.Add( 'Auto Map Export' )
        bugbuilder.Attachments.Add( attachment.Id )
        bug = bugbuilder.ToBug( bugstar )
        
        # Display new bug info.
        g_Log.Message( "New Bug:" ) 
        g_Log.Message( "\tId:          #{bug.Id}" )
        g_Log.Message( "\tSummary:     #{bug.Summary}" )
        g_Log.Message( "\tDescription: #{bug.Description}" )
        g_Log.Message( "\tPriority:    #{bug.Priority}" )   
        g_Log.Message( "\tAttachments: #{bug.AttachmentList.Count()}" )   
        
=begin        
        
        # Get bug by ID
        bug1 = Bug.GetBugById( project, 971904 )
        # Display bug info.
        g_Log.Message( "Bug:" ) 
        g_Log.Message( "\tId:          #{bug1.Id}" )
        g_Log.Message( "\tSummary:     #{bug1.Summary}" )
        g_Log.Message( "\tDescription: #{bug1.Description}" )
        g_Log.Message( "\tPriority:    #{bug1.Priority}" )
        g_Log.Message( "\tComments: " )
        bug1.Comments.each do |comment|
            g_Log.Message( "\t\t#{comment.Text}" )
        end
=end
        
    rescue SystemExit => ex
        exit( ex.status )    

    rescue Exception => ex

        g_Log.Exception( ex, "Exception during #{__FILE__}." )

        puts "Exception during #{__FILE__}: #{ex.message}"
        puts ex.backtrace.join("\n")
        
        unless ( g_Options.is_enabled?( 'nopopups' ) ) then
            dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
                g_Options.command_options.Config.EmailAddress, AUTHOR, EMAIL )
            dlg.ShowDialog( )
        end
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/test/test_bugstar_model.rb
