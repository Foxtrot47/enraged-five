#
# File:: %RS_TOOLSLIB%/Util/anim/vehicles/convert_data_ap2_to_ap3.rb
# Description:: 
#
# Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
# Date:: 13 January 2013
#
#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::SourceControl::Perforce

require 'mscorlib'
require 'System.Core'
require 'System.Xml'
include System::Collections::Generic
include System::IO
include System::Diagnostics
include System::Xml

require 'pipeline/os/options'
require 'pipeline/monkey/array'

include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [ 
    LongOption::new( 'srcdir', LongOption::ArgType.Required, 's', 'Source directory.' ), 
    LongOption::new( 'dstdir', LongOption::ArgType.Required, 'd', 'Destination directory.' ), 
    LongOption::new( 'checkout', LongOption::ArgType.None, 'c', 'Checkout data from p4.' ), 
]

#-----------------------------------------------------------------------------
# Methods
#-----------------------------------------------------------------------------
def generate_master_icd_list( master_list_filename, vehicle_list, src_dir, dest_dir )
	System::IO::File.Delete(master_list_filename) if System::IO::File.Exists(master_list_filename)
	
	xml_writer_settings = XmlWriterSettings.new()
	xml_writer_settings.Indent = true
	xml_writer_settings.IndentChars = "\t"

	xml_writer = XmlWriter.Create(master_list_filename, xml_writer_settings)
	xml_writer.WriteStartDocument()
	xml_writer.WriteStartElement("Assets")
	#vehicles_assets = []
	vehicle_list.each do | vehicle_name |
		vehicles_assets = System::Collections::Generic::List[System::String].new(Directory.GetFiles( src_dir, vehicle_name + ".*"))
		
		hi_frag_dict = src_dir + vehicle_name + "_hi.ift.zip"
		vehicles_assets << hi_frag_dict if System::IO::File.Exists(hi_frag_dict)
		
		clip_dict = src_dir + "va_" + vehicle_name + ".icd.zip" 
		vehicles_assets << clip_dict if System::IO::File.Exists(clip_dict)

		dictionary_file = Path.Combine(dest_dir, vehicle_name + ".zip")
		System::IO::File.Delete(dictionary_file) if System::IO::File.Exists(dictionary_file)
		
		xml_writer.WriteStartElement("ZipArchive")
		xml_writer.WriteAttributeString("path", dictionary_file)
		
		vehicles_assets.each do | asset | 
			xml_writer.WriteStartElement("File")
			xml_writer.WriteAttributeString("path", asset)
			xml_writer.WriteAttributeString("destination", "sp/" + File.basename(asset))
			xml_writer.WriteEndElement()
		end
		xml_writer.WriteEndElement()
		#dictionary_name_list << dictionary_file
	end
	xml_writer.WriteEndDocument()
	xml_writer.Close()
	
end


def run( config, src_dir, dst_dir, checkout, log )
	
	src_dir = config.Project.DefaultBranch.Environment.Subst(src_dir)
	dest_dir = config.Project.DefaultBranch.Environment.Subst(dst_dir)
	master_list_filename = Path.Combine(src_dir, "master_icd_list.xml")
	
	# Start by building a list of all vehicles
	vehicle_list = []
	#all_src_files = Directory.GetFiles( src_dir, "*.itd.zip")
	#all_src_files.each do | file |
	#	vehicle_list << File.basename(file, ".itd.zip")
	#	puts File.basename(file)
	#end
	
	all_src_files = Directory.GetFiles( src_dir, "*.itd.zip")
		all_src_files.each do | file |
			vehicle_name = File.basename(file, ".itd.zip")
			vehicle_list << vehicle_name if vehicle_name[vehicle_name.length-3, vehicle_name.length-1] != '_hi'
			#puts vehicle_name[vehicle_name.length-3, vehicle_name.length-1] == '_hi'
			#puts File.basename(file)
	end

	puts master_list_filename
	
	generate_master_icd_list(master_list_filename, vehicle_list, src_dir, dest_dir)
	log.Message("Zipping clip dictionaries...")
	ir_exe = config.ToolsIronRuby
	args ="$(toolsroot)/ironlib/util/data_asset_pack.rb"
	args = args + " --rebuild"
	args = args + " #{master_list_filename}"

	ir_exe = config.Environment.Subst(ir_exe)
	args = config.Environment.Subst(args)
	log.Message("Asset pack command #{ir_exe} #{args}")
	startInfo = ProcessStartInfo::new()
	startInfo.Arguments = args
	startInfo.FileName = ir_exe
	startInfo.UseShellExecute = false

	proc = System::Diagnostics::Process.Start(startInfo)
	proc.WaitForExit()
	exit_code = proc.ExitCode
	log.Error("Errors zipping animations, see log") if (exit_code != 0)

end

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------	
if ( __FILE__ == $0 ) then
	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------
	LogFactory.CreateApplicationConsoleLogTarget( )
    	g_Log = LogFactory.ApplicationLog
	begin
	
		g_Options = OS::Options::new( OPTIONS )
		
		g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )
		g_SrcDir = ( g_Options.get( 'srcdir' ) )
		g_DestDir = (  g_Options.get( 'dstdir' ) )
		g_Checkout = ( g_Options.is_enabled?( 'checkout' ) )
		
		# Initialise log and console output.
		
		
		run( g_Config, g_SrcDir, g_DestDir, g_Checkout, g_Log )
		LogFactory.ApplicationShutdown()

	rescue Exception => ex
		g_Log.Exception( ex, "Exception during #{__FILE__}: #{ex.message}")
        	
        	LogFactory.ApplicationShutdown()
	end
end