#
# File:: %RS_TOOLSLIB%/util/data_is_platform_enabled.rb
# Description:: Set process exit code based on whether the specified platform 
#               is enabled for the current project.
#
# Return Codes: 0 - platform(s) not enabled
#               1 - platform(s) enabled
#               2 - error
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 22 June 2012
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal

require 'mscorlib'
require 'System.Core'
include System::Collections::Generic
include System::IO

require 'pipeline/os/options'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [
]
EXIT_DISABLED = 0
EXIT_ENABLED = 1
EXIT_ERROR = 2

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

	# Initialise log and console output.
    g_Log = LogFactory.method(:CreateUniversal).of(UniversalLog).call( 'data_convert_file' )
	LogFactory.CreateApplicationConsoleLogTarget( )		
	g_Options = OS::Options::new( OPTIONS )	
	begin
        if ( g_Options.is_enabled?( 'help' ) )
            puts "#{__FILE__}"
            puts "Usage:"
            puts g_Options.usage()
            exit( EXIT_ERROR )
        end
        
        g_Project = g_Options.command_options.Project
        g_BranchName = g_Options.has_option?( 'branch' ) ? g_Options.get( 'branch' ) : g_Project.DefaultBranchName
        g_Branch = g_Project.Branches[g_BranchName] if ( g_Project.Branches.ContainsKey( g_BranchName ) )
        if ( g_Branch.nil? ) then
            g_Log.Error( "Invalid branch '#{g_BranchName}' for project '#{g_Project.UIName}'." )
            exit( 1 )
        end
		
        if ( 0 == g_Options.trailing.size ) then
            g_Log.Error( "No platforms specifed.  Aborting." )
            exit( EXIT_ERROR )
        end
  
        # Loop through our targets determining whether they are all enabled
        # from our configuration data.
        g_Enabled = true
        g_Options.trailing.each do |trailing|
            
            platform = trailing.downcase
            g_Branch.Targets.each do |k|
                target_key = k.Key.to_s.downcase 
                next unless ( 0 == platform.casecmp( target_key ) )
            
                g_Enabled &= k.Value.Enabled
            end
        end
  
		exit( EXIT_DISABLED ) unless ( g_Enabled )
		exit( EXIT_ENABLED ) if ( g_Enabled )

    rescue SystemExit => ex
        exit( ex.status )
    
	rescue Exception => ex
		puts "\n#{__FILE__} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")  
	end
end

# %RS_TOOLSLIB%/util/data_is_platform_enabled.rb
