#
# File:: %RS_TOOLSLIB%/util/tools_release/update_testing_label.rb
# Description:: Updates the testing and latest version label with the latest files.
#
# Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
# Date:: 10 October 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::SourceControl::Perforce

require 'mscorlib'
require 'System.Core'
using_clr_extensions System::Linq
include System

require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
    LongOption::new( 'point-release', LongOption::ArgType.None, 'p', 
        'Whether we are updating a label point release.' )
]

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
    # Initialise log and console output.
	LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog;
    g_Options = OS::Options::new( OPTIONS )
    
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            puts "#{__FILE__}"
            puts "Usage:"
            puts g_Options.usage()
            exit( 1 )
        end
        
        # Create the config
        g_Config = g_Options.command_options.Config
        
        # Perforce initialisation.
        g_P4 = P4::new( )
        g_P4.CallingProgram = __FILE__
        g_P4.Connect( )
        
        # Patch the appropriate label
        result = true
        if ( g_Options.is_enabled?( 'point-release' ) )
            result = RSG::Base::Configuration::Util::ToolsRelease::UpdateCurrentPatchTestingLabel( g_Config, g_P4 )
        else
            result = RSG::Base::Configuration::Util::ToolsRelease::UpdateCurrentTestingLabel( g_Config, g_P4 )
        end
        
        exit( result ? 0 : 1 )
    
    rescue SystemExit => ex
        exit( ex.status )
    
    rescue Exception => ex
        ConsoleLog::new( )
        Log::Log__Error( "Unhandled error." )
        Log::Log__Error( ex.backtrace.join("\n" ) )
        
        dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
            g_Config.EmailAddress, AUTHOR, EMAIL )
        dlg.ShowDialog( )
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/update_testing_label.rb
