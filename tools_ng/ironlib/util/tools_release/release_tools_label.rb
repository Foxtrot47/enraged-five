#
# File:: %RS_TOOLSLIB%/util/tools_release/release_tools_label.rb
# Description:: Releases a particular tools label to the "public".
#
# Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
# Date:: 07 March 2013
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::Base::Windows
include RSG::SourceControl::Perforce

require 'mscorlib'
require 'System.Core'
using_clr_extensions System::Linq
include System

require 'pipeline/os/options'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
AUTHOR = 'RSGEDI Tools'     
EMAIL = 'RSGEDI Tools <*tools@rockstarnorth.com>'
OPTIONS = [ 
    LongOption::new( 'version', LongOption::ArgType.Required, 'v', 
        'Version of the tools to release.' )
]

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
    # Initialise log and console output.
	LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog;
    g_Options = OS::Options::new( OPTIONS )
    
    begin
        if ( g_Options.is_enabled?( 'help' ) )
            puts "#{__FILE__}"
            puts "Usage:"
            puts g_Options.usage()
            exit( 1 )
        end
        
        # Create the config
        g_Config = g_Options.command_options.Config
        g_VersionNumber = g_Options.has_option?( 'version' ) ? g_Options.get( 'version' ) : nil
        if ( g_VersionNumber == nil ) then
            g_Log.Error( "No version supplied." )
            exit( 1 )
        end
        
        # Perforce initialisation.
        g_P4 = P4::new( )
        g_P4.CallingProgram = __FILE__
        g_P4.Connect( )
        
        # Patch the appropriate label
        result = RSG::Base::Configuration::Util::ToolsRelease::ReleaseToolsLabel( g_Config, g_P4, g_VersionNumber )
        exit( result ? 0 : 1 )
    
    rescue SystemExit => ex
        exit( ex.status )
    
    rescue Exception => ex
        ConsoleLog::new( )
        Log::Log__Error( "Unhandled error." )
        Log::Log__Error( ex.backtrace.join("\n" ) )
        
        dlg = RSG::Base::Windows::ExceptionStackTraceDlg::new( ex, 
            g_Config.EmailAddress, AUTHOR, EMAIL )
        dlg.ShowDialog( )
        
        exit( 1 )
    end
end

# %RS_TOOLSLIB%/util/release_tools_label.rb
