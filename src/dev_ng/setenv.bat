@echo off
pushd %~dp0

set PARENT_DIR=%~dp0
set PROPS_EXE=
set PROPERTY_SHEET_VIEWER=tools_ng\bin\PropertySheetViewer.exe

call :GetExecutable PROPS_EXE %PROPERTY_SHEET_VIEWER%
if NOT exist "%PROPS_EXE%" (
	echo Couldn't find %PROPERTY_SHEET_VIEWER% to generate environment
	exit /b 1
)

set exe_args=
set EndFunc=ShowEnv

:ProcessArgs
if [%1] == [] goto SetVars
if [%1] == [noreport] set EndFunc=success && shift /1 && goto :ProcessArgs
if [%1] == [/D] (
	echo overriding %2
	set exe_args=%exe_args% %1 %2
	shift /1
) else (
	set exe_args=%exe_args% %1
)

shift /1
goto :ProcessArgs

:SetVars
for /F "tokens=*" %%i in ('%PROPS_EXE% dumpenvironment %exe_args%') do set %%i

rem echo @echo off > %TMP_BATCH%
rem set TMP_BATCH=%TEMP%\setenv_tmp.bat
rem if exist %TMP_BATCH% del /f /q %TMP_BATCH%
rem if NOT "%ERRORLEVEL%" == "0" echo Failed to create temp batch file && goto :failed
rem %PROPS_EXE% DumpEnvironment | for /F "tokens=*" %%i in ('findstr "="') do echo set %%i >> %TMP_BATCH%
rem if exist %TMP_BATCH% call %TMP_BATCH% && del /f /q %TMP_BATCH%

if NOT %ERRORLEVEL% == 0 goto :failed
goto %EndFunc%

:ShowEnv
ECHO RS_BUILDBRANCH:		%RS_BUILDBRANCH%
ECHO RS_CODEBRANCH:		%RS_CODEBRANCH%
ECHO RS_PROJECT:		%RS_PROJECT%
ECHO RS_PROJROOT:		%RS_PROJROOT%
ECHO RS_SCRIPTBRANCH:	%RS_SCRIPTBRANCH%
ECHO RS_TITLE_UPDATE_NG:	%RS_TITLE_UPDATE_NG%

ECHO RS_BUILDROOT:		%RS_BUILDROOT%
ECHO RS_ASSETS:		%RS_ASSETS%
ECHO RS_EXPORT:		%RS_EXPORT%
ECHO RS_PROCESSED:		%RS_PROCESSED%

ECHO RS_TOOLSROOT:		%RS_TOOLSROOT%
ECHO RS_TOOLSBIN:		%RS_TOOLSBIN%
ECHO RS_TOOLSDCC:		%RS_TOOLSDCC%
ECHO RS_TOOLSCONFIG:		%RS_TOOLSCONFIG%
ECHO RS_TOOLSLIB:		%RS_TOOLSLIB%
ECHO RS_TOOLSSRC:		%RS_TOOLSSRC%
ECHO RS_TOOLSRUBY:		%RS_TOOLSRUBY%
ECHO RS_TOOLSIR:		%RS_TOOLSIR%
ECHO RS_TOOLSPYTHON:		%RS_TOOLSPYTHON%

ECHO RS_TOOLSCONVERT:	%RS_TOOLSCONVERT%

ECHO RAGE_DIR:		%RAGE_DIR%
ECHO RAGE_3RDPARTY:		%RAGE_3RDPARTY%
ECHO RAGE_CRASHDUMP_DIR:	%RAGE_CRASHDUMP_DIR%

ECHO RS_TITLE_UPDATE:	%RS_TITLE_UPDATE%

:success
title %RS_BRANCHNAME%
popd
exit /b 0

:failed
echo Failed to set environment variables
popd
exit /b 1

:GetExecutable
set LAST_PARENT=%PARENT_DIR%
if exist %PARENT_DIR%\%2 (
	set %~1=%PARENT_DIR%\%2
	exit /b 0
)
call :GetParent PARENT_DIR %PARENT_DIR%\..
if NOT "%PARENT_DIR%" == "%LAST_PARENT%" goto GetExecutable
exit /b 1

:GetParent
set %~1=%~f2
exit /b 0

