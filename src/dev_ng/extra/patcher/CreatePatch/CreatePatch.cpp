#pragma warning(disable: 4668)
#include "atl/map.h"
#include "data/bitbuffer.h"
#include "data/sha1.h"
#include "diag/channel.h"
#include "diag/seh.h"
#include "file/asset.h"
#include "file/device.h"
#include "system/param.h"
#include "system/timer.h"
#include "system/xtl.h"
#include <time.h>

using namespace rage;

// void* operator new[](size_t size,size_t align)
// {
// 	return NULL;
// }

#if !__DEV
#error CreatePatch should only be compiled with __DEV on since all errors are reported using Asserts
#endif

class App
{
public:

	App()
	: m_Progress(0.0f)
	, m_TotalBytesToProcess(0)
	, m_NumBytesProcessed(0)
	, m_LogFileHandle(fiHandleInvalid)
	, m_LogFileDevice(NULL)
	{
		m_ScratchBuffer[0] = rage_new u8[SIZE_OF_SCRATCH_BUFFER];
		Assert(m_ScratchBuffer[0]);

		m_ScratchBuffer[1] = rage_new u8[SIZE_OF_SCRATCH_BUFFER];
		Assert(m_ScratchBuffer[1]);

		m_FileMetadataMap.Reset();
		m_OriginalFilesMap.Reset();		
	}

	~App()
	{
		if(m_ScratchBuffer[0] != NULL)
		{
			delete [] m_ScratchBuffer[0];
		}

		if(m_ScratchBuffer[1] != NULL)
		{
			delete [] m_ScratchBuffer[1];
		}

		m_FileMetadataMap.Kill();
		m_OriginalFilesMap.Kill();

		if(m_LogFileDevice && fiIsValidHandle(m_LogFileHandle))
		{
			m_LogFileDevice->Close(m_LogFileHandle);
			m_LogFileDevice = NULL;
			m_LogFileHandle = fiHandleInvalid;
		}
	}

	void AddBytesProcessed(u64 bytesProcessed)
	{
		if(bytesProcessed == 0)
		{
			return;
		}

		m_NumBytesProcessed += bytesProcessed;

		float progress = (float)((double)m_NumBytesProcessed / (double)m_TotalBytesToProcess);
		Assert((progress >= 0.0f) && (progress <= 1.0f));
		if((m_NumBytesProcessed == m_TotalBytesToProcess) || ((progress - m_Progress) > 0.0001f))
		{
			Displayf("Progress: %.2f%%", progress * 100.0f);

			u64 numKilobytesProcessed = m_NumBytesProcessed >> 10;
			Displayf("\tKilobytes processed: %I64u / %I64u", numKilobytesProcessed, m_TotalBytesToProcess >> 10);

			u32 elapsedSeconds = (u32)m_Timer.GetTime();
			u32 elapsedMins = (u32)(elapsedSeconds / 60);
			u32 elapsedHours = (u32)(elapsedMins / 60);
			elapsedSeconds -= elapsedMins * 60;
			elapsedMins -= elapsedHours * 60;
			Displayf("\tElapsed time: %02d:%02d:%02d", elapsedHours, elapsedMins, elapsedSeconds);
			Displayf("\tSpeed: %d KB/sec", (int)(numKilobytesProcessed / m_Timer.GetTime()));

			m_Progress = progress;
		}
	}

	void OutputStringToLog(const char *fmt, ...)
	{
		rtry
		{
			char message[4096];
			int len = 0;
			char* msg = &message[0];

			bool hasFmt = (fmt != NULL) && (fmt[0] != '\0');

			if(hasFmt)
			{
				va_list args;
				va_start(args, fmt);
				_vsnprintf_s(msg, sizeof(message) - len - 1, sizeof(message) - len - 1, fmt, args);
				va_end(args); 
			}

			Displayf(message);

			if(fiIsValidHandle(m_LogFileHandle) == false)
			{
				char logFilePath[RAGE_MAX_PATH] = {0};

				char *sysDrive = getenv("SystemDrive");
				rcheck(sysDrive, catchall, );

				formatf(logFilePath, "%s\\CreatePatchLogs\\log.txt", sysDrive);

				AssertVerify(ASSET.CreateLeadingPath(logFilePath));

				m_LogFileDevice = fiDevice::GetDevice(logFilePath, false);
				rcheck(m_LogFileDevice, catchall, );

				bool newFile = ASSET.Exists(logFilePath, "") == false;				

				if(newFile)
				{
					m_LogFileHandle = m_LogFileDevice->Create(logFilePath);
				}
				else
				{
					m_LogFileHandle = m_LogFileDevice->Open(logFilePath, false);
					m_LogFileDevice->Seek64(m_LogFileHandle, 0, seekEnd);
				}
			}

			rcheck(m_LogFileDevice && fiIsValidHandle(m_LogFileHandle), catchall, );

			const char* newline = "\r\n";
			m_LogFileDevice->Write(m_LogFileHandle, message, strlen(message));
			m_LogFileDevice->Write(m_LogFileHandle, newline, strlen(newline));
		}
		rcatchall
		{
		}
	}

	bool GetProcessExePath(char (&path)[RAGE_MAX_PATH]) const
	{
		bool success = false;
		path[0] = '\0';

		if(AssertVerify(GetModuleFileNameA(NULL, path, sizeof(path)) != 0))
		{
			success = true;
		}

		return success;
	}

	bool GetProcessExeDirectory(char (&path)[RAGE_MAX_PATH]) const
	{
		bool success = false;
		path[0] = '\0';

		if(AssertVerify(GetProcessExePath(path)))
		{
			char* lastSlash = strrchr(path, '\\');
			if(lastSlash == NULL)
			{
				lastSlash = strrchr(path, '/');
			}

			if(AssertVerify(lastSlash != NULL))
			{
				lastSlash[1] = '\0';
			}

			success = true;
		}

		return success;
	}

	bool XDelta(const char* originalFilePath, const char* targetFilePath, const char* outputPath)
	{
		bool success = false;

		char processDirectory[RAGE_MAX_PATH] = {0};
		if(AssertVerify(GetProcessExeDirectory(processDirectory)))
		{
			STARTUPINFOA si;
			PROCESS_INFORMATION pi;

			ZeroMemory(&si, sizeof(si));
			si.cb = sizeof(si);
			ZeroMemory(&pi, sizeof(pi));

			char xdeltaPath[RAGE_MAX_PATH] = {0};
			formatf(xdeltaPath, "%sxdelta3.exe", processDirectory);

			char commandLine[8192] = {0};
			formatf(commandLine, "\"%s\" -A -e -s \"%s\" \"%s\" \"%s\"", xdeltaPath, originalFilePath, targetFilePath, outputPath);

			if(0 != ::CreateProcessA(xdeltaPath,
									 commandLine,
									 NULL,
									 NULL,
									 FALSE,
									 0,
									 NULL,
									 NULL,
									 &si,
									 &pi))
			{
				::WaitForSingleObject(pi.hProcess, INFINITE);

				DWORD exitCode = 0;
				if(AssertVerify(GetExitCodeProcess(pi.hProcess, &exitCode)))
				{
					if(AssertVerify(exitCode == 0))
					{
						success = true;
					}
				}

				CloseHandle(pi.hProcess);
				CloseHandle(pi.hThread);
			}
			else
			{
				Assertf(false, "Failed to run xdelta process");
				CloseHandle(pi.hProcess);
				CloseHandle(pi.hThread);
			}
		}
		return success;
	}

	bool AreFilesIdentical(const char* filename1, const char* filename2)
	{
		Assert(filename1 && (filename1[0] != '\0'));
		Assert(filename2 && (filename2[0] != '\0'));
		Assert(strcmp(filename1, filename2) != 0);

		bool identical = true;

		const fiDevice *device[2] = {fiDevice::GetDevice(filename1, true), fiDevice::GetDevice(filename2, true)};

		if(AssertVerify(device[0]) &&
		   AssertVerify(device[1]))
		{
			fiHandle hFile[2] = {device[0]->Open(filename1, true), device[1]->Open(filename2, true)};

 			if(AssertVerify(fiIsValidHandle(hFile[0])) &&
			   AssertVerify(fiIsValidHandle(hFile[1])))
 			{
				u64 fileSize[2] = {device[0]->Size64(hFile[0]), device[1]->Size64(hFile[1])};

				if(fileSize[0] != fileSize[1])
				{
					identical = false;
				}
				else if((fileSize[0] == 0) && (fileSize[1] == 0))
				{
					// both files are empty
					identical = true;
				}
				else
				{
					u64 bytesRemaining = fileSize[0];
					u64 totalBytesRead = 0;

					while(bytesRemaining > 0)
					{
						u64 numBytes = SIZE_OF_SCRATCH_BUFFER;
						if(numBytes > bytesRemaining)
						{
							numBytes = bytesRemaining;
						}

						u64 bytesRead[2] = {device[0]->Read(hFile[0], m_ScratchBuffer[0], (int)numBytes),
											device[1]->Read(hFile[1], m_ScratchBuffer[1], (int)numBytes)};
			 
 						Assert(bytesRead[0] == numBytes);
						Assert(bytesRead[1] == numBytes);
						totalBytesRead += numBytes;
						bytesRemaining -= numBytes;
						Assert(bytesRemaining >= 0);

						if(memcmp(m_ScratchBuffer[0], m_ScratchBuffer[1], (int)numBytes) != 0)
						{
							identical = false;
							break;
						}
					}

					// if the files are identical, make sure we've read the entire file
					Assert((identical == false) || (totalBytesRead == fileSize[0]));
				}

				device[0]->Close(hFile[0]);
				device[0]->Close(hFile[1]);
 			}
		}

		//Displayf("AreFilesIdentical(%s, %s) returning %s", filename1, filename2, identical ? "true" : "false");
		return identical;
	}

	bool ChecksumFile(const char* filename, u8 (&checksum)[Sha1::SHA1_DIGEST_LENGTH])
	{
		Assert(filename && (filename[0] != '\0'));
		memset(checksum, 0, sizeof(checksum));

		bool success = false;
		Sha1 sha1;

		const fiDevice *device = fiDevice::GetDevice(filename, true);

		if(AssertVerify(device))
		{
			fiHandle hFile = device->Open(filename, true);

			if(AssertVerify(fiIsValidHandle(hFile)))
			{
				u64 fileSize = device->Size64(hFile);

				if(fileSize == 0)
				{
					success = true;
				}
				else
				{
					u64 bytesRemaining = fileSize;
					u64 totalBytesRead = 0;

					while(bytesRemaining > 0)
					{
						u64 numBytes = SIZE_OF_SCRATCH_BUFFER;
						if(numBytes > bytesRemaining)
						{
							numBytes = bytesRemaining;
						}

						u64 bytesRead = device->Read(hFile, m_ScratchBuffer[0], (int)numBytes);

						Assert(bytesRead == numBytes);
						totalBytesRead += numBytes;
						bytesRemaining -= numBytes;
						Assert(bytesRemaining >= 0);

						sha1.Update(m_ScratchBuffer[0], (u32)numBytes);
					}

					Assert(totalBytesRead == fileSize);

					sha1.Final(checksum);
				}

				device->Close(hFile);
				success = true;
			}
		}

		return success;
	}

	bool DeleteSingleFile(const char* file)
	{
		if(AssertVerify(file && file[0] != '\0'))
		{
			const fiDevice *dev = fiDevice::GetDevice(file, true);
			if(AssertVerify(dev))
			{
				if(dev->GetAttributes(file) != FILE_ATTRIBUTE_INVALID)
				{
					if(!dev->Delete(file))
					{
						dev->SetAttributes(file, dev->GetAttributes(file) & ~FILE_ATTRIBUTE_READONLY);
						return AssertVerify(dev->Delete(file));
					}
					else
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	bool ProcessFile(const char* path)
	{
		char relativeFilePath[RAGE_MAX_PATH] = {0};
		formatf(relativeFilePath, "%s", &path[strlen(m_CurrentDirectory) + 1]);

		char originalFilePath[RAGE_MAX_PATH] = {0};
		formatf(originalFilePath, "%s\\%s", m_OriginalDirectory, relativeFilePath);

		char outputFilePath[RAGE_MAX_PATH] = {0};
		formatf(outputFilePath, "%s\\%s", m_PatchDataOutputDirectory, relativeFilePath);

		const fiDevice *dev = fiDevice::GetDevice(path);
		Assert(dev);
		u64 fileSize = dev->GetFileSize(path);

		u8 checksum1[Sha1::SHA1_DIGEST_LENGTH] = {0};
		u8 checksum2[Sha1::SHA1_DIGEST_LENGTH] = {0};

		bool newFile = ASSET.Exists(originalFilePath, "") == false;

		bool overlay = false;
		if(newFile && (fileSize > 0))
		{
			// detect overlays
			bool done = false;
			const char* pathToSearch = path;
			u64 smallestDelta = _UI64_MAX;
			//Displayf("'%s' is potentially a new file. Will look for moves/copies/overlays first...", path);

			do
			{
				pathToSearch = strchr(pathToSearch, '\\');

				if(pathToSearch != NULL)
				{
					for(OriginalFileMapIterator it = m_OriginalFilesMap.CreateIterator(); it; ++it)
					{
						const OriginalFile& file = it.GetData();
						const char* originalPath = it.GetKey();
						bool match = strstr(originalPath, pathToSearch) != NULL;

						if(match)
						{
							// determine if the original file is being overlayed by this new file
							// by doing a diff and seeing if the diff is smaller than the new file

							//Displayf("\tFound a potential match at '%s', checking...", originalPath);

							char deltaFilePath[RAGE_MAX_PATH] = {0};
							formatf(deltaFilePath,"%s_tmp", outputFilePath);
							AssertVerify(ASSET.CreateLeadingPath(deltaFilePath));

							if(AssertVerify(XDelta(originalPath, path, deltaFilePath)))
							{
								const fiDevice *dev = fiDevice::GetDevice(deltaFilePath, true);
								Assert(dev);

								u64 deltaFileSize = dev->GetFileSize(deltaFilePath);
								Assert(deltaFileSize > 0);

								// check that the delta is smaller than the updated file,
								// if not smaller, then keep looking for an overlay file
								if(deltaFileSize < fileSize)
								{
									// the delta is smaller so this looks like an overlay file
									if(deltaFileSize < smallestDelta)
									{
										// this is the smallest delta so far...

										// delete the destination file if it already exists
										DeleteSingleFile(outputFilePath);

										// need to create the directory structure since this can be a new file/folder
										AssertVerify(ASSET.CreateLeadingPath(outputFilePath));

										AssertVerify(MoveFileA(deltaFilePath, outputFilePath));

										smallestDelta = deltaFileSize;

										overlay = true;
										//Displayf("\t'%s' could be an overlay of '%s', stored delta, will continue searching...", path, originalPath);

										u8 checksum1[Sha1::SHA1_DIGEST_LENGTH] = {0};
										u8 checksum2[Sha1::SHA1_DIGEST_LENGTH] = {0};

										ChecksumFile(originalPath, checksum1);
										ChecksumFile(path, checksum2);

										char originalPathRelative[RAGE_MAX_PATH] = {0};
										formatf(originalPathRelative, "%s", &originalPath[strlen(m_OriginalDirectory) + 1]);

										FileMetadata fileMetadata(originalPathRelative, checksum1, checksum2, false, true, true, fileSize, deltaFileSize);
										m_FileMetadataMap[StringDuplicate(relativeFilePath)] = fileMetadata;
									}
									else
									{
										//Displayf("\tA previous match produced a smaller delta, will continue searching...");
										AssertVerify(dev->Delete(deltaFilePath));
									}
								}
								else
								{
									//Displayf("\tDelta was no smaller, will continue searching...");
									AssertVerify(dev->Delete(deltaFilePath));
								}
							}
							else
							{
								//Displayf("\tAn error occured. Ignoring potential match, will continue searching...");
							}
						}
					}
					pathToSearch += 1;
				}
				else
				{
					if(overlay == false)
					{
						// no overlay found
						//Displayf("No moves/copies/overlay found for '%s'. Will be treated as new data.", path);
					}

					done = true;
				}
			} while(!done);
		}

		if(overlay)
		{
			// already handled above
		}
		else if(newFile)
		{
			AssertVerify(ASSET.CreateLeadingPath(outputFilePath));
			AssertVerify(fiDevice::CopySingleFile(path, outputFilePath));

			u8 checksum1[Sha1::SHA1_DIGEST_LENGTH] = {0};
			u8 checksum2[Sha1::SHA1_DIGEST_LENGTH] = {0};

			ChecksumFile(path, checksum1);
			ChecksumFile(outputFilePath, checksum2);
			Assert(memcmp(checksum1, checksum2, sizeof(checksum1)) == 0);

			const fiDevice *dev = fiDevice::GetDevice(outputFilePath, true);
			Assert(dev);

			char originalPathRelative[RAGE_MAX_PATH] = {0};
			formatf(originalPathRelative, "%s", &originalFilePath[strlen(m_OriginalDirectory) + 1]);

			u64 fileSize = dev->GetFileSize(outputFilePath);
			FileMetadata fileMetadata(originalPathRelative, checksum1, checksum2, true, false, false, fileSize, 0);
			Assert(m_FileMetadataMap.Access(relativeFilePath) == NULL);
			m_FileMetadataMap[StringDuplicate(relativeFilePath)] = fileMetadata;
		}
		else if(AreFilesIdentical(path, originalFilePath) == false)
		{
			u8 checksum1[Sha1::SHA1_DIGEST_LENGTH] = {0};
			u8 checksum2[Sha1::SHA1_DIGEST_LENGTH] = {0};

			ChecksumFile(originalFilePath, checksum1);
			ChecksumFile(path, checksum2);

			Assert(memcmp(checksum1, checksum2, sizeof(checksum1)) != 0);

			char deltaFilePath[RAGE_MAX_PATH] = {0};
			formatf(deltaFilePath,"%s", outputFilePath);
			AssertVerify(ASSET.CreateLeadingPath(deltaFilePath));

			//AssertVerify(XDelta(sourceStream, targetStream, outputFilePath));
			AssertVerify(XDelta(originalFilePath, path, deltaFilePath));

			const fiDevice *dev = fiDevice::GetDevice(deltaFilePath, true);
			Assert(dev);

			u64 deltaFileSize = dev->GetFileSize(deltaFilePath);
			Assert(deltaFileSize > 0);

			u64 fileSize = dev->GetFileSize(path);
			Assert(fileSize > 0);
			
			bool isDelta = true;

			// check that the delta is smaller than the updated file,
			// if not smaller, then delete the delta and copy the full file instead
			if(fileSize <= deltaFileSize)
			{
				AssertVerify(dev->Delete(deltaFilePath));
				AssertVerify(fiDevice::CopySingleFile(path, outputFilePath));
				isDelta = false;
				deltaFileSize = fileSize;
			}

			char originalPathRelative[RAGE_MAX_PATH] = {0};
			formatf(originalPathRelative, "%s", &originalFilePath[strlen(m_OriginalDirectory) + 1]);

			FileMetadata fileMetadata(originalPathRelative, checksum1, checksum2, false, isDelta, false, fileSize, deltaFileSize);
			Assert(m_FileMetadataMap.Access(relativeFilePath) == NULL);
			m_FileMetadataMap[StringDuplicate(relativeFilePath)] = fileMetadata;
		}
		else
		{
#if 0
			// double verification that the files are identical

			u8 checksum1[Sha1::SHA1_DIGEST_LENGTH] = {0};
			u8 checksum2[Sha1::SHA1_DIGEST_LENGTH] = {0};

			ChecksumFile(originalFilePath, checksum1);
			ChecksumFile(path, checksum2);

			Assert(memcmp(checksum1, checksum2, sizeof(checksum1)) == 0);
#endif
		}

		return true;
	}

	bool ProcessDirectory(const char *pathname)
	{
		bool result = true;
		const fiDevice *dev = fiDevice::GetDevice(pathname);

		fiFindData data;
		fiHandle handle = dev->FindFileBegin(pathname, data);
		if(fiIsValidHandle(handle))
		{
			while(result)
			{
				char currentPath[RAGE_MAX_PATH];
				if((strcmp(data.m_Name, ".") != 0) && (strcmp(data.m_Name, "..") != 0))
				{
					// Found a directory, so deal with it
					if((data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
					{
						// Build the new pathname to use
						formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);

						// Recurse
						if(!ProcessDirectory(currentPath))
						{
							dev->FindFileEnd(handle);
							return false;
						}
					}
					else
					{
						formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);

						u64 numBytesProcessed = m_NumBytesProcessed;

						ProcessFile(currentPath);

						AddBytesProcessed(dev->GetFileSize(currentPath));
					}
				}
				result = dev->FindFileNext(handle, data);
			}
			dev->FindFileEnd(handle);
		}
		return true;
	}

	bool ProcessDirectories(const char* original,
							const char* current,
							const char* output)
	{
		safecpy(m_OriginalDirectory, original);
		safecpy(m_CurrentDirectory, current);
		safecpy(m_OutputDirectory, output);
		formatf(m_PatchDataOutputDirectory, "%s\\data", m_OutputDirectory);

		return ProcessDirectory(m_CurrentDirectory);
	}

	u64 GetDirectorySize(const char* pathname)
	{
		u64 directorySize = 0;
		bool result = true;
		const fiDevice *dev = fiDevice::GetDevice(pathname);

		fiFindData data;
		fiHandle handle = dev->FindFileBegin(pathname, data);
		if(fiIsValidHandle(handle))
		{
			while(result)
			{
				char currentPath[RAGE_MAX_PATH];
				if((strcmp(data.m_Name, ".") != 0) && (strcmp(data.m_Name, "..") != 0))
				{
					// Found a directory, so deal with it
					if((data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
					{
						// Build the new pathname to use
						formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);

						// Recurse
						directorySize += GetDirectorySize(currentPath);
					}
					else
					{
						formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);
						directorySize += dev->GetFileSize(currentPath);
					}
				}
				result = dev->FindFileNext(handle, data);
			}
			dev->FindFileEnd(handle);
		}
		return directorySize;
	}

	bool BuildOriginalFileMap(const char* pathname)
	{
		bool result = true;
		const fiDevice *dev = fiDevice::GetDevice(pathname);

		fiFindData data;
		fiHandle handle = dev->FindFileBegin(pathname, data);
		if(fiIsValidHandle(handle))
		{
			while(result)
			{
				char currentPath[RAGE_MAX_PATH];
				if((strcmp(data.m_Name, ".") != 0) && (strcmp(data.m_Name, "..") != 0))
				{
					// Found a directory, so deal with it
					if((data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
					{
						// Build the new pathname to use
						formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);

						// Recurse
						if(!BuildOriginalFileMap(currentPath))
						{
							return false;
						}
					}
					else
					{
						formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);

						// found a file, add it to the map
						Assert(m_OriginalFilesMap.Access(currentPath) == NULL);

						OriginalFile file;
						m_OriginalFilesMap[StringDuplicate(currentPath)] = file;
					}
				}
				result = dev->FindFileNext(handle, data);
			}
			dev->FindFileEnd(handle);
		}
		return true;
	}

	bool WriteManifestFile()
	{
		bool success = false;

		memset(m_ScratchBuffer[0], 0, SIZE_OF_SCRATCH_BUFFER);

		datExportBuffer bb;
		bb.SetReadWriteBytes(m_ScratchBuffer[0], SIZE_OF_SCRATCH_BUFFER);

		rtry
		{
			int numEntries = m_FileMetadataMap.GetNumUsed();
			int version = 1;
			rverify(bb.WriteInt(version, sizeof(version) << 3), catchall, );
			rverify(bb.WriteInt(numEntries, sizeof(numEntries) << 3), catchall, );
			for(FileMetadataMapIterator it = m_FileMetadataMap.CreateIterator(); it; ++it)
			{
				const FileMetadata& fileMetadata = it.GetData();
				rverify(bb.WriteStr(it.GetKey(), strlen(it.GetKey()) + 1), catchall, );
				rverify(bb.WriteBool(fileMetadata.m_IsNewFile), catchall, );
				rverify(bb.WriteBool(fileMetadata.m_IsDelta), catchall, );
				rverify(bb.WriteBool(fileMetadata.m_IsOverlay), catchall, );
				rverify(bb.WriteBytes(fileMetadata.m_OriginalChecksum, sizeof(fileMetadata.m_OriginalChecksum)), catchall, );
				rverify(bb.WriteBytes(fileMetadata.m_PatchedChecksum, sizeof(fileMetadata.m_PatchedChecksum)), catchall, );
				if(fileMetadata.m_IsOverlay)
				{
					rverify(bb.WriteStr(fileMetadata.m_OriginalPath, sizeof(fileMetadata.m_OriginalPath)), catchall, );
				}
			}

			char outName[RAGE_MAX_PATH] = {0};
			formatf(outName,"%s\\%s", m_OutputDirectory, "manifest.dat");
			AssertVerify(ASSET.CreateLeadingPath(outName));
			fiStream *S = ASSET.Create(outName,"");
			rverify(S, catchall, );

			rverify(S->Write(m_ScratchBuffer[0], bb.GetByteLength()) == bb.GetByteLength(), catchall, );
			S->Close();

			success = true;        
		}
		rcatchall
		{
		}

		return success;
	}

	const char* ConvertBytesToFriendlyString(u64 bytes)
	{
		static char tmp[128] = {0};

		double kilobytes = (double)bytes / 1024.0;
		double megabytes = (double)kilobytes / 1024.0;
		double gigabytes = (double)megabytes / 1024.0;

		if(gigabytes >= 1.0f)
		{
			formatf(tmp, "%I64u bytes", bytes);
			formatf(tmp, "%I64u bytes (%.2f GB)", bytes, gigabytes);
		}
		else if(megabytes >= 1.0f)
		{
			formatf(tmp, "%I64u bytes (%.2f MB)", bytes, megabytes);
		}
		else if(kilobytes >= 1.0f)
		{
			formatf(tmp, "%I64u bytes (%.2f KB)", bytes, kilobytes);
		}
		else
		{
			formatf(tmp, "%I64u bytes", bytes);
		}
	
		return tmp;
	}

	void PrintReport()
	{
		OutputStringToLog("---------------------------------------------------------------------------------------------------");

		time_t secs = time(NULL);
		struct tm* t = localtime(&secs);
		OutputStringToLog("%u/%u %u:%02u:%02u%s\n",
						  t->tm_mon + 1,
						  t->tm_mday,
						  ((t->tm_hour % 12) == 0) ? 12 : (t->tm_hour % 12),
						  t->tm_min,
						  t->tm_sec,
						  (t->tm_hour < 12) ? "am" : "pm");

		double kilobytes = (double)m_TotalBytesToProcess / 1024.0;
		double megabytes = (double)kilobytes / 1024.0;
		double gigabytes = (double)megabytes / 1024.0;

		if(gigabytes >= 1.0f)
		{
			OutputStringToLog("Processed %.2f GB of data", gigabytes);
		}
		else if(megabytes >= 1.0f)
		{
			OutputStringToLog("Processed %.2f MB of data", megabytes);
		}
		else if(kilobytes >= 1.0f)
		{
			OutputStringToLog("Processed %.2f KB of data", kilobytes);
		}
		else
		{
			OutputStringToLog("Processed %I64u bytes of data", m_TotalBytesToProcess);
		}

		u32 elapsedSeconds = (u32)m_Timer.GetTime();
		u32 elapsedMins = (u32)(elapsedSeconds / 60);
		u32 elapsedHours = (u32)(elapsedMins / 60);
		elapsedSeconds -= elapsedMins * 60;
		elapsedMins -= elapsedHours * 60;
		OutputStringToLog("Elapsed time: %02d:%02d:%02d", elapsedHours, elapsedMins, elapsedSeconds);
		OutputStringToLog("Speed: %d KB/sec\n", (int)(kilobytes / m_Timer.GetTime()));

		OutputStringToLog("Directories:");
		OutputStringToLog("\tOld: %s", m_OriginalDirectory);
		OutputStringToLog("\tNew: %s", m_CurrentDirectory);
		OutputStringToLog("\tOutput: %s\n", m_OutputDirectory);

		OutputStringToLog("--- New Files ---");

		u64 totalNewDataSize = 0;
		for(FileMetadataMapIterator it = m_FileMetadataMap.CreateIterator(); it; ++it)
		{
			const FileMetadata& fileMetadata = it.GetData();
			if(fileMetadata.m_IsNewFile)
			{
				totalNewDataSize += fileMetadata.m_ReplacementSize;
				OutputStringToLog("%s\n\tbytes: %I64u", it.GetKey(), fileMetadata.m_ReplacementSize);
			}
		}
		OutputStringToLog("---------------------------------------------------------------------------------------------------");
		OutputStringToLog("Total size of new data: %s\n", ConvertBytesToFriendlyString(totalNewDataSize));

		OutputStringToLog("--- Modified Files ---");

		u64 totalReplacementSize = 0;
		u64 totalDeltaSize = 0;
		for(FileMetadataMapIterator it = m_FileMetadataMap.CreateIterator(); it; ++it)
		{
			const FileMetadata& fileMetadata = it.GetData();
			if((fileMetadata.m_IsOverlay == false) && (fileMetadata.m_IsNewFile == false))
			{
				totalReplacementSize += fileMetadata.m_ReplacementSize;
				totalDeltaSize += fileMetadata.m_DeltaSize;
				u64 bytesSaved = fileMetadata.m_ReplacementSize - fileMetadata.m_DeltaSize;
				float percentSaved = (1.0f - ((float)fileMetadata.m_DeltaSize / (float)fileMetadata.m_ReplacementSize)) * 100.0f;
				OutputStringToLog("%s\n\tbytes saved: %I64u (%u%%)", it.GetKey(), bytesSaved, (u32)percentSaved);
			}
		}
		OutputStringToLog("---------------------------------------------------------------------------------------------------");
		u64 totalBytesSaved = totalReplacementSize - totalDeltaSize;
		float totalPercentSaved = (1.0f - ((float)totalDeltaSize / (float)totalReplacementSize)) * 100.0f;
		OutputStringToLog("Total saved by delta compression: %s (%u%%)\n", ConvertBytesToFriendlyString(totalBytesSaved), (u32)totalPercentSaved);

		OutputStringToLog("--- Overlayed Files ---");
		u64 totalReplacementSizeOverlay = 0;
		u64 totalDeltaSizeOverlay = 0;
		for(FileMetadataMapIterator it = m_FileMetadataMap.CreateIterator(); it; ++it)
		{
			const FileMetadata& fileMetadata = it.GetData();
			if(fileMetadata.m_IsOverlay)
			{
				totalReplacementSizeOverlay += fileMetadata.m_ReplacementSize;
				totalDeltaSizeOverlay += fileMetadata.m_DeltaSize;
				u64 bytesSaved = fileMetadata.m_ReplacementSize - fileMetadata.m_DeltaSize;
				float percentSaved = (1.0f - ((float)fileMetadata.m_DeltaSize / (float)fileMetadata.m_ReplacementSize)) * 100.0f;
				OutputStringToLog("%s (overlay of %s)\n\tbytes saved: %I64u (%u%%)", it.GetKey(), fileMetadata.m_OriginalPath, bytesSaved, (u32)percentSaved);
			}
		}
		OutputStringToLog("---------------------------------------------------------------------------------------------------");
		totalBytesSaved = totalReplacementSizeOverlay - totalDeltaSizeOverlay;
		totalPercentSaved = (1.0f - ((float)totalDeltaSizeOverlay / (float)totalReplacementSizeOverlay)) * 100.0f;
		OutputStringToLog("Total saved by overlay detection: %s\n", ConvertBytesToFriendlyString(totalBytesSaved));

		OutputStringToLog("===================================================================================================");
		totalReplacementSize += totalReplacementSizeOverlay;
		totalDeltaSize += totalDeltaSizeOverlay;
		totalBytesSaved = totalReplacementSize - totalDeltaSize;
		u64 totalPatchSize = totalNewDataSize + totalDeltaSize;
		totalPercentSaved = (1.0f - ((float)totalDeltaSize / (float)totalReplacementSize)) * 100.0f;
		OutputStringToLog("Total saved: %s (%u%%)", ConvertBytesToFriendlyString(totalBytesSaved), (u32)totalPercentSaved);
		OutputStringToLog("Total size of patch before being compressed by the installer: %s", ConvertBytesToFriendlyString(totalPatchSize));
	}

	bool CreatePatch(const char* original,
					 const char* current,
					 const char* output)
	{
		bool success = false;

		rtry
		{
			rverify(ASSET.Exists(original, ""), catchall, );
			rverify(ASSET.Exists(current, ""), catchall, );
			rverify(ASSET.Exists(output, "") == false, catchall, );

			m_Timer.Reset();
			Displayf("Calculating size of '%s'", current);
			m_TotalBytesToProcess = GetDirectorySize(current);
			rverify(m_TotalBytesToProcess > 0, catchall, );

			double kilobytes = (double)m_TotalBytesToProcess / 1024.0;
			double megabytes = (double)kilobytes / 1024.0;
			double gigabytes = (double)megabytes / 1024.0;

			if(gigabytes >= 1.0f)
			{
				Displayf("Processing %.2f GB of data...", gigabytes);
			}
			else if(megabytes >= 1.0f)
			{
				Displayf("Processing %.2f MB of data...", megabytes);
			}
			else if(kilobytes >= 1.0f)
			{
				Displayf("Processing %.2f KB of data...", kilobytes);
			}
			else
			{
				Displayf("Processing %I64u bytes of data...", m_TotalBytesToProcess);
			}

			rverify(BuildOriginalFileMap(original), catchall, );
			rverify(ProcessDirectories(original, current, output), catchall, );
			rverify(WriteManifestFile(), catchall, );
			PrintReport();

			success = true;
		}
		rcatchall
		{

		}

		return success;
	}

#if __DEV
	float m_Progress;
	u64 m_TotalBytesToProcess;
	u64 m_NumBytesProcessed;
	sysTimer m_Timer;
#endif

	static const u64 SIZE_OF_SCRATCH_BUFFER = 128 * 1024 * 1024;
	u8* m_ScratchBuffer[2];

	fiHandle m_LogFileHandle;
	const fiDevice* m_LogFileDevice;

	char m_OriginalDirectory[RAGE_MAX_PATH];
	char m_CurrentDirectory[RAGE_MAX_PATH];
	char m_OutputDirectory[RAGE_MAX_PATH];
	char m_PatchDataOutputDirectory[RAGE_MAX_PATH];

	struct FileMetadata
	{
		FileMetadata()
		{
			memset(m_OriginalPath, 0, sizeof(m_OriginalPath));
			memset(m_OriginalChecksum, 0, sizeof(m_OriginalChecksum));
			memset(m_PatchedChecksum, 0, sizeof(m_PatchedChecksum));
			m_IsNewFile = false;
			m_IsDelta = false;
			m_IsOverlay = false;
			m_ReplacementSize = 0;
			m_DeltaSize = 0;
		}

		FileMetadata(const char* originalPath,
					 u8 (&originalChecksum)[Sha1::SHA1_DIGEST_LENGTH],
					 u8 (&patchedChecksum)[Sha1::SHA1_DIGEST_LENGTH],
					 bool isNewFile,
					 bool isDelta,
					 bool isOverlay,
					 u64 replacementSize,
					 u64 deltaSize)
		{
			safecpy(m_OriginalPath, originalPath);
			memcpy(m_OriginalChecksum, originalChecksum, sizeof(m_OriginalChecksum));
			memcpy(m_PatchedChecksum, patchedChecksum, sizeof(m_PatchedChecksum));
			m_IsNewFile = isNewFile;
			m_IsDelta = isDelta;
			m_IsOverlay = isOverlay;
			m_ReplacementSize = replacementSize;
			m_DeltaSize = deltaSize;
		}

		char m_OriginalPath[rage::RAGE_MAX_PATH];
		u8 m_OriginalChecksum[Sha1::SHA1_DIGEST_LENGTH];
		u8 m_PatchedChecksum[Sha1::SHA1_DIGEST_LENGTH];
		bool m_IsNewFile;
		bool m_IsDelta;
		bool m_IsOverlay;
		u64 m_ReplacementSize;
		u64 m_DeltaSize;
	};

	struct FileMetadataMapEquals
	{
		bool operator ()(const char * left, const char * right) const { return strcmp(left, right) == 0; }
	};

	typedef atMap<const char*, FileMetadata, atMapHashFn<const char*>, FileMetadataMapEquals> FileMetadataMap;
	typedef atMap<const char*, FileMetadata, atMapHashFn<const char*>, FileMetadataMapEquals>::Iterator FileMetadataMapIterator;

	FileMetadataMap m_FileMetadataMap;

	struct OriginalFile
	{
		OriginalFile()
		{
		}
	};

	struct OriginalFilesMapEquals
	{
		bool operator ()(const char * left, const char * right) const { return strcmp(left, right) == 0; }
	};

	typedef atMap<const char*, OriginalFile, atMapHashFn<const char*>, OriginalFilesMapEquals> OriginalFileMap;
	typedef atMap<const char*, OriginalFile, atMapHashFn<const char*>, OriginalFilesMapEquals>::Iterator OriginalFileMapIterator;

	OriginalFileMap m_OriginalFilesMap;
};

int main(int argc,char **argv)
{
	char* params[] = {"createpatch.exe", "-all_tty=debug3"};
	sysParam::Init(2, params);

	if(AssertVerify(argc == 4))
	{
		const char* oldDir = argv[1];
		const char* newDir = argv[2];
		const char* outputDir = argv[3];

		App app;
		if(AssertVerify(app.CreatePatch(oldDir,
						newDir,
						outputDir)))
		{
			return 0;
		}
	}

	return -1;
}
