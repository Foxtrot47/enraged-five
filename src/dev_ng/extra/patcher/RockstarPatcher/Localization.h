#ifndef ROCKSTAR_PATCHER_LOCALIZATION_H 
#define ROCKSTAR_PATCHER_LOCALIZATION_H 

#include <string>
#include "file/limits.h"

class CLocalization
{
public:
	static const int LANGUAGE_CODE_BUF_SIZE = 32;

	CLocalization();
	~CLocalization();
	void Init(const char* xmlFilePath, const char* languageCode, const char* defaultLanguageCode);
	std::wstring GetString(unsigned int stringId);
private:
	char m_XmlFilePath[rage::RAGE_MAX_PATH];
	char m_LanguageCode[LANGUAGE_CODE_BUF_SIZE];
	char m_DefaultLanguageCode[LANGUAGE_CODE_BUF_SIZE];
};

#endif // ROCKSTAR_PATCHER_LOCALIZATION_H 