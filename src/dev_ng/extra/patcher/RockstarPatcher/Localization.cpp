#include "Localization.h"
#include "diag/seh.h"
#include "parser/manager.h"
#include "string/string.h"
#include "system/xtl.h"

using namespace rage;

CLocalization::CLocalization()
{

}

CLocalization::~CLocalization()
{
}

void CLocalization::Init(const char* xmlFilePath, const char* languageCode, const char* defaultLanguageCode)
{
	safecpy(m_XmlFilePath, xmlFilePath);
	safecpy(m_LanguageCode, languageCode);
	safecpy(m_DefaultLanguageCode, defaultLanguageCode);
}

std::wstring CLocalization::GetString(unsigned int stringId)
{
	bool success = false;
	const int MAX_STR_LEN_IN_CHARS = 2048;
	std::wstring ret;

	INIT_PARSER;

	parTree* tree = NULL;

	rtry
	{
		PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);
		tree = PARSER.LoadTree(m_XmlFilePath, "xml");
		PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, false);

		rverify(tree && tree->GetRoot(), catchall, );

		parTreeNode* pRoot = tree->GetRoot();
		rverify(pRoot, catchall, );

		char szLanguageId[sizeof(m_LanguageCode) + 1] = {0};
		formatf(szLanguageId, "L%s", m_LanguageCode);

		parTreeNode* pLanguage = pRoot->FindChildWithName(szLanguageId);
		if(!AssertVerify(pLanguage != NULL))
		{
			formatf(szLanguageId, "L%s", m_DefaultLanguageCode);
			pLanguage = pRoot->FindChildWithName(szLanguageId);
		}
		rverify(pLanguage, catchall, );

		char szStringId[32] = {0};
		formatf(szStringId, "s%u", stringId);

		parTreeNode* pString = pLanguage->FindChildWithName(szStringId);
		rverify(pString, catchall, );
		rverify(pString->GetDataSize() > 0, catchall, );
		rverify(pString->GetDataSize() < (MAX_STR_LEN_IN_CHARS * sizeof(wchar_t)), catchall, );
		rverify(pString->GetData() != NULL, catchall, );

		wchar_t temp[MAX_STR_LEN_IN_CHARS] = {0};

		int err = MultiByteToWideChar(CP_UTF8,
									  0,
									  pString->GetData(),
									  -1,
									  temp,
									  MAX_STR_LEN_IN_CHARS);

		if(err != 0)
		{
			ret = std::wstring(temp);	
		}
	}
	rcatchall
	{
	}

	if(tree)
	{
		delete tree;
		tree = NULL;
	}

	SHUTDOWN_PARSER;

	if(!AssertVerify(ret.length() != 0))
	{
		wchar_t temp[64] = {0};
		formatf(temp, L"Error %u", stringId);
		ret = std::wstring(temp);
	}

	return ret;
}
