#pragma warning(disable: 4668)
#include "atl/map.h"
#include "data/bitbuffer.h"
#include "data/sha1.h"
#include "diag/seh.h"
#include "file/asset.h"
#include "file/device.h"
#include "system/param.h"
#include "system/timer.h"
#include "system/xtl.h"

#define SIMPLE_HEAP_SIZE (12 * 1024)
#define SIMPLE_PHYSICAL_SIZE 32

#include "system/main.h"
#include "system/simpleallocator.h"
#include "localization.h"

using namespace rage;

class App
{
public:
	enum StringIds
	{
		SID_INVALID = 0,
		SID_ERROR_ORIGINAL_FILE_MISSING = 1,
		SID_ERROR_ORIGINAL_FILE_MODIFIED = 2,
		SID_ERROR_PATCHING_FILE_FAILED = 3,
		SID_ERROR_FAILED_TO_MOVE_FILE = 4,
		SID_ERROR_OUT_OF_MEMORY = 5,
		SID_ERROR_COULD_NOT_CREATE_OUTPUT_DIRECTORY = 6,
		SID_ERROR_PATH_DOES_NOT_EXIST = 7,
		SID_ERROR_FAILED_TO_READ_MANIFEST = 8,
	};

	App()
#if __DEV
	: m_Progress(0.0f)
	, m_TotalBytesToProcess(0)
	, m_NumBytesProcessed(0)
#endif
	{
		m_ScratchBuffer = rage_new u8[SIZE_OF_SCRATCH_BUFFER];
		Assert(m_ScratchBuffer);

		m_FileMetadataMap.Reset();

		memset(m_PatchDirectory, 0, sizeof(m_PatchDirectory));
		memset(m_ManifestPath, 0, sizeof(m_ManifestPath));
		memset(m_LocalizationXmlFilePath, 0, sizeof(m_LocalizationXmlFilePath));
		memset(m_PatchDataDirectory, 0, sizeof(m_PatchDataDirectory));
		memset(m_CurrentDirectory, 0, sizeof(m_CurrentDirectory));
		memset(m_OutputDirectory, 0, sizeof(m_OutputDirectory));
	}

	~App()
	{
	}

	void Shutdown(bool exitProcess)
	{
		// the temp output directory may not exist if the patch only contained new data.
		if(ASSET.Exists(m_OutputDirectory, ""))
		{
#if __DEV
			// there shouldn't be any data left in the temp folder at this point
			Assert(GetDirectorySize(m_OutputDirectory) == 0);
#endif

			// delete the temp folder
			AssertVerify(fiDevice::DeleteDirectory(m_OutputDirectory, true, true));
		}

		// delete the patch directory
		AssertVerify(fiDevice::DeleteDirectory(m_PatchDirectory, true, true));

		// delete the localization file
		AssertVerify(DeleteSingleFile(m_LocalizationXmlFilePath));

		if(m_ScratchBuffer != NULL)
		{
			delete [] m_ScratchBuffer;
		}

		m_FileMetadataMap.Kill();

		if(exitProcess)
		{
			ExitProcess(-1);
		}
	}

#if __DEV
	void AddBytesProcessed(u64 bytesProcessed)
	{
		if(bytesProcessed == 0)
		{
			return;
		}
		m_NumBytesProcessed += bytesProcessed;

		float progress = (float)((double)m_NumBytesProcessed / (double)m_TotalBytesToProcess);
		Assert((progress >= 0.0f) && (progress <= 1.0f));
		if((m_NumBytesProcessed == m_TotalBytesToProcess) || ((progress - m_Progress) > 0.0001f))
		{
			Displayf("Progress: %.2f%%", progress * 100.0f);

			u64 numKilobytesProcessed = m_NumBytesProcessed >> 10;
			Displayf("\tKilobytes processed: %I64u / %I64u", numKilobytesProcessed, m_TotalBytesToProcess >> 10);

			u32 elapsedSeconds = (u32)m_Timer.GetTime();
			u32 elapsedMins = (u32)(elapsedSeconds / 60);
			u32 elapsedHours = (u32)(elapsedMins / 60);
			elapsedSeconds -= elapsedMins * 60;
			elapsedMins -= elapsedHours * 60;
			Displayf("\tElapsed time: %02d:%02d:%02d", elapsedHours, elapsedMins, elapsedSeconds);
			Displayf("\tSpeed: %d KB/sec", (int)(numKilobytesProcessed / m_Timer.GetTime()));

			m_Progress = progress;
		}
	}
#endif

	void DisplayError(const std::wstring& s, bool allowContinue)
	{
		int response = MessageBoxW(NULL, s.c_str(), L"Rockstar Patcher", (allowContinue ? MB_YESNO : MB_OK) | MB_SETFOREGROUND | MB_TOPMOST | (allowContinue ? MB_ICONWARNING : MB_ICONERROR));
		if(response == IDYES)
		{
			// continue
		}
		else
		{
			Shutdown(true);
		}
	}

	void DisplayError(unsigned int stringId, bool allowContinue)
	{
		std::wstring s = m_Localization.GetString(stringId);

		std::wstring toFind(L"\\r\\n");
		std::wstring replacement(L"\r\n");

		size_t pos = s.find(toFind);
		if(pos != std::string::npos)
		{
			s.replace(pos, toFind.length(), replacement);
		}

		DisplayError(s, allowContinue);
	}

	void DisplayError(unsigned int stringId, int insertedInt, const char* insertedString, bool allowContinue)
	{
		std::wstring s = m_Localization.GetString(stringId);

		// replace all occurrences of ~n~ with a newline
		{
			std::wstring toFindCr(L"~n~");
			std::wstring replacementCr(L"\r\n");
			while(true)
			{
				size_t pos = s.find(toFindCr);
				if(pos != std::string::npos)
				{
					s.replace(pos, toFindCr.length(), replacementCr);
				}
				else
				{
					break;
				}
			}
		}

		// replace %s with instertedString
		{
			std::wstring toFindStr(L"%s");
			std::wstring replacementStr;

			const unsigned int MAX_STR_LEN_IN_CHARS = 2048;

			Assert(insertedString && (strlen(insertedString) < MAX_STR_LEN_IN_CHARS));

			wchar_t temp[MAX_STR_LEN_IN_CHARS] = {0};

			int err = MultiByteToWideChar(CP_ACP,
										  0,
										  insertedString,
										  -1,
										  temp,
										  MAX_STR_LEN_IN_CHARS);

			if(err != 0)
			{
				replacementStr = std::wstring(temp);	
			}

			size_t pos = s.find(toFindStr);
			if(pos != std::string::npos)
			{
				s.replace(pos, toFindStr.length(), replacementStr);
			}
		}

		// replace %d with instertedInt
		{
			wchar_t wszInt[32] = {0};
			formatf(wszInt, L"%d", insertedInt);

			std::wstring toFindInt(L"%d");
			std::wstring replacementInt(wszInt);

			size_t pos = s.find(toFindInt);
			if(pos != std::string::npos)
			{
				s.replace(pos, toFindInt.length(), replacementInt);
			}
		}

		DisplayError(s, allowContinue);
	}

	void DisplayError(unsigned int stringId, const char* insertedString, bool allowContinue)
	{
		DisplayError(stringId, 0, insertedString, allowContinue);
	}

	bool GetProcessExePath(char (&path)[RAGE_MAX_PATH]) const
	{
		bool success = false;
		path[0] = '\0';

		if(AssertVerify(GetModuleFileNameA(NULL, path, sizeof(path)) != 0))
		{
			success = true;
		}

		return success;
	}

	bool GetProcessExeDirectory(char (&path)[RAGE_MAX_PATH]) const
	{
		bool success = false;
		path[0] = '\0';

		if(AssertVerify(GetProcessExePath(path)))
		{
			char* lastSlash = strrchr(path, '\\');
			if(lastSlash == NULL)
			{
				lastSlash = strrchr(path, '/');
			}

			if(AssertVerify(lastSlash != NULL))
			{
				lastSlash[1] = '\0';
			}

			success = true;
		}

		return success;
	}

	int XDelta(const char* currentFilePath, const char* deltaFilePath, const char* outputPath)
	{
		int retCode = -1;

		char processDirectory[RAGE_MAX_PATH] = {0};
		if(AssertVerify(GetProcessExeDirectory(processDirectory)))
		{
			STARTUPINFOA si;
			PROCESS_INFORMATION pi;

			ZeroMemory(&si, sizeof(si));
			si.cb = sizeof(si);
			si.dwFlags = STARTF_USESHOWWINDOW;
			si.wShowWindow = SW_HIDE;
			ZeroMemory(&pi, sizeof(pi));

			char xdeltaPath[RAGE_MAX_PATH] = {0};
			formatf(xdeltaPath, "%sxdelta3.exe", processDirectory);

			char commandLine[4096] = {0};
			formatf(commandLine, "\"%s\" -d -s \"%s\" \"%s\" \"%s\"", xdeltaPath, currentFilePath, deltaFilePath, outputPath);

			if(0 != ::CreateProcessA(xdeltaPath,
									 commandLine,
									 NULL,
									 NULL,
									 FALSE,
									 0,
									 NULL,
									 NULL,
									 &si,
									 &pi))
			{
				::WaitForSingleObject(pi.hProcess, INFINITE);

				DWORD exitCode = 0;
				if(AssertVerify(GetExitCodeProcess(pi.hProcess, &exitCode) != 0))
				{
					retCode = (int)exitCode;
					Assert(exitCode == 0);
				}

				CloseHandle(pi.hProcess);
				CloseHandle(pi.hThread);
			}
			else
			{
				Assertf(false, "Failed to run xdelta process");
				CloseHandle(pi.hProcess);
				CloseHandle(pi.hThread);
			}
		}

		return retCode;
	}

	bool ChecksumFile(const char* filename, u8 (&checksum)[Sha1::SHA1_DIGEST_LENGTH])
	{
		Assert(filename && (filename[0] != '\0'));
		memset(checksum, 0, sizeof(checksum));

		bool success = false;
		Sha1 sha1;

		const fiDevice *device = fiDevice::GetDevice(filename, true);

		if(AssertVerify(device))
		{
			fiHandle hFile = device->Open(filename, true);

			if(AssertVerify(fiIsValidHandle(hFile)))
			{
				u64 fileSize = device->Size64(hFile);

				if(fileSize == 0)
				{
					success = true;
				}
				else
				{
					u64 bytesRemaining = fileSize;
					u64 totalBytesRead = 0;

					while(bytesRemaining > 0)
					{
						u64 numBytes = SIZE_OF_SCRATCH_BUFFER;
						if(numBytes > bytesRemaining)
						{
							numBytes = bytesRemaining;
						}

						u64 bytesRead = device->Read(hFile, m_ScratchBuffer, (int)numBytes);

						Assert(bytesRead == numBytes);
						totalBytesRead += numBytes;
						bytesRemaining -= numBytes;
						Assert(bytesRemaining >= 0);

						sha1.Update(m_ScratchBuffer, (u32)numBytes);
					}

					Assert(totalBytesRead == fileSize);
					sha1.Final(checksum);
				}

				device->Close(hFile);
				success = true;
			}
		}

		return success;
	}

	bool ProcessFile(const char* path)
	{
		char relativeFilePath[RAGE_MAX_PATH] = {0};
		formatf(relativeFilePath, "%s", &path[strlen(m_PatchDataDirectory) + 1]);

		char currentFilePath[RAGE_MAX_PATH] = {0};
		formatf(currentFilePath, "%s\\%s", m_CurrentDirectory, relativeFilePath);

		char outputFilePath[RAGE_MAX_PATH] = {0};
		formatf(outputFilePath, "%s\\%s", m_OutputDirectory, relativeFilePath);

		FileMetadata* mapData = m_FileMetadataMap.Access(relativeFilePath);
		if(AssertVerify(mapData != NULL))
		{
			if(mapData->m_IsDelta)
			{
				if(mapData->m_IsOverlay)
				{
					formatf(currentFilePath, "%s\\%s", m_CurrentDirectory, mapData->m_OriginalPath);
				}

				if(AssertVerify(ASSET.Exists(currentFilePath, "")))
				{
					// verify checksum of the current file before applying the patch
					u8 checksum[Sha1::SHA1_DIGEST_LENGTH] = {0};
					AssertVerify(ChecksumFile(currentFilePath, checksum));
					
					if(AssertVerify(memcmp(mapData->m_OriginalChecksum, checksum, sizeof(checksum)) == 0))
					{
						// apply the delta patch
						AssertVerify(ASSET.CreateLeadingPath(outputFilePath));

						int retCode = XDelta(currentFilePath, path, outputFilePath);
						Assert(retCode == 0);

						// verify checksum of the patched file
						AssertVerify(ChecksumFile(outputFilePath, checksum));

						if(!AssertVerify(memcmp(mapData->m_PatchedChecksum, checksum, sizeof(checksum)) == 0))
						{
							DisplayError(SID_ERROR_PATCHING_FILE_FAILED, retCode, currentFilePath, true);

							const fiDevice *dev = fiDevice::GetDevice(outputFilePath, true);
							if(AssertVerify(dev))
							{
								AssertVerify(dev->Delete(outputFilePath));
							}
						}
					}
					else if(memcmp(mapData->m_PatchedChecksum, checksum, sizeof(checksum)) == 0)
					{
						// the patch has already been applied to this file (possibly by a previous patch attempt)
					}
					else
					{
						DisplayError(SID_ERROR_ORIGINAL_FILE_MODIFIED, currentFilePath, true);
					}
				}
				else
				{
					DisplayError(SID_ERROR_ORIGINAL_FILE_MISSING, currentFilePath, true);
				}
			}
		}

		return true;
	}

	bool ProcessDirectory(const char *pathname)
	{
		bool result = true;
		const fiDevice *dev = fiDevice::GetDevice(pathname);
		if(AssertVerify(dev != NULL))
		{
			fiFindData data;
			fiHandle handle = dev->FindFileBegin(pathname, data);
			if(fiIsValidHandle(handle))
			{
				while(result)
				{
					char currentPath[RAGE_MAX_PATH];
					if((strcmp(data.m_Name, ".") != 0) && (strcmp(data.m_Name, "..") != 0))
					{
						// Found a directory, so deal with it
						if((data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
						{
							// Build the new pathname to use
							formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);

							// Recurse
							if(!ProcessDirectory(currentPath))
							{
								dev->FindFileEnd(handle);
								return false;
							}
						}
						else
						{
							formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);

#if __DEV
							u64 numBytesProcessed = m_NumBytesProcessed;
#endif

							ProcessFile(currentPath);

#if __DEV
							AddBytesProcessed(dev->GetFileSize(currentPath));
#endif
						}
					}
					result = dev->FindFileNext(handle, data);
				}
				dev->FindFileEnd(handle);
			}
		}
		return true;
	}

#if __DEV
	u64 GetDirectorySize(const char* pathname)
	{
		u64 directorySize = 0;
		bool result = true;
		const fiDevice *dev = fiDevice::GetDevice(pathname);
		if(AssertVerify(dev != NULL))
		{
			fiFindData data;
			fiHandle handle = dev->FindFileBegin(pathname, data);
			if(fiIsValidHandle(handle))
			{
				while(result)
				{
					char currentPath[RAGE_MAX_PATH];
					if((strcmp(data.m_Name, ".") != 0) && (strcmp(data.m_Name, "..") != 0))
					{
						// Found a directory, so deal with it
						if((data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
						{
							// Build the new pathname to use
							formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);

							// Recurse
							directorySize += GetDirectorySize(currentPath);
						}
						else
						{
							formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);
							directorySize += dev->GetFileSize(currentPath);
						}
					}
					result = dev->FindFileNext(handle, data);
				}
				dev->FindFileEnd(handle);
			}
		}
		return directorySize;
	}
#endif
	
	// don't use Rage's StringDuplicate since it requires a custom operator new[]
	char* CopyString(const char* str)
	{
		if(!str)
			return NULL;
		int len = strlen(str) + 1;
		char* copy = rage_new char[len];
		memcpy(copy, str, len);
		return copy;
	}

	bool ReadManifestFile()
	{
		bool success = false;

		rtry
		{
			const fiDevice *dev = fiDevice::GetDevice(m_ManifestPath, true);
			rverify(dev, catchall, );

			u64 fileSize = dev->GetFileSize(m_ManifestPath);
			rverify(fileSize <= SIZE_OF_SCRATCH_BUFFER, catchall, );

			fiHandle hFile = dev->Open(m_ManifestPath, true);
			rverify(fiIsValidHandle(hFile), catchall, );

			u8* buf = m_ScratchBuffer;
			rverify(buf, catchall, );

			memset(buf, 0, (size_t)fileSize);

			rverify(dev->Read(hFile, buf, (int)fileSize) == fileSize, catchall, );

			datImportBuffer bb;
			bb.SetReadOnlyBytes(buf, (int)fileSize);

			int version = 0;
			rverify(bb.ReadInt(version, sizeof(version) << 3), catchall, );
			rverify(version == 1, catchall, );

			int numEntries = 0;
			rverify(bb.ReadInt(numEntries, sizeof(numEntries) << 3), catchall, );

			for(int i = 0; i < numEntries; ++i)
			{
				char relativeFilePath[RAGE_MAX_PATH] = {0};
				FileMetadata fileMetadata;
				rverify(bb.ReadStr(relativeFilePath, RAGE_MAX_PATH), catchall, );
				rverify(bb.ReadBool(fileMetadata.m_IsNewFile), catchall, );
				rverify(bb.ReadBool(fileMetadata.m_IsDelta), catchall, );
				rverify(bb.ReadBool(fileMetadata.m_IsOverlay), catchall, );
				rverify(bb.ReadBytes(fileMetadata.m_OriginalChecksum, sizeof(fileMetadata.m_OriginalChecksum)), catchall, );
				rverify(bb.ReadBytes(fileMetadata.m_PatchedChecksum, sizeof(fileMetadata.m_PatchedChecksum)), catchall, );
				if(fileMetadata.m_IsOverlay)
				{
					rverify(bb.ReadStr(fileMetadata.m_OriginalPath, RAGE_MAX_PATH), catchall, );
				}

				Assert(m_FileMetadataMap.Access(relativeFilePath) == NULL);
				m_FileMetadataMap[CopyString(relativeFilePath)] = fileMetadata;
			}

			dev->Close(hFile);
			hFile = fiHandleInvalid;

			success = true;        
		}
		rcatchall
		{
		}

		return success;
	}

	bool DeleteSingleFile(const char* file)
	{
		if(AssertVerify(file && file[0] != '\0'))
		{
			const fiDevice *dev = fiDevice::GetDevice(file, true);
			if(AssertVerify(dev))
			{
				if(dev->GetAttributes(file) != FILE_ATTRIBUTE_INVALID)
				{
					if(!dev->Delete(file))
					{
						dev->SetAttributes(file, dev->GetAttributes(file) & ~FILE_ATTRIBUTE_READONLY);
						return AssertVerify(dev->Delete(file));
					}
					else
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	bool MoveSingleFile(const char* from, const char* fromRoot, const char* toRoot)
	{
		bool success = false;

		char relativeFilePath[RAGE_MAX_PATH] = {0};
		formatf(relativeFilePath, "%s", &from[strlen(fromRoot) + 1]);

		FileMetadata* mapData = m_FileMetadataMap.Access(relativeFilePath);
		if(AssertVerify(mapData != NULL))
		{
			char to[RAGE_MAX_PATH] = {0};
			formatf(to, "%s\\%s", toRoot, relativeFilePath);

			Displayf("\tMoving '%s'", relativeFilePath);

			// delete the destination file if it already exists
			DeleteSingleFile(to);

			// need to create the directory structure since this can be a new file/folder
			AssertVerify(ASSET.CreateLeadingPath(to));

			if(AssertVerify(MoveFileA(from, to) != 0))
			{
				success = true;
			}
			else
			{
				DisplayError(SID_ERROR_FAILED_TO_MOVE_FILE, to, true);
			}
		}

		return success;
	}

	bool MovePatchedFiles(const char *pathname, const char* fromRoot, const char *to, bool moveExes)
	{
		bool result = true;
		const fiDevice *dev = fiDevice::GetDevice(pathname);
		if(AssertVerify(dev != NULL))
		{
			fiFindData data;
			fiHandle handle = dev->FindFileBegin(pathname, data);
			if(fiIsValidHandle(handle))
			{
				while(result)
				{
					char currentPath[RAGE_MAX_PATH];
					if((strcmp(data.m_Name, ".") != 0) && (strcmp(data.m_Name, "..") != 0))
					{
						// Found a directory, so deal with it
						if((data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
						{
							// Build the new pathname to use
							formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);

							// Recurse
							if(!MovePatchedFiles(currentPath, fromRoot, to, moveExes))
							{
								dev->FindFileEnd(handle);
								return false;
							}
						}
						else
						{
							formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);

							if(moveExes == false)
							{
								const char* ext = ASSET.FindExtensionInPath(currentPath);
								bool isExe = (ext != NULL) && (stricmp(ext, ".exe") == 0);
								if(isExe == false)
								{
									MoveSingleFile(currentPath, fromRoot, to);
								}
							}
							else
							{
								MoveSingleFile(currentPath, fromRoot, to);
							}
						}
					}
					result = dev->FindFileNext(handle, data);
				}
				dev->FindFileEnd(handle);
			}
		}
		return true;
	}

	bool MoveNonDeltaFile(const char* path, const char* fromRoot, const char* toRoot)
	{
		char relativeFilePath[RAGE_MAX_PATH] = {0};
		formatf(relativeFilePath, "%s", &path[strlen(fromRoot) + 1]);

		char currentFilePath[RAGE_MAX_PATH] = {0};
		formatf(currentFilePath, "%s\\%s", toRoot, relativeFilePath);

		FileMetadata* mapData = m_FileMetadataMap.Access(relativeFilePath);
		if(AssertVerify(mapData != NULL))
		{
			if(mapData->m_IsDelta == false)
			{
				MoveSingleFile(path, fromRoot, toRoot);

#if __DEV
				// verify checksum of the copied file

				char relativeFilePath[RAGE_MAX_PATH] = {0};
				formatf(relativeFilePath, "%s", &path[strlen(fromRoot) + 1]);

				char to[RAGE_MAX_PATH] = {0};
				formatf(to, "%s\\%s", toRoot, relativeFilePath);

				u8 checksum[Sha1::SHA1_DIGEST_LENGTH] = {0};
				AssertVerify(ChecksumFile(to, checksum));
				AssertVerify(memcmp(mapData->m_PatchedChecksum, checksum, sizeof(checksum)) == 0);
#endif
			}
		}

		return true;
	}

	bool MoveNonDeltaFiles(const char *pathname, const char* fromRoot, const char *to, bool moveExes)
	{
		bool result = true;
		const fiDevice *dev = fiDevice::GetDevice(pathname);
		if(AssertVerify(dev != NULL))
		{
			fiFindData data;
			fiHandle handle = dev->FindFileBegin(pathname, data);
			if(fiIsValidHandle(handle))
			{
				while(result)
				{
					char currentPath[RAGE_MAX_PATH];
					if((strcmp(data.m_Name, ".") != 0) && (strcmp(data.m_Name, "..") != 0))
					{
						// Found a directory, so deal with it
						if((data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
						{
							// Build the new pathname to use
							formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);

							// Recurse
							if(!MoveNonDeltaFiles(currentPath, fromRoot, to, moveExes))
							{
								dev->FindFileEnd(handle);
								return false;
							}
						}
						else
						{
							formatf(currentPath, sizeof(currentPath), "%s\\%s", pathname, data.m_Name);

							if(moveExes == false)
							{
								const char* ext = ASSET.FindExtensionInPath(currentPath);
								bool isExe = (ext != NULL) && (stricmp(ext, ".exe") == 0);
								if(isExe == false)
								{
									MoveNonDeltaFile(currentPath, fromRoot, to);
								}
							}
							else
							{
								MoveNonDeltaFile(currentPath, fromRoot, to);
							}
						}
					}
					result = dev->FindFileNext(handle, data);
				}
				dev->FindFileEnd(handle);
			}
		}
		return true;
	}

	bool CreateUniqueDirectory(char (&path)[RAGE_MAX_PATH], const char* base)
	{
		const unsigned maxAttempts = 10;
		unsigned numAttempts = 0;
		char tmp[RAGE_MAX_PATH] = {0};

		memset(path, 0, sizeof(path));

		if(!AssertVerify((base != NULL) && (base[0] != '\0')))
		{
			return false;
		}

		while(true)
		{
			numAttempts++;

			formatf(tmp, "%s\\Temp_%u", base, numAttempts);

			if(ASSET.Exists(tmp, "") == false)
			{
				if(AssertVerify(ASSET.CreateLeadingPath(tmp)))
				{
					safecpy(path, tmp);
					return true;
				}
				else
				{
					return false;
				}
			}

			if(numAttempts >= maxAttempts)
			{
				break;
			}
		}

		return false;
	}

	bool ApplyPatch(const char* current,
					const char* patchData,
					const char* output,
					const char* languageCode)
	{
		if(!AssertVerify(m_ScratchBuffer))
		{
			DisplayError(SID_ERROR_OUT_OF_MEMORY, false);
			return false;
		}

		if(AssertVerify(GetProcessExeDirectory(m_LocalizationXmlFilePath)))
		{
			safecat(m_LocalizationXmlFilePath, "localization.xml");
		}

		Assert(ASSET.Exists(m_LocalizationXmlFilePath, ""));

		// default to English (language code 1033) if language is unknown
		m_Localization.Init(m_LocalizationXmlFilePath, languageCode, "1033");
		
		safecpy(m_PatchDirectory, patchData);
		formatf(m_ManifestPath, "%s\\manifest.dat", patchData);
		formatf(m_PatchDataDirectory, "%s\\data", patchData);
		safecpy(m_CurrentDirectory, current);
		safecpy(m_OutputDirectory, output);
		
		// make sure the output directory doesn't exist, because if someone passes us
		// for example C:\Windows as our output directory, we delete it later!
		if(ASSET.Exists(m_OutputDirectory, ""))
		{
			if(!AssertVerify(CreateUniqueDirectory(m_OutputDirectory, output)))
			{
				DisplayError(SID_ERROR_COULD_NOT_CREATE_OUTPUT_DIRECTORY, false);
				return false;
			}
		}

		if(!AssertVerify(ASSET.Exists(m_PatchDataDirectory, "")))
		{
			// patch data directory doesn't exist
			DisplayError(SID_ERROR_PATH_DOES_NOT_EXIST, m_PatchDataDirectory, false);
			return false;
		}

		if(!AssertVerify(ASSET.Exists(m_CurrentDirectory, "")))
		{
			// directory containing the data to patch doesn't exist
			DisplayError(SID_ERROR_PATH_DOES_NOT_EXIST, m_CurrentDirectory, false);
			return false;
		}

		if(!AssertVerify(ReadManifestFile()))
		{
			// failed to read manifest file
			DisplayError(SID_ERROR_FAILED_TO_READ_MANIFEST, m_ManifestPath, false);
			return false;
		}

#if __DEV
		m_Timer.Reset();
		m_TotalBytesToProcess = GetDirectorySize(m_PatchDataDirectory);
		Assert(m_TotalBytesToProcess > 0);
#endif

		AssertVerify(ProcessDirectory(m_PatchDataDirectory));

		// move all files from the temp folder to the final folder
		Displayf("\nMoving patched files from:\n\t'%s'\n\tto\n\t'%s'", m_OutputDirectory, m_CurrentDirectory);
		AssertVerify(MovePatchedFiles(m_OutputDirectory, m_OutputDirectory, m_CurrentDirectory, false));

		Displayf("\nMoving new/non-delta files from:\n\t'%s'\n\tto\n\t'%s'", m_PatchDataDirectory, m_CurrentDirectory);
		AssertVerify(MoveNonDeltaFiles(m_PatchDataDirectory, m_PatchDataDirectory, m_CurrentDirectory, false));

		// move the exe last so that if anything failed, the game's patcher will see an older version of the exe and try to patch again
		Displayf("\nMoving exes:");
		AssertVerify(MovePatchedFiles(m_OutputDirectory, m_OutputDirectory, m_CurrentDirectory, true));
		AssertVerify(MoveNonDeltaFiles(m_PatchDataDirectory, m_PatchDataDirectory, m_CurrentDirectory, true));

		Shutdown(false);

		return true;
	}

	static const u64 SIZE_OF_SCRATCH_BUFFER = 4 * 1024 * 1024; // 4 MB
	u8* m_ScratchBuffer;

#if __DEV
	float m_Progress;
	u64 m_TotalBytesToProcess;
	u64 m_NumBytesProcessed;
	sysTimer m_Timer;
#endif

	char m_PatchDirectory[RAGE_MAX_PATH];
	char m_ManifestPath[RAGE_MAX_PATH];
	char m_LocalizationXmlFilePath[RAGE_MAX_PATH];
	char m_PatchDataDirectory[RAGE_MAX_PATH];
	char m_CurrentDirectory[RAGE_MAX_PATH];
	char m_OutputDirectory[RAGE_MAX_PATH];

	struct FileMetadata
	{
		FileMetadata()
		{
			memset(m_OriginalPath, 0, sizeof(m_OriginalPath));
			memset(m_OriginalChecksum, 0, sizeof(m_OriginalChecksum));
			memset(m_PatchedChecksum, 0, sizeof(m_PatchedChecksum));
			m_IsNewFile = false;
			m_IsDelta = false;
			m_IsOverlay = false;
		}

		FileMetadata(const char* originalPath,
					 u8 (&originalChecksum)[Sha1::SHA1_DIGEST_LENGTH],
					 u8 (&patchedChecksum)[Sha1::SHA1_DIGEST_LENGTH],
					 bool isNewFile,
					 bool isDelta,
					 bool isOverlay)
		{
			safecpy(m_OriginalPath, originalPath);
			memcpy(m_OriginalChecksum, originalChecksum, sizeof(m_OriginalChecksum));
			memcpy(m_PatchedChecksum, patchedChecksum, sizeof(m_PatchedChecksum));
			m_IsNewFile = isNewFile;
			m_IsDelta = isDelta;
			m_IsOverlay = isOverlay;
		}

		char m_OriginalPath[rage::RAGE_MAX_PATH];
		u8 m_OriginalChecksum[Sha1::SHA1_DIGEST_LENGTH];
		u8 m_PatchedChecksum[Sha1::SHA1_DIGEST_LENGTH];
		bool m_IsNewFile;
		bool m_IsDelta;
		bool m_IsOverlay;
	};

	struct FileMetadataMapEquals
	{
		bool operator ()(const char * left, const char * right) const {return strcmp(left, right) == 0;}
	};

	typedef atMap<const char*, FileMetadata, atMapHashFn<const char*>, FileMetadataMapEquals> FileMetadataMap;
	typedef atMap<const char*, FileMetadata, atMapHashFn<const char*>, FileMetadataMapEquals>::Iterator FileMetadataMapIterator;

	FileMetadataMap m_FileMetadataMap;
	CLocalization m_Localization;
};

#define USE_WIN_MAIN !__DEV

#if USE_WIN_MAIN
// rage wants this
int Main() {return 0;}

int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
#else
int Main()
#endif
{
  	int argc = __argc;
  	char **argv = __argv;

	if(AssertVerify(argc >= 4))
	{
		const char* oldDir = argv[1];
		const char* newDir = argv[2];
		const char* tempDir = argv[3];

		const char* languageCode = "1033"; // default to English
		if(AssertVerify(argc >= 5))
		{
			languageCode = argv[4];
		}

		App app;
		if(AssertVerify(app.ApplyPatch(oldDir,
									   newDir,
									   tempDir,
									   languageCode)))
		{
			return 0;
		}
	}

	return -1;
}
