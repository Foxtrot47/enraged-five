::@ECHO OFF

set nsisdir=C:\Program Files (x86)\NSIS\
set nsistool=makensis.exe
set nsiscmd=/v4
set patchdir=%RS_CODEBRANCH%\extra\patchbuilder
set patchfile=%patchdir%\GTAV_PatchBuilder.nsi
set outputFolder=%RS_CODEBRANCH%\extra\patchbuilder\Output

cd /d %nsisdir%
makensis.exe /v4 %patchfile%
%RS_TOOLSROOT%\bin\python\Python-Portable.exe %RS_TOOLSROOT%\script\coding\launcher\make-manifest-and-hashes.py %RS_CODEBRANCH%\extra\patchbuilder\Output\
exit



