; VersionInfo.nsi
;
; This script shows you how to add version information to an installer.
; Windows shows this information on the Version tab of the File properties.

;--------------------------------

Name "Version Info"

;--------------------------------
;Version Information

  VIProductVersion "0.0.0.0"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "Grand Theft Auto V"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" "Grand Theft Auto V"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "Rockstar Games."
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalTrademarks" "(C) Rockstar Games. All rights reserved"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "(C) Rockstar Games. All rights reserved"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "Grand Theft Auto V - Patch"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "0.0.0.0"

;--------------------------------

Section ""

SectionEnd