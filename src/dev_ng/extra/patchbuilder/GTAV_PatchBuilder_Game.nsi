;Grand Theft Auto V Data Update Installer 1.0.0.0 
;Rockstar Games Leeds
;Developer: Nick Robey
;--------------------------------

Unicode true
SetCompressor /SOLID lzma

!define MUI_ICON "Global\Images\GTAV.ico"

# Includes
!include "Global\Includes\MUI2.nsh"
!include "Global\Includes\Lang.nsh"
!include "Global\Includes\FileFunc.nsh"
!include "Global\Includes\LogicLib.nsh"
!include "Global\Includes\VersionInfo.nsh"

; Defines 
!define COMPANY 		"Rockstar Games"
!define REGKEY 			"Software\Rockstar Games\Grand Theft Auto V\"
!define SUBREGKEY 		"InstallFolder"
!define PATCHKEY		"PatchVersion"
!define SUBLANGREGKEY   "Language"
!define URL				"http://www.rockstargames.com/V"
!define PATCHFILENAME	"GTA_V_Patch_"
!define PATCHFILENUMBER "1_0_0_0"
!define PROJECTTILE		"Grand Theft Auto V"
!define PROJECTEXE		".exe"
!define GAMEEXE 		"GTA5.exe"
!define SPLITTER		"\\"
!define IGNOREEXECHECK	"false"

; Folders
!define EXEDATA "ExeName"
!define UPDATERPF "UpdateName"
!define METAFILE "MetaName"
!define OUTPUTFOLDER "OutputName"


; Update RPF stuff
!define UPDATEINSTALLLOC "update\\"
!define METAINSTALLOC "x64\\"

; The name of the installer.exe file
OutFile "${OUTPUTFOLDER}${PATCHFILENAME}${PATCHFILENUMBER}${PROJECTEXE}"
name "${PROJECTTILE}"


; Set global variables.
Var /GLOBAL ProgramInstallLocation
Var /GLOBAL ExeVersionNumber
Var /Global ProgramLanguage
Var dWelcome
Var HControl


# Setup EULA Screen
!define MUI_PAGE_HEADER_TEXT "${PROJECTTILE}"


#Pages
!define CUST_PAGE_WELCOME_TITLE "GTA V - Update"
Page custom CUST_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE $(MUILicense)
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES


; on intialisation, setup the language select box. 
Function .onInit

Call GetVersionNumber ; Get the version number from the exe. 



!define MUI_LANGDLL_WINDOWTITLE "${PROJECTTILE} - Setup"
!define MUI_LANGDLL_ALLLANGUAGES

!insertmacro MUI_LANGDLL_DISPLAY_MOD
Call SetLanguageStrings 
call CheckGameIsInstalled
FunctionEnd



; Get the version number from the games EXE
Function GetVersionNumber
${GetFileVersion} "$ProgramInstallLocation${GAMEEXE}" $ExeVersionNumber
FunctionEnd

# check the game is installed, we check the reg key that the main installer creates. 
Function CheckGameIsInstalled

ReadRegStr $ProgramInstallLocation HKLM "${REGKEY}" "${SUBREGKEY}"
${If} ${IGNOREEXECHECK} == "false"
	${If} $ProgramInstallLocation == "" 
		MessageBox MB_OK $GameNotInstalled
		Abort
	${EndIf}
${EndIf}
FunctionEnd


# Create the custom welcome page.
Function CUST_PAGE_WELCOME  

    ; Welcome Page
    !insertmacro MUI_HEADER_TEXT $WelcomeHead1 $WelcomeHead2
    
    nsDialogs::Create /NOUNLOAD 1018
    pop $dWelcome
    
    ${If} $dWelcome == error
        MessageBox MB_ICONSTOP|MB_OK $WelcomeError
        Abort
    ${EndIf}
     
    GetDlgItem $0 $HWNDPARENT 1                                                                             ; Gets current window
    nsDialogs::CreateControl /NOUNLOAD STATIC ${WS_VISIBLE}|${WS_CHILD} 0 0 0 85% 60 $WelcomeMsg2         ;|${WS_CLIPSIBLINGS|${SS_CENTER} 145
    pop $HControl
    SetCtlColors $HControl 0x000000  ;transparent
   
    nsDialogs::Show
    
    Pop $3  
    StrCmp $3 "Cancel" 0 +2                                                             ; Catched the User Cancel.
    Quit

    FindWindow $0 "${WNDCLASS}" "${WNDTITLE}"                                           ; Runniing check
    ${If} $0 > 0
        ;MessageBox MB_ICONSTOP|MB_OK $RunError
        Quit
    ${EndIf}
    
FunctionEnd

# Load in the language file for the welcome screen. 
# strings stored in Lang.nsh
Function SetLanguageStrings

${SWITCH} $LANGUAGE
	${Case} 1033
		Call AddEnglish        
		${Break}
	${Case} 1036
		Call AddFrench        
		${Break}
	${Case} 1040
		Call AddItalian        
		${Break}
	${Case} 1034
	   Call AddSpanish        
		${Break}
	${Case} 1031
		Call AddGerman        
		${Break}
	${Case} 1041
		Call AddJapanese        
		${Break}
	${Case} 1049
		Call AddRussian        
		${Break}
	${Case} 1045
		Call AddPolish        
		${Break}
	${Case} 1046
		Call AddPortuguese        
		${Break}
	${Case} 1042
		Call AddKorean        
		${Break}
	${Case} 1028
		Call AddChinese       
		${Break}
	${Case} 2052
		Call AddChineseSimp       
		${Break}
	${Case} 3082
		Call AddSpanishMexican     
		${Break}
	${Default}
		Call AddEnglish        
		${Break}
${EndSwitch}

FunctionEnd


Section 

ClearErrors
SetOverwrite try

SetOutPath $ProgramInstallLocation
File "${EXEDATA}"

SetOutPath "$ProgramInstallLocation${SPLITTER}${UPDATEINSTALLLOC}"
File "${UPDATERPF}"

SetOutPath "$ProgramInstallLocation${SPLITTER}${UPDATEINSTALLLOC}${SPLITTER}${METAINSTALLOC}"
File "${METAFILE}"

; Write Patch version to Reg Key.
${GetFileVersion} "$ProgramInstallLocation${SPLITTER}${GAMEEXE}" $ExeVersionNumber
WriteRegStr HKLM "${REGKEY}" "${PATCHKEY}" "$ExeVersionNumber"

; Fix bug with du-DU
ReadRegStr $ProgramLanguage HKLM "${REGKEY}" "${SUBLANGREGKEY}"
${If}  $ProgramLanguage == "du-DU"
	WriteRegStr HKLM "${REGKEY}" "${SUBLANGREGKEY}" "de-DE"
${EndIf}

SectionEnd




