::@ECHO OFF

set nsisdir=C:\Program Files (x86)\NSIS\
set nsistool=makensis.exe
set nsiscmd=/v4
set patchdir=X:\gta5\src\dev_ng\extra\patchbuilder
set patchfile=%patchdir%\GTAV_PatchBuilderTest.nsi

cd /d %nsisdir%
makensis.exe /v4 /DMODIFIER=%1 /DPATCHFILENUMBER=%2 %patchfile% 
exit



