!include LogicLib.nsh

!define sysOSVERSIONINFO '(i, i, i, i, i, &t128) i'
!define sysGetVersionEx 'kernel32::GetVersionExA(i) i'
!define sysVER_PLATFORM_WIN32_NT 2
!define USLABEL "(United States)"

!macro MUI_LANGUAGE_MOD LANGUAGE LANG_NLF
  ;Include a language
  !verbose push
  !verbose ${MUI_VERBOSE}
  !insertmacro MUI_INSERT
  LoadLanguageFile "${LANG_NLF}"				;"${NSISDIR}\Contrib\Language files\${LANGUAGE}.nlf"

  ;Add language to list of languages for selection dialog
  !ifndef MUI_LANGDLL_LANGUAGES
    !define MUI_LANGDLL_LANGUAGES "'${LANGUAGE} ${USLABEL}' '${LANG_${LANGUAGE}}' "
    !define MUI_LANGDLL_LANGUAGES_CP "'${LANGUAGE}' '${LANG_${LANGUAGE}}' '${LANG_${LANGUAGE}_CP}' "
	
  !else
    !ifdef MUI_LANGDLL_LANGUAGES_TEMP
      !undef MUI_LANGDLL_LANGUAGES_TEMP
    !endif
    !define MUI_LANGDLL_LANGUAGES_TEMP "${MUI_LANGDLL_LANGUAGES}"
    !undef MUI_LANGDLL_LANGUAGES

    !ifdef MUI_LANGDLL_LANGUAGES_CP_TEMP
      !undef MUI_LANGDLL_LANGUAGES_CP_TEMP
    !endif
    !define MUI_LANGDLL_LANGUAGES_CP_TEMP "${MUI_LANGDLL_LANGUAGES_CP}"
    !undef MUI_LANGDLL_LANGUAGES_CP

	!if ${LANGUAGE} == "SpanishInternational"
    !define MUI_LANGDLL_LANGUAGES "'Spanish (Mexican)' '${LANG_${LANGUAGE}}' ${MUI_LANGDLL_LANGUAGES_TEMP}"
    !define MUI_LANGDLL_LANGUAGES_CP "'Spanish (Mexican)' '${LANG_${LANGUAGE}}' '${LANG_${LANGUAGE}_CP}' ${MUI_LANGDLL_LANGUAGES_CP_TEMP}"
	!else
		!if ${LANGUAGE} == "Chinese"
		!define MUI_LANGDLL_LANGUAGES "'Chinese (Traditional)' '${LANG_${LANGUAGE}}' ${MUI_LANGDLL_LANGUAGES_TEMP}"
		!define MUI_LANGDLL_LANGUAGES_CP "'Chinese (Traditional)' '${LANG_${LANGUAGE}}' '${LANG_${LANGUAGE}_CP}' ${MUI_LANGDLL_LANGUAGES_CP_TEMP}"
		!else
			
			!if ${LANGUAGE} == "SimpChinese"
			!define MUI_LANGDLL_LANGUAGES "'Chinese (Simplified)' '${LANG_${LANGUAGE}}' ${MUI_LANGDLL_LANGUAGES_TEMP}"
			!define MUI_LANGDLL_LANGUAGES_CP "'Chinese (Simplified)' '${LANG_${LANGUAGE}}' '${LANG_${LANGUAGE}_CP}' ${MUI_LANGDLL_LANGUAGES_CP_TEMP}"
			!else
			!define MUI_LANGDLL_LANGUAGES "'${LANGUAGE}' '${LANG_${LANGUAGE}}' ${MUI_LANGDLL_LANGUAGES_TEMP}"
			!define MUI_LANGDLL_LANGUAGES_CP "'${LANGUAGE}' '${LANG_${LANGUAGE}}' '${LANG_${LANGUAGE}_CP}' ${MUI_LANGDLL_LANGUAGES_CP_TEMP}"
			!endif
		!endif
	!endif
	
  !endif

  !verbose pop
 
!macroend


;--------------------------------
;Language selection

!macro MUI_LANGDLL_DISPLAY_MOD

!verbose push
!verbose ${MUI_VERBOSE}

!insertmacro MUI_LANGDLL_VARIABLES

!insertmacro MUI_DEFAULT MUI_LANGDLL_WINDOWTITLE "Installer Language"
!insertmacro MUI_DEFAULT MUI_LANGDLL_INFO "Please select a language."

!ifdef MUI_LANGDLL_REGISTRY_VARAIBLES

ReadRegStr $mui.LangDLL.RegistryLanguage "${MUI_LANGDLL_REGISTRY_ROOT}" "${MUI_LANGDLL_REGISTRY_KEY}" "${MUI_LANGDLL_REGISTRY_VALUENAME}"

${if} $mui.LangDLL.RegistryLanguage != ""
;Set default langauge to registry language
StrCpy $LANGUAGE $mui.LangDLL.RegistryLanguage
${endif}

!endif

!ifdef NSIS_CONFIG_SILENT_SUPPORT
${unless} ${Silent}
!endif

!ifndef MUI_LANGDLL_ALWAYSSHOW
!ifdef MUI_LANGDLL_REGISTRY_VARAIBLES
${if} $mui.LangDLL.RegistryLanguage == ""
!endif
!endif

;Show langauge selection dialog
!ifdef MUI_LANGDLL_ALLLANGUAGES
LangDLL::LangDialog "${MUI_LANGDLL_WINDOWTITLE}" "${MUI_LANGDLL_INFO}" A ${MUI_LANGDLL_LANGUAGES} ""
!else
LangDLL::LangDialog "${MUI_LANGDLL_WINDOWTITLE}" "${MUI_LANGDLL_INFO}" AC ${MUI_LANGDLL_LANGUAGES_CP} ""
!endif

Pop $LANGUAGE
${if} $LANGUAGE == "cancel"
Abort
${endif}

!ifndef MUI_LANGDLL_ALWAYSSHOW
!ifdef MUI_LANGDLL_REGISTRY_VARAIBLES
${endif}
!endif
!endif


!ifdef NSIS_CONFIG_SILENT_SUPPORT
${endif}
!endif

!verbose pop

!macroend

;Languages
!insertmacro MUI_LANGUAGE_MOD "English" "Global\Language files\English.nlf"         ;first language is the default language
!insertmacro MUI_LANGUAGE_MOD "French" "Global\Language files\French.nlf"
!insertmacro MUI_LANGUAGE_MOD "Italian" "Global\Language files\Italian.nlf"
!insertmacro MUI_LANGUAGE_MOD "German" "Global\Language files\German.nlf"
!insertmacro MUI_LANGUAGE_MOD "Spanish" "Global\Language files\Spanish.nlf"
!insertmacro MUI_LANGUAGE_MOD "SpanishInternational" "Global\Language files\SpanishInternational.nlf"
!insertmacro MUI_LANGUAGE_MOD "Russian" "Global\Language files\Russian.nlf"
!insertmacro MUI_LANGUAGE_MOD "Japanese" "Global\Language files\Japanese.nlf"
!insertmacro MUI_LANGUAGE_MOD "Polish" "Global\Language files\Polish.nlf" 
!insertmacro MUI_LANGUAGE_MOD "Portuguese" "Global\Language files\Portuguese.nlf" 
!insertmacro MUI_LANGUAGE_MOD "Korean" "Global\Language files\Korean.nlf"
!insertmacro MUI_LANGUAGE_MOD "Chinese" "Global\Language files\Chinese.nlf"
!insertmacro MUI_LANGUAGE_MOD "SimpChinese" "Global\Language files\SimpChinese.nlf"

LicenseLangString MUILicense  ${LANG_ENGLISH} "Global\EULA\EN\License.rtf" 
LicenseLangString MUILicense  ${LANG_ITALIAN} "Global\EULA\IT\License.rtf" 
LicenseLangString MUILicense  ${LANG_FRENCH} "Global\EULA\FR\License.rtf" 
LicenseLangString MUILicense  ${LANG_GERMAN} "Global\EULA\GER\License.rtf" 
LicenseLangString MUILicense  ${LANG_SPANISH} "Global\EULA\SPA\License.rtf"
LicenseLangString MUILicense  ${LANG_JAPANESE} "Global\EULA\JPN\License.rtf"
LicenseLangString MUILicense  ${LANG_RUSSIAN} "Global\EULA\RUS\License.rtf"
LicenseLangString MUILicense  ${LANG_POLISH} "Global\EULA\POL\License.rtf"
LicenseLangString MUILicense  ${LANG_PORTUGUESE} "Global\EULA\POR\License.rtf"
LicenseLangString MUILicense  ${LANG_KOREAN} "Global\EULA\KOR\License.rtf"
LicenseLangString MUILicense  ${LANG_CHINESE} "Global\EULA\CHI\License.rtf"
LicenseLangString MUILicense  ${LANG_SIMPCHINESE} "Global\EULA\CHIS\License.rtf"
LicenseLangString MUILicense  ${LANG_SPANISHINTERNATIONAL} "Global\EULA\MEX\License.rtf"

var AdminError		
var OverwriteError 
var GameNotInstalled
var OSError

var GlobalHeader

var WelcomeHead1
var WelcomeHead2
var WelcomeMsg1
var WelcomeError
var WelcomeMsg2

var InstallHead
var InstallMsg

var RunError
var RunErrorLauncher

var InitINSTALL
var RunUpdateErr
var AppErr
var NewerVerErr


var LangTitle 
var LangSelect
var AbortWarning
var TransferError

var OptHeader1
var OptHeader2
var ModifyHead					
var ModifyText	
var RemoveHead  
var RemoveText  
var ModHeader1
var ModHeader2
var LSShortcutName


Function AddLangSelectEng
	StrCpy $GlobalHeader "- Setup"
    StrCpy $LangTitle  "Installer Language" 
    StrCpy $LangSelect "Please select the language of the installer."
FunctionEnd

Function AddEnglish
	StrCpy $AdminError "$(^Name) requires administrator privileges to install.$\nPlease re-run this application as Administrator."
	StrCpy $OverwriteError "Unable to install $(^Name) update, please check $(^Name) isn't running and try again. For more information please visit www.rockstargames.com/support"
	StrCpy $GameNotInstalled "Sorry, $(^Name) isn't installed on this system. Please reinstall the game and try again."
	StrCpy $OSError     "Your system is incompatible with $(^Name)."
	
	StrCpy $WelcomeHead1 "Welcome."
	StrCpy $WelcomeHead2 "This wizard will guide you through the installation of $(^Name). You can get help at any time on www.rockstargames.com/support"
	StrCpy $WelcomeMsg1 "Welcome to the $(^Name) update installer."
	StrCpy $WelcomeMsg2 "This will install the latest features for $(^Name).$\n$\n$(^ClickNext)"

	StrCpy $InstallHead "Installing Files."
	StrCpy $InstallMsg  "Installing $(^Name) Files."

	StrCpy $RunError    "$(^Name) is currently running.$\n You Must close the application in order to complete this process."
	StrCpy $RunErrorLauncher "$(^Name) Launcher is currently running.$\n You Must close the application in order to complete this process."
	StrCpy $InitINSTALL  "Initializing $(^Name) Installer."
	StrCpy $WelcomeError "Error: Tying to run $(^Name) Patch."

	StrCpy $AbortWarning "Are you sure you want to quit $(^Name) Setup?"
	StrCpy $LSShortcutName "GTAV Language Switcher"
FunctionEnd

Function un.AddEnglish
	StrCpy $WelcomeHead1 "Welcome."
	StrCpy $WelcomeHead2 "This wizard will guide you through the installation of $(^Name)."
	StrCpy $RunError     "$(^Name) is currently running.$\n You must close the application in order to complete this process."   
	StrCpy $RunUpdateErr "$(^Name) is running and can not be updated. Please shut down all Rockstar Game Applications."
	StrCpy $AppErr       "All Rockstar Applications must be removed before removing $(^Name)"
	
	StrCpy $OptHeader1	 "Welcome to $(^Name) setup maintenance program."
	StrCpy $OptHeader2	 "This program lets you modify the current installation. Click one of the options below, and the click Next to proceed."
	StrCpy $ModifyHead	 "Modify."					
	StrCpy $ModifyText	 "Select new program features to add or select currently installed features to remove."
	StrCpy $RemoveHead   "Remove."
	StrCpy $RemoveText   "Remove all installed features."
	StrCpy $ModHeader1	 "Select Installation Options for $(^Name)." 
	StrCpy $ModHeader2   "Select the features you want to retain, and deselect the features you want to remove."

FunctionEnd

Function AddLangSelectFre
StrCpy $GlobalHeader "- Setup"
    StrCpy $LangTitle   "Langue de l'installation" 
    StrCpy $LangSelect  "Selectionnez la langue de l’installation."
FunctionEnd

Function AddFrench
	StrCpy $AdminError "Vous devez disposer des droits d'administrateur pour installer $(^Name).$\nConnectez-vous en tant qu'administrateur et relancez cette application."
	StrCpy $OverwriteError "Impossible d'installer la mise a jour de $(^Name), veuillez verifier que $(^Name) n'est pas en cours d'execution et reessayez. Pour plus d'informations veuillez consulter le site www.rockstargames.com/support"
	StrCpy $GameNotInstalled "Desole, $(^Name) n'est pas installe sur ce systeme. Veuillez reinstaller le jeu et reessayer."
	StrCpy $OSError     "Votre systeme n'est pas compatible avec $(^Name)."
	
	StrCpy $WelcomeHead1 "Bienvenue."
	StrCpy $WelcomeHead2 "Cet assistant va vous guider dans le processus d'installation de $(^Name). Vous pouvez aussi obtenir de l'aide à tout moment sur le site www.rockstargames.com/support"
	StrCpy $WelcomeMsg1 "Bienvenue dans le programme de mise a jour de $(^Name)."
	StrCpy $WelcomeMsg2 "Ceci installera les dernières fonctionnalités pour $(^Name).$\n$\nCliquez sur Suivant pour continuer."

	StrCpy $InstallHead "Installation des fichiers."
	StrCpy $InstallMsg  "Installation des fichiers  de $(^Name)."

	StrCpy $RunError    "$(^Name) est en cours d'execution. $\n Fermez l'application pour pouvoir terminer ce processus."
	StrCpy $RunErrorLauncher "Le lanceur de $(^Name) est en cours d'execution. $\n Fermez l'application pour pouvoir terminer ce processus."
	StrCpy $InitINSTALL  "Initialisation du programme d'installation de $(^Name)."
	StrCpy $WelcomeError "Erreur : Impossible d'executer la mise a jour de $(^Name)."
	StrCpy $AbortWarning "Voulez-vous vraiment quitter l'assistant d'installation de $(^Name) ?"
	StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
	
	
	StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
	StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
	StrCpy $LSShortcutName "Sélecteur de langue pour GTAV"
FunctionEnd

Function un.AddFrench
    StrCpy $WelcomeHead1 "Bienvenue."
	StrCpy $WelcomeHead2 "Cet assistant d'installation va vous guider pendant l'installation du produit $(^Name)."
	StrCpy $RunError     "$(^Name) est actuellement lancй.$\n Vous devez quitter l'application pour terminer ce processus."   
	StrCpy $RunUpdateErr "$(^Name) est lancй et ne peut pas кtre mis а jour. Veuillez quitter toutes les applications Rockstar Games."
	StrCpy $AppErr       "Toutes les applications Rockstar doivent кtre supprimйes avant de pouvoir supprimer $(^Name)."
		
	StrCpy $OptHeader1	 "Bienvenue dans le programme de maintenance pour $(^Name)."
	StrCpy $OptHeader2	 "Ce programme vous permet de modifier l'installation actuelle. Cliquez sur l'une des options ci-aprиs, puis sur Suivant pour continuer."
	StrCpy $ModifyHead	 "Modifier."					
	StrCpy $ModifyText	 "Sйlectionnez de nouvelles fonctionnalitйs а ajouter ou des fonctionnalitйs dйjа installйes а supprimer."
	StrCpy $RemoveHead   "Supprimer."
	StrCpy $RemoveText   "Supprimez toutes les fonctionnalitйs installйes."
	StrCpy $ModHeader1	 "Sйlectionnez vos options d'installation pour $(^Name)." 
	StrCpy $ModHeader2   "Sйlectionnez les fonctionnalitйs а conserver et dйsйlectionnez les fonctionnalitйs а supprimer."
FunctionEnd

Function AddLangSelectIta
	StrCpy $GlobalHeader "- Setup"
    StrCpy $LangTitle   "Lingua programma di installazione" 
    StrCpy $LangSelect  "Seleziona la lingua per il programma di installazione."
FunctionEnd

Function AddItalian
	StrCpy $AdminError "Per poter installare $(^Name) occorre godere dei privilegi di amministratore.$\nRiavvia l'applicazione come Amministratore."
	StrCpy $OverwriteError "Impossibile installare l'aggiornamento per $(^Name), verifica che $(^Name) non sia in esecuzione e riprova. Per maggiori informazioni visita www.rockstargames.com/support"
	StrCpy $GameNotInstalled "Siamo spiacenti, $(^Name) non e installato su questo sistema. Reinstalla il gioco e riprova. "
	StrCpy $OSError     "Il tuo sistema non e compatibile con $(^Name)."
	
	StrCpy $WelcomeHead1 "Benvenuto."
	StrCpy $WelcomeHead2 "Questo programma ti aiuterà a installare $(^Name). Per ricevere aiuto, visita www.rockstargames.com/support."
	StrCpy $WelcomeMsg1 "Benvenuto nel programma di installazione aggiornamento di $(^Name) . "
	StrCpy $WelcomeMsg2 "Questo provvederà a installare le funzionalità più recenti di $(^Name).$\n$\nPer proseguire, scegliere Avanti."

	StrCpy $InstallHead "Installazione dei file in corso... "
	StrCpy $InstallMsg  "Installazione dei file di $(^Name) in corso... "

	StrCpy $RunError    "$(^Name) e attualmente in esecuzione.$\n Per poter completare questa procedura devi prima chiudere l'applicazione. "
	StrCpy $RunErrorLauncher "Il launcher di $(^Name) e attualmente in esecuzione.$\n Per poter completare questa procedura devi prima chiudere l'applicazione."
	StrCpy $InitINSTALL  "Inizializzazione del programma di installazione di $(^Name) in corso... "
	StrCpy $WelcomeError "Errore: tentativo di avviare la patch di $(^Name). "
	StrCpy $AbortWarning "Sei sicuro di voler interrompere l'installazione di $(^Name)? "
	StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
	
	
	StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
	StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
	StrCpy $LSShortcutName "Selettore lingua GTAV"
FunctionEnd

Function un.AddItalian
    StrCpy $WelcomeHead1 "Benvenuto."
	StrCpy $WelcomeHead2 "Questo programma ti guiderа nell'installazione di $(^Name)."
	StrCpy $RunError     "$(^Name) и attualmente in esecuzione.$\n Per concludere il processo, devi chiudere l'applicazione."   
	StrCpy $RunUpdateErr "$(^Name) и in esecuzione e non puт essere aggiornato. Chiudi tutte le applicazioni di Rockstar."
	StrCpy $AppErr       "Per rimuovere $(^Name), occorre rimuovere tutte le applicazioni di Rockstar."
		
	StrCpy $OptHeader1	 "Benvenuto al programma di manutenzione di $(^Name)."
	StrCpy $OptHeader2	 "Questo programma ti permette di modificare lo stato dell'installazione attuale. Per continuare, clicca su una delle opzioni sotto e poi su Avanti."
	StrCpy $ModifyHead	 "Modifica."					
	StrCpy $ModifyText	 "Seleziona le nuove funzioni da aggiungere o seleziona quelle giа installate da rimuovere."
	StrCpy $RemoveHead   "Rimuovi."
	StrCpy $RemoveText   "Rimuovi tutte le funzioni installate."
	StrCpy $ModHeader1	 "Seleziona le opzioni di installazione per $(^Name)." 
	StrCpy $ModHeader2   "Seleziona le funzioni che vuoi mantenere e deseleziona quelle che vuoi rimuovere."
FunctionEnd



Function AddLangSelectGer
StrCpy $GlobalHeader "- Setup"
    StrCpy $LangTitle   "Installationssprache" 
    StrCpy $LangSelect  "Wahle die Sprache fur die Installation aus."
FunctionEnd

Function AddGerman
	StrCpy $AdminError "$(^Name) benotigt fur die Installation Administratorrechte.$\nBitte starte die Anwendung als Administrator neu. "
	StrCpy $OverwriteError "Update fur $(^Name) konnte nicht installiert werden. Bitte uberprufe, dass $(^Name) nicht lauft, und versuche es nochmal. Fur weitere Informationen, besuche bitte www.rockstargames.com/support."
	StrCpy $GameNotInstalled "$(^Name) ist auf diesem System nicht installiert. Bitte installiere das Spiel neu und versuche es nochmal."
	StrCpy $OSError     "Dein System ist mit $(^Name) nicht kompatibel."
	
	StrCpy $WelcomeHead1 "Herzlich willkommen."
	StrCpy $WelcomeHead2 "Dieser Wizard leitet dich durch die Installation von $(^Name). Du kannst dir jederzeit unter www.rockstargames.com/support Hilfe holen."
	StrCpy $WelcomeMsg1 "Willkommen beim Update-Installationsprogramm fur $(^Name)."
	StrCpy $WelcomeMsg2 "Hiermit installierst du die neuesten Features von $(^Name).$\n$\nKlicken Sie auf Weiter, um fortzufahren."

	StrCpy $InstallHead "Dateien werden installiert. "
	StrCpy $InstallMsg  "Dateien fur $(^Name) werden installiert."

	StrCpy $RunError    "$(^Name) lauft zur Zeit.$\n Um diesen Vorgang abzuschlie?en, musst du die Anwendung beenden."
	StrCpy $RunErrorLauncher "$(^Name) Launcher lauft zur Zeit.$\n Um diesen Vorgang abzuschlie?en, musst du die Anwendung beenden."
	StrCpy $InitINSTALL  "Installationsprogramm fur $(^Name) wird initialisiert."
	StrCpy $WelcomeError "Fehler beim Versuch, den Patch fur $(^Name) zu starten. "
	StrCpy $AbortWarning "Bist du sicher, dass du das Setup fur $(^Name) verlassen willst?"
	StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
	
	
	StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
	StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
	StrCpy $LSShortcutName "GTAV-Sprachauswahl"
FunctionEnd

Function un.AddGerman
    StrCpy $WelcomeHead1 "Willkommen."
	StrCpy $WelcomeHead2 "Dieser Assistent fьhrt Sie durch die Installation von $(^Name)."
	StrCpy $RunError     "$(^Name) lдuft gerade.$\n Sie mьssen das Programm beenden, um den Vorgang abzuschlieЯen."   
	StrCpy $RunUpdateErr "$(^Name) lдuft und kann nicht aktualisiert werden. Bitte sдmtliche Rockstar-Programme beenden."
	StrCpy $AppErr       "Sдmtliche Rockstar-Programme mьssen vor dem Deinstallieren von $(^Name) entfernt werden."
		
	StrCpy $OptHeader1	 "Willkommen zum $(^Name) Setup-Wartungs-Programm."
	StrCpy $OptHeader2	 "Mit diesem Programm kцnnen Sie die aktuelle Installation modifizieren. Wдhlen Sie eine der unten aufgefьhrten Optionen und klicken Sie 'Weiter', um fortzufahren."
	StrCpy $ModifyHead	 "Modifizieren."					
	StrCpy $ModifyText	 "Fьgen Sie neue Programm-Features hinzu oder entfernen Sie derzeit installierte Features."
	StrCpy $RemoveHead   "Entfernen."
	StrCpy $RemoveText   "Entfernen Sie sдmtliche installierten Features."
	StrCpy $ModHeader1	 "Installationsoptionen fьr $(^Name) wдhlen." 
	StrCpy $ModHeader2   "Wдhlen Sie die Features, die Sie behalten mцchten und wдhlen Sie die Features ab, die Sie entfernen mцchten."
FunctionEnd

Function AddLangSelectSpa
StrCpy $GlobalHeader "- Setup"
    StrCpy $LangTitle   "Idioma de la instalacion" 
    StrCpy $LangSelect  "Selecciona el idioma de la instalacion."
FunctionEnd

Function AddSpanish
	StrCpy $AdminError "$(^Name) necesita tener permisos de administrador para ser instalado.$\nInicia sesion como Administrador y reinicia esta aplicacion."
	StrCpy $OverwriteError "No se pudo instalar la actualizacion de $(^Name). Comprueba que $(^Name) no este abierto y vuelve a intentarlo de nuevo. Para obtener mas informacion, visita www.rockstargames.com/support"
	StrCpy $GameNotInstalled "$(^Name) no esta instalado en este sistema. Reinstala el juego y vuelve a intentarlo de nuevo."
	StrCpy $OSError     "Tu sistema no es compatible con $(^Name)."
	
	StrCpy $WelcomeHead1 "Bienvenido."
	StrCpy $WelcomeHead2 "Este asistente te guiará en la instalación de $(^Name). Si necesitas ayuda puedes visitar www.rockstargames.com/support."
	StrCpy $WelcomeMsg1 "Bienvenido al instalador de actualizaciones de $(^Name). "
	StrCpy $WelcomeMsg2 "Esto instalará las últimas características para $(^Name).$\n$\nPara continuar, pulsa Siguiente."

	StrCpy $InstallHead "Instalando archivos. "
	StrCpy $InstallMsg  "Instalando archivos de $(^Name). "

	StrCpy $RunError    "$(^Name) se esta ejecutando en estos momentos.$\n Debes cerrar la aplicacion para completar este proceso. "
	StrCpy $RunErrorLauncher "El iniciador de $(^Name) se esta ejecutando en estos momentos.$\n Debes cerrar la aplicacion para completar este proceso."
	StrCpy $InitINSTALL  "Iniciando el instalador de $(^Name). "
	StrCpy $WelcomeError "Error: intentando ejecutar el parche de $(^Name)."
	StrCpy $AbortWarning "?Seguro que quieres salir de la instalacion de $(^Name)? "
	StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
	
	
	StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
	StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
	StrCpy $LSShortcutName "Cambiar idioma en GTA V"
FunctionEnd

Function un.AddSpanish
    StrCpy $WelcomeHead1 "Bienvenido."
	StrCpy $WelcomeHead2 "Este asistente te guiarб durante la instalaciуn de $(^Name)."
	StrCpy $RunError     "$(^Name) se estб ejecutando.$\n Debes cerrar la aplicaciуn para completar el proceso."   
	StrCpy $RunUpdateErr "$(^Name) se estб ejecutando y no puede actualizarse. Cierra todas las aplicaciones de juego de Rockstar."
	StrCpy $AppErr       "Todas las aplicaciones de Rockstar deben eliminarse antes de quitar $(^Name)."
		
	StrCpy $OptHeader1	 "Bienvenido al programa de mantenimiento de $(^Name)."
	StrCpy $OptHeader2	 "Este programa te permite modificar la instalaciуn actual. Haz clic en una de las opciones de debajo y luego haz clic en Siguiente para continuar."
	StrCpy $ModifyHead	 "Modificar."					
	StrCpy $ModifyText	 "Elige caracterнsticas nuevas para aсadнrselas al programa o caracterнsticas instaladas actualmente para eliminarlas."
	StrCpy $RemoveHead   "Borrar."
	StrCpy $RemoveText   "Borra todas las caracterнsticas instaladas."
	StrCpy $ModHeader1	 "Selecciona las opciones de instalaciуn de $(^Name)." 
	StrCpy $ModHeader2   "Selecciona las caracterнsticas que quieras conservar y desactiva las que quieras quitar."
FunctionEnd

Function AddLangSelectRus
StrCpy $GlobalHeader "- Setup"
    StrCpy $LangTitle   "Язык установки" 
    StrCpy $LangSelect  "Пожалуйста, выберите язык установки."
FunctionEnd

Function AddRussian
    StrCpy $LANGUAGE 1049
	StrCpy $AdminError "Для установки $(^Name) требуются права администратора.$\nВыполните вход в учетную запись администратора и перезапустите приложение."
	StrCpy $OverwriteError "Невозможно установить обновление $(^Name). Пожалуйста, убедитесь, что $(^Name) не запущена и повторите попытку. Для детальной информации зайдите, пожалуйста, на сайт www.rockstargames.com/support."
	StrCpy $GameNotInstalled "На этом компьютере не установлена $(^Name). Установите игру заново и повторите попытку."
	StrCpy $OSError     "Ваша система несовместима с игрой $(^Name)."
	
	StrCpy $WelcomeHead1 "Добро пожаловать."
	StrCpy $WelcomeHead2 "Эта программа поможет вам установить игру $(^Name). Дополнительная информация на https://support.rockstargames.com"
	StrCpy $WelcomeMsg1 "Вас приветствует программа установки обновлений для игры $(^Name)."
	StrCpy $WelcomeMsg2 "Это приведет к установке самых новых функций для игры $(^Name).$\n$\nНажмите кнопку 'Далее' для продолжения."

	StrCpy $InstallHead "Идет установка файлов."
	StrCpy $InstallMsg  "Идет установка файлов для игры $(^Name)."

	StrCpy $RunError    "$(^Name) уже запущена.$\n Закройте приложение, чтобы завершить процесс."
	StrCpy $RunErrorLauncher "Программа установки $(^Name) уже запущена.$\n Закройте приложение, чтобы завершить процесс."
	StrCpy $InitINSTALL  "Запуск программы установки игры $(^Name)."
	StrCpy $WelcomeError "Ошибка: попытка запустить патч для $(^Name)."
	StrCpy $AbortWarning "Выйти из программы установки игры $(^Name)? "
	StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
	
	
	StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
	StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
	StrCpy $LSShortcutName "Переключатель языка GTA V"
FunctionEnd 

Function un.AddRussian
    StrCpy $WelcomeHead1 	"????? ??????????."
	StrCpy $WelcomeHead2 	"??? ????????? ??????? ??? ?????????? $(^Name)."
	StrCpy $RunError     	"$(^Name) ? ?????? ?????? ????????. $\n ?? ?????? ??????? ??, ????? ????????? ???????."
	StrCpy $RunUpdateErr 	"$(^Name) ? ?????? ?????? ????????, ? ?? ?????? ????????. ??????????, ???????? ??? ?????????? Rockstar."
	StrCpy $AppErr       	"????? ????????? $(^Name) ?????????? ??????? ??? ?????????? Rockstar"
		
	StrCpy $OptHeader1		"????? ?????????? ? ????????? ????????? $(^Name)."
	StrCpy $OptHeader2		"??? ????????? ????????? ??? ???????? ? ????????????? ???????????. ???????? ?????? ????? ? ??????? ?????? ''?????''."
	StrCpy $ModifyHead		"????????."
	StrCpy $ModifyText		"???????? ??????????, ??????? ?????? ???????? ??? ???????."
	StrCpy $RemoveHead   	"???????."
	StrCpy $RemoveText   	"??????? ??? ????????????? ??????????."
	StrCpy $ModHeader1		"???????? ???????? ?????????? ????????? $(^Name)."
	StrCpy $ModHeader2   	"???????? ??????????, ??????? ?????? ????????, ? ??????? ??????? ? ???, ??????? ?????? ???????."
FunctionEnd 


Function AddLangSelectPol
StrCpy $GlobalHeader "- Setup"
    StrCpy $LangTitle  "Jezyk instalatora" 
    StrCpy $LangSelect "Wybierz jezyk instalatora z opcji ponizej."
FunctionEnd

Function AddPolish
	StrCpy $AdminError "Aby zainstalowac gre $(^Name), musisz miec uprawnienia administratora.$\nUruchom te aplikacje ponownie z uprawnieniami administratora."
	StrCpy $OverwriteError "Nie mozna zainstalowac aktualizacji do $(^Name). Sprawdz, czy gra $(^Name) jest obecznie wlaczona i sprobuj ponownie. Wiecej informacji na: www.rockstargames.com/support"
	StrCpy $GameNotInstalled "Przepraszamy, gra Grand Theft Auto V nie jest zainstalowana na tym systemie. Zainstaluj ja jeszcze raz i sprobuj ponownie."
	StrCpy $OSError     "Twoj system nie jest kompatybilny z gra $(^Name)."
	
	StrCpy $WelcomeHead1 "Witaj"
	StrCpy $WelcomeHead2 "Ten kreator przeprowadzi Cię przez instalację gry $(^Name). Możesz otrzymać pomoc, wchodząc na stronę www.rockstargames.com/support."
	StrCpy $WelcomeMsg1 "Witaj w instalatorze aktualizacji gry $(^Name)."
	StrCpy $WelcomeMsg2 "Program ten zainstaluje najnowsze elementy gry $(^Name).$\n$\nKliknij Dalej, aby kontynuować."

	StrCpy $InstallHead "Trwa instalacja plikow."
	StrCpy $InstallMsg  "Trwa instalacja plikow gry $(^Name)."

	StrCpy $RunError    "Gra $(^Name) jest obecnie uruchomiona.$\n Musisz zamknac aplikacje, by proces dobiegl konca."
	StrCpy $RunErrorLauncher "Program startowy gry $(^Name) jest obecnie uruchomiony.$\n Musisz zamknac aplikacje, by proces dobiegl konca."
	StrCpy $InitINSTALL  "Uruchamianie instalatora gry $(^Name)."
	StrCpy $WelcomeError "Blad przy uruchamianiu aktualizacji gry $(^Name)."
	StrCpy $AbortWarning "Czy na pewno chcesz wyjsc z instalatora gry $(^Name)?"
	StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
	
	
	StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
	StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
	StrCpy $LSShortcutName "Narzędzie wyboru języka GTA V"
FunctionEnd

Function un.AddPolish
	StrCpy $WelcomeHead1 "Witaj."
    StrCpy $WelcomeHead2 "Ten program przeprowadzi cie przez proces instalacji gry $(^Name)."
    StrCpy $RunError    "Gra $(^Name) jest uruchomiona.$\n Musisz zamknac aplikacje, zeby dokonczyc trwajacy proces."   
    StrCpy $RunUpdateErr "Gra $(^Name) jest uruchomiona i nie moze zostac zaktualizowana. Zamknij wszystkie aplikacje Rockstar Games."
    StrCpy $AppErr       "Wszystkie aplikacje Rockstar musza zostac usuniete, zanim mozliwe bedzie usuniecie gry $(^Name)."
		
	StrCpy $OptHeader1	"Witamy w programie instalacyjnym gry $(^Name)."
	StrCpy $OptHeader2	"Program umozliwia ci modyfikacje aktualnej instalacji. Wybierz opcje ponizej i kliknij 'Dalej', zeby kontynuowac."
	StrCpy $ModifyHead	"Zmien."
	StrCpy $ModifyText	"Wybierz nowe funkcje programu do dodania lub juz zainstalowane do usuniecia."
	StrCpy $RemoveHead	"Usun."
	StrCpy $RemoveText	"Usun wszystkie zainstalowane funkcje."
	StrCpy $ModHeader1	"Wybierz opcje instalacji gry $(^Name)."
	StrCpy $ModHeader2	"Wybierz funkcje, ktуre chcesz zachowac, i odznacz te, ktуre chcesz usunac."
FunctionEnd 

Function AddLangSelectBRZ
StrCpy $GlobalHeader "- Setup"
    StrCpy $LangTitle  "Idioma de instalacao" 
    StrCpy $LangSelect "Selecione o idioma de instalacao."
FunctionEnd

Function AddPortuguese
	StrCpy $AdminError "$(^Name) exige privilegios de administrador para instalar.$\nReinicie este aplicativo como Administrador. "
	StrCpy $OverwriteError "Nao foi possivel instalar  a atualizacao de $(^Name). Verifique se $(^Name) nao esta em execucao e tente novamente. Para mais informacoes, visite: www.rockstargames.com/support"
	StrCpy $GameNotInstalled "Desculpe, $(^Name) nao esta instalado neste sistema. Por favor, reinstale o jogo e tente novamente. "
	StrCpy $OSError     "Seu sistema e incompativel com $(^Name)."
	
	StrCpy $WelcomeHead1 "Bem-vindo(a)."
	StrCpy $WelcomeHead2 "Este instalador lhe guiará pelo processo de instalação de $(^Name). Você pode obter ajuda a qualquer momento em www.rockstargames.com/support."
	StrCpy $WelcomeMsg1 "Bem-vindo(a) ao instalador de atualizacao de $(^Name). "
	StrCpy $WelcomeMsg2 "$(^Name) receberá a instalação mais recente.$\n$\nClique em Próximo para continuar."

	StrCpy $InstallHead "Instalando arquivos. "
	StrCpy $InstallMsg  "Instalando arquivos de $(^Name). "

	StrCpy $RunError    "$(^Name) esta sendo executado no momento.$\n Voce deve fechar o aplicativo primeiro para completar este processo. "
	StrCpy $RunErrorLauncher "$(^Name) esta sendo executado no momento.$\n Voce deve fechar a aplicacao primeiro para completar este processo."
	StrCpy $InitINSTALL  "Inicializando o instalador de $(^Name). "
	StrCpy $WelcomeError "Erro: tentando executar o patch de $(^Name). "
	StrCpy $AbortWarning "Tem certeza de que deseja sair do instalador de $(^Name)? "
	StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
	
	
	StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
	StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
	StrCpy $LSShortcutName "Seletor de Idioma do GTAV"
FunctionEnd

Function un.AddPortuguese
	StrCpy $WelcomeHead1	"Bem-vindo."
    StrCpy $WelcomeHead2	"Este assistente irб guiб-lo na instalaзгo de $(^Name)."
    StrCpy $RunError		"$(^Name) estб sendo executado.$\n Й necessбrio encerrar o aplicativo para completar este processo."   
    StrCpy $RunUpdateErr	"$(^Name) estб em andamento e nгo pode ser atualizado. Por favor, encerre todos os Aplicativos de Jogos da Rockstar."
    StrCpy $AppErr			"Todos os Aplicativos da Rockstar devem ser removidos antes de remover $(^Name)"
		
	StrCpy $OptHeader1	 	"Bem-vindo ao programa de manutenзгo da configuraзгo do $(^Name)."
	StrCpy $OptHeader2	 	"Este programa permite que vocк modifique a instalaзгo atual. Clique em uma das opзхes abaixo e clique em Avanзar para continuar."
	StrCpy $ModifyHead	 	"Modificar."					
	StrCpy $ModifyText	 	"Selecione novos recursos do programa para adicionar ou selecione recursos atualmente instalados para remover."
	StrCpy $RemoveHead   	"Remover."
	StrCpy $RemoveText   	"Remova todos os recursos instalados."
	StrCpy $ModHeader1	 	"Selecione as opзхes de instalaзгo do $(^Name)." 
	StrCpy $ModHeader2   	"Marque os recursos que deseja manter, e desmarque os recursos que deseja remover."
FunctionEnd 


Function AddLangSelectKor
StrCpy $GlobalHeader "- Setup"
	StrCpy $LangTitle "설치 관리자 언어" 
    StrCpy $LangSelect "설치 관리자 언어를 선택하십시오."
FunctionEnd

Function AddKorean
	StrCpy $AdminError "$(^Name) 설치를 진행하려면 관리자 권한이 필요합니다.$\n관리자로 이 애플리케이션을 다시 실행하십시오."
	StrCpy $OverwriteError "$(^Name) 업데이트 설치를 할 수 없습니다. $(^Name)이 실행하지 않는지 확인하시고 다시 시도하십시오. 자세한 내용을 보려면 https://support.rockstargames.com/hc/ko 사이트를 방문하십시오."
	StrCpy $GameNotInstalled "죄송합니다. 이 시스템에 $(^Name)가 설치되지 않았습니다. 게임을 다시 설치한 다음 다시 시도하십시오."
	StrCpy $OSError     "사용자의 시스템은 $(^Name)(와)과 호환되지 않습니다."
	
	StrCpy $WelcomeHead1 "환영합니다."
	StrCpy $WelcomeHead2 "이 마법사는 $(^Name) 설치 과정을 안내합니다. 언제든지 www.rockstargames.com/support/ko에서 도움을 얻을 수 있습니다."
	StrCpy $WelcomeMsg1 "$(^Name) 업데이트 설치 관리자에 오신 걸 환영합니다. "
	StrCpy $WelcomeMsg2 "여기에서는 $(^Name)용 최신 기능을 설치합니다.$\n$\n계속하시려면 '다음' 버튼을 눌러 주세요."

	StrCpy $InstallHead "파일을 설치하고 있습니다. "
	StrCpy $InstallMsg  "$(^Name) 파일을 설치하고 있습니다."

	StrCpy $RunError    "$(^Name)(이)가 현재 실행 중입니다.$\n 이 과정을 완료하려면 해당 애플리케이션을 종료해야 합니다."
	StrCpy $RunErrorLauncher "$(^Name) 시작 관리자가 현재 실행 중입니다.$\n 이 과정을 완료하려면 해당 애플리케이션을 종료해야 합니다."
	StrCpy $InitINSTALL  "$(^Name) 설치 관리자를 초기화하고 있습니다. "
	StrCpy $WelcomeError "오류: $(^Name) 패치 실행에 연결 중입니다."
	StrCpy $AbortWarning "정말로 $(^Name) 설치를 종료하시겠습니까? "
	StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
	
	
	StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
	StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
	StrCpy $LSShortcutName "GTAV 언어 변경"
FunctionEnd

Function un.AddKorean
	StrCpy $WelcomeHead1 "?????."
    StrCpy $WelcomeHead2 "? ???? $(^Name) ?? ??? ?????."
	StrCpy $RunError "$(^Name)?(?) ?? ?? ????.$\n ? ??? ???? ????? ??????."   
    StrCpy $RunUpdateErr "$(^Name)?(?) ?? ?? ??? ????? ? ????. ?? Rockstar ?? ????? ??????."
    StrCpy $AppErr "$(^Name)?(?) ????? ?? ?? Rockstar ????? ???? ???."
	
	StrCpy $OptHeader1 "$(^Name) ?? ?? ?? ????? ?? ?? ?????."
	StrCpy $OptHeader2 "? ????? ?? ??? ?????. ?? ?? ? ??? ???? '??'? ???? ??????."
	StrCpy $ModifyHead "??"					
	StrCpy $ModifyText "? ???? ??? ???? ????? ?? ???? ?? ??? ?????."
	StrCpy $RemoveHead "??"
	StrCpy $RemoveText "???? ?? ?? ??? ?????."
	StrCpy $ModHeader1 "$(^Name)? ?? ??? ?????." 
	StrCpy $ModHeader2 "??? ??? ???? ??? ??? ??? ??????."
FunctionEnd

Function AddLangSelectJap
StrCpy $GlobalHeader "- Setup"
	StrCpy $LangTitle "インストーラーの言語" 
    StrCpy $LangSelect "インストールで使用する言語を選択してください。"
FunctionEnd

Function AddJapanese
	StrCpy $AdminError "$(^Name)のインストールには管理者権限が必要です。$\n管理者としてアプリケーションを再起動してください。"
	StrCpy $OverwriteError "$(^Name)のアップデートをインストールできませんでした。$(^Name)がすでに起動されていないことを確認し、もう一度お試しください。詳細はsupport.rockstargames.com/hc/jaをご覧ください。"
	StrCpy $GameNotInstalled "申し訳ございません。このシステムには$(^Name)がインストールされていません。ゲームを再インストールしてもう一度お試しください。"
	StrCpy $OSError     "あなたのシステムは$(^Name)と互換性がありません。"
	
	StrCpy $WelcomeHead1 "ようこそ。"
	StrCpy $WelcomeHead2 "このウィザードは、$(^Name)のインストールをガイドします。また、ヘルプが必要な場合は、support.rockstargames.com/hc/jaでいつでもご確認ください。"
	StrCpy $WelcomeMsg1 "$(^Name)のアップデートインストーラーへようこそ。"
	StrCpy $WelcomeMsg2 "$(^Name)の最新項目をインストールします。$\n$\n続けるには [次へ] をクリックして下さい。"

	StrCpy $InstallHead "ファイルをインストール中です。"
	StrCpy $InstallMsg  "$(^Name)ファイルをインストール中です。"

	StrCpy $RunError    "$(^Name)は現在実行中です。$\n このプロセスを完了させるには、アプリケーションを閉じてください。"
	StrCpy $RunErrorLauncher "$(^Name)のランチャーは現在実行中です。$\n このプロセスを完了させるには、アプリケーションを閉じてください。"
	StrCpy $InitINSTALL  "$(^Name)のインストーラーを初期化しています。"
	StrCpy $WelcomeError "エラー：$(^Name)のパッチを実行しようとしています。"
	StrCpy $AbortWarning "$(^Name)のセットアップを終了しますか？"
	StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
	
	
	StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
	StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
	StrCpy $LSShortcutName "「GTA V」言語切り替え"

FunctionEnd

Function un.AddJapanese
    StrCpy $WelcomeHead1 "????"
    StrCpy $WelcomeHead2 "????????$(^Name)????????????????"
    StrCpy $RunError "???$(^Name)???????$\n????????????????????????????????"   
    StrCpy $RunUpdateErr "$(^Name)??????????????????????????·?????????????????????????"
    StrCpy $AppErr "??????·???????????????????????$(^Name)?????????"
	
    StrCpy $OptHeader1 "$(^Name)???????????????????"
    StrCpy $OptHeader2 "??????????????????????????????????????????1????????????????????"
    StrCpy $ModifyHead "??"					
    StrCpy $ModifyText "??????????????????????????????????????????"
    StrCpy $RemoveHead "??"
    StrCpy $RemoveText "???????????????????"
    StrCpy $ModHeader1 "$(^Name)?????????????????????" 
    StrCpy $ModHeader2 "?????????????????????????????"
FunctionEnd



Function AddLangSelectSpaMex
StrCpy $GlobalHeader "- Setup"
    StrCpy $LangTitle   "Idioma del instalador" 
    StrCpy $LangSelect  "Selecciona el idioma del instalador."
FunctionEnd

Function AddSpanishMexican
	StrCpy $AdminError "Es necesario tener permisos de administrador para instalar $(^Name).$\nInicia sesión como Administrador y reinicia esta aplicación. "
	StrCpy $OverwriteError "No se pudo instalar la actualización de $(^Name), verifica que $(^Name) no esté abierto e intenta de nuevo. Para más información visita www.rockstargames.com/support"
	StrCpy $GameNotInstalled "Lo sentimos, $(^Name) no está instalado en este sistema. Reinstala el juego e inténtalo de nuevo. "
	StrCpy $OSError     "Tu sistema no es compatible con $(^Name)."
	
	StrCpy $WelcomeHead1 "Bienvenido."
	StrCpy $WelcomeHead2 "Este asistente te guiará a través de la instalación de $(^Name). Puedes obtener ayuda en cualquier momento visitando www.rockstargames.com/support."
	StrCpy $WelcomeMsg1 "Bienvenido al instalador de actualizaciones de $(^Name). "
	StrCpy $WelcomeMsg2 "Se instalará la última actualización de $(^Name).$\n$\nPresiona Siguiente para continuar."

	StrCpy $InstallHead "Instalando archivos. "
	StrCpy $InstallMsg  "Instalando archivos de $(^Name). "

	StrCpy $RunError    "$(^Name) se está ejecutando en estos momentos.$\n Debes cerrar la aplicación para completar este proceso. "
	StrCpy $RunErrorLauncher "$(^Name) se está ejecutando en estos momentos.$\n Debes cerrar la aplicación para completar este proceso.   "
	StrCpy $InitINSTALL  "Iniciando el instalador de $(^Name). "
	StrCpy $WelcomeError "Error intentado ejecutar el parche de $(^Name). "
	StrCpy $AbortWarning "¿Seguro que quieres salir de la instalación de $(^Name)?"
	StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
	
	
	StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
	StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
	StrCpy $LSShortcutName "Selector de idioma de GTAV"
FunctionEnd

Function un.AddSpanishMexican
    StrCpy $WelcomeHead1 "Bienvenido."
	StrCpy $WelcomeHead2 "Este asistente te guiarб durante la instalaciуn de $(^Name)."
	StrCpy $RunError     "$(^Name) se estб ejecutando.$\n Debes cerrar la aplicaciуn para completar el proceso."   
	StrCpy $RunUpdateErr "$(^Name) se estб ejecutando y no puede actualizarse. Cierra todas las aplicaciones de juego de Rockstar."
	StrCpy $AppErr       "Todas las aplicaciones de Rockstar deben eliminarse antes de quitar $(^Name)."
		
	StrCpy $OptHeader1	 "Bienvenido al programa de mantenimiento de $(^Name)."
	StrCpy $OptHeader2	 "Este programa te permite modificar la instalaciуn actual. Haz clic en una de las opciones de debajo y luego haz clic en Siguiente para continuar."
	StrCpy $ModifyHead	 "Modificar."					
	StrCpy $ModifyText	 "Elige caracterнsticas nuevas para aсadнrselas al programa o caracterнsticas instaladas actualmente para eliminarlas."
	StrCpy $RemoveHead   "Borrar."
	StrCpy $RemoveText   "Borra todas las caracterнsticas instaladas."
	StrCpy $ModHeader1	 "Selecciona las opciones de instalaciуn de $(^Name)." 
	StrCpy $ModHeader2   "Selecciona las caracterнsticas que quieras conservar y desactiva las que quieras quitar."
FunctionEnd

Function AddLangSelectChi
	StrCpy $GlobalHeader "- Setup"
	StrCpy $LangTitle "安裝程式語言" 
	StrCpy $LangSelect "為安裝程式選擇語言。"
FunctionEnd

Function AddChinese
	StrCpy $AdminError "$(^Name) 需有系統管理員權限才能安裝。$\n請以系統管理員身分重新執行此應用程式。"
	StrCpy $OverwriteError "無法安裝 $(^Name) 更新，請確認 $(^Name) 沒有在執行中並再試一次。如需更多資訊，請前往 www.rockstargames.com/support"
	StrCpy $GameNotInstalled "很抱歉，$(^Name) 並未安裝在此系統中。請重新安裝遊戲，然後再試一次。"
	StrCpy $OSError     "您的系統與 $(^Name) 不相容。"
	
	StrCpy $WelcomeHead1 "歡迎。"
	StrCpy $WelcomeHead2 "此精靈將引導您完成 $(^Name) 的安裝步驟。您隨時可前往 www.rockstargames.com/support 取得協助"
	StrCpy $WelcomeMsg1 "歡迎使用 $(^Name) 更新安裝程式。"
	StrCpy $WelcomeMsg2 "此程式將安裝 $(^Name) 的最新功能。$\n$\n按一下 [下一步(N)] 繼續。"

	StrCpy $InstallHead "正在安裝檔案。"
	StrCpy $InstallMsg  "正在安裝 $(^Name) 檔案。"

	StrCpy $RunError    "$(^Name) 目前正在執行。$\n 您必須關閉應用程式才能完成此程序。"
	StrCpy $RunErrorLauncher "$(^Name) 啟動器目前正在執行中。$\n 您必須關閉應用程式才能完成此程序。"
	StrCpy $InitINSTALL  "正在初始化 $(^Name) 安裝程式。"
	StrCpy $WelcomeError "錯誤：正在嘗試執行 $(^Name) 修補程式。"
	StrCpy $AbortWarning "您確定要退出 $(^Name) 設定程序嗎？"
	StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
	
	
	StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
	StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
	StrCpy $LSShortcutName "GTA 5 語言轉換工具"

FunctionEnd

Function un.AddChinese
    StrCpy $WelcomeHead1 "????"
    StrCpy $WelcomeHead2 "????????$(^Name)????????????????"
    StrCpy $RunError "???$(^Name)???????$\n????????????????????????????????"   
    StrCpy $RunUpdateErr "$(^Name)??????????????????????????·?????????????????????????"
    StrCpy $AppErr "??????·???????????????????????$(^Name)?????????"
	
    StrCpy $OptHeader1 "$(^Name)???????????????????"
    StrCpy $OptHeader2 "??????????????????????????????????????????1????????????????????"
    StrCpy $ModifyHead "??"					
    StrCpy $ModifyText "??????????????????????????????????????????"
    StrCpy $RemoveHead "??"
    StrCpy $RemoveText "???????????????????"
    StrCpy $ModHeader1 "$(^Name)?????????????????????" 
    StrCpy $ModHeader2 "?????????????????????????????"
FunctionEnd

Function AddLangSelectChiSimp
	StrCpy $GlobalHeader "- Setup"
	StrCpy $LangTitle "安装程序语言" 
	StrCpy $LangSelect "请为安装程序选择语言。"
FunctionEnd

Function AddChineseSimp
	StrCpy $LANGUAGE 2052
	StrCpy $AdminError "$(^Name) 需要管理员权限才可安装。$\n请以管理员身份重新运行此应用程序。"
	StrCpy $OverwriteError "无法安装 $(^Name) 更新，请确认 $(^Name) 未运行，并再试一次。有关详细信息，请访问 www.rockstargames.com/support"
	StrCpy $GameNotInstalled "抱歉，此系统上并未安装 $(^Name)。请重新安装游戏并重试。"
	StrCpy $OSError     "您的系统不兼容 $(^Name)。"
	
	StrCpy $WelcomeHead1 "欢迎。"
	StrCpy $WelcomeHead2 "此向导将引导您完成 $(^Name) 的安装。您可随时访问 www.rockstargames.com/support 寻求帮助"
	StrCpy $WelcomeMsg1 "欢迎使用 $(^Name) 更新安装程序。"
	StrCpy $WelcomeMsg2 "此程序将安装 $(^Name) 的最新功能。$\n$\n$(^ClickNext)"

	StrCpy $InstallHead "正在安装文件。"
	StrCpy $InstallMsg  "正在安装 $(^Name) 文件。"

	StrCpy $RunError    "$(^Name) 当前正在运行。$\n您必须关闭应用程序才能完成此过程。"
	StrCpy $RunErrorLauncher "$(^Name) 启动程序正在运行。$\n您必须关闭应用程序才能完成此进程。"
	StrCpy $InitINSTALL  "正在初始化 $(^Name) 安装程序。"
	StrCpy $WelcomeError "错误：正在尝试运行 $(^Name) 补丁。"
	StrCpy $AbortWarning "您确定要退出 $(^Name) 安装程序吗？"
	StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
	
	
	StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
	StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
	StrCpy $LSShortcutName "GTA 5 语言切换器"

FunctionEnd

Function un.AddChineseSimp
    StrCpy $WelcomeHead1 "????"
    StrCpy $WelcomeHead2 "????????$(^Name)????????????????"
    StrCpy $RunError "???$(^Name)???????$\n????????????????????????????????"   
    StrCpy $RunUpdateErr "$(^Name)??????????????????????????·?????????????????????????"
    StrCpy $AppErr "??????·???????????????????????$(^Name)?????????"
	
    StrCpy $OptHeader1 "$(^Name)???????????????????"
    StrCpy $OptHeader2 "??????????????????????????????????????????1????????????????????"
    StrCpy $ModifyHead "??"					
    StrCpy $ModifyText "??????????????????????????????????????????"
    StrCpy $RemoveHead "??"
    StrCpy $RemoveText "???????????????????"
    StrCpy $ModHeader1 "$(^Name)?????????????????????" 
    StrCpy $ModHeader2 "?????????????????????????????"
FunctionEnd


!insertmacro LANGFILE "English" "=" "English" "="
LangString MUI_TEXT_LICENSE_TITLE ${LANG_ENGLISH} "License Agreement"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_ENGLISH} "Please review the license terms before installing $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_ENGLISH} "If you accept the terms of the agreement, click the check box below. You must accept the agreement to install $(^Name). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_ENGLISH} "Press Page Down to see the rest of the agreement."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_ENGLISH} "Installing"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_ENGLISH} "Please wait while $(^Name) is being installed."
LangString MUI_TEXT_FINISH_TITLE ${LANG_ENGLISH} "Install Complete"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_ENGLISH} "Setup was completed successfully. Click Close to finish."
LangString MUI_TEXT_ABORT_TITLE ${LANG_ENGLISH} "The uninstall was aborted."
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_ENGLISH} "Setup was not completed successfully. Click Close to finish."
LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_ENGLISH} "Uninstalling"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_ENGLISH} "Please wait while $(^Name) is being uninstalled."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_ENGLISH} "Uninstallation was completed successfully. Click Close to finish."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_ENGLISH} "Uninstallation Complete"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_ENGLISH} "Uninstallation Aborted"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_ENGLISH} "Setup was not completed successfully. Click Close to finish."


!insertmacro LANGFILE "French" "=" "French" "="
LangString MUI_TEXT_LICENSE_TITLE ${LANG_FRENCH} "Accord de licence"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_FRENCH} "Lisez attentivement l'accord de licence avant d'installer $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_FRENCH} "Si vous acceptez tous les termes de cet accord, cochez la case ci-dessous. Vous devez accepter cet accord pour installer $(^Name). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_FRENCH} "Appuyez sur la touche Page Bas pour voir le reste de l'accord."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_FRENCH} "Installation"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_FRENCH} "Veuillez patienter pendant l'installation de $(^Name)."
LangString MUI_TEXT_FINISH_TITLE ${LANG_FRENCH} "Installation terminée"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_FRENCH} "L'installation s'est déroulée correctement. Appuyez sur Fermer pour terminer."
LangString MUI_TEXT_ABORT_TITLE ${LANG_FRENCH} "La désinstallation a été annulée."
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_FRENCH} "L'installation ne s'est pas déroulée correctement. Appuyez sur Fermer pour terminer."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_FRENCH} "Dйsinstallation"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_FRENCH} "Veuillez patienter pendant la dйsinstallation de $(^Name)."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_FRENCH} "Dйsinstallation rйussie. Appuyez sur Fermer pour finir."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_FRENCH} "La dйsinstallation est terminйe."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_FRENCH} "La dйsinstallation a йtй annulйe."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_FRENCH} "L'installation a йchouй. Appuyez sur Fermer pour finir."

!insertmacro LANGFILE "Italian" "=" "Italian" "="
LangString MUI_TEXT_LICENSE_TITLE ${LANG_ITALIAN} "Accordo di licenza"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_ITALIAN} "Leggi l'accordo di licenza prima di procedere con l'installazione di $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_ITALIAN} "Se accetti tutti i termini dell'accordo, clicca sul riquadro di spunta qui sotto. Devi accettare l'accordo per poter installare $(^Name).$_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_ITALIAN} "Premi Pagina giù per leggere il resto dell'accordo."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_ITALIAN} "Installazione in corso"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_ITALIAN} "Attendi che $(^Name) venga installato. "
LangString MUI_TEXT_FINISH_TITLE ${LANG_ITALIAN} "Installazione completata"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_ITALIAN} "Installazione completata con successo. Clicca su Chiudi per uscire."
LangString MUI_TEXT_ABORT_TITLE ${LANG_ITALIAN} "Il processo di disinstallazione è stato cancellato."
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_ITALIAN} "La preparazione non è stata completata con successo. Clicca su Chiudi per uscire."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_ITALIAN} "Disinstallazione in corso..."
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_ITALIAN} "Attendi mentre $(^Name) viene disinstallato."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_ITALIAN} "La disinstallazione и stata completata con successo. Clicca 'Chiudi' per uscire."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_ITALIAN} "La disinstallazione и stata completata."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_ITALIAN} "La disinstallazione и stata interrotta."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_ITALIAN} "L'installazione non и stata completata. Clicca 'Chiudi' per uscire."

!insertmacro LANGFILE "German" "=" "German" "="
LangString MUI_TEXT_LICENSE_TITLE ${LANG_GERMAN} "Lizenzvereinbarung"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_GERMAN} "Bitte lies die Lizenzvereinbarung durch, bevor du $(^Name) installierst."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_GERMAN} "Wenn du den Bedingungen der Vereinbarung zustimmst, klicke unten das Kontrollkästchen an. Du musst die Bedingungen akzeptieren, um $(^Name) installieren zu können. $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_GERMAN} "Drücke Bild runter, um den Rest der Vereinbarung zu lesen."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_GERMAN} "Wird installiert"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_GERMAN} "Bitte warte, während $(^Name) installiert wird."
LangString MUI_TEXT_FINISH_TITLE ${LANG_GERMAN} "Installation abgeschlossen"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_GERMAN} "Die Installation wurde erfolgreich abgeschlossen. Drücke auf Schließen zum Beenden."
LangString MUI_TEXT_ABORT_TITLE ${LANG_GERMAN} "Die Deinstallation wurde abgebrochen."
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_GERMAN} "Die Installation war nicht erfolgreich. Drücke auf Schließen zum Beenden."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_GERMAN} "Deinstallieren..."
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_GERMAN} "Bitte warten, wдhrend $(^Name) deinstalliert wird."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_GERMAN} "Deinstallation erfolgreich abgeschlossen. Zum Beenden 'SchlieЯen' klicken."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_GERMAN} "Deinstallation erfolgreich."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_GERMAN} "Deinstallation abgebrochen."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_GERMAN} "Installation nicht erfolgreich. Zum Beenden 'SchlieЯen' klicken."
  
!insertmacro LANGFILE "Spanish" "=" "Spanish" "="
LangString MUI_TEXT_LICENSE_TITLE ${LANG_SPANISH} "Acuerdo de licencia"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_SPANISH} "Revisa el acuerdo de licencia antes de instalar $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_SPANISH} "Si aceptas todos los términos del acuerdo, haz clic en la casilla de abajo. Debes aceptar el acuerdo para instalar $(^Name). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_SPANISH} "Pulsa la tecla Av Pág para ver el resto del acuerdo."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_SPANISH} "Instalando"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_SPANISH} "Espera mientras se instala $(^Name)."
LangString MUI_TEXT_FINISH_TITLE ${LANG_SPANISH} "Instalación completada"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_SPANISH} "La instalación se ha completado con éxito. Haz clic en Cerrar para terminar."
LangString MUI_TEXT_ABORT_TITLE ${LANG_SPANISH} "La instalación fue cancelada."
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_SPANISH} "La instalación no se ha podido completar con éxito. Haz clic en Cerrar para terminar."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_SPANISH} "Desinstalando"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_SPANISH} "Espera mientras se desinstala $(^Name)."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_SPANISH} "La desinstalaciуn se ha completado con йxito."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_SPANISH} "La desinstalaciуn se ha completado."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_SPANISH} "La desinstalaciуn ha sido abortada."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_SPANISH} "La instalaciуn no ha sido completada con йxito. Haz clic en 'Cerrar' para finalizar."
    
!insertmacro LANGFILE "Russian" "=" "Russian" "="
LangString MUI_TEXT_LICENSE_TITLE ${LANG_RUSSIAN} "Лицензионное соглашение"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_RUSSIAN} "Прочитайте текст лицензионного соглашения, прежде чем устанавливать игру $(^NameDA)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_RUSSIAN} "Если вы согласны со всеми его условиями, поставьте галочку в окошке ниже. Вам следует принять соглашение, чтобы установить $(^Name).$_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_RUSSIAN} "Нажмите вниз страницы, чтобы увидеть остальную часть соглашения."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_RUSSIAN} "Установка"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_RUSSIAN} "Пожалуйста, подождите пока идет установка $(^Name). "
LangString MUI_TEXT_FINISH_TITLE ${LANG_RUSSIAN} "Установка завершена"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_RUSSIAN} "Установка была успешно завершена. Чтобы закончить, нажмите Закрыть."
LangString MUI_TEXT_ABORT_TITLE ${LANG_RUSSIAN} "Отмена деинсталляции."
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_RUSSIAN} "Установка не была завершена. Чтобы закончить, нажмите Закрыть."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_RUSSIAN} "????????"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_RUSSIAN} "?????????, ???? ???????????? ???????? $(^Name)."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_RUSSIAN} "???????? ???? ????????? ???????. ??????? ?????? ???????, ????? ?????????."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_RUSSIAN} "???????? ?????????."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_RUSSIAN} "???????? ???? ????????."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_RUSSIAN} "????????? ?? ???? ????????? ???????. ??????? ?????? '???????', ????? ?????????."


!insertmacro LANGFILE "Polish" "=" "Polish" "="
LangString MUI_TEXT_LICENSE_TITLE ${LANG_POLISH} "Umowa licencyjna"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_POLISH} "Zanim zainstalujesz grę $(^NameDA), przeczytaj umowę licencyjną."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_POLISH} "Jeśli wyrażasz zgodę na wszystkiewarunki umowy, zaznacz znajdujące się poniżej pole. Musisz zaakceptować umowę, zanim zainstalujesz grę $(^Name). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_POLISH} "Naciśnij Page Down, aby zobaczyć resztę umowy."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_POLISH} "Trwa instalacja"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_POLISH} "Zaczekaj, trwa instalacja gry $(^Name)."
LangString MUI_TEXT_FINISH_TITLE ${LANG_POLISH} "Instalacja ukończona"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_POLISH} "Instalacja ukończona sukcesem. Naciśnij Zamknij, aby zakończyć."
LangString MUI_TEXT_ABORT_TITLE ${LANG_POLISH} "Odinstalowywanie zostało anulowane."
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_POLISH} "Instalacja nie powiodła się. Naciśnij Zamknij, aby zakończyć."

LangString  MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_POLISH} "Deinstalacja"
LangString  MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_POLISH} "Prosze czekac, $(^NameDA) jest odinstalowywany."
LangString  MUI_UNTEXT_FINISH_TITLE ${LANG_POLISH} "Zakonczono odinstalowanie"
LangString  MUI_UNTEXT_FINISH_SUBTITLE ${LANG_POLISH} "Odinstalowanie zakonczone pomyslnie."
LangString  MUI_UNTEXT_ABORT_TITLE ${LANG_POLISH} "Deinstalacja przerwana"
LangString  MUI_UNTEXT_ABORT_SUBTITLE ${LANG_POLISH} "Deinstalacja nie zostala zakonczona pomyslnie."

!insertmacro LANGFILE "Portuguese" "=" "Portuguese" "="
LangString MUI_TEXT_LICENSE_TITLE ${LANG_PORTUGUESE} "Acordo de Licença de Uso"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_PORTUGUESE} "Confira o acordo da licença de uso antes de instalar $(^Name). "
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_PORTUGUESE} "Se você aceita todos os termos do acordo, clique na caixa de seleção abaixo. É preciso aceitar o acordo para instalar $(^Name). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_PORTUGUESE} "Pressione a tecla Page Down para ver o restante do Acordo."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_PORTUGUESE} "Instalando"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_PORTUGUESE} "Aguarde enquanto $(^Name) está sendo instalado."
LangString MUI_TEXT_FINISH_TITLE ${LANG_PORTUGUESE} "Instalação concluída"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_PORTUGUESE} "A instalação foi concluída com sucesso. Clique em Fechar para finalizar."
LangString MUI_TEXT_ABORT_TITLE ${LANG_PORTUGUESE} "Desinstalação abortada."
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_PORTUGUESE} "A instalação não foi concluída com sucesso. Clique em Fechar para finalizar."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_PORTUGUESE} "Desinstalando"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_PORTUGUESE} "Por favor, espere enquanto a $(^NameDA) estб sendo desinstalada."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_PORTUGUESE} "Desinstalaзгo Completa"
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_PORTUGUESE} "A desinstalaзгo foi completada com sucesso."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_PORTUGUESE} "Desinstalaзгo Abortada"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_PORTUGUESE} "A desinstalaзгo nгo foi completada com sucesso."

!insertmacro LANGFILE "Korean" "=" "Korean" "="
LangString MUI_TEXT_LICENSE_TITLE ${LANG_KOREAN} "라이선스 계약"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_KOREAN} "$(^NameDA) 설치를 진행하기 전에 라이선스 계약을 읽어 보십시오."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_KOREAN} "계약 조항에 모두 동의하면 아래의 체크 상자를 클릭하십시오. $(^Name)(을)를 설치를 하려면 계약 조항에 동의해야 합니다. $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_KOREAN} "나머지 계약 조항을 보려면 Page Down을 누르십시오."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_KOREAN} "설치 중"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_KOREAN} "$(^Name)이 설치되는 동안 기다려 주십시오."
LangString MUI_TEXT_FINISH_TITLE ${LANG_KOREAN} "설치 완료"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_KOREAN} "설치가 성공적으로 완료되었습니다. 마치려면 닫기를 클릭하십시오."
LangString MUI_TEXT_ABORT_TITLE ${LANG_KOREAN} "제거가 중단되었습니다."
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_KOREAN} "설치가 성공적으로 완료되지 않았습니다. 마치려면 닫기를 클릭하십시오."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_KOREAN} "?? ?? ?"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_KOREAN} "$(^Name) ??? ??? ??? ??? ??????."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_KOREAN} "?? ??? ??????. ??? ???? ??? ??????."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_KOREAN} "?? ?? ??"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_KOREAN} "?? ?? ??"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_KOREAN} "??? ???? ?????. ??? ???? ??? ??????."

!insertmacro LANGFILE "Japanese" "=" "Japanese" "="
LangString MUI_TEXT_LICENSE_TITLE ${LANG_JAPANESE} "使用許諾契約"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_JAPANESE} "$(^Name)のインストール前に、使用許諾契約を確認してください。"
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_JAPANESE} "条項および条件を承諾する場合は、下記のチェックボックスをクリックしてください。$(^Name)のインストールには承諾に同意する必要があります。$_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_JAPANESE} "Page Downを押すと使用許諾契約の続きを確認できます。" 
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_JAPANESE} "インストール中"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_JAPANESE} "$(^Name)がインストールされるのをお待ちください。"
LangString MUI_TEXT_FINISH_TITLE ${LANG_JAPANESE} "インストール完了"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_JAPANESE} "セットアップが完了しました。「閉じる」をクリックすると終了します。"
LangString MUI_TEXT_ABORT_TITLE ${LANG_JAPANESE} "アンインストールが中断されました"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_JAPANESE} "セットアップが完了しませんでした。「閉じる」をクリックすると終了します。"

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_JAPANESE} "????????"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_JAPANESE} "$(^NameDA)???????????????????????????"
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_JAPANESE} "???????????"
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_JAPANESE} "????????????????"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_JAPANESE} "???????????"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_JAPANESE} "???????????????????????"

!insertmacro LANGFILE "Chinese" "=" "Chinese" "="
LangString MUI_TEXT_LICENSE_TITLE ${LANG_CHINESE} "授權合約"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_CHINESE} "請於安裝 $(^Name) 前檢閱授權條款。"
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_CHINESE} "如果您接受所有合約條款，請勾選下方核取方塊。您必須接受合約內容才能安裝 $(^Name)。$_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_CHINESE} "按 Page Down 鍵以閱讀合約剩餘部份。" 
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_CHINESE} "正在安裝"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_CHINESE} "請等候 $(^Name) 安裝完成。"
LangString MUI_TEXT_FINISH_TITLE ${LANG_CHINESE} "安裝完成"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_CHINESE} "已成功安裝。按關閉以完成。"
LangString MUI_TEXT_ABORT_TITLE ${LANG_CHINESE} "解除安裝已終止。"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_CHINESE} "未成功安裝。按關閉以完成。"

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_CHINESE} "????????"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_CHINESE} "$(^NameDA)???????????????????????????"
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_CHINESE} "???????????"
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_CHINESE} "????????????????"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_CHINESE} "???????????"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_CHINESE} "???????????????????????"

!insertmacro LANGFILE "SimpChinese" "=" "SimpChinese" "="
LangString MUI_TEXT_LICENSE_TITLE ${LANG_SIMPCHINESE} "授權合約"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_SIMPCHINESE} "請於安裝 $(^Name) 前檢閱授權條款。"
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_SIMPCHINESE} "如果您接受所有合約條款，請勾選下方核取方塊。您必須接受合約內容才能安裝 $(^Name)。$_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_SIMPCHINESE} "按 Page Down 鍵以閱讀合約剩餘部份。" 
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_SIMPCHINESE} "正在安裝"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_SIMPCHINESE} "請等候 $(^Name) 安裝完成。"
LangString MUI_TEXT_FINISH_TITLE ${LANG_SIMPCHINESE} "安裝完成"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_SIMPCHINESE} "已成功安裝。按關閉以完成。"
LangString MUI_TEXT_ABORT_TITLE ${LANG_SIMPCHINESE} "解除安裝已終止。"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_SIMPCHINESE} "未成功安裝。按關閉以完成。"

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_SIMPCHINESE} "????????"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_SIMPCHINESE} "$(^NameDA)???????????????????????????"
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_SIMPCHINESE} "???????????"
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_SIMPCHINESE} "????????????????"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_SIMPCHINESE} "???????????"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_SIMPCHINESE} "???????????????????????"

!insertmacro LANGFILE "SpanishInternational" "=" "SpanishInternational" "="
LangString MUI_TEXT_LICENSE_TITLE ${LANG_SPANISHINTERNATIONAL} "Acuerdo de licencia"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_SPANISHINTERNATIONAL} "Revisa el acuerdo de licencia antes de instalar $(^NameDA)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_SPANISHINTERNATIONAL} "Si aceptas todos los términos del acuerdo, haz clic en la casilla de abajo. Es necesario aceptar el acuerdo de licencia antes de instalar $(^Name).  $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_SPANISHINTERNATIONAL} "Presiona Av Pág para ver el resto del acuerdo."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_SPANISHINTERNATIONAL} "Instalando"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_SPANISHINTERNATIONAL} "Espera mientras $(^Name) se está instalando. "
LangString MUI_TEXT_FINISH_TITLE ${LANG_SPANISHINTERNATIONAL} "Instalación completa"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_SPANISHINTERNATIONAL} "La instalación se completó con éxito. Haz clic en Cerrar para terminar."
LangString MUI_TEXT_ABORT_TITLE ${LANG_SPANISHINTERNATIONAL} "La isntalación fue cancelada. "
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_SPANISHINTERNATIONAL} "La instalación no se completó con éxito. Haz clic en Cerrar para terminar."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_SPANISHINTERNATIONAL} "Uninstalling"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_SPANISHINTERNATIONAL} "Please wait while $(^Name) is being uninstalled."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_SPANISHINTERNATIONAL} "Uninstallation was completed successfully. Click Close to finish."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_SPANISHINTERNATIONAL} "Uninstallation Complete"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_SPANISHINTERNATIONAL} "Uninstallation Aborted"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_SPANISHINTERNATIONAL} "Setup was not completed successfully. Click Close to finish."