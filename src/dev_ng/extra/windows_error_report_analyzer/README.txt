The windows error report analyzer is a tool that downloads and analyzes application failure reports from Microsoft's Windows Error Reporting service.

See the sample report in the same directory as this readme for an example of it's output.

**** Note: it's a work in progress, but it's functional. ****

To run the tool, you need to specify (via commandline) your Windows LiveID e-mail and password that you registered with the Windows Error Reporting site (sysdev.microsoft.com).
You need to specify which product id you want to analyze (currently no UI, so you need to modify the code).
There is some commented out code that will display a list of product IDs currently registered with WER.
Currently there are some hardcoded paths, and very little error checking.
You must have WinDbg installed.

If you have questions on it's usage, ask Nick Snell (R* Toronto).
