﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Telemetry.MetadataService.FeederLibrary;
using System.ComponentModel.Composition;
using Microsoft.Authentication.Live;

namespace Microsoft.Telemetry.MetadataService.PartnerPortalLogin
{
    [Export(typeof(ILogin))]
    public class Login : ILogin
    {
        [Import(typeof(IEnvironment))]
        public IEnvironment Environment { get; set; }

        private readonly string serviceName;
        private readonly string applicationName;
        private const string AUTHENTICATION_POLICY = "MBI_SSL";

        [ImportingConstructor]
        public Login([Import("LiveIDServiceName")]string serviceName, 
            [Import("LiveIDApplicationName")]string applicationName)
        {
            this.serviceName = serviceName;
            this.applicationName = applicationName;
        }

        public System.Net.HttpWebRequest GetAuthenticatedRequest(System.Net.HttpWebRequest request, string username, string password)
        {
            LiveIdAuthentication liveAuth = null;
            liveAuth = new LiveIdAuthentication(
                Environment.EnvironmentType,
                Guid.NewGuid(),
                "PartnerFeeder",//this.applicationName,
                "sysdev.microsoft.com", //this.serviceName,
                AUTHENTICATION_POLICY,
                true);

            System.Security.SecureString ss = new System.Security.SecureString();
            foreach (char c in password)
            {
                ss.AppendChar(c);
            }

            //
            // Authenticate user 
            //
            if (!liveAuth.Authenticate(username, ss))
            {
                throw new Exception("User or Password invalid");
            }

            string challenge = LiveIdAuthentication.GetChallengeFromServiceRequest(request.RequestUri.AbsoluteUri);
            request.CookieContainer = new System.Net.CookieContainer();

            LiveIdAuthentication.AddAuthorizationHeaderToRequestHeaders(request.Headers, liveAuth.GetResponseForHttpChallenge(challenge));

            return request;
        }
    }
}
