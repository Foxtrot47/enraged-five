﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Telemetry.MetadataService.FeederLibrary;
using System.Net;
using System.IO;
using System.Collections;
using System.ComponentModel.Composition;
using System.Xml;

namespace Microsoft.Telemetry.MetadataService.ReportMetadata
{
    [ExportMetaItems(Type=MetaItemTypes.FeedList)]
    public class FeedNamesMetadata : IMetaItems
    {
        private readonly string username;
        private readonly string password;
        private ArrayList reportArray = null;
        private const string REPORT_NAMESPACE_PREFIX = "ns";
        private string REPORT_XPATH = string.Format("/{0}:ArrayOfFeedInfo/{0}:FeedInfo/{0}:Name", REPORT_NAMESPACE_PREFIX);
        private const string REPORT_NAMESPACE = "http://schemas.datacontract.org/2004/07/Microsoft.Telemetry.PartnerPortal.Services.Reporting";

        [Import(typeof(ILogin))]
        public ILogin Login { get; set; }

        [Import(typeof(IEnvironment))]
        public IEnvironment Environment { get; set; }

        [ImportingConstructor]
        public FeedNamesMetadata([Import("UserName")]string username,
            [Import("Password")]string password)
        {
            this.username = username;
            this.password = password;
        }

        public string[] GetItems(params string[] args)
        {
            string reportName = null;
            if (args != null && args.Length > 0)
            {
                reportName = args[0];
            }

            if (string.IsNullOrEmpty(reportName) == true)
            {
                // dont have the reqd params.
                throw new ArgumentOutOfRangeException("ReportName", "ReportName cannot be null or empty");
            }

            string reportNameEncoded = ByteArrayToString(
                System.Text.ASCIIEncoding.Default.GetBytes(reportName.ToCharArray()));
            //
            // MEF parts are implemented as singletons by default. So instantiate the report
            // name list only if it has not already been populated.
            //
            if (reportArray == null)
            {
                HttpWebRequest request = null;
                Stream response = null;
                reportArray = new ArrayList();

                try
                {
                    request = (HttpWebRequest)WebRequest.Create(
                        Environment.GetReportFeedListURL(
                            reportNameEncoded));
                    request = Login.GetAuthenticatedRequest(request, this.username, this.password);
                    response = request.GetResponse().GetResponseStream();
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(response);
                    XmlNamespaceManager namespcMgr = new XmlNamespaceManager(xDoc.NameTable);
                    namespcMgr.AddNamespace(REPORT_NAMESPACE_PREFIX, REPORT_NAMESPACE);
                    XmlNodeList nodes = xDoc.SelectNodes(REPORT_XPATH, namespcMgr);
                    foreach (XmlNode node in nodes)
                    {
                        reportArray.Add(node.InnerText);
                    }
                }
                finally
                {
                    if (response != null)
                    {
                        response.Close();
                    }
                }
            }

            string[] reports = new string[reportArray.Count];
            reportArray.CopyTo(reports);
            return reports;
        }

        private static string ByteArrayToString(byte[] byteArray)
        {
            StringBuilder builder = new StringBuilder();
            foreach (byte b in byteArray)
            {
                builder.AppendFormat("{0:X2}", b);
            }
            return builder.ToString().ToUpper();
        }
    }
}
