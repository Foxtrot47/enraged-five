﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Telemetry.MetadataService.FeederLibrary;
using System.Net;
using System.IO;
using System.Collections;
using System.ComponentModel.Composition;
using System.Xml;

namespace Microsoft.Telemetry.MetadataService.ReportMetadata
{
    [ExportMetaItems(Type=MetaItemTypes.ReportList)]
    public class ReportNamesMetadata : IMetaItems
    {
        private readonly string username;
        private readonly string password;
        private ArrayList reportArray = null;
        private const string REPORT_NAMESPACE_PREFIX = "ns";
        private string REPORT_XPATH = string.Format("/{0}:ArrayOfstring/{0}:string", REPORT_NAMESPACE_PREFIX);
        private const string REPORT_NAMESPACE = "http://schemas.microsoft.com/2003/10/Serialization/Arrays";

        [Import(typeof(ILogin))]
        public ILogin Login { get; set; }

        [Import(typeof(IEnvironment))]
        public IEnvironment Environment { get; set; }

        [ImportingConstructor]
        public ReportNamesMetadata([Import("UserName")]string username,
            [Import("Password")]string password)
        {
            this.username = username;
            this.password = password;
        }

        public string[] GetItems(params string[] args)
        {
            //
            // MEF parts are implemented as singletons by default. So instantiate the report
            // name list only if it has not already been populated.
            //
            if (reportArray == null)
            {
                HttpWebRequest request = null;
                Stream response = null;
                reportArray = new ArrayList();

                try
                {
                    request = (HttpWebRequest)WebRequest.Create(Environment.ReportListURL);
                    request = Login.GetAuthenticatedRequest(request, this.username, this.password);
                    response = request.GetResponse().GetResponseStream();
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(response);
                    XmlNamespaceManager namespcMgr = new XmlNamespaceManager(xDoc.NameTable);
                    namespcMgr.AddNamespace(REPORT_NAMESPACE_PREFIX, REPORT_NAMESPACE);
                    XmlNodeList nodes = xDoc.SelectNodes(REPORT_XPATH, namespcMgr);
                    foreach (XmlNode node in nodes)
                    {
                        reportArray.Add(GetDecodedString(node.InnerText));
                    }
                }
                finally
                {
                    if (response != null)
                    {
                        response.Close();
                    }
                }
            }

            string[] reports = new string[reportArray.Count];
            reportArray.CopyTo(reports);
            return reports;
        }

        private static string GetDecodedString(string encodedString)
        {
            byte[] bytes = StringToByteArray(encodedString);
            return System.Text.ASCIIEncoding.Default.GetString(bytes);
        }

        private static byte[] StringToByteArray(String hex)
        {
            if (hex.StartsWith("0x", StringComparison.InvariantCultureIgnoreCase))
            {
                hex = hex.Substring(2);
            }
            int NumberChars = hex.Length / 2;
            byte[] bytes = new byte[NumberChars];
            for (int i = 0; i < NumberChars; i++)
                bytes[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            return bytes;
        }
    }
}
