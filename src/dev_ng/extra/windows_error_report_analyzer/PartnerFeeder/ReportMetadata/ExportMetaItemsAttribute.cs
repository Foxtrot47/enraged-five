﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Microsoft.Telemetry.MetadataService.FeederLibrary;

namespace Microsoft.Telemetry.MetadataService.ReportMetadata
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple=false)]
    public class ExportMetaItemsAttribute : ExportAttribute
    {
        public ExportMetaItemsAttribute() : base(typeof(IMetaItems)) { }
        public MetaItemTypes Type { get; set; }
    }
}
