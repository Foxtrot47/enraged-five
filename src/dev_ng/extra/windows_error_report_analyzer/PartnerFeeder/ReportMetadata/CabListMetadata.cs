﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Telemetry.MetadataService.FeederLibrary;
using System.Net;
using System.IO;
using System.Collections;
using System.ComponentModel.Composition;
using System.Xml;

namespace Microsoft.Telemetry.MetadataService.ReportMetadata
{
    [ExportMetaItems(Type=MetaItemTypes.CabList)]
    public class CabListMetadata : IMetaItems
    {
        private readonly string username;
        private readonly string password;
        private ArrayList cabLinkArray = null;
        private const string FEED_NAMESPACE_PREFIX = "ns";
        private const string METADATA_NAMESPACE_PREFIX = "m";
        private const string DATA_SERVICES_NAMESPACE_PREFIX = "d";
        private const string FEED_NAMESPACE = "http://www.w3.org/2005/Atom";
        private const string METADATA_NAMESPACE = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata";
        private const string DATA_SERVICES_NAMESPACE = "http://schemas.microsoft.com/ado/2007/08/dataservices";
        private string CAB_LINK_XPATH = string.Format("/{0}:feed/{0}:entry/{0}:content/{1}:properties/{2}:CabDownloadLink",
            FEED_NAMESPACE_PREFIX,
            METADATA_NAMESPACE_PREFIX,
            DATA_SERVICES_NAMESPACE_PREFIX);

        [Import(typeof(ILogin))]
        public ILogin Login { get; set; }

        [Import(typeof(IEnvironment))]
        public IEnvironment Environment { get; set; }

        [ImportingConstructor]
        public CabListMetadata([Import("UserName")]string username,
            [Import("Password")]string password)
        {
            this.username = username;
            this.password = password;
        }

        public string[] GetItems(params string[] args)
        {
            string reportName = null;
            string feedName = null;
            if (args != null && args.Length > 1)
            {
                reportName = args[0];
                feedName = args[1];
            }

            if (string.IsNullOrEmpty(reportName) == true)
            {
                // dont have the reqd params.
                throw new ArgumentOutOfRangeException("ReportName", "ReportName cannot be null or empty");
            }
            if (string.IsNullOrEmpty(feedName) == true)
            {
                // dont have the reqd params.
                throw new ArgumentOutOfRangeException("FeedName", "FeedName cannot be null or empty");
            }

            string reportNameEncoded = ByteArrayToString(
                System.Text.ASCIIEncoding.Default.GetBytes(reportName.ToCharArray()));

            HttpWebRequest request = null;
            Stream response = null;
            cabLinkArray = new ArrayList();

            try
            {
                request = (HttpWebRequest)WebRequest.Create(
                    Environment.GetReportFeedURL(
                        reportNameEncoded, feedName));
                request = Login.GetAuthenticatedRequest(request, this.username, this.password);
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreProcessingInstructions = true;
                response = request.GetResponse().GetResponseStream();
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(response);
                XmlNamespaceManager namespcMgr = new XmlNamespaceManager(xDoc.NameTable);
                namespcMgr.AddNamespace(FEED_NAMESPACE_PREFIX, FEED_NAMESPACE);
                namespcMgr.AddNamespace(METADATA_NAMESPACE_PREFIX, METADATA_NAMESPACE);
                namespcMgr.AddNamespace(DATA_SERVICES_NAMESPACE_PREFIX, DATA_SERVICES_NAMESPACE);
                XmlNodeList nodes = xDoc.SelectNodes(CAB_LINK_XPATH, namespcMgr);
                foreach (XmlNode node in nodes)
                {
                    cabLinkArray.Add(node.InnerText);
                }
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }

            string[] cabLinks = new string[cabLinkArray.Count];
            cabLinkArray.CopyTo(cabLinks);
            return cabLinks;
        }

        private static string ByteArrayToString(byte[] byteArray)
        {
            StringBuilder builder = new StringBuilder();
            foreach (byte b in byteArray)
            {
                builder.AppendFormat("{0:X2}", b);
            }
            return builder.ToString().ToUpper();
        }
    }
}
