﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Telemetry.MetadataService.FeederLibrary;
using System.ComponentModel.Composition;
using System.Net;
using System.Xml;
using System.Collections;
using System.IO;

namespace Microsoft.Telemetry.MetadataService.ReportMetadata
{
    [Export(typeof(IMetadata))]
    public class Metadata : IMetadata
    {
        [Import(typeof(ILogin))]
        public ILogin Login { get; set; }

        [Import(typeof(IEnvironment))]
        public IEnvironment Environment { get; set; }

        private readonly string username;
        private readonly string password;
        private const string REPORT_NAME_TAG = "string";
        private const string FEED_INFO_TAG = "FeedInfo";
        private const string NAME_TAG = "Name";
        private const string TITLE_TAG = "Title";
        private ArrayList reportArray = null;

        /*
         https://werservices.sysdev.microsoft.com/ReportingService/ReportingService.svc/Reports - Get the list of reports for your company
         https://werservices.sysdev.microsoft.com/ReportingService/ReportingService.svc/Reports/{1}/{2} - 
         * 
         * 
         */

        [ImportingConstructor]
        public Metadata([Import("UserName")]string username,
            [Import("Password")]string password)
        {
            this.username = username;
            this.password = password;
        }

        public string[] GetReportList()
        {
            //
            // MEF parts are implemented as singletons by default. So instantiate the report
            // name list only if it has not already been populated.
            //
            if (reportArray == null)
            {
                HttpWebRequest request = null;
                Stream response = null;
                reportArray = new ArrayList();

                try
                {
                    request = (HttpWebRequest)WebRequest.Create(Environment.ReportListURL.AbsoluteUri);
                    request = Login.GetAuthenticatedRequest(request, this.username, this.password);
                   XmlReaderSettings settings = new XmlReaderSettings();
                    settings.IgnoreProcessingInstructions = true;
                    response = request.GetResponse().GetResponseStream();
                    using (XmlReader reader = XmlReader.Create(response))
                    {

                        while (reader.ReadToFollowing(REPORT_NAME_TAG))
                        {
                            string reportNameEncoded = reader.ReadString();
                            // report name is encoded, convert it into real name
                            byte[] bytes = StringToByteArray(reportNameEncoded);
                            string reportName = System.Text.ASCIIEncoding.Default.GetString(bytes);
                            reportArray.Add(reportName);
                        }
                    }
                }
                finally
                {
                    if (response != null)
                    {
                        response.Close();
                    }
                }
            }

            string[] reports = new string[reportArray.Count];
            reportArray.CopyTo(reports);
            return reports;
        }

        public ArrayList GetFeedsForReport(string reportName)
        {
            string reportNameEncoded = ByteArrayToString(
                System.Text.ASCIIEncoding.Default.GetBytes(reportName.ToCharArray()));

            HttpWebRequest request = null;
            Stream response = null;
            ArrayList feedArray = new ArrayList();

            try
            {
                request = (HttpWebRequest)WebRequest.Create(
                    Environment.GetReportFeedListURL(
                        reportNameEncoded).AbsoluteUri);
                request = Login.GetAuthenticatedRequest(request, this.username, this.password);
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreProcessingInstructions = true;
                try
                {
                    response = request.GetResponse().GetResponseStream();
                    using (XmlReader reader = XmlReader.Create(response))
                    {

                        while (reader.ReadToFollowing(FEED_INFO_TAG))
                        {
                            reader.ReadToFollowing(NAME_TAG);
                            string feedName = reader.ReadString();
                           
                            reader.ReadToFollowing(TITLE_TAG);
                            string title = reader.ReadString();
                            FeedInfo feedInfo = new FeedInfo(title, feedName);
                            feedArray.Add(feedInfo);
                        }
                    }
                }
                catch (WebException)
                {
                    //
                    // one case is when there are no feeds for the report.
                    // in that case a response is not returned (could be a 404)
                    //
                    // * IGNORE FOR NOW *
                }
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }

            return (feedArray);

            /*string[] feeds = new string[feedArray.Count];
            feedArray.CopyTo(feeds);
            return feeds;*/
        }

        public string GetReportFeed(string reportName, string feedName)
        {
            string xml = null;
            string reportNameEncoded = ByteArrayToString(
                System.Text.ASCIIEncoding.Default.GetBytes(reportName.ToCharArray()));

            HttpWebRequest request = null;
            Stream response = null;

            try
            {
                request = (HttpWebRequest)WebRequest.Create(Environment.GetReportFeedURL(reportNameEncoded, feedName).AbsoluteUri);
                request = Login.GetAuthenticatedRequest(request, this.username, this.password);
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreProcessingInstructions = true;
                try
                {
                    response = request.GetResponse().GetResponseStream();
                    using (XmlReader reader = XmlReader.Create(response))
                    {
                        reader.MoveToContent();
                        xml = reader.ReadOuterXml();
                    }
                }
                catch (WebException)
                {
                    //
                    // one case is when there are no feeds for the report.
                    // in that case a response is not returned (could be a 404)
                    //
                    // * IGNORE FOR NOW *
                }
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }

            return xml;
        }

        
        
        private static string ByteArrayToString(byte[] byteArray)
        {
            StringBuilder builder = new StringBuilder();
            foreach (byte b in byteArray)
            {
                builder.AppendFormat("{0:X2}", b);
            }
            return builder.ToString().ToUpper();
        }

        public static byte[] StringToByteArray(String hex)
        {
            if (hex.StartsWith("0x", StringComparison.InvariantCultureIgnoreCase))
            {
                hex = hex.Substring(2);
            }
            int NumberChars = hex.Length / 2;
            byte[] bytes = new byte[NumberChars];
            for (int i = 0; i < NumberChars; i++)
                bytes[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            return bytes;
        }
    }
}
