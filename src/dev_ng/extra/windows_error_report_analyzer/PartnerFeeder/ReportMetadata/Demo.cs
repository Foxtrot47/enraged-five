﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Telemetry.MetadataService.ReportMetadata
{
    class Demo
    {
public System.Collections.ArrayList GetReportNames(string username, System.Security.SecureString password)
{
    const string REPORT_LIST_URL = "https://werservices.sysdev.microsoft.com/ReportingService/ReportingService.svc/Reports"; // NOT LIVE YET
    const string REPORT_XPATH = "/ArrayOfstring/string";
    System.Collections.ArrayList reportNameArray = new System.Collections.ArrayList();

    // create the request
    System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(REPORT_LIST_URL);
    // authenticate the request
    request = GetAuthenticatedRequest(request, username, password);
    // get the response
    System.IO.Stream response = request.GetResponse().GetResponseStream();
    // load the response into XmlDocument
    System.Xml.XmlDocument xDoc = new System.Xml.XmlDocument();
    xDoc.Load(response);
    // get the list of nodes for the report names
    System.Xml.XmlNodeList nodes = xDoc.SelectNodes(REPORT_XPATH); // use namespace manager in addition to the XPATH
    foreach (System.Xml.XmlNode node in nodes)
    {
        reportNameArray.Add(node.InnerText);
    }
    return reportNameArray;
}
public System.Collections.ArrayList GetFeedNames(string reportName, string username, System.Security.SecureString password)
{
    string reportFeedListURL = string.Format("https://werservices.sysdev.microsoft.com/ReportingService/ReportingService.svc/Reports/{0}", reportName);
    const string FEED_XPATH = "/ArrayOfstring/string";
    System.Collections.ArrayList feedNameArray = new System.Collections.ArrayList();

    // create the request
    System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(reportFeedListURL);
    // authenticate the request
    request = GetAuthenticatedRequest(request, username, password);
    // get the response
    System.IO.Stream response = request.GetResponse().GetResponseStream();
    // load the response into XmlDocument
    System.Xml.XmlDocument xDoc = new System.Xml.XmlDocument();
    xDoc.Load(response);
    // get the list of nodes for the feed names
    System.Xml.XmlNodeList nodes = xDoc.SelectNodes(FEED_XPATH); // use namespace manager in addition to the XPATH
    foreach (System.Xml.XmlNode node in nodes)
    {
        feedNameArray.Add(node.InnerText);
    }
    return feedNameArray;
}

public void DownloadCabs(string reportName, string feedName, string cabDowloadFolder, string username, System.Security.SecureString password)
{
    string reportFeedListURL = string.Format("https://werservices.sysdev.microsoft.com/ReportingService/ReportingService.svc/Reports/{1}/{2}", reportName, feedName);
    const string CABLINK_XPATH = "/:feed/:entry/:content/:properties/:CabDownloadLink";

    // create the request
    System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(reportFeedListURL);
    // authenticate the request
    request = GetAuthenticatedRequest(request, username, password);
    // get the response
    System.IO.Stream response = request.GetResponse().GetResponseStream();
    // load the response into XmlDocument
    System.Xml.XmlDocument xDoc = new System.Xml.XmlDocument();
    xDoc.Load(response);
    // get the list of nodes for the report names
    System.Xml.XmlNodeList nodes = xDoc.SelectNodes(CABLINK_XPATH); // use namespace manager in addition to the XPATH
    foreach (System.Xml.XmlNode node in nodes)
    {
        string cabLink = node.InnerText;
        // parse the file name from the cab link
        string cabFileName = ParseCabLink(cabLink);
        // generate the full cab file path
        string cabFilePath = System.IO.Path.Combine(cabDowloadFolder, cabFileName);
        // create the request
        System.Net.HttpWebRequest cabDownloadRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(cabLink);
        // authenticate the request
        cabDownloadRequest = GetAuthenticatedRequest(cabDownloadRequest, username, password);
        // get the response
        System.IO.Stream cabDownloadResponse = cabDownloadRequest.GetResponse().GetResponseStream();
        // save the cab file to disk
        using (System.IO.FileStream fileStream = System.IO.File.Create(cabFilePath))
        {
            cabDownloadResponse.CopyTo(fileStream);
        }
    }
}

private string ParseCabLink(string cabLink)
{
    throw new NotImplementedException();
}

        private System.Net.HttpWebRequest GetAuthenticatedRequest(System.Net.HttpWebRequest request, string username, System.Security.SecureString password)
        {
            throw new NotImplementedException();
        }
    }
}
