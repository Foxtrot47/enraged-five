﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Microsoft.Telemetry.MetadataService.FeederLibrary;
using System.ComponentModel.Composition;
using System.Net;
using System.IO;

namespace Microsoft.Telemetry.MetadataService.ReportMetadata
{
    [Export(typeof(IPersist))]
    public class DownloadCab : IPersist
    {
        private readonly string username;
        private readonly string password;

        public event OnDownloadFailure DownloadFailure;

        [Import(typeof(ILogin))]
        public ILogin Login { get; set; }

        [ImportingConstructor]
        public DownloadCab([Import("UserName")]string username,
            [Import("Password")]string password)
        {
            this.username = username;
            this.password = password;
        }

        public bool Persist(Uri resourceUri, string persistTo)
        {
            HttpWebRequest request = null;
            Stream response = null;
            bool success = false;

            try
            {
                request = (HttpWebRequest)WebRequest.Create(resourceUri);
                request.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["Timeout"]) * 1000;
                request.ReadWriteTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["ReadWriteTimeout"]) * 1000;
                request = Login.GetAuthenticatedRequest(request, this.username, this.password);
                response = request.GetResponse().GetResponseStream();
                using (FileStream fileStream = File.Create(persistTo))
                {
                    response.CopyTo(fileStream);
                }

                success = true;
            }
            catch (WebException webEx)
            {
                // do nothing. We want to move on to the next cab download.
                Console.WriteLine(webEx.Message);

                if (DownloadFailure != null)
                {
                    DownloadFailure(webEx);
                }
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }

            return success;
        }
    }
}
