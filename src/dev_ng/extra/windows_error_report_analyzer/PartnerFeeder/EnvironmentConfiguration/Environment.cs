﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Telemetry.MetadataService.FeederLibrary;
using System.ComponentModel.Composition;

namespace Microsoft.Telemetry.MetadataService.EnvironmentConfiguration
{
    [Export(typeof(IEnvironment))]
    public class Environment : IEnvironment
    {
        private const string REPORTING_SERVICE_SERVER_NAME = "werservices.sysdev.microsoft.com";
        private const string CABDOWNLOAD_SERVICE_SERVER_NAME = "werservices.sysdev.microsoft.com";
        private const string REPORT_LIST_URL_FORMAT = "https://{0}/ReportingService/ReportingService.svc/Reports";
        private const string REPORT_FEED_LIST_URL_FORMAT = "https://{0}/ReportingService/ReportingService.svc/Reports/{1}";
        private const string REPORT_FEED_URL_FORMAT = "https://{0}/ReportingService/ReportingService.svc/Reports/{1}/{2}";
        private readonly Uri reportListUri;

        public Environment()
        {
            reportListUri = new Uri(string.Format(REPORT_LIST_URL_FORMAT, REPORTING_SERVICE_SERVER_NAME));
        }

        public string EnvironmentType
        {
            get { return "PROD"; }
        }

        public Uri ReportListURL
        {
            get { return reportListUri; }
        }

        public Uri TransferServiceURL
        {
            get { throw new NotImplementedException(); }
        }

        public Uri GetReportFeedListURL(string reportName)
        {
            return new Uri(
                string.Format(
                    REPORT_FEED_LIST_URL_FORMAT, 
                    REPORTING_SERVICE_SERVER_NAME, 
                    reportName));
        }

        public Uri GetReportFeedURL(string reportName, string feedName)
        {
            return new Uri(
                string.Format(
                    REPORT_FEED_URL_FORMAT,
                    REPORTING_SERVICE_SERVER_NAME,
                    reportName,
                    feedName));
        }
    }
}
