﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using Microsoft.Telemetry.MetadataService.PartnerFeeder;

namespace Windows_Error_Reporting
{
    public partial class Form1 : Form
    {
        PartnerFeeder m_PartnerFeeder;
        ProductMap m_ProductMap;
        Creds m_Credentials;

        string pmFile = "productmap.xml";
        string loginFile = "creds.xml";
        int TotalProducts;
        int NumProducts;

        public Form1()
        {
            InitializeComponent();

            // setup
            ServicePointManager.DefaultConnectionLimit = 30;
            Directory.CreateDirectory(ConfigurationManager.AppSettings["CabDownloadFolder"]);

            m_Credentials = new Creds();
            UnserializeCredentials();

            m_ProductMap = new ProductMap();

            m_PartnerFeeder = new PartnerFeeder();
            m_PartnerFeeder.OnOutput += new PartnerFeeder.OnOutputEvent(Program_OnOutput);
            m_PartnerFeeder.OnAnalyzed += new PartnerFeeder.OnAnalyzedEvent(Program_OnAnalyzed);
            m_PartnerFeeder.ProductAnalyzed += new PartnerFeeder.OnProductAnalyzed(PartnerFeeder_ProductAnalyzed);

            textBox1.TextChanged += new EventHandler(textBox1_TextChanged);

            SetContext("");

            Dictionary<string, string> FileMap = new Dictionary<string, string>();
            FileMap.Add("GTAVLauncher.exe", "GTA V PC PreLauncher");
            FileMap.Add("PlayGTAV.exe", "GTA V PC Launcher");
            FileMap.Add("GTA5.exe", "GTA V PC");
            FileMap.Add("socialclub.dll", "Social Club");
            FileMap.Add("subprocess.exe", "Social Club");
            m_PartnerFeeder.FileMap = FileMap;
        }

        private void LoadProductMap()
        {
            TotalProducts = 0;
            NumProducts = 0;
            m_ProductMap.Products = m_PartnerFeeder.GetProductList();
            OnProductMapLoaded();

            SetContext("Serializing Product List");
            SerializeProducts();
            SetContext("");

            EnableButton(button1, true);
            EnableButton(button2, true);
            EnableButton(Analyze, true);
            EnableButton(ArxDump, true);
        }

        private void OnProductMapLoaded()
        {
            List<string> productNames = new List<string>();

            foreach (ProductMapper.Product product in m_ProductMap.Products)
            {
                if (!productNames.Contains(product.productName))
                {
                    productNames.Add(product.productName);
                }
            }

            productNames.Sort();
            foreach (String productName in productNames)
            {
                AddProductItem(productName);
            }
        }

        #region Serialization

        private void SerializeCredentials()
        {
            XmlSerializer ser = new XmlSerializer(typeof(Creds));
            using (FileStream fs = new FileStream(loginFile, FileMode.Create))
            {
                ser.Serialize(fs, m_Credentials);
            }
        }

        private void UnserializeCredentials()
        {
            XmlSerializer ser = new XmlSerializer(typeof(Creds));
            if (File.Exists(loginFile))
            {
                StreamReader reader = new StreamReader(loginFile);
                m_Credentials = (Creds)ser.Deserialize(reader);
                reader.Close();
            }

            LoginBox.Text = m_Credentials.Username;
            PasswordBox.Text = m_Credentials.Password;
        }

        private void SerializeProducts()
        {
            XmlSerializer ser = new XmlSerializer(typeof(ProductMap));
            using (FileStream fs = new FileStream(pmFile, FileMode.Create))
            {
                ser.Serialize(fs, m_ProductMap);
            }
        }

        private void UnserializeProducts()
        {
            XmlSerializer ser = new XmlSerializer(typeof(ProductMap));
            if (File.Exists(pmFile))
            {
                StreamReader reader = new StreamReader(pmFile);
                m_ProductMap = (ProductMap)ser.Deserialize(reader);
                reader.Close();
            }

            foreach (ProductMapper.Product p in m_ProductMap.Products)
            {
                string productAddText = "Loaded ProductId " + p.productId + ": " + p.productName + ", Version: " + p.productVersion;
                Log(productAddText + Environment.NewLine);
            }

            EnableButton(button1, false);
            EnableButton(button2, false);
            EnableButton(Analyze, true);
            EnableButton(ArxDump, true);

            SetContext("");
            OnProductMapLoaded();
        }

        #endregion

        #region Form Events

        private void button1_Click(object sender, EventArgs e)
        {
            SetContext("Loading Product List...");
            m_ProductMap.Clear();

            button1.Enabled = false;
            button2.Enabled = false;
            Analyze.Enabled = false;
            ArxDump.Enabled = false;

            Task t = Task.Factory.StartNew(() => LoadProductMap());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            UnserializeProducts();
        }

        private void ProductsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProductVersions.Items.Clear();
            string productName = (string)ProductsList.SelectedItem;
            List<string> productVersions = new List<string>();
            foreach (ProductMapper.Product product in m_ProductMap.Products)
            {
                if(product.productName == productName)
                {
                    productVersions.Add(product.productVersion);
                }
            }

            productVersions.Sort();
            foreach (string productVersion in productVersions)
            {
                ProductVersions.Items.Add(productVersion);
            }

        }

        private void ProductVersions_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Login_Click(object sender, EventArgs e)
        {
            m_Credentials.Username = LoginBox.Text;
            m_Credentials.Password = PasswordBox.Text;
            m_PartnerFeeder.Init(m_Credentials.Username, m_Credentials.Password);
            m_PartnerFeeder.Mapper.ProductAdded += new ProductMapper.OnProductAdded(Mapper_OnProductAdded);
            m_PartnerFeeder.Mapper.ReportListDownloaded += new ProductMapper.OnReportListDownloaded(Mapper_ReportListDownloaded);

            EnableButton(button1, true);
            if (File.Exists(pmFile))
            {
                EnableButton(button2, true);
            }

            SerializeCredentials();
        }

        private void Analyze_Click(object sender, EventArgs e)
        {
            if (ProductsList.SelectedItem == null)
                return;

            if (ProductVersions.SelectedItem == null)
                return;

            string productName = (string)ProductsList.SelectedItem;
            string productVersion = (string)ProductVersions.SelectedItem;

            string WERImagesRoot = ConfigurationManager.AppSettings["WERImagesRoot"];
            string WERPath = WERImagesRoot + "\\" + productName + "\\" + productVersion + "\\images";
            if (!Directory.Exists(WERPath))
            {
                var result = MessageBox.Show("Images path does not exist on file server:" + Environment.NewLine + WERPath,
                    "WER Analyze", MessageBoxButtons.OK, MessageBoxIcon.Question);
            }
            else
            {
                EnableButton(Analyze, false);
                EnableButton(ArxDump, false);

                foreach (ProductMapper.Product product in m_ProductMap.Products)
                {
                    if (product.productName == productName && product.productVersion == productVersion)
                    {
                        Task t = Task.Factory.StartNew(() => m_PartnerFeeder.Analyze2(product.productId, product.productName, product.productVersion));
                        break;
                    }
                }
            }

            SetContext("Analyzing products...");
        }

        private void ArxDump_Click(object sender, EventArgs e)
        {
            string productName = (string)ProductsList.SelectedItem;
            string productVersion = (string)ProductVersions.SelectedItem;

            string WERImagesRoot = ConfigurationManager.AppSettings["WERImagesRoot"];
            string WERPath = WERImagesRoot + "\\" + productName + "\\" + productVersion + "\\images";
            string ArxDumpPath = ConfigurationManager.AppSettings["ArxDumpPath"];

            if (!Directory.Exists(WERPath))
            {
                var result = MessageBox.Show("Images path does not exist on file server:" + Environment.NewLine + WERPath,
                    "WER ArxDump", MessageBoxButtons.OK, MessageBoxIcon.Question);
            }
            else if (!File.Exists(ArxDumpPath))
            {
                var result = MessageBox.Show("ArxDump tool not found:" + Environment.NewLine + WERPath,
                    "WER ArxDump", MessageBoxButtons.OK, MessageBoxIcon.Question);
            }
            else
            {
                EnableButton(Analyze, false);
                EnableButton(ArxDump, false);

                foreach (ProductMapper.Product product in m_ProductMap.Products)
                {
                    if (product.productName == productName && product.productVersion == productVersion)
                    {
                        Task t = Task.Factory.StartNew(() => m_PartnerFeeder.ArxDumpAnalyze(product.productId, product.productName, product.productVersion));
                        break;
                    }
                }
            }
        }

        void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.SelectionStart = textBox1.Text.Length;
            textBox1.ScrollToCaret();
            textBox1.SelectionLength = 0;
        }

        #endregion

        #region Delegates

        public void Mapper_OnProductAdded(ProductMapper.Product product)
        {
            string productAddText = "Loaded ProductId " + product.productId + ": " + product.productName + ", Version: " + product.productVersion;
            Log(productAddText + Environment.NewLine);
            NumProducts++;
            SetProgress(NumProducts);
        }

        public void PartnerFeeder_ProductAnalyzed(int count, int total)
        {
            NumProducts = count;
            TotalProducts = total;
            SetProgress(NumProducts);
        }

        void Mapper_ReportListDownloaded(string[] reports)
        {
            Log(string.Format("Downloaded Report List: {0} reports{1}", reports.Length, Environment.NewLine));
            TotalProducts = reports.Length;
            SetProgress(0);
        }

        void Program_OnOutput(string output)
        {
            Console.WriteLine(output);
            Log(output + Environment.NewLine);
        }


        void Program_OnAnalyzed()
        {
            EnableButton(Analyze, true);
            EnableButton(ArxDump, true);
            SetProgress(0);
            SetContext("");
        }

        #endregion

        #region Callbacks

        delegate void ContextCallback(string context);
        private void SetContext(string context)
        {
            if (this.ProductsList.InvokeRequired)
            {
                ContextCallback d = new ContextCallback(SetContext);
                this.Invoke(d, new object[] { context });
            }
            else
            {
                toolStripStatusLabel1.Text = context;
                statusStrip1.Refresh();
                toolStripProgressBar1.Visible = !String.IsNullOrEmpty(context);
            }
        }

        delegate void ProductItemCallback(string productName);
        private void AddProductItem(string productName)
        {
            if (this.ProductsList.InvokeRequired)
            {
                ProductItemCallback d = new ProductItemCallback(AddProductItem);
                this.Invoke(d, new object[] { productName });
            }
            else
            {
                ProductsList.Items.Add(productName);
            }
        }

        delegate void ProgressCallback(int progressCallback);
        private void SetProgress(int progress)
        {
            if (this.statusStrip1.InvokeRequired)
            {
                ProgressCallback d = new ProgressCallback(SetProgress);
                this.Invoke(d, new object[] { progress });
            }
            else if (TotalProducts > 0)
            {
                int pct = Convert.ToInt32(NumProducts * 100f / TotalProducts);
                if (pct > 100)
                    pct = 100;

                toolStripProgressBar1.Value = pct;
            }
            else
            {
                toolStripProgressBar1.Value = 0;
            }
        }

        delegate void LogCallback(string text);
        private void Log(string text)
        {
            if (this.textBox1.InvokeRequired)
            {
                LogCallback d = new LogCallback(Log);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.textBox1.Text += text;
            }
        }

        delegate void EnableButtonCallback(Button b, bool enabled);
        private void EnableButton(Button b, bool enabled)
        {
            if (b.InvokeRequired)
            {
                EnableButtonCallback d = new EnableButtonCallback(EnableButton);
                this.Invoke(d, new object[] { b, enabled });
            }
            else
            {
                b.Enabled = enabled;
            }
        }   
        
        #endregion
    }

    [XmlRoot("ProductMap")]
    [Serializable()]
    public class Creds
    {
        public string Username;
        public string Password;
    }

    [XmlRoot("ProductMap")]
    [Serializable()]
    public class ProductMap
    {
        public ProductMap()
        {
            Products = new List<ProductMapper.Product>();
        }

        public List<ProductMapper.Product> Products
        {
            get;
            set;
        }

        public void Clear()
        {
            if (Products != null)
            {
                Products.Clear();
            }
        }
    }
}
