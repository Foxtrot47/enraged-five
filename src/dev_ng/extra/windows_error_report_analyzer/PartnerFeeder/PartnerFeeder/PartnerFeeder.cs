﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using Microsoft.Telemetry.MetadataService.FeederLibrary;
using System.Collections;
using Microsoft.Telemetry.MetadataService.ReportMetadata;
using System.Xml;
using System.Net;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;

namespace Microsoft.Telemetry.MetadataService.PartnerFeeder
{
    public class PartnerFeeder
    {
        static string m_LiveUserName;
        static string m_LivePassword;

        public ProductMapper Mapper { get; set; }

        public Dictionary<string, string> FileMap = new Dictionary<string, string>();

        public delegate void OnOutputEvent(string output);
        public event OnOutputEvent OnOutput;

        public delegate void OnAnalyzedEvent();
        public event OnAnalyzedEvent OnAnalyzed;


        public delegate void OnProductAnalyzed(int cur, int total);
        public event OnProductAnalyzed ProductAnalyzed;

        public PartnerFeeder()
        {
            Mapper = null;
        }

        public void Init(string userName, string password)
        {
            m_LiveUserName = userName;
            m_LivePassword = password;

            PartsAssembly partsAssembly = new PartsAssembly();
            LoadParts(partsAssembly, m_LiveUserName, m_LivePassword);

            Mapper = new ProductMapper(partsAssembly, m_LiveUserName, m_LivePassword);
        }

        public List<ProductMapper.Product> GetProductList()
        {
            Mapper.Run();
            return Mapper.Products;
        }

        private void Output(string output)
        {
            if (OnOutput != null)
            {
                OnOutput(output);
            }
            else
            {
                Console.WriteLine(output);
            }
        }

        void Mapper_ProductAnalyzed(int cur, int total)
        {
            if (ProductAnalyzed != null)
            {
                ProductAnalyzed(cur, total);
            }
        }

        public void ArxDumpAnalyze(string productId, string productName, string productVersion)
        {
            string PRODUCT_NAME = productName;
            string PRODUCT_VERSION = productVersion;
            string PRODUCT_ID = productId;
            string PRODUCT_FAILURES_REPORT_NAME = "ProductDetailNonSuiteReport_" + PRODUCT_ID + ".rdl";

            Output("ArxDump for Product: " + PRODUCT_NAME + ", Version: " + PRODUCT_VERSION);

            //string PRODUCT_CAB_LIST_FEED_NAME = "";
            //string PRODUCT_CAB_LIST_FEED_XML_FILENAME = "";
            //
            //PartsAssembly partsAssembly = new PartsAssembly();
            //LoadParts(partsAssembly, m_LiveUserName, m_LivePassword);
            //
            //string[] reportNames = partsAssembly.Metadata.GetReportList();
            //foreach (string reportName in reportNames)
            //{
            //    if (reportName == PRODUCT_FAILURES_REPORT_NAME)
            //    {
            //        ArrayList feedNames = partsAssembly.Metadata.GetFeedsForReport(reportName);
            //        Parallel.ForEach(feedNames.Cast<object>(), feedObject =>
            //        {
            //            FeedInfo feedInfo = (FeedInfo)feedObject;
            //            if (feedInfo.Title == "CabFeed")
            //            {
            //                PRODUCT_CAB_LIST_FEED_NAME = feedInfo.Name;
            //                string xml = partsAssembly.Metadata.GetReportFeed(reportName, feedInfo.Name);
            //                string cabFolder = ConfigurationManager.AppSettings["CabDownloadFolder"];
            //                PRODUCT_CAB_LIST_FEED_XML_FILENAME = Path.Combine(cabFolder, reportName + "_" + feedInfo.Title + ".xml");
            //            }
            //        });
            //    }
            //}


            string PRODUCT_CAB_LIST_FEED_NAME = "";
            string PRODUCT_CAB_LIST_FEED_XML_FILENAME = "";

            PartsAssembly partsAssembly = new PartsAssembly();
            LoadParts(partsAssembly, m_LiveUserName, m_LivePassword);

            string[] reportNames = partsAssembly.Metadata.GetReportList();
            foreach (string reportName in reportNames)
            {
                if (reportName == PRODUCT_FAILURES_REPORT_NAME)
                {
                    Output("Downloading failure report: " + reportName);
                    ArrayList feedNames = partsAssembly.Metadata.GetFeedsForReport(reportName);

                    Parallel.ForEach(feedNames.Cast<object>(), feedObject =>
                    {
                        FeedInfo feedInfo = (FeedInfo)feedObject;
                        if (feedInfo.Title == "CabFeed")
                        {
                            //Console.WriteLine(feedInfo.Title);
                            PRODUCT_CAB_LIST_FEED_NAME = feedInfo.Name;
                            //Console.WriteLine(feedInfo.Name);

                            //Console.WriteLine("`````````````````````````````````````");
                            string xml = partsAssembly.Metadata.GetReportFeed(reportName, feedInfo.Name);
                            //Console.WriteLine(xml);

                            string cabFolder = ConfigurationManager.AppSettings["CabDownloadFolder"];
                            PRODUCT_CAB_LIST_FEED_XML_FILENAME = Path.Combine(cabFolder, reportName + "_" + feedInfo.Title + ".xml");
                            WriteXMLToFile(xml, reportName + "_" + feedInfo.Title);
                            //Console.WriteLine("`````````````````````````````````````");
                        }
                    });
                    //Console.WriteLine("*****************************************");
                }
            }
            //if (String.IsNullOrEmpty(PRODUCT_CAB_LIST_FEED_XML_FILENAME) || !File.Exists(PRODUCT_CAB_LIST_FEED_XML_FILENAME))
            //{
            //    Output(PRODUCT_CAB_LIST_FEED_XML_FILENAME + " not found, did your run an Analyze?");
            //}

            ArxMapAnalyzer analyzer = new ArxMapAnalyzer(partsAssembly, m_LiveUserName, m_LivePassword, PRODUCT_NAME, PRODUCT_VERSION);
            analyzer.OnOutput += Output;
            analyzer.FileMap = FileMap;
            analyzer.Run(PRODUCT_CAB_LIST_FEED_XML_FILENAME);

            if (OnAnalyzed != null)
            {
                OnAnalyzed();
            }
        }

        public void Analyze2(string productId, string productName, string productVersion)
        {
            string PRODUCT_NAME = productName;
            string PRODUCT_VERSION = productVersion;
            string PRODUCT_ID = productId;
            string PRODUCT_FAILURES_REPORT_NAME = "ProductDetailNonSuiteReport_" + PRODUCT_ID + ".rdl";

            Output("Product: " + PRODUCT_NAME + ", Version: " + PRODUCT_VERSION);

            string PRODUCT_CAB_LIST_FEED_NAME = "";
            string PRODUCT_CAB_LIST_FEED_XML_FILENAME = "";

            PartsAssembly partsAssembly = new PartsAssembly();
            LoadParts(partsAssembly, m_LiveUserName, m_LivePassword);

            string[] reportNames = partsAssembly.Metadata.GetReportList();
            foreach (string reportName in reportNames)
            {
                if (reportName == PRODUCT_FAILURES_REPORT_NAME)
                {
                    Output("Downloading failure report: " + reportName);
                    ArrayList feedNames = partsAssembly.Metadata.GetFeedsForReport(reportName);

                    Parallel.ForEach(feedNames.Cast<object>(), feedObject =>
                    {
                        FeedInfo feedInfo = (FeedInfo)feedObject;
                        if (feedInfo.Title == "CabFeed")
                        {
                            //Console.WriteLine(feedInfo.Title);
                            PRODUCT_CAB_LIST_FEED_NAME = feedInfo.Name;
                            //Console.WriteLine(feedInfo.Name);

                            //Console.WriteLine("`````````````````````````````````````");
                            string xml = partsAssembly.Metadata.GetReportFeed(reportName, feedInfo.Name);
                            //Console.WriteLine(xml);

                            string cabFolder = ConfigurationManager.AppSettings["CabDownloadFolder"];
                            PRODUCT_CAB_LIST_FEED_XML_FILENAME = Path.Combine(cabFolder, reportName + "_" + feedInfo.Title + ".xml");
                            WriteXMLToFile(xml, reportName + "_" + feedInfo.Title);
                            //Console.WriteLine("`````````````````````````````````````");
                        }
                    });
                    //Console.WriteLine("*****************************************");
                }
            }

            #region Sample
            /* 
             * Sample code for getting the metadata from the service.
             * Get list of feeds for a report 
             *
            foreach (Lazy<IMetaItems, IMetaItemTypes> metaItem in partsAssembly.MetaItems)
            {
                if (metaItem.Metadata.Type == MetaItemTypes.ReportList)
                {
                    string[] reportNames2 = metaItem.Value.GetItems();
                    foreach (string reportName in reportNames2)
                    {
                        Console.WriteLine(reportName);
                        Console.WriteLine("-----------------------------------------");
                        foreach (Lazy<IMetaItems, IMetaItemTypes> subMetaItem in partsAssembly.MetaItems)
                        {
                            if (subMetaItem.Metadata.Type == MetaItemTypes.FeedList)
                            {
                                string[] feedNames = subMetaItem.Value.GetItems(reportName);
                                foreach (string feedName in feedNames)
                                {
                                    Console.WriteLine(feedName);
                                }
                                Console.WriteLine("*****************************************");
                            }
                        }
                    }
                }
            }
            */

            /* get's our list of products
            string productSummaryReportName = PRODUCT_SUMMARY_REPORT_NAME;
            string prsCabLinksFeedName = PSR_CAB_LIST_FEED_NAME;
            string[] productNames = null;
            foreach (Lazy<IMetaItems, IMetaItemTypes> metaItem in partsAssembly.MetaItems)
            {
                if (metaItem.Metadata.Type == MetaItemTypes.ProductList)
                {
                    productNames = metaItem.Value.GetItems(productSummaryReportName, prsCabLinksFeedName);
                    foreach (string productName in productNames)
                    {
                        Console.WriteLine(productName);
                    }
                }
            }
            */
            #endregion

            CabFeedAnalyzer analyzer = new CabFeedAnalyzer(partsAssembly, m_LiveUserName, m_LivePassword, PRODUCT_NAME, PRODUCT_VERSION);
            analyzer.OnOutput += Output;
            analyzer.ProductAnalyzed += new CabFeedAnalyzer.OnProductAnalyzed(Mapper_ProductAnalyzed);
            analyzer.FileMap = FileMap;
            analyzer.Run(PRODUCT_CAB_LIST_FEED_XML_FILENAME);

            #region Sample
            //string companySummaryReportName = COMPANY_SUMMARY_REPORT_NAME;
            //string cabLinksFeedName = CAB_LIST_FEED_NAME;
            //             string[] cabLinks = null;
            //             foreach (Lazy<IMetaItems, IMetaItemTypes> metaItem in partsAssembly.MetaItems)
            //             {
            //                 if (metaItem.Metadata.Type == MetaItemTypes.CabList)
            //                 {
            //                     cabLinks = metaItem.Value.GetItems(PRODUCT_FAILURES_REPORT_NAME, PRODUCT_CAB_LIST_FEED_NAME);
            //                 }
            //             }
            //             if (cabLinks != null)
            //             {
            //                 foreach (string cabLink in cabLinks)
            //                 {
            //                     Console.WriteLine(cabLink);
            //                     partsAssembly.CabDownload.Persist(new Uri(cabLink), GetCabFilePath(cabLink));
            //                 }
            //             }
            // 
            // #if DEBUG
            //             Console.ReadLine();
            // #endif
            #endregion

            if (OnAnalyzed != null)
            {
                OnAnalyzed();
            }
        }

        public static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                MessageBox.Show("Usage: liveIdUsername liveIdPassword");
                return;
            }

            string liveIDUsername = args[0];
            string liveIDPassword = args[1];

            PartnerFeeder p = new PartnerFeeder();
            p.Init(liveIDUsername, liveIDPassword);
            p.RunProcess();
        }

        public void RunProcess()
        {
            //const string COMPANY_SUMMARY_REPORT_NAME = "CompanySummaryReport.rdl";
            //const string PRODUCT_SUMMARY_REPORT_NAME = "ProductSummaryReport.rdl";
            //const string CAB_LIST_FEED_NAME = "xAx0x3xDx5x2x0x3"; //The list of feeds has the name of the feed in the <Title> element. For this it is <Title>CabTable</Title>
            //const string PSR_CAB_LIST_FEED_NAME = "xAx0x3xDx7x4x0x3"; //The list of feeds has the name of the feed in the <Title> element. For this it is <Title>CabTable</Title>

            PartsAssembly partsAssembly = new PartsAssembly();
            LoadParts(partsAssembly, m_LiveUserName, m_LivePassword);

            #region Products
            // this prints out a list of all products and versions and tells you the product id of each (which you'll need to set below)
            // ProductMapper mapper = new ProductMapper(partsAssembly, liveIDUsername, liveIDPassword);
            // List<ProductMapper.Product> products = new List<ProductMapper.Product>();
            // mapper.Run(ref products);

            /*
                As of September 16, 2014:
                ------------------------
                ProductId 45555: GTA V PC, Version: PreRelease Build v84
             
                ProductId 20525: GTA IV PC, Version: 1.0 EFIGS
                ProductId 28934: GTA IV PC, Version: 1.0 Russian
                ProductId 3390: GTA IV PC, Version: 1.0.1 EFIGS
				ProductId 563: GTA IV PC, Version: 1.0.2 EFIGS
				ProductId 15597: GTA IV PC, Version: 1.0.3 EFIGS

                ProductId 188: Rockstar Games Social Club Launcher, Version: 1.1.3.0

                ProductId 17091: L.A. Noire, Version: 2382 (EFIGS v1.0.0.1)
                ProductId 9485: L.A. Noire, Version: 2382 (Russian v1.0.0.1)
				ProductId 6391: L.A. Noire, Version: 2393 (EFIGS)
                ProductId 4826: L.A. Noire, Version: 2393 (Russian)

                ProductId 32325: Max Payne 3 PC, Version: 1.0.0.17 EFIGS
                ProductId 32365: Max Payne 3 PC, Version: 1.0.0.22 EFIGS
                ProductId 32681: Max Payne 3 PC, Version: 1.0.0.28 EFIGS
				ProductId 32680: Max Payne 3 PC, Version: 1.0.0.28 Steam EFIGS
				ProductId 36216: Max Payne 3 PC, Version: 1.0.0.113 EFIGS
				ProductId 36215: Max Payne 3 PC, Version: 1.0.0.113 Steam EFIGS

                ProductId 32333: Social Club, Version: 1.0.9.5
                ProductId 36184: Social Club, Version: 1.1.0.1
             */

            //              const string PRODUCT_NAME = "Social Club";
            //              const string PRODUCT_VERSION = "1.1.0.1";
            //              const string PRODUCT_ID = "36184";
            #endregion

            const string PRODUCT_NAME = "GTA V PC";
            const string PRODUCT_VERSION = "PreRelease Build v84";
            const string PRODUCT_ID = "45555";
            const string PRODUCT_FAILURES_REPORT_NAME = "ProductDetailNonSuiteReport_" + PRODUCT_ID + ".rdl";

            Output("Product: " + PRODUCT_NAME + ", Version: " + PRODUCT_VERSION);

            string PRODUCT_CAB_LIST_FEED_NAME = "";
            string PRODUCT_CAB_LIST_FEED_XML_FILENAME = "";

            /* 
             * Sample code for getting the metadata from the service.
             * Get list of reports*/
            string[] reportNames = partsAssembly.Metadata.GetReportList();
            foreach (string reportName in reportNames)
            {
                if (reportName == PRODUCT_FAILURES_REPORT_NAME)
                {
                    Console.WriteLine("Downloading failure report: " + reportName);
                    ArrayList feedNames = partsAssembly.Metadata.GetFeedsForReport(reportName);
                    foreach (Object feedObject in feedNames)
                    {
                        FeedInfo feedInfo = (FeedInfo)feedObject;
                        if (feedInfo.Title == "CabFeed")
                        {
                            //Console.WriteLine(feedInfo.Title);
                            PRODUCT_CAB_LIST_FEED_NAME = feedInfo.Name;
                            //Console.WriteLine(feedInfo.Name);

                            //Console.WriteLine("`````````````````````````````````````");
                            string xml = partsAssembly.Metadata.GetReportFeed(reportName, feedInfo.Name);
                            //Console.WriteLine(xml);

                            PRODUCT_CAB_LIST_FEED_XML_FILENAME = Path.Combine(ConfigurationManager.AppSettings["CabDownloadFolder"], reportName + "_" + feedInfo.Title + ".xml");
                            WriteXMLToFile(xml, reportName + "_" + feedInfo.Title);
                            //Console.WriteLine("`````````````````````````````````````");
                        }
                    }
                    //Console.WriteLine("*****************************************");
                }
            }

            #region Sample
            /* 
             * Sample code for getting the metadata from the service.
             * Get list of feeds for a report 
             *
            foreach (Lazy<IMetaItems, IMetaItemTypes> metaItem in partsAssembly.MetaItems)
            {
                if (metaItem.Metadata.Type == MetaItemTypes.ReportList)
                {
                    string[] reportNames2 = metaItem.Value.GetItems();
                    foreach (string reportName in reportNames2)
                    {
                        Console.WriteLine(reportName);
                        Console.WriteLine("-----------------------------------------");
                        foreach (Lazy<IMetaItems, IMetaItemTypes> subMetaItem in partsAssembly.MetaItems)
                        {
                            if (subMetaItem.Metadata.Type == MetaItemTypes.FeedList)
                            {
                                string[] feedNames = subMetaItem.Value.GetItems(reportName);
                                foreach (string feedName in feedNames)
                                {
                                    Console.WriteLine(feedName);
                                }
                                Console.WriteLine("*****************************************");
                            }
                        }
                    }
                }
            }
            */

            /* get's our list of products
            string productSummaryReportName = PRODUCT_SUMMARY_REPORT_NAME;
            string prsCabLinksFeedName = PSR_CAB_LIST_FEED_NAME;
            string[] productNames = null;
            foreach (Lazy<IMetaItems, IMetaItemTypes> metaItem in partsAssembly.MetaItems)
            {
                if (metaItem.Metadata.Type == MetaItemTypes.ProductList)
                {
                    productNames = metaItem.Value.GetItems(productSummaryReportName, prsCabLinksFeedName);
                    foreach (string productName in productNames)
                    {
                        Console.WriteLine(productName);
                    }
                }
            }
            */
            #endregion

            CabFeedAnalyzer analyzer = new CabFeedAnalyzer(partsAssembly, m_LiveUserName, m_LivePassword, PRODUCT_NAME, PRODUCT_VERSION);
            analyzer.OnOutput += Output;
            analyzer.Run(PRODUCT_CAB_LIST_FEED_XML_FILENAME);

            #region Sample

            //string companySummaryReportName = COMPANY_SUMMARY_REPORT_NAME;
            //string cabLinksFeedName = CAB_LIST_FEED_NAME;
            //             string[] cabLinks = null;
            //             foreach (Lazy<IMetaItems, IMetaItemTypes> metaItem in partsAssembly.MetaItems)
            //             {
            //                 if (metaItem.Metadata.Type == MetaItemTypes.CabList)
            //                 {
            //                     cabLinks = metaItem.Value.GetItems(PRODUCT_FAILURES_REPORT_NAME, PRODUCT_CAB_LIST_FEED_NAME);
            //                 }
            //             }
            //             if (cabLinks != null)
            //             {
            //                 foreach (string cabLink in cabLinks)
            //                 {
            //                     Console.WriteLine(cabLink);
            //                     partsAssembly.CabDownload.Persist(new Uri(cabLink), GetCabFilePath(cabLink));
            //                 }
            //             }
            // 
            // #if DEBUG
            //             Console.ReadLine();
            // #endif

            #endregion
        }

        private static string GetCabFilePath(string cabLink)
        {
            string cabDownloadFolder = ConfigurationManager.AppSettings["CabDownloadFolder"];
            int lastForwardSlashIndex = cabLink.LastIndexOf("/");
            string cabFileName = cabLink.Substring(lastForwardSlashIndex + 1, cabLink.Length - 1 - lastForwardSlashIndex) + ".cab";
            string cabFilePath = Path.Combine(cabDownloadFolder, cabFileName);
            return cabFilePath;
        }

        public void LoadParts(PartsAssembly partsAssembly, string liveIDUsername, string liveIDPassword)
        {
            try
            {
                string directoryPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                directoryPath += "\\..\\..\\..\\PartnerFeeder\\bin";
                string partsDirectoryPath = Path.Combine(directoryPath, "Parts");
                DirectoryCatalog directoryCatalog = new DirectoryCatalog(partsDirectoryPath, "*.dll");
                CompositionContainer container = new CompositionContainer(directoryCatalog);
                container.ComposeExportedValue<string>("LiveIDServiceName", ConfigurationManager.AppSettings["LiveIDServiceName"]); //Input parameter to the PartnerPortalLogin part constructor
                container.ComposeExportedValue<string>("LiveIDApplicationName", ConfigurationManager.AppSettings["LiveIDApplicationName"]); //Input parameter to the PartnerPortalLogin part constructor
                container.ComposeExportedValue<string>("UserName", liveIDUsername); //Input parameter to the Metadata part constructor
                container.ComposeExportedValue<string>("Password", liveIDPassword); //Input parameter to the Metadata part constructor
                container.ComposeParts(partsAssembly); // NOTE: this needs a namespace reference to System.ComponentModel.Composition to work since ComposeParts is an extension property
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Output(ex.Message);
                Console.ResetColor();
            }
        }

        /*
         * Accepts ATOM xml as a String
         * Writes XML to file in CabDownloadFolder location */
        private bool WriteXMLToFile(String text, String name)
        {
            StreamWriter f = null;

            if (name.IndexOf(".xml", 0) <= 0)
            {
                name += ".xml";
            }
            try
            {
                string filePath = Path.Combine(ConfigurationManager.AppSettings["CabDownloadFolder"], name);
                f = File.CreateText(filePath);
                
                f.Write(text);
                f.Flush();
            }
            catch (IOException e)
            {
                Output(e.Message);
                return false;
            }
            catch (Exception e)
            {
                Output(e.Message);
                return false;
            }
            finally
            {
                if (f != null)
                {
                    f.Close();
                }
            }

            return true;
        }
    }
}
