﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using Microsoft.Telemetry.MetadataService.FeederLibrary;
using System.Collections;
using Microsoft.Telemetry.MetadataService.ReportMetadata;
using System.Xml;
using System.Net;
using System.Windows.Forms;

namespace Microsoft.Telemetry.MetadataService.PartnerFeeder
{
    public class CabFeedAnalyzer
    {
        private readonly string username;
        private readonly string password;
        private readonly string productName;
        private readonly string productVersion;

        private const string FEED_NAMESPACE_PREFIX = "ns";
        private const string METADATA_NAMESPACE_PREFIX = "m";
        private const string DATA_SERVICES_NAMESPACE_PREFIX = "d";
        private const string FEED_NAMESPACE = "http://www.w3.org/2005/Atom";
        private const string METADATA_NAMESPACE = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata";
        private const string DATA_SERVICES_NAMESPACE = "http://schemas.microsoft.com/ado/2007/08/dataservices";
        private string CAB_LINK_XPATH = string.Format("/{0}:feed/{0}:entry/{0}:content/{1}:properties/{2}:CabDownloadLink",
            FEED_NAMESPACE_PREFIX,
            METADATA_NAMESPACE_PREFIX,
            DATA_SERVICES_NAMESPACE_PREFIX);
        private string FAILURE_XML_PATH = string.Format("/{0}:feed/{0}:entry/{0}:content/{1}:properties",
            FEED_NAMESPACE_PREFIX,
            METADATA_NAMESPACE_PREFIX);
        private PartsAssembly partsAssembly;

        [Import(typeof(ILogin))]
        public ILogin Login { get; set; }

        [Import(typeof(IEnvironment))]
        public IEnvironment Environment { get; set; }

        public CabFeedAnalyzer(PartsAssembly partsAssembly,
                               string username,
                               string password,
                               string productName,
                               string productVersion)
        {
            this.partsAssembly = partsAssembly;
            this.username = username;
            this.password = password;
            this.productName = productName;
            this.productVersion = productVersion;
        }

        class FailureMetadata
        {
            public string failureName;
            public int numHits;
            public string offset;
            public string cabLink;
            public string analysisPath;
        };

        public class FailureComparer : IComparer
        {
            int IComparer.Compare(object x, object y)
            {
                FailureMetadata a = (FailureMetadata)x;
                FailureMetadata b = (FailureMetadata)y;
                if(a.numHits < b.numHits)
                {
                    return 1;
                }
                else if (a.numHits > b.numHits)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }

        private string GetCabFolderName(string cabLink)
        {
            int lastForwardSlashIndex = cabLink.LastIndexOf("/");
            return cabLink.Substring(lastForwardSlashIndex + 1, cabLink.Length - 1 - lastForwardSlashIndex);
        }

        private string GetCabFilename(string cabLink)
        {
            return GetCabFolderName(cabLink) + ".cab";
        }

        private string GetModuleName(string failureName)
        {
            int endIndex = failureName.LastIndexOf("!");
            int startIndex = failureName.LastIndexOf("_", endIndex);
            return failureName.Substring(startIndex + 1, endIndex - startIndex - 1);
        }
        
        public void Run(string xmlFileName)
        {
            ArrayList failureArray = new ArrayList();
            bool dontDownload = false;
            bool dontExpand = false;
            bool dontAnalyze = false;

            try
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreProcessingInstructions = true;
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(xmlFileName);
                XmlNamespaceManager namespcMgr = new XmlNamespaceManager(xDoc.NameTable);
                namespcMgr.AddNamespace(FEED_NAMESPACE_PREFIX, FEED_NAMESPACE);
                namespcMgr.AddNamespace(METADATA_NAMESPACE_PREFIX, METADATA_NAMESPACE);
                namespcMgr.AddNamespace(DATA_SERVICES_NAMESPACE_PREFIX, DATA_SERVICES_NAMESPACE);
                XmlNodeList nodes = xDoc.SelectNodes(FAILURE_XML_PATH, namespcMgr);
                foreach (XmlNode node in nodes)
                {
                    FailureMetadata metadata = new FailureMetadata();
                    int numHits = 0;
                    double offsetPercent = 0.0;

                    foreach (XmlNode child in node.ChildNodes)
                    {
                        if(child.Name == "d:FailureName")
                        {
                            metadata.failureName = child.InnerText;
                        }
                        else if (child.Name == "d:Hits")
                        {
                            numHits = Convert.ToInt32(child.InnerText);
                        }
                        else if (child.Name == "d:Offset")
                        {
                            metadata.offset = child.InnerText;
                        }
                        else if (child.Name == "d:OffsetPc")
                        {
                            offsetPercent = Convert.ToDouble(child.InnerText);
                        }
                        else if (child.Name == "d:CabDownloadLink")
                        {
                            metadata.cabLink = child.InnerText;
                        }
                    }

                    metadata.numHits = (int)Math.Round(((double)numHits) * offsetPercent);
                    failureArray.Add(metadata);
                }

                // sort here so we download and analyze the most important crashes first
                failureArray.Sort(new FailureComparer());
                string root = @"C:\watson\cabs\" + productName + @"\" + productVersion + @"\";
                string reportPath = root + "report.txt";

                int count = 1;
                foreach (FailureMetadata failure in failureArray)
                {
                    Console.WriteLine("Processing failure " + count + " of " + failureArray.Count);
                    string moduleName = GetModuleName(failure.failureName);
                    string cabFileName = GetCabFilename(failure.cabLink);
                    string path = root + moduleName + @"\" + failure.failureName + @"\" + failure.offset + @"\" + GetCabFolderName(failure.cabLink);
                    string cabpath = path + @"\" + cabFileName;

                    if (dontDownload == false)
                    {
                        if (String.IsNullOrEmpty(failure.cabLink) == false)
                        {

                            if (System.IO.Directory.Exists(path) == false)
                            {
                                System.IO.Directory.CreateDirectory(path);
                            }

                            if (System.IO.File.Exists(cabpath) == false)
                            {
                                Console.WriteLine("\tDownloading cab");
                                if (cabpath.Length < 260)
                                {
                                    partsAssembly.CabDownload.Persist(new Uri(failure.cabLink), cabpath);
                                }
                                else
                                {
                                    Console.WriteLine("\tPath is too long: " + cabpath);
                                }
                            }
                            else
                            {
                                Console.WriteLine("\tAlready downloaded");
                            }
                        }
                    }

                    if (dontExpand == false)
                    {
                        string expandedFolder = path + @"\" + "expanded";
                        int fileCount = 0;
                        bool folderExists = System.IO.Directory.Exists(expandedFolder);
                        if (folderExists)
                        {
                            fileCount = Directory.GetFiles(expandedFolder, "*.*", SearchOption.TopDirectoryOnly).Length;
                        }

                        if ((fileCount == 0) || (folderExists == false))
                        {
                            Console.WriteLine("\tExpanding Cab");
                            System.IO.Directory.CreateDirectory(expandedFolder);
                            string commandLine = "\"" + cabpath + "\" -F:* \"" + expandedFolder + "\"";
                            //Console.WriteLine(commandLine);
                            ProcessStartInfo processinfo = new ProcessStartInfo("expand.exe", commandLine);
                            Process process = Process.Start(processinfo);
                            process.WaitForExit();
                        }
                        else
                        {
                            Console.WriteLine("\tAlready expanded");
                        }

                        if (dontAnalyze == false)
                        {
                            string analyzedFolder = path + @"\" + "analyzed";
                            fileCount = 0;
                            folderExists = System.IO.Directory.Exists(analyzedFolder);
                            if (folderExists)
                            {
                                fileCount = Directory.GetFiles(analyzedFolder, "*.*", SearchOption.TopDirectoryOnly).Length;
                            }

                            if ((fileCount == 0) || (folderExists == false))
                            {
                                Console.WriteLine("\tAnalyzing Crash Dump");
                                if (folderExists == false)
                                {
                                    System.IO.Directory.CreateDirectory(analyzedFolder);
                                }
                                Directory.SetCurrentDirectory(analyzedFolder);
                                string winDbgPath = @"C:\Program Files\Debugging Tools for Windows (x64)\windbg.exe";
                                string imagePath = root + "Images";
                                string symbolsPath = imagePath;
                                symbolsPath = symbolsPath + @";SRV*C:\Microsoft Symbol Cache*http://msdl.microsoft.com/download/symbols";
                                string winDbgScriptPath = imagePath + @"\windbgscript.txt";
                                string commandLine = "for /f \"tokens=*\" %a IN ('dir /b \"" + expandedFolder + "\\*dmp\"') do \"" + winDbgPath + "\" -y \"" + symbolsPath + "\" -i \"" + imagePath + "\" -z \"" + expandedFolder + "\\%a\" -c \"$$>< " + winDbgScriptPath + ";q\" -Q -QS -QY -QSY";
                                //Console.WriteLine(commandLine);
                                ProcessStartInfo processinfo = new ProcessStartInfo("cmd.exe", "/c " + commandLine);
                                Process process = Process.Start(processinfo);
                                process.WaitForExit();
                            }
                            else
                            {
                                Console.WriteLine("\tAlready analyzed");
                            }

                            string[] files = Directory.GetFiles(analyzedFolder, "*.log", SearchOption.TopDirectoryOnly);
                            if (files.Length > 0)
                            {
                                failure.analysisPath = files[0];
                            }
                        }

                    }
                    
                    count++;
                }
                
                // report
                List<String> hitsArray = new List<string>();
                StreamWriter outfile = new StreamWriter(reportPath);

                //Hits	Failure Name / Offset
                //----	---------------------
                outfile.WriteLine("Hits\tFailure Name / Offset");
                outfile.WriteLine("----\t---------------------");
                foreach (FailureMetadata failure in failureArray)
                {
                    string line = failure.numHits + "\t" + failure.failureName + " Offset " + failure.offset;
                    if (hitsArray.Contains(line) == false)
                    {
                        hitsArray.Add(line);

                        outfile.WriteLine(line);
                    }

                    outfile.WriteLine("\t\t\t" + failure.analysisPath);

                    string fileLine;

                    if (String.IsNullOrEmpty(failure.analysisPath) == false)
                    {
                        System.IO.StreamReader file = new System.IO.StreamReader(failure.analysisPath);
                        bool start = false;
                        while ((fileLine = file.ReadLine()) != null)
                        {
                            if (fileLine.StartsWith("STACK_TEXT:"))
                            {
                                start = true;
                            }
                            else if (start == true)
                            {
                                if (fileLine == "")
                                {
                                    outfile.WriteLine("");
                                    break;
                                }
                                else
                                {
                                    int endIndex = fileLine.IndexOf("!");
                                    if (endIndex >= 0)
                                    {
                                        int startIndex = fileLine.LastIndexOf(" ", endIndex);
                                        outfile.WriteLine("\t\t\t\t\t" + fileLine.Substring(startIndex + 1));
                                    }
                                    else
                                    {
                                        outfile.WriteLine("\t\t\t\t\t" + fileLine);
                                    }
                                }
                            }
                        }

                        file.Close();
                    }
                }

                outfile.Close();
            }
            finally
            {
            }
        }

        private static string ByteArrayToString(byte[] byteArray)
        {
            StringBuilder builder = new StringBuilder();
            foreach (byte b in byteArray)
            {
                builder.AppendFormat("{0:X2}", b);
            }
            return builder.ToString().ToUpper();
        }
    }
    
    public class ProductMapper
    {
        private readonly string username;
        private readonly string password;

        private const string FEED_NAMESPACE_PREFIX = "ns";
        private const string METADATA_NAMESPACE_PREFIX = "m";
        private const string DATA_SERVICES_NAMESPACE_PREFIX = "d";
        private const string FEED_NAMESPACE = "http://www.w3.org/2005/Atom";
        private const string METADATA_NAMESPACE = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata";
        private const string DATA_SERVICES_NAMESPACE = "http://schemas.microsoft.com/ado/2007/08/dataservices";
        private string FAILURE_XML_PATH = string.Format("/{0}:feed/{0}:entry/{0}:content/{1}:properties", FEED_NAMESPACE_PREFIX, METADATA_NAMESPACE_PREFIX);

        private PartsAssembly partsAssembly;

        [Import(typeof(ILogin))]
        public ILogin Login { get; set; }

        [Import(typeof(IEnvironment))]
        public IEnvironment Environment { get; set; }

        public ProductMapper(PartsAssembly partsAssembly,
                               string username,
                               string password)
        {
            this.partsAssembly = partsAssembly;
            this.username = username;
            this.password = password;
        }

        private static bool WriteXMLToFile(String text, String name)
        {
            StreamWriter f = null;

            if (name.IndexOf(".xml", 0) <= 0)
            {
                name += ".xml";
            }
            try
            {
                string filePath = Path.Combine(ConfigurationManager.AppSettings["CabDownloadFolder"], name);
                f = File.CreateText(filePath);

                f.Write(text);
                f.Flush();
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            finally
            {
                if (f != null)
                {
                    f.Close();
                }
            }

            return true;
        }

        public void Run()
        {
            ArrayList failureArray = new ArrayList();

            try
            {
                string[] reportNames = partsAssembly.Metadata.GetReportList();
                foreach (string reportName in reportNames)
                {
                    if (reportName.StartsWith("ProductDetailNonSuiteReport_"))
                    {
                        ArrayList feedNames = partsAssembly.Metadata.GetFeedsForReport(reportName);
                        foreach (Object feedObject in feedNames)
                        {
                            FeedInfo feedInfo = (FeedInfo)feedObject;
                            if (feedInfo.Title == "CabFeed")
                            {
//                                 Console.WriteLine(feedInfo.Title);
//                                 Console.WriteLine(feedInfo.Name);

                                //Console.WriteLine("`````````````````````````````````````");
                                string xml = partsAssembly.Metadata.GetReportFeed(reportName, feedInfo.Name);
                                //Console.WriteLine(xml);

                                int startIndex = reportName.IndexOf("_");
                                int endIndex = reportName.IndexOf(".rdl");
                                string productId = reportName.Substring(startIndex + 1, endIndex - startIndex - 1);

                                XmlReaderSettings settings = new XmlReaderSettings();
                                settings.IgnoreProcessingInstructions = true;
                                XmlDocument xDoc = new XmlDocument();
                                xDoc.LoadXml(xml);
                                XmlNamespaceManager namespcMgr = new XmlNamespaceManager(xDoc.NameTable);
                                namespcMgr.AddNamespace(FEED_NAMESPACE_PREFIX, FEED_NAMESPACE);
                                namespcMgr.AddNamespace(METADATA_NAMESPACE_PREFIX, METADATA_NAMESPACE);
                                namespcMgr.AddNamespace(DATA_SERVICES_NAMESPACE_PREFIX, DATA_SERVICES_NAMESPACE);
                                XmlNodeList nodes = xDoc.SelectNodes(FAILURE_XML_PATH, namespcMgr);

                                string productName = "";
                                string productVersion = "";

                                foreach (XmlNode node in nodes)
                                {
                                    //FailureMetadata metadata = new FailureMetadata();


                                    foreach (XmlNode child in node.ChildNodes)
                                    {
                                        if (child.Name == "d:Product")
                                        {
                                            productName = child.InnerText;
                                        }
                                        else if (child.Name == "d:ProductVersion")
                                        {
                                            productVersion = child.InnerText;
                                        }

                                        if (productName != "" && productVersion != "")
                                        {
                                            break;
                                        }
                                    }
                                    //failureArray.Add(metadata);

                                }

                                Console.WriteLine("ProductId " + productId + ": " + productName + ", Version: " + productVersion);

                                //WriteXMLToFile(xml, reportName + "_" + feedInfo.Title);
                                //Console.WriteLine("`````````````````````````````````````");
                            }
                        }
                    }
                }
            }
            finally
            {
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                MessageBox.Show("Usage: liveIdUsername liveIdPassword");
                return;
            }

            string liveIDUsername = args[0];
            string liveIDPassword = args[1];

            //const string COMPANY_SUMMARY_REPORT_NAME = "CompanySummaryReport.rdl";
            //const string PRODUCT_SUMMARY_REPORT_NAME = "ProductSummaryReport.rdl";
            //const string CAB_LIST_FEED_NAME = "xAx0x3xDx5x2x0x3"; //The list of feeds has the name of the feed in the <Title> element. For this it is <Title>CabTable</Title>
            //const string PSR_CAB_LIST_FEED_NAME = "xAx0x3xDx7x4x0x3"; //The list of feeds has the name of the feed in the <Title> element. For this it is <Title>CabTable</Title>

            PartsAssembly partsAssembly = new PartsAssembly();
            LoadParts(partsAssembly, liveIDUsername, liveIDPassword);

            // this prints out a list of all products and versions and tells you the product id of each (which you'll need to set below)
            //ProductMapper mapper = new ProductMapper(partsAssembly, liveIDUsername, liveIDPassword);
             //mapper.Run();

            /*
                As of February 19, 2013:
                ------------------------
                ProductId 20525: GTA IV PC, Version: 1.0 EFIGS
                ProductId 28934: GTA IV PC, Version: 1.0 Russian
                ProductId 3390: GTA IV PC, Version: 1.0.1 EFIGS
				ProductId 563: GTA IV PC, Version: 1.0.2 EFIGS
				ProductId 15597: GTA IV PC, Version: 1.0.3 EFIGS

                ProductId 188: Rockstar Games Social Club Launcher, Version: 1.1.3.0

                ProductId 17091: L.A. Noire, Version: 2382 (EFIGS v1.0.0.1)
                ProductId 9485: L.A. Noire, Version: 2382 (Russian v1.0.0.1)
				ProductId 6391: L.A. Noire, Version: 2393 (EFIGS)
                ProductId 4826: L.A. Noire, Version: 2393 (Russian)

                ProductId 32325: Max Payne 3 PC, Version: 1.0.0.17 EFIGS
                ProductId 32365: Max Payne 3 PC, Version: 1.0.0.22 EFIGS
                ProductId 32681: Max Payne 3 PC, Version: 1.0.0.28 EFIGS
				ProductId 32680: Max Payne 3 PC, Version: 1.0.0.28 Steam EFIGS
				ProductId 36216: Max Payne 3 PC, Version: 1.0.0.113 EFIGS
				ProductId 36215: Max Payne 3 PC, Version: 1.0.0.113 Steam EFIGS

                ProductId 32333: Social Club, Version: 1.0.9.5
                ProductId 36184: Social Club, Version: 1.1.0.1
             */

//              const string PRODUCT_NAME = "Social Club";
//              const string PRODUCT_VERSION = "1.1.0.1";
//              const string PRODUCT_ID = "36184";

               const string PRODUCT_NAME = "Max Payne 3";
               const string PRODUCT_VERSION = "1.0.0.113 EFIGS";
               const string PRODUCT_ID = "36216";

//                 const string PRODUCT_NAME = "Max Payne 3";
//                 const string PRODUCT_VERSION = "1.0.0.113 Steam EFIGS";
//                 const string PRODUCT_ID = "36215";

            const string PRODUCT_FAILURES_REPORT_NAME = "ProductDetailNonSuiteReport_" + PRODUCT_ID + ".rdl";

            Console.WriteLine("Product: " + PRODUCT_NAME + ", Version: " + PRODUCT_VERSION);

            string PRODUCT_CAB_LIST_FEED_NAME = "";
            string PRODUCT_CAB_LIST_FEED_XML_FILENAME = "";

            /* 
             * Sample code for getting the metadata from the service.
             * Get list of reports*/
            string[] reportNames = partsAssembly.Metadata.GetReportList();
            foreach (string reportName in reportNames)
            {
                if (reportName == PRODUCT_FAILURES_REPORT_NAME)
                {
                    Console.WriteLine("Downloading failure report: " + reportName);
                    ArrayList feedNames = partsAssembly.Metadata.GetFeedsForReport(reportName);
                    foreach (Object feedObject in feedNames)
                    {
                        FeedInfo feedInfo = (FeedInfo)feedObject;
                        if(feedInfo.Title == "CabFeed")
                        {
                            //Console.WriteLine(feedInfo.Title);
                            PRODUCT_CAB_LIST_FEED_NAME = feedInfo.Name;
                            //Console.WriteLine(feedInfo.Name);

                            //Console.WriteLine("`````````````````````````````````````");
                            string xml = partsAssembly.Metadata.GetReportFeed(reportName, feedInfo.Name);
                            //Console.WriteLine(xml);

                            PRODUCT_CAB_LIST_FEED_XML_FILENAME = Path.Combine(ConfigurationManager.AppSettings["CabDownloadFolder"], reportName + "_" + feedInfo.Title + ".xml");
                            WriteXMLToFile(xml, reportName + "_" + feedInfo.Title);
                            //Console.WriteLine("`````````````````````````````````````");
                        }
                    }
                    //Console.WriteLine("*****************************************");
                }
            }
            
            /* 
             * Sample code for getting the metadata from the service.
             * Get list of feeds for a report 
             *
            foreach (Lazy<IMetaItems, IMetaItemTypes> metaItem in partsAssembly.MetaItems)
            {
                if (metaItem.Metadata.Type == MetaItemTypes.ReportList)
                {
                    string[] reportNames2 = metaItem.Value.GetItems();
                    foreach (string reportName in reportNames2)
                    {
                        Console.WriteLine(reportName);
                        Console.WriteLine("-----------------------------------------");
                        foreach (Lazy<IMetaItems, IMetaItemTypes> subMetaItem in partsAssembly.MetaItems)
                        {
                            if (subMetaItem.Metadata.Type == MetaItemTypes.FeedList)
                            {
                                string[] feedNames = subMetaItem.Value.GetItems(reportName);
                                foreach (string feedName in feedNames)
                                {
                                    Console.WriteLine(feedName);
                                }
                                Console.WriteLine("*****************************************");
                            }
                        }
                    }
                }
            }
            */

            /* get's our list of products
            string productSummaryReportName = PRODUCT_SUMMARY_REPORT_NAME;
            string prsCabLinksFeedName = PSR_CAB_LIST_FEED_NAME;
            string[] productNames = null;
            foreach (Lazy<IMetaItems, IMetaItemTypes> metaItem in partsAssembly.MetaItems)
            {
                if (metaItem.Metadata.Type == MetaItemTypes.ProductList)
                {
                    productNames = metaItem.Value.GetItems(productSummaryReportName, prsCabLinksFeedName);
                    foreach (string productName in productNames)
                    {
                        Console.WriteLine(productName);
                    }
                }
            }
            */

            CabFeedAnalyzer analyzer = new CabFeedAnalyzer(partsAssembly, liveIDUsername, liveIDPassword, PRODUCT_NAME, PRODUCT_VERSION);
            analyzer.Run(PRODUCT_CAB_LIST_FEED_XML_FILENAME);

            //string companySummaryReportName = COMPANY_SUMMARY_REPORT_NAME;
            //string cabLinksFeedName = CAB_LIST_FEED_NAME;
//             string[] cabLinks = null;
//             foreach (Lazy<IMetaItems, IMetaItemTypes> metaItem in partsAssembly.MetaItems)
//             {
//                 if (metaItem.Metadata.Type == MetaItemTypes.CabList)
//                 {
//                     cabLinks = metaItem.Value.GetItems(PRODUCT_FAILURES_REPORT_NAME, PRODUCT_CAB_LIST_FEED_NAME);
//                 }
//             }
//             if (cabLinks != null)
//             {
//                 foreach (string cabLink in cabLinks)
//                 {
//                     Console.WriteLine(cabLink);
//                     partsAssembly.CabDownload.Persist(new Uri(cabLink), GetCabFilePath(cabLink));
//                 }
//             }
// 
// #if DEBUG
//             Console.ReadLine();
// #endif
        }

        private static string GetCabFilePath(string cabLink)
        {
            string cabDownloadFolder = ConfigurationManager.AppSettings["CabDownloadFolder"];
            int lastForwardSlashIndex = cabLink.LastIndexOf("/");
            string cabFileName = cabLink.Substring(lastForwardSlashIndex + 1, cabLink.Length - 1 - lastForwardSlashIndex) + ".cab";
            string cabFilePath = Path.Combine(cabDownloadFolder, cabFileName);
            return cabFilePath;
        }

        private static void LoadParts(PartsAssembly partsAssembly, string liveIDUsername, string liveIDPassword)
        {
            try
            {
                string directoryPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string partsDirectoryPath = Path.Combine(directoryPath, "Parts");
                DirectoryCatalog directoryCatalog = new DirectoryCatalog(partsDirectoryPath, "*.dll");
                CompositionContainer container = new CompositionContainer(directoryCatalog);
                container.ComposeExportedValue<string>("LiveIDServiceName", ConfigurationManager.AppSettings["LiveIDServiceName"]); //Input parameter to the PartnerPortalLogin part constructor
                container.ComposeExportedValue<string>("LiveIDApplicationName", ConfigurationManager.AppSettings["LiveIDApplicationName"]); //Input parameter to the PartnerPortalLogin part constructor
                container.ComposeExportedValue<string>("UserName", liveIDUsername); //Input parameter to the Metadata part constructor
                container.ComposeExportedValue<string>("Password", liveIDPassword); //Input parameter to the Metadata part constructor
                container.ComposeParts(partsAssembly); // NOTE: this needs a namespace reference to System.ComponentModel.Composition to work since ComposeParts is an extension property
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }
        }

        /*
         * Accepts ATOM xml as a String
         * Writes XML to file in CabDownloadFolder location */
        private static bool WriteXMLToFile(String text, String name)
        {
            StreamWriter f = null;

            if (name.IndexOf(".xml", 0) <= 0)
            {
                name += ".xml";
            }
            try
            {
                string filePath = Path.Combine(ConfigurationManager.AppSettings["CabDownloadFolder"], name);
                f = File.CreateText(filePath);
                
                f.Write(text);
                f.Flush();
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            finally
            {
                if (f != null)
                {
                    f.Close();
                }
            }

            return true;
        }
    }
}
