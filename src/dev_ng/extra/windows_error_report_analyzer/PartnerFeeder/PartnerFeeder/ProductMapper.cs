﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using Microsoft.Telemetry.MetadataService.FeederLibrary;
using System.Collections;
using Microsoft.Telemetry.MetadataService.ReportMetadata;
using System.Xml;
using System.Net;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;

namespace Microsoft.Telemetry.MetadataService.PartnerFeeder
{
    public class ProductMapper
    {
        private readonly string username;
        private readonly string password;

        private const string FEED_NAMESPACE_PREFIX = "ns";
        private const string METADATA_NAMESPACE_PREFIX = "m";
        private const string DATA_SERVICES_NAMESPACE_PREFIX = "d";
        private const string FEED_NAMESPACE = "http://www.w3.org/2005/Atom";
        private const string METADATA_NAMESPACE = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata";
        private const string DATA_SERVICES_NAMESPACE = "http://schemas.microsoft.com/ado/2007/08/dataservices";
        private string FAILURE_XML_PATH = string.Format("/{0}:feed/{0}:entry/{0}:content/{1}:properties", FEED_NAMESPACE_PREFIX, METADATA_NAMESPACE_PREFIX);

        public List<ProductMapper.Product> Products { get; set; }

        [Serializable()]
        public struct Product
        {
            public string productId;
            public string productVersion;
            public string productName;
        }

        public delegate void OnProductAdded(Product product);
        public event OnProductAdded ProductAdded;

        public delegate void OnReportListDownloaded(string[] reports);
        public event OnReportListDownloaded ReportListDownloaded;

        private PartsAssembly partsAssembly;

        [Import(typeof(ILogin))]
        public ILogin Login { get; set; }

        [Import(typeof(IEnvironment))]
        public IEnvironment Environment { get; set; }

        private Object prodLock = new Object();

        public ProductMapper(PartsAssembly partsAssembly,
                               string username,
                               string password)
        {
            this.partsAssembly = partsAssembly;
            this.username = username;
            this.password = password;

            Products = new List<Product>();
        }

        private static bool WriteXMLToFile(String text, String name)
        {
            StreamWriter f = null;

            if (name.IndexOf(".xml", 0) <= 0)
            {
                name += ".xml";
            }
            try
            {
                string filePath = Path.Combine(ConfigurationManager.AppSettings["CabDownloadFolder"], name);
                f = File.CreateText(filePath);

                f.Write(text);
                f.Flush();
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            finally
            {
                if (f != null)
                {
                    f.Close();
                }
            }

            return true;
        }

        void GetFeedObject(string reportName, Object feedObject)
        {
            FeedInfo feedInfo = (FeedInfo)feedObject;
            if (feedInfo.Title == "CabFeed")
            {
                string xml = partsAssembly.Metadata.GetReportFeed(reportName, feedInfo.Name);
                //Console.WriteLine(xml);

                int startIndex = reportName.IndexOf("_");
                int endIndex = reportName.IndexOf(".rdl");
                string productId = reportName.Substring(startIndex + 1, endIndex - startIndex - 1);

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreProcessingInstructions = true;
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(xml);
                XmlNamespaceManager namespcMgr = new XmlNamespaceManager(xDoc.NameTable);
                namespcMgr.AddNamespace(FEED_NAMESPACE_PREFIX, FEED_NAMESPACE);
                namespcMgr.AddNamespace(METADATA_NAMESPACE_PREFIX, METADATA_NAMESPACE);
                namespcMgr.AddNamespace(DATA_SERVICES_NAMESPACE_PREFIX, DATA_SERVICES_NAMESPACE);
                XmlNodeList nodes = xDoc.SelectNodes(FAILURE_XML_PATH, namespcMgr);

                string productName = "";
                string productVersion = "";

                foreach (XmlNode node in nodes)
                {
                    //FailureMetadata metadata = new FailureMetadata();


                    foreach (XmlNode child in node.ChildNodes)
                    {
                        if (child.Name == "d:Product")
                        {
                            productName = child.InnerText;
                        }
                        else if (child.Name == "d:ProductVersion")
                        {
                            productVersion = child.InnerText;
                        }

                        if (productName != "" && productVersion != "")
                        {
                            break;
                        }
                    }
                    //failureArray.Add(metadata);

                }

                Product product = new Product();
                product.productId = productId;
                product.productName = productName;
                product.productVersion = productVersion;

                lock (prodLock)
                {

                    Products.Add(product);

                    Console.WriteLine("ProductId " + productId + ": " + productName + ", Version: " + productVersion);
                    if (ProductAdded != null)
                    {
                        ProductAdded(product);
                    }
                }

                //WriteXMLToFile(xml, reportName + "_" + feedInfo.Title);
                //Console.WriteLine("`````````````````````````````````````");
            }
        }

        void GetProductFromReport(string reportName)
        {
            if (reportName.StartsWith("ProductDetailNonSuiteReport_"))
            {
                ArrayList feedNames = partsAssembly.Metadata.GetFeedsForReport(reportName);

                bool async = true;
                if (async)
                {
                    Parallel.ForEach(feedNames.Cast<object>(), feedObject =>
                    {
                        GetFeedObject(reportName, feedObject);
                    });
                }
                else
                {
                    foreach (Object feedObject in feedNames)
                    {
                        GetFeedObject(reportName, feedObject);
                    }
                }

            }
        }

        public void Run()
        {
            ArrayList failureArray = new ArrayList();
            Products.Clear();

            try
            {
                string[] reportNames = partsAssembly.Metadata.GetReportList();
                if (ReportListDownloaded != null)
                {
                    ReportListDownloaded(reportNames);
                }

                bool async = true;
                if (async)
                {
                    Parallel.ForEach(reportNames, reportName =>
                    {
                        GetProductFromReport(reportName);
                    });
                }
                else
                {
                    foreach (string reportName in reportNames)
                    {
                        GetProductFromReport(reportName);
                    }
                }
            }
            finally
            {
            }
        }
    }

}
