﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using Microsoft.Telemetry.MetadataService.FeederLibrary;
using System.Collections;
using Microsoft.Telemetry.MetadataService.ReportMetadata;
using System.Xml;
using System.Net;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;

namespace Microsoft.Telemetry.MetadataService.PartnerFeeder
{
    public class ArxMapAnalyzer
    {
        private readonly string username;
        private readonly string password;
        private readonly string productName;
        private readonly string productVersion;

        private const string FEED_NAMESPACE_PREFIX = "ns";
        private const string METADATA_NAMESPACE_PREFIX = "m";
        private const string DATA_SERVICES_NAMESPACE_PREFIX = "d";
        private const string FEED_NAMESPACE = "http://www.w3.org/2005/Atom";
        private const string METADATA_NAMESPACE = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata";
        private const string DATA_SERVICES_NAMESPACE = "http://schemas.microsoft.com/ado/2007/08/dataservices";
        private string CAB_LINK_XPATH = string.Format("/{0}:feed/{0}:entry/{0}:content/{1}:properties/{2}:CabDownloadLink", FEED_NAMESPACE_PREFIX, METADATA_NAMESPACE_PREFIX, DATA_SERVICES_NAMESPACE_PREFIX);
        private string FAILURE_XML_PATH = string.Format("/{0}:feed/{0}:entry/{0}:content/{1}:properties", FEED_NAMESPACE_PREFIX, METADATA_NAMESPACE_PREFIX);
        private PartsAssembly partsAssembly;

        public Dictionary<string, string> FileMap = new Dictionary<string, string>();

        public delegate void OnOutputEvent(string output);
        public event OnOutputEvent OnOutput;

        [Import(typeof(ILogin))]
        public ILogin Login { get; set; }

        [Import(typeof(IEnvironment))]
        public IEnvironment Environment { get; set; }

        public ArxMapAnalyzer(PartsAssembly partsAssembly,
                               string username,
                               string password,
                               string productName,
                               string productVersion)
        {
            this.partsAssembly = partsAssembly;
            this.username = username;
            this.password = password;
            this.productName = productName;
            this.productVersion = productVersion;

            partsAssembly.CabDownload.DownloadFailure += new OnDownloadFailure(CabDownload_DownloadFailure);
        }

        void CabDownload_DownloadFailure(WebException ex)
        {
            Output(ex.Message);
        }

        class FailureMetadata
        {
            public string failureName;
            public int numHits;
            public string offset;
            public string cabLink;
            public string analysisPath;
        };

        class ArxanFailureMetaData : FailureMetadata
        {
            public string arxDumpOutput;
        };

        public class FailureComparer : IComparer
        {
            int IComparer.Compare(object x, object y)
            {
                FailureMetadata a = (FailureMetadata)x;
                FailureMetadata b = (FailureMetadata)y;
                if (a.numHits < b.numHits)
                {
                    return 1;
                }
                else if (a.numHits > b.numHits)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }

        public class ArxanFailureComparer : IComparer
        {
            int IComparer.Compare(object x, object y)
            {
                ArxanFailureMetaData a = (ArxanFailureMetaData)x;
                ArxanFailureMetaData b = (ArxanFailureMetaData)y;
                if (a.numHits < b.numHits)
                {
                    return 1;
                }
                else if (a.numHits > b.numHits)
                {
                    return -1;

                }
                else if (a.offset.CompareTo(b.offset) < 0)
                {
                    return 1;
                }
                else if (a.offset.CompareTo(b.offset) > 0)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }

        private void Output(string output)
        {
            if (OnOutput != null)
            {
                OnOutput(output);
            }
            else
            {
                Console.WriteLine(output);
            }
        }

        private string GetCabFolderName(string cabLink)
        {
            int lastForwardSlashIndex = cabLink.LastIndexOf("/");
            return cabLink.Substring(lastForwardSlashIndex + 1, cabLink.Length - 1 - lastForwardSlashIndex);
        }

        private string GetCabFilename(string cabLink)
        {
            return GetCabFolderName(cabLink) + ".cab";
        }

        private string GetModuleName(string failureName)
        {
            int endIndex = failureName.LastIndexOf("!");
            int startIndex = failureName.LastIndexOf("_", endIndex);
            return failureName.Substring(startIndex + 1, endIndex - startIndex - 1);
        }

        public void Run(string xmlFileName)
        {
            ArrayList failureArray = new ArrayList();

            try
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreProcessingInstructions = true;
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(xmlFileName);
                XmlNamespaceManager namespcMgr = new XmlNamespaceManager(xDoc.NameTable);
                namespcMgr.AddNamespace(FEED_NAMESPACE_PREFIX, FEED_NAMESPACE);
                namespcMgr.AddNamespace(METADATA_NAMESPACE_PREFIX, METADATA_NAMESPACE);
                namespcMgr.AddNamespace(DATA_SERVICES_NAMESPACE_PREFIX, DATA_SERVICES_NAMESPACE);
                XmlNodeList nodes = xDoc.SelectNodes(FAILURE_XML_PATH, namespcMgr);
                foreach (XmlNode node in nodes)
                {
                    ArxanFailureMetaData metadata = new ArxanFailureMetaData();
                    int numHits = 0;
                    double offsetPercent = 0.0;

                    foreach (XmlNode child in node.ChildNodes)
                    {
                        if (child.Name == "d:FailureName")
                        {
                            metadata.failureName = child.InnerText;
                        }
                        else if (child.Name == "d:Hits")
                        {
                            numHits = Convert.ToInt32(child.InnerText);
                        }
                        else if (child.Name == "d:Offset")
                        {
                            metadata.offset = child.InnerText;
                        }
                        else if (child.Name == "d:OffsetPc")
                        {
                            offsetPercent = Convert.ToDouble(child.InnerText);
                        }
                        else if (child.Name == "d:CabDownloadLink")
                        {
                            metadata.cabLink = child.InnerText;
                        }
                    }

                    metadata.numHits = (int)Math.Round(((double)numHits) * (offsetPercent / 100));
                    failureArray.Add(metadata);
                }

                // sort here so we download and analyze the most important crashes first
                failureArray.Sort(new ArxanFailureComparer());
                string cabFolder = ConfigurationManager.AppSettings["CabDownloadFolder"];
                string root = cabFolder + @"\" + productName + @"\" + productVersion + @"\";
                string reportPath = root + "arxanreport.txt";

                int count = 1;

                List<String> SymbolPaths = new List<String>();

                foreach (ArxanFailureMetaData failure in failureArray)
                {
                    Output("Processing ArxMap for dump " + count + " of " + failureArray.Count);
                    string moduleName = GetModuleName(failure.failureName);
                    string cabFileName = GetCabFilename(failure.cabLink);
                    string path = root + moduleName + @"\" + failure.failureName + @"\" + failure.offset + @"\" + GetCabFolderName(failure.cabLink);
                    string cabpath = path + @"\" + cabFileName;


                    string shouldDownload = ConfigurationManager.AppSettings["ArxanDownload"];

                    if (shouldDownload.Equals("true"))
                    {
                        if (String.IsNullOrEmpty(failure.cabLink) == false)
                        {
                            if (Directory.Exists(path) == false)
                            {
                                Directory.CreateDirectory(path);
                            }

                            if (File.Exists(cabpath) == false)
                            {
                                if (File.Exists(path + @"\ignore.txt") == false)
                                {
                                    Output("\tDownloading cab");
                                    if (cabpath.Length < 260)
                                    {
                                        if (!partsAssembly.CabDownload.Persist(new Uri(failure.cabLink), cabpath))
                                        {
                                            Output("\tFailed to download .cab file. Adding ignore.txt to skip this cab next time.");
                                            File.Create(path + @"\ignore.txt");
                                            count++;
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        Output("\tPath is too long: " + cabpath);
                                    }
                                }
                                else
                                {
                                    Output("\tSkipping due to ignore.txt");
                                    count++;
                                    continue;
                                }
                            }
                            else
                            {
                                Output("\tAlready downloaded");
                            }
                        }
                    }
                    string expandedFolder = path + @"\" + "expanded";
                    int fileCount = 0;
                    bool folderExists = false;

                    string shouldExpand = ConfigurationManager.AppSettings["ArxanExpand"];

                    if (shouldExpand.Equals("true"))
                    {
                        
                        folderExists = Directory.Exists(expandedFolder);
                        if (folderExists)
                        {
                            fileCount = Directory.GetFiles(expandedFolder, "*.*", SearchOption.TopDirectoryOnly).Length;
                        }

                        if ((fileCount == 0) || (folderExists == false))
                        {
                            Output("\tExpanding Cab");
                            Directory.CreateDirectory(expandedFolder);
                            string commandLine = "x" + " " + "\"" + cabpath + "\"" + " " + "\"" + expandedFolder + "\"";

                            System.Diagnostics.Process process = new System.Diagnostics.Process();
                            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                            startInfo.FileName = "C:\\Program Files\\WinRAR\\winrar.exe";
                            startInfo.Arguments = commandLine;
                            process.StartInfo = startInfo;
                            process.Start();
                            process.WaitForExit();
                        }
                        else
                        {
                            Output("\tAlready expanded");
                        }
                    }
                    else if (String.IsNullOrEmpty(failure.cabLink) == false)
                    {
                        if (Directory.Exists(path) == false)
                        {
                            Output("Cab Folder not found! Did you run analyze? Or maybe set ArxanExpand in the app.config to true?");
                            count++;
                            continue;
                        }
                    }

                    string imagePath = ConfigurationManager.AppSettings["CabImagesRoot"];
                    Directory.CreateDirectory(imagePath);
                    string myImagePath = imagePath + @"\" + productName + @"\" + productVersion + @"\images";
                    Directory.CreateDirectory(myImagePath);
                    DirectoryInfo di = new DirectoryInfo(myImagePath);
                    if (di.GetFiles().Length == 0)
                    {
                        Output("Image path is empty, copying from File Server");
                        string WERImagesRoot = ConfigurationManager.AppSettings["WERImagesRoot"];
                        string src = WERImagesRoot + @"\" + productName + @"\" + productVersion + @"\images";
                        if (!Directory.Exists(src))
                        {
                            Output(" ERROR: Images source " + src + " does not exist! ");
                        }
                        FolderCopy(src, myImagePath);
                    }

                    string symbolsPath = myImagePath;
                    if (!SymbolPaths.Contains(symbolsPath))
                    {
                        SymbolPaths.Add(symbolsPath);
                    }

                    // find other symbols
                    string appCompat = expandedFolder + "\\AppCompat.txt";
                    if (File.Exists(appCompat))
                    {
                        DATABASE d = null;

                        try
                        {
                            d = DATABASE.Deserialize(appCompat);
                            foreach (DATABASEEXE e in d.Items)
                            {
                                if (FileMap.ContainsKey(e.NAME))
                                {
                                    string extraSymbolPath = imagePath + @"\" + FileMap[e.NAME];
                                    if (Directory.Exists(extraSymbolPath))
                                    {
                                        var directory = new DirectoryInfo(extraSymbolPath);
                                        var dirs = directory.GetDirectories();
                                        if (dirs.Length > 0)
                                        {
                                            string extraProductVersion = (from f in dirs orderby f.Name descending select f).First().Name;

                                            extraSymbolPath = extraSymbolPath + @"\" + extraProductVersion + @"\images";
                                            if (!SymbolPaths.Contains(extraSymbolPath))
                                            {
                                                SymbolPaths.Add(extraSymbolPath);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Output("This dump relies on symbols you don't have copied yet. Recommend revisiting this product after handling " + FileMap[e.NAME]);
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {

                        }
                    }

                    DirectoryInfo dir = new DirectoryInfo(myImagePath);
                    FileInfo[] files = dir.GetFiles("*arxmap*");
                    if (files.Length == 0)
                    {
                        Output("No arxmap files in path '" + myImagePath + "' did not exist...continuing...");
                        count++;
                        continue;
                    }


                    string failureReportPath = expandedFolder + "\\arxdump.report";
                    if (!File.Exists(appCompat))
                    {

                        string fullSymbolPath = "";
                        foreach (string s in SymbolPaths)
                            fullSymbolPath += (s + ";");

                        fullSymbolPath += expandedFolder;
                        string ArxDumpPath = ConfigurationManager.AppSettings["ArxDumpPath"];

                        StringBuilder outputStringBuilder = new StringBuilder();

                        var process = new Process();
                        process.StartInfo.FileName = ArxDumpPath;
                        process.StartInfo.Arguments = "\"" + fullSymbolPath + "\"";
                        process.StartInfo.RedirectStandardOutput = true;
                        process.StartInfo.CreateNoWindow = true;
                        process.StartInfo.UseShellExecute = false;
                        process.OutputDataReceived += (sender, a) => outputStringBuilder.AppendLine(a.Data);
                        process.Start();
                        process.BeginOutputReadLine();
                        process.WaitForExit();

                        failure.arxDumpOutput = outputStringBuilder.ToString();
                        File.WriteAllLines(failureReportPath, failure.arxDumpOutput.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None));

                        // Move onwards with our count
                    }
                    count++;
                }


                Output("Generating report: " + reportPath);

                // report
                List<String> hitsArray = new List<string>();
                StreamWriter outfile = new StreamWriter(reportPath);

                //Hits	Failure Name / Offset
                //----	---------------------
                outfile.WriteLine("Hits\tFailure Name / Offset");
                outfile.WriteLine("----\t---------------------");
                foreach (ArxanFailureMetaData failure in failureArray)
                {
                    string line = failure.numHits + "\t" + failure.failureName + " Offset " + failure.offset;
                    if (hitsArray.Contains(line) == false)
                    {
                        hitsArray.Add(line);

                        outfile.WriteLine(line);
                    }

                    outfile.WriteLine("\t\t\t" + failure.analysisPath);
                    outfile.WriteLine(failure.arxDumpOutput);
                }

                outfile.Close();
            }
            catch(Exception e)
            {
                Output(e.Message);
            }
            finally
            {

            }
        }

        class ArxReport
        {
            public List<ArxThread> Threads = new List<ArxThread>();
        };

        class ArxThread
        {
            public List<String> Frames = new List<String>();
        }

        private static string ByteArrayToString(byte[] byteArray)
        {
            StringBuilder builder = new StringBuilder();
            foreach (byte b in byteArray)
            {
                builder.AppendFormat("{0:X2}", b);
            }
            return builder.ToString().ToUpper();
        }

        private static void FileCopy(string src, string dst)
        {
            // If copying directories, call FolderCopy instead
            if (Directory.Exists(src) && Directory.Exists(dst))
            {
                FolderCopy(src, dst);
                return;
            }
            // If source is directory, create the destination
            else if (Directory.Exists(src))
            {
                Directory.CreateDirectory(dst);
                FolderCopy(src, dst);
                return;
            }

            // If destination is a directory, create the full path
            if (Directory.Exists(dst))
            {
                FileInfo f = new FileInfo(src);
                dst = Path.Combine(dst, f.Name);
            }

            DateTime srctime = DateTime.MaxValue;
            DateTime destTime = DateTime.MinValue;

            // Clear read-only flags
            if (File.Exists(dst))
            {
                FileInfo f = new FileInfo(dst);
                if (f.IsReadOnly)
                {
                    File.SetAttributes(f.FullName, FileAttributes.Normal);
                }
            }

            File.Copy(src, dst, true);
        }

        private static void FolderCopy(string src, string dest)
        {
            DirectoryInfo srcDir = new DirectoryInfo(src);
            DirectoryInfo destDir = new DirectoryInfo(dest);

            if (!destDir.Exists)
            {
                Directory.CreateDirectory(dest);
            }

            DirectoryInfo folder = new DirectoryInfo(dest);
            if ((folder.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
            {
                folder.Attributes ^= FileAttributes.ReadOnly;
            }

            FileInfo[] files = srcDir.GetFiles();
            foreach (FileInfo file in files)
            {
                FileCopy(file.FullName, dest);
            }

            DirectoryInfo[] subDirs = srcDir.GetDirectories();
            foreach (DirectoryInfo subDir in subDirs)
            {
                string path = Path.Combine(dest, subDir.Name);
                FolderCopy(subDir.FullName, path);
            }
        }
    }
}
