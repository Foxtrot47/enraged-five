﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Telemetry.MetadataService.FeederLibrary;
using System.ComponentModel.Composition;

namespace Microsoft.Telemetry.MetadataService.PartnerFeeder
{
    public class PartsAssembly
    {
        [Import(typeof(ILogin))]
        public ILogin Login { get; set; }

        [Import(typeof(IMetadata))]
        public IMetadata Metadata { get; set; }

        [ImportMany(typeof(IMetaItems))]
        public Lazy<IMetaItems, IMetaItemTypes>[] MetaItems { get; set; }

        [Import(typeof(IPersist))]
        public IPersist CabDownload { get; set; }
    }
}
