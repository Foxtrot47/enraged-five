using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using Microsoft.Telemetry.MetadataService.FeederLibrary;
using System.Collections;
using Microsoft.Telemetry.MetadataService.ReportMetadata;
using System.Xml;
using System.Net;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;

namespace Microsoft.Telemetry.MetadataService.PartnerFeeder
{
    public class CabFeedAnalyzer
    {
        private readonly string username;
        private readonly string password;
        private readonly string productName;
        private readonly string productVersion;

        private const string FEED_NAMESPACE_PREFIX = "ns";
        private const string METADATA_NAMESPACE_PREFIX = "m";
        private const string DATA_SERVICES_NAMESPACE_PREFIX = "d";
        private const string FEED_NAMESPACE = "http://www.w3.org/2005/Atom";
        private const string METADATA_NAMESPACE = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata";
        private const string DATA_SERVICES_NAMESPACE = "http://schemas.microsoft.com/ado/2007/08/dataservices";
        private string CAB_LINK_XPATH = string.Format("/{0}:feed/{0}:entry/{0}:content/{1}:properties/{2}:CabDownloadLink", FEED_NAMESPACE_PREFIX, METADATA_NAMESPACE_PREFIX, DATA_SERVICES_NAMESPACE_PREFIX);
        private string FAILURE_XML_PATH = string.Format("/{0}:feed/{0}:entry/{0}:content/{1}:properties", FEED_NAMESPACE_PREFIX, METADATA_NAMESPACE_PREFIX);
        private PartsAssembly partsAssembly;

        public Dictionary<string, string> FileMap = new Dictionary<string, string>();

        public delegate void OnOutputEvent(string output);
        public event OnOutputEvent OnOutput;

        public delegate void OnProductAnalyzed(int cur, int total);
        public event OnProductAnalyzed ProductAnalyzed;

        [Import(typeof(ILogin))]
        public ILogin Login { get; set; }

        [Import(typeof(IEnvironment))]
        public IEnvironment Environment { get; set; }

        public CabFeedAnalyzer(PartsAssembly partsAssembly,
                               string username,
                               string password,
                               string productName,
                               string productVersion)
        {
            this.partsAssembly = partsAssembly;
            this.username = username;
            this.password = password;
            this.productName = productName;
            this.productVersion = productVersion;

            partsAssembly.CabDownload.DownloadFailure += new OnDownloadFailure(CabDownload_DownloadFailure);
        }

        void CabDownload_DownloadFailure(WebException ex)
        {
            Output(ex.Message);
        }

        class FailureMetadata
        {
            public string failureName;
            public int numHits;
            public string offset;
            public string cabLink;
            public string analysisPath;
        };


        public class FailureComparer : IComparer
        {
            int IComparer.Compare(object x, object y)
            {
                FailureMetadata a = (FailureMetadata)x;
                FailureMetadata b = (FailureMetadata)y;
                if (a.numHits < b.numHits)
                {
                    return 1;
                }
                else if (a.numHits > b.numHits)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }

        private void Output(string output)
        {
            if (OnOutput != null)
            {
                OnOutput(output);
            }
            else
            {
                Console.WriteLine(output);
            }
        }

        private string GetCabFolderName(string cabLink)
        {
            int lastForwardSlashIndex = cabLink.LastIndexOf("/");
            return cabLink.Substring(lastForwardSlashIndex + 1, cabLink.Length - 1 - lastForwardSlashIndex);
        }

        private string GetCabFilename(string cabLink)
        {
            return GetCabFolderName(cabLink) + ".cab";
        }

        private string GetModuleName(string failureName)
        {
            int endIndex = failureName.LastIndexOf("!");
            int startIndex = failureName.LastIndexOf("_", endIndex);
            return failureName.Substring(startIndex + 1, endIndex - startIndex - 1);
        }

        private string MakeValidFileName(string name)
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "_");
        }

        public void Run(string xmlFileName)
        {
            ArrayList failureArray = new ArrayList();

            bool dontDownload = false;
            bool dontExpand = false;
            bool dontAnalyze = false;

            try
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreProcessingInstructions = true;
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(xmlFileName);
                XmlNamespaceManager namespcMgr = new XmlNamespaceManager(xDoc.NameTable);
                namespcMgr.AddNamespace(FEED_NAMESPACE_PREFIX, FEED_NAMESPACE);
                namespcMgr.AddNamespace(METADATA_NAMESPACE_PREFIX, METADATA_NAMESPACE);
                namespcMgr.AddNamespace(DATA_SERVICES_NAMESPACE_PREFIX, DATA_SERVICES_NAMESPACE);
                XmlNodeList nodes = xDoc.SelectNodes(FAILURE_XML_PATH, namespcMgr);
                foreach (XmlNode node in nodes)
                {
                    FailureMetadata metadata = new FailureMetadata();
                    int numHits = 0;
                    double offsetPercent = 0.0;

                    foreach (XmlNode child in node.ChildNodes)
                    {
                        if (child.Name == "d:FailureName")
                        {
                            metadata.failureName = child.InnerText;
                        }
                        else if (child.Name == "d:Hits")
                        {
                            numHits = Convert.ToInt32(child.InnerText);
                        }
                        else if (child.Name == "d:Offset")
                        {
                            metadata.offset = child.InnerText;
                        }
                        else if (child.Name == "d:OffsetPc")
                        {
                            offsetPercent = Convert.ToDouble(child.InnerText);
                        }
                        else if (child.Name == "d:CabDownloadLink")
                        {
                            metadata.cabLink = child.InnerText;
                        }
                    }

                    metadata.numHits = (int)Math.Round(((double)numHits) * (offsetPercent / 100));
                    failureArray.Add(metadata);
                }

                // sort here so we download and analyze the most important crashes first
                failureArray.Sort(new FailureComparer());
                string cabFolder = ConfigurationManager.AppSettings["CabDownloadFolder"];
                string root = cabFolder + @"\" + productName + @"\" + productVersion + @"\";
                string reportPath = root + "report.txt";

                int count = 1;
                foreach (FailureMetadata failure in failureArray)
                {
                    Output("Processing failure " + count + " of " + failureArray.Count);
                    string moduleName = GetModuleName(failure.failureName);
                    string cabFileName = GetCabFilename(failure.cabLink);
                    string path = root + MakeValidFileName(moduleName) + @"\" + MakeValidFileName(failure.failureName) + @"\" + MakeValidFileName(failure.offset) + @"\" + MakeValidFileName(GetCabFolderName(failure.cabLink));
                    string cabpath = path + @"\" + MakeValidFileName(cabFileName);

                    if (dontDownload == false)
                    {
                        if (String.IsNullOrEmpty(failure.cabLink) == false)
                        {
                            if (Directory.Exists(path) == false)
                            {
                                Directory.CreateDirectory(path);
                            }

                            if (File.Exists(cabpath) == false)
                            {
                                if (File.Exists(path + @"\ignore.txt") == false)
                                {
                                    Output("\tDownloading cab");
                                    if (cabpath.Length < 260)
                                    {
                                        if (!partsAssembly.CabDownload.Persist(new Uri(failure.cabLink), cabpath))
                                        {
                                            Output("\tFailed to download .cab file. Adding ignore.txt to skip this cab next time.");
                                            File.Create(path + @"\ignore.txt");
                                            count++;
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        Output("\tPath is too long: " + cabpath);
                                    }
                                }
                                else
                                {
                                    Output("\tSkipping due to ignore.txt");
                                    count++;
                                    if (ProductAnalyzed != null)
                                    {
                                        ProductAnalyzed(count, failureArray.Count);
                                    }
                                    continue;
                                }
                            }
                            else
                            {
                                Output("\tAlready downloaded");
                            }
                        }
                    }

                    if (dontExpand == false)
                    {
                        string expandedFolder = path + @"\" + "expanded";
                        int fileCount = 0;
                        bool folderExists = Directory.Exists(expandedFolder);
                        if (folderExists)
                        {
                            fileCount = Directory.GetFiles(expandedFolder, "*.*", SearchOption.TopDirectoryOnly).Length;
                        }

                        if ((fileCount == 0) || (folderExists == false))
                        {
                            Output("\tExpanding Cab");
                            Directory.CreateDirectory(expandedFolder);
                            string commandLine = "x" + " " + "\"" + cabpath + "\"" + " " + "\"" + expandedFolder + "\"";

                            System.Diagnostics.Process process = new System.Diagnostics.Process();
                            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                            startInfo.FileName = "C:\\Program Files\\WinRAR\\winrar.exe";
                            startInfo.Arguments = commandLine;
                            process.StartInfo = startInfo;
                            process.Start();
                            process.WaitForExit();
                        }
                        else
                        {
                            Output("\tAlready expanded");
                        }

                        if (dontAnalyze == false)
                        {
                            string analyzedFolder = path + @"\" + "analyzed";
                            fileCount = 0;
                            folderExists = Directory.Exists(analyzedFolder);
                            if (folderExists)
                            {
                                fileCount = Directory.GetFiles(analyzedFolder, "*.*", SearchOption.TopDirectoryOnly).Length;
                            }

                            if ((fileCount == 0) || (folderExists == false))
                            {
                                Output("\tAnalyzing Crash Dump");
                                if (folderExists == false)
                                {
                                    Directory.CreateDirectory(analyzedFolder);
                                }
                                
                                Directory.SetCurrentDirectory(analyzedFolder);

                                string winDbgPath = ConfigurationManager.AppSettings["WinDbgPath"];

                                string imagePath = ConfigurationManager.AppSettings["CabImagesRoot"];
                                Directory.CreateDirectory(imagePath);
                                string myImagePath = imagePath + @"\" + productName + @"\" + productVersion + @"\images";
                                Directory.CreateDirectory(myImagePath);
                                DirectoryInfo di = new DirectoryInfo(myImagePath);
                                if (di.GetFiles().Length == 0)
                                {
                                    Output("Image path is empty, copying from File Server");
                                    string WERImagesRoot = ConfigurationManager.AppSettings["WERImagesRoot"];
                                    string src = WERImagesRoot + @"\" + productName + @"\" + productVersion + @"\images";
                                    if (!Directory.Exists(src))
                                    {
                                        Output(" ERROR: Images source " + src + " does not exist! ");
                                    }
                                    FolderCopy(src, myImagePath);
                                }
                                
                                string symbolsPath = myImagePath;

                                // find other symbols
                                string appCompat = expandedFolder + "\\AppCompat.txt";
                                if (File.Exists(appCompat))
                                {
                                    DATABASE d = null;

                                    try
                                    {
                                        d = DATABASE.Deserialize(appCompat);

                                        foreach (DATABASEEXE e in d.Items)
                                        {
                                            if (FileMap.ContainsKey(e.NAME))
                                            {
                                                string extraSymbolPath = imagePath + @"\" + FileMap[e.NAME];
                                                if (Directory.Exists(extraSymbolPath))
                                                {
                                                    var directory = new DirectoryInfo(extraSymbolPath);
                                                    var dirs = directory.GetDirectories();
                                                    if (dirs.Length > 0)
                                                    {
                                                        string extraProductVersion = (from f in dirs
                                                                                      orderby f.Name descending
                                                                                      select f).First().Name;

                                                        extraSymbolPath = extraSymbolPath + @"\" + extraProductVersion + @"\images";
                                                        if (!symbolsPath.Contains(extraSymbolPath))
                                                        {
                                                            symbolsPath += (";" + extraSymbolPath);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Output("This dump relies on symbols you don't have copied yet. Recommend revisiting this product after handling " + FileMap[e.NAME]);
                                                }
                                            }
                                        }
                                    }
                                    catch(Exception)
                                    {

                                    }
                                }

                                symbolsPath = symbolsPath + @";SRV*http://msdl.microsoft.com/download/symbols;SRV*N:\RSGEDI\Symbol\Symbol_Server";
                                string winDbgScriptPath = ConfigurationManager.AppSettings["WinDbgScript"];
                                string commandLine = "for /f \"tokens=*\" %a IN ('dir /b \"" + expandedFolder + "\\*dmp\"') do \"" + winDbgPath + "\" -y \"" + symbolsPath + "\" -i \"" + imagePath + "\" -z \"" + expandedFolder + "\\%a\" -c \"$$>< " + winDbgScriptPath + ";q\" -Q -QS -QY -QSY";
                                //Output(commandLine);
                                ProcessStartInfo processinfo = new ProcessStartInfo("cmd.exe", "/c " + commandLine);
                                Process process = Process.Start(processinfo);
                                process.WaitForExit();
                            }
                            else
                            {
                                Output("\tAlready analyzed");
                            }

                            string[] files = Directory.GetFiles(analyzedFolder, "*.log", SearchOption.TopDirectoryOnly);
                            if (files.Length > 0)
                            {
                                failure.analysisPath = files[0];
                            }
                        }

                    }

                    count++;

                    if (ProductAnalyzed != null)
                    {
                        ProductAnalyzed(count, failureArray.Count);
                    }
                }

                Output("Generating report: " + reportPath);

                // report
                List<String> hitsArray = new List<string>();
                StreamWriter outfile = new StreamWriter(reportPath);

                //Hits	Failure Name / Offset
                //----	---------------------
                outfile.WriteLine("Hits\tFailure Name / Offset");
                outfile.WriteLine("----\t---------------------");
                foreach (FailureMetadata failure in failureArray)
                {
                    string line = failure.numHits + "\t" + failure.failureName + " Offset " + failure.offset;
                    if (hitsArray.Contains(line) == false)
                    {
                        hitsArray.Add(line);

                        outfile.WriteLine(line);
                    }

                    outfile.WriteLine("\t\t\t" + failure.analysisPath);

                    string fileLine;

                    if (String.IsNullOrEmpty(failure.analysisPath) == false)
                    {
                        StreamReader file = new StreamReader(failure.analysisPath);
                        bool start = false;
                        while ((fileLine = file.ReadLine()) != null)
                        {
                            if (fileLine.StartsWith("STACK_TEXT:"))
                            {
                                start = true;
                            }
                            else if (start == true)
                            {
                                if (fileLine == "")
                                {
                                    outfile.WriteLine("");
                                    break;
                                }
                                else
                                {
                                    int endIndex = fileLine.IndexOf("!");
                                    if (endIndex >= 0)
                                    {
                                        int startIndex = fileLine.LastIndexOf(" ", endIndex);
                                        outfile.WriteLine("\t\t\t\t\t" + fileLine.Substring(startIndex + 1));
                                    }
                                    else
                                    {
                                        outfile.WriteLine("\t\t\t\t\t" + fileLine);
                                    }
                                }
                            }
                        }

                        file.Close();
                    }
                }

                outfile.Close();
            }
            finally
            {
            }

            Output("Finished.");
        }

        private static string ByteArrayToString(byte[] byteArray)
        {
            StringBuilder builder = new StringBuilder();
            foreach (byte b in byteArray)
            {
                builder.AppendFormat("{0:X2}", b);
            }
            return builder.ToString().ToUpper();
        }

        private static void FileCopy(string src, string dst)
        {
            // If copying directories, call FolderCopy instead
            if (Directory.Exists(src) && Directory.Exists(dst))
            {
                FolderCopy(src, dst);
                return;
            }
            // If source is directory, create the destination
            else if (Directory.Exists(src))
            {
                Directory.CreateDirectory(dst);
                FolderCopy(src, dst);
                return;
            }

            // If destination is a directory, create the full path
            if (Directory.Exists(dst))
            {
                FileInfo f = new FileInfo(src);
                dst = Path.Combine(dst, f.Name);
            }

            DateTime srctime = DateTime.MaxValue;
            DateTime destTime = DateTime.MinValue;

            // Clear read-only flags
            if (File.Exists(dst))
            {
                FileInfo f = new FileInfo(dst);
                if (f.IsReadOnly)
                {
                    File.SetAttributes(f.FullName, FileAttributes.Normal);
                }
            }

            File.Copy(src, dst, true);
        }

        private static void FolderCopy(string src, string dest)
        {
            DirectoryInfo srcDir = new DirectoryInfo(src);
            DirectoryInfo destDir = new DirectoryInfo(dest);

            if (!destDir.Exists)
            {
                Directory.CreateDirectory(dest);
            }

            DirectoryInfo folder = new DirectoryInfo(dest);
            if ((folder.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
            {
                folder.Attributes ^= FileAttributes.ReadOnly;
            }

            FileInfo[] files = srcDir.GetFiles();
            foreach (FileInfo file in files)
            {
                FileCopy(file.FullName, dest);
            }

            DirectoryInfo[] subDirs = srcDir.GetDirectories();
            foreach (DirectoryInfo subDir in subDirs)
            {
                string path = Path.Combine(dest, subDir.Name);
                FolderCopy(subDir.FullName, path);
            }
        }
    }
}
