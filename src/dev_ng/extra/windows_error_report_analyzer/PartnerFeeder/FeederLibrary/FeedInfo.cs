﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Telemetry.MetadataService.FeederLibrary
{
    public class FeedInfo
    {
        private string title;
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public FeedInfo(string title, string name)
        {
            Title = title;
            Name = name;
        }
    }
}
