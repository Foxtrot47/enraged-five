﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Telemetry.MetadataService.FeederLibrary
{
    public interface IEnvironment
    {
        string EnvironmentType { get; }
        Uri ReportListURL { get; }
        Uri GetReportFeedListURL(string reportName);
        Uri GetReportFeedURL(string reportName, string feedName);
        Uri TransferServiceURL { get; }
    }
}
