﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Telemetry.MetadataService.FeederLibrary
{
    public interface IMetaItems
    {
        string[] GetItems(params string[] args);
    }

    public enum MetaItemTypes
    {
        Undefined,
        ProductList,
        ReportList,
        FeedList,
        CabList
    }

    public interface IMetaItemTypes
    {
        MetaItemTypes Type { get; }
    }
}
