﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Microsoft.Telemetry.MetadataService.FeederLibrary
{
    public interface ILogin
    {
        HttpWebRequest GetAuthenticatedRequest(HttpWebRequest request, string username, string password);
    }
}
