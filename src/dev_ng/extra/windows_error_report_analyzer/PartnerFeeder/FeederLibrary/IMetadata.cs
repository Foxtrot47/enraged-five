﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;

namespace Microsoft.Telemetry.MetadataService.FeederLibrary
{
    public interface IMetadata
    {
        string[] GetReportList();
        ArrayList GetFeedsForReport(string reportName);
        string GetReportFeed(string reportName, string feedName);
    }
}
