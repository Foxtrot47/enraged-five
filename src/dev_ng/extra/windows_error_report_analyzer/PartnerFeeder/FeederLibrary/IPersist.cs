﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.IO;

namespace Microsoft.Telemetry.MetadataService.FeederLibrary
{
    public delegate void OnDownloadFailure(WebException ex);

    public interface IPersist
    {
        event OnDownloadFailure DownloadFailure;
        bool Persist(Uri resourceUri, string persistTo);
    }
}
