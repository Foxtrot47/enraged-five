#!/usr/bin/ruby -w
#
# Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
#
require 'getoptlong'
require 'Logger'
require 'singleton'
require 'fileutils'
# <globals>

# Setting up
RAGE_DIR = ENV['RAGE_DIR']
RS_CODEBRANCH = ENV['RS_CODEBRANCH']
RS_BUILDBRANCH = ENV['RS_BUILDBRANCH']

PROTECTION_PATH = RS_CODEBRANCH + "/extra/protection"
GENERATED_PATH = PROTECTION_PATH + "/generated"
GUARDSCRIPT_PATH = PROTECTION_PATH + "/template.gsml"

GUARDIT_VERSION_DIR = "/8.3.0"
GUARDIT_ROOT = RAGE_DIR + "/3rdparty/Arxan/GuardIT" + GUARDIT_VERSION_DIR
GUARDIT_PATH = GUARDIT_ROOT + "/plugins/com.arxan.guardit_8.3.0"

# Defines
LOG_NAME = ""
DEFAULT_OBFUSCATION_LEVEL = "2"
DEFAULT_VERBOSE = false
DEFAULT_DEBUG = false
DEFAULT_RELEASE = ""
DEFAULT_ARCH = "x64"
DEFAULT_CLOBBER = false
CURR_TIME = Time.now.strftime("%Y%m%d.%H.%M.%S")              
        
# </globals>

# <loggingSetup>


# Make sure that our generated directory is created
# First our protection path
begin
    Dir.mkdir(PROTECTION_PATH)
rescue Exception =>e
    puts e.message
end
# Then our generated path
begin
    Dir.mkdir(GENERATED_PATH)
rescue Exception =>e
    puts e.message
end



class MultiDelegator
  def initialize(*targets)
    @targets = targets
  end

  def self.delegate(*methods)
    methods.each do |m|
      define_method(m) do |*args|
        @targets.map { |t| t.send(m, *args) }
      end
    end
    self
  end

  class <<self
    alias to new
  end
end

class RsgLog
    include Singleton
    attr_accessor :log
    def initialize()
        log_file = File.open(GENERATED_PATH + "/" + CURR_TIME + ".log", "w")
        @log = Logger.new MultiDelegator.delegate(:write, :close).to(STDOUT, log_file)

        @log.datetime_format = "%Y%m%d-%H:%M:%S"
        @log.level = Logger::INFO
        @log.formatter = proc do |serverity, time, progname, msg|
          "[#{time.strftime(@log.datetime_format)}]\t[#{serverity}]\t#{msg}"
    end
    def info(msg, newline=true)
        if newline == true
            msg = msg + "\n"
        end
        @log.info(msg)
    end
    def debug(msg, newline=true)
        if newline == true
            msg = msg + "\n"
        end
        @log.debug(msg)
    end
    def warn(msg, newline=true)
        if newline == true
            msg = msg + "\n"
        end
        @log.warn(msg)
    end
    def error(msg, newline=true)
        if newline == true
            msg = msg + "\n"
        end
        @log.error(msg)
    end
    
    def SetLevel(level)
        @log.level = level
    end
end


end
# </loggingSetup>

# <banner>
log = RsgLog.instance();
log.info("   _   _   _   _   _   _   _   _     _   _   _   _   _  ");
log.info("  / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\   / \\ / \\ / \\ / \\ / \\ ");
log.info(" ( R | o | c | k | s | t | a | r ) ( G | a | m | e | s )");
log.info("  \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/   \\_/ \\_/ \\_/ \\_/ \\_/ ");
log.info("                                       Protection Script");
log.info("");
# </banner>

# <commandLineArguments>


class Parameters
    
    attr_reader :verbose
    attr_reader :debug
    attr_reader :release
    attr_reader :guardscript
    attr_reader :guarditroot
    attr_reader :architecture
    attr_reader :clobber
    
    def initialize
        @verbose = DEFAULT_VERBOSE #false
        @debug = DEFAULT_DEBUG #false
        @release = DEFAULT_RELEASE #nil
        @guardscript = GUARDSCRIPT_PATH
        @guarditroot = GUARDIT_PATH
        @architecture = DEFAULT_ARCH
        @clobber = DEFAULT_CLOBBER
        
        @opts = GetoptLong.new(
          [ '--help', '-h',         GetoptLong::NO_ARGUMENT ],
          [ '--verbose', '-v',      GetoptLong::NO_ARGUMENT ],
          [ '--debug', '-d',        GetoptLong::NO_ARGUMENT ],
          [ '--release', '-r',      GetoptLong::REQUIRED_ARGUMENT ],
          [ '--guardscript', '-g',  GetoptLong::REQUIRED_ARGUMENT ],
          [ '--guarditroot', '-p',  GetoptLong::REQUIRED_ARGUMENT ],
          [ '--arch', '-a',         GetoptLong::REQUIRED_ARGUMENT ],
          [ '--clobber', '-c',      GetoptLong::NO_ARGUMENT ]
          
        )
    end
    
    def PrintHelp()
        RsgLog.instance().info("Usage:  ruby protectit.rb -r <release> [options]                               ");
        RsgLog.instance().info("                                                                               ");
        RsgLog.instance().info("-h      --help          Display this                                           ");
        RsgLog.instance().info("-v      --verbose       Displays the output from GuardIT to stdout             ");
        RsgLog.instance().info("-d      --debug         Displays how the command line arguments are parsed, and");
        RsgLog.instance().info("                        skips running GuardIT                                  ");
        RsgLog.instance().info("-r      --release       Required. Supported releases are:                      ");
        RsgLog.instance().info("                        bankrelease beta debug final release                   ");
        RsgLog.instance().info("                        Default is bankrelease                                 ");
        RsgLog.instance().info("-g      -guardscript    Override the guardscript to specify your own.          ");
        RsgLog.instance().info("                        Default is generated from                              ");
        RsgLog.instance().info("                            $(RS_BUILDBRANCH)/protection/template.gsml         ");
        RsgLog.instance().info("-p      -guarditroot    Specifies the root path of GuardIT, in case somebody   ");
        RsgLog.instance().info("                        wants to be rebellious and install it somewhere else   ");
        RsgLog.instance().info("-a      -arch           Specifies the architecture of GuardIT used.            ");
        RsgLog.instance().info("                        Supported architectures are: x64 (default) x86         ");
        RsgLog.instance().info("                        Default is x64                                         ");
        RsgLog.instance().info("-c      --clobber       Takes the generated protected executable and overwrites");
        RsgLog.instance().info("                        the copy in $(RS_BUILDBRANCH). NOT SUPPORTED YET       ");
    end           
    def ParseArguments()
        @opts.each do |opt, arg|
            case opt
            when '--help'
                PrintHelp()
                exit
            when '--verbose'
                @verbose = true
            when '--debug'
                @debug = true
            when '--release'
                case arg
                    when /release/i,/bankrelease/i,/debug/i,/final/i,/beta/i
                        @release = arg
                    else
                        RsgLog.instance().error("Invalid release [#{arg}] specified")
                        exit
                end
            when '--guardscript'
                @guardscript = arg
            when '--guarditroot'
                @guarditroot = arg
            when '--arch'
                case arg
                    when /x64/i,/x86/i
                        @architecture = arg
                    else
                        RsgLog.instance().error("Invalid architecture [#{arg}] specified")
                        exit
                end
            when '--clobber'
                @clobber = true
            end
        end
    end
end

class GuardScriptFiller
    def self.Populate(fileContents, parameters)
        newContents = fileContents
        newContents = newContents.gsub("$OBFUSCATION_LEVEL", DEFAULT_OBFUSCATION_LEVEL )
        newContents = newContents.gsub("$RS_CODEBRANCH", RS_CODEBRANCH )
        newContents = newContents.gsub("$RELEASE", parameters.release )
        newContents = newContents.gsub("$RS_BUILDBRANCH", RS_BUILDBRANCH )
        newContents = newContents.gsub("$RAGE_DIR", RAGE_DIR )
        newContents = newContents.gsub("$GUARDIT_PATH", GUARDIT_PATH )
        newContents = newContents.gsub("$PROTECTION_PATH", PROTECTION_PATH )
        newContents = newContents.gsub("$CURR_TIME", CURR_TIME )
        
        newContents = newContents.gsub("\\", "/" )

        return newContents
    end
end

# </commandLineArguments>



log.info("Parsing arguments")
parameters = Parameters.new
parameters.ParseArguments()

if parameters.release == ""
    parameters.PrintHelp()
    log.error("Invalid release [#{parameters.release}] specified")
    exit
end

if parameters.debug == true
    log.SetLevel(Logger::DEBUG)
end

# <print command line arguments for good measure"
log.debug("Verbose     \t#{parameters.verbose}");
log.debug("Debug       \t#{parameters.debug}");
log.debug("Guardscript \t#{parameters.guardscript}");
log.debug("GuardIT Root\t#{parameters.guarditroot}");
log.debug("Release     \t#{parameters.release}");
log.debug("Architecture\t#{parameters.architecture}");
log.debug("Clobber     \t#{parameters.clobber}");

# Now we actually have to do the work of building the guardscript together

# Open up the file
log.info("Opening template guardscript")
templateGs = File.new(parameters.guardscript, "r")
if(templateGs)
else
    log.error("Unable to open path to guardscript '#{parameters.guardscript}'")
end

templateGsContents = templateGs.read

# Now lets fill it in with our tokens
log.info("Generating guardscript")
populatedGsContents = GuardScriptFiller.Populate(templateGsContents,parameters)
# Now that we've got the generated, lets write it out to disk.
# Create a file name so that we can debug it accordingly

generatedGsFileName = CURR_TIME + ".gsml"

log.info("Writing out generated guardscript to disk")

File.open(GENERATED_PATH +  "/" + generatedGsFileName,'w') do |outputFile|
    outputFile.puts populatedGsContents
end

log.info("Running GuardIT")
guardItCommand = GUARDIT_PATH 
if parameters.architecture == "x64"
    guardItCommand = guardItCommand + "/bin64"
else
    guardItCommand = guardItCommand + "/bin"
end

guardItCommand = guardItCommand + "/guardit.exe /pm " + GENERATED_PATH + "/" + generatedGsFileName

if parameters.debug == false
    if parameters.verbose == false
        guardItOutput = `#{guardItCommand}`
    else
        IO.popen guardItCommand do |fd|
          until fd.eof?
            log.info(fd.readline, false)
          end
        end
    end
end

log.info("GuardIT Complete!")
inputFile = /input_file\>(.*)\<\/input_file/.match(populatedGsContents)
outputFile = /output_file\>(.*)\<\/output_file/.match(populatedGsContents)

if parameters.clobber == true
    log.info("Overwriting original input file")
    log.debug("Clobbering input ")
    log.debug("\t" + inputFile[1])
    log.debug("with")
    log.debug("\t" + outputFile[1])
    
    if parameters.debug == false
        FileUtils.cp(outputFile[1],inputFile[1])
    end
end
log.info("Protection Complete!")
