!include LogicLib.nsh

!define sysOSVERSIONINFO '(i, i, i, i, i, &t128) i'
!define sysGetVersionEx 'kernel32::GetVersionExA(i) i'
!define sysVER_PLATFORM_WIN32_NT 2
!define USLABEL "(United States)"

!macro MUI_LANGUAGE_MOD LANGUAGE LANG_NLF
  ;Include a language
  !verbose push
  !verbose ${MUI_VERBOSE}
  !insertmacro MUI_INSERT
  LoadLanguageFile "${LANG_NLF}"				;"${NSISDIR}\Contrib\Language files\${LANGUAGE}.nlf"

  ;Add language to list of languages for selection dialog
  !ifndef MUI_LANGDLL_LANGUAGES
    !define MUI_LANGDLL_LANGUAGES "'${LANGUAGE} ${USLABEL}' '${LANG_${LANGUAGE}}' "
    !define MUI_LANGDLL_LANGUAGES_CP "'${LANGUAGE}' '${LANG_${LANGUAGE}}' '${LANG_${LANGUAGE}_CP}' "
	
  !else
    !ifdef MUI_LANGDLL_LANGUAGES_TEMP
      !undef MUI_LANGDLL_LANGUAGES_TEMP
    !endif
    !define MUI_LANGDLL_LANGUAGES_TEMP "${MUI_LANGDLL_LANGUAGES}"
    !undef MUI_LANGDLL_LANGUAGES

    !ifdef MUI_LANGDLL_LANGUAGES_CP_TEMP
      !undef MUI_LANGDLL_LANGUAGES_CP_TEMP
    !endif
    !define MUI_LANGDLL_LANGUAGES_CP_TEMP "${MUI_LANGDLL_LANGUAGES_CP}"
    !undef MUI_LANGDLL_LANGUAGES_CP

    !define MUI_LANGDLL_LANGUAGES "'${LANGUAGE}' '${LANG_${LANGUAGE}}' ${MUI_LANGDLL_LANGUAGES_TEMP}"
    !define MUI_LANGDLL_LANGUAGES_CP "'${LANGUAGE}' '${LANG_${LANGUAGE}}' '${LANG_${LANGUAGE}_CP}' ${MUI_LANGDLL_LANGUAGES_CP_TEMP}"
  !endif

  !verbose pop
 
!macroend


;--------------------------------
;Language selection

!macro MUI_LANGDLL_DISPLAY_MOD

!verbose push
!verbose ${MUI_VERBOSE}

!insertmacro MUI_LANGDLL_VARIABLES

!insertmacro MUI_DEFAULT MUI_LANGDLL_WINDOWTITLE "Installer Language"
!insertmacro MUI_DEFAULT MUI_LANGDLL_INFO "Please select a language."

!ifdef MUI_LANGDLL_REGISTRY_VARAIBLES

ReadRegStr $mui.LangDLL.RegistryLanguage "${MUI_LANGDLL_REGISTRY_ROOT}" "${MUI_LANGDLL_REGISTRY_KEY}" "${MUI_LANGDLL_REGISTRY_VALUENAME}"

${if} $mui.LangDLL.RegistryLanguage != ""
;Set default langauge to registry language
StrCpy $LANGUAGE $mui.LangDLL.RegistryLanguage
${endif}

!endif

!ifdef NSIS_CONFIG_SILENT_SUPPORT
${unless} ${Silent}
!endif

!ifndef MUI_LANGDLL_ALWAYSSHOW
!ifdef MUI_LANGDLL_REGISTRY_VARAIBLES
${if} $mui.LangDLL.RegistryLanguage == ""
!endif
!endif

;Show langauge selection dialog
!ifdef MUI_LANGDLL_ALLLANGUAGES
LangDLL::LangDialog "${MUI_LANGDLL_WINDOWTITLE}" "${MUI_LANGDLL_INFO}" A ${MUI_LANGDLL_LANGUAGES} ""
!else
LangDLL::LangDialog "${MUI_LANGDLL_WINDOWTITLE}" "${MUI_LANGDLL_INFO}" AC ${MUI_LANGDLL_LANGUAGES_CP} ""
!endif

Pop $LANGUAGE
${if} $LANGUAGE == "cancel"
Abort
${endif}

!ifndef MUI_LANGDLL_ALWAYSSHOW
!ifdef MUI_LANGDLL_REGISTRY_VARAIBLES
${endif}
!endif
!endif


!ifdef NSIS_CONFIG_SILENT_SUPPORT
${endif}
!endif

!verbose pop

!macroend

;Languages
!insertmacro MUI_LANGUAGE_MOD "English" "Global\Language files\English.nlf"         ;first language is the default language
!insertmacro MUI_LANGUAGE_MOD "French" "Global\Language files\French.nlf"
!insertmacro MUI_LANGUAGE_MOD "Italian" "Global\Language files\Italian.nlf"
!insertmacro MUI_LANGUAGE_MOD "German" "Global\Language files\German.nlf"
!insertmacro MUI_LANGUAGE_MOD "Spanish" "Global\Language files\Spanish.nlf"
!insertmacro MUI_LANGUAGE_MOD "Russian" "Global\Language files\Russian.nlf"
!insertmacro MUI_LANGUAGE_MOD "Japanese" "Global\Language files\Japanese.nlf"
!insertmacro MUI_LANGUAGE_MOD "Polish" "Global\Language files\Polish.nlf" 
!insertmacro MUI_LANGUAGE_MOD "PortugueseBR" "Global\Language files\PortugueseBR.nlf" 
!insertmacro MUI_LANGUAGE_MOD "Korean" "Global\Language files\Korean.nlf"

LicenseLangString MUILicense  ${LANG_ENGLISH} "Global\EULA\EN\License.rtf" 
LicenseLangString MUILicense  ${LANG_ITALIAN} "Global\EULA\IT\License.rtf" 
LicenseLangString MUILicense  ${LANG_FRENCH} "Global\EULA\FR\License.rtf" 
LicenseLangString MUILicense  ${LANG_GERMAN} "Global\EULA\GER\License.rtf" 
LicenseLangString MUILicense  ${LANG_SPANISH} "Global\EULA\SPA\License.rtf"
LicenseLangString MUILicense  ${LANG_JAPANESE} "Global\EULA\JPN\License.rtf"
LicenseLangString MUILicense  ${LANG_RUSSIAN} "Global\EULA\RUS\License.rtf"
LicenseLangString MUILicense  ${LANG_POLISH} "Global\EULA\POL\License.rtf"
LicenseLangString MUILicense  ${LANG_PORTUGUESEBR} "Global\EULA\POR\License.rtf"
LicenseLangString MUILicense  ${LANG_KOREAN} "Global\EULA\KOR\License.rtf"

; Set name using the normal interface (Name command)
;LangString Name ${LANG_ENGLISH} "English"
;LangString Name ${LANG_ITALIAN} "Italiano"
;LangString Name ${LANG_FRENCH} "Fran�ais"
;LangString Name ${LANG_GERMAN} "Deutsch"
;LangString Name ${LANG_SPANISH} "Espa�ol"
;LangString Name ${LANG_JAPANESE} "???"
;LangString Name ${LANG_RUSSIAN} "??????"

var AdminErrorRGSC		
var AdminErrPatch  
var AdminErrorDLC
var OSError
var RGSCError
var GTAIVError
var WelcomeHead1
var WelcomeHead2
var WelcomeMsg1
Var WelcomeError
var WelcomeMsg2
var InstallHead
var InstallMsg
var RunError
var InitINSTALL
var RunUpdateErr
var AppErr
var NewerVerErr
var GameNotInstalled

var LangTitle 
var LangSelect
var AbortWarning
var TransferError

var OptHeader1
var OptHeader2
var ModifyHead					
var ModifyText	
var RemoveHead  
var RemoveText  
var ModHeader1
var ModHeader2


Function AddLangSelectEng
    StrCpy $LangTitle  "Installer Language" 
    StrCpy $LangSelect "Please select the language of the installer."
FunctionEnd

Function AddEnglish
    StrCpy $AdminErrorRGSC "$(^Name) requires administrator privileges to install.$\nPlease log in as Administrator and restart this application."
	StrCpy $AdminErrPatch  "$(^Name) updater requires administrator privileges to install.$\nPlease log in as Administrator and restart this application."
	StrCpy $AdminErrorDLC  "$(^Name) download installer requires administrator privileges.$\nPlease log in as Administrator and restart this application."

    StrCpy $OSError     "Your system is incompatible with $(^Name)."
    StrCpy $WelcomeHead1 "Welcome."
    StrCpy $WelcomeHead2 "This wizard will guide you through the installation of $(^Name). You can get help at any time on www.rockstargames.com/support"
	StrCpy $WelcomeMsg1 "Welcome to the $(^Name) update installer."
    StrCpy $WelcomeMsg2 "This will install the latest features for $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Installing Files."
    StrCpy $InstallMsg  "Installing $(^Name) Files."
    StrCpy $RunError    "$(^Name) is currently running.$\n You Must close the application in order to complete this process."
    StrCpy $InitINSTALL  "Initializing $(^Name) Installer."
    StrCpy $WelcomeError "Error: Tying to run $(^Name) Installer."
    StrCpy $AbortWarning "Are you sure you want to quit $(^Name) Setup?"
    StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
    StrCpy $GameNotInstalled "Sorry, Grand Theft Auto V isn't installed on this system. Please reinstall the game and try again."
	
    StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
    StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
FunctionEnd

Function un.AddEnglish
	StrCpy $WelcomeHead1 "Welcome."
    StrCpy $WelcomeHead2 "This wizard will guide you through the installation of $(^Name)."
	StrCpy $RunError     "$(^Name) is currently running.$\n You must close the application in order to complete this process."   
    StrCpy $RunUpdateErr "$(^Name) is running and can not be updated. Please shut down all Rockstar Game Applications."
    StrCpy $AppErr       "All Rockstar Applications must be removed before removing $(^Name)"
	
	StrCpy $OptHeader1	 "Welcome to $(^Name) setup maintenance program."
	StrCpy $OptHeader2	 "This program lets you modify the current installation. Click one of the options below, and the click Next to proceed."
	StrCpy $ModifyHead	 "Modify."					
	StrCpy $ModifyText	 "Select new program features to add or select currently installed features to remove."
	StrCpy $RemoveHead   "Remove."
	StrCpy $RemoveText   "Remove all installed features."
	StrCpy $ModHeader1	 "Select Installation Options for $(^Name)." 
	StrCpy $ModHeader2   "Select the features you want to retain, and deselect the features you want to remove."

FunctionEnd

Function AddLangSelectFre
    StrCpy $LangTitle   "Langue du programme d'installation" 
    StrCpy $LangSelect  "Veuillez s�lectionner la langue du programme d'installation."
FunctionEnd

Function AddFrench
    StrCpy $AdminErrorRGSC "Pour installer le $(^Name), vous devez disposer de privil�ges d'administrateur.$\nVeuillez vous connecter en tant qu'administrateur et red�marrer cette application."
	StrCpy $AdminErrPatch "Pour installer le logiciel de mise � jour de $(^Name), vous devez disposer de privil�ges d'administrateur.$\nVeuillez vous connecter en tant qu'administrateur et red�marrer cette application."
	StrCpy $AdminErrorDLC "Pour utiliser le logiciel d'installation de contenus t�l�chargeables de $(^Name), vous devez disposer de privil�ges d'administrateur.$\nVeuillez vous connecter en tant qu'administrateur et red�marrer cette application."

    StrCpy $OSError     "Votre syst�me est incompatible avec $(^Name)."
    StrCpy $RGSCError   "Erreur : impossible de trouver le Rockstar Games Social Club.$\nAssurez-vous d'avoir bien install� le Rockstar Games Social Club et qu'il fonctionne correctement."
    StrCpy $GTAIVError  "Erreur : impossible de trouver $(^Name). Assurez-vous d'avoir bien install� $(^Name) et qu'il fonctionne correctement."
    StrCpy $WelcomeHead1 "Bienvenue."
    StrCpy $WelcomeHead2 "Vous �tes sur le point d�installer le $(^Name) sur votre ordinateur."
    StrCpy $WelcomeMsg1 "Bienvenue dans le programme d'installation de mises � jour de $(^Name)."
    StrCpy $WelcomeMsg2 "Ce programme va installer les derniers composants de $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Installation des fichiers."
    StrCpy $InstallMsg  "Installation des fichiers de $(^Name)."
    StrCpy $RunError    "$(^Name) est d�j� en cours d'utilisation.$\nVous devez fermer l'application pour que le programme de mise � jour soit pris en compte."
    StrCpy $InitINSTALL   "Initialisation du programme d'installation de $(^Name)."
    StrCpy $WelcomeError "Erreur : tentative d'acc�s au programme d'installation de $(^Name)."
    StrCpy $AbortWarning "�tes-vous s�r de vouloir quitter l'installation de $(^Name) ?"
    StrCpy $TransferError "Echec de la copie de l'un des fichiers sur votre ordinateur.$\nCe fichier est n�cessaire pour le bon fonctionnement de $(^Name).$\nPour afficher l'erreur, veuillez cliquer sur 'Plus d'infos' ou visitez www.rockstargames.com/support pour de plus amples informations."
    StrCpy $GameNotInstalled "Sorry game not installed"
	
    StrCpy $RunUpdateErr "$(^Name) est en cours d'ex�cution. Mise � jour impossible. Veuillez fermer toutes les applications Rockstar Games."
    StrCpy $NewerVerErr  "Une nouvelle version ($0) a �t� install�e pour $(^Name). L�installation va se terminer."
FunctionEnd

Function un.AddFrench
    StrCpy $WelcomeHead1 "Bienvenue."
	StrCpy $WelcomeHead2 "Cet assistant d'installation va vous guider pendant l'installation du produit $(^Name)."
	StrCpy $RunError     "$(^Name) est actuellement lanc�.$\n Vous devez quitter l'application pour terminer ce processus."   
	StrCpy $RunUpdateErr "$(^Name) est lanc� et ne peut pas �tre mis � jour. Veuillez quitter toutes les applications Rockstar Games."
	StrCpy $AppErr       "Toutes les applications Rockstar doivent �tre supprim�es avant de pouvoir supprimer $(^Name)."
		
	StrCpy $OptHeader1	 "Bienvenue dans le programme de maintenance pour $(^Name)."
	StrCpy $OptHeader2	 "Ce programme vous permet de modifier l'installation actuelle. Cliquez sur l'une des options ci-apr�s, puis sur Suivant pour continuer."
	StrCpy $ModifyHead	 "Modifier."					
	StrCpy $ModifyText	 "S�lectionnez de nouvelles fonctionnalit�s � ajouter ou des fonctionnalit�s d�j� install�es � supprimer."
	StrCpy $RemoveHead   "Supprimer."
	StrCpy $RemoveText   "Supprimez toutes les fonctionnalit�s install�es."
	StrCpy $ModHeader1	 "S�lectionnez vos options d'installation pour $(^Name)." 
	StrCpy $ModHeader2   "S�lectionnez les fonctionnalit�s � conserver et d�s�lectionnez les fonctionnalit�s � supprimer."
FunctionEnd

Function AddLangSelectIta
    StrCpy $LangTitle   "Lingua installer" 
    StrCpy $LangSelect  "Seleziona la lingua dell'installer."
FunctionEnd

Function AddItalian
    StrCpy $AdminErrorRGSC "Il Social Club di Rockstar Games necessita dei privilegi di amministratore per essere installato.$\nAccedi come Amministratore e riavvia l'applicazione."
	StrCpy $AdminErrPatch "L'updater di $(^Name) necessita dei privilegi di amministratore per essere installato.$\nAccedi come Amministratore e riavvia l'applicazione."
	StrCpy $AdminErrorDLC "Il programma d'installazione dei download di $(^Name) necessita dei privilegi di amministratore per essere installato.$\nAccedi come Amministratore e riavvia l'applicazione."

    StrCpy $OSError     "Il sistema � incompatibile con $(^Name)."
    StrCpy $RGSCError   "Errore: impossibile rilevare l'applicazione del Social Club di Rockstar Games.$\nAssicurati che l'applicazione del Social Club di Rockstar Games sia installata e funzioni correttamente."
    StrCpy $GTAIVError  "Errore: impossibile rilevare $(^Name). Assicurati che $(^Name) sia installato e funzioni correttamente."
    StrCpy $WelcomeHead1 "Benvenuto."
    StrCpy $WelcomeHead2 "Questo programma installer� $(^Name) nel vostro computer."
    StrCpy $WelcomeMsg1 "Benvenuto all'installazione dell'aggiornamento di $(^Name)."
    StrCpy $WelcomeMsg2 "Questo programma installer� le funzionalit� pi� recenti di $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Installazione file in corso..."
    StrCpy $InstallMsg  "Installazione dei file di $(^Name)."
    StrCpy $RunError    "$(^Name) � in esecuzione.$\n � necessario chiudere l'applicazione per poter completare il processo."
    StrCpy $InitINSTALL   "Inizializzazione del programma di installazione di $(^Name)."
    StrCpy $WelcomeError "Errore nel tentativo di esecuzione dell'installer di $(^Name)."
    StrCpy $AbortWarning "Sei sicuro di voler interrompere l'installazione di $(^Name) ?"
    StrCpy $TransferError "Uno dei file estratti non � stato copiato correttamente sul tuo sistema.$\nQuesto file � necessario per far funzionare $(^Name) correttamente.$\nPer mostrare i dettagli dell'errore, clicca su 'Mostra Dettagli' o visita www.rockstargames.com/support per ulteriori informazioni."
    StrCpy $GameNotInstalled "Sorry game not installed"

    StrCpy $RunUpdateErr "$(^Name) � aperto e non pu� essere aggiornato. Chiudi tutte le applicazioni Rockstar Games."
    StrCpy $NewerVerErr  "Una nuova versione ($0) di $(^Name) � stata installata. Installazione in chiusura."
FunctionEnd

Function un.AddItalian
    StrCpy $WelcomeHead1 "Benvenuto."
	StrCpy $WelcomeHead2 "Questo programma ti guider� nell'installazione di $(^Name)."
	StrCpy $RunError     "$(^Name) � attualmente in esecuzione.$\n Per concludere il processo, devi chiudere l'applicazione."   
	StrCpy $RunUpdateErr "$(^Name) � in esecuzione e non pu� essere aggiornato. Chiudi tutte le applicazioni di Rockstar."
	StrCpy $AppErr       "Per rimuovere $(^Name), occorre rimuovere tutte le applicazioni di Rockstar."
		
	StrCpy $OptHeader1	 "Benvenuto al programma di manutenzione di $(^Name)."
	StrCpy $OptHeader2	 "Questo programma ti permette di modificare lo stato dell'installazione attuale. Per continuare, clicca su una delle opzioni sotto e poi su Avanti."
	StrCpy $ModifyHead	 "Modifica."					
	StrCpy $ModifyText	 "Seleziona le nuove funzioni da aggiungere o seleziona quelle gi� installate da rimuovere."
	StrCpy $RemoveHead   "Rimuovi."
	StrCpy $RemoveText   "Rimuovi tutte le funzioni installate."
	StrCpy $ModHeader1	 "Seleziona le opzioni di installazione per $(^Name)." 
	StrCpy $ModHeader2   "Seleziona le funzioni che vuoi mantenere e deseleziona quelle che vuoi rimuovere."
FunctionEnd

Function AddLangSelectSpa
    StrCpy $LangTitle   "Idioma del instalador" 
    StrCpy $LangSelect  "Selecciona el idioma del instalador."
FunctionEnd

Function AddSpanish
    StrCpy $AdminErrorRGSC "Para instalar el Social Club de Rockstar Games se requieren privilegios de administrador.$\nInicia sesi�n como administrador y reinicia esta aplicaci�n."
	StrCpy $AdminErrPatch "Para instalar el programa de actualizaci�n de $(^Name) se requieren privilegios de administrador.$\nInicia sesi�n como administrador y reinicia esta aplicaci�n."
	StrCpy $AdminErrorDLC "El instalador de descargas de $(^Name) requiere privilegios de administrador.$\nInicia sesi�n como administrador y reinicia esta aplicaci�n."

    StrCpy $OSError     "Tu sistema no es compatible con $(^Name)."
    StrCpy $RGSCError   "Error: no se encuentra Rockstar Games Social Club.$\nAseg�rate de que Rockstar Games Social Club est� instalado y en funcionamiento."
    StrCpy $GTAIVError  "Error: no se encuentra $(^Name). Aseg�rate de que $(^Name) est� instalado y en funcionamiento."
    StrCpy $WelcomeHead1 "Bienvenido."
    StrCpy $WelcomeHead2 "Este programa instalar� $(^Name) en su ordenador."
    StrCpy $WelcomeMsg1 "Bienvenido al instalador de la actualizaci�n de $(^Name)."
    StrCpy $WelcomeMsg2 "Este programa instalar� las funciones m�s recientes de $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Instalando archivos."
    StrCpy $InstallMsg  "Instalando los archivos de $(^Name)."
    StrCpy $RunError    "$(^Name) est� ejecut�ndose.$\nPara poder completar este procedimiento, debes cerrar la aplicaci�n."
    StrCpy $InitINSTALL   "Inicializando el instalador de $(^Name)."
    StrCpy $WelcomeError "Error al intentar ejecutar el instalador de $(^Name)."
    StrCpy $AbortWarning "�Est� seguro de que desea salir de la instalaci�n de $(^Name)?"
    StrCpy $TransferError "Error al copiar uno de los archivos extra�dos a tu sistema.$\nSe requiere este archivo para ejecutar $(^Name) correctamente.$\nPara mostrar el error, haz clic en 'Ver detalles' o visita www.rockstargames.com/support para m�s informaci�n."
    StrCpy $GameNotInstalled "Sorry game not installed span"

    StrCpy $RunUpdateErr "$(^Name) est� abierto y no se puede actualizar. Cierra todas las aplicaciones de Rockstar Games."
	StrCpy $NewerVerErr  "Se ha instalado una nueva versi�n ($0) de $(^Name). Saliendo de la instalaci�n."
FunctionEnd

Function un.AddSpanish
    StrCpy $WelcomeHead1 "Bienvenido."
	StrCpy $WelcomeHead2 "Este asistente te guiar� durante la instalaci�n de $(^Name)."
	StrCpy $RunError     "$(^Name) se est� ejecutando.$\n Debes cerrar la aplicaci�n para completar el proceso."   
	StrCpy $RunUpdateErr "$(^Name) se est� ejecutando y no puede actualizarse. Cierra todas las aplicaciones de juego de Rockstar."
	StrCpy $AppErr       "Todas las aplicaciones de Rockstar deben eliminarse antes de quitar $(^Name)."
		
	StrCpy $OptHeader1	 "Bienvenido al programa de mantenimiento de $(^Name)."
	StrCpy $OptHeader2	 "Este programa te permite modificar la instalaci�n actual. Haz clic en una de las opciones de debajo y luego haz clic en Siguiente para continuar."
	StrCpy $ModifyHead	 "Modificar."					
	StrCpy $ModifyText	 "Elige caracter�sticas nuevas para a�ad�rselas al programa o caracter�sticas instaladas actualmente para eliminarlas."
	StrCpy $RemoveHead   "Borrar."
	StrCpy $RemoveText   "Borra todas las caracter�sticas instaladas."
	StrCpy $ModHeader1	 "Selecciona las opciones de instalaci�n de $(^Name)." 
	StrCpy $ModHeader2   "Selecciona las caracter�sticas que quieras conservar y desactiva las que quieras quitar."
FunctionEnd

Function AddLangSelectGer
    StrCpy $LangTitle   "Installationssprache" 
    StrCpy $LangSelect  "Bitte w�hlen Sie die Installationssprache."
FunctionEnd

Function AddGerman
    StrCpy $AdminErrorRGSC "Rockstar Games Social Club ben�tigt Administratorrechte f�r die Installation.$\nBitte melden Sie sich als Administrator an und starten Sie diese Anwendung neu."
	StrCpy $AdminErrPatch "$(^Name) Updater ben�tigt Administratorrechte f�r die Installation.$\nBitte melden Sie sich als Administrator an und starten Sie diese Anwendung neu."
	StrCpy $AdminErrorDLC "$(^Name) Download-Installationsprogramm  ben�tigt Administratorrechte.$\nBitte melden Sie sich als Administrator an und starten Sie diese Anwendung neu."

    StrCpy $OSError     "Ihr System ist nicht kompatibel mit $(^Name)."
    StrCpy $RGSCError   "Fehler: Rockstar Games Social Club wurde nicht gefunden.$\nBitte stellen Sie sicher, dass Rockstar Games Social Club installiert ist und fehlerfrei l�uft."
    StrCpy $GTAIVError  "Fehler: $(^Name) wurde nicht gefunden. Bitte stellen Sie sicher, dass $(^Name) installiert ist und fehlerfrei l�uft."
    StrCpy $WelcomeHead1 "Willkommen."
    StrCpy $WelcomeHead2 "Dieser Assistent wird Sie durch die Installation von $(^Name) begleiten."
    StrCpy $WelcomeMsg1 "Willkommen zum $(^Name) Update Installer."
    StrCpy $WelcomeMsg2 "Die neuesten Features f�r $(^Name) werden installiert.$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Installiere Dateien."
    StrCpy $InstallMsg  "Installiere $(^Name) Dateien."
    StrCpy $RunError    "$(^Name) ist ge�ffnet.$\n Sie m�ssen die Anwendung schlie�en, um diesen Vorgang abzuschlie�en."
    StrCpy $InitINSTALL   "Initialisiere $(^Name) Installer."
    StrCpy $WelcomeError "Fehler: Versuche $(^Name) Installer zu starten."
    StrCpy $AbortWarning "Sind Sie sicher, dass Sie die Installation von $(^Name) abbrechen wollen?"
    StrCpy $TransferError "Eine der extrahierten Dateien konnte nicht auf ihr System kopiert werden.$\nDiese Datei wird ben�tigt um $(^Name) starten zu k�nnen.$\nUm sich den Fehler anzeigen zu lassen, klicken sie bitte auf Details anzeigen oder besuchen sie www.rockstargames.com/support f�r weitere Informationen und Anleitungen."
	StrCpy $GameNotInstalled "Sorry game not installed German" 

    StrCpy $RunUpdateErr "$(^Name) wird gerade ausgef�hrt und kann nicht aktualisiert werden. Bitte s�mtliche Rockstar Games-Anwendungen beenden."
	StrCpy $NewerVerErr "Es ist bereits eine neuere Version ($0) von $(^Name) installiert. Die Installation wird jetzt beendet."
FunctionEnd

Function un.AddGerman
    StrCpy $WelcomeHead1 "Willkommen."
	StrCpy $WelcomeHead2 "Dieser Assistent f�hrt Sie durch die Installation von $(^Name)."
	StrCpy $RunError     "$(^Name) l�uft gerade.$\n Sie m�ssen das Programm beenden, um den Vorgang abzuschlie�en."   
	StrCpy $RunUpdateErr "$(^Name) l�uft und kann nicht aktualisiert werden. Bitte s�mtliche Rockstar-Programme beenden."
	StrCpy $AppErr       "S�mtliche Rockstar-Programme m�ssen vor dem Deinstallieren von $(^Name) entfernt werden."
		
	StrCpy $OptHeader1	 "Willkommen zum $(^Name) Setup-Wartungs-Programm."
	StrCpy $OptHeader2	 "Mit diesem Programm k�nnen Sie die aktuelle Installation modifizieren. W�hlen Sie eine der unten aufgef�hrten Optionen und klicken Sie 'Weiter', um fortzufahren."
	StrCpy $ModifyHead	 "Modifizieren."					
	StrCpy $ModifyText	 "F�gen Sie neue Programm-Features hinzu oder entfernen Sie derzeit installierte Features."
	StrCpy $RemoveHead   "Entfernen."
	StrCpy $RemoveText   "Entfernen Sie s�mtliche installierten Features."
	StrCpy $ModHeader1	 "Installationsoptionen f�r $(^Name) w�hlen." 
	StrCpy $ModHeader2   "W�hlen Sie die Features, die Sie behalten m�chten und w�hlen Sie die Features ab, die Sie entfernen m�chten."
FunctionEnd

Function AddLangSelectRus
    StrCpy $LangTitle   "???? ?????????" 
    StrCpy $LangSelect  "??????????, ???????? ???? ?????????."
FunctionEnd

Function AddRussian
    StrCpy $LANGUAGE 1049
    StrCpy $AdminErrorRGSC "??? ????????? Rockstar Games Social Club ?????????? ????? ??????????????.$\n??????? ??? ????????????? ??????? ? ????? ????????? ??? ??????????."
	StrCpy $AdminErrPatch "????????? ?????????? $(^Name) ?????????? ????? ??????????????.$\n??????? ??? ????????????? ??????? ? ????? ????????? ??? ??????????."
	StrCpy $AdminErrorDLC "????????? ????????? ?????????? $(^Name) ?????????? ????? ??????????????.$\n??????? ??? ????????????? ??????? ? ????? ????????? ??? ??????????."

    StrCpy $OSError     "???? ??????? ?? ?????????????? ????? $(^Name)."
    StrCpy $RGSCError   "??????: ?? ?????????? ?????????? Rockstar Games Social Club.$\n??????????, ????????? ??? ?????????? Rockstar Games Social Club ??????????? ? ???????? ??? ?????."
    StrCpy $GTAIVError  "??????: ?? ??????? ???? $(^Name). ??????????, ?????????, ??? ???? $(^Name) ??????????? ? ???????? ??? ?????."
    StrCpy $WelcomeHead1 "????? ??????????."
    StrCpy $WelcomeHead2 "???? ?????? ????????? ???????? ??? ????? ????????? $(^Name)."
    StrCpy $WelcomeMsg1 "??? ???????????? ????????? ????????? ?????????? ??? $(^Name)."
    StrCpy $WelcomeMsg2 "??? ????????? ????????? ????????? ??????????? ??? $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "????????? ??????."
    StrCpy $InstallMsg  "??????????? ????????? ?????? $(^Name)."
    StrCpy $RunError    "? ??? ???????? ???? $(^Name).$\n??? ?????????? ????????? ??? ?????????? ??????? ??????? ??????????."
    StrCpy $InitINSTALL   "?????????? ? ????????? $(^Name)."
    StrCpy $WelcomeError "??????: ?? ??????? ????????? ????? ????????? $(^Name)."
    StrCpy $AbortWarning "?? ????????????? ?????? ???????? ????????? $(^Name)?"
    StrCpy $TransferError "???? ?? ??????????? ?????? ?? ???? ???? ?????????? ? ???? ???????.$\n???? ???? ????? ??? ???? ????? ????????? ????????? $(^Name).$\n??????????, ???????? ?? ?????? ?????? ??? ??????? ?? www.rockstargames.com/support ??? ?????????? ??????????."
    StrCpy $GameNotInstalled "Sorry game not installed German" 
    StrCpy $RunUpdateErr "$(^Name) ???????? ? ?? ????? ???? ????????. ??????????, ???????? ??? ?????????? Rockstar Games."
	StrCpy $NewerVerErr  "??????? ????? ????? ?????? ($0) ???? $(^Name). ????? ?? ?????????."
FunctionEnd 

Function un.AddRussian
    StrCpy $WelcomeHead1 	"????? ??????????."
	StrCpy $WelcomeHead2 	"??? ????????? ??????? ??? ?????????? $(^Name)."
	StrCpy $RunError     	"$(^Name) ? ?????? ?????? ????????. $\n ?? ?????? ??????? ??, ????? ????????? ???????."
	StrCpy $RunUpdateErr 	"$(^Name) ? ?????? ?????? ????????, ? ?? ?????? ????????. ??????????, ???????? ??? ?????????? Rockstar."
	StrCpy $AppErr       	"????? ????????? $(^Name) ?????????? ??????? ??? ?????????? Rockstar"
		
	StrCpy $OptHeader1		"????? ?????????? ? ????????? ????????? $(^Name)."
	StrCpy $OptHeader2		"??? ????????? ????????? ??? ???????? ? ????????????? ???????????. ???????? ?????? ????? ? ??????? ?????? ''?????''."
	StrCpy $ModifyHead		"????????."
	StrCpy $ModifyText		"???????? ??????????, ??????? ?????? ???????? ??? ???????."
	StrCpy $RemoveHead   	"???????."
	StrCpy $RemoveText   	"??????? ??? ????????????? ??????????."
	StrCpy $ModHeader1		"???????? ???????? ?????????? ????????? $(^Name)."
	StrCpy $ModHeader2   	"???????? ??????????, ??????? ?????? ????????, ? ??????? ??????? ? ???, ??????? ?????? ???????."
FunctionEnd 


Function AddLangSelectBRZ
    StrCpy $LangTitle  "Idioma do Instalador" 
    StrCpy $LangSelect "Por favor, selecione o idioma do instalador."
FunctionEnd

Function AddPortuguese
    StrCpy $AdminErrorRGSC "Rockstar Games Social Club requer privil�gios de administrador para ser instalado.$\nPor favor fa�a o log-in como Administrador e reinicie este aplicativo."
	StrCpy $AdminErrPatch "O atualizador do $(^Name) requer privil�gios de administrador para ser instaladol.$\nPor favor fa�a o log-in como Administrador e reinicie este aplicativo."
	StrCpy $AdminErrorDLC "O instalador do $(^Name) requer privil�gios de administrador.$\nPor favor fa�a o log-in como Administrador e reinicie este aplicativo."

    StrCpy $OSError     "Seu sistema � incompat�vel com $(^Name)."
    StrCpy $WelcomeHead1 "Bem-vindo."
    StrCpy $WelcomeHead2 "Este assistente ir� gui�-lo na instala��o de $(^Name)."
    StrCpy $WelcomeMsg1 "Bem-vindo ao instalador de atualiza��es de $(^Name)."
    StrCpy $WelcomeMsg2 "Este ir� instalar os recursos mais recentes de $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Instalando os arquivos."
    StrCpy $InstallMsg  "Instalando os arquivos de $(^Name)."
    StrCpy $RunError    "$(^Name) est� sendo executado.$\n � necess�rio encerrar o aplicativo para completar este processo."
    StrCpy $InitINSTALL  "Inicializando o instalador de $(^Name)."
    StrCpy $WelcomeError "Erro: tentando executar o instalador de $(^Name)."
    StrCpy $AbortWarning "Tem certeza de que deseja sair da configura��o de $(^Name)?"
    StrCpy $TransferError "Um dos arquivos extra�dos falhou ao ser copiado em seu sistema.$\nEste arquivo � necess�rio para executar $(^Name) corretamente.$\nPara ver o erro, pressione Exibir Informa��es ou$\nacesse www.rockstargames.com/support para mais instru��es."
    StrCpy $GameNotInstalled "Sorry game not installed German" 
    StrCpy $RunUpdateErr "$(^Name) est� em andamento e n�o pode ser atualizado. Por favor, encerre todos os Aplicativos de Jogos da Rockstar."
    StrCpy $NewerVerErr  "A nova vers�o $0 para $(^Name) foi instalada. Encerrando a instala��o."
FunctionEnd

Function un.AddPortuguese
	StrCpy $WelcomeHead1	"Bem-vindo."
    StrCpy $WelcomeHead2	"Este assistente ir� gui�-lo na instala��o de $(^Name)."
    StrCpy $RunError		"$(^Name) est� sendo executado.$\n � necess�rio encerrar o aplicativo para completar este processo."   
    StrCpy $RunUpdateErr	"$(^Name) est� em andamento e n�o pode ser atualizado. Por favor, encerre todos os Aplicativos de Jogos da Rockstar."
    StrCpy $AppErr			"Todos os Aplicativos da Rockstar devem ser removidos antes de remover $(^Name)"
		
	StrCpy $OptHeader1	 	"Bem-vindo ao programa de manuten��o da configura��o do $(^Name)."
	StrCpy $OptHeader2	 	"Este programa permite que voc� modifique a instala��o atual. Clique em uma das op��es abaixo e clique em Avan�ar para continuar."
	StrCpy $ModifyHead	 	"Modificar."					
	StrCpy $ModifyText	 	"Selecione novos recursos do programa para adicionar ou selecione recursos atualmente instalados para remover."
	StrCpy $RemoveHead   	"Remover."
	StrCpy $RemoveText   	"Remova todos os recursos instalados."
	StrCpy $ModHeader1	 	"Selecione as op��es de instala��o do $(^Name)." 
	StrCpy $ModHeader2   	"Marque os recursos que deseja manter, e desmarque os recursos que deseja remover."
FunctionEnd 

Function AddLangSelectPol
    StrCpy $LangTitle  "Jezyk programu instalacyjnego" 
    StrCpy $LangSelect "Prosze wybrac jezyk programu instalacyjnego."
FunctionEnd

Function AddPolish
    StrCpy $AdminErrorRGSC "Rockstar Games Social Club wymaga do instalacji przywilej�w administratora.$\nProsze sie zalogowac jako administrator i ponownie uruchomic te aplikacje."
	StrCpy $AdminErrPatch "Aplikacja aktualizujaca gry $(^Name) wymaga do instalacji przywilej�w administratora.$\nProsze sie zalogowac jako administrator i ponownie uruchomic te aplikacje."
	StrCpy $AdminErrorDLC "Instalator gry $(^Name) wymaga przywilej�w administratora.$\nProsze sie zalogowac jako administrator i ponownie uruchomic te aplikacje."

    StrCpy $OSError     "Tw�j system jest niekompatybilny z gra $(^Name)."
    StrCpy $WelcomeHead1 "Witaj."
    StrCpy $WelcomeHead2 "Ten program przeprowadzi cie przez proces instalacji gry $(^Name)."
    StrCpy $WelcomeMsg1 "Witaj w programie instalacyjnym aktualizacji gry $(^Name)."
    StrCpy $WelcomeMsg2 "Program zainstaluje najnowsze funkcje gry $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Instalowanie plik�w."
    StrCpy $InstallMsg  "Instalowanie plik�w gry $(^Name)."
    StrCpy $RunError    "Gra $(^Name) jest uruchomiona.$\n Musisz zamknac aplikacje, zeby dokonczyc trwajacy proces."
    StrCpy $InitINSTALL  "Inicjalizacja programu instalacyjnego gry $(^Name)."
    StrCpy $WelcomeError "Blad: Pr�ba uruchomienia programu instalacyjnego $(^Name)."
    StrCpy $AbortWarning "Czy na pewno chcesz opuscic program instalacyjny gry $(^Name)?"
    StrCpy $TransferError "Jeden z rozpakowanych plik�w nie zostal przekopiowany na tw�j system.$\nPlik ten jest wymagany do prawidlowego dzialania gry $(^Name).$\nZeby zapoznac sie ze szczeg�lami tego bledu, kliknij przycisk szczeg�l�w lub$\nodwiedz strone www.rockstargames.com/support."
    StrCpy $GameNotInstalled "Sorry game not installed German" 
    StrCpy $RunUpdateErr "Gra $(^Name) jest uruchomiona i nie moze zostac zaktualizowana. Zamknij wszystkie aplikacje Rockstar Games."
    StrCpy $NewerVerErr  "Zainstalowano juz nowsza wersje gry $(^Name), $0. Opuszczanie programu instalacyjnego."
FunctionEnd

Function un.AddPolish
	StrCpy $WelcomeHead1 "Witaj."
    StrCpy $WelcomeHead2 "Ten program przeprowadzi cie przez proces instalacji gry $(^Name)."
    StrCpy $RunError    "Gra $(^Name) jest uruchomiona.$\n Musisz zamknac aplikacje, zeby dokonczyc trwajacy proces."   
    StrCpy $RunUpdateErr "Gra $(^Name) jest uruchomiona i nie moze zostac zaktualizowana. Zamknij wszystkie aplikacje Rockstar Games."
    StrCpy $AppErr       "Wszystkie aplikacje Rockstar musza zostac usuniete, zanim mozliwe bedzie usuniecie gry $(^Name)."
		
	StrCpy $OptHeader1	"Witamy w programie instalacyjnym gry $(^Name)."
	StrCpy $OptHeader2	"Program umozliwia ci modyfikacje aktualnej instalacji. Wybierz opcje ponizej i kliknij 'Dalej', zeby kontynuowac."
	StrCpy $ModifyHead	"Zmien."
	StrCpy $ModifyText	"Wybierz nowe funkcje programu do dodania lub juz zainstalowane do usuniecia."
	StrCpy $RemoveHead	"Usun."
	StrCpy $RemoveText	"Usun wszystkie zainstalowane funkcje."
	StrCpy $ModHeader1	"Wybierz opcje instalacji gry $(^Name)."
	StrCpy $ModHeader2	"Wybierz funkcje, kt�re chcesz zachowac, i odznacz te, kt�re chcesz usunac."
FunctionEnd 

Function AddLangSelectKor
	StrCpy $LangTitle "?? ??? ??" 
    StrCpy $LangSelect "?? ????? ??? ??? ?????."
FunctionEnd

Function AddKorean
    StrCpy $AdminErrorRGSC "$(^Name)?(?) ????? ??? ??? ?????.$\n???? ???? ?? ? ????? ?? ??????."
	StrCpy $AdminErrPatch "$(^Name) ???? ???? ????? ??? ??? ?????.$\n???? ???? ?? ? ????? ?? ??????."
    StrCpy $AdminErrorDLC "$(^Name) ???? ???? ??? ??? ?????.$\n???? ???? ?? ? ????? ?? ??????."

    StrCpy $OSError "???? ???? $(^Name)?(?) ???? ????."
    StrCpy $WelcomeHead1 "?????."
    StrCpy $WelcomeHead2 "? ???? $(^Name) ?? ??? ?????. "
	StrCpy $WelcomeMsg1 "$(^Name) ???? ?? ???? ?? ? ?????."
    StrCpy $WelcomeMsg2 "$(^Name)? ?? ??? ?????.$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "??? ???? ????."
    StrCpy $InstallMsg "$(^Name) ??? ???? ????."
    StrCpy $RunError "$(^Name)?(?) ?? ?? ????.$\n ? ??? ???? ????? ??????."
    StrCpy $InitINSTALL "$(^Name) ?? ???? ????? ????."
    StrCpy $WelcomeError "??: $(^Name) ?? ???? ????? ?? ????."
    StrCpy $AbortWarning "$(^Name) ??? ?????????"
    StrCpy $TransferError "??? ?? ? ??? ???? ???? ???? ?????.$\n$(^Name)?(?) ???? ????? ? ??? ?????.$\n?? ??? ??? '??? ?? ??'? ?????, $\n??? ??? www.rockstargames.com/support? ??????."
    StrCpy $GameNotInstalled "Sorry game not installed German" 
    StrCpy $RunUpdateErr "$(^Name)?(?) ?? ?? ??? ????? ? ????. ?? Rockstar ?? ????? ??????."
    StrCpy $NewerVerErr "$(^Name)? ? ?? $0?(?) ???????. ??? ????."
FunctionEnd

Function un.AddKorean
	StrCpy $WelcomeHead1 "?????."
    StrCpy $WelcomeHead2 "? ???? $(^Name) ?? ??? ?????."
	StrCpy $RunError "$(^Name)?(?) ?? ?? ????.$\n ? ??? ???? ????? ??????."   
    StrCpy $RunUpdateErr "$(^Name)?(?) ?? ?? ??? ????? ? ????. ?? Rockstar ?? ????? ??????."
    StrCpy $AppErr "$(^Name)?(?) ????? ?? ?? Rockstar ????? ???? ???."
	
	StrCpy $OptHeader1 "$(^Name) ?? ?? ?? ????? ?? ?? ?????."
	StrCpy $OptHeader2 "? ????? ?? ??? ?????. ?? ?? ? ??? ???? '??'? ???? ??????."
	StrCpy $ModifyHead "??"					
	StrCpy $ModifyText "? ???? ??? ???? ????? ?? ???? ?? ??? ?????."
	StrCpy $RemoveHead "??"
	StrCpy $RemoveText "???? ?? ?? ??? ?????."
	StrCpy $ModHeader1 "$(^Name)? ?? ??? ?????." 
	StrCpy $ModHeader2 "??? ??? ???? ??? ??? ??? ??????."
FunctionEnd

Function AddLangSelectJap
	StrCpy $LangTitle "?????????" 
    StrCpy $LangSelect "???????????????????"
FunctionEnd

Function AddJapanese
	StrCpy $AdminErrorRGSC "$(^Name)????????????????????$\n??????????????????????????????"
	StrCpy $AdminErrPatch "$(^Name)????????????????????????????$\n??????????????????????????????"
	StrCpy $AdminErrorDLC "$(^Name)???????????????????????????$\n??????????????????????????????"

    StrCpy $OSError "?????????$(^Name)??????????"
    StrCpy $WelcomeHead1 "????"
    StrCpy $WelcomeHead2 "????????$(^Name)????????????????"
    StrCpy $WelcomeMsg1 "$(^Name)???????????????????"
    StrCpy $WelcomeMsg2 "????$(^Name)????????????????$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "????????????"
    StrCpy $InstallMsg "$(^Name)?????????????"
    StrCpy $RunError "???$(^Name)???????$\n????????????????????????????????"
    StrCpy $InitINSTALL "$(^Name)?????????????"
    StrCpy $WelcomeError "???:$(^Name)?????????????"
    StrCpy $AbortWarning "$(^Name)???????????????"
    StrCpy $TransferError "??????????????????????????$\n???????$(^Name)????????????????$\n???????????????????????????$\nwww.rockstargames.com/support????????????????"
    
    StrCpy $RunUpdateErr "$(^Name)??????????????????????????�?????????????????????????"
    StrCpy $NewerVerErr "??????????$0??????????????$(^Name)?????????????"

FunctionEnd

Function un.AddJapanese
    StrCpy $WelcomeHead1 "????"
    StrCpy $WelcomeHead2 "????????$(^Name)????????????????"
    StrCpy $RunError "???$(^Name)???????$\n????????????????????????????????"   
    StrCpy $RunUpdateErr "$(^Name)??????????????????????????�?????????????????????????"
    StrCpy $AppErr "??????�???????????????????????$(^Name)?????????"
	
    StrCpy $OptHeader1 "$(^Name)???????????????????"
    StrCpy $OptHeader2 "??????????????????????????????????????????1????????????????????"
    StrCpy $ModifyHead "??"					
    StrCpy $ModifyText "??????????????????????????????????????????"
    StrCpy $RemoveHead "??"
    StrCpy $RemoveText "???????????????????"
    StrCpy $ModHeader1 "$(^Name)?????????????????????" 
    StrCpy $ModHeader2 "?????????????????????????????"
FunctionEnd

!insertmacro LANGFILE "English" "English"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_ENGLISH} "License Agreement"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_ENGLISH} "Please review the license terms before installing $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_ENGLISH} "If you accept the terms of the agreement, click the check box below. You must accept the agreement to install $(^Name). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_ENGLISH} "Press Page Down to see the rest of the agreement."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_ENGLISH} "Installing"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_ENGLISH} "Please wait while $(^Name) is being installed."
LangString MUI_TEXT_FINISH_TITLE ${LANG_ENGLISH} "Install Complete"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_ENGLISH} "Setup was completed successfully. Click Close to finish."
LangString MUI_TEXT_ABORT_TITLE ${LANG_ENGLISH} "The uninstall was aborted"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_ENGLISH} "Setup was not completed successfully. Click Close to finish."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_ENGLISH} "Uninstalling"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_ENGLISH} "Please wait while $(^Name) is being uninstalled."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_ENGLISH} "Uninstallation was completed successfully. Click Close to finish."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_ENGLISH} "Uninstallation Complete"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_ENGLISH} "Uninstallation Aborted"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_ENGLISH} "Setup was not completed successfully. Click Close to finish."


!insertmacro LANGFILE "French" "French"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_FRENCH} "Licence utilisateur"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_FRENCH} "Veuillez examiner les termes de la licence avant d'installer $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_FRENCH} "Si vous acceptez les conditions de la licence utilisateur, cochez la case ci-dessous. Vous devez accepter la licence utilisateur afin d'installer $(^Name)."
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_FRENCH} "Appuyez sur Page Suivante pour lire le reste de la licence utilisateur."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_FRENCH} "Installation en cours"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_FRENCH} "Veuillez patienter pendant que $(^Name) est en train d'�tre install�."
LangString MUI_TEXT_FINISH_TITLE ${LANG_FRENCH} "Installation termin�e"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_FRENCH} "L'installation s'est termin�e avec succ�s."
LangString MUI_TEXT_ABORT_TITLE ${LANG_FRENCH} "Installation interrompue"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_FRENCH} "L'installation n'a pas �t� termin�e."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_FRENCH} "D�sinstallation"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_FRENCH} "Veuillez patienter pendant la d�sinstallation de $(^Name)."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_FRENCH} "D�sinstallation r�ussie. Appuyez sur Fermer pour finir."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_FRENCH} "La d�sinstallation est termin�e."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_FRENCH} "La d�sinstallation a �t� annul�e."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_FRENCH} "L'installation a �chou�. Appuyez sur Fermer pour finir."

!insertmacro LANGFILE "Italian" "Italian"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_ITALIAN} "Licenza d'uso"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_ITALIAN} "Prego leggere le condizioni della licenza d'uso prima di installare $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_ITALIAN} "Se si accettano i termini della licenza d'uso, selezionare la casella sottostante. � necessario accettare i termini della licenza d'uso per installare $(^Name). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_ITALIAN} "Premere PAG GIU per vedere il resto della licenza d'uso."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_ITALIAN} "Installazione in corso"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_ITALIAN} "Prego attendere mentre $(^Name)  viene installato."
LangString MUI_TEXT_FINISH_TITLE ${LANG_ITALIAN} "Installazione completata"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_ITALIAN} "L'installazione � stata completata con successo."
LangString MUI_TEXT_ABORT_TITLE ${LANG_ITALIAN} "Installazione interrotta"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_ITALIAN} "L'installazione non � stata completata correttamente."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_ITALIAN} "Disinstallazione in corso..."
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_ITALIAN} "Attendi mentre $(^Name) viene disinstallato."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_ITALIAN} "La disinstallazione � stata completata con successo. Clicca 'Chiudi' per uscire."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_ITALIAN} "La disinstallazione � stata completata."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_ITALIAN} "La disinstallazione � stata interrotta."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_ITALIAN} "L'installazione non � stata completata. Clicca 'Chiudi' per uscire."

!insertmacro LANGFILE "German" "German"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_GERMAN} "Lizenzabkommen"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_GERMAN} "Bitte lesen Sie die Lizenzbedingungen durch, bevor Sie mit der Installation fortfahren."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_GERMAN} "Falls Sie alle Bedingungen des Abkommens akzeptieren, aktivieren Sie das K�stchen. Sie m�ssen die Lizenzvereinbarungen anerkennen, um $(^Name) installieren zu k�nnen. $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_GERMAN} "Dr�cken Sie die Bild-nach-unten Taste, um den Rest des Abkommens zu sehen."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_GERMAN} "Installiere..."
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_GERMAN} "Bitte warten Sie, w�hrend $(^Name) installiert wird."
LangString MUI_TEXT_FINISH_TITLE ${LANG_GERMAN} "Die Installation ist vollst�ndig"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_GERMAN} "Die Installation wurde erfolgreich abgeschlossen."
LangString MUI_TEXT_ABORT_TITLE ${LANG_GERMAN} "Abbruch der Installation"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_GERMAN} "Die Installation wurde nicht vollst�ndig abgeschlossen."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_GERMAN} "Deinstallieren..."
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_GERMAN} "Bitte warten, w�hrend $(^Name) deinstalliert wird."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_GERMAN} "Deinstallation erfolgreich abgeschlossen. Zum Beenden 'Schlie�en' klicken."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_GERMAN} "Deinstallation erfolgreich."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_GERMAN} "Deinstallation abgebrochen."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_GERMAN} "Installation nicht erfolgreich. Zum Beenden 'Schlie�en' klicken."
  
!insertmacro LANGFILE "Spanish" "Spanish"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_SPANISH} "Acuerdo de licencia"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_SPANISH} "Por favor revise los t�rminos de la licencia antes de instalar $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_SPANISH} "Si acepta los t�rminos del acuerdo, marque abajo la casilla. Debe aceptar los t�rminos para instalar $(^Name). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_SPANISH} "Presione Avanzar P�gina para ver el resto del acuerdo."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_SPANISH} "Instalando"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_SPANISH} "Por favor espere mientras $(^Name) se instala."
LangString MUI_TEXT_FINISH_TITLE ${LANG_SPANISH} "Instalaci�n Completada"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_SPANISH} "La instalaci�n se ha completado correctamente."
LangString MUI_TEXT_ABORT_TITLE ${LANG_SPANISH} "Instalaci�n Anulada"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_SPANISH} "La instalaci�n no se complet� correctamente."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_SPANISH} "Desinstalando"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_SPANISH} "Espera mientras se desinstala $(^Name)."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_SPANISH} "La desinstalaci�n se ha completado con �xito."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_SPANISH} "La desinstalaci�n se ha completado."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_SPANISH} "La desinstalaci�n ha sido abortada."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_SPANISH} "La instalaci�n no ha sido completada con �xito. Haz clic en 'Cerrar' para finalizar."
    
!insertmacro LANGFILE "Russian" "Russian"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_RUSSIAN} "???????????? ??????????"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_RUSSIAN} "????? ?????????? $(^Name) ???????????? ? ???????????? ???????????."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_RUSSIAN} "???? ?? ?????????? ??????? ??????????, ?????????? ?????? ????. $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_RUSSIAN} "??????? Page Down, ????? ??????????? ?????????? ????? ??????????."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_RUSSIAN} "??????????? ??????"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_RUSSIAN} "??????????? ????????? $(^Name). ??????????, ?????????."
LangString MUI_TEXT_FINISH_TITLE ${LANG_RUSSIAN} "????????? ?????????"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_RUSSIAN} "????????? ?????????. ??? ?????????? ???????? ?? ?????? ???????."
LangString MUI_TEXT_ABORT_TITLE ${LANG_RUSSIAN} "????????? ????????"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_RUSSIAN} "????????? ?? ???? ??????????? ? ?????? ????. ??? ?????????? ???????? ?? ?????? ???????."
LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_RUSSIAN} "????????"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_RUSSIAN} "?????????, ???? ???????????? ???????? $(^Name)."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_RUSSIAN} "???????? ???? ????????? ???????. ??????? ?????? ???????, ????? ?????????."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_RUSSIAN} "???????? ?????????."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_RUSSIAN} "???????? ???? ????????."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_RUSSIAN} "????????? ?? ???? ????????? ???????. ??????? ?????? '???????', ????? ?????????."


!insertmacro LANGFILE "Polish" "Polish"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_POLISH} "Umowa licencyjna"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_POLISH} "Przed instalacja programu $(^NameDA) zapoznaj sie z warunkami licencji."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_POLISH} "Jezeli akceptujesz warunki umowy, zaznacz pole wyboru ponizej, aby kontynuowac. Musisz zaakceptowac warunki umowy, aby zainstalowac $(^NameDA). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_POLISH} "Nacisnij klawisz Page Down, aby zobaczyc dalsza czesc umowy."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_POLISH} "Instalacja"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_POLISH} "Prosze czekac, podczas gdy $(^NameDA) jest instalowany."
LangString MUI_TEXT_FINISH_TITLE ${LANG_POLISH} "Zakonczono instalacje"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_POLISH} "Instalacja zakonczona pomyslnie."
LangString MUI_TEXT_ABORT_TITLE ${LANG_POLISH} "Instalacja przerwana"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_POLISH} "Instalacja nie zostala zakonczona pomyslnie."
LangString  MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_POLISH} "Deinstalacja"
LangString  MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_POLISH} "Prosze czekac, $(^NameDA) jest odinstalowywany."
LangString  MUI_UNTEXT_FINISH_TITLE ${LANG_POLISH} "Zakonczono odinstalowanie"
LangString  MUI_UNTEXT_FINISH_SUBTITLE ${LANG_POLISH} "Odinstalowanie zakonczone pomyslnie."
LangString  MUI_UNTEXT_ABORT_TITLE ${LANG_POLISH} "Deinstalacja przerwana"
LangString  MUI_UNTEXT_ABORT_SUBTITLE ${LANG_POLISH} "Deinstalacja nie zostala zakonczona pomyslnie."

!insertmacro LANGFILE "PortugueseBr" "PortugueseBr"

LangString MUI_TEXT_LICENSE_TITLE ${LANG_PORTUGUESEBR} "Acordo da licen�a"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_PORTUGUESEBR} "Por favor, reveja os termos da licen�a antes de instalar $(^NameDA)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_PORTUGUESEBR} "Se voc� aceita os termos do acordo, clique na caixa de sele��o abaixo. Voc� deve aceitar o acordo para instalar a $(^NameDA). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_PORTUGUESEBR} "Pressione Page Down para ver o resto do acordo."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_PORTUGUESEBR} "Instalando"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_PORTUGUESEBR} "Por favor, aguarde enquanto $(^NameDA) est� sendo instalado."
LangString MUI_TEXT_FINISH_TITLE ${LANG_PORTUGUESEBR} "Instala��o Completa"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_PORTUGUESEBR} "O instalador completou com sucesso."
LangString MUI_TEXT_ABORT_TITLE ${LANG_PORTUGUESEBR} "Instala��o Abortada"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_PORTUGUESEBR} "O instalador n�o completou com sucesso."
LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_PORTUGUESEBR} "Desinstalando"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_PORTUGUESEBR} "Por favor, espere enquanto a $(^NameDA) est� sendo desinstalada."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_PORTUGUESEBR} "Desinstala��o Completa"
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_PORTUGUESEBR} "A desinstala��o foi completada com sucesso."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_PORTUGUESEBR} "Desinstala��o Abortada"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_PORTUGUESEBR} "A desinstala��o n�o foi completada com sucesso."

!insertmacro LANGFILE "Korean" "Korean"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_KOREAN} "??? ??"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_KOREAN} "$(^Name)?(?) ???? ?? ?? ??? ??????."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_KOREAN} "??? ?? ??? ???? ??, ??? ?? ??? ??????. $(^Name)?(?) ????? ??? ??? ???? ???. $_CLICK"

LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_KOREAN} "??? ??? ??? ??? Page Down ?? ?????."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_KOREAN} "?? ?"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_KOREAN} "$(^Name) ??? ??? ??? ??? ??????."
LangString MUI_TEXT_FINISH_TITLE ${LANG_KOREAN} "?? ??"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_KOREAN} "??? ??????. ??? ???? ??? ??????."
LangString MUI_TEXT_ABORT_TITLE ${LANG_KOREAN} "?? ??? ???????."
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_KOREAN} "??? ???? ?????. ??? ???? ??? ??????."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_KOREAN} "?? ?? ?"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_KOREAN} "$(^Name) ??? ??? ??? ??? ??????."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_KOREAN} "?? ??? ??????. ??? ???? ??? ??????."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_KOREAN} "?? ?? ??"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_KOREAN} "?? ?? ??"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_KOREAN} "??? ???? ?????. ??? ???? ??? ??????."

!insertmacro LANGFILE "Japanese" "Japanese"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_JAPANESE} "JAPANESE"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_JAPANESE} "$(^NameDA)?????????????????????????????"
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_JAPANESE} "?????????????????????????????????????????$(^NameDA) ???????????????????????????? $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_JAPANESE} "[Page Down]???????????????????" 
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_JAPANESE} "??????"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_JAPANESE} "$(^NameDA)?????????????????????????"
LangString MUI_TEXT_FINISH_TITLE ${LANG_JAPANESE} "?????????"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_JAPANESE} "??????????????"
LangString MUI_TEXT_ABORT_TITLE ${LANG_JAPANESE} "?????????"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_JAPANESE} "?????????????????????"

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_JAPANESE} "????????"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_JAPANESE} "$(^NameDA)???????????????????????????"
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_JAPANESE} "???????????"
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_JAPANESE} "????????????????"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_JAPANESE} "???????????"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_JAPANESE} "???????????????????????"