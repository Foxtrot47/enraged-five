@ECHO OFF

Echo settings Folder Location
X:
cd X:\gta5\src\dev_ng\extra\installer\Skin\Skin Program

Echo creating skin file. 
CreateSkinFile.exe "X:\gta5\src\dev_ng\extra\installer\Skin\Skin Input" "X:\gta5\src\dev_ng\extra\installer\Skin\Skin Output\GTAVInstallerSkinFinal.isn"

Echo Copying GTAVInstallerSkin.isn in to the installshields skin folder
COPY /Y "X:\gta5\src\dev_ng\extra\installer\Skin\Skin Output\GTAVInstallerSkinFinal.isn" "C:\Program Files (x86)\InstallShield\2012\Skins\GTAVInstallerSkinFinal.isn"

Echo Please restart Installshield 2012 if you have it open and the new skin will be loaded in to the project. You must rebuild the project before it can be seen. 

pause