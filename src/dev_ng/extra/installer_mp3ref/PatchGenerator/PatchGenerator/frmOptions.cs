﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PatchGenerator
{
    public partial class frmOptions : Form
    {
        public object CurrentSettings;

        public frmOptions()
        {
            InitializeComponent();
        }

        private void frmOptions_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            e.Cancel = true;
        }

        private void frmOptions_Load(object sender, EventArgs e)
        {
            pgdSettings.SelectedObject = CurrentSettings;
        }

        private void frmOptions_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible == true)
            {
                pgdSettings.SelectedObject = CurrentSettings;
            }
        }
    }
}
