﻿namespace PatchGenerator
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chklCodeBuildType = new System.Windows.Forms.CheckedListBox();
            this.btnViewCodeSettings = new System.Windows.Forms.Button();
            this.btnBuildAll = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnBuildCode = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnViewSettings = new System.Windows.Forms.Button();
            this.cmbMainPatchType = new System.Windows.Forms.ComboBox();
            this.chkGameshieldWrap = new System.Windows.Forms.CheckBox();
            this.chkSignMainExecutable = new System.Windows.Forms.CheckBox();
            this.btnBuildMainPatch = new System.Windows.Forms.Button();
            this.chkSignMainInstaller = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMainVersion = new System.Windows.Forms.MaskedTextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnViewDLCSettings = new System.Windows.Forms.Button();
            this.btnBuildDLC = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbDLCPack = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.rtfOutput = new System.Windows.Forms.RichTextBox();
            this.chkRebuild = new System.Windows.Forms.CheckBox();
            this.tabMain.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabPage1);
            this.tabMain.Controls.Add(this.tabPage2);
            this.tabMain.Location = new System.Drawing.Point(13, 13);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(473, 414);
            this.tabMain.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtMainVersion);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(465, 388);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General Patch";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkRebuild);
            this.groupBox1.Controls.Add(this.chklCodeBuildType);
            this.groupBox1.Controls.Add(this.btnViewCodeSettings);
            this.groupBox1.Controls.Add(this.btnBuildAll);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnBuildCode);
            this.groupBox1.Location = new System.Drawing.Point(10, 49);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(441, 185);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Code Building";
            // 
            // chklCodeBuildType
            // 
            this.chklCodeBuildType.FormattingEnabled = true;
            this.chklCodeBuildType.Location = new System.Drawing.Point(9, 36);
            this.chklCodeBuildType.Name = "chklCodeBuildType";
            this.chklCodeBuildType.Size = new System.Drawing.Size(245, 139);
            this.chklCodeBuildType.TabIndex = 14;
            // 
            // btnViewCodeSettings
            // 
            this.btnViewCodeSettings.Location = new System.Drawing.Point(320, 73);
            this.btnViewCodeSettings.Name = "btnViewCodeSettings";
            this.btnViewCodeSettings.Size = new System.Drawing.Size(115, 23);
            this.btnViewCodeSettings.TabIndex = 13;
            this.btnViewCodeSettings.Text = "View Code Settings";
            this.btnViewCodeSettings.UseVisualStyleBackColor = true;
            this.btnViewCodeSettings.Click += new System.EventHandler(this.btnViewCodeSettings_Click);
            // 
            // btnBuildAll
            // 
            this.btnBuildAll.Location = new System.Drawing.Point(320, 44);
            this.btnBuildAll.Name = "btnBuildAll";
            this.btnBuildAll.Size = new System.Drawing.Size(115, 23);
            this.btnBuildAll.TabIndex = 7;
            this.btnBuildAll.Text = "Build All Code";
            this.btnBuildAll.UseVisualStyleBackColor = true;
            this.btnBuildAll.Click += new System.EventHandler(this.btnBuildAll_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Build Type";
            // 
            // btnBuildCode
            // 
            this.btnBuildCode.Location = new System.Drawing.Point(320, 15);
            this.btnBuildCode.Name = "btnBuildCode";
            this.btnBuildCode.Size = new System.Drawing.Size(115, 23);
            this.btnBuildCode.TabIndex = 4;
            this.btnBuildCode.Text = "Build Code";
            this.btnBuildCode.UseVisualStyleBackColor = true;
            this.btnBuildCode.Click += new System.EventHandler(this.btnBuildCode_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnViewSettings);
            this.groupBox2.Controls.Add(this.cmbMainPatchType);
            this.groupBox2.Controls.Add(this.chkGameshieldWrap);
            this.groupBox2.Controls.Add(this.chkSignMainExecutable);
            this.groupBox2.Controls.Add(this.btnBuildMainPatch);
            this.groupBox2.Controls.Add(this.chkSignMainInstaller);
            this.groupBox2.Location = new System.Drawing.Point(10, 240);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(441, 133);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Installer Patch";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Patch Type";
            // 
            // btnViewSettings
            // 
            this.btnViewSettings.Location = new System.Drawing.Point(320, 102);
            this.btnViewSettings.Name = "btnViewSettings";
            this.btnViewSettings.Size = new System.Drawing.Size(115, 23);
            this.btnViewSettings.TabIndex = 12;
            this.btnViewSettings.Text = "View Patch Settings";
            this.btnViewSettings.UseVisualStyleBackColor = true;
            this.btnViewSettings.Click += new System.EventHandler(this.btnViewSettings_Click);
            // 
            // cmbMainPatchType
            // 
            this.cmbMainPatchType.FormattingEnabled = true;
            this.cmbMainPatchType.Location = new System.Drawing.Point(9, 32);
            this.cmbMainPatchType.Name = "cmbMainPatchType";
            this.cmbMainPatchType.Size = new System.Drawing.Size(245, 21);
            this.cmbMainPatchType.TabIndex = 0;
            this.cmbMainPatchType.SelectedIndexChanged += new System.EventHandler(this.cmbMainPatchType_SelectedIndexChanged);
            // 
            // chkGameshieldWrap
            // 
            this.chkGameshieldWrap.AutoSize = true;
            this.chkGameshieldWrap.Location = new System.Drawing.Point(9, 108);
            this.chkGameshieldWrap.Name = "chkGameshieldWrap";
            this.chkGameshieldWrap.Size = new System.Drawing.Size(166, 17);
            this.chkGameshieldWrap.TabIndex = 11;
            this.chkGameshieldWrap.Text = "Gameshield Wrap Executable";
            this.chkGameshieldWrap.UseVisualStyleBackColor = true;
            // 
            // chkSignMainExecutable
            // 
            this.chkSignMainExecutable.AutoSize = true;
            this.chkSignMainExecutable.Location = new System.Drawing.Point(9, 62);
            this.chkSignMainExecutable.Name = "chkSignMainExecutable";
            this.chkSignMainExecutable.Size = new System.Drawing.Size(160, 17);
            this.chkSignMainExecutable.TabIndex = 8;
            this.chkSignMainExecutable.Text = "Sign Executables And DLL\'s";
            this.chkSignMainExecutable.UseVisualStyleBackColor = true;
            // 
            // btnBuildMainPatch
            // 
            this.btnBuildMainPatch.Location = new System.Drawing.Point(320, 73);
            this.btnBuildMainPatch.Name = "btnBuildMainPatch";
            this.btnBuildMainPatch.Size = new System.Drawing.Size(115, 23);
            this.btnBuildMainPatch.TabIndex = 10;
            this.btnBuildMainPatch.Text = "Build Patch";
            this.btnBuildMainPatch.UseVisualStyleBackColor = true;
            this.btnBuildMainPatch.Click += new System.EventHandler(this.btnBuildMainPatch_Click);
            // 
            // chkSignMainInstaller
            // 
            this.chkSignMainInstaller.AutoSize = true;
            this.chkSignMainInstaller.Location = new System.Drawing.Point(9, 85);
            this.chkSignMainInstaller.Name = "chkSignMainInstaller";
            this.chkSignMainInstaller.Size = new System.Drawing.Size(86, 17);
            this.chkSignMainInstaller.TabIndex = 9;
            this.chkSignMainInstaller.Text = "Sign Installer";
            this.chkSignMainInstaller.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Version";
            // 
            // txtMainVersion
            // 
            this.txtMainVersion.Location = new System.Drawing.Point(10, 23);
            this.txtMainVersion.Mask = "0.0#.0#.0##";
            this.txtMainVersion.Name = "txtMainVersion";
            this.txtMainVersion.Size = new System.Drawing.Size(121, 20);
            this.txtMainVersion.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnViewDLCSettings);
            this.tabPage2.Controls.Add(this.btnBuildDLC);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.cmbDLCPack);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(465, 388);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "DLC Patch";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnViewDLCSettings
            // 
            this.btnViewDLCSettings.Location = new System.Drawing.Point(344, 42);
            this.btnViewDLCSettings.Name = "btnViewDLCSettings";
            this.btnViewDLCSettings.Size = new System.Drawing.Size(115, 23);
            this.btnViewDLCSettings.TabIndex = 3;
            this.btnViewDLCSettings.Text = "View DLC Settings";
            this.btnViewDLCSettings.UseVisualStyleBackColor = true;
            this.btnViewDLCSettings.Click += new System.EventHandler(this.btnViewDLCSettings_Click);
            // 
            // btnBuildDLC
            // 
            this.btnBuildDLC.Location = new System.Drawing.Point(344, 13);
            this.btnBuildDLC.Name = "btnBuildDLC";
            this.btnBuildDLC.Size = new System.Drawing.Size(115, 23);
            this.btnBuildDLC.TabIndex = 2;
            this.btnBuildDLC.Text = "Build Pack";
            this.btnBuildDLC.UseVisualStyleBackColor = true;
            this.btnBuildDLC.Click += new System.EventHandler(this.btnBuildDLC_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "DLC Pack";
            // 
            // cmbDLCPack
            // 
            this.cmbDLCPack.FormattingEnabled = true;
            this.cmbDLCPack.Location = new System.Drawing.Point(13, 27);
            this.cmbDLCPack.Name = "cmbDLCPack";
            this.cmbDLCPack.Size = new System.Drawing.Size(260, 21);
            this.cmbDLCPack.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 430);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Output";
            // 
            // rtfOutput
            // 
            this.rtfOutput.Location = new System.Drawing.Point(13, 446);
            this.rtfOutput.Name = "rtfOutput";
            this.rtfOutput.Size = new System.Drawing.Size(469, 255);
            this.rtfOutput.TabIndex = 7;
            this.rtfOutput.Text = "";
            // 
            // chkRebuild
            // 
            this.chkRebuild.AutoSize = true;
            this.chkRebuild.Checked = true;
            this.chkRebuild.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRebuild.Location = new System.Drawing.Point(320, 102);
            this.chkRebuild.Name = "chkRebuild";
            this.chkRebuild.Size = new System.Drawing.Size(90, 17);
            this.chkRebuild.TabIndex = 15;
            this.chkRebuild.Text = "Rebuild Code";
            this.chkRebuild.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 716);
            this.Controls.Add(this.rtfOutput);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tabMain);
            this.Name = "frmMain";
            this.Text = "Patch Generator";
            this.tabMain.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbMainPatchType;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox txtMainVersion;
        private System.Windows.Forms.CheckBox chkSignMainInstaller;
        private System.Windows.Forms.CheckBox chkSignMainExecutable;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnBuildMainPatch;
        private System.Windows.Forms.CheckBox chkGameshieldWrap;
        private System.Windows.Forms.RichTextBox rtfOutput;
        private System.Windows.Forms.Button btnViewSettings;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBuildAll;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnBuildCode;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnViewCodeSettings;
        private System.Windows.Forms.Button btnViewDLCSettings;
        private System.Windows.Forms.Button btnBuildDLC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbDLCPack;
        private System.Windows.Forms.CheckedListBox chklCodeBuildType;
        private System.Windows.Forms.CheckBox chkRebuild;

    }
}

