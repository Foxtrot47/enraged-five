﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Xml;

namespace PatchGenerator
{
    public partial class frmMain : Form
    {
        public Dictionary<string, PatchSettings> PatchSettingsList;
        public Dictionary<string, CompileSettings> CompileSettingsList;
        public Dictionary<string, DLCSettings> DLCSettingsList;

        public PatchSettings currentSettings = null;

        public string GameshieldManagerPath = "C:\\Program Files (x86)\\Yummy Interactive\\GameShield 4.6\\GameShield_License_Manager.exe";
        public string NSISTool = "C:\\Program Files (x86)\\NSIS\\Unicode\\MakeNSIS.exe";

        public string SignToolPath = "%VS80COMNTOOLS%\\bin\\signtool.exe";
        public string SigningCertificate = "X:\\payne\\tools\\installer\\project\\installer_Patch\\rockstar.pfx";
        public string SigningCertificatePassword = "CRAPwh@ckIT";

        public string BuildPatchTool = "X:\\payne\\tools\\installer\\project\\installer_Patch\\BuildPatch.exe";
        public string BuildDLCTool = "X:\\payne\\tools\\installer\\project\\installer_DLC\\BuildDLC.exe";

        public frmOptions OptionsWindow;

        public frmMain()
        {
            PatchSettingsList = new Dictionary<string, PatchSettings>();
            CompileSettingsList = new Dictionary<string, CompileSettings>();
            DLCSettingsList = new Dictionary<string, DLCSettings>();

            InitializeComponent();
            LoadXmlSettings();

            cmbMainPatchType.SelectedIndex = 0;
            cmbDLCPack.SelectedIndex = 0;

            chkGameshieldWrap.Checked = true;
            chkSignMainExecutable.Checked = true;
            chkSignMainInstaller.Checked = true;

            txtMainVersion.Text = "1.00.00.000";

            OptionsWindow = new frmOptions();
        }

        public void LoadXmlSettings()
        {
            XmlNode rootNode = XmlUtil.LoadXmlFile("settings.xml");
            XmlNode topNode = XmlUtil.GetChildNode(rootNode, "PatchGeneratorSettings");

            XmlNode globalsNode = XmlUtil.GetChildNode(topNode, "Globals");
            XmlNode patchesNode = XmlUtil.GetChildNode(topNode, "Patches");
            XmlNode buildsNode = XmlUtil.GetChildNode(topNode, "CodeBuilds");
            XmlNode dlcPackagesNode = XmlUtil.GetChildNode(topNode, "DLCPackages");

            GameshieldManagerPath = XmlUtil.GetChildNode(globalsNode, "GameshieldManagerPath").InnerText;
            SignToolPath = XmlUtil.GetChildNode(globalsNode, "SignToolPath").InnerText;
            SigningCertificate = XmlUtil.GetChildNode(globalsNode, "SignCertificate").InnerText;
            SigningCertificatePassword = XmlUtil.GetChildNode(globalsNode, "SignCertificatePassword").InnerText;
            BuildPatchTool = XmlUtil.GetChildNode(globalsNode, "BuildPatchTool").InnerText;
            BuildDLCTool = XmlUtil.GetChildNode(globalsNode, "BuildDLCTool").InnerText;
            NSISTool = XmlUtil.GetChildNode(globalsNode, "NSISTool").InnerText;

            foreach (XmlNode dlcNode in dlcPackagesNode.ChildNodes)
            {
                DLCSettings settings = new DLCSettings();

                string dlcName = XmlUtil.GetAttributeValue(dlcNode, "Name");

                settings.PackageName = XmlUtil.GetChildNode(dlcNode, "PackageName").InnerText;
                settings.DataDirectory = XmlUtil.GetChildNode(dlcNode, "DataDirectory").InnerText;
                settings.OutputFolder = XmlUtil.GetChildNode(dlcNode, "OutputFolder").InnerText;

                DLCSettingsList.Add(dlcName, settings);
                cmbDLCPack.Items.Add(dlcName);
            }

            foreach (XmlNode patchNode in patchesNode.ChildNodes)
            {
                PatchSettings settings = new PatchSettings();

                string patchType = XmlUtil.GetAttributeValue(patchNode, "Type");
                bool gameShieldWrap = XmlUtil.GetAttributeValue(patchNode, "GameshieldWrap") == "true" ? true : false;
                bool createInstaller = XmlUtil.GetAttributeValue(patchNode, "CreateInstaller") == "true" ? true : false;

                settings.GameshieldWrap = gameShieldWrap;
                settings.CreateInstaller = createInstaller;

                settings.FinalExecutableName = XmlUtil.GetChildNode(patchNode, "FinalExecutableName").InnerText;
                settings.ExecutableName = XmlUtil.GetChildNode(patchNode, "ExecutableName").InnerText;
                settings.ExecutablePath = XmlUtil.GetChildNode(patchNode, "ExecutablePath").InnerText;

                settings.GameshieldLicenseFile = XmlUtil.GetChildNode(patchNode, "GameshieldLicenseFile").InnerText;
                settings.GameshieldLicensePath = XmlUtil.GetChildNode(patchNode, "GameshieldLicensePath").InnerText;
                settings.GameshieldLicenseProject = XmlUtil.GetChildNode(patchNode, "GameshieldLicenseProject").InnerText;
                settings.GameshieldExePath = XmlUtil.GetChildNode(patchNode, "GameshieldExePath").InnerText;
                settings.GameshieldOutputPath = XmlUtil.GetChildNode(patchNode, "GameshieldOutputPath").InnerText;
                settings.GameshieldOutputExePath = XmlUtil.GetChildNode(patchNode, "GameshieldOutputExePath").InnerText;

                settings.PatchDataDirectory = XmlUtil.GetChildNode(patchNode, "PatchDataDirectory").InnerText;

                settings.InstallerName = XmlUtil.GetChildNode(patchNode, "InstallerName").InnerText;
                settings.InstallerIcon = XmlUtil.GetChildNode(patchNode, "InstallerIcon").InnerText;
                settings.InstallerBuildDirectory = XmlUtil.GetChildNode(patchNode, "InstallerBuildDirectory").InnerText;
                settings.InstallerPatchName = XmlUtil.GetChildNode(patchNode, "InstallerPatchName").InnerText;
                settings.InstallerWaitForProcess = XmlUtil.GetChildNode(patchNode, "InstallerWaitForProcess").InnerText;

                PatchSettingsList.Add(patchType, settings);
                cmbMainPatchType.Items.Add(patchType);
            }

            foreach (XmlNode codeBuildNode in buildsNode.ChildNodes)
            {
                CompileSettings settings = new CompileSettings();

                string buildType = XmlUtil.GetAttributeValue(codeBuildNode, "Type");

                settings.ProjectFile = XmlUtil.GetChildNode(codeBuildNode, "ProjectFile").InnerText;
                settings.ProjectResourceFile = XmlUtil.GetChildNode(codeBuildNode, "ResourceFile").InnerText;
                settings.ProjectConfiguration = XmlUtil.GetChildNode(codeBuildNode, "Configuration").InnerText;
                settings.BuildFolder = XmlUtil.GetChildNode(codeBuildNode, "BuildFolder").InnerText;
                settings.CompiledExecutableName = XmlUtil.GetChildNode(codeBuildNode, "CompiledExecutableName").InnerText;
                settings.OutputExecutableName = XmlUtil.GetChildNode(codeBuildNode, "OutputExecutableName").InnerText;
                settings.OutputFolder = XmlUtil.GetChildNode(codeBuildNode, "OutputFolder").InnerText;
                settings.PreprocessorArguments = XmlUtil.GetChildNode(codeBuildNode, "PreprocessorArguments").InnerText;

                CompileSettingsList.Add(buildType, settings);
                chklCodeBuildType.Items.Add(buildType);
            }
        }

        private void OutputDebugInfo(string line)
        {
            rtfOutput.Text += line + "\r\n";
        }

        private bool WrapGameshieldExecutable(PatchSettings settings, string version)
        {
            Version expectedVersion = new Version(version);

            string executablePath = settings.ExecutablePath.Replace("%version%", expectedVersion.ToString());

            string originalExe = Path.Combine(executablePath, settings.ExecutableName);
            string gameshieldFolder = Path.Combine(settings.GameshieldLicensePath, settings.GameshieldExePath);
            string gameshieldExe = Path.Combine(gameshieldFolder, settings.FinalExecutableName);

            string gameshieldOutputFolder = Path.Combine(executablePath, "IronWrapped");
            string gameshieldOutputExeFolder = Path.Combine(gameshieldOutputFolder, settings.GameshieldOutputExePath);

            if (!Directory.Exists(gameshieldOutputFolder))
                Directory.CreateDirectory(gameshieldOutputFolder);

            if (!Directory.Exists(gameshieldOutputExeFolder))
                Directory.CreateDirectory(gameshieldOutputExeFolder);

            try
            {
                File.Copy(originalExe, gameshieldExe, true);
            }
            catch (Exception)
            {
                OutputDebugInfo("Error copying Gameshield executables.");
            }

            Process gameshieldProcess = new Process();

            gameshieldProcess.StartInfo.FileName = GameshieldManagerPath;
            gameshieldProcess.StartInfo.Arguments = "/c " + settings.GameshieldLicenseProject;

            gameshieldProcess.Start();
            gameshieldProcess.WaitForExit();

            if (gameshieldProcess.ExitCode != 0)
            {
                OutputDebugInfo("There was an error wrapping the Gameshield executable.");
                return false;
            }

            string gameshieldReleaseFolder = Path.Combine(settings.GameshieldLicensePath, settings.GameshieldOutputPath);
            
            string cleanMachinePath = Path.Combine(gameshieldReleaseFolder, "CleanMachine.exe");
            string wrappedExePath = Path.Combine(gameshieldReleaseFolder, settings.FinalExecutableName);
            string licenseFilePath = Path.Combine(gameshieldReleaseFolder, settings.GameshieldLicenseFile);

            string cleanMachineOutputPath = Path.Combine(gameshieldOutputExeFolder, "CleanMachine.exe");
            string wrappedExeOutputPath = Path.Combine(gameshieldOutputExeFolder, settings.FinalExecutableName);
            string licenseFileOutputPath = Path.Combine(gameshieldOutputExeFolder, settings.GameshieldLicenseFile);

            string wrappedExeInstallerPath = Path.Combine(settings.PatchDataDirectory, settings.FinalExecutableName);

            if (!Directory.Exists(settings.PatchDataDirectory))
            {
                Directory.CreateDirectory(settings.PatchDataDirectory);
            }

            try
            {
                File.Copy(cleanMachinePath, cleanMachineOutputPath, true);
                File.Copy(wrappedExePath, wrappedExeOutputPath, true);
                File.Copy(licenseFilePath, licenseFileOutputPath, true);

                File.Copy(wrappedExePath, wrappedExeInstallerPath, true);
            }
            catch (Exception)
            {
                OutputDebugInfo("There was an error copying the Gameshield files to the output directory.");
                return false;
            }

            if (!SignFile(wrappedExeOutputPath))
            {
                OutputDebugInfo(string.Format("Could not sign IronWrapped executable: {0}", wrappedExeOutputPath));
                return false;
            }

            return true;
        }

        private bool SignFiles(string path)
        {
            if (Directory.Exists(path))
            {
                DirectoryInfo di = new DirectoryInfo(path);

                FileInfo[] exeFiles = di.GetFiles("*.exe");
                FileInfo[] dllFiles = di.GetFiles("*.dll");

                foreach (FileInfo exeFile in exeFiles)
                {
                    if (!SignFile(exeFile.FullName))
                        return false;
                }

                foreach (FileInfo dllFile in dllFiles)
                {
                    if (!SignFile(dllFile.FullName))
                        return false;
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        private bool SignFile(string path)
        {
            string signTool = System.Environment.ExpandEnvironmentVariables(SignToolPath);

            if (!File.Exists(signTool))
            {
                OutputDebugInfo("Could not find the signing tool file.");
                return false;
            }

            if (!File.Exists(path))
            {
                OutputDebugInfo(string.Format("Invalid file to sign: {0}", path));
                return false;
            }

            MakeFileWritable(path);

            Process signProcess = new Process();

            string arguments = string.Format("sign /f {0} /p \"{1}\" /t http://timestamp.verisign.com/scripts/timstamp.dll /v \"{2}\"", SigningCertificate, SigningCertificatePassword, path);

            signProcess.StartInfo.FileName = signTool;
            signProcess.StartInfo.Arguments = arguments;

            signProcess.Start();
            signProcess.WaitForExit();

            if (signProcess.ExitCode != 0)
                return false;

            return true;
        }

        private bool MakeFileWritable(string path)
        {
            if (File.Exists(path))
            {
                File.SetAttributes(path, File.GetAttributes(path) & ~FileAttributes.ReadOnly);
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CheckExeVersions(PatchSettings settings, string version)
        {
            string executablePath = Path.Combine(settings.PatchDataDirectory, settings.FinalExecutableName);

            if (!File.Exists(executablePath))
            {
                OutputDebugInfo(string.Format("Could not find final executable: {0}", executablePath));
                return false;
            }

            Version buildVersion = new Version(version);

            FileVersionInfo vi = FileVersionInfo.GetVersionInfo(executablePath);
            Version exeVersion = new Version(vi.FileMajorPart, vi.FileMinorPart, vi.FileBuildPart, vi.FilePrivatePart);

            if (exeVersion == buildVersion)
            {
                return true;
            }

            DialogResult result = MessageBox.Show("The build version does not match the executable version\nyou are attempting to build. Are you sure?", "Version Mismatch", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                return true;
            }

            return false;
        }

        private bool BuildPatch(PatchSettings settings, string version)
        {
            if (!File.Exists(BuildPatchTool))
            {
                OutputDebugInfo("Could not find BuildPatch tool to package installer.");
                return false;
            }

            string arguments = string.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\" \"{6}\" \"{7}\"",
                NSISTool,
                settings.InstallerName,
                settings.InstallerIcon,
                settings.PatchDataDirectory,
                settings.InstallerBuildDirectory,
                settings.InstallerPatchName,
                version,
                settings.InstallerWaitForProcess);

            Process patchProcess = new Process();

            patchProcess.StartInfo.WorkingDirectory = settings.InstallerBuildDirectory;
            patchProcess.StartInfo.Arguments = arguments;
            patchProcess.StartInfo.FileName = BuildPatchTool;

            patchProcess.Start();
            patchProcess.WaitForExit();

            return true;
        }

        private bool BuildCode(CompileSettings settings, string version)
        {
            Version expectedVersion = new Version(version);

            rtfOutput.Clear();

            if (!File.Exists(settings.ProjectFile))
            {
                OutputDebugInfo(string.Format("Could not find project file to compile: {0}", settings.ProjectFile));
                return false;
            }

            Process buildProcess = new Process();

            string buildType = "/rebuild";

            if (!chkRebuild.Checked)
                buildType = "/build";

            buildProcess.StartInfo.FileName = "BuildConsole.exe";
            buildProcess.StartInfo.Arguments = string.Format("{0} {1} /cfg=\"{2}\" /cl_add=\"{3}\"", settings.ProjectFile, buildType, settings.ProjectConfiguration, settings.PreprocessorArguments);

            if (!buildProcess.StartInfo.EnvironmentVariables.ContainsKey("BUILD_FOLDER"))
                buildProcess.StartInfo.EnvironmentVariables.Add("BUILD_FOLDER", settings.BuildFolder);

            buildProcess.StartInfo.UseShellExecute = false;

            buildProcess.Start();
            buildProcess.WaitForExit();

            if (buildProcess.ExitCode != 0)
            {
                OutputDebugInfo("Error compiling, check the Incredibuild log for more info...");
                return false;
            }

            string outputFolder = settings.OutputFolder.Replace("%version%", expectedVersion.ToString());
            string compiledName = Path.GetFileNameWithoutExtension(settings.CompiledExecutableName);
            string compiledExe = Path.Combine(settings.BuildFolder, compiledName + ".exe");
            string compiledPDB = Path.Combine(settings.BuildFolder, compiledName + ".pdb");

            if (!Directory.Exists(outputFolder))
                Directory.CreateDirectory(outputFolder);

            string outputName = Path.GetFileNameWithoutExtension(settings.OutputExecutableName);
            string outputExe = Path.Combine(outputFolder, outputName + ".exe");
            string outputPDB = Path.Combine(outputFolder, outputName + ".pdb");

            File.Copy(compiledExe, outputExe, true);
            File.Copy(compiledPDB, outputPDB, true);

            return true;
        }

        private bool UpdateVersionNumber(CompileSettings settings, string version)
        {
            if (!File.Exists(settings.ProjectResourceFile))
            {
                OutputDebugInfo("Could not find resource file to update version number...");
                return false;
            }

            Version exeVersion = new Version(version);

            string tempResourceFile = settings.ProjectResourceFile + "_temp";

            StreamReader reader = new StreamReader(settings.ProjectResourceFile);
            StreamWriter writer = new StreamWriter(tempResourceFile);

            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();

                if (line.Trim().StartsWith("FILEVERSION"))
                    line = string.Format(" FILEVERSION {0},{1},{2},{3}", exeVersion.Major, exeVersion.Minor, exeVersion.Build, exeVersion.Revision);
                if (line.Trim().StartsWith("PRODUCTVERSION"))
                    line = string.Format(" PRODUCTVERSION {0},{1},{2},{3}", exeVersion.Major, exeVersion.Minor, exeVersion.Build, exeVersion.Revision);
                if (line.Trim().StartsWith("VALUE \"FileVersion\""))
                    line = string.Format("            VALUE \"FileVersion\", \"{0}, {1}, {2}, {3}\"", exeVersion.Major, exeVersion.Minor, exeVersion.Build, exeVersion.Revision);
                if (line.Trim().StartsWith("VALUE \"ProductVersion\""))
                    line = string.Format("            VALUE \"ProductVersion\", \"{0}, {1}, {2}, {3}\"", exeVersion.Major, exeVersion.Minor, exeVersion.Build, exeVersion.Revision);

                writer.WriteLine(line);
            }

            reader.Close();
            writer.Close();

            File.Copy(tempResourceFile, settings.ProjectResourceFile, true);

            return true;
        }

        private bool BuildDLC(DLCSettings settings)
        {
            Process DLCProcess = new Process();

            DLCProcess.StartInfo.FileName = BuildDLCTool;
            DLCProcess.StartInfo.Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\"", NSISTool, settings.DataDirectory, settings.OutputFolder, settings.PackageName);
            
            DLCProcess.Start();
            DLCProcess.WaitForExit();

            if (DLCProcess.ExitCode != 0)
            {
                return false;
            }

            return true;
        }

        private bool SignInstaller(PatchSettings settings, string version)
        {
            Version ver = new Version(version);

            string patchInstallerFilename = settings.InstallerPatchName + "_" + ver.Major + "_" + ver.Minor + "_" + ver.Build + "_" + ver.Revision + ".exe";
            string patchInstallerPath = Path.Combine(settings.InstallerBuildDirectory, patchInstallerFilename);

            SignFile(patchInstallerPath);

            return true;
        }

        private void btnBuildMainPatch_Click(object sender, EventArgs e)
        {
            btnBuildMainPatch.Enabled = false;

            rtfOutput.Clear();

            string patchType = (string)cmbMainPatchType.SelectedItem;

            if (!string.IsNullOrEmpty(patchType)
                && PatchSettingsList.ContainsKey(patchType))
            {
                currentSettings = PatchSettingsList[patchType];
            }
            else
            {
                return;
            }

            OutputDebugInfo("Starting Patch Process...");
            OutputDebugInfo("Building " + patchType + " Version: " + txtMainVersion.Text);

            if (chkGameshieldWrap.Checked)
            {
                OutputDebugInfo("Gameshield Wrapping...");

                if (!WrapGameshieldExecutable(currentSettings, txtMainVersion.Text))
                {
                    OutputDebugInfo("Error during Gameshield Wrapping...");
                    btnBuildMainPatch.Enabled = true;
                    return;
                }
            }

            if (chkSignMainExecutable.Checked)
            {
                OutputDebugInfo("Signing Files...");

                if (!SignFiles(currentSettings.PatchDataDirectory))
                {
                    OutputDebugInfo("Error signing Files...");
                    btnBuildMainPatch.Enabled = true;
                    return;
                }
            }

            OutputDebugInfo("Building Patch Installer...");

            if (!CheckExeVersions(currentSettings, txtMainVersion.Text))
            {
                OutputDebugInfo("Versions mismatch, aborting build...");
                btnBuildMainPatch.Enabled = true;
                return;
            }

            if (currentSettings.CreateInstaller)
            {
                BuildPatch(currentSettings, txtMainVersion.Text);

                if (chkSignMainInstaller.Checked)
                {
                    OutputDebugInfo("Signing Installer...");

                    SignInstaller(currentSettings, txtMainVersion.Text);
                }
            }

            OutputDebugInfo("Patch Process Complete...");

            btnBuildMainPatch.Enabled = true;
        }

        private void cmbMainPatchType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string patchType = (string)cmbMainPatchType.SelectedItem;

            if (PatchSettingsList.ContainsKey(patchType))
            {
                PatchSettings settings = PatchSettingsList[patchType];

                if (settings.GameshieldWrap)
                {
                    chkGameshieldWrap.Enabled = true;
                    chkGameshieldWrap.Checked = true;
                }
                else
                {
                    chkGameshieldWrap.Enabled = false;
                    chkGameshieldWrap.Checked = false;
                }
            }
        }

        private void btnViewSettings_Click(object sender, EventArgs e)
        {
            string patchType = (string)cmbMainPatchType.SelectedItem;

            if (PatchSettingsList.ContainsKey(patchType))
            {
                PatchSettings settings = PatchSettingsList[patchType];

                OptionsWindow.CurrentSettings = settings;
                OptionsWindow.Show();
            }
        }

        private void btnBuildCode_Click(object sender, EventArgs e)
        {
            btnBuildCode.Enabled = false;

            rtfOutput.Clear();

            OutputDebugInfo("Starting Compile Process...");

            for (int i = 0; i < chklCodeBuildType.Items.Count; i++)
            {
                if (chklCodeBuildType.GetItemChecked(i))
                {
                    string patchType = (string)chklCodeBuildType.Items[i];

                    OutputDebugInfo(string.Format("Compiling: {0}...", patchType));

                    if (CompileSettingsList.ContainsKey(patchType))
                    {
                        CompileSettings settings = CompileSettingsList[patchType];

                        OutputDebugInfo("Building " + patchType + " Version: " + txtMainVersion.Text);

                        UpdateVersionNumber(settings, txtMainVersion.Text);

                        BuildCode(settings, txtMainVersion.Text);
                    }
                }
            }

            btnBuildCode.Enabled = true;
        }

        private void btnBuildAll_Click(object sender, EventArgs e)
        {
            btnBuildCode.Enabled = false;

            rtfOutput.Clear();

            OutputDebugInfo("Starting Compile Process...");

            for (int i = 0; i < chklCodeBuildType.Items.Count; i++)
            {
                string patchType = (string)chklCodeBuildType.Items[i];

                OutputDebugInfo(string.Format("Compiling: {0}...", patchType));

                if (CompileSettingsList.ContainsKey(patchType))
                {
                    CompileSettings settings = CompileSettingsList[patchType];

                    OutputDebugInfo("Building " + patchType + " Version: " + txtMainVersion.Text);

                    UpdateVersionNumber(settings, txtMainVersion.Text);

                    BuildCode(settings, txtMainVersion.Text);
                }
            }

            btnBuildCode.Enabled = true;
        }

        private void btnViewCodeSettings_Click(object sender, EventArgs e)
        {
            string patchType = (string)chklCodeBuildType.SelectedItem;

            if (patchType != null && CompileSettingsList.ContainsKey(patchType))
            {
                CompileSettings settings = CompileSettingsList[patchType];

                OptionsWindow.CurrentSettings = settings;
                OptionsWindow.Show();
            }
        }

        private void btnViewDLCSettings_Click(object sender, EventArgs e)
        {
            string dlcPack = (string)cmbDLCPack.SelectedItem;

            if (DLCSettingsList.ContainsKey(dlcPack))
            {
                DLCSettings settings = DLCSettingsList[dlcPack];

                OptionsWindow.CurrentSettings = settings;
                OptionsWindow.Show();
            }
        }

        private void btnBuildDLC_Click(object sender, EventArgs e)
        {
            btnBuildDLC.Enabled = false;

            rtfOutput.Clear();

            string dlcPack = (string)cmbDLCPack.SelectedItem;

            if (DLCSettingsList.ContainsKey(dlcPack))
            {
                DLCSettings settings = DLCSettingsList[dlcPack];

                OutputDebugInfo("Starting DLC Patch Process...");
                OutputDebugInfo(string.Format("Building {0}...", settings.PackageName));

                if (BuildDLC(settings))
                {
                    OutputDebugInfo("Signing DLC installer...");

                    SignFile(Path.Combine(settings.OutputFolder, settings.PackageName));

                    OutputDebugInfo("DLC installer build complete...");
                }
                else
                {
                    OutputDebugInfo("Failed building DLC installer...");
                }
            }

            btnBuildDLC.Enabled = true;
        }
    }
}
