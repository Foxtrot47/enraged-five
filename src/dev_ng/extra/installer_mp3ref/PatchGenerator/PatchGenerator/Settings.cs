﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PatchGenerator
{
    [DefaultProperty("Compile Settings")]
    public class CompileSettings
    {
        string projectFile;
        string projectResourceFile;
        string projectConfiguration;
        string buildFolder;
        string compiledExecutableName;
        string outputExecutableName;
        string outputFolder;
        string preprocessorArguments;

        [CategoryAttribute("Compile"), DescriptionAttribute("Visual Studio project file to build code from")]
        public string ProjectFile
        {
            get { return projectFile; }
            set { projectFile = value; }
        }

        [CategoryAttribute("Compile"), DescriptionAttribute("RC file associated with the project to update the version number.")]
        public string ProjectResourceFile
        {
            get { return projectResourceFile; }
            set { projectResourceFile = value; }
        }

        [CategoryAttribute("Compile"), DescriptionAttribute("Configuration to build in the project (ex. Final|Win32)")]
        public string ProjectConfiguration
        {
            get { return projectConfiguration; }
            set { projectConfiguration = value; }
        }

        [CategoryAttribute("Compile"), DescriptionAttribute("Build folder where the executable ends up")]
        public string BuildFolder
        {
            get { return buildFolder; }
            set { buildFolder = value; }
        }

        [CategoryAttribute("Compile"), DescriptionAttribute("Executable name that Visual Studio builds out")]
        public string CompiledExecutableName
        {
            get { return compiledExecutableName; }
            set { compiledExecutableName = value; }
        }

        [CategoryAttribute("Compile"), DescriptionAttribute("Name of executable we want to rename after the compile step")]
        public string OutputExecutableName
        {
            get { return outputExecutableName; }
            set { outputExecutableName = value; }
        }

        [CategoryAttribute("Compile"), DescriptionAttribute("Final output folder to put the compiled executable")]
        public string OutputFolder
        {
            get { return outputFolder; }
            set { outputFolder = value; }
        }

        [CategoryAttribute("Compile"), DescriptionAttribute("Preprocessor arguments that we want to pass to the compiler")]
        public string PreprocessorArguments
        {
            get { return preprocessorArguments; }
            set { preprocessorArguments = value; }
        }
    }

    [DefaultProperty("Patch Settings")]
    public class PatchSettings
    {
        string executableName;
        string executablePath;
        string finalExecutableName;

        string gameshieldLicenseProject;
        string gameshieldLicenseFile;
        string gameshieldLicensePath;
        string gameshieldExePath;
        string gameshieldOutputPath;
        string gameshieldOutputExePath;

        bool gameshieldWrap;
        bool createInstaller;

        string patchDataDirectory;

        string installerName;
        string installerIcon;
        string installerBuildDirectory;
        string installerPatchName;
        string installerWaitForProcess;

        [CategoryAttribute("Executables"), DescriptionAttribute("Base executable name before IronWrapping/Signing")]
        public string ExecutableName
        {
            get { return executableName; }
            set { executableName = value; }
        }

        [CategoryAttribute("Executables"), DescriptionAttribute("Path to base executable before IronWrapping/Signing")]
        public string ExecutablePath
        {
            get { return executablePath; }
            set { executablePath = value; }
        }

        [CategoryAttribute("Executables"), DescriptionAttribute("Final executable name that will be wrapped up in the patch")]
        public string FinalExecutableName
        {
            get { return finalExecutableName; }
            set { finalExecutableName = value; }
        }

        [CategoryAttribute("Gameshield"), DescriptionAttribute("Gameshield LPJ file that is used to wrap the executable")]
        public string GameshieldLicenseProject
        {
            get { return gameshieldLicenseProject; }
            set { gameshieldLicenseProject = value; }
        }

        [CategoryAttribute("Gameshield"), DescriptionAttribute("Gameshield license file that the game loads ex. (MaxPayne3_EFIGS.ini)")]
        public string GameshieldLicenseFile
        {
            get { return gameshieldLicenseFile; }
            set { gameshieldLicenseFile = value; }
        }

        [CategoryAttribute("Gameshield"), DescriptionAttribute("Root path to where the Gameshield license folders reside")]
        public string GameshieldLicensePath
        {
            get { return gameshieldLicensePath; }
            set { gameshieldLicensePath = value; }
        }

        [CategoryAttribute("Gameshield"), DescriptionAttribute("Path to where the exe needs to go in order to wrap it")]
        public string GameshieldExePath
        {
            get { return gameshieldExePath; }
            set { gameshieldExePath = value; }
        }

        [CategoryAttribute("Gameshield"), DescriptionAttribute("Path to where the exe goes after it has been wrapped (set in the Gameshield project)")]
        public string GameshieldOutputPath
        {
            get { return gameshieldOutputPath; }
            set { gameshieldOutputPath = value; }
        }

        [CategoryAttribute("Gameshield"), DescriptionAttribute("Folder name for where the wrapped executable will be placed")]
        public string GameshieldOutputExePath
        {
            get { return gameshieldOutputExePath; }
            set { gameshieldOutputExePath = value; }
        }

        [CategoryAttribute("Gameshield"), DescriptionAttribute("Sets whether the executable needs to be wrapped in the build process")]
        public bool GameshieldWrap
        {
            get { return gameshieldWrap; }
            set { gameshieldWrap = value; }
        }

        [CategoryAttribute("Installer"), DescriptionAttribute("Sets whether the patch needs to be built into an installer")]
        public bool CreateInstaller
        {
            get { return createInstaller; }
            set { createInstaller = value; }
        }

        [CategoryAttribute("Installer"), DescriptionAttribute("Directory where the data resides that is to be built into the installer")]
        public string PatchDataDirectory
        {
            get { return patchDataDirectory; }
            set { patchDataDirectory = value; }
        }

        [CategoryAttribute("Installer"), DescriptionAttribute("Internal name of the installer")]
        public string InstallerName
        {
            get { return installerName; }
            set { installerName = value; }
        }

        [CategoryAttribute("Installer"), DescriptionAttribute("Icon file that will appear on the installer in explorer")]
        public string InstallerIcon
        {
            get { return installerIcon; }
            set { installerIcon = value; }
        }

        [CategoryAttribute("Installer"), DescriptionAttribute("Path to where the base installer scripts reside")]
        public string InstallerBuildDirectory
        {
            get { return installerBuildDirectory; }
            set { installerBuildDirectory = value; }
        }

        [CategoryAttribute("Installer"), DescriptionAttribute("Prefix of the patch to build. Version number will be appended to this name")]
        public string InstallerPatchName
        {
            get { return installerPatchName; }
            set { installerPatchName = value; }
        }

        [CategoryAttribute("Installer"), DescriptionAttribute("Process that we want to wait for if the install commandline arguments say to wait")]
        public string InstallerWaitForProcess
        {
            get { return installerWaitForProcess; }
            set { installerWaitForProcess = value; }
        }
    }

    public class DLCSettings
    {
        string packageName;
        string dataDirectory;
        string outputFolder;

        [CategoryAttribute("DLC"), DescriptionAttribute("Name of the exe that the tool builds out. This is also the folder name that is used to pull data from.")]
        public string PackageName
        {
            get { return packageName; }
            set { packageName = value; }
        }

        [CategoryAttribute("DLC"), DescriptionAttribute("Directory where the DLC data required to build resides. Uses PackageName to figure out the subfolder.")]
        public string DataDirectory
        {
            get { return dataDirectory; }
            set { dataDirectory = value; }
        }

        [CategoryAttribute("DLC"), DescriptionAttribute("Directory where the resulting installer will be written to")]
        public string OutputFolder
        {
            get { return outputFolder; }
            set { outputFolder = value; }
        }
    }
    
}
