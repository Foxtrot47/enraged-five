;Max Payne 3 Patch Data Update Installer 1.0.0.0 
;Rockstar Games Toronto
;Developer: Colin Orr
;--------------------------------

SetCompressor /SOLID lzma

;Include Modern UI
!include "MUI2.nsh"
!include "Global\Includes\CompileInfo.nsh"
!include "Global\Includes\Lang.nsh"
!include "Global\Includes\Global.nsh"
!include "Global\Includes\FileList.nsh"

!define WNDCLASS "grcWindow"
!define WNDTITLE $(^Name)

!define COMPANY		"Rockstar Games"            
!define REGKEY		"SOFTWARE\${COMPANY}\$(^Name)"
!define REGSTRING	"PatchVersion"
!define URL			"http://www.rockstargames.com/"

;!addplugindir   "X:\payne\tools\installer\project\installer_Patch\Global\Plugins"

OutFile "${FILENAME}" 
Icon "${IconLoc}"

Var dWelcome
Var InstallFolder
Var HControl

;Request application privileges for Windows Vista
RequestExecutionLevel admin

!macro IsUserAdmin RESULT
    !define Index "Line${__LINE__}"
    StrCpy ${RESULT} 0
    System::Call '*(&i1 0,&i4 0,&i1 5)i.r0'
    System::Call 'advapi32::AllocateAndInitializeSid(i r0,i 2,i 32,i 544,i 0,i 0,i 0,i 0,i 0,i 0,*i .R0)i.r5'
    System::Free $0
    System::Call 'advapi32::CheckTokenMembership(i n,i R0,*i .R1)i.r5'
    StrCmp $5 0 ${Index}_Error
    StrCpy ${RESULT} $R1
    Goto ${Index}_End
    ${Index}_Error:
    StrCpy ${RESULT} -1
    ${Index}_End:
    System::Call 'advapi32::FreeSid(i R0)i.r5'
    !undef Index
!macroend

;--------------------------------
;Interface Settings
;!define MUI_HEADERIMAGE
;!define MUI_HEADERIMAGE_BITMAP "Z:\tools_release\GTA4PatchInstaller\EFLC\BMP\GTAIVHeader.bmp"                                ; optional
!define MUI_ABORTWARNING
!define MUI_LICENSEPAGE_CHECKBOX 

;--------------------------------
;Pages
Page custom CUST_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE $(MUILicense)
!insertmacro MUI_PAGE_INSTFILES

;--------------------------------
;Reserve Files
;If you are using solid compression, files that are required before
;the actual installation should be stored first in the data block,
;because this will make your installer start faster.

!insertmacro MUI_RESERVEFILE_LANGDLL

Function CUST_PAGE_WELCOME                                                                                      ; Welcome Page
    !insertmacro MUI_HEADER_TEXT $WelcomeHead1 $WelcomeHead2
    
    nsDialogs::Create /NOUNLOAD 1018
    pop $dWelcome
    
    ${If} $dWelcome == error
        MessageBox MB_ICONSTOP|MB_OK $WelcomeError
        Abort
    ${EndIf}
     
    GetDlgItem $0 $HWNDPARENT 1                                                                             ; Gets current window
    nsDialogs::CreateControl /NOUNLOAD STATIC ${WS_VISIBLE}|${WS_CHILD} 0 0 0 85% 60 $WelcomeMsg2         ;|${WS_CLIPSIBLINGS|${SS_CENTER} 145
    pop $HControl
    SetCtlColors $HControl 0x000000  ;transparent
   
    nsDialogs::Show
    
    Pop $3  
    StrCmp $3 "Cancel" 0 +2                                                             ; Catched the User Cancel.
    Quit

    FindWindow $0 "${WNDCLASS}" "${WNDTITLE}"                                           ; Runniing check
    ${If} $0 > 0
        ;MessageBox MB_ICONSTOP|MB_OK $RunError
        Quit
    ${EndIf}
    
FunctionEnd
;--------------------------------
;Installer Sections

Section
    ClearErrors
	StrCpy $0 $InstallFolder

	;----------------------------------------------------------------------------------------------------
; Checks he last character in the Reg for thr InstallFolder and Makes sure that the last Char is a \.
   
    StrLen $R8 $0  
    System::Int64Op $R8 - 1
    Pop $R7
    StrCpy $R9 $0 $R8 $R7  
    StrCmp $R9 "\" DontAdd_Slash Add_Slash 

Add_Slash:
    StrCpy $0 "$0\"         

DontAdd_Slash:

    SetOutPath $InstallFolder                                                           ; Location to install too
        
    Push $InstallFolder
    Call AddUpdateFiles
    Pop $1

	WriteRegStr HKLM "${REGKEY}" "${REGSTRING}" "${VERSION}" 

    ${If} $1 = 9999
        MessageBox MB_ICONSTOP|MB_OK $TransferError
    ${EndIf}
SectionEnd

;--------------------------------
;Installer Functions

Function .onInit
  BringToFront
; Check if already running
; If so don't open another but bring to front
  System::Call "kernel32::CreateMutexA(i 0, i 0, t '$(^Name)') i .r0 ?e"
  Pop $0
  StrCmp $0 0 launch
   StrLen $0 "$(^Name)"
   IntOp $0 $0 + 1
  loop:
    FindWindow $1 '#32770' '' 0 $1
    IntCmp $1 0 +5
    System::Call "user32::GetWindowText(i r1, t .r2, i r0) i."
    StrCmp $2 "$(^Name)" 0 loop
    System::Call "user32::ShowWindow(i r1,i 9) i."         ; If minimized then maximize
    System::Call "user32::SetForegroundWindow(i r1) i."    ; Bring to front
    Abort
  launch:

    SetOutPath $TEMP															; Used to set the Skin for the Installer
    InitPluginsDir																; Sets up the Plugins DIR usually System Temp DIR
 
; Language Functions: Need to find what language to set for the insteller.
    Call SelectLanguage															 
    Push $LANGUAGE
    Call SetLangText															
     
    strcpy $IsLanguageArgsSet 0
	Call CheckForLanguageArgs

	${If} $IsLanguageArgsSet = 0
		Call SelectLanguage														 
	${EndIf}

	Push $LANGUAGE
	Call SetLangText															
	
    ReadRegStr $0 HKLM "${REGKEY}" "InstallFolder"								; Check Reg
        
    ${If} $0 == ""
        MessageBox MB_ICONSTOP|MB_OK $WelcomeError
        Abort
    ${EndIf}
	
	StrCpy $InstallFolder $0

	Call IsSilent															; Checks to see if It need to run silently 
	;SetSilent silent
	
	Call CheckForProcArgs													; Checks to make sure processes are shut down, Currently hard coded.
	pop $0

    SetOutPath "$TEMP\ROCKSTAR"                                             ; Sets the temp file dump location
    InitPluginsDir                                                          ; Sets up the Plugins DIR usually System Temp DIR

    !insertmacro IsUserAdmin $0                                             ; Admin user check
    ${If} $0 < 1
		MessageBox MB_ICONSTOP|MB_OK $AdminErrPatch
		SetErrorLevel 944
		Abort
   ${EndIf}
 
    InitPluginsDir
FunctionEnd


Function .onInstSuccess
	${If} $IsWaitForProcSet > 0
		Exec "$InstallFolder\$STR_PROC"
		ThreadTimer::Stop
	${EndIf}
FunctionEnd

