To build the Social Club installer, you need to have NSIS installed.
http://code.google.com/p/unsis/downloads/detail?name=nsis-2.46.3-Unicode-setup.exe&can=2&q=

BuildPatch.exe "[Path to MakeNSIS.exe]" "[Path to Data]" "[Path where to make the installer]"

After installing NSIS, MakeNSIS.exe is usually located in \Program Files (x86)\NSIS\Unicode\MakeNSIS.exe.


