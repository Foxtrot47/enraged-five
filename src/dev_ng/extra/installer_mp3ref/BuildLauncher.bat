
@SET _name="Grand Theft Auto V"
@SET _icon="X:\payne\tools\installer\project\installer_test_gta5\Global\IMG\GTAV_logo.ico"

@SET _nsisTool="C:\\Program Files (x86)\\NSIS\\Unicode\\MakeNSIS.exe"
@SET _dataLoc="X:\payne\tools\installer\project\installer_test_gta5\LAUNCHER"
@SET _buildLoc="X:\payne\tools\installer\project\installer_test_gta5"
@SET _patchName="GTAV_Launcher"
@SET _Version="1.0.0.1"

@SET _WaitProcees="GTAVLauncher_debug.exe"

BuildPatch.exe %_nsisTool% %_name% %_icon% %_dataLoc% %_buildLoc% %_patchName% %_Version% %_WaitProcees%

::pause

