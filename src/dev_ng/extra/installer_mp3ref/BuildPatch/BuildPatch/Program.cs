﻿using System;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;

namespace BuildPatch
{
    class Program
    {
        private static int intCount = 0;
        private static StreamWriter StreamFile;

        static void Main(string[] args)
        {
            if (Directory.Exists(args[3]))
            {
                WriteNSISFileINFO(args[3], args[1], args[2], args[6], args[5], args[4], args[7]);
                BuildFileList(args[3]);
                //BuildDeleteFileList(args[1]);
            }

            if (File.Exists(args[0]))
            {
                MakeNSISProject(args[0]);
            }
          }

        static void WriteCompileHeader(String sDataLoc, string strBuildLoc)
        {

            foreach (string strFile in Directory.GetFiles(sDataLoc))
            {
                Console.WriteLine("File " + strFile.ToString());
                
                string[] strDll = strFile.Split('\\');
                string strFileName = strDll[strDll.Length-1];

                if ( string.Compare( strFileName.ToLower(), "socialclub.dll" ) == 0 )
                {
                    FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(strFile);

                    if ( myFileVersionInfo.FileVersion != null )
                    {
                        string[] strVersion = myFileVersionInfo.FileVersion.Split(',');
                        //WriteNSISFileINFO(sDataLoc, strVersion, strBuildLoc);
                    }
                }
            }
        }

        static void MakeNSISProject(string strProject)
        {
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.FileName = strProject;

            proc.StartInfo.Arguments = "X:\\payne\\tools\\installer\\project\\installer_Patch\\MP3_PATCH.nsi";

            proc.Start();
            proc.WaitForExit();
            proc.Close();
        }

        static void WriteNSISFileINFO(string strDataLoc, string sName, string sIcon, string strVer, string strNAME, string strBuildLoc, string strWaitProc)
        {
            string NSISFILENAME = "X:\\payne\\tools\\installer\\project\\installer_Patch\\Global\\Includes\\CompileInfo.nsh";
            
            if (!Directory.Exists(strBuildLoc))
                Directory.CreateDirectory(strBuildLoc);
            
            if (!File.Exists(NSISFILENAME))
                File.Create(NSISFILENAME);

            File.SetAttributes(NSISFILENAME, FileAttributes.Normal);
            StreamWriter CompileInfoFile = new StreamWriter(NSISFILENAME, false, System.Text.Encoding.ASCII, 512);

            CompileInfoFile.Write("# General Symbol Definitions\n");

            Version PatchVer = new Version(strVer);

            //CompileInfoFile.Write("!define FILENAME \"" + strBuildLoc + "\\MP3_Launcher_" + PatchVer.Major + "_" + PatchVer.Minor + "_" + PatchVer.Build + "_" + PatchVer.Revision + ".exe\"\n");

            CompileInfoFile.Write("Name \"" + sName + "\"\n");
            CompileInfoFile.Write("!define IconLoc \"" + sIcon + "\"\n\n");

            CompileInfoFile.Write("!define FILENAME \"" + strBuildLoc + "\\" + strNAME + "_" + PatchVer.Major + "_" + PatchVer.Minor + "_" + PatchVer.Build + "_" + PatchVer.Revision + ".exe\"\n");
            CompileInfoFile.Write("!define VERSION ");
            CompileInfoFile.Write(strVer);
            CompileInfoFile.Write("\n!define DATALOC \"" + strDataLoc + "\"\n");
            CompileInfoFile.Write("\n!define WAITPROC \"" + strWaitProc + "\"\n");
            CompileInfoFile.Close();
        }

        static void BuildFileList(string sDataLoc)
        {

            string FILELISTNAME = "X:\\payne\\tools\\installer\\project\\installer_Patch\\\\Global\\Includes\\FileList.nsh";

            File.SetAttributes(FILELISTNAME, FileAttributes.Normal);
            StreamFile = new StreamWriter(FILELISTNAME, false, System.Text.Encoding.Unicode, 512);
  
            StreamFile.WriteLine("Function AddUpdateFiles");
            StreamFile.WriteLine("Pop $0\n");
            StreamFile.WriteLine("SetOverwrite on\n");
            StreamFile.WriteLine("SetOutPath $0");


            foreach (string f in Directory.GetFiles( sDataLoc, "*.*"))
            {
                StreamFile.WriteLine("!insertmacro CRCFile $1 $0 \"" + f.Substring(sDataLoc.Length) + "\" \"" + sDataLoc + "\"");

                intCount += 1;
            }

            StreamFile.WriteLine("\n");
            GenerateFileList(sDataLoc, sDataLoc);

            StreamFile.WriteLine("SetOutPath $0\n");
            StreamFile.WriteLine("Push $1\n");
            StreamFile.WriteLine("FunctionEnd");

            StreamFile.Flush();
            StreamFile.Close();
        }

        static void GenerateFileList(string sDir, string sParent)
        {
            int initCount;

            try
            {
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    initCount = intCount;

                    foreach (string f in Directory.GetFiles(d, "*.*"))
                    {
                        FileStream fsFile = File.OpenRead(f);

                        intCount += 1;

                        if (intCount == initCount + 1)
                        {
                            StreamFile.WriteLine("SetOutPath \"$0" + d.Substring(sParent.Length) + "\"");
                        }
                        //StreamFile.WriteLine("!insertmacro CRCFile $1 $0 \"" + d.Substring(sParent.Length) + "\\" + f.Substring(d.Length + 1) + "\" " + "\"" + d + "\"");
                        StreamFile.WriteLine("!insertmacro CRCFile $1 $0 \"\\" + f.Substring(d.Length + 1) + "\" " + "\"" + d + "\"");

                        fsFile.Close();
                    }

                    StreamFile.WriteLine("\n");

                    GenerateFileList(d, sParent);
                }
            }
            catch (System.Exception excpt)
            {
               // AddText(excpt.Message, true);
            }
        }

        static void BuildDeleteFileList(string sDataLoc)
        {

            string FILELISTNAME = "X:\\payne\\tools\\installer\\project\\installer_Patch\\\\Global\\Includes\\DeleteFileList.nsh";

            File.SetAttributes(FILELISTNAME, FileAttributes.Normal);
            StreamFile = new StreamWriter(FILELISTNAME, false, System.Text.Encoding.Unicode, 512);

            StreamFile.WriteLine("Function un.AddDeleteFiles");
            StreamFile.WriteLine("Pop $0\n");

            foreach (string f in Directory.GetFiles(sDataLoc, "*.*"))
            {
                StreamFile.WriteLine("!insertmacro DeleteFile \"" + f.Substring(sDataLoc.Length) + "\" $0"  );

                intCount += 1;
            }

            StreamFile.WriteLine("\n");
            GenerateDeleteFileList(sDataLoc, sDataLoc);
            StreamFile.WriteLine("FunctionEnd");

            StreamFile.Flush();
            StreamFile.Close();
        }

        static void GenerateDeleteFileList(string sDir, string sParent)
        {
            int initCount;

            try
            {
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    initCount = intCount;

                    foreach (string f in Directory.GetFiles(d, "*.*"))
                    {
                        FileStream fsFile = File.OpenRead(f);

                        intCount += 1;
                        //StreamFile.WriteLine("!insertmacro CRCFile $1 $0 \"" + d.Substring(sParent.Length) + "\\" + f.Substring(d.Length + 1) + "\" " + "\"" + d + "\"");
                        StreamFile.WriteLine("!insertmacro DeleteFile \"\\" + f.Substring(d.Length + 1) + "\" \"$0" + d.Substring(sParent.Length) + "\"");

                        fsFile.Close();
                    }

                    StreamFile.WriteLine("\n");

                    GenerateDeleteFileList(d, sParent);
                }
            }
            catch (System.Exception excpt)
            {
                // AddText(excpt.Message, true);
            }
        }
    }
}
