!include LogicLib.nsh                       ;Used for Syntax

!define sysOSVERSIONINFO '(i, i, i, i, i, &t128) i'
!define sysGetVersionEx 'kernel32::GetVersionExA(i) i'
!define sysVER_PLATFORM_WIN32_NT 2

!macro MUI_LANGUAGE_MOD LANGUAGE LANG_NLF

  ;Include a language
  !verbose push
  !verbose ${MUI_VERBOSE}

  !insertmacro MUI_INSERT

  LoadLanguageFile "${LANG_NLF}"				;"${NSISDIR}\Contrib\Language files\${LANGUAGE}.nlf"

  ;Add language to list of languages for selection dialog
  !ifndef MUI_LANGDLL_LANGUAGES
    !define MUI_LANGDLL_LANGUAGES "'${LANGFILE_${LANGUAGE}_NAME}' '${LANG_${LANGUAGE}}' "
    !define MUI_LANGDLL_LANGUAGES_CP "'${LANGFILE_${LANGUAGE}_NAME}' '${LANG_${LANGUAGE}}' '${LANG_${LANGUAGE}_CP}' "
  !else
    !ifdef MUI_LANGDLL_LANGUAGES_TEMP
      !undef MUI_LANGDLL_LANGUAGES_TEMP
    !endif
    !define MUI_LANGDLL_LANGUAGES_TEMP "${MUI_LANGDLL_LANGUAGES}"
    !undef MUI_LANGDLL_LANGUAGES

    !ifdef MUI_LANGDLL_LANGUAGES_CP_TEMP
      !undef MUI_LANGDLL_LANGUAGES_CP_TEMP
    !endif
    !define MUI_LANGDLL_LANGUAGES_CP_TEMP "${MUI_LANGDLL_LANGUAGES_CP}"
    !undef MUI_LANGDLL_LANGUAGES_CP

    !define MUI_LANGDLL_LANGUAGES "'${LANGFILE_${LANGUAGE}_NAME}' '${LANG_${LANGUAGE}}' ${MUI_LANGDLL_LANGUAGES_TEMP}"
    !define MUI_LANGDLL_LANGUAGES_CP "'${LANGFILE_${LANGUAGE}_NAME}' '${LANG_${LANGUAGE}}' '${LANG_${LANGUAGE}_CP}' ${MUI_LANGDLL_LANGUAGES_CP_TEMP}"
  !endif

  !verbose pop
!macroend

;Languages
!insertmacro MUI_LANGUAGE_MOD "English" "Global\LangFiles\English.nlf"         ;first language is the default language
!insertmacro MUI_LANGUAGE_MOD "French" "Global\LangFiles\French.nlf"
!insertmacro MUI_LANGUAGE_MOD "Italian" "Global\LangFiles\Italian.nlf"
!insertmacro MUI_LANGUAGE_MOD "German" "Global\LangFiles\German.nlf"
!insertmacro MUI_LANGUAGE_MOD "Spanish" "Global\LangFiles\Spanish.nlf"
!insertmacro MUI_LANGUAGE_MOD "Russian" "Global\LangFiles\Russian.nlf"
;!insertmacro MUI_LANGUAGE_MOD "Japanese" "Global\LangFiles\Japanese.nlf"
!insertmacro MUI_LANGUAGE_MOD "Polish" "Global\LangFiles\Polish.nlf" 
!insertmacro MUI_LANGUAGE_MOD "PortugueseBR" "Global\LangFiles\PortugueseBR.nlf" 

LicenseLangString MUILicense  ${LANG_ENGLISH} "Global\EULA\English\License.rtf" 
LicenseLangString MUILicense  ${LANG_ITALIAN} "Global\EULA\Italian\License.rtf" 
LicenseLangString MUILicense  ${LANG_FRENCH} "Global\EULA\French\License.rtf" 
LicenseLangString MUILicense  ${LANG_GERMAN} "Global\EULA\German\License.rtf" 
LicenseLangString MUILicense  ${LANG_SPANISH} "Global\EULA\Spanish\License.rtf"
;LicenseLangString MUILicense  ${LANG_JAPANESE} "Global\EULA\Japanese\License.rtf"
LicenseLangString MUILicense  ${LANG_RUSSIAN} "Global\EULA\Russian\License.rtf"
LicenseLangString MUILicense  ${LANG_POLISH} "Global\EULA\Polish\License.rtf"
LicenseLangString MUILicense  ${LANG_PORTUGUESEBR} "Global\EULA\PortugueseBR\License.rtf"

; Set name using the normal interface (Name command)
;LangString Name ${LANG_ENGLISH} "English"
;LangString Name ${LANG_ITALIAN} "Italiano"
;LangString Name ${LANG_FRENCH} "Français"
;LangString Name ${LANG_GERMAN} "Deutsch"
;LangString Name ${LANG_SPANISH} "Español"
;LangString Name ${LANG_JAPANESE} "日本語"
;LangString Name ${LANG_RUSSIAN} "Русско"

var AdminErrorRGSC		
var AdminErrPatch  
var AdminErrorDLC
var OSError
var RGSCError
var GTAIVError
var WelcomeHead1
var WelcomeHead2
var WelcomeMsg1
Var WelcomeError
var WelcomeMsg2
var InstallHead
var InstallMsg
var RunError
var InitINSTALL
var RunUpdateErr
var AppErr
var NewerVerErr

var LangTitle 
var LangSelect
var AbortWarning
var TransferError

var OptHeader1
var OptHeader2
var ModifyHead					
var ModifyText	
var RemoveHead  
var RemoveText  
var ModHeader1
var ModHeader2

; Checks the System OS
Function SetUILanguage
    Push $R0
    Push $R1
    Push $R2
    System::Call 'kernel32::GetUserDefaultUILanguage() i.r10'
    StrCmp $R0 "error" OldFashionedWay Finish
OldFashionedWay:
    System::Call '*${sysOSVERSIONINFO}(148,0,0,0,0,"") .r10'
    System::Call '${sysGetVersionEx}(r10)'
    System::Call "*$R0(i ., i ., i ., i ., i .r11)"
    System::Free $R0
    IntCmp $R1 ${sysVER_PLATFORM_WIN32_NT} PlatformIsNT
    ; We're running on DOS-based Windows
    StrCpy $R0 "Control Panel\desktop\ResourceLocale"
    StrCpy $R1 ""
    GoTo GetLCIDFromHKCU
PlatformIsNT:
    ; We're running on Windows NT
    StrCpy $R0 ".DEFAULT\Control Panel\International"
    StrCpy $R1 "Locale"
    GoTo GetLCIDFromHKU
GetLCIDFromHKU:
    ReadRegStr $R2 HKU $R0 $R1
    GoTo GetLangIDFromLCID
GetLCIDFromHKCU:
    ReadRegStr $R2 HKCU $R0 $R1
GetLangIDFromLCID:
    StrCpy $R2 "0x$R2"
    IntOp $R0 $R2 & 0xffff
    ; Get just the primary LANGID (NSIS messes it up otherwise)
    IntOp $R0 $R0 & 0x3ff ; remove SUBLANGID
    IntOp $R0 $R0 | 0x400 ; add SUBLANG_DEFAULT
Finish:
    StrCpy $LANGUAGE $R0
    Pop $R2
    Pop $R1
    Pop $R0
FunctionEnd

Function CheckSupportedLang()
    ${SWITCH} $LANGUAGE
        ${Case} 1033            ;English
            Push 1033
            ${Break}
        ${Case} 1036            ;French
            Push 1036
            ${Break}
        ${Case} 1040            ;Italian
            Push 1040
            ${Break}
        ${Case} 3082            ;Spanish
            Push 3082
            ${Break}
        ${Case} 1031            ;German
            Push 1031
            ${Break}
        ${Case} 1049            ;Russian
            Push 1049
            ${Break}
        ;${Case} 1041            ;Japan
        ;    Push 1041   
        ;    ${Break}
		${Case} 1045            ;Polish
            Push 1045   
            ${Break}
		${Case} 1046            ;PortugueseBR
            Push 1046   
            ${Break}
        ${Default}
            Push "error"
            ${Break}
    ${EndSwitch} 
    
FunctionEnd


; Sets UI language. 
; First Check	: Checks the registry for installed language.
; Second Check	: Checks the System OS 
; Third check	: If the language OS lang isn't supported then the lang select dialogue will be used.

Function SelectLanguage                                 
	ReadRegStr $0 HKLM "SOFTWARE\Rockstar Games\Rockstar Games Social Club" "InstallLang"

	${If} $0 == ""
		Call SetUILanguage				; Checks the System OS
	${Else}
		StrCpy $LANGUAGE $0 ; -10 15
		goto SelectLanguageFinished
	${EndIf}
	
	Call CheckSupportedLang()			; Checks The supported laguages.
	Pop $R0
		
	StrCmp $R0 "error" SelectALang SelectLanguageFinished 

SelectALang:
	Push ""
	Push ${LANG_ENGLISH}
	Push "English"
	Push ${LANG_ITALIAN}
	Push "Italiano"
	Push ${LANG_FRENCH}
	Push "Français"
	Push ${LANG_GERMAN}
	Push "Deutsch"
	Push ${LANG_SPANISH}
	Push "Español"
	;Push ${LANG_JAPANESE}
	;Push "日本語"
	Push ${LANG_RUSSIAN}
	Push "Русский"
	Push ${LANG_POLISH}
	Push "Polski"
	Push ${LANG_PORTUGUESEBR}
	Push "Português Brasileiro"
	
	Push A ; A means auto count languages
		   ; for the auto count to work the first empty push (Push "") must remain

	Push $LANGUAGE
	Call SetLangSelectText							;Set the lang list for the selection window.
	 
	LangDLL::LangDialog $LangTitle $LangSelect		;Shows language select window.

    Pop $LANGUAGE

    StrCmp $LANGUAGE "cancel" 0 +2
        Abort

SelectLanguageFinished:
FunctionEnd

Function SetLangSelectText
    pop $0
        
    ${SWITCH} $0
        ${Case} 1033
            Call AddLangSelectEng        
            ${Break}
        ${Case} 1036
            Call AddLangSelectFre        
            ${Break}
        ${Case} 1040
            Call AddLangSelectIta        
            ${Break}
        ${Case} 3082
            Call AddLangSelectSpa       
            ${Break}
        ${Case} 1031
            Call AddLangSelectGer        
            ${Break}
;	${Case} 1041
;            Call AddLangSelectJap        
;            ${Break}
        ${Case} 1049
            Call AddLangSelectRus        
            ${Break}
		${Case} 1045
            Call AddLangSelectPol        
            ${Break}
		${Case} 1046
            Call AddLangSelectBrz        
            ${Break}
        ${Default}
            Call AddLangSelectEng        
        ${Break}
    ${EndSwitch}
FunctionEnd

; Sets the required String for the installer
Function SetLangText
    pop $0
     
    ${SWITCH} $0
        ${Case} 1033
            Call AddEnglish        
            ${Break}
        ${Case} 1036
            Call AddFrench        
            ${Break}
        ${Case} 1040
            Call AddItalian        
            ${Break}
        ${Case} 3082
           Call AddSpanish        
            ${Break}
        ${Case} 1031
            Call AddGerman        
            ${Break}
;	${Case} 1041
 ;           Call AddJapanese        
 ;           ${Break}
        ${Case} 1049
            Call AddRussian        
            ${Break}
		${Case} 1045
            Call AddPolish        
            ${Break}
		${Case} 1046
            Call AddPortuguese        
            ${Break}
        ${Default}
            Call AddEnglish        
            ${Break}
    ${EndSwitch}
FunctionEnd


; Sets the language for the uninstaller, This project doesn't uninstall
Function un.SetLangText
    pop $0
     
    ${SWITCH} $0
        ${Case} 1033
            Call un.AddEnglish        
            ${Break}
        ${Case} 1036
            Call un.AddFrench        
            ${Break}
        ${Case} 1040
            Call un.AddItalian        
            ${Break}
        ${Case} 3082
           Call un.AddSpanish        
            ${Break}
        ${Case} 1031
            Call un.AddGerman        
            ${Break}
;		${Case} 1041
;           Call un.AddJapanese        
;           ${Break}
        ${Case} 1049
            Call un.AddRussian        
            ${Break}
		${Case} 1045
            Call un.AddPolish        
            ${Break}
		${Case} 1046
            Call un.AddPortuguese        
            ${Break}
        ${Default}
            Call un.AddEnglish        
            ${Break}
    ${EndSwitch}
FunctionEnd

Function AddLangSelectEng
    StrCpy $LangTitle  "Installer Language" 
    StrCpy $LangSelect "Please select the language of the installer."
FunctionEnd

Function AddEnglish
    StrCpy $AdminErrorRGSC "$(^Name) requires administrator privileges to install.$\nPlease log in as Administrator and restart this application."
	StrCpy $AdminErrPatch  "$(^Name) updater requires administrator privileges to install.$\nPlease log in as Administrator and restart this application."
	StrCpy $AdminErrorDLC  "$(^Name) download installer requires administrator privileges.$\nPlease log in as Administrator and restart this application."

    StrCpy $OSError     "Your system is incompatible with $(^Name)."
    StrCpy $WelcomeHead1 "Welcome."
    StrCpy $WelcomeHead2 "This wizard will guide you through the installation of $(^Name). "
	StrCpy $WelcomeMsg1 "Welcome to the $(^Name) update installer."
    StrCpy $WelcomeMsg2 "This will install the latest features for $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Installing Files."
    StrCpy $InstallMsg  "Installing $(^Name) Files."
    StrCpy $RunError    "$(^Name) is currently running.$\n You Must close the application in order to complete this process."
    StrCpy $InitINSTALL  "Initializing $(^Name) Installer."
    StrCpy $WelcomeError "Error: Tying to run $(^Name) Installer."
    StrCpy $AbortWarning "Are you sure you want to quit $(^Name) Setup?"
    StrCpy $TransferError "One of the extracted files failed to copy to your system.$\nThis file is required in order to run $(^Name) correctly.$\nTo show the error, please click Show Details or$\nvisit www.rockstargames.com/support for further instruction."
    
    StrCpy $RunUpdateErr "$(^Name) is running and can not be Updated. Please shut down all Rockstar Game Applications."
    StrCpy $NewerVerErr  "A newer version $0 installed for $(^Name). Exiting installation."
FunctionEnd

Function un.AddEnglish
	StrCpy $WelcomeHead1 "Welcome."
    StrCpy $WelcomeHead2 "This wizard will guide you through the installation of $(^Name)."
	StrCpy $RunError     "$(^Name) is currently running.$\n You must close the application in order to complete this process."   
    StrCpy $RunUpdateErr "$(^Name) is running and can not be updated. Please shut down all Rockstar Game Applications."
    StrCpy $AppErr       "All Rockstar Applications must be removed before removing $(^Name)"
	
	StrCpy $OptHeader1	 "Welcome to $(^Name) setup maintenance program."
	StrCpy $OptHeader2	 "This program lets you modify the current installation. Click one of the options below, and the click Next to proceed."
	StrCpy $ModifyHead	 "Modify."					
	StrCpy $ModifyText	 "Select new program features to add or select currently installed features to remove."
	StrCpy $RemoveHead   "Remove."
	StrCpy $RemoveText   "Remove all installed features."
	StrCpy $ModHeader1	 "Select Installation Options for $(^Name)." 
	StrCpy $ModHeader2   "Select the features you want to retain, and deselect the features you want to remove."

FunctionEnd

Function AddLangSelectFre
    StrCpy $LangTitle   "Langue du programme d'installation" 
    StrCpy $LangSelect  "Veuillez sélectionner la langue du programme d'installation."
FunctionEnd

Function AddFrench
    StrCpy $AdminErrorRGSC "Pour installer le $(^Name), vous devez disposer de privilèges d'administrateur.$\nVeuillez vous connecter en tant qu'administrateur et redémarrer cette application."
	StrCpy $AdminErrPatch "Pour installer le logiciel de mise à jour de $(^Name), vous devez disposer de privilèges d'administrateur.$\nVeuillez vous connecter en tant qu'administrateur et redémarrer cette application."
	StrCpy $AdminErrorDLC "Pour utiliser le logiciel d'installation de contenus téléchargeables de $(^Name), vous devez disposer de privilèges d'administrateur.$\nVeuillez vous connecter en tant qu'administrateur et redémarrer cette application."

    StrCpy $OSError     "Votre système est incompatible avec $(^Name)."
    StrCpy $RGSCError   "Erreur : impossible de trouver le Rockstar Games Social Club.$\nAssurez-vous d'avoir bien installé le Rockstar Games Social Club et qu'il fonctionne correctement."
    StrCpy $GTAIVError  "Erreur : impossible de trouver $(^Name). Assurez-vous d'avoir bien installé $(^Name) et qu'il fonctionne correctement."
    StrCpy $WelcomeHead1 "Bienvenue."
    StrCpy $WelcomeHead2 "Vous êtes sur le point d’installer le $(^Name) sur votre ordinateur."
    StrCpy $WelcomeMsg1 "Bienvenue dans le programme d'installation de mises à jour de $(^Name)."
    StrCpy $WelcomeMsg2 "Ce programme va installer les derniers composants de $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Installation des fichiers."
    StrCpy $InstallMsg  "Installation des fichiers de $(^Name)."
    StrCpy $RunError    "$(^Name) est déjà en cours d'utilisation.$\nVous devez fermer l'application pour que le programme de mise à jour soit pris en compte."
    StrCpy $InitINSTALL   "Initialisation du programme d'installation de $(^Name)."
    StrCpy $WelcomeError "Erreur : tentative d'accès au programme d'installation de $(^Name)."
    StrCpy $AbortWarning "Êtes-vous sûr de vouloir quitter l'installation de $(^Name) ?"
    StrCpy $TransferError "Echec de la copie de l'un des fichiers sur votre ordinateur.$\nCe fichier est nécessaire pour le bon fonctionnement de $(^Name).$\nPour afficher l'erreur, veuillez cliquer sur 'Plus d'infos' ou visitez www.rockstargames.com/support pour de plus amples informations."
    
    StrCpy $RunUpdateErr "$(^Name) est en cours d'exécution. Mise à jour impossible. Veuillez fermer toutes les applications Rockstar Games."
    StrCpy $NewerVerErr  "Une nouvelle version ($0) a été installée pour $(^Name). L’installation va se terminer."
FunctionEnd

Function un.AddFrench
    StrCpy $WelcomeHead1 "Bienvenue."
	StrCpy $WelcomeHead2 "Cet assistant d'installation va vous guider pendant l'installation du produit $(^Name)."
	StrCpy $RunError     "$(^Name) est actuellement lancé.$\n Vous devez quitter l'application pour terminer ce processus."   
	StrCpy $RunUpdateErr "$(^Name) est lancé et ne peut pas être mis à jour. Veuillez quitter toutes les applications Rockstar Games."
	StrCpy $AppErr       "Toutes les applications Rockstar doivent être supprimées avant de pouvoir supprimer $(^Name)."
		
	StrCpy $OptHeader1	 "Bienvenue dans le programme de maintenance pour $(^Name)."
	StrCpy $OptHeader2	 "Ce programme vous permet de modifier l'installation actuelle. Cliquez sur l'une des options ci-après, puis sur Suivant pour continuer."
	StrCpy $ModifyHead	 "Modifier."					
	StrCpy $ModifyText	 "Sélectionnez de nouvelles fonctionnalités à ajouter ou des fonctionnalités déjà installées à supprimer."
	StrCpy $RemoveHead   "Supprimer."
	StrCpy $RemoveText   "Supprimez toutes les fonctionnalités installées."
	StrCpy $ModHeader1	 "Sélectionnez vos options d'installation pour $(^Name)." 
	StrCpy $ModHeader2   "Sélectionnez les fonctionnalités à conserver et désélectionnez les fonctionnalités à supprimer."
FunctionEnd

Function AddLangSelectIta
    StrCpy $LangTitle   "Lingua installer" 
    StrCpy $LangSelect  "Seleziona la lingua dell'installer."
FunctionEnd

Function AddItalian
    StrCpy $AdminErrorRGSC "Il Social Club di Rockstar Games necessita dei privilegi di amministratore per essere installato.$\nAccedi come Amministratore e riavvia l'applicazione."
	StrCpy $AdminErrPatch "L'updater di $(^Name) necessita dei privilegi di amministratore per essere installato.$\nAccedi come Amministratore e riavvia l'applicazione."
	StrCpy $AdminErrorDLC "Il programma d'installazione dei download di $(^Name) necessita dei privilegi di amministratore per essere installato.$\nAccedi come Amministratore e riavvia l'applicazione."

    StrCpy $OSError     "Il sistema è incompatibile con $(^Name)."
    StrCpy $RGSCError   "Errore: impossibile rilevare l'applicazione del Social Club di Rockstar Games.$\nAssicurati che l'applicazione del Social Club di Rockstar Games sia installata e funzioni correttamente."
    StrCpy $GTAIVError  "Errore: impossibile rilevare $(^Name). Assicurati che $(^Name) sia installato e funzioni correttamente."
    StrCpy $WelcomeHead1 "Benvenuto."
    StrCpy $WelcomeHead2 "Questo programma installerà $(^Name) nel vostro computer."
    StrCpy $WelcomeMsg1 "Benvenuto all'installazione dell'aggiornamento di $(^Name)."
    StrCpy $WelcomeMsg2 "Questo programma installerà le funzionalità più recenti di $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Installazione file in corso..."
    StrCpy $InstallMsg  "Installazione dei file di $(^Name)."
    StrCpy $RunError    "$(^Name) è in esecuzione.$\n È necessario chiudere l'applicazione per poter completare il processo."
    StrCpy $InitINSTALL   "Inizializzazione del programma di installazione di $(^Name)."
    StrCpy $WelcomeError "Errore nel tentativo di esecuzione dell'installer di $(^Name)."
    StrCpy $AbortWarning "Sei sicuro di voler interrompere l'installazione di $(^Name) ?"
    StrCpy $TransferError "Uno dei file estratti non è stato copiato correttamente sul tuo sistema.$\nQuesto file è necessario per far funzionare $(^Name) correttamente.$\nPer mostrare i dettagli dell'errore, clicca su 'Mostra Dettagli' o visita www.rockstargames.com/support per ulteriori informazioni."
    
    StrCpy $RunUpdateErr "$(^Name) è aperto e non può essere aggiornato. Chiudi tutte le applicazioni Rockstar Games."
    StrCpy $NewerVerErr  "Una nuova versione ($0) di $(^Name) è stata installata. Installazione in chiusura."
FunctionEnd

Function un.AddItalian
    StrCpy $WelcomeHead1 "Benvenuto."
	StrCpy $WelcomeHead2 "Questo programma ti guiderà nell'installazione di $(^Name)."
	StrCpy $RunError     "$(^Name) è attualmente in esecuzione.$\n Per concludere il processo, devi chiudere l'applicazione."   
	StrCpy $RunUpdateErr "$(^Name) è in esecuzione e non può essere aggiornato. Chiudi tutte le applicazioni di Rockstar."
	StrCpy $AppErr       "Per rimuovere $(^Name), occorre rimuovere tutte le applicazioni di Rockstar."
		
	StrCpy $OptHeader1	 "Benvenuto al programma di manutenzione di $(^Name)."
	StrCpy $OptHeader2	 "Questo programma ti permette di modificare lo stato dell'installazione attuale. Per continuare, clicca su una delle opzioni sotto e poi su Avanti."
	StrCpy $ModifyHead	 "Modifica."					
	StrCpy $ModifyText	 "Seleziona le nuove funzioni da aggiungere o seleziona quelle già installate da rimuovere."
	StrCpy $RemoveHead   "Rimuovi."
	StrCpy $RemoveText   "Rimuovi tutte le funzioni installate."
	StrCpy $ModHeader1	 "Seleziona le opzioni di installazione per $(^Name)." 
	StrCpy $ModHeader2   "Seleziona le funzioni che vuoi mantenere e deseleziona quelle che vuoi rimuovere."
FunctionEnd

Function AddLangSelectSpa
    StrCpy $LangTitle   "Idioma del instalador" 
    StrCpy $LangSelect  "Selecciona el idioma del instalador."
FunctionEnd

Function AddSpanish
    StrCpy $AdminErrorRGSC "Para instalar el Social Club de Rockstar Games se requieren privilegios de administrador.$\nInicia sesión como administrador y reinicia esta aplicación."
	StrCpy $AdminErrPatch "Para instalar el programa de actualización de $(^Name) se requieren privilegios de administrador.$\nInicia sesión como administrador y reinicia esta aplicación."
	StrCpy $AdminErrorDLC "El instalador de descargas de $(^Name) requiere privilegios de administrador.$\nInicia sesión como administrador y reinicia esta aplicación."

    StrCpy $OSError     "Tu sistema no es compatible con $(^Name)."
    StrCpy $RGSCError   "Error: no se encuentra Rockstar Games Social Club.$\nAsegúrate de que Rockstar Games Social Club esté instalado y en funcionamiento."
    StrCpy $GTAIVError  "Error: no se encuentra $(^Name). Asegúrate de que $(^Name) esté instalado y en funcionamiento."
    StrCpy $WelcomeHead1 "Bienvenido."
    StrCpy $WelcomeHead2 "Este programa instalará $(^Name) en su ordenador."
    StrCpy $WelcomeMsg1 "Bienvenido al instalador de la actualización de $(^Name)."
    StrCpy $WelcomeMsg2 "Este programa instalará las funciones más recientes de $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Instalando archivos."
    StrCpy $InstallMsg  "Instalando los archivos de $(^Name)."
    StrCpy $RunError    "$(^Name) está ejecutándose.$\nPara poder completar este procedimiento, debes cerrar la aplicación."
    StrCpy $InitINSTALL   "Inicializando el instalador de $(^Name)."
    StrCpy $WelcomeError "Error al intentar ejecutar el instalador de $(^Name)."
    StrCpy $AbortWarning "¿Está seguro de que desea salir de la instalación de $(^Name)?"
    StrCpy $TransferError "Error al copiar uno de los archivos extraídos a tu sistema.$\nSe requiere este archivo para ejecutar $(^Name) correctamente.$\nPara mostrar el error, haz clic en 'Ver detalles' o visita www.rockstargames.com/support para más información."
    
    StrCpy $RunUpdateErr "$(^Name) está abierto y no se puede actualizar. Cierra todas las aplicaciones de Rockstar Games."
	StrCpy $NewerVerErr  "Se ha instalado una nueva versión ($0) de $(^Name). Saliendo de la instalación."
FunctionEnd

Function un.AddSpanish
    StrCpy $WelcomeHead1 "Bienvenido."
	StrCpy $WelcomeHead2 "Este asistente te guiará durante la instalación de $(^Name)."
	StrCpy $RunError     "$(^Name) se está ejecutando.$\n Debes cerrar la aplicación para completar el proceso."   
	StrCpy $RunUpdateErr "$(^Name) se está ejecutando y no puede actualizarse. Cierra todas las aplicaciones de juego de Rockstar."
	StrCpy $AppErr       "Todas las aplicaciones de Rockstar deben eliminarse antes de quitar $(^Name)."
		
	StrCpy $OptHeader1	 "Bienvenido al programa de mantenimiento de $(^Name)."
	StrCpy $OptHeader2	 "Este programa te permite modificar la instalación actual. Haz clic en una de las opciones de debajo y luego haz clic en Siguiente para continuar."
	StrCpy $ModifyHead	 "Modificar."					
	StrCpy $ModifyText	 "Elige características nuevas para añadírselas al programa o características instaladas actualmente para eliminarlas."
	StrCpy $RemoveHead   "Borrar."
	StrCpy $RemoveText   "Borra todas las características instaladas."
	StrCpy $ModHeader1	 "Selecciona las opciones de instalación de $(^Name)." 
	StrCpy $ModHeader2   "Selecciona las características que quieras conservar y desactiva las que quieras quitar."
FunctionEnd

Function AddLangSelectGer
    StrCpy $LangTitle   "Installationssprache" 
    StrCpy $LangSelect  "Bitte wählen Sie die Installationssprache."
FunctionEnd

Function AddGerman
    StrCpy $AdminErrorRGSC "Rockstar Games Social Club benötigt Administratorrechte für die Installation.$\nBitte melden Sie sich als Administrator an und starten Sie diese Anwendung neu."
	StrCpy $AdminErrPatch "$(^Name) Updater benötigt Administratorrechte für die Installation.$\nBitte melden Sie sich als Administrator an und starten Sie diese Anwendung neu."
	StrCpy $AdminErrorDLC "$(^Name) Download-Installationsprogramm  benötigt Administratorrechte.$\nBitte melden Sie sich als Administrator an und starten Sie diese Anwendung neu."

    StrCpy $OSError     "Ihr System ist nicht kompatibel mit $(^Name)."
    StrCpy $RGSCError   "Fehler: Rockstar Games Social Club wurde nicht gefunden.$\nBitte stellen Sie sicher, dass Rockstar Games Social Club installiert ist und fehlerfrei läuft."
    StrCpy $GTAIVError  "Fehler: $(^Name) wurde nicht gefunden. Bitte stellen Sie sicher, dass $(^Name) installiert ist und fehlerfrei läuft."
    StrCpy $WelcomeHead1 "Willkommen."
    StrCpy $WelcomeHead2 "Dieser Assistent wird Sie durch die Installation von $(^Name) begleiten."
    StrCpy $WelcomeMsg1 "Willkommen zum $(^Name) Update Installer."
    StrCpy $WelcomeMsg2 "Die neuesten Features für $(^Name) werden installiert.$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Installiere Dateien."
    StrCpy $InstallMsg  "Installiere $(^Name) Dateien."
    StrCpy $RunError    "$(^Name) ist geöffnet.$\n Sie müssen die Anwendung schließen, um diesen Vorgang abzuschließen."
    StrCpy $InitINSTALL   "Initialisiere $(^Name) Installer."
    StrCpy $WelcomeError "Fehler: Versuche $(^Name) Installer zu starten."
    StrCpy $AbortWarning "Sind Sie sicher, dass Sie die Installation von $(^Name) abbrechen wollen?"
    StrCpy $TransferError "Eine der extrahierten Dateien konnte nicht auf ihr System kopiert werden.$\nDiese Datei wird benötigt um $(^Name) starten zu können.$\nUm sich den Fehler anzeigen zu lassen, klicken sie bitte auf Details anzeigen oder besuchen sie www.rockstargames.com/support für weitere Informationen und Anleitungen."
    
    StrCpy $RunUpdateErr "$(^Name) wird gerade ausgeführt und kann nicht aktualisiert werden. Bitte sämtliche Rockstar Games-Anwendungen beenden."
	StrCpy $NewerVerErr "Es ist bereits eine neuere Version ($0) von $(^Name) installiert. Die Installation wird jetzt beendet."
FunctionEnd

Function un.AddGerman
    StrCpy $WelcomeHead1 "Willkommen."
	StrCpy $WelcomeHead2 "Dieser Assistent führt Sie durch die Installation von $(^Name)."
	StrCpy $RunError     "$(^Name) läuft gerade.$\n Sie müssen das Programm beenden, um den Vorgang abzuschließen."   
	StrCpy $RunUpdateErr "$(^Name) läuft und kann nicht aktualisiert werden. Bitte sämtliche Rockstar-Programme beenden."
	StrCpy $AppErr       "Sämtliche Rockstar-Programme müssen vor dem Deinstallieren von $(^Name) entfernt werden."
		
	StrCpy $OptHeader1	 "Willkommen zum $(^Name) Setup-Wartungs-Programm."
	StrCpy $OptHeader2	 "Mit diesem Programm können Sie die aktuelle Installation modifizieren. Wählen Sie eine der unten aufgeführten Optionen und klicken Sie 'Weiter', um fortzufahren."
	StrCpy $ModifyHead	 "Modifizieren."					
	StrCpy $ModifyText	 "Fügen Sie neue Programm-Features hinzu oder entfernen Sie derzeit installierte Features."
	StrCpy $RemoveHead   "Entfernen."
	StrCpy $RemoveText   "Entfernen Sie sämtliche installierten Features."
	StrCpy $ModHeader1	 "Installationsoptionen für $(^Name) wählen." 
	StrCpy $ModHeader2   "Wählen Sie die Features, die Sie behalten möchten und wählen Sie die Features ab, die Sie entfernen möchten."
FunctionEnd

Function AddLangSelectRus
    StrCpy $LangTitle   "Язык установки" 
    StrCpy $LangSelect  "Пожалуйста, выберите язык установки."
FunctionEnd

Function AddRussian
    StrCpy $LANGUAGE 1049
    StrCpy $AdminErrorRGSC "Для установки Rockstar Games Social Club необходимы права администратора.$\nЗайдите как администратор системы и снова запустите это приложение."
	StrCpy $AdminErrPatch "Программе обновления $(^Name) необходимы права администратора.$\nЗайдите как администратор системы и снова запустите это приложение."
	StrCpy $AdminErrorDLC "Программе установки дополнений $(^Name) необходимы права администратора.$\nЗайдите как администратор системы и снова запустите это приложение."

    StrCpy $OSError     "Ваша система не поддерживается игрой $(^Name)."
    StrCpy $RGSCError   "Ошибка: Не обнаружено приложение Rockstar Games Social Club.$\nПожалуйста, проверьте что приложение Rockstar Games Social Club установлено и работает без сбоев."
    StrCpy $GTAIVError  "Ошибка: Не найдена игра $(^Name). Пожалуйста, проверьте, что игра $(^Name) установлена и работает без сбоев."
    StrCpy $WelcomeHead1 "Добро пожаловать."
    StrCpy $WelcomeHead2 "Этот мастер установки направит вас через установку $(^Name)."
    StrCpy $WelcomeMsg1 "Вас приветствует программа установки обновления для $(^Name)."
    StrCpy $WelcomeMsg2 "Эта программа установит последние исправления для $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Установка файлов."
    StrCpy $InstallMsg  "Выполняется установка файлов $(^Name)."
    StrCpy $RunError    "У вас запущена игра $(^Name).$\nДля выполнения установки вам необходимо закрыть игровое приложение."
    StrCpy $InitINSTALL   "Подготовка к установке $(^Name)."
    StrCpy $WelcomeError "Ошибка: Не удалось запустить экран установки $(^Name)."
    StrCpy $AbortWarning "Вы действительно хотите отменить установку $(^Name)?"
    StrCpy $TransferError "Один из извлеченных файлов не смог быть скопирован в вашу систему.$\nЭтот файл нужен для того чтобы корректно запустить $(^Name).$\nПожалуйста, щелкните по кнопке Детали или зайдите на www.rockstargames.com/support для дольнейших инструкций."
    
    StrCpy $RunUpdateErr "$(^Name) работает и не может быть обновлен. Пожалуйста, закройте все приложения Rockstar Games."
	StrCpy $NewerVerErr  "Найдена более новая версия ($0) игры $(^Name). Выход из установки."
FunctionEnd 

Function un.AddRussian
    StrCpy $WelcomeHead1 	"Добро пожаловать."
	StrCpy $WelcomeHead2 	"Эта программа поможет вам установить $(^Name)."
	StrCpy $RunError     	"$(^Name) в данный момент запущена. $\n Вы должны закрыть ее, чтобы завершить процесс."
	StrCpy $RunUpdateErr 	"$(^Name) в данный момент запущена, и ее нельзя обновить. Пожалуйста, закройте все приложения Rockstar."
	StrCpy $AppErr       	"Перед удалением $(^Name) необходимо удалить все приложения Rockstar"
		
	StrCpy $OptHeader1		"Добро пожаловать в программу установки $(^Name)."
	StrCpy $OptHeader2		"Это программа позволяет вам работать с установленным приложением. Выберите нужную опцию и нажмите кнопку ''Далее''."
	StrCpy $ModifyHead		"Изменить."
	StrCpy $ModifyText		"Выберите компоненты, которые хотите добавить или удалить."
	StrCpy $RemoveHead   	"Удалить."
	StrCpy $RemoveText   	"Удалить все установленные компоненты."
	StrCpy $ModHeader1		"Выберите желаемые компоненты установки $(^Name)."
	StrCpy $ModHeader2   	"Выберите компоненты, которые хотите добавить, и снимите галочку с тех, которые хотите удалить."
FunctionEnd 


Function AddLangSelectBRZ
    StrCpy $LangTitle  "Idioma do Instalador" 
    StrCpy $LangSelect "Por favor, selecione o idioma do instalador."
FunctionEnd

Function AddPortuguese
    StrCpy $AdminErrorRGSC "Rockstar Games Social Club requer privilégios de administrador para ser instalado.$\nPor favor faça o log-in como Administrador e reinicie este aplicativo."
	StrCpy $AdminErrPatch "O atualizador do $(^Name) requer privilégios de administrador para ser instaladol.$\nPor favor faça o log-in como Administrador e reinicie este aplicativo."
	StrCpy $AdminErrorDLC "O instalador do $(^Name) requer privilégios de administrador.$\nPor favor faça o log-in como Administrador e reinicie este aplicativo."

    StrCpy $OSError     "Seu sistema é incompatível com $(^Name)."
    StrCpy $WelcomeHead1 "Bem-vindo."
    StrCpy $WelcomeHead2 "Este assistente irá guiá-lo na instalação de $(^Name)."
    StrCpy $WelcomeMsg1 "Bem-vindo ao instalador de atualizações de $(^Name)."
    StrCpy $WelcomeMsg2 "Este irá instalar os recursos mais recentes de $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Instalando os arquivos."
    StrCpy $InstallMsg  "Instalando os arquivos de $(^Name)."
    StrCpy $RunError    "$(^Name) está sendo executado.$\n É necessário encerrar o aplicativo para completar este processo."
    StrCpy $InitINSTALL  "Inicializando o instalador de $(^Name)."
    StrCpy $WelcomeError "Erro: tentando executar o instalador de $(^Name)."
    StrCpy $AbortWarning "Tem certeza de que deseja sair da configuração de $(^Name)?"
    StrCpy $TransferError "Um dos arquivos extraídos falhou ao ser copiado em seu sistema.$\nEste arquivo é necessário para executar $(^Name) corretamente.$\nPara ver o erro, pressione Exibir Informações ou$\nacesse www.rockstargames.com/support para mais instruções."
    
    StrCpy $RunUpdateErr "$(^Name) está em andamento e não pode ser atualizado. Por favor, encerre todos os Aplicativos de Jogos da Rockstar."
    StrCpy $NewerVerErr  "A nova versão $0 para $(^Name) foi instalada. Encerrando a instalação."
FunctionEnd

Function un.AddPortuguese
	StrCpy $WelcomeHead1	"Bem-vindo."
    StrCpy $WelcomeHead2	"Este assistente irá guiá-lo na instalação de $(^Name)."
    StrCpy $RunError		"$(^Name) está sendo executado.$\n É necessário encerrar o aplicativo para completar este processo."   
    StrCpy $RunUpdateErr	"$(^Name) está em andamento e não pode ser atualizado. Por favor, encerre todos os Aplicativos de Jogos da Rockstar."
    StrCpy $AppErr			"Todos os Aplicativos da Rockstar devem ser removidos antes de remover $(^Name)"
		
	StrCpy $OptHeader1	 	"Bem-vindo ao programa de manutenção da configuração do $(^Name)."
	StrCpy $OptHeader2	 	"Este programa permite que você modifique a instalação atual. Clique em uma das opções abaixo e clique em Avançar para continuar."
	StrCpy $ModifyHead	 	"Modificar."					
	StrCpy $ModifyText	 	"Selecione novos recursos do programa para adicionar ou selecione recursos atualmente instalados para remover."
	StrCpy $RemoveHead   	"Remover."
	StrCpy $RemoveText   	"Remova todos os recursos instalados."
	StrCpy $ModHeader1	 	"Selecione as opções de instalação do $(^Name)." 
	StrCpy $ModHeader2   	"Marque os recursos que deseja manter, e desmarque os recursos que deseja remover."
FunctionEnd 

Function AddLangSelectPol
    StrCpy $LangTitle  "Język programu instalacyjnego" 
    StrCpy $LangSelect "Proszę wybrać język programu instalacyjnego."
FunctionEnd

Function AddPolish
    StrCpy $AdminErrorRGSC "Rockstar Games Social Club wymaga do instalacji przywilejów administratora.$\nProszę się zalogować jako administrator i ponownie uruchomić tę aplikację."
	StrCpy $AdminErrPatch "Aplikacja aktualizująca gry $(^Name) wymaga do instalacji przywilejów administratora.$\nProszę się zalogować jako administrator i ponownie uruchomić tę aplikację."
	StrCpy $AdminErrorDLC "Instalator gry $(^Name) wymaga przywilejów administratora.$\nProszę się zalogować jako administrator i ponownie uruchomić tę aplikację."

    StrCpy $OSError     "Twój system jest niekompatybilny z grą $(^Name)."
    StrCpy $WelcomeHead1 "Witaj."
    StrCpy $WelcomeHead2 "Ten program przeprowadzi cię przez proces instalacji gry $(^Name)."
    StrCpy $WelcomeMsg1 "Witaj w programie instalacyjnym aktualizacji gry $(^Name)."
    StrCpy $WelcomeMsg2 "Program zainstaluje najnowsze funkcje gry $(^Name).$\n$\n$(^ClickNext)"
    StrCpy $InstallHead "Instalowanie plików."
    StrCpy $InstallMsg  "Instalowanie plików gry $(^Name)."
    StrCpy $RunError    "Gra $(^Name) jest uruchomiona.$\n Musisz zamknąć aplikację, żeby dokończyć trwający proces."
    StrCpy $InitINSTALL  "Inicjalizacja programu instalacyjnego gry $(^Name)."
    StrCpy $WelcomeError "Błąd: Próba uruchomienia programu instalacyjnego $(^Name)."
    StrCpy $AbortWarning "Czy na pewno chcesz opuścić program instalacyjny gry $(^Name)?"
    StrCpy $TransferError "Jeden z rozpakowanych plików nie został przekopiowany na twój system.$\nPlik ten jest wymagany do prawidłowego działania gry $(^Name).$\nŻeby zapoznać się ze szczegółami tego błędu, kliknij przycisk szczegółów lub$\nodwiedź stronę www.rockstargames.com/support."
    
    StrCpy $RunUpdateErr "Gra $(^Name) jest uruchomiona i nie może zostać zaktualizowana. Zamknij wszystkie aplikacje Rockstar Games."
    StrCpy $NewerVerErr  "Zainstalowano już nowszą wersję gry $(^Name), $0. Opuszczanie programu instalacyjnego."
FunctionEnd

Function un.AddPolish
	StrCpy $WelcomeHead1 "Witaj."
    StrCpy $WelcomeHead2 "Ten program przeprowadzi cię przez proces instalacji gry $(^Name)."
    StrCpy $RunError    "Gra $(^Name) jest uruchomiona.$\n Musisz zamknąć aplikację, żeby dokończyć trwający proces."   
    StrCpy $RunUpdateErr "Gra $(^Name) jest uruchomiona i nie może zostać zaktualizowana. Zamknij wszystkie aplikacje Rockstar Games."
    StrCpy $AppErr       "Wszystkie aplikacje Rockstar muszą zostać usunięte, zanim możliwe będzie usunięcie gry $(^Name)."
		
	StrCpy $OptHeader1	"Witamy w programie instalacyjnym gry $(^Name)."
	StrCpy $OptHeader2	"Program umożliwia ci modyfikację aktualnej instalacji. Wybierz opcje poniżej i kliknij 'Dalej', żeby kontynuować."
	StrCpy $ModifyHead	"Zmień."
	StrCpy $ModifyText	"Wybierz nowe funkcje programu do dodania lub już zainstalowane do usunięcia."
	StrCpy $RemoveHead	"Usuń."
	StrCpy $RemoveText	"Usuń wszystkie zainstalowane funkcje."
	StrCpy $ModHeader1	"Wybierz opcje instalacji gry $(^Name)."
	StrCpy $ModHeader2	"Wybierz funkcje, które chcesz zachować, i odznacz te, które chcesz usunąć."
FunctionEnd 


!insertmacro LANGFILE "English" "English"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_ENGLISH} "License Agreement"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_ENGLISH} "Please review the license terms before installing $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_ENGLISH} "If you accept the terms of the agreement, click the check box below. You must accept the agreement to install $(^Name). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_ENGLISH} "Press Page Down to see the rest of the agreement."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_ENGLISH} "Installing"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_ENGLISH} "Please wait while $(^Name) is being installed."
LangString MUI_TEXT_FINISH_TITLE ${LANG_ENGLISH} "Install Complete"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_ENGLISH} "Setup was completed successfully. Click Close to finish."
LangString MUI_TEXT_ABORT_TITLE ${LANG_ENGLISH} "The uninstall was aborted"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_ENGLISH} "Setup was not completed successfully. Click Close to finish."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_ENGLISH} "Uninstalling"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_ENGLISH} "Please wait while $(^Name) is being uninstalled."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_ENGLISH} "Uninstallation was completed successfully. Click Close to finish."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_ENGLISH} "Uninstallation Complete"
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_ENGLISH} "Uninstallation Aborted"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_ENGLISH} "Setup was not completed successfully. Click Close to finish."


!insertmacro LANGFILE "French" "French"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_FRENCH} "Licence utilisateur"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_FRENCH} "Veuillez examiner les termes de la licence avant d'installer $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_FRENCH} "Si vous acceptez les conditions de la licence utilisateur, cochez la case ci-dessous. Vous devez accepter la licence utilisateur afin d'installer $(^Name)."
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_FRENCH} "Appuyez sur Page Suivante pour lire le reste de la licence utilisateur."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_FRENCH} "Installation en cours"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_FRENCH} "Veuillez patienter pendant que $(^Name) est en train d'être installé."
LangString MUI_TEXT_FINISH_TITLE ${LANG_FRENCH} "Installation terminée"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_FRENCH} "L'installation s'est terminée avec succès."
LangString MUI_TEXT_ABORT_TITLE ${LANG_FRENCH} "Installation interrompue"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_FRENCH} "L'installation n'a pas été terminée."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_FRENCH} "Désinstallation"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_FRENCH} "Veuillez patienter pendant la désinstallation de $(^Name)."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_FRENCH} "Désinstallation réussie. Appuyez sur Fermer pour finir."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_FRENCH} "La désinstallation est terminée."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_FRENCH} "La désinstallation a été annulée."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_FRENCH} "L'installation a échoué. Appuyez sur Fermer pour finir."

!insertmacro LANGFILE "Italian" "Italian"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_ITALIAN} "Licenza d'uso"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_ITALIAN} "Prego leggere le condizioni della licenza d'uso prima di installare $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_ITALIAN} "Se si accettano i termini della licenza d'uso, selezionare la casella sottostante. È necessario accettare i termini della licenza d'uso per installare $(^Name). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_ITALIAN} "Premere PAG GIU per vedere il resto della licenza d'uso."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_ITALIAN} "Installazione in corso"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_ITALIAN} "Prego attendere mentre $(^Name)  viene installato."
LangString MUI_TEXT_FINISH_TITLE ${LANG_ITALIAN} "Installazione completata"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_ITALIAN} "L'installazione è stata completata con successo."
LangString MUI_TEXT_ABORT_TITLE ${LANG_ITALIAN} "Installazione interrotta"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_ITALIAN} "L'installazione non è stata completata correttamente."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_ITALIAN} "Disinstallazione in corso..."
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_ITALIAN} "Attendi mentre $(^Name) viene disinstallato."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_ITALIAN} "La disinstallazione è stata completata con successo. Clicca 'Chiudi' per uscire."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_ITALIAN} "La disinstallazione è stata completata."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_ITALIAN} "La disinstallazione è stata interrotta."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_ITALIAN} "L'installazione non è stata completata. Clicca 'Chiudi' per uscire."

!insertmacro LANGFILE "German" "German"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_GERMAN} "Lizenzabkommen"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_GERMAN} "Bitte lesen Sie die Lizenzbedingungen durch, bevor Sie mit der Installation fortfahren."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_GERMAN} "Falls Sie alle Bedingungen des Abkommens akzeptieren, aktivieren Sie das Kästchen. Sie müssen die Lizenzvereinbarungen anerkennen, um $(^Name) installieren zu können. $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_GERMAN} "Drücken Sie die Bild-nach-unten Taste, um den Rest des Abkommens zu sehen."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_GERMAN} "Installiere..."
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_GERMAN} "Bitte warten Sie, während $(^Name) installiert wird."
LangString MUI_TEXT_FINISH_TITLE ${LANG_GERMAN} "Die Installation ist vollständig"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_GERMAN} "Die Installation wurde erfolgreich abgeschlossen."
LangString MUI_TEXT_ABORT_TITLE ${LANG_GERMAN} "Abbruch der Installation"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_GERMAN} "Die Installation wurde nicht vollständig abgeschlossen."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_GERMAN} "Deinstallieren..."
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_GERMAN} "Bitte warten, während $(^Name) deinstalliert wird."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_GERMAN} "Deinstallation erfolgreich abgeschlossen. Zum Beenden 'Schließen' klicken."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_GERMAN} "Deinstallation erfolgreich."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_GERMAN} "Deinstallation abgebrochen."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_GERMAN} "Installation nicht erfolgreich. Zum Beenden 'Schließen' klicken."
  
!insertmacro LANGFILE "Spanish" "Spanish"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_SPANISH} "Acuerdo de licencia"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_SPANISH} "Por favor revise los términos de la licencia antes de instalar $(^Name)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_SPANISH} "Si acepta los términos del acuerdo, marque abajo la casilla. Debe aceptar los términos para instalar $(^Name). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_SPANISH} "Presione Avanzar Página para ver el resto del acuerdo."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_SPANISH} "Instalando"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_SPANISH} "Por favor espere mientras $(^Name) se instala."
LangString MUI_TEXT_FINISH_TITLE ${LANG_SPANISH} "Instalación Completada"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_SPANISH} "La instalación se ha completado correctamente."
LangString MUI_TEXT_ABORT_TITLE ${LANG_SPANISH} "Instalación Anulada"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_SPANISH} "La instalación no se completó correctamente."

LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_SPANISH} "Desinstalando"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_SPANISH} "Espera mientras se desinstala $(^Name)."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_SPANISH} "La desinstalación se ha completado con éxito."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_SPANISH} "La desinstalación se ha completado."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_SPANISH} "La desinstalación ha sido abortada."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_SPANISH} "La instalación no ha sido completada con éxito. Haz clic en 'Cerrar' para finalizar."
    
!insertmacro LANGFILE "Russian" "Russian"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_RUSSIAN} "Лицензионное соглашение"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_RUSSIAN} "Перед установкой $(^Name) ознакомьтесь с лицензионным соглашением."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_RUSSIAN} "Если вы принимаете условия соглашения, установите флажок ниже. $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_RUSSIAN} "Нажмите Page Down, чтобы просмотреть оставшуюся часть соглашения."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_RUSSIAN} "Копирование файлов"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_RUSSIAN} "Выполняется установка $(^Name). Пожалуйста, подождите."
LangString MUI_TEXT_FINISH_TITLE ${LANG_RUSSIAN} "Установка завершена"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_RUSSIAN} "Установка завершена. Для завершения щелкните по кнопке Закрыть."
LangString MUI_TEXT_ABORT_TITLE ${LANG_RUSSIAN} "Установка прервана"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_RUSSIAN} "Установка не была произведена в полной мере. Для завершения щелкните по кнопке Закрыть."
LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_RUSSIAN} "Удаление"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_RUSSIAN} "Подождите, пока производится удаление $(^Name)."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_RUSSIAN} "Удаление было завершено успешно. Нажмите кнопку Закрыть, чтобы закончить."
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_RUSSIAN} "Удаление завершено."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_RUSSIAN} "Удаление было прервано."
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_RUSSIAN} "Установка не была завершена успешно. Нажмите кнопку 'Закрыть', чтобы закончить."


!insertmacro LANGFILE "Polish" "Polish"
LangString MUI_TEXT_LICENSE_TITLE ${LANG_POLISH} "Umowa licencyjna"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_POLISH} "Przed instalacją programu $(^NameDA) zapoznaj się z warunkami licencji."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_POLISH} "Jeżeli akceptujesz warunki umowy, zaznacz pole wyboru poniżej, aby kontynuować. Musisz zaakceptować warunki umowy, aby zainstalować $(^NameDA). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_POLISH} "Naciśnij klawisz Page Down, aby zobaczyć dalszą część umowy."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_POLISH} "Instalacja"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_POLISH} "Proszę czekać, podczas gdy $(^NameDA) jest instalowany."
LangString MUI_TEXT_FINISH_TITLE ${LANG_POLISH} "Zakończono instalację"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_POLISH} "Instalacja zakończona pomyślnie."
LangString MUI_TEXT_ABORT_TITLE ${LANG_POLISH} "Instalacja przerwana"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_POLISH} "Instalacja nie została zakończona pomyślnie."
LangString  MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_POLISH} "Deinstalacja"
LangString  MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_POLISH} "Proszę czekać, $(^NameDA) jest odinstalowywany."
LangString  MUI_UNTEXT_FINISH_TITLE ${LANG_POLISH} "Zakończono odinstalowanie"
LangString  MUI_UNTEXT_FINISH_SUBTITLE ${LANG_POLISH} "Odinstalowanie zakończone pomyślnie."
LangString  MUI_UNTEXT_ABORT_TITLE ${LANG_POLISH} "Deinstalacja przerwana"
LangString  MUI_UNTEXT_ABORT_SUBTITLE ${LANG_POLISH} "Deinstalacja nie została zakończona pomyślnie."

!insertmacro LANGFILE "PortugueseBr" "PortugueseBr"

LangString MUI_TEXT_LICENSE_TITLE ${LANG_PORTUGUESEBR} "Acordo da licença"
LangString MUI_TEXT_LICENSE_SUBTITLE ${LANG_PORTUGUESEBR} "Por favor, reveja os termos da licença antes de instalar $(^NameDA)."
LangString MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX ${LANG_PORTUGUESEBR} "Se você aceita os termos do acordo, clique na caixa de seleção abaixo. Você deve aceitar o acordo para instalar a $(^NameDA). $_CLICK"
LangString MUI_INNERTEXT_LICENSE_TOP ${LANG_PORTUGUESEBR} "Pressione Page Down para ver o resto do acordo."
LangString MUI_TEXT_INSTALLING_TITLE ${LANG_PORTUGUESEBR} "Instalando"
LangString MUI_TEXT_INSTALLING_SUBTITLE ${LANG_PORTUGUESEBR} "Por favor, aguarde enquanto $(^NameDA) está sendo instalado."
LangString MUI_TEXT_FINISH_TITLE ${LANG_PORTUGUESEBR} "Instalação Completa"
LangString MUI_TEXT_FINISH_SUBTITLE ${LANG_PORTUGUESEBR} "O instalador completou com sucesso."
LangString MUI_TEXT_ABORT_TITLE ${LANG_PORTUGUESEBR} "Instalação Abortada"
LangString MUI_TEXT_ABORT_SUBTITLE ${LANG_PORTUGUESEBR} "O instalador não completou com sucesso."
LangString MUI_UNTEXT_UNINSTALLING_TITLE ${LANG_PORTUGUESEBR} "Desinstalando"
LangString MUI_UNTEXT_UNINSTALLING_SUBTITLE ${LANG_PORTUGUESEBR} "Por favor, espere enquanto a $(^NameDA) está sendo desinstalada."
LangString MUI_UNTEXT_FINISH_TITLE ${LANG_PORTUGUESEBR} "Desinstalação Completa"
LangString MUI_UNTEXT_FINISH_SUBTITLE ${LANG_PORTUGUESEBR} "A desinstalação foi completada com sucesso."
LangString MUI_UNTEXT_ABORT_TITLE ${LANG_PORTUGUESEBR} "Desinstalação Abortada"
LangString MUI_UNTEXT_ABORT_SUBTITLE ${LANG_PORTUGUESEBR} "A desinstalação não foi completada com sucesso."
