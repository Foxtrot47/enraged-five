# General Symbol Definitions
Name "Max Payne 3"
!define IconLoc "X:\payne\tools\installer\project\installer_Patch\Global\IMG\MaxPayne3.ico"

!define FILENAME "X:\payne\tools\installer\project\installer_Patch\MP3_Patch_Commerce_1_0_0_82.exe"
!define VERSION 1.00.00.82
!define DATALOC "X:\payne\tools\installer\project\installer_Patch\DATA"

!define WAITPROC "MaxPayne3.exe"
