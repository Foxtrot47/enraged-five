!include "LogicLib.nsh"                       ; Used for Syntax
!include "WinMessages.nsh"                  
!include "WinVer.nsh"
!include "WordFunc.nsh"

!insertmacro WordFind

!define GIS_NOT_INSTALLED   0 # might be wrong value
!define GIS_CURRENT_USER    1 # might be wrong value
!define GIS_ALL_USERS       2 # might be wrong value
!define GIS_ADMIN           3 # might be wrong value

!define StrStr "!insertmacro StrStr"
 
!macro StrStr ResultVar String SubString
  Push ${String}
  Push ${SubString}
  Call StrStr
  Pop  ${ResultVar}
!macroend

;!define DEBUG_MESSAGES      1

Var IsLanguageArgsSet
Var IsWaitForProcSet
Var ProcessWaitFail
Var STR_HAYSTACK
Var STR_NEEDLE
Var CHAR_COUNTER
Var ARG_LENGTH
Var STR_PROC
Var ARGS_LIST_LENGTH
Var ARGS_START_INDEX
Var CHAR_START_INDEX
Var STR_CHARACTER
Var STR_RETURN_VAR

!macro CRCFILE Result FileInstLoc LocFile FileName
    FILE "${FileName}${LocFile}"   
!macroend  

!macro DELETEFILE LocFile FileName
	IfFileExists "${FileName}${LocFile}" 0 +2 
    DELETE /REBOOTOK "${FileName}${LocFile}"   
!macroend  

!macro _StrContains INDEX VALUE NEEDLE HAYSTACK
  Push "${HAYSTACK}"
  Push "${NEEDLE}"
  Call cStrContains
  Pop "${INDEX}"
  pop "${VALUE}"
!macroend

!define StrContains '!insertmacro "_StrContains"'

!macro IfKeyExists ROOT MAIN_KEY KEY
  Push $R0
  Push $R1
  Push $R2
 
  # XXX bug if ${ROOT}, ${MAIN_KEY} or ${KEY} use $R0 or $R1
 
  StrCpy $R1 "0" # loop index
  StrCpy $R2 "0" # not found
 
  ${Do}
    EnumRegKey $R0 ${ROOT} "${MAIN_KEY}" "$R1"
    ${If} $R0 == "${KEY}"
      StrCpy $R2 "1" # found
      ${Break}
    ${EndIf}
    IntOp $R1 $R1 + 1
  ${LoopWhile} $R0 != ""
 
  ClearErrors
 
  Exch 2
  Pop $R0
  Pop $R1
  Exch $R2
!macroend

 ; GetParameters
 ; input, none
 ; output, top of stack (replaces, with e.g. whatever)
 ; modifies no other variables.
 
Function GetParameters
 
  Push $R0
  Push $R1
  Push $R2
  Push $R3
 
  StrCpy $R2 1
  StrLen $R3 $CMDLINE
 
  ;Check for quote or space
  StrCpy $R0 $CMDLINE $R2
  StrCmp $R0 '"' 0 +3
    StrCpy $R1 '"'
    Goto loop
  StrCpy $R1 " "
 
  loop:
    IntOp $R2 $R2 + 1
    StrCpy $R0 $CMDLINE 1 $R2
    StrCmp $R0 $R1 get
    StrCmp $R2 $R3 get
    Goto loop
 
  get:
    IntOp $R2 $R2 + 1
    StrCpy $R0 $CMDLINE 1 $R2
    StrCmp $R0 " " get
    StrCpy $R0 $CMDLINE "" $R2
 
  Pop $R3
  Pop $R2
  Pop $R1
  Exch $R0
 
FunctionEnd

Function CheckForLanguageArgs	
	Call GetParameters
	pop $1

	${StrContains} $R0 $R1 "1033" $1		;English
	StrCmp $R1 "1033" Set_English
	
	${StrContains} $R0 $R1 "1036" $1		;French
	StrCmp $R1 "1036" Set_French
	
	${StrContains} $R0 $R1 "1040" $1		;Italian
	StrCmp $R1 "1040" Set_Italian
	
	${StrContains} $R0 $R1 "3082" $1		;Spanish
	StrCmp $R1 "3082" Set_Spanish
	
	${StrContains} $R0 $R1 "1031" $1		;German
	StrCmp $R1 "1031" Set_German
	
	${StrContains} $R0 $R1 "1045" $1		;Polish
	StrCmp $R1 "1045" Set_Polish
	
	${StrContains} $R0 $R1 "1046" $1		;Portuguese
	StrCmp $R1 "1046" Set_Portuguese

	${StrContains} $R0 $R1 "1049" $1		;Russian
	StrCmp $R1 "1049" Set_Russian
						
;	${StrContains} $R0 $R1 "1041" $1		;Japan
;	StrCmp $R1 "1041" Set_Japan
	
	goto CheckForLanguageArgsEnd

Set_English:
	StrCpy $LANGUAGE 1033 		;English
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_French:
    StrCpy $LANGUAGE 1036		;French
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_Italian:
    StrCpy $LANGUAGE 1040		;Italian
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_Spanish:
    StrCpy $LANGUAGE 3082		;Spanish
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_German:
    StrCpy $LANGUAGE 1031		;German
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_Russian:
    StrCpy $LANGUAGE 1049		;Russian
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_Polish:
	StrCpy $LANGUAGE 1045		;Polish
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
Set_Portuguese:
	StrCpy $LANGUAGE 1046		;Portuguese
	strcpy $IsLanguageArgsSet 1
	goto CheckForLanguageArgsEnd
;Set_Japan:
;   StrCpy $LANGUAGE 1041		;Japan
;	strcpy $IsLanguageArgsSet 1
;	goto CheckForLanguageArgsEnd

CheckForLanguageArgsEnd: 

FunctionEnd

; Checks to see if the installer need to run in silent mode
Function IsSilent
  Call GetParameters
  Pop  $1
							
  ${StrContains} $R0 $R1 "Silent" $1
  StrCmp $R1 "Silent" Silent
  
  ${StrContains} $R0 $R1 "?" $1
  StrCmp $R1 "?" CmdLineArgs IsSilentEND
  
  Silent: 
	SetSilent silent
    Goto IsSilentEND
    
CmdLineArgs:
	MessageBox MB_OK "/Silent -Silent install$\n/? - info$\n/1033 - English$\n/1036 - Français$\n/1040 - Italiano$\n/3082 - Español$\n/1031 - Deutsch$\n/1049 - Русско$\n/1045 - Polish$\n/1046 - Portuguese"
	Abort 
         
IsSilentEND: 

FunctionEnd

; Finds a sub string in a string
Function StrStr
/*After this point:
  ------------------------------------------
  $R0 = SubString (input)
  $R1 = String (input)
  $R2 = SubStringLen (temp)
  $R3 = StrLen (temp)
  $R4 = StartCharPos (temp)
  $R5 = TempStr (temp)*/
 
  ;Get input from user
  Exch $R0
  Exch
  Exch $R1
  Push $R2
  Push $R3
  Push $R4
  Push $R5
 
  ;Get "String" and "SubString" length
  StrLen $R2 $R0
  StrLen $R3 $R1
  ;Start "StartCharPos" counter
  StrCpy $R4 0
 
  ;Loop until "SubString" is found or "String" reaches its end
  ${Do}
    ;Remove everything before and after the searched part ("TempStr")
    StrCpy $R5 $R1 $R2 $R4
 
    ;Compare "TempStr" with "SubString"
    ${IfThen} $R5 == $R0 ${|} ${ExitDo} ${|}
    ;If not "SubString", this could be "String"'s end
    ${IfThen} $R4 >= $R3 ${|} ${ExitDo} ${|}
    ;If not, continue the loop
    IntOp $R4 $R4 + 1
  ${Loop}
 
/*After this point:
  ------------------------------------------
  $R0 = ResultVar (output)*/
 
  ;Remove part before "SubString" on "String" (if there has one)
  StrCpy $R0 $R1 `` $R4
 
  ;Return output to user
  Pop $R5
  Pop $R4
  Pop $R3
  Pop $R2
  Pop $R1
  Exch $R0
FunctionEnd

Function StrTok
  Exch $R1
  Exch 1
  Exch $R0
  Push $R2
  Push $R3
  Push $R4
  Push $R5
 
  ;R0 fullstring
  ;R1 tokens
  ;R2 len of fullstring
  ;R3 len of tokens
  ;R4 char from string
  ;R5 testchar
 
  StrLen $R2 $R0
  IntOp $R2 $R2 + 1
 
  loop1:
    IntOp $R2 $R2 - 1
    IntCmp $R2 0 exit
 
    StrCpy $R4 $R0 1 -$R2
 
    StrLen $R3 $R1
    IntOp $R3 $R3 + 1
 
    loop2:
      IntOp $R3 $R3 - 1
      IntCmp $R3 0 loop1
 
      StrCpy $R5 $R1 1 -$R3
 
      StrCmp $R4 $R5 Found
    Goto loop2
  Goto loop1
 
  exit:
  ;Not found!!!
  StrCpy $R1 ""
  StrCpy $R0 ""
  Goto Cleanup
 
  Found:
  StrLen $R3 $R0
  IntOp $R3 $R3 - $R2
  StrCpy $R1 $R0 $R3
 
  IntOp $R2 $R2 - 1
  IntOp $R3 $R3 + 1
  StrCpy $R0 $R0 $R2 $R3
 
  Cleanup:
  Pop $R5
  Pop $R4
  Pop $R3
  Pop $R2
  Exch $R0
  Exch 1
  Exch $R1
 
FunctionEnd

Function AbortWaitForProc
	IntOp $ProcessWaitFail 0 + 1 
FunctionEnd

Function TimerStart
  GetFunctionAddress $2 AbortWaitForProc
  ThreadTimer::Start /NOUNLOAD 10000 1 $2 ; Timer ticks every 2345 milliseconds, totally 8 times calls TryMe
FunctionEnd

; Checks to make sure processes are shut down before continuing. Currently hard coded.  
Function CheckForProcArgs	
	IntOp $IsWaitForProcSet 0 + 0
	IntOp $ProcessWaitFail 0 + 0

	Call GetParameters
	pop $1

	${StrContains} $R0 $R1 "/waitforprocess" $1
	StrCmp $R1 "/waitforprocess" CheckForProcArgsRun CheckForProcArgsEnd	;CheckForProcNoParam Needs to be switched for Launcher.

;GetProcArgsEXE:
	;push $1
	;push "/waitforprocess"							
	;Call GetProcArgs 
	;pop $0
	;StrCmp $STR_PROC "0" CheckForProcArgsEnd CheckForProcArgsRun

CheckForProcArgsRun:
	IntOp $IsWaitForProcSet 0 + 1
	
	StrCpy $STR_PROC ${WAITPROC}

	Call WaitForProc
	goto CheckForProcArgsEnd

CheckForProcNoParam:														; This needs to get hit for Launcher Updater.
	
	StrCpy $STR_PROC ${WAITPROC}

	SetOutPath $TEMP
	GetTempFileName $8

	File /oname=$8 "Global\Plugins\FindProcDLL.dll"
	Push $STR_PROC
    CallInstDLL $8 FindProc
	IntCmp $R0 1 0 CheckForProcArgsEnd

	ThreadTimer::Stop
	MessageBox MB_ICONSTOP|MB_OK $RunUpdateErr
	SetErrorLevel 911
	Abort

CheckForProcArgsEnd:
	
	Push 0
FunctionEnd

Function WaitForProc
	SetOutPath $TEMP
	GetTempFileName $8
	
	Call TimerStart
	Banner::show /set 76 $InitINSTALL "..."

WaitForProc_Loop:	
	File /oname=$8 "Global\Plugins\FindProcDLL.dll"
    Push $STR_PROC
	CallInstDLL $8 FindProc
    IntCmp $ProcessWaitFail 1 ProcessAborted
	IntCmp $R0 1 0 NotRunning
	goto WaitForProc_Loop

ProcessAborted:
	Banner::destroy
	ThreadTimer::Stop
	MessageBox MB_ICONSTOP|MB_OK $RunUpdateErr
	SetErrorLevel 911
	Abort

NotRunning:
	
	Banner::destroy
FunctionEnd

Function VersionCheck
	Exch $0 ;second versionnumber
	Exch
	Exch $1 ;first versionnumber
	Push $R0 ;counter for $0
	Push $R1 ;counter for $1
	Push $3 ;temp char
	Push $4 ;temp string for $0
	Push $5 ;temp string for $1
	StrCpy $R0 "-1"
	StrCpy $R1 "-1"
Start:
	StrCpy $4 ""
DotLoop0:
	IntOp $R0 $R0 + 1
	StrCpy $3 $0 1 $R0
	StrCmp $3 "" DotFound0
	StrCmp $3 "." DotFound0
	StrCpy $4 $4$3
	Goto DotLoop0
DotFound0:
	StrCpy $5 ""
DotLoop1:
	IntOp $R1 $R1 + 1
	StrCpy $3 $1 1 $R1
	StrCmp $3 "" DotFound1
	StrCmp $3 "." DotFound1
	StrCpy $5 $5$3
	Goto DotLoop1
DotFound1:
	Strcmp $4 "" 0 Not4
	StrCmp $5 "" VersionCheckEqual
	Goto Ver2Less
Not4:
	StrCmp $5 "" Ver2More
	IntCmp $4 $5 Start Ver2Less Ver2More
VersionCheckEqual:
	StrCpy $0 "0"
	Goto VersionCheckFinish
Ver2Less:
	StrCpy $0 "1"
	Goto VersionCheckFinish
Ver2More:
	StrCpy $0 "2"
VersionCheckFinish:
	Pop $5
	Pop $4
	Pop $3
	Pop $R1
	Pop $R0
	Pop $1
	Exch $0
FunctionEnd

Function CheckVersion
    pop $1
	pop $2
	pop $3
	
	ReadRegStr $0 HKEY_LOCAL_MACHINE $3 $2
	strcmp $0 "" CheckRGSCVersionEND
	
	strCmp $1 $0 CheckRGSCVersionEND 	

	push $0
	push $1
	Call VersionCheck
	pop  $R0 
	
	${if} $R0 == 2
		goto CheckRGSCVersionEND	
	${endif}

	push $0
	
;	${SWITCH} $LANGUAGE
;        ${Case} 1033
;            Call AddEnglishVerError        
;            ${Break}
;        ${Case} 1036
;            Call AddFrenchVerError        
;            ${Break}
;        ${Case} 1040
;            Call AddItalianVerError        
;            ${Break}
;        ${Case} 3082
;           Call AddSpanishVerError        
;            ${Break}
;        ${Case} 1031
;            Call AddGermanVerError        
;            ${Break}
;		${Case} 1041
;            Call AddJapaneseVerError       
;            ${Break}    
;        ${Case} 1049
;            Call AddRussianVerError      
;            ${Break}
;		${Case} 1045            ;Polish
    ;        Push AddPolishVerError   
    ;       ${Break}
	;	${Case} 1046            ;PortugueseBR
    ;       Call AddPortugueseVerError
    ;    ${Default}
    ;        Call AddEnglishVerError        
    ;        ${Break}
    ;${EndSwitch}
	
	IfSilent +2
    MessageBox MB_OK|MB_ICONEXCLAMATION $NewerVerErr
    abort

CheckRGSCVersionEND:
   
FunctionEnd

; StrContains
; This function does a case sensitive searches for an occurrence of a substring in a string. 
; It returns the substring if it is found. 
; Otherwise it returns null(""). 
Function cStrContains
  Exch $STR_NEEDLE
  Exch 1
  Exch $STR_HAYSTACK
	
	StrCpy $STR_RETURN_VAR ""
    StrCpy $CHAR_COUNTER 0	
	IntOp  $CHAR_COUNTER $CHAR_COUNTER - 1
    StrLen $ARG_LENGTH $STR_NEEDLE
    StrLen $ARGS_LIST_LENGTH $STR_HAYSTACK

	loop:
      IntOp $CHAR_COUNTER $CHAR_COUNTER + 1
      StrCpy $STR_PROC $STR_HAYSTACK $ARG_LENGTH $CHAR_COUNTER
	  StrCmp $STR_PROC $STR_NEEDLE found
      StrCmp $CHAR_COUNTER $ARGS_LIST_LENGTH done

      Goto loop
    found:
      StrCpy $STR_RETURN_VAR $STR_NEEDLE
      Goto done
    done:
  
   Pop $STR_NEEDLE ;Prevent "invalid opcode" errors and keep the
   push $STR_RETURN_VAR  
   push $CHAR_COUNTER

FunctionEnd
 
Function GetProcArgs
	Exch $STR_NEEDLE
	Exch 1
	Exch $STR_HAYSTACK
	
	StrCpy $STR_RETURN_VAR "0"
	StrLen $ARG_LENGTH $STR_NEEDLE
	StrLen $ARGS_LIST_LENGTH $STR_HAYSTACK

	IntOp  $CHAR_COUNTER $ARG_LENGTH + $CHAR_COUNTER
	IntOp  $CHAR_COUNTER $CHAR_COUNTER + 1 

	StrCpy $STR_PROC $STR_HAYSTACK $ARGS_LIST_LENGTH $CHAR_COUNTER
	push $STR_PROC
FunctionEnd







