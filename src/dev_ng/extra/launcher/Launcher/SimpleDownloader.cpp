#include "StdAfx.h"
#include "SimpleDownloader.h"
#include "Application.h"
#include "Util.h"
#include "Channel.h"

#include <sstream>
#include <vector>

CSimpleDownloader::CSimpleDownloader(const char* server, const char* path, const char* method)
: m_server(server), m_path(path), m_method(method)
{
}

CSimpleDownloader::~CSimpleDownloader(void)
{
}

#if !RSG_FINAL
void CSimpleDownloader::Logs(const char* msg, bool addNewLine)
{
	TRACE("%s", msg);
	if (addNewLine)
		TRACE("\r\n");

	DEBUGF1("%s", msg);
}
#else
void CSimpleDownloader::Logs(const char*, bool) {}
#endif

void CSimpleDownloader::Logf(const char* fmt, ...)
{
	for (size_t bufs = 1024*32; bufs < 1024*1024; bufs *= 2)
	{
		char* buffer = new char[bufs];
		va_list args;
		va_start(args, fmt);
		int written = vsnprintf_s(buffer, bufs, bufs-1, fmt, args);
		va_end(args);

		if (written > 0 && written < (signed)bufs-1)
		{
			Logs(buffer);
			delete[] buffer;
			return;
		}
		else
			delete[] buffer;
	}
}

void CSimpleDownloader::Errors(const char* msg)
{
	ERRORF("%s", msg);

	CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_UNKNOWN, std::string(msg));
}

/*
void CSimpleDownloader::Errorf(const char* fmt, ...)
{
	for (size_t bufs = 1024; bufs < 1024*1024; bufs *= 2)
	{
		char* buffer = new char[bufs];
		va_list args;
		va_start(args, fmt);
		int written = vsnprintf_s(buffer, bufs, bufs-1, fmt, args);
		va_end(args);

		if (written > 0 && written < (signed)bufs-1)
		{
			Errors(buffer);
			delete[] buffer;
			return;
		}
		else
			delete[] buffer;
	}
} */

BOOL CSimpleDownloader::BlockingDownload(char** out_result, size_t* out_size)
{
	BOOL retval = TRUE;

	//@@: range CSIMPLEDOWNLOADER_BLOCKINGDOWNLOAD {
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2,2), &wsaData) != 0)
	{
		Errors("Failed to init WinSock.");
		return FALSE;
	}

	retval = CreateSocket(out_result, out_size);

	WSACleanup();

	//@@: } CSIMPLEDOWNLOADER_BLOCKINGDOWNLOAD

	return retval;
}

BOOL CSimpleDownloader::CreateSocket(char** out_result, size_t* out_size)
{
	//@@: range CSIMPLEDOWNLOADER_CREATESOCKET {

	SOCKET sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (sock == INVALID_SOCKET)
	{
		Errors("Failed to create socket.");
		return FALSE;
	}

	hostent* host = gethostbyname(CUtil::DecodeAndDecrypt(std::string(m_server)).c_str());

	if (host == NULL)
	{
		Errors("Host not found.");
		closesocket(sock);
		return FALSE;
	}

	char* ip = inet_ntoa(*(struct in_addr *)*host->h_addr_list);

	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(ip);
	addr.sin_port = htons(80);

	int result = connect(sock, (sockaddr*)&addr, sizeof(addr));
	if (result == SOCKET_ERROR)
	{
		Errors("Failed to connect socket.");
		closesocket(sock);
		return FALSE;
	}

	BOOL retval = Download(sock, out_result, out_size);

	closesocket(sock);

	return retval;
	//@@: } CSIMPLEDOWNLOADER_CREATESOCKET

}

BOOL CSimpleDownloader::Download(SOCKET sock, char** out_result, size_t* out_size)
{
	//@@: range CSIMPLEDOWNLOADER_DOWNLOAD {

	std::stringstream builder;

	builder << m_method << " " << m_path << " HTTP/1.1\r\n";
	builder << "Host: " << m_server << "\r\n";
	builder << "Accept: */*\r\n";
	builder << "Connection: close\r\n";
	builder << "\r\n";

	const std::string& headers_str = builder.str();
	const char* headers_cstr = headers_str.c_str();
	size_t len = strlen(headers_cstr);

	Logf("Sending headers: %s", headers_cstr);

	//int sent = sock->Send(headers_cstr, len);
	int sent = send(sock, headers_cstr, (int)len, 0);

	if (sent == SOCKET_ERROR)
	{
		Errors("Socket error sending data!");
		return FALSE;
	}
	else if (sent < (signed)len)
	{
		Logf("Only %d bytes were sent!");
		Logs("");
		return FALSE;
	}

	Logs("Successfully sent request!");


	CUtil::Sleep(100);


	const size_t buffersize = 1024;
	struct SizedBuffer
	{
		char* buffer; size_t size;
	};

	HeaderHandler headerHandler;

	size_t downloadTotal = 0;
	size_t zeros = 0;

	std::vector<SizedBuffer> buffers;
	for (int i = 0; i < 1024; i++) // Max of 1MB
	{
		char* buffer = new char[1024];
		int received = recv(sock, buffer, (int)len, 0);

		if (received == SOCKET_ERROR)
		{
			Errors("Socket error receiving data!");
			return FALSE;
		}
		else if (received == 0)
		{
			zeros++;
			if (zeros > 10)
			{
				Logs("Remote host closed connection.");
				break;
			}
			else
			{
				Logs("...");
				CUtil::Sleep(100);
			}
		}
		else if (received > 0 && received <= buffersize)
		{
			if (!headerHandler.HasPassedHeaders())
			{
				for (int i = 0; i < received; i++)
				{
					if (headerHandler.Consume(buffer[i]))
					{
						Logf("Content length is %d, expecting %d bytes total.", headerHandler.ContentLength(), (headerHandler.HeadersLength()+headerHandler.ContentLength()));
						break;
					}
				}
			}
			if (received < buffersize)
			{
				char* smallbuffer = new char[received];
				memcpy(smallbuffer, buffer, received);
				delete[] buffer;
				buffer = smallbuffer;
			}
			SizedBuffer sb = { buffer, received };
			buffers.push_back(sb);

			downloadTotal += received;

			if (headerHandler.HasPassedHeaders())
			{
				if (headerHandler.HeadersLength() > 0 && downloadTotal == headerHandler.ContentLength() + headerHandler.HeadersLength())
				{
					Logs("Download looks complete.");
					break;
				}
			}
		}
		else
		{
			Logf("Unexpected size read: %d (buffer size: %d)", received, buffersize);
		}
		//Sleep(10);
	}

	size_t total = 0;
	for (int i = (int)buffers.size(); i-->0;)
		total += buffers[i].size;

	Logf("%d bytes received.", total);


	builder.str(std::string()); // Clear

	// TODO: Make this an option?
	const bool translateCharacters = false;

	//char prevChar = '\0';
	for (size_t i = 0; i < buffers.size(); i++)
	{
		char* buf = buffers[i].buffer;
		int size = (int)buffers[i].size;
		for (int j = 0; j < size; j++)
		{
			char c = buf[j];

			if (translateCharacters)
			{
				if (c == '\n')
				{
					builder << "\r\n";
				}
				else if (c < ' ' || c >= 127)
				{
					//builder << "{" << (int)c << "}";
				}
				else
					builder << c;
			}
			else
			{
				if (c != 0)
					builder << c;
				else
					builder << '*';
			}

			//prevChar = c;
		}
		delete[] buf;
	}
	buffers.clear();

	std::string stdstring = builder.str();

	len = stdstring.length();
	*out_result = new char[len + 1];
	strcpy_s(*out_result, len+1, stdstring.c_str());
	(*out_result)[len] = '\0';
	*out_size = len+1;

	Logs("Received data:");
	char* buffer = new char[1024];
	for (size_t i = 0; i < len; i+=1023)
	{
		size_t size = 1023;
		if (size > len - i)
			size = len - i;
		strncpy_s(buffer, 1024, &((*out_result)[i]), size);
		buffer[size] = '\0';
		Logf("%s", buffer);
	}
	delete[] buffer;
	Logs("\r\n");

	return TRUE;
	//@@: } CSIMPLEDOWNLOADER_DOWNLOAD

}

bool CSimpleDownloader::GetServer(const char* url, char** out_server)
{
	//@@: range CSIMPLEDOWNLOADER_GETSERVER {
	size_t len = strlen(url);

	const char* origin = strstr(url, "//");
	if (origin == NULL)
		origin = url;
	else
		origin += 2;

	if ((unsigned)(origin - url) >= len)
		return false;

	const char* end = strstr(origin, "/");
	if (end == NULL)
		end = &(url[len]);

	size_t out_len = end - origin;
	if (out_len < 3) // ignore silly values
		return false;

	(*out_server) = new char[out_len+1];
	memcpy(*out_server, origin, out_len);
	(*out_server)[out_len] = '\0';
	return true;
	//@@: } CSIMPLEDOWNLOADER_GETSERVER

}

bool CSimpleDownloader::GetPath(const char* url, char** out_server)
{
	//@@: range CSIMPLEDOWNLOADER_GETPATH {
	size_t len = strlen(url);

	const char* origin = strstr(url, "//");
	if (origin == NULL)
		origin = url;
	else
		origin += 2;

	if ((unsigned)(origin - url) >= len)
		return false;

	origin = strstr(origin, "/");
	if (origin == NULL)
		return false;

	const char* end = &url[len];

	size_t out_len = end - origin;
	if (out_len < 3) // ignore silly values
		return false;

	(*out_server) = new char[out_len+1];
	memcpy(*out_server, origin, out_len);
	(*out_server)[out_len] = '\0';
	return true;
	//@@: } CSIMPLEDOWNLOADER_GETPATH

}

HeaderHandler::HeaderHandler()
{
	m_crlfCounter = 0;
	m_statusCode = 0;
	m_passedHeaders = false;
	m_firstSpace = -1;
	m_contentLength = 0;
}

bool HeaderHandler::HasPassedHeaders()
{
	return m_passedHeaders;
}

int HeaderHandler::StatusCode()
{
	return m_statusCode;
}

bool HeaderHandler::Consume(char c)
{
	if (c == (((m_crlfCounter % 2) == 0) ? '\r' : '\n'))
		m_crlfCounter++;
	else
		m_crlfCounter = 0;

	m_headers << c;

	if (m_statusCode == 0 && c == ' ')
	{
		if (m_firstSpace == -1)
			m_firstSpace = m_headers.tellp();
		else
		{
			u64 pos = m_headers.tellp();
			if (pos - m_firstSpace - 1 == 3)
			{
				m_statusCode = atoi(&(m_headers.str().c_str()[m_firstSpace]));
				DEBUGF1("Status code: %d", m_statusCode);
			}
		}
	}

	if (m_crlfCounter == 4)
	{
		m_contentLength = FindContentLength();
		return m_passedHeaders = true;
	}
	else
		return false;
}

const char* stristr(const char* haystack, const char* needle)
{
	size_t needleLength = strlen(needle);
	for (const char* ptr = haystack; *ptr != NULL; ptr++)
	{
		if (_strnicmp(ptr, needle, needleLength) == 0)
			return ptr;
	}
	return NULL;
}

u64 HeaderHandler::FindContentLength()
{
	std::string stdstring = m_headers.str();
	const char* const headers = stdstring.c_str();

	// TODO: Case-insensitive search
	std::string contentLength = "Content-Length:";

	const char* clHeader = strstr(headers, contentLength.c_str());
	if (!clHeader)
		return 0;

	clHeader += contentLength.size(); // Skip to end of field name

	// Let atoi handle it
	return atoi(clHeader);
}

u64 HeaderHandler::ContentLength()
{
	return m_contentLength;
}

u64 HeaderHandler::HeadersLength()
{
	return m_headers.tellp();
}