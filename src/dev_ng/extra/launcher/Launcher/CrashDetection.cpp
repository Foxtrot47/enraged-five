#include "stdafx.h"

#include "CrashDetection.h"
#include "Globals.h"
#include "Config.h"

#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <regex>

// Most (but not all) of these status codes are duplicated in winnt.h
#pragma warning(push)
#pragma warning(disable: 4005)
#include <ntstatus.h>
#pragma warning(pop)

#include "Util.h"

// in-1234
// out-1234

bool CCrashDetection::CheckInfileOutfilePairs(bool& out_foundOrphan)
{
	std::wstring path;
	if (!GetFolderForInfilesAndOutfiles(path))
	{
		return false;
	}

	std::wstring search = path + std::wstring(L"\\in*");
	//@@: location CCRASHDETECTION_CHECKINFILEOUTFILEPAIRS_CREATE_INFILE_VECTOR
	std::vector<std::wstring> infiles;
	if (!CUtil::EnumerateFilenames(search, infiles))
	{
		return false;
	}

	DEBUGF1("Found %d in-file(s)...", infiles.size());

	for (size_t i = 0; i < infiles.size(); i++)
	{
		std::string spath;
		CUtil::WStringToStdString(infiles[i], spath);
		DEBUGF1(" %s", spath.c_str());
	}

	std::wregex pattern(L"in\\-([0-9]+)");
	for (size_t i = 0; i < infiles.size(); i++)
	{
		const std::wstring& file = infiles[i];
		std::string sfile;
		CUtil::WStringToStdString(file, sfile);
		
		std::wsmatch results;
		if (!regex_match(file, results, pattern))
		{
			// This file doesn't match the pattern.
			
			WARNINGF("'%s' doesn't match.", sfile.c_str());
			continue;
		}

		if (results.size() != 2)
		{
			// There should be two results - the whole matched string, and the first capture group.
			WARNINGF("'%s' gave %d result(s).", sfile.c_str(), results.size());
			continue;
		}

		std::wssub_match match = results[1];
		std::wstring pid_wstring = match.str();

		if (pid_wstring.length() > 10)
		{
			// We also expect the process ID to fit into a DWORD (10 digits or less).
			WARNINGF("'%s' gives PID string of length %d.", sfile.c_str(), pid_wstring.length());
			continue;
		}

		// Convert to long.
		u64 pid_l = wcstoul(pid_wstring.c_str(), NULL, 10);

		// Check if it fits in a DWORD
		if (pid_l > 0xFFFFFFFF)
		{
			WARNINGF("'%S' is too big for a DWORD.", pid_wstring.c_str());
			continue;
		}

		DWORD pid = (DWORD)pid_l;

		// Corresponding out-file
		const std::wstring full_infile = path + std::wstring(L"\\") + file;
		const std::wstring full_outfile = path + std::wstring(L"\\") + std::wstring(L"out-") + pid_wstring;
		DWORD attribs = GetFileAttributesW(full_outfile.c_str());

		if (attribs != INVALID_FILE_ATTRIBUTES && !(attribs &FILE_ATTRIBUTE_DIRECTORY))
		{
			// File exists.  Delete the pair.
			DISPLAYF("'%s' - found matching out-file.", sfile.c_str());
			DeleteFileW(full_infile.c_str());
			DeleteFileW(full_outfile.c_str());
		}
		else
		{
			// File doesn't exist.  See if this process is running.
			if (IsRunningGameProcess(pid))
			{
				// Do nothing; this game instance is still runing
				DISPLAYF("'%s' - process is still running.", sfile.c_str());
			}
			else
			{
				// Orphaned in-file.  Report this and delete it.
				DISPLAYF("'%s' - orphaned in-file.", sfile.c_str());
				out_foundOrphan = true;
				DeleteFileW(full_infile.c_str());
			}
		}

	}
	
	return true;
}

bool CCrashDetection::IsRunningGameProcess(DWORD pid)
{
	// Find the process
	HANDLE processHandle = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, pid);
	if (processHandle == INVALID_HANDLE_VALUE || processHandle == NULL)
	{
		DISPLAYF("Could not open process %d.", pid);
		return false;
	}

	// Get its image name
	WCHAR imagePath[MAX_PATH];
	DWORD imagePathLength = MAX_PATH;

	BOOL querySuccess = QueryFullProcessImageNameW(processHandle, 0, imagePath, &imagePathLength);
	CloseHandle(processHandle);

	if (!querySuccess || imagePathLength == 0)
	{
		DISPLAYF("Could query process image name for pid %d.", pid);
		return false;
	}

	std::wstring imagePathWstring(imagePath);

	// Search for known executable names
	const std::string* executables[] =
	{
#if !RSG_FINAL
		&Constants::DevInstallFilenameBeta.Get(),
		&Constants::DevInstallFilenameBankRelease.Get(),
		&Constants::DevInstallFilenameRelease.Get(),
#endif
		&Constants::FinalInstallFilename.Get()
	};

	for (size_t i = 0; i < sizeof(executables) / sizeof(std::string*); i++)
	{
		std::wstring targetName;
		CUtil::StdStringToStdWString(*executables[i], targetName);

		// Admittedly, someone could spoof our executable name and somehow create a process
		// with the same PID... but it's hard to say what that would achieve.
		if (imagePathWstring.find(targetName) != std::wstring::npos)
		{
			return true;
		}
	}

	return false;
}

bool CCrashDetection::WriteInfile(DWORD processId)
{
	std::ofstream file;
	//@@: location CCRASHDETECTION_WRITEINFILE
	std::wstring path;
	if (GetFolderForInfilesAndOutfiles(path))
	{
		// Create in-file name
		path += std::wstring(L"\\in-");

		wchar_t buffer[11];
		swprintf_s(buffer, L"%lu", processId);

		path += buffer;
		

		// Create in-file
		file.open(path.c_str(), std::ios::binary | std::ofstream::out);
		if (file.good())
		{
			// TODO: Should we write anything? Such as pid?
			file.close();
			return true;
		}
		else
		{
			return false;
		}
	}

	return false;
}

bool CCrashDetection::ReadOutfile(DWORD processId, std::string& out_data)
{
	std::ifstream file;
	std::wstring path;
	if (GetFolderForInfilesAndOutfiles(path))
	{
		// Create out-file name
		path += std::wstring(L"\\out-");

		wchar_t buffer[11];
		swprintf_s(buffer, L"%lu", processId);

		path += buffer;

		// Check it's not too big (100KiB)
		__stat64 stats;
		if (_wstat64(path.c_str(), &stats) != 0)
		{
			ERRORF("Unable to stat out-file!");
			CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_GAME_CRASHED);
			return false;

		}
		else if (stats.st_size > 100 * 1024)
		{
			WARNINGF("Out-file was %lu bytes!", stats.st_size);
			CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_GAME_CRASHED);
			return false;
		}

		// Read file contents
		file.open(path.c_str(), std::ios::binary | std::ifstream::in);
		if (file.good())
		{
			std::stringstream buffer;
			buffer << file.rdbuf();
			out_data.assign(buffer.str());

			return true;
		}
		else
		{
			return false;
		}
	}

	return false;
}

bool CCrashDetection::DeleteEntryAndExitFilePair(DWORD processId)
{
	// Get folder name
	std::ifstream file;
	std::wstring path;
	if (!GetFolderForInfilesAndOutfiles(path))
	{
		return false;
	}

	wchar_t buffer[11];
	swprintf_s(buffer, L"%lu", processId);

	// Make in-file and out-file names
	std::wstring infile_path = path + std::wstring(L"\\in-") + std::wstring(buffer);
	std::wstring outfile_path = path + std::wstring(L"\\out-") + std::wstring(buffer);

	bool failed = false;

	if (!DeleteFileW(infile_path.c_str()))
	{
		failed = true;
	}

	if (!DeleteFileW(outfile_path.c_str()))
	{
		failed = true;
	}

	return !failed;
}

bool CCrashDetection::GetFolderForInfilesAndOutfiles(std::wstring& out_path)
{
	PWSTR pszPath = NULL;
	HRESULT hr = SHGetKnownFolderPath(FOLDERID_LocalAppData, 0, NULL, &pszPath);

	if (SUCCEEDED(hr))
	{
		std::wstring path(pszPath);
		std::wstring temp;

		path += Constants::AppDataSubfolderCompanyName;
		CUtil::CreateDirectoryIfDoesntExist(path);

		path += Constants::AppDataSubfolderProductName;
		CUtil::CreateDirectoryIfDoesntExist(path);

		out_path = path;
	}

	CoTaskMemFree(pszPath);

	return SUCCEEDED(hr);
}


void CCrashDetection::TranslateKnownExitCode(u32 exitCode)
{
	if (exitCode == 0)
	{
		DISPLAYF("Exit code 0x0 indicates a clean game shutdown");
		return;
	}
	else if (exitCode == 1)
	{
		ERRORF("Exit code 0x1 indicates an unexpected game termination");
		return;
	}

	const char* text = NULL;

	switch (exitCode)
	{
	case STATUS_ACCESS_VIOLATION: text = "STATUS_ACCESS_VIOLATION"; break;
	case STATUS_DLL_NOT_FOUND: text = "STATUS_DLL_NOT_FOUND"; break;
	case STATUS_DLL_INIT_FAILED: text = "STATUS_DLL_INIT_FAILED"; break;
	case STATUS_INVALID_IMAGE_FORMAT: text = "STATUS_INVALID_IMAGE_FORMAT"; break;
	default: text = NULL; break;
	}

	if (text)
	{
		ERRORF("Exit code 0x%x indicates a game crash (%s)", exitCode, text);
	}
	else if (exitCode != 0)
	{
		ERRORF("Exit code 0x%x indicates a game crash (unknown cause)", exitCode);
	}
}
