#pragma once

#include <string>
#include <queue>

#include "IDownloader.h"
#include "IPatchCheck.h"
#include "RockstarDownloader.h"

#if RSG_LAUNCHER
#include "ChunkedDownloader.h"
#endif

#define USE_CHUNKED_DOWNLOADER 1 && RSG_LAUNCHER

class Downloader
{
public:

	static Downloader& Instance();
	
	// Instead of having a fa�ade here, just return the current downloader.
	RockstarDownloader* CurrentDownloader() { return m_currentDownloader; }

	void Init();
	void Shutdown();

	void QueueFile(std::string url, std::string localFile, bool backgroundDownload, bool redownload = false, bool chunkedDownload = false);
	void QueuePatch(Patch patch);

	bool IsDownloading() { return !m_PatchQueue.empty(); }

	void DownloadComplete(DownloadCompleteStatus status, std::string filename);
	void DownloadError(const CError& error);

	void StartSynchronously();
	void Stop();
	void Reset();

	enum
	{
		INFINITE_RETRIES = -1
	};
	void SetMaxRetries(int maxRetries);

	void SetPaused(bool paused);

	void SetDownloadListener(IDownloader::DownloadListener* listener);
	bool HasErrorOccurred() { return m_bError; }

	void EnableChunkedDownloading(bool enabled);

private:
	Downloader(void);
	~Downloader(void);

	RockstarDownloader* m_currentDownloader;

	RockstarDownloader* m_regularDownloader;

#if !RSG_LAUNCHER
	typedef RockstarDownloader ChunkedDownloader;
#endif

	ChunkedDownloader* m_chunkedDownloader;

	std::queue<Patch> m_PatchQueue;
	Patch m_currentPatch;

	bool m_bDownloading;
	bool m_bError;

	bool m_bChunkedDownloadingEnabled;

	CWinThread* m_pDownloaderThread;
};
