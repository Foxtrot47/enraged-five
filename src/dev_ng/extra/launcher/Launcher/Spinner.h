#pragma once


// CSpinner

class CSpinner : public CWnd
{
	DECLARE_DYNAMIC(CSpinner)

public:
	CSpinner();
	virtual ~CSpinner();

	void QueuePaint();

protected:
	afx_msg void OnPaint();
	BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	DECLARE_MESSAGE_MAP()

	CRect m_bounds;
	bool m_autoRepaint;
	bool m_timerRunning;

	float* m_scratchBuffer;

	void MakeCache(int width, int height);
	float* m_cachedBrightness;
	float* m_cachedAngle;
	int m_cacheWidth, m_cacheHeight;

};


