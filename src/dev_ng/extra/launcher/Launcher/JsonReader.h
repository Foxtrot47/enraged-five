#pragma once

#include <string>

class JsonReader
{
public:
	JsonReader(const char* input);

	bool IsValid();

	JsonReader GetFirstChild();
	JsonReader GetNextSibling();

	JsonReader GetChild(const char* name);

	bool NameEquals(const char* value);
	bool StringValueEquals(const char* value);
	bool GetStringValue(std::string& out_value);
	bool GetUnquotedValue(std::string& out_value);

private:
	const char* m_data;

	const char* GetNextNonWhitespaceChar(const char* input);
	const char* ReadToPairSecondElement(const char* openingQuote);
	const char* ReadPastQuotedString(const char* openingQuote);
	bool ValidStartOfElement(const char* input);
	const char* ReadPastElement(const char* input);
	const char* ReadPastUnquotedValue(const char* input);
	bool QuotedJsonStringEqualToString(const char* openingQuote, const char* string);
	bool ReadQuotedString(const char* openingQuote, std::string& out_value);
};