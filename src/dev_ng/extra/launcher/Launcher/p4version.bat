setlocal enabledelayedexpansion
echo Determining the CL that $(RS_CODEBRANCH)\extra\launcher was synced to...
for /f "usebackq delims=" %%i in (`p4 changes -m 1 %RS_CODEBRANCH%\extra\launcher...#have`) do (
	set MYVAR=%%i
	set MYVAR=!MYVAR:"='!
	set MYVAR=!MYVAR:%%=x!
)
echo MYVAR is !MYVAR!
echo #define P4VERSION_STRING "!MYVAR!" > p4version.h