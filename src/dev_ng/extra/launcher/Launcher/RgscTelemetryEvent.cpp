#include "stdafx.h"

#include "RgscTelemetryEvent.h"

#include "Channel.h"

RgscTelemetryEvent::RgscTelemetryEvent(MetricName metricName)
{
	m_root = new TiXmlElement(GetXmlStringNameFromId(XSN_METRIC_ROOT_TAG));
	m_root->SetAttribute(GetXmlStringNameFromId(XSN_METRIC_NAME_ATTR), GetMetricNameFromId(metricName));
	m_root->LinkEndChild(new TiXmlElement(GetXmlStringNameFromId(XSN_FIELD_CONTAINER_TAG)));
}

RgscTelemetryEvent::RgscTelemetryEvent(const TiXmlElement* copyFrom)
{
	m_root = copyFrom->Clone()->ToElement();
}

RgscTelemetryEvent::~RgscTelemetryEvent()
{
	delete m_root;
}

void RgscTelemetryEvent::AddIntField(FieldName fieldName, u64 value)
{
	TiXmlElement* field = CreateLinkedField(fieldName, FTN_INT);
	if (field)
	{
		char buffer[30];
		sprintf_s(buffer, "%llu", value);
		field->LinkEndChild(new TiXmlText(buffer));
	}
	else
	{
		ERRORF("Unable to add int field!");
	}
}

void RgscTelemetryEvent::AddStringField(FieldName fieldName, const char* value)
{
	TiXmlElement* field = CreateLinkedField(fieldName, FTN_STRING);
	if (field)
	{
		field->LinkEndChild(new TiXmlText(value));
	}
	else
	{
		ERRORF("Unable to add string field!");
	}
}

void RgscTelemetryEvent::AddArrayField(FieldName fieldName, const std::vector<const RgscTelemetryEvent*>& subEvents)
{
	TiXmlElement* field = CreateLinkedField(fieldName, FTN_ARRAY);
	if (field)
	{
		for (size_t i = 0; i < subEvents.size(); i++)
		{
			const RgscTelemetryEvent* subEvent = subEvents[i];
			const TiXmlElement* subEventFields = subEvent->GetFieldsTag();

			if (subEventFields)
			{
				field->LinkEndChild(subEventFields->Clone());
			}
		}
	}
	else
	{
		ERRORF("Unable to add string field!");
	}
}

const char* RgscTelemetryEvent::GetMetricNameFromId(MetricName id)
{
	switch (id)
	{
	case MN_LAUNCHER_METRIC: return "PC_INSTALLER_ACTION";
	case MN_DEBUG_METRIC: return "PC_INSTALLER_ACTION_DEBUG";
	default: return "ERROR_UNRECOGNISED_METRIC_NAME";
	}
}

const char* RgscTelemetryEvent::GetFieldNameFromId(FieldName id)
{
	switch (id)
	{
	case FN_EVENT_TYPE: return "e";
	case FN_EVENT_TIMESTAMP: return "et";
	case FN_EVENT_PARAMETER: return "p";
	case FN_VERSION_INFO: return "v";
	case FN_DOWNLOAD_TYPE: return "dlt";
	case FN_BYTES_DOWNLOADED: return "bts";
	case FN_DURATION: return "dur";
	case FN_CONNECTIONS_OPENED: return "opn";
	case FN_CONNECTIONS_ERRORED: return "err";
	case FN_VERIFICATIONS_FAILED: return "vrf";
	case FN_COUNTRY_CODE: return "s";
	case FN_IP_ADDRESS: return "ip";
	case FN_DEBUG_ARRAY: return "dbg";
	case FN_DEBUG_KEY: return "n";
	case FN_DEBUG_VALUE: return "c";
	case FN_FILENAME: return "f";
	default: return "??";
	}
}

const char* RgscTelemetryEvent::GetXmlStringNameFromId(XmlStringName id)
{
	switch (id)
	{
	case XSN_METRIC_ROOT_TAG: return "metric";
	case XSN_METRIC_NAME_ATTR: return "name";
	case XSN_FIELD_CONTAINER_TAG: return "fields";
	case XSN_FIELD_TAG: return "field";
	case XSN_FIELD_NAME_ATTR: return "name";
	case XSN_FIELD_TYPE_ATTR: return "type";
	default: return "UnknownXmlString";
	}
}

const char* RgscTelemetryEvent::GetFieldTypeNameFromId(FieldTypeName id)
{
	switch (id)
	{
	case FTN_INT: return "int";
	case FTN_STRING: return "string";
	case FTN_ARRAY: return "array";
	default: return "unknowntype";
	};
}

TiXmlElement* RgscTelemetryEvent::GetFieldsTag()
{
	return m_root->FirstChildElement(GetXmlStringNameFromId(XSN_FIELD_CONTAINER_TAG));
}

const TiXmlElement* RgscTelemetryEvent::GetFieldsTag() const
{
	return m_root->FirstChildElement(GetXmlStringNameFromId(XSN_FIELD_CONTAINER_TAG));
}


TiXmlElement* RgscTelemetryEvent::CreateLinkedField(FieldName fieldName, FieldTypeName typeName)
{
	TiXmlElement* fieldContainer = GetFieldsTag();
	if (fieldContainer)
	{
		TiXmlElement* field = new TiXmlElement(GetXmlStringNameFromId(XSN_FIELD_TAG));
		field->SetAttribute(GetXmlStringNameFromId(XSN_FIELD_NAME_ATTR), GetFieldNameFromId(fieldName));
		field->SetAttribute(GetXmlStringNameFromId(XSN_FIELD_TYPE_ATTR), GetFieldTypeNameFromId(typeName));

		fieldContainer->LinkEndChild(field);

		return field;
	}
	else
	{
		return NULL;
	}
}

void RgscTelemetryEvent::AppendToXml(TiXmlElement* appendTo) const
{
	appendTo->LinkEndChild(m_root->Clone());
}

bool RgscTelemetryEvent::WriteAsJson(std::string& out_metricName, std::string& out_json) const
{
	const char* metricName = m_root->Attribute(GetXmlStringNameFromId(XSN_METRIC_NAME_ATTR));
	if (!metricName)
	{
		WARNINGF("No name for metric!");
		return false;
	}

	out_metricName = metricName;

	const TiXmlElement* fields = GetFieldsTag();
	if (!fields)
	{
		WARNINGF("No fields in metric!");
		return false;
	}

	WriteFieldContainerAsJson(out_json, fields, true);

	return true;
}

void RgscTelemetryEvent::WriteFieldContainerAsJson(std::string& out_json, const TiXmlElement* fields, bool omitBraces)
{
	const char* fieldTagName = GetXmlStringNameFromId(XSN_FIELD_TAG);

	if (!omitBraces)
		out_json += "{";

	bool first = true;
	for (const TiXmlElement* element = fields->FirstChildElement(fieldTagName); element; element = element->NextSiblingElement(fieldTagName))
	{
		WriteFieldAsJson(out_json, element, !first);

		if (first)
			first = false;
	}

	if (!omitBraces)
		out_json += "}";
}

void RgscTelemetryEvent::WriteFieldAsJson(std::string& out_json, const TiXmlElement* field, bool prependComma)
{
	const char* fieldNameAttr = GetXmlStringNameFromId(XSN_FIELD_NAME_ATTR), *fieldTypeAttr = GetXmlStringNameFromId(XSN_FIELD_TYPE_ATTR);

	const char* fieldName = field->Attribute(fieldNameAttr);
	if (!fieldName)
	{
		WARNINGF("Field name missing!");
		return;
	}

	const char* fieldTypeString = field->Attribute(fieldTypeAttr);
	if (!fieldTypeString)
	{
		WARNINGF("Field type missing!");
		return;
	}

	FieldTypeName fieldTypeEnum;
	if (!ParseFieldTypeName(fieldTypeString, fieldTypeEnum))
	{
		WARNINGF("Unrecognised field type!");
		return;
	}

	// Start writing


	// Append comma if needs be
	if (prependComma)
		out_json += ",";


	// Append field name
	out_json += "\"";
	out_json += fieldName;
	out_json += "\":";

	if (fieldTypeEnum == FTN_INT)
	{
		const char* text = field->GetText();
		if (text && text[0] != '\0')
			out_json += text;
		else
		{
			WARNINGF("Empty int field!");
			out_json += "0";
		}
	}
	else if (fieldTypeEnum == FTN_STRING)
	{
		const char* text = field->GetText();
		if (text && text[0] != '\0')
		{
			out_json += "\"";
			out_json += text;
			out_json += "\"";
		}
		else
		{
			WARNINGF("Empty string field!");
			out_json += "\"\"";
		}
	}
	else
	{
		const char* fieldContainerTag = GetXmlStringNameFromId(XSN_FIELD_CONTAINER_TAG);

		out_json += "[";
		bool first = true;
		for (const TiXmlElement* subobject = field->FirstChildElement(fieldContainerTag); subobject; subobject = subobject->NextSiblingElement(fieldContainerTag))
		{
			// Prepend array comma
			if (!first)
			{
				out_json += ",";
			}
			else
			{
				first = false;
			}

			WriteFieldContainerAsJson(out_json, subobject);
		}

		out_json += "]";
	}
}

bool RgscTelemetryEvent::ParseFieldTypeName(const char* fieldTypeString, FieldTypeName& out_fieldType)
{
	for (int i = 0; i < FTN_MAX; i++)
	{
		FieldTypeName ftn = (FieldTypeName)i;
		const char* checkForName = GetFieldTypeNameFromId(ftn);
		if (strcmp(checkForName, fieldTypeString) == 0)
		{
			out_fieldType = ftn;
			return true;
		}
	}

	return false;
}

const char* RgscTelemetryEvent::GetMetricXmlTagName()
{
	return GetXmlStringNameFromId(XSN_METRIC_ROOT_TAG);
}

bool RgscTelemetryEvent::GetFieldAsString(FieldName fieldName, std::string& out_string)
{
	const TiXmlElement* fieldsContainer = GetFieldsTag();

	if (fieldsContainer)
	{
		const char* fieldTagName = GetXmlStringNameFromId(XSN_FIELD_TAG);
		const char* fieldNameAttr = GetXmlStringNameFromId(XSN_FIELD_NAME_ATTR);
		const char* chosenFieldName = GetFieldNameFromId(fieldName);

		for (const TiXmlElement* element = fieldsContainer->FirstChildElement(fieldTagName); element; element = element->NextSiblingElement(fieldTagName))
		{
			const char* fieldNameString = element->Attribute(fieldNameAttr);

			if (fieldNameString && strcmp(fieldNameString, chosenFieldName) == 0)
			{
				const char* text = element->GetText();

				if (text && text[0] != '\0')
				{
					out_string.assign(text);
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}

	return false;
}
