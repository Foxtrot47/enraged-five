#include "stdafx.h"

#include "VersionManager.h"

#if RSG_COMBINED_VERSIONING

#include "RockstarDownloader.h"
#include "Util.h"
#include "Config.h"
#include "Globals.h"
#include "OnlineConfig.h"
#include "GamePatchCheck.h"
#include "RgscTelemetryManager.h"

#include <regex>

bool VersionManager::sm_checkedVersions = false;
Patch VersionManager::sm_bootstrap, VersionManager::sm_launcher, VersionManager::sm_socialclub, VersionManager::sm_game, VersionManager::sm_mtl;
std::vector<Patch> VersionManager::sm_manifests;
u64 VersionManager::sm_releaseDateSystemMillis = 0;

bool VersionManager::CheckVersionsSynchronous()
{
	
	//@@: range VERSION_CHECKVERSIONSSYNCHORNOUS_CALLBACKS {
	struct Callback : public IDownloader::DownloadListener
	{
		bool m_error;
		Callback() : m_error(false) {}

		virtual void DownloadComplete(const Patch& /*patch*/, DownloadCompleteStatus /*status*/)
		{
		}

		virtual void DownloadStatus(const Patch& /*patch*/, DownloadCompleteStatus /*status*/, u64 /*totalBytesComplete*/, u64 /*totalBytesInFile*/, u64 /*bytesJustDownloaded*/)
		{
		}

		virtual void DownloadError(const Patch& /*patch*/, const CError& /*error*/)
		{
			m_error = true;
		}

	} callback;
	//@@: } VERSION_CHECKVERSIONSSYNCHORNOUS_CALLBACKS


#if !RSG_BOOTSTRAP
	if (sm_checkedVersions)
	{
		DEBUGF1("Already checked versioning.");
		return true;
	}
#else
	if (sm_checkedVersions)
	{
		return true;
	}
#endif

	//@@: location VERSION_CHECKVERSIONSSYNCHORNOUS_GETBUILD_VERSION	
	Version installedBuild = GetBuildVersion();
	
	DEBUGF3("Versioning for %d.%d.%d.%d", installedBuild.major, installedBuild.minor, installedBuild.build, installedBuild.revision);


	std::string tempLocation;
	//@@: range VERSION_CHECKVERSIONSSYNCHORNOUS {
	CUtil::PrependTempoaryFolderPath(tempLocation, "versioning.xml");

	std::string url;
	std::string server, docpath;
	CUtil::GetServerAndDocpathForEnv(server, docpath, Constants::VersioningPathProd);

	url = server + docpath;

	RockstarDownloader downloader;

	downloader.SetListener(&callback);
	downloader.SetCurrentFile(url, tempLocation, false, true);
	downloader.SetMaxRetries(OnlineConfig::PatchRetries);
	downloader.StartSynchronously();

	if (callback.m_error || AUTOTEST_FAIL)
	{
		CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_DOWNLOAD_VERSIONING_FAILED);
		ERRORF("Can't get version info.");
		return false;
	}

	//TiXmlDocument doc(tempLocation.c_str());
	TiXmlDocument doc;

	std::wstring wpath;
	CUtil::StdStringToStdWString(tempLocation, wpath);

	FILE* file = _wfopen(wpath.c_str(), L"rb");

	if (!file)
	{
		ERRORF("Unable to open options file!");
		return false;
	}

	DISPLAYF("Opened options file.");

	
	bool bLoaded = doc.LoadFile(file, TIXML_ENCODING_UTF8);
	if (!bLoaded || AUTOTEST_FAIL)
	{
		CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_READ_VERSIONING_FAILED);
		ERRORF("Can't load downloaded version xml!");
		fclose(file);
		return false;
	}

	FILETIME serverFt;
	if (!RockstarDownloader::GetServerTime(url, serverFt) || AUTOTEST_FAIL)
	{
		CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_GET_SERVER_TIME_FAILED);
		ERRORF("Can't get server time.");
		fclose(file);
		return false;
	}

	u64 serverTimeSecondsSinceEpoch = FiletimeToSecondsSinceEpoch(&serverFt);

	FILETIME localTimeFt;
	GetSystemTimeAsFileTime(&localTimeFt);
	u64 localTimeSecondsSinceEpoch = FiletimeToSecondsSinceEpoch(&localTimeFt);

	const u64 toleratedClockDifferenceSeconds = 60 * 60 * 24 * 30; // Roughly a month +/- ~3 days

	if (localTimeSecondsSinceEpoch < serverTimeSecondsSinceEpoch - toleratedClockDifferenceSeconds || localTimeSecondsSinceEpoch > serverTimeSecondsSinceEpoch + toleratedClockDifferenceSeconds || AUTOTEST_FAIL)
	{
		ERRORF("System clock is off by a month or more; expect SSL/TLS certificate problems.");
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_CLOCK_WRONG);
		fclose(file);
		return false;
	}

	//@@: location VERSION_CHECKVERSIONSSYNCHORNOUS_HANDLE_XML_DOCUMENT
	TiXmlHandle docHandle(&doc);

	bool restart = true;
	for (TiXmlElement* build = NULL; build || restart; build = build->NextSiblingElement("Build"))
	{
		if (restart)
		{
			restart = false;
			build = docHandle.FirstChild("Versioning").FirstChild("Build").ToElement();
			if (!build)
			{
				ERRORF("No builds!");
				break;
			}
		}
		Version buildVersion;

		// Find our target version
		if (!ReadVersion(build->Attribute("version"), buildVersion))
		{
			DEBUGF3("Can't read version from attribute \"version\"");
			continue;
		}

		if (buildVersion != installedBuild)
		{
			//DEBUGF3("Ignoring build version %d.%d.%d.%d (doesn't match %d.%d.%d.%d)", buildVersion.major, buildVersion.minor, buildVersion.build, buildVersion.revision, installedBuild.major, installedBuild.minor, installedBuild.build, installedBuild.revision);
			continue;
		}

		DEBUGF3("Reading build for version %d.%d.%d.%d", buildVersion.major, buildVersion.minor, buildVersion.build, buildVersion.revision);

		// Check for a build upgrade (should be very infrequent)
		Version upgradeToVersion;
		//@@: location VERSIONMANAGER_CHECKVERSIONSSYNCHRONOUS_READ_UPGRADE_TO
		if (ReadVersion(build->Attribute("upgradeTo"), upgradeToVersion))
		{
			DEBUGF3("Upgrade available to version %d.%d.%d.%d", upgradeToVersion.major, upgradeToVersion.minor, upgradeToVersion.build, upgradeToVersion.revision);

			s64 timeRemainingSeconds = 0;
			if (const char* releaseDateString = build->Attribute("upgradeDate"))
			{
				if (strlen(releaseDateString) > 0)
				{
					SYSTEMTIME releaseDateSystime;
					InternetTimeToSystemTimeA(releaseDateString, &releaseDateSystime, 0);
					FILETIME releaseDateFt;
					SystemTimeToFileTime(&releaseDateSystime, &releaseDateFt);

					s64 releaseDateSecondsSinceEpoch = FiletimeToSecondsSinceEpoch(&releaseDateFt);
					timeRemainingSeconds = releaseDateSecondsSinceEpoch - serverTimeSecondsSinceEpoch;

					sm_releaseDateSystemMillis = (u64)((s64)GetTickCount64() + timeRemainingSeconds * 1000LL);


					DEBUGF3("Upgrade available in %lld seconds", timeRemainingSeconds);
				}
			}

			if (timeRemainingSeconds <= 0)
			{
				installedBuild = upgradeToVersion;

				// Restart
				DEBUGF3("Restarting scan with target version %d.%d.%d.%d", installedBuild.major, installedBuild.minor, installedBuild.build, installedBuild.revision);
				restart = true;
				continue;
			}
		}

		DEBUGF3("Using build version %d.%d.%d.%d", buildVersion.major, buildVersion.minor, buildVersion.build, buildVersion.revision);

		ReadElement(build, "Bootstrap", sm_bootstrap);
		ReadElement(build, "Launcher", sm_launcher);
		ReadElement(build, "SocialClub", sm_socialclub);
		ReadMultipleElements(build, "Manifest", sm_manifests);
		ReadElement(build, "Game", sm_game);
		ReadElement(build, "Mtl", sm_mtl);

		sm_checkedVersions = true;
		fclose(file);
		return true;
	}

	fclose(file);

	WARNINGF("No build versioning info found.");

	CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_READ_VERSIONING_FAILED);

	//@@: } VERSION_CHECKVERSIONSSYNCHORNOUS

	sm_checkedVersions = true;
	return false;
}

bool VersionManager::GetBootstrapPatch(Patch& out_patch)
{
	CheckVersionsSynchronous();
	//@@: location VERSIONMANAGER_GETBOOTSTRAPPATCH_AFTER_CVS_CALL
	if (sm_bootstrap._Version != Version(0,0,0,0))
	{
		out_patch = sm_bootstrap;
		return true;
	}

	return false;
}

bool VersionManager::GetLauncherPatch(Patch& out_patch)
{
	CheckVersionsSynchronous();
	//@@: location VERSIONMANAGER_GETLAUNCHERPATCH_AFTER_CVS_CALL
	if (sm_launcher._Version != Version(0,0,0,0))
	{
		out_patch = sm_launcher;
		return true;
	}

	return false;
}

bool VersionManager::GetSocialClubPatch(Patch& out_patch)
{
	CheckVersionsSynchronous();
	//@@: location VERSIONMANAGER_GETSOCIALCLUBPATCH_AFTER_CVS_CALL
	if (sm_socialclub._Version != Version(0,0,0,0))
	{
		out_patch = sm_socialclub;
		return true;
	}

	return false;
}

bool VersionManager::GetGamePatch(Patch& out_patch)
{
	CheckVersionsSynchronous();
	//@@: location VERSIONMANAGER_GETGAMEPATCH_AFTER_CVS_CALL
	if (sm_game._Version != Version(0,0,0,0))
	{
		out_patch = sm_game;
		return true;
	}

	return false;
}

bool VersionManager::GetMtlPatch(Patch& out_patch)
{
	CheckVersionsSynchronous();
	if (sm_mtl._Version != Version(0,0,0,0))
	{
		out_patch = sm_mtl;
		return true;
	}

	return false;
}

bool VersionManager::GetDigitalDataManifestUrls(std::vector<Patch>& out_manifests)
{
	if (!CheckVersionsSynchronous())
	{
		return false;
	}

	//@@: location VERSIONMANAGER_GETDIGITALDATAMANIFESTURLS_INSERT_MANIFESTS
	out_manifests.insert(out_manifests.end(), sm_manifests.begin(), sm_manifests.end());

	return true;
}

bool VersionManager::GetReleaseDateSystemMillis(u64& out_seconds)
{
	CheckVersionsSynchronous();
	//@@: location VERSIONMANAGER_GETRELEASEDATESYSTEMMILLIS_INSERT_MANIFESTS
	if (sm_releaseDateSystemMillis != 0)
	{
		out_seconds = sm_releaseDateSystemMillis;
		return true;
	}

	return false;
}

void VersionManager::Reset()
{
	sm_checkedVersions = false;
	sm_releaseDateSystemMillis = 0;
	sm_bootstrap = sm_launcher = sm_socialclub = sm_game = Patch();
	sm_manifests.clear();
}


s64 VersionManager::FiletimeToSecondsSinceEpoch(FILETIME* ft)
{
	//@@: location VERSIONMANAGER_FILETIMETOSECONDSSINCEPOCH
	LARGE_INTEGER lint;
	lint.LowPart = ft->dwLowDateTime;
	lint.HighPart = ft->dwHighDateTime;
	return lint.QuadPart / (10 * 1000 * 1000);
}

bool VersionManager::ReadVersion(const char* versionString, Version& version)
{
	if (!versionString || strlen(versionString) == 0)
		return false;

	version = Version(std::string(versionString));

	return true;
}

void VersionManager::ReadElement(TiXmlElement* build, char* elementName, Patch& patch)
{
	if (!build)
		return;

	//@@: range VERSIONMANAGER_READELEMENT_READ_ELEMENT {
	TiXmlElement* element = build->FirstChildElement(elementName);
	if (!element)
	{
		DEBUGF1("Element %s not found.", elementName);
		return;
	}

	//@@: location VERSIONMANAGER_READELEMENT_READ_ELEMENT_CHECK_VERSION_ATTRIBUTE
	if (const char* string = element->Attribute("version"))
	{
		patch._Version = Version(string);
	}

	if (const char* string = element->Attribute("url"))
	{
		patch.PatchURL = string;
		patch.LocalFilename = std::regex_replace(string, std::regex(".*/"), "");
	}

	patch.AlwaysDownload = false;
	patch.ChunkedDownload = false;
	patch.CommandlineArguments = "/S";
	patch.DeleteOnCompletion = true;
	patch.Installed = false;

	DEBUGF1("%s: Version %d.%d.%d.%d", elementName, patch._Version.major, patch._Version.minor, patch._Version.build, patch._Version.revision);
	if (patch.LocalFilename.empty())
	{
		DEBUGF1("%s: No URL.", elementName);
	}
	else
	{
		DEBUGF1("%s: URL: %s", elementName, patch.PatchURL.c_str());
	}
	//@@: } VERSIONMANAGER_READELEMENT_READ_ELEMENT

}

void VersionManager::ReadMultipleElements(TiXmlElement* build, char* elementName, std::vector<Patch>& patches)
{

	if (!build)
		return;

	//@@: range VERSIONMANAGER_READMULTIPLEELEMENTS_READ_ELEMENTS {
	TiXmlElement* element = build->FirstChildElement(elementName);
	//@@: location VERSIONMANAGER_READMULTIPLEELEMENTS_READ_ELEMENTS_CHECK_NULL_ELEMENT 
	if (!element)
	{
		DEBUGF1("Element %s not found.", elementName);
		return;
	}

	for (element; element; element = element->NextSiblingElement(elementName))
	{
		DEBUGF1("Reading element %s.", elementName);
		Patch patch;
		if (const char* string = element->Attribute("version"))
		{
			patch._Version = Version(string);
		}

		if (const char* string = element->Attribute("url"))
		{
			patch.PatchURL = string;
			patch.LocalFilename = std::regex_replace(string, std::regex(".*/"), "");
		}

		if (const char* string = element->Attribute("sku"))
		{
			patch.SkuName = string;
		}

		patch.AlwaysDownload = false;
		patch.ChunkedDownload = false;
		patch.CommandlineArguments = "/S";
		patch.DeleteOnCompletion = true;
		patch.Installed = false;

		DEBUGF1("%s: Version %d.%d.%d.%d", elementName, patch._Version.major, patch._Version.minor, patch._Version.build, patch._Version.revision);
		if (patch.LocalFilename.empty())
		{
			DEBUGF1("%s: No URL.", elementName);
		}
		else
		{
			DEBUGF1("%s: URL: %s", elementName, patch.PatchURL.c_str());
		}

		patches.push_back(patch);
	}
	//@@: } VERSIONMANAGER_READMULTIPLEELEMENTS_READ_ELEMENTS


}

Version VersionManager::GetBuildVersion()
{
	Version minimumVersion("1.0.757.2");

	//@@: range VERSIONMANAGER_GETBUILDVERSION {
	Version exeVersion = IPatchCheck::GetVersionForFile(GamePatchCheck::GetGameExecutableFullPath());

	//@@: location VERSIONMANAGER_GETBUILDVERSION_CHECK_AGAINST_ZERO
	if (!(exeVersion == Version(0,0,0,0)))
	{
		if (exeVersion > minimumVersion)
			return exeVersion;
	}

	Version versionTxt;
	std::string versionString;

	// Read version file
	if (CUtil::ReadSmallFileAsString("version.txt", versionString))
	{
		versionString += '\0';
		versionTxt = Version(versionString);
	}

	if (versionTxt > minimumVersion)
		return versionTxt;

	return minimumVersion;
	//@@: } VERSIONMANAGER_GETBUILDVERSION 
}

#endif