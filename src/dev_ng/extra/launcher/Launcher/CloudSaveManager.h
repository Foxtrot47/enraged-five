#pragma once

#include "cloudsave_interface.h"

#include "JsonReader.h"

#include <vector>

class SocialClub;

class CloudSaveManager
{
public:
	static void Initialize(SocialClub* sc);
	static void Shutdown();

	static CloudSaveManager* Instance();

	void ScUpdate();

	enum CloudSaveManagerState
	{
		eState_None,
		eState_WaitingToCreateAsyncStatus,
		eState_WaitingForBetaCheck,
		eState_WaitingForEnabledCheck,
		eState_WaitingForBackupSearch,
		eState_WaitingForBackupResolutions,
		eState_WaitingForBackupRestoration,
		eState_WaitingForModificationsList,
		eState_WaitingForConflictMetadata,
		eState_WaitingForConflictResolutions,
		eState_WaitingForBackupCreation,
		eState_WaitingForFileUploads,
		eState_WaitingForFileDownloads,
		eState_WaitingForFileDeletions,
		eState_SwappingDownloadedFiles
	};

	enum SyncReportStatus
	{
		eStatus_NotInBeta,
		eStatus_Disabled,
		eStatus_Succeeded,
		eStatus_Failed,
		eStatus_Cancelled,
		// Add new metrics at the end and update CloudSaveManager::SendSyncTelemetry
		eStatus_Banned
	};

	void StartCloudSync(bool exitAfterSync);
	void CancelCloudSync();
	void DisableCloudSaves();

	bool IsSyncingCloudSaves();
	bool IsWaitingForUserAction();
	bool IsTimedOut() { return m_timedOut; }
	void OnBetaCheckComplete();
	void OnEnabledCheck(bool cloudSavesEnabled);
	void OnBackupSearchComplete();
	void OnBackupResolutionsReceived(JsonReader resolutionList);
	void OnModificationListComplete();
	void OnConflictMetadataDownloaded();
	void OnConflictResolutionsReceived(JsonReader resolutionList);
	void OnFileTransmissionsComplete();
	void OnCloudSaveEnabledUpdated(const char* rosTitleName);
	bool IsCloudSaveEnabled();
	void OnSignIn(rgsc::RockstarId rockstarId);
	void OnSignOut();

	void SendConflictResolutionTelemetry();
	void SendSyncTelemetry(SyncReportStatus status);
	void SendBackupRestorationTelemetry();

private:
	CloudSaveManager(SocialClub* sc);
	~CloudSaveManager();

	SocialClub* m_socialClub;
	rgsc::ICloudSaveManifestV2* m_cloudSaveManifest;
	rgsc::ICloudSaveOperationProgressV1* m_cloudSaveOperationProgress;
	rgsc::ICloudSaveOperationProgressV2* m_cloudSaveOperationProgressV2;
	rgsc::IAsyncStatusLatestVersion* m_asyncStatus;
	rgsc::RockstarId m_RockstarId;

	CloudSaveManagerState m_state;
	int m_swapFileIndex;
	bool m_hasCheckedForBetaAccess;
	bool m_hasBetaAccess;
	bool m_cloudSavesEnabled;
	bool m_exitAfterSync;
	bool m_shownCancelButton;
	bool m_cancelled;
	int m_syncCount;
	u64 m_uStartTime;
	bool m_timedOut;

	u64 m_uSyncStartTime;
	int m_lastReportedSyncIndex;
	CloudSaveManagerState m_lastReportedSyncState;

	int m_operationProgress;
	int m_lastReportedOperationProgress;

	u64 m_uResolutionStartTime;
	u64 m_uResolutionEndTime;

	int m_NumBackupFiles;
	u64 m_uBackupRestoreStartTime;
	u64 m_uBackupRestoreEndTime;

	void SendCloudSaveConfigurationToSDK();
	void OnBanned();
	void StartBetaCheck();
	void StartEnabledCheck();
	bool TryCreateAsyncStatus();
	void StartRestoringNextBackup();
	void StartIdentifyingConflicts();

	void UnixTimeToUtcString(u64 time, std::string& out_string);

	void OnFinishCloudSync();
	void OnError();

	void NotifyUiOfSync();
	void UpdateSyncMessage();

	bool CanShowFullProgress();
	void UpdateProgress();

	static bool StringNullOrEmpty(const char* str);

	struct BackupRestorationInfo
	{
		BackupRestorationInfo(int file, int backup) : m_fileIndex(file), m_backupIndex(backup) {}
		int m_fileIndex, m_backupIndex;
	};

	std::vector<BackupRestorationInfo> m_backupsToRestore;
};