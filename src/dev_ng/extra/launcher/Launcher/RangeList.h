#pragma once

#include <vector>
#include <string>

#include "Types.h"

struct Range
{
	// Do not add or remove fields - this matches the format of .part file entries
	u64 m_offset;
	u64 m_length;

	Range() {}
	Range(u64 offset, u64 length) : m_offset(offset), m_length(length) {}

	bool operator==(const Range& other) { return m_offset == other.m_offset && m_length == other.m_length; }

	void Set(u64 offset, u64 length) { m_offset = offset; m_length = length; }

	u64 GetEnd() const { return m_offset + m_length; }

	bool Contains(u64 offset) const { return offset >= m_offset && offset < m_offset + m_length; }

	bool Contains(u64 offset, u64 length) const
	{
		u64 inLastByte = offset + length - 1;
		u64 myLastByte = m_offset + m_length - 1;

		return inLastByte > m_offset && offset < myLastByte;
	}

	bool ContainsCompleteRange(const Range& range) const
	{
		return range.m_offset >= m_offset && range.GetEnd() <= GetEnd();
	}
};

struct Sha256Range
{
	// Do not add or remove fields - this matches the format of .hash file entries
	Range m_range;
	char m_hash[32];
};

class RangeList
{
public:
	RangeList() {}

	void AddRange(const Range& range);
	void RemoveRange(const Range& range);
	void Clear()
	{
		m_ranges.clear();
#if RSG_DEBUG
		m_debugHistory.clear();
#endif
	}
	bool IsEmpty() const { return m_ranges.empty(); }
	size_t Size() const { return m_ranges.size(); }

	bool ContainsCompleteRange(const Range& range) const;

	const std::vector<Range>& GetRanges() const { return m_ranges; }

private:
	std::vector<Range> m_ranges;
#if RSG_DEBUG
	std::string m_debugHistory;
#endif
};

class PartiallyCompleteRange
{
public:
	PartiallyCompleteRange();

	void Init(const Sha256Range& range);
	void MoveFrom(PartiallyCompleteRange& range);
	void Cleanup();

	const Sha256Range& GetRange() { return m_sha256Range; }
	const char* GetData() const { return &m_data[0]; }

	bool IsComplete() const;
	bool CheckHash();
	void WriteData(u64 globalOffset, u64 length, const char* data);
private:
	Sha256Range m_sha256Range;
	char* m_data;
	RangeList m_progress;
};