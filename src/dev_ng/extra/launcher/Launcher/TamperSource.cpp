#include "stdafx.h"
#include "TamperSource.h"
#include "Config.h"
#include "Channel.h"
#define WIN32_CRYPTO 1
#if !(RSG_PRELOADER_BOOTSTRAP || RSG_PRELOADER_LAUNCHER)
#if WIN32_CRYPTO
#include <WinCrypt.h>
#define AES_BLOCK_SIZE 16
struct KeyImportBlob
{
	BLOBHEADER header;
	DWORD keySize;
	BYTE keyData[32];
};
#else
#include <openssl/aes.h>
#endif

// Commenting out tue to anti-cheat_policy
#if ENABLE_TAMPER_ACTIONS
void InitTamperCallsToNop(bool runCode)
{


	if (runCode)
	{
		//@@: range INITTAMPERCALLSTONOP_RANGE {
		TamperAction_Download(0xD9);
		TamperAction_PopUp(0x64);
		TamperAction_DupKeyboard(0xEA);
		TamperAction_HashFail(0x55);
		TamperAction_AlterState(0x6A);
		TamperAction_Flicker(0xF5);
		TamperAction_BringToFront(0xED);
		TamperAction_DamageCode(0x18);
		TamperAction_DamageCodeTwo(0x6C);
		TamperAction_RedHerring(0xE8);
		TamperAction_NoAction(0x61);
		//@@: } INITTAMPERCALLSTONOP_RANGE
	}

}

void TamperAction_NoAction(int parm)
{
	float y=5.498416795f;
	float f1=-9.2f * parm;
	(void)y;
	(void)f1;
}
void TamperAction_Download(int parm)
{

	DEBUGF1("TamperAction_Download");
	float x=5.8875736100f;
	float y=5.49811795f;
	float x1=5.142929354f;
	float y1=3.737719392f;
	float x2=9.80535768f;
	float y2=7.899937346f;
	float x3=1.89150673f;
	float y3=2.019506687f;
	float x4=9.32989239f;
	float y4=1.832625f;
	float u = 3.2f;
	float u3,u2,f1,f2,f3,f4;
	u3=u*u*u;
	u2=u*u;

	//@@: location TAMPERACTION_DOWNLOAD_ONE
	f1=-0.5f * u3 + u2 -0.5f *u;
	f2= 1.5f * u3 -2.5f * u2+1.0f;
	f3=-1.5f * u3 +2.0f * u2+0.5f*u;
	f4=0.5f*u3-0.5f*u2;

	x=x1*f1+x2*f2+x3*f3+x4*f4;
	y=y1*f1+y2*f2+y3*f3+y4*f4;
	//@@: range TAMPERACTION_DOWNLOAD_PATCH_TO_NOP {
	printf("%f %f %f %d", u, x, y, parm);
	//@@: } TAMPERACTION_DOWNLOAD_PATCH_TO_NOP 
}

void TamperAction_PopUp(int)
{
	DEBUGF1("TamperAction_PopUp");
	g_bShowWindow = true;
}
void TamperAction_DupKeyboard(int)
{
	DEBUGF1("TamperAction_DupKeyboard");
	g_eatDupKeyProb = 0.10f;
}
void TamperAction_HashFail(int)
{
	DEBUGF1("TamperAction_HashFail");
	volatile unsigned int a = 1;
	a <<= 1;
	a <<= 1;
	a <<= 1;
	g_readDiff = a;
}
void TamperAction_AlterState(int)
{
	DEBUGF1("TamperAction_AlterState");
	g_stateOffset = (int)(rand() / (float)RAND_MAX * 22) + 1;
}
void TamperAction_Flicker(int)
{
	DEBUGF1("TamperAction_Flicker");
	const float kFlickerInc = 0.008f;
	g_flickerProb = ((g_flickerProb + kFlickerInc) < 0.5f) ? g_flickerProb + kFlickerInc : 0.5f;
}
void TamperAction_BringToFront(int)
{
	DEBUGF1("TamperAction_BringToFront");
	g_bBringToFront = true;
}

void TamperAction_DamageCode(int parm)
{
	DEBUGF1("TamperAction_DamageCode");
	float x=5.88745736100f;
	float y=5.498411795f;
	float x1=5.1442929354f;
	float y1=3.7347719392f;
	float x2=9.804535768f;
	float y2=7.8949937346f;
	float x3=1.894150673f;
	float y3=2.0149506687f;
	float x4=9.324989239f;
	float y4=1.8342625f;
	float u = 3.2f;
	float u3,u2,f1,f2,f3,f4;
	u3=u*u*u;
	u2=u*u;

	//@@: location TAMPERACTION_DAMAGE_ONE
	f1=-0.5f * u3 + u2 -0.5f *u;
	f2= 1.5f * u3 -2.5f * u2+1.0f;
	f3=-1.5f * u3 +2.0f * u2+0.5f*u;
	f4=0.5f*u3-0.5f*u2;

	x=x1*f1+x2*f2+x3*f3+x4*f4;
	y=y1*f1+y2*f2+y3*f3+y4*f4;
	//@@: range TAMPERACTION_DAMAGE_PATCH_TO_NOP {
	printf("%f %f %f %d", u, x, y, parm);
	//@@: } TAMPERACTION_DAMAGE_PATCH_TO_NOP 
}

void TamperAction_DamageCodeTwo(int parm)
{
	DEBUGF1("TamperAction_DamageCodeTwo");
	float x=5.88745636100f;
	float y=5.498416795f;
	float x1=5.1442629354f;
	float y1=3.7347619392f;
	float x2=9.804565768f;
	float y2=7.8949637346f;
	float x3=1.894160673f;
	float y3=2.0149606687f;
	float x4=9.324969239f;
	float y4=1.8342625f;
	float u = 3.2f;
	float u3,u2,f1,f2,f3,f4;
	u3=u*u*u;
	u2=u*u;

	//@@: location TAMPERACTION_DAMAGETWO_ONE
	f1=-0.5f * u3 + u2 -0.5f *u;
	f2= 1.5f * u3 -2.5f * u2+1.0f;
	f3=-1.5f * u3 +2.0f * u2+0.5f*u;
	f4=0.5f*u3-0.5f*u2;

	x=x1*f1+x2*f2+x3*f3+x4*f4;
	y=y1*f1+y2*f2+y3*f3+y4*f4;
	//@@: range TAMPERACTION_DAMAGETWO_PATCH_TO_NOP {
	printf("%f %f %f %d", u, x, y, parm);
	//@@: } TAMPERACTION_DAMAGETWO_PATCH_TO_NOP
}

void TamperAction_RedHerring(int foo)
{
	float y=5.498416795f;
	float f1=-9.2f * foo;

	(void)y;
	(void)f1;
}
#endif
void RedHerringEntitlement()
{
	//@@: location REDHERRINGENTITLEMENT_CALL_WRITE
	RedHerringEntitlementWrite();
	u8 * data = NULL;
	u32 dataSize;
	//@@: location REDHERRINGENTITLEMENT_CALL_LOAD
	RedHerringEntitlementLoad(&data, dataSize);

	if (data)
		delete [] data;
}

void RedHerringEntitlementWrite()
{
	{
		std::wstring filePath = GetEntitlementFilePath();

		// Obtain Machine Info:

		const CHAR* valName = "MachineGuid";
		const CHAR* key64 = "SOFTWARE\\Microsoft\\Cryptography";

		HKEY hKey;
		DWORD dwRes = RegOpenKeyExA(
			HKEY_LOCAL_MACHINE, 
			key64,
			0,
			KEY_READ,
			&hKey);


		if(dwRes != ERROR_SUCCESS)
			return;

		ULONG allocSize = 1024;
		ULONG len = allocSize;
		CHAR * buffer;

		buffer = new CHAR[len];

		while (RegQueryValueExA(hKey, valName, NULL, NULL, (BYTE *)buffer, &len) == ERROR_MORE_DATA)
		{
			allocSize *= 2;
			len = allocSize;
			delete [] buffer;
			buffer = new CHAR[len];
		}

		// Encrypt values:
		//@@: location REDHERRINGENTITLEMENTWRITE_ENCRYPT_VALUES
		u32 dataSize = len;
		u8* data = (u8*)malloc(dataSize * sizeof(u8));
		memcpy(data, buffer, dataSize);
		delete [] buffer;

		// Adjust buffer size for block encryption:
		printf("Adjusting buffer size to a multiple of 16\n");

		if ((dataSize % AES_BLOCK_SIZE) != 0)
		{
			u32 oldSize = dataSize;
			dataSize += AES_BLOCK_SIZE - (dataSize % AES_BLOCK_SIZE);
			data = (u8 *)realloc(data, dataSize * sizeof(u8));
			for (u32 i = oldSize * sizeof(u8); i < dataSize * sizeof(u8); ++i)
				data[i] = 0x0;
		}

		u8* encrypted = (u8 *)malloc(dataSize);
		u32 encryptedSize = dataSize;
		u8 encryptIV[kIVSize];

#if WIN32_CRYPTO

		// Init CryptoAPI:
		printf("Initialize AES encryption\n");

		//@@: location REDHERRINGENTITLEMENTWRITE_INITIALISE_AES
		HCRYPTPROV provider;
		if (!CryptAcquireContext(&provider, NULL, MS_ENH_RSA_AES_PROV, PROV_RSA_AES, 0))
			return;

		// Set our secret Key:
		printf("Setting secret key\n");

		HCRYPTKEY cryptKey;

		KeyImportBlob blob;
		blob.header.bType = PLAINTEXTKEYBLOB;
		blob.header.bVersion = CUR_BLOB_VERSION;
		blob.header.reserved = 0;
		blob.header.aiKeyAlg = CALG_AES_256;
		blob.keySize = kAESKeySize;
		memcpy(blob.keyData, entitlementAESKey, kAESKeySize);
		//@@: location REDHERRINGENTITLEMENTWRITE_IMPORT_KEY
		if (!CryptImportKey(provider, (const BYTE *)&blob, sizeof(KeyImportBlob), NULL, 0, &cryptKey))
			return;

		// Generate a random IV. We will store it in the file for later decryption:
		printf("Generate random IV\n");

		CryptGenRandom(provider, kIVSize, encryptIV);
		u8 nextEncryptIV[kIVSize];
		memcpy(nextEncryptIV, encryptIV, kIVSize);
		CryptSetKeyParam(cryptKey, KP_IV, nextEncryptIV, 0);

		// Perform encryption:
		printf("Encrypt data buffer\n");

		memcpy(encrypted, data, dataSize);
		DWORD originalDataSize = dataSize;
		//@@: location REDHERRINGENTITLEMENTWRITE_ENCRYPT
		if (!CryptEncrypt(cryptKey, NULL, TRUE, 0, encrypted, &originalDataSize, encryptedSize))
		{
			DWORD error = GetLastError();
			while (error != ERROR_SUCCESS)
			{
				if (error != ERROR_MORE_DATA)
				{
					CryptDestroyKey(cryptKey);
					CryptReleaseContext(provider, 0);
					return;
				}
				else
				{
					encryptedSize = originalDataSize;
					originalDataSize = dataSize;
					encrypted = (u8*)realloc(encrypted, encryptedSize);
					if (CryptEncrypt(cryptKey, NULL, TRUE, 0, encrypted, &originalDataSize, encryptedSize))
					{
						encryptedSize = originalDataSize;
						originalDataSize = dataSize;
						break;
					}
					error = GetLastError();
				}
			}
		}

		CryptDestroyKey(cryptKey);
		CryptReleaseContext(provider, 0);
#else
		// Initialize OpenSSL encryption:
		printf("Initialize AES encryption\n");

		// Generate random IV:
		printf("Generate random IV\n");
		// TODO: if not seeded, call RAND_screen() here
		RAND_bytes(encryptIV, kIVSize);
		u8 nextEncryptIV[kIVSize];
		memcpy(nextEncryptIV, encryptIV, kIVSize);

		// Perform encryption:
		printf("Encrypt data buffer\n");

		AES_KEY aes;
		AES_set_encrypt_key(entitlementAESKey, kAESKeySize * 8, &aes);
		AES_cbc_encrypt(data, encrypted, dataSize, &aes, nextEncryptIV, AES_ENCRYPT);

#endif
		// Write data to file:
		printf("Writing encrypted data to file\n");

		std::ofstream out(filePath, std::ios::out | std::ios::binary);
		//@@: location REDHERRINGWRITE_WRITE_TO_DISK
		out.write((const char *)encryptIV, kIVSize);
		out.write((const char *)encrypted, encryptedSize);

		free(encrypted);
		free(data);
	}

}

volatile static const char CleanupRedHerringOutput[1024] = "\\Rockstar Games\\GTA V\\entitlement.info";

volatile static const wchar_t RedHerringOutput[][1024] = 
{
	L"Load IV and encrypted data\n", 
	L"Initialize decrypt context\n",
	L"Set IV\n",
	L"Decrypting data\n",
	L"\\Rockstar Games\\GTA V\\entitlement.info",
	L"Import secret key\n",
	L"Decryption failed\n"
};
void RedHerringEntitlementLoad(u8** pData, u32& dataSize)
{
	{
		// Load IV and encrypted data:
		wprintf((wchar_t*)RedHerringOutput[0]);
		//@@: location REDHERRINGENTITLEMENTLOAD_GET_PATH
		std::wstring filePath = GetEntitlementFilePath();
		std::ifstream in(filePath, std::ios::in | std::ios::binary);
		if (!in)
		{
			dataSize = 0;
			return;
		}

		in.seekg(0, std::ios::end);
		u32 fileSize = (u32)in.tellg();
		in.seekg(0, std::ios::beg);

		if (fileSize < kIVSize)
		{
			dataSize = 0;
			return;
		}

		// read IV:
		u8 encryptIV[kIVSize];
		in.read((char *)encryptIV, kIVSize);
		//@@: location REDHERRINGENTITLEMENTLOAD_READ_DATA
		// read encrypted data:
		dataSize = fileSize - kIVSize;
		u8* encData = new u8[dataSize];
		in.read((char *)(encData), dataSize);

		*pData = new u8[dataSize];

#if WIN32_CRYPTO
		// Initialize CryptoAPI:
		wprintf((wchar_t*)RedHerringOutput[1]);

		HCRYPTPROV provider;
		if (!CryptAcquireContext(&provider, NULL, MS_ENH_RSA_AES_PROV, PROV_RSA_AES, 0))
			return;

		// Import secret key:
		//@@: location REDHERRINGENTITLEMENTLOAD_IMPORT_KEY
		wprintf((wchar_t*)RedHerringOutput[5]);

		HCRYPTKEY cryptKey;

		KeyImportBlob blob;
		blob.header.bType = PLAINTEXTKEYBLOB;
		blob.header.bVersion = CUR_BLOB_VERSION;
		blob.header.reserved = 0;
		blob.header.aiKeyAlg = CALG_AES_256;
		blob.keySize = kAESKeySize;
		//@@: location REDHERRINGLOAD_MEMCPY_DATA
		memcpy(blob.keyData, entitlementAESKey, kAESKeySize);

		if (!CryptImportKey(provider, (const BYTE *)&blob, sizeof(KeyImportBlob), NULL, 0, &cryptKey))
			return;

		// Set IV:
		//@@: location REDHERRINGENTITLEMENTLOAD_SET_IV
		wprintf((wchar_t*)RedHerringOutput[2]);
		CryptSetKeyParam(cryptKey, KP_IV, encryptIV, 0);

		// Perform decryption:
		wprintf((wchar_t*)RedHerringOutput[3]);

		DWORD len = dataSize;
		if (!CryptDecrypt(cryptKey, 0, TRUE, 0, encData, &len))
		{
			delete *pData;
			dataSize = 0;
			CryptDestroyKey(cryptKey);
			CryptReleaseContext(provider, 0);
			//@@: location REDHERRINGENTITLEMENTLOAD_DECRYPTION_FAILED
			wprintf((wchar_t*)RedHerringOutput[6]);
			return;
		}

		memcpy(*pData, encData, len);

		CryptDestroyKey(cryptKey);
		CryptReleaseContext(provider, 0);

#else
		// Decrypt data
		printf("Decrypting data\n");
		AES_KEY aes;
		AES_set_decrypt_key(entitlementAESKey, kAESKeySize * 8, &aes);
		AES_cbc_encrypt(encData, *pData, dataSize, &aes, encryptIV, AES_DECRYPT);
#endif
	}

}
std::wstring GetEntitlementFilePath() 
{
	std::wstring filePath;
	PWSTR pszPath = NULL;
	HRESULT hr = SHGetKnownFolderPath(FOLDERID_LocalAppData, 0, NULL, &pszPath);

	if (SUCCEEDED(hr))
	{
		// Cleanup some long chinese path we accidentally put in everyones AppData
		std::wstring badFilePathW = std::wstring(pszPath) +  Constants::AppDataSubfolderCompanyName.Get() + 
				Constants::AppDataSubfolderProductName.Get() +  std::wstring((LPWSTR)CleanupRedHerringOutput);
		DeleteFileW(badFilePathW.c_str());

		filePath = std::wstring(pszPath) + 
			Constants::AppDataSubfolderCompanyName.Get() + 
			Constants::AppDataSubfolderProductName.Get() + 
			std::wstring((LPWSTR)RedHerringOutput[4]);

		CoTaskMemFree(pszPath);
	}

	return filePath;
}

LPWSTR GetCryptoErrorMessage(DWORD error)
{
	switch (error)
	{
	case NTE_BAD_DATA:
		return L"Bad Data";
	case NTE_BAD_ALGID:
		return L"Bad Algorithm";
	default:
		return L"Other Error";
	}
}

#endif