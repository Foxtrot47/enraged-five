#pragma once

#include "IPatchCheck.h"

class GamePatchCheck : public IPatchCheck
{
public:
	GamePatchCheck(void);
	~GamePatchCheck(void);

	virtual void ParseXML();
	virtual Version GetExecutableVersion();

	static std::string GetGameExecutableFullPath();
	static std::string GetGameExecutableFolder();
	static std::string GetGameExecutableFilename();

	static bool DoesGameExecutableExist();

	virtual void DownloadComplete(DownloadCompleteStatus status, std::string filename);
	virtual void DownloadError(const CError& errorString);
};
