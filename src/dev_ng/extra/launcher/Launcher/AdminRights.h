#pragma once

class CAdminRights
{
public:
	enum AdminRightState
	{
		ARS_UNKNOWN,
		ARS_ADMIN,
		ARS_STANDARD_USER,
		ARS_STANDARD_USER_NO_UAC,
	};

	static AdminRightState GetAdminRightsState();
	static AdminRightState CheckAdminRights();
	static bool IsUACDisabled();

	static AdminRightState sm_currentState;

};