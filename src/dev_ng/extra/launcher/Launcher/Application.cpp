#include "StdAfx.h"
#include "Application.h"

#include "CloudSaveManager.h"
#include "DownloadManager.h"
#include "UpdateCheck.h"
#include "GamePatchCheck.h"
#include "RockstarDownloader.h"
#include "Installer.h"
#include "Globals.h"
#include "Config.h"
#include "Launcher.h"
#include "LauncherDlg.h"
#include "Fsm.h"
#include "Util.h"
#include "CertificateVerify.h"
#include "Error.h"
#include "Channel.h"
#include "CrashDetection.h"
#include "GameCommunication.h"
#include "HashVerify.h"
#include "Entitlement.h"
#include "TamperSource.h"
#include "SocialClub.h"
#include "ProgressCalculator.h"
#include "RgscTelemetryManager.h"
#include "RgscDebugTelemetry.h"
#include "OnlineConfig.h"
#include <sstream>
#include <string>
#include <atlbase.h>
#include <deque>

// Static vars
CLauncherDlg* LauncherApp::m_launcherDlg = NULL;
std::deque<Patch> LauncherApp::m_patchList;
Patch LauncherApp::m_currentPatch;
bool LauncherApp::m_bRestart = false;
bool LauncherApp::m_bHasCheckedDevServer = false;
bool LauncherApp::m_bGameRunning = false;
bool LauncherApp::m_bDownloadingPatch = false;
bool LauncherApp::m_bCancelPatching = false;


LauncherApp::LauncherApp(void)
{
}

LauncherApp::~LauncherApp(void)
{
}

void LauncherApp::RestartGame()
{
	m_bRestart = true;
}

void LauncherApp::UpdateSocialClubTicket(rgsc::RockstarId rockstarId, std::string base64Ticket)
{
	if (m_launcherDlg)
	{
		m_launcherDlg->UpdateSocialClubTicket(rockstarId, base64Ticket);
	}
}

void LauncherApp::GameSignOutEvent()
{
	if (m_launcherDlg)
	{
		m_launcherDlg->GameSignOutEvent();
	}
}

void LauncherApp::GameSignInEvent(rgsc::RockstarId rockstarId)
{
	if (m_launcherDlg)
	{
		m_launcherDlg->GameSignInEvent(rockstarId);
	}
}

void LauncherApp::RunUpdateCheck(bool rgscOnly, rgsc::RockstarId rockstarId)
{
	ResetErrorState();

	static struct Closure : IDownloader::DownloadListener
	{
		void DownloadComplete(const Patch& patch, DownloadCompleteStatus status)
		{
			//@@: location LAUNCHERAPP_RUNUPDATECHECK_CLOSURE_DOWNLOAD_COMPLETE_CHECK_DLG
			if (m_launcherDlg)
			{
				//@@: range LAUNCHERAPP_RUNUPDATECHECK_CLOSURE_DOWNLOAD_COMPLETE_POST_MESSAGES {
				if (status == DL_STATUS_COMPLETE)
				{
					CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetProgress, (WPARAM)100.0f);
				}
				else if (status == DL_STATUS_PAUSE)
				{
					CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetStatusAndDelete, 0, (LPARAM)CUtil::NewCStringFromID(IDS_DOWNLOAD_PAUSED));
				}
				else
				{
					CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetStatusAndDelete, 0, (LPARAM)CUtil::NewCStringFromID(IDS_DOWNLOAD_ERROR));
					CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetProgress, (WPARAM)0.0f);
				} 
				//@@: } LAUNCHERAPP_RUNUPDATECHECK_CLOSURE_DOWNLOAD_COMPLETE_POST_MESSAGES


			}
			//@@: range LAUNCHERAPP_RUNUPDATECHECK_CLOSURE_DOWNLOAD_COMPLETE_NOTIFY_COMPLETION {
			Downloader::Instance().DownloadComplete(status, patch.LocalFilename);
			UpdateChecker::Instance().DownloadComplete(status, patch.LocalFilename);
			if (patch.BackgroundDownload)
			{
				CGameCommunication::Instance().SendCompleteMessage(patch.SkuName);
			} 
			//@@: } LAUNCHERAPP_RUNUPDATECHECK_CLOSURE_DOWNLOAD_COMPLETE_NOTIFY_COMPLETION

		}
		void DownloadStatus(const Patch& /*patch*/, DownloadCompleteStatus status, u64 /*totalBytesComplete*/, u64 /*totalBytesInFile*/, u64 /*bytesJustDownloaded*/)
		{
			if (m_launcherDlg)
			{
				if (status == DL_STATUS_RETRY)
				{
					std::wstring str = CLocalisation::Instance().GetWString(IDS_DOWNLOADER_RETRYING);
					m_launcherDlg->SetStatusTextSafe(str);
				}
				else if (status == DL_STATUS_PAUSE)
				{
					std::wstring str = CLocalisation::Instance().GetWString(IDS_DOWNLOAD_PAUSED);
					m_launcherDlg->SetStatusTextSafe(str);
				}
			}
		}
		void DownloadError(const Patch& /*patch*/, const CError& error)
		{
			ThrowError(error);
			Downloader::Instance().DownloadError(error);
			UpdateChecker::Instance().DownloadError(error);
		};
	} closure;

	//@@: range LAUNCHERAPP_RUNUPDATECHECK_INIT_RESEST {
	Downloader::Instance().Init();
	Downloader::Instance().Reset();

	UpdateChecker::Instance().Reset(); 
	//@@: } LAUNCHERAPP_RUNUPDATECHECK_INIT_RESEST


#if !RSG_FINAL
	if (!Globals::noUpdateCheck)
	{
#endif
		//@@: range LAUNCHERAPP_RUNUPDATECHECK_UPDATE_CHECK {
		// All builds will update the RGSC
		if (rgscOnly)
		{
			//@@: location LAUNCHERAPP_RUNUPDATECHECK_ADD_RGSC
			UpdateChecker::Instance().AddRGSCCheck();
		}

		// Game updates handled by steam client
#if !RSG_STEAM_LAUNCHER
		if (!rgscOnly)
		{
			//@@: range LAUNCHERAPP_RUNUPDATECHECK_ADD_GAME_CHECKS {
			UpdateChecker::Instance().AddGameCheck();

			if (m_launcherDlg)
			{
				UpdateChecker::Instance().AddDLCCheck(rockstarId);
			}
			//@@: } LAUNCHERAPP_RUNUPDATECHECK_ADD_GAME_CHECKS

		}
#endif
		//@@: location LAUNCHERAPP_RUNUPDATECHECK_CHECK_FOR_UPDATES_CALL
		if (!UpdateChecker::Instance().CheckForUpdates(&closure) || AUTOTEST_FAIL)
		{
			const CError& lastError = CError::GetLastError();

			if (lastError.IsError())
				ThrowError(lastError);
			else
				ThrowError(CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_UNKNOWN));
		}

		Downloader::Instance().Shutdown(); 
		//@@: } LAUNCHERAPP_RUNUPDATECHECK_UPDATE_CHECK

#if !RSG_FINAL
	}
	else
	{
		WARNINGF("Skipping update check!");
	}
#endif

	if (!m_launcherDlg)
	{
		ERRORF("No launcher dialog!");
		return;
	}

	// Flags for automated testing
	bool autotestNoTempSpace = false;
	bool autotestClockWrong = false;
	bool autotestOtherError = false;

#if !RSG_FINAL
	if (AUTOTEST_FAIL)
	{
		autotestNoTempSpace = true;
		std::string sizeStr;
		CUtil::AppendHumanReadableDataSize(sizeStr, 400);
		CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_NO_DISK_SPACE, sizeStr);
	}
	else if (AUTOTEST_FAIL)
	{
		autotestClockWrong = true;
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_CLOCK_WRONG);
	}
	else if (AUTOTEST_FAIL)
	{
		autotestOtherError = true;
		CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_NO_CONNECTION);
	}
#endif
	
	if (GetError().IsError(DOWNLOADER_ERR_NO_DISK_SPACE) || autotestNoTempSpace)
	{
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eNoTempSpace);
	}
	else if ((GetError().GetErrorCategory() == ERROR_CATEGORY_LAUNCHER && GetError().GetErrorCode() == LAUNCHER_ERR_CLOCK_WRONG) || autotestClockWrong)
	{
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eErrorScreen);
	}
	else if (HasErrorOccurred() || autotestOtherError)
	{
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eOffline);
	}
	else
	{
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);
	}
}

void LauncherApp::CancelUpdateCheck()
{
	UpdateChecker::Instance().Cancel();
}

void LauncherApp::RunPatchProcess()
{
	m_bCancelPatching = false;

	DEBUGF3("Running patch process.");
	//@@: location LAUNCHERAPP_RUNPATCHPROCESS_RESET_ERROR_STATE
	ResetErrorState();

	bool updateUI = true;

	if (CAdminRights::GetAdminRightsState() != CAdminRights::ARS_STANDARD_USER_NO_UAC)
	{
		DEBUGF3("We have (or can acquire) admin rights.");

		//@@: location LAUNCHERAPP_RUNPATCHPROCESS_GET_PATCHES_TO_INSTALL
		std::vector<Patch>& patches = UpdateChecker::Instance().PatchesToInstall();

		//@@: range LAUNCHERAPP_RUNPATCHPROCESS_PUSH_PATCHES {
		for (size_t i = 0; i < patches.size(); i++)
			m_patchList.push_back(patches[i]);

		//@@: location LAUNCHERAPP_RUNPATCHPROCESS_SET_DOWNLOADING_PATCH_TRUE
		patches.clear();

		DEBUGF3(m_patchList.size() == 1 ? "%d patch to process." : "%d patches to process.", m_patchList.size());

		
		m_bDownloadingPatch = true; 
		//@@: } LAUNCHERAPP_RUNPATCHPROCESS_PUSH_PATCHES


		
		while (!m_patchList.empty() && !m_bCancelPatching)
		{

			//@@: range LAUNCHERAPP_RUNPATCHPROCESS_APPLY_PATCH {
			m_currentPatch = m_patchList.front();

			updateUI = true;

			if (!DownloadCurrentPatch() || AUTOTEST_FAIL)
			{
				ERRORF("A patch has failed to download! %s", m_currentPatch.PatchURL.c_str());

				if (!GetError().IsError())
				{
					ThrowError(CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_UNKNOWN));
				}

				// If any patch fails (it has 3 attempts before giving up), we can't really continue.
				break;
			}
			//@@: range LAUNCHERAPP_RUNPATCHPROCESS_APPLY_PATCH_EARLY_RETURN {
			if (m_bCancelPatching)
			{
				
				//@@: location LAUNCHERAPP_RUNPATCHPROCESS_APPLY_PATCH_EARLY_RETURN_LOCATION
				DEBUGF1("Patching was cancelled.");
				return; 
				

			}

			//@@: } LAUNCHERAPP_RUNPATCHPROCESS_APPLY_PATCH_EARLY_RETURN
			if (m_bGameRunning)
			{
				// Download complete, but the game is running.
				// We'll do the install next time the launcher runs.
				DEBUGF3("Finished a background download.");
				m_patchList.pop_front();
			}
			else
			{
				DEBUGF3("Attempting to install patch.");

				//@@: location LAUNCHERAPP_RUNPATCHPROCESS_INSTALL_PATCH
				if (InstallCurrentPatch())
				{
					if (m_currentPatch.PatchType == PT_SCUI)
						RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_RGSC_COMPLETED_UPDATE);
					else if (m_currentPatch.PatchType == PT_GAME)
						RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_GAME_COMPLETED_UPDATE);

					// Succeeded; pop the patch.
					m_patchList.pop_front();

					// Tell the launcher to regenerate the version string
					m_launcherDlg->UpdateVersionString();
				}
				else
				{
					ERRORF("A patch has failed to install! %s", m_currentPatch.PatchURL.c_str());

					if (!GetError().IsError() || AUTOTEST_FAIL)
					{
						ThrowError(CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_INVALID_SIGNATURE));
					}

					// If any patch fails (it has 3 attempts before giving up), we can't really continue.
					if (m_currentPatch.UpdateType != UT_OPTIONAL)
						break;
				}

				// Clear the patch list if a restart is requested
				if (m_bRestart)
				{
					DISPLAYF("Restart requested.");
					while (!m_patchList.empty())
					{
						m_patchList.pop_front();
					}
				}
			}
		} 
		//@@: } LAUNCHERAPP_RUNPATCHPROCESS_APPLY_PATCH

	}

	m_bDownloadingPatch = false;

	//@@: range LAUNCHERAPP_RUNPATCHPROCESS_POST_MESSAGE {
	
	//@@: location LAUNCHERAPP_RUNPATCHPROCESS_SET_DOWNLOADING_PATCH_FALSE
	if (m_launcherDlg && updateUI && !m_bGameRunning && !m_bCancelPatching)
	{
		if (m_bRestart)
		{
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eRestart);
		}
		else if (HasErrorOccurred())
		{
			if (GetError().GetErrorCategory() == ERROR_CATEGORY_DOWNLOADER)
			{
				if (GetError().GetErrorCode() == DOWNLOADER_ERR_NO_DISK_SPACE)
				{
					DEBUGF1("No disk space.");
					CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eNoTempSpace);
				}
				else if (GetError().GetErrorCode() == DOWNLOADER_ERR_UNENTITLED)
				{
					DEBUGF1("Show code redemption.");
					CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eShowCodeRedemption);
				}
				else
				{
					DEBUGF1("Offline.");
					CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eOffline);
				}
			}
			else
			{
				CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eErrorScreen);
			}
		}
		else if (CAdminRights::GetAdminRightsState() == CAdminRights::ARS_STANDARD_USER_NO_UAC)
		{
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eOfflineNoUAC);
		}
		else
		{
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);
		}
	} 
	//@@: } LAUNCHERAPP_RUNPATCHPROCESS_POST_MESSAGE

}

void LauncherApp::CancelPatchProcess()
{
	m_bCancelPatching = true;

	if (m_bDownloadingPatch)
	{
		Downloader::Instance().Stop();
	}

	Installer::Instance().Cancel();
}

void LauncherApp::UpdatePatchPaths(const std::string& find, const std::string& replace)
{
	for (std::deque<Patch>::iterator it = m_patchList.begin(); it != m_patchList.end(); it++)
	{
		std::string& filename = (*it).LocalFilename;
		size_t pos = filename.find(find);
		if (pos != std::string::npos)
		{
			filename.erase(pos, find.size());
			filename.insert(pos, replace);
		}
	}
}

bool LauncherApp::DownloadCurrentPatch()
{
	if (m_currentPatch.PatchType == PT_SCUI)
		RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_RGSC_START_DOWNLOAD);
	else if (m_currentPatch.PatchType == PT_GAME)
		RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_GAME_START_DOWNLOAD);

	struct Closure : IDownloader::DownloadListener
	{
		bool m_error;
		bool m_done;
		bool m_certValid;
		bool m_needEntitlement;
		float m_lastProgress;
		s64 m_lastStatusUpdateMs;

		ProgressCalculator* m_progCalc;

		Closure(Patch& patch) : m_error(false), m_done(false), m_certValid(true), m_needEntitlement(false), m_lastProgress(-2), m_lastStatusUpdateMs(-1)
		{
			if (!patch.Progress)
			{
				patch.Progress = new ProgressCalculator(m_currentPatch.PatchType == PT_SCUI ? ProgressCalculator::PT_DOWNLOAD_SC_UPDATE : ProgressCalculator::PT_DOWNLOAD_PATCH);
				ProgressCalculator::AddToGlobalProgressList(patch.Progress);
			}

			m_progCalc = patch.Progress;
		}

		virtual void DownloadStarting(Patch& patch)
		{
			if (patch.NeedsAccessControlledUrl)
			{
#if RSG_TIMEBOMBED_URLS_IN_SCDLL
				EntitlementManager::EntitlementResult result;
				std::string timebombedUrl;

				SocialClub::GetEntitlementManager()->RequestTimebombedUrlSynchronous(patch.AccessControlKey, &result, timebombedUrl);

				if (result == EntitlementManager::ENTITLEMENT_TRUE)
				{
					patch.PatchURL = timebombedUrl;
				}
				else if (result == EntitlementManager::ENTITLEMENT_FALSE)
				{
					m_needEntitlement = true;
					m_error = true;
				}
				else if (result == EntitlementManager::ENTITLEMENT_ERROR)
				{
					m_error = true;
				}
#else
				ERRORF("Unable to obtain URL!");
#endif
			}
		}

		void DownloadComplete(const Patch& patch, DownloadCompleteStatus status)
		{
			//@@: range LAUNCHERAPP_DOWNLOADCURRENTPATCH_CLOSURE_DOWNLOAD_COMPLETE {
			if (m_launcherDlg)
			{
				if (status == DL_STATUS_COMPLETE)
				{
					if (patch.BackgroundDownload)
					{
						CGameCommunication::Instance().SendCompleteMessage(patch.SkuName);
					}
					else
					{
						CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetProgress, (WPARAM)100.0f);
					}
				}
				else if (status == DL_STATUS_PAUSE)
				{
					if (!patch.BackgroundDownload)
					{
						CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetStatusAndDelete, 0, (LPARAM)CUtil::NewCStringFromID(IDS_DOWNLOAD_PAUSED));
					}
				}
				else
				{
					if (!patch.BackgroundDownload)
					{
						std::string token = CLocalisation::Instance().GetString(IDS_DOWNLOAD_ERROR);
						CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetStatusAndDelete, 0, (LPARAM)CUtil::NewCString(token.c_str()));
						CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetProgress, (WPARAM)100.0f);
					}
				}
			}

			wchar_t path[MAX_PATH];
			MultiByteToWideChar(CP_UTF8, 0, patch.LocalFilename.c_str(), -1, path, MAX_PATH);

			if (!m_error && patch.HashHexDigits.length() > 0)
			{
				m_error = !HashVerify::Verify(path, patch.HashHexDigits);
				if (m_error)
				{
					m_progCalc->DownloadEvent(IDownloader::DLE_VERIFICATION_FAILURE);
					RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::DCP_HASH_VERIFICATION_FAILURE);
					std::string spath;
					CUtil::WStringToStdString(std::wstring(path), spath);
					DEBUGF1("Deleting %s as its SHA-1 hash doesn't match.", spath.c_str());
					DeleteFileW(path);
				}
			}

#if !RSG_FINAL 
			if (!m_error && Constants::CheckingCertificates)
#endif
			{

				//@@: location LAUNCHERAPP_DOWNLOADCURRENTPATCH_CALL_CERTIFICATE_VERIFY
				m_certValid = CertificateVerify::Verify(path);				
				if (!m_certValid)
				{
					m_progCalc->DownloadEvent(IDownloader::DLE_VERIFICATION_FAILURE);
					RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::DCP_CERT_VERIFICATION_FAILURE);
					std::string spath;
					CUtil::WStringToStdString(std::wstring(path), spath);
					DEBUGF1("Deleting %s as it is corrupt.", spath.c_str());
					DeleteFileW(path);
				}
				//@@: location LAUNCHERAPP_DOWNLOADCURRENTPATCH_CERTIFICATE_VERIFY_EXIT
			}

			RgscDebugTelemetry::SendDebugTelemetry();

			CUtil::Sleep(300);
			m_done = true; 
			//@@: } LAUNCHERAPP_DOWNLOADCURRENTPATCH_CLOSURE_DOWNLOAD_COMPLETE

		}

		void DownloadStatus(const Patch& patch, DownloadCompleteStatus /*status*/, u64 /*totalBytesComplete*/, u64 /*totalBytesInFile*/, u64 /*bytesJustDownloaded*/)
		{
			if (m_progCalc->IsUpdatedMessageAvailable())
			{
				float prog;
				std::wstring message;
				m_progCalc->GetCurrentStatus(message, prog);

				float percent = prog * 100.0f;
				const float PROGRESS_EPSILON = 1.0f / 16.0f;
				if (percent > m_lastProgress + PROGRESS_EPSILON || percent < m_lastProgress - PROGRESS_EPSILON)
				{
					m_lastProgress = percent;
					if (patch.BackgroundDownload)
					{
						CGameCommunication::Instance().SendProgressMessage(patch.SkuName, (int)percent);
					}
					else
					{
						if (m_launcherDlg)
						{
							CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetProgress, (WPARAM)percent);
						}
					}
				}

				m_launcherDlg->SetStatusTextSafe(message);
			}
		}

		void DownloadError(const Patch& patch, const CError& error)
		{
			if (!patch.BackgroundDownload)
			{
				ThrowError(error);
			}
			m_error = true;
		};
	};

	DEBUGF1("Attempting to download patch: %s", m_currentPatch.PatchURL.c_str());

	Globals::needsLaunchConfirmation = true;

	Closure closure(m_currentPatch);

	//@@: range LAUNCHERAPP_DOWNLOADCURRENTPATCH_QUEUE_PATCH {

	Downloader::Instance().SetDownloadListener(&closure);
	Downloader::Instance().QueuePatch(m_currentPatch);
	Downloader::Instance().SetMaxRetries(OnlineConfig::PatchRetries);
	Downloader::Instance().StartSynchronously();

	//@@: } LAUNCHERAPP_DOWNLOADCURRENTPATCH_QUEUE_PATCH

	Downloader::Instance().SetDownloadListener(NULL);
	//@@: range LAUNCHERAPP_DOWNLOADCURRENTPATCH_CHECK_ERRORS {

	if (!closure.m_error && closure.m_certValid)
	{
		return true;
	}
	else if (m_bCancelPatching)
	{
		ERRORF("Download was cancelled.");
		return false;
	}
	else if (closure.m_needEntitlement)
	{
		ERRORF("Not entitled.");
		ThrowError(CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_UNENTITLED));
		return false;
	}
	else
	{
		ERRORF("Download failed.");
	}
	return false; 
	//@@: } LAUNCHERAPP_DOWNLOADCURRENTPATCH_CHECK_ERRORS

}

bool LauncherApp::InstallCurrentPatch()
{
	if (m_currentPatch.PatchType == PT_SCUI)
		RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_RGSC_START_INSTALL);
	else if (m_currentPatch.PatchType == PT_GAME)
		RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_GAME_START_INSTALL);

	bool finished = false, succeeded = false;

	if (m_launcherDlg && !m_currentPatch.BackgroundDownload)
	{
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eInstallUpdate);
		CUtil::Sleep(500);
	}

	struct Closure : Installer::InstallListener
	{
		bool *m_finished, *m_succeeded;
		Closure(bool* finished, bool* succeeded) { m_finished = finished; m_succeeded = succeeded; }

		//@@: range LAUNCHERAPP_INSTALLCURRENTPATCH_QUEUE_PATCH_INSTALL_LISTENER_FUNCTIONS {
		virtual void InstallComplete(bool succeeded, const std::string& localFilename, const std::string& friendlyName, const std::string& skuName, int exitCode)
		{
			DISPLAYF(succeeded ? "Successfully installed patch " : "Failed to install patch ");
			DISPLAYF("%s (%s) of SKU %s. Exit code was %d.", localFilename.c_str(), friendlyName.c_str(), skuName.c_str(), exitCode);
			*m_finished = true;
			*m_succeeded = succeeded;
		}

		virtual void InstallError(const std::string& localFilename, const std::string& friendlyName, const CError& error)
		{
			ERRORF("Error occurred installing %s (%s).", localFilename.c_str(), friendlyName.c_str());
			ThrowError(error);
			*m_finished = true;
			*m_succeeded = false;
		}

		virtual void InstallRestart()
		{
			//@@: location LAUNCHERAPP_INSTALLCURRENTPATCH_QUEUE_PATCH_INSTALL_LISTENER_INSTALL_RESTART
			DISPLAYF("Installer requested restart.");
			*m_finished = true;
			*m_succeeded = true;
			m_bRestart = true;
		} 
		//@@: } LAUNCHERAPP_INSTALLCURRENTPATCH_QUEUE_PATCH_INSTALL_LISTENER_FUNCTIONS

		virtual void InstallShutdown()
		{
			DISPLAYF("Installer requested shutdown.");
			RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_MTL_STARTING_INSTALL);
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eShutdown, 0, 0);
			*m_finished = true;
			*m_succeeded = true;
		} 
	};

	//@@: range LAUNCHERAPP_INSTALLCURRENTPATCH_QUEUE_PATCH_INSTALLATION {
	Closure* closure = new Closure(&finished, &succeeded);

	Installer::Instance().SetListener(closure);
	Installer::Instance().InstallPatch(m_currentPatch);

	while (!finished)
	{
		CUtil::Sleep(100);
	}

	Installer::Instance().SetListener(NULL);
	delete closure;

	return succeeded; 
	//@@: } LAUNCHERAPP_INSTALLCURRENTPATCH_QUEUE_PATCH_INSTALLATION

}

std::string LauncherApp::GetArgs()
{
	const std::wstring& wargs = CCommandlineArgument::GetGameCommandline();

	std::string args;
	CUtil::WStringToStdString(wargs, args);

	return args;
}

std::string LauncherApp::GetGameExecutableFullPath()
{
	//@@: location LAUNCHERAPP_GETGAMEEXECUTABLEFULLPATH
	std::string folder = GamePatchCheck::GetGameExecutableFolder();
	if (folder.length() == 0)
	{
		ERRORF("Can't find game location to launch!");
		return folder;
	}

	return folder + "\\" + GamePatchCheck::GetGameExecutableFilename();
}
#if RSG_REGULAR_LAUNCHER  || RSG_STEAM_LAUNCHER
UINT LauncherApp::LaunchGameExecutable()
{
#if !RSG_FINAL
	if (DebugSwitches::DontLaunchGame)
	{
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_OPEN_PROCESS_FAILED, "Not launching game due to debug switch.");
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eErrorScreen);
		return 1;
	}
#endif

	if (Constants::DebugMode)
		CUtil::Sleep(1000);

	//@@: range LAUNCHERAPP_LAUNCHGAMEEXECUTABLE {
	STARTUPINFOW startupInfo;
	memset(&startupInfo, 0, sizeof(STARTUPINFOW));
	//@@: location LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_BUILD_STARTUPINFO
	startupInfo.cb = sizeof(startupInfo);
	PROCESS_INFORMATION procInfo;
	memset(&procInfo, 0, sizeof(PROCESS_INFORMATION));

	//@@: range LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_BUILD_PATH_CHECK_DEPENDENCIES {
	std::string folder = GamePatchCheck::GetGameExecutableFolder();
	if (folder.length() == 0 || AUTOTEST_FAIL)
	{
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_NO_GAME_PATH);
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eErrorScreen);
		return ~0U;
	}

	std::string path = GetGameExecutableFullPath();
	if (path.length() == 0 || AUTOTEST_FAIL)
	{
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_NO_GAME_PATH);
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eErrorScreen);
		return ~0U;
	}

	//@@: location LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_CHECK_DEPENDENCIES
#if !RSG_FINAL
	if (!Globals::noDependencyCheck)
#endif
	{
		if (!CheckDependencies())
		{
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eErrorScreen);
			return ~0U;
		}
	}

#if !RSG_STEAM_LAUNCHER
	if (!Globals::scofflineonly && !InPlaceDownloader::QuickMissingFileCheck())
	{
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eCheckForInPlaceDownload);
		return ~0U;
	}
#endif

	//@@: } LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_BUILD_PATH_CHECK_DEPENDENCIES


	//@@: location LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_GET_COMMAND_LINE_ARGUMENTS
	std::string args = GetArgs();

#if !RSG_FINAL
	const std::string& configArgs = Constants::CommandlineArgs.Get();

	if (configArgs.size() > 0)
	{
		args.append(" ");
		args.append(configArgs);
	}
#endif

	//@@: range LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_APPEND_COMMAND_LINE_ARGUMENTS {
#if !RSG_FINAL
	if (Globals::localAssetPath && args.find("-rootdir") == std::string::npos)
		args.append(" -rootdir=./");
#endif

	if (Globals::safemode && args.find("-safemode") == std::string::npos)
	{
		args.append(" -safemode");

		if (!Globals::safemode.WasSetFromCommandline())
		{
			Globals::safemode = false;
		}
	}

#if !RSG_FINAL && !RSG_FINAL_FAUX
	if (Globals::scofflineonly)
	{
		args.append(" -nonetwork");
		args.append(" -scOfflineOnly");
	} 
#else
	if (Globals::scofflineonly)
	{
		args.append(" -scOfflineOnly");
	}
#endif

	std::string env;
	CUtil::GetEnvStr(env);
	if (env == "dev" && args.find("-rlrosdomainenv") == std::string::npos)
	{
		args.append(" -rlrosdomainenv=dev");
	}

	std::string cmd = std::string("\"") + path + "\"" + ((args.length() > 0) ? " " + args : "");
	std::wstring wcmd;
	CUtil::StdStringToStdWString(cmd, wcmd);
	//@@: } LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_APPEND_COMMAND_LINE_ARGUMENTS
	wchar_t* wcmdline = new wchar_t[wcmd.length() + 1];
	//@@: location LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_BUILD_CMD_LINE
	wcscpy_s(wcmdline, wcmd.length() + 1, wcmd.c_str());

	//@@: range LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_LAUNCH_GAME {

	DISPLAYF("Launching game...");
	DISPLAYF("(path: %s cmdline: %s)", path.c_str(), cmd.c_str());

	DWORD exitCode = 0; 
#if !RSG_FINAL
	if (!DebugSwitches::DontLaunchGame 

		&& !Globals::testFlow
		)
#endif
	{
		std::wstring wpath, wfolder;
		CUtil::StdStringToStdWString(path, wpath);
		CUtil::StdStringToStdWString(folder, wfolder);


		BOOL succeeded = CreateProcessW(
			wpath.c_str(),
			wcmdline,
			NULL,
			NULL,
			FALSE,
			0,
			NULL,
			wfolder.c_str(),
			&startupInfo,
			&procInfo
			);

		delete[] wcmdline;

		//@@: } LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_LAUNCH_GAME

		//@@: range LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_RUNNING_GAME {

#if !RSG_FINAL
		if (Globals::autotest_launcher_gamepatch)
		{
			if (succeeded)
			{
				Sleep(3000);
				DISPLAYF("autotest_launcher_gamepatch killing process");
				TerminateProcess(procInfo.hProcess, 0);
			}
			else
			{
				DISPLAYF("Launch failed");
			}
			CLauncherDlg::PostMessageSafe(WM_CLOSE);
			return 1;
		}
#endif

		if (succeeded && !AUTOTEST_FAIL)
		{
			m_bGameRunning = true;

			// Write lockfile
            //@@: location LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_WRITE_IN_FILE
			if (CCrashDetection::WriteInfile(procInfo.dwProcessId))
			{
				DEBUGF3("Written in-file.");
			}
			else
			{
				ERRORF("Unable to write in-file!");
			}

			if (Constants::DebugMode)
				CUtil::Sleep(2000);

			// Hide window
			if (m_launcherDlg)
			{
				CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetLauncherVisibility, FALSE, TRUE);

#if ENABLE_TAMPER_ACTIONS
				if (g_bBringToFront)
					BringWindowToTop(m_launcherDlg->GetSafeHwnd());
#endif
			}


			// Wait for process to exit
			//@@: location LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_WAIT_FOR_SINGLE_OBJECT
			WaitForSingleObject(procInfo.hProcess, INFINITE);

			m_bGameRunning = false;

			// Get the exit code
			GetExitCodeProcess(procInfo.hProcess, &exitCode);


			CloseHandle(procInfo.hProcess);
			CloseHandle(procInfo.hThread);
		}
		else
		{
			CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_NO_GAME_EXE, GamePatchCheck::GetGameExecutableFilename());
			if (m_launcherDlg)
				CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eErrorScreen);
			return ~0U;
		} 
		//@@: } LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_RUNNING_GAME

		//@@: range LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_GAME_EXIT {

		DISPLAYF("Game exited with code 0x%x", exitCode);
#if !RSG_FINAL
		if (DebugSwitches::ForceGameExitCode)
			exitCode = DebugSwitches::ForceGameExitCodeValue;
#endif
	}

	//@@: location LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_POST_CHECKING
	if (m_launcherDlg)
	{
		std::string out_file;
		if (CCrashDetection::ReadOutfile(procInfo.dwProcessId, out_file))
		{
			DEBUGF1("Read game out-file.");
		}
		else
		{
			ERRORF("Unable to read out-file!");
		}

		if (exitCode == 0 && out_file.length() == 0)
		{
			if (m_bRestart)
			{
				// Game has requested a restart
				DEBUGF1("Restarting game.");
				CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eRestartGame, 0);
				m_bRestart = false;
			}
#if !RSG_STEAM_LAUNCHER
			else if (InPlaceDownloader::IsRunning())
			{
				// Patch is still downloading.
				InPlaceDownloader::ChangeVisibility(InPlaceDownloader::IN_FOREGROUND);
				Globals::needsLaunchConfirmation = true;
				CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eResumeDownloadingUpdate);
			}
#endif
			else
			{
				// Game has exited, no patch is downloading, so close the launcher
				if (m_launcherDlg->IsSocialClubOnline() && Globals::spcloudsave && OnlineConfig::CloudSaveEnabled && !Globals::skipPostgameCloudSync)
				{
					CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eCloudSyncPostGame);
				}
				else
				{
					CLauncherDlg::PostMessageSafe(WM_CLOSE, 0, 0);
				}
			}
		}
		else
		{
			//@@: range LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_ERROR_EXIT {
			Globals::lastLaunchExitCode = exitCode;
			//ShowWindowAsync(launcherDlg->GetSafeHwnd(), SW_SHOW);

			CUtil::SanitizeToSingleShortLine(out_file);
			CError::SetErrorExitCode(exitCode, out_file);

			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eExitCodeError);

			CCrashDetection::TranslateKnownExitCode(exitCode);

			CUtil::Sleep(10);

			SetForegroundWindow(m_launcherDlg->GetSafeHwnd());
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetLauncherVisibility, TRUE, TRUE); 
			//@@: } LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_ERROR_EXIT

		}
	} 
	//@@: } LAUNCHERAPP_LAUNCHGAMEEXECUTABLE_GAME_EXIT



	// Clear up in-file and out-file for this process
	if (CCrashDetection::DeleteEntryAndExitFilePair(procInfo.dwProcessId))
	{
		DEBUGF3("Cleared in-file and out-file.");
	}
	else
	{
		ERRORF("Unable to clear in-file and out-file!");
	}
	//@@: } LAUNCHERAPP_LAUNCHGAMEEXECUTABLE

	return (UINT)exitCode;
}
#endif

#if  RSG_REGULAR_LAUNCHER || RSG_STEAM_LAUNCHER
bool LauncherApp::CheckDependencies()
{
	std::vector<LPCSTR> depends;

	//@@: range LAUNCHERAPP_CHECKDEPENDENCIES_GET_LIST {
	for (size_t i = 0; i < CConfigOption::GetDependencyList().size(); i++)
		depends.push_back(CConfigOption::GetDependencyList()[i].c_str());
#if !RSG_FINAL
	if (DebugSwitches::DebugTestMissingDependency)
	{
		depends.push_back("DEBUG_TEST_MISSING_DEPENDENCY.DLL");
	}
#endif

	//@@: } LAUNCHERAPP_CHECKDEPENDENCIES_GET_LIST

	//@@: location LAUNCHERAPP_CHECKDEPENDENCIES_RESET_ERROR_STATE
	ResetErrorState();
	//@@: location LAUNCHERAPP_CHECKDEPENDENCIES_DEFAULT_ALL_LOADED
	bool bAllLoaded = true;

	//@@: range LAUNCHERAPP_CHECKDEPENDENCIES_LOAD_DEPENDENCIES {
	for (size_t i = 0; i < depends.size(); i++)
	{
		std::string dependency(depends[i]);

		//@@: range LAUNCHERAPP_CHECKDEPENDENCIES_LOAD_LIBRARY {
		HMODULE handle = LoadLibraryExA(CUtil::DecodeAndDecrypt(dependency).c_str(), NULL, LOAD_LIBRARY_AS_DATAFILE); 
		//@@: } LAUNCHERAPP_CHECKDEPENDENCIES_LOAD_LIBRARY

		//@@: location LAUNCHERAPP_CHECKDEPENDENCIES_LOAD_LIBRARY_HANDLE_CHECK
		if (handle == NULL)
		{
			DEBUGF1("Failed to load library: %s", dependency);

			if (dependency == "WMVCORE.DLL" || AUTOTEST_FAIL)
			{
				CError error(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_MISSING_WMP, std::string(dependency));
				ThrowError(error);
				CError::SetError(error);
				bAllLoaded = false;
			}
			else if (bAllLoaded || AUTOTEST_FAIL)
			{
				CError error(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_MISSING_DEPENDENCY, std::string(dependency));
				ThrowError(error);
				CError::SetError(error);
				bAllLoaded = false;
			}
		}
		else
		{
			//@@: range LAUNCHERAPP_CHECKDEPENDENCIES_FREE_LIBRARY {
			FreeLibrary(handle);
			//@@: } LAUNCHERAPP_CHECKDEPENDENCIES_FREE_LIBRARY
		}
	} 
	//@@: } LAUNCHERAPP_CHECKDEPENDENCIES_LOAD_DEPENDENCIES

	//@@: location LAUNCHERAPP_CHECKDEPENDENCIES_RETURN
	return bAllLoaded;
}
#endif
void LauncherApp::ThrowError(const CError& error)
{
	CError::SetError(error);
}

void LauncherApp::ResetErrorState()
{
	CError::ResetLastError();
}

void LauncherApp::ShowErrorMessage(bool /*noResultCheck*/)
{

}
void LauncherApp::HandleGameMessages()
{

}

