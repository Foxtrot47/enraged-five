#pragma once


// CDoubleBufferedEdit

class CDoubleBufferedEdit : public CEdit
{
	DECLARE_DYNAMIC(CDoubleBufferedEdit)

public:
	CDoubleBufferedEdit();
	virtual ~CDoubleBufferedEdit();

protected:
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	DECLARE_MESSAGE_MAP()
};


