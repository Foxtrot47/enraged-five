#pragma once

#include "Util.h"
#include <string>

struct CertificateDetails
{
	SYSTEMTIME TimeStamp;

	std::string C;
	std::string S;
	std::string L;
	std::string O;
	std::string OU;
	std::string CN;

	CertificateDetails()
	{
		ZeroMemory(&TimeStamp, sizeof(TimeStamp));
	}

	void ParseCertificate(LPTSTR certDetails)
	{
		std::wstring buffer = certDetails;			
		std::string strCertDetails;

		CUtil::WStringToStdString(buffer, strCertDetails);

		C = ParseSubject(strCertDetails, "C=");
		S = ParseSubject(strCertDetails, "S=");
		L = ParseSubject(strCertDetails, "L=");
		O = ParseSubject(strCertDetails, "O=");
		OU = ParseSubject(strCertDetails, "OU=");
		CN = ParseSubject(strCertDetails, "CN=");
	}

	void AddTimeStamp(SYSTEMTIME time)
	{
		TimeStamp = time;
	}

	private:
	std::string ParseSubject(const std::string& input, std::string key)
	{
		size_t index = input.find(key);
		if (index == std::string::npos)
		{
			return "";
		}

		std::string outStr = input.substr(index, input.length() - index);

		outStr = outStr.substr(key.length(), outStr.length() - key.length());

		return outStr;
	}
};