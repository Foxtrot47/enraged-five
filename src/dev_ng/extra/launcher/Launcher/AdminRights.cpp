#include "stdafx.h"

#include "AdminRights.h"

#include <string>
#include <Windows.h>


CAdminRights::AdminRightState CAdminRights::sm_currentState = CAdminRights::ARS_UNKNOWN;

CAdminRights::AdminRightState CAdminRights::GetAdminRightsState()
{
	if (sm_currentState == ARS_UNKNOWN)
		sm_currentState = CheckAdminRights();

	return sm_currentState;
}

/*++ 
http://msdn.microsoft.com/en-us/library/windows/desktop/aa376389(v=vs.85).aspx
*/ 
CAdminRights::AdminRightState CAdminRights::CheckAdminRights()
{
	AdminRightState adminState = ARS_STANDARD_USER;

	SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;

	PSID AdministratorsGroup; 
	BOOL b = AllocateAndInitializeSid(&NtAuthority, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS,
		0, 0, 0, 0, 0, 0, &AdministratorsGroup);

	if (b) 
	{
		if (!CheckTokenMembership(NULL, AdministratorsGroup, &b)) 
		{
			b = FALSE;
		}
		FreeSid(AdministratorsGroup);
	}

	if (b)
	{
		adminState = ARS_ADMIN;
	}
	else
	{
		if (IsUACDisabled())
		{
			adminState = ARS_STANDARD_USER_NO_UAC;
		}
		else
		{
			adminState = ARS_STANDARD_USER;
		}
	}

	return adminState;
}


bool CAdminRights::IsUACDisabled()
{
	const TCHAR* key64 = TEXT("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System");
	const TCHAR* valName = TEXT("EnableLUA");

	std::string path;

	CRegKey regKey;
	if (regKey.Open(HKEY_LOCAL_MACHINE, key64) == ERROR_SUCCESS)
	{
		DWORD dwValue;
		if (regKey.QueryDWORDValue(valName, dwValue) == ERROR_SUCCESS)
		{
			if (dwValue == 0)
			{
				return true;
			}
		}
	}

	return false;
}