#pragma once

#if RSG_STEAM_LAUNCHER

#include "steam_api.h"

class SteamCallbacks
{
public:
	SteamCallbacks() 
		: m_SteamCallbackHandle(this, &SteamCallbacks::OnSteamInputDismissed) 
		, m_callResultGameOverlay(this, &SteamCallbacks::OnGameOverlayCallback)
	{

	}
	~SteamCallbacks() {}

	// PURPOSE:	Callback function to indicate the user has dismissed the steam text input
	STEAM_CALLBACK(SteamCallbacks, OnSteamInputDismissed, GamepadTextInputDismissed_t,	m_SteamCallbackHandle);
	STEAM_CALLBACK(SteamCallbacks, OnGameOverlayCallback, GameOverlayActivated_t,		m_callResultGameOverlay);
};

class CSteam
{
public:

	// PURPOSE
	//	Early init for Steam
	static void EarlyInit();

	// PURPOSE
	//	Initializes the Steam API, collects the Steam Auth Ticket
	static void Init();

	// PURPOSE
	//	Terminates the Steam API in advance of the game
	static void Shutdown();

	// PURPOSE
	//  Updates steam functionality
	static void Update();

	// PURPOSE
	//	Updates the language from steam settings
	static void UpdateSteamLanguage();
	
	// PURPOSE
	//	Returns the Stemm App ID
	static int GetSteamAppId();

	// PURPOSE
	//	Returns TRUE if the user is in offline mode or has no internet connection
	static bool IsOfflineMode();

	// PURPOSE
	//	Returns the Steam Auth Ticket
	static const char* GetSteamAuthTicket();

	// PURPOSE
	//	Returns the length of the Steam Auth Ticket
	static u32 GetSteamAuthTicketLen();

	// PURPOSE
	//	Returns the Steam user id
	static u64 GetSteamId();

	// PURPOSE
	//	Returns the Steam persona
	static const char* GetSteamPersonaName();

	// PURPOSE
	// Shows the virtual keyboard
	static bool ShowKeyboard(const char* text, const char* existing, bool bIsPassword, int maxNumChars);

	// PURPOSE
	// Returns TRUE if the keyboard state is pending/success
	static bool IsKeyboardPending() { return sm_KeyboardState == PENDING; }
	static bool IsKeyboardSuccessful() { return sm_KeyboardState == SUCCESS; }

	// PURPOSE
	// Returns the virtual keyboard result
	static const wchar_t* GetVirtualKeyboardResult();

	// PURPOSE
	// Refreshes the steam auth ticket
	static void RefreshSteamAuthTicket();

	// PURPOSE
	// Shows the steam store
	static void SteamStoreOpenCallback(u32 appId);

	// Keyboard State
	enum KeyboardState
	{
		NONE,
		PENDING,
		SUCCESS,
		CANCELLED
	};

	// Current Keyboard State
	static KeyboardState sm_KeyboardState;

	static SteamCallbacks sm_SteamCallbacks;
	static char sm_SteamInput[2048];
};

#endif // RSG_STEAM_LAUNCHER