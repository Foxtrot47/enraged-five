// Spinner.cpp : implementation file
//

#include "stdafx.h"
#include "Launcher.h"
#include "Spinner.h"
#include "Config.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>


// CSpinner

IMPLEMENT_DYNAMIC(CSpinner, CWnd)

CSpinner::CSpinner() : m_cachedBrightness(NULL), m_cachedAngle(NULL), m_cacheWidth(0), m_cacheHeight(0), m_scratchBuffer(NULL)
{
	m_bounds.SetRect(0,0,0,0);
	m_autoRepaint = false;
	m_timerRunning = false;
}

CSpinner::~CSpinner()
{
	//@@: location CSPINNER_DESTRUCTOR
	if (m_scratchBuffer)
	{
		delete[] m_scratchBuffer;
	}
}


BEGIN_MESSAGE_MAP(CSpinner, CWnd)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_TIMER()
END_MESSAGE_MAP()



// CSpinner message handlers

void CSpinner::OnPaint()
{
	RAD_TELEMETRY_WAIT_ZONE("Spinner OnPaint");

	if (m_autoRepaint)
		m_autoRepaint = false;
	else
	{
		m_autoRepaint = true;
		Invalidate();
		return;
	}

	if (!m_timerRunning)
	{
		m_timerRunning = true;
		SetTimer(1, 16, 0);
	}

	CPaintDC dc(this);

	CRect rect;
	GetClientRect(rect);

	if (m_bounds.Width() == 0 && m_bounds.Height() == 0)
	{
		m_bounds = rect;
	}
	else
	{
		if (rect.left < m_bounds.left) m_bounds.left = rect.left;
		if (rect.top < m_bounds.top) m_bounds.top = rect.top;
		if (rect.right > m_bounds.right) m_bounds.right = rect.right;
		if (rect.bottom > m_bounds.bottom) m_bounds.bottom = rect.bottom;
	}

	CRgn rgn;
	rgn.CreateRectRgn(m_bounds.left, m_bounds.top, m_bounds.right, m_bounds.bottom);
	dc.SelectClipRgn(&rgn);

	/*
	CBrush brush;
	brush.CreateSolidBrush(RGB(255,255,255));

	dc.FillRect(&rect, &brush);
	*/
	//@@: location CSPINNER_ONPAINT_FUDGEANGLE
	static float fudge_angle = 0.0f;
	fudge_angle += 0.04f;

#if 1

	int width = m_bounds.Width(), height = m_bounds.Height();
	float width_2 = (float)width / 2.0f, height_2 = (float)height / 2.0f;

	float min_side = (float)((width <= height) ? width : height) / 2.0f;
	float min_rad = 0.45f * min_side, max_rad = 0.90f * min_side;
	float min_rad_sq = min_rad * min_rad, max_rad_sq = max_rad * max_rad;

	if (!m_scratchBuffer)
		m_scratchBuffer = new float[width*height];

	float grey = 0;
	for (int y = 0; y < height; y++)
	{
		float dy = (float)y - height_2;
		float dy2 = dy*dy;
		for (int x = 0; x < width; x++)
		{
			//int grey = 255 * y / height;
			float dx = (float)x - width_2;

			float dist_sq = dx*dx + dy2;
			if (dist_sq < min_rad_sq || dist_sq > max_rad_sq)
				grey = 0.0f;
			else
			{
				float angle = (float)(atan2f(dy,dx) / (2.0 * M_PI) + 0.5f);

				angle = 1.0f - angle;
				angle += fudge_angle;
				angle = fmodf(angle, 1.0f);

				const float greyStart = 0.3f, greyEnd = 0.6f;

				if (angle < greyStart)
					grey = 1.0f;
				else if (angle < greyEnd)
					grey = (greyEnd - angle) / (greyEnd - greyStart);
				else
					grey = 0.0f;


				float dist = sqrt(dist_sq);
				if (dist < min_rad + 0.6f || dist > max_rad - 0.6f)
					grey = 0.0f;
			}

			m_scratchBuffer[x+y*width] = grey;
			//dc.SetPixel(x+m_bounds.left, y+m_bounds.top, RGB(grey,grey,grey));
		}
	}

	// Anti-alias pass
	float fg_r = (float)Constants::TextColourR.Get();
	float fg_g = (float)Constants::TextColourG.Get();
	float fg_b = (float)Constants::TextColourB.Get();

	float bg_r = (float)Constants::BackgroundR.Get();
	float bg_g = (float)Constants::BackgroundG.Get();
	float bg_b = (float)Constants::BackgroundB.Get();

	for (int y = 1; y < height-1; y++)
	{
		for (int x = 1; x < width-1; x++)
		{
			int i = x + y * height;
			const float center_factor = 20.0f;
			const float straight_factor = 1.0f;
			const float diag_factor = 1.0;

			float grey = m_scratchBuffer[i] * center_factor
				+ (m_scratchBuffer[i+1] + m_scratchBuffer[i-1] + m_scratchBuffer[i+width] + m_scratchBuffer[i-width]) * straight_factor
				+ (m_scratchBuffer[i-width-1] + m_scratchBuffer[i-width+1] + m_scratchBuffer[i+width-1] + m_scratchBuffer[i+width+1]) * diag_factor;
			grey /= center_factor + 4.0f * straight_factor + 4.0f * diag_factor;
			grey = std::max<float>(0.0f, std::min<float>(grey, 1.0f));
			float inv = 1.0f - grey;

			int r = (int)(fg_r * grey + bg_r * inv);
			int g = (int)(fg_g * grey + bg_g * inv);
			int b = (int)(fg_b * grey + bg_b * inv);

			dc.SetPixel(x+m_bounds.left, y+m_bounds.top, RGB(r,g,b));
		}
	}
#else
	int width = m_bounds.Width(), height = m_bounds.Height();

	if (width > m_cacheWidth || height > m_cacheHeight)
		MakeCache(width, height);

	float width_2 = (float)width / 2.0f, height_2 = (float)height / 2.0f;

	float min_side = (float)((width <= height) ? width : height) / 2.0f;
	float min_rad = 0.5f * min_side, max_rad = 0.95f * min_side;
	float min_rad_sq = min_rad * min_rad, max_rad_sq = max_rad * max_rad;

	int grey = 0;
	for (int y = 0; y < height; y++)
	{
		float dy = (float)y - height_2;
		float dy2 = dy*dy;
		for (int x = 0; x < width; x++)
		{
			//int grey = 255 * y / height;
			float dx = (float)x - width_2;

			float dist_sq = dx*dx + dy2;
			if (dist_sq < min_rad_sq || dist_sq > max_rad_sq)
				grey = 0;
			else
			{
				float angle = (float)(atan2f(dy,dx) / (2.0 * M_PI) + 0.5f);

				angle = 1.0f - angle;
				angle += fudge_angle;
				angle = fmodf(angle*1.5f, 1.5f);
				angle = 1.5f - angle;
				if (angle > 1.0f)
					angle = 0;


				if (dist_sq < min_rad_sq+16.0f || dist_sq > max_rad_sq-16.0f)
					grey = (int)(127.0f * angle);
				else
					grey = (int)(255.0f * angle);
			}
			dc.SetPixel(x+m_bounds.left, y+m_bounds.top, RGB(grey,grey,grey));
		}
	}
#endif
}

void CSpinner::MakeCache(int width, int height)
{
	if (m_cachedBrightness)
		delete[] m_cachedBrightness;
	if (m_cachedAngle)
		delete[] m_cachedAngle;

	m_cachedBrightness = new float[width * height];
	m_cachedAngle = new float[width * height];

	m_cacheWidth = width;
	m_cacheHeight = height;

	float width_2 = (float)width / 2.0f, height_2 = (float)height / 2.0f;

	const float min_side = (float)((width <= height) ? width : height) / 2.0f;
	const float min_rad = 0.5f * min_side, max_rad = 0.95f * min_side;
	const float min_rad_sq = min_rad * min_rad, max_rad_sq = max_rad * max_rad;

	for (int y = 0; y < height; y++)
	{
		float dy = (float)y - height_2;
		float dy2 = dy*dy;
		for (int x = 0; x < width; x++)
		{
			int index = x + y * width;

			//int grey = 255 * y / height;
			float dx = (float)x - width_2;

			float dist_sq = dx*dx + dy2;
			if (dist_sq < min_rad_sq || dist_sq > max_rad_sq)
			{
				m_cachedBrightness[index] = 0;
			}
			else
			{
				float angle = (float)(atan2f(dy,dx) / (2.0 * M_PI) + 0.5f);
				angle = 1.0f - angle;
				
				m_cachedAngle[index] = angle;

				if (dist_sq < min_rad_sq+16.0f || dist_sq > max_rad_sq-16.0f)
					m_cachedBrightness[index] = 0.5f;
				else
					m_cachedBrightness[index] = 1.0f;
			}
		}
	}
}

void CSpinner::OnTimer(UINT_PTR /*nIDEvent*/)
{
	m_autoRepaint = true;
	Invalidate();
	KillTimer(1);
	m_timerRunning = false;
}

void CSpinner::QueuePaint()
{

}

BOOL CSpinner::OnEraseBkgnd(CDC* /*pDC*/)
{
	//return CWnd::OnEraseBkgnd(pDC);
	return FALSE;
}