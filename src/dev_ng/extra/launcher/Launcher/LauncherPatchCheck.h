#pragma once

#include "IPatchCheck.h"
#include <string>

class LauncherPatchCheck : public IPatchCheck
{
public:
	LauncherPatchCheck(void);
	~LauncherPatchCheck(void);

	virtual Version GetExecutableVersion();

	virtual void DownloadComplete(DownloadCompleteStatus status, std::string filename);
	virtual void DownloadError(const CError& errorString);

	virtual void ParseXML();
	virtual void OnAddPatch(Patch& patch);

private:
	std::string GetLauncherExecutableFilename();
};
