// DoubleBufferedEdit.cpp : implementation file
//

#include "stdafx.h"
#include "Launcher.h"
#include "LauncherDlg.h"
#include "DoubleBufferedEdit.h"


// CDoubleBufferedEdit

IMPLEMENT_DYNAMIC(CDoubleBufferedEdit, CEdit)

CDoubleBufferedEdit::CDoubleBufferedEdit()
{

}

CDoubleBufferedEdit::~CDoubleBufferedEdit()
{
}

afx_msg void CDoubleBufferedEdit::OnPaint()
{
#if 0

	// Based on http://www.codeproject.com/Articles/33/Flicker-Free-Drawing-In-MFC
	// and with suggestions from http://forums.codeguru.com/archive/index.php/t-381891.html

	// Get usual context and bounds
	CPaintDC dc(this);
	CRect bounds;
	GetClientRect(&bounds);
	bounds.bottom -= 20;

	// Create memory-backed DC
	CDC memDC;
	memDC.CreateCompatibleDC(&dc);

	// Create second buffer
	// TODO: Cache this somehow to avoid repeatedly creating bitmaps
	dc.LPtoDP(&bounds);
	CBitmap buffer;
	buffer.CreateCompatibleBitmap(&dc, bounds.Width(), bounds.Height());
	CBitmap* oldBitmap = memDC.SelectObject(&buffer);
	
	// Set parameters
	memDC.SetMapMode(dc.GetMapMode());
	memDC.SetWindowExt(dc.GetWindowExt());
	memDC.SetViewportExt(dc.GetViewportExt());

	// Set correct coordinates
	dc.DPtoLP(&bounds);
	memDC.SetWindowOrg(bounds.left, bounds.top);

	// Clear background
	memDC.FillSolidRect(&bounds, dc.GetBkColor());

	// Do default drawing
	DefWindowProc(WM_PAINT, (WPARAM)memDC.m_hDC, 0);

	// Blit the buffer
	dc.BitBlt(bounds.left, bounds.top, bounds.Width(), bounds.Height(), &memDC, bounds.left, bounds.top, SRCCOPY);

	// Clean up
	memDC.SelectObject(oldBitmap);

#else

	CPaintDC dc(this);
	DefWindowProc(WM_PAINT, (WPARAM)dc.m_hDC, 0);
	
	//CEdit::OnPaint();
#endif
}

afx_msg BOOL CDoubleBufferedEdit::OnEraseBkgnd(CDC* /*pDC*/)
{
	return TRUE;// CEdit::OnEraseBkgnd(pDC);
}

afx_msg void CDoubleBufferedEdit::OnSetFocus(CWnd* /*pOldWnd*/)
{
	CLauncherDlg::GetLastInstance()->TextClicked();
}


BEGIN_MESSAGE_MAP(CDoubleBufferedEdit, CEdit)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_SETFOCUS()
END_MESSAGE_MAP()



// CDoubleBufferedEdit message handlers


