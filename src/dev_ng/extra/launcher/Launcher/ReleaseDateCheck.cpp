#include "stdafx.h"

#include "InPlaceDownloader.h"

#if RSG_PRELOADER_LAUNCHER

#include "ReleaseDateCheck.h"

#include "Config.h"
#include "OnlineConfig.h"
#include "Fsm.h"
#include "Globals.h"
#include "LauncherDlg.h"
#include "Localisation.h"
#include "RockstarDownloader.h"
#include "VersionManager.h"
#include "Util.h"

#include <string>

u64 CReleaseDateCheck::sm_releaseDateSystemUptimeMillis = 0;
u64 CReleaseDateCheck::sm_lastReleaseDateCheckSystemUptimeMillis = 0;
bool CReleaseDateCheck::sm_releaseDateInFuture = false;
CWinThread* CReleaseDateCheck::sm_countdownThread = NULL;

bool CReleaseDateCheck::sm_countdownThreadRunning = false;

s64 FiletimeToSecondsSinceEpoch(FILETIME* ft)
{
	LARGE_INTEGER lint;
	lint.LowPart = ft->dwLowDateTime;
	lint.HighPart = ft->dwHighDateTime;
	return lint.QuadPart / (10 * 1000 * 1000);
}

void CReleaseDateCheck::CheckSynchronous()
{
	sm_lastReleaseDateCheckSystemUptimeMillis = GetTickCount64();

#if RSG_COMBINED_VERSIONING
	if (!VersionManager::GetReleaseDateSystemMillis(sm_releaseDateSystemUptimeMillis))
	{
		if (!CError::GetLastError().IsError())
		{
			CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_NO_CONNECTION);
		}
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eOffline);
		return;
	}
#else
	std::string tempLocation;
	CUtil::PrependTempoaryFolderPath(tempLocation, "release_date.xml");

	std::string url;
	std::string server, docpath;
	CUtil::GetServerAndDocpathForEnv(server, docpath, Constants::ReleaseDatePathProd);

	url = server + docpath;

	struct Callback : public IDownloader::DownloadListener
	{
		bool m_error;
		Callback() : m_error(false) {}

		virtual void DownloadComplete(const Patch& /*patch*/, DownloadCompleteStatus /*status*/)
		{
		}

		virtual void DownloadStatus(const Patch& /*patch*/, DownloadCompleteStatus /*status*/, u64 /*totalBytesComplete*/, u64 /*totalBytesInFile*/, u64 /*bytesJustDownloaded*/)
		{
		}

		virtual void DownloadError(const Patch& /*patch*/, const CError& /*error*/)
		{
			m_error = true;
		}

	} callback;


	FILETIME serverFt;
	if (!RockstarDownloader::GetServerTime(url, serverFt))
	{
		ERRORF("Can't get server time.");
		return;
	}

	RockstarDownloader downloader;

	downloader.SetListener(&callback);
	downloader.SetCurrentFile(url, tempLocation, false, true);
	downloader.StartSynchronously();

	if (callback.m_error)
	{
		ERRORF("Can't get release date.");
		return;
	}

	TiXmlDocument doc(tempLocation.c_str());
	bool bLoaded = doc.LoadFile();
	if (!bLoaded)
	{
		ERRORF("Can't load downloaded xml!");
		return;
	}

	TiXmlHandle docHandle(&doc);
	TiXmlElement* element = docHandle.FirstChild("ReleaseDate").ToElement();
	if (!element)
	{
		ERRORF("No element.");
		return;
	}

	SYSTEMTIME sysTime;
	InternetTimeToSystemTimeA(element->GetText(), &sysTime, 0);
	FILETIME releaseDateFt;
	SystemTimeToFileTime(&sysTime, &releaseDateFt);

	s64 timeRemainingSeconds = FiletimeToSecondsSinceEpoch(&releaseDateFt) - FiletimeToSecondsSinceEpoch(&serverFt);

#if RSG_DEBUG
	timeRemainingSeconds = std::min<s64>(timeRemainingSeconds, 70);
#endif

	sm_releaseDateSystemUptimeMillis = (u64)((s64)GetTickCount64() + timeRemainingSeconds * 1000LL);
#endif

	sm_releaseDateInFuture = sm_releaseDateSystemUptimeMillis > GetTickCount64();

	CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);
}

void CReleaseDateCheck::StartCountdownThread()
{
	sm_countdownThreadRunning = true;
	sm_countdownThread = AfxBeginThread(CountdownThreadEntry, NULL, 0, CREATE_SUSPENDED);
	sm_countdownThread->m_bAutoDelete = false;
	sm_countdownThread->ResumeThread();
}

void CReleaseDateCheck::JoinCountdownThread()
{
	if (sm_countdownThread)
	{
		if (sm_countdownThreadRunning)
		{
			sm_countdownThreadRunning = false;
			WaitForSingleObject(sm_countdownThread, 2000);
		}

		delete sm_countdownThread;
		sm_countdownThread = NULL;
	}
}

UINT CReleaseDateCheck::CountdownThreadEntry(void* /*param*/)
{
	while (sm_countdownThreadRunning)
	{
		s64 currentUptimeMillis = (s64)GetTickCount64();


		s64 millisSinceLastReleaseCheck = currentUptimeMillis - (s64)sm_lastReleaseDateCheckSystemUptimeMillis;

		const s64 millisPerCheck = 1000 * 60 * 10; // 10 minutes

		if (millisSinceLastReleaseCheck > millisPerCheck)
		{
			VersionManager::Reset();

			u64 out_millis;
			if (VersionManager::GetReleaseDateSystemMillis(out_millis))
			{
				sm_releaseDateSystemUptimeMillis = out_millis;
			}

			sm_lastReleaseDateCheckSystemUptimeMillis = (u64)currentUptimeMillis;
		}

		s64 secondsToGo = ((s64)sm_releaseDateSystemUptimeMillis - currentUptimeMillis) / 1000LL;

		if (secondsToGo > 0)
		{
			s64 seconds = secondsToGo % 60;
			s64 minutes = (secondsToGo / 60) % 60;
			s64 hours = (secondsToGo / (60 * 60)) % 24;
			s64 days = (secondsToGo / (60 * 60 * 24)) % 7;
			s64 weeks = secondsToGo / (60 * 60 * 24 * 7);

			wchar_t buffer[256];
			std::vector<std::wstring> params;
			swprintf_s(buffer, L"%lu", weeks);
			params.push_back(std::wstring(buffer));
			swprintf_s(buffer, L"%lu", days);
			params.push_back(std::wstring(buffer));
			swprintf_s(buffer, L"%02lu", hours);
			params.push_back(std::wstring(buffer));
			swprintf_s(buffer, L"%02lu", minutes);
			params.push_back(std::wstring(buffer));
			swprintf_s(buffer, L"%02lu", seconds);
			params.push_back(std::wstring(buffer));

			std::wstring estimateString;

			if (weeks > 0)
				estimateString = CLocalisation::Instance().GetWString(IDS_RELEASEDATE_WEEKS);
			else if (days > 0)
				estimateString = CLocalisation::Instance().GetWString(IDS_RELEASEDATE_DAYS);
			else if (hours > 0)
				estimateString = CLocalisation::Instance().GetWString(IDS_RELEASEDATE_HOURS);
			else if (minutes > 0)
				estimateString = CLocalisation::Instance().GetWString(IDS_RELEASEDATE_MINS);
			else
				estimateString = CLocalisation::Instance().GetWString(IDS_RELEASEDATE_SECS);
		
			CUtil::FormatString(estimateString, params);

			CLauncherDlg::GetLastInstance()->SetStatusTextSafe(estimateString);

			for (int i = 0; i < 10 && sm_countdownThreadRunning; i++)
				Sleep(100);
		}
		else
		{
			sm_countdownThreadRunning = false;
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eReleaseDateRestart);
		}
	}

	return 0;
}

#endif