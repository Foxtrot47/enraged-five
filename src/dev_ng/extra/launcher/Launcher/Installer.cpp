#include "StdAfx.h"
#include "Installer.h"
#include "Channel.h"
#include "Globals.h"
#include "Application.h"

#include "Util.h"

#include <sstream>

Installer::Installer(void)
{
	m_listener = NULL;
	m_bCancelled = false;
}

Installer::~Installer(void)
{
}

Installer& Installer::Instance()
{
	//@@: location INSTALLER_INSTANCE_CREATE_INSTANCE
	static Installer instance;
	return instance;
}

void Installer::InstallPatch(Patch& patch)
{
	m_bCancelled = false;

	HANDLE processHandle = 0;

	InstallState state = InstallPatch(patch, &processHandle);
	//@@: range INSTALLER_INSTALLPATCH_INSTALLATION_ERROR {

	if (state == INSTALL_ERROR)
	{
		//@@: location INSTALLER_INSTALLPATCH_CHECK_LISTENER
		if (m_listener != NULL)
		{
			m_listener->InstallError(patch.LocalFilename, patch.Description, CError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_INSTALL_FAILED));
			m_listener->InstallComplete(false, patch.LocalFilename, patch.Description, patch.SkuName, 0);
		}
	}
	//@@: } INSTALLER_INSTALLPATCH_INSTALLATION_ERROR

	//@@: range INSTALLER_INSTALLPATCH_INSTALLATION_RESTART {
	else if (state == INSTALL_RESTART)
	{
		DISPLAYF("Install needs restart.");
		if (m_listener != NULL)
			//@@: location INSTALLER_INSTALLPATCH_INSTALLATION_RESTART_CALL
			m_listener->InstallRestart();
	}
	//@@: } INSTALLER_INSTALLPATCH_INSTALLATION_RESTART
	else if (state == INSTALL_SHUTDOWN)
	{
		if (m_listener != NULL)
			m_listener->InstallShutdown();
	}

	//@@: range INSTALLER_INSTALLPATCH_INSTALLATION_RUNNING {
	else if (state == INSTALL_RUNNING)
	{
		DEBUGF1("Waiting for handle %x...", processHandle);
		
		//@@: location INSTALLER_INSTALLPATCH_WAIT_FOR_HANDLE
		while (!m_bCancelled && WaitForSingleObject(processHandle, 100) == WAIT_TIMEOUT)
		{
			// This space left intentionally blank
		}

		if (m_bCancelled)
		{
			m_listener->InstallError(patch.LocalFilename, patch.Description, CError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_INSTALL_FAILED));
			return;
		}

		DEBUGF1("Finished waiting.");

		DWORD exitCode = 0;

		BOOL result = GetExitCodeProcess(processHandle, &exitCode);
		//@@: location INSTALLER_INSTALLPATCH_CLOSE_PROCESS_HANDLE
		CloseHandle(processHandle);

		if (result)
		{
			DEBUGF1("Exit code: %x", exitCode);
			//@@: location INSTALLER_INSTALLPATCH_EXIT_EARLY
			InstallProcess_Exited(patch, (int)exitCode);
		}
		else
		{
			if (m_listener != NULL)
			{
#if !RSG_FINAL
				if(!Globals::testFlow)
#endif
				{
					m_listener->InstallError(patch.LocalFilename, patch.Description, CError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_INSTALL_FAILED));
				}
				//@@: location INSTALLER_INSTALLPATCH_SET_LISTENER_COMPLETE
				m_listener->InstallComplete(exitCode == 0, patch.LocalFilename, patch.Description, patch.SkuName, exitCode);
			}
		}
	}
	//@@: } INSTALLER_INSTALLPATCH_INSTALLATION_RUNNING

}

Installer::InstallState Installer::InstallPatch(Patch& patch, HANDLE* out_processHandle)
{
	DEBUGF1("Installing patch %s...", patch.LocalFilename.c_str());

	std::wstring wpath, wparams;

	CUtil::StdStringToStdWString(patch.LocalFilename, wpath);

	if (patch.CommandlineArguments.length() > 0)
	{
		CUtil::StdStringToStdWString(patch.CommandlineArguments, wparams);
	}

	DEBUGF1("Preparing...");

	//@@: location INSTALLER_INSTALLPATCH_BEGIN_INSTALLATION
	SHELLEXECUTEINFOW shellex;
	ZeroMemory(&shellex, sizeof(shellex));

	//@@: range INSTALLER_INSTALLPATCH_BUILD_ARGUMENTS {
	shellex.cbSize = sizeof(shellex);
	shellex.fMask = SEE_MASK_NOCLOSEPROCESS;
	shellex.lpVerb = L"runas";
	//@@: location INSTALLER_INSTALLPATCH_BUILD_LOCALFILENAME
	shellex.lpFile = wpath.c_str();
	shellex.lpParameters = wparams.c_str();

	// For the MTL upgrade, we need to show it
	if (patch.PatchType == PatchType::PT_MTL)
	{
		shellex.nShow = SW_SHOW;
	}
	else
	{
		shellex.nShow = SW_HIDE;
	}

	//@@: } INSTALLER_INSTALLPATCH_BUILD_ARGUMENTS

	DISPLAYF("About to launch process: %s (with commandline: %s)", patch.LocalFilename.c_str(), patch.CommandlineArguments.c_str());

	//@@: range INSTALLER_INSTALLPATCH_EXECUTE_INSTALLATION {
#if !RSG_FINAL
	if (!Globals::testFlow) {
#endif
		//@@: location INSTALLER_INSTALLPATCH_SHELL_EXECUTE
		if (!ShellExecuteExW(&shellex))
		{
			DWORD error = GetLastError();
			ERRORF("Error launching process: %d", error);
			CUtil::PrintSystemErrorMessage(GetLastError());

			// Possibly a corrupt file, so delete it
			CFileWrapper::DeleteFileA(patch.LocalFilename.c_str());

			return INSTALL_ERROR;
		}
#if !RSG_FINAL
	}
	else
	{
		return INSTALL_RUNNING;
	}
#endif

	//@@: } INSTALLER_INSTALLPATCH_EXECUTE_INSTALLATION

	//@@: location INSTALLER_INSTALLPATCH_SET_PROCESS_HANDLE
	if (out_processHandle)
		*out_processHandle = shellex.hProcess;

	DEBUGF1("Pid: %d", GetProcessId(shellex.hProcess));

	//@@: range INSTALLER_INSTALLPATCH_SET_STATE {
	//@@: location INSTALLER_INSTALLPATCH_CHECK_RESTART
	if (patch.RequiresRestart)
		return INSTALL_RESTART;
	else if (patch.RequiresShutdown)
	{
		return INSTALL_SHUTDOWN;
	}
	else
		return INSTALL_RUNNING;
	//@@: } INSTALLER_INSTALLPATCH_SET_STATE

}

void Installer::InstallProcess_Exited(Patch& patch, int exitCode)
{
	//@@: range INSTALLER_INSTALLPROCESS_EXITED {
	bool shouldDelete = false;
	bool wasCorrupt = false;
	if (exitCode == 0)
	{
		if (patch.DeleteOnCompletion)
		{
			shouldDelete = true;
		}
	}
	else
	{
		if (m_listener)
		{
			m_listener->InstallError(patch.LocalFilename, patch.Description, CError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_INSTALL_FAILED));
		}

		shouldDelete = true;
		wasCorrupt = true;
	}

	if (shouldDelete)
	{
		for (int i = 0; i < 3; i++)
		{
			// Windows can take quite a while to stop actually using the file, even after the process has exited.
			Sleep(1000);
			DISPLAYF("Attempting to delete %s: %s", wasCorrupt ? "corrupt patch" : "patch", patch.LocalFilename.c_str());
			if (CFileWrapper::DeleteFileA(patch.LocalFilename.c_str()))
			{
				break;
			}
			else
			{
				ERRORF("Unable to delete %s: %s", wasCorrupt ? "corrupt patch" : "patch", patch.LocalFilename.c_str());
				CUtil::PrintSystemErrorMessage(GetLastError());
			}
		}
	}


	if (m_listener)
		//@@: location INSTALLER_INSTALLPATCH_INSTALL_COMPLETE
			m_listener->InstallComplete(exitCode == 0, patch.LocalFilename, patch.Description, patch.SkuName, exitCode); 
	//@@: } INSTALLER_INSTALLPROCESS_EXITED

}
