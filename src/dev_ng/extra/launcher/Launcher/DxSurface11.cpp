#include "stdafx.h"

#if !RSG_FINAL

#include "DxSurface11.h"
#include "Channel.h"

//#pragma comment(lib,"d3d9.lib")
//#pragma comment(lib,"d3dx9.lib")
//#pragma comment(lib,"dxerr.lib")

#pragma comment(lib,"DXGI.lib")
#pragma comment(lib,"d3d11.lib")

IMPLEMENT_DYNAMIC(DxSurface11, CWnd)


BEGIN_MESSAGE_MAP(DxSurface11, CWnd)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_CHAR()
END_MESSAGE_MAP()


DxSurface11::DxSurface11()
	: m_window(NULL), m_painting(false), m_parentWindow(NULL), m_bShown(false), m_bLostDevice(false), m_device(NULL), m_deviceContext(NULL)
{
	
}

DxSurface11::~DxSurface11()
{
	DestroyDevice();
}

LRESULT DxSurface11::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_KEYDOWN)
	{
		DEBUGF1("DxSurface11 Keydown: %d", wParam);
	}
	return CWnd::WindowProc(message, wParam, lParam);
}

LRESULT DxSurface11::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_KEYDOWN)
	{
		DEBUGF1("DxSurface11 Def keydown: %d", wParam);
	}
	return CWnd::DefWindowProc(message, wParam, lParam);
}

BOOL DxSurface11::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		DEBUGF1("DxSurface11 PreTranslate keydown: %d", pMsg->wParam);
		TranslateMessage(pMsg);
		DispatchMessage(pMsg);
		return TRUE;
	}
	return CWnd::PreTranslateMessage(pMsg);
}

void DxSurface11::OnPaint()
{
}

bool DxSurface11::Show(bool show)
{
	if (show != m_bShown)
	{
		DEBUGF3(show ? "Showing DxSurface9." : "Hiding DxSurface9.");
		ShowWindowAsync(GetSafeHwnd(), show ? SW_SHOW : SW_HIDE);
		m_bShown = show;
		return true;
	}
	else
	{
		return false;
	}
}


void DxSurface11::Render()
{

	if (m_device)
	{
#define CHECK_HRESULT(hr) do { HRESULT result = (hr); if (FAILED(result)) { ERRORF("HRESULT failed when doing " #hr " : %x", result); error = true; } } while (0);

		m_deviceContext->OMSetRenderTargets(1, &m_renderTargetView, NULL);

		m_paintDelegate->OnPaint();

		m_swapChain->Present(1, 0);
	}
}

BOOL DxSurface11::OnEraseBkgnd(CDC* /*pDC*/)
{
	//return CWnd::OnEraseBkgnd(pDC);
	return FALSE;
}

HRESULT DxSurface11::CreateDevice()
{
	// Create device
	D3D_FEATURE_LEVEL featurelevel = D3D_FEATURE_LEVEL_11_0;

	// Calculate window size
	RECT windowRect;
	GetClientRect(&windowRect);

	int width = windowRect.right - windowRect.left, height = windowRect.bottom - windowRect.top;
	if (width <= 49 || height <= 49)
	{
		WARNINGF("Warning: Invalid DX window size.");
		width = 50;
		height = 50;
	}
	else
		DISPLAYF("Window size: %d x %d", width, height);

	ZeroMemory(&m_desc, sizeof(m_desc));
	m_desc.BufferCount = 1;
	m_desc.BufferDesc.Width = width;
	m_desc.BufferDesc.Height = height;
	m_desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	m_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	m_desc.OutputWindow = GetSafeHwnd();
	m_desc.SampleDesc.Count = 1;
	m_desc.SampleDesc.Quality = 0;
	m_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	m_desc.Windowed = TRUE;
	
	D3D_FEATURE_LEVEL feat;
	HRESULT hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, D3D11_CREATE_DEVICE_BGRA_SUPPORT, &featurelevel, 1, D3D11_SDK_VERSION, &m_desc, &m_swapChain, &m_device, &feat, &m_deviceContext);

	ID3D11Texture2D* backBuffer;
	m_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backBuffer);

	m_device->CreateRenderTargetView(backBuffer, NULL, &m_renderTargetView);

	D3D11_TEXTURE2D_DESC backBufferDesc = {0};
	backBuffer->GetDesc(&backBufferDesc);

	CD3D11_VIEWPORT viewport(
		0.0f,
		0.0f,
		static_cast<float>(backBufferDesc.Width),
		static_cast<float>(backBufferDesc.Height)
		);

	m_deviceContext->RSSetViewports(1, &viewport);

	DEBUGF1("Created DX11 device.");

	return hr;
}
#define SAFERELEASE(x) if (x) { x->Release(); x = NULL; }
void DxSurface11::DestroyDevice()
{
	SAFERELEASE(m_renderTargetView);
	SAFERELEASE(m_swapChain);
	SAFERELEASE(m_deviceContext);
	SAFERELEASE(m_device);
}

void DxSurface11::DrawTest()
{
}

void DxSurface11::NullDraw()
{
}

void DxSurface11::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}

void DxSurface11::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

void DxSurface11::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CWnd::OnChar(nChar, nRepCnt, nFlags);
}

IUnknown* DxSurface11::GetDxContext()
{
	if (!m_device)
	{
		CreateDevice();
	}

	return m_device;
}

void* DxSurface11::GetPresentParams()
{
	if (!m_device)
	{
		CreateDevice();
	}

	if (!m_device)
	{
		ERRORF("Can't create device.");
		return NULL;
	}

	return (void*)&m_desc;
}

#endif // !RSG_FINAL