#include "stdafx.h"
#include "WorkerThread.h"

#include "Launcher.h"
#include "LauncherDlg.h"

#include "Util.h"
#include "Test.h"
#include "Application.h"
#include "Globals.h"
#include "InPlaceDownloader.h"
#include "OnlineConfig.h"
#include "ReleaseDateCheck.h"

#if RSG_STEAM_LAUNCHER
#include "Steam.h"
#endif

static volatile unsigned int workerThreadTamperDWORD;

void workerThreadShiftOntoResult(int param)
{
	//@@: location WORKERTHREADSHIFTONTORESULT_RANGE_ENTRY
	param^=0xB8E6FD6D;
	
	switch(workerThreadTamperDWORD)
	{
	//@@: range WORKERTHREADSHIFTONTORESULT_RANGE {
	case 0x80994D8C:
		//@@: location WORKERTHREADSHIFTONTORESULT_RANGE_A
		workerThreadTamperDWORD&=param;
		break;
	case 0x5FA7EB96:
		//@@: location WORKERTHREADSHIFTONTORESULT_RANGE_B
		workerThreadTamperDWORD|=param;
		break;
	case 0x57C59018:
		//@@: location WORKERTHREADSHIFTONTORESULT_RANGE_C
		workerThreadTamperDWORD^=param;
		break;
	case 0x71F6510B:
		//@@: location WORKERTHREADSHIFTONTORESULT_RANGE_D
		workerThreadTamperDWORD<<=param;
		break;
	case 0x56C6C97C:
		//@@: location WORKERTHREADSHIFTONTORESULT_RANGE_E
		workerThreadTamperDWORD>>=param;
		break;
	default:
		break;

		//@@: } WORKERTHREADSHIFTONTORESULT_RANGE
	}
	//@@: location WORKERTHREADSHIFTONTORESULT_RANGE_EXIT
	workerThreadTamperDWORD*=0;
}

WorkerThread::WorkerThread()
	: m_bThreadStarted(false), m_bThreadShouldExit(false), m_thread(NULL), m_currentJob(JOB_NONE)
{
	workerThreadShiftOntoResult(0);
	workerThreadTamperDWORD = 0;
	//@@: location WORKERTHREAD_CONSTRUCTOR
	InitializeCriticalSection(&m_criticalSection);
	m_semaphore = CreateSemaphore(NULL, 0, INT_MAX, NULL);
}

WorkerThread::~WorkerThread()
{
	Stop();
	//@@: location WORKERTHREAD_DESTRUCTOR
	DeleteCriticalSection(&m_criticalSection);
	CloseHandle(m_semaphore);
}

WorkerThread& WorkerThread::Instance()
{
	static WorkerThread instance;

	return instance;
}

void WorkerThread::Start()
{
	if (!m_bThreadStarted)
	{
		m_bThreadShouldExit = false;
		//@@: location WORKERTHREAD_START_BEGIN_THREAD
		m_thread = AfxBeginThread(WorkerThread::ThreadEntryPoint, NULL);
	}
}

void WorkerThread::Stop()
{
	DEBUGF1("Signaling WorkerThread to stop.");
	m_bThreadShouldExit = true;

	EnterCriticalSection(&m_criticalSection);

	if (m_currentJob != JOB_NONE)
	{
		CancelJob(m_currentJob);
	}

	while (!m_jobQueue.empty())
		m_jobQueue.pop();

	LeaveCriticalSection(&m_criticalSection);

	ReleaseSemaphore(m_semaphore, 1, NULL);
}

void WorkerThread::Join()
{
	DEBUGF1("Joining WorkerThread.");
	Stop();

	if (m_thread)
		WaitForSingleObject(m_thread->m_hThread, INFINITE);
}

void WorkerThread::AddJob(const JobType& job)
{
	EnterCriticalSection(&m_criticalSection);

	//@@: location WORKERTHREAD_ADDJOB_QUEUE
	if (m_currentJob == job)
	{
		return;
	}
	m_jobQueue.push(job);
	ReleaseSemaphore(m_semaphore, 1, NULL);

	LeaveCriticalSection(&m_criticalSection);
}

UINT WorkerThread::ThreadEntryPoint(LPVOID /*param*/)
{
	CUtil::SetCurrentThreadName("Launcher Background Worker Thread");

	//@@: location WORKERTHREAD_THREADENTRYPOINT
	Instance().MainThreadLoop();

	return 0;
}

void WorkerThread::MainThreadLoop()
{
	m_bThreadStarted = true;

	while (!m_bThreadShouldExit)
	{
		//@@: range WORKERTHREAD_MAINTHREADLOOP {
		JobType eJob;

		bool bJobFound = false;

		// Block on the semaphore, for up to 1 minute
		WaitForSingleObject(m_semaphore, 60 * 1000);

		EnterCriticalSection(&m_criticalSection);
		if (!m_jobQueue.empty())
		{
			//@@: location WORKERTHREAD_MAINTHREADLOOP_POP_JOB
			eJob = m_jobQueue.front();
			m_jobQueue.pop();
			bJobFound = true;
		}
		LeaveCriticalSection(&m_criticalSection);

		if (bJobFound)
		{
			//@@: location WORKERTHREAD_MAINTHREADLOOP_DO_JOB
			DoJob(eJob);
		}
		else
		{
			//@@: location WORKERTHREAD_MAINTHREADLOOP_ON_SLEEP
			Sleep(1);
		}
		//@@: } WORKERTHREAD_MAINTHREADLOOP

	}

	m_bThreadStarted = false;
}

const char* WorkerThread::GetJobTypeName(const JobType& jobType)
{
#if !RSG_FINAL
	switch (jobType)
	{
		case JOB_DOWNLOAD_ONLINE_CONFIG:		return "JOB_DOWNLOAD_ONLINE_CONFIG";
		case JOB_CHECK_FOR_SC_UPDATES:			return "JOB_CHECK_FOR_SC_UPDATES";
		case JOB_CHECK_FOR_GAME_UPDATES:		return "JOB_CHECK_FOR_GAME_UPDATES";
		case JOB_DOWNLOAD_UPDATE:				return "JOB_DOWNLOAD_UPDATE";
		case JOB_INSTALL_UPDATE:				return "JOB_INSTALL_UPDATE";
		case JOB_UPLOAD_REPORT:					return "JOB_UPLOAD_REPORT";
#if RSG_IN_PLACE_DOWNLOAD
		case JOB_CHECK_FOR_IN_PLACE_DOWNLOAD:	return "JOB_CHECK_FOR_IN_PLACE_DOWNLOAD";
		case JOB_DOWNLOAD_IN_PLACE:				return "JOB_DOWNLOAD_IN_PLACE";
		case JOB_DOWNLOAD_IN_PLACE_IN_BACKGROUND: return "JOB_DOWNLOAD_IN_PLACE_IN_BACKGROUND";
		case JOB_VERIFY_INTEGRITY:				return "JOB_VERIFY_INTEGRITY";
#endif
#if RSG_PRELOADER_LAUNCHER
		case JOB_CHECK_FOR_RELEASE_DATE:		return "JOB_CHECK_FOR_RELEASE_DATE";
#endif
		default: return "Unknown Job Type!";
	}
#else
	(void)jobType;
	return "";
#endif
	
}

void WorkerThread::DoJob(const JobType& jobType)
{
	DEBUGF1("Beginning job: %s", GetJobTypeName(jobType));

	RAD_TELEMETRY_ZONE("Job: %s", GetJobTypeName(jobType));

	m_currentJob = jobType;
	//@@: range WORKERTHREAD_DOJOB_JOBS {

	switch (jobType)
	{
	default:
		ERRORF("Invalid job type!");
		break;

	case JOB_DOWNLOAD_ONLINE_CONFIG:
		COnlineConfigOption::DownloadOptionsSynchronous();
		break;

	case JOB_CHECK_FOR_SC_UPDATES:
		//@@: range WORKERTHREAD_DOJOB_UPDATE_SC {
		//@@: location WORKERTHREAD_DOJOB_RUNUPDATECHECK
		LauncherApp::RunUpdateCheck(true, 0);
		break;
		//@@: } WORKERTHREAD_DOJOB_UPDATE_SC

#if RSG_STEAM_LAUNCHER
	case JOB_STEAM_INIT:
		CSteam::Init();
		break;
	case JOB_STEAM_SHUTDOWN:
		CSteam::Shutdown();
		break;
#endif // RSG_STEAM_LAUNCHER

#if RSG_IN_PLACE_DOWNLOAD
	case JOB_CHECK_FOR_IN_PLACE_DOWNLOAD:
		InPlaceDownloader::CheckManifestSynchronous();
		break;
#endif

	case JOB_CHECK_FOR_GAME_UPDATES:
#if !RSG_FINAL
		if (DebugSwitches::FakeDownloadingOnly)
		{
			CTest::TestFakeDownloadUI();
		}
		else
#endif
		{
			//@@: range WORKERTHREAD_DOJOB_UPDATE_GAME {
			//@@: location WORKERTHREAD_DOJOB_UPDATE_GAME_LOCATION
			LauncherApp::RunUpdateCheck(false, CLauncherDlg::GetLastInstance()->GetSignedInRockstarId());
			//@@: } WORKERTHREAD_DOJOB_UPDATE_GAME
		}
		break;

#if RSG_IN_PLACE_DOWNLOAD
	case JOB_DOWNLOAD_IN_PLACE:
		InPlaceDownloader::DownloadFilesSynchronous(InPlaceDownloader::IN_FOREGROUND);
		break;

	case JOB_VERIFY_INTEGRITY:
		InPlaceDownloader::PerformIntegrityCheckSynchronous();
		break;

	case JOB_DOWNLOAD_IN_PLACE_IN_BACKGROUND:
		InPlaceDownloader::DownloadFilesSynchronous(InPlaceDownloader::IN_BACKGROUND);
		break;
#endif

	case JOB_DOWNLOAD_UPDATE:
#if !RSG_FINAL
		if (DebugSwitches::FakeDownloadingOnly)
		{

			CTest::FakeDownloadUpdate(CLauncherDlg::GetLastInstance());

		}
		else
#endif
		{
			//@@: range WORKERTHREAD_DOJOB_UPDATE_ME {
			LauncherApp::RunPatchProcess();
			//@@: } WORKERTHREAD_DOJOB_UPDATE_ME
		}
		break;

	case JOB_INSTALL_UPDATE:
		//@@: range WORKERTHREAD_DOJOB_INSTALL_UPDATE {
#if !RSG_FINAL
		CTest::FakeInstallUpdate(CLauncherDlg::GetLastInstance());
#endif
		break;
		//@@: } WORKERTHREAD_DOJOB_INSTALL_UPDATE

	case JOB_UPLOAD_REPORT:
#if !RSG_FINAL
		CTest::FakeUploadReport(CLauncherDlg::GetLastInstance());
#endif
		break;
#if RSG_PRELOADER_LAUNCHER
	case JOB_CHECK_FOR_RELEASE_DATE:
		CReleaseDateCheck::CheckSynchronous();
		break;
#endif
	} 
	//@@: } WORKERTHREAD_DOJOB_JOBS

	DEBUGF1("Finished job: %s", GetJobTypeName(jobType));
	m_currentJob = JOB_NONE;
}

void WorkerThread::CancelJob(const JobType& jobType)
{
	DEBUGF1("Cancelling job: %s", GetJobTypeName(jobType));

	
	switch (jobType)
	{
	default:
		ERRORF("Invalid job type!");
		break;
	//@@: range WORKERTHREAD_CANCELJOB {
	case JOB_DOWNLOAD_ONLINE_CONFIG:
		COnlineConfigOption::CancelDownloadingOptions();
		break;

	case JOB_CHECK_FOR_SC_UPDATES:
	case JOB_CHECK_FOR_GAME_UPDATES:
		LauncherApp::CancelUpdateCheck();
		break;
	case JOB_DOWNLOAD_UPDATE:
		//@@: location WORKERTHREAD_CANCELJOB_CANCEL_PATCH
		LauncherApp::CancelPatchProcess();
		break;

	case JOB_INSTALL_UPDATE:
	case JOB_UPLOAD_REPORT:
		break;
#if RSG_IN_PLACE_DOWNLOAD
	case JOB_CHECK_FOR_IN_PLACE_DOWNLOAD:
	case JOB_DOWNLOAD_IN_PLACE:
	case JOB_VERIFY_INTEGRITY:
		InPlaceDownloader::Cancel();
		break;
#endif

	//@@: } WORKERTHREAD_CANCELJOB

	}
}