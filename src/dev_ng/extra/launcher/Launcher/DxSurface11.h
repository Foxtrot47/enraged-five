#pragma once

#include <afxwin.h>
#include <d3d11.h>

#include "DxSurface.h"

class DxSurface11 : public DxSurface
{
	DECLARE_DYNAMIC(DxSurface11)

public:
	DxSurface11();
	~DxSurface11();


	void DrawTest();
	void NullDraw();

	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	virtual HRESULT CreateDevice();
	virtual void DestroyDevice();
	virtual IUnknown* GetDxContext();
	virtual void* GetPresentParams();
	virtual bool Show(bool show);
	virtual void Render();


private:
	HWND m_window, m_parentWindow;

	ID3D11Device* m_device;
	ID3D11DeviceContext* m_deviceContext;
	IDXGISwapChain* m_swapChain;
	ID3D11RenderTargetView* m_renderTargetView;
	DXGI_SWAP_CHAIN_DESC m_desc;

	bool m_painting;
	bool m_bShown;
	bool m_bLostDevice;

protected:
	afx_msg void OnPaint();
	BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	DECLARE_MESSAGE_MAP()
};