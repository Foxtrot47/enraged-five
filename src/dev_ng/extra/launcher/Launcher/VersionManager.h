#pragma once

#include "IPatchCheck.h"

#define RSG_COMBINED_VERSIONING 1

#if RSG_COMBINED_VERSIONING

#include <vector>

class VersionManager
{
public:
	static bool CheckVersionsSynchronous();

	static bool GetBootstrapPatch(Patch& out_patch);
	static bool GetLauncherPatch(Patch& out_patch);
	static bool GetSocialClubPatch(Patch& out_patch);
	static bool GetGamePatch(Patch& out_patch);
	static bool GetMtlPatch(Patch& out_patch);
	static bool GetDigitalDataManifestUrls(std::vector<Patch>& out_manifests);

	static bool GetReleaseDateSystemMillis(u64& out_systemMillis);

	static void Reset();

private:
	static bool sm_checkedVersions;
	static Patch sm_bootstrap, sm_launcher, sm_socialclub, sm_game, sm_mtl;
	static std::vector<Patch> sm_manifests;

	static Version GetBuildVersion();
	static s64 FiletimeToSecondsSinceEpoch(FILETIME* ft);
	static void ReadElement(TiXmlElement* build, char* element, Patch& patch);
	static void ReadMultipleElements(TiXmlElement* build, char* element, std::vector<Patch>& patches);

	static bool ReadVersion(const char* versionString, Version& version);

	static u64 sm_releaseDateSystemMillis;
};

#endif