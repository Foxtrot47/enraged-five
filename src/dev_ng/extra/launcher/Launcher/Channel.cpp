#include "stdafx.h"

#include "Application.h"
#include "Channel.h"
#include "Config.h"

#include <stdlib.h>
#include <string>

#include "Util.h"

std::stringstream CChannel::sm_logMemoryStream;
std::ofstream CChannel::sm_logfileStream;

std::ostream* CChannel::sm_logOutputStream = &CChannel::sm_logMemoryStream;

CRITICAL_SECTION CChannel::sm_criticalSection;
bool CChannel::sm_bInitialised = false;

// Default severity levels

CChannel::Severity CChannel::sm_fileSeverity = CChannel::SEVERITY_LEVEL_DEBUG3;
CChannel::Severity CChannel::sm_ttySeverity = CChannel::SEVERITY_LEVEL_DEBUG3;

void CChannel::CheckInitialised()
{
	if (!sm_bInitialised)
	{
		InitializeCriticalSection(&sm_criticalSection);
		sm_bInitialised = true;
	}
}

void CChannel::OpenLogFile(const wchar_t* name)
{
	CheckInitialised();

	PWSTR pszPath = NULL;
	HRESULT hr = SHGetKnownFolderPath(FOLDERID_Documents, 0, NULL, &pszPath);

	if (SUCCEEDED(hr))
	{
		std::wstring path(pszPath);

		//@@: location CCHANNEL_OPENLOGFILE_GET_SUBFOLDER
		path += Constants::DocumentsSubfolderCompanyName;
		CUtil::CreateDirectoryIfDoesntExist(path);

		path += Constants::DocumentsSubfolderProductName;
		CUtil::CreateDirectoryIfDoesntExist(path);

		path += L"\\";
		path += name;

		sm_logfileStream.open(path.c_str(), std::ofstream::binary | std::ofstream::out);

		if (sm_logfileStream.good())
		{
			sm_logOutputStream = &sm_logfileStream;

			std::string str = sm_logMemoryStream.str();

			if (!str.empty())
			{
				sm_logfileStream << str;
				sm_logfileStream.flush();
			}

			sm_logMemoryStream.str("");
			sm_logMemoryStream.clear();
		}
	}
	//@@: location CCHANNEL_OPENLOGFILE_COTASKMEMFREE
	CoTaskMemFree(pszPath);
#if !RSG_FINAL
	CAdminRights::AdminRightState adminState = CAdminRights::CheckAdminRights();

	if (adminState == CAdminRights::ARS_ADMIN)
		Displayf("Running as admin.");
	else if (adminState == CAdminRights::ARS_STANDARD_USER)
		Displayf("Running as standard user.");
	else if (adminState == CAdminRights::ARS_STANDARD_USER_NO_UAC)
		Displayf("Running as standard user (no UAC).");
	else
		Errorf("User state unknown!");
#endif
}

void CChannel::CloseLogFile()
{
	
	//@@: location CCHANNEL_CLOSELOGFILE
	CheckInitialised();

	sm_bInitialised = false;

	EnterCriticalSection(&sm_criticalSection);

	if (sm_logfileStream.is_open())
	{
		sm_logfileStream.flush();
		sm_logfileStream.close();
	}

	LeaveCriticalSection(&sm_criticalSection);

	DeleteCriticalSection(&sm_criticalSection);
	
}

void CChannel::SetGlobalLogLevels(Severity file, Severity tty)
{
	//@@: location CCHANNEL_SETGLOBALLOGLEVELS
	sm_fileSeverity = file;
	sm_ttySeverity = tty;
}

void CChannel::Errorf(const char* fmt, ...)
{
	//@@: location CCHANNEL_ERRORF
	va_list args;
	va_start(args, fmt);
	vLogf(SEVERITY_LEVEL_ERROR, fmt, args);
	va_end(args);
}
void CChannel::Warningf(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vLogf(SEVERITY_LEVEL_WARNING, fmt, args);
	va_end(args);
}
void CChannel::Displayf(const char* fmt, ...)
{
	//@@: location CCHANNEL_DISPLAYF
	va_list args;
	va_start(args, fmt);
	//@@: location CCHANNEL_DISPLAYF_TWO
	vLogf(SEVERITY_LEVEL_DISPLAY, fmt, args);
	va_end(args);
}

void CChannel::Debugf1(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vLogf(SEVERITY_LEVEL_DEBUG1, fmt, args);
	va_end(args);
}
void CChannel::Debugf2(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vLogf(SEVERITY_LEVEL_DEBUG2, fmt, args);
	va_end(args);
}
void CChannel::Debugf3(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vLogf(SEVERITY_LEVEL_DEBUG3, fmt, args);
	va_end(args);
}

void CChannel::Logf(Severity severity, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vLogf(severity, fmt, args);
	va_end(args);
}

void CChannel::vLogf(Severity severity, const char* fmt, va_list args)
{
	const size_t buffer_size = 1024;

	CheckInitialised();

	if (!sm_bInitialised || (severity > sm_fileSeverity && severity > sm_ttySeverity))
	{
		return;
	}

	// No need to synchronize string-length counting
	size_t count = _vscprintf(fmt, args);

	bool newlineTerminated = count >= 2 && (fmt[count-1] == '\n') && (fmt[count-2] == '\r');

	EnterCriticalSection(&sm_criticalSection);

	// Allocate buffer if needed
	char* buffer = NULL;
	char stack_buffer[buffer_size+1];

	if (count < buffer_size)
	{
		buffer = stack_buffer;
	}
	else
	{
		buffer = new char[count+1];
	}

	// Copy to buffer
	vsprintf_s(buffer, count+1, fmt, args);

	// Get preamble
	const char* preamble;
	switch (severity)
	{
		default:
		case SEVERITY_LEVEL_ERROR:   preamble = "[ ERROR ] "; break;
		case SEVERITY_LEVEL_WARNING: preamble = "[WARNING] "; break;
		case SEVERITY_LEVEL_DISPLAY: preamble = "[DISPLAY] "; break;
#if !RSG_FINAL || RSG_FINAL_DEBUG
		case SEVERITY_LEVEL_DEBUG1:  preamble = "[DEBUG 1] "; break;
		case SEVERITY_LEVEL_DEBUG2:  preamble = "[DEBUG 2] "; break;
		case SEVERITY_LEVEL_DEBUG3:  preamble = "[DEBUG 3] "; break;
#endif
	}

	// Get the current time
	SYSTEMTIME sysTime;
	GetLocalTime(&sysTime);
	char timeString[256] = {0};
	sprintf_s(timeString, 256, "[%4d-%02d-%02d %02d:%02d:%02d.%03d] ", sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute, sysTime.wSecond, sysTime.wMilliseconds);
	
	
	// Write out to file
	if (severity <= sm_fileSeverity)
	{
		sm_logOutputStream->write(timeString, strlen(timeString));
		sm_logOutputStream->write(preamble, strlen(preamble));
		sm_logOutputStream->write(buffer, strlen(buffer));

		if (!newlineTerminated)
		{
			sm_logOutputStream->write("\r\n", 2);
		}

		sm_logOutputStream->flush();
	}
	
	// Write out to TTY
	if (severity <= sm_ttySeverity)
	{
		if (newlineTerminated)
		{
			TRACE("%s%s%s", timeString, preamble, buffer);
		}
		else
		{
			TRACE("%s%s%s\n", timeString, preamble, buffer);
		}
	}

	// Free buffer if needed
	if (count >= buffer_size)
	{
		delete [] buffer;
	}

	LeaveCriticalSection(&sm_criticalSection);
}
