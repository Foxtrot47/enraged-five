// Launcher.cpp : Defines the class behaviors for the application.
//

#include <stdlib.h>
#include <crtdbg.h>

#include "stdafx.h"
#include "Launcher.h"
#include "LauncherDlg.h"
#include "Globals.h"
#include "Config.h"
#include "Steam.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



#include "Localisation.h"
#include "Application.h"
#include "GameCommunication.h"
#include "Test.h"
#include "Channel.h"
#include "TamperSource.h"
#include "RADTelemetrySupport.h"
#include "RgscTelemetryManager.h"

#include "p4version.h"

// CLauncherApp

BEGIN_MESSAGE_MAP(CLauncherApp, CWinApp)
END_MESSAGE_MAP()


// CLauncherApp construction

CLauncherApp::CLauncherApp()
{
#if !RSG_PRELOADER_LAUNCHER && ENABLE_TAMPER_ACTIONS
	//@@: range CLAUNCHERAPP_NOP {

	InitTamperCallsToNop(false);
	//@@: } CLAUNCHERAPP_NOP 
#endif
}

CLauncherApp::~CLauncherApp()
{
}

// The one and only CLauncherApp object

CLauncherApp theApp;

const GUID CDECL BASED_CODE _tlid =
		{ 0xBCDE64B9, 0x6A51, 0x4D66, { 0x86, 0x32, 0x1B, 0x5, 0xF0, 0xF0, 0xD1, 0x78 } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;

const TCHAR* g_windowClassName = _T("LauncherWindowClass");

int MyAllocHook(int allocType, void* /*userData*/, size_t size, int blockType, long requestNumber, const u8* /*filename*/, int /*lineNumber*/)
{
	if (blockType == _CRT_BLOCK)
		return TRUE;

	if (allocType == _HOOK_ALLOC && requestNumber > 21000 && size == 48)
	{
		//DEBUGF3("{%u} Allocated at %s : %d", requestNumber, filename, lineNumber);
	}

	return TRUE;
}

// CLauncherApp initialization

BOOL CLauncherApp::InitInstance()
{
#if !RSG_PRELOADER_LAUNCHER
    //@@: location CLAUNCHERAPP_INITINSTANCE_CALL_RED_HERRING
	RedHerringEntitlement();
#endif

	// Handle commandline parameters
	CCommandlineArgument::ProcessCommandline();

#if !RSG_FINAL
	if (Globals::autotest_launcher_exitimmediately)
	{
		if (CConfigOption::LoadOptions(IDR_CONFIG_XML))
		{
			CChannel::OpenLogFile(L"launcher.log");
			DISPLAYF("Exiting immediately due to -autotest_launcher_exitimmediately switch.");
			CChannel::CloseLogFile();
		}
		return FALSE;
	}
#endif

	// Check for other instances
	HWND hWnd = FindWindow(g_windowClassName, NULL);

	if (hWnd)
	{
		if (Globals::uac
#if RSG_PRELOADER_LAUNCHER
			 || Globals::releasedaterestart
#else
			|| Globals::generalrestart
#endif
#if !RSG_FINAL
			|| Globals::autotest_launcher_errors
#endif
			)
		{
			do
			{
				Sleep(10);
				hWnd = FindWindow(g_windowClassName, NULL);
			}
			while (hWnd);
		}
		else
		{
			ShowWindow(hWnd, SW_SHOWNORMAL);
			SetForegroundWindow(hWnd);
			PostMessage(hWnd, CLauncherDlg::eMultipleLaunchesAttempted, NULL, NULL);

			CConfigOption::LoadOptions(IDR_CONFIG_XML);
			CLocalisation::Instance().Init();

			std::wstring runningMessage = CLocalisation::Instance().GetWString(IDS_GAME_RUNNING);
			std::wstring errorTitle = CLocalisation::Instance().GetWString(IDS_HEADING_ERROR);
			MessageBox(GetForegroundWindow(), runningMessage.c_str(), errorTitle.c_str(), MB_OK);

			return FALSE;
		}
	}

#if RSG_FINAL && !RSG_FINAL_DEBUG
	CChannel::SetGlobalLogLevels(CChannel::SEVERITY_LEVEL_DISPLAY, CChannel::SEVERITY_LEVEL_DISPLAY);
#else
	CChannel::SetGlobalLogLevels(CChannel::SEVERITY_LEVEL_DEBUG3, CChannel::SEVERITY_LEVEL_DEBUG3);
#endif

	//_CrtSetAllocHook(MyAllocHook);

	InitDialog();

	CChannel::CloseLogFile();

	return FALSE;
}

void CLauncherApp::InitDialog()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	//@@: location CLAUNCHERAPP_INITDIALOG_SIZEOFCTRLS
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	// From http://www.codeproject.com/Articles/196354/Provide-Your-Custom-Class-Name-to-your-MFC-Applica
	WNDCLASS wndcls;
	GetClassInfo(NULL,MAKEINTRESOURCE(32770),&wndcls);

	wndcls.lpszClassName = g_windowClassName;
	wndcls.style |= CS_DROPSHADOW;

	// Just register the class
	//@@: location CLAUNCHERAPP_INITDIALOG_REGISTER_CLASS
	if (!RegisterClass(&wndcls))
	{
		AfxMessageBox(_T("Failed to register window class"));
		return;
	}

	// Load options
	if (!CConfigOption::LoadOptions(IDR_CONFIG_XML))
	{
		AfxMessageBox(_T("Failed to load launcher_config.xml!"));
		return;
	}

	CChannel::OpenLogFile(L"launcher.log");

#if !RSG_FINAL
	DEBUGF1("P4 Version: %s", P4VERSION_STRING);
#endif

	// Localization Initialization
	CLocalisation::Instance().Init();

#if RSG_STEAM_LAUNCHER
	CSteam::EarlyInit();
#endif

#if !RSG_FINAL
	// TESTING
	CTest::LocalizationTest();
#endif
	//CTest::TestStringReplacements();

	CWinApp::InitInstance();

#if !RSG_FINAL
	if (DebugSwitches::DebugTestRegex)
	{
		CTest::TestRegex();
	}

	CTest::AutotestSetup();

	CTest::TestJson();
#endif

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return;
	}

	// Initialize OLE libraries
	//@@: location CLAUNCHERAPP_INITDIALOG_AFX_OLE_INIT
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return;
	}

	RAD_TELEMETRY_INIT();

	// Create and run the dialog
	CLauncherDlg dlg;

	dlg.CreateWindowNoResource(wndcls);

	dlg.DoModal();

	RAD_TELEMETRY_SHUTDOWN();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return;
}
