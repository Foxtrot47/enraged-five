#include "StdAfx.h"
#include "GamePatchCheck.h"
#include "Globals.h"
#include "Config.h"
#include "OnlineConfig.h"
#include "Util.h"
#include "seh.h"
#include "Localisation.h"
#include "Channel.h"
#include "VersionManager.h"

#include "../tinyxml/tinyxml.h"

GamePatchCheck::GamePatchCheck(void)
{
	CUtil::GetServerAndDocpathForEnv(m_server, m_documentPath, Constants::GameVersionPathProd);

	m_patchCheckType = PT_GAME;
}

GamePatchCheck::~GamePatchCheck(void)
{
}

void GamePatchCheck::DownloadComplete(DownloadCompleteStatus status, std::string filename)
{
	if (status != DL_STATUS_COMPLETE)
	{
		// ThrowError();
		return;
	}
}

void GamePatchCheck::DownloadError(const CError& /*error*/)
{
	m_bCheckError = true;
}

void GamePatchCheck::ParseXML()
{
	Patch patch;
	//@@: location GAMEPATCHCHECK_PARSEXML_GETGAMEPATCH
	if (VersionManager::GetGamePatch(patch))
	{
		//@@: range GAMEPATCHCHECK_PARSEXML_SET_XML {
		patch.DeleteOnCompletion = true;
		patch.ChunkedDownload = false;
		patch.UpdateType = UT_MANDATORY;
		patch.PatchType = PT_GAME;

		//@@: location GAMEPATCHCHECK_PARSEXML_PREPENDTEMPORARYFOLDERPATH
		CUtil::PrependTempoaryFolderPath(patch.LocalFilename, patch.LocalFilename);
		//@@: location GAMEPATCHCHECK_PARSEXML_CONVERTTONEEDINGTIMEBOMB
		ConvertToNeedingAccessControlledUrl(patch);

		//@@: location GAMEPATCHCHECK_PARSEXML_CLEAR_AND_PUSH_BACK
		m_patches.clear();
		m_patches.push_back(patch);
		DEBUGF1("Adding game patch (now %d): %s", m_patches.size(), patch.PatchURL.c_str());
		//@@: } GAMEPATCHCHECK_PARSEXML_SET_XML
	}
	//@@: location GAMEPATCHCHECK_PARSEXML_RETURN
	return;
}

Version GamePatchCheck::GetExecutableVersion()
{
	std::string path = GetGameExecutableFullPath();

	//@@: location GAMEPATCHCHECK_GETEXECUTABLEVERSION
	if (path.length() == 0)
		return Version();
	else
		return GetVersionForFile(path);
}

bool GamePatchCheck::DoesGameExecutableExist()
{
	std::string path = GetGameExecutableFullPath();

	if (path.length() == 0)
	{
		return false;
	}
	
	bool exists = false;
	GetVersionForFile(path, &exists);

	return exists;
}

std::string GamePatchCheck::GetGameExecutableFullPath()
{
	std::string path = GetGameExecutableFolder();

	if (path.length() == 0)
		return GetGameExecutableFilename();

	//@@: location GAMEPATCHCHECK_GETGAMEXECUTABLEFULLPATH_APPEND_FILENAME
	path.append("\\").append(GetGameExecutableFilename());

	return path;
}

std::string GamePatchCheck::GetGameExecutableFolder()
{
#if !RSG_FINAL
	if (Globals::useDevInstallPath)
	{
		return Constants::DevInstallDirectory;
	}
	else
	{
#endif
		// The Steam launcher runs the game by booting it from the same folder as the launcher.
		// There are no install folder registry keys set by the Steam SKU.
#if !RSG_STEAM_LAUNCHER
		//@@: range GAMEPATCHCHECK_GETGAMEEXECUTABLEFOLDER_GET_FROM_REGISTRY {
		std::wstring key32, key64, valName;
		//@@: location GAMEPATCHCHECK_GETGAMEEXECUTABLEFOLDER_GET_REG_KEYS
		CUtil::StdStringToStdWString(Constants::GameInstallPathRegistryKey32.Get(), key32);
		CUtil::StdStringToStdWString(Constants::GameInstallPathRegistryKey64.Get(), key64);
		CUtil::StdStringToStdWString(Constants::GameInstallPathRegistryValueName.Get(), valName);

		// First attempt to retrieve the game install folder from the registry
		std::string registryValue = GetValueFromRegistry(key32.c_str(), key64.c_str(), valName.c_str());

		// If the key value is valid, return it.
		// Otherwise, If the registry key cannot be found, default to the launcher executable directory
		if (registryValue.length() > 0)
		{
			return registryValue;
		}
		else
		{
#endif
			rtry
			{
				// get the path to the launcher executable via GetModuleFileNameA
				wchar_t fullPath[MAX_PATH] = {0};
				rverify(GetModuleFileNameW( NULL, fullPath, MAX_PATH ), catchall, );

				// extract the folder path by identifying the last slash in the path
				std::wstring folderPath = fullPath;
				const size_t slashIndex = folderPath.rfind('\\');
				if (slashIndex != std::wstring::npos)
				{
					folderPath = folderPath.substr(0, slashIndex);
				}

				// return the folder path
				std::string ret;
				CUtil::WStringToStdString(folderPath, ret);
				return ret;
			}
			rcatchall
			{
			}
#if !RSG_STEAM_LAUNCHER
		}
#endif

		//@@: } GAMEPATCHCHECK_GETGAMEEXECUTABLEFOLDER_GET_FROM_REGISTRY

#if !RSG_FINAL
	}
#endif

	return "";
}

std::string GamePatchCheck::GetGameExecutableFilename()
{
	//@@: location GAMEPATCHCHECK_GETGAMEXECUTABLEFILENAME
#if !RSG_FINAL
	if (Globals::useDevInstallPath)
	{
		if (Globals::final)
		{
			return Constants::DevInstallFilenameFinal;
		}
		if (Globals::release)
		{
			return Constants::DevInstallFilenameRelease;
		}
		else if (Globals::bankrelease)
		{
			return Constants::DevInstallFilenameBankRelease;
		}
		else if (Globals::beta)
		{
			return Constants::DevInstallFilenameBeta;
		}
		else if (Globals::afrfriendlyd3d)
		{
			return std::string("AFR-FriendlyD3D.exe");
		}
		else
		{
			return Constants::DevInstallFilenameDefault;
		}
	}
	else
#endif //FINAL
		return Constants::FinalInstallFilename;
}
