#pragma once

#include "Util.h"

#include <string>
#include <vector>
#include <sstream>

// Base class for all config options
class CConfigOption
{
public:
	static bool LoadOptions(int resID);
	static const std::vector<CConfigOption*>& GetOptions();
	static const std::vector<std::string>& GetDependencyList();
public:
	virtual void Parse(const char* option) = 0;
	const char* GetName() const;
	virtual void GetValueAsString(std::string& out_string) const = 0;

protected:
	CConfigOption(const char* name);
	static std::vector<CConfigOption*>& GetOptionsNonConst();
	static std::vector<std::string>& GetDependencyListNonConst();

	const char* m_name;
	bool m_valueSet;
};


// Template for config options of different types
template<typename T>
class CConfigOptionType : public CConfigOption
{
public:
	CConfigOptionType(const char* name) : CConfigOption(name)
	{
	}

	virtual void Parse(const char* option)
	{
		// Handles most values of T
		std::stringstream ss;
		ss << option;
		ss >> m_value;
		m_valueSet = true;
	}

	const T& Get() const
	{
		return m_value;
	}

	operator const T() const
	{
		return m_value;
	}

	virtual void GetValueAsString(std::string& out_string) const
	{
		std::stringstream ss;
		ss << m_value;
		out_string.assign(ss.str());
	}

protected:
	T m_value;
};

// For bool values, we're expecting "true" or "false"
template<>
virtual void CConfigOptionType<bool>::Parse(const char* option)
{
	m_value = (_stricmp("true", option) == 0);
	m_valueSet = true;
}

template<>
virtual void CConfigOptionType<bool>::GetValueAsString(std::string& out_string) const
{
	out_string = (m_value ? "true" : "false");
}

// For string values, stringstream doesn't work as expected.
template<>
virtual void CConfigOptionType<std::string>::Parse(const char* option)
{
	m_value = option;
	m_valueSet = true;
}

template<>
virtual void CConfigOptionType<std::string>::GetValueAsString(std::string& out_string) const
{
	out_string = m_value;
}

// Wstring support
template<>
virtual void CConfigOptionType<std::wstring>::Parse(const char* option)
{
	const std::string s = option;
	CUtil::StdStringToStdWString(s, m_value);
	m_valueSet = true;
}

template<>
virtual void CConfigOptionType<std::wstring>::GetValueAsString(std::string& out_string) const
{
	CUtil::WStringToStdString(m_value, out_string);
}

// Macros for declaring/defining options
#ifdef DEFINE_CONFIG_OPTIONS
#define OPTION_DEFINITION(type,name) CConfigOptionType<type> name(#name)
#else
#define OPTION_DEFINITION(type,name) extern CConfigOptionType<type> name
#endif

#define INT_OPTION(name) OPTION_DEFINITION(int,name)
#define STRING_OPTION(name) OPTION_DEFINITION(std::string,name)
#define WSTRING_OPTION(name) OPTION_DEFINITION(std::wstring,name)
#define BOOL_OPTION(name) OPTION_DEFINITION(bool,name)


// The options themselves
namespace Constants
{

#if !RSG_FINAL

	STRING_OPTION(DevInstallDirectory); // "."; //"X:\\gta5\\build\\dev"
	STRING_OPTION(DevInstallFilenameBeta); //        "game_win64_beta.exe"
	STRING_OPTION(DevInstallFilenameBankRelease); // "game_win64_bankrelease.exe"
	STRING_OPTION(DevInstallFilenameRelease); //     "game_win64_release.exe"
	STRING_OPTION(DevInstallFilenameFinal); //       "game_win64_final.exe"
	STRING_OPTION(DevInstallFilenameDefault); //    DevInstallFilenameBankRelease

	BOOL_OPTION(CheckingCertificates); // false

	STRING_OPTION(CommandlineArgs); // "@args.txt"

#endif
	STRING_OPTION(LauncherName); // LAUNCHER_USER_AGENT DEBUGNAME ".exe"


	// Paths

	
	STRING_OPTION(ProductionPatchServer); // "http://patches.rockstargames.com"

	
	STRING_OPTION(TempFolder); // "Rockstar Games\\"


	STRING_OPTION(FinalInstallFilename); // "GTAV.exe"

	
	// Bootstrap
	STRING_OPTION(BootstrapVersionPathProd); // "/prod/gtav/prelauncher_version.xml"

	STRING_OPTION(BootstrapExeNameWithoutExtension); // "PlayGTAV"

	// Launcher
	STRING_OPTION(LauncherVersionPathProd); // "/prod/gtav/launcher_version.xml"

	STRING_OPTION(LauncherExeNameWithoutExtension); // "GTAVLauncher"

	STRING_OPTION(OnlineConfigPathProd); // "/prod/gtav/launcher_online_config.xml"

	STRING_OPTION(ReleaseDatePathProd); // "/prod/gtav/release_date.xml"

	STRING_OPTION(VersioningPathProd); // "/prod/gtav/versioning.xml"

	STRING_OPTION(MtlVersioningPathProd); // "/prod/gtav/mtl_versioning.xml"


	// Steam
	INT_OPTION(SteamAppId); // 271590

	// Social Club
	STRING_OPTION(RGSCVersionPathProd); // "/prod/socialclub/sc_installer_version.xml"

	// Game
	STRING_OPTION(GameVersionPathProd); // "/prod/gtav/gtav_version.xml"

	STRING_OPTION(GameInstallPathRegistryKey32); // "SOFTWARE\\Rockstar Games\\Grand Theft Auto V"
	STRING_OPTION(GameInstallPathRegistryKey64); // "SOFTWARE\\Wow6432Node\\Rockstar Games\\Grand Theft Auto V"
	STRING_OPTION(GameInstallPathRegistryValueName); // "InstallFolder"

	STRING_OPTION(GameDownloadLocationPathProd); // "/prod/gtav/gtav_download.xml"

	// DLC
	STRING_OPTION(DLCVersionPathProd); // "/prod/gtav/dlc_version.xml"

	
	// Folders
	WSTRING_OPTION(AppDataSubfolderCompanyName); // "\\Rockstar Games"
	WSTRING_OPTION(AppDataSubfolderProductName); // "\\GTA V"

	WSTRING_OPTION(DocumentsSubfolderCompanyName); // "\\Rockstar Games"
	WSTRING_OPTION(DocumentsSubfolderProductName); // "\\GTA V"


	// Title name for localisation
	STRING_OPTION(TitleName); // "GTA V"
	STRING_OPTION(TitleNameShort); // "GTA V"
	STRING_OPTION(TitleNameLong); // "Grand Theft Auto V"


	// Background image filename
	WSTRING_OPTION(BackgroundImageFilename);
	WSTRING_OPTION(SplashImageFilename);

	// Loading bar
	INT_OPTION(LoadingBarForegroundR);
	INT_OPTION(LoadingBarForegroundG);
	INT_OPTION(LoadingBarForegroundB);
	INT_OPTION(LoadingBarBackgroundR);
	INT_OPTION(LoadingBarBackgroundG);
	INT_OPTION(LoadingBarBackgroundB);

	// Text colour
	INT_OPTION(TextColourR);
	INT_OPTION(TextColourG);
	INT_OPTION(TextColourB);

	// Background colour
	INT_OPTION(BackgroundR);
	INT_OPTION(BackgroundG);
	INT_OPTION(BackgroundB);

	// Button text colour
	INT_OPTION(ButtonTextColourR);
	INT_OPTION(ButtonTextColourG);
	INT_OPTION(ButtonTextColourB);

	// Button background colour
	INT_OPTION(ButtonBackgroundR);
	INT_OPTION(ButtonBackgroundG);
	INT_OPTION(ButtonBackgroundB);

	// ROS settings
	INT_OPTION(RosScVersion); // 11
	INT_OPTION(RosTitleVersion); // 11
	STRING_OPTION(RosTitleName); // "gta5"
	STRING_OPTION(RosTitleDirectory); // "GTA V"
	//STRING_OPTION(RosTitleSecrets); // "C4pWJwWIKGUxcHd69eGl2AOwH2zrmzZAoQeHfQFcMelybd32QFw9s10px6k0o75XZeB5YsI9Q9TdeuRgdbvKsxc="

	// Named pipe
	WSTRING_OPTION(IpcPipeName); // "\\\\.\\pipe\\GTAVLauncher_Pipe"
};
