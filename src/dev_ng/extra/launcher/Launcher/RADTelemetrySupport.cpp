#include "stdafx.h"

#include "RADTelemetrySupport.h"

#if ENABLE_RAD_TELEMETRY_SUPPORT

#pragma comment(lib, "telemetry64.link.lib")

#include "Channel.h"
#include "Util.h"

HTELEMETRY CRadTelemetrySupport::sm_context = NULL;
bool CRadTelemetrySupport::sm_running = false;
bool CRadTelemetrySupport::sm_inited = false;
bool CRadTelemetrySupport::sm_shutdown = false;
u8* CRadTelemetrySupport::sm_memory = NULL;
volatile LONG CRadTelemetrySupport::sm_waitZonesEntered = 0;
CRITICAL_SECTION CRadTelemetrySupport::sm_critsec;
HANDLE CRadTelemetrySupport::sm_waitStartSema;
HANDLE CRadTelemetrySupport::sm_waitEndSema;

UINT CRadTelemetrySupport::TelemetryThreadEntry(void*)
{
	sm_shutdown = false;

	if (!sm_inited)
	{
		tmLoadTelemetry(TM_LOAD_CHECKED_LIBRARY);
		InitializeCriticalSection(&sm_critsec);
		sm_waitStartSema = CreateSemaphore(NULL, 0, 1, NULL);
		sm_waitEndSema = CreateSemaphore(NULL, 0, 1, NULL);
		sm_inited = true;
	}

	tmStartup();

	const TmU32 memSize = 4 * 1024 * 1024;
	sm_memory = new u8[memSize];

	tmInitializeContext(&sm_context, sm_memory, memSize);

	if (tmOpen(sm_context, "Launcher", __DATE__ " " __TIME__, "localhost", TMCT_TCP, TELEMETRY_DEFAULT_PORT, TMOF_DEFAULT, 1000) != TM_OK)
	{
		ERRORF("Couldn't connect to Telemetry.");
		sm_running = false;
	}

	CUtil::SetCurrentThreadName("Telemetry update thread");

	while (sm_running)
	{
		bool waited = false;

		if (WaitForSingleObject(sm_waitStartSema, 1000) == WAIT_OBJECT_0)
		{
			WaitForSingleObject(sm_waitEndSema, INFINITE);
		}

		tmTick(sm_context);
	}

	tmClose(sm_context);
	tmShutdownContext(sm_context);

	tmShutdown();

	delete[] sm_memory;

	sm_shutdown = true;
	
	return 0;
}

void CRadTelemetrySupport::InitTelemetry()
{
	if (!sm_running)
	{
		sm_running = true;

		AfxBeginThread(TelemetryThreadEntry, NULL);
	}
}

void CRadTelemetrySupport::ShutdownTelemetry()
{
	sm_running = false;

	while (!sm_shutdown)
	{
		Sleep(1);
	}
}

HTELEMETRY CRadTelemetrySupport::GetContext()
{
	return sm_context;
}


CRadTelemetrySupport::UiLockHelper::UiLockHelper(const char* name)
{
	if (sm_inited)
	{
		AUTOCRITSEC(sm_critsec);
		if (InterlockedIncrement(&sm_waitZonesEntered) == 1)
		{
			ReleaseSemaphore(sm_waitStartSema, 1, NULL);
		}
	}
	tmEnter(GetContext(), TMZF_NONE, name);
}

CRadTelemetrySupport::UiLockHelper::~UiLockHelper()
{
	tmLeave(GetContext());

	if (sm_inited)
	{
		AUTOCRITSEC(sm_critsec);
		if (InterlockedDecrement(&sm_waitZonesEntered) == 0)
		{
			ReleaseSemaphore(sm_waitEndSema, 1, NULL);
		}
	}
}

#endif
