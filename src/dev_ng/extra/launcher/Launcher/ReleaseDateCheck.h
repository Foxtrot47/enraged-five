#pragma once

#if RSG_PRELOADER_LAUNCHER
class CReleaseDateCheck
{
public:
	static void CheckSynchronous();
	static void StartCountdownThread();
	static void JoinCountdownThread();

	static bool IsReleaseDateInFuture() { return sm_releaseDateInFuture; }

private:
	static u64 sm_releaseDateSystemUptimeMillis;
	static u64 sm_lastReleaseDateCheckSystemUptimeMillis;
	static bool sm_releaseDateInFuture;

	static CWinThread* sm_countdownThread;
	static bool sm_countdownThreadRunning;
	static UINT CountdownThreadEntry(void* param);
};
#endif