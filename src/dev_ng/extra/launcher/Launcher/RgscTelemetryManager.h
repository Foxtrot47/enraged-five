#pragma once

#if RSG_LAUNCHER
#include "telemetry_interface.h"
#include "IPatchCheck.h"
#endif

#include "ProgressCalculator.h"
#include "RgscTelemetryEvent.h"

#include <string>
#include <vector>

class RgscTelemetryManager
{
public:
	enum Event
	{
		EVENT_BOOTSTRAP_START = 500,
		EVENT_BOOTSTRAP_START_CHECK_FOR_UPDATES,
		EVENT_BOOTSTRAP_START_CHECK_AGAINST_PATCH_LIST,
		EVENT_BOOTSTRAP_PATCH_TO_INSTALL,
		EVENT_BOOTSTRAP_START_DOWNLOAD,
		EVENT_BOOTSTRAP_START_INSTALL,
		EVENT_BOOTSTRAP_COMPLETED_UPDATE,

		EVENT_LAUNCHER_START = 600,
		EVENT_LAUNCHER_RGSC_CHECK_FOR_UPDATE,
		EVENT_LAUNCHER_RGSC_CHECK_AGAINST_PATCH_LIST,
		EVENT_LAUNCHER_RGSC_PATCH_TO_INSTALL,
		EVENT_LAUNCHER_RGSC_START_DOWNLOAD,
		EVENT_LAUNCHER_RGSC_START_INSTALL,
		EVENT_LAUNCHER_RGSC_COMPLETED_UPDATE,

		EVENT_LAUNCHER_SC_START = 700,

		EVENT_LAUNCHER_MACHINE_HASH_START = 800,

		EVENT_LAUNCHER_ENTITLEMENT_START = 900,
		EVENT_LAUNCHER_ENTITLEMENT_ALLOWED = 901,
		EVENT_LAUNCHER_ENTITLEMENT_CONNECTED = 902,

		EVENT_LAUNCHER_GAME_CHECK_FOR_UPDATE = 903,
		EVENT_LAUNCHER_GAME_CHECK_AGAINST_PATCH_LIST = 904,
		EVENT_LAUNCHER_GAME_PATCH_TO_INSTALL = 905,
		EVENT_LAUNCHER_GAME_START_DOWNLOAD = 906,
		EVENT_LAUNCHER_GAME_START_INSTALL = 907,
		EVENT_LAUNCHER_GAME_COMPLETED_UPDATE = 908,

		EVENT_LAUNCHER_DIP_GET_MANIFEST = 909,
		EVENT_LAUNCHER_DIP_CHECK_AGAINST_MANIFEST = 910,
		EVENT_LAUNCHER_DIP_DATA_TO_INSTALL = 911,
		EVENT_LAUNCHER_DIP_START_DOWNLOAD = 912,
		EVENT_LAUNCHER_DIP_COMPLETED_UPDATE = 913,

		EVENT_LAUNCHER_GAME_CHECK_DEPENDENCIES = 914,
		EVENT_LAUNCHER_GAME_DEPENDENCIES_SATISFIED = 915,
		EVENT_LAUNCHER_GAME_START_LAUNCH = 916,

		EVENT_LAUNCHER_ACTIVATION_DISPLAY_PROMPT,
		EVENT_LAUNCHER_ACTIVATION_CHECK,
		EVENT_LAUNCHER_ACTIVATION_QUIT,

		EVENT_LAUNCHER_DOWNLOAD_STATS = 1000,
		EVENT_LAUNCHER_ERROR_MESSAGE = 1001,
		EVENT_LAUNCHER_EXITED = 1002,
		EVENT_LAUNCHER_DEBUG = 1003,
		EVENT_LAUNCHER_DOWNLOADER_ERROR = 1004,
		EVENT_LAUNCHER_SC_LOAD_ERROR = 1005,
		EVENT_LAUNCHER_GAME_EXIT_CODE = 1006,

		EVENT_LAUNCHER_GEOLOCATION = 1007,
		// EVENT_LAUNCHER_IP = 1008, - adding this commented out to prevent the number being used or assigned to a different event - it was proposed over email but rejected
		EVENT_LAUNCHER_INTERMEDIATE_DOWNLOAD_STATS = 1009,

		EVENT_LAUNCHER_SYSTEM_INFO = 1100,

		EVENT_LAUNCHER_MTL_BETA_CHECK_SUCCEEDED = 1200,
		EVENT_LAUNCHER_MTL_BETA_CHECK_FAILED = 1201,
		EVENT_LAUNCHER_MTL_BETA_UPGRADE = 1202,

		EVENT_LAUNCHER_MTL_ASKED_TO_UPGRADE = 1203,
		EVENT_LAUNCHER_MTL_ACCEPTED_UPGRADE = 1204,
		EVENT_LAUNCHER_MTL_CANCELLED_UPGRADE = 1205,
		EVENT_LAUNCHER_MTL_ALREADY_INSTALLED = 1206,
		EVENT_LAUNCHER_MTL_DOWNLOADING = 1207,
		EVENT_LAUNCHER_MTL_STARTING_INSTALL = 1208,
		

		EVENT_MTL_FIRST_RUN = 1300,


		TOTAL_EVENT_COUNT = 56,
	};

	struct DownloadStats
	{
		u64 bytesThisSession;
		u64 sessionDuration;
		u32 connectionsOpened;
		u32 connectionsErrored;
		u32 verificationsFailed;
		ProgressCalculator::ProgressType type;
	};

	static void WriteEvent(Event event);
	static void WriteEventInt(Event event, int param);
	static void WriteEventGeolocation();
	static void WriteEventDownloadStats(Event event, DownloadStats stats);
	static void WriteEventDebugMetrics(Event event, std::vector<const RgscTelemetryEvent*> debugEvents);
	static void WriteEventFileDownload(const Patch& patch);
	

#if RSG_LAUNCHER
	static bool ReadBinaryAndConvertToXmlEvents();
	static bool TransmitXmlEvents(rgsc::ITelemetryV4* telemeteryInterface);

	static void SetVersions(const Version& versionTxt, const Version& gameVersion, const Version& scVersion, const Version& launcherVersion, bool dev);

	static void SetCountryCode(const std::string& countryCode);
	static void SetIpAddress(const std::string& ipAddress);
#endif

	static void PopulateTelemetryDownloadStatsFromProgressCalculator(const ProgressCalculator& progCalc, RgscTelemetryManager::DownloadStats& out_telemetryDownloadStats);

	static void Initialize(s64 rockstarId);
	static void Shutdown();

#if RSG_LAUNCHER
	static void SaveAllEventsAsXml();
	static void LoadAllEventsFromXml();
	static void SaveAllEventsAsXml(const std::wstring& filepath);
	static void LoadAllEventsFromXml(const std::wstring& filepath);
#endif

private:

	enum TelemetryFile
	{
		TF_BOOTSTRAP,
		TF_LAUNCHER,
		TF_TEMP,
		TF_XML_EVENTS,
	};

	enum EventType
	{
		NO_PARAM     = 1,
		INT_PARAM    = 2,
		STRING_PARAM = 3,

		// Same as above, but now with version info
		VERSIONED_NO_PARAM     = 4,
		VERSIONED_INT_PARAM    = 5,
		VERSIONED_STRING_PARAM = 6,

		// More
		DOWNLOAD_STATS_PARAM = 7,
		VERSIONED_DOWNLOAD_STATS_PARAM = 8,
	};

	enum LauncherFlags
	{
		LF_BUILD_UNKNOWN = 0x0,
		LF_BUILD_DEBUG =   0x1,
		LF_BUILD_RELEASE = 0x2,
		LF_BUILD_FINAL =   0x3,

		LF_FLAVOUR_UNKNOWN =    0x0,
		LF_FLAVOUR_PRELOADER =  0x1,
		LF_FLAVOUR_STEAM =      0x2,
		LF_FLAVOUR_FULL =       0x3,

		LF_ENVIRONMENT_DEV =  0x0,
		LF_ENVIRONMENT_PROD = 0x1,
	};

	static bool GetTelemFilePath(std::wstring& out_filepath, TelemetryFile whichFile);


#pragma pack(push, 1)
private:
	struct EventHeader
	{
		u64 timestamp;
		u16 event;
		u8 eventType;
	};

#if RSG_LAUNCHER
	struct VersionPacked
	{
		u16 versionMajor;
		u16 versionMinor;
		u16 versionBuild;
		u16 versionRevision;

		u16 gameMajor;
		u16 gameMinor;
		u16 gameBuild;
		u16 gameRevision;

		u16 scdllMajor;
		u16 scdllMinor;
		u16 scdllBuild;
		u16 scdllRevision;

		u16 launcherMajor;
		u16 launcherMinor;
		u16 launcherBuild;
		u16 launcherRevision;

		union
		{
			u8 flagsAsU8;
			struct 
			{
				u8 build : 2;
				u8 flavour : 2;
				bool env : 1;
			};
		} launcherFlags;
	};
#endif
#pragma pack(pop)

	static CRITICAL_SECTION& GetCriticalSection();

	struct EventWithData
	{
		EventWithData() : data(NULL) {}
		EventWithData(EventHeader& hdr, char* dat) : header(hdr), data(dat) {}
		EventHeader header;
#if RSG_LAUNCHER
		VersionPacked versioning;
#endif
		char* data;
	};

	static bool sm_waitingForFiles;

#if RSG_LAUNCHER
	static bool sm_versionSet;
	static VersionPacked sm_currentVersion;
	static std::string sm_rockstarIdUsername;
#endif

	static std::vector<RgscTelemetryEvent*> sm_eventsAsXml;

	static std::string sm_countryCode, sm_ipAddress;

public:
	static void AppendJsonInt(std::string& out_json, const char* name, s64 i);
	static void AppendJsonString(std::string& out_json, const char* name, const char* str);

private:
	static void AppendJsonArrayOrStruct(std::string& out_json, const char* name, const char* structureIncludingBrackets);
#if RSG_LAUNCHER
	static void AppendXmlEventVersionInfo(RgscTelemetryEvent* event, const VersionPacked& versioning);
	static void AppendXmlDownloadStats(RgscTelemetryEvent* event, const DownloadStats& ver);
	static void AppendXmlFilename(RgscTelemetryEvent* event, const Patch& ver);
#endif

	static bool EventTypeHasVersionInfo(EventType type);

	static const char* GetEventsXmlRootNodeName();
	static void LoadAllEventsFromXmlDocument(const TiXmlDocument& doc);

	const static u32 CRYPT_KEY_LENGTH = 32, CRYPT_IV_LENGTH = 16;
	static bool GenerateTelemetryEncryptionKey(u8 (&out_key)[CRYPT_KEY_LENGTH]);
	static void EncryptEventsXmlInPlace(std::string& inout_eventsXml);
	static void DecryptEventsXmlInPlace(std::string& inout_eventsXml);

	static void AppendXmlEventTimestamp(RgscTelemetryEvent* event, u64 timestamp);
	static RgscTelemetryEvent* AppendNewXmlEventOfType(Event event);

	static bool IsEventNumberValid(Event event);
};