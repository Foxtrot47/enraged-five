#ifndef _LAUNCHER_TEST_H_
#define _LAUNCHER_TEST_H_

class CLauncherDlg;

#if RSG_LAUNCHER && !RSG_FINAL

#define AUTOTEST_FAIL CTest::AutotestFail(__FILE__,__LINE__)
#define AUTOTEST_RESULT(...) CTest::AutotestErrorResult(__VA_ARGS__)

class CTest
{
public:
	static void TestRockstarDownloader();
	static void TestSimpleDownloader();
	static bool TestXML();
	static void TestJson();
	static void TestFakeDownloadUI();
	static void TestRegex();
	static void TestStringReplacements();

	static void FakeDownloadUpdate(CLauncherDlg* dlg);
	static void FakeInstallUpdate(CLauncherDlg* dlg);
	static void FakeUploadReport(CLauncherDlg* dlg);
	static void LocalizationTest();

	static void AutotestSetup();
	static void AutotestDigitalUpdate();
	static void AutotestGamePatch();

	static bool AutotestFail(const char* file, int line);

	static void ScanMemoryForPatternWhenSignalled(const char* pattern);
	static void ScanMemoryForPattern(const char* pattern);

	enum ErrorResultType
	{
		SHOWN_ERROR_SCREEN,
		RETRIED,
		NOT_AN_ERROR,
	};
	static void AutotestErrorResult(ErrorResultType result, int parameter = 0);
};

#else // RSG_LAUNCHER && RSG_FINAL

#define AUTOTEST_FAIL false
#define AUTOTEST_RESULT(...)

#endif

#endif
