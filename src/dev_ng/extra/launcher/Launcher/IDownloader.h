#pragma once

#include <string>
#include <vector>

#include "IPatchCheck.h"
#include "Error.h"

class IDownloader
{
public:

	enum DownloadEvent
	{
		DLE_CONNECTION_OPENED,
		DLE_CONNECTION_ERROR,
		DLE_VERIFICATION_FAILURE,
		DLE_ENDING,
	};

	struct DownloadListener
	{
		virtual void DownloadStarting(Patch& /*patch*/) {}
		virtual void DownloadComplete(const Patch& patch, DownloadCompleteStatus status) = 0;
		virtual void DownloadStatus(const Patch& patch, DownloadCompleteStatus status, u64 totalBytesComplete, u64 totalBytesInFile, u64 bytesJustDownloaded) = 0;
		virtual void DownloadError(const Patch& patch, const CError& error) = 0;

		virtual void DownloadEvent(Patch& /*patch*/, DownloadEvent /*event*/) {}
	};

	IDownloader(void);
	virtual ~IDownloader(void);

	Patch		CurrentPatch;

	virtual void Init()		= 0;
	virtual void StartSynchronously()	= 0;
	virtual void SetPaused(bool paused)	= 0;
	virtual void Stop()		= 0;

	virtual void SetCurrentFile(std::string downloadUrl, std::string localFilePath, bool updateProgress, bool requiresRestart) = 0;
	virtual void SetCurrentPatch(Patch& patch) = 0;
	void SetMaxRetries(int maxRetries) { m_maxRetries = maxRetries; }

	virtual int GetProgress() = 0;

	void SetListener(DownloadListener* listener);
	bool HasErrorOccurred();
	const CError& GetError();

protected:
	DownloadListener* m_listener;
	CError m_error;

	static const int scm_defaultMaxRetries = 4;
	int m_maxRetries;

};
