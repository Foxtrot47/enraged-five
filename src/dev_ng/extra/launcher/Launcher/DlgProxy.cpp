// DlgProxy.cpp : implementation file
//

#include "stdafx.h"
#include "Launcher.h"
#include "DlgProxy.h"
#include "LauncherDlg.h"

#ifdef _DEBUG
//#define new DEBUG_NEW
#endif


// CLauncherDlgAutoProxy

IMPLEMENT_DYNCREATE(CLauncherDlgAutoProxy, CCmdTarget)

CLauncherDlgAutoProxy::CLauncherDlgAutoProxy()
{
	EnableAutomation();
	
	// To keep the application running as long as an automation 
	//	object is active, the constructor calls AfxOleLockApp.
	AfxOleLockApp();

	// Get access to the dialog through the application's
	//  main window pointer.  Set the proxy's internal pointer
	//  to point to the dialog, and set the dialog's back pointer to
	//  this proxy.
	ASSERT_VALID(AfxGetApp()->m_pMainWnd);
	if (AfxGetApp()->m_pMainWnd)
	{
		ASSERT_KINDOF(CLauncherDlg, AfxGetApp()->m_pMainWnd);
		if (AfxGetApp()->m_pMainWnd->IsKindOf(RUNTIME_CLASS(CLauncherDlg)))
		{
			m_pDialog = reinterpret_cast<CLauncherDlg*>(AfxGetApp()->m_pMainWnd);
			m_pDialog->m_pAutoProxy = this;
		}
	}
}

CLauncherDlgAutoProxy::~CLauncherDlgAutoProxy()
{
	// To terminate the application when all objects created with
	// 	with automation, the destructor calls AfxOleUnlockApp.
	//  Among other things, this will destroy the main dialog
	if (m_pDialog != NULL)
		m_pDialog->m_pAutoProxy = NULL;
	AfxOleUnlockApp();
}

void CLauncherDlgAutoProxy::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CLauncherDlgAutoProxy, CCmdTarget)
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CLauncherDlgAutoProxy, CCmdTarget)
END_DISPATCH_MAP()

// Note: we add support for IID_ILauncher to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .IDL file.

// {B22FE848-5751-4303-BCEC-BE823043795A}
static const IID IID_ILauncher =
{ 0xB22FE848, 0x5751, 0x4303, { 0xBC, 0xEC, 0xBE, 0x82, 0x30, 0x43, 0x79, 0x5A } };

BEGIN_INTERFACE_MAP(CLauncherDlgAutoProxy, CCmdTarget)
	INTERFACE_PART(CLauncherDlgAutoProxy, IID_ILauncher, Dispatch)
END_INTERFACE_MAP()

// The IMPLEMENT_OLECREATE2 macro is defined in StdAfx.h of this project
// {43E9FE6F-AB8D-43F1-9D10-11649444AA45}
IMPLEMENT_OLECREATE2(CLauncherDlgAutoProxy, "Launcher.Application", 0x43e9fe6f, 0xab8d, 0x43f1, 0x9d, 0x10, 0x11, 0x64, 0x94, 0x44, 0xaa, 0x45)


// CLauncherDlgAutoProxy message handlers
