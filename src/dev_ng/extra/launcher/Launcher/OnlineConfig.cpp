#include "stdafx.h"

#define DEFINE_ONLINE_CONFIG_OPTIONS
#include "OnlineConfig.h"

#include <map>
#include <sstream>

#include "Channel.h"
#include "Config.h"
#include "Fsm.h"
#include "Globals.h"
#include "Resource.h"
#if RSG_LAUNCHER
#include "LauncherDlg.h"
#endif
#include "RockstarDownloader.h"
#include "Util.h"
#include "WhiteList.h"

#include "../tinyxml/tinyxml.h"


static CRITICAL_SECTION& GetOnlineConfigCritSec()
{
	static CRITICAL_SECTION s_critsec;
	static bool s_initialized = false;

	if (!s_initialized)
	{
		s_initialized = true;
		InitializeCriticalSection(&s_critsec);
	}

	return s_critsec;
}

static RockstarDownloader* g_OnlineConfigDownloader;
static void SetOnlineConfigRockstarDownloader(RockstarDownloader* downloader)
{
	AUTOCRITSEC(GetOnlineConfigCritSec());

	g_OnlineConfigDownloader = downloader;
}

COnlineConfigOption::COnlineConfigOption(const char* name, int minimum, int default_value, int maximum)
	: m_name(name), m_minimum(minimum), m_value(default_value), m_maximum(maximum)
{
	GetOptionsNonConst().push_back(this);
}

COnlineConfigOption::COnlineConfigOption(const char* name, bool default_value)
		: m_name(name), m_minimum(0), m_value(default_value?1:0), m_maximum(1)
{
	GetOptionsNonConst().push_back(this);
}

COnlineConfigOption::COnlineConfigOption(const char* name, const char* default_value)
	: m_name(name), m_minimum(0), m_value(0), m_maximum(0), m_rawValue(default_value)
{
	GetOptionsNonConst().push_back(this);
}

std::vector<COnlineConfigOption*>& COnlineConfigOption::GetOptionsNonConst()
{
	static std::vector<COnlineConfigOption*> s_options;
	return s_options;
}

void COnlineConfigOption::Parse(const char* option)
{
	std::stringstream ss;
	ss << option;
	ss >> m_value;

	if (m_value < m_minimum)
	{
		ERRORF("Value for %s (\"%s\", %d)  is less than %d!", m_name, option, m_value, m_minimum);
		m_value = m_minimum;
	}

	if (m_value > m_maximum)
	{
		ERRORF("Value for %s (\"%s\", %d)  is greater than %d!", m_name, option, m_value, m_maximum);
		m_value = m_maximum;
	}

	m_rawValue = option;
}

const char* COnlineConfigOption::GetName() const
{
	return m_name;
}

const std::string& COnlineConfigOption::GetRawValue() const
{
	return m_rawValue;
}

bool COnlineConfigOption::DownloadOptionsSynchronous()
{
	// Ensure dev server is added to whitelist
	std::string env;
	CUtil::GetEnvStr(env);
	
	if (Constants::DevUsesInternalServer && env == Globals::DevEnvName && !Globals::devPatchServer.GetValue().empty())
	{
		std::string patchServer;
		CUtil::WStringToStdString(Globals::devPatchServer.GetValue(), patchServer);
		WhiteList::Add(patchServer);
	}

	// Sort out the paths
	std::string optionsFilepath;
	std::string optionsUrl;
	CUtil::PrependTempoaryFolderPath(optionsFilepath, "launcher_online_options.xml");

	std::string optionsServer, optionsPath;
	CUtil::GetServerAndDocpathForEnv(optionsServer, optionsPath, Constants::OnlineConfigPathProd);

	optionsUrl = optionsServer + optionsPath;


	// Download callbacks
	struct Closure : public RockstarDownloader::DownloadListener
	{
		CError m_error;

		Closure() {}

		virtual void DownloadComplete(const Patch& /*patch*/, DownloadCompleteStatus /*status*/) {}
		virtual void DownloadStatus(const Patch& /*patch*/, DownloadCompleteStatus /*status*/, u64 /*totalBytesComplete*/, u64 /*totalBytesInFile*/, u64 /*bytesJustDownloaded*/) {}
		virtual void DownloadError(const Patch& /*patch*/, const CError& error)
		{
			m_error = error;
		};
	} closure;

	RockstarDownloader down;
	down.Init();
	down.SetCurrentFile(optionsUrl, optionsFilepath, false, true);
	//@@: location CONLINECONFIGOPTION_DOWNLOADOPTIONSSYNCHRONOUSLY_SET_LISTENER
	down.SetListener(&closure);
	SetOnlineConfigRockstarDownloader(&down);

	down.StartSynchronously();

	SetOnlineConfigRockstarDownloader(NULL);
	down.SetListener(NULL);

	// Handle errors
	if (closure.m_error.IsError())
	{
		// Set the error first, to avoid race conditions with the messages being posted
		CError::SetError(closure.m_error);

#if RSG_LAUNCHER
		if (closure.m_error.IsError(DOWNLOADER_ERR_NO_DISK_SPACE))
		{
			ERRORF("No space to download options file!");
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eNoInstallSpace);
		}
		else
		{
			ERRORF("Unable to download options file!");
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eOffline);
		}
#endif // RSG_LAUNCHER

		return false;
	}
	
	ParseOptions(optionsFilepath);

#if RSG_LAUNCHER
	CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);
#endif

	return true;
}

void COnlineConfigOption::CancelDownloadingOptions()
{
	AUTOCRITSEC(GetOnlineConfigCritSec());

	if (g_OnlineConfigDownloader)
		g_OnlineConfigDownloader->Stop();
}

bool COnlineConfigOption::ParseOptions(const std::string& filepath)
{
	// Grab a copy of the options, to whittle down
	std::vector<COnlineConfigOption*> options = GetOptionsNonConst();

	// Load and parse the XML
	TiXmlDocument doc;
	//@@: location CONLINECONFIGOPTION_PARSEOPTIONS_LOADFILE

	std::wstring wpath;
	CUtil::StdStringToStdWString(filepath, wpath);

	FILE* file = _wfopen(wpath.c_str(), L"r");

	if (!file)
	{
		ERRORF("Unable to open options file!");
		return false;
	}

	doc.LoadFile(file);

	if (doc.Error())
	{
		ERRORF("Unable to load options file!");
		fclose(file);
		return false;
	}

	TiXmlHandle docHandle(&doc);

	// Get the first child of the Options element
	TiXmlElement* opt = docHandle.FirstChild("Options").FirstChild("Option").ToElement();

	// Iterate over all "Option" elements
	for (; opt; opt = opt->NextSiblingElement("Option"))
	{
		const char* optName = opt->Attribute("name");
		const char* optValue = opt->Attribute("value");
		if (!optName || !optValue)
		{
			ERRORF("Invalid option found!");
			continue;
		}

		bool found = false;
		for (int i = 0; i < options.size(); i++)
		{
			if (strcmp(options[i]->GetName(), optName) == 0)
			{
				found = true;
				options[i]->Parse(optValue);
				options.erase(options.begin() + i);
				break;
			}
		}
		
		if (!found)
		{
			DEBUGF1("Option specified in XML does not exist: name=\"%s\" value=\"%s\"", optName, optValue);
		}
	}

	fclose(file);

	if (options.empty())
	{
		// All found; return true
		return true;
	}
	else
	{
#if !RSG_FINAL
		// Some options were missed
		for (int i = 0; i < options.size(); i++)
		{
			ERRORF("Option not specified in XML: %s", options[i]->GetName());
		}
#endif

		return false;
	}
}
