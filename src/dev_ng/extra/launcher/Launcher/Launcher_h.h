

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Wed Jul 30 12:22:03 2014
 */
/* Compiler settings for Launcher.idl:
    Oicf, W1, Zp8, env=Win64 (32b run), target_arch=AMD64 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __Launcher_h_h__
#define __Launcher_h_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ILauncher_FWD_DEFINED__
#define __ILauncher_FWD_DEFINED__
typedef interface ILauncher ILauncher;
#endif 	/* __ILauncher_FWD_DEFINED__ */


#ifndef __Launcher_FWD_DEFINED__
#define __Launcher_FWD_DEFINED__

#ifdef __cplusplus
typedef class Launcher Launcher;
#else
typedef struct Launcher Launcher;
#endif /* __cplusplus */

#endif 	/* __Launcher_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __Launcher_LIBRARY_DEFINED__
#define __Launcher_LIBRARY_DEFINED__

/* library Launcher */
/* [version][uuid] */ 


EXTERN_C const IID LIBID_Launcher;

#ifndef __ILauncher_DISPINTERFACE_DEFINED__
#define __ILauncher_DISPINTERFACE_DEFINED__

/* dispinterface ILauncher */
/* [uuid] */ 


EXTERN_C const IID DIID_ILauncher;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("B22FE848-5751-4303-BCEC-BE823043795A")
    ILauncher : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct ILauncherVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILauncher * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILauncher * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILauncher * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILauncher * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILauncher * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILauncher * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILauncher * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } ILauncherVtbl;

    interface ILauncher
    {
        CONST_VTBL struct ILauncherVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILauncher_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILauncher_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILauncher_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILauncher_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILauncher_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILauncher_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILauncher_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* __ILauncher_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_Launcher;

#ifdef __cplusplus

class DECLSPEC_UUID("43E9FE6F-AB8D-43F1-9D10-11649444AA45")
Launcher;
#endif
#endif /* __Launcher_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


