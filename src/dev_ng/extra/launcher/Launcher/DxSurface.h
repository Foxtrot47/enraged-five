#pragma once

#include <Unknwn.h>

class IPaintDelegate
{
public:
	virtual void OnPaint() = 0;
	virtual void OnLostDevice() = 0;
	virtual void OnResetDevice() = 0;
};

class DxSurface : public CWnd
{
public:
	DxSurface() : m_paintDelegate(NULL) {}

	inline void SetPaintDelegate(IPaintDelegate* delegate)
	{
		m_paintDelegate = delegate;
	}

	virtual HRESULT CreateDevice() = 0;
	virtual void DestroyDevice() = 0;
	virtual IUnknown* GetDxContext() = 0;
	virtual void* GetPresentParams() = 0;
	virtual bool Show(bool show) = 0;
	virtual void Render() = 0;

	HRESULT AddedToWindow(HWND parentWindow)
	{
		m_window = GetSafeHwnd();
		m_parentWindow = parentWindow;
		return S_OK;
	}

	void RemovedFromWindow()
	{
	}

protected:
	HWND m_window, m_parentWindow;
	IPaintDelegate* m_paintDelegate;
};