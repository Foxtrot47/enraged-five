#pragma once

#include <windows.h>
#include <queue>

#include "InPlaceDownloader.h"

class WorkerThread
{
public:

	enum JobType
	{
		JOB_NONE,
		JOB_DOWNLOAD_ONLINE_CONFIG,
#if RSG_STEAM_LAUNCHER
		JOB_STEAM_INIT,
		JOB_STEAM_SHUTDOWN,
#endif
		JOB_CHECK_FOR_SC_UPDATES,
		JOB_CHECK_FOR_GAME_UPDATES,		
		JOB_DOWNLOAD_UPDATE,
		JOB_INSTALL_UPDATE,
		JOB_UPLOAD_REPORT,
#if RSG_IN_PLACE_DOWNLOAD
		JOB_CHECK_FOR_IN_PLACE_DOWNLOAD,
		JOB_DOWNLOAD_IN_PLACE,
		JOB_VERIFY_INTEGRITY,
		JOB_DOWNLOAD_IN_PLACE_IN_BACKGROUND,
#endif
#if RSG_PRELOADER_LAUNCHER
		JOB_CHECK_FOR_RELEASE_DATE,
#endif

	};

	void Start();
	void Stop();
	void Join();

	void AddJob(const JobType& job);

	static WorkerThread& Instance();
	static unsigned int workerThreadTamperResultDWORD;
private:
	WorkerThread();
	~WorkerThread();

	CRITICAL_SECTION m_criticalSection;
	HANDLE m_semaphore;
	CWinThread* m_thread;
	std::queue<JobType> m_jobQueue;
	bool m_bThreadStarted;
	bool m_bThreadShouldExit;
	JobType m_currentJob;

	static UINT ThreadEntryPoint(LPVOID param);

	void MainThreadLoop();
	void DoJob(const JobType& jobType);
	void CancelJob(const JobType& jobType);

	const char* GetJobTypeName(const JobType& jobType);
	
};

