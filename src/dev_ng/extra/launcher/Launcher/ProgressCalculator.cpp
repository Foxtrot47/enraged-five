#include "stdafx.h"

#include "ProgressCalculator.h"
#include "Util.h"

#include "RgscTelemetryManager.h"

#include "resource.h"

std::set<ProgressCalculator*> ProgressCalculator::sm_globalProgress;

ProgressCalculator::ProgressCalculator(ProgressType type) : m_type(type)
{
	Reset();
}

void ProgressCalculator::Reset()
{
	m_currentSegment = 0;
	m_maxUpdateFreqMillis = 900;
	m_segments.clear();
	m_unsizedSegments = 0;
	m_totalBytes = m_totalDownloaded = 0;
	m_lastMessageUpdateTimeMillis = m_lastProgressReceivedTimeMillis = m_lastTelemUpdateTimeMillis = 0;
	m_messageAvailable = false;
	m_currentStatus = DL_STATUS_OK;
	m_movingAverageTotalBytes = m_movingAverageTotalMillis = 0;
	while (!m_movingAverageData.empty())
		m_movingAverageData.pop();

	m_downloadStats.Reset();
	m_progressScalar = 0;
	m_hasConnected = false;
	m_forceMessageUpdate = false;
}

void ProgressCalculator::DownloadStats::Reset()
{
	bytesThisSession = 0;
	sessionStartTimeMillis = 0;
	connectionsOpened = 0;
	connectionsErrored = 0;
	verificationsFailed = 0;
}

void ProgressCalculator::AddSegmentsOfUnknownSize(int count)
{
	m_segments.clear();
	Segment empty(0,-1);
	for (int i = 0; i < count; i++)
	{
		m_segments.push_back(empty);
	}
}

void ProgressCalculator::AddSegment(u64 bytesAlreadyDownloaded, u64 size)
{
	m_segments.push_back(Segment(bytesAlreadyDownloaded, size));

	m_totalDownloaded += bytesAlreadyDownloaded;
	m_totalBytes += size;
}

void ProgressCalculator::SetMaxUpdateFrequency(u32 millis)
{
	m_maxUpdateFreqMillis = millis;
}

void ProgressCalculator::SetCurrentSegment(int segment)
{
	m_currentSegment = segment;
}

void ProgressCalculator::NextSegment()
{
	m_currentSegment++;
}

void ProgressCalculator::SetSegmentComplete(int segment)
{
	if (segment < (int)m_segments.size())
	{
		//@@: location PROGRESSCALCULATOR_SETSEGMENTCOMPLETE
		m_segments[segment].m_complete = true;
		m_segments[segment].m_currentBytes = m_segments[segment].m_totalBytes;
	}
}

void ProgressCalculator::UpdateProgress(DownloadCompleteStatus status, u64 bytesDownloaded, u64 bytesTotal, u64 bytesJustDownloaded, int segmentIndex)
{
	const int MAX_MOVING_AVERAGE_DATAPOINTS = 3000;
	const int MAX_MOVING_AVERAGE_MILLISECONDS = 10000;

	if (m_segments.empty())
	{
		WARNINGF("No segments set before updating progress.");
		if (bytesTotal > 0)
		{
			AddSegment(0, bytesTotal);
		}
		else
		{
			AddSegmentsOfUnknownSize(1);
		}
	}

	if (segmentIndex == -1)
		segmentIndex = m_currentSegment;

	if (segmentIndex >= (int)m_segments.size())
	{
		ERRORF("Invalid segment being updated.");
		return;
	}

	Segment& segment = m_segments[segmentIndex];

	// Update downloaded bytes
	if ((s64)bytesDownloaded != segment.m_currentBytes)
	{
		m_totalDownloaded += bytesDownloaded - segment.m_currentBytes;
		segment.m_currentBytes = bytesDownloaded;
	}

	// Update total bytes (if changed for some reason)
	if (bytesTotal > 0)
	{
		if (segment.m_totalBytes < 0)
		{
			segment.m_totalBytes = (s64)bytesTotal;
			m_totalBytes += bytesTotal;
		}
		else if (bytesTotal != (u64)segment.m_totalBytes)
		{
			m_totalBytes += bytesTotal - (u64)segment.m_totalBytes;
			segment.m_totalBytes = (s64)bytesTotal;
		}
	}

	m_unsizedSegments = 0;
	int unsizedCompleteSegments = 0;
	for (u32 i = 0; i < m_segments.size(); i++)
	{
		if (m_segments[i].m_totalBytes == -1)
		{
			m_unsizedSegments++;

			if (m_segments[i].m_complete)
			{
				unsizedCompleteSegments++;
			}
		}
	}

	if (m_unsizedSegments == 0)
	{
		if (m_totalBytes == 0)
		{
			m_progressScalar = -1;
		}
		else
		{
			m_progressScalar = (float)m_totalDownloaded / (float)m_totalBytes;
		}
	}
	else
	{
		if (m_totalBytes == 0)
		{
			if (m_segments.empty())
			{
				m_progressScalar = -1;
			}
			else
			{
				m_progressScalar = (float)unsizedCompleteSegments / (float)(m_segments.size());
			}
		}
		else
		{
			u64 estimatedTotal = m_totalBytes * m_segments.size();
			u64 estimatedExtraDownloaded = (estimatedTotal - m_totalBytes) * unsizedCompleteSegments / m_unsizedSegments;
			u64 estimatedDownloaded = m_totalDownloaded + estimatedExtraDownloaded;

			m_progressScalar = (float)estimatedDownloaded / (float)estimatedTotal;
		}
	}


	if (!m_messageAvailable || status != DL_STATUS_OK)
	{
		if (status == DL_STATUS_VERIFYING && m_currentStatus != status)
		{
			m_forceMessageUpdate = true;
		}
		m_currentStatus = status;
	}

	u64 time = CUtil::CurrentTimeMillis();

	if (m_downloadStats.sessionStartTimeMillis == 0)
	{
		m_downloadStats.sessionStartTimeMillis = CUtil::CurrentTimeMillis();
	}

	// Only update timing on second chunk of data
	if (m_lastProgressReceivedTimeMillis > 0)
	{
		u64 elapsed = time - m_lastProgressReceivedTimeMillis;

		// Add current data-point
		m_movingAverageData.push(BandwidthDatapoint(bytesJustDownloaded, elapsed));
		m_movingAverageTotalBytes += bytesJustDownloaded;
		m_movingAverageTotalMillis += elapsed;

		// Trim list if it gets too long or out of date
		while ((m_movingAverageData.size() > MAX_MOVING_AVERAGE_DATAPOINTS) || (m_movingAverageData.size() > 2 && m_movingAverageTotalMillis > MAX_MOVING_AVERAGE_MILLISECONDS))
		{
			m_movingAverageTotalBytes -= m_movingAverageData.front().m_bytes;
			m_movingAverageTotalMillis -= m_movingAverageData.front().m_millis;
			m_movingAverageData.pop();
		}
	}

	const u64 MILLIS_PER_TELEM_UPDATE = 10 * 60 * 1000;
	if (m_lastTelemUpdateTimeMillis == 0)
	{
		m_lastTelemUpdateTimeMillis = time;
	}
	else if (time > m_lastTelemUpdateTimeMillis + MILLIS_PER_TELEM_UPDATE)
	{
		ReportTelemetry(false);
		m_lastTelemUpdateTimeMillis = time;
	}

	m_lastProgressReceivedTimeMillis = time;

    m_downloadStats.bytesThisSession += bytesJustDownloaded;
	m_messageAvailable = true;
}

void ProgressCalculator::ForceMessageUpdate()
{
	m_forceMessageUpdate = true;
}

bool ProgressCalculator::IsUpdatedMessageAvailable()
{
	return m_messageAvailable && (CUtil::CurrentTimeMillis() > m_lastMessageUpdateTimeMillis + m_maxUpdateFreqMillis || m_forceMessageUpdate);
}

float ProgressCalculator::GetCurrentProgress()
{
	return m_progressScalar;
}

u64 ProgressCalculator::GetDownloadSessionDurationMillis() const
{
	if (m_downloadStats.sessionStartTimeMillis == 0)
		return 0;
	else
		return CUtil::CurrentTimeMillis() - m_downloadStats.sessionStartTimeMillis;
}

void ProgressCalculator::GetCurrentStatus(std::wstring& out_localisedStatus, float& out_progressScalar)
{
	// Message timing
	u64 time = CUtil::CurrentTimeMillis();
	m_lastMessageUpdateTimeMillis = time;
	m_forceMessageUpdate = false;

	// Progress timing
	u64 millisSinceLastProgressUpdate = 0;
	if (m_lastProgressReceivedTimeMillis > 0)
	{
		millisSinceLastProgressUpdate = time - m_lastProgressReceivedTimeMillis;

		// Long time since an update...
		if (millisSinceLastProgressUpdate > 10000)
		{
			DEBUGF1("Clearing out progress, as it's been 10 seconds since we had valid data.");
			while (!m_movingAverageData.empty())
				m_movingAverageData.pop();

			m_movingAverageTotalBytes = 0;
			m_movingAverageTotalMillis = 0;
		}
	}
	

	// Estimate current download rate
	u64 bytesPerSec = 0;
	if (m_movingAverageTotalMillis > 0)
	{
		bytesPerSec = m_movingAverageTotalBytes * 1000 / (m_movingAverageTotalMillis + millisSinceLastProgressUpdate);
	}


	if (m_currentStatus == DL_STATUS_OK || m_currentStatus == DL_STATUS_SLOW_RETRY)
	{
		std::vector<std::wstring> params;
		const size_t FMT_BUFFER_SIZE = 256;
		wchar_t buffer[FMT_BUFFER_SIZE];
		if (out_progressScalar == -1 || (m_unsizedSegments == 1 && m_segments.size() == 1))
		{
			// Unknown progress, just report download speed
			std::wstring status = CLocalisation::Instance().GetWString(IDS_DOWNLOAD_PROGRESS3);

			// Format download speed
			CUtil::WriteHumanReadableDataSize(buffer, FMT_BUFFER_SIZE, bytesPerSec);
			params.push_back(std::wstring(buffer));

			// Format the message
			CUtil::FormatString(status, params);
			out_localisedStatus = status;
		}
		else if (m_unsizedSegments > 0)
		{
			// Don't know exact size, so report percent
			std::wstring status = CLocalisation::Instance().GetWString(IDS_DOWNLOAD_PROGRESS2);

			// Don't show decimal places for very vague estimates
			if (m_totalBytes == 0)
				swprintf_s(buffer, L"%.0f%%", out_progressScalar * 100.0f);
			else
				swprintf_s(buffer, L"%.2f%%", out_progressScalar * 100.0f);
			params.push_back(std::wstring(buffer));

			// Format download speed
			CUtil::WriteHumanReadableDataSize(buffer, FMT_BUFFER_SIZE, bytesPerSec);
			params.push_back(std::wstring(buffer));

			// Format the message
			CUtil::FormatString(status, params);
			out_localisedStatus = status;
		}
		else
		{
			// We know the exact size, so report it
			std::wstring status = CLocalisation::Instance().GetWString(IDS_DOWNLOAD_PROGRESS);

			// Format downloaded and total
			CUtil::WriteHumanReadableDataSize(buffer, FMT_BUFFER_SIZE, m_totalDownloaded);
			params.push_back(std::wstring(buffer));

			CUtil::WriteHumanReadableDataSize(buffer, FMT_BUFFER_SIZE, m_totalBytes);
			params.push_back(std::wstring(buffer));

			// Format download speed
			CUtil::WriteHumanReadableDataSize(buffer, FMT_BUFFER_SIZE, bytesPerSec);
			params.push_back(std::wstring(buffer));

			// Format the message
			CUtil::FormatString(status, params);
			out_localisedStatus = status;
            params.clear();
			
			// If we have enough data, append a time estimate
			m_speedMovingAverageTotal += bytesPerSec;
			m_speedMovingAverageData.push(bytesPerSec);
			while (m_speedMovingAverageData.size() > 60)
			{
				m_speedMovingAverageTotal -= m_speedMovingAverageData.front();
				m_speedMovingAverageData.pop();
			}

			AppendTimeEstimate(out_localisedStatus);

		}

		if (bytesPerSec > 0)
		{
			m_hasConnected = true;
		}
	}
	else if (m_currentStatus == DL_STATUS_RETRY)
	{
		// If we've connected already, show the reconnect message, instead of the retry message
		if (m_hasConnected)
		{
			out_localisedStatus = CLocalisation::Instance().GetWString(IDS_DOWNLOADER_RECONNECTING);
		}
		else
		{
			out_localisedStatus = CLocalisation::Instance().GetWString(IDS_DOWNLOADER_RETRYING);
		}
	}
	else if (m_currentStatus == DL_STATUS_PAUSE)
	{
		out_localisedStatus = CLocalisation::Instance().GetWString(IDS_DOWNLOAD_PAUSED);
	}
	else if (m_currentStatus == DL_STATUS_VERIFYING)
	{
		const size_t FMT_BUFFER_SIZE = 256;
		wchar_t buffer[FMT_BUFFER_SIZE];

		// Format downloaded and total
		std::vector<std::wstring> params;
		CUtil::WriteHumanReadableDataSize(buffer, FMT_BUFFER_SIZE, m_totalDownloaded);
		params.push_back(std::wstring(buffer));

		CUtil::WriteHumanReadableDataSize(buffer, FMT_BUFFER_SIZE, m_totalBytes);
		params.push_back(std::wstring(buffer));

		std::wstring status = CLocalisation::Instance().GetWString(IDS_DOWNLOAD_VERIFYING);

		CUtil::FormatString(status, params);
		out_localisedStatus = status;
		params.clear();

		AppendTimeEstimate(out_localisedStatus);
	}

	out_progressScalar = GetGlobalProgressScalar();

	// Clear flag
	m_messageAvailable = false;

#if RSG_DEBUG
	u64 elapsed = CUtil::CurrentTimeMillis()-time;
	if (elapsed > 1)
	{
		DEBUGF3("ProgressCalculator::GetCurrentStatus took %llu ms", elapsed);
	}
#endif
}

void ProgressCalculator::AppendTimeEstimate(std::wstring& out_statusString)
{
	const u64 MIN_MILLIS_BEFORE_ESTIMATE = 10 * 1000;
	const u64 MIN_BYTES_BEFORE_ESTIMATE = 100 * 1024;


	u64 totalSessionMillisElapsed = GetDownloadSessionDurationMillis();
	if (totalSessionMillisElapsed > MIN_MILLIS_BEFORE_ESTIMATE && m_downloadStats.bytesThisSession > MIN_BYTES_BEFORE_ESTIMATE && m_speedMovingAverageTotal > m_speedMovingAverageData.size())
	{
		u64 remainingBytes = m_totalBytes - m_totalDownloaded;

		u64 speedMovingAverage = m_speedMovingAverageTotal;
		if (!m_speedMovingAverageData.empty())
			speedMovingAverage /= m_speedMovingAverageData.size();

		u64 remainingMillisEstimate = 1000L * remainingBytes / speedMovingAverage;

		u32 remainingMinutesEstimate = (u32)(remainingMillisEstimate / (1024 * 60));

		u32 minutes = remainingMinutesEstimate % 60;
		u32 hours = (remainingMinutesEstimate / 60) % 24;
		u32 days = remainingMinutesEstimate / (60 * 24);

		std::wstring timeEstimate;

		wchar_t buffer[256];
		std::vector<std::wstring> params;

		if (days > 0 && days < 14)
		{
			swprintf_s(buffer, L"%lu", days);
			params.push_back(std::wstring(buffer));
			swprintf_s(buffer, L"%lu", hours);
			params.push_back(std::wstring(buffer));
			swprintf_s(buffer, L"%lu", minutes);
			params.push_back(std::wstring(buffer));

			timeEstimate = CLocalisation::Instance().GetWString(IDS_DOWNLOAD_ESTIMATE_DAYS);
			CUtil::FormatString(timeEstimate, params);
		}
		else if (hours > 0)
		{
			swprintf_s(buffer, L"%lu", hours);
			params.push_back(std::wstring(buffer));
			swprintf_s(buffer, L"%lu", minutes);
			params.push_back(std::wstring(buffer));

			timeEstimate = CLocalisation::Instance().GetWString(IDS_DOWNLOAD_ESTIMATE_HOURS);
			CUtil::FormatString(timeEstimate, params);
		}
		else if (minutes > 0)
		{
			swprintf_s(buffer, L"%lu", minutes);
			params.push_back(std::wstring(buffer));

			timeEstimate = CLocalisation::Instance().GetWString(IDS_DOWNLOAD_ESTIMATE_MINS);
			CUtil::FormatString(timeEstimate, params);
		}
		else
		{
			swprintf_s(buffer, L"%lu", 1);
			params.push_back(std::wstring(buffer));

			timeEstimate = CLocalisation::Instance().GetWString(IDS_DOWNLOAD_ESTIMATE_LESS_THAN);
			CUtil::FormatString(timeEstimate, params);
		}

		if (timeEstimate.length() > 0)
		{
			out_statusString += L", ";
			out_statusString += timeEstimate;
		}
	}

}

void ProgressCalculator::AddToGlobalProgressList(ProgressCalculator* calc)
{
	sm_globalProgress.insert(calc);
}

void ProgressCalculator::RemoveFromGlobalProgressList(ProgressCalculator* calc)
{
	sm_globalProgress.erase(calc);
}

float ProgressCalculator::GetGlobalProgressScalar()
{
	if (sm_globalProgress.empty())
	{
		return 0;
	}

	float progress = 0;

	for (std::set<ProgressCalculator*>::iterator it = sm_globalProgress.begin(); it != sm_globalProgress.end(); it++)
	{
		progress += (*it)->GetCurrentProgress();
	}

	return progress / (float)(sm_globalProgress.size());
}

const ProgressCalculator::DownloadStats& ProgressCalculator::GetDownloadStats() const
{
	return m_downloadStats;
}

void ProgressCalculator::DownloadEvent(IDownloader::DownloadEvent event)
{
	switch (event)
	{
	case IDownloader::DLE_CONNECTION_OPENED:
		m_downloadStats.connectionsOpened++;
		break;

	case IDownloader::DLE_CONNECTION_ERROR:
		m_downloadStats.connectionsErrored++;
		break;

	case IDownloader::DLE_VERIFICATION_FAILURE:
		m_downloadStats.verificationsFailed++;
		break;
	case IDownloader::DLE_ENDING:
		CUtil::LockTimings::PrintTimings();
		break;
	}
}

void ProgressCalculator::GetStatusString(std::string& out_status)
{
	char buffer[256];

	switch (m_type)
	{
	case PT_DOWNLOAD_SC_UPDATE:
		out_status += "S";
		break;
	case PT_DOWNLOAD_DIP_DATA:
		out_status += "D";
		break;
	case PT_VERIFY_DIP_DATA:
		out_status += "V";
		break;
	case PT_DOWNLOAD_PATCH:
		out_status += "P";
		break;
	default:
		out_status += "?";
		break;
	}

	// Bytes downloaded
	sprintf(buffer, "|%llu", m_downloadStats.bytesThisSession);
	out_status += buffer;

	// Session time in seconds
	sprintf(buffer, "|%llu", GetDownloadSessionDurationMillis() / 1000L);
	out_status += buffer;

	// Connections opened
	sprintf(buffer, "|%lu", m_downloadStats.connectionsOpened);
	out_status += buffer;

	// Connections errored
	sprintf(buffer, "|%lu", m_downloadStats.connectionsErrored);
	out_status += buffer;

	// Verifications failed
	sprintf(buffer, "|%lu", m_downloadStats.verificationsFailed);
	out_status += buffer;
}

void ProgressCalculator::ReportTelemetry(bool downloadEnding)
{
	std::string telemString;
	GetStatusString(telemString);
	DISPLAYF("Dl: %s", telemString.c_str());

	RgscTelemetryManager::DownloadStats stats;
	RgscTelemetryManager::PopulateTelemetryDownloadStatsFromProgressCalculator(*this, stats);

	RgscTelemetryManager::WriteEventDownloadStats(downloadEnding ? RgscTelemetryManager::EVENT_LAUNCHER_DOWNLOAD_STATS : RgscTelemetryManager::EVENT_LAUNCHER_INTERMEDIATE_DOWNLOAD_STATS, stats);

	ResetDownloadStats();
}

ProgressCalculator::ProgressType ProgressCalculator::GetDownloadType() const
{
	return m_type;
}

void ProgressCalculator::ResetDownloadStats()
{
	m_downloadStats.Reset();
}
