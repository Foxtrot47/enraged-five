/*
 * This class is based on the DownloadManager C# file UpdateCheck.cs
 */
#pragma once

#include "IPatchCheck.h"

#include <string>
#include <vector>
#include "IDownloader.h"


namespace rgsc
{
	typedef s64 RockstarId;
}

class UpdateChecker
{
public:
	static UpdateChecker& Instance();
	void Reset();
	bool IsUpToDate();
	bool CheckForUpdates(IDownloader::DownloadListener* listener);
	//std::string GetPatchTypeString(Patch& patch);

	std::vector<Patch>& PatchesToInstall() { return m_patchesToInstall; }

	bool IsChecking() { return m_bChecking; }
	UpdateType GetUpdateType() { return m_eUpdateType; }
	PatchType GetPatchType() { return m_ePatchType; }

	void DownloadComplete(DownloadCompleteStatus status, std::string filename);
	void DownloadError(const CError& error);

	void AddBootstrapCheck();
	void AddLauncherCheck();
	void AddRGSCCheck();
	void AddGameCheck();
	void AddMtlCheck();
	void AddDLCCheck(rgsc::RockstarId rockstarId);

	void AddPatch(const Patch& patch);

	void Cancel();


private:
	UpdateChecker(void);
	~UpdateChecker(void);
	std::vector<IPatchCheck*>	m_patchChecks;
	std::vector<Patch>			m_patchesToInstall;

	size_t m_currentUpdate;
	bool m_bChecking;
	UpdateType m_eUpdateType;
	PatchType m_ePatchType;
};
