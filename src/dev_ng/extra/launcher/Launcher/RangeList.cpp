#include "stdafx.h"

#include "RangeList.h"
#include "Channel.h"

#include <openssl/sha.h>

#define RANGELIST_VERBOSE_DEBUG (0 && RSG_DEBUG)

void RangeList::AddRange(const Range& in_range)
{
	Range range = in_range;

#if RANGELIST_VERBOSE_DEBUG
	DEBUGF3("Adding range: %llu - %llu inclusive (length %llu)", range.m_offset, range.m_offset+range.m_length-1, range.m_length);
	char buffer[256];
	sprintf_s(buffer, "Adding %llu - %llu\n", range.m_offset, range.m_offset+range.m_length-1);
	m_debugHistory += buffer;
#endif

	s64 expectedByteDifference = (s64)range.m_length;
	s64 byteDifference = 0;

	// Merge the range with any overlapping or adjacent ranges
	bool extended = true;
	while (extended)
	{
		u64 rangeStart = range.m_offset;
		u64 rangeEnd = range.m_offset + range.m_length;

		// Look for overlapping ranges
		extended = false;
		for (int i = 0; i < m_ranges.size(); i++)
		{
			u64 start = m_ranges[i].m_offset;
			u64 end = start + m_ranges[i].m_length;

			if (start < rangeEnd && end > rangeStart)
			{
				DEBUGF3("Actual overlap detected (incoming %llu - %llu overlaps %llu - %llu)", rangeStart, rangeEnd-1, start, end-1);
#if RANGELIST_VERBOSE_DEBUG
				DEBUGF3("%s", m_debugHistory.c_str());
#endif
			}

			if (start <= rangeEnd && end >= rangeStart)
			{
#if RANGELIST_VERBOSE_DEBUG
				DEBUGF3("Range %llu - %llu (length %llu) overlaps %llu - %llu (length %llu)", rangeStart, rangeEnd-1, range.m_length, start, end-1, m_ranges[i].m_length);
#endif

				// Merge this range with the new range
				range.m_offset = std::min<u64>(start, rangeStart);
				range.m_length = std::max<u64>(end, rangeEnd) - range.m_offset;

#if RANGELIST_VERBOSE_DEBUG
				DEBUGF3("New range: %llu - %llu (length %llu)", range.m_offset, range.m_offset+range.m_length-1, range.m_length);
#endif


				byteDifference -= m_ranges[i].m_length;
#if !RSG_FINAL
				//DEBUGF3("Merging with range %llu -> %llu (%llu bytes).", start, end-1, end-start);
#endif

				// Delete the merged-in range
				m_ranges.erase(m_ranges.begin() + i);

				// Go round again
				extended = true;
				break;
			}
		}
	}

	m_ranges.push_back(range);

	byteDifference += range.m_length;

	if (byteDifference != expectedByteDifference)
	{
		DEBUGF3("Byte change is %lld (expected %lld)", byteDifference, expectedByteDifference);
	}
}

void RangeList::RemoveRange(const Range& removalRange)
{
#if RANGELIST_VERBOSE_DEBUG
	char buffer[256];
	sprintf_s(buffer, "Removing %llu - %llu\n", removalRange.m_offset, removalRange.m_offset+removalRange.m_length-1);
	m_debugHistory += buffer;
#endif

	// Test all existing ranges (in reverse order to make removals easier)
	for (int i = (int)m_ranges.size()-1; i >= 0; i--)
	{
		const Range testRange = m_ranges[i];

		// Ignore non-intersecting ranges
		if (testRange.m_offset >= removalRange.GetEnd() || testRange.GetEnd() <= removalRange.m_offset)
			continue;

		// Find which of the four intersection cases we have, starting with comparing the starts
		if (testRange.m_offset < removalRange.m_offset)
		{
			// Check if the middle of this range is being cut out
			if (testRange.GetEnd() > removalRange.GetEnd())
			{
				// Add on the tail that is outside the removal range
				Range tail(removalRange.GetEnd(), testRange.GetEnd() - removalRange.GetEnd());
				m_ranges.push_back(tail);
			}

			// Trim the current range down to just the remaining head
			m_ranges[i].m_length = removalRange.m_offset - testRange.m_offset;
		}
		else
		{
			// Check if we're just left with a tail
			if (testRange.GetEnd() > removalRange.GetEnd())
			{
				// Trim the current range to just be the tail
				m_ranges[i].Set(removalRange.GetEnd(), testRange.GetEnd() - removalRange.GetEnd());
			}
			else
			{
				// Completely enclosed by the removal range, so erase it
				m_ranges.erase(m_ranges.begin() + i);
			}
		}
	}
}

bool RangeList::ContainsCompleteRange(const Range& range) const
{
	for (size_t i = 0; i < m_ranges.size(); i++)
	{
		if (m_ranges[i].ContainsCompleteRange(range))
			return true;
	}

	return false;
}

PartiallyCompleteRange::PartiallyCompleteRange() : m_data(NULL)
{
}

void PartiallyCompleteRange::Init(const Sha256Range& range)
{
	m_sha256Range = range;
	m_data = new char[m_sha256Range.m_range.m_length];
}

void PartiallyCompleteRange::MoveFrom(PartiallyCompleteRange& range)
{
	Cleanup();
	m_sha256Range = range.m_sha256Range;
	m_data = range.m_data;
	range.m_data = NULL;
}

void PartiallyCompleteRange::Cleanup()
{
	if (m_data)
	{
		delete[] m_data;
		m_data = NULL;
	}
}

void PartiallyCompleteRange::WriteData(u64 globalOffset, u64 length, const char* data)
{
	RAD_TELEMETRY_ZONE("PartiallyCompleteRange::WriteData");
	u64 localIndex = globalOffset - m_sha256Range.m_range.m_offset;

#if RANGELIST_VERBOSE_DEBUG
	DEBUGF1("Partial range %llu - %llu writing %llu - %llu (%llu - %llu)", m_sha256Range.m_range.m_offset, m_sha256Range.m_range.GetEnd()-1, globalOffset, globalOffset + length - 1, globalOffset-m_sha256Range.m_range.m_offset, globalOffset+length-1-m_sha256Range.m_range.m_offset);
#endif
	memcpy(&m_data[localIndex], data, length);

	m_progress.AddRange(Range(globalOffset, length));
}

bool PartiallyCompleteRange::IsComplete() const
{
	RAD_TELEMETRY_ZONE("IsComplete");
	return m_progress.ContainsCompleteRange(m_sha256Range.m_range);
}

bool PartiallyCompleteRange::CheckHash()
{
	RAD_TELEMETRY_ZONE("CheckHash");
	SHA256_CTX ctx;
	SHA256_Init(&ctx);

#if RANGELIST_VERBOSE_DEBUG
	DEBUGF3("Verifying hash of partial range %llu - %llu... ", m_sha256Range.m_range.m_offset, m_sha256Range.m_range.m_offset + m_sha256Range.m_range.m_length - 1);
#endif

	SHA256_Update(&ctx, GetData(), m_sha256Range.m_range.m_length);

	u8 hash[SHA256_DIGEST_LENGTH];
	SHA256_Final(hash, &ctx);

	if (memcmp(m_sha256Range.m_hash, hash, sizeof(hash)) == 0)
	{
		char hexhash[SHA256_DIGEST_LENGTH*2+1];
		for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
		{
			sprintf_s(&(hexhash[i*2]), 3, "%02x", hash[i]);
		}
#if RANGELIST_VERBOSE_DEBUG
		DEBUGF3("Range %llu - %llu has hash: %s", m_sha256Range.m_range.m_offset, m_sha256Range.m_range.m_offset + m_sha256Range.m_range.m_length - 1, hexhash);
#endif
		return true;
	}
	else
	{
		char hexhash[SHA256_DIGEST_LENGTH*2+1];
		for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
		{
			sprintf_s(&(hexhash[i*2]), 3, "%02x", hash[i]);
		}

		char expectedHexhash[SHA256_DIGEST_LENGTH*2+1];
		for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
		{
			sprintf_s(&(expectedHexhash[i*2]), 3, "%02x", (u8)m_sha256Range.m_hash[i]);
		}

		WARNINGF("Range %llu - %llu has hash %s instead of %s", m_sha256Range.m_range.m_offset, m_sha256Range.m_range.m_offset + m_sha256Range.m_range.m_length - 1, hexhash, expectedHexhash);
		m_progress.Clear();
		return false;
	}
}