#pragma once

#include "IDownloader.h"
#include "IPatchCheck.h"
#include "Error.h"

#include <string>
#include <WinInet.h>

class RockstarDownloader : public IDownloader
{
public:
	RockstarDownloader(void);
	~RockstarDownloader(void);

	virtual void Init();
	virtual void SetPaused(bool paused);
	virtual void Stop();

	virtual void StartSynchronously();

	virtual void SetCurrentFile(std::string downloadUrl, std::string localFilePath, bool backgroundDownload, bool alwaysdownload);
	virtual void SetCurrentPatch(Patch& patch);

	static bool GetServer(const char* url, char** out_server, bool bTrimHTTP = false);
	static bool GetDiskPath(const char* in_path, char** out_path);
	static bool GetFullPath(const char* in_path, char** out_path);
	static bool GetFilename(const char* path, char ** out_file);

	void ThrowError(const CError& error);
	virtual void UpdateDownloadProgress(Patch& patch);

	virtual int GetProgress() { return m_progress; }

	static void CALLBACK OnStatusCallback(HINTERNET hInternet, DWORD_PTR dwContext, DWORD dwInternetStatus, LPVOID lpvStatusInformation, DWORD dwStatusInformationLength);

	static bool CanAccessUrl(const std::string& url);
	static bool GetServerTime(const std::string& url, FILETIME& out_filetime);

	static bool CheckForDiskSpace(const std::string& path, u64 required);

protected:
	enum UrlInfoType
	{
		URL_INFO_TYPE_STATUS_CODE,
		URL_INFO_TYPE_CONTENT_LENGTH,
		URL_INFO_TYPE_SERVER_TIME,
	};

	bool CheckExeHash(std::string filename);
	s64 GetUrlInfoLong(const std::string& url, const UrlInfoType infoType);
	bool CheckForDiskSpace(Patch& patch);
	void DoDownload();
	virtual void UpdateListener(DownloadCompleteStatus status);
	void CloseAllHandles();
	void IncrementTimeout();
	ULONG GetTimeout() { return m_timeoutMillis; }

	bool CheckFileExists(std::string filename);

	virtual void AddBytesDownloaded(s64 bytes); // Note: bytes can be negative, if a chunk is corrupt and discarded

	static void PrintInternetError();

	volatile u64 m_bytesRead, m_totalBytes;
	int m_progress, m_logProgress;

	bool m_downloading;
	bool m_paused;
	bool m_bUsesSSL;

	u64 m_timingLastMillisecond;
	u64 m_timingDataSize;
	u64 m_timingDataPerSecond;

	ULONG m_timeoutMillis;

	// Broken down into separate steps, so errors can just return,
	// and each enclosing function can still clean up file handles etc.
	DownloadCompleteStatus DoDownload_IsUrlValid(Patch& patch);
	virtual DownloadCompleteStatus DoDownload_CheckDisk(Patch& patch);
	virtual DownloadCompleteStatus DoDownload_ReceiveData(Patch& patch);

	TCHAR m_hostURL[MAX_PATH];
	TCHAR m_fileURL[MAX_PATH];
	HINTERNET m_hOpen, m_hConnect, m_hRequest, m_hOpenUrl;
};
