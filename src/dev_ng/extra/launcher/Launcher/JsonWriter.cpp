#include "stdafx.h"
#include "JsonWriter.h"

JsonWriter::JsonWriter(std::string& buffer) : m_buffer(buffer), m_firstItem(true)
{
}

JsonWriter::JsonWriter() : m_buffer(m_internalBuffer), m_firstItem(true)
{
}

JsonWriter& JsonWriter::StartObject(const char* name /*= NULL*/)
{
	WriteCommaIfNeeded();

	if (name)
		WriteName(name);

	m_buffer += "{";
	m_firstItem = true;

	return *this;
}

JsonWriter& JsonWriter::EndObject()
{
	m_buffer += "}";
	m_firstItem = false;

	return *this;
}

JsonWriter& JsonWriter::StartArray(const char* name /*= NULL*/)
{
	WriteCommaIfNeeded();

	if (name)
		WriteName(name);

	m_buffer += "[";
	m_firstItem = true;

	return *this;
}

JsonWriter& JsonWriter::EndArray()
{
	m_buffer += "]";
	m_firstItem = false;

	return *this;
}

JsonWriter& JsonWriter::WriteNamedString(const char* name, const char* value)
{
	WriteCommaIfNeeded();

	if (name)
		WriteName(name);

	m_buffer += "\"";
	WriteEscapedString(value);
	m_buffer += "\"";
	m_firstItem = false;

	return *this;
}

JsonWriter& JsonWriter::WriteString(const char* value)
{
	WriteCommaIfNeeded();

	m_buffer += "\"";
	WriteEscapedString(value);
	m_buffer += "\"";
	m_firstItem = false;

	return *this;
}

JsonWriter& JsonWriter::WriteNamedBool(const char* name, bool value)
{
	WriteCommaIfNeeded();

	if (name)
		WriteName(name);

	m_buffer += value ? "true":"false";
	m_firstItem = false;

	return *this;
}

JsonWriter& JsonWriter::WriteBool(bool value)
{
	WriteCommaIfNeeded();

	m_buffer += value ? "true":"false";
	m_firstItem = false;

	return *this;
}

void JsonWriter::WriteCommaIfNeeded()
{
	if (!m_firstItem)
		m_buffer += ",";
}

void JsonWriter::WriteName(const char* name)
{
	if (!name)
		return;

	m_buffer += "\"";
	m_buffer += name;
	m_buffer += "\":";
}

void JsonWriter::WriteEscapedString(const char* string)
{
	const static char replacements[] = { '[', '{', '"', ']', '}', ':', '\'' };

	for (const char* ptr = string; *ptr; ptr++)
	{
		if (*ptr == '"')
		{
			m_buffer += "%22";
		}
		else
		{
			m_buffer += *ptr;
		}
	}
}
