#include "stdafx.h"
#include "Error.h"
#include "Util.h"
#include "Config.h"
#include "Application.h"
#include "resource.h"
#include "Channel.h"
#include "RgscTelemetryManager.h"
#include <regex>
#include <iomanip>


static CError s_lastError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_NONE);

void CError::SetError(const CError& error)
{
	ErrorCategory category = error.GetErrorCategory();
	int code = error.GetErrorCode();
	std::string extra = error.GetExtra(), loc = GetLocalisedErrorMessage(error);

	ERRORF("************************************************************************");
	ERRORF("* Setting error condition");
	ERRORF("* Category: %2d (%s)", category, GetErrorCategoryAsString(category));

	std::string codeString = GetErrorCodeAsString(category, code);
	ERRORF("* Code:     %2d (%s)", code, codeString.c_str());
	ERRORF("* Extra:     %s", extra.c_str());
	ERRORF("* Localised: %s", loc.c_str());
	ERRORF("************************************************************************");
	s_lastError = error;
	AutoSetSupportURL(error);
}

void CError::SetError(ErrorCategory category, InitErrorCodes error, const std::string& extra)
{
	SetError(CError(category, error, extra));
}

void CError::SetError(ErrorCategory category, LauncherErrorCodes error, const std::string& extra)
{
	SetError(CError(category, error, extra));
}

void CError::SetError(ErrorCategory category, DownloaderErrorCodes error, const std::string& extra)
{
	SetError(CError(category, error, extra));
}

void CError::SetErrorExitCode(int error, const std::string& extra)
{
	SetError(CError(ERROR_CATEGORY_EXIT_CODE, error, extra));
}

const CError& CError::GetLastError()
{
	return s_lastError;
}

void CError::ResetLastError()
{
	s_lastError.Reset();
}

const CError& CError::NoError()
{
	static CError err(ERROR_CATEGORY_NONE, 0);
	return err;
}

void CError::AutoSetSupportURL(const CError& error)
{
	const std::string& message = GetLocalisedErrorMessage(error);

	std::regex pattern("(https?://)(?:\\w+(?:\\.\\w+)+(?:[\\w\\\\/._~#?&+%\\-\\=]*[\\w#/])?)");

	std::smatch results;
	if (std::regex_search(message, results, pattern))
	{
		const std::string& result = results[0];
		SetSupportURL(result);
	}
	else
	{
		SetSupportURL("");
	}
}

std::string CError::GetLocalisedErrorMessage(const CError& err)
{
	std::string errorString;

	switch (err.GetErrorCategory())
	{
	case ERROR_CATEGORY_LAUNCHER:
		switch (err.GetErrorCode())
		{
		case LAUNCHER_ERR_NO_GAME_EXE: errorString = CLocalisation::Instance().GetString(IDS_LAUNCHER_ERR_NO_EXE); break;
		case LAUNCHER_ERR_NO_GAME_PATH: errorString = CLocalisation::Instance().GetString(IDS_LAUNCHER_ERR_NO_GAME_PATH); break;
		case LAUNCHER_ERR_MISSING_DEPENDENCY: errorString = CLocalisation::Instance().GetString(IDS_LAUNCHER_ERR_MISSING_DEPENDENCY); break;
		case LAUNCHER_ERR_DIRECTX: errorString = CLocalisation::Instance().GetString(IDS_LAUNCHER_ERR_CREATE_DEVICE); break;
		case LAUNCHER_ERR_32BIT: errorString = CLocalisation::Instance().GetString(IDS_LAUNCHER_ERR_32BIT); break;
		case LAUNCHER_ERR_UNSUPPORTED_WINDOWS_VERSION: errorString = CLocalisation::Instance().GetString(IDS_LAUNCHER_ERR_UNSUPPORTED_WINDOWS_VERSION); break;
		case LAUNCHER_ERR_INSTALL_FAILED: errorString = CLocalisation::Instance().GetString(IDS_LAUNCHER_ERR_INSTALL_FAILED); break;
		case LAUNCHER_ERR_STEAM_FAILED: errorString = CLocalisation::Instance().GetString(IDS_LAUNCHER_ERR_STEAM_FAILED); break;
		case LAUNCHER_ERR_STEAM_NO_ENTITLEMENT: errorString = CLocalisation::Instance().GetString(IDS_LAUNCHER_ERR_STEAM_NO_ENTITLEMENT); break;
		case LAUNCHER_ERR_MISSING_WMP: errorString = CLocalisation::Instance().GetString(IDS_LAUNCHER_ERR_NEED_WMP); break;
		case LAUNCHER_ERR_CLOCK_WRONG: errorString = CLocalisation::Instance().GetString(IDS_LAUNCHER_ERR_CLOCK_WRONG); break;
		case LAUNCHER_ERR_NO_BOOTSTRAP: errorString = CLocalisation::Instance().GetString(IDS_LAUNCHER_ERR_NO_BOOTSTRAP); break;
		default: errorString = CLocalisation::Instance().GetString(IDS_LAUNCHER_ERROR); break;
		}
		break;

	case ERROR_CATEGORY_DOWNLOADER:
		switch (err.GetErrorCode())
		{
		case DOWNLOADER_ERR_ACU_FAILED: errorString = CLocalisation::Instance().GetString(IDS_SCS_UNAVAILABLE); break;
		default: errorString = CLocalisation::Instance().GetString(IDS_UPDATES_UNAVAILABLE); break;
		}
		
		break;

	case ERROR_CATEGORY_SOCIAL_CLUB:
		//@@: location CERROR_GETLOCALISEDERRORMESSAGE_SOCIAL_CLUB
		switch (err.GetErrorCode())
		{
		case SC_INIT_ERR_NO_DLL: errorString = CLocalisation::Instance().GetString(IDS_SOCIALCLUB_DLL_ERROR); break;
		case SC_INIT_ERR_LOAD_LIBRARY_FAILED: errorString = CLocalisation::Instance().GetString(IDS_SOCIALCLUB_DLL_ERROR); break;
		case SC_INIT_ERR_QUERY_INTERFACE_FAILED: errorString = CLocalisation::Instance().GetString(IDS_SOCIALCLUB_OLD_ERROR); break;
		case SC_INIT_ERR_CREATE_DEVICE_DX9: errorString = CLocalisation::Instance().GetString(IDS_SC_INIT_ERR_CREATE_DEVICE_DX9); break;
		case SC_INIT_ERR_CREATE_DEVICE_DX11: errorString = CLocalisation::Instance().GetString(IDS_SC_INIT_ERR_CREATE_DEVICE_DX11); break;
		default: errorString = CLocalisation::Instance().GetString(IDS_SOCIALCLUB_DLL_ERROR); break;
		}
		break;
	
	case ERROR_CATEGORY_GAME:
		errorString = CLocalisation::Instance().GetString(IDS_GAME_ERROR);
		break;

	case ERROR_CATEGORY_EXIT_CODE:
		if (err.GetExtra().length() == 0)
		{
			std::vector<std::string> params;
			errorString = CLocalisation::Instance().GetString(IDS_EXITCODE_CRASHED);
			CUtil::FormatString(errorString, params);
			return errorString;
		}
		else
		{
			return err.GetExtra();
		}
	}

	return CUtil::FormatStringNumberAndString(errorString, err.GetErrorCode(), err.GetExtra());
}

void CError::ReportErrorTelemetry(const CError& err)
{
	switch (err.GetErrorCategory())
	{
	case ERROR_CATEGORY_LAUNCHER:
		RgscTelemetryManager::WriteEventInt(RgscTelemetryManager::EVENT_LAUNCHER_ERROR_MESSAGE, err.GetErrorCode());
		break;
	case ERROR_CATEGORY_DOWNLOADER:
		RgscTelemetryManager::WriteEventInt(RgscTelemetryManager::EVENT_LAUNCHER_DOWNLOADER_ERROR, err.GetErrorCode());
		break;
	case ERROR_CATEGORY_EXIT_CODE:
		RgscTelemetryManager::WriteEventInt(RgscTelemetryManager::EVENT_LAUNCHER_GAME_EXIT_CODE, err.GetErrorCode());
		break;
	case ERROR_CATEGORY_SOCIAL_CLUB:
		RgscTelemetryManager::WriteEventInt(RgscTelemetryManager::EVENT_LAUNCHER_SC_LOAD_ERROR, err.GetErrorCode());
		break;
	}
}

static std::string s_sSupportURL;

void CError::SetSupportURL(const std::string& url)
{
	DEBUGF1("Support URL: %s", url.c_str());
	s_sSupportURL = url;
}

const std::string& CError::GetSupportURL()
{
	return s_sSupportURL;
}


CError::CError()
{
	*this = NoError();
}

CError::CError(ErrorCategory category, InitErrorCodes error, const std::string& extra)
: m_eCategory(category), m_iError(error), m_sExtraInfo(extra)
{
}

CError::CError(ErrorCategory category, LauncherErrorCodes error, const std::string& extra)
	: m_eCategory(category), m_iError(error), m_sExtraInfo(extra)
{
}

CError::CError(ErrorCategory category, DownloaderErrorCodes error, const std::string& extra)
	: m_eCategory(category), m_iError(error), m_sExtraInfo(extra)
{
}

CError::CError(ErrorCategory category, int error, const std::string& extra)
	: m_eCategory(category), m_iError(error), m_sExtraInfo(extra)
{
}

CError::CError(const CError& error)
: m_eCategory(error.m_eCategory), m_iError(error.m_iError), m_sExtraInfo(error.m_sExtraInfo)
{
}

const CError& CError::operator=(const CError& error)
{
	m_eCategory = error.m_eCategory;
	m_iError = error.m_iError;
	m_sExtraInfo = error.m_sExtraInfo;

	return *this;
}

const bool CError::operator==(const CError& error) const
{
	return m_eCategory == error.m_eCategory &&
		m_iError == error.m_iError &&
		m_sExtraInfo == error.m_sExtraInfo;
}

const bool CError::operator!=(const CError& error) const
{
	return !(operator==(error));
}

const char* CError::GetErrorCategoryAsString(ErrorCategory category)
{
	//@@: range CERROR_GETERRORCATEGORYASSTRING {
	
	//@@: location CERROR_GETERRORCATEGORYASSTRING_LOCATION
	switch (category)
	{
	case ERROR_CATEGORY_NONE: return "ERROR_CATEGORY_NONE";
	case ERROR_CATEGORY_LAUNCHER: return "ERROR_CATEGORY_LAUNCHER";
	case ERROR_CATEGORY_DOWNLOADER: return "ERROR_CATEGORY_DOWNLOADER";
	case ERROR_CATEGORY_SOCIAL_CLUB: return "ERROR_CATEGORY_SOCIAL_CLUB";
	case ERROR_CATEGORY_GAME: return "ERROR_CATEGORY_GAME";
	case ERROR_CATEGORY_EXIT_CODE: return "ERROR_CATEGORY_EXIT_CODE";
	default: return "<UNKNOWN>";
	}
	//@@: } CERROR_GETERRORCATEGORYASSTRING

}

std::string CError::GetErrorCodeAsString(ErrorCategory category, int code)
{
	//@@: range CERROR_GETERRORCODEASSTRING {
	//@@: location CERROR_GETERRORCODEASSTRING_LOCATION
	switch (category)
	{
	case ERROR_CATEGORY_LAUNCHER:
		switch (code)
		{
		case LAUNCHER_ERR_NONE: return "LAUNCHER_ERR_NONE";
		case LAUNCHER_ERR_NO_GAME_EXE: return "LAUNCHER_ERR_NO_GAME_EXE";
		case LAUNCHER_ERR_NO_GAME_PATH: return "LAUNCHER_ERR_NO_GAME_PATH";
		case LAUNCHER_ERR_MISSING_DEPENDENCY: return "LAUNCHER_ERR_MISSING_DEPENDENCY";
		case LAUNCHER_ERR_STARTING_ENUMERATING_FILES: return "LAUNCHER_ERR_STARTING_ENUMERATING_FILES";
		case LAUNCHER_ERR_ENUMERATING_FILES: return "LAUNCHER_ERR_ENUMERATING_FILES";
		case LAUNCHER_ERR_OPEN_PROCESS_FAILED: return "LAUNCHER_ERR_OPEN_PROCESS_FAILED";
		case LAUNCHER_ERR_GAME_CRASHED: return "LAUNCHER_ERR_GAME_CRASHED";
		case LAUNCHER_ERR_DIRECTX: return "LAUNCHER_ERR_DIRECTX";
		case LAUNCHER_ERR_32BIT: return "LAUNCHER_ERR_32BIT";
		case LAUNCHER_ERR_INSTALL_FAILED: return "LAUNCHER_ERR_INSTALL_FAILED";
		case LAUNCHER_ERR_UNSUPPORTED_WINDOWS_VERSION: return "LAUNCHER_ERR_UNSUPPORTED_WINDOWS_VERSION";
		case LAUNCHER_ERR_SEH_EXCEPTION: return "LAUNCHER_ERR_SEH_EXCEPTION";
		case LAUNCHER_ERR_STEAM_FAILED: return "LAUNCHER_ERR_STEAM_FAILED";
		case LAUNCHER_ERR_STEAM_NO_ENTITLEMENT: return "LAUNCHER_ERR_STEAM_NO_ENTITLEMENT";
		case LAUNCHER_ERR_STEAM_NO_STEAM: return "LAUNCHER_ERR_STEAM_NO_STEAM";
		case LAUNCHER_ERR_OPEN_SPARSE_FILE_FAILED: return "LAUNCHER_ERR_OPEN_SPARSE_FILE_FAILED";
		case LAUNCHER_ERR_MISSING_WMP: return "LAUNCHER_ERR_MISSING_WMP";
		case LAUNCHER_ERR_CLOCK_WRONG: return "LAUNCHER_ERR_CLOCK_WRONG";
		}
		break;

	case ERROR_CATEGORY_DOWNLOADER:
		switch (code)
		{
		case DOWNLOADER_ERR_NONE: return "DOWNLOADER_ERR_NONE";
		case DOWNLOADER_ERR_UNKNOWN: return "DOWNLOADER_ERR_UNKNOWN";
		case DOWNLOADER_ERR_NO_CONNECTION: return "DOWNLOADER_ERR_NO_CONNECTION";
		case DOWNLOADER_ERR_INTERRUPTED: return "DOWNLOADER_ERR_INTERRUPTED";
		case DOWNLOADER_ERR_INVALID_SIGNATURE: return "DOWNLOADER_ERR_INVALID_SIGNATURE";
		case DOWNLOADER_ERR_BAD_CRC: return "DOWNLOADER_ERR_BAD_CRC";
		case DOWNLOADER_ERR_NO_DISK_SPACE: return "DOWNLOADER_ERR_NO_DISK_SPACE";
		case DOWNLOADER_ERR_BAD_URL: return "DOWNLOADER_ERR_BAD_URL";
		case DOWNLOADER_ERR_UNENTITLED: return "DOWNLOADER_ERR_UNENTITLED";
		case DOWNLOADER_ERR_ACU_FAILED: return "DOWNLOADER_ERR_ACU_FAILED";
		case DOWNLOADER_ERR_DOWNLOAD_MANIFEST_FAILED: return "DOWNLOADER_ERR_DOWNLOAD_MANIFEST_FAILED";
		case DOWNLOADER_ERR_READ_MANIFEST_FAILED: return "DOWNLOADER_ERR_READ_MANIFEST_FAILED";
		case DOWNLOADER_ERR_DOWNLOAD_VERSIONING_FAILED: return "DOWNLOADER_ERR_DOWNLOAD_VERSIONING_FAILED";
		case DOWNLOADER_ERR_READ_VERSIONING_FAILED: return "DOWNLOADER_ERR_READ_VERSIONING_FAILED";
		case DOWNLOADER_ERR_GET_SERVER_TIME_FAILED: return "DOWNLOADER_ERR_GET_SERVER_TIME_FAILED";
		case DOWNLOADER_ERR_PRELOAD_TIMER_EXPIRED: return "DOWNLOADER_ERR_PRELOAD_TIMER_EXPIRED";
		case DOWNLOADER_ERR_READ_FAILED: return "DOWNLOADER_ERR_READ_FAILED";
		case DOWNLOADER_ERR_READ_ERROR: return "DOWNLOADER_ERR_READ_ERROR";
		case DOWNLOADER_ERR_REQUEST_ERROR: return "DOWNLOADER_ERR_REQUEST_ERROR";
		case DOWNLOADER_ERR_SEND_ERROR: return "DOWNLOADER_ERR_SEND_ERROR";
		}
		break;

	case ERROR_CATEGORY_SOCIAL_CLUB:
		switch (code)
		{
		case SC_INIT_ERR_NONE: return "SC_INIT_ERR_NONE";
		case SC_INIT_ERR_NO_DLL: return "SC_INIT_ERR_NO_DLL";
		case SC_INIT_ERR_NO_SC_COMMAND_LINE: return "SC_INIT_ERR_NO_SC_COMMAND_LINE";
		case SC_INIT_ERR_PROGRAM_FILES_NOT_FOUND: return "SC_INIT_ERR_PROGRAM_FILES_NOT_FOUND";
		case SC_INIT_ERR_LOAD_LIBRARY_FAILED: return "SC_INIT_ERR_LOAD_LIBRARY_FAILED";
		case SC_INIT_ERR_GET_PROC_ADDRESS_FAILED: return "SC_INIT_ERR_GET_PROC_ADDRESS_FAILED";
		case SC_INIT_ERR_CALL_EXPORTED_FUNC_FAILED: return "SC_INIT_ERR_CALL_EXPORTED_FUNC_FAILED";
		case SC_INIT_ERR_QUERY_INTERFACE_FAILED: return "SC_INIT_ERR_QUERY_INTERFACE_FAILED";
		case SC_INIT_ERR_UNKNOWN_ENV: return "SC_INIT_ERR_UNKNOWN_ENV";
		case SC_INIT_ERR_INIT_CALL_FAILED: return "SC_INIT_ERR_INIT_CALL_FAILED";
		case SC_INIT_ERR_GET_SUBSYSTEM_FAILED: return "SC_INIT_ERR_GET_SUBSYSTEM_FAILED";
		case SC_INIT_ERR_RGSC_NULL_AFTER_LOAD: return "SC_INIT_ERR_RGSC_NULL_AFTER_LOAD";
		case SC_INIT_ERR_CREATE_DEVICE_DX11: return "SC_INIT_ERR_CREATE_DEVICE_DX11";
		case SC_INIT_ERR_CREATE_DEVICE_DX9: return "SC_INIT_ERR_CREATE_DEVICE_DX9";
		case SC_INIT_ERR_NULL_PIPE: return "SC_INIT_ERR_NULL_PIPE";
		case SC_INIT_ERR_PIPE_CONNECT_FAILED: return "SC_INIT_ERR_PIPE_CONNECT_FAILED";
		case SC_INIT_ERR_BAD_SIGNATURE: return "SC_INIT_ERR_BAD_SIGNATURE";
		case SC_INIT_ERR_WEBSITE_FAILED_LOAD: return "SC_INIT_ERR_WEBSITE_FAILED_LOAD";
		}
		break;

	default:
		break;
	}
	//@@: } CERROR_GETERRORCODEASSTRING


	std::stringstream ss;
	ss << std::hex << code;
	return ss.str();
}
