#include "stdafx.h"

#include <algorithm>
#include <string>

#include "rgsc_common.h"
#include "..\..\..\game\system\MachineHash.h"

#include "SparseFileManager.h"
#include "ChunkedDownloader.h"

#include "Util.h"

#define SPARSEFILE_VERBOSE_DEBUG 0

#define SPARSEFILE_USE_WRITE_LOCK 1
#define SPARSEFILE_TEST_LONG_WRITES 0

SparseFileManager::SparseFileManager() : m_fileHandle(INVALID_HANDLE_VALUE), m_partFileHandle(INVALID_HANDLE_VALUE), m_size(0), m_cancelledHashChecks(false)
{
	InitializeCriticalSection(&m_criticalSection);
	InitializeCriticalSection(&m_writeLock);

	CUtil::LockTimings::SetLockName(&m_criticalSection, "SparseFileManager::m_criticalSection");
	CUtil::LockTimings::SetLockName(&m_writeLock, "SparseFileManager::m_writeLock");
}

SparseFileManager::~SparseFileManager()
{
	DeleteCriticalSection(&m_writeLock);
	DeleteCriticalSection(&m_criticalSection);
}

bool SparseFileManager::Open(const char* fullpath, u64 size)
{
	AUTOCRITSEC(m_criticalSection);

	m_downloadedRanges.Clear();
	m_rangesInProgress.Clear();
	m_rangesOnDisk.Clear();
	m_hashRanges.clear();

	m_partFileName = fullpath;
	m_partFileName += ".part";

	m_cancelledHashChecks = false;
	m_size = size;
#if SPARSEFILE_USE_WRITE_LOCK
	m_fileHandle = CFileWrapper::CreateFileA(fullpath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_ALWAYS, FILE_FLAG_SEQUENTIAL_SCAN | FILE_FLAG_WRITE_THROUGH, NULL);
#else
	m_fileHandle = CFileWrapper::CreateFileA(fullpath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_ALWAYS, FILE_FLAG_RANDOM_ACCESS, NULL);
#endif

	if (m_fileHandle == INVALID_HANDLE_VALUE)
	{
		ERRORF("Unable to open %s", fullpath);
		CUtil::PrintSystemErrorMessage(GetLastError());
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_OPEN_SPARSE_FILE_FAILED, std::string(fullpath));

		if (!CFileWrapper::SetFileAttributesA(fullpath, FILE_ATTRIBUTE_NORMAL))
		{
			ERRORF("Unable to set attributes on %s", fullpath);
		}

		if (!CFileWrapper::DeleteFileA(fullpath))
		{
			ERRORF("Unable to delete %s", fullpath);
			CUtil::PrintSystemErrorMessage(GetLastError());
		}
		else
		{
			DISPLAYF("Deleted %s.", fullpath);
		}

		if (!CFileWrapper::DeleteFileA(m_partFileName.c_str()))
		{
			ERRORF("Unable to delete %s", fullpath);
			CUtil::PrintSystemErrorMessage(GetLastError());
		}
		else
		{
			DISPLAYF("Deleted %s.", m_partFileName.c_str());
		}

		return false;
	}

	LARGE_INTEGER filesize;
	if (GetFileSizeEx(m_fileHandle, &filesize) == 0)
		return false;

	// Ensure file is big enough
	if (filesize.QuadPart != (s64)size)
	{
		// Set size
		filesize.QuadPart = size;
		if (!SetFilePointerEx(m_fileHandle, filesize, NULL, FILE_BEGIN))
			return false;

		if (!SetEndOfFile(m_fileHandle))
			return false;

		filesize.QuadPart--;
		if (!SetFilePointerEx(m_fileHandle, filesize, NULL, FILE_BEGIN))
			return false;

		// Writing to the last byte of the file causes a zero-fill of previous unwritten bytes
		BYTE b = 0;
		DWORD bytesWritten;
		if (!WriteFile(m_fileHandle, &b, 1, &bytesWritten, NULL))
			return false;
	}

	// Open part-file
	m_partFileHandle = CFileWrapper::CreateFileA(m_partFileName.c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_ALWAYS, FILE_FLAG_RANDOM_ACCESS, NULL);

	if (m_partFileHandle == INVALID_HANDLE_VALUE)
	{
		ERRORF("Unable to open %s", m_partFileName.c_str());
		CUtil::PrintSystemErrorMessage(GetLastError());
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_OPEN_SPARSE_FILE_FAILED, m_partFileName);
		return false;
	}

	if (!ReadPartFile())
	{
		return false;
	}

	return true;
}

bool SparseFileManager::Close()
{
#if SPARSEFILE_USE_WRITE_LOCK
	CUtil::AutoCritSec lock1(m_writeLock);
#endif
	CUtil::AutoCritSec lock2(m_criticalSection);

	if (m_fileHandle != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_fileHandle);
		m_fileHandle = INVALID_HANDLE_VALUE;
	}

	if (m_partFileHandle != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_partFileHandle);
		m_partFileHandle = INVALID_HANDLE_VALUE;
	}

	// Check if we have exactly one range, which is the entire file
	if (m_downloadedRanges.Size() == 1)
	{
		const Range& onlyRange = m_downloadedRanges.GetRanges().front();

		if (onlyRange.m_offset == 0 && onlyRange.m_length >= m_size)
		{
			if (onlyRange.m_length > m_size)
			{
				DEBUGF1("Partfile range is larger than expected size!");
			}
			DEBUGF1("Deleting partfile.");
			CFileWrapper::DeleteFileA(m_partFileName.c_str());
		}
	}

	if (!m_partialRanges.empty())
	{
		DEBUGF1("Clearing up partial ranges.");
		for (size_t i = 0; i < m_partialRanges.size(); i++)
		{
			m_partialRanges[i].Cleanup();
		}
		m_partialRanges.clear();
	}

	return true;
}

bool SparseFileManager::WriteToMemory(const char* data, u64 data_len, u64 offset)
{
	AUTOCRITSEC(m_criticalSection);

	if (!ChunkedDownloader::IsDownloadInProgress())
	{
		DEBUGF1("Attempting to verify and write after download ended.");
		return false;
	}

	RAD_TELEMETRY_ZONE("VerifyAndWrite");

	m_downloadedRanges.AddRange(Range(offset, data_len));

	{
		RAD_TELEMETRY_ZONE("VerifyAndWrite - write");

		for (u64 start = 0; start < data_len;)
		{
			u64 subrangeOffset = start + offset;
			u64 subrangeLength = data_len - start;

			PartiallyCompleteRange* partialRange = GetPartialRangeContaining(subrangeOffset);

			if (!partialRange)
			{
				DEBUGF1("No partial range available for data at %llu!", start);

				Write(data + start, subrangeLength, subrangeOffset);
				return false;
			}

			u64 availableSpace = partialRange->GetRange().m_range.GetEnd() - subrangeOffset;

			u64 copyLength = std::min<u64>(availableSpace, subrangeLength);

	#if SPARSEFILE_VERBOSE_DEBUG
			DEBUGF3("Copying from %llu - %llu (%llu - %llu of total) to partial range.", start, start + copyLength-1, subrangeOffset, subrangeOffset + subrangeLength-1);
	#endif

			partialRange->WriteData(subrangeOffset, copyLength, &(data[start]));

			start += copyLength;
		}

	}


#if !RSG_FINAL
	static int partialRangesCount = 0;
	if (m_partialRanges.size() != partialRangesCount)
	{
		partialRangesCount = (int)m_partialRanges.size();
		DEBUGF1("Partial ranges count: %d", partialRangesCount);
	}
#endif

	// Check for complete ranges
	for (int i = (int)m_partialRanges.size()-1; i >= 0; i--)
	{
		PartiallyCompleteRange& partialRange = m_partialRanges[i];
		if (partialRange.IsComplete())
		{
			return true;
		}
	}

	return false;
}

bool SparseFileManager::VerifyAndWrite()
{
#if !SPARSEFILE_USE_WRITE_LOCK
	AUTOCRITSEC(m_criticalSection);
#endif

	bool verifiedOkay = true;

	RAD_TELEMETRY_ZONE("VerifyAndWrite - verify");

	PartiallyCompleteRange localRange;
	while (FindAndRemoveCompleteRange(localRange, verifiedOkay))
	{
		Write(localRange.GetData(), localRange.GetRange().m_range.m_length, localRange.GetRange().m_range.m_offset);
		localRange.Cleanup();
	}

	return verifiedOkay;
}

bool SparseFileManager::FindAndRemoveCompleteRange(PartiallyCompleteRange& out_completeRange, bool& out_verifiedOkay)
{
	AUTOCRITSEC(m_criticalSection);

	for (size_t i = 0; i < m_partialRanges.size(); i++)
	{
		RAD_TELEMETRY_ZONE("VerifyAndWrite - verify range");
		PartiallyCompleteRange& partialRange = m_partialRanges[i];
		if (partialRange.IsComplete())
		{
#if SPARSEFILE_VERBOSE_DEBUG
			DEBUGF3("Range %llu - %llu (%llu bytes) is complete.", partialRange.GetRange().m_range.m_offset, partialRange.GetRange().m_range.GetEnd()-1, partialRange.GetRange().m_range.m_length);
#endif
			if (partialRange.CheckHash())
			{
				// Move to local copy
				out_completeRange.MoveFrom(partialRange);

				// Clean up the old one
				partialRange.Cleanup();
				m_partialRanges.erase(m_partialRanges.begin() + i);

				return true;
			}
			else
			{
				DEBUGF1("Hash check failed on partial range!");
				out_verifiedOkay = false;
				m_downloadedRanges.RemoveRange(partialRange.GetRange().m_range);
			}
		}
	}

	return false;
}

bool SparseFileManager::Write(const char* data, u64 data_len, u64 offset)
{
#if SPARSEFILE_USE_WRITE_LOCK
	CUtil::AutoCritSec lock1(m_writeLock);
#endif

#if SPARSEFILE_VERBOSE_DEBUG
	DEBUGF3("Writing %llu - %llu to disk.", offset, offset + data_len - 1);
#endif


	if (m_fileHandle == INVALID_HANDLE_VALUE)
	{
		ERRORF("Invalid file handle on write!");
		return false;
	}

	LARGE_INTEGER lint;
	lint.QuadPart = offset;
	if (!SetFilePointerEx(m_fileHandle, lint, NULL, FILE_BEGIN))
	{
		ERRORF("Unable to move file pointer!");
		return false;
	}

	Range range;
	range.Set(offset, data_len);

#if SPARSEFILE_TEST_LONG_WRITES
	static int writeCount = 0;
	writeCount++;

	if (writeCount % 200 == 0)
		Sleep(30000);
	else
		Sleep(50);
	
#endif

	while (data_len > 0)
	{
		DWORD bytesToWrite = (DWORD)std::min<u64>(0xFFFFFFFF, data_len);
		DWORD bytesWritten;
#if !RSG_FINAL
		u64 writeStart = CUtil::CurrentTimeMillis();
#endif
		if (!WriteFile(m_fileHandle, data, bytesToWrite, &bytesWritten, NULL) || bytesWritten != bytesToWrite)
		{
			if (bytesWritten != bytesToWrite)
				ERRORF("Write failed: %lu written, out of %lu", bytesWritten, bytesToWrite);
			else
			{
				ERRORF("Write failed!");
				CUtil::PrintSystemErrorMessage(GetLastError());
			}
			return false;
		}
#if !RSG_FINAL
		u64 elapsed = CUtil::CurrentTimeMillis() - writeStart;
		if (elapsed > 100)
		{
			DEBUGF3("Wrote %lu bytes in %llu ms (%llu MB/s)", bytesWritten, elapsed, ((u64)bytesWritten * 1000ULL) / (elapsed * 1024ULL * 1024ULL));
		}
		else
		{
			DEBUGF3("Wrote %lu bytes in %llu ms", bytesWritten, elapsed);
		}
#endif
		data_len -= bytesWritten;
		data += bytesWritten;
	}

	CUtil::AutoCritSec lock2(m_criticalSection);

	m_rangesOnDisk.AddRange(range);
	WritePartFile();

	return true;
}

bool SparseFileManager::Read(char* data, u64 data_len, u64 offset) const
{
	AUTOCRITSEC(m_criticalSection);

	if (m_fileHandle == INVALID_HANDLE_VALUE)
		return false;

	LARGE_INTEGER lint;
	lint.QuadPart = offset;
	if (!SetFilePointerEx(m_fileHandle, lint, NULL, FILE_BEGIN))
		return false;

	while (data_len > 0)
	{
		DWORD bytesToRead = (DWORD)std::min<u64>(0xFFFFFFFF, data_len);
		DWORD bytesRead;
		if (!ReadFile(m_fileHandle, data, bytesToRead, &bytesRead, NULL) || bytesRead != bytesToRead)
			return false;

		data_len -= bytesRead;
		data += bytesRead;
	}

	return true;
}


bool SparseFileManager::ReadHashFile(const char* fullpath)
{
	AUTOCRITSEC(m_criticalSection);

	HANDLE fileHandle = CFileWrapper::CreateFileA(fullpath, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_RANDOM_ACCESS, NULL);
	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		return false;
	}

	LARGE_INTEGER lint;
	lint.QuadPart = 0;

	m_hashRanges.clear();

	while (true)
	{
		Sha256Range hashRange;
		DWORD bytesToRead = sizeof(hashRange);
		DWORD bytesRead;
		if (!ReadFile(fileHandle, (void*)&hashRange, bytesToRead, &bytesRead, NULL) || bytesRead != bytesToRead)
		{
			break;
		}

		m_hashRanges.push_back(hashRange);
	}

	CloseHandle(fileHandle);

	return true;
}

bool SparseFileManager::CheckHashes(u64* out_badBytes)
{
	AUTOCRITSEC(m_criticalSection);

	*out_badBytes = 0;

	// No data or hashes - nothing to fail, so return true
	if (m_downloadedRanges.IsEmpty() || m_hashRanges.empty())
		return true;

	bool allHashesOkay = true;

	for (int i = 0; i < m_hashRanges.size(); )
	{
		// Ignore incomplete ranges
		if (!IsRangeComplete(m_hashRanges[i].m_range))
		{
			i++;
		}
		else
		{
			// Compute the hash from disk and compare with the expected hash
			DEBUGF1("Checking hash (%llu-%llu)...", m_hashRanges[i].m_range.m_offset, m_hashRanges[i].m_range.GetEnd());
			if (!VerifySha256(m_hashRanges[i].m_hash, m_hashRanges[i].m_range))
			{
				WARNINGF("Hash check failed!");

				allHashesOkay = false;

				if (!m_cancelledHashChecks)
				{
					// Add to failed bytes
					*out_badBytes += m_hashRanges[i].m_range.m_length;

					// Remove bad section from downloaded ranges
					RemoveRange(m_hashRanges[i].m_range);
				}
				else
				{
					// Don't carry on if cancelled
					break;
				}
			}
			else
			{
				DEBUGF1("Hash check succeeded.");

				// Remove completed hash (no need to increment i)
				m_hashRanges.erase(m_hashRanges.begin() + i);
			}
		}
	}

	//@@: location SPARSEFILEMANAGER_CHECKHASHES_ALL_OKAY
	return allHashesOkay;
}

bool SparseFileManager::ReadPartFile()
{
	AUTOCRITSEC(m_criticalSection);

	DEBUGF3("Reading partfile...");

	LARGE_INTEGER lint;
	lint.QuadPart = 0;

	if (!SetFilePointerEx(m_partFileHandle, lint, NULL, FILE_BEGIN))
		return false;

	m_downloadedRanges.Clear();
	m_rangesOnDisk.Clear();

	while (true)
	{
		Range range;
		DWORD bytesToRead = sizeof(Range);
		DWORD bytesRead;
		if (!ReadFile(m_partFileHandle, (void*)&range, bytesToRead, &bytesRead, NULL) || bytesRead != bytesToRead)
		{
			break;
		}

		m_downloadedRanges.AddRange(range);
		m_rangesOnDisk.AddRange(range);
	}

	return true;
}

void SparseFileManager::AddRange(Range range)
{
	AUTOCRITSEC(m_criticalSection);

	m_downloadedRanges.AddRange(range);
}

void SparseFileManager::RemoveRange(Range removalRange)
{
	AUTOCRITSEC(m_criticalSection);

	m_downloadedRanges.RemoveRange(removalRange);
}

bool SparseFileManager::WritePartFile()
{
	AUTOCRITSEC(m_criticalSection);

	LARGE_INTEGER lint;
	lint.QuadPart = 0;

	if (!SetFilePointerEx(m_partFileHandle, lint, NULL, FILE_BEGIN))
	{
		ERRORF("Failed to update part-file!");
		return false;
	}

	for (int i = 0; i < m_rangesOnDisk.Size(); i++)
	{
		Range range = m_rangesOnDisk.GetRanges()[i];
		DWORD bytesToWrite = sizeof(Range);
		DWORD bytesWritten;
		if (!WriteFile(m_partFileHandle, (void*)&range, bytesToWrite, &bytesWritten, NULL) || bytesWritten != bytesToWrite)
		{
			ERRORF("Failed to update part-file!");
			return false;
		}
	}

	if (!SetEndOfFile(m_partFileHandle))
	{
		ERRORF("Failed to update part-file!");
		return false;
	}

	return true;
}

bool SparseFileManager::GetNextRange(Range* out_range, u64 maximumSize) const
{
	AUTOCRITSEC(m_criticalSection);

	if (!out_range)
		return false;

	// Find first opening
	u64 offset = 0;
	bool moved = true;
	while (moved)
	{
		moved = false;
		for (int i = 0; i < m_downloadedRanges.Size(); i++)
		{
			const Range& range = m_downloadedRanges.GetRanges()[i];

			// Move along any overlapping ranges
			if (range.m_offset <= offset && range.GetEnd() > offset)
			{
				offset = range.GetEnd();

				// Repeat, as the order of ranges is not guaranteed
				moved = true;
			}
		}

		// Do the same for ranges in progress
		for (int i = 0; i < m_rangesInProgress.Size(); i++)
		{
			const Range& range = m_rangesInProgress.GetRanges()[i];

			// Move along any overlapping ranges
			if (range.m_offset <= offset && range.GetEnd() > offset)
			{
				offset = range.GetEnd();

				// Repeat, as the order of ranges is not guaranteed
				moved = true;
			}
		}
	}

	// Check if there's none
	if (offset >= m_size)
		return false;

	// Find the end of this gap, starting with the maximum length
	u64 end = std::min<u64>(offset + maximumSize, m_size);

	// Only a single pass is needed for each list, as the minimum overlapping range start will be found regardless of order
	for (int i = 0; i < m_downloadedRanges.Size(); i++)
	{
		const Range& range = m_downloadedRanges.GetRanges()[i];

		if (range.m_offset > offset && range.m_offset < end)
		{
			end = range.m_offset;
		}
	}

	for (int i = 0; i < m_rangesInProgress.Size(); i++)
	{
		const Range& range = m_rangesInProgress.GetRanges()[i];

		if (range.m_offset > offset && range.m_offset < end)
		{
			end = range.m_offset;
		}
	}
	
	out_range->m_offset = offset;
	out_range->m_length = end - offset;

	return true;
}

u64 SparseFileManager::GetTotalSizeWritten() const
{
	AUTOCRITSEC(m_criticalSection);

	// This assumes no ranges overlap - they should have been merged together by AddRange
	u64 sum = 0;
	for (int i = 0; i < m_downloadedRanges.Size(); i++)
	{
		sum += m_downloadedRanges.GetRanges()[i].m_length;
	}

	return sum;
}

bool SparseFileManager::IsFileComplete() const
{
	Range wholeFile(0, m_size);
	return IsRangeComplete(wholeFile);
}

bool SparseFileManager::IsRangeComplete(const Range& queryRange) const
{
	AUTOCRITSEC(m_criticalSection);

	u64 offset = queryRange.m_offset;

	// Try and traverse the offset to (or past) the end of the passed-in range
	bool moved = true;
	while (moved && offset < queryRange.GetEnd())
	{
		moved = false;
		for (int i = 0; i < m_downloadedRanges.Size(); i++)
		{
			const Range& range = m_downloadedRanges.GetRanges()[i];

			// If the range overlaps, move to its end
			if (range.m_offset <= offset && range.GetEnd() > offset)
			{
				moved = true;
				offset = range.GetEnd();
			}
		}
	}

	return offset >= queryRange.GetEnd();
}

void SparseFileManager::SetRangeInProgress(const Range& range, bool inProgress)
{
	AUTOCRITSEC(m_criticalSection);

	if (inProgress)
	{
		m_rangesInProgress.AddRange(range);
	}
	else
	{
		m_rangesInProgress.RemoveRange(range);
	}
}

bool SparseFileManager::ClearRangesInProgess()
{
	AUTOCRITSEC(m_criticalSection);

	bool ret = !m_rangesInProgress.IsEmpty();
	m_rangesInProgress.Clear();

	return ret;
}

bool SparseFileManager::ComputeSha256(char out_hash[32], const Range& range) const
{
	const u64 MAX_CHUNK_SIZE = 4 * 1024 * 1024;
	char* buffer = new char[MAX_CHUNK_SIZE];

	SHA256_CTX ctx;
	SHA256_Init(&ctx);

	for (u64 offset = 0; offset < range.m_length; offset += MAX_CHUNK_SIZE)
	{
		// Read the maximum chunk size, or the remaining bytes, whichever is smaller
		u64 currentChunkSize = std::min<u64>(MAX_CHUNK_SIZE, range.m_length - offset);

		// Try and read the chunk
		if (m_cancelledHashChecks || !Read(buffer, currentChunkSize, range.m_offset + offset))
		{
			ERRORF("Can't read range %llu -> %llu (%llu bytes)!", range.m_offset + offset, range.m_offset + offset + currentChunkSize, currentChunkSize);
			delete[] buffer;
			return false;
		}
		else
		{

#if RSG_DEBUG
			// Debug info to help work out why hashes fail
			if (offset == 0)
			{
				std::string hexString;
				CUtil::BytesToHexString((u8*)buffer, (u32)std::min<u64>(range.m_length, 10), hexString);
				DEBUGF3("Hashing bytes beginning: %s", hexString.c_str());
			}
#endif
			// Update the SHA digest with this chunk
			SHA256_Update(&ctx, buffer, currentChunkSize);
		}
	}

	//@@: location SPARSEFILEMANAGER_COMPUTESHA256_CALL_HASH
	SHA256_Final((u8*)out_hash, &ctx);

	delete[] buffer;

	return true;
}

bool SparseFileManager::VerifySha256(const char hash[32], const Range& range) const
{
	const u32 DIGEST_LEN = 32;
	char computedHash[DIGEST_LEN];

	if (!ComputeSha256(computedHash, range))
	{
		ERRORF("Failed to compute hash!");
		return false;
	}
	//@@: location SPARSEFILEMANAGER_VERIFYSHA256
	if (memcmp(computedHash, hash, sizeof(computedHash)) == 0)
	{
		std::string hexstring;
		CUtil::BytesToHexString((u8*)hash, DIGEST_LEN, hexstring);
		DEBUGF3("Hashes match: %s", hexstring.c_str());
		return true;
	}
	else
	{
		ERRORF("Hashes do not match!");
		std::string hexstring;
		CUtil::BytesToHexString((u8*)hash, DIGEST_LEN, hexstring);
		ERRORF("Expecting: %s", hexstring.c_str());
		CUtil::BytesToHexString((u8*)computedHash, DIGEST_LEN, hexstring);
		ERRORF(" Computed: %s", hexstring.c_str());
		return false;
	}

}

PartiallyCompleteRange* SparseFileManager::GetPartialRangeContaining(u64 position)
{
	// Look for a range in progress
	for (size_t i = 0; i < m_partialRanges.size(); i++)
	{
		PartiallyCompleteRange* partialRange = &m_partialRanges[i];

		if (partialRange->GetRange().m_range.Contains(position))
		{
			return partialRange;
		}
	}

	DEBUGF1("No in-progress partial range found containing %llu, searching remaining hashes...", position);

	// None found, look through the remaining hashable ranges
	for (size_t i = 0; i < m_hashRanges.size(); i++)
	{
		Sha256Range& range = m_hashRanges[i];

		if (range.m_range.Contains(position))
		{
			m_partialRanges.push_back(PartiallyCompleteRange());

			DEBUGF1("Starting new partial range for %llu (%llu - %llu inclusive)", position, range.m_range.m_offset, range.m_range.GetEnd()-1);

			PartiallyCompleteRange* partialRange = &m_partialRanges.back();
			partialRange->Init(range);
			return partialRange;
		}
	}

	// Shouldn't happen.
	DEBUGF1("No partial range found containing %llu!", position);
	return NULL;
}
