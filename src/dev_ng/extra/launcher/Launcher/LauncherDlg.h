// LauncherDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "afxbutton.h"

#include "DoubleBufferedEdit.h"
#include "DxSurface9.h"
#include "SocialClub.h"
#include "Fsm.h"
#include "resource.h"
#include "Spinner.h"
#include "Entitlement.h"

#define RSG_FINAL_FAUX 1

// CLauncherDlg dialog
class CLauncherDlg : public CWnd
{
	DECLARE_DYNAMIC(CLauncherDlg);
	friend class CLauncherDlgAutoProxy;
	friend class CTest;

// Construction
public:
	CLauncherDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CLauncherDlg();

	static CLauncherDlg* GetLastInstance();

	virtual INT_PTR DoModal();
	void CreateWindowNoResource(WNDCLASS wndclass);
	RECT GetControlRect(DWORD id);

// Dialog Data
	enum { IDD = IDD_LAUNCHER_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	

// Implementation
protected:
	HICON m_hIcon;
	CBrush m_brush;
	CFont m_statusFont;
	CFont m_detailFont;
	CFont m_buttonFont;
	CFont m_versionFont;
	CImage m_imageBackground;

	CMFCButton m_button[3];
	CDoubleBufferedEdit m_detailText, m_detailText2;
	CDoubleBufferedEdit m_status;
	CStatic m_version;
	CSpinner m_spinner;
	DxSurface* m_surface;
	SocialClub m_socialclub;

	POINT m_lastMouseDragScreenCoords;
	bool m_lastDragCoordsValid;
	bool m_mouseCaptured;

	int m_windowWidth, m_windowHeight;
	float m_uiScale;

	bool m_initialised;

	void SkinButton(CMFCButton& button);

	LRESULT OnSetState(WPARAM wParam, LPARAM lParam);
	LRESULT OnSetProgress(WPARAM wParam, LPARAM lParam);
	LRESULT OnSetStatus(WPARAM wParam, LPARAM lParam);
	LRESULT OnSetStatusWithParams(WPARAM wParam, LPARAM lParam);
	LRESULT OnUpdateSocialClub(WPARAM wParam, LPARAM lParam);
	LRESULT OnShowCodeRedemptionUI(WPARAM wParam, LPARAM lParam);
	LRESULT OnShowSigninUI(WPARAM wParam, LPARAM lParam);
	LRESULT OnRequestShutdown(WPARAM wParam, LPARAM lParam);
	LRESULT OnActivationSuccess(WPARAM wParam, LPARAM lParam);
	LRESULT OnShowActivationError(WPARAM wParam, LPARAM lParam);
	LRESULT OnHandleControllerEvent(WPARAM wParam, LPARAM lParam);
	LRESULT OnSetLauncherVisibility(WPARAM wParam, LPARAM lParam);
	LRESULT OnUpdateStatus(WPARAM wParam, LPARAM lParam);
	LRESULT OnNextState(WPARAM wParam, LPARAM lParam);
	LRESULT OnSetControlsVisibility(WPARAM wParam, LPARAM lParam);
	LRESULT OnMultipleLaunchesAttempted(WPARAM wParam, LPARAM lParam);
	LRESULT OnSteamOfflineMode(WPARAM wParam, LPARAM LPARAM);
	

	void OnLeftButton();
	void OnMiddleButton();
	void OnRightButton();

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnClose();
	virtual void OnOK();
	DECLARE_MESSAGE_MAP()

	CFsm m_fsm;

	void HideAllControls(bool includeDxSurface = true);

	void CheckFirstRun();

	BOOL CheckWindowsVersion();
	BOOL Check64Bit();
	BOOL CheckLaunchedFromBootstrap();

	void Shutdown();

public:
	CProgressCtrl m_progress;

	class ButtonParams
	{
	public:
		ButtonParams(UINT title, void (*fn)(void*), void* param);
		ButtonParams(LPCWSTR title, void (*fn)(void*), void* param);
		ButtonParams(const std::wstring& title, void (*fn)(void*), void* param);
		ButtonParams() : m_title(L""), m_fn(NULL), m_param(NULL) {}

		void Set(const std::wstring& title, void (*fn)(void*), void* param);
		void Reset() { m_title.clear(); m_fn = NULL; m_param = NULL; }
		bool IsValid() { return m_title.size() > 0 && m_fn != NULL; }
		void Call();
		const std::wstring& GetTitle() { return m_title; }

		bool operator!=(const ButtonParams& other) const { return !(*this == other); }
		bool operator==(const ButtonParams& other) const { return m_title == other.m_title && m_fn == other.m_fn && m_param == other.m_param; }
		void operator=(const ButtonParams& other) { m_title = other.m_title; m_fn = other.m_fn; m_param = other.m_param; }

	private:
		
		std::wstring m_title;
		void (*m_fn)(void*);
		void* m_param;
	};

	void SetBackgroundImage();
	void SetLauncherVisibility(bool visible, bool animate = true);

	void SetTextWithSpinner(UINT identifier);
	void SetTextWithSpinner(const std::wstring& text);
	void SetTextWithSpinner(UINT heading, UINT text);
	void SetTextWithSpinner(UINT identifier, ButtonParams rightButton);
	void SetTextWithButton(UINT heading, const std::wstring& text, ButtonParams rightButton);
	void SetTextWithButtons(UINT heading, UINT text, ButtonParams leftButton, ButtonParams rightButton);
	void SetTextWithButtons(UINT heading, const std::wstring& text, ButtonParams leftButton, ButtonParams rightButton);
	void SetTextWithButtons(UINT heading, UINT text, ButtonParams leftButton, ButtonParams middleButton, ButtonParams rightButton);
	void SetTextWithButtons(UINT heading, const std::wstring& text, ButtonParams leftButton, ButtonParams middleButton, ButtonParams rightButton);
	void SetProgressBar(UINT heading, UINT text);
	void SetProgressBar(UINT heading, const std::wstring& text);
	void SetProgressBar(UINT heading, UINT text, ButtonParams leftButton);
	void SetProgressBar(UINT heading, const std::wstring& text, ButtonParams leftButton);
	void SetStatusTextSafe(const std::wstring& text);

	static void ProgressCancelCallback(void*);

	void InitializeEntitlementChecker();
	void SetSocialClubHotkey(bool bEnable);
	void SetSocialClubVisible();
	void ShutdownSocialClub();
	void ShowCodeRedemptionUI();
	void OnExitCodeError();
	void UpdateSocialClubTicket(rgsc::RockstarId rockstarId, std::string base64Ticket);
	void GameSignOutEvent();
	void GameSignInEvent(rgsc::RockstarId rockstarId);
	void CreateSignInTransferFile();

	CFsm::FsmState GetCurrentState();

	void TextClicked();

	float GetUiScale();

	void Exit();

	static BOOL PostMessageSafe(UINT message, WPARAM wParam = 0, LPARAM lParam = 0);

	bool IsSocialClubInitialized() { return m_bSocialClubInitialized; }
	u64 GetCloudSaveBanTime();
	rgsc::RgscGamepad* GetScuiPad(unsigned index) const;

	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void ShowFolderPicker(const std::wstring& startingLocation, std::wstring& out_pickedLocation);

	rgsc::RockstarId GetSignedInRockstarId();
	bool IsSocialClubOnline();

	void UpdateVersionString();

	struct SetStatusParams
	{
		bool force;
		void (*cancelFn)(void*);
		std::string status;
	};


	enum Message
	{
		eSetState = WM_APP,
		eSetProgress,
		eSetStatusAndDelete,
		eSetStatusWithParamsAndDelete,
		eUpdateSocialClub,
		eShowCodeRedemptionUI,
		eShowSigninUI,
		eShutdown,
		eShowActivationSuccess,
		eShowActivationError,
		eHandleControllerEvent,
		eSetLauncherVisibility,
		eUpdateStatus,
		eNextState,
		eSetControlsVisibility,
		eMultipleLaunchesAttempted,
		eSteamOfflineMode
	};

private:
	struct UiState
	{
		UiState() : progressVisible(false), spinnerVisible(false), progressPercent(0) {}
		std::wstring heading;
		std::wstring status;
		bool progressVisible, spinnerVisible;
		float progressPercent;
		ButtonParams buttons[3];

		bool operator==(const UiState& other)
		{
			return heading == other.heading
				&& status == other.status
				&& progressPercent == other.progressPercent
				&& spinnerVisible == other.spinnerVisible
				&& progressPercent == other.progressPercent
				&& buttons[0] == other.buttons[0]
				&& buttons[1] == other.buttons[1]
				&& buttons[2] == other.buttons[2];
		}

		void operator=(const UiState& other)
		{
			heading = other.heading;
			status = other.status;
			progressPercent = other.progressPercent;
			spinnerVisible = other.spinnerVisible;
			progressPercent = other.progressPercent;
			buttons[0] = other.buttons[0];
			buttons[1] = other.buttons[1];
			buttons[2] = other.buttons[2];
		}
	} m_uiShadowCopy, m_uiPreviousShadowCopy;

	void RefreshUiFromShadowCopy(bool force = false);
	void RefreshUsingMFC();
	void RefreshUsingSCUI(bool force);
	bool m_bSocialClubInitialized;
	bool m_bVisible;
	bool m_bShownQuitConfirm;

	bool LoadImageFromResource(CImage& image, int id);

	void GetVersionString(std::string& out_string);
	void SetVersionString();

private:
	static UINT ControllerMonitorThread(LPVOID param);
	CWinThread* m_controllerThread;

	enum ControllerEvent
	{
		CONTROLLER_LEFT,
		CONTROLLER_RIGHT,
		CONTROLLER_UP,
		CONTROLLER_DOWN,
		CONTROLLER_ACCEPT,
		CONTROLLER_CANCEL
	};

private:
	CRITICAL_SECTION statusUpdateLock;
	std::wstring statusUpdateString;
	std::string m_versionString;
};
