#pragma once

#include "DxSurface.h"

#if !RSG_BOOTSTRAP
#include "Entitlement.h"

#include "activation_interface.h"
#include "cloudsave_interface.h"
#include "configuration_interface.h"
#include "commerce_interface.h"
#include "delegate_interface.h"
#include "file_system_interface.h"
#include "pad_interface.h"
#include "patching_interface.h"
#include "profiles_interface.h"
#include "rgsc_common.h"
#include "rgsc_interface.h"
#include "rgsc_ui_interface.h"
#include "titleid_interface.h"
#include "tasks_interface.h"
#include "telemetry_interface.h"

#include "Error.h"

#include <string>
#include <vector>
#include <queue>

class CLauncherDlg;

class SocialClub : public IPaintDelegate, public rgsc::IRgscDelegateLatestVersion
{
public:
	SocialClub();
	virtual ~SocialClub();

	// Initialization
	bool Initialize(DxSurface* surface, HWND parentWindow);
	void Uninitialize();
	bool IsInitialized();

	// Events
	virtual void OnPaint();
	virtual void OnLostDevice();
	virtual void OnResetDevice();

	// Entitlement
	static void OnBladeUIActivationCodeSubmit( const int contentId, const char* activationCode, const rgsc::IActivationLatestVersion::ActivationAction action );
#if !RSG_PRELOADER_LAUNCHER
	void RunEntitlementCheck();
#endif
	void ShowCodeRedemptionUI();
	void ShowSigninUI();
	void ShowActivationSuccess(int contentId);
	void ShowActivationError(int errorCode);
	void SetSocialClubHotkey(bool bEnable);
	void OnExitError();
	void UpdateSocialClubTicket(rgsc::RockstarId rockstarId, std::string base64Ticket);
	void GameSignOutEvent();
	void GameSignInEvent(rgsc::RockstarId id);
	void CreateSignInTransferFile();
	rgsc::RgscGamepad* GetScuiPad(unsigned index) const;

	static bool IsBanned() { return sm_bSocialClubBanned; }
	static u64 GetBanEndTime() { return sm_uSocialClubBanEndDate; }

	// New UI
	void SendMessageToScui(const char* messageJson);
	bool IsReadyForScuiMessages();

	// Shutdown
	void RequestShutdown();
	void Join();

	rgsc::RockstarId GetRockstarId();
	rgsc::ITaskManagerLatestVersion* GetTaskManager();
	rgsc::IActivationLatestVersion* GetActivationSystem();
	rgsc::ICommerceManagerV4* GetCommerceManager();
	static EntitlementManager* GetEntitlementManager();
	rgsc::IProfileManagerV2* GetProfileManager();
	rgsc::ITelemetryV4* GetTelemetryManager();
	rgsc::ICloudSaveManagerV4* GetCloudSaveManager();
	rgsc::IFileSystemV3* GetFileSystem();
	rgsc::IGamepadManagerV3* GetGamepadManager();

	HWND GetParentWnd() { return m_hParentWnd; }
	bool IsUiShowing() { return m_bWasUIShowing; }
	bool IsWaitingToShowCodeRedemptionUI() { return sm_bShowActivationUi; }
	bool IsTextboxFocussed() { return m_bTextboxHasFocus; }

	void SetGameVersionHeaderInfo(u32 version);

	bool IsOnline();

private:
	DxSurface* m_surface;
	HMODULE m_hRgscDll;
	HWND m_hParentWnd;
	CWinThread* m_pThread;

	rgsc::IRgscV7* m_rgsc;
	rgsc::IRgscUiV3* m_rgscUI;
	rgsc::IProfileManagerV2* m_profileManager;
	rgsc::IActivationV2* m_ActivationSystem;
	rgsc::ITaskManagerV1* m_TaskManager;
	rgsc::ICommerceManagerV4* m_CommerceManager;
	rgsc::ITelemetryV4* m_Telemetry;
	rgsc::ICloudSaveManagerV4* m_CloudSaveManager;
	rgsc::IFileSystemV3* m_FileSystem;
	rgsc::IGamepadManagerV3* m_GamepadManager;

	void ThreadEntryPoint();

	bool CreateSocialClubPath(wchar_t (&path)[MAX_PATH]);
	bool LoadDll();
#if RSG_STEAM_LAUNCHER
	void InstallDll();
#endif
	bool InitDll();
	void Update();
	void DoShutdown();
	bool PrePopulateKey();

	void OnUIShown();
	void OnUIHidden();
	void OnSignIn();
	void OnSignOut();

	bool m_bInitialized;
	bool m_bWasUIShowing;
	bool m_bWasSignedIn;
	bool m_bWaitingForSignIn;
	bool m_bWaitingForActivation;
	bool m_bWaitingForTicketRefresh;
	bool m_bSignInScuiEvent;
	bool m_bSignInNotificationEvent;
	bool m_bTextboxHasFocus;
	static int sm_CurrentContentId;
	static bool sm_bShowActivationUi;
	static bool sm_bSocialClubBanned;
	static u64 sm_uSocialClubBanEndDate;
	u64 m_lastUpdateTime;

	InitErrorCodes m_InitializationErrorCode;
	static EntitlementManager sm_EntitlementManager;
	CRITICAL_SECTION m_CriticalSection;
	std::vector<std::string> m_MessagesForScui;

	std::vector<char*> m_Argvmb;
	wchar_t** m_Argvw;

	struct GameEvent
	{
		enum GameEventType
		{
			TICKET_CHANGED,
			SIGNED_OUT,
			SIGNED_IN
		};

		GameEvent(GameEventType eventType, rgsc::RockstarId rockstarId, std::string base64Ticket)
		{
			EventType = eventType;
			RockstarId = rockstarId;
			Ticket = base64Ticket;
		}

		GameEventType EventType;
		std::string Ticket;
		rgsc::RockstarId RockstarId;
	};

	void AddGameEvent(GameEvent& e);
	void ProcessGameEvents();
	std::queue<GameEvent> m_GameEventQueue;

	static std::string m_headerInfo;
	static const char* GetHeaderInfo();

public:
	rgsc::RGSC_HRESULT RGSC_CALL QueryInterface(rgsc::RGSC_REFIID riid, void** ppvObject);
	void RGSC_CALL Output(OutputSeverity severity, const char* msg);
	bool RGSC_CALL GetStatsData(char** data);
	void RGSC_CALL FreeStatsData(const char* data);
	void RGSC_CALL SetTextBoxHasFocus(const bool hasFocus);
	void RGSC_CALL SetTextBoxHasFocusV2(const bool hasFocus, const char* prompt, const char* text, const bool isPassword, const u32 maxNumChars);
	void RGSC_CALL UpdateSocialClubDll(const char* commandLine);
	void RGSC_CALL HandleNotification(const rgsc::NotificationType id, const void* param);
};
#endif

