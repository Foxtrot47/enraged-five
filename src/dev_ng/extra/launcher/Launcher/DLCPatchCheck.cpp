#include "stdafx.h"

#include "Globals.h"
#include "Config.h"
#include "OnlineConfig.h"
#include "DLCPatchCheck.h"

#include "Localisation.h"
#include "Channel.h"
#include "Localisation.h"

#include <sstream>

DLCPatchCheck::DLCPatchCheck(const std::string& /*rockstarId*/)
{
	//@@: location DLCPATCHCHECK_CONSTRUCTOR
	CUtil::GetServerAndDocpathForEnv(m_server, m_documentPath, Constants::DLCVersionPathProd);
}

DLCPatchCheck::~DLCPatchCheck()
{

}

void DLCPatchCheck::UrlEncode(const std::string& in, std::string& out)
{
	//@@: location DLCPATCHCHECK_URLENCODE
	std::stringstream strm;
	strm << std::hex;
	for (size_t i = 0; i < in.size(); i++)
	{
		char c = in[i];
		strm << "%" << (int)c;
	}

	out.assign(strm.str());
}

void DLCPatchCheck::ParseXML()
{
	//@@: location DLCPATCHCHECK_PARSEXML_CREATE_DOC
	TiXmlDocument doc(m_localXmlFile.c_str());
	//@@: range DLCPATCHCHECK_PARSEXML {
	doc.LoadFile();

	TiXmlHandle docHandle(&doc);

	TiXmlElement* sku = docHandle.FirstChild("RockstarPatches").FirstChild("Skus").FirstChildElement("Sku").ToElement();

	std::string currentSkuName = CLocalisation::Instance().GetProductSku();

	for (; sku; sku = sku->NextSiblingElement())
	{
		const char* skuName = sku->Attribute("id");
		if (!skuName || currentSkuName.compare(skuName) != 0)
		{
			continue;
		}

		//@@: location DLCPATCHCHECK_PARSEXML_GET_SKU_HANDLE
		TiXmlHandle skuHandle(sku);
		TiXmlElement* release = skuHandle.FirstChild("Releases").FirstChildElement("Release").ToElement();
		if (!release)
		{
			ERRORF("Sku with no releases!");
			continue;
		}

		for (; release; release = release->NextSiblingElement("Release"))
		{
			TiXmlElement* patchNode = release->FirstChildElement("Patch");

			if (!patchNode)
			{
				ERRORF("Release node with no patch node.");
				continue;
			}

			Patch patch;
			//@@: location DLCPATCHCHECK_PARSEXML_POPULATE_PATCH
			PopulatePatch(patch, skuName, release, patchNode);

			patch.PatchType = PT_DLC;
			//@@: location DLCPATCHCHECK_PARSEXML_PUSH_BACK
			m_patches.push_back(patch);
		}
	}
	//@@: } DLCPATCHCHECK_PARSEXML

	//@@: location DLCPATCHCHECK_PARSEXML_RETURN
	return;

}
