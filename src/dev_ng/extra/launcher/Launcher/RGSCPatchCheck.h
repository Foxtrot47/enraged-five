#pragma once

#include "IPatchCheck.h"

class RGSCPatchCheck : public IPatchCheck
{
public:
	RGSCPatchCheck(void);
	~RGSCPatchCheck(void);

	static Version GetRgscDllVersion();
	virtual Version GetExecutableVersion();

	virtual void DownloadComplete(DownloadCompleteStatus status, std::string filename);
	virtual void DownloadError(const CError& errorString);

	virtual void ParseXML();
	virtual void OnAddPatch(Patch& patch);
};
