#include "stdafx.h"

#include "DxSurface9.h"
#include "Channel.h"

#include <DxErr.h>
#include <dxsdkver.h>
#include "TamperSource.h"

#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")
#pragma comment(lib,"dxerr.lib")

#define RSG_NVIDIA_SUPPORT 1

#if RSG_NVIDIA_SUPPORT

#include "nvapi.h"
#pragma comment(lib,"nvapi64.lib")

#endif

IMPLEMENT_DYNAMIC(DxSurface9, CWnd)


BEGIN_MESSAGE_MAP(DxSurface9, CWnd)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_CHAR()
END_MESSAGE_MAP()


DxSurface9::DxSurface9()
	: m_direct3D9(NULL), m_painting(false), m_bShown(false), m_delegateState(READY), m_bDisabledStereo(false)
{
}

DxSurface9::~DxSurface9()
{
	if (m_device)
	{
		unsigned long refs = m_device->Release();
		if (refs > 0)
		{
			WARNINGF("Still %ld ref(s) to m_device!");
		}
	}
	if (m_direct3D9)
	{
		unsigned long refs = m_direct3D9->Release();
		if (refs > 0)
		{
			WARNINGF("Still %ld ref(s) to m_direct3D9!");
		}
	}
}

LRESULT DxSurface9::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_KEYDOWN)
	{
		DEBUGF1("DxSurface9 Keydown: %d", wParam);
	}
	return CWnd::WindowProc(message, wParam, lParam);
}

LRESULT DxSurface9::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_KEYDOWN)
	{
		DEBUGF1("DxSurface9 Def keydown: %d", wParam);
	}
	return CWnd::DefWindowProc(message, wParam, lParam);
}

BOOL DxSurface9::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		DEBUGF1("DxSurface9 PreTranslate keydown: %d", pMsg->wParam);
		TranslateMessage(pMsg);
		DispatchMessage(pMsg);
		return TRUE;
	}
	return CWnd::PreTranslateMessage(pMsg);
}


LPDIRECT3DDEVICE9 DxSurface9::m_device = NULL;

void DxSurface9::OnPaint()
{
	DEBUGF1("DxSurface9::OnPaint()");

	PAINTSTRUCT ps;

	BeginPaint(&ps);

	EndPaint(&ps);
	
	// Moved rendering to a separate thread.
	//Render();

	// Not sure if this needs to be called.
	// Don't want any default drawing.
	//CWnd::OnPaint();
}

bool DxSurface9::Show(bool show)
{
	if (show != m_bShown)
	{
		DEBUGF3(show ? "Showing DxSurface9." : "Hiding DxSurface9.");
		ShowWindowAsync(GetSafeHwnd(), show ? SW_SHOW : SW_HIDE);
		m_bShown = show;
		return true;
	}
	else
	{
		return false;
	}
}


void DxSurface9::Render()
{
	if (m_device)
	{
		bool error = false;
#define CHECK_HRESULT(hr) do { HRESULT result = (hr); if (FAILED(result)) { ERRORF("HRESULT failed when doing " #hr " : %x", result); error = true; } } while (0);

		HRESULT cooperativeLevel = m_device->TestCooperativeLevel();

		if (m_delegateState == READY && cooperativeLevel == D3D_OK)
		{
			CHECK_HRESULT(m_device->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_ARGB(0x00,0x00,0x00,0x00), 0, 0));
			CHECK_HRESULT(m_device->BeginScene());

			try
			{
				RAD_TELEMETRY_ZONE("Draw");
				if (!m_paintDelegate)
					DrawTest();

				if (m_paintDelegate
#if !RSG_PRELOADER_LAUNCHER && ENABLE_TAMPER_ACTIONS
					&& g_flickerProb < 0.0001f
#endif
					)
#if !RSG_PRELOADER_LAUNCHER && ENABLE_TAMPER_ACTIONS
				{
					m_paintDelegate->OnPaint();
				}
				else
				{
					float r = rand() / (float)RAND_MAX;
					if (m_paintDelegate  && r > g_flickerProb)
#endif
					{
						m_paintDelegate->OnPaint();
					}
#if !RSG_PRELOADER_LAUNCHER && ENABLE_TAMPER_ACTIONS
					else
					{
						NullDraw();
					}

				}
#endif
			}
			catch (...)
			{
				ERRORF("Exception happened.");
				error = true;
			}

			{
				RAD_TELEMETRY_ZONE("EndScene");
				CHECK_HRESULT(m_device->EndScene());
			}
			{
				RAD_TELEMETRY_ZONE("Present");
				CHECK_HRESULT(m_device->Present(NULL, NULL, NULL, NULL));
			}

			cooperativeLevel = m_device->TestCooperativeLevel();
		}

		// Timer to avoid spamming too much
		static ULONGLONG lastMessageTicks = 0;
		ULONGLONG currentTicks = GetTickCount64();
		bool canEmitMessage = (currentTicks > lastMessageTicks + 5000) || (currentTicks < lastMessageTicks);
		bool emittedMessage = false;

		if (error && cooperativeLevel == D3D_OK)
		{
			if (canEmitMessage)
			{
				ERRORF("Co-operative level is OK, but an error occurred.  Upping to D3DERR_DEVICELOST.");
				emittedMessage = true;
			}

			cooperativeLevel = D3DERR_DEVICELOST;
		}

		if (m_delegateState == READY && (cooperativeLevel == D3DERR_DEVICELOST || cooperativeLevel == D3DERR_DEVICENOTRESET))
		{
			// Show a message, but not too frequently
			if (canEmitMessage)
			{
				WARNINGF("Device lost.");
				emittedMessage = true;
			}

			m_paintDelegate->OnLostDevice();
			m_delegateState = DEVICELOST;
		}

		if (cooperativeLevel == D3DERR_DEVICELOST && error)
		{
			// If we're forcing a reset due to error, advance to the next state
			if (canEmitMessage)
			{
				DEBUGF1("Attempting reset due to error.");
				emittedMessage = true;
			}
			
			cooperativeLevel = D3DERR_DEVICENOTRESET;
		}
		
		if (cooperativeLevel == D3DERR_DEVICENOTRESET && m_delegateState == DEVICELOST)
		{
			if (canEmitMessage)
			{
				WARNINGF("Resetting device...");
				emittedMessage = true;
			}
			
			HRESULT resetResult = m_device->Reset(&m_d3dpp);

			if (SUCCEEDED(resetResult))
			{
				DISPLAYF("Reset succeeded.");
				m_paintDelegate->OnResetDevice();
				m_delegateState = READY;
			}
			else if (resetResult == D3DERR_DEVICELOST)
			{
				if (canEmitMessage)
				{
					WARNINGF("Reset failed with D3DERR_DEVICELOST.");
				}
			}
			else
			{
				if (canEmitMessage)
				{
					WARNINGF("Reset failed with %X.", resetResult);
				}
			}
		}

		if (canEmitMessage && emittedMessage)
		{
			lastMessageTicks = currentTicks;
		}
	}
	else
	{
		ERRORF("Trying to render with no device.");
	}
}

BOOL DxSurface9::OnEraseBkgnd(CDC* /*pDC*/)
{
	//return CWnd::OnEraseBkgnd(pDC);
	return FALSE;
}

HRESULT DxSurface9::CreateDevice()
{
	// Wait for screen unlock
	// HACK: There is no officially-supported way of doing this
	if (GetForegroundWindow() == NULL)
	{
		WARNINGF("User's screen appears locked.");

		while (GetForegroundWindow() == NULL)
		{
			Sleep(1000);
		}
	}

	// Disable stereo
#if RSG_NVIDIA_SUPPORT
	if (NvAPI_Initialize() == NVAPI_OK)
	{
		DEBUGF1("Initialized NvAPI");
		NvU8 enabled = 0;
		if (NvAPI_Stereo_IsEnabled(&enabled) == NVAPI_OK)
		{
			if (enabled != 0)
			{
				DEBUGF1("Stereo was enabled");
				if (NvAPI_Stereo_Disable() == NVAPI_OK)
				{
					DEBUGF1("Disabled stereo");
					m_bDisabledStereo = true;
				}
				else
				{
					DEBUGF1("Unable to disable stereo");
				}
			}
			else
			{
				DEBUGF1("Stereo was disabled");
			}
		}
		else
		{
			DEBUGF1("Unable to query stereo enabled state");
		}
	}
	else
	{
		DEBUGF1("Unable to initialize NvAPI");
	}
#endif


	// Create device
	DISPLAYF("Attempting to create device...");
	if (m_window == NULL || m_hWnd == NULL)
	{
		ERRORF("No window.");
		return -1;
	}

	if (!m_direct3D9)
	{
		DISPLAYF("Attempting to initialise DX9 COM object...");
		m_direct3D9 = Direct3DCreate9(D3D_SDK_VERSION);

		if (!m_direct3D9)
		{
			ERRORF("Error! Could not create Direct3D.");
			return -2;
		}
		else
		{
			DEBUGF1("Created Direct3D.");
		}
	}

	// Calculate window size
	RECT windowRect;
	GetClientRect(&windowRect);

	int width = windowRect.right - windowRect.left, height = windowRect.bottom - windowRect.top;
	if (width <= 49 || height <= 49)
	{
		WARNINGF("Warning: Invalid DX window size.");
		width = 50;
		height = 50;
	}
	else
		DISPLAYF("Window size: %d x %d", width, height);


	// Setup present params
	ZeroMemory(&m_d3dpp, sizeof(D3DPRESENT_PARAMETERS));

	m_d3dpp.BackBufferWidth = width;
	m_d3dpp.BackBufferHeight = height;
	m_d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	m_d3dpp.BackBufferCount = 1;
	m_d3dpp.EnableAutoDepthStencil = FALSE;
	m_d3dpp.hDeviceWindow = m_window;
	m_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	m_d3dpp.Windowed = TRUE;
	m_d3dpp.FullScreen_RefreshRateInHz = 0;
	m_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

	HRESULT hr = S_OK;
	for (int i = 0; i < (1<<5); i++)
	{
		DWORD behaviourflags = 0;

		behaviourflags |= ((i & (1<<0)) == 0) ? D3DCREATE_HARDWARE_VERTEXPROCESSING : D3DCREATE_SOFTWARE_VERTEXPROCESSING;
		behaviourflags |= ((i & (1<<1)) == 0) ? D3DCREATE_MULTITHREADED : 0;

		int format = (i & ((1<<2)|(1<<3)|(1<<4))) >> 2; // 3 bits
		const D3DFORMAT fmts[8] =
		{
			D3DFMT_UNKNOWN,
			D3DFMT_R8G8B8,
			D3DFMT_A8R8G8B8,
			D3DFMT_X8R8G8B8,

			D3DFMT_A8B8G8R8,
			D3DFMT_X8B8G8R8,
			D3DFMT_R5G6B5,
			D3DFMT_A4R4G4B4,
		};

#if (!RSG_FINAL) || (RSG_FINAL_DEBUG)
		const char* fmtNames[8] =
		{
			"D3DFMT_UNKNOWN",
			"D3DFMT_R8G8B8",
			"D3DFMT_A8R8G8B8",
			"D3DFMT_X8R8G8B8",

			"D3DFMT_A8B8G8R8",
			"D3DFMT_X8B8G8R8",
			"D3DFMT_R5G6B5",
			"D3DFMT_A4R4G4B4",
		};
#endif

		m_d3dpp.BackBufferFormat = fmts[format];
		
		DEBUGF3("Attempting %s, %s, %s...",
			fmtNames[format],
			(behaviourflags & D3DCREATE_HARDWARE_VERTEXPROCESSING) ? "D3DCREATE_HARDWARE_VERTEXPROCESSING" : "D3DCREATE_SOFTWARE_VERTEXPROCESSING",
			(behaviourflags & D3DCREATE_MULTITHREADED) ? "D3DCREATE_MULTITHREADED" : "single-threaded.");

		hr = m_direct3D9->CreateDevice(0, D3DDEVTYPE_HAL, m_window, behaviourflags, &m_d3dpp, &m_device);

		if (FAILED(hr))
		{
			ERRORF("Failed to create device, code %x", hr);
			ERRORF("String: %s", DXGetErrorStringA(hr));
			ERRORF("Description: %s", DXGetErrorDescriptionA(hr));
		}
		else
		{
			break;
		}
	}

	if (!m_device)
	{
		ERRORF("Error! Could not create Direct3DDevice9.");
		return hr;
	}

	EnableWindow(TRUE);
	SetFocus();

	DISPLAYF("Successfully created DX Device!");

	return S_OK;
}

void DxSurface9::DestroyDevice()
{
	if (m_device)
	{
		m_device->Release();
		m_device = NULL;
	}

	if (m_direct3D9)
	{
		m_direct3D9->Release();
		m_direct3D9 = NULL;
	}

	//Re-enable stereo (if needed)
#if RSG_NVIDIA_SUPPORT
	if (m_bDisabledStereo)
	{
		DEBUGF1("Attempting to re-enable stereo");
		if (NvAPI_Stereo_Enable() == NVAPI_OK)
		{
			DEBUGF1("Re-enabled stereo");
			m_bDisabledStereo = false;
		}
		else
		{
			DEBUGF1("Unable to re-enable stereo");
		}

		if (NvAPI_Unload() == NVAPI_OK)
		{
			DEBUGF1("Unloaded NvAPI");
		}
		else
		{
			DEBUGF1("Unable to unload NvAPI");
		}
	}
	else
	{
		DEBUGF1("No need to re-enable stereo");
	}
#endif
}

void DxSurface9::DrawTest()
{
	if (!m_device)
		return;

	m_device->SetRenderState(D3DRS_LIGHTING, FALSE);
	m_device->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);

	struct FVF
	{
		float x,y,z;
		DWORD diffuse;
	};

	FVF tri[] =
	{
		{-1.0f,  1.0f, 0.0f, D3DCOLOR_ARGB(0xff,0xff,0x00,0x00)},
		{ 1.0f,  1.0f, 0.0f, D3DCOLOR_ARGB(0xff,0x00,0xff,0x00)},
		{-0.5f + 0.1f*(float)(rand()%11), -1.0f, 0.0f, D3DCOLOR_ARGB(0xff,0x00,0x00,0xff)}
	};

	m_device->DrawPrimitiveUP(D3DPT_TRIANGLELIST, 1, tri, sizeof(FVF));
	m_device->SetRenderState(D3DRS_LIGHTING, TRUE);
}

void DxSurface9::NullDraw()
{
	if (!m_device)
		return;

	m_device->SetRenderState(D3DRS_LIGHTING, FALSE);
	m_device->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);

	// TODO : render
	struct FVF
	{
		float x,y,z;
		DWORD diffuse;
	};

	FVF tri[] =
	{
		{-1.0f, 1.0f, 0.0f, D3DCOLOR_ARGB(0xff, 0xff, 0xff, 0xff)},
		{ 1.0f, 1.0f, 0.0f, D3DCOLOR_ARGB(0xff, 0xff, 0xff, 0xff)},
		{ 1.0f, -1.0f, 0.0f, D3DCOLOR_ARGB(0xff, 0xff, 0xff, 0xff)},
		{-1.0f, -1.0f, 0.0f, D3DCOLOR_ARGB(0xff, 0xff, 0xff, 0xff)}
	};

	m_device->DrawPrimitiveUP(D3DPT_TRIANGLEFAN, 2, tri, sizeof(FVF));
	m_device->SetRenderState(D3DRS_LIGHTING, TRUE);
}

void DxSurface9::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}

void DxSurface9::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

void DxSurface9::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CWnd::OnChar(nChar, nRepCnt, nFlags);
}

IUnknown* DxSurface9::GetDxContext()
{
	if (!m_device)
	{
		CreateDevice();
	}

	return m_device;
}

void* DxSurface9::GetPresentParams()
{
	if (!m_device)
	{
		CreateDevice();
	}

	if (!m_device)
	{
		ERRORF("Can't create device.");
		return NULL;
	}

	return (void*)&m_d3dpp;
}
