#pragma once

#include <string>
#include "RgscTelemetryEvent.h"

class RgscDebugTelemetry
{
public:
	enum DebugEvent
	{
		// These should really be sequential to avoid wasting memory
		RD_RETRY = 0,
		RD_PAUSED = 1,
		RD_ERROR_1 = 2,
		RD_ERROR_CATCHALL = 3,

		CDC_TIMED_OUT = 4,
		CDC_ERROR = 5,
		CDC_VERIFICATION_FAILURE = 6,
		CDC_FORCE_CLOSE = 7,

		CD_SIZE_MISMATCH = 8,
		CD_ENDED_WITH_ERROR = 9,
		CD_SLOW_CONNECTIONS = 10,
		CD_SLOW_CONNECTION_RETRY = 11,

		DCP_HASH_VERIFICATION_FAILURE = 12,
		DCP_CERT_VERIFICATION_FAILURE = 13,

		DEBUG_EVENT_COUNT
	};

	static void AddDebugEvent(DebugEvent evt, u64 count = 1);
	static void SendDebugTelemetry();
	static void Reset();

#if RSG_LAUNCHER
	static void TranslateToXmlEvent(RgscTelemetryEvent* parentEvent, const char* originalString);
#endif

private:
	static u64 m_eventCounters[DEBUG_EVENT_COUNT];
	static bool m_anyEventFired;
	static u64 m_lastTransmitTimeMillis;
};