/******************************************************************
Verifying the Signature of a Portable Executable File
Reference: http://msdn.microsoft.com/en-us/library/windows/desktop/aa382384(v=vs.85).aspx
*******************************************************************/
#include "StdAfx.h"
#include "CertificateVerify.h"
#include "Channel.h"

#include <SoftPub.h>
#include <WinCrypt.h>
#include <WinTrust.h>

#include <openssl/pem.h>
#include <openssl/applink.c>
#include <openssl/bio.h>
#if RSG_DEBUG
#pragma comment(lib, "ssleay32MTd.lib")
#pragma comment(lib, "libeay32MTd.lib")
#else
#pragma comment(lib, "ssleay32MT.lib")
#pragma comment(lib, "libeay32MT.lib")
#endif
#pragma comment(lib, "crypt32.lib")
#pragma comment(lib, "Wintrust.lib")

#define SHA1LEN 20
#define FIELD_LENGTH 256
#define ENCODING (X509_ASN_ENCODING | PKCS_7_ASN_ENCODING)
char *sm_publicCertificate = 
"-----BEGIN CERTIFICATE-----\n"
"MIIFKzCCBBOgAwIBAgIQUFcobkAz/LAAAAAAVWX1rDANBgkqhkiG9w0BAQsFADCB\n"
"tDELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUVudHJ1c3QsIEluYy4xKDAmBgNVBAsT\n"
"H1NlZSB3d3cuZW50cnVzdC5uZXQvbGVnYWwtdGVybXMxOTA3BgNVBAsTMChjKSAy\n"
"MDE1IEVudHJ1c3QsIEluYy4gLSBmb3IgYXV0aG9yaXplZCB1c2Ugb25seTEoMCYG\n"
"A1UEAxMfRW50cnVzdCBDb2RlIFNpZ25pbmcgQ0EgLSBPVkNTMTAeFw0xNjAzMDQy\n"
"MTQ2MTVaFw0xNzAzMjAyMjE2MTNaMIGKMQswCQYDVQQGEwJVUzERMA8GA1UECBMI\n"
"TmV3IFlvcmsxETAPBgNVBAcTCE5ldyBZb3JrMR0wGwYDVQQKExRSb2Nrc3RhciBH\n"
"YW1lcywgSW5jLjEXMBUGA1UECxMOUm9ja3N0YXIgR2FtZXMxHTAbBgNVBAMTFFJv\n"
"Y2tzdGFyIEdhbWVzLCBJbmMuMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKC\n"
"AQEApaErQpNem/165IOKRtlVV8qIO/sNeYuelEdjX/Py8w8WhxndB5ZAoio2L6ii\n"
"z1u8AVdhfMQIYzHfpdTXaxlgog/spjM8xjEwFu8ByRgxYOX+1YAk4fWnpc+brG17\n"
"rRkKus/HJFRI6NYVyXmQrKfcWB7CDW1pliOWIwh+RhVQo9VMuLjkTNl/19Fy2FzO\n"
"RXYJTXS/wlbIBtst+NQEeuFzKeEGdNhc6fjEXPMFTk/c70EIXO8BYzfcjB6JHz8w\n"
"FFJ2kcTUjLaw3EfXbV/nBXpPNxJw/TrHpiHDiPp0oFyy/WR5J7IQLALLvtDhtAwV\n"
"9TXIqIZ80UWO4RKfgHiqEMs4AQIDAQABo4IBXzCCAVswDgYDVR0PAQH/BAQDAgeA\n"
"MBMGA1UdJQQMMAoGCCsGAQUFBwMDMGoGCCsGAQUFBwEBBF4wXDAjBggrBgEFBQcw\n"
"AYYXaHR0cDovL29jc3AuZW50cnVzdC5uZXQwNQYIKwYBBQUHMAKGKWh0dHA6Ly9h\n"
"aWEuZW50cnVzdC5uZXQvb3ZjczEtY2hhaW4yNTYuY2VyMDEGA1UdHwQqMCgwJqAk\n"
"oCKGIGh0dHA6Ly9jcmwuZW50cnVzdC5uZXQvb3ZjczEuY3JsMEoGA1UdIARDMEEw\n"
"NgYKYIZIAYb6bAoBAzAoMCYGCCsGAQUFBwIBFhpodHRwOi8vd3d3LmVudHJ1c3Qu\n"
"bmV0L3JwYTAHBgVngQwBBDAfBgNVHSMEGDAWgBR+Gh8aEXRcZMkMH5QBq/2BZC6h\n"
"LDAdBgNVHQ4EFgQU9Z6rz09zOvAUQ1X1zJ0Bk4z64E4wCQYDVR0TBAIwADANBgkq\n"
"hkiG9w0BAQsFAAOCAQEAy6yf3EuGZIG1+imEHhvPZr3srdEnY0WAodsi1SKKuLf7\n"
"HxdEnXfZDg4MQ4mApknEMENs3+SKHnLaqPwqoXrRimuRe8wT1MgzkSyn+SP3nfQK\n"
"YJtX3MHm69anxQp5D8r3eqi2/zp8kPdp9mDlExSSsOVzIm8bK/1FD4stmA6k/328\n"
"6mRrIr+EtFfBLxfKaELlDV8jv58ie+YKZMjbvY9XfSsXDUkhbzwZUDP0/BDdViY9\n"
"XgY118krVHjwsDHqiN04IM06zU3dz192hy4kpQNpxlF9/hGh3+O1dfwPfxyzi+A4\n"
"lZ9CzppY1N6xqT/fL2SOvBQdcOT/8WE8VucfWZosfg==\n"
"-----END CERTIFICATE-----"; 

typedef struct {
	LPWSTR lpszProgramName;
	LPWSTR lpszPublisherLink;
	LPWSTR lpszMoreInfoLink;
} SPROG_PUBLISHERINFO, *PSPROG_PUBLISHERINFO;

LPTSTR CertificateVerify::GetCertificateDescription(PCCERT_CONTEXT pCertCtx)
{
	DWORD dwStrType;
	DWORD dwCount;
	//@@: location CERTIFICATEVERIFY_GETCERTIFICATEDESCRIPTION_SET_SUBJECT_NULL
	LPTSTR szSubjectRDN = NULL;
	//@@: range CERTIFICATEVERIFY_GETCERTIFICATEDESCRIPTION {

	dwStrType = CERT_X500_NAME_STR;
	dwCount = CertGetNameString(pCertCtx,
		CERT_NAME_RDN_TYPE,
		0,
		&dwStrType,
		NULL,
		0);
	

	if (dwCount)
	{
		szSubjectRDN = (LPTSTR) LocalAlloc(0, dwCount * sizeof(TCHAR));
		CertGetNameString(pCertCtx,
			CERT_NAME_RDN_TYPE,
			0,
			&dwStrType,
			szSubjectRDN,
			dwCount);

		//@@: } CERTIFICATEVERIFY_GETCERTIFICATEDESCRIPTION
	}

	return szSubjectRDN;
}

bool CertificateVerify::Verify(TCHAR* fileName)
{
	//@@: range CERTIFICATEVERIFY_VERIFY {

#if UNICODE
	std::string sfilename;
	CUtil::WStringToStdString(std::wstring(fileName), sfilename);
	DEBUGF1("Verifying certificate of %s", sfilename.c_str());
#else
	DEBUGF1("Verifying certificate of %s", fileName);
#endif

	CertificateDetails detail;

	GUID guidAction = WINTRUST_ACTION_GENERIC_VERIFY_V2;
	WINTRUST_FILE_INFO sWintrustFileInfo;
	WINTRUST_DATA      sWintrustData;
	HRESULT            hr;

	memset((void*)&sWintrustFileInfo, 0x00, sizeof(WINTRUST_FILE_INFO));
	memset((void*)&sWintrustData, 0x00, sizeof(WINTRUST_DATA));

	sWintrustFileInfo.cbStruct = sizeof(WINTRUST_FILE_INFO);
	sWintrustFileInfo.pcwszFilePath = fileName;
	sWintrustFileInfo.hFile = NULL;

	//@@: location CERTIFICATEVERIFY_VERIFY_SET_WINTRUST_DATA
	sWintrustData.cbStruct            = sizeof(WINTRUST_DATA);
	sWintrustData.dwUIChoice          = WTD_UI_NONE;
	sWintrustData.fdwRevocationChecks = WTD_REVOKE_NONE;
	sWintrustData.dwUnionChoice       = WTD_CHOICE_FILE;
	sWintrustData.pFile               = &sWintrustFileInfo;
	sWintrustData.dwStateAction       = WTD_STATEACTION_VERIFY;

	hr = WinVerifyTrust((HWND)INVALID_HANDLE_VALUE, &guidAction, &sWintrustData);

	if (TRUST_E_NOSIGNATURE == hr)
	{
		ERRORF("No signature found on the file.");
		return false;
	}
	else if (TRUST_E_BAD_DIGEST == hr)
	{
		ERRORF("The signature of the file is invalid.");
		return false;
	}
	else if (TRUST_E_PROVIDER_UNKNOWN == hr)
	{
		ERRORF("No trust provider on this machine can verify this type of files.");
		return false;
	}
	else if (S_OK != hr)
	{
		ERRORF("WinVerifyTrust failed with error 0x%.8X", hr);
		return false;
	}
	else
	{
		DEBUGF1("File signature is OK.\n");

		// Now lets actually use our public certificate to do some qualifying reads on it to be sure
		// not that it's just a pristine certificate, but it's one that we trust. We're going to do 
		// this by 
		// 1) Inspecting the issuer (provided by Entrust)
		// 2) Verifying that the subject is the same(ish) with a Rockstar Games
		// 3) Verifying the fingerprint of the certificate

		// First, lets load the certificate 
		BIO *bio;
		//@@: location CERTIFICATEVERIFY_VERIFY_ALLOCATE_BIO_BUF
		bio = BIO_new_mem_buf(sm_publicCertificate, -1);


		X509 *cert = PEM_read_bio_X509(bio, NULL, NULL, NULL);
		if (!cert) 
		{
			DEBUGF1("Unable to parse certificate");
			return false;
		}
		// Good, the certificate's loaded in, lets front-run the 
		// thumbprint of the certificate
		const EVP_MD *digest = EVP_sha1();
		unsigned len;
		char buf[SHA1LEN];
		int rc = X509_digest(cert, digest, (unsigned char*) buf, &len);
		if (rc == 0 || len != SHA1LEN)
		{
			DEBUGF1("Failed to calculate digest of certificate");
			return false;
		}

		// Now lets use our Windows API's to get the data from the DLL

		// Declare our local variables
		HCRYPTPROV hProv;
		HCERTSTORE hStore = NULL;
		HCRYPTMSG hMsg = NULL; 
		PCCERT_CONTEXT pCertContext = NULL;
		BOOL fResult;   
		DWORD dwEncoding, dwContentType, dwFormatType;
		PCMSG_SIGNER_INFO pSignerInfo = NULL;
		PCMSG_SIGNER_INFO pCounterSignerInfo = NULL;
		DWORD dwSignerInfo;
		CERT_INFO CertInfo;     
		PSPROG_PUBLISHERINFO ProgPubInfo;
		// Start by creating a crypto provider.
		if(!CryptAcquireContext(
			&hProv, 
			NULL, 
			NULL, 
			PROV_RSA_FULL, 
			0)) 
		{
			DEBUGF1("Failed to create a crypto provider.");
		}
		// Zero out the memory we intend on using
		ZeroMemory(&ProgPubInfo, sizeof(ProgPubInfo));
		// Next lets query the object
		fResult = CryptQueryObject(CERT_QUERY_OBJECT_FILE,
			fileName,
			CERT_QUERY_CONTENT_FLAG_PKCS7_SIGNED_EMBED,
			CERT_QUERY_FORMAT_FLAG_BINARY,
			0,
			&dwEncoding,
			&dwContentType,
			&dwFormatType,
			&hStore,
			&hMsg,
			NULL);
		if (!fResult)
		{
			DEBUGF1("CryptQueryObject failed with %x\n", GetLastError());
			return false;
		}
		// Fetch signer information size.
		fResult = CryptMsgGetParam(hMsg, 
			CMSG_SIGNER_INFO_PARAM, 
			0, 
			NULL, 
			&dwSignerInfo);
		if (!fResult)
		{
			DEBUGF1("CryptMsgGetParam failed with %x\n", GetLastError());
		}

		// Allocate memory for signer information.
		pSignerInfo = (PCMSG_SIGNER_INFO)LocalAlloc(LPTR, dwSignerInfo);
		if (!pSignerInfo)
		{
			DEBUGF1("Unable to allocate memory for Signer Info.\n");
			return false;
		}

		// Fetch Signer Information.
		fResult = CryptMsgGetParam(hMsg, 
			CMSG_SIGNER_INFO_PARAM, 
			0, 
			(PVOID)pSignerInfo, 
			&dwSignerInfo);
		if (!fResult)
		{
			DEBUGF1("CryptMsgGetParam failed with %x\n", GetLastError());
			return false;
		}

		CertInfo.Issuer = pSignerInfo->Issuer;
		CertInfo.SerialNumber = pSignerInfo->SerialNumber;
		//@@: location CERTIFICATEVERIFY_VERIFY_CERTFINDCERTIFICATE_IN_STORE
		pCertContext = CertFindCertificateInStore(hStore,
			ENCODING,
			0,
			CERT_FIND_SUBJECT_CERT,
			(PVOID)&CertInfo,
			NULL);
		if (!pCertContext)
		{
			DEBUGF1("CertFindCertificateInStore failed with %x\n", GetLastError());
			return false;
		}

		DWORD dwData;

		// Get Issuer name size.
		if (!(dwData = CertGetNameString(pCertContext, 
			CERT_NAME_SIMPLE_DISPLAY_TYPE,
			CERT_NAME_ISSUER_FLAG,
			NULL,
			NULL,
			0)))
		{
			DEBUGF1("CertGetNameString failed.\n");
		}

		// Allocate memory for Issuer name.
		//@@: location CERTIFICATEVERIFY_VERIFY_ALLOC_ISSUER_NAME
		LPTSTR szName = (LPTSTR)LocalAlloc(LPTR, dwData * sizeof(TCHAR));
		if (!szName)
		{
			DEBUGF1("Unable to allocate memory for issuer name.\n");
			return false;
		}

		// Get Issuer name.
		if (!(CertGetNameString(pCertContext, 
			CERT_NAME_SIMPLE_DISPLAY_TYPE,
			CERT_NAME_ISSUER_FLAG,
			NULL,
			szName,
			dwData)))
		{
			DEBUGF1("CertGetNameString failed.\n");
			return false;
		}

		// Pull the issuer from our X509 Certificate
		char genericField[FIELD_LENGTH];
		X509_NAME_get_text_by_NID(X509_get_issuer_name(cert), NID_commonName, genericField, FIELD_LENGTH);

		std::string genericFieldString;
		CUtil::WStringToStdString(szName, genericFieldString);
		if(strcmp(genericField,genericFieldString.c_str())!=0)
		{
			DEBUGF1("Comparison of Issuer Failed. This is catastrophically bad [%s] - [%s].\n", genericField, genericFieldString.c_str());
			return false;
		}

		X509_NAME_get_text_by_NID(X509_get_issuer_name(cert), NID_commonName, genericField, FIELD_LENGTH);

		LocalFree(szName);

		// Get subject name size.
		if (!(dwData = CertGetNameString(pCertContext, 
			CERT_NAME_SIMPLE_DISPLAY_TYPE,
			NULL,
			NULL,
			NULL,
			0)))
		{
			DEBUGF1("CertGetNameString failed.\n");
		}

		// Allocate memory for subject name.
		szName = (LPTSTR)LocalAlloc(LPTR, dwData * sizeof(TCHAR));
		if (!szName)
		{
			DEBUGF1("Unable to allocate memory for subject name.\n");
			return false;
		}

		// Get subject name.
		if (!(CertGetNameString(pCertContext, 
			CERT_NAME_SIMPLE_DISPLAY_TYPE,
			NULL,
			NULL,
			szName,
			dwData)))
		{
			DEBUGF1("CertGetNameString failed.\n");
			return false;
		}


		// Now lets check our subject name
		X509_NAME_get_text_by_NID(X509_get_subject_name(cert), NID_commonName, genericField, FIELD_LENGTH);

		genericFieldString.clear();
		CUtil::WStringToStdString(szName, genericFieldString);
		if(strcmp(genericField,genericFieldString.c_str())!=0)
		{
			DEBUGF1("Comparison of Subject Failed. This is catastrophically bad [%s] - [%s].\n", genericField, genericFieldString.c_str());
			return false;
		}

		return true;
	}
	return false;
	//@@: } CERTIFICATEVERIFY_VERIFY

}
