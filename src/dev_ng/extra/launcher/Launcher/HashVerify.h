#pragma once
#include <string>
class HashVerify
{
public:
	static bool Verify(const wchar_t* filepath, const std::string& hash);

private:
	static bool HexCharsToByte(char c1, char c2, unsigned char* out);
};