#include "StdAfx.h"
#include "BootstrapPatchCheck.h"
#include "SimpleDownloader.h"
#include "Application.h"
#include "Globals.h"
#include "Config.h"
#include "OnlineConfig.h"
#include "Localisation.h"
#include "seh.h"
#include "GamePatchCheck.h"
#include "Channel.h"
#include "VersionManager.h"
#include "RgscTelemetryManager.h"

#include <string>
#include <sstream>

#include "../tinyxml/tinyxml.h"

BootstrapPatchCheck::BootstrapPatchCheck(void)
{
	//@@: range BOOTSTRAPPATCHCHECK_CONSTRUCTOR {
	CUtil::GetServerAndDocpathForEnv(m_server, m_documentPath, Constants::BootstrapVersionPathProd);

	m_bIsRunning = false;
	m_bCheckError = false;
	m_bUpToDate = false;
	m_bRedownload = true; 
	m_patchCheckType = PT_BOOSTRAP;
	//@@: } BOOTSTRAPPATCHCHECK_CONSTRUCTOR
}

BootstrapPatchCheck::~BootstrapPatchCheck(void)
{
}

// /Silent /waitforprocess PlayMaxPayne3.exe
void BootstrapPatchCheck::OnAddPatch(Patch& patch)
{
	//@@: location BOOSTRAPPATCHCHECK_ONADDPATCH
	IPatchCheck::OnAddPatch(patch);
	patch.CommandlineArguments = "/S /silent /waitforprocess ";
	patch.CommandlineArguments += GetBootstrapExecutableFilename();
}

void BootstrapPatchCheck::DownloadComplete(DownloadCompleteStatus status, std::string filename)
{

	if (status != DL_STATUS_COMPLETE)
	{
		// ThrowError();
		return;
	}
}

void BootstrapPatchCheck::DownloadError(const CError& /*error*/)
{
	//@@: location BOOTSTRAPPATCHCHECK_DOWNLOADERROR
	m_bCheckError = true;
}

void BootstrapPatchCheck::ParseXML()
{
	Patch patch;
	if (VersionManager::GetBootstrapPatch(patch))
	{
		patch.DeleteOnCompletion = true;
		patch.ChunkedDownload = false;
		patch.UpdateType = UT_MANDATORY;
		patch.PatchType = PT_BOOSTRAP;
		patch.AlwaysDownload = true;

		CUtil::PrependTempoaryFolderPath(patch.LocalFilename, patch.LocalFilename);

		m_patches.clear();
		DEBUGF1("Adding bootstrap patch.");
		m_patches.push_back(patch);
	}
}

Version BootstrapPatchCheck::GetExecutableVersion()
{
	//@@: range BOOTSTRAPPATCHCHECK_GETEXECUTABLEVESION {
	std::string path = GamePatchCheck::GetGameExecutableFolder();
	std::string filename = GetBootstrapExecutableFilename();

	//@@: location BOOTSTRAPPATCHCHECK_GETEXECUTABLEVERSION_CHECK_LENGTH
	if (path.length() == 0 || filename.length() == 0)
		return Version();

	path.append("\\").append(filename);
	return GetVersionForFile(path);
	//@@: } BOOTSTRAPPATCHCHECK_GETEXECUTABLEVESION

}

std::string BootstrapPatchCheck::GetBootstrapExecutableFilename()
{
	//@@: range BOOTSTRAPPATCHCHECK_GETBOOTSTRAPEXECUTABLEFILENAME {
	std::string filename = Constants::BootstrapExeNameWithoutExtension;
	//@@: location BOOTSTRAPPATCHCHECK_GETBOOTSTRAPEXECUTABLEFILENAME_LOCATION
	filename += ".exe";
	return filename;
	//@@: } BOOTSTRAPPATCHCHECK_GETBOOTSTRAPEXECUTABLEFILENAME

}

