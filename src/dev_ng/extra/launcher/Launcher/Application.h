/*
 * This class is based on the DownloadManager C# file Application.cs
 * For now, none of the UI code is here - that's in CLauncherDlg, and the
 * finite state machine controlling it is CFsm.
 */

#pragma once

#include "IPatchCheck.h"

// rgsc
#include "rgsc_common.h"

// launcher
#include "Error.h"
#include "AdminRights.h"

#include <string>
#include <queue>

class CLauncherDlg;

class LauncherApp
{

private:
	static std::deque<Patch> m_patchList;
	static Patch m_currentPatch;
	static bool m_bRestart;
	static bool m_bHasCheckedDevServer;
	static bool m_bGameRunning;
	static bool m_bDownloadingPatch;
	static bool m_bCancelPatching;

public:
	static void RestartGame();
	static void UpdateSocialClubTicket(rgsc::RockstarId rockstarId, std::string base64Ticket);
	static void GameSignOutEvent();
	static void GameSignInEvent(rgsc::RockstarId rockstarId);
	static void RunUpdateCheck(bool rgscOnly, rgsc::RockstarId);
	static void CancelUpdateCheck();

	static void RunPatchProcess();
	static void CancelPatchProcess();

	static void UpdatePatchPaths(const std::string& find, const std::string& replace);

	static std::string GetArgs();
private:
	static bool DownloadCurrentPatch();
	static bool InstallCurrentPatch();
public:
	static std::string GetGameExecutableFullPath();
	static UINT LaunchGameExecutable();
	static void ShowErrorMessage(bool noResultCheck);
	static void ThrowError(const CError& error);
	static void ResetErrorState();
	static bool HasErrorOccurred() { return CError::GetLastError().IsError(); }
	static const CError& GetError() { return CError::GetLastError(); }
	static bool IsGameExecutableRunning() { return m_bGameRunning; }
	static bool IsDownloadingPatch() { return m_bDownloadingPatch; }

private:
	static void HandleGameMessages();

private:
	static bool CheckDependencies();

private:
	static CLauncherDlg* m_launcherDlg;
public:
	static void SetLauncherDlg(CLauncherDlg* launcherDlg) { m_launcherDlg = launcherDlg; }
private:
	LauncherApp(void);
	~LauncherApp(void);
};
