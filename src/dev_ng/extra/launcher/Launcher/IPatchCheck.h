#pragma once

#include <string>
#include <vector>

#include "Error.h"

#include "../tinyxml/tinyxml.h"

#define FORCE_UPDATE_ALL 0

enum UpdateType
{
	UT_MANDATORY,
	UT_OPTIONAL
};

enum PatchType
{
	PT_UNKNOWN,
	PT_BOOSTRAP,
	PT_LAUNCHER,
	PT_GAME,
	PT_SCUI,
	PT_DLC,
	PT_GAMEINSTALL,
	PT_MTL
};

enum DownloadCompleteStatus
{
	DL_STATUS_OK,
	DL_STATUS_COMPLETE,
	DL_STATUS_PAUSE,
	DL_STATUS_CANCEL,
	DL_STATUS_ERROR,
	DL_STATUS_RETRY,
	DL_STATUS_SLOW_RETRY,
	DL_STATUS_VERIFYING,
};

enum SkuType
{
	SKU_EFIGS,
	SKU_GERMAN,
	SKU_STEAM
};

struct Version
{
	Version(int major, int minor, int build, int revision)
	{
		this->major = major;
		this->minor = minor;
		this->build = build;
		this->revision = revision;
	}
	
	explicit Version(const std::string& version)
	{
		minor = build = revision = 0;
		//@@: location VERSION_CONSTRUCTOR_BUILD_C_STR
		const char* str = version.c_str();

		major = atoi(str);
		
		if ((str = strchr(str, '.')) == NULL)
			return;
		str++;
		minor = atoi(str);

		if ((str = strchr(str, '.')) == NULL)
			return;
		str++;
		build = atoi(str);

		if ((str = strchr(str, '.')) == NULL)
			return;
		str++;
		revision = atoi(str);
	}
	Version() { major = minor = build = revision = 0; }

	bool operator>(const Version& rhs)
	{
		if (major < rhs.major)
			return false;
		else if (major > rhs.major)
			return true;

		if (minor < rhs.minor)
			return false;
		else if (minor > rhs.minor)
			return true;

		if (build < rhs.build)
			return false;
		else if (build > rhs.build)
			return true;

		if (revision < rhs.revision)
			return false;
		else if (revision > rhs.revision)
			return true;

		return false;
	}

	bool operator==(const Version& rhs)
	{
		return major == rhs.major && minor == rhs.minor && build == rhs.build && revision == rhs.revision;
	}

	bool operator!=(const Version& rhs)
	{
		return !(*this == rhs);
	}

	int major, minor, build, revision;
};

class ProgressCalculator;

struct Patch
{
	Version     _Version;
	Version     EndVersion;
	std::string SkuName;
	std::string Description;
	std::string PatchURL;
	std::string LocalFilename;
	std::string AccessControlKey;
	u64         FileSize;
	u64         ContentLength;
	bool        Installed;
	bool        RequiresRestart;
	bool        RequiresShutdown;
	bool        AlwaysDownload;
	bool        DeleteOnCompletion;
	bool        BackgroundDownload;
	bool        ChunkedDownload;
	bool		NeedsAccessControlledUrl;
	bool		ExpectsHashFile;
	std::string CommandlineArguments;
	std::string DownloadToken;
	std::string HashHexDigits;
	UpdateType  UpdateType;
	PatchType   PatchType;
	ProgressCalculator* Progress;

	Patch()
	{
		SkuName = "";
		Description = "";
		PatchURL = "";
		LocalFilename = "";
		FileSize = 0;
		ContentLength = 0;
		Installed = false;
		RequiresRestart = false;
		RequiresShutdown = false;
		AlwaysDownload = false;
		DeleteOnCompletion = false;
		BackgroundDownload = false;
		ChunkedDownload = false;
		NeedsAccessControlledUrl = false;
		ExpectsHashFile = false;
		CommandlineArguments = "";
		DownloadToken = "";
		HashHexDigits = "";
		UpdateType = UT_OPTIONAL;
		PatchType = PT_UNKNOWN;
		Progress = NULL;
	}
};

class IPatchCheck
{
public:
	IPatchCheck(void);
	virtual ~IPatchCheck(void);

	virtual void CheckForPatches();
	void Cancel();

	virtual void ParseXML() = 0;
	virtual void CleanupXML();

	virtual void CheckExecutableVersion();
	virtual Version GetExecutableVersion() { return Version(); }

	static Version GetVersionForFile(const std::string& path, bool* out_exists = NULL);

	const std::vector<Patch>& GetPatches() {return m_patches; }

	static std::string getTextTo(const char** cursor, const char* terminator);
	static std::string getQuoted(const char** cursor);
	static std::string findAttributeValue(const char* cursor, const char* attrib);
	static std::string findTagValue(const char* cursor, const char* tagname);

	static void ConvertToNeedingAccessControlledUrl(Patch& patch);

	bool IsRunning() { return m_bIsRunning; }
	bool CheckError() { return m_bCheckError; }
	bool UpToDate() { return m_bUpToDate; }

	virtual void DownloadComplete(DownloadCompleteStatus status, std::string filename) { status; filename; }
	virtual void DownloadError(const CError& error) { error; }

	void ThrowError();

	static std::string GetValueFromRegistry(const TCHAR* key32, const TCHAR* key64, const TCHAR* valueName);

protected:
	std::vector<Patch> m_patches;

	std::string m_server, m_documentPath;

	virtual void OnAddPatch(Patch& patch);

	void PopulatePatch(Patch& patch, const char* skuName, const TiXmlElement* releaseElement, const TiXmlElement* patchElement);

	static std::string SafeAttribute(const TiXmlElement* element, const char* attrName);

	bool m_bIsRunning;
	bool m_bIsDownloading;
	bool m_bCheckError;
	bool m_bUpToDate;
	bool m_bRedownload;
	std::string m_localXmlFile;

	PatchType m_patchCheckType;

};
