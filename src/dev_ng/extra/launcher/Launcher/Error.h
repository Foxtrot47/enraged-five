#pragma once

#include <string>

enum ErrorCategory
{
	ERROR_CATEGORY_NONE,
	ERROR_CATEGORY_LAUNCHER,
	ERROR_CATEGORY_DOWNLOADER,
	ERROR_CATEGORY_SOCIAL_CLUB,
	ERROR_CATEGORY_GAME,
	ERROR_CATEGORY_EXIT_CODE,
};

// Taken from rlpc.h
enum InitErrorCodes
{
	SC_INIT_ERR_NONE = 0,
	SC_INIT_ERR_NO_DLL = 1,
	SC_INIT_ERR_NO_SC_COMMAND_LINE = 2,
	SC_INIT_ERR_PROGRAM_FILES_NOT_FOUND = 3,
	SC_INIT_ERR_LOAD_LIBRARY_FAILED = 4,
	SC_INIT_ERR_GET_PROC_ADDRESS_FAILED = 5,
	SC_INIT_ERR_CALL_EXPORTED_FUNC_FAILED = 6,
	SC_INIT_ERR_QUERY_INTERFACE_FAILED = 7,
	SC_INIT_ERR_UNKNOWN_ENV = 8,
	SC_INIT_ERR_INIT_CALL_FAILED = 9,
	SC_INIT_ERR_GET_SUBSYSTEM_FAILED = 10,
	SC_INIT_ERR_RGSC_NULL_AFTER_LOAD = 11,
	SC_INIT_ERR_CREATE_DEVICE_DX11 = 12,
	SC_INIT_ERR_CREATE_DEVICE_DX9 = 13,
	SC_INIT_ERR_NULL_PIPE = 14,
	SC_INIT_ERR_PIPE_CONNECT_FAILED = 15,
	SC_INIT_ERR_BAD_SIGNATURE = 16,
	SC_INIT_ERR_WEBSITE_FAILED_LOAD = 17,
};

enum LauncherErrorCodes
{
	LAUNCHER_ERR_NONE = 100,
	LAUNCHER_ERR_NO_GAME_EXE = 101,
	LAUNCHER_ERR_NO_GAME_PATH = 102,
	LAUNCHER_ERR_MISSING_DEPENDENCY = 103,
	LAUNCHER_ERR_STARTING_ENUMERATING_FILES = 104,
	LAUNCHER_ERR_ENUMERATING_FILES = 105,
	LAUNCHER_ERR_OPEN_PROCESS_FAILED = 106,
	LAUNCHER_ERR_GAME_CRASHED = 107,
	LAUNCHER_ERR_DIRECTX = 108,
	LAUNCHER_ERR_32BIT = 109,
	LAUNCHER_ERR_INSTALL_FAILED = 110,
	LAUNCHER_ERR_UNSUPPORTED_WINDOWS_VERSION = 111,
	LAUNCHER_ERR_SEH_EXCEPTION = 112,
	LAUNCHER_ERR_STEAM_FAILED = 113,
	LAUNCHER_ERR_STEAM_NO_ENTITLEMENT = 114,
	LAUNCHER_ERR_STEAM_NO_STEAM = 115,
	LAUNCHER_ERR_OPEN_SPARSE_FILE_FAILED = 116,
	LAUNCHER_ERR_MISSING_WMP = 117,
	LAUNCHER_ERR_CLOCK_WRONG = 118,
	LAUNCHER_ERR_NO_BOOTSTRAP = 119,
};

enum DownloaderErrorCodes
{
	DOWNLOADER_ERR_NONE = 200,
	DOWNLOADER_ERR_UNKNOWN = 201,
	DOWNLOADER_ERR_NO_CONNECTION = 202,
	DOWNLOADER_ERR_INTERRUPTED = 203,
	DOWNLOADER_ERR_INVALID_SIGNATURE = 204,
	DOWNLOADER_ERR_BAD_CRC = 205,
	DOWNLOADER_ERR_NO_DISK_SPACE = 206,
	DOWNLOADER_ERR_BAD_URL = 207,
	DOWNLOADER_ERR_UNENTITLED = 208,
	DOWNLOADER_ERR_ACU_FAILED = 209,
	DOWNLOADER_ERR_DOWNLOAD_MANIFEST_FAILED = 210,
	DOWNLOADER_ERR_READ_MANIFEST_FAILED = 211,
	DOWNLOADER_ERR_DOWNLOAD_VERSIONING_FAILED = 212,
	DOWNLOADER_ERR_READ_VERSIONING_FAILED = 213,
	DOWNLOADER_ERR_GET_SERVER_TIME_FAILED = 214,
	DOWNLOADER_ERR_PRELOAD_TIMER_EXPIRED = 215,
	DOWNLOADER_ERR_READ_FAILED = 216,
	DOWNLOADER_ERR_READ_ERROR = 217,
	DOWNLOADER_ERR_REQUEST_ERROR = 218,
	DOWNLOADER_ERR_SEND_ERROR = 219,
};

class CError
{
public:
	// Error handling
	static void SetError(const CError& error);
	static void SetError(ErrorCategory category, InitErrorCodes error, const std::string& extra = "");
	static void SetError(ErrorCategory category, LauncherErrorCodes error, const std::string& extra = "");
	static void SetError(ErrorCategory category, DownloaderErrorCodes error, const std::string& extra = "");
	static void SetErrorExitCode(int error, const std::string& extra = "");
	static const CError& GetLastError();
	static void ResetLastError();

	// Object to represent no error
	static const CError& NoError();

	// Error translation
	static std::string GetLocalisedErrorMessage(const CError& err);

	// Error telemetry
	static void ReportErrorTelemetry(const CError& err);

	// Support URL handling
	static void AutoSetSupportURL(const CError& error);
	static void SetSupportURL(const std::string& url);
	static const std::string& GetSupportURL();

public:
	explicit CError(ErrorCategory category, InitErrorCodes error, const std::string& extra = "");
	explicit CError(ErrorCategory category, LauncherErrorCodes error, const std::string& extra = "");
	explicit CError(ErrorCategory category, DownloaderErrorCodes error, const std::string& extra = "");

	CError();
	CError(const CError& error);

	const CError& operator=(const CError& error);
	const bool operator==(const CError& error) const;
	const bool operator!=(const CError& error) const;

private:
	explicit CError(ErrorCategory category, int error, const std::string& extra = "");
	static const char* GetErrorCategoryAsString(ErrorCategory category);
	static std::string GetErrorCodeAsString(ErrorCategory category, int code);

	ErrorCategory m_eCategory;
	int m_iError;
	std::string m_sExtraInfo;

public:
	ErrorCategory GetErrorCategory() const { return m_eCategory; }
	int GetErrorCode() const { return m_iError; }
	const std::string& GetExtra() const { return m_sExtraInfo; }
	bool IsError() const { return m_eCategory != ERROR_CATEGORY_NONE; }
	void Reset() { *this = NoError(); }

	bool IsError(InitErrorCodes scErrorCode) const { return m_eCategory == ERROR_CATEGORY_SOCIAL_CLUB && m_iError == scErrorCode; }
	bool IsError(LauncherErrorCodes launcherErrorCode) const { return m_eCategory == ERROR_CATEGORY_LAUNCHER && m_iError == launcherErrorCode; }
	bool IsError(DownloaderErrorCodes downloaderErrorCode) const { return m_eCategory == ERROR_CATEGORY_DOWNLOADER && m_iError == downloaderErrorCode; }
};
