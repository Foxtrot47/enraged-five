#include "StdAfx.h"
#include "LauncherPatchCheck.h"
#include "SimpleDownloader.h"
#include "Application.h"
#include "Globals.h"
#include "Config.h"
#include "OnlineConfig.h"
#include "Localisation.h"
#include "GamePatchCheck.h"
#include "Channel.h"
#include "seh.h"
#include "VersionManager.h"

#include <string>
#include <sstream>

#include "../tinyxml/tinyxml.h"

LauncherPatchCheck::LauncherPatchCheck(void)
{
	//@@: range LAUNCHERPATCHCHECK_CONSTRUCTOR {
	CUtil::GetServerAndDocpathForEnv(m_server, m_documentPath, Constants::LauncherVersionPathProd);

	m_bIsRunning = false;
	m_bCheckError = false;
	m_bUpToDate = false;
	m_bRedownload = true;
	m_patchCheckType = PT_LAUNCHER;

	//@@: } LAUNCHERPATCHCHECK_CONSTRUCTOR

}

LauncherPatchCheck::~LauncherPatchCheck(void)
{
}

// /Silent /waitforprocess PlayMaxPayne3.exe
void LauncherPatchCheck::OnAddPatch(Patch& patch)
{
	//@@: range LAUNCHERPATCHCHECK_ONADDPATCH {
	IPatchCheck::OnAddPatch(patch);
	patch.CommandlineArguments = "/S /silent /waitforprocess ";
	//@@: location LAUNCHERPATCHCHECK_ONADDPATCH_CREATE_EXE_NAME
	patch.CommandlineArguments += Constants::LauncherExeNameWithoutExtension;
	patch.CommandlineArguments += ".exe";
	//@@: } LAUNCHERPATCHCHECK_ONADDPATCH

}

void LauncherPatchCheck::DownloadComplete(DownloadCompleteStatus status, std::string filename)
{
	if (status != DL_STATUS_COMPLETE)
	{
		// ThrowError();
		return;
	}
}

void LauncherPatchCheck::DownloadError(const CError& /*error*/)
{
	m_bCheckError = true;
}

void LauncherPatchCheck::ParseXML()
{
	Patch patch;
	if (VersionManager::GetLauncherPatch(patch))
	{
		patch.DeleteOnCompletion = true;
		patch.ChunkedDownload = false;
		patch.UpdateType = UT_MANDATORY;
		patch.PatchType = PT_LAUNCHER;

		CUtil::PrependTempoaryFolderPath(patch.LocalFilename, patch.LocalFilename);

		patch.CommandlineArguments = "/S /silent";
		patch.CommandlineArguments += GetLauncherExecutableFilename();

		patch.RequiresRestart = false;
		patch.AlwaysDownload = true;

		DEBUGF1("Adding launcher patch.");
		m_patches.clear();
		m_patches.push_back(patch);
	}
}

Version LauncherPatchCheck::GetExecutableVersion()
{
	std::string path = GamePatchCheck::GetGameExecutableFolder();
	std::string filename = GetLauncherExecutableFilename();

	if (path.length() == 0 || filename.length() == 0)
		return Version();
	
	path.append("\\").append(filename);
	return GetVersionForFile(path);
}

std::string LauncherPatchCheck::GetLauncherExecutableFilename()
{
	std::string filename = Constants::LauncherExeNameWithoutExtension;
#if _DEBUG
	filename += "_debug";
#endif
	filename += ".exe";
	return filename;
}
