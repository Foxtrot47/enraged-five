#pragma once

#include "Channel.h"

#include "RockstarDownloader.h"
#include "SparseFileManager.h"

#include <map>

class ChunkedDownloader : public RockstarDownloader
{
public:
	ChunkedDownloader();

	static bool IsDownloadInProgress();

protected:
	virtual DownloadCompleteStatus DoDownload_CheckDisk(Patch& patch);
	virtual DownloadCompleteStatus DoDownload_ReceiveData(Patch& patch);

	virtual void Stop();

	virtual void AddBytesDownloaded(s64 bytes); // Note: bytes can be negative, if a chunk is corrupt and discarded
	virtual void UpdateDownloadProgress(Patch& patch);
	virtual void UpdateListener(DownloadCompleteStatus status);

	void NotifyDownloadEvent(DownloadEvent event);

	void DownloadHashFile(Patch& patch);
	void DeleteHashFile(const Patch& patch);

	SparseFileManager m_sparseFile;

	static const int HASH_BYTES = 32;

	static void CALLBACK ConnectionCallback(HINTERNET handle, DWORD_PTR context, DWORD status, LPVOID statusInformation, DWORD statusInformationLength);

	class Connection
	{
	public:
		Connection();
		~Connection();

		void Init(ChunkedDownloader* downloader, TCHAR* hostname, TCHAR* documentPath, bool useTls, SparseFileManager* sparseFile);
		void SetChunk(u64 offset, u64 length);
		void StartCheckingChunk();
		int GetIndex() { return m_index; }
		void Callback(HINTERNET handle, DWORD status, LPVOID statusInformation, DWORD statusInformationLength);
		void ClearError();
		void ClearCompletion();
		void Cancel();
		void Cleanup(bool force = false);

		void SetRangeInProgress(bool inProgress);

		void SetInCallback(bool inCallback) { if (inCallback) m_callbackDepth++; else m_callbackDepth--; }
		bool IsInCallback() { return m_callbackDepth != 0; }

		u64 GetLastUpdateTimeMillis() { return m_lastUpdateTimeMillis; }

		u64 GetBytesDownloaded() { return m_currentOffset-m_initialOffset; }
		u64 GetBytesTotal() { return m_chunkSize; }
		bool HasBeenUsed() { return m_hasBeenUsed; }

#if !RSG_FINAL
		void DebugConnectionStatus();
#endif

		enum ConnectionState
		{
			STATE_IDLE,
			STATE_STARTING_DOWNLOAD,
			STATE_DOWNLOADING,
			STATE_ERROR,
			STATE_COMPLETE,
			STATE_CANCELLED,
			STATE_VERIFYING,
		};

		ConnectionState GetState() { return m_state; }
		const CError& GetError() { return m_error; }

#if (!RSG_FINAL) || (RSG_FINAL_DEBUG)
		static const char* GetConnectionStateName(ConnectionState state);
#endif

	private:
		void CreateContext();
		void StartDownloadingChunk();
		void ReadChunkPart();
		void ReportBytes(u64 bytes, bool force = false);

		void Register();
		bool IsRegistered() { return m_index != -1; }

		void SetState(ConnectionState state);

		volatile ConnectionState m_state;
		u64 m_initialOffset;
		u64 m_currentOffset;
		u64 m_chunkSize;
		HINTERNET m_context;
		HINTERNET m_connection;
		HINTERNET m_request;
		TCHAR* m_hostname;
		TCHAR* m_documentPath;
		ChunkedDownloader* m_parentDownloader;
		SparseFileManager* m_sparseFile;
		bool m_useTls;
		bool m_hasBeenUsed;
		int m_index;
		INTERNET_BUFFERS m_buffers;
		DWORD m_bufferSize;
		u64 m_lastUpdateTimeMillis;
		u64 m_lastReportTimeMillis;
		u64 m_bytesToReport;
		CError m_error;
#if !RSG_FINAL
		u64 m_approxBytesPerSec;
#endif
		volatile int m_callbackDepth;
	};

	static bool sm_initialised;
	static bool sm_chunkedDownloadRunning;
	static CRITICAL_SECTION sm_critsec;
	static std::map<int, Connection*> sm_connectionMap;
	static int sm_connectionIndex;
	static int RegisterConnection(Connection* connection);
	static void UnregisterConnection(Connection* connection);
	static Connection* GetConnection(int key);

	static const char* sm_hashSuffix;
};
