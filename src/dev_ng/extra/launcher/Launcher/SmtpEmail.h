#ifndef _SMTP_EMAIL_H_
#define _SMTP_EMAIL_H_

#define SIPHON_LAUNCHER_DATA 0

#if SIPHON_LAUNCHER_DATA

#define WIN32_LEAN_AND_MEAN
#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>

int SendEmail(char * msg);

#endif


#endif