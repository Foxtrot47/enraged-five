#pragma once

#include <string>
#include <Windows.h>
#include <WinCrypt.h>
#include <stdio.h>
#include <tchar.h>

#include "CertificateDetails.h"

#define ENCODING (X509_ASN_ENCODING | PKCS_7_ASN_ENCODING)

class CertificateVerify
{
public:
	static LPTSTR GetCertificateDescription(PCCERT_CONTEXT pCertCtx);
	static bool Verify(TCHAR* fileName);
};

