#include "stdafx.h"

#include "Globals.h"
#include "Localisation.h"
#include "resource.h"
#include "Util.h"
#include <algorithm>
#include <string>

CLocalisation::CLocalisation(void)
{

}

CLocalisation::~CLocalisation(void)
{
}

CLocalisation& CLocalisation::Instance()
{
	//@@: location CLOCALISTATION_INSTANCE
	static CLocalisation instance;
	return instance;
}

void CLocalisation::Init()
{
	WCHAR localeName[LOCALE_NAME_MAX_LENGTH];
	GetUserDefaultLocaleName(localeName,LOCALE_NAME_MAX_LENGTH);

	//@@: location CLOCALISATION_INIT
	std::wstring localeWstr = localeName;
	std::string culture;
	CUtil::WStringToStdString(localeWstr, culture);

#if RSG_STEAM_LAUNCHER
	const char* keyName32 = "SOFTWARE\\Wow6432Node\\Rockstar Games\\Grand Theft Auto V Steam";
	const char* keyName64 = "SOFTWARE\\Rockstar Games\\Grand Theft Auto V Steam";
#else
	const char* keyName32 = "SOFTWARE\\Wow6432Node\\Rockstar Games\\Grand Theft Auto V";
	const char* keyName64 = "SOFTWARE\\Rockstar Games\\Grand Theft Auto V";
#endif // STEAM_BUILD

	HKEY hkey;
	if (RegOpenKeyExA(HKEY_LOCAL_MACHINE, keyName32, 0, KEY_READ, &hkey) == ERROR_SUCCESS
		|| RegOpenKeyExA(HKEY_LOCAL_MACHINE, keyName64, 0, KEY_READ, &hkey) == ERROR_SUCCESS)
	{
		char buffer[1024];
		DWORD length = sizeof(buffer);

		if (RegQueryValueExA(hkey, "Language", NULL, NULL, (LPBYTE)buffer, &length) == ERROR_SUCCESS)
		{
			m_installedLanguage = buffer;
		}
	}

	SetLanguageID(culture.c_str(), m_installedLanguage);

	m_productSku = "efigs";
}

std::string CLocalisation::GetProductSku()
{
	return m_productSku;
}

void CLocalisation::SetLanguageID(const char* culture, const std::string& installedLang)
{
#if !RSG_FINAL
	if (DebugSwitches::DebugUseFrenchLanguage)
	{
		m_languageId = MAKELANGID(LANG_FRENCH, SUBLANG_FRENCH);
		return;
	}
#endif

	//@@: location CLOCALISATION_SETLANGUAGEID_MAKE_STR
	static const struct
	{
		const char* culture;
		const char* lang_and_country;
		LANGID langID;

	} s_languages[] =
	{
		{ "en", "en-US", MAKELANGID(LANG_ENGLISH,SUBLANG_ENGLISH_US) },
		{ "de", "de-DE", MAKELANGID(LANG_GERMAN,SUBLANG_GERMAN) },
		{ "es", "es-ES", MAKELANGID(LANG_SPANISH,SUBLANG_SPANISH_MODERN) },
        { "es", "es-MX", MAKELANGID(LANG_SPANISH,SUBLANG_SPANISH_MEXICAN) },
		{ "fr", "fr-FR", MAKELANGID(LANG_FRENCH, SUBLANG_FRENCH) },
		{ "it", "it-IT", MAKELANGID(LANG_ITALIAN, SUBLANG_ITALIAN) },
		{ "ru", "ru-RU", MAKELANGID(LANG_RUSSIAN, SUBLANG_RUSSIAN_RUSSIA) },
		{ "pl", "pl-PL", MAKELANGID(LANG_POLISH, SUBLANG_POLISH_POLAND) },
		{ "pt", "pt-BR", MAKELANGID(LANG_PORTUGUESE, SUBLANG_PORTUGUESE_BRAZILIAN) },
		{ "zh", "zh-CHT", MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_TRADITIONAL) },
		{ "zh", "zh-CHS", MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_SIMPLIFIED) },
		{ "zh", "zh-TW", MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_TRADITIONAL) },
		{ "zh", "zh-CN", MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_SIMPLIFIED) },
		{ "ja", "ja-JP", MAKELANGID(LANG_JAPANESE, SUBLANG_JAPANESE_JAPAN) },
		{ "ko", "ko-KR", MAKELANGID(LANG_KOREAN, SUBLANG_KOREAN) },
	};

	const size_t LANGCOUNT = sizeof(s_languages) / sizeof(s_languages[0]);
	//@@: location CLOCALISATION_SETLANGUAGEID_CHECK_EMPTY_SET
	if (!installedLang.empty())
	{
		// Look for exact match against language-and-country identifiers
		for (int i = 0; i < LANGCOUNT; i++)
		{
			if (installedLang == s_languages[i].lang_and_country)
			{
				m_languageId = s_languages[i].langID;
				return;
			}
		}

		// No exact match found; look for matching language
		for (int i = 0; i < LANGCOUNT; i++)
		{
			if (strncmp(installedLang.c_str(), s_languages[i].culture, 2) == 0)
			{
				m_languageId = s_languages[i].langID;
				return;
			}
		}
	}

	// Look for exact match against default locale
	for (int i = 0; i < LANGCOUNT; i++)
	{
		if (strcmp(culture, s_languages[i].lang_and_country) == 0)
		{
			m_languageId = s_languages[i].langID;
			return;
		}
	}

	// No match found; Look for langauge match against default locale
	for (int i = 0; i < LANGCOUNT; i++)
	{
		if (strncmp(culture, s_languages[i].culture, 2) == 0)
		{
			m_languageId = s_languages[i].langID;
			return;
		}
	}

	// Default to English
	//@@: location CLOCALISATION_SETLANGUAGEID_DEFAULT_TO_ENGLISH
	m_languageId = s_languages[0].langID;
}

std::string CLocalisation::GetString(UINT identifier)
{
	HINSTANCE instance = GetModuleHandle(NULL);
	CString buffer;
	BOOL bRet;
	//@@: location CLOCALISATION_GETSTRING_LOAD_STRING
	bRet = buffer.LoadString(instance, identifier, m_languageId);

	if (!bRet)
	{
		ERRORF("Can't load string %lu for language ID 0x%lx!", identifier, m_languageId);

		bRet = buffer.LoadString(instance, identifier, MAKELANGID(LANG_ENGLISH,SUBLANG_ENGLISH_US));

		if (bRet)
		{
			std::wstring wstr = buffer;
			std::string str;
			CUtil::WStringToStdString(wstr, str);
			ERRORF("String was: %s", str.c_str());
		}
		else
		{
			ERRORF("String not found in English either!");
		}
	}

	if (bRet)
	{
		std::wstring wstr = buffer;
		std::replace(wstr.begin(), wstr.end(), L'�', (wchar_t)0x00A0);

		std::string outStdStr;
		CUtil::WStringToStdString(wstr, outStdStr);

		//@@: location CLOCALISATION_GETSTRING_FORMAT_STRING
		CUtil::FormatString(outStdStr);

		return outStdStr;
	}

	return "";
}

std::wstring CLocalisation::GetWString(UINT identifier)
{
	HINSTANCE instance = GetModuleHandle(NULL);
	CString buffer;
	BOOL bRet;
	bRet = buffer.LoadString(instance, identifier, m_languageId);

	if (!bRet)
	{
		ERRORF("Can't load string %lu for language ID 0x%lx!", identifier, m_languageId);

		bRet = buffer.LoadString(instance, identifier, MAKELANGID(LANG_ENGLISH,SUBLANG_ENGLISH_US));

		if (bRet)
		{
			std::wstring wstr = buffer;
			std::string str;
			CUtil::WStringToStdString(wstr, str);
			ERRORF("String was: %s", str.c_str());
		}
		else
		{
			ERRORF("String not found in English either!");
		}
	}

	if (bRet)
	{
		std::wstring ws = buffer;

		std::replace(ws.begin(), ws.end(), L'�', (wchar_t)0x00A0);

		CUtil::FormatString(ws);
		return ws;
	}

	return L"";
}

void CLocalisation::SetLanguageFromSteam(const char* steamLanguage)
{
	// language already set from the registry
	if (m_installedLanguage.length() > 0)
		return;

	std::string culture;

	// get default locale
	WCHAR localeName[LOCALE_NAME_MAX_LENGTH];
	GetUserDefaultLocaleName(localeName,LOCALE_NAME_MAX_LENGTH);

	// get default culture
	std::wstring localeWstr = localeName;
	CUtil::WStringToStdString(localeWstr, culture);

	// Language codes from Steam API, to override culture.
	// - https://partner.steamgames.com/documentation/languages
	if (!_stricmp(steamLanguage, "english"))
	{
		culture = "en-US";
	}
	else if (!_stricmp(steamLanguage, "tchinese")) // traditional chinese, not a typo
	{
		culture = "zh-CHT";
	}
	else if (!_stricmp(steamLanguage, "schinese")) // simplified chinese, not a typo
	{
		culture = "zh-CHS";
	}
	else if (!_stricmp(steamLanguage, "french"))
	{
		culture = "fr-FR";
	}
	else if (!_stricmp(steamLanguage, "german"))
	{
		culture = "de-DE";
	}
	else if (!_stricmp(steamLanguage, "italian"))
	{
		culture = "it-IT";
	}
	else if (!_stricmp(steamLanguage, "japanese"))
	{
		culture = "ja-JP";
	}
	else if (!_stricmp(steamLanguage, "koreana")) // yes, not "korean". Ask Valve not me.
	{
		culture = "ko-KR";
	}
	else if (!_stricmp(steamLanguage, "spanish"))
	{
		int langID = GetUserDefaultUILanguage();
		int primID = PRIMARYLANGID(langID);
		int subID = SUBLANGID(langID);

		wchar_t wideLocaleName[LOCALE_NAME_MAX_LENGTH] = {0};
		GetSystemDefaultLocaleName(wideLocaleName, LOCALE_NAME_MAX_LENGTH);

		if (primID == LANG_SPANISH)
		{
			// Steam doesn't natively support Mexican Spanish at this time, so we need to 
			// try a little harder to detect Mexican Spanish as a system locale/ui language.
			if (subID == SUBLANG_SPANISH_MEXICAN || (_wcsicmp(wideLocaleName, L"es-MX") == 0))
			{
				culture = "es-MX";
			}
			else
			{
				culture = "es-ES";
			}
				
		}
		else
		{
			culture = "es-ES";
		}
	}
	else if (!_stricmp(steamLanguage, "polish"))
	{
		culture = "pl-PL";
	}
	else if (!_stricmp(steamLanguage, "brazilian"))
	{
		culture = "pt-BR";
	}
	else if (!_stricmp(steamLanguage, "russian"))
	{
		culture = "ru-RU";
	}

	// set the new language id
	SetLanguageID(culture.c_str(), m_installedLanguage);
}