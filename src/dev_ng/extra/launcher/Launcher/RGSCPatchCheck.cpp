#include "StdAfx.h"
#include "RGSCPatchCheck.h"

#include "Globals.h"
#include "Config.h"
#include "OnlineConfig.h"
#include "DownloadManager.h"
#include "RockstarDownloader.h"
#include "Application.h"
#include "Channel.h"
#include "VersionManager.h"

#include <sstream>
#include "seh.h"
#include "Localisation.h"

#include "../tinyxml/tinyxml.h"

RGSCPatchCheck::RGSCPatchCheck(void)
{
	CUtil::GetServerAndDocpathForEnv(m_server, m_documentPath, Constants::RGSCVersionPathProd);

	//@@: location RGSCPATCHCHECK_CONSTRUCTOR
	m_bIsRunning = false;
	m_bCheckError = false;
	m_bUpToDate = false;
	m_bRedownload = true;
	m_patchCheckType = PT_SCUI;
}

RGSCPatchCheck::~RGSCPatchCheck(void)
{
}

void RGSCPatchCheck::DownloadComplete(DownloadCompleteStatus status, std::string filename)
{
	if (status != DL_STATUS_COMPLETE)
	{
		// ThrowError();
		return;
	}
}

void RGSCPatchCheck::DownloadError(const CError& /*error*/)
{
	WARNINGF("Download Error.");
	m_bCheckError = true;
}

void RGSCPatchCheck::ParseXML()
{
	Patch patch;
	//@@: location RGSCPATCHCHECK_PARSEXML_GETSOCIALCLUBPATCH
	if (VersionManager::GetSocialClubPatch(patch))
	{
		//@@: range RGSCPATCHCHECK_PARSEXML_PARSE_FIELDS {
		patch.DeleteOnCompletion = true;
		patch.ChunkedDownload = false;
		patch.UpdateType = UT_MANDATORY;
		patch.PatchType = PT_SCUI;
		//@@: location RGSCPATCHCHECK_PARSEXML_SET_COMMAND_LINE
		patch.CommandlineArguments = "/silent";
		patch.RequiresRestart = false;
		patch.SkuName = "RGSC";
		patch.Description = "RGSC";
		//@@: location RGSCPATCHCHECK_PARSEXML_PREPENDTEMPORARYFOLDERPATH
		CUtil::PrependTempoaryFolderPath(patch.LocalFilename, patch.LocalFilename);
		m_patches.clear();
		DEBUGF1("Adding RGSC patch (removing any previous).");
		//@@: location RGSCPATCHCHECK_PARSEXML_PUSH_BACK_PATCH
		m_patches.push_back(patch);
		//@@: } RGSCPATCHCHECK_PARSEXML_PARSE_FIELDS 
	}
	else
	{
		if (CError::GetLastError().GetErrorCategory() == ERROR_CATEGORY_LAUNCHER && CError::GetLastError().GetErrorCode() == LAUNCHER_ERR_CLOCK_WRONG)
		{
			WARNINGF("Error - clock wrong.");
			m_bCheckError = true;
		}
	}
	//@@: location RGSCPATCHCHECK_PARSEXML_RETURN
	return;
}

void RGSCPatchCheck::OnAddPatch(Patch& patch)
{
	patch.CommandlineArguments = "/silent";
}

Version RGSCPatchCheck::GetRgscDllVersion()
{
	//@@: range RGSCPATCHCHECK_GETEXECUTABLEVERSION_GET_REGISTRY_PATHS {
	const TCHAR* key64 = TEXT("SOFTWARE\\Wow6432Node\\Rockstar Games\\Rockstar Games Social Club");
	const TCHAR* key32 = TEXT("SOFTWARE\\Rockstar Games\\Rockstar Games Social Club");
	//@@: location RGSCPATCHCHECK_GETEXECUTABLEVERSION_TEXTIFY_INSTALLL_FOLDER
	const TCHAR* valName = TEXT("InstallFolder");

	std::string path = GetValueFromRegistry(key32, key64, valName);

	if (path.length() == 0)
		return Version();

	//@@: location RGSCPATCHCHECK_GETEXECUTABLEVERSION_APPEND_DLL
	path.append("\\socialclub.dll");

	return GetVersionForFile(path);
	//@@: } RGSCPATCHCHECK_GETEXECUTABLEVERSION_GET_REGISTRY_PATHS
}

Version RGSCPatchCheck::GetExecutableVersion()
{
	return GetRgscDllVersion();
}
