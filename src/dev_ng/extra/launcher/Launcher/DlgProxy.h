// DlgProxy.h: header file
//

#pragma once

class CLauncherDlg;


// CLauncherDlgAutoProxy command target

class CLauncherDlgAutoProxy : public CCmdTarget
{
	DECLARE_DYNCREATE(CLauncherDlgAutoProxy)

	CLauncherDlgAutoProxy();           // protected constructor used by dynamic creation

// Attributes
public:
	CLauncherDlg* m_pDialog;

// Operations
public:

// Overrides
	public:
	virtual void OnFinalRelease();

// Implementation
protected:
	virtual ~CLauncherDlgAutoProxy();

	// Generated message map functions

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(CLauncherDlgAutoProxy)

	// Generated OLE dispatch map functions

	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

