#include "stdafx.h"

#include "RgscTelemetryManager.h"
#include "RgscDebugTelemetry.h"

#include "Config.h"
#include "Globals.h"
#include "Util.h"

#include <openssl/aes.h>
#include <openssl/sha.h>
#include <openssl/rand.h>

#include <regex>


namespace rlTelemetryPolicies
{
	enum
	{
		DEFAULT_LOG_CHANNEL,
	};
}

#include "../../../game/network/Live/NetworkMetricsChannels.h"


#if RSG_LAUNCHER
bool RgscTelemetryManager::sm_versionSet = false;
RgscTelemetryManager::VersionPacked RgscTelemetryManager::sm_currentVersion;

std::string RgscTelemetryManager::sm_rockstarIdUsername;

#endif

std::vector<RgscTelemetryEvent*> RgscTelemetryManager::sm_eventsAsXml;

std::string RgscTelemetryManager::sm_countryCode, RgscTelemetryManager::sm_ipAddress;

void RgscTelemetryManager::Initialize(s64 rockstarId)
{
#if RSG_LAUNCHER
	char buffer[40] = {0};
	sprintf_s(buffer, "%lld", rockstarId);
	sm_rockstarIdUsername = buffer;

	ReadBinaryAndConvertToXmlEvents();
	LoadAllEventsFromXml();
#else
	(void)rockstarId;
#endif
	
}

void RgscTelemetryManager::Shutdown()
{
#if RSG_LAUNCHER
	if (!sm_rockstarIdUsername.empty())
	{
		SaveAllEventsAsXml();
	}
#endif
}

void RgscTelemetryManager::WriteEvent(Event event)
{
#if RSG_LAUNCHER
	AUTOCRITSEC(GetCriticalSection());
	DEBUGF3("RgscTelemetryManager: Writing event %d", event);

	AppendNewXmlEventOfType(event);
#else
	(void)event;
#endif
}

void RgscTelemetryManager::WriteEventInt(Event event, int param)
{
#if RSG_LAUNCHER
	AUTOCRITSEC(GetCriticalSection());
	DEBUGF3("RgscTelemetryManager: Writing event %d param %d", event, param);

	RgscTelemetryEvent* tmEvent = AppendNewXmlEventOfType(event);
	tmEvent->AddIntField(RgscTelemetryEvent::FN_EVENT_PARAMETER, param);
#else
	(void)event;
	(void)param;
#endif
}

void RgscTelemetryManager::WriteEventGeolocation()
{
#if RSG_LAUNCHER
	AUTOCRITSEC(GetCriticalSection());
	DEBUGF3("RgscTelemetryManager: Writing event %d country code \"%s\", IP address \"%s\"", EVENT_LAUNCHER_GEOLOCATION, sm_countryCode.c_str(), sm_ipAddress.c_str());

	RgscTelemetryEvent* tmEvent = AppendNewXmlEventOfType(EVENT_LAUNCHER_GEOLOCATION);
	tmEvent->AddStringField(RgscTelemetryEvent::FN_COUNTRY_CODE, sm_countryCode.c_str());
#endif
}

void RgscTelemetryManager::WriteEventDownloadStats(Event event, DownloadStats stats)
{
#if RSG_LAUNCHER
	AUTOCRITSEC(GetCriticalSection());
	DEBUGF3("RgscTelemetryManager: Writing event %d with download stats", event);

	RgscTelemetryEvent* tmEvent = AppendNewXmlEventOfType(event);
	AppendXmlDownloadStats(tmEvent, stats);
#else
	(void)event;
	(void)stats;
#endif
}


void RgscTelemetryManager::WriteEventDebugMetrics(Event event, std::vector<const RgscTelemetryEvent*> debugEvents)
{
#if RSG_LAUNCHER
	AUTOCRITSEC(GetCriticalSection());
	DEBUGF3("RgscTelemetryManager: Writing event %d with debug events", event);

	RgscTelemetryEvent* tmEvent = AppendNewXmlEventOfType(event);
	tmEvent->AddArrayField(RgscTelemetryEvent::FN_DEBUG_ARRAY, debugEvents);
#endif
}

void RgscTelemetryManager::WriteEventFileDownload(const Patch& patch)
{
#if RSG_LAUNCHER
	AUTOCRITSEC(GetCriticalSection());
	if (!patch.Progress)
	{
		DEBUGF1("Attempting to write telemetry of file download with no progress calculator!");
		return;
	}

	ProgressCalculator* prog = patch.Progress;

	DEBUGF3("RgscTelemetryManager: Writing event %d with file download stats", EVENT_LAUNCHER_DOWNLOAD_STATS);

	RgscTelemetryEvent* tmEvent = AppendNewXmlEventOfType(EVENT_LAUNCHER_DOWNLOAD_STATS);

	// Add regular download stats
	DownloadStats stats;
	PopulateTelemetryDownloadStatsFromProgressCalculator(*prog, stats);
	AppendXmlDownloadStats(tmEvent, stats);
	prog->ResetDownloadStats();

	// Add filename
	AppendXmlFilename(tmEvent, patch);
#else
	(void)patch;
#endif
}

void RgscTelemetryManager::AppendJsonInt(std::string& out_json, const char* name, s64 i)
{
	if (!out_json.empty())
		out_json += ",";

	out_json += "\"";
	out_json += name;
	out_json += "\":";

	char buffer[64];
	sprintf_s(buffer, "%lld", i);
	out_json += buffer;
}

void RgscTelemetryManager::AppendJsonString(std::string& out_json, const char* name, const char* str)
{
	if (!out_json.empty())
		out_json += ",";

	out_json += "\"";
	out_json += name;
	out_json += "\":\"";
	out_json += str;
	out_json += "\"";
}

void RgscTelemetryManager::AppendJsonArrayOrStruct(std::string& out_json, const char* name, const char* structureIncludingBrackets)
{
	if (!out_json.empty())
		out_json += ",";

	out_json += "\"";
	out_json += name;
	out_json += "\":";
	out_json += structureIncludingBrackets;
}

#if RSG_LAUNCHER
void RgscTelemetryManager::AppendXmlEventVersionInfo(RgscTelemetryEvent* event, const VersionPacked& versioning)
{
	char buffer[256];

	char build = '?', flavour = '?', env = '?';

	if (versioning.launcherFlags.build == LF_BUILD_DEBUG)
		build = 'D';
	else if (versioning.launcherFlags.build == LF_BUILD_RELEASE)
		build = 'R';
	if (versioning.launcherFlags.build == LF_BUILD_FINAL)
		build = 'F';

	if (versioning.launcherFlags.flavour == LF_FLAVOUR_PRELOADER)
		flavour = 'P';
	else if (versioning.launcherFlags.flavour == LF_FLAVOUR_STEAM)
		flavour = 'S';
	if (versioning.launcherFlags.flavour == LF_FLAVOUR_FULL)
		flavour = 'F';

	if (versioning.launcherFlags.env == LF_ENVIRONMENT_DEV)
		env = 'D';
	else
		env = 'P';

	sprintf_s(buffer, "%d.%d.%d.%d|%d.%d.%d.%d|%d.%d.%d.%d|%d.%d.%d.%d|%c%c%c",
		versioning.versionMajor,
		versioning.versionMinor,
		versioning.versionBuild,
		versioning.versionRevision,

		versioning.gameMajor,
		versioning.gameMinor,
		versioning.gameBuild,
		versioning.gameRevision,

		versioning.scdllMajor,
		versioning.scdllMinor,
		versioning.scdllBuild,
		versioning.scdllRevision,

		versioning.launcherMajor,
		versioning.launcherMinor,
		versioning.launcherBuild,
		versioning.launcherRevision,

		build,
		flavour,
		env);

	event->AddStringField(RgscTelemetryEvent::FN_VERSION_INFO, buffer);
}

void RgscTelemetryManager::AppendXmlDownloadStats(RgscTelemetryEvent* event, const DownloadStats& ver)
{
	switch (ver.type)
	{
	case ProgressCalculator::PT_DOWNLOAD_SC_UPDATE:
		event->AddStringField(RgscTelemetryEvent::FN_DOWNLOAD_TYPE, "sc");
		break;
	case ProgressCalculator::PT_DOWNLOAD_DIP_DATA:
		event->AddStringField(RgscTelemetryEvent::FN_DOWNLOAD_TYPE, "dip");
		break;
	case ProgressCalculator::PT_VERIFY_DIP_DATA:
		event->AddStringField(RgscTelemetryEvent::FN_DOWNLOAD_TYPE, "verify");
		break;
	case ProgressCalculator::PT_DOWNLOAD_PATCH:
		event->AddStringField(RgscTelemetryEvent::FN_DOWNLOAD_TYPE, "patch");
		break;
	case ProgressCalculator::PT_DOWNLOAD_BASE_DATA:
		event->AddStringField(RgscTelemetryEvent::FN_DOWNLOAD_TYPE, "base");
		break;
	case ProgressCalculator::PT_DOWNLOAD_DLC:
		event->AddStringField(RgscTelemetryEvent::FN_DOWNLOAD_TYPE, "dlc");
		break;
	}

	event->AddIntField(RgscTelemetryEvent::FN_BYTES_DOWNLOADED, ver.bytesThisSession);
	event->AddIntField(RgscTelemetryEvent::FN_DURATION, ver.sessionDuration);
	event->AddIntField(RgscTelemetryEvent::FN_CONNECTIONS_OPENED, ver.connectionsOpened);
	event->AddIntField(RgscTelemetryEvent::FN_CONNECTIONS_ERRORED, ver.connectionsErrored);
	event->AddIntField(RgscTelemetryEvent::FN_VERIFICATIONS_FAILED, ver.verificationsFailed);
}


void RgscTelemetryManager::AppendXmlFilename(RgscTelemetryEvent* event, const Patch& patch)
{
	if (!patch.AccessControlKey.empty())
	{
		// Strip out the folder prefixes out and, if found, everything up to the dlcpacks/ and Game_EFIGS/ folders.
		std::string accessKey = patch.AccessControlKey;

		accessKey = std::regex_replace(accessKey, std::regex(".*DigitalData/[a-zA-Z0-9_]+/"), "");
		accessKey = std::regex_replace(accessKey, std::regex(".*dlcpacks/"), "");
		accessKey = std::regex_replace(accessKey, std::regex(".*Game_EFIGS/"), "");

		if (!accessKey.empty())
		{
			event->AddStringField(RgscTelemetryEvent::FN_FILENAME, accessKey.c_str());
		}
		else
		{
			event->AddStringField(RgscTelemetryEvent::FN_FILENAME, patch.AccessControlKey.c_str());
		}
	}
	else if (!patch.PatchURL.empty())
	{
		// patches.rockstargames.com/gtav/
		std::regex pattern("patches\\.rockstargames\\.com/[a-z]+/([a-z0-9A-Z\\-_/. %]+)");

		std::smatch results;
		if (std::regex_search(patch.PatchURL, results, pattern) && results.size() == 2)
		{
			const std::string& result = results[1];
			event->AddStringField(RgscTelemetryEvent::FN_FILENAME, result.c_str());
		}
		else if (std::regex_search(patch.PatchURL, results, std::regex("(/[^/]+/[a-z0-9A-Z\\-_. %]+)$")) && results.size() == 2)
		{
			const std::string& result = results[1];
			event->AddStringField(RgscTelemetryEvent::FN_FILENAME, result.c_str());
		}
		else
		{
			event->AddStringField(RgscTelemetryEvent::FN_FILENAME, patch.PatchURL.c_str());
		}
	}
}

bool RgscTelemetryManager::ReadBinaryAndConvertToXmlEvents()
{
	std::wstring bootstrapPath, launcherPath, tempPath;

	if (!GetTelemFilePath(bootstrapPath, TF_BOOTSTRAP) || !GetTelemFilePath(launcherPath, TF_LAUNCHER) || !GetTelemFilePath(tempPath, TF_TEMP))
	{
		ERRORF("Unable to get telemetry filepaths!");
		return false;
	}

	// Read more events if we have none
	if (!CUtil::FileExists(tempPath))
	{
		AUTOCRITSEC(GetCriticalSection());

		// Move one of the files to be the temp file
		if (CUtil::FileExists(launcherPath))
		{
			DEBUGF3("Using launcher telemetry as source.");
			MoveFileW(launcherPath.c_str(), tempPath.c_str());
		}
		else if (CUtil::FileExists(bootstrapPath))
		{
			DEBUGF3("Using bootstrap telemetry as source.");
			MoveFileW(bootstrapPath.c_str(), tempPath.c_str());
		}
		else
		{
			// Nothing to do, return
			return true;
		}
	}
	else
	{
		DEBUGF3("Telemetry temp file exists.");
	}

	// Read from the temp file
	std::ifstream input(tempPath, std::ifstream::binary | std::ifstream::in);
	std::vector<RgscTelemetryManager::EventWithData> binaryEvents;

	if (input)
	{
		while (input.good() && !input.eof())
		{
			EventWithData evt;
			input.read((char*)&evt.header, sizeof(EventHeader));
			
			if (!input)
			{
				if (input.eof())
				{
					DEBUGF3("Telem eof.");
				}
				else
				{
					ERRORF("Bad tm read.");
				}
				break;
			}

			if (!IsEventNumberValid((Event)evt.header.event))
			{
				WARNINGF("Invalid tm read.");
				break;
			}

			if (EventTypeHasVersionInfo((EventType)evt.header.eventType))
			{
				input.read((char*)&evt.versioning, sizeof(VersionPacked));

				if (!input)
				{
					if (input.eof())
					{
						DEBUGF3("Telem veof.");
					}
					else
					{
						ERRORF("Bad tm vread.");
					}
					break;
				}
			}

			if (evt.header.eventType == INT_PARAM || evt.header.eventType == VERSIONED_INT_PARAM)
			{
				evt.data = new char[sizeof(int)];
				input.read(evt.data, sizeof(int));
				if (!input)
				{
					ERRORF("Bad tm param read.");
					delete[] evt.data;
					evt.data = NULL;
					break;
				}
			}
			else if (evt.header.eventType == STRING_PARAM || evt.header.eventType == VERSIONED_STRING_PARAM)
			{
				u32 len = 0;
				input.read((char*)&len, sizeof(len));
				if (!input)
				{
					ERRORF("Bad tm len.");
					break;
				}
				evt.data = new char[len+1];
				input.read(evt.data, len);
				if (!input)
				{
					ERRORF("Bad tm param read.");
					delete[] evt.data;
					evt.data = NULL;
					break;
				}
				evt.data[len] = '\0';
			}
			else if (evt.header.eventType == DOWNLOAD_STATS_PARAM || evt.header.eventType == VERSIONED_DOWNLOAD_STATS_PARAM)
			{
				evt.data = new char[sizeof(DownloadStats)];
				input.read(evt.data, sizeof(DownloadStats));
				if (!input)
				{
					ERRORF("Bad tm param read.");
					delete[] evt.data;
					evt.data = NULL;
					break;
				}
			}

			binaryEvents.push_back(evt);
		}

		input.close();
	}
	else
	{
		ERRORF("Unable to read tm file!");
	}

	for (size_t i = 0; i < binaryEvents.size(); i++)
	{
		EventWithData& evt = binaryEvents[i];

		RgscTelemetryEvent* tmEvent = new RgscTelemetryEvent(RgscTelemetryEvent::MN_LAUNCHER_METRIC);

		bool valid = true;

		switch (evt.header.eventType)
		{
			case NO_PARAM:
			case VERSIONED_NO_PARAM:
			{
				tmEvent->AddIntField(RgscTelemetryEvent::FN_EVENT_TYPE, evt.header.event);
				AppendXmlEventTimestamp(tmEvent, evt.header.timestamp);
				break;
			}
			case INT_PARAM:
			case VERSIONED_INT_PARAM:
			{
				tmEvent->AddIntField(RgscTelemetryEvent::FN_EVENT_TYPE, evt.header.event);
				AppendXmlEventTimestamp(tmEvent, evt.header.timestamp);
				tmEvent->AddIntField(RgscTelemetryEvent::FN_EVENT_PARAMETER, *((int*)(evt.data)));
				break;
			}
			case STRING_PARAM:
			case VERSIONED_STRING_PARAM:
			{
				tmEvent->AddIntField(RgscTelemetryEvent::FN_EVENT_TYPE, evt.header.event);
				AppendXmlEventTimestamp(tmEvent, evt.header.timestamp);

				if (evt.header.event == EVENT_LAUNCHER_DEBUG)
				{
					RgscDebugTelemetry::TranslateToXmlEvent(tmEvent, evt.data);
				}
				else
				{
					tmEvent->AddStringField(RgscTelemetryEvent::FN_COUNTRY_CODE, evt.data);
				}
				break;
			}
			case DOWNLOAD_STATS_PARAM:
			case VERSIONED_DOWNLOAD_STATS_PARAM:
			{
				tmEvent->AddIntField(RgscTelemetryEvent::FN_EVENT_TYPE, evt.header.event);
				AppendXmlEventTimestamp(tmEvent, evt.header.timestamp);
				AppendXmlDownloadStats(tmEvent, *(DownloadStats*)evt.data);
				break;
			}
			default:
			{
				ERRORF("Unsupported tm type! %d", evt.header.eventType);
				valid = false;
				break;
			}
		}

		if (valid)
		{
			if (EventTypeHasVersionInfo((EventType)evt.header.eventType))
			{
				AppendXmlEventVersionInfo(tmEvent, evt.versioning);
			}

			sm_eventsAsXml.push_back(tmEvent);
		}
		else
		{
			delete[] tmEvent;
			tmEvent = NULL;
		}

		if (evt.data)
		{
			delete[] evt.data;
			evt.data = NULL;
		}
	}

	binaryEvents.clear();

	DEBUGF3("RgscTelemtry: All metrics written; deleting file.");
	DeleteFileW(tempPath.c_str());

	return true;
}


bool RgscTelemetryManager::TransmitXmlEvents(rgsc::ITelemetryV4* telemetryInterface)
{
	AUTOCRITSEC(GetCriticalSection());
	bool writtenMetric = false;

	while (telemetryInterface->HasMemoryForMetric() && !sm_eventsAsXml.empty())
	{
		std::string metricName;
		std::string json;

		RgscTelemetryEvent* event = sm_eventsAsXml.front();
		event->WriteAsJson(metricName, json);

		int priority = rgsc::ITelemetryPolicyV1::RGSC_LOGLEVEL_LOW_PRIORITY;

		std::string eventTypeAsString;
		if (event->GetFieldAsString(RgscTelemetryEvent::FN_EVENT_TYPE, eventTypeAsString))
		{
			int eventNumber = atoi(eventTypeAsString.c_str());
			if (eventNumber == EVENT_LAUNCHER_DOWNLOAD_STATS)
			{
				priority = rgsc::ITelemetryPolicyV1::RGSC_LOGLEVEL_VERYLOW_PRIORITY;
			}
		}

		if (telemetryInterface->Write(metricName.c_str(), json.c_str(), TELEMETRY_CHANNEL_MISC, priority))
		{
			DEBUGF3("RgscTelemetry: Metric name: `%s` metric: `%s`", metricName.c_str(), json.c_str());
			delete event;
			sm_eventsAsXml.erase(sm_eventsAsXml.begin());
			writtenMetric = true;
		}
		else
		{
			DEBUGF3("RgscTelemetry: Unable to write metric: `%s` metric: `%s`", metricName.c_str(), json.c_str());
			return false;
		}
	}

	// If there's a large backlog (or, conversely, if we're at the end of the events), try to flush submission
	if (writtenMetric && (sm_eventsAsXml.size() > TOTAL_EVENT_COUNT * 2 || sm_eventsAsXml.empty()))
	{
		DEBUGF3("RgscTelemtry: flush.");
		if (telemetryInterface->HasAnyMetrics())
		{
			telemetryInterface->Flush(false);
		}
	}

	return true;
}


void RgscTelemetryManager::SetVersions(const Version& versionTxt, const Version& gameVersion, const Version& scVersion, const Version& launcherVersion, bool dev)
{
	sm_currentVersion.versionMajor = (u16)versionTxt.major;
	sm_currentVersion.versionMinor = (u16)versionTxt.minor;
	sm_currentVersion.versionBuild = (u16)versionTxt.build;
	sm_currentVersion.versionRevision = (u16)versionTxt.revision;

	sm_currentVersion.gameMajor = (u16)gameVersion.major;
	sm_currentVersion.gameMinor = (u16)gameVersion.minor;
	sm_currentVersion.gameBuild = (u16)gameVersion.build;
	sm_currentVersion.gameRevision = (u16)gameVersion.revision;

	sm_currentVersion.scdllMajor = (u16)scVersion.major;
	sm_currentVersion.scdllMinor = (u16)scVersion.minor;
	sm_currentVersion.scdllBuild = (u16)scVersion.build;
	sm_currentVersion.scdllRevision = (u16)scVersion.revision;

	sm_currentVersion.launcherMajor = (u16)launcherVersion.major;
	sm_currentVersion.launcherMinor = (u16)launcherVersion.minor;
	sm_currentVersion.launcherBuild = (u16)launcherVersion.build;
	sm_currentVersion.launcherRevision = (u16)launcherVersion.revision;

#if RSG_DEBUG
	sm_currentVersion.launcherFlags.build = LF_BUILD_DEBUG;
#elif RSG_RELEASE
	sm_currentVersion.launcherFlags.build = LF_BUILD_RELEASE;
#elif RSG_FINAL
	sm_currentVersion.launcherFlags.build = LF_BUILD_FINAL;
#else
	sm_currentVersion.launcherFlags.build = LF_BUILD_UNKNOWN;
#endif

#if RSG_PRELOADER_LAUNCHER
	sm_currentVersion.launcherFlags.flavour = LF_FLAVOUR_PRELOADER;
#elif RSG_STEAM_LAUNCHER
	sm_currentVersion.launcherFlags.flavour = LF_FLAVOUR_STEAM;
#else
	sm_currentVersion.launcherFlags.flavour = LF_FLAVOUR_FULL;
#endif

	sm_currentVersion.launcherFlags.env = !dev;

	sm_versionSet = true;

	sm_currentVersion.launcherFlags.env = !dev;
}

#endif

bool RgscTelemetryManager::GetTelemFilePath(std::wstring& out_filepath, TelemetryFile whichFile)
{
	//@@: location RGSC_TELEMETRYMANAGER_GETTELEMFILEPATH
	PWSTR pszPath = NULL;
	HRESULT hr = SHGetKnownFolderPath(FOLDERID_LocalAppData, 0, NULL, &pszPath);

	if (FAILED(hr))
	{
		ERRORF("Can't find AppData!");
		return false;
	}

	std::wstring telemfile = std::wstring(pszPath);

	CoTaskMemFree(pszPath);

	telemfile += Constants::AppDataSubfolderCompanyName.Get();
	telemfile += Constants::AppDataSubfolderProductName.Get();

	switch (whichFile)
	{
	case TF_BOOTSTRAP:
		telemfile += std::wstring(L"\\bootstrap.telem");
		break;
	case TF_LAUNCHER:
		telemfile += std::wstring(L"\\launcher.telem");
		break;
	case TF_TEMP:
		telemfile += std::wstring(L"\\temp.telem");
		break;
	case TF_XML_EVENTS:
		telemfile += std::wstring(L"\\metrics.telem");
		break;
	default:
		ERRORF("Unknown TF type!");
		return false;
	}

	out_filepath = telemfile;

	return true;
}

CRITICAL_SECTION& RgscTelemetryManager::GetCriticalSection()
{
	static bool s_inited = false;
	static CRITICAL_SECTION s_critsec;

	if (!s_inited)
	{
		InitializeCriticalSection(&s_critsec);
		s_inited = true;
	}

	return s_critsec;
}

bool RgscTelemetryManager::EventTypeHasVersionInfo(EventType type)
{
	return type == VERSIONED_NO_PARAM || type == VERSIONED_INT_PARAM || type == VERSIONED_STRING_PARAM || type == VERSIONED_DOWNLOAD_STATS_PARAM;
}

const char* RgscTelemetryManager::GetEventsXmlRootNodeName()
{
	return "metrics";
}

#if RSG_LAUNCHER
void RgscTelemetryManager::SaveAllEventsAsXml()
{
	std::wstring path;
	if (GetTelemFilePath(path, TF_XML_EVENTS))
	{
		CUtil::CreateDirectoryHierarchyIfDoesntExist(path);
		SaveAllEventsAsXml(path);
	}
}

void RgscTelemetryManager::SaveAllEventsAsXml(const std::wstring& filepath)
{
	TiXmlDocument doc;

	doc.LinkEndChild(new TiXmlDeclaration("1.0", "utf-8", ""));

	TiXmlElement* metricsContainer = new TiXmlElement(GetEventsXmlRootNodeName());

	doc.LinkEndChild(metricsContainer);

	for (size_t i = 0; i < sm_eventsAsXml.size(); i++)
	{
		sm_eventsAsXml[i]->AppendToXml(metricsContainer);
	}

	// Create and configure printer
	TiXmlPrinter printer;
	printer.SetIndent("\t");

	// Print to memory
	doc.Accept(&printer);

	std::string printedXml(printer.CStr());

	EncryptEventsXmlInPlace(printedXml);

	HANDLE file = CreateFileW(filepath.c_str(), GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_ALWAYS, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	
	if (file != INVALID_HANDLE_VALUE)
	{
		DWORD bytesWritten;
		WriteFile(file, printedXml.c_str(), (DWORD)printedXml.size(), &bytesWritten, NULL);
		CloseHandle(file);
	}
	else
	{
		WARNINGF("Unable to save metrics!");
	}
	
}

void RgscTelemetryManager::LoadAllEventsFromXml()
{
	std::wstring path;
	if (GetTelemFilePath(path, TF_XML_EVENTS))
	{
		LoadAllEventsFromXml(path);
	}
}

void RgscTelemetryManager::LoadAllEventsFromXml(const std::wstring& filepath)
{
	HANDLE file = CreateFileW(filepath.c_str(), GENERIC_READ | DELETE, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN | FILE_FLAG_DELETE_ON_CLOSE, NULL);

	if (file != INVALID_HANDLE_VALUE)
	{
		// Check the size
		LARGE_INTEGER lint;
		GetFileSizeEx(file, &lint);
		u64 fileSize = lint.QuadPart;

		// Don't load excessively-large files
		if (fileSize < 1024 * 1024 * 1024)
		{
			char* buffer = new char[fileSize];

			// We can safely cast due to the enclosing if
			DWORD bytesToRead = (DWORD)fileSize;
			DWORD bytesRead;
			if (ReadFile(file, buffer, bytesToRead, &bytesRead, NULL))
			{
				std::string xmlString;
				xmlString.assign(buffer, (size_t)bytesRead);

				delete[] buffer;

				DecryptEventsXmlInPlace(xmlString);

				TiXmlDocument doc;
				if (doc.Parse(xmlString.c_str()) != NULL)
				{
					LoadAllEventsFromXmlDocument(doc);
				}
			}
			else
			{
				WARNINGF("Unable to read metrics!");
				delete[] buffer;
			}
		}

		// File is deleted on close
		CloseHandle(file);
	}
}

void RgscTelemetryManager::LoadAllEventsFromXmlDocument(const TiXmlDocument& doc)
{
	const TiXmlElement* root = doc.FirstChildElement(GetEventsXmlRootNodeName());

	if (!root)
		return;

	const char* metricTagName = RgscTelemetryEvent::GetMetricXmlTagName();

	for (const TiXmlElement* metric = root->FirstChildElement(metricTagName); metric; metric = metric->NextSiblingElement(metricTagName))
	{
		RgscTelemetryEvent* event = new RgscTelemetryEvent(metric);
		sm_eventsAsXml.push_back(event);
	}
}

// Generates an encryption key based on the user's Rockstar ID
// This is only used to protected telemetry events that are saved to disk, which are not
// particularly sensitive, and therefore don't warrant more secure crypto.
bool RgscTelemetryManager::GenerateTelemetryEncryptionKey(u8 (&out_key)[CRYPT_KEY_LENGTH])
{
	if (sm_rockstarIdUsername.empty())
	{
		DEBUGF1("No rockstar ID set when generating telemetry crypto key!");
		return false;
	}
	else
	{
		SHA256_CTX ctx;
		SHA256_Init(&ctx);
		SHA256_Update(&ctx, sm_rockstarIdUsername.c_str(), sm_rockstarIdUsername.size());

		u8 hash[SHA256_DIGEST_LENGTH];
		SHA256_Final(hash, &ctx);

	for (size_t i = 0; i < CRYPT_KEY_LENGTH; i++)
	{
			out_key[i] = hash[i % SHA256_DIGEST_LENGTH];
		}
		return true;
	}
}

void RgscTelemetryManager::EncryptEventsXmlInPlace(std::string& inout_eventsXml)
{
	// Ensure multiple of block size
	size_t remainder = inout_eventsXml.size() % AES_BLOCK_SIZE;
	if (remainder > 0)
	{
		inout_eventsXml.append(AES_BLOCK_SIZE - remainder, '\0');
	}

	u8 key[CRYPT_KEY_LENGTH];
	if (!GenerateTelemetryEncryptionKey(key))
		return;

	size_t dataSize = inout_eventsXml.size();

	u8 IV[CRYPT_IV_LENGTH];
	RAND_bytes((u8*)IV, CRYPT_IV_LENGTH);

	u8 IVcopy[CRYPT_IV_LENGTH];
	memcpy(IVcopy, IV, CRYPT_IV_LENGTH);

	u8* inputBuffer = new u8[dataSize];
	memcpy(inputBuffer, inout_eventsXml.c_str(), dataSize);

	u8* outputBuffer = new u8[dataSize];

	AES_KEY aes;
	AES_set_encrypt_key(key, CRYPT_KEY_LENGTH * 8, &aes);
	AES_cbc_encrypt(inputBuffer, outputBuffer, dataSize, &aes, IVcopy, AES_ENCRYPT);

	inout_eventsXml.clear();
	inout_eventsXml.append((char*)IV, CRYPT_IV_LENGTH);
	inout_eventsXml.append((char*)outputBuffer, dataSize);
}

void RgscTelemetryManager::DecryptEventsXmlInPlace(std::string& inout_eventsXml)
{
	if (inout_eventsXml.size() <= CRYPT_IV_LENGTH)
		return;

	size_t dataSize = inout_eventsXml.size() - CRYPT_IV_LENGTH;

	u8 key[CRYPT_KEY_LENGTH];
	if (!GenerateTelemetryEncryptionKey(key))
		return;

	u8 IV[CRYPT_IV_LENGTH];
	memcpy(IV, inout_eventsXml.c_str(), CRYPT_IV_LENGTH);

	u8* inputBuffer = new u8[dataSize];
	memcpy(inputBuffer, &(inout_eventsXml.c_str()[CRYPT_IV_LENGTH]), dataSize);

	u8* outputBuffer = new u8[dataSize];

	AES_KEY aes;
	AES_set_decrypt_key(key, CRYPT_KEY_LENGTH * 8, &aes);
	AES_cbc_encrypt(inputBuffer, outputBuffer, dataSize, &aes, IV, AES_DECRYPT);

	inout_eventsXml.assign((char*)outputBuffer, dataSize);
}


void RgscTelemetryManager::AppendXmlEventTimestamp(RgscTelemetryEvent* event, u64 timestamp)
{
	char timestampStringBuffer[32];
	LARGE_INTEGER lint;
	lint.QuadPart = timestamp * 10000;

	FILETIME ft;
	ft.dwHighDateTime = lint.HighPart;
	ft.dwLowDateTime = lint.LowPart;

	SYSTEMTIME st;
	FileTimeToSystemTime(&ft, &st);
	sprintf_s(timestampStringBuffer, "%d-%02d-%02d %02d:%02d:%02d.%03d", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);

	event->AddStringField(RgscTelemetryEvent::FN_EVENT_TIMESTAMP, timestampStringBuffer);
}

RgscTelemetryEvent* RgscTelemetryManager::AppendNewXmlEventOfType(Event event)
{
	RgscTelemetryEvent* tmEvent = new RgscTelemetryEvent(RgscTelemetryEvent::MN_LAUNCHER_METRIC);

	// Set the type
	tmEvent->AddIntField(RgscTelemetryEvent::FN_EVENT_TYPE, event);

	// Set the timestamp
	AppendXmlEventTimestamp(tmEvent, CUtil::CurrentTimeMillis());

	// Set the version field (if known)
	if (sm_versionSet)
	{
		AppendXmlEventVersionInfo(tmEvent, sm_currentVersion);
	}

	// Set the IP addres (if known)
	if (!sm_ipAddress.empty())
	{
		tmEvent->AddStringField(RgscTelemetryEvent::FN_IP_ADDRESS, sm_ipAddress.c_str());
	}

	// Append to list of events
	sm_eventsAsXml.push_back(tmEvent);

	return tmEvent;
}

bool RgscTelemetryManager::IsEventNumberValid(Event event)
{
	return 
		(event >= EVENT_BOOTSTRAP_START && event <= EVENT_BOOTSTRAP_COMPLETED_UPDATE) ||
		(event >= EVENT_LAUNCHER_START  && event <= EVENT_LAUNCHER_RGSC_COMPLETED_UPDATE) ||
		(event == EVENT_LAUNCHER_SC_START) ||
		(event >= EVENT_LAUNCHER_GAME_CHECK_FOR_UPDATE && event <= EVENT_LAUNCHER_DIP_COMPLETED_UPDATE) ||
		(event >= EVENT_LAUNCHER_DOWNLOAD_STATS        && event <= EVENT_LAUNCHER_GEOLOCATION) ||
		(event == EVENT_LAUNCHER_INTERMEDIATE_DOWNLOAD_STATS) ||
		(event >= EVENT_LAUNCHER_MTL_BETA_CHECK_SUCCEEDED && event <= EVENT_LAUNCHER_MTL_STARTING_INSTALL);
}

void RgscTelemetryManager::SetCountryCode(const std::string& countryCode)
{
	sm_countryCode = countryCode;

	if (!sm_countryCode.empty() && !sm_ipAddress.empty())
	{
		WriteEventGeolocation();
	}
}

void RgscTelemetryManager::SetIpAddress(const std::string& ipAddress)
{
	sm_ipAddress = ipAddress;

	if (!sm_countryCode.empty() && !sm_ipAddress.empty())
	{
		WriteEventGeolocation();
	}
}

#endif

void RgscTelemetryManager::PopulateTelemetryDownloadStatsFromProgressCalculator(const ProgressCalculator& progCalc, DownloadStats& stats)
{
	stats.type = progCalc.GetDownloadType();

	const ProgressCalculator::DownloadStats& progStats = progCalc.GetDownloadStats();

	stats.bytesThisSession = progStats.bytesThisSession;
	stats.sessionDuration = progCalc.GetDownloadSessionDurationMillis() / 1000L;
	stats.connectionsOpened = progStats.connectionsOpened;
	stats.connectionsErrored = progStats.connectionsErrored;
	stats.verificationsFailed = progStats.verificationsFailed;
}
