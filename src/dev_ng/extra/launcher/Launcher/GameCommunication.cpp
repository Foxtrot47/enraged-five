#include "stdafx.h"

#include "resource.h"
#include "GameCommunication.h"
#include "Config.h"
#include "Util.h"
#include "IPatchCheck.h"
#include "UpdateCheck.h"
#include "DownloadManager.h"
#include "Application.h"
#include "WorkerThread.h"
#include "LauncherDlg.h"
#include "Fsm.h"
#include "InPlaceDownloader.h"

#include <sstream>

const int CGameCommunication::BASE64_SHIFT[4] =
{
	10, 4, 6, 8,
};

const char CGameCommunication::BASE64_ALPHABET[65] =
{
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	"abcdefghijklmnopqrstuvwxyz"
	"0123456789"
	"+/"
};

#define ENCRYPT_COMMS 1
#if LAUNCHER_CHALLENGE_RESPONSE
	// Libraries
	#pragma comment(lib, "TFIT_SHA_HMAC_iHMACredK_64.lib")
	#pragma comment(lib, "TFIT_ECC_DH_iECDHgreenSSfastP256_64.lib")
	#pragma comment(lib, "TFIT_AES_CBC_Encrypt_iAES13_64.lib")
	#pragma comment(lib, "TFIT_AES_CBC_Decrypt_iAES14_64.lib")
	#pragma comment(lib, "TFIT_Utils_64.lib")
	
	// Our own headers
	#include "TFIT_ecc_fast_iECDHgreenSSfastP256.h"
	#include "TFIT_hmac_key_iHMACredK.h"

// This needs to not be scoped by class, per recommendation of Arxan's guides
int ephemeralFast(const unsigned int data_size, wbecc_enc_t *const data, const wbecc_enc_cfg * const cfg)
{
	//@@: range EPHEMERALFAST {
	(void)cfg;
	RAND_bytes(data, data_size);
	return 0;
	//@@: } EPHEMERALFAST

}

void CGameCommunication::hexArrayToString(char *dst, unsigned char *src, unsigned int len)
{
	// Set the pointer
	char * str = dst;
	unsigned int i = 0;
	// Sprintf the data
	for (i=0; i<len; i++) 
		sprintf(str+i*2, "%02X", src[i]);
	// Set my NULL pointer
	str[i*2] = 0;

}
void CGameCommunication::hexStringToArray(unsigned char * dst, const char *src, unsigned int len)
{
	// Shorten the length down
	int byteArrayLen = (len-1) / 2;
	// Set a walking pointer
	const char * ptrWalker = src;
	// Get a temporary character; for some reason directly writing to the address does too much

	// scanff is expecting int(4), not char (1). Reading into a char directly causes stack corruption.
	// Read as an int, then cast to a charge to use the lower 8 bits.
	int temp = 0;

	// Hammer in the data
	//@@: range CGAMECOMMUNICATION_HEXSTRINGTOARRAY_CONVERT {
	for(int i = 0; i < byteArrayLen; i++) 
	{
		sscanf(ptrWalker, "%02x", &temp);
		dst[i] = (char)temp;
		ptrWalker += 2 * sizeof(char);
	}
	//@@: } CGAMECOMMUNICATION_HEXSTRINGTOARRAY_CONVERT

}
#if !RSG_FINAL
	void CGameCommunication::printPointer(char * indicator, const unsigned char * ptr, unsigned int len)
	{

		char debugBuff[GENERIC_BUFFER_SIZE] = {0};
		char *debugBuffPtr = debugBuff;
		int debugBuffPtrIdx = 0;
		debugBuffPtrIdx+=sprintf(debugBuffPtr+debugBuffPtrIdx, "%s:		0x:", indicator);

		for(unsigned int i = 0; i < len; i ++)
			debugBuffPtrIdx+=sprintf(debugBuffPtr+debugBuffPtrIdx, "%02X",ptr[i] );
		DEBUGF1("%s", debugBuff);

	}
#endif


#endif

void CGameCommunication::InitializeChallengeResponse()
{

	
#if LAUNCHER_CHALLENGE_RESPONSE
	// Initialize our ECC key.
#if !RSG_FINAL
	int ret = 
#endif
	//@@: range CGAMECOMMUNICATION_INITIALIZECHALLENGERESPONSE {
	TFIT_init_wbecc_dh_iECDHgreenSSfastP256(
		&m_launcherPipeEccCtx, // the context to use
		&TFIT_ecc_fast_iECDHgreenSSfastP256,
		&ephemeralFast // a stand in ephemeral generation function
		);
	DEBUGF1("TFIT_init_wbecc_dh_iECDHgreenSSfastP256: %d", ret);
#if !RSG_FINAL
	ret += 
#endif
		TFIT_wbecc_dh_generate_key_iECDHgreenSSfastP256(&m_launcherPipeEccCtx);
	DEBUGF1("TFIT_wbecc_dh_generate_key_iECDHgreenSSfastP256 : %d", ret);

#if !RSG_FINAL
	ret += 
#endif
		TFIT_wbecc_dh_get_ephemeral_public_key_iECDHgreenSSfastP256(
		&m_launcherPipeEccCtx, // the TransformIT context
		m_launcherPipeEccKey, // the buffer to store the ephmeral key in
		sizeof(m_launcherPipeEccKey), // the size of the buffer to store the ephemeral key in
		&m_launcherPipeEccKeyLength // will be written with how long of
		);
	//@@: } CGAMECOMMUNICATION_INITIALIZECHALLENGERESPONSE


	DEBUGF1("TFIT_wbecc_dh_get_ephemeral_public_key_iECDHgreenSSfastP256 : %d", ret);
#if !RSG_FINAL
	printPointer("Launcher key", m_launcherPipeEccKey, m_launcherPipeEccKeyLength);
#endif
#endif
}
CGameCommunication::CGameCommunication() : m_shuttingDown(false), m_pipeWaitingToConnect(NULL), m_thread(NULL)
{
	InitializeCriticalSection(&critsec);
}

CGameCommunication::~CGameCommunication()
{
	DeleteCriticalSection(&critsec);
}

CGameCommunication& CGameCommunication::Instance()
{
	static CGameCommunication instance;
	return instance;
}

void CGameCommunication::Listen()
{
	// Don't start another listening thread if one is already runnning
	if (m_thread != NULL)
	{
		DEBUGF1("GameComm: Already listening!");
		return;
	}

	struct Closure
	{
		static UINT invoke(LPVOID param)
		{
			CUtil::SetCurrentThreadName("GameCommunication Thread");

			CGameCommunication* gameCom = (CGameCommunication*)param;
			gameCom->Start();

			// Mark thread as having finished
			gameCom->m_thread = NULL;

			return 0;
		}
	};

	//@@: location CGAMECOMMUNICATION_LISTEN_START_THREAD
	m_thread = AfxBeginThread(Closure::invoke, this);
}

void CGameCommunication::StopListening()
{
	m_shuttingDown = true;

	EnterCriticalSection(&critsec);
		if (m_thread)
		{
			if (m_pipeWaitingToConnect)
			{
				m_pipeWaitingToConnect->Shutdown();
			}
		}
	LeaveCriticalSection(&critsec);

	if (m_thread)
	{
		WaitForSingleObject(m_thread->m_hThread, INFINITE);
	}

	DWORD handleCount = 0;
	HANDLE* handles = NULL;
	EnterCriticalSection(&critsec);
		handleCount = (DWORD)pipes.size();

		if (handleCount > 0)
		{
			handles = new HANDLE[handleCount];
			for (size_t i = 0; i < pipes.size(); i++)
			{
				pipes[i]->Shutdown();
				handles[i] = pipes[i]->GetThread();
			}
		}
	LeaveCriticalSection(&critsec);

	if (handleCount > 0)
	{
		WaitForMultipleObjects(handleCount, handles, TRUE, INFINITE);

		delete[] handles;
	}
}

bool CGameCommunication::IsListening()
{
	return (m_thread != NULL);
}

bool CGameCommunication::Start()
{
	// Keep listening for new connections
	while (!m_shuttingDown)
	{
		EnterCriticalSection(&critsec);
		m_pipeWaitingToConnect = new CConnectedPipe();
		LeaveCriticalSection(&critsec);

		// Wait for connection
		bool result = m_pipeWaitingToConnect->Connect();

		if (m_shuttingDown)
			break;
		
		if (result)
		{
			// Add to the list, which kicks off a new listening thread
			// The object is deleted in RemovePipe, after the thread finishes
			AddPipe(m_pipeWaitingToConnect);
		}
		else
		{
			// Unable to connect.  Keep trying until we're told to stop.
			DEBUGF1("Unable to create named pipe connection!");
		}
	}

	EnterCriticalSection(&critsec);
	if (m_pipeWaitingToConnect)
	{
		delete m_pipeWaitingToConnect;
		m_pipeWaitingToConnect = NULL;
	}
	LeaveCriticalSection(&critsec);

	return 0;
}

void CGameCommunication::AddPipe(CConnectedPipe* pipe)
{
	struct Closure
	{
		static UINT ThreadEntry(void* ptr)
		{
			((CConnectedPipe*)ptr)->Listen();

			// Listen loop has finished; remove this pipe
			CGameCommunication::Instance().RemovePipe((CConnectedPipe*)ptr);

			return 0;
		}
	};

	EnterCriticalSection(&critsec);
	//@@: location CGAMECOMMUNICATION_ADDPIPE_PUSH_PIPE
	pipes.push_back(pipe);

	CWinThread* thread = AfxBeginThread(Closure::ThreadEntry, pipe, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
	pipe->SetThread(thread->m_hThread);
	thread->ResumeThread();

	LeaveCriticalSection(&critsec);
}

void CGameCommunication::RemovePipe(CConnectedPipe* pipe)
{
	EnterCriticalSection(&critsec);

		for (size_t i = 0; i < pipes.size(); i++)
		{
			if (pipes[i] == pipe)
			{
				pipes.erase(pipes.begin() + i);
				delete pipe;
				break;
			}
		}

	LeaveCriticalSection(&critsec);
}

bool CGameCommunication::Base64Encode(const u8* src, const unsigned srcLen, char* dst, const unsigned dstMaxSize, unsigned* charsUsed)
{
	if(!src || !srcLen || !dstMaxSize || !dst)
	{
		return false;
	}

	const unsigned maxSize = GetMaxBase64EncodedSize(srcLen);

	if(dstMaxSize < maxSize)
	{
		return false;
	}

	memset(dst, 0, dstMaxSize);

	int idx0 = 0;    
	int idx1 = 6;

	const int numBits = (int)srcLen * 8;
	unsigned len = (numBits + 5) / 6;

	for(unsigned i = 0; i < len; ++i, idx0 += 6, idx1 += 6)
	{
		unsigned tmp = (src[idx0 >> 3] << 8);
		if(idx1 < numBits)
		{
			tmp |= src[idx1 >> 3];
		}

		const int shift = BASE64_SHIFT[i & 0x03];
		const u8 val = (u8) (tmp >> shift) & 0x3F;

		dst[i] = BASE64_ALPHABET[val];
	}

	while(len < maxSize-1)
	{
		dst[len++] = '=';
	}

	if (len < dstMaxSize)
	{
		dst[len] = '\0';
	}

	if(charsUsed){*charsUsed = maxSize;}

	return true;
}

unsigned CGameCommunication::GetMaxBase64EncodedSize(const unsigned size)
{
	//Add 1 for the terminating null.
	return (((size + 2) / 3) * 4) + 1;
}

void CGameCommunication::WriteMessage(const char* message)
{
	DEBUGF3("Writing pipe message: %s", message);
	EnterCriticalSection(&critsec);

		size_t messageSize = strlen(message);
		char* buffer = new char[4+messageSize];
		buffer[0] = (messageSize >>  0) & 0xFF;
		buffer[1] = (messageSize >>  8) & 0xFF;
		buffer[2] = (messageSize >> 16) & 0xFF;
		buffer[3] = (messageSize >> 24) & 0xFF;

		// Note: don't need the NULL-terminator
		memcpy(&(buffer[4]), message, messageSize);

		for (size_t i = 0; i < pipes.size(); i++)
		{
			pipes[i]->WriteRaw(buffer, (u32)(4+messageSize));
		}

		delete[] buffer;

	LeaveCriticalSection(&critsec);
}

void CGameCommunication::SendCompleteMessage(const std::string& sku)
{
	std::string message("COMP:");
	message += sku;
	message += ":";
	WriteMessage(message.c_str());
}

void CGameCommunication::SendProgressMessage(const std::string& sku, int progressPercent)
{
	std::stringstream ss;
	ss << "PROG:" << sku << ":" << progressPercent;
	std::string message = ss.str();
	WriteMessage(message.c_str());
}

void CGameCommunication::SendErrorMessage(const std::string& sku, const std::string& errorCode)
{
	std::string message("ERRO:");
	message += sku;
	message += ":";
	message += errorCode;
	WriteMessage(message.c_str());
}

void CGameCommunication::SendAliveMessage()
{
	WriteMessage("PING:launcher:");
}

void CGameCommunication::SendUACMessage()
{
	WriteMessage("UACE:launcher:");
}

void CGameCommunication::SendTicketUpdate(rgsc::RockstarId rockstarId, const char* scTicketMsg)
{
	DEBUGF1("SendTicketUpdate for RockstarId: %llu", rockstarId);

	unsigned len = (unsigned)strlen(scTicketMsg);
	int maxSize = GetMaxBase64EncodedSize(len);
	char* buf = new char[maxSize];
	if (buf)
	{
		unsigned encoded = 0;
		if (Base64Encode((const u8*)scTicketMsg, len, buf, maxSize, &encoded))
		{
			std::stringstream message;
			message << "LSCT:" << rockstarId << ":" << buf;

			WriteMessage(message.str().c_str());
		}

		delete[] buf;
	}
}

#if LAUNCHER_CHALLENGE_RESPONSE
void CGameCommunication::CalculateSharedSecret(const char *ptr)
{

	// Get the game's ECC key from the c-string buffer, as a byte array
	uint8_t gamePipeEccKey[CURVE_BUFFER_SIZE] = {0};
	//@@: location CGAMECOMMUNICATION_CALCULATESHAREDSECRET_FORMAT_GAME_ECC_KEY
	uint8_t iv[IV_SIZE];
	hexStringToArray(iv, ptr, IV_STRING_SIZE);
	hexStringToArray(gamePipeEccKey, ptr+IV_STRING_SIZE-1, CURVE_BUFFER_STRING_SIZE);

	//@@: range CGAMECOMMUNICATION_CALCULATESHAREDSECRET_DO_CALCULATIONS {
	// Set the IV's
	memcpy(m_encryptionIV, iv, IV_SIZE);
	memcpy(m_decryptionIV, iv, IV_SIZE);
	// Set some initial variables
	uint8_t sharedSecret[SHARED_SECRET_SIZE];
	unsigned int sharedSecretLen = 0;
	unsigned int dynamicKeyLength = 0;

	// Compute our secret from what we've gotten from the launcher
	//@@: location CGAMECOMMUNICATION_CALCULATESHAREDSECRET_COMPUTE_SECRET
#if !RSG_FINAL
	int ret = 
#endif
		TFIT_wbecc_dh_compute_secret_iECDHgreenSSfastP256(
		&m_launcherPipeEccCtx,
		TFIT_WBECC_DH_EPHEMERAL_UNIFIED_MODEL,
		NULL, 
		gamePipeEccKey, 
		m_launcherPipeEccKeyLength,
		sharedSecret,
		SHARED_SECRET_SIZE,
		&sharedSecretLen);

#if !RSG_FINAL
	DEBUGF1("TFIT_wbecc_dh_compute_secret_iECDHgreenSSfastP256:%d", ret);
	printPointer("Shared Secret", sharedSecret, sharedSecretLen);
#endif

	//@@: location CGAMECOMMUNICATION_CALCULATESHAREDSECRET_PREPARE_DYNAMIC_KEYS
#if !RSG_FINAL
	ret+=
#endif 
		TFIT_prepare_dynamic_key_iAES13_green(sharedSecret, sharedSecretLen/2, (uint8_t *)&m_launcherPipeEncryptionKey, sizeof(m_launcherPipeEncryptionKey), &dynamicKeyLength);
#if !RSG_FINAL
	DEBUGF1("TFIT_prepare_dynamic_key_iAES13_green %d", ret);
	//printPointer("TFIT_prepare_dynamic_key_iAES13_green", (uint8_t *)&m_launcherPipeEncryptionKey, sizeof(m_launcherPipeEncryptionKey));
	ret+=
#endif
		TFIT_prepare_dynamic_key_iAES14_green(sharedSecret,sharedSecretLen/2, (uint8_t *)&m_launcherPipeDecryptionKey, sizeof(m_launcherPipeDecryptionKey), &dynamicKeyLength);
#if !RSG_FINAL
	DEBUGF1("TFIT_prepare_dynamic_key_iAES14_green %d", ret);
	//printPointer("TFIT_prepare_dynamic_key_iAES14_green", (uint8_t *)&m_launcherPipeDecryptionKey, sizeof(m_launcherPipeDecryptionKey));
#endif
	//@@: } CGAMECOMMUNICATION_CALCULATESHAREDSECRET_DO_CALCULATIONS

	//@@: location CGAMECOMMUNICATION_CALCULATESHAREDSECRET_EXIT_POINT
}

void CGameCommunication::SendLauncherKey()
{
	//@@: range CGAMECOMMUNICATION_SENDLAUNCHERKEY {
	// Declare my variables
	char outputBuffer[GENERIC_BUFFER_SIZE] = {0};
	char str[CURVE_BUFFER_STRING_SIZE] = {0};
	// Shove into an ASCII string for thhe game 
	hexArrayToString(str, m_launcherPipeEccKey, m_launcherPipeEccKeyLength);
	// Sprintf to correct foramtting
 	sprintf(outputBuffer, "TINI:launcher:%s",str);
	// Write it out to the pipe
	WriteMessage(outputBuffer);
	//memset(m_encryptionIV, 0, sizeof(m_encryptionIV));
	//memset(m_decryptionIV, 0, sizeof(m_decryptionIV));
	//@@: } CGAMECOMMUNICATION_SENDLAUNCHERKEY


}


void CGameCommunication::Decrypt(unsigned char *dst, const unsigned char *src)
{
#if !RSG_FINAL
	int retVal = 0;
	LARGE_INTEGER start, end, freq;
	// Write out the counter variable
	char ctrBuff[1024];
	hexArrayToString(ctrBuff, m_decryptionIV, 16);
	DEBUGF1("pre_decrypt:block_t counter:0x%s",ctrBuff);
	printPointer("pre_decrypt:input", src, CHALLENGE_SIZE);
	printPointer("pre_decrypt:desti", dst, CHALLENGE_SIZE);
	QueryPerformanceFrequency(&freq);
	QueryPerformanceCounter(&start); 

	//printPointer("pre_decrypt:mykey", (const unsigned char *)&m_launcherPipeDecryptionKey, sizeof(TFIT_key_iAES14_t));
	retVal+=
#endif

	//@@: range CGAMECOMMUNICATION_DECRYPT {
		TFIT_wbaes_cbc_decrypt_iAES14(&m_launcherPipeDecryptionKey, src, HMAC_SIZE, m_decryptionIV, dst);
	//@@: } CGAMECOMMUNICATION_DECRYPT


#if !RSG_FINAL
	QueryPerformanceCounter(&end);


	DEBUGF1("TFIT_wbaes_cbc_encrypt_iAES13 Result: %d", retVal);
	DEBUGF1("TFIT_wbaes_cbc_encrypt_iAES13: %d microseconds", (end.QuadPart - start.QuadPart) * 1000000 / freq.QuadPart);
	hexArrayToString(ctrBuff, m_decryptionIV, 16);
	DEBUGF1("pst_decrypt:block_t counter:0x%s",ctrBuff);
	//printPointer("pst_decrypt:mykey", (const unsigned char *)&m_launcherPipeDecryptionKey, sizeof(TFIT_key_iAES14_t));
	printPointer("pst_decrypt:input", src, CHALLENGE_SIZE);
	printPointer("pst_decrypt:desti", dst, CHALLENGE_SIZE);
	// Display our performance metrics

	char decryptedBufferStr[1024];
	hexArrayToString(decryptedBufferStr, dst, CHALLENGE_SIZE);
#endif

}

void CGameCommunication::Encrypt(unsigned char *dst, const unsigned char *src)
{

#if !RSG_FINAL
	int retVal = 0;
	LARGE_INTEGER start, end, freq;
	// Write out the counter variable
	char ctrBuff[1024];
	hexArrayToString(ctrBuff, m_encryptionIV, 16);
	DEBUGF1("pre_encrypt:block_t counter:0x%s",ctrBuff);
	printPointer("pre_encrypt:input", src, CHALLENGE_SIZE);
	printPointer("pre_encrypt:desti", dst, CHALLENGE_SIZE);
	QueryPerformanceFrequency(&freq);
	QueryPerformanceCounter(&start); 
	
	//printPointer("pre_encrypt:mykey", (const unsigned char *)&m_launcherPipeEncryptionKey, sizeof(TFIT_key_iAES13_t));
	retVal+=
#endif

	//@@: range CGAMECOMMUNICATION_ENCRYPT {
		TFIT_wbaes_cbc_encrypt_iAES13(&m_launcherPipeEncryptionKey, src, HMAC_SIZE, m_encryptionIV, dst);
	//@@: } CGAMECOMMUNICATION_ENCRYPT 


#if !RSG_FINAL
	QueryPerformanceCounter(&end);


	DEBUGF1("TFIT_wbaes_cbc_encrypt_iAES13 Result: %d", retVal);
	DEBUGF1("TFIT_wbaes_cbc_encrypt_iAES13: %d microseconds", (end.QuadPart - start.QuadPart) * 1000000 / freq.QuadPart);

	//printPointer("pst_encrypt:mykey", (const unsigned char *)&m_launcherPipeEncryptionKey, sizeof(TFIT_key_iAES13_t));
	printPointer("pst_encrypt:input", src, CHALLENGE_SIZE);
	printPointer("pst_encrypt:desti", dst, CHALLENGE_SIZE);
	// Display our performance metrics

	char decryptedBufferStr[1024];
	hexArrayToString(decryptedBufferStr, dst, CHALLENGE_SIZE);
#endif

}
void CGameCommunication::CalculateAndSendHMAC(const char *ptr)
{
#if !RSG_FINAL
	// Setup our performance variables
	LARGE_INTEGER start, end, freq;
	QueryPerformanceFrequency(&freq);
	QueryPerformanceCounter(&start); 
	int retVal = 0;
#endif

	// Convert the bytes to a byte-array
	uint8_t gameChallenge[CHALLENGE_SIZE];
	DEBUGF1("TRVR:encrypted:%s", ptr);
	//@@: range CGAMECOMMUNICATION_CALCULATEANDSENDHMAC {
	//@@: location CGAMECOMMUNICATION_CALCULATEANDSENDHMAC_FORMAT_CHALLENGE
	hexStringToArray(gameChallenge, ptr, CHALLENGE_STRING_SIZE);

	
	// Decrypt the challenge
	unsigned char decryptedBuffer[GENERIC_BUFFER_SIZE] = {0};
	// Encrypt the message before writing it back out to the buffer
#if ENCRYPT_COMMS
	//@@: location CGAMECOMMUNICATION_CALCULATEANDSENDHMAC_CALL_DECRYPT
	Decrypt(decryptedBuffer, gameChallenge);
#endif

#if !RSG_FINAL && ENCRYPT_COMMS
	char decryptedBufferStr[1024];
	hexArrayToString(decryptedBufferStr, decryptedBuffer, CHALLENGE_SIZE);
	DEBUGF1("TRVR:decrypted:%s",decryptedBufferStr);
#endif



	/*
	* Initialize the Transformit whitebox HMAC-SHA-256 instance.
	*/
	wbsha_hmac_key_t *hmacKey = (wbsha_hmac_key_t*)&TFIT_hmac_key_iHMACredK;
	wbsha_hmac_ctx_t hmacCtx;
#if !RSG_FINAL
	retVal += 
#endif
		TFIT_init_wbsha_hmac_iHMACredK(
		&hmacCtx, // Transformit context
		hmacKey, // The whitebox HMAC key
		TFIT_WBSHA_SHA2_256 // Specify SHA-256 as the underlying hash function
		);

	DEBUGF1("TFIT_init_wbsha_hmac_iHMACredK: %d", retVal);

	/*
	* Feed the message buffer into the Transformit HMAC-SHA-256
	* instance.
	*/
#if !RSG_FINAL
#if ENCRYPT_COMMS
	printPointer("TFIT_init_wbsha_hmac_iHMACredK:input", decryptedBuffer, HMAC_SIZE);
#else
	printPointer("TFIT_init_wbsha_hmac_iHMACredK:input", gameChallenge, HMAC_SIZE);
#endif
	retVal = 
#endif
		//@@: location CGAMECOMMUNICATION_CALCULATEANDSENDHMAC_UPDATE_HMAC
		TFIT_update_wbsha_hmac_iHMACredK(
		&hmacCtx, // TransformIT context
#if ENCRYPT_COMMS
		decryptedBuffer, // The buffer to generate an HMAC tag for
#else
		gameChallenge, 
#endif
		HMAC_SIZE	// The size of the buffer
		);

#if !RSG_FINAL
	QueryPerformanceCounter(&end);
	DEBUGF1("TFIT_update_wbsha_hmac_iHMACredK: %d", retVal);
	DEBUGF1("TFIT_update_wbsha_hmac_iHMACredK: %d microseconds", (end.QuadPart - start.QuadPart) * 1000000 / freq.QuadPart);
	QueryPerformanceCounter(&start); 
#endif


	/*
	* Generate the HMAC tag using Trasnformit whitebox
	* HMAC-SHA-256
	*/
	uint8_t hmacTag[HMAC_SIZE];
	unsigned int hmacTagLen = 0;
#if !RSG_FINAL
	retVal = 
#endif
		TFIT_final_wbsha_hmac_iHMACredK(
		&hmacCtx,			// TransformIT context
		WBSHA_HMAC_SIGN,	// Specify tag generation
		NULL,				// No signature to verify
		0,					// No signature legnth
		hmacTag,			// Store results here
		HMAC_SIZE,			// Size of the results buffer
		&hmacTagLen			// Record the length of the generated tag
		);

#if !RSG_FINAL
	QueryPerformanceCounter(&end);
	DEBUGF1("TFIT_final_wbsha_hmac_iHMACredK Result: %d", retVal);
	DEBUGF1("TFIT_final_wbsha_hmac_iHMACredK: %d microseconds", (end.QuadPart - start.QuadPart) * 1000000 / freq.QuadPart);
	DEBUGF1("TFIT_final_wbsha_hmac_iHMACredK:tag_len:%d", hmacTagLen);
	QueryPerformanceCounter(&start);
	char hmacBuff[1024];
	hexArrayToString(hmacBuff, hmacTag, HMAC_SIZE);
	DEBUGF1("MCHL:decrypted:%s",hmacBuff);
#endif


	//@@: location CGAMECOMMUNICATION_CALCULATEANDSENDHMAC_ENCRYPT_DATA
	// Now Encrypt the Data
	unsigned char encryptedBuffer[HMAC_SIZE] = {0};

#if ENCRYPT_COMMS
	Encrypt(encryptedBuffer, hmacTag);
#endif
	char outputBuffer[GENERIC_BUFFER_SIZE] = {0};
	char str[CURVE_BUFFER_STRING_SIZE] = {0};
#if ENCRYPT_COMMS
	hexArrayToString(str, encryptedBuffer, hmacTagLen);
#else
	hexArrayToString(str, hmacTag, hmacTagLen);
#endif
	sprintf(outputBuffer, "MCHL:launcher:%s",str);
#if !RSG_FINAL
	DEBUGF1("MCHL:encrypted:%s", str);
#endif
	//@@: } CGAMECOMMUNICATION_CALCULATEANDSENDHMAC

	//@@: location CGAMECOMMUNICATION_CALCULATEANDSENDHMAC_WRITE_MESSAGE
	WriteMessage(outputBuffer);
}
#endif 
CGameCommunication::CConnectedPipe::CConnectedPipe()
	: pipe(INVALID_HANDLE_VALUE), event(INVALID_HANDLE_VALUE), m_bPipeConnected(false), m_hThread(NULL)
{

}

CGameCommunication::CConnectedPipe::~CConnectedPipe()
{
	if (pipe != INVALID_HANDLE_VALUE)
	{
		CloseHandle(pipe);
		pipe = INVALID_HANDLE_VALUE;
	}

	if (event != INVALID_HANDLE_VALUE)
	{
		CloseHandle(event);
		event = INVALID_HANDLE_VALUE;
	}
}

bool CGameCommunication::CConnectedPipe::Connect()
{
	// Create new instance of the pipe
	DEBUGF1("GameComm: Creating named pipe instance...");
	pipe = CreateNamedPipe(Constants::IpcPipeName.Get().c_str(), PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED, PIPE_TYPE_BYTE, 255, PIPE_BUFFER_SIZE, PIPE_BUFFER_SIZE, 0, NULL);

	if (pipe == NULL || pipe == INVALID_HANDLE_VALUE)
	{
		ERRORF("GameComm: Unable to create named pipe!");
		return false;
	}

	ZeroMemory(&overlapped, sizeof(overlapped));
	overlapped.hEvent = event = CreateEvent(NULL, TRUE, TRUE, NULL);

	// Wait for a connection
	DEBUGF1("GameComm: Waiting for connections on named pipe instance...");
	//@@: location CCCONNECTEDPIPE_CONNECT_CONNECT_NAMED_PIPE
	BOOL bResult = ConnectNamedPipe(pipe, &overlapped);
	if (!bResult)
	{
		DWORD err = GetLastError();

		if (err != ERROR_IO_PENDING)
		{
			ERRORF("GameComm: Unable to connect pipe.");
			CUtil::PrintSystemErrorMessage(err);
			CloseHandle(pipe);
			pipe = INVALID_HANDLE_VALUE;
			return false;
		}
	}

	WaitForSingleObject(event, INFINITE);

	DWORD transferred;
	if (pipe == INVALID_HANDLE_VALUE || !GetOverlappedResult(pipe, &overlapped, &transferred, TRUE))
	{
		return false;
	}
	
	return true;
}

void CGameCommunication::CConnectedPipe::Shutdown()
{
	if (pipe != INVALID_HANDLE_VALUE)
	{
		HANDLE localPipe = pipe;
		pipe = INVALID_HANDLE_VALUE;

		DeleteFile(Constants::IpcPipeName.Get().c_str());
		DisconnectNamedPipe(localPipe);
		CloseHandle(localPipe);
	}
}

void CGameCommunication::CConnectedPipe::Listen()
{
	DEBUGF1("Listening to pipe on new thread...");
	char buffer[PIPE_BUFFER_SIZE];
	DWORD cbBytesRead = 0;

	BOOL bResult;

	while (pipe != INVALID_HANDLE_VALUE)
	{
		// Blocks waiting for input
		bResult = ReadFile(pipe, buffer, PIPE_BUFFER_SIZE, &cbBytesRead, NULL);

		if (!bResult)
		{
			ERRORF("GameComm: ReadFile failed.");
			break;
		}
		else if (cbBytesRead < MSG_HEADER_SIZE)
		{
			ERRORF("GameComm: Read %u bytes (insufficient for header)");
			break;
		}
		else if (cbBytesRead > PIPE_BUFFER_SIZE)
		{
			// This should never happen
			ERRORF("GameComm: Read %u bytes.");
			break;
		}
		else
		{
			ParseMessages(buffer, cbBytesRead);
		}
	}

	if (pipe != INVALID_HANDLE_VALUE)
	{
		bResult = DisconnectNamedPipe(pipe);
		if (!bResult)
		{
			ERRORF("GameComm: Unable to disconnect pipe.");
			CUtil::PrintSystemErrorMessage(GetLastError());
		}

		CloseHandle(pipe);
		pipe = INVALID_HANDLE_VALUE;
	}
}


void CGameCommunication::CConnectedPipe::ParseMessages(const char* buffer, u32 cbBytesRead)
{
	DEBUGF1("Parsing messages...");
	DWORD offset = 0;
	int contentSize = 0;
	// Fixing the parsing of messages
	char content[PIPE_BUFFER_SIZE] = {0};

	while (offset < cbBytesRead)
	{
		DWORD cursor = offset;
		DWORD max_cursor = cbBytesRead;

		if (cursor + 4 > max_cursor)
		{
			ERRORF("GameComm: Insufficient data to read message size.");
			break;
		}
		// And them against the masks accordingly
		contentSize = (	((buffer[cursor+3] << 24)	& 0xFF000000) |
						((buffer[cursor+2] << 16)	& 0x00FF0000) |
						((buffer[cursor+1] << 8)	& 0x0000FF00) |
						((buffer[cursor+0])			& 0x000000FF));
		cursor += 4;

		if (cursor + contentSize > max_cursor)
		{
			ERRORF("GameComm: Pipe message longer than read data.");
			break;
		}

		// Limit cursor to size of message
		max_cursor = cursor + contentSize;

		if (contentSize == 0)
		{
			ERRORF("GameComm: Pipe message empty.");
			break;
		}


		// Read the content
		for (int i = 0; i < contentSize && cursor < max_cursor; i++)
		{
			content[i] = buffer[cursor];
			content[i + 1] = '\0';
			cursor++;
		}

		HandleMessage(content, contentSize);

		offset += (contentSize + MSG_HEADER_SIZE);
		if (offset != cursor)
		{
			WARNINGF("GameComm: Cursor isn't where we expected it to be.");
		}
	}
}

void CGameCommunication::CConnectedPipe::HandleMessage(const char* messageBuffer, u32 bufferSize)
{
#if RSG_FINAL
	(void)bufferSize;
#endif
	DEBUGF3("GameComm: Handling message of %u bytes: %s", bufferSize, messageBuffer);
	//@@: range CGAMECOMMUNICATION_CCONNECTEDPIPEUHANDLEMESSAGE {

	if (CUtil::StringStartsWith(messageBuffer, "RESTART"))
	{
		LauncherApp::RestartGame();
	}
#if !RSG_STEAM_LAUNCHER
	//@@: location CGAMECOMMUNICATION_CCONNECTEDPIPEUHANDLEMESSAGE_ENTRY 
	else if (CUtil::StringStartsWith(messageBuffer, "DOWN:"))
	{
		std::vector<std::string> tokens;
		CUtil::SplitString(messageBuffer, ':', tokens);

		if (tokens.size() == 2)
		{
			DEBUGF1("Downloading DIP data for SKU %s", tokens[1].c_str());
			InPlaceDownloader::StartDownloadingFilesForSku(tokens[1]);
		}
		else
		{
			ERRORF("Invalid download request: %s", messageBuffer);
		}
	}
	else if (CUtil::StringStartsWith(messageBuffer, "DOWNURL\n"))
	{
		Patch patch;
		patch.BackgroundDownload = true;

		std::vector<std::string> tokens;
		CUtil::SplitString(messageBuffer, '\n', tokens);

		if (tokens.size() < 3)
		{
			ERRORF("Insufficient tokens to download file: %s", messageBuffer);
			return;
		}

		patch.SkuName = tokens[1];
		patch.PatchURL = tokens[2];

		if (tokens.size() > 3)
		{
			patch.LocalFilename = tokens[3];
		}
		else
		{
			char nameBuffer[256];
			sprintf_s(nameBuffer, 256, "patch_%x.exe", rand());
			patch.LocalFilename = nameBuffer;
		}

		if (tokens.size() > 4)
		{
			patch.CommandlineArguments = tokens[4];
		}

		DEBUGF1("Downloading patch with SKU '%s' from URL '%s' with local filename '%s' and parameters '%s'", patch.SkuName.c_str(), patch.PatchURL.c_str(), patch.LocalFilename.c_str(), patch.CommandlineArguments.c_str());
		UpdateChecker::Instance().AddPatch(patch);
		if (!LauncherApp::IsDownloadingPatch())
		{
			WorkerThread::Instance().AddJob(WorkerThread::JOB_DOWNLOAD_UPDATE);
		}
	}
	else if (CUtil::StringStartsWith(messageBuffer, "PAUSE"))
	{
		Downloader::Instance().SetPaused(true);
	}
	else if (CUtil::StringStartsWith(messageBuffer, "RESUME"))
	{
		Downloader::Instance().SetPaused(false);
	}
#endif
	else if (CUtil::StringStartsWith(messageBuffer, "SCTC:"))
	{
		std::vector<std::string> tokens;
		CUtil::SplitString(messageBuffer, ':', tokens);

		if (tokens.size() == 3)
		{
			rgsc::RockstarId id;
			if (sscanf_s(tokens[1].c_str(), "%I64d", &id) == 1)
			{
				DEBUGF1("Social Club ticket changed for Rockstar ID (%s), XmlResponse64: %s", tokens[1].c_str(), tokens[2].c_str());
				LauncherApp::UpdateSocialClubTicket(id, tokens[2].c_str());
			}
		}
		else if (tokens.size() > 3)
		{
			std::string ticket = "";
			for (int i = 2; i < tokens.size(); i++)
			{
				ticket += tokens[i];
			}

			rgsc::RockstarId id;
			if (sscanf_s(tokens[1].c_str(), "%I64d", &id) == 1)
			{
				DEBUGF1("Social Club ticket changed for Rockstar ID (%s), XmlResponse64: %s", tokens[1].c_str(), tokens[2].c_str());
				LauncherApp::UpdateSocialClubTicket(id, ticket);
			}
		}
		else
		{
			ERRORF("Invalid ticket change event : %s", messageBuffer);
		}
	}
	else if (CUtil::StringStartsWith(messageBuffer, "SCSO"))
	{
		DEBUGF1("Social Club sign out.");
		LauncherApp::GameSignOutEvent();
	}
	else if (CUtil::StringStartsWith(messageBuffer, "SCSI:"))
	{
		std::vector<std::string> tokens;
		CUtil::SplitString(messageBuffer, ':', tokens);

		if (tokens.size() == 2)
		{
			rgsc::RockstarId id;
			if (sscanf_s(tokens[1].c_str(), "%I64d", &id) == 1)
			{
				DEBUGF1("Social Club sign in for Rockstar ID (%s)", tokens[1].c_str());
				LauncherApp::GameSignInEvent(id);
			}
		}
		else
		{
			ERRORF("Invalid social club sign in event : %s", messageBuffer);
		}
	}
#if LAUNCHER_CHALLENGE_RESPONSE
	//@@: range CGAMECOMMUNICATION_CCONNECTEDPIPE_HANDLEMESSAGE_CHALLENGE_RESPONSE {
	else if (CUtil::StringStartsWith(messageBuffer, "INIT"))
	{
		//@@: location CGAMECOMMUNICATION_CCONNECTEDPIPE_HANDLEMESSAGE_CHALLENGE_RESPONSE_INIT
		CGameCommunication::Instance().InitializeChallengeResponse();
		//@@: location CGAMECOMMUNICATION_CCONNECTEDPIPE_HANDLEMESSAGE_SEND_LAUNCHER_KEY
		CGameCommunication::Instance().SendLauncherKey();
		CGameCommunication::Instance().CalculateSharedSecret(messageBuffer+5);
	}
	else if (CUtil::StringStartsWith(messageBuffer, "TRVR"))
	{
		//@@: location CGAMECOMMUNICATION_CCONNECTEDPIPE_HANDLEMESSAGE_CHALLENGE_RESPONSE_TRVR
		CGameCommunication::Instance().CalculateAndSendHMAC(messageBuffer+5);
	}
	//@@: } CGAMECOMMUNICATION_CCONNECTEDPIPE_HANDLEMESSAGE_CHALLENGE_RESPONSE

	//@@: } CGAMECOMMUNICATION_CCONNECTEDPIPEUHANDLEMESSAGE 
	//@@: location CGAMECOMMUNICATION_CCONNECTEDPIPEUHANDLEMESSAGE_EXIT_POINT
#endif
}

void CGameCommunication::CConnectedPipe::WriteRaw(const char* data, u32 size)
{
	BOOL result = WriteFile(pipe, data, size, NULL, &overlapped);
	if (!result)
	{
		ERRORF("Failed to write to pipe!");
		CUtil::PrintSystemErrorMessage(GetLastError());
	}
}

void CGameCommunication::CConnectedPipe::SetThread(HANDLE hThread)
{
	m_hThread = hThread;
}

HANDLE CGameCommunication::CConnectedPipe::GetThread()
{
	return m_hThread;
}
