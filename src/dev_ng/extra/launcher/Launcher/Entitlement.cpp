#include "stdafx.h"

#include "Entitlement.h"
#include "SocialClub.h"
#include "Channel.h"
#include "resource.h"
#include "LauncherDlg.h"
#include "Util.h"
#include "Globals.h"
#include "Config.h"
#include "OnlineConfig.h"

#include "CrashDetection.h"
#include "TamperSource.h"
#include "commerce_interface.h"

#include <comdef.h>
#include <WbemIdl.h>

#ifndef WIN32_SECURE_RAND
#include <openssl/rand.h>
#endif

#if !RSG_PRELOADER_LAUNCHER
#define SKU_NAME "RS-GTAV"

__THREAD int RAGE_LOG_DISABLE = 0;
#include "..\..\..\game\system\MachineHash.h"

#define TRANSFORMIT 0

#if TRANSFORMIT

#else
	#include <openssl/rand.h>
	#if _DEBUG
		#pragma comment(lib, "libeay32MTd.lib")
		#pragma comment(lib, "ssleay32MTd.lib")
	#else
		#pragma comment(lib, "libeay32MT.lib")
		#pragma comment(lib, "ssleay32MT.lib")
	#endif
#endif
#endif

// TODO - HACKALERT - At the very least set this to 0 in the next couple of weeks
#define SUPPORT_OLD_ENTITLEMENT_STRING 0
#if RSG_STEAM_LAUNCHER 
static char * sm_GameEntitlement = "96C261A9C3EF0DA32CBC68A83FB7699CA1D5973D6B4D4EA1F9EF8B4E5C7D85B8";
#elif RSG_REGULAR_LAUNCHER
static char * sm_GameEntitlement = "D962AD7FF354A224C6599EB313EFA50E2E04B3E82274480A2143697681FC5DC6";
#endif

EntitlementManager::EntitlementManager()
{
#if !RSG_PRELOADER_LAUNCHER
	//@@: range ENTITLEMENTMANAGER_CONSTRUCTOR {
	m_bWaitingForEntitlementData = false;
	m_bWaitingToRefreshEntitlement = false;
	m_bEntitledToMainGame = false;
	//@@: location ENTITLEMENT_CONSTRUCTOR_LOCATION
	m_bDoingMainEntitlementCheck = false;

	m_EntitlementBlock = NULL;
	m_AllocatedBytesForEntitlementBlock = 0;

	m_bCanDeleteEntitlementBlock = false;
	m_EntitlementBlockSize = 0;
	m_bHaveEntitlementBlock = false;

	m_bLoadingOfflineEntitlementData = false;
#endif
	m_bIsUIShowing = false;
	m_CurrentActivationCode[0] = '\0';
	m_ActivatedSKUName[0] = '\0';
	m_CurrentContentId = 0;
	m_bQuitRequested = false;
	m_DownloadUrlBuffer = NULL;
	m_bInitializedComSecurity = false;
	m_SocialClub = NULL;
	m_ActivationState = Activation_None;
	m_bSignedIn = false;
	m_bInitialized = false;
#if RSG_REGULAR_LAUNCHER
	m_bWaitingForEntitlementJson = false;
	m_bWantEntitlementJson = false;
#endif

	//@@: } ENTITLEMENTMANAGER_CONSTRUCTOR

#if ENTITLEMENT_IN_SCDLL
	m_AsyncStatus = NULL;
#endif

	InitializeCriticalSection(&m_criticalSection);
}

EntitlementManager::~EntitlementManager()
{
	if (m_bInitialized)
	{
		//@@: location ENTITLEMENTMANAGER_DESTRUCTOR
		Shutdown();
	}

	DeleteCriticalSection(&m_criticalSection);
}

void EntitlementManager::Shutdown()
{
	m_bInitialized = false;

#if ENTITLEMENT_IN_SCDLL
	// If m_AsyncStatus needs to be shut down
	if(m_AsyncStatus)
	{
		if (m_AsyncStatus->Pending())
		{
			m_AsyncStatus->Cancel();
		}

		m_AsyncStatus->Release();
		m_AsyncStatus = NULL;
	}
#endif

#if RSG_REGULAR_LAUNCHER
	//@@: range ENTITLEMENTMANAGER_SHUTDOWN_DELETE_MACHINE_HASH {
	if (g_machineHash)
	{
		//@@: location ENTITLEMENTMANAGER_SHUTDOWN_DELETE_MACHINE_HASH_LOCATION
		memset((void *)g_machineHash, 0, HASH_DIGEST_STRING_LENGTH);
	}
	//@@: } ENTITLEMENTMANAGER_SHUTDOWN_DELETE_MACHINE_HASH
#endif

#if NEW_VOUCHER_FLOW_IN_SCDLL
	// If m_AsyncStatus needs to be shut down
	if(m_VoucherStatus)
	{
		if (m_VoucherStatus->Pending()
		{
			m_VoucherStatus->Cancel();
		}
		m_VoucherStatus->Release();
		m_VoucherStatus = NULL;
	}
#endif
}

bool EntitlementManager::Initialize(SocialClub* sc)
{
	//@@: range ENTITLEMENTMANAGER_INITIALIZE {
	m_SocialClub = sc;
	m_bInitialized = true;
	return true;
	//@@: } ENTITLEMENTMANAGER_INITIALIZE
}

bool EntitlementManager::Update()
{
	//@@: range ENTITLEMENTMANAGER_UPDATE {
	switch(m_ActivationState)
	{
	case Activation_WaitingForUserInput:
		UpdateWaitForUserInput();
		break;
#if !RSG_PRELOADER_LAUNCHER

	case Activation_CheckEntitlement:
		UpdateRefreshEntitlement();
		break;
	case Activation_GettingDownloadUrls:
		UpdateGettingDownloadUrls();
		break;
	case Activation_FetchingJsonEntitlements:
		UpdateFetchingJsonEntitlements();
		break;
#endif
#if RSG_TIMEBOMBED_URLS_IN_SCDLL
	case Activation_GettingTimebombedUrls:
		UpdateGettingTimebombedUrls();
		break;
#endif
	default:
#if !RSG_PRELOADER_LAUNCHER
		if (!m_PendingDownloadUrlChecks.empty())
		{
			m_ActivationState = Activation_GettingDownloadUrls;
		}
		else if (m_bWantEntitlementJson)
		{
			m_ActivationState = Activation_FetchingJsonEntitlements;
		}
		else
#endif
#if RSG_TIMEBOMBED_URLS_IN_SCDLL
		if (!m_PendingTimebombedUrlRequests.empty())
		{
			m_ActivationState = Activation_GettingTimebombedUrls;
		}
#else
		{}
#endif
		break;
	}

	//@@: location ENTITLEMENTMANAGER_UPDATE_REQUEST_QUIT
	if (m_bQuitRequested)
	{
		PostMessage(m_SocialClub->GetParentWnd(), CLauncherDlg::eShutdown, 0, 0);
		m_ActivationState = Activation_None;
		m_bQuitRequested = true;
	}

	return 0;
	//@@: } ENTITLEMENTMANAGER_UPDATE

}

const size_t ENTITLEMENT_BLOCK_SIZE = 1024 * 64;

#if !RSG_PRELOADER_LAUNCHER
void EntitlementManager::UpdateRefreshEntitlement()
{
	// Wait for signin to continue
	//@@: range ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_CHECK_SIGNED_IN {
	if (!m_bSignedIn)
	{
		//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_SIGNED_IN
		return;
	}
	//@@: } ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_CHECK_SIGNED_IN
	
	//@@: range ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_RUN_ENTITLEMENT_CHECK {
	if (!m_bWaitingToRefreshEntitlement && !m_bWaitingForEntitlementData)
	{
		m_bWaitingToRefreshEntitlement = true;
	}
	//@@: } ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_RUN_ENTITLEMENT_CHECK

	//@@: range ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_WAITING_TO_REFRESH_ENTITLEMENT {
	// We're waiting for the entitlement utility to be ready to refresh data
	if (m_bWaitingToRefreshEntitlement)
	{
#if ENTITLEMENT_IN_SCDLL
		if (m_AsyncStatus)
		{
			if (m_AsyncStatus->Pending())
			{
				m_AsyncStatus->Cancel();
			}

			m_AsyncStatus->Release();
			m_AsyncStatus = NULL;
		}

		if(!g_machineHash)
		{
			g_machineHash = new char[HASH_DIGEST_STRING_LENGTH];
			//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_CALL_GET_MACHINE_HASH
			CMachineHash::GetMachineHash((char *)g_machineHash, m_SocialClub->GetRockstarId());
		}
#if !RSG_FINAL && !RSG_FINAL_FAUX
		if (Globals::scofflineonly)
		{
			m_bWaitingForEntitlementData = true;
			m_bWaitingToRefreshEntitlement = false;
		}
		else
#endif
		{

			//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_CREATE_ASYNC_LOCATION
			HRESULT hr = m_SocialClub->GetTaskManager()->CreateAsyncStatus(((rgsc::IAsyncStatus**)&m_AsyncStatus));
			if (FAILED(hr))
				return;

			// TODO JRM:
			//	I've made this static so it can maintain scope in the async 'RequestEntitlementBlock' task.
			//	The next SDK (1.1.9.6) should include support for NULL, afterwards we can delete this variable.
			//	When the variable was not static, it would lose scope and be accessed in the DLL later; causing stack corruption.
			static int version = -1;
			

#if !RSG_FINAL
			if (g_machineHash)
#endif
			{

				//@@: range ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_REQUEST_ENTITLEMENT_BLOCK {
				//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_CHECK_AND_DELETE_BLOCK
				if (m_EntitlementBlock)
				{
					if (m_bCanDeleteEntitlementBlock)
					{
						delete[] m_EntitlementBlock;
					}

					m_EntitlementBlock = NULL;
					m_AllocatedBytesForEntitlementBlock = 0;
				}
				
				//@@: range ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_REQUEST_ENTITLEMENT_BLOCK_INNER {
				m_bWaitingForEntitlementData = true;
				m_bWaitingToRefreshEntitlement = false;
				m_EntitlementBlockSize = 0;
				//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_REQUEST_ENTITLEMENT_BLOCK_CALL
				if (m_SocialClub->GetProfileManager()->IsOnline())
				{
					m_EntitlementBlock = new char[ENTITLEMENT_BLOCK_SIZE];
					m_AllocatedBytesForEntitlementBlock = ENTITLEMENT_BLOCK_SIZE;
					m_bCanDeleteEntitlementBlock = true;
					hr = m_SocialClub->GetCommerceManager()->RequestEntitlementBlock((const char*)g_machineHash, "en-US", m_EntitlementBlock, (u32)ENTITLEMENT_BLOCK_SIZE, &version, (u32*)&m_EntitlementBlockSize, m_AsyncStatus);
				}
				//@@: } ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_REQUEST_ENTITLEMENT_BLOCK_INNER
				if (!SUCCEEDED(hr))
				{
					m_AsyncStatus->Release();
					m_AsyncStatus = NULL;
				}
				//@@: } ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_REQUEST_ENTITLEMENT_BLOCK
			}
			

		}
#else
		m_bWaitingToRefreshEntitlement = false;
		m_bWaitingForEntitlementData = true;
#endif

	}
	//@@: } ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_WAITING_TO_REFRESH_ENTITLEMENT

	
	if (m_bWaitingForEntitlementData)
	{
#if ENTITLEMENT_IN_SCDLL
		if (m_AsyncStatus)
		{
			//@@: range ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_SAVE_LOAD_ENTITLEMENT_DATA {
			if (m_AsyncStatus->Pending())
			{
				//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_WAITING_FOR_ENTITLEMENT_DATA_LOCATION
				return;
			}
			else if (m_AsyncStatus->Succeeded())
			{
				//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_ASYNC_SUCCEEDED
				m_SocialClub->GetCommerceManager()->SaveOfflineEntitlement((u8*)m_EntitlementBlock, m_EntitlementBlockSize, (u8*)g_machineHash);
				//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_ASYNC_SUCCEEDED_AFTER_SAVE
			}
			else
			{
				//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_OFFLINE_MACHINE_HASH
				// We didn't get any data, so free the entitlement block buffer
				if (m_EntitlementBlock)
				{
					if (m_bCanDeleteEntitlementBlock)
					{
						delete[] m_EntitlementBlock;
					}

					m_EntitlementBlock = NULL;
					m_AllocatedBytesForEntitlementBlock = 0;
				}
				m_SocialClub->GetCommerceManager()->LoadOfflineEntitlement((u8*)g_machineHash);
				m_bLoadingOfflineEntitlementData = true;
			}
			//@@: } ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_SAVE_LOAD_ENTITLEMENT_DATA

			
			m_AsyncStatus->Release();
			m_AsyncStatus = NULL;
		}
		else
		{

			//@@: range ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_IS_ENTITLTMENT_IOI_IN_PROGRESS {
			//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_IS_ENTITLTMENT_IOI_IN_PROGRESS_LOCATION
			if(m_SocialClub->GetCommerceManager()->IsEntitlementIOInProgress())
			{
				//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_IS_ENTITLTMENT_IOI_IN_PROGRESS_ENTITLEMENT
				return;
			}
			//@@: } ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_IS_ENTITLTMENT_IOI_IN_PROGRESS
			else
			{
				//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_CHECK_BOOL
				if(!m_bLoadingOfflineEntitlementData)
				{
					//@@: range ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_OFFLINE {	
					//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_OFFLINE_LOCATION
					m_SocialClub->GetCommerceManager()->LoadOfflineEntitlement((u8*)g_machineHash);
					//@@: } ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_OFFLINE
					m_bLoadingOfflineEntitlementData = true;	
				}
				else
				{
					//@@: range ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_OFFLINE_ASSIGNMENT {	
					//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_OFFLINE_ASSIGNMENT_LOCATION
					m_EntitlementBlock = (char*)m_SocialClub->GetCommerceManager()->GetOfflineEntitlement();
					m_bCanDeleteEntitlementBlock = false;
					//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_OFFLINE_ASSIGNMENT_LENGTH
					m_EntitlementBlockSize = m_SocialClub->GetCommerceManager()->GetOfflineEntitlementLength();
					//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_OFFLINE_SET_BOOL_FALSE
					m_bLoadingOfflineEntitlementData = false;
					//@@: } ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_OFFLINE_ASSIGNMENT
				}

				/*
					This is separated intentionally. This lets me re-damage some of the code after it's been called.
				 */
				//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_LOAD_OFFLINE_RETURN
				if (m_bLoadingOfflineEntitlementData)
					return;	
				
			}
			
		}

		if(m_bLoadingOfflineEntitlementData)
			return;

		//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_SET_WAITING_FALSE
		m_bWaitingForEntitlementData = false;

		//@@: range ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_CHECK_ENTITLED_TO_GAME {
		if (m_EntitlementBlock
			
#if !RSG_FINAL
			|| Globals::alwaysEntitledToGame
#endif
			)
		{
			// Workaround for SC DLL casting away const and modifying the entitlement block
			//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_COPY_ENTITLEMENT_BLOCK
			char* entitlementBlockCopy = new char[m_EntitlementBlockSize];
			memcpy(entitlementBlockCopy, m_EntitlementBlock, m_EntitlementBlockSize);

			//@@: range ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_CHECK_ENTITLED_TO_GAME_INNER {

			if (m_SocialClub->GetCommerceManager()->DoesEntitlementBlockGrantEntitlement(m_SocialClub->GetRockstarId(), (const char*)g_machineHash,sm_GameEntitlement, entitlementBlockCopy, (u32)m_EntitlementBlockSize)
#if !RSG_FINAL
				|| Globals::alwaysEntitledToGame
#endif
				)
			{
				//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_ENTITLED_TO_GAME
				m_bEntitledToMainGame = true;
			}
			else
			{

#if SUPPORT_OLD_ENTITLEMENT_STRING
				memcpy(entitlementBlockCopy, m_EntitlementBlock, m_EntitlementBlockSize);
				if(m_SocialClub->GetCommerceManager()->DoesEntitlementBlockGrantEntitlement(m_SocialClub->GetRockstarId(), (const char*)g_machineHash,"ENT-GTAV-MAIN-PLAY", entitlementBlockCopy, (u32)m_EntitlementBlockSize))
				{
					m_bEntitledToMainGame = true;
				}
				else
				{
					m_bEntitledToMainGame = false;
				}
#else
				m_bEntitledToMainGame = false;
#endif
			}
			//@@: } ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_CHECK_ENTITLED_TO_GAME_INNER

			//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_DELETE_ENTITLEMENT_COPY
			memset(entitlementBlockCopy, 0, m_EntitlementBlockSize);
			delete[] entitlementBlockCopy;
			entitlementBlockCopy = NULL;
		}
		else
		{
			//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_NO_ENTITLEMENT_BLOCK
			m_bEntitledToMainGame = false;
		}
		//@@: } ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_CHECK_ENTITLED_TO_GAME

#else
		m_bEntitledToMainGame = true;
#endif
		//@@: location ENTITLEMENTMANAGER_UPDATEREFRESH_ENTITLEMENT_SET_DEFAULTS
		m_bWaitingForEntitlementData = false;

		//@@: range ENTITLEMENTMANAGER_UPDATEREFRESH_ENTITLEMENT_PUSH_STATE {
		//@@: location ENTITLEMENTMANAGER_UPDATEREFRESH_ENTITLEMENT_PUSH_STATE_DOING_ENTITLEMENT_CHECK
		if (m_bDoingMainEntitlementCheck)
		{
			// Determine state change
			if (m_bEntitledToMainGame)
			{
				m_ActivationState = Activation_None;
				CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);
			}

			else if (m_SocialClub->GetProfileManager()->IsOnline() && m_EntitlementBlock)
			{
				PostMessage(m_SocialClub->GetParentWnd(), CLauncherDlg::eShowCodeRedemptionUI, 0, 0);
				m_ActivationState = Activation_WaitingForUserInput;
			}
			else
			{
				PostMessage(m_SocialClub->GetParentWnd(), CLauncherDlg::eSetState, CFsm::eNoInternetForActivate, 0);
				m_ActivationState = Activation_None;
			}

			//@@: } ENTITLEMENTMANAGER_UPDATEREFRESH_ENTITLEMENT_PUSH_STATE
			//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_EXIT
			m_bDoingMainEntitlementCheck = false;
		}
		else
		{
			//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_SET_ACTIVATION_PENDING
			m_ActivationState = Activation_PendingEntitlementChecks;
		}
		
		if (m_EntitlementBlock)
		{
			if (m_bCanDeleteEntitlementBlock)
			{
				//@@: location ENTITLEMENTMANAGER_UPDATEREFRESHENTITLEMENT_CLEANUP_ENTITLEMENT_BLOCK
				memset(m_EntitlementBlock, 0, m_AllocatedBytesForEntitlementBlock);
				delete[] m_EntitlementBlock;
			}
			m_EntitlementBlock = NULL;
			m_AllocatedBytesForEntitlementBlock = 0;
		}
	}
}


void EntitlementManager::UpdateGettingDownloadUrls()
{
	//@@: location ENTITLEMENTMANAGER_UPDATEGETTINGDOWNLOADURLS_ALREADY_SIGNED_IN
	if (!m_bSignedIn)
		return;

	// Get the front element
	//@@: range ENTITLEMENTMANAGER_UPDATEGETTINGDOWNLOADURLS_CHECK_FRONT_ELEMENT {
	EnterCriticalSection(&m_criticalSection);
	const PendingEntitlementCheck* check = NULL;
	if (!m_PendingDownloadUrlChecks.empty())
	{
		check = &m_PendingDownloadUrlChecks.front();
	}
	LeaveCriticalSection(&m_criticalSection);

	// Cancel if there's none
	if (!check)
	{
		m_ActivationState = Activation_None;
		return;
	}
	//@@: } ENTITLEMENTMANAGER_UPDATEGETTINGDOWNLOADURLS_CHECK_FRONT_ELEMENT


	if (m_AsyncStatus == NULL)
	{
		//@@: range ENTITLEMENTMANAGER_UPDATEGETTINGDOWNLOADURLS_GET_ENTITLEMENT_URL {
		HRESULT hr = m_SocialClub->GetTaskManager()->CreateAsyncStatus(((rgsc::IAsyncStatus**)&m_AsyncStatus));
		if (FAILED(hr))
			return;

		if (!m_DownloadUrlBuffer)
		{
			m_DownloadUrlBuffer = new char[512];
		}

		if (!m_SocialClub->GetCommerceManager()->GetEntitlementDownloadUrl(check->sku.c_str(), m_DownloadUrlBuffer, 512, m_AsyncStatus))
		{
			m_AsyncStatus->Release();
			m_AsyncStatus = NULL;
		}
		//@@: } ENTITLEMENTMANAGER_UPDATEGETTINGDOWNLOADURLS_GET_ENTITLEMENT_URL

	}

	if (m_AsyncStatus)
	{
		if (!m_AsyncStatus->Pending())
		{
			//@@: range ENTITLEMENTMANAGER_UPDATEGETTINGDOWNLOADURLS_RECEIVED_ENTITLEMENT_URL {
			if (m_AsyncStatus->Succeeded())
			{
				check->delegate->OnReceivedEntitlementDownloadUrl(check->sku.c_str(), m_DownloadUrlBuffer);
			}
			else
			{
				check->delegate->OnReceivedEntitlementDownloadUrl(check->sku.c_str(), NULL);
			}
			//@@: } ENTITLEMENTMANAGER_UPDATEGETTINGDOWNLOADURLS_RECEIVED_ENTITLEMENT_URL


			//@@: range ENTITLEMENTMANAGER_UPDATEGETTINGDOWNLOADURLS_CLEAR_PENDING_URLS {
			EnterCriticalSection(&m_criticalSection);

			if (!m_PendingDownloadUrlChecks.empty())
			{
				m_PendingDownloadUrlChecks.erase(m_PendingDownloadUrlChecks.begin());
			}

			if (m_PendingDownloadUrlChecks.empty())
			{
				m_ActivationState = Activation_None;
			}
			LeaveCriticalSection(&m_criticalSection);
			//@@: } ENTITLEMENTMANAGER_UPDATEGETTINGDOWNLOADURLS_CLEAR_PENDING_URLS


			m_AsyncStatus->Release();
			m_AsyncStatus = NULL;
		}
	}
}
#endif 

#if RSG_TIMEBOMBED_URLS_IN_SCDLL
void EntitlementManager::UpdateGettingTimebombedUrls()
{
	if (!m_bSignedIn || !m_SocialClub->GetProfileManager()->IsOnline())
	{
		// Notify all pending checks of error
		EnterCriticalSection(&m_criticalSection);
		for (size_t i = 0; i < m_PendingTimebombedUrlRequests.size(); i++)
		{
			PendingTimebombedUrlRequest& check = m_PendingTimebombedUrlRequests[i];
			check.delegate->OnCheckedEntitlement(check.inputUrl.c_str(), ENTITLEMENT_ERROR);
		}
		m_PendingTimebombedUrlRequests.clear();
		LeaveCriticalSection(&m_criticalSection);
		m_ActivationState = Activation_None;
		return;
	}

	// Get the front element
	EnterCriticalSection(&m_criticalSection);
	const PendingTimebombedUrlRequest* check = NULL;
	if (!m_PendingTimebombedUrlRequests.empty())
	{
		check = &m_PendingTimebombedUrlRequests.front();
	}
	LeaveCriticalSection(&m_criticalSection);

	// Cancel if there's none
	if (!check)
	{
		m_ActivationState = Activation_None;
		return;
	}

	if (m_AsyncStatus == NULL)
	{
		HRESULT hr = m_SocialClub->GetTaskManager()->CreateAsyncStatus(((rgsc::IAsyncStatus**)&m_AsyncStatus));
		if (FAILED(hr))
			return;

		if (!m_DownloadUrlBuffer)
		{
			m_DownloadUrlBuffer = new char[512];
		}

		if (!m_SocialClub->GetCommerceManager()->GetContentPathUrl(check->inputUrl.c_str(), m_DownloadUrlBuffer, 512, m_AsyncStatus))
		{
			m_AsyncStatus->Release();
			m_AsyncStatus = NULL;
		}
	}

	if (m_AsyncStatus)
	{
		if (!m_AsyncStatus->Pending())
		{
			if (m_AsyncStatus->Succeeded() && !AUTOTEST_FAIL)
			{
				DEBUGF3("Timebomb succeeded: %s", m_DownloadUrlBuffer);
				check->delegate->OnCheckedEntitlement(check->inputUrl.c_str(), ENTITLEMENT_TRUE);
				check->delegate->OnReceivedEntitlementDownloadUrl(check->inputUrl.c_str(), m_DownloadUrlBuffer);
			}
			else
			{
				if (m_AsyncStatus->GetResultCode() == rage::RLEN_ERROR_NOT_ENTITLED)
				{
					check->delegate->OnCheckedEntitlement(check->inputUrl.c_str(), ENTITLEMENT_FALSE);
				}
				else
				{
					check->delegate->OnCheckedEntitlement(check->inputUrl.c_str(), ENTITLEMENT_ERROR);
				}
				check->delegate->OnReceivedEntitlementDownloadUrl(check->inputUrl.c_str(), NULL);
			}


			EnterCriticalSection(&m_criticalSection);

			if (!m_PendingTimebombedUrlRequests.empty())
			{
				m_PendingTimebombedUrlRequests.erase(m_PendingTimebombedUrlRequests.begin());
			}

			if (m_PendingTimebombedUrlRequests.empty())
			{
				m_ActivationState = Activation_None;
			}
			LeaveCriticalSection(&m_criticalSection);

			m_AsyncStatus->Release();
			m_AsyncStatus = NULL;
		}
	}
}
#endif

void EntitlementManager::UpdateWaitForUserInput()
{
	if (!m_bIsUIShowing)
	{
		//@@: range ENTITLEMENTMANAGER_UPDATEWAITFORUSERINPUT_CHECK_SIGNED_IN {
		//@@: location ENTITLEMENTMANAGER_UPDATEWAITFORUSERINPUT_CHECK_SIGNED_IN_LOCATION
		if (m_bSignedIn)
		{
			PostMessage(m_SocialClub->GetParentWnd(), CLauncherDlg::eShowCodeRedemptionUI, 0, 0);
			m_bIsUIShowing = true;
		}
		else
		{
			PostMessage(m_SocialClub->GetParentWnd(), CLauncherDlg::eShowSigninUI, 0, 0);
			m_bIsUIShowing = true;
		}
		//@@: } ENTITLEMENTMANAGER_UPDATEWAITFORUSERINPUT_CHECK_SIGNED_IN

	}
}

#if !RSG_PRELOADER_LAUNCHER
void EntitlementManager::UpdateFetchingJsonEntitlements()
{
	// Only request the data once
	if (!m_bWaitingForEntitlementJson)
	{
		m_SocialClub->GetCommerceManager()->RequestEntitlementData("", "en-US");
		m_bWaitingForEntitlementJson = true;
		return;
	}

	if (m_SocialClub->GetCommerceManager()->IsEntitlementDataInErrorState())
	{
		m_bWaitingForEntitlementJson = false;
		m_bWantEntitlementJson = false;
		m_ActivationState = Activation_None;
		return;
	}

	if (!m_SocialClub->GetCommerceManager()->IsEntitlementDataValid())
	{
		return;
	}

	const char* jsonString = m_SocialClub->GetCommerceManager()->GetEntitlementDataAsJson();

	AUTOCRITSEC(m_criticalSection);
	if (jsonString)
	{
		DEBUGF3("Assigning entitlement json");
		m_EntitlementJson.assign(jsonString);
	}
	else
	{
		DEBUGF3("Clearing entitlement json");
		m_EntitlementJson.clear();
	}

	m_bWaitingForEntitlementJson = false;
	m_bWantEntitlementJson = false;
	m_ActivationState = Activation_None;
}
#endif

void EntitlementManager::ActivationQuit()
{
	m_bQuitRequested = true;
}

void EntitlementManager::ActivationAttempt()
{
	//@@: range ENTITLEMENTMANAGER_ACTIVATIONATTEMPT {
#if !RSG_PRELOADER_LAUNCHER
	m_ActivationState = Activation_CheckEntitlement;
	m_bDoingMainEntitlementCheck = true;
#endif
	//@@: } ENTITLEMENTMANAGER_ACTIVATIONATTEMPT
}

void EntitlementManager::OnUIShown()
{
	m_bIsUIShowing = true;
}

void EntitlementManager::OnUIHidden()
{
	m_bIsUIShowing = false;
}

void EntitlementManager::OnSignIn()
{
	//@@: range ENTITLEMENTMANAGER_ONSIGNIN {
	m_bSignedIn = true;
	if (m_ActivationState != Activation_None)
	{
#if !RSG_PRELOADER_LAUNCHER
		m_ActivationState = Activation_CheckEntitlement;
		m_bDoingMainEntitlementCheck = true;
#endif
	}
	//@@: } ENTITLEMENTMANAGER_ONSIGNIN
}

void EntitlementManager::OnSignOut()
{
	//@@: range ENTITLEMENTMANAGER_ONSIGNOUT {
	m_bSignedIn = false;
	//@@: } ENTITLEMENTMANAGER_ONSIGNOUT
}
#if !RSG_PRELOADER_LAUNCHER
void EntitlementManager::RunEntitlementCheck()
{
	//@@: range ENTITLEMENTMANAGER_RUNENTITLEMENTCHECK {
	m_bWaitingToRefreshEntitlement = true;
	m_ActivationState = Activation_CheckEntitlement;
	m_bDoingMainEntitlementCheck = true;
	// This is an intentional change. I don't want to provide a 
	// string that says where we're checking entitlements.
#if !RSG_FINAL
	CLauncherDlg::GetLastInstance()->SetTextWithSpinner(IDS_CHECKING_ENTITLEMENT);
#else
	CLauncherDlg::GetLastInstance()->SetTextWithSpinner(IDS_LOADING);
#endif
	//@@: } ENTITLEMENTMANAGER_RUNENTITLEMENTCHECK

}


// Not protecting these as it's going to shift to socialclub.dll
void EntitlementManager::GetEntitlementDataBlockFilename(std::wstring* out_filename)
{
	CCrashDetection::GetFolderForInfilesAndOutfiles(*out_filename);

	*out_filename += L"\\datablock.dat";
}

void EntitlementManager::SaveEntitlementDataBlock()
{
	if (!m_EntitlementBlock || m_EntitlementBlockSize == 0)
		return;

	std::wstring path;
	GetEntitlementDataBlockFilename(&path);

	std::ofstream file;
	file.open(path.c_str(), std::ios::binary | std::ofstream::out);
	if (file.is_open())
	{
		std::string mbpath;
		CUtil::WStringToStdString(path, mbpath);
		DEBUGF1("Writing out entitlements file to %s", mbpath.c_str());
		file.write(m_EntitlementBlock, m_EntitlementBlockSize);
		DEBUGF1("Closing entitlements file");
		file.close();
	}
	return;
	
}

// Same
void EntitlementManager::LoadEntitlementDataBlock()
{
	if (m_EntitlementBlock)
	{
		if (m_bCanDeleteEntitlementBlock)
		{
			delete[] m_EntitlementBlock;
		}
		m_EntitlementBlock = NULL;
	}
	m_EntitlementBlockSize = 0;

	std::wstring path;
	GetEntitlementDataBlockFilename(&path);

	// Check it's not too big (100KiB)
	__stat64 stats;
	if (_wstat64(path.c_str(), &stats) != 0)
	{
		ERRORF("Unable to stat datablock!");
		return;
	}
	else if (stats.st_size > 100 * 1024)
	{
		WARNINGF("Datablock was %lu bytes!", stats.st_size);
		return;
	}

	m_EntitlementBlockSize = (u32)stats.st_size;

	DWORD attrs = GetFileAttributesW(path.c_str());

	if ((attrs & FILE_ATTRIBUTE_READONLY) || (attrs & FILE_ATTRIBUTE_DIRECTORY))
	{
		ERRORF("Invalid datablock file attributes!");
		return;
	}

	std::ifstream file;
	file.open(path.c_str(), std::ios::binary | std::ifstream::in);
	if (!file.good())
	{
		return;
	}

	m_EntitlementBlock = new char[m_EntitlementBlockSize];
	m_bCanDeleteEntitlementBlock = true;

	file.read(m_EntitlementBlock, m_EntitlementBlockSize);

	if (file.fail() || file.bad() || !file.eof())
	{
		delete[] m_EntitlementBlock;
		m_EntitlementBlock = NULL;
		m_EntitlementBlockSize = 0;
	}
}

#endif 

static bool FindAndReplaceString(std::string& inout_string, const std::string& find, const std::string& replace)
{
	size_t index = inout_string.find(find);
	if (index != std::string::npos && !replace.empty())
	{
		inout_string.erase(index, find.size());
		inout_string.insert(index, replace);

		return true;
	}

	return false;
}

#if RSG_TIMEBOMBED_URLS_IN_SCDLL
bool EntitlementManager::RequestTimebombedUrlSynchronous(const std::string& input, EntitlementResult* out_result, std::string& out_downloadUrl)
{
	struct Closure : public CheckEntitlementListener
	{
		bool m_bComplete;
		EntitlementResult m_Result;
		std::string m_Url;

		Closure()
		{
			m_bComplete = false;
			m_Result = ENTITLEMENT_ERROR;
		}

		virtual void OnCheckedEntitlement(const char* /*sku*/, EntitlementResult result)
		{
			m_Result = result;
			if (result != ENTITLEMENT_TRUE)
			{
				if (result == ENTITLEMENT_ERROR)
				{
					CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_ACU_FAILED);
				}
				m_bComplete = true;
			}
		}

		virtual void OnReceivedEntitlementDownloadUrl(const char* /*sku*/, const char* downloadUrl)
		{
			if (downloadUrl)
			{
				m_Url.assign(downloadUrl);
			}
			m_bComplete = true;

		}
	} closure;

	PendingTimebombedUrlRequest request;
	request.inputUrl = input;
	request.delegate = &closure;

	EnterCriticalSection(&m_criticalSection);
	m_PendingTimebombedUrlRequests.push_back(request);
	LeaveCriticalSection(&m_criticalSection);

	while (!closure.m_bComplete)
	{
		Sleep(100);
	}

	if (out_result)
	{
		*out_result = closure.m_Result;
	}

	if (closure.m_Result == ENTITLEMENT_TRUE && !closure.m_Url.empty())
	{
		out_downloadUrl = closure.m_Url;

		std::string patchEnv;
		CUtil::GetPatchEnvStr(patchEnv);

		if (Constants::DevUsesInternalServer && patchEnv == Globals::DevEnvName && !Globals::devPatchServer.GetValue().empty())
		{
			const std::string findUrl("patches.rockstargames.com");
			std::string patchServer;
			CUtil::WStringToStdString(Globals::devPatchServer.GetValue(), patchServer);

			if (FindAndReplaceString(out_downloadUrl, findUrl, patchServer))
			{
				if (FindAndReplaceString(out_downloadUrl, "/prod/", "/dev/"))
				{
					DEBUGF3("Translated server and env to: %s", out_downloadUrl.c_str());
				}
				else
				{
					DEBUGF3("Translated server to: %s", out_downloadUrl.c_str());
				}
			}
			else
			{
				DEBUGF3("Unable to translate download URL: %s", out_downloadUrl.c_str());
			}
		}


		return true;
	}
	else
	{
		return false;
	}
}
#endif 

#if !RSG_PRELOADER_LAUNCHER
bool EntitlementManager::GetEntitlementJsonSynchronous(std::string* out_json)
{
	m_bWantEntitlementJson = true;

	m_EntitlementJson.clear();

	while (m_bWantEntitlementJson)
	{
		Sleep(100);
	}

	if (m_EntitlementJson.empty())
	{
		return false;
	}

	DEBUGF3("Entitlement JSON: %s", m_EntitlementJson.c_str());
	out_json->assign(m_EntitlementJson);

	return true;
}
#endif