#include "stdafx.h"
#include "RgscDebugTelemetry.h"
#include "RgscTelemetryManager.h"
#include "Util.h"

#include <string>
#include <regex>

u64 RgscDebugTelemetry::m_eventCounters[DEBUG_EVENT_COUNT] = {0};
bool RgscDebugTelemetry::m_anyEventFired = false;
u64 RgscDebugTelemetry::m_lastTransmitTimeMillis = 0;

void RgscDebugTelemetry::AddDebugEvent(DebugEvent evt, u64 count)
{
	m_eventCounters[evt] += count;
	m_anyEventFired = true;

	// Fire it off every 10 minutes
	const u64 MILLIS_PER_TELEM_SUBMIT = 10 * 60 * 1000;

	u64 time = CUtil::CurrentTimeMillis();

	if (m_lastTransmitTimeMillis == 0)
		m_lastTransmitTimeMillis = time;
	else if (time > m_lastTransmitTimeMillis + MILLIS_PER_TELEM_SUBMIT)
	{
		SendDebugTelemetry();
		m_lastTransmitTimeMillis = time;
	}
}

void RgscDebugTelemetry::SendDebugTelemetry()
{
#if RSG_LAUNCHER
	if (!m_anyEventFired)
		return;

	const int MAX_EVENTS_PER_METRIC = 3;

	std::vector<const RgscTelemetryEvent*> events;

	for (int i = 0; i < DEBUG_EVENT_COUNT; i++)
	{
		if (m_eventCounters[i] > 0)
		{
			RgscTelemetryEvent* dbgEvent = new RgscTelemetryEvent(RgscTelemetryEvent::MN_DEBUG_METRIC);
			dbgEvent->AddIntField(RgscTelemetryEvent::FN_DEBUG_KEY, i);
			dbgEvent->AddIntField(RgscTelemetryEvent::FN_DEBUG_VALUE, m_eventCounters[i]);
			events.push_back(dbgEvent);
		}

		if (events.size() >= MAX_EVENTS_PER_METRIC || (i == DEBUG_EVENT_COUNT-1 && !events.empty()))
		{
			RgscTelemetryManager::WriteEventDebugMetrics(RgscTelemetryManager::EVENT_LAUNCHER_DEBUG, events);
			events.clear();
		}
	}

	m_anyEventFired = false;
#endif
}

void RgscDebugTelemetry::Reset()
{
	for (int i = 0; i < DEBUG_EVENT_COUNT; i++)
	{
		m_eventCounters[i] = 0;
	}

	m_anyEventFired = false;
}

#if RSG_LAUNCHER

void RgscDebugTelemetry::TranslateToXmlEvent(RgscTelemetryEvent* parentEvent, const char* originalString)
{
	const char* errorNames[] = {
		"A0", //"RD_RETRY",
		"A1", //"RD_PAUSED",
		"A2", //"RD_ERROR_1",
		"A3", //"RD_ERROR_CATCHALL",
		"B4", //"CDC_TIMED_OUT",
		"B5", //"CDC_ERROR",
		"B6", //"CDC_VERIFICATION_FAILURE",
		"B7", //"CDC_FORCE_CLOSE",
		"C8", //"CD_SIZE_MISMATCH",
		"C9", //"CD_ENDED_WITH_ERROR",
		"CA", //"CD_SLOW_CONNECTIONS",
		"CB", //"CD_SLOW_CONNECTION_RETRY",
		"DC", //"DCP_HASH_VERIFICATION_FAILURE",
		"DD", //"DCP_CERT_VERIFICATION_FAILURE",
	};

	std::regex pattern("([0-9]+):([0-9]+)");
	std::string search(originalString);
	std::sregex_iterator end;

	std::vector<const RgscTelemetryEvent*> subEvents;

	for (std::sregex_iterator it(search.begin(), search.end(), pattern); it != end; it++)
	{
		std::smatch match = *it;

		if (match.size() == 3)
		{
			std::string m1 = match[1], m2 = match[2];
			int eventIndex = atoi(m1.c_str());
			int count = atoi(m2.c_str());

			if (eventIndex >= 0 && eventIndex < DEBUG_EVENT_COUNT)
			{
				RgscTelemetryEvent* subEvent = new RgscTelemetryEvent(RgscTelemetryEvent::MN_DEBUG_METRIC);

				subEvent->AddStringField(RgscTelemetryEvent::FN_DEBUG_KEY, errorNames[eventIndex]);
				subEvent->AddIntField(RgscTelemetryEvent::FN_DEBUG_VALUE, count);

				subEvents.push_back(subEvent);
			}
		}
	}

	parentEvent->AddArrayField(RgscTelemetryEvent::FN_DEBUG_ARRAY, subEvents);

	for (size_t i = 0; i < subEvents.size(); i++)
	{
		delete subEvents[i];
	}
	subEvents.clear();
}

#endif