#pragma once

#include <stdint.h>
#include <string>
#include <vector>

#if !RSG_BOOTSTRAP
#include "rgsc_common.h"
#include "tasks_interface.h"
#include "activation_interface.h"

#define ENTITLEMENT_IN_SCDLL 1
#define NEW_VOUCHER_FLOW_IN_SCDLL 0
#define RSG_TIMEBOMBED_URLS_IN_SCDLL 1

// From //rage/gta5/dev_ng/rage/base/src/rline/entitlement/rlrosentitlementtask.h
namespace rage
{
	enum rlEntitlementErrorCode
	{
		RLEN_ERROR_UNEXPECTED_RESULT = -1,              //Unhandled error code
		RLEN_ERROR_NONE = 0,                            //Success
		RLEN_ERROR_AUTHENTICATIONFAILED_TICKET,			//Invalid ROS Ticket
		RLEN_ERROR_ALREADY_EXISTS,						
		RLEN_ERROR_SERIAL_DOES_NOT_EXIST,				//Serial Number does not exist in DB
		RLEN_ERROR_SERIAL_DOES_NOT_MATCH,				//Serial Number does not match
		RLEN_ERROR_REDUNDANT_SERIAL,					//Redundant serial 
		RLEN_ERROR_SERIAL_ALREADY_ACTIVATED,			//Serial Number already activated by another user.
		RLEN_ERROR_SERIAL_NOT_ACTIVATED_ON_SERVER,		//Serial Number is owned but has not been activated on the license server
		RLEN_ERROR_INVALID_SKU,							//Invalid SKU to activate/purchase
		RLEN_ERROR_NOT_ENTITLED,						//User is not entitled to particular content
        RLEN_ERROR_NOT_ALLOWED_RULE                     //User is not allowed to complete due to rule restriction
	};
}

enum eActivationState
{
	Activation_None,
	Activation_CheckEntitlement,
	Activation_WaitingForUserInput,
	Activation_RedeemVoucher,
	Activation_WaitForCloseUI,
	Activation_PendingEntitlementChecks,
	Activation_GettingDownloadUrls,

#if RSG_TIMEBOMBED_URLS_IN_SCDLL
	Activation_GettingTimebombedUrls,
#endif

#if !RSG_PRELOADER_LAUNCHER
	Activation_FetchingJsonEntitlements,
#endif


};

class SocialClub;

#if !RSG_PRELOADER_LAUNCHER
namespace Rand
{
	bool SecureGenInit();
	bool SecureGenRandom(u8* pBuffer, DWORD bufferLen);
	void SecureGenRelease();
}
#endif

class EntitlementManager
{
public:
	EntitlementManager();
	~EntitlementManager();

	bool Initialize(SocialClub* sc);
	void Shutdown();

	bool Update();
#if !RSG_PRELOADER_LAUNCHER
	void RunEntitlementCheck();
#endif

private:
	// Activation functions
#if !RSG_PRELOADER_LAUNCHER
	void UpdateRefreshEntitlement();
	void UpdateGettingDownloadUrls();
	void UpdateFetchingJsonEntitlements();
#endif

	void UpdateWaitForUserInput();
#if RSG_TIMEBOMBED_URLS_IN_SCDLL
	void UpdateGettingTimebombedUrls();
#endif

#if !RSG_PRELOADER_LAUNCHER
#if !RSG_FINAL
	bool CheckOfflineEntitlementDebugSwitches();
#endif
#endif

public:
	// UI callbacks
	void ActivationQuit();
	void ActivationAttempt();

	// State Events
	void OnUIShown();
	void OnUIHidden();
	void OnSignIn();
	void OnSignOut();


public:
	enum EntitlementResult
	{
		ENTITLEMENT_TRUE,
		ENTITLEMENT_FALSE,
		ENTITLEMENT_ERROR
	};

	struct CheckEntitlementListener
	{
		virtual void OnCheckedEntitlement(const char* /*sku*/, EntitlementResult /*result*/) {};
		virtual void OnReceivedEntitlementDownloadUrl(const char* /*sku*/, const char* /*url*/) {};
	};

	bool GetEntitlementJsonSynchronous(std::string* out_json);

#if RSG_TIMEBOMBED_URLS_IN_SCDLL
	bool RequestTimebombedUrlSynchronous(const std::string& input, EntitlementResult* out_result, std::string& out_downloadUrl);
#endif
#if !RSG_PRELOADER_LAUNCHER
	static void SHA1HashHelper(const u8* in_data, u32 in_dataSize, char* out_hash);
#endif
private:
	
#if !RSG_PRELOADER_LAUNCHER
	struct PendingEntitlementCheck
	{
		CheckEntitlementListener* delegate;
		std::string sku;
	};

	std::vector<PendingEntitlementCheck> m_PendingDownloadUrlChecks;
#endif

#if RSG_TIMEBOMBED_URLS_IN_SCDLL
	struct PendingTimebombedUrlRequest
	{
		CheckEntitlementListener* delegate;
		std::string inputUrl;
	};

	std::vector<PendingTimebombedUrlRequest> m_PendingTimebombedUrlRequests;
#endif

#if !RSG_PRELOADER_LAUNCHER
	void SaveEntitlementDataBlock();
	void LoadEntitlementDataBlock();
	void GetEntitlementDataBlockFilename(std::wstring* out_filename);
#endif

private:
	SocialClub* m_SocialClub;
	rgsc::IAsyncStatusLatestVersion* m_AsyncStatus;
#if !RSG_PRELOADER_LAUNCHER
	bool m_bLoadingOfflineEntitlementData;
	bool m_bWaitingForEntitlementData;
	bool m_bWaitingToRefreshEntitlement;
	bool m_bEntitledToMainGame;
	bool m_bDoingMainEntitlementCheck;
	bool m_bWantEntitlementJson;
	bool m_bWaitingForEntitlementJson;
#endif
	eActivationState m_ActivationState;
	bool m_bIsUIShowing;
	bool m_bSignedIn;
	bool m_bQuitRequested;

	bool m_bInitialized;
	int m_CurrentContentId;
	char m_CurrentActivationCode[32];
	char m_ActivatedSKUName[256];

#if !RSG_PRELOADER_LAUNCHER
	char* m_EntitlementBlock;
	u32 m_AllocatedBytesForEntitlementBlock;

	u32 m_EntitlementBlockSize;
	bool m_bHaveEntitlementBlock;
	bool m_bCanDeleteEntitlementBlock;
#endif
	bool m_bInitializedComSecurity;

	char* m_DownloadUrlBuffer;
#if !RSG_PRELOADER_LAUNCHER
	char* m_MachineHash;

	std::string m_EntitlementJson;

#endif

	CRITICAL_SECTION m_criticalSection;

}; // class EntitlementManager
#endif