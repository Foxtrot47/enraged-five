#pragma once

#include <sstream>

class CSimpleDownloader
{
public:
	CSimpleDownloader(const char* server, const char* path, const char* method = "GET");
	~CSimpleDownloader(void);

	BOOL BlockingDownload(char** out_result, size_t* out_size);
	
	void Logs(const char* msg, bool addNewLine = true);
	void Logf(const char* fmt, ...);

	void Errors(const char* msg);
	//void Errorf(const char* fmt, ...);

	static bool GetServer(const char* url, char** out_server);
	static bool GetPath(const char* url, char** out_server);

protected:
	const char* m_server, *m_path, *m_method;

	BOOL CreateSocket(char** out_result, size_t* out_size);
	BOOL Download(SOCKET sock, char** out_result, size_t* out_size);
};

class HeaderHandler
{
private:
	int m_crlfCounter;
	int m_statusCode;
	bool m_passedHeaders;
	std::stringstream m_headers;
	s64 m_firstSpace;
	u64 m_contentLength;
	u64 FindContentLength();
public:
	HeaderHandler();
	bool HasPassedHeaders();
	int StatusCode();
	bool Consume(char c);
	u64 ContentLength();
	u64 HeadersLength();
	
};