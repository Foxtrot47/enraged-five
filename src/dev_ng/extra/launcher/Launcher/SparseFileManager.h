#pragma once

#include <Windows.h>

#include <vector>
#include <string>

#include "RangeList.h"

class SparseFileManager
{
public:
	SparseFileManager();
	~SparseFileManager();

	bool Open(const char* fullpath, u64 size);
	bool Close();

	bool WriteToMemory(const char* data, u64 data_len, u64 offset);
	bool VerifyAndWrite();
	bool FindAndRemoveCompleteRange(PartiallyCompleteRange& out_completeRange, bool& out_verifiedOkay);


	bool Write(const char* data, u64 data_len, u64 offset);
	bool Read(char* data, u64 data_len, u64 offset) const;

	bool ReadHashFile(const char* fullpath);

	// Returns false if any hash checks fail
	bool CheckHashes(u64* out_badBytes);

	bool GetNextRange(Range* out_range, u64 maximumSize) const;
	u64 GetTotalSizeWritten() const;

	bool IsFileComplete() const;
	bool IsRangeComplete(const Range& range) const;
	bool ComputeSha256(char out_hash[32], const Range& range) const;
	bool VerifySha256(const char hash[32], const Range& range) const;
	void CancelHashChecks() { m_cancelledHashChecks = true; }

	void SetRangeInProgress(const Range& range, bool inProgress);
	bool ClearRangesInProgess();

private:
	bool ReadPartFile();
	bool WritePartFile();

	void AddRange(Range range);
	void RemoveRange(Range range);

	mutable CRITICAL_SECTION m_criticalSection;
	mutable CRITICAL_SECTION m_writeLock;
	HANDLE m_fileHandle;
	HANDLE m_partFileHandle;

	std::string m_partFileName;
	RangeList m_downloadedRanges;
	RangeList m_rangesInProgress;
	RangeList m_rangesOnDisk;
	std::vector<Sha256Range> m_hashRanges;
	u64 m_size;
	bool m_cancelledHashChecks;

	std::vector<PartiallyCompleteRange> m_partialRanges;

	PartiallyCompleteRange* GetPartialRangeContaining(u64 position);
};
