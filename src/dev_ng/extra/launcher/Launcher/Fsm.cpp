#include "stdafx.h"
#include "Launcher.h"
#include "Fsm.h"
#include "Channel.h"

#include "Application.h"
#include "Config.h"
#include "CloudSaveManager.h"
#include "DownloadManager.h"
#include "Entitlement.h"
#include "Error.h"
#include "GameCommunication.h"
#include "GamePatchCheck.h"
#include "Globals.h"
#include "InPlaceDownloader.h"
#include "LauncherDlg.h"
#include "MtlInstallCheck.h"
#include "OnlineConfig.h"
#include "ReleaseDateCheck.h"
#include "RockstarDownloader.h"
#include "RgscTelemetryManager.h"
#include "SimpleDownloader.h"
#include "SocialClub.h"
#include "TamperSource.h"
#include "Test.h"
#include "UpdateCheck.h"
#include "Util.h"
#include "WorkerThread.h"



#include <string>
#include <sstream>
#include <iomanip>

#include "Localisation.h"

extern void TimetToFileTime( time_t t, LPFILETIME pft );

CFsm::CFsm(CLauncherDlg* launcher)
{
	m_state = eNone;
	m_launcher = launcher;

	//@@: location CFSM_CONSTRUCTOR
#if RSG_STEAM_LAUNCHER

	// Steam flow
	m_applicationFlow.push_back(eDownloadOnlineConfig);
	m_applicationFlow.push_back(eSteamInit);
	m_applicationFlow.push_back(eSocialClub);
	m_applicationFlow.push_back(eCheckForEntitlement);
	m_applicationFlow.push_back(eSteamShutdown);

#if ENABLE_SP_CLOUD_SAVE
	m_applicationFlow.push_back(eCloudSyncPreGame);
#endif
	m_applicationFlow.push_back(eLoading);

#elif RSG_PRELOADER_LAUNCHER

	// Pre-loader build flow
	m_applicationFlow.push_back(eDownloadOnlineConfig);
	m_applicationFlow.push_back(eCheckForRGSCUpdates);
	m_applicationFlow.push_back(eSocialClub);
	m_applicationFlow.push_back(eCheckForInPlaceDownload);
	m_applicationFlow.push_back(eCheckForReleaseDate);
	m_applicationFlow.push_back(eReleaseDateRestart);

#else

	// Post-release build flow
	m_applicationFlow.push_back(eDownloadOnlineConfig);

#ifndef RSG_NO_UPDATE
	// Check for Social Club updates unless disabled
	m_applicationFlow.push_back(eCheckForRGSCUpdates);
#endif

	// User must sign into social club and check for entitlements
	m_applicationFlow.push_back(eSocialClub);
	m_applicationFlow.push_back(eCheckForEntitlement);

#ifndef RSG_NO_UPDATE

	// Check for updates - but if we're testing cloud save conflicts, don't !!!
#if ENABLE_SP_CLOUD_SAVE && !RSG_FINAL
	if (!Globals::cloudsave_singleconflict && !Globals::cloudsave_multipleconflicts)
#endif
	{
		m_applicationFlow.push_back(eCheckForUpdate);
	}

	// Check for in-place updates - but if we're testing cloud save conflicts, don't !!!
#if RSG_IN_PLACE_DOWNLOAD
#if !RSG_FINAL
	if (!Globals::autotest_launcher_gamepatch && !Globals::noDIP
#if ENABLE_SP_CLOUD_SAVE
		&& !Globals::cloudsave_singleconflict && !Globals::cloudsave_multipleconflicts
#endif
		)
#endif
	{
		m_applicationFlow.push_back(eCheckForInPlaceDownload);
	}

	m_applicationFlow.push_back(eMTLAvailable);

#endif // RSG_IN_PLACE_DOWNLOAD
#endif // RSG_NO_UPDATE

#if ENABLE_SP_CLOUD_SAVE
	// We should always sync cloud saves before launching the game
	m_applicationFlow.push_back(eCloudSyncPreGame);
#endif

	// Actually launch the game
	m_applicationFlow.push_back(eLoading);

#endif


}

CFsm::~CFsm(void)
{
}

CFsm::FsmState CFsm::GetState() const
{
	return m_state;
}

#if !RSG_FINAL
const char* CFsm::GetStateName(FsmState state)
{
	switch (state)
	{
	case eNone: return "eNone";
	case eDownloadOnlineConfig: return "eDownloadOnlineConfig";
	case eCheckForRGSCUpdates: return "eCheckForRGSCUpdates";
#if RSG_STEAM_LAUNCHER
	case eSteamInit: return "eSteamInit";
	case eSteamShutdown: return "eSteamShutdown";
#endif
	case eSocialClub: return "eSocialClub";
	case eSignInAgain: return "eSignInAgain";
#if !RSG_PRELOADER_LAUNCHER
	case eCheckForEntitlement: return "eCheckForEntitlement";
#endif
	case eShowCodeRedemption: return "eShowCodeRedemption";
#if RSG_IN_PLACE_DOWNLOAD
	case eCheckForInPlaceDownload: return "eCheckForInPlaceDownload";
#endif
	case eCheckForUpdate: return "eCheckForUpdate";
#if ENABLE_SP_CLOUD_SAVE
	case eCloudSyncPreGame: return "eCloudSyncPreGame";
	case eCloudSyncPostGame: return "eCloudSyncPostGame";
	case eCloudSyncPreGameError: return "eCloudSyncPreGameError";
	case eCloudSyncPostGameError: return "eCloudSyncPostGameError";
	case eCloudSyncPreGameTimeout: return "eCloudSyncPreGameTimeout";
	case eCloudSyncPreGameBannedError: return "eCloudSyncPreGameBannedError";
	case eCloudSyncPreGamePermaBannedError: return "eCloudSyncPreGamePermaBannedError";
	case eCloudSyncPostGameBannedError: return "eCloudSyncPostGameBannedError";
	case eCloudSyncPostGamePermaBannedError: return "eCloudSyncPostGamePermaBannedError";
#endif
	case eShutdownSocialClub: return "eShutdownSocialClub";
#if RSG_PRELOADER_LAUNCHER
	case eCheckForReleaseDate: return "eCheckForReleaseDate";
	case eReleaseDateCountdown: return "eReleaseDateCountdown";
	case eReleaseDateRestart: return "eReleaseDateRestart";
#else
	case eLoading: return "eLoading";
	case eRestartGame: return "eRestartGame";
#endif
	case eRequiredUpdate: return "eRequiredUpdate";
	case eOptionalUpdate: return "eOptionalUpdate";
	case eDLCAvailable: return "eDLCAvailable";
	case eMTLAvailable: return "eMTLAvailable";
#if RSG_IN_PLACE_DOWNLOAD
	case eGameInstallAvailable: return "eGameInstallAvailable";
	case eInPlaceIntegrityCheck: return "eInPlaceIntegrityCheck";
	case eDownloadInPlaceData: return "eDownloadInPlaceData";
	case eDownloadInPlaceDataError: return "eDownloadInPlaceDataError";
#endif
	case eDownloadUpdate: return "eDownloadUpdate";
	case eDownloadOptionalUpdate: return "eDownloadOptionalUpdate";
	case eResumeDownloadingUpdate: return "eResumeDownloadingUpdate";
	case eInstallUpdate: return "eInstallUpdate";
	case eSocialClubError: return "eSocialClubError";
	case eErrorScreen: return "eErrorScreen";
	case eExitCodeError: return "eExitCodeError";
	case eNoTempSpace: return "eNoTempSpace";
	case eNoInstallSpace: return "eNoInstallSpace";
	case eNoInternetForActivate: return "eNoInternetForActivate";
	case eOffline: return "eOffline";
	case eOfflineNoUAC: return "eOfflineNoUAC";
	case eSafeMode: return "eSafeMode";
	case eSendingReport: return "eSendingReport";
	case eRestart: return "eRestart";
	case eRestartAsAdmin: return "eRestartAsAdmin";
	case eShuttingDown: return "eShuttingDown";
	default: return "Unknown state!";
	};
}
#endif

void CFsm::SetState(FsmState state)
{
	if (m_state == eShuttingDown)
		return;
	//@@: location CFSM_SETSTATE_ONLEAVESTATE
	OnLeaveState(m_state);
#if RSG_PRELOADER_LAUNCHER || !ENABLE_TAMPER_ACTIONS
	OnEnterState(m_state = (FsmState)(state));
#else
	OnEnterState(m_state = (FsmState)(state + g_stateOffset));
#endif

	CUtil::LockTimings::PrintTimings();
}

void CFsm::SetFlowState(FsmState state)
{
	if (m_state == eShuttingDown)
		return;

	if (m_applicationFlow.front() != state)
	{
#if !RSG_FINAL
		DEBUGF1("Inserting new flow state at the front: %s", GetStateName(state));
#endif
		m_applicationFlow.insert(m_applicationFlow.begin(), state);
	}

	SetState(state);
}

void CFsm::RemoveOnlineStates()
{
	RemoveFlowState(eDownloadOnlineConfig);
	RemoveFlowState(eCheckForRGSCUpdates);
#if RSG_IN_PLACE_DOWNLOAD
	RemoveFlowState(eCheckForInPlaceDownload);
	RemoveFlowState(eDownloadInPlaceData);
#endif
#if !RSG_PRELOADER_LAUNCHER
	RemoveFlowState(eCheckForUpdate);
	RemoveFlowState(eMTLAvailable);
	RemoveFlowState(eDownloadUpdate);
	RemoveFlowState(eDownloadOptionalUpdate);
#if ENABLE_SP_CLOUD_SAVE
	RemoveFlowState(eCloudSyncPreGame);
#endif
#endif
}

void CFsm::OnLeaveState(FsmState /*state*/)
{

}

void CFsm::OnEnterState(FsmState state)
{
	//@@: location CFSM_ONENTERSTATE_CHECK_NULL
	if (m_launcher == NULL)
	{
		ERRORF("Error! Launcher dialog is NULL!");
		return;
	}
	
#if !RSG_FINAL
	DEBUGF1("Entering state %d (%s)", state, GetStateName(state));
	RAD_TELEMETRY_ZONE("State %s", GetStateName(state));
#endif

	struct ButtonCallbacks
	{
		static void OfflineMode(void* param)
		{
			//@@: range CFSM_ONENTERSTATE_INNER_OFFLINEMODE {
			CFsm* fsm = (CFsm*)param;
			//@@: location CFSM_ONENTERSTATE_INNER_OFFLINEMODE_REMOVE_FLOW_STATES

			fsm->RemoveOnlineStates();

			Globals::scofflineonly = true;

			fsm->NextState();
			//@@: } CFSM_ONENTERSTATE_INNER_OFFLINEMODE

		}
		static void SafeMode(void* param)
		{
			//@@: location CFSM_ONENTERSTATE_INNER_SAFE_MODE
			Globals::safemode = true;
			((CFsm*)param)->NextState();
		}
		static void Retry(void* param)
		{
			((CFsm*)param)->NextState();
		}
		static void Relaunch(void* param)
		{
			CFsm* fsm = (CFsm*)param;
			if (!Globals::scofflineonly)
			{
				fsm->m_applicationFlow.clear();
				fsm->m_applicationFlow.push_back(eCheckForUpdate);
#if RSG_IN_PLACE_DOWNLOAD
				fsm->m_applicationFlow.push_back(eCheckForInPlaceDownload);
#endif
			}
			fsm->NextState();
		}
		static void SafeModeRelaunch(void* param)
		{
			Globals::safemode = true;
			Relaunch(param);
		}
#if RSG_IN_PLACE_DOWNLOAD
		static void DownloadInPlaceData(void* param)
		{
			((CFsm*)param)->SetFlowState(eDownloadInPlaceData);
		}
#endif
		static void Update(void* param)
		{
			((CFsm*)param)->SetFlowState(eDownloadUpdate);
		}
		static void MtlUpdate(void* param)
		{
			RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_MTL_ACCEPTED_UPGRADE);
			
			if (MtlInstallCheck::IsRecentMtlInstalled() && MtlInstallCheck::LaunchMtl())
			{
				RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_MTL_ALREADY_INSTALLED);
				CLauncherDlg::PostMessageSafe(CLauncherDlg::eShutdown, 0, 0);
			}
			else
			{
				RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_MTL_DOWNLOADING);
				UpdateChecker::Instance().Reset();
				UpdateChecker::Instance().AddMtlCheck();
				UpdateChecker::Instance().CheckForUpdates(NULL);

				if (UpdateChecker::Instance().PatchesToInstall().empty())
				{
					std::wstring errorMsgWString;
					CError err(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_READ_VERSIONING_FAILED);
					CUtil::StdStringToStdWString(CError::GetLocalisedErrorMessage(err), errorMsgWString);

					((CFsm*)param)->m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, errorMsgWString,
						CLauncherDlg::ButtonParams(IDS_BUTTON_PLAY, ButtonCallbacks::Play, param),
						CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
				}
				else
				{
					((CFsm*)param)->SetFlowState(eDownloadUpdate);
				}
			}
		}
		static void CancelMtlUpdate(void*)
		{
			RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_MTL_CANCELLED_UPGRADE);
			CLauncherDlg::GetLastInstance()->Exit();
		}
		static void UpdateOptional(void* param)
		{
			((CFsm*)param)->SetFlowState(eDownloadOptionalUpdate);
		}
#if !RSG_PRELOADER_LAUNCHER
		static void Play(void* param)
		{
			
			//@@: location CFSM_ONENTERSTATE_INNER_PLAY
			((CFsm*)param)->SetState(eLoading);
		}
#endif
		static void GetHelp(void* /*param*/)
		{
#ifdef _DEBUG
			if (IsDebuggerPresent())
			{
				MessageBoxA(NULL, "Do not attempt to debug calls to ShellExecute with Visual Studio's debugger attached.", "Debugger detected", MB_OK);
			}
			else
#endif
			{
				const std::string& sSupportURL = CError::GetSupportURL();
				ShellExecuteA(NULL, "open", sSupportURL.c_str(), NULL, NULL, SW_SHOWNORMAL);
			}
		}

		static void SendReport(void* param)
		{
			((CFsm*)param)->SetState(eSendingReport);
		}

#if RSG_REGULAR_LAUNCHER || RSG_STEAM_LAUNCHER
		static UINT LaunchGameThread(LPVOID param)
		{

			//@@: range CFSM_ONENTERSTATE_INNER_LAUNCHGAMETHREAD {
			//@@: location CFSM_ONENTERSTATE_INNER_LAUNCHGAMETHREAD_SET_CURRENT_THREAD_NAME
			CUtil::SetCurrentThreadName("LaunchGameThread");
			LauncherApp::SetLauncherDlg((CLauncherDlg*)param);
			//@@: location CFSM_ONENTERSTATE_INNER_LAUNCHGAMETHREAD_LAUNCH_GAME
			return LauncherApp::LaunchGameExecutable();
			//@@: } CFSM_ONENTERSTATE_INNER_LAUNCHGAMETHREAD
			return 0;
		}

		static UINT RestartGameThread(LPVOID param)
		{
			// Adding a delay of 2 seconds to give Windows time to change resolution and show the launcher again
			Sleep(2000);
			return LaunchGameThread(param);
		}
#endif

		static void BrowseForTemp(void* param)
		{
			std::wstring current;
			std::wstring chosen;

			CUtil::PrependTempoaryFolderPath(current, L"");

			CLauncherDlg::GetLastInstance()->ShowFolderPicker(current, chosen);

			if (!chosen.empty())
			{
				Globals::alternativeTempPath = chosen;

				// Update any patches that are in-flight
				std::string currentStr, chosenStr;
				CUtil::WStringToStdString(current, currentStr);
				CUtil::WStringToStdString(chosen, chosenStr);
				LauncherApp::UpdatePatchPaths(currentStr, chosenStr);

				Retry(param);
			}
		}

		static void Resume(void*)
		{
			CLauncherDlg::GetLastInstance()->SetProgressBar(IDS_HEADING_INSTALLING, IDS_DOWNLOADING_UPDATE, CLauncherDlg::ButtonParams(IDS_BUTTON_PAUSE, ButtonCallbacks::Pause, NULL));
			Downloader::Instance().SetPaused(false);
		}

		static void Pause(void*)
		{
			CLauncherDlg::GetLastInstance()->SetProgressBar(IDS_HEADING_INSTALLING, IDS_DOWNLOAD_PAUSED, CLauncherDlg::ButtonParams(IDS_BUTTON_RESUME, ButtonCallbacks::Resume, NULL));
			Downloader::Instance().SetPaused(true);
		}

		static void Exit(void*)
		{
			CLauncherDlg::GetLastInstance()->Exit();
		}

		static void RestartWithArg(const wchar_t* str, bool asAdmin, FsmState failstate)
		{
			LPWSTR cmd = GetCommandLineW();
			if (cmd)
			{
				int argc = 0;
				LPWSTR* argv = CommandLineToArgvW(cmd, &argc);
				std::wstring argsString;
				for (int i = 1; i < argc; i++)
				{
					if (i > 1)
						argsString += L" ";
					argsString += argv[i];
				}
				if (!argsString.empty())
				{
					argsString += L" ";
				}
				
				argsString += str;

				std::string prelauncher_path = GamePatchCheck::GetGameExecutableFolder();
				prelauncher_path += "\\";
				prelauncher_path += Constants::BootstrapExeNameWithoutExtension;
				prelauncher_path += ".exe";

				std::wstring prelauncher_wpath;
				CUtil::StdStringToStdWString(prelauncher_path, prelauncher_wpath);

				if (ShellExecuteW(NULL, asAdmin ? L"runas" : L"open", prelauncher_wpath.c_str(), argsString.c_str(), NULL, SW_SHOWNORMAL))
				{
					CLauncherDlg::PostMessageSafe(WM_CLOSE);
				}
				else
				{
					CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, failstate);
				}

				LocalFree(argv);
			}

		}

#if RSG_PRELOADER_LAUNCHER
		static void ReleaseDateRestart(void*)
		{
			RestartWithArg(L"-releasedaterestart", true, eOfflineNoUAC);
		}
#endif
		static void OfflineRestart(void*)
		{
			RestartWithArg(L"-generalrestart", false, eOfflineNoUAC);
		}

		static void RestartAsAdmin(void* param)
		{
			((CFsm*)param)->SetState(eRestartAsAdmin);
		}

		static void DisableCloud(void* param)
		{
			CloudSaveManager* csm = CloudSaveManager::Instance();
			if (csm)
			{
				csm->DisableCloudSaves();
			}

			// Cloud saves are disabled -- continue into the game.
			CFsm* fsm = (CFsm*)param;
			if (fsm)
			{
				fsm->SetState(eLoading);
			}
		}

		static void DoNotDisableCloud(void* param)
		{
			CFsm* fsm = (CFsm*)param;
			if (fsm)
			{
				// Fallback to the previous error message so users understand that
				// cloud saves are not synced.
				fsm->SetState(eCloudSyncPreGameError);
			}
		}
	};
	
	//@@: location CFSM_ONENTERSTATE_ENTER_SWITCH
	switch (state)
	{
		//@@: range CFSM_ONENTERSTATE_SWITCHING_STATES {
		case eNone:
		{
			ERRORF("Invalid state!");
			break;
		}
		case eDownloadOnlineConfig:
		{
			m_launcher->SetTextWithSpinner(IDS_LOADING, CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			WorkerThread::Instance().AddJob(WorkerThread::JOB_DOWNLOAD_ONLINE_CONFIG);
			break;
		}
		case eCheckForRGSCUpdates:
		{
			
			// Set UI state
			//@@: location CFSM_ONENTERSTATE_SET_SC_UPDATE_TEXT
			m_launcher->SetTextWithSpinner(IDS_LOADING, CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			// Start thread
			WorkerThread::Instance().AddJob(WorkerThread::JOB_CHECK_FOR_SC_UPDATES);
			break;
			

		}
#if RSG_STEAM_LAUNCHER
		case eSteamInit:
			{
				// Ui
				m_launcher->SetTextWithSpinner(IDS_LOADING);
				// Start Thread
				WorkerThread::Instance().AddJob(WorkerThread::JOB_STEAM_INIT);
				break;
			}
		case eSteamShutdown:
			{
				// Start Thread
				WorkerThread::Instance().AddJob(WorkerThread::JOB_STEAM_SHUTDOWN);
				break;
			}
#endif
		case eSocialClub:
		{
			//@@: location CFSM_ONENTERSTATE_SET_SC_INIT_TEXT
			m_launcher->SetTextWithSpinner(IDS_LOADING, CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			m_launcher->SetSocialClubVisible();
			break;
		}
		case eSignInAgain:
		{
			// Do nothing.
			break;
		}
		case eShutdownSocialClub:
		{
			m_launcher->ShutdownSocialClub();
			break;
		}
#if !RSG_PRELOADER_LAUNCHER
		case eCheckForEntitlement:
		{
			//@@: location CFSM_ONENTERSTATE_SET_CHECKING_ENTITLEMENT_TEXT
			m_launcher->InitializeEntitlementChecker();
			
			break;
		}
#endif
		case eShowCodeRedemption:
		{
			m_launcher->ShowCodeRedemptionUI();
			break;
		}

#if RSG_IN_PLACE_DOWNLOAD
		case eCheckForInPlaceDownload:
		{
			// Set UI state
			m_launcher->SetTextWithSpinner(IDS_LOADING);

			WorkerThread::Instance().AddJob(WorkerThread::JOB_CHECK_FOR_IN_PLACE_DOWNLOAD);
			break;
		}
#endif
		case eCheckForUpdate:
		{
			
			// Set UI state
			
			m_launcher->SetSocialClubHotkey(false);
			m_launcher->SetTextWithSpinner(IDS_LOADING);

			// Start thread
			//@@: location CFSM_ONENTERSTATE_ADD_GAME_UPDATE_JOB
			WorkerThread::Instance().AddJob(WorkerThread::JOB_CHECK_FOR_GAME_UPDATES);
			break;
		}

#if ENABLE_SP_CLOUD_SAVE
		case eCloudSyncPreGame:
		{
			if (m_launcher->IsSocialClubOnline() && Globals::spcloudsave && OnlineConfig::CloudSaveEnabled && !Globals::skipPregameCloudSync)
			{
				m_launcher->SetTextWithSpinner(IDS_LOADING);
				CloudSaveManager* csm = CloudSaveManager::Instance();
				if (csm)
				{
					csm->StartCloudSync(false);
				}
			}
			else
			{
				m_launcher->PostMessageSafe(CLauncherDlg::eNextState);
			}
			break;
		}
		case eCloudSyncPostGame:
		{
			m_launcher->SetTextWithSpinner(IDS_CLOUDSAVE_SYNC);
			CloudSaveManager* csm = CloudSaveManager::Instance();
			if (csm)
			{
				csm->StartCloudSync(true);
			}
			break;
		}
		case eCloudSyncPreGameTimeout:
		{
			m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, IDS_CLOUDSAVE_TIMEOUT,
				CLauncherDlg::ButtonParams(IDS_BUTTON_DISABLE_CLOUD, ButtonCallbacks::DisableCloud, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::DoNotDisableCloud, this));
			break;
		}
		case eCloudSyncPreGameError:
		{
			CloudSaveManager* csm = CloudSaveManager::Instance();
			if (csm && csm->IsTimedOut() && !csm->IsCloudSaveEnabled())
			{
				// If user opts out, don't display 'retry' button.
				m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, IDS_CLOUDSAVE_PRESYNC_ERROR,
					CLauncherDlg::ButtonParams(IDS_BUTTON_PLAY, ButtonCallbacks::Play, this),
					CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			}
			else
			{
				m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, IDS_CLOUDSAVE_PRESYNC_ERROR,
					CLauncherDlg::ButtonParams(IDS_BUTTON_RETRY, ButtonCallbacks::Retry, this),
					CLauncherDlg::ButtonParams(IDS_BUTTON_PLAY, ButtonCallbacks::Play, this),
					CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			}

			break;
		}
		case eCloudSyncPostGameError:
		{
			m_applicationFlow.clear();
			m_applicationFlow.push_back(eCloudSyncPostGame);
			m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, IDS_CLOUDSAVE_POSTSYNC_ERROR,
				CLauncherDlg::ButtonParams(IDS_BUTTON_RETRY, ButtonCallbacks::Retry, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));

			break;
		}
		case eCloudSyncPreGameBannedError:
		{
			std::wstring text;
			GetCloudSaveBannedString(text);

			m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, text,
				CLauncherDlg::ButtonParams(IDS_BUTTON_PLAY, ButtonCallbacks::Play, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));

			break;
		}
		case eCloudSyncPreGamePermaBannedError:
		{
			m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, IDS_CLOUDSAVE_PERMABANNED,
				CLauncherDlg::ButtonParams(IDS_BUTTON_PLAY, ButtonCallbacks::Play, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));

			break;
		}
		case eCloudSyncPostGameBannedError:
		{
			m_applicationFlow.clear();
			m_applicationFlow.push_back(eCloudSyncPostGame);

			std::wstring text;
			GetCloudSaveBannedString(text);

			m_launcher->SetTextWithButton(IDS_HEADING_ERROR, text, CLauncherDlg::ButtonParams(IDS_BUTTON_CLOSE, ButtonCallbacks::Exit, NULL));

			break;
		}
		case eCloudSyncPostGamePermaBannedError:
		{
			m_applicationFlow.clear();
			m_applicationFlow.push_back(eCloudSyncPostGame);
		
			std::wstring text = CLocalisation::Instance().GetWString(IDS_CLOUDSAVE_PERMABANNED);
			m_launcher->SetTextWithButton(IDS_HEADING_ERROR, text, CLauncherDlg::ButtonParams(IDS_BUTTON_CLOSE, ButtonCallbacks::Exit, NULL));

			break;
		}
#endif

#if RSG_PRELOADER_LAUNCHER
		case eCheckForReleaseDate:
		{
			m_launcher->SetTextWithSpinner(IDS_CHECKING_RELEASE_DATE);
			WorkerThread::Instance().AddJob(WorkerThread::JOB_CHECK_FOR_RELEASE_DATE);
			break;
		}
		case eReleaseDateCountdown:
		{
			std::wstring message = CLocalisation::Instance().GetWString(IDS_CHECKING_RELEASE_DATE);
			m_launcher->SetTextWithButton(0, message, CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			CReleaseDateCheck::StartCountdownThread();
			break;
		}
		case eReleaseDateRestart:
		{
			if (Globals::releasedaterestart)
			{
				ERRORF("Trying to restart for release-date update a second time!");
				CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_PRELOAD_TIMER_EXPIRED);
				m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, IDS_OFFLINE,
					CLauncherDlg::ButtonParams(IDS_BUTTON_RETRY, ButtonCallbacks::ReleaseDateRestart, this),
					CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
				break;
			}

			ButtonCallbacks::ReleaseDateRestart(NULL);
			break;
		}
#else
		case eLoading:
		{
			if (Globals::needsLaunchConfirmation)
			{
				Globals::needsLaunchConfirmation = false;

				m_launcher->SetTextWithButtons(IDS_HEADING_SETUP_COMPLETE, IDS_READY_TO_LAUNCH,
					CLauncherDlg::ButtonParams(IDS_BUTTON_PLAY, ButtonCallbacks::Play, this),
					CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			}
			else
			{
				// Set UI state
				// Create the Pipe with the game

				// Game Pipe Init
				CGameCommunication::Instance().Listen();

				//@@: location CFSM_ONENTERSTATE_CREATE_LOADING_STRING
				m_launcher->SetTextWithSpinner(IDS_LOADING);
				// Start thread
				//@@: location CFSM_ONENTERSTATE_BEGIN_THREAD
				AfxBeginThread(ButtonCallbacks::LaunchGameThread, m_launcher);
			}
			break;
		}

		case eRestartGame:
		{
			m_launcher->SetTextWithSpinner(IDS_LOADING);
			m_launcher->SetLauncherVisibility(true, false);
			SetForegroundWindow(m_launcher->GetSafeHwnd());
			AfxBeginThread(ButtonCallbacks::RestartGameThread, m_launcher);
			break;
		}
#endif
		case eRequiredUpdate:
		{
			//@@: location CFSM_ONENTERSTATE_CREATE_DOWNLOAD_STRINGS
			m_launcher->SetTextWithButtons(0, IDS_UPDATE_REQUIRED,
				CLauncherDlg::ButtonParams(IDS_BUTTON_DOWNLOAD, ButtonCallbacks::Update, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
#if !RSG_FINAL
			if (Globals::autotest_launcher_gamepatch)
			{
				CTest::AutotestGamePatch();
			}
#endif
			break;
		}
#if !RSG_PRELOADER_LAUNCHER
		case eOptionalUpdate:
		{
			
			//@@: location CFSM_ONENTERSTATE_CREATE_OPTIONAL_DOWNLOAD_STRINGS
			m_launcher->SetTextWithButtons(0, IDS_UPDATE_AVAILABLE,
				CLauncherDlg::ButtonParams(IDS_BUTTON_PLAY, ButtonCallbacks::Play, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_DOWNLOAD, ButtonCallbacks::UpdateOptional, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			break;
		}
		case eDLCAvailable:
		{
			
			//@@: location CFSM_ONENTERSTATE_CREATE_DLC_DOWNLOAD_STRINGS
			m_launcher->SetTextWithButtons(0, IDS_DLC_AVAILABLE,
				CLauncherDlg::ButtonParams(IDS_BUTTON_PLAY, ButtonCallbacks::Play, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_DOWNLOAD, ButtonCallbacks::UpdateOptional, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			break;
		}
#endif
#if RSG_IN_PLACE_DOWNLOAD
		case eGameInstallAvailable:
		{
			m_launcher->SetTextWithButtons(0, IDS_GAMEINSTALL_AVAILABLE,
				CLauncherDlg::ButtonParams(IDS_BUTTON_DOWNLOAD, ButtonCallbacks::DownloadInPlaceData, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
#if !RSG_FINAL
			if (Globals::autotest_launcher_digitalupdate)
			{
				CTest::AutotestDigitalUpdate();
			}
#endif
			break;
		}
		case eMTLAvailable:
		{
			RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_MTL_ASKED_TO_UPGRADE);
			m_launcher->SetTextWithButtons(0, IDS_MTL_UPGRADE_AVAILABLE,
				CLauncherDlg::ButtonParams(IDS_BUTTON_START, ButtonCallbacks::MtlUpdate, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::CancelMtlUpdate, NULL));
			break;
		}
		case eDownloadInPlaceData:
		{
			m_launcher->SetProgressBar(IDS_HEADING_INSTALLING, IDS_DOWNLOADING_UPDATE, CLauncherDlg::ButtonParams(IDS_BUTTON_PAUSE, ButtonCallbacks::Pause, this));
			m_launcher->SendMessage(CLauncherDlg::eSetProgress, (WPARAM)0.0f, 0);
			WorkerThread::Instance().AddJob(WorkerThread::JOB_DOWNLOAD_IN_PLACE);
			break;
		}
		case eDownloadInPlaceDataError:
		{
			std::wstring errorMsgWString;
			CUtil::StdStringToStdWString(CError::GetLocalisedErrorMessage(CError::GetLastError()), errorMsgWString);

			m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, errorMsgWString,
				CLauncherDlg::ButtonParams(IDS_BUTTON_RETRY, ButtonCallbacks::Retry, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			break;
		}
		case eInPlaceIntegrityCheck:
		{
			m_launcher->SetProgressBar(IDS_HEADING_INSTALLING, IDS_VERIFYING_GAME_DATA);
			m_launcher->SendMessage(CLauncherDlg::eSetProgress, (WPARAM)0.0f, 0);
			WorkerThread::Instance().AddJob(WorkerThread::JOB_VERIFY_INTEGRITY);
			break;
		}
#endif
		case eDownloadUpdate:
		{
			
			//@@: location CFSM_ONENTERSTATE_DOWNLOAD_UPDATE_SET_PROGRESS_BAR
			m_launcher->SetProgressBar(IDS_HEADING_INSTALLING, IDS_DOWNLOADING_UPDATE, CLauncherDlg::ButtonParams(IDS_BUTTON_PAUSE, ButtonCallbacks::Pause, this));
			m_launcher->SendMessage(CLauncherDlg::eSetProgress, (WPARAM)0.0f, 0);
			WorkerThread::Instance().AddJob(WorkerThread::JOB_DOWNLOAD_UPDATE);
			break;
		}
#if !RSG_PRELOADER_LAUNCHER
		case eDownloadOptionalUpdate:
		{
			//@@: location CFSM_ONENTERSTATE_DOWNLOAD_OPTIONAL_UPDATE_CREATE_PLAY_TEXT
			m_launcher->SetProgressBar(IDS_HEADING_INSTALLING, IDS_DOWNLOADING_UPDATE, CLauncherDlg::ButtonParams(IDS_BUTTON_PLAY, ButtonCallbacks::Play, this));
			m_launcher->SendMessage(CLauncherDlg::eSetProgress, (WPARAM)0.0f, 0);
			WorkerThread::Instance().AddJob(WorkerThread::JOB_DOWNLOAD_UPDATE);
			break;
		}
		case eResumeDownloadingUpdate:
		{
			
			//@@: location CFSM_ONENTERSTATE_RESUME_DOWNLOAD_UPDATE_CREATE_PLAY_TEXT
			m_launcher->SetProgressBar(IDS_HEADING_INSTALLING, IDS_DOWNLOADING_UPDATE, CLauncherDlg::ButtonParams(IDS_BUTTON_PLAY, ButtonCallbacks::Play, this));
			m_launcher->SendMessage(CLauncherDlg::eSetProgress, (WPARAM)0.0f, 0);
			break;
		}
#endif
		case eInstallUpdate:
		{
			//@@: location CFSM_ONENTERSTATE_INSTALL_UPDATE_SET_PROGRESS_BAR
			m_launcher->SetTextWithSpinner(IDS_INSTALLING, CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));

			// This state is only used to change the UI while installing a downloaded an update
			// We haven't really left the download update state, so change back to it in case an error occurs
			m_state = eDownloadUpdate;
			break;
		}
		case eSocialClubError:
		{
			//@@: location CFSM_ONENTERSTATE_SOCIALCLUBERROR_GET_LAST_ERROR
			std::wstring errorMsgWString;
			CUtil::StdStringToStdWString(CError::GetLocalisedErrorMessage(CError::GetLastError()), errorMsgWString);

			if (CError::GetSupportURL().length() < 8)
			{
				m_launcher->SetTextWithButton(IDS_HEADING_ERROR, errorMsgWString, CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			}
			else
			{
				m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, errorMsgWString,
					CLauncherDlg::ButtonParams(IDS_BUTTON_GET_HELP, ButtonCallbacks::GetHelp, this),
					CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			}

			AUTOTEST_RESULT(CTest::SHOWN_ERROR_SCREEN, state);

			break;

		}
		case eErrorScreen:
		{
			//@@: location CFSM_ONENTERSTATE_ERRORSCREEN_GET_LAST_ERROR
			std::wstring errorMessage;
			CUtil::StdStringToStdWString(CError::GetLocalisedErrorMessage(CError::GetLastError()), errorMessage);
			CError::ReportErrorTelemetry(CError::GetLastError());

			if (CError::GetSupportURL().length() < 8)
			{
				DEBUGF1("No support URL detected.");
				m_launcher->SetTextWithButton(IDS_HEADING_ERROR, errorMessage, CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			}
			else
			{
				DEBUGF1("Support URL detected.");
				m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, errorMessage,
					CLauncherDlg::ButtonParams(IDS_BUTTON_GET_HELP, ButtonCallbacks::GetHelp, this),
					CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			}

			AUTOTEST_RESULT(CTest::SHOWN_ERROR_SCREEN, state);

			break;
		}
		case eExitCodeError:
		{
			std::wstring errorMessage;
			CUtil::StdStringToStdWString(CError::GetLocalisedErrorMessage(CError::GetLastError()), errorMessage);
			CError::ReportErrorTelemetry(CError::GetLastError());

			m_launcher->OnExitCodeError();
			m_launcher->SetTextWithButtons(IDS_HEADING_SORRY, errorMessage,
				CLauncherDlg::ButtonParams(IDS_BUTTON_SAFE_MODE, ButtonCallbacks::SafeModeRelaunch, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_RETRY, ButtonCallbacks::Relaunch, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));

			AUTOTEST_RESULT(CTest::SHOWN_ERROR_SCREEN, state);

			break;
		}
		case eNoTempSpace:
		{
			std::wstring errorMessage = CUtil::WideStringFromID(IDS_NO_TEMP_SPACE);

			std::wstring wspaceRequired;
			CUtil::StdStringToStdWString(CError::GetLastError().GetExtra(), wspaceRequired);
			CError::ReportErrorTelemetry(CError::GetLastError());

			std::vector<std::wstring> params;
			params.push_back(wspaceRequired);

			CUtil::FormatString(errorMessage, params);

			m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, errorMessage,
				CLauncherDlg::ButtonParams(IDS_BUTTON_BROWSE, ButtonCallbacks::BrowseForTemp, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_RETRY, ButtonCallbacks::Retry, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));

			AUTOTEST_RESULT(CTest::SHOWN_ERROR_SCREEN, state);

			break;
		}
		case eNoInstallSpace:
		{
			std::wstring errorMessage = CUtil::WideStringFromID(IDS_NO_INSTALL_SPACE);

			std::wstring wspaceRequired;
			CUtil::StdStringToStdWString(CError::GetLastError().GetExtra(), wspaceRequired);
			CError::ReportErrorTelemetry(CError::GetLastError());

			std::vector<std::wstring> params;
			params.push_back(wspaceRequired);

			CUtil::FormatString(errorMessage, params);

			m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, errorMessage,
				CLauncherDlg::ButtonParams(IDS_BUTTON_RETRY, ButtonCallbacks::Retry, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));

			AUTOTEST_RESULT(CTest::SHOWN_ERROR_SCREEN, state);

			break;
		}
		case eNoInternetForActivate:
		{
			//@@: location CFSM_ONENTERSTATE_NOINTERNET_CREATE_TEXT
			m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, IDS_OFFLINE_ACTIVATE,
				CLauncherDlg::ButtonParams(IDS_BUTTON_RETRY, ButtonCallbacks::OfflineRestart, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			AUTOTEST_RESULT(CTest::SHOWN_ERROR_SCREEN, state);
			break;
		}
		case eOffline:
		{
			//@@: location CFSM_ONENTERSTATE_OFFLINE_CREATE_TEXT
			std::wstring errorMessage;

			DWORD flags;
			if (!InternetGetConnectedState(&flags, 0))
			{
				errorMessage = CUtil::WideStringFromID(IDS_OFFLINE);
			}
			else
			{
				std::string errorString = CError::GetLocalisedErrorMessage(CError::GetLastError());
				CError::ReportErrorTelemetry(CError::GetLastError());
				CUtil::StdStringToStdWString(errorString, errorMessage);
			}

#if !RSG_PRELOADER_LAUNCHER
			//@@: location CFSM_ONENTERSTATE_OFFLINE_DOES_GAME_EXE_EXIST
			if (GamePatchCheck::DoesGameExecutableExist())
			{
				m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, errorMessage,
					CLauncherDlg::ButtonParams(IDS_BUTTON_OFFLINE, ButtonCallbacks::OfflineMode, this),
					CLauncherDlg::ButtonParams(IDS_BUTTON_RETRY, ButtonCallbacks::Retry, this),
					CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			}
			else
#endif
			{
				if (CError::GetSupportURL().length() < 8)
				{
					m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, errorMessage,
						CLauncherDlg::ButtonParams(IDS_BUTTON_RETRY, ButtonCallbacks::Retry, this),
						CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
				}
				else
				{
					m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, errorMessage,
						CLauncherDlg::ButtonParams(IDS_BUTTON_GET_HELP, ButtonCallbacks::GetHelp, this),
						CLauncherDlg::ButtonParams(IDS_BUTTON_RETRY, ButtonCallbacks::Retry, this),
						CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
				}
			}			

			AUTOTEST_RESULT(CTest::SHOWN_ERROR_SCREEN, state);
			break;
		}
		case eOfflineNoUAC:
		{
			//@@: location CFSM_ONENTERSTATE_OFFLINE_NO_UAC_CREATE_TEXT
#if !RSG_PRELOADER_LAUNCHER
			if (GamePatchCheck::DoesGameExecutableExist())
			{
				m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, IDS_ERROR_NO_UAC,
					CLauncherDlg::ButtonParams(IDS_BUTTON_OFFLINE, ButtonCallbacks::OfflineMode, this),
					CLauncherDlg::ButtonParams(IDS_BUTTON_RETRY, ButtonCallbacks::Retry, this),
					CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			}
			else
#endif
			{
				m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, IDS_ERROR_NO_UAC,
					CLauncherDlg::ButtonParams(IDS_BUTTON_RETRY, ButtonCallbacks::Retry, this),
					CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			}
			
			AUTOTEST_RESULT(CTest::SHOWN_ERROR_SCREEN, state);
			break;
		}
		case eSafeMode:
		{
			//@@: location CFSM_ONENTERSTATE_SAFEMODE_CREATE_MSG
			m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, Globals::safemode.WasSetFromCommandline() ? CUtil::WideStringFromID(IDS_SAFEMODE) : CUtil::WideStringFromID(IDS_BAD_SHUTDOWN),
				CLauncherDlg::ButtonParams(IDS_BUTTON_SAFE_MODE, ButtonCallbacks::SafeMode, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_NORMAL, ButtonCallbacks::Retry, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			Globals::safemode = false;
			AUTOTEST_RESULT(CTest::SHOWN_ERROR_SCREEN, state);
			break;
		}
		case eRestart:
		{
			ASSERT(AfxGetMainWnd() != NULL);
			AfxGetMainWnd()->SendMessage(WM_CLOSE);
			break;
		}
		case eNeedToRestartAsAdmin:
		{
			m_launcher->SetTextWithButtons(IDS_HEADING_ERROR, IDS_NEED_ADMIN_RESTART,
				CLauncherDlg::ButtonParams(IDS_BUTTON_RETRY, ButtonCallbacks::RestartAsAdmin, this),
				CLauncherDlg::ButtonParams(IDS_BUTTON_CANCEL, ButtonCallbacks::Exit, NULL));
			break;
		}
		case eRestartAsAdmin:
		{
			if (Globals::uac)
			{
				ERRORF("Trying to respawn with admin rights, when we should already have them!");
				CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, eOfflineNoUAC);
				break;
			}

			ButtonCallbacks::RestartWithArg(L"-uac", true, eOfflineNoUAC);
			
			break;
		}
		case eShuttingDown:
		{
			DEBUGF1("Shutting down.");
			break;
		}
		
	//@@: } CFSM_ONENTERSTATE_SWITCHING_STATES
	}
	//@@: location CFSM_ONENTERSTATE_EXIT_SWITCH
	return;
}

void CFsm::NextState()
{
	DEBUGF1("Going to next flow state...");
	if (m_applicationFlow.size() > 0 && m_applicationFlow[0] == m_state)
	{
		// Erase the completed state
#if !RSG_FINAL
		DEBUGF1("Erasing completed state (%s)", GetStateName(m_state));
#endif
		m_applicationFlow.erase(m_applicationFlow.begin());
	}

	// Default to launching the game
	
	//@@: location CFSM_NEXTSTATE_DEFAULT_STATE
#if RSG_PRELOADER_LAUNCHER
	FsmState nextState = eCheckForReleaseDate;
#else
	FsmState nextState = eLoading;
#endif


	// Depending on what state we're coming out of, we may have to do a download
	// or we may just be able to progress
	switch (m_state)
	{
		case eCheckForRGSCUpdates:
			if (!UpdateChecker::Instance().IsUpToDate())
			{
				DEBUGF1("Flow disrupted due to update available.");
				m_applicationFlow.insert(m_applicationFlow.begin(), eDownloadUpdate);
			}
			break;

		case eCheckForUpdate:
			if (!UpdateChecker::Instance().IsUpToDate())
			{
				DEBUGF1("Flow disrupted due to update available.");				
#if !RSG_IN_PLACE_DOWNLOAD
				if (UpdateChecker::Instance().GetPatchType() == PT_GAMEINSTALL)
				{
					ERRORF("Unexpected patch type!");
				}
#endif
				bool inserted = false;
				for (size_t i = 0; i < m_applicationFlow.size(); i++)
				{
					FsmState state = m_applicationFlow[i];
					if (state == eCloudSyncPreGame || state == eLoading || state == eMTLAvailable)
					{
						m_applicationFlow.insert(m_applicationFlow.begin()+i, eDownloadUpdate);
						inserted = true;
						break;
					}
				}
				if (!inserted)
				{
					m_applicationFlow.insert(m_applicationFlow.end()-1, eDownloadUpdate);
				}
			}
			break;
		case eExitCodeError:
			// url:bugstar:3000696 - SCUI - GTAV - After crashing, "retry" doesn't log you back in when the game reloads
			// => When clicking 'retry' to transition from a crashed game to a retry (loading) state, ensure that a signin
			//	  transfer file is available for the game client to sign in.
			if (nextState == eLoading)
			{
				m_launcher->CreateSignInTransferFile();
			}
			break;
#if RSG_IN_PLACE_DOWNLOAD
		case eCheckForInPlaceDownload:
			if (!InPlaceDownloader::IsUpToDate())
			{
				if (Globals::verify)
				{
					m_applicationFlow.insert(m_applicationFlow.begin(), eInPlaceIntegrityCheck);
				}
				m_applicationFlow.insert(m_applicationFlow.begin(), eDownloadInPlaceData);
			}
			break;
		case eInPlaceIntegrityCheck:
			if (!InPlaceDownloader::IsUpToDate())
			{
				m_applicationFlow.insert(m_applicationFlow.begin(), eInPlaceIntegrityCheck);
				m_applicationFlow.insert(m_applicationFlow.begin(), eDownloadInPlaceData);
			}
			break;
#endif
#if RSG_PRELOADER_LAUNCHER
		case eCheckForReleaseDate:
			if (CReleaseDateCheck::IsReleaseDateInFuture())
			{
				m_applicationFlow.insert(m_applicationFlow.begin(), eReleaseDateCountdown);
			}
			break;
#endif
	}

	// Moved this after, since all the above cases that change the current state also insert it at the front
	if (m_applicationFlow.size() > 0)
	{
		nextState = m_applicationFlow[0];
	}

		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, nextState);
}

void CFsm::RemoveFlowState(FsmState state)
{
	//@@: range CFSM_REMOVEFLOWSTATE {
	for (int i = (int)m_applicationFlow.size()-1; i >=0; i--)
	{
		if (m_applicationFlow[i] == state)
		{
			m_applicationFlow.erase(m_applicationFlow.begin() + i);
		}
	}
	//@@: } CFSM_REMOVEFLOWSTATE

}

void CFsm::GetCloudSaveBannedString(std::wstring& text)
{
	u64 cloudSaveBanEnd = m_launcher->GetCloudSaveBanTime();

	FILETIME filetime;
	FILETIME localfiletime;
	SYSTEMTIME systemtime;

	TimetToFileTime(cloudSaveBanEnd, &filetime);
	FileTimeToLocalFileTime(&filetime, &localfiletime);
	FileTimeToSystemTime(&localfiletime, &systemtime);

	std::vector<std::wstring> params;
	std::wstringstream ss;

	ss.str(L"");
	ss.clear();
	ss << systemtime.wYear;
	params.push_back(ss.str());

	ss.str(L"");
	ss.clear();
	if (systemtime.wMonth < 10)
		ss << L"0";
	ss << systemtime.wMonth;
	params.push_back(ss.str());

	ss.str(L"");
	ss.clear();
	if (systemtime.wDay < 10)
		ss << L"0";
	ss << systemtime.wDay;
	params.push_back(ss.str());

	ss.str(L"");
	ss.clear();
	ss << systemtime.wHour << L":" << systemtime.wMinute;
	params.push_back(ss.str());

	text = CLocalisation::Instance().GetWString(IDS_CLOUDSAVE_BANNED);
	CUtil::FormatString(text, params);
}