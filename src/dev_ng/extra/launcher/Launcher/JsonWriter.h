#pragma once

#include <string>

class JsonWriter
{
public:
	JsonWriter();
	JsonWriter(std::string& buffer);

	JsonWriter& StartObject(const char* name = NULL);
	JsonWriter& EndObject();
	JsonWriter& StartArray(const char* name = NULL);
	JsonWriter& EndArray();

	JsonWriter& WriteNamedString(const char* name, const char* value);
	JsonWriter& WriteString(const char* value);

	JsonWriter& WriteNamedBool(const char* name, bool value);
	JsonWriter& WriteBool(bool value);

	const std::string& GetBuffer() { return m_buffer; }

private:
	std::string& m_buffer;
	std::string m_internalBuffer;
	bool m_firstItem;
	void WriteCommaIfNeeded();
	void WriteName(const char* name);
	void WriteEscapedString(const char* string);
};