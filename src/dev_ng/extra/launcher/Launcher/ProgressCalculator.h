#pragma once

#include "Types.h"

#include "IPatchCheck.h"
#include "IDownloader.h"

#include <vector>
#include <string>
#include <queue>
#include <set>

class ProgressCalculator
{
public:
	enum ProgressType
	{
		PT_DOWNLOAD_UNKNOWN = 0,
		PT_DOWNLOAD_SC_UPDATE = 1,
		PT_DOWNLOAD_DIP_DATA = 2,
		PT_VERIFY_DIP_DATA = 3,
		PT_DOWNLOAD_PATCH = 4,
		PT_DOWNLOAD_BASE_DATA = 5,
		PT_DOWNLOAD_DLC = 6,
	};

	ProgressCalculator(ProgressType type);

	void Reset();
	
	void AddSegment(u64 bytesAlreadyDownloaded, u64 size);
	void AddSegmentsOfUnknownSize(int count);

	void AdjustSegment(int segment, u64 progress, u64 size);

	void SetMaxUpdateFrequency(u32 millis);

	void UpdateProgress(DownloadCompleteStatus status, u64 bytesDownloaded, u64 bytesTotal, u64 bytesJustDownloaded, int segment = -1);

	void SetCurrentSegment(int segment);
	void NextSegment();

	void SetSegmentComplete(int segment);

	bool IsUpdatedMessageAvailable();
	void ForceMessageUpdate();

	float GetCurrentProgress();
	void GetCurrentStatus(std::wstring& out_localisedStatus, float& out_progressScalar);

	static void AddToGlobalProgressList(ProgressCalculator* calc);
	static void RemoveFromGlobalProgressList(ProgressCalculator* calc);
	static float GetGlobalProgressScalar();

	struct DownloadStats
	{
		u64 bytesThisSession;
		u64 sessionStartTimeMillis;
		u32 connectionsOpened;
		u32 connectionsErrored;
		u32 verificationsFailed;

		void Reset();
	};

	void DownloadEvent(IDownloader::DownloadEvent event);
	const DownloadStats& GetDownloadStats() const;
	void ResetDownloadStats();
	void GetStatusString(std::string& out_status);

	ProgressType GetDownloadType() const;
	
	void ReportTelemetry(bool downloadEnding);

	u64 GetDownloadSessionDurationMillis() const;

private:
	void AppendTimeEstimate(std::wstring& out_statusString);

	struct Segment
	{
		s64 m_currentBytes;
		s64 m_totalBytes;
		bool m_complete;

		Segment(s64 current = 0, s64 total = -1) : m_currentBytes(current), m_totalBytes(total), m_complete(false) {}
	};

	std::vector<Segment> m_segments;
	int m_currentSegment;
	int m_unsizedSegments;

	u64 m_totalBytes;
	u64 m_totalDownloaded;

	int m_maxUpdateFreqMillis;
	u64 m_lastMessageUpdateTimeMillis;
	u64 m_lastProgressReceivedTimeMillis;
	u64 m_lastTelemUpdateTimeMillis;

	bool m_messageAvailable;
	bool m_forceMessageUpdate;
	DownloadCompleteStatus m_currentStatus;

	u64 m_movingAverageTotalBytes;
	u64 m_movingAverageTotalMillis;

	struct BandwidthDatapoint
	{
		u64 m_bytes;
		u64 m_millis;

		BandwidthDatapoint(u64 bytes, u64 millis) : m_bytes(bytes), m_millis(millis) {}
	};

	std::queue<BandwidthDatapoint> m_movingAverageData;

	u64 m_speedMovingAverageTotal;
	std::queue<u64> m_speedMovingAverageData;
	DownloadStats m_downloadStats;


	float m_progressScalar;

	static std::set<ProgressCalculator*> sm_globalProgress;

	bool m_hasConnected;

	ProgressType m_type;
};
