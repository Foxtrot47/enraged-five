#include "StdAfx.h"
#include "Globals.h"

#include "UpdateCheck.h"

#include "BootstrapPatchCheck.h"
#include "LauncherPatchCheck.h"
#include "RGSCPatchCheck.h"
#include "GamePatchCheck.h"
#include "MtlInstallCheck.h"
#include "DownloadManager.h"
#include "DLCPatchCheck.h"

#if RSG_LAUNCHER
#include "ProgressCalculator.h"
#endif

#include <sstream>

#include "Channel.h"

UpdateChecker::UpdateChecker(void)
	: m_bChecking(false), m_currentUpdate(0), m_eUpdateType(UT_MANDATORY), m_ePatchType(PT_GAME)
{
}

UpdateChecker::~UpdateChecker(void)
{
	//@@: location UPDATE_CHECKER_DESTRUCTOR
	for (size_t i = 0; i < m_patchChecks.size(); i++)
		delete m_patchChecks[i];
	m_patchChecks.clear();
}

UpdateChecker& UpdateChecker::Instance()
{
	//@@: location UPDATECHECKER_INSTANCE
	static UpdateChecker instance;
	return instance;
}

void UpdateChecker::Reset()
{
	//@@: location UPDATECHECKER_RESET
	m_patchesToInstall.clear();
	for (size_t i = 0; i < m_patchChecks.size(); i++)
		delete m_patchChecks[i];
	m_patchChecks.clear();
}

void UpdateChecker::AddBootstrapCheck()
{
	//@@: location UPDATECHECKER_ADDBOOTSTRAPCHECK
	m_patchChecks.push_back(new BootstrapPatchCheck());
}

void UpdateChecker::AddLauncherCheck()
{
	//@@: location UPDATECHECKER_ADDLAUNCHERCHECK
	m_patchChecks.push_back(new LauncherPatchCheck());
}

void UpdateChecker::AddRGSCCheck()
{
	//@@: location UPDATECHECKER_ADDRSGCCHECK
	m_patchChecks.push_back(new RGSCPatchCheck());
}

void UpdateChecker::AddGameCheck()
{
	//@@: location UPDATECHECKER_ADDGAMECHECK
	m_patchChecks.push_back(new GamePatchCheck());
}

void UpdateChecker::AddMtlCheck()
{
	m_patchChecks.push_back(new MtlInstallCheck());
}

void UpdateChecker::AddDLCCheck(rgsc::RockstarId rockstarId)
{
	std::stringstream strm;
	strm << rockstarId;

	m_patchChecks.push_back(new DLCPatchCheck(strm.str()));
}

void UpdateChecker::AddPatch(const Patch& patch)
{
	m_patchesToInstall.push_back(patch);
}


bool UpdateChecker::IsUpToDate()
{
	//@@: range UPDATECHECKER_ISUPTODATE_RANGE {
	
	//@@: location UPDATECHECKER_ISUPTODATE
	if (m_patchesToInstall.empty())
	{
		DEBUGF3("No patches to install; we are up-to-date.");
		return true;
	}
	else
	{

		DEBUGF3("%d patch(es) to install; we are not up-to-date.", m_patchesToInstall.size());
		return false;
	}
	//@@: } UPDATECHECKER_ISUPTODATE_RANGE
}


void UpdateChecker::DownloadComplete(DownloadCompleteStatus status, std::string filename)
{
	//@@: range UPDATECHECKER_DOWNLOADCOMPLETE {
	//@@: location UPDATECHECKER_DOWNLOADCOMPLETE_LOCATION
	m_patchChecks[m_currentUpdate]->DownloadComplete(status, filename); 
	//@@: } UPDATECHECKER_DOWNLOADCOMPLETE

}


void UpdateChecker::DownloadError(const CError& error)
{
	m_patchChecks[m_currentUpdate]->DownloadError(error);
	m_bChecking = false;
	m_patchesToInstall.clear();
}

bool UpdateChecker::CheckForUpdates(IDownloader::DownloadListener* listener)
{
	
	DEBUGF1("Checking for updates...");

	//@@: location UPDATECHECKER_CHECKFORUPDATES_SET_DEFAULT
	m_bChecking = true;
	
	m_patchesToInstall.clear();

	//DEBUG ONLY
#if !RSG_FINAL
	if (DebugSwitches::DebugTestMissingPatch)
	{
		WARNINGF("Testing missing patch!");
		Patch patch;
		patch.AlwaysDownload = true;
		patch.CommandlineArguments = std::string();
		patch.DeleteOnCompletion = false;
		patch.Description = "dummy patch";
		patch.PatchURL = "http://www.example.com/patch";

		m_patchesToInstall.push_back(patch);
	}
#endif
	DEBUGF1("Setting download listener...");
	//@@: location UPDATECHECKER_CHECKFORUPDATES_SET_DOWNLOAD_LISTENER
	Downloader::Instance().SetDownloadListener(listener);

	DEBUGF1("There is/are %u check(s) to do.", m_patchChecks.size());
	
	//@@: range UPDATECHCKER_CHECKFORUPDATES_INNER_LOOP {
	for (m_currentUpdate = 0; m_currentUpdate < m_patchChecks.size(); m_currentUpdate++)
	{
		DEBUGF3("Update check %d/%d starting...", (m_currentUpdate+1), m_patchChecks.size());

		// Run the current update check
		//@@: location UPDATECHECKER_CHECKFORUPDATES_CHECKFORPATCHES
		m_patchChecks[m_currentUpdate]->CheckForPatches();

		if (m_patchChecks[m_currentUpdate]->CheckError())
		{
			ERRORF("Update check %d/%d failed!", (m_currentUpdate+1), m_patchChecks.size());
			m_bChecking = false;
			m_currentUpdate = 0;

			return false;
		}

		DEBUGF3("Update check %d/%d done.", (m_currentUpdate+1), m_patchChecks.size());

		//@@: location UPDATECHECKER_CHECKFORUPDATES_CHECK_PATCH
		IPatchCheck* check = m_patchChecks[m_currentUpdate];

		if (!check->UpToDate())
		{
			DEBUGF3("Check %d was not up-to-date; adding patches.", (m_currentUpdate+1));

			//@@: location UPDATECHECKER_CHECKFORUPDATES_QUEUE_PATCHES
			for (u32 i = 0; i < check->GetPatches().size(); i++)
			{
				if (!check->GetPatches()[i].Installed)
				{
					DEBUGF2("Check %d patch %d is not installed; queueing.", (m_currentUpdate+1), i+1);
					m_patchesToInstall.push_back(check->GetPatches()[i]);

#if RSG_LAUNCHER
					ProgressCalculator* progress = new ProgressCalculator(check->GetPatches()[i].PatchType == PT_SCUI ? ProgressCalculator::PT_DOWNLOAD_SC_UPDATE : ProgressCalculator::PT_DOWNLOAD_PATCH);
					m_patchesToInstall.back().Progress = progress;
					ProgressCalculator::AddToGlobalProgressList(progress);
#endif
				}
				else
				{
					DEBUGF3("Check %d patch %d is installed.", (m_currentUpdate+1), i+1);
				}
			}
		}
		else
		{
			DEBUGF3("Check %d was up-to-date.", (m_currentUpdate+1));
		}
	
	}
	//@@: } UPDATECHCKER_CHECKFORUPDATES_INNER_LOOP

	// Reached the last patch
	DEBUGF3("No more checks.");
	m_bChecking = false;
	//@@: location UPDATECHECKER_CHECKFORUPDATES_NO_MORE_CHECKS
	m_currentUpdate = 0;

	// Check if there's any mandatory patches
	//@@: range UPDATECHCKER_CHECKFORUPDATES_SET_MANDATORY_TYPES {
	m_eUpdateType = UT_OPTIONAL;
	for (u32 i = 0; i < m_patchesToInstall.size(); i++)
	{
		if (m_patchesToInstall[i].UpdateType == UT_MANDATORY)
		{
			DEBUGF3("We have become mandatory.");
			m_eUpdateType = UT_MANDATORY;
			break;
		}
	}
	//@@: } UPDATECHCKER_CHECKFORUPDATES_SET_MANDATORY_TYPES

	// Check for DLC
	//@@: location UPDATECHECKER_CHECKFORUPDATES_SET_PATCH_TYPE
	m_ePatchType = PT_GAME;
	//@@: range UPDATECHCKER_CHECKFORUPDATES_SET_DLC_TYPES {
	for (u32 i = 0; i < m_patchesToInstall.size(); i++)
	{
		if (m_patchesToInstall[i].PatchType == PT_DLC)
		{
			DEBUGF3("We have DLC.");
			m_ePatchType = PT_DLC;
			break;
		}
	}
	//@@: } UPDATECHCKER_CHECKFORUPDATES_SET_DLC_TYPES

	// Check for game install
	for (u32 i = 0; i < m_patchesToInstall.size(); i++)
	{
		if (m_patchesToInstall[i].PatchType == PT_GAMEINSTALL)
		{
			DEBUGF3("We have DLC.");
			m_ePatchType = PT_GAMEINSTALL;
			break;
		}
	}

	//@@: location UPDATECHECKER_CHECKFORUPDATES_RETURN
	return true;
}

void UpdateChecker::Cancel()
{
	m_bChecking = false;
	if (m_currentUpdate < m_patchChecks.size())
	{
		m_patchChecks[m_currentUpdate]->Cancel();
	}
}
