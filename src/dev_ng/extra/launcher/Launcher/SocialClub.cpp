#include "stdafx.h"

#include "SocialClub.h"

// rgsc
#include "activation_interface.h"
#include "rgsc_common.h"

// launcher
#include "resource.h"
#include "LauncherDlg.h"
#include "CloudSaveManager.h"
#include "Util.h"
#include "Globals.h"
#include "Config.h"
#include "OnlineConfig.h"
#include "CertificateVerify.h"
#include "GameCommunication.h"
#include "seh.h"
#include "Error.h"
#include "Channel.h"
#include "JsonReader.h"

#include "RgscTelemetryManager.h"

#if RSG_STEAM_LAUNCHER
#include "Steam.h"
#endif

#include <regex>

bool ending = false;
bool threadRunning = false;
int SocialClub::sm_CurrentContentId = 0;
bool SocialClub::sm_bShowActivationUi = false;
bool SocialClub::sm_bSocialClubBanned = false;
u64 SocialClub::sm_uSocialClubBanEndDate = 0;

static char sm_PrePopulateKey[MAX_PATH] = {0};

EntitlementManager SocialClub::sm_EntitlementManager;
std::string SocialClub::m_headerInfo;

static CRITICAL_SECTION s_GameEventLock;

static rgsc::RgscGamepad* s_ScuiPads[rgsc::RGSC_MAX_PADS];
unsigned s_NumScuiPads;

#define MAIN_LICENSE_ID 0


#if RSG_LAUNCHER
u8 rosTitleSecrets[] = {
	0x8F, 0x10, 0xB6, 0xAE, 0x59, 0x1C, 0x87, 0x18, 0xE4, 0x49, 0x03, 0x8A, 0x02, 0x93, 0x99, 0xB5,
	0x88, 0x92, 0x44, 0x14, 0x46, 0xBD, 0xC6, 0xA3, 0x23, 0xA9, 0xB1, 0x91, 0x48, 0xA0, 0x88, 0x8F,
	0x0F, 0xC6, 0x6A, 0x6B, 0x82, 0xC9, 0xCE, 0x3F, 0x8F, 0xA4, 0x69, 0x2A, 0xBD, 0x83, 0xD1, 0x36,
	0xD6, 0x6E, 0xA4, 0x62, 0x94, 0x32, 0xEC, 0x6B, 0xF4, 0xE9, 0xFC, 0xD9, 0x18, 0xC0, 0x74, 0x59,
	0xA9, 0x12, 0x24, 0xE7, 0x47, 0xFD, 0x44, 0xE1, 0x43, 0x55, 0x74, 0x6E, 0x55, 0x60, 0xC8, 0xEE,
	0x32, 0x8A, 0x8A, 0x8F, 0x57, 0x87, 0x47, 0xC6, 0xED, 0xA7, 0x9A, 0xAA, 0x96, 0x6E, 0x39, 0x77,
	0xE8, 0xEA, 0x36, 0xB6, 0x4A, 0x61, 0xC6, 0x2E, 0x1F, 0x56, 0x91, 0x19, 0x34, 0xA2, 0x34, 0xEB,
	0xCC, 0xB6, 0x20, 0xF1, 0xB8, 0xE2, 0xEE, 0x9B, 0x17, 0x6F, 0xB8, 0xC0, 0x9A, 0xC8, 0x0E, 0x21,
	0x21, 0x18, 0x09, 0xC1, 0xF4, 0x6F, 0x1A, 0xAE, 0x0A, 0xC9, 0xC9, 0xE8, 0x12, 0xA8, 0x40, 0xD5,
	0x51, 0xE1, 0x16, 0xA3, 0xA5, 0xB4, 0xA3, 0x36, 0x54, 0x9B, 0xFB, 0x14, 0x36, 0x3D, 0x88, 0xCA,
	0xC7, 0x5C, 0xF6, 0x24, 0x3C, 0xD5, 0xF2, 0xB4, 0x85, 0x13, 0x31, 0x8D, 0xB6, 0xDF, 0xD0, 0x29,
	0xEB, 0xBB, 0x93, 0x8F, 0x89, 0x4F, 0xCD, 0x2D, 0x73, 0x15, 0x16, 0x4D, 0x21, 0x21, 0x81, 0xFD,
	0xE3, 0xAF, 0xF9, 0xE2, 0x24, 0xF5, 0x89, 0xAA, 0xBD, 0xDD, 0xBA, 0x67, 0x65, 0xF4, 0x7F, 0xB9,
	0xF4, 0x84, 0x5F, 0xE8, 0x2B, 0x22, 0x78, 0x29, 0x3A, 0xA9, 0xA5, 0x21, 0x9E, 0x2E, 0xCF, 0x01,
	0x76, 0xE8, 0x39, 0x12, 0xDB, 0xAD, 0x09, 0x11, 0x48, 0x81, 0x64, 0xCC, 0x8F, 0xFA, 0x9E, 0x92,
	0x8C, 0x80, 0xE7, 0xCA, 0x83, 0xE8, 0xF0, 0x1C, 0xD9, 0x5C, 0x63, 0x16, 0x3C, 0xAD, 0x60, 0x9A
	
};
#endif

SocialClub::SocialClub()
	: m_surface(NULL), m_hRgscDll(NULL), m_hParentWnd(NULL), m_pThread(NULL), m_rgsc(NULL), m_rgscUI(NULL), m_ActivationSystem(NULL), m_profileManager(NULL), m_TaskManager(NULL), 
	m_CommerceManager(NULL), m_Telemetry(NULL), m_CloudSaveManager(NULL), m_FileSystem(NULL), m_GamepadManager(NULL), m_bTextboxHasFocus(false), m_lastUpdateTime(0)
{
	DEBUGF1("Social Club constructed.");
	//@@: range SOCIALCLUB_CONSTRUCTOR {
	m_InitializationErrorCode = SC_INIT_ERR_NONE;
	ending = false;
	//@@: location SOCIALCLUB_CONSTRUCTOR_SET_VALUES
	m_bWasUIShowing = false;
	m_bWasSignedIn = false;
	m_bInitialized = false;
	m_bWaitingForSignIn = true;
	m_bWaitingForActivation = false;
	m_bWaitingForTicketRefresh = false;
	m_bSignInScuiEvent = false;
	m_bSignInNotificationEvent = false;
	PrePopulateKey();

	memset(s_ScuiPads, 0, sizeof(s_ScuiPads));
	s_NumScuiPads = 0;

	m_Argvw = NULL;

	InitializeCriticalSection(&m_CriticalSection);
	InitializeCriticalSection(&s_GameEventLock);
	//@@: } SOCIALCLUB_CONSTRUCTOR

}

SocialClub::~SocialClub()
{
	if (threadRunning && m_pThread)
	{
		// Signal thread to shutdown
		ending = true;

		WaitForSingleObject(m_pThread->m_hThread, INFINITE);
	}

	if (m_Argvw)
	{
		LocalFree(m_Argvw);
		for (size_t i = 0; i < m_Argvmb.size(); i++)
		{
			delete[] m_Argvmb[i];
		}
		m_Argvmb.clear();
		m_Argvw = NULL;
	}

	Uninitialize();

	DeleteCriticalSection(&s_GameEventLock);
	DeleteCriticalSection(&m_CriticalSection);

	DEBUGF1("Social Club destructed.");
}

bool SocialClub::Initialize(DxSurface* surface, HWND parentWindow)
{
	DISPLAYF("Social Club initializing...");

	//@@: location SOCIALCLUB_INITIALIZE_CHECK_SURFACE
	if (surface == NULL)
	{
		ERRORF("No surface.");
		return FALSE;
	}
	//@@: range SOCIALCLUB_INITIALIZE_INITIALIZE_VALUES {

	m_surface = surface;
	//@@: location SOCIALCLUB_INITIALIZE_SET_PAINT_DELEGATE
	surface->SetPaintDelegate(this);
	m_hParentWnd = parentWindow;
	m_bWasUIShowing = false;
	m_bWasSignedIn = false;
	//@@: } SOCIALCLUB_INITIALIZE_INITIALIZE_VALUES

	RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_SC_START);
	
	struct Closure
	{
		static void HandleException()
		{
			CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_SEH_EXCEPTION);
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eSocialClubError);
		}

		static UINT ThreadEntry(LPVOID ptr)
		{
			SocialClub* thiz = (SocialClub*)ptr;
			__try
			{
				//TODO: Does it matter what we name this thread? Can I encode this in Util.h?
				//DONE: ARS - I don't think so. I haven't seen any adverse behaviour from this yet.
				CUtil::SetCurrentThreadName("Social Club link thread");
				thiz->ThreadEntryPoint();
			}
			__except(0)
			{
				ERRORF("Exception thrown.");

				if (thiz)
				{
					ERRORF("Emergency SC shutdown.");
					thiz->DoShutdown();
				}

				HandleException();
			}

			return 0;
		}
	};

	//@@: range SOCIALCLUB_INITIALIZE_INITIALIZE_THREAD {
	if (!threadRunning)
	{
		//@@: location SOCIALCLUB_INITIALIZE_CHECK_THREAD_RUNNING
		threadRunning = true;
		m_pThread = AfxBeginThread(Closure::ThreadEntry, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		m_pThread->m_bAutoDelete = FALSE;
		m_pThread->ResumeThread();
	}

	//@@: } SOCIALCLUB_INITIALIZE_INITIALIZE_THREAD

	//@@: location SOCIALCLUB_INITIALIZE_EXIT
	return TRUE;

}

void SocialClub::Uninitialize()
{
	//@@: location SOCIALCLUB_UNINITIALIZE
	DISPLAYF("Uninitializing.");
	if (m_surface)
	{
		m_surface->SetPaintDelegate(NULL);
		m_surface->DestroyDevice();
		m_surface = NULL;
	}

	m_bInitialized = false;
}

bool SocialClub::IsInitialized()
{
	return m_bInitialized && m_rgscUI && m_rgscUI->IsReadyToAcceptCommands() && m_rgscUI->IsVisible();
}

void SocialClub::ThreadEntryPoint()
{
	DEBUGF1("Starting Social Club thread...");

	//@@: range SOCIALCLUB_THREADENTRYPOINT_LOAD_DLL {
	if (!LoadDll())
	{
		ERRORF("Unable to load Social Club DLL!");
		threadRunning = false;
		return;
	}
	else
	{
		DEBUGF1("Social Club seems to have loaded correctly.");
	}
	//@@: } SOCIALCLUB_THREADENTRYPOINT_LOAD_DLL

	//@@: range SOCIALCLUB_THREADENTRYPOINT_INITIALIZE_DLL {
	if (!sm_EntitlementManager.Initialize(this))
	{
		DEBUGF1("Unable to initialize Entitlement!");
		threadRunning = false;
		return;
	}
	else
	{
		DEBUGF1("Entitlement seems to have initialized correctly.");
	}
	//@@: } SOCIALCLUB_THREADENTRYPOINT_INITIALIZE_DLL

	bool readyForCommands = false;

	{
		RAD_TELEMETRY_ZONE("Waiting for ready");
		while (!ending && !m_rgscUI->IsReadyToAcceptCommands())
		{
			m_rgsc->Update();
			Sleep(0);

			// SCUI has initialized but failed to load the website. Set the init error code.
			if (m_rgscUI->IsInFailState())
			{
				ERRORF("SCUI load failed!");
				m_InitializationErrorCode = SC_INIT_ERR_WEBSITE_FAILED_LOAD;
				threadRunning = false;
				return;
			}
		}
	}

	if (!ending)
	{
		DEBUGF1("SCUI is ready to accept commands.  Showing SCUI...");
		RAD_TELEMETRY_ZONE("Showing SCUI");

		if (m_profileManager->IsSignedIn() == false)
		{
			m_rgscUI->ShowSignInUi();
		}
		else
		{
			m_rgscUI->ShowUi();
		}

		while (!ending && !m_rgscUI->IsVisible())
		{
			RAD_TELEMETRY_ZONE("Waiting for visible");
			m_rgsc->Update();
			Sleep(1);
		}
	}


	if (!ending)
	{
		RAD_TELEMETRY_ZONE("Prerendering");

		// Note that this should be removed after B*2069665
		DEBUGF1("Pre-rendering...");
		for (int i = 0; i < 30; i++)
		{
			{
				RAD_TELEMETRY_ZONE("Update");
				m_rgsc->Update();
				m_surface->Render();
			}
		}

		DEBUGF1("SCUI is visible.");
		m_surface->Show(true);
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetControlsVisibility, 0);
		m_rgscUI->EnableClosingUi(false);
	}

	while (!ending)
	{
		RAD_TELEMETRY_WAIT_ZONE("SC Update Loop");
		{
			RAD_TELEMETRY_ZONE("SC Update 1");
			Update();
		}

		//@@: location SOCIALCLUB_THREADENTRYPOINT_ONUPDATE
		{
			RAD_TELEMETRY_ZONE("SC Update 2");
			m_rgsc->Update();
		}

		{
			RAD_TELEMETRY_ZONE("SC Render");
			m_surface->Render();
		}

		if (m_rgscUI->IsReadyToAcceptCommands())
		{
			if (!readyForCommands)
			{
				readyForCommands = true;
				DEBUGF1("SCUI is ready for commands.");
				if (m_rgscUI->IsVisible())
				{
					DEBUGF1("SCUI is visible.");
				}
				else
				{
					DEBUGF1("SCUI is not visible, however.");
				}
			}

			//@@: range SOCIALCLUB_THREADENTRYPOINT_NOT_SIGNED_IN {
			if (m_profileManager->IsSignedIn() == false)
			{
				if(!m_rgscUI->IsVisible())
				{
					DEBUGF1("Not signed-in; showing SCUI.");
					m_rgscUI->ShowSignInUi();
					PostMessage(m_hParentWnd, CLauncherDlg::eSetLauncherVisibility, TRUE, TRUE);
				}
			}
			else
			{
 				if (m_bWaitingForSignIn && m_bSignInScuiEvent && m_bSignInNotificationEvent)
				{
					m_bWaitingForSignIn = false;
					m_bSignInScuiEvent = false;
					m_bSignInNotificationEvent = false;
					DEBUGF1("Signed-in; continuing.");

					// Load in cached telemetry
					RgscTelemetryManager::Initialize(GetRockstarId());

					CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);
				}
				else if (!m_bSignInScuiEvent && m_profileManager->IsSignedIn())
				{
					m_bSignInScuiEvent = true;
				}
			}
			//@@: } SOCIALCLUB_THREADENTRYPOINT_NOT_SIGNED_IN
		}

		// Rate limit due to now presenting immediately
		if (m_lastUpdateTime != 0)
		{
			u64 elapsed = CUtil::CurrentTimeMillis() - m_lastUpdateTime;
			const u64 MIN_FRAME_TIME_MILLIS = 33;
			u64 millisEarly = MIN_FRAME_TIME_MILLIS - elapsed;
			
			RAD_TELEMETRY_WAIT_ZONE("ScSleep");
			
			// Less sleeping when we're communicating with the cloud server to poll rgsc faster than our usual update frequency.
			if (!OnlineConfig::CloudSaveWorkerThread && CloudSaveManager::Instance() != NULL &&
				(CloudSaveManager::Instance()->IsSyncingCloudSaves() && !CloudSaveManager::Instance()->IsWaitingForUserAction()))
			{
				Sleep(1);
			}
			else if (millisEarly <= MIN_FRAME_TIME_MILLIS)
			{
				Sleep((DWORD)millisEarly);
			}
			else
			{
				Sleep(0);
			}
		}

		m_lastUpdateTime = CUtil::CurrentTimeMillis();
		

#if RSG_DEBUG
		static bool debuglog = false;

		if (debuglog)
		{
			debuglog = false;

			std::string command = "{\"Commands\":[{ \"Command\" : \"ShowDebugLog\" }]}";
			SendMessageToScui(command.c_str());
		}
#endif
	}

	if (m_profileManager && m_profileManager->IsSignedIn() && m_Telemetry->HasAnyMetrics())
	{
		DEBUGF1("Want to shut down Social Club, flushing telemetry...");
		m_Telemetry->Flush(true);
	}

	DEBUGF1("Preparing to end Social Club thread, waiting for ready...");
	
	while (!m_rgsc->IsReadyToShutdown())
	{
		m_rgsc->Update();
		m_surface->Render();
		Sleep(1);
	}

	DEBUGF1("Thread no longer running.");
	threadRunning = false;
	
	DoShutdown();
}

void SocialClub::DoShutdown()
{
	// Don't shutdown unless Init() succeeded
	if (m_rgsc && m_bInitialized)
	{
		// Check we can shutdown before attempting to do so
		while (!m_rgsc->IsReadyToShutdown())
		{
			m_rgsc->Update();
			Sleep(1);
		}

		//@@: range SOCIALCLUB_THREADENTRYPOINT_SHUTDOWN_ENTITLEMENT {
		DEBUGF1("Shutting down Entitlement...");
		sm_EntitlementManager.Shutdown();
		//@@: } SOCIALCLUB_THREADENTRYPOINT_SHUTDOWN_ENTITLEMENT

		DEBUGF1("Shutting down CloudSaveManager...");
		CloudSaveManager::Shutdown();

		DEBUGF1("Shutting down SCUI...");
		if (m_rgscUI)
		{
			m_rgscUI->OnLostDevice();
		}

		DEBUGF1("Shutting down RGSC...");
		m_rgsc->Shutdown();
		m_rgscUI = NULL;
		m_rgsc = NULL;
	}

	//@@: range SOCIALCLUB_THREADENTRYPOINT_FREE_LIBRARY {
	// Don't free the library unless Init() succeeded
	if (m_hRgscDll && m_bInitialized)
	{
		DEBUGF1("Freeing library...");
		FreeLibrary(m_hRgscDll);
	}
	//@@: } SOCIALCLUB_THREADENTRYPOINT_FREE_LIBRARY

	m_bInitialized = false;

	if (m_surface)
	{
		m_surface->DestroyDevice();
		m_surface = NULL;
	}

	RgscTelemetryManager::Shutdown();
}

bool FileExists(wchar_t* name)
{
	FILE* file;
	if (_wfopen_s(&file, name, L"rb") == S_OK)
	{
		fclose(file);
		return true;
	}

	return false;
}

bool SocialClub::CreateSocialClubPath(wchar_t (&path)[MAX_PATH])
{
	rtry
	{
		//@@: range SOCIALCLUB_LOADDLL_BUILD_DLL_PATH {
		//@@: location SOCIALCLUB_LOADDLL_GET_PROG_FILES
		rverify(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, SHGFP_TYPE_CURRENT, path) == S_OK, catchall, m_InitializationErrorCode = SC_INIT_ERR_PROGRAM_FILES_NOT_FOUND);

#if RSG_DEBUG
		wcscat_s(path, MAX_PATH, L"\\Rockstar Games\\Social Club Debug\\socialclub.dll");
#else
		wcscat_s(path, MAX_PATH, L"\\Rockstar Games\\Social Club\\socialclub.dll");
#endif
		//@@: } SOCIALCLUB_LOADDLL_BUILD_DLL_PATH

		return true;
	}
	rcatchall
	{
		return false;
	}
}

#if RSG_STEAM_LAUNCHER
void SocialClub::InstallDll()
{
	DEBUGF1("Steam - Install DLL");
	RAD_TELEMETRY_ZONE("InstallDll");

	rtry
	{
		// get social club path
		wchar_t path[MAX_PATH] = {0};
		rcheck(CreateSocialClubPath(path), catchall, );

		// check if file exists
		rcheck(!CUtil::FileExists(path), catchall, );

		// if file doesn't exist, get the current module directory
		wchar_t folderPath[MAX_PATH] = {0};
		rverify(GetModuleFileNameW( NULL, folderPath, MAX_PATH ), catchall, );

		// Find last slash in path, replace with 0.
		wchar_t* lastSlash = wcsrchr(&folderPath[0], '\\');
		rcheck(lastSlash, catchall, );
		lastSlash++;
		*lastSlash = 0;

		// Does this path exist?
		rcheck(wcscat_s(folderPath, L"Installers") == 0, catchall, );
		rcheck(PathFileExistsW(folderPath), catchall, );

		// Create the folder path - append \
		// Create the search path - append \*
		wchar_t searchPath[MAX_PATH] = {0};
		wcscpy_s(searchPath, folderPath);
		rcheck(wcscat_s(folderPath, L"\\") == 0, catchall, );
		rcheck(wcscat_s(searchPath, L"\\*") == 0, catchall, );

		// Find first file
		WIN32_FIND_DATAW FindFileData;
		HANDLE hFind = FindFirstFileW(searchPath, &FindFileData);
		rcheck(INVALID_HANDLE_VALUE != hFind, catchall, );
		wchar_t fullPath[MAX_PATH] = {0};

		LARGE_INTEGER filesize;
		bool bFoundInstaller = false;

		// Iterate all files in this folder
		do
		{
			fullPath[0] = L'\0';
			// log out directories seperately
			if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				DEBUGF1("  %s   <DIR>", FindFileData.cFileName);
			}
			else
			{
				// We're interested in files
				filesize.LowPart = FindFileData.nFileSizeLow;
				filesize.HighPart = FindFileData.nFileSizeHigh;

				// Create the full path by combining the folder paht and the filename
				wcscat_s(fullPath, MAX_PATH, folderPath);
				wcscat_s(fullPath, MAX_PATH, FindFileData.cFileName);

				// Find the Rockstar verified installer. Steam will only include one in this folder
				if (CertificateVerify::Verify(fullPath))
				{
					bFoundInstaller = true;
					break;
				}
			}
		}
		while (FindNextFileW(hFind, &FindFileData) != 0); // look for more

		// Check that we actually found the installer
		rcheck(bFoundInstaller, catchall, );

		// Run the installer silently
		wcscat_s(fullPath, L" /silent");

		PROCESS_INFORMATION processInformation = {0};
		STARTUPINFO startupInfo                = {0};
		startupInfo.cb                         = sizeof(startupInfo);

		BOOL result = CreateProcess(NULL, fullPath,  NULL, NULL, FALSE, 
									NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW, 
									NULL, NULL, &startupInfo, &processInformation);
		rcheck(result == TRUE, catchall, );

		// Wait until child process exits.
		WaitForSingleObject( processInformation.hProcess, INFINITE );

		// Close process and thread handles. 
		CloseHandle( processInformation.hProcess );
		CloseHandle( processInformation.hThread );
	}
	rcatchall
	{

	}
}
#endif

bool SocialClub::LoadDll()
{
	DEBUGF1("Loading DLL...");
	RAD_TELEMETRY_ZONE("LoadDll");
	//@@: location SOCIALCLUB_LOADDLL_ENTRY
	rtry
	{

		wchar_t path[MAX_PATH] = {0};
		rverify(CreateSocialClubPath(path), catchall, );
		
#if RSG_STEAM_LAUNCHER
		InstallDll();
#endif
		rverify(FileExists(path), catchall, m_InitializationErrorCode = SC_INIT_ERR_NO_DLL);

#if !RSG_DEBUG
#if !RSG_FINAL
		if (Constants::CheckingCertificates)
#endif
		{
			//@@: location SOCIALCLUB_LOADDLL_CERTIFICATE_VERIFY
			rcheck(CertificateVerify::Verify(path), catchall, m_InitializationErrorCode = SC_INIT_ERR_BAD_SIGNATURE);
		}
#endif

		DEBUGF1("Calling LoadLibraryExW: %ls", path);
		//@@: range SOCIALCLUB_LOADDLL_LOAD_LIBRARY {
		//@@: location SOCIALCLUB_LOADDLL_LOAD_LIBRARY_LOCATION
		m_hRgscDll = LoadLibraryExW(path, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
		rcheck(m_hRgscDll, catchall, m_InitializationErrorCode = SC_INIT_ERR_LOAD_LIBRARY_FAILED);
		//@@: } SOCIALCLUB_LOADDLL_LOAD_LIBRARY

		//@@: range SOCIALCLUB_LOADDLL_INIT_DLL {
		//@@: location SOCIALCLUB_LOADDLL_INIT_DLL_LOCATION
		//@@: range SOCIALCLUB_LOADDLL_INIT_DLL_CALL {
		if (!InitDll())
			//@@: } SOCIALCLUB_LOADDLL_INIT_DLL_CALL

		{
			//@@: location SOCIALCLUB_LOADDLL_CHECK_RGSC
			ERRORF("Failed to init SC DLL!");
			//@@: range SOCIALCLUB_LOADDLL_FREE_LIBRARY {
			if (m_rgsc && m_bInitialized)
			{
				// Check we can shutdown before attempting to do so
				while (!m_rgsc->IsReadyToShutdown())
				{
					m_rgsc->Update();
					Sleep(1);
				}

				m_rgsc->Shutdown();
				m_rgsc = NULL;

				FreeLibrary(m_hRgscDll);
			}

			//@@: } SOCIALCLUB_LOADDLL_FREE_LIBRARY
			m_hRgscDll = NULL;
			//@@: location SOCIALCLUB_LOADDLL_RETURN_INIT
			return false;
		}

		rcheck(m_rgsc != NULL, catchall, m_InitializationErrorCode = SC_INIT_ERR_RGSC_NULL_AFTER_LOAD);
		//@@: } SOCIALCLUB_LOADDLL_INIT_DLL

	}
	rcatchall
	{
		CError::SetError(ERROR_CATEGORY_SOCIAL_CLUB, m_InitializationErrorCode);
		CUtil::PrintSystemErrorMessage(GetLastError());
		PostMessage(m_hParentWnd, CLauncherDlg::eSetState, CFsm::eSocialClubError, 0);
		//@@: location SOCIALCLUB_LOADDLL_CATCH_ALL
		return false;
	}
	//@@: location SOCIALCLUB_LOADDLL_EXIT
	return true;
}

bool SocialClub::InitDll()
{
	int argc = 0;
	//@@: location SOCIALCLUB_INITDLL_ENTRY


	bool retval = true;

	DEBUGF3("Initialising DLL...");
	RAD_TELEMETRY_ZONE("InitDLL");
	rtry
	{
		RAD_TELEMETRY_ENTER("Pre-init");

		//@@: range SOCIALCLUB_INITDLL_GET_INTERFACE_ADDRESS {
		typedef rgsc::IRgsc* (*GetRgscInstancePtr)();
		const unsigned int kFunctionOrdinal = 1;

		//@@: location SOCIALCLUB_INITDLL_GETPROCADDRESS
		DEBUGF3("Getting proc address...");
		GetRgscInstancePtr func = (GetRgscInstancePtr)GetProcAddress(m_hRgscDll, (LPCSTR)kFunctionOrdinal);
		rcheck(func, catchall, m_InitializationErrorCode = SC_INIT_ERR_GET_PROC_ADDRESS_FAILED);

		
		rgsc::IRgsc* rgsc = func();
		//@@: location SOCIALCLUB_INITDLL_QUERY_INTERFACE_PROFILE_MGR
		rcheck(rgsc, catchall, m_InitializationErrorCode = SC_INIT_ERR_CALL_EXPORTED_FUNC_FAILED);
		DEBUGF3("Obtaining interfaces...");
		HRESULT hr = rgsc->QueryInterface(rgsc::IID_IRgscV6, (void**)&m_rgsc);
		rcheck((hr == S_OK), catchall, m_InitializationErrorCode = SC_INIT_ERR_QUERY_INTERFACE_FAILED);
		hr = m_rgsc->GetProfileManager()->QueryInterface(rgsc::IID_IProfileManagerV2, (void**) &m_profileManager);
		rcheck((hr == S_OK), catchall, m_InitializationErrorCode = SC_INIT_ERR_QUERY_INTERFACE_FAILED);
		//@@: } SOCIALCLUB_INITDLL_GET_INTERFACE_ADDRESS

		rgsc::TitleId rgscTitleId;

		// convert language enums
		//@@: location SOCIALCLUB_INITDLL_LOCALISE
		rgsc::RgscLanguage rgscLang;

		WORD langId = PRIMARYLANGID(CLocalisation::Instance().GetCurrentLanguage());
		WORD sublangId = SUBLANGID(CLocalisation::Instance().GetCurrentLanguage());
		switch(langId)
		{
		case LANG_ENGLISH:
			rgscLang = rgsc::RGSC_LANGUAGE_ENGLISH;
			break;
		case LANG_SPANISH:
			if (sublangId == SUBLANG_SPANISH_MEXICAN)
				rgscLang = rgsc::RGSC_LANGUAGE_MEXICAN;
			else
				rgscLang = rgsc::RGSC_LANGUAGE_SPANISH;
			break;
		case LANG_FRENCH:
			rgscLang = rgsc::RGSC_LANGUAGE_FRENCH;
			break;
		case LANG_GERMAN:
			rgscLang = rgsc::RGSC_LANGUAGE_GERMAN;
			break;
		case LANG_ITALIAN:
			rgscLang = rgsc::RGSC_LANGUAGE_ITALIAN;
			break;
		case LANG_JAPANESE:
			rgscLang = rgsc::RGSC_LANGUAGE_JAPANESE;
			break;
		case LANG_RUSSIAN:
			rgscLang = rgsc::RGSC_LANGUAGE_RUSSIAN;
			break;
		case LANG_PORTUGUESE:
			rgscLang = rgsc::RGSC_LANGUAGE_PORTUGUESE;
			break;
		case LANG_POLISH:
			rgscLang = rgsc::RGSC_LANGUAGE_POLISH;
			break;
		case LANG_KOREAN:
			rgscLang = rgsc::RGSC_LANGUAGE_KOREAN;
			break;
		case LANG_CHINESE:
			if (sublangId == SUBLANG_CHINESE_TRADITIONAL)
			{
				rgscLang = rgsc::RGSC_LANGUAGE_CHINESE_TRADITIONAL;
			}
			else
			{
				rgscLang = rgsc::RGSC_LANGUAGE_CHINESE_SIMPLIFIED;
			}
			break;
		case LANG_CHINESE_TRADITIONAL:
			rgscLang = rgsc::RGSC_LANGUAGE_CHINESE_TRADITIONAL;
			break;
		default:
			rgscLang = rgsc::RGSC_LANGUAGE_ENGLISH;
			break;
		}

		DEBUGF3("Setting up ROS interface...");

		rgsc::ITitleId::RosEnvironment rgscEnvironment;

		std::string env;
		CUtil::GetEnvStr(env);

		if (env == "dev")
		{
			rgscEnvironment = rgsc::ITitleId::RLROS_ENV_DEV;
		}
		else
		{
			rgscEnvironment = rgsc::ITitleId::RLROS_ENV_PROD;
		}

		rgsc::ITitleId::Platform rgscPlatform;

#if RSG_STEAM_LAUNCHER
		rgscPlatform = rgsc::ITitleId::PLATFORM_STEAM;
#else
		rgscPlatform = rgsc::ITitleId::PLATFORM_PC;
#endif

		//@@: range SOCIALCLUB_INITDLL_SET_TITLE_QUALIFIERS {

		rgscTitleId.SetPlatform(rgscPlatform);
		rgscTitleId.SetRosEnvironment(rgscEnvironment);
		rgscTitleId.SetRosTitleVersion(Constants::RosTitleVersion);
		rgscTitleId.SetScVersion(Constants::RosScVersion);
		rgscTitleId.SetRosTitleName(Constants::RosTitleName.Get().c_str());
		rgscTitleId.SetTitleDirectoryName(Constants::RosTitleDirectory.Get().c_str());
		
#if !RSG_FINAL
		u32 processInstance = 0;
		if(Globals::processinstance)
		{
			processInstance = Globals::processinstance.GetIntValue();
			if(processInstance > 0)
			{
				wchar_t rootDir[MAX_PATH] = {0};
				if(SHGetFolderPathW(NULL, CSIDL_MYDOCUMENTS, NULL, SHGFP_TYPE_CURRENT, rootDir) == S_OK)
				{
					wchar_t rsgPath[MAX_PATH] = {0};
					swprintf(rsgPath, MAX_PATH, L"\\Rockstar Games %u\\", processInstance);
					wcscat(rootDir, rsgPath);

					char fileNameA[MAX_PATH] = {0};
					WideCharToMultiByte(CP_UTF8, 0, rootDir, -1, fileNameA, MAX_PATH, NULL, NULL);
					rgscTitleId.SetRootDataDirectory(fileNameA);
				}
			}
		}
#endif

#if RSG_STEAM_LAUNCHER
		rgscTitleId.SetSteamAppId(CSteam::GetSteamAppId());
		rgscTitleId.SetSteamAuthTicket(CSteam::GetSteamAuthTicket());
		rgscTitleId.SetSteamId(CSteam::GetSteamId());
		rgscTitleId.SetSteamPersona(CSteam::GetSteamPersonaName());
#endif

		DEBUGF3("Beginning decryption of ROS Secrets");
		//@@: location SOCIALCLUBDLL_INITDLL_SETROSTITLESECRETS

		std::string out_plaintext;
		rgscTitleId.SetRosTitleSecrets(CUtil::Decrypt(rosTitleSecrets, sizeof(rosTitleSecrets), out_plaintext).c_str());
		DEBUGF3("Finished decryption of ROS Secrets");

		static u8 publicRsaKey[140] = {
			0x30, 0x81, 0x89,       //SEQUENCE (0x89 bytes = 137 bytes)
			0x02, 0x81, 0x81,   //INTEGER (0x81 bytes = 129 bytes)
			0x00,           //Leading zero of unsigned 1024 bit modulus
			0xab, 0xc1, 0x95, 0x54, 0x2b, 0x9c, 0x48, 0x52, 0x36, 0x71, 0xc9, 0x15, 0x2b, 0x09, 0xb8, 0x59, 
			0xd0, 0x16, 0xd5, 0xa4, 0x2a, 0x7a, 0x84, 0xfc, 0x55, 0xf6, 0x5d, 0x79, 0x2b, 0xcd, 0x81, 0x78, 
			0xa3, 0x2b, 0x02, 0x7d, 0x7f, 0xfc, 0x34, 0xee, 0x4f, 0x18, 0x73, 0xf5, 0xde, 0xc1, 0x22, 0xc7, 
			0xfc, 0xc4, 0x2b, 0xfe, 0xaa, 0x8d, 0xc8, 0x05, 0xcc, 0x40, 0x97, 0xcf, 0xea, 0x0a, 0x5a, 0x42, 
			0xb0, 0x24, 0xb7, 0xe6, 0x17, 0x6c, 0x9f, 0x1c, 0xbe, 0x17, 0xa7, 0x51, 0xb8, 0xf5, 0xda, 0x9b, 
			0xef, 0x25, 0x1a, 0xe0, 0xe1, 0x1b, 0x8e, 0x80, 0x12, 0x5b, 0x52, 0x3e, 0x49, 0x5b, 0xd5, 0xf5, 
			0xbb, 0x5b, 0x0e, 0xb0, 0x6c, 0x7d, 0x35, 0x02, 0x22, 0x32, 0xc9, 0xcf, 0x80, 0xa4, 0x94, 0x4c, 
			0x12, 0x26, 0x40, 0x0b, 0xda, 0x81, 0xdd, 0x6e, 0x65, 0xd9, 0x3d, 0xc4, 0x44, 0x6b, 0x42, 0x17,
			0x02, 0x03,         //INTEGER (0x03 bytes = 3 bytes)
			0x01, 0x00, 0x01 //Public exponent, 65537
		};


		rgscTitleId.SetPublicRsaKey(publicRsaKey, sizeof(publicRsaKey));

		//@@: } SOCIALCLUB_INITDLL_SET_TITLE_QUALIFIERS

#if !RSG_FINAL || RSG_FINAL_DEBUG
		// TODO HACK: Take this out!
		static char* params[] = {"rgsc", "-all_tty=debug3"};
#endif //FINAL

		//@@: range SOCIALCLUB_INITDLL_SET_CONFIG_QUALIFIERS {
		rgsc::Configuration config;
		//@@: location SOCIALCLUBDLL_INITDLL_SET_CONFIG_QUALIFIERS_LOCATION

		//TODO: This needs to be provided from Social Club DLL, not the other way around

		if (Globals::scofflineonly)
		{
			DISPLAYF("Setting Social Club 'Offline Only' mode from the command line.");
		}

#if RSG_STEAM_LAUNCHER
		config.SetOfflineOnlyMode(CSteam::IsOfflineMode() || Globals::scofflineonly);
#else
		config.SetOfflineOnlyMode(Globals::scofflineonly);
#endif
		config.SetPatchingEnabled(false);
		//@@: location SOCIALCLUB_INITDLL_SET_LOCAL_PROFILES
		config.SetLocalProfilesEnabled(false);
		config.SetIsLauncher(true);
		config.SetGamepadSupport(rgsc::IConfigurationV6::GAMEPADS_MARSHALLED);
#if !RSG_FINAL
		config.SetHandleAutoSignIn(Globals::noAutoSignIn || Globals::autotest_launcher_selfsignin ? false : true);
#else
		config.SetHandleAutoSignIn(Globals::noAutoSignIn ? false : true);
#endif
		//@@: } SOCIALCLUB_INITDLL_SET_CONFIG_QUALIFIERS


		//@@: location SOCIALCLUB_INITDLL_GETCOMMANDLINE
		m_Argvw = CommandLineToArgvW(GetCommandLineW(), &argc);

		if (m_Argvw && argc)
		{
			for (int i = 0; i < argc; i++)
			{
				std::string s;
				CUtil::WStringToStdString(std::wstring(m_Argvw[i]), s);
				m_Argvmb.push_back(CUtil::NewCString(s));

				// Ensure that the SCUI path is the second argument (after image name) so it can be overridden
				if (i == 0 && Constants::EnableSCUIv2ByDefault)
				{
					m_Argvmb.push_back(CUtil::NewCString("-scuiurlpath=/scui/v2/desktop"));
				}
			}
			config.SetCommandLineParams(argc+1, &m_Argvmb[0]);
		}
#if !RSG_FINAL
		else
		{
			config.SetCommandLineParams(sizeof(params)/sizeof(char*), params);
		}
#endif

		//@@: range SOCIALCLUB_INITDLL_SETUP_RGSCUI {

		RAD_TELEMETRY_LEAVE();
		RAD_TELEMETRY_ENTER("Init");

		DEBUGF3("Initialising RGSC...");
		rcheck(m_rgsc->Init(&config, &rgscTitleId, rgscLang, this) == S_OK,catchall, m_InitializationErrorCode = SC_INIT_ERR_INIT_CALL_FAILED);

		DEBUGF3("Setting up SCUI...");

		RAD_TELEMETRY_LEAVE();
		RAD_TELEMETRY_ENTER("Setup SCUI");

		rgsc::IRgscUi* ui = m_rgsc->GetUiInterface();
		//@@: location SOCIALCLUB_INITDLL_SETUP_SCUI
		rcheck(ui->QueryInterface(rgsc::IID_IRgscUiV3, (void**)&m_rgscUI) == S_OK, catchall, m_InitializationErrorCode = SC_INIT_ERR_QUERY_INTERFACE_FAILED);

		//@@: } SOCIALCLUB_INITDLL_SETUP_RGSCUI


		//@@: range SOCIALCLUB_INITDLL_GET_ACTIVATION_SYSTEM {

		DEBUGF3("Getting activation system...");
		rgsc::IActivation* activationSystem = m_rgscUI->GetActivationSystem();
		rcheck(activationSystem, catchall, m_InitializationErrorCode = SC_INIT_ERR_GET_SUBSYSTEM_FAILED);


		rcheck(activationSystem->QueryInterface(rgsc::IID_IActivationV2, (void**) &m_ActivationSystem) == S_OK, catchall, m_InitializationErrorCode = SC_INIT_ERR_QUERY_INTERFACE_FAILED);

		rgsc::ITaskManager* taskManager = m_rgsc->GetTaskManager();
		rcheck(taskManager, catchall, m_InitializationErrorCode = SC_INIT_ERR_GET_SUBSYSTEM_FAILED);
		rverify(m_TaskManager == NULL, catchall, m_InitializationErrorCode = SC_INIT_ERR_QUERY_INTERFACE_FAILED);

		// if this fails, then the DLL on the local computer doesn't match the headers
		//@@: location SOCIALCLUB_INITDLL_QUERY_TASK_MAN
		hr = taskManager->QueryInterface(rgsc::IID_ITaskManagerV1, (void**) &m_TaskManager);
		rverify((hr == S_OK) && m_TaskManager, catchall, m_InitializationErrorCode = SC_INIT_ERR_QUERY_INTERFACE_FAILED);

		// Get commerce manager

		rgsc::ICommerceManager* commerce = m_rgsc->GetCommerceManager();
		rcheck(commerce, catchall, m_InitializationErrorCode = SC_INIT_ERR_GET_SUBSYSTEM_FAILED);
		//@@: location SOCIALCLUB_INITDLL_GET_COMMERCE_MANAGER
		rcheck(commerce->QueryInterface(rgsc::IID_ICommerceManagerV4, (void**) &m_CommerceManager) == S_OK, catchall, m_InitializationErrorCode = SC_INIT_ERR_QUERY_INTERFACE_FAILED);

#if RSG_STEAM_LAUNCHER
		m_CommerceManager->SetSteamStoreOpenCallback(CSteam::SteamStoreOpenCallback);
#endif

		// Get telemetry

		rgsc::ITelemetry* telemetry = m_rgsc->GetTelemetry();
		rcheck(telemetry, catchall, m_InitializationErrorCode = SC_INIT_ERR_GET_SUBSYSTEM_FAILED);
		rcheck(telemetry->QueryInterface(rgsc::IID_ITelemetryV4, (void**) &m_Telemetry) == S_OK, catchall, m_InitializationErrorCode = SC_INIT_ERR_QUERY_INTERFACE_FAILED);
		m_Telemetry->SetGameHeaderInfoCallback(&GetHeaderInfo);

		// Get cloud save manager
		rgsc::ICloudSaveManager* cloudSaveManager = m_rgsc->GetCloudSaveManager();
		rcheck(cloudSaveManager, catchall, m_InitializationErrorCode = SC_INIT_ERR_GET_SUBSYSTEM_FAILED);
		rcheck(cloudSaveManager->QueryInterface(rgsc::IID_ICloudSaveManagerV4, (void**) &m_CloudSaveManager) == S_OK, catchall, m_InitializationErrorCode = SC_INIT_ERR_QUERY_INTERFACE_FAILED);

		// Get filesystem
		rgsc::IFileSystem* fileSystem = m_rgsc->GetFileSystem();
		rcheck(fileSystem, catchall, m_InitializationErrorCode = SC_INIT_ERR_GET_SUBSYSTEM_FAILED);
		rcheck(fileSystem->QueryInterface(rgsc::IID_IFileSystemV3, (void**) &m_FileSystem) == S_OK, catchall, m_InitializationErrorCode = SC_INIT_ERR_QUERY_INTERFACE_FAILED);		
		
		// Get gamepad manager
		rgsc::IGamepadManager* gamePadManager = m_rgsc->GetGamepadMgr();
		rcheck(gamePadManager, catchall, m_InitializationErrorCode = SC_INIT_ERR_GET_SUBSYSTEM_FAILED);
		rcheck(gamePadManager->QueryInterface(rgsc::IID_IGamepadManagerV3, (void**) &m_GamepadManager) == S_OK, catchall, m_InitializationErrorCode = SC_INIT_ERR_QUERY_INTERFACE_FAILED);

		// setup marshaled gamepads
		s_NumScuiPads = 0;
		for (u32 i = 0; i < rgsc::RGSC_MAX_PADS; i++)
		{
			s_ScuiPads[s_NumScuiPads] = new rgsc::RgscGamepad();
			if (s_ScuiPads[s_NumScuiPads])
			{
				s_ScuiPads[s_NumScuiPads]->ClearInputs();
				s_ScuiPads[s_NumScuiPads]->SetPadIndex(i);
				s_NumScuiPads++;
			}
		}

		m_GamepadManager->SetMarshalledGamepads((rgsc::IGamepad**)&s_ScuiPads[0], s_NumScuiPads);

		// Initialize CloudSaveManager
		CloudSaveManager::Initialize(this);

		m_rgscUI->SetUiScale(CLauncherDlg::GetLastInstance()->GetUiScale());

		RAD_TELEMETRY_LEAVE();
		RAD_TELEMETRY_ENTER("OnCreateDevice");

		DEBUGF3("Notifying SCUI of OnCreateDevice...");
		rcheck(m_rgscUI->OnCreateDevice(m_surface->GetDxContext(), m_surface->GetPresentParams()) == S_OK, catchall, m_InitializationErrorCode = SC_INIT_ERR_CREATE_DEVICE_DX9);

		m_bInitialized = true;

		//@@: } SOCIALCLUB_INITDLL_GET_ACTIVATION_SYSTEM
	}
	rcatchall
	{
		CError::SetError(ERROR_CATEGORY_SOCIAL_CLUB, m_InitializationErrorCode);
		PostMessage(m_hParentWnd, CLauncherDlg::eSetState, CFsm::eSocialClubError, 0);
		retval = false;
	} 

	RAD_TELEMETRY_LEAVE();

	//@@: location SOCIALCLUB_INITDLL_CHECK_ARGV

	return retval;
}

void SocialClub::OnPaint()
{
	if (m_rgscUI && m_rgscUI->IsReadyToAcceptCommands())
	{
		m_rgscUI->Render();
	}
}

void SocialClub::OnLostDevice()
{
	if (m_rgscUI)
	{
		m_rgscUI->OnLostDevice();
	}
}

void SocialClub::OnResetDevice()
{
	if (m_rgscUI && m_surface)
	{
		m_rgscUI->OnResetDevice(m_surface->GetPresentParams());
	}
}

rgsc::RGSC_HRESULT RGSC_CALL SocialClub::QueryInterface(rgsc::RGSC_REFIID riid, void** ppvObject)
{
	//@@: range SOCIALCLUB_QUERYINTERFACE {
	if (!ppvObject)
		return rgsc::RGSC_INVALIDARG;

	if (riid == rgsc::IID_IRgscUnknown)
	{
		*ppvObject = static_cast<IRgscDelegate*>(this);
	}
	else if (riid == rgsc::IID_IRgscDelegateV1)
	{
		*ppvObject = static_cast<IRgscDelegateV1*>(this);
	}
	else if (riid == rgsc::IID_IRgscDelegateV2)
	{
		*ppvObject = static_cast<IRgscDelegateV2*>(this);
	}
	else if (riid == rgsc::IID_IRgscDelegateV3)
	{
		//@@: location SOCIALCLUB_QUERYINTERFACE_DELEGATE3
		*ppvObject = static_cast<IRgscDelegateV3*>(this);
	}
	else
		return rgsc::RGSC_NOINTERFACE;

	return rgsc::RGSC_OK;
	//@@: } SOCIALCLUB_QUERYINTERFACE

}


bool SocialClub::PrePopulateKey()
{
	bool result = false;

#if RSG_STEAM_LAUNCHER

	HKEY	hKey = NULL;
	DWORD	dwType = 0;
	DWORD	dwSize = MAX_PATH;
	LRESULT lResult;

	// Location of the registry key where Steam will be putting the serial number.
	char *wSteamKey = "Software\\Rockstar Games";

	// Open the registry key.
	lResult = RegOpenKeyExA(HKEY_CURRENT_USER, wSteamKey, 0, KEY_READ, &hKey);

	// Make sure the key exists.
	if (lResult == ERROR_SUCCESS)
	{
		// Make sure the "CDKey" key value exists, otherwise return an empty serial number.
		lResult = RegQueryValueExA(hKey, "Grand Theft Auto V", 0, &dwType, (BYTE*)sm_PrePopulateKey, &dwSize);
		if (lResult == ERROR_SUCCESS && dwSize < MAX_PATH)
		{
			sm_PrePopulateKey[dwSize] = '\0';
			RegCloseKey(hKey);
			result = true;
		}
		else
		{
			RegCloseKey(hKey);
			result = false;
		}
	}
	// If the key doesn't even exist then return an empty serial.
	else
	{
		result = true;
	}

	DEBUGF1("PrePopulateKey %s: %s", result ? "success" : "failed", sm_PrePopulateKey);
#endif

	return result;
}

void SocialClub::Update()
{
	sm_EntitlementManager.Update();
#if RSG_STEAM_LAUNCHER
	CSteam::Update();
#endif
	ProcessGameEvents();

	//@@: range SOCIALCLUB_UPDATE_SHOW_ACTIVATION_UI {
	if (m_ActivationSystem && sm_bShowActivationUi)
	{
		DEBUGF1("ShowActivationCodeUi called");
		m_ActivationSystem->ShowActivationCodeUi(MAIN_LICENSE_ID, sm_PrePopulateKey, false, OnBladeUIActivationCodeSubmit);
		sm_bShowActivationUi = false;
	}
	//@@: } SOCIALCLUB_UPDATE_SHOW_ACTIVATION_UI

	if (m_profileManager->IsOnline() && m_profileManager->IsSignedIn() && m_Telemetry->IsAcceptingWrites())
	{
		RgscTelemetryManager::TransmitXmlEvents(m_Telemetry);
	}

	CloudSaveManager* csm = CloudSaveManager::Instance();
	if (csm)
	{
		csm->ScUpdate();
	}

	//@@: range SOCIALCLUB_UPDATE_HANDLE_SIGN_IN {
	if (m_rgscUI)
	{
		if (m_MessagesForScui.size() > 0)
		{
			if (!m_rgscUI->IsVisible())
			{
				DEBUGF3("UI is not visible, but we're trying to update the SCUI version.");
				//m_rgscUI->ShowUi();
			}
			else
			{
				EnterCriticalSection(&m_CriticalSection);

				if (!m_MessagesForScui.empty())
				{
					if (IsReadyForScuiMessages())
					{
						for (size_t i = 0; i < m_MessagesForScui.size(); i++)
						{
							DEBUGF1("Posting SCUI message: %s", m_MessagesForScui[i].c_str());
							m_rgscUI->SendMessageToScui(m_MessagesForScui[i].c_str());
						}
					}
					else
					{
						for (size_t i = 0; i < m_MessagesForScui.size(); i++)
						{
							DEBUGF1("Ignoring SCUI message we're not ready for: %s", m_MessagesForScui[i].c_str());
						}
					}

					m_MessagesForScui.clear();
				}

				LeaveCriticalSection(&m_CriticalSection);
			}
		}


		bool bUIShowing = m_rgscUI->IsVisible();
		if (!bUIShowing && m_bWasUIShowing)
		{
			DEBUGF1("SCUI is now hidden.");
			OnUIHidden();
		}
		else if (bUIShowing && !m_bWasUIShowing)
		{
			DEBUGF1("SCUI is now visible.");
			OnUIShown();
		}

		bool bSignedIn = m_profileManager->IsSignedIn();
		if (!bSignedIn && m_bWasSignedIn)
		{
			OnSignOut();
		}
		else if (bSignedIn && !m_bWasSignedIn)
		{
			OnSignIn();
		}

		m_bWasUIShowing = bUIShowing;
		m_bWasSignedIn = bSignedIn;

#if RSG_STEAM_LAUNCHER
		if (CSteam::IsKeyboardSuccessful())
		{
			m_rgscUI->SendVirtualKeyboardResult(CSteam::GetVirtualKeyboardResult());
		}
#endif
	}
	//@@: } SOCIALCLUB_UPDATE_HANDLE_SIGN_IN
}

#if !RSG_PRELOADER_LAUNCHER
void SocialClub::RunEntitlementCheck()
{
	//@@: range SOCIALCLUB_RUNENTITLEMENTCHECK {
	sm_EntitlementManager.RunEntitlementCheck();
	//@@: } SOCIALCLUB_RUNENTITLEMENTCHECK

}
#endif
void SocialClub::OnUIShown()
{
	sm_EntitlementManager.OnUIShown();
}

void SocialClub::OnUIHidden()
{
	sm_EntitlementManager.OnUIHidden();
}

void SocialClub::OnSignIn()
{
	//@@: range SOCIALCLUB_ONSIGNIN {
	sm_EntitlementManager.OnSignIn();

	rgsc::RockstarId rockstarId = GetRockstarId();
	CloudSaveManager::Instance()->OnSignIn(rockstarId);
	//@@: } SOCIALCLUB_ONSIGNIN

}

void SocialClub::OnSignOut()
{
	//@@: range SOCIALCLUB_ONSIGNOUT {
	sm_EntitlementManager.OnSignOut();
	CloudSaveManager::Instance()->OnSignOut();

	if (CLauncherDlg::GetLastInstance()->GetCurrentState() == CFsm::eCheckForEntitlement)
	{
		ShowSigninUI();
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eSignInAgain);
	}
#if !RSG_PRELOADER_LAUNCHER
	else if (CLauncherDlg::GetLastInstance()->GetCurrentState() != CFsm::eLoading)
#else
	else
#endif
	{
		m_bWaitingForSignIn = true;
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eSignInAgain);
	}
	//@@: } SOCIALCLUB_ONSIGNOUT

}

bool SocialClub::IsOnline()
{
	if (m_profileManager)
	{
		return m_profileManager->IsOnline();
	}

	return false;
}

void SocialClub::OnBladeUIActivationCodeSubmit( const int contentId, const char* /*activationCode*/, const rgsc::IActivationLatestVersion::ActivationAction action )
{
	//@@: range SOCIALCLUB_ONBLADEUIACTIVATIONCODESUBMIT_ACTIVATION_ATTEMPT {
	if (action == rgsc::IActivationLatestVersion::ACTIVATION_USER_QUIT)
	{
		sm_EntitlementManager.ActivationQuit();
	}
	//@@: } SOCIALCLUB_ONBLADEUIACTIVATIONCODESUBMIT_ACTIVATION_ATTEMPT

	sm_CurrentContentId = contentId;
}

void SocialClub::ShowCodeRedemptionUI()
{
	sm_bShowActivationUi = true;
	m_bWaitingForActivation = true;
	m_bWaitingForTicketRefresh = true;
}

void SocialClub::ShowSigninUI()
{
	DEBUGF1("Showing sign-in UI.");
	//@@: location SOCIALCLUB_SHOWSIGNINUI
	m_rgscUI->ShowSignInUi();
	m_bWaitingForSignIn = true;
}

enum StringId
{
	STRINGID_ACTIVATION_ACTIVATION_SUCCESS_MSG = 21,
	STRINGID_ACTIVATION_ACTIVATE_GENERIC_ERROR = 26,
	STRINGID_ACTIVATION_IN_PROGRESS_MSG = 31,
	STRINGID_ACTIVATION_CONNECTION_FAILURE_ERROR = 32,
	STRINGID_ACTIVATION_INVALID_SERIAL_ERROR = 33,
	STRINGID_ACTIVATION_QUIT_BUTTON = 34,
	STRINGID_ACTIVATION_ALREADY_ACTIVATED = 38,
};

void SocialClub::ShowActivationSuccess(int contentId)
{
	//@@: range SOCIALCLUB_SHOWACTIVATIONSUCCESS {
	m_ActivationSystem->ActivationCodeResult(contentId, true, STRINGID_ACTIVATION_ACTIVATION_SUCCESS_MSG);
	//@@: } SOCIALCLUB_SHOWACTIVATIONSUCCESS

}

void SocialClub::ShowActivationError(int errorCode)
{
	//@@: range SOCIALCLUB_SHOWACTIVATIONERROR {
	switch (errorCode)
	{
#if NEW_VOUCHER_FLOW_IN_SCDLL
	case rgsc::IVoucherV1::REDEMPTION_FAILURE_IN_USE:
	case rgsc::IVoucherV1::REDEMPTION_FAILURE_CONSUMPTION_LIMIT:
		m_ActivationSystem->ActivationCodeResult(sm_CurrentContentId, 0, STRINGID_ACTIVATION_ALREADY_ACTIVATED);
		break;
	case rgsc::IVoucherV1::REDEMPTION_FAILURE_DOES_NOT_EXIST:
		m_ActivationSystem->ActivationCodeResult(sm_CurrentContentId, 0, STRINGID_ACTIVATION_INVALID_SERIAL_ERROR);
		break;
	case rgsc::IVoucherV1::REDEMPTION_FAILURE_NO_NEW_CONTENT:
	case rgsc::IVoucherV1::REDEMPTION_FAILURE_NOT_ELIGIBLE:
#endif
	case 0:
	default:
		m_ActivationSystem->ActivationCodeResult(sm_CurrentContentId, 0, STRINGID_ACTIVATION_ACTIVATE_GENERIC_ERROR);
		break;
	}
	//@@: } SOCIALCLUB_SHOWACTIVATIONERROR
}

void SocialClub::SetSocialClubHotkey(bool bEnable)
{
	if (m_rgscUI)
	{
		m_rgscUI->EnableHotkey(bEnable);
	}
}

void SocialClub::OnExitError()
{
	if(m_rgsc)
	{
		u64 actionToTake = rgsc::OGC_UNADVERTISE_ALL_MATCHES | rgsc::OGC_PRESENCE_SIGNOUT;
		m_rgsc->OnGameCrashed(actionToTake);
	}
}

void SocialClub::AddGameEvent(GameEvent& e)
{
	AUTOCRITSEC(s_GameEventLock);
	m_GameEventQueue.push(e);
}

void SocialClub::ProcessGameEvents()
{
	AUTOCRITSEC(s_GameEventLock);
	while(!m_GameEventQueue.empty())
	{
		GameEvent e = m_GameEventQueue.front();
		switch(e.EventType)
		{
		case GameEvent::SIGNED_IN:
			if (m_profileManager)
			{
				DEBUGF1("Processing Game Sign In Event");
				m_profileManager->OnLauncherReceivedSignInEvent(e.RockstarId);
			}
			break;
		case GameEvent::SIGNED_OUT:
			if (m_profileManager)
			{
				DEBUGF1("Processing Game Sign Out Event");
				m_profileManager->OnLauncherReceivedSignOutEvent();
			}
			break;
		case GameEvent::TICKET_CHANGED:
			if (m_profileManager)
			{
				DEBUGF1("Processing Game Ticket Changed Event");
				m_profileManager->OnLauncherReceivedUpdatedSocialClubTicket(e.RockstarId, e.Ticket.c_str());
			}
			break;
		}
		m_GameEventQueue.pop();
	}
}

void SocialClub::UpdateSocialClubTicket(rgsc::RockstarId rockstarId, std::string base64Ticket)
{
	DEBUGF1("Queueing game ticket change Event");
	GameEvent e(GameEvent::TICKET_CHANGED, rockstarId, base64Ticket);
	AddGameEvent(e);
}

void SocialClub::GameSignOutEvent()
{
	DEBUGF1("Queueing game signout Event");
	GameEvent e(GameEvent::SIGNED_OUT, 0, "");
	AddGameEvent(e);
}

void SocialClub::GameSignInEvent(rgsc::RockstarId rockstarId)
{
	DEBUGF1("Queueing game signin Event");
	GameEvent e(GameEvent::SIGNED_IN, rockstarId, "");
	AddGameEvent(e);
}

void SocialClub::CreateSignInTransferFile()
{
	if (m_profileManager)
	{
		m_profileManager->CreateSigninTransferProfile();
	}
}

rgsc::RgscGamepad* SocialClub::GetScuiPad(unsigned index) const
{
	if (index < s_NumScuiPads)
	{
		return s_ScuiPads[index];
	}

	return NULL;
}

void SocialClub::SendMessageToScui(const char* message)
{
	EnterCriticalSection(&m_CriticalSection);

	m_MessagesForScui.push_back(std::string(message));

	LeaveCriticalSection(&m_CriticalSection);
}

bool SocialClub::IsReadyForScuiMessages()
{
	return m_bInitialized && m_rgscUI && m_rgscUI->IsReadyToAcceptCommands();
}

void SocialClub::RequestShutdown()
{
	if (threadRunning)
	{
		ending = true;
	}
}

void SocialClub::Join()
{
	if (m_pThread)
	{
		if (threadRunning)
		{
			ending = true;
			WaitForSingleObject(m_pThread->m_hThread, INFINITE);
		}
		delete m_pThread;
		m_pThread = NULL;
	}
}

#if !RSG_FINAL
void RGSC_CALL SocialClub::Output(OutputSeverity severity, const char* msg)
{
	switch (severity)
	{
	case RGSC_OUTPUT_SEVERITY_INFO:
		DEBUGF2("Info: %s", msg);
		break;
	case RGSC_OUTPUT_SEVERITY_WARNING:
		WARNINGF("Warning: %s", msg);
		break;
	case RGSC_OUTPUT_SEVERITY_ERROR:
		ERRORF("Error: %s", msg);
		break;
	case RGSC_OUTPUT_SEVERITY_ASSERT:
		{
			ERRORF("Assert: %s", msg);
			if (strlen(msg) > 2 && strncmp(msg, "\n\n", 2) == 0) msg += 2; // hack: remove leading \n\n from many scui asserts
			
			int msgboxID = MessageBoxA( NULL, msg, "SCUI Assert", MB_ICONWARNING | MB_ABORTRETRYIGNORE);
			switch (msgboxID)
			{
			case IDABORT:
				__debugbreak();
				break;
			case IDIGNORE:
				// TODO: add code
				break;

			}
		}
		break;
	default:
		DISPLAYF("Message: %s", msg);
		break;
	}
}
#else
void RGSC_CALL SocialClub::Output(OutputSeverity severity, const char* msg)
{
	switch (severity)
	{
	case RGSC_OUTPUT_SEVERITY_ERROR:
		ERRORF("Error: %s", msg);
		break;
	case RGSC_OUTPUT_SEVERITY_ASSERT:
		ERRORF("Assert: %s", msg);
		break;
	}
}
#endif

bool RGSC_CALL SocialClub::GetStatsData(char** /*data*/)
{
	DEBUGF1("GetStatsData() called");
	return false;
}

void RGSC_CALL SocialClub::FreeStatsData(const char* /*data*/)
{
	DEBUGF1("FreeStatsData() called");
}

void RGSC_CALL SocialClub::SetTextBoxHasFocus(const bool hasFocus)
{
	DEBUGF1("SetTextBoxHasFocus(%s) called", hasFocus ? "true" : "false");
	m_bTextboxHasFocus = hasFocus;
}

void RGSC_CALL SocialClub::SetTextBoxHasFocusV2(const bool hasFocus, const char* prompt, const char* text, const bool isPassword, const unsigned int maxNumChars)
{
	DEBUGF1("SetTextBoxHasFocusV2(%s, %s, %s, %s, %d chars) called", hasFocus ? "true" : "false", prompt, text, isPassword ? "true" : "false", maxNumChars);

	rtry
	{
		rcheck(hasFocus, catchall, );
		rcheck(m_rgscUI->GetInputMethod() == rgsc::RGSC_IM_CONTROLLER, catchall, );

#if RSG_STEAM_LAUNCHER
		rcheck(!CSteam::IsKeyboardPending(), catchall, );
		if (!CSteam::ShowKeyboard(prompt, text, isPassword, maxNumChars))
#endif
		{
			m_rgscUI->ShowVirtualKeyboard(text, isPassword, maxNumChars); // pass the text into the virtual keyboard
		}
	}
	rcatchall
	{
		// None of the rchecks above indicate an error,
		// but they're getting picked up by automated testing.
		// Signal that they're not a problem.
		AUTOTEST_RESULT(CTest::NOT_AN_ERROR);
	}

	m_bTextboxHasFocus = hasFocus; 
}

#if !RSG_FINAL
void RGSC_CALL SocialClub::UpdateSocialClubDll(const char* commandLine)
{
	DEBUGF1("UpdateSocialClubDll(%s) called", commandLine);
}
#else
void RGSC_CALL SocialClub::UpdateSocialClubDll(const char*) {}
#endif


void RGSC_CALL SocialClub::HandleNotification(const rgsc::NotificationType id, const void* param)
{
	//@@: range SOCIALCLUB_HANDLENOTIFICATION {
	if (id == rgsc::NOTIFY_SIGN_IN_STATE_CHANGED)
	{
		//@@: location SOCIALCLUB_HANDLENOTIFICATION_SIGN_IN_STATE_CHANGED
		rgsc::SignInStateFlags state = *(rgsc::SignInStateFlags*)param;
		if (state & rgsc::STATE_SIGNED_IN)
		{
			DEBUGF1("Signed-in; not immediately continuing, though.");

			if (m_bWaitingForSignIn)
			{
				//m_bWaitingForSignIn = false;
				//CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);
				m_bSignInNotificationEvent = true;
			}
		}

		if (state & rgsc::STATE_SIGNED_ONLINE)
		{
			DEBUGF1("Signed online.");
		}
		
		if (state & rgsc::STATE_SIGNED_OFFLINE)
		{
			DEBUGF1("Signed offline.");
		}
		
		if (state & rgsc::STATE_SIGNED_OUT)
		{
			DEBUGF1("Signed out.");

			if (m_Telemetry)
			{
				m_Telemetry->CancelFlushes();
			}
		}
	}
	else if (id == rgsc::NOTIFY_ROS_TICKET_CHANGED)
	{
		DEBUGF1("NOTIFY_ROS_TICKET_CHANGED");

		// reset banned status
		sm_bSocialClubBanned = false;
		sm_uSocialClubBanEndDate = 0;

		const char* xml = (const char*)param;
		if (xml)
		{
			TiXmlDocument doc;
			doc.Parse(xml);

			TiXmlHandle docHandle(&doc);

			TiXmlHandle response = docHandle.FirstChild("Response");
			TiXmlHandle priv = response.FirstChild("Privs");

			TiXmlElement* opt = priv.FirstChild().ToElement();
			for (; opt; opt = opt->NextSiblingElement())
			{
				bool privEnabled = false;
				int privId = -1;
				u64 endDate = 0;

				// get privilege ID
				const char* idstr = opt->Attribute("id", &privId);

				// get privilege enabled
				const char* gstr = opt->Attribute("g");
				if (!_stricmp(gstr, "True"))
				{
					privEnabled = true;
				}

				// get privilege end date
				const char* edstr = opt->Attribute("ed");
				if (sscanf_s(edstr, "%I64u", &endDate) != 1)
				{
					// failed to read or indefinite
					endDate = 0;
					DISPLAYF("Privilege %s - %s - permanent.", idstr, gstr);
				}
				else
				{
					DISPLAYF("Privilege %s - %s - expires %I64d.", idstr, gstr, endDate);
				}

				const int RLROS_PRIVILEGEID_BANNED = 7;    //Denies all other privileges except CREATE_TICKET
				if (privId == RLROS_PRIVILEGEID_BANNED && privEnabled)
				{
					sm_bSocialClubBanned = true;
					sm_uSocialClubBanEndDate = endDate;
				}
			}
		}

		if (CGameCommunication::Instance().IsListening())
		{	
			CGameCommunication::Instance().SendTicketUpdate(GetRockstarId(), xml);
		}

		if (m_bWaitingForTicketRefresh && !m_bWaitingForActivation)
		{
			DEBUGF1("Ticket refreshed.");
#if !RSG_PRELOADER_LAUNCHER
			sm_EntitlementManager.ActivationAttempt();
#else
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);
#endif
		}
		else
		{
			DEBUGF1("Waiting for activation.");
		}

		m_bWaitingForTicketRefresh = false;
	}
	else if (id == rgsc::NOTIFY_UI_EVENT)
	{
		//@@: location SOCIALCLUB_HANDLENOTIFICATION_UI_EVENT
		JsonReader data = JsonReader((const char*)param).GetChild("Data");
		JsonReader action = data.GetChild("Action");
		JsonReader parameter = data.GetChild("Parameter");

		if (action.StringValueEquals("Minimize"))
		{
			//@@: location SOCIALCLUB_HANDLENOTIFICATION_MINIMIZE_EVENT
			DEBUGF1("Minimize action.");
			CLauncherDlg::PostMessageSafe(WM_SYSCOMMAND, SC_MINIMIZE, 0);
		}
		else if (action.StringValueEquals("Close"))
		{
			//@@: location SOCIALCLUB_HANDLENOTIFICATION_CLOSE_EVENT
			DEBUGF1("Close action.");
			CLauncherDlg::PostMessageSafe(WM_CLOSE);
		}
		else if (action.StringValueEquals("LauncherButton"))
		{
			std::string btnString;
			if (parameter.GetUnquotedValue(btnString))
			{
				int index = std::stoi(btnString);

				DEBUGF1("Button pressed: %d", index);
				switch (index)
				{
				case 0: CLauncherDlg::PostMessageSafe(WM_COMMAND, MAKEWPARAM(IDC_RIGHTBUTTON, BN_CLICKED)); break;
				case 1: CLauncherDlg::PostMessageSafe(WM_COMMAND, MAKEWPARAM(IDC_LEFTBUTTON, BN_CLICKED)); break;
				case 2: CLauncherDlg::PostMessageSafe(WM_COMMAND, MAKEWPARAM(IDC_MIDDLEBUTTON, BN_CLICKED)); break;
				default:
					ERRORF("Unknown button pressed!");
					break;
				}

			}
			else
			{
				DEBUGF1("Invalid button?");
			}
		}
		else if (action.StringValueEquals("SignIn"))
		{
			DEBUGF1("NOTIFY_UI_EVENT: Signed in.");

			m_bSignInScuiEvent = true;
		}
		else if (action.StringValueEquals("Activate"))
		{
			DEBUGF1("NOTIFY_UI_EVENT: Activated.");

			if (m_bWaitingForActivation && !m_bWaitingForTicketRefresh)
			{
#if !RSG_PRELOADER_LAUNCHER
				sm_EntitlementManager.ActivationAttempt();
#else
				CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);
#endif
			}
			else
			{
				DEBUGF1("Waiting for ticket refresh.");
			}

			m_bWaitingForActivation = false;
		}
#if !RSG_FINAL || RSG_FINAL_DEBUG
		else if (action.StringValueEquals("Debug"))
		{
			std::string paramStr;
			if (parameter.GetStringValue(paramStr))
			{
				DEBUGF1("SCUIv2: %s", paramStr.c_str());
			}
		}
#endif
		else if (action.StringValueEquals("Geo"))
		{
			std::string countryCode;
			if (parameter.GetStringValue(countryCode))
			{
				DISPLAYF("Geo: %s", countryCode.c_str());

				RgscTelemetryManager::SetCountryCode(countryCode);
			}
		}
		else if (action.StringValueEquals("ExternIP"))
		{
			std::string ipAddress;
			if (parameter.GetStringValue(ipAddress))
			{
				DEBUGF1("IP: %s", ipAddress.c_str());

				RgscTelemetryManager::SetIpAddress(ipAddress);
			}
		}
#if ENABLE_SP_CLOUD_SAVE
		else if (action.StringValueEquals("CloudState"))
		{
			std::string state;
			if (parameter.GetChild("state").GetUnquotedValue(state))
			{
				if (state == "true")
				{
					CloudSaveManager::Instance()->OnEnabledCheck(true);
				}
				else
				{
					CloudSaveManager::Instance()->OnEnabledCheck(false);
				}
			}
		}
		else if (action.StringValueEquals("ResolveCloudConflict"))
		{
			CloudSaveManager::Instance()->OnConflictResolutionsReceived(parameter);
		}
		else if (action.StringValueEquals("ResolveLocalBackup"))
		{
			CloudSaveManager::Instance()->OnBackupResolutionsReceived(parameter);
		}
#endif
		else
		{
			DEBUGF1("Unknown UI Event: %s", (const char*)param);
		}
	}
#if RSG_STEAM_LAUNCHER
	else if (id == rgsc::NOTIFY_REFRESH_STEAM_AUTH_TICKET)
	{
		CSteam::RefreshSteamAuthTicket();
		m_rgsc->SetSteamAuthTicket(CSteam::GetSteamAuthTicket());
	}
#endif
	//
	else if (id == rgsc::NOTIFY_FATAL_ERROR)
	{
		OUTPUT_ONLY(int errCode = *(int*)param;)
		DEBUGF1("NOTIFY FATAL ERROR: %d", errCode);
	}
#if RGSC_SDK_VERSION >= (1176)
	else if (id == rgsc::NOTIFY_CLOUD_SAVE_ENABLED_UPDATED)
	{
		CloudSaveManager::Instance()->OnCloudSaveEnabledUpdated((const char*)param);
		DEBUGF1("Cloud Save Enabled Updated for title: %s", (const char*)param);
	}
#endif
	else
	{
		DEBUGF1("HandleNotification(%d, %p)", (int)id, param);
	}
	//@@: } SOCIALCLUB_HANDLENOTIFICATION

}

rgsc::RockstarId SocialClub::GetRockstarId()
{

	if (m_profileManager)
	{
		//@@: range SOCIALCLUB_GETROCKSTARID {
		rgsc::Profile profile;
		//@@: location SOCIALCLUB_GETROCKSTARID_GETSIGNININFO
		m_profileManager->GetSignInInfo(&profile);
		return profile.GetProfileId();
		//@@: } SOCIALCLUB_GETROCKSTARID

	}
	else
	{
		return 0;
	}
}

rgsc::ITaskManagerLatestVersion* SocialClub::GetTaskManager()
{
	return m_TaskManager;
}

rgsc::IActivationLatestVersion* SocialClub::GetActivationSystem()
{
	return m_ActivationSystem;
}

rgsc::ICommerceManagerV4* SocialClub::GetCommerceManager()
{
	return m_CommerceManager;
}

EntitlementManager* SocialClub::GetEntitlementManager()
{
	return &sm_EntitlementManager;
}

rgsc::IProfileManagerV2* SocialClub::GetProfileManager()
{
	return m_profileManager;
}

rgsc::ITelemetryV4* SocialClub::GetTelemetryManager()
{
	return m_Telemetry;
}

rgsc::ICloudSaveManagerV4* SocialClub::GetCloudSaveManager()
{
	return m_CloudSaveManager;
}

rgsc::IFileSystemV3* SocialClub::GetFileSystem()
{
	return m_FileSystem;
}

rgsc::IGamepadManagerV3* SocialClub::GetGamepadManager()
{
	return m_GamepadManager;
}

void SocialClub::SetGameVersionHeaderInfo(u32 version)
{
	m_headerInfo.clear();
	RgscTelemetryManager::AppendJsonInt(m_headerInfo, "ver", version);
}

const char* SocialClub::GetHeaderInfo()
{
	if (m_headerInfo.empty())
	{
		return NULL;
	}
	else
	{
		return m_headerInfo.c_str();
	}
}
