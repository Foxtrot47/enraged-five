#pragma once

#include "../tinyxml/tinyxml.h"

#include <vector>
#include <string>

class RgscTelemetryEvent
{
public:

	enum MetricName
	{
		MN_LAUNCHER_METRIC,
		MN_DEBUG_METRIC,
	};

	RgscTelemetryEvent(MetricName metricName);
	RgscTelemetryEvent(const TiXmlElement* copyFrom);
	~RgscTelemetryEvent();

	enum FieldName
	{
		FN_EVENT_TYPE,
		FN_EVENT_TIMESTAMP,

		FN_EVENT_PARAMETER,
		FN_VERSION_INFO,

		FN_DOWNLOAD_TYPE,
		FN_BYTES_DOWNLOADED,
		FN_DURATION,
		FN_CONNECTIONS_OPENED,
		FN_CONNECTIONS_ERRORED,
		FN_VERIFICATIONS_FAILED,

		FN_COUNTRY_CODE,
		FN_IP_ADDRESS,

		FN_DEBUG_ARRAY,
		FN_DEBUG_KEY,
		FN_DEBUG_VALUE,

		FN_FILENAME,
	};

	void AddIntField(FieldName fieldName, u64 value);
	void AddStringField(FieldName fieldName, const char* value);
	void AddArrayField(FieldName fieldName, const std::vector<const RgscTelemetryEvent*>& subEvents);

	void AppendToXml(TiXmlElement* appendTo) const;

	bool WriteAsJson(std::string& out_metricName, std::string& out_json) const;

	bool GetFieldAsString(FieldName fieldName, std::string& out_string);

	static const char* GetMetricXmlTagName();

private:
	enum XmlStringName
	{
		XSN_METRIC_ROOT_TAG,
		XSN_METRIC_NAME_ATTR,
		XSN_FIELD_CONTAINER_TAG,
		XSN_FIELD_TAG,
		XSN_FIELD_NAME_ATTR,
		XSN_FIELD_TYPE_ATTR,
	};

	enum FieldTypeName
	{
		FTN_INT = 0,
		FTN_STRING,
		FTN_ARRAY,

		FTN_MAX,
	};

	static const char* GetMetricNameFromId(MetricName id);
	static const char* GetFieldNameFromId(FieldName id);
	static const char* GetXmlStringNameFromId(XmlStringName id);
	static const char* GetFieldTypeNameFromId(FieldTypeName id);

	static bool ParseFieldTypeName(const char* fieldTypeString, FieldTypeName& out_fieldType);

	TiXmlElement* GetFieldsTag();
	const TiXmlElement* GetFieldsTag() const;
	TiXmlElement* CreateLinkedField(FieldName fieldName, FieldTypeName typeName);

	static void WriteFieldContainerAsJson(std::string& out_json, const TiXmlElement* fieldContainer, bool omitBraces = false);
	static void WriteFieldAsJson(std::string& out_json, const TiXmlElement* field, bool prependComma);

	TiXmlElement* m_root;
};
