#include "StdAfx.h"
#include "IPatchCheck.h"

#include "DownloadManager.h"
#include "RockstarDownloader.h"
#include "Application.h"
#include "Globals.h"
#include "Config.h"
#include "Channel.h"
#include "Util.h"
#include "RgscTelemetryManager.h"

#if RSG_LAUNCHER
#include "Entitlement.h"
#endif

#include <sstream>
#include "seh.h"

#include <iostream>
#include <fstream>

#include <atlbase.h>
#include <regex>


IPatchCheck::IPatchCheck(void)
{
	//@@: location IPATCHCHECK_CONSTRUCTOR
	m_bIsRunning = false;
	m_bCheckError = false;
	m_bUpToDate = false;
	m_bRedownload = false;
}

IPatchCheck::~IPatchCheck(void)
{
}

std::string IPatchCheck::getTextTo(const char** cursor, const char* terminator)
{
	const char* end = strstr(*cursor, terminator);
	if (!end || end == *cursor)
		return std::string();
	else
	{
		std::string ret (*cursor, end-(*cursor));
		(*cursor) = end;
		return ret;
	}
}

std::string IPatchCheck::getQuoted(const char** cursor)
{
	if (**cursor == '"')
		(*cursor)++;

	const char* end = strchr(*cursor, '"');
	if (!end)
		return std::string();
	else
	{
		std::string ret (*cursor, end-(*cursor));
		(*cursor) = end;
		return ret;
	}
}

std::string IPatchCheck::findAttributeValue(const char* cursor, const char* attrib)
{
	const char* ptr = strstr(cursor, attrib);

	if (!ptr)
		return std::string();
	else
		ptr += strlen(attrib);

	ptr = strchr(ptr, '"');
	if (!ptr)
		return std::string();

	return getQuoted(&ptr);
}

std::string IPatchCheck::findTagValue(const char* cursor, const char* tagname)
{
	std::stringstream openTag;
	std::stringstream closeTag;

	openTag << "<" << tagname << ">";
	closeTag << "</" << tagname << ">";

	const char* start = strstr(cursor, openTag.str().c_str());
	if (!start)
		return std::string();

	start += (s64)openTag.tellp();

	const char* end = strstr(start, closeTag.str().c_str());
	if (!end || end == start)
		return std::string();

	return std::string(start, end-start);
}

void IPatchCheck::CheckForPatches()
{
	//@@: location IPATCHCHECK_CHECKFORPATCHES_START_TELEMETRY_WRITES
	if (m_patchCheckType == PT_BOOSTRAP)
		RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_BOOTSTRAP_START_CHECK_FOR_UPDATES);
	else if (m_patchCheckType == PT_SCUI)
		RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_RGSC_CHECK_FOR_UPDATE);
	else if (m_patchCheckType == PT_GAME)
		RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_GAME_CHECK_FOR_UPDATE);

	//@@: range IPATCHCHECK_CHECKFORPATCHES_PARSE_XML_AND_CHECK {
	DEBUGF3("Parsing XML...");
	//@@: location IPATCHCHECK_CHECKFORPATCHES_CALL_PARSEXML
	ParseXML();
	DEBUGF3("Cleaning up XML...");
	//@@: location IPATCHCHECK_CHECKFORPATCHES_CLEANUPXML
	CleanupXML();
	DEBUGF3("Checking EXE...");
	//@@: location IPATCHCHECK_CHECKFORPATCHES_CHECK_EXECUTABLE_VERSION
	CheckExecutableVersion();
	DEBUGF3("Done.");

	//@@: } IPATCHCHECK_CHECKFORPATCHES_PARSE_XML_AND_CHECK

	m_bIsRunning = false; 

	if (m_bCheckError)
	{
		//@@: location IPATCHCHECK_CHECKFORPATCHES_GET_LAST_ERROR
		if (!CError::GetLastError().IsError())
		{
			CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_UNKNOWN);
		}
	}
	else
	{
		if (m_patchCheckType == PT_BOOSTRAP)
			RgscTelemetryManager::WriteEventInt(RgscTelemetryManager::EVENT_BOOTSTRAP_PATCH_TO_INSTALL, m_patches.empty() ? 0 : 1);
		else if (m_patchCheckType == PT_SCUI)
			RgscTelemetryManager::WriteEventInt(RgscTelemetryManager::EVENT_LAUNCHER_RGSC_PATCH_TO_INSTALL, m_patches.empty() ? 0 : 1);
		else if (m_patchCheckType == PT_GAME)
			RgscTelemetryManager::WriteEventInt(RgscTelemetryManager::EVENT_LAUNCHER_GAME_PATCH_TO_INSTALL, m_patches.empty() ? 0 : 1);
	}

}

void IPatchCheck::ConvertToNeedingAccessControlledUrl(Patch& patch)
{
	if (patch.NeedsAccessControlledUrl)
	{
		return;
	}

	patch.AccessControlKey = std::regex_replace(patch.PatchURL, std::regex("http://patches.rockstargames.com/[a-z]+/gtav/"), "");
	patch.NeedsAccessControlledUrl = true;
}

void IPatchCheck::Cancel()
{
	m_bIsRunning = false;

	if (m_bIsDownloading)
	{
		//@@: location IPATCHCHECK_CANCEL_STOP_DOWNLOAD
		Downloader::Instance().Stop();
	}
}

void IPatchCheck::CheckExecutableVersion()
{
	m_bUpToDate = true;
	Version currentVersion = GetExecutableVersion();

	DEBUGF3("Executable version %d.%d.%d.%d", currentVersion.major, currentVersion.minor, currentVersion.build, currentVersion.revision);

	if (m_patchCheckType == PT_BOOSTRAP)
		RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_BOOTSTRAP_START_CHECK_AGAINST_PATCH_LIST);
	else if (m_patchCheckType == PT_SCUI)
		RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_RGSC_CHECK_AGAINST_PATCH_LIST);
	else if (m_patchCheckType == PT_GAME)
		RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_GAME_CHECK_AGAINST_PATCH_LIST);

	//@@: location IPATCHCHECK_CHECKEXECUTABLEVERSION_CHECK_PATCH_SIZE
	if (m_patches.size() > 0)
	{
		DEBUGF3("Checking %d patches...", m_patches.size());

		//@@: range IPATCHCHECK_CHECKEXECUTABLEVERSION_ITERATE_PATCHES {
		for (u32 i = 0; i < m_patches.size(); i++)
		{
			if (
				m_patches[i]._Version > currentVersion 
#if !RSG_FINAL
				|| Globals::testFlow
#endif
			)
			{
				//@@: location IPATCHCHECK_CHECKEXECUTABLEVERSION_NEEDS_INSTALLING
				DEBUGF1("Patch %d needs installing.", i+1);
				m_patches[i].Installed = false;
				m_bUpToDate = false;
			}
			else
			{
				//@@: location IPATCHCHECK_CHECKEXECUTABLEVERSION_ALREADY_INSTALLED
				m_patches[i].Installed = true;

				DEBUGF1("Patch %d is already installed.", i+1);

				// Clear old files (i.e. RestartRequired files)
				if (CUtil::FileExists(m_patches[i].LocalFilename))
				{
					DEBUGF1("Deleting file: %s", m_patches[i].LocalFilename.c_str());
					CFileWrapper::remove(m_patches[i].LocalFilename.c_str());
				}
			}
		}
		//@@: } IPATCHCHECK_CHECKEXECUTABLEVERSION_ITERATE_PATCHES

	}
	else
	{
		DEBUGF2("No patches to check.");
	}
}

void IPatchCheck::OnAddPatch(Patch& patch)
{
	// Fix local filename
	if (patch.LocalFilename.size() > 0)
	{
		CUtil::PrependTempoaryFolderPath(patch.LocalFilename, patch.LocalFilename);
	}
}

Version IPatchCheck::GetVersionForFile(const std::string& path, bool* out_exists)
{
#if FORCE_UPDATE_ALL
	if (out_exists)
	{
		*out_exists = false;
	}
	return Version();
#else
	//@@: range IPATCHCHECK_GETVERSIONFORFILE {
	//@@: location IPATCHCHECK_GETVERSIONFORFILE_CALL_GET_FILE_VERSION_A
	DWORD infoSize = GetFileVersionInfoSizeA(path.c_str(), NULL);
	if (infoSize == 0)
	{
		DISPLAYF("Unable to get file version info size for %s", path.c_str());

		CUtil::PrintSystemErrorMessage(GetLastError());

		if (out_exists)
		{
			*out_exists = false;
		}
		return Version();
	}

	//@@: location IPATCHCHECK_GETVERSIONFORFILE_CALL_GET_FILE_VERSION_B
	char* info = new char[infoSize];
	if (!GetFileVersionInfoA(path.c_str(), NULL, infoSize, info))
	{
		DISPLAYF("Unable to get file version info for %s", path.c_str());
		delete[] info;

		if (out_exists)
		{
			*out_exists = false;
		}
		return Version();
	}

	//@@: location IPATCHCHECK_GETVERSIONFORFILE_CALL_QUERY_FILE_VERSION
	VS_FIXEDFILEINFO* fileInfo;
	UINT fileInfoLength;
	if (!VerQueryValueA(info, "\\", (void**)&fileInfo, &fileInfoLength))
	{
		DISPLAYF("Unable query file version info for %s", path.c_str());
		delete[] info;

		if (out_exists)
		{
			*out_exists = false;
		}
		return Version();
	}

	if (out_exists)
	{
		*out_exists = true;
	}

	DISPLAYF("File version %x:%x (for %s)", fileInfo->dwFileVersionMS, fileInfo->dwFileVersionLS, path.c_str());
	//@@: location IPATCHCHECK_GETVERSIONFORFILE_CALL_RETURN_FILE_VERSION
	return Version(HIWORD(fileInfo->dwFileVersionMS), LOWORD(fileInfo->dwFileVersionMS), HIWORD(fileInfo->dwFileVersionLS), LOWORD(fileInfo->dwFileVersionLS)); 
	//@@: } IPATCHCHECK_GETVERSIONFORFILE

#endif
}

std::string IPatchCheck::GetValueFromRegistry(const TCHAR* key32, const TCHAR* key64, const TCHAR* valueName)
{
	std::string path;

	CRegKey regKey;
	//@@: location IPATCHCHECK_GETVALUEFROMREGISTRY_OPEN_REG_KEY
	if (regKey.Open(HKEY_LOCAL_MACHINE, key64, KEY_READ) == ERROR_SUCCESS || regKey.Open(HKEY_LOCAL_MACHINE, key32, KEY_READ) == ERROR_SUCCESS)
	{
		TCHAR* tbuffer = new TCHAR[1024];
		ZeroMemory(tbuffer, 1024);
		
		ULONG len = 1024;
		//@@: location IPATCHCHECK_GETVALUEFROMREGISTRY_QUERY_VALUE
		if (regKey.QueryStringValue(valueName, tbuffer, &len) == ERROR_SUCCESS)
		{
			CUtil::WStringToStdString(tbuffer, path);
		}

		delete[] tbuffer;

		regKey.Close();
	}

	return path;
}

void IPatchCheck::CleanupXML()
{
	//@@: range IPATCHCHECK_CLEANUPXML_RANGE {
	//@@: location IPATCHCHECK_CLEANUPXML
	if (CUtil::FileExists(m_localXmlFile))
	{
		CFileWrapper::remove(m_localXmlFile.c_str());
	} 
	//@@: } IPATCHCHECK_CLEANUPXML_RANGE

}

std::string IPatchCheck::SafeAttribute(const TiXmlElement* element, const char* attrName)
{
	//@@: location IPATCHCHECK_SAFEATTRIBUTE
	const char* str = element->Attribute(attrName);
	if (str)
		return str;
	else
		return "";
}

void IPatchCheck::PopulatePatch(Patch& patch, const char* skuName, const TiXmlElement* releaseElement, const TiXmlElement* patchElement)
{
	//@@: range IPATCHCHECK_POPULATEPATCH {
	patch._Version = Version(SafeAttribute(releaseElement, "version"));

	patch.SkuName = skuName ? skuName : "(unknown SKU)";
	//@@: location IPATCHCHECK_POPULATEPATCH_FETCH_DESCRIPTION
	patch.Description = SafeAttribute(patchElement, "description");
	patch.PatchURL = SafeAttribute(patchElement, "patchFile");
	patch.DeleteOnCompletion = true;
	patch.ChunkedDownload = false;
	patch.HashHexDigits = SafeAttribute(patchElement, "SHA1Hash");
	patch.UpdateType = SafeAttribute(patchElement, "patchType").compare("mandatory") == 0 ? UT_MANDATORY : UT_OPTIONAL;
	patch.CommandlineArguments = SafeAttribute(patchElement, "commandlineArguments");

	CUtil::PrependTempoaryFolderPath(patch.LocalFilename, SafeAttribute(patchElement, "localFileName"));
	//@@: } IPATCHCHECK_POPULATEPATCH

}
