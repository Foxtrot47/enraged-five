#include "stdafx.h"

#include "Util.h"
#include "Config.h"
#include "Globals.h"
#include "OnlineConfig.h"
#define PRELOADED_STRIP RSG_BOOTSTRAP

#if !PRELOADED_STRIP
#include "TFIT_RSA_iRSA1024.h"
#include "TFIT_RSA_Launcher_public.h"
#pragma comment(lib,"TFIT_RSA_iRSA1024_64.lib")	
#pragma comment(lib,"TFIT_Utils_64.lib")
#endif

#include <regex>


bool CUtil::threadNameSet = false;

// Looking for numerals of the form ~1~, ~999~, ~TitleName~, etc
static bool FindParameter(const std::string& str, size_t& out_position, size_t& out_length, std::string& out_parameter, bool numeric)
{
	RAD_TELEMETRY_ZONE("FindParameter");
	
	const size_t length = str.length();
	std::string buffer;
	for (size_t begin = 0; begin < length; begin++)
	{
		buffer.clear();
		begin = str.find('~', begin);
		if (begin == std::string::npos)
			return false;

		for (size_t end = begin+1; end < length; end++)
		{
			char c = str[end];
			if (c == '~')
			{
				if (!buffer.empty())
				{
					out_position = begin;
					out_length = end - begin + 1;
					out_parameter = buffer;
					return true;
				}
				else
				{
					break;
				}
			}
			else if (numeric && (c >= '0' && c <= '9'))
			{
				buffer += c;
			}
			else if ((!numeric) && ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')))
			{
				buffer += c;
			}
			else
			{
				// close this tag
				begin = str.find(L'~', begin);
				break;
			}
		}
	}

	return false;
}

// Looking for numerals of the form ~1~, ~999~, ~TitleName~, etc
static bool FindParameter(const std::wstring& str, size_t& out_position, size_t& out_length, std::wstring& out_parameter, bool numeric)
{
	RAD_TELEMETRY_ZONE("FindParameter");

	const size_t length = str.length();
	std::wstring buffer;
	for (size_t begin = 0; begin < length; begin++)
	{
		buffer.clear();
		begin = str.find(L'~', begin);
		if (begin == std::string::npos)
			return false;

		for (size_t end = begin+1; end < length; end++)
		{
			wchar_t c = str[end];
			if (c == L'~')
			{
				if (!buffer.empty())
				{
					out_position = begin;
					out_length = end - begin + 1;
					out_parameter = buffer;
					return true;
				}
				else
				{
					break;
				}
			}
			else if (numeric && (c >= L'0' && c <= L'9'))
			{
				buffer += c;
			}
			else if ((!numeric) && ((c >= L'a' && c <= L'z') || (c >= L'A' && c <= L'Z')))
			{
				buffer += c;
			}
			else
			{
				// close this tag
				begin = str.find(L'~', begin);
				break;
			}
		}
	}

	return false;
}

void CUtil::FormatString(std::string& inOutStr)
{
	RAD_TELEMETRY_ZONE("CUtil::FormatString(std::string& inOutStr)");

	//@@: location CUTIL_FORMATSTRING_BUILD_TEXTUAL_PATTERN

	// Get the list of recognised config options
	const std::vector<CConfigOption*>& options = CConfigOption::GetOptions();

	// Must repeat the search as we do conditional replacement
	std::string parameter;
	size_t param_pos, param_length;
	while (FindParameter(inOutStr, param_pos, param_length, parameter, false))
	{
		RAD_TELEMETRY_ZONE("Replace body");

		// Look for the specified key in the option list
		bool found = false;
		for (size_t i = 0; i < options.size(); i++)
		{
			CConfigOption* opt = options[i];
			if (!opt)
			{
				ERRORF("NULL option (%d)!", i);
				continue;
			}

			if (parameter.compare(opt->GetName()) == 0)
			{
				found = true;
				std::string replacement;
				opt->GetValueAsString(replacement);
				inOutStr.replace(param_pos, param_length, replacement);
				break;
			}
		}

		// Remove any parameters that aren't found
		if (!found)
		{
			ERRORF("Named config option not found in string replacement: %s", parameter.c_str());
			inOutStr.replace(param_pos, param_length, "");
		}
	}

	// Replace "\n" with actual newline characters
	RAD_TELEMETRY_ZONE("Newline insertion");
	size_t pos = std::string::npos;
	while ((pos = inOutStr.find("\\n")) != std::string::npos)
	{
		inOutStr.replace(inOutStr.begin() + pos, inOutStr.begin() + pos + 2, "\r\n");
	}
}

void CUtil::FormatString(std::string& inOutStr, const std::vector<std::string>& params, FormatStringOption formatStringOptions)
{
	RAD_TELEMETRY_ZONE("CUtil::FormatString(std::string& inOutStr, const std::vector<std::string>& params, FormatStringOption formatStringOptions)");
	if (formatStringOptions & FORMAT_STRING_OPTION_PARAMS)
	{
		std::string parameter;
		size_t param_pos, param_length;
		// Must repeat the search as we do conditional replacement
		while (FindParameter(inOutStr, param_pos, param_length, parameter, true))
		{
			RAD_TELEMETRY_ZONE("Replace body");
			// String to int
			std::stringstream ss;
			ss << parameter.c_str();
			int index;
			ss >> index;
			index--;

			// Replace if in range of params
			if (index >= 0 && index < (signed)params.size())
			{
				const std::string& replacement = params[index];
				inOutStr.replace(param_pos, param_length, replacement);
			}
			else
			{
				ERRORF("Numbered parameter not in range of passed parameters: %s (size %u)", parameter.c_str(), params.size());
				inOutStr.replace(param_pos, param_length, "");
			}
		}
	}

	if (formatStringOptions & FORMAT_STRING_OPTION_CONFIG_OPTIONS)
	{
		FormatString(inOutStr);
	}

	// Replace "\n" with actual newline characters
	RAD_TELEMETRY_ZONE("Newline insertion");
	size_t pos = std::string::npos;
	while ((pos = inOutStr.find("\\n")) != std::string::npos)
	{
		inOutStr.replace(inOutStr.begin() + pos, inOutStr.begin() + pos + 2, "\r\n");
	}
}

void CUtil::FormatString(std::wstring& inOutWStr)
{
	RAD_TELEMETRY_ZONE("CUtil::FormatString(std::wstring& inOutWStr)");

	// Get the list of recognised config options
	const std::vector<CConfigOption*>& options = CConfigOption::GetOptions();

	// Must repeat the search as we do conditional replacement
	std::wstring wparameter;
	size_t param_pos, param_length;
	while (FindParameter(inOutWStr, param_pos, param_length, wparameter, false))
	{
		RAD_TELEMETRY_ZONE("Replace body");

		std::string parameter;
		WStringToStdString(wparameter, parameter);

		// Look for the specified key in the option list
		bool found = false;
		for (size_t i = 0; i < options.size(); i++)
		{
			CConfigOption* opt = options[i];
			if (!opt)
			{
				ERRORF("NULL option (%d)!", i);
				continue;
			}

			if (parameter.compare(opt->GetName()) == 0)
			{
				found = true;
				std::string replacementStr;
				opt->GetValueAsString(replacementStr);
				std::wstring replacementWstr;
				CUtil::StdStringToStdWString(replacementStr, replacementWstr);
				inOutWStr.replace(param_pos, param_length, replacementWstr);
				break;
			}
		}

		// Remove any parameters that aren't found
		if (!found)
		{
			ERRORF("Named config option not found in string replacement: %s", parameter.c_str());
			inOutWStr.replace(param_pos, param_length, L"");
		}
	}

	// Replace "\n" with actual newline characters
	RAD_TELEMETRY_ZONE("Newline insertion");
	size_t pos = std::string::npos;
	while ((pos = inOutWStr.find(L"\\n")) != std::string::npos)
	{
		inOutWStr.replace(inOutWStr.begin() + pos, inOutWStr.begin() + pos + 2, L"\r\n");
	}
}

void CUtil::FormatString(std::wstring& inOutStr, const std::vector<std::wstring>& params, FormatStringOption formatStringOptions)
{
	RAD_TELEMETRY_ZONE("CUtil::FormatString(std::wstring& inOutStr, const std::vector<std::wstring>& params, FormatStringOption formatStringOptions)");
	if (formatStringOptions & FORMAT_STRING_OPTION_PARAMS)
	{
		// Check for parameters being used, in the form ~1~
		// Must repeat the search as we do conditional replacement
		std::wstring wparameter;
		size_t param_pos, param_length;
		while (FindParameter(inOutStr, param_pos, param_length, wparameter, true))
		{
			RAD_TELEMETRY_ZONE("Replace body");

			// String to int
			std::wstringstream ss;
			ss << wparameter.c_str();
			int index;
			ss >> index;
			index--;

			// Replace if in range of params
			if (index >= 0 && index < (signed)params.size())
			{
				const std::wstring& replacement = params[index];
				inOutStr.replace(param_pos, param_length, replacement);
			}
			else
			{
				std::string parameter;
				WStringToStdString(wparameter, parameter);
				ERRORF("Numbered parameter not in range of passed parameters: %s (size %u)", parameter.c_str(), params.size());
				inOutStr.replace(param_pos, param_length, L"");
			}
		}
	}

	if (formatStringOptions & FORMAT_STRING_OPTION_CONFIG_OPTIONS)
	{
		FormatString(inOutStr);
	}

	// Replace "\n" with actual newline characters
	RAD_TELEMETRY_ZONE("Newline insertion");
	size_t pos = std::string::npos;
	while ((pos = inOutStr.find(L"\\n")) != std::string::npos)
	{
		inOutStr.replace(inOutStr.begin() + pos, inOutStr.begin() + pos + 2, L"\r\n");
	}
}


std::string& CUtil::DecodeAndDecrypt(std::string& inout_string)
{
#if ENABLE_ENCRYPT_XML
	char* plaintext = DecodeAndDecrypt_Internal(inout_string.c_str());
	inout_string.assign(plaintext);
	delete[] plaintext;
	return inout_string;
#else
	return inout_string;
#endif
}

std::string CUtil::DecodeAndDecrypt(const std::string& in_string)
{
	std::string temp = in_string;
	DecodeAndDecrypt(temp);
	return temp;
}

std::string& CUtil::Decrypt(u8* encrypted, u32 len, std::string& out_string)
{
    //@@: location CUTIL_DECRYPT_CALL_DECRYPT_INTERNAL
	char* ret = (char*)CUtil::Decrypt_Internal(encrypted, len);
    //@@: location CUTIL_DECRYPT_EXIT_DECRYPT_INTERNAL
	if (ret)
	{
		out_string.assign(ret);
		delete[] ret;
	}

	return out_string;
}


char * CUtil::DecodeAndDecrypt_Internal(char* input)
{
#if ENABLE_ENCRYPT_XML
	size_t len = strlen(input);
	size_t lenBytes = len/2;
	u8* bytes = new u8[lenBytes];
	memset(bytes, 0, lenBytes);
	for (size_t i = 0; i < lenBytes; ++i )
	{
			int temp;
		sscanf( input + 2 * i, "%2x", &temp );
		bytes[i] = temp;
	}

	char* ret = (char*)CUtil::Decrypt_Internal(bytes, (u32)lenBytes);
	if (ret)
	{
		delete[] bytes;
		return ret;
	}
	else
	{
		return (char*)bytes;
	}
#else
	return input;
#endif
}

u8* CUtil::Decrypt_Internal(u8 * encrypted, u32 len)
{
	
#if !PRELOADED_STRIP
	//@@: location CUTIL_DECRYPT_ENTRY
	TFIT_rsa_key_iRSA1024_t publicKey;
	TFIT_rsa_context_iRSA1024_t publicKeyContext;

	u32 retVal = TFIT_rsa_deserialize_key_iRSA1024(TFIT_RSA_Launcher_public_key, TFIT_RSA_Launcher_public_key_len, &publicKey);


	if(retVal!=0)
	{
		ERRORF("Error initializing: %d.", retVal);
		return NULL;
	}

	TFIT_rsa_prepare_context_iRSA1024(&publicKey, &publicKeyContext);
	//@@: location CUTIL_DECRYPT_VALIDATE_KEY
	retVal =  TFIT_validate_wb_key_iRSA1024(&publicKey);

	if(retVal!=0)
	{
		ERRORF("Error initializing: %d.", retVal);
		return NULL;
	}
	u32 newBytesWritten = 0;
	uint8_t *recombined_message = new uint8_t[len+1];
	memset(recombined_message, 0, len+1);
	DEBUGF1("Beginning Decryption");
	//@@: location CUTIL_DECRYPT_ON_DECRYPT
	retVal = TFIT_rsa_decrypt_iRSA1024(WBC_PKCS1_OAEP_PADDING, encrypted, len, &publicKey, &publicKeyContext, recombined_message, len, &newBytesWritten );
	//@@: location CUTIL_DECRYPT_DECRYPT_COMPLETE
	DEBUGF1("Finished Decryption");
	if(retVal!=0)
	{
		ERRORF("Error initializing: %d.", retVal);
		return NULL;
	}
	recombined_message[newBytesWritten+1]='\0';
	//@@: location CUTIL_DECRYPT_EXIT
	return recombined_message;
#else
	return NULL;
#endif
}


void CUtil::PrependTempoaryFolderPath(std::string& inout_path, const std::string& filename)
{
	std::wstring wprefix;
	PrependTempoaryFolderPath(wprefix, L"");

	std::string prefix;
	CUtil::WStringToStdString(wprefix, prefix);

	inout_path = prefix + filename;
}

void CUtil::PrependTempoaryFolderPath(std::wstring& inout_path, const std::wstring& filename)
{
	if (Globals::alternativeTempPath.empty())
	{
		wchar_t buffer[MAX_PATH];
		GetTempPathW(MAX_PATH, buffer);
		Globals::alternativeTempPath = buffer;
	}

	inout_path = Globals::alternativeTempPath + filename;
}

void CUtil::GetEnvStr(std::string& out_string)
{
#if RSG_FINAL
	out_string = "prod";
#else
	out_string = "dev";
#endif


	if (OnlineConfig::UseDev)
	{
		out_string = "dev";
	}
	
	if (!Globals::env)
	{
		return;
	}

	const size_t MAX_ENV_STRING_LENGTH = 10;

	std::wstring envstr = Globals::env.GetValue();
	if (envstr.size() > MAX_ENV_STRING_LENGTH)
	{
		return;
	}

	for (size_t i = 0; i < envstr.size(); i++)
	{
		wchar_t c = envstr[i];

		// If not a-z or 0-9
		if ((c < L'a' || c > L'z') && (c < L'0' || c > L'9'))
		{
			return;
		}
	}

	WStringToStdString(envstr, out_string);
}

void CUtil::GetPatchEnvStr(std::string& out_string)
{
	GetEnvStr(out_string);

	if (!Globals::patchenv)
	{
		return;
	}

	const size_t MAX_ENV_STRING_LENGTH = 10;

	std::wstring envstr = Globals::patchenv.GetValue();
	if (envstr.size() > MAX_ENV_STRING_LENGTH)
	{
		return;
	}

	for (size_t i = 0; i < envstr.size(); i++)
	{
		wchar_t c = envstr[i];

		// If not a-z or 0-9
		if ((c < L'a' || c > L'z') && (c < L'0' || c > L'9'))
		{
			return;
		}
	}

	WStringToStdString(envstr, out_string);
}

void CUtil::GetServerAndDocpathForEnv(std::string& out_server, std::string& out_docpath, const std::string& in_docpath)
{
	std::string envstr;

	GetPatchEnvStr(envstr);

	// Only use dev server if one is passed in via the command-line
	if (Constants::DevUsesInternalServer && envstr == Globals::DevEnvName && !Globals::devPatchServer.GetValue().empty())
	{
		std::string patchServer;
		CUtil::WStringToStdString(Globals::devPatchServer.GetValue(), patchServer);
		out_server = patchServer;
	}
	else
		out_server = Constants::ProductionPatchServer;


	if (in_docpath.front() == '/')
	{
		out_docpath = "/";
	}
	else
	{
		out_docpath.clear();
	}

	out_docpath += envstr;

	// Add everything after "/prod"
	out_docpath.append(in_docpath.begin() + 4 + (in_docpath.front() == '/' ? 1 : 0), in_docpath.end());
}


HANDLE CFileWrapper::CreateFileA(_In_ LPCSTR lpFileName, _In_ DWORD dwDesiredAccess, _In_ DWORD dwShareMode, _In_opt_ LPSECURITY_ATTRIBUTES lpSecurityAttributes, _In_ DWORD dwCreationDisposition, _In_ DWORD dwFlagsAndAttributes, _In_opt_ HANDLE hTemplateFile)
{
	std::wstring wfilename;
	CUtil::StdStringToStdWString(lpFileName, wfilename);

	return CreateFileW(wfilename.c_str(), dwDesiredAccess, dwShareMode, lpSecurityAttributes, dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile);
}

BOOL CFileWrapper::DeleteFileA(_In_ LPCSTR lpFileName)
{
	std::wstring wfilename;
	CUtil::StdStringToStdWString(lpFileName, wfilename);

	return DeleteFileW(wfilename.c_str());
}

int CFileWrapper::remove(const char *path)
{
	std::wstring wfilename;
	CUtil::StdStringToStdWString(path, wfilename);

	return DeleteFileW(wfilename.c_str()) ? 0 : 1;
}

BOOL CFileWrapper::SetFileAttributesA(_In_ LPCSTR lpFileName, _In_ DWORD dwFileAttributes)
{
	std::wstring wfilename;
	CUtil::StdStringToStdWString(lpFileName, wfilename);

	return SetFileAttributesW(wfilename.c_str(), dwFileAttributes);
}

BOOL CFileWrapper::GetDiskFreeSpaceExA(_In_opt_ LPCSTR lpDirectoryName, _Out_opt_ PULARGE_INTEGER lpFreeBytesAvailableToCaller, _Out_opt_ PULARGE_INTEGER lpTotalNumberOfBytes, _Out_opt_ PULARGE_INTEGER lpTotalNumberOfFreeBytes)
{
	std::wstring wfilename;
	CUtil::StdStringToStdWString(lpDirectoryName, wfilename);

	return GetDiskFreeSpaceExW(wfilename.c_str(), lpFreeBytesAvailableToCaller, lpTotalNumberOfBytes, lpTotalNumberOfFreeBytes);
}
