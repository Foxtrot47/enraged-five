#include "stdafx.h"

#if RSG_STEAM_LAUNCHER

#include "Steam.h"
#include "Channel.h"
#include "Config.h"
#include "LauncherDlg.h"
#include "Localisation.h"
#include "seh.h"

// rgsc includes
#include "rgsc_common.h"

#pragma comment(lib, "steam_api64.lib")

static u32 sm_AuthTickenLen = 0;
static u64 sm_SteamId = 0;
static bool m_bSteamApiInitialized = false;
static bool sm_bInitialized = false;
char sm_SteamPersonaName[rgsc::RGSC_STEAM_MAX_PERSONA_BUF_SIZE] = {0};
char sm_SteamAuthTicket[rgsc::RGSC_STEAM_TICKET_ENCODED_BUF_SIZE] = {0};
bool sm_bHasOwnership = false;
bool sm_bOfflineMode = false;

std::wstring sm_KeyboardResult;

static const int MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD = 512;

CSteam::KeyboardState CSteam::sm_KeyboardState = CSteam::NONE;
SteamCallbacks CSteam::sm_SteamCallbacks;

static CRITICAL_SECTION m_SteamCs;

void CSteam::EarlyInit()
{
	if (SteamAPI_Init())
	{
		m_bSteamApiInitialized = true;
		UpdateSteamLanguage();
	}
}

void CSteam::Init()
{
	rtry
	{
		InitializeCriticalSection(&m_SteamCs);

		rverify(!SteamAPI_RestartAppIfNecessary(GetSteamAppId()), nosteam, );
		rverify(m_bSteamApiInitialized || SteamAPI_Init(), catchall, ERRORF("Could not initialize Steam API"));
		
		rverify(SteamUser(), catchall, ERRORF("Could not initialize Steam User"));

		sm_bOfflineMode = !SteamUser()->BLoggedOn();
		CSteamID steamId = SteamUser()->GetSteamID();
		sm_SteamId = steamId.ConvertToUint64();

		rcheck(SteamUtils(), catchall, ERRORF("Could not initialize Steam Utils"));
		SteamUtils()->SetOverlayNotificationPosition(k_EPositionTopRight);

		sm_bHasOwnership = SteamApps()->BIsSubscribedApp(GetSteamAppId());
		rverify(sm_bHasOwnership, noentitlement, ERRORF("No entitlement"));

		RefreshSteamAuthTicket();
		rverify(strlen(sm_SteamAuthTicket) == (sm_AuthTickenLen * 2), catchall, ERRORF("Error converting steam App ID"));

		rverify(SteamFriends(), catchall, );

		strncpy_s(sm_SteamPersonaName, rgsc::RGSC_STEAM_MAX_PERSONA_BUF_SIZE, SteamFriends()->GetPersonaName(), _TRUNCATE);

		DISPLAYF("Steam initialized successfully");
		DISPLAYF("	Steam id: %I64u", sm_SteamId);
		DEBUGF1("	Steam ticket len: %u", sm_AuthTickenLen);
		DISPLAYF("	Steam offline: %s", sm_bOfflineMode ? "TRUE" : "FALSE");
		DISPLAYF("	Steam persona: %s", sm_SteamPersonaName);

		if (sm_bOfflineMode)
		{
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSteamOfflineMode);	
		}

		sm_bInitialized = true;
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);
	}
	rcatchall
	{
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_STEAM_FAILED, "Steam API processing did not succeed.");
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eErrorScreen);
	}
	rcatch(nosteam)
	{
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_STEAM_NO_STEAM, "The game was not launched via the Steam Client");
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eErrorScreen);
	}
	rcatch(noentitlement)
	{
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_STEAM_NO_ENTITLEMENT, "Steam API processing determined we did not own the app.");
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eErrorScreen);
	}
}

void CSteam::Shutdown()
{
	EnterCriticalSection(&m_SteamCs);

	SteamAPI_Shutdown();
	sm_bInitialized = false;
	CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);

	LeaveCriticalSection(&m_SteamCs);
}

void CSteam::Update()
{
	if (!sm_bInitialized)
		return;

	EnterCriticalSection(&m_SteamCs);
	SteamAPI_RunCallbacks();
	LeaveCriticalSection(&m_SteamCs);
}

void CSteam::UpdateSteamLanguage()
{
	if (SteamApps() == NULL)
		return;

	CLocalisation::Instance().SetLanguageFromSteam(SteamApps()->GetCurrentGameLanguage());
}

int CSteam::GetSteamAppId()
{
	return Constants::SteamAppId;
}

const char* CSteam::GetSteamAuthTicket()
{
	return sm_SteamAuthTicket;
}

u32 CSteam::GetSteamAuthTicketLen()
{
	return sm_AuthTickenLen;
}

u64 CSteam::GetSteamId()
{
	return sm_SteamId;
}

const char* CSteam::GetSteamPersonaName()
{
	return sm_SteamPersonaName;
}

bool CSteam::IsOfflineMode()
{
	return sm_bOfflineMode;
}

bool CSteam::ShowKeyboard(const char* text, const char* existing, bool bIsPassword, int maxNumChars)
{
	rtry
	{
		rverify(!IsKeyboardPending(), catchall, );

		EGamepadTextInputMode inputMode = bIsPassword ? k_EGamepadTextInputModePassword : k_EGamepadTextInputModeNormal;
		ISteamUtils* utils = SteamUtils();
		rverify(utils, catchall, );
		rcheck(utils->ShowGamepadTextInput(inputMode, k_EGamepadTextInputLineModeSingleLine, text, maxNumChars, existing), catchall, );

		sm_KeyboardState = PENDING;
		return true;
	}
	rcatchall
	{

	}

	return false;
}


const wchar_t* CSteam::GetVirtualKeyboardResult()
{
	char tmpResult[MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD];
	if(SteamUtils() && SteamUtils()->GetEnteredGamepadTextInput(tmpResult, MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD))
	{				
		std::string inStr(tmpResult);
		CUtil::StdStringToStdWString(inStr, sm_KeyboardResult);

		CSteam::sm_KeyboardState = CSteam::NONE;
		return sm_KeyboardResult.c_str();
	}

	CSteam::sm_KeyboardState = CSteam::NONE;
	return L"";
}

void CSteam::RefreshSteamAuthTicket()
{
	rtry
	{
		rcheck(SteamUser(), catchall, );
		memset(sm_SteamAuthTicket, 0, sizeof(sm_SteamAuthTicket));

		u8 authTicketU8[rgsc::RGSC_STEAM_TICKET_BUF_SIZE];
		HAuthTicket hTicket;
		hTicket = SteamUser()->GetAuthSessionTicket(authTicketU8, sizeof(authTicketU8), &sm_AuthTickenLen);
		rverify(hTicket != k_HAuthTicketInvalid, catchall, ERRORF("Invalid Steam Ticket"));

		// hex-encode the steam auth ticket
		char * strPtr = sm_SteamAuthTicket;
		for(u32 i = 0; i < sm_AuthTickenLen; i++)
		{
			strPtr += sprintf(strPtr, "%02X", authTicketU8[i]);
		}
	}
	rcatchall
	{

	}

	DEBUGF1("Refresh Steam ticket: %s", sm_SteamAuthTicket);
}


void CSteam::SteamStoreOpenCallback(u32 appId)
{
	if (SteamFriends())
	{
		SteamFriends()->ActivateGameOverlayToStore(appId, k_EOverlayToStoreFlag_None);
	}
}

void SteamCallbacks::OnSteamInputDismissed( GamepadTextInputDismissed_t *pParam )
{
	if(pParam != NULL && pParam->m_bSubmitted)
	{
		CSteam::sm_KeyboardState = CSteam::SUCCESS;
	}
	else
	{
		CSteam::sm_KeyboardState = CSteam::CANCELLED;
	}
}

void SteamCallbacks::OnGameOverlayCallback(GameOverlayActivated_t* goat)
{
	if (goat && !goat->m_bActive)
	{
		if (CSteam::IsKeyboardPending())
		{
			CSteam::sm_KeyboardState = CSteam::CANCELLED;
		}
		else
		{
			CSteam::sm_KeyboardState = CSteam::NONE;
		}
	}
}

#endif // RSG_STEAM_LAUNCHER