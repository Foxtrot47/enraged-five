#pragma once

#define RSG_IN_PLACE_DOWNLOAD (RSG_PRELOADER_LAUNCHER || RSG_DIGITAL_LAUNCHER)

#define RSG_PAID_DLC_SUPPORT 0

#if RSG_IN_PLACE_DOWNLOAD

#include <vector>
#include "IPatchCheck.h"

class InPlaceDownloader
{
public:
	enum DownloadVisibility
	{
		IN_FOREGROUND,
		IN_BACKGROUND
	};
	static void CheckManifestSynchronous();
	static void DownloadFilesSynchronous(DownloadVisibility downloadType);
	static void PerformIntegrityCheckSynchronous();
	static void StartDownloadingFilesForSku(const std::string& entitlementCode);
	static void Cancel();

	static bool QuickMissingFileCheck();

	static bool IsUpToDate();
	static bool IsRunning();

	static void ChangeVisibility(DownloadVisibility visibility);

	struct Manifest
	{
		Manifest() : entitled(false), addedToList(false), backgroundDownloadInProgress(false) {}

		Patch sourceData;
		std::string urlPrefix;
		bool entitled;
		bool addedToList;
		bool backgroundDownloadInProgress;

		std::vector<Patch> fileList;
	};

private:
	static bool sm_initialized;
	static bool sm_upToDate;
	static bool sm_cancelled;
	static bool sm_running;

	static std::vector<Manifest> sm_manifests;

	static std::string sm_destPathPrefix;

	static std::vector<Patch> sm_downloadFileList;

	static u64 sm_totalAlreadyDownloaded;
	static u64 sm_remainingDownloadBytes;

	static void Init();
	static bool DownloadAndReadAllManifests();
	static bool DownloadAndReadManifest(Manifest& manifest);
	static bool ReadManifest(Manifest& manifest);

	class VerificationDelegate
	{
	public:
		virtual void VerifiedBytes(u64 bytes) = 0;
	};

	static bool TrimDownloadFileList();

	enum FilePresenceStatus
	{
		FILE_COMPLETE,
		FILE_PARTIAL,
		FILE_MISSING,
		FILE_ERROR
	};
	static FilePresenceStatus CheckPresence(const Patch& file);

	static bool VerifyIntegrity(const Patch& file, VerificationDelegate& delegate);
	static bool VerifyIntegrityOfOpenFile(const Patch& file, HANDLE fileHandle, VerificationDelegate& delegate);

	static bool CheckDigitalDataVersionSynchronous();

	static u64 SumManifestFilesizes();

	static bool DeleteCorruptFile(const std::string& filepath);

	static ProgressCalculator sm_downloadProgress, sm_verifyProgress;

	static DownloadVisibility sm_downloadVisibility;
};

#endif
