#pragma once

#include <vector>

#include "InPlaceDownloader.h"

#define ENABLE_SP_CLOUD_SAVE 1

class CLauncherDlg;

class CFsm
{
public:
	CFsm(CLauncherDlg* launcher);
	~CFsm(void);

	enum FsmState
	{
		eNone,
#if RSG_STEAM_LAUNCHER
		eSteamInit,
		eSteamShutdown,
#endif // RSG_STEAM_LAUNCHER
		eDownloadOnlineConfig,
		eCheckForRGSCUpdates,
		eSocialClub,
		eSignInAgain,
#if !RSG_PRELOADER_LAUNCHER
		eCheckForEntitlement,
#endif
		eShowCodeRedemption,
#if RSG_IN_PLACE_DOWNLOAD
		eCheckForInPlaceDownload,
#endif
		eCheckForUpdate,
#if ENABLE_SP_CLOUD_SAVE
		eCloudSyncPreGame,
		eCloudSyncPostGame,
		eCloudSyncPreGameError,
		eCloudSyncPreGameTimeout,
		eCloudSyncPostGameError,
		eCloudSyncPreGameBannedError,
		eCloudSyncPreGamePermaBannedError,
		eCloudSyncPostGameBannedError,
		eCloudSyncPostGamePermaBannedError,
#endif
		eShutdownSocialClub,
#if RSG_PRELOADER_LAUNCHER
		eCheckForReleaseDate,
		eReleaseDateCountdown,
		eReleaseDateRestart,
#else
		eLoading,
		eRestartGame,
#endif
		eRequiredUpdate,
		eOptionalUpdate,
		eDLCAvailable,
		eMTLAvailable,
#if RSG_IN_PLACE_DOWNLOAD
		eGameInstallAvailable,
		eDownloadInPlaceData,
		eDownloadInPlaceDataError,
		eInPlaceIntegrityCheck,
#endif
		eDownloadUpdate,
		eDownloadOptionalUpdate,
		eResumeDownloadingUpdate,
		eInstallUpdate,
		eSocialClubError,
		eErrorScreen,
		eExitCodeError,
		eNoTempSpace,
		eNoInstallSpace,
		eNoInternetForActivate,
		eOffline,
		eOfflineNoUAC,
		eSafeMode,
		eSendingReport,
		eRestart,
		eNeedToRestartAsAdmin,
		eRestartAsAdmin,
		eShuttingDown,
	};

	FsmState GetState() const;
	void SetState(FsmState state);
	void SetFlowState(FsmState state);

	void RemoveFlowState(FsmState state);
	void RemoveOnlineStates();
	void NextState();

	static const char* GetStateName(FsmState state);

private:
	void OnLeaveState(FsmState state);
	void OnEnterState(FsmState state);

	void GetCloudSaveBannedString(std::wstring& text);

	CLauncherDlg* m_launcher;
	FsmState m_state;

	std::vector<FsmState> m_applicationFlow;
};
