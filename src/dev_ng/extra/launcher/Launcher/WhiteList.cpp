#include "StdAfx.h"
#include "WhiteList.h"
#include "RockstarDownloader.h"
#include "SocialClub.h"
#include "Globals.h"
#include "Util.h"

WhiteList WhiteList::g_whiteList;

#define ENABLE_WHITELIST_SHA_VALUES 0

WhiteList::WhiteList()
{
	//@@: range WHITELIST_CONSTRUCTOR {
	//@@: location WHITELIST_CONSTRUCTOR_ADD_WHITELIST
#if ENABLE_WHITELIST_SHA_VALUES
		Add("4B2AF8B1F5C81231B47FF94D427A01453D797E4874E18D2C71E807792033B651");
		Add("8AD3FEAF3A83BEC01000F2E22150572B6C8E9D191839D145C4183F487AF985C5");
	#if !RSG_FINAL
		if (DebugSwitches::DebugTestMissingPatch)
		{
			Add("4143ECD0AD3A38D1C8C970378AE7B1B803B5F55B9AD82C1F4C550234A3B5EBEF");
		}
		/*
		if (DebugSwitches::UseLocalWebserver)
		{
			Add("E21CEC34BC88018461CEDAF1B6D52335534115669B2E11D1C28C44F2E3FE74C6");
		}
		*/
	#endif //FINAL

	#else
		Add("patches.rockstargames.com");
		Add("rsgames.hs.llnwd.net");
		Add("rsgames.vo.llnwd.net");
		Add("rsgldswww7242");
		Add("gamedownloads.rockstargames.com");
		Add("gamedownloads-rockstargames-com.akamaized.net");
	#if !RSG_FINAL
		if (DebugSwitches::DebugTestMissingPatch)
		{
			Add("example.com");
		}

		/*
		if (DebugSwitches::UseLocalWebserver)
		{
			Add("localhost");
		}
		*/
	#endif //FINAL
#endif

	//@@: } WHITELIST_CONSTRUCTOR

}

WhiteList::~WhiteList()
{
	Clear();
}

void WhiteList::Clear()
{
	//@@: location WHITELIST_CLEAR
	g_whiteList.m_WhiteList.clear();
}

bool WhiteList::Add(const std::string& url)
{
	if (!Exists_internal(url))
	{
		g_whiteList.m_WhiteList.push_back(url);
	}
	return true;
}

bool WhiteList::Exists(const std::string& url)
{
	if (!Exists_internal(url))
	{
		WARNINGF("Invalid URL: %s", url.c_str());
		return false;
	}
	else
	{
		return true;
	}
}

bool WhiteList::Exists_internal(const std::string& url)
{
	//@@: range WHITELIST_EXISTS {
	char* server;

	if (!RockstarDownloader::GetServer(url.c_str(), &server, true))
		return false;
#if ENABLE_WHITELIST_SHA_VALUES
	char *outHash = new char[HASH_DIGEST_STRING_LENGTH];
	SHAHashHelper((u8*)server, strlen(server), outHash);
	delete [] server;
	server = outHash;
#endif
	//@@: location WHITELIST_EXISTS_ITERATE_LIST
	for (std::vector<std::string>::iterator itr = g_whiteList.m_WhiteList.begin(); itr != g_whiteList.m_WhiteList.end(); ++itr)
	{
		if (strcmp((*itr).c_str(), server) == 0)
		{
			delete[] server;
			return true;
		}
	}

	delete[] server;
	return false; 
	//@@: } WHITELIST_EXISTS

}
