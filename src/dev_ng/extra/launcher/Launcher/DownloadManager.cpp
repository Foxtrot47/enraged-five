#include "StdAfx.h"
#include "DownloadManager.h"
#include "RockstarDownloader.h"
#if RSG_LAUNCHER
#include "ChunkedDownloader.h"
#endif
#include "Channel.h"
#include "Globals.h"

Downloader::Downloader(void)
	: m_bDownloading(false), m_currentDownloader(NULL), m_regularDownloader(NULL), m_chunkedDownloader(NULL), m_bError(false), m_pDownloaderThread(NULL), m_bChunkedDownloadingEnabled(true)
{

}

Downloader::~Downloader(void)
{
	//@@: location DOWNLOADER_DESTRUCTOR
	m_currentDownloader = NULL;
	if (m_regularDownloader)
	{
		delete m_regularDownloader;
		m_regularDownloader = NULL;
	}
	if (m_chunkedDownloader)
	{
		delete m_chunkedDownloader;
		m_chunkedDownloader = NULL;
	}
}

Downloader& Downloader::Instance()
{
	//@@: location DOWNLOADER_INSTANCE
	static Downloader instance;
	return instance;
}

void Downloader::Init()
{
	DEBUGF1("Initialising downloader.");
	RAD_TELEMETRY_ZONE("Downloader::Init");
	//@@: location DOWNLOADER_INIT_INITIALISING
	m_bDownloading = false;
	//@@: range DOWNLOADER_INIT_SET_DOWNLOADER {
	if (!m_regularDownloader)
	{
		// Remove connection limit due to misreading of RFC2616
		DWORD maxConns = 40;
		if (!InternetSetOption(NULL, INTERNET_OPTION_MAX_CONNS_PER_SERVER, &maxConns, sizeof(maxConns)))
		{
			ERRORF("Unable to set maximum connections");
		}
		if (!InternetSetOption(NULL, INTERNET_OPTION_MAX_CONNS_PER_1_0_SERVER, &maxConns, sizeof(maxConns)))
		{
			ERRORF("Unable to set maximum 1.0 connections");
		}

		//@@: location DOWNLOADER_INIT_CREATE_DOWNLOADER
		m_regularDownloader = new RockstarDownloader();
		m_chunkedDownloader = new ChunkedDownloader();

		m_currentDownloader = m_chunkedDownloader;

		if (Globals::noChunkedDownload)
		{
			EnableChunkedDownloading(false);
		}
	}
	//@@: } DOWNLOADER_INIT_SET_DOWNLOADER

	m_bError = false;
	m_pDownloaderThread = NULL;
}

void Downloader::Shutdown()
{
	DEBUGF1("Shutting down downloader.");
	if (m_pDownloaderThread)
		WaitForSingleObject(m_pDownloaderThread->m_hThread, INFINITE);
	
	m_currentDownloader = NULL;
	if (m_regularDownloader)
	{
		delete m_regularDownloader;
		m_regularDownloader = NULL;
	}
	if (m_chunkedDownloader)
	{
		delete m_chunkedDownloader;
		m_chunkedDownloader = NULL;
	}
	//@@: location DOWNLOADER_SHUTDOWN
	m_pDownloaderThread = NULL;
}

void Downloader::QueueFile(std::string url, std::string localFile, bool backgroundDownload, bool redownload, bool chunkedDownload)
{
	//@@: range DOWNLOADER_QUEUE_FILE_RANGE {
	//@@: location DOWNLOADER_QUEUE_FILE
	Patch patch;
	patch.PatchURL = url;
	patch.LocalFilename = localFile;
	patch.BackgroundDownload = backgroundDownload;
	patch.AlwaysDownload = redownload;
	patch.ChunkedDownload = chunkedDownload;

	QueuePatch(patch);
	
//@@: } DOWNLOADER_QUEUE_FILE_RANGE
}

void Downloader::QueuePatch(Patch patch)
{
	//@@: range DOWNLOADER_QUEUEPATCH_RANGE {
	//@@: location DOWNLOADER_QUEUEPATCH
	m_PatchQueue.push(patch); 
	//@@: } DOWNLOADER_QUEUEPATCH_RANGE

} 


void Downloader::DownloadComplete(DownloadCompleteStatus /*status*/, std::string /*filename*/)
{
	if (m_bDownloading && m_PatchQueue.empty())
	{
		//@@: location DOWNLOADER_DOWNLOADCOMPLETE_SET_INACTIVE
	}
	
	m_bDownloading = false;
}

void Downloader::DownloadError(const CError& /*error*/)
{
	//@@: location DOWNLOADER_DOWNLOADERROR
	m_bDownloading = false;
	m_bError = true;
}

void Downloader::StartSynchronously()
{
	DEBUGF1("Starting downloader synchronously...");

	RAD_TELEMETRY_ZONE("Downloader::StartSynchronously");

	if (!m_currentDownloader)
	{
		Init();
	}

	m_bDownloading = true;


	//@@: range DOWNLOADER_STARTSYNCHRONOUSLY_LOOP_AND_COMPLETE {
	while (!m_PatchQueue.empty() && m_bDownloading)
	{
		Patch currentPatch = m_PatchQueue.front();
		//@@: location DOWNLOADER_STARTSYNCRHONOUSLY_POP_FROM_QUEUE
		m_PatchQueue.pop();

		DEBUGF1("Downloading %s", currentPatch.PatchURL.c_str());

		if (currentPatch.ChunkedDownload && m_bChunkedDownloadingEnabled)
		{
			DEBUGF1("Selecting chunked downloader.");
			m_currentDownloader = m_chunkedDownloader;
		}
		else
		{
			DEBUGF1("Selecting regular downloader.");
			m_currentDownloader = m_regularDownloader;
		}

		m_currentDownloader->SetCurrentPatch(currentPatch);

		m_currentDownloader->StartSynchronously();

		if (m_currentDownloader->HasErrorOccurred())
		{
			ERRORF("Error occurred downloading.");
			break;
		}
	}

	//@@: location DOWNLOADER_STARTSYNCRHONOUSLY_DOWNLOADING_ENDED
	DEBUGF1("Synchronous downloading ended.");
	//@@: location DOWNLOADER_STARTSYNCRHONOUSLY_DOWNLOADING_ENDED_LOCATION
	m_bDownloading = false; 
	//@@: } DOWNLOADER_STARTSYNCHRONOUSLY_LOOP_AND_COMPLETE

	if (!m_PatchQueue.empty())
	{
		WARNINGF("Not all files were completed.");
	}
}

void Downloader::SetMaxRetries(int maxRetries)
{
	if (!m_currentDownloader)
	{
		Init();
	}

	m_currentDownloader->SetMaxRetries(maxRetries);
}

void Downloader::SetDownloadListener(IDownloader::DownloadListener* listener)
{
	if (!m_currentDownloader)
	{
		Init();
	}

	m_regularDownloader->SetListener(listener);
	m_chunkedDownloader->SetListener(listener);
}

void Downloader::EnableChunkedDownloading(bool enabled)
{
	m_bChunkedDownloadingEnabled = enabled;
	DEBUGF1(enabled ? "Chunked downloading enabled." : "Chunked downloading disabled.");
}

void Downloader::SetPaused(bool paused)
{
	//@@: range DOWNLOADER_SETPAUSED {
	if (paused)
		DEBUGF1("Pausing downloader.");
	else
		DEBUGF1("Resuming downloader.");

	if (m_currentDownloader)
	{
		//@@: location DOWNLOADER_SETPAUSED_PAUSE_DOWNLOAD
		m_currentDownloader->SetPaused(paused);
	}
	else
	{
		ERRORF("No downloader!");
	} 
	//@@: } DOWNLOADER_SETPAUSED

}

void Downloader::Stop()
{
	//@@: range DOWNLOADER_STOP {
	DEBUGF1("Stopping downloader.");
	m_bDownloading = false;

	if (m_currentDownloader)
	{
		m_currentDownloader->Stop();
	}
	else
	{
		ERRORF("No downloader!");
	} 
	//@@: } DOWNLOADER_STOP

}

void Downloader::Reset()
{
	//@@: range DOWNLOADER_RESET {
	DEBUGF1("Resetting downloader.");
	//@@: location DOWNLOADER_RESET_STOP_CALL
	Stop();
	while (!m_PatchQueue.empty())
		m_PatchQueue.pop(); 
	//@@: } DOWNLOADER_RESET

}
