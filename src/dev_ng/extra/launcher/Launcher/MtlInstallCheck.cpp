#include "stdafx.h"

#include "MtlInstallCheck.h"
#include "IPatchCheck.h"
#include "Util.h"
#include "VersionManager.h"

const char* MTL_LAUNCHER_EXE = "Launcher.exe";
const char* MTL_BOOTSTRAP_EXE = "LauncherPatcher.exe";
Version MTL_MIN_VERSION = Version(1,0,3,0);

MtlInstallCheck::MtlInstallCheck(void)
{
	m_patchCheckType = PT_MTL;
}

MtlInstallCheck::~MtlInstallCheck(void)
{

}

void MtlInstallCheck::ParseXML()
{
	Patch patch;
	if (VersionManager::GetMtlPatch(patch))
	{
		patch.DeleteOnCompletion = true;
		patch.ChunkedDownload = false;
		patch.UpdateType = UT_MANDATORY;
		patch.PatchType = PT_MTL;

		// MTL requires no command-line arguments
		patch.CommandlineArguments = "";

		// MTL upgrade requires the launcher to exit as soon as it executes the MTL installer
		patch.RequiresShutdown = true;

		CUtil::PrependTempoaryFolderPath(patch.LocalFilename, patch.LocalFilename);

		// MTL upgrade doesn't require this
		//ConvertToNeedingAccessControlledUrl(patch);

		m_patches.clear();
		m_patches.push_back(patch);
		DEBUGF1("Adding MTL patch (now %d): %s", m_patches.size(), patch.PatchURL.c_str());
	}
	return;
}

Version MtlInstallCheck::GetExecutableVersion()
{
	Version version;

	GetMtlVersion(version);

	return version;
}

void MtlInstallCheck::DownloadComplete(DownloadCompleteStatus status, std::string filename)
{

}

void MtlInstallCheck::DownloadError(const CError& errorString)
{
	m_bCheckError = true;
}

bool MtlInstallCheck::GetMtlVersion(Version& out_version)
{
	std::string path;
	if (!GetMtlExePath(path, MTL_LAUNCHER_EXE))
	{
		DISPLAYF("Can't find RGL installed path");
		return false;
	}

	bool exists = false;
	Version version = IPatchCheck::GetVersionForFile(path, &exists);

	if (!exists)
	{
		DISPLAYF("Can't find RGL installed exe");
		return false;
	}
	else
	{
		out_version = version;
		return true;
	}
}

bool MtlInstallCheck::IsRecentMtlInstalled()
{
	Version version;

	if (!GetMtlVersion(version))
		return false;

	if (MTL_MIN_VERSION > version)
	{
		DISPLAYF("RGL version is old: %d.%d.%d.%d", version.major, version.minor, version.build, version.revision);
		return false;
	}

	DISPLAYF("Found installed RGL, version: %d.%d.%d.%d", version.major, version.minor, version.build, version.revision);

	return true;
}

bool MtlInstallCheck::LaunchMtl()
{
	std::string path;
	if (!GetMtlExePath(path, MTL_BOOTSTRAP_EXE))
	{
		return false;
	}

	std::string args = "-minmodeApp=gta5";

	std::wstring wpath, wargs;
	CUtil::StdStringToStdWString(path, wpath);
	CUtil::StdStringToStdWString(args, wargs);

	HINSTANCE hInst = ShellExecuteW(NULL, L"open", wpath.c_str(), wargs.c_str(), NULL, SW_SHOWNORMAL);
	if ((int)hInst > 32)
	{
		DISPLAYF("Started RGL.");
		return true;
	}
	else
	{
		DISPLAYF("Failed to start RGL.");
		return false;
	}
}

bool MtlInstallCheck::GetMtlExePath(std::string& out_path, const char* filename)
{
	std::wstring key32, key64, valName;

	CUtil::StdStringToStdWString("SOFTWARE\\Rockstar Games\\Launcher", key32);
	CUtil::StdStringToStdWString("SOFTWARE\\Wow6432Node\\Rockstar Games\\Launcher", key64);
	CUtil::StdStringToStdWString("InstallFolder", valName);

	// First attempt to retrieve the game install folder from the registry
	out_path = IPatchCheck::GetValueFromRegistry(key32.c_str(), key64.c_str(), valName.c_str());

	// If the key value is valid, return it.
	// Otherwise, If the registry key cannot be found, default to the launcher executable directory
	if (out_path.length() > 0)
	{
		out_path += "\\";
		out_path += filename;
		return true;
	}
	else
	{
		return false;
	}
}