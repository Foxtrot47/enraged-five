#pragma once

#include "stdafx.h"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <sstream>
#include "Channel.h"
#include "Error.h"
#include "Localisation.h"
#include "RADTelemetrySupport.h"

#include "resource.h"

#define ENABLE_ENCRYPT_XML 0
class CUtil
{
public:
	static std::string& DecodeAndDecrypt(std::string& inout_string);
	static std::string DecodeAndDecrypt(const std::string& in_string);
	static std::string& Decrypt(u8* encrypted, u32 len, std::string& out_string);

private:
	static char* DecodeAndDecrypt_Internal(char* input);
	static u8* Decrypt_Internal(u8* encrypted, u32 len);

public:

#if RSG_FINAL
	static void SetCurrentThreadName(const char*) 
	{
	}
#else
	static void SetCurrentThreadName(const char* name)
	{
		if (HasThreadNameBeenSet())
			return;

		threadNameSet = true;
		// From: http://msdn.microsoft.com/en-us/library/xcb2z8hs(v=vs.90).aspx
#pragma pack(push,8)
		typedef struct tagTHREADNAME_INFO
		{
			DWORD dwType; // Must be 0x1000.
			LPCSTR szName; // Pointer to name (in user addr space).
			DWORD dwThreadID; // Thread ID (-1=caller thread).
			DWORD dwFlags; // Reserved for future use, must be zero.
		} THREADNAME_INFO;
#pragma pack(pop)

		//@@: location CUTIL_SETCURRENTTHREADNAME_SET_THREAD_INFO
		THREADNAME_INFO info;
		info.dwType = 0x1000;
		info.szName = name;
		info.dwThreadID = (DWORD)(-1);
		info.dwFlags = 0;

		const DWORD MS_VC_EXCEPTION=0x406D1388;

		__try
		{
			RaiseException( MS_VC_EXCEPTION, 0, sizeof(info)/sizeof(ULONG_PTR), (ULONG_PTR*)&info );
		}
		__except(EXCEPTION_CONTINUE_EXECUTION)
		{
		}

#if ENABLE_RAD_TELEMETRY_SUPPORT
		tmThreadName(CRadTelemetrySupport::GetContext(), 0, name);
#endif
	}
#endif

	static __declspec(thread) bool threadNameSet;
	static bool HasThreadNameBeenSet()
	{
		return threadNameSet;
	}

	static int WriteHumanReadableDataSize(wchar_t* buffer, size_t buffersize, u64 bytes)
	{
		std::wstring wstr;
		AppendHumanReadableDataSize(wstr, bytes);
		return swprintf_s(buffer, buffersize, L"%s", wstr.c_str());
	}

	static int WriteHumanReadableDataSize(char* buffer, size_t buffersize, u64 bytes)
	{
		std::wstring wstr;
		AppendHumanReadableDataSize(wstr, bytes);

		std::string sstr;
		WStringToStdString(wstr, sstr);

		return sprintf_s(buffer, buffersize, "%s", sstr.c_str());
	}

	static void AppendHumanReadableDataSize(std::wstring& str, u64 bytes)
	{
		const size_t buffersize = 64;
		wchar_t buffer[buffersize];
		UINT id = IDS_UNITS_BYTES;

		if (bytes < 1024)
		{
			id = IDS_UNITS_BYTES;
			_stprintf_s(buffer, buffersize, TEXT("%d"), (int)bytes);
		}
		else if (bytes < 1024 * 1024)
		{
			id = IDS_UNITS_KILOBYTES;
			float k = (float)bytes / 1024.0f;
			_stprintf_s(buffer, buffersize, TEXT("%.1f"), k);
		}
		else if (bytes < 1024 * 1024 * 1024)
		{
			id = IDS_UNITS_MEGABYTES;
			float M = (float)bytes / (1024.0f * 1024.0f);
			_stprintf_s(buffer, buffersize, TEXT("%.2f"), M);
		}
		else if (bytes < 1024LL * 1024LL * 1024LL * 1024LL)
		{
			id = IDS_UNITS_GIGABYTES;
			float G = (float)bytes / (1024.0f * 1024.0f * 1024.0f);
			_stprintf_s(buffer, buffersize, TEXT("%.2f"), G);
		}
		else
		{
			id = IDS_UNITS_TERABYTES;
			float T = (float)bytes / (1024.0f * 1024.0f * 1024.0f * 1024.0f);
			_stprintf_s(buffer, buffersize, TEXT("%.2f"), T);
		}

		std::wstring formatted = WideStringFromID(id);
		std::vector<std::wstring> params;
		params.push_back(std::wstring(buffer));
		FormatString(formatted, params);

		str += formatted;
	}

	static void AppendHumanReadableDataSize(std::string& str, u64 bytes)
	{
		std::wstring wstr;
		AppendHumanReadableDataSize(wstr, bytes);

		std::string mbstr;
		WStringToStdString(wstr, mbstr);

		str.append(mbstr);
	}

	static void FormatStringNumbers(std::string& inOutStr, int num1, int num2)
	{
		std::vector<std::string> params;

		char buffer[64];
		_itoa_s(num1, buffer, 64, 10);
		params.push_back(buffer);
		_itoa_s(num2, buffer, 64, 10);
		params.push_back(buffer);

		FormatString(inOutStr, params);
	}

	static void FormatStringNumbers(std::string& inOutStr, int num1, int num2, int num3)
	{
		std::vector<std::string> params;

		char buffer[64];
		_itoa_s(num1, buffer, 64, 10);
		params.push_back(buffer);
		_itoa_s(num2, buffer, 64, 10);
		params.push_back(buffer);
		_itoa_s(num3, buffer, 64, 10);
		params.push_back(buffer);

		FormatString(inOutStr, params);
	}

	static std::string FormatStringNumberAndString(std::string inStr, int num1, std::string str2)
	{
		std::string outStr = inStr;

		std::vector<std::string> params;

		char buffer[64];
		std::string strNum1, strNum2;
		_itoa_s(num1, buffer, 64, 10);
		params.push_back(buffer);
		params.push_back(str2);

		FormatString(outStr, params);

		return outStr;
	}

	enum FormatStringOption
	{
		FORMAT_STRING_OPTION_PARAMS = 1<<0,
		FORMAT_STRING_OPTION_CONFIG_OPTIONS = 1<<1,
	};

	static void FormatString(std::string& inOutStr);
	static void FormatString(std::string& inOutStr, const std::vector<std::string>& params, FormatStringOption options = (FormatStringOption)(FORMAT_STRING_OPTION_PARAMS | FORMAT_STRING_OPTION_CONFIG_OPTIONS));

	static void FormatString(std::wstring& inOutWStr);
	static void FormatString(std::wstring& inOutStr, const std::vector<std::wstring>& params, FormatStringOption options = (FormatStringOption)(FORMAT_STRING_OPTION_PARAMS | FORMAT_STRING_OPTION_CONFIG_OPTIONS));

	static std::wstring WideStringFromID(UINT identifier)
	{
		return CLocalisation::Instance().GetWString(identifier);
	}

	static char* NewCStringFromID(UINT identifier)
	{
		std::string idStr = CLocalisation::Instance().GetString(identifier);
		return NewCString(idStr);
	}

	static char* NewCString(const std::string& str)
	{
		const char* c_str = str.c_str();
		size_t len = strlen(c_str) + 1;
		char* buffer = new char[len];
		strcpy_s(buffer, len, c_str);
		return buffer;
	}

	static bool ReadSmallFileAsString(const std::string& filename, std::string& out_str)
	{
		std::wstring wfilename;
		StdStringToStdWString(filename, wfilename);
		std::ifstream strm(wfilename.c_str());
		if (strm.good())
		{
			std::stringstream buffer;
			buffer << strm.rdbuf();
			out_str.assign(buffer.str());

			return true;
		}
		else
		{
			return false;
		}
	}

	static void SanitizeSingleLineUTF8(std::string& str, bool remove = true)
	{
		for (int i = (int)str.length()-1; i >= 0; i--)
		{
			u8 c = str.at(i);
			if (c < ' ')
			{
				if (remove)
				{
					str.erase(str.begin() + i);
				}
				else
				{
					str.replace(i, 1, 1, '?');
				}
			}
		}
	}

	static void SanitizeToSingleShortLine(std::string &str)
	{
		size_t index = 0, start_index = 0, wordLength = 0;
		for (; index < str.length() && index < 200; index++)
		{
			char c = str.at(index);
			if (c == ' ')
			{
				wordLength = 0;

			}
			else
			{
				wordLength++;

				if ((index != start_index && (c == '\n' || c == '\r')) || wordLength > 50)
				{
					break;
				}
			}

			// Skip leading whitespace / unprintable characters
			if (index == start_index && c <= ' ')
			{
				start_index++;
			}
		}

		// Trim from the end
		if (index < str.length())
		{
			str.resize(index);
		}

		// Trim from the start
		if (start_index > 0)
		{
			str.erase(0, start_index);
		}

		SanitizeSingleLineUTF8(str);
	}

	static void StdStringToStdWString(const std::string& in, std::wstring& out)
	{
		size_t len = MultiByteToWideChar(CP_UTF8, 0, in.c_str(), -1, NULL, 0);
		WCHAR* outStr = new WCHAR[len];

		MultiByteToWideChar(CP_UTF8, 0, in.c_str(), -1, outStr, (int)len);

		out.assign(outStr);

		delete [] outStr;
	}

	static void WStringToStdString(const std::wstring& in, std::string& out)
	{
		size_t len = WideCharToMultiByte(CP_UTF8, 0, in.c_str(), -1, NULL, 0, NULL, NULL);

		char* outStr = new char[len];

		WideCharToMultiByte(CP_UTF8, 0, in.c_str(), -1, outStr, (int)len, NULL, NULL);

		out.assign(outStr);

		delete [] outStr;
	}


	static void Sleep(DWORD dmillis)
	{
		int millis = (int)dmillis;
#ifndef _DEBUG
		millis = 1;
#else
		static std::map<int, int> breakdown;
		static int sum = 0;
		sum += millis;

		std::map<int, int>::iterator it = breakdown.find(millis);
		
		if (it == breakdown.end())
		{
			breakdown[millis] = 1;
		}
		else
		{
			int prev = it->second;
			breakdown[millis] = prev+1;
		}

		static int max_sum = 10;

		if (sum >= max_sum)
		{
			if (max_sum < 60 * 1000)
			{
				max_sum *= 2;
			}
			std::stringstream message;
			message << "Slept for " << sum << " millis total! (";

			for (it = breakdown.begin(); it != breakdown.end(); it++)
			{
				if (it != breakdown.begin())
					message << ", ";
				int ms = it->first, count = it->second;
				message << count << "x" << ms << "ms";
			}

			message << ")";

			DEBUGF3("%s", message.str().c_str());
			sum = 0;
			breakdown.clear();
		}

#endif
		::Sleep(dmillis);
	}

	static bool EnumerateFilenames(const std::wstring& pattern, std::vector<std::wstring>& out_fileList)
	{
		WIN32_FIND_DATAW ffd;
		//@@: location CUTIL_ENUMERATEFILENAMES
		HANDLE hFind = FindFirstFileW(pattern.c_str(), &ffd);

		if (hFind == INVALID_HANDLE_VALUE)
		{
			DWORD error = GetLastError();

			if (error != ERROR_FILE_NOT_FOUND || AUTOTEST_FAIL)
			{
				char buffer[256];
				FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM, NULL, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), buffer, sizeof(buffer)-1, NULL);
				
				CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_STARTING_ENUMERATING_FILES, std::string(buffer));

				return false;
			}
			else
			{
				return true;
			}
		}

		do
		{
			if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				out_fileList.push_back(std::wstring(ffd.cFileName));
			}
		}
		while (FindNextFileW(hFind, &ffd) != 0);

		if (GetLastError() != ERROR_NO_MORE_FILES || AUTOTEST_FAIL)
		{
			CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_ENUMERATING_FILES);
			FindClose(hFind);
			return false;
		}
		else
		{
			FindClose(hFind);
			return true;
		}
	}

	static void PrintSystemErrorMessage(DWORD error)
	{
		char temp[1024];
		DWORD result = FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), temp, sizeof(temp), NULL);
		if (result != 0)
		{
			ERRORF("Last error: %x : %s", error, temp);
		}
		else
		{
			ERRORF("Last error: %x (unknown)", error);
		}
	}

	static bool DirectoryExists(const std::wstring& path)
	{
		DWORD attribs = GetFileAttributesW(path.c_str());
		return attribs != INVALID_FILE_ATTRIBUTES && attribs & FILE_ATTRIBUTE_DIRECTORY;
	}

	static bool FileExists(const std::string& filename)
	{
		std::wstring wfilename;
		StdStringToStdWString(filename, wfilename);

		std::ifstream fTest(wfilename.c_str());
		bool ret = fTest.good();
		DEBUGF3((ret ? "%s exists." : "%s doesn't exist"), filename.c_str());
		return ret;
	}

	static bool FileExists(const std::wstring& filename)
	{
		std::ifstream fTest(filename.c_str());
		bool ret = fTest.good();

		std::string sfilename;
		WStringToStdString(filename, sfilename);
		DEBUGF3((ret ? "%s exists." : "%s doesn't exist"), sfilename.c_str());
		return ret;
	}

	static void CreateDirectoryIfDoesntExist(const std::wstring& path)
	{
		if (DirectoryExists(path))
			return;
		//@@: location CUTIL_CREATEDIRECTORYIFDOESNTEXIST_CREATE_DIR
		CreateDirectoryW(path.c_str(), NULL);
	}

	static void CreateDirectoryHierarchyIfDoesntExist(const std::wstring& path)
	{
		size_t offset = 1;
		if (path.size() > 2 && path[1] == L':')
			offset = 3;

		while ((offset = path.find_first_of(L"\\/", offset)) != std::string::npos)
		{
			std::wstring leading = path.substr(0, offset);
			CreateDirectoryIfDoesntExist(leading);
			offset++;
		}
	}

	static u64 CurrentTimeMillis()
	{
		FILETIME ft;
		GetSystemTimeAsFileTime(&ft);

		ULARGE_INTEGER ulint;
		ulint.HighPart = ft.dwHighDateTime;
		ulint.LowPart = ft.dwLowDateTime;

		return ulint.QuadPart / 10000;
	}

	static bool StringStartsWith(const char* string, const char* prefix)
	{
		return strncmp(string, prefix, strlen(prefix)) == 0;
	}

	static void SplitString(const std::string& string, char delimeter, std::vector<std::string>& out_tokens)
	{
		std::stringstream ss(string);
		std::string item;
		while (std::getline(ss, item, delimeter))
		{
			out_tokens.push_back(item);
		}
	}

	static void BytesToHexString(const u8* byte_array, u32 length, std::string& out_string)
	{
		static const char* hexchars = "0123456789ABCDEF";

		out_string.clear();
		while (length)
		{
			u8 b = *byte_array;
			out_string += hexchars[b >> 4];
			out_string += hexchars[b % 16];

			byte_array++;
			length--;
		}
	}

	static void PrependTempoaryFolderPath(std::string& inout_path, const std::string& filename);
	static void PrependTempoaryFolderPath(std::wstring& inout_path, const std::wstring& filename);

	static void AppendStringEscapingQuotes(std::string& out_appendee, const std::string& appendage)
	{
		for (size_t i = 0; i < appendage.size(); i++)
		{
			char c = appendage[i];
			if (c == '"')
				out_appendee += "%22";
			else if (c == '\'')
				out_appendee += "%27";
			else if (c == '%')
				out_appendee += "%25";
			else
				out_appendee += c;
		}
	}

	static void GetEnvStr(std::string& out_string);
	static void GetPatchEnvStr(std::string& out_string);
	static void GetServerAndDocpathForEnv(std::string& out_server, std::string& out_docpath, const std::string& in_docpath);

#define DEBUG_LOCK_TIMINGS 0

	class LockTimings
	{
	public:
		static u64 Enter(CRITICAL_SECTION* cs)
		{
			return Instance().EnterImpl(cs);
		}

		static u64 Leave(CRITICAL_SECTION* cs)
		{
			return Instance().LeaveImpl(cs);
		}

		static void SetLockName(CRITICAL_SECTION* cs, const char* name)
		{
			Instance().SetLockNameImpl(cs, name);
		}

		static void PrintTimings()
		{
			Instance().PrintTimingsImpl();
		}

	private:
		CRITICAL_SECTION m_selfLock;
		struct LockInfo
		{
			u64 totalWaitMillis;
			u64 totalHoldMillis;
			u64 peakWaitMillis;
			u64 peakHoldMillis;
			u64 waitCount;
			u64 holdCount;

			u64 acquireTime;
			const char* name;

			LockInfo() : totalWaitMillis(0), totalHoldMillis(0), peakWaitMillis(0), peakHoldMillis(0), waitCount(0), holdCount(0), acquireTime(0), name(NULL) {}
		};

		typedef std::map<void*,LockInfo> infomap_t;
		infomap_t m_lockInfoMap;

		u64 m_firstLockTime;

		static LockTimings& Instance()
		{
			static LockTimings self;
			return self;
		}

		LockTimings() : m_firstLockTime(0)
		{
			InitializeCriticalSection(&m_selfLock);
		}
		~LockTimings()
		{
			DeleteCriticalSection(&m_selfLock);
		}

		LockInfo& GetLockInfo(void* lock)
		{
			return m_lockInfoMap[lock];
		}

		u64 EnterImpl(CRITICAL_SECTION* cs)
		{
			u64 before = CurrentTimeMillis();
			EnterCriticalSection(cs);
			u64 acquireTime = CurrentTimeMillis();
			u64 wait = acquireTime-before;

			EnterCriticalSection(&m_selfLock);
			LockInfo& info = GetLockInfo(cs);
			info.totalWaitMillis += wait;
			info.waitCount++;
			info.acquireTime = acquireTime;
			if (info.peakWaitMillis < wait)
				info.peakWaitMillis = wait;
			if (m_firstLockTime == 0)
				m_firstLockTime = acquireTime;
			LeaveCriticalSection(&m_selfLock);

			return wait;
		}

		u64 LeaveImpl(CRITICAL_SECTION* cs)
		{
			u64 leaveTime = CurrentTimeMillis();

			EnterCriticalSection(&m_selfLock);
			LockInfo& info = GetLockInfo(cs);
			
			u64 holdTime = 0;
			if (info.acquireTime != 0)
			{
				holdTime = leaveTime - info.acquireTime;
				info.totalHoldMillis += holdTime;
				if (info.peakHoldMillis < holdTime)
					info.peakHoldMillis = holdTime;
			}
			info.holdCount++;
			LeaveCriticalSection(&m_selfLock);

			LeaveCriticalSection(cs);

			return holdTime;
		}

		void SetLockNameImpl(CRITICAL_SECTION* cs, const char* name)
		{
			EnterCriticalSection(&m_selfLock);
			GetLockInfo(cs).name = name;
			LeaveCriticalSection(&m_selfLock);
		}

		void PrintTimingsImpl()
		{
#if DEBUG_LOCK_TIMINGS
			DISPLAYF("Lock timings (ms) follow:");
			DISPLAYF("Address,            Total wait, Total hold, Peak wait, Peak hold, Wait count, Hold count, Name");
			EnterCriticalSection(&m_selfLock);
			u64 totalWait = 0, totalHold = 0, peakWait = 0, peakHold = 0, waitCount = 0, holdCount = 0;
			for (infomap_t::iterator it = m_lockInfoMap.begin(); it != m_lockInfoMap.end(); it++)
			{
				void* addr = it->first;
				LockInfo& info = it->second;

				if (info.totalWaitMillis > 0 || info.totalHoldMillis > 0)
				{
					totalWait += info.totalWaitMillis;
					totalHold += info.totalHoldMillis;

					waitCount += info.waitCount;
					holdCount += info.holdCount;

					if (info.peakWaitMillis > peakWait)
						peakWait = info.peakWaitMillis;

					if (info.peakHoldMillis > peakHold)
						peakHold = info.peakHoldMillis;

					DISPLAYF("\"0x%p\", %8llu, %8llu, %8llu, %8llu, %8llu, %8llu, \"%s\"", addr, info.totalWaitMillis, info.totalHoldMillis, info.peakWaitMillis, info.peakHoldMillis, info.waitCount, info.holdCount, info.name ? info.name : "(unnamed)");
				}
			}

			DISPLAYF("\"Totals\",           %8llu, %8llu, %8llu, %8llu, %8llu, %8llu, \"Totals\"", totalWait, totalHold, peakWait, peakHold, waitCount, holdCount);

			if (m_firstLockTime != 0)
			{
				DISPLAYF("\"Run duration\", %8llu", CurrentTimeMillis() - m_firstLockTime);
			}

			LeaveCriticalSection(&m_selfLock);
#endif
		}
	};

	class AutoCritSec
	{
	public:
		AutoCritSec(CRITICAL_SECTION& lpcs) : m_lpcs(&lpcs)
		{
#if DEBUG_LOCK_TIMINGS
			u64 wait = LockTimings::Enter(m_lpcs);

			if (wait > 1000)
			{
				ERRORF("Waiting %llu ms for lock!", wait);
			}
			else if (wait > 100)
			{
				ERRORF("Waiting %llu ms for lock!", wait);
			}
			else if (wait > 10)
			{
				WARNINGF("Waiting %llu ms for lock!", wait);
			}
			else if (wait > 1)
			{
				WARNINGF("Waiting %llu ms for lock!", wait);
			}
#else
			EnterCriticalSection(m_lpcs);
#endif
		}
		~AutoCritSec()
		{
#if DEBUG_LOCK_TIMINGS
			u64 heldTime = LockTimings::Leave(m_lpcs);
			if (heldTime > 1000)
			{
				ERRORF("Held lock for %llu ms!", heldTime);
			}
			else if (heldTime > 100)
			{
				ERRORF("Held lock for %llu ms!", heldTime);
			}
			else if (heldTime > 10)
			{
				WARNINGF("Held lock for %llu ms!", heldTime);
			}
			else if (heldTime > 2)
			{
				DEBUGF3("Held lock for %llu ms!", heldTime);
			}
#else
			LeaveCriticalSection(m_lpcs);
#endif
		}
	private:
		LPCRITICAL_SECTION m_lpcs;
	};

#define AUTOCRITSEC(x) CUtil::AutoCritSec lock(x)
};


class CFileWrapper
{
public:
	static HANDLE CreateFileA(
		_In_ LPCSTR lpFileName,
		_In_ DWORD dwDesiredAccess,
		_In_ DWORD dwShareMode,
		_In_opt_ LPSECURITY_ATTRIBUTES lpSecurityAttributes,
		_In_ DWORD dwCreationDisposition,
		_In_ DWORD dwFlagsAndAttributes,
		_In_opt_ HANDLE hTemplateFile
		);

	static BOOL DeleteFileA(
		_In_ LPCSTR lpFileName
		);

	static BOOL SetFileAttributesA(
		_In_ LPCSTR lpFileName,
		_In_ DWORD dwFileAttributes
		);

	static BOOL GetDiskFreeSpaceExA(
		_In_opt_ LPCSTR lpDirectoryName,
		_Out_opt_ PULARGE_INTEGER lpFreeBytesAvailableToCaller,
		_Out_opt_ PULARGE_INTEGER lpTotalNumberOfBytes,
		_Out_opt_ PULARGE_INTEGER lpTotalNumberOfFreeBytes
		);

	static int remove(const char *path);
};