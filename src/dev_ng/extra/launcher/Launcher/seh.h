// 
// diag/seh.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DIAG_SEH_H
#define DIAG_SEH_H

#include <assert.h>

/*  Macros for structured error handling

    Structed error handling is intended to look like exception handling,
    but its real benefit is in being able to handle errors in a structured
    way while keeping code as readable as possible.
*/

#ifdef __SNC__
#define SNC_PRAGMA(x)	_Pragma(x)
#else
#define SNC_PRAGMA(x)
#endif

#define rtry	SNC_PRAGMA("diag_suppress=129")

//Handles an error by performing action then jumping to dest.
//The destination is defined on a subsequent line by rcatch(dest).
//Action is typically an error printing statement.
//Example:
//rtry
//{
//    if(!someCondition)
//    {
//        throw(foo, Print("someCondition was false"));
//    }
//}
//rcatch(foo)
//{
//    HandleError();
//}
#define rthrow(dest, action) do{action;goto rexcept_##dest;}while(0);

//Checks a condition and performs an rthrow if the condition is false.
//Example:
//rtry
//{
//    rcheck(someCondition, foo, Print("someCondition was false"));
//}
//rcatch(foo)
//{
//    HandleError();
//}
#define rcheck(cond, dest, action)\
	do{if(!(cond)||AUTOTEST_FAIL){rthrow(dest, action);}}while(0);

//Checks a condition and asserts and performs an rthrow if the
//condition is false.
//Example:
//rtry
//{
//    rverify(someCondition, foo, Print("someCondition was false"));
//}
//rcatch(foo)
//{
//    HandleError();
//}
#define rverify(cond, dest, action)\
	do{if(!(cond)||AUTOTEST_FAIL){do{action;}while(0); assert(#cond); rthrow(dest,);}}while(0);

//Defines the destination for an rthrow.  See above examples.
#define rcatch(label)\
	while(0)\
	rexcept_##label: SNC_PRAGMA("diag_default=129")

//Convenience macro for defining the catchall destination for throws.
//Example:
//rtry
//{
//    rverify(someCondition, catchall, Print("someCondition was false"));
//}
//rcatchall
//{
//    HandleError();
//}
#define rcatchall rcatch(catchall)

#endif  //DIAG_SEH_H

