#pragma once

#include "IPatchCheck.h"

#include <string>

class MtlInstallCheck : public IPatchCheck
{
public:
	MtlInstallCheck(void);
	~MtlInstallCheck(void);

	virtual void ParseXML();
	virtual Version GetExecutableVersion();

	virtual void DownloadComplete(DownloadCompleteStatus status, std::string filename);
	virtual void DownloadError(const CError& errorString);

	static bool GetMtlVersion(Version& out_version);
	static bool IsRecentMtlInstalled();
	static bool LaunchMtl();

private:
	static bool GetMtlExePath(std::string& out_path, const char* filename);
};