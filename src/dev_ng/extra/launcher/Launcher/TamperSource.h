#ifndef _TAMPERSOURCE_H__
#define _TAMPERSOURCE_H__
#include <iostream>

#if !(RSG_PRELOADER_BOOTSTRAP || RSG_PRELOADER_LAUNCHER)
static const u32 kIVSize = 16;
static const u32 kAESKeySize = 32;
static const u8 entitlementAESKey[kAESKeySize] = {
	0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
	0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF,
	0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
	0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF
};


#define ENABLE_TAMPER_ACTIONS 0
#if ENABLE_TAMPER_ACTIONS
static volatile int tamperLength;
static volatile int g_bShowWindow = false;
static float g_eatDupKeyProb = 0.00f;
static unsigned int g_readDiff = 0;
static int g_stateOffset = 0;
static float g_flickerProb = 0.0f;
static bool g_bBringToFront = false;
__declspec(noinline) void TamperAction_Download(int);
__declspec(noinline) void TamperAction_PopUp(int);
__declspec(noinline) void TamperAction_DupKeyboard(int);
__declspec(noinline) void TamperAction_HashFail(int);
__declspec(noinline) void TamperAction_AlterState(int);
__declspec(noinline) void TamperAction_Flicker(int);
__declspec(noinline) void TamperAction_BringToFront(int);
__declspec(noinline) void TamperAction_DamageCode(int);
__declspec(noinline) void TamperAction_DamageCodeTwo(int);
__declspec(noinline) void TamperAction_NoAction(int);
__declspec(noinline) void TamperAction_RedHerring(int);
__declspec(noinline) void InitTamperCallsToNop(bool);
#endif
__declspec(noinline) void RedHerringEntitlement();
__declspec(noinline) void RedHerringEntitlementLoad(u8** pData, u32& dataSize);
__declspec(noinline) void RedHerringEntitlementWrite();
__declspec(noinline) std::wstring GetEntitlementFilePath();
#endif

#endif
