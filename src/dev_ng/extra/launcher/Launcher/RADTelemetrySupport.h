#pragma once

#define ENABLE_RAD_TELEMETRY_SUPPORT 0 && !RSG_FINAL

#if ENABLE_RAD_TELEMETRY_SUPPORT

#include "Types.h"
#include "telemetry.h"

#define RAD_TELEMETRY_INIT CRadTelemetrySupport::InitTelemetry
#define RAD_TELEMETRY_ENTER(...) tmEnter(CRadTelemetrySupport::GetContext(), TMZF_NONE, __VA_ARGS__)
#define RAD_TELEMETRY_LEAVE(...) tmLeave(CRadTelemetrySupport::GetContext())
#define RAD_TELEMETRY_ZONE(...) tmZone(CRadTelemetrySupport::GetContext(), TMZF_NONE, __VA_ARGS__)
#define RAD_TELEMETRY_WAIT_ZONE(name) CRadTelemetrySupport::UiLockHelper TMLINENAME(lockhelper)(name)
#define RAD_TELEMETRY_SHUTDOWN CRadTelemetrySupport::ShutdownTelemetry

class CRadTelemetrySupport
{
public:
	static void InitTelemetry();
	static void ShutdownTelemetry();

	static HTELEMETRY GetContext();

	class UiLockHelper
	{
	public:
		UiLockHelper(const char* name);
		~UiLockHelper();
	};

private:
	static HTELEMETRY sm_context;
	static bool sm_running;
	static bool sm_inited;
	static bool sm_shutdown;
	static u8* sm_memory;

	static UINT TelemetryThreadEntry(void*);
	volatile static LONG sm_waitZonesEntered;
	static CRITICAL_SECTION sm_critsec;
	static HANDLE sm_waitStartSema;
	static HANDLE sm_waitEndSema;
};


#else

#define RAD_TELEMETRY_INIT(...)
#define RAD_TELEMETRY_ENTER(...)
#define RAD_TELEMETRY_LEAVE(...)
#define RAD_TELEMETRY_ZONE(...)
#define RAD_TELEMETRY_WAIT_ZONE(name)
#define RAD_TELEMETRY_SHUTDOWN(...)

#endif

