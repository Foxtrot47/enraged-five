#include "stdafx.h"

#define DEFINE_CONFIG_OPTIONS
#include "Config.h"
#include "Globals.h"
#include "Channel.h"
#include "Util.h"
#include "resource.h"
#include "WhiteList.h"

#include <map>

#include "../tinyxml/tinyxml.h"

CConfigOption::CConfigOption(const char* name)
	: m_valueSet(false), m_name(name)
{
	//@@: range CCONFIGOPTION_CONSTRUCTOR_RANGE {
	//@@: location CCONFIGOPTION_CONSTRUCTOR
	GetOptionsNonConst().push_back(this); 
	//@@: } CCONFIGOPTION_CONSTRUCTOR_RANGE

}

std::vector<CConfigOption*>& CConfigOption::GetOptionsNonConst()
{
	static std::vector<CConfigOption*> s_options;
	return s_options;
}

const std::vector<CConfigOption*>& CConfigOption::GetOptions()
{
	return GetOptionsNonConst();
}

std::vector<std::string>& CConfigOption::GetDependencyListNonConst()
{
	static std::vector<std::string> s_dependencies;
	return s_dependencies;
}

const std::vector<std::string>& CConfigOption::GetDependencyList()
{
	//@@: range CCONFIGOPTION_GETDEPENDENCYLIST {
	//@@: location CCONFIGOPTION_GETDEPENDENCYLIST_CALL
	return GetDependencyListNonConst(); 
	//@@: } CCONFIGOPTION_GETDEPENDENCYLIST

}

const char* CConfigOption::GetName() const
{
	return m_name;
}

bool CConfigOption::LoadOptions(int resID)
{
	// Make a map of the options, for faster lookup
	std::map<std::string, CConfigOption*> optionMap;

	const std::vector<CConfigOption*>& opts = GetOptions();
	for (std::vector<CConfigOption*>::const_iterator it = opts.begin(); it != opts.end(); it++)
	{
		CConfigOption* opt = *it;

		if (opt)
		{
			optionMap[std::string(opt->GetName())] = opt;
		}
		else
		{
			ERRORF("Option is null...");
		}
	}


	// Load and parse the XML
	TiXmlDocument doc;

#if !RSG_FINAL
	if (Constants::EnableLoadingConfigFromFile)
	{
		doc.LoadFile("./launcher_config.xml");
	}

	//@@: range CCONFIGOPTION_LOADOPTIONS_ERROR_LOADING_FILE {
	//@@: location CCONFIGOPTION_LOADOPTIONS_CHECK_ERROR_LOADING_FILE
	if ((!Constants::EnableLoadingConfigFromFile || doc.Error()))
	{
		if (!Constants::EnableLoadingConfigFromResources)
		{
			ERRORF("Can't load config XML from file!");
			return false;
		}
#endif
		HRSRC resource = FindResource(NULL, MAKEINTRESOURCE(resID), L"Xml");
		if (resource == NULL)
		{
			ERRORF("Can't find config XML resource!");
			CUtil::PrintSystemErrorMessage(GetLastError());
			return false;
		}
		//@@: location CCONFIGOPTION_LOADOPTIONS_LOAD_RESOURCE
		HGLOBAL memory = LoadResource(NULL, resource);
		if (memory == NULL)
		{
			ERRORF("Can't load config XML resource!");
			CUtil::PrintSystemErrorMessage(GetLastError());
			return false;
		}

		DWORD size = SizeofResource(NULL, resource);
		if (size == 0)
		{
			ERRORF("Zero-length config XML resource!");
			CUtil::PrintSystemErrorMessage(GetLastError());
			return false;
		}
		//@@: location CCONFIGOPTION_LOADOPTIONS_LOCK_PTR
		LPVOID ptr = LockResource(memory);
		if (ptr == NULL)
		{
			ERRORF("Can't lock config XML resource!");
			CUtil::PrintSystemErrorMessage(GetLastError());
			return false;
		}

		char* copy = new char[size+1];
		memcpy(copy, ptr, size);
		copy[size] = '\0';

		doc.Parse(copy);

		delete[] copy;
#if !RSG_FINAL
	}
#endif
	//@@: } CCONFIGOPTION_LOADOPTIONS_ERROR_LOADING_FILE


	//@@: range CCONFIGOPTION_LOADOPTIONS_GET_HANDLE_AND_INITIAL_PARSE {
	TiXmlHandle docHandle(&doc);

	//@@: location CCONFIGOPTION_LOADOPTION_LOAD_FIRST_CHILD
	TiXmlElement* opt = docHandle.FirstChild("Options").FirstChild().ToElement();

	//@@: } CCONFIGOPTION_LOADOPTIONS_GET_HANDLE_AND_INITIAL_PARSE

	//@@: range CCONFIGOPTION_LOADOPTIONS_PARSING_FILE {

	for (; opt; opt = opt->NextSiblingElement())
	{
		if (strcmp(opt->Value(), "Option") == 0)
		{
			// This is an option
			//@@: location CCONFIGOPTION_LOADOPTIONS_GETOPTION
			const char* optName = opt->Attribute("name");
			const char* optValue = opt->Attribute("value");
			if (!optName || !optValue)
			{
				ERRORF("Invalid option found!");
				continue;
			}

			std::string strName(optName);

			std::map<std::string, CConfigOption*>::iterator location = optionMap.find(strName);
			if (location != optionMap.end())
			{
				DEBUGF1("Config: %32s = \"%s\"", optName, optValue);
				location->second->Parse(optValue);
				optionMap.erase(location);
			}
			else
			{
				DEBUGF1("Option specified in XML does not exist: name=\"%s\" value=\"%s\"", optName, optValue);
			}
		}
#if !RSG_FINAL
		else if (strcmp(opt->Value(), "WhitelistEntry") == 0)
		{
			// This is a whitelist entry
			const char* entryValue = opt->Attribute("value");
			if (!entryValue)
			{
				ERRORF("Invalid whitelist entry!");
				continue;
			}

			// Add it
			DEBUGF1("Config: %32s : \"%s\"", "Whitelist entry", entryValue);
			WhiteList::Add(entryValue);
		}
#endif
		else if (strcmp(opt->Value(), "Dependency") == 0)
		{
			// This is a dependency
			const char* entryValue = opt->Attribute("value");
			if (!entryValue)
			{
				ERRORF("Invalid dependency list entry!");
				continue;
			}

			// Add it
			//@@: location CCONFIGOPTION_LOADOPTIONS_PUSHING_DEPENDENCY
			DEBUGF1("Config: %32s : \"%s\"", "Dependency", entryValue);
			GetDependencyListNonConst().push_back(entryValue);
		}
	}
	//@@: } CCONFIGOPTION_LOADOPTIONS_PARSING_FILE


	//@@: range CCONFIGOPTION_LOADOPTIONS_DETERMINE_UNSPECIFIED_OPTIONS {
	// Check we've got them all
	//@@: location CCONFIGOPTION_LOADOPTIONS_DETERMINE_UNSPECIFIED_OPTIONS_LOCATION
	bool allFound = true;
	for (std::map<std::string, CConfigOption*>::iterator it = optionMap.begin(); it != optionMap.end(); it++)
	{
		DEBUGF1("Option not specified in XML: %s", it->first.c_str());
		allFound = false;
	}
	//@@: } CCONFIGOPTION_LOADOPTIONS_DETERMINE_UNSPECIFIED_OPTIONS


	return allFound;
}
