#include "stdafx.h"

#include "Channel.h"
#include "CloudSaveManager.h"
#include "Globals.h"
#include "JsonWriter.h"
#include "LauncherDlg.h"
#include "OnlineConfig.h"
#include "seh.h"
#include "SocialClub.h"
#include "Util.h"

namespace rlTelemetryPolicies
{
	enum
	{
		DEFAULT_LOG_CHANNEL,
	};
}

const int SECONDS_BEFORE_CANCEL_BUTTON = 30;
const int SECONDS_BEFORE_PROGRESS = 10;

#include "../../../game/network/Live/NetworkMetricsChannels.h"

static CloudSaveManager* g_cloudSaveManager = NULL;

typedef rgsc::ICloudSaveFileLatestVersion::ConflictState ConflictState;
typedef rgsc::ICloudSaveFileLatestVersion::ConflictResolutionType ConflictResolutionType;

template< typename T >
inline bool BIT_ISSET( const unsigned bit, const T a ) { return !!( a & ( T( 1 ) << bit ) ); }

class Gta5CloudSaveConfiguration : public rgsc::ICloudSaveTitleConfigurationV1
{
public:
	Gta5CloudSaveConfiguration(SocialClub* sc) : m_socialClub(sc)
	{
	}

	// PURPOSE
	//	Returns the title name for the configuration
	virtual const char* RGSC_CALL GetTitleName()
	{
		return "gta5";
	}

	// PURPOSE
	//	Returns the number of cloud save files in the title configuration
	virtual u32 RGSC_CALL GetNumCloudFiles()
	{
		return 16;
	}

	// PURPOSE
	//	Returns the file name of the configured file at a given index
	virtual const char* RGSC_CALL GetFileName(unsigned index)
	{
		switch (index)
		{
		case  0: return "SGTA50000";
		case  1: return "SGTA50001";
		case  2: return "SGTA50002";
		case  3: return "SGTA50003";
		case  4: return "SGTA50004";
		case  5: return "SGTA50005";
		case  6: return "SGTA50006";
		case  7: return "SGTA50007";
		case  8: return "SGTA50008";
		case  9: return "SGTA50009";
		case 10: return "SGTA50010";
		case 11: return "SGTA50011";
		case 12: return "SGTA50012";
		case 13: return "SGTA50013";
		case 14: return "SGTA50014";
		case 15: return "SGTA50015";
		default: return NULL;
		}
	}

	// PURPOSE
	//	Returns the directory that houses the per-profile/per-title save data
	virtual const char* RGSC_CALL GetSaveDirectory()
	{
		static char buffer[rgsc::RGSC_MAX_PATH];
		m_socialClub->GetFileSystem()->GetTitleProfileDirectory(buffer, true);

		return buffer;
	}

	rgsc::RGSC_HRESULT RGSC_CALL QueryInterface(rgsc::RGSC_REFIID riid, void** ppvObj)
	{
		if(ppvObj == NULL)
		{
			return rgsc::RGSC_INVALIDARG;
		}

		if(riid == rgsc::IID_IRgscUnknown)
		{
			*ppvObj = static_cast<rgsc::ICloudSaveTitleConfiguration*>(this);
		}
		else if(riid == rgsc::IID_ICloudSaveTitleConfigurationV1)
		{
			*ppvObj = static_cast<rgsc::ICloudSaveTitleConfigurationV1*>(this);
		}
		else
		{
			*ppvObj = NULL;
			return rgsc::RGSC_NOINTERFACE;
		}

		return rgsc::RGSC_OK;
	}

	virtual const char* RGSC_CALL GetHardwareId()
	{
		if (m_hardwareId.empty())
		{
			m_hardwareId = "Unknown Machine";

			wchar_t buffer[MAX_COMPUTERNAME_LENGTH+1];
			DWORD bufferSize = MAX_COMPUTERNAME_LENGTH+1;

			if (GetComputerNameW(buffer, &bufferSize) == 0)
			{
				if (GetLastError() == ERROR_BUFFER_OVERFLOW && bufferSize > 1 && bufferSize <= rgsc::ICloudSaveManager::HARDWARE_ID_MAX_BUF_SIZE)
				{
					wchar_t* heapBuff = new wchar_t[bufferSize];
					if (GetComputerNameW(heapBuff, &bufferSize))
					{
						CUtil::WStringToStdString(std::wstring(heapBuff), m_hardwareId);
					}
				}
			}
			else if (bufferSize > 0)
			{
				CUtil::WStringToStdString(std::wstring(buffer), m_hardwareId);
			}
		}

		return m_hardwareId.c_str();
	}

private:
	SocialClub* m_socialClub;
	std::string m_hardwareId;

};

Gta5CloudSaveConfiguration* g_gta5CloudSaveConfig = NULL;

CloudSaveManager* CloudSaveManager::Instance()
{
	if (!g_cloudSaveManager)
	{
		ERRORF("Cloud Save Manager not yet initialized.");
		return NULL;
	}

	return g_cloudSaveManager;
}

void CloudSaveManager::Initialize(SocialClub* sc)
{
	if (g_cloudSaveManager)
	{
		ERRORF("Cloud Save Manager initialized a second time!");
		return;
	}

	g_gta5CloudSaveConfig = new Gta5CloudSaveConfiguration(sc);
	g_cloudSaveManager = new CloudSaveManager(sc);

	g_cloudSaveManager->SendCloudSaveConfigurationToSDK();
}

void CloudSaveManager::Shutdown()
{
	delete g_cloudSaveManager;
	g_cloudSaveManager = NULL;
}

CloudSaveManager::CloudSaveManager(SocialClub* sc) : m_socialClub(sc), m_cloudSaveManifest(NULL), m_cloudSaveOperationProgress(NULL), m_cloudSaveOperationProgressV2(NULL), m_asyncStatus(NULL), m_state(eState_None), m_swapFileIndex(0), m_cloudSavesEnabled(false), 
													m_exitAfterSync(false), m_RockstarId(-1), m_hasBetaAccess(false), m_hasCheckedForBetaAccess(false), m_uStartTime(0), m_uSyncStartTime(0), m_uResolutionStartTime(0), 
													m_uResolutionEndTime(0), m_NumBackupFiles(0), m_uBackupRestoreStartTime(0), m_uBackupRestoreEndTime(0), m_shownCancelButton(false), m_cancelled(false), m_timedOut(false),
													m_operationProgress (0), m_lastReportedOperationProgress(0), m_syncCount(0)
{
}

CloudSaveManager::~CloudSaveManager()
{
	if (m_socialClub)
	{
		if (m_cloudSaveManifest)
		{
			m_socialClub->GetCloudSaveManager()->UnregisterTitle(m_cloudSaveManifest);
			m_cloudSaveManifest = NULL;
		}

		if (m_asyncStatus)
		{
			if (m_asyncStatus->Pending())
			{
				m_asyncStatus->Cancel();
			}

			m_asyncStatus->Release();
			m_asyncStatus = NULL;
		}

		if (OnlineConfig::CloudSaveWorkerThread)
		{
			m_socialClub->GetCloudSaveManager()->ShutdownWorkerThread();
		}
	}
}

void CloudSaveManager::ScUpdate()
{
	switch (m_state)
	{
	case eState_None:
		break;
	case eState_WaitingToCreateAsyncStatus:
		if (TryCreateAsyncStatus())
		{
			NotifyUiOfSync();

			if (m_socialClub && m_socialClub->IsBanned())
			{
				OnBanned();
			}
			else if (OnlineConfig::CloudSaveBetaOnly)
			{
				StartBetaCheck();
			}
			else
			{
				StartEnabledCheck();
			}
		}
		else
		{
			OnError();
		}
		break;
	case eState_WaitingForBetaCheck:
		if (m_asyncStatus && !m_asyncStatus->Pending())
		{
			if (m_asyncStatus->Succeeded())
			{
				OnBetaCheckComplete();
			}
			else
			{
				OnError();
			}
		}
		break;
	case eState_WaitingForEnabledCheck:
		break;
	case eState_WaitingForBackupSearch:
		if (m_asyncStatus && !m_asyncStatus->Pending())
		{
			if (m_asyncStatus->Succeeded())
			{
				OnBackupSearchComplete();
			}
			else
			{
				OnError();
			}
		}
		break;
	case eState_WaitingForBackupResolutions:
		break;
	case eState_WaitingForBackupRestoration:
		if (m_asyncStatus && !m_asyncStatus->Pending())
		{
			if (m_asyncStatus->Succeeded())
			{
				StartRestoringNextBackup();
			}
			else
			{
				OnError();
			}
		}
		break;
	case eState_WaitingForModificationsList:
		if (m_asyncStatus && !m_asyncStatus->Pending())
		{
			if (m_asyncStatus->Succeeded())
			{
				OnModificationListComplete();
			}
			else
			{
				OnError();
			}
		}
		break;
	case eState_WaitingForConflictMetadata:
		if (m_asyncStatus && !m_asyncStatus->Pending())
		{
			if (m_asyncStatus->Succeeded())
			{
				OnConflictMetadataDownloaded();
			}
			else
			{
				OnError();
			}
		}
		break;
	case eState_WaitingForConflictResolutions:
		break;
	case eState_WaitingForBackupCreation:
		if (!m_asyncStatus->Pending())
		{
			DEBUGF1("eState_WaitingForBackupCreation: %s", m_asyncStatus->Succeeded() ? "Succeeded" : "Failed");
			if (m_asyncStatus->Succeeded())
			{
				m_state = eState_WaitingForFileUploads;
				NotifyUiOfSync();
				m_socialClub->GetCloudSaveManager()->PostUpdatedFiles(m_cloudSaveManifest, m_asyncStatus);
			}
			else
			{
				OnError();
			}
		}
		break;
	case eState_WaitingForFileUploads:
		UpdateProgress();
		if (!m_asyncStatus->Pending())
		{
			DEBUGF1("eState_WaitingForFileUploads: %s", m_asyncStatus->Succeeded() ? "Succeeded" : "Failed");
			if (m_asyncStatus->Succeeded())
			{
				m_state = eState_WaitingForFileDownloads;
				NotifyUiOfSync();
				m_socialClub->GetCloudSaveManager()->GetUpdatedFiles(m_cloudSaveManifest, m_asyncStatus);
			}
			else
			{
				OnError();
			}
		}
		break;
	case eState_WaitingForFileDownloads:
		UpdateProgress();
		if (!m_asyncStatus->Pending())
		{
			DEBUGF1("eState_WaitingForFileDownloads: %s", m_asyncStatus->Succeeded() ? "Succeeded" : "Failed");
			if (m_asyncStatus->Succeeded())
			{
				m_state = eState_WaitingForFileDeletions;
				NotifyUiOfSync();
				m_socialClub->GetCloudSaveManager()->DeleteUpdatedfiles(m_cloudSaveManifest, m_asyncStatus);
			}
			else
			{
				OnError();
			}
		}
		break;
	case eState_WaitingForFileDeletions:
		if (!m_asyncStatus->Pending())
		{
			DEBUGF1("eState_WaitingForFileDeletions: %s", m_asyncStatus->Succeeded() ? "Succeeded" : "Failed");
			if (m_asyncStatus->Succeeded())
			{
				m_state = eState_SwappingDownloadedFiles;
				NotifyUiOfSync();
				m_socialClub->GetCloudSaveManager()->CopyUpdatedFiles(m_cloudSaveManifest, m_asyncStatus);
			}
			else
			{
				OnError();
			}
		}
		break;
	case eState_SwappingDownloadedFiles:
		if (!m_asyncStatus->Pending())
		{
			DEBUGF1("eState_SwappingDownloadedFiles: %s", m_asyncStatus->Succeeded() ? "Succeeded" : "Failed");
			if (m_asyncStatus->Succeeded())
			{
				m_cloudSaveManifest->Save();
				OnFinishCloudSync();
			}
			else
			{
				OnError();
			}
		}
		break;
	default:
		break;
	}


	if (!m_shownCancelButton)
	{
		if (CUtil::CurrentTimeMillis() > m_uSyncStartTime + SECONDS_BEFORE_CANCEL_BUTTON * 1000
			&& m_state != eState_WaitingForBackupResolutions
			&& m_state != eState_WaitingForConflictResolutions
			&& m_state != eState_WaitingForEnabledCheck
			&& m_state != eState_None)
		{
			NotifyUiOfSync();
		}
	}
	else
	{
		if (m_state != eState_None && m_cancelled)
		{
			if (m_asyncStatus && m_asyncStatus->Pending())
			{
				m_asyncStatus->Cancel();
			}

			OnFinishCloudSync();
		}
	}

	UpdateSyncMessage();
}

void CloudSaveManager::OnSignOut()
{
	DEBUGF1("CloudSaveManager::OnSignOut");
	if (m_asyncStatus)
	{
		m_asyncStatus->Cancel();
		m_asyncStatus->Release();
		m_asyncStatus = NULL;
	}

	if (m_cloudSaveManifest)
	{
		m_cloudSaveManifest->Clear();
	}

	if (m_state != eState_None)
	{
		OnError();
	}

	m_state = eState_None;
	m_cloudSavesEnabled = false;
	m_hasCheckedForBetaAccess = false;
	m_hasBetaAccess = false;
	m_RockstarId = -1;
}

void CloudSaveManager::OnSignIn(rgsc::RockstarId rockstarId)
{
	DEBUGF1("CloudSaveManager::OnSignIn");

	bool bSignedIntoNewAccount = m_RockstarId != rockstarId && rockstarId != -1;
	bool bValidStateToRecheck = m_cloudSaveManifest && m_state == eState_None;
	if (bValidStateToRecheck && bSignedIntoNewAccount)
	{
		m_cloudSaveManifest->SetCloudSavesEnabled(rgsc::ICloudSaveManifestV2::CloudSavesEnabledState::UNKNOWN);

		// Load the manifest
		m_cloudSaveManifest->Load();

		// Check if cloud is enabled for this account.
		if (m_cloudSaveManifest->GetCloudSaveEnabled() == rgsc::ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES)
		{
			m_cloudSavesEnabled = true;
		}
	}

	m_RockstarId = rockstarId;
}

void CloudSaveManager::StartCloudSync(bool exitAfterSync)
{
	DEBUGF1("CloudSaveManager::StartCloudSync(%s)", exitAfterSync ? "true" : "false");

	m_shownCancelButton = false;
	m_cancelled = false;
	m_timedOut = false;
	m_uStartTime = m_uSyncStartTime = CUtil::CurrentTimeMillis();
	m_uResolutionStartTime = 0;
	m_uResolutionEndTime = 0;
	m_lastReportedSyncIndex = -1;
	m_lastReportedOperationProgress = 0;
	m_lastReportedSyncState = eState_None;

	m_NumBackupFiles = 0;
	m_uBackupRestoreStartTime = 0;
	m_uBackupRestoreEndTime = 0;

	// The maximum size of a file - could have been updated by online config
    if (m_socialClub && m_socialClub->GetCloudSaveManager())
	{
		m_socialClub->GetCloudSaveManager()->SetMaximumFileSize(OnlineConfig::CloudSaveMaxFileSize);
	}

	// Reset when switching from pre-sync to post sync
	if (m_exitAfterSync != exitAfterSync)
	{
		m_syncCount = 0;
	}
	
	// Increment with each new sync - should be '1' for the first.
	m_syncCount++;
	m_exitAfterSync = exitAfterSync;

	if (m_hasCheckedForBetaAccess && !m_hasBetaAccess)
	{
		DEBUGF1("Already checked for beta access (user does not have access).");
		return;
	}

	if (m_state != eState_None)
	{
		ERRORF("Cloud Save sync already in progress!");
		return;
	}

	if (m_cloudSaveManifest)
	{
		//	Clear the Manifest, Enabled State and load from disk - could have been disabled in game between syncs
		m_cloudSavesEnabled = false;
		m_cloudSaveManifest->Clear();
		m_cloudSaveManifest->SetCloudSavesEnabled(rgsc::ICloudSaveManifestV2::CloudSavesEnabledState::INVALID);

		// reload manifest from disk
		m_cloudSaveManifest->Load();

		// Check if cloud is enabled for this account.
		if (m_cloudSaveManifest->GetCloudSaveEnabled() == rgsc::ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES)
		{
			m_cloudSavesEnabled = true;
		}

		// If cloud saves are not enabled, early out
		if (!m_cloudSavesEnabled && exitAfterSync)
		{
			OnFinishCloudSync();
		}
	}

	if (m_asyncStatus != NULL || TryCreateAsyncStatus())
	{
		if (m_socialClub && m_socialClub->IsBanned())
		{
			OnBanned();
		}
		else if (OnlineConfig::CloudSaveBetaOnly)
		{
			StartBetaCheck();
		}
		else
		{
			StartEnabledCheck();
		}
	}
	else
	{
		m_state = eState_WaitingToCreateAsyncStatus;
	}
}

void CloudSaveManager::CancelCloudSync()
{
	m_cancelled = true;
}

void CloudSaveManager::DisableCloudSaves()
{
	if (m_cloudSaveManifest)
	{
		// Get the most up to date version from disk - this will overwrite the local copy if the manifest exists on disk
		m_cloudSaveManifest->Load();

		// Update the cloud saves enabled state and save to disk
		m_cloudSaveManifest->SetCloudSavesEnabled(rgsc::ICloudSaveManifestV2::DISABLE_CLOUD_SAVES);
		m_cloudSaveManifest->Save();
	}

	m_cloudSavesEnabled = false;
}

bool CloudSaveManager::IsSyncingCloudSaves()
{
	return m_state > eState_None;
}

bool CloudSaveManager::IsWaitingForUserAction()
{
	switch(m_state)
	{
	case eState_WaitingForBackupResolutions:
	case eState_WaitingForConflictResolutions:
		return true;
	}

	return false;
}

void CloudSaveManager::OnBetaCheckComplete()
{
	if (m_hasBetaAccess)
	{
		DEBUGF1("User has beta access.");
		StartEnabledCheck();
	}
	else
	{
		DEBUGF1("User doesn't have beta access.");
		OnFinishCloudSync();
	}
}

void CloudSaveManager::OnEnabledCheck(bool cloudSavesEnabled)
{
	DEBUGF1("CloudSaveManager::OnEnabledCheck(%s)", cloudSavesEnabled ? "true" : "false");

	m_cloudSavesEnabled = cloudSavesEnabled;

	m_shownCancelButton = false;
	m_uSyncStartTime = CUtil::CurrentTimeMillis();

	if (cloudSavesEnabled)
	{
		if (Globals::restorecloudbackups && !m_exitAfterSync)
		{
			m_socialClub->GetCloudSaveManager()->SearchForBackups(m_cloudSaveManifest, m_asyncStatus);
			m_state = eState_WaitingForBackupSearch;
		}
		else
		{
			StartIdentifyingConflicts();
		}
	}
	else
	{
		NotifyUiOfSync();
		OnFinishCloudSync();
	}
}

void CloudSaveManager::OnBackupSearchComplete()
{
	rgsc::ICloudSaveBackupInfo* backupInfo = NULL;

	JsonWriter jsonWriter;

	jsonWriter.StartObject().StartArray("Commands").StartObject();
	jsonWriter.WriteNamedString("Command", "RequestLocalBackupResolution");
	jsonWriter.StartObject("Parameter").StartArray("conflicts");

	bool anyBackups = false;

	m_uBackupRestoreStartTime = CUtil::CurrentTimeMillis();

	rtry
	{
		backupInfo = m_socialClub->GetCloudSaveManager()->AllocateBackupInfo();
		rverify(backupInfo, catchall, );

		rgsc::ICloudSaveBackupInfoV1* backupInfoV1 = NULL;
		HRESULT hr = backupInfo->QueryInterface(rgsc::IID_ICloudSaveBackupInfoV1, (void**)&backupInfoV1);
		rverify(SUCCEEDED(hr) && backupInfoV1, catchall, );
		backupInfoV1->Clear();

		for (unsigned i = 0; i < m_cloudSaveManifest->GetNumFiles(); i++)
		{
			rgsc::ICloudSaveFile* file = m_cloudSaveManifest->GetFile(i);
			rverify(file, catchall, );

			rgsc::ICloudSaveFileV3* fileV3 = NULL;
			hr = file->QueryInterface(rgsc::IID_ICloudSaveFileV3, (void**)&fileV3);
			rverify(SUCCEEDED(hr) && fileV3, catchall, );

			rgsc::s64 backupSlots = fileV3->GetBackupSlots();
			int maxBackupSlots = m_socialClub->GetCloudSaveManager()->GetNumBackupSlots();

			int backupsExist = 0;

			for (int j = 0; j < maxBackupSlots; j++)
			{
				// Each bit that is set represents a backup file that exists.
				//	After backup resolution is displayed call:
				//	m_socialClub->GetCloudSaveManager()->RestoreBackup(m_cloudSaveManifest, File Index (i), Backup Index (j), m_asyncStatus);
				if (BIT_ISSET<rgsc::s64>(j, backupSlots))
				{
					// TODO: Backup resolution states - for now, log it out.
					m_socialClub->GetCloudSaveManager()->GetBackupInfo(m_cloudSaveManifest, i, j, backupInfoV1);
					DEBUGF3("Backup file %d - slot %d (%s)", i, j, fileV3->GetFileName());
					DEBUGF3("  Last Modified: %I64u", backupInfoV1->GetClientLastModifiedDate());
					DEBUGF3("  Metadata: %s", backupInfoV1->GetMetaData());

					if (!backupsExist)
					{
						anyBackups = true;
						u64 timestamp = fileV3->GetClientLastModifiedDate();
						std::string timestampString;
						UnixTimeToUtcString(timestamp, timestampString);

						jsonWriter.StartObject().WriteNamedString("fileName", fileV3->GetFileName());
						jsonWriter.StartObject("current");
						jsonWriter.WriteNamedString("lastModified", timestampString.c_str());
						jsonWriter.StartArray("metadata").WriteString(fileV3->GetMetaData()).EndArray();
						jsonWriter.EndObject();
					}

					char slotName[64];
					sprintf(slotName, "localbackup%d", ++backupsExist);
					jsonWriter.StartObject(slotName);

					u64 timestamp = backupInfoV1->GetClientLastModifiedDate();
					std::string timestampString;
					UnixTimeToUtcString(timestamp, timestampString);
					jsonWriter.WriteNamedString("lastModified", timestampString.c_str());
					jsonWriter.StartArray("metadata").WriteString(backupInfoV1->GetMetaData()).EndArray();
					jsonWriter.EndObject();

					backupInfoV1->Clear();
				}
			}

			if (backupsExist)
			{
				m_NumBackupFiles++;
				jsonWriter.EndObject();
			}
		}
	}
	rcatchall
	{

	}

	if (backupInfo)
	{
		m_socialClub->GetCloudSaveManager()->FreeBackupInfo(backupInfo);
	}


	jsonWriter.EndArray().EndObject();
	jsonWriter.EndObject().EndArray().EndObject();

	if (anyBackups)
	{
		DEBUGF1("Backups to resolve.");
		m_state = eState_WaitingForBackupResolutions;
		m_socialClub->SendMessageToScui(jsonWriter.GetBuffer().c_str());
	}
	else
	{
		// And then go on to syncing
		DEBUGF1("No backups to resolve.");
		StartIdentifyingConflicts();
	}
}

void CloudSaveManager::OnBackupResolutionsReceived(JsonReader resolutionList)
{
	DEBUGF1("CloudSaveManager::OnBackupResolutionsReceived");
	rgsc::ICloudSaveBackupInfo* backupInfo = NULL;

	m_backupsToRestore.clear();

	m_shownCancelButton = false;
	m_uBackupRestoreEndTime = m_uSyncStartTime = CUtil::CurrentTimeMillis();

	rtry
	{
		backupInfo = m_socialClub->GetCloudSaveManager()->AllocateBackupInfo();
		rverify(backupInfo, catchall, );

		rgsc::ICloudSaveBackupInfoV1* backupInfoV1 = NULL;
		HRESULT hr = backupInfo->QueryInterface(rgsc::IID_ICloudSaveBackupInfoV1, (void**)&backupInfoV1);
		rverify(SUCCEEDED(hr) && backupInfoV1, catchall, );
		backupInfoV1->Clear();

		for (JsonReader conflict = resolutionList.GetFirstChild(); conflict.IsValid(); conflict = conflict.GetNextSibling())
		{
			std::string fileName, resolution;
			if (conflict.GetChild("fileName").GetStringValue(fileName) && conflict.GetChild("resolution").GetStringValue(resolution))
			{
				if (resolution == "useCurrent")
					continue;

				int selectedBackupIndex = 0;
				if (resolution == "useLocalBackup2")
					selectedBackupIndex = 1;

				for (unsigned i = 0; i < m_cloudSaveManifest->GetNumFiles(); i++)
				{
					rgsc::ICloudSaveFile* file = m_cloudSaveManifest->GetFile(i);
					rverify(file, catchall, );

					rgsc::ICloudSaveFileV3* fileV3 = NULL;
					hr = file->QueryInterface(rgsc::IID_ICloudSaveFileV3, (void**)&fileV3);
					rverify(SUCCEEDED(hr) && fileV3, catchall, );

					if (fileName == fileV3->GetFileName())
					{
						rgsc::s64 backupSlots = fileV3->GetBackupSlots();
						int maxBackupSlots = m_socialClub->GetCloudSaveManager()->GetNumBackupSlots();

						int backupsIndex = 0;
						for (int j = 0; j < maxBackupSlots; j++)
						{
							// Each bit that is set represents a backup file that exists.
							if (BIT_ISSET<rgsc::s64>(j, backupSlots))
							{
								// TODO: Backup resolution states - for now, log it out.
								m_socialClub->GetCloudSaveManager()->GetBackupInfo(m_cloudSaveManifest, i, j, backupInfoV1);

								/*
								DEBUGF3("Backup file %d - slot %d (%s)", i, j, fileV3->GetFileName());
								DEBUGF3("  Last Modified: %I64u", backupInfoV1->GetClientLastModifiedDate());
								DEBUGF3("  Metadata: %s", backupInfoV1->GetMetaData());
								*/

								if (backupsIndex++ == selectedBackupIndex)
								{
									DEBUGF3("Restoring backup file %d - slot %d (%s)", i, j, fileV3->GetFileName());
									DEBUGF3("  Last Modified: %I64u", backupInfoV1->GetClientLastModifiedDate());
									DEBUGF3("  Metadata: %s", backupInfoV1->GetMetaData());

									m_backupsToRestore.push_back(BackupRestorationInfo(i,j));
								}

								backupInfoV1->Clear();
							}
						}
					}
				}
			}
		}
	}
	rcatchall
	{

	}

	SendBackupRestorationTelemetry();

	if (backupInfo)
	{
		m_socialClub->GetCloudSaveManager()->FreeBackupInfo(backupInfo);
	}

	if (m_backupsToRestore.empty())
		StartIdentifyingConflicts();
	else
		StartRestoringNextBackup();
}

void CloudSaveManager::OnModificationListComplete()
{
	if (m_socialClub->GetCloudSaveManager()->GetConflictMetadata(m_cloudSaveManifest, m_asyncStatus))
	{
		m_state = eState_WaitingForConflictMetadata;
	}
	else
	{
		OnError();
	}
}

void CloudSaveManager::OnConflictMetadataDownloaded()
{
	DEBUGF1("CloudSaveManager::OnModificationListComplete");
	JsonWriter jsonWriter;

	jsonWriter.StartObject().StartArray("Commands").StartObject();
	jsonWriter.WriteNamedString("Command", "RequestCloudConflictResolution");
	jsonWriter.StartObject("Parameter").StartArray("conflicts");

	bool anyConflicts = false;

#if !RSG_FINAL
	if (Globals::cloudsave_singleconflict || Globals::cloudsave_multipleconflicts)
	{
		anyConflicts = true;

		jsonWriter.StartObject();
		jsonWriter.WriteNamedString("fileName", "save1");
		jsonWriter.StartObject("cloud");
		jsonWriter.WriteNamedString("lastModified", "2016-01-01 12:00 UTC");
		jsonWriter.StartArray("metadata");
		jsonWriter.WriteString("Mission 10 name"); 
		jsonWriter.WriteString("8% Complete");
		jsonWriter.EndArray();
		jsonWriter.EndObject();
		jsonWriter.StartObject("local");
		jsonWriter.WriteNamedString("lastModified", "2016-01-02 12:00 UTC");
		jsonWriter.StartArray("metadata");
		jsonWriter.WriteString("Mission 11 name"); 
		jsonWriter.WriteString("9% Complete");
		jsonWriter.EndArray();
		jsonWriter.EndObject();
		jsonWriter.EndObject();


		if (Globals::cloudsave_multipleconflicts)
		{
			jsonWriter.StartObject();
			jsonWriter.WriteNamedString("fileName", "save2");
			jsonWriter.StartObject("cloud");
			jsonWriter.WriteNamedString("lastModified", "2016-01-01 12:00 UTC");
			jsonWriter.StartArray("metadata");
			jsonWriter.WriteString("Mission 12 name"); 
			jsonWriter.WriteString("10% Complete");
			jsonWriter.EndArray();
			jsonWriter.EndObject();
			jsonWriter.StartObject("local");
			jsonWriter.WriteNamedString("lastModified", "2016-01-02 12:00 UTC");
			jsonWriter.StartArray("metadata");
			jsonWriter.WriteString("Mission 13 name"); 
			jsonWriter.WriteString("11% Complete");
			jsonWriter.EndArray();
			jsonWriter.EndObject();
			jsonWriter.EndObject();
		}
	}
	else
	{
#endif
		//TODO: Present actual conflicts

		for (u32 i = 0; i < m_cloudSaveManifest->GetNumFiles(); i++)
		{
			rgsc::ICloudSaveFile* iCloudFile = m_cloudSaveManifest->GetFile(i);
			rgsc::ICloudSaveFileV2* cloudFile = NULL;
			iCloudFile->QueryInterface(rgsc::IID_ICloudSaveFileV2, (void**)&cloudFile);

			if (cloudFile)
			{
				ConflictState conflictState = cloudFile->GetConflictState();
				if (conflictState == ConflictState::CS_ServerHasNewerWithConflict || conflictState == ConflictState::CS_ClientHasNewerWithConflict || conflictState == ConflictState::CS_FileDeletedWithConflict)
				{
					anyConflicts = true;

					u64 clientTimestamp = cloudFile->GetClientLastModifiedDate();
					u64 serverTimestamp = cloudFile->GetServerLastModifiedDate();

					std::string serverTimestampString, clientTimestampString;
					UnixTimeToUtcString(clientTimestamp, clientTimestampString);
					UnixTimeToUtcString(serverTimestamp, serverTimestampString);

					const char* clientMetadata = cloudFile->GetMetaData();
					const char* serverMetadata = cloudFile->GetServerMetadata();
					bool clientDeleted = false, serverDeleted = false;

					// If the file has been deleted locally but there's a newer version on the server
					if (conflictState == ConflictState::CS_FileDeletedWithConflict)
					{
						clientDeleted = true;
					}

					// TODO: Hack - for now, metadata may not come down from the cloud
					if (StringNullOrEmpty(clientMetadata))
					{
						clientDeleted = true;
					}
					if (StringNullOrEmpty(serverMetadata))
					{
						serverDeleted = true;
					}

					jsonWriter.StartObject();

					// Filename
					jsonWriter.WriteNamedString("fileName", cloudFile->GetFileName());

					// Cloud info
					if (serverDeleted)
					{
						jsonWriter.WriteNamedBool("cloud", false);
					}
					else
					{
						jsonWriter.StartObject("cloud");
						jsonWriter.WriteNamedString("lastModified", serverTimestampString.c_str());
						jsonWriter.StartArray("metadata");
						jsonWriter.WriteString(serverMetadata); 
						jsonWriter.EndArray();
						jsonWriter.EndObject();
					}

					// Local info
					if (clientDeleted)
					{
						jsonWriter.WriteNamedBool("local", false);
					}
					else
					{
						jsonWriter.StartObject("local");
						jsonWriter.WriteNamedString("lastModified", clientTimestampString.c_str());
						jsonWriter.StartArray("metadata");
						jsonWriter.WriteString(clientMetadata); 
						jsonWriter.EndArray();
						jsonWriter.EndObject();
					}

					jsonWriter.EndObject();
				}
			}
		}


#if !RSG_FINAL
	}
#endif

	jsonWriter.EndArray().EndObject();
	jsonWriter.EndObject().EndArray().EndObject();

	if (anyConflicts)
	{
		DEBUGF1("Conflicts.");
		m_state = eState_WaitingForConflictResolutions;
		m_uResolutionStartTime = CUtil::CurrentTimeMillis();
		m_socialClub->SendMessageToScui(jsonWriter.GetBuffer().c_str());
	}
	else
	{
		DEBUGF1("No conflicts.");
		m_state = eState_WaitingForFileUploads;
		m_socialClub->GetCloudSaveManager()->PostUpdatedFiles(m_cloudSaveManifest, m_asyncStatus);
	}
}

void CloudSaveManager::OnConflictResolutionsReceived(JsonReader resolutionList)
{
	
	DEBUGF1("CloudSaveManager::OnConflictResolutionsReceived");

	m_shownCancelButton = false;
	m_uResolutionEndTime = m_uSyncStartTime = CUtil::CurrentTimeMillis();
	DEBUGF3("%lld ms spent resolving conflicts", m_uResolutionEndTime - m_uResolutionStartTime);

	JsonReader conflict = resolutionList.GetFirstChild();
	while (conflict.IsValid())
	{
		std::string fileName, resolution;
		if (conflict.GetChild("fileName").GetStringValue(fileName) && conflict.GetChild("resolution").GetStringValue(resolution))
		{
			u32 numFiles = m_cloudSaveManifest->GetNumFiles();
			for (u32 i = 0; i < numFiles; i++)
			{
				rgsc::ICloudSaveFile* iCloudFile = m_cloudSaveManifest->GetFile(i);
				rgsc::ICloudSaveFileV2* cloudFile = NULL;
				iCloudFile->QueryInterface(rgsc::IID_ICloudSaveFileV2, (void**)&cloudFile);
				if (cloudFile && fileName == cloudFile->GetFileName())
				{
					if (resolution == "useLocal")
					{
						cloudFile->SetResolveType(ConflictResolutionType::CRT_AcceptLocal);
					}
					else if (resolution == "useCloud")
					{
						cloudFile->SetResolveType(ConflictResolutionType::CRT_AcceptRemote);
					}
					else if (resolution == "ignore")
					{
						// If any are ignored, then abort the whole process
						m_state = eState_None;
						DEBUGF1("Cloud Sync: Ignored Conflict, finishing.");
						OnFinishCloudSync();
						return;
					}
					else
					{
						ERRORF("Invalid cloud conflict resolution type!");
					}
				}
			}
		}
		conflict = conflict.GetNextSibling();
	}

	m_state = eState_WaitingForBackupCreation;
	SendConflictResolutionTelemetry();
	NotifyUiOfSync();
	m_socialClub->GetCloudSaveManager()->BackupConflictedFiles(m_cloudSaveManifest, m_asyncStatus);
}

void CloudSaveManager::OnFileTransmissionsComplete()
{
	DEBUGF1("CloudSaveManager::OnFileTransmissionsComplete");
	NotifyUiOfSync();
	m_state = eState_SwappingDownloadedFiles;
}

void CloudSaveManager::OnCloudSaveEnabledUpdated(const char* rosTitleName)
{
	DEBUGF1("CloudSaveManager::OnCloudSaveEnabledUpdated(%s)", rosTitleName ? rosTitleName : "NULL");

	// If we're idle, we can load/save the manifest.
	//	If we aren't idle, the manifest will be saved at the end of the operation.
	if (m_cloudSaveManifest 
		&& strcmp(m_cloudSaveManifest->GetTitleName(), rosTitleName) == 0 &&
		(m_state == eState_None || m_state == eState_WaitingForEnabledCheck))
	{
		// cache the new enabled state from the local copy
		rgsc::ICloudSaveManifestV2::CloudSavesEnabledState state = m_cloudSaveManifest->GetCloudSaveEnabled();

		// Get the most up to date version from disk - this will overwrite the local copy if the manifest exists on disk
		m_cloudSaveManifest->Load();

		// Update the cloud saves enabled state and save to disk
		m_cloudSaveManifest->SetCloudSavesEnabled(state);
		m_cloudSaveManifest->Save();

		m_cloudSavesEnabled = (m_cloudSaveManifest->GetCloudSaveEnabled() == rgsc::ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES);
	}
}

bool CloudSaveManager::IsCloudSaveEnabled()
{
	return m_cloudSavesEnabled;
}

void CloudSaveManager::SendConflictResolutionTelemetry()
{
	DEBUGF1("CloudSaveManager::SendConflictResolutionTelemetry");
	m_socialClub->GetCloudSaveManager()->WriteConflictTelemetry(m_cloudSaveManifest);
	m_socialClub->GetTelemetryManager()->Flush(false);
}

void CloudSyncTelemetryHelper(std::string& str, const char* id, int value)
{
	char stateBuf[64];
	sprintf_s(stateBuf, "\"%s\":%d,", id, value);
	str.append(stateBuf);
}

void CloudSyncTelemetryHelper64(std::string& str, const char* id, u64 value)
{
	char stateBuf[64];
	sprintf_s(stateBuf, "\"%s\":%llu,", id, value);
	str.append(stateBuf);
}

void CloudSyncTelemetryHelperStr(std::string& str, const char* id, const char* value)
{
	char stateBuf[256];
	sprintf_s(stateBuf, "\"%s\":\"%s\",", id, value);
	str.append(stateBuf);
}

void CloudSaveManager::SendSyncTelemetry(SyncReportStatus status)
{
	DEBUGF1("CloudSaveManager::SendSyncTelemetry");

	// can't send telemetry if offline
	if (!m_socialClub->IsOnline())
		return;

	u64 duration = 0;

	int errorCode = 0;

	rgsc::ICloudSaveHttpDebugInfo* debugInfo = NULL;

	// Log out the status of the cloud sync
	std::string json;
	switch(status)
	{
	case eStatus_NotInBeta:
		json.append("\"s\":0,");
		break;
	case eStatus_Disabled:
		json.append("\"s\":1,");
		break;
	case eStatus_Succeeded:
		json.append("\"s\":2,");
		duration = CUtil::CurrentTimeMillis() - m_uStartTime;
		break;
	case eStatus_Failed:
		json.append("\"s\":3,");
		duration = CUtil::CurrentTimeMillis() - m_uStartTime;
		errorCode = m_asyncStatus->GetResultCode(); 
		if (m_cloudSaveManifest)
		{
			rgsc::ICloudSaveManifestV4* manifestV4 = NULL;
			HRESULT hr = m_cloudSaveManifest->QueryInterface(rgsc::IID_ICloudSaveManifestV4, (void**)&manifestV4);
			if (SUCCEEDED(hr) && manifestV4)
			{
				debugInfo = manifestV4->GetHttpDebugInfo();
			}
		}
		break;
	case eStatus_Cancelled:
		json.append("\"s\":4,");
		duration = CUtil::CurrentTimeMillis() - m_uStartTime;
		errorCode = m_asyncStatus->GetResultCode(); 
		break;
	case eStatus_Banned:
		json.append("\"s\":5,");
		break;
	}

	// By default, the error code is zero. It is only set for failure metrics.
	CloudSyncTelemetryHelper(json, "e", errorCode);

	//	The client state machine step should be included as well.
	CloudSyncTelemetryHelper(json, "c", (int)m_state);

	// The overall duration of cloud save syncing
	CloudSyncTelemetryHelper64(json, "d", duration);

	//	The time spent resolving conflicts
	u64 conflictDuration = m_uResolutionEndTime - m_uResolutionStartTime;
	CloudSyncTelemetryHelper64(json, "r", conflictDuration);

	//	The hardware id
	CloudSyncTelemetryHelperStr(json, "h", g_gta5CloudSaveConfig->GetHardwareId());

	//  The sync count
	CloudSyncTelemetryHelper(json, "sc", m_syncCount);

	if (debugInfo)
	{
		HRESULT hr = S_OK;

		rgsc::ICloudSaveHttpDebugInfoV2* debugInfoV2 = NULL;
		hr = debugInfo->QueryInterface(rgsc::IID_ICloudSaveHttpDebugInfoV2, (void**)&debugInfoV2);
		if (SUCCEEDED(hr) && debugInfoV2)
		{
			CloudSyncTelemetryHelper(json, "ca", (int)debugInfoV2->HadMemoryAllocationError()); 
			CloudSyncTelemetryHelper(json, "ar", debugInfoV2->GetAbortReason()); 
			CloudSyncTelemetryHelper(json, "tr", debugInfoV2->GetTcpResultState());
			CloudSyncTelemetryHelper(json, "te", debugInfoV2->GetTcpError());
		}

		rgsc::ICloudSaveHttpDebugInfoV1* debugInfoV1 = NULL;
		hr = debugInfo->QueryInterface(rgsc::IID_ICloudSaveHttpDebugInfoV1, (void**)&debugInfoV1);
		if (SUCCEEDED(hr) && debugInfoV1)
		{
			CloudSyncTelemetryHelper(json, "hs", debugInfoV1->GetSendState());
			CloudSyncTelemetryHelper(json, "hr", debugInfoV1->GetRecvState());
			CloudSyncTelemetryHelper(json, "hi", debugInfoV1->GetInContentBytesRcvd());
			CloudSyncTelemetryHelper(json, "ho", debugInfoV1->GetOutContentBytesSent());
			CloudSyncTelemetryHelper(json, "hc", (int)debugInfoV1->IsCommitted());
			CloudSyncTelemetryHelper(json, "ht", (int)debugInfoV1->GetHttpStatusCode());

			// reset for the next time through
			debugInfoV1->Reset();
		}
	}

	// remove trailing comma
	json.resize(json.size() - 1);

	DEBUGF1("Sync Telemetry: %s", json.c_str());

	m_socialClub->GetTelemetryManager()->Write("SC_CS_SYNC", json.c_str(), TELEMETRY_CHANNEL_MISC, rgsc::ITelemetryPolicyV1::RGSC_LOGLEVEL_LOW_PRIORITY);
	m_socialClub->GetTelemetryManager()->Flush(false);
}

void CloudSaveManager::SendBackupRestorationTelemetry()
{
	DEBUGF1("CloudSaveManager::SendBackupRestorationTelemetry");

	// Log out the status of the cloud sync
	std::string json;

	//	The hardware id
	char hardwareBuffer[64];
	sprintf_s(hardwareBuffer, "\"h\":\"%s\",", g_gta5CloudSaveConfig->GetHardwareId());
	json.append(hardwareBuffer);

	//	The time spent resolving backup ui
	u64 uiDuration = m_uBackupRestoreEndTime - m_uBackupRestoreStartTime;
	char uiDurationBuf[64];
	sprintf_s(uiDurationBuf, "\"r\":%lld,", uiDuration);
	json.append(uiDurationBuf);

	// number of files that backups existed for on disk
	char numFilesBuf[64];
	sprintf_s(numFilesBuf, "\"n\":%lld,", m_NumBackupFiles);
	json.append(numFilesBuf);

	// array of files chosen for restoration
	json.append("\"a\":[");
	for (std::vector<BackupRestorationInfo>::iterator itr = m_backupsToRestore.begin(); itr != m_backupsToRestore.end(); ++itr)
	{
		char fileBuf[64];
		sprintf_s(fileBuf, "%s{\"f\":\"%d\",\"b\":\"%d\"}", itr == m_backupsToRestore.begin() ? "" : "," , (*itr).m_fileIndex, (*itr).m_backupIndex);
		json.append(fileBuf);
	}
	json.append("]");

	DEBUGF1("Restore Backup Telemetry: %s", json.c_str());

	m_socialClub->GetTelemetryManager()->Write("SC_CS_RSTR", json.c_str(), TELEMETRY_CHANNEL_MISC, rgsc::ITelemetryPolicyV1::RGSC_LOGLEVEL_LOW_PRIORITY);
	m_socialClub->GetTelemetryManager()->Flush(false);
}

void CloudSaveManager::SendCloudSaveConfigurationToSDK()
{
	DEBUGF1("CloudSaveManager::SendCloudSaveConfigurationToSDK");

	// The maximum size of a file
	m_socialClub->GetCloudSaveManager()->SetMaximumFileSize(OnlineConfig::CloudSaveMaxFileSize);

	if (OnlineConfig::CloudSaveWorkerThread)
	{
		m_socialClub->GetCloudSaveManager()->CreateWorkerThread();
	}

	rgsc::ICloudSaveManifest* manifest = m_socialClub->GetCloudSaveManager()->RegisterTitle(g_gta5CloudSaveConfig);
	if (manifest)
	{
		manifest->QueryInterface(rgsc::IID_ICloudSaveManifestV2, (void**)&m_cloudSaveManifest);

		rgsc::ICloudSaveManifestV3* manifestV3 = NULL;
		HRESULT hr = manifest->QueryInterface(rgsc::IID_ICloudSaveManifestV3, (void**)&manifestV3);
		if (SUCCEEDED(hr) && manifestV3)
		{
			rgsc::ICloudSaveOperationProgress* progress = manifestV3->GetProgressTracker();
			if (progress)
			{
				progress->QueryInterface(rgsc::IID_ICloudSaveOperationProgressV1, (void**)&m_cloudSaveOperationProgress);
				progress->QueryInterface(rgsc::IID_ICloudSaveOperationProgressV2, (void**)&m_cloudSaveOperationProgressV2);
			}
		}
	}
}

void CloudSaveManager::OnBanned()
{
	DEBUGF1("CloudSaveManager::OnBanned");

	// Load the manifest if necessary
	if (m_cloudSaveManifest->GetCloudSaveEnabled() == rgsc::ICloudSaveManifestV2::CloudSavesEnabledState::INVALID)
	{
		m_cloudSaveManifest->Load();
	}

	// Check if the user previously opted into cloud saves.
	if (m_cloudSaveManifest->GetCloudSaveEnabled() != rgsc::ICloudSaveManifestV2::CloudSavesEnabledState::ENABLE_CLOUD_SAVES)
	{
		// Bypass cloud saving, user is not opted into cloud saves. No error is screen is required.
		OnFinishCloudSync();
	}
	// Check if the ban has an end date.
	else if (m_socialClub->GetBanEndTime() == 0)
	{
		DEBUGF1("Permanently Banned");

		// If we're opted into cloud saves and permanently banned, reset the state to unknown.
		//	This will ensure that the user is only shown a cloud sync error once and then never again.
		DEBUGF1("Resetting cloud save opt in state to UNKNOWN");
		m_cloudSaveManifest->SetCloudSavesEnabled(rgsc::ICloudSaveManifestV2::CloudSavesEnabledState::UNKNOWN);
		m_cloudSaveManifest->Save();

		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, m_exitAfterSync ? CFsm::eCloudSyncPostGamePermaBannedError : CFsm::eCloudSyncPreGamePermaBannedError);
		SendSyncTelemetry(eStatus_Banned);
		m_state = eState_None;
	}
	else
	{
		DEBUGF1("Ban expires: %I64d", m_socialClub->GetBanEndTime());

		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, m_exitAfterSync ? CFsm::eCloudSyncPostGameBannedError : CFsm::eCloudSyncPreGameBannedError);
		SendSyncTelemetry(eStatus_Banned);
		m_state = eState_None;
	}
}

void CloudSaveManager::StartBetaCheck()
{
	DEBUGF1("CloudSaveManager::StartBetaCheck");
	m_socialClub->GetCloudSaveManager()->HasBetaAccessAsync("launcher/csbcheck.json", &m_hasBetaAccess, m_asyncStatus);
	m_state = eState_WaitingForBetaCheck;
}

void CloudSaveManager::StartEnabledCheck()
{
	DEBUGF1("CloudSaveManager::StartEnabledCheck");
	m_socialClub->SendMessageToScui("{\"Commands\":[{ \"Command\": \"GetCloudSaveState\", \"Parameter\": {} }]}");
	m_state = eState_WaitingForEnabledCheck;
}

bool CloudSaveManager::TryCreateAsyncStatus()
{
	if (!m_asyncStatus)
	{
		rgsc::IAsyncStatus* asyncStatus;
		m_socialClub->GetTaskManager()->CreateAsyncStatus(&asyncStatus);
		if (asyncStatus)
		{
			asyncStatus->QueryInterface(rgsc::IID_IAsyncStatusLatestVersion, (void**)&m_asyncStatus);
		}
	}

	DEBUGF1("CloudSaveManager::TryCreateAsyncStatus: %s", m_asyncStatus != NULL ? "Succeeded" : "Failed");

	return m_asyncStatus != NULL;
}

void CloudSaveManager::StartRestoringNextBackup()
{
	if (!m_backupsToRestore.empty())
	{
		m_state = eState_WaitingForBackupRestoration;
		NotifyUiOfSync();
		BackupRestorationInfo info = m_backupsToRestore.back();
		m_backupsToRestore.pop_back();

		m_socialClub->GetCloudSaveManager()->RestoreBackup(m_cloudSaveManifest, info.m_fileIndex, info.m_backupIndex, m_asyncStatus);
	}
	else
	{
		m_cloudSaveManifest->Save();
		StartIdentifyingConflicts();
	}
}

void CloudSaveManager::StartIdentifyingConflicts()
{
	m_socialClub->GetCloudSaveManager()->IdentifyConflicts(m_cloudSaveManifest, m_asyncStatus);
	m_state = eState_WaitingForModificationsList;
	NotifyUiOfSync();
}

// From https://msdn.microsoft.com/en-us/library/ms724228
// Uses magic numbers to convert to Microsoft's ridiculous date format
void TimetToFileTime( time_t t, LPFILETIME pft )
{
	LONGLONG ll = Int32x32To64(t, 10000000) + 116444736000000000;
	pft->dwLowDateTime = (DWORD) ll;
	pft->dwHighDateTime = ll >>32;
}

void CloudSaveManager::UnixTimeToUtcString(u64 secondsSinceEpoch, std::string& out_string)
{
	time_t time = secondsSinceEpoch;
	FILETIME filetime;
	FILETIME localfiletime;
	SYSTEMTIME systemtime;

	TimetToFileTime(time, &filetime);
	FileTimeToLocalFileTime(&filetime, &localfiletime);
	FileTimeToSystemTime(&localfiletime, &systemtime);
	

	TIME_ZONE_INFORMATION tzi;
	DWORD result = GetTimeZoneInformation(&tzi);

	int offsetMinutes = 0;

	switch(result)
	{
	case TIME_ZONE_ID_STANDARD:
		offsetMinutes = (int)(tzi.Bias + tzi.StandardBias);
		break;
	case TIME_ZONE_ID_DAYLIGHT:
		offsetMinutes = (int)(tzi.Bias + tzi.DaylightBias);
		break;
	case TIME_ZONE_ID_INVALID:
		DEBUGF1("Time zone information invalid: %d", GetLastError());
		break;
	case TIME_ZONE_ID_UNKNOWN:
	default:
		offsetMinutes = (int)tzi.Bias;
		break;
	}
	
	int absOffsetMinutes = abs(offsetMinutes);

	char buffer[256];
	sprintf_s(buffer, "%d-%02d-%02dT%02d:%02d:%02d%c%02d:%02d",
		(int)systemtime.wYear, (int)systemtime.wMonth, (int)systemtime.wDay,
		(int)systemtime.wHour, (int)systemtime.wMinute, (int)systemtime.wSecond,
		offsetMinutes > 0 ? '-' : '+', // Reversed
		absOffsetMinutes / 60, absOffsetMinutes % 60);

	out_string += buffer;
}

void CloudSaveManager::OnFinishCloudSync()
{
	if (m_socialClub && m_socialClub->IsBanned())
	{
		SendSyncTelemetry(eStatus_Banned);
	}
	else if (!m_hasBetaAccess && OnlineConfig::CloudSaveBetaOnly)
	{
		SendSyncTelemetry(eStatus_NotInBeta);
	}
	else if (!m_cloudSavesEnabled)
	{
		SendSyncTelemetry(eStatus_Disabled);
	}
	else if (m_cancelled)
	{
		SendSyncTelemetry(eStatus_Cancelled);
	}
	else 
	{
		SendSyncTelemetry(eStatus_Succeeded);
	}

	m_state = eState_None;

	if (m_exitAfterSync)
	{
		DEBUGF1("CloudSaveManager::OnFinishCloudSync (exiting)");
		CLauncherDlg::PostMessageSafe(WM_CLOSE, 0, 0);
	}
	else
	{
		DEBUGF1("CloudSaveManager::OnFinishCloudSync (going to next state)");
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);
	}
}

void CloudSaveManager::OnError()
{
	DEBUGF1("CloudSaveManager::OnError");
	if (m_asyncStatus && m_asyncStatus->Failed())
	{
		DEBUGF3("Cloud Error Code: %d while in state %d", m_asyncStatus->GetResultCode(), m_state);
	}

	rgsc::ICloudSaveHttpDebugInfo* debugInfo = NULL;

	if (m_cloudSaveManifest)
	{
		rgsc::ICloudSaveManifestV4* manifestV4 = NULL;
		HRESULT hr = m_cloudSaveManifest->QueryInterface(rgsc::IID_ICloudSaveManifestV4, (void**)&manifestV4);
		if (SUCCEEDED(hr) && manifestV4)
		{
			debugInfo = manifestV4->GetHttpDebugInfo();
		}
	}

	if (debugInfo)
	{
		rgsc::ICloudSaveHttpDebugInfoV1* debugInfoV1 = NULL;
		HRESULT hr = debugInfo->QueryInterface(rgsc::IID_ICloudSaveHttpDebugInfoV1, (void**)&debugInfoV1);
		if (SUCCEEDED(hr) && debugInfoV1)
		{
			if (debugInfoV1->GetHttpStatusCode() == 408) /* Timed Out */
			{
				m_timedOut = true;
			}
		}
	}

	// If we timeout on the pre-sync, offer the user the ability to opt out of cloud saves.
	if (m_timedOut && !m_exitAfterSync)
	{
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eCloudSyncPreGameTimeout);
	}
	else
	{
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, m_exitAfterSync ? CFsm::eCloudSyncPostGameError : CFsm::eCloudSyncPreGameError);
	}
	
	SendSyncTelemetry(eStatus_Failed);
	m_state = eState_None;
}

void CloudSaveManager::NotifyUiOfSync()
{
	DEBUGF1("CloudSaveManager::NotifyUiOfSync");

	CLauncherDlg::SetStatusParams* params = new CLauncherDlg::SetStatusParams();

	params->force = false;
	
	if (m_cloudSaveOperationProgress && m_cloudSaveOperationProgress->IsActive())
	{
		UINT stringId = IDS_CLOUDSAVE_SYNC;

		switch (m_state)
		{
		// ----- Simple Progress ----- /
		case eState_WaitingForConflictMetadata:
			stringId = IDS_CLOUDSAVE_WAITING_FOR_METADATA;
			break;
		case eState_WaitingForBackupCreation:
			stringId = IDS_CLOUDSAVE_WAITING_FOR_BACKUP_CREATION;
			break;
		case eState_WaitingForFileDeletions:
			stringId = IDS_CLOUDSAVE_WAITING_FOR_FILE_DELETIONS;
			break;
		// ----- Advanced Progress (%) ----- /
		case eState_WaitingForFileUploads:
			stringId = CanShowFullProgress() ? IDS_CLOUDSAVE_WAITING_FOR_FILE_UPLOADS_PROGRESS : IDS_CLOUDSAVE_WAITING_FOR_FILE_UPLOADS;
			break;
		case eState_WaitingForFileDownloads:
			stringId = CanShowFullProgress() ? IDS_CLOUDSAVE_WAITING_FOR_FILE_DOWNLOADS_PROGRESS : IDS_CLOUDSAVE_WAITING_FOR_FILE_DOWNLOADS;
			break;

		default:
			DEBUGF1("Unrecognised state.");
			break;
		}

		params->status = CLocalisation::Instance().GetString(stringId); 

		// For upload/download of files, we can show approximate progress for long running operations.
		if (CanShowFullProgress())
		{
			CUtil::FormatStringNumbers(params->status, m_cloudSaveOperationProgress->GetCompletedCount()+1, m_cloudSaveOperationProgress->GetStepCount(), m_operationProgress);
		}
		else
		{
			CUtil::FormatStringNumbers(params->status, m_cloudSaveOperationProgress->GetCompletedCount()+1, m_cloudSaveOperationProgress->GetStepCount());
		}
	}
	else
	{
		params->status = CLocalisation::Instance().GetString(IDS_CLOUDSAVE_SYNC);
	}

	if (CUtil::CurrentTimeMillis() < m_uSyncStartTime + SECONDS_BEFORE_CANCEL_BUTTON * 1000)
	{
		params->cancelFn = NULL;
	}
	else
	{
		m_shownCancelButton = true;
		struct Callback
		{
			static void CancelSync(void*)
			{
				Instance()->CancelCloudSync();
			}
		};

		params->cancelFn = Callback::CancelSync;
	}

	CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetStatusWithParamsAndDelete, 0, (LPARAM)params);
}

bool CloudSaveManager::CanShowFullProgress()
{
	if (CUtil::CurrentTimeMillis() < m_uSyncStartTime + SECONDS_BEFORE_PROGRESS * 1000)
		return false;

	if (m_state != eState_WaitingForFileDownloads && m_state != eState_WaitingForFileUploads)
		return false;

	if (m_cloudSaveOperationProgressV2 == NULL)
		return false;

	return true;
}

void CloudSaveManager::UpdateSyncMessage()
{
	if (m_cloudSaveOperationProgress && m_cloudSaveOperationProgress->IsActive())
	{
		if (m_lastReportedSyncState != m_state || 
			m_lastReportedSyncIndex != m_cloudSaveOperationProgress->GetCompletedCount() ||
			m_lastReportedOperationProgress != m_operationProgress)
		{
			m_lastReportedSyncIndex = m_cloudSaveOperationProgress->GetCompletedCount();
			m_lastReportedSyncState = m_state;
			m_lastReportedOperationProgress = m_operationProgress;
			NotifyUiOfSync();
		}
	}
}

void CloudSaveManager::UpdateProgress()
{
	if (m_cloudSaveOperationProgressV2)
	{
		rgsc::ICloudSaveOperationProgressV2* c = m_cloudSaveOperationProgressV2;

		float totalPct = c->GetTotalLength() > 0 ?
							(float)(c->GetTotalProgress() + c->GetStepProgress()) * 100.0f / (float)(c->GetTotalLength()) :
							0;

		// Chunk progress is not exact since the chunked encoding can add bytes not represented by save file data, so clamp 1-100.
		if (totalPct < 0)
			totalPct = 0;
		if (totalPct > 100)
			totalPct = 100;

		m_operationProgress = (int)totalPct;
	}
}

bool CloudSaveManager::StringNullOrEmpty(const char* str)
{
	return !str || str[0] == '\0';
}

