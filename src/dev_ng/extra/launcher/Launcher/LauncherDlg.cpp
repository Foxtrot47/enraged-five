// LauncherDlg.cpp : implementation file
//


#include "stdafx.h"
#include "Launcher.h"
#include "LauncherDlg.h"
#include "Globals.h"
#include "Config.h"
#include "DownloadManager.h"
#include "Localisation.h"
#include "Channel.h"
#include "CrashDetection.h"
#include "CloudSaveManager.h"
#include "Util.h"
#include "Application.h"
#include "WorkerThread.h"
#include "GameCommunication.h"
#include "BootstrapPatchCheck.h"
#include "GamePatchCheck.h"
#include "RGSCPatchCheck.h"
#include "RgscTelemetryManager.h"
#include "seh.h"
#include "TamperSource.h"

#if !RSG_FINAL
#include "DxSurface11.h"
#endif

#include <afxpriv.h>

// Needed for controller support
#include <XInput.h>
#include "rgsc_common.h"
#include "pad_interface.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#if !defined(XUSER_MAX_COUNT)
#define XUSER_MAX_COUNT 4
#endif

#define LAUNCHERDLG_MULTICOLOUR_BACKGROUNDS 0

static CLauncherDlg* sm_lastInstance = NULL;

IMPLEMENT_DYNAMIC(CLauncherDlg, CDialog);

CLauncherDlg::CLauncherDlg(CWnd* /*pParent*/)
	: CWnd(), m_fsm(this)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_LAUNCHER_ICON);
	m_bSocialClubInitialized = false;
	m_bVisible = false;
	//@@: location CLAUNCHERDLG_CLAUNCHERDLG
	sm_lastInstance = this;
	m_lastDragCoordsValid = false;
	m_mouseCaptured = false;
	m_initialised = false;
	m_bShownQuitConfirm = false;
	InitializeCriticalSection(&statusUpdateLock);
}

CLauncherDlg::~CLauncherDlg()
{
	DEBUGF1("~CLauncherDlg()");
	Shutdown();

	if (sm_lastInstance == this)
	{
		sm_lastInstance = NULL;
	}

	DeleteCriticalSection(&statusUpdateLock);

	DEBUGF1("Destroying window...");
	DestroyWindow();
}

CLauncherDlg* CLauncherDlg::GetLastInstance()
{
	//@@: location CLAUNCHERDLG_GETLASTINSTANCE
	return sm_lastInstance;
}

void CLauncherDlg::DoDataExchange(CDataExchange* pDX)
{
	CWnd::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESSBAR, m_progress);
	DDX_Control(pDX, IDC_LEFTBUTTON, m_button[1]);
	DDX_Control(pDX, IDC_MIDDLEBUTTON, m_button[2]);
	DDX_Control(pDX, IDC_RIGHTBUTTON, m_button[0]);
	DDX_Control(pDX, IDC_STATUS, m_status);
	DDX_Control(pDX, IDC_DETAILTEXT, m_detailText);
	DDX_Control(pDX, IDC_DETAILTEXT2, m_detailText2);
	DDX_Control(pDX, IDC_VERSION_STRING, m_version);
}

BEGIN_MESSAGE_MAP(CLauncherDlg, CWnd)
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(CLauncherDlg::eSetState, OnSetState)
	ON_MESSAGE(CLauncherDlg::eSetProgress, OnSetProgress)
	ON_MESSAGE(CLauncherDlg::eSetStatusAndDelete, OnSetStatus)
	ON_MESSAGE(CLauncherDlg::eSetStatusWithParamsAndDelete, OnSetStatusWithParams)
	ON_MESSAGE(CLauncherDlg::eShowCodeRedemptionUI, OnShowCodeRedemptionUI)
	ON_MESSAGE(CLauncherDlg::eShowSigninUI, OnShowSigninUI)
	ON_MESSAGE(CLauncherDlg::eShutdown, OnRequestShutdown)
	ON_MESSAGE(CLauncherDlg::eShowActivationSuccess, OnActivationSuccess)
	ON_MESSAGE(CLauncherDlg::eShowActivationError, OnShowActivationError)
	ON_MESSAGE(CLauncherDlg::eHandleControllerEvent, OnHandleControllerEvent)
	ON_MESSAGE(CLauncherDlg::eSetLauncherVisibility, OnSetLauncherVisibility)
	ON_MESSAGE(CLauncherDlg::eUpdateStatus, OnUpdateStatus)
	ON_MESSAGE(CLauncherDlg::eNextState, OnNextState)
	ON_MESSAGE(CLauncherDlg::eSetControlsVisibility, OnSetControlsVisibility)
	ON_MESSAGE(CLauncherDlg::eMultipleLaunchesAttempted, OnMultipleLaunchesAttempted)
	ON_MESSAGE(CLauncherDlg::eSteamOfflineMode, OnSteamOfflineMode)
	ON_BN_CLICKED(IDC_LEFTBUTTON, OnLeftButton)
	ON_BN_CLICKED(IDC_MIDDLEBUTTON, OnMiddleButton)
	ON_BN_CLICKED(IDC_RIGHTBUTTON, OnRightButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static bool s_exitControllerThread  = false;
static bool s_controllerThreadAlive = false;

// CLauncherDlg message handlers

void CLauncherDlg::SkinButton(CMFCButton& button)
{
	button.m_nFlatStyle = CMFCButton::BUTTONSTYLE_SEMIFLAT;
	button.EnableWindowsTheming(FALSE);
	button.SetFaceColor(RGB(
		Constants::ButtonBackgroundR.Get(),
		Constants::ButtonBackgroundG.Get(),
		Constants::ButtonBackgroundB.Get()), TRUE);
	button.SetTextColor(RGB(
		Constants::ButtonTextColourR.Get(),
		Constants::ButtonTextColourG.Get(),
		Constants::ButtonTextColourB.Get()));

	RECT margins = {0,0,0,0};
	button.SetTextMargin(&margins);

	button.SetFont(&m_buttonFont);
}

int GetDpi()
{
	HDC screen = ::GetDC(0);
	int dpiX = GetDeviceCaps(screen, LOGPIXELSX);
	int dpiY = GetDeviceCaps(screen, LOGPIXELSY);
	::ReleaseDC(0, screen);

	return std::max<int>(dpiX, dpiY);
}

BOOL CLauncherDlg::OnInitDialog()
{
	BringWindowToTop();

	CUtil::SetCurrentThreadName("Main Application Thread");

	m_initialised = true;

	//@@: location CLAUNCHERDLG_ONINITDIALOG_CDIALOG_ON_INIT_DIALOG

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	//@@: location CLAUNCHERDLG_ONINITDIALOG_CREATE_SOLID_BRUSH
	m_brush.CreateSolidBrush(RGB(
		Constants::BackgroundR.Get(),
		Constants::BackgroundG.Get(),
		Constants::BackgroundB.Get()));

	// TODO: Add extra initialization here

	// Setup progress bar
	m_progress.SetRange(0, 100);
	m_progress.SetPos(10);

	// Skin it
	//@@: location CLAUNCHERDLG_ONINITDIALOG_SET_WINDOW_THEME
	SetWindowTheme(m_progress.GetSafeHwnd(), L"", L"");
	m_progress.SetBkColor(RGB(
		Constants::LoadingBarBackgroundR.Get(),
		Constants::LoadingBarBackgroundG.Get(),
		Constants::LoadingBarBackgroundB.Get()));
	m_progress.SetBarColor(RGB(
		Constants::LoadingBarForegroundR.Get(),
		Constants::LoadingBarForegroundG.Get(),
		Constants::LoadingBarForegroundB.Get()));

	// Set background image
	SetBackgroundImage();

	int dpi = GetDpi();
	const int BASE_DPI = 96;

	float fontScale = m_uiScale * (float)BASE_DPI / (float)dpi;

	const float FONT_SIZE_LARGE = 140.0f, FONT_SIZE_MEDIUM = 120.0f, FONT_SIZE_TINY = 60.0f;

	// Setup status message
	//@@: location CLAUNCHERDLG_ONINITDIALOG_SETUP_STATUS_MESSAGE
	m_statusFont.CreatePointFont((int)(FONT_SIZE_LARGE * fontScale), TEXT("Arial"));
	m_status.SetFont(&m_statusFont);

	// Setup detail message
	//@@: location CLAUNCHERDLG_ONINITDIALOG_SETUP_DETAIL_MESSAGE
	m_detailFont.CreatePointFont((int)(FONT_SIZE_MEDIUM * fontScale), TEXT("Arial"));
	m_detailText.SetFont(&m_detailFont);
	m_detailText2.SetFont(&m_detailFont);

	m_versionFont.CreatePointFont((int)(FONT_SIZE_TINY * fontScale), TEXT("Arial"));
	m_version.SetFont(&m_versionFont);

	const float BUTTON_FONT_PIXEL_WIDTH = 17.0f, BUTTON_FONT_PIXEL_HEIGHT = 5.0f;

	m_buttonFont.CreateFontW((int)(BUTTON_FONT_PIXEL_WIDTH * m_uiScale), (int)(BUTTON_FONT_PIXEL_HEIGHT * m_uiScale), 0, 0, FW_DEMIBOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, DEFAULT_PITCH | FF_SWISS, NULL);

	SetVersionString();

	std::string versionString;
	GetVersionString(versionString);

	RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_START);

	// Skin the buttons
	SkinButton(m_button[1]);
	SkinButton(m_button[2]);
	SkinButton(m_button[0]);

	// Setup spinner
	CRect statusRect;
	
	m_status.GetWindowRect(&statusRect);
	ScreenToClient(&statusRect);

	const float SPINNER_PIXEL_WIDTH_HEIGHT = 30.0f;

	int spinnerSize = (int)(SPINNER_PIXEL_WIDTH_HEIGHT * m_uiScale);
	CRect spinnerRect((statusRect.left + statusRect.right - spinnerSize) / 2, statusRect.bottom, (statusRect.left + statusRect.right + spinnerSize) / 2, statusRect.bottom + spinnerSize);
	//@@: location CLAUNCHERDLG_ONINITDIALOG_CREATE_SPINNER
	if (!m_spinner.Create(NULL, _T(""), WS_VISIBLE, spinnerRect, this, 1010))
		ERRORF("m_spinner failed.");

	// Setup DirectX surface
	CRect dxRect;
	GetClientRect(&dxRect);
	//@@: location CLAUNCHERDLG_ONINITDIALOG_CREATE_RECT
#if !RSG_FINAL
	// If we're running automated tests, and there's no foreground window (screen is probably locked) use DX11 instead
	if (Globals::autotest_launcher_selfsignin && GetForegroundWindow() == NULL)
	{
		DEBUGF1("Creating DX11 surface.");
		m_surface = new DxSurface11();
	}
	else
#endif
	{
		DEBUGF1("Creating DX9 surface.");
		m_surface = new DxSurface9();
	}
	m_surface->Create(NULL, _T(""), 0, dxRect, this, 1069);
	HRESULT dxResult = m_surface->AddedToWindow(GetSafeHwnd());

	// Setup initial UI state
	HideAllControls();

	m_uiShadowCopy.spinnerVisible = true;
	RefreshUiFromShadowCopy(true);

	::SendMessage(m_spinner.GetSafeHwnd(), WM_PAINT, 0, 0);

	// Check last exit code
	bool bFoundOrphanInfile = false;

	//@@: location CLAUNCHERDLG_ONINITDIALOG_CHECK_FILE_PAIRS
	if (CCrashDetection::CheckInfileOutfilePairs(bFoundOrphanInfile))
	{
		if (bFoundOrphanInfile)
		{
			DEBUGF3("Found in-file without out-file, setting safemode.");
			Globals::safemode = true;
		}
	}
	else
	{
		DEBUGF3("Unable to check for in-files matching out-files.");
		Globals::safemode = true;
	}

	LauncherApp::SetLauncherDlg(this);
	//@@: location CLAUNCHERDLG_ONINITDIALOG_START_THREAD
	WorkerThread::Instance().Start();

	if (Globals::scofflineonly)
	{
		m_fsm.RemoveOnlineStates();
	}

	// Set initial state
	//@@: range CLAUNCHERDLG_ONINITDIALOG_SET_INIT_STATE {
	if (!CheckWindowsVersion() || AUTOTEST_FAIL)
	{
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_UNSUPPORTED_WINDOWS_VERSION);
		PostMessage(eSetState, CFsm::eErrorScreen);
	}
	//@@: location CLAUNCHERDLG_ONINITDIALOG_CHECK_BIT
	else if (!Check64Bit() || AUTOTEST_FAIL)
	{
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_32BIT);
		PostMessage(eSetState, CFsm::eErrorScreen);
	}
	else if (FAILED(dxResult) || AUTOTEST_FAIL)
	{
		std::stringstream strm;
		strm << std::hex << dxResult;
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_DIRECTX, strm.str());
		PostMessage(eSetState, CFsm::eErrorScreen);
	}
	else if (Globals::safemode)
	{
		PostMessage(eSetState, CFsm::eSafeMode);
	}
#if !RSG_FINAL && !RSG_PRELOADER_LAUNCHER
	else if (Globals::noSocialClub)
	{
		PostMessage(eSetState, CFsm::eLoading);
	}
#endif //FINAL
	else if (GamePatchCheck::GetGameExecutableFolder().empty() || AUTOTEST_FAIL)
	{
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_NO_GAME_PATH);
		PostMessageSafe(CLauncherDlg::eSetState, CFsm::eErrorScreen);
	}
	else if (!CheckLaunchedFromBootstrap())
	{
		CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_NO_BOOTSTRAP);
		PostMessageSafe(CLauncherDlg::eSetState, CFsm::eErrorScreen);
	}
	else
	{
		PostMessage(eNextState);
	}
	//@@: } CLAUNCHERDLG_ONINITDIALOG_SET_INIT_STATE

	
	// If this is the first run of the launcher, default to visible
	// This is done by setting the silent flag to false (unless it
	// has been set one way or the other on the command-line).
	CheckFirstRun();

	if (!Globals::silent)
	{
		SetLauncherVisibility(true, true);
	}

	m_controllerThread = AfxBeginThread(ControllerMonitorThread, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	m_controllerThread->m_bAutoDelete = false;
	m_controllerThread->ResumeThread();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLauncherDlg::UpdateVersionString()
{
	DEBUGF1("Latest.");
	std::string versionString;

	// Read version file
	if (CUtil::ReadSmallFileAsString("version.txt", versionString))
	{
		CUtil::SanitizeSingleLineUTF8(versionString);
	}

	Version versionTxt(versionString);

	char buffer[256] = {0};
	Version gameVersion = IPatchCheck::GetVersionForFile(GamePatchCheck::GetGameExecutableFullPath());

	sprintf(buffer, "|%d.%d.%d.%d|", gameVersion.major, gameVersion.minor, gameVersion.build, gameVersion.revision);
	versionString += buffer;

	Version scdllVersion = RGSCPatchCheck::GetRgscDllVersion();

	sprintf(buffer, "%d.%d.%d.%d|", scdllVersion.major, scdllVersion.minor, scdllVersion.build, scdllVersion.revision);
	versionString += buffer;

	wchar_t currentExePath[MAX_PATH];
	Version launcherVersion;
	if (GetModuleFileNameW(NULL, currentExePath, sizeof(currentExePath)))
	{
		std::wstring fileWide(currentExePath);
		std::string fileNarrow;
		CUtil::WStringToStdString(fileWide, fileNarrow);

		launcherVersion = IPatchCheck::GetVersionForFile(fileNarrow);
		sprintf(buffer, "%d.%d.%d.%d|", launcherVersion.major, launcherVersion.minor, launcherVersion.build, launcherVersion.revision);
		versionString += buffer;
	}

#if RSG_DEBUG
	versionString += "D";
#elif RSG_RELEASE
	versionString += "R";
#elif RSG_FINAL
	versionString += "F";
#else
	versionString += "?";
#endif

#if RSG_PRELOADER_LAUNCHER
	versionString += "P";
#elif RSG_STEAM_LAUNCHER
	versionString += "S";
#else
	versionString += "F";
#endif

	std::string envstring;
	CUtil::GetEnvStr(envstring);

	bool inDev = false;

	if (envstring == "dev")
	{
		versionString += "D";
		inDev = true;
	}
	if (envstring == "prod" || envstring.empty())
		versionString += "P";

	DISPLAYF("Version: %s", versionString.c_str());
	//@@: location CLAUNCHERDLG_UPDATEVERSIONSTRING
	AUTOCRITSEC(statusUpdateLock);
	m_versionString = versionString;

	m_socialclub.SetGameVersionHeaderInfo(gameVersion.build);

	RgscTelemetryManager::SetVersions(versionTxt, gameVersion, scdllVersion, launcherVersion, inDev);
}

void CLauncherDlg::GetVersionString(std::string& out_string)
{
	out_string = m_versionString;
}


void CLauncherDlg::SetVersionString()
{
	AUTOCRITSEC(statusUpdateLock);
	
	if (m_versionString.empty())
		UpdateVersionString();

	// Set to label
	std::wstring versionWString;
	CUtil::StdStringToStdWString(m_versionString, versionWString);
	m_version.SetWindowText(versionWString.c_str());
	m_version.ShowWindow(SW_SHOW);
}

BOOL CLauncherDlg::CheckWindowsVersion()
{
	OSVERSIONINFO info = {0};
	info.dwOSVersionInfoSize = sizeof(info);

	if (!GetVersionEx(&info))
	{
		ERRORF("Can't get OS info.");
	}
	else
	{
		DISPLAYF("OS version: Major: %d, Minor: %d, Build: %d, Platform: %d", info.dwMajorVersion, info.dwMinorVersion, info.dwBuildNumber, info.dwPlatformId);
		if (info.dwMajorVersion < 5 || (info.dwMajorVersion == 5 && info.dwMinorVersion < 2))
		{
			return FALSE;
		}
	}

	return TRUE;
}

// From: http://msdn.microsoft.com/en-us/library/ms684139%28VS.85%29.aspx
BOOL CLauncherDlg::Check64Bit()
{
#ifdef _WIN64
	return TRUE;
#else
	typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);

	LPFN_ISWOW64PROCESS fnIsWow64Process;

	BOOL bIsWow64 = FALSE;

	//IsWow64Process is not available on all supported versions of Windows.
	//Use GetModuleHandle to get a handle to the DLL that contains the function
	//and GetProcAddress to get a pointer to the function if available.

	fnIsWow64Process = (LPFN_ISWOW64PROCESS) GetProcAddress(GetModuleHandle(TEXT("kernel32")),"IsWow64Process");

	if (fnIsWow64Process)
	{
		if (!fnIsWow64Process(GetCurrentProcess(),&bIsWow64))
		{
			bIsWow64 = FALSE;
		}
	}
	return bIsWow64;
#endif
}

BOOL CLauncherDlg::CheckLaunchedFromBootstrap()
{
#if !RSG_STEAM_LAUNCHER
	BootstrapPatchCheck patchCheck;
	Version version = patchCheck.GetExecutableVersion();
	if (version > Version("1.0.0.3") && !Globals::frombootstrap)
	{
		return false;
	}
#endif
	return true;
}

RECT MakeRect(int x, int y, int width, int height)
{
	RECT ret =
	{
		x,
		y,
		x+width,
		y+height
	};

	return ret;
}

RECT CLauncherDlg::GetControlRect(DWORD id)
{
	struct ControlRect
	{
		DWORD id;
		int x, y, width, height;
	};

	const int assumedWidth = 690, assumedHeight = 496;

	// This is a conversion of the rectangles previously defined in the Launcher.rc file
	const ControlRect rects[] =
	{
		{ IDC_LEFTBUTTON,     270,449, 116, 27, },
		{ IDC_MIDDLEBUTTON,   145,449, 116, 27, },
		{ IDC_RIGHTBUTTON,     21,449, 116, 27, },
		{ IDC_STATUS,          25,239, 341, 44, },
		{ IDC_DETAILTEXT,      25, 89, 341,348, },
		{ IDC_DETAILTEXT2,     25, 89, 341,348, },
		{ IDC_PROGRESSBAR,    439,456, 232, 12, },
		{ IDC_VERSION_STRING, assumedWidth-200,assumedHeight-12,200,12 },
	};

	for (int i = 0; i < sizeof(rects)/sizeof(ControlRect); i++)
	{
		const ControlRect& rect = rects[i];

		if (rects[i].id == id)
		{
			RECT out;
			out.left = (rect.x * m_windowWidth) / assumedWidth;
			out.top = (rect.y * m_windowHeight) / assumedHeight;
			out.right = out.left + (rect.width * m_windowWidth / assumedWidth);
			out.bottom = out.top + (rect.height * m_windowHeight / assumedHeight);

			return out;
		}
	}

	ERRORF("No rect found for %d!", id);
	return MakeRect(0,0,0,0);
}

void CLauncherDlg::CreateWindowNoResource(WNDCLASS wndClass)
{
	const int backgroundImageWidth = 690, backgroundImageHeight = 496;

	int dpi = GetDpi();

	DISPLAYF("Using DPI %d", dpi);

	const float BASE_DPI = 96.0f;

	// Calculate the scaling factor
	m_uiScale = std::min<float>(2.0f, std::max<float>(1.0f, (float)dpi / BASE_DPI));

	// Calculate window size based on UI scale and background size
	m_windowWidth = (int)((float)backgroundImageWidth * m_uiScale), m_windowHeight = (int)((float)backgroundImageHeight * m_uiScale);

	// Position the window in the screen
	int screenWidth = GetSystemMetrics(SM_CXSCREEN), screenHeight = GetSystemMetrics(SM_CYSCREEN);

	int x = (screenWidth - m_windowWidth) / 2, y = (screenHeight - m_windowHeight) / 2;

	if (!CreateEx(0, wndClass.lpszClassName, TEXT("Launcher"), WS_POPUP /*| WS_VISIBLE*/, x, y, m_windowWidth, m_windowHeight, NULL, NULL))
	{
		ERRORF("Can't create window!");
	}

	// This is a conversion of the elements previously added in the Launcher.rc file
	m_button[1].Create(L"Button1", 0, GetControlRect(IDC_LEFTBUTTON), this, IDC_LEFTBUTTON);
	m_button[2].Create(L"Button2", 0, GetControlRect(IDC_MIDDLEBUTTON), this, IDC_MIDDLEBUTTON);
	m_button[0].Create(L"Cancel", 0, GetControlRect(IDC_RIGHTBUTTON), this, IDC_RIGHTBUTTON);
	m_status.Create(ES_CENTER | ES_MULTILINE | ES_AUTOVSCROLL | ES_READONLY, GetControlRect(IDC_STATUS), this, IDC_STATUS);
	m_detailText.Create(ES_CENTER | ES_MULTILINE | ES_AUTOVSCROLL | ES_READONLY, GetControlRect(IDC_DETAILTEXT), this, IDC_DETAILTEXT);
	m_detailText2.Create(ES_CENTER | ES_MULTILINE | ES_AUTOVSCROLL | ES_READONLY, GetControlRect(IDC_DETAILTEXT2), this, IDC_DETAILTEXT2);
	m_progress.Create(0, GetControlRect(IDC_PROGRESSBAR), this, IDC_PROGRESSBAR);
	m_version.Create(L"v0.0", SS_RIGHT, GetControlRect(IDC_VERSION_STRING), this, IDC_VERSION_STRING);
}

INT_PTR CLauncherDlg::DoModal()
{
	OnInitDialog();

	//@@: location CLAUNCHERDLG_DOMODAL_SET_TEMPLATE
	SendMessage(WM_PAINT);

	//@@: location CLAUNCHERDLG_DOMODAL_PREMODAL
	RunModalLoop();

	return 0;
}

LRESULT CLauncherDlg::OnSetState(WPARAM wParam, LPARAM /*lParam*/)
{
	//@@: location CLAUNCHERDLG_ONSETSTATE
	m_fsm.SetState((CFsm::FsmState)wParam);
	return 0;
}

LRESULT CLauncherDlg::OnSetProgress(WPARAM wParam, LPARAM /*lParam*/)
{
	float progress = (float)wParam;

	m_uiShadowCopy.progressPercent = progress;

	RefreshUiFromShadowCopy();

	return 0;
}

LRESULT CLauncherDlg::OnSetStatus(WPARAM wParam, LPARAM lParam)
{
	const char* detailString = (const char*)lParam;

	std::string status(detailString);
	CUtil::StdStringToStdWString(status, m_uiShadowCopy.status);
	delete[] detailString;

	RefreshUiFromShadowCopy(wParam != 0);

	return 0;
}

LRESULT CLauncherDlg::OnSetStatusWithParams(WPARAM wParam, LPARAM lParam)
{
	SetStatusParams* params = (SetStatusParams*)lParam;

	CUtil::StdStringToStdWString(params->status, m_uiShadowCopy.status);

	if (params->cancelFn)
	{
		m_uiShadowCopy.buttons[0] = ButtonParams(CLocalisation::Instance().GetWString(IDS_BUTTON_CANCEL), params->cancelFn, NULL);
	}
	else
	{
		m_uiShadowCopy.buttons[0].Reset();
	}

	RefreshUiFromShadowCopy(params->force);

	delete params;

	return 0;
}

LRESULT CLauncherDlg::OnShowCodeRedemptionUI(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	//@@: range CLAUNCHERDLG_ONSHOWCODEREDEMPTIONUI {
	
	DEBUGF1("Waiting to show code redemption...");
	// Note that this should be removed after B*2069665
	Sleep(1000);
	DEBUGF1("Showing code redemption screen...");
	m_socialclub.ShowCodeRedemptionUI();
	SetLauncherVisibility(true);
	HideAllControls(false);
	return 0;
	//@@: } CLAUNCHERDLG_ONSHOWCODEREDEMPTIONUI
}

LRESULT CLauncherDlg::OnShowSigninUI(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	//@@: range CLAUNCHERDLG_ONSHOWSIGNINUI {
	DEBUGF1("CLauncherDlg::OnShowSigninUI()");
	m_socialclub.ShowSigninUI();
	SetLauncherVisibility(true);
	HideAllControls(false);
	return 0;
	//@@: } CLAUNCHERDLG_ONSHOWSIGNINUI

}

LRESULT CLauncherDlg::OnRequestShutdown(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	OnClose();
	return 0;
}


LRESULT CLauncherDlg::OnHandleControllerEvent(WPARAM wParam, LPARAM /*lParam*/)
{
	if (
#if !RSG_FINAL
		Globals::useScuiFrontend && 
#endif
		m_socialclub.IsInitialized())
	{
		return 0;
	}

	// Find all the visible buttons in the window
	std::vector<HWND> activeButtons;
	if (m_uiShadowCopy.buttons[0].IsValid())
	{
		activeButtons.push_back(m_button[0].GetSafeHwnd());
	}
	if (m_uiShadowCopy.buttons[1].IsValid())
	{
		activeButtons.push_back(m_button[1].GetSafeHwnd());
	}
	if (m_uiShadowCopy.buttons[2].IsValid())
	{
		activeButtons.push_back(m_button[2].GetSafeHwnd());
	}

	// Without any active buttons, there's nothing to do
	if (activeButtons.empty())
	{
		return 0;
	}

	// Find which one has focus (if any)
	int focussedButtonIndex = -1;
	HWND focusHwnd = ::GetFocus();
	for (int i = 0; i < activeButtons.size(); i++)
	{
		if (focusHwnd == activeButtons[i])
		{
			focussedButtonIndex = i;
			break;
		}
	}

	if (wParam == CONTROLLER_RIGHT)
	{
		DEBUGF3("Controller Right.");

		if (focussedButtonIndex > -1)
			::SetFocus(activeButtons[(focussedButtonIndex + activeButtons.size() - 1) % activeButtons.size()]);
		else
			::SetFocus(activeButtons.front());
	}
	else if (wParam == CONTROLLER_LEFT)
	{
		//@@: location CLAUNCHERDLG_ONHANDLECONTROLLEREVENTCONTROLLERLEFT
		DEBUGF3("Controller Left.");
		::SetFocus(activeButtons[(focussedButtonIndex + 1) % activeButtons.size()]);
	}
	else if (wParam == CONTROLLER_ACCEPT)
	{
		DEBUGF3("Controller Accept.");
		if (focussedButtonIndex > -1)
		{
			::SendMessage(activeButtons[focussedButtonIndex], BM_CLICK, 0, 0);
		}
	}
	else if (wParam == CONTROLLER_CANCEL)
	{
		DEBUGF3("Controller Cancel.");
	}

	return 0;
}

LRESULT CLauncherDlg::OnActivationSuccess(WPARAM /*wParam*/, LPARAM lParam)
{
	//@@: range CLAUNCHERDLG_ONACTIVATIONSUCCESS {
	m_socialclub.ShowActivationSuccess((int)lParam);
	return 0;
	//@@: } CLAUNCHERDLG_ONACTIVATIONSUCCESS

}

LRESULT CLauncherDlg::OnShowActivationError(WPARAM /*wParam*/, LPARAM lParam)
{
	//@@: range CLAUNCHERDLG_ONACTIVATIONERROR {
	m_socialclub.ShowActivationError((int)lParam);
	return 0;
	//@@: } CLAUNCHERDLG_ONACTIVATIONERROR

}


LRESULT CLauncherDlg::OnSetLauncherVisibility(WPARAM wParam, LPARAM lParam)
{
	//@@: location CLAUNCHERDLG_ONSETLAUNCHERVISIBILITY
	DEBUGF1("OnSetLauncherVisibility(%d,%d)", wParam, lParam);
	SetLauncherVisibility(wParam == 1, lParam == 1);
	return 0;
}

LRESULT CLauncherDlg::OnUpdateStatus(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	EnterCriticalSection(&statusUpdateLock);
	m_uiShadowCopy.status = statusUpdateString;
	LeaveCriticalSection(&statusUpdateLock);

	RefreshUiFromShadowCopy();

	return 0;
}

LRESULT CLauncherDlg::OnNextState(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	//@@: location CLAUNCHERDLG_ONNEXTSTATE
	DEBUGF3("OnNextState()");
	m_fsm.NextState();
	return 0;
}

LRESULT CLauncherDlg::OnSetControlsVisibility(WPARAM wParam, LPARAM /*lParam*/)
{
	if (wParam == 1)
		RefreshUiFromShadowCopy(true);
	else
		HideAllControls(false);

	return 0;
}

LRESULT CLauncherDlg::OnMultipleLaunchesAttempted(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	if (LauncherApp::IsGameExecutableRunning())
	{
		SetTextWithSpinner(IDS_GAME_RUNNING);
	}

	return 0;
}

LRESULT CLauncherDlg::OnSteamOfflineMode(WPARAM /*wParam*/, LPARAM /*LPARAM*/)
{
	m_fsm.RemoveOnlineStates();
	return 0;
}


// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLauncherDlg::OnPaint()
{
	if (!m_initialised)
		return;

	CUtil::SetCurrentThreadName("OnPaint Thread");
	RAD_TELEMETRY_WAIT_ZONE("OnPaint");

	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CWnd::OnPaint();
	}
}

afx_msg BOOL CLauncherDlg::OnEraseBkgnd(CDC* pDC)
{
	if (!m_initialised)
		return FALSE;

	RAD_TELEMETRY_WAIT_ZONE("OnEraseBkgnd");

	RECT dest;
	GetClientRect(&dest);
	RECT src;
	src.left = 0;
	src.top = 0;
	src.right = m_imageBackground.GetWidth()-1;
	src.bottom = m_imageBackground.GetHeight()-1;
	m_imageBackground.StretchBlt(pDC->GetSafeHdc(), dest, src);

	return FALSE;
}

HBRUSH CLauncherDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT /*nCtlColor*/)
{
	if (pWnd == &m_version)
	{
		pDC->SetTextColor(RGB(128,128,128));
		pDC->SetBkMode(TRANSPARENT);
	}
	else
	{
		pDC->SetTextColor(RGB(
			Constants::TextColourR.Get(),
			Constants::TextColourG.Get(),
			Constants::TextColourB.Get()));
		pDC->SetBkMode(TRANSPARENT);
	}

#if !RSG_FINAL && LAUNCHERDLG_MULTICOLOUR_BACKGROUNDS
	return CreateSolidBrush(RGB(rand()%255,rand()%255,rand()%255));
#else

	return (HBRUSH)GetStockObject(NULL_BRUSH);
#endif
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLauncherDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CLauncherDlg::OnLeftButton()
{
	m_uiShadowCopy.buttons[1].Call();
}

void CLauncherDlg::OnMiddleButton()
{
	m_uiShadowCopy.buttons[2].Call();
}
void CLauncherDlg::OnRightButton()
{
	m_uiShadowCopy.buttons[0].Call();
}

void CLauncherDlg::TextClicked()
{
	const std::string& supportUrl = CError::GetSupportURL();

	if (!supportUrl.empty())
	{
		SetFocus();
		ShellExecuteA(NULL, "open", supportUrl.c_str(), NULL, NULL, SW_SHOWNORMAL);
	}
}

void CLauncherDlg::OnClose()
{
	DEBUGF1("OnClose()");

	if (LauncherApp::IsGameExecutableRunning() && !m_bShownQuitConfirm)
	{
		struct Callback
		{
			static void Quit(void*)
			{
				PostMessageSafe(WM_CLOSE);
			}
			static void Minimize(void* ptr)
			{
				CLauncherDlg* launcher = (CLauncherDlg*)ptr;
				launcher->SetTextWithSpinner(IDS_GAME_RUNNING);
				PostMessageSafe(WM_SYSCOMMAND, SC_MINIMIZE, 0);
			}
		};
		if (CloudSaveManager::Instance()->IsCloudSaveEnabled())
		{
			SetTextWithButtons(IDS_HEADING_ERROR, IDS_CONFIRM_EXIT_CLOUD_SYNC, ButtonParams(IDS_BUTTON_CLOSE, Callback::Quit, NULL), ButtonParams(IDS_BUTTON_MINIMIZE, Callback::Minimize, this));
		}
		else
		{
			SetTextWithButtons(IDS_HEADING_ERROR, IDS_CONFIRM_EXIT_GENERAL, ButtonParams(IDS_BUTTON_CLOSE, Callback::Quit, NULL), ButtonParams(IDS_BUTTON_MINIMIZE, Callback::Minimize, this));
		}
		m_bShownQuitConfirm = true;
	}
	else
	{
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetLauncherVisibility, FALSE, FALSE);
		m_fsm.SetState(CFsm::eShuttingDown);
		CWnd::OnClose();
		PostQuitMessage(0);
	}
}

void CLauncherDlg::Exit()
{
	DEBUGF1("Exit()");
	OnClose();
}

void CLauncherDlg::OnOK()
{
	DEBUGF1("OnOK()");
	//@@: location CLAUNCHERDLG_ONOK_SET_SHUTDOWNS_STATE
	m_fsm.SetState(CFsm::eShuttingDown);
}

void CLauncherDlg::Shutdown()
{
	DEBUGF1("Shutdown()");

	DEBUGF1("Waiting for WorkerThread...");
	WorkerThread::Instance().Join();

	DEBUGF1("Waiting for Social Club...");
	m_socialclub.Join();

	//@@: location CLAUNCHERDLG_SHUTDOWN_EXIT_CONTROLLER_THREAD
	s_exitControllerThread = true;
	if (m_controllerThread)
	{
		DEBUGF1("Waiting for controller thread...");
		WaitForSingleObject(m_controllerThread->m_hThread, INFINITE);
		delete m_controllerThread;
		m_controllerThread = NULL;
	}

	DEBUGF1("Shutting down game communication...");
	CGameCommunication::Instance().StopListening();

	RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_EXITED);
}

u64 CLauncherDlg::GetCloudSaveBanTime()
{
	return m_socialclub.GetBanEndTime();
}

rgsc::RgscGamepad* CLauncherDlg::GetScuiPad(unsigned index) const
{
	return m_socialclub.GetScuiPad(index);
}

float CLauncherDlg::GetUiScale()
{
	return m_uiScale;
}

// Set of helper methods that keep the visible controls in sync
void CLauncherDlg::HideAllControls(bool includeDxSurface)
{
	//@@: location CLAUNCHERDLG_HIDEALLCONTROLS
	DEBUGF1("Hiding all controls.");
	m_status.ShowWindow(SW_HIDE);
	m_detailText.ShowWindow(SW_HIDE);
	m_detailText2.ShowWindow(SW_HIDE);
	m_spinner.ShowWindow(SW_HIDE);
	m_button[1].ShowWindow(SW_HIDE);
	m_button[2].ShowWindow(SW_HIDE);
	m_button[0].ShowWindow(SW_HIDE);
	m_progress.ShowWindow(SW_HIDE);

	m_button[1].EnableWindow(FALSE);
	m_button[2].EnableWindow(FALSE);
	m_button[0].EnableWindow(FALSE);

	if (includeDxSurface)
		m_surface->ShowWindow(SW_HIDE);
	else
		m_surface->SetFocus();
}

void CLauncherDlg::SetTextWithSpinner(UINT identifier)
{
	//@@: location CLAUNCHERDLG_SETTEXTWITHSPINNER_CREATE_TEXT
	std::wstring text = CLocalisation::Instance().GetWString(identifier);
	SetTextWithSpinner(text.c_str());
}

void CLauncherDlg::SetTextWithSpinner(const std::wstring& text)
{
	std::wstring title;
	CUtil::StdStringToStdWString(Constants::TitleNameLong, title);
	m_uiShadowCopy.heading = title;
	m_uiShadowCopy.spinnerVisible = true;
	m_uiShadowCopy.progressVisible = false;
	m_uiShadowCopy.status = text;
	m_uiShadowCopy.buttons[0].Reset();
	m_uiShadowCopy.buttons[1].Reset();
	m_uiShadowCopy.buttons[2].Reset();

	RefreshUiFromShadowCopy();
}

void CLauncherDlg::SetTextWithSpinner(UINT heading, UINT text)
{
	m_uiShadowCopy.heading = CLocalisation::Instance().GetWString(heading);
	m_uiShadowCopy.spinnerVisible = true;
	m_uiShadowCopy.progressVisible = false;
	m_uiShadowCopy.status = CLocalisation::Instance().GetWString(text);
	m_uiShadowCopy.buttons[0].Reset();
	m_uiShadowCopy.buttons[1].Reset();
	m_uiShadowCopy.buttons[2].Reset();

	RefreshUiFromShadowCopy();
}

void CLauncherDlg::SetTextWithSpinner(UINT identifier, ButtonParams rightButton)
{
	std::wstring title;
	CUtil::StdStringToStdWString(Constants::TitleNameLong, title);
	m_uiShadowCopy.heading = title;
	m_uiShadowCopy.spinnerVisible = true;
	m_uiShadowCopy.progressVisible = false;
	m_uiShadowCopy.status = CLocalisation::Instance().GetWString(identifier);
	m_uiShadowCopy.buttons[0] = rightButton;
	m_uiShadowCopy.buttons[1].Reset();
	m_uiShadowCopy.buttons[2].Reset();

	RefreshUiFromShadowCopy();
}

void CLauncherDlg::SetTextWithButton(UINT heading, const std::wstring& text, ButtonParams rightButton)
{
	if (heading > 0)
		m_uiShadowCopy.heading = CUtil::WideStringFromID(heading);
	else
		m_uiShadowCopy.heading = L"";
	m_uiShadowCopy.spinnerVisible = false;
	m_uiShadowCopy.progressVisible = false;
	m_uiShadowCopy.status = text;
	m_uiShadowCopy.buttons[0] = rightButton;
	m_uiShadowCopy.buttons[1].Reset();
	m_uiShadowCopy.buttons[2].Reset();

	RefreshUiFromShadowCopy();
}

void CLauncherDlg::SetTextWithButtons(UINT heading, UINT text, ButtonParams leftButton, ButtonParams rightButton)
{
	SetTextWithButtons(heading, CUtil::WideStringFromID(text), leftButton, rightButton);
}

void CLauncherDlg::SetTextWithButtons(UINT heading, const std::wstring& text, ButtonParams leftButton, ButtonParams rightButton)
{
	if (heading > 0)
		m_uiShadowCopy.heading = CUtil::WideStringFromID(heading);
	else
		m_uiShadowCopy.heading = L"";

	m_uiShadowCopy.spinnerVisible = false;
	m_uiShadowCopy.progressVisible = false;
	m_uiShadowCopy.status = text;
	m_uiShadowCopy.buttons[0] = rightButton;
	m_uiShadowCopy.buttons[1] = leftButton;
	m_uiShadowCopy.buttons[2].Reset();

	RefreshUiFromShadowCopy();
}

void CLauncherDlg::SetTextWithButtons(UINT heading, UINT text, ButtonParams leftButton, ButtonParams middleButton, ButtonParams rightButton)
{
	SetTextWithButtons(heading, CUtil::WideStringFromID(text), leftButton, middleButton, rightButton);
}

void CLauncherDlg::SetTextWithButtons(UINT heading, const std::wstring& text, ButtonParams leftButton, ButtonParams middleButton, ButtonParams rightButton)
{
	if (heading > 0)
		m_uiShadowCopy.heading = CUtil::WideStringFromID(heading);
	else
		m_uiShadowCopy.heading = L"";

	m_uiShadowCopy.spinnerVisible = false;
	m_uiShadowCopy.progressVisible = false;
	m_uiShadowCopy.status = text;
	m_uiShadowCopy.buttons[0] = rightButton;
	m_uiShadowCopy.buttons[1] = leftButton;
	m_uiShadowCopy.buttons[2] = middleButton;

	RefreshUiFromShadowCopy();
}

void CLauncherDlg::ProgressCancelCallback(void*)
{
	CLauncherDlg::GetLastInstance()->Exit();
}

void CLauncherDlg::SetProgressBar(UINT heading, UINT text)
{
	SetProgressBar(heading, CUtil::WideStringFromID(text));
}

void CLauncherDlg::SetProgressBar(UINT heading, const std::wstring& text)
{
	if (heading > 0)
		m_uiShadowCopy.heading = CUtil::WideStringFromID(heading);
	else
		m_uiShadowCopy.heading = L"";

	m_uiShadowCopy.spinnerVisible = false;
	m_uiShadowCopy.progressVisible = true;
	m_uiShadowCopy.status = text;
	m_uiShadowCopy.buttons[0] = ButtonParams(CUtil::WideStringFromID(IDS_BUTTON_CANCEL).c_str(), ProgressCancelCallback, NULL);
	m_uiShadowCopy.buttons[1].Reset();
	m_uiShadowCopy.buttons[2].Reset();

	RefreshUiFromShadowCopy();
}

void CLauncherDlg::SetProgressBar(UINT heading, UINT text, ButtonParams leftButton)
{
	SetProgressBar(heading, CUtil::WideStringFromID(text), leftButton);
}

void CLauncherDlg::SetProgressBar(UINT heading, const std::wstring& text, ButtonParams leftButton)
{
	if (heading > 0)
		m_uiShadowCopy.heading = CUtil::WideStringFromID(heading);
	else
		m_uiShadowCopy.heading = L"";

	m_uiShadowCopy.spinnerVisible = false;
	m_uiShadowCopy.progressVisible = true;
	m_uiShadowCopy.status = text;
	m_uiShadowCopy.buttons[0] = ButtonParams(CUtil::WideStringFromID(IDS_BUTTON_CANCEL).c_str(), ProgressCancelCallback, NULL);
	m_uiShadowCopy.buttons[1] = leftButton;
	m_uiShadowCopy.buttons[2].Reset();

	RefreshUiFromShadowCopy();
}

void CLauncherDlg::SetStatusTextSafe(const std::wstring& text)
{
	EnterCriticalSection(&statusUpdateLock);

	statusUpdateString = text;

	LeaveCriticalSection(&statusUpdateLock);

	if (!PostMessageSafe(eUpdateStatus))
	{
		DEBUGF3("Failed setting text (safe).");
	}
}

void CLauncherDlg::RefreshUiFromShadowCopy(bool force)
{
	if (!force && m_uiShadowCopy == m_uiPreviousShadowCopy)
	{
		//DEBUGF1("(Ignoring UI refresh request.)");
		return;
	}

	if (force)
	{
		DEBUGF1("Forcing UI refresh.");
	}
	else
	{
		//DEBUGF1("Enacting UI refresh.");
	}

	RAD_TELEMETRY_WAIT_ZONE("RefreshUiFromShadowCopy");

	if (
#if !RSG_FINAL
		Globals::useScuiFrontend && 
#endif
		m_socialclub.IsReadyForScuiMessages())
	{
		RefreshUsingSCUI(force);
	}
	else
	{
		RefreshUsingMFC();
	}

	SetLauncherVisibility(true);
}

void CLauncherDlg::RefreshUsingMFC()
{
	RAD_TELEMETRY_ZONE("RefreshUsingMFC");
	DEBUGF1("RefreshUsingMFC");

	m_surface->ShowWindow(SW_HIDE);

	if (m_uiShadowCopy.spinnerVisible)
	{
		m_status.ShowWindow(SW_SHOW);
		m_detailText2.ShowWindow(SW_HIDE);
		m_detailText.ShowWindow(SW_HIDE);

		m_spinner.ShowWindow(SW_SHOW);
		m_progress.ShowWindow(SW_HIDE);
		//@@: location CLAUNCHERDLG_SETTEXTWITHBUTTONS_SETDLGITEMTEXT
		SetDlgItemText(IDC_STATUS, m_uiShadowCopy.status.c_str());
	}
	else if (m_uiShadowCopy.progressVisible)
	{
		m_status.ShowWindow(SW_HIDE);
		m_detailText.ShowWindow(SW_SHOW);
		m_detailText2.ShowWindow(SW_HIDE);

		m_progress.ShowWindow(SW_SHOW);
		m_spinner.ShowWindow(SW_HIDE);

		SetDlgItemText(IDC_DETAILTEXT, m_uiShadowCopy.status.c_str());

		m_progress.SetPos((int)m_uiShadowCopy.progressPercent);
		m_progress.Invalidate();

		if (!m_uiShadowCopy.status.empty())
		{
			// Find current sizes
			RECT rectOriginal = GetControlRect(IDC_DETAILTEXT);

			// Determine minimum text-box size
			CClientDC cdc(&m_detailText);

			RECT rectNeeded = rectOriginal;
			rectNeeded.left += 10;
			rectNeeded.right -= 10;
			cdc.SelectObject(m_detailFont);
			cdc.DrawText(m_uiShadowCopy.status.c_str(), (int)m_uiShadowCopy.status.size(), &rectNeeded, DT_CALCRECT | DT_CENTER | DT_WORDBREAK | DT_EXTERNALLEADING | DT_EDITCONTROL);

			// Adjust this to a fixed width, and ensure the bottom line is kept the same
			rectNeeded.left = rectOriginal.left;
			rectNeeded.right = rectOriginal.right;

			int height = rectNeeded.bottom - rectNeeded.top;
			rectNeeded.top = rectOriginal.top + ((rectOriginal.bottom - rectOriginal.top) - height) / 2;
			rectNeeded.bottom = rectNeeded.top + height;

			// Apply new positions / sizes
			m_detailText.SetWindowPos(NULL, rectNeeded.left, rectNeeded.top, rectNeeded.right-rectNeeded.left, rectNeeded.bottom-rectNeeded.top, SWP_NOOWNERZORDER | SWP_NOZORDER);
		}

	}
	else
	{
		m_status.ShowWindow(SW_HIDE);
		m_detailText.ShowWindow(SW_HIDE);
		m_detailText2.ShowWindow(SW_SHOW);

		m_spinner.ShowWindow(SW_HIDE);
		m_progress.ShowWindow(SW_HIDE);

		SetDlgItemText(IDC_DETAILTEXT2, m_uiShadowCopy.status.c_str());

		if (!m_uiShadowCopy.status.empty())
		{
			// Find current sizes
			RECT rectOriginal = GetControlRect(IDC_DETAILTEXT2);

			// Determine minimum text-box size
			CClientDC cdc(&m_detailText2);

			RECT rectNeeded = rectOriginal;
			rectNeeded.left += 10;
			rectNeeded.right -= 10;
			cdc.SelectObject(m_detailFont);
			cdc.DrawText(m_uiShadowCopy.status.c_str(), (int)m_uiShadowCopy.status.size(), &rectNeeded, DT_CALCRECT | DT_CENTER | DT_WORDBREAK | DT_EXTERNALLEADING | DT_EDITCONTROL);

			// Adjust this to a fixed width, and ensure the bottom line is kept the same
			rectNeeded.left = rectOriginal.left;
			rectNeeded.right = rectOriginal.right;

			int height = rectNeeded.bottom - rectNeeded.top;
			rectNeeded.top = rectOriginal.top + ((rectOriginal.bottom - rectOriginal.top) - height) / 2;
			rectNeeded.bottom = rectNeeded.top + height;

			// Apply new positions / sizes
			m_detailText2.SetWindowPos(NULL, rectNeeded.left, rectNeeded.top, rectNeeded.right-rectNeeded.left, rectNeeded.bottom-rectNeeded.top, SWP_NOOWNERZORDER | SWP_NOZORDER);
		}
	}

	if (m_uiShadowCopy.buttons[0].IsValid())
	{
		m_button[0].ShowWindow(SW_SHOW);
		m_button[0].EnableWindow(TRUE);
		SetDlgItemText(IDC_RIGHTBUTTON, m_uiShadowCopy.buttons[0].GetTitle().c_str());
	}
	else
	{
		m_button[0].ShowWindow(SW_HIDE);
		m_button[0].EnableWindow(FALSE);
	}

	if (m_uiShadowCopy.buttons[1].IsValid())
	{
		m_button[1].ShowWindow(SW_SHOW);
		m_button[1].EnableWindow(TRUE);
		SetDlgItemText(IDC_LEFTBUTTON, m_uiShadowCopy.buttons[1].GetTitle().c_str());
	}
	else
	{
		m_button[1].ShowWindow(SW_HIDE);
		m_button[1].EnableWindow(FALSE);
	}

	if (m_uiShadowCopy.buttons[2].IsValid())
	{
		m_button[2].ShowWindow(SW_SHOW);
		m_button[2].EnableWindow(TRUE);
		SetDlgItemText(IDC_MIDDLEBUTTON, m_uiShadowCopy.buttons[2].GetTitle().c_str());
	}
	else
	{
		m_button[2].ShowWindow(SW_HIDE);
		m_button[2].EnableWindow(FALSE);
	}

	RedrawWindow(NULL, NULL, RDW_ALLCHILDREN | RDW_INVALIDATE | RDW_ERASE);

	m_uiPreviousShadowCopy = m_uiShadowCopy;
}

void CLauncherDlg::RefreshUsingSCUI(bool force)
{
	if (!m_socialclub.IsReadyForScuiMessages())
	{
		DEBUGF1("Ignoring SCUI update as it's not ready.");
		return;
	}

	RAD_TELEMETRY_ZONE("RefreshUsingSCUI");

	HideAllControls(false);
	
	m_surface->ShowWindow(SW_SHOW);

	std::string command = "{\"Commands\":[";

	bool changeMade = false;

	std::string convertString;

	const float progressEpsilon = 1.0f / 16.0f;

	if (force
		|| m_uiShadowCopy.status != m_uiPreviousShadowCopy.status
		|| m_uiShadowCopy.spinnerVisible != m_uiPreviousShadowCopy.spinnerVisible
		|| m_uiShadowCopy.progressVisible != m_uiPreviousShadowCopy.progressVisible
		|| (m_uiShadowCopy.progressVisible && (m_uiShadowCopy.progressPercent > m_uiPreviousShadowCopy.progressPercent + progressEpsilon || m_uiShadowCopy.progressPercent < m_uiPreviousShadowCopy.progressPercent - progressEpsilon))
		|| m_uiShadowCopy.heading != m_uiPreviousShadowCopy.heading)
	{
		

		CUtil::WStringToStdString(m_uiShadowCopy.status, convertString);

		//@@: location CLAUNCHERDLG_REFRESHUSINGSCUI_CHECK_SPINNER_VISIBLE
		if (m_uiShadowCopy.spinnerVisible)
		{
			command += "{\"Command\":\"SetSpinnerText\",\"Parameter\":\"";
			CUtil::AppendStringEscapingQuotes(command, convertString);
			command += "\"}";
		}
		else if (m_uiShadowCopy.progressVisible)
		{
			command += "{\"Command\":\"SetProgressText\",\"Parameter\":\"";
			CUtil::AppendStringEscapingQuotes(command, convertString);
			command += "\"},";

			command += "{\"Command\":\"SetProgress\", \"Parameter\":";
			char buffer[10];
			sprintf_s(buffer, "%.3f", m_uiShadowCopy.progressPercent / 100.0f);
			command += buffer;
			command += "}";
		}
		else
		{
			command += "{\"Command\":\"SetText\",\"Parameter\":\"";
			CUtil::AppendStringEscapingQuotes(command, convertString);
			command += "\"}";
		}

		if (!m_uiShadowCopy.heading.empty())
		{
			CUtil::WStringToStdString(m_uiShadowCopy.heading, convertString);
			command += ",{\"Command\":\"SetHeaderText\",\"Parameter\":\"";
			CUtil::AppendStringEscapingQuotes(command, convertString);
			command += "\"}";
		}

		changeMade = true;
	}

	bool buttonDiffers = false;
	for (int i = 0; i < 3; i++)
	{
		if (m_uiShadowCopy.buttons[i] != m_uiPreviousShadowCopy.buttons[i])
		{
			buttonDiffers = true;
			break;
		}
	}

	if (buttonDiffers)
	{
		if (changeMade)
		{
			command += ",";
		}

		command += "{\"Command\":\"SetButtons\",\"Parameter\":[";

		bool buttonAdded = false;

		for (int i = 0; i < 3; i++)
		{
			if (m_uiShadowCopy.buttons[i].IsValid())
			{
				if (buttonAdded)
				{
					command += ",\"";
				}
				else
				{
					buttonAdded = true;
					command += "\"";
				}

				CUtil::WStringToStdString(m_uiShadowCopy.buttons[i].GetTitle(), convertString);
				CUtil::AppendStringEscapingQuotes(command, convertString);

				command += "\"";
			}
		}

		command += "]}";
		changeMade = true;

	}


	{
		AUTOCRITSEC(statusUpdateLock);

		if (m_versionString.empty())
			UpdateVersionString();

		if (changeMade)
		{
			command += ",";
		}
		else
		{
			changeMade = true;
		}
		command += "{ \"Command\" : \"SetVersionText\", \"Parameter\": \"";
		command += m_versionString;
		command += "\" }";
	}

	command += "]}";

	if (changeMade)
	{
		m_socialclub.SendMessageToScui(command.c_str());
		m_uiPreviousShadowCopy = m_uiShadowCopy;
	}
	else
	{
		DEBUGF1("Not sending a UI update message to SCUI (nothing has changed).");
	}

}

void CLauncherDlg::InitializeEntitlementChecker()
{
#if !RSG_PRELOADER_LAUNCHER
	//@@: range CLAUNCHERDLG_INITIALIZEENTITLEMENTCHECKER {
	m_socialclub.RunEntitlementCheck();
	//@@: } CLAUNCHERDLG_INITIALIZEENTITLEMENTCHECKER
	//@@: location CLAUNCHERDLG_INITIALIZEENTITLEMENTCHECKER_EXIT
#endif

}

void CLauncherDlg::SetSocialClubVisible()
{
	m_bSocialClubInitialized = m_socialclub.Initialize(m_surface, GetSafeHwnd());

#if !RSG_FINAL
	if (Globals::autotest_launcher_selfsignin)
	{
		static HWND hwnd;

		struct SelfSignin
		{
			static UINT ThreadEntry(LPVOID /*param*/)
			{
				Sleep(20000);

				::SetForegroundWindow(CLauncherDlg::GetLastInstance()->GetSafeHwnd());

				Sleep(1000);

				//HWND hwnd = m_surface->GetSafeHwnd();
				WNDPROC proc = (WNDPROC)GetWindowLongPtrA(hwnd, GWLP_WNDPROC);
				
				int x = 30;
				int y = 160;
				{
					DEBUGF1("Clicking at (%d,%d)", x, y);

					CallWindowProc(proc, hwnd, WM_MOUSEMOVE, 0, MAKELPARAM(x,y));

					Sleep(10);

					CallWindowProc(proc, hwnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(x,y));

					Sleep(10);

					CallWindowProc(proc, hwnd, WM_MOUSEMOVE, MK_LBUTTON, MAKELPARAM(x+1,y));

					Sleep(10);

					CallWindowProc(proc, hwnd, WM_LBUTTONUP, 0, MAKELPARAM(x,y));

					Sleep(100);
				}

				Sleep(1000);

				const char* str = "launcher_test@rockfoo.com\trockstar1A\n";

				for (int i = 0; i < strlen(str); i++)
				{
					char c = str[i];

					if (c == '\t')
					{
						PostMessageSafe(WM_KEYDOWN, (WPARAM)VK_TAB, 0);
						PostMessageSafe(WM_KEYUP, (WPARAM)VK_TAB, 0);
					}
					else if (c == '\n')
					{
						PostMessageSafe(WM_KEYDOWN, (WPARAM)VK_RETURN, 0);
						PostMessageSafe(WM_KEYUP, (WPARAM)VK_RETURN, 0);
					}
					else
					{
						PostMessageSafe(WM_CHAR, (WPARAM)c, 0);
					}
					Sleep(100);
				}

				return 0;
			}
		};

		hwnd = m_surface->GetSafeHwnd();
		AfxBeginThread(SelfSignin::ThreadEntry, NULL);
	}
#endif
}

void CLauncherDlg::ShowCodeRedemptionUI()
{
	if (m_bSocialClubInitialized)
	{
		m_socialclub.ShowCodeRedemptionUI();
	}
}

void CLauncherDlg::ShutdownSocialClub()
{
	if (m_bSocialClubInitialized)
	{
		m_socialclub.RequestShutdown();
		m_bSocialClubInitialized = false;
	}
	else
	{
		PostMessageSafe(eNextState);
	}
}

void CLauncherDlg::SetSocialClubHotkey(bool bEnable)
{
	//@@: location CLAUNCHERDLG_SETSOCIALCLUBHOTKEY
	m_socialclub.SetSocialClubHotkey(bEnable);
}

void CLauncherDlg::OnExitCodeError()
{
	m_socialclub.OnExitError();
}

void CLauncherDlg::UpdateSocialClubTicket(rgsc::RockstarId rockstarId, std::string base64Ticket)
{
	m_socialclub.UpdateSocialClubTicket(rockstarId, base64Ticket);
}

void CLauncherDlg::GameSignOutEvent()
{
	m_socialclub.GameSignOutEvent();
}

void CLauncherDlg::GameSignInEvent(rgsc::RockstarId rockstarId)
{
	m_socialclub.GameSignInEvent(rockstarId);
}

void CLauncherDlg::CreateSignInTransferFile()
{
	m_socialclub.CreateSignInTransferFile();
}

CFsm::FsmState CLauncherDlg::GetCurrentState()
{
	return m_fsm.GetState();
}

bool CLauncherDlg::LoadImageFromResource(CImage& image, int id)
{
	HRSRC resource = FindResource(NULL, MAKEINTRESOURCE(id), RT_RCDATA);
	if (resource == NULL)
	{
		ERRORF("Can't find background image resource!");
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	HGLOBAL memory = LoadResource(NULL, resource);
	if (memory == NULL)
	{
		ERRORF("Can't load background image resource!");
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	DWORD size = SizeofResource(NULL, resource);
	if (size == 0)
	{
		ERRORF("Zero-length background image resource!");
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	LPVOID ptr = LockResource(memory);
	if (ptr == NULL)
	{
		ERRORF("Can't lock background image resource!");
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, size);
	if (!hGlobal)
	{
		ERRORF("Can't allocate background image memory!");
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	void* dest = GlobalLock(hGlobal);
	if (!dest)
	{
		ERRORF("Can't allocate background image memory!");
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	CopyMemory(dest, ptr, size);

	IStream* pStream = NULL;
	if (CreateStreamOnHGlobal(hGlobal, TRUE, &pStream) != S_OK)
	{
		ERRORF("Can't CreateStreamOnHGlobal for background image!");
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	image.Destroy();
	image.Load(pStream);

	pStream->Release();

	GlobalUnlock(hGlobal);
	GlobalFree(hGlobal);

	return true;
}

void CLauncherDlg::SetBackgroundImage()
{
	RAD_TELEMETRY_ZONE("SetBackgroundImage");

	HRESULT imageLoaded = -1;
	
#if !RSG_FINAL
	m_imageBackground.Load(Constants::BackgroundImageFilename.Get().c_str());
#endif

	if (!SUCCEEDED(imageLoaded))
	{
		LoadImageFromResource(m_imageBackground, IDR_LAUNCHER_BACKGROUND);
	}

	m_imageBackground.SetHasAlphaChannel(false);
	m_imageBackground.SetTransparentColor(RGB(255,0,255));

	RedrawWindow(NULL, NULL, RDW_ALLCHILDREN | RDW_INVALIDATE | RDW_ERASENOW | RDW_ERASE);
}

void CLauncherDlg::SetLauncherVisibility(bool visible, bool animate)
{
	if (m_bVisible != visible)
	{
		if (visible)
		{
			//@@: location CLAUNCHERDLG_SETLAUNCHERVISIBILITY_TRUE
			if (animate)
			{
				AnimateWindow(500, AW_BLEND);
			}
			else
			{
				ShowWindow(SW_SHOW);
			}
		}
		else
		{
			if (animate)
			{
				AnimateWindow(500, AW_HIDE | AW_BLEND);
			}
			else
			{
				ShowWindow(SW_HIDE);
			}
		}
		m_bVisible = visible;
	}
	
#if !RSG_PRELOADER_LAUNCHER && ENABLE_TAMPER_ACTIONS
	if (g_bShowWindow && !visible)
	{
		SetLauncherVisibility(!visible, false);
	}
#endif
}

CLauncherDlg::ButtonParams::ButtonParams(UINT title, void (*fn)(void*), void* param)
	: m_title(CUtil::WideStringFromID(title)), m_fn(fn), m_param(param)
{
}

CLauncherDlg::ButtonParams::ButtonParams(LPCWSTR title, void (*fn)(void*), void* param)
	: m_title(title), m_fn(fn), m_param(param)
{
}

CLauncherDlg::ButtonParams::ButtonParams(const std::wstring& title, void (*fn)(void*), void* param)
	: m_title(title), m_fn(fn), m_param(param)
{
}

void CLauncherDlg::ButtonParams::Set(const std::wstring& title, void (*fn)(void*), void* param)
{
	m_title = title;
	m_fn = fn;
	m_param = param;
}

void CLauncherDlg::ButtonParams::Call()
{
	if (m_fn)
	{
		m_fn(m_param);
	}
	else
	{
		ERRORF("No handler for clicked button!");
	}
}

BOOL CLauncherDlg::PostMessageSafe(UINT message, WPARAM wParam, LPARAM lParam)
{
	//@@: location CLAUNCHERDLG_POSTMESSAGESAFE_GETLASTINSTANCE
	CLauncherDlg* dlg = GetLastInstance();

	if (!dlg || !dlg->GetSafeHwnd())
	{
		return FALSE;
	}
	else
	{
		return dlg->PostMessage(message, wParam, lParam);
	}
}

LRESULT CLauncherDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_KEYDOWN)
	{
		DEBUGF1("LauncherDlg keydown: %d", wParam);
	}
	return CWnd::WindowProc(message, wParam, lParam);
}

LRESULT CLauncherDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_KEYDOWN)
	{
		DEBUGF1("LauncherDlg Def keydown: %d", wParam);
	}
	return CWnd::DefWindowProc(message, wParam, lParam);
}

BOOL CLauncherDlg::PreTranslateMessage(MSG* pMsg)
{
	bool passToSCUI = false;
	if (pMsg->message == WM_KEYDOWN || pMsg->message == WM_KEYUP || pMsg->message == WM_CHAR)
	{
		DEBUGF1("LauncherDlg PreTranslate key message: %d", pMsg->wParam);
		TranslateMessage(pMsg);
		passToSCUI = true
#if !RSG_PRELOADER_LAUNCHER&& ENABLE_TAMPER_ACTIONS
			&& (rand() / (float)RAND_MAX > g_eatDupKeyProb)
#endif
			;
	}
	else if (pMsg->message == WM_MOUSEMOVE && (pMsg->hwnd == m_hWnd || pMsg->hwnd == m_surface->GetSafeHwnd()))
	{
		// Get the mouse position (in window client coordinates)
		POINT mouse_window_coords;
		mouse_window_coords.x = GET_X_LPARAM(pMsg->lParam);
		mouse_window_coords.y = GET_Y_LPARAM(pMsg->lParam);

		// Convert to screen coordinates
		POINT mouse_screen_coords = mouse_window_coords;
		ClientToScreen(&mouse_screen_coords);

		if (pMsg->wParam & MK_LBUTTON)
		{
			if (m_lastDragCoordsValid)
			{
				int deltaX = mouse_screen_coords.x - m_lastMouseDragScreenCoords.x;
				int deltaY = mouse_screen_coords.y - m_lastMouseDragScreenCoords.y;

				// Ignore very slight movement or ridiculously fast movement
				const int deadzone = 0, maxMove = 200;

				int absX = abs(deltaX);
				int absY = abs(deltaY);

				if ((absX > deadzone || absY > deadzone) && absX < maxMove && absY < maxMove)
				{
					DEBUGF3("Mouse dragging by %d, %d", deltaX, deltaY);

					// Get current window position
					RECT rect;
					GetWindowRect(&rect);
				
					// Set the new position, without changing the size or z-order
					SetWindowPos(NULL, rect.left + deltaX, rect.top + deltaY, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
				}
			}
			else if (mouse_window_coords.y < 64)
			{
				m_lastDragCoordsValid = true;
				SetCapture();
				m_mouseCaptured = true;
			}

			m_lastMouseDragScreenCoords = mouse_screen_coords;
			
		}
		else
		{
			m_lastDragCoordsValid = false;
			if (m_mouseCaptured)
			{
				ReleaseCapture();
				m_mouseCaptured = false;
				passToSCUI = true;
			}
		}
	}
	else if (pMsg->message == WM_LBUTTONUP)
	{
		if (m_mouseCaptured)
		{
			ReleaseCapture();
			m_mouseCaptured = false;
			passToSCUI = true;
		}
	}
	else if (pMsg->message == WM_CAPTURECHANGED && ((HWND)pMsg->lParam != pMsg->hwnd))
	{
		m_lastDragCoordsValid = false;
	}

	if (pMsg->message == WM_MOUSEWHEEL)
	{
		passToSCUI = true;
	}

	if (passToSCUI)
	{
		RAD_TELEMETRY_ZONE("PreTranslateMessage - passToSCUI");
		HWND hwnd = m_surface->GetSafeHwnd();
		WNDPROC proc = (WNDPROC)GetWindowLongPtrA(hwnd, GWLP_WNDPROC);

		CallWindowProc(proc, hwnd, pMsg->message, pMsg->wParam, pMsg->lParam);

		return TRUE;
	}
	else
	{
		return CWnd::PreTranslateMessage(pMsg);
	}
}

static inline u8 RemapAxis(short axis) {  return (u8)((axis >> 8) + 128); }
static inline bool AnaDown(u8 b) { return b >= 64; }

UINT CLauncherDlg::ControllerMonitorThread(LPVOID param)
{
	CUtil::SetCurrentThreadName("ControllerMonitorThread");
	DEBUGF1("Controller monitor thread launched.");

	CLauncherDlg* dlg = (CLauncherDlg*)param;

	int lastDirection = 0;
	WORD lastButtons = 0;

	s_controllerThreadAlive = true;

	// TODO: Do we want to stop / throttle this while the game is running?
	while (!s_exitControllerThread)
	{
		if (LauncherApp::IsGameExecutableRunning())
		{
			Sleep(1000);
			continue;
		}

		// Handle controller movement (either stick)
		int currentDirection = 0;
		WORD currentButtons = 0;


		// Sum the input from all sources
		for (int i = 0; i < XUSER_MAX_COUNT; i++)
		{
			XINPUT_STATE state;
			ZeroMemory(&state, sizeof(XINPUT_STATE));

			DWORD dwResult = XInputGetState(i, &state);

			if (dwResult == ERROR_SUCCESS)
			{
				// Controller is connected.
				if (state.Gamepad.sThumbLX < -8192)
					currentDirection--;
				else if (state.Gamepad.sThumbLX > 8192)
					currentDirection++;

				if (state.Gamepad.sThumbRX < -8192)
					currentDirection--;
				else if (state.Gamepad.sThumbRX > 8192)
					currentDirection++;

				if ((state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) == XINPUT_GAMEPAD_DPAD_LEFT)
					currentDirection--;
				if ((state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) == XINPUT_GAMEPAD_DPAD_RIGHT)
					currentDirection++;

				currentButtons |= state.Gamepad.wButtons;
			}

			if (dlg->IsSocialClubInitialized())
			{
				rgsc::RgscGamepad* scuiPad = dlg->GetScuiPad(i);
				if (scuiPad)
				{
					scuiPad->SetIsConnected(dwResult == ERROR_SUCCESS);

					if (scuiPad->IsConnected())
					{
						u32 buttons = 0;
						
						// Map xinput to RGSC
						if (state.Gamepad.wButtons & XINPUT_GAMEPAD_A) buttons |= rgsc::IGamepadV1::RGSC_RDOWN;
						if (state.Gamepad.wButtons & XINPUT_GAMEPAD_B) buttons |= rgsc::IGamepadV1::RGSC_RRIGHT;
						if (state.Gamepad.wButtons & XINPUT_GAMEPAD_X) buttons |= rgsc::IGamepadV1::RGSC_RLEFT;
						if (state.Gamepad.wButtons & XINPUT_GAMEPAD_Y) buttons |= rgsc::IGamepadV1::RGSC_RUP;
						if (state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) buttons |= rgsc::IGamepadV1::RGSC_L1;
						if (state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) buttons |= rgsc::IGamepadV1::RGSC_R1;
						if (state.Gamepad.wButtons & XINPUT_GAMEPAD_START) buttons |= rgsc::IGamepadV1::RGSC_START;
						if (state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK) buttons |= rgsc::IGamepadV1::RGSC_SELECT;
						if (state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB) buttons |= rgsc::IGamepadV1::RGSC_L3;
						if (state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB) buttons |= rgsc::IGamepadV1::RGSC_R3;
						if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP) buttons |= rgsc::IGamepadV1::RGSC_LUP;
						if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) buttons |= rgsc::IGamepadV1::RGSC_LDOWN;
						if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) buttons |= rgsc::IGamepadV1::RGSC_LLEFT;
						if (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) buttons |= rgsc::IGamepadV1::RGSC_LRIGHT;
						if (AnaDown(state.Gamepad.bLeftTrigger)) buttons |= rgsc::IGamepadV1::RGSC_L2;
						if (AnaDown(state.Gamepad.bRightTrigger)) buttons |= rgsc::IGamepadV1::RGSC_R2;

						// Setup pad
						scuiPad->SetButtons(buttons);
						scuiPad->SetAxis(0, RemapAxis(state.Gamepad.sThumbLX));
						scuiPad->SetAxis(1, (rgsc::u8)~RemapAxis(state.Gamepad.sThumbLY));
						scuiPad->SetAxis(2, RemapAxis(state.Gamepad.sThumbRX));
						scuiPad->SetAxis(3, (rgsc::u8)~RemapAxis(state.Gamepad.sThumbRY));
						scuiPad->SetHasInput();
					}
				}
			}
		}

		// Handle controller movement
		if (lastDirection == 0 && currentDirection != 0)
		{
			if (currentDirection < 0)
			{
				dlg->PostMessage(eHandleControllerEvent, CONTROLLER_LEFT);
			}
			else if (currentDirection > 0)
			{
				dlg->PostMessage(eHandleControllerEvent, CONTROLLER_RIGHT);
			}
		}

		lastDirection = currentDirection;


		// Handle controller buttons
		WORD previouslyUnpressedButtons = ~lastButtons;
		WORD newlyPressedButtons = previouslyUnpressedButtons & currentButtons;
		if ((newlyPressedButtons & XINPUT_GAMEPAD_A) == XINPUT_GAMEPAD_A)
		{
			dlg->PostMessage(eHandleControllerEvent, CONTROLLER_ACCEPT);
		}
		else if ((newlyPressedButtons & XINPUT_GAMEPAD_B) == XINPUT_GAMEPAD_B)
		{
			dlg->PostMessage(eHandleControllerEvent, CONTROLLER_CANCEL);
		}

		lastButtons = currentButtons;

		Sleep(33);
	}

	s_controllerThreadAlive = false;

	return 0;
}

void CLauncherDlg::CheckFirstRun()
{
	// Get marker file path
	std::wstring firstRunFilePath;

	PWSTR pszPath = NULL;
	//@@: location CLAUNCHERDLG_CHECKFIRSTRUN_GETPATH
	HRESULT hr = SHGetKnownFolderPath(FOLDERID_LocalAppData, 0, NULL, &pszPath);

	if (SUCCEEDED(hr))
	{
		firstRunFilePath = std::wstring(pszPath) + Constants::AppDataSubfolderCompanyName.Get() + Constants::AppDataSubfolderProductName.Get() +std::wstring(L"\\silentlauncher");
	}
	else
	{
		return;
	}

	CoTaskMemFree(pszPath);


	// Check if file exists
	DWORD attribs = GetFileAttributesW(firstRunFilePath.c_str());

	if (attribs != INVALID_FILE_ATTRIBUTES && !(attribs &FILE_ATTRIBUTE_DIRECTORY))
	{
		// File exists; set silent mode (unless a flag was passed in for it)
		if (!Globals::silent.WasSetFromCommandline())
		{
			Globals::silent = true;
		}
	}
	else
	{
		// Create file
		std::ofstream file;
		file.open(firstRunFilePath.c_str(), std::ios::binary | std::ofstream::out);
		if (file.good())
		{
			file.close();
		}
		else
		{
			ERRORF("Unable to write first-run file.");
		}
	}
}

rgsc::RockstarId CLauncherDlg::GetSignedInRockstarId()
{
	//@@: range CLAUNCHERDLG_GETSIGNEDINROCKSTARID {
	return m_socialclub.GetRockstarId();
	//@@: } CLAUNCHERDLG_GETSIGNEDINROCKSTARID

}

bool CLauncherDlg::IsSocialClubOnline()
{
	rtry
	{
		rcheck(m_socialclub.IsInitialized(), catchall, );
		rcheck(m_socialclub.GetProfileManager(), catchall, );
		rcheck(m_socialclub.GetProfileManager()->IsOnline(), catchall, );
		return true;
	}
	rcatchall
	{
		return false;
	}
}

void CLauncherDlg::ShowFolderPicker(const std::wstring& /*startingLocation*/, std::wstring& out_pickedLocation)
{
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
	if (hr == RPC_E_CHANGED_MODE)
	{
		hr = CoInitializeEx(NULL, COINIT_MULTITHREADED | COINIT_DISABLE_OLE1DDE);
	}

	if (FAILED(hr))
	{
		ERRORF("Unable to initialize COM!");
		return;
	}

	IFileDialog* fileDialog = NULL;
	if (SUCCEEDED(hr))
	{
		hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&fileDialog));
	}

	if (SUCCEEDED(hr))
	{
		DWORD options = 0;
		fileDialog->GetOptions(&options);
		fileDialog->SetOptions(options | FOS_PICKFOLDERS);
		hr = fileDialog->Show(NULL);
	}

	IShellItem* result = NULL;
	if (SUCCEEDED(hr))
	{
		hr = fileDialog->GetResult(&result);
	}

	LPWSTR pathWstring = NULL;
	if (SUCCEEDED(hr))
	{
		hr = result->GetDisplayName(SIGDN_FILESYSPATH, &pathWstring);
	}

	if (SUCCEEDED(hr))
	{
		out_pickedLocation = pathWstring;
		if (!out_pickedLocation.empty() && out_pickedLocation.back() != L'\\' && out_pickedLocation.back() != L'/')
		{
			out_pickedLocation += L'/';
		}
	}

	if (pathWstring)
		CoTaskMemFree(pathWstring);

	if (result)
		result->Release();

	if (fileDialog)
		fileDialog->Release();

	CoUninitialize();

	if (FAILED(hr))
	{
		ERRORF("Unable to show folder-picker!");
	}
}
