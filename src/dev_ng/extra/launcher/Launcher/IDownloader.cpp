#include "StdAfx.h"
#include "IDownloader.h"
#include "IPatchCheck.h"

IDownloader::IDownloader(void)
{
	//@@: location IDOWNLOADER_CONSTRUCTOR
	m_listener = NULL;
}

IDownloader::~IDownloader(void)
{
}

void IDownloader::SetListener(DownloadListener* listener)
{
	//@@: location IDOWNLOADER_SETLISTENER
	m_listener = listener;
}

bool IDownloader::HasErrorOccurred()
{
	return m_error.IsError();
}

const CError& IDownloader::GetError()
{
	return m_error;
}
