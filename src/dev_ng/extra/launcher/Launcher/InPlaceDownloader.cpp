#include "stdafx.h"

#include "InPlaceDownloader.h"

#if RSG_IN_PLACE_DOWNLOAD

#include "resource.h"
#include "Globals.h"
#include "Config.h"
#include "OnlineConfig.h"
#include "GamePatchCheck.h"
#include "RockstarDownloader.h"
#include "LauncherDlg.h"
#include "DownloadManager.h"
#include "ProgressCalculator.h"
#include "SocialClub.h"
#include "VersionManager.h"
#include "RgscTelemetryManager.h"
#include "RgscDebugTelemetry.h"
#include "WorkerThread.h"
#include "GameCommunication.h"

#include "..\..\..\game\system\MachineHash.h"

#include <regex>
#include <set>

bool InPlaceDownloader::sm_initialized = false;
bool InPlaceDownloader::sm_upToDate = false;
bool InPlaceDownloader::sm_cancelled = false;
bool InPlaceDownloader::sm_running = false;

std::vector<InPlaceDownloader::Manifest> InPlaceDownloader::sm_manifests;
std::string InPlaceDownloader::sm_destPathPrefix;
std::vector<Patch> InPlaceDownloader::sm_downloadFileList;
u64 InPlaceDownloader::sm_totalAlreadyDownloaded = 0;
u64 InPlaceDownloader::sm_remainingDownloadBytes = 0;
ProgressCalculator InPlaceDownloader::sm_downloadProgress(ProgressCalculator::PT_DOWNLOAD_DIP_DATA);
ProgressCalculator InPlaceDownloader::sm_verifyProgress(ProgressCalculator::PT_VERIFY_DIP_DATA);
InPlaceDownloader::DownloadVisibility InPlaceDownloader::sm_downloadVisibility = InPlaceDownloader::IN_FOREGROUND;

const std::string BOOTSTRAP_NAME = "PlayGTAV.exe";

static bool StringHasSuffix(const std::string& haystack, const std::string& needle)
{
	if (haystack.length() < needle.length())
		return false;

	return haystack.compare(haystack.length() - needle.length(), needle.length(), needle) == 0;
}

void InPlaceDownloader::CheckManifestSynchronous()
{
	sm_cancelled = false;

	if (!sm_initialized)
	{
		Init();
	}

	//@@: location INPLACEDOWNLOADER_CHECKMANIFESTSYNCHRONOUS
	if (!DownloadAndReadAllManifests())
	{
		ERRORF("Unable to read manifest!");
		if (CError::GetLastError().IsError(DOWNLOADER_ERR_NO_DISK_SPACE))
		{
			DEBUGF1("No disk space.");
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eNoTempSpace);
		}
		else if (CError::GetLastError().IsError(DOWNLOADER_ERR_UNENTITLED))
		{
			DEBUGF1("Show code redemption.");
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eShowCodeRedemption);
		}
		else
		{
			DEBUGF1("Download error.");
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eDownloadInPlaceDataError);
		}
		return;
	}

	RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_DIP_CHECK_AGAINST_MANIFEST);

	if (!TrimDownloadFileList())
	{
		DEBUGF1("Error trimming manifest.");
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eNeedToRestartAsAdmin);
		return;
	}

	sm_upToDate = sm_downloadFileList.empty() && !Globals::verify;

	RgscTelemetryManager::WriteEventInt(RgscTelemetryManager::EVENT_LAUNCHER_DIP_DATA_TO_INSTALL, sm_upToDate ? 1 : 0);

	if (sm_upToDate)
		DISPLAYF("In-place download is up-to-date.");
    else
	{
		DISPLAYF("In-place download is not up-to-date; download required.");

		std::string testFilename = sm_destPathPrefix + "test.test";

		// Test if we need elevation
		HANDLE file =  CFileWrapper::CreateFileA(testFilename.c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_ALWAYS, FILE_FLAG_RANDOM_ACCESS | FILE_FLAG_DELETE_ON_CLOSE, NULL);

		if (file == INVALID_HANDLE_VALUE)
		{
			ERRORF("Unable to open %s", testFilename.c_str());

			DWORD err = GetLastError();
			if (err == ERROR_ACCESS_DENIED)
			{
				ERRORF("Access denied.");
				CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eNeedToRestartAsAdmin);
				return;
			}
			else
			{
				CUtil::PrintSystemErrorMessage(err);
			}
		}
		else
		{
			CloseHandle(file);
		}
	}

	CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);
}

bool InPlaceDownloader::QuickMissingFileCheck()
{
	TrimDownloadFileList();

	sm_upToDate = sm_downloadFileList.empty();

	return sm_upToDate;
}

void InPlaceDownloader::DownloadFilesSynchronous(DownloadVisibility downloadType)
{
	sm_cancelled = false;
	sm_running = true;
	sm_downloadVisibility = downloadType;

	sm_downloadProgress.Reset();
	sm_verifyProgress.Reset();
	ProgressCalculator::AddToGlobalProgressList(&sm_downloadProgress);

	if (Globals::verify)
	{
		ProgressCalculator::AddToGlobalProgressList(&sm_verifyProgress);
	}

	if (!TrimDownloadFileList())
	{
		DEBUGF1("Error trimming file list.");
		CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eDownloadInPlaceDataError);
	}

	static const std::string TEMP_SUFFIX = ".temp";

	struct Listener : IDownloader::DownloadListener
	{
		bool m_error;
		bool m_needEntitlement;
		std::map<std::string, int> m_fileIndexMap;
		std::map<std::string, Patch*> m_filePatchMap;

		Listener() : m_error(false), m_needEntitlement(false)
		{
			sm_downloadProgress.AddSegment(sm_totalAlreadyDownloaded, sm_totalAlreadyDownloaded);
			sm_downloadProgress.SetSegmentComplete(0);
		}

		void MapFile(Patch& file, int index)
		{
			if (StringHasSuffix(file.LocalFilename, BOOTSTRAP_NAME))
			{
				file.LocalFilename += TEMP_SUFFIX;
			}

			m_fileIndexMap[file.LocalFilename] = index;
			m_filePatchMap[file.LocalFilename] = &file;
			//@@: location INPLACEDOWNLOADER_LISTENER_MAPFILE
			sm_downloadProgress.AddSegment(0, file.FileSize);

			if (!file.Progress)
			{
				if (!file.AccessControlKey.empty() && file.AccessControlKey.find("dlcpacks") != std::string::npos)
				{
					file.Progress = new ProgressCalculator(ProgressCalculator::PT_DOWNLOAD_DLC);
				}
				else
				{
					file.Progress = new ProgressCalculator(ProgressCalculator::PT_DOWNLOAD_BASE_DATA);
				}
			}
		}

		virtual void DownloadStarting(Patch& patch)
		{
			if (patch.NeedsAccessControlledUrl)
			{
#if RSG_TIMEBOMBED_URLS_IN_SCDLL
				EntitlementManager::EntitlementResult result;
				std::string timebombedUrl;

				SocialClub::GetEntitlementManager()->RequestTimebombedUrlSynchronous(patch.AccessControlKey, &result, timebombedUrl);

				if (result == EntitlementManager::ENTITLEMENT_TRUE)
				{
					patch.PatchURL = timebombedUrl;
				}
				else if (result == EntitlementManager::ENTITLEMENT_FALSE)
				{
					sm_cancelled = true;
					m_needEntitlement = true;
					m_error = true;
				}
				else if (result == EntitlementManager::ENTITLEMENT_ERROR)
				{
					sm_cancelled = true;
					m_error = true;
				}
#endif
			}
		}

		virtual void DownloadComplete(const Patch& patch, DownloadCompleteStatus /*status*/)
		{
			//@@: location INPLACEDOWNLOADER_LISTENER_DOWNLOADCOMPLETE
			int index = m_fileIndexMap[patch.LocalFilename];
			sm_downloadProgress.SetSegmentComplete(index);

			m_filePatchMap.erase(patch.LocalFilename);
			bool filesRemain = false;
			for (std::map<std::string, Patch*>::iterator it = m_filePatchMap.begin(); it != m_filePatchMap.end(); it++)
			{
				if (it->second->SkuName == patch.SkuName)
				{
					filesRemain = true;
					break;
				}
			}

			if (!filesRemain)
			{
				if (sm_downloadVisibility == IN_BACKGROUND)
				{
					CGameCommunication::Instance().SendCompleteMessage(patch.SkuName);
				}

				for (std::vector<Manifest>::iterator it = sm_manifests.begin(); it != sm_manifests.end(); it++)
				{
					if (it->backgroundDownloadInProgress && it->sourceData.SkuName == patch.SkuName)
					{
						it->backgroundDownloadInProgress = false;
					}
				}
			}

			if (StringHasSuffix(patch.LocalFilename, TEMP_SUFFIX))
			{
				std::string originalFilename(patch.LocalFilename, 0, patch.LocalFilename.length() - TEMP_SUFFIX.length());

				std::wstring wOriginal, wNew;
				CUtil::StdStringToStdWString(originalFilename, wOriginal);
				CUtil::StdStringToStdWString(patch.LocalFilename, wNew);

				DISPLAYF("Moving file into place: %s -> %s", patch.LocalFilename.c_str(), originalFilename.c_str());

				if (!MoveFileExW(wNew.c_str(), wOriginal.c_str(), MOVEFILE_REPLACE_EXISTING))
				{
					CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_OPEN_SPARSE_FILE_FAILED, originalFilename);
					sm_cancelled = true;
					m_error = true;
				}
			}
		}
		virtual void DownloadStatus(const Patch& patch, DownloadCompleteStatus status, u64 totalBytesComplete, u64 totalBytesInFile, u64 bytesJustDownloaded)
		{
			int index = m_fileIndexMap[patch.LocalFilename];
			sm_downloadProgress.UpdateProgress(status, totalBytesComplete, totalBytesInFile, bytesJustDownloaded, index);

			if (sm_downloadProgress.IsUpdatedMessageAvailable())
			{
				if (sm_downloadVisibility == IN_FOREGROUND)
				{
					//DEBUGF3("New message received.");
					float progress;
					std::wstring message;
					sm_downloadProgress.GetCurrentStatus(message, progress);

					CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetProgress, (WPARAM)(progress * 100.0f));
					CLauncherDlg::GetLastInstance()->SetStatusTextSafe(message);
				}
				else
				{
					float progressScalar = sm_downloadProgress.GetCurrentProgress();
					CGameCommunication::Instance().SendProgressMessage(patch.SkuName, (int)(progressScalar * 100.0f));
				}
			}
		}
		virtual void DownloadError(const Patch& /*patch*/, const CError& error)
		{
			CError::SetError(error);
			DEBUGF3("Download error reported.");
			m_error = true;
		}

		virtual void DownloadEvent(Patch& /*patch*/, IDownloader::DownloadEvent event)
		{
			sm_downloadProgress.DownloadEvent(event);
		}

	} closure;

	if (!sm_downloadFileList.empty())
	{
		DEBUGF1("%lu files to download in-place.", sm_downloadFileList.size());

		RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_DIP_START_DOWNLOAD);

		Globals::needsLaunchConfirmation = true;

		if (!RockstarDownloader::CheckForDiskSpace(sm_downloadFileList.front().LocalFilename, sm_remainingDownloadBytes))
		{
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eNoInstallSpace);
			sm_running = false;
			return;
		}

		Downloader::Instance().Reset();
		//@@: location DOWNLOADER_INSTANCE_SETDOWNLOADLISTENER
		Downloader::Instance().SetDownloadListener(&closure);

		for (int i = 0; i < sm_downloadFileList.size(); i++)
		{
			closure.MapFile(sm_downloadFileList[i], i+1);
			Downloader::Instance().QueuePatch(sm_downloadFileList[i]);
		}

		Downloader::Instance().SetMaxRetries(OnlineConfig::DIPRetries);
	
		Downloader::Instance().StartSynchronously();

		Downloader::Instance().SetMaxRetries(OnlineConfig::PatchRetries);

		Downloader::Instance().SetDownloadListener(NULL);

		sm_downloadProgress.DownloadEvent(IDownloader::DLE_ENDING);

		RgscDebugTelemetry::SendDebugTelemetry();

		for (size_t i = 0; i < sm_downloadFileList.size(); i++)
		{
			Patch& patch = sm_downloadFileList[i];
			if (patch.Progress)
			{
				delete patch.Progress;
				patch.Progress = NULL;
			}
		}
	}

	if (sm_downloadVisibility == IN_FOREGROUND)
	{
		if (closure.m_error)
		{
			ERRORF("In-place download encountered errors.");
			if (CError::GetLastError().IsError(DOWNLOADER_ERR_NO_DISK_SPACE))
			{
				DEBUGF1("No disk space.");
				CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eNoInstallSpace);
			}
			else if (closure.m_needEntitlement)
			{
				DEBUGF1("Show code redemption.");
				CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eShowCodeRedemption);
			}
			else
			{
				DEBUGF1("DIP download error.");
				CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eDownloadInPlaceDataError);
			}
		}
		else
		{
			DEBUGF1("In-place download succeeded.");
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);
		}
	}
	else
	{

		for (std::vector<Manifest>::iterator it = sm_manifests.begin(); it != sm_manifests.end(); it++)
		{
			if (it->backgroundDownloadInProgress)
			{
				it->backgroundDownloadInProgress = false;

				if (closure.m_error)
				{
					CGameCommunication::Instance().SendErrorMessage(it->sourceData.SkuName, "E");
				}
				else
				{
					CGameCommunication::Instance().SendCompleteMessage(it->sourceData.SkuName);
				}

				for (std::vector<Manifest>::iterator it2 = it+1; it2 != sm_manifests.end(); it2++)
				{
					if (it2->sourceData.SkuName == it->sourceData.SkuName)
					{
						it2->backgroundDownloadInProgress = false;
					}
				}
			}
		}
	}

	sm_running = false;
}

void InPlaceDownloader::StartDownloadingFilesForSku(const std::string& entitlementCode)
{
	for (std::vector<Manifest>::iterator it = sm_manifests.begin(); it != sm_manifests.end(); it++)
	{
		Manifest& manifest = *it;

		if (manifest.sourceData.SkuName == entitlementCode)
		{
			if (!manifest.addedToList)
			{
				DownloadAndReadManifest(manifest);
			}
			manifest.backgroundDownloadInProgress = true;
		}
	}

	WorkerThread::Instance().AddJob(WorkerThread::JOB_DOWNLOAD_IN_PLACE_IN_BACKGROUND);
}

u64 InPlaceDownloader::SumManifestFilesizes()
{
	u64 total = 0;
	for (size_t i = 0; i < sm_manifests.size(); i++)
	{
		const Manifest& manifest = sm_manifests[i];

		for (size_t j = 0; j < manifest.fileList.size(); j++)
		{
			total += manifest.fileList[j].FileSize;
		}
	}

	return total;
}

void InPlaceDownloader::PerformIntegrityCheckSynchronous()
{
	sm_upToDate = true;

	sm_verifyProgress.Reset();
	ProgressCalculator::AddToGlobalProgressList(&sm_verifyProgress);

	class IntegrityCheckDelegate : public VerificationDelegate
	{
	private:
		u64 totalSizeToVerify;
		u64 totalVerified;
		int lastPercent;

	public:
		IntegrityCheckDelegate(u64 total) : totalVerified(0), totalSizeToVerify(total), lastPercent(0)
		{
		}

		virtual void VerifiedBytes(u64 bytes)
		{
			totalVerified += bytes;

			sm_verifyProgress.UpdateProgress(DL_STATUS_OK, totalVerified, totalSizeToVerify, bytes);
			float pc = 100.0f * ProgressCalculator::GetGlobalProgressScalar();
			CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetProgress, (WPARAM)pc, 0);
		}

	} delegate(SumManifestFilesizes());

	std::vector<Patch> fileList;
	
	if (Globals::verify)
	{
		for (size_t i = 0; i < sm_manifests.size(); i++)
		{
			fileList.insert(fileList.end(), sm_manifests[i].fileList.begin(), sm_manifests[i].fileList.end());
		}
	}
	else
	{
		fileList.assign(sm_downloadFileList.begin(), sm_downloadFileList.end());
	}

	for (int i = 0; i < fileList.size(); i++)
	{
		Patch& patch = fileList[i];

		if (!VerifyIntegrity(patch, delegate))
		{
			if (sm_cancelled)
			{
				return;
			}

			sm_upToDate = false;

			if (!DeleteCorruptFile(patch.LocalFilename))
			{
				DEBUGF1("Unable to completely delete %s", patch.LocalFilename.c_str());
				CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_INSTALL_FAILED);

				CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetState, CFsm::eNeedToRestartAsAdmin);
				return;
			}
		}
	}

	if (sm_upToDate)
	{
		RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_DIP_COMPLETED_UPDATE);
		DEBUGF1("In-place data is up-to-date.");
	}
	else
	{
		DEBUGF1("In-place data is not up-to-date.");
	}

	//CLauncherDlg::PostMessageSafe(CLauncherDlg::eSetProgress, (WPARAM)100.0f, 0);
	CLauncherDlg::PostMessageSafe(CLauncherDlg::eNextState);
}


bool InPlaceDownloader::DeleteCorruptFile(const std::string& filepath)
{
	// Don't delete the Boostrap here.
	if (StringHasSuffix(filepath, BOOTSTRAP_NAME))
	{
		return FILE_MISSING;
	}

	const char* fileFullPath = filepath.c_str();
	WARNINGF("Deleting corrupt file: %s", fileFullPath);
	if (!CFileWrapper::DeleteFileA(fileFullPath))
	{
		WARNINGF("Restting attrs on %s...", fileFullPath);
		if (!CFileWrapper::SetFileAttributesA(fileFullPath, FILE_ATTRIBUTE_NORMAL))
		{
			ERRORF("Unable to set attrs on %s!", fileFullPath);
			return false;
		}


		if (!CFileWrapper::DeleteFileA(fileFullPath))
		{
			ERRORF("Unable to delete %s!", fileFullPath);
			return false;
		}
		else
		{
			DEBUGF1("Deleted.");
		}
	}

	std::string partfileString = filepath + ".part";
	const char* partfile = partfileString.c_str();
	HANDLE partfileHandle = CFileWrapper::CreateFileA(partfile, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	if (partfileHandle != INVALID_HANDLE_VALUE)
	{
		DEBUGF1("Deleting partfile too.");
		CloseHandle(partfileHandle);

		if (!CFileWrapper::DeleteFileA(partfile))
		{
			WARNINGF("Restting attrs on %s...", partfile);
			if (!CFileWrapper::SetFileAttributesA(partfile, FILE_ATTRIBUTE_NORMAL))
			{
				ERRORF("Unable to set attrs on %s!", partfile);
				return false;
			}


			if (!CFileWrapper::DeleteFileA(partfile))
			{
				ERRORF("Unable to delete %s!", partfile);
				return false;
			}
			else
			{
				DEBUGF1("Deleted.");
			}
		}
	}

	return true;
}


void InPlaceDownloader::Cancel()
{
	DEBUGF1("Cancelling in-place download!");
	sm_cancelled = true;
	Downloader::Instance().Stop();
}

bool InPlaceDownloader::IsUpToDate()
{
	return sm_upToDate;
}


bool InPlaceDownloader::IsRunning()
{
	return sm_running;
}

void InPlaceDownloader::ChangeVisibility(DownloadVisibility visibility)
{
	sm_downloadVisibility = visibility;
}



void InPlaceDownloader::Init()
{
	// Set up the destination filepath
	sm_destPathPrefix = GamePatchCheck::GetGameExecutableFolder();
	
	if (sm_destPathPrefix.size() > 0)
	{
		char c = sm_destPathPrefix[sm_destPathPrefix.size()-1];
		if (c != '/' && c != '\\')
			sm_destPathPrefix += "\\";
	}

	sm_initialized = true;
}

bool InPlaceDownloader::DownloadAndReadAllManifests()
{
	if (!sm_initialized)
	{
		Init();
	}

	RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_LAUNCHER_DIP_GET_MANIFEST);

	std::string out_json;

#if RSG_PAID_DLC_SUPPORT && !RSG_PRELOADER_LAUNCHER
	SocialClub::GetEntitlementManager()->GetEntitlementJsonSynchronous(&out_json);
#endif
	
#if RSG_COMBINED_VERSIONING
	sm_manifests.clear();
	std::vector<Patch> all_manifests;
	if (!VersionManager::GetDigitalDataManifestUrls(all_manifests))
	{
		return false;
	}

	for (std::vector<Patch>::iterator it = all_manifests.begin(); it != all_manifests.end(); it++)
	{
		Patch& location = *it;

		sm_manifests.push_back(Manifest());
		Manifest& manifest = sm_manifests.back();

		manifest.sourceData = location;
		manifest.urlPrefix = std::regex_replace(location.PatchURL, std::regex("(http://patches.rockstargames.com/[a-z]+/gtavp?/)?(.*/)manifest.*"), "$2");

		CUtil::PrependTempoaryFolderPath(manifest.sourceData.LocalFilename, manifest.sourceData.LocalFilename);

		IPatchCheck::ConvertToNeedingAccessControlledUrl(manifest.sourceData);

		DEBUGF1("Adding manifest with path: %s", manifest.sourceData.AccessControlKey.c_str());
		DEBUGF1("               and prefix: %s", manifest.urlPrefix.c_str());
		DEBUGF1("               writing to: %s", manifest.sourceData.LocalFilename.c_str());
		
		if (!location.SkuName.empty())
		{
			DEBUGF1("    requiring entitlement: %s", manifest.sourceData.SkuName.c_str());
			// TODO: Parse JSON when format is known
			if (out_json.find(location.SkuName) == std::string::npos)
			{
				// Not entitled to this manifest; ignore it
				DEBUGF1("Ignoring as we're not entitled to this.");
				manifest.entitled = false;
				continue;
			}
			else
			{
				DEBUGF1("We have the required entitlement.");
			}
		}

		manifest.entitled = true;
	}
#endif

	sm_downloadFileList.clear();

	for (std::vector<Manifest>::iterator it = sm_manifests.begin(); it != sm_manifests.end(); it++)
	{
		(*it).addedToList = false;
	}


	for (std::vector<Manifest>::iterator it = sm_manifests.begin(); it != sm_manifests.end(); it++)
	{
		Manifest& manifest = *it;

		if (!manifest.entitled)
		{
			// We're not entitled to this manifest, so don't even try to download it
			// (we'd be blocked anyway)
			continue;
		}
		
		if (!DownloadAndReadManifest(manifest))
		{
			return false;
		}
	}

	return true;
}

bool InPlaceDownloader::DownloadAndReadManifest(Manifest& manifest)
{
	// Ensure correct parameters are set up
	manifest.sourceData.AlwaysDownload = true;
	manifest.sourceData.BackgroundDownload = false;
	manifest.sourceData.ChunkedDownload = false;
	manifest.sourceData.DeleteOnCompletion = false;

	// Grab timebombed URL
	IPatchCheck::ConvertToNeedingAccessControlledUrl(manifest.sourceData);

	EntitlementManager::EntitlementResult result;
	SocialClub::GetEntitlementManager()->RequestTimebombedUrlSynchronous(manifest.sourceData.AccessControlKey, &result, manifest.sourceData.PatchURL);

	if (result == EntitlementManager::ENTITLEMENT_FALSE)
	{
		DEBUGF1("Not entitled to access-controlled URL for %s", manifest.sourceData.AccessControlKey.c_str());
		manifest.entitled = false;
		if (manifest.sourceData.SkuName.empty())
		{
			CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_UNENTITLED);
			return false;
		}
	}
	else if (result == EntitlementManager::ENTITLEMENT_ERROR || AUTOTEST_FAIL)
	{
		CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_ACU_FAILED);
		DEBUGF1("Error obtaining access-controlled URL for %s", manifest.sourceData.AccessControlKey.c_str());
		return false;
	}
	else
	{
		DEBUGF1("Obtained access-controlled URL for %s", manifest.sourceData.AccessControlKey.c_str());
		manifest.entitled = true;
	}

	// Always download the manifest, in case it changes
	struct Closure : public RockstarDownloader::DownloadListener
	{
		CError m_error;

		Closure() {}

		virtual void DownloadComplete(const Patch& /*patch*/, DownloadCompleteStatus /*status*/) {}
		virtual void DownloadStatus(const Patch& /*patch*/, DownloadCompleteStatus /*status*/, u64 /*totalBytesComplete*/, u64 /*totalBytesInFile*/, u64 /*bytesJustDownloaded*/) {}
		virtual void DownloadError(const Patch& /*patch*/, const CError& error)
		{
			m_error = error;
		};
	};

	Closure closure;

	DEBUGF1("Attempting to download manifest from %s", manifest.sourceData.PatchURL.c_str());

	// Create, start, and clean-up the download
	RockstarDownloader down;
	down.Init();
	down.SetCurrentPatch(manifest.sourceData);
	down.SetListener(&closure);
	down.StartSynchronously();
	down.SetListener(NULL);

	// If we got it, read it in
	if (!closure.m_error.IsError() || AUTOTEST_FAIL)
	{
		DEBUGF1("Got manifest.");
		if (!ReadManifest(manifest) || AUTOTEST_FAIL)
		{
			CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_READ_MANIFEST_FAILED);
			DEBUGF1("Failed to read downloaded manifest.");
			return false;
		}
		else
		{
			DEBUGF1("Read and added downloaded manifest.");
			manifest.addedToList = true;
		}
	}
	else
	{
		DEBUGF1("Error downloading manifest.");
		CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_DOWNLOAD_MANIFEST_FAILED);
		return false;
	}

	return true;
}

bool InPlaceDownloader::ReadManifest(Manifest& manifest)
{
	DEBUGF1("Parsing manifest from %s...", manifest.sourceData.PatchURL.c_str());

	if (!manifest.sourceData.SkuName.empty())
	{
		DEBUGF1("(sku %s)", manifest.sourceData.SkuName.c_str());
	}

	std::ifstream input;
	std::wstring wpath;
	CUtil::StdStringToStdWString(manifest.sourceData.LocalFilename, wpath);
	input.open(wpath, std::ios::in);
	while (!input.eof() && !input.bad() && !input.fail())
	{
		std::string filepath, sizeString, hashHexString;
		if (std::getline(input, filepath).fail())
			break;
		if (std::getline(input, sizeString).fail() || std::getline(input, hashHexString).fail())
		{
			ERRORF("Bad manifest.");
			return false;
		}

		if (filepath.empty() || filepath.find_first_of(":?*|><\"") != std::string::npos)
		{
			// Reserved characters for future expansion
			return true;
		}

		u64 sizeValue;
		std::istringstream(sizeString) >> sizeValue;
		
		Patch patch;
		patch.AccessControlKey = manifest.urlPrefix + filepath;
		patch.LocalFilename = sm_destPathPrefix + filepath;
		patch.FileSize = sizeValue;
		patch.HashHexDigits = hashHexString;
		patch.ChunkedDownload = true;
		patch.DeleteOnCompletion = false;
		patch.NeedsAccessControlledUrl = true;
		patch.ExpectsHashFile = true;
		patch.SkuName = manifest.sourceData.SkuName;
		manifest.fileList.push_back(patch);

		DEBUGF3("%s %s %llu", patch.HashHexDigits.c_str(), patch.AccessControlKey.c_str(), patch.FileSize);
	}

	return true;
}

bool InPlaceDownloader::TrimDownloadFileList()
{
	sm_downloadFileList.clear();
	sm_totalAlreadyDownloaded = 0;
	sm_remainingDownloadBytes = 0;

	for (size_t i = 0; i < sm_manifests.size(); i++)
	{
		Manifest& manifest = sm_manifests[i];

		u64 manifestAlreadyDownloadedBytes = 0;
		u64 manifestRemainingBytes = 0;

		for (size_t j = 0; j < manifest.fileList.size(); j++)
		{
			Patch& patch = manifest.fileList[j];

			FilePresenceStatus status = CheckPresence(patch);
			if (status == FILE_COMPLETE)
			{
				manifestAlreadyDownloadedBytes += patch.FileSize;
			}
			else if (status == FILE_MISSING)
			{
				sm_downloadFileList.push_back(patch);
				manifestRemainingBytes += patch.FileSize;
			}
			else if (status == FILE_PARTIAL)
			{
				sm_downloadFileList.push_back(patch);
			}
			else if (status == FILE_ERROR)
			{
				return false;
			}
		}

		if (manifestRemainingBytes > 0)
		{
			sm_totalAlreadyDownloaded += manifestAlreadyDownloadedBytes;
			sm_remainingDownloadBytes += manifestRemainingBytes;
		}
	}

	if (sm_downloadFileList.empty())
	{
		DEBUGF1("Trimmed file list is empty.");
	}
	else
	{
		DEBUGF1("Trimmed file list is non-empty.");
	}

	return true;
}

InPlaceDownloader::FilePresenceStatus InPlaceDownloader::CheckPresence(const Patch& file)
{
	std::wstring wfilepath;
	CUtil::StdStringToStdWString(file.LocalFilename, wfilepath);
	CUtil::CreateDirectoryHierarchyIfDoesntExist(wfilepath);

	// Check file exists
	HANDLE fileHandle = CFileWrapper::CreateFileA(file.LocalFilename.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);

	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		DEBUGF3("*Missing:* %s", file.LocalFilename.c_str());
		return FILE_MISSING;
	}

	CloseHandle(fileHandle);

	__stat64 stats;
	if (_wstat64(wfilepath.c_str(), &stats) != 0)
	{
		DEBUGF3("Unable to stat file %s!", file.LocalFilename.c_str());
		if (!DeleteCorruptFile(file.LocalFilename))
		{
			DEBUGF1("Unable to completely delete %s", file.LocalFilename.c_str());
			CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_INSTALL_FAILED);
			return FILE_ERROR;
		}
		else
		{
			return FILE_MISSING;
		}
	}

	if ((u64)(stats.st_size) != file.FileSize)
	{
		WARNINGF("Incorrect size for %s (%llu vs %llu)", file.LocalFilename.c_str(), stats.st_size, file.FileSize);
		if (!DeleteCorruptFile(file.LocalFilename))
		{
			DEBUGF1("Unable to completely delete %s", file.LocalFilename.c_str());
			CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_INSTALL_FAILED);
			return FILE_ERROR;
		}
		else
		{
			return FILE_MISSING;
		}
	}

	// Check file *doesn't* have a partfile
	std::string partfile = file.LocalFilename + ".part";
	fileHandle = CFileWrapper::CreateFileA(partfile.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);

	if (fileHandle != INVALID_HANDLE_VALUE)
	{
		DEBUGF3("*Partial:* %s", file.LocalFilename.c_str());
		CloseHandle(fileHandle);

		return FILE_PARTIAL;
	}

	DEBUGF3("Complete:  %s", file.LocalFilename.c_str());

	return FILE_COMPLETE;
}

bool InPlaceDownloader::VerifyIntegrity(const Patch& file, VerificationDelegate& delegate)
{
	std::wstring wfilepath;
	CUtil::StdStringToStdWString(file.LocalFilename, wfilepath);
	CUtil::CreateDirectoryHierarchyIfDoesntExist(wfilepath);

	HANDLE fileHandle = CFileWrapper::CreateFileA(file.LocalFilename.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);

	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		DEBUGF3("%s missing.", file.LocalFilename.c_str());
		delegate.VerifiedBytes(file.FileSize);
		return false;
	}

	bool ret = VerifyIntegrityOfOpenFile(file, fileHandle, delegate);

	CloseHandle(fileHandle);

	return ret;
}

int hexDigitToInt(char c)
{
	if (c >= '0' && c <= '9') return c - '0';
	if (c >= 'a' && c <= 'f') return 10 + c - 'a';
	if (c >= 'A' && c <= 'F') return 10 + c - 'A';
	return -1;
}

#define SKIP_HASHES (0 && RSG_DEBUG)

bool InPlaceDownloader::VerifyIntegrityOfOpenFile(const Patch& file, HANDLE fileHandle, VerificationDelegate& delegate)
{
	DEBUGF3("Verifying %s. Expecting filesize of %llu.", file.LocalFilename.c_str(), file.FileSize);
	LARGE_INTEGER filesize;
	if (!GetFileSizeEx(fileHandle, &filesize))
	{
		DEBUGF3("%s can't get filesize.", file.LocalFilename.c_str());
		return false;
	}

	// Check file size
	if (filesize.QuadPart != (s64)file.FileSize)
	{
		DEBUGF3("%s incorrect size (%llu instead of %llu).", file.LocalFilename.c_str(), filesize.QuadPart, file.FileSize);
		delegate.VerifiedBytes(file.FileSize);
		return false;
	}

	// Check hash is valid
	if (file.HashHexDigits.length() != SHA256_DIGEST_LENGTH * 2)
	{
		DEBUGF3("%s has invalid hash ('%s', %u hex digits).", file.LocalFilename.c_str(), file.HashHexDigits.c_str(), file.HashHexDigits.length());
		delegate.VerifiedBytes(file.FileSize);
		return false;
	}

	u8 expectedHash[SHA256_DIGEST_LENGTH];
	const char* cursor = file.HashHexDigits.c_str();
	for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
	{
		int c1 = hexDigitToInt(*cursor), c2 = hexDigitToInt(*(cursor+1));
		if (c1 == -1 || c2 == -1)
		{
			DEBUGF3("%s has invalid hash ('%s', %c%c is not hex).", file.LocalFilename.c_str(), file.HashHexDigits.c_str(), *cursor, *(cursor+1));
			delegate.VerifiedBytes(file.FileSize);
			return false;
		}
		else
		{
			expectedHash[i] = (u8)((c1 << 4) + c2);
		}

		cursor += 2;
	}

	// Calculate hash
	SHA256_CTX ctx;
	SHA256_Init(&ctx);

	const u32 MAX_CHUNK_SIZE = 4 * 1024 * 1024;

	u32 chunkSize = MAX_CHUNK_SIZE;
	char* buffer = new char[MAX_CHUNK_SIZE];

	DEBUGF3("Verifying hash of %s... (filesize: %lld chunksize: %lu)", file.LocalFilename.c_str(), filesize.QuadPart, chunkSize);

	for (u64 totalRead = 0; totalRead < (u64)filesize.QuadPart; totalRead += chunkSize)
	{
		chunkSize = (u32)std::min<u64>(MAX_CHUNK_SIZE, filesize.QuadPart - totalRead);

		DEBUGF3("Attempting to read chunk size %lu from position %llu...", chunkSize, totalRead);

		DWORD bytesRead = 0;
#if !SKIP_HASHES
		if (!ReadFile(fileHandle, buffer, chunkSize, &bytesRead, NULL) || bytesRead != chunkSize || sm_cancelled)
		{
			DEBUGF1("Unable to read file! Total = %llu, bytesRead = %lu, chunkSize = %lu, cancelled = %s", totalRead, bytesRead, chunkSize, sm_cancelled ? "true" : "false");
			delete[] buffer;
			delegate.VerifiedBytes(file.FileSize - totalRead);
			return false;
		}
		else
		{
			DEBUGF3("Read %lu bytes starting from %llu. (chunk size %lu)", bytesRead, totalRead, chunkSize);
		}

		SHA256_Update(&ctx, buffer, chunkSize);

#endif

		delegate.VerifiedBytes(chunkSize);

	}

	delete[] buffer;

	u8 hash[SHA256_DIGEST_LENGTH];
	SHA256_Final(hash, &ctx);

#if SKIP_HASHES
	WARNINGF("Skipping hash check. [DEBUG ONLY]");
	Sleep(100);
	return true;
#else

	if (memcmp(expectedHash, hash, sizeof(hash)) == 0)
	{
		char hexhash[SHA256_DIGEST_LENGTH*2+1];
		for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
		{
			sprintf_s(&(hexhash[i*2]), 3, "%02x", hash[i]);
		}
		DEBUGF3("%s has hash: %s", file.LocalFilename.c_str(), hexhash);
		return true;
	}
	else
	{
		char hexhash[SHA256_DIGEST_LENGTH*2+1];
		for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
		{
			sprintf_s(&(hexhash[i*2]), 3, "%02x", hash[i]);
		}
		WARNINGF("%s failed hash check (%s instead of %s).", file.LocalFilename.c_str(), hexhash, file.HashHexDigits.c_str());
		return false;
	}
#endif
}

#endif
