#pragma once

#include <string>

class CCrashDetection
{

public:
	// Checking for sudden exit by writing an in-file
	static bool CheckInfileOutfilePairs(bool& out_foundOrphan);
	static bool WriteInfile(DWORD processId);
	
	// Checking for error information by reading the game's out-file
	static bool ReadOutfile(DWORD processId, std::string& out_data);

	// Clean up afterwards
	static bool DeleteEntryAndExitFilePair(DWORD processId);

	static bool GetFolderForInfilesAndOutfiles(std::wstring& out_path);

	static void TranslateKnownExitCode(u32 exitCode);

private:
	static bool IsRunningGameProcess(DWORD pid);

};