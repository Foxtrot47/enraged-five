#include "stdafx.h"

#define DEFINE_GLOBALS
#include "Globals.h"

#include "Channel.h"
#include "Util.h"

const std::vector<CCommandlineArgument*>& CCommandlineArgument::GetSupportedArgs()
{
	return sm_supportedCommandlineArgs;
}

const std::wstring& CCommandlineArgument::GetGameCommandline()
{
	return sm_commandlineForGame;
}

CCommandlineArgument::CCommandlineArgument(const wchar_t* name, bool defaultValue)
	: m_bSet(defaultValue), m_bSetFromCommandline(false) , m_IntValue(0)
{
	m_wszName = std::wstring(L"-") + std::wstring(name);
	m_wszInverseName = std::wstring(L"");

	sm_supportedCommandlineArgs.push_back(this);
}

CCommandlineArgument::CCommandlineArgument(const wchar_t* name, const wchar_t* inverseName, bool defaultValue)
	: m_bSet(defaultValue), m_bSetFromCommandline(false) , m_IntValue(0)
{
	m_wszName = std::wstring(L"-") + std::wstring(name);
	m_wszInverseName = std::wstring(L"-") + std::wstring(inverseName);

	sm_supportedCommandlineArgs.push_back(this);
}

CCommandlineArgument::~CCommandlineArgument()
{
}

bool CCommandlineArgument::operator=(bool bSet)
{
	if (m_bSetFromCommandline)
	{
		ERRORF("Parameter %S is being re-set!", m_wszName.c_str());
	}
	
	m_bSet = bSet;

	return bSet;
}

CCommandlineArgument::operator bool() const
{
	return m_bSet;
}

bool CCommandlineArgument::WasSetFromCommandline() const
{
	return m_bSetFromCommandline;
}

void CCommandlineArgument::ProcessCommandline()
{
	// Clear game's commandline
	//@@: location CCOMMANDLINEARGUMENT_PROCESSCOMMANDLINE
	sm_commandlineForGame.clear();

	// Handle commandline parameters
	LPWSTR cmd = GetCommandLineW();
	if (cmd)
	{
		int argc = 0;
		LPWSTR* argv = CommandLineToArgvW(cmd, &argc);
		if (argv)
		{
			for (int i = 1; i < argc; i++)
			{
				LPWSTR arg = argv[i];

				const std::vector<CCommandlineArgument*>& knownArgs = CCommandlineArgument::GetSupportedArgs();
				bool handled = false;
				for (size_t j = 0; j < knownArgs.size(); j++)
				{
					if (knownArgs[j]->HandleCommandlineArg(arg))
					{
						handled = true;
						break;
					}
				}

				if (!handled)
				{
					std::string str;
					CUtil::WStringToStdString(arg, str);
					DEBUGF1("Unknown argument:  '%s'", str.c_str());

					// Append to game's commandline
					if (sm_commandlineForGame.size() == 0)
					{
						sm_commandlineForGame = arg;
					}
					else
					{
						sm_commandlineForGame += L" ";
						sm_commandlineForGame += arg;
					}
				}
			}

			LocalFree(argv);
		}
		else
		{
			//@@: location CCOMMANDLINE_PROCESSCOMMANDLINE_NO_ARGS
			DISPLAYF("No command-line arguments.");
		}
	}
}

bool CCommandlineArgument::HandleCommandlineArg(const wchar_t* arg)
{
	if (!arg)
	{
		return false;
	}

	size_t arglen = wcslen(arg);

	if (m_wszName.size() > 0 && _wcsicmp(m_wszName.c_str(), arg) == 0)
	{
		DISPLAYF("Accepted argument: '%S'", m_wszName.c_str());

		m_bSet = true;
		m_bSetFromCommandline = true;

		return true;
	}
	else if (m_wszInverseName.size() > 0 && _wcsicmp(m_wszInverseName.c_str(), arg) == 0)
	{
		DISPLAYF("Accepted inverse argument: '%S'", m_wszInverseName.c_str());

		m_bSet = false;
		m_bSetFromCommandline = true;

		return true;
	}
	else if (m_wszName.size() > 0 && _wcsnicmp(m_wszName.c_str(), arg, m_wszName.size()) == 0 && arglen > m_wszName.size() + 1 && arg[m_wszName.size()] == L'=')
	{
		m_value = &(arg[m_wszName.size()+1]);

		DISPLAYF("Accepted argument: '%S' with parameter: '%S'", m_wszName.c_str(), m_value.c_str());
		if (TryGetIntValue(m_value, &m_IntValue))
		{
			DISPLAYF("Argument was parsed as integer: %d", m_IntValue);
		}

		m_bSet = true;
		m_bSetFromCommandline = true;

		return true;
	}

	return false;
}

bool CCommandlineArgument::TryGetIntValue(std::wstring str, int* outValue)
{
	if(str.empty() || ((!isdigit(str[0])) && (str[0] != '-') && (str[0] != '+')))
	{
		return false;
	}

	wchar_t * p ;
	wcstol(str.c_str(), &p, 10);

	if ((*p == 0))
	{
		*outValue = _wtoi(str.c_str());
		return true;
	}

	return false;
}