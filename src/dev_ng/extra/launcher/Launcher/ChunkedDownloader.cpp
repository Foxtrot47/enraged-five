#include "stdafx.h"
#include "Entitlement.h"
#include "ChunkedDownloader.h"
#include "Config.h"
#include "Globals.h"
#include "Util.h"
#include "TamperSource.h"
#include "OnlineConfig.h"
#include "RgscDebugTelemetry.h"
#include "ProgressCalculator.h"
#include "RgscTelemetryManager.h"

#if RSG_LAUNCHER
#include "SocialClub.h"
#endif

bool ChunkedDownloader::sm_initialised = false;
bool ChunkedDownloader::sm_chunkedDownloadRunning = false;

CRITICAL_SECTION ChunkedDownloader::sm_critsec;
std::map<int, ChunkedDownloader::Connection*> ChunkedDownloader::sm_connectionMap;
int ChunkedDownloader::sm_connectionIndex = 1; // must be non-zero

const char* ChunkedDownloader::sm_hashSuffix = ".hash";

#define CONNECTION_DEBUGF3(fmt, ...) DEBUGF3("[%x] " fmt, m_index, __VA_ARGS__)

#define CONNECTION_VERBOSE_DEBUG 0

#define PROGRESSIVE_HASHES 0

ChunkedDownloader::ChunkedDownloader()
{
	if (!sm_initialised)
	{
		InitializeCriticalSection(&sm_critsec);
		CUtil::LockTimings::SetLockName(&sm_critsec, "ChunkedDownloader::sm_critsec");
	}
}

DownloadCompleteStatus ChunkedDownloader::DoDownload_CheckDisk(Patch& patch)
{
	if (m_listener)
		m_listener->DownloadStatus(CurrentPatch, DL_STATUS_OK, 0, patch.FileSize, 0);

	//@@: location CHUNKEDDOWNLOADER_DODOWNLOAD_CHECKDISK
	m_totalBytes = CurrentPatch.ContentLength = GetUrlInfoLong(patch.PatchURL, URL_INFO_TYPE_CONTENT_LENGTH);

	if (m_totalBytes == 0)
	{
		return DL_STATUS_ERROR;
	}

	if (!CheckForDiskSpace(patch))
	{
		return DL_STATUS_ERROR;
	}

	return DL_STATUS_OK;
}

void ChunkedDownloader::NotifyDownloadEvent(DownloadEvent event)
{
	AUTOCRITSEC(sm_critsec);
	if (m_listener)
		m_listener->DownloadEvent(CurrentPatch, event);

	if (CurrentPatch.Progress)
		CurrentPatch.Progress->DownloadEvent(event);
}

void ChunkedDownloader::Stop()
{
	m_downloading = false;
	m_sparseFile.CancelHashChecks();
}

bool ChunkedDownloader::IsDownloadInProgress()
{
	return sm_chunkedDownloadRunning;
}

DownloadCompleteStatus ChunkedDownloader::DoDownload_ReceiveData(Patch& patch)
{
	// Mark that the download is in progress
	sm_chunkedDownloadRunning = true;

	// Check the state on the disk
	if (!m_sparseFile.Open(patch.LocalFilename.c_str(), m_totalBytes))
	{
		ERRORF("Unable to open sparse file!");
		if (CError::GetLastError().IsError())
		{
			m_error = CError::GetLastError();
		}
		return DL_STATUS_ERROR;
	}

	m_bytesRead = m_sparseFile.GetTotalSizeWritten();

	// Get hash file
	if (patch.ExpectsHashFile)
	{
		DownloadHashFile(patch);
	}
	
	if (m_bytesRead > 0)
	{
		UpdateListener(DL_STATUS_VERIFYING);
	}
	
	u64 badBytes = 0;
	if (!m_sparseFile.CheckHashes(&badBytes))
	{
		WARNINGF("Bad hash detected! (%llu bytes!)", badBytes);
		m_bytesRead -= badBytes;
	}

	// Allocate and set up initial connections
	const int HARD_MAX_DOWNLOAD_CONNECTIONS = OnlineConfig::ConnectionLimit;
	const int MIN_CHUNK_SIZE = OnlineConfig::DownloadChunkSizeBytes / 2;
	const int MAX_CHUNK_SIZE = 1024 * 1024 * 1024;

	// Check the last 2 connections to finish
	const int CHECK_LAST_N_CONNECTIONS = 2;
	// Ignore chunks smaller than this
	const u64 MIN_SLOW_CHUNK_SIZE = 2 * MIN_CHUNK_SIZE;
	// Connection is slow if it hasn't finished this percentage
	const u64 SLOW_CHUNK_PERCENTAGE = 50;
	// Allow time before running slow check
	const u64 SLOW_CHECK_AFTER_MILLIS = 60*1000;

	// Adding +1 as padding to round upwards
	u64 desiredChunkSize = (u64)(m_totalBytes - m_bytesRead) / (u64)HARD_MAX_DOWNLOAD_CONNECTIONS + 1;
	
	// Clamp desired chunk size
	int CHUNK_SIZE = std::max<int>(MIN_CHUNK_SIZE, (int)std::min<u64>(desiredChunkSize, MAX_CHUNK_SIZE));

	Connection* connections = new Connection[HARD_MAX_DOWNLOAD_CONNECTIONS];
	
	for (int i = 0; i < HARD_MAX_DOWNLOAD_CONNECTIONS; i++)
	{
		connections[i].Init(this, m_hostURL, m_fileURL, false, &m_sparseFile);
	}

	DownloadCompleteStatus status = DL_STATUS_COMPLETE;

	u64 lastUiUpdate = 0;
	
	int errorCount = 0;
	bool allIdle = false;

	Range nextRange;
	bool hasNextRange = m_sparseFile.GetNextRange(&nextRange, CHUNK_SIZE);

#if !RSG_FINAL
	static int connectionsOpened = 0;
	static int connectionsTimedOut = 0;
	u64 last_status_update_time = 0;
#endif

	u64 downloadStartTimeMillis = CUtil::CurrentTimeMillis();

	while ((!allIdle || hasNextRange) && m_downloading)
	{
#if !RSG_FINAL
		u64 currentTime = CUtil::CurrentTimeMillis();
		if (currentTime > last_status_update_time + 1000)
		{
			last_status_update_time = currentTime;
			DEBUGF3("URL: %s", patch.PatchURL.c_str());
			for (int i = 0; i < HARD_MAX_DOWNLOAD_CONNECTIONS; i++)
			{
				connections[i].DebugConnectionStatus();
			}
			if (hasNextRange)
			{
				DEBUGF3("Next range: %llu - %llu", nextRange.m_offset, nextRange.GetEnd()-1);
			}
			else
			{
				DEBUGF3("Next range: none.");
			}
		}
#endif
		allIdle = true;
		int idleCount = 0;
		int usedCount = 0;
		int verifyingCount = 0;
		for (int i = 0; i < HARD_MAX_DOWNLOAD_CONNECTIONS; i++)
		{
			Connection::ConnectionState state = connections[i].GetState();

			if (connections[i].HasBeenUsed())
			{
				usedCount++;
			}

			if (state == Connection::STATE_IDLE)
			{
				if (hasNextRange)
				{
					allIdle = false;
					DEBUGF1("Giving connection %d range %llu - %llu (length %llu)", i, nextRange.m_offset, nextRange.m_offset + nextRange.m_length - 1, nextRange.m_length);
					if (!connections[i].HasBeenUsed())
					{
						NotifyDownloadEvent(DLE_CONNECTION_OPENED);
					}
					connections[i].SetChunk(nextRange.m_offset, nextRange.m_length);
					connections[i].SetRangeInProgress(true);
					connections[i].StartCheckingChunk();

					hasNextRange = m_sparseFile.GetNextRange(&nextRange, CHUNK_SIZE);
					if (hasNextRange)
					{
						DEBUGF1("(next range is %llu - %llu)", nextRange.m_offset, nextRange.m_offset + nextRange.m_length - 1);
					}
					else
					{
						DEBUGF1("(no next range)");
					}
#if !RSG_FINAL
					connectionsOpened++;
#endif
				}
				else
				{
					idleCount++;
				}
			}
			else if (state == Connection::STATE_VERIFYING)
			{
				verifyingCount++;
				allIdle = false;
			}
			else if (state == Connection::STATE_COMPLETE)
			{
				DEBUGF1("Connection %d reports completion", i);
				connections[i].SetRangeInProgress(false);
#if PROGRESSIVE_HASHES
				if (!m_sparseFile.CheckHashes(&badBytes))
				{
					if (m_downloading)
					{
						WARNINGF("Bad hash detected! (%llu bytes!)", badBytes);
						RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::CDC_VERIFICATION_FAILURE);
						NotifyDownloadEvent(DLE_VERIFICATION_FAILURE);
						m_bytesRead = m_sparseFile.GetTotalSizeWritten();

						hasNextRange = m_sparseFile.GetNextRange(&nextRange, CHUNK_SIZE);
						if (hasNextRange)
						{
							DEBUGF1("(next range is %llu - %llu)", nextRange.m_offset, nextRange.m_offset + nextRange.m_length - 1);
						}
						else
						{
							DEBUGF1("(no next range)");
						}
					}
					else
					{
						DEBUGF1("Hash check cancelled.");
					}
				}
#endif
				connections[i].ClearCompletion();
			}
			else if (state == Connection::STATE_STARTING_DOWNLOAD || state == Connection::STATE_DOWNLOADING)
			{
				allIdle = false;
				u64 millisSinceLastUpdate = CUtil::CurrentTimeMillis() - connections[i].GetLastUpdateTimeMillis();
				if (millisSinceLastUpdate > m_timeoutMillis)
				{
					RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::CDC_TIMED_OUT);
					ERRORF("Connection %x is unresponsive; terminating.", connections[i].GetIndex());
#if !RSG_FINAL
					connectionsTimedOut++;
					DEBUGF3("Opened %d connections, %d have timed out (%.2f%% timed out)", connectionsOpened, connectionsTimedOut, 100.0f * ((float)connectionsTimedOut) / ((float)connectionsOpened));
#endif
					UnregisterConnection(&connections[i]);
					connections[i].SetRangeInProgress(false);
					hasNextRange = m_sparseFile.GetNextRange(&nextRange, CHUNK_SIZE);
					if (hasNextRange)
					{
						DEBUGF1("(next range is %llu - %llu)", nextRange.m_offset, nextRange.m_offset + nextRange.m_length - 1);
					}
					else
					{
						DEBUGF1("(no next range)");
					}
					UpdateListener(DL_STATUS_RETRY);
				}
			}
			else if (state == Connection::STATE_ERROR)
			{
				const int MAX_ERRORS = 3;
				errorCount++;

				RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::CDC_ERROR);
				NotifyDownloadEvent(DLE_CONNECTION_ERROR);

				WARNINGF("Download error %lu / %lu.", errorCount, MAX_ERRORS);

				connections[i].SetRangeInProgress(false);
				hasNextRange = m_sparseFile.GetNextRange(&nextRange, CHUNK_SIZE);
				if (hasNextRange)
				{
					DEBUGF1("(next range is %llu - %llu)", nextRange.m_offset, nextRange.m_offset + nextRange.m_length - 1);
				}
				else
				{
					DEBUGF1("(no next range)");
				}

				if (errorCount >= 3)
				{
					DEBUGF1("Exceeded maximum chunk error count.");
					status = DL_STATUS_ERROR;
					ThrowError(connections[i].GetError());
					UpdateListener(DL_STATUS_RETRY);
					break;
				}
				else
				{
					m_bytesRead = m_sparseFile.GetTotalSizeWritten();
					connections[i].ClearError();
				}
			}
			else if (state == Connection::STATE_CANCELLED)
			{
			}
		}

		// Heuristically check for slow connections
		u64 downloadDurationMillis = CUtil::CurrentTimeMillis() - downloadStartTimeMillis;
		if (usedCount > CHECK_LAST_N_CONNECTIONS && idleCount >= usedCount - CHECK_LAST_N_CONNECTIONS && idleCount < HARD_MAX_DOWNLOAD_CONNECTIONS && downloadDurationMillis > SLOW_CHECK_AFTER_MILLIS)
		{
			int slowCount = 0;
			bool anyFast = false;
			for (int i = 0; i < HARD_MAX_DOWNLOAD_CONNECTIONS; i++)
			{
				Connection::ConnectionState state = connections[i].GetState();
				if (state == Connection::STATE_STARTING_DOWNLOAD || state == Connection::STATE_DOWNLOADING)
				{
					u64 downloaded = connections[i].GetBytesDownloaded();
					u64 total = connections[i].GetBytesTotal();

					// Check if this connection is slow
					if (total >= MIN_SLOW_CHUNK_SIZE && downloaded < (total * SLOW_CHUNK_PERCENTAGE) / 100UL)
					{
						DEBUGF1("Slow connection:");
#if !RSG_FINAL
						connections[i].DebugConnectionStatus();
#endif
						slowCount++;
					}
					else
					{
						anyFast = true;
					}
				}
			}

			if (slowCount > 0 && !anyFast)
			{
				RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::CD_SLOW_CONNECTIONS, slowCount);
				RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::CD_SLOW_CONNECTION_RETRY);
				DISPLAYF("%d slow connection(s) detected; reconnecting.", slowCount);
				status = DL_STATUS_ERROR;
				UpdateListener(DL_STATUS_SLOW_RETRY);
				break;
			}
		}

		// Send update message if we're only verifying
		if (verifyingCount > 0 && idleCount == usedCount - verifyingCount)
		{
			UpdateListener(DL_STATUS_VERIFYING);
		}

		if (status == DL_STATUS_ERROR)
		{
			break;
		}

		// Paused, but not cancelled
		if (m_paused && m_downloading)
		{
			status = DL_STATUS_PAUSE;
			break;
		}

		// Slow the loop down
		Sleep(1);

		// Cancelled
		if (!m_downloading)
		{
			DEBUGF1("Downloading cancelled.");
			status = DL_STATUS_CANCEL;
			break;
		}
		
		u64 time = CUtil::CurrentTimeMillis();
		if (time > lastUiUpdate + 10)
		{
			AUTOCRITSEC(sm_critsec);
			u64 timenow = CUtil::CurrentTimeMillis();
			if (timenow > time)
			{
				DEBUGF3("Waited %llu millis to update!", timenow - time);
			}
			UpdateDownloadProgress(CurrentPatch);
			lastUiUpdate = time;
		}

		if (allIdle)
		{
			DEBUGF3("All idle.");
			if (m_sparseFile.ClearRangesInProgess())
			{
				WARNINGF("Clearing ranges in progress with no active connections.");
			}

#if PROGRESSIVE_HASHES
			if (m_sparseFile.IsFileComplete())
			{
				if (!m_sparseFile.CheckHashes(&badBytes))
				{
					WARNINGF("Bad hash detected after finishing file! (%llu bytes!)", badBytes);
					m_bytesRead = m_sparseFile.GetTotalSizeWritten();
				}
			}
#endif

			hasNextRange = m_sparseFile.GetNextRange(&nextRange, CHUNK_SIZE);
			if (hasNextRange)
			{
				DEBUGF1("All idle with unfinished range! next range is %llu - %llu", nextRange.m_offset, nextRange.m_offset + nextRange.m_length - 1);
			}
			else
			{
				DEBUGF1("All idle and no next range.");
			}
		}
	}

	if (!m_downloading && status != DL_STATUS_CANCEL)
	{
		DEBUGF1("Downloading cancelled.");
		status = DL_STATUS_CANCEL;
	}

	// Mark the download as stopping
	sm_chunkedDownloadRunning = false;


	DEBUGF1("Unregistering connections...");
	for (int i = 0; i < HARD_MAX_DOWNLOAD_CONNECTIONS; i++)
	{
		UnregisterConnection(&connections[i]);
	}

	DEBUGF1("Cancelling any hash checks...");
	m_sparseFile.CancelHashChecks();

	bool allCleanedUp = false;
	u64 waitStartTime = CUtil::CurrentTimeMillis();
	while (!allCleanedUp)
	{
		allCleanedUp = true;
		for (int i = 0; i < HARD_MAX_DOWNLOAD_CONNECTIONS; i++)
		{
			if (connections[i].GetState() != Connection::STATE_IDLE)
				allCleanedUp = false;
		}

		if (!allCleanedUp)
		{
			// Wait a maximum of 3 seconds for connections to clean up...
			if (CUtil::CurrentTimeMillis() - waitStartTime < 3000)
			{
				DEBUGF1("Some connections haven't cleaned up; waiting...");
				UpdateListener(DL_STATUS_RETRY);
				Sleep(100);
			}
			else
			{
				RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::CDC_FORCE_CLOSE);
				ERRORF("Some connections never cleaned up!  Doing it manually!");
				for (int i = 0; i < HARD_MAX_DOWNLOAD_CONNECTIONS; i++)
				{
					connections[i].Cleanup(true);
				}
			}
		}
		else
		{
			DEBUGF1("All connections cleaned up.");
		}
	}

	delete[] connections;

	if (m_sparseFile.GetNextRange(&nextRange, 1) && status == DL_STATUS_COMPLETE)
	{
		RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::CD_SIZE_MISMATCH);
		DEBUGF1("Size mismatch on download!");
		status = DL_STATUS_ERROR;
	}
	else if (status == DL_STATUS_ERROR)
	{
		RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::CD_ENDED_WITH_ERROR);
	}


	m_sparseFile.Close();

	DeleteHashFile(patch);

	if (patch.Progress)
	{
		RgscTelemetryManager::WriteEventFileDownload(patch);
	}
	
	return status;
}

void ChunkedDownloader::AddBytesDownloaded(s64 bytes)
{
	AUTOCRITSEC(sm_critsec);
	m_timingDataSize += bytes;

	//@@: location CHUNKEDDOWNLOADER_ADDBYTESDOWNLOADED_CHECK_SIZE_WRITTEN
	m_bytesRead = m_sparseFile.GetTotalSizeWritten();

	UpdateDownloadProgress(CurrentPatch);

	if (CurrentPatch.Progress && bytes > 0)
	{
		CurrentPatch.Progress->UpdateProgress(DL_STATUS_OK, m_bytesRead, m_totalBytes, (u64)bytes);
	}
}

void ChunkedDownloader::UpdateDownloadProgress(Patch& patch)
{
	AUTOCRITSEC(sm_critsec);
	if (!m_paused)
	{
		RockstarDownloader::UpdateDownloadProgress(patch);
	}
}

void ChunkedDownloader::UpdateListener(DownloadCompleteStatus status)
{
	AUTOCRITSEC(sm_critsec);
	if (m_paused)
	{
		RockstarDownloader::UpdateListener(DL_STATUS_PAUSE);
	}
	else
	{
		RockstarDownloader::UpdateListener(status);
	}
}

void CALLBACK ChunkedDownloader::ConnectionCallback(HINTERNET handle, DWORD_PTR context, DWORD status, LPVOID statusInformation, DWORD statusInformationLength)
{
	Connection* connection = NULL;
	{
		AUTOCRITSEC(sm_critsec);
		connection = GetConnection((int)context);
		if (connection)
		{
			connection->SetInCallback(true);
		}
	}

	if (connection)
	{
		connection->Callback(handle, status, statusInformation, statusInformationLength);
	}

	{
		AUTOCRITSEC(sm_critsec);
		if (connection)
		{
			connection->SetInCallback(false);

			if (connection->GetState() == Connection::STATE_CANCELLED)
			{
				if (connection->IsInCallback())
				{
					WARNINGF("Wanting to clean up connection %x, but depth is not 0");
				}
				else
				{
					DEBUGF1("Cleaning up connection that was cancelled during a callback.");
					connection->Cleanup();
				}
			}
		}
	}
}

int ChunkedDownloader::RegisterConnection(Connection* connection)
{
	AUTOCRITSEC(sm_critsec);
	sm_connectionMap[sm_connectionIndex] = connection;
	//@@: location CHUNKEDDOWNLOADER_REGISTERCONNECTION_INCREMENT
	return sm_connectionIndex++;
}

void ChunkedDownloader::UnregisterConnection(Connection* connection)
{
	AUTOCRITSEC(sm_critsec);

	//@@: location CHUNKEDDOWNLOADER_UNREGISTERCONNECTION
	sm_connectionMap.erase(connection->GetIndex());

	if (connection->IsInCallback())
	{
		DEBUGF1("Cancelling a connection that is in the callback.");
		connection->Cancel();
	}
	else
	{
		connection->Cleanup();
	}
}

ChunkedDownloader::Connection* ChunkedDownloader::GetConnection(int key)
{
	AUTOCRITSEC(sm_critsec);
	return sm_connectionMap[key];
}


void ChunkedDownloader::DownloadHashFile(Patch& patch)
{
	std::string hashFilePathOnDisk = patch.LocalFilename + sm_hashSuffix;
#if RSG_TIMEBOMBED_URLS_IN_SCDLL && RSG_LAUNCHER
	
	std::string hashFileURL;

	if (patch.NeedsAccessControlledUrl)
	{
		std::string hashFileAccessKey = patch.AccessControlKey + sm_hashSuffix;

		EntitlementManager::EntitlementResult result = EntitlementManager::ENTITLEMENT_ERROR;
		SocialClub::GetEntitlementManager()->RequestTimebombedUrlSynchronous(hashFileAccessKey, &result, hashFileURL);

		if (result == EntitlementManager::ENTITLEMENT_ERROR || result == EntitlementManager::ENTITLEMENT_FALSE)
		{
			ERRORF("Unable to request URL for hash file %s", hashFileAccessKey.c_str());
			return;
		}
	}
	else
	{
		hashFileURL = patch.PatchURL + sm_hashSuffix;
	}
#else
	std::string hashFileURL = patch.PatchURL + sm_hashSuffix;
#endif

	// Not found, so try and download it
	struct Closure : public DownloadListener
	{
		bool m_error;

		Closure() : m_error(false) {}

		virtual void DownloadComplete(const Patch& /*patch*/, DownloadCompleteStatus /*status*/) {}
		virtual void DownloadStatus(const Patch& /*patch*/, DownloadCompleteStatus /*status*/, u64 /*totalBytesComplete*/, u64 /*totalBytesInFile*/, u64 /*bytesJustDownloaded*/) {}
		virtual void DownloadError(const Patch& /*patch*/, const CError& /*error*/)
		{
			m_error = true;
		};
	} closure;

	// Create, start, and clean-up the download
	RockstarDownloader down;
	down.Init();
	down.SetCurrentFile(hashFileURL, hashFilePathOnDisk, false, true);
	down.SetListener(&closure);
	down.SetMaxRetries(OnlineConfig::PatchRetries);
	down.StartSynchronously();
	down.SetListener(NULL);

	// If we got it, read it in
	if (!closure.m_error)
	{
		m_sparseFile.ReadHashFile(hashFilePathOnDisk.c_str());
	}
}

void ChunkedDownloader::DeleteHashFile(const Patch& patch)
{
	std::string hashFilePathOnDisk = patch.LocalFilename + sm_hashSuffix;

	if (CUtil::FileExists(hashFilePathOnDisk))
	{
		if (!CFileWrapper::DeleteFileA(hashFilePathOnDisk.c_str()))
		{
			WARNINGF("Unable to delete hash file %s!", hashFilePathOnDisk.c_str());
		}
	}
	else
	{
		DEBUGF1("No hash file to delete.");
	}
}

ChunkedDownloader::Connection::Connection() :
	m_state(STATE_IDLE),
	m_context(NULL),
	m_connection(NULL),
	m_request(NULL),
	m_initialOffset(0),
	m_currentOffset(0),
	m_chunkSize(0),
	m_hostname(NULL),
	m_documentPath(NULL),
	m_useTls(false),
	m_hasBeenUsed(false),
	m_sparseFile(NULL),
	m_lastUpdateTimeMillis(0),
	m_lastReportTimeMillis(0),
	m_bytesToReport(0),
	m_index(-1),
	m_callbackDepth(0),
	m_parentDownloader(NULL),
	m_bufferSize(0)
#if !RSG_FINAL
	,m_approxBytesPerSec(0)
#endif
{
	ZeroMemory(&m_buffers, sizeof(m_buffers));
	m_buffers.dwStructSize = sizeof(m_buffers);
}

ChunkedDownloader::Connection::~Connection()
{
}

void ChunkedDownloader::Connection::Cleanup(bool force)
{
	AUTOCRITSEC(sm_critsec);

	if (m_callbackDepth != 0)
	{
		ERRORF("Attempting to cleanup connection %x while it's in the callback!", m_index);

		if (!force)
		{
			return;
		}
	}

	m_index = -1;

	if (m_request)
	{
		InternetCloseHandle(m_request);
		m_request = NULL;
	}

	if (m_connection)
	{
		InternetCloseHandle(m_connection);
		m_connection = NULL;
	}

	if (m_context)
	{
		InternetCloseHandle(m_context);
		m_context = NULL;
	}

	if (m_buffers.lpvBuffer)
	{
		delete[] m_buffers.lpvBuffer;
		m_buffers.lpvBuffer = NULL;
	}

	CONNECTION_DEBUGF3("Cleaned up.");
	SetState(STATE_IDLE);
}

void ChunkedDownloader::Connection::Init(ChunkedDownloader* downloader, TCHAR* hostname, TCHAR* documentPath, bool useTls, SparseFileManager* sparseFile)
{
	m_parentDownloader = downloader;
	m_hostname = hostname;
	//@@: location CHUNKEDDOWNLOADER_CONNECTION_INIT
	m_documentPath = documentPath;
	m_useTls = useTls;
	m_sparseFile = sparseFile;
	m_error.Reset();
}

void ChunkedDownloader::Connection::Register()
{
	m_index = RegisterConnection(this);
}

void ChunkedDownloader::Connection::SetRangeInProgress(bool inProgress)
{
	Range range(m_initialOffset, m_chunkSize);
	m_sparseFile->SetRangeInProgress(range, inProgress);
}

void ChunkedDownloader::Connection::SetChunk(u64 offset, u64 length)
{
	m_initialOffset = offset;
	m_currentOffset = offset;
	m_chunkSize = length;
	m_error.Reset();
}

void ChunkedDownloader::Connection::StartCheckingChunk()
{
	m_currentOffset = m_initialOffset;
	m_hasBeenUsed = true;

	if (m_buffers.lpvBuffer)
	{
		delete[] m_buffers.lpvBuffer;
	}

	m_bufferSize = OnlineConfig::DownloadBufferSizeBytes;
	m_buffers.lpvBuffer = new char[m_bufferSize];
	m_buffers.dwBufferLength = 0;

	m_lastUpdateTimeMillis = CUtil::CurrentTimeMillis();

	CreateContext();

	StartDownloadingChunk();

}

void ChunkedDownloader::Connection::CreateContext()
{
	if (!m_context)
	{
		// Get user agent name
		std::wstring userAgent;
		CUtil::StdStringToStdWString(Constants::LauncherExeNameWithoutExtension, userAgent);

		m_context = InternetOpen(userAgent.c_str(), INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, INTERNET_FLAG_ASYNC);

		ULONG timeout = m_parentDownloader->GetTimeout();
		if (!InternetSetOption(m_context, INTERNET_OPTION_RECEIVE_TIMEOUT, &timeout, sizeof(timeout)))
		{
			ERRORF("Unable to set timeout on m_context");
		}

		InternetSetStatusCallback(m_context, ConnectionCallback);
	}
}

#if (!RSG_FINAL) || (RSG_FINAL_DEBUG)
static const char* GetInternetStatusName(DWORD id)
{
	switch (id)
	{
	case INTERNET_STATUS_RESOLVING_NAME: return "INTERNET_STATUS_RESOLVING_NAME";
	case INTERNET_STATUS_NAME_RESOLVED: return "INTERNET_STATUS_NAME_RESOLVED";
	case INTERNET_STATUS_CONNECTING_TO_SERVER: return "INTERNET_STATUS_CONNECTING_TO_SERVER";
	case INTERNET_STATUS_CONNECTED_TO_SERVER: return "INTERNET_STATUS_CONNECTED_TO_SERVER";
	case INTERNET_STATUS_SENDING_REQUEST: return "INTERNET_STATUS_SENDING_REQUEST";
	case INTERNET_STATUS_REQUEST_SENT: return "INTERNET_STATUS_REQUEST_SENT";
	case INTERNET_STATUS_RECEIVING_RESPONSE: return "INTERNET_STATUS_RECEIVING_RESPONSE";
	case INTERNET_STATUS_RESPONSE_RECEIVED: return "INTERNET_STATUS_RESPONSE_RECEIVED";
	case INTERNET_STATUS_CTL_RESPONSE_RECEIVED: return "INTERNET_STATUS_CTL_RESPONSE_RECEIVED";
	case INTERNET_STATUS_PREFETCH: return "INTERNET_STATUS_PREFETCH";
	case INTERNET_STATUS_CLOSING_CONNECTION: return "INTERNET_STATUS_CLOSING_CONNECTION";
	case INTERNET_STATUS_CONNECTION_CLOSED: return "INTERNET_STATUS_CONNECTION_CLOSED";
	case INTERNET_STATUS_HANDLE_CREATED: return "INTERNET_STATUS_HANDLE_CREATED";
	case INTERNET_STATUS_HANDLE_CLOSING: return "INTERNET_STATUS_HANDLE_CLOSING";
	case INTERNET_STATUS_DETECTING_PROXY: return "INTERNET_STATUS_DETECTING_PROXY";
	case INTERNET_STATUS_REQUEST_COMPLETE: return "INTERNET_STATUS_REQUEST_COMPLETE";
	case INTERNET_STATUS_REDIRECT: return "INTERNET_STATUS_REDIRECT";
	case INTERNET_STATUS_INTERMEDIATE_RESPONSE: return "INTERNET_STATUS_INTERMEDIATE_RESPONSE";
	case INTERNET_STATUS_USER_INPUT_REQUIRED: return "INTERNET_STATUS_USER_INPUT_REQUIRED";
	case INTERNET_STATUS_STATE_CHANGE: return "INTERNET_STATUS_STATE_CHANGE";
	case INTERNET_STATUS_COOKIE_SENT: return "INTERNET_STATUS_COOKIE_SENT";
	case INTERNET_STATUS_COOKIE_RECEIVED: return "INTERNET_STATUS_COOKIE_RECEIVED";
	case INTERNET_STATUS_PRIVACY_IMPACTED: return "INTERNET_STATUS_PRIVACY_IMPACTED";
	case INTERNET_STATUS_P3P_HEADER: return "INTERNET_STATUS_P3P_HEADER";
	case INTERNET_STATUS_P3P_POLICYREF: return "INTERNET_STATUS_P3P_POLICYREF";
	case INTERNET_STATUS_COOKIE_HISTORY: return "INTERNET_STATUS_COOKIE_HISTORY";
	default: return "unknown";
	}
}

const char* ChunkedDownloader::Connection::GetConnectionStateName(ConnectionState state)
{
	switch (state)
	{
	case ChunkedDownloader::Connection::STATE_IDLE: return "STATE_IDLE";
	case ChunkedDownloader::Connection::STATE_STARTING_DOWNLOAD: return "STATE_STARTING_DOWNLOAD";
	case ChunkedDownloader::Connection::STATE_DOWNLOADING: return "STATE_DOWNLOADING";
	case ChunkedDownloader::Connection::STATE_ERROR: return "STATE_ERROR";
	case ChunkedDownloader::Connection::STATE_COMPLETE: return "STATE_COMPLETE";
	case ChunkedDownloader::Connection::STATE_CANCELLED: return "STATE_CANCELLED";
	default:
		return "Unknown connection state!";
	}
}
#endif

#if !RSG_FINAL && CONNECTION_VERBOSE_DEBUG
#define NONFINAL_VERBOSE_ONLY(x) x
#else
#define NONFINAL_VERBOSE_ONLY(x)
#endif

void ChunkedDownloader::Connection::Callback(HINTERNET handle, DWORD status, LPVOID NONFINAL_VERBOSE_ONLY(statusInformation), DWORD NONFINAL_VERBOSE_ONLY(statusInformationLength))
{
#if !RSG_FINAL && CONNECTION_VERBOSE_DEBUG
	CONNECTION_DEBUGF3("Callback: status %lu (%s), info %p, infoLen %lu", status, GetInternetStatusName(status), statusInformation, statusInformationLength);
#endif

	if (!sm_chunkedDownloadRunning)
	{
		DEBUGF1("Callback coming in after download ended.");
		return;
	}

	if (GetState() != STATE_STARTING_DOWNLOAD && GetState() != STATE_DOWNLOADING)
	{
#if RSG_DEBUG
		WARNINGF("Callback in state %s", GetConnectionStateName(GetState()));
#endif
		return;
	}

	if (status == INTERNET_STATUS_CONNECTION_CLOSED)
	{
		CONNECTION_DEBUGF3("Connection closed (timed out?).");
		WARNINGF("CDC: 1");
		m_error = CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_INTERRUPTED);
		SetState(STATE_ERROR);
		return;
	}

	// Ignore all but complete requests
	if (status != INTERNET_STATUS_REQUEST_COMPLETE)
	{
#if RSG_DEBUG && CONNECTION_VERBOSE_DEBUG
		CONNECTION_DEBUGF3("Ignoring callback status: %s", GetInternetStatusName(status));
#endif
		return;
	}

	if (handle != m_request)
	{
		if (handle == m_connection)
		{
			CONNECTION_DEBUGF3("Passed connection handle %p, expecting request handle %p", handle, m_request);
		}
		else
		{
			CONNECTION_DEBUGF3("Wrong handle? %p, expecting %p", handle, m_request);
		}
	}

	ReadChunkPart();

	if (m_currentOffset - m_initialOffset == m_chunkSize)
	{
		//@@: location CHUNKEDDOWNLOADER_CONNECTION_CALLBACK_CHECK_HASH
		CONNECTION_DEBUGF3("No hash; assuming download succeeded.");
		SetState(STATE_COMPLETE);

	}
}

void ChunkedDownloader::Connection::ReportBytes(u64 bytesRead, bool force)
{
	m_bytesToReport += bytesRead;

	u64 time = CUtil::CurrentTimeMillis();
	m_lastUpdateTimeMillis = time;

	if (time < m_lastReportTimeMillis + 1000 && !force)
	{
		return;
	}
#if !RSG_FINAL

	if (!force)
	{
		m_approxBytesPerSec = (m_bytesToReport * 1000L) / (time-m_lastReportTimeMillis);
	}

#if CONNECTION_VERBOSE_DEBUG
	CONNECTION_DEBUGF3("Downloading at %llu bytes/sec", m_approxBytesPerSec);
#endif

#endif

	m_lastReportTimeMillis = time;

	m_parentDownloader->AddBytesDownloaded(m_bytesToReport);

	m_bytesToReport = 0;
}

#if !RSG_FINAL
void ChunkedDownloader::Connection::DebugConnectionStatus()
{
	if (m_state == STATE_IDLE)
		CONNECTION_DEBUGF3("STATE_IDLE");
	else if (m_state == STATE_STARTING_DOWNLOAD)
		CONNECTION_DEBUGF3("STATE_STARTING_DOWNLOAD");
	else if (m_state == STATE_DOWNLOADING)
		CONNECTION_DEBUGF3("STATE_DOWNLOADING at %8llu bytes/sec, %10llub / %10llub (%3d%%)", m_approxBytesPerSec, m_currentOffset-m_initialOffset, m_chunkSize, (int)(100 * (m_currentOffset-m_initialOffset) / (m_chunkSize?m_chunkSize:1)));
	else if (m_state == STATE_COMPLETE)
		CONNECTION_DEBUGF3("STATE_COMPLETE");
	else if (m_state == STATE_ERROR)
		CONNECTION_DEBUGF3("STATE_ERROR");
	else if (m_state == STATE_CANCELLED)
		CONNECTION_DEBUGF3("STATE_CANCELLED");
	else if (m_state == STATE_VERIFYING)
		CONNECTION_DEBUGF3("STATE_VERIFYING");
	else
		CONNECTION_DEBUGF3("(state unknown)");
}
#endif

void ChunkedDownloader::Connection::ReadChunkPart()
{
	m_lastUpdateTimeMillis = CUtil::CurrentTimeMillis();

	u64 bytesToReport = 0;

	bool dataAvailable = true;
	while (dataAvailable && (GetState() == STATE_STARTING_DOWNLOAD || GetState() == STATE_DOWNLOADING) && sm_chunkedDownloadRunning)
	{
		dataAvailable = false;

		DWORD bytesRead = m_buffers.dwBufferLength;
		if (bytesRead > 0)
		{
#if RSG_DEBUG && CONNECTION_VERBOSE_DEBUG
			if ((bytesRead % 1024) == 0)
			{
				CONNECTION_DEBUGF3("%luKB @ %llu", bytesRead / 1024, m_currentOffset);
			}
			else
			{
				CONNECTION_DEBUGF3("%luB @ %llu", bytesRead, m_currentOffset);
			}
#endif
			
#if RSG_DEBUG
#if CONNECTION_VERBOSE_DEBUG
			{
				AUTOCRITSEC(sm_critsec);

				u64 prevParent = m_parentDownloader->m_bytesRead;
				u64 prevFile = m_sparseFile->GetTotalSizeWritten();

				if (prevParent != prevFile)
				{
					CONNECTION_DEBUGF3("Initial mismatch (%llu vs %llu)", prevParent, prevFile);
				}
			}
#endif
#endif
			if (m_sparseFile->WriteToMemory((char*)m_buffers.lpvBuffer, bytesRead, m_currentOffset))
			{
				ConnectionState oldState = m_state;
				SetState(STATE_VERIFYING);
				if (!m_sparseFile->VerifyAndWrite())
				{
					WARNINGF("Corrupt chunk downloaded.");
					RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::CDC_VERIFICATION_FAILURE);
					m_parentDownloader->m_listener->DownloadEvent(m_parentDownloader->CurrentPatch, IDownloader::DLE_VERIFICATION_FAILURE);
					if (m_parentDownloader->CurrentPatch.Progress)
					{
						m_parentDownloader->CurrentPatch.Progress->DownloadEvent(IDownloader::DLE_VERIFICATION_FAILURE);
					}

					ReportBytes(bytesToReport);
					m_error = CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_BAD_CRC);
					SetState(STATE_ERROR);
					
					return;
				}
				SetState(oldState);
			}

			if (!sm_chunkedDownloadRunning)
			{
				DEBUGF1("Download has ended; returning before we try to download anything else.");
				return;
			}

			m_currentOffset += bytesRead;
			bytesToReport += bytesRead;

			// This keeps the main thread informed that the connection hasn't stopped responding
			if (bytesToReport > 0)
			{
				ReportBytes(bytesToReport);
				bytesToReport = 0;
			}
		}
		else if (GetState() == STATE_DOWNLOADING)
		{
			if (m_currentOffset - m_initialOffset < m_chunkSize)
			{
				CONNECTION_DEBUGF3("Received 0 bytes.");
				WARNINGF("CDC: 2");
				m_error = CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_READ_FAILED);
				SetState(STATE_ERROR);
			}
			else
			{
				CONNECTION_DEBUGF3("Finished download?");
				DISPLAYF("CDC: 4");
				SetState(STATE_COMPLETE);
			}

			ReportBytes(bytesToReport, true);

			return;
		}

		if (m_currentOffset - m_initialOffset < m_chunkSize)
		{
			//DEBUGF3("Requesting further read.");
			m_buffers.dwBufferLength = m_bufferSize;
			memset(m_buffers.lpvBuffer, 0, m_bufferSize);
#if CONNECTION_VERBOSE_DEBUG
			CONNECTION_DEBUGF3("InternetReadFileEx");
#endif
			if (InternetReadFileEx(m_request, &m_buffers, IRF_ASYNC, m_index))
			{
				dataAvailable = true;
#if CONNECTION_VERBOSE_DEBUG
				CONNECTION_DEBUGF3("Instant read...");
#endif

#if RSG_DEBUG
				if (m_buffers.dwBufferLength == 0)
				{
					CONNECTION_DEBUGF3("Error - zero-length instant read.");

					m_buffers.dwBufferLength = m_bufferSize;
					memset(m_buffers.lpvBuffer, 0, m_bufferSize);
					if (InternetReadFileEx(m_request, &m_buffers, IRF_NO_WAIT, m_index))
					{
						if (m_buffers.dwBufferLength == 0)
						{
							CONNECTION_DEBUGF3("Error - another zero-length instant read.");
						}
					}
					else
					{
						if (GetLastError() != ERROR_IO_PENDING)
						{
							CONNECTION_DEBUGF3("Error:");
							PrintInternetError();
							SetState(STATE_ERROR);
							return;
						}
						else
						{
							CONNECTION_DEBUGF3("IRF_NO_WAIT but IO_PENDING...");
						}
					}

				}
				else
				{
					size_t writtenSize = m_bufferSize;
					for (;writtenSize >= 1; writtenSize--)
					{
						if (((char*)m_buffers.lpvBuffer)[writtenSize-1] != '\0')
						{
							break;
						}
					}
					
					if (writtenSize != m_buffers.dwBufferLength)
					{
						CONNECTION_DEBUGF3("Error - %llu bytes written, but %lu reported. (Total buffer size: %lu)", writtenSize, m_buffers.dwBufferLength, m_bufferSize);
					}
				}
#endif
			}
			else
			{
				if (GetLastError() != ERROR_IO_PENDING)
				{
					CONNECTION_DEBUGF3("Error:");
					PrintInternetError();
					m_error = CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_READ_ERROR);
					WARNINGF("CDC: 5");
					SetState(STATE_ERROR);

					ReportBytes(bytesToReport, true);
					return;
				}
				else
				{
#if CONNECTION_VERBOSE_DEBUG
					CONNECTION_DEBUGF3("IO_PENDING...");
#endif
				}
			}

			if (m_state == STATE_STARTING_DOWNLOAD)
			{
				CONNECTION_DEBUGF3("Downloading...");
			}
			SetState(STATE_DOWNLOADING);
		}
		else
		{
			CONNECTION_DEBUGF3("No more data left in chunk.");
			break;
		}
	}

	if (!sm_chunkedDownloadRunning)
	{
		DEBUGF1("Download has ended; returning.");
		return;
	}

	ReportBytes(bytesToReport, true);
	
	// Mark connection as complete if there's no more data to read
	if (m_currentOffset - m_initialOffset >= m_chunkSize)
	{
		CONNECTION_DEBUGF3("Finished download!");
		SetState(STATE_COMPLETE);
	}
}


void ChunkedDownloader::Connection::StartDownloadingChunk()
{
	if (!IsRegistered())
	{
		Register();
	}

	SetState(STATE_STARTING_DOWNLOAD);

	// WinInet seems reluctant to reuse connections, so make sure we clean up the old request and connection.
	if (m_request)
	{
		CONNECTION_DEBUGF3("Closing old request.");
		InternetCloseHandle(m_request);
		m_request = NULL;
	}

	if (m_connection)
	{
		CONNECTION_DEBUGF3("Closing old connection.");
		InternetCloseHandle(m_connection);
		m_connection = NULL;
	}

	// Open connection
	if (!m_connection)
	{
		m_connection = InternetConnect(m_context, m_hostname, m_useTls ? INTERNET_DEFAULT_HTTPS_PORT : INTERNET_DEFAULT_HTTP_PORT, NULL, NULL, INTERNET_SERVICE_HTTP, 0, m_index);
	}

	// Request flags
	DWORD flags = INTERNET_FLAG_RELOAD | INTERNET_FLAG_DONT_CACHE | INTERNET_FLAG_PRAGMA_NOCACHE | (m_useTls ? INTERNET_FLAG_SECURE : 0) | INTERNET_FLAG_NO_CACHE_WRITE;

	// Accept types
	const WCHAR* acceptTypes[] = { L"*/*", NULL };

	// Range header
	TCHAR wideHeader[64];
	char header[64];
	
	// Range request is inclusive, hence -1
	sprintf_s(header, "Range: bytes=%llu-%llu", m_initialOffset, m_initialOffset + m_chunkSize - 1);
	MultiByteToWideChar(CP_UTF8, 0, header, -1, wideHeader, 64);

#if CONNECTION_VERBOSE_DEBUG
	CONNECTION_DEBUGF3("Using range header: %s", header);
#endif

	// Open request
	m_request = HttpOpenRequest(m_connection, L"GET", m_documentPath, HTTP_VERSION, NULL, acceptTypes, flags, m_index);

	if (!m_request)
	{
		CONNECTION_DEBUGF3("No request.");
		WARNINGF("CDC: 3");
		PrintInternetError();
		m_error = CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_REQUEST_ERROR);
		SetState(STATE_ERROR);
		return;
	}

	// Add range header
	HttpAddRequestHeaders(m_request, wideHeader, (DWORD)_tcslen(wideHeader), HTTP_ADDREQ_FLAG_ADD_IF_NEW); 

	// Send
	CONNECTION_DEBUGF3("HttpSendRequest");
	if (!HttpSendRequest(m_request, NULL, 0, NULL, 0))
	{
		if (GetLastError() != ERROR_IO_PENDING)
		{
			CONNECTION_DEBUGF3("HttpSendRequest failed.");
			PrintInternetError();
			m_error = CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_SEND_ERROR);
			WARNINGF("CDC: 6");
			SetState(STATE_ERROR);
			return;
		}
		else
		{
			CONNECTION_DEBUGF3("HttpSendRequest I/O pending.");
		}
	}
	else
	{
		CONNECTION_DEBUGF3("HttpSendRequest instant success.");
		
		// Carry on with the read if HttpSendRequest returns instantly
		ReadChunkPart();
	}
}

void ChunkedDownloader::Connection::ClearError()
{
	CONNECTION_DEBUGF3("Clearing error.");
	m_error.Reset();
	SetState(STATE_IDLE);
}

void ChunkedDownloader::Connection::ClearCompletion()
{
	CONNECTION_DEBUGF3("Clearing completion.");
	SetState(STATE_IDLE);
}

void ChunkedDownloader::Connection::Cancel()
{
	CONNECTION_DEBUGF3("Cancelling.");
	SetState(STATE_CANCELLED);
}

void ChunkedDownloader::Connection::SetState(ConnectionState state)
{
	AUTOCRITSEC(sm_critsec);

	if (m_state == STATE_CANCELLED && state != STATE_IDLE)
	{
		CONNECTION_DEBUGF3("Trying to set a state other than STATE_IDLE after being cancelled!");
		return;
	}

	if (state != m_state)
	{
#if CONNECTION_VERBOSE_DEBUG
		CONNECTION_DEBUGF3("Setting state %s", GetConnectionStateName(state));
#endif
	}

	m_state = state;
}
