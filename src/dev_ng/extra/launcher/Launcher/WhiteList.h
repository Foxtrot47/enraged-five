#pragma once

#include <string>
#include <vector>

class WhiteList
{
public:	
	static bool Add(const std::string& url);
	static bool Exists(const std::string& url);
	static void Clear();
private:
	static bool Exists_internal(const std::string& url);
	static WhiteList g_whiteList;
	std::vector<std::string> m_WhiteList;
	WhiteList();
	~WhiteList();

};
