#include "stdafx.h"
#include "JsonReader.h"

JsonReader Invalid(NULL);

JsonReader::JsonReader(const char* input) : m_data(input)
{
}

bool JsonReader::IsValid()
{
	return m_data != NULL;
}

JsonReader JsonReader::GetFirstChild()
{
	if (!m_data)
		return Invalid;

	const char* cursor = GetNextNonWhitespaceChar(m_data);
	if (!cursor)
		return Invalid;

	if (*cursor == '\"')
	{
		// Read past name of the current element.
		// E.g. "property" : { ...
		cursor = ReadToPairSecondElement(cursor);

		if (!cursor)
			return Invalid;
	}

	// Should be at a bracket now.
	if (*cursor != '[' && *cursor != '{')
		return Invalid;

	// Finally, get the next thing after the bracket and check it's okay
	cursor = GetNextNonWhitespaceChar(cursor+1);
	if (!cursor || !ValidStartOfElement(cursor))
		return Invalid;

	return JsonReader(cursor);
}

JsonReader JsonReader::GetNextSibling()
{
	if (!m_data)
		return Invalid;

	const char* cursor = GetNextNonWhitespaceChar(m_data);
	if (!cursor || *cursor == '\0')
		return Invalid;

	// See if we are named - if so, skip the name and colon
	if (*cursor == '\"')
	{
		cursor = ReadToPairSecondElement(cursor);
		if (!cursor)
			return Invalid;
	}

	// Read past this entire element
	cursor = ReadPastElement(cursor);

	// Should be a comma
	cursor = GetNextNonWhitespaceChar(cursor);
	if (!cursor || *cursor != ',')
		return Invalid;

	// Should be the start of an element
	cursor = GetNextNonWhitespaceChar(cursor+1);
	if (!ValidStartOfElement(cursor))
		return Invalid;

	return JsonReader(cursor);
}

JsonReader JsonReader::GetChild(const char* name)
{
	if (!m_data)
		return Invalid;

	JsonReader sibling = GetFirstChild();
	while (sibling.IsValid())
	{
		if (sibling.NameEquals(name))
			return sibling;

		sibling = sibling.GetNextSibling();
	}

	return Invalid;
}


bool JsonReader::NameEquals(const char* value)
{
	if (!m_data)
		return false;

	const char* cursor = GetNextNonWhitespaceChar(m_data);
	if (!cursor || *cursor != '"' || !value)
		return false;

	// Ensure this is in fact a name, and not a string in an array
	const char* colonCheck = GetNextNonWhitespaceChar(ReadPastQuotedString(cursor));
	if (!colonCheck || *colonCheck != ':')
		return false;

	return QuotedJsonStringEqualToString(cursor, value);
}

bool JsonReader::StringValueEquals(const char* value)
{
	if (!m_data)
		return false;

	const char* cursor = GetNextNonWhitespaceChar(m_data);
	if (!cursor || *cursor != '"' || !value)
		return false;

	// Read past name
	cursor = ReadToPairSecondElement(cursor);
	if (!cursor || *cursor != '"')
		return false;

	return QuotedJsonStringEqualToString(cursor, value);
}

bool JsonReader::GetStringValue(std::string& out_value)
{
	if (!m_data)
		return false;

	const char* cursor = GetNextNonWhitespaceChar(m_data);
	if (!cursor || *cursor != '"')
		return false;

	// Read past name
	cursor = ReadToPairSecondElement(cursor);
	if (!cursor || *cursor != '"')
		return false;

	return ReadQuotedString(cursor, out_value);
}

bool isStartOfUnquotedElement(char c)
{
	// Must be number, or the start of "true", "false", or "null" (t, f, or n)
	return (c >= '0' && c <= '9') || c == 't' || c == 'f' || c == 'n';
}

bool JsonReader::GetUnquotedValue(std::string& out_value)
{
	if (!m_data)
		return false;

	// Check this is part of a name-value pair
	const char* cursor = GetNextNonWhitespaceChar(m_data);
	if (!cursor || *cursor != '"')
		return false;

	// Read past name
	cursor = ReadToPairSecondElement(cursor);
	if (!cursor || !isStartOfUnquotedElement(*cursor))
		return false;

	// Find the end of this value
	const char* endPoint = ReadPastUnquotedValue(cursor);
	if (!endPoint || endPoint == cursor)
		return false;

	out_value.assign(cursor, endPoint);
	return true;
}

bool isWhitespace(char c)
{
	return c == ' ' || c == '\t' || c == '\n' || c == '\r';
}

const char* JsonReader::GetNextNonWhitespaceChar(const char* input)
{
	if (!input)
		return NULL;

	while (isWhitespace(*input))
		input++;

	return input;
}

const char* JsonReader::ReadToPairSecondElement(const char* openingQuote)
{
	// Read past the first element in the pair
	const char* cursor = ReadPastQuotedString(openingQuote);
	if (!cursor)
		return NULL;

	// Check we've got a colon
	cursor = GetNextNonWhitespaceChar(cursor);
	if (!cursor  || *cursor != ':')
		return NULL;

	// Get the next character, which should be the valid start of an element
	cursor = GetNextNonWhitespaceChar(cursor+1);
	if (!ValidStartOfElement(cursor))
		return NULL;

	return cursor;
}

const char* JsonReader::ReadPastQuotedString(const char* openingQuote)
{
	// Check we have a valid starting character
	if (!openingQuote || *openingQuote != '\"')
		return NULL;

	const char* cursor = openingQuote;

	bool escaping = false;
	while (true)
	{
		cursor++;
		char c = *cursor;
		if (c == '\0')
			return NULL;
		
		if (escaping)
			escaping = false;
		else
		{
			if (c == '\\')
				escaping = true;
			else if (c == '"')
				break;
		}
	}

	// Check we've got to the second quote
	if (!cursor || *cursor != '\"')
		return NULL;

	cursor++;

	if (*cursor != '\0')
		return cursor;
	else
		return NULL;
}

bool JsonReader::ValidStartOfElement(const char* input)
{
	if (!input)
		return false;

	char c = *input;

	if (c == '"' || c == '{' || c == '[' ||  isStartOfUnquotedElement(c))
		return true;
	else
		return false;
}

const char* JsonReader::ReadPastElement(const char* input)
{
	const char* cursor = input;

	int depth = 0;
	while (true)
	{
		cursor = GetNextNonWhitespaceChar(cursor);

		if (!cursor || *cursor == '\0')
			return NULL;

		char c = *cursor;

		if (c == '"')
		{
			cursor = ReadPastQuotedString(cursor);
			if (depth == 0)
				return cursor;
		}
		else if (c == '{' || c == '[')
		{
			depth++;
			cursor++;
		}
		else if (isStartOfUnquotedElement(c))
		{
			cursor = ReadPastUnquotedValue(cursor);
			if (depth == 0)
				return cursor;
		}
		else if (c == ',' || c == ':')
		{
			cursor++;
		}
		else if (c == '}' || c == ']')
		{
			depth--;
			cursor++;

			if (depth == 0)
				return cursor;
			if (depth < 0)
				return NULL;
		}
		else
			return NULL;
	}
}

const char* JsonReader::ReadPastUnquotedValue(const char* input)
{
	if (!input || !isStartOfUnquotedElement(*input))
		return NULL;

	const char* cursor = input;
	while (true)
	{
		cursor++;
		char c = *cursor;

		if (!(c >= '0' && c <= '9') && !(c >= 'a' && c <= 'z') && c != '.')
			return cursor;
	}
}

bool JsonReader::QuotedJsonStringEqualToString(const char* openingQuote, const char* string)
{
	if (!openingQuote || !string || *openingQuote != '"')
		return false;

	const char* cursor = openingQuote+1;

	bool escaping = false;
	while (true)
	{
		char c = *cursor;
		if (escaping)
		{
			// Translate escaped characters
			if (c == 'n') c = '\n';
			else if (c == 'r') c = '\r';
			else if (c == 't') c = '\t';
		}
		else
		{
			// Check if we have the start of a json escape character
			if (c == '\\')
			{
				escaping = true;
				// Don't increment the comparison string, just the json string
				cursor++;
				continue;
			}
			// If this is an unescaped quote, the string has ended
			else if (c == '\"')
			{
				if (*string == '\0')
					return true;
				else
					return false;
			}
		}

		// Check for failed match, or end of the comparison string (end of the json string is checked above), or unexpected end of the json
		if (c != *string || *string == '\0' || c == '\0')
			return false;

		// Characters must match, increment both strings
		cursor++;
		string++;
	}
}

bool JsonReader::ReadQuotedString(const char* openingQuote, std::string& out_value)
{
	if (!openingQuote || *openingQuote != '\"')
		return false;

	out_value.clear();

	const char* cursor = openingQuote+1;

	bool escaping = false;
	while (true)
	{
		char c = *cursor;
		if (escaping)
		{
			// Translate escaped characters
			if (c == 'n') c = '\n';
			else if (c == 'r') c = '\r';
			else if (c == 't') c = '\t';
		}
		else
		{
			// Check if we have the start of a json escape character
			if (c == '\\')
			{
				escaping = true;
				cursor++;
				continue;
			}
			// If this is an unescaped quote, the string has ended
			else if (c == '\"')
			{
				return true;
			}
		}

		// Check for unexpected end of string
		if (c == '\0')
			return false;

		out_value += c;

		cursor++;
	}

}
