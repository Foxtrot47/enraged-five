#include "stdafx.h"
#include "Test.h"

#include "resource.h"
#include "Globals.h"
#include "Config.h"
#include "JsonReader.h"
#include "JsonWriter.h"
#include "LauncherDlg.h"
#include "DownloadManager.h"
#include "RockstarDownloader.h"
#include "SimpleDownloader.h"
#include "Util.h"
#include "Channel.h"

#include "seh.h"

#include <regex>
#include <set>
#include <algorithm>

#if !RSG_FINAL
void CTest::TestRockstarDownloader()
{
	static class Listener : public IDownloader::DownloadListener
	{
		virtual void DownloadComplete(const Patch& /*patch*/, DownloadCompleteStatus status)
		{
			DEBUGF1("Download ended with status: %d", status);
		}

		virtual void DownloadError(const Patch& /*patch*/, const CError& error)
		{
			DEBUGF1("Download erred with %s", CError::GetLocalisedErrorMessage(error));
		}

		virtual void DownloadStatus(const Patch& /*patch*/, DownloadCompleteStatus /*status*/, u64 totalBytesComplete, u64 totalBytesInFile, u64 bytesJustDownloaded)
		{
			DEBUGF1("Download status: %llu / %llu (added %llu bytes)", totalBytesComplete, totalBytesInFile, bytesJustDownloaded);
		}

	} listener;

	RockstarDownloader* downloader = new RockstarDownloader();
	downloader->SetListener(&listener);

	downloader->SetCurrentFile("http://patches.rockstargames.com/prod/mp3/launcher_version.xml", "C:\\newswire.rss.txt", true, false);
	downloader->StartSynchronously();

	downloader->SetListener(NULL);

	delete downloader;
}

void CTest::TestSimpleDownloader()
{
	CSimpleDownloader downloader("www.rockstargames.com", "/newswire.rss");

	size_t downloadSize;
	char* data = NULL;
	downloader.BlockingDownload(&data, &downloadSize);

	if (data)
		delete[] data;
}

bool CTest::TestXML()
{
	bool success = false;

	/*
		Example XML file:
		<version>
			<current_version>1.2.3.4</current_version>
			<url>http://patches.rockstargames.com/prod/socialclub/Social%20Club%20Setup%20v1.2.3.4.exe<\url>
			<filename>Social Club Setup v1.2.3.4.exe<\filename>
		</version>
	*/

	/*

    INIT_PARSER;

    PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);
	parTree* tree = PARSER.LoadTree("C:\\sc_installer_version.xml", "xml");
    PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, false);

    if (tree && tree->GetRoot())
    {
        rtry
        {
            parTreeNode* pRoot = tree->GetRoot();
            rverify(pRoot, catchall, );

			// current_version
			parTreeNode* pNode = pRoot->FindChildWithName("current_version");
			rverify(pNode, catchall, );

			const char* v = pNode->GetData();
			rverify(v, catchall, );

            success = true;
        }
        rcatchall
        {
        }

        delete tree;
        tree = NULL;
    }

    SHUTDOWN_PARSER;

	*/

    return success;
}

#define QUOT(x) "\"" x "\""

#define ASSERTF(x) if (!(x)) { ERRORF("Assert failure: " #x ); *((int*)0) = 0; }

void CTest::TestJson()
{
	const char* testJson1 = "{"
								QUOT("a") ":123,"
								QUOT("b") ":" QUOT("abc") ","
								QUOT("c") ":"
								"{"
									QUOT("d") ":true,"
									QUOT("e") ":" QUOT("def")
								"},"
								QUOT("f") ":"
								"["
									"{" QUOT("g") ":" QUOT("ghi") "},"
									"{" QUOT("h") ":" QUOT("jkl") "}"
								"],"
								QUOT("i") ":" QUOT("mno")
							"}";

	JsonReader root1(testJson1);
	ASSERTF(root1.IsValid());

	JsonReader a = root1.GetFirstChild();
	ASSERTF(a.IsValid());
	ASSERTF(a.NameEquals("a"));
	std::string aval;
	ASSERTF(a.GetUnquotedValue(aval));
	ASSERTF(aval == "123");

	JsonReader b = a.GetNextSibling();
	ASSERTF(b.IsValid());
	ASSERTF(b.NameEquals("b"));
	ASSERTF(b.StringValueEquals("abc"));
	std::string bval;
	b.GetStringValue(bval);
	ASSERTF(bval == "abc");

	JsonReader c = root1.GetChild("c");
	ASSERTF(c.IsValid());

	JsonReader d = c.GetFirstChild();
	ASSERTF(d.IsValid());
	std::string dval;
	ASSERTF(d.GetUnquotedValue(dval));
	ASSERTF(dval == "true");
	
	JsonReader e = d.GetNextSibling();
	ASSERTF(e.IsValid());
	ASSERTF(e.StringValueEquals("def"));

	JsonReader e2 = e.GetNextSibling();
	ASSERTF(!e2.IsValid());

	JsonReader f = c.GetNextSibling();
	ASSERTF(f.IsValid());
	ASSERTF(f.NameEquals("f"));

	JsonReader g = f.GetFirstChild().GetFirstChild();
	ASSERTF(g.IsValid());
	ASSERTF(g.NameEquals("g"));
	ASSERTF(g.StringValueEquals("ghi"));

	JsonReader h = f.GetFirstChild().GetNextSibling().GetFirstChild();
	ASSERTF(h.IsValid());
	ASSERTF(h.NameEquals("h"));
	ASSERTF(h.StringValueEquals("jkl"));

	JsonReader i = f.GetNextSibling();
	ASSERTF(i.IsValid());
	ASSERTF(i.NameEquals("i"));
	ASSERTF(i.StringValueEquals("mno"));

	JsonWriter writer;
	writer.StartObject().WriteNamedString("a", "abc").StartObject("b").WriteNamedString("c", "def").WriteNamedString("d", "ghi").EndObject().StartArray("e").WriteString("f").WriteString("g").EndArray().EndObject();
	const char* writerExpected = "{" QUOT("a") ":" QUOT("abc") "," QUOT("b") ":{" QUOT("c") ":" QUOT("def") "," QUOT("d") ":" QUOT("ghi") "}," QUOT("e") ":[" QUOT("f") "," QUOT("g") "]}";
	ASSERTF(writer.GetBuffer() == writerExpected);
}

void CTest::TestFakeDownloadUI()
{
	CLauncherDlg* dlg = CLauncherDlg::GetLastInstance();
	if (!dlg)
	{
		ERRORF("NULL dialog!");
		return;
	}

	srand(GetTickCount());
	CUtil::Sleep(500 + (abs(rand()) % 500));

	static int testMode = 0;
	if (testMode == 0)
		dlg->SendMessage(CLauncherDlg::eSetState, CFsm::eNoInternetForActivate, 0);
	else if (testMode == 1)
		dlg->SendMessage(CLauncherDlg::eSetState, CFsm::eOffline, 0);
	else
		dlg->SendMessage(CLauncherDlg::eSetState, CFsm::eRequiredUpdate, 0);

	testMode++;
}

void CTest::TestRegex()
{
	const std::string message = "See http://support.rockstargames.com/hc/en-us.";

	std::regex pattern("(https?://)?(?:\\w+(?:\\.\\w+)+(?:[\\w\\\\/._~#?&+%\\-\\=]*[\\w#/])?)");

	std::smatch results;
	if (std::regex_search(message, results, pattern))
	{
		for (size_t i = 0; i < results.size(); i++)
		{
			const std::string& result = results[i];
			DEBUGF1("Found %s", result.c_str());
		}

		DEBUGF1("Found %d results.", results.size());
	}
	else
	{
		DEBUGF1("Not found.");
	}

}

void CTest::FakeDownloadUpdate(CLauncherDlg* dlg)
{
	if (!dlg)
	{
		ERRORF("NULL dialog!");
		return;
	}

	dlg->SendMessage(CLauncherDlg::eSetProgress, 0, 0);
	dlg->SetDlgItemText(IDC_DETAILTEXT, TEXT("Connecting..."));

	CUtil::Sleep(1000);

	dlg->SendMessage(CLauncherDlg::eSetProgress, 5, 0);
	dlg->SetDlgItemText(IDC_DETAILTEXT, TEXT("Connection established!"));

	CUtil::Sleep(500);


	size_t bufsize = 1024;
	TCHAR buffer[1024];

	long currentBytes = 0, totalBytes = 17 * 1024 * 1024 + 337 * 1024 + 42; // Random-looking filesize (around 17Mb)
	int baseSpeed = 1000;

	while (currentBytes < totalBytes)
	{
		size_t index = 0;
		index = _stprintf_s(&(buffer[index]), bufsize-index, TEXT("Downloading update 1.0.0.69... ("));
		index += CUtil::WriteHumanReadableDataSize(&(buffer[index]), bufsize-index, currentBytes);
		index += _stprintf_s(&(buffer[index]), bufsize-index, TEXT(" / "));
		index += CUtil::WriteHumanReadableDataSize(&(buffer[index]), bufsize-index, totalBytes);
		buffer[index++] = (TCHAR)')';
		buffer[index++] = 0;

		dlg->SetDlgItemText(IDC_DETAILTEXT, buffer);

		dlg->SendMessage(CLauncherDlg::eSetProgress, 5 + (int)(95 * currentBytes / totalBytes), 0);
		CUtil::Sleep(50);

		currentBytes += baseSpeed + (abs(rand()) % 1000);
		if (baseSpeed < 10 * 1024 * 1024)
			baseSpeed += 1000;
	}

	dlg->SendMessage(CLauncherDlg::eSetProgress, 100, 0);
	dlg->SetDlgItemText(IDC_DETAILTEXT, TEXT("Download complete!"));

	CUtil::Sleep(500);

	dlg->SendMessage(CLauncherDlg::eSetState, CFsm::eInstallUpdate, 0);
}

void CTest::FakeInstallUpdate(CLauncherDlg* dlg)
{
	if (!dlg)
	{
		ERRORF("NULL dialog!");
		return;
	}

	CUtil::SetCurrentThreadName("InstallPatchThread");

	//dlg->SendMessage(CLauncherDlg::eSetProgress, 0, 0);
	dlg->SetDlgItemText(IDC_DETAILTEXT, TEXT("Installing..."));

	for (int progress = 0; progress < 100; progress += 20)
	{
		CUtil::Sleep(500 + (abs(rand()) % 2000));
		//dlg->SendMessage(CLauncherDlg::eSetProgress, progress, 0);
	}

	dlg->SendMessage(CLauncherDlg::eSetProgress, 100, 0);
	dlg->SetDlgItemText(IDC_DETAILTEXT, TEXT("Installation complete!"));

	if (Constants::DebugMode)
		CUtil::Sleep(1000);

	dlg->SendMessage(CLauncherDlg::eSetState, CFsm::eShutdownSocialClub, 0);
}

void CTest::FakeUploadReport(CLauncherDlg* dlg)
{
	if (!dlg)
	{
		ERRORF("NULL dialog!");
		return;
	}

	DEBUGF1("Uploading report.");
	dlg->SendMessage(CLauncherDlg::eSetProgress, 0, 0);
	dlg->SetDlgItemText(IDC_DETAILTEXT, TEXT("Sending report..."));

	for (int progress = 0; progress < 100; progress += 25)
	{
		CUtil::Sleep(500 + (abs(rand()) % 2000));
		dlg->SendMessage(CLauncherDlg::eSetProgress, progress, 0);

		DEBUGF1("Progress: %d", progress);
	}

	DEBUGF1("Done.");

	dlg->SendMessage(CLauncherDlg::eSetProgress, 100, 0);
	dlg->SetDlgItemText(IDC_DETAILTEXT, TEXT("Upload complete!"));

	if (Constants::DebugMode)
		CUtil::Sleep(1000);

	dlg->SendMessage(CLauncherDlg::eSetState, CFsm::eSafeMode, 0);
}

void CTest::LocalizationTest()
{
	int m_attemptNumber = 1;
	int maxAttempts = 3;
	std::string token = CLocalisation::Instance().GetString(IDS_DOWNLOAD_ERROR_ATTEMPT);
	CUtil::FormatStringNumbers(token, m_attemptNumber, maxAttempts);
}

void CTest::TestStringReplacements()
{
	std::vector<std::string> params;
	params.push_back("param1");
	params.push_back("param2");

	std::string test = "This should replace '~1~' and '~2~' and leave an empty string for '~3~'";

	DEBUGF1("Replacing string: \"%s\"", test.c_str());
	CUtil::FormatString(test, params);
	DEBUGF1("Result: \"%s\"", test.c_str());

	test = "Now testing SKU: '~GameSku~' and entitlement check: '~SkipEntitlementCheck~' and timeout: '~InitialDownloadTimeoutMillis~' and nonsense parameter: '~foo~'";

	DEBUGF1("Replacing string: \"%s\"", test.c_str());
	CUtil::FormatString(test, params);
	DEBUGF1("Result: \"%s\"", test.c_str());
}


void CTest::AutotestDigitalUpdate()
{
	struct Inner
	{
		static UINT ThreadEntry(LPVOID /*param*/)
		{
			Sleep(1000);

			DEBUGF1("AutotestDigitalUpdate pressing update button...");
			CLauncherDlg::PostMessageSafe(WM_COMMAND, MAKEWPARAM(IDC_LEFTBUTTON, BN_CLICKED));

			Sleep(20000);

			DEBUGF1("AutotestDigitalUpdate terminating launcher...");
			CLauncherDlg::PostMessageSafe(WM_CLOSE);

			return 0;
		}
	};

	AfxBeginThread(Inner::ThreadEntry, NULL);
}

void CTest::AutotestGamePatch()
{
	struct Inner
	{
		static UINT ThreadEntry(LPVOID /*param*/)
		{
			Sleep(1000);

			DEBUGF1("AutotestGamePatch pressing update button...");
			CLauncherDlg::PostMessageSafe(WM_COMMAND, MAKEWPARAM(IDC_LEFTBUTTON, BN_CLICKED));

			return 0;
		}
	};

	AfxBeginThread(Inner::ThreadEntry, NULL);
}

static class AutotestFailData
{
private:
	CRITICAL_SECTION m_critsec;
	std::set<u64> m_hashes;
	size_t m_target;
	bool m_inited;
	volatile bool m_errorHandled;

public:
	AutotestFailData() : m_inited(false), m_target(0)
	{
	}

	~AutotestFailData()
	{
		DeleteCriticalSection(&m_critsec);
	}

	bool Test(const char* file, int line)
	{
		if (!m_inited)
			Init();

		u64 hash = CalcHash(file, line);

		if (m_hashes.insert(hash).second)
		{
			if (m_hashes.size() == m_target)
			{
				DEBUGF1("***");
				DEBUGF1("* AutotestFail: Testing error message at %s:%d", file, line);
				DEBUGF1("***");
				m_errorHandled = false;
				AfxBeginThread(StartTerminationThread, this);
				return true;
			}
			else
			{
				DEBUGF1("AutotestFail: Registering error message at %s:%d", file, line);
			}
		}

		return false;
	}

	void NotifyErrorHandled()
	{
		m_errorHandled = true;
	}

private:
	void Init()
	{
		DEBUGF1("Initializing AutotestFail data.");
		InitializeCriticalSection(&m_critsec);
		std::wstring& param = Globals::autotest_launcher_errors.GetValue();
		m_target = (size_t)_wtol(param.c_str());
		m_inited = true;
	}

	u64 CalcHash(const char* file, int line)
	{
		u64 hash = (u64)file;
		hash ^= ((u64)line << 32) + ((u64)line);
		return hash;
	}

	static UINT StartTerminationThread(LPVOID param)
	{
		((AutotestFailData*)param)->TerminationThreadEntry();
		return 0;
	}

	void TerminationThreadEntry()
	{
		DEBUGF1("Termination thread running...");

		for (int i = 0; i < 100 && !m_errorHandled; i++)
		{
			Sleep(100);
		}

		if (m_errorHandled)
		{
			DEBUGF1("AutotestFail saw error screen.");

			if (Globals::autotest_launcher_errors_delay)
			{
				u64 delay = (u64)_wtol(Globals::autotest_launcher_errors_delay.GetValue().c_str());
				Sleep((DWORD)delay);
			}
			else
			{
				Sleep(500);
			}
		}
		else
		{
			DEBUGF1("AutotestFail assuming failure to display error.");
		}

		if (Globals::autotest_launcher_errors_respawn)
		{
			Respawn();
		}

		if (!Globals::noautokill)
		{
			CLauncherDlg::PostMessageSafe(WM_CLOSE);
		}
	}

	void Respawn()
	{
		DEBUGF1("Respawning...");
		std::wstring argname = Globals::autotest_launcher_errors.GetName();
		argname += L"=";

		LPWSTR cmd = GetCommandLineW();
		if (cmd)
		{
			int argc = 0;
			LPWSTR* argv = CommandLineToArgvW(cmd, &argc);
			std::wstring argsString;
			for (int i = 1; i < argc; i++)
			{
				if (i > 1)
					argsString += L" ";

				if (_wcsnicmp(argname.c_str(), argv[i], argname.size()) == 0)
				{
					argsString += argname;

					std::wstringstream conv;
					conv << (m_target+1);
					argsString += conv.str();
				}
				else
				{
					argsString += argv[i];
				}
			}

			std::wstring path = argv[0];

			if (ShellExecuteW(NULL, L"open", path.c_str(), argsString.c_str(), NULL, SW_SHOWNORMAL))
			{
				DEBUGF1("Launched next process.");
			}
			else
			{
				ERRORF("Failed to launch next process!  How ironic.");
			}

			LocalFree(argv);
		}
	}

} sm_autotestFailData;

bool CTest::AutotestFail(const char* file, int line)
{
	if (!Globals::autotest_launcher_errors)
		return false;

	return sm_autotestFailData.Test(file, line);
}

void CTest::AutotestErrorResult(ErrorResultType result, int parameter)
{
	if (!Globals::autotest_launcher_errors)
		return;

	if (result == CTest::SHOWN_ERROR_SCREEN)
	{
		DEBUGF1("AutotestFail: Notified about reaching error screen.");
		DEBUGF1("AutotestFail: Error state: %s", CFsm::GetStateName((CFsm::FsmState)parameter));

		CLauncherDlg::UiState uistate = CLauncherDlg::GetLastInstance()->m_uiShadowCopy;

		std::wstring message = L"\"";
		message += uistate.status;
		std::replace(message.begin(), message.end(), L'\r', L' ');
		std::replace(message.begin(), message.end(), L'\n', L' ');
		message += L"\" [";

		for (int i = 0; i < 3; i++)
		{
			if (uistate.buttons[i].IsValid())
			{
				if (message[message.size()-1] != L'[')
					message += L", ";
				message += uistate.buttons[i].GetTitle();
			}
		}
		message += L']';

		std::string mbcs;
		CUtil::WStringToStdString(message, mbcs);

		DEBUGF1("AutotestFail: UI state: %s", mbcs.c_str());
	}
	else if (result == CTest::RETRIED)
	{
		DEBUGF1("AutotestFail: Notified about retrying.");
	}
	else if (result == CTest::NOT_AN_ERROR)
	{
		DEBUGF1("AutotestFail: Notified that this is not an error.");
	}

	sm_autotestFailData.NotifyErrorHandled();
}

void CTest::AutotestSetup()
{
	if (!Globals::noautokill && (Globals::autotest_launcher_selfsignin || Globals::autotest_launcher_digitalupdate || Globals::autotest_launcher_gamepatch || Globals::autotest_launcher_errors))
	{
		struct Inner
		{
			static UINT ThreadEntry(LPVOID /*param*/)
			{
				// Maximum of 120 seconds, in case test is unattended and gets stuck.
				for (int i = 0; i < 12; i++)
				{
					Sleep(10*1000);
					DEBUGF1("AutotestAutokill running.");
				}
				DEBUGF1("AutotestAutokill thread wreaking its ruinous vengeance.");
				CLauncherDlg::PostMessageSafe(WM_CLOSE);
				return 0;
			}
		};


		AfxBeginThread(Inner::ThreadEntry, NULL);
	}
}

void CTest::ScanMemoryForPatternWhenSignalled(const char* pattern)
{
	struct Inner
	{
		static UINT ThreadEntry(LPVOID param)
		{
			const char* pattern = (const char*)param;

			while (true)
			{
				CUtil::SetCurrentThreadName("Scanning thread");

				DEBUGF1("Waiting to scan...");

				static volatile bool change_me_in_the_debugger = false;

				while (!change_me_in_the_debugger)
				{
					Sleep(1);
				}

				ScanMemoryForPattern(pattern);
			}
		}
	};


	AfxBeginThread(Inner::ThreadEntry, (LPVOID)pattern, THREAD_PRIORITY_ABOVE_NORMAL);
}

bool AttemptMemCmp(const void* ptr1, const void* ptr2, size_t size, bool* match)
{
	__try
	{
		*match = memcmp(ptr1, ptr2, size) == 0;
	}
	__except (1)
	{
		return false;
	}

	return true;
}

void CTest::ScanMemoryForPattern(const char* pattern)
{
	DEBUGF1("Scanning virtual regions for pattern.");
	size_t patternLength = strlen(pattern);

	MEMORY_BASIC_INFORMATION buffer;
	char* ptr = (char*)0x00000000;

	int rangesScanned = 0;
	int rangesTotal = 0;
	int occurrencesFound = 0;

	static std::set<void*> s_previousFinds;

	std::set<void*> currentFinds;

	// Arbitrarily limit to 32-bit address space while testing
	for (rangesTotal = 0; ptr < (void*)(0xFFFFFFFF); rangesTotal++)
	{
		SIZE_T result = VirtualQuery(ptr, &buffer, sizeof(buffer));

		if (result == 0)
		{
			ERRORF("Unable to query page at %p", ptr);
			return;
		}

		const char* stateStr = "unkn";
		if (buffer.State == MEM_COMMIT)
			stateStr = "CMMT";
		else if (buffer.State == MEM_FREE)
			stateStr = "FREE";
		else if (buffer.State == MEM_RESERVE)
			stateStr = "RSRV";
		
		const char* typeStr = "unkn";
		if (buffer.Type == MEM_IMAGE)
			typeStr = "IMGE";
		else if (buffer.Type == MEM_MAPPED)
			typeStr = "MAPD";
		else if (buffer.Type == MEM_PRIVATE)
			typeStr = "PRIV";

		const char* protectLo = "???";
		if ((buffer.Protect & 0xFF) == PAGE_EXECUTE)
			protectLo = "--x";
		else if ((buffer.Protect & 0xFF) == PAGE_EXECUTE_READ)
			protectLo = "r-x";
		else if ((buffer.Protect & 0xFF) == PAGE_EXECUTE_READWRITE)
			protectLo = "rwx";
		else if ((buffer.Protect & 0xFF) == PAGE_EXECUTE_WRITECOPY)
			protectLo = "cwx";
		else if ((buffer.Protect & 0xFF) == PAGE_NOACCESS)
			protectLo = "---";
		else if ((buffer.Protect & 0xFF) == PAGE_READONLY)
			protectLo = "r--";
		else if ((buffer.Protect & 0xFF) == PAGE_READWRITE)
			protectLo = "rw-";
		else if ((buffer.Protect & 0xFF) == PAGE_WRITECOPY)
			protectLo = "cw-";

		char protectHi[4] = {'-','-','-','\0'};
		if ((buffer.Protect & 0xF00) == PAGE_GUARD)
			protectHi[0] = 'g';
		else if ((buffer.Protect & 0xF00) == PAGE_NOCACHE)
			protectHi[0] = 'n';
		else if ((buffer.Protect & 0xF00) == PAGE_WRITECOMBINE)
			protectHi[0] = 'w';

		bool shouldScan = false;

		if (buffer.State == MEM_COMMIT && buffer.Type == MEM_PRIVATE && ((buffer.Protect & PAGE_GUARD) == 0) && ((buffer.Protect & 0xF0) == 0) && ((buffer.Protect & 0xF) != PAGE_NOACCESS))
		{
			shouldScan = true;
		}

		if (shouldScan)
		{
			DEBUGF1("[%03x] Base: %p AllocBase: %p Size: %8x State/type: %s %s Protect: %s %s", rangesTotal, buffer.BaseAddress, buffer.AllocationBase, buffer.RegionSize, stateStr, typeStr, protectLo, protectHi);
			char* start = (char*)buffer.BaseAddress;
			char* end = (char*)buffer.BaseAddress + buffer.RegionSize - patternLength;

			for (start; start < end; start++)
			{
				bool match = false;
				if (!AttemptMemCmp(pattern, start, patternLength, &match))
				{
					DEBUGF1("Error reading mem.");
					break;
				}

				if (match)
				{
					DEBUGF1("Found pattern at: %p", start);
					currentFinds.insert(start);
					occurrencesFound++;
				}
			}

			rangesScanned++;
		}

		if (buffer.RegionSize > 0)
		{
			ptr += buffer.RegionSize;
		}
		else
		{
			ERRORF("Zero-length region.");
			ptr++;
		}

	}

	if (currentFinds != s_previousFinds)
	{
		DEBUGF1("%d of %d ranges scanned, %d occurrences of pattern found.", rangesScanned, rangesTotal, occurrencesFound);

		s_previousFinds = currentFinds;
	}
}

#endif
