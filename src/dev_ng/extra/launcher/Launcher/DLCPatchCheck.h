#pragma once

#include "IPatchCheck.h"
#include <string>

class DLCPatchCheck : public IPatchCheck
{
public:
	DLCPatchCheck(const std::string& rockstarId);
	~DLCPatchCheck();

	static void UrlEncode(const std::string& in, std::string& out);

	virtual void ParseXML();
};