#pragma once

#include <string>
#include <vector>

// Build types

#if !defined(EFIGS_LAUNCHER_BUILD)
#define EFIGS_LAUNCHER_BUILD 1
#endif

#if !defined(GERMAN_LAUNCHER_BUILD)
#define GERMAN_LAUNCHER_BUILD 0
#endif

#if !defined(RSG_STEAM_LAUNCHER)
#define RSG_STEAM_LAUNCHER 0
#endif

// Version names

#include "IPatchCheck.h"


// Class to ease command-line parameter declaration / definition

class CCommandlineArgument
{
public:
	const static std::vector<CCommandlineArgument*>& GetSupportedArgs();
	const static std::wstring& GetGameCommandline();
	static void ProcessCommandline();

	CCommandlineArgument(const wchar_t* name, bool defaultValue);
	CCommandlineArgument(const wchar_t* name, const wchar_t* inverseName, bool defaultValue);
	~CCommandlineArgument();

	bool operator=(bool bSet);
	operator bool() const;

	bool HandleCommandlineArg(const wchar_t* arg);
	bool WasSetFromCommandline() const;
	bool TryGetIntValue(std::wstring str, int* outValue);

	std::wstring& GetValue() { return m_value; }
	std::wstring& GetName() { return m_wszName; }
	int GetIntValue() { return m_IntValue; }

private:
	bool m_bSet;
	bool m_bSetFromCommandline;
	std::wstring m_wszName, m_wszInverseName;
	std::wstring m_value;
	int m_IntValue;

	static std::vector<CCommandlineArgument*> sm_supportedCommandlineArgs;
	static std::wstring sm_commandlineForGame;
};


// Constants

namespace Constants
{
#ifdef _DEBUG
	const bool DebugMode = true;
#else
	const bool DebugMode = false;
#endif

	const bool EnableLoadingConfigFromFile = true;
	const bool EnableLoadingConfigFromResources = true;
	const bool EnableSCUIv2ByDefault = true;

	const bool DevUsesInternalServer = true;
};


// Debug switches, which should all be off for release

namespace DebugSwitches
{
#ifdef _DEBUG
	const bool Enable = true;
#else
	const bool Enable = false;
#endif


#if !RSG_FINAL
	const bool DebugUseFrenchLanguage = Enable && false;
	const bool WriteDebugRegistry = Enable && false;
	const bool FakeDownloadingOnly = Enable && false;
	const bool DebugTestMissingPatch = Enable && false;
	const bool DebugTestMissingDependency = Enable && false;
	const bool ForceGameExitCode = Enable && false;
	const int ForceGameExitCodeValue = 42;
	const bool DebugTestRegex = Enable && false;
	const bool DontLaunchGame = Enable && false;
#endif
};


// Globals, some of which can be set on command-line

// DEFINE_GLOBALS is defined only in Globals.cpp
#ifdef DEFINE_GLOBALS

#define GLOBAL_DEFINITION(x,y) x = y
#define PARAM_DEFINITION(x) CCommandlineArgument x(L#x, false)
#define PARAM_DEFINITION_VA(x,...) CCommandlineArgument x(__VA_ARGS__)

std::vector<CCommandlineArgument*> CCommandlineArgument::sm_supportedCommandlineArgs;
std::wstring CCommandlineArgument::sm_commandlineForGame;

#else

#define GLOBAL_DEFINITION(x,y) extern x
#define PARAM_DEFINITION(x) extern CCommandlineArgument x
#define PARAM_DEFINITION_VA(x,...) extern CCommandlineArgument x

#endif

#if RSG_FINAL
#define NONFINAL_PARAM(x) GLOBAL_DEFINITION(bool x, false)
#define NONFINAL_PARAM_VA(x,...) GLOBAL_DEFINITION(bool x, false)
#else
#define NONFINAL_PARAM(x) PARAM_DEFINITION(x)
#define NONFINAL_PARAM_VA(x,...) PARAM_DEFINITION_VA(x, __VA_ARGS__)
#endif

namespace Globals
{
	GLOBAL_DEFINITION(int lastLaunchExitCode, 0);
	GLOBAL_DEFINITION(std::wstring alternativeTempPath, L"");
	GLOBAL_DEFINITION(bool needsLaunchConfirmation, false);

	GLOBAL_DEFINITION(const char* DevEnvName, "dev");
	GLOBAL_DEFINITION(const wchar_t* DevEnvNameW, L"dev");

	PARAM_DEFINITION(noAutoSignIn);
	PARAM_DEFINITION(safemode);
	PARAM_DEFINITION_VA(silent, L"silent", L"nosilent", false);
	PARAM_DEFINITION(scofflineonly);
	PARAM_DEFINITION(uac);
	PARAM_DEFINITION(releasedaterestart);
	PARAM_DEFINITION(generalrestart);
	PARAM_DEFINITION(env);
	PARAM_DEFINITION(patchenv);
	PARAM_DEFINITION(verify);

	PARAM_DEFINITION(irfnowait);

	PARAM_DEFINITION(noChunkedDownload);

	PARAM_DEFINITION(devPatchServer);

	PARAM_DEFINITION_VA(spcloudsave, L"spcloudsave", L"nospcloudsave", true);

	PARAM_DEFINITION(skipPregameCloudSync);
	PARAM_DEFINITION(skipPostgameCloudSync);

	PARAM_DEFINITION(restorecloudbackups);

	PARAM_DEFINITION(frombootstrap);

#if !RSG_FINAL

	NONFINAL_PARAM_VA(localAssetPath, L"localassetpath", L"defaultassetpath", true);
	NONFINAL_PARAM_VA(useScuiFrontend, L"useScuiFrontend", L"noScuiFrontend", true);
	NONFINAL_PARAM(final);

	NONFINAL_PARAM_VA(useDevInstallPath, L"devinstallpath", false);

	NONFINAL_PARAM(failEntitlement);
	NONFINAL_PARAM(failVoucher);
	NONFINAL_PARAM(passOfflineEntitlement);
	NONFINAL_PARAM(failOfflineEntitlement);

	NONFINAL_PARAM(testFullDisk);
	NONFINAL_PARAM(testFlow);

	NONFINAL_PARAM(autotest_bootstrap_nolauncher);
	NONFINAL_PARAM(autotest_launcher_exitimmediately);
	NONFINAL_PARAM(autotest_launcher_selfsignin);
	NONFINAL_PARAM(autotest_launcher_digitalupdate);
	NONFINAL_PARAM(autotest_launcher_gamepatch);
	NONFINAL_PARAM(autotest_launcher_errors);
	NONFINAL_PARAM(autotest_launcher_errors_delay);
	NONFINAL_PARAM(autotest_launcher_errors_respawn);
	NONFINAL_PARAM(noautokill);

	NONFINAL_PARAM(cloudsave_singleconflict);
	NONFINAL_PARAM(cloudsave_multipleconflicts);
	NONFINAL_PARAM(cloudsave_testdelete);


	NONFINAL_PARAM(alwaysEntitledToGame);
	NONFINAL_PARAM(alwaysEntitledToDLC);
	NONFINAL_PARAM(noSocialClub);

	NONFINAL_PARAM(processinstance);

	PARAM_DEFINITION(noDependencyCheck);
#ifdef RSG_NO_UPDATE
	NONFINAL_PARAM_VA(noUpdateCheck, L"noupdatecheck", L"forceupdatecheck", true);
#else
	NONFINAL_PARAM_VA(noUpdateCheck, L"noupdatecheck", L"forceupdatecheck", false);
#endif
	NONFINAL_PARAM_VA(noDlcCheck, L"noDlcCheck", L"forceDlcCheck", true);
	NONFINAL_PARAM_VA(noDIP, L"noDIP", L"forceDIP", false);

	NONFINAL_PARAM(beta);
	NONFINAL_PARAM(bankrelease);
	NONFINAL_PARAM(release);
	NONFINAL_PARAM(afrfriendlyd3d);

#endif // FINAL
};



// Clean up these macros

#undef GLOBAL_DEFINITION
#undef PARAM_DEFINITION
#undef PARAM_DEFINITION_VA
#undef NONFINAL_PARAM
#undef NONFINAL_PARAM_VA
