#pragma once

#include "Windows.h"
#include <string>
#include <vector>

#include "rgsc_common.h"
#include "rgsc_interface.h"

#define LAUNCHER_CHALLENGE_RESPONSE RSG_CPU_X64 && RSG_LAUNCHER && !RSG_PRELOADER_LAUNCHER

#if LAUNCHER_CHALLENGE_RESPONSE
// Diffie Helman Headers
#include "TFIT_ECC_DH_iECDHgreenSSfastP256.h"
#include "TFIT_ECC_DH_iECDHredKgreenSS.h"
// AES CTR Headers
#include "TFIT_DK_AES_CBC_Encrypt_iAES13_green.h"	
#include "TFIT_DK_AES_CBC_Decrypt_iAES14_green.h"
#include "TFIT_AES_CBC_Encrypt_iAES13.h"
#include "TFIT_AES_CBC_Decrypt_iAES14.h"	
// HMAC Headers
#include "TFIT_SHA_HMAC_iHMACredK.h"
#include "wrapper_modes.h"
#include <openssl/rand.h>

#define CURVE_BUFFER_SIZE 64
#define CURVE_BUFFER_STRING_SIZE ((CURVE_BUFFER_SIZE *2) + 1)
#define SHARED_SECRET_SIZE 32
#define GENERIC_BUFFER_SIZE 256
#define CHALLENGE_SIZE 32
#define CHALLENGE_STRING_SIZE ((CHALLENGE_SIZE *2) + 1)
#define HMAC_SIZE 32
#define HMAC_STRING_SIZE ((HMAC_SIZE *2) + 1)
#define IV_SIZE 16
#define IV_STRING_SIZE ((IV_SIZE *2) + 1)

#endif


#define PIPE_BUFFER_SIZE 4096
#define MSG_HEADER_SIZE 4
class CGameCommunication
{
public:
	CGameCommunication();
	~CGameCommunication();

	static CGameCommunication& Instance();

	void Listen();
	void StopListening();
	bool IsListening();

	void WriteMessage(const char* message);

	void SendCompleteMessage(const std::string& sku);
	void SendProgressMessage(const std::string& sku, int progressPercent);
	void SendErrorMessage(const std::string& sku, const std::string& errorCode);
	void SendAliveMessage();
	void SendUACMessage();
	void InitializeChallengeResponse();
	void SendTicketUpdate(rgsc::RockstarId rockstarId, const char* scTicketMsg);
#if LAUNCHER_CHALLENGE_RESPONSE
	void SendLauncherKey();
	void CalculateSharedSecret(const char * ptr);
	void CalculateAndSendHMAC(const char *ptr);
	void Decrypt(unsigned char *dst, const unsigned char *src);
	void Encrypt(unsigned char *dst, const unsigned char *src);
#endif

private:

	class CConnectedPipe
	{
	public:
		CConnectedPipe();
		~CConnectedPipe();
		bool Connect();
		void Shutdown();
		void Listen();
		void WriteRaw(const char* data, u32 size);
		void SetThread(HANDLE hThread);
		HANDLE GetThread();

	private:
		void ParseMessages(const char* buffer, u32 bytes);
		void HandleMessage(const char* messageBuffer, u32 bufferSize);
		HANDLE pipe;
		HANDLE event;
		OVERLAPPED overlapped;
		bool m_bPipeConnected;
		HANDLE m_hThread;
	};


	std::vector<CConnectedPipe*> pipes;

	CRITICAL_SECTION critsec;

	bool Start();

	void AddPipe(CConnectedPipe* pipe);
	void RemovePipe(CConnectedPipe* pipe);

	// Base64 Encoding
	static bool Base64Encode(const u8* src, const unsigned srcLen, char* dst, const unsigned dstMaxSize, unsigned* charsUsed);
	static unsigned GetMaxBase64EncodedSize(const unsigned size);
	static const int CGameCommunication::BASE64_SHIFT[4];
	static const char CGameCommunication::BASE64_ALPHABET[65];

	CWinThread* m_thread;
	CConnectedPipe* m_pipeWaitingToConnect;
	bool m_shuttingDown;

#if LAUNCHER_CHALLENGE_RESPONSE
	void hexArrayToString(char *dst, unsigned char *src, unsigned int len);
	void hexStringToArray(unsigned char * dst, const char *src, unsigned int len);
#if !RSG_FINAL
	void printPointer(char * indicator, const unsigned char * ptr, unsigned int len);
#endif
	wbecc_dh_ctx_t m_launcherPipeEccCtx;
	uint8_t m_launcherPipeEccKey[CURVE_BUFFER_SIZE];
	unsigned int m_launcherPipeEccKeyLength;

	TFIT_key_iAES13_t		m_launcherPipeEncryptionKey;
	TFIT_key_iAES14_t		m_launcherPipeDecryptionKey;
	block_t					m_decryptionIV;
	block_t					m_encryptionIV;

#endif
};
