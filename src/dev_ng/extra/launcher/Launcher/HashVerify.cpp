#include "stdafx.h"

#include "HashVerify.h"
#include "Util.h"
#include <WinCrypt.h>
#include <fstream>
#include "TamperSource.h"
#include <stdio.h>
#include <string.h>
#include <openssl/sha.h>
#if _DEBUG
	#pragma comment(lib, "libeay32MTd.lib")
	#pragma comment(lib, "ssleay32MTd.lib")
#else
	#pragma comment(lib, "libeay32MT.lib")
	#pragma comment(lib, "ssleay32MT.lib")
#endif
bool HashVerify::Verify(const wchar_t* filepath, const std::string& hash)
{

	//@@: range HASHVERIFY_VERIFY_CHECK_SIZE {
	//@@: location HASHVERIFY_VERIFY_CHECK_SIZE_LOCATION
	if (hash.size() == 0 || ((hash.size() % 2) != 0))
	{
		DEBUGF1("Invalid hexadecimal hash length: %d digits.", hash.size());
		return false;
	}
	//@@: } HASHVERIFY_VERIFY_CHECK_SIZE

	size_t incomingHashBytes = hash.size() / 2;
	u8* incomingHashBinary = new u8[incomingHashBytes];
	for (size_t i = 0; i < incomingHashBytes; i++)
	{
		u8 val = 0;
		char c1 = hash.at(i*2), c2 = hash.at(i*2+1);
		if (HexCharsToByte(c1, c2, &val))
		{
			incomingHashBinary[i] = val;
		}
		else
		{
			ERRORF("Invalid hex string: %s", hash.c_str());
			delete[] incomingHashBinary;
			return false;
		}
	}
	bool errorHashing = false;

	//@@: location HASHVERIFY_VERIFY_CREATE_HASH
	SHA_CTX ctx;
	SHA1_Init(&ctx);
	u8 opensslHash[SHA_DIGEST_LENGTH];
	std::ifstream filestream(filepath);
	

	if (filestream.good())
	{
		while (!filestream.eof())
		{
			char buffer[1024];
#if RSG_PRELOADER_LAUNCHER || !ENABLE_TAMPER_ACTIONS
			filestream.read(buffer, sizeof(buffer));
#else
			filestream.read(buffer, sizeof(buffer) - g_readDiff);
#endif
			std::streamsize read = filestream.gcount();
			if (read > 0)
			{
				SHA1_Update(&ctx, buffer, read);
			}
		}
		SHA1_Final(opensslHash, &ctx);
	}
	else
	{
		ERRORF("Filestream error.");
		errorHashing = true;
	}

	DWORD hashLength = 0;
	
	//@@: range HASHVERIFY_VERIFY_CHECK_HASHES {
	{
		u8 * hashBuffer = opensslHash;

		//@@: location HASHVERIFY_VERIFY_CHECK_HASH_LENGTH_B_LOCATION
		if (memcmp(hashBuffer, incomingHashBinary, hashLength) == 0)
		{
			DEBUGF1("Hashes match.");
		}
		else
		{
			DEBUGF1("Hashes don't match! ");
			errorHashing = true;
		}
		delete []hashBuffer;
	}
	//@@: } HASHVERIFY_VERIFY_CHECK_HASHES

	delete[] incomingHashBinary;

	return !errorHashing;
}

bool HashVerify::HexCharsToByte(char c1, char c2, u8* out)
{
	u8 val = 0;

	if (c1 >= '0' && c1 <= '9')
		val += c1 - '0';
	else if (c1 >= 'a' && c1 <= 'f')
		val += c1 - 'a' + 10;
	else if (c1 >= 'A' && c1 <= 'F')
		val += c1 - 'A' + 10;
	else
		return false;

	val <<= 4;

	if (c2 >= '0' && c2 <= '9')
		val += c2 - '0';
	else if (c2 >= 'a' && c2 <= 'f')
		val += c2 - 'a' + 10;
	else if (c2 >= 'A' && c2 <= 'F')
		val += c2 - 'A' + 10;
	else
		return false;

	*out = val;

	return true;
}
