#pragma once

#include <d3d9.h>

#include "DxSurface.h"

class DxSurface9 : public DxSurface
{
	DECLARE_DYNAMIC(DxSurface9)

public:
	DxSurface9();
	~DxSurface9();

	
	void DrawTest();
	void NullDraw();

	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	virtual HRESULT CreateDevice();
	virtual void DestroyDevice();
	virtual IUnknown* GetDxContext();
	virtual void* GetPresentParams();
	virtual bool Show(bool show);
	virtual void Render();


private:
	LPDIRECT3D9 m_direct3D9;
	static LPDIRECT3DDEVICE9 m_device;
	D3DPRESENT_PARAMETERS m_d3dpp;
	D3DDEVICE_CREATION_PARAMETERS m_creationParams;
	bool m_painting;
	bool m_bShown;
	bool m_bDisabledStereo;

	enum DelegateState
	{
		READY,
		DEVICELOST,
	};

	DelegateState m_delegateState;

protected:
	afx_msg void OnPaint();
	BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	DECLARE_MESSAGE_MAP()
};