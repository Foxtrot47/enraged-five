#pragma once

typedef signed   __int8  s8;
typedef unsigned __int8  u8;

typedef signed   __int16 s16;
typedef unsigned __int16 u16;

typedef signed   __int32 s32;
typedef unsigned __int32 u32;

typedef signed   __int64 s64;
typedef unsigned __int64 u64;
