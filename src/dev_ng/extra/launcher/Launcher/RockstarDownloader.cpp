#include "StdAfx.h"
#include "RockstarDownloader.h"
#include "SimpleDownloader.h"
#include "DownloadManager.h"
#include "WhiteList.h"

#include "Globals.h"
#include "Config.h"
#include "OnlineConfig.h"
#include "RgscDebugTelemetry.h"

#include "resource.h"

#include "Util.h"
#include "Channel.h"

#include <ctype.h>
#include <sstream>
#include "seh.h"

#include <iostream>
#include <fstream>
#include <math.h>

#include "TamperSource.h"
#include <WinInet.h>
#pragma comment(lib,"Wininet.lib")

#include "ProgressCalculator.h"
#include "RgscTelemetryManager.h"


RockstarDownloader::RockstarDownloader(void)
{
	//@@: location ROCKSTARDOWNLOADER_CONSTRUCTOR
	Init();

}

RockstarDownloader::~RockstarDownloader(void)
{
	m_listener = NULL;
}

void RockstarDownloader::Init()
{
	//@@: location ROCKSTARDOWNLOADER_INIT
	m_bytesRead = m_totalBytes = 0;
	m_downloading = false;
	m_paused = false;
	m_hOpen = NULL;
	m_hConnect = NULL;
	m_hRequest = NULL;
	m_hOpenUrl = NULL;
	m_hostURL[0] = _T('\0');
	m_fileURL[0] = _T('\0');
	m_progress = 0;
	m_logProgress = 0;
	m_bUsesSSL = false;
	m_timingLastMillisecond = CUtil::CurrentTimeMillis();
	m_timingDataSize = 0;
	m_timingDataPerSecond = 0;
	m_timeoutMillis = OnlineConfig::DownloadTimeoutInitialSeconds * 1000;
	m_maxRetries = OnlineConfig::PatchRetries;
}

void RockstarDownloader::StartSynchronously()
{
	m_error.Reset();

	if (m_downloading)
	{
		ERRORF("RockstarDownloader already downloading!");
		return;
	}
	else
	{
		DEBUGF1("RockstarDownloader starting.");
	}

	m_paused = false;
	m_bytesRead = m_totalBytes = 0;
	m_downloading = true;

	__try
	{
		//@@: range ROCKSTARDOWNLOADER_STARTSYNCHRONOUSLY_START_DOWNLOAD {
		DEBUGF1("RockstarDownloader doing download.");
		//@@: location ROCKSTARDOWNLOADER_DODOWNLOAD_CALL
		DoDownload();
		//@@: location ROCKSTARDOWNLOADER_DODOWNLOAD_CALL_EXIT
		DEBUGF1("RockstarDownloader ending download.");
		//@@: } ROCKSTARDOWNLOADER_STARTSYNCHRONOUSLY_START_DOWNLOAD

	}
	__except(1)
	{
		// Necessary due to SEH rules.
		struct Closure
		{
			static void errorOccured(RockstarDownloader* down)
			{
				// TODO: Display more error info
				//@@: location ROCKSTARDOWNLOADER_STARTSYNCHRONOUSLY_ERROR_OCCURED
				MessageBox(NULL, TEXT("R* Downloader Thread Failure"), TEXT("R* Downloader Thread Failure"), MB_OK);
				down->ThrowError(CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_UNKNOWN, "ErrorCatastrophic"));
			}
		};

		Closure::errorOccured(this);
	}
}

void RockstarDownloader::SetPaused(bool paused)
{
	//@@: location ROCKSTARDOWNLOADER_SETPAUSED
	m_paused = paused;
}

void RockstarDownloader::Stop()
{
	m_downloading = false;
}

void RockstarDownloader::SetCurrentFile(std::string downloadUrl, std::string localFilePath, bool backgroundDownload, bool alwaysdownload)
{
	//@@: location ROCKSTARDOWNLOADER_SETCURRENTFILE
	CurrentPatch.BackgroundDownload = backgroundDownload;
	CurrentPatch.LocalFilename = localFilePath;
	CurrentPatch.PatchURL = downloadUrl;
	CurrentPatch.AlwaysDownload = alwaysdownload;
}

void RockstarDownloader::SetCurrentPatch(Patch& patch)
{
	//@@: location ROCKSTARDOWNLOADER_SETCURRENTPATCH
	CurrentPatch = patch;
}

void RockstarDownloader::ThrowError(const CError& error)
{
	// Ensure the first (innermost) error is the one reported
	if (!m_error.IsError())
	{
		m_error = error;
		CError::SetError(error);
	}
}

bool RockstarDownloader::GetServerTime(const std::string& url, FILETIME& out_filetime)
{
	RockstarDownloader down;
	s64 time = down.GetUrlInfoLong(url, URL_INFO_TYPE_SERVER_TIME);
	if (time != 0)
	{
		out_filetime = *(FILETIME*)&time;
		return true;
	}
	else
	{
		return false;
	}
}

bool RockstarDownloader::CanAccessUrl(const std::string& url)
{
	RockstarDownloader down;

	//@@: range ROCKSTARDOWNLOADER_CANACCESSURL {

	//@@: location ROCKSTARDOWNLOADER_CANACCESSURL_GETURLINFOLONG_CALL
	s64 statusCode = down.GetUrlInfoLong(url, URL_INFO_TYPE_STATUS_CODE);

	down.CloseAllHandles();
	//@@: location ROCKSTARDOWNLOADER_CANACCESSURL_CHECK_STATUS
	if (statusCode == 0)
	{
		ERRORF("Failed to connect to URL.");
		return false;
	}
	else if (statusCode >= 100 && statusCode <= 299)
	{
		DEBUGF1("Test URL accessible.");
		return true;
	}
	else if (statusCode >= 300 && statusCode <= 399)
	{
		WARNINGF("Test URL is a redirect..?");
		return true;
	}
	else if (statusCode == 401)
	{
		DEBUGF1("Unauthorized to access test URL.");
		return false;
	}
	else if (statusCode == 403)
	{
		DEBUGF1("Forbidden to access test URL.");
		return false;
	}
	else if (statusCode == 404)
	{
		ERRORF("Test file is missing!");
		return false;
	}
	else if (statusCode >= 400 && statusCode <= 499)
	{
		ERRORF("Client error when accessing test URL.");
		return false;
	}
	else if (statusCode >= 500 && statusCode <= 599)
	{
		ERRORF("Server error when accessing test URL.");
		return false;
	}
	else
	{
		ERRORF("Invalid status code (%d) when accessing test URL.", statusCode);
		return false;
	}
	//@@: } ROCKSTARDOWNLOADER_CANACCESSURL

}

bool RockstarDownloader::CheckExeHash(std::string filename)
{
	DEBUGF2("Skipping hash check of %s", filename.c_str());
	return true;
}

typedef struct{
	HWND       hWindow;      // Window handle
	int        nStatusList;  // List box control to hold callbacks
	HINTERNET  hResource;    // HINTERNET handle created by InternetOpenUrl
	char       szMemo[512];  // String to store status memo
} REQUEST_CONTEXT;

void CALLBACK RockstarDownloader::OnStatusCallback(HINTERNET /*hInternet*/, DWORD_PTR /*dwContext*/, DWORD /*dwInternetStatus*/,
											LPVOID /*lpvStatusInformation*/, DWORD /*dwStatusInformationLength*/)
{
}

s64 RockstarDownloader::GetUrlInfoLong(const std::string& url, const UrlInfoType infoType)
{
	RAD_TELEMETRY_ZONE("GetUrlInfoLong");

	//@@: range ROCKSTARDOWNLOADER_GETURLINFOLONG_GETSERVER {

	//@@: location ROCKSTARDOWNLOADER_GETURLINFOLONG_CHECK_LENGTH
	if (infoType == URL_INFO_TYPE_CONTENT_LENGTH)
	{
		DEBUGF1("Hitting %s to determine length...", url.c_str());
	}
	else if (infoType == URL_INFO_TYPE_STATUS_CODE)
	{
		DEBUGF1("Hitting %s to determine status code...", url.c_str());
	}
	else if (infoType == URL_INFO_TYPE_SERVER_TIME)
	{
		DEBUGF1("Hitting %s to determine time...", url.c_str());
	}

	
	// Check for SSL / TLS
	std::string httpsPrefix("https");
	//@@: location ROCKSTARDOWNLOADER_GETURLINFOLONG_COMPARE_PREFIX
	if (url.compare(0, httpsPrefix.size(), httpsPrefix) == 0)
		m_bUsesSSL = true;

	// Get hostname and document name parts of URL, as wide-character strings
	char *temp;
	//@@: location ROCKSTARDOWNLOADER_GETURLINFOLONG_GETSERVER_CALL
	if (!RockstarDownloader::GetServer(url.c_str(), &temp, true))
	{
		ERRORF("Unable to get hostname.");
		return 0;
	}

	MultiByteToWideChar(CP_UTF8, 0, temp, -1, m_hostURL, MAX_PATH);
	delete[] temp;

	if (!CSimpleDownloader::GetPath(url.c_str(), &temp))
	{
		ERRORF("Unable to get path.");
		return 0;
	}

	MultiByteToWideChar(CP_UTF8, 0, temp, -1, m_fileURL, MAX_PATH);
	delete[] temp;
	//@@: } ROCKSTARDOWNLOADER_GETURLINFOLONG_GETSERVER



	// Get requested info
	s64 infoValue = 0;
	rtry
	{
		// Initialize WinInet
		RAD_TELEMETRY_ENTER("Init");
		std::wstring userAgent;
		CUtil::StdStringToStdWString(Constants::LauncherExeNameWithoutExtension, userAgent);
		//@@: range ROCKSTARDOWNLOADER_GETURLINFOLONG {
		m_hOpen = InternetOpen(userAgent.c_str(), INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
		rverify(m_hOpen,catchall,ERRORF("Open failed."));

		if (!InternetSetOption(m_hOpen, INTERNET_OPTION_RECEIVE_TIMEOUT, &m_timeoutMillis, sizeof(m_timeoutMillis)))
		{
			ERRORF("Unable to set timeout on m_hOpen");
		}

		// Set global callback
		INTERNET_STATUS_CALLBACK hOpenStatus = InternetSetStatusCallback(m_hOpen, OnStatusCallback);
		rverify(hOpenStatus != INTERNET_INVALID_STATUS_CALLBACK, catchall, ERRORF("Setting callback failed."));

		// Create connection
		//@@: location ROCKSTARDOWNLOADER_GETURLINFOLONG_CREATE_CXN
		INTERNET_PORT port = m_bUsesSSL ? INTERNET_DEFAULT_HTTPS_PORT : INTERNET_DEFAULT_HTTP_PORT;
		m_hConnect = InternetConnect(m_hOpen, m_hostURL, port, NULL, NULL, INTERNET_SERVICE_HTTP, 0, reinterpret_cast<DWORD_PTR>(this));
		rverify(m_hConnect,catchall,ERRORF("Connect failed."));

		if (!InternetSetOption(m_hConnect, INTERNET_OPTION_RECEIVE_TIMEOUT, &m_timeoutMillis, sizeof(m_timeoutMillis)))
		{
			ERRORF("Unable to set timeout on m_hConnect");
		}
				
		// Specify request flags
		DWORD dwFlags = INTERNET_FLAG_RELOAD | INTERNET_FLAG_DONT_CACHE | INTERNET_FLAG_PRAGMA_NOCACHE | INTERNET_FLAG_KEEP_CONNECTION;
		if (m_bUsesSSL)
		{
			dwFlags	|= (INTERNET_FLAG_SECURE | INTERNET_FLAG_IGNORE_CERT_CN_INVALID | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID);
		}

		// Specify accept types
		const TCHAR* lplpszAcceptTypes[] = {L"*/*", NULL};

		// Open request
		m_hRequest = HttpOpenRequest(m_hConnect, _T("GET"), m_fileURL, HTTP_VERSION, NULL, lplpszAcceptTypes, dwFlags, 0);
		rverify(m_hRequest, catchall, ERRORF("Opening request failed."));

		if (!InternetSetOption(m_hRequest, INTERNET_OPTION_RECEIVE_TIMEOUT, &m_timeoutMillis, sizeof(m_timeoutMillis)))
		{
			ERRORF("Unable to set timeout on m_hRequest");
		}

		RAD_TELEMETRY_LEAVE();
		RAD_TELEMETRY_ENTER("Send");

		// Send request
		BOOL bResult = HttpSendRequest(m_hRequest, NULL, 0, NULL, 0);
		rverify(bResult, catchall, ERRORF("Sending request failed."));

		RAD_TELEMETRY_LEAVE();
		RAD_TELEMETRY_ENTER("Query");

		// Receieve info
		TCHAR bufQuery[128] ;
		DWORD dwLengthBufQuery = sizeof (bufQuery);
		BOOL bQuery;
		
		if (infoType == URL_INFO_TYPE_CONTENT_LENGTH)
			bQuery = HttpQueryInfo(m_hRequest, HTTP_QUERY_CONTENT_LENGTH, bufQuery, &dwLengthBufQuery, 0);
		else if (infoType == URL_INFO_TYPE_STATUS_CODE)
			bQuery = HttpQueryInfo(m_hRequest, HTTP_QUERY_STATUS_CODE, bufQuery, &dwLengthBufQuery, 0);
		else if (infoType == URL_INFO_TYPE_SERVER_TIME)
			bQuery = HttpQueryInfo(m_hRequest, HTTP_QUERY_DATE | HTTP_QUERY_FLAG_SYSTEMTIME, bufQuery, &dwLengthBufQuery, 0);
		else
			bQuery = false;

		//@@: } ROCKSTARDOWNLOADER_GETURLINFOLONG
		rverify(bQuery, catchall, ERRORF("Query failed."));

		if (infoType == URL_INFO_TYPE_CONTENT_LENGTH || infoType == URL_INFO_TYPE_STATUS_CODE)
		{
			infoValue = _tstoi64(bufQuery);
		}
		else
		{
			SYSTEMTIME* time = (SYSTEMTIME*)bufQuery;
			FILETIME ft;
			SystemTimeToFileTime(time, &ft);
			memcpy(&infoValue, &ft, sizeof(ft));
		}

		// Tidy up
		InternetCloseHandle(m_hRequest);
	}
	rcatchall
	{
		WARNINGF("Catch-all error.");
		PrintInternetError();
		//@@: range ROCKSTARDOWNLOADER_GETURLINFOLONG_ERROR {
		ERRORF("Error occurred attempting to obtain info.");
		//@@: location ROCKSTARDOWNLOADER_GETURLINFOLONG_ERROR_THROW_ERROR
		ThrowError(CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_NO_CONNECTION));
		CloseAllHandles();
		//@@: } ROCKSTARDOWNLOADER_GETURLINFOLONG_ERROR

		RAD_TELEMETRY_LEAVE();
		return 0;
	}

	RAD_TELEMETRY_LEAVE();

	
	if (infoType == URL_INFO_TYPE_CONTENT_LENGTH)
	{
		DEBUGF2("Content length: %llu", infoValue);
	}
	else if (infoType == URL_INFO_TYPE_STATUS_CODE)
	{
		DEBUGF2("Status code: %ld", infoValue);
	}
	//@@: location ROCKSTARDOWNLOADER_GETURLINFOLONG_EXIT_POINT
	return infoValue;
}

void RockstarDownloader::IncrementTimeout()
{
	if (m_timeoutMillis < (ULONG)OnlineConfig::DownloadTimeoutMaximumSeconds * 1000)
	{
		m_timeoutMillis = std::min<ULONG>(m_timeoutMillis + (ULONG)OnlineConfig::DownloadTimeoutIncreaseSeconds * 1000, (ULONG)OnlineConfig::DownloadTimeoutMaximumSeconds * 1000);

		DISPLAYF("Increasing timeout value to %d milliseconds.", m_timeoutMillis);
	}
}

void RockstarDownloader::CloseAllHandles()
{
	//@@: range ROCKSTARDOWNLOADER_CLOSEALLHANDLES {
	InternetCloseHandle(m_hOpen);
	InternetCloseHandle(m_hConnect);
	InternetCloseHandle(m_hRequest);
	InternetCloseHandle(m_hOpenUrl);
	//@@: } ROCKSTARDOWNLOADER_CLOSEALLHANDLES

	//@@: location ROCKSTARDOWNLOADER_CLOSEALLHANDLES_RETURN
	return;

}

bool RockstarDownloader::CheckForDiskSpace(Patch& patch)
{
	if (!CheckForDiskSpace(patch.LocalFilename, patch.ContentLength))
	{
		ThrowError(CError::GetLastError());
		return false;
	}
	return true;
}

bool RockstarDownloader::CheckForDiskSpace(const std::string& const_path, u64 required)
{
	const char* cpath = NULL;
	std::string path = const_path;

	size_t slash = path.find_first_of("\\/");
	if (slash != std::string::npos)
	{
		path.erase(slash, std::string::npos);
		cpath = path.c_str();
	}

	ULARGE_INTEGER freeBytesAvailable;

	//@@: location ROCKSTARDOWNLOADER_CHECKFORDISKSPACE
	u64 requiredExtraSpace = required;
	if (CFileWrapper::GetDiskFreeSpaceExA(cpath, &freeBytesAvailable, NULL, NULL) && !AUTOTEST_FAIL)
	{

#if !RSG_FINAL
		if (Globals::testFullDisk || AUTOTEST_FAIL)
		{
			static std::wstring s_temp;
			std::wstring temp;
			CUtil::PrependTempoaryFolderPath(temp, L"");

			if (s_temp.empty())
			{
				s_temp = temp;
				freeBytesAvailable.QuadPart = 0;
			}
			else if (s_temp == temp)
			{
				freeBytesAvailable.QuadPart = 0;
			}
		}
#endif

		if (freeBytesAvailable.QuadPart > required)
		{
			DEBUGF1("Disk %s has %llu bytes available; we only need %llu.", cpath, freeBytesAvailable.QuadPart, required);
			return true;
		}
		else
		{
			ERRORF("Disk %s only has %llu bytes available; we need %llu!", cpath, freeBytesAvailable.QuadPart, required);
			requiredExtraSpace = required - freeBytesAvailable.QuadPart;
		}
	}
	else
	{
		ERRORF("Can't get free disk space on %s!", cpath);
	}


	std::string bytesRequiredHumanReadable;
	CUtil::AppendHumanReadableDataSize(bytesRequiredHumanReadable, requiredExtraSpace);

	CError::SetError(CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_NO_DISK_SPACE, bytesRequiredHumanReadable));
	return false;
}

#define VERIFY_STATUS(status, ...) rverify(status == DL_STATUS_OK || status == DL_STATUS_COMPLETE,catchall,__VA_ARGS__);
void RockstarDownloader::DoDownload()
{
	RAD_TELEMETRY_ZONE("DoDownload");
	DownloadCompleteStatus status = DL_STATUS_OK;
	DWORD retrySleepMillis = 1000;

	int attempts = 0;
	do
	{
		if (attempts > 0 && status != DL_STATUS_SLOW_RETRY)
		{
#if RSG_LAUNCHER
			AUTOTEST_RESULT(CTest::RETRIED);
			RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::RD_RETRY);
#endif
			DEBUGF3("Waiting %lu millis before retry...", retrySleepMillis);

			// Don't sleep for long durations without checking if we've been cancelled / paused
			const int sleepInterval = 500;
			for (int i = 0; i < retrySleepMillis && m_downloading && !m_paused; i += sleepInterval)
			{
				UpdateListener(DL_STATUS_RETRY);
				Sleep(sleepInterval);
			}

			if (retrySleepMillis < 30 * 1000)
			{
				retrySleepMillis *= 2;
			}

			// Reset error condition each attempt
			m_error.Reset();
		}

		// Check if URL is valid
		rtry
		{
			//@@: range ROCKSTARDOWNLOADER_DODOWNLOAD_LOOP {
			DEBUGF3("RockstarDownloader checking URL validity...");	
			//@@: location ROCKSTARDOWNLOADER_DODOWNLOAD_CALL_ISURLVALID


			if (m_listener)
			{
				m_listener->DownloadStarting(CurrentPatch);
			}

			status = DoDownload_IsUrlValid(CurrentPatch);
			VERIFY_STATUS(status, CError::SetError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_BAD_URL));

			DEBUGF3("RockstarDownloader checking disk...");
			if ((status = DoDownload_CheckDisk(CurrentPatch)) != DL_STATUS_OK)
			{
				UpdateListener(status);
			}

			do
			{
			
				if (status == DL_STATUS_OK || status == DL_STATUS_COMPLETE || status == DL_STATUS_PAUSE)
				{
					DEBUGF3("RockstarDownloader receiving data...");

					status = DoDownload_ReceiveData(CurrentPatch);
				}

				DEBUGF3("RockstarDownloader closing handles...");
				CloseAllHandles();

				if (status == DL_STATUS_PAUSE)
				{
					RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::RD_PAUSED);
					DEBUGF3("RockstarDownloader updating listener...");
					UpdateListener(status);

					while (m_paused && m_downloading)
					{
						Sleep(100);
					}
				}

			}
			while (status == DL_STATUS_PAUSE && m_downloading);

			
			if (status == DL_STATUS_ERROR)
			{
				RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::RD_ERROR_1);
				DEBUGF3("RockstarDownloader incrementing timeout...");
				IncrementTimeout();
			}

			//@@: location ROCKSTARDOWNLOADER_DODOWNLOAD_CHECK_STATUS_ERROR
			if (status != DL_STATUS_ERROR)
			{
				DEBUGF3("RockstarDownloader updating listener...");
				UpdateListener(status);
			}
			//@@: } ROCKSTARDOWNLOADER_DODOWNLOAD_LOOP

		}
		rcatchall
		{
			DEBUGF3("RockstarDownloader catchall - updating listener...");
			RgscDebugTelemetry::AddDebugEvent(RgscDebugTelemetry::RD_ERROR_CATCHALL);
			if (status == DL_STATUS_OK)
				status = DL_STATUS_ERROR;
			UpdateListener(status);
		}
	
		attempts++;
	}
	while (status == DL_STATUS_ERROR && (attempts < m_maxRetries || m_maxRetries == Downloader::INFINITE_RETRIES) && m_downloading);

	if (status == DL_STATUS_ERROR)
	{
		DEBUGF3("RockstarDownloader updating listener...");
		UpdateListener(status);
	}

	m_downloading = false;
	m_paused = false;

	//@@: location ROCKSTARDOWNLOADER_DODOWNLOAD_EXIT
	DEBUGF3("RockstarDownloader leaving download...");
}

void RockstarDownloader::UpdateListener(DownloadCompleteStatus status)
{
	if (m_listener)
	{
		if (status != DL_STATUS_COMPLETE || AUTOTEST_FAIL)
		{
			switch (status)
			{
				//@@: range ROCKSTARDOWNLOADER_UPDATELISTENER_UPDATE {
			case DL_STATUS_ERROR:
				if (m_error.IsError())
				{
					m_listener->DownloadError(CurrentPatch, m_error);
				}
				else
				{
					ThrowError(CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_UNKNOWN));
					m_listener->DownloadError(CurrentPatch, m_error);
				}
				break;
			case DL_STATUS_OK:
				ERRORF("Shouldn't get DL_STATUS_OK here.");
				m_listener->DownloadError(CurrentPatch, CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_UNKNOWN));
				break;
			case DL_STATUS_PAUSE:
				DISPLAYF("Received DL_STATUS_PAUSE.");
				m_listener->DownloadStatus(CurrentPatch, status, m_bytesRead, m_totalBytes, 0);
				break;
			case DL_STATUS_VERIFYING:
				DEBUGF1("Received DL_STATUS_VERIFYING.");
				m_listener->DownloadStatus(CurrentPatch, status, m_bytesRead, m_totalBytes, 0);
				break;
			case DL_STATUS_SLOW_RETRY:
				DEBUGF1("Received DL_STATUS_SLOW_RETRY.");
				m_listener->DownloadStatus(CurrentPatch, status, m_bytesRead, m_totalBytes, 0);
				break;
			case DL_STATUS_CANCEL:
				DISPLAYF("Received DL_STATUS_CANCEL.");
				break;
			case DL_STATUS_RETRY:
				m_listener->DownloadStatus(CurrentPatch, status, m_bytesRead, CurrentPatch.FileSize, 0);
				break;
			default:
				m_listener->DownloadError(CurrentPatch, CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_UNKNOWN));
				break;
				//@@: } ROCKSTARDOWNLOADER_UPDATELISTENER_UPDATE

			}
		}
		else
		{
			//@@: range ROCKSTARDOWNLOADER_UPDATELISTENER_SET_COMPLETE {
			DEBUGF2("Download complete.");
			m_listener->DownloadComplete(CurrentPatch, status);
			//@@: } ROCKSTARDOWNLOADER_UPDATELISTENER_SET_COMPLETE

		}
	}
}
DownloadCompleteStatus RockstarDownloader::DoDownload_IsUrlValid(Patch& patch)
{
	
	//@@: location ROCKSTARDOWNLOADER_DODOWNLOAD_ISURLVALID
	if (!WhiteList::Exists(patch.PatchURL) || AUTOTEST_FAIL)
	{
		ThrowError(CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_BAD_URL));
		return DL_STATUS_ERROR;
	}
	else
	{
		return DL_STATUS_OK;
	}
}

DownloadCompleteStatus RockstarDownloader::DoDownload_CheckDisk(Patch& patch)
{
	RAD_TELEMETRY_ZONE("DoDownload_CheckDisk");

	if (m_listener)
		m_listener->DownloadStatus(CurrentPatch, DL_STATUS_OK, 0, patch.FileSize, 0);
	
	//@@: location ROCKSTARDOWNLOADER_DODOWNLOAD_CHECKDISK
	m_totalBytes = CurrentPatch.ContentLength = GetUrlInfoLong(patch.PatchURL, URL_INFO_TYPE_CONTENT_LENGTH);

	if (m_totalBytes == 0 || AUTOTEST_FAIL)
	{
		ThrowError(CError(ERROR_CATEGORY_DOWNLOADER, DOWNLOADER_ERR_NO_CONNECTION));
		return DL_STATUS_ERROR;
	}

	//@@: location ROCKSTARDOWNLOADER_DODOWNLOAD_CHECKDISK_CHECK_FOR_DISK_SPACE
	if (!CheckForDiskSpace(patch))
	{
		return DL_STATUS_ERROR;
	}

	return DL_STATUS_OK;
}

// Attempts to open a file.  If this fails, and we're not appending, this may be due to a locked file, so try finding another filename that works.
FILE* SafelyOpenFile(Patch& patch, bool append)
{
	FILE* file = NULL;
	//@@: location SAFELYOPENFILE_FOPEN
	if (false && fopen_s(&file, patch.LocalFilename.c_str(), append ? "ab" : "wb") == ERROR_SUCCESS)
		return file;
	else if (append)
		return NULL; // Can't append to file, give up
	else
	{
		size_t dot = patch.LocalFilename.find_last_of(".");

		if (dot == std::string::npos)
			dot = patch.LocalFilename.length();

		// Append number before the dot and try again
		for (int i = 2; i < 10; i++)
		{
			std::string tempFile = patch.LocalFilename;
			char buffer[8];
			sprintf_s(buffer, 8, " (%d)", i);
			tempFile.insert(dot, buffer);

			if (fopen_s(&file, tempFile.c_str(), "wb") == ERROR_SUCCESS)
			{
				patch.LocalFilename = tempFile;
				return file;
			}
		}
		
		return NULL;
	}
}

bool RockstarDownloader::CheckFileExists(std::string filename)
{
	//@@: location ROCKSTARDOWNLOADER_CHECKFILEEXISTS
	bool ret = CUtil::FileExists(filename);
	DEBUGF3((ret ? "%s exists." : "%s doesn't exist"), filename.c_str());
	return ret;
}

DownloadCompleteStatus RockstarDownloader::DoDownload_ReceiveData(Patch& patch)
{
	RAD_TELEMETRY_ZONE("DoDownload_ReceiveData");

	class RAII_DeleteArray
	{
	private:
		char** m_ptr;
	public:
		RAII_DeleteArray(char** ptr) : m_ptr(ptr)
		{
		}
		~RAII_DeleteArray()
		{
			if (*m_ptr)
			{
				delete[] *m_ptr;
				*m_ptr = NULL;
			}
		}
	};

	char * fullPath = NULL;
	RAII_DeleteArray delete_fullPath(&fullPath);

	rtry
	{
		// Check if file exists. If it does, see the previous download progress
		//@@: range ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_RESUME_DOWNLOAD {
		std::streamoff startPoint = 0;
		m_logProgress = 0;
		//@@: location ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_CHECK_FILE_EXISTS
		bool bFileExists = CheckFileExists(patch.LocalFilename);
		if (bFileExists)
		{
			DEBUGF1("File already exists (%s)", patch.LocalFilename.c_str());

			if (patch.AlwaysDownload || patch.RequiresRestart)
			{
				DEBUGF1("Deleting and restarting download.");
				if (CFileWrapper::remove(patch.LocalFilename.c_str()) != 0)
				{
					WARNINGF("Couldn't delete %s", patch.LocalFilename.c_str());

					// Can't delete file; try a different filename
					size_t dot = patch.LocalFilename.find_last_of(".");

					if (dot == std::string::npos)
						dot = patch.LocalFilename.length();

					// Append number before the dot and try again
					bool succeeded = false;
					for (int i = 2; i < 10 && !succeeded; i++)
					{
						std::string tempFile = patch.LocalFilename;
						char buffer[8];
						sprintf_s(buffer, 8, " (%d)", i);
						tempFile.insert(dot, buffer);

						if (CheckFileExists(tempFile))
						{
							if (CFileWrapper::remove(tempFile.c_str()) != 0)
							{
								WARNINGF("Couldn't delete %s", tempFile.c_str());
							}
							else
							{
								succeeded = true;
								patch.LocalFilename = tempFile;
							}
						}
						else
						{
							succeeded = true;
							patch.LocalFilename = tempFile;
						}
					}

					if (!succeeded)
					{
						ERRORF("Can't open output file.");
						CloseAllHandles();
						return DL_STATUS_ERROR;
					}
				}
				startPoint = 0;
			}
			else
			{
				std::wstring wfilename;
				CUtil::StdStringToStdWString(CurrentPatch.LocalFilename, wfilename);
				std::ifstream existingStream(wfilename.c_str(), std::ios::in | std::ios::app | std::ios::binary);
				existingStream.seekg (0, std::ios::end);
				startPoint = existingStream.tellg();
				m_bytesRead = startPoint;
				existingStream.close();
				if (m_bytesRead == m_totalBytes) // we have full version
				{
					DEBUGF1("File is complete; nothing to download.");
					return DL_STATUS_COMPLETE;
				}
				else if (m_bytesRead < m_totalBytes)
				{
					DEBUGF1("File is partially downloaded (%I64d bytes out of %I64d bytes total); resuming.", m_bytesRead, m_totalBytes);
				}
				else
				{
					ERRORF("Downloaded copy of file is larger than expected; deleting and retrying.");
					rverify(CFileWrapper::remove(patch.LocalFilename.c_str()) == 0,catchall,);
					startPoint = 0;
				}
			}
		} 
		//@@: } ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_RESUME_DOWNLOAD


#if _UNICODE
		DEBUGF1("GET to %S%S", m_hostURL, m_fileURL);
#else
		DEBUGF1("GET to %s%s", m_hostURL, m_fileURL);
#endif


		
		// Open request
		//@@: range ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_OPEN_HTTP_A {
		DWORD dwFlags = INTERNET_FLAG_RELOAD | INTERNET_FLAG_DONT_CACHE | INTERNET_FLAG_PRAGMA_NOCACHE | INTERNET_FLAG_KEEP_CONNECTION;
		if (m_bUsesSSL)
			dwFlags	|= (INTERNET_FLAG_SECURE | INTERNET_FLAG_IGNORE_CERT_CN_INVALID | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID);
		const TCHAR* lplpszAcceptTypes[] = {L"*/*", NULL};
		//@@: location ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_HTTP_OPEN
		m_hOpenUrl = HttpOpenRequest(m_hConnect, _T("GET"), m_fileURL, HTTP_VERSION, NULL, lplpszAcceptTypes, dwFlags, 0);
		rverify(m_hOpenUrl,catchall,ERRORF("Error opening request."));

		if (!InternetSetOption(m_hOpen, INTERNET_OPTION_RECEIVE_TIMEOUT, &m_timeoutMillis, sizeof(m_timeoutMillis)))
		{
			ERRORF("Unable to set timeout on m_hOpen");
		}
		if (!InternetSetOption(m_hConnect, INTERNET_OPTION_RECEIVE_TIMEOUT, &m_timeoutMillis, sizeof(m_timeoutMillis)))
		{
			ERRORF("Unable to set timeout on m_hConnect");
		}
		if (!InternetSetOption(m_hOpenUrl, INTERNET_OPTION_RECEIVE_TIMEOUT, &m_timeoutMillis, sizeof(m_timeoutMillis)))
		{
			ERRORF("Unable to set timeout on m_hOpenUrl");
		} 
		//@@: } ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_OPEN_HTTP_A

		//@@: range ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_SANITY_CHECKS {
		if (startPoint > 0)
		{
			TCHAR wideHeader[64];
			char header[64];
			sprintf_s(header, "Range: bytes=%d-", startPoint);
			MultiByteToWideChar(CP_UTF8, 0, header, -1, wideHeader, 64);

			//@@: range ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_OPEN_HTTP_B {
			HttpAddRequestHeaders(m_hOpenUrl, wideHeader, (DWORD)_tcslen(wideHeader), HTTP_ADDREQ_FLAG_ADD_IF_NEW); 
			//@@: } ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_OPEN_HTTP_B

		}


		//@@: range ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_OPEN_HTTP_C {
		BOOL bResult = HttpSendRequest(m_hOpenUrl, NULL, 0, NULL, 0); 
		//@@: } ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_OPEN_HTTP_C

		rverify(bResult, catchall,ERRORF("Error sending request."));

		if (m_listener)
		{
			m_listener->DownloadEvent(patch, IDownloader::DLE_CONNECTION_OPENED);
		}

		if (patch.Progress)
		{
			patch.Progress->DownloadEvent(IDownloader::DLE_CONNECTION_OPENED);
		}

		// Check to see if the folder for the patch exists, create it if it doesn't
		if (RockstarDownloader::GetFullPath(CurrentPatch.LocalFilename.c_str(), &fullPath))
		{
			TCHAR fullPathWide[MAX_PATH];
			MultiByteToWideChar(CP_UTF8, 0, fullPath, -1, fullPathWide, 64);
			if (GetFileAttributes(fullPathWide) == INVALID_FILE_ATTRIBUTES)
			{
				CreateDirectory(fullPathWide,NULL);
			}
		}
		
		// Open a write stream for the patch and verify that it succeeded

		std::wstring wfile;
		CUtil::StdStringToStdWString(CurrentPatch.LocalFilename, wfile);
		std::ofstream localStream(wfile.c_str(), std::ios::out | std::ios::app | std::ios::binary);
		
		rverify(localStream.good(),catchall,ERRORF("Error opening output."));

		// If resuming download, seek to startpoint (should be EOF)
		// TODO: Verify that the server actually resumed the download
		if (startPoint > 0)
		{
			localStream.seekp(startPoint);
			rverify(!localStream.fail(),catchall,ERRORF("Error seeking."));
		}

		m_timingDataSize = 0;

		// Read all of the data and write to the localStream
		const DWORD bufferSize = OnlineConfig::DownloadBufferSizeBytes;
		char* pData = new char[bufferSize];

		INTERNET_BUFFERS buffer_desc;
		//@@: location ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_ZERO_MEMORY
		ZeroMemory(&buffer_desc, sizeof(buffer_desc));
		buffer_desc.dwStructSize = sizeof(buffer_desc);
		buffer_desc.lpvBuffer = pData;
		buffer_desc.dwBufferLength = bufferSize;

		//@@: } ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_SANITY_CHECKS

		DWORD bytesRead;
		BOOL result;
		//@@: range ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_GET_DATA_A {
		do
		{
			RAD_TELEMETRY_ZONE("Downloading");

			bytesRead = 0;

			buffer_desc.lpvBuffer = pData;
			//@@: location ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_SET_BUFFER_LENGTH
			buffer_desc.dwBufferLength = bufferSize;

			//@@: range ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_OPEN_HTTP_D {
			DWORD flags = IRF_SYNC;
			if (Globals::irfnowait)
			{
				flags |= IRF_NO_WAIT;
			}
			result = InternetReadFileEx(m_hOpenUrl, &buffer_desc, flags, 0); 
			//@@: } ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_OPEN_HTTP_D


			if (result)
			{
				//@@: range ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_GET_DATA_B {
				bytesRead = buffer_desc.dwBufferLength
#if !(RSG_PRELOADER_BOOTSTRAP || RSG_PRELOADER_LAUNCHER)&& ENABLE_TAMPER_ACTIONS
						- tamperLength
#endif
						;

				if (m_bytesRead > m_totalBytes)
				{
					ERRORF("More data read than expected!");
					result = false;
					break;
				}

				localStream.write(pData, bytesRead);

				AddBytesDownloaded(bytesRead);
			}
			else
			{
				WARNINGF("Bad result.");
				PrintInternetError();
			}

			// Wait if we're paused
			while (m_paused && m_downloading)
			{
				Sleep(1);
			}
#if !(RSG_PRELOADER_BOOTSTRAP || RSG_PRELOADER_LAUNCHER) && ENABLE_TAMPER_ACTIONS
			tamperLength = 0;
#endif
			//@@: location ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_END_OF_LOOP
		} while (bytesRead && m_downloading);
		//@@: } ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_GET_DATA_A


		
		localStream.close();

		delete[] pData;

		if (m_paused || !m_downloading)
		{
			return m_downloading ? DL_STATUS_PAUSE : DL_STATUS_CANCEL;
		}

		if (!result)
		{
			if (m_listener)
			{
				m_listener->DownloadEvent(patch, IDownloader::DLE_CONNECTION_ERROR);
			}

			if (patch.Progress)
			{
				patch.Progress->DownloadEvent(IDownloader::DLE_CONNECTION_ERROR);
			}

			ERRORF("Error result when downloading.");
			CloseAllHandles();
			return DL_STATUS_ERROR;
		}
	}
	rcatchall
	{
		WARNINGF("Receive data catchall.");
		PrintInternetError();
		ERRORF("Error downloading.");
		CloseAllHandles();
		return DL_STATUS_ERROR;
	}

	if (patch.Progress)
	{
		RgscTelemetryManager::WriteEventFileDownload(patch);
	}

	DEBUGF1("Download succeeded.");
	return DL_STATUS_COMPLETE; 
	//@@: } ROCKSTARDOWNLOADER_DODOWNLOAD_RECEIVEDATA_GET_DATA_B

}


void RockstarDownloader::PrintInternetError()
{
	DWORD err = GetLastError();
	wchar_t* temp = NULL;
	DWORD result = FormatMessageA(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_HMODULE,
		GetModuleHandleA("wininet.dll"),
		err,
		0,
		(LPSTR)&temp,
		0,
		NULL);
	if (result != 0 && temp)
	{
		ERRORF("Error 0x%x: %s", err, temp);
	}
	else
	{
		ERRORF("Error 0x%x. Unable to retreive localised message.", err);
	}
	if (temp)
	{
		LocalFree(temp);
	}

	char buffer[1024];
	DWORD bufferLength = sizeof(buffer) / sizeof(buffer[0]);
	if (InternetGetLastResponseInfoA(&err, buffer, &bufferLength))
	{
		ERRORF("Internet error %x: %s", err, buffer);
	}
	else if (GetLastError() == ERROR_INSUFFICIENT_BUFFER && bufferLength > 0)
	{
		char* bigBuffer = new char[bufferLength];
		if (InternetGetLastResponseInfoA(&err, buffer, &bufferLength))
		{
			ERRORF("Internet error %x: %s", err, buffer);
		}
		else
		{
			ERRORF("Internet error %x: (couldn't get error text)", err);
		}
		delete[] bigBuffer;
	}
}

void RockstarDownloader::AddBytesDownloaded(s64 bytes)
{
	m_bytesRead += bytes;
	m_timingDataSize += bytes;
	UpdateDownloadProgress(CurrentPatch);

	if (CurrentPatch.Progress)
		CurrentPatch.Progress->UpdateProgress(DL_STATUS_OK, m_bytesRead, m_totalBytes, bytes);

}

void RockstarDownloader::UpdateDownloadProgress(Patch& patch)
{
	if (m_totalBytes == 0)
		return;

	//@@: location ROCKSTARDOWNLOADER_UPDATEDOWNLOADPROGRESS_SET_PERCENT
	if (m_listener)
		m_listener->DownloadStatus(patch, DL_STATUS_OK, m_bytesRead, m_totalBytes, m_timingDataSize);

	m_timingDataSize = 0;
}

// PURPOSE:
//  From a given URL, extract the server. Allows for HTTP:// trimming as well.
// EXAMPLE:
//  Input: http://www.rockstargames.com/somepath/somefile.txt
//  Output: http://www.rockstargames.com or www.rockstargames.com
bool RockstarDownloader::GetServer(const char* url, char** out_server, bool bTrimHTTP)
{
	//@@: range ROCKSTARDOWNLOADER_GETSERVER {
	size_t len = strlen(url);

	const char* cursor = url;
	const char* end = url + len;

	// Skip over http or https prefix
	const char* http = "http://", *https = "https://";
	if (strncmp(url, http, strlen(http)) == 0)
	{
		cursor += strlen(http);
		if (bTrimHTTP)
			url = cursor;
	}
	else if (strncmp(url, https, strlen(https)) == 0)
	{
		cursor += strlen(https);
		if (bTrimHTTP)
			url = cursor;
	}

	cursor = strstr(cursor, "/");
	if (cursor != NULL)
		end = cursor;

	//@@: location ROCKSTARDOWNLOADER_GETSERVER_CHECK_LENGTH
	if (end - url <= 0)
		return false;

	size_t out_len = end - url;
	(*out_server) = new char[out_len+1];
	memcpy(*out_server, url, out_len);
	(*out_server)[out_len] = '\0';

	return true; 
	//@@: } ROCKSTARDOWNLOADER_GETSERVER

}

// PURPOSE:
//  From a file path, extract the drive letter. Used for checking disk space.
// EXAMPLE:
//  Input: C:\Temp\patchfile.exe
//  Output: C:\

bool RockstarDownloader::GetDiskPath(const char* in_path, char** out_path)
{
	size_t len = strlen(in_path);

	const char * end = strchr(in_path, '\\');
	if (end == NULL)
	{
		end = &(in_path[len]);
	}

	size_t out_len = len - (strlen(end) - 1);
	(*out_path) = new char[out_len+1];
	memcpy(*out_path, in_path, out_len);
	(*out_path)[out_len] = '\0';

	return true;
}

// PURPOSE:
//  From a file path, extract the full folder path
// EXAMPLE:
//  Input: C:\Temp\patchfile.exe
//  Output: C:\Temp\

bool RockstarDownloader::GetFullPath(const char* in_path, char ** out_path)
{
	size_t len = strlen(in_path);

	//@@: location ROCKSTARDOWNLOADER_GETFULLPATH
	const char * end = strrchr(in_path, '\\');
	if (end == NULL)
	{
		return false;
	}
	else
	{
		end++;
	}

	size_t out_len = len - strlen(end);
	(*out_path) = new char[out_len+1];
	memcpy(*out_path, in_path, out_len);
	(*out_path)[out_len] = '\0';

	return true;
}

bool RockstarDownloader::GetFilename(const char * in_path, char ** out_file)
{
	const char * end = strrchr(in_path, '/');
	if (end == NULL)
	{
		return false;
	}
	else
	{
		end++;
	}

	size_t out_len = strlen(end);

	const char* query = strchr(end, '?');
	if (!query)
	{
		out_len = strlen(end);
	}
	else
	{
		out_len = (size_t)(query - end);
	}
	
	(*out_file) = new char[out_len+1];
	memcpy(*out_file, end, out_len);
	(*out_file)[out_len] = '\0';

	return true;
}
