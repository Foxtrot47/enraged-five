#pragma once

#include <string>
#include <vector>

class CLocalisation
{
public:
	CLocalisation();
	~CLocalisation();

	static CLocalisation& Instance();
	void Init();

	std::string GetProductSku();
	std::string GetString(UINT identifier);
	std::wstring CLocalisation::GetWString(UINT identifier);

	LANGID GetCurrentLanguage() { return m_languageId; }
	
	void SetLanguageFromSteam(const char* steamLanguage);

private:
	void SetLanguageID(const char* culture, const std::string& installedLang);
	std::string m_productSku;
	std::string m_installedLanguage;

	LANGID m_languageId;
};