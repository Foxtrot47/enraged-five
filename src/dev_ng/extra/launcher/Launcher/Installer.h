#pragma once

#include "IPatchCheck.h"

class Installer
{
public:
	struct InstallListener
	{
		virtual void InstallComplete(bool succeeded, const std::string& localFilename, const std::string& friendlyName, const std::string& skuName, int exitCode) = 0;
		virtual void InstallError(const std::string& localFilename, const std::string& friendlyName, const CError& error) = 0;
		virtual void InstallRestart() = 0;
		virtual void InstallShutdown() {}
	};

	enum InstallState
	{
		INSTALL_IDLE,
		INSTALL_RUNNING,
		INSTALL_ERROR,
		INSTALL_RESTART,
		INSTALL_SHUTDOWN
	};

	static Installer& Instance();

	void SetListener(InstallListener* listener) { m_listener = listener; }
	void InstallPatch(Patch& patch);

	void Cancel() { m_bCancelled = true; }

private:
	Installer(void);
	~Installer(void);

	Patch m_currentInstallPatch;
	InstallListener* m_listener;

	InstallState InstallPatch(Patch& patch, HANDLE* out_processHandle);
	void InstallProcess_Exited(Patch& patch, int exitCode);

	bool m_bCancelled;
};
