#pragma once

#include <stdarg.h>
#include <fstream>
#include <Windows.h>

#define RSG_FINAL_DEBUG 0

#if RSG_FINAL && !RSG_FINAL_DEBUG
#define DEBUGF1(...)
#define DEBUGF2(...)
#define DEBUGF3(...)
#define OUTPUT_ONLY(...)
#else
#define DEBUGF1  CChannel::Debugf1
#define DEBUGF2  CChannel::Debugf2
#define DEBUGF3  CChannel::Debugf3
#define OUTPUT_ONLY(x) x
#endif

#define ERRORF	 CChannel::Errorf
#define WARNINGF CChannel::Warningf
#define DISPLAYF CChannel::Displayf

class CChannel
{
public:
	static void OpenLogFile(const wchar_t* name);
	static void CloseLogFile();

	enum Severity
	{
		SEVERITY_LEVEL_ERROR,
		SEVERITY_LEVEL_WARNING,
		SEVERITY_LEVEL_DISPLAY,
		SEVERITY_LEVEL_DEBUG1,
		SEVERITY_LEVEL_DEBUG2,
		SEVERITY_LEVEL_DEBUG3
	};

	static void SetGlobalLogLevels(Severity file, Severity tty);

	static void Errorf(const char* fmt, ...);
	static void Warningf(const char* fmt, ...);
	static void Displayf(const char* fmt, ...);

	static void Debugf1(const char* fmt, ...);
	static void Debugf2(const char* fmt, ...);
	static void Debugf3(const char* fmt, ...);

	static void Logf(Severity severity, const char* fmt, ...);

private:
	CChannel() {};
	~CChannel() {};

private:
	static void CheckInitialised();
	static void vLogf(Severity severity, const char* fmt, va_list args);

	static std::stringstream sm_logMemoryStream;
	static std::ofstream sm_logfileStream;

	static std::ostream* sm_logOutputStream;

	static CRITICAL_SECTION sm_criticalSection;
	static bool sm_bInitialised;

	static Severity sm_fileSeverity, sm_ttySeverity;
};
