#pragma once

#include <vector>
#include <string>

// Based on CConfigOption, but we only need ints
class COnlineConfigOption
{
public:
	explicit COnlineConfigOption(const char* name, int minimum, int default_value, int maximum);
	explicit COnlineConfigOption(const char* name, bool default_value);
	explicit COnlineConfigOption(const char* name, const char* default_value);

	void Parse(const char* option);
	const char* GetName() const;
	const std::string& GetRawValue() const;

	operator int() const { return m_value; }

	static bool DownloadOptionsSynchronous();
	static void CancelDownloadingOptions();

private:
	static bool ParseOptions(const std::string& filepath);
	static std::vector<COnlineConfigOption*>& GetOptionsNonConst();
	const char* m_name;
	int m_minimum, m_value, m_maximum;
	std::string m_rawValue;
};

#ifdef DEFINE_ONLINE_CONFIG_OPTIONS
#define ONLINE_OPTION(name, minimum, def, maximum) COnlineConfigOption name(#name, minimum, def, maximum)
#define ONLINE_BOOLEAN(name, def) COnlineConfigOption name(#name, def)
#define ONLINE_STRING(name, def) COnlineConfigOption name(#name, def)
#else
#define ONLINE_OPTION(name, minimum, def, maximum) extern COnlineConfigOption name
#define ONLINE_BOOLEAN(name, def) extern COnlineConfigOption name
#define ONLINE_STRING(name, def) extern COnlineConfigOption name
#endif

// The online options themselves
namespace OnlineConfig
{
	const int KB = 1024;
	const int MB = 1024 * KB;
	const int MINUTES = 60;

	ONLINE_BOOLEAN(UseDev, false);
	ONLINE_OPTION(ConnectionLimit, 1, 10, 40);
	ONLINE_OPTION(DownloadChunkSizeBytes, 1 * KB, 2 * MB, 64 * MB);
	ONLINE_OPTION(DownloadBufferSizeBytes, 1 * KB, 16 * KB, 2 * MB);

	ONLINE_OPTION(DownloadTimeoutInitialSeconds, 1, 10, 1 * MINUTES);
	ONLINE_OPTION(DownloadTimeoutIncreaseSeconds, 1, 10, 1 * MINUTES);
	ONLINE_OPTION(DownloadTimeoutMaximumSeconds, 1, 30, 5 * MINUTES);

	ONLINE_OPTION(PatchRetries, 1, 3, 100);
	ONLINE_OPTION(DIPRetries, 1, 20, 100);

	ONLINE_BOOLEAN(CloudSaveEnabled, false);
	ONLINE_BOOLEAN(CloudSaveBetaOnly, true);
	ONLINE_BOOLEAN(CloudSaveWorkerThread, false);
	ONLINE_OPTION(CloudSaveMaxFileSize, 1 * MB, 1 * MB, 2 * MB);
}

#undef ONLINE_OPTION