#include "../Launcher/stdafx.h"
#include "../Launcher/Application.h"
#include "../Launcher/IDownloader.h"
#include "../Launcher/Installer.h"
#include "../Launcher/LauncherPatchCheck.h"
#include "../Launcher/RockstarDownloader.h"
#include "../Launcher/Localisation.h"
#include "../Launcher/Util.h"
#include "../Launcher/CertificateVerify.h"
#include "../Launcher/DownloadManager.h"
#include "../Launcher/UpdateCheck.h"
#include "../Launcher/Globals.h"
#include "../Launcher/Config.h"
#include "../Launcher/OnlineConfig.h"
#include "../Launcher/Channel.h"
#include "../Launcher/TamperSource.h"
#include "../Launcher/GamePatchCheck.h"
#include "../Launcher/Resource.h"
#include "../Launcher/RgscTelemetryManager.h"

#if RSG_DEBUG
#include "../Launcher/Test.h"
#endif

#include "afxwin.h"
#include "afxcmn.h"
#include "afxbutton.h"

#include <stdio.h>
#include <vector>
#include <queue>
#include <sstream>

#include "resource.h"

CImage splashImage;
BITMAP bmpInfo = {0};

HWND g_progressBar;

bool g_hasErrorOccurred = false;
bool g_requireRestart = false;
bool g_PatchInstalled = false;

bool g_emergencyMode = false;


std::queue<Patch> m_patchList;
Patch m_currentPatch;
std::vector<Patch> patches;

void RunUpdateCheck()
{
	static struct Closure : IDownloader::DownloadListener
	{
		//@@: range RUNUPDATECHECK_CLOSURE_METHODS {
		void DownloadComplete(const Patch& patch, DownloadCompleteStatus status)
		{
			if (status == DL_STATUS_COMPLETE)
			{
				//@@: location RUNUPDATECHECK_CLOSURE_DOWNLOADCOMPLETE
				DISPLAYF("Download complete!");
			}
			else if (status == DL_STATUS_PAUSE)
			{
				DISPLAYF("Download paused.");
			}
			else
			{
				std::string token = CLocalisation::Instance().GetString(204);//IDS_DOWNLOAD_ERROR_ATTEMPT);
				ERRORF("Download completed with error: %s", token.c_str());
			}

			Downloader::Instance().DownloadComplete(status, patch.LocalFilename);
			UpdateChecker::Instance().DownloadComplete(status, patch.LocalFilename);
		}
		void DownloadStatus(const Patch& patch, DownloadCompleteStatus status, u64 totalBytesComplete, u64 totalBytesInFile, u64 bytesJustDownloaded)
		{
			if (bytesJustDownloaded > 0)
			{
				DEBUGF1("Download status: %llu / %llu", totalBytesComplete, totalBytesInFile);
			}
		}
		void DownloadError(const Patch& patch, const CError& error)
		{
			g_hasErrorOccurred = true;
			ERRORF("Download error: %s", error.GetLocalisedErrorMessage(error).c_str());
			Downloader::Instance().DownloadError(error);
			UpdateChecker::Instance().DownloadError(error);
		} 
		//@@: } RUNUPDATECHECK_CLOSURE_METHODS

	} closure;

	//@@: range RUNUPDATECHECK_CORE_CALLS {
	DISPLAYF("Initializing downloader...");
	//@@: location RUNUPDATECHECK_INIT_DOWNLOADER
	Downloader::Instance().Init();

	if (g_emergencyMode)
	{
		Downloader::Instance().EnableChunkedDownloading(false);
	}

	DISPLAYF("Adding patch checks...");
	UpdateChecker::Instance().AddBootstrapCheck();
	UpdateChecker::Instance().AddLauncherCheck();

	DISPLAYF("Checking for updates...");
	//@@: location RUNUPDATECHECK_CHECKFORUPDATES_CALL
	UpdateChecker::Instance().CheckForUpdates(&closure);

	DISPLAYF("Shutting down downloader...");
	//@@: location RUNUPDATECHECK_SHUTDOWN_CALL
	Downloader::Instance().Shutdown();
	//@@: } RUNUPDATECHECK_CORE_CALLS

}

bool ProcessNextPatch(Patch& m_currentPatch)
{
	RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_BOOTSTRAP_START_DOWNLOAD);

	bool error = false;
	bool done = false;
	bool certValid = false;
	int attemptNumber = 0;
	const static int maxAttempts = 3;

	struct Closure : IDownloader::DownloadListener
	{
		bool* m_error;
		bool* m_done;
		int* m_attemptNumber;
		bool* m_certValid;

		Closure(bool* error, bool* done, int* attempts, bool* certValid) : m_error(error), m_done(done), m_attemptNumber(attempts), m_certValid(certValid) {}
		//@@: range PROCESSNEXTPATCH_CLOSURE {

		void DownloadComplete(const Patch& patch, DownloadCompleteStatus status)
		{
			if (status == DL_STATUS_COMPLETE)
			{
				//@@: location PROCESSNEXTPATCH_CLOSURE_DOWNLOAD_COMPLETE
				DISPLAYF("Download complete!");
			}
			else if (status == DL_STATUS_PAUSE)
			{
				DISPLAYF("Download paused.");
			}
			else
			{
				std::string token = CLocalisation::Instance().GetString(213);//IDS_DOWNLOAD_ERROR_ATTEMPT);
				CUtil::FormatStringNumbers(token, *m_attemptNumber, maxAttempts);
				ERRORF("Download completed with error: %s", token.c_str());
			}

			TCHAR path[MAX_PATH];
			MultiByteToWideChar(CP_UTF8, 0, patch.LocalFilename.c_str(), -1, path, MAX_PATH);

#if !RSG_FINAL
			if (Constants::CheckingCertificates)
#endif
			{

				//@@: location PROCESSNEXTPATCH_CLOSURE_CHECK_CERTIFICATE
				DEBUGF1("Checking certificate...");
				*m_certValid = CertificateVerify::Verify(path);
			}
#if !RSG_FINAL
			else
			{
				WARNINGF("Skipping certificate check.");
				*m_certValid = true;
			}
#endif

			Sleep(300);
			*m_done = true;
		}
		void DownloadStatus(const Patch& patch, DownloadCompleteStatus status, u64 totalBytesComplete, u64 totalBytesInFile, u64 bytesJustDownloaded)
		{
			if (bytesJustDownloaded > 0)
			{
				DEBUGF1("Download status: %llu / %llu", totalBytesComplete, totalBytesInFile);
				SendMessage(g_progressBar, PBM_SETPOS, (int)(100 * totalBytesComplete / totalBytesInFile), 0);
			}
		}
		void DownloadError(const Patch& patch, const CError& error)
		{
			g_hasErrorOccurred = true;
			ERRORF("Download error: %s", CError::GetLocalisedErrorMessage(error).c_str());
			*m_error = true;
		}; 
		//@@: } PROCESSNEXTPATCH_CLOSURE

	};

	ShowWindow(g_progressBar, SW_SHOW);

	//@@: location PROCESSNEXTPATCH_ATTEMPT
	for (attemptNumber = 1; attemptNumber <= maxAttempts; attemptNumber++) // Have 3 attempts before giving up.
	{
		DEBUGF1("Download attempt %d of %d...", attemptNumber, maxAttempts);
		error = false;
		done = false;
		certValid = true;

		//@@: range PROCESSNEXTPATCH_DOWNLOADFILE {
		Closure closure(&error, &done, &attemptNumber, &certValid);
		RockstarDownloader* down = new RockstarDownloader();
		down->SetListener(&closure);
		//@@: location PROCESSNEXTPATCH_SETCURRENTPATCH
		down->SetCurrentPatch(m_currentPatch);
		down->SetMaxRetries(OnlineConfig::PatchRetries);
		down->StartSynchronously();
		down->SetListener(NULL);
		delete down;
		error = error || !certValid; // If the cert is invalid, an error occured

		if (!error)
		{
			//@@: location PROCESSNEXTPATCH_SUCCESSFUL_DOWNLOAD
			DISPLAYF("Download attempt succeeded.");
			return true;
		}
		else
		{
			TRACE("Download attempt %d of %d failed.\r\n", attemptNumber, maxAttempts);

			const CError& err = CError::GetLastError();

			if (err.GetErrorCategory() == ERROR_CATEGORY_DOWNLOADER && err.GetErrorCode() == DOWNLOADER_ERR_NO_DISK_SPACE)
			{
				std::wstring errorMessage = CUtil::WideStringFromID(IDS_NO_INSTALL_SPACE);

				std::wstring wspaceRequired;
				CUtil::StdStringToStdWString(err.GetExtra(), wspaceRequired);

				std::vector<std::wstring> params;
				params.push_back(wspaceRequired);

				CUtil::FormatString(errorMessage, params);

				std::wstring title = CLocalisation::Instance().GetWString(IDS_HEADING_SORRY);
				MessageBox(NULL, errorMessage.c_str(), title.c_str(), MB_OK);
				ExitProcess(0);
				return false;
			}
		}
		//@@: } PROCESSNEXTPATCH_DOWNLOADFILE
	}

	//@@: location PROCESSNEXTPATCH_NO_DOWNLOAD_OCCURED
	return false;
}

bool InstallCurrentPatch(Patch& m_currentPatch)
{
	bool finished = false, succeeded = false;
	g_PatchInstalled = true;

	RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_BOOTSTRAP_START_INSTALL);

	//@@: range INSTALLCURRENTPATCH_CLOSURE {
	DISPLAYF("Installing update...");
	//@@: location INSTALLCURRENTPATCH_INITIAL_SLEEP
	Sleep(500);

	struct Closure : Installer::InstallListener
	{
		bool *m_finished, *m_succeeded;
		Closure(bool* finished, bool* succeeded) { m_finished = finished; m_succeeded = succeeded; }

		virtual void InstallComplete(bool succeeded, const std::string& localFilename, const std::string& friendlyName, const std::string& skuName, int exitCode)
		{
			if (succeeded)
			{
				//@@: location INSTALLCURRENTPATCH_CLOSURE_INSTALL_COMPLETE
				DISPLAYF("Successfully installed patch.");
			}
			else
			{
				ERRORF("Failed to install patch!");
			}
			DEBUGF1("Filename: %s (Name: %s) of SKU %s", localFilename.c_str(), friendlyName.c_str(), skuName.c_str());
			DEBUGF1("Exit code was %d.", exitCode);
			*m_finished = true;
			//@@: location INSTALLCURRENTPATCH_CLOSURE_SET_SUCCEEDED
			*m_succeeded = succeeded;
		}

		virtual void InstallError(const std::string& localFilename, const std::string& friendlyName, const CError& error)
		{
			const std::string msg = CError::GetLocalisedErrorMessage(error);
			ERRORF("Error occurred installing %s (%s): %s.\r\n", localFilename.c_str(), friendlyName.c_str(), msg.c_str());
			*m_succeeded = false;
		}

		virtual void InstallRestart()
		{
			//@@: location INSTALLCURRENTPATCH_CLOSURE_INSTALLRESTART
			DISPLAYF("Installer requested restart.");
			*m_finished = true;
			*m_succeeded = true;
			g_requireRestart = true;
		}
	}; 
	//@@: } INSTALLCURRENTPATCH_CLOSURE


	Closure* closure = new Closure(&finished, &succeeded);

	//@@: range INSTALLCURRENTPATCH_INSTALL_PATCH {

	Installer::Instance().SetListener(closure);

	Installer::Instance().InstallPatch(m_currentPatch);
	//@@: location INSTALLCURRENTPATCH_INSTALLPATCH_START_LOOP
	while (!finished)
	{
		Sleep(100);
	}

	DEBUGF1("Installer finished.");

	//@@: location INSTALLCURRENTPATCH_SET_INSTALL_LISTENER
	Installer::Instance().SetListener(NULL);
	delete closure;
	return succeeded;
	//@@: } INSTALLCURRENTPATCH_INSTALL_PATCH

}

UINT BackgroundThreadEntry(void* param)
{
	//@@: range BACKGROUNDTHREADENTRY {
	__try
	{
		g_hasErrorOccurred = false;
		DEBUGF1("Starting update thread...");
#if !RSG_FINAL	
		if (Globals::noUpdateCheck)
		{
			Sleep(1000);
			return EXIT_SUCCESS;
		}
#endif

		COnlineConfigOption::DownloadOptionsSynchronous();

		// Run launcher update check
		RunUpdateCheck();

		//@@: location BACKGROUNDTHREADENTRY_GET_PATCHES_TO_INSTALL
		DEBUGF1("Retrieving patch list...");
		patches = UpdateChecker::Instance().PatchesToInstall();

		DEBUGF1("Found %d patches.", patches.size());

		for (size_t i = 0; i < patches.size(); i++)
			m_patchList.push(patches[i]);


		DEBUGF1("About to start WinSock...");
		//@@: range BACKGROUNDTHREADENTRY_WINSOCK_CALLS {
		WSADATA wsaData;
		//@@: location BACKGROUNDTHREADENTRY_WINSOCK_STARTUP
		if (WSAStartup(MAKEWORD(2,2), &wsaData) == 0)
		{
			DEBUGF1("Initialized WinSock.");

			//@@: location BACKGROUNDTHREADENTRY_CHECK_PATCH_LIST
			while (!m_patchList.empty())
			{
				m_currentPatch = m_patchList.front();
				m_patchList.pop();

				//@@: range BACKGROUNDTHREADENTRY_PROCESSNEXTPATCH {
				if (!ProcessNextPatch(m_currentPatch) || !InstallCurrentPatch(m_currentPatch))
				{
					// If any patch fails (it has 3 attempts before giving up), we can't really continue.
					if (m_currentPatch.UpdateType != UT_OPTIONAL)
						break;
				}
				//@@: } BACKGROUNDTHREADENTRY_PROCESSNEXTPATCH


				if (g_requireRestart)
				{
					//@@: location BACKGROUNDTHREADENTRY_REQUIRE_RESTART
					break;
				}
			}

			//@@: location BACKGROUNDTHREADENTRY_WSA_CLEANUP
			WSACleanup();
		}
		//@@: } BACKGROUNDTHREADENTRY_WINSOCK_CALLS

		else
		{
			//@@: location BACKGROUNDTHREADENTRY_WINSOCK_FAIL
			ERRORF("Failed to init WinSock!");
		}
	}
	__except(1)
	{
		ERRORF("Patcher failed.");

		struct Closure
		{
			static void SEHException()
			{
				CError::SetError(ERROR_CATEGORY_LAUNCHER, LAUNCHER_ERR_SEH_EXCEPTION);
			}
		};

		Closure::SEHException();
		g_hasErrorOccurred = true;

		if (g_emergencyMode)
		{
			ERRORF("Patcher failure!");
		}
	}

	//@@: location BACKGROUNDTHREADENTRY_CHECK_ERROR
	if (g_hasErrorOccurred)
	{
		return EXIT_FAILURE;
	}
	else
	{
		RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_BOOTSTRAP_COMPLETED_UPDATE);

		//@@: location BACKGROUNDTHREADENTRY_RETURN_SUCCESS
		return EXIT_SUCCESS;
	}
	//@@: } BACKGROUNDTHREADENTRY

}

wchar_t* AllocateArgs()
{
	std::wstring wargs;

	// Pass on the entire commandline to the launcher
	//@@: location ALLOCATEARGS_GET_COMMAND_LINE
	LPWSTR cmd = GetCommandLineW();
	if (cmd)
	{
		int argc = 0;
		LPWSTR* argv = CommandLineToArgvW(cmd, &argc);

		// Skip first one, as it's the executable path
		for (int i = 1; i < argc; i++)
		{
			if (i == 1)
				wargs = argv[i];
			else
			{
				wargs += L" ";
				wargs += argv[i];
			}
		}
	}

	if (!wargs.empty())
		wargs += L" ";
	wargs += L"-frombootstrap";

	std::string args;
	CUtil::WStringToStdString(wargs, args);

	DEBUGF1("Command line args: %s", args.c_str());

	wchar_t* allocated = new wchar_t[wargs.length()+1];
	wcscpy_s(allocated, wargs.length()+1, wargs.c_str());
	return allocated;
}


bool LoadBackgroundImage()
{
	//@@: location LOADBACKGROUNDIMAGE_LOAD_IMAGE

	HRSRC resource = FindResource(NULL, MAKEINTRESOURCE(IDR_SPLASH), RT_RCDATA);
	if (resource == NULL)
	{
		ERRORF("Can't find background image resource!");
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	HGLOBAL memory = LoadResource(NULL, resource);
	if (memory == NULL)
	{
		ERRORF("Can't load background image resource!");
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	DWORD size = SizeofResource(NULL, resource);
	if (size == 0)
	{
		ERRORF("Zero-length background image resource!");
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	LPVOID ptr = LockResource(memory);
	if (ptr == NULL)
	{
		ERRORF("Can't lock background image resource!");
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, size);
	if (!hGlobal)
	{
		ERRORF("Can't allocate background image memory!");
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	void* dest = GlobalLock(hGlobal);
	if (!dest)
	{
		ERRORF("Can't allocate background image memory!");
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	CopyMemory(dest, ptr, size);

	IStream* pStream = NULL;
	if (CreateStreamOnHGlobal(hGlobal, TRUE, &pStream) != S_OK)
	{
		ERRORF("Can't CreateStreamOnHGlobal for background image!");
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	splashImage.Load(pStream);
	if (!(HBITMAP)splashImage)
	{
		CUtil::PrintSystemErrorMessage(GetLastError());
		return false;
	}

	if (splashImage.GetBPP() == 32)
	{
		// Premultiply the alpha
		unsigned char* bits = (unsigned char*)splashImage.GetBits();
		int pitch = splashImage.GetPitch();
		int width = splashImage.GetWidth();
		int height = splashImage.GetHeight();

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int i = x * 4;
				unsigned char alpha = bits[i+3];
				bits[i+0] = (bits[i+0] * alpha) / 256;
				bits[i+1] = (bits[i+1] * alpha) / 256;
				bits[i+2] = (bits[i+2] * alpha) / 256;
			}

			bits += (ptrdiff_t)pitch;
		}
	}

	GlobalUnlock(hGlobal);
	GlobalFree(hGlobal);

	//@@: location LOADBACKGROUNDIMAGE_SETTRANSPARENTCOLOR
	splashImage.SetTransparentColor(RGB(255,0,255));

	return true;
}

void GetLauncherPath(wchar_t (&outPath)[MAX_PATH])
{
	std::string path = GamePatchCheck::GetGameExecutableFolder();
	path += "\\";
	path += Constants::LauncherName.Get();

	DISPLAYF("Launcher path: %s", path.c_str());

	std::wstring wpath;
	CUtil::StdStringToStdWString(path, wpath);
	wcscpy_s(outPath, wpath.c_str());
}

void CreateProgressBar(HWND hwnd, HINSTANCE hInst, int wndWidth, int wndHeight)
{
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	InitCtrls.dwICC = ICC_WIN95_CLASSES | ICC_PROGRESS_CLASS;
	InitCommonControlsEx(&InitCtrls);


	int margin = 5;
	int height = 8;
	
	g_progressBar = CreateWindowEx(0, PROGRESS_CLASS, NULL, WS_CHILD | PBS_SMOOTH,
		margin,
		wndHeight - height - margin,
		wndWidth - margin * 2,
		height,
		hwnd, NULL, hInst, NULL);

	SendMessage(g_progressBar, PBM_SETRANGE32, 0, 100);
	SetWindowTheme(g_progressBar, L"", L"");

	SendMessage(g_progressBar, PBM_SETBKCOLOR, 0, RGB(
		Constants::LoadingBarBackgroundR.Get(),
		Constants::LoadingBarBackgroundG.Get(),
		Constants::LoadingBarBackgroundB.Get()));

	SendMessage(g_progressBar, PBM_SETBARCOLOR, 0, RGB(
		Constants::LoadingBarForegroundR.Get(),
		Constants::LoadingBarForegroundG.Get(),
		Constants::LoadingBarForegroundB.Get()));
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_CLOSE:
		//@@: location WNDPROC_POSTQUITMESSAGE
		PostQuitMessage(0);
		return 0;
	case WM_PAINT:
		DEBUGF3("Bootstrap painting.");


		PAINTSTRUCT ps;

		HDC hdc = BeginPaint(hWnd, &ps);

		RECT rect = {0,0, bmpInfo.bmWidth, bmpInfo.bmHeight};
		
		splashImage.Draw(hdc, rect, rect);

		EndPaint(hWnd, &ps);


		return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	//@@: range WINMAIN_EARLY_ENTRY {
	//@@: location MAIN_ENTRY_POINT
	__try
	{
#if RSG_FINAL
		CChannel::SetGlobalLogLevels(CChannel::SEVERITY_LEVEL_DISPLAY, CChannel::SEVERITY_LEVEL_DISPLAY);
#else
		CChannel::SetGlobalLogLevels(CChannel::SEVERITY_LEVEL_DEBUG3, CChannel::SEVERITY_LEVEL_DEBUG3);
#endif

		// Handle commandline parameters
		//@@: location MAIN_PROCESS_COMMAND_LINE
		CCommandlineArgument::ProcessCommandline();

		//@@: } WINMAIN_EARLY_ENTRY

		//@@: range MAIN_PREPARE_BACKGROUND_THREAD {
		CConfigOption::LoadOptions(IDR_LAUNCHER_CONFIG_XML);

		//@@: location MAIN_CREATE_LOG_FILE
		CChannel::OpenLogFile(L"bootstrap.log");

		DEBUGF1("Bootstrap starting!");

		//@@: location MAIN_LOAD_BACKGROUND_IMAGE
		if (!LoadBackgroundImage())
		{
			ERRORF("Failed to load bitmap!");
			return 1;
		}

		CBitmap *bmp = CBitmap::FromHandle(splashImage);

		if (!bmp)
		{
			ERRORF("Failed to load bitmap!");
			return 1;
		}


		WNDCLASS wc;
		ZeroMemory(&wc, sizeof(wc));
		wc.hbrBackground = (HBRUSH)COLOR_BACKGROUND;
		wc.hInstance = hInstance;
		wc.lpfnWndProc = WndProc;
		wc.lpszClassName = TEXT("BootstrapLauncher");
		//@@: location WINMAIN_REGISTERCLASS
		if (!RegisterClass(&wc))
		{
			ERRORF("Can't register window class!");
			return 1;
		}

		int width = 320, height = 240;

		bmp->GetBitmap(&bmpInfo);

		DEBUGF1("Bmp size: %d x %d", bmpInfo.bmWidth, bmpInfo.bmHeight);

		if (width < bmpInfo.bmWidth)
			width = bmpInfo.bmWidth;
		if (height < bmpInfo.bmHeight)
			height = bmpInfo.bmHeight;

		//@@: location WINMAIN_GETSYSTEMMETRICS
		int screenWidth = GetSystemMetrics(SM_CXSCREEN), screenHeight = GetSystemMetrics(SM_CYSCREEN);

		int x = (screenWidth - width) / 2, y = (screenHeight - height) / 2;

		HWND hWnd = CreateWindowEx(0, wc.lpszClassName, TEXT("Bootstrap"), WS_POPUP | WS_VISIBLE, x, y, width, height, NULL, NULL, hInstance, NULL);
		if (!hWnd)
		{
			ERRORF("Can't create window!");
			return 1;
		}

		CLocalisation::Instance().Init();

		RgscTelemetryManager::WriteEvent(RgscTelemetryManager::EVENT_BOOTSTRAP_START);

		ShowWindow(hWnd, nShowCmd);
		//@@: location WINMAIN_UPDATEWINDOW
		UpdateWindow(hWnd);

		//@@: location MAIN_CREATECOMPATIBLEDC
		CreateProgressBar(hWnd, hInstance, width, height);
		//@@: location MAIN_UPDATELAYEREDWINDOW

		//@@: } MAIN_PREPARE_BACKGROUND_THREAD

		//@@: range MAIN_CREATE_BACKGROUND_THREAD {
		//@@: location MAIN_AFXBEGINTHREAD
		CWinThread* pWinThread = AfxBeginThread(BackgroundThreadEntry, NULL, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
		pWinThread->m_bAutoDelete = false;
		//@@: location MAIN_RESUMETHREAD
		pWinThread->ResumeThread();

		//@@: } MAIN_CREATE_BACKGROUND_THREAD


		DWORD dwExit;
		MSG msg;
		//@@: location MAIN_ZERO_MEMORY
		ZeroMemory(&msg, sizeof(msg));
		bool peekRet = 0;

		//@@: range MAIN_MAIN_LOOP {
		while (true)
		{
			//@@: location WINMAIN_PEEKMESSAGE
			if (peekRet = PeekMessage (&msg, NULL, 0, 0, PM_REMOVE) == TRUE)
			{
				if (msg.message == WM_QUIT)
				{
					DEBUGF1("Received quit message!");
					break;
				}
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			//@@: location WINMAIN_GETEXITCODETHREAD
			BOOL bRet = GetExitCodeThread(pWinThread->m_hThread, &dwExit);
			if (bRet && (dwExit == EXIT_FAILURE || dwExit == EXIT_SUCCESS))
			{
				if (dwExit == EXIT_FAILURE && CError::GetLastError().IsError(LAUNCHER_ERR_SEH_EXCEPTION) && !g_emergencyMode)
				{
					g_emergencyMode = true;
					ERRORF("Emergency mode.");
					delete pWinThread;
					pWinThread = AfxBeginThread(BackgroundThreadEntry, NULL, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
					pWinThread->m_bAutoDelete = false;
					pWinThread->ResumeThread();
				}
				//@@: location MAIN_SEND_SUCCESS_MESSAGE
				SendMessage(hWnd, WM_CLOSE, NULL, NULL);
			}
		}
		DEBUGF1("Exited main loop.");
		//@@: } MAIN_MAIN_LOOP

	}
	__except(1)
	{
		ERRORF("Bootstrap failure!");
	}

	__try
	{
		//@@: range MAIN_LAUNCH_LAUNCHER {
		//@@: location MAIN_CHECK_RESTART
		if (!g_requireRestart)
		{
			DEBUGF1("No restart required; chaining Launcher.");
			wchar_t strPath[MAX_PATH];
			GetLauncherPath(strPath);
			//@@: location MAIN_BUILD_PATH

			wchar_t* args = AllocateArgs();
#if !RSG_FINAL
			if(!Globals::testFlow && !Globals::autotest_bootstrap_nolauncher)
			{
#endif
				//@@: location MAIN_SHELL_EXECUTE
				HINSTANCE hInst = ShellExecuteW(NULL, L"open", strPath, args, NULL, SW_SHOWNORMAL);
				if ((int)hInst > 32)
				{
					DEBUGF1("ShellExecute succeeded.");
				}
				else
				{
					struct Closure
					{
						static void ErrorOccurred()
						{
							std::wstring title = CLocalisation::Instance().GetWString(IDS_HEADING_SORRY);
							std::wstring text = CLocalisation::Instance().GetWString(IDS_LAUNCHER_ERR_NO_GAME_PATH);
							MessageBox(NULL, text.c_str(), title.c_str(), MB_OK);
						}
					};
					Closure::ErrorOccurred();
				}
#if !RSG_FINAL
			}
			else
			{
				ERRORF("Aborting launch due to debug args.");
			}
#endif
			delete[] args;
		}
		else
		{
			DEBUGF1("Restart required; exiting.");
		}
		//@@: } MAIN_LAUNCH_LAUNCHER
	}
	__except(1)
	{
		struct Closure
		{
			static void ErrorOccurred()
			{
				std::wstring title = CLocalisation::Instance().GetWString(IDS_HEADING_SORRY);
				std::wstring text = CLocalisation::Instance().GetWString(IDS_LAUNCHER_ERR_NO_GAME_PATH);
				MessageBox(NULL, text.c_str(), title.c_str(), MB_OK);
			}
		};
		Closure::ErrorOccurred();
	}
	

	//@@: location MAIN_CLOSE_LOG_FILE
	CChannel::CloseLogFile();

#if !(RSG_PRELOADER_LAUNCHER || RSG_PRELOADER_BOOTSTRAP) && ENABLE_TAMPER_ACTIONS
	//@@: range MAIN_NOP {
	InitTamperCallsToNop(false);
	//@@: } MAIN_NOP
#endif
	return 0;
}

