@echo off
REM Uber batch script to project generate everything.

pushd %~dp0
set _started=%time% 
@echo.
@echo ************************************************
@echo BUILD EM ALL : Project Generation of Entire Game 
@echo ************************************************
@echo.
	
call setenv.bat

call %RS_TOOLSROOT%\script\util\projGen9\generate.bat -changelist %* || goto Error

@echo.
@echo.
@echo.
@echo %~nx0 STARTED  : %_started% 
@echo %~nx0 FINISHED : %time% 
@echo ************************************************
@echo BUILD EM ALL : Completed. 
@echo ************************************************
@echo.

popd
exit /b 0

:Error
popd
exit /b 1
