<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
							generate="class">
 
  <enumdef type="CPedMotionStates::eMotionState" values="hash">    
		<enumval name="MotionState_None"/>
		<enumval name="MotionState_Idle"/>
		<enumval name="MotionState_Walk"/>
		<enumval name="MotionState_Run"/>
		<enumval name="MotionState_Sprint"/>
		<enumval name="MotionState_Crouch_Idle"/>
		<enumval name="MotionState_Crouch_Walk"/>
		<enumval name="MotionState_Crouch_Run"/>
		<enumval name="MotionState_DoNothing"/>
		<enumval name="MotionState_AnimatedVelocity"/>
		<enumval name="MotionState_InVehicle"/>
		<enumval name="MotionState_Aiming"/>
		<enumval name="MotionState_Diving_Idle"/>
		<enumval name="MotionState_Diving_Swim"/>
		<enumval name="MotionState_Swimming_TreadWater"/>
		<enumval name="MotionState_Dead"/>
		<enumval name="MotionState_Stealth_Idle"/>
		<enumval name="MotionState_Stealth_Walk"/>
		<enumval name="MotionState_Stealth_Run"/>
		<enumval name="MotionState_Parachuting"/>
		<enumval name="MotionState_ActionMode_Idle"/>
		<enumval name="MotionState_ActionMode_Walk"/>
		<enumval name="MotionState_ActionMode_Run"/>
    <enumval name="MotionState_Jetpack"/>
  </enumdef>
</ParserSchema>

