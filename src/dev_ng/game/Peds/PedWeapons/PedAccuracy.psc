<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="sPedAccuracyModifiers">
  <float name="PLAYER_RECOIL_MODIFIER_MIN" init="1.f" min="0.f" max="10.f"/>
  <float name="PLAYER_RECOIL_MODIFIER_MAX" init="1.f" min="0.f" max="10.f"/>
  <float name="PLAYER_RECOIL_CROUCHED_MODIFIER" init="0.0f" min="0.0f" max="10.0f"/>
  <float name="PLAYER_BLIND_FIRE_MODIFIER_MIN" init="1.f" min="0.f" max="10.f"/>
  <float name="PLAYER_BLIND_FIRE_MODIFIER_MAX" init="1.f" min="0.f" max="10.f"/>
  <float name="PLAYER_RECENTLY_DAMAGED_MODIFIER" init="0.0f" min="0.0f" max="10.0f"/>
  <float name="AI_GLOBAL_MODIFIER" init="1.f" min="0.f" max="10.f"/>
  <float name="AI_TARGET_IN_COMBAT_ROLL_MODIFIER" init="0.2f" min="0.0f" max="10.0f"/>
  <float name="AI_TARGET_COMING_OUT_OF_COVER_MODIFIER" init="0.0f" min="0.0f" max="10.0f"/>
  <float name="AI_TARGET_IN_COVER_MODIFIER" init="0.0f" min="0.0f" max="10.0f"/>
  <float name="AI_TARGET_WALKING_MODIFIER" init="0.0f" min="0.0f" max="10.0f"/>
  <float name="AI_TARGET_RUNNING_MODIFIER" init="0.0f" min="0.0f" max="10.0f"/>
  <float name="AI_TARGET_IN_AIR_MODIFIER" init="0.0f" min="0.0f" max="10.0f"/>
  <float name="AI_TARGET_DRIVING_AT_SPEED_MODIFIER" init="0.0f" min="0.0f" max="10.0f"/>
  <float name="AI_BLIND_FIRE_MODIFIER" init="0.0f" min="0.0f" max="10.0f"/>
  <float name="AI_DISTANCE_FOR_MAX_ACCURACY_BOOST" init="0.0f" min="0.0f" max="100.0f"/>
  <float name="AI_DISTANCE_FOR_MIN_ACCURACY_BOOST" init="0.0f" min="0.0f" max="100.0f"/>
  <float name="AI_HURT_ON_GROUND_MODIFIER" init="1.0f" min="0.0f" max="10.0f"/>
  <float name="AI_HURT_MODIFIER" init="1" min="0.0f" max="10.0f"/>
  <float name="AI_PROFESSIONAL_PISTOL_VS_AI_MODIFIER" init="1.0f" min="1.0f" max="10.0f"/>
</structdef>

</ParserSchema>
