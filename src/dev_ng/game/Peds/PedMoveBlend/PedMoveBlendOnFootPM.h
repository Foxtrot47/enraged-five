
// Title	:	PedMoveBlendOnFootPM.h
// Author	:	James Broad
// Started	:	18/02/09
// A moveblender derived from CPedMoveBlendOnFoot and of the same MoveBlendType,
// but which implements some of the movement using Parametrised Motion.

#ifndef PED_MOVE_BLEND_ON_FOOT_PM_H
#define	PED_MOVE_BLEND_ON_FOOT_PM_H

#include "PedMoveBlendOnFoot.h"

#endif	// PED_MOVE_BLEND_ON_FOOT_PM_H