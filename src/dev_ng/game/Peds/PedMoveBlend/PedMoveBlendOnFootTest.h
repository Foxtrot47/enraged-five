#ifndef PED_MOVE_BLEND_ON_FOOT_TEST_H
#define PED_MOVE_BLEND_ON_FOOT_TEST_H

//*********************************************************************
// PedMoveBlendOnFootTest.h
// This is a test for a new implementation of ped locomotion system,
// using MoVE networks and the ai motion task-tree.

#include "peds/PedMoveBlend/PedMoveBlendTypes.h"
#include "PedMoveBlendOnFoot.h"
#include "Peds/ped.h"


#endif // PED_MOVE_BLEND_ON_FOOT_TEST_H