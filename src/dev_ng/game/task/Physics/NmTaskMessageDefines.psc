<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
							generate="class">
 
  <enumdef type="NmTaskMessages::eMessages" values="hash">    
		<enumval name="CTaskNM_RagdollDamping"/>
    <enumval name="CTaskNM_UpdateCharacterHealth"/>
  </enumdef>
  
  <enumdef type="NmTaskMessages::eParameters" values="hash">    
		<enumval name="LinConstant"/>
		<enumval name="LinVelocity"/>
		<enumval name="LinVelocity2"/>
		<enumval name="AngConstant"/>
		<enumval name="AngVelocity"/>
		<enumval name="AngVelocity2"/>
    <enumval name="AdaptiveAngVelocity2"/>
    <enumval name="AdaptiveAngVelocity2Max"/>
    <enumval name="AdaptiveAngVelocity2_MinMag"/>
    <enumval name="AdaptiveAngVelocity2_MaxMag"/>
  </enumdef>
</ParserSchema>

