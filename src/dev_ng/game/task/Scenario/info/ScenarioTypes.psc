<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<enumdef type="eVehicleScenarioType">
		<enumval name="UNKNOWN"/>
		<enumval name="BROKEN_DOWN_VEHICLE"/>
		<enumval name="LOOKING_IN_BOOT"/>
		<enumval name="DELIVERY_DRIVER"/>
		<enumval name="SMOKE_THEN_DRIVE_AWAY"/>
		<enumval name="LEANING"/>
	</enumdef>

	<enumdef type="eLookAtImportance">
		<enumval name="MEDIUM"/>
		<enumval name="HIGH"/>
		<enumval name="LOW"/>
	</enumdef>

</ParserSchema>
