//
// task/Scenario/ScenarioPointGroup.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "task/Scenario/ScenarioPointGroup.h"
#include "optimisations.h"

AI_OPTIMISATIONS()

//-----------------------------------------------------------------------------

CScenarioPointGroup::CScenarioPointGroup()
		: m_EnabledByDefault(false)
{
}

//-----------------------------------------------------------------------------

// End of file 'task/Scenario/ScenarioPointGroup.cpp'
