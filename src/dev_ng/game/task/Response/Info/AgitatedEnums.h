#ifndef INC_AGITATED_ENUMS_H
#define INC_AGITATED_ENUMS_H

////////////////////////////////////////////////////////////////////////////////
// AgitatedType
////////////////////////////////////////////////////////////////////////////////

enum AgitatedType
{
	AT_None = -1,
	AT_Insulted,
	AT_Bumped,
	AT_BumpedByVehicle,
	AT_BumpedInVehicle,
	AT_Dodged,
	AT_DodgedVehicle,
	AT_HonkedAt,
	AT_RevvedAt,
	AT_Harassed,
	AT_Hostile,
	AT_Loitering,
	AT_Following,
	AT_BecomingArmed,
	AT_TerritoryIntruded,
	AT_Intimidate,
	AT_Ranting,
	AT_Intervene,
	AT_MeleeLockon,
	AT_GunAimedAt,
	AT_Griefing,
};

#endif // INC_AGITATED_ENUMS_H
