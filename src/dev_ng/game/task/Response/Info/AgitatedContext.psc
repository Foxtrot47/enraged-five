<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">
	
	<structdef type="CAgitatedContext">
		<string name="m_Response" type="atHashString" />
		<array name="m_Situations" type="atArray">
			<struct type="CAgitatedSituation" />
		</array>
	</structdef>
  
</ParserSchema>
