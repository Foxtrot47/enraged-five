<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<structdef type="CAgitatedSituation">
		<pointer name="m_Condition" type="CAgitatedCondition" policy="owner"/>
		<string name="m_Response" type="atHashString" />
	</structdef>
  
</ParserSchema>
