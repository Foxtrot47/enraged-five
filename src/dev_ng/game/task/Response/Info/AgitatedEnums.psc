<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<enumdef type="AgitatedType">
		<enumval name="AT_Insulted"/>
		<enumval name="AT_Bumped"/>
		<enumval name="AT_BumpedByVehicle"/>
		<enumval name="AT_BumpedInVehicle"/>
		<enumval name="AT_Dodged"/>
		<enumval name="AT_DodgedVehicle"/>
		<enumval name="AT_HonkedAt"/>
		<enumval name="AT_RevvedAt"/>
		<enumval name="AT_Harassed"/>
		<enumval name="AT_Hostile"/>
		<enumval name="AT_Loitering"/>
		<enumval name="AT_Following"/>
		<enumval name="AT_BecomingArmed"/>
		<enumval name="AT_TerritoryIntruded"/>
		<enumval name="AT_Intimidate"/>
		<enumval name="AT_Ranting"/>
		<enumval name="AT_Intervene"/>
		<enumval name="AT_MeleeLockOn"/>
		<enumval name="AT_GunAimedAt"/>
		<enumval name="AT_Griefing"/>
	</enumdef>
	  
</ParserSchema>
