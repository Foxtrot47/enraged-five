<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="CCrimeInfoManager">
    <array name="m_Crimes" type="atArray">
      <pointer type="CDefaultCrimeInfo" policy="owner"/>
    </array>
  </structdef>

</ParserSchema>
