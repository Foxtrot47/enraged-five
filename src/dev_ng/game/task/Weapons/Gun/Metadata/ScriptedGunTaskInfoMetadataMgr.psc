<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">
 
  <structdef type="CScriptedGunTaskMetadataMgr">
    <array name="m_ScriptedGunTaskInfos" type="atArray">
      <pointer type="CScriptedGunTaskInfo" policy="owner"/>
    </array>
  </structdef>

</ParserSchema>