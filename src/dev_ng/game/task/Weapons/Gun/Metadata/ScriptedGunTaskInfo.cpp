//
// Task/Weapons/Gun/Metadata/ScriptedGunTaskInfo.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

// File Header
#include "Task/Weapons/Gun/Metadata/ScriptedGunTaskInfo.h"

// Rage Headers
#include "ai/aichannel.h"
#include "fwanimation/clipsets.h"

// Parser Header
#include "ScriptedGunTaskInfo_parser.h"

// Game Headers

// Macro to disable optimisations if set
AI_OPTIMISATIONS()

////////////////////////////////////////////////////////////////////////////////

CScriptedGunTaskInfo::CScriptedGunTaskInfo()
{

}

////////////////////////////////////////////////////////////////////////////////

CScriptedGunTaskInfo::~CScriptedGunTaskInfo()
{

}

////////////////////////////////////////////////////////////////////////////////

