<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="CLadderMetadataManager">
    <array name="m_LadderData" type="atArray">
      <struct type="CLadderMetadata"/>
    </array>
  </structdef>

</ParserSchema>