<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="CSaveGameMigrationMetadata" preserveNames="true">
  <string name="m_LastCompletedMissionLocalizedText" type="atString" />
  <float name="m_fCompletionPercentage"/>
  <u32 name="m_SavePosixTime"/>
  <u32 name="m_HashOfTextKeyOfLastCompletedMission"/>
  <u32 name="m_CompressedSize"/>
  <u32 name="m_UncompressedSize"/>
</structdef>

</ParserSchema>
