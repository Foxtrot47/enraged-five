#if 0 //Removing file

// 
// audio/pedaudioentity.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
// 

#include "gtaaudioentity.h"
#include "audioengine/widgets.h"

audStringHash g_UtilEnvelopeHash("UTIL_ENVELOPE");
audStringHash g_BaseCategoryHash("BASE");

namespace northAudio
{
	audStringHash g_Boat("BOAT");
	audStringHash g_Vehicle("VEHICLE");
	audStringHash g_Driver("DRIVER");
	audStringHash g_UpperClothing("UPPER_CLOTHING");
	audStringHash g_LowerClothing("LOWER_CLOTHING");
	audStringHash g_Weapon("WEAPON");
	audStringHash g_Car("CAR");
	audStringHash g_Heli("HELI");
	audStringHash g_Motorbike("MOTORBIKE");
	audStringHash g_Ped("PED");
	audStringHash g_EntityBulletImpactMaterial("BULLET_IMPACT_ENTITY_MATERIAL");
	audStringHash g_GroundBulletImpactMaterial("BULLET_IMPACT_GROUND_MATERIAL");
	audStringHash g_EntityBehindBulletImpactMaterial("BULLET_IMPACT_ENTITY_BEHIND_MATERIAL");
}

#endif

