// NOTE: This file should not have include guards.

// Second param is the hash of the flag name.

SCRIPT_AUDIO_FLAG(RadioOverlapDisabled, 0xFED7A7F)
SCRIPT_AUDIO_FLAG(AggressiveHorns, 0x20A7858F)
SCRIPT_AUDIO_FLAG(CarsStartFirstTime, 0xA11C2259)
SCRIPT_AUDIO_FLAG(BoostPlayerCarVolume, 0x8DE4700)
SCRIPT_AUDIO_FLAG(UserRadioControlDisabled, 0x989F652F)
SCRIPT_AUDIO_FLAG(FrontendRadioDisabled, 0x3C9E76BA)
SCRIPT_AUDIO_FLAG(MobileRadioInGame, 0xA805FEB0)
SCRIPT_AUDIO_FLAG(ForceSeamlessRadioSwitch, 0x4B94EA26)
SCRIPT_AUDIO_FLAG(AllowScoreAndRadio, 0x803ACD34)
SCRIPT_AUDIO_FLAG(PoliceScannerDisabled, 0x7C741226)
SCRIPT_AUDIO_FLAG(OnlyAllowScriptTriggerPoliceScanner, 0x31DB9EBD)
SCRIPT_AUDIO_FLAG(AllowPoliceScannerWhenPlayerHasNoControl, 0xDF386F18)
SCRIPT_AUDIO_FLAG(DisableAbortConversationForRagdoll, 0x669CED42)
SCRIPT_AUDIO_FLAG(DisableAbortConversationForOnFire, 0x51F22743)
SCRIPT_AUDIO_FLAG(DisableAbortConversationForDeathAndInjury, 0x2052B35C)
SCRIPT_AUDIO_FLAG(LimitAmbientRadioStations, 0x71472DC)
SCRIPT_AUDIO_FLAG(SpeechDucksScore, 0xF9928BCC)
SCRIPT_AUDIO_FLAG(ListenerReverbDisabled, 0x7ADBDD48)
SCRIPT_AUDIO_FLAG(ScriptedSpeechDuckingDisabled, 0xA959BA1A)
SCRIPT_AUDIO_FLAG(ScriptedConvListenerMaySpeak, 0xBBE89B60)
SCRIPT_AUDIO_FLAG(OnlyAllowCombatSay, 0x87A08871)
SCRIPT_AUDIO_FLAG(AllowPlayerAIOnMission, 0xED1057CE)
SCRIPT_AUDIO_FLAG(AllowBuddyAIOnMission , 0x1584AD7A)
SCRIPT_AUDIO_FLAG(GroupShootout, 0x8582cfcb)
SCRIPT_AUDIO_FLAG(TrevorRageIsOverriden, 0x7E5E2FB0)
SCRIPT_AUDIO_FLAG(UnderWaterStreamOverriden, 0xAE4F72DB)
SCRIPT_AUDIO_FLAG(WantedMusicDisabled, 0x5D16D1FA)
SCRIPT_AUDIO_FLAG(DisablePlayerBreathing, 0x6B2F4B8)
SCRIPT_AUDIO_FLAG(WantedMusicOnMission, 0x5D4CDC96)
SCRIPT_AUDIO_FLAG(DisableSniperAudio, 0x8B5A48BA)
SCRIPT_AUDIO_FLAG(ForceSniperAudio, 0x98FBD539)
SCRIPT_AUDIO_FLAG(OverridePlayerGroundMaterial, 0xD8CB0473)
SCRIPT_AUDIO_FLAG(ScriptForceMicPosition, 0x5CBB4874)
SCRIPT_AUDIO_FLAG(ScriptForceGunfightConductorIntensity, 0x2E9F93A9)
SCRIPT_AUDIO_FLAG(DisableFlightMusic, 0xD93BEA86)
SCRIPT_AUDIO_FLAG(DoNotBlipScriptedSpeech, 0x92109B7D)
SCRIPT_AUDIO_FLAG(DisableBarks, 0xB7EC9E4D)
SCRIPT_AUDIO_FLAG(RepeatLineForRagdolling, 0xCABDBB1D)
SCRIPT_AUDIO_FLAG(DontRepeatLineForPlayerRagdolling, 0xB3FD4A52)
SCRIPT_AUDIO_FLAG(OverrideElevationWind, 0x370D94E5)
SCRIPT_AUDIO_FLAG(HoldMissionCompleteWhenPrepared, 0xA0F7938F)
SCRIPT_AUDIO_FLAG(AvoidMissionCompleteDelay, 0xCBE1CE81)
SCRIPT_AUDIO_FLAG(AllowScriptedSpeechInSlowMo, 0xC27F1271)
SCRIPT_AUDIO_FLAG(ActivateSwitchWheelAudio, 0x9E3258EB)
SCRIPT_AUDIO_FLAG(AllowAmbientSpeechInSlowMo, 0x551CDA5B)
SCRIPT_AUDIO_FLAG(AllowRadioOverScreenFade, 0xCB6D663C)
SCRIPT_AUDIO_FLAG(ScriptPlayingDistantSiren, 0x7DACE87F)
SCRIPT_AUDIO_FLAG(AllowRadioDuringSwitch, 0xF9DE416F)
SCRIPT_AUDIO_FLAG(LoadMPData, 0x882E6E9E)
SCRIPT_AUDIO_FLAG(UseQuietSceneSoftVersion, 0x16B447E7)
SCRIPT_AUDIO_FLAG(PlayMenuMusic, 0xBD867739)
SCRIPT_AUDIO_FLAG(OverrideMicrophoneSettings, 0xA3A58604)
SCRIPT_AUDIO_FLAG(SuppressPlayerScubaBreathing, 0x7E046BBC)
SCRIPT_AUDIO_FLAG(IsPlayerOnMissionForSpeech, 0xD95FDB98)
SCRIPT_AUDIO_FLAG(AllowPainAndAmbientSpeechToPlayDuringCutscene, 0x5842C0ED)
SCRIPT_AUDIO_FLAG(ForceConversationInterrupt, 0x285FECC6)
REPLAY_ONLY(SCRIPT_AUDIO_FLAG(DisableReplayScriptStreamRecording, 0x9351AC43))
REPLAY_ONLY(SCRIPT_AUDIO_FLAG(DisableReplayScriptFrontendStreamRecording,0x50032E75))
SCRIPT_AUDIO_FLAG(AllowForceRadioAfterRetune, 0xAE6D0D59)
SCRIPT_AUDIO_FLAG(IsDirectorModeActive, 0xD6351785)
SCRIPT_AUDIO_FLAG(EnableHeadsetBeep, 0xD25D71BC)
SCRIPT_AUDIO_FLAG(DisableHeadsetOnBeep, 0x1F7F6423)
SCRIPT_AUDIO_FLAG(DisableHeadsetOffBeep, 0xE24C3AA6)
SCRIPT_AUDIO_FLAG(AllowCutsceneOverScreenFade, 0xBFFDD2B7)
SCRIPT_AUDIO_FLAG(SuppressRadioSwitchBeep, 0x3FCC2ABF)
SCRIPT_AUDIO_FLAG(EnableGameplayCriticalMusicEmitters, 0x2EB25AD0)
SCRIPT_AUDIO_FLAG(EnableMissileLockWarningForAllVehicles, 0x2B56D9D0)
SCRIPT_AUDIO_FLAG(PlayerOnDLCHeist4Island, 0xAE6A6DE9)
SCRIPT_AUDIO_FLAG(DisableNPCHeadsetSpeechAttenuation, 0x49D479E5)