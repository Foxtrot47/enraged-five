Project game2_lib

RootDirectory ..

ParserIgnore

IncludePath $(ProjectDir)..
IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\suite\src
IncludePath $(RAGE_DIR)\naturalmotion\include
IncludePath $(RAGE_DIR)\naturalmotion\src
IncludePath $(RAGE_DIR)\script\src
IncludePath $(RAGE_DIR)\framework\src

ForceInclude basetypes.h
ForceInclude game_config.h

Files {
	Include game/makefile.txt
	Include glassPaneSyncing/makefile.txt
	Include ik/makefile.txt
	Include modelinfo/makefile.txt
	Include Objects/makefile.txt
	Include pathserver/makefile.txt
	Include PedGroup/makefile.txt
	Include Peds/makefile.txt
	Include performance/makefile.txt
	Include pickups/makefile.txt
	Include physics/makefile.txt
	Include renderer/makefile.txt
	Include replaycoordinator/makefile.txt
	Include SaveLoad/makefile.txt
	Include SaveMigration/makefile.txt
}

Libraries {
	..\vs_project_lib_psc\game_lib_psc
}
