//
//
//
//
#ifndef __SPUPMMGR_PM_H__
#define __SPUPMMGR_PM_H__

#define PM_SPU_MEMORY_MAP_POLICYMODULE_START	(0xa00)				// The address of the start of the policy module
#define PM_SPU_MEMORY_MAP_STACK_BASE			(256*1024 - 16*3)	// The stack starts here
#define PM_SPU_MEMORY_MAP_STACK_SIZE			(24*1024)			// The stack size is 24KB

#endif //__SPUPMMGR_PM_H__...


