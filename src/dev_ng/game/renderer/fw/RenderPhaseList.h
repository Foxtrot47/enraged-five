
//**********************************
// RenderPhaseList.h
// (C) Rockstar North Ltd.
// description:	Hi level renderer - abstracts rendering into a platform independent set of phases.
// Started - 11/2005 - Derek Ward
//**********************************
// History
// Created 11/2005 - Derek Ward
//**********************************

#ifndef INC_RENDERPHASELIST_H_
#define INC_RENDERPHASELIST_H_

// C++ hdr
// Rage hdrs
#include "atl/array.h"
// Core hdrs
#include "Renderer/RenderSettings.h" 

// GTA hdrs
#include "debug\debug.h"

// Frefs
class CRenderPhase;
class CViewport;


#endif //INC_RENDERPHASELIST_H_

