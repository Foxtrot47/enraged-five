#if __XENON || __PS3 
#define DYNAMICGRIDELEMENTS	(128)
#else
#define DYNAMICGRIDELEMENTS (256)
#endif

#if RSG_ORBIS
#define BUMPRTSIZE 512
#else
#define BUMPRTSIZE 256
#endif

#define WAVETEXTURERES		(128)

#define DYNAMICGRIDSIZE		(2)

