<?xml version="1.0"?>

<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
 generate="psoChecks">	

<structdef type="TerrainTessellationParameters" layoutHint="novptr" simple="true">
	<int name="m_PixelsPerVertex"/>
	<float name="m_WorldTexelUnit"/>
	<Vector2 name="m_NearNoise"/>
	<Vector2 name="m_FarNoise"/>
	<Vector4 name="m_Envelope"/>
	<float name="m_UseNearWEnd"/>
</structdef>

</ParserSchema>
