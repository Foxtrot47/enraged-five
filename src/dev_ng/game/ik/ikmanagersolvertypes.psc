<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
							generate="class">

 <enumdef type="IKManagerSolverTypes::Flags" generate="bitset">
  <enumval name="ikSolverTypeArm"/>
  <enumval name="ikSolverTypeBodyLook"/>
  <enumval name="ikSolverTypeBodyRecoil"/>
  <enumval name="ikSolverTypeHead"/>
  <enumval name="ikSolverTypeLeg"/>
  <enumval name="ikSolverTypeTorso"/>
  <enumval name="ikSolverTypeTorsoReact"/>
  <enumval name="ikSolverTypeTorsoVehicle"/>
  <enumval name="ikSolverTypeRootSlopeFixup"/>
  <!--<enumval name="ikSolverTypeQuadLeg"/>-->
  <!--<enumval name="ikSolverTypeQuadrupedReact"/>-->
  <enumval name="ikSolverTypeCount"/>
</enumdef>

</ParserSchema>
