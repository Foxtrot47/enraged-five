
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 normal tangent0 
	#define PRAGMA_DCL
	#define ISOLATE_8
#endif
//
// gta_glass_emissivenight shader - night-only emissive shader;
//
// 2006/09/04 - Andrzej:	- initial;
//
//
//
#define USE_EMISSIVE_NIGHTONLY

#include "glass_emissive.fx"

 
