
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 normal tangent0 
	#define PRAGMA_DCL
	#define USE_ANIMATED_UVS			// add AnimationUV support (define before gta_common.h)
	#define ISOLATE_8
#endif
//
// gta_glass_emissive shader;
//
// 2006/09/04 - Andrzej:	- initial;
//
//
//
#define USE_EMISSIVE

#include "glass.fx"

