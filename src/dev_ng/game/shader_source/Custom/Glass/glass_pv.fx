#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 normal tangent
	#define PRAGMA_DCL
	#define DISABLE_ALPHA_DOF
#endif

#define USE_PV_AMBIENT_BAKES

#include "glass_pv_env.fx"
