
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 normal tangent0 
	#define PRAGMA_DCL
#endif
//
// vehicle_cutout.fx shader;
//
// props: spec/bump/dirt/damage
//
// 10/11/2016 - Andrzej: - initial;
//
//

#define USE_SPECULAR
#define USE_NORMAL_MAP
#define USE_DEFAULT_TECHNIQUES
#define CUTOUT_SHADER

#define USE_VEHICLE_DAMAGE

//#define VEHCONST_SPECTEXTILEUV		(1.0f)

#include "vehicle_common.fxh"

 
