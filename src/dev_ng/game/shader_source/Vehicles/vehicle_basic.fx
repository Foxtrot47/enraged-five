
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 normal tangent0 
	#define PRAGMA_DCL
#endif
//
// gta_vehicle_basic.fx shader;
//
// props: spec/reflect/bump/No damage
//
// 10/01/2007 - Andrzej: - initial;
//
//

#define USE_SPECULAR
#define USE_NORMAL_MAP
#define USE_DEFAULT_TECHNIQUES

#define USE_VEHICLE_DAMAGE

//#define VEHCONST_SPECTEXTILEUV		(1.0f)

#include "vehicle_common.fxh"
 
