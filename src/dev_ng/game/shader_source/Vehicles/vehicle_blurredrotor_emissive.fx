
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 texcoord1 normal
	#define PRAGMA_DCL
#endif
//
// vehicle_blurredrotor_emissive.fx shader;
//
//
// 14/09/2017 - Andrzej: - initial;
//
//
#define USE_EMISSIVE
#include "vehicle_blurredrotor.fx"
