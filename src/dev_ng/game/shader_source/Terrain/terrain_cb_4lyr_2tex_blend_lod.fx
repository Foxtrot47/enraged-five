//
// GTA terrain Combo shader (up to 8 layers using a lookup texture, multitexture):
//

#define SPECULAR 1
#define NORMAL_MAP 0
#define USE_DOUBLE_LOOKUP 1
#define USE_ALPHACLIP_FADE
#define LAYER_COUNT 4

#include "terrain_cb_common.fxh"
