
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse specular texcoord0 texcoord3 normal tangent0
	#define PRAGMA_DCL
	#define USE_EDGE_WEIGHT 1
#endif

#define PARALLAX_MAP 1

#include "terrain_cb_w_4lyr_spec.fx"
