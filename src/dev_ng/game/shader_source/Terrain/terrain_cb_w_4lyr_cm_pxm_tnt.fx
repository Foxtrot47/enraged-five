
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 texcoord1 texcoord3 normal tangent0
	#define PRAGMA_DCL
	#define USE_EDGE_WEIGHT 1
#endif

#define PARALLAX_MAP 1

#include "terrain_cb_w_4lyr_cm_tnt.fx"
