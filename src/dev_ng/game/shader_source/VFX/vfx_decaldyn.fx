//
// vfx_decaldyn.fx - shader for immediate mode projected textures
//

#ifndef PRAGMA_DCL
	#pragma dcl position
	#define PRAGMA_DCL
#endif

#define USE_PARALLAX_MAP
#define USE_EMISSIVE

#include "vfx_decaldyn.fxh"
