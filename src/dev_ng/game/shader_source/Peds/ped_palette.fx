
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse specular texcoord0 texcoord1 normal tangent0 
	#define PRAGMA_DCL
#endif
//
// gta_ped_palette shader:
//
//	2008/07/04	- Andrzej:		- initial ("born on 4th of July" :-);
//
//
//
#define USE_SPECULAR
#define USE_NORMAL_MAP

#define USE_PED_COLOR_VARIATION_TEXTURE
#define USE_PED_CPV_WIND
#define USE_PED_DAMAGE_TARGETS

#define USE_DEFAULT_TECHNIQUES
#define USE_UI_TECHNIQUES

#include "ped_common.fxh"

 
