
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse specular texcoord0 texcoord1 normal tangent0 
	#define PRAGMA_DCL
#endif
//
// ped_cloth_enveff shader:
//
//	2014/07/08	- Andrzej:		- initial;
//
//
#define USE_SPECULAR
#define USE_NORMAL_MAP
#define USE_PEDSHADER_SNOW
#define USE_PED_CPV_WIND
#define USE_PED_DAMAGE_TARGETS 
#define USE_PED_DAMAGE_NO_SKIN

#define USE_PED_CLOTH

#define USE_DEFAULT_TECHNIQUES
#define USE_UI_TECHNIQUES

#include "ped_common.fxh"
 
