
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 normal 
	#define PRAGMA_DCL
#endif
//
// gta_ped_default_palette shader:
//
//	2008/07/10	- Andrzej:		- cheap palettized shader for ped LODs;
//
//
//
#define USE_PED_BASIC_SHADER

#define USE_PED_COLOR_VARIATION_TEXTURE

#define USE_DEFAULT_TECHNIQUES
#define USE_UI_TECHNIQUES

#include "ped_common.fxh"

 
