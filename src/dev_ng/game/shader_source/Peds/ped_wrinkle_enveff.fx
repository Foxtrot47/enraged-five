
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse specular texcoord0 texcoord1 normal tangent0 
	#define PRAGMA_DCL
#endif
//
// ped_wrinkle_enveff shader:
//
//	2014/05/20	- Andrzej:		- initial;
//
//
#define USE_SPECULAR
#define USE_NORMAL_MAP
#define USE_PED_WRINKLE
#define USE_PEDSHADER_SNOW
#define USE_PED_CPV_WIND
#define USE_PED_DAMAGE_TARGETS 

#define USE_DEFAULT_TECHNIQUES
#define USE_UI_TECHNIQUES

#include "ped_common.fxh"
 

