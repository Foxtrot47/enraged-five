
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 normal tangent0 
	#define PRAGMA_DCL
#endif

#define USE_WETNESS_MULTIPLIER
#define DECAL_SHADER
#include "normal_spec.fx"
