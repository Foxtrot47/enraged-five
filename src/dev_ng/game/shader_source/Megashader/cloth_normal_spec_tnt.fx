
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 normal tangent0 
	#define PRAGMA_DCL
	#define ISOLATE_8
#endif
//
//
//
//
#define USE_SPECULAR
#define USE_NORMAL_MAP

#define USE_REFLECT
#define USE_DYNAMIC_REFLECT
#define USE_DEFAULT_TECHNIQUES
#define USE_CLOTH

#define CAN_BE_ALPHA_SHADER
#define CAN_BE_CUTOUT_SHADER
#define USE_BACKLIGHTING_HACK

#define USE_PALETTE_TINT

#include "../Megashader/megashader.fxh"


 
