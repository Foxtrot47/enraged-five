#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 normal 
	#define PRAGMA_DCL
	#define USE_ANIMATED_UVS		// add AnimationUV support (define before gta_common.h)
	#define USE_UI_TECHNIQUES
	#define USE_WETNESS_MULTIPLIER
#endif

#define USE_TERRAIN_WETNESS

#include "default.fx"
