
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 normal 
	#define PRAGMA_DCL
	#define ISOLATE_8
	#define USE_UI_TECHNIQUES
#endif
//
// gta_emissivenight - night-only emissive shader;
//
// 2006/09/05 - Andrzej: - initial;
// 
//
//
#define USE_EMISSIVE
#define USE_EMISSIVE_NIGHTONLY
#define USE_DONT_TRUST_DIFFUSE_ALPHA

#include "default.fx"
 
