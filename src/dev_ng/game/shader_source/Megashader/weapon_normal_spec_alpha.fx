//
// weapon_normal_spec_alpha - normal_spec with sophisticated forward renderstate;
//
// 2014/08/22	- Andrzej:	- initial;
//
//
#define ALPHA_SHADER
#define ALPHA_SHADER_DISABLE_ZWRITE		// disables z-write in forward pass

#include "normal_spec.fx"
