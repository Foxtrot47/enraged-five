
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 normal 
	#define PRAGMA_DCL
	#define USE_ANIMATED_UVS		// add AnimationUV support (define before gta_common.h)
	#define USE_UI_TECHNIQUES
#endif

#define USE_EMISSIVE
#define USE_DONT_TRUST_DIFFUSE_ALPHA

#include "default.fx"
