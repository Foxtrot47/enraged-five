//
// weapon_emissivestrong_alpha - emissivestrong with sophisticated forward renderstate;
//
// 2014/08/22	- Andrzej:	- initial;
//
//
#define ALPHA_SHADER
#define ALPHA_SHADER_DISABLE_ZWRITE		// disables z-write in forward pass

#include "emissivestrong.fx"
