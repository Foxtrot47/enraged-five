
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 normal tangent0 
	#define PRAGMA_DCL
#endif
//
//
// Configure the megashder
//
#define USE_REFLECT
#define USE_NORMAL_MAP
#define USE_DEFAULT_TECHNIQUES
#define USE_ALPHACLIP_FADE

#include "../Megashader/megashader.fxh"

 
