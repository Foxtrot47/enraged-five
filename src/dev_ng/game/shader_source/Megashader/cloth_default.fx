
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 normal 
	#define PRAGMA_DCL
	//#define USE_ANIMATED_UVS		// add AnimationUV support (define before gta_common.h)
#endif
// Configure the megashder
#define USE_DEFAULT_TECHNIQUES
#define USE_CLOTH
#define USE_BACKLIGHTING_HACK
#define USE_ALPHACLIP_FADE

#include "../Megashader/megashader.fxh"
 
