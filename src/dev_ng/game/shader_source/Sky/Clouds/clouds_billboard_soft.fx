// 
// clouds_billboard_soft.fx
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
//

#ifndef PRAGMA_DCL
#pragma dcl position diffuse texcoord0 texcoord1
#define PRAGMA_DCL
#endif

#define USE_BILLBOARD

#include "clouds_soft.fx"


