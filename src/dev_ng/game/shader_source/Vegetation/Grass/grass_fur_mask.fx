
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse texcoord0 texcoord1 texcoord2 normal tangent0
	#define PRAGMA_DCL
#endif
// 
//	grass_fur_mask.fx
//
//	2013/10/28	-	Andrzej:	- initial;
//
//
//
//
//
#define	 USE_GRASS_FUR_MASK
#define  USE_GRASS_FUR_HF_DIFFUSE		// use additional 'high-frequency' diffuse texture
#include "grass_fur.fx"
