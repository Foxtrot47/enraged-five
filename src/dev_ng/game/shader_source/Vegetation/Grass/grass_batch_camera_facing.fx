#ifndef PRAGMA_DCL
	#pragma dcl position diffuse specular texcoord0 binormal
	// Binormal 0 - Centre XYZ
	#define PRAGMA_DCL
#endif

#define USE_CAMERA_FACING

#include "grass_batch.fx"
