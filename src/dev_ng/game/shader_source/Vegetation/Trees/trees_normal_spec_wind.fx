
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse specular texcoord0 normal tangent0
	#define PRAGMA_DCL
#endif
// 
//	trees_normal_spec.fx
//
//	2009/11/30	-	Andrzej:	- initial;
//
//
//
//
#define USE_NORMAL_MAP
#define USE_SPECULAR
#define USE_ALPHACLIP_FADE
#define USE_WIND_DISPLACEMENT

#include "trees_common.fxh"
