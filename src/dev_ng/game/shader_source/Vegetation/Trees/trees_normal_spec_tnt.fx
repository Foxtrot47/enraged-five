
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse specular texcoord0 normal tangent0
	#define PRAGMA_DCL
#endif
// 
//	trees_normal_spec_tnt.fx
//
//	2013/06/27 - Oscar V:	- initial;
//	2013/12/04 - Andrzej:	- up and running;
//
//
//
//
#define USE_NORMAL_MAP
#define USE_SPECULAR
#define USE_ALPHACLIP_FADE
#define USE_PALETTE_TINT

#include "trees_common.fxh"
