
#ifndef PRAGMA_DCL
	#pragma dcl position diffuse specular texcoord0 normal
	#define PRAGMA_DCL
#endif
// 
//	trees_lod.fx
//
//	2011/03/10	-	Andrzej:	- initial;
//
//

#define USE_INSTANCED
#define USE_TREE_LOD
#define USE_ALPHACLIP_FADE

#include "trees_common.fxh"
