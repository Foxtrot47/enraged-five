#ifndef _REPLAYMGRPROXY_H_
#define _REPLAYMGRPROXY_H_

#include "ReplaySettings.h"

#if GTA_REPLAY

class CReplayMgrProxy
{
public:
	static bool				IsEditModeActive();
};

#endif // GTA_REPLAY

#endif // _REPLAYMGRPROXY_H_
