<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<structdef type="CWeaponFallOffModifier">
		<float name="m_RangeModifier" init="1.0f"/>
		<float name="m_DamageModifier" init="1.0f"/>
	</structdef>

</ParserSchema>
