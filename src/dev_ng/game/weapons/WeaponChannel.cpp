//
// weapons/weaponchannel.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

// File header
#include "weapons/weaponchannel.h"

// Rage headers
#include "system/param.h"

RAGE_DEFINE_CHANNEL(weapon)

RAGE_DEFINE_CHANNEL(weaponitem)

