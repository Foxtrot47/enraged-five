// File header
#include "Weapons/WeaponNetworkModifiers.h"

// Macro to disable optimisations if set
WEAPON_OPTIMISATIONS()

//////////////////////////////////////////////////////////////////////////
// CWeaponNetworkModifiers
//////////////////////////////////////////////////////////////////////////

/* Deprecated ... please adjust through weapons.meta ... 
// Network static members
dev_float CWeaponNetworkModifiers::ms_fNetPlayerRammedByCarModifier  = 5.0f;
dev_float CWeaponNetworkModifiers::ms_fNetPedRammedByCarModifier     = 1.0f;
dev_float CWeaponNetworkModifiers::ms_fNetPlayerRunOverByCarModifier = 3.0f;
dev_float CWeaponNetworkModifiers::ms_fNetPedRunOverByCarModifier    = 1.0f;
dev_float CWeaponNetworkModifiers::ms_fNetPlayerExplosionModifier    = 1.0f;
dev_float CWeaponNetworkModifiers::ms_fNetPedExplosionModifier       = 1.0f;
dev_float CWeaponNetworkModifiers::ms_fNetPlayerUziDriveByModifier   = 1.0f;
dev_float CWeaponNetworkModifiers::ms_fNetPedUziDriveByModifier      = 1.0f;
dev_float CWeaponNetworkModifiers::ms_fNetPlayerDrowningModifier     = 1.0f;
dev_float CWeaponNetworkModifiers::ms_fNetPedDrowningModifier        = 1.0f;
dev_float CWeaponNetworkModifiers::ms_fNetPlayerFallModifier         = 1.0f;
dev_float CWeaponNetworkModifiers::ms_fNetPedFallModifier            = 1.0f;
*/
