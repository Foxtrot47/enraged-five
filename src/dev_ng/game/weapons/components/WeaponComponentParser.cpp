//
// weapons/weaponcomponentparser.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

WEAPON_OPTIMISATIONS()

// Game headers
#include "Weapons/Components/WeaponComponentClip.h"
#include "Weapons/Components/WeaponComponentFlashLight.h"
#include "Weapons/Components/WeaponComponentGroup.h"
#include "Weapons/Components/WeaponComponentLaserSight.h"
#include "Weapons/Components/WeaponComponentManager.h"
#include "Weapons/Components/WeaponComponentProgrammableTargeting.h"
#include "Weapons/Components/WeaponComponentReloadData.h"
#include "Weapons/Components/WeaponComponentScope.h"
#include "Weapons/Components/WeaponComponentSuppressor.h"
#include "Weapons/Components/WeaponComponentVariantModel.h"
#include "Weapons/Info/WeaponSwapData.h"

// Parser header
#include "WeaponComponentParser_parser.h"

