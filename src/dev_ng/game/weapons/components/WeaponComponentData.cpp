//
// weapons/weaponcomponentdata.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

// File header
#include "Weapons/Components/WeaponComponentData.h"

WEAPON_OPTIMISATIONS()
INSTANTIATE_RTTI_CLASS(CWeaponComponentData,0x1F58DDE4);

////////////////////////////////////////////////////////////////////////////////
