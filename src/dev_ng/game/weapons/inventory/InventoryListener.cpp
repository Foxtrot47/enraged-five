//
// weapons/inventorylistener.cpp
//
// Copyright (C) 1999-2010 Rockstar North.  All Rights Reserved. 
//

// File header
#include "Weapons/Inventory/InventoryListener.h"

// Macro to disable optimisations if set
WEAPON_OPTIMISATIONS()
