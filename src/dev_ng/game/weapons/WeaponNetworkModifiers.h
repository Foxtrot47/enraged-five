#ifndef WEAPON_NETWORK_MODIFIERS_H
#define WEAPON_NETWORK_MODIFIERS_H

//////////////////////////////////////////////////////////////////////////
// CWeaponNetworkModifiers
//////////////////////////////////////////////////////////////////////////

/* Deprecated ... please adjust through weapons.meta ... 
class CWeaponNetworkModifiers
{
public:

	// Network modifiers for additional weapon types not stored in the weapons array
	static dev_float ms_fNetPlayerRammedByCarModifier;   // multiplier applied to damage value when applying to network players
	static dev_float ms_fNetPedRammedByCarModifier;      // multiplier applied to damage value when applying to network peds
	static dev_float ms_fNetPlayerRunOverByCarModifier;  // multiplier applied to damage value when applying to network players
	static dev_float ms_fNetPedRunOverByCarModifier;     // multiplier applied to damage value when applying to network peds
	static dev_float ms_fNetPlayerExplosionModifier;     // multiplier applied to damage value when applying to network players
	static dev_float ms_fNetPedExplosionModifier;        // multiplier applied to damage value when applying to network peds
	static dev_float ms_fNetPlayerUziDriveByModifier;    // multiplier applied to damage value when applying to network players
	static dev_float ms_fNetPedUziDriveByModifier;       // multiplier applied to damage value when applying to network peds
	static dev_float ms_fNetPlayerDrowningModifier;      // multiplier applied to damage value when applying to network players
	static dev_float ms_fNetPedDrowningModifier;         // multiplier applied to damage value when applying to network peds
	static dev_float ms_fNetPlayerFallModifier;          // multiplier applied to damage value when applying to network players
	static dev_float ms_fNetPedFallModifier;             // multiplier applied to damage value when applying to network peds
};
*/

#endif // WEAPON_NETWORK_MODIFIERS_H
