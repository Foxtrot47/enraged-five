<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<structdef type="CVehicleWeaponInfo">
		<string name="m_Name" type="atHashString"/>
		<float name="m_KickbackAmplitude"/>
		<float name="m_KickbackImpulse"/>
		<float name="m_KickbackOverrideTiming" init="0.0f"/>
	</structdef>
</ParserSchema>
