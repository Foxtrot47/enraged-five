//
// weapons/vehicleweaponinfo.cpp
//
// Copyright (C) 1999-2010 Rockstar North.  All Rights Reserved. 
//

// File header
#include "Weapons/Info/VehicleWeaponInfo.h"

// Parser headers
#include "VehicleWeaponInfo_parser.h"

// Macro to disable optimisations if set
WEAPON_OPTIMISATIONS()

////////////////////////////////////////////////////////////////////////////////
// CVehicleWeaponInfo
////////////////////////////////////////////////////////////////////////////////
