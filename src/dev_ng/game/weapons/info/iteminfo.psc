<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<structdef type="CItemInfo" onPostLoad="OnPostLoad">
		<string name="m_Name" type="atHashString" ui_key="true"/>
		<string name="m_Model" type="atHashString"/>
		<string name="m_Audio" type="atHashString"/>
		<string name="m_Slot" type="atHashString"/>
	</structdef>

</ParserSchema>
