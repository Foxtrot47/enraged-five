@echo off
setlocal
if "%1"=="VS2010" goto VS2010
%RS_TOOLSROOT%\script\coding\projbuild\helper.rb --project=game --in_format=ragegen --unity_build_enabled type=project template=templates\tester_2005
if "%1"=="VS2008" goto END
:VS2010
call %RS_TOOLSROOT%\script\util\projgen\rebuildGameLibrary.bat make.txt %2 %3
call %RS_TOOLSROOT%\script\util\projgen\rebuildGameLibrary.bat game.slndef %2 %3
:END
