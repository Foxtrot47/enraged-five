@echo off

setlocal
pushd "%~dp0"

pushd ..\..
set RS_CODEBRANCH=%cd%
popd

CALL setenv.bat

pushd ..\..\rage
set RAGE_DIR=%CD%
popd

set RS_BUILDBRANCH=%RS_TITLE_UPDATE_NG%
REM Detect if current SDK is either unset or older than 430 and switch.
IF NOT EXIST %SCE_PS3_ROOT%\info\old\420.001\Bugfix_SDK_e.txt set SCE_PS3_ROOT=X:/ps3sdk/dev/usr/local/430_001/cell

ECHO LOAD_SLN ENVIRONMENT
ECHO RAGE_DIR:               %RAGE_DIR%
ECHO SCE_PS3_ROOT:           %SCE_PS3_ROOT%
ECHO END LOAD_SLN ENVIRONMENT

start "" %cd%\game_2010_unity.sln

popd
