@echo off
if "%1"=="" goto usage

SETLOCAL

SET X=0
SET P=0
SET W=0

SET B=0
SET R=0
SET F=0
SET BR=0
SET PR=0
SET SLN=game_2008.sln
SET PAU=0
SET RB=
SET SH=0
SET NOEXIT=0

FOR %%I IN (%*) DO (
	if /I %%I EQU 360 SET X=1
	if /I %%I EQU xbox SET X=1
	if /I %%I EQU xbox360 SET X=1

	if /I %%I EQU ps3 SET P=1
	if /I %%I EQU psn SET P=1
	
	if /I %%I EQU win32 SET W=1

	if /I %%I EQU beta SET B=1
	if /I %%I EQU bankrelease SET BR=1
	if /I %%I EQU release SET R=1
	if /I %%I EQU final SET F=1
	if /I %%I EQU profile SET PR=1
	if /I %%I EQU shaders SET SH=1

	if /I %%I EQU /rebuild SET RB=/rebuild
	if /I %%I EQU /clean SET RB=/clean

	if /I %%I EQU /unity SET SLN=game_2008_unity.sln

	if /I %%I EQU both SET X=1
	if /I %%I EQU both SET P=1

	if /I %%I EQU all SET B=1
	if /I %%I EQU all SET BR=1
	if /I %%I EQU all SET R=1
	if /I %%I EQU all SET F=1
	if /I %%I EQU all SET PR=1

	if /I %%I EQU /pause SET pau=1

	if /I %%I EQU /noexit SET NOEXIT=1
)

if %X%%P%%W%==000 goto usage
if %B%%BR%%R%%F%%PR%%SH%==000000 goto usage	  

set LOGFILE=batch_gta5.txt
if exist %LOGFILE% del %LOGFILE%

set COUNT=0
set COMPLETED=0
set PCT=0
set TITLE=GTA5 Batch
if %B%==1 SET TITLE=%TITLE% Beta
if %BR%==1 SET TITLE=%TITLE% BankRelease
if %R%==1 SET TITLE=%TITLE% Release
if %F%==1 SET TITLE=%TITLE% Final
if %PR%==1 SET TITLE=%TITLE% Profile
if %SH%==1 SET TITLE=%TITLE% Shaders
if %B%%BR%%R%%F%%PR%==11111 SET TITLE=GTA5 Batch All
set TITLE=%TITLE% (
if %X%==1 set TITLE=%TITLE% 360	
if %X%%P%==11 set TITLE=%TITLE% +	
if %P%==1 set TITLE=%TITLE% PS3
if %W%==1 if not %X%%P%==00 set TITLE=%TITLE% +		
if %W%==1 set TITLE=%TITLE% Win32
set TITLE=%TITLE% )

SET /A COUNT=%COUNT% + %B%
SET /A COUNT=%COUNT% + %BR%
SET /A COUNT=%COUNT% + %R%
SET /A COUNT=%COUNT% + %F%
SET /A COUNT=%COUNT% + %PR%
SET /A COUNT=%COUNT% + %SH%
if %X%%P%==11 SET /A COUNT=%COUNT% * 2
title %TITLE% (building %COUNT% targets...)

echo Building %SLN% %RB%
if %X%%B%==11 call :build_360 Beta
if %X%%BR%==11 call :build_360 BankRelease
if %X%%R%==11 call :build_360 Release
if %X%%F%==11 call :build_360 Final
if %X%%PR%==11 call :build_360 Profile
if %X%%SH%==11 call :build_360 Shaders

if %P%%B%==11 call :build_ps3 Beta
if %P%%BR%==11 call :build_ps3 BankRelease
if %P%%R%==11 call :build_ps3 Release
if %P%%F%==11 call :build_ps3 Final
if %P%%PR%==11 call :build_ps3 Profile
if %P%%SH%==11 call :build_ps3 Shaders

if %W%%B%==11 call :build_win32 Beta
if %W%%BR%==11 call :build_win32 BankRelease
if %W%%R%==11 call :build_win32 Release
if %WP%%F%==11 call :build_win32 Final
if %W%%PR%==11 call :build_win32 Profile
if %W%%SH%==11 call :build_win32 Shaders

echo Done.
title %TITLE% (complete)
grep ": warning" %LOGFILE% | grep -v "Microcode Compiler"
grep ": error" %LOGFILE%
if %PAU%==1 pause

goto :EOF 

:build_360 
SET CFG=%1
SET LOCALSLN=%SLN%
if %1==Shaders set LOCALSLN="..\shader_source\VS_Project\shaders_2008.sln"  
if %1==Shaders set CFG=BankRelease
echo BuildConsole.exe %LOCALSLN% /CFG="%CFG%|Xbox 360" %RB%
title %TITLE% [360 %1] (%PCT%%% complete)
BuildConsole.exe %LOCALSLN% /CFG="%CFG%|Xbox 360" %RB% | tee --append %LOGFILE%
SET /A COMPLETE=%COMPLETE%+1
SET /A PCT = (%COMPLETE% * 100 ) / %COUNT%
title %TITLE% (%PCT%%% complete)
if %NOEXIT%==0 exit/b

:build_ps3
SET CFG=%1
SET PROJ=game
if %1==Shaders set PROJ=shaders_2008 
if %1==Shaders set CFG=BankRelease
echo "vsibuild" %SLN% %RB% "%CFG%|PS3SNC" /incredi
title %TITLE% [PS3 %1] (%PCT%%% complete)
vsibuild %SLN% %RB% "%CFG%|PS3SNC" /project %PROJ% /incredi | tee --append %LOGFILE%
SET /A COMPLETE=%COMPLETE%+1
SET /A PCT = (%COMPLETE% * 100 ) / %COUNT%
title %TITLE% (%PCT%%% complete)
if %NOEXIT%==0 exit/b

:build_win32 
SET CFG=%1
SET LOCALSLN=%SLN%
if %1==Shaders set LOCALSLN="..\shader_source\VS_Project\shaders_2008.sln"  
if %1==Shaders set CFG=BankRelease
echo BuildConsole.exe %SLN% /CFG="%CFG%|Win32" %RB%
title %TITLE% [Win32 %1] (%PCT%%% complete)
BuildConsole.exe %LOCALSLN% /CFG="%CFG%|Win32" %RB% | tee --append %LOGFILE%
SET /A COMPLETE=%COMPLETE%+1
SET /A PCT = (%COMPLETE% * 100 ) / %COUNT%
title %TITLE% (%PCT%%% complete)
if %NOEXIT%==0 exit/b
		 

:usage
	echo usage: batch_gta5 [platforms configs flags]
	echo platforms: ps3 psn xbox 360 xbox360 both win32 
	echo configs: beta bankrelease profile release final all
	echo flags: /rebuild /pause /unity
	exit/b
