@echo off
echo converting [%~1] to [%~2] . . .
grep vcproj Jimmy_2005.sln | cut -c53- > projects.txt

for /f "tokens=2 delims=, " %%P in (projects.txt) do (
	pushd %%~pP
	echo Project %%~nxP [in %%~pP] . . .
	p4 edit %%~nxP
	copy %%~nxP %%~nxP.bak
	sed "s/%~1/%~2/g" %%~nxP.bak > %%~nxP
	popd
)
