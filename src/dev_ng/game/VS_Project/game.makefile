Project game

ConfigurationType exe
TitleId 0x545408A7
AdditionalSections 545408A7=..\..\..\..\xlast\Fuzzy_live.spa

RootDirectory .

IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\suite\src
IncludePath $(RAGE_DIR)\script\src
IncludePath $(RAGE_DIR)\framework\src
IncludePath $(RAGE_DIR)\3rdParty

ForceInclude basetypes.h
ForceInclude game_config.h

Files {
	..\core\main.cpp	
	game.rc
	resource.h

	Folder Resources {
		Package.appxmanifest
		Logo.png
		SmallLogo.png
		StoreLogo.png
		SplashScreen.png
		WideLogo.png
	}
}

Libraries {

	..\VS_Project1_lib\game1_lib
	..\VS_Project2_lib\game2_lib
	..\VS_Project3_lib\game3_lib
	..\VS_Project4_lib\game4_lib
	..\VS_Project_network\network	
	..\VS_Project\RageMisc\RageMisc
	
	%RAGE_DIR%\base\src\vcproj\RageAudio\RageAudio
	%RAGE_DIR%\base\src\vcproj\RageCore\RageCore
	%RAGE_DIR%\base\src\vcproj\RageCreature\RageCreature
	%RAGE_DIR%\base\src\vcproj\RageGraphics\RageGraphics
	%RAGE_DIR%\base\src\vcproj\RageNet\RageNet
	%RAGE_DIR%\base\src\vcproj\RagePhysics\RagePhysics
    %RAGE_DIR%\base\src\vcproj\RageSec\RageSec

	%RAGE_DIR%\suite\src\vcproj\RageSuiteCreature\RageSuiteCreature
	%RAGE_DIR%\naturalmotion\vcproj\NaturalMotion\NaturalMotion
	%RAGE_DIR%\script\src\vcproj\RageScript\RageScript
	%RAGE_DIR%\framework\src\vcproj\RageFramework\RageFramework
	%RAGE_DIR%\scaleform\src\vcproj\ScaleformGfx\ScaleformGfx
	
	# Please note PS� Unity projects are not listed here, since these are conditional they are specified in the rules files.
}