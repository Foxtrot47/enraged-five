<?xml version="1.0"?>
<ParserSchema xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
							xsi:noNamespaceSchemaLocation="schemas/parsermetadata.xsd"
							generate="class">
 
  <enumdef type="eAnimSceneEntityType">
    <enumval name="ANIM_SCENE_ENTITY_NONE" />
    <enumval name="ANIM_SCENE_ENTITY_PED" />
    <enumval name="ANIM_SCENE_ENTITY_VEHICLE" />
    <enumval name="ANIM_SCENE_ENTITY_OBJECT" />
    <enumval name="ANIM_SCENE_SYNCED_SCENE_ID" />
    <enumval name="ANIM_SCENE_BOOLEAN" />
    <enumval name="ANIM_SCENE_ENTITY_CAMERA" />
  </enumdef>
</ParserSchema>
