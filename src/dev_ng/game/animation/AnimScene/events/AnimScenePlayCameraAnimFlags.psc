<?xml version="1.0"?>
<ParserSchema xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:noNamespaceSchemaLocation="schemas/parsermetadata.xsd" generate="class">
    <enumdef type="eCameraSettings" generate="bitset">
      <enumval name="ASCF_USE_SCENE_ORIGIN" description ="Use an offset from scene origin for the camera transform."/>
      <enumval name="ASCF_CATCHUP_CAM" description ="When the camera finishes, use catchup for the gameplay camera."/>
    </enumdef>    
</ParserSchema>