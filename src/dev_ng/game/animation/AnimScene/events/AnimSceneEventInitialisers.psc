<?xml version="1.0"?> 
<ParserSchema xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
							xsi:noNamespaceSchemaLocation="schemas/parsermetadata.xsd">

  <structdef type="CAnimSceneEventInitialiser" >
  </structdef>

  <structdef type="CAnimSceneEquipWeaponEventInitialiser" base="CAnimSceneEventInitialiser">
  </structdef>
  
  <structdef type="CAnimScenePlayAnimEventInitialiser" base="CAnimSceneEventInitialiser">
  </structdef>

  <structdef type="CAnimScenePlayCameraAnimEventInitialiser" base="CAnimSceneEventInitialiser">
  </structdef>

  <structdef type="CAnimSceneForceMotionStateEventInitialiser" base="CAnimSceneEventInitialiser">
  </structdef>

  <structdef type="CAnimSceneCreatePedEventInitialiser" base="CAnimSceneEventInitialiser">
  </structdef>

  <structdef type="CAnimSceneCreateObjectEventInitialiser" base="CAnimSceneEventInitialiser">
  </structdef>

  <structdef type="CAnimSceneCreateVehicleEventInitialiser" base="CAnimSceneEventInitialiser">
  </structdef>

  <structdef type="CAnimScenePlaySceneEventInitialiser" base="CAnimSceneEventInitialiser">
  </structdef>

  <structdef type="CAnimSceneInternalLoopEventInitialiser" base="CAnimSceneEventInitialiser">
  </structdef>

  <structdef type="CAnimScenePlayVfxEventInitialiser" base="CAnimSceneEventInitialiser">
  </structdef>

</ParserSchema>