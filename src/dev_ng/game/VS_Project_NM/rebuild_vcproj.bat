@echo off
setlocal

if "%1"=="VS2010" goto VS2010
REM %RS_TOOLSROOT%\script\coding\projbuild\helper.rb --project=game --in_format=rageprojbuilder type=project template=nm_tester_2008
%RS_TOOLSROOT%\script\coding\projbuild\helper.rb --project=game --in_format=ragegen --unity_build_enabled type=project template=templates\nm_tester_2008
if "%1"=="VS2008" goto END
:VS2010

REM Build the game normally...
call %RS_TOOLSROOT%\script\util\projgen\rebuildGameNMLibrary.bat make.txt %2 %3
call %RS_TOOLSROOT%\script\util\projgen\rebuildGameNMLibrary.bat game.slndef %2 %3

REM Build the Game for NM distribution...
call %RS_TOOLSROOT%\script\util\projgen\rebuildGameNMLibrary.bat makeNM.txt %2 %3
call %RS_TOOLSROOT%\script\util\projgen\rebuildGameNMLibrary.bat gameNM.slndef %2 %3

:END

IF "%PAUSEOPTION%"=="" PAUSE
