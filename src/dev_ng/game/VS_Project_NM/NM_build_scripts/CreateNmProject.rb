# ****************************************************************************
# *** Make some local modifications to the game code before batch building ***
# *** the game libraries. Copy the game libraries to a special NM project  ***
# *** folder and create a Visual Studio project and solution file.         ***
# ****************************************************************************
#
# Author:: Richard Archibald
# Started:: 16th December 2010

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/gui/exception_dialog'
require 'pipeline/gui/messagebox'
require 'pipeline/os/path'
include Pipeline

require 'FileHelpers'
include NmBuild


#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
LOG_FILE = 'NmCreateProj.log'
GAME_NAME = 'game'
BRANCH = 'dev'
CONFIGS = # The platform configurations which Natural Motion should be able to use.
[
 'psn_beta_2008',
 'psn_bankrelease_2008'
]
CONFIG_IDS = # These are used to deduce the order of the platform configs in the build log file.
[
 'Beta|PS3SNC',
 'BankRelease|PS3SNC',
]
CONFIG_HDRS = # These are the releveant header files for the above platform configs. This is where #define NM_BUILD will go.
[
 'rage/base/src/forceinclude/psn_beta.h',
 'rage/base/src/forceinclude/psn_bankrelease.h'
]


#----------------------------------------------------------------------------
# Globals
#----------------------------------------------------------------------------
$g_AppName = ""
$proj_root = ""
$project = ""
$cl_num = 0
$bAllowRevert = true


#----------------------------------------------------------------------------
# Classes
#----------------------------------------------------------------------------
class Depot
  # Used like enums to let the perforce subroutines decide whether to act
  # on the R*North repo or the RAGE proxy.
  # NB - this assumes that .p4config files have been placed appropriately.

  NORTH = 0
  RAGE  = 1
end


#----------------------------------------------------------------------------
# Subroutines
#----------------------------------------------------------------------------
def IsCheckedOut?(filename, depot)
  msg = "Checking if #{filename} is checked out in Perforce."
  puts msg
  SafeWriteLogFile(LOG_FILE, msg)
  begin
    if depot == Depot::NORTH
      system("p4 opened > p4openfiles.tmp 2>&1")
    elsif depot == Depot::RAGE
      system("p4 -d x:/#{$project}/src/#{BRANCH}/rage opened > p4openfiles.tmp 2>&1")
    else
      msg = "Unrecognised depot selection: #{depot}. Script will terminate."
      SafeWriteLogFile(LOG_FILE, msg)
      Pipeline::GUI::MessageBox::error("#{$g_AppName}", msg)
      Kernel::exit
    end

    bCheckedOut = false
    File.open("p4openfiles.tmp") do |file|
      file.each_line do |line|
        if line.downcase.include?("#{filename.downcase}")
          puts line
          msg = "File: #{filename} is checked out in Perforce."
          SafeWriteLogFile(LOG_FILE, msg)
          bCheckedOut = true
        end
      end
    end
    
    return bCheckedOut

  ensure
    FileUtils::rm_f("p4openfiles.tmp") if File::exists?("p4openfiles.tmp")
  end
end

#----------------------------------------------------------------------------

def CreateLibs()
  # Check out the game header files and enable the NM_BUILD changes in code before
  # kicking off a batch build of the required configurations. Afterwards, the
  # log file is checked for errors and this is reported to the user.
  begin
    CONFIG_HDRS.each do |hdr|
      hdr_depot_file = "//rage/#{$project}/#{BRANCH}/#{hdr}"

      # Check if this header file is already checked out and stop if it is.
      if IsCheckedOut?(hdr_depot_file, Depot::RAGE)
        puts "... it is. Quit!"
        msg = "File: #{hdr_depot_file} is already checked out in Perforce. Script will terminate."
        SafeWriteLogFile(LOG_FILE, msg)
        Pipeline::GUI::MessageBox::error("#{$g_AppName}", msg)
        $bAllowRevert = false
        Kernel::exit
      end
    end

    # Try to check out the header files.
    CONFIG_HDRS.each do |hdr|
      hdr_depot_file = "//rage/#{$project}/#{BRANCH}/#{hdr}"
      SafeWriteLogFile(LOG_FILE, "Trying to check out #{hdr_depot_file}...")
      SafeWriteLogFile(LOG_FILE, `p4 -d x:/#{$project}/src/#{BRANCH}/rage edit #{hdr_depot_file}`)
    end

    # Create a new changelist and add the checked out files to it.
    SafeWriteLogFile(LOG_FILE, "Try to add file to a new change list.")
    File.open("cldesc.tmp", "w") do |cldesc|
      cldesc.puts "Change: new"
      cldesc.puts "Status: new"
      cldesc.puts "Description: Any files in this changelist have been checked out automatically by the NM build script and SHOULD NOT BE SUBMITTED!!"
      cldesc.printf("Files:")
      CONFIG_HDRS.each do |hdr|
        hdr_depot_file = "//rage/#{$project}/#{BRANCH}/#{hdr}"
        cldesc.puts " #{hdr_depot_file}"
      end # Loop over different header files.
    end
    if File::exists?("cldesc.tmp")
      create_cl_output = `p4 -d x:/#{$project}/src/#{BRANCH}/rage change -i < cldesc.tmp`
      SafeWriteLogFile(LOG_FILE, "output from create changelist command: #{create_cl_output}")
      split_cl_output = create_cl_output.split
      # Check validity of message.
      unless split_cl_output[0].downcase=="change" && split_cl_output[2].downcase=="created"
        msg = "ERROR: changelist not created properly. Script will terminate."
        SafeWriteLogFile(LOG_FILE, msg)
        Pipeline::GUI::MessageBox::error("#{$g_AppName}", msg)
        Kernel::exit
      end
      # Iterate over each space delineated word in the perforce command output to find the first numeric string.
      split_cl_output.each do |word|
        if word.to_i > 0
          $cl_num = word.to_i
          break
        end
      end
      puts "CL number: #{$cl_num}"
      FileUtils::rm_f("cldesc.tmp")
    else # Something must have gone wrong writing the changelist descriptor file.
      msg = "ERROR: changelist descriptor file does not exist! Script will terminate."
      SafeWriteLogFile(LOG_FILE, msg)
      Pipeline::GUI::MessageBox::error("#{$g_AppName}", msg)
      Kernel::exit
    end

    # Make the necessary changes to the headers to enable any NM build specific code changes (like dongle protection!).
    SafeWriteLogFile(LOG_FILE, "Making modifications to forceinclude header files to enable NM build specific code...")
    CONFIG_HDRS.each do |hdr|
      hdr_disk_file = "x:/#{$project}/src/#{BRANCH}/#{hdr}"
      SafeWriteLogFile(LOG_FILE, "#{hdr_disk_file}")

      bFoundHackGta4line = false
      File.open(hdr_disk_file) do |hdr_file|
        File.open("new_hdr.tmp", "w") do |temp_file|
          hdr_file.each_line do |line|
            if line.include?("#define HACK_GTA4")
              bFoundHackGta4line = true
              temp_file.puts line
              temp_file.puts "#define NM_BUILD"
            else
              temp_file.puts line
            end
          end
        end
      end

      # If we didn't find the line to enable the dongle, let the user know and exit.
      unless bFoundHackGta4line
        msg = "Couldn't find HACK_GTA4 definition line in #{hdr_disk_file}. The script will terminate."
        SafeWriteLogFile(LOG_FILE, msg)
        Pipeline::GUI::MessageBox::error("#{$g_AppName}", msg)
        puts "Terminating script."
        Kernel::exit
      end

      # Copy the modified file over the original and delete the temp copy.
      SmartCopy("new_hdr.tmp", hdr_disk_file)
      FileUtils::rm_f("new_hdr.tmp")
    end # Loop over different header files.

    # Make sure the debug symbols get stripped from the libraries (on PS3) by renaming the ps3debug info file temporarily.
    SafeWriteLogFile(LOG_FILE, "Stripping debug symbols for PS3 (i.e. copying debug text file to a temporary file).")
    SmartCopy("#{$proj_root}/src/#{BRANCH}/game/VS_Project/game_2008_debuggroups.txt", "#{$proj_root}/src/#{BRANCH}/game/VS_Project/game_2008_debuggroups.tmp")
    FileUtils::rm_f("#{$proj_root}/src/#{BRANCH}/game/VS_Project/game_2008_debuggroups.txt")

    # Kick off the batch build process.
    SafeWriteLogFile(LOG_FILE, "Starting the batch build process...")
    ENV['PAUSEOPTION'] = 'NOPAUSE'
    system("BatchBuildLibs.bat")

    # Check the build logs for errors. If there were any, notify the user and terminate the script.
    SafeWriteLogFile(LOG_FILE, "Searching build log for errors.")
    line_number = 0
    fail_lines = [] # The line numbers at which failures are detected.
    config_order = []
    broken_configs = []
    File.open("#{$proj_root}/src/#{BRANCH}/game/VS_Project_NM/NM_build_scripts/BatchBuild.log") do |file|
      file.each_line do |line|
        for i in 0..CONFIGS.size-1 do
			next if line.nil? 
          if line.downcase.include?(CONFIG_IDS[i].downcase)
            config_order += [CONFIGS[i]]
            SafeWriteLogFile(LOG_FILE, "Platform configuration #{CONFIGS[i]} detected at line number #{line_number}")
            break
          end
        end
        if line.downcase.include?('failed')
          fail_lines += [line_number] unless line.downcase.include?('0 failed')
        end
        line_number += 1
      end # Loop over each line in build log.
    end # Close build log file.
    if fail_lines.size() > 0
      for i in 0..fail_lines.size()-1 do
        broken_configs += ["#{config_order[i]}, "]
      end
      SafeWriteLogFile(LOG_FILE, "WARNING: There were errors in at least one build!")
      unless Pipeline::GUI::MessageBox::question("#{$g_AppName}", "WARNING: Some libraries may not have been built. If you continue, the following configurations may not work properly: #{broken_configs}. Do you wish to proceed?")
        puts "Aborting..."
        SafeWriteLogFile(LOG_FILE, "User decided to abort. The following configurations may not work properly:")
        broken_configs.each do |broken_config|
          SafeWriteLogFile(LOG_FILE, "#{broken_config}")
        end
        Kernel::exit
      end
      SafeWriteLogFile(LOG_FILE, "User decided to continue. The following configurations may not work properly:")
      broken_configs.each do |broken_config|
        SafeWriteLogFile(LOG_FILE, "#{broken_config}")
      end
    end

  rescue RuntimeError => ex
    GUI::ExceptionDialog::show_dialog(ex, "Exception caught while creating game libs: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "Exception caught while creating game libs: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "backtrace:\n #{ex.backtrace.join("\n")}")
    Kernel::exit
  end
end

#------------------------------------------------------------------------------

def CopyLibs
  # Copy the compiled libraries for the game into the special NM project folder.
  begin
    root_folder=$proj_root + "/src/#{BRANCH}/game"

    bSomeLibsNotCopied = false
    broken_configs = []
    
    CONFIGS.each do |config|
      bThisConfigBroken = false
      msg = "------------------------------------------------------"
      puts msg
      SafeWriteLogFile(LOG_FILE, msg)
      msg = "Copy libs for target: #{config}"
      puts msg
      SafeWriteLogFile(LOG_FILE, msg)
      msg = "------------------------------------------------------"
      puts msg
      SafeWriteLogFile(LOG_FILE, msg)

      for lib_num in 1..4 do
        puts "Copy #{GAME_NAME}#{lib_num}_lib.lib"
        src = "#{root_folder}/VS_Project#{lib_num}_lib/#{config}/#{GAME_NAME}#{lib_num}_lib.lib"
        bThisConfigBroken = true unless SmartCopy(src, "#{root_folder}/VS_Project_NM/#{GAME_NAME}#{lib_num}_lib/#{config}/")
        if File::exists?(src)
          timestamp = File::mtime(src)
          msg = "Library last-modified timestamp: #{timestamp}"
          puts msg
          SafeWriteLogFile(LOG_FILE, msg)
        end
      end # Loop over the multiple game libs.

      puts "Copying network.lib"
      src = "#{root_folder}/VS_Project_network/#{config}/network.lib"
      bThisConfigBroken = true unless SmartCopy(src, "#{root_folder}/VS_Project_NM/network/#{config}/")
      if File::exists?(src)
        timestamp = File::mtime(src)
        msg = "Library last-modified timestamp: #{timestamp}"
        puts msg
        SafeWriteLogFile(LOG_FILE, msg)
      end

      broken_configs += ["#{config}, "] if bThisConfigBroken
      bSomeLibsNotCopied = true if bThisConfigBroken
    end # Loop over platform configurations.

    if bSomeLibsNotCopied
      SafeWriteLogFile(LOG_FILE, "WARNING: Some libraries were not copied!")
      unless Pipeline::GUI::MessageBox::question("#{$g_AppName}", "WARNING: Some libraries were not copied. If you continue, the following configurations will not work properly: #{broken_configs}. Do you wish to proceed?")
        puts "Aborting..."
        SafeWriteLogFile(LOG_FILE, "User decided to abort. The following configurations will not work properly:")
        broken_configs.each do |broken_config|
          SafeWriteLogFile(LOG_FILE, "#{broken_config}")
        end
        Kernel::exit
      end
      SafeWriteLogFile(LOG_FILE, "User decided to continue. The following configurations will not work properly:")
      broken_configs.each do |broken_config|
        SafeWriteLogFile(LOG_FILE, "#{broken_config}")
      end
    end

  rescue RuntimeError => ex
    GUI::ExceptionDialog::show_dialog(ex, "Exception caught while copying game libs: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "Exception caught while copying game libs: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "backtrace:\n #{ex.backtrace.join("\n")}")
    Kernel::exit
  end
end

#------------------------------------------------------------------------------

def CreateNmProject()
  # Use the RAGE auto project generation utilities to create a project for the NM stand-alone build
  # which links the game code from pre-built libraries instead of from source code.

  begin
    msg = "========== Generating the NM project and solution files =========="
    puts msg
    SafeWriteLogFile(LOG_FILE, msg)

    Dir.chdir '..' do
      ENV['PAUSEOPTION'] = 'NOPAUSE'
      system("rebuild_vcproj.bat")
      system("rebuild_sln.bat")
    end
    puts "=================================================================="

  rescue RuntimeError => ex
    GUI::ExceptionDialog::show_dialog(ex, "Exception caught while creating the NM stand-alone project: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "Exception caught while creating the NM stand-alone project: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "backtrace:\n #{ex.backtrace.join("\n")}")
    Kernel::exit
  end
end

#------------------------------------------------------------------------------

def CleanUp()
  # Undo any changes made by the script.

  begin
    SafeWriteLogFile(LOG_FILE, "===== CleanUp =====")
    puts "Cleaning up local changes..."

    # Revert app.cpp (if checked out) and delete the temporary changelist if it exists.
    if $bAllowRevert
      CONFIG_HDRS.each do |hdr|
        hdr_depot_file = "//rage/#{$project}/#{BRANCH}/#{hdr}"

        unless IsCheckedOut?(hdr_depot_file, Depot::RAGE)
          puts "... it isn't. Uh-oh!"
          msg = "File: #{hdr_depot_file} wasn't checked out in Perforce. Something must be wrong; script will terminate."
          SafeWriteLogFile(LOG_FILE, msg)
          Pipeline::GUI::MessageBox::error("#{$g_AppName}", msg)
          Kernel::exit
        end

        SafeWriteLogFile(LOG_FILE, "Reverting temporary changelist.")
        SafeWriteLogFile(LOG_FILE, `p4 -d x:/#{$project}/src/#{BRANCH}/rage revert #{hdr_depot_file}`)
        SafeWriteLogFile(LOG_FILE, `p4 -d x:/#{$project}/src/#{BRANCH}/rage change -d #{$cl_num}`)
      end
    end

    # Make sure that the PS3 debuginfo file is replaced.
    SafeWriteLogFile(LOG_FILE, "Resetting debug symbol file for PS3.")
    if File::exists?("#{$proj_root}/src/#{BRANCH}/game/VS_Project/game_2008_debuggroups.tmp")
      SmartCopy("#{$proj_root}/src/#{BRANCH}/game/VS_Project/game_2008_debuggroups.tmp", "#{$proj_root}/src/#{BRANCH}/game/VS_Project/game_2008_debuggroups.txt")
    end
    if File::exists?("#{$proj_root}/src/#{BRANCH}/game/VS_Project/game_2008_debuggroups.txt")
      FileUtils::rm_f("#{$proj_root}/src/#{BRANCH}/game/VS_Project/game_2008_debuggroups.tmp")
    end

    SafeWriteLogFile(LOG_FILE, "Script finished.")

  rescue RuntimeError => ex
    GUI::ExceptionDialog::show_dialog(ex, "Exception caught in CleanUp(): #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "Exception caught in CleanUp(): #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "backtrace:\n #{ex.backtrace.join("\n")}")
    Kernel::exit
  end  
end


#----------------------------------------------------------------------------
# MAIN
#----------------------------------------------------------------------------
if (__FILE__ == $0) then
  begin
    $g_AppName = File::basename(__FILE__, '.rb')

    # Initialise the logging system.
    SafeCreateLogFile(LOG_FILE)

    OS::Path::set_downcase_on_normalise(false)
    $proj_root=OS::Path.normalise(ENV['RS_PROJROOT'])
    SafeWriteLogFile(LOG_FILE, "Project root: #{$proj_root}")
    $project=OS::Path.normalise(ENV['RS_PROJECT'])
    SafeWriteLogFile(LOG_FILE, "Project: #{$project}")

    # Perform each task required to create the special NM project.
    CreateLibs()
    CopyLibs()
    CreateNmProject()

    # Assume all is well if we got here.
    puts "Done. :-)"
    SafeWriteLogFile(LOG_FILE, "Script completed successfully! :)")
    Pipeline::GUI::MessageBox::information("#{$g_AppName}", "Script completed successfully! :)")

  rescue SystemExit => ex # Allows script to exit quietly after fatal exceptions.
  rescue Exception => ex
    puts "Unhandled exception: #{ex.message}"
    puts ex.backtrace.join( "\n" )
    Pipeline::GUI::ExceptionDialog::show_dialog( ex, "Unhandled exception while creating the project for the NM stand-alone build: #{ex.message}" )
    SafeWriteLogFile(LOG_FILE, "Fatal exception: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "backtrace:\n #{ex.backtrace.join("\n")}")
  ensure
    # Make sure that any changes we made are aborted on exit - even if exit was forced by error.
    CleanUp()
  end # Main try-catch block.
end # Main.
