# **************************************************************************
# *** Iterate through the build folder of the stand-alone NM build and   ***
# *** pack the files into RPFs so that they are encrypted.               ***
# *** CAVEAT - due to a bug in the RPF loading system, pack files cannot ***
# *** be nested and so the script will save all .img and .rpf files it   ***
# *** encounters within the disk_images/ folder. These files should then ***
# *** be copied over original common, ps3, xbox360, etc. folders         ***
# **************************************************************************
#
# Author:: Richard Archibald
# Started:: 12th November 2010


#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/gui/exception_dialog'
require 'pipeline/gui/log_window'
require 'pipeline/gui/messagebox'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_extract'
require 'pipeline/projectutil/data_convert'
require 'pipeline/projectutil/data_rpf'
require 'pipeline/util/rage'
include Pipeline

require 'GetNmBuildPath'
require 'FileHelpers'
include NmBuild


#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
LOG_FILE="MakeDiskImages.log"

BRANCH = "dev"


#----------------------------------------------------------------------------
# Subroutines
#----------------------------------------------------------------------------

def CreateRpfFileRecursively(src_dir, packfile, compressed=true)
  msg = "\n***** CREATING RPF: #{packfile} *****"
  puts msg
  SafeWriteLogFile(LOG_FILE, msg)
  SafeWriteLogFile(LOG_FILE, "src_dir: #{src_dir} => #{packfile}")
  SafeWriteLogFile(LOG_FILE, "#{packfile} will be compressed.") if compressed

  begin
    c = Pipeline::Config::instance()
    project = c.projects['gta5']
    project.load_config()

    src_dir.downcase!
    # Recursively add those files from the src_dir to the pack file.
    file_list = []
	puts "SRC: #{src_dir}"
	files = OS::FindEx::find_files_recurse("#{src_dir}/*.*")
    files.each do |filename|
      # Don't add file if it is itself a packfile or an image.
      file_ext = OS::Path::get_extension(filename)
      # Remove "src_dir" from the file path inside the RPF.
      filename_no_src_dir = filename.gsub("#{src_dir}/", '')
      if file_ext == 'rpf' or file_ext == 'img'
        msg = "***** EXCLUDING: " + filename
        puts msg
        SafeWriteLogFile(LOG_FILE, msg)
        path_no_src_dir = OS::Path::get_directory("/#{filename_no_src_dir}")
        CreateDirIfMissing(OS::Path::remove_extension(packfile)+"#{path_no_src_dir}")
        SmartCopy(filename, OS::Path::remove_extension(packfile)+"#{path_no_src_dir}")
        next
      end
      # Remove "src_dir" from the file path inside the RPF.
      entry = {}
	  entry[:src] = filename
	  entry[:dst] = filename_no_src_dir
	  
	  #puts "FILE: #{filename} ==> #{filename_no_src_dir}"
	  #$stdin.gets
	  
	  file_list << entry
    end
	FileUtils::mkdir_p( OS::Path::get_directory( packfile ) ) \
		unless ( File::directory?( OS::Path::get_directory( packfile ) ) )
	ProjectUtil::data_rpf_create( packfile, file_list, compressed, project.targets['ps3'] )
	
    SafeWriteLogFile(LOG_FILE, "Closed packfile: #{packfile}")

  rescue Exception => ex
    GUI::ExceptionDialog::show_dialog( ex, "Exception caught while creating pack file: #{ex.message}" )
    r.pack.close()
    Kernel::exit
  end

end # CreateReducedRpfFile()


#----------------------------------------------------------------------------
# MAIN
#----------------------------------------------------------------------------
if (__FILE__ == $0) then

  begin
    g_AppName = File::basename(__FILE__,'.rb')

    # Initialise the logging system.
    SafeCreateLogFile(LOG_FILE)

    # Get the path to the NM stand-alone build folder.
    nm_folder = GetNmBuildPath()
    SafeWriteLogFile(LOG_FILE, "nm_folder retrieved as #{nm_folder}.")
    BUILD="#{nm_folder}/build/#{BRANCH}"
    DISKIMAGES="#{nm_folder}/build/#{BRANCH}/disk_images"
    SafeWriteLogFile(LOG_FILE, "BUILD=#{BUILD}")
    SafeWriteLogFile(LOG_FILE, "DISKIMAGES=#{DISKIMAGES}")

    # Pack the common/ and platform specific data directories into RPF.
    CreateRpfFileRecursively("#{BUILD}/common", "#{DISKIMAGES}/common.rpf", false)

    # Not really any need to give NM the platform "lite" versions.
    platforms = [
                 'ps3',
                 #'xbox360',
                 #'ps3lite',
                 #'xbox360lite',
                ]
    platforms.each do |platform|
      CreateDirIfMissing("#{DISKIMAGES}/#{platform}")
      #CreateRpfFileRecursively("#{BUILD}/#{platform}/audio", "#{DISKIMAGES}/audio_#{platform}.rpf", false)
	  CreateRpfFileRecursively("#{BUILD}/#{platform}/audio", "#{DISKIMAGES}/audio.rpf", false)
      # Now that the audio folder has been packed, delete it so it doesn't get duplicated in
      # the platform packfile.
      FileUtils::rm_rf("#{BUILD}/#{platform}/audio")
      CreateRpfFileRecursively("#{BUILD}/#{platform}", "#{DISKIMAGES}/#{platform}.rpf", false)
    end # Loop to create each platform specific RPF.

    # Delete the original data folders and replace them with the new encrypted RPF versions.
    SafeWriteLogFile(LOG_FILE, "Deleting #{BUILD}/common")
    FileUtils::rm_rf("#{BUILD}/common")
    platforms.each do |platform|
      SafeWriteLogFile(LOG_FILE, "Deleting #{BUILD}/#{platform}")
      FileUtils::rm_rf("#{BUILD}/#{platform}")
    end # Loop to delete each un-encrypted platform directory.
    SafeWriteLogFile(LOG_FILE, "Copying diskimages from #{DISKIMAGES} ==> #{BUILD}...")
    disk_image_files = OS::FindEx::find_dirs("#{DISKIMAGES}")
    disk_image_files += OS::FindEx::find_files("#{DISKIMAGES}/*.*")
    disk_image_files.each do |file|
      SmartCopy("#{file}", "#{BUILD}/")
    end
    SafeWriteLogFile(LOG_FILE, "Deleting #{DISKIMAGES}")
    FileUtils::rm_rf("#{DISKIMAGES}")

    # We're good if we got here, i.e. we didn't throw an exception.
    msg = "\n\ndone. :)"
    puts msg
    SafeWriteLogFile(LOG_FILE, msg)
    Pipeline::GUI::MessageBox::information("#{g_AppName}", "Script completed successfully! :)")


    ################################ EXCEPTIONS ################################
  rescue SystemExit => ex # Allows script to exit quietly after fatal exceptions.
  rescue Exception => ex
    GUI::ExceptionDialog::show_dialog( ex, "Exception caugt in main script: #{ex.message}" )
  ensure # Do these clean up tasks whether an exception is thrown or not.
  end

end # Main
