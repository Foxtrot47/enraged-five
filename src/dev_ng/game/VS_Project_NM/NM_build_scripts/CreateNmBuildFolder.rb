# ***********************************************************************
# *** Create a new stand-alone directory tree for a new build to give ***
# *** Natural Motion and copy the required source and data there.     ***
# ***********************************************************************
#
# Author : Richard Archibald
# Started: 12/11/2010

#----------------------------------------------------------------------------
# Includes
#----------------------------------------------------------------------------
require 'pipeline/gui/exception_dialog'
require 'pipeline/gui/messagebox'
require 'pipeline/gui/application'
include Pipeline

require 'SetNmBuildPath'
require 'FileHelpers'
include NmBuild

#------------------------------------------------------------------------------
# Constants
#------------------------------------------------------------------------------
# Bit-mask values for the GUI menu system.
OPTION_GAMESRC=1
OPTION_GAMEDATA=2
OPTION_COPYTOOLS=4
OPTION_COPYRAGE=8
OPTION_COPYPS3SDK=16

LOG_FILE="NmBuild.log"

BRANCH="dev"


#------------------------------------------------------------------------------
# Globals
#------------------------------------------------------------------------------
$nm_folder=""


#------------------------------------------------------------------------------
# Objects
#------------------------------------------------------------------------------
class NmBuildMenuDialog < Wx::Dialog

  attr_reader :cbx_copy_game_code, :cbx_copy_game_data
  attr_reader :cbx_copy_tools, :cbx_copy_rage, :cbx_copy_ps3_sdk

  def initialize(title = TITLE_DEFAULT, prompt = PROMPT_DEFAULT)
    super(nil, Wx::ID_ANY, title, Wx::DEFAULT_POSITION, Wx::Size.new(300,230))

    stc_text = Wx::StaticText.new(self, Wx::ID_ANY, "Please select the tasks to be performed:")

    txt_nm_dir = Wx::StaticText.new(self, Wx::ID_ANY, "NM folder set as: #{$nm_folder}")
    @cbx_copy_game_code = Wx::CheckBox.new(self, Wx::ID_ANY, "Copy game code")
    @cbx_copy_game_data = Wx::CheckBox.new(self, Wx::ID_ANY, "Copy game data")
    @cbx_copy_tools = Wx::CheckBox.new(self, Wx::ID_ANY, "Copy tools")
    @cbx_copy_rage = Wx::CheckBox.new(self, Wx::ID_ANY, "Copy RAGE")
    @cbx_copy_ps3_sdk = Wx::CheckBox.new(self, Wx::ID_ANY, "Copy RAGE version of PS3 SDK")

    items_sizer = Wx::BoxSizer.new(Wx::VERTICAL)
    items_sizer.add(txt_nm_dir, 1, Wx::GROW)
    items_sizer.add(cbx_copy_game_code, 1, Wx::GROW)
    items_sizer.add(cbx_copy_game_data, 1, Wx::GROW)
    items_sizer.add(cbx_copy_tools, 1, Wx::GROW)
    items_sizer.add(cbx_copy_rage, 1, Wx::GROW)
    items_sizer.add(cbx_copy_ps3_sdk, 1, Wx::GROW)

    main_sizer = Wx::BoxSizer.new(Wx::VERTICAL)
    main_sizer.add(stc_text, 2, Wx::ALL | Wx::GROW, 5)
    main_sizer.add(items_sizer, 12, Wx::ALL | Wx::GROW, 15)
    main_sizer.add(create_separated_button_sizer(Wx::OK | Wx::CANCEL), 5, Wx::ALL | Wx::GROW, 15)

    set_sizer main_sizer
  end

  def NmBuildMenuDialog::show_dialog(title = TITLE_DEFAULT, prompt = PROMPT_DEFAULT)
    app = GUI::Application::instance()

    output = 0

    app.do do
      dlg = NmBuildMenuDialog::new(title, prompt)
      ret = dlg.show_modal()

      case ret
      when Wx::ID_OK
        # output should be a bit-mask of all enabled flags.
        output |= OPTION_GAMESRC if dlg.cbx_copy_game_code.get_value()
        output |= OPTION_GAMEDATA if dlg.cbx_copy_game_data.get_value()
        output |= OPTION_COPYTOOLS if dlg.cbx_copy_tools.get_value()
        output |= OPTION_COPYRAGE if dlg.cbx_copy_rage.get_value()
        output |= OPTION_COPYPS3SDK if dlg.cbx_copy_ps3_sdk.get_value()
      else
        output = -1
      end
    end # app.do

    SafeWriteLogFile(LOG_FILE, "Executing menu selections: #{output}")
    return output
  end
end


#------------------------------------------------------------------------------
# Subroutines
#------------------------------------------------------------------------------

def DisplayMenu()
  # Display a menu and process the user's seleciton.
  begin
    output = NmBuildMenuDialog.show_dialog('Create a stand-alone build for Natural Motion', 'Prompt')
    if output==-1
      puts 'User cancelled input.'
      Kernel::exit
    end
  rescue RuntimeError => ex
    GUI::ExceptionDialog::show_dialog(ex, "Exception caught during user input: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "Exception caught during user input: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "backtrace:\n #{ex.backtrace.join("\n")}")
    Kernel::exit
  end
end

#------------------------------------------------------------------------------

# === Copy the game source files ===
def CopyGameSource()
  msg = "Copying game source files to #{$nm_folder}..."
  puts msg
  SafeWriteLogFile(LOG_FILE, msg)

  orig_proj_root=OS::Path.normalise(ENV['RS_PROJROOT'])
  orig_game_src_path=orig_proj_root + "/src/#{BRANCH}"
  SafeWriteLogFile(LOG_FILE, "Game source path set as: #{orig_game_src_path}")

  begin
    SmartCopy("#{orig_game_src_path}/game/core/main.cpp", "#{$nm_folder}/src/#{BRANCH}/game/core/")
    SmartCopy("#{orig_game_src_path}/game/core/app.h", "#{$nm_folder}/src/#{BRANCH}/game/core/")
    SmartCopy("#{orig_game_src_path}/game/basetypes.h", "#{$nm_folder}/src/#{BRANCH}/game/")
    SmartCopy("#{orig_game_src_path}/game/game_config.h", "#{$nm_folder}/src/#{BRANCH}/game/")
    SmartCopy("#{orig_game_src_path}/game/optimisations.h", "#{$nm_folder}/src/#{BRANCH}/game/")
    SmartCopy("#{orig_game_src_path}/game/shader_source/common.fxh", "#{$nm_folder}/src/#{BRANCH}/game/shader_source/")
    SmartCopy("#{orig_game_src_path}/game/shader_source/Peds/ped_common_values.h", "#{$nm_folder}/src/#{BRANCH}/game/shader_source/Peds/")
    SmartCopy("#{orig_game_src_path}/game/shader_source/Vehicles/vehicle_common_values.h", "#{$nm_folder}/src/#{BRANCH}/game/shader_source/Vehicles/")
    SmartCopy("#{orig_game_src_path}/game/VS_Project/RageMisc/RageMisc_2008.vcproj", "#{$nm_folder}/src/#{BRANCH}/game/VS_Project/RageMisc/")
    SmartCopy("#{orig_game_src_path}/game/VS_Project/RageMisc/RageMisc.guid", "#{$nm_folder}/src/#{BRANCH}/game/VS_Project/RageMisc/")
    SmartCopy("#{orig_proj_root}/xlast/Fuzzy.spa", "#{$nm_folder}/xlast/")

    for lib_num in 1..4 do
      SmartCopy("#{orig_game_src_path}/game/VS_Project_NM/game#{lib_num}_lib", "#{$nm_folder}/src/#{BRANCH}/game/VS_Project_NM/")
    end # Loop over the game libraries.
    SmartCopy("#{orig_game_src_path}/game/VS_Project_NM/network", "#{$nm_folder}/src/#{BRANCH}/game/VS_Project_NM/")
    SmartCopy("#{orig_game_src_path}/game/VS_Project_NM/clean_build.rb", "#{$nm_folder}/src/#{BRANCH}/game/VS_Project_NM/")
    SmartCopy("#{orig_game_src_path}/game/VS_Project_NM/game.txt", "#{$nm_folder}/src/#{BRANCH}/game/VS_Project_NM/")
    SmartCopy("#{orig_game_src_path}/game/VS_Project_NM/game.guid", "#{$nm_folder}/src/#{BRANCH}/game/VS_Project_NM/")
    SmartCopy("#{orig_game_src_path}/game/VS_Project_NM/game_2008.sln", "#{$nm_folder}/src/#{BRANCH}/game/VS_Project_NM/")
    SmartCopy("#{orig_game_src_path}/game/VS_Project_NM/game_2008.vcproj", "#{$nm_folder}/src/#{BRANCH}/game/VS_Project_NM/")
    SmartCopy("#{orig_game_src_path}/game/VS_Project_NM/load_sln__standalone.bat", "#{$nm_folder}/src/#{BRANCH}/game/VS_Project_NM/")
    SmartCopy("#{orig_game_src_path}/game/VS_Project_NM/set_nm_branch_env__standalone.bat", "#{$nm_folder}/src/#{BRANCH}/game/VS_Project_NM/")

    # Copy the command line param file and correct the -rootdir and -nmfolder args to point to the location of
    # the stand-alone build.
    FileUtils::rm("#{$nm_folder}/commandline.txt", :force=>true)
    File.open("#{orig_game_src_path}/game/VS_Project_NM/commandline.txt", 'r') do |srcFile|
      File.open("#{$nm_folder}/commandline.txt", 'w') do |destFile|
        srcFile.each_line do |srcLine|
          if srcLine.include?('-rootdir')
            destFile.puts("-rootdir=#{$nm_folder}/build/#{BRANCH}/")
          elsif srcLine.include?('-nmfolder')
            destFile.puts("-nmfolder=#{$nm_folder}/build/#{BRANCH}/naturalmotion/")
          else
            destFile.puts(srcLine)
          end
        end # Loop over each line in commandline.txt
      end # close dest file.
    end # Close src file.
  rescue Exception => ex
    GUI::ExceptionDialog::show_dialog( ex, "Exception caught while copying game source: #{ex.message}" )
    SafeWriteLogFile(LOG_FILE, "Exception caught while copying game source: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "backtrace:\n #{ex.backtrace.join("\n")}")
    Kernel::exit
  end

end # CopyGameSource()

#------------------------------------------------------------------------------

# === Copy the tools files ===
def CopyTools()
  msg = "Copying required tools files to #{$nm_folder}..."
  puts msg
  SafeWriteLogFile(LOG_FILE, msg)

  begin
    tools_src_path=OS::Path.normalise(ENV['RS_TOOLSROOT'])
    tools_dest_path="#{$nm_folder}/tools"
    SafeWriteLogFile(LOG_FILE, "tools_src_path set as #{tools_src_path}")
    SafeWriteLogFile(LOG_FILE, "tools_dest_path set as #{tools_dest_path}")

    # Copy RAG.
    SmartCopy("#{tools_src_path}/bin/rag", "#{tools_dest_path}/bin/")

    # Copy SysTrayRfs.
    SmartCopy("#{tools_src_path}/bin/SysTrayRfs.exe", "#{tools_dest_path}/bin/")

    # Copy main setenv.bat file.
    SmartCopy("#{tools_src_path}/bin/setenv.bat", "#{$nm_folder}/src/#{BRANCH}/game/VS_Project_NM/")

    # Copy tool scripts required for custom build steps.
    SmartCopy("#{tools_src_path}/script/coding", "#{tools_dest_path}/script/")
    SmartCopy("#{tools_src_path}/bin/coding", "#{tools_dest_path}/bin/")

  rescue Exception => ex
    GUI::ExceptionDialog::show_dialog( ex, "Exception caught while copying required tools: #{ex.message}" )
    SafeWriteLogFile(LOG_FILE, "Exception caught while copying required tools: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "backtrace:\n #{ex.backtrace.join("\n")}")
    Kernel::exit
  end

end # CopyTools()

#------------------------------------------------------------------------------

# === Copy RAGE source ===
def CopyRAGE()
  # Clean all compiled object data from the RAGE directory tree and copy the remaining
  # files to the stand-alone NM build folder.

  SafeWriteLogFile(LOG_FILE, "Cleaning all compiled object data from the RAGE directory tree...")
  OS::Path::set_downcase_on_normalise(false)
  rage_src_path=OS::Path.normalise(ENV['RS_PROJROOT']) + "/src/#{BRANCH}/rage"
  SafeWriteLogFile(LOG_FILE, "RAGE source path set as: #{rage_src_path}")

  # Relative to RAGE root.
  exclude_folders =
  [
   'base/tools',
   'framework/tools',
   'framework/deprecated',
   'naturalmotion/dump',
   '3rdParty',
  ]

  # Copy the RAGE directory tree, excluding anything which would unnecessarily bloat the build.
  begin
    puts "Copying RAGE tree to #{$nm_folder} (this will take some time)..."

    rage_top_level_dirs = OS::FindEx::find_dirs(rage_src_path)
    rage_top_level_files = OS::FindEx::find_files("#{rage_src_path}/*.*")
    puts "RAGE:"
    rage_top_level_dirs.each do |dir|
      dir_partial = dir.sub("#{rage_src_path}/", "")
      msg = "|-#{dir} --> x:/NM_FOLDER/src/dev/rage/#{dir_partial}"
      puts msg
      SafeWriteLogFile(LOG_FILE, msg)

      # Find all directories within this top-level RAGE folder.
      level2_dirs = OS::FindEx::find_dirs(dir)

      # Now copy all subdirectories and files within this dir as long as we weren't asked to exclude them.
      level2_dirs.each do |dir_at_level2|

        # Check if this directory is in the exclusion list.
        bExcludeThisDir = false
        for i in 0..exclude_folders.size()-1 do
          bExcludeThisDir = true if dir_at_level2.downcase.include?("#{rage_src_path}/#{exclude_folders[i].downcase}")
        end

        # If not, go ahead and copy it wholesale to the NM folder.
        unless bExcludeThisDir
          dir2_partial = dir_at_level2.sub("#{rage_src_path}/", "")
          msg = "| |-#{dir_at_level2} --> #{$nm_folder}/src/#{BRANCH}/rage/#{dir2_partial}"
          puts msg
          SafeWriteLogFile(LOG_FILE, msg)
          dir2_parts = OS::Path::get_parts(dir2_partial)
          # Remove the last directory reliably, i.e. even if the directory name looks like a file.
          dir2_partial_dest = ""
          for i in 0..dir2_parts.size()-2 do
            dir2_partial_dest = OS::Path::combine(dir2_partial_dest, dir2_parts[i])
          end
          SmartCopy("#{dir_at_level2}", "#{$nm_folder}/src/#{BRANCH}/rage/#{dir2_partial_dest}/")
        end
      end

      # As before, but for the individual files within this dir.
      files = OS::FindEx::find_files("#{dir}/*.*")
      files.each do |file_at_level2|
        file2_partial = file_at_level2.sub("#{rage_src_path}/", "")
        msg = "| |-#{file_at_level2} --> #{$nm_folder}/src/#{BRANCH}/rage/#{file2_partial}"
        puts msg
        SafeWriteLogFile(LOG_FILE, msg)
        SmartCopy("#{file_at_level2}", "#{$nm_folder}/src/#{BRANCH}/rage/#{file2_partial}")
      end
    end # Loop over top level directories in RAGE tree.

    # Copy any loose files in the RAGE root directory.
    rage_top_level_files.each do |file|
      file_partial = file.sub("#{rage_src_path}/", "")
      msg = "|-#{file} --> #{$nm_folder}/src/#{BRANCH}/rage/#{file_partial}"
      puts msg
      SafeWriteLogFile(LOG_FILE, msg)
      SmartCopy("#{file}", "#{$nm_folder}/src/#{BRANCH}/rage/#{file_partial}")
    end

    # Delete any unwanted files from the copy.
    unwanted_files = ["#{$nm_folder}/src/#{BRANCH}/rage/.p4config"]
    FileUtils::rm_rf(unwanted_files) # Don't throw an exception if a file/dir doesn't exist.
  rescue Exception => ex
    GUI::ExceptionDialog::show_dialog( ex, "Exception caught while copying RAGE source code: #{ex.message}" )
    SafeWriteLogFile(LOG_FILE, "Exception caught while copying RAGE source code: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "backtrace:\n #{ex.backtrace.join("\n")}")
    Kernel::exit
  end

end # CopyRAGE()

#------------------------------------------------------------------------------

# === Copy the RAGE version of the PS3 SDK ===
def CopyPs3Sdk()
  puts "Copying RAGE version of PS3 SDK to #{$nm_folder}..."
  SafeWriteLogFile(LOG_FILE, "Copying RAGE version of PS3 SDK to #{$nm_folder}...")

  # TODO RA: only copy the current sdk in the event that multiple copies exist
  # on the user's workspace.

  begin
    SmartCopy("x:/ps3sdk", "#{$nm_folder}/")
  rescue RuntimeError => ex
    GUI::ExceptionDialog::show_dialog( ex, "Exception caught while copying required tools: #{ex.message}" )
    SafeWriteLogFile(LOG_FILE, "Exception caught while copying required tools: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "backtrace:\n #{ex.backtrace.join("\n")}")
    Kernel::exit
  end

end # CopyPs3Sdk()

#------------------------------------------------------------------------------

# === Copy the scaled down game data ===
# Copy only the game data which is to be included with the NM build and
# then bundle it into RPF files in-situ.
def CopyGameData()
  puts "Copying game data to #{$nm_folder}..."
  SafeWriteLogFile(LOG_FILE, "Copying game data to #{$nm_folder}")

  orig_game_data_path=OS::Path.normalise(ENV['RS_PROJROOT']) + "/build/#{BRANCH}"

  # Copy only the necessary data to the NM build directory.
  CreateDirIfMissing("#{$nm_folder}/build/#{BRANCH}/common/data")
  begin
    # Copy the directories in :common/data/.
    common_data_dirs = ['ai', 'anim', 'effects', 'fragments', 'maps', 'materials', 'move', 'Scaleform', 'script', 'timecycle']
    common_data_dirs.each do |common_data_dir|
      SafeWriteLogFile(LOG_FILE, "Attemping to copy recursively from #{orig_game_data_path}/common/data/#{common_data_dir} to #{$nm_folder}/build/#{BRANCH}/common/data/")
      SmartCopy("#{orig_game_data_path}/common/data/#{common_data_dir}", "#{$nm_folder}/build/#{BRANCH}/common/data/")
    end
    # Copy the files in :common/data/.
    common_data_files = OS::FindEx::find_files(OS::Path::combine("#{orig_game_data_path}/common/data", "*.*"))
    common_data_files.each do |cmndfile|
      SmartCopy(cmndfile, "#{$nm_folder}/build/#{BRANCH}/common/data/")
    end
    CreateDirIfMissing("#{$nm_folder}/build/#{BRANCH}/naturalmotion")
    #FileUtils::mv("#{$nm_folder}/build/#{BRANCH}/common/data/taskparams.txt", "#{$nm_folder}/build/#{BRANCH}/naturalmotion")

    # Copy the data for the nm_test level only.
    SmartCopy("#{orig_game_data_path}/common/data/levels/nm_test", "#{$nm_folder}/build/#{BRANCH}/common/data/levels/")
	
    # Copy the data for the common level only.
    SmartCopy("#{orig_game_data_path}/common/data/levels/common", "#{$nm_folder}/build/#{BRANCH}/common/data/levels/")
		
    # Copy the natural motion directory to a special location for NM builds.
    SmartCopy("#{orig_game_data_path}/common/data/naturalmotion", "#{$nm_folder}/build/#{BRANCH}/")

    # Copy the shaders.
    SmartCopy("#{orig_game_data_path}/common/shaders", "#{$nm_folder}/build/#{BRANCH}/common/")

    # Copy :common/text/.
    SmartCopy("#{orig_game_data_path}/common/text", "#{$nm_folder}/build/#{BRANCH}/common/")

    # Copy metadata.
    SmartCopy("#{orig_game_data_path}/metadata", "#{$nm_folder}/build/#{BRANCH}/")

    # Platform specific data, platforms and directories to copy are defined in the arrays below:
    platform_list = ["ps3"]
    platform_dirs = ["anim", "audio", "data", "models", "textures"]
    platform_list.each do |platform|
      platform_dirs.each do |platform_dir|
        SmartCopy("#{orig_game_data_path}/#{platform}/#{platform_dir}", "#{$nm_folder}/build/#{BRANCH}/#{platform}/")
      end # Iteration over platform_dirs.

      # Copy the level data.
      SmartCopy("#{orig_game_data_path}/#{platform}/levels/nm_test", "#{$nm_folder}/build/#{BRANCH}/#{platform}/levels/")
      SmartCopy("#{orig_game_data_path}/#{platform}/levels/generic", "#{$nm_folder}/build/#{BRANCH}/#{platform}/levels/")
      SmartCopy("#{orig_game_data_path}/#{platform}/levels/testbed", "#{$nm_folder}/build/#{BRANCH}/#{platform}/levels/")
	  SmartCopy("#{orig_game_data_path}/#{platform}/levels/gta5/props/lev_des", "#{$nm_folder}/build/#{BRANCH}/#{platform}/levels/gta5/props")
	  
	  # Replace the usual script.rpf with the special version for NM builds.
      #FileUtils::rm_f("#{$nm_folder}/build/#{BRANCH}/#{platform}/levels/nm_test/script/script.rpf")
      #SmartCopy("#{$nm_folder}/build/#{BRANCH}/#{platform}/levels/nm_test/nm_build/script/script.rpf", "#{$nm_folder}/build/#{BRANCH}/#{platform}/levels/nm_test/script/")

      # Copy the NM data to the special dir.
      puts "Copying #{orig_game_data_path}/#{platform}/data/naturalmotion"
      nm_platform_files = OS::FindEx::find_files_recurse("#{orig_game_data_path}/#{platform}/data/naturalmotion/*.*")
      nm_platform_files.each do |nmpfile|
        SmartCopy(nmpfile, "#{$nm_folder}/build/#{BRANCH}/naturalmotion/")
      end

      # Copy those RPF files which have special, scaled down versions in the nm_test level folder.
      #SmartCopy("#{orig_game_data_path}/#{platform}/levels/nm_test/componentpeds_nmbuild.rpf", "#{$nm_folder}/build/#{BRANCH}/#{platform}/models/cdimages/componentpeds.rpf")
      #SmartCopy("#{orig_game_data_path}/#{platform}/levels/nm_test/pedprops_nmbuild.rpf", "#{$nm_folder}/build/#{BRANCH}/#{platform}/models/cdimages/pedprops.rpf")
      #SmartCopy("#{orig_game_data_path}/#{platform}/levels/nm_test/streamedpeds_nmbuild.rpf", "#{$nm_folder}/build/#{BRANCH}/#{platform}/models/cdimages/streamedpeds.rpf")
      #SmartCopy("#{orig_game_data_path}/#{platform}/levels/nm_test/vehicles_nmbuild.rpf", "#{$nm_folder}/build/#{BRANCH}/#{platform}/levels/nm_test/vehicles.rpf")
    end # Loop over each platform.

    # Overwrite those IDE files which have been scaled down in make_rpf_files_for_nm_build.rb.
    #SmartCopy("#{orig_game_data_path}/common/data/peds_temp.ide", "#{$nm_folder}/build/#{BRANCH}/common/data/peds.ide")
    #FileUtils::rm_f("#{orig_game_data_path}/common/data/peds_temp.ide")
    #FileUtils::rm_f("#{$nm_folder}/build/#{BRANCH}/common/data/peds_temp.ide")
    #SmartCopy("#{orig_game_data_path}/common/data/levels/nm_test/vehicles_temp.ide", "#{$nm_folder}/build/#{BRANCH}/common/data/levels/nm_test/vehicles.ide")
    #FileUtils::rm_f("#{orig_game_data_path}/common/data/levels/nm_test/vehicles_temp.ide")
    #FileUtils::rm_f("#{$nm_folder}/build/#{BRANCH}/common/data/levels/nm_test/vehicles_temp.ide")

    # Delete any unwanted files from the copy.
    unwanted_files = ["#{$nm_folder}/build/#{BRANCH}/ps3/data/root.sys"]
    FileUtils::rm_rf(unwanted_files) # Don't throw an exception if a file/dir doesn't exist.

  rescue Exception => ex
    GUI::ExceptionDialog::show_dialog( ex, "Exception caught while copying game data: #{ex.message}" )
    SafeWriteLogFile(LOG_FILE, "Exception caught while copying game data: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "backtrace:\n #{ex.backtrace.join("\n")}")
    Kernel::exit
  end

end # CopyGameData()


#------------------------------------------------------------------------------
# Main
#------------------------------------------------------------------------------
if(__FILE__ == $0) then
  begin
    g_AppName = File::basename(__FILE__,'.rb')

    # Initialise the logging system.
    SafeCreateLogFile(LOG_FILE)

    # Create a best guess at the NM build folder based on the 'x:/NM_MONTH_YEAR' convention.
    time = Time.new
    month = time.strftime("%b").upcase
    year = time.strftime("%Y")
    nm_folder="X:\\NM_" + month + "_" + year

    # Declare the variable which stores the choices from the build task menu.
    menu_output = 0

    bTerminatedByUser = false
    app = GUI::Application::instance()
    app.do do
      # Allow the user to set/change the location for the stand alone build.
      dlg = InputDialog::new('Select path for stand-alone NM build', 'Prompt', "#{nm_folder}")
      ret = dlg.show_modal()

      case ret
      when Wx::ID_OK
        $nm_folder = dlg.txt_text.get_value
        WriteBatFile($nm_folder)
        # "Normalise" the directory separation slashes for internal Ruby operations (i.e. make them all '/').
        OS::Path::set_downcase_on_normalise(false)
        $nm_folder = OS::Path::normalise($nm_folder)
      else
        puts 'User cancelled input.'
        bTerminatedByUser = true
      end

      # Display the action menu to allow the user to select which build tasks should be performed.
      unless bTerminatedByUser
        msg = "nm_folder is set as: #{$nm_folder}"
        puts "\n\n#{msg}"
        SafeWriteLogFile(LOG_FILE, msg)
        puts "\n\nDisplaying user menu..."

        menu_dlg = NmBuildMenuDialog::new('Create a stand-alone build for Natural Motion', 'Prompt')
        menu_ret = menu_dlg.show_modal()

        case menu_ret
        when Wx::ID_OK
          # Output should be a bit-mask of all enabled flags.
          menu_output |= OPTION_GAMESRC if menu_dlg.cbx_copy_game_code.get_value()
          menu_output |= OPTION_GAMEDATA if menu_dlg.cbx_copy_game_data.get_value()
          menu_output |= OPTION_COPYTOOLS if menu_dlg.cbx_copy_tools.get_value()
          menu_output |= OPTION_COPYRAGE if menu_dlg.cbx_copy_rage.get_value()
          menu_output |= OPTION_COPYPS3SDK if menu_dlg.cbx_copy_ps3_sdk.get_value()
        else
          puts 'User cancelled input.'
          bTerminatedByUser = true
        end
        SafeWriteLogFile(LOG_FILE, "Executing menu selections: #{menu_output}")
      end
    end # app.do

    # If the user clicked "cancel" in any of the input dialog windows above, we just quit.
    Kernel::exit if bTerminatedByUser

    # Successively execute each command selected in the GUI menu.
    CopyGameSource() unless menu_output&OPTION_GAMESRC==0
    CopyGameData() unless menu_output&OPTION_GAMEDATA==0
    CopyTools() unless menu_output&OPTION_COPYTOOLS==0
    CopyRAGE() unless menu_output&OPTION_COPYRAGE==0
    CopyPs3Sdk() unless menu_output&OPTION_COPYPS3SDK==0

    SafeWriteLogFile(LOG_FILE, "Script completed successfully! :)")
    Pipeline::GUI::MessageBox::information("#{g_AppName}", "Script completed successfully! :)")

  rescue SystemExit => ex # Allows script to exit quietly after fatal exceptions.
  rescue Exception => ex
    puts "Unhandled exception: #{ex.message}"
    puts ex.backtrace.join( "\n" )
    Pipeline::GUI::ExceptionDialog::show_dialog( ex, "Unhandled exception while creating the NM stand-alone build: #{ex.message}" )
    SafeWriteLogFile(LOG_FILE, "Fatal exception: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "backtrace:\n #{ex.backtrace.join("\n")}")
  end # Main try-catch block.
end # Main.
