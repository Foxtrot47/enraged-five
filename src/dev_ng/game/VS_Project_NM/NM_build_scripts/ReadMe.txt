/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// INSTRUCTIONS FOR MAKING A STAND-ALONE BUILD FOR NATURAL MOTION
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

* Run X:\gta5\src\dev\game\VS_Project_NM\NM_build_scripts\CreateNmProject.rb which will make the modifications required to dongle protect the code, build
  and copy the necessary libraries and auto-generate the Visual Studio project and solution files for the NM build.

* If necessary, create a new dongle:
   [How to make the dongle (see also http://rsgediwiki1/wiki/index.php/How_to_Dongle_ure_console):
    Get dongle tool executables from depot //depot/gta5/src/dev/tools/DongleTool.
    Have the MAC address from the console ready.
    (On Xenon: from the launcher press "X" to access tools menu, then Network Settings->Game configuration->IP settings)
    Run MakeDongles.bat and supply the teststring password (found from app.cpp in the game code) and MAC address (CAPITALS ONLY!).
    Write data to a memory card and you're done.
    Make sure the game library was built with dongle enabled as described above.]

* [OPTIONAL STEP] At this point, the game should now build from the VS_Project_NM directory. Test this by running load_sln.bat and trying to build
  beta/bankrelease on PS3/XBox360.

* Edit the text files in x:\gta5\src\dev\game\VS_Project_NM\NM_build_scripts\RPF_scripts\ so that they contain only the object types required for the NM build and
  run make_rpf_files_for_nm_build.rb to create the new RPF files.

* Run the Ruby script CreateNmBuildFolder.rb which will create the stand-alone directory structure which can eventually be compressed and sent to NM.

* Run x:\gta5\src\dev\game\VS_Project_NM\NM_build_scripts\make_disk_images.rb to encrypt the data as in release builds.

* Test that the solution builds in the new location (<NM_FOLDER>):
       - Run X:\<NM_FOLDER>\src\dev\game\VS_Project_NM\load_sln__standalone.bat
       - Build xenon beta and xenon bankrelease.
       - Run with the command arguments: @X:\<NM_FOLDER>\commandline.txt (key params: -usepackfiles, -nmfolder=x:/<NM FOLDER>/build/dev/naturalmotion/)
       - Check it only runs with a dongle (For PS3, the dongle is an encrypted file (root.sys) and should be put in X:\<NM_FOLDER>\build\dev\ps3\data\).

* To cut down on file size when shipping to NM:
       - Run X:\<NM_FOLDER>\src\dev\game\VS_Project_NM\clean_build.rb
       - Compress with WinRAR and make sure the archive is encrypted with a password.
