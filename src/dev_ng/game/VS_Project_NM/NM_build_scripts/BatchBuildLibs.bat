@echo off
REM ************************************************************
REM * Script to call the batch builder to create the game libs *
REM * for beta and bankrelease on both PS3 and XBox360.        *
REM ************************************************************

CALL setenv
REM Detect if current SDK is either unset or older than 350 and switch.
IF NOT EXIST %SCE_PS3_ROOT%\info\old\340.001\Bugfix_SDK_e.txt set SCE_PS3_ROOT=X:/ps3sdk/dev/usr/local/360_001/cell
ECHO SCE_PS3_ROOT: 	%SCE_PS3_ROOT%

CD ..\..\VS_Project
CALL batch_gta5 ps3 beta bankrelease | tee ..\VS_Project_NM\NM_build_scripts\BatchBuild.log

IF "%PAUSEOPTION%"=="" PAUSE
