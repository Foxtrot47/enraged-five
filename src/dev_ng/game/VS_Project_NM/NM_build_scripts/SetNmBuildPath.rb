# ***********************************************************************
# *** Pop up a dialog box to allow the user to set the location where ***
# *** the stand-alone build should live.                              ***
# *** This script generates set_nm_branch_env__standalone.bat         ***
# ***********************************************************************
#
# Author : Richard Archibald
# Started: 24/11/2010

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/gui/exception_dialog'
require 'pipeline/gui/application'
include Pipeline

#------------------------------------------------------------------------------
# Object definitions
#------------------------------------------------------------------------------

class InputDialog < Wx::Dialog

  attr_reader :txt_text

  def initialize(title = TITLE_DEFAULT, prompt = PROMPT_DEFAULT, default_input = "")
    super(nil, Wx::ID_ANY, title, Wx::DEFAULT_POSITION, Wx::Size.new(300,100))

    stc_text = Wx::StaticText.new(self, Wx::ID_ANY, "NM build path:")
    @txt_text = Wx::TextCtrl.new(self, Wx::ID_ANY, default_input)

    items_sizer = Wx::BoxSizer.new(Wx::HORIZONTAL)
    items_sizer.add(stc_text, 1, Wx::GROW)
    items_sizer.add(txt_text, 2, Wx::GROW)

    main_sizer = Wx::BoxSizer.new(Wx::VERTICAL)
    main_sizer.add(items_sizer, 0, Wx::ALL | Wx::GROW, 5)
    main_sizer.add(create_separated_button_sizer(Wx::OK | Wx::CANCEL), 0, Wx::ALL | Wx::GROW, 5)

    set_sizer main_sizer
  end

  def InputDialog::show_dialog(title = TITLE_DEFAULT, prompt = PROMPT_DEFAULT, default_input = "")
    app = GUI::Application::instance()

    output = ""

    app.do do
      dlg = InputDialog::new(title, prompt, default_input)
      ret = dlg.show_modal()

      case ret
      when Wx::ID_OK
        output = dlg.txt_text.get_value
      else
        nil
      end
    end # app.do

    return output
  end
end


#------------------------------------------------------------------------------
# Subroutines
#------------------------------------------------------------------------------

def WriteBatFile(nm_folder)
  # Write the batch file which defines the NM stand-alone build folder.
  begin

    # Make sure the path contains "\" instead of "/" as the directory separation character.
    OS::Path::set_downcase_on_normalise(false)
    nm_folder = OS::Path::dos_format(nm_folder)

    batFileName = "../set_nm_branch_env__standalone.bat"
    if(File.exists?(batFileName))
      FileUtils::rm(batFileName, :force=>true)
    end
    File.open(batFileName, "w") do |envBatFile|
      envBatFile.puts "REM *** This file was generated automatically by SetNmBuildPath.rb ***"
      envBatFile.puts "REM *** Any modifications made here will be lost! ***"
      envBatFile.puts "SET RS_PROJROOT=#{nm_folder}"
      envBatFile.puts "SET RS_BUILDROOT=%RS_PROJROOT%\\build"
      envBatFile.puts "SET RS_TOOLSROOT=%RS_PROJROOT%\\tools"
      envBatFile.puts "SET RS_BUILDBRANCH=%RS_BUILDROOT%\\dev"
      envBatFile.puts "SET RS_CODEBRANCH=%RS_PROJROOT%\\src\\dev"
      envBatFile.puts "SET RAGE_DIR=%RS_PROJROOT%\\src\\dev\\rage"

      envBatFile.puts "\nCALL setenv.bat"
    end
  rescue RuntimeError => ex
    GUI::ExceptionDialog::show_dialog( ex, 'Fatal exception while writing NM env batch file' )
    Kernel::exit
  end
end

#------------------------------------------------------------------------------

def PromptUserForNmBuildPath()
  # Create a best guess at the NM build folder based on the 'x:/NM_MONTH_YEAR' convention.
  time = Time.new
  month = time.strftime("%b").upcase
  year = time.strftime("%Y")
  nm_folder="X:\\NM_" + month + "_" + year

  # Display a menu and process the user's seleciton.
  begin
    input = InputDialog.show_dialog( 'Select path for stand-alone NM build', 'Prompt', "#{nm_folder}" )
    if input==''
      puts 'User cancelled input.'
      Kernel::exit
    end
    nm_folder = input

    WriteBatFile(nm_folder)

    return nm_folder

  rescue RuntimeError => ex
    GUI::ExceptionDialog::show_dialog( ex, 'Fatal exception during user input' )
    Kernel::exit
  ensure
    LogSystem::instance( ).shutdown( )
  end
end	

#------------------------------------------------------------------------------
# Main
#------------------------------------------------------------------------------
if(__FILE__ == $0) then
  begin

    nm_path = PromptUserForNmBuildPath()
    puts "\n\nNM build folder set to: #{nm_path}"

  rescue SystemExit => ex # Allows script to exit quietly after fatal exceptions.
  rescue Exception => ex
    GUI::ExceptionDialog::show_dialog( ex, 'Unhandled exception while creating the NM stand-alone build' )
  end # Main try-catch block.
end # Main.
