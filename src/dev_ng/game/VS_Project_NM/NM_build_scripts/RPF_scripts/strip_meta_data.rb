# ***********************************************************************
# *** Strip out all unwanted metadata from peds and vehicle files.    ***
# ***********************************************************************
#
# Author : Richard Archibald
# Started: 8/2/2011

#----------------------------------------------------------------------------
# Includes
#----------------------------------------------------------------------------
require 'rexml/document'

require 'pipeline/gui/exception_dialog'
require 'pipeline/gui/messagebox'
require 'pipeline/gui/application'
include Pipeline

require '../SetNmBuildPath'
require '../FileHelpers'
include NmBuild


#------------------------------------------------------------------------------
# Constants
#------------------------------------------------------------------------------
LOG_FILE="StripMetaData.log"
BRANCH="dev"
# List of XML files to parse:
XML_FILE_LIST=
[
 'test_XML_delete_me/peds.meta'
]
# List of XML nodes to save from above file list.
XML_SAVE_NODE_LIST=
[
 'CPedModelInfo__InitDataList/InitDatas/Item'
]
KEEP_LIST=
[
 'Player_Zero',
 'S_M_Y_Cop'
]

#------------------------------------------------------------------------------
# Globals
#------------------------------------------------------------------------------



#------------------------------------------------------------------------------
# Main
#------------------------------------------------------------------------------
if(__FILE__ == $0) then
  begin
    g_AppName = File::basename(__FILE__,'.rb')

    # Initialise the logging system.
    SafeCreateLogFile(LOG_FILE)

    XML_FILE_LIST.each do |filename|

      # Register an XML file with REXML.
      doc = nil
      File.open(filename) do |xmlFile|
        doc = REXML::Document.new(xmlFile)
      end

      doc.elements.each(XML_SAVE_NODE_LIST[0]) do |e|
        e.elements.each('Name') do |idtag|
          unless KEEP_LIST.include?(idtag.text)
            e.elements.delete_all('*')
            while node = e.get_text
              e.delete node
            end
          end
        end
      end
      doc.write

      # Open the new file for output and write the basic hierarchy.
      File.open("#{filename}_temp", "w") do |outFile|
        doc.write(outFile)
      end # Close output file.

    end # XML_FILE_LIST.each
    

    # We're good if we got here, i.e. we didn't throw an exception.
    SafeWriteLogFile(LOG_FILE, "Script completed successfully! :)")
    Pipeline::GUI::MessageBox::information("#{g_AppName}", "Script completed successfully! :)")
    puts "\n\ndone. :)"

  rescue SystemExit => ex # Allows script to exit quietly after fatal exceptions.
  rescue Exception => ex
    puts "Unhandled exception: #{ex.message}"
    puts ex.backtrace.join( "\n" )
    Pipeline::GUI::ExceptionDialog::show_dialog( ex, "Unhandled exception while creating the NM stand-alone build: #{ex.message}" )
    SafeWriteLogFile(LOG_FILE, "Fatal exception: #{ex.message}")
    SafeWriteLogFile(LOG_FILE, "backtrace:\n #{ex.backtrace.join("\n")}")
  end # Main try-catch block.
end # Main
