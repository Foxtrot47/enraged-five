#
# File::        make_rpf_files_for_nm_build.rb
# Description:: Updates / creates new independent RPF files for the
#               Natural Motion test level based on text files in the working
#               directory.
#               The RPF files will be checked out of Perforce so that the user
#               has a reminder to check them in if desired.
#               This script also copies the relevant IDE files from GTA5 and
#               removes any entries which have been excluded.
#
# Author:: Richard Archibald
# Started:: 8th November 2010


#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/gui/exception_dialog'
require 'pipeline/gui/log_window'
require 'pipeline/gui/messagebox'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_extract'
require 'pipeline/projectutil/data_convert'
require 'pipeline/util/rage'
include Pipeline

require '../FileHelpers'
include NmBuild


#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
INPUT_BASE            = "x:/gta5/build/dev/"
OUTPUT_BASE           = "x:/gta5/build/dev/"
INPUT_EXPORT_BASE     = "x:/gta5/assets/export/"
OUTPUT_EXPORT_BASE    = "x:/gta5/assets/export/"

LOG_FILE="MakeRpfFiles.log"

INCLUDE_FILES = # Excluding the file extension.
[
 'VEHICLES'
]
INCLUDE_ZIP_FILES =
[
 'COMPONENTPEDS',
 'PEDPROPS',
 'STREAMEDPEDS'
]

INPUT_RPF_BASES =
[
 INPUT_EXPORT_BASE+'levels/gta5'
]
INPUT_ZIP_BASES =
[
 INPUT_EXPORT_BASE+'models/cdimages',
 INPUT_EXPORT_BASE+'models/cdimages',
 INPUT_EXPORT_BASE+'models/cdimages'
]
OUTPUT_RPF_BASES =
[
 OUTPUT_EXPORT_BASE+'levels/nm_test'
]
OUTPUT_ZIP_BASES =
[
 OUTPUT_EXPORT_BASE+'models/cdimages',
 OUTPUT_EXPORT_BASE+'models/cdimages',
 OUTPUT_EXPORT_BASE+'models/cdimages'
]
INPUT_RPF_FILENAMES =
[
 'vehicles.rpf'
]
INPUT_ZIP_FILENAMES = 
[
  'componentpeds.zip',
 'pedprops.zip',
 'streamedpeds.zip'
]

INPUT_IDE_BASES =
[
 INPUT_BASE+'common/data/levels/gta5',
]
INPUT_IDE_ZIP_BASES =
[
 INPUT_BASE+'common/data',
 '',
 INPUT_BASE+'common/data'
]
OUTPUT_IDE_BASES =
[
 OUTPUT_BASE+'common/data/levels/nm_test',
]
OUTPUT_IDE_ZIP_BASES =
[
 OUTPUT_BASE+'common/data',
 '',
 OUTPUT_BASE+'common/data'
]
INPUT_IDE_FILENAMES =
[
 'vehicles.ide',
]
INPUT_IDE_ZIP_FILENAMES =
[
 'peds.ide',
 '',
 'peds_temp.ide' # We already remove some data from peds.ide first time it is encountered; don't wan't to loose changes.
]

#----------------------------------------------------------------------------
# Subroutines
#----------------------------------------------------------------------------
def CreateReducedRpfFile(include_filename, input_rpf_filename, output_rpf_base, input_ide_filename)

  begin
    g_AppName = File::basename(__FILE__,'.rb')

    SafeWriteLogFile(LOG_FILE, "Creating Reduced RPF from: #{input_rpf_filename}")

    c = Pipeline::Config::instance()
    project = c.projects['gta5']
    project.load_config()

    r = RageUtils::new(project)

    # Extract the files from the main RPF file into a temporary directory.
    output_dir = OS::Path::combine(output_rpf_base, 'extract_temp')
    extracted_files = []
    extracted_files += ProjectUtil::data_extract_rpf(r, input_rpf_filename, output_dir, true) do |filename|
      puts "\t#{filename}"
    end

    # Load the list of vehicles for the test level.
    include_txt_file = File::open(include_filename, 'r')
    include_list = []
    include_txt_file.each_line do |line|
      # Add the first space-delineated word from a line which doesn't start with a comment "#".
      include_list += [(''+line).split(' ')[0].downcase] unless line.match('^[^a-z,A-Z]*#')
    end

    # Remove those files which aren't mentioned in the include_list array.
    excluded_files = []
    extracted_files.each do |filename|
      next unless(File::exists?(filename))
      basename = OS::Path::get_basename(filename)
      next if(include_list.include?(basename.downcase))
      # Don't want to exclude files within a directory which is related to the include objects.
      dirname = File::dirname(filename).split('/')[-1]
      next if(include_list.include?(dirname.downcase))

      excluded_files += [filename]
      FileUtils::remove(filename)
    end

    # Write to a temporary new RPF.
    rpf_file_basename = OS::Path::get_basename(input_rpf_filename)
    output_rpf_filename = OS::Path::combine(output_rpf_base, rpf_file_basename)
    output_rpf_filename += '_temp.rpf'
    SafeWriteLogFile(LOG_FILE, "Output RPF: #{output_rpf_filename}")

    # Package those files which are mentioned in the include_list array.
    msg = "\n***** CREATING RPF *****"
    puts msg
    files = OS::FindEx::find_files_recurse(OS::Path::combine(output_dir, '*.*'))
    r.pack.start()
    files.each do |filename|
      # Find the temp directory name in the file path and remove everything before it to form
      # the RPF's internal file system.
      if(filename.split('/')[-2] == 'extract_temp')
        SafeWriteLogFile(LOG_FILE, "Adding #{OS::Path::get_filename(filename)}")
        r.pack.add(filename, OS::Path::get_filename(filename))
      else
        SafeWriteLogFile(LOG_FILE, "Adding #{OS::Path::combine(filename.split('/')[-2], OS::Path::get_filename(filename))}")
        r.pack.add(filename, OS::Path::combine(filename.split('/')[-2], OS::Path::get_filename(filename)))
      end
    end
    SafeWriteLogFile(LOG_FILE, "Saving RPF.")
    r.pack.save(output_rpf_filename)
    SafeWriteLogFile(LOG_FILE, "Closing RPF.")
    r.pack.close()

    # Convert independent RPF to platform specific versions.
    toolsroot = OS::Path.normalise(ENV['RS_TOOLSROOT'])
    toolslib = "#{toolsroot}/lib"
    msg = "\nConverting RPF file (#{output_rpf_filename}) to platform specific versions..."
    puts msg
    SafeWriteLogFile(LOG_FILE, msg)
    ProjectUtil::data_convert_file(output_rpf_filename)

    ################################ IDE FILE ################################

    # Copy the main IDE file and strip out all entries for excluded objects.
    if input_ide_filename != '' then
      output_ide_filename = 'temp.ide'
      SafeWriteLogFile(LOG_FILE, "Stripping entries from #{input_ide_filename} => #{output_ide_filename}")
      gta_ide_file = File::open(input_ide_filename, 'r')
      nm_ide_file = File::open(output_ide_filename, 'w')

      # Build an array of just the names of the excluded objects.
      excluded_veh_names = []
      excluded_files.each do |line|
        SafeWriteLogFile(LOG_FILE, "Excluding #{line}")
        excluded_veh_names += [OS::Path::get_basename(line).downcase.split('.')[0]]
      end

      gta_ide_file.each_line do |line|
        # Only add those lines which don't refer to objects in the excluded list.
        veh_name = line.split(',')[0]
        next if excluded_veh_names.include?(veh_name.downcase)

        nm_ide_file.puts(line)
      end # iteration over every line in the full game IDE file.
    end


    ################################ EXCEPTIONS ################################
  rescue Exception => ex
    puts "\n#{g_AppName} unhandled exception: #{ex.message}"
    puts ex.backtrace.join("\n\t")
    GUI::ExceptionDialog::show_dialog( ex, "Exception caught in CreateReducedRpfFile(): #{ex.message}" )
    exit(ex.status) if('exit'==ex.message)
    Kernel::exit

  ensure # Do these clean up tasks whether an exception is thrown or not.
    # Delete the temporary directory used during unpacking.
    include_txt_file.close unless include_txt_file.nil?
    gta_ide_file.close unless gta_ide_file.nil?
    nm_ide_file.close unless nm_ide_file.nil?
    FileUtils::remove_dir(output_dir)
  end

end # CreateReducedRpfFile()

#
#
def CreateReducedZipFile(include_filename, input_rpf_filename, output_rpf_base, input_ide_filename)

  begin
    g_AppName = File::basename(__FILE__,'.rb')
    SafeWriteLogFile(LOG_FILE, "Creating Reduced ZIP from: #{input_rpf_filename}")

    c = Pipeline::Config::instance()
    project = c.projects['gta5']
    project.load_config()

    r = RageUtils::new(project)

    # Extract the files from the main RPF file into a temporary directory.
    output_dir = OS::Path::combine(output_rpf_base, 'extract_temp')
    extracted_files = []
    extracted_files += ProjectUtil::data_extract_zip(r, input_rpf_filename, output_dir, true) do |filename|
      puts "\t#{filename}"
    end

    # Load the list of vehicles for the test level.
    include_txt_file = File::open(include_filename, 'r')
    include_list = []
    include_txt_file.each_line do |line|
      # Add the first space-delineated word from a line which doesn't start with a comment "#".
      include_list += [(''+line).split(' ')[0].downcase] unless line.match('^[^a-z,A-Z]*#')
    end

    # Remove those files which aren't mentioned in the include_list array.
    excluded_files = []
    extracted_files.each do |filename|
      next unless(File::exists?(filename))
      basename = OS::Path::get_basename(filename)
      next if(include_list.include?(basename.downcase))
      # Don't want to exclude files within a directory which is related to the include objects.
      dirname = File::dirname(filename).split('/')[-1]
      next if(include_list.include?(dirname.downcase))

      excluded_files += [filename]
      FileUtils::remove(filename)
    end

    # Write to a temporary new RPF.
    rpf_file_basename = OS::Path::get_basename(input_rpf_filename)
    output_rpf_filename = OS::Path::combine(output_rpf_base, rpf_file_basename)
    output_rpf_filename += '_temp.zip'
    SafeWriteLogFile(LOG_FILE, "Output ZIP: #{output_rpf_filename}")

    # Package those files which are mentioned in the include_list array.
    msg = "\n***** CREATING ZIP *****"
    puts msg
    files = OS::FindEx::find_files_recurse(OS::Path::combine(output_dir, '*.*'))
    r.zip.start()
    files.each do |filename|
      # Find the temp directory name in the file path and remove everything before it to form
      # the RPF's internal file system.
      if(filename.split('/')[-2] == 'extract_temp')
        SafeWriteLogFile(LOG_FILE, "Adding #{OS::Path::get_filename(filename)}")
        r.zip.add(filename, OS::Path::get_filename(filename))
      else
        SafeWriteLogFile(LOG_FILE, "Adding #{OS::Path::combine(filename.split('/')[-2], OS::Path::get_filename(filename))}")
        r.zip.add(filename, OS::Path::combine(filename.split('/')[-2], OS::Path::get_filename(filename)))
      end
    end
    SafeWriteLogFile(LOG_FILE, "Saving ZIP.")
    r.zip.save(output_rpf_filename)
    SafeWriteLogFile(LOG_FILE, "Closing ZIP.")
    r.zip.close()

    # Convert independent RPF to platform specific versions.
    toolsroot = OS::Path.normalise(ENV['RS_TOOLSROOT'])
    toolslib = "#{toolsroot}/lib"
    msg = "\nConverting RPF file (#{output_rpf_filename}) to platform specific versions..."
    puts msg
    SafeWriteLogFile(LOG_FILE, msg)
    ProjectUtil::data_convert_file(output_rpf_filename)

    ################################ IDE FILE ################################

    # Copy the main IDE file and strip out all entries for excluded objects.
    if input_ide_filename != '' then
      output_ide_filename = 'temp.ide'
      SafeWriteLogFile(LOG_FILE, "Stripping entries from #{input_ide_filename} => #{output_ide_filename}")
      gta_ide_file = File::open(input_ide_filename, 'r')
      nm_ide_file = File::open(output_ide_filename, 'w')

      # Build an array of just the names of the excluded objects.
      excluded_veh_names = []
      excluded_files.each do |line|
        SafeWriteLogFile(LOG_FILE, "Excluding #{line}")
        excluded_veh_names += [OS::Path::get_basename(line).downcase.split('.')[0]]
      end

      gta_ide_file.each_line do |line|
        # Only add those lines which don't refer to objects in the excluded list.
        veh_name = line.split(',')[0]
        next if excluded_veh_names.include?(veh_name.downcase)

        nm_ide_file.puts(line)
      end # iteration over every line in the full game IDE file.
    end


    ################################ EXCEPTIONS ################################
  rescue Exception => ex
    puts "\n#{g_AppName} unhandled exception: #{ex.message}"
    puts ex.backtrace.join("\n\t")
    GUI::ExceptionDialog::show_dialog( ex, "Exception caught in CreateReducedRpfFile(): #{ex.message}" )
    exit(ex.status) if('exit'==ex.message)
    Kernel::exit

  ensure # Do these clean up tasks whether an exception is thrown or not.
    # Delete the temporary directory used during unpacking.
    include_txt_file.close unless include_txt_file.nil?
    gta_ide_file.close unless gta_ide_file.nil?
    nm_ide_file.close unless nm_ide_file.nil?
    FileUtils::remove_dir(output_dir)
  end

end # CreateReducedZipFile()


#----------------------------------------------------------------------------
# MAIN
#----------------------------------------------------------------------------
if (__FILE__ == $0) then

  begin
    g_AppName = File::basename(__FILE__,'.rb')

    # Initialise the logging system.
    SafeCreateLogFile(LOG_FILE)

    # Iterate through the different RPF and IDE files and call the function which
    # creates new, cut-down versions.
    for i in 0...INCLUDE_FILES.length
      include_filename = INCLUDE_FILES[i] + '.txt'

      input_rpf_filename = OS::Path::combine(INPUT_RPF_BASES[i], INPUT_RPF_FILENAMES[i])
      output_rpf_base = OUTPUT_RPF_BASES[i]

      input_ide_filename = OS::Path::combine(INPUT_IDE_BASES[i], INPUT_IDE_FILENAMES[i])
      output_ide_base = OUTPUT_IDE_BASES[i]

      CreateReducedRpfFile(include_filename, input_rpf_filename, output_rpf_base, input_ide_filename)

      if input_ide_filename != '' then
        # Check out IDE file if it exists and copy the temp IDE file over it.
        ide_file_basename = OS::Path::get_basename(input_ide_filename)
        output_ide_filename = OS::Path::combine(output_ide_base, ide_file_basename)
        output_ide_filename += '_temp.ide'

        move_command = OS::Path::combine(output_ide_base,OS::Path::get_basename(input_ide_filename))+'_temp.ide'
        puts "\nMoving temp.ide ==> " + move_command
        system 'move temp.ide ' + move_command
      end
    end
    
    # Loop through zip.
    INCLUDE_ZIP_FILES.each_with_index do |zip_file, i|
    
	  include_filename = "#{zip_file}.txt"

	  input_rpf_filename = OS::Path::combine(INPUT_ZIP_BASES[i], INPUT_ZIP_FILENAMES[i])
	  output_rpf_base = OUTPUT_ZIP_BASES[i]

	  input_ide_filename = OS::Path::combine(INPUT_IDE_ZIP_BASES[i], INPUT_IDE_ZIP_FILENAMES[i])
	  output_ide_base = OUTPUT_IDE_ZIP_BASES[i]

	  CreateReducedZipFile(include_filename, input_rpf_filename, output_rpf_base, input_ide_filename)

	  if input_ide_filename != '' then
	    # Check out IDE file if it exists and copy the temp IDE file over it.
	    ide_file_basename = OS::Path::get_basename(input_ide_filename)
	    output_ide_filename = OS::Path::combine(output_ide_base, ide_file_basename)
	    output_ide_filename += '_temp.ide'

	    move_command = OS::Path::combine(output_ide_base,OS::Path::get_basename(input_ide_filename))+'_temp.ide'
	    puts "\nMoving temp.ide ==> " + move_command
	    system 'move temp.ide ' + move_command
      end
    end

    # We're good if we got here, i.e. we didn't throw an exception.
    puts "\n\ndone. :)"
    Pipeline::GUI::MessageBox::information("#{g_AppName}", "Script completed successfully! :)")


    ################################ EXCEPTIONS ################################
  rescue SystemExit => ex # Allows script to exit quietly after fatal exceptions.
  rescue Exception => ex
    exit(ex.status) if('exit'==ex.message)
    puts "\n#{g_AppName} unhandled exception: #{ex.message}"
    puts ex.backtrace.join("\n\t")
    GUI::ExceptionDialog::show_dialog( ex, "Exception caught: #{ex.message}" )

  ensure # Do these clean up tasks whether an exception is thrown or not.
  end

end # Main
