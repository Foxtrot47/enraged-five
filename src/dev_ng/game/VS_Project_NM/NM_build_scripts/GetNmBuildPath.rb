#----------------------------------------------------------------------------
# Includes
#----------------------------------------------------------------------------
require 'pipeline/gui/exception_dialog'
require 'pipeline/gui/application'
include Pipeline


#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
BRANCH="dev"


def GetNmBuildPath()
  # Parse the file set_nm_branch_env__standalone.bat looking for a line like this:
  # SET RS_PROJROOT=X:\NM_NOV_2010
  # and return the normalised path to the NM build folder.

  nm_path = ""
  begin
    File.open("x:/gta5/src/#{BRANCH}/game/VS_Project_NM/set_nm_branch_env__standalone.bat", 'r') do |batFile|
      batFile.each_line do |line|
        next unless line.include?('SET RS_PROJROOT')
        line.chomp!

        # Split the line into two parts: the sub-strings on the left and right
        # of an equal sign and save the right hand side which should be the
        # NM build folder path.
        nm_path = line.split('=')[1]
      end
    end

  rescue LoadError => ex
    GUI::ExceptionDialog::show_dialog(ex, 'Load error in GetNmBuildPath()')
  rescue RuntimeError => ex
    GUI::ExceptionDialog::show_dialog(ex, 'Fatal exception in GetNmBuildPath()')
    Kernel::exit
  end

  OS::Path::set_downcase_on_normalise(false)
  nm_path = OS::Path::normalise(nm_path)
  return nm_path
end # GetNmBuildPath()


#------------------------------------------------------------------------------
# Test implementation
#------------------------------------------------------------------------------
if(__FILE__ == $0) then
  begin
    # Test the function above for parsing the NM build folder from a batch file.
    nm_folder = GetNmBuildPath()
    puts nm_folder

  rescue SystemExit => ex # Allows script to exit quietly after fatal exceptions.
  rescue Exception => ex
    GUI::ExceptionDialog::show_dialog( ex, 'Unhandled exception' )
  end # Main try-catch block.
end # Main.
