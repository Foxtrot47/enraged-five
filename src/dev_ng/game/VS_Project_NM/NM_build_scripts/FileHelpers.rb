# *************************************************************************
# *** Helper functions to support common tasks in the NM build pipeline ***
# *************************************************************************
#
# Author : Richard Archibald
# Started: 10/12/2010


module NmBuild

  # === Utility function for building the directory tree ===
  def CreateDirIfMissing(dirname)
    msg = "Creating #{dirname}"
    SafeWriteLogFile(LOG_FILE, msg)
    FileUtils::mkdir_p(dirname) unless File::exists?(dirname)
  end

  #------------------------------------------------------------------------------

  # === Utility function for copying files, avoiding the common causes of failure ===
  def SmartCopy(src, dest)
    msg = "Copying #{src} to #{dest}"
    puts msg
    SafeWriteLogFile(LOG_FILE, msg)

    OS::Path::set_downcase_on_normalise(false)

    # Check whether src represents an individual file or a directory.
    bSrcIsFile = File.file?(src)
    bSrcIsDir  = File.directory?(src)

    src_file = ''
    bSuccess = false
    if bSrcIsFile
      src_dir = OS::Path.get_directory(src)
      src_file = OS::Path.get_filename(src)
    elsif bSrcIsDir
      src_dir = OS::Path.get_directory(src)
      src_file = ''
    else
      puts "WARNING: src is neither a valid file or directory - skipping."
      SafeWriteLogFile(LOG_FILE, "SmartCopy() WARNING: #{src} is not a valid file or directory! Not copied.")
      return bSuccess
    end

    # A destination is defined as a directory if the last char is '/'. If the last
    # character is '/', strip it off and declare the remainder to be the destination
    # directory. Otherwise, assume "dest" contains directory and file so split into
    # directory and filename.
    # A destination is defined as a directory if the last char is '/'
    dest_dir = ''
    dest_file = ''
    if dest.split('').last == '/'
      dest_dir = dest.chomp('/')
      dest_file = src
    else
      dest_dir = OS::Path.get_directory(dest)
      dest_file = OS::Path.get_filename(dest)
    end

    # If the destination directory doesn't exist, create it and any others
    # above it in the directory tree.
    unless File.directory?(dest_dir)
      puts "creating dir"
      SafeWriteLogFile(LOG_FILE, "Destination director #{dest_dir} doesn't exist. Creating it...")
      FileUtils::mkdir_p(dest_dir)
    end

    # Actually do the copying.
    if bSrcIsFile
      SafeWriteLogFile(LOG_FILE, "Source is an individual file.")
      FileUtils::cp_r(src, dest, {:remove_destination=>true})
      SafeWriteLogFile(LOG_FILE, "File copied successfully.")
      bSuccess = true
    elsif bSrcIsDir
      SafeWriteLogFile(LOG_FILE, "Source is a directory.")
      FileUtils::cp_r(src, dest, {:remove_destination=>true})
      SafeWriteLogFile(LOG_FILE, "Directory copied successfully.")
      bSuccess = true
    else
      SafeWriteLogFile(LOG_FILE, "SmartCopy() ERROR: source is not a valid file or directory!")
      Kernel::exit # this time it is fatal, cause we should have returned above!
    end

    return bSuccess
  end

  #------------------------------------------------------------------------------

  # === Utility function to safely create a log file; the file will be closed in case of error ===
  def SafeCreateLogFile(logfilename)
    begin
      File.open(logfilename, "w") do |logFile|
        logFile.puts "===== Logfile started at #{Time.new} ====="
      end
    rescue Errno::EACCES => ex # The file permissions are set wrongly.
      puts "Problem trying to create the log file:"
      puts "Error type: #{ex.type}"
      puts "Error message: #{ex.message}"
      Pipeline::GUI::MessageBox::error("Error creating log file", "Couldn't create log file. Please check permissions are not READONLY on #{LOG_FILE} and delete file if necessary.")
      Kernel::exit()
    rescue SystemExit => ex # Allows script to exit quietly after fatal exceptions.
    rescue Exception => ex
      GUI::ExceptionDialog::show_dialog( ex, "Exception caught while creating the log file: #{ex.message}" )
      Kernel::exit()
    end
  end

  # === Utility function to safely write to the log file; the file will be closed in case of error ===
  def SafeWriteLogFile(logfilename, msg, timestamp=true)
    begin
      # Prepend the time to the message unless the user has disabled it in the function call.
      msg = "#{Time.new.strftime("(%H:%M:%S)")}: #{msg}" if timestamp
      File.open(logfilename, "a") do |logFile|
        logFile.puts msg
      end
    rescue Errno::EACCES => ex # The file permissions are set wrongly.
      puts "Problem trying to write to the log file:"
      puts "Error type: #{ex.type}"
      puts "Error message: #{ex.message}"
      puts "Log message: #{msg}"
      Pipeline::GUI::MessageBox::error("Error creating log file", "Couldn't write to log file. Please check permissions are not READONLY on #{logfilename} and delete file if necessary.")
      Kernel::exit()
    rescue SystemExit => ex # Allows script to exit quietly after fatal exceptions.
    rescue Exception => ex
      GUI::ExceptionDialog::show_dialog(ex, "Exception caught in SafeWriteLogFile(): #{ex.message}")
      Kernel::exit
    end
  end

end # module NmBuild.
