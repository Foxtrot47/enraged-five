# This will delete all compiler object files to cut down file size when sending
# the build to Natural Motion.


#----------------------------------------------------------------------------
# Includes
#----------------------------------------------------------------------------
require 'pipeline/gui/exception_dialog'
require 'pipeline/os/path'
require 'pipeline/gui/messagebox'

include Pipeline


#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
# Source base paths:
RAGE_PATH="../../rage"
GAME_PATH="../../game"
# The locations of vcproj directories containing compiler objects which can safely be deleted:
PATHS_TO_CLEAN =
[
 "#{RAGE_PATH}/framework/src/vcproj",
 "#{RAGE_PATH}/suite/src/vcproj",
 "#{RAGE_PATH}/base/src/vcproj",
 "#{RAGE_PATH}/scaleform/src/vcproj",
 "#{RAGE_PATH}/naturalmotion/vcproj",
 "#{GAME_PATH}/VS_Project/RageMisc"
]
# The file types for removal:
FILE_EXTENSIONS = ['lib', 'obj', 'idb', 'pdb', 'd']


#----------------------------------------------------------------------------
# Main
#----------------------------------------------------------------------------
if(__FILE__ == $0)
  # Clean out all object files which can be recompiled in the new location.
  begin
    g_AppName = File::basename(__FILE__, '.rb')
    OS::Path::set_downcase_on_normalise(false)

    puts "Cleaning compiled object files and libs: #{File::expand_path(RAGE_PATH)}"
    FILE_EXTENSIONS.each do |file_ext|
      PATHS_TO_CLEAN.each do |path_to_clean|
        path_to_clean = File::expand_path(path_to_clean)
        puts "Removing *." + file_ext + " files from #{path_to_clean}"
        files = OS::FindEx::find_files_recurse("#{path_to_clean}/*.#{file_ext}")
        FileUtils::rm_f(files)
      end # Loop over file extensions.
    end # Loop over paths.

    puts "Removing user specific Visual Studio files:"
    FileUtils::rm_f("#{GAME_PATH}/VS_Project_NM/game_2008.ncb", :verbose=>true)
    FileUtils::rm_f("#{GAME_PATH}/VS_Project_NM/game_2008.suo", :verbose=>true)
    files = OS::FindEx::find_files("#{GAME_PATH}/VS_Project_NM/game_2008.vcproj.ROCKSTAR.*")
    FileUtils::rm_f(files, :verbose=>true) unless files.size()==0
    
    # Assume all is well if we got here.
    puts "Done. :-)"
    Pipeline::GUI::MessageBox::information("#{g_AppName}", "Script completed successfully! :)")

  rescue Exception => ex
    GUI::ExceptionDialog::show_dialog( ex, 'Unknown exception while deleting object files from RAGE tree.' )
    Kernel::exit
  end
end
