@echo off

pushd "%~dp0"


CALL set_nm_branch_env__standalone.bat

REM Detect if current SDK is either unset or older than 430 and switch.
IF NOT EXIST %SCE_PS3_ROOT%\info\old\420.001\Bugfix_SDK_e.txt set SCE_PS3_ROOT=%RS_PROJROOT%/ps3sdk/dev/usr/local/430_001/cell
ECHO SCE_PS3_ROOT: 	%SCE_PS3_ROOT%

start "" %cd%\gameNM_2010_unity.sln

popd
