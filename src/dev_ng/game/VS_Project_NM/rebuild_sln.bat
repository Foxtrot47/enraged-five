@echo off
setlocal

if "%1"=="VS2010" goto VS2010
%RS_TOOLSROOT%\script\coding\projbuild\helper.rb --project=game_sln --in_format=ragegen --out_type=solution type=solution
if "%1"=="VS2008" goto END
:VS2010
REM Build the Game normally ( not for distribution )
call %RS_TOOLSROOT%\script\util\projgen\rebuildGameNMLibrary.bat game.slndef %2 %3

REM Build the Game for NM distribution...
call %RS_TOOLSROOT%\script\util\projgen\rebuildGameNMLibrary.bat gameNM.slndef %2 %3

:END

IF "%PAUSEOPTION%"=="" PAUSE
