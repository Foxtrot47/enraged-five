@echo off
REM  Check for any differences between files at head of shared NM Perforce server in NYC and the files integrated
REM  to the RAGE proxy here at North.

echo Testing for differences between NM shared perforce server in NYC and local dump file...

ruby.exe x:\gta5\tools\lib\util\perforce\p4_diff.rb --brief --source_port=rsgnycp4p1:1669 --source_client=EDIW-RARCHIW7_NM --source_user=rarchibald --source_path=//depot/branches/north/...  --target_port=rsgedip4proxy01:1667 --target_client=EDIW-RARCHIW7_2 --target_user=richard.archibald --target_path=//rage/gta5/dev_nm/rage/naturalmotion/dump/... //depot/branches/north/... > C:\Users\richard.archibald\Desktop\NM_diff_tmp.txt

echo Generating "NM_PERFORCE_DIFF_REPORT.txt" which will contain output if any diffs were found...
grep -Hni "text files differ" C:\Users\richard.archibald\Desktop\NM_diff_tmp.txt > C:\Users\richard.archibald\Desktop\NM_PERFORCE_DIFF_REPORT.txt

PAUSE

del c:\Users\richard.archibald\Desktop\NM_diff_tmp.txt
