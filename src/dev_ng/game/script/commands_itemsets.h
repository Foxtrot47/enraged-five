// 
// script/commands_itemsets.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SCRIPT_COMMANDS_ITEMSETS_H
#define SCRIPT_COMMANDS_ITEMSETS_H

#include "script/script_itemsets.h"

//-----------------------------------------------------------------------------

namespace itemsets_commands
{
	void SetupScriptCommands();
}	// namespace itemsets_commands

//-----------------------------------------------------------------------------

#endif	// SCRIPT_COMMANDS_ITEMSETS_H

// End of file script/command_itemsets.h
