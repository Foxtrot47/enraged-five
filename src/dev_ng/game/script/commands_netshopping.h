//
// filename:	commands_netshopping.h
// description:	
//

#ifndef INC_COMMANDS_NETSHOPPING_H_
#define INC_COMMANDS_NETSHOPPING_H_

// --- Include Files ------------------------------------------------------------

// C headers
// Rage headers
// Game headers

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

namespace netshopping_commands
{
	void  SetupScriptCommands();
	bool  CommandNetGameServerIsCatalogValid( );
}


#endif // !INC_COMMANDS_NETSHOPPING_H_
