#ifndef _COMMANDS_INTERIOR_H_
#define _COMMANDS_INTERIOR_H_

namespace interior_commands
{
	void SetupScriptCommands();
	int GetProxyAtCoords(const scrVector & scrVecInCoors, int typeKey);
}

#endif	//	_COMMANDS_INTERIOR_H_

