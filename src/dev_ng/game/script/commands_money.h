//
// filename:	commands_money.h
// description:	
//

#ifndef INC_COMMANDS_MONEY_H_
#define INC_COMMANDS_MONEY_H_

// --- Include Files ------------------------------------------------------------

// C headers
// Rage headers
// Game headers

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

namespace money_commands
{
	void SetupScriptCommands();
}


#endif // !INC_COMMANDS_MONEY_H_
