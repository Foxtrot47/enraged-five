//
// filename:	commands_extrametadata.h
// description:	
//

#ifndef INC_COMMANDS_EXTRAMETADATA_H_
#define INC_COMMANDS_EXTRAMETADATA_H_

// --- Include Files ------------------------------------------------------------


// --- Globals ------------------------------------------------------------------

namespace extrametadata_commands
{
	void SetupScriptCommands();
}


#endif // INC_COMMANDS_EXTRAMETADATA_H_
