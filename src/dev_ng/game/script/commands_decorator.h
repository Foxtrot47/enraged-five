#ifndef _COMMANDS_DECORATOR_H_
#define _COMMANDS_DECORATOR_H_

namespace decorator_commands
{
	void Shutdown(u32 shutdownMode);

    void SetupScriptCommands();
} // namespace decorator_commands

#endif