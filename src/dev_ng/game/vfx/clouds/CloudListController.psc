<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef name="CloudListController" type="rage::CloudListController" onPostLoad="PostLoad" onPreLoad="PreLoad">
	<array name="mProbability" type="atArray">
		<u32/>
	</array>
	<bitset name="mBits" type="atBitSet"/>
</structdef>

</ParserSchema>