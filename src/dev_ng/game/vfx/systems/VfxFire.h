///////////////////////////////////////////////////////////////////////////////
//  
//	FILE: 	VfxFire.h
//	BY	: 	Mark Nicholson
//	FOR	:	Rockstar North
//	ON	:	04 Jun 2007
//	WHAT:	
//
///////////////////////////////////////////////////////////////////////////////

#ifndef VFX_FIRE_H
#define	VFX_FIRE_H


///////////////////////////////////////////////////////////////////////////////
//  INCLUDES
///////////////////////////////////////////////////////////////////////////////													

// rage
#include "atl/hashstring.h"

// game
#include "Physics/GtaMaterial.h"


///////////////////////////////////////////////////////////////////////////////
//  FORWARD DECLARATIONS
///////////////////////////////////////////////////////////////////////////////	

class CFire;
class CPed;


///////////////////////////////////////////////////////////////////////////////
//  DEFINES
///////////////////////////////////////////////////////////////////////////////													


///////////////////////////////////////////////////////////////////////////////
//  ENUMS
///////////////////////////////////////////////////////////////////////////////	

// fire ptfx offsets for registered systems that play on the same triggered fire
enum PtFxFireOffsets_e
{
	PTFXOFFSET_FIRE_STEAM				= 0,
	PTFXOFFSET_FIRE_FLAMES
};


///////////////////////////////////////////////////////////////////////////////
//  STRUCTURES
///////////////////////////////////////////////////////////////////////////////	

struct VfxFireInfo_s
{
	float						flammability;
	float						burnTime;
	float						burnStrength;

	atHashWithStringNotFinal	ptFxMapFireHashName;

};


///////////////////////////////////////////////////////////////////////////////
//  CLASSES
///////////////////////////////////////////////////////////////////////////////

class CVfxFire
{	
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		void				Init						(unsigned initMode);
		void				Shutdown					(unsigned shutdownMode);

#if __BANK
		void				InitWidgets					();
#endif

		// data table access
		VfxFireInfo_s*		GetInfo						(VfxGroup_e vfxGroup)		{return &m_vfxFireInfos[vfxGroup];}

		void				ProcessPedFire				(CFire* pFire, CPed* pPed);

		// particle systems
		void				UpdatePtFxFireMap			(CFire* pFire);
		void				UpdatePtFxFirePetrolTrail	(CFire* pFire);
		void				UpdatePtFxFirePetrolPool	(CFire* pFire);
		void				UpdatePtFxFireEntity		(CFire* pFire);
		void				UpdatePtFxFireSteam			(CFire* pFire);
		void				UpdatePtFxFireVehWrecked	(CFire* pFire);
		void				UpdatePtFxFireVehWheel		(CFire* pFire);
		void				UpdatePtFxFirePetrolTank	(CFire* pFire);
		void				UpdatePtFxFirePedBone		(CFire* pFire, s32 id, s32 boneTag1, s32 boneTag2, atHashWithStringNotFinal ptFxHashName, float speedEvoMin, float speedEvoMax);

		void				TriggerPtFxPedFireSmoulder	(CPed* pPed);


	private: //////////////////////////

		// init helper functions
		void				LoadData					();


	///////////////////////////////////
	// VARIABLES
	///////////////////////////////////

	public: ///////////////////////////


	private: //////////////////////////

		// file version
		float				m_version;

		// wheel data table
		VfxFireInfo_s		m_vfxFireInfos				[NUM_VFX_GROUPS];


}; // CVfxFire


///////////////////////////////////////////////////////////////////////////////
//  EXTERNS
///////////////////////////////////////////////////////////////////////////////

extern	CVfxFire		g_vfxFire;


#endif // VFX_ENTITY_H


///////////////////////////////////////////////////////////////////////////////
