///////////////////////////////////////////////////////////////////////////////
//  
//	FILE: 	PtFxDefines.h
//	BY	: 	Mark Nicholson
//	FOR	:	Rockstar North
//	ON	:	18 Jun 2008
//	WHAT:	
//
///////////////////////////////////////////////////////////////////////////////

#ifndef PTFX_DEFINES_H
#define	PTFX_DEFINES_H


///////////////////////////////////////////////////////////////////////////////
//  INCLUDES
///////////////////////////////////////////////////////////////////////////////													

// rage
#include "rmptfx/ptxconfig.h"


///////////////////////////////////////////////////////////////////////////////
//  DEFINES
///////////////////////////////////////////////////////////////////////////////													

// compile flags
#define PTFX_ALLOW_INSTANCE_SORTING			(1)
#define PTFX_ENABLE_ATTACH_CHECK			(0)
#define PTFX_RENDER_IN_MIRROR_REFLECTIONS	(1)

// rmptfx sizes
#define PTFX_MAX_PARTICLES			(5*1024)
#define PTFX_MAX_INSTS				(384)
#define PTFX_MAX_TRAILS				(0)

// ptfx sizes
#define PTFX_MAX_REGISTERED			(256)
#define	PTFX_MAX_SCRIPTED			(128)
#define PTFX_MAX_HISTORIES			(256)

#endif // PTFX_DEFINES_H







