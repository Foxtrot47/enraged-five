
#ifndef __UNITYBUILD
# define __UNITYBUILD
#endif //
#include "forceinclude/_unity_prologue.h"
#include "../../camera/CamInterface.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/CinematicDirector.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/animated/CinematicAnimatedCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/orbit/CinematicVehicleOrbitCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/mounted/CinematicMountedCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/mounted/CinematicMountedPartCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/tracking/BaseCinematicTrackingCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/tracking/CinematicCamManCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/tracking/CinematicFirstPersonIdleCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/tracking/CinematicGroupCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/tracking/CinematicIdleCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/tracking/CinematicHeliChaseCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/tracking/CinematicPedCloseUpCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/tracking/CinematicPositionCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/tracking/CinematicStuntCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/tracking/CinematicTrainTrackCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/tracking/CinematicTwoShotCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/tracking/CinematicVehicleTrackingCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/camera/tracking/CinematicWaterCrashCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/BaseCinematicContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicBustedContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicOnFootIdleContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicInVehicleContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicInVehicleFirstPersonContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicInVehicleOverriddenFirstPersonContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicInVehicleWantedContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicFallFromHeliContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicMissileKillContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicWaterCrashContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicOnFootAssistedAimingContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicOnFootMeleeContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicOnFootSpectatingContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicOnFootWantedContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicParachuteContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicScriptContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicSpectatorNewsChannelContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/context/CinematicStuntJumpContext.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/BaseCinematicShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicBustedShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicOnFootAssistedAimingShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicOnFootFirstPersonIdleShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicOnFootIdleShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicOnFootMeleeShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicOnFootSpectatingShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicInVehicleShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicInVehicleGroupShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicInVehicleWantedShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicFallFromHeliShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicMissileKillShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicWaterCrashShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicParachuteShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicScriptShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicStuntJumpShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cinematic/shot/CinematicTrainShot.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/animscene/AnimSceneDirector.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/base/BaseCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/base/BaseDirector.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/base/BaseObject.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cutscene/AnimatedCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/cutscene/CutsceneDirector.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/debug/DebugDirector.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/debug/FreeCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/debug/ShakeDebugger.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/GameplayDirector.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/ThirdPersonCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/aim/AimCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/aim/FirstPersonAimCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/aim/FirstPersonHeadTrackingAimCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/aim/FirstPersonPedAimCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/aim/FirstPersonShooterCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/aim/ThirdPersonAimCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/aim/ThirdPersonPedAimCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/aim/ThirdPersonPedAimInCoverCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/aim/ThirdPersonPedAssistedAimCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/aim/ThirdPersonPedMeleeAimCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/aim/ThirdPersonVehicleAimCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/follow/FollowCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/follow/FollowObjectCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/follow/FollowParachuteCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/follow/FollowPedCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/follow/FollowVehicleCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/gameplay/follow/TableGamesCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/AnimatedFrameShaker.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/BaseFrameShaker.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/CatchUpHelper.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/Collision.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/ControlHelper.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/DampedSpring.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/Envelope.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/Frame.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/FrameInterpolator.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/FramePropagator.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/FrameShaker.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/HandShaker.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/HintHelper.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/InconsistentBehaviourZoomHelper.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/Interpolator.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/LookAheadHelper.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/LookAtDampingHelper.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/NearClipScanner.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/Oscillator.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/SplineNode.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/SpringMount.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/StickyAimHelper.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/ThirdPersonFrameInterpolator.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/switch/BaseSwitchHelper.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/switch/LongSwoopSwitchHelper.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/switch/ShortRotationSwitchHelper.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/switch/ShortTranslationSwitchHelper.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/switch/ShortZoomInOutSwitchHelper.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/helpers/switch/ShortZoomToHeadSwitchHelper.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/marketing/MarketingAToBCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/marketing/MarketingDirector.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/marketing/MarketingFreeCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/marketing/MarketingMountedCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/marketing/MarketingOrbitCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/marketing/MarketingStickyCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/replay/ReplayBaseCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/replay/ReplayDirector.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/replay/ReplayRecordedCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/replay/ReplayFreeCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/replay/ReplayPresetCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/scripted/BaseSplineCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/scripted/CustomTimedSplineCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/scripted/RoundedSplineCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/scripted/ScriptDirector.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/scripted/ScriptedCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/scripted/ScriptedFlyCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/scripted/SmoothedSplineCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/scripted/TimedSplineCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/syncedscene/SyncedSceneDirector.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/switch/SwitchCamera.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/switch/SwitchDirector.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/system/CameraMetadata.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/system/CameraFactory.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/system/CameraManager.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/system/debug/CameraDebugInfo.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/viewports/Viewport.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/viewports/ViewportFader.cpp"
#include "forceinclude/_unity_epilogue.h"
#include "forceinclude/_unity_prologue.h"
#include "../../camera/viewports/ViewportManager.cpp"
#include "forceinclude/_unity_epilogue.h"
