<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <enumdef type="eVehicleModType">
    <enumval name="VMT_SPOILER"/>
    <enumval name="VMT_BUMPER_F"/>
    <enumval name="VMT_BUMPER_R"/>
    <enumval name="VMT_SKIRT"/>
    <enumval name="VMT_EXHAUST"/>
    <enumval name="VMT_CHASSIS"/>
    <enumval name="VMT_GRILL"/>
    <enumval name="VMT_BONNET"/>
    <enumval name="VMT_WING_L"/>
    <enumval name="VMT_WING_R"/>
    <enumval name="VMT_ROOF"/>

    <enumval name="VMT_PLTHOLDER" />
    <enumval name="VMT_PLTVANITY" />

    <enumval name="VMT_INTERIOR1" />
    <enumval name="VMT_INTERIOR2" />
    <enumval name="VMT_INTERIOR3" />
    <enumval name="VMT_INTERIOR4" />
    <enumval name="VMT_INTERIOR5" />
    <enumval name="VMT_SEATS" />
    <enumval name="VMT_STEERING" />
    <enumval name="VMT_KNOB" />
    <enumval name="VMT_PLAQUE" />
    <enumval name="VMT_ICE" />

    <enumval name="VMT_TRUNK" />
    <enumval name="VMT_HYDRO" />

    <enumval name="VMT_ENGINEBAY1" />
    <enumval name="VMT_ENGINEBAY2" />
    <enumval name="VMT_ENGINEBAY3" />

    <enumval name="VMT_CHASSIS2" />
    <enumval name="VMT_CHASSIS3" />
    <enumval name="VMT_CHASSIS4" />
    <enumval name="VMT_CHASSIS5" />


    <enumval name="VMT_DOOR_L" />
    <enumval name="VMT_DOOR_R" />

    <enumval name="VMT_LIVERY_MOD" />
    <enumval name="VMT_LIGHTBAR" />

    
    <enumval name="VMT_ENGINE"/>
    <enumval name="VMT_BRAKES"/>
    <enumval name="VMT_GEARBOX"/>
    <enumval name="VMT_HORN"/>
    <enumval name="VMT_SUSPENSION"/>
    <enumval name="VMT_ARMOUR"/>

    <enumval name="VMT_NITROUS"/>
    <enumval name="VMT_TURBO"/>
    <enumval name="VMT_SUBWOOFER"/>
    <enumval name="VMT_TYRE_SMOKE"/>
    <enumval name="VMT_HYDRAULICS"/>
    <enumval name="VMT_XENON_LIGHTS"/>

    <enumval name="VMT_WHEELS"/>
    <enumval name="VMT_WHEELS_REAR_OR_HYDRAULICS"/>
  </enumdef>

  <const name="VEH_CHASSIS" value="0"/>
  <const name="VEH_BODYSHELL" value="48"/>
  <const name="VEH_BUMPER_F" value="49"/>
  <const name="VEH_BUMPER_R" value="50"/>
  <const name="VEH_WING_RF" value="51"/>
  <const name="VEH_WING_LF" value="52"/>
  <const name="VEH_BONNET" value="53"/>
  <const name="VEH_BOOT" value="54"/>
  <const name="VEH_BOOT_2" value="55"/>
  <const name="VEH_EXHAUST" value="56"/>
  <const name="VEH_EXHAUST_2" value="57"/>
  <const name="VEH_EXHAUST_3" value="58"/>
  <const name="VEH_EXHAUST_4" value="59"/>
  <const name="VEH_EXHAUST_5" value="60"/>
  <const name="VEH_EXHAUST_6" value="61"/>
  <const name="VEH_EXHAUST_7" value="62"/>
  <const name="VEH_EXHAUST_8" value="63"/>
  <const name="VEH_EXHAUST_9" value="64"/>
  <const name="VEH_EXHAUST_10" value="65"/>
  <const name="VEH_EXHAUST_11" value="66"/>
  <const name="VEH_EXHAUST_12" value="67"/>
  <const name="VEH_EXHAUST_13" value="68"/>
  <const name="VEH_EXHAUST_14" value="69"/>
  <const name="VEH_EXHAUST_15" value="70"/>
  <const name="VEH_EXHAUST_16" value="71"/>
  <const name="VEH_EXHAUST_17" value="72"/>
  <const name="VEH_EXHAUST_18" value="73"/>
  <const name="VEH_EXHAUST_19" value="74"/>
  <const name="VEH_EXHAUST_20" value="75"/>
  <const name="VEH_EXHAUST_21" value="76"/>
  <const name="VEH_EXHAUST_22" value="77"/>
  <const name="VEH_EXHAUST_23" value="78"/>
  <const name="VEH_EXHAUST_24" value="79"/>
  <const name="VEH_EXHAUST_25" value="80"/>
  <const name="VEH_EXHAUST_26" value="81"/>
  <const name="VEH_EXHAUST_27" value="82"/>
  <const name="VEH_EXHAUST_28" value="83"/>
  <const name="VEH_EXHAUST_29" value="84"/>
  <const name="VEH_EXHAUST_30" value="85"/>
  <const name="VEH_EXHAUST_31" value="86"/>
  <const name="VEH_EXHAUST_32" value="87"/>
  <const name="VEH_EXTRA_1" value="420"/>
  <const name="VEH_EXTRA_2" value="421"/>
  <const name="VEH_EXTRA_3" value="422"/>
  <const name="VEH_EXTRA_4" value="423"/>
  <const name="VEH_EXTRA_5" value="424"/>
  <const name="VEH_EXTRA_6" value="425"/>
  <const name="VEH_EXTRA_7" value="426"/>
  <const name="VEH_EXTRA_8" value="427"/>
  <const name="VEH_EXTRA_9" value="428"/>
  <const name="VEH_EXTRA_10" value="429"/>
  <const name="VEH_EXTRA_11" value="430"/>
  <const name="VEH_EXTRA_12" value="431"/>
  <const name="VEH_EXTRA_13" value="432"/>
  <const name="VEH_EXTRA_14" value="433"/>
  <const name="VEH_EXTRA_15" value="434"/>
  <const name="VEH_EXTRA_16" value="435"/>
  <const name="VEH_BREAKABLE_EXTRA_1" value="436"/>
  <const name="VEH_BREAKABLE_EXTRA_2" value="437"/>
  <const name="VEH_BREAKABLE_EXTRA_3" value="438"/>
  <const name="VEH_BREAKABLE_EXTRA_4" value="439"/>
  <const name="VEH_BREAKABLE_EXTRA_5" value="440"/>
  <const name="VEH_BREAKABLE_EXTRA_6" value="441"/>
  <const name="VEH_BREAKABLE_EXTRA_7" value="442"/>
  <const name="VEH_BREAKABLE_EXTRA_8" value="443"/>
  <const name="VEH_BREAKABLE_EXTRA_9" value="444"/>
  <const name="VEH_BREAKABLE_EXTRA_10" value="445"/>
  <const name="VEH_MOD_COLLISION_1" value="446"/>
  <const name="VEH_MOD_COLLISION_2" value="447"/>
  <const name="VEH_MOD_COLLISION_3" value="448"/>
  <const name="VEH_MOD_COLLISION_4" value="449"/>
  <const name="VEH_MOD_COLLISION_5" value="450"/>
  <const name="VEH_MOD_COLLISION_6" value="451"/>
  <const name="VEH_MOD_COLLISION_7" value="452"/>
  <const name="VEH_MOD_COLLISION_8" value="453"/>
  <const name="VEH_MOD_COLLISION_9" value="454"/>
  <const name="VEH_MOD_COLLISION_10" value="455"/>
  <const name="VEH_MOD_COLLISION_11" value="456"/>
  <const name="VEH_MOD_COLLISION_12" value="457"/>
  <const name="VEH_MOD_COLLISION_13" value="458"/>
  <const name="VEH_MOD_COLLISION_14" value="459"/>
  <const name="VEH_MOD_COLLISION_15" value="460"/>
  <const name="VEH_MOD_COLLISION_16" value="461"/>
  <const name="VEH_SPOILER" value="381"/>
  <const name="VEH_MISC_A" value="385"/>
  <const name="VEH_MISC_B" value="386"/>
  <const name="VEH_MISC_C" value="387"/>
  <const name="VEH_MISC_D" value="388"/>
  <const name="VEH_MISC_E" value="389"/>
  <const name="VEH_MISC_F" value="390"/>
  <const name="VEH_MISC_G" value="391"/>
  <const name="VEH_MISC_H" value="392"/>
  <const name="VEH_MISC_I" value="393"/>
  <const name="VEH_MISC_J" value="394"/>
  <const name="VEH_MISC_K" value="395"/>
  <const name="VEH_MISC_L" value="396"/>
  <const name="VEH_MISC_M" value="397"/>
  <const name="VEH_MISC_N" value="398"/>
  <const name="VEH_MISC_O" value="399"/>
  <const name="VEH_MISC_P" value="400"/>
  <const name="VEH_MISC_Q" value="401"/>
  <const name="VEH_MISC_R" value="402"/>
  <const name="VEH_MISC_S" value="403"/>
  <const name="VEH_MISC_T" value="404"/>
  <const name="VEH_MISC_U" value="405"/>
  <const name="VEH_MISC_V" value="406"/>
  <const name="VEH_MISC_W" value="407"/>
  <const name="VEH_MISC_X" value="408"/>
  <const name="VEH_MISC_Y" value="409"/>
  <const name="VEH_MISC_Z" value="410"/>
  <const name="VEH_MISC_1" value="411"/>
  <const name="VEH_MISC_2" value="412"/>
  <const name="VEH_MISC_3" value="413"/>
  <const name="VEH_MISC_4" value="414"/>
  <const name="VEH_MISC_5" value="415"/>
  <const name="VEH_MISC_6" value="416"/>
  <const name="VEH_MISC_7" value="417"/>
  <const name="VEH_MISC_8" value="418"/>
  <const name="VEH_MISC_9" value="419"/>
  <const name="VEH_NEON_L" value="120"/>
  <const name="VEH_NEON_R" value="121"/>
  <const name="VEH_NEON_F" value="122"/>
  <const name="VEH_NEON_B" value="123"/>
  <const name="VEH_STEERING_WHEEL" value="95"/>
  <const name="VEH_CAR_STEERING_WHEEL" value="96"/>
  <const name="VEH_TRANSMISSION_R" value="29"/>
  <const name="VEH_SUSPENSION_LF" value="21"/>
  <const name="VEH_SUSPENSION_RF" value="22"/>
  <const name="VEH_HEADLIGHT_L" value="107"/>
  <const name="VEH_HEADLIGHT_R" value="108"/>
  <const name="VEH_INDICATOR_LR" value="113"/>
  <const name="VEH_INDICATOR_LF" value="111"/>
  <const name="VEH_INDICATOR_RR" value="114"/>
  <const name="VEH_INDICATOR_RF" value="112"/>
  <const name="VEH_TAILLIGHT_L" value="109"/>
  <const name="VEH_TAILLIGHT_R" value="110"/>
  <const name="VEH_REVERSINGLIGHT_L" value="118"/>
  <const name="VEH_REVERSINGLIGHT_R" value="119"/>
  <const name="VEH_WINDOW_LF" value="42"/>
  <const name="VEH_WINDOW_RF" value="43"/>
  <const name="VEH_WINDOW_RR" value="45"/>
  <const name="VEH_WINDOW_LR" value="44"/>
  <const name="VEH_WINDOW_LM" value="46"/>
  <const name="VEH_WINDOW_RM" value="47"/>
  <const name="VEH_WHEELHUB_LF" value="30"/>
  <const name="VEH_WHEELHUB_RF" value="31"/>
  <const name="VEH_WINDSCREEN_R" value="41"/>
  <const name="VEH_DOOR_DSIDE_F" value="3"/>
  <const name="VEH_DOOR_DSIDE_R" value="4"/>
  <const name="VEH_DOOR_PSIDE_F" value="5"/>
  <const name="VEH_DOOR_PSIDE_R" value="6"/>
  <const name="VEH_BOBBLE_HEAD" value="377"/>
  <const name="VEH_BOBBLE_BASE" value="378"/>
  <const name="VEH_BOBBLE_HAND" value="379"/>
  <const name="VEH_BOBBLE_ENGINE" value="380"/>
  <const name="VEH_MOD_A" value="493"/>
  <const name="VEH_MOD_B" value="494"/>
  <const name="VEH_MOD_C" value="495"/>
  <const name="VEH_MOD_D" value="496"/>
  <const name="VEH_MOD_E" value="497"/>
  <const name="VEH_MOD_F" value="498"/>
  <const name="VEH_MOD_G" value="499"/>
  <const name="VEH_MOD_H" value="500"/>
  <const name="VEH_MOD_I" value="501"/>
  <const name="VEH_MOD_J" value="502"/>
  <const name="VEH_MOD_K" value="503"/>
  <const name="VEH_MOD_L" value="504"/>
  <const name="VEH_MOD_M" value="505"/>
  <const name="VEH_MOD_N" value="506"/>
  <const name="VEH_MOD_O" value="507"/>
  <const name="VEH_MOD_P" value="508"/>
  <const name="VEH_MOD_Q" value="509"/>
  <const name="VEH_MOD_R" value="510"/>
  <const name="VEH_MOD_S" value="511"/>
  <const name="VEH_MOD_T" value="512"/>
  <const name="VEH_MOD_U" value="513"/>
  <const name="VEH_MOD_V" value="514"/>
  <const name="VEH_MOD_W" value="515"/>
  <const name="VEH_MOD_X" value="516"/>
  <const name="VEH_MOD_Y" value="517"/>
  <const name="VEH_MOD_Z" value="518"/>
  <const name="VEH_MOD_AA" value="519"/>
  <const name="VEH_MOD_AB" value="520"/>
  <const name="VEH_MOD_AC" value="521"/>
  <const name="VEH_MOD_AD" value="522"/>
  <const name="VEH_MOD_AE" value="523"/>
  <const name="VEH_MOD_AF" value="524"/>
  <const name="VEH_MOD_AG" value="525"/>
  <const name="VEH_MOD_AH" value="526"/>
  <const name="VEH_MOD_AI" value="527"/>
  <const name="VEH_MOD_AJ" value="528"/>
  <const name="VEH_MOD_AK" value="529"/>

  <const name="VEH_TURRET_A1_MOD" value="530"/>
  <const name="VEH_TURRET_A2_MOD" value="531"/>
  <const name="VEH_TURRET_A3_MOD" value="532"/>
  <const name="VEH_TURRET_A4_MOD" value="533"/>

  <const name="VEH_TURRET_A1_BASE" value="534"/>
  <const name="VEH_TURRET_A1_BARREL" value="535"/>
  <const name="VEH_TURRET_A2_BASE" value="536"/>
  <const name="VEH_TURRET_A2_BARREL" value="537"/>
  <const name="VEH_TURRET_A3_BASE" value="538"/>
  <const name="VEH_TURRET_A3_BARREL" value="539"/>
  <const name="VEH_TURRET_A4_BASE" value="540"/>
  <const name="VEH_TURRET_A4_BARREL" value="541"/>
  <const name="VEH_TURRET_A_AMMO_BELT" value="542"/>

  <const name="VEH_TURRET_B1_MOD" value="543"/>
  <const name="VEH_TURRET_B2_MOD" value="544"/>
  <const name="VEH_TURRET_B3_MOD" value="545"/>
  <const name="VEH_TURRET_B4_MOD" value="546"/>

  <const name="VEH_TURRET_B1_BASE" value="547"/>
  <const name="VEH_TURRET_B1_BARREL" value="548"/>
  <const name="VEH_TURRET_B2_BASE" value="549"/>
  <const name="VEH_TURRET_B2_BARREL" value="550"/>
  <const name="VEH_TURRET_B3_BASE" value="551"/>
  <const name="VEH_TURRET_B3_BARREL" value="552"/>
  <const name="VEH_TURRET_B4_BASE" value="553"/>
  <const name="VEH_TURRET_B4_BARREL" value="554"/>
  <const name="VEH_TURRET_B_AMMO_BELT" value="555"/>

  <const name="VEH_BLADE_R_1_MOD" value="579"/>
  <const name="VEH_BLADE_R_1_FAST" value="580"/>
  <const name="VEH_BLADE_R_2_MOD" value="581"/>
  <const name="VEH_BLADE_R_2_FAST" value="582"/>
  <const name="VEH_BLADE_R_3_MOD" value="583"/>
  <const name="VEH_BLADE_R_3_FAST" value="584"/>
  <const name="VEH_BLADE_F_1_MOD" value="585"/>
  <const name="VEH_BLADE_F_1_FAST" value="586"/>
  <const name="VEH_BLADE_F_2_MOD" value="587"/>
  <const name="VEH_BLADE_F_2_FAST" value="588"/>
  <const name="VEH_BLADE_F_3_MOD" value="589"/>
  <const name="VEH_BLADE_F_3_FAST" value="590"/>
  <const name="VEH_BLADE_S_1_MOD" value="591"/>
  <const name="VEH_BLADE_S_1_L_FAST" value="592"/>
  <const name="VEH_BLADE_S_1_R_FAST" value="593"/>
  <const name="VEH_BLADE_S_2_MOD" value="594"/>
  <const name="VEH_BLADE_S_2_L_FAST" value="595"/>
  <const name="VEH_BLADE_S_2_R_FAST" value="596"/>
  <const name="VEH_BLADE_S_3_MOD" value="597"/>
  <const name="VEH_BLADE_S_3_L_FAST" value="598"/>
  <const name="VEH_BLADE_S_3_R_FAST" value="599"/>
  <const name="VEH_SPIKE_1_MOD" value="600"/>
  <const name="VEH_SPIKE_1_PED" value="601"/>
  <const name="VEH_SPIKE_1_CAR" value="602"/>
  <const name="VEH_SPIKE_2_MOD" value="603"/>
  <const name="VEH_SPIKE_2_PED" value="604"/>
  <const name="VEH_SPIKE_2_CAR" value="605"/>
  <const name="VEH_SPIKE_3_MOD" value="606"/>
  <const name="VEH_SPIKE_3_PED" value="607"/>
  <const name="VEH_SPIKE_3_CAR" value="608"/>
  <const name="VEH_SCOOP_1_MOD" value="609"/>
  <const name="VEH_SCOOP_2_MOD" value="610"/>
  <const name="VEH_SCOOP_3_MOD" value="611"/>
  <const name="VEH_RAMP_1_MOD" value="612"/>
  <const name="VEH_RAMP_2_MOD" value="613"/>
  <const name="VEH_RAMP_3_MOD" value="614"/>
  <const name="VEH_FRONT_SPIKE_1" value="615"/>
  <const name="VEH_FRONT_SPIKE_2" value="616"/>
  <const name="VEH_FRONT_SPIKE_3" value="617"/>
  <const name="VEH_RAMMING_BAR_1" value="618"/>
  <const name="VEH_RAMMING_BAR_2" value="619"/>
  <const name="VEH_RAMMING_BAR_3" value="620"/>
  <const name="VEH_RAMMING_BAR_4" value="621"/>
  <const name="VEH_BOBBLE_MISC_1" value="622"/>
  <const name="VEH_BOBBLE_MISC_2" value="623"/>
  <const name="VEH_BOBBLE_MISC_3" value="624"/>
  <const name="VEH_BOBBLE_MISC_4" value="625"/>
  <const name="VEH_BOBBLE_MISC_5" value="626"/>
  <const name="VEH_BOBBLE_MISC_6" value="627"/>
  <const name="VEH_BOBBLE_MISC_7" value="628"/>
  <const name="VEH_BOBBLE_MISC_8" value="629"/>
  <const name="VEH_SUPERCHARGER_1" value="630"/>
  <const name="VEH_SUPERCHARGER_2" value="631"/>
  <const name="VEH_SUPERCHARGER_3" value="632"/>

  
  <enumdef type="CVehicleModVisible::eVehicleModBone">
    <enumval name="none" value="-1"/>
    <enumval name="chassis" value="VEH_CHASSIS"/>
    <enumval name="bodyshell" value="VEH_BODYSHELL"/>
    <enumval name="bumper_f" value="VEH_BUMPER_F"/>
    <enumval name="bumper_r" value="VEH_BUMPER_R"/>
    <enumval name="wing_rf" value="VEH_WING_RF"/>
    <enumval name="wing_lf" value="VEH_WING_LF"/>
    <enumval name="bonnet" value="VEH_BONNET"/>
    <enumval name="boot" value="VEH_BOOT"/>
    <enumval name="exhaust" value="VEH_EXHAUST"/>
    <enumval name="exhaust_2" value="VEH_EXHAUST_2"/>
    <enumval name="exhaust_3" value="VEH_EXHAUST_3"/>
    <enumval name="exhaust_4" value="VEH_EXHAUST_4"/>
    <enumval name="exhaust_5" value="VEH_EXHAUST_5"/>
    <enumval name="exhaust_6" value="VEH_EXHAUST_6"/>
    <enumval name="exhaust_7" value="VEH_EXHAUST_7"/>
    <enumval name="exhaust_8" value="VEH_EXHAUST_8"/>
    <enumval name="exhaust_9" value="VEH_EXHAUST_9"/>
    <enumval name="exhaust_10" value="VEH_EXHAUST_10"/>
    <enumval name="exhaust_11" value="VEH_EXHAUST_11"/>
    <enumval name="exhaust_12" value="VEH_EXHAUST_12"/>
    <enumval name="exhaust_13" value="VEH_EXHAUST_13"/>
    <enumval name="exhaust_14" value="VEH_EXHAUST_14"/>
    <enumval name="exhaust_15" value="VEH_EXHAUST_15"/>
    <enumval name="exhaust_16" value="VEH_EXHAUST_16"/>
    <enumval name="exhaust_17" value="VEH_EXHAUST_17"/>
    <enumval name="exhaust_18" value="VEH_EXHAUST_18"/>
    <enumval name="exhaust_19" value="VEH_EXHAUST_19"/>
    <enumval name="exhaust_20" value="VEH_EXHAUST_20"/>
    <enumval name="exhaust_21" value="VEH_EXHAUST_21"/>
    <enumval name="exhaust_22" value="VEH_EXHAUST_22"/>
    <enumval name="exhaust_23" value="VEH_EXHAUST_23"/>
    <enumval name="exhaust_24" value="VEH_EXHAUST_24"/>
    <enumval name="exhaust_25" value="VEH_EXHAUST_25"/>
    <enumval name="exhaust_26" value="VEH_EXHAUST_26"/>
    <enumval name="exhaust_27" value="VEH_EXHAUST_27"/>
    <enumval name="exhaust_28" value="VEH_EXHAUST_28"/>
    <enumval name="exhaust_29" value="VEH_EXHAUST_29"/>
    <enumval name="exhaust_30" value="VEH_EXHAUST_30"/>
    <enumval name="exhaust_31" value="VEH_EXHAUST_31"/>
    <enumval name="exhaust_32" value="VEH_EXHAUST_32"/>
    <enumval name="extra_1" value="VEH_EXTRA_1"/>
    <enumval name="extra_2" value="VEH_EXTRA_2"/>
    <enumval name="extra_3" value="VEH_EXTRA_3"/>
    <enumval name="extra_4" value="VEH_EXTRA_4"/>
    <enumval name="extra_5" value="VEH_EXTRA_5"/>
    <enumval name="extra_6" value="VEH_EXTRA_6"/>
    <enumval name="extra_7" value="VEH_EXTRA_7"/>
    <enumval name="extra_8" value="VEH_EXTRA_8"/>
    <enumval name="extra_9" value="VEH_EXTRA_9"/>
    <enumval name="extra_10" value="VEH_EXTRA_10"/>
    <enumval name="extra_11" value="VEH_EXTRA_11"/>
    <enumval name="extra_12" value="VEH_EXTRA_12"/>
    <enumval name="extra_13" value="VEH_EXTRA_13"/>
    <enumval name="extra_14" value="VEH_EXTRA_14"/>
    <enumval name="break_extra_1" value="VEH_BREAKABLE_EXTRA_1"/>
    <enumval name="break_extra_2" value="VEH_BREAKABLE_EXTRA_2"/>
    <enumval name="break_extra_3" value="VEH_BREAKABLE_EXTRA_3"/>
    <enumval name="break_extra_4" value="VEH_BREAKABLE_EXTRA_4"/>
    <enumval name="break_extra_5" value="VEH_BREAKABLE_EXTRA_5"/>
    <enumval name="break_extra_6" value="VEH_BREAKABLE_EXTRA_6"/>
    <enumval name="break_extra_7" value="VEH_BREAKABLE_EXTRA_7"/>
    <enumval name="break_extra_8" value="VEH_BREAKABLE_EXTRA_8"/>
    <enumval name="break_extra_9" value="VEH_BREAKABLE_EXTRA_9"/>
    <enumval name="break_extra_10" value="VEH_BREAKABLE_EXTRA_10"/>
    <enumval name="mod_col_1" value="VEH_MOD_COLLISION_1"/>
    <enumval name="mod_col_2" value="VEH_MOD_COLLISION_2"/>
    <enumval name="mod_col_3" value="VEH_MOD_COLLISION_3"/>
    <enumval name="mod_col_4" value="VEH_MOD_COLLISION_4"/>
    <enumval name="mod_col_5" value="VEH_MOD_COLLISION_5"/>
    <enumval name="mod_col_6" value="VEH_MOD_COLLISION_6"/>
    <enumval name="mod_col_7" value="VEH_MOD_COLLISION_7"/>
    <enumval name="mod_col_8" value="VEH_MOD_COLLISION_8"/>
    <enumval name="mod_col_9" value="VEH_MOD_COLLISION_9"/>
    <enumval name="mod_col_10" value="VEH_MOD_COLLISION_10"/>
    <enumval name="mod_col_11" value="VEH_MOD_COLLISION_11"/>
    <enumval name="mod_col_12" value="VEH_MOD_COLLISION_12"/>
    <enumval name="mod_col_13" value="VEH_MOD_COLLISION_13"/>
    <enumval name="mod_col_14" value="VEH_MOD_COLLISION_14"/>
    <enumval name="mod_col_15" value="VEH_MOD_COLLISION_15"/>
    <enumval name="mod_col_16" value="VEH_MOD_COLLISION_16"/>
    <enumval name="misc_a" value="VEH_MISC_A"/>
    <enumval name="misc_b" value="VEH_MISC_B"/>
    <enumval name="misc_c" value="VEH_MISC_C"/>
    <enumval name="misc_d" value="VEH_MISC_D"/>
    <enumval name="misc_e" value="VEH_MISC_E"/>
    <enumval name="misc_f" value="VEH_MISC_F"/>
    <enumval name="misc_g" value="VEH_MISC_G"/>
    <enumval name="misc_h" value="VEH_MISC_H"/>
    <enumval name="misc_i" value="VEH_MISC_I"/>
    <enumval name="misc_j" value="VEH_MISC_J"/>
    <enumval name="misc_k" value="VEH_MISC_K"/>
    <enumval name="misc_l" value="VEH_MISC_L"/>
    <enumval name="misc_m" value="VEH_MISC_M"/>
    <enumval name="misc_n" value="VEH_MISC_N"/>
    <enumval name="misc_o" value="VEH_MISC_O"/>
    <enumval name="misc_p" value="VEH_MISC_P"/>
    <enumval name="misc_q" value="VEH_MISC_Q"/>
    <enumval name="misc_r" value="VEH_MISC_R"/>
    <enumval name="misc_s" value="VEH_MISC_S"/>
    <enumval name="misc_t" value="VEH_MISC_T"/>
    <enumval name="misc_u" value="VEH_MISC_U"/>
    <enumval name="misc_v" value="VEH_MISC_V"/>
    <enumval name="misc_w" value="VEH_MISC_W"/>
    <enumval name="misc_x" value="VEH_MISC_X"/>
    <enumval name="misc_y" value="VEH_MISC_Y"/>
    <enumval name="misc_z" value="VEH_MISC_Z"/>
    <enumval name="misc_1" value="VEH_MISC_1"/>
    <enumval name="misc_2" value="VEH_MISC_2"/>
    <enumval name="handlebars" value="VEH_STEERING_WHEEL"/>
    <enumval name="steeringwheel" value="VEH_CAR_STEERING_WHEEL"/>
    <enumval name="swingarm" value="VEH_TRANSMISSION_R"/>
    <enumval name="forks_u" value="VEH_SUSPENSION_LF"/>
    <enumval name="forks_l" value="VEH_SUSPENSION_RF"/>
    <enumval name="headlight_l"	value="VEH_HEADLIGHT_L"/>
    <enumval name="headlight_r"	value="VEH_HEADLIGHT_R"/>
    <enumval name="indicator_lr" value="VEH_INDICATOR_LR"/>
    <enumval name="indicator_lf" value="VEH_INDICATOR_LF"/>
    <enumval name="indicator_rr" value="VEH_INDICATOR_RR"/>
    <enumval name="indicator_rf" value="VEH_INDICATOR_RF"/>
    <enumval name="taillight_l" value="VEH_TAILLIGHT_L"/>
    <enumval name="taillight_r" value="VEH_TAILLIGHT_R"/>
    <enumval name="window_lf" value="VEH_WINDOW_LF"/>
    <enumval name="window_rf" value="VEH_WINDOW_RF"/>
    <enumval name="window_rr" value="VEH_WINDOW_RR"/>
    <enumval name="window_lr" value="VEH_WINDOW_LR"/>
    <enumval name="window_lm" value="VEH_WINDOW_LM"/>
    <enumval name="window_rm" value="VEH_WINDOW_RM"/>
    <enumval name="hub_lf" value="VEH_WHEELHUB_LF"/>
    <enumval name="hub_rf" value="VEH_WHEELHUB_RF"/>
    <enumval name="windscreen_r" value="VEH_WINDSCREEN_R"/>
    <enumval name="neon_l" value="VEH_NEON_L"/>
    <enumval name="neon_r" value="VEH_NEON_R"/>
    <enumval name="neon_f" value="VEH_NEON_F"/>
    <enumval name="neon_b" value="VEH_NEON_B"/>
    <enumval name="door_dside_f" value="VEH_DOOR_DSIDE_F"/>
    <enumval name="door_dside_r" value="VEH_DOOR_DSIDE_R"/>
    <enumval name="door_pside_f" value="VEH_DOOR_PSIDE_F"/>
    <enumval name="door_pside_r" value="VEH_DOOR_PSIDE_R"/>
    <enumval name="bobble_head" value="VEH_BOBBLE_HEAD"/>
    <enumval name="bobble_base" value="VEH_BOBBLE_BASE"/>
    <enumval name="bobble_hand" value="VEH_BOBBLE_HAND"/>
    <enumval name="engineblock" value="VEH_BOBBLE_ENGINE"/>
    <enumval name="spoiler" value="VEH_SPOILER"/>
    <enumval name="mod_a" value="VEH_MOD_A"/>
    <enumval name="mod_b" value="VEH_MOD_B"/>
    <enumval name="mod_c" value="VEH_MOD_C"/>
    <enumval name="mod_d" value="VEH_MOD_D"/>
    <enumval name="mod_e" value="VEH_MOD_E"/>
    <enumval name="mod_f" value="VEH_MOD_F"/>
    <enumval name="mod_g" value="VEH_MOD_G"/>
    <enumval name="mod_h" value="VEH_MOD_H"/>
    <enumval name="mod_i" value="VEH_MOD_I"/>
    <enumval name="mod_j" value="VEH_MOD_J"/>
    <enumval name="mod_k" value="VEH_MOD_K"/>
    <enumval name="mod_l" value="VEH_MOD_L"/>
    <enumval name="mod_m" value="VEH_MOD_M"/>
    <enumval name="mod_n" value="VEH_MOD_N"/>
    <enumval name="mod_o" value="VEH_MOD_O"/>
    <enumval name="mod_p" value="VEH_MOD_P"/>
    <enumval name="mod_q" value="VEH_MOD_Q"/>
    <enumval name="mod_r" value="VEH_MOD_R"/>
    <enumval name="mod_s" value="VEH_MOD_S"/>
    <enumval name="mod_t" value="VEH_MOD_T"/>
    <enumval name="mod_u" value="VEH_MOD_U"/>
    <enumval name="mod_v" value="VEH_MOD_V"/>
    <enumval name="mod_w" value="VEH_MOD_W"/>
    <enumval name="mod_x" value="VEH_MOD_X"/>
    <enumval name="mod_y" value="VEH_MOD_Y"/>
    <enumval name="mod_z" value="VEH_MOD_Z"/>
    <enumval name="mod_aa" value="VEH_MOD_AA"/>
    <enumval name="mod_ab" value="VEH_MOD_AB"/>
    <enumval name="mod_ac" value="VEH_MOD_AC"/>
    <enumval name="mod_ad" value="VEH_MOD_AD"/>
    <enumval name="mod_ae" value="VEH_MOD_AE"/>
    <enumval name="mod_af" value="VEH_MOD_AF"/>
    <enumval name="mod_ag" value="VEH_MOD_AG"/>
    <enumval name="mod_ah" value="VEH_MOD_AH"/>
    <enumval name="mod_ai" value="VEH_MOD_AI"/>
    <enumval name="mod_aj" value="VEH_MOD_AJ"/>
    <enumval name="mod_ak" value="VEH_MOD_AK"/>
    <enumval name="turret_a1" value="VEH_TURRET_A1_MOD"/>
    <enumval name="turret_a2" value="VEH_TURRET_A2_MOD"/>
    <enumval name="turret_a3" value="VEH_TURRET_A3_MOD"/>
    <enumval name="turret_a4" value="VEH_TURRET_A4_MOD"/>
    <enumval name="turret_b1" value="VEH_TURRET_B1_MOD"/>
    <enumval name="turret_b2" value="VEH_TURRET_B2_MOD"/>
    <enumval name="turret_b3" value="VEH_TURRET_B3_MOD"/>
    <enumval name="turret_b4" value="VEH_TURRET_B4_MOD"/>
    <enumval name="rblade_1mod" value="VEH_BLADE_R_1_MOD"/>
    <enumval name="rblade_1fast" value="VEH_BLADE_R_1_FAST"/>
    <enumval name="rblade_2mod" value="VEH_BLADE_R_2_MOD"/>
    <enumval name="rblade_2fast" value="VEH_BLADE_R_2_FAST"/>
    <enumval name="rblade_3mod" value="VEH_BLADE_R_3_MOD"/>
    <enumval name="rblade_3fast" value="VEH_BLADE_R_3_FAST"/>
    <enumval name="fblade_1mod" value="VEH_BLADE_F_1_MOD"/>
    <enumval name="fblade_1fast" value="VEH_BLADE_F_1_FAST"/>
    <enumval name="fblade_2mod" value="VEH_BLADE_F_2_MOD"/>
    <enumval name="fblade_2fast" value="VEH_BLADE_F_2_FAST"/>
    <enumval name="fblade_3mod" value="VEH_BLADE_F_3_MOD"/>
    <enumval name="fblade_3fast" value="VEH_BLADE_F_3_FAST"/>
    <enumval name="sblade_1mod" value="VEH_BLADE_S_1_MOD"/>
    <enumval name="sblade_1_lfast" value="VEH_BLADE_S_1_L_FAST"/>
    <enumval name="sblade_1_rfast" value="VEH_BLADE_S_1_R_FAST"/>
    <enumval name="sblade_2mod" value="VEH_BLADE_S_2_MOD"/>
    <enumval name="sblade_2_1fast" value="VEH_BLADE_S_2_L_FAST"/>
    <enumval name="sblade_2_1fast" value="VEH_BLADE_S_2_R_FAST"/>
    <enumval name="sblade_3mod" value="VEH_BLADE_S_3_MOD"/>
    <enumval name="sblade_3_lfast" value="VEH_BLADE_S_3_L_FAST"/>
    <enumval name="sblade_3_rfast" value="VEH_BLADE_S_3_R_FAST"/>
    <enumval name="spike_1mod" value="VEH_SPIKE_1_MOD"/>
    <enumval name="spike_1ped_col" value="VEH_SPIKE_1_PED"/>
    <enumval name="spike1car_col" value="VEH_SPIKE_1_CAR"/>
    <enumval name="spike_2mod" value="VEH_SPIKE_2_MOD"/>
    <enumval name="spike_2ped_col" value="VEH_SPIKE_2_PED"/>
    <enumval name="spike_2car_col" value="VEH_SPIKE_2_CAR"/>
    <enumval name="spike_3mod" value="VEH_SPIKE_3_MOD"/>
    <enumval name="spike_3ped_col" value="VEH_SPIKE_3_PED"/>
    <enumval name="spike_3car_col" value="VEH_SPIKE_3_CAR"/>
    <enumval name="scoop_1mod" value="VEH_SCOOP_1_MOD"/>
    <enumval name="scoop_2mod" value="VEH_SCOOP_2_MOD"/>
    <enumval name="scoop_3mod" value="VEH_SCOOP_3_MOD"/>
    <enumval name="ramp_1mod" value="VEH_RAMP_1_MOD"/>
    <enumval name="ramp_2mod" value="VEH_RAMP_2_MOD"/>
    <enumval name="ramp_3mod" value="VEH_RAMP_3_MOD"/>
    <enumval name="spike_1modf" value="VEH_FRONT_SPIKE_1"/>
    <enumval name="spike_2modf" value="VEH_FRONT_SPIKE_2"/>
    <enumval name="spike_3modf" value="VEH_FRONT_SPIKE_3"/>
    <enumval name="ram_1modf" value="VEH_RAMMING_BAR_1"/>
    <enumval name="ram_2modf" value="VEH_RAMMING_BAR_2"/>
    <enumval name="ram_3modf" value="VEH_RAMMING_BAR_3"/>
    <enumval name="ram_4modf" value="VEH_RAMMING_BAR_4"/>
    <enumval name="miscwobble_1" value="VEH_BOBBLE_MISC_1"/>
    <enumval name="miscwobble_2" value="VEH_BOBBLE_MISC_2"/>
    <enumval name="miscwobble_3" value="VEH_BOBBLE_MISC_3"/>
    <enumval name="miscwobble_4" value="VEH_BOBBLE_MISC_4"/>
    <enumval name="miscwobble_5" value="VEH_BOBBLE_MISC_5"/>
    <enumval name="miscwobble_6" value="VEH_BOBBLE_MISC_6"/>
    <enumval name="miscwobble_7" value="VEH_BOBBLE_MISC_7"/>
    <enumval name="miscwobble_8" value="VEH_BOBBLE_MISC_8"/>
    <enumval name="supercharger_1" value="VEH_SUPERCHARGER_1"/>
    <enumval name="supercharger_2" value="VEH_SUPERCHARGER_2"/>
    <enumval name="supercharger_3" value="VEH_SUPERCHARGER_3"/>
    <enumval name="reversinglight_l" value="VEH_REVERSINGLIGHT_L"/>
    <enumval name="reversinglight_r" value="VEH_REVERSINGLIGHT_R"/>
  </enumdef>

  <enumdef type="eVehicleModCameraPos">
    <enumval name="VMCP_DEFAULT"/>
    <enumval name="VMCP_FRONT"/>
    <enumval name="VMCP_FRONT_LEFT"/>
    <enumval name="VMCP_FRONT_RIGHT"/>
    <enumval name="VMCP_REAR"/>
    <enumval name="VMCP_REAR_LEFT"/>
    <enumval name="VMCP_REAR_RIGHT"/>
    <enumval name="VMCP_LEFT"/>
    <enumval name="VMCP_RIGHT"/>
    <enumval name="VMCP_TOP"/>
    <enumval name="VMCP_BOTTOM"/>
  </enumdef>

  <structdef type="CVehicleModLink">
    <string name="m_modelName" type="atHashString" description="Name of this mod" ui_key="true"/>
    <enum name="m_bone" type="CVehicleModVisible::eVehicleModBone" description="Bone to attach this mod to"/>
    <bool name="m_turnOffExtra" description="If true this mod will turn off any extras that are visible on the same bone"/>
  </structdef>

  <structdef type="CVehicleModVisible" onPostLoad="PostLoad">
    <string name="m_modelName" type="atHashString" description="Name of this mod" ui_key="true"/>
    <string name="m_modShopLabel" type="ConstString" description="Label to display in the mod shop UI"/>
    <array name="m_linkedModels" type="atArray">
      <string type="atHashString"/>
    </array>
    <array name="m_turnOffBones" type="atArray">
      <enum type="CVehicleModVisible::eVehicleModBone"/>
    </array>
    <enum name="m_type" type="eVehicleModType" description="Type of mod"/>
    <enum name="m_bone" type="CVehicleModVisible::eVehicleModBone" description="Bone to attach this mod to"/>
    <enum name="m_collisionBone" type="CVehicleModVisible::eVehicleModBone" description="Bone to enable in order to get extra collision"/>
    <enum name="m_cameraPos" type="eVehicleModCameraPos" description="Indentifier to tell script where to place the camera in the mod shop"/>
    <float name="m_audioApply" description="Audio specific data" init="1.0"/>
    <u8 name="m_weight" min="0" max="100" description="Percentage modifier for weight applied by this mod"/>
    <bool name="m_turnOffExtra" description="If true this mod will turn off any extras that are visible on the same bone"/>
    <bool name="m_disableBonnetCamera" description="If true, this mod will disable the vehicle bonnet camera. Only works on specific vehicle models, see camCinematicDirector::Update" init="false"/>
    <bool name="m_allowBonnetSlide" description="If false, the player cannot slide across thet bonnet" init="true"/>
    <s8 name="m_weaponSlot" min="-1" max="5" description="The vehicle weapon slot associated to this mod (if any)" init="-1"/>
    <s8 name="m_weaponSlotSecondary" min="-1" max="5" description="The second vehicle weapon slot associated to this mod (if any)" init="-1"/>
    <bool name="m_disableProjectileDriveby" description="If true, projectile and unarmed weapons will be disabled for driveby when this mod is attached" init="false"/>
    <bool name="m_disableDriveby" description="If true, all weapons will be disabled for driveby when this mod is attached" init="false"/>
		<s32 name="m_disableDrivebySeat" min="-1" max="15" description="If set, the above driveby disable flags will only apply to a specific seat. By default (-1), they apply to all seats on the vehicle." init="-1"/>
    <s32 name="m_disableDrivebySeatSecondary" min="-1" max="15" description="If set, the above driveby disable flags will only apply to a specific seat. By default (-1), they apply to all seats on the vehicle." init="-1"/>
  </structdef>

  <structdef type="CVehicleModStat">
    <string name="m_identifier" type="atHashString" description="Unique strign identifier for this mod" ui_key="true"/>
    <u32 name="m_modifier" description="Modifier value for this mod"/>
    <float name="m_audioApply" description="Audio specific data" init="1.0"/>
    <u8 name="m_weight" min="0" max="100" description="Percentage modifier for weight applied by this mod"/>
    <enum name="m_type" type="eVehicleModType" description="Type of mod"/>
  </structdef>

  <structdef type="CVehicleWheel">
    <string name="m_wheelName" type="atHashString" description="Name of wheel" ui_key="true"/>
    <string name="m_wheelVariation" type="atHashString" description="Variation of wheel"/>
    <string name="m_modShopLabel" type="ConstString" description="Label to display in the mod shop UI"/>
    <float name="m_rimRadius" description="Rim radius used for burst tyres"/>
    <bool name="m_rear" description="Flags this wheel as a rear wheel. If false wheel is a front wheel. Applies only to bikes."/>
  </structdef>

  <enumdef type="eModKitType">
    <enumval name="MKT_STANDARD" value="0"/>
    <enumval name="MKT_SPORT"/>
    <enumval name="MKT_SUV"/>
    <enumval name="MKT_SPECIAL"/>
  </enumdef>

  <structdef type="CVehicleKit::sSlotNameOverride">
    <enum name="slot" type="eVehicleModType" init="VMT_CHASSIS" description="The mod slot that requires a name override" ui_key="true"/>
    <string name="name" type="ConstString" description="The new slot name to display in the mod shop UI"/>
  </structdef>

  <structdef type="CVehicleKit">
    <string name="m_kitName" type="atHashString" description="Name of mod kit. Should match dwd and txd name" ui_key="true"/>
    <u16 name="m_id" init = "0xFFFF" description="Mod kit id"/>
    <enum name="m_kitType" type="eModKitType" init="MKT_STANDARD" description="Type of mod kit"/>
    <array name="m_visibleMods" type="atArray" description="List of renderable mods available in this kit">
      <struct type="CVehicleModVisible"/>
    </array>
    <array name="m_linkMods" type="atArray" description="List of renderable mods not available by themselves only though linking from other mods">
      <struct type="CVehicleModLink"/>
    </array>
    <array name="m_statMods" type="atArray" description="List of stat mods available in this kit">
      <struct type="CVehicleModStat"/>
    </array>
    <array name="m_slotNames" type="atArray" description="List of slot name overrides">
      <struct type="CVehicleKit::sSlotNameOverride"/>
    </array>
    <array name="m_liveryNames" type="atArray" description="List of livery names for the vehicle owning this kit">
      <string type="ConstString"/>
    </array>
    <array name="m_livery2Names" type="atArray" description="List of livery2 names for the vehicle owning this kit">
      <string type="ConstString"/>
    </array>
  </structdef>

  <enumdef type="EVehicleModelAudioColor">
    <enumval name="POLICE_SCANNER_COLOUR_black"/>
    <enumval name="POLICE_SCANNER_COLOUR_blue"/>
    <enumval name="POLICE_SCANNER_COLOUR_brown"/>
    <enumval name="POLICE_SCANNER_COLOUR_beige"/>
    <enumval name="POLICE_SCANNER_COLOUR_graphite"/>
    <enumval name="POLICE_SCANNER_COLOUR_green"/>
    <enumval name="POLICE_SCANNER_COLOUR_grey"/>
    <enumval name="POLICE_SCANNER_COLOUR_orange"/>
    <enumval name="POLICE_SCANNER_COLOUR_pink"/>
    <enumval name="POLICE_SCANNER_COLOUR_red"/>
    <enumval name="POLICE_SCANNER_COLOUR_silver"/>
    <enumval name="POLICE_SCANNER_COLOUR_white"/>
    <enumval name="POLICE_SCANNER_COLOUR_yellow"/>
  </enumdef>

  <enumdef type="EVehicleModelAudioPrefix">
    <enumval name="none"/>
    <enumval name="POLICE_SCANNER_PREFIX_bright"/>
    <enumval name="POLICE_SCANNER_PREFIX_light"/>
    <enumval name="POLICE_SCANNER_PREFIX_dark"/>
  </enumdef>

  <enumdef type="EVehicleModelColorMetallicID">
    <enumval name="none" value="-1"/>
    <enumval name="EVehicleModelColorMetallic_normal" value="0"/>
    <enumval name="EVehicleModelColorMetallic_1"/>
    <enumval name="EVehicleModelColorMetallic_2"/>
    <enumval name="EVehicleModelColorMetallic_3"/>
    <enumval name="EVehicleModelColorMetallic_4"/>
    <enumval name="EVehicleModelColorMetallic_5"/>
    <enumval name="EVehicleModelColorMetallic_6"/>
    <enumval name="EVehicleModelColorMetallic_7"/>
    <enumval name="EVehicleModelColorMetallic_8"/>
    <enumval name="EVehicleModelColorMetallic_9"/>
  </enumdef>

  <structdef type="CVehicleModelColor">
    <Color32 name="m_color"/>
    <enum name="m_metallicID" size="8" type="EVehicleModelColorMetallicID" init="-1"/>
    <enum name="m_audioColor" size="8" type="EVehicleModelAudioColor"/>
    <enum name="m_audioPrefix" size="8" type="EVehicleModelAudioPrefix"/>
    <u32 name="m_audioColorHash"/>
    <u32 name="m_audioPrefixHash"/>
    <string name="m_colorName" type="pointer" ui_key="true"/>
  </structdef>

  <structdef type="CVehicleModelColorGen9">
    <Color32 name="m_color"/>
    <enum name="m_metallicID" size="8" type="EVehicleModelColorMetallicID" init="-1"/>
    <enum name="m_audioColor" size="8" type="EVehicleModelAudioColor"/>
    <enum name="m_audioPrefix" size="8" type="EVehicleModelAudioPrefix"/>
    <u32 name="m_audioColorHash"/>
    <u32 name="m_audioPrefixHash"/>
    <string name="m_colorName" type="pointer" ui_key="true"/>
    <string name="m_rampTextureName" type="pointer" ui_key="true"/>
  </structdef>

  <structdef type="CVehicleModelColorsGen9">
    <array name="m_Colors" type="atArray">
      <struct type="CVehicleModelColorGen9"/>
    </array>
  </structdef>
  
  <structdef type="CVehicleMetallicSetting">
    <float name="m_specInt" description="Spec Intensity"/>
    <float name="m_specFalloff" description="Spec Falloff"/>
    <float name="m_specFresnel" description="Spec Fresnel"/>
  </structdef>

  <structdef type="CVehicleWindowColor">
    <Color32 name="m_color"/>
    <string name="m_name" type="atHashString" ui_key="true"/>
  </structdef>

  <structdef type="CVehicleVariationGlobalData">
    <Color32 name="m_xenonLightColor"/>
    <Color32 name="m_xenonCoronaColor"/>
    <float name="m_xenonLightIntensityModifier"/>
    <float name="m_xenonCoronaIntensityModifier"/>
  </structdef>

  <structdef type="CVehicleXenonLightColor">
    <Color32 name="m_lightColor"/>
    <Color32 name="m_coronaColor"/>
    <float   name="m_lightIntensityModifier"/>
    <float   name="m_coronaIntensityModifier"/>
  </structdef>

  <const name="VWT_MAX" value="13"/>

  <structdef type="CVehicleModelInfoVarGlobal" onPreSave="OnPreSave">
    <struct name="m_VehiclePlates" type="CVehicleModelInfoPlates"/>
    <array name="m_Colors" type="atArray">
      <struct type="CVehicleModelColor"/>
    </array>
    <array name="m_MetallicSettings" type="atArray">
      <struct type="CVehicleMetallicSetting"/>
    </array>
    <array name="m_WindowColors" type="atArray">
      <struct type="CVehicleWindowColor"/>
    </array>
    <array name="m_Lights" type="atArray">
      <struct type="vehicleLightSettings"/>
    </array>
    <array name="m_Sirens" type="atArray">
      <struct type="sirenSettings"/>
    </array>
    <array name="m_Kits" type="atArray">
      <struct type="CVehicleKit"/>
    </array>
    <array name="m_Wheels" type="member" size="VWT_MAX">
      <array type="atArray">
        <struct type="CVehicleWheel"/>
      </array>
    </array>
    <struct name="m_GlobalVariationData" type="CVehicleVariationGlobalData"/>
    <array name="m_XenonLightColors" type="atArray">
      <struct type="CVehicleXenonLightColor"/>
    </array>

  </structdef>

  <structdef type="CVehicleModColor">
    <string name="name" type="ConstString" description="Label to display in the mod shop UI" ui_key="true"/>
    <u8 name="col" description="Primary color"/>
    <u8 name="spec" description="Specondary specular color"/>
  </structdef>

  <structdef type="CVehicleModPearlescentColors">
    <array name="m_baseCols" type="atArray">
      <struct type="CVehicleModColor"/>
    </array>
    <array name="m_specCols" type="atArray">
      <struct type="CVehicleModColor"/>
    </array>
  </structdef>

  <structdef type="CVehicleModColors">
    <array name="m_metallic" type="atArray">
      <struct type="CVehicleModColor"/>
    </array>
    <array name="m_classic" type="atArray">
      <struct type="CVehicleModColor"/>
    </array>
    <array name="m_matte" type="atArray">
      <struct type="CVehicleModColor"/>
    </array>
    <array name="m_metals" type="atArray">
      <struct type="CVehicleModColor"/>
    </array>
    <array name="m_chrome" type="atArray">
      <struct type="CVehicleModColor"/>
    </array>

    <struct name="m_pearlescent" type="CVehicleModPearlescentColors"/>
  </structdef>

  <structdef type="CVehicleModColorsGen9">
    <array name="m_chameleon" type="atArray">
      <struct type="CVehicleModColor"/>
    </array>
  </structdef>

</ParserSchema>
