#include "uiCareerBuilderMessages.h"

#if UI_PAGE_DECK_ENABLED

FWUI_DEFINE_MESSAGE(uiSelectCareerMessage, 0x9A584852);
FWUI_DEFINE_MESSAGE(uiSelectCareerItemMessage, 0x33501FF3);
FWUI_DEFINE_MESSAGE(uiSelectMpCharacterSlotMessage, 0x4BA67A32);

#endif // UI_PAGE_DECK_ENABLED
