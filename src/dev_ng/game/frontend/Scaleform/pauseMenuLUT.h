// 
// scaleform/pauseMenuLUT.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PAUSEMENULUT_H
#define PAUSEMENULUT_H

#include "scaleform/scaleform.h"

void CScaleformInstallPauseMenuLUT(sfScaleformMovieView& movie);

#endif // PAUSEMENULUT_H
