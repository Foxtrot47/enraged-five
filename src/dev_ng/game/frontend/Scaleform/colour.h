// 
// scaleform/colour.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SCALEFORM_COLOUR_H
#define SCALEFORM_COLOUR_H

#include "scaleform/scaleform.h"

void CScaleformInstallColour(sfScaleformMovieView& movie);

#endif