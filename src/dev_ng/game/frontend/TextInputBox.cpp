﻿#if RSG_PC
#include "TextInputBox.h"
#include "Frontend/ui_channel.h"
#include "Frontend/PauseMenu.h"
#include "system/keyboard.h"
#include "Frontend/NewHud.h"

#if __BANK
#include "bank/bkmgr.h"
#include "game/localisation.h"
#include "input/virtualkeyboard.h"
#endif // __BANK

//OPTIMISATIONS_OFF();

#define TEXT_INPUT_BOX_MOVIE_NAME "TEXT_INPUT_BOX"
#define MOVIE_CONTENT_MC "CONTENT"
#define MOVIE_INPUT_TEXTFIELD "InputTF"
#define KEY_HELD_THRESHOLD 100
#if __BANK

#define UTF_16_SPEW 1

#define DEBUG_STRING_SIZE ioVirtualKeyboard::MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD
char s_pszDebugTitle[DEBUG_STRING_SIZE];
char s_pszDebugDescription[DEBUG_STRING_SIZE];
char s_pszDebugDefaultInput[DEBUG_STRING_SIZE];

char16 s_pszDebugTitle16[DEBUG_STRING_SIZE];
char16 s_pszDebugDescription16[DEBUG_STRING_SIZE];
char16 s_pszDebugDefaultInput16[DEBUG_STRING_SIZE];

int s_iInputBoxState;
#endif //__BANK

atArray<CMenuButtonWithSound> CTextInputBox::ms_ButtonData;
atRangeArray<u64,4>	 CTextInputBox::ms_AffectedButtonMasks;
CScaleformMovieWrapper CTextInputBox::ms_Movie;

CTextInputBox::CTextInputBox() :
  m_bEnabled(true)
, m_bActive(false)
, m_bImeTextInputHasFocus(false)
, m_bWaitForEnterRelease(false)
, m_bWaitForBackRelease(false)
, m_bCursorThisFrame(false)
, m_bCursorPreviousFrame(false)
, m_bAllowComma(false)
, m_uLastInputTime(0)
, m_uCursorLocation(0)
, m_uMaxLength(0)
, m_uKeyHeldDelay(0)
{
	m_eState = ioVirtualKeyboard::kKBState_INVALID;
	memset(m_Text, '\0', sizeof(m_Text));

	m_AcceptCallback = NullCallback;
	m_DeclineCallback = NullCallback;
}


void CTextInputBox::Init(unsigned initMode)
{
	if( initMode == INIT_CORE )
	{
		if (!STextInputBox::IsInstantiated())
		{
			STextInputBox::Instantiate();
		}

		REGISTER_FRONTEND_XML(CTextInputBox::HandleXML, "TextEntryBox");

		// preload some entries so we can live happily due to startup procedures needing this nearly instantly 
		if( ms_ButtonData.empty() ) 
		{ 
			ms_ButtonData.ResizeGrow(2); 

			int iBitIndex = 1; 
			CMenuButtonWithSound& OKButton = ms_ButtonData[iBitIndex];   
			OKButton.m_ButtonInput = INPUT_FRONTEND_ACCEPT;
			OKButton.m_ButtonInputGroup = INPUTGROUP_INVALID;
			OKButton.m_RawButtonIcon = ICON_INVALID;
			OKButton.m_hButtonHash.SetFromString("IB_OK"); 

			int iButton = GetButtonMaskIndex(OKButton.m_ButtonInput); 
			if( iButton != -1 ) 
			{ 
				ms_AffectedButtonMasks[iButton] |= BIT_64(iBitIndex); 
			} 
		}
	}
}

void CTextInputBox::Shutdown(unsigned shutdownMode)
{
	if ( shutdownMode == rage::SHUTDOWN_CORE )
	{
		if (STextInputBox::IsInstantiated())
		{
			STextInputBox::Destroy();
		}
	}
}

void CTextInputBox::Open(const ioVirtualKeyboard::Params& WIN32PC_ONLY(params))
{ 
	if(!m_bEnabled)
	{
		return;
	}

#if !__FINAL
	if(CControlMgr::GetKeyboard().GetKeyboardMode() != KEYBOARD_MODE_GAME)
	{
		CControlMgr::GetKeyboard().SetCurrentMode(KEYBOARD_MODE_GAME);
	}
#endif // !__FINAL

	if(!m_bActive)
	{
		LoadTextInputBoxMovie();
	}

	//UpdateDisplayParam();
	m_bCursorThisFrame = false;
	m_bCursorPreviousFrame = false;
	m_uLastInputTime = 0;
	m_uCursorLocation = 0;
	m_uKeyHeldDelay = 0;
	m_uMaxLength = params.m_MaxLength;
	memset(m_Text, '\0', sizeof(m_Text));
	
	m_eTextType = params.m_KeyboardType;
	SetValidCharacters();
	SetInvalidCharacters();
	ioKeyboard::SetKeyboardLayoutSwitchingEnable(true);
	
	if(CScaleformMgr::BeginMethod(m_iMovieID, SF_BASE_CLASS_GENERIC, "SET_MULTI_LINE"))
	{
		bool bUseMultiLine = false;

		switch (params.m_MultiLineUsage)
		{
			case ioVirtualKeyboard::Params::FORCE_SINGLE_LINE :
				bUseMultiLine = false;
				break;

			case ioVirtualKeyboard::Params::FORCE_MULTI_LINE :
				bUseMultiLine = true;
				break;

			case ioVirtualKeyboard::Params::USE_MULTI_LINE_FOR_LONG_STRINGS :
				{
					const int MAX_SINGLE_LINE_SIZE = 32;
					bUseMultiLine = m_uMaxLength > MAX_SINGLE_LINE_SIZE;
				}
				break;
		}

		CScaleformMgr::AddParamBool(bUseMultiLine);	// DEFAULT VALUE
		CScaleformMgr::EndMethod();
	}

	if(CScaleformMgr::BeginMethod(m_iMovieID, SF_BASE_CLASS_GENERIC, "SET_TEXT_BOX"))
	{
		const int MAX_STR_LENGTH = ioVirtualKeyboard::MAX_STRING_LENGTH_FOR_VIRTUAL_KEYBOARD;
		char title[MAX_STR_LENGTH] = { 0 };
		char description[MAX_STR_LENGTH] = { 0 };

		if( params.m_Title != NULL )
		{
            WideToUtf8( title, params.m_Title, MAX_STR_LENGTH );
        }
		if( params.m_Description != NULL )
		{
            WideToUtf8( description, params.m_Description, MAX_STR_LENGTH );
        }
		if(params.m_InitialValue != NULL)
		{
            safecpy( m_Text, params.m_InitialValue, NELEM(m_Text) );
        }

		m_uCursorLocation = (u32)StringLengthChar16(m_Text);

		// note: currently title doesn't do anything so it has to be swapped with description.
		USES_CONVERSION;
		CScaleformMgr::AddParamString(description);				// TITLE
		CScaleformMgr::AddParamString(title);					// DESCRIPTION
		CScaleformMgr::AddParamString(WIDE_TO_UTF8(m_Text), false);	// DEFAULT VALUE
		CScaleformMgr::EndMethod();

		if(ms_Movie.BeginMethod("SET_CURSOR_LOCATION"))
		{
			ms_Movie.AddParam(m_uCursorLocation);
			ms_Movie.EndMethod();
		}

		m_eState = ioVirtualKeyboard::kKBState_PENDING;

		ioValue::ReadOptions options;
		options.SetFlags(ioValue::ReadOptions::F_READ_DISABLED, true);

		const CControl& control  = CControlMgr::GetMainFrontendControl();
		m_bWaitForEnterRelease   = control.GetFrontendAccept().IsDown(ioValue::BUTTON_DOWN_THRESHOLD, options) ||
								   control.GetFrontendAccept().IsReleased(ioValue::BUTTON_DOWN_THRESHOLD, options);

		#if RSG_PC
		const bool isKBM		 = control.WasKeyboardMouseLastKnownSource();

		const ioValue& enterExit = control.GetFrontendEnterExitAlternate();
		const ioValue& back		 = control.GetFrontendCancel();

		m_bWaitForBackRelease	 = (isKBM && (enterExit.IsDown(ioValue::BUTTON_DOWN_THRESHOLD, options) || enterExit.IsReleased(ioValue::BUTTON_DOWN_THRESHOLD, options)) ) ||
								   (isKBM == false && (back.IsDown(ioValue::BUTTON_DOWN_THRESHOLD, options) || back.IsReleased(ioValue::BUTTON_DOWN_THRESHOLD, options)) );
		
		#else
		m_bWaitForBackRelease = false;
		#endif

		CTextInputBox::SetInstructionalButtons(CControlMgr::GetPlayerMappingControl().WasKeyboardMouseLastKnownSource() ? FE_WARNING_OK | FE_WARNING_QUIT : FE_WARNING_OK_CANCEL);
		m_bLastInputKeyboard = CControlMgr::GetPlayerMappingControl().WasKeyboardMouseLastKnownSource();

		return;
	}

	m_eState = ioVirtualKeyboard::kKBState_FAILED;
}


//////////////////////////////////////////////////////////////////////////

void CTextInputBox::HandleXML( parTreeNode* pButtonMenu )
{
	parTreeNode::ChildNodeIterator pChildrenStart = pButtonMenu->BeginChildren();
	parTreeNode::ChildNodeIterator pChildrenEnd = pButtonMenu->EndChildren();

	// first, we have to find the highest bitindex in the list (we can't use NumChildren because bits may go dead)
	int iHighestBitIndex = -1;
	for(parTreeNode::ChildNodeIterator ci = pChildrenStart; ci != pChildrenEnd; ++ci)
	{
		parElement& rCurElement = (*ci)->GetElement();
		iHighestBitIndex = Max(rCurElement.FindAttributeIntValue("BitIndex", -1, true), iHighestBitIndex);
	}

	ms_ButtonData.Reset();
	ms_ButtonData.ResizeGrow(iHighestBitIndex+1); // +1 to adjust for Zero

	for(parTreeNode::ChildNodeIterator ci = pChildrenStart; ci != pChildrenEnd; ++ci)
	{
		parElement& rCurElement = (*ci)->GetElement();
		// check for bitIndex
		u32 iBitIndex = rCurElement.FindAttributeIntValue("BitIndex", -1, true);
		if( iBitIndex == -1 || !uiVerifyf(iBitIndex >= 0 && iBitIndex < 64, "Can only handle 64-bit values for bitindex! '%u' is just out of range!", iBitIndex) )
			continue;

		CMenuButtonWithSound& curSound = ms_ButtonData[iBitIndex];

		PARSER.LoadObject(*ci, curSound);

		uiAssertf(curSound.m_ButtonInput != UNDEFINED_INPUT || curSound.m_ButtonInputGroup != INPUTGROUP_INVALID || curSound.m_RawButtonIcon != ICON_INVALID,
			"Invalid button icon!");

		// set up sound
		char temp[32];
		curSound.m_Sound.SetFromString( rCurElement.FindAttributeStringValue("Sound", "SELECT", temp, 32) );

		int iButton = GetButtonMaskIndex(curSound.m_ButtonInput);
		if( iButton != -1 )
		{
			ms_AffectedButtonMasks[iButton] |= BIT_64(iBitIndex);
		}
	}
}
void CTextInputBox::UpdateDisplayParam()
{
	if(CScaleformMgr::BeginMethod(m_iMovieID, SF_BASE_CLASS_GENERIC, "UPDATE_DISPLAY_PARAMS"))
	{
		CScaleformMgr::AddParamInt(VideoResManager::GetUIWidth());
		CScaleformMgr::AddParamInt(VideoResManager::GetUIHeight());
		CScaleformMgr::EndMethod();
	}
}

void CTextInputBox::SetCursorLocation(int iCursorLocation)
{
	if(uiVerify(iCursorLocation >= 0 && iCursorLocation < m_uMaxLength))
	{
		m_uCursorLocation = (u32)iCursorLocation;
	}
}


void CTextInputBox::HandleInput()
{
	ioValue::ReadOptions options;
	options.SetFlags(ioValue::ReadOptions::F_READ_DISABLED, true);

	const CControl& control = CControlMgr::GetMainFrontendControl();

	const ioValue& exit   = control.GetFrontendEnterExitAlternate();
	const ioValue& cancel = control.GetFrontendCancel();
	const ioValue& left = control.GetFrontendLeft();
	const ioValue& right = control.GetFrontendRight();
	const ioValue& up = control.GetFrontendUp();
	const ioValue& down = control.GetFrontendDown();

	// For the arrow movement keys.
	bool bCursorLocChanged = false;
	bool bTextChanged = false;

	if(m_bImeTextInputHasFocus)
	{
		const ioValue& accept = control.GetFrontendAccept();

		// Clear IME input focus when both buttons are up. This will be reset next frame if it still has input focus. Note, we require two frames up so
		// that IsReleased() does not cause the input window to close.
		m_bImeTextInputHasFocus = ioKeyboard::ImeIsInProgress() ||
								  accept.IsDown(ioValue::BUTTON_DOWN_THRESHOLD, options) ||
								  accept.WasDown(ioValue::BUTTON_DOWN_THRESHOLD, options) ||
								  exit.IsDown(ioValue::BUTTON_DOWN_THRESHOLD, options) ||
								  exit.WasDown(ioValue::BUTTON_DOWN_THRESHOLD, options);
	}
	else
	{
		if(m_bWaitForEnterRelease || m_bWaitForBackRelease)
		{
			if(control.GetFrontendAccept().IsUp(ioValue::BUTTON_DOWN_THRESHOLD, options))
			{
				m_bWaitForEnterRelease = false;
			}

			// When using keyboard/mouse use GetFrontendEnterExit (escape key), otherwise use the GetFrontendCancel (controller B/O button).
			if( (cancel.GetLastSource().m_DeviceIndex != ioSource::IOMD_KEYBOARD_MOUSE && cancel.IsUp(ioValue::BUTTON_DOWN_THRESHOLD, options)) ||
				(exit.GetLastSource().m_DeviceIndex == ioSource::IOMD_KEYBOARD_MOUSE) && exit.IsUp(ioValue::BUTTON_DOWN_THRESHOLD, options))
			{
				m_bWaitForBackRelease = false;
			}

			return;
		}

		if (control.GetFrontendAccept().IsReleased(ioValue::BUTTON_DOWN_THRESHOLD, options))
		{
			m_eState = ioVirtualKeyboard::kKBState_SUCCESS;
			m_AcceptCallback.Call();
			Close();
			return;
		}

		// When using keyboard/mouse use GetFrontendEnterExit (escape key), otherwise use the GetFrontendCancel (controller B/O button).
		if( CMousePointer::IsMouseBack() ||
			(cancel.GetLastSource().m_DeviceIndex != ioSource::IOMD_KEYBOARD_MOUSE && cancel.IsReleased(ioValue::BUTTON_DOWN_THRESHOLD, options)) ||
			(exit.GetLastSource().m_DeviceIndex == ioSource::IOMD_KEYBOARD_MOUSE && exit.IsReleased(ioValue::BUTTON_DOWN_THRESHOLD, options)))
		{
			m_eState = ioVirtualKeyboard::kKBState_CANCELLED;
			m_DeclineCallback.Call();
			Close();
			return;
		}

		// Arrow keys
		GFxKey::Code keyPressed = GFxKey::Code::VoidSymbol;

		if(left.GetLastSource().m_DeviceIndex == ioSource::IOMD_KEYBOARD_MOUSE && left.IsDown(ioValue::BUTTON_DOWN_THRESHOLD, options))
		{
			if (m_uCursorLocation > 0 &&
				fwTimer::GetSystemTimeInMilliseconds() - m_uKeyHeldDelay > KEY_HELD_THRESHOLD)
			{
				m_uCursorLocation--;
				bCursorLocChanged = true;
				m_uKeyHeldDelay = fwTimer::GetSystemTimeInMilliseconds();
				keyPressed = GFxKey::Code::Left;
			}
		}
		if(right.GetLastSource().m_DeviceIndex == ioSource::IOMD_KEYBOARD_MOUSE && right.IsDown(ioValue::BUTTON_DOWN_THRESHOLD, options))
		{
			if(m_uCursorLocation < m_uMaxLength)
			{
				if (m_Text[m_uCursorLocation + 1] != '\0' && 
					fwTimer::GetSystemTimeInMilliseconds() - m_uKeyHeldDelay > KEY_HELD_THRESHOLD)
				{
					m_uCursorLocation++;
					bCursorLocChanged = true;
					m_uKeyHeldDelay = fwTimer::GetSystemTimeInMilliseconds();
					keyPressed = GFxKey::Code::Right;
				}
				else if(m_Text[m_uCursorLocation] != '\0' && m_Text[m_uCursorLocation + 1] == '\0' && 
					fwTimer::GetSystemTimeInMilliseconds() - m_uKeyHeldDelay > KEY_HELD_THRESHOLD)
				{
					m_uCursorLocation++;
					bCursorLocChanged = true;
					m_uKeyHeldDelay = fwTimer::GetSystemTimeInMilliseconds();
					keyPressed = GFxKey::Code::Right;
				}
			}
		}

		if(up.GetLastSource().m_DeviceIndex == ioSource::IOMD_KEYBOARD_MOUSE && up.IsDown(ioValue::BUTTON_DOWN_THRESHOLD, options))
		{
			if(fwTimer::GetSystemTimeInMilliseconds() - m_uKeyHeldDelay > KEY_HELD_THRESHOLD)
			{
				keyPressed = GFxKey::Code::Up;
				m_uKeyHeldDelay = fwTimer::GetSystemTimeInMilliseconds();
			}
		}
		else if(down.GetLastSource().m_DeviceIndex == ioSource::IOMD_KEYBOARD_MOUSE && down.IsDown(ioValue::BUTTON_DOWN_THRESHOLD, options))
		{
			if(fwTimer::GetSystemTimeInMilliseconds() - m_uKeyHeldDelay > KEY_HELD_THRESHOLD)
			{
				keyPressed = GFxKey::Code::Down;
				m_uKeyHeldDelay = fwTimer::GetSystemTimeInMilliseconds();
			}
		}

		if(keyPressed != GFxKey::Code::VoidSymbol && ms_Movie.BeginMethod("HANDLE_KEY_PRESS"))
		{
			ms_Movie.AddParam(keyPressed);
			ms_Movie.EndMethod();
		}

		if (CControlMgr::GetKeyboard().GetKeyDown(KEY_DELETE, KEYBOARD_MODE_GAME) && 
			fwTimer::GetSystemTimeInMilliseconds() - m_uKeyHeldDelay > KEY_HELD_THRESHOLD)
		{
			ShiftArrayBack(m_uCursorLocation);
			bTextChanged = true;
			m_uKeyHeldDelay = fwTimer::GetSystemTimeInMilliseconds();
		}
	}

	const char16 *pInputBuffer = ioKeyboard::GetInputText();

	// There is no PC text and text hasn't changed (i.e. deleted)
	if((pInputBuffer == NULL || pInputBuffer[0] == '\0') && !bTextChanged)
	{
		if(bCursorLocChanged && ms_Movie.BeginMethod("SET_CURSOR_LOCATION"))
		{
			ms_Movie.AddParam(m_uCursorLocation);
			ms_Movie.EndMethod();
		}
				
		return;
	}

	for(u32 i = 0; i < ioKeyboard::TEXT_BUFFER_LENGTH && pInputBuffer[i] != '\0'; ++i)
	{
		// add each character.
		if(pInputBuffer[i] == KEY_BACK)
		{
			if(m_uCursorLocation > 0)
			{
				ShiftArrayBack(m_uCursorLocation - 1);
				m_uCursorLocation--;
			}
		}

		if(!CTextFormat::DoesCharacterExistInFont(FONT_STYLE_STANDARD, pInputBuffer[i]))
			continue;

		if (pInputBuffer[i]  && 
			CheckCharacterValidity(pInputBuffer[i]) &&
			pInputBuffer[i] != KEY_BACK &&
			pInputBuffer[i] != KEY_RETURN &&
			pInputBuffer[i] != KEY_ESCAPE)
		{
			if(StringLengthChar16(m_Text) < m_uMaxLength)
			{

#if __BANK && UTF_16_SPEW
				Displayf("Added character %x to text input string", pInputBuffer[i]);
#endif // __BANK && UTF_16_SPEW

				ShiftArrayForward(m_uCursorLocation);
				
				if(CTextFormat::IsUnsupportedSpace(pInputBuffer[i]))
					m_Text[m_uCursorLocation] = ' ';
				else
					m_Text[m_uCursorLocation] = pInputBuffer[i];

				m_uCursorLocation++;
			}
		}
	}

	if (IsActive() && CScaleformMgr::IsMovieActive(m_iMovieID))
	{
		if(ms_Movie.BeginMethod("UPDATE_INPUT"))
		{
			USES_CONVERSION;
			const char* textToShow = WIDE_TO_UTF8(m_Text);
			ms_Movie.AddParamString(textToShow, false);
			ms_Movie.AddParam(m_uCursorLocation);
			ms_Movie.EndMethod();
		}

		m_uLastInputTime = fwTimer::GetSystemTimeInMilliseconds();
	}

#if __BANK
	s_iInputBoxState = (int)m_eState;
#endif // __BANK
}

void CTextInputBox::Close()
{
	ioKeyboard::SetKeyboardLayoutSwitchingEnable(false);
	RemoveTextInputBoxMovie();
	
	m_AcceptCallback = NullCallback;
	m_DeclineCallback = NullCallback;
	m_bAllowComma = false;
}

int CTextInputBox::GetButtonMaskIndex(InputType eButton)
{
	switch(eButton) {
	case INPUT_FRONTEND_ACCEPT:
		return 0;

	case INPUT_FRONTEND_CANCEL:
		return 1;

	// There appears to be two input for this button.
	case INPUT_FRONTEND_RLEFT:
	case INPUT_FRONTEND_X:
		return 2;

	// There appears to be two input for this button.
	case INPUT_FRONTEND_RUP:
	case INPUT_FRONTEND_Y:
		return 3;

	default:
		//uiAssertf(0, "Unhandled eButton! I don't know what index '%s' belongs in!", parser_eInstructionButtons_Strings[eButton] );
		return -1;
	}
}

void CTextInputBox::SetInstructionalButtons(u64 iFlags, CScaleformMovieWrapper* pOverride /* = NULL */)
{
//	if (iFlags != FE_WARNING_NONE)
	{
		CMenuButtonList ButtonsToUse;

		ASSERT_ONLY(u64 iSanityField = 0;)

		// for all the bits that are turned on
		for(int i=0; iFlags; ++i, iFlags>>=1)
		{
			if( iFlags & 1 && i < ms_ButtonData.GetCount() && BIT_64(i) != FE_WARNING_NOSOUND)
			{
				ButtonsToUse.Add( ms_ButtonData[i] );

#if __ASSERT
				int iIndex = GetButtonMaskIndex(ms_ButtonData[i].m_ButtonInput);
				if( iIndex != -1 )
				{
					uiAssertf(!BIT_ISSET(iIndex, iSanityField), "Warning Screen was assigned button prompts with the same input! This simply will not work! Check your bitindices against frontend.xml/WarningScreen and try again!");
					iSanityField |= BIT_64(iIndex);
				}
#endif
			}
		}

		// sort those into whatever order code says
		ButtonsToUse.postLoad();

		// buttons GOOOOO
		ButtonsToUse.Draw( pOverride ? pOverride : &ms_Movie );
	}
}

void CTextInputBox::CheckIncomingFunctions(atHashWithStringBank methodName, const GFxValue* args)
{
	if (methodName == ATSTRINGHASH("CURSOR_LOCATION_CHANGED",0x4540bf03))
	{
		if (uiVerifyf(args[1].IsNumber(), "CURSOR_LOCATION_CHANGED params not compatible: %s", sfScaleformManager::GetTypeName(args[1])))
		{
			if(STextInputBox::IsInstantiated())
			{
				STextInputBox::GetInstance().SetCursorLocation((int)args[1].GetNumber());
			}
		}
	}
}

void CTextInputBox::ClearInstructionalButtons(CScaleformMovieWrapper* pOverride /* = NULL */)
{
	CMenuButtonList ButtonsToUse;

	// remove any buttons that may already be there
	ButtonsToUse.Reset();
	ButtonsToUse.Draw( pOverride ? pOverride : &ms_Movie );
}

void CTextInputBox::Update()
{
	STextInputBox::GetInstance().UpdateMenu();
}

void CTextInputBox::UpdateMenu()
{
	PF_AUTO_PUSH_TIMEBAR("CTextInputBox UpdateMenu");

	if (m_bActive && CScaleformMgr::IsMovieActive(m_iMovieID) && (!CWarningScreen::IsActive() || SReportMenu::GetInstance().IsActive()) )
	{
		if(CNewHud::UpdateImeText())
			m_bImeTextInputHasFocus = true;

		CControlMgr::GetMainPlayerControl(false).DisableAllInputs();
		m_bCursorThisFrame = ((fwTimer::GetSystemTimeInMilliseconds() / 1000) % 2) == 0;
		UpdateInput();
		m_bCursorPreviousFrame = m_bCursorThisFrame;

		bool const c_InputIsKeyboardMouse = CControlMgr::GetPlayerMappingControl().WasKeyboardMouseLastKnownSource();
		if (c_InputIsKeyboardMouse != m_bLastInputKeyboard)
		{
			ClearInstructionalButtons();
			CTextInputBox::SetInstructionalButtons(CControlMgr::GetPlayerMappingControl().WasKeyboardMouseLastKnownSource() ? FE_WARNING_OK | FE_WARNING_QUIT : FE_WARNING_OK_CANCEL);
		}

		m_bLastInputKeyboard = c_InputIsKeyboardMouse;
	}

}

void CTextInputBox::Render()
{
	STextInputBox::GetInstance().RenderMenu();
}

void CTextInputBox::RenderMenu()
{
	if (IsActive() && CScaleformMgr::IsMovieActive(m_iMovieID))
	{
		GRC_ALLOC_SCOPE_AUTO_PUSH_POP();
		grcStateBlock::SetRasterizerState(grcStateBlock::RS_NoBackfaceCull);
		grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_IgnoreDepth);
		grcStateBlock::SetBlendState(grcStateBlock::BS_CompositeAlpha);

		grcBindTexture(NULL);
		grcBegin(drawTriStrip, 4);
		grcColor4f(0.0f, 0.0f, 0.0f, 0.3f);
		grcVertex2f(0.0f, 0.0f);
		grcVertex2f(1.0f, 0.0f);
		grcVertex2f(0.0f, 1.0f);
		grcVertex2f(1.0f, 1.0f);
		grcEnd();

		// Reset stateblocks after drawing 
		grcStateBlock::SetRasterizerState(grcStateBlock::RS_Default);
		grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_Default);
		grcStateBlock::SetBlendState(grcStateBlock::BS_Default);
		
		CScaleformMgr::RenderMovie(m_iMovieID, 0.0f, true, false, 1.0f);
	}
}

void CTextInputBox::LoadTextInputBoxMovie()
{
	if (!m_bActive)
	{
		ms_Movie.CreateMovieAndWaitForLoad(SF_BASE_CLASS_GENERIC, TEXT_INPUT_BOX_MOVIE_NAME, Vector2(0.0f, 0.0f), Vector2(1.0f, 1.0f), true); 
		m_iMovieID = ms_Movie.GetMovieID();

		CScaleformMgr::ForceMovieUpdateInstantly(ms_Movie.GetMovieID(), true);
		CBusySpinner::RegisterInstructionalButtonMovie(ms_Movie.GetMovieID());

		m_bActive = true;
	}
}

void CTextInputBox::RemoveTextInputBoxMovie()
{
	reportDisplayf("CTextInputBox::RemoveReportMovie");

	if(CScaleformMgr::IsMovieActive(m_iMovieID))
	{ 
		ms_Movie.CallMethod("CLEANUP");
		ms_Movie.RemoveMovie();
		m_iMovieID = -1;
		m_bActive = false;

		CBusySpinner::UnregisterInstructionalButtonMovie(ms_Movie.GetMovieID()); // remove if it's a message on top of a message, and doesn't require this

	}
}

bool CTextInputBox::UpdateInput()
{
	if (!IsActive())
	{
		return false;
	}

	HandleInput();

	return false;
}

void CTextInputBox::SetInvalidCharacters()
{
	m_InvalidCharacters.clear();
	
	const int MAX_FILE_NAME_OFFLIMIT_CHARS = 10;
	const char16 INVALID_FILENAME_CHARS[MAX_FILE_NAME_OFFLIMIT_CHARS] = {0x3a, 0x2f, 0x5c, 0x7c, 0x2c, 0x22, 0x3f, 0x3c, 0x3e, 0};

	// Control characters 0-31 are not supported in the below filters
	const int FIRST_VALID_CHAR = 32; // Space
	if (m_eTextType == ioVirtualKeyboard::kTextType_INVALID ||
		m_eTextType == ioVirtualKeyboard::kTextType_DEFAULT || 
		m_eTextType == ioVirtualKeyboard::kTextType_EMAIL ||
		m_eTextType == ioVirtualKeyboard::kTextType_PASSWORD ||
		m_eTextType == ioVirtualKeyboard::kTextType_ALPHABET ||
		m_eTextType == ioVirtualKeyboard::kTextType_FILENAME)
	{
		for(int i = 0; i < FIRST_VALID_CHAR; ++i)
		{
			m_InvalidCharacters.PushAndGrow(char16(i));
		}
	}

	if(m_eTextType == ioVirtualKeyboard::kTextType_FILENAME)
	{
		for(int i = 0; i < MAX_FILE_NAME_OFFLIMIT_CHARS; ++i)
		{
			if(m_bAllowComma && INVALID_FILENAME_CHARS[i] == 0x2c)
				continue;

			m_InvalidCharacters.PushAndGrow(INVALID_FILENAME_CHARS[i]);
		}
	}
}

void CTextInputBox::SetValidCharacters()
{
    // TODO - Move all this defining to fwTextUtil so other systems can access our whitelist of characters 
#define NUMERIC_CHARS L"0123456789"
#define ENGLISH_CHARS L"ABCDEFGHIKJLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define EXTENDED_CHARS L"ðÐǦŞǧŽžêÊæÆáóéñĄĆĘŁŃÓŚŹŻąćęłńśźż¥£æþÞýÝøØåŠĞğšÿŸЁёàâäãèêëïîìíôòöõœùûüúçßÀÁÄÂÃÉÈÊËÏÎÌÍÔÒÖÕŒÙÛÜÚÑÇµАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабвгдежзийклмнопрстуфхцчшщьыъэюяЁё₸₽"
#define KOREAN_CHARS L"가각간갇갈갉갊감갑값갓갔강갖갗같갚갛개객갠갤갬갭갯갰갱갸갹갼걀걋걍걔걘걜거걱건걷걸걺검겁것겄겅겆겉겊겋게겐겔겜겝겟겠겡겨격겪견겯결겸겹겻겼경곁계곈곌곕곗고곡곤곧골곪곬곯곰곱곳공곶과곽관괄괆괌괍괏광괘괜괠괩괬괭괴괵괸괼굄굅굇굉교굔굘굡굣구국군굳굴굵굶굻굼굽굿궁궂궈궉권궐궜궝궤궷귀귁귄귈귐귑귓규균귤그극근귿글긁금급긋긍긔기긱긴긷길긺김깁깃깅깆깊까깍깎깐깔깖깜깝깟깠깡깥깨깩깬깰깸깹깻깼깽꺄꺅꺌꺼꺽꺾껀껄껌껍껏껐껑께껙껜껨껫껭껴껸껼꼇꼈꼍꼐꼬꼭꼰꼲꼴꼼꼽꼿꽁꽂꽃꽈꽉꽐꽜꽝꽤꽥꽹꾀꾄꾈꾐꾑꾕꾜꾸꾹꾼꿀꿇꿈꿉꿋꿍꿎꿔꿜꿨꿩꿰꿱꿴꿸뀀뀁뀄뀌뀐뀔뀜뀝뀨끄끅끈끊끌끎끓끔끕끗끙끝끼끽낀낄낌낍낏낑나낙낚난낟날낡낢남납낫났낭낮낯낱낳내낵낸낼냄냅냇냈냉냐냑냔냘냠냥너넉넋넌널넒넓넘넙넛넜넝넣네넥넨넬넴넵넷넸넹녀녁년녈념녑녔녕녘녜녠노녹논놀놂놈놉놋농높놓놔놘놜놨뇌뇐뇔뇜뇝뇟뇨뇩뇬뇰뇹뇻뇽누눅눈눋눌눔눕눗눙눠눴눼뉘뉜뉠뉨뉩뉴뉵뉼늄늅늉느늑는늘늙늚늠늡늣능늦늪늬늰늴니닉닌닐닒님닙닛닝닢다닥닦단닫달닭닮닯닳담답닷닸당닺닻닿대댁댄댈댐댑댓댔댕댜더덕덖던덛덜덞덟덤덥덧덩덫덮데덱덴델뎀뎁뎃뎄뎅뎌뎐뎔뎠뎡뎨뎬도독돈돋돌돎돐돔돕돗동돛돝돠돤돨돼됐되된될됨됩됫됴두둑둔둘둠둡둣둥둬뒀뒈뒝뒤뒨뒬뒵뒷뒹듀듄듈듐듕드득든듣들듦듬듭듯등듸디딕딘딛딜딤딥딧딨딩딪따딱딴딸땀땁땃땄땅땋때땍땐땔땜땝땟땠땡떠떡떤떨떪떫떰떱떳떴떵떻떼떽뗀뗄뗌뗍뗏뗐뗑뗘뗬또똑똔똘똥똬똴뙈뙤뙨뚜뚝뚠뚤뚫뚬뚱뛔뛰뛴뛸뜀뜁뜅뜨뜩뜬뜯뜰뜸뜹뜻띄띈띌띔띕띠띤띨띰띱띳띵라락란랄람랍랏랐랑랒랖랗래랙랜랠램랩랫랬랭랴략랸럇량러럭런럴럼럽럿렀렁렇레렉렌렐렘렙렛렝려력련렬렴렵렷렸령례롄롑롓로록론롤롬롭롯롱롸롼뢍뢨뢰뢴뢸룀룁룃룅료룐룔룝룟룡루룩룬룰룸룹룻룽뤄뤘뤠뤼뤽륀륄륌륏륑류륙륜률륨륩륫륭르륵른를름릅릇릉릊릍릎리릭린릴림립릿링마막만많맏말맑맒맘맙맛망맞맡맣매맥맨맬맴맵맷맸맹맺먀먁먈먕머먹먼멀멂멈멉멋멍멎멓메멕멘멜멤멥멧멨멩며멱면멸몃몄명몇몌모목몫몬몰몲몸몹못몽뫄뫈뫘뫙뫼묀묄묍묏묑묘묜묠묩묫무묵묶문묻물묽묾뭄뭅뭇뭉뭍뭏뭐뭔뭘뭡뭣뭬뮈뮌뮐뮤뮨뮬뮴뮷므믄믈믐믓미믹민믿밀밂밈밉밋밌밍및밑바박밖밗반받발밝밞밟밤밥밧방밭배백밴밸뱀뱁뱃뱄뱅뱉뱌뱍뱐뱝버벅번벋벌벎범법벗벙벚베벡벤벧벨벰벱벳벴벵벼벽변별볍볏볐병볕볘볜보복볶본볼봄봅봇봉봐봔봤봬뵀뵈뵉뵌뵐뵘뵙뵤뵨부북분붇불붉붊붐붑붓붕붙붚붜붤붰붸뷔뷕뷘뷜뷩뷰뷴뷸븀븃븅브븍븐블븜븝븟비빅빈빌빎빔빕빗빙빚빛빠빡빤빨빪빰빱빳빴빵빻빼빽뺀뺄뺌뺍뺏뺐뺑뺘뺙뺨뻐뻑뻔뻗뻘뻠뻣뻤뻥뻬뼁뼈뼉뼘뼙뼛뼜뼝뽀뽁뽄뽈뽐뽑뽕뾔뾰뿅뿌뿍뿐뿔뿜뿟뿡쀼쁑쁘쁜쁠쁨쁩삐삑삔삘삠삡삣삥사삭삯산삳살삵삶삼삽삿샀상샅새색샌샐샘샙샛샜생샤샥샨샬샴샵샷샹섀섄섈섐섕서석섞섟선섣설섦섧섬섭섯섰성섶세섹센셀셈셉셋셌셍셔셕션셜셤셥셧셨셩셰셴셸솅소속솎손솔솖솜솝솟송솥솨솩솬솰솽쇄쇈쇌쇔쇗쇘쇠쇤쇨쇰쇱쇳쇼쇽숀숄숌숍숏숑수숙순숟술숨숩숫숭숯숱숲숴쉈쉐쉑쉔쉘쉠쉥쉬쉭쉰쉴쉼쉽쉿슁슈슉슐슘슛슝스슥슨슬슭슴습슷승시식신싣실싫심십싯싱싶싸싹싻싼쌀쌈쌉쌌쌍쌓쌔쌕쌘쌜쌤쌥쌨쌩썅써썩썬썰썲썸썹썼썽쎄쎈쎌쏀쏘쏙쏜쏟쏠쏢쏨쏩쏭쏴쏵쏸쐈쐐쐤쐬쐰쐴쐼쐽쑈쑤쑥쑨쑬쑴쑵쑹쒀쒔쒜쒸쒼쓩쓰쓱쓴쓸쓺쓿씀씁씌씐씔씜씨씩씬씰씸씹씻씽아악안앉않알앍앎앓암압앗았앙앝앞애액앤앨앰앱앳앴앵야약얀얄얇얌얍얏양얕얗얘얜얠얩어억언얹얻얼얽얾엄업없엇었엉엊엌엎에엑엔엘엠엡엣엥여역엮연열엶엷염엽엾엿였영옅옆옇예옌옐옘옙옛옜오옥온올옭옮옰옳옴옵옷옹옻와왁완왈왐왑왓왔왕왜왝왠왬왯왱외왹왼욀욈욉욋욍요욕욘욜욤욥욧용우욱운울욹욺움웁웃웅워웍원월웜웝웠웡웨웩웬웰웸웹웽위윅윈윌윔윕윗윙유육윤율윰윱윳융윷으윽은을읊음읍읏응읒읓읔읕읖읗의읜읠읨읫이익인일읽읾잃임입잇있잉잊잎자작잔잖잗잘잚잠잡잣잤장잦재잭잰잴잼잽잿쟀쟁쟈쟉쟌쟎쟐쟘쟝쟤쟨쟬저적전절젊점접젓정젖제젝젠젤젬젭젯젱져젼졀졈졉졌졍졔조족존졸졺좀좁좃종좆좇좋좌좍좔좝좟좡좨좼좽죄죈죌죔죕죗죙죠죡죤죵주죽준줄줅줆줌줍줏중줘줬줴쥐쥑쥔쥘쥠쥡쥣쥬쥰쥴쥼즈즉즌즐즘즙즛증지직진짇질짊짐집짓징짖짙짚짜짝짠짢짤짧짬짭짯짰짱째짹짼쨀쨈쨉쨋쨌쨍쨔쨘쨩쩌쩍쩐쩔쩜쩝쩟쩠쩡쩨쩽쪄쪘쪼쪽쫀쫄쫌쫍쫏쫑쫓쫘쫙쫠쫬쫴쬈쬐쬔쬘쬠쬡쭁쭈쭉쭌쭐쭘쭙쭝쭤쭸쭹쮜쮸쯔쯤쯧쯩찌찍찐찔찜찝찡찢찧차착찬찮찰참찹찻찼창찾채책챈챌챔챕챗챘챙챠챤챦챨챰챵처척천철첨첩첫첬청체첵첸첼쳄쳅쳇쳉쳐쳔쳤쳬쳰촁초촉촌촐촘촙촛총촤촨촬촹최쵠쵤쵬쵭쵯쵱쵸춈추축춘출춤춥춧충춰췄췌췐취췬췰췸췹췻췽츄츈츌츔츙츠측츤츨츰츱츳층치칙친칟칠칡침칩칫칭카칵칸칼캄캅캇캉캐캑캔캘캠캡캣캤캥캬캭컁커컥컨컫컬컴컵컷컸컹케켁켄켈켐켑켓켕켜켠켤켬켭켯켰켱켸코콕콘콜콤콥콧콩콰콱콴콸쾀쾅쾌쾡쾨쾰쿄쿠쿡쿤쿨쿰쿱쿳쿵쿼퀀퀄퀑퀘퀭퀴퀵퀸퀼큄큅큇큉큐큔큘큠크큭큰클큼큽킁키킥킨킬킴킵킷킹타탁탄탈탉탐탑탓탔탕태택탠탤탬탭탯탰탱탸턍터턱턴털턺텀텁텃텄텅테텍텐텔템텝텟텡텨텬텼톄톈토톡톤톨톰톱톳통톺톼퇀퇘퇴퇸툇툉툐투툭툰툴툼툽툿퉁퉈퉜퉤튀튁튄튈튐튑튕튜튠튤튬튱트특튼튿틀틂틈틉틋틔틘틜틤틥티틱틴틸팀팁팃팅파팍팎판팔팖팜팝팟팠팡팥패팩팬팰팸팹팻팼팽퍄퍅퍼퍽펀펄펌펍펏펐펑페펙펜펠펨펩펫펭펴편펼폄폅폈평폐폘폡폣포폭폰폴폼폽폿퐁퐈퐝푀푄표푠푤푭푯푸푹푼푿풀풂품풉풋풍풔풩퓌퓐퓔퓜퓟퓨퓬퓰퓸퓻퓽프픈플픔픕픗피픽핀필핌핍핏핑하학한할핥함합핫항해핵핸핼햄햅햇했행햐향허헉헌헐헒험헙헛헝헤헥헨헬헴헵헷헹혀혁현혈혐협혓혔형혜혠혤혭호혹혼홀홅홈홉홋홍홑화확환활홧황홰홱홴횃횅회획횐횔횝횟횡효횬횰횹횻후훅훈훌훑훔훗훙훠훤훨훰훵훼훽휀휄휑휘휙휜휠휨휩휫휭휴휵휸휼흄흇흉흐흑흔흖흗흘흙흠흡흣흥흩희흰흴흼흽힁히힉힌힐힘힙힛힝"
#define CHINESE_CHARS_1 L"一乙丁七乃九了二人儿入八几刀刁力匕十卜又三下丈上丫丸凡久么也乞于亡兀刃勺千叉口土士夕大女子孑孓寸小尢尸山川工己已巳巾干廾弋弓才丑丐不中丰丹之尹予云井互五亢仁什仃仆仇仍今介仄元允內六兮公冗凶分切刈勻勾勿化匹午升卅卞厄友及反壬天夫太夭孔少尤尺屯巴幻廿弔引心戈戶手扎支文斗斤方日曰月木欠止歹毋比毛氏水火爪父爻片牙牛犬王丙世丕且丘主乍乏乎以付仔仕他仗代令仙仞充兄冉冊冬凹出凸刊加功包匆北匝仟半卉卡占卯卮去可古右召叮叩叨叼司叵叫另只史叱台句叭叻四囚外央失奴奶孕它尼巨巧左市布平幼弁弘弗必戊打扔扒扑斥旦朮本未末札正母民氐永汁汀氾犯玄玉瓜瓦甘生用甩田由甲申疋白皮皿目矛矢石示禾穴立丞丟乒乓乩亙交亦亥仿伉伙伊伕伍伐休伏仲件任仰仳份企伋光兇兆先全共再冰列刑划刎刖劣匈匡匠印危吉吏同吊吐吁吋各向名合吃后吆吒因回囝圳地在圭圬圯圩夙多夷夸妄奸妃好她如妁字存宇守宅安寺尖屹州帆并年式弛忙忖戎戌戍成扣扛托收早旨旬旭曲曳有朽朴朱朵次此死氖汝汗汙江池汐汕污汛汍汎灰牟牝百竹米糸缶羊羽老考而耒耳聿肉肋肌臣自至臼舌舛舟艮色艾虫血行衣西阡串亨位住佇佗佞伴佛何估佐佑伽伺伸佃佔似但佣作你伯低伶余佝佈佚兌克免兵冶冷別判利刪刨劫助努劬匣即卵吝吭吞吾否呎吧呆呃吳呈呂君吩告吹吻吸吮吵吶吠吼呀吱含吟听囪困囤囫坊坑址坍均坎圾坐坏圻壯夾妝妒妨妞妣妙妖妍妤妓妊妥孝孜孚孛完宋宏尬局屁尿尾岐岑岔岌巫希序庇床廷弄弟彤形彷役忘忌志忍忱快忸忪戒我抄抗抖技扶抉扭把扼找批扳抒扯折扮投抓抑抆改攻攸旱更束李杏材村杜杖杞杉杆杠杓杗步每求汞沙沁沈沉沅沛汪決沐汰沌汨沖沒汽沃汲汾汴沆汶沍沔沘沂灶灼災灸牢牡牠狄狂玖甬甫男甸皂盯矣私秀禿究系罕肖肓肝肘肛肚育良芒芋芍見角言谷豆豕貝赤走足身車辛辰迂迆迅迄巡邑邢邪邦那酉釆里防阮阱阪阬並乖乳事些亞享京佯依侍佳使佬供例來侃佰併侈佩佻侖佾侏侑佺兔兒兕兩具其典冽函刻券刷刺到刮制剁劾劻卒協卓卑卦卷卸卹取叔受味呵咖呸咕咀呻呷咄咒咆呼咐呱呶和咚呢周咋命咎固垃坷坪坩坡坦坤坼夜奉奇奈奄奔妾妻委妹妮姑姆姐姍始姓姊妯妳姒姅孟孤季宗定官宜宙宛尚屈居屆岷岡岸岩岫岱岳帘帚帖帕帛帑幸庚店府底庖延弦弧弩往征彿彼忝忠忽念忿怏怔怯怵怖怪怕怡性怩怫怛或戕房戾所承拉拌拄抿拂抹拒招披拓拔拋拈抨抽押拐拙拇拍抵拚抱拘拖拗拆抬拎放斧於旺昔易昌昆昂明昀昏昕昊昇服朋杭枋枕東果杳杷枇枝林杯杰板枉松析杵枚枓杼杪杲欣武歧歿氓氛泣注泳沱泌泥河沽沾沼波沫法泓沸泄油況沮泗泅泱沿治泡泛泊沬泯泜泖泠炕炎炒炊炙爬爭爸版牧物狀狎狙狗狐玩玨玟玫玥甽疝疙疚的盂盲直知矽社祀祁秉秈空穹竺糾罔羌羋者肺肥肢肱股肫肩肴肪肯臥臾舍芳芝芙芭芽芟芹花芬芥芯芸芣芰芾芷虎虱初表軋迎返近邵邸邱邶采金長門阜陀阿阻附陂隹雨青非亟亭亮信侵侯便俠俑俏保促侶俘俟俊俗侮俐俄係俚俎俞侷兗冒冑冠剎剃削前剌剋則勇勉勃勁匍南卻厚叛咬哀咨哎哉咸咦咳哇哂咽咪品哄哈咯咫咱咻咩咧咿囿垂型垠垣垢城垮垓奕契奏奎奐姜姘姿姣姨娃姥姪姚姦威姻孩宣宦室客宥封屎屏屍屋峙峒巷帝帥帟幽庠度建弈弭彥很待徊律徇後徉怒思怠急怎怨恍恰恨恢恆恃恬恫恪恤扁拜挖按拼拭持拮拽指拱拷拯括拾拴挑挂政故斫施既春昭映昧是星昨昱昤曷柿染柱柔某柬架枯柵柩柯柄柑枴柚查枸柏柞柳枰柙柢柝柒歪殃殆段毒毗氟泉洋洲洪流津洌洱洞洗活洽派洶洛泵洹洧洸洩洮洵洎洫炫為炳炬炯炭炸炮炤爰牲牯牴狩狠狡玷珊玻玲珍珀玳甚甭畏界畎畋疫疤疥疢疣癸皆皇皈盈盆盃盅省盹相眉看盾盼眇矜砂研砌砍祆祉祈祇禹禺科秒秋穿突竿竽籽紂紅紀紉紇約紆缸美羿耄耐耍耑耶胖胥胚胃胄背胡胛胎胞胤胝致舢苧范茅苣苛苦茄若茂茉苒苗英茁苜苔苑苞苓苟苯茆虐虹虻虺衍衫要觔計訂訃貞負赴赳趴軍軌述迦迢迪迥迭迫迤迨郊郎郁郃酋酊重閂限陋陌降面革韋韭音頁風飛食首香乘亳倌倍倣俯倦倥俸倩倖倆值借倚倒們俺倀倔倨俱倡個候倘俳修倭倪俾倫倉兼冤冥冢凍凌准凋剖剜剔剛剝匪卿原厝叟哨唐唁唷哼哥哲唆哺唔哩哭員唉哮哪哦唧唇哽唏圃圄埂埔埋埃堉夏套奘奚娑娘娜娟娛娓姬娠娣娩娥娌娉孫屘宰害家宴宮宵容宸射屑展屐峭峽峻峪峨峰島崁峴差席師庫庭座弱徒徑徐恙恣恥恐恕恭恩息悄悟悚悍悔悌悅悖扇拳挈拿捎挾振捕捂捆捏捉挺捐挽挪挫挨捍捌效敉料旁旅時晉晏晃晒晌晅晁書朔朕朗校核案框桓根桂桔栩梳栗桌桑栽柴桐桀格桃株桅栓栘桁殊殉殷氣氧氨氦氤泰浪涕消涇浦浸海浙涓浬涉浮浚浴浩涌涊浹涅浥涔烊烘烤烙烈烏爹特狼狹狽狸狷玆班琉珮珠珪珞畔畝畜畚留疾病症疲疳疽疼疹痂疸皋皰益盍盎眩真眠眨矩砰砧砸砝破砷砥砭砠砟砲祕祐祠祟祖神祝祗祚秤秣秧租秦秩秘窄窈站笆笑粉紡紗紋紊素索純紐紕級紜納紙紛缺罟羔翅翁耆耘耕耙耗耽耿胱脂胰脅胭胴脆胸胳脈能脊胼胯臭臬舀舐航舫舨般芻茫荒荔荊茸荐草茵茴荏茲茹茶茗荀茱茨荃虔蚊蚪蚓蚤蚩蚌蚣蚜衰衷袁袂衽衹記訐討訌訕訊託訓訖訏訑豈豺豹財貢起躬軒軔軏辱送逆迷退迺迴逃追逅迸邕郡郝郢酒配酌釘針釗釜釙閃院陣陡陛陝除陘陞隻飢馬骨高鬥鬲鬼乾偺偽停假偃偌做偉健偶偎偕偵側偷偏倏偯偭兜冕凰剪副勒務勘動匐匏匙匿區匾參曼商啪啦啄啞啡啃啊唱啖問啕唯啤唸售啜唬啣唳啁啗圈國圉域堅堊堆埠埤基堂堵執培夠奢娶婁婉婦婪婀娼婢婚婆婊孰寇寅寄寂宿密尉專將屠屜屝崇崆崎崛崖崢崑崩崔崙崤崧崗巢常帶帳帷康庸庶庵庾張強彗彬彩彫得徙從徘御徠徜恿患悉悠您惋悴惦悽情悻悵惜悼惘惕惆惟悸惚惇戚戛扈掠控捲掖探接捷捧掘措捱掩掉掃掛捫推掄授掙採掬排掏掀捻捩捨捺敝敖救教敗啟敏敘敕敔斜斛斬族旋旌旎晝晚晤晨晦晞曹勗望梁梯梢梓梵桿桶梱梧梗械梃棄梭梆梅梔條梨梟梡梂欲殺毫毬氫涎涼淳淙液淡淌淤添淺清淇淋涯淑涮淞淹涸混淵淅淒渚涵淚淫淘淪深淮淨淆淄涪淬涿淦烹焉焊烽烯爽牽犁猜猛猖猓猙率琅琊球理現琍瓠瓶瓷甜產略畦畢異疏痔痕疵痊痍皎盔盒盛眷眾眼眶眸眺硫硃硎祥票祭移窒窕笠笨笛第符笙笞笮粒粗粕絆絃統紮紹紼絀細紳組累終紲紱缽羞羚翌翎習耜聊聆脯脖脣脫脩脰脤舂舵舷舶船莎莞莘荸莢莖莽莫莒莊莓莉莠荷荻荼莆莧處彪蛇蛀蚶蛄蚵蛆蛋蚱蚯蛉術袞袈被袒袖袍袋覓規訪訝訣訥許設訟訛訢豉豚販責貫貨貪貧赧赦趾趺軛軟這逍通逗連速逝逐逕逞造透逢逖逛途部郭都酗野釵釦釣釧釭釩閉陪陵陳陸陰陴陶陷陬雀雪雩章竟頂頃魚鳥鹵鹿麥麻傢傍傅備傑傀傖傘傚最凱割剴創剩勞勝勛博厥啻喀喧啼喊喝喘喂喜喪喔喇喋喃喳單喟唾喲喚喻喬喱啾喉喫喙圍堯堪場堤堰報堡堝堠壹壺奠婷媚婿媒媛媧孳孱寒富寓寐尊尋就嵌嵐崴嵇巽幅帽幀幃幾廊廁廂廄弼彭復循徨惑惡悲悶惠愜愣惺愕惰惻惴慨惱愎惶愉愀愒戟扉掣掌描揀揩揉揆揍插揣提握揖揭揮捶援揪換摒揚揹敞敦敢散斑斐斯普晰晴晶景暑智晾晷曾替期朝棺棕棠棘棗椅棟棵森棧棹棒棲棣棋棍植椒椎棉棚楮棻款欺欽殘殖殼毯氮氯氬港游湔渡渲湧湊渠渥渣減湛湘渤湖湮渭渦湯渴湍渺測湃渝渾滋溉渙湎湣湄湲湩湟焙焚焦焰無然煮焜牌犄犀猶猥猴猩琺琪琳琢琥琵琶琴琯琛琦琨甥甦畫番痢痛痣痙痘痞痠登發皖皓皴盜睏短硝硬硯稍稈程稅稀窘窗窖童竣等策筆筐筒答筍筋筏筑粟粥絞結絨絕紫絮絲絡給絢絰絳善翔翕耋聒肅腕腔腋腑腎脹腆脾腌腓腴舒舜菩萃菸萍菠菅萋菁華菱菴著萊菰萌菌菽菲菊萸萎萄菜萇菔菟虛蛟蛙蛭蛔蛛蛤蛐蛞街裁裂袱覃視註詠評詞証詁詔詛詐詆訴診訶詖象貂貯貼貳貽賁費賀貴買貶貿貸越超趁跎距跋跚跑跌跛跆軻軸軼辜逮逵週逸進逶鄂郵鄉郾酣酥量鈔鈕鈣鈉鈞鈍鈐鈇鈑閔閏開閑間閒閎隊階隋陽隅隆隍陲隄雁雅雄集雇雯雲韌項順須飧飪飯飩飲飭馮馭黃黍黑亂傭債傲傳僅傾催傷傻傯僇剿剷剽募勦勤勢勣匯嗟嗨嗓嗦嗎嗜嗇嗑嗣嗤嗯嗚嗡嗅嗆嗥嗉園圓塞塑塘塗塚塔填塌塭塊塢塒塋奧嫁嫉嫌媾媽媼媳嫂媲嵩嵯幌幹廉廈弒彙徬微愚意慈感想愛惹愁愈慎慌慄慍愾愴愧愍愆愷戡戢搓搾搞搪搭搽搬搏搜搔損搶搖搗搆敬斟新暗暉暇暈暖暄暘暍會榔業楚楷楠楔極椰概楊楨楫楞楓楹榆楝楣楛歇歲毀殿毓毽溢溯滓溶滂源溝滇滅溥溘溼溺溫滑準溜滄滔溪溧溴煎煙煩煤煉照煜煬煦煌煥煞煆煨煖爺牒猷獅猿猾瑯瑚瑕瑟瑞瑁琿瑙瑛瑜當畸瘀痰瘁痲痱痺痿痴痳盞盟睛睫睦睞督睹睪睬睜睥睨睢矮碎碰碗碘碌碉硼碑碓硿祺祿禁萬禽稜稚稠稔稟稞窟窠筷節筠筮筧粱粳粵經絹綑綁綏絛置罩罪署義羨群聖聘肆肄腱腰腸腥腮腳腫腹腺腦舅艇蒂葷落萱葵葦葫葉葬葛萼萵葡董葩葭葆虞虜號蛹蜓蜈蜇蜀蛾蛻蜂蜃蜆蜊衙裟裔裙補裘裝裡裊裕裒覜解詫該詳試詩詰誇詼詣誠話誅詭詢詮詬詹詻訾詨豢貊貉賊資賈賄貲賃賂賅跡跟跨路跳跺跪跤跦躲較載軾輊辟農運遊道遂達逼違遐遇遏過遍遑逾遁鄒鄗酬酪酩釉鈷鉗鈸鈽鉀鈾鉛鉋鉤鉑鈴鉉鉍鉅鈹鈿鉚閘隘隔隕雍雋雉雊雷電雹零靖靴靶預頑頓頊頒頌飼飴飽飾馳馱馴髡鳩麂鼎鼓鼠僧僮僥僖僭僚僕像僑僱僎僩兢凳劃劂匱厭嗾嘀嘛嘗嗽嘔嘆嘉嘍嘎嗷嘖嘟嘈嘐嗶團圖塵塾境墓墊塹墅塽壽夥夢夤奪奩嫡嫦嫩嫗嫖嫘嫣孵寞寧寡寥實寨寢寤察對屢嶄嶇幛幣幕幗幔廓廖弊彆彰徹慇愿態慷慢慣慟慚慘慵截撇摘摔撤摸摟摺摑摧搴摭摻敲斡旗旖暢暨暝榜榨榕槁榮槓構榛榷榻榫榴槐槍榭槌榦槃榣歉歌氳漳演滾漓滴漩漾漠漬漏漂漢滿滯漆漱漸漲漣漕漫漯澈漪滬漁滲滌滷熔熙煽熊熄熒爾犒犖獄獐瑤瑣瑪瑰瑭甄疑瘧瘍瘋瘉瘓盡監瞄睽睿睡磁碟碧碳碩碣禎福禍種稱窪窩竭端管箕箋筵算箝箔箏箸箇箄粹粽精綻綰綜綽綾綠緊綴網綱綺綢綿綵綸維緒緇綬罰翠翡翟聞聚肇腐膀膏膈膊腿膂臧臺與舔舞艋蓉蒿蓆蓄蒙蒞蒲蒜蓋蒸蓀蓓蒐蒼蓑蓊蜿蜜蜻蜢蜥蜴蜘蝕蜷蜩裳褂裴裹裸製裨褚裯誦誌語誣認誡誓誤說誥誨誘誑誚誧豪貍貌賓賑賒赫趙趕跼輔輒輕輓辣遠遘遜遣遙遞遢遝遛鄙鄘鄞酵酸酷酴鉸銀銅銘銖鉻銓銜銨鉼銑閡閨閩閣閥閤隙障際雌雒需靼鞅韶頗領颯颱餃餅餌餉駁骯骰髦魁魂鳴鳶鳳麼鼻齊億儀僻僵價儂儈儉儅凜劇劈劉劍劊勰厲嘮嘻嘹嘲嘿嘴嘩噓噎噗噴嘶嘯嘰墀墟增墳墜墮墩墦奭嬉嫻嬋嫵嬌嬈寮寬審寫層履嶝嶔幢幟幡廢廚廟廝廣廠彈影德徵慶慧慮慝慕憂慼慰慫慾憧憐憫憎憬憚憤憔憮戮摩摯摹撞撲撈撐撰撥撓撕撩撒撮播撫撚撬撙撢撳敵敷數暮暫暴暱樣樟槨樁樞標槽模樓樊槳樂樅槭樑歐歎殤毅毆漿潼澄潑潦潔澆潭潛潸潮澎潺潰潤澗潘滕潯潠潟熟熬熱熨牖犛獎獗瑩璋璃瑾璀畿瘠瘩瘟瘤瘦瘡瘢皚皺盤瞎瞇瞌瞑瞋磋磅確磊碾磕碼磐稿稼穀稽稷稻窯窮箭箱範箴篆篇篁箠篌糊締練緯緻緘緬緝編緣線緞緩綞緙緲緹罵罷羯翩耦膛膜膝膠膚膘蔗蔽蔚蓮蔬蔭蔓蔑蔣蔡蔔蓬蔥蓿蔆螂蝴蝶蝠蝦蝸蝨蝙蝗蝌蝓衛衝褐複褒褓褕褊誼諒談諄誕請諸課諉諂調誰論諍誶誹諛豌豎豬賠賞賦賤賬賭賢賣賜質賡赭趟趣踫踐踝踢踏踩踟踡踞躺輝輛輟輩輦輪輜輞輥適遮遨遭遷鄰鄭鄧鄱醇醉醋醃鋅銻銷鋪銬鋤鋁銳銼鋒鋇鋰銲閭閱霄霆震霉靠鞍鞋鞏頡頫頜颳養餓餒餘駝駐駟駛駑駕駒駙骷髮髯鬧魅魄魷魯鴆鴉鴃麩麾黎墨齒儒儘儔儐儕冀冪凝劑劓勳噙噫噹噩噤噸噪器噥噱噯噬噢噶壁墾壇壅奮嬝嬴學寰導彊憲憑憩憊懍憶憾懊懈戰擅擁擋撻撼據擄擇擂操撿擒擔撾整曆曉暹曄曇暸樽樸樺橙橫橘樹橄橢橡橋橇樵機橈歙歷氅濂澱澡濃澤濁澧澳激澹澶澦澠澴熾燉燐燒燈燕熹燎燙燜燃燄獨璜璣璘璟璞瓢甌甍瘴瘸瘺盧盥瞠瞞瞟瞥磨磚磬磧禦積穎穆穌穋窺篙簑築篤篛篡篩篦糕糖縊縑縈縛縣縞縝縉縐罹羲翰翱翮耨膳膩膨臻興艘艙蕊蕙蕈蕨蕩蕃蕉蕭蕪蕞螃螟螞螢融衡褪褲褥褫褡親覦諦諺諫諱謀諜諧諮諾謁謂諷諭諳諶諼豫豭貓賴蹄踱踴蹂踹踵輻輯輸輳辨辦遵遴選遲遼遺鄴醒錠錶鋸錳錯錢鋼錫錄錚錐錦錡錕錮錙閻隧隨險雕霎霑霖霍霓霏靛靜靦鞘頰頸頻頷頭頹頤餐館餞餛餡餚駭駢駱骸骼髻髭鬨鮑鴕鴣鴦鴨鴒鴛默黔龍龜優償儡儲勵嚎嚀嚐嚅嚇嚏壕壓壑壎嬰嬪嬤孺尷屨嶼嶺嶽嶸幫彌徽應懂懇懦懋戲戴擎擊擘擠擰擦擬擱擢擭斂斃曙曖檀檔檄檢檜櫛檣橾檗檐檠歜殮毚氈濘濱濟濠濛濤濫濯澀濬濡濩濕濮濰燧營燮燦燥燭燬燴燠爵牆獰獲璩環璦璨癆療癌盪瞳瞪瞰瞬瞧瞭矯磷磺磴磯礁禧禪穗窿簇簍篾篷簌篠糠糜糞糢糟糙糝縮績繆縷縲繃縫總縱繅繁縴縹繈縵縿縯罄翳翼聱聲聰聯聳臆臃膺臂臀膿膽臉膾臨舉艱薪薄蕾薜薑薔薯薛薇薨薊虧蟀蟑螳蟒蟆螫螻螺蟈蟋褻褶襄褸褽覬謎謗謙講謊謠謝謄謐豁谿豳賺賽購賸賻趨蹉蹋蹈蹊轄輾轂轅輿避遽還邁邂邀鄹醣醞醜鍍鎂錨鍵鍊鍥鍋錘鍾鍬鍛鍰鍚鍔闊闋闌闈闆隱隸雖霜霞鞠韓顆颶餵騁駿鮮鮫鮪鮭鴻鴿麋黏點黜黝黛鼾齋叢嚕嚮壙壘嬸彝懣戳擴擲擾攆擺擻擷斷曜朦檳檬櫃檻檸櫂檮檯歟歸殯瀉瀋濾瀆濺瀑瀏燻燼燾燸獷獵璧璿甕癖癘癒瞽瞿瞻瞼礎禮穡穢穠竄竅簫簧簪簞簣簡糧織繕繞繚繡繒繙罈翹翻職聶臍臏舊藏薩藍藐藉薰薺薹薦蟯蟬蟲蟠覆覲觴謨謹謬謫豐贅蹙蹣蹦蹤蹟蹕軀轉轍邇邃邈醫醬釐鎔鎊鎖鎢鎳鎮鎬鎰鎘鎚鎗闔闖闐闕離雜雙雛雞霤鞣鞦鞭韹額顏題顎顓颺餾餿餽餮馥騎髁鬃鬆魏魎魍鯊鯉鯽鯈鯀鵑鵝鵠黠鼕鼬儳嚥壞壟壢寵龐廬懲懷懶懵攀攏曠曝櫥櫝櫚櫓瀛瀟瀨瀚瀝瀕瀘爆爍牘犢獸獺璽瓊瓣疇疆癟癡矇礙禱穫穩簾簿簸簽簷籀繫繭繹繩繪羅繳羶羹羸臘藩藝藪藕藤藥藷蟻蠅蠍蟹蟾襠襟襖襞譁譜識證譚譎譏譆譙贈贊蹼蹲躇蹶蹬蹺蹴轔轎辭邊邋醱醮鏡鏑鏟鏃鏈鏜鏝鏖鏢鏍鏘鏤鏗鏨關隴難霪霧靡韜韻類願顛颼饅饉騖騙鬍鯨鯧鯖鯛鶉鵡鵲鵪鵬麒麗麓麴勸嚨嚷嚶嚴嚼壤孀孃孽寶巉懸懺攘攔攙曦朧櫬瀾瀰瀲爐獻瓏癢癥礦礪礬礫竇競籌籃籍糯糰辮繽繼纂罌耀臚艦藻藹蘑藺蘆蘋蘇蘊蠔蠕襤覺觸議譬警譯譟譫贏贍躉躁躅躂醴釋鐘鐃鏽闡霰飄饒饑馨騫騰騷騵鰓鰍鹹麵黨鼯齟齣齡儷儸囁囀囂夔屬巍懼懾攝攜斕曩櫻欄櫺殲灌爛犧瓖瓔癩矓籐纏續羼蘗蘭蘚蠣蠢蠡蠟襪襬覽譴護譽贓躊躍躋轟辯醺鐮鐳鐵鐺鐸鐲鐫闢霸霹露響顧顥饗驅驃驀騾髏魔魑鰭鰥鶯鶴鷂鶸麝黯鼙齜齦齧儼儻囈囊囉孿巔巒彎懿攤權歡灑灘玀瓤疊癮癬禳籠籟聾聽臟襲襯觼讀贖贗躑躓轡酈鑄鑑鑒霽霾韃韁顫饕驕驍髒鬚鱉鰱鰾鰻鷓鷗鼴齬齪龔囌巖戀攣攫攪曬欐瓚竊籤籣籥纓纖纔臢蘸蘿蠱變邐邏鑣鑠鑤靨顯饜驚驛驗髓體髑鱔鱗鱖鷥麟黴囑壩攬灞癱癲矗罐羈蠶蠹衢讓讒讖艷贛釀鑪靂靈靄韆顰驟鬢魘鱟鷹鷺鹼鹽鼇齷齲廳欖灣籬籮蠻觀躡釁鑲鑰顱饞髖鬣黌灤矚讚鑷韉驢驥纜讜躪釅鑽鑾鑼鱷鱸黷豔鑿鸚爨驪鬱鸛鸞籲啥掰屌噁坨欸飆鵰屄剉嗝糗踪憋崽蜍洨摀胺閹捅潢喵侄搐尻紈殭鋌枷儆鐐璐掐饋跩祂蔘殞柺餬摳菇砒癇氙鈦闇覷佼髪酮鯰浣喏孬撂鴇掮緋"
#define CHINESE_CHARS_2 L"ㄅㄆㄇㄈㄉㄊㄋㄌㄍㄎㄏㄐㄑㄒㄓㄔㄕㄖㄗㄘㄙㄚㄛㄜㄝㄞㄟㄠㄡㄢㄣㄤㄥㄦㄧㄨㄩ乂乜凵匚厂万丌乇亍囗兀屮彳丏冇与丮亓仂仉仈冘勼卬厹圠夃夬尐巿旡殳毌气爿丱丼仨仜仩仡仝仚刌匜卌圢圣夗夯宁宄尒尻屴屳帄庀庂忉戉扐氕氶汃氿氻犮犰玊禸肊阞伎优伬仵伔仱伀价伈伝伂伅伢伓伄仴伒冱刓刉刐劦匢匟卍厊吇囡囟圮圪圴夼妀奼妅奻奾奷奿孖尕尥屼屺屻屾巟幵庄异弚彴忕忔忏扜扞扤扡扦扢扙扠扚扥旯旮朾朹朸朻机朿朼朳氘汆汒汜汏汊汔汋汌灱牞犴犵玎甪癿穵网艸艼芀艽艿虍襾邙邗邘邛邔阢阤阠阣佖伻佢佉体佤伾佧佒佟佁佘伭伳伿佡冏冹刜刞刡劭劮匉卣卲厎厏吰吷吪呔呅吙吜吥吘吽呏呁吨吤呇囮囧囥坁坅坌坉坋坒夆奀妦妘妠妗妎妢妐妏妧妡宎宒尨尪岍岏岈岋岉岒岊岆岓岕巠帊帎庋庉庌庈庍弅弝彸彶忒忑忐忭忨忮忳忡忤忣忺忯忷忻怀忴戺抃抌抎抏抔抇扱扻扺扰抁抈扷扽扲扴攷旰旴旳旲旵杅杇杙杕杌杈杝杍杚杋毐氚汸汧汫沄沋沏汱汯汩沚汭沇沕沜汦汳汥汻沎灴灺牣犿犽狃狆狁犺狅玕玗玓玔玒町甹疔疕皁礽耴肕肙肐肒肜芐芏芅芎芑芓芊芃芄豸迉辿邟邡邥邞邧邠阰阨阯阭丳侘侅佽侀侇佶佴侉侄佷佌侗佪侚佹侁佸侐侜侔侞侒侂侕佫佮冞冼冾刵刲刳剆刱劼匊匋匼厒厔咇呿咁咑咂咈呫呺呾呥呬呴呦咍呯呡呠咘呣呧呤囷囹坯坲坭坫坱坰坶垀坵坻坳坴坢坽夌奅妵妺姏姎妲姌姁妶妼姃姖妱妽姀姈妴姇孢孥宓宕屇岮岤岠岵岯岨岬岟岣岭岢岪岧岝岥岶岰岦帗帔帙弨弢弣弤彔徂彾彽忞忥怭怦怙怲怋怴怊怗怳怚怞怬怢怍怐怮怓怑怌怉怜戔戽抭抴拑抾抪抶拊抮抳抯抻抩抰抸攽斨斻昉旼昄昒昈旻昃昋昍昅旽昑昐曶朊枅杬枎枒杶杻枘枆构杴枍枌杺枟枑枙枃杽极杸杹枔欥殀歾毞氝沓泬泫泮泙沶泔沭泧沷泐泂沺泃泆泭泲泒泝沴沊沝沀泞泀洰泍泇沰泹泏泩泑炔炘炅炓炆炄炑炖炂炚炃牪狖狋狘狉狜狒狔狚狌狑玤玡玭玦玢玠玬玝瓝瓨甿畀甾疌疘皯盳盱盰盵矸矼矹矻矺矷礿秅穸穻竻籵糽耵肏肮肣肸肵肭舠芠苀芫芚芘芛芵芧芮芼芞芺芴芨芡芩苂芤苃芶芢虰虯虭虮豖迒迋迓迍迖迕迗邲邴邯邳邰阹阽阼阺陃俍俅俓侲俉俋俁俔俜俙侻侳俛俇俖侺俀侹俬剄勀勂匽卼厗厖厙厘咺咡咭咥哏哃茍咷咮哖咶哅哆咠呰咼咢咾呲哞咰垵垞垟垤垌垗垝垛垔垘垏垙垥垚垕壴复奓姡姞姮娀姱姝姺姽姼姶姤姲姷姛姩姳姵姠姾姴姭宨峐峘峌峗峋峛峞峚峉峇峊峖峓峔峏峈峆峎峟峸巹帡帢帣帠帤庰庤庢庛庣庥弇弮彖徆怷怹恔恲恞恅恓恇恉恛恌恀恂恟怤恄恘恦恮扂扃拏挍挋拵挎挃拫拹挏挌拸拶挀挓挔拺挕拻拰敁敃斪斿昶昡昲昵昜昦昢昳昫昺昝昴昹昮朏朐柁柲柈枺柜枻柸柘柀枷柅柫柤柟枵柍枳柷柶柮柣柂枹柎柧柰枲柼柆柭柌枮柦柛柺柉柊柃柪柋欨殂殄殶毖毘毠氠氡洴洭洟洼洿洒洊泚洳洄洙洺洚洑洀洝浂洁洘洷洃洏浀洇洠洬洈洢洉洐炷炟炾炱炰炡炴炵炩牁牉牊牬牰牳牮狊狤狨狫狟狪狦狣玅珌珂珈珅玹玶玵玴珫玿珇玾珃珆玸珋瓬瓮甮畇畈疧疪癹盄眈眃眄眅眊盷盻盺矧矨砆砑砒砅砐砏砎砉砃砓祊祌祋祅祄秕种秏秖秎窀穾竑笀笁籺籸籹籿粀粁紃紈紁罘羑羍羾耇耎耏耔耷胘胇胠胑胈胂胐胅胣胙胜胊胕胉胏胗胦胍臿舡芔苙苾苹茇苨茀苕茺苫苖苴苬苡苲苵茌苻苶苰苪苤苠苺苳苭虷虴虼虳衁衎衧衪衩觓訄訇赲迣迡迮迠郱邽邿郕郅邾郇郋郈釔釓陔陏陑陓陊陎倞倅倇倓倢倰倛俵俴倳倷倬俶俷倗倜倠倧倵倯倱倎党冔冓凊凄凅凈凎剡剚剒剞剟剕剢勍匎厞唦哢唗唒哧哳哤唚哿唄唈哫唑唅哱唊哻哷哸哠唎唃唋圁圂埌堲埕埒垺埆垽垼垸垶垿埇埐垹埁夎奊娙娖娭娮娕娏娗娊娞娳宧宭宬尃屖屔峬峿峮峱峷崀峹帩帨庨庮庪庬弳弰彧恝恚恧恁悢悈悀悒悁悝悃悕悛悗悇悜悎戙扆拲挐捖挬捄挶捃揤挹捋捊挼挩捁挴捘捔捙挭捇挳捚捑挸捗捀捈敊敆旆旃旄旂晊晟晇晑朒朓栟栚桉栲栳栻桋桏栖栱栜栵栫栭栯桎桄栴栝栒栔栦栨栮桍栺栥栠欬欯欭欱欴歭肂殈毦毤毨毣毢毧氥浺浤浶洍浡涒浘浢浭浯涑涍淯浿涆浞浧浠涗浰浼浟涂涘洯浨涋浾涀涄洖涃浻浽浵涐烜烓烑烝烋缹烢烗烒烞烠烔烍烅烆烇烚烎烡牂牸牷牶猀狺狴狾狶狳狻猁珓珙珥珖玼珧珣珩珜珒珛珔珝珚珗珘珨瓞瓟瓴瓵甡畛畟疰痁疻痄痀疿疶疺皊盉眝眛眐眓眒眣眑眕眙眚眢眧砣砬砢砵砯砨砮砫砡砩砳砪砱祔祛祏祜祓祒祑秫秬秠秮秭秪秜秞秝窆窉窅窋窌窊窇竘笐笄笓笅笏笈笊笎笉笒粄粑粊粌粈粍粅紞紝紑紎紘紖紓紟紒紏紌罜罡罞罠罝罛羖羒翃翂翀耖耾耹胲胹胵脁胻脀舁舯舥茳茭荄茙荑茥荖茿荁茦茜茢荂荎茛茪茈茼荍茖茤茠茷茯茩荇荅荌荓茞茬荋茧荈虓虒蚢蚨蚖蚍蚑蚞蚇蚗蚆蚋蚚蚅蚥蚙蚡蚧蚕蚘蚎蚝蚐蚔衃衄衭衵衶衲袀衱衿衯袃衾衴衼訒豇豗豻貤貣赶赸趵趷趶軑軓迾迵适迿迻逄迼迶郖郠郙郚郣郟郥郘郛郗郜郤酐酎酏釕釢釚陜陟隼飣髟鬯乿偰偪偡偞偠偓偋偝偲偈偍偁偛偊偢倕偅偟偩偫偣偤偆偀偮偳偗偑凐剫剭剬剮勖勓匭厜啵啶唼啍啐唴唪啑啢唶唵唰啒啅唌唲啎唹啈唭唻啀啋圊圇埻堔埢埶埜埴堀埭埽堈埸堋埳埏堇埮埣埲埥埬埡堎埼堐埧堁堌埱埩埰堍堄奜婠婘婕婧婞娸娵婭婐婟婥婬婓婤婗婃婝婒婄婛婈媎娾婍娹婌婰婩婇婑婖婂婜孲孮寁寀屙崞崋崝崚崠崌崨崍崦崥崏崰崒崣崟崮帾帴庱庴庹庲庳弶弸徛徖徟悊悐悆悾悰悺惓惔惏惤惙惝惈悱惛悷惊悿惃惍惀挲捥掊掂捽掽掞掭掝掗掫掎捯掇据掯捵掜捭捼掤挻掟捸掅掁掑掍捰敓旍晥晡晛晙晜晢朘桹梇梐梜桭桮梮梫楖桯梣梬梩桵桴梲梏桷梒桼桫桲梪梀桱桾梛梖梋梠梉梤桸桻梑梌梊桽欶欳欷殑殏殍殎殌氪淀涫涴涳湴涬淩淢涷淶淔渀淈淠淟淖涾淥淜淝淛淴淊涽淭淰涺淕淂淏淉淐淲淓淽淗淍淣涻烺焍烷焗烴焌烰焄烳焐烼烿焆焓焀烸烶焋焂焎牾牻牼牿猝猗猇猑猘猊猈狿猏猞玈珶珸珵琄琁珽琇琀珺珼珿琌琋珴琈畤畣痎痒痏痋痌痑痐皏皉盓眹眯眭眱眲眴眳眽眥眻眵硈硒硉硍硊硌砦硅硐祤祧祩祪祣祫祡离秺秸秶秷窏窔窐笵筇笴笥笰笢笤笳笘笪笝笱笫笭笯笲笸笚笣粔粘粖粣紵紽紸紶紺絅紬紩絁絇紾紿絊紻紨罣羕羜羝羛翊翋翍翐翑翇翏翉耟耞耛聇聃聈脘脥脙脛脭脟脬脞脡脕脧脝脢舑舸舳舺舴舲艴莐莣莨莍荺荳莤荴莏莁莕莙荵莔莩荽莃莌莝莛莪莋荾莥莯莈莗莰荿莦莇莮荶莚虙虖蚿蚷蛂蛁蛅蚺蚰蛈蚹蚳蚸蛌蚴蚻蚼蛃蚽蚾衒袉袕袨袢袪袚袑袡袟袘袧袙袛袗袤袬袌袓袎覂觖觙觕訰訧訬訞谹谻豜豝豽貥赽赻赹趼跂趹趿跁軘軞軝軜軗軠軡逤逋逑逜逌逡郯郪郰郴郲郳郔郫郬郩酖酘酚酓酕釬釴釱釳釸釤釹釪釫釷釨釮镺閆閈陼陭陫陱陯隿靪頄飥馗傛傕傔傞傋傣傃傌傎傝偨傜傒傂傇兟凔匒匑厤厧喑喨喥喭啷噅喢喓喈喁喣喒喤啽喌喦啿喕喡喎圌堩堷堙堞堧堣堨埵塈堥堜堛堳堿堶堮堹堸堭堬堻奡媯媔媟婺媢媞婸媦婼媥媬媕媮娷媄媊媗媃媋媩婻婽媌媜媏媓媝寪寍寋寔寑寊寎尌尰崷嵃嵫嵁嵋崿崵嵑嵎嵕崳崺嵒崱嵙嵂崹嵉崸崼崲崶嵀嵅幄幁彘徦徥徫惉悹惌惢惎惄愔惲愊愖愅惵愓惸惼惾惁愃愘愝愐惿愄愋扊掔掱揎揥揨揯揃撝揳揊揠揶揕揲揵摡揟掾揝揜揄揘揓揂揇揌揋揈揰揗揙攲敧敪敤敜敨敥斌斝斞斮旐旒晼晬晻暀晱晹晪晲朁椌棓椄棜椪棬棪棱椏棖棷棫棤棶椓椐棳棡椇棌椈楰梴椑棯棆椔棸棐棽棼棨椋椊椗棎棈棝棞棦棴棑椆棔棩椕椥棇欹欻欿欼殔殗殙殕殽毰毲毳氰淼湆湇渟湉溈渼渽湅湢渫渿湁湝湳渜渳湋湀湑渻渃渮湞湨湜湡渱渨湠湱湫渹渢渰湓湥渧湸湤湷湕湹湒湦渵渶湚焠焞焯烻焮焱焣焥焢焲焟焨焺焛牋牚犈犉犆犅犋猒猋猰猢猱猳猧猲猭猦猣猵猌琮琬琰琫琖琚琡琭琱琤琣琝琩琠琲瓻甯畯畬痧痚痡痦痝痟痤痗皕皒盚睆睇睄睍睅睊睎睋睌矞矬硠硤硥硜硭硱硪确硰硩硨硞硢祴祳祲祰稂稊稃稌稄窙竦竤筊笻筄筈筌筎筀筘筅粢粞粨粡絘絯絣絓絖絧絪絏絭絜絫絒絔絩絑絟絎缾缿罥罦羢羠羡翗聑聏聐胾胔腃腊腒腏腇脽腍脺臦臮臷臸臹舄舼舽舿艵茻菏菹萣菀菨萒菧菤菼菶萐菆菈菫菣莿萁菝菥菘菿菡菋菎菖菵菉萉萏菞萑萆菂菳菕菺菑菪萓菃菬菮菄菻菗菢萛菛菾蛘蛢蛦蛓蛣蛚蛪蛝蛫蛜蛬蛩蛗蛨蛑衈衖衕袺裗袹袸裀袾袶袼袷袽袲褁裉覕覘覗觝觚觛詎詍訹詙詀詗詘詄詅詒詈詑詊詌詏豟貁貀貺貾貰貹貵趄趀趉跘跓跍跇跖跜跏跕跙跈跗跅軯軷軺軹軦軮軥軵軧軨軶軫軱軬軴軩逭逴逯鄆鄬鄄郿郼鄈郹郻鄁鄀鄇鄅鄃酡酤酟酢酠鈁鈊鈥鈃鈚鈏鈌鈀鈒釿釽鈆鈄鈧鈂鈜鈤鈙鈗鈅鈖镻閍閌閐隇陾隈隉隃隀雂雈雃雱雰靬靰靮頇颩飫鳦黹亃亄亶傽傿僆傮僄僊傴僈僂傰僁傺傱僋僉傶傸凗剺剸剻剼嗃嗛嗌嗐嗋嗊嗀嗔嗄嗩喿嗒喍嗏嗕嗢嗖嗈嗲嗍嗙嗂圔塓塨塤塏塍塉塯塕塎塝塙塥塛堽塣塱壼嫇嫄嫋媺媸媱媵媰媿嫈媻嫆媷嫀嫊媴媶嫍媹媐寖寘寙尟尳嵱嵣嵊嵥嵲嵬嵞嵨嵧嵢巰幏幎幊幍幋廅廌廆廋廇彀徯徭惷慉慊愫慅愶愲愮慆愯慏愩慀戠酨戣戥戤揅揱揫搐搒搉搠搤搳摃搟搕搘搹搷搢搣搌搦搰搨摁搵搯搊搚摀搥搧搋揧搛搮搡搎敯斒旓暆暌暕暐暋暊暙暔晸朠楦楟椸楎楢楱椿楅楪椹楂楗楙楺楈楉椵楬椳椽楥棰楸椴楩楀楯楄楶楘楁楴楌椻楋椷楜楏楑椲楒椯楻椼歆歅歃歂歈歁殛嗀毻毼毹毷毸溛滖滈溏滀溟溓溔溠溱溹滆滒溽滁溞滉溷溰滍溦滏溲溾滃滜滘溙溒溎溍溤溡溿溳滐滊溗溮溣煇煔煒煣煠煁煝煢煲煸煪煡煂煘煃煋煰煟煐煓煄煍煚牏犍犌犑犐犎猼獂猻猺獀獊獉瑄瑊瑋瑒瑑瑗瑀瑏瑐瑎瑂瑆瑍瑔瓡瓿瓾瓽甝畹畷榃痯瘏瘃痷痾痼痹痸瘐痻痶痭痵痽皙皵盝睕睟睠睒睖睚睩睧睔睙睭矠碇碚碔碏碄碕碅碆碡碃硹碙碀碖硻祼禂祽祹稑稘稙稒稗稕稢稓稛稐窣窢窞竫筦筤筭筴筩筲筥筳筱筰筡筸筶筣粲粴粯綈綆綀綍絿綅絺綎絻綃絼綌綔綄絽綒罭罫罧罨罬羦羥羧翛翜耡腤腠腷腜腩腛腢腲朡腞腶腧腯腄腡舝艉艄艀艂艅蓱萿葖葶葹蒏蒍葥葑葀蒆葧萰葍葽葚葙葴葳葝蔇葞萷萺萴葺葃葸萲葅萩菙葋萯葂萭葟葰萹葎葌葒葯蓅蒎萻葇萶萳葨葾葄萫葠葔葮葐蜋蜄蛷蜌蛺蛖蛵蝍蛸蜎蜉蜁蛶蜅裖裋裍裎裞裛裚裌裐覅覛觟觥觤觡觠觢觜触詶誆詿詡訿詷誂誄詵誃誁詴詺谼豋豊豥豤豦貆貄貅賌赨赩趑趌趎趏趍趓趔趐趒跰跠跬跱跮跐跣跢跧跲跫跴輆軿輁輀輅輇輈輂輋遒逿遄遉逽鄐鄍鄏鄑鄖鄔鄋鄎酯鉈鉒鈰鈺鉦鈳鉥鉞銃鈮鉊鉆鉭鉬鉏鉠鉧鉯鈶鉡鉰鈱鉔鉣鉐鉲鉎鉓鉌鉖鈲閟閜閞閛隒隓隑隗雎雺雽雸雵靳靷靸靲頏頍頎颬飶飹馯馲馰馵骭骫魛鳪鳭鳧麀黽僦僔僗僨僳僛僪僝僤僓僬僰僯僣僠凘劀劁勩勫匰厬嘧嘕嘌嘒嗼嘏嘜嘁嘓嘂嗺嘝嘄嗿嗹墉塼墐墘墆墁塿塴墋塺墇墑墎塶墂墈塻墔墏壾奫嫜嫮嫥嫕嫪嫚嫭嫫嫳嫢嫠嫛嫬嫞嫝嫙嫨嫟孷寠寣屣嶂嶀嵽嶆嵺嶁嵷嶊嶉嶈嵾嵼嶍嵹嵿幘幙幓廘廑廗廎廜廕廙廒廔彄彃彯徶愬愨慁慞慱慳慒慓慲慬憀慴慔慺慛慥愻慪慡慖戩戧戫搫摍摛摝摴摶摲摽摵摦撦摎摞摜摋摓摠摐摿搿摬摫摙摥摷敳斠暡暠暟朅朄朢榱榶槉榠槎榖榰榬榼榑榙榎榧榍榩榾榯榿槄榽榤槔榹槊榚槏榳榓榪榡榞槙榗榐槂榵榥槆歊歍歋殞殟殠毃毄毾滎滵滱漃漥滸漷滻漮漉潎漙漚漧漘漻漒滭漊漶潳滹滮漭潀漰漼漵滫漇漎潃漅滽滶漹漜滼漺漟漍漞漈漡熇熐熉熀熅熂熏煻熆熁熗牄牓犗犕犓獃獍獑獌瑢瑳瑱瑵瑲瑧瑮甀甂甃畽疐瘖瘈瘌瘕瘑瘊瘔皸瞁睼瞅瞂睮瞀睯睾瞃碲碪碴碭碨硾碫碞碥碠碬碢碤禘禊禋禖禕禔禓禗禈禒禐稫穊稰稯稨稦窨窫窬竮箈箜箊箑箐箖箍箌箛箎箅箘劄箙箤箂粻粿粼粺綧綷緂綣綪緁緀緅綝緎緄緆緋緌綯綹綖綼綟綦綮綩綡緉罳翢翣翥翞耤聝聜膉膆膃膇膍膌膋舕蒗蒤蒡蒟蒺蓎蓂蒬蒮蒫蒹蒴蓁蓍蒪蒚蒱蓐蒝蒧蒻蒢蒔蓇蓌蒛蒩蒯蒨蓖蒘蒶蓏蒠蓗蓔蓒蓛蒰蒑虡蜳蜣蜨蝫蝀蜮蜞蜡蜙蜛蝃蜬蝁蜾蝆蜠蜲蜪蜭蜼蜒蜺蜱蜵蝂蜦蜧蜸蜤蜚蜰蜑裷裧裱裲裺裾裮裼裶裻裰裬裫覝覡覟覞觩觫觨誫誙誋誒誏誖谽豨豩賕賏賗趖踉踂跿踍跽踊踃踇踆踅跾踀踄輐輑輎輍鄣鄜鄠鄢鄟鄝鄚鄤鄡鄛酺酲酹酳銥銤鉶銛鉺銠銔銪銍銦銚銫鉹銗鉿銣鋮銎銂銕銢鉽銈銡銊銆銌銙銧鉾銇銩銝銋鈭隞隡雿靘靽靺靾鞃鞀鞂靻鞄鞁靿韎韍頖颭颮餂餀餇馝馜駃馹馻馺駂馽駇骱髣髧鬾鬿魠魡魟鳱鳲鳵麧僿儃儰僸儆儇僶僾儋儌僽儊劋劌勱勯噈噂噌嘵噊噉噆噘噚噀嘳嘽嘬嘾嘸嘪嘺圚墫墝墱墠墣墯墬墥墡壿嫿嫴嫽嫷嫶嬃嫸嬂嫹嬁嬇嬅嬏屧嶙嶗嶟嶒嶢嶓嶕嶠嶜嶡嶚嶞幩幝幠幜緳廛廞廡彉徲憃慹憱憰憢憉憛憓憯憭憟憒憪憡憍慦憳戭摮摰撖撠撅撗撜撏撋撊撌撣撟摨撱撘敶敺敹敻斲斳暵暰暩暲暷暪暯樀樆樗槥槸樕槱槤樠槿槬槢樛樝槾樧槲槮樔槷槧橀樈槦槻樍槼槫樉樄樘樥樏槶樦樇槴樖歑殥殣殢殦氁氀毿氂潁漦潾澇濆澒澍澉澌潏澅潚澖潶潬澂潕潲潒潐潗澔澓潝漀潡潫潽潧澐潓澋潩潿澕潣潷潪潻熲熯熛熰熠熚熩熵熝熥熞熤熡熪熜熧熳犘犚獘獒獞獟獠獝獛獡獚獙獢璇璉璊璆璁瑽璅璈瑼瑹甈甇畾瘥瘞瘙瘝瘜瘣瘚瘨瘛皜皝皞皛瞍瞏瞉瞈磍碻磏磌磑磎磔磈磃磄磉禚禡禠禜禢禛歶稹窲窴窳箷篋箾箬篎箯箹篊箵糅糈糌糋緷緛緪緧緗緡縃緺緦緶緱緰緮緟罶羬羰羭翭翫翪翬翦翨聤聧膣膟膞膕膢膙膗舖艏艓艒艐艎艑蔤蔻蔏蔀蔩蔎蔉蔍蔟蔊蔧蔜蓻蔫蓺蔈蔌蓴蔪蓲蔕蓷蓫蓳蓼蔒蓪蓩蔖蓾蔨蔝蔮蔂蓽蔞蓶蔱蔦蓧蓨蓰蓯蓹蔘蔠蔰蔋蔙蔯虢蝖蝣蝤蝷蟡蝳蝘蝔蝛蝒蝡蝚蝑蝞蝭蝪蝐蝎蝟蝝蝯蝬蝺蝮蝜蝥蝏蝻蝵蝢蝧蝩衚褅褌褔褋褗褘褙褆褖褑褎褉覢覤覣觭觰觬諏諆誸諓諑諔諕誻諗誾諀諅諘諃誺誽諙谾豍貏賥賟賙賨賚賝賧趠趜趡趛踠踣踥踤踮踕踛踖踑踙踦踧踔踒踘踓踜踗踚輬輤輘輚輠輣輖輗遳遰遯遧遫鄯鄫鄩鄪鄲鄦鄮醅醆醊醁醂醄醀鋐鋃鋄鋀鋙銶鋏鋱鋟鋘鋩鋗鋝鋯鋂鋨鋊鋈鋎鋦鋍鋕鋉鋠鋞鋧鋑鋓銵鋡鋆銴镼閬閫閮閰隤隢雓霅霈霂靚鞊鞎鞈韐韏頞頝頦頩頨頠頛頧颲餈飺餑餔餖餗餕駜駍駏駓駔駎駉駖駘駋駗駌骳髬髫髳髲髱魆魃魧魴魱魦魶魵魰魨魤魬鳼鳺鳽鳿鳷鴇鴀鳹鳻鴈鴅鴄麃黓鼏鼐儜儓儗儚儑凞匴叡噰噠噮噳噦噣噭噲噞噷圜圛壈墽壉墿墺壂墼壆嬗嬙嬛嬡嬔嬓嬐嬖嬨嬚嬠嬞寯嶬嶱嶩嶧嶵嶰嶮嶪嶨嶲嶭嶯嶴幧幨幦幯廩廧廦廨廥彋徼憝憨憖懅憴懆懁懌憺憿憸憌擗擖擐擏擉撽撉擃擛擳擙攳敿敼斢曈暾曀曊曋曏暽暻暺曌朣樴橦橉橧樲橨樾橝橭橶橛橑樨橚樻樿橁橪橤橐橏橔橯橩橠樼橞橖橕橍橎橆歕歔歖殧殪殫毈毇氄氃氆澭濋澣濇澼濎濈潞濄澽澞濊澨瀄澥澮澺澬澪濏澿澸澢濉澫濍澯澲澰燅燂熿熸燖燀燁燋燔燊燇燏熽燘熼燆燚燛犝犞獩獦獧獬獥獫獪瑿璚璠璔璒璕璡甋疀瘯瘭瘱瘽瘳瘼瘵瘲瘰皻盦瞚瞝瞡瞜瞛瞢瞣瞕瞙瞗磝磩磥磪磞磣磛磡磢磭磟磠禤穄穈穇窶窸窵窱窷篞篣篧篝篕篥篚篨篹篔篪篢篜篫篘篟糒糔糐糑縒縡縗縌縟縠縓縎縜縕縚縢縋縏縖縍縔縥縤罃罻罼罺羱翯耪耩聬膱膦膮膹膵膫膰膬膴膲膷膧臲艕艖艗蕖蕅蕫蕍蕓蕡蕘蕀蕆蕤蕁蕢蕄蕑蕇蕣蔾蕛蕱蕎蕮蕵蕕蕧蕠薌蕦蕝蕔蕥蕬虣虥虤螛螏螗螓螒螈螁螖螘蝹螇螣螅螐螑螝螄螔螜螚螉褞褦褰褭褮褧褱褢褩褣褯褬褟觱諠諢諲諴諵諝謔諤諟諰諈諞諡諨諿諯諻貑貒貐賵賮賱賰賳赬赮趥趧踳踾踸蹀蹅踶踼踽蹁踰踿躽輶輮輵輲輹輷輴遶遹遻邆郺鄳鄵鄶醓醐醑醍醏錧錞錈錟錆錏鍺錸錼錛錣錒錁鍆錭錎錍鋋錝鋺錥錓鋹鋷錴錂錤鋿錩錹錵錪錔錌錋鋾錉錀鋻錖閼闍閾閺閶閿閵閽隩雔霋霒霐鞙鞗鞔韰韸頵頯頲餤餟餧餩馞駮駬駥駤駰駣駪駩駧骹骿骴骻髶髺髹髷鬳鮀鮅鮇魼魾魻鮂鮓鮒鮐魺鮕魽鮈鴥鴗鴠鴞鴔鴩鴝鴘鴢鴐鴙鴟麈麆麇麮麭黕黖黺鼒鼽儦儥儢儤儠儩勴嚓嚌嚍嚆嚄嚃噾嚂噿嚁壖壔壏壒嬭嬥嬲嬣嬬嬧嬦嬯嬮孻寱寲嶷幬幪徾徻懃憵憼懧懠懥懤懨懞擯擩擣擫擤擨斁斀斶旚曒檍檖檁檥檉檟檛檡檞檇檓檎檕檃檨檤檑橿檦檚檅檌檒歛殭氉濌澩濴濔濣濜濭濧濦濞濲濝濢濨燡燱燨燲燤燰燢獳獮獯璗璲璫璪璭璱璥璯甐甑甒甏疄癃癈癉皤盩瞵瞫瞲瞷瞶瞴瞱瞨矰磳磽礂磻磼磲礅磹磾礄禫禨穜穛穖穘穔穚窾竀竁簅簏篲簀篿篻簎篴簋篳簂簉簃簁篸篽簆篰篱簐簊糨縭縼繂縳顈縸縪繉繀繇縩繌縰縻縶繄縺罅罿罾罽翴翲耬膻臄臌臊臅臇膼臩艛艚艜薃薀薏薧薕薠薋薣蕻薤薚薞蕷蕼薉薡蕺蕸蕗薎薖薆薍薙薝薁薢薂薈薅蕹蕶薘薐薟虨螾螪螭蟅螰螬螹螵螼螮蟉蟃蟂蟌螷螯蟄蟊螴螶螿螸螽蟞螲褵褳褼褾襁襒褷襂覭覯覮觲觳謞謘謖謑謅謋謢謏謒謕謇謍謈謆謜謓謚豏豰豲豱豯貕貔賹赯蹎蹍蹓蹐蹌蹇轃轀邅遾鄸醚醢醛醙醟醡醝醠鎡鎃鎯鍤鍖鍇鍼鍘鍜鍶鍉鍐鍑鍠鍭鎏鍌鍪鍹鍗鍕鍒鍏鍱鍷鍻鍡鍞鍣鍧鎀鍎鍙闀闉闃闅閷隮隰隬霠霟霘霝霙鞚鞡鞜鞞鞝韕韔韱顁顄顊顉顅顃餥餫餪餳餲餯餭餱餰馘馣馡騂駺駴駷駹駸駶駻駽駾駼騃骾髾髽鬁髼魈鮚鮨鮞鮛鮦鮡鮥鮤鮆鮢鮠鮯鴳鵁鵧鴶鴮鴯鴱鴸鴰鵅鵂鵃鴾鴷鵀鴽翵鴭麊麉麍麰黈黚黻黿鼤鼣鼢齔龠儱儭儮嚘嚜嚗嚚嚝嚙奰嬼屩屪巀幭幮懘懟懭懮懱懪懰懫懖懩擿攄擽擸攁攃擼斔旛曚曛曘櫅檹檽櫡櫆檺檶檷櫇檴檭歞毉氋瀇瀌瀍瀁瀅瀔瀎濿瀀濻瀦濼濷瀊爁燿燹爃燽獶璸瓀璵瓁璾璶璻瓂甔甓癜癤癙癐癓癗癚皦皽盬矂瞺磿礌礓礔礉礐礒礑禭禬穟簜簩簙簠簟簭簝簦簨簢簥簰繜繐繖繣繘繢繟繑繠繗繓羵羳翷翸聵臑臒臐艟艞薴藆藀藃藂薳薵薽藇藄薿藋藎藈藅薱薶藒蘤薸薷薾虩蟧蟦蟢蟛蟫蟪蟥蟟蟳蟤蟔蟜蟓蟭蟘蟣螤蟗蟙蠁蟴蟨蟝襓襋襏襌襆襐襑襉謪謧謣謳謰謵譇謯謼謾謱謥謷謦謶謮謤謻謽謺豂豵貙貘貗賾贄贂贀蹜蹢蹠蹗蹖蹞蹥蹧蹛蹚蹡蹝蹩蹔轆轇轈轋鄨鄺鄻鄾醨醥醧醯醪鎵鎌鎒鎷鎛鎝鎉鎧鎎鎪鎞鎦鎕鎈鎙鎟鎍鎱鎑鎲鎤鎨鎴鎣鎥闒闓闑隳雗雚巂雟雘雝霣霢霥鞬鞮鞨鞫鞤鞪鞢鞥韗韙韖韘韺顐顑顒颸饁餼餺騏騋騉騍騄騑騊騅騇騆髀髜鬈鬄鬅鬩鬵魊魌魋鯇鯆鯃鮿鯁鮵鮸鯓鮶鯄鮹鮽鵜鵓鵏鵊鵛鵋鵙鵖鵌鵗鵒鵔鵟鵘鵚麎麌黟鼁鼀鼖鼥鼫鼪鼩鼨齌齕儴儵劖勷厴嚫嚭嚦嚧嚪嚬壚壝壛夒嬽嬾嬿巃幰徿懻攇攐攍攉攌攎斄旞旝曞櫧櫠櫌櫑櫙櫋櫟櫜櫐櫫櫏櫍櫞歠殰氌瀙瀧瀠瀖瀫瀡瀢瀣瀩瀗瀤瀜瀪爌爊爇爂爅犥犦犤犣犡瓋瓅璷瓃甖癠矉矊矄矱礝礛礡礜礗礞禰穧穨簳簼簹簬簻糬糪繶繵繸繰繷繯繺繲繴繨罋罊羃羆羷翽翾聸臗臕艤艡艣藫藱藭藙藡藨藚藗藬藲藸藘藟藣藜藑藰藦藯藞藢蠀蟺蠃蟶蟷蠉蠌蠋蠆蟼蠈蟿蠊蠂襢襚襛襗襡襜襘襝襙覈覷覶觶譐譈譊譀譓譖譔譋譕譑譂譒譗豃豷豶貚贆贇贉趬趪趭趫蹭蹸蹳蹪蹯蹻軂轒轑轏轐轓辴酀鄿醰醭鏞鏇鏏鏂鏚鏐鏹鏬鏌鏙鎩鏦鏊鏔鏮鏣鏕鏄鏎鏀鏒鏧镽闚闛雡霩霫霬霨霦鞳鞷鞶韝韞韟顜顙顝顗颿颽颻颾饈饇饃馦馧騚騕騥騝騤騛騢騠騧騣騞騜騔髂鬋鬊鬎鬌鬷鯪鯫鯠鯞鯤鯦鯢鯔鯗鯬鯜鯙鯥鯕鯡鯚鵷鶁鶊鶄鶈鵱鶀鵸鶆鶋鶌鵽鵫鵴鵵鵩鶅鵳鵻鶂鵯鵹鵿鶇鵨麔麑黀黼鼭齀齁齍齖齗齘匷嚲嚵嚳壣孅巆巇廮廯忀忁懹攗攖攕攓旟曨曣曤櫳櫰櫪櫨櫹櫱櫮櫯瀼瀵瀯瀷瀴瀱灂瀸瀿瀺瀹灀瀻瀳灁爓爔犨獽獼璺皫皪皾盭矌矎矏矍矲礥礣礧礨礤礩禲穮穬穭竷籉籈籊籇籅糮繻繾纁纀羺翿聹臛臙舋艨艩蘢藿蘁藾蘛蘀藶蘄蘉蘅蘌藽蠙蠐蠑蠗蠓蠖襣襦覹觷譠譪譝譨譣譥譧譭趮躆躈躄轙轖轗轕轘轚邍酃酁醷醵醲醳鐋鐓鏻鐠鐏鐔鏾鐕鐐鐨鐙鐍鏵鐀鏷鐇鐎鐖鐒鏺鐉鏸鐊鏿鏼鐌鏶鐑鐆闞闠闟霮霯鞹鞻韽韾顠顢顣顟飁飂饐饎饙饌饓騲騴騱騬騪騶騩騮騸騭髇髊髆鬐鬒鬑鰋鰈鯷鰅鰒鯸鱀鰇鰎鰆鰗鰔鰉鶟鶙鶤鶝鶒鶘鶐鶛鶠鶔鶜鶪鶗鶡鶚鶢鶨鶞鶣鶿鶩鶖鶦鶧麙麛麚黥黤黧黦鼰鼮齛齠齞齝齙龑儺儹劘劗囃嚽嚾孈孇巋巏廱懽攛欂櫼欃櫸欀灃灄灊灈灉灅灆爝爚爙獾甗癪矐礭礱礯籔籓糲纊纇纈纋纆纍罍羻耰臝蘘蘪蘦蘟蘣蘜蘙蘧蘮蘡蘠蘩蘞蘥蠩蠝蠛蠠蠤蠜蠫衊襭襩襮襫觺譹譸譅譺譻贐贔趯躎躌轞轛轝酆酄酅醹鐿鐻鐶鐩鐽鐼鐰鐹鐪鐷鐬鑀鐱闥闤闣霵霺鞿韡顤飉飀饘饖騹騽驆驄驂驁騺騿髍鬕鬗鬘鬖鬺魒鰫鰝鰜鰬鰣鰨鰩鰤鰡鶷鶶鶼鷁鷇鷊鷏鶾鷅鷃鶻鶵鷎鶹鶺鶬鷈鶱鶭鷌鶳鷍鶲鹺麜黫黮黭鼛鼘鼚鼱齎齥齤龒亹囆囅囋奱孋孌巕巑廲攡攠攦攢欋欈欉氍灕灖灗灒爞爟犩獿瓘瓕瓙瓗癭皭礵禴穰穱籗籜籙籛籚糴糱纑罏羇臞艫蘴蘵蘳蘬蘲蘶蠬蠨蠦蠪蠥襱覿覾觻譾讄讂讆讅譿贕躕躔躚躒躐躖躗轠轢酇鑌鑐鑊鑋鑏鑇鑅鑈鑉鑆霿韣顪顩飋饔饛驎驓驔驌驏驈驊驉驒驐髐鬙鬫鬻魖魕鱆鱈鰿鱄鰹鰳鱁鰼鰷鰴鰲鰽鰶鷛鷒鷞鷚鷋鷐鷜鷑鷟鷩鷙鷘鷖鷵鷕鷝麶黰鼵鼳鼲齂齫龕龢儽劙壨壧奲孍巘蠯彏戁戃戄攩攥斖曫欑欒欏毊灛灚爢玂玁玃癰矔籧籦纕艬蘺虀蘹蘼蘱蘻蘾蠰蠲蠮蠳襶襴襳觾讌讎讋讈豅贙躘轤轣醼鑢鑕鑝鑗鑞韄韅頀驖驙鬞鬟鬠鱒鱘鱐鱊鱍鱋鱕鱙鱌鱎鷻鷷鷯鷣鷫鷸鷤鷶鷡鷮鷦鷲鷰鷢鷬鷴鷳鷨鷭黂黐黲黳鼆鼜鼸鼷鼶齃齏齱齰齮齯囓囍孎屭攭曭曮欓灟灡灝灠爣瓛瓥矕礸禷禶籪纗羉艭虃蠸蠷蠵衋讔讕躞躟躠躝醾醽釂鑫鑨鑩雥靆靃靇韇韥驞髕魙鱣鱧鱦鱢鱞鱠鸂鷾鸇鸃鸆鸅鸀鸁鸉鷿鷽鸄麠鼞齆齴齵齶囔攮斸欘欙欗欚灢爦犪矘矙礹籩籫糶纚纘纛纙臠臡虆虇虈襹襺襼襻觿讘讙躥躤躣鑮鑭鑯鑱鑳靉顲饟鱨鱮鱭鸋鸍鸐鸏鸒鸑麡黵鼉齇齸齻齺齹圞灦籯蠼趲躦釃鑴鑸鑶鑵驠鱴鱳鱱鱵鸔鸓黶鼊龤灨灥糷虪蠾蠽蠿讞貜躩軉靋顳顴飌饡馫驤驦驧鬤鸕鸗齈戇欞爧虌躨钂钀钁驩驨鬮鸙爩虋讟钃鱹麷癵驫鱺鸝灩灪麤齾齉龘碁銹裏墻恒粧嫺"
#define JAPANESE_CHARS L"亜哀挨愛曖悪握圧扱宛嵐安案暗以衣位囲医依委威為畏胃尉異移萎偉椅彙意違維慰遺緯域育一壱逸茨芋引印因咽姻員院淫陰飲隠韻右宇羽雨唄鬱畝浦運雲永泳英映栄営詠影鋭衛易疫益液駅悦越謁閲円延沿炎怨宴媛援園煙猿遠鉛塩演縁艶汚王凹央応往押旺欧殴桜翁奥横岡屋億憶臆虞乙俺卸音恩温穏下化火加可仮何花佳価果河苛科架夏家荷華菓貨渦過嫁暇禍靴寡歌箇稼課蚊牙瓦我画芽賀雅餓介回灰会快戒改怪拐悔海界皆械絵開階塊楷解潰壊懐諧貝外劾害崖涯街慨蓋該概骸垣柿各角拡革格核殻郭覚較隔閣確獲嚇穫学岳楽額顎掛潟括活喝渇割葛滑褐轄且株釜鎌刈干刊甘汗缶完肝官冠巻看陥乾勘患貫寒喚堪換敢棺款間閑勧寛幹感漢慣管関歓監緩憾還館環簡観韓艦鑑丸含岸岩玩眼頑顔願企伎危机気岐希忌汽奇祈季紀軌既記起飢鬼帰基寄規亀喜幾揮期棋貴棄毀旗器畿輝機騎技宜偽欺義疑儀戯擬犠議菊吉喫詰却客脚逆虐九久及弓丘旧休吸朽臼求究泣急級糾宮救球給嗅窮牛去巨居拒拠挙虚許距魚御漁凶共叫狂京享供協況峡挟狭恐恭胸脅強教郷境橋矯鏡競響驚仰暁業凝曲局極玉巾斤均近金菌勤琴筋僅禁緊錦謹襟吟銀区句苦駆具惧愚空偶遇隅串屈掘窟熊繰君訓勲薫軍郡群兄刑形系径茎係型契計恵啓掲渓経蛍敬景軽傾携継詣慶憬稽憩警鶏芸迎鯨隙劇撃激桁欠穴血決結傑潔月犬件見券肩建研県倹兼剣拳軒健険圏堅検嫌献絹遣権憲賢謙鍵繭顕験懸元幻玄言弦限原現舷減源厳己戸古呼固股虎孤弧故枯個庫湖雇誇鼓錮顧五互午呉後娯悟碁語誤護口工公勾孔功巧広甲交光向后好江考行坑孝抗攻更効幸拘肯侯厚恒洪皇紅荒郊香候校耕航貢降高康控梗黄喉慌港硬絞項溝鉱構綱酵稿興衡鋼講購乞号合拷剛傲豪克告谷刻国黒穀酷獄骨駒込頃今困昆恨根婚混痕紺魂墾懇左佐沙査砂唆差詐鎖座挫才再災妻采砕宰栽彩採済祭斎細菜最裁債催塞歳載際埼在材剤財罪崎作削昨柵索策酢搾錯咲冊札刷刹拶殺察撮擦雑皿三山参桟蚕惨産傘散算酸賛残斬暫士子支止氏仕史司四市矢旨死糸至伺志私使刺始姉枝祉肢姿思指施師恣紙脂視紫詞歯嗣試詩資飼誌雌摯賜諮示字寺次耳自似児事侍治持時滋慈辞磁餌璽鹿式識軸七叱失室疾執湿嫉漆質実芝写社車舎者射捨赦斜煮遮謝邪蛇尺借酌釈爵若弱寂手主守朱取狩首殊珠酒腫種趣寿受呪授需儒樹収囚州舟秀周宗拾秋臭修袖終羞習週就衆集愁酬醜蹴襲十汁充住柔重従渋銃獣縦叔祝宿淑粛縮塾熟出述術俊春瞬旬巡盾准殉純循順準潤遵処初所書庶暑署緒諸女如助序叙徐除小升少召匠床抄肖尚招承昇松沼昭宵将消症祥称笑唱商渉章紹訟勝掌晶焼焦硝粧詔証象傷奨照詳彰障憧衝賞償礁鐘上丈冗条状乗城浄剰常情場畳蒸縄壌嬢錠譲醸色拭食植殖飾触嘱織職辱尻心申伸臣芯身辛侵信津神唇娠振浸真針深紳進森診寝慎新審震薪親人刃仁尽迅甚陣尋腎須図水吹垂炊帥粋衰推酔遂睡穂随髄枢崇数据杉裾寸瀬是井世正生成西声制姓征性青斉政星牲省凄逝清盛婿晴勢聖誠精製誓静請整醒税夕斥石赤昔析席脊隻惜戚責跡積績籍切折拙窃接設雪摂節説舌絶千川仙占先宣専泉浅洗染扇栓旋船戦煎羨腺詮践箋銭潜線遷選薦繊鮮全前善然禅漸膳繕狙阻祖租素措粗組疎訴塑遡礎双壮早争走奏相荘草送倉捜挿桑巣掃曹曽爽窓創喪痩葬装僧想層総遭槽踪操燥霜騒藻造像増憎蔵贈臓即束足促則息捉速側測俗族属賊続卒率存村孫尊損遜他多汰打妥唾堕惰駄太対体耐待怠胎退帯泰堆袋逮替貸隊滞態戴大代台第題滝宅択沢卓拓託濯諾濁但達脱奪棚誰丹旦担単炭胆探淡短嘆端綻誕鍛団男段断弾暖談壇地池知値恥致遅痴稚置緻竹畜逐蓄築秩窒茶着嫡中仲虫沖宙忠抽注昼柱衷酎鋳駐著貯丁弔庁兆町長挑帳張彫眺釣頂鳥朝貼超腸跳徴嘲潮澄調聴懲直勅捗沈珍朕陳賃鎮追椎墜通痛塚漬坪爪鶴低呈廷弟定底抵邸亭貞帝訂庭逓停偵堤提程艇締諦泥的笛摘滴適敵溺迭哲鉄徹撤天典店点展添転填田伝殿電斗吐妬徒途都渡塗賭土奴努度怒刀冬灯当投豆東到逃倒凍唐島桃討透党悼盗陶塔搭棟湯痘登答等筒統稲踏糖頭謄藤闘騰同洞胴動堂童道働銅導瞳峠匿特得督徳篤毒独読栃凸突届屯豚頓貪鈍曇丼那奈内梨謎鍋南軟難二尼弐匂肉虹日入乳尿任妊忍認寧熱年念捻粘燃悩納能脳農濃把波派破覇馬婆罵拝杯背肺俳配排敗廃輩売倍梅培陪媒買賠白伯拍泊迫剥舶博薄麦漠縛爆箱箸畑肌八鉢発髪伐抜罰閥反半氾犯帆汎伴判坂阪板版班畔般販斑飯搬煩頒範繁藩晩番蛮盤比皮妃否批彼披肥非卑飛疲秘被悲扉費碑罷避尾眉美備微鼻膝肘匹必泌筆姫百氷表俵票評漂標苗秒病描猫品浜貧賓頻敏瓶不夫父付布扶府怖阜附訃負赴浮婦符富普腐敷膚賦譜侮武部舞封風伏服副幅復福腹複覆払沸仏物粉紛雰噴墳憤奮分文聞丙平兵併並柄陛閉塀幣弊蔽餅米壁璧癖別蔑片辺返変偏遍編弁便勉歩保哺捕補舗母募墓慕暮簿方包芳邦奉宝抱放法泡胞俸倣峰砲崩訪報蜂豊飽褒縫亡乏忙坊妨忘防房肪某冒剖紡望傍帽棒貿貌暴膨謀頬北木朴牧睦僕墨撲没勃堀本奔翻凡盆麻摩磨魔毎妹枚昧埋幕膜枕又末抹万満慢漫未味魅岬密蜜脈妙民眠矛務無夢霧娘名命明迷冥盟銘鳴滅免面綿麺茂模毛妄盲耗猛網目黙門紋問冶夜野弥厄役約訳薬躍闇由油喩愉諭輸癒唯友有勇幽悠郵湧猶裕遊雄誘憂融優与予余誉預幼用羊妖洋要容庸揚揺葉陽溶腰様瘍踊窯養擁謡曜抑沃浴欲翌翼拉裸羅来雷頼絡落酪辣乱卵覧濫藍欄吏利里理痢裏履璃離陸立律慄略柳流留竜粒隆硫侶旅虜慮了両良料涼猟陵量僚領寮療瞭糧力緑林厘倫輪隣臨瑠涙累塁類令礼冷励戻例鈴零霊隷齢麗暦歴列劣烈裂恋連廉練錬呂炉賂路露老労弄郎朗浪廊楼漏籠六録麓論和話賄脇惑枠湾腕々ヶ唖阿逢葵茜旭芦梓斡姐飴綾鮎或粟鞍杏伊惟謂亥郁磯溢鰯允蔭烏迂卯鵜窺碓嘘欝鰻厩瓜閏噂云曳洩瑛厭堰焔苑於甥荻桶牡伽嘉珂禾茄嘩迦霞俄臥蛾駕廻魁晦芥凱咳鎧蛙笠樫恰鰹叶鞄兜噛鴨茅粥姦柑桓澗潅竿巌癌贋雁嬉毅稀徽蟻鞠吃杵仇汲灸渠鋸亨侠卿怯饗桐欽禽芹衿倶狗玖躯駈喰寓屑窪隈栗卦袈圭慧桂繋罫荊頚戟訣倦喧捲牽鹸絃諺姑狐糊袴胡跨伍吾檎瑚醐鯉垢宏巷弘昂晃杭浩腔膏劫壕濠轟甑惚狛此坤昏梱艮些叉嵯裟坐砦冴堺榊肴朔窄鮭笹匙薩鯖捌錆鮫晒撒燦珊讃餐仔屍孜獅爾痔而蒔汐竺雫篠偲柴縞紗勺灼惹綬洲繍讐什戎峻竣舜駿楯淳醇曙渚藷哨妾娼庄廠昌梢裳醤鍾鞘擾杖穣燭蝕疹秦塵壬訊靭笥厨翠錐錘瑞雛菅雀摺棲栖脆蹟蝉尖煽穿舛賎閃噌曾楚疏蘇鼠叢惣掻槍漕糟綜聡蒼其揃詑舵楕陀腿苔鯛醍鷹啄托琢鐸茸凧蛸只叩辰巽竪辿狸樽坦湛箪耽蛋檀弛智蜘馳筑註猪凋喋寵帖暢脹蝶諜槌鎚掴槻柘辻綴鍔椿壷吊剃悌挺梯碇蹄釘鏑轍纏甜顛澱兎堵屠杜鍍砥塘套宕嶋梼淘涛燈祷蕩鐙撞涜禿寅酉敦沌遁呑乍凪薙灘楢馴畷楠汝迩賑韮濡祢葱撚乃之埜膿覗蚤巴播杷琶芭盃牌楳煤狽這蝿秤萩柏箔粕曝莫函硲肇筈幡鳩噺蛤隼挽磐庇斐緋誹樋毘琵柊稗疋髭彦菱紐謬瓢豹錨鋲蛭彬瀕埠冨斧芙撫蕪楓淵弗鮒吻扮焚糞頁僻碧瞥篇娩鞭鋪甫輔戊菩呆捧朋烹萌鳳鉾吠殆幌槙鮪柾亦俣沫迄麿蔓巳箕湊蓑稔粍姪牝蒙儲勿尤貰悶也爺耶靖佑柚祐傭楊熔遥淀螺莱洛李掠溜琉龍凌梁稜諒遼淋燐鱗怜玲憐煉蓮牢狼篭蝋禄肋倭歪鷲詫藁碗戮榴瞑絆柩あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをんがぎぐげござじずぜぞだぢづでどばびぶべぼぱぴぷぺぽぁぃぅぇぉゃゅょっアイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワヲンヴガギグゲゴザジズゼゾダヂヅデドバビブベボパピプペポァィゥェォャュョッ１２３４５６７８９０！？丑丞乎亘亙亮佃伶侃侑俠俐俱倖凜凛凰劉勁匁匡廿卜叡吞哉哩喬嘗圃尭堯埴塙夷奄奎娃姥孟宋宥峨崚嵩嶺已庚庵廟彗彪徠忽恢恕悉惇惺按掬捷捺摑撰擢斯昊昴晏晄晋晟晨暉枇桧檜栞桔梧梛梶椛椋椀椰榎樺榛槇樟橘橙櫂櫛櫓欣歎毬汀沓洸洵浬渚渥湘滉漱漣澪焰煌熙燕燎燿牒牟犀猪珈珀琢琥琳瑶瑳畠畢皐皓眸矩砧硯碩祇禰祐禱祿禎禎稟穰穹笈笙簞簾籾紘紬絢綺綸繫繡纂羚翔耀胤脩苺茉莞莉菫菖萄萠萊葦萱葺董葡蒐蒲蓉蔣蔦蓬蕎蕨蕉蕃蕾蕗蘭蝦蟬蟹蠟裡襖詢誼諏諄赳輯輿迪逞逗遙邑祁鄭醬釉釧銑鋒錫鍬閤頌頗顚颯馨驍魯鱒鳶鴻鵬鷗鷺麒麟黎黛鼎亞惡爲逸榮衞謁圓緣薗應櫻奧橫溫價禍悔海壞懷樂渴卷陷寬漢氣祈器僞戲虛峽狹響曉勤謹勳薰惠揭鷄藝擊縣儉劍險圈檢顯驗嚴廣恆黃國黑穀碎雜祉視兒濕實社者煮壽收臭從澁獸縱祝暑署緖諸敍將祥涉燒奬條狀乘淨剩疊孃讓釀神眞寢愼盡粹醉穗瀨齊靜攝節專戰纖禪祖壯爭莊搜巢裝僧層瘦騷增憎藏贈臟卽帶滯瀧單嘆團彈晝鑄著廳徵聽懲鎭轉傳都盜稻德突難拜賣梅髮拔繁晚卑祕碑賓敏侮福拂佛勉步峯墨飜每萬默彌藥與搖樣謠來賴覽欄虜凉綠淚壘類禮曆歷練鍊郞朗廊錄巖ー"

#define ASIAN_CHARS KOREAN_CHARS CHINESE_CHARS_1 CHINESE_CHARS_2 JAPANESE_CHARS
#define GAMERTAG_CHARS NUMERIC_CHARS ENGLISH_CHARS EXTENDED_CHARS ASIAN_CHARS
#define FILENAME_CHARS L"ABCDEFGHIKJLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" NUMERIC_CHARS L"_ ()&[]{}@'#£$%"

	m_ValidCharacters.clear();
	int iCount = 0;

	wchar_t const * const c_charactersToCheck = m_eTextType == ioVirtualKeyboard::kTextType_FILENAME ? FILENAME_CHARS : 
		m_eTextType == ioVirtualKeyboard::kTextType_GAMERTAG ? GAMERTAG_CHARS :
		m_eTextType == ioVirtualKeyboard::kTextType_NUMERIC ? NUMERIC_CHARS : NULL;

	while( c_charactersToCheck && c_charactersToCheck[iCount] != '\0' )
	{
		m_ValidCharacters.PushAndGrow( c_charactersToCheck[iCount] );
		iCount++;
	}
}

bool CTextInputBox::CheckCharacterValidity(rage::char16 c)
{
	switch(m_eTextType)
	{
	case ioVirtualKeyboard::kTextType_NUMERIC:
	case ioVirtualKeyboard::kTextType_GAMERTAG:
		return IsCharacterValid(c);
	case ioVirtualKeyboard::kTextType_FILENAME:
	case ioVirtualKeyboard::kTextType_INVALID:
	case ioVirtualKeyboard::kTextType_DEFAULT:
	case ioVirtualKeyboard::kTextType_EMAIL:
	case ioVirtualKeyboard::kTextType_PASSWORD:
	case ioVirtualKeyboard::kTextType_ALPHABET:
		return !IsCharacterInvalid(c);
	default:
		return true;
	}
}

bool CTextInputBox::IsCharacterValid(rage::char16 c)
{
	for(int i = 0; i < m_ValidCharacters.size(); ++i)
	{
		if(m_ValidCharacters[i] == c)
		{
			return true;
		}
	}

	return false;
}

bool CTextInputBox::IsCharacterInvalid(rage::char16 c)
{
	if(!CTextFormat::IsValidDisplayCharacter(c))
		return true;

	for(int i = 0; i < m_InvalidCharacters.size(); ++i)
	{
		if(m_InvalidCharacters[i] == c)
		{
			return true;
		}
	}

	return false;
}

void CTextInputBox::ShiftArrayBack(int iIndex)
{
	for(int i = iIndex; i < m_uMaxLength; ++i)
	{
		m_Text[i] = m_Text[i+1];
	}
}

void CTextInputBox::ShiftArrayForward(int iIndex)
{
	for(int i = m_uMaxLength; i > iIndex; --i)
	{
		m_Text[i] = m_Text[i-1];
	}
}

int CTextInputBox::StringLengthChar16(char16* str)
{
	int iCount = 0;
	while(str[iCount] != '\0')
	{
		iCount++;
	}

	return iCount;
}

void CTextInputBox::OnSignOut()
{
	if(STextInputBox::GetInstance().IsActive())
	{
		STextInputBox::GetInstance().Close();
		STextInputBox::GetInstance().DestroyState();
	}
}

#if __BANK
void CTextInputBox::InitWidgets()
{
	bkBank *pBank = BANKMGR.FindBank(UI_DEBUG_BANK_NAME);
	if (!pBank)  // create the bank if not found
	{
		pBank = &BANKMGR.CreateBank(UI_DEBUG_BANK_NAME);
	}

	if (pBank)
	{
		pBank->AddButton("Create Text Input Box widgets", &CTextInputBox::CreateBankWidgets);
	}
}

void CTextInputBox::CreateBankWidgets()
{
	bkBank *pBank = BANKMGR.FindBank(UI_DEBUG_BANK_NAME);
	if(pBank)
	{
		pBank->PushGroup("Text Input Box");
		{
			pBank->AddText("Box Title", &s_pszDebugTitle[0], DEBUG_STRING_SIZE);
			pBank->AddText("Box Description", &s_pszDebugDescription[0], DEBUG_STRING_SIZE);
			pBank->AddText("Box Default Input", &s_pszDebugDefaultInput[0], DEBUG_STRING_SIZE);
			pBank->AddButton("Open Text Input Box", &CTextInputBox::DebugOpen);
			pBank->AddButton("Close Text Input Box", &CTextInputBox::DebugClose);
			pBank->AddText("TextInputBoxState: ", &s_iInputBoxState);
		}
		pBank->PopGroup();
	}
}

void CTextInputBox::DebugOpen()
{
	ioVirtualKeyboard::Params params;

	if(s_pszDebugTitle != NULL)
		Utf8ToWide(s_pszDebugTitle16, s_pszDebugTitle, DEBUG_STRING_SIZE);
	if(s_pszDebugDescription != NULL)
		Utf8ToWide(s_pszDebugDescription16, s_pszDebugDescription, DEBUG_STRING_SIZE);
	if(s_pszDebugDefaultInput != NULL)
		Utf8ToWide(s_pszDebugDefaultInput16, s_pszDebugDefaultInput, DEBUG_STRING_SIZE);

	params.m_Title = s_pszDebugTitle16;
	params.m_Description = s_pszDebugDescription16;
	params.m_InitialValue = s_pszDebugDefaultInput16;

	STextInputBox::GetInstance().Open(params);
}

void CTextInputBox::DebugClose()
{
	STextInputBox::GetInstance().Close();
}
#endif //__BANK
#endif // RSG_PC