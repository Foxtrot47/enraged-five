<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<structdef type="CWaitingForProcessPage" base="CCategorylessPageBase" constructible="false">
		<pointer name="m_onCompleteAction" parName="OnCompleteAction" type="uiPageDeckMessageBase" policy="owner" />
	</structdef>
  
</ParserSchema>
