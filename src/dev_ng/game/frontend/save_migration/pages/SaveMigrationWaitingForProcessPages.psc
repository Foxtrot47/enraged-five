<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<structdef type="CFetchingMultiplayerAccountPage" base="CWaitingForProcessPage">
	</structdef>

	<structdef type="CProcessingMultiplayerAccountMigrationPage" base="CWaitingForProcessPage">
	</structdef>

	<structdef type="CLoadingLocalSingleplayerSavesPage" base="CWaitingForProcessPage">
	</structdef>

	<structdef type="CFetchingSingleplayerSavesPage" base="CWaitingForProcessPage">
	</structdef>

	<structdef type="CDownloadingSingleplayerSavePage" base="CWaitingForProcessPage">
	</structdef>

	<structdef type="CCompletingSingleplayerMigrationPage" base="CWaitingForProcessPage">
	</structdef>
  
</ParserSchema>
