<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<structdef type="CStoryModeSaveMigrationView" base="CPopulatablePageView">
		<pointer name="m_onSelectMessage" parName="OnSelect" type="uiPageDeckMessageBase" policy="owner" />
	</structdef>

</ParserSchema>
