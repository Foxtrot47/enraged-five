<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="CMigrateProfilePageView" base="CPopulatablePageView">
    <struct name="m_targetPageLink" parName="TargetPageLinkInfo" type="uiPageLink" />
  </structdef>

</ParserSchema>
