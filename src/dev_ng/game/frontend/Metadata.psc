<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">
	<structdef type="CMiniMap::Tunables"  base="CTuning">
    <struct name="Sonar" type="CMiniMap_SonarTunables"/>
    <struct name="HealthBar" type="CMiniMap_HealthBarTunables"/>
    <struct name="Bitmap" type="CMiniMap_BitmapTunables"/>
    <struct name="FogOfWar" type="CMiniMap_FogOfWarTunables"/>
    <struct name="Camera" type="CMiniMap_CameraTunables"/>
    <struct name="Tiles" type="CMiniMap_TileTunables"/>
    <struct name="Overlay" type="CMiniMap_OverlayTunables"/>
	<struct name="Display" type="CMiniMap_DisplayTunables"/>
  </structdef>
</ParserSchema>
