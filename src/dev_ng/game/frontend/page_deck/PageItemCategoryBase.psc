<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="CPageItemCategoryBase" constructible="false" >
    <pointer name="m_layout" parName="Layout" type="CPageLayoutBase" policy="owner" />
    <string name="m_title" parName="Title" type="atHashString" />
    <string name="m_id" parName="ID" type="atHashString" />
  </structdef>
  
</ParserSchema>
