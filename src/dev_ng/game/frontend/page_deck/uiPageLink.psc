<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="uiPageLink" simple="true">
	<array name="m_transitionSteps" parName="TransitionSteps" type="atArray">
		<struct parName="Item" type="uiPageInfo" />
	</array>
  </structdef>
  
</ParserSchema>
