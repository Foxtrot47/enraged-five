<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="CTabbedPageView" base="CPopulatablePageView" constructible="false" >
    <float name="m_transitionDuration" parName="TransitionDurationMS" init="250.f" />
  </structdef>
  
</ParserSchema>
