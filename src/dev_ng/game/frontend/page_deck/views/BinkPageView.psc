<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="CBinkPageView" base="CPageViewBase">
    <string name="m_movieName" parName="MovieName" type="ConstString"/>
    <pointer name="m_onFinishMessage" parName="OnFinish" type="uiPageDeckMessageBase" policy="owner" />
  </structdef>
  
</ParserSchema>
