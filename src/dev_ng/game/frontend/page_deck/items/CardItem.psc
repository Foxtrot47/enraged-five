<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<structdef type="CCardItem" base="CCardItemBase" >
    <string name="m_description" parName="Description" type="atHashString" />
    <int name="m_trackingBit" parName="TrackingBit" init="-1" />
	</structdef>
  
</ParserSchema>
