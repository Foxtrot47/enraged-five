<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<structdef type="CMultiCardItem" base="CCardItem" >
      <array name="m_featuredTxdCollection" parName="FeaturedTxdCollection" type="atArray">
        <string type="ConstString" />
      </array>
      <array name="m_featuredTextureCollection" parName="FeaturedTextureCollection" type="atArray">
        <string type="ConstString" />
      </array>
	</structdef>
  
</ParserSchema>
