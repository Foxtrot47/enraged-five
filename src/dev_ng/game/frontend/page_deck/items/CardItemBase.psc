<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<structdef type="CCardItemBase" base="CPageItemBase" constructible="false">
    <string name="m_txd" parName="Txd" type="ConstString" />
    <string name="m_texture" parName="Texture" type="ConstString" />
    <string name="m_featuredTxd" parName="FeaturedTxd" type="ConstString" />
    <string name="m_featuredTexture" parName="FeaturedTexture" type="ConstString" />
	</structdef>
  
</ParserSchema>
