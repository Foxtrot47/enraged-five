<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="CStaticDataCategory" base="CPageItemCategoryBase">
    <array name="m_items" parName="Items" type="atArray">
      <pointer type="CPageItemBase" policy="owner" />
    </array>
  </structdef>
  
</ParserSchema>
