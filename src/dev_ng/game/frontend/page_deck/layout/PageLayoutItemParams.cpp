/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : PageLayoutItemParams.cpp
// PURPOSE : Params that are used to affect content layout
//
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#include "PageLayoutItemParams.h"

#include "frontend/page_deck/uiPageConfig.h"
#if UI_PAGE_DECK_ENABLED

// framework
#include "fwutil/xmacro.h"

// game
#include "frontend/ui_channel.h"



#endif // UI_PAGE_DECK_ENABLED
