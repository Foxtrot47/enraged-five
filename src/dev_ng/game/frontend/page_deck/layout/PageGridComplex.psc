<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="CPageGridComplex" base="CPageGrid">
    <array name="m_columns" parName="Columns" type="atArray">
      <struct type="PageGridCol"/>
    </array>

    <array name="m_rows" parName="Rows" type="atArray">
      <struct type="PageGridRow"/>
    </array>
  </structdef>


</ParserSchema>
