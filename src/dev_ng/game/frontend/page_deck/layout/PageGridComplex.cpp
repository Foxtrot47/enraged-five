/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : PageGridComplex.cpp
// PURPOSE : Extension of the base grid that exposes full row/column
//           configuration to PSC. Should only be used for highly bespoke cases
//
// AUTHOR  : james.strain
// STARTED : January 2021
//
/////////////////////////////////////////////////////////////////////////////////
#include "PageGridComplex.h"

#include "frontend/page_deck/uiPageConfig.h"
#if UI_PAGE_DECK_ENABLED
#include "PageGridComplex_parser.h"

// Nothing here. This exists just to bring in the parser header

#endif // UI_PAGE_DECK_ENABLED
