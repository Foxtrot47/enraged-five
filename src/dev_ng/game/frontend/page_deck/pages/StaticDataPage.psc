<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<structdef type="CStaticDataPage" base="CPageBase">
    <array name="m_categories" parName="Categories" type="atArray">
      <pointer type="CPageItemCategoryBase" policy="owner" />
    </array>
	</structdef>
  
</ParserSchema>
