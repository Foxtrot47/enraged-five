/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : BinkPage.cpp
// PURPOSE : Bespoke page for displaying a bink video
//
// AUTHOR  : brian.beacom
// STARTED : August 2021
//
/////////////////////////////////////////////////////////////////////////////////
#include "BinkPage.h"

#if GEN9_LANDING_PAGE_ENABLED

#include "BinkPage_parser.h"

// game

FWUI_DEFINE_TYPE(CBinkPage, 0x859D5A2F);



#endif // GEN9_LANDING_PAGE_ENABLED
