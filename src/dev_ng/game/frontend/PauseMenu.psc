<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">


	<enumdef type="eMenuPref">
		<enumval name="PREF_CONTROL_CONFIG"/>
		<enumval name="PREF_CONTROL_CONFIG_FPS"/>
		<enumval name="PREF_VIBRATION"/>
		<enumval name="PREF_TARGET_CONFIG"/>
		<enumval name="PREF_INVERT_LOOK"/>
		<enumval name="PREF_CONTROLLER_SENSITIVITY"/>
		<enumval name="PREF_CONTROLLER_LIGHT_EFFECT"/>
		<enumval name="PREF_SFX_VOLUME"/>
		<enumval name="PREF_MUSIC_VOLUME"/>
		<enumval name="PREF_GPS_SPEECH"/>
		<enumval name="PREF_SPEAKER_OUTPUT"/>
		<enumval name="PREF_INTERACTIVE_MUSIC"/>
		<enumval name="PREF_RADIO_STATION"/>
		<enumval name="PREF_VOICE_OUTPUT"/>
		<enumval name="PREF_GAMMA"/>
		<enumval name="PREF_PCGAMEPAD"/>
		<enumval name="PREF_CAMERA_HEIGHT"/>
		<enumval name="PREF_FEED_PHONE"/>
		<enumval name="PREF_FEED_STATS"/>
		<enumval name="PREF_FEED_CREW"/>
		<enumval name="PREF_FEED_FRIENDS"/>
		<enumval name="PREF_FEED_SOCIAL"/>
		<enumval name="PREF_FEED_STORE"/>
		<enumval name="PREF_FEED_TOOLTIP"/>
		<enumval name="PREF_FEED_DELAY"/>
		<enumval name="PREF_DOF"/>
		<enumval name="PREF_SKFX"/>
		<enumval name="PREF_SUBTITLES"/>
		<enumval name="PREF_RADAR_MODE"/>
		<enumval name="PREF_DISPLAY_HUD"/>
		<enumval name="PREF_RETICULE"/>
		<enumval name="PREF_RETICULE_SIZE"/>
		<enumval name="PREF_DISPLAY_GPS"/>
		<enumval name="PREF_SAFEZONE_SIZE"/>
		<enumval name="PREF_CURRENT_LANGUAGE"/>
		<enumval name="PREF_AUTOSAVE"/>
		<enumval name="PREF_CINEMATIC_SHOOTING"/>
		<enumval name="PREF_MUSIC_VOLUME_IN_MP"/>
		<enumval name="PREF_FACEBOOK_UPDATES"/>
		<enumval name="PREF_PREVIOUS_LANGUAGE"/>
		<enumval name="PREF_SYSTEM_LANGUAGE"/>
		<enumval name="PREF_HDR"/>
		<enumval name="PREF_DIAG_BOOST"/>
		<enumval name="PREF_VOICE_SPEAKERS"/>
		<enumval name="PREF_SS_FRONT"/>
		<enumval name="PREF_SS_REAR"/>
		<enumval name="PREF_SNIPER_ZOOM"/>
		<enumval name="PREF_CONTROLS_CONTEXT"/>
		<enumval name="PREF_MP_CAMERA_ZOOM_ON_FOOT"/>
		<enumval name="PREF_MP_CAMERA_ZOOM_IN_VEHICLE"/>
		<enumval name="PREF_MP_CAMERA_ZOOM_ON_BIKE"/>
		<enumval name="PREF_MP_CAMERA_ZOOM_IN_BOAT"/>
		<enumval name="PREF_MP_CAMERA_ZOOM_IN_AIRCRAFT"/>
		<enumval name="PREF_MP_CAMERA_ZOOM_IN_HELI"/>
		<enumval name="PREF_STARTUP_FLOW"/>
		<enumval name="PREF_VID_SCREEN_TYPE"/>
		<enumval name="PREF_VID_RESOLUTION"/>
		<enumval name="PREF_VID_REFRESH"/>
		<enumval name="PREF_VID_ADAPTER"/>
		<enumval name="PREF_VID_MONITOR"/>
		<enumval name="PREF_VID_ASPECT"/>
		<enumval name="PREF_VID_VSYNC"/>
		<enumval name="PREF_VID_STEREO"/>
    <enumval name="PREF_VID_STEREO_CONVERGENCE"/>
    <enumval name="PREF_VID_STEREO_SEPARATION"/>
		<enumval name="PREF_VID_PAUSE_ON_FOCUS_LOSS"/>
		<enumval name="PREF_GFX_DXVERSION"/>
		<enumval name="PREF_GFX_TEXTURE_QUALITY"/>
		<enumval name="PREF_GFX_SHADER_QUALITY"/>
		<enumval name="PREF_GFX_SHADOW_QUALITY"/>
		<enumval name="PREF_GFX_REFLECTION_QUALITY"/>
		<enumval name="PREF_GFX_REFLECTION_MSAA"/>
		<enumval name="PREF_GFX_WATER_QUALITY"/>
		<enumval name="PREF_GFX_PARTICLES_QUALITY"/>
		<enumval name="PREF_GFX_GRASS_QUALITY"/>
		<enumval name="PREF_GFX_SHADOW_SOFTNESS"/>
		<enumval name="PREF_GFX_CITY_DENSITY"/>
		<enumval name="PREF_GFX_POP_VARIETY"/>
		<enumval name="PREF_GFX_FXAA"/>
		<enumval name="PREF_GFX_MSAA"/>
		<enumval name="PREF_GFX_SCALING"/>
		<enumval name="PREF_GFX_TXAA"/>
		<enumval name="PREF_GFX_ANISOTROPIC_FILTERING"/>
		<enumval name="PREF_GFX_AMBIENT_OCCLUSION"/>
		<enumval name="PREF_GFX_TESSELLATION"/>
		<enumval name="PREF_GFX_POST_FX"/>
		<enumval name="PREF_GFX_DOF"/>
		<enumval name="PREF_GFX_MB_STRENGTH"/>
		<enumval name="PREF_GFX_DIST_SCALE"/>
    <enumval name="PREF_ADV_GFX_LONG_SHADOWS"/>
    <enumval name="PREF_ADV_GFX_ULTRA_SHADOWS"/>
    <enumval name="PREF_ADV_GFX_HD_FLIGHT"/>
    <enumval name="PREF_ADV_GFX_MAX_LOD"/>
    <enumval name="PREF_ADV_GFX_SHADOWS_DIST_MULT"/>
		<enumval name="PREF_MEMORY_BAR"/>
		<enumval name="PREF_GFX_VID_OVERRIDE"/>
		<enumval name="PREF_VOICE_ENABLE"/>
		<enumval name="PREF_VOICE_OUTPUT_DEVICE"/>
		<enumval name="PREF_VOICE_OUTPUT_VOLUME"/>
		<enumval name="PREF_VOICE_SOUND_VOLUME"/>
		<enumval name="PREF_VOICE_MUSIC_VOLUME"/>
		<enumval name="PREF_VOICE_TALK_ENABLED"/>
		<enumval name="PREF_VOICE_FEEDBACK"/>
		<enumval name="PREF_VOICE_INPUT_DEVICE"/>
		<enumval name="PREF_VOICE_CHAT_MODE"/>
		<enumval name="PREF_VOICE_MIC_VOLUME"/>
		<enumval name="PREF_VOICE_MIC_SENSITIVITY"/>

		<enumval name="PREF_MOUSE_TYPE"/>
		<enumval name="PREF_MOUSE_DRIVE" />
		<enumval name="PREF_MOUSE_FLY" />
		<enumval name="PREF_MOUSE_SUB" />
		<enumval name="PREF_MOUSE_ON_FOOT_SCALE" />
		<enumval name="PREF_MOUSE_DRIVING_SCALE" />
		<enumval name="PREF_MOUSE_PLANE_SCALE" />
		<enumval name="PREF_MOUSE_HELI_SCALE" />
    <enumval name="PREF_MOUSE_SUB_SCALE"/>
    <enumval name="PREF_MOUSE_WEIGHT_SCALE"/>
    <enumval name="PREF_MOUSE_ACCELERATION"/>
    <enumval name="PREF_KBM_TOGGLE_AIM"/>
		<enumval name="PREF_ALT_VEH_MOUSE_CONTROLS"/>

    <enumval name="PREF_FPS_PERSISTANT_VIEW" />
		<enumval name="PREF_FPS_FIELD_OF_VIEW" />
		<enumval name="PREF_FPS_LOOK_SENSITIVITY" />
		<enumval name="PREF_FPS_AIM_SENSITIVITY" />
		<enumval name="PREF_FPS_AIM_DEADZONE" />
		<enumval name="PREF_FPS_AIM_ACCELERATION" />
		<enumval name="PREF_AIM_DEADZONE" />
		<enumval name="PREF_AIM_ACCELERATION" />
		<enumval name="PREF_FPS_AUTO_LEVEL" />
		<enumval name="PREF_FPS_RAGDOLL" />
		<enumval name="PREF_FPS_COMBATROLL" />
		<enumval name="PREF_FPS_HEADBOB" />
		<enumval name="PREF_FPS_THIRD_PERSON_COVER" />
    <enumval name="PREF_HOOD_CAMERA"/>
    <enumval name="PREF_FPS_VEH_AUTO_CENTER"/>

		<enumval name="PREF_LOOK_AROUND_SENSITIVITY"/>
		<enumval name="PREF_INVERT_MOUSE"/>
		<enumval name="PREF_INVERT_MOUSE_FLYING"/>
		<enumval name="PREF_INVERT_MOUSE_SUB"/>
    <enumval name="PREF_SWAP_ROLL_YAW_MOUSE_FLYING"/>
    <enumval name="PREF_MOUSE_AUTOCENTER_BIKE" />
    <enumval name="PREF_MOUSE_AUTOCENTER_CAR" />
    <enumval name="PREF_MOUSE_AUTOCENTER_PLANE" />

		<enumval name="PREF_RESTORE_DEFAULT"/>
		
		<enumval name="PREF_REPLAY_MODE"/>
		<enumval name="PREF_REPLAY_MEM_LIMIT"/>
    <enumval name="PREF_REPLAY_AUTO_RESUME_RECORDING"/>
    <enumval name="PREF_REPLAY_AUTO_SAVE_RECORDING"/>
    <enumval name="PREF_VIDEO_UPLOAD_PRIVACY"/>

		<enumval name="PREF_BIG_RADAR"/>
		<enumval name="PREF_BIG_RADAR_NAMES"/>
    <enumval name="PREF_SHOW_TEXT_CHAT"/>

    <enumval name="PREF_SIXAXIS_HELI"/>
		<enumval name="PREF_SIXAXIS_BIKE"/>
		<enumval name="PREF_SIXAXIS_BOAT"/>
		<enumval name="PREF_SIXAXIS_RELOAD"/>
		<enumval name="PREF_SIXAXIS_CALIBRATION"/>
		<enumval name="PREF_SIXAXIS_ACTIVITY"/>
		<enumval name="PREF_SIXAXIS_AFTERTOUCH"/>
		<enumval name="PREF_ACCEPT_IS_CROSS"/>
		<enumval name="PREF_WIRELESS_STEREO_HEADSET"/>
		<enumval name="PREF_CTRL_SPEAKER"/>
		<enumval name="PREF_CTRL_SPEAKER_VOL"/>
		<enumval name="PREF_PULSE_HEADSET"/>
		<enumval name="PREF_CTRL_SPEAKER_HEADPHONE"/>
		<enumval name="PREF_ALTERNATE_HANDBRAKE"/>
		<enumval name="PREF_ALTERNATE_DRIVEBY"/>
    <enumval name="PREF_LANDING_PAGE"/>    
    <enumval name="PREF_UR_PLAYMODE"/>
    <enumval name="PREF_UR_AUTOSCAN"/>
    <enumval name="PREF_AUDIO_MUTE_ON_FOCUS_LOSS"/>
    <enumval name="PREF_ROCKSTAR_EDITOR_TOOLTIP"/>
    <enumval name="PREF_ROCKSTAR_EDITOR_EXPORT_GRAPHICS_UPGRADE"/>
    <enumval name="PREF_MEASUREMENT_SYSTEM"/>
	<enumval name="PREF_FPS_DEFAULT_AIM_TYPE" />
    <enumval name="PREF_FPS_RELATIVE_VEHICLE_CAMERA_DRIVEBY_AIMING"/>
  </enumdef>

	<structdef type="CMenuItem" onPreLoad="preLoad">
		<string name="cTextId" type="atHashWithStringDev"/>
		<enum name="MenuUniqueId" type="eMenuScreen" init="MENU_UNIQUE_ID_INVALID"/>
		<enum name="MenuPref"	type="eMenuPref"	size="8"/>
		<enum name="MenuOption" type="eMenuOption" size="8"/>
		<enum name="MenuAction" type="eMenuAction" size="8"/>
		<bitset name="m_Flags" type="fixed8" values="eMenuItemBits"/>
	</structdef>

	<structdef type="CContextMenuOption" onPreLoad="preLoad">
		<string name="contextOption" type="atHashWithStringBank"/>
		<string name="text" type="atHashString"/>
		<bool name="bIsSelectable" init="true"/>
	</structdef>

	<structdef type="CContextMenu" onPostLoad="postLoad">
		<enum name="m_TriggerMenuId"	type="eMenuScreen" init="MENU_UNIQUE_ID_INVALID"/>
		<enum name="m_ContextMenuId"	type="eMenuScreen" init="MENU_UNIQUE_ID_INVALID"/>
		<enum name="m_depth"			type="eDepth"		init="MENU_DEPTH_2ND"/>
		<array name="contextOptions" type="atArray">
			<string name="contextOption" type="atHashWithStringBank"/>
		</array>
	</structdef>

	<structdef type="CMenuVersion" onPreLoad="preLoad">
		<enum name="m_InitialScreen" type="eMenuScreen" init="MENU_UNIQUE_ID_INVALID"/>
		<bitset name="m_VersionFlags" type="fixed32" values="eMenuVersionBits"/>
		<string name="m_MenuVersionId"	type="atHashWithStringDev" description="String ID by which this mode will be identified"/>
		<string name="m_MenuItemColour" type="atHashWithStringDev" init="HUD_COLOUR_FREEMODE" description="HUD colour for headers"/>
		<string name="m_MenuHeader"		type="atHashWithStringDev"/>
	</structdef>

	<structdef type="CMenuScreen" version="2">
		<pointer name="m_pDynamicData" parName="runtime"	 type="CPauseMenuDynamicData"	policy="simple_owner"/>
		<pointer name="m_pContextMenu" parName="ContextMenu" type="CContextMenu"			policy="simple_owner"/>
		<pointer name="m_pButtonList" parName="ButtonList" type="CMenuButtonList"			policy="simple_owner"/>
		<string name="cGfxFilename" type="atString"/>
		<enum name="depth" type="eDepth"/>
		<enum name="MenuScreen" type="eMenuScreen" init="MENU_UNIQUE_ID_INVALID"/>
		<bitset name="m_Flags"		 type="fixed16" values="eMenuScreenBits"/>
		<bitset name="m_ScrollBarFlags" type="fixed16" values="eMenuScrollBits"/>

		<array name="MenuItems" type="atArray">
			<struct type="CMenuItem"/>
		</array>
	</structdef>

	<structdef type="CMenuArray" onPostLoad="postLoad" version="5">
		<struct type="CGeneralPauseData" name="GeneralData"/>
		
		<array name="DisplayValues" type="atArray">
			<struct type="CMenuDisplayValue"/>
		</array>

		<struct name="DefaultButtonList" type="CMenuButtonList"/>

		<array name="MenuVersions" type="atArray">
			<struct type="CMenuVersion"/>
		</array>

		<array name="MenuScreens" type="atArray">
			<struct type="CMenuScreen"/>
		</array>

		<array name="ContextMenuOptions" type="atArray">
			<struct type="CContextMenuOption"/>
		</array>		
	</structdef>

</ParserSchema>
