<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
  generate="class">

  <enumdef type="eBlipSprite">
    <enumval name="RADAR_TRACE_WEAPON_HEALTH" 				value="153" />
    <enumval name="RADAR_TRACE_POI" 						value="162" />
    <enumval name="RADAR_TRACE_WEAPON_ARMOUR" 				value="175" />
    <enumval name="RADAR_TRACE_AI" 							value="270" />
    <enumval name="RADAR_TRACE_BOUNTY_HIT" 					value="303" />
    <enumval name="RADAR_TRACE_ENEMY_HELI_SPIN" 			value="353" 	description="This one is red, so it doesn't take the color very well"/>
    <enumval name="RADAR_TRACE_BOOST" 						value="354" />
    <enumval name="RADAR_TRACE_ROCKETS" 					value="368" />
    <enumval name="RADAR_TRACE_TESTOSTERONE" 				value="403" />
    <enumval name="RADAR_TRACE_CAPTURE_THE_FLAG" 			value="408" />
    <enumval name="RADAR_TRACE_LEVEL_INSIDE" 				value="417" />
    <enumval name="RADAR_TRACE_BOUNTY_HIT_INSIDE" 			value="418" />
    <enumval name="RADAR_TRACE_CAPTURE_THE_USAFLAG" 		value="419" 	description="For 1894081" />
    <enumval name="RADAR_TRACE_PLAYER_HELI" 				value="422" 	description="This one is white, so it colors just fine"/>
    <enumval name="RADAR_TRACE_HOT_PROPERTY" 				value="436" />
    <enumval name="RADAR_TRACE_KING_OF_THE_CASTLE" 			value="438" />
    <enumval name="RADAR_TRACE_PLAYER_KING" 				value="439" />
    <enumval name="RADAR_TRACE_DEAD_DROP" 					value="440" />
    <enumval name="RADAR_TRACE_EDGE_POINTER" 				value="443" />
    <enumval name="RADAR_TRACE_PICKUP_BEAST" 				value="463" />
    <enumval name="RADAR_TRACE_PICKUP_ZONED" 				value="464" />
    <enumval name="RADAR_TRACE_PICKUP_RANDOM" 				value="465" />
    <enumval name="RADAR_TRACE_PICKUP_SLOW_TIME" 			value="466" />
    <enumval name="RADAR_TRACE_PICKUP_SWAP" 				value="467" />
    <enumval name="RADAR_TRACE_PICKUP_THERMAL" 				value="468" />
    <enumval name="RADAR_TRACE_PICKUP_WEED" 				value="469" />
    <enumval name="RADAR_TRACE_WEAPON_RAILGUN" 				value="470" />
    <enumval name="RADAR_TRACE_BALL" 						value="471" />
    <enumval name="RADAR_TRACE_HIDDEN" 						value="472" />
	<enumval name="RADAR_TRACE_CONTRABAND"					value="478" />	
    <enumval name="RADAR_TRACE_PICKUP_ACCELERATOR" 			value="483" />
    <enumval name="RADAR_TRACE_PICKUP_GHOST" 				value="484" />
    <enumval name="RADAR_TRACE_PICKUP_DETONATOR" 			value="485" />
    <enumval name="RADAR_TRACE_PICKUP_BOMB" 				value="486" />
    <enumval name="RADAR_TRACE_PICKUP_ARMOURED" 			value="487" />
    <enumval name="RADAR_TRACE_PICKUP_JUMP" 				value="515" />
    <enumval name="RADAR_TRACE_PICKUP_DEADLINE"				value="522" />
    <enumval name="RADAR_TRACE_PICKUP_REPAIR"				value="544" />
    <enumval name="RADAR_TRACE_PICKUP_ROCKET_BOOST"			value="547" />
    <enumval name="RADAR_TRACE_PICKUP_HOMING_ROCKET"		value="548" />
    <enumval name="RADAR_TRACE_PICKUP_MACHINEGUN"			value="549" />
    <enumval name="RADAR_TRACE_PICKUP_PARACHUTE"			value="550" />
	<enumval name="RADAR_TRACE_PICKUP_TIME_5"				value="551" />
	<enumval name="RADAR_TRACE_PICKUP_TIME_10"				value="552" />
	<enumval name="RADAR_TRACE_PICKUP_TIME_15"				value="553" />
	<enumval name="RADAR_TRACE_PICKUP_TIME_20"				value="554" />
    <enumval name="RADAR_TRACE_PICKUP_TIME_30"				value="555" />
    <enumval name="RADAR_TRACE_SM_CARGO"					value="568" />
    <enumval name="RADAR_TRACE_SM_HANGAR"					value="569" />
	<enumval name="RADAR_TRACE_NHP_BAG"						value="586" />
	<enumval name="RADAR_TRACE_PICKUP_DTB_HEALTH"			value="621" />
	<enumval name="RADAR_TRACE_PICKUP_DTB_BLAST_INCREASE"	value="622" />
	<enumval name="RADAR_TRACE_PICKUP_DTB_BLAST_DECREASE"	value="623" />
	<enumval name="RADAR_TRACE_PICKUP_DTB_BOMB_INCREASE"	value="624" />
	<enumval name="RADAR_TRACE_PICKUP_DTB_BOMB_DECREASE"	value="625" />
	<enumval name="RADAR_TRACE_BAT_CARGO"	                value="615" />
	<enumval name="RADAR_TRACE_TEMP_1"	                    value="492" />
	<enumval name="RADAR_TRACE_BAT_ASSASSINATE"	            value="630" />
  </enumdef>

</ParserSchema>