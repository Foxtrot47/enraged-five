/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : BinkDefs.h
// PURPOSE : Common definitions for playback of Bink videos
//
// AUTHOR  : brian.beacom
// STARTED : September 2021
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef BINK_DEFS
#define BINK_DEFS

#define FRONTEND_BINK_BUFFER_SIZE_MULTIPLIER 16;
#define FRONTEND_BINK_READ_AHEAD_BUFFER_SIZE 524288 * FRONTEND_BINK_BUFFER_SIZE_MULTIPLIER;

#endif // BINK_DEFS