<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="CLandingPageCloudDataCategory" base="CCloudDataCategory">
    <struct name="m_freeRoamCard" parName="FreeRoamCard" type="CCardItem" />
    <struct name="m_heistCard" parName="HeistCard" type="CCardItem" />
    <struct name="m_seriesCard" parName="SeriesCard" type="CCardItem" />
    <struct name="m_newsCard" parName="NewsCard" type="CNewsItem" />
    <struct name="m_mainCard" parName="MainCard" type="CCardItem" />

    <struct name="m_networkErrorCard" parName="NetworkErrorCard" type="CNetworkErrorItem" />
    <struct name="m_noCharacterCard" parName="NoCharacterCard" type="CParallaxCardItem" />
  </structdef>

</ParserSchema>
