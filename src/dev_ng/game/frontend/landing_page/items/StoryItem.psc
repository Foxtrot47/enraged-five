<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<structdef type="CStoryItem" base="CParallaxCardItem" >
        <string name="m_storyUpsellTitle" parName="UpsellTitle" type="atHashString" />
        <string name="m_storyUpsellDescription" parName="UpsellDescription" type="atHashString" />
	</structdef>
  
</ParserSchema>
