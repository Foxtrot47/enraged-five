<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<structdef type="CSocialClubPage" base="CCategorylessPageBase">
		<pointer name="m_onDismissNotSignedInAction" parName="OnDismissNotSignedInAction" type="uiPageDeckMessageBase" policy="owner" />
	</structdef>
  
</ParserSchema>
