/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : GxtString.h
// PURPOSE : A wrapper class to manage dynamically sized strings of chars and
//				is built upon the functionality of the TextConversion class.
// AUTHOR  : James Chagaris
// STARTED : 30/08/2012
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef GXTSTRING_H
#define GXTSTRING_H

// framework
#include "fwlocalisation/localisedString.h"

// TODO4FIVE - Typdef for now, proper replace later when bringing RDR text tech in full
typedef rage::fwLocalisedString CGxtString;

#endif // GXTSTRING_H