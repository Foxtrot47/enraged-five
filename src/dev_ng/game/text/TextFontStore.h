#if 0

/*
/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : TextFontStore.h
// PURPOSE : manages the storage of the text
// AUTHOR  : Derek Payne
// STARTED : 25/05/2009
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef TEXTFONTSTORE_H
#define TEXTFONTSTORE_H


#include "fwtl\assetstore.h"
#include "text\FontDef.h"


//
// store for the fonts:
//
class CTextFontStore : public fwAssetStore<CFontDef>
{
public:
	CTextFontStore();

	virtual bool LoadFile(int index, const char* pFilename);
};

extern CTextFontStore g_TextFontStore;

#endif // TEXTFONTSTORE_H

// eof
*/

#endif
