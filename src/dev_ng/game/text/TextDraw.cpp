#if 0
/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : TextDraw.cpp
// PURPOSE : manages the rendering of the text
// AUTHOR  : Derek Payne
// STARTED : 17/03/2009
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////

// Framework headers
#include "fwmaths\Rect.h"

// Game headers
//#include "Text\TextDraw.h"
#include "Text\TextFormat.h"

#include "debug\debugdraw.h"
#include "camera\viewports\Viewport.h"
#include "renderer\sprite2d.h"
#include "frontend\Radar.h"

#include "frontend/frontendgfx.h"



// eof
#endif
