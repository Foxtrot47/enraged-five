<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="CVehicleScenarioLayoutInfo" simple="true">
    <string name="m_Name" type="atHashString" ui_key="true"/>
    <array name="m_ScenarioPoints" type="atArray">
      <struct type="CExtensionDefSpawnPoint"/>
    </array>
  </structdef>

</ParserSchema>
