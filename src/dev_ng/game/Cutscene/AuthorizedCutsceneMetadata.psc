<?xml version="1.0"?>
<ParserSchema xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:noNamespaceSchemaLocation="schemas/parsermetadata.xsd">

  <structdef type="AuthorizedCutscene" simple="true">
	<array name="m_AuthorizedCutsceneList" type="atArray">
    <string type="atHashString"/>
	</array>
  </structdef>
  
</ParserSchema>