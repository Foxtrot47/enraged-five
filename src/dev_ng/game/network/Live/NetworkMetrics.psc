<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <enumdef type="eStorePurchaseLocation">
    <enumval name="SPL_UNKNOWN" value="0" />
    <enumval name="SPL_SCUI" />
    <enumval name="SPL_INGAME" />
    <enumval name="SPL_STORE" />
    <enumval name="SPL_PHONE" />
    <enumval name="SPL_AMBIENT" />
    <enumval name="SPL_ONLINE_TAB" />
    <enumval name="SPL_STORE_GUN" />
    <enumval name="SPL_STORE_CLOTH" />
    <enumval name="SPL_STORE_HAIR" />
    <enumval name="SPL_STORE_CARMOD" />
    <enumval name="SPL_STORE_TATTOO" />
    <enumval name="SPL_STORE_PERSONAL_MOD" />
    <enumval name="SPL_STARTER_PACK" />
    <enumval name="SPL_LANDING_MP" />
    <enumval name="SPL_LANDING_SP_UPSELL" />
    <enumval name="SPL_PRIORITY_FEED" />
    <enumval name="SPL_WHATS_NEW" />
    <enumval name="SPL_MEMBERSHIP_INGAME" />
    <enumval name="SPL_PAUSE_PLAY_SP" />
    <enumval name="SPL_CHARACTER_SELECTION_WHEEL" />
  </enumdef>

</ParserSchema>
