//
// name:        BikeSyncNodes.cpp
// description: Network sync nodes used by CNetObjBikes
// written by:    John Gurney
//

#include "network/objects/synchronisation/syncnodes/BikeSyncNodes.h"

NETWORK_OPTIMISATIONS()

DATA_ACCESSOR_ID_IMPL(IBikeNodeDataAccessor);