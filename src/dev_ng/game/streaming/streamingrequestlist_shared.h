//
// streaming/streamingrequestlist_shared.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

//shared values between the recording/debug and the playback side of SRLs
#ifndef _SRL_SHARED_H_
#define _SRL_SHARED_H_

#define STREAMING_REQUEST_UPDATE	1000
#define MAX_REQUESTS_PER_FRAME		2048
#define SRL_ENABLED					(1)
#define LONG_CAMERA_CUT_THRESHOLD	(300.0f)
#define LONG_CAMERA_CUT_THRESHOLD_PRESTREAM	(50.0f)

#endif// _SRL_SHARED_H_
