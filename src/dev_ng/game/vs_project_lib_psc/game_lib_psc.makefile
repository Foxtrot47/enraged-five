Project game_lib_psc
ProjectType util
ForceLanguage Cpp

RootDirectory ..

ParserExtract

Include vs_project1_lib/game1_lib.txt
Include vs_project2_lib/game2_lib.txt
Include vs_project3_lib/game3_lib.txt
Include vs_project4_lib/game4_lib.txt
Include vs_project_network/network.txt

Libraries {
	..\..\rage\framework\src\framework_lib_psc\framework_lib_psc
	..\..\rage\base\src\rage_lib_psc\rage_lib_psc
	..\..\rage\suite\src\suite_lib_psc\suite_lib_psc
}