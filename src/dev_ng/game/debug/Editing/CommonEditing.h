#ifndef DEBUG_COMMON_EDITING_H_
#define DEBUG_COMMON_EDITING_H_

#if __BANK

// =============================================================================================== //
// INCLUDES
// =============================================================================================== //
#include "bank\console.h"

namespace DebugEditing
{
	//static const char* TrueString = "true";
	//static const char* FalseString = "false";

	static const char* HelpArgument = "-help";
}
#endif

#endif //DEBUG_COMMON_EDITING_H_
