/*
 * includes in various places, notably bSpyPacket.h, to define and iterate over 
 * the various internal bSpy packet types.
 */

BSPY_PACKET(Ping)
BSPY_PACKET(Identity)
BSPY_PACKET(BeginStep)
BSPY_PACKET(AgentState)
BSPY_PACKET(EndStep)
BSPY_PACKET(SelectAsset)
BSPY_PACKET(SelectAgent)
BSPY_PACKET(AddStringToCache)
BSPY_PACKET(TransformStream)
BSPY_PACKET(DirectInvoke)
BSPY_PACKET(Poly)
BSPY_PACKET(DynamicCollisionShape)
BSPY_PACKET(TaskDescriptor)
BSPY_PACKET(TaskBeginTask)
BSPY_PACKET(TaskVar)
BSPY_PACKET(TaskMatrixVar)
BSPY_PACKET(TaskEndTask)
BSPY_PACKET(ContactPoint)
BSPY_PACKET(LogString)
BSPY_PACKET(Feedback)
BSPY_PACKET(DebugLine)
BSPY_PACKET(TransmissionControl)
BSPY_PACKET(Scratchpad)
BSPY_PACKET(MemoryUsageRecord)
BSPY_PACKET(Profiler)
BSPY_PACKET(LimbsBegin)
BSPY_PACKET(LimbsEnd)
BSPY_PACKET(LimbComponentIK)
BSPY_PACKET(LimbComponent1Dof)
BSPY_PACKET(LimbComponent3Dof)
BSPY_PACKET(LimbComponentSetStiffness)
