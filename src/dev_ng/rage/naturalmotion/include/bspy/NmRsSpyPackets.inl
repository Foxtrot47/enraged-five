/*
 * Copyright (c) 2005-2012 NaturalMotion Ltd. All rights reserved. 
 *
 * Not to be copied, adapted, modified, used, distributed, sold,
 * licensed or commercially exploited in any manner without the
 * written consent of NaturalMotion. 
 *
 * All non public elements of this software are the confidential
 * information of NaturalMotion and may not be disclosed to any
 * person nor used for any purpose not expressly approved by
 * NaturalMotion in writing.
 *
 */

NMRS_BSPY_PACKET(AssetDescriptor)

NMRS_BSPY_PACKET(CharacterUpdate)
NMRS_BSPY_PACKET(CharacterUpdateVar)

NMRS_BSPY_PACKET(GPartDescriptor)
NMRS_BSPY_PACKET(GPartUpdate)

NMRS_BSPY_PACKET(Effector1DofDescriptor)
NMRS_BSPY_PACKET(Effector1DofUpdate)

NMRS_BSPY_PACKET(Effector3DofDescriptor)
NMRS_BSPY_PACKET(Effector3DofUpdate)

NMRS_BSPY_PACKET(EffectorModifyLimits)
