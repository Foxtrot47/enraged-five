3$User$POSSIBLE_STRAY_TOPIC
3$Programming$POSSIBLE_STRAY_TOPIC
3$, scrThread *) = 0$SYMBOL_NO_DESC
3$,const void*,int,int) = CreateThread$SYMBOL_NO_DESC
3$dest, int maxBuf, const char *input, const char *ext) = DefaultGetFullFilename$SYMBOL_NO_DESC
1$Header: //rage/jimmy/dev/rage/script/doc/qa/qa_script.txt#2 $SYMBOL_NO_DESC
    2$Header: //rage/jimmy/dev/rage/script/doc/qa/qa_script.txt#2 $SYMBOL_NO_DESC
    2$Header: //rage/jimmy/dev/rage/script/doc/qa/qa_script.txt#2 $SYMBOL_NO_DESC
    2$Header: //rage/jimmy/dev/rage/script/doc/qa/qa_script.txt#2 $SYMBOL_NO_DESC
    2$Header: //rage/jimmy/dev/rage/script/doc/qa/qa_script.txt#2 $SYMBOL_NO_DESC
    2$Header: //rage/jimmy/dev/rage/script/doc/qa/qa_script.txt#2 $SYMBOL_NO_DESC
1$rage$FUNC_PARAM_NO_DESC
    2$rage::scrProgram::DefaultGetFullFilename@char *@int@const char *@const char *$FUNC_PARAM_NO_DESC
    2$rage::scrProgram::Load@const char *$FUNC_PARAM_NO_DESC
    2$rage::scrProgram::Load@const char *@const char *$FUNC_PARAM_NO_DESC
    2$rage::scrProgram::Validate@scrProgramId$FUNC_PARAM_NO_DESC
    2$rage::scrThread::DumpState@void *printerconst char *fmt,...$FUNC_PARAM_NO_DESC
    2$rage::scrThread::GetGlobal@int$FUNC_PARAM_NO_DESC
    2$rage::scrThread::GetLocal@int$FUNC_PARAM_NO_DESC
    2$rage::scrThread::GetProgramCounter@int@const$FUNC_PARAM_NO_DESC
    2$rage::scrThread::GetStatic@int$FUNC_PARAM_NO_DESC
    2$rage::scrThread::GetThreadByIndex@int$FUNC_PARAM_NO_DESC
    2$rage::scrThread::IsValidGlobal@scrValue *$FUNC_PARAM_NO_DESC
    2$rage::scrThread::IsValidReference@scrValue *$FUNC_PARAM_NO_DESC
    2$rage::scrThread::IsValidStatic@scrValue *$FUNC_PARAM_NO_DESC
    2$rage::scrThread::LoadGlobalVariableInfo@const char *$FUNC_PARAM_NO_DESC
    2$rage::scrThread::SetBreakpoint@int@bool$FUNC_PARAM_NO_DESC
    2$rage::scrThread::SetDebugFaultHandler@FaultHandlerFunc$FUNC_PARAM_NO_DESC
    2$rage::scrThread::SetIsPaused@bool$FUNC_PARAM_NO_DESC
    2$rage::scrThread::SetStepRegion@int@int$FUNC_PARAM_NO_DESC
    2$rage::scrThread::SetThreadBlockState@bool$FUNC_PARAM_NO_DESC
    2$rage::scrThread::SetTimeStep@float$FUNC_PARAM_NO_DESC
    2$rage::scrThread::SetTimeStepUnwarped@float$FUNC_PARAM_NO_DESC
    2$rage::scrThread::Throw@int$FUNC_PARAM_NO_DESC
    2$rage::scrThread::TLS@int$FUNC_PARAM_NO_DESC
    2$rage::scrThread::Validate@u8 *@int$FUNC_PARAM_NO_DESC
1$rage$FUNC_PARAM_NO_NAME
    2$rage::scrProgram::DefaultGetFullFilename@char *@int@const char *@const char *$FUNC_PARAM_NO_NAME
    2$rage::scrProgram::Load@const char *$FUNC_PARAM_NO_NAME
    2$rage::scrProgram::Load@const char *@const char *$FUNC_PARAM_NO_NAME
    2$rage::scrProgram::Validate@scrProgramId$FUNC_PARAM_NO_NAME
    2$rage::scrThread::DumpState@void *printerconst char *fmt,...$FUNC_PARAM_NO_NAME
    2$rage::scrThread::GetGlobal@int$FUNC_PARAM_NO_NAME
    2$rage::scrThread::GetLocal@int$FUNC_PARAM_NO_NAME
    2$rage::scrThread::GetProgramCounter@int@const$FUNC_PARAM_NO_NAME
    2$rage::scrThread::GetStatic@int$FUNC_PARAM_NO_NAME
    2$rage::scrThread::GetThreadByIndex@int$FUNC_PARAM_NO_NAME
    2$rage::scrThread::IsValidGlobal@scrValue *$FUNC_PARAM_NO_NAME
    2$rage::scrThread::IsValidReference@scrValue *$FUNC_PARAM_NO_NAME
    2$rage::scrThread::IsValidStatic@scrValue *$FUNC_PARAM_NO_NAME
    2$rage::scrThread::LoadGlobalVariableInfo@const char *$FUNC_PARAM_NO_NAME
    2$rage::scrThread::RegisterCommand@const char *@void *handlerInfo&$FUNC_PARAM_NO_NAME
    2$rage::scrThread::SetBreakpoint@int@bool$FUNC_PARAM_NO_NAME
    2$rage::scrThread::SetDebugFaultHandler@FaultHandlerFunc$FUNC_PARAM_NO_NAME
    2$rage::scrThread::SetIsPaused@bool$FUNC_PARAM_NO_NAME
    2$rage::scrThread::SetStepRegion@int@int$FUNC_PARAM_NO_NAME
    2$rage::scrThread::SetThreadBlockState@bool$FUNC_PARAM_NO_NAME
    2$rage::scrThread::SetTimeStep@float$FUNC_PARAM_NO_NAME
    2$rage::scrThread::SetTimeStepUnwarped@float$FUNC_PARAM_NO_NAME
    2$rage::scrThread::Throw@int$FUNC_PARAM_NO_NAME
    2$rage::scrThread::TLS@int$FUNC_PARAM_NO_NAME
    2$rage::scrThread::Validate@u8 *@int$FUNC_PARAM_NO_NAME
1$rage$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrProgram::DefaultGetFullFilename@char *@int@const char *@const char *$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrProgram::GetProgramId@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrProgram::Load@const char *$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrProgram::Load@const char *@const char *$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrProgram::Validate@scrProgramId$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::~scrThread$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::AddThreadStack@int@int$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::AllocateThreads@int@int$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::DumpState@void *printerconst char *fmt,...$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::ExecuteNetworkCommand@u32@int@scrValue *$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::GetCurrentThread$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::GetGlobal@int$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::GetLocal@int$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::GetProgram@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::GetProgramCounter@int@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::GetScriptName$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::GetState@const$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::GetStatic@int$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::GetTimeStep$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::GetTimeStepUnwarped$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::InitClass@int$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::IsValidGlobal@scrValue *$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::IsValidStatic@scrValue *$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::Kill$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::KillAllThreads$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::KillThread@scrThread*$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::KillThread@scrThreadId$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::LoadGlobalVariableInfo@const char *$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::RegisterBuiltinCommands$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::RegisterCommand@const char *@void *handlerInfo&$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::Reset@scrProgramId@const void *@int$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::SetIsPaused@bool$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::SetTimeStep@float$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::SetTimeStepUnwarped@float$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::ShutdownClass$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::TLS@int$FUNC_RETURN_VAL_NO_DESC
    2$rage::scrThread::Validate@u8 *@int$FUNC_RETURN_VAL_NO_DESC
1$rage$SYMBOL_NO_DESC
    2$rage::CompileTimeAssert@sizeofscrValue == 4$SYMBOL_NO_DESC
    2$rage::g_PrintStackTraceFunc$SYMBOL_NO_DESC
    2$rage::rageScriptDebug$SYMBOL_NO_DESC
    2$rage::rageScriptDebug::float val)$SYMBOL_NO_DESC
    2$rage::rageScriptDebug::gScriptPrintFloat$SYMBOL_NO_DESC
    2$rage::rageScriptDebug::gScriptPrintInt$SYMBOL_NO_DESC
    2$rage::rageScriptDebug::gScriptPrintNewLine$SYMBOL_NO_DESC
    2$rage::rageScriptDebug::gScriptPrintVector3$SYMBOL_NO_DESC
    2$rage::rageScriptDebug::int val)$SYMBOL_NO_DESC
    2$rage::rageScriptDebug::prec$SYMBOL_NO_DESC
    2$rage::rageScriptDebug::str$SYMBOL_NO_DESC
    2$rage::rageScriptDebug::val$SYMBOL_NO_DESC
    2$rage::rageScriptDebug::width$SYMBOL_NO_DESC
    2$rage::scrComputeHash@const char *$SYMBOL_NO_DESC
    2$rage::scrHash$SYMBOL_NO_DESC
    2$rage::scrHash::~scrHash$SYMBOL_NO_DESC
    2$rage::scrHash::Delete$SYMBOL_NO_DESC
    2$rage::scrHash::enum@1::DELETED$SYMBOL_NO_DESC
    2$rage::scrHash::enum@1::EMPTY$SYMBOL_NO_DESC
    2$rage::scrHash::Init@int$SYMBOL_NO_DESC
    2$rage::scrHash::Insert@u32@T$SYMBOL_NO_DESC
    2$rage::scrHash::Kill$SYMBOL_NO_DESC
    2$rage::scrHash::Lookup@u32$SYMBOL_NO_DESC
    2$rage::scrHash::Remove@u32$SYMBOL_NO_DESC
    2$rage::scrHash::scrHash$SYMBOL_NO_DESC
    2$rage::scrHash::Slot$SYMBOL_NO_DESC
    2$rage::scrHash::Slot::code$SYMBOL_NO_DESC
    2$rage::scrHash::Slot::data$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_CATCH$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_F2I$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_F2V$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_FDIV$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_FEQ$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_FGE$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_FGT$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_FIRST_UNDEFINED$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_FLE$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_FLT$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_FMOD$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_FMUL$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_FNE$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_FNEG$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_FSUB$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_IDIV$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_IEQ$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_IGE$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_IGT$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_ILE$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_ILT$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_IMOD$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_IMUL$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_INE$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_INEG$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_INOT$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_IOR$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_ISUB$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_IXOR$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_JNZ$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_JZ$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_LAST_UNDEFINED$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_LOCAL1$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_LOCAL2$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_LOCAL3$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_LOCAL4$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_LOCAL5$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_LOCAL6$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_LOCAL7$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_PUSH_IM1$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_PUSH_IP1$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_PUSH_IP31$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_PUSH_IZERO$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_TEXT_LABEL_APPEND_INT$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_TEXT_LABEL_APPEND_STRING$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_TEXT_LABEL_ASSIGN_INT$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_TEXT_LABEL_COPY$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_THROW$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_VDIV$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_VMUL$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_VNEG$SYMBOL_NO_DESC
    2$rage::scrOpcode::OP_VSUB$SYMBOL_NO_DESC
    2$rage::scrPointer$SYMBOL_NO_DESC
    2$rage::scrPointer::*@const$SYMBOL_NO_DESC
    2$rage::scrPointer::_T*@const$SYMBOL_NO_DESC
    2$rage::scrPointer::=@_T*$SYMBOL_NO_DESC
    2$rage::scrPointer::->@const$SYMBOL_NO_DESC
    2$rage::scrPointer::GetObject@const$SYMBOL_NO_DESC
    2$rage::scrProgram$SYMBOL_NO_DESC
    2$rage::scrProgram::AddRef$SYMBOL_NO_DESC
    2$rage::scrProgram::GetGlobalHash$SYMBOL_NO_DESC
    2$rage::scrProgram::GetGlobals$SYMBOL_NO_DESC
    2$rage::scrProgram::GetGlobalSize$SYMBOL_NO_DESC
    2$rage::scrProgram::GetGlobalsOriginal$SYMBOL_NO_DESC
    2$rage::scrProgram::GetNumRefs$SYMBOL_NO_DESC
    2$rage::scrProgram::GetOpcodes@const$SYMBOL_NO_DESC
    2$rage::scrProgram::GetOpcodeSize@const$SYMBOL_NO_DESC
    2$rage::scrProgram::GetProgram@scrProgramId$SYMBOL_NO_DESC
    2$rage::scrProgram::GetScriptName@const$SYMBOL_NO_DESC
    2$rage::scrProgram::GetStatics@const$SYMBOL_NO_DESC
    2$rage::scrProgram::GetStaticSize@const$SYMBOL_NO_DESC
    2$rage::scrProgram::Init@const char *@const u8 *@int@const scrValue *@int@const scrValue *@int@int$SYMBOL_NO_DESC
    2$rage::scrProgram::InitClass$SYMBOL_NO_DESC
    2$rage::scrProgram::Release$SYMBOL_NO_DESC
    2$rage::scrProgram::ResetGlobals$SYMBOL_NO_DESC
    2$rage::scrProgram::Save@const char *@bool$SYMBOL_NO_DESC
    2$rage::scrProgram::SetGlobals@int@int$SYMBOL_NO_DESC
    2$rage::scrProgram::ShutdownClass$SYMBOL_NO_DESC
    2$rage::scrProgramId$SYMBOL_NO_DESC
    2$rage::scrProgramId::srcpidNONE$SYMBOL_NO_DESC
    2$rage::scrTextLabel15$SYMBOL_NO_DESC
    2$rage::scrTextLabel23$SYMBOL_NO_DESC
    2$rage::scrTextLabel31$SYMBOL_NO_DESC
    2$rage::scrTextLabel63$SYMBOL_NO_DESC
    2$rage::scrThread::c_DefaultStackSize$SYMBOL_NO_DESC
    2$rage::scrThread::CheckStackAvailability@int$SYMBOL_NO_DESC
    2$rage::scrThread::FaultHandlerFunc$SYMBOL_NO_DESC
    2$rage::scrThread::Global$SYMBOL_NO_DESC
    2$rage::scrThread::Info$SYMBOL_NO_DESC
    2$rage::scrThread::Info::Buffer$SYMBOL_NO_DESC
    2$rage::scrThread::Info::CopyReferencedParametersOut$SYMBOL_NO_DESC
    2$rage::scrThread::Info::enum@1::MAX_VECTOR3$SYMBOL_NO_DESC
    2$rage::scrThread::Info::GetVector3@int &$SYMBOL_NO_DESC
    2$rage::scrThread::Info::Info@scrValue *@int@const scrValue *$SYMBOL_NO_DESC
    2$rage::scrThread::Info::Orig$SYMBOL_NO_DESC
    2$rage::scrThread::ResetTimer$SYMBOL_NO_DESC
    2$rage::scrThread::Serialized::m_CatchFP$SYMBOL_NO_DESC
    2$rage::scrThread::Serialized::m_CatchPC$SYMBOL_NO_DESC
    2$rage::scrThread::Serialized::m_CatchSP$SYMBOL_NO_DESC
    2$rage::scrThread::Serialized::m_MaxPC$SYMBOL_NO_DESC
    2$rage::scrThread::Serialized::m_MinPC$SYMBOL_NO_DESC
    2$rage::scrThread::Serialized::m_StackSize$SYMBOL_NO_DESC
    2$rage::scrThread::Serialized::m_TimerA$SYMBOL_NO_DESC
    2$rage::scrThread::Serialized::m_TimerB$SYMBOL_NO_DESC
    2$rage::scrThread::SetFaultOnFunc@const char *@bool@bool$SYMBOL_NO_DESC
    2$rage::scrThread::State::ABORTED$SYMBOL_NO_DESC
    2$rage::scrThread::State::BLOCKED$SYMBOL_NO_DESC
    2$rage::scrThread::State::HALTED$SYMBOL_NO_DESC
    2$rage::scrThread::State::RUNNING$SYMBOL_NO_DESC
    2$rage::scrThread::ThreadStack$SYMBOL_NO_DESC
    2$rage::scrThread::ThreadStack::m_Owner$SYMBOL_NO_DESC
    2$rage::scrThread::ThreadStack::m_Stack$SYMBOL_NO_DESC
    2$rage::scrThread::ThreadStack::m_StackSize$SYMBOL_NO_DESC
    2$rage::scrThreadId$SYMBOL_NO_DESC
    2$rage::scrValue$SYMBOL_NO_DESC
    2$rage::scrValue::==@const scrValue &$SYMBOL_NO_DESC
    2$rage::scrValue::Bool$SYMBOL_NO_DESC
    2$rage::scrValue::enum@1::BOOL$SYMBOL_NO_DESC
    2$rage::scrValue::enum@1::FLOAT$SYMBOL_NO_DESC
    2$rage::scrValue::enum@1::INT$SYMBOL_NO_DESC
    2$rage::scrValue::enum@1::OBJECT$SYMBOL_NO_DESC
    2$rage::scrValue::enum@1::STRING$SYMBOL_NO_DESC
    2$rage::scrValue::enum@1::TEXT_LABEL$SYMBOL_NO_DESC
    2$rage::scrValue::enum@1::UNKNOWN$SYMBOL_NO_DESC
    2$rage::scrValue::enum@1::VECTOR$SYMBOL_NO_DESC
    2$rage::scrValue::Float$SYMBOL_NO_DESC
    2$rage::scrValue::Int$SYMBOL_NO_DESC
    2$rage::scrValue::Reference$SYMBOL_NO_DESC
    2$rage::scrValue::String$SYMBOL_NO_DESC
    2$rage::scrVector$SYMBOL_NO_DESC
    2$rage::scrVector::=@const Vector3&$SYMBOL_NO_DESC
    2$rage::scrVector::scrVector$SYMBOL_NO_DESC
    2$rage::scrVector::scrVector@const Vector3 &$SYMBOL_NO_DESC
    2$rage::scrVector::scrVector@float@float@float$SYMBOL_NO_DESC
    2$rage::scrVector::Vector3::Vector3Param@const$SYMBOL_NO_DESC
    2$rage::scrVector::Vector3@const$SYMBOL_NO_DESC
    2$rage::scrVector::x$SYMBOL_NO_DESC
    2$rage::scrVector::y$SYMBOL_NO_DESC
    2$rage::scrVector::z$SYMBOL_NO_DESC
    2$rage::scrWrapper$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg::Value@scrThread::Info&@int&$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<bool&>$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<bool&>::Value@scrThread::Info&@int&$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<bool>$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<bool>::Value@scrThread::Info&@int&$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<const char*>$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<const char*>::Value@scrThread::Info&@int&$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<const scrVector&>$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<const scrVector&>::Value@scrThread::Info&@int&$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<const Vector3&>$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<const Vector3&>::Value@scrThread::Info&@int&$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<float&>$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<float&>::Value@scrThread::Info&@int&$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<float>$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<float>::Value@scrThread::Info&@int&$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<int&>$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<int&>::Value@scrThread::Info&@int&$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<int>$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<int>::Value@scrThread::Info&@int&$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<scrVector>$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<scrVector>::Value@scrThread::Info&@int&$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<Vector3&>$SYMBOL_NO_DESC
    2$rage::scrWrapper::Arg<Vector3&>::Value@scrThread::Info&@int&$SYMBOL_NO_DESC
    2$rage::scrWrapper::AssignReturnValue@scrThread::Info&@T$SYMBOL_NO_DESC
1$scrProgram$SYMBOL_NO_DESC
    2$scrProgram::sm_Globals$SYMBOL_NO_DESC
    2$scrProgram::sm_GlobalSize$SYMBOL_NO_DESC
    2$scrProgram::sm_GlobalsOriginal$SYMBOL_NO_DESC
1$scrThread$SYMBOL_NO_DESC
    2$scrThread::sm_Globals$SYMBOL_NO_DESC
    2$scrThread::sm_IsPaused$SYMBOL_NO_DESC
    2$scrThread::sm_Stacks$SYMBOL_NO_DESC
    2$scrThread::sm_ThreadId$SYMBOL_NO_DESC
    2$scrThread::sm_Threads$SYMBOL_NO_DESC
    2$scrThread::sm_TimeStep$SYMBOL_NO_DESC
    2$scrThread::sm_TimeStepUnwarped$SYMBOL_NO_DESC
3$CheckForExtraOverflow$SYMBOL_NO_DESC
3$CheckForOverflow$SYMBOL_NO_DESC
3$Cmd$SYMBOL_NO_DESC
3$LASTEST_CONSOLE_COMMAND_LEN$SYMBOL_NO_DESC
3$LoadImm16$SYMBOL_NO_DESC
3$LoadImm32$SYMBOL_NO_DESC
3$LoadImm8$SYMBOL_NO_DESC
3$MAX_LEGIT_OPS$SYMBOL_NO_DESC
3$MAX_TOKENS$SYMBOL_NO_DESC
3$NON_STATIC_FAULT$SYMBOL_NO_DESC
3$s_AllRefCount$SYMBOL_NO_DESC
3$s_bClearOnHit$SYMBOL_NO_DESC
3$s_bPrintOnly$SYMBOL_NO_DESC
3$s_BreakFuncName$SYMBOL_NO_DESC
3$s_CommandHash$SYMBOL_NO_DESC
3$s_CurrentThread$SYMBOL_NO_DESC
3$s_GlobalHash$SYMBOL_NO_DESC
3$s_iNumTokens$SYMBOL_NO_DESC
3$s_LastestConsoleCommand$SYMBOL_NO_DESC
3$s_LastestTokenizedConsoleCommand$SYMBOL_NO_DESC
3$s_Null$SYMBOL_NO_DESC
3$s_ProgHash$SYMBOL_NO_DESC
3$s_STSO$SYMBOL_NO_DESC
3$s_Tokens$SYMBOL_NO_DESC
3$SCR_MAGIC$SYMBOL_NO_DESC
