// ps3ppusnc -Xs......u -O2 threadtest4.cpp: 0.174
// Cannot be compared to others because it increments by four.

enum {
	OP_LOAD_R0_CONST,
	OP_LOAD_R1_CONST,
	OP_ADD_R0_U8,
	OP_J,
	OP_JGE,
	OP_RET
};

void Run(unsigned *fp, unsigned char *startpc)
{
	static void *dispatch[] = {
		&&LOAD_R0_CONST,
		&&LOAD_R1_CONST,
		&&ADD_R0_U8,
		&&J,
		&&JGE,
		&&RET
	};

#define NEXT goto *dispatch[*++pc]
#define LoadImm24 (pc+=3,*(unsigned*)(pc-3) & 0xFFFFFF)

	unsigned r0 = 0, r1 = 0, t;
	unsigned char *pc = --startpc;

	NEXT;

	LOAD_R0_CONST: r0 = LoadImm24; NEXT;

	LOAD_R1_CONST: r1 = LoadImm24; NEXT;

	ADD_R0_U8: r0 += LoadImm24; NEXT;

	J: pc = startpc + LoadImm24; NEXT;

	JGE: t = LoadImm24; if (r0 >= r1) pc = startpc + t; NEXT;

	RET: return;
}

unsigned char opcodes[] = {
	OP_LOAD_R0_CONST, 0x00,0x00,0x00,	// 0000
	OP_LOAD_R1_CONST, 0x98,0x96,0x80,	// 0004
	OP_JGE, 0x00,0x00,0x20,			// 0008
	OP_ADD_R0_U8, 0x00,0x00,0x01,		// 000c
	OP_ADD_R0_U8, 0x00,0x00,0x01,		// 0010
	OP_ADD_R0_U8, 0x00,0x00,0x01,		// 0014
	OP_ADD_R0_U8, 0x00,0x00,0x01,		// 0018
	OP_J, 0x00,0x00,0x04,			// 001c
	OP_RET					// 0020
};


#include <stdio.h>
#include <sys/time_util.h>
#define TPS 79800000.0f

int main() {
	unsigned long long start, end;
	unsigned long long best = ~0U;
	const int runs = 5;
	unsigned fp[256];
	for (int i=0; i<runs; i++) {
		SYS_TIMEBASE_GET(start);
		Run(fp, opcodes);
		SYS_TIMEBASE_GET(end);
		if (best > (end-start))
			best = end-start;
	}
	printf("%f best over %d runs\n",best / TPS,runs);
	return 0;
}
