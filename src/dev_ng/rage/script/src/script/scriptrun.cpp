#include "system/ipc.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timer.h"

#include "program.h"
#include "thread.h"

using namespace rage;

int Main() {
	if (sysParam::GetArgCount() == 1)
		Quitf("Usage: scriptrun script-name");

	const char *arg = sysParam::GetArg(1);
	const char *ext = strrchr(arg, '.');
	char buf[128];

	if (ext && !strcmp(ext+2,"sc")) {		// .xsc or .csc, a resourced script
		scrProgram *pt;
		pgRscBuilder::Load(pt,arg,"#sc",scrProgram::RORC_VERSION);
		Displayf("Resourced script loaded at %p",pt);
		formatf(buf,"rsc:%p",pt);
		arg = buf;
	}

	scrThread::InitClass(953);
	scrThread::RegisterBuiltinCommands();
	scrThread::AllocateThreads(20);

	scrThread::CreateThread(arg);
	sysTimer T;
	scrThread::UpdateAll(200000000);
	Displayf("%f us",T.GetUsTime());

	scrThread::ShutdownClass();

	return 0;
}
