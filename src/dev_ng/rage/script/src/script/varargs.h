// Call this at the top of your VARARGS function, passing in the scrThread::Info object (usually named info).
#define SCRIPT_VA_BEGIN(x) const scrValue *SCRIPT_VA_ARG = info.Params; int SCRIPT_VA_COUNT = info.ParamCount - 1; unsigned __t = SCRIPT_VA_ARG++->Int

// Call this to get the type of the current parameter, one of the scrValue::VA_... types.
#define SCRIPT_VA_TYPE	(__t & 3)

// Call this when you're done processing the current parameter.  This updates SCRIPT_VA_ARG, SCRIPT_VA_COUNT, and SCRIPT_VA_TYPE.
#define SCRIPT_VA_NEXT	(++SCRIPT_VA_ARG, __t >>= 2, --SCRIPT_VA_COUNT)

// SCRIPT_VA_ARG points to the current parameter.  SCRIPT_VA_COUNT contains the count of remaining unprocessed parameters.

/*
	SCRIPT_VA_BEGIN(info);
	while (SCRIPT_VA_COUNT) {
		switch (SCRIPT_VA_TYPE) {
			case scrValue::VA_INT: 
				// Use SCRIPT_VA_ARG->Int...
				break;
			case scrValue::VA_FLOAT: 
				// Use SCRIPT_VA_ARG->Float...
				break;
			case scrValue::VA_STRINGPTR: 
				// Use SCRIPT_VA_ARG->String...
				break;
			case scrValue::VA_VECTOR:
				// Use SCRIPT_VA_ARG->Reference[0].Float for x, [1].Float for y, etc...
				break;
		}
		SCRIPT_VA_NEXT;
	}
*/

