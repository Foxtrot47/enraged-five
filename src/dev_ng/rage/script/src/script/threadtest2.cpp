// ps3ppusnc -Xs......u -O2 threadtest2.cpp: 0.525s

enum {
	OP_LOAD_R0_CONST,
	OP_LOAD_R1_CONST=4,
	OP_ADD_R0_U8=8,
	OP_J=12,
	OP_JGE=16,
	OP_RET=20
};

void Run(unsigned *fp, unsigned char *startpc)
{
	static void *dispatch[] = {
		&&LOAD_R0_CONST,
		&&LOAD_R1_CONST,
		&&ADD_R0_U8,
		&&J,
		&&JGE,
		&&RET
	};

#define NEXT goto **(void**)(((char*)dispatch)+*++pc)
#define LoadImm32 (pc+=4,*(unsigned*)(pc-3))

	unsigned r0 = 0, r1 = 0, t;
	unsigned char *pc = --startpc;

	NEXT;

	LOAD_R0_CONST: r0 = LoadImm32; NEXT;

	LOAD_R1_CONST: r1 = LoadImm32; NEXT;

	ADD_R0_U8: r0 += *++pc; NEXT;

	J: pc = startpc + LoadImm32; NEXT;

	JGE: t = LoadImm32; if (r0 >= r1) pc = startpc + t; NEXT;

	RET: return;
}

unsigned char opcodes[] = {
	OP_LOAD_R0_CONST, 0x00,0x00,0x00,0x00,	// 0000
	OP_LOAD_R1_CONST, 0x00,0x98,0x96,0x80,	// 0005
	OP_JGE, 0x00,0x00,0x00,0x16,		// 000a
	OP_ADD_R0_U8, 0x01,			// 000f
	OP_J, 0x00,0x00,0x00,0x05,		// 0011
	OP_RET					// 0016
};


#include <stdio.h>
#include <sys/time_util.h>
#define TPS 79800000.0f

int main() {
	unsigned long long start, end;
	unsigned long long best = ~0U;
	const int runs = 5;
	unsigned fp[256];
	for (int i=0; i<runs; i++) {
		SYS_TIMEBASE_GET(start);
		Run(fp, opcodes);
		SYS_TIMEBASE_GET(end);
		if (best > (end-start))
			best = end-start;
	}
	printf("%f best over %d runs\n",best / TPS,runs);
	return 0;
}
