#include <stdio.h>
#include <string.h>

typedef unsigned u32;

u32 scrComputeHash(const char *name) {
	// This is the one-at-a-time hash from this page:
	// http://burtleburtle.net/bob/hash/doobs.html
	// (borrowed from /soft/swat/src/swcore/string2key.cpp)
	// (borrowed again from \rage\base\src\string\stringhash.cpp)

	bool quotes = (*name == '\"');
	u32 key = 0;

	if (quotes) 
		name++;

	while (*name && (!quotes || *name != '\"'))
	{
		char character = *name++;
		if (character >= 'A' && character <= 'Z') 
			character += 'a'-'A';
		else if (character == '\\')
			character = '/';

		key += character;
		key += (key << 10);	//lint !e701
		key ^= (key >> 6);	//lint !e702
	}
	key += (key << 3);	//lint !e701
	key ^= (key >> 11);	//lint !e702
	key += (key << 15);	//lint !e701

	if (key < 2) key += 2;	// disallow zero (EMPTY) and one (DELETED)

	return key;
}


void main(int argc,char **argv)
{
	char buf[2048];
	char newbuf[2048];

	FILE *in = fopen(argv[1],"r");
	if (!in) { fprintf(stderr,"bad input"); return; }
	FILE *out = fopen(argv[2],"w");
	if (!out) { fprintf(stderr,"bad output"); return; }
	const char *macro = "SCR_REGISTER_SECURE(";
	const char *prefix = "";
	if (argc > 3)
		macro = argv[3];
	if (argc > 4)
		prefix = argv[4];
	int ml = strlen(macro);

	while (fgets(buf,sizeof(buf),in)) {
		char *b = buf, *o = newbuf;
		while (*b == 32 || *b == 9)
			*o++ = *b++;
		if (!strncmp(b,macro,ml)) {
			b += ml;
			strcpy(o,macro);
			while (*b == 32 || *b == 9)
				b++;
			const char *sym = b;
			while (*b != ',' && *b != 32 && *b != 9 && *b)
				b++;
			char temp[256];
			memset(temp,0,sizeof(temp));
			strcpy(temp,prefix);
			memcpy(temp+strlen(prefix),sym,b-sym);

			// skip existing hashcode
			while (*b && *b != ',')
				b++;
			if (*b) b++;

			while (*b  == 32 || *b == 9)
				b++;

			if (!strncmp(b,"SCRHASH",7)) {
				while (*b != ')')
					b++;
				b++;
			}
			while (*b && *b != ',')
				b++;
	
			sprintf(newbuf+strlen(newbuf),"%s, 0x%x", temp + strlen(prefix), scrComputeHash(temp));
			strcat(newbuf,b);
			strcpy(buf,newbuf);
		}

		fputs(buf,out);
	}
	fclose(in);
	fclose(out);
}
