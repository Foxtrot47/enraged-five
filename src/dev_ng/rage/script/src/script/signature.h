// 
// script/signature.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SCRIPT_SIGNATURE_H 
#define SCRIPT_SIGNATURE_H 

#include "scrchannel.h"
#include "value.h"

namespace rage {

template <class T,int initSize> class scrHash;

// This is a function signature for an "exported" function. 
// It can represent functions with 1 return value and up to 18 arguments
class scrSignature
{
public:
	scrSignature() : m_Storage(0x0) {}

	explicit scrSignature(u64 data) : m_Storage(data) {}

	inline explicit scrSignature(
		scrValue::ValueType returnValue,
		scrValue::ValueType arg0 = scrValue::UNKNOWN,
		scrValue::ValueType arg1 = scrValue::UNKNOWN,
		scrValue::ValueType arg2 = scrValue::UNKNOWN,
		scrValue::ValueType arg3 = scrValue::UNKNOWN,
		scrValue::ValueType arg4 = scrValue::UNKNOWN,
		scrValue::ValueType arg5 = scrValue::UNKNOWN,
		scrValue::ValueType arg6 = scrValue::UNKNOWN,
		scrValue::ValueType arg7 = scrValue::UNKNOWN,
		scrValue::ValueType arg8 = scrValue::UNKNOWN,
		scrValue::ValueType arg9 = scrValue::UNKNOWN,
		scrValue::ValueType arg10 = scrValue::UNKNOWN,
		scrValue::ValueType arg11 = scrValue::UNKNOWN,
		scrValue::ValueType arg12 = scrValue::UNKNOWN,
		scrValue::ValueType arg13 = scrValue::UNKNOWN,
		scrValue::ValueType arg14 = scrValue::UNKNOWN,
		scrValue::ValueType arg15 = scrValue::UNKNOWN,
		scrValue::ValueType arg16 = scrValue::UNKNOWN,
		scrValue::ValueType arg17 = scrValue::UNKNOWN
		);

	// PURPOSE: Counts the number of arguments defined in this signature
	// RETURNS: The number of arguments [0-17] or -1 on error
	inline int CountNumArgs();

	// PURPOSE: Counts the number of stack entries expected for arguments defined in this signature,
	//          counting a vector as 3 entries.
	// RETURNS: The number of arguments [0-17] or -1 on error
	inline int CountNumStackEntriesForArgs();

	// PURPOSE: Returns the return type of this signature, or UNKNOWN if there is no return value
	inline scrValue::ValueType GetReturnType();

	// PURPOSE: Returns the type for argument N, or UNKNOWN if there is no argument N.
	inline scrValue::ValueType GetArgumentType(u32 i);

	// PURPOSE: Is this a valid signature?
	inline bool IsValid();

#if __DEV
	// PURPOSE: Fills out outBuf with a rage-script style function signature
	void PrettyPrint(char* outBuf, int outBufSize, const char* fnName);
#endif

	//////////////////////////////////////////////////
	// Format for a scrSignature
	//
	// Bits:
	// 6 6 6 6 5 5 5 5 5 5 5 5 5 5 4 4 4 4 4 4 4 4 4 4 3 3 3 3 3 3 3 3 3 3 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0
	// 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
	// [ ] [argI][argH][argG][argF][argE][argD][argC][argB][argA][arg9][arg8][arg7][arg6][arg5][arg4][arg3][arg2][arg1][arg0][retv][st]
	//
	// Note that retv is near the lower args. The reason for this is that
	// for smaller #s of arguments we can generate a function signature with a 
	// single 'load immediate' instruction.
	enum {
		ARG0_BITS = 5,
		TYPE_WIDTH = 3,
		TYPE_MASK = (1 << TYPE_WIDTH) - 1,
		MAX_ARGS = 18,
		RETV_BITS = 2,
		STATE_BITS = 0,
		STATE_WIDTH = 2,
		STATE_MASK = (1 << STATE_WIDTH) - 1
	};

	// We have 2 bits for state = 4 state values
	enum {
		STATE_INVALID = 0,
		STATE_VALID = 1,
		STATE_UNUSED0 = 2,
		STATE_UNUSED1 = 3
	};

protected:
	u64 m_Storage;

	inline u64 GetState();

};

#define SCR_MAKE_SIGNATURE(retV, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17)	\
(																					\
	(scrSignature::STATE_VALID << scrSignature::STATE_BITS)						|	\
	((u64)(retV)  << scrSignature::RETV_BITS)									|	\
	((u64)(arg0)  << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 0))	|	\
	((u64)(arg1)  << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 1))	|	\
	((u64)(arg2)  << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 2))	|	\
	((u64)(arg3)  << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 3))	|	\
	((u64)(arg4)  << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 4))	|	\
	((u64)(arg5)  << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 5))	|	\
	((u64)(arg6)  << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 6))	|	\
	((u64)(arg7)  << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 7))	|	\
	((u64)(arg8)  << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 8))	|	\
	((u64)(arg9)  << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 9))	|	\
	((u64)(arg10) << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 10))	|	\
	((u64)(arg11) << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 11))	|	\
	((u64)(arg12) << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 12))	|	\
	((u64)(arg13) << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 13))	|	\
	((u64)(arg14) << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 14))	|	\
	((u64)(arg15) << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 15))	|	\
	((u64)(arg16) << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 16))	|	\
	((u64)(arg17) << (scrSignature::ARG0_BITS + scrSignature::TYPE_WIDTH * 17))		\
)																					\
//END

// Try to keep scrSignatures small
CompileTimeAssert(sizeof(scrSignature) == sizeof(u64));

scrSignature::scrSignature( scrValue::ValueType returnValue, 
						   scrValue::ValueType arg0 /* = scrValue::UNKNOWN */, 
						   scrValue::ValueType arg1 /* = scrValue::UNKNOWN */, 
						   scrValue::ValueType arg2 /* = scrValue::UNKNOWN */, 
						   scrValue::ValueType arg3 /* = scrValue::UNKNOWN */, 
						   scrValue::ValueType arg4 /* = scrValue::UNKNOWN */, 
						   scrValue::ValueType arg5 /* = scrValue::UNKNOWN */, 
						   scrValue::ValueType arg6 /* = scrValue::UNKNOWN */, 
						   scrValue::ValueType arg7 /* = scrValue::UNKNOWN */, 
						   scrValue::ValueType arg8 /* = scrValue::UNKNOWN */,
						   scrValue::ValueType arg9 /* = scrValue::UNKNOWN */,
						   scrValue::ValueType arg10 /* = scrValue::UNKNOWN */,
						   scrValue::ValueType arg11 /* = scrValue::UNKNOWN */,
						   scrValue::ValueType arg12 /* = scrValue::UNKNOWN */,
						   scrValue::ValueType arg13 /* = scrValue::UNKNOWN */,
						   scrValue::ValueType arg14 /* = scrValue::UNKNOWN */,
						   scrValue::ValueType arg15 /* = scrValue::UNKNOWN */,
						   scrValue::ValueType arg16 /* = scrValue::UNKNOWN */,
						   scrValue::ValueType arg17 /* = scrValue::UNKNOWN */ )
{
	m_Storage = 
		(
		(STATE_VALID << STATE_BITS) |
		((u64)returnValue << RETV_BITS) |
		((u64)arg17 << (TYPE_WIDTH * 17)) |
		((u64)arg16 << (TYPE_WIDTH * 16)) |
		((u64)arg15 << (TYPE_WIDTH * 15)) |
		((u64)arg14 << (TYPE_WIDTH * 14)) |
		((u64)arg13 << (TYPE_WIDTH * 13)) |
		((u64)arg12 << (TYPE_WIDTH * 12)) |
		((u64)arg11 << (TYPE_WIDTH * 11)) |
		((u64)arg10 << (TYPE_WIDTH * 10)) |
		((u64)arg9 << (TYPE_WIDTH * 9)) |
		((u64)arg8 << (TYPE_WIDTH * 8)) |
		((u64)arg7 << (TYPE_WIDTH * 7)) |
		((u64)arg6 << (TYPE_WIDTH * 6)) |
		((u64)arg5 << (TYPE_WIDTH * 5)) |
		((u64)arg4 << (TYPE_WIDTH * 4)) |
		((u64)arg3 << (TYPE_WIDTH * 3)) |
		((u64)arg2 << (TYPE_WIDTH * 2)) |
		((u64)arg1 << (TYPE_WIDTH * 1)) |
		((u64)arg0 << (TYPE_WIDTH * 0))
		);

#if __ASSERT
	// Check that there aren't any UNKNOWN values in the middle of the arg list
	int knownCount = 0;
	if (arg0 != scrValue::UNKNOWN) knownCount++;
	if (arg1 != scrValue::UNKNOWN) knownCount++;
	if (arg2 != scrValue::UNKNOWN) knownCount++;
	if (arg3 != scrValue::UNKNOWN) knownCount++;
	if (arg4 != scrValue::UNKNOWN) knownCount++;
	if (arg5 != scrValue::UNKNOWN) knownCount++;
	if (arg6 != scrValue::UNKNOWN) knownCount++;
	if (arg7 != scrValue::UNKNOWN) knownCount++;
	if (arg8 != scrValue::UNKNOWN) knownCount++;
	if (arg9 != scrValue::UNKNOWN) knownCount++;
	if (arg10 != scrValue::UNKNOWN) knownCount++;
	if (arg11 != scrValue::UNKNOWN) knownCount++;
	if (arg12 != scrValue::UNKNOWN) knownCount++;
	if (arg13 != scrValue::UNKNOWN) knownCount++;
	if (arg14 != scrValue::UNKNOWN) knownCount++;
	if (arg15 != scrValue::UNKNOWN) knownCount++;
	if (arg16 != scrValue::UNKNOWN) knownCount++;
	if (arg17 != scrValue::UNKNOWN) knownCount++;
	scrAssertf(knownCount == CountNumArgs(), "\"UNKNOWN\" arguments are not allowed in the argument list. Signature is 0x%x%x",
		(u32)(m_Storage >> 32), (u32)(m_Storage & 0xffffffff));
#endif
}

u64 scrSignature::GetState()
{
	return (m_Storage >> STATE_BITS) & STATE_MASK;
}

bool scrSignature::IsValid()
{
	return GetState() == STATE_VALID;
}

scrValue::ValueType scrSignature::GetReturnType()
{
	return (scrValue::ValueType)((m_Storage >> RETV_BITS) & TYPE_MASK);
}

scrValue::ValueType scrSignature::GetArgumentType(u32 i)
{
	scrAssertf(i < MAX_ARGS, "Argument %d out of range for a scrSignature", i);
	return (scrValue::ValueType)((m_Storage >> (ARG0_BITS + TYPE_WIDTH * i)) & TYPE_MASK);
}

int scrSignature::CountNumArgs()
{
	int i = 0;
	scrValue::ValueType lastType = scrValue::UNKNOWN;
	do 
	{
		lastType = GetArgumentType(i);
		i++;
	} while (i < MAX_ARGS && lastType != scrValue::UNKNOWN);
	return i < MAX_ARGS ? i-1 : -1;
}

int scrSignature::CountNumStackEntriesForArgs()
{
	int i = 0;
	scrValue::ValueType lastType = scrValue::UNKNOWN;
	do 
	{
		lastType = GetArgumentType(i);
		if(lastType == scrValue::VECTOR)
		{
			// Vectors are passed as 3 floats (2 arguments more than expected)
			i += 2;
		}
		i++;
	} while (i < MAX_ARGS && lastType != scrValue::UNKNOWN);
	return i < MAX_ARGS ? i-1 : -1;
}

} // namespace rage

#endif // SCRIPT_SIGNATURE_H 
