#ifndef SCRIPT_VALUE_H

namespace rage {

typedef unsigned u32;
typedef int s32;
typedef unsigned short u16;
typedef signed short s16;
typedef unsigned char u8;

const u32 scrPageShift = 14;
const u32 scrPageSize = (1 << scrPageShift);
const u32 scrPageMask = scrPageSize - 1;

extern u32 atStringHash(const char*,const u32 = 0);

union scrValue { int Int; float Float; unsigned Uns; scrValue *Reference; const char *String; };

#ifdef __SNC__
struct Vector3 { u32 x, y, z; } __attribute__((aligned(16)));
#else
__declspec(align(16)) struct Vector3 { float x, y, z; };
#endif

struct scrProgram
{
	void *vptr;					// pgBase
	u32 *m_PageMap;				// pgBase

	u8 **Table;					// +8
	u32 GlobalsHash;			// +12
	u32 OpcodeSize;				// +16
	u32 ArgStructSize;			// +20
	u32 StaticSize;				// +24
	u32 GlobalSize;				// +28
	u32 NativeSize;				// +32
	scrValue *Statics;			// +36
	scrValue *Globals;			// +40
	u32 *Natives;				// +44
	u32 ProcCount;				// +48
	const char **ProcNames;		// +52
	u32 HashCode;
	u32 RefCount;
	const char *ScriptName;		// +64
	const char *StringHeap;
	u32 StringHeapSize;
};

class scrThread
{
public:
	struct Info {
		Info(scrValue *resultPtr, int parameterCount, const scrValue *params) :
			ResultPtr(resultPtr), ParamCount(parameterCount), Params(params), BufferCount(0) { }

		// Return result, if applicable
		scrValue *ResultPtr;
		// Parameter count
		int ParamCount;
		// Pointer to parameter values
		const scrValue *Params;

		int BufferCount;
		u32 *Orig[4];
		Vector3 Buffer[4];

		void CopyReferencedParametersOut() { 
			int bc = BufferCount;
			while (bc--) {
				u32 *dst = Orig[bc];
				u32 *src = &Buffer[bc].x;
				dst[0] = src[0]; 
				dst[1] = src[1]; 
				dst[2] = src[2];
			}
		}
	};

	static const int MAX_CALLSTACK = 16;

	struct Serialized {
		void *m_ThreadId;				// +0 Unique thread ID (always increasing)
		void *m_Prog;				// +4 Stored by hashcode rather than pointer so load/save can work
		int m_State;						// +8 Current thread state
		int m_PC;							// +12 Current program counter (index into program's opcodes array)
		int m_FP;							// +16 Current frame pointer (anchor for local variables)
		int m_SP;							// +20 Stack pointer
		int m_TimerA, m_TimerB;				// Local per-thread timer values
		float m_Wait;							// Accumulated wait timer
		int m_MinPC, m_MaxPC;				// Step region for debugger
		scrValue m_TLS[4];		// Thread-local storage for blocking functions
		int m_StackSize;
		int m_CatchPC, m_CatchFP, m_CatchSP;
#if !__FINAL
		int m_CallDepth;
		int m_CallStack[MAX_CALLSTACK];
#endif
	};

	typedef char sizecheck[sizeof(Info)==96? 1 : -1];

	static u32 Run(scrValue * __restrict stack,scrValue * __restrict globals,const scrProgram &pt,Serialized * __restrict ser);

#if !__FINAL
	static void SetIsInNativeFunction(bool f) { sm_InNativeFunction = f; }
	static bool sm_InNativeFunction;
#endif
};

}	// namespace rage

enum State { RUNNING, BLOCKED, ABORTED, HALTED, RESET_INSTRUCTION_COUNT };
enum { c_NativeInsnLength = 4 };

inline float scr_fmodf(float x, float y)	{ return y? x - ((int)(x/y) * y) : 0; }

#define Fault(string,...) goto FAULT

#define FULL_LOGGING 0

#ifndef DRIVER
static rage::scrValue s_Null;
#endif

#else
#undef LoadImm32
#undef LoadImm24
#undef LoadImm16
#undef LoadImmS16
#undef LoadImm8

#define Fault(string,...) do { Errorf("FAULT: " string " (%s pc = 0x%x)",##__VA_ARGS__,s_ScriptName,pc - opcodes); goto FAULT; } while (0)

#endif

#define LoadImm32	((pc+=4), *(u32*)(pc-3))
#if defined(__SNC__) || defined(_XBOX_VER)
#define LoadImm24	((pc+=3), *(u32*)(pc-3) & 0xFFFFFF)
#else
#define LoadImm24	((pc+=3), *(u32*)(pc-3) >> 8)
#endif
#define LoadImm16	((pc+=2), *(u16*)(pc-1))
#define LoadImmS16	((pc+=2), *(s16*)(pc-1))
#define LoadImm8	(*++pc)

#include "opcode.h"


#ifndef DRIVER

namespace rage {

typedef void (*Cmd)(scrThread::Info&);

static void scr_assign_string(char *dst,unsigned siz,const char *src)
{
	if (src)
	{
		while (*src && --siz)
			*dst++ = *src++;
	}
	*dst = '\0';
}

static void scr_append_string(char *dst,unsigned siz,const char *src)
{
	while (*dst)
		dst++, --siz;
	scr_assign_string(dst,siz,src);
}

static void scr_itoa(char *dest,int value)
{
	char stack[16], *sp = stack;
	if (value < 0) 
	{
		*dest++ = '-';
		value = -value;
	}
	else if (!value) 
	{
		dest[0] = '0';
		dest[1] = 0;
		return;
	}
	while (value)
	{
		*sp++ = (char)((value % 10) + '0');
		value /= 10;
	}
	while (sp != stack)
	{
		*dest++ = *--sp;
	}
	*dest = 0;
}

#define DEBUG_TRACE 0

#if DEBUG_TRACE
struct scrTrace { u32 pc:24, opcode:8; };
const int s_TraceMax = 16;
scrTrace s_TraceBuf[s_TraceMax];
int s_TraceNext;
#endif

#define THREADED_INTERPRETER	(__PS3 && !DEBUG_TRACE)		// Both gcc and snc support computed goto's.

#if __DEV || __BANK
// Pick a reasonable value that fits in a PPC compare immediate insn.
#define VALIDATE_PC(target) do { if (target < 4) Fault("Impossible jump target caught"); } while (0)
#define VALIDATE_PTR(value)	do { if (value.Uns < 16384) Fault("Invalid reference caught"); } while (0)
#else
#define VALIDATE_PC(target)
#define VALIDATE_PTR(value)
#endif

#if STREAMABLE_PROGRAMS
#define SET_PC(_o)	do { u32 o = _o; pc = (opcodesTbl[o>>scrPageShift] + (o & scrPageMask) - 1); opcodes = pc - o; } while (0)
#define ADD_PC(_r)	SET_PC((pc - opcodes) + (_r))
#define CHECK_PC	SET_PC(pc - opcodes)
#else
#define SET_PC(_o) (pc = opcodes + (_o))
#define ADD_PC(_r) (pc += (_r))
#define CHECK_PC
#endif

#if !__FINAL && defined(SCRIPT_VALUE_H)
#define DEC_CALLDEPTH --(ser->m_CallDepth)
#else
#define DEC_CALLDEPTH
#endif


#if FULL_LOGGING
u32 scrThread::Run(scrValue * __restrict stack,scrValue * __restrict stackMax,scrValue * __restrict globals,scrValue * __restrict globalsMax,const u8 * __restrict opcodes,const u8 * __restrict opcodesMax,Serialized * __restrict ser)
#else
u32 scrThread::Run(scrValue * __restrict stack,scrValue * __restrict globals,const scrProgram &pt,Serialized * __restrict ser)
#endif
{
	const u8 **opcodesTbl = const_cast<const u8**>(pt.Table);

	scrValue *sp = stack + ser->m_SP - 1;
	scrValue *fp = stack + ser->m_FP;
	const u8 *pc;
#if STREAMABLE_PROGRAMS
	const u8 *opcodes;
#else
	--opcodes;			// so we don't have to subtract one constantly
#endif
	SET_PC(ser->m_PC);
	char buf[16];
#if !__FINAL && !THREADED_INTERPRETER
	unsigned insnLimit = 1000000000;
#endif

	// sp[0] is TOS, sp[-1] is next down
	// Push -> *++sp = (x)
	// Pop -> *sp--;
	// unsigned st0, st1, st2, st3;

#if THREADED_INTERPRETER
	static void *jumpTable[256] = {
#define OP(a,b,c) &&LABEL_OP_##a
#include "opcodes.h"
#undef OP
		// This needs to be adjusted if we add or remove opcodes:
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default,
	};
	// FETCH_INSN must appear after all LoadImm... opcodes or any references to 'pc'
#define CASE(x) LABEL_##x:
#define DEFAULT LABEL_default:
# if 1
	void *ni;
# define FETCH_INSN ni = jumpTable[LoadImm8]
# define NEXT_INSN goto *ni
# else
# define FETCH_INSN
# define NEXT_INSN goto *jumpTable[LoadImm8]
# endif
#else
#define CASE(x) case x:
#define DEFAULT default:
#define FETCH_INSN
#define NEXT_INSN goto DISPATCH
DISPATCH:
#endif

	{
#if FULL_LOGGING
		LogThreadState(stack,stackMax,globals,globalsMax,sp-stack+1,fp-stack,opcodes+1,opcodesMax,pc-opcodes);
#endif
#if !__FINAL && !THREADED_INTERPRETER
		if (!--insnLimit)
			Fault("Script is stuck in a loop?");
#endif

		// printf("pc %x opcode %d sp %x fp %x\n",pc-opcodes,pc[1],ser->m_SP,ser->m_FP);
		// Displayf("%x: %x SP=%p FP=%p TOS=%x %x %x %x",pc,opcode,sp,ser->m_FP,sp[0].Int,sp[-1].Int,sp[-2].Int,sp[-3].Int);
#if THREADED_INTERPRETER
		FETCH_INSN;
		NEXT_INSN;		// start the threaded interpreter
#else

#if DEBUG_TRACE
		scrTrace &T = s_TraceBuf[s_TraceNext];
		s_TraceNext = (s_TraceNext + 1) & (s_TraceMax-1);
		T.pc = pc - opcodes;
		T.opcode = pc[1];
#endif
		switch (LoadImm8) 
		{
#endif
			CASE(OP_NOP) CHECK_PC; FETCH_INSN; NEXT_INSN;
			CASE(OP_IADD) FETCH_INSN; --sp; sp[0].Int += sp[1].Int; NEXT_INSN;
			CASE(OP_ISUB) FETCH_INSN; --sp; sp[0].Int -= sp[1].Int; NEXT_INSN;
			CASE(OP_IMUL) FETCH_INSN; --sp; sp[0].Int *= sp[1].Int; NEXT_INSN;
			CASE(OP_IDIV) FETCH_INSN; --sp; if (sp[1].Int) sp[0].Int /= sp[1].Int; NEXT_INSN;
			CASE(OP_IMOD) FETCH_INSN; --sp; if (sp[1].Int) sp[0].Int %= sp[1].Int; NEXT_INSN;
			CASE(OP_INOT) FETCH_INSN; sp[0].Int = !sp[0].Int; NEXT_INSN;
			CASE(OP_INEG) FETCH_INSN; sp[0].Int = -sp[0].Int; NEXT_INSN;

			CASE(OP_IEQ) FETCH_INSN; --sp; sp[0].Int = sp[0].Int == sp[1].Int; NEXT_INSN;
			CASE(OP_INE) FETCH_INSN; --sp; sp[0].Int = sp[0].Int != sp[1].Int; NEXT_INSN;
			CASE(OP_IGE) FETCH_INSN; --sp; sp[0].Int = sp[0].Int >= sp[1].Int; NEXT_INSN;
			CASE(OP_IGT) FETCH_INSN; --sp; sp[0].Int = sp[0].Int >  sp[1].Int; NEXT_INSN;
			CASE(OP_ILE) FETCH_INSN; --sp; sp[0].Int = sp[0].Int <= sp[1].Int; NEXT_INSN;
			CASE(OP_ILT) FETCH_INSN; --sp; sp[0].Int = sp[0].Int <  sp[1].Int; NEXT_INSN;

			CASE(OP_FADD) FETCH_INSN; --sp; sp[0].Float += sp[1].Float; NEXT_INSN;
			CASE(OP_FSUB) FETCH_INSN; --sp; sp[0].Float -= sp[1].Float; NEXT_INSN;
			CASE(OP_FMUL) FETCH_INSN; --sp; sp[0].Float *= sp[1].Float; NEXT_INSN;
			CASE(OP_FDIV) FETCH_INSN; --sp; if (sp[1].Int) sp[0].Float /= sp[1].Float; NEXT_INSN;
			CASE(OP_FMOD) FETCH_INSN; --sp; if (sp[1].Int) sp[0].Float = scr_fmodf(sp[0].Float,sp[1].Float); NEXT_INSN;
			CASE(OP_FNEG) FETCH_INSN; sp[0].Uns ^= 0x80000000; NEXT_INSN;

			CASE(OP_FEQ) FETCH_INSN; --sp; sp[0].Int = sp[0].Float == sp[1].Float; NEXT_INSN;	// TODO: Use integer compare?
			CASE(OP_FNE) FETCH_INSN; --sp; sp[0].Int = sp[0].Float != sp[1].Float; NEXT_INSN;	// TODO: Use integer compare?
			CASE(OP_FGE) FETCH_INSN; --sp; sp[0].Int = sp[0].Float >= sp[1].Float; NEXT_INSN;
			CASE(OP_FGT) FETCH_INSN; --sp; sp[0].Int = sp[0].Float >  sp[1].Float; NEXT_INSN;
			CASE(OP_FLE) FETCH_INSN; --sp; sp[0].Int = sp[0].Float <= sp[1].Float; NEXT_INSN;
			CASE(OP_FLT) FETCH_INSN; --sp; sp[0].Int = sp[0].Float <  sp[1].Float; NEXT_INSN;

			CASE(OP_VADD) FETCH_INSN; sp-=3; sp[-2].Float += sp[1].Float; sp[-1].Float += sp[2].Float; sp[0].Float += sp[3].Float; NEXT_INSN;
			CASE(OP_VSUB) FETCH_INSN; sp-=3; sp[-2].Float -= sp[1].Float; sp[-1].Float -= sp[2].Float; sp[0].Float -= sp[3].Float; NEXT_INSN;
			CASE(OP_VMUL) FETCH_INSN; sp-=3; sp[-2].Float *= sp[1].Float; sp[-1].Float *= sp[2].Float; sp[0].Float *= sp[3].Float; NEXT_INSN;
			CASE(OP_VDIV) FETCH_INSN; sp-=3; if (sp[1].Int) sp[-2].Float /= sp[1].Float; if (sp[2].Int) sp[-1].Float /= sp[2].Float; if (sp[3].Int) sp[0].Float /= sp[3].Float; NEXT_INSN;
			CASE(OP_VNEG) FETCH_INSN; sp[-2].Uns ^= 0x80000000; sp[-1].Uns ^= 0x80000000; sp[0].Uns ^= 0x80000000; NEXT_INSN;

			CASE(OP_IAND) FETCH_INSN; --sp; sp[0].Int &= sp[1].Int; NEXT_INSN;
			CASE(OP_IOR)  FETCH_INSN; --sp; sp[0].Int |= sp[1].Int; NEXT_INSN;
			CASE(OP_IXOR) FETCH_INSN; --sp; sp[0].Int ^= sp[1].Int; NEXT_INSN;

			CASE(OP_I2F) FETCH_INSN; sp[0].Float = (float) sp[0].Int; NEXT_INSN;
			CASE(OP_F2I) FETCH_INSN; sp[0].Int = (int) sp[0].Float; NEXT_INSN;
			CASE(OP_F2V) FETCH_INSN; sp+=2; sp[-1].Int = sp[0].Int = sp[-2].Int; NEXT_INSN;

			CASE(OP_PUSH_CONST_U8) ++sp; sp[0].Int = LoadImm8; FETCH_INSN; NEXT_INSN;
			CASE(OP_PUSH_CONST_U8_U8) sp+=2; sp[-1].Int = LoadImm8; sp[0].Int = LoadImm8; FETCH_INSN; NEXT_INSN;
			CASE(OP_PUSH_CONST_U8_U8_U8) sp+=3; sp[-2].Int = LoadImm8; sp[-1].Int = LoadImm8; sp[0].Int = LoadImm8; FETCH_INSN; NEXT_INSN;
			
			CASE(OP_PUSH_CONST_U32) 
			CASE(OP_PUSH_CONST_F) ++sp; sp[0].Uns = LoadImm32; FETCH_INSN; NEXT_INSN;

			CASE(OP_DUP) FETCH_INSN; ++sp; sp[0].Int = sp[-1].Int; NEXT_INSN;
			CASE(OP_DROP) FETCH_INSN; --sp; NEXT_INSN;

			CASE(OP_NATIVE)
				{
					int returnSize = LoadImm8;
					int paramCount = (returnSize >> 2) & 63;
					u32 imm = (LoadImm8 << 8);
					imm |= LoadImm8;
					returnSize &= 3;
					Cmd cmd = (Cmd)(u32)pt.Natives[imm];
					// TrapGE(imm,pt.NativeSize);
					// Preserve state so it's correct if the function blocked, and more importantly, in case
					// the function does anything nasty like subclass our thread class and access these directly.
					ser->m_PC = pc-opcodes-c_NativeInsnLength;
					ser->m_FP = fp - stack;
						ser->m_SP = sp - stack + 1;
						Info curInfo(returnSize? &stack[ser->m_SP - paramCount] : 0, paramCount, &stack[ser->m_SP - paramCount]);
#if !__FINAL
					scrThread::SetIsInNativeFunction(true);
#endif
						(*cmd)(curInfo);
#if !__FINAL
					scrThread::SetIsInNativeFunction(false);
#endif
					if (ser->m_State != RUNNING)
							return ser->m_State;
						// If we didn't block, copy referenced parameters back into caller's stack.
						curInfo.CopyReferencedParametersOut();
						// Drop call parameters off of the stack but leave any return result there.
						sp -= paramCount - returnSize;
					FETCH_INSN; 
					NEXT_INSN;
				}

			CASE(OP_ENTER)
				{
					u32 paramCount = LoadImm8;
					u32 localCount = LoadImm16;
					u32 nameCount = LoadImm8;
#if !__FINAL
					if (ser->m_CallDepth < MAX_CALLSTACK)
						ser->m_CallStack[ser->m_CallDepth] = pc - opcodes + 1;
					++(ser->m_CallDepth);
#endif
					pc += nameCount;
					FETCH_INSN; 
					if (sp - stack >= (int)(ser->m_StackSize - localCount))
						Fault("Stack overflow");
					(++sp)->Int = fp - stack;
					fp = sp - paramCount - 1;
					while (localCount--)
						(++sp)->Int = 0;
					sp -= paramCount;
					NEXT_INSN;
				}

			CASE(OP_LEAVE)
				{
					DEC_CALLDEPTH;
					u32 paramCount = LoadImm8;	// Get number of parameters to clean off the stack
					u32 returnSize = LoadImm8;	// Get size of the return value
					scrValue *result = sp - returnSize;	// Remember start of return value
					sp = fp + paramCount - 1;
					fp = stack + sp[2].Uns;
					u32 newPc = sp[1].Uns;		// Pull return address off of the stack placed by OP_CALL/OP_CALLINDIRECT;
					SET_PC(newPc);
					sp -= paramCount;				// Drop the caller's parameters
					// Copy the return value to the new top-of-stack
					while (returnSize--)
						*++sp = *++result;
					if (!newPc)				
						return ser->m_State = ABORTED;
					FETCH_INSN; 
					NEXT_INSN;
				}

			CASE(OP_LOAD) FETCH_INSN; VALIDATE_PTR(sp[0]); sp[0].Reference = sp[0].Reference->Reference; NEXT_INSN;
			CASE(OP_STORE) FETCH_INSN; sp-=2; VALIDATE_PTR(sp[2]); sp[2].Reference->Reference = sp[1].Reference; NEXT_INSN;
			CASE(OP_STORE_REV) FETCH_INSN; --sp; VALIDATE_PTR(sp[0]); sp[0].Reference->Reference = sp[1].Reference; NEXT_INSN;

			CASE(OP_LOAD_N) 
				{ 
					FETCH_INSN; 
					scrValue *addr = (sp--)->Reference;
					u32 count = (sp--)->Int;
					for (u32 i=0; i<count; i++)
						(++sp)->Reference = addr[i].Reference;
					NEXT_INSN; 
				}

			CASE(OP_STORE_N)
				{
					FETCH_INSN; 
					// Values are pushed up to the stack in evaluation order,
					// so reverse them when storing
					scrValue *addr = (sp--)->Reference;
					u32 count = (sp--)->Int;
					for (u32 i=0; i<count; i++)
						addr[count-1-i].Reference = (sp--)->Reference;
					NEXT_INSN; 
				}

			CASE(OP_ARRAY_U8)
				{
					--sp;
					scrValue *r = sp[1].Reference;
					u32 idx = sp[0].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm8;
					FETCH_INSN; 
					sp[0].Reference = r;
					NEXT_INSN;
				}

			CASE(OP_ARRAY_U8_LOAD)
				{
					--sp;
					scrValue *r = sp[1].Reference;
					u32 idx = sp[0].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm8;
					FETCH_INSN; 
					sp[0].Reference = r->Reference;
					NEXT_INSN;
				}

			CASE(OP_ARRAY_U8_STORE)
				{
					sp-=3; 
					scrValue *r = sp[3].Reference;
					u32 idx = sp[2].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm8;
					FETCH_INSN; 
					r->Reference = sp[1].Reference;
					NEXT_INSN;
				}

			CASE(OP_LOCAL_U8) ++sp; sp[0].Reference = fp + LoadImm8; FETCH_INSN; NEXT_INSN;
			CASE(OP_LOCAL_U8_LOAD) ++sp; sp[0].Int = fp[LoadImm8].Int; FETCH_INSN; NEXT_INSN;
			CASE(OP_LOCAL_U8_STORE) --sp; fp[LoadImm8].Int = sp[1].Int; FETCH_INSN; NEXT_INSN;

			CASE(OP_STATIC_U8) ++sp; sp[0].Reference = stack + LoadImm8; FETCH_INSN; NEXT_INSN;
			CASE(OP_STATIC_U8_LOAD) ++sp; sp[0].Int = stack[LoadImm8].Int; FETCH_INSN; NEXT_INSN;
			CASE(OP_STATIC_U8_STORE) --sp; stack[LoadImm8].Int = sp[1].Int; FETCH_INSN; NEXT_INSN;

			CASE(OP_IADD_U8) sp[0].Int += LoadImm8; FETCH_INSN; NEXT_INSN;
			CASE(OP_IADD_U8_LOAD) VALIDATE_PTR(sp[0]); sp[0].Int = *(int*)(sp[0].Uns + LoadImm8); FETCH_INSN; NEXT_INSN;
			CASE(OP_IADD_U8_STORE) sp-=2; VALIDATE_PTR(sp[2]); *(int*)(sp[2].Uns + LoadImm8) = sp[1].Int; FETCH_INSN; NEXT_INSN;
			CASE(OP_IMUL_U8) sp[0].Int *= LoadImm8; FETCH_INSN; NEXT_INSN;

			CASE(OP_PUSH_CONST_S16) ++sp; sp[0].Int = LoadImmS16; FETCH_INSN; NEXT_INSN;
			CASE(OP_IADD_S16) sp[0].Int += LoadImmS16; FETCH_INSN; NEXT_INSN;
			CASE(OP_IADD_S16_LOAD) VALIDATE_PTR(sp[0]); sp[0].Int = *(int*)(sp[0].Uns + LoadImmS16); FETCH_INSN; NEXT_INSN;
			CASE(OP_IADD_S16_STORE) sp-=2; VALIDATE_PTR(sp[2]); *(int*)(sp[2].Uns + LoadImmS16) = sp[1].Int; FETCH_INSN; NEXT_INSN;
			CASE(OP_IMUL_S16) sp[0].Int *= LoadImmS16; FETCH_INSN; NEXT_INSN;

			CASE(OP_ARRAY_U16)
				{
					--sp;
					scrValue *r = sp[1].Reference;
					u32 idx = sp[0].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm16;
					FETCH_INSN; 
					sp[0].Reference = r;
					NEXT_INSN;
				}

			CASE(OP_ARRAY_U16_LOAD)
				{
					--sp;
					scrValue *r = sp[1].Reference;
					u32 idx = sp[0].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm16;
					FETCH_INSN; 
					sp[0].Reference = r->Reference;
					NEXT_INSN;
				}

			CASE(OP_ARRAY_U16_STORE)
				{
					sp-=3; 
					scrValue *r = sp[3].Reference;
					u32 idx = sp[2].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm16;
					FETCH_INSN; 
					r->Reference = sp[1].Reference;
					NEXT_INSN;
				}

			CASE(OP_LOCAL_U16) ++sp; sp[0].Reference = fp + LoadImm16; FETCH_INSN; NEXT_INSN;
			CASE(OP_LOCAL_U16_LOAD) ++sp; sp[0].Int = fp[LoadImm16].Int; FETCH_INSN; NEXT_INSN;
			CASE(OP_LOCAL_U16_STORE) --sp; fp[LoadImm16].Int = sp[1].Int; FETCH_INSN; NEXT_INSN;

			CASE(OP_STATIC_U16) ++sp; sp[0].Reference = stack + LoadImm16; FETCH_INSN; NEXT_INSN;
			CASE(OP_STATIC_U16_LOAD) ++sp; sp[0].Int = stack[LoadImm16].Int; FETCH_INSN; NEXT_INSN;
			CASE(OP_STATIC_U16_STORE) --sp; stack[LoadImm16].Int = sp[1].Int; FETCH_INSN; NEXT_INSN;

			CASE(OP_GLOBAL_U16) ++sp; sp[0].Reference = globals + LoadImm16; FETCH_INSN; NEXT_INSN;
			CASE(OP_GLOBAL_U16_LOAD) ++sp; sp[0].Int = globals[LoadImm16].Int; FETCH_INSN; NEXT_INSN;
			CASE(OP_GLOBAL_U16_STORE) --sp; globals[LoadImm16].Int = sp[1].Int; FETCH_INSN; NEXT_INSN;

			CASE(OP_GLOBAL_U24) ++sp; sp[0].Reference = globals + LoadImm24; FETCH_INSN; NEXT_INSN;
			CASE(OP_GLOBAL_U24_LOAD) ++sp; sp[0].Int = globals[LoadImm24].Int; FETCH_INSN; NEXT_INSN;
			CASE(OP_GLOBAL_U24_STORE) --sp; globals[LoadImm24].Int = sp[1].Int; FETCH_INSN; NEXT_INSN;
			CASE(OP_PUSH_CONST_U24) ++sp; sp[0].Int = LoadImm24; FETCH_INSN; NEXT_INSN;

			CASE(OP_CALL_0) { u32 imm = (0x0 << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_1) { u32 imm = (0x1 << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_2) { u32 imm = (0x2 << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_3) { u32 imm = (0x3 << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_4) { u32 imm = (0x4 << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_5) { u32 imm = (0x5 << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_6) { u32 imm = (0x6 << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_7) { u32 imm = (0x7 << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_8) { u32 imm = (0x8 << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_9) { u32 imm = (0x9 << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_A) { u32 imm = (0xA << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_B) { u32 imm = (0xB << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_C) { u32 imm = (0xC << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_D) { u32 imm = (0xD << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_E) { u32 imm = (0xE << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_CALL_F) { u32 imm = (0xF << 16) | LoadImm16; ++sp; sp[0].Uns = pc - opcodes; SET_PC(imm); FETCH_INSN; NEXT_INSN; }

			CASE(OP_J) { s32 imm = LoadImmS16; ADD_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_JZ) { s32 imm = LoadImmS16; --sp; if (sp[1].Int == 0) ADD_PC(imm); else CHECK_PC; FETCH_INSN; NEXT_INSN; }
			CASE(OP_IEQ_JZ) { s32 imm = LoadImmS16; sp-=2; if (!(sp[1].Int == sp[2].Int)) ADD_PC(imm); else CHECK_PC; FETCH_INSN; NEXT_INSN; }
			CASE(OP_INE_JZ) { s32 imm = LoadImmS16; sp-=2; if (!(sp[1].Int != sp[2].Int)) ADD_PC(imm); else CHECK_PC; FETCH_INSN; NEXT_INSN; }
			CASE(OP_IGE_JZ) { s32 imm = LoadImmS16; sp-=2; if (!(sp[1].Int >= sp[2].Int)) ADD_PC(imm); else CHECK_PC; FETCH_INSN; NEXT_INSN; }
			CASE(OP_IGT_JZ) { s32 imm = LoadImmS16; sp-=2; if (!(sp[1].Int >  sp[2].Int)) ADD_PC(imm); else CHECK_PC; FETCH_INSN; NEXT_INSN; }
			CASE(OP_ILE_JZ) { s32 imm = LoadImmS16; sp-=2; if (!(sp[1].Int <= sp[2].Int)) ADD_PC(imm); else CHECK_PC; FETCH_INSN; NEXT_INSN; }
			CASE(OP_ILT_JZ) { s32 imm = LoadImmS16; sp-=2; if (!(sp[1].Int <  sp[2].Int)) ADD_PC(imm); else CHECK_PC; FETCH_INSN; NEXT_INSN; }

			CASE(OP_SWITCH)
				{
					--sp;
					u32 label = sp[1].Uns;
					u32 count = LoadImm8;
					CHECK_PC;
					for (u32 i=0; i<count; i++) {
						u32 match = LoadImm32;
						u32 target = LoadImm16;
						if (label == match) {
							ADD_PC(target);
							break;
						}
					}
					CHECK_PC;
					FETCH_INSN; 
					NEXT_INSN;
				}
			CASE(OP_STRING)
				{
					sp[0].String = (pt.StringHeap + sp[0].Int);
					FETCH_INSN; 
					NEXT_INSN;
				}
			CASE(OP_NULL) FETCH_INSN; ++sp; sp[0].Reference = &s_Null; NEXT_INSN;

			CASE(OP_TEXT_LABEL_ASSIGN_STRING)
				{
					sp-=2;
					char *dest = const_cast<char*>(sp[2].String);
					const char *src = sp[1].String;
					scr_assign_string(dest,LoadImm8,src);
					FETCH_INSN; 
					NEXT_INSN;
				}

			CASE(OP_TEXT_LABEL_ASSIGN_INT)
				{
					sp-=2;
					char *dest = const_cast<char*>(sp[2].String);
					int value = sp[1].Int;
					scr_itoa(buf,value);
					scr_assign_string(dest,LoadImm8,buf);
					FETCH_INSN; 
					NEXT_INSN;
				}
			CASE(OP_TEXT_LABEL_APPEND_STRING)
				{
					sp-=2;
					char *dest = const_cast<char*>(sp[2].String);
					const char *src = sp[1].String;
					scr_append_string(dest,LoadImm8,src);
					FETCH_INSN; 
					NEXT_INSN;
				}
			CASE(OP_TEXT_LABEL_APPEND_INT)
				{
					sp-=2;
					char *dest = const_cast<char*>(sp[2].String);
					int value = sp[1].Int;
					scr_itoa(buf,value);
					scr_append_string(dest,LoadImm8,buf);
					FETCH_INSN; 
					NEXT_INSN;
				}

			CASE(OP_TEXT_LABEL_COPY)
				{
					FETCH_INSN; 
					sp-=3;
					scrValue *dest = sp[3].Reference;
					int destSize = sp[2].Int;
					int srcSize = sp[1].Int;
					// Remove excess
					while (srcSize > destSize) {
						--srcSize;
						--sp;
					}
					// Do the bulk of the copy
					for (int i=0; i<srcSize; i++)
						dest[srcSize-1-i].Reference = (sp--)->Reference;
					// Make sure it's still NUL-terminated
					char *cDest = (char*)dest;
					cDest[(srcSize*sizeof(scrValue))-1] = '\0';
					NEXT_INSN;
				}

			CASE(OP_CATCH)
				FETCH_INSN; 
				ser->m_CatchPC = pc - opcodes;
				ser->m_CatchFP = fp - stack;
				ser->m_CatchSP = sp - stack + 1;
				++sp;
				sp[0].Int = -1;
				NEXT_INSN;

			CASE(OP_THROW)
				{
					int imm = sp[0].Int;	// caught value (sp is now wrong but will be replaced below)
					if (!ser->m_CatchPC)
						Fault("THROW with no CATCH");
					else {
						SET_PC(ser->m_CatchPC);
						fp = stack + ser->m_CatchFP;
						sp = stack + ser->m_CatchSP;
						sp[0].Int = imm;
					}
					FETCH_INSN; 
					NEXT_INSN;
				}

			CASE(OP_CALLINDIRECT) 
				{ 
					u32 imm = sp[0].Uns; 
					if (!imm)
						Fault("Attempted to call through uninitialized (zero) function pointer");
					sp[0].Uns = pc - opcodes; 
					SET_PC(imm); 
					FETCH_INSN; 
					NEXT_INSN; 
				}

#define COPY_RETURN_0 ++result							// no operation
#define COPY_RETURN_1 *++sp = *++result
#define COPY_RETURN_2 COPY_RETURN_1; COPY_RETURN_1
#define COPY_RETURN_3 COPY_RETURN_2; COPY_RETURN_1

#define IMPL_LEAVE(paramCount,returnSize) { \
			DEC_CALLDEPTH; \
			scrValue *result = sp - returnSize; \
			sp = fp + paramCount - 1; \
			fp = stack + sp[2].Uns; \
			u32 newPc = sp[1].Uns; \
			SET_PC(newPc); \
			sp -= paramCount;	\
			COPY_RETURN_##returnSize; \
			if (!newPc)				\
				return ser->m_State = ABORTED; \
			FETCH_INSN; \
			NEXT_INSN; \
		}

			CASE(OP_LEAVE_0_0) IMPL_LEAVE(0,0)
			CASE(OP_LEAVE_0_1) IMPL_LEAVE(0,1)
			CASE(OP_LEAVE_0_2) IMPL_LEAVE(0,2)
			CASE(OP_LEAVE_0_3) IMPL_LEAVE(0,3)
			CASE(OP_LEAVE_1_0) IMPL_LEAVE(1,0)
			CASE(OP_LEAVE_1_1) IMPL_LEAVE(1,1)
			CASE(OP_LEAVE_1_2) IMPL_LEAVE(1,2)
			CASE(OP_LEAVE_1_3) IMPL_LEAVE(1,3)
			CASE(OP_LEAVE_2_0) IMPL_LEAVE(2,0)
			CASE(OP_LEAVE_2_1) IMPL_LEAVE(2,1)
			CASE(OP_LEAVE_2_2) IMPL_LEAVE(2,2)
			CASE(OP_LEAVE_2_3) IMPL_LEAVE(2,3)
			CASE(OP_LEAVE_3_0) IMPL_LEAVE(3,0)
			CASE(OP_LEAVE_3_1) IMPL_LEAVE(3,1)
			CASE(OP_LEAVE_3_2) IMPL_LEAVE(3,2)
			CASE(OP_LEAVE_3_3) IMPL_LEAVE(3,3)

			CASE(OP_PUSH_CONST_M1) FETCH_INSN; ++sp; sp[0].Int = -1; NEXT_INSN;
			CASE(OP_PUSH_CONST_0) FETCH_INSN; ++sp; sp[0].Int = 0; NEXT_INSN;
			CASE(OP_PUSH_CONST_1) FETCH_INSN; ++sp; sp[0].Int = 1; NEXT_INSN;
			CASE(OP_PUSH_CONST_2) FETCH_INSN; ++sp; sp[0].Int = 2; NEXT_INSN;
			CASE(OP_PUSH_CONST_3) FETCH_INSN; ++sp; sp[0].Int = 3; NEXT_INSN;
			CASE(OP_PUSH_CONST_4) FETCH_INSN; ++sp; sp[0].Int = 4; NEXT_INSN;
			CASE(OP_PUSH_CONST_5) FETCH_INSN; ++sp; sp[0].Int = 5; NEXT_INSN;
			CASE(OP_PUSH_CONST_6) FETCH_INSN; ++sp; sp[0].Int = 6; NEXT_INSN;
			CASE(OP_PUSH_CONST_7) FETCH_INSN; ++sp; sp[0].Int = 7; NEXT_INSN;

			CASE(OP_PUSH_CONST_FM1) FETCH_INSN; ++sp; sp[0].Uns = 0xbf800000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F0) FETCH_INSN; ++sp; sp[0].Uns = 0x00000000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F1) FETCH_INSN; ++sp; sp[0].Uns = 0x3f800000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F2) FETCH_INSN; ++sp; sp[0].Uns = 0x40000000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F3) FETCH_INSN; ++sp; sp[0].Uns = 0x40400000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F4) FETCH_INSN; ++sp; sp[0].Uns = 0x40800000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F5) FETCH_INSN; ++sp; sp[0].Uns = 0x40a00000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F6) FETCH_INSN; ++sp; sp[0].Uns = 0x40c00000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F7) FETCH_INSN; ++sp; sp[0].Uns = 0x40e00000; NEXT_INSN;

			CASE(OP_STRINGHASH) FETCH_INSN; sp[0].Uns = atStringHash(sp[0].String); NEXT_INSN;

			DEFAULT Fault("Invalid opcode");
#if !THREADED_INTERPRETER
		}
#endif
	}
FAULT:
#if DEBUG_TRACE
	int traceCount = s_TraceMax;
	Displayf("Last %d insns:",s_TraceMax);
	while (traceCount--) {
		s_TraceNext = (s_TraceNext - 1) & (s_TraceMax-1);
		scrTrace &T = s_TraceBuf[s_TraceNext];
		Displayf("pc %x opcode %x",T.pc,T.opcode);
	}
#endif

#if !__FINAL && defined(SCRIPT_VALUE_H)
	while (ser->m_CallDepth--)
		if (ser->m_CallDepth < MAX_CALLSTACK)
			Displayf(">>>%s",(char*)(opcodes + ser->m_CallStack[ser->m_CallDepth] + 1));
	Quitf("Script fault, see TTY for details");
#endif
	return ser->m_State = ABORTED;
}

#undef Fault

}	// namespace rage

#endif	// DRIVER


#ifdef STANDALONE

namespace rage { u32 atStringHash(const char*,u32) { return 0; } }

#ifdef __SNC__
#include <sys/time_util.h>
#define SWAP_16(a,b) a,b
#define SWAP_32(a,b,c,d) a,b,c,d
#define TPS 79800000.0f
#elif defined (_XBOX_VER)
extern "C" { unsigned __int64 __mftb(); }
#define SYS_TIMEBASE_GET(x) x = __mftb()
#define SWAP_16(a,b) a,b
#define SWAP_32(a,b,c,d) a,b,c,d
#define TPS 49875000.0f
#else
#define STRICT
#include <windows.h>
#define SYS_TIMEBASE_GET(x)	x = timeGetTime()
#define SWAP_16(a,b) b,a
#define SWAP_32(a,b,c,d) d,c,b,a
#pragma comment(lib,"winmm.lib")
#define TPS 1000.0f
#endif

using namespace rage;

static unsigned char opcodes[] = {
	OP_ENTER,0x00,SWAP_16(0x00,0x03),0x00,			// 0000:
	OP_PUSH_CONST_U8,0,								// 0005:
	OP_LOCAL_U8_STORE,2,							// 0007:
	OP_LOCAL_U8_LOAD,2,								// 0009:
	OP_PUSH_CONST_U32,SWAP_32(0x00,0x98,0x96,0x80), // 000b:
	OP_ILE_JZ,SWAP_16(0x00,0x09),					// 0010: ILE_JZ 0x1C
	OP_LOCAL_U8_LOAD,2,								// 0013:
	OP_IADD_U8,1,									// 0015:
	OP_LOCAL_U8_STORE,2,							// 0017:
	OP_J,SWAP_16(0xFF,0xED),						// 0019: J 0x09
	OP_LEAVE,0x00,0x00,								// 001C:
};
scrThread::Serialized state;
rage::scrValue stack[256], globals[256];

#include <stdio.h>
#include <string.h>

void Test_Run(scrValue *stack,scrValue *sp,scrValue *fp,scrValue *globals)
{
	// OP_ENTER 0, 3, 0
	int paramCount = 0, localCount = 3;
	(++sp)->Int = fp - stack;
	fp = sp - paramCount - 1;
	while (localCount--)
		(++sp)->Int = 0;
	sp -= paramCount;

	// OP_PUSH_CONST_U8
	(++sp)->Int = 0;

	// OP_LOCAL_U8_STORE
	fp[2] = *sp--;

L1:
	// OP_LOCAL_U8_LOAD
	*++sp = fp[2];

	// OP_PUSH_CONST_U32
	(++sp)->Int = 10000000;

	// OP_ILE_JZ
	sp-=2;
	if (sp[1].Int > sp[2].Int) goto L2;

	// OP_LOCAL_U8_LOAD
	*++sp = fp[2];

	// OP_IADD_U8
	sp->Int += 1;

	// OP_LOCAL_U8_STORE
	fp[2] = *sp--;

	goto L1;
L2:
	// don't bother implemented OP_LEAVE.
	;
}

static void __attribute__((noinline)) Test_Run_Asm(scrValue *stack,scrValue *sp,scrValue *fp,scrValue *globals)
//													            r3           r4           r5
{
	__asm(
		"addic %r4,%r4,12\n"

		// OP_PUSH_CONST_U8
		"li %r7,0\n"
		"stwu %r7,4(%r4)\n"

		// OP_LOCAL_U8_STORE
		"lwz %r7,0(%r4)\n"
		"addic %r4,%r4,-4\n"
		"stw %r7,8(%r5)\n"

	"L3:\n"
		// OP_LOCAL_U8_LOAD
		"lwz %r7,8(%r5)\n"
		"stwu %r7,4(%r4)\n"

		// OP_PUSH_CONST_U32
		"lis %r7,0x98\n"
		"ori %r7,%r7,0x9680\n"
		"stwu %r7,4(%r4)\n"

		// OP_ILE_JZ
		"lwz %r7,-4(%r4)\n"
		"lwz %r8,0(%r4)\n"
		"addic %r4,%r4,-8\n"
		"cmpw %r7,%r8\n"
		"bgt L4\n"

		// OP_LOCAL_U8_LOAD
		"lwz %r7,8(%r5)\n"
		"stwu %r7,4(%r4)\n"

		// OP_IADD_U8
		"lwz %r7,0(%r4)\n"
		"addic %r7,%r7,1\n"
		"stw %r7,0(%r4)\n"

		// OP_LOCAL_U8_STORE
		"lwz %r7,0(%r4)\n"
		"addic %r4,%r4,-4\n"
		"stw %r7,8(%r5)\n"

		"b L3\n"
	"L4:\n");
}

int main() {
	unsigned long long start, end;
	unsigned long long best = ~0U;
	const int runs = 5;
	scrProgram p;
	u8 *temp = opcodes;
	p.Table = &temp;
	for (int i=0; i<runs; i++) {
		memset(&state,0,sizeof(state));
		memset(stack,0,sizeof(stack));
		state.m_StackSize = 256;
		stack[state.m_SP++].Int = 0;		// return address
		SYS_TIMEBASE_GET(start);
		// rage::scrThread::Run(stack,globals,p,&state);
		Test_Run_Asm(stack,stack,stack,globals);
		SYS_TIMEBASE_GET(end);
		if (best > (end-start))
			best = end-start;
	}
	printf("%f best over %d runs\n",best / TPS,runs);
	return 0;
}

#endif	// STANDALONE
