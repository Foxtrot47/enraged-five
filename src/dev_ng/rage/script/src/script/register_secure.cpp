#include <stdio.h>
#include <string.h>

typedef unsigned u32;

u32 scrComputeHash(const char *name) {
	// This is the one-at-a-time hash from this page:
	// http://burtleburtle.net/bob/hash/doobs.html
	// (borrowed from /soft/swat/src/swcore/string2key.cpp)
	// (borrowed again from \rage\base\src\string\stringhash.cpp)

	bool quotes = (*name == '\"');
	u32 key = 0;

	if (quotes) 
		name++;

	while (*name && (!quotes || *name != '\"'))
	{
		char character = *name++;
		if (character >= 'A' && character <= 'Z') 
			character += 'a'-'A';
		else if (character == '\\')
			character = '/';

		key += character;
		key += (key << 10);	//lint !e701
		key ^= (key >> 6);	//lint !e702
	}
	key += (key << 3);	//lint !e701
	key ^= (key >> 11);	//lint !e702
	key += (key << 15);	//lint !e701

	if (key < 2) key += 2;	// disallow zero (EMPTY) and one (DELETED)

	return key;
}


void main(int argc,char **argv)
{
	char buf[2048];
	char newbuf[2048];

	FILE *in = fopen(argv[1],"r");
	if (!in) { fprintf(stderr,"bad input"); return; }
	FILE *out = fopen(argv[2],"w");
	if (!out) { fprintf(stderr,"bad output"); return; }

	while (fgets(buf,sizeof(buf),in)) {
		char *b = buf, *o = newbuf;
		while (*b == 32 || *b == 9)
			*o++ = *b++;
		if (!strncmp(b,"SCR_REGISTER(",13)) {
			b += 13;
			strcpy(o,"SCR_REGISTER_SECURE(");
			while (*b == 32 || *b == 9)
				b++;
			const char *sym = b;
			while (*b != ',' && *b != 32 && *b != 9 && *b)
				b++;
			char temp[256];
			memcpy(temp,sym,b-sym);
			temp[b-sym] = 0;
			sprintf(newbuf+strlen(newbuf),"%s, 0x%x", temp, scrComputeHash(temp));
			strcat(newbuf,b);
			strcpy(buf,newbuf);
		}

		fputs(buf,out);
	}
	fclose(in);
	fclose(out);
}
