// 
// script/hash.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "hash.h"

#include "string/stringhash.h"

#if __ASSERT
#include "thread.h"
#endif

namespace rage {

u32 scrComputeHash(const char *name) {
	// This is the one-at-a-time hash from this page:
	// http://burtleburtle.net/bob/hash/doobs.html
	// (borrowed from /soft/swat/src/swcore/string2key.cpp)
	// (borrowed again from \rage\base\src\string\stringhash.cpp)

	bool quotes = (*name == '\"');
	u32 key = 0;

	if (quotes) 
		name++;

	while (*name && (!quotes || *name != '\"'))
	{
		char character = *name++;
		if (character >= 'A' && character <= 'Z') 
			character += 'a'-'A';
		else if (character == '\\')
			character = '/';

		key += character;
		key += (key << 10);	//lint !e701
		key ^= (key >> 6);	//lint !e702
	}
	key += (key << 3);	//lint !e701
	key ^= (key >> 11);	//lint !e702
	key += (key << 15);	//lint !e701

	if (key < 2) key += 2;	// disallow zero (EMPTY) and one (DELETED)

	return key;
}


#if __ASSERT

u64 scrValidateHash(const char *str,u64 hash,const char *file,int line) {
	if (hash == (u32)hash)
	{
		u32 check = scrComputeHash(str);
		Assertf(check == hash,"%s(%d): SCRHASH(%s,0x%016" I64FMT "x) macro out of date, second parameter should be 0x%x",file,line,str,hash,check);
		return check;
	}
	else /* If upper bits are set, then it must be a random number instead */
		return hash;
}
#endif

}	// namespace rage
