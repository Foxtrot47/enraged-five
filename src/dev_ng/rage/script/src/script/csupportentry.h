#ifdef USES_EXCEPTIONS
jmp_buf throw_buffer;
#ifdef __SNC__
#pragma comment(lib,"c")
#endif
#endif

static void scr_assign_string(char *dst,unsigned siz,const char *src)
{
	if (src)
	{
		while (*src && --siz)
			*dst++ = *src++;
	}
	*dst = '\0';
}

static void scr_append_string(char *dst,unsigned siz,const char *src)
{
	while (*dst)
		dst++, --siz;
	scr_assign_string(dst,siz,src);
}

static void scr_itoa(char *dest,int value)
{
	char stack[32], *sp = stack;
	if (value < 0) 
	{
		*dest++ = '-';
		value = -value;
	}
	else if (!value) 
	{
		dest[0] = '0';
		dest[1] = 0;
		return;
	}
	while (value)
	{
		*sp++ = (value % 10) + '0';
		value /= 10;
	}
	while (sp != stack)
	{
		*dest++ = *--sp;
	}
	*dest = 0;
}

static void scr_assign_int(char *dst,unsigned siz,int value)
{
	char temp[32];
	scr_itoa(temp,value);
	scr_assign_string(dst,siz,temp);
}


static void scr_append_int(char *dst,unsigned siz,int value)
{
	char temp[32];
	scr_itoa(temp,value);
	scr_append_string(dst,siz,temp);
}

#ifdef __GNUG__

#include <sys/prx.h>

// This is cut and pasted from Sony headers.  The only reason we redefine it
// is to get rid of the # operator on 'name' so we can pass something that
// is already a string in
#define MY_SYS_MODULE_INFO( name, attribute, major, minor )             \
	SYS_LIB_TABEL_ADDRESS_DEFINE();                                  \
	extern                                                           \
	SceModuleInfo __psp_moduleinfo SYS_MODULE_INFO_SECTION = {       \
	attribute, { minor, major }, name, 0, static_cast<void*>(0), \
	static_cast<const void*>(__begin_of_section_lib_ent+1),      \
	static_cast<const void*>(__end_of_section_lib_ent),          \
	static_cast<const void*>(__begin_of_section_lib_stub+1),     \
	static_cast<const void*>(__end_of_section_lib_stub) };       \
	SYS_LIB_EXPORT_VAR_OTHER_NAME( module_info, __psp_moduleinfo, )

MY_SYS_MODULE_INFO(MODULE_NAME,0,1,1);
SYS_MODULE_START(start_script);

extern "C"
int start_script(unsigned args,script_args *argp) 

#else

extern "C"
__declspec(dllexport) void start_script(unsigned args,script_args *argp)

#endif

{
	globals = (script_globals*) argp->in_globals;
	resolver = argp->in_resolver;
	argp->out_script_def_count = sizeof(defs) / sizeof(defs[0]);
	argp->out_script_defs = defs;

#ifdef __GNUG__
	return SYS_PRX_RESIDENT;
#endif
}

#undef SCRIPT_NAME
