// 
// /scannative.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 
#include "system/main.h"
#include "system/param.h"
#include "data/aes.h"
#include "file/stream.h"
#include "string/string.h"
#include "atl/array.h"

#include "program.h"

using namespace rage;

atArray<u32> hashes;
atArray<char *> lines;
atArray<u32> references;

extern bool g_Validate;

PARAM(edit,"Edit the files to comment out unused functions");

int Main()
{
	if (sysParam::GetArgCount() < 3) {
		Displayf("usage: scannative script_response_file.txt register_response_file.txt [-aeskey xxx] [-edit]");
		Displayf("script_response_file.txt should contain names of all the .sco files you wish to scan");
		Displayf("register_response_file should contain the results of searching your codebase for SCR_REGISTER_SECURE");
		Displayf("Add -edit flag to edit the files and mark them with SCR_DEV_ONLY macro.");
		return 1;
	}

	char buf[1024];
	g_Validate = false;

	// Collect all programs registered by the game.
	fiStream *registers = fiStream::Open(sysParam::GetArg(2));
	if (!registers) {
		Errorf("Cannot open '%s'",sysParam::GetArg(2));
		return 1;
	}

	while (fgetline(buf,sizeof(buf),registers)) {
		if (strstr(buf,"#define"))
			continue;
		else if (!strncmp(buf,"Find all ",8))
			continue;
		else if (!strncmp(buf,"  Matching lines:",17))
			continue;
		else if (strstr(buf,"NOSTRIP") && PARAM_edit.Get())
			continue;
		const char *x = strchr(buf,',');
		const char *slsl = strstr(buf,"//");
		if (!x)
			Warningf("Bad register input '%s'",buf);
		else if (slsl && slsl < x)
			continue;		// commented out
		else {
			u32 hash = strtoul(x+1,NULL,0);
			if (!hash)
				Warningf("Bad hash code in line: %s",buf);
			else if (hashes.Find(hash) != -1)
				Warningf("Duplicate hash code %x in line: %s",hash,buf);
			else {
				hashes.Grow(256) = hash;
				lines.Grow(256) = StringDuplicate(buf);
				references.Grow(256) = 0;
			}
		}
	}
	int total = hashes.GetCount();
	Displayf("%d hashcodes registered by game",total);
	registers->Close();

	// Collect all native functions used by all scripts
	fiStream *scripts = fiStream::Open(sysParam::GetArg(1));
	if (!scripts) {
		Errorf("Cannot open '%s'",sysParam::GetArg(1));
		return 1;
	}

	int referenced = 0;
	while (fgetline(buf,sizeof(buf),scripts)) {
		scrProgramId id = scrProgram::Load(buf);
		if (!id)
			Warningf("Unable to open script '%s'",buf);
		else {
			const scrPageTable &pt = scrProgram::GetProgram(id)->GetOpcodes();
			for (unsigned i=0; i<pt.NativeSize; i++) {
				int idx = hashes.Find(pt.Natives[i]);
				if (idx == -1)
					Warningf("Hash code %x used by script %s not found anywhere?",pt.Natives[i],buf);
				else {
					if (references[idx] == 0)
						referenced++;
					references[idx]++;
				}
			}
			scrProgram::GetProgram(id)->Release();
		}
	}
	scripts->Close();

	Displayf("Functions that are never used:");
	for (int i=0; i<total; i++)
		if (!references[i])
			Displayf("%s",lines[i]);
	/* Displayf("Functions only used once:");
	for (int i=0; i<total; i++)
		if (references[i]==1)
			Displayf("%s",lines[i]); */
	Displayf("%d native functions were used at least once.",referenced);

	if (PARAM_edit.Get()) {
		char lastfile[256] = "";
		fiStream *laststream = 0;
		char *lastcontents = 0;
		int lastsize = 0;
		for (int i=0; i<total; i++) {
			if (!references[i]) {
				char *st = lines[i] + 2;
				char *op = strchr(st,'(');
				char *cp = strchr(op,')');
				if (st && op && cp) {
					*op++ = 0;
					*cp = 0;
					const char *data = cp + 2;
					// new file?
					if (strcmp(lastfile,st)) {
						if (laststream) {
							laststream->Write(lastcontents,lastsize);
							laststream->Close();
						}
						char cmd[256];
						sprintf(cmd,"p4 edit %s",st);
						system(cmd);
						strcpy(lastfile,st);
						laststream = fiStream::Open(lastfile,false);
						if (!laststream)
							Quitf("failed to edit %s",lastfile);
						lastsize = laststream->Size();
						lastcontents = rage_new char[lastsize];
						laststream->Read(lastcontents,lastsize);
						laststream->Seek(0);
					}
					char *line = lastcontents;
					int lineno = 1;
					int matchline = atoi(op);
					while (lineno != matchline) {
						line = strchr(line,'\n');
						if (line) {
							++line;
							++lineno;
						}
						else
							Quitf("unable to find line %d of %s",matchline,lastfile);
					}
					if (strncmp(data,line,strlen(data)))
						Quitf("mismatched line %s of %s",matchline,lastfile);
					char *scr = strstr(line,"SCR_REGISTER_SECURE");
					if (scr) {
						scr[0] = 'O';
						scr[1] = 'P';
						scr[2] = 'T';
					}
					else if ((scr = strstr(line,"AI")) != 0) {
						scr[0] = 'O';
						scr[1] = 'A';
					}
					else
						Quitf("line mismatched?");
				}
			}
		}
		if (laststream) {
			laststream->Write(lastcontents,lastsize);
			laststream->Close();
		}
	}

	return 0;
}