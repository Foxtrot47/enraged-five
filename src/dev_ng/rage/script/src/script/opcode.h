#ifndef SCRIPT_OPCODE_H
#define SCRIPT_OPCODE_H

namespace rage {


/*
	Virtual machine opcode listing.  When adding a rage_new opcode, remember to
	add it to the switch statement in scrThread::Run, the label array at
	the top of scrThread::Disassemble, and add appropriate logic to the
	disassembler itself.
*/
enum scrOpcode {

#define OP(a,b,c) OP_##a
#include "opcodes.h"
#undef OP

	OP_FIRST_INVALID
};

}	// namespace rage

#define INSN_THAT_CAN_BE_AT_END_OF_PAGE(op) (op == OP_NOP || (op == OP_CALL) || op == OP_CALLINDIRECT || op == OP_LEAVE || op == OP_SWITCH || op == OP_THROW || (op >= OP_J && op <= OP_ILE_JZ))

#endif
