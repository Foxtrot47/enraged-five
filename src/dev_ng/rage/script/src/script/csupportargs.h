struct script_def
{
	unsigned arg_struct_size;
	unsigned statics_size;
	unsigned globals_hash;
	const char *script_name;
	void (*script_entry)(scrValue*);
	void (*construct_statics)(scrValue*);
};

struct script_args 
{
	// INPUTS
	scrValue *in_globals;
	scrCmd (*in_resolver)(unsigned);

	// OUTPUTS
	int out_script_def_count;
	script_def **out_script_defs;
};

