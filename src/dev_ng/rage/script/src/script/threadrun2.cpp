#ifndef SCRIPT_VALUE_H

namespace rage {

typedef unsigned u32;
typedef unsigned short u16;
typedef signed short s16;
typedef unsigned char u8;

union scrValue { int Int; float Float; unsigned Uns; scrValue *Reference; const char *String; };

#ifdef __SNC__
struct Vector3 { u32 x, y, z; } __attribute__((aligned(16)));
#else
__declspec(align(16)) struct Vector3 { float x, y, z; };
#endif

class scrThread
{
public:
	struct Info {
		Info(scrValue *resultPtr, int parameterCount, const scrValue *params) :
			ResultPtr(resultPtr), ParamCount(parameterCount), Params(params), BufferCount(0) { }

		// Return result, if applicable
		scrValue *ResultPtr;
		// Parameter count
		int ParamCount;
		// Pointer to parameter values
		const scrValue *Params;

		int BufferCount;
		u32 *Orig[4];
		Vector3 Buffer[4];

		void CopyReferencedParametersOut() { 
			int bc = BufferCount;
			while (bc--) {
				u32 *dst = Orig[bc];
				u32 *src = &Buffer[bc].x;
				dst[0] = src[0]; 
				dst[1] = src[1]; 
				dst[2] = src[2];
			}
		}
	};

	static const int MAX_CALLSTACK = 8;

	struct Serialized {
		void *m_ThreadId;				// +0 Unique thread ID (always increasing)
		void *m_Prog;				// +4 Stored by hashcode rather than pointer so load/save can work
		int m_State;						// +8 Current thread state
		int m_PC;							// +12 Current program counter (index into program's opcodes array)
		int m_FP;							// +16 Current frame pointer (anchor for local variables)
		int m_SP;							// +20 Stack pointer
		int m_TimerA, m_TimerB;				// Local per-thread timer values
		float m_Wait;							// Accumulated wait timer
		int m_MinPC, m_MaxPC;				// Step region for debugger
		scrValue m_TLS[4];		// Thread-local storage for blocking functions
		int m_StackSize;
		int m_CatchPC, m_CatchFP, m_CatchSP;
#if !__FINAL
		int m_CallDepth;
		int m_CallStack[MAX_CALLSTACK];
#endif
	};

	typedef char sizecheck[sizeof(Info)==96? 1 : -1];

	static u32 Run(scrValue * __restrict stack,scrValue * __restrict globals,const u8 * __restrict opcodes,Serialized * __restrict ser);
};

}	// namespace rage

enum State { RUNNING, BLOCKED, ABORTED, HALTED, RESET_INSTRUCTION_COUNT };

inline float scr_fmodf(float x, float y)	{ return y? x - ((int)(x/y) * y) : 0; }

#define Fault(string,...) goto FAULT

#define FULL_LOGGING 0

#ifndef DRIVER
static rage::scrValue s_Null;
#endif

#else
#undef LoadImm32
#undef LoadImm24
#undef LoadImm16
#undef LoadImmS16
#undef LoadImm8

#define Fault(string,...) do { Errorf("FAULT: " string " (%s pc = 0x%x)",##__VA_ARGS__,s_ScriptName,pc - opcodes); goto FAULT; } while (0)

#endif

#define LoadImm32(off)	(*(u32*)(pc+(off)))
#if defined(__SNC__) || defined(_XBOX_VER)
#define LoadImm24(off)	(*(u32*)(pc+off) >> 8)
#else
#define LoadImm24(off)	(*(u32*)(pc+off) & 0xFFFFFF)
#endif
#define LoadImm16(off)	(*(u16*)(pc+(off)))
#define LoadImmS16(off)	(*(s16*)(pc+(off)))
#define LoadImm8(off)	(pc[off])

#include "opcode.h"


#ifndef DRIVER

namespace rage {

typedef void (*Cmd)(scrThread::Info&);

static void scr_assign_string(char *dst,unsigned siz,const char *src)
{
	if (src)
	{
		while (*src && --siz)
			*dst++ = *src++;
	}
	*dst = '\0';
}

static void scr_append_string(char *dst,unsigned siz,const char *src)
{
	while (*dst)
		dst++, --siz;
	scr_assign_string(dst,siz,src);
}

static void scr_itoa(char *dest,int value)
{
	char stack[16], *sp = stack;
	if (value < 0) 
	{
		*dest++ = '-';
		value = -value;
	}
	else if (!value) 
	{
		dest[0] = '0';
		dest[1] = 0;
		return;
	}
	while (value)
	{
		*sp++ = (char)((value % 10) + '0');
		value /= 10;
	}
	while (sp != stack)
	{
		*dest++ = *--sp;
	}
	*dest = 0;
}


#define THREADED_INTERPRETER __PS3		// Both gcc and snc support computed goto's.


#if FULL_LOGGING
u32 scrThread::Run(scrValue * __restrict stack,scrValue * __restrict stackMax,scrValue * __restrict globals,scrValue * __restrict globalsMax,const u8 * __restrict opcodes,const u8 * __restrict opcodesMax,Serialized * __restrict ser)
#else
u32 scrThread::Run(scrValue * __restrict stack,scrValue * __restrict globals,const u8 * __restrict opcodes,Serialized * __restrict ser)
#endif
{

//	--opcodes;	// to save subtracting 1 from pc constantly
	scrValue *sp = stack + ser->m_SP - 1;
	scrValue *fp = stack + ser->m_FP;
	const u8 *pc = (opcodes + ser->m_PC);
	char buf[16];
#if !__FINAL && !THREADED_INTERPRETER
	unsigned insnLimit = 1000000000;
#endif

	// sp[0] is TOS, sp[-1] is next down
	// Push -> *++sp = (x)
	// Pop -> *sp--;
	// unsigned st0, st1, st2, st3;

#if THREADED_INTERPRETER
	static void *jumpTable[256] = {
#define OP(a,b,c) &&LABEL_OP_##a
#include "opcodes.h"
#undef OP
		// This needs to be adjusted if we add or remove opcodes:
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
	};
#define CASE(x) LABEL_##x:
#define DEFAULT LABEL_default:
  void *ni;
# define FETCH_INSN(size)	ni = jumpTable[*(pc += (size))]
# define NEXT_INSN			goto *ni
#else
#define CASE(x)				case x: // printf("%06x: " #x "\n",pc - opcodes);
#define DEFAULT				default:
#define FETCH_INSN(size)	(pc += (size))
#define NEXT_INSN			goto DISPATCH
DISPATCH:
#endif

	{
#if FULL_LOGGING
		LogThreadState(stack,stackMax,globals,globalsMax,sp-stack+1,fp-stack,opcodes+1,opcodesMax,pc-opcodes);
#endif
#if !__FINAL && !THREADED_INTERPRETER
		if (!--insnLimit)
			Fault("Script is stuck in a loop?");
#endif

		// Going into each case, pc is pointing at the current opcode.
		// FETCH_INSN should contain the length of the instruction including its opcode, as this allows
		// us to point to the next opcode.
		// LoadImm... operations accept a bias that is relative to the location of the subsequent opcode.
		// printf("pc %x opcode %x sp %x fp %x\n",pc,opcode,ser->m_SP,ser->m_FP);
		// Displayf("%x: %x SP=%p FP=%p TOS=%x %x %x %x",pc,opcode,sp,ser->m_FP,sp[0].Int,sp[-1].Int,sp[-2].Int,sp[-3].Int);
#if THREADED_INTERPRETER
		FETCH_INSN(0);
		NEXT_INSN;		// start the threaded interpreter
#else
		switch (*pc) 
		{
#endif
			CASE(OP_NOP) FETCH_INSN(1); NEXT_INSN;
			CASE(OP_IADD) FETCH_INSN(1); --sp; sp[0].Int += sp[1].Int; NEXT_INSN;
			CASE(OP_ISUB) FETCH_INSN(1); --sp; sp[0].Int -= sp[1].Int; NEXT_INSN;
			CASE(OP_IMUL) FETCH_INSN(1); --sp; sp[0].Int *= sp[1].Int; NEXT_INSN;
			CASE(OP_IDIV) FETCH_INSN(1); --sp; if (sp[1].Int) sp[0].Int /= sp[1].Int; NEXT_INSN;
			CASE(OP_IMOD) FETCH_INSN(1); --sp; if (sp[1].Int) sp[0].Int %= sp[1].Int; NEXT_INSN;
			CASE(OP_INOT) FETCH_INSN(1); sp[0].Int = !sp[0].Int; NEXT_INSN;
			CASE(OP_INEG) FETCH_INSN(1); sp[0].Int = -sp[0].Int; NEXT_INSN;

			CASE(OP_IEQ) FETCH_INSN(1); --sp; sp[0].Int = sp[0].Int == sp[1].Int; NEXT_INSN;
			CASE(OP_INE) FETCH_INSN(1); --sp; sp[0].Int = sp[0].Int != sp[1].Int; NEXT_INSN;
			CASE(OP_IGE) FETCH_INSN(1); --sp; sp[0].Int = sp[0].Int >= sp[1].Int; NEXT_INSN;
			CASE(OP_IGT) FETCH_INSN(1); --sp; sp[0].Int = sp[0].Int >  sp[1].Int; NEXT_INSN;
			CASE(OP_ILE) FETCH_INSN(1); --sp; sp[0].Int = sp[0].Int <= sp[1].Int; NEXT_INSN;
			CASE(OP_ILT) FETCH_INSN(1); --sp; sp[0].Int = sp[0].Int <  sp[1].Int; NEXT_INSN;

			CASE(OP_FADD) FETCH_INSN(1); --sp; sp[0].Float += sp[1].Float; NEXT_INSN;
			CASE(OP_FSUB) FETCH_INSN(1); --sp; sp[0].Float -= sp[1].Float; NEXT_INSN;
			CASE(OP_FMUL) FETCH_INSN(1); --sp; sp[0].Float *= sp[1].Float; NEXT_INSN;
			CASE(OP_FDIV) FETCH_INSN(1); --sp; if (sp[1].Int) sp[0].Float /= sp[1].Float; NEXT_INSN;
			CASE(OP_FMOD) FETCH_INSN(1); --sp; if (sp[1].Int) sp[0].Float = scr_fmodf(sp[0].Float,sp[1].Float); NEXT_INSN;
			CASE(OP_FNEG) FETCH_INSN(1); sp[0].Uns ^= 0x80000000; NEXT_INSN;

			CASE(OP_FEQ) FETCH_INSN(1); --sp; sp[0].Int = sp[0].Float == sp[1].Float; NEXT_INSN;	// TODO: Use integer compare?
			CASE(OP_FNE) FETCH_INSN(1); --sp; sp[0].Int = sp[0].Float != sp[1].Float; NEXT_INSN;	// TODO: Use integer compare?
			CASE(OP_FGE) FETCH_INSN(1); --sp; sp[0].Int = sp[0].Float >= sp[1].Float; NEXT_INSN;
			CASE(OP_FGT) FETCH_INSN(1); --sp; sp[0].Int = sp[0].Float >  sp[1].Float; NEXT_INSN;
			CASE(OP_FLE) FETCH_INSN(1); --sp; sp[0].Int = sp[0].Float <= sp[1].Float; NEXT_INSN;
			CASE(OP_FLT) FETCH_INSN(1); --sp; sp[0].Int = sp[0].Float <  sp[1].Float; NEXT_INSN;

			CASE(OP_VADD) FETCH_INSN(1); sp-=3; sp[-2].Float += sp[1].Float; sp[-1].Float += sp[2].Float; sp[0].Float += sp[3].Float; NEXT_INSN;
			CASE(OP_VSUB) FETCH_INSN(1); sp-=3; sp[-2].Float -= sp[1].Float; sp[-1].Float -= sp[2].Float; sp[0].Float -= sp[3].Float; NEXT_INSN;
			CASE(OP_VMUL) FETCH_INSN(1); sp-=3; sp[-2].Float *= sp[1].Float; sp[-1].Float *= sp[2].Float; sp[0].Float *= sp[3].Float; NEXT_INSN;
			CASE(OP_VDIV) FETCH_INSN(1); sp-=3; if (sp[1].Int) sp[-2].Float /= sp[1].Float; if (sp[2].Int) sp[-1].Float /= sp[2].Float; if (sp[3].Int) sp[0].Float /= sp[3].Float; NEXT_INSN;
			CASE(OP_VNEG) FETCH_INSN(1); sp[-2].Uns ^= 0x80000000; sp[-1].Uns ^= 0x80000000; sp[0].Uns ^= 0x80000000; NEXT_INSN;

			CASE(OP_IAND) FETCH_INSN(1); --sp; sp[0].Int &= sp[1].Int; NEXT_INSN;
			CASE(OP_IOR)  FETCH_INSN(1); --sp; sp[0].Int |= sp[1].Int; NEXT_INSN;
			CASE(OP_IXOR) FETCH_INSN(1); --sp; sp[0].Int ^= sp[1].Int; NEXT_INSN;

			CASE(OP_I2F) FETCH_INSN(1); sp[0].Float = (float) sp[0].Int; NEXT_INSN;
			CASE(OP_F2I) FETCH_INSN(1); sp[0].Int = (int) sp[0].Float; NEXT_INSN;
			CASE(OP_F2V) FETCH_INSN(1); sp+=2; sp[-1].Int = sp[0].Int = sp[-2].Int; NEXT_INSN;

			CASE(OP_PUSH_CONST_U8) FETCH_INSN(2); ++sp; sp[0].Int = LoadImm8(-1); NEXT_INSN;
			CASE(OP_PUSH_CONST_U8_U8) FETCH_INSN(3); sp+=2; sp[-1].Int = LoadImm8(-2); sp[0].Int = LoadImm8(-1); NEXT_INSN;
			CASE(OP_PUSH_CONST_U8_U8_U8) FETCH_INSN(4); sp+=3; sp[-2].Int = LoadImm8(-3); sp[-1].Int = LoadImm8(-2); sp[0].Int = LoadImm8(-1); NEXT_INSN;
			
			CASE(OP_PUSH_CONST_U32) 
			CASE(OP_PUSH_CONST_F) FETCH_INSN(5); ++sp; sp[0].Uns = LoadImm32(-4); NEXT_INSN;

			CASE(OP_DUP) FETCH_INSN(1); ++sp; sp[0].Int = sp[-1].Int; NEXT_INSN;
			CASE(OP_DROP) FETCH_INSN(1); --sp; NEXT_INSN;

			CASE(OP_NATIVE)
				{
					FETCH_INSN(7);
					int paramCount = LoadImm8(-6);
					int returnSize = LoadImm8(-5);
					u32 imm = LoadImm32(-4);
					Cmd cmd = (Cmd)(u32)imm;
					if (cmd) {
						ser->m_SP = sp - stack + 1;
						Info curInfo(returnSize? &stack[ser->m_SP - paramCount] : 0, paramCount, &stack[ser->m_SP - paramCount]);
						(*cmd)(curInfo);
						if (ser->m_State != RUNNING) {
							// Preserve PC of this opcode
							ser->m_PC = pc-opcodes-7;
							// Preserve FP so it's correct on re-entry.
							ser->m_FP = fp - stack;
							return ser->m_State;
						}
						// If we didn't block, copy referenced parameters back into caller's stack.
						curInfo.CopyReferencedParametersOut();
						// Drop call parameters off of the stack but leave any return result there.
						sp -= paramCount - returnSize;
					}
					NEXT_INSN;
				}

			CASE(OP_ENTER)
				{
					u32 paramCount = LoadImm8(1);
					u32 localCount = LoadImm16(2);
					u32 nameCount = LoadImm8(4);
#if !__FINAL && defined(SCRIPT_VALUE_H)
					if (ser->m_CallDepth < MAX_CALLSTACK)
						ser->m_CallStack[ser->m_CallDepth] = pc - opcodes + 6;
					++(ser->m_CallDepth);
#endif
					FETCH_INSN(5 + nameCount); 
					if (sp - stack >= (int)(ser->m_StackSize - localCount))
						Fault("Stack overflow");
					(++sp)->Int = fp - stack;
					fp = sp - paramCount - 1;
					while (localCount--)
						(++sp)->Int = 0;
					sp -= paramCount;
					NEXT_INSN;
				}

			CASE(OP_LEAVE)
				{
#if !__FINAL && defined(SCRIPT_VALUE_H)
					--(ser->m_CallDepth);
#endif
					u32 paramCount = LoadImm8(1);	// Get number of parameters to clean off the stack
					u32 returnSize = LoadImm8(2);	// Get size of the return value
					scrValue *result = sp - returnSize;	// Remember start of return value
					sp = fp + paramCount - 1;
					fp = stack + sp[2].Uns;
					pc = opcodes + sp[1].Uns;		// Pull return address off of the stack placed by OP_CALL/OP_CALLINDIRECT
					sp -= paramCount;				// Drop the caller's parameters
					// Copy the return value to the new top-of-stack
					while (returnSize--)
						*++sp = *++result;
					if (pc == opcodes)				
						return ser->m_State = ABORTED;
					FETCH_INSN(0);
					NEXT_INSN;
				}

			CASE(OP_LOAD) FETCH_INSN(1); sp[0].Reference = sp[0].Reference->Reference; NEXT_INSN;
			CASE(OP_STORE) FETCH_INSN(1); sp-=2; sp[2].Reference->Reference = sp[1].Reference; NEXT_INSN;
			CASE(OP_STORE_REV) FETCH_INSN(1); --sp; sp[0].Reference->Reference = sp[1].Reference; NEXT_INSN;

			CASE(OP_LOAD_N) 
				{ 
					FETCH_INSN(1); 
					scrValue *addr = (sp--)->Reference;
					u32 count = (sp--)->Int;
					for (u32 i=0; i<count; i++)
						(++sp)->Reference = addr[i].Reference;
					NEXT_INSN; 
				}

			CASE(OP_STORE_N)
				{
					FETCH_INSN(1); 
					// Values are pushed up to the stack in evaluation order,
					// so reverse them when storing
					scrValue *addr = (sp--)->Reference;
					u32 count = (sp--)->Int;
					for (u32 i=0; i<count; i++)
						addr[count-1-i].Reference = (sp--)->Reference;
					NEXT_INSN; 
				}

			CASE(OP_ARRAY_U8)
				{
					FETCH_INSN(2); 
					--sp;
					scrValue *r = sp[1].Reference;
					u32 idx = sp[0].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm8(-1);
					sp[0].Reference = r;
					NEXT_INSN;
				}

			CASE(OP_ARRAY_U8_LOAD)
				{
					FETCH_INSN(2);
					--sp;
					scrValue *r = sp[1].Reference;
					u32 idx = sp[0].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm8(-1);
					sp[0].Reference = r->Reference;
					NEXT_INSN;
				}

			CASE(OP_ARRAY_U8_STORE)
				{
					FETCH_INSN(2);
					sp-=3; 
					scrValue *r = sp[3].Reference;
					u32 idx = sp[2].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm8(-1);
					r->Reference = sp[1].Reference;
					NEXT_INSN;
				}

			CASE(OP_LOCAL_U8) FETCH_INSN(2); ++sp; sp[0].Reference = fp + LoadImm8(-1); NEXT_INSN;
			CASE(OP_LOCAL_U8_LOAD) FETCH_INSN(2); ++sp; sp[0].Int = fp[LoadImm8(-1)].Int; NEXT_INSN;
			CASE(OP_LOCAL_U8_STORE) FETCH_INSN(2); --sp; fp[LoadImm8(-1)].Int = sp[1].Int; NEXT_INSN;

			CASE(OP_STATIC_U8) FETCH_INSN(2); ++sp; sp[0].Reference = stack + LoadImm8(-1); NEXT_INSN;
			CASE(OP_STATIC_U8_LOAD) FETCH_INSN(2); ++sp; sp[0].Int = stack[LoadImm8(-1)].Int; NEXT_INSN;
			CASE(OP_STATIC_U8_STORE) FETCH_INSN(2); --sp; stack[LoadImm8(-1)].Int = sp[1].Int; NEXT_INSN;

			CASE(OP_IADD_U8) FETCH_INSN(2); sp[0].Int += LoadImm8(-1); NEXT_INSN;
			CASE(OP_IADD_U8_LOAD) FETCH_INSN(2); sp[0].Int = *(int*)(sp[0].Uns + LoadImm8(-1)); NEXT_INSN;
			CASE(OP_IADD_U8_STORE) FETCH_INSN(2); sp-=2; *(int*)(sp[2].Uns + LoadImm8(-1)) = sp[1].Int; NEXT_INSN;
			CASE(OP_IMUL_U8) FETCH_INSN(2); sp[0].Int *= LoadImm8(-1); NEXT_INSN;

			CASE(OP_PUSH_CONST_S16) FETCH_INSN(3); ++sp; sp[0].Int = LoadImmS16(-2); NEXT_INSN;
			CASE(OP_IADD_S16) FETCH_INSN(3); sp[0].Int += LoadImmS16(-2); NEXT_INSN;
			CASE(OP_IADD_S16_LOAD) FETCH_INSN(3); sp[0].Int = *(int*)(sp[0].Uns + LoadImmS16(-2)); NEXT_INSN;
			CASE(OP_IADD_S16_STORE) FETCH_INSN(3); sp-=2; *(int*)(sp[2].Uns + LoadImmS16(-2)) = sp[1].Int; NEXT_INSN;
			CASE(OP_IMUL_S16) FETCH_INSN(3); sp[0].Int *= LoadImmS16(-2); NEXT_INSN;

			CASE(OP_ARRAY_U16)
				{
					FETCH_INSN(3);
					--sp;
					scrValue *r = sp[1].Reference;
					u32 idx = sp[0].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm16(-2);
					sp[0].Reference = r;
					NEXT_INSN;
				}

			CASE(OP_ARRAY_U16_LOAD)
				{
					FETCH_INSN(3);
					--sp;
					scrValue *r = sp[1].Reference;
					u32 idx = sp[0].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm16(-2);
					sp[0].Reference = r->Reference;
					NEXT_INSN;
				}

			CASE(OP_ARRAY_U16_STORE)
				{
					FETCH_INSN(3);
					sp-=3; 
					scrValue *r = sp[3].Reference;
					u32 idx = sp[2].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm16(-2);
					r->Reference = sp[1].Reference;
					NEXT_INSN;
				}

			CASE(OP_LOCAL_U16) FETCH_INSN(3); ++sp; sp[0].Reference = fp + LoadImm16(-2); NEXT_INSN;
			CASE(OP_LOCAL_U16_LOAD) FETCH_INSN(3); ++sp; sp[0].Int = fp[LoadImm16(-2)].Int; NEXT_INSN;
			CASE(OP_LOCAL_U16_STORE) FETCH_INSN(3); --sp; fp[LoadImm16(-2)].Int = sp[1].Int; NEXT_INSN;

			CASE(OP_STATIC_U16) FETCH_INSN(3); ++sp; sp[0].Reference = stack + LoadImm16(-2); NEXT_INSN;
			CASE(OP_STATIC_U16_LOAD) FETCH_INSN(3); ++sp; sp[0].Int = stack[LoadImm16(-2)].Int; NEXT_INSN;
			CASE(OP_STATIC_U16_STORE) FETCH_INSN(3); --sp; stack[LoadImm16(-2)].Int = sp[1].Int; NEXT_INSN;

			CASE(OP_GLOBAL_U16) FETCH_INSN(3); ++sp; sp[0].Reference = globals + LoadImm16(-2); NEXT_INSN;
			CASE(OP_GLOBAL_U16_LOAD) FETCH_INSN(3); ++sp; sp[0].Int = globals[LoadImm16(-2)].Int; NEXT_INSN;
			CASE(OP_GLOBAL_U16_STORE) FETCH_INSN(3); --sp; globals[LoadImm16(-2)].Int = sp[1].Int; NEXT_INSN;

			CASE(OP_GLOBAL_U24) FETCH_INSN(4); ++sp; sp[0].Reference = globals + LoadImm24(-3); NEXT_INSN;
			CASE(OP_GLOBAL_U24_LOAD) FETCH_INSN(4); ++sp; sp[0].Int = globals[LoadImm24(-3)].Int; NEXT_INSN;
			CASE(OP_GLOBAL_U24_STORE) FETCH_INSN(4); --sp; globals[LoadImm24(-3)].Int = sp[1].Int; NEXT_INSN;

			CASE(OP_CALL) { u32 imm = LoadImm24(1); ++sp; sp[0].Uns = pc - opcodes + 4; pc = opcodes + imm; FETCH_INSN(0); NEXT_INSN; }

			CASE(OP_J) { u32 imm = LoadImm24(1); pc = opcodes + imm; FETCH_INSN(0); NEXT_INSN; }
			CASE(OP_JZ) { u32 imm = LoadImm24(1); --sp; if (sp[1].Int == 0) pc = opcodes + imm; else pc += 4; FETCH_INSN(0); NEXT_INSN; }
			CASE(OP_IEQ_JZ) { u32 imm = LoadImm24(1); sp-=2; if (!(sp[1].Int == sp[2].Int)) pc = opcodes + imm; else pc += 4; FETCH_INSN(0); NEXT_INSN; }
			CASE(OP_INE_JZ) { u32 imm = LoadImm24(1); sp-=2; if (!(sp[1].Int != sp[2].Int)) pc = opcodes + imm; else pc += 4; FETCH_INSN(0); NEXT_INSN; }
			CASE(OP_IGE_JZ) { u32 imm = LoadImm24(1); sp-=2; if (!(sp[1].Int >= sp[2].Int)) pc = opcodes + imm; else pc += 4; FETCH_INSN(0); NEXT_INSN; }
			CASE(OP_IGT_JZ) { u32 imm = LoadImm24(1); sp-=2; if (!(sp[1].Int >  sp[2].Int)) pc = opcodes + imm; else pc += 4; FETCH_INSN(0); NEXT_INSN; }
			CASE(OP_ILE_JZ) { u32 imm = LoadImm24(1); sp-=2; if (!(sp[1].Int <= sp[2].Int)) pc = opcodes + imm; else pc += 4; FETCH_INSN(0); NEXT_INSN; }
			CASE(OP_ILT_JZ) { u32 imm = LoadImm24(1); sp-=2; if (!(sp[1].Int <  sp[2].Int)) pc = opcodes + imm; else pc += 4; FETCH_INSN(0); NEXT_INSN; }

			CASE(OP_SWITCH)
				{
					--sp;
					u32 label = sp[1].Uns;
					u32 count = LoadImm8(1);
					const u8 *base = pc+2;
					pc += 2 + 7 * count;	// Skip table by default
					for (u32 i=0; i<count; i++, base+=7) {
						u32 match = *(u32*)(base);
#if defined(__SNC__) || defined(_XBOX_VER)
						u32 target = *(u32*)(base+4) >> 8;
#else
						u32 target = *(u32*)(base+4) & 0xFFFFFF;
#endif
						if (label == match) {
							pc = opcodes + target;
							break;
						}
					}
					FETCH_INSN(0);
					NEXT_INSN;
				}
			CASE(OP_STRING)
				{
					u32 imm = LoadImm8(1);
					++sp;
					sp[0].Int = (int)(pc+2);
					FETCH_INSN(imm + 2);
					NEXT_INSN;
				}
			CASE(OP_DATATABLE)
				{
					++sp;
					sp[0].Int = (int)(pc+1);
					u32 imm = LoadImm32(1);
					FETCH_INSN(imm + 5);
					NEXT_INSN;
				}
			CASE(OP_NULL) FETCH_INSN(1); ++sp; sp[0].Reference = &s_Null; NEXT_INSN;

			CASE(OP_TEXT_LABEL_ASSIGN_STRING)
				{
					FETCH_INSN(2); 
					sp-=2;
					char *dest = const_cast<char*>(sp[2].String);
					const char *src = sp[1].String;
					scr_assign_string(dest,LoadImm8(-1),src);
					NEXT_INSN;
				}

			CASE(OP_TEXT_LABEL_ASSIGN_INT)
				{
					FETCH_INSN(2); 
					sp-=2;
					char *dest = const_cast<char*>(sp[2].String);
					int value = sp[1].Int;
					scr_itoa(buf,value);
					scr_assign_string(dest,LoadImm8(-1),buf);
					NEXT_INSN;
				}
			CASE(OP_TEXT_LABEL_APPEND_STRING)
				{
					FETCH_INSN(2);
					sp-=2;
					char *dest = const_cast<char*>(sp[2].String);
					const char *src = sp[1].String;
					scr_append_string(dest,LoadImm8(-1),src);
					NEXT_INSN;
				}
			CASE(OP_TEXT_LABEL_APPEND_INT)
				{
					FETCH_INSN(2); 
					sp-=2;
					char *dest = const_cast<char*>(sp[2].String);
					int value = sp[1].Int;
					scr_itoa(buf,value);
					scr_append_string(dest,LoadImm8(-1),buf);
					NEXT_INSN;
				}

			CASE(OP_TEXT_LABEL_COPY)
				{
					FETCH_INSN(1); 
					sp-=3;
					scrValue *dest = sp[3].Reference;
					int destSize = sp[2].Int;
					int srcSize = sp[1].Int;
					// Remove excess
					while (srcSize > destSize) {
						--srcSize;
						--sp;
					}
					// Do the bulk of the copy
					for (int i=0; i<srcSize; i++)
						dest[srcSize-1-i].Reference = (sp--)->Reference;
					// Make sure it's still NUL-terminated
					char *cDest = (char*)dest;
					cDest[(srcSize*sizeof(scrValue))-1] = '\0';
					NEXT_INSN;
				}

			CASE(OP_CATCH)
				FETCH_INSN(1);
				ser->m_CatchPC = pc - opcodes;
				ser->m_CatchFP = fp - stack;
				ser->m_CatchSP = sp - stack + 1;
				++sp;
				sp[0].Int = -1;
				NEXT_INSN;

			CASE(OP_THROW)
				{
					int imm = sp[0].Int;	// caught value (sp is now wrong but will be replaced below)
					if (!ser->m_CatchPC)
						Fault("THROW with no CATCH");
					else {
						pc = opcodes + ser->m_CatchPC;
						fp = stack + ser->m_CatchFP;
						sp = stack + ser->m_CatchSP;
						sp[0].Int = imm;
					}
					FETCH_INSN(0); 
					NEXT_INSN;
				}

			CASE(OP_CALLINDIRECT) 
				{ 
					u32 imm = sp[0].Uns; 
					if (!imm)
						Fault("Attempted to call through uninitialized (zero) function pointer");
					sp[0].Uns = pc - opcodes + 1; 
					pc = opcodes + imm;
					FETCH_INSN(0); 
					NEXT_INSN; 
				}

			DEFAULT Fault("Invalid opcode");
#if !THREADED_INTERPRETER
		}
#endif
	}
FAULT:
#if !__FINAL && defined(SCRIPT_VALUE_H)
	while (ser->m_CallDepth--)
		if (ser->m_CallDepth < MAX_CALLSTACK)
			Displayf(">>>%s",(char*)(opcodes + ser->m_CallStack[ser->m_CallDepth]));
	Quitf("Script fault, see TTY for details");
#endif
	return ser->m_State = ABORTED;
}

#undef Fault

}	// namespace rage

#endif	// DRIVER


#ifdef STANDALONE

#ifdef __SNC__
#include <sys/time_util.h>
#define SWAP_16(a,b) a,b
#define SWAP_24(a,b,c) a,b,c
#define SWAP_32(a,b,c,d) a,b,c,d
#define TPS 79800000.0f
#elif defined (_XBOX_VER)
extern "C" { unsigned __int64 __mftb(); }
#define SYS_TIMEBASE_GET(x) x = __mftb()
#define SWAP_16(a,b) a,b
#define SWAP_24(a,b,c) a,b,c
#define SWAP_32(a,b,c,d) a,b,c,d
#define TPS 49875000.0f
#else
#define STRICT
#include <windows.h>
#define SYS_TIMEBASE_GET(x)	x = timeGetTime()
#define SWAP_16(a,b) b,a
#define SWAP_24(a,b,c) c,b,a
#define SWAP_32(a,b,c,d) d,c,b,a
#pragma comment(lib,"winmm.lib")
#define TPS 1000.0f
#endif

using namespace rage;

static unsigned char opcodes[] = {
	OP_ENTER,0x00,SWAP_16(0x00,0x03),0x00,			// 0000:
	OP_PUSH_CONST_U8,0,								// 0005:
	OP_LOCAL_U8_STORE,2,							// 0007:
	OP_LOCAL_U8_LOAD,2,								// 0009:
	OP_PUSH_CONST_U32,SWAP_32(0x00,0x98,0x96,0x80), // 000b:
	OP_ILE_JZ,SWAP_24(0x00,0x00,0x1e),				// 0010:
	OP_LOCAL_U8_LOAD,2,								// 0014:
	OP_IADD_U8,1,									// 0016:
	OP_LOCAL_U8_STORE,2,							// 0018:
	OP_J,SWAP_24(0x00,0x00,0x09),					// 001A: J 0x000008
	OP_LEAVE,0x00,0x00,								// 001E: LEAVE 0,0
};
scrThread::Serialized state;
rage::scrValue stack[256], globals[256];

#include <stdio.h>
#include <string.h>

int main() {
	unsigned long long start, end;
	unsigned long long accum = 0;
	const int runs = 5;
	for (int i=0; i<runs; i++) {
		memset(&state,0,sizeof(state));
		memset(stack,0,sizeof(stack));
		stack[state.m_SP++].Int = 0;		// return address
		SYS_TIMEBASE_GET(start);
		rage::scrThread::Run(stack,globals,opcodes,&state);
		SYS_TIMEBASE_GET(end);
		accum += (end-start);
	}
	printf("%f avg over %d runs\n",accum / runs / TPS,runs);
	return 0;
}

#endif	// STANDALONE
