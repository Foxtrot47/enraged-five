// 
// script/hash.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SCRIPT_HASH_H
#define SCRIPT_HASH_H

#include "system/new.h"

namespace rage {

u32 scrComputeHash(const char *name);

template <class T,int initSize = 0> class scrHash {
	struct Slot { u32 code; T data; };

	enum { EMPTY, DELETED };
public:

	scrHash() : m_Slots(NULL), m_InitSize(initSize) { }

	void Init(int size) {
		m_Slots = rage_new Slot[m_Size = size];
		for (int i=0; i<size; i++)
			m_Slots[i].code = 0;
		m_Occupancy = 0;
	}

	void Kill() {
		delete[] m_Slots;
		m_Slots = NULL;
	}

	~scrHash() { Kill(); }

	void Delete() {
		// This invokes delete on every non-empty slot.
		int size = m_Size;
		for (int i=0; i<size; i++) {
			if (m_Slots[i].code) {
				m_Slots[i].code = 0;
				delete m_Slots[i].data;
			}
		}
		m_Occupancy = 0;
	}

	T Lookup(u32 hashCode) {
		if (!m_Size)
			return NULL;
		FastAssert(hashCode);
		u32 slot = hashCode % m_Size;
		u32 delta = hashCode;
		u32 thisCode;
		u32 deadman = m_Size * 2;
		while ((thisCode = m_Slots[slot].code) != hashCode && thisCode && --deadman) {
			delta = (delta >> 1) + 1;
			slot = (slot + delta) % m_Size;
		}
		if (!thisCode)
			return NULL;
		else
			return m_Slots[slot].data;
	}

	bool Insert(u32 hashCode,T data) {
		if (!m_Size)
			Init(m_InitSize);
		if (m_Occupancy == m_Size)
			return false;
		FastAssert(hashCode);
		u32 slot = hashCode % m_Size;
		u32 delta = hashCode;
		while (m_Slots[slot].code > DELETED) {
			if (m_Slots[slot].code == hashCode)
				return false;
			delta = (delta >> 1) + 1;
			slot = (slot + delta) % m_Size;
		}
		m_Slots[slot].code = hashCode;
		m_Slots[slot].data = data;
		++m_Occupancy;
		return true;
	}

	bool Remove(u32 hashCode) {
		u32 slot = hashCode % m_Size;
		u32 delta = hashCode;
		while (m_Slots[slot].code) {
			if (m_Slots[slot].code == hashCode) {
				m_Slots[slot].code = DELETED;
				m_Slots[slot].data = NULL;
				--m_Occupancy;
				return true;
			}
			delta = (delta >> 1) + 1;
			slot = (slot + delta) % m_Size;
		}
		return false;
	}

private:
	Slot *m_Slots;
	int m_Size, m_InitSize, m_Occupancy;
};

}	// namespace rage

#endif	// SCRIPT_HASH_H
