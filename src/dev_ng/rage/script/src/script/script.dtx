@@User Manual
The scripting language is based upon the Rockstar North GTA
scripting language. It is a simple imperative language which
looks a lot like BASIC.

@@Comments
<GROUP User Manual>
You can do comments to end-of-line with //. You can bracket
multi-line comments in /* and */.

@@Variable Declarations
<GROUP User Manual>
\Note that our lexer is not case-sensitive. You can use
GET_ACTOR_POSITION or get_actor_position interchangeably.

All variable declarations must start with a typename. The
common typenames are:
  * <c>INT</c> - Basic integer type (whole numbers only).
  * <c>FLOAT</c> - Basic floating-point type (decimal
    numbers).
  * <c>BOOL</c> - A special variable that only holds either
    TRUE or FALSE. The value of any expression used in IF or
    WHILE should be a BOOL.
  * <c>TEXT_LABEL</c> - A short string up to fifteen letters
    \long. Typically used for referencing a localized string.
    TEXT_LABEL_3, TEXT_LABEL_7, TEXT_LABEL_15, TEXT_LABEL_23, 
    TEXT_LABEL_31, and TEXT_LABEL_63 also exist.  When passing 
    TEXT_LABEL variants around to functions, their size must match 
    exactly; consider using a STRING type instead if you only 
    need to read the text.
  * <c>VECTOR</c> - A structure containing X, Y, and Z fields
    which represent a floating-point position in space.
Any variable declared within a SCRIPT / ENDSCRIPT, PROC /
ENDPROC, or FUNC / ENDFUNC block is local and is unique to
that particular block (strictly speaking it is unique to the
thread owning the script, so if the same script is attached
to multiple objects they all have their own local variables).

Variables may be declared in a special GLOBALS / ENDGLOBALS
block, in which case they are true global variables that are
shared amongst all scripts. There can only be one GLOBALS
block per script; it is intended to be placed inside a USING
\file so that all scripts can see the same master list of
globals. In fact, there can only be a single GLOBALS block
for all scripts in the game; this keeps all globals in a
central place and allows the compiler to still assign static
addresses.

Any variable declared outside any blocks and also not within
a GLOBALS / ENDGLOBALS block is still unique to that copy of
that script, but is available to any SCRIPT, FUNC, or PROC
block after its declaration; this is like a normal member
variable in C++.

The script language supports basic function and procedure
pointers through the use of the TYPEDEF directive and the
ampersand operator.  To actually call through such a pointer,
apply the CALL directive.

<CODE>
TYPEDEF FUNC INT callback1(INT A,INT B)

TYPEDEF PROC callback2(FLOAT C)

FUNC INT my_callback(INT A,INT B)
	RETURN A + B
ENDFUNC

PROC my_callback2(FLOAT A)
	PRINTFLOAT (A)
ENDPROC
	
SCRIPT
	callback1 cb1 = &my_callback
	
	callback2 cb2 = &my_callback2
	
	INT C = CALL cb1(10,20)
	
	PRINTINT(C)
	PRINTNL()
	
	CALL cb2(12.34)
ENDSCRIPT
</CODE>

All variables are initialized to zero on creation. You may
explicitly initialize any local variable to any valid
expression. You can declare more than one variable at once by
separating the names with commas.
<code>
INT A, B // Declare two local variables initialize to zero
FLOAT C = 1.23 // Declare a local variable and initialize it.
VECTOR Pos = \<\< C+2, 5, 2 \>\> // Note the special syntax for vector literals
</code>
Global and member variables can be initialized as well, but
\only to constant expressions (if they are integers) or
constant numbers (if they are floating-point).

When you wish to change the value of a variable after its
creation you just assign a new value:
<code>
A = A + 1
Pos.y = Pos.y + 9.8
</code>

You cannot assign directly between INT and FLOAT variables.
To assign a FLOAT to an INT, call the CEIL, FLOOR, or ROUND
function. To assign an INT to a FLOAT, call the TO_FLOAT
function.

The ++, --, +=, -=, *=, and /= shortcuts from C are
available, but they are treated as statements, not
expressions. Therefore there is no distinction between
preincrement or postincrement (since it is impossible to
reference the value in an outer expression) and either form
is accepted.

You can also use the HASH("STRING LITERAL") operator in
expressions and enumerant definitions to compute the standard
rage string hash of a string literal at compile time.

@@Constant Declarations
<GROUP User Manual>
You can introduce new constants into the language via the
CONST_INT and CONST_FLOAT directives. CONST_INT variables in
particular can be used anywhere the compiler wants a constant
integer, even in CASE labels and array size declarations. You
cannot declare more than one constant at a time.
<CODE>
CONST_INT MaxCount 5
CONST_FLOAT PI 3.14159
</CODE>

There are variations of this, TWEAK_INT and TWEAK_FLOAT, which
are identical to CONST_INT and CONST_FLOAT in final builds, but
turn into normal INT and FLOAT (ie modifiable) variables in
debug builds.  TWEAK_INT and TWEAK_FLOAT must appear at file
scope but not within a GLOBALS block.

@@Preprocessor
<GROUP User Manual>
There is a very crude preprocessor in the script compiler.
It understands #ifdef/#endif and #ifndef/#endif blocks.
They may not currently be nested, and #else is not supported
either.  They check if the symbol (of any type) is defined.
The intended use is to allow a crude form of inheritance;
you can place a standard event loop in a header file, and you
can put default implementations of functions that the user may
have overridden (explicitly defined themselves prior to
inclusion) within #ifndef blocks.

#if is also supported, and differs in that it will issue an
error if it doesn't understand the symbol.  This is to catch
typos where possible.  You can also apply the NOT operator
to the symbol being tested, and the DEFINED operator will work
as well as alternate syntax

<CODE>
#IFDEF SIMPLE_MODE // is symbol defined?
#IF DEFINED SIMPLE_MODE // is symbol defined?
#IF NOT DEFINED SIMPLE_MODE // is symbol NOT defined?
#IF DEBUG_LEVEL // does symbol exist and evaluate to nonzero?
</CODE>

@@Advanced Variable Declarations
<GROUP User Manual>
Some commands declare their own special handle types (like
GUID). You can use these types to declare variables; however,
you can only compare and assign them to variables of the
exact same handle type. As a special case, the special
variable NULL is compatible with all handle types, and is
also the default value of any handle variable not explicitly
initialized to something in particular.

The special handle types are declared via the NATIVE keyword,
which also supports a very simple form of polymorphism.

<CODE>
NATIVE OBJECT
NATIVE ACTOR : OBJECT
NATIVE VOLUME : OBJECT
</CODE>

Any subprogram that accepts an OBJECT can also accept an ACTOR
or VOLUME instead.

The purpose of the handle types is to protect you from
passing in the wrong type of object into a command.

You can also declare multi-dimensional arrays of objects.
Array elements are accessed starting at element zero. Arrays
can be useful for holding lists of data such as waypoints or
item prices.  Arrays have their bounds checked automatically
at runtime.  You can also pass an array to the COUNT_OF operator
to determine the number of elements in the array at runtime,
even if it was a variable-length array:

<CODE>
FUNC INT ADDER(INT &inputs[])
    INT SUM, I
    FOR I=0 TO COUNT_OF(input)-1
        SUM += input[I]
   ENDFOR
  RETURN SUM
ENDFUNC
</CODE>

You can introduce new structures (like VECTOR already is) with:
<CODE>
STRUCT PlayerData
	INT AttackState, Health
	INT AMMO = 100
	FLOAT Chances[4]
ENDSTRUCT
</CODE>
Structures may contain basic types or even previously defined nested structures.

You can initialize simple INT and FLOAT members of STRUCT's and they will be 
constructed with that value instead of the default of zero.

I added structures in anticipation
of minigames being written in the scripting language; I didn't
want to see lots of parallel arrays being used everywhere.

@@Special Functions and Commands
<GROUP User Manual>
The TIMERA and TIMERB variables are special INT functions
available to every script. They are incremented by the
current frame step whenever the script is running; for
backward compatibility, they are measured in milliseconds.
You can change their values directly with the SETTIMERA and
SETTIMERB commands.

The TIMESTEP variable is a special FLOAT function available
to all scripts; it contains the last time step (typically one
\or two vertical blanking intervals). The TIMESTEP variable
is used by the special +@ and -@ operators which automatically
multiply the right-hand side by the frame time.

The WAIT command accepts a single int parameter which is the
number of milliseconds to block the currently running script.
WAIT 0 will always block the script for the remainder of the
current game frame.

Currently the scripting system executes 1000 instructions per frame. 
If your script doesnt pause then it will run round and round its 
main loop until these are all used up (so stopping any other 
scripts running until the next frame). 

<CODE> 
script
	//Main loop
	WHILE true
		// Do My Stuff Please Mr Script Engine
		WAIT 0
	ENDWHILE
endscript
</CODE>
 
You can also wait at any other point in the script.

In order to use the TIMERA, TIMERB, SETTIMERA, SETTIMERB, and
WAIT commands, you must have an appropriate USING statement
in your declaration section.  For RDR2 this is

<CODE>
USING "../../scripting/builtins.sc"
</CODE>

As mentioned elsewhere, NULL is a special variable which is
compatible with all handle types.

Basic exception handling (one level only, intended for restarts and
such) is also available.  CATCH is a builtin function which returns
-1 the first time you call it; internally it takes a snapshot of
the current thread location (stack pointer, etc).  When you later
execute the THROW statement (either via THROW on its own, which does
an implicit THROW(-1), or with an explicit expression passed in)
the virtual machine pops back to the CATCH function except that it
returns whatever THROW gave it.

This is more general than GOTO because it works even deep inside
user procedures and functions, and it can also be triggered by the
game engine itself.  It's intended for things like event restart.

Each script can only have one active CATCH, they will not nest.

<CODE>
SCRIPT
	DO_INITIAL_SETUP()
	
	INT STATE = CATCH
	IF STATE = 123
		PRINTSTRING("Did a restart?\n")
	ENDIF
	
	WHILE TRUE
		THROW(123)
	ENDWHILE
ENDSCRIPT
</CODE>

@@Text Labels
<GROUP User Manual>
A TEXT_LABEL is a mutable string buffer which can hold up to 15 characters.
Its intended use is for dynamically constructing localization label names
or filenames.  A TEXT_LABEL can be passed into any command that accepts a
string.  TEXT_LABEL variables are initialized to an empty string by default.

You can also do very limited assignments and append operations to TEXT_LABEL
objects.  You can assign or append strings or text labels or integer expressions
directly to text labels.
<CODE>
	TEXT_LABEL A, B
	A = "Some string"
	B = A
	A = 7
	A += B
	A += "Some string"
	A += 7
</CODE>

There are several variations of text labels: TEXT_LABEL_3, 7, 15, 23, 31, and 63.


@@Enumerations
<GROUP User Manual>
You can introduce new enumerants into the language (as typenames):
<CODE>
ENUM TypeFlag
	TF_DISABLED,
	TF_ENABLED,
	TF_READY
ENDENUM
</CODE>
Enumerants are all at global scope, and are only assignment-
compatible with themselves.  This is particularly useful for
making command calls more type-safe.  You can explicitly cast
between enumerants and integers, although the syntax is intentionally
verbose because it's dangerous if abused:
<CODE>
TypeFlag T
INT I = ENUM_TO_INT(T)
T = INT_TO_ENUM(TypeFlag, I)
</CODE>

Enumerants will usually be supplied by programmers and will match
the internal C++ code values, but designers are free to define their
own enumerants for clarity as well (for example, for assigning
meaningful names to states).  Enumerants can be used as array
indices and FOR loop control variables and FOR loop initial and
final values as well.

You can apply the COUNT_OF operator to an enumerant type; the value
is one more than the value of the last member of the enumerant.

<CODE>
ENUM STATE
	WANDER,
	ACTIVE,
	DEAD
ENDENUM
</CODE>

In this case, COUNT_OF(STATE) will be 3.

You can also declare a parameter to a FUNC or PROC as an ENUM_TO_INT.
This can avoid having to use ENUM_TO_INT in every call of the FUNC or 
PROC in some situations but is not done by default to preserve type-safety.
Note that the only thing you can do to an ENUM_TO_INT parameter is assign
it to a real INT local variable, which is what you should use from then onward.
You can also declare that a function accepts a reference to any kind of
enumerant or integer via ENUM_TO_INT & syntax.

<CODE>
PROC SomeProc(ENUM_TO_INT A)
	INT B = A	// Special case, no conversion required
ENDPROC
</CODE>

It is also possible to forward declare ENUMs and STRUCTs, which is useful for native function
definitions or function pointers.

<CODE>
FORWARD ENUM SPECIAL_FLAGS
FORWARD STRUCT SPECIAL_STRUCT

NATIVE PROC SET_SPECIAL_FLAGS(SPECIAL_FLAGS SF)

STRUCT SPECIAL_STRUCT
  INT SPECIAL_INT
  BOOL SPECIAL_BOOL
ENDSTRUCT
</CODE>

Anywhere you can specify ENUM you can also specify STRICT_ENUM; this
is a variation of enumerants that doesn't automatically promote to integer in
many situations.  In particular, you cannot use binary operators like OR on it,
which is useful for cases where the enumerant does not represent a bitmask and
you don't want people accidentally attempting to use it that way.

There is a final variation for enumerants: HASH_ENUM or STRICT_HASH_ENUM.  The
only difference between these and their non-HASH versions is that if you don't
explicitly specify a value for an enumerant, it will default to the hash of its
own name instead of the previous enumerant value plus one.

You may also declare any PROC or FUNC to accept a parameter of type STRINGHASH.
Under the hood this is just an integer, but can only be copied or passed around,
not directly manipulated.  If you pass a string literal to a subprogram expecting
a STRINGHASH, the string is hashed at compile time just like with the HASH operator.
If you pass in a STRING or TEXT_LABEL, the compiler emits an opcode that causes the
hash code to be generated at runtime before being passed down.

@@Flow Control
<GROUP User Manual>
Basic decision making is done with the IF statement.
<CODE>
IF expr
  // if expr true
ENDIF

IF expr
  // if expr true
ELSE
  // if expr not true
ENDIF

IF expr
  // if expr true
ELIF expr2
  // if expr not true but expr2 is true
ELIF expr3
  // if neither expr nor expr2 true but expr3 is true
ELSE
  // none of expr, expr2, or expr3 are true
ENDIF

</CODE>

There are three different looping constructs:
<CODE>
WHILE expr
  // do something while expression is true
ENDWHILE
</CODE>
\Note that a WHILE statement may not execute even once if the
initial value of the expression turns out to be false.

<CODE>
REPEAT count varname
  // do something 'count ' times
ENDREPEAT
</CODE>
The REPEAT statement accepts count and a variable name.
The variable is initialized to zero and counts up
until it reaches the value, which must be an integer
literal or constant.

<CODE>
FOR varname = start TO finish
  // do something
ENDFOR
</CODE>
The FOR statement assigns varname to the value of start.
While start is less than or equal to finish, it runs the body
\of the loop. After every iteration, it adds one to start and
checks again. Note that finish is re-evaluated on every loop
iteration so it may lead to unexpected behavior if it depends
\on something changing in the loop itself.
<CODE>
FOR varname = start TO finish STEP value
  // do something
ENDFOR
</CODE>
This form is similar, except that you can count by something
\other than one. You can also count backwards with a negative
STEP value, in which case the loop continues as long as
varname is greater than or equal to finish. The step value
must be a nonzero constant integer.

You can abort execution of a WHILE, REPEAT, or FOR loop with
the BREAKLOOP directive.  This is separate from BREAK, which
is only for leaving a SWITCH CASE for legacy reasons.  You
can resume execution at the top of a loop (after modifying
the counter in a REPEAT or FOR loop) with the RELOOP directive.

The next flow control construct is the SWITCH statement.
<CODE>
SWITCH expr
  CASE 1  // Do something
    BREAK // Skip to ENDSWITCH
  CASE 2  // Do something else
          // ... and fall through
    FALLTHRU
  CASE 3  // Do another thing
    BREAK
  DEFAULT // Go here if no other values matched
ENDSWITCH
</CODE>
The expression must evaluate to an integer; CASE labels must
be expressions that evaluate to integer constants (including
enumerants and the HASH operator).  If the SWITCH expression is an integer, all case
statements must also evaluate to integers.  If the SWITCH expression
is an enumerant, all case statements must evaluate to enumerants of
that exact type.

You generally shouldn't declare variables within SWITCH statements,
particularly at the top before the first CASE, which used to work but
is now prohibited.  Variables declared after CASE labels will not
be usable in later CASE blocks when translated to C++ code, and therefore
should be avoided.

The BREAK statement allows you to jump to the enclosing ENDSWITCH; otherwise 
flow will fall through any other CASE labels. The DEFAULT label, if present,
is used as a catch-all if the expression didn't match any
values. You can nest SWITCH statements very deep if you
really want to, but that could get really confusing quickly.

The FALLTHRU statement informs the compiler that you really did
mean to fall off the end of one CASE block into the next one; if the
compiler will warn if it sees a block that doesn't end in a BREAK,
FALLTHRU, EXIT, or RETURN statement.  Its error checking isn't perfect,
so it can be confused by nested blocks like an IF / ENDIF where both
branches end in a RETURN -- in that case it will still want to see
a BREAK statement after the IF block.

The next flow control is the RETURN statement, which simply
exits the current function, returning a value to the
caller:
<CODE>
\RETURN N+1
</CODE>
The compiler will issue an error if it discovers any code
paths out of a function that are missing a RETURN statement.
In rare cases you may have to add an extra return statement
to satisfy it.

The final flow control is the EXIT statement, which simply
exits the current script or procedure.  
<CODE>
EXIT
</CODE>

@@Procedures and Functions
<GROUP User Manual>
The meat of the scripting language is the suite of commands
that the game supplies to the designers. These commands are
broken down into procedures and functions. The only
difference is that procedures don't return a value and
therefore cannot be used in expressions; functions do return
a value and can be used in expressions or statements.

All commands require their parameter list to be enclosed in
parentheses, even if they don't take any parameters. This is
to remind you that you're making a function call, because
\otherwise the syntax is indistiguishable from normal
variable access.
<code>
FLOAT T = TIMESTEP()
FLOAT Angle = ATAN2(Y,X)
FLOAT Mag = SQRT (X*X + Y*Y + Z*Z)
CREATE_ACTOR(SomeActor,SomePosition)
</code>

You can declare your own procedures and functions and call
them from your code.
<code>
PROC MyFunction(INT A,VECTOR B,FLOAT &C)
    IF (A \> 0)
        C = VMAG(B)
    ELSE
        C = 0
    ENDIF
ENDPROC

FUNC VECTOR VDIFF(VECTOR A, VECTOR B)
    VECTOR V = A - B
    RETURN V
ENDFUNC
</code>

You can pass parameters by value (like A and B) or by
reference (like C). When you pass a value by reference, any
changes made to the formal parameter will be immediately
reflected in the actual value of the variable. When you pass
a value by value, any changes made to the formal parameter
affect only that copy.

When you pass an array to a function, the size of the array
must match. As a special exception, if you pass the array by
reference and declare the last dimension of the array in the
parameter list as having no size:

<code>
FUNC INT ADDER(INT &someArray[])
</code>

\or

<code>
PROC ADDERMULTI(INT &someArray[3][3][])
</code>

then the type-checking is relaxed and you can pass an
arbitrarily-sized array into the command.  Internally the virtual
machine remembers the size of every array in a hidden count
at the head of that array, so runtime range checking still
works even with dynamic arrays.

You can also declare any PROC or FUNC as accepting default
parameters.  This should work for INT, FLOAT, ENUM, and BOOL
types.  Once you declare a default parameter, any parameters
after it must have a default as well.  This is experimental
code, so the type checking isn't particularly rigorous -- you
can mix up enumerant types in particular and it won't catch it.
When calling a subprogram that has default parameters, you can
always substitute the keyword DEFAULT in place of that parameter
to explicitly request its default value; this is useful when
there are multiple default parameters and you need to modify
the second one while leaving the first one unchanged.

<code>
FUNC INT ADDER(INT A,INT B = 1)
FUNC FLOAT FADDER(FLOAT A,FLOAT B = 1.0)
FUNC FLOAT FADDER(FLOAT A,FLOAT B = 1)	// BAD!
</code>

You can declare a PROC or FUNC as DEBUGONLY which will cause it
to be removed from the final build of the game.  If you apply this
to a FUNC that returns INT, the function will act as if it simply
returned zero if the value is assigned anywhere.  It can only
be used on a FUNC that returns something other than an INT if the
value is never used.

<code>
DEBUGONLY PROC PRINT_STATUS(STRING A)
DEBUGONLY FUNC INT GET_DEBUG_STATUS()
</code>

There is also limited support for variable arguements in native
functions.

<code>
NATIVE PROC PRINTLN(VARARGS)
NATIVE FUNC INT ADD_MANY(VARARGS)
</code>

Only basic types -- INT, BOOL, ENUM, STRING, text labels, and 
VECTORS are supported.  On the C++ side, there are macros documented
in rage/script/src/script/varargs.h which allow you to determine the
runtime type of a given parameter.  Up to 16 parameters are supported.

You may also specify VARARGS1, VARARGS2, or VARARGS3 to indicate that
the subprogram must accept at least one, two or three parameters or else
the compiler will issue a diagnostic.

@@Expressions
<GROUP User Manual>
The usual arithmetic operators work: +, -, *, /. % is used
for modulo.

Multiple conditions can be tested at once by using the NOT,
AND, and OR operators. You can mix and match them at will,
but use parentheses for clarity.  ANDALSO and ORELSE are
available as boolean short-circuit operators; ANDALSO does
not evaluate its right-hand parameter if the left-hand parameter
is already false; ORELSE does not evaluate its right-hand parameter
if the left-hand parameter is already true.

Check for equality with =, and inequality with != or \<\>.

You can use ! for a synonym for the NOT keyword.

Vectors can appear in some arithmetic expressions: +, -, *,
and /, and unary negation. If one side of a binary expression
is a VECTOR, and the other side is a FLOAT, the float is
implicitly converted to a VECTOR through replicating the
value into x, y, and z. This allows multiplication and
division to work like you'd expect, although it will lead to
surprising behavior for addition and subtraction!

The COUNT_OF operator can be used on enumerant types and array
variables.  The SIZE_OF operator can be used on any typename or
variable to return the size of the object in virtual machine words 
(currently four bytes per word).

The HASH operator can be used to generate a standard hashcode:

	INT MODEL = HASH("PED_SLOW")
	
It only works on string literals.

@@Passing Parameters To Scripts
<GROUP User Manual>
It is possible to pass arbitrary data to a script when starting it up.
If you need more than a single parameter, you must place them all into
a single STRUCT.  For example:

<CODE>
// ParamStruct.sch
STRUCT MyParam
	VECTOR Position
	TEXT_LABEL Name
ENDSTRUCT
</CODE>

<CODE>
// CalledScript.sc
USING "ParamStruct.sch"

SCRIPT(MyParam args)
  SPAWN_NAMED_ACTOR_AT_POSITION(args.Name,args.Position)
ENDSCRIPT
</CODE>

<CODE>
// CallingScript.sc
USING "ParamStruct.sch"
USING "rage_builtins.sch"

SCRIPT
  MyParam myargs
  myargs.Position = << 1, 2, 3>>
  myargs.Name = "Mailbox"
  START_NEW_SCRIPT_WITH_ARGS("CalledScript", myargs, SIZE_OF(myargs))
ENDSCRIPT
</CODE>

Note how the parameters are declared as a single non-reference variable
to the SCRIPT block.  Also note the use of the SIZE_OF operator to
indicate the size of the data structure; each script remembers the size
of any incoming parameter structure, and the virtual machine will assert
out if you pass in a parameter structure of the wrong size.  However,
you are always free to invoke a script that accepts parameters without
any parameters, in which case the parameter data will be initialized with
zeroes like any other variable; you can use this fact to behave differently
depending on whether you were invoked with parameters or not if you so
choose.  The parameter structure is copied into the calling script
immediately at startup so there are no issues with object lifetime if the
parent script terminates immediately after invoking one or more child
scripts.

@@Operator Precedence
<GROUP User Manual>
This is mostly for people with Computer Science degrees who
are curious.

All expressions are left-associative except for the unary
\operators, which are right-associative. From highest
precedence to lowest: (all operators on the same line have
equal precedence)
      * NOT and unary negation and unary address (&).
      * / %
      * + - +@ -@
      * \< \<= \> \>= = \<\>
      * & (bitwise and)
      * ^ (bitwise xor)
      * | (bitwise or)
      * AND
      * OR
Therefore the expression NOT A OR B AND C evaluates as (NOT
A) OR (B AND C), which is probably not obvious to anybody, so
make sure you use parentheses.

You can get a bitwise NOT by using "val ^ -1".

Note that AND and OR do <b>not</b> short-circuit like they do in C++, 
unless you specifically request this on the command line (which all
current projects do now).

You can suppress an unused variable warning with UNUSED_PARAMETER(x)
where x is the variable you want the compiler to pretend is still
being accessed.  This has zero run-time cost.  We now flag most attempts
to assign variables to themselves since that was what people used to
have to do to get rid of the warning.

@@Why a Custom Language?
<GROUP User Manual>
One obvious question is why do we use a custom language
instead of an off-the-shelf scripting language. Lua is tiny
but requires garbage collection and has untyped variables.
Python is just too big. The RAGE scripting language is
heavily based upon the time-proven Rockstar North scripting
language. It is strongly typed, and requires no runtime
memory allocation for the virtual machine itself once a
thread is allocated. It is a simple, imperative language that
is easy to understand and extend. The virtual machine itself
is tiny, with the main loop being less than 300 lines of
code.

@@Programming Manual
This section is intended for use by programmers who are
adding new commands to the script language itself.

You declare new commands (where a command is either a
function or a procedure) using the NATIVE directive.
<CODE>
NATIVE FUNC FLOAT ABS(FLOAT F)
NATIVE PROC PRINTSTRING(STRING S)
NATIVE FUNC FLOAT ATAN2(FLOAT Y,FLOAT X)
</CODE>

You can mark a NATIVE PROC or FUNC DEBUGONLY as well, just
like you can user-supplied subprograms:
<CODE>
NATIVE DEBUGONLY FUNC IS_DEBUG_HUD_ENABLED()
NATIVE DEBUGONLY PROC PRINTSTRING(STRING S)
</CODE>

You can also introduce new handle types into the language
with a variant of the NATIVE syntax:
<CODE>
NATIVE SPRITE
NATIVE FUNC SPRITE CREATE_SPRITE(STRING Filename)
</CODE>
The handle types are compatible only with themselves, and
help catch more potential problems at compile time.

You associate the run-time support for a script command by
calling <C>scrThread::RegisterCommand(commandName,funcPtr)</C>
where <C>commandName</C> should match the name of the
function in the NATIVE declaration. The compiler and the <C>RegisterCommand</C>
both use the same hashing algorithm so the names will match
up.

The function should be void, accepting a reference to a
non-const <C>scrThread::Info</C> object. See the code for
specific details and examples, but you basically pull the <C>Param</C>
values out of the array, operate on them, and then set the <C>Return</C>
value if necessary. You can also modify the state of the
thread so that it will become blocked and return immediately
to higher-level code. A script that is blocked by a command
will immediately re-execute that command the next time it
gets scheduled.

There are also some templated wrapper functions in <C>wrapper.h</C>
that can simplify integration.

If you want to pass parameters to a script at startup like you can
with the builting <C>START_NEW_SCRIPT_WITH_ARGS</C> command, just
pack the data into a struct matching the script engine's view of the
data, and pass the structure into CreateThread.  The size here should
be in bytes, not virtual machine words.  Keep in mind that VECTOR types
in the scripting language are not aligned or padded, so you probably
want to use a scrVector instead.  You can use a scrTextLabel wherever
a TEXT_LABEL would be used.  If you want to pass in an array of objects,
remember that the virtual machine has a hidden word at the start of
every array which contains the count of elements in that array.

Also note that a BOOL in the scripting language is the same as an INT,
so it's not compatible with a C++ bool, which is only a byte.

<CODE>
STRUCT MyArgs
	TEXT_LABEL Name
	VECTOR EndPoints[2]
ENDSTRUCT
</CODE>

would translate to:

<CODE>
struct c__MyArgs {
	scrTextLabel Name;
	int EndPoints_Count;
	scrVector EndPoints[2];
};

c_MyArgs args;
safecpy(args.Name,some_name,sizeof(args.Name));
args.EndPoints_Count = NELEM(args.EndPoints);
args.EndPoints[0] = some_Vector3;
args.EndPoints[1] = some_other_Vector3;
</CODE>
	
