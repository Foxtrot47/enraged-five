#ifndef SCRIPT_VALUE_H
#define SCRIPT_VALUE_H

#include <stdlib.h> // size_t comes from here

namespace rage {

enum scrProgramId { srcpidNONE };

#define SCRVALUE_OFFSETS		0 //(__64BIT && !__DEV && !__RESOURCECOMPILER) // Temporarily disabled until array overruns can be investigated

/* Purpose:
sizeof(scrValue) is 4 on old-gen and 8 on next-gen.
You can use this if you need to shift the padding at the beginning of an array
Or if you want to use sizeof(scrTextLabelN) to have the maximum size of the typedef
by doing sizeof(scrTextLabelN)/SCRTYPES_ALIGNMENT */
#define SCRTYPES_ALIGNMENT		(sizeof(scrValue)/sizeof(int))

union scrValue {
	// Basic datatype, for globals access
	enum ValueType {
		UNKNOWN, BOOL, INT, FLOAT, VECTOR, TEXT_LABEL, STRING, OBJECT
	};
	enum VarArgType {		// IMPORTANT - change script/src/script/csupport.h, Info::Fill if this changes.
		VA_INT, VA_FLOAT, VA_STRINGPTR, VA_VECTOR
	};

	int Int;
	unsigned Uns;
	float Float;

#if SCRVALUE_OFFSETS
	unsigned String;
	unsigned Reference;
	unsigned Any;
#else
	const char *String;
	scrValue *Reference;
	size_t Any;
#endif

	bool operator==(const scrValue &that) {
		return Int==that.Int;
	}
};

#if SCRVALUE_OFFSETS
typedef unsigned scrReference;
typedef unsigned scrAny;
#else
typedef scrValue *scrReference;
typedef size_t scrAny;
#endif

const int scrMaxTextLabelSize = 64;	// Must be a multiple of sizeof(scrValue).

CompileTimeAssertSize(scrValue,4,SCRVALUE_OFFSETS? 4 : 8);

}	// namespace rage

#endif
