#include "program.h"

#include "hash.h"
#include "thread.h"
#include "scrchannel.h"
#include "system/magicnumber.h"
#include "system/endian.h"
#include "data/aes.h"
#include "diag/tracker.h"
#include "file/asset.h"
#include "file/packfile.h"
#include "system/criticalsection.h"
#include "system/param.h"
#include "system/memory.h"
#include "system/memmanager.h"
#include "system/memvisualize.h"
#include "system/tinyheap.h"
#include "string/stringhash.h"
#include "paging/ref.h"

#include <string.h>
#include <algorithm>

#if RSG_PC
#include "system/memory.h"
#include "system/xtl.h"
#endif

#include "zlib/zlib.h"

#define ENCRYPT_GLOBALS			(__XENON && 0)

#if ENCRYPT_GLOBALS
#include "system/xtl.h"			// For EncryptedAlloc/Free
#endif

#define SCRIPT_GLOBAL_COUNT (1 << 18)

#if EXECUTABLE_SCRIPTS
#if __PS3
#include <sys/prx.h>
#else
#include "system/exec.h"
#endif
#endif

#if __WIN32PC
#include "system/param.h"
PARAM(nozlib,"Don't compress scripts with zlib");
#endif
PARAM(scriptproject,"[scripting] specify root script path for sco objects (if using the option to compile scripts to an output directory based on project config)");

namespace rage {
	const char* gScriptProject = NULL;
	const char* gBaseScriptDirectory = NULL; // might want to set this if you're going to set gScriptProject

#if EXECUTABLE_SCRIPTS
#include "csupportargs.h"
#endif
}

#define __RAGE_SCRIPT_COMPILER	(0)

using namespace rage;

RAGE_DEFINE_CHANNEL( Script );

#if __BE
inline void Convert_LE(u32*d,u32 c) { for (;c;c--,d++) *d = sysEndian::Swap(*d); }
inline void Convert_BE(u32*,u32) { }
inline void Convert_BE(u64*,u32) { }
#else
inline void Convert_BE(u32*d,u32 c) { for (;c;c--,d++) *d = sysEndian::Swap(*d); }
inline void Convert_BE(u64*d,u32 c) { for (;c;c--,d++) *d = sysEndian::Swap(*d); }
inline void Convert_LE(u32*,u32) { }
#endif

// Can't use the "normal" script hash because it doesn't support defragmentation.
class scrProgramRegistry 
{
public:
	scrProgramRegistry()
	{
		memset(&First[0],0,sizeof(First));
		// Slot zero never gets used in order to simplify other logic, so recycle it as the free list pointer.
		// Set up the free list.
		FirstFree = 1;
		// Trying to insert a full list will cause an array trap because
		// we've intentionally initialized the last free entry with an
		// invalid array index, one past the end.
		for (Index i=1; i<MAX; i++)
			Next[i] = (Index)(i+1);
	}
	scrProgram *Lookup(u32 code)
	{
		SYS_CS_SYNC(sm_Token);
		u32 i = First[code % HASHSIZE];
		while (i && Progs[i]->GetHashCode() != code)
			i = Next[i];
		if (i) {
			AssertMsg(Progs[i],"Attempting to retrieve NULL scrProgram, did we evict the streamable script somehow?");
			return Progs[i];
		}
		else
			return NULL;
	}
	void Insert(scrProgram *p)
	{
		SYS_CS_SYNC(sm_Token);

#if !__NO_OUTPUT && !__RAGE_SCRIPT_COMPILER
		scrDisplayf("scrProgramRegistry::Insert - about to insert %s", p->GetScriptName());
		VerifyLists();
#endif	//	!__NO_OUTPUT

		AssertMsg(p,"Attempting to insert NULL scrProgram into scrProgramRegistry!");
		u32 code = p->GetHashCode();
		Assert(!Lookup(code));
		// Get next free slot

#if !__NO_OUTPUT
		if (FirstFree == MAX)
		{
			scrDisplayf("scrProgramRegistry::Insert - array is full. Trying to insert %s", p->GetScriptName());
			VerifyLists();
			scrAssertf(0, "scrProgramRegistry::Insert - array is full. Trying to insert %s", p->GetScriptName());
		}
#endif	//	!__NO_OUTPUT

		Index nextFree = Next[FirstFree];
		// Chain new slot into existing hash chain
		Next[FirstFree] = First[code % HASHSIZE];
		// New slot is now start of this hash chain
		First[code % HASHSIZE] = FirstFree;
		Progs[FirstFree] = p;
		SAFE_ADD_KNOWN_REF(Progs[FirstFree]);
		// Update next free item
		FirstFree = nextFree;

#if !__NO_OUTPUT && !__RAGE_SCRIPT_COMPILER
		scrDisplayf("scrProgramRegistry::Insert - after inserting %s", p->GetScriptName());
		VerifyLists();
#endif	//	!__NO_OUTPUT
	}
	bool Remove(scrProgram *p)
	{
		SYS_CS_SYNC(sm_Token);

#if !__NO_OUTPUT && !__RAGE_SCRIPT_COMPILER
		scrDisplayf("scrProgramRegistry::Remove - about to remove %s", p->GetScriptName());
		VerifyLists();
#endif	//	!__NO_OUTPUT

		Assert(Lookup(p->GetHashCode()));
		// Get pointer to link head
		Index *pNext = &First[p->GetHashCode() % HASHSIZE];
		// Keep searching list until we find the entry
		while (*pNext && Progs[*pNext] != p)
			pNext = &Next[*pNext];
		if (!*pNext)
		{
			AssertMsg(false,"Unable to remove program even though Lookup succeeded?");
			return false;
		}
		// Remove the known reference
		Index nextFree = *pNext;
		SAFE_REMOVE_KNOWN_REF(Progs[nextFree]);
		Progs[nextFree] = NULL;
		// Patch ourselves out of the hash chain (possibly terminating it)
		*pNext = Next[nextFree];
		// Patch ourselves back into the free list again.
		Next[nextFree] = FirstFree;
		FirstFree = nextFree;

#if !__NO_OUTPUT && !__RAGE_SCRIPT_COMPILER
		scrDisplayf("scrProgramRegistry::Remove - after removing %s", p->GetScriptName());
		VerifyLists();
#endif	//	!__NO_OUTPUT

		return true;
	}

#if USE_PROFILER_NORMAL
	void GenerateProfilerDescriptions()
	{
		SYS_CS_SYNC(sm_Token);
		for (int i = 0; i < Progs.GetMaxCount(); ++i) {
			if (const scrProgram* program = Progs[i]) {
				scrThread::GenerateProfilerEvents(*program);
			}
		}
	}
#endif

private:
#if !__NO_OUTPUT
	void VerifyLists()
	{
		u32 TotalUsed = 0;
		for (u32 loop1 = 0; loop1 < HASHSIZE; loop1++)
		{
			Index *pIndex = &First[loop1];
			while (*pIndex)
			{
				TotalUsed++;
				pIndex = &Next[*pIndex];
			}
		}

		u32 TotalFree = 0;
		Index currentFreeIndex = FirstFree;
		while (currentFreeIndex != MAX)
		{
			TotalFree++;
			currentFreeIndex = Next[currentFreeIndex];
		}

		scrDisplayf("scrProgramRegistry::VerifyLists - TotalUsed = %u TotalFree = %u", TotalUsed, TotalFree);
		scrAssertf( (TotalUsed + TotalFree) == (MAX - 1), "scrProgramRegistry::VerifyLists - Expected TotalUsed + TotalFree to equal %u but it's %u", (MAX - 1), (TotalUsed + TotalFree));

		if (TotalUsed > WARNING_THRESHOLD)
		{
			scrDisplayf("scrProgramRegistry::VerifyLists - Warning! TotalUsed is over %u. TotalUsed = %u TotalFree = %u", WARNING_THRESHOLD, TotalUsed, TotalFree);
			scrAssertf(0, "scrProgramRegistry::VerifyLists - Warning! TotalUsed is over %u. TotalUsed = %u TotalFree = %u", WARNING_THRESHOLD, TotalUsed, TotalFree);
		}
	}
#endif	//	!__NO_OUTPUT

private:
	typedef u8 Index;
	static const Index HASHSIZE = 32;
	static const Index MAX = 176;
#if !__NO_OUTPUT
	static const Index WARNING_THRESHOLD = 160;
#endif	//	!__NO_OUTPUT
	Index FirstFree;
	atRangeArray<Index,HASHSIZE> First;
	atRangeArray<Index,MAX> Next;
	atRangeArray<scrProgram *,MAX> Progs;
	static sysCriticalSectionToken sm_Token;
} s_ProgHash;

sysCriticalSectionToken scrProgramRegistry::sm_Token;

#if ENABLE_SCRIPT_GLOBALS_HEAP
static sysTinyHeap s_GlobalsHeap;
#endif // ENABLE_SCRIPT_GLOBALS_HEAP

atRangeArray<scrValue *,scrProgram::MAX_GLOBAL_BLOCKS> scrProgram::sm_Globals;
atRangeArray<int,scrProgram::MAX_GLOBAL_BLOCKS> scrProgram::sm_GlobalSizes;
atRangeArray<ConstString,scrProgram::MAX_GLOBAL_BLOCKS> s_NamesOfScriptsAssociatedWithGlobalBlocks;
#if RSG_PC
atMap<u32,u32> scrProgram::sm_GlobalBlockIndexToNameHash;
#endif
bool scrProgram::sm_AllowMismatchedGlobals = false;
ConstString s_GlobalsScript;
#if __ASSERT
bool scrProgram::sm_AllowDelete = true;
#endif

static int s_AllRefCounts;
static u32 s_GlobalsHashes[scrProgram::MAX_GLOBAL_BLOCKS];

#if SCRVALUE_OFFSETS
static void compressData(scrValue *inOut,u32 count) {
	u32 *d = (u32*) inOut;
	u64 *s = (u64*) inOut;
	while (count--)
		*d++ = (u32) *s++;
}
#endif

scrProgram::scrProgram(const char *name,u32 opcodeSize,u32 nativeSize,u32 staticSize,u32 globalSize,int globalsBlock,u32 stringHeapSize,u32 argStructSize,u32 globalsHash) {
	if (!opcodeSize) 
		++opcodeSize;
	OpcodeSize = opcodeSize;
	opcodeSize = ((opcodeSize + scrPageMask)) >> scrPageShift;
	Table = rage_new u8*[opcodeSize];
	for (u32 i=0; i<opcodeSize-1; i++)
		Table[i] = rage_new u8[scrPageSize];
	// Last page doesn't need to be complete (but make sure it's not bogus if it's an exact multiple!)
	Table[opcodeSize-1] = rage_new u8[(OpcodeSize & scrPageMask)?(OpcodeSize & scrPageMask):scrPageSize];
	Statics = rage_new scrValue[StaticSize = staticSize];

	GlobalSizeAndBlock = globalSize | (globalsBlock << MAX_GLOBAL_BLOCKS_SHIFT);
	if (globalSize) {
		GlobalsTable = rage_new scrValue*[GetGlobalsPageCount()];
		for (u32 i=0; i<GetGlobalsPageCount(); i++)
			GlobalsTable[i] = rage_new scrValue[GetGlobalsPageChunkSize(i)];
	}
	else
		GlobalsTable = NULL;

	ArgStructSize = argStructSize;
	GlobalsHash = globalsHash;
	Natives = rage_new u64[NativeSize = nativeSize];
	StringHeapSize = stringHeapSize;
	u32 numStringHeaps = GetStringHeapCount();
	StringHeaps = rage_new const char*[numStringHeaps];
	for (u32 i=0; i<numStringHeaps; i++)
		StringHeaps[i] = rage_new char[GetStringHeapChunkSize(i)];
	ProcCount = 0;
	ProcNames = 0;
	HashCode = scrComputeHash(name);
	ScriptName = name;
	RefCount = 0;
	m_programBreakpoints = NULL;
	AddRef();

#if !__RAGE_SCRIPT_COMPILER
	scrDisplayf("scrProgram::scrProgram - Script '%s' has globals signature %u", ScriptName.c_str(), GlobalsHash);
#endif // !__RAGE_SCRIPT_COMPILER

	if (GlobalsHash && s_GlobalsHashes[globalsBlock] && GlobalsHash != s_GlobalsHashes[globalsBlock])
	{
		if (!sm_AllowMismatchedGlobals)
		{
			Quitf(ERR_DEFAULT,"scrProgram - Script '%s' %u has different globals signature than '%s' %u", ScriptName.c_str(), GlobalsHash, s_GlobalsScript.c_str(), s_GlobalsHashes[globalsBlock]);
		}
		else
		{
			Errorf("scrProgram - Script '%s' %u has different globals signature than '%s' %u", ScriptName.c_str(), GlobalsHash, s_GlobalsScript.c_str(), s_GlobalsHashes[globalsBlock]);
		}
	}
}

inline u64 RotateLeftN(u64 value,u32 count) {
	count &= 63;
	return (value << count) | (value >> (64-count));
}

inline u64 RotateRightN(u64 value,u32 count) {
	count &= 63;
	return (value >> count) | (value << (64-count));
}

scrProgram::scrProgram(datResource &rsc) : ScriptName(rsc) {
	rsc.PointerFixup(Table);
	u32 opcodeSize = ((OpcodeSize + scrPageMask)) >> scrPageShift;
	for (u32 i=0; i<opcodeSize; i++)
		rsc.PointerFixup(Table[i]);
	rsc.PointerFixup(Statics);
	rsc.PointerFixup(Natives);
	for (u32 i=0; i<NativeSize; i++)
		Natives[i] = RotateLeftN(Natives[i],OpcodeSize+i);
	rsc.PointerFixup(GlobalsTable);
	for (u32 i=0; i<GetGlobalsPageCount(); i++) {
		rsc.PointerFixup(GlobalsTable[i]);
#if SCRVALUE_OFFSETS
		compressData(GlobalsTable[i],GetGlobalsPageChunkSize(i));
#endif
	}
#if SCRVALUE_OFFSETS
	// The scrValue's in resources are still 64 bits (at least for now).
	compressData(Statics,StaticSize);
#endif
	rsc.PointerFixup(StringHeaps);
	for (u32 i=0; i<GetStringHeapCount(); i++)
		rsc.PointerFixup(StringHeaps[i]);

	// This will update ProcCount and ProcNames if profiler is enabled...
	if (!rsc.IsDefragmentation()) {
		scrDisplayf("scrProgram::scrProgram - Script '%s' has globals signature %u", ScriptName.c_str(), GlobalsHash);

		if (GlobalsHash && s_GlobalsHashes[GlobalSizeAndBlock >> MAX_GLOBAL_BLOCKS_SHIFT] && GlobalsHash != s_GlobalsHashes[GlobalSizeAndBlock >> MAX_GLOBAL_BLOCKS_SHIFT])
		{
			if (!sm_AllowMismatchedGlobals) {
				Quitf(ERR_DEFAULT,"scrProgram - Script '%s' %u has different globals signature than '%s' %u", ScriptName.c_str(), GlobalsHash, s_GlobalsScript.c_str(), s_GlobalsHashes[GlobalSizeAndBlock >> MAX_GLOBAL_BLOCKS_SHIFT]);
			}
			else {
				Errorf("scrProgram - Script '%s' %u has different globals signature than '%s' %u", ScriptName.c_str(), GlobalsHash, s_GlobalsScript.c_str(), s_GlobalsHashes[GlobalSizeAndBlock >> MAX_GLOBAL_BLOCKS_SHIFT]);
			}
		}

		++s_AllRefCounts;
		if (GlobalSizeAndBlock)
			SetGlobals();
		scrThread::Validate(*this);

		// Since we know the string heap is always on specific pages, lock only them
		// so the rest of the script can still defragment.
		for (u32 i=0; i<GetStringHeapCount(); i++) {
			int stringPageIndex = rsc.GetMap().ContainsDest((void*)StringHeaps[i]);
			Assert(stringPageIndex != -1);
			void *stringPage = rsc.GetMap().Chunks[stringPageIndex].DestAddr;
			ASSERT_ONLY(const bool ok =) sysMemAllocator::GetCurrent().TryLockBlock(stringPage);
			// Since this is not a defragmentation call, TryLockBlock should always
			// work.
			Assert(ok);
		}

		s_ProgHash.Insert(this);
	}
	else {
		// If we're defragmenting, these need to be fixed up instead.
		// Note that the toplevel pointer array is allocated from the C++ heap,
		// not the resource heap, so it doesn't need to be fixed up.
		// String heap is locked above so we never have to fix it up.
		for (u32 i=0; i<ProcCount; i++)
			rsc.PointerFixup(ProcNames[i]);
	}
}

int scrProgram::GetNumRefs() const { 
	return RefCount; 
}

scrProgram::~scrProgram() {
	AssertVerify(s_ProgHash.Remove(this));
	u32 opcodeSize = (OpcodeSize + scrPageMask) >> scrPageShift;
	while (opcodeSize)
		delete Table[--opcodeSize];
	delete[] Table;
	delete[] Statics;
	delete[] Natives;
	for (u32 i=0; i<GetStringHeapCount(); i++)
		delete[] StringHeaps[i];
	delete[] StringHeaps;
	for (u32 i=0; i<GetGlobalsPageCount(); i++)
		delete[] GlobalsTable[i];
	delete[] GlobalsTable;
	delete[] ProcNames;
	delete m_programBreakpoints;
}

#if __DECLARESTRUCT
void scrProgram::DeclareStruct(datTypeStruct &s) {
	u32 opcodeSize = (OpcodeSize + scrPageMask) >> scrPageShift;
	for (u32 i=0; i<opcodeSize; i++)
		datSwapper((void*&)Table[i]);
	for (u32 i=0; i<NativeSize; i++) {
		Natives[i] = RotateRightN(Natives[i],OpcodeSize+i);
		datSwapper(Natives[i]);
	}
	for (u32 i=0; i<StaticSize; i++)
		datSwapper(Statics[i].Uns);
	for (u32 i=0; i<GetGlobalsPageCount(); i++) {
		for (u32 j=0; j<GetGlobalsPageChunkSize(i); j++)
			datSwapper(GlobalsTable[i][j].Uns);
		datSwapper((void*&)GlobalsTable[i]);
	}
	STRUCT_BEGIN(scrProgram);
	pgBase::DeclareStruct(s);
	STRUCT_FIELD_VP(Table);
	STRUCT_FIELD(GlobalsHash);
	STRUCT_FIELD(OpcodeSize);
	STRUCT_FIELD(ArgStructSize);
	STRUCT_FIELD(StaticSize);
	STRUCT_FIELD(GlobalSizeAndBlock);
	STRUCT_FIELD(NativeSize);
	STRUCT_FIELD_VP(Statics);
	STRUCT_FIELD_VP(GlobalsTable);
	STRUCT_FIELD_VP(Natives);
	STRUCT_FIELD(ProcCount);
	STRUCT_FIELD_VP(ProcNames);
	STRUCT_FIELD(HashCode);
	STRUCT_FIELD(RefCount);
	STRUCT_FIELD(ScriptName);
	for (u32 i=0; i<GetStringHeapCount(); i++)
		datSwapper(StringHeaps[i]);
	STRUCT_FIELD_VP(StringHeaps);
	STRUCT_FIELD(StringHeapSize);
	STRUCT_IGNORE(m_programBreakpoints);
	STRUCT_END();
}
#endif

#if __RESOURCECOMPILER
scrProgram::scrProgram(const scrProgram &that) {
	OpcodeSize = that.OpcodeSize;
	u32 opcodeSize = ((OpcodeSize + scrPageMask)) >> scrPageShift;
	Table = rage_new u8*[opcodeSize];
	for (u32 i=0; i<opcodeSize-1; i++) {
		Table[i] = rage_new u8[scrPageSize];
		for (u32 j=0; j<scrPageSize; j++)
			Table[i][j] = that.Table[i][j];
	}
	u32 lastSize = (OpcodeSize & scrPageMask)?(OpcodeSize & scrPageMask):scrPageSize;
	Table[opcodeSize-1] = rage_new u8[lastSize];
	for (u32 j=0; j<lastSize; j++)
		Table[opcodeSize-1][j] = that.Table[opcodeSize-1][j];

	Natives = rage_new u64[NativeSize = that.NativeSize];
	for (u32 i=0; i<that.NativeSize; i++)
		Natives[i] = that.Natives[i];
	Statics = rage_new scrValue[StaticSize = that.StaticSize];
	for (u32 i=0; i<that.StaticSize; i++)
		Statics[i] = that.Statics[i];

	GlobalSizeAndBlock = that.GlobalSizeAndBlock;
	if (GetGlobalsPageCount()) {
		GlobalsTable = rage_new scrValue*[GetGlobalsPageCount()];
		for (u32 i=0; i<GetGlobalsPageCount(); i++) {
			GlobalsTable[i] = rage_new scrValue[GetGlobalsPageChunkSize(i)];
			memcpy(GlobalsTable[i], that.GlobalsTable[i], GetGlobalsPageChunkSize(i) * sizeof(scrValue));
		}
	}
	else
		GlobalsTable = NULL;

	StringHeapSize = that.StringHeapSize;
	StringHeaps = rage_new const char*[GetStringHeapCount()];
	for (u32 i=0; i<GetStringHeapCount(); i++) {
		StringHeaps[i] = rage_new char[GetStringHeapChunkSize(i)];
		memcpy((char*)StringHeaps[i], that.StringHeaps[i], GetStringHeapChunkSize(i));
	}
	ArgStructSize = that.ArgStructSize;
	GlobalsHash = that.GlobalsHash;
	ProcCount = 0;
	ProcNames = 0;
	HashCode = that.HashCode;
	ScriptName = that.ScriptName;
	RefCount = 1;
	m_programBreakpoints = NULL;
}
#endif



u32 scrProgram::CalculateGlobalHash(const scrValue *pGlobalBuffer, u32 numberOfGlobals)
{
	u32 globalHash = 0;

	char *pTempBuffer = rage_new char[numberOfGlobals*4];
	if (pTempBuffer)
	{
		u32 *pTempU32 = (u32*)pTempBuffer;
		for (u32 loop = 0; loop < numberOfGlobals; loop++)
		{
			*pTempU32 = (u32)pGlobalBuffer[loop].Int;
			pTempU32++;
		}
		globalHash = atDataHash(pTempBuffer, numberOfGlobals*4);
		delete[] pTempBuffer;
	}

	return globalHash;
}

#if ENABLE_SCRIPT_GLOBALS_HEAP
void scrProgram::InitGlobalsHeap(void* heapStart, int heapSize)
{
	if(s_GlobalsHeap.GetHeapStart())
	{
		Quitf("Globals Heap is already initialised!");
	}

	s_GlobalsHeap.Init(heapStart, heapSize);
#if RAGE_TRACKING 
	if(diagTracker::GetCurrent() && !sysMemVisualize::GetInstance().HasXTL())
		diagTracker::GetCurrent()->InitHeap("Script Globals Heap", heapStart, heapSize);
#endif
}

#if RSG_ASSERT
void scrProgram::AssertGlobalsHeapEmpty()
{
	for(int i = 0; i < scrProgram::MAX_GLOBAL_BLOCKS; ++i)
	{
		scrAssertf(sm_Globals[i] == nullptr, "Script %s seems to be loaded still!", s_NamesOfScriptsAssociatedWithGlobalBlocks[i].c_str());
	}
}
#endif
#endif // ENABLE_SCRIPT_GLOBALS_HEAP

#if __WIN32PC
void scrProgram::Init(const char *filename,const u8 *opcodes,int opcodeSize,const u64 *natives,int nativeSize,const scrValue *statics,int staticSize,const scrValue *globals,int globalSize,int globalsBlock,const char *stringHeap,int stringHeapSize,int argStructSize) {
	// Only used by script compiler
	char scriptname[64];
	ASSET.BaseName(scriptname,sizeof(scriptname),ASSET.FileName(filename));
	u32 globalHash = CalculateGlobalHash(globals, globalSize);
	scrProgram *p = rage_new scrProgram(scriptname,opcodeSize,nativeSize,staticSize,globalSize,globalsBlock,stringHeapSize,argStructSize,globalHash);
	for (int i=0; i<opcodeSize; i++)
		(*p)[i] = opcodes[i];
	memcpy(p->Natives, natives, nativeSize * sizeof(u64));
	memcpy(p->Statics, statics, staticSize * sizeof(scrValue));
	for (u32 i=0; i<p->GetGlobalsPageCount(); i++)
		memcpy(p->GlobalsTable[i], globals + (i << scrGlobalsPageShift), p->GetGlobalsPageChunkSize(i) * sizeof(scrValue));
	for (u32 i=0; i<p->GetStringHeapCount(); i++,stringHeap += scrStringSize)
		memcpy((char*)p->StringHeaps[i],stringHeap,p->GetStringHeapChunkSize(i));
	p->SetGlobals();
	s_ProgHash.Insert(p);
}
#endif


void scrProgram::SetGlobals()
{
	USE_MEMBUCKET(MEMBUCKET_SCRIPT);

	AssertMsg(!s_GlobalsHashes[GlobalSizeAndBlock >> MAX_GLOBAL_BLOCKS_SHIFT],"Cannot have more than one script with defined globals in this block");
	AssertMsg(sm_Globals[GlobalSizeAndBlock >> MAX_GLOBAL_BLOCKS_SHIFT]==NULL,"Memory for global variables has already been allocated");

	// OPTIMIZATION: Reuse the same memory block to avoid fragmentation
	const u32 pos = GlobalSizeAndBlock >> MAX_GLOBAL_BLOCKS_SHIFT;
	const u32 count = sm_GlobalSizes[pos] = (GlobalSizeAndBlock & GLOBAL_SIZE_MASK);
	if (count > SCRIPT_GLOBAL_COUNT)
		Quitf(ERR_DEFAULT,"Too many script globals: %d! Decrease the number of script globals until they are <= %d", count, SCRIPT_GLOBAL_COUNT);

// #if RSG_PS3 || RSG_XENON		// Only do this crap on current gen consoles
// 	if (pos == 0) {	// Main globals block is preallocated
// 		void* ptr = sysMemManager::GetInstance().GetGlobalScriptMemory();
// 		scrAssertf(ptr, "Script Globals are NULL! We're fucked...");
// 		sm_Globals[pos] = rage_placement_new(ptr) scrValue[count];
// 	}
// 	else
// #endif
	
#if RSG_PC
	//scrValue* pNewGlobals = (scrValue*) VirtualAlloc(NULL, count*sizeof(scrValue), MEM_RESERVE | MEM_COMMIT | MEM_WRITE_WATCH, PAGE_READWRITE);
	scrValue* pNewGlobals = (scrValue*) sysMemVirtualAllocate( count*sizeof(scrValue), true );
#else

#if ENABLE_SCRIPT_GLOBALS_HEAP
	scrValue* pNewGlobals = (scrValue*)s_GlobalsHeap.Allocate(count * sizeof(scrValue));

	if(!pNewGlobals)
	{
		Quitf("Out of ScriptGlobalMemory! Increase runtime tweakable!");
	}
	OUTPUT_ONLY(size_t remaining = s_GlobalsHeap.GetMemoryAvailable());
	scrDebugf1("Remaining Script Global size %u", (u32)remaining);

#if RAGE_TRACKING 
	if(diagTracker::GetCurrent() && !sysMemVisualize::GetInstance().HasXTL())
		diagTracker::GetCurrent()->Tally(pNewGlobals, count, MEMBUCKET_SCRIPT);
#endif
#else
	scrValue* pNewGlobals = rage_new scrValue[count];
#endif // ENABLE_SCRIPT_GLOBALS_HEAP

#endif

	if (Verifyf(pNewGlobals, "Failed to allocate script globals!"))
	{
		for (u32 i=0; i<GetGlobalsPageCount(); i++)
		{
			memcpy(pNewGlobals + (i << scrGlobalsPageShift), GlobalsTable[i], GetGlobalsPageChunkSize(i) * sizeof(scrValue));
		}
	}

	s_GlobalsHashes[pos] = GlobalsHash;
	s_GlobalsScript = ScriptName;
	s_NamesOfScriptsAssociatedWithGlobalBlocks[pos] = ScriptName;

	sm_Globals[pos] = pNewGlobals;	// memcpy is finished, so set ptr here

#if RSG_PC
	u32 scriptNameHash = atStringHash(ScriptName.c_str());
	sm_GlobalBlockIndexToNameHash[pos] = scriptNameHash;
#endif

#if !__RAGE_SCRIPT_COMPILER
	scrDisplayf("scrProgram::SetGlobals - global block %u has been initialized. Global Hash is %u", pos, GlobalsHash);
#endif // !__RAGE_SCRIPT_COMPILER
}

u32 scrProgram::GetGlobalsHash()
{
	return GlobalsHash;
}

void scrProgram::AddRef() {
	++RefCount;
	++s_AllRefCounts;
}

int scrProgram::Release() {

	USE_MEMBUCKET(MEMBUCKET_RENDER);

	Assert(s_AllRefCounts);
	Assert(RefCount > 0);

	if (--s_AllRefCounts == 0) {
#if !__WIN32PC	// ...don't print if it's the compiler
		scrDisplayf("Last scrProgram freed, resetting all global variables");
#endif
		// Block zero is preallocated; others should be freed normally
		for (int i=0; i<MAX_GLOBAL_BLOCKS; i++) {
// #if RSG_PS3 || RSG_XENON		// Only do this crap on current gen consoles
// 			if (i)		
// #endif

#if RSG_PC
			sysMemVirtualFree(sm_Globals[i]);
#else

#if ENABLE_SCRIPT_GLOBALS_HEAP
#if RAGE_TRACKING 
			if(diagTracker::GetCurrent() && !sysMemVisualize::GetInstance().HasXTL())
				diagTracker::GetCurrent()->UnTally(sm_Globals[i], sm_GlobalSizes[i]);
#endif
			s_GlobalsHeap.Free(sm_Globals[i]);
#else
			delete[] sm_Globals[i];
#endif // ENABLE_SCRIPT_GLOBALS_HEAP
#endif

			sm_Globals[i] = NULL;
			sm_GlobalSizes[i] = 0;
			s_GlobalsHashes[i] = 0;


 			s_NamesOfScriptsAssociatedWithGlobalBlocks[i] = "";

#if !__RAGE_SCRIPT_COMPILER
			scrDisplayf("scrProgram::Release - global block %d has been reset", i);
#endif // !__RAGE_SCRIPT_COMPILER
		}
	}

	if (--RefCount == 0) {
		delete this;
		return 0;
	}
	else
		return RefCount;
}


struct Header {
	// First four words are not encrypted
	u32 Magic;			// SCR_MAGIC;
	u32 GlobalsHash;
	u32 CompressedSize;
	u32 EncryptionKey;

	u32 OpcodeSize;
	u32 StaticSize;
	u32 GlobalSize;
	u32 ArgStructSize;
	
	u32 NativeSize;
	u32 StringHeapSize;
	u32 GlobalsBlock;
	u32 Pad[1];
};

const int SCR_MAGIC = MAKE_MAGIC_NUMBER('S','C','R',12);

void scrProgram::DefaultGetFullFilename(char *dest, int maxBuf, const char *input, const char *ext) {
	ASSET.FullPath(dest, maxBuf, input, ext);
}


void (*scrProgram::GetFullFilename)(char *dest, int maxBuf, const char *input, const char *ext) = DefaultGetFullFilename;

namespace rage { extern unsigned char* atou(const char *s);	}	// hiding in device_common memory file handling code

scrProgramId scrProgram::Load( const char *inputFilename ) {
	char filename[256];
	GetFullFilename(filename, sizeof(filename), inputFilename, "sco");

	char scriptname[256];
	ASSET.RemoveExtensionFromPath(scriptname, sizeof(scriptname), filename);

	return scrProgram::Load(inputFilename,scriptname);
}

const bool g_Validate = !__RESOURCECOMPILER;

#if __64BIT && !SCRVALUE_OFFSETS
void expandData(scrValue* inOut,u32 count)
{
	// Zero expand data (so NULL pointers stay truly NULL) in a downward in-place copy.
	// Input:  abcdefg???????
	// Output: a0b0c0d0e0f0g0
	u32 *inOut32 = (u32*) inOut;
	while (count--)
		inOut[count].Any = inOut32[count];
}
#endif

scrProgramId scrProgram::Load(const char * filename,const char *scriptname_) {
	char scriptname[64];
	ASSET.BaseName(scriptname,sizeof(scriptname),ASSET.FileName(scriptname_));
	u32 hash = scrComputeHash(scriptname);
	scrProgram *p = s_ProgHash.Lookup(hash);
	if (p) {
		p->AddRef();
		return (scrProgramId)(size_t)p->HashCode;
	}

	char newfilename[RAGE_MAX_PATH];
	bool isMem = !strncmp(filename,"memory:",7);
	if(gScriptProject && *gScriptProject && !isMem)
	{
		// working copy of the string, so we can mess with it
		safecpy(newfilename,filename);
		int len = StringLength(newfilename);

		// replace '\\' with '/'
		for(int i=0;i<len;i++)
			if(newfilename[i]=='\\')
				newfilename[i]='/';

		// lower case it
		strlwr(newfilename);

		// add our script project directory
		Verifyf(ASSET.InsertPathElement(newfilename,sizeof(newfilename),gScriptProject,gBaseScriptDirectory),"ASSET.InsertPathElement failed during scrProgram::Load with %s",filename);
		filename = newfilename;
	}
	//fiStream *f = fiStream::Open(filename, true);
	fiStream *f = ASSET.Open(filename, "sco");
					
	if (f) {
		RAGE_TRACK(ScriptLoader);

		Header hdr;
		f->Read(&hdr.Magic,sizeof(hdr));
		Convert_LE(&hdr.Magic,1);
		Convert_BE(&hdr.GlobalsHash,11);

		if (hdr.Magic != SCR_MAGIC) {
			Errorf("%s - Old version of script format (header should be %x but is %x)?", scriptname, SCR_MAGIC, hdr.Magic);
			f->Close();
			return (scrProgramId)0;
		}

		AES aes(hdr.EncryptionKey);
		scrProgram *data = rage_new scrProgram(scriptname,hdr.OpcodeSize,hdr.NativeSize,hdr.StaticSize,hdr.GlobalSize,hdr.GlobalsBlock,hdr.StringHeapSize,hdr.ArgStructSize,hdr.GlobalsHash);

		int compSize = hdr.CompressedSize;
		Byte *compBuffer;
		if (isMem) {
			// Since apparently this buffer isn't freed when I expected, make sure we don't decrypt it more than once.
			compBuffer = atou(filename[9]=='x'?filename+10:filename+8);
			((Header*)compBuffer)->EncryptionKey = 0;
			compBuffer += sizeof(hdr);
		}
		else {
			compBuffer = rage_new Byte[compSize];
			f->Read(compBuffer, compSize);
		}
		if (hdr.EncryptionKey)
			aes.Decrypt(compBuffer,compSize);

		// BEGIN ZLIB DECOMPRESSION
		z_stream c_stream;
		memset(&c_stream,0,sizeof(c_stream));

		if (inflateInit(&c_stream) < 0) {
			Quitf(ERR_DEFAULT,"Error in inflateInit");
		}
		c_stream.next_in = compBuffer;
		c_stream.avail_in = (uInt) compSize;

		int zerr;
		#define inflateTo(c_stream,dest,destSize) \
			do { c_stream.next_out = (Byte*)(dest); c_stream.avail_out = (uInt) (destSize); \
			if ((destSize) && (zerr = inflate(&c_stream, Z_SYNC_FLUSH)) < 0) Quitf(ERR_DEFAULT,"Error %d in script inflate to %s reading %s(%s) (destSize=%u)",zerr,#dest,scriptname,scriptname,destSize); Assert(c_stream.avail_out == 0); } while (0)

		int opcodePage = 0;
		int opcodeSize = hdr.OpcodeSize;
		while (opcodeSize) {
			int toCopy = opcodeSize;
			if (toCopy > scrPageSize)
				toCopy = scrPageSize;
			inflateTo(c_stream,data->Table[opcodePage++],toCopy);
			opcodeSize -= toCopy;
		}

		for (u32 i=0; i<data->GetStringHeapCount(); i++)
			inflateTo(c_stream,data->StringHeaps[i],data->GetStringHeapChunkSize(i));
		inflateTo(c_stream,data->Natives,hdr.NativeSize*8);
		inflateTo(c_stream,data->Statics,hdr.StaticSize*4);

		// GetGlobalsPageChunkSize()*4 instead of sizeof(scrValue) is intentional here.  They're stored as u32's and expanded to u64's after byte swapping, below
		for (u32 i=0; i<data->GetGlobalsPageCount(); i++)
			inflateTo(c_stream,data->GlobalsTable[i],data->GetGlobalsPageChunkSize(i)*4);	
		inflateEnd(&c_stream);
		// END ZLIB COMPRESSION

		if (!isMem)
			delete[] compBuffer;
		f->Close();

		Convert_BE(data->Natives,hdr.NativeSize);
		Convert_BE(&data->Statics->Uns,hdr.StaticSize);
		for (u32 i=0; i<data->GetGlobalsPageCount(); i++)
			Convert_BE(&data->GlobalsTable[i]->Uns,data->GetGlobalsPageChunkSize(i));

#if __64BIT && !SCRVALUE_OFFSETS
		// Expand data to 64 bits *after* byte swapping.
		expandData(data->Statics,hdr.StaticSize);
		for (u32 i=0; i<data->GetGlobalsPageCount(); i++)
			expandData(data->GlobalsTable[i],data->GetGlobalsPageChunkSize(i));
#endif

		if (data->GlobalSizeAndBlock)
			data->SetGlobals();

		if (g_Validate && !scrThread::Validate(*data)) {
			Errorf("Script '%s' failed validation, it's using unsupported NATIVE commands",scriptname);
			AssertMsg(false,"You can ignore this assertion but the script will not work properly.");
		}

		// Resource compiler is opposite endian of some targets, so check the flag first so we
		// don't accidentally reswap data incorrectly.
#if __RESOURCECOMPILER
		if (!g_ByteSwap)
#endif
			scrThread::SwapForPlatform(*data);

		s_ProgHash.Insert(data);

		if (g_Validate && hdr.GlobalsHash && !s_GlobalsHashes[data->GlobalSizeAndBlock >> MAX_GLOBAL_BLOCKS_SHIFT])
			Quitf(ERR_DEFAULT,"Script '%s' requires globals that are not available.  Load the script with the GLOBALS TRUE directive first!",scriptname);

		return (scrProgramId)hash;
	}
	else
		return (scrProgramId)0;
}

#if __WIN32PC
#include "opcode.h"

static int GetInstructionSize(const scrProgram &opcode,int pc) {
	static u8 sizes[256] = {
#define OP(a,b,c) b
#include "opcodes.h"
#undef OP
	};
	int result = sizes[opcode[pc]];
	if (!result) switch (opcode[pc]) {
		// Weird cases:
		case OP_ENTER: 
			return 5 + opcode[pc+4];
		case OP_SWITCH:
			return 2+6*opcode[pc+1];
		default:
			Quitf(ERR_DEFAULT,"invalid insn (%d) in GetInstructionSize",opcode[pc]);
			return -1;
	}
	return result;
}


bool scrProgram::Save(const char *filename,int globalsBlock) {
	char scriptname[64];
	ASSET.BaseName(scriptname,sizeof(scriptname),ASSET.FileName(filename));
	u32 hash = scrComputeHash(scriptname);
	scrProgram *p = s_ProgHash.Lookup(hash);
	if (!p)
		return false;
	fiStream *f = ASSET.Create(filename,"sco");
	if (f) {
		scrProgram &pt = *p;
		struct native_temp { 
			u64 hash, idx; 
			bool operator<(const native_temp &that) const { return hash < that.hash; }
		};
		native_temp *tmp = Alloca(native_temp, pt.NativeSize);
		u16 *remap = Alloca(u16, pt.NativeSize);
		for (u32 i=0; i<pt.NativeSize; i++) {
			tmp[i].hash = pt.Natives[i];
			tmp[i].idx = i;
		}
		// Sort natives by increasing hash (which should be random).
		std::random_shuffle(tmp,tmp+pt.NativeSize);
		// Build a remap to its original slot
		for (u16 i=0; i<pt.NativeSize; i++) {
			remap[tmp[i].idx] = i;
			pt.Natives[i] = tmp[i].hash;
		}

		u32 pc = 0, opcodeSize = pt.OpcodeSize;
		while (pc != opcodeSize) {
			int is = GetInstructionSize(pt,pc);
			int op = pt[pc];
			if (op == OP_NATIVE) { // +2 is high byte, +3 is low byte
				u16 orig = (pt[pc+2] << 8) | pt[pc+3];
				orig = remap[orig];
				pt[pc+2] = u8(orig >> 8);
				pt[pc+3] = u8(orig);
			}
			Assertf((pc >> scrPageShift) == ((pc + is - 1) >> scrPageShift),"Insn at %x (opcode %d) crosses page boundary",pc,op);
			Assertf(((pc + is) & scrPageMask) || INSN_THAT_CAN_BE_AT_END_OF_PAGE(op),"Insn at %x (opcode %d) not allowed right before page boundary",pc,op);
			pc += is;
		}
		Header hdr;
		hdr.Magic = SCR_MAGIC;
		hdr.OpcodeSize = pt.OpcodeSize;
		hdr.StaticSize = pt.StaticSize;
		hdr.GlobalSize = globalsBlock != -1? sm_GlobalSizes[globalsBlock] : 0;
		hdr.ArgStructSize = pt.ArgStructSize;
		int hashBlock = globalsBlock==-1? 0 : globalsBlock;
//		hdr.GlobalsHash = sm_GlobalSizes[hashBlock]? atDataHash((char*)sm_Globals[hashBlock],sm_GlobalSizes[hashBlock]*4) : 0;
		hdr.GlobalsHash = sm_GlobalSizes[hashBlock]? CalculateGlobalHash(sm_Globals[hashBlock],sm_GlobalSizes[hashBlock]) : 0;
		hdr.NativeSize = pt.NativeSize;
		hdr.StringHeapSize = pt.StringHeapSize;
		hdr.GlobalsBlock = globalsBlock != -1? globalsBlock : 0;
		hdr.Pad[0] = 0;
		scrDebugf2("globalSize=%d, hash=%d",hdr.GlobalSize,hdr.GlobalsHash);

		AES aes;
		hdr.EncryptionKey = aes.GetKeyId();
		Assert(hdr.EncryptionKey);

		// Slap everything together into the same buffer for compression and encryption
		int sourceBufferSize = pt.OpcodeSize + pt.NativeSize*8 + pt.StaticSize*4 + (globalsBlock!=-1?(sm_GlobalSizes[hdr.GlobalsBlock]*4):0) + pt.StringHeapSize;
		Byte *sourceBuffer = rage_new Byte[sourceBufferSize];
		int compBufferSize = sourceBufferSize * 2;
		Byte *compBuffer = rage_new Byte[compBufferSize];
		for (u32 i=0; i<pt.OpcodeSize; i++)
			sourceBuffer[i] = pt[i];
		Byte *d = sourceBuffer + pt.OpcodeSize;
		for (u32 i=0; i<pt.GetStringHeapCount(); i++, d+=scrStringSize)
			memcpy(d, pt.StringHeaps[i], pt.GetStringHeapChunkSize(i));
		Byte *natives = sourceBuffer + pt.OpcodeSize + pt.StringHeapSize;
		memcpy(natives, pt.Natives, pt.NativeSize * 8);
		Convert_BE((u64*)natives,pt.NativeSize);

		Byte *statics = natives + pt.NativeSize*8;
//		memcpy(statics, pt.Statics, pt.StaticSize * 4);
		u32 *pStaticsU32 = (u32*)statics;
		for (u32 statics_loop=0; statics_loop<pt.StaticSize;statics_loop++)
		{
			*pStaticsU32 = (u32)pt.Statics[statics_loop].Int;
			pStaticsU32++;
		}
		Convert_BE((u32*)statics,pt.StaticSize);

		Byte *globals = statics + pt.StaticSize * 4;
		if (globalsBlock != -1) {
//			memcpy(globals, sm_Globals[hdr.GlobalsBlock], sm_GlobalSizes[hdr.GlobalsBlock]*4);
			u32 *pGlobalsU32 = (u32*)globals;
			for (s32 globals_loop=0; globals_loop<sm_GlobalSizes[hdr.GlobalsBlock]; globals_loop++)
			{
				*pGlobalsU32 = (u32)sm_Globals[hdr.GlobalsBlock][globals_loop].Int;
				pGlobalsU32++;
			}
			Convert_BE((u32*)globals,sm_GlobalSizes[hdr.GlobalsBlock]);
		}

		// BEGIN ZLIB COMPRESSION
		z_stream c_stream;
		memset(&c_stream,0,sizeof(c_stream));

		int compressionQuality = Z_BEST_COMPRESSION;
		if (deflateInit(&c_stream, compressionQuality) < 0) {
			Quitf(ERR_DEFAULT,"Error in deflateInit");
		}
		c_stream.next_in = sourceBuffer;
		c_stream.avail_in = (uInt) sourceBufferSize;
		c_stream.next_out = compBuffer;
		c_stream.avail_out = (uInt) compBufferSize;
		if (deflate(&c_stream, Z_FINISH) < 0) {
			Quitf(ERR_DEFAULT,"Error in deflate (virtual)");
		}
		if (c_stream.avail_in) {
			Quitf(ERR_DEFAULT,"deflate didn't consume all input (virtual)?");
		}
		deflateEnd(&c_stream);
		// END ZLIB COMPRESSION

		int compSize = (int) (compBufferSize - c_stream.avail_out);
		scrDebugf2("Script '%s' %d/%d, compressed to %d%% of original size",filename,compSize,sourceBufferSize,(compSize*100)/sourceBufferSize);

		hdr.CompressedSize = compSize;

		Convert_LE(&hdr.Magic,1);
		Convert_BE(&hdr.GlobalsHash,11);
		f->Write(&hdr.Magic,sizeof(hdr));

		/* char *test = rage_new char[compSize];
		memcpy(test,compBuffer,compSize); */

		aes.Encrypt(compBuffer,compSize);
		f->Write(compBuffer,compSize);

		/* for (int i=0; i<(compSize & ~15); i+=16)
			aes.Encrypt(test + i, 16);
		if (memcmp(compBuffer,test,compSize & ~15))
			Quitf("crap.");
		delete[] test; */

		delete[] compBuffer;
		delete[] sourceBuffer;

		f->Close();
		return true;
	}
	else
		return false;
}
#endif

#if !__FINAL

void scrProgram::SetBreakpoint( int offset, bool setOrUnSet, bool stopsGame)
{
	if (!m_programBreakpoints)
		m_programBreakpoints = rage_new atMap<s32,bool>;
	if ( setOrUnSet )
    {
        bool *pVal = m_programBreakpoints->Access( offset );
        if ( pVal == NULL )
        {
            m_programBreakpoints->operator[](offset) = stopsGame;
            pVal = m_programBreakpoints->Access( offset );
        }

        *pVal = stopsGame;
    }
    else
    {
        m_programBreakpoints->Delete( offset );
    }
}

bool scrProgram::HasBreakpoint( int offset, bool *stopsGame)
{
	if (!m_programBreakpoints)
		return false;

    bool *pVal = m_programBreakpoints->Access( offset );

    if ( stopsGame != NULL )
    {
        *stopsGame = (pVal != NULL) && *pVal;
    }

    return pVal != NULL;
}

#endif // !__FINAL

bool scrProgram::Validate(scrProgramId progId) {
	scrProgram *p = GetProgram(progId);
	if (!p)
		return false;
	scrThread::SwapForPlatform(*p);
	return scrThread::Validate(*p);
}


scrProgram* scrProgram::GetProgram(scrProgramId progId) {
	return s_ProgHash.Lookup(progId);
}


#if EXECUTABLE_SCRIPTS
extern scrHash<scrCmd> s_CommandHash;

static scrCmd scr_resolver(unsigned hashcode) {
	// be careful what is called here, this is invoked from prx startup code.
	// even a Displayf seemed to cause mayhem.
	scrCmd cmd = s_CommandHash.Lookup(hashcode);
	// printf("scr_resolver(%x) -> %p\n",hashcode,cmd);
	return cmd;
}


bool scrProgram::RegisterBinary(const char *name) {
	script_args args = { sm_Globals, scr_resolver, 0, NULL };
	AssertMsg(sm_Globals,"Call RegisterBinary after you've initialized globals!");

#if __PS3
	char prxPath[256];
	// TODO: Final build needs more thought
	strcpy(prxPath,"/app_home/");
	ASSET.FullPath(prxPath+10,sizeof(prxPath)-10,name,"sprx");
	sys_prx_id_t hPrx = sys_prx_load_module(prxPath, 0, NULL);
	if (hPrx <= 0)
		return false;
	int modres = -1;

	if (sys_prx_start_module(hPrx, sizeof(args), &args, &modres, 0, NULL) < 0) {
		Errorf("sys_prx_start_module failed");
		sys_prx_unload_module(hPrx, 0, NULL);
		return false;
	}
#elif __WIN32
	char prxPath[256];
#if __WIN32PC
	ASSET.FullPath(prxPath,sizeof(prxPath),name,"dll");
#else
	formatf(prxPath,"game:\\%s.dll",name);
#endif
	sysLibrary *hInst = sysLoadLibrary(prxPath);
	if (!hInst)
		return 0;

	void (*start_script)(unsigned,script_args*) = 
		(void(*)(unsigned,script_args*)) sysGetProcAddress(hInst, __XENON? (const char*)1 : "start_script");

	if (start_script)
		start_script(sizeof(args), &args);
	else
		Errorf("RegisterBinary(%s) - bad DLL?",name);
#endif

	for (int i=0; i<args.out_script_def_count; i++)
		s_ProgHash.Insert(rage_new scrProgram(*args.out_script_defs[i]));
	return true;
}


scrProgram::scrProgram(const script_def &def) {
	// statics_size is in bytes.  If there are no statics, the size of an empty struct is 1, hence the test below.
	// If there are any statics -- even one -- the size will be at least sizeof(scrValue) instead.
	if (def.statics_size > 1) {
		StaticSize = def.statics_size / sizeof(scrValue);
		Statics = rage_new scrValue[StaticSize];
		memset(Statics, 0, def.statics_size);
		def.construct_statics(Statics);
	}
	else {
		Statics = NULL;
		StaticSize = 0;
	}
	Table = NULL;

	GlobalsHash = def.globals_hash;
	OpcodeSize = 0;
	ArgStructSize = def.arg_struct_size;
	GlobalSize = 0;
	NativeSize = 0;
	Globals = NULL;
	Natives = (u32*) def.script_entry;		// this is a function pointer.
	ProcCount = 0;
	ProcNames = NULL;
	HashCode = scrComputeHash(def.script_name);
	RefCount = 1;
	ScriptName = def.script_name;
	StringHeaps = NULL;
	StringHeapSize = 0;
	m_programBreakpoints = NULL;

	if (GlobalsHash && s_GlobalsHash && GlobalsHash != s_GlobalsHash)
	{
		if (!sm_AllowMismatchedGlobals)
			Quitf("scrProgram - Script '%s' %u has different globals signature than '%s' %u", ScriptName.c_str(), GlobalsHash, s_GlobalsScript.c_str(), s_GlobalsHash);
		else
			Errorf("scrProgram - Script '%s' %u has different globals signature than '%s' %u", ScriptName.c_str(), GlobalsHash, s_GlobalsScript.c_str(), s_GlobalsHash);
	}
}
#endif


#if EXECUTABLE_SCRIPTS
scrProgramId scrProgram::IsCompiledAndResident(const char *scriptName) {
	u32 hash = scrComputeHash(scriptName);
	scrProgram *p = s_ProgHash.Lookup(hash);
	return (scrProgramId)(p && p->IsCompiled()? hash : 0);
}
#endif

#if USE_PROFILER_NORMAL
void RockyCaptureControlScriptCallback(Profiler::ControlState state, u32 mode) {
	if ((mode & Profiler::Mode::SCRIPTS) != 0 && state == Profiler::ROCKY_START) {
		s_ProgHash.GenerateProfilerDescriptions();
	}
}
#endif


void scrProgram::InitClass()
{
	if(PARAM_scriptproject.Get())
	{
		PARAM_scriptproject.GetParameter( gScriptProject );
	}

#if !ENCRYPT_GLOBALS && (RSG_PS3 || RSG_XENON)		// Only do this crap on current gen consoles
	// OPTIMIZATION: Reuse the same memory block to avoid fragmentation	
	void* ptr = sysMemManager::GetInstance().GetGlobalScriptMemory();

	if (!ptr)
	{					
		sysMemAutoUseFlexMemory flex;
		ptr = rage_new scrValue[SCRIPT_GLOBAL_COUNT];
		scrAssertf(ptr, "Script Globals are NULL! We're fucked...");
		sysMemManager::GetInstance().SetGlobalScriptMemory(ptr);
	}
#endif

#if USE_PROFILER_NORMAL
	Profiler::RegisterCaptureControlCallback(RockyCaptureControlScriptCallback);
#endif
}

void scrProgram::ShutdownClass()
{
#if !ENCRYPT_GLOBALS && (RSG_PS3 || RSG_XENON)		// Only do this crap on current gen consoles
	// OPTIMIZATION: Reuse the same memory block to avoid fragmentation
	void* ptr = sysMemManager::GetInstance().GetGlobalScriptMemory();

	if (ptr)
	{					
		sysMemAutoUseFlexMemory flex;
#if RSG_ORBIS	// TODO: remove temp fix after EJ reviews
		delete [] (char*)ptr;
#else
		delete [] ptr;
#endif // RSG_ORBIS
		sysMemManager::GetInstance().SetGlobalScriptMemory(NULL);
	}
#endif

#if USE_PROFILER_NORMAL
	Profiler::UnRegisterCaptureControlCallback(RockyCaptureControlScriptCallback);
#endif
}
