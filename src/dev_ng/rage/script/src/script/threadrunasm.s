	 #	"SN ARM_C++ Front End <UNK>; Back End 4.112 SN Systems 280.2.00 Jul 29 2009"
	#.file	"threadrun.cpp"
 #	Options: -XYc=cp+gnu_ext+rtti-exceptions+bool+wchar_t+array_nd+tmplname -O2 -Xswbr=0 -Xassumecorrectalignment=1 -Xassumecorrectsign=1 -Xsmallabi=1 -Xsmallppu=1 -Xnotocrestore=2 --display_error_number -D_BIG_ENDIAN -D__ALTIVEC__ -f
	.ident	"SN Systems ps3ppusnc - 'ps3ppucf.exe' 280.2.5785.0 (rel,ps3,280.2,ppu @60441 #301899 ) WIN32"
	.section	".toc","aw"
	.text
	.align	2

	.globl	._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj
	.section	".opd","aw"
	.align	3
_ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj:
	.long	._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj,.TOC.@tocbase32
	.size	_ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj,8
	.text
	.align	2
	.globl	_ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj

# rodata for _ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj
	.align	2
_ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj$rodata:
	.long	0x00000000
	.size	_ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj$rodata, 4

	.align	3


	.type	._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj,@function
._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj:
	stdu	%sp,-320(%sp)
	mflr	%r0
	std	%r0,336(%sp)
	std	%r31,312(%sp)
	ori	%r31,%r3,0
	std	%r30,304(%sp)
	lwz	%r3,8(%r6)
	std	%r29,296(%sp)
	ori	%r30,%r4,0
	ori	%r29,%r6,0
	cmpwi	1,%r3,3
	cmpwi	0,%r3,2
	std	%r28,288(%sp)
	std	%r27,280(%sp)
	std	%r26,272(%sp)
	std	%r25,264(%sp)
	std	%r24,256(%sp)
	std	%r23,248(%sp)
	cror	2,2,6
	beq	0, ..L.3 # bc 12,2
	lwz	%r3,20(%r29)		// r29 = serialized state
	lwz	%r4,16(%r29)
	subic	%r28,%r5,1
	rlwinm	%r3,%r3,2,0,29
	lwz	%r25,12(%r29)		// 
	rlwinm	%r26,%r4,2,0,29
	addc	%r27,%r31,%r3	// r31 = stack
	addc	%r26,%r31,%r26
	addc	%r25,%r28,%r25	// r28 = opcodes
	subic	%r3,%r27,4		// r3 = sp (r27 was next sp)
	ori	%r24,%r25,0		// r24 = pc (r25 was next pc)
	addis	%r27,%r0,.CSW_1@ha
	addic	%r27,%r27,.CSW_1@l	// r27 = jump table base
	b	..L.4			// r26 = fp
..L.3:
	b	..LCRET._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj
..L.4:
	lbzu	%r5,1(%r24)			// fetch next opcode, with update
	rlwinm	%r5,%r5,2,0,29		// shift left twice
	lwzx	%r5,%r27,%r5		// read from jump table
	mtctr	%r5					// fetch target (do as soon as possible)
	bcctr	20,0				// jump to counter.
..L.6:
	addi	%r3,%r0,2
	stw	%r3,8(%r29)
	b	..LCRET._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj
..L.7:
	b	..L.4
..L.8:
	lwz	%r4,-4(%r3)
	lwz	%r5,0(%r3)
	subic	%r27,%r27,4
	addc	%r4,%r4,%r5
	stw	%r4,-4(%r3)
	b	..L.4
..L.9:
	lwz	%r4,-4(%r3)
	lwz	%r5,0(%r3)
	subic	%r27,%r27,4
	subfc	%r4,%r5,%r4
	stw	%r4,-4(%r3)
	b	..L.4
..L.10:
	lwz	%r4,-4(%r3)
	lwz	%r5,0(%r3)
	subic	%r27,%r27,4
	mullw	%r4,%r4,%r5
	stw	%r4,-4(%r3)
	b	..L.4
..L.11:
	lwz	%r4,0(%r3)
	subic	%r27,%r27,4
	cmpwi	0,%r4,0
	beq	0, ..L.4 # bc 12,2
	lwz	%r5,-4(%r3)
	divw	%r4,%r5,%r4
	stw	%r4,-4(%r3)
	b	..L.4
..L.13:
	lwz	%r4,0(%r3)
	subic	%r27,%r27,4
	cmpwi	0,%r4,0
	beq	0, ..L.4 # bc 12,2
	lwz	%r5,-4(%r3)
	divw	%r6,%r5,%r4
	mullw	%r4,%r6,%r4
	subfc	%r4,%r4,%r5
	stw	%r4,-4(%r3)
	b	..L.4
..L.15:
	lwz	%r3,0(%r27)
	cntlzw	%r3,%r3
	rlwinm	%r3,%r3,27,31,31
	stw	%r3,0(%r27)
	b	..L.4
..L.16:
	lwz	%r3,0(%r27)
	neg	%r3,%r3
	stw	%r3,0(%r27)
	b	..L.4
..L.17:
	lwz	%r5,-4(%r3)
	addi	%r4,%r0,0
	lwz	%r6,0(%r3)
	subic	%r27,%r27,4
	cmpw	0,%r5,%r6
	beq	0, ..L.19 # bc 12,2
..L.18:
	stw	%r4,-4(%r3)
	b	..L.4
..L.19:
	addi	%r4,%r0,1
	b	..L.18
..L.20:
	lwz	%r5,-4(%r3)
	addi	%r4,%r0,0
	lwz	%r6,0(%r3)
	subic	%r27,%r27,4
	cmpw	0,%r5,%r6
	bne	0, ..L.22 # bc 4,2
..L.21:
	stw	%r4,-4(%r3)
	b	..L.4
..L.22:
	addi	%r4,%r0,1
	b	..L.21
..L.23:
	lwz	%r5,-4(%r3)
	addi	%r4,%r0,0
	lwz	%r6,0(%r3)
	subic	%r27,%r27,4
	cmpw	0,%r5,%r6
	bge	0, ..L.25 # bc 4,0
..L.24:
	stw	%r4,-4(%r3)
	b	..L.4
..L.25:
	addi	%r4,%r0,1
	b	..L.24
..L.26:
	lwz	%r5,-4(%r3)
	addi	%r4,%r0,0
	lwz	%r6,0(%r3)
	subic	%r27,%r27,4
	cmpw	0,%r5,%r6
	bgt	0, ..L.28 # bc 12,1
..L.27:
	stw	%r4,-4(%r3)
	b	..L.4
..L.28:
	addi	%r4,%r0,1
	b	..L.27
..L.29:
	lwz	%r5,-4(%r3)
	addi	%r4,%r0,0
	lwz	%r6,0(%r3)
	subic	%r27,%r27,4
	cmpw	0,%r5,%r6
	ble	0, ..L.31 # bc 4,1
..L.30:
	stw	%r4,-4(%r3)
	b	..L.4
..L.31:
	addi	%r4,%r0,1
	b	..L.30
..L.32:
	lwz	%r5,-4(%r3)
	addi	%r4,%r0,0
	lwz	%r6,0(%r3)
	subic	%r27,%r27,4
	cmpw	0,%r5,%r6
	blt	0, ..L.34 # bc 12,0
..L.33:
	stw	%r4,-4(%r3)
	b	..L.4
..L.34:
	addi	%r4,%r0,1
	b	..L.33
..L.35:
	lfs	%f1,-4(%r3)
	subic	%r27,%r27,4
	lfs	%f2,0(%r3)
	fadds	%f1,%f1,%f2
	stfs	%f1,-4(%r3)
	b	..L.4
..L.36:
	lfs	%f1,-4(%r3)
	subic	%r27,%r27,4
	lfs	%f2,0(%r3)
	fsubs	%f1,%f1,%f2
	stfs	%f1,-4(%r3)
	b	..L.4
..L.37:
	lfs	%f1,-4(%r3)
	subic	%r27,%r27,4
	lfs	%f2,0(%r3)
	fmuls	%f1,%f1,%f2
	stfs	%f1,-4(%r3)
	b	..L.4
..L.38:
	lwz	%r4,0(%r3)
	subic	%r27,%r27,4
	cmpwi	0,%r4,0
	beq	0, ..L.4 # bc 12,2
	lfs	%f1,-4(%r3)
	lfs	%f2,0(%r3)
	fdivs	%f1,%f1,%f2
	stfs	%f1,-4(%r3)
	b	..L.4
..L.40:
	lwz	%r4,0(%r3)
	subic	%r27,%r27,4
	cmpwi	0,%r4,0
	beq	0, ..L.4 # bc 12,2
	addis	%r4,%r0,_ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj$rodata@ha
	lfs	%f2,0(%r3)
	addi	%r4,%r4,_ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj$rodata@l
	lfs	%f3,0(%r4)
	lfs	%f1,-4(%r3)
	fcmpu	0,%f2,%f3
	beq	0, ..L.43 # bc 12,2
	fdivs	%f3,%f1,%f2
	fctiwz	%f3,%f3
	fcfid	%f3,%f3
	fnmsubs	%f3,%f3,%f2,%f1
..L.43:
	stfs	%f3,-4(%r3)
	b	..L.4
..L.44:
	addis	%r3,%r0,-32768
	lwz	%r4,0(%r27)
	xor	%r3,%r4,%r3
	stw	%r3,0(%r27)
	b	..L.4
..L.45:
	lfs	%f1,-4(%r3)
	lfs	%f2,0(%r3)
	addi	%r4,%r0,0
	subic	%r27,%r27,4
	fcmpu	0,%f1,%f2
	bne	0, ..L.47 # bc 4,2
	addi	%r4,%r0,1
..L.47:
	extsw	%r4,%r4
	std	%r4,224(%sp)
	lfd	%f1,224(%sp)
	fcfid	%f1,%f1
	frsp	%f1,%f1
	stfs	%f1,-4(%r3)
	b	..L.4
..L.48:
	lfs	%f1,-4(%r3)
	lfs	%f2,0(%r3)
	addi	%r4,%r0,0
	subic	%r27,%r27,4
	fcmpu	0,%f1,%f2
	beq	0, ..L.50 # bc 12,2
	addi	%r4,%r0,1
..L.50:
	extsw	%r4,%r4
	std	%r4,224(%sp)
	lfd	%f1,224(%sp)
	fcfid	%f1,%f1
	frsp	%f1,%f1
	stfs	%f1,-4(%r3)
	b	..L.4
..L.51:
	lfs	%f1,-4(%r3)
	lfs	%f2,0(%r3)
	addi	%r4,%r0,0
	subic	%r27,%r27,4
	fcmpu	0,%f1,%f2
	cror	6,1,2
	beq	1, ..L.52 # bc 12,6
	b	..L.53
..L.52:
	addi	%r4,%r0,1
..L.53:
	extsw	%r4,%r4
	std	%r4,224(%sp)
	lfd	%f1,224(%sp)
	fcfid	%f1,%f1
	frsp	%f1,%f1
	stfs	%f1,-4(%r3)
	b	..L.4
..L.54:
	lfs	%f1,-4(%r3)
	lfs	%f2,0(%r3)
	addi	%r4,%r0,0
	subic	%r27,%r27,4
	fcmpu	0,%f1,%f2
	bgt	0, ..L.55 # bc 12,1
	b	..L.56
..L.55:
	addi	%r4,%r0,1
..L.56:
	extsw	%r4,%r4
	std	%r4,224(%sp)
	lfd	%f1,224(%sp)
	fcfid	%f1,%f1
	frsp	%f1,%f1
	stfs	%f1,-4(%r3)
	b	..L.4
..L.57:
	lfs	%f1,-4(%r3)
	addi	%r4,%r0,0
	lfs	%f2,0(%r3)
	subic	%r27,%r27,4
	fcmpu	0,%f1,%f2
	cror	6,0,2
	beq	1, ..L.58 # bc 12,6
	b	..L.59
..L.58:
	addi	%r4,%r0,1
..L.59:
	extsw	%r4,%r4
	std	%r4,224(%sp)
	lfd	%f1,224(%sp)
	fcfid	%f1,%f1
	frsp	%f1,%f1
	stfs	%f1,-4(%r3)
	b	..L.4
..L.60:
	lfs	%f1,-4(%r3)
	addi	%r4,%r0,0
	lfs	%f2,0(%r3)
	subic	%r27,%r27,4
	fcmpu	0,%f1,%f2
	blt	0, ..L.61 # bc 12,0
	b	..L.62
..L.61:
	addi	%r4,%r0,1
..L.62:
	extsw	%r4,%r4
	std	%r4,224(%sp)
	lfd	%f1,224(%sp)
	fcfid	%f1,%f1
	frsp	%f1,%f1
	stfs	%f1,-4(%r3)
	b	..L.4
..L.63:
	lfs	%f1,-20(%r3)
	lfs	%f2,-8(%r3)
	subic	%r27,%r27,12
	lfs	%f3,-16(%r3)
	fadds	%f1,%f1,%f2
	lfs	%f2,-4(%r3)
	lfs	%f4,-12(%r3)
	fadds	%f2,%f3,%f2
	lfs	%f3,0(%r3)
	fadds	%f3,%f4,%f3
	stfs	%f1,-20(%r3)
	stfs	%f2,-16(%r3)
	stfs	%f3,-12(%r3)
	b	..L.4
..L.64:
	lfs	%f1,-20(%r3)
	lfs	%f2,-8(%r3)
	subic	%r27,%r27,12
	lfs	%f3,-16(%r3)
	fsubs	%f1,%f1,%f2
	lfs	%f2,-4(%r3)
	lfs	%f4,-12(%r3)
	fsubs	%f2,%f3,%f2
	lfs	%f3,0(%r3)
	fsubs	%f3,%f4,%f3
	stfs	%f1,-20(%r3)
	stfs	%f2,-16(%r3)
	stfs	%f3,-12(%r3)
	b	..L.4
..L.65:
	lfs	%f1,-20(%r3)
	lfs	%f2,-8(%r3)
	subic	%r27,%r27,12
	lfs	%f3,-16(%r3)
	fmuls	%f1,%f1,%f2
	lfs	%f2,-4(%r3)
	lfs	%f4,-12(%r3)
	fmuls	%f2,%f3,%f2
	lfs	%f3,0(%r3)
	fmuls	%f3,%f4,%f3
	stfs	%f1,-20(%r3)
	stfs	%f2,-16(%r3)
	stfs	%f3,-12(%r3)
	b	..L.4
..L.66:
	lwz	%r4,-8(%r3)
	subic	%r27,%r27,12
	cmpwi	0,%r4,0
	beq	0, ..L.68 # bc 12,2
	lfs	%f1,-20(%r3)
	lfs	%f2,-8(%r3)
	fdivs	%f1,%f1,%f2
	stfs	%f1,-20(%r3)
..L.68:
	lwz	%r4,-4(%r3)
	cmpwi	0,%r4,0
	beq	0, ..L.70 # bc 12,2
	lfs	%f1,-16(%r3)
	lfs	%f2,-4(%r3)
	fdivs	%f1,%f1,%f2
	stfs	%f1,-16(%r3)
..L.70:
	lwz	%r4,0(%r3)
	cmpwi	0,%r4,0
	beq	0, ..L.4 # bc 12,2
	lfs	%f1,-12(%r3)
	lfs	%f2,0(%r3)
	fdivs	%f1,%f1,%f2
	stfs	%f1,-12(%r3)
	b	..L.4
..L.72:
	addis	%r3,%r0,-32768
	lwz	%r4,-8(%r27)
	lwz	%r5,-4(%r27)
	xor	%r4,%r4,%r3
	lwz	%r6,0(%r27)
	xor	%r5,%r5,%r3
	xor	%r3,%r6,%r3
	stw	%r4,-8(%r27)
	stw	%r5,-4(%r27)
	stw	%r3,0(%r27)
	b	..L.4
..L.73:
	lwz	%r4,-4(%r3)
	lwz	%r5,0(%r3)
	subic	%r27,%r27,4
	and	%r4,%r4,%r5
	stw	%r4,-4(%r3)
	b	..L.4
..L.74:
	lwz	%r4,-4(%r3)
	lwz	%r5,0(%r3)
	subic	%r27,%r27,4
	or	%r4,%r4,%r5
	stw	%r4,-4(%r3)
	b	..L.4
..L.75:
	lwz	%r4,-4(%r3)
	lwz	%r5,0(%r3)
	subic	%r27,%r27,4
	xor	%r4,%r4,%r5
	stw	%r4,-4(%r3)
	b	..L.4
..L.76:
	lwz	%r3,0(%r27)
	extsw	%r3,%r3
	std	%r3,224(%sp)
	lfd	%f1,224(%sp)
	fcfid	%f1,%f1
	frsp	%f1,%f1
	stfs	%f1,0(%r27)
	b	..L.4
..L.77:
	lfs	%f1,0(%r27)
	fctiwz	%f1,%f1
	stfiwx	%f1,%r0,%r27
	b	..L.4
..L.78:
	addic	%r27,%r27,8
	lwz	%r3,0(%r3)
	stw	%r3,0(%r27)
	stw	%r3,-4(%r27)
	b	..L.4
..L.79:		// PUSH_CONST_U8
	lbzu	%r5,2(%r24)			// fetch next opcode, with update
	rlwinm	%r5,%r5,2,0,29		// shift left twice
	lwzx	%r5,%r27,%r5		// read from jump table
	mtctr	%r5					// fetch target (do as soon as possible)

	lbz		%r4,-1(%r24)		// get constant from current insn
	stwu	%r4,4(%r3)

	bcctr	20,0				// jump to counter.
..L.80:
	lbz	%r4,2(%r24)
	addic	%r25,%r24,3
	stw	%r4,4(%r3)
	lbz	%r4,3(%r24)
	addic	%r27,%r27,8
	stw	%r4,8(%r3)
	b	..L.4
..L.81:
	lbz	%r4,2(%r24)
	addic	%r25,%r24,4
	addic	%r27,%r27,12
	stw	%r4,4(%r3)
	lbz	%r4,3(%r24)
	stw	%r4,8(%r3)
	lbz	%r4,4(%r24)
	stw	%r4,12(%r3)
	b	..L.4
..L.82:		// PUSH_CONST_U32
	lbzu	%r5,5(%r24)			// fetch next opcode, with update
	rlwinm	%r5,%r5,2,0,29		// shift left twice
	lwzx	%r5,%r27,%r5		// read from jump table
	mtctr	%r5					// fetch target (do as soon as possible)

	lwz		%r4,-4(%r24)
	stwu	%r4,4(%r3)
	
	bcctr	20,0				// jump to counter.
..L.83:
	addic	%r27,%r27,4
	lwz	%r3,0(%r3)
	stw	%r3,0(%r27)
	b	..L.4
..L.84:
	subic	%r27,%r27,4
	b	..L.4
..L.85:
	ori	%r3,%r24,0
	addic	%r25,%r3,7
	lbz	%r24,2(%r3)
	lbz	%r23,3(%r3)
	lwz	%r3,4(%r3)
	cmpwi	1,%r23,0
	cmpwi	0,%r3,0
	beq	0, ..L.4 # bc 12,2
	subfc	%r4,%r31,%r27
	addi	%r5,%r0,0
	srawi	%r4,%r4,2
	addze	%r4,%r4
	addic	%r4,%r4,1
	stw	%r4,20(%r29)
	beq	1, ..L.88 # bc 12,6
	subfc	%r5,%r24,%r4
	sldi	%r5,%r5,2
	addc	%r5,%r31,%r5
..L.88:
	ori	%r6,%r3,0
	subfc	%r3,%r24,%r4
	stw	%r5,112(%sp)
	addi	%r4,%r0,0
	stw	%r24,116(%sp)
	sldi	%r3,%r3,2
	addc	%r7,%r31,%r3
	addic	%r3,%sp,112
	stw	%r7,120(%sp)
	stw	%r4,124(%sp)
	lwz	%r4,0(%r6)
	mtctr	%r4
	bcctrl	20,30
	lwz	%r3,8(%r29)
	cmpwi	0,%r3,0
	bne	0, ..L.90 # bc 4,2
	addic	%r3,%sp,112
	bl	._ZN4rage9scrThread4Info27CopyReferencedParametersOutEv		
	#nop
	subfc	%r3,%r23,%r24
	sldi	%r3,%r3,2
	subfc	%r27,%r3,%r27
	b	..L.4
..L.90:
	subfc	%r5,%r31,%r26
	subfc	%r4,%r28,%r25
	srawi	%r5,%r5,2
	addze	%r5,%r5
	subic	%r4,%r4,7
	stw	%r4,12(%r29)
	stw	%r5,16(%r29)
	b	..LCRET._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj
..L.91:		// ENTER
	subfc	%r4,%r31,%r26		// fp - stack
	lbz		%r6,1(%r24)			// paramCount
	lhz		%r5,2(%r24)			// localCount
	lbz		%r7,4(%r24)			// nameCount (and update r24)
	srawi	%r4,%r4,2			// (fp - stack) >> 2
	addc	%r24,%r24,%r7		// pc += nameCount
	addic	%r24,%r24,4
	stwu	%r4,4(%r3)			// store previous fp to stack
	rlwinm	%r7,%r6,2,0,29		// paramCount<<2
	cmpwi	0,%r5,0				// check localCount
	subfc	%r26,%r7,%r3		// fp = sp - paramCount
	subic	%r26,%r26,4			// fp = sp - paramCount - 1
	beq	0, ..L.94 # bc 12,2
	addi	%r4,%r0,0
	mtctr	%r5
..L.93:
	stwu	%r4,4(%r3)
	bc	16,0,..L.93
..L.94:
	subfc	%r3,%r7,%r3		// sp = sp - paramCount
	b	..L.4
..L.95:		// LEAVE
	b	..LCRET._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj		// HACK HACK HACK
	lbz	%r3,2(%r24)
	lbz	%r5,3(%r24)
	rlwinm	%r3,%r3,2,0,29
	rlwinm	%r24,%r5,2,0,29
	addc	%r6,%r26,%r3
	subfc	%r24,%r24,%r27
	subfc	%r27,%r3,%r6
	lwzx	%r25,%r4,%r3
	cmpwi	0,%r5,0
	subic	%r27,%r27,4
	lwz	%r4,4(%r6)
	addc	%r25,%r28,%r25
	rlwinm	%r26,%r4,2,0,29
	subic	%r3,%r5,1
	addc	%r26,%r31,%r26
	bne	0, ..L.98 # bc 4,2
..L.96:
	cmpw	0,%r25,%r28
	bne	0, ..L.4 # bc 4,2
	addi	%r3,%r0,2
	stw	%r3,8(%r29)
	b	..LCRET._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj
..L.98:
	addic	%r3,%r3,1
	mtctr	%r3
..L.99:
	addic	%r27,%r27,4
	lwz	%r4,4(%r24)
	addic	%r24,%r24,4
	stw	%r4,0(%r27)
	bc	16,0,..L.99
	b	..L.96
..L.100:
	lwz	%r3,0(%r27)
	lwz	%r3,0(%r3)
	stw	%r3,0(%r27)
	b	..L.4
..L.101:
	lwz	%r4,0(%r3)
	lwz	%r3,-4(%r3)
	subic	%r27,%r27,8
	stw	%r3,0(%r4)
	b	..L.4
..L.102:
	lwz	%r4,-4(%r3)
	subic	%r27,%r27,4
	lwz	%r3,0(%r3)
	stw	%r3,0(%r4)
	b	..L.4
..L.103:
	lwz	%r4,-4(%r3)
	lwz	%r3,0(%r27)
	cmpwi	0,%r4,0
	subic	%r27,%r27,8
	beq	0, ..L.4 # bc 12,2
	ori	%r5,%r4,0
	addi	%r4,%r0,0
	mtctr	%r5
..L.105:
	addic	%r27,%r27,4
	lwzx	%r5,%r3,%r4
	addic	%r4,%r4,4
	stw	%r5,0(%r27)
	bc	16,0,..L.105
	b	..L.4
..L.106:
	lwz	%r4,-4(%r3)
	lwz	%r3,0(%r27)
	cmpwi	0,%r4,0
	subic	%r27,%r27,8
	beq	0, ..L.4 # bc 12,2
	mtctr	%r4
..L.108:
	subic	%r5,%r4,1
	lwz	%r6,0(%r27)
	subic	%r4,%r4,1
	sldi	%r5,%r5,2
	subic	%r27,%r27,4
	stwx	%r6,%r3,%r5
	bc	16,0,..L.108
	b	..L.4
..L.109:
	lwz	%r4,0(%r3)
	lwz	%r5,-4(%r3)
	subic	%r27,%r27,4
	lwz	%r6,0(%r4)
	cmplw	0,%r5,%r6
	bge	0, ..L.111 # bc 4,0
	lbz	%r6,2(%r24)
	mullw	%r5,%r5,%r6
	addic	%r25,%r24,2
	addic	%r5,%r5,1
	rlwinm	%r5,%r5,2,0,29
	addc	%r4,%r4,%r5
	stw	%r4,-4(%r3)
	b	..L.4
..L.111:
	addi	%r3,%r0,2
	stw	%r3,8(%r29)
	b	..LCRET._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj
..L.112:
	ori	%r4,%r3,0
	subic	%r27,%r27,4
	lwz	%r3,0(%r4)
	lwz	%r4,-4(%r4)
	lwz	%r5,0(%r3)
	cmplw	0,%r4,%r5
	bge	0, ..L.114 # bc 4,0
	lbz	%r5,2(%r24)
	addic	%r25,%r24,2
	mullw	%r4,%r4,%r5
	addic	%r4,%r4,1
	rlwinm	%r4,%r4,2,0,29
	lwzx	%r3,%r3,%r4
	stw	%r3,0(%r27)
	b	..L.4
..L.114:
	addi	%r3,%r0,2
	stw	%r3,8(%r29)
	b	..LCRET._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj
..L.115:
	lwz	%r4,0(%r3)
	subic	%r27,%r27,12
	lwz	%r5,-4(%r3)
	lwz	%r6,0(%r4)
	cmplw	0,%r5,%r6
	bge	0, ..L.117 # bc 4,0
	lbz	%r6,2(%r24)
	addic	%r25,%r24,2
	mullw	%r5,%r5,%r6
	lwz	%r3,-8(%r3)
	addic	%r5,%r5,1
	rlwinm	%r5,%r5,2,0,29
	addc	%r4,%r4,%r5
	stw	%r3,0(%r4)
	b	..L.4
..L.117:
	addi	%r3,%r0,2
	stw	%r3,8(%r29)
	b	..LCRET._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj
..L.118:
	lbz	%r4,2(%r24)
	addic	%r25,%r24,2
	rlwinm	%r4,%r4,2,0,29
	addic	%r27,%r27,4
	addc	%r4,%r26,%r4
	stw	%r4,4(%r3)
	b	..L.4
..L.119:		// LOCAL_U8_LOAD
	lbzu	%r5,2(%r24)			// fetch next opcode, with update
	rlwinm	%r5,%r5,2,0,29		// shift left twice
	lwzx	%r5,%r27,%r5		// read from jump table
	mtctr	%r5					// fetch target (do as soon as possible)

	lbz		%r4,-1(%r24)
	rlwinm	%r4,%r4,2,0,29
	lwzx	%r4,%r26,%r4
	stwu	%r4,4(%r3)

	bcctr	20,0				// jump to counter.
..L.120:		// LOCAL_U8_STORE
	lbzu	%r5,2(%r24)			// fetch next opcode, with update
	rlwinm	%r5,%r5,2,0,29		// shift left twice
	lwzx	%r5,%r27,%r5		// read from jump table
	mtctr	%r5					// fetch target (do as soon as possible)

	lbz		%r4,-1(%r24)
	lwz		%r25,0(%r3)
	rlwinm	%r4,%r4,2,0,29
	subic	%r3,%r3,4
	stwx	%r25,%r26,%r4
	
	bcctr	20,0
..L.121:
	lbz	%r4,2(%r24)
	addic	%r25,%r24,2
	addic	%r27,%r27,4
	rlwinm	%r4,%r4,2,0,29
	addc	%r4,%r31,%r4
	stw	%r4,4(%r3)
	b	..L.4
..L.122:
	lbz	%r3,2(%r24)
	addic	%r27,%r27,4
	rlwinm	%r3,%r3,2,0,29
	addic	%r25,%r24,2
	lwzx	%r3,%r31,%r3
	stw	%r3,0(%r27)
	b	..L.4
..L.123:
	lbz	%r4,2(%r24)
	addic	%r25,%r24,2
	lwz	%r3,0(%r3)
	subic	%r27,%r27,4
	rlwinm	%r4,%r4,2,0,29
	stwx	%r3,%r31,%r4
	b	..L.4
..L.124:	// IADD_U8
	lbzu	%r5,2(%r24)			// fetch next opcode, with update
	rlwinm	%r5,%r5,2,0,29		// shift left twice
	lwzx	%r5,%r27,%r5		// read from jump table
	mtctr	%r5					// fetch target (do as soon as possible)

	lwz	%r25,0(%r3)
	lbz	%r4,-1(%r24)
	addc	%r25,%r25,%r4
	stw	%r25,0(%r3)
	
	bcctr	20,0
..L.125:
	lwz	%r3,0(%r27)
	lbz	%r4,2(%r24)
	addic	%r25,%r24,2
	lwzx	%r3,%r3,%r4
	stw	%r3,0(%r27)
	b	..L.4
..L.126:
	lwz	%r4,0(%r3)
	lbz	%r5,2(%r24)
	addic	%r25,%r24,2
	lwz	%r3,-4(%r3)
	subic	%r27,%r27,8
	stwx	%r3,%r4,%r5
	b	..L.4
..L.127:
	lwz	%r3,0(%r27)
	addic	%r25,%r24,2
	lbz	%r4,2(%r24)
	mullw	%r3,%r3,%r4
	stw	%r3,0(%r27)
	b	..L.4
..L.128:
	lhz	%r4,2(%r24)
	addic	%r25,%r24,3
	addic	%r27,%r27,4
	extsh	%r4,%r4
	stw	%r4,4(%r3)
	b	..L.4
..L.129:
	lhz	%r3,2(%r24)
	addic	%r25,%r24,3
	lwz	%r4,0(%r27)
	extsh	%r3,%r3
	addc	%r3,%r4,%r3
	stw	%r3,0(%r27)
	b	..L.4
..L.130:
	lhz	%r3,2(%r24)
	lwz	%r4,0(%r27)
	addic	%r25,%r24,3
	extsh	%r3,%r3
	lwzx	%r3,%r4,%r3
	stw	%r3,0(%r27)
	b	..L.4
..L.131:
	lhz	%r4,2(%r24)
	addic	%r25,%r24,3
	lwz	%r5,0(%r3)
	subic	%r27,%r27,8
	extsh	%r4,%r4
	lwz	%r3,-4(%r3)
	stwx	%r3,%r5,%r4
	b	..L.4
..L.132:
	lhz	%r3,2(%r24)
	addic	%r25,%r24,3
	lwz	%r4,0(%r27)
	extsh	%r3,%r3
	mullw	%r3,%r4,%r3
	stw	%r3,0(%r27)
	b	..L.4
..L.133:
	lwz	%r4,0(%r3)
	lwz	%r5,-4(%r3)
	subic	%r27,%r27,4
	lwz	%r6,0(%r4)
	cmplw	0,%r5,%r6
	bge	0, ..L.135 # bc 4,0
	lhz	%r6,2(%r24)
	mullw	%r5,%r5,%r6
	addic	%r25,%r24,3
	addic	%r5,%r5,1
	rlwinm	%r5,%r5,2,0,29
	addc	%r4,%r4,%r5
	stw	%r4,-4(%r3)
	b	..L.4
..L.135:
	addi	%r3,%r0,2
	stw	%r3,8(%r29)
	b	..LCRET._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj
..L.136:
	lwz	%r4,0(%r3)
	lwz	%r3,-4(%r3)
	subic	%r27,%r27,4
	lwz	%r5,0(%r4)
	cmplw	0,%r3,%r5
	bge	0, ..L.138 # bc 4,0
	lhz	%r5,2(%r24)
	mullw	%r3,%r3,%r5
	addic	%r25,%r24,3
	addic	%r3,%r3,1
	rlwinm	%r3,%r3,2,0,29
	lwzx	%r3,%r4,%r3
	stw	%r3,0(%r27)
	b	..L.4
..L.138:
	addi	%r3,%r0,2
	stw	%r3,8(%r29)
	b	..LCRET._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj
..L.139:
	lwz	%r5,0(%r3)
	lwz	%r4,-4(%r3)
	subic	%r27,%r27,12
	lwz	%r6,0(%r5)
	cmplw	0,%r4,%r6
	bge	0, ..L.141 # bc 4,0
	lhz	%r6,2(%r24)
	mullw	%r4,%r4,%r6
	lwz	%r3,-8(%r3)
	addic	%r4,%r4,1
	addic	%r25,%r24,3
	rlwinm	%r4,%r4,2,0,29
	addc	%r4,%r5,%r4
	stw	%r3,0(%r4)
	b	..L.4
..L.141:
	addi	%r3,%r0,2
	stw	%r3,8(%r29)
	b	..LCRET._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj
..L.142:
	lhz	%r4,2(%r24)
	addic	%r25,%r24,3
	addic	%r27,%r27,4
	rlwinm	%r4,%r4,2,0,29
	addc	%r4,%r26,%r4
	stw	%r4,4(%r3)
	b	..L.4
..L.143:
	lhz	%r3,2(%r24)
	addic	%r27,%r27,4
	rlwinm	%r3,%r3,2,0,29
	addic	%r25,%r24,3
	lwzx	%r3,%r26,%r3
	stw	%r3,0(%r27)
	b	..L.4
..L.144:
	lhz	%r4,2(%r24)
	addic	%r25,%r24,3
	lwz	%r3,0(%r3)
	subic	%r27,%r27,4
	rlwinm	%r4,%r4,2,0,29
	stwx	%r3,%r26,%r4
	b	..L.4
..L.145:
	lhz	%r4,2(%r24)
	addic	%r25,%r24,3
	rlwinm	%r4,%r4,2,0,29
	addic	%r27,%r27,4
	addc	%r4,%r31,%r4
	stw	%r4,4(%r3)
	b	..L.4
..L.146:
	lhz	%r3,2(%r24)
	addic	%r27,%r27,4
	addic	%r25,%r24,3
	rlwinm	%r3,%r3,2,0,29
	lwzx	%r3,%r31,%r3
	stw	%r3,0(%r27)
	b	..L.4
..L.147:
	lhz	%r4,2(%r24)
	lwz	%r3,0(%r3)
	addic	%r25,%r24,3
	rlwinm	%r4,%r4,2,0,29
	subic	%r27,%r27,4
	stwx	%r3,%r31,%r4
	b	..L.4
..L.148:
	lhz	%r4,2(%r24)
	addic	%r25,%r24,3
	addic	%r27,%r27,4
	rlwinm	%r4,%r4,2,0,29
	addc	%r4,%r30,%r4
	stw	%r4,4(%r3)
	b	..L.4
..L.149:
	lhz	%r3,2(%r24)
	addic	%r27,%r27,4
	rlwinm	%r3,%r3,2,0,29
	addic	%r25,%r24,3
	lwzx	%r3,%r30,%r3
	stw	%r3,0(%r27)
	b	..L.4
..L.150:
	lhz	%r4,2(%r24)
	addic	%r25,%r24,3
	lwz	%r3,0(%r3)
	subic	%r27,%r27,4
	rlwinm	%r4,%r4,2,0,29
	stwx	%r3,%r30,%r4
	b	..L.4
..L.151:
	addi	%r4,%r0,1
	addic	%r27,%r27,4
	addic	%r25,%r24,4
	lwzx	%r4,%r24,%r4
	rlwinm	%r4,%r4,2,6,29
	addc	%r4,%r30,%r4
	stw	%r4,4(%r3)
	b	..L.4
..L.152:
	addi	%r3,%r0,1
	addic	%r27,%r27,4
	addic	%r25,%r24,4
	lwzx	%r3,%r24,%r3
	rlwinm	%r3,%r3,2,6,29
	lwzx	%r3,%r30,%r3
	stw	%r3,0(%r27)
	b	..L.4
..L.153:
	addi	%r4,%r0,1
	subic	%r27,%r27,4
	addic	%r25,%r24,4
	lwzx	%r4,%r24,%r4
	lwz	%r3,0(%r3)
	rlwinm	%r4,%r4,2,6,29
	stwx	%r3,%r30,%r4
	b	..L.4
..L.154:
	addi	%r4,%r0,1
	subfc	%r5,%r28,%r24
	addic	%r27,%r27,4
	addic	%r5,%r5,4
	lwzx	%r4,%r24,%r4
	rlwinm	%r25,%r4,0,8,31
	stw	%r5,4(%r3)
	addc	%r25,%r28,%r25
	b	..L.4
..L.155:		// J
	lwz		%r25,0(%r24)
	rlwinm	%r25,%r25,0,8,31
	addc	%r24,%r28,%r25
	lbzu	%r5,1(%r24)			// fetch next opcode, with update
	rlwinm	%r5,%r5,2,0,29		// shift left twice
	lwzx	%r5,%r27,%r5		// read from jump table
	mtctr	%r5					// fetch target (do as soon as possible)
	bcctr	20,0				// jump to counter.
..L.156:
	addi	%r4,%r0,1
	addic	%r25,%r24,4
	subic	%r27,%r27,4
	lwzx	%r24,%r24,%r4
	lwz	%r3,0(%r3)
	rlwinm	%r24,%r24,0,8,31
	cmpwi	0,%r3,0
	bne	0, ..L.4 # bc 4,2
	addc	%r25,%r28,%r24
	b	..L.4
..L.158:
	addi	%r4,%r0,1
	addic	%r25,%r24,4
	subic	%r27,%r27,8
	lwzx	%r24,%r24,%r4
	lwz	%r4,-4(%r3)
	lwz	%r3,0(%r3)
	rlwinm	%r24,%r24,0,8,31
	cmpw	0,%r4,%r3
	beq	0, ..L.4 # bc 12,2
	addc	%r25,%r28,%r24
	b	..L.4
..L.160:
	addi	%r4,%r0,1
	addic	%r25,%r24,4
	subic	%r27,%r27,8
	lwzx	%r24,%r24,%r4
	lwz	%r4,-4(%r3)
	lwz	%r3,0(%r3)
	rlwinm	%r24,%r24,0,8,31
	cmpw	0,%r4,%r3
	bne	0, ..L.4 # bc 4,2
	addc	%r25,%r28,%r24
	b	..L.4
..L.162:
	addi	%r4,%r0,1
	addic	%r25,%r24,4
	subic	%r27,%r27,8
	lwzx	%r24,%r24,%r4
	lwz	%r4,-4(%r3)
	lwz	%r3,0(%r3)
	rlwinm	%r24,%r24,0,8,31
	cmpw	0,%r4,%r3
	bge	0, ..L.4 # bc 4,0
	addc	%r25,%r28,%r24
	b	..L.4
..L.164:
	addi	%r4,%r0,1
	addic	%r25,%r24,4
	subic	%r27,%r27,8
	lwzx	%r24,%r24,%r4
	lwz	%r4,-4(%r3)
	lwz	%r3,0(%r3)
	rlwinm	%r24,%r24,0,8,31
	cmpw	0,%r4,%r3
	bgt	0, ..L.4 # bc 12,1
	addc	%r25,%r28,%r24
	b	..L.4
..L.166:		// JZ_ILE
	lbzu	%r5,4(%r24)			// fetch next opcode, with update
	rlwinm	%r5,%r5,2,0,29		// shift left twice
	lwzx	%r5,%r27,%r5		// read from jump table
	mtctr	%r5					// fetch target (do as soon as possible)
	lwz		%r5,-4(%r24)		// get target addr
	subic	%r3,%r3,8
	lwz		%r4,4(%r3)
	lwz		%r6,8(%r3)
	rlwinm	%r5,%r5,0,8,31
	cmpw	0,%r4,%r6
	blectr						// bcctr	4,1					// branch to ctr if le
	addc	%r24,%r28,%r5
	lbzu	%r5,1(%r24)			// fetch next opcode, with update
	rlwinm	%r5,%r5,2,0,29		// shift left twice
	lwzx	%r5,%r27,%r5		// read from jump table
	mtctr	%r5					// fetch target (do as soon as possible)
	bcctr	20,0				// jump to counter.
..L.168:
	addi	%r4,%r0,1
	addic	%r25,%r24,4
	subic	%r27,%r27,8
	lwzx	%r24,%r24,%r4
	lwz	%r4,-4(%r3)
	lwz	%r3,0(%r3)
	rlwinm	%r24,%r24,0,8,31
	cmpw	0,%r4,%r3
	blt	0, ..L.4 # bc 12,0
	addc	%r25,%r28,%r24
	b	..L.4
..L.170:
	lwz	%r4,0(%r3)
	lbz	%r3,2(%r24)
	addic	%r5,%r24,3
	subic	%r27,%r27,4
	sldi	%r6,%r3,3
	cmpwi	0,%r3,0
	subfc	%r6,%r3,%r6
	addc	%r25,%r24,%r6
	addi	%r24,%r0,0
	addic	%r25,%r25,2
	beq	0, ..L.173 # bc 12,2
..L.171:
	lwz	%r6,0(%r5)
	lwz	%r7,4(%r5)
	cmpw	0,%r4,%r6
	rlwinm	%r6,%r7,24,8,31
	beq	0, ..L.174 # bc 12,2
	addic	%r24,%r24,1
	addic	%r5,%r5,7
	cmplw	0,%r3,%r24
	bgt	0, ..L.171 # bc 12,1
..L.173:
	b	..L.4
..L.174:
	addc	%r25,%r28,%r6
	b	..L.173
..L.175:
	addic	%r4,%r24,3
	lbz	%r5,2(%r24)
	addic	%r27,%r27,4
	addc	%r25,%r24,%r5
	stw	%r4,4(%r3)
	addic	%r25,%r25,2
	b	..L.4
..L.176:
	addic	%r4,%r24,2
	addi	%r5,%r0,2
	addic	%r27,%r27,4
	stw	%r4,4(%r3)
	lwzx	%r3,%r24,%r5
	addc	%r25,%r24,%r3
	addic	%r25,%r25,5
	b	..L.4
..L.177:
	addis	%r4,%r0,s_Null@ha
	addic	%r27,%r27,4
	addic	%r4,%r4,s_Null@l
	stw	%r4,4(%r3)
	b	..L.4
..L.178:
	lwz	%r5,-4(%r3)
	addic	%r25,%r24,2
	lwz	%r3,0(%r3)
	subic	%r27,%r27,8
	lbz	%r6,2(%r24)
	ori	%r4,%r6,0
	bl	._ZN4rage17scr_assign_stringEPcjPKc
	b	..L.4
..L.179:
	ori	%r4,%r3,0
	addic	%r3,%sp,208
	subic	%r27,%r27,8
	lwz	%r23,0(%r4)
	lwz	%r5,-4(%r4)
	ori	%r4,%r5,0
	bl	._ZN4rage8scr_itoaEPci
	lbz	%r4,2(%r24)
	addic	%r25,%r24,2
	addic	%r5,%sp,208
	ori	%r3,%r23,0
	bl	._ZN4rage17scr_assign_stringEPcjPKc
	b	..L.4
..L.180:
	lwz	%r5,-4(%r3)
	lwz	%r3,0(%r3)
	addic	%r25,%r24,2
	lbz	%r6,2(%r24)
	subic	%r27,%r27,8
	ori	%r4,%r6,0
	bl	._ZN4rage17scr_append_stringEPcjPKc
	b	..L.4
..L.181:
	ori	%r4,%r3,0
	addic	%r3,%sp,208
	subic	%r27,%r27,8
	lwz	%r23,0(%r4)
	lwz	%r5,-4(%r4)
	ori	%r4,%r5,0
	bl	._ZN4rage8scr_itoaEPci
	addic	%r25,%r24,2
	lbz	%r4,2(%r24)
	addic	%r5,%sp,208
	ori	%r3,%r23,0
	bl	._ZN4rage17scr_append_stringEPcjPKc
	b	..L.4
..L.182:
	lwz	%r4,-8(%r27)
	subic	%r6,%r27,12
	lwz	%r5,-4(%r27)
	lwz	%r3,0(%r27)
	cmpw	0,%r5,%r4
	ori	%r27,%r6,0
	bge	0, ..L.185 # bc 4,0
	subfc	%r5,%r5,%r4
	mtctr	%r5
..L.184:
	subic	%r27,%r27,4
	subic	%r4,%r4,1
	bc	16,0,..L.184
..L.185:
	addi	%r5,%r0,0
	cmpw	0,%r5,%r4
	bge	0, ..L.188 # bc 4,0
	sldi	%r5,%r4,2
	subic	%r6,%r3,4
	mtctr	%r4
..L.187:
	lwz	%r7,0(%r27)
	subic	%r27,%r27,4
	stwx	%r7,%r6,%r5
	subic	%r5,%r5,4
	bc	16,0,..L.187
..L.188:
	rlwinm	%r4,%r4,2,0,29
	addi	%r5,%r0,0
	subic	%r4,%r4,1
	stbx	%r5,%r3,%r4
	b	..L.4
..L.189:
	subfc	%r4,%r31,%r26
	subfc	%r5,%r31,%r27
	srawi	%r4,%r4,2
	addi	%r7,%r0,-1
	addze	%r4,%r4
	subfc	%r6,%r28,%r25
	srawi	%r5,%r5,2
	addze	%r5,%r5
	stw	%r6,64(%r29)
	addic	%r27,%r27,4
	stw	%r4,68(%r29)
	addic	%r4,%r5,1
	stw	%r4,72(%r29)
	stw	%r7,4(%r3)
	b	..L.4
..L.190:
	lwz	%r3,64(%r29)
	lwz	%r27,0(%r27)
	cmpwi	0,%r3,0
	beq	0, ..L.192 # bc 12,2
	addc	%r25,%r28,%r3
	lwz	%r3,72(%r29)
	ori	%r4,%r27,0
	lwz	%r5,68(%r29)
	rlwinm	%r27,%r3,2,0,29
	rlwinm	%r26,%r5,2,0,29
	addc	%r27,%r31,%r27
	addc	%r26,%r31,%r26
	stw	%r4,0(%r27)
	b	..L.4
..L.192:
	addi	%r3,%r0,2
	stw	%r3,8(%r29)
..LCRET._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj:
	ld	%r0,336(%sp)
	mtlr	%r0
	ld	%r23,248(%sp)
	ld	%r24,256(%sp)
	ld	%r25,264(%sp)
	ld	%r26,272(%sp)
	ld	%r27,280(%sp)
	ld	%r28,288(%sp)
	ld	%r29,296(%sp)
	ld	%r30,304(%sp)
	ld	%r31,312(%sp)
	addi	%sp,%sp,320
	bclr	20,0
..L.193:
	subfc	%r3,%r28,%r25
	lwz	%r25,0(%r27)
	addc	%r25,%r28,%r25
	stw	%r3,0(%r27)
	b	..L.4
	.align	2
	.size	._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj,.-._ZN4rage9scrThread3RunEPNS_8scrValueES2_PKhPj
	.rodata
	.align	4
	.align	0
	.align	4
.CSW_1:
	.long	..L.7		// 0
	.long	..L.8		// 1
	.long	..L.9		// 2
	.long	..L.10		// 3
	.long	..L.11		// 4
	.long	..L.13		// 5
	.long	..L.15		// 6
	.long	..L.16		// 7
	.long	..L.17		// 8
	.long	..L.20		// 9
	.long	..L.26		// 10
	.long	..L.23		// 11
	.long	..L.32		// 12
	.long	..L.29		// 13
	.long	..L.35		// 14
	.long	..L.36		// 15
	.long	..L.37		// 16
	.long	..L.38		// 17
	.long	..L.40		// 18
	.long	..L.44		// 19
	.long	..L.45		// 20
	.long	..L.48		// 21
	.long	..L.54		// 22
	.long	..L.51		// 23
	.long	..L.60		// 24
	.long	..L.57		// 25
	.long	..L.63		// 26
	.long	..L.64		// 27
	.long	..L.65		// 28
	.long	..L.66		// 29
	.long	..L.72		// 30
	.long	..L.73		// 31
	.long	..L.74		// 32
	.long	..L.75		// 33
	.long	..L.76		// 34
	.long	..L.77		// 35
	.long	..L.78		// 36
	.long	..L.79		// 37 - PUSH_CONST_U8
	.long	..L.80		// 38
	.long	..L.81		// 39
	.long	..L.82		// 40 - PUSH_CONST_U32
	.long	..L.82		// 41
	.long	..L.83		// 42
	.long	..L.84		// 43
	.long	..L.85		// 44
	.long	..L.91		// 45 - ENTER
	.long	..L.95		// 46 - LEAVE
	.long	..L.100		// 47
	.long	..L.101		// 48
	.long	..L.102		// 49
	.long	..L.103		// 50
	.long	..L.106		// 51
	.long	..L.109		// 52
	.long	..L.112		// 53
	.long	..L.115		// 54
	.long	..L.118		// 55
	.long	..L.119		// 56 - LOCAL_U8_LOAD
	.long	..L.120		// 57 - LOCAL_U8_STORE
	.long	..L.121		// 58
	.long	..L.122		// 59
	.long	..L.123		// 60
	.long	..L.124		// 61 - IADD_U8
	.long	..L.125		// 62
	.long	..L.126		// 63
	.long	..L.127		// 64
	.long	..L.128		// 65
	.long	..L.129		// 66
	.long	..L.130		// 67
	.long	..L.131		// 68
	.long	..L.132		// 69
	.long	..L.133		// 70
	.long	..L.136		// 71
	.long	..L.139		// 72
	.long	..L.142		// 73
	.long	..L.143		// 74
	.long	..L.144		// 75
	.long	..L.145		// 76
	.long	..L.146		// 77
	.long	..L.147		// 78
	.long	..L.148		// 79
	.long	..L.149		// 80
	.long	..L.150		// 81
	.long	..L.151		// 82
	.long	..L.152		// 83
	.long	..L.153		// 84
	.long	..L.154		// 85
	.long	..L.155		// 86 - J
	.long	..L.156		// 87
	.long	..L.158		// 88
	.long	..L.160		// 89
	.long	..L.164		// 90
	.long	..L.162		// 91
	.long	..L.168		// 92
	.long	..L.166		// 93 - JZ_ILE
	.long	..L.170		// 94
	.long	..L.175		// 95
	.long	..L.176		// 96
	.long	..L.177		// 97
	.long	..L.178		// 98
	.long	..L.179		// 99
	.long	..L.180		// 100
	.long	..L.181		// 101
	.long	..L.182		// 102
	.long	..L.189		// 103
	.long	..L.190		// 104
	.long	..L.193		// 105
	.long	..L.6		// invalid insns follow
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.long	..L.6
	.text
	.section	".toc","aw"
	.text
	.align	2

	.text
	.align	2
	.type	._ZN4rage17scr_assign_stringEPcjPKc,@function
._ZN4rage17scr_assign_stringEPcjPKc:
	cmpwi	0,%r5,0
	beq	0, ..L.198 # bc 12,2
..L.195:
	lbz	%r6,0(%r5)
	cmpwi	0,%r6,0
	beq	0, ..L.198 # bc 12,2
	subic	%r4,%r4,1
	cmpwi	0,%r4,0
	beq	0, ..L.198 # bc 12,2
	stb	%r6,0(%r3)
	addic	%r5,%r5,1
	addic	%r3,%r3,1
	b	..L.195
..L.198:
	addi	%r4,%r0,0
	stb	%r4,0(%r3)
	bclr	20,0
	.align	2
	.size	._ZN4rage17scr_assign_stringEPcjPKc,.-._ZN4rage17scr_assign_stringEPcjPKc
	.section	".toc","aw"
	.text
	.align	2

	.text
	.align	2
	.type	._ZN4rage17scr_append_stringEPcjPKc,@function
._ZN4rage17scr_append_stringEPcjPKc:
	lbz	%r6,0(%r3)
	cmpwi	1,%r5,0
	cmpwi	0,%r6,0
	beq	0, ..L.201 # bc 12,2
..L.200:
	lbz	%r6,1(%r3)
	subic	%r4,%r4,1
	addic	%r3,%r3,1
	cmpwi	0,%r6,0
	bne	0, ..L.200 # bc 4,2
..L.201:
	ori	%r6,%r5,0
	ori	%r5,%r3,0
	ori	%r3,%r6,0
	beq	1, ..L.205 # bc 12,6
..L.202:
	lbz	%r6,0(%r3)
	cmpwi	0,%r6,0
	beq	0, ..L.205 # bc 12,2
	subic	%r4,%r4,1
	cmpwi	0,%r4,0
	beq	0, ..L.205 # bc 12,2
	stb	%r6,0(%r5)
	addic	%r3,%r3,1
	addic	%r5,%r5,1
	b	..L.202
..L.205:
	addi	%r3,%r0,0
	stb	%r3,0(%r5)
	bclr	20,0
	.align	2
	.size	._ZN4rage17scr_append_stringEPcjPKc,.-._ZN4rage17scr_append_stringEPcjPKc
	.section	".toc","aw"
	.text
	.align	2

	.text
	.align	2
	.type	._ZN4rage8scr_itoaEPci,@function
._ZN4rage8scr_itoaEPci:
	stdu	%sp,-64(%sp)
	cmpwi	1,%r4,0
	addic	%r5,%sp,48
	bge	1, ..L.213 # bc 4,4
	addi	%r6,%r0,45
	neg	%r4,%r4
	stb	%r6,0(%r3)
	addic	%r3,%r3,1
..L.208:
	cmpwi	0,%r4,0
	bne	0, ..L.215 # bc 4,2
..L.209:
	addic	%r4,%sp,48
	cmpw	0,%r5,%r4
	beq	0, ..L.212 # bc 12,2
	subfc	%r4,%r4,%r5
	mtctr	%r4
..L.211:
	lbz	%r6,-1(%r5)
	subic	%r5,%r5,1
	stb	%r6,0(%r3)
	addic	%r3,%r3,1
	bc	16,0,..L.211
..L.212:
	addi	%r4,%r0,0
	stb	%r4,0(%r3)
	b	..LCRET._ZN4rage8scr_itoaEPci
..L.213:
	bne	1, ..L.208 # bc 4,6
	addi	%r4,%r0,48
	addi	%r5,%r0,0
	stb	%r4,0(%r3)
	stb	%r5,1(%r3)
..LCRET._ZN4rage8scr_itoaEPci:
	addi	%sp,%sp,64
	bclr	20,0
..L.215:
	addis	%r6,%r0,26214
	ori	%r6,%r6,26215
	mulhw	%r6,%r6,%r4
	srawi	%r6,%r6,2
	srwi	%r7,%r6,31
	addc	%r6,%r6,%r7
	sldi	%r7,%r6,3
	cmpwi	0,%r6,0
	addc	%r7,%r6,%r7
	addc	%r7,%r6,%r7
	subfc	%r4,%r7,%r4
	addic	%r7,%r4,48
	ori	%r4,%r6,0
	stb	%r7,0(%r5)
	addic	%r5,%r5,1
	bne	0, ..L.215 # bc 4,2
	b	..L.209
	.align	2
	.size	._ZN4rage8scr_itoaEPci,.-._ZN4rage8scr_itoaEPci
	.section	".toc","aw"
	.text
	.align	2

	.section	".gnu.linkonce.t._ZN4rage9scrThread4Info27CopyReferencedParametersOutEv","axG",@progbits,_ZN4rage9scrThread4Info27CopyReferencedParametersOutEv,comdat
	.globl	._ZN4rage9scrThread4Info27CopyReferencedParametersOutEv
	.type	._ZN4rage9scrThread4Info27CopyReferencedParametersOutEv,@function
._ZN4rage9scrThread4Info27CopyReferencedParametersOutEv:
	.weak	._ZN4rage9scrThread4Info27CopyReferencedParametersOutEv
	.weak	_ZN4rage9scrThread4Info27CopyReferencedParametersOutEv
	lwz	%r5,12(%r3)
	cmpwi	0,%r5,0
	subic	%r4,%r5,1
	beq	0, ..L.219 # bc 12,2
	ori	%r6,%r3,0
	sldi	%r3,%r4,2
	sldi	%r4,%r4,4
	mtctr	%r5
	addc	%r3,%r6,%r3
	addc	%r4,%r6,%r4
..L.218:
	lwz	%r5,16(%r3)
	lwz	%r6,32(%r4)
	subic	%r3,%r3,4
	stw	%r6,0(%r5)
	lwz	%r6,36(%r4)
	stw	%r6,4(%r5)
	lwz	%r6,40(%r4)
	subic	%r4,%r4,16
	stw	%r6,8(%r5)
	bc	16,0,..L.218
..L.219:
	bclr	20,0
	.align	2
	.size	._ZN4rage9scrThread4Info27CopyReferencedParametersOutEv,.-._ZN4rage9scrThread4Info27CopyReferencedParametersOutEv
	.data
	.lcomm	s_Null,4,4
