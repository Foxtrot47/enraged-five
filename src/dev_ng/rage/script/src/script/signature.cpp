// 
// script/signature.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "signature.h"

#include "string/string.h"

namespace rage {

#if __DEV
	const char* g_scrValueTypeNames[] = {
		"UNKNOWN",
		"BOOL",
		"INT",
		"FLOAT",
		"VECTOR",
		"TEXT_LABEL",
		"STRING",
		"OBJECT",
		NULL
	};

	void scrSignature::PrettyPrint( char* outBuf, int outBufSize, const char* fnName )
	{
		if (GetReturnType() == scrValue::UNKNOWN)
		{
			formatf(outBuf, outBufSize, "PROC %s(", fnName);
		}
		else
		{
			formatf(outBuf, outBufSize, "FUNC %s %s(", g_scrValueTypeNames[GetReturnType()], fnName);
		}

		int args = CountNumArgs();
		for(int i = 0; i < args-1; i++)
		{
			safecatf(outBuf, outBufSize, "%s, ", g_scrValueTypeNames[GetArgumentType(i)]);
		}
		if (args > 0)
		{
			safecatf(outBuf, outBufSize, "%s)", g_scrValueTypeNames[GetArgumentType(args-1)]);
		}
		else
		{
			safecatf(outBuf, outBufSize, ")");
		}
	}
#endif

} // namespace rage
