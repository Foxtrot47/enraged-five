// 
// scriptgui/bankscript.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "bankscript.h"
#include "wrapper.h"

#if __BANK

#include "bank/bkangle.h"
#include "bank/bkmatrix.h"
#include "bank/bkmgr.h"
#include "bank/bkvector.h"
#include "bank/button.h"
#include "bank/color.h"
#include "bank/combo.h"
#include "bank/imageviewer.h"
#include "bank/slider.h"
#include "bank/text.h"
#include "bank/toggle.h"
#include "bank/widget.h"

#include <stdio.h>

#endif // __BANK

using namespace rage;

namespace rageBankScriptBindings {

//#############################################################################
// Helper Functions
//#############################################################################

#if __BANK

static bkAngle* FindAngleWidget( const char *path )
{
    bkWidget *pWidget = BANKMGR.FindWidget( path );
    if ( pWidget == NULL )
    {
        return NULL;
    }

    return dynamic_cast<bkAngle *>( pWidget );    
}

static bkVector2* FindVector2Widget( const char *path )
{
    bkWidget *pWidget = BANKMGR.FindWidget( path );
    if ( pWidget == NULL )
    {
        return NULL;
    }

    return dynamic_cast<bkVector2 *>( pWidget );    
}

static bkVector3* FindVector3Widget( const char *path )
{
    bkWidget *pWidget = BANKMGR.FindWidget( path );
    if ( pWidget == NULL )
    {
        return NULL;
    }

    return dynamic_cast<bkVector3 *>( pWidget );
}

static bkVector4* FindVector4Widget( const char *path )
{
    bkWidget *pWidget = BANKMGR.FindWidget( path );
    if ( pWidget == NULL )
    {
        return NULL;
    }

    return dynamic_cast<bkVector4 *>( pWidget );    
}

static bkButton* FindButtonWidget( const char *path )
{
    bkWidget *pWidget = BANKMGR.FindWidget( path );
    if ( pWidget == NULL )
    {
        return NULL;
    }

    return dynamic_cast<bkButton *>( pWidget );    
}

static bkColor* FindColorWidget( const char *path )
{
    bkWidget *pWidget = BANKMGR.FindWidget( path );
    if ( pWidget == NULL )
    {
        return NULL;
    }

    return dynamic_cast<bkColor *>( pWidget );    
}

static bkCombo* FindComboWidget( const char *path )
{
    bkWidget *pWidget = BANKMGR.FindWidget( path );
    if ( pWidget == NULL )
    {
        return NULL;
    }

    return dynamic_cast<bkCombo *>( pWidget );    
}

static bkImageViewer* FindImageViewerWidget( const char *path )
{
    bkWidget *pWidget = BANKMGR.FindWidget( path );
    if ( pWidget == NULL )
    {
        return NULL;
    }

    return dynamic_cast<bkImageViewer *>( pWidget );    
}

static bkSlider* FindSliderWidget( const char *path )
{
    bkWidget *pWidget = BANKMGR.FindWidget( path );
    if ( pWidget == NULL )
    {
        return NULL;
    }

    return dynamic_cast<bkSlider *>( pWidget );    
}

static bkToggle* FindToggleWidget( const char *path )
{
    bkWidget *pWidget = BANKMGR.FindWidget( path );
    if ( pWidget == NULL )
    {
        return NULL;
    }

    return dynamic_cast<bkToggle *>( pWidget );    
}

#endif // __BANK

//#############################################################################
// General Widget Functions
//#############################################################################

#if __BANK
static char s_tempWidgetStringValue[512];
#endif

// PURPOSE: Indicates if a widget exists at the given path
// PARAMS:
//    path - the path of the widget
// RETURNS: true or false
static bool WidgetExists( const char* BANK_ONLY( path ) )
{
#if __BANK
    return BANKMGR.FindWidget( path ) != NULL;
#else // __BANK
    return false;
#endif // __BANK
}

// PURPOSE: Retrieves the string value of the widget
// PARAMS:
//    path - the path of the widget
// RETURNS: NULL if the widget is not found
static const char* GetWidgetValueToString( const char* BANK_ONLY( path ) )
{
#if __BANK
    bkWidget *pWidget = BANKMGR.FindWidget( path );
    if ( pWidget == NULL )
    {
        return NULL;
    }

    pWidget->GetStringRepr( s_tempWidgetStringValue, sizeof(s_tempWidgetStringValue) );
    return s_tempWidgetStringValue;
#else // __BANK
    return NULL;
#endif // __BANK
}

// PURPOSE: Sets the value of the widget.
// PARAMS:
//    path - the path of the widget
//    value - the string value
// RETURNS: false if the widget is not found
static bool SetWidgetValueFromString( const char* BANK_ONLY( path ), const char* BANK_ONLY( value ) )
{
#if __BANK
    bkWidget *pWidget = BANKMGR.FindWidget( path );
    if ( pWidget == NULL )
    {
        return false;
    }

    pWidget->SetStringRepr( value );
    return true;
#else // __BANK
    return false;
#endif // __BANK
}

//#############################################################################
// bkAngle
//#############################################################################

static scrVector s_tempWidgetAngleVectorValue;

static float GetWidgetAngleValueDegrees( const char* BANK_ONLY( path ) )
{
#if __BANK
    bkAngle *pAngleWidget = FindAngleWidget( path );
    if ( pAngleWidget == NULL )
    {
        return 0.0f;
    }

    return pAngleWidget->GetDegrees();
#else // __BANK
    return 0.0f;
#endif // __BANK
}

static bool SetWidgetAngleValueDegrees( const char* BANK_ONLY( path ), float BANK_ONLY( degrees ) )
{
#if __BANK
    bkAngle *pAngleWidget = FindAngleWidget( path );
    if ( pAngleWidget == NULL )
    {
        return false;
    }

    pAngleWidget->SetDegrees( degrees );
    return true;
#else // __BANK
    return false;
#endif
}

static float GetWidgetAngleValueFraction( const char* BANK_ONLY( path ) )
{
#if __BANK
    bkAngle *pAngleWidget = FindAngleWidget( path );
    if ( pAngleWidget == NULL )
    {
        return 0.0f;
    }

    return pAngleWidget->GetFraction();
#else // __BANK
    return 0.0f;
#endif
}

static bool SetWidgetAngleValueFraction( const char* BANK_ONLY( path ), float BANK_ONLY( fraction ) )
{
#if __BANK
    bkAngle *pAngleWidget = FindAngleWidget( path );
    if ( pAngleWidget == NULL )
    {
        return false;
    }

    pAngleWidget->SetFraction( fraction );
    return true;
#else // __BANK
    return false;
#endif
}

static float GetWidgetAngleValueRadians( const char* BANK_ONLY( path ) )
{
#if __BANK
    bkAngle *pAngleWidget = FindAngleWidget( path );
    if ( pAngleWidget == NULL )
    {
        return 0.0f;
    }

    return pAngleWidget->GetRadians();
#else // __BANK
    return 0.0f;
#endif
}

static bool SetWidgetAngleValueRadians( const char* BANK_ONLY( path ), float BANK_ONLY( radians ) )
{
#if __BANK
    bkAngle *pAngleWidget = FindAngleWidget( path );
    if ( pAngleWidget == NULL )
    {
        return false;
    }

    pAngleWidget->SetRadians( radians );
    return true;
#else // __BANK
    return false;
#endif
}

static const scrVector& GetWidgetAngleValueVector2( const char* BANK_ONLY( path ) )
{
#if __BANK
    bkAngle *pAngleWidget = FindAngleWidget( path );
    if ( pAngleWidget == NULL )
    {
        s_tempWidgetAngleVectorValue.x = 0.0f;
        s_tempWidgetAngleVectorValue.y = 0.0f;
        s_tempWidgetAngleVectorValue.z = 0.0f;
        return s_tempWidgetAngleVectorValue;
    }

    const Vector2 &v2 = pAngleWidget->GetVector();
    
    s_tempWidgetAngleVectorValue.x = v2.x;
    s_tempWidgetAngleVectorValue.y = v2.y;
    s_tempWidgetAngleVectorValue.z = 0.0f;
    return s_tempWidgetAngleVectorValue;
#else // __BANK
    s_tempWidgetAngleVectorValue.x = 0.0f;
    s_tempWidgetAngleVectorValue.y = 0.0f;
    s_tempWidgetAngleVectorValue.z = 0.0f;
    return s_tempWidgetAngleVectorValue;
#endif
}

static bool SetWidgetAngleValueVector2( const char* BANK_ONLY( path ), const scrVector & BANK_ONLY( v ) )
{
#if __BANK
    bkAngle *pAngleWidget = FindAngleWidget( path );
    if ( pAngleWidget == NULL )
    {
        return false;
    }

    Vector2 v2( v.x, v.y );
    pAngleWidget->SetVector( v2 );
    return true;
#else // __BANK
    return false;
#endif
}

//#############################################################################
// bkVector2
//#############################################################################

static scrVector s_tempWidgetVector2Value;

static const scrVector& GetWidgetVector2Value( const char* BANK_ONLY( path ) )
{
#if __BANK
    bkVector2 *pVector2Widget = FindVector2Widget( path );
    if ( pVector2Widget == NULL )
    {
        s_tempWidgetVector2Value.x = 0.0f;
        s_tempWidgetVector2Value.y = 0.0f;
        s_tempWidgetVector2Value.z = 0.0f;
        return s_tempWidgetVector2Value;
    }

    const Vector2 &v = pVector2Widget->GetVector();
    s_tempWidgetVector2Value.x = v.x;
    s_tempWidgetVector2Value.y = v.y;
    s_tempWidgetVector2Value.z = 0.0f;
    return s_tempWidgetVector2Value;
#else // __BANK
    s_tempWidgetVector2Value.x = 0.0f;
    s_tempWidgetVector2Value.y = 0.0f;
    s_tempWidgetVector2Value.z = 0.0f;
    return s_tempWidgetVector2Value;
#endif
}

static bool SetWidgetVector2Value( const char* BANK_ONLY( path ), const scrVector &BANK_ONLY( v ) )
{
#if __BANK
    bkVector2 *pVector2Widget = FindVector2Widget( path );
    if ( pVector2Widget == NULL )
    {
        return false;
    }

    Vector2 value( v.x, v.y );
    pVector2Widget->SetVector( value );
    return true;
#else // __BANK
    return false;
#endif
}

//#############################################################################
// bkVector3
//#############################################################################

static scrVector s_tempWidgetVector3Value;

static const scrVector& GetWidgetVector3Value( const char* BANK_ONLY( path ) )
{
#if __BANK
    bkVector3 *pVector3Widget = FindVector3Widget( path );
    if ( pVector3Widget == NULL )
    {
        s_tempWidgetVector3Value.x = 0.0f;
        s_tempWidgetVector3Value.y = 0.0f;
        s_tempWidgetVector3Value.z = 0.0f;
        return s_tempWidgetVector3Value;
    }

    const Vector3 &v = pVector3Widget->GetVector();
    s_tempWidgetVector3Value.x = v.x;
    s_tempWidgetVector3Value.y = v.y;
    s_tempWidgetVector3Value.z = v.z;
    return s_tempWidgetVector3Value;
#else // __BANK
    s_tempWidgetVector3Value.x = 0.0f;
    s_tempWidgetVector3Value.y = 0.0f;
    s_tempWidgetVector3Value.z = 0.0f;
    return s_tempWidgetVector3Value;
#endif
}

static bool SetWidgetVector3Value( const char* BANK_ONLY( path ), const scrVector &BANK_ONLY( v ) )
{
#if __BANK
    bkVector3 *pVector3Widget = FindVector3Widget( path );
    if ( pVector3Widget == NULL )
    {
        return false;
    }

    Vector3 value( v.x, v.y, v.z );
    pVector3Widget->SetVector( value );
    return true;
#else // __BANK
    return false;
#endif
}

//#############################################################################
// bkVector4
//#############################################################################

static scrVector s_tempWidgetVector4Value;

static const scrVector& GetWidgetVector4Value( const char* BANK_ONLY( path ), float& w )
{
#if __BANK
    bkVector4 *pVector4Widget = FindVector4Widget( path );
    if ( pVector4Widget == NULL )
    {
        s_tempWidgetVector4Value.x = 0.0f;
        s_tempWidgetVector4Value.y = 0.0f;
        s_tempWidgetVector4Value.z = 0.0f;
        w = 0.0f;
        return s_tempWidgetVector4Value;
    }

    const Vector4 &v = pVector4Widget->GetVector();
    s_tempWidgetVector4Value.x = v.x;
    s_tempWidgetVector4Value.y = v.y;
    s_tempWidgetVector4Value.z = v.z;
    w = v.w;
    return s_tempWidgetVector2Value;
#else // __BANK
    s_tempWidgetVector4Value.x = 0.0f;
    s_tempWidgetVector4Value.y = 0.0f;
    s_tempWidgetVector4Value.z = 0.0f;
    w = 0.0f;
    return s_tempWidgetVector4Value;
#endif
}

static bool SetWidgetVector4Value( const char* BANK_ONLY( path ), const scrVector &BANK_ONLY( v ), float BANK_ONLY( w ) )
{
#if __BANK
    bkVector4 *pVector4Widget = FindVector4Widget( path );
    if ( pVector4Widget == NULL )
    {
        return false;
    }

    Vector4 value( v.x, v.y, v.z, w );
    pVector4Widget->SetVector( value );
    return true;
#else // __BANK
    return false;
#endif
}

//#############################################################################
// bkButton
//#############################################################################

static bool ActivateWidgetButton( const char* BANK_ONLY( path ) )
{
#if __BANK
    bkButton *pButtonWidget = FindButtonWidget( path );
    if ( pButtonWidget == NULL )
    {
        return false;
    }

    pButtonWidget->Activate();
    return true;
#else // __BANK
    return false;
#endif
}

//#############################################################################
// bkColor
//#############################################################################

static bool GetWidgetColorValueFloat( const char* BANK_ONLY( path ), 
                                     float& BANK_ONLY( red ), float& BANK_ONLY( green ), float& BANK_ONLY( blue ), float& BANK_ONLY( alpha ) )
{
#if __BANK
    bkColor *pColorWidget = FindColorWidget( path );
    if ( pColorWidget == NULL )
    {
        return false;
    }

    pColorWidget->Getf( red, green, blue, alpha );
    return true;
#else // __BANK
    return false;
#endif
}

static bool SetWidgetColorValueFloat( const char* BANK_ONLY( path ), 
                                     float BANK_ONLY( red ), float BANK_ONLY( green ), float BANK_ONLY( blue ), float BANK_ONLY( alpha ) )
{
#if __BANK
    bkColor *pColorWidget = FindColorWidget( path );
    if ( pColorWidget == NULL )
    {
        return false;
    }

    pColorWidget->Setf( red, green, blue, alpha );
    return true;
#else // __BANK
    return false;
#endif
}

static bool GetWidgetColorValueInt( const char* BANK_ONLY( path ), 
                                   int& BANK_ONLY( red ), int& BANK_ONLY( green ), int& BANK_ONLY( blue ), int& BANK_ONLY( alpha ))
{
#if __BANK
    bkColor *pColorWidget = FindColorWidget( path );
    if ( pColorWidget == NULL )
    {
        return false;
    }

    pColorWidget->Get( red, green, blue, alpha );
    return true;
#else // __BANK
    return false;
#endif
}

static bool SetWidgetColorValueInt( const char* BANK_ONLY( path ), 
                                   int BANK_ONLY( red ), int BANK_ONLY( green ), int BANK_ONLY( blue ), int BANK_ONLY( alpha ) )
{
#if __BANK
    bkColor *pColorWidget = FindColorWidget( path );
    if ( pColorWidget == NULL )
    {
        return false;
    }

    pColorWidget->Set( red, green, blue, alpha );
    return true;
#else // __BANK
    return false;
#endif
}

//#############################################################################
// bkCombo
//#############################################################################

static int GetWidgetComboSelectedIndex( const char* BANK_ONLY( path ) )
{
#if __BANK
    bkCombo *pComboWidget = FindComboWidget( path );
    if ( pComboWidget == NULL )
    {
        return -1;
    }

    return pComboWidget->GetValue();
#else // __BANK
    return -1;
#endif
}

static bool SetWidgetComboSelectedIndex( const char* BANK_ONLY( path ), int BANK_ONLY( index ) )
{
#if __BANK
    bkCombo *pComboWidget = FindComboWidget( path );
    if ( pComboWidget == NULL )
    {
        return false;
    }

    pComboWidget->SetValue( index );
    return true;
#else // __BANK
    return false;
#endif
}

//#############################################################################
// bkImageViewer
//#############################################################################

static const char* GetWidgetImageViewerFilename( const char* BANK_ONLY( path ) )
{
#if __BANK
    bkImageViewer *pImageViewerWidget = FindImageViewerWidget( path );
    if ( pImageViewerWidget == NULL )
    {
        return NULL;
    }

    return pImageViewerWidget->GetImage();
#else // __BANK
    return NULL;
#endif // __BANK
}

static bool SetWidgetImageViewerFilename( const char* BANK_ONLY( path ), const char* BANK_ONLY( filename ) ) 
{
#if __BANK
    bkImageViewer *pImageViewerWidget = FindImageViewerWidget( path );
    if ( pImageViewerWidget == NULL )
    {
        return false;
    }

    pImageViewerWidget->SetImage( filename );
    return true;
#else // __BANK
    return false;
#endif // __BANK
}

//#############################################################################
// bkSlider
//#############################################################################

static float GetWidgetSliderFloatValue( const char* BANK_ONLY( path ) )
{
#if __BANK
    bkSlider* pSliderWidget = FindSliderWidget( path );
    if ( pSliderWidget == NULL )
    {
        return 0.0f;
    }

    char cBuf[32];
    pSliderWidget->GetStringRepr( cBuf, sizeof(cBuf) );

    return (float)atof( cBuf );
#else // __BANK
    return 0.0f;
#endif
}

static bool SetWidgetSliderFloatValue( const char* BANK_ONLY( path ), float BANK_ONLY( f ) )
{
#if __BANK
    bkSlider* pSliderWidget = FindSliderWidget( path );
    if ( pSliderWidget == NULL )
    {
        return false;
    }

    char cBuf[32];
    sprintf( cBuf, "%f", f );
    pSliderWidget->SetStringRepr( cBuf );
    return true;
#else // __BANK
    return false;
#endif
}

static int GetWidgetSliderIntValue( const char* BANK_ONLY( path ) )
{
#if __BANK
    bkSlider* pSliderWidget = FindSliderWidget( path );
    if ( pSliderWidget == NULL )
    {
        return 0;
    }

    char cBuf[32];
    pSliderWidget->GetStringRepr( cBuf, sizeof(cBuf) );

    return atoi( cBuf );
#else // __BANK
    return 0;
#endif
}

static bool SetWidgetSliderIntValue( const char* BANK_ONLY( path ), int BANK_ONLY( i ) )
{
#if __BANK
    bkSlider* pSliderWidget = FindSliderWidget( path );
    if ( pSliderWidget == NULL )
    {
        return false;
    }

    char cBuf[32];
    sprintf( cBuf, "%d", i );
    pSliderWidget->SetStringRepr( cBuf );
    return true;
#else // __BANK
    return false;
#endif
}

//#############################################################################
// bkToggle
//#############################################################################

static bool GetWidgetToggleValue( const char* BANK_ONLY( path ) )
{
#if __BANK
    bkToggle *pToggleWidget = FindToggleWidget( path );
    if ( pToggleWidget == NULL )
    {
        return false;
    }

    return pToggleWidget->GetBool();
#else // __BANK
    return false;
#endif
}

static bool SetWidgetToggleValue( const char* BANK_ONLY( path ), bool BANK_ONLY( b ) )
{
#if __BANK
    bkToggle *pToggleWidget = FindToggleWidget( path );
    if ( pToggleWidget == NULL )
    {
        return false;
    }

    pToggleWidget->SetBool( b );
    return true;
#else // __BANK
    return false;
#endif
}

//#############################################################################
// Register_BankScript
//#############################################################################

void Register_BankScript()
{
    // General
    SCR_REGISTER_SECURE(WIDGET_EXISTS, 0x44256f57, WidgetExists );
    SCR_REGISTER_SECURE(GET_WIDGET_VALUE_TO_STRING, 0x314cbe68, GetWidgetValueToString );
    SCR_REGISTER_SECURE(SET_WIDGET_VALUE_FROM_STRING, 0x3fdd7aa2, SetWidgetValueFromString );

    // bkAngle
    SCR_REGISTER_SECURE(GET_WIDGET_ANGLE_VALUE_DEGREES, 0xffa08a69, GetWidgetAngleValueDegrees );
    SCR_REGISTER_SECURE(SET_WIDGET_ANGLE_VALUE_DEGREES, 0x83696b23, SetWidgetAngleValueDegrees );
    SCR_REGISTER_SECURE(GET_WIDGET_ANGLE_VALUE_FRACTION, 0x44a3ece7, GetWidgetAngleValueFraction );
    SCR_REGISTER_SECURE(SET_WIDGET_ANGLE_VALUE_FRACTION, 0xf4f368df, SetWidgetAngleValueFraction );
    SCR_REGISTER_SECURE(GET_WIDGET_ANGLE_VALUE_RADIANS, 0xef651f10, GetWidgetAngleValueRadians );
    SCR_REGISTER_SECURE(SET_WIDGET_ANGLE_VALUE_RADIANS, 0x5146e2a9, SetWidgetAngleValueRadians );
    SCR_REGISTER_SECURE(GET_WIDGET_ANGLE_VALUE_VECTOR2, 0x8dc10027, GetWidgetAngleValueVector2 );
    SCR_REGISTER_SECURE(SET_WIDGET_ANGLE_VALUE_VECTOR2, 0x936799bb, SetWidgetAngleValueVector2 );

    // bkVector2
    SCR_REGISTER_SECURE(GET_WIDGET_VECTOR2_VALUE, 0xb67cd79f, GetWidgetVector2Value );
    SCR_REGISTER_SECURE(SET_WIDGET_VECTOR2_VALUE, 0x4d838fc, SetWidgetVector2Value );

    // bkVector3
    SCR_REGISTER_SECURE(GET_WIDGET_VECTOR3_VALUE, 0x79e619c7, GetWidgetVector3Value );
    SCR_REGISTER_SECURE(SET_WIDGET_VECTOR3_VALUE, 0x70a2d917, SetWidgetVector3Value );

    // bkVector4
    SCR_REGISTER_SECURE(GET_WIDGET_VECTOR4_VALUE, 0x94e226a5, GetWidgetVector4Value );
    SCR_REGISTER_SECURE(SET_WIDGET_VECTOR4_VALUE, 0x13ce754, SetWidgetVector4Value );

    // bkButton
    SCR_REGISTER_SECURE(ACTIVATE_WIDGET_BUTTON, 0x3e9506e0, ActivateWidgetButton );

    // bkColor
    SCR_REGISTER_SECURE(GET_WIDGET_COLOR_VALUE_FLOAT, 0xb16c9828, GetWidgetColorValueFloat );
    SCR_REGISTER_SECURE(SET_WIDGET_COLOR_VALUE_FLOAT, 0xedfeda03, SetWidgetColorValueFloat );
    SCR_REGISTER_SECURE(GET_WIDGET_COLOR_VALUE_INT, 0xc9d43bbd, GetWidgetColorValueInt );
    SCR_REGISTER_SECURE(SET_WIDGET_COLOR_VALUE_INT, 0x4f773a2d, SetWidgetColorValueInt );

    // bkCombo
    SCR_REGISTER_SECURE(GET_WIDGET_COMBO_SELECTED_INDEX, 0x47204633, GetWidgetComboSelectedIndex );
    SCR_REGISTER_SECURE(SET_WIDGET_COMBO_SELECTED_INDEX, 0xbd09fce0, SetWidgetComboSelectedIndex );

    // bkImageViewer
    SCR_REGISTER_SECURE(GET_WIDGET_IMAGEVIEWER_FILENAME, 0xb7fde219, GetWidgetImageViewerFilename );
    SCR_REGISTER_SECURE(SET_WIDGET_IMAGEVIEWER_FILENAME, 0xb86f743e, SetWidgetImageViewerFilename );

    // bkSlider
    SCR_REGISTER_SECURE(GET_WIDGET_SLIDER_FLOAT_VALUE, 0xcf484fd8, GetWidgetSliderFloatValue );
    SCR_REGISTER_SECURE(SET_WIDGET_SLIDER_FLOAT_VALUE, 0xd2eb75c2, SetWidgetSliderFloatValue );
    SCR_REGISTER_SECURE(GET_WIDGET_SLIDER_INT_VALUE, 0xe320c5eb, GetWidgetSliderIntValue );
    SCR_REGISTER_SECURE(SET_WIDGET_SLIDER_INT_VALUE, 0xf4b7149f, SetWidgetSliderIntValue );

    // bkToggle
    SCR_REGISTER_SECURE(GET_WIDGET_TOGGLE_VALUE, 0x7002d14c, GetWidgetToggleValue );
    SCR_REGISTER_SECURE(SET_WIDGET_TOGGLE_VALUE, 0x2d52e8e3, SetWidgetToggleValue );
}

} // namespace rageBankScriptBindings