// ps3ppusnc -Xs......u -O2 threadtest5.cpp: 0.144s
// (Compare to threadtest4.cpp and threadtest5.cpp)

enum {
	OP_LOAD_R0_CONST,
	OP_LOAD_R1_CONST,
	OP_ADD_R0_U8,
	OP_J,
	OP_JGE,
	OP_RET
};

void Run(unsigned *fp, unsigned *startpc)
{
	static void *dispatch[] = {
		&&LOAD_R0_CONST,
		&&LOAD_R1_CONST,
		&&ADD_R0_U8,
		&&J,
		&&JGE,
		&&RET
	};

#define NEXT do { ni = nni; no = nno; nno = pc[2]; nni = dispatch[nno>>24]; pc+=1; goto *ni; } while(0)
#define LoadImm24 (no & 0xFFFFFF)
#define SETPC(o) do { pc = (unsigned*)((char*)startpc + (o)); no = pc[0]; ni = dispatch[no>>24]; nno = pc[1]; nni = dispatch[nno>>24]; goto *ni; } while (0)

	unsigned r0 = 0, r1 = 0;
	unsigned *pc = startpc;
	void *ni, *nni;
	unsigned no, nno;

	SETPC(0);

	LOAD_R0_CONST: r0 = LoadImm24; NEXT;

	LOAD_R1_CONST: r1 = LoadImm24; NEXT;

	ADD_R0_U8: r0 += LoadImm24; NEXT;

	J: SETPC(LoadImm24);

	JGE: if (r0 >= r1) SETPC(LoadImm24); else NEXT;

	RET: return;
}

unsigned char opcodes[] = {
	OP_LOAD_R0_CONST, 0x00,0x00,0x00,	// 0000
	OP_LOAD_R1_CONST, 0x98,0x96,0x80,	// 0004
	OP_JGE, 0x00,0x00,0x20,			// 0008
	OP_ADD_R0_U8, 0x00,0x00,0x01,		// 000c
	OP_ADD_R0_U8, 0x00,0x00,0x01,		// 0010
	OP_ADD_R0_U8, 0x00,0x00,0x01,		// 0014
	OP_ADD_R0_U8, 0x00,0x00,0x01,		// 0018
	OP_J, 0x00,0x00,0x04,			// 001c
	OP_RET					// 0020
};


#include <stdio.h>
#include <sys/time_util.h>
#define TPS 79800000.0f

int main() {
	unsigned long long start, end;
	unsigned long long best = ~0U;
	const int runs = 5;
	unsigned fp[256];
	for (int i=0; i<runs; i++) {
		SYS_TIMEBASE_GET(start);
		Run(fp, (unsigned*)opcodes);
		SYS_TIMEBASE_GET(end);
		if (best > (end-start))
			best = end-start;
	}
	printf("%f best over %d runs\n",best / TPS,runs);
	return 0;
}
