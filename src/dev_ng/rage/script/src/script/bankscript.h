// 
// scriptgui/bankscript.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SCRIPT_BANKSCRIPT_H 
#define SCRIPT_BANKSCRIPT_H 

namespace rageBankScriptBindings {
    void Register_BankScript();
} // namespace rageBankScriptBindings


#endif // SCRIPT_BANKSCRIPT_H 
