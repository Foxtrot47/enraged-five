#ifndef SCRIPT_ARRAY_H
#define SCRIPT_ARRAY_H

#include "script/wrapper.h"

//////////////////////////////////////////////////////////////////////////

// this is used by variable-length arrays (passed by reference into subprograms)


/*
USAGE:
Script side: 
STRUCT sMyStruct
INT someValue
STRING someString
ENDSTRUCT

NATIVE PROC MY_FUNCTION( sMyStruct& inputData[] )

SCRIPT
	sMyStruct someData[4]
	// fill it out
	MY_FUNCTION(someData)
ENDSCRIPT


Code side: 
struct scrMyStruct {
	int someValue;
	const char* someString;
};

CommandMyFunction( scrArrayBaseAddr& thePoint) { 
	int dataSize = GetCount(thePoint); // will return 4 in the example above
	scrMyStruct pData[] = GetElements(thePoint); 
	...
}

*/

typedef int scrArrayBaseAddr;

namespace scrArray
{
	template <typename _Type> 
	static __forceinline _Type* GetElements(scrArrayBaseAddr& point)
	{
		// we need to increment forward to get at the real data
		return reinterpret_cast<_Type*>((scrValue*)&point + 1);
	}

	template <typename _Type> 
	static __forceinline const _Type* const GetElements(const scrArrayBaseAddr& point)
	{
		// we need to increment forward to get at the real data
		return reinterpret_cast<const _Type* const>((const scrValue*)&point + 1);
	}

	static __forceinline const int GetCount(const scrArrayBaseAddr& point)
	{
		return point;
	}
};

SCR_DEFINE_NEW_POINTER_PARAM_AND_RET(scrArrayBaseAddr);

#endif // SCRIPT_ARRAY_H

