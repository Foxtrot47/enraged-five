#include "thread.h"
#include "opcode.h"
#include "program.h"
#include "scrchannel.h"
#include "hash.h"
#include "wrapper.h"
#include "varargs.h"
#include "atl/binmap.h"
#include "atl/functor.h"
#include "bank/console.h"
#include "bank/button.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/combo.h"
#include "diag/debuglog.h"
#include "diag/output.h"
#include "file/asset.h"
#include "profile/timebars.h"
#include "system/bootmgr.h"
#include "system/cache.h"
#include "system/exception.h"
#include "system/nelem.h"
#include "system/magicnumber.h"
#include "system/param.h"
#include "system/stack.h"
#include "system/timer.h"

#include <setjmp.h>

#if __PPU
#include <sn/libsntuner.h>
#endif

#if !__FINAL
#include "file/remote.h"
#endif

#include "system/exec.h"
#if __PS3
#include <sys/prx.h>
#endif

#include <limits.h>
#include <math.h>
#include <stdarg.h>

using namespace rage;
using namespace rage::rageScriptDebug;

#if EXECUTABLE_SCRIPTS
namespace rage {

const int max_stack_save_size = 4096;

struct scrExecState 
{
	jmp_buf wait, exec;
	unsigned stack_save_size;
	u8 stack_save[max_stack_save_size];
};

}
#endif


#if __ASSERT && 0
namespace rage
{
	extern Functor0Ret<bool> g_PrintStackTraceFunc;
}
#endif

#define LoadImm32	((pc+=4), *(u32*)(&opcodes[pc-4]))
#if __BE
#define LoadImm24	((pc+=3), *(u32*)(&opcodes[pc-4]) & 0xFFFFFF)
#else
#define LoadImm24	((pc+=3), *(u32*)(&opcodes[pc-4]) >> 8)
#endif
#define LoadImm16	((pc+=2), *(u16*)(&opcodes[pc-2]))
#define LoadImmS16	((pc+=2), *(s16*)(&opcodes[pc-2]))
#define LoadImm8	((++pc), (opcodes[pc-1]))

#define LOCAL_STACK			(!SCRIPT_DEBUGGING)
#define VALIDATE_REFERENCES	(SCRIPT_DEBUGGING)

#if LOCAL_STACK //	Changelist 119020 by Matt Shepcar - scrThread optimisation - store sp in a local variable
#define _Pushi(v)	(m_Stack[m_Serialized.m_SP++].Int = v)
#define Pushi(v)	((++sp)->Int = (v))
#define Pushf(v)	((++sp)->Float = (v))
#define Pushr(v)	((++sp)->Reference = (v))
#define Pusha(v)	((++sp)->Any = (v))
#define Popi()		((sp--)->Int)
#define Popf()		((sp--)->Float)
#define Popr()		((sp--)->Reference)
#define Popa()		((sp--)->Any)
#define Topi()		(sp->Int)
#define Topf()		(sp->Float)
#define Topr()		(sp->Reference)
#define Topa()		(sp->Any)
#define StoreSp()	(m_Serialized.m_SP = ptrdiff_t_to_int(sp - m_Stack) + 1) // use before reads from m_Serialized.m_SP
#define LoadSp()	(sp = m_Stack + m_Serialized.m_SP - 1) // use after writes to m_Serialized.m_SP
#else
#define _Pushi Pushi
#define LoadSp()
#define StoreSp()
#endif	//	LOCAL_STACK

#if __WIN32
#pragma warning(disable:4709)
#endif

#if __DEV
const char *scrThread::sm_DevCurrentCmdName;
u32 scrThread::sm_DevCurrentCmdNameCounter;
#endif
float scrThread::sm_TimeStep = 1.0f / 60;
float scrThread::sm_TimeStepUnwarped = 1.0f / 60;
bool scrThread::sm_IsPaused = false;
bool scrThread::sm_IsBlocked = false;
bool scrThread::sm_UpdateTimersWhenPaused = true;		// if true (default) we want timerA and timerB to update while the sm_IsPaused flag is set

atArray<scrThread*> scrThread::sm_Threads;
atArray<scrThread::ThreadStack> scrThread::sm_Stacks;
atArray<scrThread::Global> scrThread::sm_Globals;
#if RSG_PC && RSG_CPU_X64
atMap<sysObfuscated<u32>, sysObfuscated<u64>> scrThread::sm_ProgIdRunFunctions;
#endif




u32 scrThread::sm_ThreadId;

const char* scrThread::sm_PrintStackPrefix = ">>>";


scrCommandHash<scrCmd> s_CommandHash;

struct scrSignatureAndCmd
{
	scrSignatureAndCmd() : cmd(NULL) {}
	scrSignatureAndCmd(scrSignature s, scrCmd c) : sig(s), cmd(c) {}

	scrSignature sig;
	scrCmd cmd;
};

static atStringMap<scrSignatureAndCmd> s_SignatureHash; // Using a stringMap instead of a scrHash because we can afford slower lookup, but want to use less memory

#if SCRIPT_DEBUGGING
struct NativeCallLogInfo
{
	enum Action
	{
		ACTION_None,
		ACTION_Log,
		ACTION_DumpStack,
		ACTION_Break,

		ACTION_Count
	};

	NativeCallLogInfo() {}
	NativeCallLogInfo(scrCmd c, const char* f, const char* n) : cmd(c), filename(f), name(n), action(ACTION_None), pWidget(NULL) {}

	scrCmd cmd;
	const char* filename;
	const char* name;
	int action;
	bkWidget* pWidget;
};
typedef atMap<void*, NativeCallLogInfo> LogNativeCallMap;
static LogNativeCallMap s_LogNativeCalls;

static const char* s_nativeTraceOptions[] =
{
	"None",
	"Log calls",
	"Dump stack on call",
	"Assert on call"
};
CompileTimeAssert(NELEM(s_nativeTraceOptions) == NativeCallLogInfo::ACTION_Count);

bkBank* scrThread::ms_pNativeTraceBank = NULL;
bkButton* scrThread::ms_pCreateNativeTraceButton = NULL;
#endif

#if !__FINAL
CScriptArraySizeMonitor scrThread::ms_scriptArraySizeMonitor;
#endif	//	!__FINAL


static scrThread *s_CurrentThreadGlobal;
static __THREAD scrThread *s_CurrentThread;
static __THREAD bool s_ThisThreadIsRunningAScript;

inline float scr_fmodf(float x, float y)	{ return y? x - ((int)(x/y) * y) : 0; }

#if __BANK
void scrWrapper::BeginTrack(const char* RAGE_TRACKING_ONLY(name))
{
#if RAGE_TRACKING
#if __DEV
	scrThread::sm_DevCurrentCmdName = name;
	scrThread::sm_DevCurrentCmdNameCounter++;
#endif
	++g_TrackerDepth;
	if (diagTracker::GetCurrent()) {
		diagTracker::GetCurrent()->Push(name); 
	}
#endif
}

void scrWrapper::EndTrack()
{
#if RAGE_TRACKING
	if (diagTracker::GetCurrent()) 
		diagTracker::GetCurrent()->Pop(); 
	--g_TrackerDepth;
#endif
}
#endif


#if !__FINAL
CScriptArraySizeMonitor::CScriptArraySizeMonitor()
{
	Reset();
}

void CScriptArraySizeMonitor::StartMonitoring(s32 *pAddressToMonitor, scrThreadId ownerThread)
{
	m_pAddressToMonitor = pAddressToMonitor;
	m_OriginalValue = *m_pAddressToMonitor;
	m_OwnerThreadId = ownerThread;
}

void CScriptArraySizeMonitor::StopMonitoringForThisThread(scrThreadId threadIdToCheck)
{
	if (threadIdToCheck == m_OwnerThreadId)
	{
		Reset();
	}
}

bool CScriptArraySizeMonitor::CheckIfMonitoredAddressHasChanged(const char *pStringToDisplay, s32 integerToDisplay)
{
	bool bReturnValue = false;

	if (m_pAddressToMonitor)
	{
		s32 currentValue = *m_pAddressToMonitor;
		if (currentValue != m_OriginalValue)
		{
			if (integerToDisplay != 0)
			{
				scrDisplayf("CScriptArraySizeMonitor::CheckIfMonitoredAddressHasChanged - size of monitored array has changed. Address = 0x%p, Original size = %d New size = %d %s %d", 
					m_pAddressToMonitor, m_OriginalValue, currentValue, pStringToDisplay?pStringToDisplay:"", integerToDisplay);
			}
			else
			{
				scrDisplayf("CScriptArraySizeMonitor::CheckIfMonitoredAddressHasChanged - size of monitored array has changed. Address = 0x%p, Original size = %d New size = %d %s", 
					m_pAddressToMonitor, m_OriginalValue, currentValue, pStringToDisplay?pStringToDisplay:"");
			}
			scrDisplayf("CScriptArraySizeMonitor::CheckIfMonitoredAddressHasChanged - Script callstack is ");
			scrThread::PrePrintStackTrace();

			scrDisplayf("CScriptArraySizeMonitor::CheckIfMonitoredAddressHasChanged - Full callstack is ");
			sysStack::PrintStackTrace();

			Reset();	//	We've reported the first occurrence. That should be enough.

			bReturnValue = true;
		}
	}

	return bReturnValue;
}

void CScriptArraySizeMonitor::Reset()
{
	m_pAddressToMonitor = NULL;
	m_OriginalValue = 0;
	m_OwnerThreadId = THREAD_INVALID;
}
#endif	//	!__FINAL


#if SCRIPT_DEBUGGING
static scrCmd s_FaultOnFunc=0;		// Function to throw a fault on
static char s_BreakFuncName[64]={0};
static bool s_bClearOnHit=false;
static bool s_bPrintOnly=false;
static atMap<const char*, scrCmd> s_DebugCommandNameMap;
void scrThread::SetFaultOnFunc(const char *pFuncName, bool bPrintOnly, bool bClearOnHit)
{
	if (pFuncName)
	{
		formatf(s_BreakFuncName, sizeof(s_BreakFuncName), "%s", pFuncName);
		scrCmd* pCmd = s_DebugCommandNameMap.Access(pFuncName);
		s_FaultOnFunc = pCmd ? *pCmd : 0;
		s_bClearOnHit = bClearOnHit;
		s_bPrintOnly = bPrintOnly;
	}
	else
	{
		s_FaultOnFunc = 0;
		s_BreakFuncName[0] = 0;
	}
}

// CScriptCutoutTimeTracker static members
int	CScriptCutoutTimeTracker::m_funcCount = 0;

#endif


#if USE_PROFILER_NORMAL
struct ScriptEventDescriptionStorage
{
	atArray<const Profiler::EventDescription*> m_descriptions;

	ScriptEventDescriptionStorage(u32 count)
	{
		m_descriptions.Resize(count);
	}

	const Profiler::EventDescription* GetDescription(u16 index)
	{
		if (index >= m_descriptions.GetCount())
		{
			static Profiler::EventDescription* invalidScriptFunction = Profiler::EventDescription::Create("UNKNOWN");
			return invalidScriptFunction;
		}

		return m_descriptions[index];
	}

	const Profiler::EventDescription* AddSharedDescription(u16 index, const char* name)
	{
		Profiler::EventDescription* newDescription = Profiler::EventDescription::Create(name, "Script");
		m_descriptions[index] = newDescription;
		return newDescription;
	}
};

struct ScriptProfilerStorage
{
	typedef atMap<u32, ScriptEventDescriptionStorage*> ProgramStorageMap;
	ProgramStorageMap id2storage;

	ScriptEventDescriptionStorage* AddStorage(const scrProgram& program)
	{
		PROFILER_CATEGORY("ScriptProfilerStorage::AddStorage", Profiler::CategoryColor::Debug);
		USE_ROCKY_MEMORY;
		ScriptEventDescriptionStorage* newStorage = nullptr;

		if (scrThread::GenerateProcNames(program) && program.GetProcCount()) {
			newStorage = rage_new ScriptEventDescriptionStorage(program.GetProcCount());
			for (u16 index = 0; index < program.GetProcCount(); ++index) {
				newStorage->AddSharedDescription(index, program.GetProcNames()[index]);
			}
		}

		id2storage.Insert(program.GetProgramId(), newStorage);

		return newStorage;
	}

	ScriptEventDescriptionStorage* GetOrCreateStorage(const scrProgram& program)
	{
		if (ScriptEventDescriptionStorage** storage = id2storage.Access(program.GetProgramId()))
		{
			// Check that all the names are generated properly
			if (!scrThread::GenerateProcNames(program))
				return nullptr;

			// Check that count matches
			scrAssertf((*storage)->m_descriptions.GetCount() == (int)program.GetProcCount(), "Descrtiption count mismatch! [%d != %d]", (*storage)->m_descriptions.GetCount(), program.GetProcCount());

			return *storage;
		}

		return AddStorage(program);
	}

	~ScriptProfilerStorage()
	{
		USE_ROCKY_MEMORY;
		for (ProgramStorageMap::Iterator it = id2storage.CreateIterator(); !it.AtEnd(); it.Next())
			delete it.GetData();

		id2storage.Reset();
	}
};

ScriptProfilerStorage g_eventDescriptions;
#endif

#if USE_PROFILER
static u64 s_autosamplingTimeLimit = 0;
static u64 s_nextTimestamp = 0;
static bool s_bIsRockyCapturingScriptCallstacks = false;

static void InitCallstackAutoSamplingVars(Profiler::ControlState state, u32 mode)
{
	s_bIsRockyCapturingScriptCallstacks = (state == Profiler::ROCKY_START && (mode & Profiler::Mode::SCRIPTS_AUTOSAMPLING));
	if (s_bIsRockyCapturingScriptCallstacks)
		s_autosamplingTimeLimit = (u64)((1.0f / sysTimer::GetTicksToMilliseconds()) * (1000.0f / Profiler::App::GetScriptSamplingFrequency()));
}

static bool ResolveScriptAddress(size_t address, Profiler::ScriptSymbol& outSymbol)
{
	scrThread::ResolveAddressForRocky(address, outSymbol);
	return true;
}
#endif // USE_PROFILER

scrThread::scrThread() 
:	m_Stack(NULL)
,	m_iInstructionCount(0)
,	m_argStructOffset(0)
,	m_argStructSize(0)
#if SCRIPT_DEBUGGING
,	m_MaxStackUse(0)
,	m_fUpdateTime(0.0f)
,	m_fConsoleOutputTime(0.0f)
#endif
#if SCRIPT_PROFILING
,	m_ProfStackCount(0)
,	m_ProfUpdateCount(0)
#endif
,	m_AbortReason("Not aborted yet?")
#if EXECUTABLE_SCRIPTS
,	m_ExecState(0)
#endif
{
	memset(&m_Serialized,0,sizeof(m_Serialized));
	m_Serialized.m_MinPC = -1;
	m_Serialized.m_MaxPC = -1;
	m_Serialized.m_State = RUNNING;
	m_Serialized.m_StackSize = 0;
	m_Serialized.m_Priority = THREAD_PRIO_NORMAL;
	m_Serialized.m_CallDepth = 0;
}


scrThread::~scrThread() {
#if EXECUTABLE_SCRIPTS
	delete[] m_ExecState;
#endif
}


#if LOCAL_STACK	//	Changelist 119020 by Matt Shepcar - scrThread optimisation - store sp in a local variable
				//	and Changelist 112752 by Adam Fowler - Removed a bunch of checks from release builds

#if SCRIPT_DEBUGGING
static char s_STSO[] = "Script thread stack overflow";
#define CheckForExtraOverflow(offset)\
	do {	StoreSp();\
			if (m_Serialized.m_SP+(offset) > m_MaxStackUse) m_MaxStackUse = m_Serialized.m_SP+(offset);\
			if (m_Serialized.m_SP+(offset) >= m_Serialized.m_StackSize) return Fault(pc,s_STSO, THREAD_STACK_OVERFLOW);\
	}while(0)
#else
#define CheckForExtraOverflow(offset)
//	if (m_Serialized.m_SP+(offset) >= m_Serialized.m_StackSize)	return Fault(s_STSO); else
#endif

#else	//	LOCAL_STACK

static char s_STSO[] = "Script thread stack overflow";
#if SCRIPT_DEBUGGING
#define CheckForExtraOverflow(offset)\
	do {	if (m_Serialized.m_SP+(offset) > m_MaxStackUse) m_MaxStackUse = m_Serialized.m_SP+(offset);\
	if (m_Serialized.m_SP+(offset) >= m_Serialized.m_StackSize) return Fault(pc,s_STSO, THREAD_STACK_OVERFLOW);\
	}while(0)
#else
#define CheckForExtraOverflow(offset)\
	if (m_Serialized.m_SP+(offset) >= m_Serialized.m_StackSize)	return Fault(pc,s_STSO, THREAD_STACK_OVERFLOW); else
#endif

#endif	//	LOCAL_STACK

#define CheckForOverflow()				CheckForExtraOverflow(0)


scrThread* scrThread::GetActiveThread() {
	return s_CurrentThread;
}

scrThread* scrThread::GetActiveThreadGlobal() {
	return s_CurrentThreadGlobal;
}

bool scrThread::IsThisThreadRunningAScript() {
	return s_ThisThreadIsRunningAScript;
}

void (*scrThread::AppFaultHandler)(const char *, scrThread *, scrThreadFaultID) = 0;

scrThread::State scrThread::Fault(int pc,const char *string, scrThreadFaultID faultType) 
{
	if (pc)
		m_Serialized.m_PC = pc-1;	// roll back to opcode of faulting insn
	m_AbortReason = string;

#if !__FINAL
	if (pc) {
		Errorf( "Script '%s' aborted due to '%s' [ins used = %d, pc at %d, stack at %d]", GetScriptName(), string, GetInstructionsUsed(), GetProgramCounter(0), GetStackSize());
		PrintStackTrace();
	}
#endif

#if __BANK
	if (m_debugFaultHandler)
		m_debugFaultHandler(string, this);
#endif

	if (AppFaultHandler)
		AppFaultHandler(string, this, faultType);

	return m_Serialized.m_State = ABORTED;
}

void scrThread::DisassembleInsn(char *dest,const scrProgram &opcodes,u32 pc)
{
	static const char *opnames[] = {
#define OP(a,b,c) #a
#include "opcodes.h"
#undef OP
	};
	static const char *fmts[] = {
#define OP(a,b,c) c
#include "opcodes.h"
#undef OP
	};

	u8 op = opcodes[pc];

	const char *name = opnames[op];
	const char *f = fmts[op];
	sprintf(dest,"%06d : %s",pc,name);
	u32 offset = pc+1, imm;
	s32 simm;
	while (*f) {
		switch (*f++) {
			case 'a':
#if __BE
				sprintf(dest + strlen(dest)," %06d",*(u32*)(&opcodes[offset])>>8);
#else
				sprintf(dest + strlen(dest)," %06d",*(u32*)(&opcodes[offset])&0xFFFFFF);
#endif
				offset += 3; 
				break;
			case 'b':
				sprintf(dest + strlen(dest)," %d",opcodes[offset++]); 
				break;
			case 'd':
				imm = *(u32*)(&opcodes[offset]);
				sprintf(dest + strlen(dest)," %d(0x%x)",imm,imm);
				offset += 4;
				break;
			case 'f':
				{
					volatile union { float f; unsigned u; } x;
					x.u = *(u32*)(&opcodes[offset]);
					sprintf(dest + strlen(dest)," %f",x.f);
				}
				offset += 4;
				break;
			case 'h':
				sprintf(dest + strlen(dest)," %d",*(u16*)(&opcodes[offset])); 
				offset += 2;
				break;
			case 'R':
				simm = *(s16*)(&opcodes[offset]);
				offset += 2;
				sprintf(dest + strlen(dest)," %06d (%+d)",offset + simm,simm); 
				break;
			case 'S':
				{
					int cases = opcodes[offset++];
					sprintf(dest + strlen(dest)," [%d]",cases);
					for (int i=0; i<(cases>4?4:cases); i++,offset+=6)
						sprintf(dest + strlen(dest)," %d:%06d",*(s32*)&opcodes[offset],offset + 6 + *(s16*)&opcodes[offset+4]);
					if (cases > 4)
						strcat(dest," ...");
					break;
				}
			case 's':
				sprintf(dest + strlen(dest)," %d", *(s16*)(&opcodes[offset])); 
				offset += 2;
				break;
			case '$':
				{
					// string is always last, don't need to update offset.
					const char *s = opcodes[offset]? (const char*)&opcodes[offset+1] : "";
					if (*(u8*)s == 255)		// skip special OP_ENTER marker
						++s;
					char *d = dest + strlen(dest);
					*d++ = ' ';
					*d++ = '[';
					int maxCount = 43;
					while (*s && --maxCount) {
						if (*s == '\n') {
							*d++ = '\\';
							*d++ = 'n';
						}
						else if (*s == '\r') {
							*d++ = '\\';
							*d++ = 'r';
						}
						else
							*d++ = *s;
						++s;
					}
					if (*s) {
						*d++ = '.';
						*d++ = '.';
						*d++ = '.';
					}
					*d++ = ']';
					*d = 0;
					break;
				}
		}
	}
}

const char *s_ScriptName;	// SOLELY FOR DEBUGGING, DO NOT REMOVE.

#if !__FINAL
scrTimingHelper::~scrTimingHelper()
{
	utimer_t delta = sysTimer::GetTicks() - Now;
	float ms = delta * sysTimerConsts::TicksToMilliseconds;

	if (ms > 1000.0f)
		Errorf("REALLY SLOW Script '%s' native function %016" I64FMT "x took %5.2f milliseconds to complete!",s_ScriptName,Hash,ms);
	else if (ms > 10.0f)
		Warningf("Script '%s' native function %016" I64FMT "x took %5.2f milliseconds to complete.",s_ScriptName,Hash,ms);
}
#endif

#define FULL_LOGGING 0
#if	FULL_LOGGING

static void Eval(char *dest,const scrValue *minSp,const scrValue *maxSp,const scrValue *minGl,const scrValue *maxGl,scrValue val) 
{
	if (val.Reference >= minSp && val.Reference < maxSp)
		sprintf(dest,"&stk%04x ",val.Reference - minSp);
	else if (val.Reference >= minGl && val.Reference < maxGl)
		sprintf(dest,"&g%06x ",val.Reference - minGl);
	/* else if (val.String >= (const char*)minOp && val.String < (const char*)maxOp)
		sprintf(dest,"&pc%05x ",val.String - (const char*)minOp); */
	else
		sprintf(dest,"%08x ",val.Uns);
}

static void LogThreadState(const scrValue *stack,const scrValue * /*stackMax*/,const scrValue *globals,const scrValue *globalsMax,u32 sp,u32 fp,const scrPageTable &opcodes,u32 pc)
{
	char buf[128];
	scrThread::DisassembleInsn(buf,opcodes,pc);
	Displayf("%s : SP=%04x FP=%04x | %s",s_ScriptName,sp,fp,buf);
	for (u32 i=0; i<sp+1; i+=8) {
		sprintf(buf,"%04x: ",i);
		for (u32 j=0; j<8; j++)
			Eval(buf + 6 + 9*j,stack,stack+sp+8,globals,globalsMax,stack[i+j]);
	}
	Displayf("%s",buf);
}
#endif	// FULL_LOGGING

#define SMALL_SCRIPT_CORE		(!SCRIPT_DEBUGGING && !SCRIPT_PROFILING)

struct scriptNameSetter {
	scriptNameSetter(const char* name) { m_Prev = s_ScriptName; s_ScriptName = name; }
	~scriptNameSetter() { s_ScriptName=m_Prev; }
	const char* m_Prev;
};

extern const char *diagAssertTitle;

PARAM(fastscriptcore,"Enable use of fast script core");

#if SCRIPT_PROFILING
PARAM(scriptprofiling,"Enable script profiling (counters reset every 250 updates by default)");
bool scrThread::sm_EnableScriptProfiling;
u32 scrThread::sm_ProfileResetInterval = 250;
#define PROF_TALLY(var) \
	do { if (m_ProfStackCount) { \
		utimer_t nowTick = sysTimer::GetTicks(); \
		u32 tix = u32(nowTick - lastTick); \
		lastTick = nowTick; \
		local##var[m_ProfStack[m_ProfStackCount-1]] += tix; \
		for (u32 i=0; i<m_ProfStackCount; i++) \
			m_ProfBuffer[m_ProfStack[i]].InclTicks += tix; \
	} } while (0)
#define PROF_TALLY_INSNS(var) \
	do { if (m_ProfStackCount) { \
		u32 tix = u32(lastInsns - insnCount); \
		lastInsns = insnCount; \
		local##var[m_ProfStack[m_ProfStackCount-1]] += tix; \
		for (u32 i=0; i<m_ProfStackCount; i++) \
			m_ProfBuffer[m_ProfStack[i]].InclInsns += tix; \
	} } while (0)
#endif

#if EXECUTABLE_SCRIPTS

static bool in_exec;
/* PS3 internally aligns the storage address to 16, so make sure everything is aligned properly so the location of r1
	doesn't magically change across builds:
	longjmp
	0160DDF8 3863000F addi       r3,r3,0xF
	0160DDFC 786306E4 clrrdi     r3,r3,4                       01 (0160DDF8) REG PIPE
	0160DE00 E8230000 ld         r1,0x0(r3)                    03 (0160DDFC) REG LSU
	0160DE04 E8430008 ld         r2,0x8(r3)                     PIPE
	0160DE08 E9A30010 ld         r13,0x10(r3)
	: : : : (r14 through r30)
	0160DE50 EBE300A0 ld         r31,0xA0(r3)
	0160DE54 E80300A8 ld         r0,0xA8(r3)                    PIPE
	0160DE58 7C0803A6 mtspr      lr,r0                         01 (0160DE54) REG
	0160DE5C E80300B0 ld         r0,0xB0(r3)
	0160DE60 7C101120 mtocrf     0x01,r0                       01 (0160DE5C) REG
	: : : : (other CR bits accordingly)
	0160DE7C 7C180120 mtocrf     0x80,r0                        PIPE
	0160DE80 C9C300B8 lfd        f14,0xB8(r3)
	: : : : (f14 through f30)
	0160DEC4 CBE30140 lfd        f31,0x140(r3)                  PIPE
	0160DEC8 E8030148 ld         r0,0x148(r3)
	0160DECC 7C0043A6 mtspr      VRSAVE,r0                     34+02 (0160DEC8) REG
	0160DEE0 7E8028CE lvx        v20,0,r5   ; 0x150
	: : : : (v21 through v30)
	0160DF2C 7FE040CE lvx        v31,0,r8                       PIPE
	0160DF30 38600000 li         r3,0x0
	0160DF34 7C041810 subfc      r0,r4,r3                      01 (0160DF30) REG PIPE
	0160DF38 7C640194 addze      r3,r4
*/

#if __PS3
#define r1_value(x)	((unsigned)(x)[1])
#elif __XENON
#define r1_value(x)	((unsigned)((_JUMP_BUFFER*)x)->Gpr1)
unsigned __declspec(naked) __builtin_frame_address() {
	__asm { 
		mr r3,r1
		blr
	}
}
#pragma warning(disable:4611)
#endif


#define COMPILED_SCRIPT_YIELD	do { if (in_exec) { if (setjmp(thread.m_ExecState->wait) == 0) longjmp(thread.m_ExecState->exec, 1); } } while (0)

scrThread::State scrThread::Exec(scrProgram *prog,unsigned caller_r1)
{
	Assert(!s_CurrentThread);
	s_CurrentThread = this;
	s_CurrentThreadGlobal = this;
	s_ThisThreadIsRunningAScript = true;
	in_exec = true;
	ASSERT_ONLY(unsigned sp = (unsigned)__builtin_frame_address());

	if (!m_ExecState)	// we store the jmp_buf, the size of the stack copied, followed by the stack bytes.
		m_ExecState = rage_new scrExecState;	// allocations 16 bytes or bigger are guaranteed to be 16-byte-aligned

	if (m_Serialized.m_State == ABORTED)
		/* fall through to common exit */;
	else if (m_Serialized.m_State == BLOCKED) {
		// Need to restore stack from saved state.
		Assertf(sp == r1_value(m_ExecState->exec),"%x != %x, stack nesting changed?",sp,r1_value(m_ExecState->exec));
		memcpy((void*)r1_value(m_ExecState->wait),m_ExecState->stack_save,m_ExecState->stack_save_size);
		longjmp(m_ExecState->wait,1);
	}
	else {
		// First run.
		void (*func)(scrValue*) = (void (*)(scrValue*))prog->Natives;
		if (setjmp(m_ExecState->exec) == 0) {
			func(m_Stack);
			Assertf(false,"Compiled script aborted on first run?");
			m_Serialized.m_State = ABORTED;
		}
		else {
			// Do initial stack save.  Also, a subsequent execution of a blocked thread
			// will end up back here again as well.  We need to know our caller's r1
			// value so that we can preserve our own Parameter Save State area
			m_ExecState->stack_save_size = caller_r1 - r1_value(m_ExecState->wait);
			Assert(m_ExecState->stack_save_size <= max_stack_save_size);
			memcpy(m_ExecState->stack_save,(void*)r1_value(m_ExecState->wait),m_ExecState->stack_save_size);
			m_Serialized.m_State = BLOCKED;
		}
	}
	in_exec = false;
	s_CurrentThread = 0;
	s_CurrentThreadGlobal = 0;
	s_ThisThreadIsRunningAScript = false;
	return m_Serialized.m_State;
}

#else

#define COMPILED_SCRIPT_YIELD

#endif

inline u16 DecodeIndex(const u8 *opcodes)
{
	// opcodes[-3] - Name length (u8)
	// opcodes[-2] - Index (u16)
	return opcodes[-3] ? *(u16*)(&opcodes[-2]) : (u16)-1;
}

#if USE_PROFILER_NORMAL
struct RockyStackEventsGuard
{
	int startEventCount;
	RockyStackEventsGuard() : startEventCount(0)
	{
		if (Profiler::EventStorage* storage = Profiler::TLS::storage)
			startEventCount = storage->startStopStack.GetCount();
	}

	~RockyStackEventsGuard()
	{
		if (Profiler::EventStorage* storage = Profiler::TLS::storage)
		{
			int leaveEventCount = storage->startStopStack.GetCount();

			while (leaveEventCount > startEventCount)
			{
				Profiler::StackedEvent::StackPop();
				--leaveEventCount;
			}
		}
	}
};
#endif

/*
	PURPOSE
		Execute the current thread for the specified number of instructions.
	PARAMS
		insnCount - Max number of instructions to execute
	RETURNS
		True if thread became blocked, else false
*/
scrThread::State scrThread::Run(int 
#if !SMALL_SCRIPT_CORE
								insnCount
#endif
								) 
{
	s_ScriptName = GetScriptName();

	PROFILER_STRING_EVENT(s_ScriptName);

	RAGE_TRACK(scrThread__Run);
#if RAGE_TRACKING
	::rage::diagTrackerHelper track_ScriptName(s_ScriptName);
#endif // RAGE_TRACKING

#if USE_PROFILER_NORMAL
	ScriptEventDescriptionStorage* storage = nullptr;
	if (Profiler::IsCollectingEvents(Profiler::Mode::SCRIPTS)) {
		storage = g_eventDescriptions.GetOrCreateStorage(*scrProgram::GetProgram(m_Serialized.m_Prog));
	}

	// Creating Profiling Event Guard to ensure PUSH/POP match
	RockyStackEventsGuard rockyStackGuard;
	(void)rockyStackGuard;

	// Restore all the profiling events
	if (m_Serialized.m_CallDepth > 0 && storage) {
		scrProgram* pProgram = scrProgram::GetProgram(m_Serialized.m_Prog);
		for (int i = 0; i < m_Serialized.m_CallDepth; ++i) {
			u16 index = DecodeIndex(&(*pProgram)[m_Serialized.m_CallStack[i]]);
			Profiler::StackedEvent::StackPush(storage->GetDescription(index));
		}
	}
#endif

#if SMALL_SCRIPT_CORE

	if (m_Serialized.m_State == ABORTED || m_Serialized.m_State == HALTED)
		return m_Serialized.m_State;

	scrThread *previous = s_CurrentThread;
	scrThread *previousGlobal = s_CurrentThreadGlobal;
	s_CurrentThread = this;
	s_CurrentThreadGlobal = this;
	s_ThisThreadIsRunningAScript = true;
#if FULL_LOGGING
	State result = (State) Run(m_Stack, m_Stack + m_Serialized.m_StackSize, scrProgram::GetGlobals(), scrProgram::GetGlobals() + scrProgram::GetGlobalSize(), scrProgram::GetProgram(m_Serialized.m_Prog)->GetOpcodes(), scrProgram::GetProgram(m_Serialized.m_Prog)->GetOpcodes() + scrProgram::GetProgram(m_Serialized.m_Prog)->GetOpcodeSize(),&m_Serialized);
#elif STREAMABLE_PROGRAMS
	const scrProgram &pt = *scrProgram::GetProgram(m_Serialized.m_Prog);
	State result = (State) Run(m_Stack, scrProgram::GetGlobalsBlocks(), pt, &m_Serialized);
#else
	State result = (State) Run(m_Stack, scrProgram::GetGlobalsBlocks(), scrProgram::GetProgram(m_Serialized.m_Prog)->GetOpcodes(), &m_Serialized);
#endif
	s_CurrentThread = previous;
	s_CurrentThreadGlobal = previousGlobal;
	s_ThisThreadIsRunningAScript = previous != 0;
	return result;

#else	// !SMALL_SCRIPT_CORE

	char faultString[48];
	PrefetchDC(m_Stack);

	if (m_Serialized.m_State == ABORTED || m_Serialized.m_State == HALTED)
		return m_Serialized.m_State;

	// Make sure s_CurrentThread is restored to its previous value on any way out.
	struct restorePrevious_t {
		scrThread *previous;
		scrThread *previousG;
		restorePrevious_t(scrThread *p, scrThread *pG) : previous(p), previousG(pG) { }
		~restorePrevious_t() {
			s_CurrentThread = previous;
			s_CurrentThreadGlobal = previousG;
			s_ThisThreadIsRunningAScript = previous != 0;
		}
	} restorePrevious(s_CurrentThread, s_CurrentThreadGlobal);
	s_CurrentThread = this;
	s_CurrentThreadGlobal = this;
	s_ThisThreadIsRunningAScript = true;

	// If there are no breakpoints in the current thread, and script profiling isn't enabled, use the faster core
	if (m_threadBreakpoints.GetNumUsed() == 0
#if SCRIPT_PROFILING
		&& !sm_EnableScriptProfiling
#endif
		&& (PARAM_fastscriptcore.Get() PS3_ONLY(|| snIsTunerRunning()))
		) {
		const scrProgram &pt = *scrProgram::GetProgram(m_Serialized.m_Prog);
		return (State) Run(m_Stack, scrProgram::GetGlobalsBlocks(), pt, &m_Serialized);
	}

	// Copy to locals for speed
	int pc = m_Serialized.m_PC;
	scrAssertf((pc>=0) && (pc<MAX_LEGIT_OPS), "Script thread has got a broken PC (%d) [broken externally to thread]", pc);
	scrProgram *pProgram = scrProgram::GetProgram(m_Serialized.m_Prog);
	scrAssertf(pProgram, "Unable to retrieve the program for id %d.", m_Serialized.m_Prog);
	const scrProgram &opcodes = *pProgram;
#if FULL_LOGGING
	u32 opSize = pProgram->GetOpcodeSize();
#endif
	scrValue **globals = scrProgram::GetGlobalsBlocks();

#if SCRIPT_DEBUGGING
	sysTimer runTimer;

	bool firstInsn = true;
	bool stepping = m_Serialized.m_MinPC != -1;
#if !__WIN32PC
	int closeToReachingInstructionLimit = insnCount >> 6;
#endif
#endif

	register scrValue* fp = &m_Stack[m_Serialized.m_FP];
#if LOCAL_STACK
	register scrValue* sp;
	LoadSp();
#else	//	LOCAL_STACK
	m_iInstructionCount = 0;
#endif	//	LOCAL_STACK

#if SCRIPT_PROFILING
	// u32 *localInclTicks = Alloca(u32, opcodes.ProcCount);
	u32 *localExclTicks = Alloca(u32, opcodes.ProcCount);
	u32 *localExclInsns = Alloca(u32, opcodes.ProcCount);
	u32 *localNatvTicks = Alloca(u32, opcodes.ProcCount);
	memset(localExclTicks, 0, sizeof(u32) * opcodes.ProcCount);
	memset(localExclInsns, 0, sizeof(u32) * opcodes.ProcCount);
	memset(localNatvTicks, 0, sizeof(u32) * opcodes.ProcCount);
	utimer_t lastTick = sysTimer::GetTicks();
	int lastInsns = insnCount;
#endif

#if !__FINAL
	ms_scriptArraySizeMonitor.CheckIfMonitoredAddressHasChanged("Before running script for this frame", 0);
#endif	//	!__FINAL

#define GOTO_HACK	(!SCRIPT_DEBUGGING)
#if !GOTO_HACK
	while (insnCount) {
		--insnCount;
#else
	{ DISPATCH:
		if (!insnCount) goto UNDISPATCH;
		--insnCount;
#define break goto DISPATCH
#endif

#if !LOCAL_STACK
		++m_iInstructionCount;
#endif

		AssertMsg(pc>=0,"Script thread has got a broken PC [broken during thread operation]");

#if FULL_LOGGING
#if LOCAL_STACK
		LogThreadState(&m_Stack[0],sp-&m_Stack[0]+1,fp-&m_Stack[0],opcodes,pc);
#else
		LogThreadState(&m_Stack[0],&m_Stack[0] + m_Serialized.m_StackSize,globals,globals + scrProgram::GetGlobalSize(),m_Serialized.m_SP,m_Serialized.m_FP,opcodes,pc);
#endif
#endif
		u8 opcode = opcodes[pc++];

#if SCRIPT_DEBUGGING
        if ( HasBreakpoint( pc - 1 ) || pProgram->HasBreakpoint( pc - 1 ) ) {
			if (!firstInsn && !stepping) {
				m_Serialized.m_PC = pc - 1;	// Point at instruction generating a fault.
				StoreSp();

                m_Serialized.m_State = HALTED;
#if __BANK
                if (m_debugBreakpointCallback)
                    m_debugBreakpointCallback(this, m_Serialized.m_PC);
#endif

				return m_Serialized.m_State;
			}
		}

		if (stepping && (pc - 1 < m_Serialized.m_MinPC || pc - 1 >= m_Serialized.m_MaxPC)) {
			StoreSp();
			m_Serialized.m_PC = pc - 1;	// Point at instruction generating a fault.
			m_Serialized.m_MinPC = -1;
			return m_Serialized.m_State = HALTED;
		}

		firstInsn = false;
#endif

#if __DEV && 0	// This is still pretty slow
		static char insn_trace[16][128];
		static int insn_next;
		insn_next = (insn_next+1) & 15;
		DisassembleInsn(insn_trace[insn_next],opcodes,pc-1);
		// Displayf("%s",insn_trace[insn_next]);
#endif

		switch (opcode) {
		
		case OP_IADD: { int i = Popi(); Topi() += i; break; }
		case OP_ISUB: { int i = Popi(); Topi() -= i; break; }
		case OP_IMUL: { int i = Popi(); Topi() *= i; break; }
		case OP_IDIV: { int i = Popi(); if (i) Topi() /= i; break; }
		case OP_IMOD: { int i = Popi(); if (i) Topi() %= i; break; }
		case OP_INOT: { Topi() = !Topi(); break; }
		case OP_INEG: { Topi() = -Topi(); break; }

		case OP_IEQ: { int i = Popi(); Topi() = Topi() == i; break; }
		case OP_INE: { int i = Popi(); Topi() = Topi() != i; break; }
		case OP_IGE: { int i = Popi(); Topi() = Topi() >= i; break; }
		case OP_IGT: { int i = Popi(); Topi() = Topi() > i; break; }
		case OP_ILE: { int i = Popi(); Topi() = Topi() <= i; break; }
		case OP_ILT: { int i = Popi(); Topi() = Topi() < i; break; }

		case OP_FADD: { float f = Popf(); Topf() += f; break; }
		case OP_FSUB: { float f = Popf(); Topf() -= f; break; }
		case OP_FMUL: { float f = Popf(); Topf() *= f; break; }
		case OP_FDIV: { float f = Popf(); if (f) Topf() /= f; break; }
		case OP_FMOD: { float f = Popf(); if (f) Topf() = scr_fmodf(Topf(),f); break; }
		case OP_FNEG: { Topf() = -Topf(); break; }

		case OP_FEQ: { float f = Popf(); Topi() = Topf() == f; break; }
		case OP_FNE: { float f = Popf(); Topi() = Topf() != f; break; }
		case OP_FGE: { float f = Popf(); Topi() = Topf() >= f; break; }
		case OP_FGT: { float f = Popf(); Topi() = Topf() > f; break; }
		case OP_FLE: { float f = Popf(); Topi() = Topf() <= f; break; }
		case OP_FLT: { float f = Popf(); Topi() = Topf() < f; break; }

		case OP_VADD: { float rz = Popf(), ry = Popf(), rx = Popf(); 
						float lz = Popf(), ly = Popf(), lx = Popf();
						Pushf(lx+rx); Pushf(ly+ry); Pushf(lz+rz); break; }
		case OP_VSUB: { float rz = Popf(), ry = Popf(), rx = Popf(); 
			float lz = Popf(), ly = Popf(), lx = Popf();
			Pushf(lx-rx); Pushf(ly-ry); Pushf(lz-rz); break; }
		case OP_VMUL: { float rz = Popf(), ry = Popf(), rx = Popf(); 
			float lz = Popf(), ly = Popf(), lx = Popf();
			Pushf(lx*rx); Pushf(ly*ry); Pushf(lz*rz); break; }
		case OP_VDIV: { float rz = Popf(), ry = Popf(), rx = Popf(); 
			float lz = Popf(), ly = Popf(), lx = Popf();
			Pushf(rx? lx/rx : 0.0f); Pushf(ry? ly/ry : 0.0f); Pushf(rz? lz/rz : 0.0f); break; }
		case OP_VNEG: { float z = Popf(), y = Popf(), x = Popf(); 
			Pushf(-x); Pushf(-y); Pushf(-z); break; }

		case OP_IAND: { int i = Popi(); Topi() &= i; break; }
		case OP_IOR: { int i = Popi(); Topi() |= i; break; }
		case OP_IXOR: { int i = Popi(); Topi() ^= i; break; }

		case OP_J: { int imm = LoadImmS16; pc += imm; break; }
		case OP_JZ: { int imm = LoadImmS16; if (Popi() == 0) pc += imm; break; }

		case OP_I2F: { Topf() = (float) Topi(); break; }
		case OP_F2I: { Topi() = (int) Topf(); break; }
		case OP_F2V: { float f = Popf(); Pushf(f); CheckForOverflow(); Pushf(f); CheckForOverflow(); Pushf(f); break; }

		case OP_PUSH_CONST_U8:
			Pushi(LoadImm8);
			break;

		case OP_PUSH_CONST_S16: 
			{
				int imm = LoadImmS16;
				CheckForOverflow();
				Pushi(imm); 
				break; 
			}

		case OP_PUSH_CONST_U24:
			{
				u32 imm = LoadImm24;
				Pushi(imm);
				break;
			}

		// These opcodes are distinct only for disassembly purposes:
		case OP_PUSH_CONST_U32:
		case OP_PUSH_CONST_F:
			{
				u32 imm = LoadImm32;
				CheckForOverflow();
				Pushi(imm); 
				break; 
			}

		case OP_DUP:
			{
				CheckForOverflow();
				scrAny top = Topa();
				Pusha(top);
				break;
			}

		case OP_DROP:
			{
				Popa();
				break;
			}

		case OP_NATIVE: 
			{
				m_Serialized.m_PC = pc - 1;
				int returnSize = LoadImm8;
				int paramCount = (returnSize >> 2) & 63;
				u32 imm = LoadImm8 << 8;
				imm |= LoadImm8;
				returnSize &= 3;
				scrCmd cmd = (scrCmd)opcodes.Natives[imm];
				{
#if SCRIPT_DEBUGGING
					if (s_FaultOnFunc==cmd)
					{
						scrDisplayf("Script %s calls user break function %s [ins=%d, stack=%d, pc=%d]", GetScriptName(), s_BreakFuncName, m_iInstructionCount, GetStackSize(), GetProgramCounter(0));
						PrintStackTrace();

						if (!s_bPrintOnly && AppFaultHandler)
						{
							scrDisplayf("Script %s calls user break function %s [ins=%d, stack=%d, pc=%d]", GetScriptName(), s_BreakFuncName, m_iInstructionCount, GetStackSize(), GetProgramCounter(0));
							AppFaultHandler("scrBreak triggered [see tty]", this, THREAD_UNKNOWN_ERROR_TYPE);

							//Did the app function stop the script?
							if (m_Serialized.m_State != RUNNING) {
								// Preserve PC of this opcode
								m_Serialized.m_PC = pc-c_NativeInsnLength;
								return m_Serialized.m_State;
							}
						}

						if (!s_bPrintOnly)
						{
							if (s_bClearOnHit || fiRemoteShowMessageBox("Would you like to clear this breakpoint so it doesn't happen again until the script sets it?",diagAssertTitle?diagAssertTitle:sysParam::GetProgramName(),MB_YESNO,IDNO)==IDYES)
								SetFaultOnFunc();
						}
					}
#endif

					StoreSp();
					Info curInfo(returnSize? &m_Stack[m_Serialized.m_SP - paramCount] : NULL, paramCount, &m_Stack[m_Serialized.m_SP - paramCount]);

#if SCRIPT_DEBUGGING
					const NativeCallLogInfo* nativeCallInfo = s_LogNativeCalls.Access((void*)cmd);
					if(nativeCallInfo && nativeCallInfo->action == NativeCallLogInfo::ACTION_Log)
					{
						// Lookup the signature for this native
						scrSignatureAndCmd* sigAndCmd = s_SignatureHash.SafeGet(scrComputeHash(nativeCallInfo->name));
						if(sigAndCmd)
						{
							// Grab name
							char buff[512];
							formatf(buff, "%s(", nativeCallInfo->name);
							size_t buffLen = strlen(buff);

							// Format parameters
							scrSignature* sig = &sigAndCmd->sig;
							Assertf(sig->CountNumStackEntriesForArgs() == paramCount, "Got %d args but expected %d", paramCount, sig->CountNumStackEntriesForArgs());
							int argIdx = 0;
							for(int i=0; i<paramCount; ++i)
							{
								char paramBuff[256];
								switch(sig->GetArgumentType(argIdx))
								{
								case scrValue::BOOL:
									formatf(paramBuff, "%s", curInfo.Params[i].Int ? "TRUE" : "FALSE");
									break;
								case scrValue::INT:
									formatf(paramBuff, "%d", curInfo.Params[i].Int);
									break;
								case scrValue::FLOAT:
									formatf(paramBuff, "%f", curInfo.Params[i].Float);
									break;
								case scrValue::VECTOR:
									{
										formatf(paramBuff, "(%f, %f, %f)", curInfo.Params[i].Float, curInfo.Params[i+1].Float, curInfo.Params[i+2].Float);
										i += 2;
									}
									break;
								case scrValue::TEXT_LABEL:
									formatf(paramBuff, "\"%s\"", scrDecodeString(curInfo.Params[i].String));
									break;
								case scrValue::STRING:
									formatf(paramBuff, "\"%s\"", scrDecodeString(curInfo.Params[i].String));
									break;
								case scrValue::OBJECT:
									formatf(paramBuff, "<Obj %p>", scrDecodeReference(curInfo.Params[i].Reference));
									break;
								default:
									formatf(paramBuff, "<Unknown>");
									break;
								}
								++argIdx;

								size_t paramLen = strlen(paramBuff);
								Assertf(buffLen+paramLen+2 < NELEM(buff), "Buffer overflow, increase size of buffer for native trace!");
								memcpy(buff+buffLen, paramBuff, paramLen);
								if(i != paramCount-1)
								{
									buff[buffLen+paramLen+0] = ',';
									buff[buffLen+paramLen+1] = ' ';
									buffLen += 2;
								}
								buffLen += paramLen;
							}
							buff[buffLen+0] = ')';
							buff[buffLen+1] = 0;
							scrDisplayf("Native trace: %s", buff);
						}
					}
					else if(nativeCallInfo && nativeCallInfo->action == NativeCallLogInfo::ACTION_DumpStack)
					{
						scrDisplayf("%s() called. Script Name = %s : Program Counter = %d : Stack:", nativeCallInfo->name, m_ScriptName, m_Serialized.m_PC);
						PrePrintStackTrace();
					}
					else if(nativeCallInfo && nativeCallInfo->action == NativeCallLogInfo::ACTION_Break)
					{
						Assertf(false, "Break on this script native requested. Script Name = %s : Program Counter = %d", m_ScriptName, m_Serialized.m_PC);
					}
#endif // SCRIPT_DEBUGGING

# if SCRIPT_PROFILING
					if (sm_EnableScriptProfiling) {
						PROF_TALLY(ExclTicks);
						PROF_TALLY_INSNS(ExclInsns);
					}
# endif
					(*cmd)(curInfo);
# if SCRIPT_PROFILING
					if (sm_EnableScriptProfiling)
						PROF_TALLY(NatvTicks);
# endif

#if !__FINAL
					ms_scriptArraySizeMonitor.CheckIfMonitoredAddressHasChanged("After calling native command", 0);
#endif	//	!__FINAL

					if (m_Serialized.m_State != RUNNING) {
						// Preserve PC of this opcode
						m_Serialized.m_PC = pc-c_NativeInsnLength;
#if SCRIPT_PROFILING
						// Recompute peak values on our way out.
						if (sm_EnableScriptProfiling) {
							++m_ProfUpdateCount;
							u32 pbc = m_ProfBuffer.GetCount();
							for (u32 i=0; i<pbc; i++) {
								ProfEntry &pe = m_ProfBuffer[i];
								if (pe.PeakExclTicks < localExclTicks[i])
									pe.PeakExclTicks = localExclTicks[i];
								if (pe.PeakExclInsns < localExclInsns[i])
									pe.PeakExclInsns = localExclInsns[i];
								if (pe.PeakNatvTicks < localNatvTicks[i])
									pe.PeakNatvTicks = localNatvTicks[i];
								if (m_ProfTotals.PeakExclTicks < localExclTicks[i])
									m_ProfTotals.PeakExclTicks = localExclTicks[i];
								if (m_ProfTotals.PeakExclInsns < localExclInsns[i])
									m_ProfTotals.PeakExclInsns = localExclInsns[i];
								if (m_ProfTotals.PeakNatvTicks < localNatvTicks[i])
									m_ProfTotals.PeakNatvTicks = localNatvTicks[i];
								pe.ExclTicks += localExclTicks[i];
								pe.ExclInsns += localExclInsns[i];
								pe.NatvTicks += localNatvTicks[i];
								m_ProfTotals.ExclTicks += localExclTicks[i];
								m_ProfTotals.ExclInsns += localExclInsns[i];
								m_ProfTotals.NatvTicks += localNatvTicks[i];
							}
						}
# endif
						return m_Serialized.m_State;
					}
					// If we didn't block, copy referenced parameters back into caller's stack.
					curInfo.CopyReferencedParametersOut();
					// Drop call parameters off of the stack but leave any return result there.
					m_Serialized.m_SP -= paramCount - returnSize;
					LoadSp();
				}
				break;
			}

		case OP_CALL:
			{
				int newpc = LoadImm24;
				CheckForOverflow();
				Pushi(pc);						// Return address is *next* insn
				pc = newpc;
				break;
			}

		case OP_CALLINDIRECT:
			{
				int newpc = Popi();
				CheckForOverflow();
				if (!newpc)
					return Fault(pc,"Attempted to call through uninitialized function pointer", THREAD_UNKNOWN_ERROR_TYPE);
				Pushi(pc);						// Return address is *next* insn
				pc = newpc;
				break;
			}

		case OP_ENTER:
			{
				int paramCount = LoadImm8;
				int localCount = LoadImm16;
				int nameLen = LoadImm8;
#if SCRIPT_PROFILING
				if (sm_EnableScriptProfiling) {
					PROF_TALLY(ExclTicks);
					PROF_TALLY_INSNS(ExclInsns);
					u16 slot = nameLen? *(u16*)&opcodes[pc] : 0;	// Pull bits out from the local count since we don't need all of them.
					m_ProfStack[m_ProfStackCount++] = slot;
				}
#endif

#if USE_PROFILER_NORMAL
				if (storage != nullptr) {
					u16 index = nameLen ? *(u16*)&opcodes[pc] : (u16)-1;
					Profiler::StackedEvent::StackPush(storage->GetDescription(index));
				}
#endif

				if (m_Serialized.m_CallDepth < MAX_CALLSTACK)
					m_Serialized.m_CallStack[m_Serialized.m_CallDepth] = pc + 2;
				++(m_Serialized.m_CallDepth);
				pc += nameLen;
				CheckForOverflow();
				Pushi(m_Serialized.m_FP);
				StoreSp();
				m_Serialized.m_FP = m_Serialized.m_SP - paramCount - 2;
				fp = &m_Stack[m_Serialized.m_FP];
				CheckForExtraOverflow(localCount);
				for (int i=m_Serialized.m_SP; i<m_Serialized.m_SP+localCount; i++)
					m_Stack[i].Reference = 0;
				m_Serialized.m_SP += localCount - paramCount;
				LoadSp();
				CheckForOverflow();
				break;
			}

		case OP_LEAVE:
			{
				int paramCount = LoadImm8;	// Get number of parameters to clean off the stack
				int returnSize = LoadImm8;	// Get size of the return value
#if SCRIPT_PROFILING
				if (sm_EnableScriptProfiling) {
					PROF_TALLY(ExclTicks);
					PROF_TALLY_INSNS(ExclInsns);
				--m_ProfStackCount;
				}
#endif

#if USE_PROFILER_NORMAL
				if (storage != nullptr) {
					Profiler::StackedEvent::StackPop();
				}
#endif 

				--(m_Serialized.m_CallDepth);
				StoreSp();
				int resultOffset = m_Serialized.m_SP - returnSize;	// Remember start of return value
				m_Serialized.m_SP = m_Serialized.m_FP + paramCount + 2;// Restore stack pointer, killing off all local variables
				LoadSp();
				CheckForOverflow();
				m_Serialized.m_FP = Popi();			// Get back previous frame pointer OP_ENTER preserved for us
				fp = &m_Stack[m_Serialized.m_FP];
				pc = Popi();			// Pull return address off of the stack placed by OP_CALL/OP_CALLINDIRECT
		
				scrAssertf((pc>=0) && (pc<=MAX_LEGIT_OPS), "Script thread has got a broken PC (%d) [broken during thread operation OP_LEAVE]", pc);

				StoreSp();
				m_Serialized.m_SP -= paramCount;		// Drop the caller's parameters
				// Copy the return value to the new top-of-stack
				scrAssertf(m_Serialized.m_SP <= resultOffset, "We need to copy in the other direction!");	// If this fails we need to copy in the other direction!
				while (returnSize--)
				{
					m_Stack[m_Serialized.m_SP++] = m_Stack[resultOffset++];
				}
				LoadSp();
				
				if (!pc) {
					m_AbortReason = "Reached end of program.";
					return m_Serialized.m_State = ABORTED;
				}

				break;
			}

		case OP_LOAD: 
			{ 
				scrValue *addr = Popr();
#if VALIDATE_REFERENCES
				if (!IsValidReference(addr))
					return Fault(pc,"Invalid reference caught during LOAD", THREAD_UNKNOWN_ERROR_TYPE);
#endif
				CheckForOverflow();
				Pusha(addr->Any);
				break; 
			 }

		case OP_STORE: 
			{ 
				scrValue *addr = Popr();
#if VALIDATE_REFERENCES
				if (!IsValidReference(addr))
					return Fault(pc,"Invalid reference caught during STORE", THREAD_UNKNOWN_ERROR_TYPE);
#endif
				addr->Any = Popa();
				break; 
			}

		case OP_STORE_REV: 
			{ 
				scrAny any = Popa();
				scrValue *addr = Topr();
#if VALIDATE_REFERENCES
				if (!IsValidReference(addr))
					return Fault(pc,"Invalid reference caught during STORE_REV", THREAD_UNKNOWN_ERROR_TYPE);
#endif
				addr->Any = any;
				break; 
			}

		case OP_LOAD_N: 
			{ 
				scrValue *addr = Popr(); 
				int count = Popi();
				CheckForExtraOverflow(count);
				for (int i=0; i<count; i++)
					Pusha(addr[i].Any);
				break; 
			}

		case OP_STORE_N:
			{
				// Values are pushed up to the stack in evaluation order,
				// so reverse them when storing
				scrValue *addr = Popr(); 
				int count = Popi();
				for (int i=0; i<count; i++)
					addr[count-1-i].Any = Popa();
				break; 
			}

		case OP_LOCAL_U8:
			Pushr(fp + LoadImm8); 
			break; 
		case OP_LOCAL_U16:
			Pushr(fp + LoadImm16); 
			break; 

		case OP_STATIC_U8:
			Pushr(&m_Stack[LoadImm8]); 
			break; 
		case OP_STATIC_U16:
			Pushr(&m_Stack[LoadImm16]); 
			break; 
		case OP_STATIC_U24:
			Pushr(&m_Stack[LoadImm24]);
			break;

		case OP_GLOBAL_U16:
			Assert(globals);
			Pushr(&globals[0][LoadImm16]); 
			break; 
		case OP_GLOBAL_U24:
			{
				u32 imm = LoadImm24, block = imm >> scrProgram::MAX_GLOBAL_BLOCKS_SHIFT, ofs = imm & scrProgram::GLOBAL_SIZE_MASK; 
				if (!globals[block]) 
				{
					formatf(faultString, "Global block %u not resident", block);
					Fault(pc,faultString, THREAD_UNKNOWN_ERROR_TYPE); 
				}
				Pushr(&globals[block][ofs]); 
				break; 
			}

		case OP_ARRAY_U8:
			{
				scrValue *r = Popr();	// Base address of array
				int idx = Popi();			// Array index
				if (idx < 0 || idx >= r->Int) {
					scrErrorf("Script %s ARRAY_U8 - array overrun - index = %d, max size = %d, pc = %d", GetScriptName(), idx, r->Int, pc);
					if (!r->Int) DumpArrayInfo(r);
					return Fault(pc,"Array overrun", THREAD_ARRAY_OVERFLOW);
				}
				r = r + 1 + idx * LoadImm8;
				Pushr(r);
				break;
			}
		case OP_ARRAY_U16:
			{
				scrValue *r = Popr();	// Base address of array
				int idx = Popi();			// Array index
				if (idx < 0 || idx >= r->Int) {
					scrErrorf("Script %s - ARRAY_U16 array overrun - index = %d, max size = %d, pc = %d", GetScriptName(), idx, r->Int, pc);
					if (!r->Int) DumpArrayInfo(r);
					return Fault(pc,"Array overrun", THREAD_ARRAY_OVERFLOW);
				}
				r = r + 1 + idx * LoadImm16;
				Pushr(r);
				break;
			}
		case OP_ARRAY_U8_LOAD:
			{
				scrValue *r = Popr();	// Base address of array
				int idx = Popi();			// Array index
				if (idx < 0 || idx >= r->Int) {
					scrErrorf("Script %s - ARRAY_U8_LOAD array overrun - index = %d, max size = %d, pc = %d", GetScriptName(), idx, r->Int, pc);
					if (!r->Int) DumpArrayInfo(r);
					return Fault(pc,"Array overrun", THREAD_ARRAY_OVERFLOW);
				}
				r = r + 1 + idx * LoadImm8;
				Pusha(r->Any);
				break;
			}
		case OP_ARRAY_U16_LOAD:
			{
				scrValue *r = Popr();	// Base address of array
				int idx = Popi();			// Array index
				if (idx < 0 || idx >= r->Int) {
					scrErrorf("Script %s - ARRAY_U16_LOAD array overrun - index = %d, max size = %d, pc = %d", GetScriptName(), idx, r->Int, pc);
					if (!r->Int) DumpArrayInfo(r);
					return Fault(pc,"Array overrun", THREAD_ARRAY_OVERFLOW);
				}
				r = r + 1 + idx * LoadImm16;
				Pusha(r->Any);
				break;
			}
		case OP_ARRAY_U8_STORE:
			{
				scrValue *r = Popr();	// Base address of array
				int idx = Popi();			// Array index
				if (idx < 0 || idx >= r->Int) {
					scrErrorf("Script %s - ARRAY_U8_STORE array overrun - index = %d, max size = %d, pc = %d", GetScriptName(), idx, r->Int, pc);
					if (!r->Int) DumpArrayInfo(r);
					return Fault(pc,"Array overrun", THREAD_ARRAY_OVERFLOW);
				}
				r = r + 1 + idx * LoadImm8;
				r->Any = Popa();
				break;
			}
		case OP_ARRAY_U16_STORE:
			{
				scrValue *r = Popr();	// Base address of array
				int idx = Popi();			// Array index
				if (idx < 0 || idx >= r->Int) {
					scrErrorf("Script %s - ARRAY_U16_STORE array overrun - index = %d, max size = %d, pc = %d", GetScriptName(), idx, r->Int, pc);
					if (!r->Int) DumpArrayInfo(r);
					return Fault(pc,"Array overrun", THREAD_ARRAY_OVERFLOW);
				}
				r = r + 1 + idx * LoadImm16;
				r->Any = Popa();
				break;
			}

		case OP_SWITCH:
			{
				int label = Popi();
				u32 count = LoadImm8;
				for (u32 i=0; i<count; i++) {
					s32 match = LoadImm32;
					s32 target = LoadImmS16;
					if (label == match) {
						pc += target;
						scrAssertf((pc>=0) && (pc<=MAX_LEGIT_OPS), "Script thread has got a broken PC (%d) [broken during thread operation OP_SWITCH - incorrect label]", pc);
						break;
					}
				}
				break;
			}

		case OP_STRING:
			{
				if ((unsigned)Topi() >= pProgram->StringHeapSize)
					Fault(pc,"Invalid string offset", THREAD_UNKNOWN_ERROR_TYPE);
				int offset = Popi();
				Pushr((scrValue*)(pProgram->StringHeaps[offset >> scrStringShift] + (offset & scrStringMask)));
				break;
			}

		case OP_TEXT_LABEL_ASSIGN_STRING:
			{
				char *dest = (char*) Popr();
				const char *src = (const char*) Popr();
				int maxCopy = LoadImm8;
#if __ASSERT
				const char *pStartOfSourceString = src;
				const char *pStartOfDestString = dest;
				bool bAssert = false;
#endif
				if (src) {
					while (*src && --maxCopy)
						*dest++ = *src++;
#if __ASSERT
					if (*src)
					{	//	If the source string is longer than the maximum length of the destination then assert
						bAssert = true;
					}
#endif
				}
				*dest = '\0';
				scrAssertf(!bAssert, "OP_TEXT_LABEL_ASSIGN_STRING source string is too long for the declared text label. Source = %s Destination = %s (%s %d)", pStartOfSourceString, pStartOfDestString, GetScriptName(), pc);
				break;
			}

		case OP_TEXT_LABEL_COPY:
			{
				scrValue *dest = Popr();
				int destSize = Popi();
				int srcSize = Popi();
				// Remove excess
				while (srcSize > destSize) {
					--srcSize;
					Popi();
				}
				// Do the bulk of the copy
				for (int i=0; i<srcSize; i++)
					dest[srcSize-1-i].Any = Popa();
				// Make sure it's still NUL-terminated
				char *cDest = (char*)dest;
				scrAssertf(istrlen(cDest) < (srcSize * sizeof(scrValue)), "OP_TEXT_LABEL_COPY source string is too long for the declared text label (%d max) - text label contains %s (%s %d)", srcSize * (int)sizeof(scrValue) - 1,cDest, GetScriptName(), pc);
				cDest[(srcSize*sizeof(scrValue))-1] = '\0';
				break;
			}

		case OP_TEXT_LABEL_ASSIGN_INT:
			{
				char *dest = (char*) Popr();
				int i = Popi();
				scrTextLabel buf;
				formatf(buf,sizeof(buf),"%d",i);
				const char *src = buf;
				int maxCopy = LoadImm8;
				scrAssertf(strlen(buf) < (size_t) maxCopy, "OP_TEXT_LABEL_ASSIGN_INT text label is not long enough to contain this number %d (%s %d)", i, GetScriptName(), pc);
				while (*src && --maxCopy)
					*dest++ = *src++;
				*dest = '\0';
				break;
			}

		case OP_TEXT_LABEL_APPEND_STRING:
			{
				char *dest = (char*) Popr();
				const char *src = (const char*) Popr();
				int maxCopy = LoadImm8;
#if __ASSERT
				const char *pStartOfSourceString = src;
				const char *pStartOfDestString = dest;
#endif
				if (src) {
					while (*dest && --maxCopy)
						++dest;
					while (*src && --maxCopy)
						*dest++ = *src++;
					*dest = '\0';
					scrAssertf(*src == 0, "OP_TEXT_LABEL_APPEND_STRING source string is too long for the declared text label. Source = %s Destination = %s (%s %d)", pStartOfSourceString, pStartOfDestString, GetScriptName(), pc);
				}
				break;
			}

		case OP_TEXT_LABEL_APPEND_INT:
			{
				char *dest = (char*) Popr();
				int i = Popi();
				scrTextLabel buf;
				formatf(buf,sizeof(buf),"%d",i);
				const char *src = buf;
#if __ASSERT
				const char *pStartOfSourceString = src;
				const char *pStartOfDestString = dest;
#endif
				int maxCopy = LoadImm8;
				while (*dest && --maxCopy)
					++dest;
				while (*src && --maxCopy)
					*dest++ = *src++;
				*dest = '\0';
				scrAssertf(*src == 0, "OP_TEXT_LABEL_APPEND_INT source string is too long for the declared text label. Source = %s Destination = %s (%s %d)", pStartOfSourceString, pStartOfDestString, GetScriptName(), pc);
				break;
			}

		case OP_CATCH:
			{
				m_Serialized.m_CatchPC = pc;
				m_Serialized.m_CatchFP = m_Serialized.m_FP;
				StoreSp();
				m_Serialized.m_CatchSP = m_Serialized.m_SP;
				Pushi(-1);
				break;
			}

		case OP_THROW:
			{
				int caught = Popi();
				if (m_Serialized.m_CatchPC == 0)
				{
					return Fault(pc,"THROW with no CATCH", THREAD_UNKNOWN_ERROR_TYPE);
				}
				else {
					pc = m_Serialized.m_CatchPC;
					m_Serialized.m_FP = m_Serialized.m_CatchFP;
					fp = &m_Stack[m_Serialized.m_FP];
					m_Serialized.m_SP = m_Serialized.m_CatchSP;
					LoadSp();
					Pushi(caught);
				}
				break;
			}

		case OP_NOP:
			break;

		case OP_IADD_U8:
			Topi() += LoadImm8;
			break;

		case OP_IADD_S16:
			Topi() += LoadImmS16;
			break;
		case OP_IMUL_U8:
			Topi() *= LoadImm8;
			break;
		case OP_IMUL_S16:
			Topi() *= LoadImmS16;
			break;
		case OP_LOCAL_U8_LOAD: 
			Pusha(fp[LoadImm8].Any);
			break;
		case OP_LOCAL_U16_LOAD:;
			Pusha(fp[LoadImm16].Any); 
			break;
		case OP_STATIC_U8_LOAD: 
			Pusha(m_Stack[LoadImm8].Any);
			break;
		case OP_STATIC_U16_LOAD: 
			Pusha(m_Stack[LoadImm16].Any);
			break;
		case OP_STATIC_U24_LOAD:
			Pusha(m_Stack[LoadImm24].Any);
			break;

		case OP_GLOBAL_U16_LOAD: 
			Assert(globals);
			Pusha(globals[0][LoadImm16].Any);
			break;
		case OP_GLOBAL_U24_LOAD: 
			{
				u32 imm = LoadImm24, block = imm >> scrProgram::MAX_GLOBAL_BLOCKS_SHIFT, ofs = imm & scrProgram::GLOBAL_SIZE_MASK; 
				if (!globals[block]) 
				{
					formatf(faultString, "Global block %u not resident", block);
					Fault(pc,faultString, THREAD_UNKNOWN_ERROR_TYPE); 
				}
				Pusha(globals[block][ofs].Any);
				break;
			}
		case OP_LOCAL_U8_STORE:
			fp[LoadImm8].Any = Popa();
			break;
		case OP_LOCAL_U16_STORE:
			fp[LoadImm16].Any = Popa();
			break;
		case OP_STATIC_U8_STORE:
			m_Stack[LoadImm8].Any = Popa();
			break;
		case OP_STATIC_U16_STORE:
			m_Stack[LoadImm16].Any = Popa();
			break;
		case OP_STATIC_U24_STORE:
			m_Stack[LoadImm24].Any = Popa();
			break;

		case OP_GLOBAL_U16_STORE:
			Assert(globals);
			globals[0][LoadImm16].Any = Popa();
			break;
		case OP_GLOBAL_U24_STORE:
			{
				u32 imm = LoadImm24, block = imm >> scrProgram::MAX_GLOBAL_BLOCKS_SHIFT, ofs = imm & scrProgram::GLOBAL_SIZE_MASK; 
				if (!globals[block]) 
				{
					formatf(faultString, "Global block %u not resident", block);
					Fault(pc,faultString, THREAD_UNKNOWN_ERROR_TYPE); 
				}
				globals[block][ofs].Any = Popa();
				break;
			}
		case OP_IEQ_JZ: { int imm = LoadImmS16; int i = Popi(); if (!(Popi() == i)) pc += imm; break; }
		case OP_INE_JZ: { int imm = LoadImmS16; int i = Popi(); if (!(Popi() != i)) pc += imm; break; }
		case OP_IGE_JZ: { int imm = LoadImmS16; int i = Popi(); if (!(Popi() >= i)) pc += imm; break; }
		case OP_IGT_JZ: { int imm = LoadImmS16; int i = Popi(); if (!(Popi() > i)) pc += imm; break; }
		case OP_ILE_JZ: { int imm = LoadImmS16; int i = Popi(); if (!(Popi() <= i)) pc += imm; break; }
		case OP_ILT_JZ: { int imm = LoadImmS16; int i = Popi(); if (!(Popi() < i)) pc += imm; break; }
		case OP_PUSH_CONST_U8_U8:
			Pushi(LoadImm8);
			Pushi(LoadImm8);
			break;
		case OP_PUSH_CONST_U8_U8_U8:
			Pushi(LoadImm8);
			Pushi(LoadImm8);
			Pushi(LoadImm8);
			break;
		case OP_PUSH_CONST_M1: Pushi(-1); break;
		case OP_PUSH_CONST_0: Pusha(0); break;
		case OP_PUSH_CONST_1: Pushi(1); break;
		case OP_PUSH_CONST_2: Pushi(2); break;
		case OP_PUSH_CONST_3: Pushi(3); break;
		case OP_PUSH_CONST_4: Pushi(4); break;
		case OP_PUSH_CONST_5: Pushi(5); break;
		case OP_PUSH_CONST_6: Pushi(6); break;
		case OP_PUSH_CONST_7: Pushi(7); break;

		case OP_PUSH_CONST_FM1: Pushf(-1.0f); break;
		case OP_PUSH_CONST_F0: Pushf(0.0f); break;
		case OP_PUSH_CONST_F1: Pushf(1.0f); break;
		case OP_PUSH_CONST_F2: Pushf(2.0f); break;
		case OP_PUSH_CONST_F3: Pushf(3.0f); break;
		case OP_PUSH_CONST_F4: Pushf(4.0f); break;
		case OP_PUSH_CONST_F5: Pushf(5.0f); break;
		case OP_PUSH_CONST_F6: Pushf(6.0f); break;
		case OP_PUSH_CONST_F7: Pushf(7.0f); break;

		case OP_STRINGHASH: { Topi() = atStringHash((const char*)Topr()); break; }

		case OP_IOFFSET: { int i = Popi(); Topa() += i * sizeof(scrValue); break; }
		case OP_IOFFSET_U8:
			Topa() += LoadImm8 * sizeof(scrValue);
			break;

		case OP_IOFFSET_U8_LOAD:
			{
				scrValue *imm = Popr();
				imm += LoadImm8;
				Pusha(imm->Any);
				break;
			}
		case OP_IOFFSET_U8_STORE:
			{
				scrValue *imm = Popr();
				imm += LoadImm8;
				imm->Any = Popa();
				break;
			}
		case OP_IOFFSET_S16:
			Topa() += LoadImmS16 * sizeof(scrValue);
			break;
		case OP_IOFFSET_S16_LOAD:
			{
				scrValue *imm = Popr();
				imm += LoadImmS16;
				Pusha(imm->Any);
				break;
			}
		case OP_IOFFSET_S16_STORE:
			{
				scrValue *imm = Popr();
				imm += LoadImmS16;
				imm->Any = Popa();
				break;
			}

		case OP_IS_BIT_SET:
			{
				int i = Popi();
				scrAssertf((i >= 0) && (i <= 31), "IS_BIT_SET - bit must be between 0 to 31");
				Topi() = ((Topi() & (1 << i)) != 0);
				break;
			}

		default:
			// scrErrorf("Faulting opcode = %d(%s)",opcode,opnames[opcode]);
			//Fault(pc,"Invalid opcode");
			Quitf("Invalid opcode. PC = 0x%x", pc);
			break;
		}
		
#if SCRIPT_DEBUGGING && !__WIN32PC	// so script compiler tests don't get spammed
		if (insnCount < closeToReachingInstructionLimit)
		{	//	Help to identify infinite loops in scripts by displaying the PC for each instruction as the instruction limit is approached
			scrDisplayf("Script %s is approaching instruction allocation for frame - Program Counter is %d", GetScriptName(), pc);
		}

		scrAssertf((pc>=0) && (pc<=MAX_LEGIT_OPS), "Script thread has got a broken PC (%d) [broken during thread operation OP_SWITCH]", pc);
#endif

#if !__FINAL
		ms_scriptArraySizeMonitor.CheckIfMonitoredAddressHasChanged("After executing script opcode ", opcode);
#endif	//	!__FINAL

	}

#if GOTO_HACK
UNDISPATCH:
#undef break
#endif

#if SCRIPT_DEBUGGING
	if (!insnCount)
		Fault(pc,"Ran over per-script instruction allocation for frame", THREAD_INSTRUCTION_COUNT_OVERFLOW);

	if (runTimer.GetMsTime() > 1.0f)
		scrWarningf("Script '%s' took %5.2fms to execute.",GetScriptName(),runTimer.GetMsTime());
#endif

	StoreSp();
	m_Serialized.m_PC = pc;

	return RUNNING;
#endif	// !ASM_SCRIPT_CORE
}



void scrThread::Throw(int value) {
	if (s_CurrentThread == this)
		scrErrorf("scrThread::Throw - cannot throw to currently executing script, ignored");
	else if (!m_Serialized.m_CatchPC)
		scrErrorf("scrThread::Throw - cannot throw to script that didn't execute CATCH yet, ignored");
	else {
		m_Serialized.m_PC = m_Serialized.m_CatchPC;
		m_Serialized.m_FP = m_Serialized.m_CatchFP;
		m_Serialized.m_SP = m_Serialized.m_CatchSP;
		_Pushi(value);
		// Clear any blocked execution flag
		SetThreadBlockState(false);
	}
}


#if !__FINAL

void scrThread::SetBreakpoint( int offset, bool setOrUnSet, bool stopsGame )
{
	if (setOrUnSet)
    {
        bool *pVal = m_threadBreakpoints.Access( offset );
        if ( pVal == NULL )
        {
            m_threadBreakpoints[offset] = 0;
            pVal = m_threadBreakpoints.Access( offset );
        }

        *pVal = stopsGame;
    }
	else
    {
        m_threadBreakpoints.Delete( offset );
    }
}

bool scrThread::HasBreakpoint( int offset, bool *stopsGame )
{
    bool *pVal = m_threadBreakpoints.Access( offset );

    if ( stopsGame != NULL )
{
        *stopsGame = (pVal != NULL) && *pVal;
    }
    
    return pVal != NULL;
}

void scrThread::Continue() {
	if (m_Serialized.m_State == HALTED)
		m_Serialized.m_State = RUNNING;
	else
		scrWarningf("scrThread - continued when not stopped");
}


void scrThread::Pause() {
	if (m_Serialized.m_State == RUNNING || m_Serialized.m_State == BLOCKED)
		m_Serialized.m_State = HALTED;
	else
		scrWarningf("scrThread - pausing when not running");
}


void scrThread::SetStepRegion(int minPc,int maxPc) {
	if (m_Serialized.m_State == HALTED) {
		m_Serialized.m_MinPC = minPc;
		m_Serialized.m_MaxPC = maxPc;
		m_Serialized.m_State = RUNNING;
	}
	else
		scrWarningf("scrThread - stepping when not stopped");
}

#endif		// !__FINAL

// #pragma optimize("",off)

int scrThread::GetProgramCounter(int level) const {
	scrAssertf(level >= 0, "scrThread::GetProgramCounter must be given a non-negative level. Now we have an infinite loop!");
	int pc = m_Serialized.m_PC;
	int sp = m_Serialized.m_SP;
	int fp = m_Serialized.m_FP;
	scrProgram *p = scrProgram::GetProgram(m_Serialized.m_Prog);
	const scrProgram& opcodes = *p;
	int pSize = p->GetOpcodeSize();

	while (level) {
		if (pc < 0 || pc >= pSize) {
			scrWarningf("GetProgramCounter - Invalid program counter during stack walk");
			return -1;
		}

		// Scan for the next OP_LEAVE instruction, but watch out for nested functions.
		while (opcodes[pc] != OP_LEAVE) {
			// Removed a bunch of logic here, we no longer do nested functions and we don't have to worry about page breaks.
			if (opcodes[pc] < OP_FIRST_INVALID)
				pc += GetInstructionSize(opcodes,pc);
			else {
				scrWarningf("GetProgramCounter - crap insn, level = %d, pc = %d",level,pc);
					return -1;
			}
			if (pc >= pSize) {
				scrWarningf("GetProgramCounter - pc went off end during OP_ENTER scan");
				return -1;
			}
		}

		if (pc + 2 >= pSize) {
			scrWarningf("GetProgramCounter - Truncated OP_LEAVE instruction?");
			return -1;
		}
		int paramCount = opcodes[++pc];	// Get number of parameters to clean off the stack
		int returnSize = opcodes[++pc];	// Get size of the return value

		sp = fp + paramCount + 2;	// Restore stack pointer, killing off all local variables
		if (sp > m_Serialized.m_StackSize || sp < 2) {
			scrWarningf("GetProgramCounter - Stack pointer from frame pointer invalid");
			return -1;
		}
		fp = m_Stack[--sp].Int;		// Get back previous frame pointer OP_ENTER preserved for us
		pc = m_Stack[--sp].Int;		// Pull return address off of the stack placed by OP_CALL
		sp += returnSize;
		sp -= paramCount;			// Drop the caller's parameters.
		if (sp > m_Serialized.m_StackSize || sp < 0) {
			scrWarningf("GetProgramCounter - Stack pointer after cleanup invalid");
			return -1;
		}
		if (fp > m_Serialized.m_StackSize || fp < 0) {
			scrWarningf("GetProgramCounter - Frame pointer invalid");
			return -1;
		}
		if (pc >= pSize) {
			scrWarningf("GetProgramCounter - Saved prog counter on stack invalid");
			return -1;
		}

		--level;
		if (!pc)
			return -1;
	}
	return pc;
}


bool scrThread::IsValidReference(scrValue *ref) {
	bool result = (ref >= &m_Stack[0] && ref <= &m_Stack[m_Serialized.m_StackSize-1]);
	scrValue **globals = scrProgram::GetGlobalsBlocks();
	for (int i=0; !result && i<scrProgram::MAX_GLOBAL_BLOCKS; i++)
		if (globals[i] && ref >= globals[i] && ref < globals[i] + scrProgram::GetGlobalSize(i))
			result = true;
	scrAssertf(result, "Unable to determine if we have a valid reference.");
	return result;
}

void scrThread::DumpArrayInfo(scrValue *OUTPUT_ONLY(ref)) {
	scrErrorf("Zero-length array caught; this is usually caused by global variables being out of date across different scripts or your executable.  Try running script\\dev\\Clear Debug Script Files.bat and doing a full rebuild, making sure that your script code is on the same p4 label as your executable.");
	scrErrorf("Array is at %p, IsValidReference is %d",ref,IsValidReference(ref));
}


scrValue* scrThread::GetLocal(int offset) {
	offset += m_Serialized.m_FP;
	if (offset >= 0 && offset < m_Serialized.m_SP)
		return &m_Stack[offset];
	else
		return NULL;
}

scrValue* scrThread::GetStatic(int offset) {
	return &m_Stack[offset];
}

bool scrThread::IsValidStatic(scrValue *ref)
{
    scrProgram *p = scrProgram::GetProgram(m_Serialized.m_Prog);
    if ( !scrVerifyf( p, "Unable to retrieve the program for id %d.", m_Serialized.m_Prog) )
		return false;

	return ((ref >= &m_Stack[0]) && (ref < &m_Stack[p->GetStaticSize()]));
}

bool scrThread::IsValidGlobal(scrValue *ref)
{
	scrValue **globals = scrProgram::GetGlobalsBlocks();
	for (int i=0; i<scrProgram::MAX_GLOBAL_BLOCKS; i++)
		if (globals[i] && ref >= globals[i] && ref < globals[i] + scrProgram::GetGlobalSize(i))
			return true;
	return false;
}



scrValue* scrThread::GetGlobal(int offset) {
	return &scrProgram::GetGlobalsBlocks()[offset >> scrProgram::MAX_GLOBAL_BLOCKS_SHIFT][offset & scrProgram::GLOBAL_SIZE_MASK];
}

static void InvalidNative(scrThread::Info &)
{
#if !__FINAL
	scrThread::PrePrintStackTrace();
#endif
	Quitf(ERR_DEFAULT,"Attempted to execute missing NATIVE function.  You should have seen errors when this script was loaded.");
}


bool scrThread::Validate(scrProgram &opcodes) {
	PROFILE
	bool result = true;

	for (u32 n=0; n<opcodes.NativeSize; n++) {
		scrCmd cmd = s_CommandHash.Lookup(opcodes.Natives[n]);
		if (!cmd) {
			cmd = InvalidNative;
#if __FINAL && __PS3 && (!defined(__MASTER) || !__MASTER)
			printf("scrThread::Validate - NATIVE definition for %x is missing in '%s'\n",opcodes.Natives[n],opcodes.GetScriptName());
#else
			Errorf("scrThread::Validate - NATIVE definition for %" I64FMT "x is missing in '%s'",opcodes.Natives[n],opcodes.GetScriptName());
#endif
			result = false;
		}
		opcodes.Natives[n] = (size_t) cmd;
	}


#if SCRIPT_PROFILING
	if (sm_EnableScriptProfiling) {
		opcodes.ProcNames = nullptr;
		for (int pass=1; pass<=2; pass++) {
			u32 pc = 0, opcodeSize = opcodes.OpcodeSize;
			opcodes.ProcCount = 0;
			while (pc < opcodeSize) {
				if (opcodes[pc] == OP_ENTER && opcodes[pc+4]) {		// Don't choke on release scripts
					if (pass == 2) {
						// opcode, paramCount(u8), localCount(u16), nameLen(u8), profStub, profStub, name chars....
						opcodes.ProcNames[opcodes.ProcCount] = (const char*)&opcodes[pc+7];
						*(u16*)&opcodes[pc+5] = (u16)opcodes.ProcCount;
					}
					opcodes.ProcCount++;
				}
				pc += GetInstructionSize(opcodes,pc);
			}
			if (pass == 1) {
				if (opcodes.ProcCount) {
					opcodes.ProcNames = rage_new const char*[opcodes.ProcCount];
				}
			}
		}
	}
#endif	// SCRIPT_PROFILING
#if !__FINAL
	if (!result)
		fiRemoteShowMessageBox("One or more NATIVE commands are missing (may be incorrectly marked optional), see TTY for details; please include the 'NATIVE definition for ######## is missing' spew.","REPORT THIS TO CODERS!",MB_ICONERROR|MB_OK,IDOK);
#endif
	return result;
}

#if (USE_PROFILER_NORMAL || SCRIPT_PROFILING)
bool scrThread::GenerateProcNames(const scrProgram &_opcodes) {
	if (_opcodes.ProcNames != nullptr)
		return _opcodes.ProcCount > 0;

	//vile
	scrProgram& opcodes = const_cast<scrProgram&>(_opcodes);

	PROFILER_CATEGORY("scrThread::GenerateProcNames", Profiler::CategoryColor::Debug);

	delete[] opcodes.ProcNames;
	opcodes.ProcNames = 0;

	const int namesCapacity = 512;
	rage::atArray<const char*> names(0, namesCapacity);

	u32 pc = 0, opcodeSize = opcodes.OpcodeSize;
	while (pc < opcodeSize) {
		if (opcodes[pc] == OP_ENTER && opcodes[pc + 4]) {		// Don't choke on release scripts
			// opcode, paramCount(u8), localCount(u16), nameLen(u8), profStub, profStub, name chars....
			u16 index = (u16)names.GetCount();
			*(u16*)&opcodes[pc + 5] = index;
			names.PushAndGrow((const char*)&opcodes[pc + 7], namesCapacity);
		}
		pc += GetInstructionSize(opcodes, pc);
	}

	opcodes.ProcCount = (u32)names.GetCount();

	if (opcodes.ProcCount) {
		opcodes.ProcNames = rage_new const char*[opcodes.ProcCount];
		sysMemCpy(opcodes.ProcNames, names.GetElements(), opcodes.ProcCount * sizeof(char*));
	}

	return opcodes.ProcCount > 0;
}
#endif



#if USE_PROFILER_NORMAL
bool scrThread::GenerateProfilerEvents(const scrProgram &program) {
	return g_eventDescriptions.GetOrCreateStorage(program) != nullptr;
}
#endif

void scrThread::SwapForPlatform(scrProgram &
#if !__BE
								opcodes
#endif
								) {
#if !__BE
	// On little-endian machines, we'll need to byte-swap all the immediate operands
	u32 pc = 0, opcodeSize = opcodes.OpcodeSize;
	while (pc != opcodeSize) {
		int op = opcodes[pc];
		if (op == OP_PUSH_CONST_U32 || op == OP_PUSH_CONST_F) {
			u8 imm1 = opcodes[pc+1];
			u8 imm2 = opcodes[pc+2];
			u8 imm3 = opcodes[pc+3];
			u8 imm4 = opcodes[pc+4];
			opcodes[pc+1] = imm4;
			opcodes[pc+2] = imm3;
			opcodes[pc+3] = imm2;
			opcodes[pc+4] = imm1;
		}
		else if (op >= OP_CALL && op <= OP_PUSH_CONST_U24) {
			u8 imm1 = opcodes[pc+1];
			u8 imm3 = opcodes[pc+3];
			opcodes[pc+1] = imm3;
			opcodes[pc+3] = imm1;
		}
		else if (op == OP_ENTER) {
			u8 imm2 = opcodes[pc+2];
			u8 imm3 = opcodes[pc+3];
			opcodes[pc+2] = imm3;
			opcodes[pc+3] = imm2;
		}
		else if (op >= OP_PUSH_CONST_S16 && op <= OP_ILE_JZ) {
			u8 imm1 = opcodes[pc+1];
			u8 imm2 = opcodes[pc+2];
			opcodes[pc+1] = imm2;
			opcodes[pc+2] = imm1;
		}
		else if (op == OP_SWITCH) {
			int stop = pc+2+opcodes[pc+1]*6;
			for (int i=pc+2; i<stop; i+=6) {
				u8 imm0 = opcodes[i+0];
				u8 imm1 = opcodes[i+1];
				u8 imm2 = opcodes[i+2];
				u8 imm3 = opcodes[i+3];
				opcodes[i+0] = imm3;
				opcodes[i+1] = imm2;
				opcodes[i+2] = imm1;
				opcodes[i+3] = imm0;
				imm0 = opcodes[i+4];
				imm1 = opcodes[i+5];
				opcodes[i+4] = imm1;
				opcodes[i+5] = imm0;
			}
		}
		Assertf((pc >> scrPageShift) == ((pc + GetInstructionSize(opcodes,pc) - 1) >> scrPageShift),"Insn at %x (opcode %d) crosses page boundary",pc,op);
		pc += GetInstructionSize(opcodes,pc);
	}
#endif
}

int scrThread::GetInstructionSize(const scrProgram &opcode,int pc) {
	static u8 sizes[256] = {
#define OP(a,b,c) b
#include "opcodes.h"
#undef OP
	};
	int result = sizes[opcode[pc]];
	if (!result) switch (opcode[pc]) {
		// Weird cases:
		case OP_ENTER: 
			return 5 + opcode[pc+4];
		case OP_SWITCH:
			return 2+6*opcode[pc+1];
		default:
			Quitf(ERR_DEFAULT,"invalid insn (%d) in GetInstructionSize",opcode[pc]);
			return -1;
	}
	return result;
}

scrThread::State scrThread::Update(int insnCount) {
	if (sm_IsBlocked)
	{
		return BLOCKED;
	}

	const float fElapsed = (sm_TimeStep * 1000.0f);
	if (!sm_IsPaused || sm_UpdateTimersWhenPaused)
	{
		m_Serialized.m_TimerA += fElapsed;
		m_Serialized.m_TimerB += fElapsed;
	}

#if __ASSERT && 0
	Functor0Ret<bool> oldStackTraceFunc = g_PrintStackTraceFunc;
	Functor0Ret<bool> stackTraceFunc = MakeFunctorRet( *this, &scrThread::PrintStackTrace );
	g_PrintStackTraceFunc = stackTraceFunc;
#endif

#if SCRIPT_DEBUGGING
	utimer_t start = sysTimer::GetTicks();
#endif

#if EXECUTABLE_SCRIPTS
	scrProgram *prog = scrProgram::GetProgram(m_Serialized.m_Prog);
	scrAssertf(prog, "scrThread::Update - GetProgram failed for %s", m_ScriptName);
	scrThread::State ret = prog->IsCompiled()? Exec(prog,(unsigned)__builtin_frame_address()) : Run(insnCount);
#else
	scrThread::State ret = Run(insnCount);
#endif

#if SCRIPT_DEBUGGING
	utimer_t delta = sysTimer::GetTicks() - start;
	m_fUpdateTime += delta * sysTimerConsts::TicksToSeconds;

	const int timeLimit = 1000;
	float fScriptTimeMS = delta * sysTimerConsts::TicksToMilliseconds;
	if( fScriptTimeMS > timeLimit )
	{
		Displayf("Script %s took longer than %ims (%ims) - Stack Trace Follows", GetScriptName(), timeLimit, (int)fScriptTimeMS);
		// Dump the script stack
		PrintStackTrace();
	}
#endif

#if SCRIPT_PROFILING
	if (sm_EnableScriptProfiling && sm_ProfileResetInterval) {
		if (m_ProfUpdateCount >= sm_ProfileResetInterval)
			ResetProfileData();
	}
#endif
	
#if __ASSERT && 0
	g_PrintStackTraceFunc = oldStackTraceFunc;
#endif

	return ret;
}


bool scrThread::UpdateAll(int insnCount) {
	PF_AUTO_PUSH_TIMEBAR("Update All Scripts"); 
#if __PROFILE && __PPU
	snPushMarker("Update All Scripts");
#endif
	bool result = false;
	if (insnCount == 0)
		insnCount = DEFAULT_INSTRUCTION_COUNT;
	for (int prio=THREAD_PRIO_HIGHEST; prio<=THREAD_PRIO_LOWEST; prio++) {
		for (int i=0; i<sm_Threads.GetCount(); i++) {
			if (sm_Threads[i]->m_Serialized.m_Priority != prio)
				continue;
#if SCRIPT_DEBUGGING
			sm_Threads[i]->ResetTimer();
#endif
			if (sm_Threads[i]->m_Serialized.m_ThreadId)
			{
				PF_START_TIMEBAR_BUDGETED(sm_Threads[i]->GetScriptName(), 5.0f);
#if __PROFILE && __PPU
				snPushMarker(sm_Threads[i]->GetScriptName());
#endif
			}

			if (sm_Threads[i]->m_Serialized.m_ThreadId && sm_Threads[i]->Update(insnCount) != ABORTED)
				result = true;

#if SCRIPT_DEBUGGING
			if(sm_Threads[i]->GetUpdateTime() > 10.0f)
			{
				Displayf("** POSSIBLE SCRIPT STALL: Script %s update took %f seconds",
					sm_Threads[i]->GetScriptName(), sm_Threads[i]->GetUpdateTime());
			}
#endif

#if __PROFILE && __PPU
			if (sm_Threads[i]->m_Serialized.m_ThreadId) 
				snPopMarker();
#endif
		}
	}

#if __PROFILE && __PPU
	snPopMarker();
#endif
	return result;
}


void scrThread::AddThreadStack(int count,int stackSize) {
	while (count--) {
		ThreadStack &ts = sm_Stacks.Append();
		ts.m_Owner = NULL;
		ts.m_StackSize = stackSize;
		ts.m_Stack = rage_new scrValue[stackSize];
	}
}


void scrThread::AllocateThreads(int count,int stackSize) {
	sm_Threads.Resize(count);
	sm_Stacks.Reserve(count*2);
	for (int i=0; i<count; i++)
		sm_Threads[i] = rage_new scrThread;
	if (stackSize)
		AddThreadStack(count,stackSize);
}


#if !__FINAL
static void scrThread__PrintLine(const char* fmt, ...)
{
	va_list args;
	va_start(args,fmt);
	char buf[256];
	vformatf(buf,sizeof(buf),fmt,args);
	diagLoggedPrintf(TPurple "%s" TNorm "\n", buf);
	va_end(args);
}

static void scrThread_ExceptionDisplayStackLine(size_t addr,const char *sym,size_t offset)
{
	if (strstr(sym,"scrThread::Run") && s_CurrentThread)
	{
		diagLoggedPrintf(TPurple ">> BEGIN SCRIPT TRACEBACK" TNorm "\n");
		s_CurrentThread->PrintStackTrace(scrThread__PrintLine);
		diagLoggedPrintf(TPurple ">> END SCRIPT TRACEBACK" TNorm "\n");
	}
	sysStack::DefaultDisplayLine(addr, sym, offset);
}
#endif

#if SCRVALUE_OFFSETS
size_t s_HeapBase;
size_t s_CodeBase;
extern "C" int main();
const size_t s_NonHeapOffset = 64 * 1024 * 1024;
const size_t s_CodeMaxSize = 64 * 1024 * 1024;	// This can be adjusted...
// The virtual heap size is effectively limited to 3968M now because the first two 64M "segments" are special.

unsigned scrThread::EncodePointer(void *p_)
{
	size_t p = (size_t) p_;
	if (p < s_NonHeapOffset)
		return (unsigned) p;
	else if (p > s_CodeBase && p < s_CodeBase + s_CodeMaxSize)
		return (unsigned) (p - s_CodeBase + s_NonHeapOffset);
	size_t o = (size_t)p - s_HeapBase;
	TrapLT(o,s_NonHeapOffset + s_CodeMaxSize);
	TrapGT(o,(size_t)0xFFFFFFFF);
	return (unsigned) o;
}

void* scrThread::DecodePointer(unsigned u)
{
	if (u < s_NonHeapOffset)
		return (void*) (size_t) u;
	else if (u < s_NonHeapOffset + s_CodeMaxSize)
		return (void*) (u + s_CodeBase - s_NonHeapOffset);
	else
		return (void*)(s_HeapBase + u);
}
#endif

void scrThread::InitClass() 
{
#if SCRVALUE_OFFSETS
	// Our arena nodes are at least 16 bytes so a legal pointer cannot actually be at the heap base
	s_HeapBase = (size_t) sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetHeapBase() - s_NonHeapOffset - s_CodeMaxSize;
	s_CodeBase = (size_t) main;
#endif

#if SCRIPT_PROFILING
	sm_EnableScriptProfiling = PARAM_scriptprofiling.Get();
	if (sm_EnableScriptProfiling)
		PARAM_scriptprofiling.Get(sm_ProfileResetInterval);
#endif
    rageScriptDebug::Init();

	s_CommandHash.Init();

#if USE_PROFILER
	Profiler::RegisterResolveScriptSymbolCallback(ResolveScriptAddress);
	Profiler::RegisterCaptureControlCallback(InitCallstackAutoSamplingVars);
#endif

#if !__FINAL
	scrThread::OriginalPrePrintStackTrace = sysStack::PrePrintStackTrace;
	sysStack::PrePrintStackTrace = scrThread::PrePrintStackTrace;
#if EXCEPTION_HANDLING
	sysException::ExceptionDisplayStackLine = scrThread_ExceptionDisplayStackLine;
#endif
#endif
}


void scrThread::ShutdownClass() 
{
#if !__FINAL
    sysStack::PrePrintStackTrace = scrThread::OriginalPrePrintStackTrace;
#endif // !__FINAL

	s_CommandHash.Kill();
	for (int i=0; i<sm_Threads.GetCount(); i++)
		delete sm_Threads[i];
	for (int i=0; i<sm_Stacks.GetCount(); i++)
		delete sm_Stacks[i].m_Stack;
	sm_Threads.Reset();
	sm_Stacks.Reset();
}


#if !__FINAL
PARAM(testunusednatives,"Omit unused commands for testing (only makes sense with release scripts, use -override_script=script_rel)");

void scrThread::RegisterUnusedCommand(u64 hashCode,void (*handler)(scrThread::Info&) SCRIPT_DEBUGGING_ONLY(, const char* name, const char* file, scrSignature sig)) {
	if (PARAM_testunusednatives.Get())
		return;
	// Work correctly whether the hashcodes came from atStringHash or scrComputeHash.
	// The odds are against this ever mattering but we might as well get it correct.
	if (hashCode < 2)
		hashCode += 2;
	s_CommandHash.Insert(hashCode,handler);

#if SCRIPT_DEBUGGING
	USE_DEBUG_MEMORY();

	s_DebugCommandNameMap.Insert(name, handler);

	const char* filename = strrchr(file, '/');
	if(!filename)
	{
		filename = file;
	}
	else
	{
		++filename;
	}
	s_LogNativeCalls.Insert((void*)handler,NativeCallLogInfo(handler, filename, name));
	u32 scrHashCode = scrComputeHash(name);
	scrAssertf(!s_SignatureHash.Has(scrHashCode), "Existing native command collided with '%x', or hash table is full (raise scrThread::InitClass value).",scrHashCode);
	s_SignatureHash.InsertSorted(scrHashCode, scrSignatureAndCmd(sig, handler));
#endif
}
#endif

void scrThread::RegisterCommand(u64 hashCode,void (*handler)(scrThread::Info&) SCRIPT_DEBUGGING_ONLY(, const char* name, const char* file, scrSignature sig)) {
	PROFILE

	s_CommandHash.Insert(hashCode,handler);

#if SCRIPT_DEBUGGING
	USE_DEBUG_MEMORY();

	s_DebugCommandNameMap.Insert(name, handler);

	const char* filename = strrchr(file, '/');
	if(!filename)
	{
		filename = file;
	}
	else
	{
		++filename;
	}
	s_LogNativeCalls.Insert((void*)handler,NativeCallLogInfo(handler, filename, name));
	u32 scrHashCode = scrComputeHash(name);
	scrAssertf(!s_SignatureHash.Has(scrHashCode), "Existing native command collided with '%x', or hash table is full (raise scrThread::InitClass value).",scrHashCode);
	s_SignatureHash.InsertSorted(scrHashCode, scrSignatureAndCmd(sig, handler));
#endif
}


scrSignature scrThread::LookupCommandSignature(u32 hashCode)
{
	scrSignatureAndCmd* sig = s_SignatureHash.SafeGet(hashCode);
	return sig ? sig->sig : scrSignature();
}

#if SCRIPT_DEBUGGING
void* scrThread::GetCmdFromName(const char *cmdName) {
	u32 hashCode = scrComputeHash(cmdName);
	return (void*)s_CommandHash.Lookup(hashCode);
}
#endif

int scrThread::TimerA() {
	return (int) scrThread::GetActiveThread()->m_Serialized.m_TimerA;
}


int scrThread::TimerB() {
	return (int) scrThread::GetActiveThread()->m_Serialized.m_TimerB;
}


void scrThread::SetTimerA(int i) {
	scrThread::GetActiveThread()->m_Serialized.m_TimerA = (float) i;
}


void scrThread::SetTimerB(int i) {
	scrThread::GetActiveThread()->m_Serialized.m_TimerB = (float) i;
}


float scrThread::TimeStep() {
	return scrThread::GetTimeStep();
}

float scrThread::TimeStepUnwarped() {
	return scrThread::GetTimeStepUnwarped();
}

void scrThread::Wait(scrThread::Info &info) {
	// Were we already waiting?
	scrThread &thread = *scrThread::GetActiveThread();
	if (thread.GetState() == BLOCKED) {
		thread.m_Serialized.m_Wait -= sm_TimeStep;
		if (thread.m_Serialized.m_Wait <= 0.0f) {
			thread.m_Serialized.m_Wait = 0.0f;
			thread.SetThreadBlockState(false);
		}
	}
	else {
		thread.m_Serialized.m_Wait = info.Params[0].Int * (1.0f / 1000);
		thread.SetThreadBlockState(true);
		COMPILED_SCRIPT_YIELD;
	}
}

void scrThread::WaitUnwarped(scrThread::Info &info) {
	// Were we already waiting?
	scrThread &thread = *scrThread::GetActiveThread();
	if (thread.GetState() == BLOCKED) {
		thread.m_Serialized.m_Wait -= sm_TimeStepUnwarped;
		if (thread.m_Serialized.m_Wait <= 0.0f) {
			thread.m_Serialized.m_Wait = 0.0f;
			thread.SetThreadBlockState(false);
		}
	}
	else {
		thread.m_Serialized.m_Wait = info.Params[0].Int * (1.0f / 1000);
		thread.SetThreadBlockState(true);
		COMPILED_SCRIPT_YIELD;
	}
}

 
void scrThread::WaitUnpaused(scrThread::Info &info) {
	// Were we already waiting?
	scrThread &thread = *scrThread::GetActiveThread();
	if (thread.GetState() == BLOCKED) {
		if (!sm_IsPaused)
			thread.m_Serialized.m_Wait -= sm_TimeStepUnwarped;
		if (thread.m_Serialized.m_Wait <= 0.0f) {
			thread.m_Serialized.m_Wait = 0.0f;
			thread.SetThreadBlockState(false);
		}
	}
	else {
		thread.m_Serialized.m_Wait = info.Params[0].Int * (1.0f / 1000);
		thread.SetThreadBlockState(true);
		COMPILED_SCRIPT_YIELD;
	}
}


scrThreadId (*scrThread::StartNewThreadOverride)(const char *,const void*,int,int) = CreateThread;
scrThreadId (*scrThread::StartNewThreadWithNameHashOverride)(atHashValue,const void*,int,int) = CreateThreadWithNameHash;

void scrThread::StartNewScript(scrThread::Info &info) {
	// START_NEW_SCRIPT will block if no new threads are available.
	AssertMsg(info.ParamCount == 2,"You forgot to add stack size parameter");
	scrThreadId id = StartNewThreadOverride(scrDecodeString(info.Params[0].String),NULL,0,info.Params[1].Int? info.Params[1].Int : c_DefaultStackSize);
	
	StartNewScriptInternal(info, id);
}


void scrThread::StartNewScriptWithArgs(scrThread::Info &info) {
	// START_NEW_SCRIPT will block if no new threads are available.
	AssertMsg(info.ParamCount == 4,"You forgot to add stack size parameter");
	scrThreadId id = StartNewThreadOverride(scrDecodeString(info.Params[0].String),scrDecodeReference(info.Params[1].Reference),info.Params[2].Int * sizeof(scrValue),info.Params[3].Int? info.Params[3].Int : c_DefaultStackSize);

	StartNewScriptInternal(info, id);
}

void scrThread::StartNewScriptWithNameHash(Info &info)
{
	// START_NEW_SCRIPT will block if no new threads are available.
	AssertMsg(info.ParamCount == 2,"You forgot to add stack size parameter");
	scrThreadId id = StartNewThreadWithNameHashOverride(atHashValue((u32)info.Params[0].Int), NULL,0,info.Params[1].Int? info.Params[1].Int : c_DefaultStackSize);

	StartNewScriptInternal(info, id);
}

void scrThread::StartNewScriptWithNameHashAndArgs(Info &info)
{
	// START_NEW_SCRIPT will block if no new threads are available.
	AssertMsg(info.ParamCount == 4,"You forgot to add stack size parameter");
	scrThreadId id = StartNewThreadWithNameHashOverride(atHashValue((u32)info.Params[0].Int), scrDecodeReference(info.Params[1].Reference),info.Params[2].Int * sizeof(scrValue),info.Params[3].Int? info.Params[3].Int : c_DefaultStackSize);

	StartNewScriptInternal(info, id);
}


void scrThread::StartNewScriptInternal(scrThread::Info &info, scrThreadId id)
{
	if (!id) {
		if (scrThread::GetActiveThread())
			scrThread::GetActiveThread()->m_Serialized.m_State = BLOCKED;
	}
	else
	{
		info.ResultPtr->Int = (int) id;
		if (scrThread::GetActiveThread()) {
			scrThread::GetActiveThread()->m_Serialized.m_State = RUNNING;

			int tlsSize = scrThread::GetActiveThread()->m_Serialized.m_TLS.GetMaxCount();
			for(int i=0;i<tlsSize;i++)
				if (!scrThread::GetThread(id)->TLS(i).Reference)
					scrThread::GetThread(id)->TLS(i) = scrThread::GetActiveThread()->TLS(i);
		}
	}
}


scrValue scrThread::ExecuteNetworkCommand(u32 commandHash, int numParameters, scrValue *pParameters) {
	scrValue result;
	result.Int = 0;
	scrSignatureAndCmd* sigAndCmd = s_SignatureHash.SafeGet(commandHash);
	scrAssertf(sigAndCmd, "Unable to find the command for hash %d.", commandHash);
	if (sigAndCmd) {
		Info curInfo(&result, numParameters, pParameters);
		(sigAndCmd->cmd)(curInfo);
	}
	return result;
}

void scrThread::Reset(scrProgramId program,const void *argStruct,int argStructSize) { 
	if (program)
		m_Serialized.m_Prog = program;
	m_Serialized.m_PC=0; 
	m_Serialized.m_CallDepth = 0;
	if (!m_Serialized.m_Prog) {
		Fault(0,"Reset: Invalid program id", THREAD_UNKNOWN_ERROR_TYPE);
		return;
	}
	scrProgram *prog = scrProgram::GetProgram(m_Serialized.m_Prog);
	m_ScriptNameHash = prog->GetHashCode();
	safecpy(m_ScriptName, prog->GetScriptName());
	int progArgStructSize = prog->GetArgStructSize();
	char msg[256];
	if (argStructSize && (progArgStructSize*(int)sizeof(scrValue)) != argStructSize) {
		formatf(msg, "Reset: Arg size mismatch, calling code has %u but script expects %u",argStructSize,progArgStructSize*(int)sizeof(scrValue));
		Fault(0,msg, THREAD_UNKNOWN_ERROR_TYPE);
		return;
	}

	m_Serialized.m_State=RUNNING;
	int staticSize = prog->GetStaticSize();
	const scrValue *statics = prog->GetStatics();
	if (staticSize >= m_Serialized.m_StackSize) {
		formatf(msg, "Reset: Too many static variables for program %s\n, need bigger stack (%d > %d)", prog->GetScriptName(), staticSize, m_Serialized.m_StackSize);
		Fault(0,msg, THREAD_UNKNOWN_ERROR_TYPE);
		return;
	}

#if SCRIPT_DEBUGGING
	m_MaxStackUse = 0;
#endif
#if SCRIPT_PROFILING
	m_ProfStackCount = 0;
	m_ProfUpdateCount = 0;
	if (sm_EnableScriptProfiling) {
		m_ProfBuffer.Reset();
		m_ProfBuffer.Resize(prog->ProcCount);
		for (u32 i=0; i<prog->ProcCount; i++) {
			m_ProfBuffer[i].SelfIndex = i;
			m_ProfBuffer[i].Zero();
		}
		m_ProfTotals.Zero();
		m_ProfTotals.SelfIndex = 0;
	}
#endif

	m_argStructSize = progArgStructSize*(int)sizeof(scrValue);
	m_argStructOffset = staticSize-progArgStructSize;

	m_Serialized.m_SP = staticSize;
	for (int i=0; i<staticSize; i++)
		m_Stack[i] = statics[i];
	if (argStructSize)
		memcpy(&m_Stack[m_argStructOffset],argStruct,argStructSize);
	m_Serialized.m_FP = m_Serialized.m_SP; 
	m_Serialized.m_TimerA = 0.0f; 
	m_Serialized.m_TimerB = 0.0f; 
	m_Serialized.m_Wait = 0.0f; 
	m_Serialized.m_CatchPC = 0;
	m_Serialized.m_CatchFP = 0;
	m_Serialized.m_CatchSP = 0;
	m_Serialized.m_Priority = scrThread::THREAD_PRIO_NORMAL;
	_Pushi(0);	// Return address
}


scrThreadId scrThread::CreateThread(const char *progname,const void *argStruct,int argStructSize,int stackSize) {
	scrProgramId progId = scrProgram::Load(progname);
	scrThreadId id = scrThread::CreateThread(progId,argStruct,argStructSize,stackSize);
	// Always release the reference count here because the inner CreateThread AddRef's it on success
	if (progId)
		scrProgram::GetProgram(progId)->Release();
	return id;
}

scrThreadId scrThread::CreateThreadWithNameHash(atHashValue,const void*,int,int)
{
	scrAssertf(0, "scrThread::CreateThreadWithNameHash has not been implemented. Define your own and override with StartNewThreadWithNameHashOverride");

	return THREAD_INVALID;
}

int scrThread::CheckStackAvailability(int stackSize)
{
	int iCount=0;
	for (int i=0; i<sm_Stacks.GetCount(); i++) {
		if (!sm_Stacks[i].m_Owner && sm_Stacks[i].m_StackSize == stackSize) {
			++iCount;
		}
	}
	return iCount;
}

#if !__NO_OUTPUT

void scrThread::PrePrintStackTrace()
{
    if ( OriginalPrePrintStackTrace )
    {
        OriginalPrePrintStackTrace();
    }

    if ( (s_CurrentThread != NULL) && s_ThisThreadIsRunningAScript )
    {
        s_CurrentThread->PrintStackTrace();
    }
}

void (*scrThread::OriginalPrePrintStackTrace)();

#endif // !__FINAL

bool scrThread::AllocateStack(int stackSize) {
	scrAssertf(!m_Stack, "The stack has already been allocated.");
	ASSERT_ONLY(bool gotOneMatch = false);

	// Grab a new stack, possibly from another dead thread.
	// We defer recycling stacks as long as possible so that the debugger can still examine aborted threads.
	if (!m_Serialized.m_StackSize) {
		for (int i=0; i<sm_Stacks.GetCount(); i++) {
#if __ASSERT
			if (sm_Stacks[i].m_StackSize == stackSize)
				gotOneMatch = true;
#endif
			if (!sm_Stacks[i].m_Owner && sm_Stacks[i].m_StackSize == stackSize) {
				m_Serialized.m_StackSize = stackSize;
				m_Stack = sm_Stacks[i].m_Stack;
				sm_Stacks[i].m_Owner = this;
				return true;
			}
		}
	}
	AssertMsg(gotOneMatch,"Attempted to allocate a thread with a stack size that doesn't exist. Check that gameconfig.xml and stack_sizes.sch are in sync");

	Assertf(0, "scrThread::AllocateStack - failed to allocate a stack of size %d when attempting to start a new script", stackSize);

	return false;
}

void scrThread::GetStackSummary(int &outStackInUse,int &outStackTotal)
{
	outStackInUse = 0;
	outStackTotal = 0;
	for (int i=0; i<sm_Stacks.GetCount(); i++) {
		int thisStack = sm_Stacks[i].m_StackSize * sizeof(scrValue);
		outStackTotal += thisStack;
		if(sm_Stacks[i].m_Owner)
			outStackInUse += thisStack;
	}
}

#if !__FINAL
void scrThread::StartMonitoringScriptArraySize(s32 *pAddressToMonitor)
{
	if (scrVerifyf(GetActiveThread(), "scrThread::StartMonitoringScriptArraySize - no active script thread"))
	{
		ms_scriptArraySizeMonitor.StartMonitoring(pAddressToMonitor, GetActiveThread()->GetThreadId());
	}
}

bool scrThread::CheckIfMonitoredScriptArraySizeHasChanged(const char *pStringToDisplay, s32 integerToDisplay)
{
	return ms_scriptArraySizeMonitor.CheckIfMonitoredAddressHasChanged(pStringToDisplay, integerToDisplay);
}
#endif	//	!__FINAL


#if !__NO_OUTPUT
void scrThread::CaptureStackTrace(int *addresses, int kMaxAddresses)
{
	if (m_Serialized.m_CallDepth) {
		int count = 0;
		for (int i=m_Serialized.m_CallDepth; --i>=0; )
		{
			if (count < kMaxAddresses)
			{
				addresses[count] = m_Serialized.m_CallStack[i];
				++count;
			}
		}
	}
}

void scrThread::DefaultPrintFn(const char* fmt, ...)
{
	va_list args;
	va_start(args,fmt);
	diagLogDefault(Channel_Script, DIAG_SEVERITY_DISPLAY, __FILE__, __LINE__, fmt, args);
	va_end(args);
}
#endif // !__NO_OUTPUT

inline const char* DecodeName(const scrProgram& opcodes, int callstackIndex)
{
	//Note(crolivier): Prevent an issue where callstackIndex is bad and might cause a deref. I don't
	//					see how this can happen naturally but if for w/e reason callstackIndex gets
	//					corrupted, lets not crash here.
	const u8* name = &opcodes[callstackIndex];
	return (callstackIndex & scrPageMask) >= 3 && name[-3] ? (const char*)(name) : "unknown";
}

bool scrThread::PrintStackTrace(void (*printFn)(const char* fmt, ...))
{
	if (m_Serialized.m_CallDepth) {
		const scrProgram& opcodes = *scrProgram::GetProgram(m_Serialized.m_Prog);
		for (int i = m_Serialized.m_CallDepth; --i >= 0; )
		{
			if (i < MAX_CALLSTACK)
			{
				int pc = GetProgramCounter(m_Serialized.m_CallDepth - i - 1);
				const int callstack = m_Serialized.m_CallStack[i];
				int offset = pc - (callstack - 7);
				const char* decodeName = DecodeName(opcodes, callstack);
				if (i)
				{
					printFn("%s%s+%d(pc = %d)", sm_PrintStackPrefix, decodeName, offset, pc);
				}
				else
				{
					printFn("%s%s[%s]+%d(pc = %d)", sm_PrintStackPrefix, decodeName, GetScriptName(), offset, pc);
				}
			}
		}
	}

#if __BANK
	if ( m_debugFaultHandler )
	{
		m_debugFaultHandler( NULL, this );
	}
#endif

	return true;
}

#if USE_PROFILER
__forceinline void scrThread::TryAutoSample(const scrThread::Serialized& ser)
{
	if (!s_bIsRockyCapturingScriptCallstacks)
		return;

	u64 timestamp = sysTimer::GetTicks();
	if (timestamp < s_nextTimestamp)
		return;

	SaveCallstackForRocky(ser, timestamp);
}

void scrThread::SaveCallstackForRocky(const scrThread::Serialized& ser, u64 timestamp)
{
	if (size_t* callstack = Profiler::CreateCallStack(Profiler::Mode::SCRIPTS_AUTOSAMPLING, Profiler::Timestamp::Pack(timestamp)))
	{
		size_t programMask = (size_t)(ser.m_Prog) << 32ull;

		for (int i = Min((int)ser.m_CallDepth, MAX_CALLSTACK - 1); --i >= 0;)
		{
			*callstack++ = (programMask | ser.m_CallStack[i]);
		}

		//Note(crolivier): scrProgramId can't be 0 or 1 so used top 32 bits of 0 to 
		//				   identify threadId to be used for names on symbol resolution
		*callstack++ = (size_t)(ser.m_ThreadId);
		*callstack++ = 0;
	}

	s_nextTimestamp = timestamp + s_autosamplingTimeLimit;
}

void scrThread::ResolveAddressForRocky(size_t address, Profiler::ScriptSymbol& outSymbol)
{
	int progamId = (address >> 32);
	if (progamId == 0)
	{
		//programId of 0 means the address has a threadId instead
		scrThreadId threadId = (scrThreadId)(address & 0xffffffff);
		const scrThread* pThread = scrThread::GetThread(threadId);
		if (pThread)
			outSymbol.m_Name = pThread->GetScriptName();
		else
			outSymbol.m_Name.AppendFormatted(threadId, "ThreadId: %u");
	}
	else
	{
		if (const scrProgram* pProgram = scrProgram::GetProgram((scrProgramId)(progamId)))
		{
			const int callstack = (address & 0xffffffff);
			const char* decodeName = DecodeName(*pProgram, callstack);
			outSymbol.m_Name = decodeName;
		}
		else
		{
			outSymbol.m_Name = "UNKNOWN";
		}
	}
}
#endif //USE_PROFILER

#if RSG_PC && RSG_CPU_X64

/*
This code is exclusively used to support the assert statement in CreateThread
to be sure that the value I'm reading from the vtable is actually that of run.

Courtesy of voodoo from LH + SM
*/

static scrThread::State DummyCallScrThreadRun(scrThread *dummyPtr, int insnCount) {
	return dummyPtr->scrThread::Run(insnCount);
}

static u8 * FindJump(const void *code) {
	u8 *byte = (u8*)code;
	while(*(u8*)byte != 0xE8 &&
		  *(u8*)byte != 0xE9
		  ){	byte++; }
	return byte;
}

static volatile const void *FollowJmpNear(const void *code) {
	const u8 *jmp = FindJump(code);
	u32 relativeOffset = *(u32*)(jmp+1);
	u64 baseAddress = (u64)(jmp+5);
	const void *ret;
	ret = (void*)(baseAddress+relativeOffset);
	return ret;
}

#endif


scrThreadId scrThread::CreateThread(scrProgramId progId,const void *argStruct,int argStructSize,int stackSize) {
	if (progId) {
		// Make an initial pass through the list, killing off all aborted threads that have stack
		// sizes compatible with the one we're trying to create.
		for (int i=0; i<sm_Threads.GetCount(); i++) {
			if (sm_Threads[i]->m_Serialized.m_State==ABORTED && sm_Threads[i]->m_Serialized.m_ThreadId && sm_Threads[i]->m_Serialized.m_StackSize == stackSize)
				sm_Threads[i]->Kill();
		}
		// Then find an available thread and attach an appropriate-sized stack to it.
		for (int i=0; i<sm_Threads.GetCount(); i++) {
			if (sm_Threads[i]->m_Serialized.m_ThreadId==0) {
				if (!sm_Threads[i]->AllocateStack(stackSize))
					return THREAD_INVALID;
				int tlsSize = sm_Threads[i]->m_Serialized.m_TLS.GetMaxCount();
				for(int iTLS=0;iTLS<tlsSize;iTLS++)
					sm_Threads[i]->m_Serialized.m_TLS[iTLS].Reference = 0;
				//@@: location SCRTHREAD_CREATETHREAD_RESET
				sm_Threads[i]->Reset(progId,argStruct,argStructSize);
				if (progId)
				{
					scrProgram::GetProgram(progId)->AddRef();
				}
#if RSG_PC && RSG_CPU_X64 && !__RESOURCECOMPILER
				
				//@@: range SCRTHREAD_CREATETHREAD_STORE_SCRTHREAD_VFT {
				// Get my V-Table pointer, *for this instance of the object*
				DWORD64* vtablePtr = (DWORD64*)((DWORD64*)sm_Threads[i])[0];
				// Add two DWORDS, as that'll let me index to the run method, which we know they're currently
				// hooking
				vtablePtr+=2;
				// Ensure that it's pointing to run, in case somebody makes scrThread more complex.
//				Assertf(vtablePtr == (DWORD64)&scrThread::Run, "The second vtable pointer is not pointing to run. This will cause problems in our cheat-detection."); 
				// Create sysObfuscated containers for each of these, to avoid Cheat Engine scans
				sysObfuscated<u32> progId((u32)(sm_Threads[i]->m_Serialized.m_Prog));
				//@@: location SCRTHREAD_CREATETHREAD_POPULATE_RUN_FXN
				sysObfuscated<u64> runFunctionPtr(*vtablePtr);
				// Store them off in our atMap<>
				sm_ProgIdRunFunctions[progId] = runFunctionPtr;

				/*
				Ensuring that the vtablePtr is aiming at the Run() function, in case anybody ever modifies
                the scrThread function to have more virtual functions. This secures us from incorrectly 
                banning innocents, at least at the Beta level.
				*/
				ASSERT_ONLY(volatile const void *func = FollowJmpNear(DummyCallScrThreadRun));
				ASSERT_ONLY(DWORD64 dwFunc = (DWORD64)(func));
				Assert(dwFunc == *vtablePtr);


				//@@: } SCRTHREAD_CREATETHREAD_STORE_SCRTHREAD_VFT 
#endif
				// Return as normal
				//@@: location SCRTHREAD_CREATETHREAD_RETURN_SUCCESS
				return (sm_Threads[i]->m_Serialized.m_ThreadId = (scrThreadId) ++sm_ThreadId);
			}
		}
	}
	return THREAD_INVALID;
}


void scrThread::KillAllThreads() {
	for (int i = 0; i < sm_Threads.GetCount(); i++) {
		if (sm_Threads[i]->m_Serialized.m_ThreadId)
			sm_Threads[i]->Kill();
	}
}

#ifdef EXAMPLE_PROFILER_GLUE_CODE
// Example typical rage glue code
#include "grcore/font.h"

# if SCRIPT_PROFILING
static float profY, profHeight;
static int profInt;

static void profDisplayf(bool severe,const char *fmt,...)
{
	va_list args;
	va_start(args,fmt);
	char buffer[256];
	vformatf(buffer,sizeof(buffer),fmt,args);
	va_end(args);

	// don't run off the bottom of the screen
	if (profY > GRCDEVICE.GetGlobalHeight() - profHeight * 10)
		return;

	Color32 sevColor;
	if (severe) 
		sevColor.Set(240,0,0);
	else 
		sevColor.Set(240,240,240);
	grcFont::GetCurrent().Draw(50,profY,sevColor,buffer);
	profY += profHeight;
	// Periodic spacing so it's easier to read
	if (++profInt == 4) 
	{
		profY += profHeight*0.5f;
		profInt = 0;
	}
}

void YourFunction(scrThread *profScript,bool showOverview) {
	if (profScript || showOverview) {
		PUSH_DEFAULT_SCREEN();
		profY = 50.0f;
		profInt = -1;		// for inserting a gap periodically so it's easier to read
		profHeight = (float) grcFont::GetCurrent().GetHeight();
		if (showOverview)
			scrThread::DisplayProfileOverview(profDisplayf);
		else
			profScript->DisplayProfileData(profDisplayf);
		POP_DEFAULT_SCREEN();
	}
}
# endif	// SCRIPT_PROFILING
#endif	// EXAMPLE_PROFILER_GLUE_CODE

#if SCRIPT_PROFILING
int scrThread::sm_ProfileSortMethod;
const char *scrThread::sm_ProfileSortMethodStrings[] = { "Inclusive Time", "Exclusive Time", "Native Function Time", "Peak Exclusive Time", "Peak Native Time", "Inclusive Insns", "Exclusive Insns", "Peak Exclusive Insns", "Entry Name" };
scrProgram *scrThread::sm_ProfileProg;

void scrThread::DisplayProfileData(void (*displayf)(bool severe,const char*,...)) {
	// This is designed to be called every frame when active, so it needs to be fast.
	// It's also not particularly thread-safe, let's see if that's a problem in practice.
	if (m_ProfUpdateCount && m_Serialized.m_State != ABORTED) {
		scrProgram *prog = scrProgram::GetProgram(m_Serialized.m_Prog);
		const char *name = prog->GetScriptName();
		sm_ProfileProg = prog;
		size_t sl = strlen(name);
		displayf(false,"%55s,%8s,%8s,%8s,%8s,%8s,%8s,%8s,%8s",name+(sl>55?sl-55:0),
		sm_ProfileSortMethod==0?"INCLTIME":"InclTime",
		sm_ProfileSortMethod==1?"EXCLTIME":"ExclTime",
		sm_ProfileSortMethod==2?"NATVTIME":"NatvTime",
		sm_ProfileSortMethod==3?"EXPKTIME":"ExPkTime",
		sm_ProfileSortMethod==4?"NATVPEAK":"NatvPeak",
		sm_ProfileSortMethod==5?"INCLINSN":"InclInsn",
		sm_ProfileSortMethod==6?"EXCLINSN":"ExclInsn",
		sm_ProfileSortMethod==7?"EXPKINSN":"ExPkInsn");
		// Copy so we can sort, and also to maximize thread safety
		u32 pbc = m_ProfBuffer.GetCount();
		ProfEntry *tempData = Alloca(ProfEntry,pbc);
		memcpy(tempData, &m_ProfBuffer[0], pbc * sizeof(ProfEntry));
		switch (sm_ProfileSortMethod) {
			case 0: std::sort(&tempData[0],&tempData[pbc],ProfEntry::CompareByInclTime); break;
			case 1: std::sort(&tempData[0],&tempData[pbc],ProfEntry::CompareByExclTime); break;
			case 2: std::sort(&tempData[0],&tempData[pbc],ProfEntry::CompareByNatvTime); break;
			case 3: std::sort(&tempData[0],&tempData[pbc],ProfEntry::CompareByPeakExclTime); break;
			case 4: std::sort(&tempData[0],&tempData[pbc],ProfEntry::CompareByPeakNatvTime); break;
			case 5: std::sort(&tempData[0],&tempData[pbc],ProfEntry::CompareByInclInsns); break;
			case 6: std::sort(&tempData[0],&tempData[pbc],ProfEntry::CompareByExclInsns); break;
			case 7: std::sort(&tempData[0],&tempData[pbc],ProfEntry::CompareByPeakExclInsns); break;
			case 8: std::sort(&tempData[0],&tempData[pbc],ProfEntry::CompareByName_Thread); break;
		}
		float ttm = sysTimer::GetTicksToMicroseconds();
		float oouc = ttm / m_ProfUpdateCount;
		for (u32 i=0; i<pbc; i++) {
			ProfEntry &pe = tempData[i];
			float inclUsec = pe.InclTicks * oouc;
			float exclUsec = pe.ExclTicks * oouc;
			float natvUsec = pe.NatvTicks * oouc;
			float peakExcl = pe.PeakExclTicks * ttm;
			float peakNatv = pe.PeakNatvTicks * ttm;
			bool severe = exclUsec > 250.0f || peakExcl > 250.0f || natvUsec > 250.0f || peakNatv > 250.0f;
			const char *name = prog->ProcNames[pe.SelfIndex];
			size_t sl = strlen(name);
			displayf(severe,"%55s,%8.0f,%8.0f,%8.0f,%8.0f,%8.0f,%8u,%8u,%8u",name+(sl>55?sl-55:0),inclUsec,exclUsec,natvUsec,peakExcl,peakNatv,pe.InclInsns/m_ProfUpdateCount,pe.ExclInsns/m_ProfUpdateCount,pe.PeakExclInsns);
		}
		sm_ProfileProg = NULL;
	}
}

void scrThread::DisplayProfileOverview(void (*displayf)(bool severe,const char*,...)) {
	ProfEntry *tempData = Alloca(ProfEntry, sm_Threads.GetCount());		// actually max scripts here.
	// Accumulate all of the data across all running threads.
	u32 tc = 0;
	for (int i=0; i<sm_Threads.GetCount(); i++) {
		scrThread *t = sm_Threads[i];
		if (t && t->sm_ThreadId && t->m_ProfUpdateCount && t->m_Serialized.m_State != ABORTED) {
			tempData[tc] = t->m_ProfTotals;
			// Compute the averages
			float ttm = sysTimer::GetTicksToMicroseconds();
			float oouc = ttm / t->m_ProfUpdateCount;
			tempData[tc].SelfIndex = i;			// so that we can find ourselves again after sorting
			tempData[tc].ExclTicks = (u32)(tempData[tc].ExclTicks * oouc);
			tempData[tc].NatvTicks = (u32)(tempData[tc].NatvTicks * oouc);
			tempData[tc].InclTicks = tempData[tc].ExclTicks + tempData[tc].NatvTicks;
			tempData[tc].PeakExclTicks = (u32)(tempData[tc].PeakExclTicks * ttm);
			tempData[tc].PeakNatvTicks = (u32)(tempData[tc].PeakNatvTicks * ttm);
			tempData[tc].ExclInsns /= t->m_ProfUpdateCount;
			++tc;
		}
	}
	// Sort the data
	switch (sm_ProfileSortMethod) {
			case 0: std::sort(&tempData[0],&tempData[tc],ProfEntry::CompareByInclTime); break;
			case 1: std::sort(&tempData[0],&tempData[tc],ProfEntry::CompareByExclTime); break;
			case 2: std::sort(&tempData[0],&tempData[tc],ProfEntry::CompareByNatvTime); break;
			case 3: std::sort(&tempData[0],&tempData[tc],ProfEntry::CompareByPeakExclTime); break;
			case 4: std::sort(&tempData[0],&tempData[tc],ProfEntry::CompareByPeakNatvTime); break;
			case 5: 
			case 6: std::sort(&tempData[0],&tempData[tc],ProfEntry::CompareByExclInsns); break;
			case 7: std::sort(&tempData[0],&tempData[tc],ProfEntry::CompareByPeakExclInsns); break;
			case 8: std::sort(&tempData[0],&tempData[tc],ProfEntry::CompareByName_Overview); break;
	}
	// Display the data
	displayf(false,"%55s,%5s,%8s,%8s,%8s,%8s,%8s,%8s,%8s","Thread Name","Sleep",
		sm_ProfileSortMethod==0?"BOTHTIME":"BothTime",
		sm_ProfileSortMethod==1?"EXCLTIME":"ExclTime",
		sm_ProfileSortMethod==2?"NATVTIME":"NatvTime",
		sm_ProfileSortMethod==3?"EXPKTIME":"ExPkTime",
		sm_ProfileSortMethod==4?"NATVPEAK":"NatvPeak",
		sm_ProfileSortMethod==5||
		sm_ProfileSortMethod==6?"EXCLINSN":"ExclInsn",
		sm_ProfileSortMethod==7?"EXPKINSN":"ExPkInsn");

	for (u32 i=0; i<tc; i++) {
		ProfEntry &pe = tempData[i];
		bool severe = pe.InclTicks > 500 || pe.PeakExclTicks > 250 || pe.PeakNatvTicks > 250;
		const char *name = sm_Threads[pe.SelfIndex]->GetScriptName();
		size_t sl = strlen(name);
		displayf(severe,"%55s,%5.0f,%8u,%8u,%8u,%8u,%8u,%8u,%8u",name+(sl>55?sl-55:0),sm_Threads[pe.SelfIndex]->m_Serialized.m_Wait * 1000.0f,pe.InclTicks,pe.ExclTicks,pe.NatvTicks,
			pe.PeakExclTicks,pe.PeakNatvTicks,pe.ExclInsns,pe.PeakExclInsns);
	}
}

void scrThread::ResetProfileData() {
	for (int i=0; i<m_ProfBuffer.GetCount(); i++)
		m_ProfBuffer[i].Zero();
	m_ProfTotals.Zero();
	m_ProfUpdateCount = 0;
}
#endif		// SCRIPT_PROFILING

void scrThread::Kill() {
	scrAssertf(m_Serialized.m_ThreadId, "Invalid ThreadId.  You may be trying to kill a thread that was never started.");

#if !__FINAL
	ms_scriptArraySizeMonitor.StopMonitoringForThisThread(m_Serialized.m_ThreadId);
#endif	//	!__FINAL

	if (s_CurrentThread == this) {
		m_AbortReason = "via scrThread::Kill on myself";
		m_Serialized.m_State = ABORTED;
	}
	else {
		scrProgram *prog = scrProgram::GetProgram(m_Serialized.m_Prog);
		if (prog)
			prog->Release();
		for (int i=0; i<sm_Stacks.GetCount(); i++)
			if (sm_Stacks[i].m_Owner == this)
				sm_Stacks[i].m_Owner = NULL;
		m_Serialized.m_ThreadId = THREAD_INVALID;
		m_Serialized.m_StackSize = 0;
		m_Stack = NULL;
	}

#if !__FINAL
    m_threadBreakpoints.Kill();
#endif
#if __BANK
	m_debugFaultHandler = FaultHandlerFunc::NullFunctor();
#endif
}


void scrThread::KillThread(scrThreadId id) {
	for(int i = 0; i < sm_Threads.GetCount(); i++) {
		if (sm_Threads[i]->m_Serialized.m_ThreadId == id)
			sm_Threads[i]->Kill();
	}
}


scrThread* scrThread::GetThread(scrThreadId id) {
	// Early out on obviously bad id; also avoids accidentally
	// matching a deallocated thread.
	if (!id)
		return NULL;

	for (int i=0; i<sm_Threads.GetCount(); i++)
		if (sm_Threads[i]->m_Serialized.m_ThreadId == id)
			return sm_Threads[i];
	return NULL;
}


int scrThread::GetThreadCount() {
	return sm_Threads.GetCount();
}


scrThread& scrThread::GetThreadByIndex(int i) {
	return *sm_Threads[i];
}

namespace rage {
	namespace rageScriptDebug
    {
        static char s_printBuffer[1024];

		//default print impl...
		void DefaultPrintString(const char* str) 
        {
			char *stop = s_printBuffer + sizeof(s_printBuffer) - 1;
			if (!str) 
				str = "[null]";
			char *dst = s_printBuffer + (int) strlen(s_printBuffer);
			while (*str)
			{
				if (*str == '\n')
				{
					*dst = 0;
					scrDisplayf( "%s", s_printBuffer );
					dst = s_printBuffer;
				}
				else if (dst < stop)
				{
					*dst++ = *str;
				}
				++str;
			}
			*dst = 0;
		}

		void DefaultPrintFloat(int width,int prec,float val) 
        {

            char cValue[64];
			if ( width || prec )
            {
				sprintf(cValue, "%*.*f",width,prec,val);
            }
			else
            {
                sprintf(cValue, "%f",val);
			}

            safecat( s_printBuffer, cValue, sizeof(s_printBuffer) );
		}

		void DefaultPrintInt(int width,int val) 
        {
            char cValue[64];
			if ( width )
            {
                sprintf(cValue, "%*d",width,val);
            }
			else
            {
                sprintf(cValue, "%d",val);
            }

            safecat( s_printBuffer, cValue, sizeof(s_printBuffer) );
		}

		void DefaultPrintNl(void) {
			scrDisplayf( "%s", s_printBuffer );
            s_printBuffer[0] = 0;
		}

		void DefaultPrintVector(const Vector3& val) 
        {
            char cValue[256];
            sprintf( cValue, "<< %g, %g, %g >>",val.x,val.y,val.z );
			
            safecat( s_printBuffer, cValue, sizeof(s_printBuffer) );
		}

        void Init()
        {
            s_printBuffer[0] = 0;
		}

		void	ReBindDefaults()
		{

			gScriptPrintString		= DefaultPrintString;
			gScriptPrintFloat		= DefaultPrintFloat;
			gScriptPrintInt			= DefaultPrintInt;
			gScriptPrintNewLine		= DefaultPrintNl;
			gScriptPrintVector3		= DefaultPrintVector;
		}

		//global print callbacks defaults.
		PrinterStrFn*		gScriptPrintString		= DefaultPrintString;
		PrinterFloatFn*		gScriptPrintFloat		= DefaultPrintFloat;
		PrinterIntFn*		gScriptPrintInt			= DefaultPrintInt;
		PrinterNewLineFn*	gScriptPrintNewLine		= DefaultPrintNl;
		PrinterVectorFn*	gScriptPrintVector3		= DefaultPrintVector;
	}
}

#if	!__FINAL	// script output isn't supported in final, even if output is enabled
static void PrintString(const char *s) 
{
	SCRIPT_TIME_CUTOUT()

	scrAssertf(gScriptPrintString, "The ScriptPrintString function is not defined.");
	(*gScriptPrintString)(s);
}

static void PrintFloat(float f)
{
	SCRIPT_TIME_CUTOUT()

	scrAssertf(gScriptPrintFloat, "The ScriptPrintFloat function is not defined.");
	(*gScriptPrintFloat)(0,0,f);
}

static void PrintFloat2(int a,int b,float f)
{
	SCRIPT_TIME_CUTOUT()

	scrAssertf(gScriptPrintFloat, "The ScriptPrintFloat function is not defined.");
	(*gScriptPrintFloat)(a,b,f);
}

static void PrintInt(int i) 
{
	SCRIPT_TIME_CUTOUT()

	scrAssertf(gScriptPrintInt, "The ScriptPrintInt function is not defined.");
	(*gScriptPrintInt)(0,i);
}

static void PrintInt2(int a,int b)
{
	SCRIPT_TIME_CUTOUT()

	scrAssertf(gScriptPrintInt, "The ScriptPrintInt function is not defined.");
	(*gScriptPrintInt)(a,b);
}

static void PrintVector(const scrVector & v)
{
	SCRIPT_TIME_CUTOUT()

	scrAssertf(gScriptPrintVector3, "The ScriptPrintVector3 function is not defined.");
	(*gScriptPrintVector3)(Vector3(v.x,v.y,v.z));
}

static void PrintNl()
{
	SCRIPT_TIME_CUTOUT()

	scrAssertf(gScriptPrintNewLine, "The ScriptPrintNewLine function is not defined.");
	(*gScriptPrintNewLine)();
}
#endif	// !__FINAL

#if !__NO_OUTPUT
static void PrintCommonFinal(diagChannel &, diagSeverity, unsigned types, int argc, const scrValue *argv)
{
	char buffer[1024], *bp = buffer;
	int remain = sizeof(buffer);

	while (argc) {
		switch (types & 3) {
		case scrValue::VA_INT: 
			formatf(bp,remain,"%d",argv->Int);
			break;
		case scrValue::VA_FLOAT: 
			formatf(bp,remain,"%f",argv->Float);
			break;
		case scrValue::VA_STRINGPTR: 
			formatf(bp,remain,"%s",scrDecodeString(argv->String));
			break;
		case scrValue::VA_VECTOR:
			{ scrValue *v = scrDecodeReference(argv->Reference);
			formatf(bp,remain,"<< %g, %g, %g >>",v[0].Float,v[1].Float,v[2].Float); }
			break;
		}
		int len = StringLength(bp);
		bp += len;
		remain -= len;
		--argc, ++argv, types >>= 2;
	}
	// We're attempting to make the output look like SCRIPT_ASSERT in commands_debug.cpp without having to duplicate a bunch of code.
	Displayf("[SCRIPT_FNL] %s\n", buffer);	
}

static void PrintLnFinalVarargs(scrThread::Info &info) 
{
	SCRIPT_TIME_CUTOUT()
	SCRIPT_VA_BEGIN(info);
	PrintCommonFinal(Channel_Script, DIAG_SEVERITY_DISPLAY, __t, SCRIPT_VA_COUNT, SCRIPT_VA_ARG);
}

PARAM(noscripttimestamps,"Disable timestamps on script TTY output");
static void PrintCommon(diagChannel &ch,diagSeverity severity,unsigned types,int argc,const scrValue *argv)
{
	char buffer[1024], *bp = buffer;
	int remain = sizeof(buffer);

	while (argc) {
		switch (types & 3) {
			case scrValue::VA_INT: 
				formatf(bp,remain,"%d",argv->Int);
				break;
			case scrValue::VA_FLOAT: 
				formatf(bp,remain,"%f",argv->Float);
				break;
			case scrValue::VA_STRINGPTR: 
				formatf(bp,remain,"%s",scrDecodeString(argv->String));
				break;
			case scrValue::VA_VECTOR:
				{ scrValue *v = scrDecodeReference(argv->Reference);
				formatf(bp,remain,"<< %g, %g, %g >>",v[0].Float,v[1].Float,v[2].Float); }
				break;
		}
		int len = StringLength(bp);
		bp += len;
		remain -= len;
		--argc, ++argv, types >>= 2;
	}
	// We're attempting to make the output look like SCRIPT_ASSERT in commands_debug.cpp without having to duplicate a bunch of code.
	if (severity == DIAG_SEVERITY_ASSERT && s_CurrentThread)
	{
		if (severity <= ch.MaxLevel)
		{
			diagLogf(ch, severity, 
				s_CurrentThread->GetScriptName(), s_CurrentThread->GetProgramCounter(0),
				"Assert: SCRIPT: Script Name = %s : Program Counter = %d : %s",s_CurrentThread->GetScriptName(),s_CurrentThread->GetProgramCounter(0),buffer);
		}
	}
	else if (PARAM_noscripttimestamps.Get())
		diagLogfHelper(ch,severity,"%s",buffer);
	else
		diagLogfHelper(ch,severity,"[%08u] %s",diagGetAdjustedSystemTime(),buffer);
}

static void PrintLnVarargs(scrThread::Info &info) 
{
	SCRIPT_TIME_CUTOUT()

	SCRIPT_VA_BEGIN(info);
	PrintCommon(Channel_Script,DIAG_SEVERITY_DISPLAY,__t,SCRIPT_VA_COUNT,SCRIPT_VA_ARG);
}

static void AssertLnVarargs(scrThread::Info &info) 
{
	SCRIPT_TIME_CUTOUT()

	SCRIPT_VA_BEGIN(info);
	PrintCommon(Channel_Script,DIAG_SEVERITY_ASSERT,__t,SCRIPT_VA_COUNT,SCRIPT_VA_ARG);
}

atRangeArray<diagChannel*,192> scriptChannels;

#if	!__FINAL	// script output isn't supported in final, even if output is enabled
static void RegisterScriptChannel(int OUTPUT_ONLY(channel),const char *OUTPUT_ONLY(name),int OUTPUT_ONLY(parentChannel))
{
	if (scriptChannels[channel]) {
		if (!strstr(scriptChannels[channel]->Tag,name))
			Errorf("Script channel with this slot already registered");
	}
	else {
		char temp[96], temp2[96];
		diagChannel *parent = parentChannel==-1?&Channel_Script:scriptChannels[parentChannel];
		scriptChannels[channel] = rage_new diagChannel(parent,StringDuplicate(formatf(temp,"[Script_%s] ",name)),StringDuplicate(formatf(temp2,"Script_%s",name)));
		// Call this again to pick up newly added channel
		diagChannel::InitArgs();
#if __BANK
		BANKMGR.SendSubChannelInfo(parent->ParamTag, scriptChannels[channel]->ParamTag);
#endif
	}
}

static int GetScriptChannelLevel(int OUTPUT_ONLY(channel),int OUTPUT_ONLY(chtype))
{
	if (!scriptChannels[channel] || chtype<0 || chtype>2) {
		Errorf("No script channel registered here or bad channel type");
		return 0;
	}
	else {
		u8 *id = &scriptChannels[channel]->FileLevel;
		return id[chtype];
	}
}

static void SetScriptChannelLevel(int channel,int chtype,int severity)
{
	if (!scriptChannels[channel] || chtype<0 || chtype>2 || severity<0 || severity>7) 
		Errorf("No script channel registered here or bad channel type or severity");
	else {
		u8 *id = &scriptChannels[channel]->FileLevel;
		id[chtype] = u8(severity);
		// Update MaxLevel
		id[3] = Max(id[0], Max(id[1], id[2]));
	}
}
#endif	// !__FINAL


static void CLogLn(scrThread::Info & OUTPUT_ONLY(info)) 
{
	SCRIPT_TIME_CUTOUT()

	SCRIPT_VA_BEGIN(info);

	if (SCRIPT_VA_COUNT < 3) {
		scrAssertf(false,"Insufficient parameters passed to CLogLn, PC = %d",s_CurrentThread->GetProgramCounter(0));
		return;
	}
	diagChannel *ch = scriptChannels[SCRIPT_VA_ARG->Int]; 
	SCRIPT_VA_NEXT;
	if (!ch) {
		scrAssertf(false,"Bad channel passed to CLogLn, PC = %d",s_CurrentThread->GetProgramCounter(0));
		return;
	}
	diagSeverity severity = (diagSeverity) SCRIPT_VA_ARG->Int; 
	SCRIPT_VA_NEXT;
	if (severity < DIAG_SEVERITY_FATAL || severity > DIAG_SEVERITY_DEBUG3) {
		scrAssertf(false,"Bad severity passed to CLogLn, PC = %d",s_CurrentThread->GetProgramCounter(0));
		return;
	}
	PrintCommon(*ch,severity,__t,SCRIPT_VA_COUNT,SCRIPT_VA_ARG);
}

static void CPrintCommon(scrThread::Info &info,const char *ASSERT_ONLY(tag),diagSeverity severity) 
{
	SCRIPT_TIME_CUTOUT()

	ASSERT_ONLY(tag=(tag?tag:"(INVALID TAG)");) // Stop crashing at me Bro

	SCRIPT_VA_BEGIN(info);
	if (SCRIPT_VA_COUNT < 2) {
		scrAssertf(false,"Insufficient parameters passed to %s, PC = %d",tag,s_CurrentThread->GetProgramCounter(0));
		return;
	}
	int channel = SCRIPT_VA_ARG->Int;
	SCRIPT_VA_NEXT;
	if (channel < 0 || channel >= scriptChannels.GetMaxCount()) {
		scrAssertf(false,"Channel index(%d, max=%d) out of range passed to %s, PC = %d",channel, scriptChannels.GetMaxCount(), tag,s_CurrentThread->GetProgramCounter(0));
		return;
	}
	diagChannel *ch = scriptChannels[channel]; 
	if (!ch) {
		scrAssertf(false,"Bad channel passed to %s, PC = %d",tag,s_CurrentThread->GetProgramCounter(0));
		return;
	}
	PrintCommon(*ch, severity, __t, SCRIPT_VA_COUNT, SCRIPT_VA_ARG);
}

static void CDebug3Ln(scrThread::Info &info) 
{
	CPrintCommon(info,"CDebug3Ln",DIAG_SEVERITY_DEBUG3);
}

static void CDebug2Ln(scrThread::Info &info) 
{
	CPrintCommon(info,"CDebug2Ln",DIAG_SEVERITY_DEBUG2);
}

static void CDebug1Ln(scrThread::Info &info) 
{
	CPrintCommon(info,"CDebug1Ln",DIAG_SEVERITY_DEBUG1);
}

static void CPrintLn(scrThread::Info &info) 
{
	CPrintCommon(info,"CPrintLn",DIAG_SEVERITY_DISPLAY);
}

static void CWarningLn(scrThread::Info & OUTPUT_ONLY(info)) 
{
	CPrintCommon(info,"CWarningLn",DIAG_SEVERITY_WARNING);
}

static void CErrorLn(scrThread::Info & OUTPUT_ONLY(info)) 
{
	CPrintCommon(info,"CErrorLn",DIAG_SEVERITY_ERROR);
}

static void CAssertLn(scrThread::Info & OUTPUT_ONLY(info)) 
{
	CPrintCommon(info,"CAssertLn",DIAG_SEVERITY_ASSERT);
}
#endif		// __NO_OUTPUT

#if !__FINAL
static void PrintCallstack()
{
	SCRIPT_TIME_CUTOUT()

	scrThread::PrePrintStackTrace();
}

static void BreakPoint(void) {
	__debugbreak();
}

#if RSG_PROSPERO || RSG_SCARLETT
static void RockyTag(const char* name)
{
	PROFILER_TAG("Game\\Script", name);
}
#endif //RSG_PROSPERO || RSG_SCARLETT
#endif // !__FINAL

static float Sin(float f) {
	return sinf(f * DtoR);
}

static float Cos(float f) {
	return cosf(f * DtoR);
}

static float Sqrt(float f) {
	return sqrtf(f);
}

static float Pow(float a,float b) {
	return powf( a, b );
}

static float Log10(float f) {
	return log10(f);
}

static float VMag(const scrVector &v) {
	return sqrtf(v.x*v.x + v.y*v.y + v.z*v.z);
}

static float VMag2(const scrVector &v) {
	return v.x*v.x + v.y*v.y + v.z*v.z;
}


static inline float sqr(float f) { return f * f; }

static float VDist(const scrVector &a,const scrVector &b) {
	return sqrtf(sqr(a.x-b.x)+sqr(a.y-b.y)+sqr(a.z-b.z));
}

static float VDist2(const scrVector &a,const scrVector &b) {
	return sqr(a.x-b.x)+sqr(a.y-b.y)+sqr(a.z-b.z);
}

static int ShiftR(int a,int b) {
	return a >> b;
}

static int ShiftL(int a,int b) {
	return a << b;
}

static int Floor(float f) {
	return (int)floorf(f);
}

static int Ceil(float f) {
	return (int)ceilf(f);
}

static int Round(float f) {
	return (int)floorf(f + 0.5f);
}

static float ToFloat(int i) {
	return (float) i;
}

static void SetThisThreadPriority(int prio) {
	s_CurrentThread->SetThreadPriority( static_cast<scrThread::Priority>(prio) );
}

#if !__FINAL
static int GetThisThreadPriority() {
	return s_CurrentThread->GetThreadPriority();
}
#endif	// !__FINAL

void ClearTextLabel(const char *label) {
	((char*)label)[0] = 0;
}

#if !__FINAL
static const int LASTEST_CONSOLE_COMMAND_LEN=256;
static int s_iNumTokens=0;
static char s_LastestConsoleCommand[LASTEST_CONSOLE_COMMAND_LEN];
static const int MAX_TOKENS=16;
static const char* s_Tokens[MAX_TOKENS];
static char s_LastestTokenizedConsoleCommand[LASTEST_CONSOLE_COMMAND_LEN];

void sSetLastestConsoleCommand(CONSOLE_ARG_LIST)
{
	CONSOLE_HELP_TEXT("Specify the string that script command GET_LATEST_CONSOLE_COMMAND will return.");

	int totalLength=0;
	s_LastestConsoleCommand[0]='\0';
	s_iNumTokens = numArgs;
	if (s_iNumTokens > MAX_TOKENS)
	{
		s_iNumTokens = 0;
		scrWarningf("Commandline has too many tokens for tokenized script command!");
	}
	for (int i=0;i<numArgs;i++)
	{
		if (s_iNumTokens)
		{
			s_Tokens[i] = &s_LastestTokenizedConsoleCommand[totalLength];
			strncpy(&s_LastestTokenizedConsoleCommand[totalLength],args[i], sizeof(s_LastestTokenizedConsoleCommand)-totalLength);
		}
		totalLength+=(int) strlen(args[i]);
		safecat(s_LastestConsoleCommand,args[i],LASTEST_CONSOLE_COMMAND_LEN);
		if (i<numArgs-1)
		{
			safecat(s_LastestConsoleCommand," ",LASTEST_CONSOLE_COMMAND_LEN);
			++totalLength;
		}
	}

	// see if we truncated:
	if (totalLength>=LASTEST_CONSOLE_COMMAND_LEN)
	{
		output("WARNING - lastest console command was truncated for the scripting language");
	}
}
#endif

#if !__FINAL
static const char *GetLatestConsoleCommand() {
#if !__FINAL
	return s_LastestConsoleCommand;
#else
	return "";
#endif
}

static int GetNumConsoleCommandTokens() {
#if !__FINAL
	return s_iNumTokens;
#else
	return 0;
#endif
}

static const char *GetConsoleCommandToken(int NOTFINAL_ONLY(iToken)) {
#if !__FINAL
	if ((iToken < 0) || (iToken > s_iNumTokens)) {
		scrErrorf("Cannot get token %d, max is %d%s", iToken, s_iNumTokens, (!s_iNumTokens&&s_LastestConsoleCommand[0])? "(Failed to tokenize)" : "");
		return "";
	}
	return s_Tokens[iToken];
#else
	return "";
#endif
}

static void ResetLatestConsoleCommand() {
#if !__FINAL
	s_LastestConsoleCommand[0]='\0';
	s_iNumTokens = 0;
#endif
}
#endif	// !__FINAL

#if !__FINAL
void sSetScriptGlobalVar(CONSOLE_ARG_LIST)
{
	CONSOLE_HELP_TEXT("Set a script global variable; first parameter is the name, second parameter is the new value");

	if (numArgs < 2)
		output("Need two parameters");
	else {
		int idx = scrThread::LookupGlobalVariable(args[0]);
		if (idx == -1)
			output("Unknown global name");
		else {
			scrValue *v = scrThread::GetGlobalVariableAddress(idx);
			switch (scrThread::GetGlobalVariableType(idx)) {
				case scrValue::BOOL: v->Int = args[1][0]=='t'||args[1][0]=='T'; break;
				case scrValue::INT: v->Int = atoi(args[1]); break;
				case scrValue::FLOAT: v->Float = (float)atof(args[1]); break;
				case scrValue::TEXT_LABEL: strcpy((char*)v,args[1]); break;	// POTENTIALLY UNSAFE!
				default: output("Sorry, don't handle this type yet."); break;
			}
		}
	}
}

#if SCRIPT_DEBUGGING
void sGetScriptBreakOnFunc(CONSOLE_ARG_LIST)
{
	CONSOLE_HELP_TEXT("Set a breakpoint on any script call to a given native function; parameter is the name");
	if (numArgs < 1) {
		scrThread::SetFaultOnFunc();
	}
	else {
		bool bClearOnHit=true;
		for (int i=1 ; i<numArgs ; i++)
		{
			if (args[i] && args[i][0])
			{
				if (!stricmp(args[i], "all"))
				{
					bClearOnHit = false;
				}
			}
		}
		scrThread::SetFaultOnFunc(args[0], false, bClearOnHit);
	}
}
void sGetScriptTraceFunc(CONSOLE_ARG_LIST)
{
	CONSOLE_HELP_TEXT("Trace script calls to a given native function; parameter is the name");
	if (numArgs < 1) {
		scrThread::SetFaultOnFunc();
	}
	else {
		scrThread::SetFaultOnFunc(args[0], true, false);
	}
}
#endif

void sGetScriptGlobalVar(CONSOLE_ARG_LIST)
{
	CONSOLE_HELP_TEXT("Get a script global variable; parameter is the name");

	if (numArgs < 1)
		output("Need one parameter");
	else {
		int idx = scrThread::LookupGlobalVariable(args[0]);
		if (idx == -1)
			output("Unknown global name");
		else {
			scrValue *v = scrThread::GetGlobalVariableAddress(idx);
			char outbuf[256];
			strcpy(outbuf,"Sorry, don't handle this type yet.");
			switch (scrThread::GetGlobalVariableType(idx)) {
				case scrValue::BOOL: strcpy(outbuf,v->Int?"true":"false"); break;
				case scrValue::INT: sprintf(outbuf,"%d",v->Int); break;
				case scrValue::FLOAT: sprintf(outbuf,"%g",v->Float); break;
				case scrValue::TEXT_LABEL: output((char*)v); return;	// POTENTIALLY UNSAFE!
				case scrValue::VECTOR: sprintf(outbuf,"<<%g,%g,%g>>",v[0].Float,v[1].Float,v[2].Float); break;
			}
			output(outbuf);
		}
	}
}

const int NUM_NATIVE_FUNC_REGS = 10;
scrValue g_callNativeRegisters[NUM_NATIVE_FUNC_REGS];
scrValue::ValueType g_callNativeRegisterTypes[NUM_NATIVE_FUNC_REGS];

void sPrintNativeFuncRegs(CONSOLE_ARG_LIST)
{
	CONSOLE_HELP_TEXT("Prints the registers for use when calling native functions");
	char outbuf[256];

	for(int i = 0; i < NUM_NATIVE_FUNC_REGS; i++)
	{
		scrValue& val = g_callNativeRegisters[i];
		switch(g_callNativeRegisterTypes[i])
		{
		case scrValue::UNKNOWN:
			formatf(outbuf, "$%d = <empty>\n", i); break;
		case scrValue::BOOL:
			formatf(outbuf, "$%d = %s\n", i, val.Int ? "true" : "false"); 	break;
		case scrValue::INT:
			formatf(outbuf, "$%d = %d\n", i, val.Int); break;
		case scrValue::FLOAT:
			formatf(outbuf, "$%d = %f\n", i, val.Float); break;
		case scrValue::STRING:
			formatf(outbuf, "$%d = \"%s\"\n", i, val.String); break;
		default:
			formatf(outbuf, "$%d = <0x%x>\n", i, val.Reference); break;
		}
		output(outbuf);
	}
}

void sCallNativeFunction(CONSOLE_ARG_LIST)
{
	CONSOLE_HELP_TEXT("Calls a native function from the console. Parameters can be bools, floats, ints, strings");
	char outbuf[256];

	if (numArgs < 1)
	{
		output("Must specify a native function to call");
		return;
	}

	const char** arg = args; // The current arg we're processing

	int numArgsForCall = numArgs - 1;

	// Are we making a register assignment?
	int outputRegisterIndex = -1;

	if (*arg[0] == '$')
	{
		if (numArgs < 3 || strcmp(*(arg+1), "=") != 0)
		{
			formatf(outbuf, "Invalid register assignment. Should be call $N = FUNCNAME ...");
			output(outbuf);
			return;
		}
		outputRegisterIndex = strtol(*arg+1, NULL, 0);
		if (outputRegisterIndex < 0 || outputRegisterIndex >= NUM_NATIVE_FUNC_REGS)
		{
			formatf(outbuf, "Invalid register $%d, should be $0-$%d", outputRegisterIndex, NUM_NATIVE_FUNC_REGS);
			output(outbuf);
			return;
		}
		arg += 2;
		numArgsForCall -= 2;
	}

	const char* nativeName = *arg;
	arg++;

	u32 nativeHash = scrComputeHash(nativeName);
	scrSignature nativeSig = scrThread::LookupCommandSignature(nativeHash);

	if (!nativeSig.IsValid())
	{
		formatf(outbuf, "Couldn't find a signature for function %s. Was it exported?", nativeName);
		output(outbuf);
		return;
	}

	if (outputRegisterIndex >= 0 && nativeSig.GetReturnType() == scrValue::UNKNOWN)
	{
		formatf(outbuf, "Warning: No return value from function");
		output(outbuf);
		outputRegisterIndex = -1; // don't modify the register
		// don't return
	}

	int scrArgC = nativeSig.CountNumArgs();
	scrValue* scrArgs = Alloca(scrValue, scrArgC);

	if (numArgsForCall != nativeSig.CountNumArgs())
	{
#if __DEV
		char prettySig[256];
		nativeSig.PrettyPrint(prettySig, 256, nativeName);
#else
		const char* prettySig = nativeName;
#endif
		formatf(outbuf, "Mismatched number of arguments for NATIVE %s", prettySig);
		output(outbuf);
		return;
	}


	for(int i = 0; i < scrArgC; i++)
	{
		if (*arg[0] == '$')
		{
			// It's a register, make sure its in the right range and has the right type
			int index = strtol(*arg + 1, NULL, 0);
			if (index < 0 || index >= NUM_NATIVE_FUNC_REGS) {
				formatf(outbuf, "Invalid register $%d, should be $0-$%d", index, NUM_NATIVE_FUNC_REGS);
				output(outbuf);
				return;
			}
			if (g_callNativeRegisterTypes[index] != nativeSig.GetArgumentType(i))
			{
				formatf(outbuf, "Register $%d has wrong type for argument, expecting %d and found %d", index, nativeSig.GetArgumentType(i), g_callNativeRegisterTypes[index]);
				output(outbuf);
				return;
			}
			scrArgs[i] = g_callNativeRegisters[index];
			arg++;
		}
		else
		{
			switch(nativeSig.GetArgumentType(i))
			{
			case scrValue::BOOL:
				if ((*arg)[0] != 'f' &&
					(*arg)[0] != 'F')
				{
					scrArgs[i].Int = false;
				}
				else
				{
					scrArgs[i].Int = true;
				}
				arg++;
				break;
			case scrValue::INT:
				scrArgs[i].Int = strtol(*arg, NULL, 0);
				arg++;
				break;
			case scrValue::FLOAT:
				scrArgs[i].Float = (float)strtod(*arg, NULL);
				arg++;
				break;
			case scrValue::STRING:
				scrArgs[i].String = scrEncodeString(*arg);
				arg++;
				break;
			default:
				formatf(outbuf, "Can't call native functions that take arguments of type %d", nativeSig.GetArgumentType(i));
				output(outbuf);
				return;
			}
		}
	}

	scrValue result = scrThread::ExecuteNetworkCommand(nativeHash, scrArgC, scrArgs);

	if (outputRegisterIndex >= 0)
	{
		g_callNativeRegisters[outputRegisterIndex] = result;
		g_callNativeRegisterTypes[outputRegisterIndex] = nativeSig.GetReturnType();
	}

	switch(nativeSig.GetReturnType())
	{
	case scrValue::UNKNOWN:
		outbuf[0] = '\0';
		break; // no return value -> no output
	case scrValue::BOOL:
		formatf(outbuf, "%s", result.Int ? "true" : "false"); break;
	case scrValue::INT:
		formatf(outbuf, "%d", result.Int); break;
	case scrValue::FLOAT:
		formatf(outbuf, "%f", result.Float); break;
	case scrValue::STRING:
		formatf(outbuf, "\"%s\"", result.String); break;
	default:
		formatf(outbuf, "<0x%x>", result.Reference); break;
	}
	output(outbuf);
}
#endif

#if SCRIPT_DEBUGGING
PARAM(nativetrace, "Set per-native trace action in a semicolon-delimited list. Actions are: log,stack,assert. E.g. -nativetrace=SET_SLEEP_MODE_ACTIVE=log;IS_STRING_NULL=assert");

void scrThread::ParseNativeTraceCommandLine()
{
	const char* args = NULL;
	if(PARAM_nativetrace.Get(args))
	{
		Assert(args);
		while(*args != '\0')
		{
			// Copy the next ';' delimited arg
			char arg[256];
			char *p = arg;
			for (;;)
			{
				char c = *args;
				if (c == '\0')
				{
					break;
				}
				++args;
				if (c == ';')
				{
					break;
				}
				*p++ = c;

				// Non-final code, so trap is good enough here (ie,
				// don't need to protect against malicous buffer
				// overflows).
 				TrapGE((size_t)(p - arg), (size_t)NELEM(arg));
			}
			*p = '\0';

			// Split into NATIVE=action
			p = strchr(arg, '=');
			if(p)
			{
				*p = '\0';

				scrSignatureAndCmd* sigAndCmd = s_SignatureHash.SafeGet(scrComputeHash(arg));
				NativeCallLogInfo* nativeCallInfo = s_LogNativeCalls.Access((void*)sigAndCmd->cmd);
				if(nativeCallInfo)
				{
					if(stricmp(p+1, "log") == 0)
					{
						scrDebugf3("Native %s set to log", arg);
						nativeCallInfo->action = NativeCallLogInfo::ACTION_Log;
					}
					else if(stricmp(p+1, "dump") == 0 || stricmp(p+1, "stack") == 0 || stricmp(p+1, "dumpstack") == 0)
					{
						scrDebugf3("Native %s set to dump stack", arg);
						nativeCallInfo->action = NativeCallLogInfo::ACTION_DumpStack;
					}
					else if(stricmp(p+1, "break") == 0 || stricmp(p+1, "assert") == 0)
					{
						scrDebugf3("Native %s set to break", arg);
						nativeCallInfo->action = NativeCallLogInfo::ACTION_Break;
					}
					else
					{
						// Warning as well as assert so we see this in BankRelease
						scrWarningf("Unknown action for native trace: '%s'", p+1);
						scrAssertf(false, "Unknown action for native trace: '%s'", p+1);
					}
				}
				else
				{
					// Warning rather than assert so we see this in BankRelease
					scrWarningf("Native '%s' not found for native trace!", arg);
				}
			}
		}
	}
}

void scrThread::CreateNativeTraceBank()
{
	scrAssertf(ms_pNativeTraceBank, "Script native trace bank needs to be created first");
	if(ms_pCreateNativeTraceButton)//delete the create bank button
	{
		ms_pCreateNativeTraceButton->Destroy();
		ms_pCreateNativeTraceButton = NULL;
	}
	else
	{
		//bank must already be setup as the create button doesn't exist so just return.
		return;
	}

	bkBank& bank = *ms_pNativeTraceBank;

	// Re-organise the native call info's to group by filename
	typedef atMap<const char*, atMap<const char*, NativeCallLogInfo> > NativeCallsByName;
	NativeCallsByName nativeCallsByName;
	for(LogNativeCallMap::Iterator it = s_LogNativeCalls.CreateIterator(); !it.AtEnd(); ++it)
	{
		const NativeCallLogInfo& info = *it;
		atMap<const char*, NativeCallLogInfo>* pMap = nativeCallsByName.Access(info.filename);
		if(!pMap)
		{
			nativeCallsByName.Insert(info.filename, atMap<const char*, NativeCallLogInfo>());
			pMap = nativeCallsByName.Access(info.filename);
			Assert(pMap);
		}
		pMap->Insert(info.name, info);
	}

	// Create banks
	for(NativeCallsByName::Iterator it = nativeCallsByName.CreateIterator(); !it.AtEnd(); ++it)
	{
		const atMap<const char*, NativeCallLogInfo>& natives = *it;
		bool first = true;
		for(atMap<const char*, NativeCallLogInfo>::ConstIterator it = natives.CreateIterator(); !it.AtEnd(); ++it)
		{
			const NativeCallLogInfo& info = *it;
			NativeCallLogInfo* sourceInfo = s_LogNativeCalls.Access((void*)info.cmd);
			if(!sourceInfo)
			{
				continue;
			}
			if(first)
			{
				bank.PushGroup(sourceInfo->filename, false);
				first = false;
			}
			sourceInfo->pWidget = bank.AddCombo(sourceInfo->name, &sourceInfo->action, NELEM(s_nativeTraceOptions), s_nativeTraceOptions);
		}
		if(!first)
		{
			bank.PopGroup();
		}
	}
}
#endif // SCRIPT_DEBUGGING

#if SCRIPT_DEBUGGING
#define REGISTER_BUILTIN_COMMAND(name, hash, fn) RegisterCommand(SCRHASH(name,hash), fn, name, __FILE__, ::rage::scrSignature());
#else
#define REGISTER_BUILTIN_COMMAND(name, hash, fn) RegisterCommand(hash, fn);
#endif

extern "C" void ApiCheckPlugin_StartNativeHash();

void scrThread::RegisterBuiltinCommands() {
	ApiCheckPlugin_StartNativeHash();
	REGISTER_BUILTIN_COMMAND("WAIT",0x4ede34fbadd967a6,Wait);
	REGISTER_BUILTIN_COMMAND("START_NEW_SCRIPT",0xe81651ad79516e48,StartNewScript);
	REGISTER_BUILTIN_COMMAND("START_NEW_SCRIPT_WITH_ARGS",0xb8ba7f44df1575e1,StartNewScriptWithArgs);
	REGISTER_BUILTIN_COMMAND("START_NEW_SCRIPT_WITH_NAME_HASH",0xeb1c67c3a5333a92,StartNewScriptWithNameHash);
	REGISTER_BUILTIN_COMMAND("START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS",0xc4bb298bd441be78,StartNewScriptWithNameHashAndArgs);
#if !__NO_OUTPUT
	REGISTER_BUILTIN_COMMAND("PRINTLN_FINAL",0x91a49dbad86281f8,PrintLnFinalVarargs);
	REGISTER_BUILTIN_COMMAND("PRINTLN",0x4be6713cecfcfff3,PrintLnVarargs);
	REGISTER_BUILTIN_COMMAND("ASSERTLN",0xcf8d79ecfcf47473,AssertLnVarargs);
	REGISTER_BUILTIN_COMMAND("CLOGLN",0xef256ae8a5a27966,CLogLn);
	REGISTER_BUILTIN_COMMAND("CPRINTLN",0xf0783374333fd8ce,CPrintLn);
	REGISTER_BUILTIN_COMMAND("CWARNINGLN",0x9a6c65dddbec9c52, CWarningLn);
	REGISTER_BUILTIN_COMMAND("CERRORLN",0xd9911c7b5f8cd69c, CErrorLn);
	REGISTER_BUILTIN_COMMAND("CASSERTLN",0x83407b92d46f25c3, CAssertLn);
	REGISTER_BUILTIN_COMMAND("CDEBUG1LN",0xa308f935bdeccec0, CDebug1Ln);
	REGISTER_BUILTIN_COMMAND("CDEBUG2LN",0x4dc69742196f818a, CDebug2Ln);
	REGISTER_BUILTIN_COMMAND("CDEBUG3LN",0x1b08d1eb9d8c4931, CDebug3Ln);
#endif

#if 0	// Solely for grepping out for scannative since the SCRHASH syntax above is special.
	SCR_REGISTER_SECURE(WAIT,0x4ede34fbadd967a6,Wait);
	SCR_REGISTER_SECURE(START_NEW_SCRIPT,0xe81651ad79516e48,StartNewScript);
	SCR_REGISTER_SECURE(START_NEW_SCRIPT_WITH_ARGS,0xb8ba7f44df1575e1,StartNewScriptWithArgs);
	SCR_REGISTER_SECURE(START_NEW_SCRIPT_WITH_NAME_HASH,0xeb1c67c3a5333a92,StartNewScriptWithNameHash);
	SCR_REGISTER_SECURE(START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS,0xc4bb298bd441be78,StartNewScriptWithNameHashAndArgs);
	SCR_REGISTER_UNUSED(PRINTLN,0x4be6713cecfcfff3, PrintLnVarargs);
	SCR_REGISTER_UNUSED(ASSERTLN,0xcf8d79ecfcf47473, AssertLnVarargs);
	SCR_REGISTER_UNUSED(CLOGLN,0xef256ae8a5a27966,CLogLn)
	SCR_REGISTER_UNUSED(CPRINTLN,0xf0783374333fd8ce,CPrintLn);
	SCR_REGISTER_UNUSED(CWARNINGLN,0x9a6c65dddbec9c52, CWarningLn);
	SCR_REGISTER_UNUSED(CERRORLN,0xd9911c7b5f8cd69c, CErrorLn);
	SCR_REGISTER_UNUSED(CASSERTLN,0x83407b92d46f25c3, CAssertLn);
	SCR_REGISTER_UNUSED(CDEBUG1LN,0xa308f935bdeccec0, CDebug1Ln);
	SCR_REGISTER_UNUSED(CDEBUG2LN,0x4dc69742196f818a, CDebug2Ln);
	SCR_REGISTER_UNUSED(CDEBUG3LN,0x1b08d1eb9d8c4931, CDebug3Ln);
#endif

	SCR_REGISTER_SECURE(TIMERA,0x83666f9fb8febd4b,TimerA);
	SCR_REGISTER_SECURE(TIMERB,0xc9d9444186b5a374,TimerB);
	SCR_REGISTER_SECURE(SETTIMERA,0xc1b1e9a034a63a62,SetTimerA);
	SCR_REGISTER_SECURE(SETTIMERB,0x5ae11bc36633de4e,SetTimerB);
	SCR_REGISTER_SECURE(TIMESTEP,0x50597ee2,TimeStep);
	SCR_REGISTER_UNUSED(TIMESTEPUNWARPED,0x99b02b53,TimeStepUnwarped);
	SCR_REGISTER_UNUSED(PRINTSTRING,0x98119e8aebfadf2e,PrintString);
	SCR_REGISTER_UNUSED(PRINTFLOAT,0x8288714b173c1042,PrintFloat);
	SCR_REGISTER_UNUSED(PRINTFLOAT2,0x0b38d631ec77e0db,PrintFloat2);
	SCR_REGISTER_UNUSED(PRINTINT,0x9192b022dbbaf2b4,PrintInt);
	SCR_REGISTER_UNUSED(PRINTINT2,0x5e6b1c5242268fe0,PrintInt2);
	SCR_REGISTER_UNUSED(PRINTNL,0x096bf1fe3e5d27d2, PrintNl);
	SCR_REGISTER_UNUSED(PRINTVECTOR,0x5f5f8ee44dd2ff4e, PrintVector);
	SCR_REGISTER_UNUSED(DEBUG_PRINTCALLSTACK,0x355e72323aee83cc, PrintCallstack);
	SCR_REGISTER_UNUSED(BREAKPOINT,0x0c2977d4e0d7f26c, BreakPoint);
#if RSG_PROSPERO || RSG_SCARLETT
	SCR_REGISTER_UNUSED(ROCKY_TAG, 0xa59c19b532027a23, RockyTag);
#endif //RSG_PROSPERO || RSG_SCARLETT

	SCR_REGISTER_SECURE(SIN,0x0badbfa3b172435f,Sin);
	SCR_REGISTER_SECURE(COS,0xd0ffb162f40a139c,Cos);
	SCR_REGISTER_SECURE(SQRT,0x71d93b57d07f9804,Sqrt);
	SCR_REGISTER_SECURE(POW,0xe3621cc40f31fe2e,Pow);
	SCR_REGISTER_SECURE(LOG10,0xe816e655de37fe20,Log10);
	SCR_REGISTER_SECURE(VMAG,0x652d2eeef1d3e62c,VMag); 
	SCR_REGISTER_SECURE(VMAG2,0xa8ceacb4f35ae058,VMag2);
	SCR_REGISTER_SECURE(VDIST,0x2a488c176d52cca5,VDist);
	SCR_REGISTER_SECURE(VDIST2,0xb7a628320eff8e47,VDist2);
	SCR_REGISTER_SECURE(SHIFT_LEFT,0xedd95a39e5544de8,ShiftL);
	SCR_REGISTER_SECURE(SHIFT_RIGHT,0x97ef1e5bce9dc075,ShiftR);
	SCR_REGISTER_SECURE(FLOOR,0xf34ee736cf047844,Floor);
	SCR_REGISTER_SECURE(CEIL,0x11e019c8f43acc8a,Ceil);
	SCR_REGISTER_SECURE(ROUND,0xf2db717a73826179,Round);
	SCR_REGISTER_SECURE(TO_FLOAT,0xbbda792448db5a89,ToFloat);
	SCR_REGISTER_UNUSED(CLEAR_TEXT_LABEL,0x0a69342469658fd3,ClearTextLabel);
	SCR_REGISTER_UNUSED(GET_LATEST_CONSOLE_COMMAND,0x1e83f317118b814c,GetLatestConsoleCommand);
	SCR_REGISTER_UNUSED(RESET_LATEST_CONSOLE_COMMAND,0x5e9dab9a1203fff6,ResetLatestConsoleCommand);
	SCR_REGISTER_UNUSED(GET_CONSOLE_COMMAND_TOKEN,0xab31250f6a376b07,GetConsoleCommandToken);
	SCR_REGISTER_UNUSED(GET_NUM_CONSOLE_COMMAND_TOKENS,0x4afd729dfad4119e,GetNumConsoleCommandTokens);
	SCR_REGISTER_SECURE(SET_THIS_THREAD_PRIORITY,0x42b65deef2edf2a1,SetThisThreadPriority);
	SCR_REGISTER_UNUSED(GET_THIS_THREAD_PRIORITY,0x02c7f121b3b2f9ca,GetThisThreadPriority);

#if !__NO_OUTPUT
	SCR_REGISTER_UNUSED(REGISTER_SCRIPT_CHANNEL,0x1bf3433758178133,RegisterScriptChannel);
	SCR_REGISTER_UNUSED(GET_SCRIPT_CHANNEL_LEVEL,0xa54a5969c06e2354,GetScriptChannelLevel);
	SCR_REGISTER_UNUSED(SET_SCRIPT_CHANNEL_LEVEL,0xc626c7753808559e,SetScriptChannelLevel);
#endif

#if !__FINAL
	// add a command to the console:
	bkConsole::CommandFunctor functor;
	functor.Reset<&sSetLastestConsoleCommand>();
	bkConsole::AddCommand("script",functor);
	functor.Reset<&sSetScriptGlobalVar>();
	bkConsole::AddCommand("set_global_var",functor);
	bkConsole::AddCommand("sgv",functor);
	functor.Reset<&sGetScriptGlobalVar>();
	bkConsole::AddCommand("get_global_var",functor);
	bkConsole::AddCommand("ggv",functor);
	functor.Reset<&sCallNativeFunction>();
	bkConsole::AddCommand("call", functor);
	functor.Reset<&sPrintNativeFuncRegs>();
	bkConsole::AddCommand("regs", functor);
#endif
#if SCRIPT_DEBUGGING
	functor.Reset<&sGetScriptBreakOnFunc>();
	bkConsole::AddCommand("scrbreak",functor);

	functor.Reset<&sGetScriptTraceFunc>();
	bkConsole::AddCommand("scrtrace",functor);

	// Create the native trace bank
	ms_pNativeTraceBank = &BANKMGR.CreateBank("Script Native Trace", 0, 0, false); 
	if(scrVerifyf(ms_pNativeTraceBank, "Failed to create Script native trace bank"))
	{
		ms_pCreateNativeTraceButton = ms_pNativeTraceBank->AddButton("Create native trace widgets", &scrThread::CreateNativeTraceBank);
	}
#endif
}


bool scrThread::LoadGlobalVariableInfo(const char *filename) {
	fiStream *S = ASSET.Open(filename,"sgv");
	if (!S)
		return false;
	int magic;
	S->ReadInt(&magic,1);
	if (magic != MAKE_MAGIC_NUMBER('s','g','v','0')) {
		S->Close();
		return false;
	}
	int count = (S->Size() - sizeof(int)) / sizeof(Global);
	sm_Globals.Reset();
	sm_Globals.Resize(count);
	S->ReadInt(&sm_Globals[0].BasicType,count*(sizeof(Global)/sizeof(int)));
	S->Close();
	return true;
}


int scrThread::LookupGlobalVariable(const char *name) {
	u32 hash = scrComputeHash(name);
	for (int i=0; i<sm_Globals.GetCount(); i++)
		if (hash == sm_Globals[i].Hash)
			return i;
	return -1;
}


scrValue* scrThread::GetGlobalVariableAddress(int idx) {
	return GetGlobal(sm_Globals[idx].Offset);
}




#if __DEV
#include <stdio.h>

/*
	PURPOSE
		Provides instruction-level disassembly of the underlying virtual machine
	PARAMS
		dest - Destination buffer (size is determined by length of longest script
			name or longest command name)
		program - Index of program number to disassemble
		pc - Program counter within specified program
	RETURNS
		Program counter value of next instruction
	NOTES
		The disassembly buffer will not have a carriage return in it	
*/
int scrThread::Disassemble(char *dest,scrProgramId program,int pc) {
	const scrProgram &opcodes = *scrProgram::GetProgram(program);
	DisassembleInsn(dest,opcodes,pc);
	return pc + GetInstructionSize(opcodes,pc);
}


void scrThread::DumpState(void (*printer)(const char *,...)) {
	char curLine[128];
	Disassemble(curLine,m_Serialized.m_Prog,m_Serialized.m_PC);
	printer("%s",curLine);
	printer("PC=%04x FP=%d SP=%d",m_Serialized.m_PC,m_Serialized.m_FP,m_Serialized.m_SP);
	for (int i=0; i<m_Serialized.m_SP; i++) {
		int mantissa = m_Stack[i].Int & 0x7F800000;
		if (mantissa == 0 || mantissa == 0x7F800000)
			printer("  %p %02d.  %d [int]",&m_Stack[i],i,m_Stack[i].Int);
		else if (m_Stack[i].Reference >= &m_Stack[0] && m_Stack[i].Reference <= &m_Stack[m_Serialized.m_StackSize-1])
			printer("  %p %02d.  %p [local]",&m_Stack[i],i,m_Stack[i].Reference);
		else
			printer("  %p %02d.  %f [float]",&m_Stack[i],i,m_Stack[i].Float);
	}
}

#endif

// threadrun.cpp used to be in a different file so we could use some nonstandard compilation options.
// Made the code fragile for relatively little additional benefit.
#undef LoadImm32
#undef LoadImm24
#undef LoadImm16
#undef LoadImmS16
#undef LoadImm8

#define Fault(string,...) do { if (s_CurrentThread) { s_CurrentThread->m_Serialized.m_PC = ptrdiff_t_to_int(pc - opcodes); } diagLoggedPrintf("FAULT: " string " (%s pc = %" PTRDIFFTFMT "d)\n",##__VA_ARGS__,s_ScriptName,pc - opcodes); FaultCommon(opcodesTbl,ser); return ser->m_State = ABORTED; } while (0)
#define FaultQuit(string,...) do { if (s_CurrentThread) { s_CurrentThread->m_Serialized.m_PC = ptrdiff_t_to_int(pc - opcodes); } Quitf(ERR_DEFAULT,"FAULT: " string " (%s pc = %" PTRDIFFTFMT "d)\n",##__VA_ARGS__,s_ScriptName,pc - opcodes); FaultCommon(opcodesTbl,ser); return ser->m_State = ABORTED; } while (0)


#define LoadImm32	((pc+=4), *(u32*)(pc-3))
#if defined(__SNC__) || defined(_XBOX_VER)
#define LoadImm24	((pc+=3), *(u32*)(pc-3) & 0xFFFFFF)
#else
#define LoadImm24	((pc+=3), *(u32*)(pc-3) >> 8)
#endif
#define LoadImm16	((pc+=2), *(u16*)(pc-1))
#define LoadImmS16	((pc+=2), *(s16*)(pc-1))
#define LoadImm8	(*++pc)

#define THREADED_INTERPRETER	(RSG_ORBIS)		// Both gcc and snc support computed goto's.

#if __DEV || __BANK
// Pick a reasonable value that fits in a PPC compare immediate insn.
#define VALIDATE_PC(target) do { if (target < 4) Fault("Impossible jump target caught"); } while (0)
#define VALIDATE_PTR(value)	do { if (value.Uns < 16384) Fault("Invalid reference caught"); } while (0)
#else
#define VALIDATE_PC(target)
#define VALIDATE_PTR(value)
#endif

#if STREAMABLE_PROGRAMS
#define SET_PC(_o)	do { intptr_t o = _o; pc = (opcodesTbl[o>>scrPageShift] + (o & scrPageMask) - 1); opcodes = pc - o; } while (0)
#define ADD_PC(_r)	SET_PC((pc - opcodes) + (_r))
#define CHECK_PC	SET_PC(pc - opcodes)
#else
#define SET_PC(_o) (pc = opcodes + (_o))
#define ADD_PC(_r) (pc += (_r))
#define CHECK_PC
#endif

#if defined(SCRIPT_VALUE_H)
#define DEC_CALLDEPTH --(ser->m_CallDepth)
#else
#define DEC_CALLDEPTH
#endif

static void scr_assign_string(char *dst,unsigned siz,const char *src)
{
	if (src)
	{
		while (*src && --siz)
			*dst++ = *src++;
	}
	*dst = '\0';
}

static void scr_append_string(char *dst,unsigned siz,const char *src)
{
	while (*dst)
		dst++, --siz;
	scr_assign_string(dst,siz,src);
}

static void scr_itoa(char *dest,int value)
{
	char stack[16], *sp = stack;
	if (value < 0) 
	{
		*dest++ = '-';
		value = -value;
	}
	else if (!value) 
	{
		dest[0] = '0';
		dest[1] = 0;
		return;
	}
	while (value)
	{
		*sp++ = (char)((value % 10) + '0');
		value /= 10;
	}
	while (sp != stack)
	{
		*dest++ = *--sp;
	}
	*dest = 0;
}

void scrThread::FaultCommon(const u8 **NOTFINAL_ONLY(opcodesTbl),scrThread::Serialized * __restrict NOTFINAL_ONLY(ser))
{
#if !__FINAL
	while (ser->m_CallDepth--)
		if (ser->m_CallDepth < MAX_CALLSTACK) {
			int o = ser->m_CallStack[ser->m_CallDepth];
			diagLoggedPrintf(">>>%s\n",(opcodesTbl[o>>scrPageShift] + (o & scrPageMask) - 1));
		}
	Quitf("Script fault (usually an array overrun, assign to Default Levels; see log file for details)");
#endif
}

#if RSG_ORBIS
static __forceinline void __attribute__((__always_inline__, __nodebug__))
__stosq(size_t*__dst, size_t __x, size_t __n)
{
	__asm__("rep stosq" : : "D"(__dst), "a"(__x), "c"(__n) : "%rdi", "%rcx");
}
#endif

u32 scrThread::Run(scrValue * __restrict stack,scrValue ** __restrict globals,const scrProgram &pt,Serialized * __restrict ser)
{
	const u8 **opcodesTbl = const_cast<const u8**>(pt.Table);

	scrValue *sp = stack + ser->m_SP - 1;
	scrValue *fp = stack + ser->m_FP;
	const u8 *pc;
#if STREAMABLE_PROGRAMS
	const u8 *opcodes;
#else
	--opcodes;			// so we don't have to subtract one constantly
#endif
	SET_PC(ser->m_PC);
	char buf[16];
#if !__FINAL && !THREADED_INTERPRETER
	unsigned insnLimit = 1000000000;
#endif

#if USE_PROFILER_NORMAL
	ScriptEventDescriptionStorage* storage = nullptr;
	if (Profiler::IsCollectingEvents(Profiler::Mode::SCRIPTS)) {
		storage = g_eventDescriptions.GetOrCreateStorage(pt);
	}
	RockyStackEventsGuard rockyStackGuard;
	(void)rockyStackGuard;
#endif

	// sp[0] is TOS, sp[-1] is next down
	// Push -> *++sp = (x)
	// Pop -> *sp--;
	// unsigned st0, st1, st2, st3;

#if THREADED_INTERPRETER
	static void *jumpTable[256] = {
#define OP(a,b,c) &&LABEL_OP_##a
#include "opcodes.h"
#undef OP
		// This needs to be adjusted if we add or remove opcodes:
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,

		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,

		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,

		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
		&&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default, &&LABEL_default,
	};
	// FETCH_INSN must appear after all LoadImm... opcodes or any references to 'pc'
#define CASE(x) LABEL_##x:
#define DEFAULT LABEL_default:
# if 1
	void *ni;
# define FETCH_INSN ni = jumpTable[LoadImm8]
# define NEXT_INSN goto *ni
# else
# define FETCH_INSN
# define NEXT_INSN goto *jumpTable[LoadImm8]
# endif
#else
#define CASE(x) case x:
#define DEFAULT default:
#define FETCH_INSN
#define NEXT_INSN goto DISPATCH
DISPATCH:
#endif

	{
#if FULL_LOGGING
		LogThreadState(stack,stackMax,globals,globalsMax,sp-stack+1,fp-stack,opcodes+1,opcodesMax,pc-opcodes);
#endif
#if !__FINAL && !THREADED_INTERPRETER
		if (!--insnLimit)
			Fault("Script is stuck in a loop?");
#endif

		// printf("pc %x opcode %d sp %x fp %x\n",pc-opcodes,pc[1],ser->m_SP,ser->m_FP);
		// Displayf("%x: %x SP=%p FP=%p TOS=%x %x %x %x",pc,opcode,sp,ser->m_FP,sp[0].Int,sp[-1].Int,sp[-2].Int,sp[-3].Int);
#if THREADED_INTERPRETER
		FETCH_INSN;
		NEXT_INSN;		// start the threaded interpreter
#else

		switch (LoadImm8) 
		{
#endif
			CASE(OP_NOP) CHECK_PC; FETCH_INSN; NEXT_INSN;
			CASE(OP_IADD) FETCH_INSN; --sp; sp[0].Int += sp[1].Int; NEXT_INSN;
			CASE(OP_ISUB) FETCH_INSN; --sp; sp[0].Int -= sp[1].Int; NEXT_INSN;
			CASE(OP_IMUL) FETCH_INSN; --sp; sp[0].Int *= sp[1].Int; NEXT_INSN;
			CASE(OP_IDIV) FETCH_INSN; --sp; if (sp[1].Int) sp[0].Int /= sp[1].Int; NEXT_INSN;
			CASE(OP_IMOD) FETCH_INSN; --sp; if (sp[1].Int) sp[0].Int %= sp[1].Int; NEXT_INSN;
			CASE(OP_INOT) FETCH_INSN; sp[0].Int = !sp[0].Int; NEXT_INSN;
			CASE(OP_INEG) FETCH_INSN; sp[0].Int = -sp[0].Int; NEXT_INSN;

			CASE(OP_IEQ) FETCH_INSN; --sp; sp[0].Int = sp[0].Int == sp[1].Int; NEXT_INSN;
			CASE(OP_INE) FETCH_INSN; --sp; sp[0].Int = sp[0].Int != sp[1].Int; NEXT_INSN;
			CASE(OP_IGE) FETCH_INSN; --sp; sp[0].Int = sp[0].Int >= sp[1].Int; NEXT_INSN;
			CASE(OP_IGT) FETCH_INSN; --sp; sp[0].Int = sp[0].Int >  sp[1].Int; NEXT_INSN;
			CASE(OP_ILE) FETCH_INSN; --sp; sp[0].Int = sp[0].Int <= sp[1].Int; NEXT_INSN;
			CASE(OP_ILT) FETCH_INSN; --sp; sp[0].Int = sp[0].Int <  sp[1].Int; NEXT_INSN;

			CASE(OP_FADD) FETCH_INSN; --sp; sp[0].Float += sp[1].Float; NEXT_INSN;
			CASE(OP_FSUB) FETCH_INSN; --sp; sp[0].Float -= sp[1].Float; NEXT_INSN;
			CASE(OP_FMUL) FETCH_INSN; --sp; sp[0].Float *= sp[1].Float; NEXT_INSN;
			CASE(OP_FDIV) FETCH_INSN; --sp; if (sp[1].Int) sp[0].Float /= sp[1].Float; NEXT_INSN;
			CASE(OP_FMOD) FETCH_INSN; --sp; if (sp[1].Int) sp[0].Float = scr_fmodf(sp[0].Float,sp[1].Float); NEXT_INSN;
			CASE(OP_FNEG) FETCH_INSN; sp[0].Uns ^= 0x80000000; NEXT_INSN;

			CASE(OP_FEQ) FETCH_INSN; --sp; sp[0].Int = sp[0].Float == sp[1].Float; NEXT_INSN;	// TODO: Use integer compare?
			CASE(OP_FNE) FETCH_INSN; --sp; sp[0].Int = sp[0].Float != sp[1].Float; NEXT_INSN;	// TODO: Use integer compare?
			CASE(OP_FGE) FETCH_INSN; --sp; sp[0].Int = sp[0].Float >= sp[1].Float; NEXT_INSN;
			CASE(OP_FGT) FETCH_INSN; --sp; sp[0].Int = sp[0].Float >  sp[1].Float; NEXT_INSN;
			CASE(OP_FLE) FETCH_INSN; --sp; sp[0].Int = sp[0].Float <= sp[1].Float; NEXT_INSN;
			CASE(OP_FLT) FETCH_INSN; --sp; sp[0].Int = sp[0].Float <  sp[1].Float; NEXT_INSN;

			CASE(OP_VADD) FETCH_INSN; sp-=3; sp[-2].Float += sp[1].Float; sp[-1].Float += sp[2].Float; sp[0].Float += sp[3].Float; NEXT_INSN;
			CASE(OP_VSUB) FETCH_INSN; sp-=3; sp[-2].Float -= sp[1].Float; sp[-1].Float -= sp[2].Float; sp[0].Float -= sp[3].Float; NEXT_INSN;
			CASE(OP_VMUL) FETCH_INSN; sp-=3; sp[-2].Float *= sp[1].Float; sp[-1].Float *= sp[2].Float; sp[0].Float *= sp[3].Float; NEXT_INSN;
			CASE(OP_VDIV) FETCH_INSN; sp-=3; if (sp[1].Int) sp[-2].Float /= sp[1].Float; if (sp[2].Int) sp[-1].Float /= sp[2].Float; if (sp[3].Int) sp[0].Float /= sp[3].Float; NEXT_INSN;
			CASE(OP_VNEG) FETCH_INSN; sp[-2].Uns ^= 0x80000000; sp[-1].Uns ^= 0x80000000; sp[0].Uns ^= 0x80000000; NEXT_INSN;

			CASE(OP_IAND) FETCH_INSN; --sp; sp[0].Int &= sp[1].Int; NEXT_INSN;
			CASE(OP_IOR)  FETCH_INSN; --sp; sp[0].Int |= sp[1].Int; NEXT_INSN;
			CASE(OP_IXOR) FETCH_INSN; --sp; sp[0].Int ^= sp[1].Int; NEXT_INSN;

			CASE(OP_I2F) FETCH_INSN; sp[0].Float = (float) sp[0].Int; NEXT_INSN;
			CASE(OP_F2I) FETCH_INSN; sp[0].Int = (int) sp[0].Float; NEXT_INSN;
			CASE(OP_F2V) FETCH_INSN; sp+=2; sp[-1].Int = sp[0].Int = sp[-2].Int; NEXT_INSN;

			CASE(OP_PUSH_CONST_U8) ++sp; sp[0].Int = LoadImm8; FETCH_INSN; NEXT_INSN;
			CASE(OP_PUSH_CONST_U8_U8) sp+=2; sp[-1].Int = LoadImm8; sp[0].Int = LoadImm8; FETCH_INSN; NEXT_INSN;
			CASE(OP_PUSH_CONST_U8_U8_U8) sp+=3; sp[-2].Int = LoadImm8; sp[-1].Int = LoadImm8; sp[0].Int = LoadImm8; FETCH_INSN; NEXT_INSN;
			
			CASE(OP_PUSH_CONST_U32) 
			CASE(OP_PUSH_CONST_F) ++sp; sp[0].Uns = LoadImm32; FETCH_INSN; NEXT_INSN;

			CASE(OP_DUP) FETCH_INSN; ++sp; sp[0].Any = sp[-1].Any; NEXT_INSN;
			CASE(OP_DROP) FETCH_INSN; --sp; NEXT_INSN;

			CASE(OP_NATIVE)
				{
					int returnSize = LoadImm8;
					int paramCount = (returnSize >> 2) & 63;
					u32 imm = (LoadImm8 << 8);
					imm |= LoadImm8;
					returnSize &= 3;
					scrCmd cmd = (scrCmd)(size_t)pt.Natives[imm];
					// TrapGE(imm,pt.NativeSize);
					// Preserve state so it's correct if the function blocked, and more importantly, in case
					// the function does anything nasty like subclass our thread class and access these directly.
					ser->m_PC = ptrdiff_t_to_int(pc-opcodes-c_NativeInsnLength);
					ser->m_FP = ptrdiff_t_to_int(fp - stack);
					ser->m_SP = ptrdiff_t_to_int(sp - stack + 1);
					Info curInfo(returnSize? &stack[ser->m_SP - paramCount] : 0, paramCount, &stack[ser->m_SP - paramCount]);
					(*cmd)(curInfo);
					if (ser->m_State != RUNNING)
							return ser->m_State;
						// If we didn't block, copy referenced parameters back into caller's stack.
						curInfo.CopyReferencedParametersOut();
						// Drop call parameters off of the stack but leave any return result there.
						sp -= paramCount - returnSize;
					FETCH_INSN; 
					NEXT_INSN;
				}

			CASE(OP_ENTER)
				{
					u32 paramCount = LoadImm8;
					u32 localCount = LoadImm16;
					u32 nameCount = LoadImm8;

#if USE_PROFILER_NORMAL
					TryAutoSample(*ser);
					if (storage != nullptr) {
						u16 index = nameCount ? *(u16*)(pc + 1) : (u16)-1;
						Profiler::StackedEvent::StackPush(storage->GetDescription(index));
					}
#endif

					if (ser->m_CallDepth < MAX_CALLSTACK)
						ser->m_CallStack[ser->m_CallDepth] = ptrdiff_t_to_int(pc - opcodes + 2);
					++(ser->m_CallDepth);
					pc += nameCount;
					FETCH_INSN; 
					if (sp - stack >= (int)(ser->m_StackSize - localCount))
						Fault("Stack overflow");
					(++sp)->Int = ptrdiff_t_to_int(fp - stack);
					fp = sp - paramCount - 1;

#if RSG_ORBIS
					 __stosq(&sp[1].Any, 0, localCount);
					 sp += localCount;
#else
					while (localCount--)
						(++sp)->Any = 0;
#endif
					sp -= paramCount;
					NEXT_INSN;
				}

			CASE(OP_LEAVE)
				{
					DEC_CALLDEPTH;
#if USE_PROFILER_NORMAL
					TryAutoSample(*ser);
					if (storage != nullptr) {
						Profiler::StackedEvent::StackPop();
					}
#endif 

					u32 paramCount = LoadImm8;	// Get number of parameters to clean off the stack
					u32 returnSize = LoadImm8;	// Get size of the return value
					scrValue *result = sp - returnSize;	// Remember start of return value
					sp = fp + paramCount - 1;
					fp = stack + sp[2].Uns;
					u32 newPc = sp[1].Uns;		// Pull return address off of the stack placed by OP_CALL/OP_CALLINDIRECT;
					SET_PC(newPc);
					sp -= paramCount;				// Drop the caller's parameters
					// Copy the return value to the new top-of-stack
					while (returnSize--)
						*++sp = *++result;
					if (!newPc)				
						return ser->m_State = ABORTED;
					FETCH_INSN; 
					NEXT_INSN;
				}

			CASE(OP_LOAD) FETCH_INSN; VALIDATE_PTR(sp[0]); sp[0].Any = scrDecodeReference(sp[0].Reference)->Any; NEXT_INSN;
			CASE(OP_STORE) FETCH_INSN; sp-=2; VALIDATE_PTR(sp[2]); scrDecodeReference(sp[2].Reference)->Any = sp[1].Any; NEXT_INSN;
			CASE(OP_STORE_REV) FETCH_INSN; --sp; VALIDATE_PTR(sp[0]); scrDecodeReference(sp[0].Reference)->Any = sp[1].Any; NEXT_INSN;

			CASE(OP_LOAD_N) 
				{ 
					FETCH_INSN; 
					scrValue *addr = scrDecodeReference((sp--)->Reference);
					u32 count = (sp--)->Int;
					for (u32 i=0; i<count; i++)
						(++sp)->Any = addr[i].Any;
					NEXT_INSN; 
				}

			CASE(OP_STORE_N)
				{
					FETCH_INSN; 
					// Values are pushed up to the stack in evaluation order,
					// so reverse them when storing
					scrValue *addr = scrDecodeReference((sp--)->Reference);
					u32 count = (sp--)->Int;
					for (u32 i=0; i<count; i++)
						addr[count-1-i].Any = (sp--)->Any;
					NEXT_INSN; 
				}

			CASE(OP_ARRAY_U8)
				{
					--sp;
					scrValue *r = scrDecodeReference(sp[1].Reference);
					u32 idx = sp[0].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm8;
					FETCH_INSN; 
					sp[0].Reference = scrEncodeReference(r);
					NEXT_INSN;
				}

			CASE(OP_ARRAY_U8_LOAD)
				{
					--sp;
					scrValue *r = scrDecodeReference(sp[1].Reference);
					u32 idx = sp[0].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm8;
					FETCH_INSN; 
					sp[0].Any = r->Any;
					NEXT_INSN;
				}

			CASE(OP_ARRAY_U8_STORE)
				{
					sp-=3; 
					scrValue *r = scrDecodeReference(sp[3].Reference);
					u32 idx = sp[2].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm8;
					FETCH_INSN; 
					r->Any = sp[1].Any;
					NEXT_INSN;
				}

			CASE(OP_LOCAL_U8) ++sp; sp[0].Reference = scrEncodeReference(fp + LoadImm8); FETCH_INSN; NEXT_INSN;
			CASE(OP_LOCAL_U8_LOAD) ++sp; sp[0].Any = fp[LoadImm8].Any; FETCH_INSN; NEXT_INSN;
			CASE(OP_LOCAL_U8_STORE) --sp; fp[LoadImm8].Any = sp[1].Any; FETCH_INSN; NEXT_INSN;

			CASE(OP_STATIC_U8) ++sp; sp[0].Reference = scrEncodeReference(stack + LoadImm8); FETCH_INSN; NEXT_INSN;
			CASE(OP_STATIC_U8_LOAD) ++sp; sp[0].Any = stack[LoadImm8].Any; FETCH_INSN; NEXT_INSN;
			CASE(OP_STATIC_U8_STORE) --sp; stack[LoadImm8].Any = sp[1].Any; FETCH_INSN; NEXT_INSN;

			CASE(OP_IADD_U8) sp[0].Int += LoadImm8; FETCH_INSN; NEXT_INSN;
			CASE(OP_IMUL_U8) sp[0].Int *= LoadImm8; FETCH_INSN; NEXT_INSN;

			CASE(OP_IOFFSET) FETCH_INSN; --sp; sp[0].Any += sp[1].Int * sizeof(scrValue); NEXT_INSN;
			CASE(OP_IOFFSET_U8) sp[0].Any += LoadImm8 * sizeof(scrValue); FETCH_INSN; NEXT_INSN;
			CASE(OP_IOFFSET_U8_LOAD) VALIDATE_PTR(sp[0]); sp[0].Any = scrDecodeReference(sp[0].Reference)[LoadImm8].Any; FETCH_INSN; NEXT_INSN;
			CASE(OP_IOFFSET_U8_STORE) sp-=2; VALIDATE_PTR(sp[2]); scrDecodeReference(sp[2].Reference)[LoadImm8].Any = sp[1].Any; FETCH_INSN; NEXT_INSN;

			CASE(OP_PUSH_CONST_S16) ++sp; sp[0].Int = LoadImmS16; FETCH_INSN; NEXT_INSN;
			CASE(OP_IADD_S16) sp[0].Int += LoadImmS16; FETCH_INSN; NEXT_INSN;
			CASE(OP_IMUL_S16) sp[0].Int *= LoadImmS16; FETCH_INSN; NEXT_INSN;

			CASE(OP_IOFFSET_S16) sp[0].Any += LoadImmS16 * sizeof(scrValue); FETCH_INSN; NEXT_INSN;
			CASE(OP_IOFFSET_S16_LOAD) VALIDATE_PTR(sp[0]); sp[0].Any = scrDecodeReference(sp[0].Reference)[LoadImmS16].Any; FETCH_INSN; NEXT_INSN;
			CASE(OP_IOFFSET_S16_STORE) sp-=2; VALIDATE_PTR(sp[2]); scrDecodeReference(sp[2].Reference)[LoadImmS16].Any = sp[1].Any; FETCH_INSN; NEXT_INSN;

			CASE(OP_ARRAY_U16)
				{
					--sp;
					scrValue *r = scrDecodeReference(sp[1].Reference);
					u32 idx = sp[0].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm16;
					FETCH_INSN; 
					sp[0].Reference = scrEncodeReference(r);
					NEXT_INSN;
				}

			CASE(OP_ARRAY_U16_LOAD)
				{
					--sp;
					scrValue *r = scrDecodeReference(sp[1].Reference);
					u32 idx = sp[0].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm16;
					FETCH_INSN; 
					sp[0].Any = r->Any;
					NEXT_INSN;
				}

			CASE(OP_ARRAY_U16_STORE)
				{
					sp-=3; 
					scrValue *r = scrDecodeReference(sp[3].Reference);
					u32 idx = sp[2].Uns;
					if (idx >= r->Uns)
						Fault("Array overrun, %u >= %u",idx,r->Uns);
					r += 1U + idx * LoadImm16;
					FETCH_INSN; 
					r->Any = sp[1].Any;
					NEXT_INSN;
				}

			CASE(OP_LOCAL_U16) ++sp; sp[0].Reference = scrEncodeReference(fp + LoadImm16); FETCH_INSN; NEXT_INSN;
			CASE(OP_LOCAL_U16_LOAD) ++sp; sp[0].Any = fp[LoadImm16].Any; FETCH_INSN; NEXT_INSN;
			CASE(OP_LOCAL_U16_STORE) --sp; fp[LoadImm16].Any = sp[1].Any; FETCH_INSN; NEXT_INSN;

			CASE(OP_STATIC_U16) ++sp; sp[0].Reference = scrEncodeReference(stack + LoadImm16); FETCH_INSN; NEXT_INSN;
			CASE(OP_STATIC_U16_LOAD) ++sp; sp[0].Any = stack[LoadImm16].Any; FETCH_INSN; NEXT_INSN;
			CASE(OP_STATIC_U16_STORE) --sp; stack[LoadImm16].Any = sp[1].Any; FETCH_INSN; NEXT_INSN;

			CASE(OP_GLOBAL_U16) ++sp; sp[0].Reference = scrEncodeReference(globals[0] + LoadImm16); FETCH_INSN; NEXT_INSN;
			CASE(OP_GLOBAL_U16_LOAD) ++sp; sp[0].Any = globals[0][LoadImm16].Any; FETCH_INSN; NEXT_INSN;
			CASE(OP_GLOBAL_U16_STORE) --sp; globals[0][LoadImm16].Any = sp[1].Any; FETCH_INSN; NEXT_INSN;

			CASE(OP_STATIC_U24) ++sp; sp[0].Reference = scrEncodeReference(stack + LoadImm24); FETCH_INSN; NEXT_INSN;
			CASE(OP_STATIC_U24_LOAD) ++sp; sp[0].Any = stack[LoadImm24].Any; FETCH_INSN; NEXT_INSN;
			CASE(OP_STATIC_U24_STORE) --sp; stack[LoadImm24].Any = sp[1].Any; FETCH_INSN; NEXT_INSN;

			CASE(OP_GLOBAL_U24) 
				{ 
					u32 imm = LoadImm24, block = imm >> scrProgram::MAX_GLOBAL_BLOCKS_SHIFT, ofs = imm & scrProgram::GLOBAL_SIZE_MASK; 
					++sp; 
					if (!globals[block]) 
						Fault("Global block %u/%u not resident",block,ofs); 
					else
						sp[0].Reference = scrEncodeReference(&globals[block][ofs]); 
					FETCH_INSN; NEXT_INSN; 
				}
			CASE(OP_GLOBAL_U24_LOAD) 
				{ 
					u32 imm = LoadImm24, block = imm >> scrProgram::MAX_GLOBAL_BLOCKS_SHIFT, ofs = imm & scrProgram::GLOBAL_SIZE_MASK; 
					++sp; 
					if (!globals[block]) 
						Fault("Global block %u/%u not resident",block,ofs); 
					else
						sp[0].Any = globals[block][ofs].Any; 
					FETCH_INSN; NEXT_INSN; 
				}
			CASE(OP_GLOBAL_U24_STORE) 
				{ 
					u32 imm = LoadImm24, block = imm >> scrProgram::MAX_GLOBAL_BLOCKS_SHIFT, ofs = imm & scrProgram::GLOBAL_SIZE_MASK; 
					--sp;
					if (!globals[block]) 
						Fault("Global block %u/%u not resident",block,ofs); 
					else
						globals[block][ofs].Any = sp[1].Any; 
					FETCH_INSN; NEXT_INSN; 
				}
			CASE(OP_PUSH_CONST_U24) ++sp; sp[0].Int = LoadImm24; FETCH_INSN; NEXT_INSN;

			CASE(OP_CALL) { u32 imm = LoadImm24; ++sp; sp[0].Uns = ptrdiff_t_to_int(pc - opcodes); SET_PC(imm); FETCH_INSN; NEXT_INSN; }

			CASE(OP_J) { s32 imm = LoadImmS16; ADD_PC(imm); FETCH_INSN; NEXT_INSN; }
			CASE(OP_JZ) { s32 imm = LoadImmS16; --sp; if (sp[1].Int == 0) ADD_PC(imm); else CHECK_PC; FETCH_INSN; NEXT_INSN; }
			CASE(OP_IEQ_JZ) { s32 imm = LoadImmS16; sp-=2; if (!(sp[1].Int == sp[2].Int)) ADD_PC(imm); else CHECK_PC; FETCH_INSN; NEXT_INSN; }
			CASE(OP_INE_JZ) { s32 imm = LoadImmS16; sp-=2; if (!(sp[1].Int != sp[2].Int)) ADD_PC(imm); else CHECK_PC; FETCH_INSN; NEXT_INSN; }
			CASE(OP_IGE_JZ) { s32 imm = LoadImmS16; sp-=2; if (!(sp[1].Int >= sp[2].Int)) ADD_PC(imm); else CHECK_PC; FETCH_INSN; NEXT_INSN; }
			CASE(OP_IGT_JZ) { s32 imm = LoadImmS16; sp-=2; if (!(sp[1].Int >  sp[2].Int)) ADD_PC(imm); else CHECK_PC; FETCH_INSN; NEXT_INSN; }
			CASE(OP_ILE_JZ) { s32 imm = LoadImmS16; sp-=2; if (!(sp[1].Int <= sp[2].Int)) ADD_PC(imm); else CHECK_PC; FETCH_INSN; NEXT_INSN; }
			CASE(OP_ILT_JZ) { s32 imm = LoadImmS16; sp-=2; if (!(sp[1].Int <  sp[2].Int)) ADD_PC(imm); else CHECK_PC; FETCH_INSN; NEXT_INSN; }

			CASE(OP_SWITCH)
				{
					--sp;
					u32 label = sp[1].Uns;
					u32 count = LoadImm8;
					CHECK_PC;
					for (u32 i=0; i<count; i++) {
						u32 match = LoadImm32;
						u32 target = LoadImm16;
						if (label == match) {
							ADD_PC(target);
							break;
						}
					}
					CHECK_PC;
					FETCH_INSN; 
					NEXT_INSN;
				}
			CASE(OP_STRING)
				{
					u32 offset = sp[0].Uns;
					sp[0].String = scrEncodeString(pt.StringHeaps[offset >> scrStringShift] + (offset & scrStringMask));
					FETCH_INSN; 
					NEXT_INSN;
				}

			CASE(OP_TEXT_LABEL_ASSIGN_STRING)
				{
					sp-=2;
					char *dest = const_cast<char*>(scrDecodeString(sp[2].String));
					const char *src = scrDecodeString(sp[1].String);
					scr_assign_string(dest,LoadImm8,src);
					FETCH_INSN; 
					NEXT_INSN;
				}

			CASE(OP_TEXT_LABEL_ASSIGN_INT)
				{
					sp-=2;
					char *dest = const_cast<char*>(scrDecodeString(sp[2].String));
					int value = sp[1].Int;
					scr_itoa(buf,value);
					scr_assign_string(dest,LoadImm8,buf);
					FETCH_INSN; 
					NEXT_INSN;
				}
			CASE(OP_TEXT_LABEL_APPEND_STRING)
				{
					sp-=2;
					char *dest = const_cast<char*>(scrDecodeString(sp[2].String));
					const char *src = scrDecodeString(sp[1].String);
					scr_append_string(dest,LoadImm8,src);
					FETCH_INSN; 
					NEXT_INSN;
				}
			CASE(OP_TEXT_LABEL_APPEND_INT)
				{
					sp-=2;
					char *dest = const_cast<char*>(scrDecodeString(sp[2].String));
					int value = sp[1].Int;
					scr_itoa(buf,value);
					scr_append_string(dest,LoadImm8,buf);
					FETCH_INSN; 
					NEXT_INSN;
				}

			CASE(OP_TEXT_LABEL_COPY)
				{
					FETCH_INSN; 
					sp-=3;
					scrValue *dest = scrDecodeReference(sp[3].Reference);
					int destSize = sp[2].Int;
					int srcSize = sp[1].Int;
					// Remove excess
					while (srcSize > destSize) {
						--srcSize;
						--sp;
					}
					// Do the bulk of the copy
					for (int i=0; i<srcSize; i++)
						dest[srcSize-1-i].Any = (sp--)->Any;
					// Make sure it's still NUL-terminated
					char *cDest = (char*)dest;
					cDest[(srcSize*sizeof(scrValue))-1] = '\0';
					NEXT_INSN;
				}

			CASE(OP_CATCH)
				FETCH_INSN; 
				ser->m_CatchPC = ptrdiff_t_to_int(pc - opcodes);
				ser->m_CatchFP = ptrdiff_t_to_int(fp - stack);
				ser->m_CatchSP = ptrdiff_t_to_int(sp - stack + 1);
				++sp;
				sp[0].Int = -1;
				NEXT_INSN;

			CASE(OP_THROW)
				{
					int imm = sp[0].Int;	// caught value (sp is now wrong but will be replaced below)
					if (!ser->m_CatchPC)
						Fault("THROW with no CATCH");
					else {
						SET_PC(ser->m_CatchPC);
						fp = stack + ser->m_CatchFP;
						sp = stack + ser->m_CatchSP;
						sp[0].Int = imm;
					}
					FETCH_INSN; 
					NEXT_INSN;
				}

			CASE(OP_CALLINDIRECT) 
				{ 
					u32 imm = sp[0].Uns; 
					if (!imm)
						Fault("Attempted to call through uninitialized (zero) function pointer");
					sp[0].Uns = ptrdiff_t_to_int(pc - opcodes); 
					SET_PC(imm); 
					FETCH_INSN; 
					NEXT_INSN; 
				}

			CASE(OP_PUSH_CONST_M1) FETCH_INSN; ++sp; sp[0].Int = -1; NEXT_INSN;
			CASE(OP_PUSH_CONST_0) FETCH_INSN; ++sp; sp[0].Any = 0; NEXT_INSN;
			CASE(OP_PUSH_CONST_1) FETCH_INSN; ++sp; sp[0].Int = 1; NEXT_INSN;
			CASE(OP_PUSH_CONST_2) FETCH_INSN; ++sp; sp[0].Int = 2; NEXT_INSN;
			CASE(OP_PUSH_CONST_3) FETCH_INSN; ++sp; sp[0].Int = 3; NEXT_INSN;
			CASE(OP_PUSH_CONST_4) FETCH_INSN; ++sp; sp[0].Int = 4; NEXT_INSN;
			CASE(OP_PUSH_CONST_5) FETCH_INSN; ++sp; sp[0].Int = 5; NEXT_INSN;
			CASE(OP_PUSH_CONST_6) FETCH_INSN; ++sp; sp[0].Int = 6; NEXT_INSN;
			CASE(OP_PUSH_CONST_7) FETCH_INSN; ++sp; sp[0].Int = 7; NEXT_INSN;

			CASE(OP_PUSH_CONST_FM1) FETCH_INSN; ++sp; sp[0].Uns = 0xbf800000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F0) FETCH_INSN; ++sp; sp[0].Uns = 0x00000000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F1) FETCH_INSN; ++sp; sp[0].Uns = 0x3f800000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F2) FETCH_INSN; ++sp; sp[0].Uns = 0x40000000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F3) FETCH_INSN; ++sp; sp[0].Uns = 0x40400000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F4) FETCH_INSN; ++sp; sp[0].Uns = 0x40800000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F5) FETCH_INSN; ++sp; sp[0].Uns = 0x40a00000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F6) FETCH_INSN; ++sp; sp[0].Uns = 0x40c00000; NEXT_INSN;
			CASE(OP_PUSH_CONST_F7) FETCH_INSN; ++sp; sp[0].Uns = 0x40e00000; NEXT_INSN;

			CASE(OP_STRINGHASH) FETCH_INSN; sp[0].Uns = atStringHash(scrDecodeString(sp[0].String)); NEXT_INSN;

 			CASE(OP_IS_BIT_SET)
	 			{
	 				FETCH_INSN;
	 				--sp;
	 				sp[0].Int = ((sp[0].Int & (1 << sp[1].Int)) != 0);
	 				NEXT_INSN;
	 			}

//DEFAULT Fault("Invalid opcode");
DEFAULT FaultQuit("Invalid opcode");
#if !THREADED_INTERPRETER
		}
#endif
	}
}

#undef Fault


Vector3& scrThread::Info::GetVector3(int &N){
	FastAssert(BufferCount < MAX_VECTOR3);
#if __64BIT
	scrValue *v = scrDecodeReference(Params[N++].Reference);
	Orig[BufferCount] = v;
	Buffer[BufferCount].Set(v[0].Float,v[1].Float,v[2].Float);
#else
	float *v = &Params[N++].Reference->Float;
	Orig[BufferCount] = v;
	Buffer[BufferCount].Set(v[0],v[1],v[2]);
#endif
	return Buffer[BufferCount++];
}

void scrThread::Info::CopyReferencedParametersOut() {
	while (BufferCount--) {
#if __64BIT
		Orig[BufferCount][0].Float = Buffer[BufferCount].x;
		Orig[BufferCount][1].Float = Buffer[BufferCount].y;
		Orig[BufferCount][2].Float = Buffer[BufferCount].z;
#else
		Orig[BufferCount][0] = Buffer[BufferCount].x;
		Orig[BufferCount][1] = Buffer[BufferCount].y;
		Orig[BufferCount][2] = Buffer[BufferCount].z;
#endif
	}
}

