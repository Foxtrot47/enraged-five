#ifdef __SNC__
// ps3ppusnc -mprx -zgenprx --oformat=fsprx -Os -Wl,--notocrestore -I x:\gta5\src\dev\rage\script\src\script whatever.sc.cpp -o path-to-sprx
#pragma control notocrestore=2
#pragma control fastmath=1
#pragma control assumecorrectalignment=1
#pragma control assumecorrectsign=1
#pragma control relaxalias=0
#pragma control checknew=0
#pragma control c+=rtti
#pragma control c-=exceptions
#pragma diag_suppress 112
#pragma diag_suppress 178
#pragma diag_suppress 552
#pragma diag_suppress 953
#define _CPPRTTI 
#endif

#ifdef __GNUG__
#define csupport_inline __attribute__((__always_inline__)) inline
#define csupport_noinline __attribute__((__noinline__))
#else
#define csupport_inline __forceinline
#define csupport_noinline
#endif

#include <setjmp.h>
#include <new>
#include <stdarg.h>

typedef int BOOL;
const int FALSE	= 0;
const int TRUE = 1;

static void scr_assign_string(char *dst,unsigned siz,const char *src);
static void scr_assign_int(char *dst,unsigned siz,int value);
static void scr_append_int(char *dst,unsigned siz,int value);
static void scr_append_string(char *dst,unsigned siz,const char *src);

extern void atStringHash(const char *);

template <int N> struct scrTextLabelN 
{
	scrTextLabelN() { text[0] = '\0'; }
	scrTextLabelN(const char *value) { scr_assign_string(text,sizeof(text),value); }

	void operator=(const char *value) { scr_assign_string(text,sizeof(text),value); }
	void operator=(int value) { scr_assign_int(text,sizeof(text),value); }
	void append_int(int value) { scr_append_int(text,sizeof(text),value); }
	void append_string(const char* value) { scr_append_string(text,sizeof(text),value); }
	operator const char*() const { return text; }

	char text[N + 1];
};

union scrValue {
	int Int;
	float Float;
	bool Bool;
	void *Reference;
	const char *String;
};

struct 
#ifndef __GNUG__
	__declspec(align(16))
#endif
	DummyVector3 {
	float x,y,z,w;
	}
#ifdef __GNUG__
	__attribute__((aligned(16)))
#endif
;

struct Info {
	Info(scrValue *resultPtr, int parameterCount, scrValue *params) : ResultPtr(resultPtr), ParamCount(parameterCount), Params(params) { }

	// Return result, if applicable
	scrValue *ResultPtr;
	// Parameter count
	int ParamCount;
	// Pointer to parameter values
	scrValue *Params;

	void Fill(unsigned count,unsigned mask,va_list args);
};

void Info::Fill(unsigned count,unsigned mask,va_list args) 
{
	Params[ParamCount++].Int = mask;
	while (count) {
		int type = mask & 3; mask >>= 2; --count;
		if (type == 0)
			Params[ParamCount++].Int = va_arg(args,int);
		else if (type == 1)	
			Params[ParamCount++].Float = va_arg(args,double);	// floats are expanded to doubles via ...
		else if (type == 2)
			Params[ParamCount++].String = va_arg(args,char*);
		else // scrVector; passed by reference
			Params[ParamCount++].Reference = va_arg(args,scrValue*);
	}
}

struct Info_with_scrVector_byref: public Info {
	Info_with_scrVector_byref(scrValue *resultPtr, int parameterCount, scrValue *params) : Info(resultPtr,parameterCount,params), BufferCount(0) { }
	~Info_with_scrVector_byref();

	// Temp storage for managing copyin/copyout of Vector3 parameters
	int BufferCount;
	enum { MAX_VECTOR3 = 4 };
	float *Orig[MAX_VECTOR3];
	DummyVector3 Buffer[MAX_VECTOR3];
};

Info_with_scrVector_byref::~Info_with_scrVector_byref() { 
	while (BufferCount--) {
		Orig[BufferCount][0] = Buffer[BufferCount].x;
		Orig[BufferCount][1] = Buffer[BufferCount].y;
		Orig[BufferCount][2] = Buffer[BufferCount].z;
	}
}

typedef void (*scrCmd)(Info&);
scrCmd (*resolver)(unsigned);

static void csupport_noinline native_call(scrCmd &func,unsigned hash,Info &info)
{
	if (!func)
		func = resolver(hash);
	func(info);
}


typedef scrTextLabelN<3> scrTextLabel3;
typedef scrTextLabelN<7> scrTextLabel7;
typedef scrTextLabelN<15> scrTextLabel15;
typedef scrTextLabelN<23> scrTextLabel23;
typedef scrTextLabelN<31> scrTextLabel31;
typedef scrTextLabelN<63> scrTextLabel63;

#ifdef __GNUG__
#define __debugbreak() __builtin_trap()
#endif

// this is used by variable-length arrays (passed by reference into subprograms)
template <typename _Type> struct scrArrayBase
{
	size_t Count;					// == _count

	scrArrayBase(unsigned _count) : Count(_count) { }

	_Type& operator [](unsigned index) 
	{
		// Check against Count, not _count, because variable-length arrays can be passed by reference into subprograms
		if (index >= Count)
			__debugbreak();
		return ((_Type*)(this + 1))[index];
	}
};

template <typename _Type,unsigned _count> struct scrArray: public scrArrayBase<_Type>
{
	typedef size_t ElementBlock[sizeof(_Type) / sizeof(size_t)];
	ElementBlock Elements[_count];

	scrArray() : scrArrayBase<_Type>(_count) {
		// Note that we append () below to make sure that POD types are initialized to zero.
		for (unsigned i=0; i<_count; i++)
			::new(Elements+i) _Type();
	}

	_Type& operator [](unsigned index) 
	{
		if (index >= _count)
			__debugbreak();
		return (_Type&) Elements[index];
	}
};


#define op_array(index,base) ((base)[index])

struct scrVector 
{
	scrVector() : x(0), y(0), z(0) { }
	explicit scrVector(float _x,float _y,float _z) : x(_x), y(_y), z(_z) { }
	scrVector operator+(const scrVector b) const { return scrVector(x + b.x,y + b.y,z + b.z); }
	scrVector operator-() const { return scrVector(-x,-y,-z); }
	scrVector operator-(const scrVector b) const { return scrVector(x - b.x,y - b.y,z - b.z); }
	scrVector operator*(const scrVector b) const { return scrVector(x * b.x,y * b.y,z * b.z); }
	// We don't have to worry about big-endian 64-bit platforms
	float x;
#if __64BIT
	float xPad;
#endif
	float y;
#if __64BIT
	float yPad;
#endif
	float z;
#if __64BIT
	float zPad;
#endif
}
#ifdef __GNUG__
 __attribute__( ( d64_abi ) )
#endif
;

#define __ignored__(x)

// csupport_inline int op_iadd(int a,int b) { return a + b; }
// csupport_inline int op_isub(int a,int b) { return a - b; }
// csupport_inline int op_imul(int a,int b) { return a * b; }
csupport_inline int op_idiv(int a,int b) { return b? a / b : 0; }
csupport_inline int op_imod(int a,int b) { return b? a % b : 0; }
// csupport_inline int op_inot(int a) { return !a; }
// csupport_inline int op_ineg(int a) { return -a; }

// csupport_inline int op_ieq(int a,int b) { return a==b; }
// csupport_inline int op_ieq(void* a,void *b) { return a==b; }
// csupport_inline int op_ine(int a,int b) { return a!=b; }
// csupport_inline int op_ine(void* a,void *b) { return a!=b; }
// csupport_inline int op_ige(int a,int b) { return a>=b; }
// csupport_inline int op_igt(int a,int b) { return a>b; }
// csupport_inline int op_ile(int a,int b) { return a<=b; }
// csupport_inline int op_ilt(int a,int b) { return a<b; }

// csupport_inline float op_fadd(float a,float b) { return a + b; }
// csupport_inline float op_fsub(float a,float b) { return a - b; }
// csupport_inline float op_fmul(float a,float b) { return a * b; }
csupport_inline float op_fdiv(float a,float b) { return b? a / b : 0.0f; }
csupport_inline float op_fmod(float a,float b) { return b? a - ((int)(a/b) * b) : 0; }
// csupport_inline float op_fneg(float a) { return -a; }

// csupport_inline int op_feq(float a,float b) { return a==b; }
// csupport_inline int op_fne(float a,float b) { return a!=b; }
// csupport_inline int op_fge(float a,float b) { return a>=b; }
// csupport_inline int op_fgt(float a,float b) { return a>b; }
// csupport_inline int op_fle(float a,float b) { return a<=b; }
// csupport_inline int op_flt(float a,float b) { return a<b; }

// csupport_inline scrVector op_vadd(scrVector a,scrVector b) { return scrVector(a.X + b.X,a.Y + b.Y,a.Z + b.Z); }
// csupport_inline scrVector op_vsub(scrVector a,scrVector b) { return scrVector(a.X - b.X,a.Y - b.Y,a.Z - b.Z); }
// csupport_inline scrVector op_vmul(scrVector a,scrVector b) { return scrVector(a.X * b.X,a.Y * b.Y,a.Z * b.Z); }
csupport_inline scrVector op_vdiv(scrVector a,scrVector b) { return scrVector(op_fdiv(a.x,b.x),op_fdiv(a.y,b.y),op_fdiv(a.z,b.z)); }
csupport_inline scrVector op_vneg(scrVector a) { return scrVector(-a.x,-a.y,-a.z); }

// csupport_inline int op_iand(int a,int b) { return a & b; }
// csupport_inline int op_ior(int a,int b) { return a | b; }
// csupport_inline int op_ixor(int a,int b) { return a ^ b; }

csupport_inline float op_i2f(int i) { return (float) i; }
csupport_inline scrVector op_f2v(float a) { return scrVector(a,a,a); }
csupport_inline int op_f2i(float f) { return (int) f; }

#define op_store(value,dest) ((dest) = (value))

#define op_indirectcall(args,func)	func args

#define count_of(array)	((array).Count)

extern jmp_buf throw_buffer;

#define op_throw(value)	longjmp(throw_buffer,(value))
#define op_catch() setjmp(throw_buffer)

extern "C" float TIMESTEP();

#define op_text_label_append_int(value,dest) ((dest).append_int(value))
#define op_text_label_append_string(value,dest) ((dest).append_string(value))

#define enum_to_int(a)	(a)
#define int_to_enum(a)	(a)
csupport_inline int native_to_int(void* value) { return (int) value; }
csupport_inline void* int_to_native(int value) { return (void*) value; }

csupport_inline void CLEAR_BIT(int &x,int s) { x &= ~(1<<s); }
csupport_inline void SET_BIT(int &x,int s) { x |= (1<<s); }
csupport_inline BOOL IS_BIT_SET(int x,int s) { return ((x >> s) & 1) != 0; }

#ifdef _M_IX86
#define SELECT_DATATABLE(be,le)		le
#else
#define SELECT_DATATABLE(be,le)		be
#endif

#include "csupportargs.h"