// 
// script/wrapper.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SCRIPT_WRAPPER_H
#define SCRIPT_WRAPPER_H

#include "thread.h"
#include "string/stringhash.h"
#include "system/timer.h"

#if __BANK		// Have all native functions do a RAGE_TRACK of their name?  Lots of information, but extra code and larger tracker files.
#include "diag/tracker.h"
#define SCR_RAGE_BEGIN_TRACK(n)		::rage::scrWrapper::BeginTrack(n)
#define SCR_RAGE_TRACK2(n,h)		RAGE_TRACK(n)
#define SCR_RAGE_END_TRACK			::rage::scrWrapper::EndTrack()
#elif !__FINAL && !__PROFILE
#define SCR_RAGE_BEGIN_TRACK(x) 
#define SCR_RAGE_TRACK2(n,h)
#define SCR_RAGE_END_TRACK			
#else
#define SCR_RAGE_BEGIN_TRACK(n)
#define SCR_RAGE_TRACK2(n,h)		RAGE_TRACK(n)
#define SCR_RAGE_END_TRACK			
#endif

namespace rage {

#define SCRHASH(str,code)	(code)

#if !__FINAL
struct scrTimingHelper {
	scrTimingHelper(u64 hash) : Now(sysTimer::GetTicks()), Hash(hash) { }
	~scrTimingHelper();
	utimer_t Now;
	u64 Hash;
};
#endif

extern "C" void ApiCheckPlugin_AddNative(u64);
namespace scrWrapper {

#if __BANK
// Defined in thread.cpp. Using these instead of the normal RAGE_TRACK macros
// because the RAGE_TRACK macros were getting inlined and bloating the code in beta builds.
// This adds some function call overhead but saves space
void BeginTrack(const char* name);
void EndTrack();
#endif

//////////////////////////////////
// Return value templates.
// Info.Result should be set to the value of the second argument

inline void AssignReturnValue(scrThread::Info& info, float f) {
	info.ResultPtr->Float = f;
}

inline void AssignReturnValue(scrThread::Info& info, int i) {
	info.ResultPtr->Int = i;
}

inline void AssignReturnValue(scrThread::Info& info, u32 val) {
	info.ResultPtr->Int = val;
}

inline static void AssignReturnValue(scrThread::Info& info, scrVector v) {
#if __64BIT
	scrValue *ret = info.ResultPtr;
	ret[0].Float = v.x;
	ret[1].Float = v.y;
	ret[2].Float = v.z;
#else
	*reinterpret_cast<scrVector*>(&info.ResultPtr->Reference) = v;  
#endif
}

/*inline static void AssignReturnValue(scrThread::Info& info, const Vector3& v) {
	*reinterpret_cast<scrVector*>(&info.ResultPtr->Reference->Float) = v;
}*/

inline void AssignReturnValue(scrThread::Info& info, bool b) {
	info.ResultPtr->Int = b;
}

inline void AssignReturnValue(scrThread::Info& info, const char* s) {
	info.ResultPtr->String = scrEncodeString(s);
}

#if 0
template<typename T>
inline void AssignReturnValueIfRunning(scrThread::Info& info, T t)
{
	if(scrThread::GetCurrentThread()->GetState() == scrThread::RUNNING)
		AssignReturnValue(info, t);
}
#endif

//////////////////////////////////
// Argument type templates.

template<typename T>
struct Arg {
	inline static T Value(scrThread::Info& s, int& N);
};

template<>
struct Arg<float> {
	inline static float Value(scrThread::Info& info, int& N) {return info.Params[N++].Float;}
};

template<>
struct Arg<int> {
	inline static int Value(scrThread::Info& info, int& N) {return info.Params[N++].Int;}
};

template<>
struct Arg<bool> {
	inline static bool Value(scrThread::Info& info, int& N) {return info.Params[N++].Int != 0;}
};

template<>
struct Arg<const char*> {
	inline static const char* Value(scrThread::Info& info, int& N) {return scrDecodeString(info.Params[N++].String);}
};

// This generates terrible code.  Better to force people to change their wrappers to take a const scrVector&
// Note that the script interface (ie the .sch file) still needs to declare it as a VECTOR (ie by value)).
#if 0
template<>
struct Arg<scrVector> {
	inline static scrVector Value(scrThread::Info& info, int& N) {
		// Tried removing the increment and just add once at the end, made code
		// size slightly worse.
		const float x = info.Params[N].Float;N++;
		const float y = info.Params[N].Float;N++;
		const float z = info.Params[N].Float;N++;
		return scrVector(x,y,z);
	}
};
#endif

template<>
struct Arg<const scrVector&> {
	inline static scrVector& Value(scrThread::Info& info, int& N) {
		N += 3;
		return *(scrVector*)(&info.Params[N-3]);
	}
};

template<>
struct Arg<Vector3> {
	inline static Vector3 Value(scrThread::Info& info, int& N) {
#if __64BIT
		const scrValue * v = &info.Params[N];
		N += 3;
		Vector3 vec;
		vec.x = v[0].Float;
		vec.y = v[1].Float;
		vec.z = v[2].Float;
#else
		const float* f = &info.Params[N].Float;
		N += 3;
		Vector3 vec;
		vec.x = f[0];
		vec.y = f[1];
		vec.z = f[2];
#endif
		return vec;
	}
};

template<>
struct Arg<float&> {
	inline static float& Value(scrThread::Info& info, int& N) {return scrDecodeReference(info.Params[N++].Reference)->Float;}
};

template<>
struct Arg<int&> {
	inline static int& Value(scrThread::Info& info, int& N) {return scrDecodeReference(info.Params[N++].Reference)->Int;}
};


// Intentionally unimplemented, it cannot be done safely due to size differences between C++ bool and script BOOL.
// template<>
// struct Arg<bool&> {
//	static bool& Value(scrThread::Info& info, int& N);
//};

// template<>
// struct Arg<const char*&> {
//	inline static const char*& Value(scrThread::Info& info, int& N) {return info.Params[N++].Reference->String;}
//};

template<>
struct Arg<Vector3&> {
	inline static Vector3& Value(scrThread::Info& info, int& N) {
		return info.GetVector3(N);
	}
};

template<>
struct Arg<const Vector3&> {
	inline static const Vector3& Value(scrThread::Info& info, int& N) {
		return info.GetVector3(N);
	}
};

///////////////////////////////////
template<typename _ArgType> struct ArgType {
	static const scrValue::ValueType Value = scrValue::UNKNOWN;
};

template<>
struct ArgType<void> {
	static const scrValue::ValueType Value = scrValue::UNKNOWN;
};

struct UnspecifiedArg {};

template<>
struct ArgType<UnspecifiedArg> {
	static const scrValue::ValueType Value = scrValue::UNKNOWN;
};

template<>
struct ArgType<int> {
	static const scrValue::ValueType Value = scrValue::INT;
};

template<>
struct ArgType<u32> {
	static const scrValue::ValueType Value = scrValue::INT;
};

template<>
struct ArgType<bool> {
	static const scrValue::ValueType Value = scrValue::BOOL;
};

template<>
struct ArgType<float> {
	static const scrValue::ValueType Value = scrValue::FLOAT;
};

template<>
struct ArgType<const char*> {
	static const scrValue::ValueType Value = scrValue::STRING;
};

template<>
struct ArgType<char*> {
	static const scrValue::ValueType Value = scrValue::STRING;
};

template<>
struct ArgType<scrVector> {
	static const scrValue::ValueType Value = scrValue::VECTOR;
};

template<>
struct ArgType<const scrVector&> {
	static const scrValue::ValueType Value = scrValue::VECTOR;
};

template<>
struct ArgType<Vector3&> {
	static const scrValue::ValueType Value = scrValue::VECTOR;
};

template<>
struct ArgType<const Vector3&> {
	static const scrValue::ValueType Value = scrValue::VECTOR;
};

template<>
struct ArgType<Vector3> {
	static const scrValue::ValueType Value = scrValue::VECTOR;
};

template<>
struct ArgType<float&> {
	static const scrValue::ValueType Value = scrValue::OBJECT;
};

template<>
struct ArgType<int&> {
	static const scrValue::ValueType Value = scrValue::OBJECT;
};

/* template<>
struct ArgType<void*> {
	static const scrValue::ValueType Value = scrValue::OBJECT;
}; */

template<typename _Unused> struct SignatureGenerator;

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3,
	typename _Arg4, typename _Arg5, typename _Arg6, typename _Arg7, typename _Arg8,
	typename _Arg9, typename _Arg10, typename _Arg11, typename _Arg12, typename _Arg13,
	typename _Arg14, typename _Arg15, typename _Arg16, typename _Arg17>
struct SignatureGenerator<_RT (*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8, _Arg9, _Arg10, _Arg11, _Arg12, _Arg13, _Arg14, _Arg15, _Arg16, _Arg17)> {
	static const u64 Signature = 
		SCR_MAKE_SIGNATURE(ArgType<_RT>::Value,	
			ArgType<_Arg0>::Value,	ArgType<_Arg1>::Value,	ArgType<_Arg2>::Value,
			ArgType<_Arg3>::Value,	ArgType<_Arg4>::Value,	ArgType<_Arg5>::Value,	
			ArgType<_Arg6>::Value,	ArgType<_Arg7>::Value,	ArgType<_Arg8>::Value,
			ArgType<_Arg9>::Value,	ArgType<_Arg10>::Value,	ArgType<_Arg11>::Value,
			ArgType<_Arg12>::Value,	ArgType<_Arg13>::Value,	ArgType<_Arg14>::Value,
			ArgType<_Arg15>::Value,	ArgType<_Arg16>::Value,	ArgType<_Arg17>::Value);
};

template<typename _RT>
inline u64 GetFunctionSignature(_RT(*)(void))
{
	return SignatureGenerator<_RT(*)(
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0>
inline u64 GetFunctionSignature(_RT(*)(_Arg0))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3, typename _Arg4>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		_Arg4,			UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3, typename _Arg4, typename _Arg5>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4, _Arg5))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		_Arg4,			_Arg5,			UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3, typename _Arg4, typename _Arg5, typename _Arg6>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		_Arg4,			_Arg5,			_Arg6,			UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3, typename _Arg4, typename _Arg5, typename _Arg6, typename _Arg7>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		_Arg4,			_Arg5,			_Arg6,			_Arg7,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3, typename _Arg4, typename _Arg5, typename _Arg6, typename _Arg7, typename _Arg8>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		_Arg4,			_Arg5,			_Arg6,			_Arg7,
		_Arg8,			UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3, typename _Arg4, typename _Arg5, typename _Arg6, typename _Arg7, typename _Arg8,
	typename _Arg9>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8, _Arg9))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		_Arg4,			_Arg5,			_Arg6,			_Arg7,
		_Arg8,			_Arg9,			UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3, typename _Arg4, typename _Arg5, typename _Arg6, typename _Arg7, typename _Arg8,
	typename _Arg9, typename _Arg10>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8, _Arg9, _Arg10))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		_Arg4,			_Arg5,			_Arg6,			_Arg7,
		_Arg8,			_Arg9,			_Arg10,			UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3, typename _Arg4, typename _Arg5, typename _Arg6, typename _Arg7, typename _Arg8,
	typename _Arg9, typename _Arg10, typename _Arg11>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8, _Arg9, _Arg10, _Arg11))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		_Arg4,			_Arg5,			_Arg6,			_Arg7,
		_Arg8,			_Arg9,			_Arg10,			_Arg11,
		UnspecifiedArg, UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3, typename _Arg4, typename _Arg5, typename _Arg6, typename _Arg7, typename _Arg8,
	typename _Arg9, typename _Arg10, typename _Arg11, typename _Arg12>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8, _Arg9, _Arg10, _Arg11, _Arg12))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		_Arg4,			_Arg5,			_Arg6,			_Arg7,
		_Arg8,			_Arg9,			_Arg10,			_Arg11,
		_Arg12,			UnspecifiedArg, UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3, typename _Arg4, typename _Arg5, typename _Arg6, typename _Arg7, typename _Arg8,
	typename _Arg9, typename _Arg10, typename _Arg11, typename _Arg12, typename _Arg13>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8, _Arg9, _Arg10, _Arg11, _Arg12, _Arg13))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		_Arg4,			_Arg5,			_Arg6,			_Arg7,
		_Arg8,			_Arg9,			_Arg10,			_Arg11,
		_Arg12,			_Arg13,			UnspecifiedArg, UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3, typename _Arg4, typename _Arg5, typename _Arg6, typename _Arg7, typename _Arg8,
	typename _Arg9, typename _Arg10, typename _Arg11, typename _Arg12, typename _Arg13, typename _Arg14>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8, _Arg9, _Arg10, _Arg11, _Arg12, _Arg13, _Arg14))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		_Arg4,			_Arg5,			_Arg6,			_Arg7,
		_Arg8,			_Arg9,			_Arg10,			_Arg11,
		_Arg12,			_Arg13,			_Arg14,			UnspecifiedArg,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3, typename _Arg4, typename _Arg5, typename _Arg6, typename _Arg7, typename _Arg8,
	typename _Arg9, typename _Arg10, typename _Arg11, typename _Arg12, typename _Arg13, typename _Arg14, typename _Arg15>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8, _Arg9, _Arg10, _Arg11, _Arg12, _Arg13, _Arg14, _Arg15))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		_Arg4,			_Arg5,			_Arg6,			_Arg7,
		_Arg8,			_Arg9,			_Arg10,			_Arg11,
		_Arg12,			_Arg13,			_Arg14,			_Arg15,
		UnspecifiedArg, UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3, typename _Arg4, typename _Arg5, typename _Arg6, typename _Arg7, typename _Arg8,
	typename _Arg9, typename _Arg10, typename _Arg11, typename _Arg12, typename _Arg13, typename _Arg14, typename _Arg15, typename _Arg16>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8, _Arg9, _Arg10, _Arg11, _Arg12, _Arg13, _Arg14, _Arg15, _Arg16))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		_Arg4,			_Arg5,			_Arg6,			_Arg7,
		_Arg8,			_Arg9,			_Arg10,			_Arg11,
		_Arg12,			_Arg13,			_Arg14,			_Arg15,
		_Arg16,			UnspecifiedArg)>::Signature;
}

template<typename _RT, typename _Arg0, typename _Arg1, typename _Arg2, typename _Arg3, typename _Arg4, typename _Arg5, typename _Arg6, typename _Arg7, typename _Arg8,
	typename _Arg9, typename _Arg10, typename _Arg11, typename _Arg12, typename _Arg13, typename _Arg14, typename _Arg15, typename _Arg16, typename _Arg17>
inline u64 GetFunctionSignature(_RT(*)(_Arg0, _Arg1, _Arg2, _Arg3, _Arg4, _Arg5, _Arg6, _Arg7, _Arg8, _Arg9, _Arg10, _Arg11, _Arg12, _Arg13, _Arg14, _Arg15, _Arg16, _Arg17))
{
	return SignatureGenerator<_RT(*)(
		_Arg0,			_Arg1,			_Arg2,			_Arg3,
		_Arg4,			_Arg5,			_Arg6,			_Arg7,
		_Arg8,			_Arg9,			_Arg10,			_Arg11,
		_Arg12,			_Arg13,			_Arg14,			_Arg15,
		_Arg16,			_Arg17)>::Signature;
}


///////////////////////////////////
// Type-based access to TLS values
//

template<typename T, int N>
inline static T* GetTlsGlobal() {
	scrThread *pThread = scrThread::GetActiveThread();
	return reinterpret_cast<T*>(pThread->TLS(N).Reference);
}

// Use this to store a particular pointer type in a threads local storage. For example if you stored an aActor pointer
// in slot 0, you would use SCR_DEFINE_TLS_TYPE_VALUE(aActor, 0). Then any functions which took an aActor* argument
// would get the aActor* in slot 0.
#define SCR_DEFINE_TLS_TYPE_VALUE(tlsType, value) \
namespace rage {\
namespace scrWrapper {\
template<>\
struct Arg<tlsType*> {\
	inline static tlsType* Value(scrThread::Info& , int&) {return ::rage::scrWrapper::GetTlsGlobal<tlsType, value>();}\
};\
}\
}\
//END

// Should be able to do more template voodoo here. Given a conversion function, automatically create this template?
// The conversionFunc should be T(*)(scrValue&), and Arg::Value should return T.
#define SCR_DEFINE_NEW_PARAM_TYPE(newType, conversionFunc) \
namespace rage{\
namespace scrWrapper {\
template<>\
struct Arg<newType> {\
	inline static newType Value(scrThread::Info& info, int& N) {return conversionFunc(info.Params[N++]);}\
};\
}\
}\
//END

// PURPOSE: Define newType as a new wrappable type for both function parameters and return values.
#define SCR_DEFINE_NEW_POINTER_PARAM_AND_RET(newType) \
namespace rage {\
namespace scrWrapper {\
template<>\
struct Arg<newType*> {\
	inline static newType* Value(scrThread::Info& info, int& N) {return reinterpret_cast<newType*>(scrDecodeReference(info.Params[N++].Reference));}\
};\
template<>\
struct Arg<const newType*> {\
	inline static const newType* Value(scrThread::Info& info, int& N) {return reinterpret_cast<const newType*>(scrDecodeReference(info.Params[N++].Reference));}\
};\
inline void AssignReturnValue(scrThread::Info& info, newType* p) {info.ResultPtr->Reference = scrEncodeReference(reinterpret_cast<scrValue*>(const_cast<newType*>(p)));}\
inline void AssignReturnValue(scrThread::Info& info, const newType* p) {info.ResultPtr->Reference = scrEncodeReference(reinterpret_cast<scrValue*>(const_cast<newType*>(p)));}\
template<> struct ArgType<newType*> {static const scrValue::ValueType Value = scrValue::OBJECT;};\
template<> struct ArgType<const newType*> {static const scrValue::ValueType Value = scrValue::OBJECT;};\
}\
}\
//END

///////////////////////////////////
// Function call templates.
// Each of these comes in two forms. One for a void return value, one for non-void.

#if __ASSERT
#define AssertParamMatchN(n)		Assertf((scrThread::sm_DevCurrentCmdName=scriptName,s.ParamCount == n),"Script signature mismatch, got %d words, but '%s' expected %d; this will probably crash.",s.ParamCount,scriptName,n) 
#define SCRIPTNAME const char *scriptName, 
#define SCRIPTNAME_ONLY(x)			x
#define SCRIPTNAME_PARAM(x)			#x,
#else
#define AssertParamMatchN(n)
#define SCRIPTNAME
#define SCRIPTNAME_ONLY(x)
#define SCRIPTNAME_PARAM(x)
#endif

#define AssertParamMatch()			AssertParamMatchN(N)

// Non-void return types
template <typename RT>
inline void CallFunction(SCRIPTNAME RT(*fn)(), scrThread::Info& s)
{
	AssertParamMatchN(0);
	AssignReturnValue(s, fn());
}

// Specializing zero-parameter version had no gain.
template <typename RT, typename T0>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0));
}

template <typename RT, typename T0, typename T1>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1));
}

template <typename RT, typename T0, typename T1>
inline void CallFunctionBlocking(SCRIPTNAME RT(*fn)(T0, T1), scrThread::Info& s) // could just change CallFunction and give it an explicit template bool (evaluated at compile time), but then I'd have to change EVERYTHING in this file
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	AssertParamMatch();
	AssignReturnValueIfRunning(s, fn(t0, t1));
}

template <typename RT, typename T0, typename T1, typename T2>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2));
}

template <typename RT, typename T0, typename T1, typename T2>
inline void CallFunctionBlocking(SCRIPTNAME RT(*fn)(T0, T1, T2), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	AssertParamMatch();
	AssignReturnValueIfRunning(s, fn(t0, t1, t2));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3, T4), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3, T4, T5), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3, T4, T5, T6), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	T15 t15 = Arg<T15>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	T15 t15 = Arg<T15>::Value(s, N);
	T16 t16 = Arg<T16>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16));
}

template <typename RT, typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17>
inline void CallFunction(SCRIPTNAME RT(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	T15 t15 = Arg<T15>::Value(s, N);
	T16 t16 = Arg<T16>::Value(s, N);
	T17 t17 = Arg<T17>::Value(s, N);
	AssertParamMatch();
	AssignReturnValue(s, fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16, t17));
}

// Void return types

template <>
inline void CallFunction(SCRIPTNAME void(*fn)(), scrThread::Info& SCRIPTNAME_ONLY(s))
{
	AssertParamMatchN(0);
	fn();
}

template <typename T0>
inline void CallFunction(SCRIPTNAME void(*fn)(T0), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	AssertParamMatch();
	fn(t0);
}

template <typename T0, typename T1>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1);
}

template <typename T0, typename T1, typename T2>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2);
}

template <typename T0, typename T1, typename T2, typename T3>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3, T4), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3, t4);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3, T4, T5), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3, t4, t5);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3, T4, T5, T6), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3, t4, t5, t6);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3, t4, t5, t6, t7);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	T15 t15 = Arg<T15>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	T15 t15 = Arg<T15>::Value(s, N);
	T16 t16 = Arg<T16>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16);
}

template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17>
inline void CallFunction(SCRIPTNAME void(*fn)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17), scrThread::Info& s)
{
	int N = 0;
	T0 t0 = Arg<T0>::Value(s, N);
	T1 t1 = Arg<T1>::Value(s, N);
	T2 t2 = Arg<T2>::Value(s, N);
	T3 t3 = Arg<T3>::Value(s, N);
	T4 t4 = Arg<T4>::Value(s, N);
	T5 t5 = Arg<T5>::Value(s, N);
	T6 t6 = Arg<T6>::Value(s, N);
	T7 t7 = Arg<T7>::Value(s, N);
	T8 t8 = Arg<T8>::Value(s, N);
	T9 t9 = Arg<T9>::Value(s, N);
	T10 t10 = Arg<T10>::Value(s, N);
	T11 t11 = Arg<T11>::Value(s, N);
	T12 t12 = Arg<T12>::Value(s, N);
	T13 t13 = Arg<T13>::Value(s, N);
	T14 t14 = Arg<T14>::Value(s, N);
	T15 t15 = Arg<T15>::Value(s, N);
	T16 t16 = Arg<T16>::Value(s, N);
	T17 t17 = Arg<T17>::Value(s, N);
	AssertParamMatch();
	fn(t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16, t17);
}

} // namespace scrWrapper


// Given a function pointer, return a function pointer that calls CallFunction with the f.p. and the scrThread::Info
#define SCR_REGISTER_SECURE_UNHASHED(name, hashcode, fn) \
	struct scrWrapped_ ## name {\
		static void Call(::rage::scrThread::Info& s) ATTR_COLD {\
		PROFILER_EVENT(#name"::Native") \
		SCR_RAGE_TRACK2(name,hashcode); \
		::rage::scrWrapper::CallFunction(SCRIPTNAME_PARAM(name) fn, s);\
	}\
	};\
	::rage::scrThread::RegisterCommand(hashcode, scrWrapped_ ## name::Call SCRIPT_DEBUGGING_ONLY(, #name, __FILE__, ::rage::scrSignature(::rage::scrWrapper::GetFunctionSignature(fn))))

// Given a function pointer, return a function pointer that calls CallFunction with the f.p. and the scrThread::Info
#define SCR_REGISTER_SECURE(name, hashcode, fn) \
	struct scrWrapped_ ## name {\
		static void Call(::rage::scrThread::Info& s) ATTR_COLD {\
		PROFILER_EVENT(#name"::Native") \
		SCR_RAGE_TRACK2(name,hashcode); \
		::rage::scrWrapper::CallFunction(SCRIPTNAME_PARAM(name) fn, s); \
	}\
	};\
	ApiCheckPlugin_AddNative((u64)fn);\
	::rage::scrThread::RegisterCommand(hashcode, scrWrapped_ ## name::Call SCRIPT_DEBUGGING_ONLY(, #name, __FILE__, ::rage::scrSignature(::rage::scrWrapper::GetFunctionSignature(fn))))

#if RSG_PC
#define SCR_REGISTER_SECURE_HONEYPOT(name, hashcode, fn) \
struct scrWrapped_ ## name {\
	static void Call(::rage::scrThread::Info& s) ATTR_COLD {\
	SCR_RAGE_TRACK2(name,hashcode); \
	SetRuntimeRsp(_AddressOfReturnAddress()); \
	::rage::scrWrapper::CallFunction(SCRIPTNAME_PARAM(name) fn, s);\
}\
};\
	::rage::scrThread::RegisterCommand(hashcode, scrWrapped_ ## name::Call SCRIPT_DEBUGGING_ONLY(, #name, __FILE__, ::rage::scrSignature(::rage::scrWrapper::GetFunctionSignature(fn))))
#else
#define SCR_REGISTER_SECURE_HONEYPOT(name, hashcode, fn) SCR_REGISTER_SECURE(name, hashcode, fn)
#endif


#define SCR_REGISTER_SECURE_BLOCKING(name, hashcode, fn) \
	struct scrWrapped_ ## name {\
		static void Call(::rage::scrThread::Info& s) ATTR_COLD {\
			PROFILER_EVENT(#name"::Native") \
			SCR_RAGE_TRACK2(name,hashcode); \
			::rage::scrWrapper::CallFunctionBlocking(SCRIPTNAME_PARAM(name) fn, s);\
		}\
	};\
	::rage::scrThread::RegisterCommand(hashcode, scrWrapped_ ## name::Call SCRIPT_DEBUGGING_ONLY(, #name, __FILE__, ::rage::scrSignature(::rage::scrWrapper::GetFunctionSignature(fn))))

#define SCR_REGISTER_SECURE_EXPORT(name, hashcode, fn) SCR_REGISTER_SECURE(name, hashcode, fn)

#undef SCRIPTNAME

// For marking things we think are dead-stripped.
#if __FINAL
#define SCR_REGISTER_UNUSED(x,y,z)
#else
#define SCR_REGISTER_UNUSED(name, hashcode, fn) \
	struct scrWrapped_ ## name {\
		static void Call(::rage::scrThread::Info& s) ATTR_COLD {\
			::rage::scrWrapper::CallFunction(SCRIPTNAME_PARAM(name) fn, s);\
		}\
	};\
	::rage::scrThread::RegisterUnusedCommand(hashcode, scrWrapped_ ## name::Call SCRIPT_DEBUGGING_ONLY(, #name, __FILE__, ::rage::scrSignature(::rage::scrWrapper::GetFunctionSignature(fn))))
#endif

// And finally, for marking things we do NOT ever want dead-stripped.
#define DLC_SCR_REGISTER_SECURE(x,y,z)			SCR_REGISTER_SECURE(x,y,z)
#define DLC_SCR_REGISTER_SECURE_BLOCKING(x,y,z)	SCR_REGISTER_SECURE_BLOCKING(x,y,z)
#define DLC_SCR_REGISTER_SECURE_EXPORT(x,y,z)	SCR_REGISTER_SECURE_EXPORT(x,y,z)


} // namespace rage

#endif
