#ifndef SCRIPT_THREAD_H
#define SCRIPT_THREAD_H

#include "atl/array.h"
#include "atl/map.h"
#include "atl/functor.h"
#include "profile/rocky.h"
#include "script/value.h"
#include "script/program.h"
#include "script/signature.h"
#include "security/obfuscatedtypes.h"
#include "vector/vector3.h"
#include "vectormath/vec3v.h"
#include "system/timer.h"

#define SCRIPT_DEBUGGING	(__DEV || __BANK)
#if SCRIPT_DEBUGGING
#	define SCRIPT_DEBUGGING_ONLY(...) __VA_ARGS__
#else
#	define SCRIPT_DEBUGGING_ONLY(...)
#endif

#define SCRIPT_PROFILING	(__BANK && __OPTIMIZED)

#define DEFAULT_INSTRUCTION_COUNT (13000000)

namespace Profiler
{
	struct ScriptSymbol;
}

namespace rage {

	class bkButton;

	namespace rageScriptDebug
	{
		//expose the print functions so external systems can hook them as needed.
		typedef void (PrinterStrFn) (const char *str);
		typedef void (PrinterFloatFn) (int width,int prec,float val);
		typedef void (PrinterIntFn) (int width,int val);
		typedef void (PrinterNewLineFn) (void);
		typedef void (PrinterVectorFn) (const Vector3& val);

        //PURPOSE: Initializes the script output functions
        void    Init();

		//PURPOSE: rebinds the default script output functions.
		void	ReBindDefaults();

		extern PrinterStrFn*			gScriptPrintString;
		extern PrinterFloatFn*			gScriptPrintFloat;
		extern PrinterIntFn*			gScriptPrintInt;
		extern PrinterNewLineFn*		gScriptPrintNewLine;
		extern PrinterVectorFn*			gScriptPrintVector3;
	}

class scrProgram;

enum scrThreadId : int {
	THREAD_INVALID = 0,
};

enum scrThreadFaultID {
	THREAD_UNKNOWN_ERROR_TYPE = 0,
	THREAD_STACK_OVERFLOW = 1,
	THREAD_ARRAY_OVERFLOW = 2,
	THREAD_INSTRUCTION_COUNT_OVERFLOW = 3
};

typedef char scrTextLabel[scrMaxTextLabelSize];	// this does NOT match TEXT_LABEL - internal use only!

// use these for interfacing scripts with game code
typedef char scrTextLabel7[2*sizeof(scrValue)];
typedef char scrTextLabel15[4*sizeof(scrValue)];
typedef char scrTextLabel23[6*sizeof(scrValue)];
typedef char scrTextLabel31[8*sizeof(scrValue)];
typedef char scrTextLabel63[16*sizeof(scrValue)];

struct scrVector {
	scrVector() : x(0), y(0), z(0) { }
	scrVector(float _x,float _y,float _z) : x(_x), y(_y), z(_z) { }
	scrVector(const Vector3 &v) : x(v.x), y(v.y), z(v.z) { }
	scrVector(const Vec3V &v) : x(v.GetXf()), y(v.GetYf()), z(v.GetZf()) { }
	scrVector& operator=(const Vector3&v) {
		x = v.x; y = v.y; z = v.z;
		return *this;
	}
	scrVector& operator=(const Vec3V&v) {
		x = v.GetXf(); y = v.GetYf(); z = v.GetZf();
		return *this;
	}
	operator Vector3 () const {
		return Vector3(x, y, z);
	}
	operator Vec3V () const {
		return Vec3V(x, y, z);
	}
#if __XENON && VECTORIZED
	operator Vector3::Vector3Param () const {
		return Vector3(x, y, z);
	}
#endif
#if __64BIT && !SCRVALUE_OFFSETS
	float x, xPad, y, yPad, z, zPad;
#else
	float x, y, z;
#endif
};

struct scrExecState;

#define MAX_CALLSTACK 16



#if !__FINAL
class CScriptArraySizeMonitor
{
public:
	CScriptArraySizeMonitor();

	void StartMonitoring(s32 *pAddressToMonitor, scrThreadId ownerThread);
	void StopMonitoringForThisThread(scrThreadId threadIdToCheck);
	bool CheckIfMonitoredAddressHasChanged(const char *pStringToDisplay, s32 integerToDisplay);

private:
	void Reset();

	s32 *m_pAddressToMonitor;
	s32 m_OriginalValue;
	scrThreadId m_OwnerThreadId;
};
#endif	//	!__FINAL

/*
	A scrThread encapsulates a virtual machine's state.  We intentionally avoid pointers here
	so that load/save is simplified.
*/
class scrThread {
public:
	enum { c_DefaultStackSize = 512 };		// static const causes missing symbol under gcc 4.1.1 debug builds.
											// presume it's something to do with its use as a default parameter.
	enum { c_NativeInsnLength = 4 };		// length of OP_NATIVE insn in case caller needs PC of *next* insn.

	// Current machine state; if RUNNING, the thread overran the current remaining
	// instruction count.  If BLOCKED, the thread called a function which did not
	// complete this frame (the canonical example being the WAIT command).  If ABORTED,
	// the thread triggered some sort of serious fault condition and will never run.
	// If HALTED, the script is being debugged.
	enum State { RUNNING, BLOCKED, ABORTED, HALTED };
	enum Priority { THREAD_PRIO_HIGHEST, THREAD_PRIO_NORMAL, THREAD_PRIO_LOWEST, THREAD_PRIO_MANUAL_UPDATE = 100 };
#if RSG_PC && RSG_CPU_X64
	// 2015-11-13 - Security related.
	// One of the attacks that we're seeing is that people are hooking the run function in the vtable of each instance
	// of scrThread. So this is an attempt to address that, in as best of a way as we can
	// This is going to record each progId (effective hash of the script name) in addition to the 
	// pointer of it's recorded Run() function after the thread has been created. At runtime, we'll sample
	// a handful of them randomly, and ensure that they're what we expect. 
	static atMap<sysObfuscated<u32>, sysObfuscated<u64>> sm_ProgIdRunFunctions;
#endif
private:
	// Structure containing all thread state that is serialized.  No pointers allowed!
	struct Serialized {
		scrThreadId m_ThreadId;				// +0 Unique thread ID (always increasing)
		scrProgramId m_Prog;				// +4 Stored by hashcode rather than pointer so load/save can work
		State m_State;						// +8 Current thread state
		int m_PC;							// +12 Current program counter (index into program's opcodes array)
		int m_FP;							// +16 Current frame pointer (anchor for local variables)
		int m_SP;							// +20 Stack pointer
		float m_TimerA, m_TimerB;				// Local per-thread timer values
		float m_Wait;							// Accumulated wait timer
		int m_MinPC, m_MaxPC;				// Step region for debugger
		atRangeArray<scrValue,4> m_TLS;		// Thread-local storage for blocking functions
		int m_StackSize;
		int m_CatchPC, m_CatchFP, m_CatchSP;
		Priority m_Priority;				// 0=highest (runs first), 2=lowest (runs last)
		s8 m_CallDepth;
		atRangeArray<int,MAX_CALLSTACK> m_CallStack;
		CompileTimeAssert(MAX_CALLSTACK < (1 << (sizeof(s8) * 8 - 1)));
	};

	struct Global {
		int BasicType;		// enumerant in scrValue
		int Offset;			// Base address of this variable
		u32 Hash;			// Hashed name of this variable (as per scrComputeHash)
	};

public:
	// PURPOSE:	Allow applications to override functionality
	//	used to start a new thread.
	static scrThreadId (*StartNewThreadOverride)(const char *,const void*,int,int);

	static scrThreadId (*StartNewThreadWithNameHashOverride)(atHashValue,const void*,int,int);
	
	// PURPOSE:	Allow applications to register an exception handler
    // PARAMS:
    //    const char* - message providing details about the fault.  This can be NULL.
    //    scrThread* - the thread that triggered the fault.
	static void (*AppFaultHandler)(const char *, scrThread *, scrThreadFaultID);

#if __BANK
    // PURPOSE:	Fault handler functor for the script debugger.
    // PARAMS:
    //    const char* - message providing details about the fault.  This can be NULL.
    //    scrThread* - the thread that triggered the fault.
	typedef Functor2<const char*, scrThread *> FaultHandlerFunc;
	
	// PURPOSE: Allow the script debugger to register an exception handler
    // PARAMS:
    //    del - the fault handler functor
	void SetDebugFaultHandler( FaultHandlerFunc del );

    // PURPOSE:	Breakpoint callback functor for the script debugger.
    // PARAMS:
    //    scrThread* - the thread that triggered the fault.
    //    int - the pc of the breakpoint
    typedef Functor2<scrThread *, int> ThreadBreakpointFunc;

    // PURPOSE: Allow the script debugger to register a breakpoint callback
    // PARAMS:
    //    del - the breakpoint callback functor
    void SetDebugBreakpointCallback( ThreadBreakpointFunc del );
#endif

	struct Info {
		Info(scrValue *resultPtr, int parameterCount, const scrValue *params) :
			ResultPtr(resultPtr), ParamCount(parameterCount), Params(params),
				BufferCount(0) { }

		// Return result, if applicable
		scrValue *ResultPtr;
		// Parameter count
		int ParamCount;
		// Pointer to parameter values
		const scrValue *Params;

		// Temp storage for managing copyin/copyout of Vector3 parameters
		int BufferCount;
		enum { MAX_VECTOR3 = 4 };
#if __64BIT
		scrValue *Orig[MAX_VECTOR3];
#else
		float *Orig[MAX_VECTOR3];
#endif
		Vector3 Buffer[MAX_VECTOR3];

		Vector3& GetVector3(int &N);
		void CopyReferencedParametersOut();
	};

	// PURPOSE:	Constructor
	scrThread();

	// PURPOSE:	Destructor
	virtual ~scrThread();
	
	// PURPOSE:	Reset the thread to a runnable state at the top of the current program
	//			and copy initial values of all statics in from the program.
	// PARAMS:	program - Program index to attach to, or NULL to stay attached to existing program
	//			argStruct - optional pointer to arg structure
	//			argStructSize - optional arg structure size (in bytes)
	// NOTES:	If argStruct is used, it must match the size of the program being attached to
	//			(or else we will assert out)
	virtual void Reset(scrProgramId program,const void *argStruct,int argStructSize);

	// PURPOSE:	Execute script instructions.
	// PARAMS:	insnCount - variable containing the maximum number of instructions
	//				to execute before returning. Can be zero, in which case the script does nothing.
	// RETURNS:	New machine state
	virtual State Run(int insnCount);
	State Exec(scrProgram *prog,unsigned caller_r1);

	// PURPOSE: Access thread-local storage; intended for blocking native functions to use to preserve state.
	//			The state should only be valid
	// NOTES:	This needs more work to figure out how to manage these variables.
	scrValue& TLS(int i = 0) { return m_Serialized.m_TLS[i]; }

	// PURPOSE: Gets the max amount of local storage spaces
	int TLSMaxCount() const { return m_Serialized.m_TLS.GetMaxCount(); }    
	
	// PURPOSE: Access a thread's own id for reflexivity
    // RETURNS: scrThreadId
    scrThreadId GetThreadId() const { return m_Serialized.m_ThreadId; }

	static void DisassembleInsn(char *dest,const scrProgram &opcodes,u32 pc);

#if __DEV
	// PURPOSE:	Disassemble the virtual machine code for the specied program at the specified address
	// PARAMS:	dest - Destination buffer to receive disassembly test
	//			program - Program index to disassemble
	//			pc - program counter to disassemble at (should start with zero)
	// RETURNS:	Program counter value for NEXT instruction
	static int Disassemble(char *dest,scrProgramId program,int pc);

	// PURPOSE:	Dumps thread state to console (program counter, stack contents, etc)
	virtual void DumpState(void (*printer)(const char *fmt,...));
#endif

	// PURPOSE: Get the hash of the script that this thread is running
	u32 GetScriptNameHash() const { return m_ScriptNameHash; }

	// PURPOSE: Get the filename of the script that this thread is running
	// NOTES:	This needs more work to figure out how to manage these variables.
	const char *GetScriptName() const { return m_ScriptName; }

	// PURPOSE:	Reserve space for fixed number of threads
	// PARAMS:	count - Maximum number of threads we will support simultaneously
	//			stackSize - Default stack size to allocate for each thread.  Set to zero
	//				to allocate multiple stack sizes instead.
	// NOTES:	Reimplement this in a subclass and call that version if you subclass
	//			the script thread.
	static void AllocateThreads(int count,int stackSize = c_DefaultStackSize);

	// PURPOSE:	Allocate thread stacks of a particular size.  There are always twice as
	//			many possible thread stacks as there are threads in the system, although
	//			they do not all have to get allocated.
	// PARAMS:	count - Number of stacks to allocate
	//			stackSize - Size of the stack
	static void AddThreadStack(int count,int stackSize);

	// PURPOSE:	Update TIMERA and TIMERB values and then call Run.
	// PARAMS:	insnCount - As per scrThread::Run
	// RETURNS:	New thread state as per scrThread::Run
	// NOTES:	This functionality is separated out mostly for single-stepping support
	virtual State Update(int insnCount);

	// PURPOSE:	Updates all active script threads
	// PARAMS:	insnCount - Maximum number of instructions to execute (across all threads).
	//				If 0 is passed in here, we substitute in MAX_INT instead.
	// RETURNS:	True if any threads existed and were not in the ABORTED state.
	// NOTES:	Internally tracks the last thread to run and always starts with the next
	//			thread so that it's impossible for any one thread to starve out the entire
	//			system.
	static bool UpdateAll(int insnCount);

	// PURPOSE:	Initialize command hash table
	static void InitClass();

	// PURPOSE: Free all memory allocated by InitClass
	static void ShutdownClass();

	// PURPOSE:	Associate a command with a particular handler function.
	// PARAMS:	hashCode - Magic number associated with native.
	//			handler - Address of handler function.
	// NOTES:	Will assert out on a hash collision (should be very rare), in which case
	//			you will have to rename the offending function and hope for the best.
	static void RegisterCommand(u64 hashCode,void (*handler)(Info&) SCRIPT_DEBUGGING_ONLY(, const char* name, const char* file, scrSignature sig));
#if !__FINAL
	static void RegisterUnusedCommand(u64 hashCode,void (*handler)(Info&) SCRIPT_DEBUGGING_ONLY(, const char* name, const char* file, scrSignature sig));
#endif

	static scrSignature LookupCommandSignature(u32 hashCode);

	// PURPOSE:	Register all built-in commands (TIMERA, TIMERB, WAIT, etc)
	static void RegisterBuiltinCommands();
	
	// PURPOSE:	Set the time step value returned by the TIMESTEP script function and used by WAIT.
	static void SetTimeStep(float t) { sm_TimeStep = t; }
	
	// PURPOSE:	Retrieve the current script timestep
	static float GetTimeStep() { return sm_TimeStep; }

	// PURPOSE:	Set the time step value returned by the TIMESTEPUNWARPED script function and used by WAITUNWARPED.
	static void SetTimeStepUnwarped(float t) { sm_TimeStepUnwarped = t; }

	// PURPOSE:	Set whether the game is paused to be used by WAITUNPAUSED.
	static void SetIsPaused(bool b) { sm_IsPaused = b; }

	// PURPOSE:	Set whether script updates are blocked.
	static void SetIsBlocked(bool b) { sm_IsBlocked = b; }
	
	// PURPOSE:	Retrieve the current script unwarped timestep
	static float GetTimeStepUnwarped() { return sm_TimeStepUnwarped; }

	// PURPOSE:	Set whether the script timers (TIMERA and TIMERB) should update when the IsPaused flag is set (true) or whether they should pause (false).
	//			DOES NOT affect WAIT (at the moment)
	static void SetUpdateTimersWhenPaused(bool b) { sm_UpdateTimersWhenPaused = b; }
	
	// PURPOSE:	Create a new thread attached to the particular program ID.
	// PARAMS:	filename - file name to load (overload assumes they're the same)
	//			progname - Program to attach to
	//			argStruct - Optional pointer to a structure to copy into the script's address space
	//			argStructSize - sizeof(*argStruct), in bytes
	//			stackSize - Size of the stack (in words)
	// RETURNS:	New thread identifier (guaranteed unique forever) or NULL if no threads available
	static scrThreadId CreateThread(const char *progname,const void *argStruct = NULL,int argStructSize = 0,int stackSize = c_DefaultStackSize);

	// PURPOSE:	Create a new thread attached to the particular program ID.
	// PARAMS:	progId - Program ID to attach to
	//			argStruct - Optional pointer to a structure to copy into the script's address space
	//			argStructSize - sizeof(*argStruct), in bytes
	//			stackSize - Size of the stack (in words)
	// RETURNS:	New thread identifier (guaranteed unique forever) or NULL if no threads available
	static scrThreadId CreateThread(scrProgramId progId,const void *argStruct = NULL,int argStructSize = 0,int stackSize = c_DefaultStackSize);

	// PURPOSE:	Returns a pointer to a thread given a thread identifier
	// PARAMS:	id - Thread id to search for
	// RETURNS:	Pointer to actual thread, if it exists; otherwise NULL.
	static scrThread* GetThread(scrThreadId id);

	// PURPOSE: Removes a thread from the active thread list, deletes the program the thread was running
	// PARAMS: id - Thread to kill
	static void KillThread(scrThreadId id);
	
	// PURPOSE: Removes a thread from the active thread list, deletes the program the thread was running
	// PARAMS: thread - Thread to kill
	static void KillThread(scrThread* thread) {
		KillThread(thread->m_Serialized.m_ThreadId);
	}

	// PURPOSE:	Kills all running threads
	static void KillAllThreads();

	// PURPOSE: Virtual entry point called by KillAllThreads on each thread.
	// NOTES:	If you derive from this, don't forget to call the base class version!
	virtual void Kill();

	// PURPOSE:	Scans the program code and makes sure that all native function calls are registered.
	//			Also replaces hashcodes of opcodes with actual function pointers for speed.
	static bool Validate(scrProgram &opcodes);

#if (USE_PROFILER_NORMAL || SCRIPT_PROFILING)
	// PURPOSE:	Generates a debug table with all the function names
	static bool GenerateProcNames(const scrProgram &opcodes);
#endif

#if USE_PROFILER_NORMAL
	// PURPOSE:	Generates Rocky events for all the functions
	static bool GenerateProfilerEvents(const scrProgram &program);
#endif

	// PURPOSE: Prepares a script for execution (or resourcing) on target platform.
	// NOTES:	The .sco format defines integer literals in the code stream in big-endian form
	//			but the interpreter expects them in native endian.
	static void SwapForPlatform(scrProgram &opcodes);

	// PURPOSE:	Returns the currently executing script thread *ON THIS CPU THREAD*, if any.
	static scrThread* GetActiveThread();
	static bool IsThisThreadRunningAScript();

	// PURPOSE:	Returns the currently executing script thread *ON ANY CPU THREAD*, if any.
	static scrThread* GetActiveThreadGlobal();

	// PURPOSE: Retrieves local variable at specified offset.  Used by debugger.
	scrValue *GetLocal(int offset);

	// PURPOSE: Retrieves static variable at specified offset.  Used by debugger.
	scrValue *GetStatic(int offset);

	// PURPOSE: Allows code to validate that a reference lies in the static data section
	bool IsValidStatic(scrValue *ref);

	// PURPOSE: Allows code to validate that a reference lies in the global data section
	static bool IsValidGlobal(scrValue *ref);

	// PURPOSE: Retrieves global variable at specified offset.  Used by debugger.
	static scrValue *GetGlobal(int offset);

	// PURPOSE: Sets or unsets a local breakpoint at the specified address
    // PARAMS:
    //    offset - the address of the breakpoint
    //    setOrUnSet - add or remove the breakpoint
    //    stopsGame - the breakpoint stops the game when hit
	void SetBreakpoint( int offset, bool setOrUnSet, bool stopsGame=false );

    // PURPOSE: Indicates if there is any type of breakpoint at the specified address.
    // PARAMS:
    //    offset - the address to check
    //    local - set if there's a local breakpont
    //    stopGame - set if the local breakpoint stops the game when hit
    // RETURNS: true or false
    bool HasBreakpoint( int offset, bool *stopGame=NULL );

	// PURPOSE: Continue after setting a breakpoint
	void Continue();

	// PURPOSE: Pause execution
	void Pause();

	// PURPOSE: Simulate the THROW command from C++ code.  Cannot be called within
	//			a NATIVE function on the currently executing script.
	void Throw(int value);

	// PURPOSE: Tells the thread to go into the halt state when the program counter
	//			leaves the half-open interval [minPc,maxPc).  This is used to step
	//			to the next source line.
	void SetStepRegion(int minPc,int maxPc);

	// PURPOSE: Returns program counter value for the specified (zero-based) stack level, or -1 if it doesn't exist.
	int GetProgramCounter(int level) const;

	// PURPOSE: Returns program currently attached to this thread.
	scrProgramId GetProgram() const { return m_Serialized.m_Prog; }

	// PURPOSE: Returns current VM state
	State GetState() const { return m_Serialized.m_State; }

	// PURPOSE: Sets current thread state to BLOCKED or RUNNING (but only if it was already BLOCKED or RUNNING)
	void SetThreadBlockState(bool flag) {
		if (flag && m_Serialized.m_State == RUNNING)
			m_Serialized.m_State = BLOCKED;
		else if (!flag && m_Serialized.m_State == BLOCKED)
			m_Serialized.m_State = RUNNING;
	}

	// PURPOSE:	Cancel any pending wait in this thread (assuming it has already started)
	void CancelWait() {
		m_Serialized.m_Wait = -1.0f;
	}

	// RETURNS: Reason this thread had aborted, if any; used by debugger.
	const char *GetAbortReason() const { return m_AbortReason; }

	// RETURNS: Number of threads allocated in AllocateThreads
	static int GetThreadCount();

	// RETURNS:	Thread object by index
	static scrThread& GetThreadByIndex(int i);

	// RETURNS: True if this is a valid reference
	bool IsValidReference(scrValue *ref);

	// PURPOSE: Internal debugging function.
	void DumpArrayInfo(scrValue *ref);

	// PURPOSE:	Execute a native command directly (for networking use)
	// PARAMS:	commandHash - Command to execute
	//			numParameters - Number of parameters
	//			pParameters - Parameter list
	static scrValue ExecuteNetworkCommand(u32 commandHash, int numParameters, scrValue *pParameters);

	// RETURNS:	Pointer to serializable state block
	const void *GetSerializedState() const { return &m_Serialized; }

	// RETURNS:	Pointer to serializable state block
	void *GetSerializedState() { return &m_Serialized; }

	// RETURNS: Size of the state block, in bytes; can change because we only serialize as much of the stack as we actually use
	int GetSerializedStateSize() const { return __FINAL? sizeof(m_Serialized) : sizeof(m_Serialized) - (MAX_CALLSTACK+1)*sizeof(int); }

	// RETURNS: Current stack, for serialization
	scrValue* GetStack() const { return m_Stack; }

	// RETURNS: Current stack size, for serialization
	int GetStackSize() const { return m_Serialized.m_SP * sizeof(scrValue); }

	// RETURNS: Total stack size, for serialization
	int GetTotalStackSize() const { return m_Serialized.m_StackSize * sizeof(scrValue); }

	// RETURNS: Current args data structure owned by the script (was the parameters passed in and possibly subsequently modified)
	scrValue* GetArgs() const { return &m_Stack[m_argStructOffset]; }

	// RETURNS: Size of args data structure owned by the script (was the parameters passed in and possibly subsequently modified)
	int GetArgsSize() const { return m_argStructSize; }

	// PURPOSE:	Loads global variable information
	static bool LoadGlobalVariableInfo(const char *filename);

	// RETURNS:	Number of global variables
	static int GetGlobalVariableCount() { return sm_Globals.GetCount(); }

	// PURPOSE:	Look up a global variable by name
	// PARAMS:	name - Name to look up
	// RETURNS:	Index of global variable, or -1 if not found.
	static int LookupGlobalVariable(const char *name);

	// PURPOSE: Returns the type of a global variable
	// PARAMS:	index - As returned by LookupGlobalVariable
	// RETURNS:	Variable type (scrValue enumerant)
	static int GetGlobalVariableType(int index) { return sm_Globals[index].BasicType; }
												  
	// PURPOSE: Returns the address of the global variable
	// PARAMS:	index - As returned by LookupGlobalVariable
	// RETURNS:	Pointer to global storage
	static scrValue* GetGlobalVariableAddress(int index);

	// RETURNS: Number of instructions the script has used (updated during Run operation)
	int GetInstructionsUsed() const { return m_iInstructionCount; }

#if SCRIPT_DEBUGGING
	// RETURNS: Maximum amount of the stack that has been used
	int GetMaxStackUse() const {return m_MaxStackUse;}
	static void* GetCmdFromName(const char *cmdName);
	// RETURNS: Time it took to update last time it was called (in seconds)
	float GetUpdateTime() const { return m_fUpdateTime; }
	// RETURNS: Time the script spent sending stuff to the console
	float GetConsoleOutputTime() const { return m_fConsoleOutputTime; }
	void AddToConsoleOutputTime(float time) { m_fConsoleOutputTime += time; }
	void ResetTimer(){ m_fUpdateTime = m_fConsoleOutputTime = 0.0f; }
	static void SetFaultOnFunc(const char *pFuncName=0,bool bPrintOnly=false,  bool bClearOnHit=false);
#endif

#if __DEV
	static const char *sm_DevCurrentCmdName;
	static u32 sm_DevCurrentCmdNameCounter;
	// RETURNS: The name of the most-recently executed script native
	static const char *GetCurrentCmdName() {return sm_DevCurrentCmdName; }
#endif

	static int CheckStackAvailability(int stackSize);

#if SCRIPT_PROFILING
	static bool IsProfilingEnabled() { return sm_EnableScriptProfiling; }
	struct ProfEntry {
		int SelfIndex;
		u32 InclTicks, InclInsns;
		u32 ExclTicks, NatvTicks, ExclInsns;
		u32 PeakExclTicks, PeakNatvTicks, PeakExclInsns;
		// You generally want to sort by highest value first, so these sort functions are opposite of normal.
		static bool CompareByInclTime(const ProfEntry &a,const ProfEntry &b) { return a.InclTicks > b.InclTicks; }
		static bool CompareByInclInsns(const ProfEntry &a,const ProfEntry &b) { return a.InclInsns > b.InclInsns; }
		static bool CompareByExclTime(const ProfEntry &a,const ProfEntry &b) { return a.ExclTicks > b.ExclTicks; }
		static bool CompareByExclInsns(const ProfEntry &a,const ProfEntry &b) { return a.ExclInsns > b.ExclInsns; }
		static bool CompareByNatvTime(const ProfEntry &a,const ProfEntry &b) { return a.NatvTicks > b.NatvTicks; }
		static bool CompareByPeakExclTime(const ProfEntry &a,const ProfEntry &b) { return a.PeakExclTicks > b.PeakExclTicks; }
		static bool CompareByPeakExclInsns(const ProfEntry &a,const ProfEntry &b) { return a.PeakExclInsns > b.PeakExclInsns; }
		static bool CompareByPeakNatvTime(const ProfEntry &a,const ProfEntry &b) { return a.PeakNatvTicks > b.PeakNatvTicks; }
		static bool CompareByName_Thread(const ProfEntry &a,const ProfEntry &b) { return strcmp(sm_ProfileProg->ProcNames[a.SelfIndex],sm_ProfileProg->ProcNames[b.SelfIndex]) < 0; }
		static bool CompareByName_Overview(const ProfEntry &a,const ProfEntry &b) { return strcmp(sm_Threads[a.SelfIndex]->GetScriptName(),sm_Threads[b.SelfIndex]->GetScriptName()) < 0; }
		void Zero() {
			InclTicks = ExclTicks = NatvTicks = PeakExclTicks = PeakNatvTicks = InclInsns = ExclInsns = PeakExclInsns = 0;
		}
	};
	void ResetProfileData();
	void DisplayProfileData(void (*displayf)(bool severe,const char*,...));		// this dumps to display (via callback below), doesn't reset counters.
	static void DisplayProfileOverview(void (*displayf)(bool severe,const char*,...));
	static int sm_ProfileSortMethod;				// Inclusive Exclusive, Native, ExclusivePeak, NativePeak
	static u32 sm_ProfileResetInterval;
	static const int sm_ProfileSortMethodCount = 9;
	static const char *sm_ProfileSortMethodStrings[];		// for the combo box
	static scrProgram *sm_ProfileProg;						// current program being profiled (when not in overview mode)
#endif

#if !__NO_OUTPUT
    // PURPOSE: Our sysStack::PrePrintStackTrace() function that prints the stack track for
    //    the currently executing thread.
    static void PrePrintStackTrace();
	static void SetPrintStackPrefix(const char* prefix) { FastAssert(prefix); sm_PrintStackPrefix = prefix; }
	void CaptureStackTrace(int *addresses, int kMaxAddresses);
	static void DefaultPrintFn(const char* fmt, ...);
#endif	//	!__NO_OUTPUT

	bool PrintStackTrace(void (*printFn)(const char *fmt,...) OUTPUT_ONLY( = DefaultPrintFn) );

#if USE_PROFILER
	static void TryAutoSample(const scrThread::Serialized& ser);
	static void SaveCallstackForRocky(const scrThread::Serialized& ser, u64 timestamp);
	static void ResolveAddressForRocky(size_t address, Profiler::ScriptSymbol& outSymbol);
#endif //USE_PROFILER

#if SCRIPT_DEBUGGING
    // PURPOSE: Parse the -nativetrace command line argument for actions to take on natives. This should be done after all
	//    script natives have been registered. It is valid (though redundant) to call this function more than once.
	static void ParseNativeTraceCommandLine();
#endif

	Priority GetThreadPriority() const { return m_Serialized.m_Priority; }
	void SetThreadPriority(Priority p) { Assert((p >= THREAD_PRIO_HIGHEST && p <= THREAD_PRIO_LOWEST) || p==THREAD_PRIO_MANUAL_UPDATE); m_Serialized.m_Priority = p; }

	// Used in AutoGPUCapture for script timing.
	static atArray<scrThread*> &GetScriptThreads() { return sm_Threads; }
#if RSG_PC && RSG_CPU_X64
	static atMap<sysObfuscated<u32>, sysObfuscated<u64>> &GetProgRunFunctions() { return sm_ProgIdRunFunctions;}
#endif
	static void StartNewScriptWithArgs(Info &info);
	static void GetStackSummary(int &outStackInUse,int &outStackTotal);

#if !__FINAL
	static void StartMonitoringScriptArraySize(s32 *pAddressToMonitor);
	static bool CheckIfMonitoredScriptArraySizeHasChanged(const char *pStringToDisplay, s32 integerToDisplay);
#endif	//	!__FINAL

protected:
	static u32 Run(scrValue * __restrict stack,scrValue ** __restrict globals,const scrProgram &pt,Serialized * __restrict ser);

#if !__FINAL
    atMap<int, bool> m_threadBreakpoints;
#endif // !__FINAL

    struct ThreadStack {
		scrThread *m_Owner;
		int m_StackSize;
		scrValue *m_Stack;
	};

	static atArray<ThreadStack> sm_Stacks;
	static void (*OriginalPrePrintStackTrace)();

	State Fault(int pc,const char *string, scrThreadFaultID faultType);
	static void FaultCommon(const u8 **opcodesTbl,Serialized * __restrict ser);
	static int GetInstructionSize(const scrProgram& opcode,int pc);

	static int TimerA();
	static int TimerB();
	static void SetTimerA(int i);
	static void SetTimerB(int i);
	static float TimeStep();
	static float TimeStepUnwarped();
	static void Wait(Info &info);
	static void WaitUnwarped(Info &info);
	static void WaitUnpaused(Info &info);
	static void StartNewScript(Info &info);
	static void StartNewScriptWithNameHash(Info &info);
	static void StartNewScriptWithNameHashAndArgs(Info &info);
	bool AllocateStack(int stackSize);

	void Pushi(int i) { m_Stack[m_Serialized.m_SP++].Int = i; }
	void Pushf(float f) { m_Stack[m_Serialized.m_SP++].Float = f; }
	int Popi() { return m_Stack[--m_Serialized.m_SP].Int; }
	float Popf() { return m_Stack[--m_Serialized.m_SP].Float; }
	int &Topi() { return m_Stack[m_Serialized.m_SP-1].Int; }
	float &Topf() { return m_Stack[m_Serialized.m_SP-1].Float; }

#if SCRVALUE_OFFSETS
	public:
	static unsigned EncodePointer(void *ptr);
	static void *DecodePointer(unsigned ofs);
	protected:
#define scrEncodePointer(x)		scrThread::EncodePointer(x)
#define scrDecodePointer(x)		scrThread::DecodePointer(x)
#define scrEncodeReference(x)	scrEncodePointer(x)
#define scrDecodeReference(x)	static_cast<scrValue*>(scrDecodePointer(x))
#define scrEncodeString(x)		scrEncodePointer(const_cast<char*>(x))
#define scrDecodeString(x)		static_cast<const char*>(scrDecodePointer(x))
#define scrStringLiteral(n,x)	static const char *n = StringDuplicate(x)
#else
#define scrEncodePointer(x)		(x)
#define scrDecodePointer(x)		(x)
#define scrStringLiteral(n,x)	const char *n = x
#define scrEncodeString(x)		(x)
#define scrDecodeString(x)		(x)
#define scrEncodeReference(x)	(x)
#define scrDecodeReference(x)	(x)
#endif

	void Pushr(scrValue *r) { m_Stack[m_Serialized.m_SP++].Reference = scrEncodeReference(r); }
	scrValue* Popr() { return scrDecodeReference(m_Stack[--m_Serialized.m_SP].Reference); }
	scrValue* Topr() { return scrDecodeReference(m_Stack[m_Serialized.m_SP-1].Reference); }

	void Pusha(scrAny any) { m_Stack[m_Serialized.m_SP++].Any = any; }
	scrAny Popa() { return m_Stack[--m_Serialized.m_SP].Any; }
	scrAny &Topa() { return m_Stack[m_Serialized.m_SP-1].Any; }

	Serialized m_Serialized;
	scrValue* m_Stack;
	int	m_iInstructionCount;
	int m_argStructSize;
	int m_argStructOffset;
	static float sm_TimeStep;			// Current time step
	static float sm_TimeStepUnwarped;	// Current time step with no game dependant warping (bullet time/slowmo)
	static bool sm_IsPaused;			// Whether the game is paused
	static bool sm_IsBlocked;			// Whether this script thread is blocked from updating
	static bool sm_UpdateTimersWhenPaused; // Whether TIMERA and TIMERB are updating while the IsPaused flag is set
	static atArray<scrThread*> sm_Threads;
	static u32  sm_ThreadId;		// Next thread ID
	static atArray<Global> sm_Globals;
#if SCRIPT_DEBUGGING
	int m_MaxStackUse;
	float m_fUpdateTime;
	float m_fConsoleOutputTime;
#endif
#if SCRIPT_PROFILING
	static bool sm_EnableScriptProfiling;
	static const int MaxStackDepth = 64;
	atRangeArray<u16,MaxStackDepth> m_ProfStack;
	ProfEntry m_ProfTotals;								// Totals for the thread.
	atArray<ProfEntry> m_ProfBuffer;
	u32 m_ProfStackCount;
	u32 m_ProfUpdateCount;
#endif

	const char *m_AbortReason;

	u32 m_ScriptNameHash;
	char m_ScriptName[64];

#if EXECUTABLE_SCRIPTS
	scrExecState *m_ExecState;
#endif

#if __BANK
	FaultHandlerFunc m_debugFaultHandler;
    ThreadBreakpointFunc m_debugBreakpointCallback;
#endif
	static const char* sm_PrintStackPrefix;

#if SCRIPT_DEBUGGING
	static void CreateNativeTraceBank();
	static bkBank* ms_pNativeTraceBank;
	static bkButton* ms_pCreateNativeTraceButton;
#endif

private:

	static void StartNewScriptInternal(scrThread::Info &info, scrThreadId id);

	// Doesn't do anything. We expect the game code to have its own function to override this one (see StartNewThreadWithNameHashOverride).
	static scrThreadId CreateThreadWithNameHash(atHashValue hashOfProgName,const void *argStruct = NULL,int argStructSize = 0,int stackSize = c_DefaultStackSize);

private:
#if !__FINAL
	static CScriptArraySizeMonitor ms_scriptArraySizeMonitor;
#endif	//	!__FINAL
};

typedef void (*scrCmd)(scrThread::Info&);

#if __BANK

inline void scrThread::SetDebugFaultHandler( FaultHandlerFunc del )
{
	m_debugFaultHandler = del;
}

inline void scrThread::SetDebugBreakpointCallback( ThreadBreakpointFunc del )
{
    m_debugBreakpointCallback = del;
}

#endif // __BANK



#if SCRIPT_DEBUGGING

// A class to track script cut out time, currently only wired up to remove times for any console output / asserts
class CScriptCutoutTimeTracker
{
public:
	CScriptCutoutTimeTracker()
	{
		m_preTime = sysTimer::GetTicks();
		++m_funcCount;
	}

	~CScriptCutoutTimeTracker()
	{
		--m_funcCount;
		if ( m_funcCount == 0 )
		{
			utimer_t delta = sysTimer::GetTicks() - m_preTime;
			scrThread::GetActiveThread()->AddToConsoleOutputTime(delta * sysTimerConsts::TicksToSeconds);			
		}
	}

private:

	utimer_t	m_preTime;
	static int	m_funcCount;
};

#define SCRIPT_TIME_CUTOUT()	CScriptCutoutTimeTracker track;

#else	// SCRIPT_DEBUGGING

#define SCRIPT_TIME_CUTOUT()

#endif	// SCRIPT_DEBUGGING

// Semi-generic hash, only works for POD's
template <typename _T> class scrCommandHash
{
private:
	static const int ToplevelSize = 256;	// Must be power of two
	static const int PerBucket = 7;
	struct Bucket {
		// Data arranged this way for optimal packing on x64
#if 1 //RSG_PC
		sysObfuscated<Bucket *, false> obf_Next;
		_T Data[PerBucket];
		sysObfuscated<u32, false> obf_Count;
		sysObfuscated<u64, false> obf_Hashes[PerBucket];
		u64							plainText_Hashes[PerBucket];
#else //RSG_PC
		Bucket * Next;
		_T Data[PerBucket];
		u32 Count;
		u64 Hashes[PerBucket];
#endif //RSG_PC
	};
public:
	void RegistrationComplete(bool val)
	{
		m_bRegistrationComplete = val;
	}
	void Init()
	{
		m_Occupancy = 0;	
		m_bRegistrationComplete = false;
		for (int i=0; i<ToplevelSize; i++)
			m_Buckets[i] = NULL;
	}
	void Kill()
	{
		for (int i=0; i<ToplevelSize; i++) {
			Bucket *b = m_Buckets[i];
			while (b) {
				char *old = (char*) b;
				b = b->obf_Next.Get();
				delete[] old;
			}
			m_Buckets[i] = NULL;
		}
		m_Occupancy = 0;
	}
	void Insert(u64 hashcode,_T cmd)
	{
		Assert(hashcode);
		Assert(cmd);		// Lookup returns 0 on failure, so make sure it can't be confused with valid data
		Assertf(!Lookup(hashcode),"Duplicate hashcode %" I64FMT "x in script command hash table",hashcode);
		Bucket * b = m_Buckets[hashcode & (ToplevelSize-1)];
		// If this chain is empty, or the first bucket is full, allocate and patch in a new bucket
		if (!b || b->obf_Count.Get() == PerBucket) {
			Bucket *nb = (Bucket*) rage_new char[sizeof(Bucket)];
			nb->obf_Next.Set(m_Buckets[hashcode & (ToplevelSize-1)]);
			nb->obf_Count.Set(0);
			b = m_Buckets[hashcode & (ToplevelSize-1)] = nb;
		}
		b->obf_Hashes[b->obf_Count.Get()].Set(hashcode);
		b->plainText_Hashes[b->obf_Count.Get()] = 0; //hashcode;
		b->Data[b->obf_Count] = cmd;
		OUTPUT_ONLY(u32 i = b->obf_Count.Get());

		b->obf_Count.Set(b->obf_Count.Get()+1);		//inc count

	
		//Assertf( b->plainText_Hashes[i] == b->obf_Hashes[i].Get(), "plaintext (%" I64FMT "x) doesn't match obfuscated (%" I64FMT "x : %" I64FMT "x @ %p)", b->plainText_Hashes[i], b->obf_Hashes[i].Get(), *((u64*)&b->obf_Hashes[i]), &b->obf_Hashes[i]);
		Assertf( hashcode == b->obf_Hashes[i].Get(), "hashcode (%" I64FMT "x) doesn't match obfuscated (%" I64FMT "x : %" I64FMT "x @ %p)", hashcode, b->obf_Hashes[i].Get(), *((u64*)&b->obf_Hashes[i]), &b->obf_Hashes[i]);

		Displayf("[scrCommandHash debug]  Insert to bucket (%p) -> plaintext (%" I64FMT "x)    obfuscated (%" I64FMT "x : %" I64FMT "x @ %p)", b, b->plainText_Hashes[i], b->obf_Hashes[i].Get(), *((u64*)&b->obf_Hashes[i]), &b->obf_Hashes[i]);

		Assertf( Lookup(hashcode) != 0, "hashcode (%" I64FMT "x) has just been inserted, but Lookup returned 0!", hashcode);

	}
	_T Lookup(u64 hashcode)
	{
		Bucket *b = m_Buckets[hashcode & (ToplevelSize-1)];
		while (b) {
			for (u32 i=0; i<b->obf_Count.Get(); i++)
				if (b->obf_Hashes[i].Get() == hashcode)
					return b->Data[i];
			b = b->obf_Next.Get();
		}
/*
		if (m_bRegistrationComplete)
		{
			Displayf("[scrCommandHash] BUCKET DUMP!!!");
			b = m_Buckets[hashcode & (ToplevelSize-1)];
			while (b) {
				Displayf("bucket %p (count : %d)", b, b->obf_Count.Get());
				for (u32 i=0; i<b->obf_Count.Get(); i++)
				{
					Displayf("	(%d) hash %" I64FMT "x : obfuscates to (%" I64FMT "x : %" I64FMT "x @ %p)", i, b->plainText_Hashes[i], b->obf_Hashes[i].Get(), *((u64*)&b->obf_Hashes[i]), &b->obf_Hashes[i]);
				}
				b = b->obf_Next.Get();
			}
		}
*/
		return 0;
	}
private:
#if 0 //RSG_PC
	//sysObfuscated<Bucket *, false> m_Buckets[ToplevelSize];
	Bucket *						m_Buckets[ToplevelSize];
	sysObfuscated<int, false>		m_Occupancy;
#else //RSG_PC
	Bucket*		m_Buckets[ToplevelSize];
	int			m_Occupancy;
#endif //RSG_PC
	bool		m_bRegistrationComplete;
};
}	// namespace rage

#endif
