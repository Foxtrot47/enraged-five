// 
// scr_grcore/grcore.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "script/wrapper.h"

#include "grcore/setup.h"
#include "grcore/im.h"
#include "grcore/viewport.h"

using namespace rage;

namespace rageScriptBindings {

static grcSetup *s_Setup;

static void BeginUpdate()
{
	s_Setup->BeginUpdate();
}


static void EndUpdate()
{
	s_Setup->EndUpdate();
}


static void BeginDraw()
{
	s_Setup->BeginDraw();
	grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
}


static void EndDraw()
{
	s_Setup->EndDraw();
}


static int GetWidth()
{
	return GRCDEVICE.GetWidth();
}


static int GetHeight()
{
	return GRCDEVICE.GetHeight();
}


static void Begin(int drawMode,int vertexCount)
{
	grcBegin((grcDrawMode)drawMode,vertexCount);
}


static void End()
{
	grcEnd();
}


static void Vertex2f(float x,float y)
{
	grcVertex3f(x,y,0);
}


static void Vertex3f(float x,float y,float z)
{
	grcVertex3f(x,y,z);
}


static void Vertex3fv(const Vector3 &v)
{
	grcVertex3f(v);
}


static void TexCoord2f(float s,float t)
{
	grcTexCoord2f(s,t);
}


static void Color3f(float r,float g,float b)
{
	grcColor3f(r,g,b);
}


static void Color4f(float r,float g,float b,float a)
{
	grcColor4f(r,g,b,a);
}




void Register_grcore(grcSetup &setup)
{
	s_Setup = &setup;

	SCR_REGISTER_SECURE(GRC_BEGIN_UPDATE,SCRHASH("GRC_BEGIN_UPDATE",0x51114a18),BeginUpdate);
	SCR_REGISTER_SECURE(GRC_END_UPDATE,SCRHASH("GRC_END_UPDATE",0x66700c46),EndUpdate);
	SCR_REGISTER_SECURE(GRC_BEGIN_DRAW,SCRHASH("GRC_BEGIN_DRAW",0x67b011c2),BeginDraw);
	SCR_REGISTER_SECURE(GRC_END_DRAW,SCRHASH("GRC_END_DRAW",0x6f203cd),EndDraw);
	SCR_REGISTER_SECURE(GRC_GET_WIDTH,SCRHASH("GRC_GET_WIDTH",0x1c6d5eb8),GetWidth);
	SCR_REGISTER_SECURE(GRC_GET_HEIGHT,SCRHASH("GRC_GET_HEIGHT",0xbdac538d),GetHeight);
	SCR_REGISTER_SECURE(GRC_BEGIN,SCRHASH("GRC_BEGIN",0xc525d683),Begin);
	SCR_REGISTER_SECURE(GRC_END,SCRHASH("GRC_END",0x470fb8a6),End);
	SCR_REGISTER_SECURE(GRC_VERTEX2F,SCRHASH("GRC_VERTEX2F",0x6d6c0325),Vertex2f);
	SCR_REGISTER_SECURE(GRC_VERTEX3F,SCRHASH("GRC_VERTEX3F",0x30a68f17),Vertex3f);
	SCR_REGISTER_SECURE(GRC_VERTEX3FV,SCRHASH("GRC_VERTEX3FV",0x99328556),Vertex3fv);
	SCR_REGISTER_SECURE(GRC_TEXCOORD2F,SCRHASH("GRC_TEXCOORD2F",0x88c5588c),TexCoord2f);
	SCR_REGISTER_SECURE(GRC_COLOR3F,SCRHASH("GRC_COLOR3F",0x6af2874a),Color3f);
	SCR_REGISTER_SECURE(GRC_COLOR4F,SCRHASH("GRC_COLOR4F",0x5990e567),Color4f);
}

}
