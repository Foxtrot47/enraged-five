// 
// scr_grcore/grcore.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SCR_GRCORE_GRCORE_H
#define SCR_GRCORE_GRCORE H

namespace rage {
	class grcSetup;
}

namespace rageScriptBindings {

	void Register_grcore(rage::grcSetup &);

};

#endif	// SCR_GRCORE_GRCORE H
