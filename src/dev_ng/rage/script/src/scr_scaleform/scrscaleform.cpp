// 
// scr_grcore/scaleform.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 
#include "script/thread.h"

#include "scaleform.h"

#include "profile/profiler.h"
#include "scaleform/channel.h"
#include "scaleform/scaleform.h"
#include "script/value.h"
#include "script/wrapper.h"
#include "string/stringhash.h"
#include "string/unicode.h"

EXT_PF_GROUP(Scaleform);
PF_TIMER(ExternalInterface, Scaleform);
PF_TIMER(InvokeAS, Scaleform);
PF_TIMER(SetASVars, Scaleform);

using namespace rage;

inline void InvalidArg(scrSignature DEV_ONLY(sig), const char* OUTPUT_ONLY(methodName), int OUTPUT_ONLY(arg))
{
#if __DEV
	char funcSig[256];
	sig.PrettyPrint(funcSig, 256, methodName);
#elif !__NO_OUTPUT
	const char* funcSig = methodName;
#endif

	sfErrorf("ExternalInterface: Can't convert argument %d to expected type. Rage function is %s", arg+1, funcSig);
}

void sfCallRageScriptFromFlash::Callback(GFxMovieView* movieView, const char* methodName, const GFxValue* args, UInt argCount)
{
	PF_FUNC(ExternalInterface);
	scrValue* values = Alloca(scrValue, argCount);

	USES_CONVERSION;

	u32 nameHash = atStringHash(methodName);

	scrSignature nativeSig = scrThread::LookupCommandSignature(nameHash);

	if (!nativeSig.IsValid())
	{
		sfErrorf("ExternalInterface: Method named %s isn't a rage script , or hasn't been registered as an EXPORTed method", methodName);
		return;
	}

	if (nativeSig.CountNumArgs() != (int)argCount)
	{
#if __DEV
		char funcSig[256];
		nativeSig.PrettyPrint(funcSig, 256, methodName);
		sfErrorf("ExternalInterface: Not enough arguments for NATIVE %s", funcSig);
#else
		sfErrorf("ExternalInterface: Not enough arguments for NATIVE %s", methodName);
#endif
		return;
	}

	for(u32 i = 0; i < argCount; i++)
	{
		switch(nativeSig.GetArgumentType(i))
		{
		case scrValue::BOOL:
			if (args[i].GetType() == GFxValue::VT_Number)
			{
				values[i].Int = (int)args[i].GetBool(); 
			}
			else
			{
				InvalidArg(nativeSig, methodName, i);
				return;
			}
			break;
		case scrValue::INT:
			if (args[i].GetType() == GFxValue::VT_Number)
			{
				values[i].Int = (int)args[i].GetNumber(); 
			}
			else
			{
				InvalidArg(nativeSig, methodName, i);
				return;
			}
			break;
		case scrValue::FLOAT:
			if (args[i].GetType() == GFxValue::VT_Number)
			{
				values[i].Float = (float)args[i].GetNumber(); 
			}
			else
			{
				InvalidArg(nativeSig, methodName, i);
				return;
			}
			break;
		case scrValue::STRING:
			if (args[i].GetType() == GFxValue::VT_String)
			{
				values[i].String = scrEncodeString(args[i].GetString()); 
			}
			else if (args[i].GetType() == GFxValue::VT_StringW)
			{

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 6263) // "_alloca" used in a loop. Not worried about blowing the stack here because the loop is relatively short.
#endif

				CompileTimeAssert(sizeof(wchar_t) == sizeof(char16));
				values[i].String = scrEncodeString(WIDE_TO_UTF8((const char16*)args[i].GetStringW()));

#if __WIN32
#pragma warning(pop)
#endif

			}
			else
			{
				InvalidArg(nativeSig, methodName, i);
				return;
			}
			break;
		default:
			sfErrorf("ExternalInterface: Flash can't call rage script functions with arguments of type %d", nativeSig.GetArgumentType(i));
			return;
		}
	}

	scrValue rageResult = scrThread::ExecuteNetworkCommand(nameHash, argCount, values);

	GFxValue sfResult;
	switch(nativeSig.GetReturnType())
	{
	case scrValue::UNKNOWN:
		return; // !!!! returning early, not setting a return value
	case scrValue::BOOL:
		sfResult.SetBoolean(rageResult.Int != 0); break;
	case scrValue::INT:
		sfResult.SetNumber((Double)rageResult.Int); break;
	case scrValue::FLOAT:
		sfResult.SetNumber((Double)rageResult.Float); break;
	case scrValue::STRING:
		sfResult.SetString(scrDecodeString(rageResult.String)); break;
	default:
		sfErrorf("ExternalInterface: Don't know how to return a value of type %d to flash", nativeSig.GetReturnType());
		return;
	}

	movieView->SetExternalInterfaceRetVal(sfResult);
}

SCR_DEFINE_NEW_POINTER_PARAM_AND_RET(::rage::sfScaleformMovieView);

namespace rage
{

namespace sfRageScriptBindings
{
	sfScaleformManager* g_Manager;

	sfScaleformMovieView* CreateMovie(const char* name)
	{
		sfAssertf(g_Manager, "No scaleform manager defined");
		sfScaleformMovie* movie = g_Manager->LoadMovie(name);
		if (!movie) 
		{
			sfErrorf("Couldn't load movie %s from script", name);
			return NULL;
		}
		sfScaleformMovieView* movieView = g_Manager->CreateMovieView(*movie);
		return movieView;
	}

	void DestroyMovie(sfScaleformMovieView* movieView)
	{
		sfAssertf(g_Manager, "No scaleform manager defined");
		sfScaleformMovie* movie = movieView->GetMovie();
		g_Manager->DeleteMovieView(movieView);
		g_Manager->DeleteMovie(movie);
	}

	void InvokeActionscript(sfScaleformMovieView* movieView, const char* functionName)
	{
		PF_FUNC(InvokeAS);
		sfAssertf(movieView, "Scaleform movie was missing, can't call %s", functionName);
		movieView->GetMovieView().Invoke(functionName, (GFxValue*)NULL, 0);
	}

	void InvokeActionscriptFloat(sfScaleformMovieView* movieView, const char* functionName, float arg0)
	{
		PF_FUNC(InvokeAS);
		sfAssertf(movieView, "Scaleform movie was missing, can't call %s", functionName);
		GFxValue args[1];
		args[0].SetNumber((Double)arg0);
		movieView->GetMovieView().Invoke(functionName, args, 1);
	}

	void InvokeActionscriptInt(sfScaleformMovieView* movieView, const char* functionName, int arg0)
	{
		PF_FUNC(InvokeAS);
		sfAssertf(movieView, "Scaleform movie was missing, can't call %s", functionName);
		GFxValue args[1];
		args[0].SetNumber((Double)arg0);
		movieView->GetMovieView().Invoke(functionName, args, 1);
	}

	void InvokeActionscriptString(sfScaleformMovieView* movieView, const char* functionName, const char* arg0)
	{
		PF_FUNC(InvokeAS);
		sfAssertf(movieView, "Scaleform movie was missing, can't call %s", functionName);
		GFxValue args[1];
		args[0].SetString(arg0);
		movieView->GetMovieView().Invoke(functionName, args, 1);
	}

	void InvokeActionscriptBool(sfScaleformMovieView* movieView, const char* functionName, bool arg0)
	{
		PF_FUNC(InvokeAS);
		sfAssertf(movieView, "Scaleform movie was missing, can't call %s", functionName);
		GFxValue args[1];
		args[0].SetBoolean(arg0);
		movieView->GetMovieView().Invoke(functionName, args, 1);
	}

	void GotoFrame(sfScaleformMovieView* movieView, int frameNumber)
	{
		sfAssertf(movieView, "Scaleform movie was missing");
		movieView->GetMovieView().GotoFrame(frameNumber);
	}

	void GotoLabel(sfScaleformMovieView* movieView, const char* label, int offset)
	{
		sfAssertf(movieView, "Scaleform movie was missing");
		movieView->GetMovieView().GotoLabeledFrame(label, offset);
	}

	bool IsPlaying(sfScaleformMovieView* movieView)
	{
		sfAssertf(movieView, "Scaleform movie was missing");
		return movieView->GetIsActive() && (movieView->GetMovieView().GetPlayState() != GFxMovie::Stopped);
	}

	bool IsVisible(sfScaleformMovieView* movieView)
	{
		sfAssertf(movieView, "Scaleform movie was missing");
		return movieView->GetIsVisible() && movieView->GetMovieView().GetVisible();
	}

	bool HasLooped(sfScaleformMovieView* movieView)
	{
		sfAssertf(movieView, "Scaleform movie was missing");
		return movieView->GetMovieView().HasLooped();
	}

	void SetVisible(sfScaleformMovieView* movieView, bool value)
	{
		sfAssertf(movieView, "Scaleform movie was missing");
		// use scaleform's setting for now
		movieView->GetMovieView().SetVisible(value);
	}

	void SetPlaying(sfScaleformMovieView* movieView, bool value)
	{
		sfAssertf(movieView, "Scaleform movie was missing");
		movieView->GetMovieView().SetPlayState(value ? GFxMovie::Playing : GFxMovie::Stopped);
	}

	int GetCurrentFrame(sfScaleformMovieView* movieView)
	{
		sfAssertf(movieView, "Scaleform movie was missing");
		return (int)movieView->GetMovieView().GetCurrentFrame();
	}

	const char* GetMovieName(sfScaleformMovieView* movieView)
	{
		sfAssertf(movieView, "Scaleform movie was missing");
		return movieView->GetMovie()->GetMovie().GetFileURL();
	}

	void SetFloat(sfScaleformMovieView* movieView, const char* varName, float value)
	{
		PF_FUNC(SetASVars);
		sfAssertf(movieView, "Scaleform movie was missing");
		sfAssertf(varName, "Variable name was missing");
		movieView->GetMovieView().SetVariableDouble(varName, (Double)value, GFxMovie::SV_Normal);
	}

	void SetInt(sfScaleformMovieView* movieView, const char* varName, int value)
	{
		PF_FUNC(SetASVars);
		sfAssertf(movieView, "Scaleform movie was missing");
		sfAssertf(varName, "Variable name was missing");
		movieView->GetMovieView().SetVariableDouble(varName, (Double)value, GFxMovie::SV_Normal);
	}

	void SetString(sfScaleformMovieView* movieView, const char* varName, const char* value)
	{
		PF_FUNC(SetASVars);
		sfAssertf(movieView, "Scaleform movie was missing");
		sfAssertf(varName, "Variable name was missing");
		movieView->GetMovieView().SetVariable(varName, value, GFxMovie::SV_Normal);
	}

	void SetBool(sfScaleformMovieView* movieView, const char* varName, bool value)
	{
		PF_FUNC(SetASVars);
		sfAssertf(movieView, "Scaleform movie was missing");
		sfAssertf(varName, "Variable name was missing");
		GFxValue val;
		val.SetBoolean(value);
		movieView->GetMovieView().SetVariable(varName, val, GFxMovie::SV_Normal);
	}

	void Register(sfScaleformManager* sfmanager)
	{
		g_Manager = sfmanager;

		SCR_REGISTER_SECURE_EXPORT(SF_CREATE_MOVIE, 0x23705624, CreateMovie);
		SCR_REGISTER_SECURE_EXPORT(SF_DESTROY_MOVIE, 0xf147bbf8, DestroyMovie);
		SCR_REGISTER_SECURE_EXPORT(SF_INVOKE_ACTIONSCRIPT, 0xcbb82d9b, InvokeActionscript);
		SCR_REGISTER_SECURE_EXPORT(SF_INVOKE_ACTIONSCRIPT_FLOAT, 0x9c24bd1e, InvokeActionscriptFloat);
		SCR_REGISTER_SECURE_EXPORT(SF_INVOKE_ACTIONSCRIPT_INT, 0x32f26bec, InvokeActionscriptInt);
		SCR_REGISTER_SECURE_EXPORT(SF_INVOKE_ACTIONSCRIPT_STRING, 0xfa256011, InvokeActionscriptString);
		SCR_REGISTER_SECURE_EXPORT(SF_INVOKE_ACTIONSCRIPT_BOOL, 0x9c5562bf, InvokeActionscriptBool);
		SCR_REGISTER_SECURE_EXPORT(SF_GOTO_FRAME, 0x014ec615, GotoFrame);
		SCR_REGISTER_SECURE_EXPORT(SF_GOTO_LABEL, 0xbbcc8fe5, GotoLabel);
		SCR_REGISTER_SECURE_EXPORT(SF_IS_PLAYING, 0x6020bb09, IsPlaying);
		SCR_REGISTER_SECURE_EXPORT(SF_IS_VISIBLE, 0x08590743, IsVisible);
		SCR_REGISTER_SECURE_EXPORT(SF_HAS_LOOPED, 0x0ef5a02c, HasLooped);
		SCR_REGISTER_SECURE_EXPORT(SF_SET_VISIBLE, 0x90b398d8, SetVisible);
		SCR_REGISTER_SECURE_EXPORT(SF_SET_PLAYING, 0x3487b7fc, SetPlaying);
		SCR_REGISTER_SECURE_EXPORT(SF_GET_CURRENT_FRAME, 0xb48e5fea, GetCurrentFrame);
		SCR_REGISTER_SECURE_EXPORT(SF_GET_MOVIE_NAME, 0x327bbd45, GetMovieName);
		SCR_REGISTER_SECURE_EXPORT(SF_SET_FLOAT, 0x15439ab7, SetFloat);
		SCR_REGISTER_SECURE_EXPORT(SF_SET_INT, 0xa587e809, SetInt);
		SCR_REGISTER_SECURE_EXPORT(SF_SET_STRING, 0x943e2897, SetString);
		SCR_REGISTER_SECURE_EXPORT(SF_SET_BOOL, 0xf0216e87, SetBool);
	}
}

}
