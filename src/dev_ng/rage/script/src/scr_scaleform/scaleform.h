// 
// scr_grcore/scaleform.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SCR_GRCORE_SCALEFORM_H 
#define SCR_GRCORE_SCALEFORM_H 

#include "scaleform/scaleform.h"

namespace rage {

class sfCallRageScriptFromFlash : public GFxExternalInterface
{
public:
	virtual void Callback(GFxMovieView* , const char* methodName, const GFxValue* args, UInt argCount);
};

namespace sfRageScriptBindings
{
	void Register(sfScaleformManager* manager);
}

} // namespace rage

#endif // SCR_GRCORE_SCALEFORM_H 
