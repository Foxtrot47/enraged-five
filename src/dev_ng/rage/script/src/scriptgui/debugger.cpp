// 
// script/debugger.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "debugger.h"
#include "widgeteditor.h"

#if __BANK

#include "atl/functor.h"
#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "bank/combo.h"
#include "bank/group.h"
#include "bank/list.h"
#include "bank/treelist.h"
#include "diag/output.h"
#include "file/asset.h"
#include "script/scrchannel.h"
#include "script/thread.h"
#include "script/program.h"
#include "system/memory.h"

#include <stdlib.h>
#include <limits.h>

#define HACK_HACK_HACK	1	// rag line numbers seem off-by-one relative to ours?

// Change these to modify the prefix before all script trace printouts, if you want it to stand out.
// The bizarre bit below allows it to show up in the error logs of CI build machine runs.
#define PREFIX	TPurple "debugger.cpp(%d): Error : [Script] "
#define SUFFIX	TNorm

namespace rage 
{

static int s_itemKey = 0;

///// TYPE HANDLERS /////

void scrDebugger::Type_t::SetupDisplay( Node_t* &root, scrThread *thread, scrDebugger *pDebugger, 
                                       scrDebugger::Item *item, bkTreeList *treelist, s32 nodeKey ) const 
{
    root = rage_new Node_t;
	root->Type = this;
	root->ValueAddr = item->Address;
    root->Debugger = pDebugger;

    scrValue *value = scrDebugger::Resolve( thread, root->ValueAddr );
    if ( value )
    {
        BuildTreeListNode( treelist, item->Name, (s32)(size_t)root, value, nodeKey );
    }
}

void scrDebugger::Type_t::RemoveDisplay( Node_t* node, bkTreeList *treelist ) const 
{
	treelist->RemoveNode( (s32)(size_t)node );
	delete node;
}

void scrDebugger::Type_t::UpdateDisplay( Node_t* node, bkTreeList *treelist, scrThread *thread ) const 
{
	char valbuf[ValueBufferSize];
	safecpy( valbuf, "<unavailable>", sizeof(valbuf) );
	
    scrValue *value = scrDebugger::Resolve( thread, node->ValueAddr );
	if ( value )
    {
        GetValue( valbuf, sizeof(valbuf), value );
    }
    
    if ( strcmp(valbuf,node->LastValue) )
    {
		treelist->SetItemValue( (s32)(size_t)node, valbuf );
		strcpy( node->LastValue, valbuf );
	}
}

void scrDebugger::Type_t::BuildTreeListNode( bkTreeList *treelist, const char *name, s32 key, int val, s32 nodeKey, 
                                            bool readOnly, bool updateParent ) const
{
    treelist->AddNode( name, key, bkTreeList::NodeDataTypeNative, nodeKey, false );
    treelist->AddReadOnlyItem( s_itemKey++, name, key );
    treelist->AddTextBoxItem( key, val, key, readOnly, updateParent );
    treelist->AddReadOnlyItem( s_itemKey++, GetName(), key );
}

void scrDebugger::Type_t::BuildTreeListNode( bkTreeList *treelist, const char *name, s32 key, float val, s32 nodeKey, 
                                            bool readOnly, bool updateParent ) const
{
    treelist->AddNode( name, key, bkTreeList::NodeDataTypeNative, nodeKey, false );
    treelist->AddReadOnlyItem( s_itemKey++, name, key );
    treelist->AddTextBoxItem( key, val, key, readOnly, updateParent );
    treelist->AddReadOnlyItem( s_itemKey++, GetName(), key );
}

void scrDebugger::Type_t::BuildTreeListNode( bkTreeList *treelist, const char *name, s32 key, const char *val, s32 nodeKey, 
                                            bool readOnly, bool updateParent ) const
{
    treelist->AddNode( name, key, bkTreeList::NodeDataTypeNative, nodeKey, false );
    treelist->AddReadOnlyItem( s_itemKey++, name, key );
    treelist->AddTextBoxItem( key, val, key, readOnly, updateParent );
    treelist->AddReadOnlyItem( s_itemKey++, GetName(), key );
}

void scrDebugger::Type_t::BuildTreeListNode( bkTreeList *treelist, const char *name, s32 key, bool val, s32 nodeKey, 
                                            bool readOnly, bool updateParent ) const
{
    treelist->AddNode( name, key, bkTreeList::NodeDataTypeNative, nodeKey, false );
    treelist->AddReadOnlyItem( s_itemKey++, name, key );
    treelist->AddTextBoxItem( key, val, key, readOnly, updateParent );
    treelist->AddReadOnlyItem( s_itemKey++, GetName(), key );
}

//#############################################################################

struct typeBOOL : public scrDebugger::Type_t 
{
	const char *GetName() const { return "BOOL"; }
	
    bool GetValue(char *dest,int destSize,const scrValue *value) const 
    {
		safecpy(dest,value->Int != 0? "true" : "false",destSize); 
		return true;
	}
	
    bool SetValue(scrValue *value,const char *newValue) const 
    {
		switch ( newValue[0] ) 
        {
			case 't': case 'T': case '1': value->Int = 1; return true;
			case 'f': case 'F': case '0': value->Int = 0; return true;
		}
		return false;
	}

    void BuildTreeListNode( bkTreeList *treelist, const char* name, s32 key, scrValue *value, s32 nodeKey ) const
    {
        bool val = value->Int != 0;
        scrDebugger::Type_t::BuildTreeListNode( treelist, name, key, val, nodeKey );
    }
};

//#############################################################################

struct typeINT : public scrDebugger::Type_t 
{
	const char *GetName() const { return "INT"; }

    bool GetValue(char *dest,int destSize,const scrValue *value) const 
    {
		formatf(dest,destSize,"%d",value->Int);
		return true;
	}
	
    bool SetValue(scrValue *value,const char *newValue) const 
    {
		value->Int = atoi(newValue);
		return true;
	}

    void BuildTreeListNode( bkTreeList *treelist, const char* name, s32 key, scrValue *value, s32 nodeKey ) const
    {
        scrDebugger::Type_t::BuildTreeListNode( treelist, name, key, value->Int, nodeKey );
    }
};

//#############################################################################

struct typeTYPEDEF : public scrDebugger::Type_t 
{
	const char *GetName() const { return "TYPEDEF"; }

	bool GetValue(char *dest,int destSize,const scrValue *value) const 
	{
		formatf(dest,destSize,"&%d",value->Int);
		return true;
	}

	bool SetValue(scrValue * /*value*/,const char * /*newValue*/) const 
	{
		return false;
	}

	void BuildTreeListNode( bkTreeList *treelist, const char* name, s32 key, scrValue *value, s32 nodeKey ) const
	{
		scrDebugger::Type_t::BuildTreeListNode( treelist, name, key, value->Int, nodeKey );
	}
};

//#############################################################################

struct typeFLOAT : public scrDebugger::Type_t 
{
	const char *GetName() const { return "FLOAT"; }

    bool GetValue(char *dest,int destSize,const scrValue *value) const 
    {
		formatf(dest,destSize,"%g",value->Float);
		return true;
	}
	
    bool SetValue(scrValue *value,const char *newValue) const 
    {
		value->Float = (float) atof(newValue);
		return true;
	}

    void BuildTreeListNode( bkTreeList *treelist, const char* name, s32 key, scrValue *value, s32 nodeKey ) const
    {
        scrDebugger::Type_t::BuildTreeListNode( treelist, name, key, value->Float, nodeKey );
    }
};

//#############################################################################

struct typeOBJECT : public scrDebugger::Type_t 
{
	typeOBJECT(const char *name) : Name(name) { }

    const char *GetName() const { return Name; }

    bool GetValue(char *dest,int destSize,const scrValue *value) const 
    {
		formatf(dest,destSize,value->Reference?"%p":"NULL",value->Reference); 
		return true;
	}
	
    bool SetValue(scrValue *,const char *) const 
    {
		return false;
	}

	ConstString Name;

    void BuildTreeListNode( bkTreeList *treelist, const char* name, s32 key, scrValue* /*value*/, s32 nodeKey ) const
    {
        scrDebugger::Type_t::BuildTreeListNode( treelist, name, key, (const char *)Name, nodeKey );
    }
};

//#############################################################################

struct typeVECTOR : public scrDebugger::Type_t 
{
	const char *GetName() const { return "VECTOR"; }
	
    bool GetValue(char *dest,int destSize,const scrValue *value) const 
    {
		formatf(dest,destSize,"<<%g,%g,%g>>",value[0].Float,value[1].Float,value[2].Float);
		return true;
	}
	
    bool SetValue(scrValue *value,const char *newValue) const
    {
		if (*newValue=='<')
			++newValue;
		if (*newValue=='<')
			++newValue;
		value[0].Float = (float) atof(newValue);
		newValue = strchr(newValue,',');
		if (newValue) 
        {
			value[1].Float = (float) atof(newValue+1);
			newValue = strchr(newValue+1,',');
			if (newValue)
				value[2].Float = (float) atof(newValue+1);
		}
		return true;
	}

	int GetSize(const scrValue*) const { return 3; }       

    void BuildTreeListNode( bkTreeList* treelist, const char* name, s32 key, scrValue* value, s32 nodeKey ) const
    {
        Vector3 v;
        v.x = value[0].Float;
        v.y = value[1].Float;
        v.z = value[2].Float;

        treelist->AddNode( name, key, bkTreeList::NodeDataTypeNative, nodeKey );
        treelist->AddReadOnlyItem( s_itemKey++, name, key );
        treelist->AddTextBoxItem( key, v, key ); 
        treelist->AddReadOnlyItem( s_itemKey++, GetName(), key );
    }
};

//#############################################################################

struct typeSTRING : public scrDebugger::Type_t 
{
	const char *GetName() const { return "STRING"; }
	
    bool GetValue(char *dest,int destSize,const scrValue *value) const 
    {
		const char *s = scrDecodeString(value->String);
		if (!s)
			s = "<null>";
#if __WIN32
		__try 
        {
#endif
			safecpy(dest,s,destSize);
#if __WIN32
		} __except(1) { safecpy(dest,"<invalid>",destSize); }
#endif
		return true;
	}
	
    bool SetValue(scrValue *,const char *) const 
    {
		return false;
	}

    void BuildTreeListNode( bkTreeList *treelist, const char* name, s32 key, scrValue *value, s32 nodeKey ) const
    {
        char strVal[scrDebugger::ValueBufferSize];
        GetValue( strVal, sizeof(strVal), value );
        scrDebugger::Type_t::BuildTreeListNode( treelist, name, key, strVal, nodeKey, true ); // STRINGs are read-only
    }
};

//#############################################################################

struct typeTEXT_LABEL : public scrDebugger::Type_t 
{
	typeTEXT_LABEL(int count) : Count(count) { sprintf(Name,"TEXT_LABEL_%d",count-1); }

    const char *GetName() const { return Name; }

    bool GetValue(char *dest,int destSize,const scrValue *value) const 
    {
		const char *s = (const char*) value;
		safecpy(dest,s,destSize<Count?destSize:Count);
		return true;
	}
	
    bool SetValue(scrValue *value,const char *newValue) const 
    {
		safecpy((char*)value,newValue,Count);
		return true;
	}
	
    int GetSize(const scrValue*) const { return Count / sizeof(scrValue); }
	
    int Count;
	char Name[32];

    void BuildTreeListNode( bkTreeList *treelist, const char* name, s32 key, scrValue *value, s32 nodeKey ) const
    {
        char valStr[scrDebugger::ValueBufferSize];
        GetValue( valStr, sizeof(valStr), value );
        scrDebugger::Type_t::BuildTreeListNode( treelist, name, key, valStr, nodeKey );
    }
};

//#############################################################################

struct typeBAG : public scrDebugger::Type_t 
{
    void UpdateDisplay( scrDebugger::Node_t* node, bkTreeList *treelist, scrThread *thread ) const 
    {
		scrDebugger::Node_t *i = node->Child;
		while ( i ) 
        {
			i->Type->UpdateDisplay(i,treelist,thread);
			i = i->Sibling;
		}
	}

    void RemoveDisplay( scrDebugger::Node_t* node, bkTreeList *treelist ) const
    {
        scrDebugger::Node_t *i = node->Child;
        while ( i ) 
        {
            scrDebugger::Node_t *nextI = i->Sibling;
            i->Type->RemoveDisplay( i, treelist );
            i = nextI;
        }
        
        scrDebugger::Type_t::RemoveDisplay( node, treelist );
    }
};

//#############################################################################

struct typeARRAY : public typeBAG 
{
	typeARRAY(int count,const Type_t *baseType) : Count(count), BaseType(baseType) { }
	const char *GetName() const { return "ARRAY"; }
	bool GetValue(char *,int ,const scrValue *) const { return false; }
	bool SetValue(scrValue *,const char *) const { return false; }

	// Arrays store their size in a hidden word at the beginning.
	// This allows fixed-size and variable-sized arrays to be mixed at runtime.
	// We increment the base offset before passing it into the inner type so that
	// multidimensional arrays work properly.
	int GetSize(const scrValue* value) const { return 1 + (value? value->Int * BaseType->GetSize(value+1) : Count * BaseType->GetSize(NULL)); }

    void SetupDisplay( scrDebugger::Node_t* &root, scrThread *thread, scrDebugger *pDebugger, 
        scrDebugger::Item *item, bkTreeList *treelist, s32 nodeKey ) const 
    {
        root = rage_new scrDebugger::Node_t;		// placeholder
		root->Type = this;
        root->ValueAddr = item->Address;
        root->Debugger = pDebugger;

        int arrayNodeKey = (s32)(size_t)root;
        scrDebugger::Type_t::BuildTreeListNode( treelist, item->Name, arrayNodeKey, "", nodeKey, true );  // top-node's value column is read-only

		scrDebugger::Node_t **child = &root->Child;
		scrValue *v = scrDebugger::Resolve( thread, item->Address );
		int count = v ? v->Int : Count;
		int baseSize = BaseType->GetSize( v ? v + 1 : 0 );
		for (int i=0; i < count; i++) 
        {            
            int address = item->Address + 1 + (i*baseSize);
            scrDebugger::Item *pSubItem = scrDebugger::Item::FindItem( pDebugger, item->SubItems, address );
            if ( pSubItem == NULL )
            {
                char buf[24];
                formatf(buf,sizeof(buf),"[%d]",i);

                scrDebugger::Item &subItem = item->SubItems.Grow();
                subItem.Address = address;
                subItem.Frame = item->Frame;
                safecpy( subItem.Name, buf, sizeof(subItem.Name) );
                subItem.SetScope( item->GetScope() );
                subItem.Type = const_cast<scrDebugger::Type_t *>( BaseType );    

                pSubItem = &subItem;
            }

			pSubItem->Type->SetupDisplay( *child, thread, pDebugger, pSubItem, treelist, arrayNodeKey );
			child = &(*child)->Sibling;
		}
	}

    void BuildTreeListNode( bkTreeList* /*treelist*/, const char* /*name*/, s32 /*key*/, scrValue* /*value*/, s32 /*nodeKey*/ ) const
    {
        // do nothing
    }

	int Count;
	const Type_t *BaseType;
};

//#############################################################################

struct typeSTRUCT : public typeBAG 
{
	struct Member 
    {
		const Type_t *Type;
		ConstString Name;
		int Offset;
	};
	
    typeSTRUCT(const char *name,int memberCount) : Name(name), Members(0,memberCount), Size(0) { }
	~typeSTRUCT() { }
	
    void AddMember(const Type_t *type,const char *name,int offset) 
    {
		Member &m = Members.Append();
		m.Type = type;
		m.Name = name;
		m.Offset = offset;
		// Structures cannot contain variable-length arrays so passing NULL is okay here.
		Size += type->GetSize(NULL);
	}

	const char *GetName() const { return Name; }
	
    bool GetValue(char *,int ,const scrValue *) const { return false; }
	
    bool SetValue(scrValue *,const char *) const { return false; }
	
    int GetSize(const scrValue*) const { return Size; }

    void SetupDisplay( scrDebugger::Node_t* &root, scrThread *thread, scrDebugger *pDebugger, 
        scrDebugger::Item *item, bkTreeList *treelist, s32 nodeKey ) const 
    {
        root = rage_new scrDebugger::Node_t;		// placeholder
		root->Type = this;
        root->ValueAddr = item->Address;
        root->Debugger = pDebugger;

        int structNodeKey = (s32)(size_t)root;
        treelist->AddNode( item->Name, structNodeKey, bkTreeList::NodeDataTypeStruct, nodeKey, false );
        treelist->AddReadOnlyItem( s_itemKey++, item->Name, structNodeKey );
        treelist->AddReadOnlyItem( s_itemKey++, "", structNodeKey );    // top-node's value column is read-only
        treelist->AddReadOnlyItem( s_itemKey++, GetName(), structNodeKey );

        scrDebugger::Node_t **child = &root->Child;
		for (int i=0; i<Members.GetCount(); i++) 
        {
            int address = item->Address + Members[i].Offset;
            scrDebugger::Item *pSubItem = scrDebugger::Item::FindItem( pDebugger, item->SubItems, address );
            if ( pSubItem == NULL )
            {
                scrDebugger::Item &subItem = item->SubItems.Grow();
                subItem.Address = address;
                subItem.Frame = item->Frame;
                safecpy( subItem.Name, Members[i].Name.m_String, sizeof(subItem.Name) );
                subItem.SetScope( item->GetScope() );
                subItem.Type = const_cast<scrDebugger::Type_t *>( Members[i].Type );

                pSubItem = &subItem;
            }

            pSubItem->Type->SetupDisplay( *child, thread, pDebugger, pSubItem, treelist, structNodeKey );
            child = &(*child)->Sibling;
		}
	}

    void BuildTreeListNode( bkTreeList* /*treelist*/, const char* /*name*/, s32 /*key*/, scrValue* /*value*/, s32 /*nodeKey*/ ) const
    {
        // do nothing
    }

	ConstString Name;
	atArray<Member> Members;
	int Size;
};

//#############################################################################

struct typeENUM : public scrDebugger::Type_t
{
	struct Member 
    {
		ConstString Name;
		int Value;
	};
	
    typeENUM(const char *name,int memberCount) : Name(name), Members(0,memberCount) { }
	~typeENUM() { }
	
    void AddMember(const char *name,int value) 
    {
		Member &m = Members.Append();
		m.Name = name;
		m.Value = value;
	}
	
    const char *GetName() const { return Name; }
	
    bool GetValue(char *dest,int destSize,const scrValue *value) const 
    {
		for (int i=0; i<Members.GetCount(); i++)
        {
            if (Members[i].Value==value->Int) 
            {
				formatf(dest,destSize,"%s",Members[i].Name.m_String);
				return true;
			}
        }
		formatf(dest,destSize,"%d",value->Int);
		return true;
	}

	bool SetValue(scrValue *value,const char *newValue) const 
    {
		for (int i=0; i<Members.GetCount(); i++)
        {
			if (!stricmp(Members[i].Name,newValue)) 
            {
				value->Int = Members[i].Value;
				return true;
			}
        }
		value->Int = atoi(newValue);
		return true;
	}

    void BuildTreeListNode( bkTreeList* treelist, const char* name, s32 key, scrValue* value, s32 nodeKey ) const
    {
        char strValue[scrDebugger::ValueBufferSize];
        if ( GetValue(strValue,sizeof(strValue),value) )
        {
            scrDebugger::Type_t::BuildTreeListNode( treelist, name, key, strValue, nodeKey );
        }
    }

	ConstString Name;
	atArray<Member> Members;
};


//#############################################################################

void scrDebugger::Node_t::UpdateList( scrDebugger::Node_t *i, bkTreeList *treelist, scrThread *thread ) 
{
    if ( i == NULL )
    {
        return;
    }

    bool getThreads = thread == NULL;

	while ( i ) 
    {
        if ( getThreads )
        {
            thread = scrThread::GetThread( i->Debugger->m_ThreadId );
        }

        if ( (thread != NULL) && (thread->GetState() != scrThread::ABORTED) )
        {
            i->Type->UpdateDisplay( i, treelist, thread );
        }

        i = i->Sibling;
	}
}

void scrDebugger::Node_t::RemoveList( scrDebugger::Node_t *i, bkTreeList *treelist)
{
	while ( i ) 
    {
		Node_t *nextI = i->Sibling;
		i->Type->RemoveDisplay( i, treelist );
		i = nextI;
	}
}

scrDebugger::Type_t* scrDebugger::ParseType(const char *typeString) 
{
	// Compound types don't need STRUCT or ENUM tag, they already
	// must be globally unique.
	if (!strncmp(typeString,"STRUCT ",7))
		typeString += 7;
	else if (!strncmp(typeString,"ENUM ",5))
		typeString += 5;

	if (typeString[0]=='[') 
    {
		int count = atoi(typeString+1);
		typeString = strchr(typeString,']')+1;
		Type_t *newType = rage_new typeARRAY(count,ParseType(typeString));
		// Shove it into the hash so that it gets cleaned up properly
		char buf[32];
		sprintf(buf,"array_%p",newType);
		m_Types[ConstString(buf)] = newType;
		return newType;
	}
	else if (!strncmp(typeString,"OBJECT ",7)) 
    {
		Type_t *newType = rage_new typeOBJECT(typeString+7);
		// Shove it into the hash so that it gets cleaned up properly
		char buf[32];
		sprintf(buf,"object_%p",newType);
		m_Types[ConstString(buf)] = newType;
		return newType;
	}
	else if (Type_t **tp = m_Types.Access(typeString)) 
    {
		return *tp;
	}
	else 
    {
		Assertf(false,"Unknown typeString '%s' encountered.",typeString);
		return NULL;
	}
}

///// DEBUGGER /////

atArray< scrDebugger* > scrDebugger::sm_scriptDebuggers;
bkBank* scrDebugger::sm_debuggerBank = NULL;
bool scrDebugger::sm_killDebuggersUponCompletion = false;
atArray<scrDebugger::Item> scrDebugger::sm_masterGlobalsItems;
bkTreeList* scrDebugger::sm_masterGlobalsTreeList = NULL;
scrDebugger::Node_t* scrDebugger::sm_masterGlobalsRoots = NULL;
int scrDebugger::sm_masterComboBoxIndex = -1;
bkCombo* scrDebugger::sm_masterComboBox = NULL;
scrDebugger* scrDebugger::sm_masterDebugger = NULL;
bool scrDebugger::sm_createMasterDebugger = false;
scrDebugger::DebuggerBreakpointFunc scrDebugger::sm_breakpointCallback; 
scrDebugger::DebuggerStoppedGameUpdateFunc scrDebugger::sm_stoppedGameUpdateCallback;

scrDebugger::scrDebugger(const char *inputScriptName,scrThreadId threadId, bkBank* pParentBank, bool bDisplayGlobals) 
: m_Interface(NULL)
, m_LastLineNumber(-1)
, m_LastFileIndex(-1)
, m_LastStackFrame(-1)
, m_Group(NULL)
, m_parentBankPassedIn(false)
, m_stepPC(-1)
, m_stepHadBreakpoint(false)
, m_stepBreakpointStopsGame(false)
{
	m_TreeLists[LOCAL] = m_TreeLists[STATIC] = m_TreeLists[GLOBAL] = NULL;
	m_Status[0] = '\0';

	m_Roots[LOCAL] = NULL;
	m_Roots[STATIC] = NULL;
	m_Roots[GLOBAL] = NULL;
	Node_t **nextNodes[3];
	nextNodes[LOCAL] = &m_Roots[LOCAL];
	nextNodes[STATIC] = &m_Roots[STATIC];
	nextNodes[GLOBAL] = &m_Roots[GLOBAL];

    bkTreeList::UpdateItemFuncType functors[3];
	functors[LOCAL].Reset<scrDebugger,&scrDebugger::UpdateLocal>(this);
	functors[STATIC].Reset<scrDebugger,&scrDebugger::UpdateStatic>(this);
	functors[GLOBAL].Reset<scrDebugger,&scrDebugger::UpdateGlobal>(this);

	m_ThreadId = threadId;
	scrThread *thread = scrThread::GetThread(threadId);

	scrThread::FaultHandlerFunc faultHandler = MakeFunctor( *this, &scrDebugger::ScriptFaultHandler );
	thread->SetDebugFaultHandler( faultHandler );

    scrThread::ThreadBreakpointFunc threadBreakpointCallback = MakeFunctor( *this, &scrDebugger::ScriptBreakpointCallback );
    thread->SetDebugBreakpointCallback( threadBreakpointCallback );

    fiStream *S = ASSET.Open(inputScriptName, "scd");   // Respect the -path=
	if ( !S )
    {
        return;
    }

	char inputScriptNameNoExt[512];
	ASSET.FullPath( inputScriptNameNoExt, sizeof(inputScriptNameNoExt), inputScriptName, NULL );

    int numGroupsPushed = 0;
	if ( pParentBank )
	{
        m_parentBankPassedIn = true;

		m_Interface = pParentBank;

		if ( scrDebugger::sm_createMasterDebugger )
		{
			m_Group = m_Interface->PushGroup( "Master Script", false );
		}
		else
		{
			m_Group = m_Interface->PushGroup( inputScriptNameNoExt, false );
		}

        ++numGroupsPushed;
    }
	else
	{
        m_Interface = BANKMGR.FindBank( inputScriptNameNoExt );
        if ( m_Interface == NULL )
        {
            m_Interface = &BANKMGR.CreateBank( inputScriptNameNoExt );
            
            m_Interface->AddButton( "Load Script", datCallback(CFA(scrDebugger::BankLoadScript)) );
            m_Interface->AddButton( "Kill Threads", datCallback(CFA1(scrDebugger::BankKillScripts),m_Interface) );
        }

        char groupname[64];
        sprintf( groupname, "Thread %d", m_ThreadId );
        m_Group = m_Interface->PushGroup( groupname, false );
        ++numGroupsPushed;
	}
	
	if ( scrDebugger::sm_createMasterDebugger )
	{
		m_Interface->AddTitle( "%s Thread %d", inputScriptNameNoExt, m_ThreadId );
	}
	else
	{
		m_Interface->AddButton( "View as Master Script", datCallback(MFA(scrDebugger::BankMakeMeMaster),this) );
	}

    m_Interface->AddButton( "Kill", datCallback(MFA(scrDebugger::BankKillScript),this) );
    m_Interface->AddToggle( "Kill Debugger Upon Thread Completion", &m_killDebuggerUponCompletion );

	char linebuf[256];
	int linenum = 0;
	
	fgets( linebuf, sizeof(linebuf), S );
	++linenum;	
	if (!strstr(linebuf,"[VERSION]"))
	{
		Quitf( "Old version of debug info in '%s.scd', please recompile.", inputScriptNameNoExt );
	}
	
	fgets( linebuf, sizeof(linebuf), S );
	++linenum;
	if (atoi(linebuf) != 4)
	{
		Quitf( "Mismatched version of debug info in '%s.scd', please recompile.", inputScriptNameNoExt );
	}

	fgets( linebuf, sizeof(linebuf), S );
	++linenum;

	m_Types[ConstString("BOOL")] = rage_new typeBOOL;
	m_Types[ConstString("INT")] = rage_new typeINT;
	m_Types[ConstString("FLOAT")] = rage_new typeFLOAT;
	m_Types[ConstString("VECTOR")] = rage_new typeVECTOR;
	m_Types[ConstString("STRING")] = rage_new typeSTRING;
	m_Types[ConstString("TEXT_LABEL")] = rage_new typeTEXT_LABEL(16);
	m_Types[ConstString("TEXT_LABEL_3")] = rage_new typeTEXT_LABEL(4);
	m_Types[ConstString("TEXT_LABEL_7")] = rage_new typeTEXT_LABEL(8);
	m_Types[ConstString("TEXT_LABEL_15")] = rage_new typeTEXT_LABEL(16);
	m_Types[ConstString("TEXT_LABEL_23")] = rage_new typeTEXT_LABEL(24);
	m_Types[ConstString("TEXT_LABEL_31")] = rage_new typeTEXT_LABEL(32);
	m_Types[ConstString("TEXT_LABEL_63")] = rage_new typeTEXT_LABEL(64);
	m_Types[ConstString("TYPEDEF")] = rage_new typeTYPEDEF;

	// Parse [VARIABLES] section
	int currentFrame = -1;
	while ( fgets(linebuf,sizeof(linebuf),S) )
    {
		++linenum;
		if (linebuf[0]=='[')
		{
			break;
		}

		char *scope = strtok(linebuf,",\r\n");
		char *type = strtok(NULL,",\r\n");
		char *name = strtok(NULL,",\r\n");
		char *addr = strtok(NULL,",\r\n");
		if ( !strcmp(scope,"STRUCT") ) 
        {
			int count = atoi(name);
			typeSTRUCT *ts = rage_new typeSTRUCT(type,count);
			m_Types[ConstString(type)] = ts;
			for (int i=0; i<count; i++) 
            {
				++linenum;
				fgets(linebuf,sizeof(linebuf),S);
				strtok(linebuf,",\r\n");
				char *type = strtok(NULL,",\r\n");
				char *name = strtok(NULL,",\r\n");
				char *offset = strtok(NULL,",\r\n");

				Type_t *t = ParseType(type);
				Assertf(t,"ParseType[STRUCT] failed in line %d of '%s'",linenum,inputScriptName);
				ts->AddMember(t,name,atoi(offset));
			}
		}
		else if (!strcmp(scope,"ENUM")) 
        {
			int count = atoi(name);
			typeENUM *te = rage_new typeENUM(type,count);
			m_Types[ConstString(type)] = te;
			for (int i=0; i<count; i++) 
            {
				++linenum;
				fgets(linebuf,sizeof(linebuf),S);
				strtok(linebuf,",\r\n");
				char *name = strtok(NULL,",\r\n");
				char *offset = strtok(NULL,",\r\n");
				te->AddMember(name,atoi(offset));
			}
		}
		else if (scope && type && name && addr) 
        {
			// FUNC, PROC, or EVENT
			if ( (*scope == 'F') || (*scope == 'P') || (*scope == 'E') ) 
            {
				StackFrame &sf = m_StackFrames.Grow();
				sf.Start = atoi(addr);
				sf.End = atoi(strtok(NULL,",\r\n"));
				sf.Name = name;
				++currentFrame;
				continue;
			}

			int iScope = *scope=='L'? LOCAL : *scope=='S'? STATIC : GLOBAL;
			if ( (iScope != GLOBAL) || bDisplayGlobals )
			{
				Item &item = m_Items.Grow();
				item.Type = ParseType(type);
				Assertf(item.Type,"ParseType[var] failed in line %d of '%s'",linenum,inputScriptName);
				item.SetAddress(atoi(addr));
				item.SetScope(iScope);
				item.Frame = item.GetScope()==LOCAL? currentFrame : -1;
				safecpy(item.Name,name,sizeof(item.Name));

				// Create the pane just in time if we have any variables of that type
				if ( !m_TreeLists[item.GetScope()] )
                {
					static const char *titles[] = { "Local Variables", "Static Variables", "Global Variables" };
					m_TreeLists[item.GetScope()] = m_Interface->AddTreeList( titles[item.GetScope()], 0, 
                        iScope != LOCAL, iScope == GLOBAL, iScope == GLOBAL, iScope != GLOBAL );
                    m_TreeLists[item.GetScope()]->AddReadOnlyColumn( "Name" );
					m_TreeLists[item.GetScope()]->AddTextBoxColumn( "Value" );
                    m_TreeLists[item.GetScope()]->AddReadOnlyColumn( "Type" );
					m_TreeLists[item.GetScope()]->SetUpdateItemFunc( functors[item.GetScope()] );

                    if ( iScope == GLOBAL )
                    {
                        bkTreeList::SpecialFuncType dragDropFunctor;
                        dragDropFunctor.Reset<scrDebugger,&scrDebugger::DragDropGlobalNode>(this);
                        m_TreeLists[GLOBAL]->SetDragDropFunc( dragDropFunctor );

                        bkTreeList::SpecialFuncType deleteFunctor;
                        deleteFunctor.Reset<scrDebugger,&scrDebugger::DeleteGlobalNode>(this);
                        m_TreeLists[GLOBAL]->SetDeleteNodeFunc( deleteFunctor );
                    }
				}

				if ( item.GetScope() != LOCAL )
                {
                    item.Type->SetupDisplay( *nextNodes[item.GetScope()], thread, this, &item, m_TreeLists[item.GetScope()] );
                    nextNodes[item.GetScope()] = &(*nextNodes[item.GetScope()])->Sibling;
				}
			}
		}
	}

	m_Callstack = m_Interface->AddList("Callstack");
	m_Callstack->AddColumnHeader(0,"Depth",bkList::INT);
	m_Callstack->AddColumnHeader(1,"Function Name",bkList::STRING);

    bkList::ClickItemFuncType callstackDblClickFtor;
    callstackDblClickFtor.Reset<scrDebugger,&scrDebugger::BankCallstackDoubleClickFunction>( this );
    m_Callstack->SetDoubleClickItemFunc( callstackDblClickFtor );

	m_CallstackDepth = 0;

	// Parse [LINEINFO] section
	if (!strstr(linebuf,"[LINEINFO]"))
	{
		Quitf( "Expected [LINEINFO] here in '%s.scd'", inputScriptNameNoExt );
	}

	fgets(linebuf,sizeof(linebuf),S);
	int lineCount = atoi(linebuf);
	m_LineInfo.Reserve(lineCount);
	for ( int i = 0; i < lineCount; i++ ) 
	{
		fgets(linebuf,sizeof(linebuf),S);
		char *basePc = strtok(linebuf,":");
		char *lineNumber = strtok(NULL,":");
		char *fileIndex = strtok(NULL,":");
		LineInfo &li = m_LineInfo.Append();
		li.m_BasePc = atoi(basePc);
		li.m_LineNumber = u16(atoi(lineNumber));
		li.m_FileIndex = u16(atoi(fileIndex));
	}
	
	fgets(linebuf,sizeof(linebuf),S);
	int fileCount = atoi(linebuf);
	m_FileInfo.Reserve(fileCount);
	for (int i = 0; i < fileCount; i++) 
	{
		fgetline(linebuf,sizeof(linebuf),S);
		m_FileInfo.Append() = linebuf;
	}

	fgets(linebuf,sizeof(linebuf),S);
	if (!strstr(linebuf,"[EOF]"))
	{
		Quitf( "Expected [EOF] marker here in '%s.scd'", inputScriptNameNoExt );
	}

	m_Interface->AddText("Status:",m_Status,sizeof(m_Status),true);
	
	m_Editor = rage_new scrWidgetEditor( (const char *)(m_FileInfo[0]), inputScriptNameNoExt, NULL );
	m_Interface->AddWidget(*m_Editor);

	scrWidgetEditor::BreakpointFuncType breakCB;
	breakCB.Reset<scrDebugger,&scrDebugger::SetBreakpoint>(this);
	m_Editor->SetBreakpointCB(breakCB);

    scrWidgetEditor::DebuggerActionFuncType continueCB;
	continueCB.Reset<scrDebugger,&scrDebugger::Continue>(this);
	m_Editor->SetDebuggerActionCB(scrWidgetEditor::ACTION_CONTINUE,continueCB);

	scrWidgetEditor::DebuggerActionFuncType pauseCB;
	pauseCB.Reset<scrDebugger,&scrDebugger::Pause>(this);
	m_Editor->SetDebuggerActionCB(scrWidgetEditor::ACTION_PAUSE,pauseCB);

	scrWidgetEditor::DebuggerActionFuncType stepIntoCB;
	stepIntoCB.Reset<scrDebugger,&scrDebugger::StepInto>(this);
	m_Editor->SetDebuggerActionCB(scrWidgetEditor::ACTION_STEP_INTO,stepIntoCB);

    scrWidgetEditor::DebuggerActionFuncType stepOverCB;
    stepOverCB.Reset<scrDebugger,&scrDebugger::StepOver>(this);
    m_Editor->SetDebuggerActionCB(scrWidgetEditor::ACTION_STEP_OVER,stepOverCB);

    scrWidgetEditor::DebuggerActionFuncType stepOutCB;
    stepOutCB.Reset<scrDebugger,&scrDebugger::StepOut>(this);
    m_Editor->SetDebuggerActionCB(scrWidgetEditor::ACTION_STEP_OUT,stepOutCB);

    S->Close();

	for (int i = 0; i < numGroupsPushed; ++i)
	{
		m_Interface->PopGroup();
	}

	if ( !scrDebugger::sm_createMasterDebugger )
	{
		scrDebugger::PopulateMasterDebuggerComboBox();

		if ( scrDebugger::sm_masterDebugger == NULL )
		{
			BankComboBoxChanged();
		}
	}
}

scrDebugger::~scrDebugger() 
{
	for ( int i = 0; i < 3; ++i )
	{
		if ( m_TreeLists[i] != NULL )
		{
			Node_t::RemoveList( m_Roots[i], m_TreeLists[i] );
		}
	}

	if ( m_Interface ) 
    {
		if ( m_Group )
		{
			m_Interface->DeleteGroup( *m_Group );
            m_Group = NULL;
		}
		
        int count = m_Interface->GetNumWidgets();
        if ( !m_parentBankPassedIn && (count <= 2) )
		{
			BANKMGR.DestroyBank( *m_Interface );
		}

		m_Interface = NULL;
	}

	// Delete any types associated with the debug information.
	atMap<ConstString,Type_t*>::Iterator entry = m_Types.CreateIterator();
	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		delete entry.GetData(); /// do something with it
	}
    m_Types.Kill();

	if ( !scrDebugger::sm_createMasterDebugger )
	{
		scrDebugger::PopulateMasterDebuggerComboBox();
	}
}

void scrDebugger::InitClass( int count, bool killDebuggersUponCompletion )
{
    scrDebugger::sm_scriptDebuggers.Reserve( count );

    scrDebugger::sm_killDebuggersUponCompletion = killDebuggersUponCompletion;

    // create bank
    scrDebugger::sm_debuggerBank = &BANKMGR.CreateBank( "rage - Script" );
    scrDebugger::sm_debuggerBank->AddButton( "Load Script", datCallback(CFA(scrDebugger::BankLoadScript)) );
    scrDebugger::sm_debuggerBank->AddButton( "Kill All Scripts", datCallback(CFA(scrDebugger::BankKillAllScripts)) );
    scrDebugger::sm_debuggerBank->AddToggle( "Kill Debuggers Upon Thread Completion", &scrDebugger::sm_killDebuggersUponCompletion, 
        NullCB, "Default value for newly loaded scripts" );

	scrDebugger::sm_debuggerBank->AddSeparator();

    // create global variables list in the bank
    scrDebugger::sm_masterGlobalsTreeList = scrDebugger::sm_debuggerBank->AddTreeList( "Global Variables", 0, true, true, true, false );
    scrDebugger::sm_masterGlobalsTreeList->AddReadOnlyColumn( "Name" );
    scrDebugger::sm_masterGlobalsTreeList->AddTextBoxColumn( "Value" );
    scrDebugger::sm_masterGlobalsTreeList->AddReadOnlyColumn( "Type" );

    bkTreeList::UpdateItemFuncType updateFunctor;
    updateFunctor.Reset<&scrDebugger::UpdateMasterGlobal>();
    scrDebugger::sm_masterGlobalsTreeList->SetUpdateItemFunc( updateFunctor );

    bkTreeList::SpecialFuncType dragDropFunctor;
    dragDropFunctor.Reset<&scrDebugger::DragDropMasterGlobal>();
    scrDebugger::sm_masterGlobalsTreeList->SetDragDropFunc( dragDropFunctor );

    bkTreeList::SpecialFuncType deleteNodeFunctor;
    deleteNodeFunctor.Reset<&scrDebugger::DeleteNodeMasterGlobal>();
    scrDebugger::sm_masterGlobalsTreeList->SetDeleteNodeFunc( deleteNodeFunctor );

	scrDebugger::PopulateMasterDebuggerComboBox();

    BANKMGR.CreateOutputWindow( "Script", "NamedColor:Chocolate" );
}

void scrDebugger::ShutdownClass()
{
    scrDebugger::KillAllDebuggers();

    BANKMGR.DestroyBank( *scrDebugger::sm_debuggerBank );
}

bool scrDebugger::CreateDebugger( const char *scriptName, scrThreadId id, bkBank* pParentBank, bool bDisplayGlobals )
{
    if ( sm_scriptDebuggers.GetCount() < sm_scriptDebuggers.GetCapacity() )
    {
        return scrDebugger::RegisterDebugger( rage_new scrDebugger(scriptName,id,pParentBank,bDisplayGlobals) );
    }

    return false;
}

bool scrDebugger::RegisterDebugger( scrDebugger *pDebugger )
{
    if ( sm_scriptDebuggers.GetCount() < sm_scriptDebuggers.GetCapacity() )
    {
        // set default
        pDebugger->m_killDebuggerUponCompletion = scrDebugger::sm_killDebuggersUponCompletion;

        Node_t **nextNode = &sm_masterGlobalsRoots;
        if ( sm_masterGlobalsRoots != NULL )
        {
            while ( *nextNode )
            {
                // find empty Sibling so we can assign a root to it
                nextNode = &(*nextNode)->Sibling;
            }
        }

        // add global items to the master list
        scrThread *pThread = scrThread::GetThread( pDebugger->GetThreadId() );
        for (int i = 0; i < pDebugger->m_Items.GetCount(); i++) 
        {
            Item &item = pDebugger->m_Items[i];
            if ( item.GetScope() == GLOBAL ) 
            {
                // copy the item
                Item &globalItem = scrDebugger::sm_masterGlobalsItems.Grow();
                globalItem.Address = item.Address;
                globalItem.Frame = item.Frame;
                safecpy( globalItem.Name, item.Name, sizeof(globalItem.Name) );
                globalItem.SetScope( item.GetScope() );
                globalItem.Type = item.Type;

                // add it to the master globals list
                globalItem.Type->SetupDisplay( *nextNode, pThread, pDebugger, &globalItem, scrDebugger::sm_masterGlobalsTreeList );
                nextNode = &(*nextNode)->Sibling;
            }
        }

        sm_scriptDebuggers.Append() = pDebugger;
        return true;
    }

    return false;
}

void scrDebugger::KillDebugger( scrDebugger *pDebugger )
{
    // remove globals
    Node_t *n = scrDebugger::sm_masterGlobalsRoots;
    Node_t *prevN = NULL;
    while ( n )
    {
        Node_t *nextN = n->Sibling;

        if ( n->Debugger == pDebugger )
        {
            // remove item
            int count = scrDebugger::sm_masterGlobalsItems.GetCount();
            for (int x = 0; x < count; ++x)
            {
                Item item = scrDebugger::sm_masterGlobalsItems[x];
                if ( item.Address == n->ValueAddr )
                {
                    scrDebugger::sm_masterGlobalsItems.Delete( x );
                    break;
                }
            }

            // remove from display and delete the n
            n->Type->RemoveDisplay( n, scrDebugger::sm_masterGlobalsTreeList );
            
            if ( prevN != NULL )
            {
                // hop over the one we are removing
                prevN->Sibling = nextN;
            }
            else
            {
                scrDebugger::sm_masterGlobalsRoots = nextN;
            }
        }
        else
        {
            if ( prevN == NULL )
            {
                // we didn't remove the first n in the list
                prevN = n;
            }
            else
            {
                prevN = prevN->Sibling;
            }
        }

        n = nextN;
    }

    delete pDebugger;
}

scrDebugger* scrDebugger::GetDebugger( scrThreadId id )
{
    for ( int i = 0; i < scrDebugger::sm_scriptDebuggers.GetCount(); ++i )
    {
        scrDebugger *pDebugger = scrDebugger::sm_scriptDebuggers[i];
        if ( pDebugger->GetThreadId() == id )
        {
            return pDebugger;
        }
    }

    return NULL;
}

void scrDebugger::KillDebugger( scrThreadId id )
{
    for ( int i = 0; i < scrDebugger::sm_scriptDebuggers.GetCount(); )
    {
        scrDebugger *pDebugger = scrDebugger::sm_scriptDebuggers[i];
        if ( pDebugger->GetThreadId() == id )
        {
            scrDebugger::KillDebugger( pDebugger );
            scrDebugger::sm_scriptDebuggers.Delete( i );
        }
        else
        {
            ++i;
        }
    }
}

void scrDebugger::UpdateAll()
{
    for (int i = 0; i < scrDebugger::sm_scriptDebuggers.GetCount(); )
    {
        scrDebugger *pDebugger = scrDebugger::sm_scriptDebuggers[i];

        pDebugger->Update();

        scrThread *pThread = scrThread::GetThread( pDebugger->GetThreadId() );        
        if ( pDebugger->m_killDebuggerUponCompletion )
        {
            if ( !pThread || (pThread->GetState() == scrThread::ABORTED) )
            {
                scrDebugger::KillDebugger( pDebugger->GetThreadId() );
            }
            else
            {
                ++i;
            }
        }
        else
        {
            ++i;
        }
    }

	if ( (scrDebugger::sm_masterGlobalsTreeList != NULL) && scrDebugger::sm_masterGlobalsTreeList->IsOpen() )
	{
		Node_t::UpdateList( scrDebugger::sm_masterGlobalsRoots, scrDebugger::sm_masterGlobalsTreeList, NULL );
	}

	if ( scrDebugger::sm_masterDebugger != NULL )
	{
		scrDebugger::sm_masterDebugger->Update();

		scrThread *pThread = scrThread::GetThread( scrDebugger::sm_masterDebugger->GetThreadId() );        
		if ( scrDebugger::sm_masterDebugger->m_killDebuggerUponCompletion )
		{
			if ( !pThread || (pThread->GetState() == scrThread::ABORTED) )
			{
				scrDebugger::KillDebugger( scrDebugger::sm_masterDebugger );
				scrDebugger::sm_masterDebugger = NULL;
			}
		}
	}
}

void scrDebugger::KillAllDebuggers()
{
    int count = scrDebugger::sm_scriptDebuggers.GetCount();
    for (int i = 0; i < count; ++i)
    {
		scrDebugger::KillDebugger( scrDebugger::sm_scriptDebuggers[i] );
    }

    scrDebugger::sm_scriptDebuggers.Reset();

	if ( scrDebugger::sm_masterDebugger != NULL )
	{
		scrDebugger::KillDebugger( scrDebugger::sm_masterDebugger );
		scrDebugger::sm_masterDebugger = NULL;
	}
}

scrValue* scrDebugger::Resolve(scrThread *thread,int address) 
{
    int scope = address >> 28;
    address &= 0xFFFFFFF;
    if (scope == LOCAL)
    {
        if (address & IS_REFERENCE) 
        {
            scrValue *v = thread->GetLocal(address >> OFFSET_SHIFT);
            if (!v || !thread->IsValidReference(v))
                return NULL;
            v = scrDecodeReference(v->Reference) + (address & (IS_REFERENCE-1));
            if (!thread->IsValidReference(v))
                return NULL;
            return v;
        }
        else
            return thread->GetLocal(address);
    }
    else if (scope == STATIC)
        return thread->GetStatic(address);
    else if (scope == GLOBAL)
        return thread->GetGlobal(address);
    else
        return NULL;
}

void scrDebugger::SetBreakpointCallback( DebuggerBreakpointFunc del )
{
    sm_breakpointCallback = del;
}

void scrDebugger::SetStoppedGameUpdateCallback( DebuggerStoppedGameUpdateFunc del )
{
    sm_stoppedGameUpdateCallback = del;
}

void scrDebugger::ScriptFaultHandler( const char *str, scrThread *pThread )
{
	// not sure if we want this
	// pThread->DumpState( Displayf );

	int level = 0, pc, frame;																								  
	// the "level<16" loop sometimes gets confused, so this test prevents an infinite loop:
	while ( ((pc = pThread->GetProgramCounter(level++)) != -1) 
		&& ((frame = GetStackFrameFromProgramCounter(pc)) != -1) 
		&& (level < 16) ) 
	{
        int lineInfoIndex = GetLineInfoFromProgramCounter( pc, level - 1, frame );
        if ( lineInfoIndex != -1 )
        {
            // HACK HACK HACK:  We're probably in the middle of executing something, which means that the pc
            // is somewhere in between lines.
            if ( (level == 1) && (pc != m_LineInfo[lineInfoIndex].m_BasePc) && (lineInfoIndex + 1 < m_LineInfo.GetCount()) )
            {
                ++lineInfoIndex;
            }

            diagLoggedPrintf( PREFIX "%s - %s (line %d)" SUFFIX "\n", __LINE__, m_StackFrames[frame].Name.c_str(), 
                m_FileInfo[m_LineInfo[lineInfoIndex].m_FileIndex].c_str(), m_LineInfo[lineInfoIndex].m_LineNumber );
        }
    }

    if ( str != NULL )
    {
        diagLoggedPrintf( PREFIX "%s" SUFFIX "\n", __LINE__, str );
    }
}

void scrDebugger::ScriptBreakpointCallback( scrThread* pThread, int pc )
{
    if ( pc == m_stepPC )
    {
        return;
    }

    if ( sm_breakpointCallback )
    {
        int lineInfoIndex = -1;

        int frame = GetStackFrameFromProgramCounter( pc );
        if ( frame != -1 )
        {
            lineInfoIndex = GetLineInfoFromProgramCounter( pc, 0, frame );
        }

        if ( lineInfoIndex != -1 )
        {
            sm_breakpointCallback( this, m_FileInfo[m_LineInfo[lineInfoIndex].m_FileIndex].c_str(), m_LineInfo[lineInfoIndex].m_LineNumber );
        }
        else
        {
            sm_breakpointCallback( this, NULL, -1 );
        }
    }

    // check if breakpoint stops the game
    if ( sm_stoppedGameUpdateCallback )
    {
        bool threadBreakpointStopsGame, programBreakpointStopsGame;
        scrProgram *pProgram = scrProgram::GetProgram( pThread->GetProgram() );
        
        if ( (pThread->HasBreakpoint( pc, &threadBreakpointStopsGame ) && threadBreakpointStopsGame) 
            || (pProgram->HasBreakpoint( pc, &programBreakpointStopsGame ) && programBreakpointStopsGame) )
        {
            scrDisplayf( "Stopping game" );
            
            Update();   // update the debugger one last time

            while ( sm_stoppedGameUpdateCallback() && (pThread->GetState() == scrThread::HALTED) )
            {
                // wait
            }

            scrDisplayf( "Resuming game" );
        }
    }
}

int scrDebugger::GetStackFrameFromProgramCounter(int pc) const 
{
	int currentFrame = -1;
	for (int i=m_StackFrames.GetCount()-1; i>=0; i--) 
    {
		// HACK - Pretend OP_ENTER is part of somebody else so
		// that we don't try to set up locals too soon before references
		// are properly bound.
		if ( (pc > m_StackFrames[i].Start) && (pc <= m_StackFrames[i].End) )
		{
			currentFrame = i;
		}
	}

	return currentFrame;
}

int scrDebugger::GetLineInfoFromProgramCounter(int pc, int level, int frame) const
{
    // HACK HACK HACK:  Why is the pc for anything in the *SCRIPT function 2 less than the LineInfo's BasePC for it?
    if ( (frame != -1) && (frame == m_StackFrames.GetCount() - 1) )
    {
        pc += 2;
    }

    // Find the index of the LineInfo whose BasePC is at or before the program counter
    int lineInfoIndex = -1;
    for ( int i = 0; i < m_LineInfo.GetCount(); ++i )
    {
        if ( (pc >= m_LineInfo[i].m_BasePc) 
            && ((i+1 == m_LineInfo.GetCount()) || (pc < m_LineInfo[i+1].m_BasePc)) ) 
        {
            lineInfoIndex = i;
            break;
        }
    }

    // HACK HACK HACK: Make sure the lineInfoIndex makes sense for the frame
    if ( lineInfoIndex != -1 )
    {
        int correctedLineInfoIndex = lineInfoIndex;
        while ( (GetStackFrameFromProgramCounter( m_LineInfo[correctedLineInfoIndex].m_BasePc ) != frame) 
            && (correctedLineInfoIndex < m_LineInfo.GetCount()) )
        {
            ++correctedLineInfoIndex;
        }

        if ( correctedLineInfoIndex < m_LineInfo.GetCount() )
        {
            lineInfoIndex = correctedLineInfoIndex;
        }
    }

    // HACK HACK HACK: On top of that, the pc is where we are going to return to, not where we came from.
    if ( (lineInfoIndex != -1) && (level > 0) )
    {
        u32 currentMax = 0;
        int correctedLineInfoIndex = lineInfoIndex;
        for ( int i = lineInfoIndex - 1; i >= 0; --i )
        {
            // Find the index of the LineInfo for the previous instruction in the *SCRIPT function.
            if ( (m_LineInfo[i].m_FileIndex == m_LineInfo[lineInfoIndex].m_FileIndex) 
                && (m_LineInfo[i].m_LineNumber <= m_LineInfo[lineInfoIndex].m_LineNumber) )
            {
                if ( m_LineInfo[i].m_LineNumber > currentMax )
                {
                    currentMax = m_LineInfo[i].m_LineNumber;
                    correctedLineInfoIndex = i;
                }
            }
        }

        lineInfoIndex = correctedLineInfoIndex;
    }

    return lineInfoIndex;
}

int scrDebugger::GetBasePC( const char *filename, s32 line ) const
{
    line += HACK_HACK_HACK;

    int fileIndex = -1;
    for (int i = 0; i < m_FileInfo.GetCount(); i++) 
    {
        if ( !strcmp( m_FileInfo[i].c_str(), filename ) ) 
        {
            fileIndex = i;
            break;
        }
    }

    if ( fileIndex == -1 ) 
    {
        return -2;
    }

    for (int i = 0; i < m_LineInfo.GetCount(); i++)
    {
        if ( (m_LineInfo[i].m_LineNumber == u16(line)) && (m_LineInfo[i].m_FileIndex == fileIndex) )
        {
            return m_LineInfo[i].m_BasePc;
        }
    }

    return -1;
}

void scrDebugger::Update() 
{
	if ( !m_Interface || !m_Interface->IsShown() )
    {
        return;
    }

	scrThread *thread = scrThread::GetThread( m_ThreadId );
	if ( !thread )
    {
		m_Editor->Exit();
        return;
    }

	// See if the stack frame has changed (and we need to update the locals pane)
	// Due to the way dead code elimination words, later stack frames are broader
	// so we search from the end backward for the best fit.
    int pc = thread->GetProgramCounter(0);
	int currentFrame = GetStackFrameFromProgramCounter( pc );

	if ( currentFrame != m_LastStackFrame ) 
    {
		// Check if we need to remove previous stack frame's local variables, if any
        Node_t *removeRoot = (m_LastStackFrame != -1) ? m_Roots[LOCAL] : NULL;
        if ( removeRoot )
        {
            m_Roots[LOCAL] = NULL;
        }

		m_LastStackFrame = currentFrame;
		if (m_LastStackFrame != -1) 
        {
			Node_t **key = &m_Roots[LOCAL];
			for (int i=0; i < m_Items.GetCount(); i++) 
            {
                Item *item = &m_Items[i];
				if ( (item->GetScope() == LOCAL) && (item->Frame == m_LastStackFrame) ) 
                {
					item->Type->SetupDisplay( *key, thread, this, item, m_TreeLists[LOCAL] );
					key = &(*key)->Sibling;
				}
			}
		}

        // Now remove the roots.  We needed to wait because of the unique key situation. 
        // By allocating the roots for the new stack frame before deleting the old ones,
        // we avoid the potential of allocating new keys with any of the old key values.
        if ( removeRoot ) 
        {
            Node_t::RemoveList( removeRoot, m_TreeLists[LOCAL] );
        }

		// Clear the callstack
		while (m_CallstackDepth)
			m_Callstack->RemoveItem(--m_CallstackDepth);

        m_currentStackFrameInfo.Reset();

		// ...and repopulate it
		int level = 0, pc, frame;																								  
		// the "level<16" loop sometimes gets confused, so this test prevents an infinite loop:
		while ((pc = thread->GetProgramCounter(level++)) != -1 && (frame = GetStackFrameFromProgramCounter(pc)) != -1 && level<16 ) 
        {
            // Record the stack frame info for the BankCallstackDoubleClickFunction().
            StackFrameInfo sfInfo;
            sfInfo.pc = pc;
            sfInfo.level = level - 1;
            sfInfo.frame = frame;
            m_currentStackFrameInfo.PushAndGrow( sfInfo );
           
			m_Callstack->AddItem(m_CallstackDepth,0,m_CallstackDepth);
			m_Callstack->AddItem(m_CallstackDepth,1,m_StackFrames[frame].Name.c_str());
			++m_CallstackDepth;
		}
	}

	// Check all variables for changes.
	for ( int i = 0; i < 3; ++i )
	{
		if ( (m_TreeLists[i] != NULL) && m_TreeLists[i]->IsOpen() )
		{
			Node_t::UpdateList( m_Roots[i], m_TreeLists[i], thread );
		}
	}

    if ( m_stepPC == pc )
    {        
        // if we land here, restore the phantom breakpoint to its previous state
        thread->SetBreakpoint( m_stepPC, m_stepHadBreakpoint, m_stepBreakpointStopsGame );
        
        m_stepPC = -1;
    }

	UpdateLineInfo( thread, false );
}


void scrDebugger::UpdateLineInfo( scrThread *thread, bool force ) 
{
	int pc = thread->GetProgramCounter( 0 );
	const char *filename = "unknown";

	// Find the line number at or before current program counter
	int lineNumber = 0;
	int fileIndex = 0;
	for (int i = 0; i < m_LineInfo.GetCount(); i++) 
    {
		if ( (pc >= m_LineInfo[i].m_BasePc) && ((i+1 == m_LineInfo.GetCount()) || (pc < m_LineInfo[i+1].m_BasePc)) ) 
    {
			lineNumber = m_LineInfo[i].m_LineNumber;
			filename = m_FileInfo[fileIndex = m_LineInfo[i].m_FileIndex];
			break;
		}
    }

	if ( (lineNumber != m_LastLineNumber) || (fileIndex != m_LastFileIndex) || force )
    {
		// This debug spew sucks when you have multiple WAIT cmds
		scrDebugf2( "%s(%d), pc=%d", filename, lineNumber, pc );		

		m_LastLineNumber = lineNumber;
		m_LastFileIndex = fileIndex;
	}

	static const char *states[] = { "Running", "Waiting", "Aborted", "Stopped" };
	
	scrThread::State state = thread->GetState();

	if ( state == scrThread::RUNNING )
    {
        strcpy( m_Status, states[0] );
    }
	else
    {
        formatf( m_Status, sizeof(m_Status), "%s at line %d",
			state == scrThread::ABORTED ? thread->GetAbortReason() : states[thread->GetState()], lineNumber );
    }

	m_Editor->UpdateThreadState( state, filename, lineNumber - HACK_HACK_HACK, m_Status );
}

bool scrDebugger::SetBreakpoint( const char *filename, s32 line, bool setOrUnSet, bool breakAllThreads, bool breakStopsGame ) 
{
    scrDisplayf( "SetBreakpoint(%s,%d,%d)", filename, line, setOrUnSet );

	int basePC = GetBasePC( filename, line );
    if ( basePC == -2 )
    {
        scrWarningf( "Unknown file [%s], cannot set breakpoint here.", filename );
        return false;
    }
    else if ( basePC == -1 )
    {
        scrWarningf( "SetBreakpoint failed." );
        return false;
    }

    scrDisplayf( "%s breakpoint at pc=%d%s%s", setOrUnSet ? "Adding" : "Removing", basePC, 
        breakAllThreads ? " (Break All Threads)" : "", breakStopsGame ? " (Stops Game)" : "" );

    scrThread *thread = scrThread::GetThread( m_ThreadId );
    if ( (thread != NULL) && (thread->GetState() != scrThread::ABORTED) )
    {
        bool localBreakpointStopsGame;
        bool hasLocalBreakpoint = thread->HasBreakpoint( basePC, &localBreakpointStopsGame );

        scrProgram *program = scrProgram::GetProgram( thread->GetProgram() );
        bool allThreadsBreakpointStopsGame;
        bool hasAllThreadsBreakpoint = program->HasBreakpoint( basePC, &allThreadsBreakpointStopsGame );       

        bool changedAllThreadsBreakpoint = false;
        bool addedAllThreadsBreakpoint = false;
        bool removedAllThreadsBreakpoint = false;

        if ( setOrUnSet )
        {
            if ( hasLocalBreakpoint && !hasAllThreadsBreakpoint && breakAllThreads )
            {
                // changed from local to global
                thread->SetBreakpoint( basePC, false );
                program->SetBreakpoint( basePC, true, breakStopsGame );

                hasAllThreadsBreakpoint = true;
                changedAllThreadsBreakpoint = true;
            }
            else if ( !hasLocalBreakpoint && hasAllThreadsBreakpoint && !breakAllThreads )
            {
                // changed from global to local
                thread->SetBreakpoint( basePC, true, breakStopsGame );
                program->SetBreakpoint( basePC, false );

                hasAllThreadsBreakpoint = false;
                changedAllThreadsBreakpoint = true;
            }
            else
            {
                if ( breakAllThreads )
                {
                    // add global
                    program->SetBreakpoint( basePC, true, breakStopsGame );

                    hasAllThreadsBreakpoint = true;
                    addedAllThreadsBreakpoint = true;
                }
                else
                {
                    // add local
                    thread->SetBreakpoint( basePC, true, breakStopsGame );
                }
            }
        }
        else
        {
            if ( breakAllThreads )
            {
                // remove global
                program->SetBreakpoint( basePC, false );

                hasAllThreadsBreakpoint = false;
                removedAllThreadsBreakpoint = true;
            }
            else
            {
                // remove local
                thread->SetBreakpoint( basePC, false );
            }
        }

        if ( changedAllThreadsBreakpoint || addedAllThreadsBreakpoint || removedAllThreadsBreakpoint )
        {
            // go through the threads to adjust their breakpoints and/or inform their debuggers
            for ( int i = 0; i < scrThread::GetThreadCount(); ++i )
            {
                scrThread &otherThread = scrThread::GetThreadByIndex( i );
                if ( (otherThread.GetThreadId() == 0) || (otherThread.GetThreadId() == m_ThreadId) 
                    || (otherThread.GetProgram() != thread->GetProgram()) )
                {
                    continue;
                }
                
                scrDebugger *debugger = scrDebugger::GetDebugger( otherThread.GetThreadId() );
                if ( addedAllThreadsBreakpoint || removedAllThreadsBreakpoint )
                {
                    // On add and remove, don't remember the local breakpoint
                    otherThread.SetBreakpoint( basePC, false );

                    if ( debugger != NULL )
                    {
                        debugger->m_Editor->SetBreakpoint( filename, line, addedAllThreadsBreakpoint, true, breakStopsGame );
                    }
                }
                else if ( debugger != NULL )
                {
                    // add a global breakpoint or restore the other thread's breakpoint to its previous state
                    hasLocalBreakpoint = otherThread.HasBreakpoint( basePC );
                    debugger->m_Editor->SetBreakpoint( filename, line, hasLocalBreakpoint || hasAllThreadsBreakpoint, 
                        hasAllThreadsBreakpoint, breakStopsGame );
                }
            }
        }

        return true;
    }

    return false;
}

void scrDebugger::StepInto() 
{
    ClearPhantomBreakpoint();

    scrThread *thread = scrThread::GetThread(m_ThreadId);
    if ( thread ) 
    {
        int pc = thread->GetProgramCounter( 0 );
        for ( int i = m_LineInfo.GetCount() - 2; i >= 0; --i ) 
        {
            if ( pc >= m_LineInfo[i].m_BasePc ) 
            {
                while ( (i+1 < m_LineInfo.GetCount()) && (int(m_LineInfo[i+1].m_LineNumber) == m_LastLineNumber) )
                {
                    ++i;
                }

                if ( i+1 < m_LineInfo.GetCount() )
                {
                    thread->SetStepRegion( pc, m_LineInfo[i+1].m_BasePc );

                    // Find the line number at or before current program counter
                    for (int j = 0; j < m_LineInfo.GetCount(); j++) 
                    {
                        if ( (pc >= m_LineInfo[j].m_BasePc) && ((j+1 == m_LineInfo.GetCount()) || (pc < m_LineInfo[j+1].m_BasePc)) ) 
                        {
                            m_Editor->StepInto( m_FileInfo[m_LineInfo[j].m_FileIndex].c_str(), m_LineInfo[j].m_LineNumber - HACK_HACK_HACK );
                            break;
                        }
                    }

                    UpdateLineInfo( thread, true );
                    return;
                }
            }
        }
    }

    scrWarningf( "StepInto failed." );
    m_Editor->OperationFailed();
}

void scrDebugger::StepOver() 
{
    ClearPhantomBreakpoint();

    scrThread *thread = scrThread::GetThread(m_ThreadId);
    if ( thread ) 
    {
        int pc = thread->GetProgramCounter( 0 );
        for ( int i = m_LineInfo.GetCount() - 2; i >= 0; --i ) 
        {
            if ( pc >= m_LineInfo[i].m_BasePc ) 
            {
                while ( (i+1 < m_LineInfo.GetCount()) && (int(m_LineInfo[i+1].m_LineNumber) == m_LastLineNumber) )
                {
                    ++i;
                }

                int originalIndex = i;
                int currentFileIndex = m_LineInfo[i].m_FileIndex;
                while ( (i+1 < m_LineInfo.GetCount()) 
                    && ((int(m_LineInfo[i+1].m_FileIndex) != currentFileIndex) || (int(m_LineInfo[i+1].m_LineNumber) <= m_LastLineNumber)) )
                {
                    ++i;
                }

                // if we go off the end, treat it like a StepInto
                if ( i+1 >= m_LineInfo.GetCount() )
                {
                    i = originalIndex;
                }

                if ( i+1 < m_LineInfo.GetCount() )
                {
                    pc = m_LineInfo[i+1].m_BasePc;
                    m_stepHadBreakpoint = thread->HasBreakpoint( pc, &m_stepBreakpointStopsGame );
                    if ( !m_stepHadBreakpoint )
                    {
                        // this is a phantom breakpoint that should not show up in the UI
                        // FIXME: This is where we could possibly set stopsGame=true if the last breakpoint we hit stopped the game.
                        thread->SetBreakpoint( pc, true );  
                    }

                    Continue();

                    // set here, because Continue() will normally clear it
                    m_stepPC = m_LineInfo[i+1].m_BasePc;
                    return;
                }
            }
        }
    }

    scrWarningf( "StepOver failed." );
    m_Editor->OperationFailed();
}

void scrDebugger::StepOut() 
{
    ClearPhantomBreakpoint();

    scrThread *thread = scrThread::GetThread(m_ThreadId);
    if ( thread ) 
    {
        int pc = thread->GetProgramCounter( 0 );
        if ( pc != -1 )
        {
            int level = 1;
            while ( ((pc = thread->GetProgramCounter( level )) == -1) && (level < 16) )
            {
                ++level;
            }

            if ( pc != -1 )
            {
                // HACK HACK HACK:  Why is the pc for anything in the *SCRIPT function 2 less than the LineInfo's BasePC for it?
                int frame = GetStackFrameFromProgramCounter( pc );
                if ( (frame != -1) && (frame == m_StackFrames.GetCount() - 1) )
                {
                    pc += 2;
                }

                m_stepHadBreakpoint = thread->HasBreakpoint( pc, &m_stepBreakpointStopsGame );
                if ( !m_stepHadBreakpoint )
                {
                    // this is a phantom breakpoint that should not show up in the UI
                    // FIXME: This is where we could possibly set stopsGame=true if the last breakpoint we hit stopped the game.
                    thread->SetBreakpoint( pc, true );
                }

                Continue();

                // set here, because Continue() will normally clear it
                m_stepPC = pc;
                return;
            }
        }
    }

    scrWarningf( "StepOut failed." );
    m_Editor->OperationFailed();
}

void scrDebugger::ClearPhantomBreakpoint()
{
    if ( m_stepPC != -1 )
    {    
        // if we are here, that means we stepped, but were stopped by another breakpoint inside
        scrThread *thread = scrThread::GetThread( m_ThreadId );
        if ( thread )
        {
            // restore the phantom breakpoint to its previous state
            thread->SetBreakpoint( m_stepPC, m_stepHadBreakpoint, m_stepBreakpointStopsGame );
        }

        m_stepPC = -1;
    }
}

void scrDebugger::Pause() 
{
    ClearPhantomBreakpoint();

	scrThread *thread = scrThread::GetThread( m_ThreadId );
	if ( thread ) 
    {
		thread->Pause();
		UpdateLineInfo( thread, true );
	}
}

void scrDebugger::Continue() 
{
    ClearPhantomBreakpoint();

	scrThread *thread = scrThread::GetThread( m_ThreadId );
	if ( thread ) 
    {
		thread->Continue();
		UpdateLineInfo( thread, true );
	}
}

void scrDebugger::UpdateLocal( s32 key, s32 dataType, const char *newValue ) 
{
    scrDebugger::UpdateCommon( LOCAL, this, key, dataType, newValue );
}

void scrDebugger::UpdateStatic( s32 key, s32 dataType, const char *newValue ) 
{
    scrDebugger::UpdateCommon( STATIC, this, key, dataType, newValue );
}

void scrDebugger::UpdateGlobal( s32 key, s32 dataType, const char *newValue )
{
    scrDebugger::UpdateCommon( GLOBAL, this, key, dataType, newValue );
}

void scrDebugger::UpdateMasterGlobal( s32 key, s32 dataType, const char *newValue )
{
    Node_t *pKeyNode = (Node_t*) (size_t) key;
    scrDebugger *pDebugger = pKeyNode->Debugger;
    scrDebugger::UpdateCommon( GLOBAL, pDebugger, key, dataType, newValue );
}

void scrDebugger::UpdateCommon( int /*scope*/, scrDebugger *pDebugger, s32 key, s32 /*dataType*/, const char *newValue ) 
{
	scrThread *thread = scrThread::GetThread( pDebugger->m_ThreadId );
	Node_t *node = (Node_t*) (size_t)key;

	scrValue *value = scrDebugger::Resolve( thread, node->ValueAddr );
	if ( value )
    {
        node->Type->SetValue( value, newValue );
    }
}

void scrDebugger::DragDropGlobalNode( s32 key )
{
    scrDebugger::DragDropNodeCommon( (Node_t*)(size_t)key, m_Items, m_Roots[GLOBAL], m_TreeLists[GLOBAL] );
}

void scrDebugger::DragDropMasterGlobal( s32 key )
{
    scrDebugger::DragDropNodeCommon( (Node_t*)(size_t)key, sm_masterGlobalsItems, sm_masterGlobalsRoots, sm_masterGlobalsTreeList );
}

void scrDebugger::DragDropNodeCommon( Node_t *node, atArray<Item> &items, Node_t* &roots, bkTreeList *treelist )
{
    Node_t **nextNode = &roots;
    if ( roots != NULL )
    {
        while ( *nextNode )
        {
            // find empty Sibling so we can assign a root to it
            nextNode = &(*nextNode)->Sibling;
        }
    }

    // search the source debugger's items for ValueAddr
    Item *item = Item::FindItem( node->Debugger, node->Debugger->m_Items, node->ValueAddr );
    if ( item != NULL )
    {
        // copy the item
        Item &copyItem = items.Grow();
        copyItem.Address = item->Address;
        copyItem.Frame = item->Frame;
        safecpy( copyItem.Name, item->Name, sizeof(copyItem.Name) );
        copyItem.SetScope( item->GetScope() );
        copyItem.Type = item->Type;           

        // add it to the treelist
        copyItem.Type->SetupDisplay( *nextNode, scrThread::GetThread(node->Debugger->GetThreadId()), node->Debugger, &copyItem, treelist );
    }
}

static int s_numTabs = 0;

scrDebugger::Item* scrDebugger::Item::FindItem( scrDebugger *debugger, atArray<Item> &items, int address )
{
    /* not allowing locals at this time
    scrThread *thread = scrThread::GetThread( debugger->m_ThreadId );
    if ( thread == NULL )
    {
        return NULL;
    }

    int currentFrame = debugger->GetStackFrameFromProgramCounter( thread->GetProgramCounter(0) );
    */

    for (int i = 0; i < items.GetCount(); ++i) 
    {
        Item *item = &items[i];

        // match address.  if local, match address and current frame
        if ( item->Address == address /*&& ((item->GetScope() != LOCAL || item->Frame == currentFrame))*/ ) 
        {
            return item;
        }
        else
        {
            ++s_numTabs;
            item = FindItem( debugger, item->SubItems, address );
            --s_numTabs;

            if ( item != NULL )
            {
                return item;
            }
        }
    }

    return NULL;
}

void scrDebugger::DeleteGlobalNode( s32 key )
{
    scrDebugger::DeleteNodeCommon( (Node_t*)(size_t)key, m_Items, m_Roots[GLOBAL], m_TreeLists[GLOBAL] );
}

void scrDebugger::DeleteNodeMasterGlobal( s32 key )
{
    scrDebugger::DeleteNodeCommon( (Node_t*)(size_t)key, sm_masterGlobalsItems, sm_masterGlobalsRoots, sm_masterGlobalsTreeList );
}

void scrDebugger::SetMasterDebugger( scrThreadId threadId )
{
	if ( scrDebugger::sm_masterComboBox != NULL )
	{
		scrThread *pThread = scrThread::GetThread( threadId );
		if ( pThread != NULL )
		{
			char script[128];
			sprintf( script, "%s Thread %d", pThread->GetScriptName(), pThread->GetThreadId());

			int index = scrDebugger::sm_masterComboBox->GetStringIndex( script );
			if ( (index != -1) && (index != scrDebugger::sm_masterComboBoxIndex) )
			{
				scrDebugger::sm_masterComboBoxIndex = index;
				BankComboBoxChanged();
			}
		}
	}
}

void scrDebugger::DeleteNodeCommon( Node_t *node, atArray<Item> &items, Node_t* &roots, bkTreeList *treelist )
{
    // remove globals
    Node_t *n = roots;
    Node_t *prevN = NULL;
    while ( n )
    {
        Node_t *nextN = n->Sibling;

        if ( n == node )
        {
            // remove item
            int count = items.GetCount();
            for (int i = 0; i < count; ++i)
            {
                Item item = items[i];
                if ( item.Address == n->ValueAddr )
                {
                    items.Delete( i );
                    break;
                }
            }

            n->Type->RemoveDisplay( n, treelist );

            if ( prevN != NULL )
            {
                // hop over the one we are removing
                prevN->Sibling = nextN;
            }
            else
            {
                roots = nextN;
            }
            break;
        }
        else
        {
            if ( prevN == NULL )
            {
                // we didn't remove the first n in the list
                prevN = n;
            }
            else
            {
                prevN = prevN->Sibling;
            }
        }

        n = nextN;
    }
}

void scrDebugger::BankKillAllScripts()
{
    scrThread::KillAllThreads();
}

void scrDebugger::BankLoadScript()
{
    char fullScriptPath[256];
    memset( fullScriptPath, 0, 256 );

    if ( BANKMGR.OpenFile(fullScriptPath,256,"*.sco",false,"Compiled Script Files (*.sco)") )
    {
        fiAssetManager::RemoveExtensionFromPath( fullScriptPath, 256, fullScriptPath );
        scrDebugger::RegisterDebugger( rage_new scrDebugger(fullScriptPath,scrThread::CreateThread(fullScriptPath)) );
        scrThread::LoadGlobalVariableInfo( fullScriptPath );
    }
}

void scrDebugger::BankKillScripts( bkBank *pBank )
{
    int count = scrThread::GetThreadCount();
    for (int i = 0; i < count; ++i)
    {
        scrThread &thread = scrThread::GetThreadByIndex( i );
        if ( (thread.GetThreadId() != 0) && (strcmp(thread.GetScriptName(),pBank->GetTitle()) == 0) )
        {
            scrThread::KillThread( &thread );
        }
    }
}

void scrDebugger::BankComboBoxChanged()
{
	scrDebugger::sm_createMasterDebugger = true;

	if ( scrDebugger::sm_masterDebugger != NULL )
	{
		scrDebugger::KillDebugger( scrDebugger::sm_masterDebugger );
		scrDebugger::sm_masterDebugger = NULL;
	}

	if ( (scrDebugger::sm_masterComboBoxIndex >= 0) && (scrDebugger::sm_masterComboBoxIndex < scrThread::GetThreadCount()) )
	{
		const char* selectedText = scrDebugger::sm_masterComboBox->GetString( scrDebugger::sm_masterComboBoxIndex );
		if ( selectedText != NULL )
		{
			int i = StringLength( selectedText );
			while ( (i > 0) && (selectedText[i - 1] != ' ') )
			{
				--i;
			}

			if ( i >= 0 )
			{
				int id = atoi( &(selectedText[i]) );
				if ( id )
				{
					scrThread *pThread = scrThread::GetThread( static_cast<scrThreadId>( id ) );
					if ( pThread != NULL )
					{
						scrDebugger::sm_masterDebugger = rage_new scrDebugger( pThread->GetScriptName(), pThread->GetThreadId(), scrDebugger::sm_debuggerBank, false );
					}
				}
			}
		}		
	}

	scrDebugger::sm_createMasterDebugger = false;
}

void scrDebugger::PopulateMasterDebuggerComboBox()
{
	if ( scrDebugger::sm_debuggerBank != NULL )
	{
        // get all of the active script threads
		atArray<scrThread *> scripts( 0, 32 );
		for ( int i = 0; i < scrThread::GetThreadCount(); ++i )
		{
			scrThread &thread = scrThread::GetThreadByIndex( i );

			if ( thread.GetThreadId() )
			{
				scripts.Append() = &thread;
			}
		}

        // create the combo box
		if ( scrDebugger::sm_masterComboBox == NULL )
		{
            const char *tempList[] = { "" };
			scrDebugger::sm_masterComboBox = dynamic_cast<bkCombo *>( 
                scrDebugger::sm_debuggerBank->AddCombo( "Script", &scrDebugger::sm_masterComboBoxIndex, 1, tempList ) );
		}

        // remember what is currently selected
        const char* selectedText = scrDebugger::sm_masterComboBox->GetString( scrDebugger::sm_masterComboBoxIndex );
        scrDebugger::sm_masterComboBoxIndex = -1;

        // update the combo with the name of each script thread.  We will do it one by one in case of long script names
        int count = scripts.GetCount();        
		scrDebugger::sm_masterComboBox->UpdateCombo( scrDebugger::sm_masterComboBox->GetTitle(), &scrDebugger::sm_masterComboBoxIndex,
			count, NULL, datCallback(CFA(scrDebugger::BankComboBoxChanged)) );

        for ( int i = 0; i < count; ++i )
        {
            char text[256];
            sprintf( text, "%s Thread %d", scripts[i]->GetScriptName(), scripts[i]->GetThreadId() );

            scrDebugger::sm_masterComboBox->SetString( i, text );
        }

        // restore the current selection        
        if ( (selectedText != NULL) && strcmp( selectedText, "" ) )
        {
            for ( int i = 0; i < count; ++i )
            {
                if ( strcmp( selectedText, scrDebugger::sm_masterComboBox->GetString(i) ) == 0 )
                {
                    scrDebugger::sm_masterComboBoxIndex = i;
                    break;
                }
            }
        }
	}
}

void scrDebugger::BankMakeMeMaster()
{
	scrDebugger::SetMasterDebugger( m_ThreadId );
}

void scrDebugger::BankKillScript()
{
    scrThread::KillThread( m_ThreadId );
}

void scrDebugger::BankCallstackDoubleClickFunction( s32 row )
{
    if ( (row >= 0) && (row < m_currentStackFrameInfo.GetCount()) )
    {
        if ( row == 0 )
        {
            m_Editor->SetProgramCounter( m_FileInfo[m_LastFileIndex].c_str(), 
                m_LastLineNumber - HACK_HACK_HACK );
        }
        else 
        {
            int lineInfoIndex = GetLineInfoFromProgramCounter( m_currentStackFrameInfo[row].pc, 
                m_currentStackFrameInfo[row].level, m_currentStackFrameInfo[row].frame );
            if ( lineInfoIndex != -1 )
            {
                m_Editor->SetProgramCounter( m_FileInfo[m_LineInfo[lineInfoIndex].m_FileIndex].c_str(), 
                    m_LineInfo[lineInfoIndex].m_LineNumber - HACK_HACK_HACK );
            }
        }
    }
}

}	// namespace rage

#else

int scrDebuggerDummySymbolSoLinkerDoesntComplain;

#endif
