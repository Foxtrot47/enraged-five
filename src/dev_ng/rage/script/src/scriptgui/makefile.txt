Project scriptgui

IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\script\src

Files {
	debugger.cpp
	debugger.h
	widgeteditor.cpp
	widgeteditor.h
}
Custom {
	scriptgui.dtx
}
