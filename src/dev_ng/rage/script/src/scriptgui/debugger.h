// 
// script/debugger.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SCRIPT_DEBUGGER_H
#define SCRIPT_DEBUGGER_H

#include "atl/array.h"
#include "atl/functor.h"
#include "atl/map.h"
#include "data/callback.h"
#include "string/string.h"
#include "script/thread.h"

// The debugger is implemented using the bank interface, so it's not going to work in a release build.
#if __BANK

namespace rage {

class bkBank;
class bkCombo;
class bkList;
class bkGroup;
class bkTreeList;
class scrThread;
class scrWidgetEditor;
union scrValue;

class scrDebugger : public datBase 
{
	enum { LOCAL, STATIC, GLOBAL };
	enum { IS_REFERENCE = 512, OFFSET_SHIFT = 10 };

public:
    // PURPOSE: parses static, local, global, and stack trace info from the scrThread, builds the debugger widgets
    // PARAMS:  scriptName - path and file name of script to load (sans .sco extension)
    //          id - returned by scrThread::CreateThread(...)
    //          pParentBank - bank under which the debugger widgets should be created.  If null, we'll create a bank for the program (if it doesn't
    //              exist already), and a group for the thread.  This way, a program with multiple threads is easily identifiable.
    //          bDisplayGlobals - whether or not to display the list of globals on the debugger in question.
	scrDebugger( const char *scriptName, scrThreadId id, bkBank* pParentBank=0, bool bDisplayGlobals=true );
	
    ~scrDebugger();

    // PURPOSE: initialize debugger array, the default killDebuggersUponCompletion flag, and create the base "rage - Script" widget bank
    // PARAMS:  count - number of debuggers to allocate.  Should normally equal the first value passed into scrThread::AllocateThreads(...).
    //          killDebuggersUponCompletion - when true, debuggers will be killed (i.e. removed from the widgets) when their associated
    //              thread terminates.
    // NOTES:   needs to be called if you plan on using CreateDebugger(...) or RegisterDebugger(...) to have your debuggers managed for you
    static void InitClass( int count, bool killDebuggersUponCompletion );
    
    // PURPOSE: kill all active debuggers and clear the debugger array
    // NOTES:   needs to be called if you plan on using CreateDebugger(...) or RegisterDebugger(...) to have your debuggers managed for you
    static void ShutdownClass();

    // PURPOSE: create a debugger and register it in the debugger array so users don't have to manage it on their own
    // PARAMS:  scriptName - path and file name of script to load (sans .sco extension)
    //          id - returned by scrThread::CreateThread(...)
    //          pParentBank - bank under which the debugger widgets should be created.  If null, we'll create a bank for the program (if it doesn't
    //              exist already), and a group for the thread.  This way, a program with multiple threads is easily identifiable.
    //          bDisplayGlobals - whether or not to display the list of globals on the debugger in question.
    // RETURNS: true on success, false if our debugger array is full
    // NOTES:   Using this method to create your script debugger, turns over control to the static scrDebugger functions.  You need to call 
    //              InitClass, ShutdownClass, and UpdateAll in the appropriate places.
    static bool CreateDebugger( const char *scriptName, scrThreadId id, bkBank* pParentBank=0, bool bDisplayGlobals=true );
    
    // PURPOSE: register a debugger you have already created on your own
    // PARAMS:  pDebugger
    // RETURNS: true on success, false if our debugger array is full
    // NOTES:   See Notes for CreateDebugger
    static bool RegisterDebugger( scrDebugger *pDebugger );

    // PURPOSE: Retrieve a registered debugger by its thread id
    // PARAMS:  id
    // RETURNS: NULL if not found (but doesn't mean that the thread doesn't have a debugger attached)
    static scrDebugger* GetDebugger( scrThreadId id );

    // PURPOSE: manually terminate a debugger
    // PARAMS:  id
    // NOTES:   this does NOT kill the related scrThread
    static void KillDebugger( scrThreadId id );

    // PURPOSE: calls update for every debugger added via CreateDebugger(...) and RegisterDebugger(...)
    // NOTES:   this will call KillDebugger(...) for debuggers with completed threads if sm_killDebuggersUponCompletion is true
    static void UpdateAll();

    // PURPOSE: terminates all debuggers in the debugger list
    static void KillAllDebuggers();

    static scrValue* Resolve(scrThread *thread,int address);	

    // PURPOSE: Breakpoint callback functor for applications.
    // PARAMS:
    //    scrDebugger* - the debugger for the thread
    //    const char* - the filename of the breakpoint
    //    int - the line number in the file
    typedef Functor3<scrDebugger*, const char*, int> DebuggerBreakpointFunc;    

    // PURPOSE: Allow applications to register a breakpoint callback
    // PARAMS:
    //    del - the breakpoint callback functor
    static void SetBreakpointCallback( DebuggerBreakpointFunc del );

    // PURPOSE: Callback functor for a breakpoint that stops the game
    // RETURNS: true to keep waiting for the debugger GUI to hit Play or Step, false to continue without waiting.
    typedef Functor0Ret<bool> DebuggerStoppedGameUpdateFunc;

    // PURPOSE: Allow applications to register a stopped game update callback
    // PARAMS:
    //    del - the stopped game update callback functor
    static void SetStoppedGameUpdateCallback( DebuggerStoppedGameUpdateFunc del );

	void Update();

	scrThreadId GetThreadId() const { return m_ThreadId; }

	static const int ValueBufferSize = 64;

	void UpdateLineInfo(scrThread *thread,bool force);

	int GetLastLineNumber() const
	{
		return m_LastLineNumber;
	}

    class Type_t;

    struct Item 
    {
        scrValue *Resolve(scrThread *thread) const;
        int GetScope() const { return Address >> 28; }
        void SetScope(int scope) { Address = (Address & 0xFFFFFFF) | (scope << 28); }
        void SetAddress(int addr) { Address = addr; }

        static Item* FindItem( scrDebugger *debugger, atArray<Item> &items, int address );

        Type_t *Type;
        int Address;
        int Frame;
        char Name[24];

        atArray<Item> SubItems;
    };

	struct Node_t 
    {
		Node_t() : Child(0), Sibling(0) 
        { 
			LastValue[0] = '\0';
        }

		const scrDebugger::Type_t *Type;
		int ValueAddr;
		char LastValue[ValueBufferSize];
		Node_t *Child, *Sibling;
        scrDebugger *Debugger;
		
        static void UpdateList( Node_t *node, bkTreeList *treelist, scrThread *thread );
		static void RemoveList( Node_t *node, bkTreeList *treelist );
	};

	class Type_t 
    {
	public:
		virtual ~Type_t() {}

		virtual const char *GetName() const = 0;
		virtual bool GetValue(char *dest,int destSize,const scrValue *value) const = 0;
		virtual bool SetValue(scrValue *value,const char *input) const = 0;
		virtual int GetSize(const scrValue *) const { return 1; }
 
        virtual void SetupDisplay( Node_t* &root, scrThread *thread, scrDebugger *pDebugger, 
            Item *item, bkTreeList *treelist, s32 nodeKey=-1 ) const;
        virtual void RemoveDisplay( Node_t* root, bkTreeList *treelist ) const;
        virtual void UpdateDisplay( Node_t* root, bkTreeList *treelist, scrThread *thread ) const;

        virtual void BuildTreeListNode( bkTreeList *treelist, const char* name, s32 key, scrValue *value, s32 nodeKey ) const = 0;
    
    protected:
        void BuildTreeListNode( bkTreeList *treelist, const char *name, s32 key, int val, s32 nodeKey, bool readOnly=false, bool updateParent=true ) const;
        void BuildTreeListNode( bkTreeList *treelist, const char *name, s32 key, float val, s32 nodeKey, bool readOnly=false, bool updateParent=true ) const;
        void BuildTreeListNode( bkTreeList *treelist, const char *name, s32 key, const char *val, s32 nodeKey, bool readOnly=false, bool updateParent=true ) const;
        void BuildTreeListNode( bkTreeList *treelist, const char *name, s32 key, bool val, s32 nodeKey, bool readOnly=false, bool updateParent=true ) const;
	};

    // Made public to placate PS3 compiler
    static void UpdateMasterGlobal( s32 key, s32 dataType, const char* newValue );
    static void DragDropMasterGlobal( s32 key );
    static void DeleteNodeMasterGlobal( s32 key );

	static void SetMasterDebugger( scrThreadId threadId );

	static void PopulateMasterDebuggerComboBox();

private:
    struct LineInfo 
    {
        int m_BasePc;
        u32 m_LineNumber;
        u16 m_FileIndex;
    };

	int GetStackFrameFromProgramCounter(int pc) const;
    int GetLineInfoFromProgramCounter(int pc, int level, int frame) const;
    int GetBasePC( const char *filename, s32 line ) const;
	Type_t* ParseType(const char *typeString);
	
    void UpdateLocal( s32 key, s32 dataType, const char *newValue );
	void UpdateStatic( s32 key, s32 dataType, const char *newValue );
	void UpdateGlobal( s32 key, s32 dataType, const char *newValue );   
	static void UpdateCommon( int scope, scrDebugger *pDebugger, s32 address, s32 dataType, const char *newValue );

    void DragDropGlobalNode( s32 key );
    static void DragDropNodeCommon( Node_t *node, atArray<Item> &items, Node_t* &roots, bkTreeList *treelist );

    void DeleteGlobalNode( s32 key );
    static void DeleteNodeCommon( Node_t *node, atArray<Item> &items, Node_t* &roots, bkTreeList *treelist );
	
	bool SetBreakpoint( const char *filename, s32 line, bool setOrUnSet, bool breakAllThreads, bool breakStopsGame );
	void Continue();
	void Pause();
	void StepInto();
    void StepOver();
    void StepOut();

    int m_stepPC;
    bool m_stepHadBreakpoint;
    bool m_stepBreakpointStopsGame;

    void ClearPhantomBreakpoint();

    static void KillDebugger( scrDebugger *pDebugger );

	void ScriptFaultHandler(const char *str, scrThread *pThread);
    void ScriptBreakpointCallback(scrThread *pThread, int pc);

	atArray<Item> m_Items;
	bkBank *m_Interface;
	bkGroup *m_Group;					//Used if interface is not owned
	bkTreeList *m_TreeLists[3];					// Indexed by Scope
	Node_t *m_Roots[3];
	bkList *m_Callstack;
	int m_CallstackDepth;
	scrThreadId m_ThreadId;
	scrWidgetEditor *m_Editor;
	atArray<LineInfo, 0, u32> m_LineInfo;
	atArray<ConstString> m_FileInfo;
	
    struct StackFrame { int Start, End; ConstString Name; };
	atArray<StackFrame> m_StackFrames;

    struct StackFrameInfo { int pc, level, frame; };
    atArray<StackFrameInfo> m_currentStackFrameInfo;

	atMap<ConstString,Type_t*> m_Types;
	int m_LastLineNumber;
	int m_LastFileIndex;
	int m_LastStackFrame;
	char m_Status[32];

    static void BankKillAllScripts();
    static void BankLoadScript();

    static void BankKillScripts( bkBank *pBank );
	static void BankComboBoxChanged();	

	void BankMakeMeMaster();
    void BankKillScript();
    void BankCallstackDoubleClickFunction( s32 row );

    bool m_parentBankPassedIn;
    bool m_killDebuggerUponCompletion;

    static atArray< scrDebugger* > sm_scriptDebuggers;
    static bkBank *sm_debuggerBank;
    static bool sm_killDebuggersUponCompletion;
    
    static atArray<Item> sm_masterGlobalsItems;
    static bkTreeList *sm_masterGlobalsTreeList;
    static Node_t *sm_masterGlobalsRoots;
	static int sm_masterComboBoxIndex;
	static bkCombo *sm_masterComboBox;
	static scrDebugger *sm_masterDebugger;
	static bool sm_createMasterDebugger;

    static DebuggerBreakpointFunc sm_breakpointCallback;
    static DebuggerStoppedGameUpdateFunc sm_stoppedGameUpdateCallback;
};

}	// namespace rage

#endif

#endif	// SCRIPT_DEBUGGER_H
