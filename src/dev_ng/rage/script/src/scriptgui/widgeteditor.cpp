//
// bank/widgeteditor.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "widgeteditor.h"

#include "bank/pane.h"
#include "script/scrchannel.h"

using namespace rage;

scrWidgetEditor::scrWidgetEditor( const char* scriptfile, const char *title, const char *memo, const char *fillColor )
: bkWidget( NullCallback, title, memo, fillColor )
, m_scriptFile(scriptfile)
, m_lastThreadState(scrThread::ABORTED)
{
	// add the handler if it hasn't been added all ready:
	static bool addedHandler = false;
	if ( addedHandler == false )
	{
		bkRemotePacket::AddType( GetStaticGuid(), RemoteHandler );
		addedHandler = true;
	}
}

scrWidgetEditor::~scrWidgetEditor() 
{
}

int scrWidgetEditor::GetGuid() const { return GetStaticGuid(); }


int scrWidgetEditor::DrawLocal( int x, int y ) 
{
	Drawf( x, y, m_Title );
	return bkWidget::DrawLocal( x, y );
}

enum 
{
	ENABLE_BREAKPOINT			=bkRemotePacket::USER+0,
	DISABLE_BREAKPOINT			=bkRemotePacket::USER+1,
	CONTINUE					=bkRemotePacket::USER+2,
	PAUSE						=bkRemotePacket::USER+3,
	STEP_INTO					=bkRemotePacket::USER+4,
	SET_PROGRAM_COUNTER			=bkRemotePacket::USER+5,
	EXIT						=bkRemotePacket::USER+6,
    STEP_OVER					=bkRemotePacket::USER+7,
    STEP_OUT					=bkRemotePacket::USER+8,
    OPERATION_FAILED            =bkRemotePacket::USER+9
};


void scrWidgetEditor::RemoteCreate() 
{
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin( bkRemotePacket::CREATE, GetStaticGuid(), this );
	p.WriteWidget( m_Parent );
	p.Write_const_char( m_Title );
	p.Write_const_char( GetTooltip() );
	p.Write_const_char( GetFillColor() );
	p.Write_bool( IsReadOnly() );
	p.Write_const_char( m_scriptFile.c_str() );
	
	switch ( m_lastThreadState )
	{
	case scrThread::ABORTED:
		p.Write_s32( EXIT );
		break;
	case scrThread::HALTED:
		p.Write_s32( PAUSE );
		break;
	default:
		p.Write_s32( CONTINUE );
		break;
	}
	
	p.Send();
}

void scrWidgetEditor::UpdateThreadState( scrThread::State state, const char* filename, int lineNumber, const char* status )
{
	if ( state != m_lastThreadState )
	{
		switch ( state )
		{
		case scrThread::ABORTED:
			{
				bool normalExit = true;
				if ( status != NULL )
				{
					if ( (strcmp( status, "Normal exit." ) != 0) 
						&& (strcmp( status, "via scrThread::Kill on myself" ) != 0) )
					{
						normalExit = false;

						// Set the program counter to the point of the fault
						SetProgramCounter( filename, lineNumber );
					}
				}

				Exit( normalExit );
			}
			break;
		case scrThread::BLOCKED:
			if ( m_lastThreadState == scrThread::ABORTED )
			{
				Continue();
			}

			SetProgramCounter( filename, lineNumber );
			break;
		case scrThread::HALTED:
			Pause( filename, lineNumber );
			break;
		case scrThread::RUNNING:
			Continue();
			break;
		}
	}

	m_lastThreadState = state;
}

void scrWidgetEditor::SetProgramCounter( const char* filename, int lineNumber )
{
	if ( !bkRemotePacket::IsConnected() )
	{
		return;
	}

	bkRemotePacket p;
	p.Begin( SET_PROGRAM_COUNTER, GetStaticGuid(), this );
	p.Write_const_char( filename );
	p.Write_s32( lineNumber );
	p.Send();

	scrDebugf1( "SetProgramCounter %s %d", filename, lineNumber + 1 );
}

void scrWidgetEditor::SetBreakpoint( const char* filename, int lineNumber, bool setOrUnSet, bool breakAllThreads, bool breakStopsGame )
{
    SendBreakpointRequestResponse( setOrUnSet ? ENABLE_BREAKPOINT : DISABLE_BREAKPOINT, filename, lineNumber, true, 
        breakAllThreads, breakStopsGame );
}

void scrWidgetEditor::Continue()
{
	if ( !bkRemotePacket::IsConnected() )
	{
		return;
	}

	bkRemotePacket p;
	p.Begin( CONTINUE, GetStaticGuid(), this );
	p.Send();

	scrDebugf1( "Continue %s", m_scriptFile.c_str() );
}

void scrWidgetEditor::Pause( const char* filename, int lineNumber )
{
	if ( !bkRemotePacket::IsConnected() )
	{
		return;
	}

	bkRemotePacket p;
	p.Begin( PAUSE, GetStaticGuid(), this );
	p.Write_const_char( filename );
	p.Write_s32( lineNumber );
	p.Send();

	scrDebugf1( "Pause %s %d", filename, lineNumber + 1 );
}

void scrWidgetEditor::StepInto( const char* filename, int lineNumber )
{
	if ( !bkRemotePacket::IsConnected() || (lineNumber == -1) )
	{
		return;
	}

	bkRemotePacket p;
	p.Begin( STEP_INTO, GetStaticGuid(), this );
	p.Write_const_char( filename );
	p.Write_s32( lineNumber );
	p.Send();

	scrDebugf1( "StepInto %s %d", filename, lineNumber + 1 );
}

void scrWidgetEditor::StepOver( const char* filename, int lineNumber )
{
    if ( !bkRemotePacket::IsConnected() || (lineNumber == -1) )
    {
        return;
    }

    bkRemotePacket p;
    p.Begin( STEP_OVER, GetStaticGuid(), this );
    p.Write_const_char( filename );
    p.Write_s32( lineNumber );
    p.Send();

    scrDebugf1( "StepOver %s %d", filename, lineNumber + 1 );
}

void scrWidgetEditor::StepOut( const char* filename, int lineNumber )
{
    if ( !bkRemotePacket::IsConnected() || (lineNumber == -1) )
    {
        return;
    }

    bkRemotePacket p;
    p.Begin( STEP_OUT, GetStaticGuid(), this );
    p.Write_const_char( filename );
    p.Write_s32( lineNumber );
    p.Send();

    scrDebugf1( "StepOut %s %d", filename, lineNumber + 1 );
}

void scrWidgetEditor::Exit( bool normalExit )
{
	if ( !bkRemotePacket::IsConnected() || (m_lastThreadState == scrThread::ABORTED) )
	{
		return;
	}

	bkRemotePacket p;
	p.Begin( EXIT, GetStaticGuid(), this );
	p.Write_bool( normalExit );
	p.Send();

	m_lastThreadState = scrThread::ABORTED;

	scrDebugf1( "Exit %s:", m_scriptFile.c_str() );
}

void scrWidgetEditor::OperationFailed()
{
    if ( !bkRemotePacket::IsConnected() )
    {
        return;
    }

    bkRemotePacket p;
    p.Begin( OPERATION_FAILED, GetStaticGuid(), this );
    p.Send();

    scrDebugf1( "OperationFailed %s:", m_scriptFile.c_str() );
}

void scrWidgetEditor::RemoteUpdate() 
{
	//bkRemotePacket p;
	//p.Begin(bkRemotePacket::CHANGED,GetStaticGuid());
	//p.Write_bkWidget(bkRemotePacket::LocalToRemote(*this));
	//p.Write_const_char(m_String);
	//p.Send();
}

#if __WIN32PC
#include "system/xtl.h"

void scrWidgetEditor::WindowCreate() 
{
	if ( !bkRemotePacket::IsConnectedToRag() )
	{
		GetPane()->AddLastWindow( this, "STATIC", 0, SS_LEFT | SS_NOPREFIX );
	}
}
#endif // __WIN32PC

void scrWidgetEditor::RemoteHandler(const bkRemotePacket& packet) 
{
	switch ( packet.GetCommand() )
	{
	case ENABLE_BREAKPOINT:
	case DISABLE_BREAKPOINT:
		{
			packet.Begin();		
			scrWidgetEditor* widget = packet.ReadWidget<scrWidgetEditor>();
			if (!widget) return;

			const char* filename = packet.Read_const_char();
			s32 lineNumber = packet.Read_s32();
            bool breakAllThreads = packet.Read_bool();
            bool breakStopsGame = packet.Read_bool();
			packet.End();

			bool breakpointAllowed = false;
			if ( widget->m_BreakpointFunc.IsValid() )
			{
				breakpointAllowed = widget->m_BreakpointFunc( filename, lineNumber, 
                    packet.GetCommand() == ENABLE_BREAKPOINT, breakAllThreads, breakStopsGame );
			}
	
			widget->SendBreakpointRequestResponse( packet.GetCommand(), filename, lineNumber, breakpointAllowed, breakAllThreads, breakStopsGame );
		}
		break;
	case CONTINUE:
		{
			ProcessDebuggerAction( packet, ACTION_CONTINUE );
		}
		break;
	case PAUSE:		
		{
			ProcessDebuggerAction( packet, ACTION_PAUSE );
		}
		break;
	case STEP_INTO:
		{
			ProcessDebuggerAction( packet, ACTION_STEP_INTO );
		}
		break;
    case STEP_OVER:
        {
            ProcessDebuggerAction( packet, ACTION_STEP_OVER );
        }
        break;
    case STEP_OUT:
        {
            ProcessDebuggerAction( packet, ACTION_STEP_OUT );
        }
        break;
	default:
		break;
	}
}

void scrWidgetEditor::SendBreakpointRequestResponse( int command, const char* filename, int lineNumber, bool allowed, 
                                                    bool breakAllThreads, bool breakStopsGame )
{
	if ( !bkRemotePacket::IsConnected() ) return;

	bkRemotePacket p;
	p.Begin( command, GetStaticGuid(), this );
	p.Write_const_char( filename );
	p.Write_s32( lineNumber );
	p.Write_bool( allowed );
    p.Write_bool( breakAllThreads );
    p.Write_bool( breakStopsGame );
	p.Send();
}

void scrWidgetEditor::ProcessDebuggerAction( const bkRemotePacket& packet, EnumDebuggerAction action )
{
	packet.Begin();		

	scrWidgetEditor *widget = packet.ReadWidget<scrWidgetEditor>();
	if ( !widget ) return;
	packet.End();

	if ( widget->m_DebuggerActionFuncs[action].IsValid() )
	{
		widget->m_DebuggerActionFuncs[action]();
	}
}

void scrWidgetEditor::Update() 
{
}

#endif
