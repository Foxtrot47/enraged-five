//
// bank/widgeteditor.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_WIDGETEDITOR_H
#define BANK_WIDGETEDITOR_H

#if __BANK

#include "atl/atfunctor.h"
#include "atl/string.h"
#include "bank/widget.h"
#include "bank/packet.h"
#include "script/thread.h"

namespace rage 
{

class scrWidgetEditor : public bkWidget 
{
public:
	scrWidgetEditor( const char* scriptfile, const char *title, const char *memo, const char *fillColor=NULL );

	typedef atFunctor5<bool, const char*, s32, bool, bool, bool> BreakpointFuncType;
	typedef atFunctor0<void> DebuggerActionFuncType;

	//
	// PURPOSE
	//	Set the functor that gets called when the GUI sets/unsets a breakpoint.
	//  UpdateItemFuncType receives arguments in this order :
	//	the const char* filename, s32 lineNumber, bool setOrUnSet, and bool breakAllThreads.
	//  the functor should return true if the breakpoint is allowed, false 
	//  if not.
	// PARAMS
	//	func - the functor we'll call when a breakpoint is set by the GUI
	//
	void SetBreakpointCB( BreakpointFuncType func );

	enum EnumDebuggerAction
	{	
		ACTION_CONTINUE,		// cause execution to continue
		ACTION_PAUSE,			// cause execution to pause
		ACTION_STEP_INTO,		// cause execution to step into
        ACTION_STEP_OVER,       // cause execution to step over
        ACTION_STEP_OUT,        // cause execution to step out
	};

	enum { NUM_DEBUGGER_ACTIONS = ACTION_STEP_OUT + 1 };

	//
	// PURPOSE
	//	Set the functor that gets called when the GUI requests a debugger action.
	//  DebuggerActionFuncType receives no arguments.
	// PARAMS
	//  action - the action that will cause the functor to trigger
	//	func - the functor we'll call when GUI requests a debugger action.
	//
	void SetDebuggerActionCB( EnumDebuggerAction action, DebuggerActionFuncType func );

	//
	// PURPOSE
	//  Synchronizes the editors' thread state with its associated scrDebugger by sending
	//  a message to the Rag Script Debugger.
	// PARAMS
	//  state - the new state
	//  filename - the current filename where the program counter is
	//  lineNumber - the line in that file where the program counter is
	//  status - a string of text describing the state in more detail.  For example, if state 
	//    is ABORTED, that status should be the reason for the abort.
	//
	void UpdateThreadState( scrThread::State state, const char* filename, int lineNumber, const char* status=NULL );
	
	//
	// PURPOSE
	//  Let the GUI know where the program counter is (typically when we're in the BLOCKED state)
	// PARAMS
	//	filename - the file that we're in
	//  lineNumber - where the program counter is
	//
	void SetProgramCounter( const char* filename, int lineNumber );

    // PURPOSE
    //   Tell the GUI to add a breakpoint (typically when another GUI has modified a breakpoint's state).
    // PARAMS
    //	filename - the file that we're in
    //  lineNumber - where the program counter is
    //  setOrUnSet - enable or disable the breakpoint
    //  breakAllThreads - whether or not the breakpoint applies to all threads of the same program
    //  breakStopsGame - whether or not the breakpoint stops the game when hit
    void SetBreakpoint( const char* filename, int lineNumber, bool setOrUnSet, bool breakAllThreads, bool breakStopsGame );

	// 
	// PURPOSE
	//  Tell the GUI to Play
	//
	void Continue();

	// 
	// PURPOSE
	//  Tell the GUI to Pause in the given file name on and the line number
	// PARAMS
	//  filename - the file to pause in
	//  lineNumber - the line in the file to pause on
	//
	void Pause( const char* filename, int lineNumber );

	//
	// PURPOSE
	//  Tell the GUI to Step Into the given file name on the line number
    // PARAMS
	//  filename - the file to step to
	//  lineNumber - the line in the file to step to
	//
	void StepInto( const char* filename, int lineNumber );

    //
    // PURPOSE:
    //  Tell the GUI to Step Over to the given file name and the line number
    // PARAMS
    //  filename - the file to step to
    //  lineNumber - the line in the file to step to
    //
    void StepOver( const char* filename, int lineNumber );

    //
    // PURPOSE:
    //  Tell the GUI to Step Out to the given file name and the line number
    // PARAMS
    //  filename - the file to step to
    //  lineNumber - the line in the file to step to
    //
    void StepOut( const char* filename, int lineNumber );

    // 
	// PURPOSE
	//  Tell the GUI that the program is no longer executing
	// PARAMS
	//  normalExit - whether we exited normally or due to a fault
	//
	void Exit( bool normalExit=true );

    //
    // PURPOSE
    //  Tell the GUI that our operation failed so it can allow breakpoints and other operations again.
    //
    void OperationFailed();

protected:
	~scrWidgetEditor();

	static int GetStaticGuid() { return BKGUID('s','c','e','d'); }
	int GetGuid() const;

	int DrawLocal(int x,int y);

	void Update();
	static void RemoteHandler( const bkRemotePacket& p );
	static void ProcessDebuggerAction( const bkRemotePacket& packet, EnumDebuggerAction action );

	void SendBreakpointRequestResponse( int command, const char* filename, int lineNumber, bool allowed, bool breakAllThreads, bool breakStopsGame );

	BreakpointFuncType m_BreakpointFunc;
	atRangeArray<DebuggerActionFuncType, NUM_DEBUGGER_ACTIONS> m_DebuggerActionFuncs;

	void RemoteCreate();
	void RemoteUpdate();

#if __WIN32PC
	void WindowCreate();
	rageLRESULT WindowMessage( rageUINT, rageWPARAM, rageLPARAM ) { return 0; }
#endif

private:
	scrWidgetEditor();
	const scrWidgetEditor& operator=( scrWidgetEditor& );

	atString m_scriptFile;
	scrThread::State m_lastThreadState;
};

inline void scrWidgetEditor::SetBreakpointCB( BreakpointFuncType func )
{
	m_BreakpointFunc = func;
}

inline void scrWidgetEditor::SetDebuggerActionCB( EnumDebuggerAction action, DebuggerActionFuncType func )
{
	m_DebuggerActionFuncs[action] = func;
}

}	// namespace rage


#endif

#endif
