// 
// /sample_scr_generic.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// TITLE: sample_scr_generic
//
// PURPOSE:
//   This purpose of this sample is to demonstrate how to use Scripts (created 
//   using the Script Editor and compiled with the Script Compiler) in the rage
//   game engine.
//

#include "system/main.h"
#include "system/param.h"
#include "system/rageroot.h"

#include "sample_grcore/sample_grcore.h"

#include "bank/bkmgr.h"
#include "grcore/im.h"
#include "input/input.h"
#include "scriptgui/debugger.h"
#include "script/bankscript.h"
#include "script/program.h"
#include "script/thread.h"
#include "scr_grcore/grcore.h"

// STEP #1.  Decide whether to run this sample in managed or unmanaged mode.
// -- This compile flag is used to switch this sample's script debuggers between managed and unmanaged mode.
#define UNMANAGED_DEBUGGER 0
// -STOP

using namespace rage;

PARAM(script,"Script to load and run");

class scrGenericSample: public ragesamples::grcSampleManager 
{
public:
    static const int c_MaxScripts = 4;
    int m_numScripts;
#if __BANK
    scrDebugger *m_debuggers[c_MaxScripts];
#endif

	void InitClient()
	{
		// STEP #2. Initialize the scripting engine
		// -- Initialize the scrProgram and scrThread classes
		scrProgram::InitClass();
		scrThread::InitClass(127);
		scrThread::AllocateThreads(20);
		
		// -- In managed mode, you must also initialize the debugger class.
#if __BANK

#if !UNMANAGED_DEBUGGER
        scrDebugger::InitClass( 20, false );
#endif

        // -- An example of using the Breakpoint Callback.
        scrDebugger::SetBreakpointCallback( MakeFunctor(*this, &scrGenericSample::BreakpointCallback) );
        scrDebugger::SetStoppedGameUpdateCallback( MakeFunctorRet(*this, &scrGenericSample::StoppedGameUpdateCallback) );
#endif
		// -STOP

		// STEP #2. Register additional commands
		// -- Register the builtin commands.
		scrThread::RegisterBuiltinCommands();
        
        // -- Register the bank commands.
        rageBankScriptBindings::Register_BankScript();

		// -- Register the commands that are unique to this sample.
		rageScriptBindings::Register_grcore(const_cast<grcSetup&>(*m_Setup));
		// -STOP

		const char *progNames[c_MaxScripts] = { "demo" };
		m_numScripts = 1;
		char nameBuffer[256];
		if (PARAM_script.Get())
		{
			m_numScripts = PARAM_script.GetArray(progNames,c_MaxScripts,nameBuffer,sizeof(nameBuffer));
		}
	
		// STEP #3. Create a thread
        // -- To load and debug a script in unmanaged mode, create the scrThread and a scrDebugger.
#if __BANK
#if UNMANAGED_DEBUGGER
        for (int i = 0; i < m_numScripts; ++i)
        {
            m_debuggers[i] = rage_new scrDebugger( progNames[i], scrThread::CreateThread(progNames[i]) );
            scrThread::LoadGlobalVariableInfo( progNames[i] );
        }
        // -- To load and debug a script in managed mode, create the scrThread and a scrDebugger, and register that debugger.
#else
		for (int i=0; i < m_numScripts; i++) 
        {
            scrDebugger::RegisterDebugger( rage_new scrDebugger(progNames[i],scrThread::CreateThread(progNames[i])) );
			scrThread::LoadGlobalVariableInfo( progNames[i] );
		}
#endif // UNMANAGED_DEBUGGER
		// -- To load a script when __BANK is disabled, just create the scrThread.
#else
		for (int i=0; i < m_numScripts; i++) 
        {
			scrThread::CreateThread( progNames[i] );
			scrThread::LoadGlobalVariableInfo( progNames[i] );
		}
#endif // __BANK
		// -STOP
	}

	void Update()
	{
        m_Setup->BeginUpdate();
		rage::INPUT.Update();
		m_Setup->EndUpdate();
	}

	void Draw()
	{
		m_Setup->BeginDraw();
		grcViewport *old = grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
		grcBindTexture(NULL);

		// STEP #4. Update the script threads
		// -- Call the scrThread update function.
		scrThread::UpdateAll(0);
		// -STOP

		// STEP #5. Update the script debuggers
		// -- In unmanaged mode, you must update each one individually
#if __BANK
#if UNMANAGED_DEBUGGER
        for (int i = 0; i < m_numScripts; ++i)
        {
            m_debuggers[i]->Update();
        }
		// -- In managed mode, you can update all of them with one function call.
#else
        scrDebugger::UpdateAll();
#endif // UNMANAGED_DEBUGGER
#endif // __BANK
		// -STOP

		grcViewport::SetCurrent(old);
		m_Setup->EndDraw();
	}

	void ShutdownClient()
	{
		// STEP #6. Delete the debuggers.
		// -- In unmanaged mode, you must delete the debuggers individually.
#if __BANK
#if UNMANAGED_DEBUGGER
		for (int i = 0; i < m_numScripts; ++i)
		{
			delete m_debuggers[i];
		}
		// -- In managed mode, you can tear down everything with 2 function calls.
#else
		scrDebugger::KillAllDebuggers();
		scrDebugger::ShutdownClass();
#endif // UNMANAGED_DEBUGGER
#endif // __BANK

		// STEP #7. Tear down the script engine.
		// -- Shutdown the scrThread and scrProgram classes
		scrThread::KillAllThreads();
		scrThread::ShutdownClass();
		scrProgram::ShutdownClass();
		// -STOP
	}

#if __BANK
    void BreakpointCallback( scrDebugger* /*pDebuggger*/, const char* pFilename, int lineNumber )
    {
        Displayf( "Break @ '%s' (line %d).", pFilename, lineNumber );
    }

    bool StoppedGameUpdateCallback()
    {
        if ( bkManager::IsEnabled() )
        {
            bkManager::GetInstance().Update();
            return true;
        }

        return false;
    }
#endif
};

int Main()
{
	scrGenericSample sample;

	sample.Init(RAGE_ASSET_ROOT "sample_scr_generic");
	sample.UpdateLoop();
	sample.Shutdown();

	return 0;
}
