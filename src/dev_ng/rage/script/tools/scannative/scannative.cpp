// How to build the x64 version of this -- 
//	1. Load rage/script/tools/scannative/scannative_2012.sln
//	2. Pick the RscRelease x64 configuration
//	3. Skip the compilation of service_pc.cpp
//		The easiest way to do that is by changing line 14 of service_pc.cpp from 
//		#if RSG_PC && !__TOOL
//		to 
//		#if RSG_PC && !__TOOL && 0
//	4. Change line 50 of diagerrorcodes.cpp from 
//		sysLanguage language = g_SysService.GetSystemLanguage();
//		to 
//		sysLanguage language = LANGUAGE_ENGLISH;
//	5. In thread.cpp around line 4385, skip compilation of this line 
//			extern "C" void ApiCheckPlugin_StartNativeHash();
//		and the entire scrThread::RegisterBuiltinCommands() function by wrapping them in #if 0
//	6. To avoid a crash when running, make the following change to hashstring.cpp line 312
//		#define INITNS(nameSpace, maxSize, hashFn, allocator, stringName, valueName) InitNamespace(nameSpace, maxSize, hashFn, (!__TOOL && !__GAMETOOL) ? allocator : NULL, stringName, valueName)
//		change it so that it always passes NULL for the allocator 
//		#define INITNS(nameSpace, maxSize, hashFn, allocator, stringName, valueName) InitNamespace(nameSpace, maxSize, hashFn, NULL, stringName, valueName)
//		I think this is required because the scannative tool doesn't set up the Master Allocator.
//		This change doesn't appear to cause any problems when running scannative
//	7. In program.cpp, change __RAGE_SCRIPT_COMPILER to 1. This disables some debug output.
//	8. Do a rebuild all
//	9. Run rage/script/tools/scannative/install_north.bat
//	10. Revert the changes you made in steps 3 to 7

#include <stdlib.h>
#include <set>

#include "file/device.h"
#include "file/stream.h"
#include "script/program.h"

void* operator new(size_t size,size_t /*align*/) { return ::operator new(size); }
void* operator new[](size_t size,size_t /*align*/) { return ::operator new[](size); }

using namespace rage;

// to process the codes file:
// sort < rawcodes.txt | uniq > finalcodes.txt

int main(int argc,char **argv)
{
	int returnCode = 0;
	std::set<u64> codes;

	if (argc != 3) {
		Errorf("usage: %s directory-containing-sco-files file-of-cpp-files-to-process",argv[0]);
		return 1;
	}

	fiStream *resp = fiStream::Open(argv[2]);
	if (!resp) {
		Errorf("Unable to open response file '%s'",argv[2]);
		return 1;
	}

	// Scan all .sco files in given directory
	const fiDevice &dev = fiDeviceLocal::GetInstance();
	fiFindData data;
	fiHandle fh = dev.FindFileBegin(argv[1],data);
	if (fh != fiHandleInvalid) {
		do {
			if (strstr(data.m_Name,".sco")) {
				char scoName[256];
				sprintf(scoName,"%s/%s",argv[1],data.m_Name);
				scrProgramId progId = scrProgram::Load(scoName);
				if (!progId) {
					Errorf("Unable to load '%s'",scoName);
					returnCode = 1;
				}
				else {
					scrProgram *prog = scrProgram::GetProgram(progId);
					u32 ns = prog->GetNativeSize();
					Displayf("Gathering %u codes from %s",ns,scoName);
					const u64 *n = prog->GetNatives();
					for (u32 i=0; i<ns; i++) {
						// Displayf("Gathering code %08x from %s",n[i],scoName);
						codes.insert(n[i]);
					}
					prog->Release();
				}
			}
		} while (dev.FindFileNext(fh,data));
		dev.FindFileEnd(fh);
	}
	else {
		Errorf("Unable to find any .sco files in %s",argv[1]);
		returnCode = 1;
	}

	printf("%u unique natives in all .sco files.\n",codes.size());
	if (returnCode)
		return returnCode;

	char filename[256];
	int added = 0, removed = 0;
	while (fgetline(filename,sizeof(filename),resp)) {
		fiStream *ff = fiStream::Open(filename);
		if (!ff) {
			Errorf("Unable to open file '%s'",filename);
			return 1;
		}

		size_t fs = ff->Size();
		char *filebuf = rage_new char[fs + 1];
		ff->Read(filebuf,fs);
		ff->Close();
		filebuf[fs] = '\0';
		// printf("read %u bytes from %s\n",fs,filename);

		char *sec = filebuf;
		bool touched = false;

		while ((sec = strstr(sec,"SCR_REGISTER_")) != NULL) {
			char *comma;
			if ((sec==filebuf||sec[-1]!='_') && (!memcmp(sec+13,"SECURE(",7) || !memcmp(sec+13,"UNUSED(",7)) && (comma = strchr(sec,',')) != NULL) {
				bool prevUnused = !memcmp(sec+13,"UNUSED(",7);
				while (comma[1]==' ' || comma[1]=='\t')
					++comma;
				u64 thiscode = 0;
				sscanf(comma+1,"0x%I64x",&thiscode);
				bool thisUnused = (codes.find(thiscode) == codes.end());
				// Displayf("%s: Checking for code %08x in table - %s",filename,thiscode,thisUnused?"UNUSED":"SECURE");
				if (prevUnused != thisUnused) {
					if (!touched) {
						char cmd[256];
						sprintf(cmd,"p4 edit %s",filename);
						system(cmd);
						touched = true;
					}
					memcpy(sec+13,thisUnused?"UNUSED":"SECURE",6);
					removed += thisUnused;
					added += !thisUnused;
				}
			}
			// Prevent previous string from matching.
			++sec;
		}
		if (touched) {
			ff = fiStream::Create(filename);
			if (!ff) {
				Errorf("Unable to recreate '%s'",filename);
				return 1;
			}
			ff->Write(filebuf,fs);
			ff->Close();
		}

		delete[] filebuf;
	}
	resp->Close();
	printf("%d removed, %d added\n",removed,added);
}
