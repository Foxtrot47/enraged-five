﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SanScriptLanguage")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Rockstar Games")]
[assembly: AssemblyProduct("SanScriptLanguage")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]   
[assembly: ComVisible(false)]     
[assembly: CLSCompliant(false)]
[assembly: NeutralResourcesLanguage("en-US")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: InternalsVisibleTo("SanScriptLanguage_IntegrationTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100ef195c4f3b5ab850a30b1fe17593956e8a2963697c3fb470e7fc00aea9d9e65e46201ddd0aa09090cc3570656b6ca98ef8bc66fdc552c3360218eb22f8dc4b1c18e7ff459789502cdabe267422a646d9041bf9b6ac4c67327643bef9708e67cb29874d5302f35e2b3366f5f856d3161ec04b31e94fa84ee96714ce3f5a0e57c6")]
[assembly: InternalsVisibleTo("SanScriptLanguage_UnitTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100ef195c4f3b5ab850a30b1fe17593956e8a2963697c3fb470e7fc00aea9d9e65e46201ddd0aa09090cc3570656b6ca98ef8bc66fdc552c3360218eb22f8dc4b1c18e7ff459789502cdabe267422a646d9041bf9b6ac4c67327643bef9708e67cb29874d5302f35e2b3366f5f856d3161ec04b31e94fa84ee96714ce3f5a0e57c6")]
