%using Microsoft.VisualStudio.TextManager.Interop
%namespace Babel.Parser
%valuetype LexValue
%partial

/* %expect 5 */


%union {
    public string str;
}


%{
	public int DefineLevel;

	List<String> RegisteredTypes = new List<String>() { "INT", "BOOL" };
    ErrorHandler handler = null;
    public void SetHandler(ErrorHandler hdlr) { handler = hdlr; }
    internal void CallHdlr(string msg, LexLocation val)
    {
		//Process this Lexer location.
		string[] splitByNewlines = yyval.str.Split('\n');
		int stringIndex = 0;
		foreach(string split in splitByNewlines)
		{
			if ( stringIndex == 0 )
			{
				handler.AddError(msg, val.sLin, val.sCol, split.Length);
			}
			else if ( stringIndex == splitByNewlines.Length - 1 )
			{
				handler.AddError(msg, val.eLin, 0, split.Length);
			}
			else
			{
				handler.AddError(msg, val.sLin + stringIndex, 0, split.Length);
			}

			stringIndex++;
		}

        //handler.AddError(msg, val, 1);
    }
    internal TextSpan MkTSpan(LexLocation s) { return TextSpan(s.sLin, s.sCol, s.eLin, s.eCol); }

    internal void Match(LexLocation lh, LexLocation rh) 
    {
        DefineMatch(MkTSpan(lh), MkTSpan(rh)); 
    }

	internal void ParseHeader(LexLocation lh, LexLocation rh)
	{
		//Call BabelLanguageService.ParseSource(string filename)
	}

	internal void RegisterSymbol(LexLocation loc, LexValue token)
	{
		RegisterName(MkTSpan(loc), token);
	}
%}

%token IF ELSE ELIF ENDIF WHILE ENDWHILE REPEAT ENDREPEAT SCRIPT ENDSCRIPT RETURN EXIT ENDPROC ENDFUNC
%token AND OR NOT GE LE NE LSH RSH FALLTHRU FOR FORWARD ENDFOR TO STEP TYPEDEF
%token PLUS_TIME MINUS_TIME STRLIT USING CATCH THROW CALL
%token TYPENAME FUNCNAME PROCNAME INTLIT FLOATLIT NEWSYM VAR_REF FIELD TRUE FALSE LABEL
%token SWITCH ENDSWITCH CASE DEFAULT BREAK GOTO SCOPE ENDSCOPE
%token CONST_INT CONST_FLOAT FUNC PROC NATIVE GLOBALS ENDGLOBALS ENUM ENDENUM ENUMLIT STRUCT ENDSTRUCT NL
%token PLUS MINUS PLUSPLUS MINUSMINUS PLUS_EQ MINUS_EQ TIMES_EQ DIVIDE_EQ COUNT_OF ENUM_TO_INT INT_TO_ENUM SIZE_OF
%token AND_EQ OR_EQ XOR_EQ
%token NATIVE_TO_INT INT_TO_NATIVE HASH DEBUGONLY VARARGS STRICT_ENUM TWEAK_INT TWEAK_FLOAT ANDALSO ORELSE

%token BROKENSTRLIT
%token COND_DEF_MACRO END_DEFINE UNKNOWN_MACRO MACRO_DEFINE
%token COLON QUESTIONMARK
%token LEX_WHITE LEX_COMMENT LEX_ERROR LEX_DISABLED
%token maxParseToken
%%
Program
	: optScriptBlocks 
	;

optScriptBlocks
	: optScriptBlock optScriptBlocks
	| optScriptBlock
	;

optScriptBlock
	: optDeclList SCRIPT optStruct optStmtList ENDSCRIPT endDeclList
	;
	
optStruct
	: '(' TYPENAME NEWSYM ')'
	| /* empty */ 
	;
	
optDeclList
	: declList
	| /* empty */
	;
	
declList
	: declList decl
	| decl
	;

debugFunc
	: DEBUGONLY FUNC	
	| FUNC				
	;
	
debugProc
	: DEBUGONLY PROC	
	| PROC				
	;

endDeclList
	: endDeclList endDecl
	| endDecl
	;

endDecl
	: EndIfStatement
	| /* empty */
	;
	
decl
	: UsingStatement 
	| IfDefStatement
	| EndIfStatement
	| TYPENAME 
	| GLOBALS
	| ENDGLOBALS
	| FORWARD enumOrStrictEnum NEWSYM
	| FORWARD enumOrStrictEnum TYPENAME 
	| enumOrStrictEnum NEWSYM enumerantList ENDENUM
	| enumOrStrictEnum TYPENAME enumerantList ENDENUM
	| STRUCT NEWSYM	structMemberList ENDSTRUCT { RegisterSymbol(@2, $2); }
	| STRUCT TYPENAME
	| NATIVE debugFunc TYPENAME NEWSYM optFormalOrVarargsList
	| NATIVE debugFunc TYPENAME FUNCNAME optFormalOrVarargsList
	| NATIVE debugProc NEWSYM optFormalOrVarargsList
	| NATIVE debugProc PROCNAME optFormalOrVarargsList
	| NATIVE NEWSYM ':' TYPENAME
	| NATIVE NEWSYM
	| NATIVE TYPENAME
	| NATIVE TYPENAME ':' TYPENAME
	| debugFunc TYPENAME NEWSYM optFormalList optStmtList ENDFUNC
	| debugFunc TYPENAME FUNCNAME optFormalList optStmtList ENDFUNC
	| debugProc NEWSYM optFormalList optStmtList ENDPROC
	| debugProc PROCNAME optFormalList optStmtList ENDPROC
	| CONST_INT NEWSYM expr
	| TWEAK_INT NEWSYM expr
	| TYPEDEF FUNC TYPENAME NEWSYM optFormalList
	| TYPEDEF PROC NEWSYM optFormalList
	| CONST_INT INTLIT
		/*{
			scr_error("Duplicate CONST_INT name");
		}
		error*/
	| CONST_FLOAT NEWSYM floatexpr
	| TWEAK_FLOAT NEWSYM floatexpr
	| CONST_FLOAT floatexpr
		/*{
			scr_error("Duplicate CONST_FLOAT name");
		}
		error*/
	| GOTO FALSE
	| GLOBALS TRUE
	| NATIVE_TO_INT FALSE
	| INT_TO_NATIVE FALSE
	;

UsingStatement
	: USING STRLIT		{ ParseHeader(@1, @2); } 
	| USING BROKENSTRLIT { CallHdlr("Invalid filename.", @2); }
	;

IfDefStatement
	: COND_DEF_MACRO MACRO_DEFINE { DefineLevel++; }
	| UNKNOWN_MACRO error { CallHdlr("Unknown macro definition.", @1); }
	;

EndIfStatement
	: END_DEFINE 
		{ 
			if ( DefineLevel == 0 ) 
				CallHdlr("Unmatched END_DEFINE statement.", @1); 

			DefineLevel--; 
		}
	;

enumOrStrictEnum
	: ENUM 
	| STRICT_ENUM 
	;
	
stmt
	: UsingStatement
	| SCOPE scopedStmtList ENDSCOPE					
	| lvalue '=' expr								
	| lvalue PLUS_EQ expr							
	| lvalue MINUS_EQ expr							
	| lvalue TIMES_EQ expr							
	| lvalue DIVIDE_EQ expr							
	| lvalue AND_EQ expr							
	| lvalue OR_EQ expr								
	| lvalue XOR_EQ expr							
	| lvalue PLUSPLUS								
	| PLUSPLUS lvalue								
	| lvalue MINUSMINUS								
	| MINUSMINUS lvalue	
	| IF expr scopedStmtList optElses ENDIF
	| WHILE expr scopedStmtList ENDWHILE
	| REPEAT expr lvalue scopedStmtList ENDREPEAT	
	| FOR lvalue '=' expr TO expr optStep scopedStmtList ENDFOR		
	| SWITCH expr scopedStmtList ENDSWITCH
	| CASE intexpr
	| CASE ENUMLIT
	| BREAK 
	| FALLTHRU
	| DEFAULT
	| PROCNAME argExprList
	| CALL rvalue argExprList						
	| FUNCNAME argExprList
	| TYPENAME lvarDeclList NL
	| CONST_INT NEWSYM expr
	| CONST_FLOAT NEWSYM floatexpr
	/* We now have separate statements for EXIT and RETURN to avoid a parsing ambiguity that allows
		us to eliminate the requirement for the SET command */
	| EXIT					
	| RETURN expr
	| NEWSYM ':'
	| LABEL ':'
	| GOTO NEWSYM
	| GOTO LABEL
	| THROW
	| THROW '(' expr ')'
	;
	
intexpr
	: INTLIT		
	| '-' INTLIT	
	;
	
floatexpr
	: FLOATLIT		
	| '-' FLOATLIT	
	;

/* boolexpr
	: TRUE			
	| FALSE			
	; */
	
optFormalList
	: '(' formalList ')'	
	| '(' ')'			
	| /* empty */
	;

optFormalOrVarargsList
	: optFormalList		
	| '(' VARARGS ')'
	;
	
formalList
	: formal ',' formalList
	| formal			
	;
	
formal
	: TYPENAME '&' NEWSYM foptArrays
	| TYPENAME NEWSYM optArraysOrDefaultValue
	| ENUM_TO_INT NEWSYM optArraysOrDefaultValue
	| ENUM_TO_INT '&' NEWSYM foptArrays
	| STRUCT '&' NEWSYM
	;
	
optStep
	: STEP intexpr		
	| /* empty */
	;
	
enumerantList
	: enumerant
	| enumerantList ',' enumerant
	;
	
enumerant
	: NEWSYM 
	| NEWSYM '=' enumExpr
	;
	
structMemberList
	: structMember
	| structMemberList structMember
	;
	
structMember
	: TYPENAME svarDeclList
	;
	
svarDeclList
	: svarDeclList ',' svarDecl
	| svarDecl
	;
	
svarDecl
	: NEWSYM optArraysOrDefaultValue
	;
	
enumExpr
	: ENUMLIT					
	| INTLIT					
	| '-' INTLIT				
	| enumExpr '+' INTLIT		
	| HASH '(' STRLIT ')'		
	;
	
lvarDeclList
	: lvarDeclList ',' lvarDecl		
	| lvarDecl					
	;

gvarDeclList
	: gvarDeclList ',' gvarDecl
	| gvarDecl
	;

lvarDecl
	: NEWSYM optArrays optAssignment			
	| VAR_REF optArrays optAssignment
	;
	
optAssignment
	: '=' expr
	| /* empty */	
	;
	
gvarDecl
	: NEWSYM optArrays optGlobalOrStaticAssignment
	;

foptArrays
	: '[' expr ']' optArrays
	| '[' ']'
	|	/* */
	;
	
optArraysOrDefaultValue
	: optArrays		
	| '=' enumExpr	
	| '=' floatexpr	
	| '=' TRUE		
	| '=' FALSE		
	| '=' VAR_REF	
	;
	
optArrays
	: '[' expr ']' optArrays
	|	'[' ']'
	| /* empty */
	;
	
optGlobalOrStaticAssignment
	: '=' expr	
	| /* empty */
	;
	
optElses
	: ELSE scopedStmtList					
	| ELIF expr scopedStmtList optElses		
	| /* empty */
	;

scopedStmtList
	: optStmtList
	;
		  
optStmtList
	: /* empty */ 
	| stmtList	
	;
	
stmtList
	: stmtList stmt		
	| stmt				
	;

primaryExpr
	: '(' expr ')'						
	| LSH expr ',' expr ',' expr RSH	
	| rvalue							
	| INTLIT							
	| FLOATLIT							
	| STRLIT							
	| ENUMLIT							
	| TRUE								
	| FALSE								
	| FUNCNAME argExprList				
	| CALL rvalue argExprList			
	| PROCNAME argExprList				
	| '&' FUNCNAME						
	| '&' PROCNAME						
	| CATCH								
	| ENUM_TO_INT '(' expr ')'
	| NATIVE_TO_INT '(' expr ')'
	| COUNT_OF '(' arrayReference ')'	
	| COUNT_OF '(' TYPENAME ')'	
	| SIZE_OF '(' TYPENAME ')'	
	| SIZE_OF '(' rvalue ')'	
	| INT_TO_ENUM '(' TYPENAME ',' expr ')'
	| INT_TO_NATIVE '(' TYPENAME ',' expr ')'
	| HASH '(' STRLIT ')'
	;
	
argExprList
	: '(' exprList ')'
	| '(' ')'		
	;
	
exprList
	: expr ',' exprList	
	| expr				
	;

lvalue
	: VAR_REF
	| NEWSYM
	| lvalue '.' FIELD 
	| lvalue '[' expr ']'
	;

rvalue
	: VAR_REF
	| rvalue '.' FIELD 
	| rvalue '[' expr ']'
	| NEWSYM
	;

arrayReference
	: VAR_REF							
	| NEWSYM				
	| arrayReference '.'  FIELD 
	| arrayReference '[' ']'
	;



unaryExpr
	: primaryExpr						
	| NOT unaryExpr						
	| '-' unaryExpr						
	;

multExpr
	: unaryExpr							
	| multExpr '*' unaryExpr			
	| multExpr '/' unaryExpr			
	| multExpr '%' unaryExpr			
	;

addExpr
	: multExpr							
	| addExpr '+' multExpr				
	| addExpr '-' multExpr				
	| addExpr PLUS_TIME multExpr		
	| addExpr MINUS_TIME multExpr		
	;

relExpr
	: addExpr							
	| relExpr '<' addExpr				
	| relExpr LE addExpr				
	| relExpr '>' addExpr				
	| relExpr GE addExpr				
	;

equalityExpr
	: relExpr							
	| equalityExpr '=' relExpr			
	| equalityExpr NE relExpr			
	;

bitAndExpr
	: equalityExpr						
	| bitAndExpr '&' equalityExpr		
	;
	
bitXorExpr
	: bitAndExpr						
	| bitXorExpr '^' bitAndExpr			
	;

bitOrExpr
	: bitXorExpr						
	| bitOrExpr '|' bitXorExpr			
	;
	
logAndExpr
	: bitOrExpr							
	| logAndExpr AND bitOrExpr			
	| logAndExpr ANDALSO bitOrExpr	
	;

logOrExpr
	: logAndExpr					
	| logOrExpr OR logAndExpr		
	| logOrExpr ORELSE logAndExpr	
	;

conditionalExpr
	: logOrExpr									
	// | logOrExpr QUESTIONMARK expr COLON conditionalExpr	
	;
	
expr
	: conditionalExpr					
	;
