%using Babel;
%using Babel.Parser;

%namespace Babel.Lexer


%x COMMENT

%{

		List<string> DefinitionMacros = new List<string>();
		List<string> RegisteredTypes = new List<string>() { "INT", "BOOL" };

		Tokens LastToken;
		Tokens GetDefaultType()
		{
			if ( LastToken == Tokens.COND_DEF_MACRO )
				return Tokens.MACRO_DEFINE;

			return Tokens.NEWSYM;
		}
         int GetIdToken(string txt)
         {
			Tokens thisToken = GetDefaultType();
			char character = txt[0].ToString().ToLower()[0];

			foreach(string registeredType in RegisteredTypes)
			{
				if ( txt.Equals(registeredType, StringComparison.CurrentCultureIgnoreCase))
				{
					LastToken = thisToken;
					return (int) Tokens.TYPENAME;
				}
			}

            switch (character)
            {
				case '+':
					if (txt.Equals("+@", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.PLUS_TIME;
					}
					else if (txt.Equals("++", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.PLUSPLUS;
					}
					else if (txt.Equals("+=", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.PLUS_EQ;
					}
					else if (txt.Equals("+", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.PLUS;
					}
					break;
				case '-':
					if (txt.Equals("-@", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.MINUS_TIME;
					}
					else if (txt.Equals("--", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.MINUSMINUS;
					}
					else if (txt.Equals("-=", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken =  Tokens.MINUS_EQ;
					}
					else if (txt.Equals("-", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.MINUS;
					}
					break;
				case 'a':
					if (txt.Equals("and", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.AND;
					}
					else if (txt.Equals("andalso", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.ANDALSO;
					}
					break;
				case 'e':
					if ( txt.Equals("ENDSCRIPT") )
					{
						thisToken = Tokens.ENDSCRIPT;
					}
					else if ( txt.Equals("ENDSTRUCT") )
					{
						thisToken = Tokens.ENDSTRUCT;
					}
					break;
				case 'g':
					if (txt.Equals("ge", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.GE;
					}
					break;
				case 'l':
					if (txt.Equals("le", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.LE;
					}
					break;
				case 'n':
					if (txt.Equals("ne", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.NE;
					}
					break;
				case 'o':
					if (txt.Equals("or", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.OR;
					}
					else if (txt.Equals("orelse", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.ORELSE;
					}
					break;
				case 's':
					if ( txt.Equals("SCRIPT") )
					{
						thisToken = Tokens.SCRIPT;
					}
					else if (txt.Equals("STRUCT") )
					{
						thisToken = Tokens.STRUCT;
					}
					break;
				case 'u':
					if (txt.Equals("using", StringComparison.CurrentCultureIgnoreCase)) 
					{
						thisToken = Tokens.USING;
					}
					break;
				case '"':
					if ( txt.EndsWith("\"") == true )
						thisToken = Tokens.STRLIT;
					else
						thisToken = Tokens.BROKENSTRLIT; 

					break;
				case '#':
					if ( txt.Equals("#IFDEF") || txt.Equals("#IFNDEF"))
					{
						thisToken = Tokens.COND_DEF_MACRO;
					}
					else if ( txt.Equals("#ENDIF") )
					{
						thisToken = Tokens.END_DEFINE;
					}
					else
					{
						thisToken = Tokens.UNKNOWN_MACRO;
					}	
					break;
                default: 
                    break;
            }

			LastToken = thisToken;
			return (int) thisToken;
       }
       
       internal void LoadYylval()
       {
           yylval.str = yytext;
		   yylloc = new LexLocation(tokLin, tokCol, tokELin, tokECol);
       }

	   public override bool yyignore(int token)
	   {
			if ( token == (int) Tokens.LEX_COMMENT )
				return true;

			return false;
	   }
       
       public override void yyerror(string s, params object[] a)
       {
			if (handler != null) 
			{
				handler.AddError(s, tokLin, tokCol, tokELin, tokECol);       
			}
	   }
%}

White0          [ \t\r\f\v]
White           {White0}|\n

Quote \"
BlockCommentStart    \/\*
BlockCommentEnd      \*\/
LineComment			\/\/
ABStar       [^\*\n]*


%%
[White0]						{ return (int) Tokens.LEX_WHITE; }
[\"][a-zA-Z0-9._\r\t]*[\"|\n]		{ return GetIdToken(yytext); }
[_a-zA-Z][_a-zA-Z0-9]*			{ return GetIdToken(yytext); }
#[a-zA-Z0-9]+					{ return GetIdToken(yytext); }
:								{ return (int) Tokens.COLON; }
?								{ return (int) Tokens.QUESTIONMARK; }

{BlockCommentStart}{ABStar}\**{BlockCommentEnd} { return (int)Tokens.LEX_COMMENT; } 
{BlockCommentStart}{ABStar}\**					{ BEGIN(COMMENT); return (int)Tokens.LEX_COMMENT; }
{LineComment}.*\n							{ return (int)Tokens.LEX_COMMENT; }
<COMMENT>\n                     |
<COMMENT>{ABStar}\**							{ return (int)Tokens.LEX_COMMENT; }
<COMMENT>{ABStar}\**{BlockCommentEnd}			{ BEGIN(INITIAL); return (int)Tokens.LEX_COMMENT; }

%{
							LoadYylval();
%}

%%

/* .... */
