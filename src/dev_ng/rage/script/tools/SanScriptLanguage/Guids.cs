﻿// Guids.cs
// MUST match guids.h
using System;

namespace RockstarGames.SanScriptLanguage
{
    static class GuidList
    {
        public const string guidSanScriptLanguagePkgString = "a1590380-706b-4273-9987-ac1f1c1ce234";
        public const string guidSanScriptLanguageCmdSetString = "1dc323c0-a948-4b3a-92b8-77f00d1302dc";

        public static readonly Guid guidSanScriptLanguageCmdSet = new Guid(guidSanScriptLanguageCmdSetString);
    };
}