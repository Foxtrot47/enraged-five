using System;
using System.Runtime.InteropServices;
namespace Babel
{
    [Microsoft.VisualStudio.Shell.ProvideService(typeof(Babel.LanguageService))]
    [Microsoft.VisualStudio.Shell.ProvideLanguageExtension(typeof(Babel.LanguageService), Configuration.Extension)]
    //For full detail on using the language service options, see: 
    // http://207.46.16.248/en-us/library/microsoft.visualstudio.shell.providelanguageserviceattribute(VS.90).aspx
    [Microsoft.VisualStudio.Shell.ProvideLanguageService(typeof(Babel.LanguageService), Configuration.Name, 0,
        CodeSense = true, //General Intellisense Support
        EnableCommenting = true,
        QuickInfo = true,
        RequestStockColors = false, //Custom colorable items
        EnableAsyncCompletion = true, //Supports background parsing
        MatchBraces = true, //Match braces on command
        MatchBracesAtCaret = true, //Match braces while typing
        ShowMatchingBrace = true
        )]
    //generate a new GUID
    [Guid("00000000-0000-0000-0000-000000000000")]
    class Package : BabelPackage
    {
    }
}
