using System;
using System.Runtime.InteropServices;

namespace Babel
{
    //generate a new GUID
    [Guid("00000000-0000-0000-0000-000000000000")]
    class LanguageService : BabelLanguageService
    {
        public override string GetFormatFilterList()
        {
            return "SanScript Files (*.sc)\n*.sch";
        }
    }
}
