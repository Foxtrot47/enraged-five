using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.Package;

namespace Babel
{
    public class Resolver : Babel.IASTResolver
    {
        #region IASTResolver Members
        public IList<Babel.Declaration> FindCompletions(object result, int line, int col)
        {
            return new List<Babel.Declaration>();
        }

        public IList<Babel.Declaration> FindMembers(object result, int line, int col)
        {
            ParseRequest request = (ParseRequest)result;

            List<Babel.Declaration> members = new List<Babel.Declaration>();

            members.Add(new Babel.Declaration("Test", "Display Text", 0, "Member Name"));

            return members;
        }

        public string FindQuickInfo(object result, int line, int col)
        {
            return null;
        }

        public IList<Babel.Method> FindMethods(object result, int line, int col, string name)
        {
            return new List<Babel.Method>();
        }
        #endregion
    }
}
