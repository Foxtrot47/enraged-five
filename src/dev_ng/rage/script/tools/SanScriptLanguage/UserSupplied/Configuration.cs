using System;
using Microsoft.VisualStudio.Package;
using Babel.Parser;
using Microsoft.VisualStudio.TextManager.Interop;

namespace Babel
{
    public static partial class Configuration
    {
        public const string Name = "SanScript";
        public const string Extension = ".sc";

        static CommentInfo commentInfo;
        public static CommentInfo MyCommentInfo { get { return commentInfo; } }

        static Configuration()
        {
            commentInfo.BlockEnd = "*/";
            commentInfo.BlockStart = "/*";
            commentInfo.LineStart = "//";
            commentInfo.UseLineComments = true;

            // default colors - currently, these need to be declared
            CreateColor("Keyword", COLORINDEX.CI_BLUE, COLORINDEX.CI_USERTEXT_BK);
            CreateColor("Comment", COLORINDEX.CI_DARKGREEN, COLORINDEX.CI_USERTEXT_BK);
            CreateColor("Identifier", COLORINDEX.CI_SYSPLAINTEXT_FG, COLORINDEX.CI_USERTEXT_BK);
            CreateColor("String", COLORINDEX.CI_PURPLE, COLORINDEX.CI_USERTEXT_BK);
            CreateColor("Number", COLORINDEX.CI_MAGENTA, COLORINDEX.CI_USERTEXT_BK);
            CreateColor("Text", COLORINDEX.CI_SYSPLAINTEXT_FG, COLORINDEX.CI_USERTEXT_BK);
            TokenColor error = CreateColor("Error", COLORINDEX.CI_RED, COLORINDEX.CI_USERTEXT_BK, false, true);

            //
            // map tokens to color classes
            //
            ColorToken((int)Tokens.USING, TokenType.Keyword, TokenColor.Keyword, TokenTriggers.None);
            ColorToken((int)Tokens.STRLIT, TokenType.Literal, TokenColor.String, TokenTriggers.None);
            ColorToken((int)Tokens.BROKENSTRLIT, TokenType.Literal, TokenColor.String, TokenTriggers.None);

            ColorToken((int)Tokens.COND_DEF_MACRO, TokenType.Keyword, TokenColor.Keyword, TokenTriggers.None);
            ColorToken((int)Tokens.END_DEFINE, TokenType.Keyword, TokenColor.Keyword, TokenTriggers.None);
            ColorToken((int)Tokens.UNKNOWN_MACRO, TokenType.Keyword, error, TokenTriggers.None);
            ColorToken((int)Tokens.MACRO_DEFINE, TokenType.Identifier, TokenColor.Text, TokenTriggers.None);

            ColorToken((int)Tokens.SCRIPT, TokenType.Keyword, TokenColor.Keyword, TokenTriggers.None);
            ColorToken((int)Tokens.ENDSCRIPT, TokenType.Keyword, TokenColor.Keyword, TokenTriggers.None);

            ColorToken((int)Tokens.STRUCT, TokenType.Keyword, TokenColor.Keyword, TokenTriggers.None);
            ColorToken((int)Tokens.ENDSTRUCT, TokenType.Keyword, TokenColor.Keyword, TokenTriggers.None);

            ColorToken((int)Tokens.TYPENAME, TokenType.Identifier, TokenColor.Keyword, TokenTriggers.None);
            ColorToken((int)Tokens.NEWSYM, TokenType.Text, TokenColor.Text, TokenTriggers.MemberSelect);

            ColorToken((int)'(', TokenType.Delimiter, TokenColor.Text, TokenTriggers.MatchBraces);
            ColorToken((int)')', TokenType.Delimiter, TokenColor.Text, TokenTriggers.MatchBraces);
            ColorToken((int)'{', TokenType.Delimiter, TokenColor.Text, TokenTriggers.MatchBraces);
            ColorToken((int)'}', TokenType.Delimiter, TokenColor.Text, TokenTriggers.MatchBraces);

            //// Extra token values internal to the scanner
            ColorToken((int)Tokens.LEX_ERROR, TokenType.Text, error, TokenTriggers.None);
            ColorToken((int)Tokens.LEX_COMMENT, TokenType.Text, TokenColor.Comment, TokenTriggers.None);

        }
    }
}
