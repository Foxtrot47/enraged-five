﻿//---------------------------------------------------------------------------------------------
// <copyright file="ParseCoordinator.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace SanScriptParser
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using ActiproSoftware.SyntaxEditor;
    using ragScriptEditorShared;
    using RSG.Base.Extensions;

    /// <summary>
    /// Coordinates the loading of parse caches for a project and the queuing of files for parsing.
    /// Acts as an intermediary for the Actipro parser service, integrating the cache with the parser service.
    /// </summary>
    public class ParseCoordinator : ISemanticParseDataTarget
    {
        /// <summary>
        /// Stats structure for displaying info about a snapshot of the current state of the coordinator.
        /// </summary>
        public struct Stats
        {
            /// <summary>
            /// Gets the total number of requests, pending and already processed
            /// </summary>
            public int TotalNumFiles { get; }

            /// <summary>
            /// Gets the number of requests that are already processed.
            /// </summary>
            public int NumProcessedFiles { get; }

            /// <summary>
            /// Gets the number of files that still need to be processed.
            /// </summary>
            public int NumPendingFiles { get; }

            /// <summary>
            /// Construct a new <see cref="Stats"/> object.
            /// </summary>
            /// <param name="totalNumFiles">The total number of files.</param>
            /// <param name="numProcessedFiles">The number of processed files.</param>
            /// <param name="numPendingFiles">The number of pending files.</param>
            public Stats(int totalNumFiles, int numProcessedFiles, int numPendingFiles)
            {
                this.TotalNumFiles = totalNumFiles;
                this.NumProcessedFiles = numProcessedFiles;
                this.NumPendingFiles = numPendingFiles;
            }
        }

        private const string LogCtx = nameof(ParseCoordinator);
        private const int MaxConcurrentOpenedFiles = 16;

        private readonly SanScriptIdentifierCache _identifierCache;

        /// <summary>
        /// The full collection of filenames that have been given to the parse coordinator to handle in this batch.
        /// </summary>
        private readonly ISet<string> _allRequestedFiles = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// The full collection of filenames that have finished processing in this batch.
        /// </summary>
        private readonly ISet<string> _allProcessedFiles = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Queue of parse requests waiting to be loaded from the disk cache (stage 1)
        /// </summary>
        private readonly List<string> _requestedFilesToLoadFromCache = new List<string>();

        /// <summary>
        /// Dictionary of currently-queued semantic requests. (stage 3)
        /// </summary>
        private readonly IDictionary<string, SemanticParserServiceRequest> _semRequests = new Dictionary<string, SemanticParserServiceRequest>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Queue of parse requests waiting to be read into memory (stage 2)
        /// </summary>
        private readonly List<string> _requestedFilesToRead = new List<string>();

        /// <summary>
        /// Number of semantic requests that are currently queued. This is a separate variable from _semRequests because it is incremented before
        /// items are added to _semRequests. The item is incremented, the file is read from disk, then it is inserted into the dictionary.
        /// This way, we never go over 16 files in memory/loading at once despite being multithreaded.
        /// </summary>
        private int _pendingSemanticRequestCount = 0;

        private int _numWorkers = 0;

        /// <summary>
        /// Synchronization object for all collection operations. This ensures that all collection transactions and requests are atomic
        /// (rather than using lockless collections and risking intermediary states). This is fine because the locked code never performs slow operations
        /// (like I/O)
        /// </summary>
        private readonly object _requestLockObj = new object();

        /// <summary>
        /// Construct a new parse coordinator with a provided identifier cache.
        /// </summary>
        /// <param name="identifierCache"></param>
        public ParseCoordinator(SanScriptIdentifierCache identifierCache)
        {
            this._identifierCache = identifierCache;
        }

        /// <summary>
        /// Gets a snapshot of statistics for the coordinator.
        /// </summary>
        public Stats Statistics
        {
            get
            {
                lock (this._requestLockObj)
                {
                    return new Stats(
                        this._allRequestedFiles.Count,
                        this._allProcessedFiles.Count,
                        this._requestedFilesToLoadFromCache.Count + this._requestedFilesToRead.Count + this._pendingSemanticRequestCount);

                }
            }
        }

        /// <summary>
        /// Given a parent path, queue up more files for loading identifiers.
        /// If the parent path is no longer registered with the coordinator, this sub-request will be ignored.
        /// Otherwise, this will attempt to load identifiers from the disk cache, if one exists for the files. A full parse will be queued
        /// if no disk cache exists.
        /// </summary>
        /// <param name="parentPath">The previously-requested path that refers to the cache files.</param>
        /// <param name="filenames">List of filenames to add identifiers for.</param>
        internal void RequestIdentifiersForFiles(string parentPath, string[] filenames)
        {
            if (string.IsNullOrEmpty(parentPath) || filenames == null || filenames.None())
            {
                return;
            }

            lock (this._requestLockObj)
            {
                if (!this._allRequestedFiles.Contains(parentPath))
                {
                    // Either parent path was never requested, or the path was deleted during the parse.
                    // Ignore its children in this case.
                    return;
                }

                this.RequestIdentifiersForFiles(filenames);
            }
        }

        /// <summary>
        /// Queue up a set of filenames for loading identifiers. This will first attempt to load identifiers from the disk cache, if one exists for the files.
        /// Otherwise a full parse will be queued. 
        /// </summary>
        /// <param name="filenames">List of filenames to add identifiers for.</param>
        internal void RequestIdentifiersForFiles(params string[] filenames)
        {
            if (filenames == null || filenames.None())
            {
                return;
            }

            lock (this._requestLockObj)
            {
                // Get files not yet registered.
                IEnumerable<string> newFilenames = filenames
                    .Where(f => !string.IsNullOrWhiteSpace(f) && !this._allRequestedFiles.Contains(f))
                    .ToList();

                if (newFilenames.None())
                {
                    return;
                }

                if (this._numWorkers <= 0 && this._pendingSemanticRequestCount <= 0)
                {
                    // If we had no work until now, reset the batch of work.
                    this.ResetBatch();
                }

                // Add files to list for deserializing from cache.
                this._requestedFilesToLoadFromCache.AddRange(newFilenames);
                this._allRequestedFiles.AddRange(newFilenames);

                foreach (string filename in newFilenames)
                {
                    ApplicationSettings.Log.DebugCtx(LogCtx, "Add identifier request for {0}", filename);
                }

                // Start workers if necessary.
                this.StartWorkers(newFilenames.Count());
            }
        }

        /// <summary>
        /// Remove a set of files from the cache entirely, including pending requests.
        /// </summary>
        /// <param name="filenames">The list of filenames to remove.</param>
        public void RemoveFilesFromCache(params string[] filenames)
        {
            if (filenames == null || filenames.None())
            {
                return;
            }

            // First clear any requests, then clear the files from the cache.
            this.ClearParseRequests(filenames);
            this._identifierCache.RemoveFiles(filenames);
        }

        /// <summary>
        /// Remove all requested files from the queue. This does not remove them from the identifier cache.
        /// </summary>
        public void ClearAllParseRequests()
        {
            lock (this._requestLockObj)
            {
                this.ClearParseRequests(this._allRequestedFiles.ToArray());
            }
        }

        /// <summary>
        /// Removes the provided files from the queue. This does not remove them from the identifier cache.
        /// </summary>
        /// <param name="filenames">List of filenames to remove from the queue.</param>
        public void ClearParseRequests(params string[] filenames)
        {
            lock (this._requestLockObj)
            {
                foreach (string filename in filenames)
                {
                    if (string.IsNullOrWhiteSpace(filename))
                    {
                        continue;
                    }

                    ApplicationSettings.Log.DebugCtx(LogCtx, "Removing {0} from cache.", filename);

                    this._allProcessedFiles.Remove(filename);
                    this._allRequestedFiles.Remove(filename);
                    this._requestedFilesToLoadFromCache.Remove(filename);
                    this._requestedFilesToRead.Remove(filename);

                    SemanticParserServiceRequest semRequest;
                    if (!this._semRequests.TryGetValue(filename, out semRequest))
                    {
                        continue;
                    }

                    ApplicationSettings.Log.DebugCtx(LogCtx, "Removing pending request for {0}", filename);
                    this._semRequests.Remove(filename);
                    SemanticParserService.RemovePendingRequests(semRequest.ParseHashKey);
                    this._pendingSemanticRequestCount--;
                }
            }
        }

        /// <inheritdoc />
        public void NotifySemanticParseComplete(SemanticParserServiceRequest request)
        {
            lock (this._requestLockObj)
            {
                if (this._allRequestedFiles.Contains(request.Filename))
                {
                    // Only add to completed queue if this request wasn't deleted during processing.
                    this._allProcessedFiles.Add(request.Filename);
                }

                this._semRequests.Remove(request.Filename);
                this._pendingSemanticRequestCount--;
                ApplicationSettings.Log.DebugCtx(LogCtx, "Semantic parse completed for {0}", request.Filename);

                // A semantic parse slot is now opened up, add a worker to handle this, if we are below the limit
                this.StartWorkers(MaxConcurrentOpenedFiles - this._pendingSemanticRequestCount);
            }
        }

        /// <inheritdoc />
        public string Guid { get; } = System.Guid.NewGuid().ToString();

        
        /// <summary>
        /// Reset the numbers on requested files, as well as the unique list of processed files.
        /// Call this when a chunk of parsing completes to clear the slate.
        /// </summary>
        private void ResetBatch()
        {
            lock (this._requestLockObj)
            {
                this._allProcessedFiles.Clear();
                this._allRequestedFiles.Clear();
            }
        }

        /// <summary>
        /// Start enough workers to handle some work. The number of actual workers created will never exceed the limit.
        /// </summary>
        /// <param name="num">Number of workers that could ideally be created. This will be clamped to the actual value.</param>
        private void StartWorkers(int num)
        {
            lock (this._requestLockObj)
            {
                int workersToCreate = Math.Min(Environment.ProcessorCount - 2, num) - this._numWorkers;
                if (workersToCreate <= 0)
                {
                    return;
                }

                for (int i = 0; i < workersToCreate; i++)
                {
                    Task.Factory.StartNew(this.Process);
                }

                this._numWorkers += workersToCreate;
            }
        }

        /// <summary>
        /// Processing loop for a single worker.
        /// </summary>
        private void Process()
        {
            bool didWork;
            do
            {
                didWork = false;

                string fileToLoadFromCache;
                lock (this._requestLockObj)
                {
                    // Try to grab the next item in the queue for loading from cache.
                    fileToLoadFromCache = this._requestedFilesToLoadFromCache.FirstOrDefault();
                    if (fileToLoadFromCache != null)
                    {
                        this._requestedFilesToLoadFromCache.RemoveAt(0);
                    }
                }

                if (fileToLoadFromCache != null)
                {
                    this.LoadFileFromCache(fileToLoadFromCache);
                    didWork = true;
                }

                string fileToRead;
                lock (this._requestLockObj)
                {
                    // Check if we are at max files open, and bail if so. OK if this worker terminates because didWork==false,
                    // because completed parses will spin up a new worker.
                    if (this._pendingSemanticRequestCount >= MaxConcurrentOpenedFiles)
                    {
                        continue;
                    }

                    // Grab the next file to read for semantic parsing from the front of the queue.
                    fileToRead = this._requestedFilesToRead.FirstOrDefault();
                    if (fileToRead != null)
                    {
                        this._requestedFilesToRead.RemoveAt(0);
                        this._pendingSemanticRequestCount++;
                    }
                }

                if (fileToRead != null)
                {
                    this.AddParseRequest(fileToRead);
                    didWork = true;
                }
            } while (didWork);

            lock (this._requestLockObj)
            {
                this._numWorkers--;
            }
        }

        /// <summary>
        /// Load a single file from the cache. If it fails, add the file to the semantic parsing queue.
        /// </summary>
        /// <param name="filename">The filename to load.</param>
        private void LoadFileFromCache(string filename)
        {
            bool deserialized = this.TryDeserializeCacheFile(filename);
            lock (this._requestLockObj)
            {
                // Already scheduled to read from disk, let it happen.
                if (this._requestedFilesToRead.Contains(filename))
                {
                    return;
                }

                if (deserialized)
                {
                    // Deserialized, finish parse request.
                    this._allProcessedFiles.Add(filename);
                    ApplicationSettings.Log.DebugCtx(LogCtx, "Deserialized from cache successfully: {0}", filename);
                }
                else
                {
                    // Not deserialized, queue up for reading into memory.
                    this._requestedFilesToRead.Add(filename);
                    ApplicationSettings.Log.DebugCtx(LogCtx, "Did not deserialize {0}, adding to request queue", filename);
                }
            }
        }

        /// <summary>
        /// Attempt to deserialize an identifier cache file and add it to the cache. This is a slow operation and is done in a worker thread.
        /// </summary>
        /// <param name="filename">The filename to attempt to deserialize.</param>
        /// <returns>Whether the deserialize succeeded</returns>
        private bool TryDeserializeCacheFile(string filename)
        {
            int filenameHashCode = SanScriptIdentifierCache.GetFilenameHashCode(filename);
            string cacheFilename = SanScriptIdentifierCache.GetCacheOutputFileName(filename);

            DateTime diskCacheDateTime;
            try
            {
                if (!File.Exists(cacheFilename))
                {
                    // No cache exists
                    return false;
                }

                diskCacheDateTime = File.GetLastWriteTime(cacheFilename);
                DateTime srcDateTime = File.GetLastWriteTime(filename);
                if (diskCacheDateTime < srcDateTime)
                {
                    // The source code is newer than the cache
                    return false;
                }
            }
            catch (Exception ex)
            {
                ApplicationSettings.Log.ToolExceptionCtx(LogCtx, ex, "File deserialize failed on I/O operations due to an exception for {0}", filename);
                return false;
            }

            DateTime lastCacheUpdateTime = this._identifierCache.GetLastUpdateTime(filenameHashCode);
            if (diskCacheDateTime < lastCacheUpdateTime)
            {
                // The deserialization already happened and is up to date.
                return true;
            }

            if (!this._identifierCache.DeserializeFile(filename))
            {
                // Deserialize failed
                return false;
            }

            // Request this file's usings for the identifier cache as well.
            string currentDir = Path.GetDirectoryName(filename);
            IEnumerable<string> usings = this._identifierCache.GetUsings(filenameHashCode);
            string[] usingFilenames = usings.Select(u => SanScriptProjectResolver.ParserPlugin.GetFullPathName(u, currentDir)).ToArray();
            this.RequestIdentifiersForFiles(filename, usingFilenames);

            return true;
        }

        /// <summary>
        /// Open this filename and add a semantic parse request to the semantic parser service.
        /// This work should be done in a worker thread due to I/O
        /// </summary>
        /// <param name="filename">The filename to add a parse request for.</param>
        private void AddParseRequest(string filename)
        {
            if (!SemanticParserService.IsRunning)
            {
                return;
            }

            try
            {
                if (!File.Exists(filename))
                {
                    ApplicationSettings.Log.Warning("File '{0}', requested to parse, does not exist. Skipping parse.", filename);
                    lock (this._requestLockObj)
                    {
                        this._pendingSemanticRequestCount--;
                        return;
                    }
                }

                string fileContents;
                using (StreamReader reader = File.OpenText(filename))
                {
                    // url:bugstar:1689219 - This is needed because the Syntax Editor accounts for \r\n line breaks as if it was two characters, not one.
                    // As a result, this would cause the "Go To Definition" functionality to navigate to an incorrect offset of the file.
                    // viv: Would save a lot of CPU if there was a way to fix this.
                    fileContents = reader.ReadToEnd().Replace("\r\n", "\n");
                }

                // Check again in case this value was changed in another thread
                if (!SemanticParserService.IsRunning)
                {
                    return;
                }

                filename = filename.ToLower();
                SemanticParserServiceRequest semRequest = new SemanticParserServiceRequest(
                    SemanticParserServiceRequest.LowPriority,
                    filename,
                    fileContents,
                    new TextRange(0, fileContents.Length),
                    SemanticParseFlags.None,
                    SanScriptProjectResolver.Language,
                    this);

                lock (this._requestLockObj)
                {
                    this._semRequests.Add(filename, semRequest);
                }

                ApplicationSettings.Log.DebugCtx(LogCtx, "Added semantic parse request for {0}", filename);
                SemanticParserService.Parse(semRequest);
            }
            catch (Exception ex)
            {
                ApplicationSettings.Log.ToolExceptionCtx(LogCtx, ex, "Failed to add parse request for {0}", filename);

                lock (this._requestLockObj)
                {
                    this._pendingSemanticRequestCount--;
                }
            }
        }
    }
}
