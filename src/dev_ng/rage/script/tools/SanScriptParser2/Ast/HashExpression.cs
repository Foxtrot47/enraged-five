using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a HASH expression.
	/// </summary>
	public partial class HashExpression : SanScriptParser.Expression {

		/// <summary>
		/// Gets the context ID for the string constant to find the hash of.
		/// </summary>
		public const byte ArgumentContextID = SanScriptParser.Expression.ExpressionContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte HashExpressionContextIDBase = SanScriptParser.Expression.ExpressionContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>HashExpression</c> class.
		/// </summary>
		/// <param name="arg">The native string to find the hash of</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public HashExpression( Expression arg, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.Argument = arg;
		}

		/// <summary>
		/// Initializes a new instance of the <c>HashExpression</c> class. 
		/// </summary>
		public HashExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>HashExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public HashExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.Keyword;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.HashExpression;
			}
		}

		/// <summary>
		/// Gets or sets the string constant to find the hash of.
		/// </summary>
		/// <value>The string constant to find the hash of.</value>
		public Expression Argument {
			get {
				return this.GetChildNode(HashExpression.ArgumentContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, HashExpression.ArgumentContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "HASH" );
			
			Expression arg = this.Argument;
			if ( arg != null )
				{
				sb.Append( "( " );
				arg.BuildDisplayText(sb);
				sb.Append( " )" );
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>HASH</c>" );
			
			Expression arg = this.Argument;
			if ( arg != null )
				{
				sb.Append( "<c=text>(</c>" );
				arg.BuildMarkupText(sb);
				sb.Append( "<c=text> )</c>" );
			}
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to see the expected parameters.
		/// </summary>
		/// <value>Text for the node that can be used for displaying a function declaration.</value>
		public static string DefinitionText
		{
			get
			{
				StringBuilder text = new StringBuilder();
				
				text.Append( "INT HASH( str )\n\n" );
				text.Append( "/// PURPOSE: Gets the hash of the string constant\n" );
				text.Append( "/// PARAMS: \n" );
				text.Append( "///    str - What to get the hash of.\n" );
				text.Append( "/// RETURNS: An integer." );
				
				return text.ToString();
			}
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to see a QuickInfo popup with the expected parameters
		/// </summary>
		/// <value>Text for the node that can be used for the IntelliPrompt function declaration.</value>
		public static string DefinitionMarkupText
		{
			get
			{
				string[] args = HashExpression.DefinitionArgumentsMarkupText;
				StringBuilder text = new StringBuilder();
				
				text.Append( "<c=reserved>INT HASH</c>" );
				text.Append( "<c=text>( </c>" );
				text.Append( args[0] );
				text.Append( "<c=text> )</c><br/><br/>" );
				
				text.Append( "<c=commentdelim>//</c><c=comment>/ PURPOSE: Gets the hash of the string constant.</c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/ PARAMS: </c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/&nbsp;&nbsp;&nbsp;&nbsp;str - What to get the hash of.</c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/ RETURNS: An integer.</c>" );
				
				return text.ToString();
			}
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to retrieve the individual arguments for use in a QuickInfo popup
		/// </summary>
		/// <value>Markup text for each argument.</value>
		public static string[] DefinitionArgumentsMarkupText
		{
			get
			{
				string[] args = new string[1];
				
				StringBuilder text = new StringBuilder();
				text.Append( "<c=text>str - What to get the hash of.</c>" );
				args[0] = text.ToString();
				
				return args;
			}
		}

	}

}
