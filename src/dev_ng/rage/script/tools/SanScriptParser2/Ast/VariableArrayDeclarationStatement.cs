using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for an array variable declaration.
	/// </summary>
	public partial class VariableArrayDeclarationStatement : SanScriptParser.VariableDeclarationStatement {

		/// <summary>
		/// Gets the context ID for the expression defining the size of the array.
		/// </summary>
		public const byte ArraySizeExpressionContextID = SanScriptParser.VariableDeclarationStatement.VariableDeclarationStatementContextIDBase;

		/// <summary>
		/// Gets the context ID for the expression defining the size of the array for the next dimension in a multi-dimensional array.
		/// </summary>
		public const byte MultiDimensionArraySizeExpressionContextID = SanScriptParser.VariableDeclarationStatement.VariableDeclarationStatementContextIDBase + 1;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte VariableArrayDeclarationStatementContextIDBase = SanScriptParser.VariableDeclarationStatement.VariableDeclarationStatementContextIDBase + 2;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>VariableArrayDeclarationStatement</c> class.
		/// </summary>
		/// <param name="sizeExpr">The <see cref="Expression"/> that initializes the size of the array.</param>
		/// <param name="nextSizeExpr">The <see cref="Expression"/> that initializes the size of the array's next dimension.</param>
		/// <param name="name">The <see cref="Identifier"/> name of the variable.</param>
		/// <param name="type">The <see cref="Identifier"/> name of data type.</param>
		/// <param name="isRef"><c>true</c> if this declaration is a reference.</param>
		/// <param name="isSubDeclaration"><c>true</c> if this declaration occurs after the first declaration in a comma-separated list.</param>
		/// <param name="subDeclaration">A <see cref="Statement"/> declared on the same line, separated by commas.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public VariableArrayDeclarationStatement( Expression sizeExpr, Expression nextSizeExpr, Identifier name, Identifier type, bool isRef, bool isSubDeclaration,
		Statement subDeclaration, TextRange textRange )
			: base( name, type, isRef, isSubDeclaration, subDeclaration, textRange )
			{
			// Initialize parameters
			this.ArraySizeExpression = sizeExpr;
			this.MultiDimensionArraySizeExpression = nextSizeExpr;
		}

		/// <summary>
		/// Initializes a new instance of the <c>VariableArrayDeclarationStatement</c> class. 
		/// </summary>
		public VariableArrayDeclarationStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>VariableArrayDeclarationStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public VariableArrayDeclarationStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.VariableArrayDeclarationStatement;
			}
		}

		/// <summary>
		/// Gets or sets the expression defining the size of the array.
		/// </summary>
		/// <value>The expression defining the size of the array.</value>
		public Expression ArraySizeExpression {
			get {
				return this.GetChildNode(VariableArrayDeclarationStatement.ArraySizeExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, VariableArrayDeclarationStatement.ArraySizeExpressionContextID);
			}
		}

		/// <summary>
		/// Gets or sets the expression defining the size of the array for the next dimension in a multi-dimensional array.
		/// </summary>
		/// <value>The expression defining the size of the array for the next dimension in a multi-dimensional array.</value>
		public Expression MultiDimensionArraySizeExpression {
			get {
				return this.GetChildNode(VariableArrayDeclarationStatement.MultiDimensionArraySizeExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, VariableArrayDeclarationStatement.MultiDimensionArraySizeExpressionContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			base.BuildDisplayText(sb);
			
			Expression size = this.ArraySizeExpression;
			if ( size != null )
				{
				if ( size.NodeType == SanScriptNodeType.ArrayAccessExpression )
					{
					size.BuildDisplayText(sb);
				}
				else
					{
					sb.Append( "[" );
					size.BuildDisplayText(sb);
					sb.Append( "]" );
				}
			}
			
			Expression nextSize = this.MultiDimensionArraySizeExpression;
			if ( nextSize != null )
				{
				if ( nextSize.NodeType == SanScriptNodeType.ArrayAccessExpression )
					{
					nextSize.BuildDisplayText(sb);
				}
				else
					{
					sb.Append( "[" );
					nextSize.BuildDisplayText(sb);
					sb.Append( "]" );
				}
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			base.BuildMarkupText(sb);
			
			Expression size = this.ArraySizeExpression;
			if ( size != null )
				{
				if ( size.NodeType == SanScriptNodeType.ArrayAccessExpression )
					{
					size.BuildMarkupText(sb);
				}
				else
					{
					sb.Append( "<c=text>[</c>" );
					size.BuildMarkupText(sb);
					sb.Append( "<c=text>]</c>" );
				}
			}
			
			Expression nextSize = this.MultiDimensionArraySizeExpression;
			if ( nextSize != null )
				{
				if ( nextSize.NodeType == SanScriptNodeType.ArrayAccessExpression )
					{
					nextSize.BuildMarkupText(sb);
				}
				else
					{
					sb.Append( "<c=text>[</c>" );
					nextSize.BuildMarkupText(sb);
					sb.Append( "<c=text>]</c>" );
				}
			}
		}

	}

}
