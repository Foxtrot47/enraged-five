using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents the base class for a typedef declaration.
	/// </summary>
	public abstract partial class TypedefSubroutineDeclarationStatement : SanScriptParser.IdentifierStatement {

		/// <summary>
		/// Gets the context ID for the collection of parameters, if any.
		/// </summary>
		public const byte ArgumentContextID = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte TypedefSubroutineDeclarationStatementContextIDBase = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>TypedefSubroutineDeclarationStatement</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Identifier"/> of the subroutine.</param>
		/// <param name="args">The <see cref="AstNodeList"/> of arguments to pass in.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public TypedefSubroutineDeclarationStatement( Identifier name, AstNodeList args, TextRange textRange )
			: base( name, textRange )
			{
			// Initialize parameters
			this.Arguments.AddRange( args.ToArray() );
			
			if ( name != null )
				{
				name.DeclareType = Identifier.DeclarationType.Typedef;
				name.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicClass;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <c>TypedefSubroutineDeclarationStatement</c> class. 
		/// </summary>
		public TypedefSubroutineDeclarationStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>TypedefSubroutineDeclarationStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public TypedefSubroutineDeclarationStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicClass;
			}
		}

		/// <summary>
		/// Gets the collection of parameters, if any.
		/// </summary>
		/// <value>The collection of statements.</value>
		public IAstNodeList Arguments {
			get {
				return new AstNodeListWrapper(this, TypedefSubroutineDeclarationStatement.ArgumentContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			this.Name?.BuildDisplayText(sb);
			
			sb.Append( "(" );
			
			IAstNodeList args = this.Arguments;
			if ( (args != null) && (args.Count > 0) )
				{
				sb.Append( " " );
				for ( int i = 0; i < args.Count; ++i )
					{
					if ( i > 0 )
						{
						sb.Append( ", " );
					}
					
					((AstNode)args[i]).BuildDisplayText(sb);
				}
				sb.Append( " " );
			}
			
			sb.Append( ")" );
			
			string purpose = this.PurposeDisplayText;
			if ( purpose != null )
				{
				sb.Append( "\n\n" );
				sb.Append( purpose );
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			this.Name?.BuildMarkupText(sb);
			
			sb.Append( "<c=text>(</c>" );
			
			string[] args = this.ArgumentsMarkupText;
			if ( (args != null) && (args.Length > 0) )
				{
				sb.Append( " " );
				for ( int i = 0; i < args.Length; ++i )
					{
					if ( i > 0 )
						{
						sb.Append( "<c=text>, </c>" );
					}
					
					sb.Append( args[i] );
				}
				sb.Append( " " );
			}
			
			sb.Append( "<c=text>)</c>" );
			
			string purpose = this.PurposeMarkupText;
			if ( purpose != null )
				{
				sb.Append( "<br/><br/>" );
				sb.Append( purpose );
			}
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to retrieve the individual arguments for use in a QuickInfo popup
		/// </summary>
		/// <value>Markup text for each argument.</value>
		public string[] ArgumentsMarkupText
		{
			get
			{
				string[] args = null;
				
				IAstNodeList argList = this.Arguments;
				if ( (argList != null) && (argList.Count > 0) )
					{
					args = new string[argList.Count];
					for ( int i = 0; i < argList.Count; ++i )
						{
						args[i] = ((AstNode)argList[i]).MarkupText;
					}
				}
				
				return args;
			}
		}

	}

}
