using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents an identifier.
	/// </summary>
	public partial class Identifier : SanScriptParser.AstNode {

		private System.String	text;
		private DeclarationType	declareType;
		private System.Int32	imageIndexOverride;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte IdentifierContextIDBase = SanScriptParser.AstNode.AstNodeContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>Identifier</c> class.
		/// </summary>
		/// <param name="text">The identifier name.</param>
		/// <param name="declareType">The type of identifier.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public Identifier( string text, DeclarationType declareType, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.Text = text;
			this.DeclareType = declareType;
			this.ImageIndexOverride = -1;
		}

		/// <summary>
		/// Initializes a new instance of the <c>Identifier</c> class. 
		/// </summary>
		public Identifier() {}

		/// <summary>
		/// Initializes a new instance of the <c>Identifier</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public Identifier(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.Identifier;
			}
		}

		/// <summary>
		/// Gets or sets the text of the qualified identifier.
		/// </summary>
		/// <value>The text of the qualified identifier.</value>
		public System.String Text {
			get {
				return text;
			}
			set {
				text = value;
			}
		}

		/// <summary>
		/// Gets or sets local, Native or User.
		/// </summary>
		/// <value>Local, Native or User.</value>
		public DeclarationType DeclareType {
			get {
				return declareType;
			}
			set {
				declareType = value;
			}
		}

		/// <summary>
		/// Gets or sets if not -1, overrides the default ImageIndex.
		/// </summary>
		/// <value>If not -1, overrides the default ImageIndex.</value>
		public System.Int32 ImageIndexOverride {
			get {
				return imageIndexOverride;
			}
			set {
				imageIndexOverride = value;
			}
		}

		public enum DeclarationType
		{
			Local,
			User,
			Native,
			Typedef,
			NativeType,	// built-in keyword
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex
		{
			get
			{
				if ( this.ImageIndexOverride != -1 )
					{
					return this.ImageIndexOverride;
				}
				else
					{
					return base.ImageIndex;
				}
			}
		}
		
		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append(this.Text);
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			switch ( this.DeclareType )
				{
				case DeclarationType.Local:
				sb.Append( "<c=local>" );
				break;
				case DeclarationType.Typedef:
				case DeclarationType.User:
				sb.Append( "<c=user>" );
				break;
				case DeclarationType.Native:
				sb.Append( "<c=native>" );
				break;
				case DeclarationType.NativeType:
				sb.Append( "<c=nativetype>" );
				break;
			}
			
			sb.Append( IntelliPrompt.EscapeMarkupText( this.Text ) );
			sb.Append( "</c>" );
		}

	}

}
