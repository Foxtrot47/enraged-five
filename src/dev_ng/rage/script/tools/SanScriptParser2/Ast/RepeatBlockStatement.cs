using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a REPEAT ENDPREAT block.
	/// </summary>
	public partial class RepeatBlockStatement : SanScriptParser.BlockStatement {

		/// <summary>
		/// Gets the context ID for the number of times to repeat.  Can only be an integer or an integer identifier.
		/// </summary>
		public const byte CountContextID = SanScriptParser.BlockStatement.BlockStatementContextIDBase;

		/// <summary>
		/// Gets the context ID for the variable/expression that gets incremented.
		/// </summary>
		public const byte VariableContextID = SanScriptParser.BlockStatement.BlockStatementContextIDBase + 1;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte RepeatBlockStatementContextIDBase = SanScriptParser.BlockStatement.BlockStatementContextIDBase + 2;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>RepeatBlockStatement</c> class.
		/// </summary>
		/// <param name="count">The <see cref="Expression"/> for the number of iterations.</param>
		/// <param name="variable">The variable name <see cref="Expression"/> for the iterator.</param>
		/// <param name="statements">The <see cref="AstNodeList"/> of statements contained in this block.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public RepeatBlockStatement( Expression count, Expression variable, AstNodeList statements, TextRange textRange )
			: base( statements, textRange )
			{
			// Initialize parameters
			this.Count = count;
			this.Variable = variable;
		}

		/// <summary>
		/// Initializes a new instance of the <c>RepeatBlockStatement</c> class. 
		/// </summary>
		public RepeatBlockStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>RepeatBlockStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public RepeatBlockStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.RepeatBlockStatement;
			}
		}

		/// <summary>
		/// Gets or sets the number of times to repeat.  Can only be an integer or an integer identifier.
		/// </summary>
		/// <value>The number of times to repeat.  Can only be an integer or an integer identifier.</value>
		public Expression Count {
			get {
				return this.GetChildNode(RepeatBlockStatement.CountContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, RepeatBlockStatement.CountContextID);
			}
		}

		/// <summary>
		/// Gets or sets the variable/expression that gets incremented.
		/// </summary>
		/// <value>The variable/expression that gets incremented.</value>
		public Expression Variable {
			get {
				return this.GetChildNode(RepeatBlockStatement.VariableContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, RepeatBlockStatement.VariableContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "REPEAT" );
			
			Expression count = this.Count;
			if ( count != null )
				{
				sb.Append( " " );
				count.BuildDisplayText(sb);
			}
			
			Expression variable = this.Variable;
			if ( variable != null )
				{
				sb.Append( " " );
				variable.BuildDisplayText(sb);
			}
		}
		
		/// <summary>
		/// Gets the character offset at which to navigate when the editor's caret should jump to the text representation of the AST node.
		/// </summary>
		/// <value>The character offset at which to navigate when the editor's caret should jump to the text representation of the AST node.</value>
		public override int NavigationOffset
		{
			get
			{
				Expression count = this.Count;
				if ( (count != null) && count.HasStartOffset )
					{
					return count.NavigationOffset;
				}
				else
					{
					return base.NavigationOffset;
				}
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>REPEAT</c>" );
			
			Expression count = this.Count;
			if ( count != null )
				{
				sb.Append( " " );
				count.BuildMarkupText(sb);
			}
			
			Expression variable = this.Variable;
			if ( variable != null )
				{
				sb.Append( " " );
				variable.BuildMarkupText(sb);
			}
		}

	}

}
