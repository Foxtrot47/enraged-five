using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a FORWARD ENUM declaration.
	/// </summary>
	public partial class ForwardEnumDeclarationStatement : SanScriptParser.IdentifierStatement {

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte ForwardEnumDeclarationStatementContextIDBase = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>ForwardEnumDeclarationStatement</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Identifier"/> name of the enumeration type.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ForwardEnumDeclarationStatement( Identifier name, TextRange textRange )
			: base( name, textRange )
			{
			// Initialize parameters
			if ( name != null )
				{
				name.DeclareType = Identifier.DeclarationType.User;
				name.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicEnumeration;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <c>ForwardEnumDeclarationStatement</c> class. 
		/// </summary>
		public ForwardEnumDeclarationStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>ForwardEnumDeclarationStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ForwardEnumDeclarationStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicEnumeration;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.ForwardEnumDeclarationStatement;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "FORWARD ENUM" );
			
			Identifier name = this.Name;
			if ( name != null )
				{
				sb.Append( " " );
				name.BuildDisplayText(sb);
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>FORWARD ENUM</c>" );
			
			Identifier name = this.Name;
			if ( name != null )
				{
				sb.Append( " " );
				name.BuildMarkupText(sb);
			}
		}

	}

}
