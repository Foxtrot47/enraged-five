using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a GLOBALS ENDGLOBALS block.
	/// </summary>
	public partial class GlobalsDefinitionBlockStatement : SanScriptParser.BlockStatement {

		/// <summary>
		/// Gets the context ID for the name of the global.
		/// </summary>
		public const byte NameContextID = SanScriptParser.BlockStatement.BlockStatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte GlobalsDefinitionBlockStatementContextIDBase = SanScriptParser.BlockStatement.BlockStatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>GlobalsDefinitionBlockStatement</c> class.
		/// </summary>
		/// <param name="name">The global block's <see cref="Identifier"/>.</param>
		/// <param name="statements">The <see cref="AstNodeList"/> of statements contained in this block.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public GlobalsDefinitionBlockStatement(Identifier name, AstNodeList statements, TextRange textRange )
			: base( statements, textRange )
			{
			this.Name = name;
		}

		/// <summary>
		/// Initializes a new instance of the <c>GlobalsDefinitionBlockStatement</c> class. 
		/// </summary>
		public GlobalsDefinitionBlockStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>GlobalsDefinitionBlockStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public GlobalsDefinitionBlockStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.GlobalsDefinitionBlockStatement;
			}
		}

		/// <summary>
		/// Gets or sets the name of the global.
		/// </summary>
		/// <value>The name of the global.</value>
		public Identifier Name {
			get {
				return this.GetChildNode(GlobalsDefinitionBlockStatement.NameContextID) as Identifier;
			}
			set {
				this.ChildNodes.Replace(value, GlobalsDefinitionBlockStatement.NameContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append("GLOBALS ");
			this.Name?.BuildDisplayText(sb);
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append("<c=reserved>GLOBALS ");
			this.Name?.BuildMarkupText(sb);
			sb.Append("</c>");
		}

	}

}
