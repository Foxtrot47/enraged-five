using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a INTTONATIVE expression.
	/// </summary>
	public partial class IntToNativeExpression : SanScriptParser.Expression {

		/// <summary>
		/// Gets the context ID for the name of the NATIVE type to convert to.
		/// </summary>
		public const byte NativeTypeContextID = SanScriptParser.Expression.ExpressionContextIDBase;

		/// <summary>
		/// Gets the context ID for the integer to convert.
		/// </summary>
		public const byte ExpressionToConvertContextID = SanScriptParser.Expression.ExpressionContextIDBase + 1;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte IntToNativeExpressionContextIDBase = SanScriptParser.Expression.ExpressionContextIDBase + 2;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>IntToNativeExpression</c> class.
		/// </summary>
		/// <param name="nativeType">The native type <see cref="Identifier"/> passed to the subroutine.</param>
		/// <param name="expr">The <see cref="Expression"/> passed to to the subroutine that is to be converted.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public IntToNativeExpression( Identifier nativeType, Expression expr, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.NativeType = nativeType;
			this.ExpressionToConvert = expr;
		}

		/// <summary>
		/// Initializes a new instance of the <c>IntToNativeExpression</c> class. 
		/// </summary>
		public IntToNativeExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>IntToNativeExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public IntToNativeExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.Keyword;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.IntToNativeExpression;
			}
		}

		/// <summary>
		/// Gets or sets the name of the NATIVE type to convert to.
		/// </summary>
		/// <value>The name of the NATIVE type to convert to.</value>
		public Identifier NativeType {
			get {
				return this.GetChildNode(IntToNativeExpression.NativeTypeContextID) as Identifier;
			}
			set {
				this.ChildNodes.Replace(value, IntToNativeExpression.NativeTypeContextID);
			}
		}

		/// <summary>
		/// Gets or sets the integer to convert.
		/// </summary>
		/// <value>The integer to convert.</value>
		public Expression ExpressionToConvert {
			get {
				return this.GetChildNode(IntToNativeExpression.ExpressionToConvertContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, IntToNativeExpression.ExpressionToConvertContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "INT_TO_NATIVE" );
			
			Identifier nativeType = this.NativeType;
			Expression expr = this.ExpressionToConvert;
			if ( (nativeType != null) && (expr != null) )
				{
				sb.Append( "( " );
				nativeType.BuildDisplayText(sb);
				sb.Append( ", " );
				expr.BuildDisplayText(sb);
				sb.Append( " )" );
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>INT_TO_NATIVE</c>" );
			
			Identifier nativeType = this.NativeType;
			Expression expr = this.ExpressionToConvert;
			if ( (nativeType != null) && (expr != null) )
				{
				sb.Append( "<c=text>(</c>" );
				nativeType.BuildMarkupText(sb);
				sb.Append( "<c=text>, </c>" );
				expr.BuildMarkupText(sb);
				sb.Append( "<c=text> )</c>" );
			}
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to see the expected parameters.
		/// </summary>
		/// <value>Text for the node that can be used for displaying a function declaration.</value>
		public static string DefinitionText
		{
			get
			{
				StringBuilder text = new StringBuilder();
				
				text.Append( "<nativeTypeName> INT_TO_ENUM( NATIVE nativeTypeName, INT value )\n\n" );
				text.Append( "/// PURPOSE: Converts an integer to its nativeTypeName-equivalent.\n" );
				text.Append( "/// PARAMS: \n" );
				text.Append( "///    nativeTypeName - The NATIVE type to convert to.\n" );
				text.Append( "///    value - The integer to convert.\n" );
				text.Append( "/// RETURNS: A value of type nativeTypeName." );
				
				return text.ToString();
			}
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to see a QuickInfo popup with the expected parameters
		/// </summary>
		/// <value>Text for the node that can be used for the IntelliPrompt function declaration.</value>
		public static string DefinitionMarkupText
		{
			get
			{
				string[] args = IntToNativeExpression.DefinitionArgumentsMarkupText;
				StringBuilder text = new StringBuilder();
				
				text.Append( "<c=native>&lt;nativeTypeName&gt; </c>" );
				text.Append( "<c=reserved>INT_TO_NATIVE</c>" );
				text.Append( "<c=text>( </c>" );
				text.Append( args[0] );
				text.Append( "<c=text>, </c>" );
				text.Append( args[1] );
				text.Append( "<c=text> )</c><br/><br/>" );
				
				text.Append( "<c=commentdelim>//</c><c=comment>/ PURPOSE: Converts an integer to its nativeTypeName-equivalent.</c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/ PARAMS: </c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/&nbsp;&nbsp;&nbsp;&nbsp;nativeTypeName - The NATIVE type to convert to.</c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/&nbsp;&nbsp;&nbsp;&nbsp;value - The integer to convert.</c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/ RETURNS: A value of type nativeTypeName.</c>" );
				
				return text.ToString();
			}
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to retrieve the individual arguments for use in a QuickInfo popup
		/// </summary>
		/// <value>Markup text for each argument.</value>
		public static string[] DefinitionArgumentsMarkupText
		{
			get
			{
				string[] args = new string[2];
				
				StringBuilder text = new StringBuilder();
				text.Append( "<c=reserved>NATIVE </c>" );
				text.Append( "<c=native>nativeTypeName</c>" );
				args[0] = text.ToString();
				
				text = new StringBuilder();
				text.Append( "<c=reserved>INT </c>" );
				text.Append( "<c=text>value</c>" );
				args[1] = text.ToString();
				
				return args;
			}
		}

	}

}
