using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents the base class for a variable declaration.
	/// </summary>
	public abstract partial class VariableDeclarationStatement : SanScriptParser.IdentifierStatement {

		private bool	isReference;
		private bool	isSubDeclaration;
		private System.Int32	imageIndexOverride;

		/// <summary>
		/// Gets the context ID for the data type.
		/// </summary>
		public const byte TypeContextID = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase;

		/// <summary>
		/// Gets the context ID for additional declaration declared on the same line, separated by commas.
		/// </summary>
		public const byte SubDeclarationContextID = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase + 1;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte VariableDeclarationStatementContextIDBase = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase + 2;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>VariableDeclarationStatement</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Identifier"/> name of the variable.</param>
		/// <param name="type">The <see cref="Identifier"/> name of data type.</param>
		/// <param name="isRef"><c>true</c> if this declaration is a reference.</param>
		/// <param name="isSubDeclaration"><c>true</c> if this declaration occurs after the first declaration in a comma-separated list.</param>
		/// <param name="subDeclaration">A <see cref="Statement"/> declared on the same line, separated by commas.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public VariableDeclarationStatement( Identifier name, Identifier type, bool isRef, bool isSubDeclaration, Statement subDeclaration, TextRange textRange )
			: base( name, textRange )
			{
			// Initialize parameters
			this.Type = type;
			this.IsReference = isRef;
			this.IsSubDeclaration = isSubDeclaration;
			this.SubDeclaration = subDeclaration;
		}

		/// <summary>
		/// Initializes a new instance of the <c>VariableDeclarationStatement</c> class. 
		/// </summary>
		public VariableDeclarationStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>VariableDeclarationStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public VariableDeclarationStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Gets or sets the data type.
		/// </summary>
		/// <value>The data type.</value>
		public Identifier Type {
			get {
				return this.GetChildNode(VariableDeclarationStatement.TypeContextID) as Identifier;
			}
			set {
				this.ChildNodes.Replace(value, VariableDeclarationStatement.TypeContextID);
			}
		}

		/// <summary>
		/// Gets or sets if the declaration uses an ampersand.  Only valid for subroutine argument lists.
		/// </summary>
		/// <value>If the declaration uses an ampersand.  Only valid for subroutine argument lists.</value>
		public bool IsReference {
			get {
				return isReference;
			}
			set {
				isReference = value;
			}
		}

		/// <summary>
		/// Gets or sets if the declaration occurs after the first declaration in a comma-separated list.
		/// </summary>
		/// <value>If the declaration occurs after the first declaration in a comma-separated list.</value>
		public bool IsSubDeclaration {
			get {
				return isSubDeclaration;
			}
			set {
				isSubDeclaration = value;
			}
		}

		/// <summary>
		/// Gets or sets additional declaration declared on the same line, separated by commas.
		/// </summary>
		/// <value>Additional declaration declared on the same line, separated by commas.</value>
		public Statement SubDeclaration {
			get {
				return this.GetChildNode(VariableDeclarationStatement.SubDeclarationContextID) as Statement;
			}
			set {
				this.ChildNodes.Replace(value, VariableDeclarationStatement.SubDeclarationContextID);
			}
		}

		/// <summary>
		/// Gets or sets if not -1, overrides the default ImageIndex.
		/// </summary>
		/// <value>If not -1, overrides the default ImageIndex.</value>
		public System.Int32 ImageIndexOverride {
			get {
				return imageIndexOverride;
			}
			set {
				imageIndexOverride = value;
			}
		}

		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex
		{
			get
			{
				if ( this.ImageIndexOverride != -1 )
					{
					return this.ImageIndexOverride;
				}
				else
					{
					return base.ImageIndex;
				}
			}
		}
		
		internal override void BuildDisplayText(StringBuilder sb)
			{
			Identifier type = this.Type;
			if ( type != null )
				{
				type.BuildDisplayText(sb);
				
				if ( this.IsReference )
					{
					sb.Append( "&" );
				}
			}
			
			Identifier name = this.Name;
			if ( name != null )
				{
				sb.Append( " " );
				name.BuildDisplayText(sb);
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			Identifier type = this.Type;
			if ( type != null )
				{
				type.BuildMarkupText(sb);
				
				if ( this.IsReference )
					{
					sb.Append( "&amp;" );
				}
			}
			
			Identifier name = this.Name;
			if ( name != null )
				{
				sb.Append( " " );
				name.BuildMarkupText(sb);
			}
		}

	}

}
