using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a return statement.
	/// </summary>
	public partial class ReturnStatement : SanScriptParser.Statement {

		/// <summary>
		/// Gets the context ID for the expression to be returned, if any.
		/// </summary>
		public const byte ReturnExpressionContextID = SanScriptParser.Statement.StatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte ReturnStatementContextIDBase = SanScriptParser.Statement.StatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>ReturnStatement</c> class.
		/// </summary>
		/// <param name="expr">The <see cref="Expression"/> that gets returned.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ReturnStatement( Expression expr, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.ReturnExpression = expr;
		}

		/// <summary>
		/// Initializes a new instance of the <c>ReturnStatement</c> class. 
		/// </summary>
		public ReturnStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>ReturnStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ReturnStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.Keyword;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.ReturnStatement;
			}
		}

		/// <summary>
		/// Gets or sets the expression to be returned, if any.
		/// </summary>
		/// <value>The expression to be returned, if any.</value>
		public Expression ReturnExpression {
			get {
				return this.GetChildNode(ReturnStatement.ReturnExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, ReturnStatement.ReturnExpressionContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "RETURN" );
			
			Expression rtn = this.ReturnExpression;
			if ( rtn != null )
				{
				sb.Append( " " );
				rtn.BuildDisplayText(sb);
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>RETURN</c>" );
			
			Expression rtn = this.ReturnExpression;
			if ( rtn != null )
				{
				sb.Append( " " );
				rtn.BuildMarkupText(sb);
			}
		}

	}

}
