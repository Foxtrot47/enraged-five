using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for TIMESTEP expression.
	/// </summary>
	public partial class TimestepExpression : SanScriptParser.Expression {

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte TimestepExpressionContextIDBase = SanScriptParser.Expression.ExpressionContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>TimestepExpression</c> class. 
		/// </summary>
		public TimestepExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>TimestepExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public TimestepExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.Keyword;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.TimestepExpression;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append("TIMESTEP()");
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append("<c=reserved>TIMESTEP</c><c=text>()</c>");
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to see the expected parameters.
		/// </summary>
		/// <value>Text for the node that can be used for displaying a function declaration.</value>
		public static string DefinitionText
		{
			get
			{
				StringBuilder text = new StringBuilder();
				
				text.Append( "FLOAT TIMESTEP()\n\n" );
				text.Append( "/// PURPOSE: Gets the last time step (typically one or two vertical blanking intervals).\n" );
				text.Append( "/// RETURNS: A float." );
				
				return text.ToString();
			}
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to see a QuickInfo popup with the expected parameters
		/// </summary>
		/// <value>Text for the node that can be used for the IntelliPrompt function declaration.</value>
		public static string DefinitionMarkupText
		{
			get
			{
				string[] args = SizeOfExpression.DefinitionArgumentsMarkupText;
				StringBuilder text = new StringBuilder();
				
				text.Append( "<c=nativetype>FLOAT </c><c=reserved>TIMESTEP</c>" );
				text.Append( "<c=text>()</c><br/><br/>" );
				
				text.Append( "<c=commentdelim>//</c><c=comment>/ PURPOSE: Gets the last time step (typically one or two vertical blanking intervals).</c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/ RETURNS: A float.</c>" );
				
				return text.ToString();
			}
		}

	}

}
