using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a binary expression.
	/// </summary>
	public partial class BinaryExpression : SanScriptParser.Expression {

		private System.String	operatorString;

		/// <summary>
		/// Gets the context ID for the left expression affected by the binary operator.
		/// </summary>
		public const byte LeftExpressionContextID = SanScriptParser.Expression.ExpressionContextIDBase;

		/// <summary>
		/// Gets the context ID for the right expression affected by the binary operator.
		/// </summary>
		public const byte RightExpressionContextID = SanScriptParser.Expression.ExpressionContextIDBase + 1;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte BinaryExpressionContextIDBase = SanScriptParser.Expression.ExpressionContextIDBase + 2;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>BinaryExpression</c> class.
		/// </summary>
		/// <param name="leftExpr">The left-hand <see cref="Expression"/>.</param>
		/// <param name="operatorString">The string version of the operator.</param>
		/// <param name="rightExpr">The right-hand <see cref="Expression"/>.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public BinaryExpression( Expression leftExpr, System.String operatorString, Expression rightExpr, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.LeftExpression = leftExpr;
			this.OperatorString = operatorString;
			this.RightExpression = rightExpr;
		}

		/// <summary>
		/// Initializes a new instance of the <c>BinaryExpression</c> class. 
		/// </summary>
		public BinaryExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>BinaryExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public BinaryExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.Operator;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.BinaryExpression;
			}
		}

		/// <summary>
		/// Gets or sets the left expression affected by the binary operator.
		/// </summary>
		/// <value>The left expression affected by the binary operator.</value>
		public Expression LeftExpression {
			get {
				return this.GetChildNode(BinaryExpression.LeftExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, BinaryExpression.LeftExpressionContextID);
			}
		}

		/// <summary>
		/// Gets or sets the right expression affected by the binary operator.
		/// </summary>
		/// <value>The right expression affected by the binary operator.</value>
		public Expression RightExpression {
			get {
				return this.GetChildNode(BinaryExpression.RightExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, BinaryExpression.RightExpressionContextID);
			}
		}

		/// <summary>
		/// Gets or sets an integer indicating the binary operator type's token id.
		/// </summary>
		/// <value>An integer indicating the binary operator type's token id.</value>
		public System.String OperatorString {
			get {
				return operatorString;
			}
			set {
				operatorString = value;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			Expression leftExpr = this.LeftExpression;
			if ( leftExpr != null )
				{
				leftExpr.BuildDisplayText(sb);
			}
			
			if ( this.OperatorString != null )
				{
				if ( leftExpr != null )
					{
					sb.Append( " " );
				}
				
				sb.Append(this.OperatorString);
				
				Expression rightExpr = this.RightExpression;
				if ( rightExpr != null )
					{
					if ( leftExpr != null )
						{
						sb.Append( " " );
					}
					
					rightExpr.BuildDisplayText(sb);
				}
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			Expression leftExpr = this.LeftExpression;
			if ( leftExpr != null )
				{
				leftExpr.BuildMarkupText(sb);
			}
			
			if ( this.OperatorString != null )
				{
				if ( leftExpr != null )
					{
					sb.Append( " " );
				}
				
				if ( (this.OperatorString == "AND") || (this.OperatorString == "OR") || (this.OperatorString == "NOT") )
					{
					sb.Append( "<c=wordoperator>" );
				}
				else
					{
					sb.Append( "<c=operator>" );
				}
				sb.Append( IntelliPrompt.EscapeMarkupText( this.OperatorString ) );
				sb.Append( "</c>" );
				
				Expression rightExpr = this.RightExpression;
				if ( rightExpr != null )
					{
					if ( leftExpr != null )
						{
						sb.Append( " " );
					}
					
					rightExpr.BuildMarkupText(sb);
				}
			}
		}

	}

}
