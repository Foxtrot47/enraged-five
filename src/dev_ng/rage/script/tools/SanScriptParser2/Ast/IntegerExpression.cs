using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for an integer expression.
	/// </summary>
	public partial class IntegerExpression : SanScriptParser.NumberExpression {

		private System.Int32	intValue;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte IntegerExpressionContextIDBase = SanScriptParser.NumberExpression.NumberExpressionContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>IntegerExpression</c> class.
		/// </summary>
		/// <param name="value">The value of the expression.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public IntegerExpression( System.Int32 value, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.IntValue = value;
		}

		/// <summary>
		/// Initializes a new instance of the <c>IntegerExpression</c> class. 
		/// </summary>
		public IntegerExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>IntegerExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public IntegerExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.IntegerExpression;
			}
		}

		/// <summary>
		/// Gets or sets the number.
		/// </summary>
		/// <value>The number.</value>
		public System.Int32 IntValue {
			get {
				return intValue;
			}
			set {
				intValue = value;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append(this.IntValue.ToString());
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=number>" );
			BuildDisplayText(sb);
			sb.Append( "</c>" );
		}

	}

}
