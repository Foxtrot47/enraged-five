using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a NATIVE FUNC declaration.
	/// </summary>
	public partial class NativeFuncDeclarationStatement : SanScriptParser.NativeSubroutineDeclarationStatement {

		/// <summary>
		/// Gets the context ID for the return type.
		/// </summary>
		public const byte ReturnTypeContextID = SanScriptParser.NativeSubroutineDeclarationStatement.NativeSubroutineDeclarationStatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte NativeFuncDeclarationStatementContextIDBase = SanScriptParser.NativeSubroutineDeclarationStatement.NativeSubroutineDeclarationStatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		bool IsDebugOnly;
		
		/// <summary>
		/// Initializes a new instance of the <c>NativeFuncDeclarationStatement</c> class.
		/// </summary>
		/// <param name="returnType">The <see cref="Identifier"/> of the return type.</param>
		/// <param name="name">The <see cref="Identifier"/> of the subroutine.</param>
		/// <param name="args">The <see cref="AstNodeList"/> of arguments to pass in.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public NativeFuncDeclarationStatement( Identifier returnType, Identifier name, AstNodeList args, TextRange textRange, bool isDebugOnly )
			: base( name, args, textRange )
			{
			// Initialize parameters
			IsDebugOnly = isDebugOnly;
			this.ReturnType = returnType;
		}

		/// <summary>
		/// Initializes a new instance of the <c>NativeFuncDeclarationStatement</c> class. 
		/// </summary>
		public NativeFuncDeclarationStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>NativeFuncDeclarationStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public NativeFuncDeclarationStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.NativeFuncDeclarationStatement;
			}
		}

		/// <summary>
		/// Gets or sets the return type.
		/// </summary>
		/// <value>The return type.</value>
		public Identifier ReturnType {
			get {
				return this.GetChildNode(NativeFuncDeclarationStatement.ReturnTypeContextID) as Identifier;
			}
			set {
				this.ChildNodes.Replace(value, NativeFuncDeclarationStatement.ReturnTypeContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			if ( IsDebugOnly )
				{
				sb.Append( "NATIVE DEBUGONLY FUNC " );
			}
			else
				{
				sb.Append( "NATIVE FUNC " );
			}
			
			Identifier rtn = this.ReturnType;
			if ( rtn != null )
				{
				rtn.BuildDisplayText(sb);
				sb.Append( " " );
			}
			
			base.BuildDisplayText(sb);
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			if ( IsDebugOnly)
				{
				sb.Append( "<c=reserved>NATIVE DEBUGONLY FUNC </c>" );
			}
			else
				{
				sb.Append( "<c=reserved>NATIVE FUNC </c>" );
			}
			
			Identifier rtn = this.ReturnType;
			if ( rtn != null )
				{
				rtn.BuildMarkupText(sb);
				sb.Append( " " );
			}
			
			base.BuildMarkupText(sb);
		}

	}

}
