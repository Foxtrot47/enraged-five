using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a simple name.
	/// </summary>
	public abstract partial class IdentifierExpression : SanScriptParser.Expression {

		/// <summary>
		/// Gets the context ID for the name.
		/// </summary>
		public const byte NameContextID = SanScriptParser.Expression.ExpressionContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte IdentifierExpressionContextIDBase = SanScriptParser.Expression.ExpressionContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>IdentifierExpression</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Identifier"/> representing the name.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public IdentifierExpression( Identifier name, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.Name = name;
		}

		/// <summary>
		/// Initializes a new instance of the <c>IdentifierExpression</c> class. 
		/// </summary>
		public IdentifierExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>IdentifierExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public IdentifierExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		public Identifier Name {
			get {
				return this.GetChildNode(IdentifierExpression.NameContextID) as Identifier;
			}
			set {
				this.ChildNodes.Replace(value, IdentifierExpression.NameContextID);
			}
		}

		/// <summary>
		/// Gets the character offset at which to navigate when the editor's caret should jump to the text representation of the AST node.
		/// </summary>
		/// <value>The character offset at which to navigate when the editor's caret should jump to the text representation of the AST node.</value>
		public override int NavigationOffset
		{
			get
			{
				Identifier name = this.Name;
				if ( (name != null) && name.HasStartOffset )
					{
					return name.NavigationOffset;
				}
				else
					{
					return base.NavigationOffset;
				}
			}
		}
		
		internal override void BuildDisplayText(StringBuilder sb)
			{
			this.Name?.BuildDisplayText(sb);
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			this.Name?.BuildMarkupText(sb);
		}

	}

}
