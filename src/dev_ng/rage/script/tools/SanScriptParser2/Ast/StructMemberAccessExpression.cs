using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for struct member access expressions.
	/// </summary>
	public partial class StructMemberAccessExpression : SanScriptParser.IdentifierExpression {

		private bool	isCall;

		/// <summary>
		/// Gets the context ID for the IdentifierExpression that accesses a member of the structure.
		/// </summary>
		public const byte AccessExpressionContextID = SanScriptParser.IdentifierExpression.IdentifierExpressionContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte StructMemberAccessExpressionContextIDBase = SanScriptParser.IdentifierExpression.IdentifierExpressionContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>StructMemberAccessExpression</c> class.
		/// </summary>
		/// <param name="isCall">If this is a call with CALL.</param>
		/// <param name="expr">The <see cref="Expression"/> that accesses the structure.</param>
		/// <param name="name">The <see cref="Identifier"/> representing the name structure.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public StructMemberAccessExpression( bool isCall, Expression expr, Identifier name, TextRange textRange )
			: base( name, textRange )
			{
			// Initialize parameters
			this.IsCall = isCall;
			this.AccessExpression = expr;
			
			// bubble up IsCall
			if ( !this.IsCall && (this.AccessExpression != null) )
				{
				if ( this.AccessExpression.NodeType == SanScriptNodeType.ArrayAccessExpression )
					{
					ArrayAccessExpression access = this.AccessExpression as ArrayAccessExpression;
					if ( access.IsCall )
						{
						this.IsCall = true;
					}
				}
				else if ( this.AccessExpression.NodeType == SanScriptNodeType.StructMemberAccessExpression )
					{
					StructMemberAccessExpression access = this.AccessExpression as StructMemberAccessExpression;
					if ( access.IsCall )
						{
						this.IsCall = true;
					}
				}
				else if ( this.AccessExpression.NodeType == SanScriptNodeType.SubroutineAccessExpression )
					{
					SubroutineAccessExpression access = this.AccessExpression as SubroutineAccessExpression;
					if ( access.IsCall )
						{
						this.IsCall = true;
					}
				}
			}
			
			if ( name != null )
				{
				if ( this.IsCall )
					{
					name.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicEvent;
				}
				else
					{
					name.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicField;
				}
			}
		}

		/// <summary>
		/// Initializes a new instance of the <c>StructMemberAccessExpression</c> class. 
		/// </summary>
		public StructMemberAccessExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>StructMemberAccessExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public StructMemberAccessExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicField;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.StructMemberAccessExpression;
			}
		}

		/// <summary>
		/// Gets or sets true if the member access is a typedef'd subroutine and is called with the CALL prefix.
		/// </summary>
		/// <value>True if the member access is a typedef'd subroutine and is called with the CALL prefix.</value>
		public bool IsCall {
			get {
				return isCall;
			}
			set {
				isCall = value;
			}
		}

		/// <summary>
		/// Gets or sets the IdentifierExpression that accesses a member of the structure.
		/// </summary>
		/// <value>The IdentifierExpression that accesses a member of the structure.</value>
		public Expression AccessExpression {
			get {
				return this.GetChildNode(StructMemberAccessExpression.AccessExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, StructMemberAccessExpression.AccessExpressionContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			if ( this.IsCall )
				{
				sb.Append( "CALL " );
			}
			
			base.BuildDisplayText(sb);
			
			Expression expr = this.AccessExpression;
			if ( expr != null )
				{
				sb.Append( "." );
				expr.BuildDisplayText(sb);
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			if ( this.IsCall )
				{
				sb.Append( "<c=reserved>CALL</c>" );
			}
			
			base.BuildMarkupText(sb);
			
			Expression expr = this.AccessExpression;
			if ( expr != null )
				{
				sb.Append( "<c=text>.</c>" );
				expr.BuildMarkupText(sb);
			}
		}

	}

}
