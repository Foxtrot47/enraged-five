using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a NATIVE type declaration.
	/// </summary>
	public partial class NativeTypeDeclarationStatement : SanScriptParser.IdentifierStatement {

		/// <summary>
		/// Gets the context ID for the identifier for the NATIVE type that this type derives from.
		/// </summary>
		public const byte InheritsFromContextID = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte NativeTypeDeclarationStatementContextIDBase = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>NativeTypeDeclarationStatement</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Identifier"/> name given to the native type.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public NativeTypeDeclarationStatement( Identifier name, Identifier inheritsFrom, TextRange textRange )
			: base( name, textRange )
			{
			// Initialize parameters
			if ( name != null )
				{
				name.DeclareType = Identifier.DeclarationType.Native;
				name.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicClass;
			}
			
			this.InheritsFrom = inheritsFrom;
			if ( inheritsFrom != null )
				{
				inheritsFrom.DeclareType = Identifier.DeclarationType.Native;
				inheritsFrom.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicClass;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <c>NativeTypeDeclarationStatement</c> class. 
		/// </summary>
		public NativeTypeDeclarationStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>NativeTypeDeclarationStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public NativeTypeDeclarationStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicClass;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.NativeTypeDeclarationStatement;
			}
		}

		/// <summary>
		/// Gets or sets the identifier for the NATIVE type that this type derives from.
		/// </summary>
		/// <value>The identifier for the NATIVE type that this type derives from.</value>
		public Identifier InheritsFrom {
			get {
				return this.GetChildNode(NativeTypeDeclarationStatement.InheritsFromContextID) as Identifier;
			}
			set {
				this.ChildNodes.Replace(value, NativeTypeDeclarationStatement.InheritsFromContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "NATIVE" );
			
			Identifier name = this.Name;
			if ( name != null )
				{
				sb.Append( " " );
				name.BuildDisplayText(sb);
			}
			
			Identifier inheritsFrom = this.InheritsFrom;
			if ( inheritsFrom != null )
				{
				sb.Append( " : " );
				inheritsFrom.BuildDisplayText(sb);
			}
			
			string purpose = this.PurposeDisplayText;
			if ( purpose != null )
				{
				sb.Append( "\n\n" );
				sb.Append( purpose );
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>NATIVE</c>" );
			
			Identifier name = this.Name;
			if ( name != null )
				{
				sb.Append( " " );
				name.BuildMarkupText(sb);
			}
			
			Identifier inheritsFrom = this.InheritsFrom;
			if ( inheritsFrom != null )
				{
				sb.Append( " <c=text>:</c> " );
				inheritsFrom.BuildMarkupText(sb);
			}
			
			string purpose = this.PurposeMarkupText;
			if ( purpose != null )
				{
				sb.Append( "<br/><br/>" );
				sb.Append( purpose );
			}
		}

	}

}
