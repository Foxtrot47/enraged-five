using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Specifies the type of an <see cref="AstNode"/>.
	/// </summary>
	public enum SanScriptNodeType {

		/// <summary>
		/// An identifier.
		/// </summary>
		Identifier,

		/// <summary>
		/// A class for a boolean expression.
		/// </summary>
		BooleanExpression,

		/// <summary>
		/// A class for an integer expression.
		/// </summary>
		IntegerExpression,

		/// <summary>
		/// A class for an float expression.
		/// </summary>
		FloatExpression,

		/// <summary>
		/// A class for a default expression.
		/// </summary>
		DefaultExpression,

		/// <summary>
		/// A class for an NULL expression.
		/// </summary>
		NullExpression,

		/// <summary>
		/// A class for a vector expression.
		/// </summary>
		VectorExpression,

		/// <summary>
		/// A class for a string expression.
		/// </summary>
		StringExpression,

		/// <summary>
		/// An ampersand followed by a func or proc identifier.
		/// </summary>
		FuncAddrExpression,

		/// <summary>
		/// A class for a #if, #if not, or #endif expression.
		/// </summary>
		PreprocessorMacroExpression,

		/// <summary>
		/// A class for array access expressions.
		/// </summary>
		ArrayAccessExpression,

		/// <summary>
		/// A class for struct member access expressions.
		/// </summary>
		StructMemberAccessExpression,

		/// <summary>
		/// A class for a subroutine access expression.
		/// </summary>
		SubroutineAccessExpression,

		/// <summary>
		/// A class for a simple variable expression.
		/// </summary>
		VariableExpression,

		/// <summary>
		/// A class for a COUNTOF expression.
		/// </summary>
		CountOfExpression,

		/// <summary>
		/// A class for an ENUMTOINT expression.
		/// </summary>
		EnumToIntExpression,

		/// <summary>
		/// A class for an INTTOENUM expression.
		/// </summary>
		IntToEnumExpression,

		/// <summary>
		/// A class for a INTTONATIVE expression.
		/// </summary>
		IntToNativeExpression,

		/// <summary>
		/// A class for a NATIVETOINT expression.
		/// </summary>
		NativeToIntExpression,

		/// <summary>
		/// A class for a COUNTOF expression.
		/// </summary>
		SizeOfExpression,

		/// <summary>
		/// A class for a HASH expression.
		/// </summary>
		HashExpression,

		/// <summary>
		/// A class for CATCH expression.
		/// </summary>
		CatchExpression,

		/// <summary>
		/// A class for TIMESTEP expression.
		/// </summary>
		TimestepExpression,

		/// <summary>
		/// A parenthesized expression.
		/// </summary>
		ParenthesizedExpression,

		/// <summary>
		/// A binary expression.
		/// </summary>
		BinaryExpression,

		/// <summary>
		/// An empty statement.
		/// </summary>
		EmptyStatement,

		/// <summary>
		/// An assignment statement which can include = += -= *= and /=.
		/// </summary>
		AssignmentStatement,

		/// <summary>
		/// A class for pre/post increment/decrement.
		/// </summary>
		AdditiveStatement,

		/// <summary>
		/// A class for a USING include directive.
		/// </summary>
		UsingStatement,

		/// <summary>
		/// A class for a THROW statement.
		/// </summary>
		ThrowStatement,

		/// <summary>
		/// A class for HSMHISTORY statement.
		/// </summary>
		HistoryStatement,

		/// <summary>
		/// A class for BREAK statement.
		/// </summary>
		BreakStatement,

		/// <summary>
		/// A class for FALLTHRU statement.
		/// </summary>
		FallthruStatement,

		/// <summary>
		/// A return statement.
		/// </summary>
		ReturnStatement,

		/// <summary>
		/// A class for an EXIT statement.
		/// </summary>
		ExitStatement,

		/// <summary>
		/// A class for a BREAKLOOP statement.
		/// </summary>
		BreakLoopStatement,

		/// <summary>
		/// A class for a RELOOP statement.
		/// </summary>
		ReLoopStatement,

		/// <summary>
		/// A class for a GLOBALS TRUE statement.
		/// </summary>
		GlobalsStatement,

		/// <summary>
		/// A class for a #if, #if not, or #endif statement.
		/// </summary>
		PreprocessorMacroStatement,

		/// <summary>
		/// A class for a CONSTINT or CONSTFLOAT definition statement.
		/// </summary>
		ConstDefinitionStatement,

		/// <summary>
		/// A class for a NATIVE type declaration.
		/// </summary>
		NativeTypeDeclarationStatement,

		/// <summary>
		/// A class for a FORWARD ENUM declaration.
		/// </summary>
		ForwardEnumDeclarationStatement,

		/// <summary>
		/// A class for a FORWARD STRUCT declaration.
		/// </summary>
		ForwardStructDeclarationStatement,

		/// <summary>
		/// A class for a label declaration for use by a GOTO statement.
		/// </summary>
		LabelDeclarationStatement,

		/// <summary>
		/// A class for a GOTO statement.
		/// </summary>
		GotoStatement,

		/// <summary>
		/// A class for a GOTOHSMSTATE statement.
		/// </summary>
		GotoStateStatement,

		/// <summary>
		/// A class for a DEFAULTHSMSTATE statement.
		/// </summary>
		DefaultStateStatement,

		/// <summary>
		/// A class for a CALL statement
		/// </summary>
		CallStatement,

		/// <summary>
		/// A class for a PROC or HSMEVENT subroutine call
		/// </summary>
		SubroutineAccessStatement,

		/// <summary>
		/// A class for a NATIVE PROC declaration.
		/// </summary>
		NativeProcDeclarationStatement,

		/// <summary>
		/// A class for a NATIVE HSMEVENT declaration.
		/// </summary>
		NativeEventDeclarationStatement,

		/// <summary>
		/// A class for a NATIVE FUNC declaration.
		/// </summary>
		NativeFuncDeclarationStatement,

		/// <summary>
		/// A class for a TYPEDEF PROC declaration.
		/// </summary>
		TypedefProcDeclarationStatement,

		/// <summary>
		/// A class for a TYPEDEF FUNC declaration.
		/// </summary>
		TypedefFuncDeclarationStatement,

		/// <summary>
		/// A class for an uninitialized non-array variable declaration.
		/// </summary>
		VariableNonArrayDeclarationStatement,

		/// <summary>
		/// A class for an array variable declaration.
		/// </summary>
		VariableArrayDeclarationStatement,

		/// <summary>
		/// A class for an IF ENDIF block, including ELIF and ELSE.
		/// </summary>
		IfBlockStatement,

		/// <summary>
		/// A class for a WHILE ENDWHILE block.
		/// </summary>
		WhileBlockStatement,

		/// <summary>
		/// A class for a REPEAT ENDPREAT block.
		/// </summary>
		RepeatBlockStatement,

		/// <summary>
		/// A class for a FOR ENDFOR block.
		/// </summary>
		ForBlockStatement,

		/// <summary>
		/// A class for a CASE or DEFAULT statement block.
		/// </summary>
		CaseBlockStatement,

		/// <summary>
		/// A class for a SWITCH ENDSWITCH statement block.
		/// </summary>
		SwitchBlockStatement,

		/// <summary>
		/// A class for a HSMACTIVATE ENDHSMACTIVATE block.
		/// </summary>
		ActivateBlockStatement,

		/// <summary>
		/// A class for a HSMDEACTIVATE ENDHSMDEACTIVATE block.
		/// </summary>
		DeactivateBlockStatement,

		/// <summary>
		/// A class for a GLOBALS ENDGLOBALS block.
		/// </summary>
		GlobalsDefinitionBlockStatement,

		/// <summary>
		/// A class for a SCRIPT ENDSCRIPT block.
		/// </summary>
		ScriptBlockStatement,

		/// <summary>
		/// A class for a STRUCT ENDSTRUCT block.
		/// </summary>
		StructDefinitionBlockStatement,

		/// <summary>
		/// A class for a HSMSTATE ENDHSMSTATE block.
		/// </summary>
		StateDefinitionBlockStatement,

		/// <summary>
		/// A class for an ENUM ENDENUM block.
		/// </summary>
		EnumDefinitionBlockStatement,

		/// <summary>
		/// A class for an HASH_ENUM ENDENUM block.
		/// </summary>
		HashEnumDefinitionBlockStatement,

		/// <summary>
		/// A class for an STRICT_ENUM ENDENUM block.
		/// </summary>
		StrictEnumDefinitionBlockStatement,

		/// <summary>
		/// A class for a PROC declaration and code block.
		/// </summary>
		ProcDefinitionBlockStatement,

		/// <summary>
		/// A class for a HSMEVENT declaration and code block.
		/// </summary>
		EventDefinitionBlockStatement,

		/// <summary>
		/// A class for a FUNC declaration and code block.
		/// </summary>
		FuncDefinitionBlockStatement,

		/// <summary>
		/// A class to hold a multi-line comments block.
		/// </summary>
		MultiLineCommentBlock,

		/// <summary>
		/// A class to hold a PURPOSE comments block.
		/// </summary>
		PurposeCommentBlock,

		/// <summary>
		/// A Simple language compilation unit.
		/// </summary>
		CompilationUnit,

	}

}
