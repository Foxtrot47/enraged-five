using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser
{

    /// <summary>
    /// Represents a class for an INTINDEXTOHASHENUM expression.
    /// </summary>
    public partial class IntIndexToHashEnumExpression : SanScriptParser.Expression
    {

        /// <summary>
        /// Gets the context ID for the name of the ENUM to convert to.
        /// </summary>
        public const byte EnumTypeContextID = SanScriptParser.Expression.ExpressionContextIDBase;

        /// <summary>
        /// Gets the context ID for the integer to convert.
        /// </summary>
        public const byte ExpressionToConvertContextID = SanScriptParser.Expression.ExpressionContextIDBase + 1;

        /// <summary>
        /// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
        /// </summary>
        /// <remarks>
        /// Base all your context ID constants off of this value.
        /// </remarks>
        protected const byte IntToEnumExpressionContextIDBase = SanScriptParser.Expression.ExpressionContextIDBase + 2;

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // OBJECT
        /////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Initializes a new instance of the <c>IntIndexToHashEnumExpression</c> class.
        /// </summary>
        /// <param name="enumType">The <see cref="Identifier"/> that is the enumeration passed to the subroutine.</param>
        /// <param name="expr">The <see cref="Expression"/> passed to to the subroutine that is to be converted.</param>
        /// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
        public IntIndexToHashEnumExpression(Identifier enumType, Expression expr, TextRange textRange)
            : this(textRange)
        {
            // Initialize parameters
            this.EnumType = enumType;
            this.ExpressionToConvert = expr;
        }

        /// <summary>
        /// Initializes a new instance of the <c>IntIndexToHashEnumExpression</c> class. 
        /// </summary>
        public IntIndexToHashEnumExpression() { }

        /// <summary>
        /// Initializes a new instance of the <c>IntIndexToHashEnumExpression</c> class. 
        /// </summary>
        /// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
        public IntIndexToHashEnumExpression(TextRange textRange) : base(textRange) { }

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // PUBLIC PROCEDURES
        /////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Accepts the specified visitor for visiting this node.
        /// </summary>
        /// <param name="visitor">The visitor to accept.</param>
        /// <remarks>This method is part of the visitor design pattern implementation.</remarks>
        protected override void AcceptCore(AstVisitor visitor)
        {
            if (visitor.OnVisiting(this))
            {
                // Visit children
                if (this.ChildNodeCount > 0)
                    this.AcceptChildren(visitor, this.ChildNodes);
            }
            visitor.OnVisited(this);
        }

        /// <summary>
        /// Gets the image index that is applicable for displaying this node in a user interface control.
        /// </summary>
        /// <value>The image index that is applicable for displaying this node in a user interface control.</value>
        public override int ImageIndex
        {
            get
            {
                return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.Keyword;
            }
        }

        /// <summary>
        /// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
        /// </summary>
        /// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
        public override SanScriptNodeType NodeType
        {
            get
            {
                return SanScriptNodeType.IntIndexToHashEnumExpression;
            }
        }

        /// <summary>
        /// Gets or sets the name of the ENUM to convert to.
        /// </summary>
        /// <value>The name of the ENUM to convert to.</value>
        public Identifier EnumType
        {
            get
            {
                return this.GetChildNode(IntIndexToHashEnumExpression.EnumTypeContextID) as Identifier;
            }
            set
            {
                this.ChildNodes.Replace(value, IntIndexToHashEnumExpression.EnumTypeContextID);
            }
        }

        /// <summary>
        /// Gets or sets the integer to convert.
        /// </summary>
        /// <value>The integer to convert.</value>
        public Expression ExpressionToConvert
        {
            get
            {
                return this.GetChildNode(IntIndexToHashEnumExpression.ExpressionToConvertContextID) as Expression;
            }
            set
            {
                this.ChildNodes.Replace(value, IntIndexToHashEnumExpression.ExpressionToConvertContextID);
            }
        }

        /// <summary>
        /// Gets text representing the node that can be used for display, such as in a document outline.
        /// </summary>
        /// <value>Text representing the node that can be used for display, such as in a document outline.</value>
        public override string DisplayText
        {
            get
            {
                StringBuilder text = new StringBuilder();

                text.Append("INT_INDEX_TO_HASH_ENUM");

                Identifier enumType = this.EnumType;
                Expression expr = this.ExpressionToConvert;
                if ((enumType != null) && (expr != null))
                {
                    text.Append("( ");
                    text.Append(enumType.DisplayText);
                    text.Append(", ");
                    text.Append(expr.DisplayText);
                    text.Append(" )");
                }

                return text.ToString();
            }
        }

        /// <summary>
        /// Gets the marked up text that is appropriate for IntelliPrompt.
        /// </summary>
        /// <value>Text for the node that can be used for IntelliPrompt.</value>
        public override string MarkupText
        {
            get
            {
                StringBuilder text = new StringBuilder();

                text.Append("<c=reserved>INT_INDEX_TO_HASH_ENUM</c>");

                Identifier enumType = this.EnumType;
                Expression expr = this.ExpressionToConvert;
                if ((enumType != null) && (expr != null))
                {
                    text.Append("<c=text>(</c>");
                    text.Append(enumType.MarkupText);
                    text.Append("<c=text>, </c>");
                    text.Append(expr.MarkupText);
                    text.Append("<c=text> )</c>");
                }

                return text.ToString();
            }
        }

        /// <summary>
        /// Since this is a built-in function, this will provide a way to see the expected parameters.
        /// </summary>
        /// <value>Text for the node that can be used for displaying a function declaration.</value>
        public static string DefinitionText
        {
            get
            {
                StringBuilder text = new StringBuilder();

                text.Append("<enumTypeName> INT_INDEX_TO_HASH_ENUM( ENUM enumTypeName, INT value )\n\n");
                text.Append("/// PURPOSE: Converts in index to the enumTypeName to the match HASH_ENUM value.\n");
                text.Append("/// PARAMS: \n");
                text.Append("///    enumTypeName - The type of ENUM to convert to.\n");
                text.Append("///    value - The index to convert.\n");
                text.Append("/// RETURNS: Returns the hash enum value that was defined at the supplied index for the enumTypeName.");

                return text.ToString();
            }
        }

        /// <summary>
        /// Since this is a built-in function, this will provide a way to see a QuickInfo popup with the expected parameters
        /// </summary>
        /// <value>Text for the node that can be used for the IntelliPrompt function declaration.</value>
        public static string DefinitionMarkupText
        {
            get
            {
                string[] args = IntIndexToHashEnumExpression.DefinitionArgumentsMarkupText;
                StringBuilder text = new StringBuilder();

                text.Append("<c=user>&lt;enumTypeName&gt; </c>");
                text.Append("<c=reserved>INT_INDEX_TO_HASH_ENUM</c>");
                text.Append("<c=text>( </c>");
                text.Append(args[0]);
                text.Append("<c=text>, </c>");
                text.Append(args[1]);
                text.Append("<c=text> )</c><br/><br/>");

                text.Append("<c=commentdelim>//</c><c=comment>/ PURPOSE: Converts in index to the enumTypeName to the match HASH_ENUM value.</c><br/>");
                text.Append("<c=commentdelim>//</c><c=comment>/ PARAMS: </c><br/>");
                text.Append("<c=commentdelim>//</c><c=comment>/&nbsp;&nbsp;&nbsp;&nbsp;enumTypeName - The type of ENUM to convert to.</c><br/>");
                text.Append("<c=commentdelim>//</c><c=comment>/&nbsp;&nbsp;&nbsp;&nbsp;value - The index to convert.</c><br/>");
                text.Append("<c=commentdelim>//</c><c=comment>/ RETURNS: Returns the hash enum value that was defined at the supplied index for the enumTypeName.</c>");

                return text.ToString();
            }
        }

        /// <summary>
        /// Since this is a built-in function, this will provide a way to retrieve the individual arguments for use in a QuickInfo popup
        /// </summary>
        /// <value>Markup text for each argument.</value>
        public static string[] DefinitionArgumentsMarkupText
        {
            get
            {
                string[] args = new string[2];

                StringBuilder text = new StringBuilder();
                text.Append("<c=reserved>ENUM </c>");
                text.Append("<c=user>enumTypeName</c>");
                args[0] = text.ToString();

                text = new StringBuilder();
                text.Append("<c=reserved>INT </c>");
                text.Append("<c=text>value</c>");
                args[1] = text.ToString();

                return args;
            }
        }

    }

}
