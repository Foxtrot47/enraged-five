using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for an ENUMTOINT expression.
	/// </summary>
	public partial class EnumToIntExpression : SanScriptParser.Expression {

		/// <summary>
		/// Gets the context ID for the expression returning the ENUM to convert.
		/// </summary>
		public const byte ExpressionToConvertContextID = SanScriptParser.Expression.ExpressionContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte EnumToIntExpressionContextIDBase = SanScriptParser.Expression.ExpressionContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>EnumToIntExpression</c> class.
		/// </summary>
		/// <param name="expr">The <see cref="Expression"/> returning the enumeration that gets passed to the subroutine.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public EnumToIntExpression( Expression expr, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.ExpressionToConvert = expr;
		}

		/// <summary>
		/// Initializes a new instance of the <c>EnumToIntExpression</c> class. 
		/// </summary>
		public EnumToIntExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>EnumToIntExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public EnumToIntExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.Keyword;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.EnumToIntExpression;
			}
		}

		/// <summary>
		/// Gets or sets the expression returning the ENUM to convert.
		/// </summary>
		/// <value>The expression returning the ENUM to convert.</value>
		public Expression ExpressionToConvert {
			get {
				return this.GetChildNode(EnumToIntExpression.ExpressionToConvertContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, EnumToIntExpression.ExpressionToConvertContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "ENUM_TO_INT" );
			
			Expression expr = this.ExpressionToConvert;
			if ( expr != null )
				{
				sb.Append( "( " );
				expr.BuildDisplayText(sb);
				sb.Append( " )" );
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>ENUM_TO_INT</c>" );
			
			Expression expr = this.ExpressionToConvert;
			if ( expr != null )
				{
				sb.Append( "<c=text>(</c>" );
				expr.BuildMarkupText(sb);
				sb.Append( "<c=text> )</c>" );
			}
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to see the expected parameters.
		/// </summary>
		/// <value>Text for the node that can be used for displaying a function declaration.</value>
		public static string DefinitionText
		{
			get
			{
				StringBuilder text = new StringBuilder();
				
				text.Append( "INT ENUM_TO_INT( <enumTypeName> enumName )\n\n" );
				text.Append( "/// PURPOSE: Gets the integer equivalent of the specified enumeration.\n" );
				text.Append( "/// PARAMS: \n" );
				text.Append( "///    enumName - The enum value.\n" );
				text.Append( "/// RETURNS: An integer." );
				
				return text.ToString();
			}
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to see a QuickInfo popup with the expected parameters
		/// </summary>
		/// <value>Text for the node that can be used for the IntelliPrompt function declaration.</value>
		public static string DefinitionMarkupText
		{
			get
			{
				string[] args = EnumToIntExpression.DefinitionArgumentsMarkupText;
				StringBuilder text = new StringBuilder();
				
				text.Append( "<c=reserved>INT ENUM_TO_INT</c>" );
				text.Append( "<c=text>( </c>" );
				text.Append( args[0] );
				text.Append( "<c=text> )</c><br/><br/>" );
				
				text.Append( "<c=commentdelim>//</c><c=comment>/ PURPOSE: Gets the integer equivalent of the specified enumeration.</c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/ PARAMS: </c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/&nbsp;&nbsp;&nbsp;&nbsp;enumName - The enum value.</c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/ RETURNS: An integer.</c>" );
				
				return text.ToString();
			}
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to retrieve the individual arguments for use in a QuickInfo popup
		/// </summary>
		/// <value>Markup text for each argument.</value>
		public static string[] DefinitionArgumentsMarkupText
		{
			get
			{
				string[] args = new string[1];
				
				StringBuilder text = new StringBuilder();
				text.Append( "<c=user>&lt;enumTypeName&gt; </c>" );
				text.Append( "<c=text>name</c>" );
				args[0] = text.ToString();
				
				return args;
			}
		}

	}

}
