using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for an uninitialized non-array variable declaration.
	/// </summary>
	public partial class VariableNonArrayDeclarationStatement : SanScriptParser.VariableDeclarationStatement {

		/// <summary>
		/// Gets the context ID for the expression that initializes the variable, if any.
		/// </summary>
		public const byte InitializerContextID = SanScriptParser.VariableDeclarationStatement.VariableDeclarationStatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte VariableNonArrayDeclarationStatementContextIDBase = SanScriptParser.VariableDeclarationStatement.VariableDeclarationStatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>VariableNonArrayDeclarationStatement</c> class.
		/// </summary>
		/// <param name="init">The <see cref="Expression"/> that initializes the variable.</param>
		/// <param name="name">The <see cref="Identifier"/> name of the variable.</param>
		/// <param name="type">The <see cref="Identifier"/> name of data type.</param>
		/// <param name="isRef"><c>true</c> if this declaration is a reference.</param>
		/// <param name="isSubDeclaration"><c>true</c> if this declaration occurs after the first declaration in a comma-separated list.</param>
		/// <param name="subDeclaration">A <see cref="Statement"/> declared on the same line, separated by commas.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public VariableNonArrayDeclarationStatement( Expression init, Identifier name, Identifier type, bool isRef, bool isSubDeclaration,
		Statement subDeclaration, TextRange textRange )
			: base( name, type, isRef, isSubDeclaration, subDeclaration, textRange )
			{
			// Initialize parameters
			this.Initializer = init;
		}

		/// <summary>
		/// Initializes a new instance of the <c>VariableNonArrayDeclarationStatement</c> class. 
		/// </summary>
		public VariableNonArrayDeclarationStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>VariableNonArrayDeclarationStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public VariableNonArrayDeclarationStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.VariableNonArrayDeclarationStatement;
			}
		}

		/// <summary>
		/// Gets or sets the expression that initializes the variable, if any.
		/// </summary>
		/// <value>The expression that initializes the variable, if any.</value>
		public Expression Initializer {
			get {
				return this.GetChildNode(VariableNonArrayDeclarationStatement.InitializerContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, VariableNonArrayDeclarationStatement.InitializerContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			base.BuildDisplayText(sb);
			
			Expression init = this.Initializer;
			if ( init != null )
				{
				sb.Append( " = " );
				init.BuildDisplayText(sb);
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			base.BuildMarkupText(sb);
			
			Expression init = this.Initializer;
			if ( init != null )
				{
				sb.Append( "<c=operator> = </c>" );
				init.BuildMarkupText(sb);
			}
		}

	}

}
