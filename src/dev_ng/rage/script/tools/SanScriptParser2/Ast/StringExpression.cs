using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a string expression.
	/// </summary>
	public partial class StringExpression : SanScriptParser.Expression {

		private System.String	stringValue;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte StringExpressionContextIDBase = SanScriptParser.Expression.ExpressionContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>StringExpression</c> class.
		/// </summary>
		/// <param name="value">The value of the expression.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public StringExpression( string value, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.StringValue = value;
		}

		/// <summary>
		/// Initializes a new instance of the <c>StringExpression</c> class. 
		/// </summary>
		public StringExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>StringExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public StringExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.StringExpression;
			}
		}

		/// <summary>
		/// Gets or sets the string (without the surrounding quotes).
		/// </summary>
		/// <value>The string (without the surrounding quotes).</value>
		public System.String StringValue {
			get {
				return stringValue;
			}
			set {
				stringValue = value;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "\"" );
			sb.Append( this.StringValue.ToUpper() );
			sb.Append( "\"" );
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=stringdelim>" );
			sb.Append( IntelliPrompt.EscapeMarkupText( "\"" ) );
			sb.Append( "</c>" );
			
			sb.Append( "<c=string>" );
			sb.Append( IntelliPrompt.EscapeMarkupText( this.StringValue ) );
			sb.Append( "</c>" );
			
			sb.Append( "<c=stringdelim>" );
			sb.Append( IntelliPrompt.EscapeMarkupText( "\"" ) );
			sb.Append( "</c>" );
		}

	}

}
