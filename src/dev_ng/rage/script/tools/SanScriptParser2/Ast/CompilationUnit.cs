using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a Simple language compilation unit.
	/// </summary>
	public partial class CompilationUnit : SanScriptParser.AstNode, ICompilationUnit, ISemanticParseData {

		private System.String	sourceKey;

		private bool						m_hasLanguageTransitions;
		private ArrayList					m_syntaxErrors;

		/// <summary>
		/// Gets the context ID for the collection of USINGs.
		/// </summary>
		public const byte UsingStatementContextID = SanScriptParser.AstNode.AstNodeContextIDBase;

		/// <summary>
		/// Gets the context ID for the collection of CONST_INTs and CONST_FLOATs and ENUM values.
		/// </summary>
		public const byte ConstantContextID = SanScriptParser.AstNode.AstNodeContextIDBase + 1;

		/// <summary>
		/// Gets the context ID for the collection of STRUCTs, NATIVE types, ENUM types, and FORWARD ENUM types defined.
		/// </summary>
		public const byte DataTypeContextID = SanScriptParser.AstNode.AstNodeContextIDBase + 2;

		/// <summary>
		/// Gets the context ID for the GLOBAL statement block, if any.
		/// </summary>
		public const byte GlobalsSectionContextID = SanScriptParser.AstNode.AstNodeContextIDBase + 3;

		/// <summary>
		/// Gets the context ID for the collection of top-level variables declared that are local to this compilation unit.
		/// </summary>
		public const byte StaticVariableContextID = SanScriptParser.AstNode.AstNodeContextIDBase + 4;

		/// <summary>
		/// Gets the context ID for the collection of FUNCs, PROCs, top-level EVENTs, and/or SCRIPT found.
		/// </summary>
		public const byte SubroutineContextID = SanScriptParser.AstNode.AstNodeContextIDBase + 5;

		/// <summary>
		/// Gets the context ID for the collection of STATEs found.
		/// </summary>
		public const byte StateContextID = SanScriptParser.AstNode.AstNodeContextIDBase + 6;

		/// <summary>
		/// Gets the context ID for any other top-level statements not already categorized.
		/// </summary>
		public const byte OtherStatementContextID = SanScriptParser.AstNode.AstNodeContextIDBase + 7;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte CompilationUnitContextIDBase = SanScriptParser.AstNode.AstNodeContextIDBase + 8;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>CompilationUnit</c> class. 
		/// </summary>
		public CompilationUnit() {}

		/// <summary>
		/// Initializes a new instance of the <c>CompilationUnit</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public CompilationUnit(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.CompilationUnit;
			}
		}

		/// <summary>
		/// Gets or sets the string-based key that identifies the source of the code, which typically is a filename.
		/// </summary>
		/// <value>The string-based key that identifies the source of the code, which typically is a filename.</value>
		public System.String SourceKey {
			get {
				return sourceKey;
			}
			set {
				sourceKey = value;
			}
		}

		/// <summary>
		/// Gets the collection of USINGs.
		/// </summary>
		/// <value>The collection of statements.</value>
		public IAstNodeList UsingStatements {
			get {
				return new AstNodeListWrapper(this, CompilationUnit.UsingStatementContextID);
			}
		}

		/// <summary>
		/// Gets the collection of CONST_INTs and CONST_FLOATs and ENUM values.
		/// </summary>
		/// <value>The collection of statements.</value>
		public IAstNodeList Constants {
			get {
				return new AstNodeListWrapper(this, CompilationUnit.ConstantContextID);
			}
		}

		/// <summary>
		/// Gets the collection of STRUCTs, NATIVE types, ENUM types, and FORWARD ENUM types defined.
		/// </summary>
		/// <value>The collection of statements.</value>
		public IAstNodeList DataTypes {
			get {
				return new AstNodeListWrapper(this, CompilationUnit.DataTypeContextID);
			}
		}

		/// <summary>
		/// Gets the GLOBAL statement block, if any.
		/// </summary>
		/// <value>The collection of statements.</value>
		public IAstNodeList GlobalsSection {
			get {
				return new AstNodeListWrapper(this, CompilationUnit.GlobalsSectionContextID);
			}
		}

		/// <summary>
		/// Gets the collection of top-level variables declared that are local to this compilation unit.
		/// </summary>
		/// <value>The collection of statements.</value>
		public IAstNodeList StaticVariables {
			get {
				return new AstNodeListWrapper(this, CompilationUnit.StaticVariableContextID);
			}
		}

		/// <summary>
		/// Gets the collection of FUNCs, PROCs, top-level EVENTs, and/or SCRIPT found.
		/// </summary>
		/// <value>The collection of statements.</value>
		public IAstNodeList Subroutines {
			get {
				return new AstNodeListWrapper(this, CompilationUnit.SubroutineContextID);
			}
		}

		/// <summary>
		/// Gets the collection of STATEs found.
		/// </summary>
		/// <value>The collection of statements.</value>
		public IAstNodeList States {
			get {
				return new AstNodeListWrapper(this, CompilationUnit.StateContextID);
			}
		}

		/// <summary>
		/// Gets any other top-level statements not already categorized.
		/// </summary>
		/// <value>The collection of statements.</value>
		public IAstNodeList OtherStatements {
			get {
				return new AstNodeListWrapper(this, CompilationUnit.OtherStatementContextID);
			}
		}

		/// <summary>
		/// Returns whether an <see cref="CollapsibleNodeOutliningParser"/> should visit the child nodes of the specified <see cref="IAstNode"/>
		/// to look for collapsible nodes.
		/// </summary>
		/// <param name="node">The <see cref="IAstNode"/> to examine.</param>
		/// <returns>
		/// <c>true</c> if the child nodes should be visited; otherwise, <c>false</c>.
		/// </returns>
		bool ICompilationUnit.ShouldVisitChildNodesForOutlining( IAstNode node )
			{
			return true;
		}
		
		/// <summary>
		/// Adds any extra <see cref="CollapsibleNodeOutliningParserData"/> nodes to the <see cref="CollapsibleNodeOutliningParser"/>,
		/// such as for comments that should be marked as collapsible.
		/// </summary>
		/// <param name="outliningParser">The <see cref="CollapsibleNodeOutliningParser"/> to update.</param>
		void ICompilationUnit.UpdateOutliningParser( CollapsibleNodeOutliningParser outliningParser )
			{
		}
		
		/// <summary>
		/// Gets whether the compilation unit contains errors.
		/// </summary>
		/// <value>
		/// <c>true</c> if the compilation unit contains errors.
		/// </value>
		public bool HasErrors
		{
			get
			{
				return ((m_syntaxErrors != null) && (m_syntaxErrors.Count > 0));
			}
		}
		
		/// <summary>
		/// Gets or sets whether the compilation unit contains any language transitions.
		/// </summary>
		/// <value>
		/// <c>true</c> if the compilation unit contains any language transitions; otherwise, <c>false</c>.
		/// </value>
		public bool HasLanguageTransitions
		{
			get
			{
				return m_hasLanguageTransitions;
			}
			set
			{
				m_hasLanguageTransitions = value;
			}
		}
		
		/// <summary>
		/// Gets whether the AST node is a language root node.
		/// </summary>
		/// <value>
		/// <c>true</c> if the AST node is a language root node; otherwise, <c>false</c>.
		/// </value>
		/// <remarks>
		/// When in a scenario where AST node trees from multiple languages have been merged together,
		/// it is useful to identify where child language AST node trees begin within their parents.
		/// </remarks>
		public override bool IsLanguageRoot
		{
			get
			{
				return true;
			}
		}
		
		/// <summary>
		/// Gets the collection of syntax errors that were found in the compilation unit.
		/// </summary>
		/// <value>The collection of syntax errors that were found in the compilation unit.</value>
		public IList SyntaxErrors
		{
			get
			{
				if ( m_syntaxErrors == null )
					{
					m_syntaxErrors = new ArrayList();
				}
				
				return m_syntaxErrors;
			}
		}
		
		public static bool AllowedInGlobalsSection( AstNode node )
			{
			return (node.NodeType == SanScriptNodeType.ConstDefinitionStatement)
				|| (node.NodeType == SanScriptNodeType.NativeTypeDeclarationStatement)
				|| (node.NodeType == SanScriptNodeType.VariableArrayDeclarationStatement)
				|| (node.NodeType == SanScriptNodeType.VariableNonArrayDeclarationStatement)
				|| (node.NodeType == SanScriptNodeType.ForwardEnumDeclarationStatement)
				|| (node.NodeType == SanScriptNodeType.ForwardStructDeclarationStatement)
				|| (node.NodeType == SanScriptNodeType.StructDefinitionBlockStatement)
				|| (node.NodeType == SanScriptNodeType.EnumDefinitionBlockStatement)
				|| (node.NodeType == SanScriptNodeType.HashEnumDefinitionBlockStatement)
				|| (node.NodeType == SanScriptNodeType.StrictEnumDefinitionBlockStatement)
				|| (node.NodeType == SanScriptNodeType.PurposeCommentBlock)
				|| (node.NodeType == SanScriptNodeType.MultiLineCommentBlock)
				|| (node.NodeType == SanScriptNodeType.UsingStatement)
				|| (node.NodeType == SanScriptNodeType.NativeProcDeclarationStatement)
				|| (node.NodeType == SanScriptNodeType.NativeEventDeclarationStatement)
				|| (node.NodeType == SanScriptNodeType.NativeFuncDeclarationStatement)
				|| (node.NodeType == SanScriptNodeType.TypedefProcDeclarationStatement)
				|| (node.NodeType == SanScriptNodeType.TypedefFuncDeclarationStatement)
				|| (node.NodeType == SanScriptNodeType.PreprocessorMacroStatement);
		}
		
		public static bool IsNativeType( string text )
			{
			text = text.ToUpper();
			return (text == "BOOL")
				|| (text == "CONST_INT")
				|| (text == "CONST_FLOAT")
				|| (text == "ENUM_TO_INT")
				|| (text == "FLOAT")
				|| (text == "TWEAK_FLOAT")
				|| (text == "INT")
				|| (text == "TWEAK_INT")
				|| (text == "VECTOR")
				|| (text == "STRING")
				|| (text == "TEXT_LABEL")
				|| (text == "TEXT_LABEL_3")
				|| (text == "TEXT_LABEL_7")
				|| (text == "TEXT_LABEL_15")
				|| (text == "TEXT_LABEL_23")
				|| (text == "TEXT_LABEL_31")
				|| (text == "TEXT_LABEL_63");
		}

	}

}
