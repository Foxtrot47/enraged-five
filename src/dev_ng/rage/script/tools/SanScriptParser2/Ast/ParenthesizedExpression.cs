using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a parenthesized expression.
	/// </summary>
	public partial class ParenthesizedExpression : SanScriptParser.Expression {

		/// <summary>
		/// Gets the context ID for the expression contained by the parenthesis.
		/// </summary>
		public const byte InnerExpressionContextID = SanScriptParser.Expression.ExpressionContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte ParenthesizedExpressionContextIDBase = SanScriptParser.Expression.ExpressionContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>ParenthesizedExpression</c> class.
		/// </summary>
		/// <param name="expr">The <see cref="Expression"/>.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ParenthesizedExpression( Expression expr, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.InnerExpression = expr;
		}

		/// <summary>
		/// Initializes a new instance of the <c>ParenthesizedExpression</c> class. 
		/// </summary>
		public ParenthesizedExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>ParenthesizedExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ParenthesizedExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.ParenthesizedExpression;
			}
		}

		/// <summary>
		/// Gets or sets the expression contained by the parenthesis.
		/// </summary>
		/// <value>The expression contained by the parenthesis.</value>
		public Expression InnerExpression {
			get {
				return this.GetChildNode(ParenthesizedExpression.InnerExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, ParenthesizedExpression.InnerExpressionContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "(" );
			
			Expression expr = this.InnerExpression;
			if ( expr != null )
				{
				sb.Append( " " );
				expr.BuildDisplayText(sb);
				sb.Append( " " );
			}
			
			sb.Append( ")" );
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=text>(</c>" );
			
			Expression expr = this.InnerExpression;
			if ( expr != null )
				{
				sb.Append( " " );
				expr.BuildMarkupText(sb);
				sb.Append( " " );
			}
			
			sb.Append( "<c=text>)</c>" );
		}

	}

}
