using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a boolean expression.
	/// </summary>
	public partial class BooleanExpression : SanScriptParser.Expression {

		private bool	boolValue;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte BooleanExpressionContextIDBase = SanScriptParser.Expression.ExpressionContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>BooleanExpression</c> class.
		/// </summary>
		/// <param name="value">The value of the expression.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public BooleanExpression( bool value, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.BoolValue = value;
		}

		/// <summary>
		/// Initializes a new instance of the <c>BooleanExpression</c> class. 
		/// </summary>
		public BooleanExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>BooleanExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public BooleanExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.BooleanExpression;
			}
		}

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		public bool BoolValue {
			get {
				return boolValue;
			}
			set {
				boolValue = value;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append(this.BoolValue.ToString());
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=nativetype>" );
			BuildDisplayText(sb);
			sb.Append( "</c>" );
		}

	}

}
