using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a NATIVE HSMEVENT declaration.
	/// </summary>
	public partial class NativeEventDeclarationStatement : SanScriptParser.NativeSubroutineDeclarationStatement {

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte NativeEventDeclarationStatementContextIDBase = SanScriptParser.NativeSubroutineDeclarationStatement.NativeSubroutineDeclarationStatementContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>NativeEventDeclarationStatement</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Identifier"/> of the subroutine.</param>
		/// <param name="args">The <see cref="AstNodeList"/> of arguments to pass in.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public NativeEventDeclarationStatement( Identifier name, AstNodeList args, TextRange textRange )
			: base( name, args, textRange )
			{
		}

		/// <summary>
		/// Initializes a new instance of the <c>NativeEventDeclarationStatement</c> class. 
		/// </summary>
		public NativeEventDeclarationStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>NativeEventDeclarationStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public NativeEventDeclarationStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.NativeEventDeclarationStatement;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "NATIVE HSMEVENT " );
			base.BuildDisplayText(sb);
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>NATIVE HSMEVENT </c>" );
			base.BuildMarkupText(sb);
		}

	}

}
