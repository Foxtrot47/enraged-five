using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a SWITCH ENDSWITCH statement block.
	/// </summary>
	public partial class SwitchBlockStatement : SanScriptParser.BlockStatement {

		/// <summary>
		/// Gets the context ID for the expression to evaluate.  Must be integer.
		/// </summary>
		public const byte ConditionContextID = SanScriptParser.BlockStatement.BlockStatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte SwitchBlockStatementContextIDBase = SanScriptParser.BlockStatement.BlockStatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>SwitchBlockStatement</c> class.
		/// </summary>
		/// <param name="condition">The condition <see cref="Expression"/>.</param>
		/// <param name="statements">The <see cref="AstNodeList"/> of statements contained in this block.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public SwitchBlockStatement( Expression condition, AstNodeList statements, TextRange textRange )
			: base( statements, textRange )
			{
			// Initialize parameters
			this.Condition = condition;
		}

		/// <summary>
		/// Initializes a new instance of the <c>SwitchBlockStatement</c> class. 
		/// </summary>
		public SwitchBlockStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>SwitchBlockStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public SwitchBlockStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.SwitchBlockStatement;
			}
		}

		/// <summary>
		/// Gets or sets the expression to evaluate.  Must be integer.
		/// </summary>
		/// <value>The expression to evaluate.  Must be integer.</value>
		public Expression Condition {
			get {
				return this.GetChildNode(SwitchBlockStatement.ConditionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, SwitchBlockStatement.ConditionContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "SWITCH " );
			this.Condition?.BuildDisplayText(sb);
		}
		
		/// <summary>
		/// Gets the character offset at which to navigate when the editor's caret should jump to the text representation of the AST node.
		/// </summary>
		/// <value>The character offset at which to navigate when the editor's caret should jump to the text representation of the AST node.</value>
		public override int NavigationOffset
		{
			get
			{
				Expression condition = this.Condition;
				if ( (condition != null) && condition.HasStartOffset )
					{
					return condition.NavigationOffset;
				}
				else
					{
					return base.NavigationOffset;
				}
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>SWITCH</c>" );
			
			Expression condition = this.Condition;
			if ( condition != null )
				{
				sb.Append( " " );
				condition.BuildMarkupText(sb);
			}
		}

	}

}
