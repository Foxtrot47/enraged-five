using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a FOR ENDFOR block.
	/// </summary>
	public partial class ForBlockStatement : SanScriptParser.BlockStatement {

		/// <summary>
		/// Gets the context ID for the iterator.
		/// </summary>
		public const byte VariableContextID = SanScriptParser.BlockStatement.BlockStatementContextIDBase;

		/// <summary>
		/// Gets the context ID for variable that gets initialized and incremented.  Can only be an integer or an integer identifier.
		/// </summary>
		public const byte StartExpressionContextID = SanScriptParser.BlockStatement.BlockStatementContextIDBase + 1;

		/// <summary>
		/// Gets the context ID for when to terminate.  Can only be an integer or an integer identifier.
		/// </summary>
		public const byte EndExpressionContextID = SanScriptParser.BlockStatement.BlockStatementContextIDBase + 2;

		/// <summary>
		/// Gets the context ID for the step value.  Null if none provided.
		/// </summary>
		public const byte StepExpressionContextID = SanScriptParser.BlockStatement.BlockStatementContextIDBase + 3;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte ForBlockStatementContextIDBase = SanScriptParser.BlockStatement.BlockStatementContextIDBase + 4;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>ForBlockStatement</c> class.
		/// </summary>
		/// <param name="start">The <see cref="Expression"/> that initializes the iterator.</param>
		/// <param name="end">The <see cref="Expression"/> that ends the for-loop.</param>
		/// <param name="step">The <see cref="Expression"/> for the step size.</param>
		/// <param name="statements">The <see cref="AstNodeList"/> of statements contained in this block.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ForBlockStatement( Expression variable, Expression start, Expression end, Expression step, AstNodeList statements, TextRange textRange )
			: base( statements, textRange )
			{
			// Initialize parameters
			this.Variable = variable;
			this.StartExpression = start;
			this.EndExpression = end;
			this.StepExpression = step;
		}

		/// <summary>
		/// Initializes a new instance of the <c>ForBlockStatement</c> class. 
		/// </summary>
		public ForBlockStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>ForBlockStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ForBlockStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.ForBlockStatement;
			}
		}

		/// <summary>
		/// Gets or sets the iterator.
		/// </summary>
		/// <value>The iterator.</value>
		public Expression Variable {
			get {
				return this.GetChildNode(ForBlockStatement.VariableContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, ForBlockStatement.VariableContextID);
			}
		}

		/// <summary>
		/// Gets or sets variable that gets initialized and incremented.  Can only be an integer or an integer identifier.
		/// </summary>
		/// <value>Variable that gets initialized and incremented.  Can only be an integer or an integer identifier.</value>
		public Expression StartExpression {
			get {
				return this.GetChildNode(ForBlockStatement.StartExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, ForBlockStatement.StartExpressionContextID);
			}
		}

		/// <summary>
		/// Gets or sets when to terminate.  Can only be an integer or an integer identifier.
		/// </summary>
		/// <value>When to terminate.  Can only be an integer or an integer identifier.</value>
		public Expression EndExpression {
			get {
				return this.GetChildNode(ForBlockStatement.EndExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, ForBlockStatement.EndExpressionContextID);
			}
		}

		/// <summary>
		/// Gets or sets the step value.  Null if none provided.
		/// </summary>
		/// <value>The step value.  Null if none provided.</value>
		public Expression StepExpression {
			get {
				return this.GetChildNode(ForBlockStatement.StepExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, ForBlockStatement.StepExpressionContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "FOR" );
			
			Expression variable = this.Variable;
			if ( variable != null )
				{
				sb.Append( " " );
				variable.BuildDisplayText(sb);
				
				Expression start = this.StartExpression;
				if ( start != null )
					{
					sb.Append( " = " );
					start.BuildDisplayText(sb);
					
					Expression end = this.EndExpression;
					if ( end != null )
						{
						sb.Append( " TO " );
						end.BuildDisplayText(sb);
					}
				}
				
				Expression step = this.StepExpression;
				if ( step != null )
					{
					sb.Append( " STEP " );
					step.BuildDisplayText(sb);
				}
			}
		}
		
		/// <summary>
		/// Gets the character offset at which to navigate when the editor's caret should jump to the text representation of the AST node.
		/// </summary>
		/// <value>The character offset at which to navigate when the editor's caret should jump to the text representation of the AST node.</value>
		public override int NavigationOffset
		{
			get
			{
				Expression variable = this.Variable;
				if ( (variable != null) && variable.HasStartOffset )
					{
					return variable.NavigationOffset;
				}
				else
					{
					return base.NavigationOffset;
				}
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>FOR</c>" );
			
			Expression variable = this.Variable;
			if ( variable != null )
				{
				sb.Append( " " );
				variable.BuildMarkupText(sb);
				
				Expression start = this.StartExpression;
				if ( start != null )
					{
					sb.Append( "<c=operator> = </c>" );
					start.BuildMarkupText(sb);
					
					Expression end = this.EndExpression;
					if ( end != null )
						{
						sb.Append( "<c=reserved> TO </c>" );
						end.BuildMarkupText(sb);
					}
				}
				
				Expression step = this.StepExpression;
				if ( step != null )
					{
					sb.Append( "<c=reserved> STEP </c>" );
					step.BuildMarkupText(sb);
				}
			}
		}

	}

}
