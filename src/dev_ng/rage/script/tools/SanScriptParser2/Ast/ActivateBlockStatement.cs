using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a HSMACTIVATE ENDHSMACTIVATE block.
	/// </summary>
	public partial class ActivateBlockStatement : SanScriptParser.BlockStatement {

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte ActivateBlockStatementContextIDBase = SanScriptParser.BlockStatement.BlockStatementContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>ActivateBlockStatement</c> class.
		/// </summary>
		/// <param name="statements">The <see cref="AstNodeList"/> of statements contained in this block.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ActivateBlockStatement( AstNodeList statements, TextRange textRange )
			: base( statements, textRange )
			{
		}

		/// <summary>
		/// Initializes a new instance of the <c>ActivateBlockStatement</c> class. 
		/// </summary>
		public ActivateBlockStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>ActivateBlockStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ActivateBlockStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.ActivateBlockStatement;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append("HSMACTIVATE");
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append("<c=reserved>HSMACTIVATE</c>");
		}

	}

}
