using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a CALL statement
	/// </summary>
	public partial class CallStatement : SanScriptParser.IdentifierStatement {

		private Expression	expression;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte CallStatementContextIDBase = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>CallStatement</c> class.
		/// </summary>
		/// <param name="expr">The <see cref="Expression"/> that is being called.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public CallStatement( Expression expr, TextRange textRange )
			: base( null, textRange )
			{
			// Initialize parameters
			this.Expression = expr;
			
			if ( expr != null )
				{
				// Set IsCall to false, we'll do that this in class.
				if ( expr.NodeType == SanScriptNodeType.ArrayAccessExpression )
					{
					ArrayAccessExpression access = expr as ArrayAccessExpression;
					
					base.Name = access.Name;
					access.IsCall = false;
				}
				else if ( expr.NodeType == SanScriptNodeType.StructMemberAccessExpression )
					{
					StructMemberAccessExpression access = expr as StructMemberAccessExpression;
					
					base.Name = access.Name;
					access.IsCall = false;
				}
				else if ( expr.NodeType == SanScriptNodeType.SubroutineAccessExpression )
					{
					SubroutineAccessExpression access = expr as SubroutineAccessExpression;
					
					base.Name = access.Name;
					access.IsCall = false;
				}
			}
		}

		/// <summary>
		/// Initializes a new instance of the <c>CallStatement</c> class. 
		/// </summary>
		public CallStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>CallStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public CallStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicEvent;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.CallStatement;
			}
		}

		/// <summary>
		/// Gets or sets the expression that is being called.
		/// </summary>
		/// <value>The expression that is being called.</value>
		public Expression Expression {
			get {
				return expression;
			}
			set {
				expression = value;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append("CALL ");
			this.Expression?.BuildDisplayText(sb);
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append("<c=reserved>CALL</c> ");
			this.Expression?.BuildMarkupText(sb);
		}

	}

}
