using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for an INTTOENUM expression.
	/// </summary>
	public partial class IntToEnumExpression : SanScriptParser.Expression {

		/// <summary>
		/// Gets the context ID for the name of the ENUM to convert to.
		/// </summary>
		public const byte EnumTypeContextID = SanScriptParser.Expression.ExpressionContextIDBase;

		/// <summary>
		/// Gets the context ID for the integer to convert.
		/// </summary>
		public const byte ExpressionToConvertContextID = SanScriptParser.Expression.ExpressionContextIDBase + 1;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte IntToEnumExpressionContextIDBase = SanScriptParser.Expression.ExpressionContextIDBase + 2;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>IntToEnumExpression</c> class.
		/// </summary>
		/// <param name="enumType">The <see cref="Identifier"/> that is the enumeration passed to the subroutine.</param>
		/// <param name="expr">The <see cref="Expression"/> passed to to the subroutine that is to be converted.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public IntToEnumExpression( Identifier enumType, Expression expr, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.EnumType = enumType;
			this.ExpressionToConvert = expr;
		}

		/// <summary>
		/// Initializes a new instance of the <c>IntToEnumExpression</c> class. 
		/// </summary>
		public IntToEnumExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>IntToEnumExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public IntToEnumExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.Keyword;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.IntToEnumExpression;
			}
		}

		/// <summary>
		/// Gets or sets the name of the ENUM to convert to.
		/// </summary>
		/// <value>The name of the ENUM to convert to.</value>
		public Identifier EnumType {
			get {
				return this.GetChildNode(IntToEnumExpression.EnumTypeContextID) as Identifier;
			}
			set {
				this.ChildNodes.Replace(value, IntToEnumExpression.EnumTypeContextID);
			}
		}

		/// <summary>
		/// Gets or sets the integer to convert.
		/// </summary>
		/// <value>The integer to convert.</value>
		public Expression ExpressionToConvert {
			get {
				return this.GetChildNode(IntToEnumExpression.ExpressionToConvertContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, IntToEnumExpression.ExpressionToConvertContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "INT_TO_ENUM" );
			
			Identifier enumType = this.EnumType;
			Expression expr = this.ExpressionToConvert;
			if ( (enumType != null) && (expr != null) )
				{
				sb.Append( "( " );
				enumType.BuildDisplayText(sb);
				sb.Append( ", " );
				expr.BuildDisplayText(sb);
				sb.Append( " )" );
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>INT_TO_ENUM</c>" );
			
			Identifier enumType = this.EnumType;
			Expression expr = this.ExpressionToConvert;
			if ( (enumType != null) && (expr != null) )
				{
				sb.Append( "<c=text>(</c>" );
				enumType.BuildMarkupText(sb);
				sb.Append( "<c=text>, </c>" );
				expr.BuildMarkupText(sb);
				sb.Append( "<c=text> )</c>" );
			}
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to see the expected parameters.
		/// </summary>
		/// <value>Text for the node that can be used for displaying a function declaration.</value>
		public static string DefinitionText
		{
			get
			{
				StringBuilder text = new StringBuilder();
				
				text.Append( "<enumTypeName> INT_TO_ENUM( ENUM enumTypeName, INT value )\n\n" );
				text.Append( "/// PURPOSE: Converts an integer to its enumTypeName-equivalent.\n" );
				text.Append( "/// PARAMS: \n" );
				text.Append( "///    enumTypeName - The type of ENUM to convert to.\n" );
				text.Append( "///    value - The integer to convert.\n" );
				text.Append( "/// RETURNS: An enumerant value of type enumTypeName." );
				
				return text.ToString();
			}
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to see a QuickInfo popup with the expected parameters
		/// </summary>
		/// <value>Text for the node that can be used for the IntelliPrompt function declaration.</value>
		public static string DefinitionMarkupText
		{
			get
			{
				string[] args = IntToEnumExpression.DefinitionArgumentsMarkupText;
				StringBuilder text = new StringBuilder();
				
				text.Append( "<c=user>&lt;enumTypeName&gt; </c>" );
				text.Append( "<c=reserved>INT_TO_ENUM</c>" );
				text.Append( "<c=text>( </c>" );
				text.Append( args[0] );
				text.Append( "<c=text>, </c>" );
				text.Append( args[1] );
				text.Append( "<c=text> )</c><br/><br/>" );
				
				text.Append( "<c=commentdelim>//</c><c=comment>/ PURPOSE: Converts an integer to its enumTypeName-equivalent.</c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/ PARAMS: </c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/&nbsp;&nbsp;&nbsp;&nbsp;enumTypeName - The type of ENUM to convert to.</c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/&nbsp;&nbsp;&nbsp;&nbsp;value - The integer to convert.</c><br/>" );
				text.Append( "<c=commentdelim>//</c><c=comment>/ RETURNS: An enumerant value of type enumTypeName.</c>" );
				
				return text.ToString();
			}
		}
		
		/// <summary>
		/// Since this is a built-in function, this will provide a way to retrieve the individual arguments for use in a QuickInfo popup
		/// </summary>
		/// <value>Markup text for each argument.</value>
		public static string[] DefinitionArgumentsMarkupText
		{
			get
			{
				string[] args = new string[2];
				
				StringBuilder text = new StringBuilder();
				text.Append( "<c=reserved>ENUM </c>" );
				text.Append( "<c=user>enumTypeName</c>" );
				args[0] = text.ToString();
				
				text = new StringBuilder();
				text.Append( "<c=reserved>INT </c>" );
				text.Append( "<c=text>value</c>" );
				args[1] = text.ToString();
				
				return args;
			}
		}

	}

}
