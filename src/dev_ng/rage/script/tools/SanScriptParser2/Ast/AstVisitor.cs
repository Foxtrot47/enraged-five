using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Provides the base class for an AST node visitor.
	/// </summary>
	/// <remarks>
	/// <para>
	/// This class supports the visitor pattern.
	/// </para>
	/// <para>
	/// Each visitor implements a <c>OnVisiting</c> and an <c>OnVisited</c> method for each AST node type.
	/// By default the <c>OnVisiting</c> method returns <c>true</c>, while the <c>OnVisited</c> method does nothing.
	/// </para>
	/// <para>
	/// The <c>OnVisiting</c> method should return <c>true</c> if its child nodes should be visited.
	/// If <c>true</c> is returned, the child nodes are visiting in order.
	/// After the child nodes are visited, the <c>OnVisited</c> method is called.
	/// If <c>OnVisiting</c> returns <c>false</c>, child node visits are not made and <c>OnVisited</c> is called directly.
	/// </para>
	/// <para>
	/// Additionally, the generic <see cref="AstVisitor.OnPreVisiting"/> method is called before any type-specific <c>OnVisiting</c> method is executed
	/// and the generic <see cref="AstVisitor.OnPostVisited"/> method is called after any type-specific <c>OnVisited</c> method is executed.
	/// </para>
	/// </remarks>
	public abstract partial class AstVisitor {

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// GENERIC VISIT PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Visits the <see cref="IAstNode"/> after the type-specific <c>OnVisited</c> method is executed.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnPostVisited(AstNode node) {}

		/// <summary>
		/// Visits the <see cref="IAstNode"/> before the type-specific <c>OnVisiting</c> method is executed.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node and its children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnPreVisiting(AstNode node) {
			return true;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// VISITING PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(Identifier node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(BooleanExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(IntegerExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(FloatExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(DefaultExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(NullExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(VectorExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(StringExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(FuncAddrExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(PreprocessorMacroExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(ArrayAccessExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(StructMemberAccessExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(SubroutineAccessExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(VariableExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(CountOfExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(EnumToIntExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(IntToEnumExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(IntToNativeExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(NativeToIntExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(SizeOfExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(HashExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(CatchExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(TimestepExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(ParenthesizedExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(BinaryExpression node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(EmptyStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(AssignmentStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(AdditiveStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(UsingStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(ThrowStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(HistoryStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(BreakStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(FallthruStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(ReturnStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(ExitStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(BreakLoopStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(ReLoopStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(GlobalsStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(PreprocessorMacroStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(ConstDefinitionStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(NativeTypeDeclarationStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(ForwardEnumDeclarationStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(ForwardStructDeclarationStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(LabelDeclarationStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(GotoStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(GotoStateStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(DefaultStateStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(CallStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(SubroutineAccessStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(NativeProcDeclarationStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(NativeEventDeclarationStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(NativeFuncDeclarationStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(TypedefProcDeclarationStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(TypedefFuncDeclarationStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(VariableNonArrayDeclarationStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(VariableArrayDeclarationStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(IfBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(WhileBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(RepeatBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(ForBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(CaseBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(SwitchBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(ActivateBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(DeactivateBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(GlobalsDefinitionBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(ScriptBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(StructDefinitionBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(StateDefinitionBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(EnumDefinitionBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(HashEnumDefinitionBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(StrictEnumDefinitionBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(ProcDefinitionBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(EventDefinitionBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(FuncDefinitionBlockStatement node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(MultiLineCommentBlock node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(PurposeCommentBlock node) {
			return true;
		}

		/// <summary>
		/// Visits the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		/// <returns>
		/// <c>true</c> if the node's children should be visited; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool OnVisiting(CompilationUnit node) {
			return true;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// VISITED PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(Identifier node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(BooleanExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(IntegerExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(FloatExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(DefaultExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(NullExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(VectorExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(StringExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(FuncAddrExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(PreprocessorMacroExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(ArrayAccessExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(StructMemberAccessExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(SubroutineAccessExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(VariableExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(CountOfExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(EnumToIntExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(IntToEnumExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(IntToNativeExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(NativeToIntExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(SizeOfExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(HashExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(CatchExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(TimestepExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(ParenthesizedExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(BinaryExpression node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(EmptyStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(AssignmentStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(AdditiveStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(UsingStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(ThrowStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(HistoryStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(BreakStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(FallthruStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(ReturnStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(ExitStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(BreakLoopStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(ReLoopStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(GlobalsStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(PreprocessorMacroStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(ConstDefinitionStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(NativeTypeDeclarationStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(ForwardEnumDeclarationStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(ForwardStructDeclarationStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(LabelDeclarationStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(GotoStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(GotoStateStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(DefaultStateStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(CallStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(SubroutineAccessStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(NativeProcDeclarationStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(NativeEventDeclarationStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(NativeFuncDeclarationStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(TypedefProcDeclarationStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(TypedefFuncDeclarationStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(VariableNonArrayDeclarationStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(VariableArrayDeclarationStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(IfBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(WhileBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(RepeatBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(ForBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(CaseBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(SwitchBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(ActivateBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(DeactivateBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(GlobalsDefinitionBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(ScriptBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(StructDefinitionBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(StateDefinitionBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(EnumDefinitionBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(HashEnumDefinitionBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(StrictEnumDefinitionBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(ProcDefinitionBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(EventDefinitionBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(FuncDefinitionBlockStatement node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(MultiLineCommentBlock node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(PurposeCommentBlock node) {}

		/// <summary>
		/// Ends the visit of the specified type of <see cref="IAstNode"/>.
		/// </summary>
		/// <param name="node">The node to visit.</param>
		public virtual void OnVisited(CompilationUnit node) {}

	}

}
