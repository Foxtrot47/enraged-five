using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a CONSTNATIVE definition statement.
	/// </summary>
	public partial class NativeConstDefinitionStatement : SanScriptParser.IdentifierStatement {

		/// <summary>
		/// Gets the context ID for the native type of the constant
		/// </summary>
		public const byte NativeTypeContextID = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase;

		/// <summary>
		/// Gets the context ID for the expression.
		/// </summary>
		public const byte ValueExpressionContextID = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase + 1;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte NativeConstDefinitionStatementContextIDBase = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase + 2;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>NativeConstDefinitionStatement</c> class.
		/// </summary>
		/// <param name="nativeType">The native type of the constant</param>
		/// <param name="name">The <see cref="Identifier"/> name given to the constant.</param>
		/// <param name="value">The <see cref="Expression"/> assigned to the constant.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public NativeConstDefinitionStatement( Identifier nativeType, Identifier name, Expression value, TextRange textRange )
			: base( name, textRange )
			{
			// Initialize parameters
			this.NativeType = nativeType;
			this.ValueExpression = value;
			
			if ( name != null )
				{
				name.DeclareType = Identifier.DeclarationType.User;
				name.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicConstant;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <c>NativeConstDefinitionStatement</c> class. 
		/// </summary>
		public NativeConstDefinitionStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>NativeConstDefinitionStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public NativeConstDefinitionStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicConstant;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.NativeConstDefinitionStatement;
			}
		}

		/// <summary>
		/// Gets or sets the native type of the constant
		/// </summary>
		/// <value>The native type of the constant</value>
		public Identifier NativeType {
			get {
				return this.GetChildNode(NativeConstDefinitionStatement.NativeTypeContextID) as Identifier;
			}
			set {
				this.ChildNodes.Replace(value, NativeConstDefinitionStatement.NativeTypeContextID);
			}
		}

		/// <summary>
		/// Gets or sets the expression.
		/// </summary>
		/// <value>The expression.</value>
		public Expression ValueExpression {
			get {
				return this.GetChildNode(NativeConstDefinitionStatement.ValueExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, NativeConstDefinitionStatement.ValueExpressionContextID);
			}
		}

		/// <summary>
		/// Gets text representing the node that can be used for display, such as in a document outline.
		/// </summary>
		/// <value>Text representing the node that can be used for display, such as in a document outline.</value>
		public override string DisplayText
		{
			get
			{
				StringBuilder text = new StringBuilder();
				
				text.Append( "CONST_NATIVE" );
				
				if ( this.NativeType != null )
					{
					text.Append( " " );
					text.Append( this.NativeType.DisplayText );
				}
				
				Identifier name = this.Name;
				if ( name != null )
					{
					text.Append( " " );
					text.Append( name.DisplayText );
					
					Expression value = this.ValueExpression;
					if ( value != null )
						{
						text.Append( " " );
						text.Append( value.DisplayText );
					}
				}
				
				string purpose = this.PurposeDisplayText;
				if ( purpose != null )
					{
					text.Append( "\n\n" );
					text.Append( purpose );
				}
				
				return text.ToString();
			}
		}
		
		/// <summary>
		/// Gets the marked up text that is appropriate for IntelliPrompt.
		/// </summary>
		/// <value>Text for the node that can be used for IntelliPrompt.</value>
		public override string MarkupText
		{
			get
			{
				StringBuilder text = new StringBuilder();
				
				text.Append( "<c=nativetype>CONST_NATIVE</c>" );
				
				if ( this.NativeType != null )
					{
					text.Append( " " );
					text.Append( this.NativeType.MarkupText );
				}
				
				Identifier name = this.Name;
				if ( name != null )
					{
					text.Append( " " );
					text.Append( name.MarkupText );
					
					Expression value = this.ValueExpression;
					if ( value != null )
						{
						text.Append( " " );
						text.Append( value.MarkupText );
					}
				}
				
				string purpose = this.PurposeMarkupText;
				if ( purpose != null )
					{
					text.Append( "<br/><br/>" );
					text.Append( purpose );
				}
				
				return text.ToString();
			}
		}

	}

}
