using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a NATIVE PROC declaration.
	/// </summary>
	public partial class NativeProcDeclarationStatement : SanScriptParser.NativeSubroutineDeclarationStatement {

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte NativeProcDeclarationStatementContextIDBase = SanScriptParser.NativeSubroutineDeclarationStatement.NativeSubroutineDeclarationStatementContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		private bool IsDebugOnly;
		
		/// <summary>
		/// Initializes a new instance of the <c>NativeProcDeclarationStatement</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Identifier"/> of the subroutine.</param>
		/// <param name="args">The <see cref="AstNodeList"/> of arguments to pass in.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public NativeProcDeclarationStatement( Identifier name, AstNodeList args, TextRange textRange, bool debugOnly )
			: base( name, args, textRange )
			{
			IsDebugOnly = debugOnly;
		}

		/// <summary>
		/// Initializes a new instance of the <c>NativeProcDeclarationStatement</c> class. 
		/// </summary>
		public NativeProcDeclarationStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>NativeProcDeclarationStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public NativeProcDeclarationStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.NativeProcDeclarationStatement;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			if ( IsDebugOnly )
				sb.Append( "NATIVE DEBUGONLY PROC " );
			else
				sb.Append( "NATIVE PROC " );
			
			base.BuildDisplayText(sb);
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			if ( IsDebugOnly )
				sb.Append( "<c=reserved>NATIVE DEBUGONLY PROC </c>" );
			else
				sb.Append( "<c=reserved>NATIVE PROC </c>" );
			
			base.BuildMarkupText(sb);
		}

	}

}
