using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a PROC or HSMEVENT subroutine call
	/// </summary>
	public partial class SubroutineAccessStatement : SanScriptParser.IdentifierStatement {

		private bool	isSuper;
		private bool	isCall;

		/// <summary>
		/// Gets the context ID for the collection of parameters, if any.
		/// </summary>
		public const byte ArgumentContextID = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte SubroutineAccessStatementContextIDBase = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>SubroutineAccessStatement</c> class.
		/// </summary>
		/// <param name="isSuper">If this is a call with HSMSUPER.</param>
		/// <param name="isCall">If this is a call with CALL.</param>
		/// <param name="name">The <see cref="Identifier"/> of the subroutine.</param>
		/// <param name="args">The <see cref="AstNodeList"/> of arguments passed in.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public SubroutineAccessStatement( bool isSuper, bool isCall, Identifier name, AstNodeList args, TextRange textRange )
			: base( name, textRange )
			{
			// Initialize parameters
			this.IsSuper = isSuper;
			this.IsCall = isCall;
			this.Arguments.AddRange( args.ToArray() );
			
			if ( name != null )
				{
				name.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicEvent;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <c>SubroutineAccessStatement</c> class. 
		/// </summary>
		public SubroutineAccessStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>SubroutineAccessStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public SubroutineAccessStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicEvent;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.SubroutineAccessStatement;
			}
		}

		/// <summary>
		/// Gets or sets true if the subroutine is called with the HSMSUPER prefix.
		/// </summary>
		/// <value>True if the subroutine is called with the HSMSUPER prefix.</value>
		public bool IsSuper {
			get {
				return isSuper;
			}
			set {
				isSuper = value;
			}
		}

		/// <summary>
		/// Gets or sets true if the subroutine is called with the CALL prefix.
		/// </summary>
		/// <value>True if the subroutine is called with the CALL prefix.</value>
		public bool IsCall {
			get {
				return isCall;
			}
			set {
				isCall = value;
			}
		}

		/// <summary>
		/// Gets the collection of parameters, if any.
		/// </summary>
		/// <value>The collection of statements.</value>
		public IAstNodeList Arguments {
			get {
				return new AstNodeListWrapper(this, SubroutineAccessStatement.ArgumentContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			if ( this.IsSuper )
				{
				sb.Append( "HSMSUPER." );
			}
			
			if ( this.IsCall )
				{
				sb.Append( "CALL " );
			}
			
			Identifier name = this.Name;
			if ( name != null )
				{
				name.BuildDisplayText(sb);
				sb.Append( "(" );
				
				IAstNodeList args = this.Arguments;
				if ( (args != null) && (args.Count > 0) )
					{
					sb.Append( " " );
					
					for ( int i = 0; i < args.Count; ++i )
						{
						if ( i > 0 )
							{
							sb.Append( ", " );
						}
						
						((AstNode)args[i]).BuildDisplayText(sb);
					}
					
					sb.Append( " " );
				}
				
				sb.Append( ")" );
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			if ( this.IsCall )
				{
				sb.Append( "<c=reserved>CALL</c>" );
			}
			
			if ( this.IsSuper )
				{
				sb.Append( "<c=reserved>HSMSUPER</c><c=text>.</c>" );
			}
			else
				{
				sb.Append( ' ' );
			}
			
			Identifier name = this.Name;
			if ( name != null )
				{
				name.BuildMarkupText(sb);
				sb.Append( "<c=text>(</c>" );
				
				IAstNodeList args = this.Arguments;
				if ( (args != null) && (args.Count > 0) )
					{
					sb.Append( " " );
					
					for ( int i = 0; i < args.Count; ++i )
						{
						if ( i > 0 )
							{
							sb.Append( "<c=text>, </c>" );
						}
						
						((AstNode)args[i]).BuildMarkupText(sb);
					}
					
					sb.Append( " " );
				}
				
				sb.Append( "<c=text>)</c>" );
			}
		}

	}

}
