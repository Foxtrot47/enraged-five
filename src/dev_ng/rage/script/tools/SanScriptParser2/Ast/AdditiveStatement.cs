using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for pre/post increment/decrement.
	/// </summary>
	public partial class AdditiveStatement : SanScriptParser.Statement {

		private bool	isAddition;

		/// <summary>
		/// Gets the context ID for the name/expression.
		/// </summary>
		public const byte NameContextID = SanScriptParser.Statement.StatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte AdditiveStatementContextIDBase = SanScriptParser.Statement.StatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>AdditiveStatement</c> class.
		/// </summary>
		/// <param name="isAdd">Specifies if this is an increment or a decrement operation.</param>
		/// <param name="expr">The <see cref="Expression"/> name of the variable in question.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public AdditiveStatement( bool isAdd, Expression name, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.IsAddition = isAdd;
			this.Name = name;
		}

		/// <summary>
		/// Initializes a new instance of the <c>AdditiveStatement</c> class. 
		/// </summary>
		public AdditiveStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>AdditiveStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public AdditiveStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.Operator;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.AdditiveStatement;
			}
		}

		/// <summary>
		/// Gets or sets true if ++, false if --.
		/// </summary>
		/// <value>True if ++, false if --.</value>
		public bool IsAddition {
			get {
				return isAddition;
			}
			set {
				isAddition = value;
			}
		}

		/// <summary>
		/// Gets or sets the name/expression.
		/// </summary>
		/// <value>The name/expression.</value>
		public Expression Name {
			get {
				return this.GetChildNode(AdditiveStatement.NameContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, AdditiveStatement.NameContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			Expression name = this.Name;
			if ( name != null )
				{
				name.BuildDisplayText(sb);
				
				if ( this.IsAddition )
					{
					sb.Append( "++" );
				}
				else
					{
					sb.Append( "--" );
				}
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			Expression name = this.Name;
			if ( name != null )
				{
				name.BuildMarkupText(sb);
				
				if ( this.IsAddition )
					{
					sb.Append( "<c=operator>++</c>" );
				}
				else
					{
					sb.Append( "<c=operator>--</c>" );
				}
			}
		}

	}

}
