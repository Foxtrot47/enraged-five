using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a SCRIPT ENDSCRIPT block.
	/// </summary>
	public partial class ScriptBlockStatement : SanScriptParser.BlockStatement {

		/// <summary>
		/// Gets the context ID for the collection of parameters, if any.
		/// </summary>
		public const byte ArgumentContextID = SanScriptParser.BlockStatement.BlockStatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte ScriptBlockStatementContextIDBase = SanScriptParser.BlockStatement.BlockStatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>ScriptBlockStatement</c> class.
		/// </summary>
		/// <param name="arg">The <see cref="Statement"/> of the argument to pass in.</param>
		/// <param name="statements">The <see cref="AstNodeList"/> of statements contained in this block.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ScriptBlockStatement( Statement arg, AstNodeList statements, TextRange textRange )
			: base( statements, textRange )
			{
			// Initialize parameters
			this.Argument = arg;
		}

		/// <summary>
		/// Initializes a new instance of the <c>ScriptBlockStatement</c> class. 
		/// </summary>
		public ScriptBlockStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>ScriptBlockStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ScriptBlockStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.ScriptBlockStatement;
			}
		}

		/// <summary>
		/// Gets or sets the collection of parameters, if any.
		/// </summary>
		/// <value>The collection of parameters, if any.</value>
		public Statement Argument {
			get {
				return this.GetChildNode(ScriptBlockStatement.ArgumentContextID) as Statement;
			}
			set {
				this.ChildNodes.Replace(value, ScriptBlockStatement.ArgumentContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "SCRIPT" );
			
			Statement arg = this.Argument;
			if ( arg != null )
				{
				sb.Append( "( " );
				arg.BuildDisplayText(sb);
				sb.Append( " )" );
			}
			
			string purpose = this.PurposeDisplayText;
			if ( purpose != null )
				{
				sb.Append( "\n\n" );
				sb.Append( purpose );
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>SCRIPT</c>" );
			
			Statement arg = this.Argument;
			if ( arg != null )
				{
				sb.Append( "<c=text>( </c>" );
				arg.BuildMarkupText(sb);
				sb.Append( "<c=text> )</c>" );
			}
			
			string purpose = this.PurposeMarkupText;
			if ( purpose != null )
				{
				sb.Append( "<br/><br/>" );
				sb.Append( purpose );
			}
		}

	}

}
