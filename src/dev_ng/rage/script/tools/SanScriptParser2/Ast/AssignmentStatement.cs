using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents an assignment statement which can include = += -= *= and /=.
	/// </summary>
	public partial class AssignmentStatement : SanScriptParser.Statement {

		private System.String	operatorString;

		/// <summary>
		/// Gets the context ID for the variable name/expression.
		/// </summary>
		public const byte NameContextID = SanScriptParser.Statement.StatementContextIDBase;

		/// <summary>
		/// Gets the context ID for the expression.
		/// </summary>
		public const byte ValueExpressionContextID = SanScriptParser.Statement.StatementContextIDBase + 1;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte AssignmentStatementContextIDBase = SanScriptParser.Statement.StatementContextIDBase + 2;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>AssignmentStatement</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Expression"/> of the variable being assigned to.</param>
		/// <param name="operatorString">The assignment operator string used.</param>
		/// <param name="value">The <see cref="Expression"/> assigned to variable.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public AssignmentStatement( Expression name, System.String operatorString, Expression value, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.Name = name;
			this.OperatorString = operatorString;
			this.ValueExpression = value;
		}

		/// <summary>
		/// Initializes a new instance of the <c>AssignmentStatement</c> class. 
		/// </summary>
		public AssignmentStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>AssignmentStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public AssignmentStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.AssignmentStatement;
			}
		}

		/// <summary>
		/// Gets or sets the variable name/expression.
		/// </summary>
		/// <value>The variable name/expression.</value>
		public Expression Name {
			get {
				return this.GetChildNode(AssignmentStatement.NameContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, AssignmentStatement.NameContextID);
			}
		}

		/// <summary>
		/// Gets or sets the token id of the operator used.
		/// </summary>
		/// <value>The token id of the operator used.</value>
		public System.String OperatorString {
			get {
				return operatorString;
			}
			set {
				operatorString = value;
			}
		}

		/// <summary>
		/// Gets or sets the expression.
		/// </summary>
		/// <value>The expression.</value>
		public Expression ValueExpression {
			get {
				return this.GetChildNode(AssignmentStatement.ValueExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, AssignmentStatement.ValueExpressionContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			Expression name = this.Name;
			if ( name != null )
				{
				name.BuildDisplayText(sb);
				
				if ( this.OperatorString != null )
					{
					sb.Append( " " );
					sb.Append( this.OperatorString );
				}
				
				Expression value = this.ValueExpression;
				if ( value != null )
					{
					sb.Append( " " );
					value.BuildDisplayText(sb);
				}
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			Expression name = this.Name;
			if ( name != null )
				{
				name.BuildMarkupText(sb);
				
				if ( this.OperatorString != null )
					{
					if ( (this.OperatorString == "AND") || (this.OperatorString == "OR") || (this.OperatorString == "NOT") )
						{
						sb.Append( "<c=wordoperator>" );
					}
					else
						{
						sb.Append( "<c=operator>" );
					}
					sb.Append( IntelliPrompt.EscapeMarkupText( this.OperatorString ) );
					sb.Append( "</c>" );
				}
				
				Expression value = this.ValueExpression;
				if ( value != null )
					{
					sb.Append( " " );
					value.BuildMarkupText(sb);
				}
			}
		}

	}

}
