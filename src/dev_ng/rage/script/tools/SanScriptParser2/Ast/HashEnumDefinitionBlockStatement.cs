using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for an HASH_ENUM ENDENUM block.
	/// </summary>
	public partial class HashEnumDefinitionBlockStatement : SanScriptParser.IdentifierBlockStatement {

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte HashEnumDefinitionBlockStatementContextIDBase = SanScriptParser.IdentifierBlockStatement.IdentifierBlockStatementContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>HashEnumDefinitionBlockStatement</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Identifier"/> of the new enum type.</param>
		/// <param name="statements">The <see cref="AstNodeList"/> of statements contained in this block.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public HashEnumDefinitionBlockStatement( Identifier name, AstNodeList statements, TextRange textRange )
			: base( name, statements, textRange )
			{
			// Initialize parameters
			if ( name != null )
				{
				name.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicEnumeration;
			}
			
			IAstNodeList enums = this.Statements;
			foreach ( IAstNode enumeration in enums )
				{
				if ( enumeration is VariableDeclarationStatement )
					{
					VariableDeclarationStatement var = enumeration as VariableDeclarationStatement;
					var.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.EnumerationItem;
					
					Identifier varName = var.Name;
					if ( varName != null )
						{
						varName.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.EnumerationItem;
					}
				}
			}
		}

		/// <summary>
		/// Initializes a new instance of the <c>HashEnumDefinitionBlockStatement</c> class. 
		/// </summary>
		public HashEnumDefinitionBlockStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>HashEnumDefinitionBlockStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public HashEnumDefinitionBlockStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicEnumeration;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.HashEnumDefinitionBlockStatement;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "HASH_ENUM" );
			
			Identifier name = this.Name;
			if ( name != null )
				{
				sb.Append( " " );
				name.BuildDisplayText(sb);
			}
			
			string purpose = this.PurposeDisplayText;
			if ( purpose != null )
				{
				sb.Append( "\n\n" );
				sb.Append( purpose );
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>HASH_ENUM</c>" );
			
			Identifier name = this.Name;
			if ( name != null )
				{
				sb.Append( " " );
				name.BuildMarkupText(sb);
			}
			
			string purpose = this.PurposeMarkupText;
			if ( purpose != null )
				{
				sb.Append( "<br/><br/>" );
				sb.Append( purpose );
			}
		}

	}

}
