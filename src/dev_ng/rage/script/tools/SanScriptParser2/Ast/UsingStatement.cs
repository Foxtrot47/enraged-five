using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a USING include directive.
	/// </summary>
	public partial class UsingStatement : SanScriptParser.Statement {

		/// <summary>
		/// Gets the context ID for the file to use.
		/// </summary>
		public const byte FileNameContextID = SanScriptParser.Statement.StatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte UsingStatementContextIDBase = SanScriptParser.Statement.StatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>UsingStatement</c> class.
		/// </summary>
		/// <param name="filename">The <see cref="Expression"/> that is the filename to include.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public UsingStatement( Expression filename, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.FileName = filename;
		}

		/// <summary>
		/// Initializes a new instance of the <c>UsingStatement</c> class. 
		/// </summary>
		public UsingStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>UsingStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public UsingStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.Assembly;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.UsingStatement;
			}
		}

		/// <summary>
		/// Gets or sets the file to use.
		/// </summary>
		/// <value>The file to use.</value>
		public Expression FileName {
			get {
				return this.GetChildNode(UsingStatement.FileNameContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, UsingStatement.FileNameContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "USING" );
			
			Expression filename = this.FileName;
			if ( filename != null )
				{
				sb.Append( " " );
				filename.BuildDisplayText(sb);
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>USING</c>" );
			
			Expression filename = this.FileName;
			if ( filename != null )
				{
				sb.Append( " " );
				filename.BuildMarkupText(sb);
			}
		}

	}

}
