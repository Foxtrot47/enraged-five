using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a base class for Block Statements that have a Name.
	/// </summary>
	public abstract partial class IdentifierBlockStatement : SanScriptParser.BlockStatement {

		/// <summary>
		/// Gets the context ID for the name.
		/// </summary>
		public const byte NameContextID = SanScriptParser.BlockStatement.BlockStatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte IdentifierBlockStatementContextIDBase = SanScriptParser.BlockStatement.BlockStatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>IdentifierBlockStatement</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Identifier"/> of the new data type.</param>
		/// <param name="statements">The <see cref="AstNodeList"/> of statements contained in this block.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public IdentifierBlockStatement( Identifier name, AstNodeList statements, TextRange textRange )
			: base( statements, textRange )
			{
			// Initialize parameters
			this.Name = name;
			
			if ( name != null )
				{
				name.DeclareType = Identifier.DeclarationType.User;
				name.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicStructure;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <c>IdentifierBlockStatement</c> class. 
		/// </summary>
		public IdentifierBlockStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>IdentifierBlockStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public IdentifierBlockStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		public Identifier Name {
			get {
				return this.GetChildNode(IdentifierBlockStatement.NameContextID) as Identifier;
			}
			set {
				this.ChildNodes.Replace(value, IdentifierBlockStatement.NameContextID);
			}
		}

		/// <summary>
		/// Gets the character offset at which to navigate when the editor's caret should jump to the text representation of the AST node.
		/// </summary>
		/// <value>The character offset at which to navigate when the editor's caret should jump to the text representation of the AST node.</value>
		public override int NavigationOffset
		{
			get
			{
				Identifier name = this.Name;
				if ( (name != null) && name.HasStartOffset )
					{
					return name.NavigationOffset;
				}
				else
					{
					return base.NavigationOffset;
				}
			}
		}
		
		internal override void BuildDisplayText(StringBuilder sb)
			{
			this.Name?.BuildDisplayText(sb);
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			this.Name?.BuildMarkupText(sb);
		}

	}

}
