using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a label declaration for use by a GOTO statement.
	/// </summary>
	public partial class LabelDeclarationStatement : SanScriptParser.IdentifierStatement {

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte LabelDeclarationStatementContextIDBase = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>LabelDeclarationStatement</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Identifier"/> of the label.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public LabelDeclarationStatement( Identifier name, TextRange textRange )
			: base( name, textRange )
			{
		}

		/// <summary>
		/// Initializes a new instance of the <c>LabelDeclarationStatement</c> class. 
		/// </summary>
		public LabelDeclarationStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>LabelDeclarationStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public LabelDeclarationStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.LabelDeclarationStatement;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			Identifier name = this.Name;
			if ( name != null )
				{
				name.BuildDisplayText(sb);
				sb.Append( ":" );
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			Identifier name = this.Name;
			if ( name != null )
				{
				name.BuildMarkupText(sb);
				sb.Append( "<c=text>:</c>" );
			}
		}

	}

}
