using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a #if, #if not, or #endif statement.
	/// </summary>
	public partial class PreprocessorMacroStatement : SanScriptParser.IdentifierStatement {

		private System.Int32	tokenID;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte PreprocessorMacroStatementContextIDBase = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>PreprocessorMacroStatement</c> class.
		/// </summary>
		/// <param name="tokenID">The tokenID of the macro.</param>
		/// <param name="name">The <see cref="Identifier"/> representing the name of the array.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public PreprocessorMacroStatement( int tokenID, Identifier name, TextRange textRange )
			: base( name, textRange )
			{
			// Initialize parameters
			this.TokenID = tokenID;
		}

		/// <summary>
		/// Initializes a new instance of the <c>PreprocessorMacroStatement</c> class. 
		/// </summary>
		public PreprocessorMacroStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>PreprocessorMacroStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public PreprocessorMacroStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.PreprocessorMacroStatement;
			}
		}

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		public System.Int32 TokenID {
			get {
				return tokenID;
			}
			set {
				tokenID = value;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			switch ( this.TokenID )
				{
				case SanScriptTokenID.PoundIf:
				sb.Append( "#IF" );
				break;
				case SanScriptTokenID.PoundEndif:
				sb.Append( "#ENDIF" );
				break;
			}
			
			sb.Append( " " );
			base.BuildDisplayText(sb);
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			switch ( this.TokenID )
				{
				case SanScriptTokenID.PoundIf:
				sb.Append( "<c=macro>#IF</c>" );
				break;
				case SanScriptTokenID.PoundEndif:
				sb.Append( "<c=macro>#ENDIF</c>" );
				break;
			}
			
			sb.Append( " " );
			base.BuildMarkupText(sb);
		}

	}

}
