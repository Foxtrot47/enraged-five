using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a CONSTINT or CONSTFLOAT definition statement.
	/// </summary>
	public partial class ConstDefinitionStatement : SanScriptParser.IdentifierStatement {

		private bool	isFloat;

		/// <summary>
		/// Gets the context ID for the expression.
		/// </summary>
		public const byte ValueExpressionContextID = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte ConstDefinitionStatementContextIDBase = SanScriptParser.IdentifierStatement.IdentifierStatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>ConstDefinitionStatement</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Identifier"/> name given to the constant.</param>
		/// <param name="isFloat">Whether this is an integer or a float constant.</param>
		/// <param name="value">The <see cref="Expression"/> assigned to the constant.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ConstDefinitionStatement( Identifier name, bool isFloat, Expression value, TextRange textRange )
			: base( name, textRange )
			{
			// Initialize parameters
			this.IsFloat = isFloat;
			this.ValueExpression = value;
			
			if ( name != null )
				{
				name.DeclareType = Identifier.DeclarationType.User;
				name.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicConstant;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <c>ConstDefinitionStatement</c> class. 
		/// </summary>
		public ConstDefinitionStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>ConstDefinitionStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ConstDefinitionStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicConstant;
			}
		}

		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.ConstDefinitionStatement;
			}
		}

		/// <summary>
		/// Gets or sets true if this is a CONSTFLOAT, false if CONSTINT.
		/// </summary>
		/// <value>True if this is a CONSTFLOAT, false if CONSTINT.</value>
		public bool IsFloat {
			get {
				return isFloat;
			}
			set {
				isFloat = value;
			}
		}

		/// <summary>
		/// Gets or sets the expression.
		/// </summary>
		/// <value>The expression.</value>
		public Expression ValueExpression {
			get {
				return this.GetChildNode(ConstDefinitionStatement.ValueExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, ConstDefinitionStatement.ValueExpressionContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			if ( this.IsFloat )
				{
				sb.Append( "CONST_FLOAT" );
			}
			else
				{
				sb.Append( "CONST_INT" );
			}
			
			Identifier name = this.Name;
			if ( name != null )
				{
				sb.Append( " " );
				name.BuildDisplayText(sb);
				
				Expression value = this.ValueExpression;
				if ( value != null )
					{
					sb.Append( " " );
					value.BuildDisplayText(sb);
				}
			}
			
			string purpose = this.PurposeDisplayText;
			if ( purpose != null )
				{
				sb.Append( "\n\n" );
				sb.Append( purpose );
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			if ( this.IsFloat )
				{
				sb.Append( "<c=nativetype>CONST_FLOAT</c>" );
			}
			else
				{
				sb.Append( "<c=nativetype>CONST_INT</c>" );
			}
			
			Identifier name = this.Name;
			if ( name != null )
				{
				sb.Append( " " );
				name.BuildMarkupText(sb);
				
				Expression value = this.ValueExpression;
				if ( value != null )
					{
					sb.Append( " " );
					value.BuildMarkupText(sb);
				}
			}
			
			string purpose = this.PurposeMarkupText;
			if ( purpose != null )
				{
				sb.Append( "<br/><br/>" );
				sb.Append( purpose );
			}
		}

	}

}
