using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a HSMSTATE ENDHSMSTATE block.
	/// </summary>
	public partial class StateDefinitionBlockStatement : SanScriptParser.IdentifierBlockStatement {

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte StateDefinitionBlockStatementContextIDBase = SanScriptParser.IdentifierBlockStatement.IdentifierBlockStatementContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>StateDefinitionBlockStatement</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Identifier"/> of the new state.</param>
		/// <param name="statements">The <see cref="AstNodeList"/> of statements contained in this block.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public StateDefinitionBlockStatement( Identifier name, AstNodeList statements, TextRange textRange )
			: base( name, statements, textRange )
			{
		}

		/// <summary>
		/// Initializes a new instance of the <c>StateDefinitionBlockStatement</c> class. 
		/// </summary>
		public StateDefinitionBlockStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>StateDefinitionBlockStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public StateDefinitionBlockStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.StateDefinitionBlockStatement;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "HSMSTATE" );
			
			Identifier name = this.Name;
			if ( name != null )
				{
				sb.Append( " " );
				name.BuildDisplayText(sb);
			}
			
			string purpose = this.PurposeDisplayText;
			if ( purpose != null )
				{
				sb.Append( "\n\n" );
				sb.Append( purpose );
			}
		}
		
		/// <summary>
		/// Gets the character offset at which to navigate when the editor's caret should jump to the text representation of the AST node.
		/// </summary>
		/// <value>The character offset at which to navigate when the editor's caret should jump to the text representation of the AST node.</value>
		public override int NavigationOffset
		{
			get
			{
				Identifier name = this.Name;
				if ( (name != null) && name.HasStartOffset )
					{
					return name.NavigationOffset;
				}
				else
					{
					return base.NavigationOffset;
				}
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=reserved>HSMSTATE</c>" );
			
			Identifier name = this.Name;
			if ( name != null )
				{
				sb.Append( " " );
				name.BuildMarkupText(sb);
			}
			
			string purpose = this.PurposeMarkupText;
			if ( purpose != null )
				{
				sb.Append( "<br/><br/>" );
				sb.Append( purpose );
			}
		}

	}

}
