using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a vector expression.
	/// </summary>
	public partial class VectorExpression : SanScriptParser.Expression {

		/// <summary>
		/// Gets the context ID for a list of the x, y and z values
		/// </summary>
		public const byte VectorValueContextID = SanScriptParser.Expression.ExpressionContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte VectorExpressionContextIDBase = SanScriptParser.Expression.ExpressionContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>VectorExpression</c> class.
		/// </summary>
		/// <param name="value">The <see cref="AstNodeList"/> that represents the value of the expression.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public VectorExpression( AstNodeList value, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			this.VectorValue.AddRange( value.ToArray() );
		}

		/// <summary>
		/// Initializes a new instance of the <c>VectorExpression</c> class. 
		/// </summary>
		public VectorExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>VectorExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public VectorExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.VectorExpression;
			}
		}

		/// <summary>
		/// Gets a list of the x, y and z values
		/// </summary>
		/// <value>The collection of statements.</value>
		public IAstNodeList VectorValue {
			get {
				return new AstNodeListWrapper(this, VectorExpression.VectorValueContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append( "<<" );
			
			IAstNodeList values = this.VectorValue;
			for ( int i = 0; i < values.Count; ++i )
				{
				if ( i > 0 )
					{
					sb.Append( ", " );
				}
				
				((AstNode)values[i]).BuildDisplayText(sb);
			}
			
			sb.Append( ">>" );
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append( "<c=text>" );
			sb.Append( "&lt;&lt;" );
			sb.Append( "</c>" );
			
			IAstNodeList values = this.VectorValue;
			for ( int i = 0; i < values.Count; ++i )
				{
				if ( i > 0 )
					{
					sb.Append( "<c=text>" );
					sb.Append( ", " );
					sb.Append( "</c>" );
				}
				
				((AstNode)values[i]).BuildMarkupText(sb);
			}
			
			sb.Append( "<c=text>" );
			sb.Append( "&gt;&gt;" );
			sb.Append( "</c>" );
		}

	}

}
