using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents an ampersand followed by a func or proc identifier.
	/// </summary>
	public partial class FuncAddrExpression : SanScriptParser.Expression {

		/// <summary>
		/// Gets the context ID for the expression representing the PROC or FUNC.
		/// </summary>
		public const byte ExprContextID = SanScriptParser.Expression.ExpressionContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte FuncAddrExpressionContextIDBase = SanScriptParser.Expression.ExpressionContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>FuncAddrExpression</c> class.
		/// </summary>
		/// <param name="expr">The <see cref="Expression"/> representing the PROC or FUNC.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public FuncAddrExpression( Expression expr, TextRange textRange )
			: base( textRange )
			{
			this.Expr = expr;
		}

		/// <summary>
		/// Initializes a new instance of the <c>FuncAddrExpression</c> class. 
		/// </summary>
		public FuncAddrExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>FuncAddrExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public FuncAddrExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.FuncAddrExpression;
			}
		}

		/// <summary>
		/// Gets or sets the expression representing the PROC or FUNC.
		/// </summary>
		/// <value>The expression representing the PROC or FUNC.</value>
		public Expression Expr {
			get {
				return this.GetChildNode(FuncAddrExpression.ExprContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, FuncAddrExpression.ExprContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append("&");
			this.Expr?.BuildDisplayText(sb);
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append("&amp;");
			this.Expr?.BuildMarkupText(sb);
		}

	}

}
