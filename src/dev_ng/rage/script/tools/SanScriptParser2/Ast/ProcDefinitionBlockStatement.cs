using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a PROC declaration and code block.
	/// </summary>
	public partial class ProcDefinitionBlockStatement : SanScriptParser.SubroutineDefinitionBlockStatement {

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte ProcDefinitionBlockStatementContextIDBase = SanScriptParser.SubroutineDefinitionBlockStatement.SubroutineDefinitionBlockStatementContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		bool IsDebugOnly;
		
		/// <summary>
		/// Initializes a new instance of the <c>ProcDefinitionBlockStatement</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Identifier"/> of the subroutine.</param>
		/// <param name="args">The <see cref="AstNodeList"/> of arguments to pass in.</param>
		/// <param name="statements">The <see cref="AstNodeList"/> of statements contained in this block.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ProcDefinitionBlockStatement( Identifier name, AstNodeList args, AstNodeList statements, TextRange textRange, bool isDebugOnly )
			: base( name, args, statements, textRange )
			{
			IsDebugOnly = isDebugOnly;
		}

		/// <summary>
		/// Initializes a new instance of the <c>ProcDefinitionBlockStatement</c> class. 
		/// </summary>
		public ProcDefinitionBlockStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>ProcDefinitionBlockStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ProcDefinitionBlockStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.ProcDefinitionBlockStatement;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			if ( IsDebugOnly )
				sb.Append( "DEBUGONLY PROC " );
			else
				sb.Append( "PROC " );
			
			base.BuildDisplayText(sb);
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			if ( IsDebugOnly )
				sb.Append( "<c=reserved>DEBUGONLY PROC </c>" );
			else
				sb.Append( "<c=reserved>PROC </c>" );
			
			base.BuildMarkupText(sb);
		}

	}

}
