using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents the base class for a Statement.
	/// </summary>
	public abstract partial class Statement : SanScriptParser.AstNode {

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte StatementContextIDBase = SanScriptParser.AstNode.AstNodeContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>Statement</c> class. 
		/// </summary>
		public Statement() {}

		/// <summary>
		/// Initializes a new instance of the <c>Statement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public Statement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		protected string PurposeDisplayText
		{
			get
			{
				// try to locate the PurposeCommentBlock for this.
				IAstNode parentNode = this.ParentNode;
				if ( parentNode != null )
					{
					IAstNodeList nodes = parentNode.ChildNodes;
					int indexOf = nodes.IndexOf( this );
					if ( indexOf > 1 )
						{
						AstNode node = nodes[indexOf-1] as AstNode;
						if ( node.NodeType == SanScriptNodeType.PurposeCommentBlock )
							{
							return node.DisplayText;
						}
					}
				}
				
				return null;
			}
		}
		
		protected string PurposeMarkupText
		{
			get
			{
				// try to locate the PurposeCommentBlock for this.
				IAstNode parentNode = this.ParentNode;
				if ( parentNode != null )
					{
					IAstNodeList nodes = parentNode.ChildNodes;
					int indexOf = nodes.IndexOf( this );
					if ( indexOf > 0 )
						{
						AstNode node = nodes[indexOf-1] as AstNode;
						if ( node.NodeType == SanScriptNodeType.PurposeCommentBlock )
							{
							return node.MarkupText;
						}
					}
				}
				
				return null;
			}
		}

	}

}
