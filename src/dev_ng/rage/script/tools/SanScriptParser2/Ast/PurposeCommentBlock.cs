using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class to hold a PURPOSE comments block.
	/// </summary>
	public partial class PurposeCommentBlock : SanScriptParser.BlockStatement {

		private string	purpose;
		private string	parameters;
		private string	returns;
		private string	notes;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte PurposeCommentBlockContextIDBase = SanScriptParser.BlockStatement.BlockStatementContextIDBase;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>PurposeCommentBlock</c> class.
		/// </summary>
		/// <param name="purpose">The PURPOSE comment text.</param>
		/// <param name="parameters">The PARAMS comment text.</param>
		/// <param name="returns">The RETURNS comment text.</param>
		/// <param name="notes">The NOTES comment text.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public PurposeCommentBlock( string purpose, string parameters, string returns, string notes, TextRange textRange )
			: base( null, textRange )
			{
			// Initialize parameters
			this.Purpose = purpose;
			this.Parameters = parameters;
			this.Returns = returns;
			this.Notes = notes;
		}

		/// <summary>
		/// Initializes a new instance of the <c>PurposeCommentBlock</c> class. 
		/// </summary>
		public PurposeCommentBlock() {}

		/// <summary>
		/// Initializes a new instance of the <c>PurposeCommentBlock</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public PurposeCommentBlock(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.PurposeCommentBlock;
			}
		}

		/// <summary>
		/// Gets or sets the PURPOSE line(s) of the comment block.
		/// </summary>
		/// <value>The PURPOSE line(s) of the comment block.</value>
		public string Purpose {
			get {
				return purpose;
			}
			set {
				purpose = value;
			}
		}

		/// <summary>
		/// Gets or sets the PARAMS line(s) of the comment block.
		/// </summary>
		/// <value>The PARAMS line(s) of the comment block.</value>
		public string Parameters {
			get {
				return parameters;
			}
			set {
				parameters = value;
			}
		}

		/// <summary>
		/// Gets or sets the RETURNS line(s) of the comment block.
		/// </summary>
		/// <value>The RETURNS line(s) of the comment block.</value>
		public string Returns {
			get {
				return returns;
			}
			set {
				returns = value;
			}
		}

		/// <summary>
		/// Gets or sets the NOTES line(s) of the comment block.
		/// </summary>
		/// <value>The NOTES line(s) of the comment block.</value>
		public string Notes {
			get {
				return notes;
			}
			set {
				notes = value;
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			if ( this.Purpose != null )
				{
				PrepareDisplayText(sb, "PURPOSE", this.Purpose);
				
				if ( this.Parameters != null )
					{
					sb.Append( "\n" );
					PrepareDisplayText(sb, "PARAMS", this.Parameters );
				}
				
				if ( this.Returns != null )
					{
					sb.Append( "\n" );
					PrepareDisplayText(sb, "RETURNS", this.Returns );
				}
				
				if ( this.Notes != null )
					{
					sb.Append( "\n" );
					PrepareDisplayText(sb, "NOTES", this.Notes );
				}
			}
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			if ( this.Purpose != null )
				{
				EscapeMarkupText(sb, "PURPOSE", this.Purpose );
				
				if ( this.Parameters != null )
					{
					sb.Append( "<br/>" );
					EscapeMarkupText(sb, "PARAMS", this.Parameters );
				}
				
				if ( this.Returns != null )
					{
					sb.Append( "<br/>" );
					EscapeMarkupText(sb, "RETURNS", this.Returns );
				}
				
				if ( this.Notes != null )
					{
					sb.Append( "<br/>" );
					EscapeMarkupText(sb, "NOTES", this.Notes );
				}
			}
		}
		
		/// <summary>
		/// Gets the text appropriate for displaying
		/// </summary>
		/// <value>Text for the item that can be used for display.</value>
		private void PrepareDisplayText(StringBuilder sb, string prefix, string text )
			{
			sb.Append( "/// " );
			sb.Append( prefix );
			sb.Append( ": " );
			
			string[] split = text.Split( new char[]{ '\n' } );
			for ( int i = 0; i < split.Length; ++i )
				{
				if ( i > 0 )
					{
					sb.Append( "\n///    " );
				}
				
				sb.Append( split[i].Trim() );
			}
		}
		
		/// <summary>
		/// Gets the marked up text that is appropriate for IntelliPrompt.
		/// </summary>
		/// <value>Text for the node that can be used for IntelliPrompt.</value>
		private void EscapeMarkupText(StringBuilder sb, string prefix, string text )
			{
			sb.Append( "<c=commentdelim>//</c>" );
			sb.Append( "<c=comment>/ ");
			sb.Append( prefix );
			sb.Append( ": " );
			
			string[] split = text.Split( new char[]{ '\n' } );
			for ( int i = 0; i < split.Length; ++i )
				{
				if ( i > 0 )
					{
					sb.Append( "<br/><c=commentdelim>//</c><c=comment>/&nbsp;&nbsp;&nbsp;&nbsp;" );
				}
				
				sb.Append( IntelliPrompt.EscapeMarkupText( split[i].Trim() ) );
				sb.Append( "</c>" );
			}
		}

	}

}
