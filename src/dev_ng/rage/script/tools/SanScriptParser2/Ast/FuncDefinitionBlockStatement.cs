using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a FUNC declaration and code block.
	/// </summary>
	public partial class FuncDefinitionBlockStatement : SanScriptParser.SubroutineDefinitionBlockStatement {

		/// <summary>
		/// Gets the context ID for the return type.
		/// </summary>
		public const byte ReturnTypeContextID = SanScriptParser.SubroutineDefinitionBlockStatement.SubroutineDefinitionBlockStatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte FuncDefinitionBlockStatementContextIDBase = SanScriptParser.SubroutineDefinitionBlockStatement.SubroutineDefinitionBlockStatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		private bool IsDebugOnly;
		/// <summary>
		/// Initializes a new instance of the <c>FuncDefinitionBlockStatement</c> class.
		/// </summary>
		/// <param name="returnType">The <see cref="Identifier"/> of the return type.</param>
		/// <param name="name">The <see cref="Identifier"/> of the subroutine.</param>
		/// <param name="args">The <see cref="AstNodeList"/> of arguments to pass in.</param>
		/// <param name="statements">The <see cref="AstNodeList"/> of statements contained in this block.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public FuncDefinitionBlockStatement( Identifier returnType, Identifier name, AstNodeList args, AstNodeList statements, TextRange textRange, bool isDebugOnly )
			: base( name, args, statements, textRange )
			{
			// Initialize parameters
			IsDebugOnly = isDebugOnly;
			this.ReturnType = returnType;
		}

		/// <summary>
		/// Initializes a new instance of the <c>FuncDefinitionBlockStatement</c> class. 
		/// </summary>
		public FuncDefinitionBlockStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>FuncDefinitionBlockStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public FuncDefinitionBlockStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.FuncDefinitionBlockStatement;
			}
		}

		/// <summary>
		/// Gets or sets the return type.
		/// </summary>
		/// <value>The return type.</value>
		public Identifier ReturnType {
			get {
				return this.GetChildNode(FuncDefinitionBlockStatement.ReturnTypeContextID) as Identifier;
			}
			set {
				this.ChildNodes.Replace(value, FuncDefinitionBlockStatement.ReturnTypeContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			if ( IsDebugOnly )
				sb.Append( "DEBUGONLY FUNC " );
			else
				sb.Append( "FUNC " );
			
			Identifier rtn = this.ReturnType;
			if ( rtn != null )
				{
				rtn.BuildDisplayText(sb);
				sb.Append( " " );
			}
			
			base.BuildDisplayText(sb);
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			if ( IsDebugOnly )
				sb.Append( "<c=reserved>DEBUGONLY FUNC </c>" );
			else
				sb.Append( "<c=reserved>FUNC </c>" );
			
			Identifier rtn = this.ReturnType;
			if ( rtn != null )
				{
				rtn.BuildMarkupText(sb);
				sb.Append( " " );
			}
			
			base.BuildMarkupText(sb);
		}

	}

}
