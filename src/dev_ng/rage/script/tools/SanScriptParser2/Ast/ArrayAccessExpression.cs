using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for array access expressions.
	/// </summary>
	public partial class ArrayAccessExpression : SanScriptParser.IdentifierExpression {

		private bool	isCall;

		/// <summary>
		/// Gets the context ID for the expression that accesses an element of the array.
		/// </summary>
		public const byte AccessExpressionContextID = SanScriptParser.IdentifierExpression.IdentifierExpressionContextIDBase;

		/// <summary>
		/// Gets the context ID for the sub expression that acts on the element of the array.
		/// </summary>
		public const byte AccessSubExpressionContextID = SanScriptParser.IdentifierExpression.IdentifierExpressionContextIDBase + 1;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte ArrayAccessExpressionContextIDBase = SanScriptParser.IdentifierExpression.IdentifierExpressionContextIDBase + 2;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>ArrayAccessExpression</c> class.
		/// </summary>
		/// <param name="isCall">If this is a call with CALL.</param>
		/// <param name="expr">The <see cref="Expression"/> that accesses the array.</param>
		/// <param name="subExpr">The <see cref="Expression"/> that acts on the element of the array.</param>
		/// <param name="name">The <see cref="Identifier"/> representing the name of the array.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ArrayAccessExpression( bool isCall, Expression expr, Expression subExpr, Identifier name, TextRange textRange )
			: base( name, textRange )
			{
			// Initialize parameters
			this.IsCall = isCall;
			this.AccessExpression = expr;
			this.AccessSubExpression = subExpr;
			
			// bubble up IsCall
			if ( !this.IsCall && (this.AccessSubExpression != null) )
				{
				if ( this.AccessSubExpression.NodeType == SanScriptNodeType.ArrayAccessExpression )
					{
					ArrayAccessExpression access = this.AccessSubExpression as ArrayAccessExpression;
					if ( access.IsCall )
						{
						this.IsCall = true;
					}
				}
				else if ( this.AccessSubExpression.NodeType == SanScriptNodeType.StructMemberAccessExpression )
					{
					StructMemberAccessExpression access = this.AccessSubExpression as StructMemberAccessExpression;
					if ( access.IsCall )
						{
						this.IsCall = true;
					}
				}
				else if ( this.AccessSubExpression.NodeType == SanScriptNodeType.SubroutineAccessExpression )
					{
					SubroutineAccessExpression access = this.AccessSubExpression as SubroutineAccessExpression;
					if ( access.IsCall )
						{
						this.IsCall = true;
					}
				}
			}
			
			if ( (name != null) && this.IsCall )
				{
				name.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicEvent;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <c>ArrayAccessExpression</c> class. 
		/// </summary>
		public ArrayAccessExpression() {}

		/// <summary>
		/// Initializes a new instance of the <c>ArrayAccessExpression</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public ArrayAccessExpression(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.ArrayAccessExpression;
			}
		}

		/// <summary>
		/// Gets or sets true if the subroutine is called with the CALL prefix.
		/// </summary>
		/// <value>True if the subroutine is called with the CALL prefix.</value>
		public bool IsCall {
			get {
				return isCall;
			}
			set {
				isCall = value;
			}
		}

		/// <summary>
		/// Gets or sets the expression that accesses an element of the array.
		/// </summary>
		/// <value>The expression that accesses an element of the array.</value>
		public Expression AccessExpression {
			get {
				return this.GetChildNode(ArrayAccessExpression.AccessExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, ArrayAccessExpression.AccessExpressionContextID);
			}
		}

		/// <summary>
		/// Gets or sets the sub expression that acts on the element of the array.
		/// </summary>
		/// <value>The sub expression that acts on the element of the array.</value>
		public Expression AccessSubExpression {
			get {
				return this.GetChildNode(ArrayAccessExpression.AccessSubExpressionContextID) as Expression;
			}
			set {
				this.ChildNodes.Replace(value, ArrayAccessExpression.AccessSubExpressionContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			if ( this.IsCall )
				{
				sb.Append( "CALL " );
			}
			
			base.BuildDisplayText(sb);
			
			Expression expr = this.AccessExpression;
			if ( expr != null )
				{
				sb.Append( "[" );
				expr.BuildDisplayText(sb);
				sb.Append( "]" );
			}
			
			this.AccessSubExpression?.BuildDisplayText(sb);
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			if ( this.IsCall )
				{
				sb.Append( "<c=reserved>CALL</c> " );
			}
			
			base.BuildMarkupText(sb);
			
			Expression expr = this.AccessExpression;
			if ( expr != null )
				{
				sb.Append( "<c=text>[</c>" );
				expr.BuildMarkupText(sb);
				sb.Append( "<c=text>]</c>" );
			}
			
			this.AccessSubExpression?.BuildMarkupText(sb);
		}

	}

}
