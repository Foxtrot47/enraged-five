using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a block of statements.
	/// </summary>
	public abstract partial class BlockStatement : SanScriptParser.Statement, ICollapsibleNode {

		/// <summary>
		/// Gets the context ID for the collection of statements.
		/// </summary>
		public const byte StatementContextID = SanScriptParser.Statement.StatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte BlockStatementContextIDBase = SanScriptParser.Statement.StatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>BlockStatement</c> class.
		/// </summary>
		/// <param name="statements">The <see cref="AstNodeList"/> of statements contained in this block.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public BlockStatement( AstNodeList statements, TextRange textRange )
			: this( textRange )
			{
			// Initialize parameters
			if ( statements != null )
				{
				this.Statements.AddRange( statements.ToArray() );
			}
		}

		/// <summary>
		/// Initializes a new instance of the <c>BlockStatement</c> class. 
		/// </summary>
		public BlockStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>BlockStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public BlockStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Gets the image index that is applicable for displaying this node in a user interface control.
		/// </summary>
		/// <value>The image index that is applicable for displaying this node in a user interface control.</value>
		public override int ImageIndex {
			get {
				return (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.Namespace;
			}
		}

		/// <summary>
		/// Gets the collection of statements.
		/// </summary>
		/// <value>The collection of statements.</value>
		public IAstNodeList Statements {
			get {
				return new AstNodeListWrapper(this, BlockStatement.StatementContextID);
			}
		}

		/// <summary>
		/// Gets the offset at which the outlining node ends.
		/// </summary>
		/// <value>The offset at which the outlining node ends.</value>
		int ICollapsibleNode.EndOffset
		{
			get
			{
				return this.TextRange.EndOffset;
			}
		}
		
		/// <summary>
		/// Gets whether the node is collapsible.
		/// </summary>
		/// <value>
		/// <c>true</c> if the node is collapsible; otherwise, <c>false</c>.
		/// </value>
		bool ICollapsibleNode.IsCollapsible
		{
			get
			{
				return (this.StartOffset != -1) && ((this.StartOffset < this.EndOffset) || (this.EndOffset == this.StartOffset - 1));
			}
		}
		
		/// <summary>
		/// Gets the offset at which the outlining node starts.
		/// </summary>
		/// <value>The offset at which the outlining node starts.</value>
		int ICollapsibleNode.StartOffset
		{
			get
			{
				return this.TextRange.StartOffset;
			}
		}
		
		/// <summary>
		/// Gets whether the outlining indicator should be visible for the node.
		/// </summary>
		/// <value>
		/// <c>true</c> if the outlining indicator should be visible for the node; otherwise, <c>false</c>.
		/// </value>
		bool IOutliningNodeParseData.IndicatorVisible
		{
			get
			{
				return true;
			}
		}
		
		/// <summary>
		/// Gets whether the outlining node is for a language transition.
		/// </summary>
		/// <value>
		/// <c>true</c> if the outlining node is for a language transition; otherwise, <c>false</c>.
		/// </value>
		bool IOutliningNodeParseData.IsLanguageTransition
		{
			get
			{
				return false;
			}
		}

	}

}
