using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	/// <summary>
	/// Represents a class for a GLOBALS TRUE statement.
	/// </summary>
	public partial class GlobalsStatement : SanScriptParser.Statement {

		/// <summary>
		/// Gets the context ID for the name of the global.
		/// </summary>
		public const byte NameContextID = SanScriptParser.Statement.StatementContextIDBase;

		/// <summary>
		/// Gets the minimum context ID that should be used in your code for AST nodes inheriting this class.
		/// </summary>
		/// <remarks>
		/// Base all your context ID constants off of this value.
		/// </remarks>
		protected const byte GlobalsStatementContextIDBase = SanScriptParser.Statement.StatementContextIDBase + 1;

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// OBJECT
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Initializes a new instance of the <c>GlobalsStatement</c> class.
		/// </summary>
		/// <param name="name">The <see cref="Identifier"/> of the global.</param>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public GlobalsStatement( Identifier name, TextRange textRange )
			: base( textRange )
			{
			// Initialize parameters
			this.Name = name;
		}

		/// <summary>
		/// Initializes a new instance of the <c>GlobalsStatement</c> class. 
		/// </summary>
		public GlobalsStatement() {}

		/// <summary>
		/// Initializes a new instance of the <c>GlobalsStatement</c> class. 
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the AST node.</param>
		public GlobalsStatement(TextRange textRange) : base(textRange) {}

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// PUBLIC PROCEDURES
		/////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Accepts the specified visitor for visiting this node.
		/// </summary>
		/// <param name="visitor">The visitor to accept.</param>
		/// <remarks>This method is part of the visitor design pattern implementation.</remarks>
		protected override void AcceptCore(AstVisitor visitor) {
			if (visitor.OnVisiting(this)) {
				// Visit children
				if (this.ChildNodeCount > 0)
					this.AcceptChildren(visitor, this.ChildNodes);
			}
			visitor.OnVisited(this);
		}
		
		/// <summary>
		/// Gets the <see cref="SanScriptNodeType"/> that identifies the type of node.
		/// </summary>
		/// <value>The <see cref="SanScriptNodeType"/> that identifies the type of node.</value>
		public override SanScriptNodeType NodeType { 
			get {
				return SanScriptNodeType.GlobalsStatement;
			}
		}

		/// <summary>
		/// Gets or sets the name of the global.
		/// </summary>
		/// <value>The name of the global.</value>
		public Identifier Name {
			get {
				return this.GetChildNode(GlobalsStatement.NameContextID) as Identifier;
			}
			set {
				this.ChildNodes.Replace(value, GlobalsStatement.NameContextID);
			}
		}

		internal override void BuildDisplayText(StringBuilder sb)
			{
			sb.Append("GLOBALS ");
			this.Name?.BuildDisplayText(sb);
			sb.Append(" TRUE");
		}
		
		internal override void BuildMarkupText(StringBuilder sb)
			{
			sb.Append("<c=reserved>GLOBALS </c>");
			this.Name?.BuildMarkupText(sb);
			sb.Append(" TRUE");
		}

	}

}
