using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using ActiproSoftware.SyntaxEditor;
using ActiproSoftware.SyntaxEditor.ParserGenerator;

namespace SanScriptParser
{
    /// <summary>
    /// Represents a <c>Simple</c> recursive descent lexical parser implementation.
    /// </summary>
    internal class SanScriptRecursiveDescentLexicalParser : MergableRecursiveDescentLexicalParser
    {
        /// <summary>
        /// Initializes a new instance of the <c>SanScriptRecursiveDescentLexicalParser</c> class.
        /// </summary>
        /// <param name="language">The <see cref="SanScriptSyntaxLanguage"/> to use.</param>
        /// <param name="manager">The <see cref="MergableLexicalParserManager"/> to use for coordinating merged languages.</param>
        public SanScriptRecursiveDescentLexicalParser( SanScriptSyntaxLanguage language, MergableLexicalParserManager manager ) : base( language, manager ) { }

        #region Variables
        private int m_bracketLevel = 0;
        private int m_previousTokenID = SanScriptTokenID.Invalid;
        #endregion

        #region MergableRecursiveDescentLexicalParser Overrides
        /// <summary>
        /// Returns the next <see cref="IToken"/> and seeks past it.
        /// </summary>
        /// <returns>The next <see cref="IToken"/>.</returns>
        protected override IToken GetNextTokenCore()
        {
            IToken token = null;
            int startOffset = this.TextBufferReader.Offset;

            while ( !this.IsAtEnd )
            {
                // Get the next token
                token = this.Manager.GetNextToken();

                if ( !token.HasFlag( LexicalParseFlags.LanguageStart ) )
                {
                    switch ( token.ID )
                    {
                        case SanScriptTokenID.LineTerminator:
                        case SanScriptTokenID.Whitespace:
                            break;  // Consume non-significant token
                        case SanScriptTokenID.SingleLineCommentStart:
                        case SanScriptTokenID.SingleLineComment:
                        case SanScriptTokenID.SingleLineCommentEnd:
                            m_previousTokenID = token.ID;
                            if ( m_bracketLevel == 0 )
                            {
                                return token;   // Return the significant token
                            }
                            break;  // Consume non-significant token
                        case SanScriptTokenID.MultiLineCommentStart:
                        case SanScriptTokenID.MultiLineComment:
                        case SanScriptTokenID.MultiLineCommentEnd:
                            m_previousTokenID = token.ID;
                            return token;
                        default:
                            if ( SanScriptToken.GetIsPairedStart( token.ID ) )
                            {
                                if ( (token.ID != SanScriptTokenID.GLOBALS)
                                    && (token.ID != SanScriptTokenID.HSMSTATE)
                                    && (m_previousTokenID != SanScriptTokenID.NATIVE) 
                                    && (m_previousTokenID != SanScriptTokenID.FORWARD) )
                                {
                                    ++m_bracketLevel;
                                }
                            }
                            
                            if ( SanScriptToken.GetIsPairedEnd( token.ID ) )
                            {
                                if ( (token.ID != SanScriptTokenID.ENDGLOBALS)
                                    && (token.ID != SanScriptTokenID.ENDHSMSTATE) )
                                {
                                    --m_bracketLevel;

                                    if ( m_bracketLevel < 0 )
                                    {
                                        m_bracketLevel = 0;
                                    }
                                }
                            }

                            m_previousTokenID = token.ID;
                            return token;   // Return the significant token
                    }                    
                }
                else
                {
                    m_previousTokenID = token.ID;

                    // Return the significant token (which is in a different language)
                    return token;
                }

                // Advance the start offset
                startOffset = this.TextBufferReader.Offset;
            }

            // Return an end of document token
            if ( this.Token != null )
            {
                IToken t = this.Language.CreateDocumentEndToken( startOffset, this.Token.LexicalState );
                m_previousTokenID = t.ID;
                return t;
            }
            else
            {
                IToken t = this.Language.CreateDocumentEndToken( startOffset, this.Language.DefaultLexicalState );
                m_previousTokenID = t.ID;
                return t;
            }
        }
        #endregion
    }
}
