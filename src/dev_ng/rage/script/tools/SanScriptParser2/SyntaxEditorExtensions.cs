﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ActiproSoftware.SyntaxEditor;
using ragScriptEditorShared;

namespace SanScriptParser
{
    /// <summary>
    /// Extension methods to hack around broken Actipro software
    /// </summary>
    public static class SyntaxEditorExtensions
    {
        /// <summary>
        /// Use this extension method instead of calling Add/AddRange/Remove/RemoveAt whenever possible.
        /// First of all, IntelliPromptMemberList has extremely inefficient implementations because they manually refresh the UI each time.
        /// So unless you're removing one or two items it's always going to be more efficient to clear the list and use AddRange.
        /// Secondly, if you clear the list while it's shown, the editor likes to crash. So this also ensures that if you've emptied the list
        /// that it is hidden.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="itemList"></param>
        public static void SetListSafe(this IntelliPromptMemberList list, IEnumerable<IntelliPromptMemberListItem> itemList)
        {
            list.Clear();
            list.AddRange(itemList.ToArray());

            if (list.Count <= 0)
            {
                list.Abort();
            }
        }

        /// <summary>
        /// Use this instead of Remove, as it will auto-close the list if it has no more items. Otherwise, if the list is open and you have no more items, a crash can occur.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="item"></param>
        public static void RemoveSafe(this IntelliPromptMemberList list, IntelliPromptMemberListItem item)
        {
            list.Remove(item);

            if (list.Count <= 0)
            {
                list.Abort();
            }
        }

        /// <summary>
        /// Use this instead of RemoveAt, as it will auto-close the list if it has no more items. Otherwise, if the list is open and you have no more items, a crash can occur.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="inex"></param>
        public static void RemoveAtSafe(this IntelliPromptMemberList list, int index)
        {
            list.RemoveAt(index);

            if (list.Count <= 0)
            {
                list.Abort();
            }
        }

        /// <summary>
        /// InsertSurroundingText sometimes throws exceptions, mainly caused by race conditions with the parser service. This disables the parser service while adding text, and
        /// ignores those exceptions since they're probably not the end of the world.
        ///
        /// Original documentation:
        /// Inserts text into the document that surrounds the currently selected text in the view, using a DocumentModificationType.Typing modification type.
        /// </summary>
        /// <param name="view">The view.</param>
        /// <param name="preString">The text to insert before the current selection</param>
        /// <param name="postString">The text to insert after the current selection.</param>
        /// <returns>true if a text modification was allowed to occur; otherwise, false</returns>
        public static bool InsertSurroundingTextSafe(this EditorView view, String preString, String postString)
        {
            try
            {
                bool wasRunning = view.SyntaxEditor.Document.SemanticParsingEnabled;
                if (wasRunning)
                {
                    view.SyntaxEditor.Document.SemanticParsingEnabled = false;
                }

                bool result = view.InsertSurroundingText(preString, postString);

                if (wasRunning)
                {
                    view.SyntaxEditor.Document.SemanticParsingEnabled = true;
                }

                return result;
            }
            catch (Exception e)
            {
                ApplicationSettings.Log.ToolException(e, "Caught an exception in InsertSurroundingText. Oh well, who cares.");
                return false;
            }
        }
    }
}
