using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using ActiproSoftware.SyntaxEditor;
using ActiproSoftware.SyntaxEditor.ParserGenerator;

namespace SanScriptParser
{
    /// <summary>
    /// Represents a <c>Simple</c> lexical parser implementation.
    /// </summary>
    internal class SanScriptLexicalParser : IMergableLexicalParser
    {        
        /// <summary>
        /// Initializes a new instance of the <c>SanScriptLexicalParser</c> class.
        /// </summary>
        public SanScriptLexicalParser()
        {
            // Initialize keywords
            for ( int index = SanScriptTokenID.KeywordStart + 1; index < SanScriptTokenID.KeywordEnd; ++index )
            {
                switch ( index )
                {
                    case SanScriptTokenID.CONSTINT:
                        m_keywords.Add( "CONST_INT", index );
                        break;
                    case SanScriptTokenID.CONSTFLOAT:
                        m_keywords.Add( "CONST_FLOAT", index );
                        break;
                    case SanScriptTokenID.ENUMTOINTdatatype:
                        m_keywords.Add( SanScriptLexicalParser.EnumToIntDatatypeName, index );
                        break;
                    case SanScriptTokenID.TEXTLABEL:
                        m_keywords.Add( "TEXT_LABEL", index );
                        break;
                    case SanScriptTokenID.TEXTLABELTHREE:
                        m_keywords.Add( "TEXT_LABEL_3", index );
                        break;
                    case SanScriptTokenID.TEXTLABELSEVEN:
                        m_keywords.Add( "TEXT_LABEL_7", index );
                        break;
                    case SanScriptTokenID.TEXTLABELFIFTEEN:
                        m_keywords.Add( "TEXT_LABEL_15", index );
                        break;
                    case SanScriptTokenID.TEXTLABELTWENTYTHREE:
                        m_keywords.Add( "TEXT_LABEL_23", index );
                        break;
                    case SanScriptTokenID.TEXTLABELTHIRTYONE:
                        m_keywords.Add( "TEXT_LABEL_31", index );
                        break;
                    case SanScriptTokenID.TEXTLABELSIXTYTHREE:
                        m_keywords.Add( "TEXT_LABEL_63", index );
                        break;
                    case SanScriptTokenID.COUNTOF:
                        m_keywords.Add( "COUNT_OF", index );
                        break;
                    case SanScriptTokenID.ENUMTOINT:
                        m_keywords.Add( "ENUM_TO_INT", index );
                        break;
                    case SanScriptTokenID.INTTOENUM:
                        m_keywords.Add( "INT_TO_ENUM", index );
                        break;
                    case SanScriptTokenID.INTTONATIVE:
                        m_keywords.Add( "INT_TO_NATIVE", index );
                        break;
                    case SanScriptTokenID.NATIVETOINT:
                        m_keywords.Add( "NATIVE_TO_INT", index );
                        break;
                    case SanScriptTokenID.SIZEOF:
                        m_keywords.Add( "SIZE_OF", index );
                        break;
                    case SanScriptTokenID.PoundIf:
                        m_keywords.Add("#IF", index);
                        break;
                    case SanScriptTokenID.PoundEndif:
                        m_keywords.Add( "#ENDIF", index );
                        break;
                    default:
                        if ( (index != SanScriptTokenID.NativeTypeStart) && (index != SanScriptTokenID.NativeTypeEnd)
                            && (index != SanScriptTokenID.WordOperatorStart) && (index != SanScriptTokenID.WordOperatorEnd) )
                        {
                            m_keywords.Add( SanScriptTokenID.GetTokenKey( index ).ToUpper(), index );
                        }
                        break;
                }
            }
        }

        #region Variables
        private Dictionary<string, int> m_keywords = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
        #endregion

        #region Properties
        public Dictionary<string, int> Keywords
        {
            get
            {
                return m_keywords;
            }
        }
        #endregion

        #region Const Properties
        public const string EnumToIntDatatypeName = "ENUM_TO_INT (datatype)";
        public const string EnumToIntDatatypePreText = "ENUM_TO_INT";
        #endregion

        #region IMergableLexicalParser Interface
        /// <summary>
        /// Returns a single-character <see cref="ITokenLexicalParseData"/> representing the lexical parse data for the
        /// default token in the <see cref="ILexicalState"/> and seeks forward one position in the <see cref="ITextBufferReader"/>
        /// </summary>
        /// <param name="reader">An <see cref="ITextBufferReader"/> that is reading a text source.</param>
        /// <param name="lexicalState">The <see cref="ILexicalState"/> that specifies the current context.</param>
        /// <returns>The <see cref="ITokenLexicalParseData"/> for default text in the <see cref="ILexicalState"/>.</returns>
        public ITokenLexicalParseData GetLexicalStateDefaultTokenLexicalParseData( ITextBufferReader reader, ILexicalState lexicalState )
        {
            reader.Read();
            return new LexicalStateAndIDTokenLexicalParseData( lexicalState, (byte)lexicalState.DefaultTokenID );
        }

        /// <summary>
        /// Performs a lexical parse to return the next <see cref="ITokenLexicalParseData"/> 
        /// from a <see cref="ITextBufferReader"/> and seeks past it if there is a match.
        /// </summary>
        /// <param name="reader">An <see cref="ITextBufferReader"/> that is reading a text source.</param>
        /// <param name="lexicalState">The <see cref="ILexicalState"/> that specifies the current context.</param>
        /// <param name="lexicalParseData">Returns the next <see cref="ITokenLexicalParseData"/> from a <see cref="ITextBufferReader"/>.</param>
        /// <returns>A <see cref="MatchType"/> indicating the type of match that was made.</returns>
        public MatchType GetNextTokenLexicalParseData( ITextBufferReader reader, ILexicalState lexicalState, ref ITokenLexicalParseData lexicalParseData )
        {
            // Initialize
            int tokenID = SanScriptTokenID.Invalid;

            if ( lexicalState.ID == SanScriptLexicalStateID.StringState )
            {
                tokenID = ParseString( reader );
            }
            else if ( lexicalState.ID == SanScriptLexicalStateID.CommentState )
            {
                tokenID = ParseSingleLineComment( reader );
            }
            else if ( lexicalState.ID == SanScriptLexicalStateID.MultiLineCommentState )
            {
                tokenID = ParseMultiLineComment( reader );
            }
            else
            {
                // Get the next character
                char ch = reader.Read();

                // If the character is a letter or digit...
                if ( Char.IsLetter( ch ) || ch == '_' )
                {
                    // Parse the identifier
                    tokenID = this.ParseIdentifier(reader);
                }
                else if ( (ch != '\n') && (Char.IsWhiteSpace( ch )) )
                {
                    while ( (reader.Peek() != '\n') && (Char.IsWhiteSpace( reader.Peek() )) )
                    {
                        reader.Read();
                    }

                    tokenID = SanScriptTokenID.Whitespace;
                }
                else
                {
                    tokenID = SanScriptTokenID.Invalid;
                    switch ( ch )
                    {
                        case '\n':
                            tokenID = SanScriptTokenID.LineTerminator;
                            break;
                        case '(':
                            tokenID = SanScriptTokenID.OpenParenthesis;
                            break;
                        case ')':
                            tokenID = SanScriptTokenID.CloseParenthesis;
                            break;
                        case '<':
                            switch ( reader.Peek() )
                            {
                                case '<':
                                    reader.Read();
                                    tokenID = SanScriptTokenID.OpenVector;
                                    break;
                                case '=':
                                    reader.Read();
                                    tokenID = SanScriptTokenID.LessThanEqualTo;
                                    break;
                                case '>':
                                    reader.Read();
                                    tokenID = SanScriptTokenID.Inequality;
                                    break;
                                default:
                                    tokenID = SanScriptTokenID.LessThan;
                                    break;
                            }
                            break;
                        case '>':
                            switch ( reader.Peek() )
                            {
                                case '>':
                                    reader.Read();
                                    tokenID = SanScriptTokenID.CloseVector;
                                    break;
                                case '=':
                                    reader.Read();
                                    tokenID = SanScriptTokenID.GreaterThanEqualTo;
                                    break;
                                default:
                                    tokenID = SanScriptTokenID.GreaterThan;
                                    break;
                            }
                            break;
                        case '[':
                            tokenID = SanScriptTokenID.OpenBracket;
                            break;
                        case ']':
                            tokenID = SanScriptTokenID.CloseBracket;
                            break;
                        case '\"':
                            tokenID = ParseString( reader );
                            break;
                        case ',':
                            tokenID = SanScriptTokenID.Comma;
                            break;
                        case '&':
                            tokenID = SanScriptTokenID.Ampersand;
                            break;
                        case '|':
                            tokenID = SanScriptTokenID.Pipe;
                            break;
                        case '^':
                            tokenID = SanScriptTokenID.Caret;
                            break;
                        case '=':
                            tokenID = SanScriptTokenID.AssignmentOrEquality;
                            break;
                        case '+':
                            switch ( reader.Peek() )
                            {
                                case '=':
                                    reader.Read();
                                    tokenID = SanScriptTokenID.PlusEquals;
                                    break;
                                case '+':
                                    reader.Read();
                                    tokenID = SanScriptTokenID.Increment;
                                    break;
                                case '@':
                                    reader.Read();
                                    tokenID = SanScriptTokenID.PlusTimestepMultipliedBy;
                                    break;
                                default:
                                    tokenID = SanScriptTokenID.Addition;
                                    break;
                            }
                            break;
                        case '-':
                            switch ( reader.Peek() )
                            {
                                case '=':
                                    reader.Read();
                                    tokenID = SanScriptTokenID.MinusEquals;
                                    break;
                                case '-':
                                    reader.Read();
                                    tokenID = SanScriptTokenID.Decrement;
                                    break;
                                case '@':
                                    reader.Read();
                                    tokenID = SanScriptTokenID.MinusTimestepMultipliedBy;
                                    break;
                                default:
                                    tokenID = SanScriptTokenID.Subtraction;
                                    break;
                            }
                            break;
                        case '*':
                            if ( reader.Peek() == '=' )
                            {
                                reader.Read();
                                tokenID = SanScriptTokenID.MultiplyEquals;
                            }
                            else if ( reader.Peek() != '/' )
                            {
                                tokenID = SanScriptTokenID.Multiplication;
                            }
                            break;
                        case '/':
                            if ( reader.Peek() == '=' )
                            {
                                reader.Read();
                                tokenID = SanScriptTokenID.DivideEquals;
                            }
                            else if ( (reader.Peek() != '/') && (reader.Peek() != '*') )
                            {
                                tokenID = SanScriptTokenID.Division;
                            }
                            break;
                        case '%':
                            tokenID = SanScriptTokenID.Mod;
                            break;
                        case ':':
                            tokenID = SanScriptTokenID.Colon;
                            break;
                        case '#':
                            tokenID = ParsePreprocessorMacro( reader, ch );
                            break;
                        case '!':
                            if ( reader.Peek() == '=' )
                            {
                                reader.Read();
                                tokenID = SanScriptTokenID.AltInequality;
                            }
                            else
                            {
                                tokenID = SanScriptTokenID.AltNOT;
                            }
                            break;
                        default:
                            if ( ((ch >= '0') && (ch <= '9')) || (ch == '.') )
                            {
                                // Parse the number
                                tokenID = this.ParsePeriodOrNumber( reader, ch );
                            }
                            break;
                    }
                }
            }

            if ( tokenID != SanScriptTokenID.Invalid )
            {
                lexicalParseData = new LexicalStateAndIDTokenLexicalParseData( lexicalState, (byte)tokenID );
                return MatchType.ExactMatch;
            }
            else
            {
                if ( lexicalState.ID == SanScriptLexicalStateID.Default )
                {
                    reader.ReadReverse();
                }

                return MatchType.NoMatch;
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenID"></param>
        /// <returns></returns>
        public string GetTokenString( int tokenID )
        {
            if ( (tokenID > SanScriptTokenID.KeywordStart) && (tokenID < SanScriptTokenID.KeywordEnd) )
            {
                foreach ( KeyValuePair<string, int> pair in m_keywords )
                {
                    if ( pair.Value == tokenID )
                    {
                        return pair.Key.ToUpper();
                    }
                }
            }

            switch ( tokenID )
            {
                // General
                // Native Type Values
                case SanScriptTokenID.BooleanValue:
                    return "Boolean";
                case SanScriptTokenID.IntegerValue:
                    return "Integer";
                case SanScriptTokenID.FloatValue:
                    return "Float";
                case SanScriptTokenID.VectorValue:
                    return "Vector";
                case SanScriptTokenID.StringValue:
                    return "String";
                // Punctuation
                case SanScriptTokenID.OpenParenthesis:
                    return "(";
                case SanScriptTokenID.CloseParenthesis:
                    return ")";
                case SanScriptTokenID.OpenVector:
                    return "<<";
                case SanScriptTokenID.CloseVector:
                    return ">>";
                case SanScriptTokenID.OpenBracket:
                    return "[";
                case SanScriptTokenID.CloseBracket:
                    return "]";
                case SanScriptTokenID.Comma:
                    return ",";
                case SanScriptTokenID.Period:
                    return ".";
                case SanScriptTokenID.Ampersand:
                    return "&";
                case SanScriptTokenID.Colon:
                    return ":";
                // Operators
                case SanScriptTokenID.Pipe:
                    return "|";
                case SanScriptTokenID.Caret:
                    return "^";
                case SanScriptTokenID.AssignmentOrEquality:
                    return "=";
                case SanScriptTokenID.Inequality:
                    return "<>";
                case SanScriptTokenID.Addition:
                    return "+";
                case SanScriptTokenID.Subtraction:
                    return "-";
                case SanScriptTokenID.Multiplication:
                    return "*";
                case SanScriptTokenID.Division:
                    return "/";
                case SanScriptTokenID.LessThanEqualTo:
                    return "<=";
                case SanScriptTokenID.LessThan:
                    return "<";
                case SanScriptTokenID.GreaterThanEqualTo:
                    return ">=";
                case SanScriptTokenID.GreaterThan:
                    return ">";
                case SanScriptTokenID.Mod:
                    return "%";
                case SanScriptTokenID.PlusTimestepMultipliedBy:
                    return "+@";
                case SanScriptTokenID.MinusTimestepMultipliedBy:
                    return "-@";
                case SanScriptTokenID.Increment:
                    return "++";
                case SanScriptTokenID.Decrement:
                    return "--";
                case SanScriptTokenID.PlusEquals:
                    return "+=";
                case SanScriptTokenID.MinusEquals:
                    return "-=";
                case SanScriptTokenID.MultiplyEquals:
                    return "*=";
                case SanScriptTokenID.DivideEquals:
                    return "/=";
                case SanScriptTokenID.AltNOT:
                    return "!";
                case SanScriptTokenID.AltInequality:
                    return "!=";
                default:
                    return SanScriptTokenID.GetTokenKey( tokenID );
            }
        }

        /// <summary>
        /// Represents the method that will handle <see cref="ITokenLexicalParseData"/> matching callbacks.
        /// </summary>
        /// <param name="reader">An <see cref="ITextBufferReader"/> that is reading a text source.</param>
        /// <param name="lexicalScope">The <see cref="ILexicalScope"/> that specifies the lexical scope to check.</param>
        /// <param name="lexicalParseData">Returns the <see cref="ITokenLexicalParseData"/> that was parsed, if any.</param>
        /// <returns>A <see cref="MatchType"/> indicating the type of match that was made.</returns>
        public MatchType IsStringStateScopeStart( ITextBufferReader reader, ILexicalScope lexicalScope, 
            ref ITokenLexicalParseData lexicalParseData )
        {
            if ( reader.Peek() == '\"')
            {
                reader.Read();
                lexicalParseData = new LexicalScopeAndIDTokenLexicalParseData( lexicalScope, SanScriptTokenID.OpenString );
                return MatchType.ExactMatch;
            }
            
            return MatchType.NoMatch;
        }
        /// <summary>
        /// Represents the method that will handle <see cref="ITokenLexicalParseData"/> matching callbacks.
        /// </summary>
        /// <param name="reader">An <see cref="ITextBufferReader"/> that is reading a text source.</param>
        /// <param name="lexicalScope">The <see cref="ILexicalScope"/> that specifies the lexical scope to check.</param>
        /// <param name="lexicalParseData">Returns the <see cref="ITokenLexicalParseData"/> that was parsed, if any.</param>
        /// <returns>A <see cref="MatchType"/> indicating the type of match that was made.</returns>
        public MatchType IsStringStateScopeEnd( ITextBufferReader reader, ILexicalScope lexicalScope,
            ref ITokenLexicalParseData lexicalParseData )
        {
            if ( reader.Peek() == '\"' || reader.Peek() == '\n' )
            {
                reader.Read();
                lexicalParseData = new LexicalScopeAndIDTokenLexicalParseData( lexicalScope, SanScriptTokenID.CloseString );
                return MatchType.ExactMatch;
            }

            return MatchType.NoMatch;
        }

        /// <summary>
        /// Represents the method that will handle <see cref="ITokenLexicalParseData"/> matching callbacks.
        /// </summary>
        /// <param name="reader">An <see cref="ITextBufferReader"/> that is reading a text source.</param>
        /// <param name="lexicalScope">The <see cref="ILexicalScope"/> that specifies the lexical scope to check.</param>
        /// <param name="lexicalParseData">Returns the <see cref="ITokenLexicalParseData"/> that was parsed, if any.</param>
        /// <returns>A <see cref="MatchType"/> indicating the type of match that was made.</returns>
        public MatchType IsCommentStateScopeStart( ITextBufferReader reader, ILexicalScope lexicalScope,
            ref ITokenLexicalParseData lexicalParseData )
        {
            if ( (reader.Peek( 1 ) == '/') && (reader.Peek( 2 ) == '/') )
            {
                reader.Read();
                reader.Read();
                lexicalParseData = new LexicalScopeAndIDTokenLexicalParseData( lexicalScope, SanScriptTokenID.SingleLineCommentStart );
                return MatchType.ExactMatch;
            }

            return MatchType.NoMatch;
        }
        /// <summary>
        /// Represents the method that will handle <see cref="ITokenLexicalParseData"/> matching callbacks.
        /// </summary>
        /// <param name="reader">An <see cref="ITextBufferReader"/> that is reading a text source.</param>
        /// <param name="lexicalScope">The <see cref="ILexicalScope"/> that specifies the lexical scope to check.</param>
        /// <param name="lexicalParseData">Returns the <see cref="ITokenLexicalParseData"/> that was parsed, if any.</param>
        /// <returns>A <see cref="MatchType"/> indicating the type of match that was made.</returns>
        public MatchType IsCommentStateScopeEnd( ITextBufferReader reader, ILexicalScope lexicalScope,
            ref ITokenLexicalParseData lexicalParseData )
        {
            if ( reader.Peek() == '\n' )
            {
                reader.Read();
                lexicalParseData = new LexicalScopeAndIDTokenLexicalParseData( lexicalScope, SanScriptTokenID.SingleLineCommentEnd );
                return MatchType.ExactMatch;
            }

            return MatchType.NoMatch;
        }

        /// <summary>
        /// Represents the method that will handle <see cref="ITokenLexicalParseData"/> matching callbacks.
        /// </summary>
        /// <param name="reader">An <see cref="ITextBufferReader"/> that is reading a text source.</param>
        /// <param name="lexicalScope">The <see cref="ILexicalScope"/> that specifies the lexical scope to check.</param>
        /// <param name="lexicalParseData">Returns the <see cref="ITokenLexicalParseData"/> that was parsed, if any.</param>
        /// <returns>A <see cref="MatchType"/> indicating the type of match that was made.</returns>
        public MatchType IsMultiLineCommentStateScopeStart( ITextBufferReader reader, ILexicalScope lexicalScope,
            ref ITokenLexicalParseData lexicalParseData )
        {
            if ( (reader.Peek( 1 ) == '/') && (reader.Peek( 2 ) == '*') )
            {
                reader.Read();
                reader.Read();
                lexicalParseData = new LexicalScopeAndIDTokenLexicalParseData( lexicalScope, SanScriptTokenID.MultiLineCommentStart );
                return MatchType.ExactMatch;
            }
            return MatchType.NoMatch;
        }
        /// <summary>
        /// Represents the method that will handle <see cref="ITokenLexicalParseData"/> matching callbacks.
        /// </summary>
        /// <param name="reader">An <see cref="ITextBufferReader"/> that is reading a text source.</param>
        /// <param name="lexicalScope">The <see cref="ILexicalScope"/> that specifies the lexical scope to check.</param>
        /// <param name="lexicalParseData">Returns the <see cref="ITokenLexicalParseData"/> that was parsed, if any.</param>
        /// <returns>A <see cref="MatchType"/> indicating the type of match that was made.</returns>
        public MatchType IsMultiLineCommentStateScopeEnd( ITextBufferReader reader, ILexicalScope lexicalScope,
            ref ITokenLexicalParseData lexicalParseData )
        {
            if ( (reader.Peek( 1 ) == '*') && (reader.Peek( 2 ) == '/') )
            {
                reader.Read();
                reader.Read();
                lexicalParseData = new LexicalScopeAndIDTokenLexicalParseData( lexicalScope, SanScriptTokenID.MultiLineCommentEnd );
                return MatchType.ExactMatch;
            }

            return MatchType.NoMatch;
        }
        #endregion

        #region Protected Functions
        protected virtual int ParseString( ITextBufferReader reader )
        {
            int startOffset = reader.Offset;

            while ( !reader.IsAtEnd )
            {
                char peek1 = reader.Peek(1);
                char peek2 = reader.Peek(2);
                if (peek1 == '\\' && (peek2 == '\\' || peek2 == '\"'))
                {
                    reader.Read();
                }
                else if (peek1 == '\"' || peek1 == '\n')
                {
                    break;
                }

                reader.Read();
            }

            return (reader.Offset > startOffset) ? SanScriptTokenID.StringValue : SanScriptTokenID.Invalid;
        }

        /// <summary>
        /// Parses a single line comment.
        /// </summary>
        /// <param name="reader">An <see cref="ITextBufferReader"/> that is reading a text source.</param>
        /// <returns>The ID of the token that was matched.</returns>
        protected virtual int ParseSingleLineComment( ITextBufferReader reader )
        {
            int startOffset = reader.Offset;

            while ( !reader.IsAtEnd && (reader.Peek() != '\n') )
            {
                reader.Read();
            }

            return (reader.Offset > startOffset) ? SanScriptTokenID.SingleLineComment : SanScriptTokenID.Invalid;
        }


        /// <summary>
        /// Parses a multiple line comment.
        /// </summary>
        /// <param name="reader">An <see cref="ITextBufferReader"/> that is reading a text source.</param>
        /// <returns>The ID of the token that was matched.</returns>
        protected virtual int ParseMultiLineComment( ITextBufferReader reader )
        {
            int startOffset = reader.Offset;

            while ( !reader.IsAtEnd )
            {
                if ( (reader.Peek( 1 ) == '*') && (reader.Peek( 2 ) == '/') )
                {
                    break;
                }

                reader.Read();
            }

            return (reader.Offset > startOffset) ? SanScriptTokenID.MultiLineComment : SanScriptTokenID.Invalid;
        }

        /// <summary>
        /// Parses an identifier.
        /// </summary>
        /// <param name="reader">An <see cref="ITextBufferReader"/> that is reading a text source.</param>
        /// <returns>The ID of the token that was matched.</returns>
        protected virtual int ParseIdentifier( ITextBufferReader reader)
        {
            // Get the entire word
            int startOffset = reader.Offset - 1;
            while ( !reader.IsAtEnd )
            {
                char ch = reader.Read();
                // NOTE: This could be improved by supporting \u escape sequences
                if ( ch != '_' && !char.IsLetterOrDigit( ch ))
                {
                    reader.ReadReverse();
                    break;
                }
            }

            string word = reader.GetSubstring(startOffset, reader.Offset - startOffset);
            
            int value = SanScriptTokenID.Invalid;
            if ( m_keywords.TryGetValue( word, out value ) )
            {
                if ( value == SanScriptTokenID.STRUCT )
                {
                    int i = 1;
                    char ch2 = reader.Peek( i );
                    while ( !reader.IsAtEnd )
                    {
                        if ( ch2 == '&' )
                        {
                            return SanScriptTokenID.StructArgument;
                        }
                        else if ( !Char.IsWhiteSpace( ch2 ) )
                        {
                            break;
                        }
                        else
                        {
                            ++i;
                            ch2 = reader.Peek( i );
                        }
                    }
                }
                else if ( value == SanScriptTokenID.ENUMTOINT )
                {
                    int i = 1;
                    char ch = reader.Peek( i );
                    while ( !reader.IsAtEnd )
                    {
                        if ( Char.IsLetter( ch ) || ch == '_' || ch == '&' )
                        {
                            return SanScriptTokenID.ENUMTOINTdatatype;
                        }
                        else if ( !Char.IsWhiteSpace( ch ) )
                        {
                            break;
                        }
                        else
                        {
                            ++i;
                            ch = reader.Peek( i );
                        }
                    }
                }

                return value;
            }                
            else
            {
                // We don't know if this is a Identifier, UserIdentifier, or NativeIdentifier here.
                return SanScriptTokenID.Identifier;
            }
        }

        /// <summary>
        /// Parses a number.
        /// </summary>
        /// <param name="reader">An <see cref="ITextBufferReader"/> that is reading a text source.</param>
        /// <param name="ch">The first character of the number.</param>
        /// <returns>The ID of the token that was matched.</returns>
        protected virtual int ParsePeriodOrNumber( ITextBufferReader reader, char ch )
        {
            int tokenID = SanScriptTokenID.IntegerValue;
            if ( ch == '.' )
            {
                if ( Char.IsNumber( reader.Peek() ) )
                {
                    reader.Read();
                    tokenID = SanScriptTokenID.FloatValue;
                }
                else
                {
                    return SanScriptTokenID.Period;
                }
            }

            while ( !reader.IsAtEnd && Char.IsNumber( reader.Peek() ) )
            {
                reader.Read();
            }

            if ( (reader.Peek() == '.') && (tokenID == SanScriptTokenID.IntegerValue) )
            {
                reader.Read();
                tokenID = SanScriptTokenID.FloatValue;

                while ( !reader.IsAtEnd && Char.IsNumber( reader.Peek() ) )
                {
                    reader.Read();
                }
            }

            // new exponential numbers support
            ch = reader.Peek();
            if ( (ch == 'E') || (ch == 'e') )
            {
                tokenID = SanScriptTokenID.FloatValue;

                reader.Read();

                ch = reader.Peek();
                if ( (ch == '+') || (ch == '-') || Char.IsNumber( ch ) )
                {
                    reader.Read();

                    if ( Char.IsNumber( ch ) || Char.IsNumber( reader.Peek() ) )
                    {
                        while ( !reader.IsAtEnd && Char.IsNumber( reader.Peek() ) )
                        {
                            reader.Read();
                        }
                    }
                    else
                    {
                        tokenID = SanScriptTokenID.Invalid;
                    }
                }
                else
                {
                    tokenID = SanScriptTokenID.Invalid;
                }
            }

            return tokenID;
        }

        /// <summary>
        /// Parses a preprocessor macro
        /// </summary>
        /// <param name="reader">An <see cref="ITextBufferReader"/> that is reading a text source.</param>
        /// <param name="ch">The first character of the macro.  Should be '#'.</param>
        /// <returns>The ID of the token that was matched.</returns>
        private int ParsePreprocessorMacro( ITextBufferReader reader, char ch )
        {
            StringBuilder word = new StringBuilder();
            word.Append( ch );

            while ( !reader.IsAtEnd )
            {
                ch = reader.Peek();
                if ( Char.IsWhiteSpace( ch ) )
                {
                    break;
                }

                reader.Read();
                word.Append( ch );
            }

            int value = SanScriptTokenID.Invalid;
            if ( m_keywords.TryGetValue( word.ToString().ToUpper(), out value ) )
            {
                 return value;
            }

            return SanScriptTokenID.Invalid;
        }
        #endregion
    }
}
