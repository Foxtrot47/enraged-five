using System;
using System.Linq;
using System.Windows.Forms;

using ActiproSoftware.SyntaxEditor;

namespace SanScriptParser
{
    /// <summary>
    /// Provides a class for updating a document outline <see cref="TreeView"/> with nodes from various language ASTs.
    /// </summary>
    public static class SanScriptDocumentOutlineUpdater
    {
        #region Static Variables
        private static bool sm_verboseDocumentOutline = false;
        #endregion

        #region Public Static Properties
        public static bool VerboseDocumentOutline
        {
            get
            {
                return sm_verboseDocumentOutline;
            }
            set
            {
                sm_verboseDocumentOutline = value;
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Processes the specified root <see cref="ICompilationUnit"/>.
        /// </summary>
        /// <param name="compilationUnit">The <see cref="ICompilationUnit"/> to examine.</param>
        public static void Process( string guid, TreeView documentOutlineTreeView, ICompilationUnit compilationUnit)
        {
            documentOutlineTreeView.BeginUpdate();
            if (documentOutlineTreeView.Tag != (object)guid)
            {
                documentOutlineTreeView.Nodes.Clear();
            }

            documentOutlineTreeView.BeforeExpand -= ExpandNode;
            documentOutlineTreeView.BeforeExpand += ExpandNode;
            documentOutlineTreeView.AfterCollapse -= CollapseNode;
            documentOutlineTreeView.AfterCollapse += CollapseNode;
            ProcessNode(documentOutlineTreeView, null, compilationUnit, compilationUnit, 1);

#if !NET11
            if ((documentOutlineTreeView.Tag != (object)guid) && (documentOutlineTreeView.Nodes.Count > 0))
            {
                documentOutlineTreeView.TopNode = documentOutlineTreeView.Nodes[0];
            }
#endif
            documentOutlineTreeView.EndUpdate();
        }
        #endregion

        #region Private Functions
        private static void ExpandNode(Object sender, TreeViewCancelEventArgs e)
        {
            TreeNode node = e.Node;
            ICompilationUnit compilationUnit;
            IAstNode astNode = node.Tag as IAstNode;
            if (astNode == null)
            {
                // Shouldn't happen, just don't do anything.
                return;
            }

            IAstNode n = astNode;
            do
            {
                if (n == null)
                {
                    // Reached root without finding compilation unit?
                    return;
                }

                compilationUnit = n as ICompilationUnit;
                n = n.ParentNode;
            }
            while (compilationUnit == null);

            node.TreeView.BeginUpdate();
            node.Nodes.Clear();
            ProcessChildNodes(node.TreeView, node, compilationUnit, astNode, 1);
            node.TreeView.EndUpdate();
        }

        private static void CollapseNode(Object sender, TreeViewEventArgs e)
        {
            e.Node.Nodes.Clear();

            // Add a stub node to parent [should never be visible as it gets deleted on pre-expansion event]
            e.Node.Nodes.Add(new TreeNode("Loading..."));
        }

        /// <summary>
        /// Processes the child nodes of the specified <see cref="IAstNode"/>.
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="parent">Parent of current node.</param>
        /// <param name="compilationUnit">The parent <see cref="ICompilationUnit"/>.</param>
        /// <param name="node">The <see cref="IAstNode"/> to examine.</param>
        /// <param name="numLevelsToProcess">Number of levels to process. If less than 0, process full tree. If count == 1, the child nodes will be stubs</param>
        private static void ProcessChildNodes(TreeView treeView, TreeNode parent, ICompilationUnit compilationUnit, IAstNode node, int numLevelsToProcess)
        {
            if (node.ChildNodeCount <= 0)
            {
                return;
            }


            IAstNodeList childNodes;
            if (node is ICompilationUnit)
            {
                compilationUnit = (ICompilationUnit)node;
                childNodes = node.ChildNodes;
            }
            else if (node is BlockStatement)
            {
                BlockStatement block = node as BlockStatement;
                if (!(SanScriptDocumentOutlineUpdater.VerboseDocumentOutline
                    || block.NodeType == SanScriptNodeType.GlobalsDefinitionBlockStatement
                    || block.NodeType == SanScriptNodeType.EnumDefinitionBlockStatement
                    || block.NodeType == SanScriptNodeType.HashEnumDefinitionBlockStatement
                    || block.NodeType == SanScriptNodeType.StrictEnumDefinitionBlockStatement
                    || block.NodeType == SanScriptNodeType.StructDefinitionBlockStatement
                    || block.Statements.OfType<BlockStatement>().Any()))
                {
                    // Only including following types of block statements, or those containing other block statements
                    return;
                }

                childNodes = block.Statements;
            }
            else
            {
                // Only handle compilation units and block statements recursively.
                return;
            }

            if (numLevelsToProcess == 0)
            {
                if (parent != null)
                {
                    parent.Nodes.Add(new TreeNode("Loading..."));
                }
                return;
            }

            foreach ( IAstNode child in childNodes)
            {
                ProcessNode(treeView, parent, compilationUnit, child, Math.Max(numLevelsToProcess - 1, -1));
            }
        }

        /// <summary>
        /// Processes the specified <see cref="IAstNode"/>.
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="parent">Parent of current node</param>
        /// <param name="compilationUnit">The parent <see cref="ICompilationUnit"/>.</param>
        /// <param name="node">The <see cref="IAstNode"/> to examine.</param>
        /// <param name="numLevelsToProcess">The levels deep to process. If less or equal to 0, process infinitely</param>
        private static void ProcessNode(TreeView treeView, TreeNode parent, ICompilationUnit compilationUnit,  IAstNode node, int numLevelsToProcess)
        {
            if ( node is SanScriptParser.AstNode )
            {
                // Process Simple node
                ProcessSanScriptNode(treeView, parent, compilationUnit, (SanScriptParser.AstNode)node, numLevelsToProcess);
            }
            else if (node is LanguageTransitionAstNode)
            {
                // Process child nodes
                ProcessChildNodes(treeView, parent, compilationUnit, node, numLevelsToProcess);
            }
            else
            {
                // Show all nodes for other language
                ProcessOtherLanguageNode(treeView, parent, compilationUnit, node, numLevelsToProcess);
            }
        }

        /// <summary>
        /// Processes the specified <see cref="IAstNode"/>.
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="parent">Parent node</param>
        /// <param name="compilationUnit">The parent <see cref="ICompilationUnit"/>.</param>
        /// <param name="node">The <see cref="IAstNode"/> to examine.</param>
        /// <param name="numLevelsToProcess"></param>
        private static void ProcessOtherLanguageNode(TreeView treeView, TreeNode parent, ICompilationUnit compilationUnit, IAstNode node, int numLevelsToProcess)
        {
            // Process a node start
            if (!(node is ICompilationUnit))
            {
                parent = StartNode(treeView, parent, node );
            }

            ProcessChildNodes(treeView, parent, compilationUnit, node, numLevelsToProcess);
        }

        /// <summary>
        /// Processes the specified <see cref="IAstNode"/>.
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="parent"></param>
        /// <param name="compilationUnit">The parent <see cref="ICompilationUnit"/>.</param>
        /// <param name="node">The <see cref="IAstNode"/> to examine.</param>
        /// <param name="numLevelsToProcess"></param>
        private static void ProcessSanScriptNode(TreeView treeView, TreeNode parent, ICompilationUnit compilationUnit, AstNode node, int numLevelsToProcess)
        {
            // Process a node start
            switch ( node.NodeType )
            {
                case SanScriptNodeType.EmptyStatement:
                    return;
                case SanScriptNodeType.Identifier:
                case SanScriptNodeType.BooleanExpression:
                case SanScriptNodeType.IntegerExpression:
                case SanScriptNodeType.FloatExpression:
                case SanScriptNodeType.VectorExpression:
                case SanScriptNodeType.StringExpression:
                case SanScriptNodeType.ArrayAccessExpression:
                case SanScriptNodeType.StructMemberAccessExpression:
                case SanScriptNodeType.SubroutineAccessExpression:
                case SanScriptNodeType.VariableExpression:
                case SanScriptNodeType.CountOfExpression:
                case SanScriptNodeType.EnumToIntExpression:
                case SanScriptNodeType.IntToEnumExpression:
                case SanScriptNodeType.IntToNativeExpression:
                case SanScriptNodeType.NativeToIntExpression:
                case SanScriptNodeType.SizeOfExpression:
                case SanScriptNodeType.CatchExpression:
                case SanScriptNodeType.TimestepExpression:
                case SanScriptNodeType.ParenthesizedExpression:
                case SanScriptNodeType.BinaryExpression:
                case SanScriptNodeType.AssignmentStatement:
                case SanScriptNodeType.AdditiveStatement:
                case SanScriptNodeType.ThrowStatement:
                case SanScriptNodeType.LabelDeclarationStatement:
                case SanScriptNodeType.GotoStatement:
                case SanScriptNodeType.GotoStateStatement:
                case SanScriptNodeType.DefaultStateStatement:
                case SanScriptNodeType.HistoryStatement:
                case SanScriptNodeType.ReturnStatement:
                case SanScriptNodeType.ExitStatement:
                case SanScriptNodeType.GlobalsStatement:
                case SanScriptNodeType.CallStatement:
                case SanScriptNodeType.SubroutineAccessStatement:
                case SanScriptNodeType.UsingStatement:
                case SanScriptNodeType.ForwardEnumDeclarationStatement:
                case SanScriptNodeType.ForwardStructDeclarationStatement:
                case SanScriptNodeType.ConstDefinitionStatement:
                case SanScriptNodeType.NativeTypeDeclarationStatement:
                case SanScriptNodeType.NativeProcDeclarationStatement:
                case SanScriptNodeType.NativeEventDeclarationStatement:
                case SanScriptNodeType.NativeFuncDeclarationStatement:
                case SanScriptNodeType.TypedefFuncDeclarationStatement:
                case SanScriptNodeType.TypedefProcDeclarationStatement:
                case SanScriptNodeType.PreprocessorMacroStatement:
                case SanScriptNodeType.PreprocessorMacroExpression:
                    StartNode(treeView, parent, node );
                    if ( (compilationUnit != null) && (!compilationUnit.HasLanguageTransitions) )
                    {
                        return;
                    }
                    break;
                case SanScriptNodeType.VariableNonArrayDeclarationStatement:
                case SanScriptNodeType.VariableArrayDeclarationStatement:
                    {
                        VariableDeclarationStatement decl = node as VariableDeclarationStatement;
                        while ( decl != null )
                        {
                            StartNode(treeView, parent, decl );
                            decl = decl.SubDeclaration as VariableDeclarationStatement;
                        }

                        if ( (compilationUnit != null) && (!compilationUnit.HasLanguageTransitions) )
                        {
                            return;
                        }
                    }
                    break;
                case SanScriptNodeType.IfBlockStatement:
                case SanScriptNodeType.WhileBlockStatement:
                case SanScriptNodeType.RepeatBlockStatement:
                case SanScriptNodeType.ForBlockStatement:
                case SanScriptNodeType.CaseBlockStatement:
                case SanScriptNodeType.SwitchBlockStatement:
                case SanScriptNodeType.StructDefinitionBlockStatement:
                case SanScriptNodeType.StateDefinitionBlockStatement:
                case SanScriptNodeType.ActivateBlockStatement:
                case SanScriptNodeType.DeactivateBlockStatement:
                case SanScriptNodeType.EnumDefinitionBlockStatement:
                case SanScriptNodeType.HashEnumDefinitionBlockStatement:
                case SanScriptNodeType.StrictEnumDefinitionBlockStatement:
                case SanScriptNodeType.GlobalsDefinitionBlockStatement:
                case SanScriptNodeType.ScriptBlockStatement:
                case SanScriptNodeType.ProcDefinitionBlockStatement:
                case SanScriptNodeType.EventDefinitionBlockStatement:
                case SanScriptNodeType.FuncDefinitionBlockStatement:
                    parent = StartNode(treeView, parent, node );

                    break;
                case SanScriptNodeType.CompilationUnit:
                    break;
            }

            // Process child nodes
            ProcessChildNodes(treeView, parent, compilationUnit, node, numLevelsToProcess);
        }

        /// <summary>
        /// Adds a new <see cref="TreeNode"/> to the <see cref="TreeView"/>.
        /// </summary>
        /// <param name="treeView">The tree view</param>
        /// <param name="parent">The parent node</param>
        /// <param name="node">The <see cref="IAstNode"/> represented by the <see cref="TreeNode"/>.</param>
        private static TreeNode StartNode( TreeView treeView, TreeNode parent, IAstNode node )
        {
            string displayText = node.DisplayText;
            int indexOf = displayText.IndexOf( '\n' );
            if ( indexOf > -1 )
            {
                displayText = displayText.Substring( 0, indexOf );
            }

            // Create a new node
            TreeNode treeNode = new TreeNode();
            treeNode.Text = displayText;
            treeNode.ImageIndex = node.ImageIndex;
            treeNode.SelectedImageIndex = node.ImageIndex;
            treeNode.Tag = node;

            // Add the new node
            if ( parent == null )
            {
                treeView.Nodes.Add(treeNode);
            }
            else
            {
                parent.Nodes.Add( treeNode );
            }

            return treeNode;
        }
        #endregion
    }
}