using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using ActiproSoftware.SyntaxEditor;
using SanScriptParser;

namespace SanScriptParser {

	#region Token IDs
	/// <summary>
	/// Contains the token IDs for the <c>SanScript</c> language.
	/// </summary>
	public class SanScriptTokenID {

		/// <summary>
		/// Returns the string-based key for the specified token ID.
		/// </summary>
		/// <param name="id">The token ID to examine.</param>
		public static string GetTokenKey(int id) {
			System.Reflection.FieldInfo[] fields = typeof(SanScriptTokenID).GetFields();
			foreach (System.Reflection.FieldInfo field in fields) {
				if ((field.IsStatic) && (field.IsLiteral) && (id.Equals(field.GetValue(null))))
					return field.Name;
			}
			return null;
		}

		/// <summary>
		/// The Invalid token ID.
		/// </summary>
		public const int Invalid = 0;

		/// <summary>
		/// The DocumentEnd token ID.
		/// </summary>
		public const int DocumentEnd = 1;

		/// <summary>
		/// The Whitespace token ID.
		/// </summary>
		public const int Whitespace = 2;

		/// <summary>
		/// The LineTerminator token ID.
		/// </summary>
		public const int LineTerminator = 3;

		/// <summary>
		/// The SingleLineComment token ID.
		/// </summary>
		public const int SingleLineComment = 4;

		/// <summary>
		/// The SingleLineCommentStart token ID.
		/// </summary>
		public const int SingleLineCommentStart = 5;

		/// <summary>
		/// The SingleLineCommentEnd token ID.
		/// </summary>
		public const int SingleLineCommentEnd = 6;

		/// <summary>
		/// The MultiLineComment token ID.
		/// </summary>
		public const int MultiLineComment = 7;

		/// <summary>
		/// The MultiLineCommentStart token ID.
		/// </summary>
		public const int MultiLineCommentStart = 8;

		/// <summary>
		/// The MultiLineCommentEnd token ID.
		/// </summary>
		public const int MultiLineCommentEnd = 9;

		/// <summary>
		/// The BooleanValue token ID.
		/// </summary>
		public const int BooleanValue = 10;

		/// <summary>
		/// The IntegerValue token ID.
		/// </summary>
		public const int IntegerValue = 11;

		/// <summary>
		/// The FloatValue token ID.
		/// </summary>
		public const int FloatValue = 12;

		/// <summary>
		/// The VectorValue token ID.
		/// </summary>
		public const int VectorValue = 13;

		/// <summary>
		/// The StringValue token ID.
		/// </summary>
		public const int StringValue = 14;

		/// <summary>
		/// The DefaultValue token ID.
		/// </summary>
		public const int DefaultValue = 15;

		/// <summary>
		/// The Identifier token ID.
		/// </summary>
		public const int Identifier = 16;

		/// <summary>
		/// The UserIdentifier token ID.
		/// </summary>
		public const int UserIdentifier = 17;

		/// <summary>
		/// The NativeIdentifier token ID.
		/// </summary>
		public const int NativeIdentifier = 18;

		/// <summary>
		/// The TypedefIdentifier token ID.
		/// </summary>
		public const int TypedefIdentifier = 19;

		/// <summary>
		/// The StructArgument token ID.
		/// </summary>
		public const int StructArgument = 20;

		/// <summary>
		/// The Disabled token ID.
		/// </summary>
		public const int Disabled = 21;

		/// <summary>
		/// The KeywordStart token ID.
		/// </summary>
		public const int KeywordStart = 22;

		/// <summary>
		/// The NativeTypeStart token ID.
		/// </summary>
		public const int NativeTypeStart = 23;

		/// <summary>
		/// The BOOL token ID.
		/// </summary>
		public const int BOOL = 24;

		/// <summary>
		/// The CONSTINT token ID.
		/// </summary>
		public const int CONSTINT = 25;

		/// <summary>
		/// The CONSTFLOAT token ID.
		/// </summary>
		public const int CONSTFLOAT = 26;

		/// <summary>
		/// The ENUMTOINTdatatype token ID.
		/// </summary>
		public const int ENUMTOINTdatatype = 27;

		/// <summary>
		/// The HASH token ID.
		/// </summary>
		public const int HASH = 28;

		/// <summary>
		/// The FLOAT token ID.
		/// </summary>
		public const int FLOAT = 29;

		/// <summary>
		/// The TWEAK_FLOAT token ID.
		/// </summary>
		public const int TWEAK_FLOAT = 30;

		/// <summary>
		/// The INT token ID.
		/// </summary>
		public const int INT = 31;

		/// <summary>
		/// The TWEAK_INT token ID.
		/// </summary>
		public const int TWEAK_INT = 32;

		/// <summary>
		/// The VECTOR token ID.
		/// </summary>
		public const int VECTOR = 33;

		/// <summary>
		/// The STRING token ID.
		/// </summary>
		public const int STRING = 34;

		/// <summary>
		/// The VARARGS token ID.
		/// </summary>
		public const int VARARGS = 35;

		/// <summary>
		/// The TEXTLABEL token ID.
		/// </summary>
		public const int TEXTLABEL = 36;

		/// <summary>
		/// The TEXTLABELTHREE token ID.
		/// </summary>
		public const int TEXTLABELTHREE = 37;

		/// <summary>
		/// The TEXTLABELSEVEN token ID.
		/// </summary>
		public const int TEXTLABELSEVEN = 38;

		/// <summary>
		/// The TEXTLABELFIFTEEN token ID.
		/// </summary>
		public const int TEXTLABELFIFTEEN = 39;

		/// <summary>
		/// The TEXTLABELTWENTYTHREE token ID.
		/// </summary>
		public const int TEXTLABELTWENTYTHREE = 40;

		/// <summary>
		/// The TEXTLABELTHIRTYONE token ID.
		/// </summary>
		public const int TEXTLABELTHIRTYONE = 41;

		/// <summary>
		/// The TEXTLABELSIXTYTHREE token ID.
		/// </summary>
		public const int TEXTLABELSIXTYTHREE = 42;

		/// <summary>
		/// The NativeTypeEnd token ID.
		/// </summary>
		public const int NativeTypeEnd = 43;

		/// <summary>
		/// The FALSE token ID.
		/// </summary>
		public const int FALSE = 44;

		/// <summary>
		/// The TRUE token ID.
		/// </summary>
		public const int TRUE = 45;

		/// <summary>
		/// The PreprocessorStart token ID.
		/// </summary>
		public const int PreprocessorStart = 46;

		/// <summary>
		/// The PoundIf token ID.
		/// </summary>
		public const int PoundIf = 47;

		/// <summary>
		/// The DEFINED token ID.
		/// </summary>
		public const int DEFINED = 48;

		/// <summary>
		/// The PoundEndif token ID.
		/// </summary>
		public const int PoundEndif = 49;

		/// <summary>
		/// The PreprocessorEnd token ID.
		/// </summary>
		public const int PreprocessorEnd = 50;

		/// <summary>
		/// The WordOperatorStart token ID.
		/// </summary>
		public const int WordOperatorStart = 51;

		/// <summary>
		/// The AND token ID.
		/// </summary>
		public const int AND = 52;

		/// <summary>
		/// The NOT token ID.
		/// </summary>
		public const int NOT = 53;

		/// <summary>
		/// The OR token ID.
		/// </summary>
		public const int OR = 54;

		/// <summary>
		/// The WordOperatorEnd token ID.
		/// </summary>
		public const int WordOperatorEnd = 55;

		/// <summary>
		/// The CASE token ID.
		/// </summary>
		public const int CASE = 56;

		/// <summary>
		/// The DEFAULT token ID.
		/// </summary>
		public const int DEFAULT = 57;

		/// <summary>
		/// The ENUM token ID.
		/// </summary>
		public const int ENUM = 58;

		/// <summary>
		/// The HASH_ENUM token ID.
		/// </summary>
		public const int HASH_ENUM = 59;

		/// <summary>
		/// The STRICT_ENUM token ID.
		/// </summary>
		public const int STRICT_ENUM = 60;

		/// <summary>
		/// The FOR token ID.
		/// </summary>
		public const int FOR = 61;

		/// <summary>
		/// The FUNC token ID.
		/// </summary>
		public const int FUNC = 62;

		/// <summary>
		/// The GLOBALS token ID.
		/// </summary>
		public const int GLOBALS = 63;

		/// <summary>
		/// The HSMACTIVATE token ID.
		/// </summary>
		public const int HSMACTIVATE = 64;

		/// <summary>
		/// The HSMDEACTIVATE token ID.
		/// </summary>
		public const int HSMDEACTIVATE = 65;

		/// <summary>
		/// The HSMEVENT token ID.
		/// </summary>
		public const int HSMEVENT = 66;

		/// <summary>
		/// The HSMSTATE token ID.
		/// </summary>
		public const int HSMSTATE = 67;

		/// <summary>
		/// The IF token ID.
		/// </summary>
		public const int IF = 68;

		/// <summary>
		/// The PROC token ID.
		/// </summary>
		public const int PROC = 69;

		/// <summary>
		/// The REPEAT token ID.
		/// </summary>
		public const int REPEAT = 70;

		/// <summary>
		/// The SCRIPT token ID.
		/// </summary>
		public const int SCRIPT = 71;

		/// <summary>
		/// The STRUCT token ID.
		/// </summary>
		public const int STRUCT = 72;

		/// <summary>
		/// The SWITCH token ID.
		/// </summary>
		public const int SWITCH = 73;

		/// <summary>
		/// The WHILE token ID.
		/// </summary>
		public const int WHILE = 74;

		/// <summary>
		/// The ELIF token ID.
		/// </summary>
		public const int ELIF = 75;

		/// <summary>
		/// The ELSE token ID.
		/// </summary>
		public const int ELSE = 76;

		/// <summary>
		/// The BREAK token ID.
		/// </summary>
		public const int BREAK = 77;

		/// <summary>
		/// The BREAKLOOP token ID.
		/// </summary>
		public const int BREAKLOOP = 78;

		/// <summary>
		/// The ENDENUM token ID.
		/// </summary>
		public const int ENDENUM = 79;

		/// <summary>
		/// The ENDFOR token ID.
		/// </summary>
		public const int ENDFOR = 80;

		/// <summary>
		/// The ENDFUNC token ID.
		/// </summary>
		public const int ENDFUNC = 81;

		/// <summary>
		/// The ENDGLOBALS token ID.
		/// </summary>
		public const int ENDGLOBALS = 82;

		/// <summary>
		/// The ENDHSMACTIVATE token ID.
		/// </summary>
		public const int ENDHSMACTIVATE = 83;

		/// <summary>
		/// The ENDHSMDEACTIVATE token ID.
		/// </summary>
		public const int ENDHSMDEACTIVATE = 84;

		/// <summary>
		/// The ENDHSMEVENT token ID.
		/// </summary>
		public const int ENDHSMEVENT = 85;

		/// <summary>
		/// The ENDHSMSTATE token ID.
		/// </summary>
		public const int ENDHSMSTATE = 86;

		/// <summary>
		/// The ENDIF token ID.
		/// </summary>
		public const int ENDIF = 87;

		/// <summary>
		/// The ENDPROC token ID.
		/// </summary>
		public const int ENDPROC = 88;

		/// <summary>
		/// The ENDREPEAT token ID.
		/// </summary>
		public const int ENDREPEAT = 89;

		/// <summary>
		/// The ENDSCRIPT token ID.
		/// </summary>
		public const int ENDSCRIPT = 90;

		/// <summary>
		/// The ENDSTRUCT token ID.
		/// </summary>
		public const int ENDSTRUCT = 91;

		/// <summary>
		/// The ENDSWITCH token ID.
		/// </summary>
		public const int ENDSWITCH = 92;

		/// <summary>
		/// The ENDWHILE token ID.
		/// </summary>
		public const int ENDWHILE = 93;

		/// <summary>
		/// The FALLTHRU token ID.
		/// </summary>
		public const int FALLTHRU = 94;

		/// <summary>
		/// The CATCH token ID.
		/// </summary>
		public const int CATCH = 95;

		/// <summary>
		/// The COUNTOF token ID.
		/// </summary>
		public const int COUNTOF = 96;

		/// <summary>
		/// The DEFAULTHSMSTATE token ID.
		/// </summary>
		public const int DEFAULTHSMSTATE = 97;

		/// <summary>
		/// The ENUMTOINT token ID.
		/// </summary>
		public const int ENUMTOINT = 98;

		/// <summary>
		/// The EXIT token ID.
		/// </summary>
		public const int EXIT = 99;

		/// <summary>
		/// The FORWARD token ID.
		/// </summary>
		public const int FORWARD = 100;

		/// <summary>
		/// The GOTO token ID.
		/// </summary>
		public const int GOTO = 101;

		/// <summary>
		/// The GOTOHSMSTATE token ID.
		/// </summary>
		public const int GOTOHSMSTATE = 102;

		/// <summary>
		/// The HSMHISTORY token ID.
		/// </summary>
		public const int HSMHISTORY = 103;

		/// <summary>
		/// The HSMSUPER token ID.
		/// </summary>
		public const int HSMSUPER = 104;

		/// <summary>
		/// The INTTOENUM token ID.
		/// </summary>
		public const int INTTOENUM = 105;

		/// <summary>
		/// The INTTONATIVE token ID.
		/// </summary>
		public const int INTTONATIVE = 106;

		/// <summary>
		/// The NATIVE token ID.
		/// </summary>
		public const int NATIVE = 107;

		/// <summary>
		/// The NATIVETOINT token ID.
		/// </summary>
		public const int NATIVETOINT = 108;

		/// <summary>
		/// The RELOOP token ID.
		/// </summary>
		public const int RELOOP = 109;

		/// <summary>
		/// The RETURN token ID.
		/// </summary>
		public const int RETURN = 110;

		/// <summary>
		/// The TIMESTEP token ID.
		/// </summary>
		public const int TIMESTEP = 111;

		/// <summary>
		/// The THROW token ID.
		/// </summary>
		public const int THROW = 112;

		/// <summary>
		/// The TO token ID.
		/// </summary>
		public const int TO = 113;

		/// <summary>
		/// The USING token ID.
		/// </summary>
		public const int USING = 114;

		/// <summary>
		/// The SIZEOF token ID.
		/// </summary>
		public const int SIZEOF = 115;

		/// <summary>
		/// The STEP token ID.
		/// </summary>
		public const int STEP = 116;

		/// <summary>
		/// The NULL token ID.
		/// </summary>
		public const int NULL = 117;

		/// <summary>
		/// The TYPEDEF token ID.
		/// </summary>
		public const int TYPEDEF = 118;

		/// <summary>
		/// The CALL token ID.
		/// </summary>
		public const int CALL = 119;

		/// <summary>
		/// The DEBUGONLY token ID.
		/// </summary>
		public const int DEBUGONLY = 120;

		/// <summary>
		/// The UNUSED token ID.
		/// </summary>
		public const int UNUSED = 121;

		/// <summary>
		/// The KeywordEnd token ID.
		/// </summary>
		public const int KeywordEnd = 122;

		/// <summary>
		/// The OpenString token ID.
		/// </summary>
		public const int OpenString = 123;

		/// <summary>
		/// The CloseString token ID.
		/// </summary>
		public const int CloseString = 124;

		/// <summary>
		/// The PunctuationStart token ID.
		/// </summary>
		public const int PunctuationStart = 125;

		/// <summary>
		/// The OpenParenthesis token ID.
		/// </summary>
		public const int OpenParenthesis = 126;

		/// <summary>
		/// The CloseParenthesis token ID.
		/// </summary>
		public const int CloseParenthesis = 127;

		/// <summary>
		/// The OpenVector token ID.
		/// </summary>
		public const int OpenVector = 128;

		/// <summary>
		/// The CloseVector token ID.
		/// </summary>
		public const int CloseVector = 129;

		/// <summary>
		/// The OpenBracket token ID.
		/// </summary>
		public const int OpenBracket = 130;

		/// <summary>
		/// The CloseBracket token ID.
		/// </summary>
		public const int CloseBracket = 131;

		/// <summary>
		/// The Comma token ID.
		/// </summary>
		public const int Comma = 132;

		/// <summary>
		/// The Period token ID.
		/// </summary>
		public const int Period = 133;

		/// <summary>
		/// The Ampersand token ID.
		/// </summary>
		public const int Ampersand = 134;

		/// <summary>
		/// The Colon token ID.
		/// </summary>
		public const int Colon = 135;

		/// <summary>
		/// The PunctuationEnd token ID.
		/// </summary>
		public const int PunctuationEnd = 136;

		/// <summary>
		/// The OperatorStart token ID.
		/// </summary>
		public const int OperatorStart = 137;

		/// <summary>
		/// The Pipe token ID.
		/// </summary>
		public const int Pipe = 138;

		/// <summary>
		/// The Caret token ID.
		/// </summary>
		public const int Caret = 139;

		/// <summary>
		/// The AssignmentOrEquality token ID.
		/// </summary>
		public const int AssignmentOrEquality = 140;

		/// <summary>
		/// The Inequality token ID.
		/// </summary>
		public const int Inequality = 141;

		/// <summary>
		/// The Addition token ID.
		/// </summary>
		public const int Addition = 142;

		/// <summary>
		/// The Subtraction token ID.
		/// </summary>
		public const int Subtraction = 143;

		/// <summary>
		/// The Multiplication token ID.
		/// </summary>
		public const int Multiplication = 144;

		/// <summary>
		/// The Division token ID.
		/// </summary>
		public const int Division = 145;

		/// <summary>
		/// The LessThanEqualTo token ID.
		/// </summary>
		public const int LessThanEqualTo = 146;

		/// <summary>
		/// The LessThan token ID.
		/// </summary>
		public const int LessThan = 147;

		/// <summary>
		/// The GreaterThanEqualTo token ID.
		/// </summary>
		public const int GreaterThanEqualTo = 148;

		/// <summary>
		/// The GreaterThan token ID.
		/// </summary>
		public const int GreaterThan = 149;

		/// <summary>
		/// The Mod token ID.
		/// </summary>
		public const int Mod = 150;

		/// <summary>
		/// The PlusTimestepMultipliedBy token ID.
		/// </summary>
		public const int PlusTimestepMultipliedBy = 151;

		/// <summary>
		/// The MinusTimestepMultipliedBy token ID.
		/// </summary>
		public const int MinusTimestepMultipliedBy = 152;

		/// <summary>
		/// The Increment token ID.
		/// </summary>
		public const int Increment = 153;

		/// <summary>
		/// The Decrement token ID.
		/// </summary>
		public const int Decrement = 154;

		/// <summary>
		/// The PlusEquals token ID.
		/// </summary>
		public const int PlusEquals = 155;

		/// <summary>
		/// The MinusEquals token ID.
		/// </summary>
		public const int MinusEquals = 156;

		/// <summary>
		/// The MultiplyEquals token ID.
		/// </summary>
		public const int MultiplyEquals = 157;

		/// <summary>
		/// The DivideEquals token ID.
		/// </summary>
		public const int DivideEquals = 158;

		/// <summary>
		/// The AltNOT token ID.
		/// </summary>
		public const int AltNOT = 159;

		/// <summary>
		/// The AltInequality token ID.
		/// </summary>
		public const int AltInequality = 160;

		/// <summary>
		/// The OperatorEnd token ID.
		/// </summary>
		public const int OperatorEnd = 161;

		/// <summary>
		/// The MaxTokenID token ID.
		/// </summary>
		public const int MaxTokenID = 162;

	}
	#endregion

	#region Lexical State IDs
	/// <summary>
	/// Contains the lexical state IDs for the <c>SanScript</c> language.
	/// </summary>
	public class SanScriptLexicalStateID {

		/// <summary>
		/// Returns the string-based key for the specified lexical state ID.
		/// </summary>
		/// <param name="id">The lexical state ID to examine.</param>
		public static string GetLexicalStateKey(int id) {
			System.Reflection.FieldInfo[] fields = typeof(SanScriptLexicalStateID).GetFields();
			foreach (System.Reflection.FieldInfo field in fields) {
				if ((field.IsStatic) && (field.IsLiteral) && (id.Equals(field.GetValue(null))))
					return field.Name;
			}
			return null;
		}

		/// <summary>
		/// The Default lexical state ID.
		/// </summary>
		public const int Default = 0;

		/// <summary>
		/// The StringState lexical state ID.
		/// </summary>
		public const int StringState = 1;

		/// <summary>
		/// The CommentState lexical state ID.
		/// </summary>
		public const int CommentState = 2;

		/// <summary>
		/// The MultiLineCommentState lexical state ID.
		/// </summary>
		public const int MultiLineCommentState = 3;

	}
	#endregion

	#region Semantic Parser
	/// <summary>
	/// Provides a semantic parser for the <c>SanScript</c> language.
	/// </summary>
	internal class SanScriptSemanticParser : ActiproSoftware.SyntaxEditor.ParserGenerator.RecursiveDescentSemanticParser {

		private CompilationUnit	m_compilationUnit;
		private System.Int32	m_scopeLevel;
		//private bool			m_inGlobals;

		/// <summary>
		/// Initializes a new instance of the <c>SanScriptSemanticParser</c> class.
		/// </summary>
		/// <param name="lexicalParser">The <see cref="ActiproSoftware.SyntaxEditor.ParserGenerator.IRecursiveDescentLexicalParser"/> to use for lexical parsing.</param>
		public SanScriptSemanticParser( ActiproSoftware.SyntaxEditor.ParserGenerator.IRecursiveDescentLexicalParser lexicalParser ) 
			: base( lexicalParser ) 
			{
			}
	
		/// <summary>
		/// Advances to the next <see cref="IToken"/>.
		/// </summary>
		/// <returns>
		/// The <see cref="IToken"/> that was read.
		/// </returns>
		protected override IToken AdvanceToNext() 
		{
			// FIXME: how to handle Case/Default/Break/Fallthru and If/Elif,Else
			
			IToken token = base.AdvanceToNext();
			if ( !this.TokenIsLanguageChange(token) ) 
			{
				switch ( token.ID ) 
				{
					case SanScriptTokenID.GLOBALS:
						//m_inGlobals = true;
						++m_scopeLevel;
						break;
					case SanScriptTokenID.HSMACTIVATE:
					case SanScriptTokenID.CASE:
					case SanScriptTokenID.HSMDEACTIVATE:
					case SanScriptTokenID.DEFAULT:
					case SanScriptTokenID.ENUM:
					case SanScriptTokenID.HASH_ENUM:
					case SanScriptTokenID.STRICT_ENUM:
					case SanScriptTokenID.HSMEVENT:
					case SanScriptTokenID.FOR:
					case SanScriptTokenID.FUNC:
					case SanScriptTokenID.IF:
					case SanScriptTokenID.PROC:
					case SanScriptTokenID.REPEAT:
					case SanScriptTokenID.HSMSTATE:
					case SanScriptTokenID.SCRIPT:
					case SanScriptTokenID.STRUCT:
					case SanScriptTokenID.SWITCH:
					case SanScriptTokenID.WHILE:
						++m_scopeLevel;
						break;
					case SanScriptTokenID.ELIF:
					case SanScriptTokenID.ELSE:
						// do nothing
						break;
					case SanScriptTokenID.ENDGLOBALS:
						//m_inGlobals = false;
						--m_scopeLevel;
						break;
					case SanScriptTokenID.BREAK:
					case SanScriptTokenID.ENDHSMACTIVATE:
					case SanScriptTokenID.ENDHSMDEACTIVATE:
					case SanScriptTokenID.ENDENUM:
					case SanScriptTokenID.ENDHSMEVENT:
					case SanScriptTokenID.ENDFOR:
					case SanScriptTokenID.ENDFUNC:
					case SanScriptTokenID.ENDIF:
					case SanScriptTokenID.ENDPROC:
					case SanScriptTokenID.ENDREPEAT:
					case SanScriptTokenID.ENDSCRIPT:
					case SanScriptTokenID.ENDHSMSTATE:
					case SanScriptTokenID.ENDSTRUCT:
					case SanScriptTokenID.ENDSWITCH:
					case SanScriptTokenID.ENDWHILE:
					case SanScriptTokenID.FALLTHRU:
					//case SanScriptTokenID.PoundEndif:
						--m_scopeLevel;
						break;
				}
			}
			
			return token;
		}

		/// <summary>
		/// Advances to (or past) the next matching close token.
		/// </summary>
		/// <param name="scopeStartTokenId"></param>
		/// <param name="scopeEndTokenId"></param>
		/// <param name="openScopeLevel">The level of the open token.</param>		
		/// <param name="movePast">Whether to move past the close token.</param>
		private void AdvanceToNextScopeEnding( System.Int32 scopeStartTokenId, System.Int32[] scopeEndTokenIds, System.Int32 openScopeLevel, System.Boolean movePast ) 
		{			
			// FIXME: how to handle Case/Default/Break/Fallthru and If/Elif,Else
			while ( !this.IsAtEnd ) 
			{
				this.AdvanceToNext( scopeStartTokenId );
				this.AdvanceToNext( scopeEndTokenIds );
				
				bool finished = false;
				foreach ( System.Int32 scopeEndTokenId in scopeEndTokenIds )
				{
					if ( this.TokenIs( this.LookAheadToken, scopeEndTokenId ) && (m_scopeLevel <= openScopeLevel + 1) )
					{
						finished = true;
						break;
					}					
				}
				
				if ( finished )
				{	
					if ( movePast )
					{
						this.AdvanceToNext();
					}
					break;
				}								
			}		
		}
	
		/// <summary>
		/// Gets the <see cref="CompilationUnit"/> that was parsed.
		/// </summary>
		/// <value>The <see cref="CompilationUnit"/> that was parsed.</value>
		public CompilationUnit CompilationUnit 
		{
			get 
			{
				return m_compilationUnit;
			}
		}
	
		/// <summary>
		/// Reports a syntax error.
		/// </summary>
		/// <param name="textRange">The <see cref="TextRange"/> of the error.</param>
		/// <param name="message">The error message.</param>
		protected override void ReportSyntaxError( TextRange textRange, System.String message )
		{		
			// Don't allow multiple errors at the same offset
			if ( (m_compilationUnit.SyntaxErrors.Count > 0) && (((SyntaxError)m_compilationUnit.SyntaxErrors[m_compilationUnit.SyntaxErrors.Count - 1]).TextRange.StartOffset == textRange.StartOffset) )
			{
				return;
			}
			
			m_compilationUnit.SyntaxErrors.Add( new SyntaxError(textRange, message) );
		}
		
		private bool SortCompilationUnitStatements( IAstNodeList statements )
		{
			bool isCompilationUnit = false;
			
			foreach ( AstNode node in statements )
			{
				switch ( node.NodeType )
				{
					case SanScriptNodeType.ConstDefinitionStatement:
						isCompilationUnit = true;
						m_compilationUnit.Constants.Add( node );
						break;
					case SanScriptNodeType.NativeTypeDeclarationStatement:
					case SanScriptNodeType.ForwardEnumDeclarationStatement:
					case SanScriptNodeType.ForwardStructDeclarationStatement:
					case SanScriptNodeType.StructDefinitionBlockStatement:
					case SanScriptNodeType.EnumDefinitionBlockStatement:
					case SanScriptNodeType.HashEnumDefinitionBlockStatement:
					case SanScriptNodeType.StrictEnumDefinitionBlockStatement:
					case SanScriptNodeType.TypedefFuncDeclarationStatement:
					case SanScriptNodeType.TypedefProcDeclarationStatement:
						isCompilationUnit = true;
						m_compilationUnit.DataTypes.Add( node );
						break;
					case SanScriptNodeType.VariableArrayDeclarationStatement:
					case SanScriptNodeType.VariableNonArrayDeclarationStatement:
						{
							isCompilationUnit = true;
							m_compilationUnit.StaticVariables.Add( node );
							
							// fixup Identifer DeclarationType
							VariableDeclarationStatement var = node as VariableDeclarationStatement;
							while ( var != null )
							{
								Identifier name = var.Name;
								if ( name != null )
								{
									name.DeclareType = Identifier.DeclarationType.User;
								}
								
								var = var.SubDeclaration as VariableDeclarationStatement;
							}
						}
						break;
					case SanScriptNodeType.UsingStatement:
						isCompilationUnit = true;
						m_compilationUnit.UsingStatements.Add( node );					
						break;
					case SanScriptNodeType.NativeEventDeclarationStatement:
					case SanScriptNodeType.NativeFuncDeclarationStatement:
					case SanScriptNodeType.NativeProcDeclarationStatement:
					case SanScriptNodeType.EventDefinitionBlockStatement:
					case SanScriptNodeType.FuncDefinitionBlockStatement:
					case SanScriptNodeType.ProcDefinitionBlockStatement:
					case SanScriptNodeType.ScriptBlockStatement:
						isCompilationUnit = true;
						m_compilationUnit.Subroutines.Add( node );
						break;						
					case SanScriptNodeType.StateDefinitionBlockStatement:
						m_compilationUnit.States.Add( node );
						break;
					case SanScriptNodeType.GlobalsDefinitionBlockStatement:
						{
							m_compilationUnit.GlobalsSection.Add( node );
							
							// fixup Identifer DeclarationType
							IAstNodeList statementList = ((GlobalsDefinitionBlockStatement)node).Statements;
							foreach ( IAstNode stmt in statementList )
							{
								if ( stmt is VariableDeclarationStatement )
								{
									// fixup Identifer DeclarationType
									VariableDeclarationStatement var = stmt as VariableDeclarationStatement;
									while ( var != null )
									{
										Identifier name = var.Name;
										if ( name != null )
										{
											name.DeclareType = Identifier.DeclarationType.User;
										}
										
										var = var.SubDeclaration as VariableDeclarationStatement;
									}
								}
							}
						}
						break;
					default:
						m_compilationUnit.OtherStatements.Add( node );
						break;
				}
			}
			
			return isCompilationUnit;
		}
				
		bool IsComment( out bool isPurpose )
		{
			isPurpose = false;
			bool match = false;
			
			this.StartPeek();
			
			IToken peekToken = this.Peek();
			if ( this.TokenIs( peekToken, SanScriptTokenID.SingleLineCommentStart ) )
			{
				peekToken = this.Peek();
				if ( this.TokenIs( peekToken, SanScriptTokenID.SingleLineComment ) )
				{
					string peekText = this.GetTokenText( peekToken );
					if ( peekText.StartsWith( "/ PURPOSE:" ) || peekText.StartsWith( "PURPOSE:" ) ) 
					{ 
						isPurpose = true; 
					}
					
					match = true;
				}
				else if ( this.TokenIs( peekToken, SanScriptTokenID.SingleLineCommentEnd ) )
				{
					match = true;
				}
			}
			
			this.StopPeek();
			return match;
		}

		/// <summary>
		/// Parses the document and generates a document object model.
		/// </summary>
		public void Parse() {
			this.MatchCompilationUnit();
		}

		/// <summary>
		/// Matches a <c>CompilationUnit</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>CompilationUnit</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>BOOL</c>, <c>FLOAT</c>, <c>TWEAK_FLOAT</c>, <c>INT</c>, <c>TWEAK_INT</c>, <c>VECTOR</c>, <c>STRING</c>, <c>VARARGS</c>, <c>TEXTLABEL</c>, <c>TEXTLABELTHREE</c>, <c>TEXTLABELSEVEN</c>, <c>TEXTLABELFIFTEEN</c>, <c>TEXTLABELTWENTYTHREE</c>, <c>TEXTLABELTHIRTYONE</c>, <c>TEXTLABELSIXTYTHREE</c>, <c>ENUMTOINTdatatype</c>, <c>StructArgument</c>, <c>DEFAULT</c>, <c>CALL</c>, <c>PoundIf</c>, <c>PoundEndif</c>, <c>CONSTINT</c>, <c>CONSTFLOAT</c>, <c>Increment</c>, <c>Decrement</c>, <c>HSMSUPER</c>, <c>FORWARD</c>, <c>ENUM</c>, <c>STRUCT</c>, <c>USING</c>, <c>THROW</c>, <c>GOTO</c>, <c>GOTOHSMSTATE</c>, <c>DEFAULTHSMSTATE</c>, <c>HSMHISTORY</c>, <c>BREAK</c>, <c>FALLTHRU</c>, <c>RETURN</c>, <c>EXIT</c>, <c>BREAKLOOP</c>, <c>RELOOP</c>, <c>IF</c>, <c>ELIF</c>, <c>ELSE</c>, <c>WHILE</c>, <c>REPEAT</c>, <c>FOR</c>, <c>CASE</c>, <c>SWITCH</c>, <c>HSMSTATE</c>, <c>HSMACTIVATE</c>, <c>HSMDEACTIVATE</c>, <c>HASH_ENUM</c>, <c>STRICT_ENUM</c>, <c>GLOBALS</c>, <c>SCRIPT</c>, <c>PROC</c>, <c>HSMEVENT</c>, <c>FUNC</c>, <c>NATIVE</c>, <c>TYPEDEF</c>, <c>MultiLineCommentStart</c>, <c>SingleLineCommentStart</c>.
		/// </remarks>
		protected virtual bool MatchCompilationUnit() {
			m_scopeLevel = 0;
			m_compilationUnit = new CompilationUnit();
			m_compilationUnit.StartOffset = this.LookAheadToken.StartOffset;
			
			AstNodeList statements = new AstNodeList( null );
			
			// gather the top-level statements
			while ( !this.IsAtEnd )
				{
				Statement stmt = null;
				if ( this.IsInMultiMatchSet(0, this.LookAheadToken) )
					{
					if (!this.MatchStatement(out stmt)) {
						this.AdvanceToNext();
					}
					else {
						if ( stmt != null ) statements.Add( stmt );
					}
				}
				else
					{
					this.AdvanceToNext();
				}
			}
			
			// Sort the statements for easier intellisense retrieval
			if ( !SortCompilationUnitStatements( statements ) )
				{
				this.ReportSyntaxError( "Empty or invalid compilation unit." );
			}
			
			m_compilationUnit.EndOffset = this.LookAheadToken.EndOffset;
			return true;
		}

		/// <summary>
		/// Matches a <c>Identifier</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>Identifier</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>.
		/// </remarks>
		protected virtual bool MatchIdentifier(out Identifier id) {
			id = null;
			Identifier.DeclarationType declareType = Identifier.DeclarationType.Local;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Identifier)) {
				if (!this.Match(SanScriptTokenID.Identifier))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.UserIdentifier)) {
				if (!this.Match(SanScriptTokenID.UserIdentifier))
					return false;
				else {
					declareType = Identifier.DeclarationType.User;
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.NativeIdentifier)) {
				if (!this.Match(SanScriptTokenID.NativeIdentifier))
					return false;
				else {
					declareType = Identifier.DeclarationType.Native;
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.TypedefIdentifier)) {
				if (!this.Match(SanScriptTokenID.TypedefIdentifier))
					return false;
				else {
					declareType = Identifier.DeclarationType.Typedef;
				}
			}
			else
				return false;
			id = new Identifier( this.TokenText, declareType, this.Token.TextRange );
			return true;
		}

		/// <summary>
		/// Matches a <c>NativeDataType</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>NativeDataType</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>BOOL</c>, <c>FLOAT</c>, <c>TWEAK_FLOAT</c>, <c>INT</c>, <c>TWEAK_INT</c>, <c>VECTOR</c>, <c>STRING</c>, <c>VARARGS</c>, <c>TEXTLABEL</c>, <c>TEXTLABELTHREE</c>, <c>TEXTLABELSEVEN</c>, <c>TEXTLABELFIFTEEN</c>, <c>TEXTLABELTWENTYTHREE</c>, <c>TEXTLABELTHIRTYONE</c>, <c>TEXTLABELSIXTYTHREE</c>, <c>ENUMTOINTdatatype</c>, <c>StructArgument</c>.
		/// </remarks>
		protected virtual bool MatchNativeDataType(out Identifier id, bool allowEnumToIntDatatype, bool allowStructArgument) {
			id = null;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.BOOL)) {
				if (!this.Match(SanScriptTokenID.BOOL))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.FLOAT)) {
				if (!this.Match(SanScriptTokenID.FLOAT))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.TWEAK_FLOAT)) {
				if (!this.Match(SanScriptTokenID.TWEAK_FLOAT))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.INT)) {
				if (!this.Match(SanScriptTokenID.INT))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.TWEAK_INT)) {
				if (!this.Match(SanScriptTokenID.TWEAK_INT))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.VECTOR)) {
				if (!this.Match(SanScriptTokenID.VECTOR))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.STRING)) {
				if (!this.Match(SanScriptTokenID.STRING))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.VARARGS)) {
				if (!this.Match(SanScriptTokenID.VARARGS))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.TEXTLABEL)) {
				if (!this.Match(SanScriptTokenID.TEXTLABEL))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.TEXTLABELTHREE)) {
				if (!this.Match(SanScriptTokenID.TEXTLABELTHREE))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.TEXTLABELSEVEN)) {
				if (!this.Match(SanScriptTokenID.TEXTLABELSEVEN))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.TEXTLABELFIFTEEN)) {
				if (!this.Match(SanScriptTokenID.TEXTLABELFIFTEEN))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.TEXTLABELTWENTYTHREE)) {
				if (!this.Match(SanScriptTokenID.TEXTLABELTWENTYTHREE))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.TEXTLABELTHIRTYONE)) {
				if (!this.Match(SanScriptTokenID.TEXTLABELTHIRTYONE))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.TEXTLABELSIXTYTHREE)) {
				if (!this.Match(SanScriptTokenID.TEXTLABELSIXTYTHREE))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.ENUMTOINTdatatype)) {
				if (!this.Match(SanScriptTokenID.ENUMTOINTdatatype))
					return false;
				else {
				if ( !allowEnumToIntDatatype ) { this.ReportSyntaxError( "( expected." ); return false; }
			}
		}
		else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.StructArgument)) {
			if (!this.Match(SanScriptTokenID.StructArgument))
				return false;
			else {
			if ( !allowStructArgument ) { this.ReportSyntaxError( "Identifier expected." ); return false; }
		}
	}
	else
		return false;
	id = new Identifier( this.TokenText, Identifier.DeclarationType.NativeType, this.Token.TextRange );
			return true;
		}

		/// <summary>
		/// Matches a <c>DefaultExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>DefaultExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>DEFAULT</c>.
		/// </remarks>
		protected virtual bool MatchDefaultExpression(out Expression expr) {
			expr = null;
			if (!this.Match(SanScriptTokenID.DEFAULT))
				return false;
			--m_scopeLevel;	// There are two uses of 'DEFAULT', one of which creates a new scope in a SWITCH statement.  This code specifically handles the value in a PROC of FUNC, so undo that change.
			expr = new DefaultExpression();
			return true;
		}

		/// <summary>
		/// Matches a <c>VectorExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>VectorExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>OpenVector</c>.
		/// </remarks>
		protected virtual bool MatchVectorExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression value;
			AstNodeList values = new AstNodeList( null );
			if (!this.Match(SanScriptTokenID.OpenVector))
				return false;
			if (this.MatchExpression(out value)) {
				values.Add( value );
			}
			this.Match(SanScriptTokenID.Comma);
			if (this.MatchExpression(out value)) {
				values.Add( value );
			}
			this.Match(SanScriptTokenID.Comma);
			if (this.MatchExpression(out value)) {
				values.Add( value );
			}
			if (!this.Match(SanScriptTokenID.CloseVector)) {
				this.ReportSyntaxError( "Closing vector expected." );
			}
			if ( values.Count < 3 )
				{
				this.ReportSyntaxError( "Three comma-separated expressions expected." );
			}
			
			expr = new VectorExpression( values, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>StringExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>StringExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>OpenString</c>.
		/// </remarks>
		protected virtual bool MatchStringExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			string text = string.Empty;
			if (!this.Match(SanScriptTokenID.OpenString))
				return false;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.StringValue)) {
				if (this.Match(SanScriptTokenID.StringValue)) {
					text = this.TokenText;
				}
			}
			if (!this.Match(SanScriptTokenID.CloseString)) {
				this.ReportSyntaxError( "Ending quotation mark expected." );
			}
			expr = new StringExpression( text, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>NativeTypeExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>NativeTypeExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>DEFAULT</c>, <c>OpenVector</c>, <c>OpenString</c>, <c>TRUE</c>, <c>FALSE</c>, <c>IntegerValue</c>, <c>FloatValue</c>, <c>NULL</c>.
		/// </remarks>
		protected virtual bool MatchNativeTypeExpression(out Expression expr) {
			expr = null;
			bool isInteger = false;
			bool isFloat = false;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.TRUE)) {
				if (!this.Match(SanScriptTokenID.TRUE))
					return false;
				else {
					expr = new BooleanExpression( true, this.Token.TextRange );
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.FALSE)) {
				if (!this.Match(SanScriptTokenID.FALSE))
					return false;
				else {
					expr = new BooleanExpression( false, this.Token.TextRange );
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.IntegerValue)) {
				if (!this.Match(SanScriptTokenID.IntegerValue))
					return false;
				else {
					isInteger = true;
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.FloatValue)) {
				if (!this.Match(SanScriptTokenID.FloatValue))
					return false;
				else {
					isFloat = true;
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.NULL)) {
				if (!this.Match(SanScriptTokenID.NULL))
					return false;
				else {
					expr = new NullExpression( this.Token.TextRange );
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.DEFAULT)) {
				if (!this.MatchDefaultExpression(out expr))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenVector)) {
				if (!this.MatchVectorExpression(out expr))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenString)) {
				if (!this.MatchStringExpression(out expr))
					return false;
			}
			else
				return false;
			if ( isInteger )
				{
				int value = -1;
				try
				{
					value = int.Parse( this.TokenText );
				}
				catch ( OverflowException )
					{
					value = int.MaxValue;
					this.ReportSyntaxError( this.Token.TextRange, "Integer value is out of range." );
				}
				finally
				{
					expr = new IntegerExpression( value, this.Token.TextRange );
				}
			}
			else if ( isFloat )
				{
				float value = -1.0f;
				try
				{
					value = float.Parse( this.TokenText );
				}
				catch ( OverflowException )
					{
					value = float.MaxValue;
					this.ReportSyntaxError( this.Token.TextRange, "Float value is out of range." );
				}
				finally
				{
					expr = new FloatExpression( value, this.Token.TextRange );
				}
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>SubroutineAccessArgumentList</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>SubroutineAccessArgumentList</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>OpenParenthesis</c>.
		/// </remarks>
		protected virtual bool MatchSubroutineAccessArgumentList(AstNodeList argumentList) {
			Expression arg;
			if (!this.Match(SanScriptTokenID.OpenParenthesis)) {
				this.ReportSyntaxError( "Missing open parenthesis." );
			}
			if (this.IsInMultiMatchSet(1, this.LookAheadToken)) {
				if (!this.MatchExpression(out arg))
					return false;
				else {
					argumentList.Add( arg );
				}
				while (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Comma)) {
					if (!this.Match(SanScriptTokenID.Comma))
						return false;
					if (this.MatchExpression(out arg)) {
						argumentList.Add( arg );
					}
				}
			}
			if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
				this.ReportSyntaxError( "Closing parenthesis expected." );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>ArrayAccessExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>ArrayAccessExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>OpenBracket</c>.
		/// </remarks>
		protected virtual bool MatchArrayAccessExpression(out Expression expr, out Expression subExpr) {
			expr = null;
			subExpr = null;
			System.Int32 startOffset = 0;
			AstNodeList argumentList = new AstNodeList( null );
			Expression accessExpr = null;
			Expression subSubExpr = null;
			bool isSubroutine = false;
			bool isArrayAccess = false;
			bool isStructAccess = false;
			if (!this.Match(SanScriptTokenID.OpenBracket)) {
				this.ReportSyntaxError( "Missing open bracket." );
			}
			if (this.IsInMultiMatchSet(1, this.LookAheadToken)) {
				if (!this.MatchExpression(out expr)) {
					this.ReportSyntaxError( "Invalid array indexer." );
				}
			}
			if (!this.Match(SanScriptTokenID.CloseBracket)) {
				this.ReportSyntaxError( "Closing bracket expected." );
			}
			if (((this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenParenthesis)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenBracket)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Period)))) {
				startOffset = this.LookAheadToken.StartOffset;
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenParenthesis)) {
					if (!this.MatchSubroutineAccessArgumentList(argumentList)) {
						this.ReportSyntaxError( "Invalid argument list." );
					}
					else {
						isSubroutine = true;
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenBracket)) {
					if (this.MatchArrayAccessExpression(out accessExpr, out subSubExpr)) {
						isArrayAccess = true;
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Period)) {
					if (this.MatchStructAccessExpression(out accessExpr)) {
						isStructAccess = true;
					}
				}
				else
					return false;
			}
			if ( isSubroutine )
				{
				// wrap it in an SubroutineAccessExpression with a null Identifier for display purposes
				subExpr = new SubroutineAccessExpression( false, argumentList, null, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			else if ( isArrayAccess )
				{
				// wrap it in an ArrayAccessExpression with a null Identifier for display purposes
				subExpr = new ArrayAccessExpression( false, accessExpr, subSubExpr, null, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			else if ( isStructAccess )
				{
				// wrap it in an StructMemberAccessExpression with a null Identifier for display purposes
				subExpr = new StructMemberAccessExpression( false, accessExpr, null, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>StructAccessExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>StructAccessExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Period</c>.
		/// </remarks>
		protected virtual bool MatchStructAccessExpression(out Expression expr) {
			expr = null;
			if (!this.Match(SanScriptTokenID.Period))
				return false;
			this.MatchIdentifierExpression(out expr);
			return true;
		}

		/// <summary>
		/// Matches a <c>IdentifierExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>IdentifierExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>CALL</c>.
		/// </remarks>
		protected virtual bool MatchIdentifierExpression(out Expression expr) {
			expr = null;
			Identifier id = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			
			AstNodeList argumentList = new AstNodeList( null );
			Expression accessExpr = null;
			Expression subExpr = null;
			bool isCall = false;
			bool isSubroutine = false;
			bool isArrayAccess = false;
			bool isStructAccess = false;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.CALL)) {
				if (this.Match(SanScriptTokenID.CALL)) {
					isCall = true;
				}
			}
			if (!this.MatchIdentifier(out id))
				return false;
			if (((this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenParenthesis)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenBracket)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Period)))) {
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenParenthesis)) {
					if (!this.MatchSubroutineAccessArgumentList(argumentList)) {
						this.ReportSyntaxError( "Invalid argument list." );
					}
					else {
						isSubroutine = true;
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenBracket)) {
					if (this.MatchArrayAccessExpression(out accessExpr, out subExpr)) {
						isArrayAccess = true;
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Period)) {
					if (this.MatchStructAccessExpression(out accessExpr)) {
						isStructAccess = true;
					}
				}
				else
					return false;
			}
			if ( isSubroutine )
				{
				expr = new SubroutineAccessExpression( isCall, argumentList, id, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			else if ( isArrayAccess )
				{
				expr = new ArrayAccessExpression( isCall, accessExpr, subExpr, id, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			else if ( isStructAccess )
				{
				expr = new StructMemberAccessExpression( isCall, accessExpr, id, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			else
				{
				if ( isCall )
					{
					this.ReportSyntaxError( "The CALL keyword can only be used to invoke TYPEDEF'd PROCs or FUNCS." );
					return false;
				}
				
				expr = new VariableExpression( id, this.Token.TextRange );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>CountOfExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>CountOfExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>COUNTOF</c>.
		/// </remarks>
		protected virtual bool MatchCountOfExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			bool parens = false;
			Expression arg = null;
			if (!this.Match(SanScriptTokenID.COUNTOF))
				return false;
			if (this.Match(SanScriptTokenID.OpenParenthesis)) {
				parens = true;
			}
			if (this.MatchIdentifierExpression(out arg)) {
				if (!(arg is ArrayAccessExpression) && !(arg is StructMemberAccessExpression) && !(arg is VariableExpression)) this.ReportSyntaxError("COUNT_OF can only iterate through arrays and enums.");
			}
			if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
				if ( parens ) this.ReportSyntaxError( "Closing parenthesis expected." );
			}
			else {
				if ( !parens ) this.ReportSyntaxError( "Missing opening parenthesis." );
			}
			expr = new CountOfExpression( arg, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>EnumToIntExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>EnumToIntExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>ENUMTOINT</c>.
		/// </remarks>
		protected virtual bool MatchEnumToIntExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			bool parens = false;
			Expression arg = null;
			if (!this.Match(SanScriptTokenID.ENUMTOINT))
				return false;
			if (this.Match(SanScriptTokenID.OpenParenthesis)) {
				parens = true;
			}
			this.MatchExpression(out arg);
			if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
				if ( parens ) this.ReportSyntaxError( "Closing parenthesis expected." );
			}
			else {
				if ( !parens ) this.ReportSyntaxError( "Missing opening parenthesis." );
			}
			expr = new EnumToIntExpression( arg, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>IntToEnumExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>IntToEnumExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>INTTOENUM</c>.
		/// </remarks>
		protected virtual bool MatchIntToEnumExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			bool parens = false;
			Identifier enumType = null;
			Expression exprToConvert = null;
			if (!this.Match(SanScriptTokenID.INTTOENUM))
				return false;
			if (this.Match(SanScriptTokenID.OpenParenthesis)) {
				parens = true;
			}
			this.MatchIdentifier(out enumType);
			this.Match(SanScriptTokenID.Comma);
			this.MatchExpression(out exprToConvert);
			if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
				if ( parens ) this.ReportSyntaxError( "Closing parenthesis expected." );
			}
			else {
				if ( !parens ) this.ReportSyntaxError( "Missing opening parenthesis." );
			}
			expr = new IntToEnumExpression( enumType, exprToConvert, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>IntToNativeExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>IntToNativeExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>INTTONATIVE</c>.
		/// </remarks>
		protected virtual bool MatchIntToNativeExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			bool parens = false;
			Identifier nativeType = null;
			Expression exprToConvert = null;
			if (!this.Match(SanScriptTokenID.INTTONATIVE))
				return false;
			if (this.Match(SanScriptTokenID.OpenParenthesis)) {
				parens = true;
			}
			this.MatchIdentifier(out nativeType);
			this.Match(SanScriptTokenID.Comma);
			this.MatchExpression(out exprToConvert);
			if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
				if ( parens ) this.ReportSyntaxError( "Closing parenthesis expected." );
			}
			else {
				if ( !parens ) this.ReportSyntaxError( "Missing opening parenthesis." );
			}
			expr = new IntToNativeExpression( nativeType, exprToConvert, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>NativeToIntExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>NativeToIntExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>NATIVETOINT</c>.
		/// </remarks>
		protected virtual bool MatchNativeToIntExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			bool parens = false;
			Expression arg = null;
			if (!this.Match(SanScriptTokenID.NATIVETOINT))
				return false;
			if (this.Match(SanScriptTokenID.OpenParenthesis)) {
				parens = true;
			}
			this.MatchExpression(out arg);
			if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
				if ( parens ) this.ReportSyntaxError( "Closing parenthesis expected." );
			}
			else {
				if ( !parens ) this.ReportSyntaxError( "Missing opening parenthesis." );
			}
			expr = new NativeToIntExpression( arg, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>SizeOfExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>SizeOfExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>SIZEOF</c>.
		/// </remarks>
		protected virtual bool MatchSizeOfExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			bool parens = false;
			Expression arg = null;
			if (!this.Match(SanScriptTokenID.SIZEOF))
				return false;
			if (this.Match(SanScriptTokenID.OpenParenthesis)) {
				parens = true;
			}
			this.MatchExpression(out arg);
			if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
				if ( parens ) this.ReportSyntaxError( "Closing parenthesis expected." );
			}
			else {
				if ( !parens ) this.ReportSyntaxError( "Missing opening parenthesis." );
			}
			expr = new SizeOfExpression( arg, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>HashExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>HashExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>HASH</c>.
		/// </remarks>
		protected virtual bool MatchHashExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression arg = null;
			if (!this.Match(SanScriptTokenID.HASH))
				return false;
			if (!this.Match(SanScriptTokenID.OpenParenthesis)) {
				this.ReportSyntaxError( "Missing open parenthesis." );
			}
			if (!this.MatchStringExpression(out arg)) {
				this.ReportSyntaxError( "Missing string constant." );
			}
			if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
				this.ReportSyntaxError( "Closing parenthesis expected." );
			}
			expr = new HashExpression( arg, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>CatchExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>CatchExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>CATCH</c>.
		/// </remarks>
		protected virtual bool MatchCatchExpression(out Expression expr) {
			expr = null;
			if (!this.Match(SanScriptTokenID.CATCH))
				return false;
			expr = new CatchExpression( this.Token.TextRange );
			return true;
		}

		/// <summary>
		/// Matches a <c>TimestepExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>TimestepExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>TIMESTEP</c>.
		/// </remarks>
		protected virtual bool MatchTimestepExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			if (!this.Match(SanScriptTokenID.TIMESTEP))
				return false;
			if (!this.Match(SanScriptTokenID.OpenParenthesis)) {
				this.ReportSyntaxError( "Missing open parenthesis." );
			}
			if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
				this.ReportSyntaxError( "Closing parenthesis expected." );
			}
			expr = new TimestepExpression( new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>PreprocessorMacroExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>PreprocessorMacroExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>PoundIf</c>, <c>PoundEndif</c>.
		/// </remarks>
		protected virtual bool MatchPreprocessorMacroExpression(out Expression expr) {
			expr = null;
			System.Int32 tokenID = this.LookAheadToken.ID;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier name = null;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.PoundIf)) {
				if (!this.Match(SanScriptTokenID.PoundIf))
					return false;
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.NOT)) {
					this.Match(SanScriptTokenID.NOT);
				}
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.DEFINED)) {
					this.Match(SanScriptTokenID.DEFINED);
				}
				if (this.IsInMultiMatchSet(2, this.LookAheadToken)) {
					if (!this.MatchIdentifier(out name)) {
						this.ReportSyntaxError( "Identifier expected" );
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenParenthesis)) {
					if (!this.Match(SanScriptTokenID.OpenParenthesis)) {
						this.ReportSyntaxError( "Missing open parenthesis." );
					}
					if (!this.MatchIdentifier(out name)) {
						this.ReportSyntaxError( "Identifier expected" );
					}
					if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
						this.ReportSyntaxError( "Closing parenthesis expected." );
					}
				}
				else
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.PoundEndif)) {
				if (!this.Match(SanScriptTokenID.PoundEndif))
					return false;
			}
			else
				return false;
			expr = new PreprocessorMacroExpression( tokenID, name, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>FuncAddrExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>FuncAddrExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Ampersand</c>.
		/// </remarks>
		protected virtual bool MatchFuncAddrExpression(out Expression expr) {
			expr = null;
			Expression funcProcExpr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			if (!this.Match(SanScriptTokenID.Ampersand))
				return false;
			if (!this.MatchExpression(out funcProcExpr))
				return false;
			expr = new FuncAddrExpression( funcProcExpr, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>PrimaryExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>PrimaryExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>DEFAULT</c>, <c>OpenVector</c>, <c>OpenString</c>, <c>TRUE</c>, <c>FALSE</c>, <c>IntegerValue</c>, <c>FloatValue</c>, <c>NULL</c>, <c>OpenParenthesis</c>, <c>CALL</c>, <c>COUNTOF</c>, <c>ENUMTOINT</c>, <c>INTTOENUM</c>, <c>INTTONATIVE</c>, <c>NATIVETOINT</c>, <c>SIZEOF</c>, <c>HASH</c>, <c>CATCH</c>, <c>TIMESTEP</c>, <c>PoundIf</c>, <c>PoundEndif</c>, <c>Ampersand</c>.
		/// </remarks>
		protected virtual bool MatchPrimaryExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			if (this.IsInMultiMatchSet(3, this.LookAheadToken)) {
				if (!this.MatchNativeTypeExpression(out expr))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.COUNTOF)) {
				if (!this.MatchCountOfExpression(out expr))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.ENUMTOINT)) {
				if (!this.MatchEnumToIntExpression(out expr))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.INTTOENUM)) {
				if (!this.MatchIntToEnumExpression(out expr))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.INTTONATIVE)) {
				if (!this.MatchIntToNativeExpression(out expr))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.NATIVETOINT)) {
				if (!this.MatchNativeToIntExpression(out expr))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.SIZEOF)) {
				if (!this.MatchSizeOfExpression(out expr))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.HASH)) {
				if (!this.MatchHashExpression(out expr))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.CATCH)) {
				if (!this.MatchCatchExpression(out expr))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.TIMESTEP)) {
				if (!this.MatchTimestepExpression(out expr))
					return false;
			}
			else if (this.IsInMultiMatchSet(4, this.LookAheadToken)) {
				if (!this.MatchIdentifierExpression(out expr))
					return false;
			}
			else if (((this.TokenIs(this.LookAheadToken, SanScriptTokenID.PoundIf)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.PoundEndif)))) {
				if (!this.MatchPreprocessorMacroExpression(out expr))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Ampersand)) {
				if (!this.MatchFuncAddrExpression(out expr))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenParenthesis)) {
				if (!this.Match(SanScriptTokenID.OpenParenthesis))
					return false;
				this.MatchExpression(out expr);
				if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
					this.ReportSyntaxError( "Closing parenthesis expected." );
				}
				expr = new ParenthesizedExpression( expr, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			else
				return false;
			return true;
		}

		/// <summary>
		/// Matches a <c>UnaryExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>UnaryExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>DEFAULT</c>, <c>OpenVector</c>, <c>OpenString</c>, <c>TRUE</c>, <c>FALSE</c>, <c>IntegerValue</c>, <c>FloatValue</c>, <c>NULL</c>, <c>OpenParenthesis</c>, <c>CALL</c>, <c>COUNTOF</c>, <c>ENUMTOINT</c>, <c>INTTOENUM</c>, <c>INTTONATIVE</c>, <c>NATIVETOINT</c>, <c>SIZEOF</c>, <c>HASH</c>, <c>CATCH</c>, <c>TIMESTEP</c>, <c>PoundIf</c>, <c>NOT</c>, <c>PoundEndif</c>, <c>Ampersand</c>, <c>Subtraction</c>, <c>AltNOT</c>.
		/// </remarks>
		protected virtual bool MatchUnaryExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression rightExpr = null;
			string op = null;
			if (((this.TokenIs(this.LookAheadToken, SanScriptTokenID.NOT)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Subtraction)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.AltNOT)))) {
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Subtraction)) {
					if (!this.Match(SanScriptTokenID.Subtraction))
						return false;
					else {
						op = "-";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.NOT)) {
					if (!this.Match(SanScriptTokenID.NOT))
						return false;
					else {
						op = "NOT";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.AltNOT)) {
					if (!this.Match(SanScriptTokenID.AltNOT))
						return false;
					else {
						op = "!";
					}
				}
				else
					return false;
			}
			if (!this.MatchPrimaryExpression(out rightExpr))
				return false;
			if ( op == null )
				{
				expr = rightExpr;
			}
			else
				{
				expr = new BinaryExpression( null, op, rightExpr, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>MultiplicativeExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>MultiplicativeExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>DEFAULT</c>, <c>OpenVector</c>, <c>OpenString</c>, <c>TRUE</c>, <c>FALSE</c>, <c>IntegerValue</c>, <c>FloatValue</c>, <c>NULL</c>, <c>OpenParenthesis</c>, <c>CALL</c>, <c>COUNTOF</c>, <c>ENUMTOINT</c>, <c>INTTOENUM</c>, <c>INTTONATIVE</c>, <c>NATIVETOINT</c>, <c>SIZEOF</c>, <c>HASH</c>, <c>CATCH</c>, <c>TIMESTEP</c>, <c>PoundIf</c>, <c>NOT</c>, <c>PoundEndif</c>, <c>Ampersand</c>, <c>Subtraction</c>, <c>AltNOT</c>.
		/// </remarks>
		protected virtual bool MatchMultiplicativeExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression rightExpr = null;
			if (!this.MatchUnaryExpression(out expr))
				return false;
			while (((this.TokenIs(this.LookAheadToken, SanScriptTokenID.Multiplication)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Division)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Mod)))) {
				string op = null;
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Multiplication)) {
					if (!this.Match(SanScriptTokenID.Multiplication))
						return false;
					else {
						op = "*";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Division)) {
					if (!this.Match(SanScriptTokenID.Division))
						return false;
					else {
						op = "/";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Mod)) {
					if (!this.Match(SanScriptTokenID.Mod))
						return false;
					else {
						op = "%";
					}
				}
				else
					return false;
				if (!this.MatchMultiplicativeExpression(out rightExpr))
					return false;
				expr = new BinaryExpression( expr, op, rightExpr, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>AdditiveExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>AdditiveExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>DEFAULT</c>, <c>OpenVector</c>, <c>OpenString</c>, <c>TRUE</c>, <c>FALSE</c>, <c>IntegerValue</c>, <c>FloatValue</c>, <c>NULL</c>, <c>OpenParenthesis</c>, <c>CALL</c>, <c>COUNTOF</c>, <c>ENUMTOINT</c>, <c>INTTOENUM</c>, <c>INTTONATIVE</c>, <c>NATIVETOINT</c>, <c>SIZEOF</c>, <c>HASH</c>, <c>CATCH</c>, <c>TIMESTEP</c>, <c>PoundIf</c>, <c>NOT</c>, <c>PoundEndif</c>, <c>Ampersand</c>, <c>Subtraction</c>, <c>AltNOT</c>.
		/// </remarks>
		protected virtual bool MatchAdditiveExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression rightExpr = null;
			if (!this.MatchMultiplicativeExpression(out expr))
				return false;
			while (this.IsInMultiMatchSet(5, this.LookAheadToken)) {
				string op = null;
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Addition)) {
					if (!this.Match(SanScriptTokenID.Addition))
						return false;
					else {
						op = "+";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Subtraction)) {
					if (!this.Match(SanScriptTokenID.Subtraction))
						return false;
					else {
						op = "-";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.PlusTimestepMultipliedBy)) {
					if (!this.Match(SanScriptTokenID.PlusTimestepMultipliedBy))
						return false;
					else {
						op = "+@";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.MinusTimestepMultipliedBy)) {
					if (!this.Match(SanScriptTokenID.MinusTimestepMultipliedBy))
						return false;
					else {
						op = "-@";
					}
				}
				else
					return false;
				if (!this.MatchAdditiveExpression(out rightExpr))
					return false;
				expr = new BinaryExpression( expr, op, rightExpr, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>EqualityExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>EqualityExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>DEFAULT</c>, <c>OpenVector</c>, <c>OpenString</c>, <c>TRUE</c>, <c>FALSE</c>, <c>IntegerValue</c>, <c>FloatValue</c>, <c>NULL</c>, <c>OpenParenthesis</c>, <c>CALL</c>, <c>COUNTOF</c>, <c>ENUMTOINT</c>, <c>INTTOENUM</c>, <c>INTTONATIVE</c>, <c>NATIVETOINT</c>, <c>SIZEOF</c>, <c>HASH</c>, <c>CATCH</c>, <c>TIMESTEP</c>, <c>PoundIf</c>, <c>NOT</c>, <c>PoundEndif</c>, <c>Ampersand</c>, <c>Subtraction</c>, <c>AltNOT</c>.
		/// </remarks>
		protected virtual bool MatchEqualityExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression rightExpr = null;
			if (!this.MatchAdditiveExpression(out expr))
				return false;
			while (this.IsInMultiMatchSet(6, this.LookAheadToken)) {
				string op = null;
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.AssignmentOrEquality)) {
					if (!this.Match(SanScriptTokenID.AssignmentOrEquality))
						return false;
					else {
						op = "=";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Inequality)) {
					if (!this.Match(SanScriptTokenID.Inequality))
						return false;
					else {
						op = "<>";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.AltInequality)) {
					if (!this.Match(SanScriptTokenID.AltInequality))
						return false;
					else {
						op = "!=";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.LessThanEqualTo)) {
					if (!this.Match(SanScriptTokenID.LessThanEqualTo))
						return false;
					else {
						op = "<=";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.LessThan)) {
					if (!this.Match(SanScriptTokenID.LessThan))
						return false;
					else {
						op = "<";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.GreaterThanEqualTo)) {
					if (!this.Match(SanScriptTokenID.GreaterThanEqualTo))
						return false;
					else {
						op = ">=";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.GreaterThan)) {
					if (!this.Match(SanScriptTokenID.GreaterThan))
						return false;
					else {
						op = ">";
					}
				}
				else
					return false;
				if (!this.MatchEqualityExpression(out rightExpr))
					return false;
				expr = new BinaryExpression( expr, op, rightExpr, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>BitwiseAndExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>BitwiseAndExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>DEFAULT</c>, <c>OpenVector</c>, <c>OpenString</c>, <c>TRUE</c>, <c>FALSE</c>, <c>IntegerValue</c>, <c>FloatValue</c>, <c>NULL</c>, <c>OpenParenthesis</c>, <c>CALL</c>, <c>COUNTOF</c>, <c>ENUMTOINT</c>, <c>INTTOENUM</c>, <c>INTTONATIVE</c>, <c>NATIVETOINT</c>, <c>SIZEOF</c>, <c>HASH</c>, <c>CATCH</c>, <c>TIMESTEP</c>, <c>PoundIf</c>, <c>NOT</c>, <c>PoundEndif</c>, <c>Ampersand</c>, <c>Subtraction</c>, <c>AltNOT</c>.
		/// </remarks>
		protected virtual bool MatchBitwiseAndExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression rightExpr = null;
			if (!this.MatchEqualityExpression(out expr))
				return false;
			while (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Ampersand)) {
				string op = null;
				if (!this.Match(SanScriptTokenID.Ampersand))
					return false;
				else {
					op = "&";
				}
				if (!this.MatchBitwiseAndExpression(out rightExpr))
					return false;
				expr = new BinaryExpression( expr, op, rightExpr, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>BitwiseXOrExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>BitwiseXOrExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>DEFAULT</c>, <c>OpenVector</c>, <c>OpenString</c>, <c>TRUE</c>, <c>FALSE</c>, <c>IntegerValue</c>, <c>FloatValue</c>, <c>NULL</c>, <c>OpenParenthesis</c>, <c>CALL</c>, <c>COUNTOF</c>, <c>ENUMTOINT</c>, <c>INTTOENUM</c>, <c>INTTONATIVE</c>, <c>NATIVETOINT</c>, <c>SIZEOF</c>, <c>HASH</c>, <c>CATCH</c>, <c>TIMESTEP</c>, <c>PoundIf</c>, <c>NOT</c>, <c>PoundEndif</c>, <c>Ampersand</c>, <c>Subtraction</c>, <c>AltNOT</c>.
		/// </remarks>
		protected virtual bool MatchBitwiseXOrExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression rightExpr = null;
			if (!this.MatchBitwiseAndExpression(out expr))
				return false;
			while (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Caret)) {
				string op = null;
				if (!this.Match(SanScriptTokenID.Caret))
					return false;
				else {
					op = "^";
				}
				if (!this.MatchBitwiseXOrExpression(out rightExpr))
					return false;
				expr = new BinaryExpression( expr, op, rightExpr, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>BitwiseOrExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>BitwiseOrExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>DEFAULT</c>, <c>OpenVector</c>, <c>OpenString</c>, <c>TRUE</c>, <c>FALSE</c>, <c>IntegerValue</c>, <c>FloatValue</c>, <c>NULL</c>, <c>OpenParenthesis</c>, <c>CALL</c>, <c>COUNTOF</c>, <c>ENUMTOINT</c>, <c>INTTOENUM</c>, <c>INTTONATIVE</c>, <c>NATIVETOINT</c>, <c>SIZEOF</c>, <c>HASH</c>, <c>CATCH</c>, <c>TIMESTEP</c>, <c>PoundIf</c>, <c>NOT</c>, <c>PoundEndif</c>, <c>Ampersand</c>, <c>Subtraction</c>, <c>AltNOT</c>.
		/// </remarks>
		protected virtual bool MatchBitwiseOrExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression rightExpr = null;
			if (!this.MatchBitwiseXOrExpression(out expr))
				return false;
			while (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Pipe)) {
				string op = null;
				if (!this.Match(SanScriptTokenID.Pipe))
					return false;
				else {
					op = "|";
				}
				if (!this.MatchBitwiseOrExpression(out rightExpr))
					return false;
				expr = new BinaryExpression( expr, op, rightExpr, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>AndExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>AndExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>DEFAULT</c>, <c>OpenVector</c>, <c>OpenString</c>, <c>TRUE</c>, <c>FALSE</c>, <c>IntegerValue</c>, <c>FloatValue</c>, <c>NULL</c>, <c>OpenParenthesis</c>, <c>CALL</c>, <c>COUNTOF</c>, <c>ENUMTOINT</c>, <c>INTTOENUM</c>, <c>INTTONATIVE</c>, <c>NATIVETOINT</c>, <c>SIZEOF</c>, <c>HASH</c>, <c>CATCH</c>, <c>TIMESTEP</c>, <c>PoundIf</c>, <c>NOT</c>, <c>PoundEndif</c>, <c>Ampersand</c>, <c>Subtraction</c>, <c>AltNOT</c>.
		/// </remarks>
		protected virtual bool MatchAndExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression rightExpr = null;
			if (!this.MatchBitwiseOrExpression(out expr))
				return false;
			while (this.TokenIs(this.LookAheadToken, SanScriptTokenID.AND)) {
				string op = null;
				if (!this.Match(SanScriptTokenID.AND))
					return false;
				else {
					op = "AND";
				}
				if (!this.MatchAndExpression(out rightExpr))
					return false;
				expr = new BinaryExpression( expr, op, rightExpr, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>OrExpression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>OrExpression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>DEFAULT</c>, <c>OpenVector</c>, <c>OpenString</c>, <c>TRUE</c>, <c>FALSE</c>, <c>IntegerValue</c>, <c>FloatValue</c>, <c>NULL</c>, <c>OpenParenthesis</c>, <c>CALL</c>, <c>COUNTOF</c>, <c>ENUMTOINT</c>, <c>INTTOENUM</c>, <c>INTTONATIVE</c>, <c>NATIVETOINT</c>, <c>SIZEOF</c>, <c>HASH</c>, <c>CATCH</c>, <c>TIMESTEP</c>, <c>PoundIf</c>, <c>NOT</c>, <c>PoundEndif</c>, <c>Ampersand</c>, <c>Subtraction</c>, <c>AltNOT</c>.
		/// </remarks>
		protected virtual bool MatchOrExpression(out Expression expr) {
			expr = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression rightExpr = null;
			if (!this.MatchAndExpression(out expr))
				return false;
			while (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OR)) {
				string op = null;
				if (!this.Match(SanScriptTokenID.OR))
					return false;
				else {
					op = "OR";
				}
				if (!this.MatchOrExpression(out rightExpr))
					return false;
				expr = new BinaryExpression( expr, op, rightExpr, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>Expression</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>Expression</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>DEFAULT</c>, <c>OpenVector</c>, <c>OpenString</c>, <c>TRUE</c>, <c>FALSE</c>, <c>IntegerValue</c>, <c>FloatValue</c>, <c>NULL</c>, <c>OpenParenthesis</c>, <c>CALL</c>, <c>COUNTOF</c>, <c>ENUMTOINT</c>, <c>INTTOENUM</c>, <c>INTTONATIVE</c>, <c>NATIVETOINT</c>, <c>SIZEOF</c>, <c>HASH</c>, <c>CATCH</c>, <c>TIMESTEP</c>, <c>PoundIf</c>, <c>NOT</c>, <c>PoundEndif</c>, <c>Ampersand</c>, <c>Subtraction</c>, <c>AltNOT</c>.
		/// </remarks>
		protected virtual bool MatchExpression(out Expression expr) {
			if (!this.MatchOrExpression(out expr))
				return false;
			return true;
		}

		/// <summary>
		/// Matches a <c>ConstDefinitionStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>ConstDefinitionStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>CONSTINT</c>, <c>CONSTFLOAT</c>.
		/// </remarks>
		protected virtual bool MatchConstDefinitionStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			bool isFloat;
			Identifier name;
			Expression value;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.CONSTINT)) {
				if (!this.Match(SanScriptTokenID.CONSTINT))
					return false;
				else {
					isFloat = false;
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.CONSTFLOAT)) {
				if (!this.Match(SanScriptTokenID.CONSTFLOAT))
					return false;
				else {
					isFloat = true;
				}
			}
			else
				return false;
			if (!this.MatchIdentifier(out name))
				return false;
			this.MatchExpression(out value);
			stmt = new ConstDefinitionStatement( name, isFloat, value, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>IdentifierStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>IdentifierStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>CALL</c>.
		/// </remarks>
		protected virtual bool MatchIdentifierStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			string op = null;
			int add = 0;
			bool isLabel = false;
			Expression variableName;
			Expression value = null;
			if (!this.MatchIdentifierExpression(out variableName))
				return false;
			// Subroutine Access, nothing further to check
			if ( variableName.NodeType == SanScriptNodeType.SubroutineAccessExpression )
				{
				SubroutineAccessExpression subroutine = variableName as SubroutineAccessExpression;
				AstNodeList newArgs = new AstNodeList( null );
				
				IAstNodeList args = subroutine.Arguments;
				if ( args != null )
					{
					foreach ( IAstNode arg in args )
						{
						newArgs.Add( arg as AstNode );
					}
				}
				
				stmt = new SubroutineAccessStatement( false, subroutine.IsCall, subroutine.Name, newArgs, new TextRange( startOffset, this.Token.EndOffset ) );
				return true;
			}
			else if ( (variableName.NodeType == SanScriptNodeType.VariableExpression) && (this.IsInMultiMatchSet(2, this.LookAheadToken)) )
				{
				VariableExpression var = variableName as VariableExpression;
				if (!this.MatchVariableDeclarationStatement(var.Name, false, false, true, true, true, false, out stmt))
					return false;
				else {
					stmt.TextRange = new TextRange( startOffset, stmt.TextRange.EndOffset ); return true;
				}
			}
			else if ( variableName.NodeType == SanScriptNodeType.ArrayAccessExpression )
				{
				ArrayAccessExpression access = variableName as ArrayAccessExpression;
				if ( access.IsCall )
					{
					stmt = new CallStatement( variableName, new TextRange( startOffset, this.Token.EndOffset ) );
					return true;
				}
			}
			else if ( variableName.NodeType == SanScriptNodeType.StructMemberAccessExpression )
				{
				StructMemberAccessExpression access = variableName as StructMemberAccessExpression;
				if ( access.IsCall )
					{
					stmt = new CallStatement( variableName, new TextRange( startOffset, this.Token.EndOffset ) );
					return true;
				}
			}
			if (this.IsInMultiMatchSet(7, this.LookAheadToken)) {
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.AssignmentOrEquality)) {
					if (this.Match(SanScriptTokenID.AssignmentOrEquality)) {
						op = "=";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.PlusEquals)) {
					if (this.Match(SanScriptTokenID.PlusEquals)) {
						op = "+=";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.MinusEquals)) {
					if (this.Match(SanScriptTokenID.MinusEquals)) {
						op = "-=";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.MultiplyEquals)) {
					if (this.Match(SanScriptTokenID.MultiplyEquals)) {
						op = "*=";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.DivideEquals)) {
					if (this.Match(SanScriptTokenID.DivideEquals)) {
						op = "/=";
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Increment)) {
					if (this.Match(SanScriptTokenID.Increment)) {
						add = 1;
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Decrement)) {
					if (this.Match(SanScriptTokenID.Decrement)) {
						add = -1;
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Colon)) {
					if (this.Match(SanScriptTokenID.Colon)) {
						isLabel = true;
					}
				}
				else
					return false;
			}
			if ( isLabel )
				{
				IdentifierExpression idExpr = variableName as IdentifierExpression;
				stmt = new LabelDeclarationStatement( idExpr.Name, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			else if ( add != 0 )
				{
				stmt = new AdditiveStatement( (add == 1) ? true : false, variableName, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			else
				{
				if ( op != null )
					{
					if (!this.MatchExpression(out value)) {
						this.ReportSyntaxError( "Empty assignment expression." );
					}
				}
				else
					{
					this.ReportSyntaxError( "Incomplete statement." );
				}
				
				stmt = new AssignmentStatement( variableName, op, value, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>PreAdditiveStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>PreAdditiveStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Increment</c>, <c>Decrement</c>.
		/// </remarks>
		protected virtual bool MatchPreAdditiveStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			int add = 0;
			Expression variableName;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Increment)) {
				if (!this.Match(SanScriptTokenID.Increment))
					return false;
				else {
					add = 1;
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Decrement)) {
				if (!this.Match(SanScriptTokenID.Decrement))
					return false;
				else {
					add = -1;
				}
			}
			else
				return false;
			if (!this.MatchIdentifierExpression(out variableName)) {
				this.ReportSyntaxError( "Identifier expected." );
			}
			stmt = new AdditiveStatement( (add == 1) ? true : false, variableName, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>SuperSubroutineAccessStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>SuperSubroutineAccessStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>HSMSUPER</c>.
		/// </remarks>
		protected virtual bool MatchSuperSubroutineAccessStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier id = null;
			AstNodeList argumentList = new AstNodeList( null );
			if (!this.Match(SanScriptTokenID.HSMSUPER))
				return false;
			if (!this.Match(SanScriptTokenID.Period)) {
				this.ReportSyntaxError( "Period expected." );
			}
			if (!this.MatchIdentifier(out id)) {
				this.ReportSyntaxError( "Subroutine call expected." );
			}
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenParenthesis)) {
				if (!this.MatchSubroutineAccessArgumentList(argumentList)) {
					this.ReportSyntaxError( "Invalid argument list." );
				}
			}
			stmt = new SubroutineAccessStatement( true, false, id, argumentList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>NativeVariableDeclarationStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>NativeVariableDeclarationStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>BOOL</c>, <c>FLOAT</c>, <c>TWEAK_FLOAT</c>, <c>INT</c>, <c>TWEAK_INT</c>, <c>VECTOR</c>, <c>STRING</c>, <c>VARARGS</c>, <c>TEXTLABEL</c>, <c>TEXTLABELTHREE</c>, <c>TEXTLABELSEVEN</c>, <c>TEXTLABELFIFTEEN</c>, <c>TEXTLABELTWENTYTHREE</c>, <c>TEXTLABELTHIRTYONE</c>, <c>TEXTLABELSIXTYTHREE</c>, <c>ENUMTOINTdatatype</c>, <c>StructArgument</c>.
		/// </remarks>
		protected virtual bool MatchNativeVariableDeclarationStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier dataType;
			if (!this.MatchNativeDataType(out dataType, false, false))
				return false;
			if (!this.MatchVariableDeclarationStatement(dataType, false, false, true, true, true, false, out stmt))
				return false;
			// adjust the start offset
			stmt.StartOffset = startOffset;
			return true;
		}

		/// <summary>
		/// Matches a <c>VariableDeclarationStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>VariableDeclarationStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>BOOL</c>, <c>FLOAT</c>, <c>TWEAK_FLOAT</c>, <c>INT</c>, <c>TWEAK_INT</c>, <c>VECTOR</c>, <c>STRING</c>, <c>VARARGS</c>, <c>TEXTLABEL</c>, <c>TEXTLABELTHREE</c>, <c>TEXTLABELSEVEN</c>, <c>TEXTLABELFIFTEEN</c>, <c>TEXTLABELTWENTYTHREE</c>, <c>TEXTLABELTHIRTYONE</c>, <c>TEXTLABELSIXTYTHREE</c>, <c>ENUMTOINTdatatype</c>, <c>StructArgument</c>.
		/// </remarks>
		protected virtual bool MatchVariableDeclarationStatement(Identifier dataType, bool allowEnumToIntDatatype, bool allowReference, bool allowArrayAccess, bool allowMultiDecl, bool useDataTypeForMultiDecl, bool isSubDeclaration, out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier variableName;
			Expression expr = null;
			Expression subExpr = null;
			Statement subDecl = null;
			bool isStructArg = false;
			bool isEnumToIntDatatype = false;
			bool isRef = false;
			bool isArray = false;
			bool isVarArgs = false;
			bool isUnused = false;
			
			if ( dataType == null )
				{
				if (this.IsInMultiMatchSet(8, this.LookAheadToken)) {
					if (!this.MatchNativeDataType(out dataType, allowEnumToIntDatatype, allowReference))
						return false;
				}
				else if (this.IsInMultiMatchSet(2, this.LookAheadToken)) {
					if (!this.MatchIdentifier(out dataType))
						return false;
				}
				else
					return false;
				if ( dataType.Text.ToUpper() == "STRUCT" )
					{
					isStructArg = true;
				}
				else if ( dataType.Text.ToUpper() == "ENUM_TO_INT" )
					{
					isEnumToIntDatatype = true;
				}
				else if ( dataType.Text.ToUpper() == "VARARGS" )
					{
					isVarArgs = true;
					System.Int32 endTokenOffset = this.Token.EndOffset;
					stmt = new VariableNonArrayDeclarationStatement( expr, new Identifier(null, Identifier.DeclarationType.Native, new TextRange(0, 0)), dataType, isRef, isSubDeclaration, subDecl, new TextRange( startOffset, endTokenOffset  ) );
					return true; //?
				}
			}
			if (this.IsInMultiMatchSet(2, this.LookAheadToken)) {
				if (!this.MatchIdentifier(out variableName))
					return false;
			}
			else if (allowReference) {
				if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.Ampersand ) )
					{
					if (!this.Match(SanScriptTokenID.Ampersand)) {
						if ( isStructArg ) this.ReportSyntaxError( "Ampersand expected." );
					}
					else {
						isRef = true;
					}
				}
				if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.UNUSED ) )
					{
					if (!this.Match(SanScriptTokenID.UNUSED)) {
						isUnused = false;
					}
					else {
						isUnused = true;
					}
				}
				
				if ( isUnused )
					{
					this.MatchIdentifier(out variableName);
				}
				else
					{
					if (!this.MatchIdentifier(out variableName)) {
						this.ReportSyntaxError( "Missing identifier." );
					}
				}
			}
			else
				return false;
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.AssignmentOrEquality ) )
				{
				if (!this.Match(SanScriptTokenID.AssignmentOrEquality))
					return false;
				if (!this.MatchExpression(out expr)) {
					this.ReportSyntaxError( "Empty assignment statement." );
				}
			}
			else if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.OpenBracket ) )
				{
				if ( allowArrayAccess && !isStructArg && !isEnumToIntDatatype )
					{
					if (!this.MatchArrayAccessExpression(out expr, out subExpr)) {
						this.ReportSyntaxError( "Array length expected." );
					}
					else {
						isArray = true;
					}
				}
				else
					{
					this.ReportSyntaxError( "Array declaration not permitted." );
				}
			}
			
			System.Int32 endOffset = this.Token.EndOffset;
			
			if ( allowMultiDecl && !isVarArgs )
				{
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Comma)) {
					if (!this.Match(SanScriptTokenID.Comma))
						return false;
					if (!this.MatchVariableDeclarationStatement(useDataTypeForMultiDecl ? dataType : null, allowEnumToIntDatatype, allowReference, allowArrayAccess, allowMultiDecl, useDataTypeForMultiDecl, true, out subDecl)) {
						this.ReportSyntaxError( "Additional declarations expected." );
					}
				}
			}
			
			if ( isArray )
				{
				// check if ArrayAccessExpression, don't allow StructMemberAccessExpression
				Expression subExprTest = subExpr;
				while ( subExprTest != null )
					{
					if ( subExprTest.NodeType == SanScriptNodeType.StructMemberAccessExpression )
						{
						this.ReportSyntaxError( subExprTest.TextRange, "Invalid expression." );
						break;
					}
					else if ( subExprTest.NodeType == SanScriptNodeType.ArrayAccessExpression )
						{
						subExprTest = ((ArrayAccessExpression)subExprTest).AccessSubExpression;
					}
					else
						{
						break;
					}
				}
				
				stmt = new VariableArrayDeclarationStatement( expr, subExpr, variableName, dataType, isRef, isSubDeclaration, subDecl, new TextRange( startOffset, endOffset ) );
			}
			else
				{
				stmt = new VariableNonArrayDeclarationStatement( expr, variableName, dataType, isRef, isSubDeclaration, subDecl, new TextRange( startOffset, endOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>ForwardDeclarationStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>ForwardDeclarationStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>FORWARD</c>.
		/// </remarks>
		protected virtual bool MatchForwardDeclarationStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier name;
			bool isEnum = false;
			if (!this.Match(SanScriptTokenID.FORWARD))
				return false;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.ENUM)) {
				if (!this.Match(SanScriptTokenID.ENUM)) {
					this.ReportSyntaxError( "ENUM expected." ); isEnum = true;
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.STRUCT)) {
				if (!this.Match(SanScriptTokenID.STRUCT)) {
					this.ReportSyntaxError( "STRUCT expected." );
				}
			}
			else
				return false;
			if (!this.MatchIdentifier(out name)) {
				this.ReportSyntaxError( "Identifier expected." );
			}
			--m_scopeLevel;	// fixup scope level
			if (isEnum)
				stmt = new ForwardEnumDeclarationStatement( name, new TextRange( startOffset, this.Token.EndOffset ) );
			else
				stmt = new ForwardStructDeclarationStatement( name, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>UsingStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>UsingStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>USING</c>.
		/// </remarks>
		protected virtual bool MatchUsingStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression filename;
			if (!this.Match(SanScriptTokenID.USING))
				return false;
			if (!this.MatchStringExpression(out filename)) {
				this.ReportSyntaxError( "File name string expected." );
			}
			stmt = new UsingStatement( filename, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>ThrowStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>ThrowStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>THROW</c>.
		/// </remarks>
		protected virtual bool MatchThrowStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression expr = null;
			if (!this.Match(SanScriptTokenID.THROW))
				return false;
			this.MatchExpression(out expr);
			stmt = new ThrowStatement( expr, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>GotoStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>GotoStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>GOTO</c>.
		/// </remarks>
		protected virtual bool MatchGotoStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier name = null;
			if (!this.Match(SanScriptTokenID.GOTO))
				return false;
			if (this.IsInMultiMatchSet(2, this.LookAheadToken)) {
				if (!this.MatchIdentifier(out name)) {
					this.ReportSyntaxError( "Missing identifier." );
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.FALSE)) {
				this.Match(SanScriptTokenID.FALSE);
			}
			else
				return false;
			stmt = new GotoStatement( name, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>GotoStateStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>GotoStateStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>GOTOHSMSTATE</c>.
		/// </remarks>
		protected virtual bool MatchGotoStateStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier name;
			if (!this.Match(SanScriptTokenID.GOTOHSMSTATE))
				return false;
			if (!this.MatchIdentifier(out name)) {
				this.ReportSyntaxError( "Missing identifier." );
			}
			stmt = new GotoStateStatement( name, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>DefaultStateStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>DefaultStateStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>DEFAULTHSMSTATE</c>.
		/// </remarks>
		protected virtual bool MatchDefaultStateStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier name;
			if (!this.Match(SanScriptTokenID.DEFAULTHSMSTATE))
				return false;
			if (!this.MatchIdentifier(out name)) {
				this.ReportSyntaxError( "Missing identifier." );
			}
			stmt = new DefaultStateStatement( name, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>HistoryStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>HistoryStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>HSMHISTORY</c>.
		/// </remarks>
		protected virtual bool MatchHistoryStatement(out Statement stmt) {
			stmt = null;
			if (!this.Match(SanScriptTokenID.HSMHISTORY))
				return false;
			stmt = new HistoryStatement( this.Token.TextRange );
			return true;
		}

		/// <summary>
		/// Matches a <c>BreakStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>BreakStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>BREAK</c>.
		/// </remarks>
		protected virtual bool MatchBreakStatement(out Statement stmt) {
			stmt = null;
			if (!this.Match(SanScriptTokenID.BREAK))
				return false;
			stmt = new BreakStatement( this.Token.TextRange );
			return true;
		}

		/// <summary>
		/// Matches a <c>FallthruStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>FallthruStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>FALLTHRU</c>.
		/// </remarks>
		protected virtual bool MatchFallthruStatement(out Statement stmt) {
			stmt = null;
			if (!this.Match(SanScriptTokenID.FALLTHRU))
				return false;
			stmt = new FallthruStatement( this.Token.TextRange );
			return true;
		}

		/// <summary>
		/// Matches a <c>ReturnStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>ReturnStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>RETURN</c>.
		/// </remarks>
		protected virtual bool MatchReturnStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression rtn = null;
			if (!this.Match(SanScriptTokenID.RETURN))
				return false;
			if (this.IsInMultiMatchSet(1, this.LookAheadToken)) {
				if (!this.MatchExpression(out rtn))
					return false;
			}
			stmt = new ReturnStatement( rtn, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>ExitStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>ExitStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>EXIT</c>.
		/// </remarks>
		protected virtual bool MatchExitStatement(out Statement stmt) {
			stmt = null;
			if (!this.Match(SanScriptTokenID.EXIT))
				return false;
			stmt = new ExitStatement( this.Token.TextRange );
			return true;
		}

		/// <summary>
		/// Matches a <c>BreakLoopStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>BreakLoopStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>BREAKLOOP</c>.
		/// </remarks>
		protected virtual bool MatchBreakLoopStatement(out Statement stmt) {
			stmt = null;
			if (!this.Match(SanScriptTokenID.BREAKLOOP))
				return false;
			stmt = new BreakLoopStatement( this.Token.TextRange );
			return true;
		}

		/// <summary>
		/// Matches a <c>ReLoopStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>ReLoopStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>RELOOP</c>.
		/// </remarks>
		protected virtual bool MatchReLoopStatement(out Statement stmt) {
			stmt = null;
			if (!this.Match(SanScriptTokenID.RELOOP))
				return false;
			stmt = new ReLoopStatement( this.Token.TextRange );
			return true;
		}

		/// <summary>
		/// Matches a <c>BlockStatementList</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>BlockStatementList</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>BOOL</c>, <c>FLOAT</c>, <c>TWEAK_FLOAT</c>, <c>INT</c>, <c>TWEAK_INT</c>, <c>VECTOR</c>, <c>STRING</c>, <c>VARARGS</c>, <c>TEXTLABEL</c>, <c>TEXTLABELTHREE</c>, <c>TEXTLABELSEVEN</c>, <c>TEXTLABELFIFTEEN</c>, <c>TEXTLABELTWENTYTHREE</c>, <c>TEXTLABELTHIRTYONE</c>, <c>TEXTLABELSIXTYTHREE</c>, <c>ENUMTOINTdatatype</c>, <c>StructArgument</c>, <c>DEFAULT</c>, <c>CALL</c>, <c>PoundIf</c>, <c>PoundEndif</c>, <c>CONSTINT</c>, <c>CONSTFLOAT</c>, <c>Increment</c>, <c>Decrement</c>, <c>HSMSUPER</c>, <c>FORWARD</c>, <c>ENUM</c>, <c>STRUCT</c>, <c>USING</c>, <c>THROW</c>, <c>GOTO</c>, <c>GOTOHSMSTATE</c>, <c>DEFAULTHSMSTATE</c>, <c>HSMHISTORY</c>, <c>BREAK</c>, <c>FALLTHRU</c>, <c>RETURN</c>, <c>EXIT</c>, <c>BREAKLOOP</c>, <c>RELOOP</c>, <c>IF</c>, <c>ELIF</c>, <c>ELSE</c>, <c>WHILE</c>, <c>REPEAT</c>, <c>FOR</c>, <c>CASE</c>, <c>SWITCH</c>, <c>HSMSTATE</c>, <c>HSMACTIVATE</c>, <c>HSMDEACTIVATE</c>, <c>HASH_ENUM</c>, <c>STRICT_ENUM</c>, <c>GLOBALS</c>, <c>SCRIPT</c>, <c>PROC</c>, <c>HSMEVENT</c>, <c>FUNC</c>, <c>NATIVE</c>, <c>TYPEDEF</c>, <c>MultiLineCommentStart</c>, <c>SingleLineCommentStart</c>.
		/// </remarks>
		protected virtual bool MatchBlockStatementList(System.Int32[] endTokenIds, AstNodeList statementList) {
			System.Int32 statementScopeBraceLevel = m_scopeLevel;
			
			while ( !this.IsAtEnd )
				{
				foreach ( System.Int32 endTokenId in endTokenIds )
					{
					if ( this.TokenIs( this.LookAheadToken, endTokenId ) && (m_scopeLevel == statementScopeBraceLevel) )
						{
						return true;
					}
				}
				
				if ( this.IsInMultiMatchSet(0, this.LookAheadToken) )
					{
					Statement statement = null;
					if (this.MatchStatement(out statement)) {
						if ( statement != null ) statementList.Add( statement );
					}
				}
				else
					{
					this.AdvanceToNext();
				}
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>IfBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>IfBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>IF</c>, <c>ELIF</c>, <c>ELSE</c>.
		/// </remarks>
		protected virtual bool MatchIfBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			int startsWith = SanScriptTokenID.IF;
			Expression condition = null;
			AstNodeList statementList = new AstNodeList( null );
			
			System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.ELIF, SanScriptTokenID.ELSE, SanScriptTokenID.ENDIF };
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.IF)) {
				if (!this.Match(SanScriptTokenID.IF))
					return false;
				if (!this.MatchExpression(out condition)) {
					this.ReportSyntaxError( "Condition expression expected." );
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.ELIF)) {
				if (!this.Match(SanScriptTokenID.ELIF))
					return false;
				else {
					startsWith = SanScriptTokenID.ELIF;
				}
				if (!this.MatchExpression(out condition)) {
					this.ReportSyntaxError( "Condition expression expected." );
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.ELSE)) {
				if (!this.Match(SanScriptTokenID.ELSE))
					return false;
				else {
					startsWith = SanScriptTokenID.ELSE;
				}
			}
			else
				return false;
			this.MatchBlockStatementList(endTokenIds, statementList);
			int endsWith = SanScriptTokenID.Invalid;
			
			int endOffset = this.Token.EndOffset;
			
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ELIF ) )
				{
				endsWith = SanScriptTokenID.ELIF;
				endOffset = this.LookAheadToken.StartOffset - 1;
			}
			else if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ELSE ) )
				{
				endsWith = SanScriptTokenID.ELSE;
				endOffset = this.LookAheadToken.StartOffset - 1;
			}
			else if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDIF ) )
				{
				endsWith = SanScriptTokenID.ENDIF;
				this.AdvanceToNext();
				endOffset = this.Token.EndOffset;
			}
			
			stmt = new IfBlockStatement( startsWith, endsWith, condition, statementList, new TextRange( startOffset, endOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>WhileBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>WhileBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>WHILE</c>.
		/// </remarks>
		protected virtual bool MatchWhileBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression condition = null;
			AstNodeList statementList = new AstNodeList( null );
			System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.ENDWHILE };
			if (!this.Match(SanScriptTokenID.WHILE))
				return false;
			if (!this.MatchExpression(out condition)) {
				this.ReportSyntaxError( "Condition expression expected." );
			}
			this.MatchBlockStatementList(endTokenIds, statementList);
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDWHILE ) )
				{
				this.AdvanceToNext();
			}
			
			stmt = new WhileBlockStatement( condition, statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>RepeatBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>RepeatBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>REPEAT</c>.
		/// </remarks>
		protected virtual bool MatchRepeatBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression count = null;
			Expression variable = null;
			AstNodeList statementList = new AstNodeList( null );
			System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.ENDREPEAT };
			if (!this.Match(SanScriptTokenID.REPEAT))
				return false;
			if (!this.MatchExpression(out count)) {
				this.ReportSyntaxError( "Missing iteration count." );
			}
			if (!this.MatchIdentifierExpression(out variable)) {
				this.ReportSyntaxError( "Missing iterator." );
			}
			this.MatchBlockStatementList(endTokenIds, statementList);
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDREPEAT ) )
				{
				this.AdvanceToNext();
			}
			
			stmt = new RepeatBlockStatement( count, variable, statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>ForBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>ForBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>FOR</c>.
		/// </remarks>
		protected virtual bool MatchForBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression variable = null;
			Expression start = null;
			Expression end = null;
			Expression step = null;
			AstNodeList statementList = new AstNodeList( null );
			System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.ENDFOR };
			if (!this.Match(SanScriptTokenID.FOR))
				return false;
			if (!this.MatchIdentifierExpression(out variable)) {
				this.ReportSyntaxError( "Missing iterator." );
			}
			if (!this.Match(SanScriptTokenID.AssignmentOrEquality)) {
				this.ReportSyntaxError( "Assignment operator expected." );
			}
			if (!this.MatchExpression(out start)) {
				this.ReportSyntaxError( "Missing initialization expression." );
			}
			if (!this.Match(SanScriptTokenID.TO)) {
				this.ReportSyntaxError( "TO keyword expected." );
			}
			if (!this.MatchExpression(out end)) {
				this.ReportSyntaxError( "Terminating condition expected." );
			}
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.STEP)) {
				if (!this.Match(SanScriptTokenID.STEP))
					return false;
				if (!this.MatchUnaryExpression(out step)) {
					this.ReportSyntaxError( "STEP expression expected." );
				}
			}
			this.MatchBlockStatementList(endTokenIds, statementList);
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDFOR ) )
				{
				this.AdvanceToNext();
			}
			
			stmt = new ForBlockStatement( variable, start, end, step, statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>CaseBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>CaseBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>DEFAULT</c>, <c>CASE</c>.
		/// </remarks>
		protected virtual bool MatchCaseBlockStatement(bool embedded, out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			int startsWith = SanScriptTokenID.CASE;
			Expression condition = null;
			AstNodeList statementList = new AstNodeList( null );
			System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.BREAK, SanScriptTokenID.CASE, SanScriptTokenID.DEFAULT, SanScriptTokenID.FALLTHRU, SanScriptTokenID.RETURN, SanScriptTokenID.EXIT };
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.CASE)) {
				if (!this.Match(SanScriptTokenID.CASE))
					return false;
				if (!this.MatchExpression(out condition)) {
					this.ReportSyntaxError( "CASE condition expected." );
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.DEFAULT)) {
				if (!this.Match(SanScriptTokenID.DEFAULT))
					return false;
				else {
					startsWith = SanScriptTokenID.DEFAULT;
				}
			}
			else
				return false;
			this.MatchBlockStatementList(endTokenIds, statementList);
			int endsWith = SanScriptTokenID.Invalid;
			
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.FALLTHRU ) || this.TokenIs( this.LookAheadToken, SanScriptTokenID.BREAK ) )
				{
				if ( !embedded )
					{
					endsWith = this.LookAheadToken.ID;
					
					Statement endStmt = null;
					
					if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.FALLTHRU ) )
						{
						if (!this.MatchFallthruStatement(out endStmt))
							return false;
					}
					else
						{
						if (!this.MatchBreakStatement(out endStmt))
							return false;
					}
					
					if ( endStmt != null )
						{
						statementList.Add( endStmt );
					}
				}
			}
			else if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.EXIT ) || this.TokenIs( this.LookAheadToken, SanScriptTokenID.RETURN ) || this.TokenIs( this.LookAheadToken, SanScriptTokenID.BREAKLOOP) || this.TokenIs( this.LookAheadToken, SanScriptTokenID.RELOOP ))
				{
				if ( !embedded )
					{
					endsWith = this.LookAheadToken.ID;
					
					Statement endStmt = null;
					Statement extraBreakStmt = null;
					
					if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.RETURN ) )
						{
						if (!this.MatchReturnStatement(out endStmt))
							return false;
						if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.BREAK)) {
							if (!this.MatchBreakStatement(out extraBreakStmt))
								return false;
						}
					}
					else if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.BREAKLOOP) )
						{
						if (!this.MatchBreakLoopStatement(out endStmt))
							return false;
					}
					else if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.RELOOP) )
						{
						if (!this.MatchReLoopStatement(out endStmt))
							return false;
					}
					else
						{
						if (!this.MatchExitStatement(out endStmt))
							return false;
						if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.BREAK)) {
							if (!this.MatchBreakStatement(out extraBreakStmt))
								return false;
						}
					}
					
					if ( endStmt != null )
						{
						statementList.Add( endStmt );
					}
					
					if ( extraBreakStmt == null )
						{
						// fix the scope level if no extra break, since this doesn't normally trigger a change
						--m_scopeLevel;
					}
				}
			}
			else if ( ((this.TokenIs(this.LookAheadToken, SanScriptTokenID.DEFAULT)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.CASE))) )
				{
				Statement subStmt = null;
				this.MatchCaseBlockStatement(true, out subStmt);
				if ( subStmt != null )
					{
					statementList.Add( subStmt );
					
					if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.FALLTHRU ) || this.TokenIs( this.LookAheadToken, SanScriptTokenID.BREAK ) )
						{
						if ( !embedded )
							{
							endsWith = this.LookAheadToken.ID;
							
							Statement endStmt = null;
							
							if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.FALLTHRU ) )
								{
								this.MatchFallthruStatement(out endStmt);
							}
							else
								{
								this.MatchBreakStatement(out endStmt);
							}
							
							if ( endStmt != null )
								{
								statementList.Add( endStmt );
							}
						}
					}
					else if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.EXIT ) || this.TokenIs( this.LookAheadToken, SanScriptTokenID.RETURN ) || this.TokenIs( this.LookAheadToken, SanScriptTokenID.BREAKLOOP ) || this.TokenIs( this.LookAheadToken, SanScriptTokenID.RELOOP ))
						{
						if ( !embedded )
							{
							endsWith = this.LookAheadToken.ID;
							
							Statement endStmt = null;
							Statement extraBreakStmt = null;
							
							if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.RETURN ) )
								{
								this.MatchReturnStatement(out endStmt);
								if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.BREAK)) {
									this.MatchBreakStatement(out extraBreakStmt);
								}
							}
							else if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.BREAKLOOP ) )
								{
								this.MatchBreakLoopStatement(out endStmt);
							}
							else if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.RELOOP ) )
								{
								this.MatchReLoopStatement(out endStmt);
							}
							else
								{
								this.MatchExitStatement(out endStmt);
								if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.BREAK)) {
									if (!this.MatchBreakStatement(out extraBreakStmt))
										return false;
								}
							}
							
							if ( endStmt != null )
								{
								statementList.Add( endStmt );
							}
							
							if ( extraBreakStmt == null )
								{
								// fix the scope level if no extra break, since this doesn't normally trigger a change
								--m_scopeLevel;
							}
						}
					}
					
					// fix the scope level since we're embedding Case/Default statements that fallthru
					--m_scopeLevel;
				}
			}
			
			stmt = new CaseBlockStatement( startsWith, endsWith, condition, statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>SwitchBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>SwitchBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>SWITCH</c>.
		/// </remarks>
		protected virtual bool MatchSwitchBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Expression condition = null;
			AstNodeList statementList = new AstNodeList( null );
			System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.ENDSWITCH };
			if (!this.Match(SanScriptTokenID.SWITCH))
				return false;
			if (!this.MatchExpression(out condition)) {
				this.ReportSyntaxError( "Condition expression expected." );
			}
			this.MatchBlockStatementList(endTokenIds, statementList);
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDSWITCH ) )
				{
				this.AdvanceToNext();
			}
			
			int defaultCaseCount = 0;
			foreach ( AstNode node in statementList )
				{
				switch ( node.NodeType )
					{
					case SanScriptNodeType.CaseBlockStatement:
					{
						CaseBlockStatement caseStmt = node as CaseBlockStatement;
						if ( caseStmt.StartTokenID == SanScriptTokenID.DEFAULT )
							{
							++defaultCaseCount;
							if ( defaultCaseCount > 1 )
								{
								this.ReportSyntaxError( node.TextRange, "Only one DEFAULT case statement is permitted within a SWITCH statement block." );
							}
						}
					}
					break;
					case SanScriptNodeType.MultiLineCommentBlock:
					// this is ok
					break;
					case SanScriptNodeType.BreakStatement:
					case SanScriptNodeType.FallthruStatement:
					// This Syntax Error is reported elsewhere.
					break;
					case SanScriptNodeType.ReturnStatement:
					this.ReportSyntaxError( node.TextRange, "Extra or misplaced RETURN statement." );
					break;
					case SanScriptNodeType.ExitStatement:
					this.ReportSyntaxError( node.TextRange, "Extra or misplaced EXIT statement." );
					break;
					default:
					this.ReportSyntaxError( node.TextRange, "Only CASE and DEFAULT statement blocks are permitted wtihin a SWITCH statement block." );
					break;
				}
			}
			
			stmt = new SwitchBlockStatement( condition, statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>StructDefinitionBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>StructDefinitionBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>STRUCT</c>.
		/// </remarks>
		protected virtual bool MatchStructDefinitionBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier id = null;
			AstNodeList statementList = new AstNodeList( null );
			System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.ENDSTRUCT };
			if (!this.Match(SanScriptTokenID.STRUCT))
				return false;
			if (!this.MatchIdentifier(out id)) {
				this.ReportSyntaxError( "Identifier expected." );
			}
			this.MatchBlockStatementList(endTokenIds, statementList);
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDSTRUCT ) )
				{
				this.AdvanceToNext();
			}
			
			foreach ( AstNode node in statementList )
				{
				if ( !(node is VariableDeclarationStatement) && (node.NodeType != SanScriptNodeType.PreprocessorMacroStatement) )
					{
					this.ReportSyntaxError( node.TextRange, "Only varable declaration statements and preprocessor directives are permitted within a STRUCT statement block." );
				}
				else
					{
					VariableDeclarationStatement var = node as VariableDeclarationStatement;
					while ( var != null )
						{
						var.ImageIndexOverride = (System.Int32)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicField;
						var = var.SubDeclaration as VariableDeclarationStatement;
					}
				}
			}
			
			stmt = new StructDefinitionBlockStatement( id, statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>StateDefinitionBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>StateDefinitionBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>HSMSTATE</c>.
		/// </remarks>
		protected virtual bool MatchStateDefinitionBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier id = null;
			AstNodeList statementList = new AstNodeList( null );
			System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.ENDHSMSTATE };
			if (!this.Match(SanScriptTokenID.HSMSTATE))
				return false;
			if (!this.MatchIdentifier(out id)) {
				this.ReportSyntaxError( "Identifier expected." );
			}
			this.MatchBlockStatementList(endTokenIds, statementList);
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDHSMSTATE ) )
				{
				this.AdvanceToNext();
			}
			
			stmt = new StateDefinitionBlockStatement( id, statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>ActivateBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>ActivateBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>HSMACTIVATE</c>.
		/// </remarks>
		protected virtual bool MatchActivateBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			AstNodeList statementList = new AstNodeList( null );
			System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.ENDHSMACTIVATE };
			if (!this.Match(SanScriptTokenID.HSMACTIVATE))
				return false;
			this.MatchBlockStatementList(endTokenIds, statementList);
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDHSMACTIVATE ) )
				{
				this.AdvanceToNext();
			}
			
			stmt = new ActivateBlockStatement( statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>DeactivateBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>DeactivateBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>HSMDEACTIVATE</c>.
		/// </remarks>
		protected virtual bool MatchDeactivateBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			AstNodeList statementList = new AstNodeList( null );
			System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.ENDHSMDEACTIVATE };
			if (!this.Match(SanScriptTokenID.HSMDEACTIVATE))
				return false;
			this.MatchBlockStatementList(endTokenIds, statementList);
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDHSMDEACTIVATE ) )
				{
				this.AdvanceToNext();
			}
			
			stmt = new DeactivateBlockStatement( statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>EnumDefinitionBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>EnumDefinitionBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>ENUM</c>.
		/// </remarks>
		protected virtual bool MatchEnumDefinitionBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier id = null;
			
			AstNodeList statementList = new AstNodeList( null );
			if (!this.Match(SanScriptTokenID.ENUM))
				return false;
			if (!this.MatchIdentifier(out id)) {
				this.ReportSyntaxError( "Identifier expected." );
			}
			while ( !this.IsAtEnd )
				{
				if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDENUM ) )
					{
					break;
				}
				
				// This includes too many things that aren't valid for an enum block:  if ( this.IsInMultiMatchSet(8, this.LookAheadToken) )
					if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.Identifier ) || this.TokenIs( this.LookAheadToken, SanScriptTokenID.UserIdentifier ) )
					{
					Statement enumeration = null;
					if (this.MatchVariableDeclarationStatement(id, false, false, false, false, true, false, out enumeration)) {
						if ( enumeration != null ) statementList.Add( enumeration );
					}
				}
				else
					{
					this.AdvanceToNext();
				}
			}
			if (!this.Match(SanScriptTokenID.ENDENUM)) {
				this.ReportSyntaxError( "ENDENUM expected" );
			}
			stmt = new EnumDefinitionBlockStatement( id, statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>HashEnumDefinitionBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>HashEnumDefinitionBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>HASH_ENUM</c>.
		/// </remarks>
		protected virtual bool MatchHashEnumDefinitionBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier id = null;
			
			AstNodeList statementList = new AstNodeList( null );
			if (!this.Match(SanScriptTokenID.HASH_ENUM))
				return false;
			if (!this.MatchIdentifier(out id)) {
				this.ReportSyntaxError( "Identifier expected." );
			}
			while ( !this.IsAtEnd )
				{
				if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDENUM ) )
					{
					break;
				}
				
				// This includes too many things that aren't valid for an enum block:  if ( this.IsInMultiMatchSet(8, this.LookAheadToken) )
					if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.Identifier ) || this.TokenIs( this.LookAheadToken, SanScriptTokenID.UserIdentifier ) )
					{
					Statement enumeration = null;
					if (this.MatchVariableDeclarationStatement(id, false, false, false, false, true, false, out enumeration)) {
						if ( enumeration != null ) statementList.Add( enumeration );
					}
				}
				else
					{
					this.AdvanceToNext();
				}
			}
			if (!this.Match(SanScriptTokenID.ENDENUM)) {
				this.ReportSyntaxError( "ENDENUM expected" );
			}
			stmt = new HashEnumDefinitionBlockStatement( id, statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>StrictEnumDefinitionBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>StrictEnumDefinitionBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>STRICT_ENUM</c>.
		/// </remarks>
		protected virtual bool MatchStrictEnumDefinitionBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier id = null;
			
			AstNodeList statementList = new AstNodeList( null );
			if (!this.Match(SanScriptTokenID.STRICT_ENUM))
				return false;
			if (!this.MatchIdentifier(out id)) {
				this.ReportSyntaxError( "Identifier expected." );
			}
			while ( !this.IsAtEnd )
				{
				if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDENUM ) )
					{
					break;
				}
				
				// This includes too many things that aren't valid for an enum block:  if ( this.IsInMultiMatchSet(8, this.LookAheadToken) )
					if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.Identifier ) || this.TokenIs( this.LookAheadToken, SanScriptTokenID.UserIdentifier ) )
					{
					Statement enumeration = null;
					if (this.MatchVariableDeclarationStatement(id, false, false, false, false, true, false, out enumeration)) {
						if ( enumeration != null ) statementList.Add( enumeration );
					}
				}
				else
					{
					this.AdvanceToNext();
				}
			}
			if (!this.Match(SanScriptTokenID.ENDENUM)) {
				this.ReportSyntaxError( "ENDENUM expected" );
			}
			stmt = new StrictEnumDefinitionBlockStatement( id, statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>GlobalsDefinitionBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>GlobalsDefinitionBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>GLOBALS</c>.
		/// </remarks>
		protected virtual bool MatchGlobalsDefinitionBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier id = null;
			if (!this.Match(SanScriptTokenID.GLOBALS))
				return false;
			if (this.IsInMultiMatchSet(9, this.LookAheadToken)) {
				if (!this.MatchIdentifier(out id))
					return false;
			}
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.TRUE ) )
				{
				this.Match(SanScriptTokenID.TRUE);
				stmt = new GlobalsStatement(id, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			else
				{
				AstNodeList statementList = new AstNodeList( null );
				System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.ENDGLOBALS };
				this.MatchBlockStatementList(endTokenIds, statementList);
				if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDGLOBALS ) )
					{
					this.AdvanceToNext();
				}
				
				foreach ( AstNode node in statementList )
					{
					if ( !CompilationUnit.AllowedInGlobalsSection( node ) )
						{
						this.ReportSyntaxError( node.TextRange, "Statement not allowed in a GLOBALS statement block." );
					}
				}
				
				stmt = new GlobalsDefinitionBlockStatement(id, statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>ScriptBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>ScriptBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>SCRIPT</c>.
		/// </remarks>
		protected virtual bool MatchScriptBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			AstNodeList statementList = new AstNodeList( null );
			Statement arg = null;
			System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.ENDSCRIPT };
			if (!this.Match(SanScriptTokenID.SCRIPT))
				return false;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenParenthesis)) {
				if (!this.Match(SanScriptTokenID.OpenParenthesis))
					return false;
				if (!this.MatchVariableDeclarationStatement(null, false, true, false, false, false, false, out arg)) {
					this.ReportSyntaxError( "Argument expected." );
				}
				if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
					this.ReportSyntaxError( "Closing parenthesis expected." );
				}
			}
			this.MatchBlockStatementList(endTokenIds, statementList);
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDSCRIPT ) )
				{
				this.AdvanceToNext();
			}
			
			stmt = new ScriptBlockStatement( arg, statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>ProcDefinitionBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>ProcDefinitionBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>PROC</c>.
		/// </remarks>
		protected virtual bool MatchProcDefinitionBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier id = null;
			Statement arg = null;
			AstNodeList statementList = new AstNodeList( null );
			System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.ENDPROC };
			bool isDebugOnly = false;
			
			if ( this.Token != null && this.TokenIs(this.Token, SanScriptTokenID.DEBUGONLY))
				{
				isDebugOnly = true;
			}
			if (!this.Match(SanScriptTokenID.PROC))
				return false;
			if (!this.MatchIdentifier(out id)) {
				this.ReportSyntaxError( "Identifier expected." );
			}
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenParenthesis)) {
				if (!this.Match(SanScriptTokenID.OpenParenthesis))
					return false;
				if (this.IsInMultiMatchSet(10, this.LookAheadToken)) {
					if (!this.MatchVariableDeclarationStatement(null, true, true, true, true, false, false, out arg)) {
						this.ReportSyntaxError( "Invalid argument list." );
					}
				}
				if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
					this.ReportSyntaxError( "Closing parenthesis expected." );
				}
			}
			this.MatchBlockStatementList(endTokenIds, statementList);
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDPROC ) )
				{
				this.AdvanceToNext();
			}
			
			AstNodeList argumentList = new AstNodeList( null );
			while ( arg != null )
				{
				argumentList.Add( arg );
				arg = ((VariableDeclarationStatement)arg).SubDeclaration;
			}
			
			stmt = new ProcDefinitionBlockStatement( id, argumentList, statementList, new TextRange( startOffset, this.Token.EndOffset ), isDebugOnly );
			return true;
		}

		/// <summary>
		/// Matches a <c>EventDefinitionBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>EventDefinitionBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>HSMEVENT</c>.
		/// </remarks>
		protected virtual bool MatchEventDefinitionBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier id = null;
			Statement arg = null;
			AstNodeList statementList = new AstNodeList( null );
			System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.ENDHSMEVENT };
			if (!this.Match(SanScriptTokenID.HSMEVENT))
				return false;
			if (!this.MatchIdentifier(out id)) {
				this.ReportSyntaxError( "Identifier expected." );
			}
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenParenthesis)) {
				if (!this.Match(SanScriptTokenID.OpenParenthesis))
					return false;
				if (this.IsInMultiMatchSet(10, this.LookAheadToken)) {
					if (!this.MatchVariableDeclarationStatement(null, true, true, true, true, false, false, out arg)) {
						this.ReportSyntaxError( "Invalid argument list." );
					}
				}
				if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
					this.ReportSyntaxError( "Closing parenthesis expected." );
				}
			}
			this.MatchBlockStatementList(endTokenIds, statementList);
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDHSMEVENT ) )
				{
				this.AdvanceToNext();
			}
			
			AstNodeList argumentList = new AstNodeList( null );
			while ( arg != null )
				{
				argumentList.Add( arg );
				arg = ((VariableDeclarationStatement)arg).SubDeclaration;
			}
			
			stmt = new EventDefinitionBlockStatement( id, argumentList, statementList, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>FuncDefinitionBlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>FuncDefinitionBlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>FUNC</c>.
		/// </remarks>
		protected virtual bool MatchFuncDefinitionBlockStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier returnType = null;
			Identifier id = null;
			Statement arg = null;
			AstNodeList statementList = new AstNodeList( null );
			System.Int32[] endTokenIds = new System.Int32[]{ SanScriptTokenID.ENDFUNC };
			bool isDebugOnly = false;
			
			if ( this.Token != null && this.TokenIs(this.Token, SanScriptTokenID.DEBUGONLY))
				{
				isDebugOnly = true;
			}
			if (!this.Match(SanScriptTokenID.FUNC))
				return false;
			if (this.IsInMultiMatchSet(8, this.LookAheadToken)) {
				if (!this.MatchNativeDataType(out returnType, false, false)) {
					this.ReportSyntaxError( "Missing or invalid return type." );
				}
			}
			else if (this.IsInMultiMatchSet(2, this.LookAheadToken)) {
				if (!this.MatchIdentifier(out returnType)) {
					this.ReportSyntaxError( "Missing or invalid return type." );
				}
			}
			else
				return false;
			if (!this.MatchIdentifier(out id)) {
				this.ReportSyntaxError( "Identifier expected." );
			}
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenParenthesis)) {
				if (!this.Match(SanScriptTokenID.OpenParenthesis))
					return false;
				if (this.IsInMultiMatchSet(10, this.LookAheadToken)) {
					if (!this.MatchVariableDeclarationStatement(null, true, true, true, true, false, false, out arg)) {
						this.ReportSyntaxError( "Invalid argument list." );
					}
				}
				if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
					this.ReportSyntaxError( "Closing parenthesis expected." );
				}
			}
			this.MatchBlockStatementList(endTokenIds, statementList);
			if ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.ENDFUNC ) )
				{
				this.AdvanceToNext();
			}
			
			AstNodeList argumentList = new AstNodeList( null );
			while ( arg != null )
				{
				argumentList.Add( arg );
				arg = ((VariableDeclarationStatement)arg).SubDeclaration;
			}
			
			stmt = new FuncDefinitionBlockStatement( returnType, id, argumentList, statementList, new TextRange( startOffset, this.Token.EndOffset ), isDebugOnly );
			return true;
		}

		/// <summary>
		/// Matches a <c>NativeStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>NativeStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>NATIVE</c>.
		/// </remarks>
		protected virtual bool MatchNativeStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			bool isProc = false;
			bool isEvent = false;
			bool isFunc = false;
			bool isDebugOnly = false;
			Identifier id = null;
			Identifier returnType = null;
			Identifier inheritsFrom = null;
			Statement arg = null;
			if (!this.Match(SanScriptTokenID.NATIVE))
				return false;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.DEBUGONLY)) {
				if (this.Match(SanScriptTokenID.DEBUGONLY)) {
					isDebugOnly = true;
				}
			}
			if (((this.TokenIs(this.LookAheadToken, SanScriptTokenID.PROC)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.HSMEVENT)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.FUNC)))) {
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.PROC)) {
					if (!this.Match(SanScriptTokenID.PROC))
						return false;
					else {
						isProc = true; --m_scopeLevel;
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.HSMEVENT)) {
					if (!this.Match(SanScriptTokenID.HSMEVENT))
						return false;
					else {
						isEvent = true; --m_scopeLevel;
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.FUNC)) {
					if (!this.Match(SanScriptTokenID.FUNC))
						return false;
					else {
						isFunc = true; --m_scopeLevel;
					}
				}
				else
					return false;
			}
			if ( isFunc )
				{
				if (this.IsInMultiMatchSet(8, this.LookAheadToken)) {
					if (!this.MatchNativeDataType(out returnType, false, false)) {
						this.ReportSyntaxError( "Missing or invalid return type." );
					}
				}
				else if (this.IsInMultiMatchSet(2, this.LookAheadToken)) {
					if (!this.MatchIdentifier(out returnType)) {
						this.ReportSyntaxError( "Missing or invalid return type." );
					}
				}
				else
					return false;
			}
			if (!this.MatchIdentifier(out id)) {
				this.ReportSyntaxError( "Identifier expected." );
			}
			if ( isProc || isEvent || isFunc )
				{
				bool parens = false;
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenParenthesis)) {
					if (this.Match(SanScriptTokenID.OpenParenthesis)) {
						parens = true;
					}
					if (this.IsInMultiMatchSet(10, this.LookAheadToken)) {
						if (!this.MatchVariableDeclarationStatement(null, true, true, true, true, false, false, out arg)) {
							this.ReportSyntaxError( "Invalid argument list." );
						}
					}
					if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
						if ( parens ) this.ReportSyntaxError( "Closing parenthesis expected." );
					}
					else {
						if ( !parens ) this.ReportSyntaxError( "Missing opening parenthesis." );
					}
				}
				AstNodeList argumentList = new AstNodeList( null );
				while ( arg != null )
					{
					argumentList.Add( arg );
					arg = ((VariableDeclarationStatement)arg).SubDeclaration;
				}
				
				if ( isProc )
					{
					stmt = new NativeProcDeclarationStatement( id, argumentList, new TextRange( startOffset, this.Token.EndOffset ), isDebugOnly );
				}
				else if ( isEvent )
					{
					stmt = new NativeEventDeclarationStatement( id, argumentList, new TextRange( startOffset, this.Token.EndOffset ) );
				}
				else if ( isFunc )
					{
					stmt = new NativeFuncDeclarationStatement( returnType, id, argumentList, new TextRange( startOffset, this.Token.EndOffset ), isDebugOnly );
				}
			}
			else
				{
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Colon)) {
					if (!this.Match(SanScriptTokenID.Colon))
						return false;
					if (!this.MatchIdentifier(out inheritsFrom)) {
						this.ReportSyntaxError( "Identifier expected." );
					}
				}
				stmt = new NativeTypeDeclarationStatement( id, inheritsFrom, new TextRange( startOffset, this.Token.EndOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>TypedefStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>TypedefStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>TYPEDEF</c>.
		/// </remarks>
		protected virtual bool MatchTypedefStatement(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			bool isProc = false;
			bool isFunc = false;
			Identifier id = null;
			Identifier returnType = null;
			Statement arg = null;
			if (!this.Match(SanScriptTokenID.TYPEDEF))
				return false;
			if (((this.TokenIs(this.LookAheadToken, SanScriptTokenID.PROC)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.FUNC)))) {
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.PROC)) {
					if (!this.Match(SanScriptTokenID.PROC))
						return false;
					else {
						isProc = true; --m_scopeLevel;
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.FUNC)) {
					if (!this.Match(SanScriptTokenID.FUNC))
						return false;
					else {
						isFunc = true; --m_scopeLevel;
					}
				}
				else
					return false;
			}
			if ( isFunc )
				{
				if (this.IsInMultiMatchSet(8, this.LookAheadToken)) {
					if (!this.MatchNativeDataType(out returnType, false, false)) {
						this.ReportSyntaxError( "Missing or invalid return type." );
					}
				}
				else if (this.IsInMultiMatchSet(2, this.LookAheadToken)) {
					if (!this.MatchIdentifier(out returnType)) {
						this.ReportSyntaxError( "Missing or invalid return type." );
					}
				}
				else
					return false;
			}
			if (!this.MatchIdentifier(out id)) {
				this.ReportSyntaxError( "Identifier expected." );
			}
			if ( isProc || isFunc )
				{
				bool parens = false;
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenParenthesis)) {
					if (this.Match(SanScriptTokenID.OpenParenthesis)) {
						parens = true;
					}
					if (this.IsInMultiMatchSet(10, this.LookAheadToken)) {
						if (!this.MatchVariableDeclarationStatement(null, true, true, true, true, false, false, out arg)) {
							this.ReportSyntaxError( "Invalid argument list." );
						}
					}
					if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
						if ( parens ) this.ReportSyntaxError( "Closing parenthesis expected." );
					}
					else {
						if ( !parens ) this.ReportSyntaxError( "Missing opening parenthesis." );
					}
				}
				AstNodeList argumentList = new AstNodeList( null );
				while ( arg != null )
					{
					argumentList.Add( arg );
					arg = ((VariableDeclarationStatement)arg).SubDeclaration;
				}
				
				if ( isProc )
					{
					stmt = new TypedefProcDeclarationStatement( id, argumentList, new TextRange( startOffset, this.Token.EndOffset ) );
				}
				else if ( isFunc )
					{
					stmt = new TypedefFuncDeclarationStatement( returnType, id, argumentList, new TextRange( startOffset, this.Token.EndOffset ) );
				}
			}
			else
				{
				this.ReportSyntaxError( "Invalid or incomplete TYPDEF declaration." );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>PreprocessorMacroStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>PreprocessorMacroStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>PoundIf</c>, <c>PoundEndif</c>.
		/// </remarks>
		protected virtual bool MatchPreprocessorMacroStatement(out Statement stmt) {
			stmt = null;
			System.Int32 tokenID = this.LookAheadToken.ID;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			Identifier name = null;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.PoundIf)) {
				if (!this.Match(SanScriptTokenID.PoundIf))
					return false;
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.NOT)) {
					this.Match(SanScriptTokenID.NOT);
				}
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.DEFINED)) {
					this.Match(SanScriptTokenID.DEFINED);
				}
				if (this.IsInMultiMatchSet(2, this.LookAheadToken)) {
					if (!this.MatchIdentifier(out name)) {
						this.ReportSyntaxError( "Identifier expected" );
					}
				}
				else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.OpenParenthesis)) {
					if (!this.Match(SanScriptTokenID.OpenParenthesis)) {
						this.ReportSyntaxError( "Missing open parenthesis." );
					}
					if (!this.MatchIdentifier(out name)) {
						this.ReportSyntaxError( "Identifier expected" );
					}
					if (!this.Match(SanScriptTokenID.CloseParenthesis)) {
						this.ReportSyntaxError( "Closing parenthesis expected." );
					}
				}
				else
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.PoundEndif)) {
				if (!this.Match(SanScriptTokenID.PoundEndif))
					return false;
			}
			else
				return false;
			stmt = new PreprocessorMacroStatement( tokenID, name, new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>MultiLineCommentBlock</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>MultiLineCommentBlock</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>MultiLineCommentStart</c>.
		/// </remarks>
		protected virtual bool MatchMultiLineCommentBlock(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			if (!this.Match(SanScriptTokenID.MultiLineCommentStart))
				return false;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.MultiLineComment)) {
				this.Match(SanScriptTokenID.MultiLineComment);
			}
			if (!this.Match(SanScriptTokenID.MultiLineCommentEnd)) {
				return false;
			}
			stmt = new MultiLineCommentBlock( new TextRange( startOffset, this.Token.EndOffset ) );
			return true;
		}

		/// <summary>
		/// Matches a <c>PurposeCommentBlock</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>PurposeCommentBlock</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>SingleLineCommentStart</c>.
		/// </remarks>
		protected virtual bool MatchPurposeCommentBlock(out Statement stmt) {
			stmt = null;
			System.Int32 startOffset = this.LookAheadToken.StartOffset;
			bool isPurpose = false;
			System.Int32 lastEndOffset = int.MaxValue;
			List<string> comments = new List<string>();
			string commentText = null;
			
			if ( !this.IsComment( out isPurpose ) )
				{
				return false;
			}
			if (!this.Match(SanScriptTokenID.SingleLineCommentStart))
				return false;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.SingleLineComment)) {
				if (this.Match(SanScriptTokenID.SingleLineComment)) {
					commentText = this.TokenText; lastEndOffset = this.Token.EndOffset + 1;
				}
			}
			if (this.Match(SanScriptTokenID.SingleLineCommentEnd)) {
				lastEndOffset = this.Token.EndOffset;
			}
			if ( commentText != null )
				{
				comments.Add( commentText );
			}
			else
				{
				comments.Add( string.Empty );
			}
			
			while ( this.TokenIs( this.LookAheadToken, SanScriptTokenID.SingleLineCommentStart ) )
				{
				/* FIXME:  This does not work when the comment block is indented.
				if ( this.LookAheadToken.StartOffset > lastEndOffset )
					{
					// we have an empty line
					break;
				}
				*/
				
				bool purposeFound = false;
				if ( !this.IsComment( out purposeFound ) )
					{
					// it's not a valid comment
					break;
				}
				
				if ( purposeFound )
					{
					// we have a PURPOSE block
					break;
				}
				
				commentText = null;
				this.Match(SanScriptTokenID.SingleLineCommentStart);
				if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.SingleLineComment)) {
					if (this.Match(SanScriptTokenID.SingleLineComment)) {
						commentText = this.TokenText; lastEndOffset = this.Token.EndOffset + 1;
					}
				}
				if (this.Match(SanScriptTokenID.SingleLineCommentEnd)) {
					lastEndOffset = this.Token.EndOffset;
				}
				if ( commentText != null )
					{
					comments.Add( commentText );
				}
				else
					{
					comments.Add( string.Empty );
				}
			}
			
			if ( !isPurpose )
				{
				if ( comments.Count > 1 )
					{
					stmt = new MultiLineCommentBlock( new TextRange( startOffset, this.Token.EndOffset ) );
				}
			}
			else
				{
				string[] sectionNames = new string[4]{ "/ PURPOSE:", "/ PARAMS:", "/ RETURNS:", "/ NOTES:" };
				string[] oldSectionNames = new string[4]{ "PURPOSE:", "PARAMS:", "RETURNS:", "NOTES:" };
				
				StringBuilder[] sections = new StringBuilder[4];
				
				int sectionNum = -1;
				foreach ( string comment in comments )
					{
					if ( (sectionNum < 3) && comment.StartsWith( sectionNames[sectionNum + 1] ) )
						{
						++sectionNum;
						sections[sectionNum] = new StringBuilder();
						
						sections[sectionNum].Append( comment.Substring( sectionNames[sectionNum].Length ).Trim() );
					}
					else if ( (sectionNum < 3) && comment.StartsWith( oldSectionNames[sectionNum + 1] ) )
						{
						++sectionNum;
						sections[sectionNum] = new StringBuilder();
						
						sections[sectionNum].Append( comment.Substring( oldSectionNames[sectionNum].Length ).Trim() );
					}
					else if ( (sectionNum > -1) && (sectionNum < 4) )
						{
						sections[sectionNum].Append( "\n" );
						
						if ( comment.StartsWith( "/" ) )
							{
							sections[sectionNum].Append( comment.Substring( 1 ).Trim() );
						}
						else
							{
							sections[sectionNum].Append( comment.Trim() );
						}
					}
				}
				
				stmt = new PurposeCommentBlock( sections[0].ToString(),
				(sections[1] != null) ? sections[1].ToString() : null,
				(sections[2] != null) ? sections[2].ToString() : null,
				(sections[3] != null) ? sections[3].ToString() : null,
				new TextRange( startOffset, this.Token.EndOffset ) );
			}
			return true;
		}

		/// <summary>
		/// Matches a <c>BlockStatement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>BlockStatement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>DEFAULT</c>, <c>ENUM</c>, <c>STRUCT</c>, <c>IF</c>, <c>ELIF</c>, <c>ELSE</c>, <c>WHILE</c>, <c>REPEAT</c>, <c>FOR</c>, <c>CASE</c>, <c>SWITCH</c>, <c>HSMSTATE</c>, <c>HSMACTIVATE</c>, <c>HSMDEACTIVATE</c>, <c>HASH_ENUM</c>, <c>STRICT_ENUM</c>, <c>GLOBALS</c>, <c>SCRIPT</c>, <c>PROC</c>, <c>HSMEVENT</c>, <c>FUNC</c>, <c>MultiLineCommentStart</c>, <c>SingleLineCommentStart</c>.
		/// </remarks>
		protected virtual bool MatchBlockStatement(out Statement stmt) {
			stmt = null;
			if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.MultiLineCommentStart)) {
				if (!this.MatchMultiLineCommentBlock(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.SingleLineCommentStart)) {
				if (!this.MatchPurposeCommentBlock(out stmt))
					return false;
			}
			else if (((this.TokenIs(this.LookAheadToken, SanScriptTokenID.IF)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.ELIF)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.ELSE)))) {
				if (!this.MatchIfBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.WHILE)) {
				if (!this.MatchWhileBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.REPEAT)) {
				if (!this.MatchRepeatBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.FOR)) {
				if (!this.MatchForBlockStatement(out stmt))
					return false;
			}
			else if (((this.TokenIs(this.LookAheadToken, SanScriptTokenID.DEFAULT)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.CASE)))) {
				if (!this.MatchCaseBlockStatement(false, out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.SWITCH)) {
				if (!this.MatchSwitchBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.STRUCT)) {
				if (!this.MatchStructDefinitionBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.HSMSTATE)) {
				if (!this.MatchStateDefinitionBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.HSMACTIVATE)) {
				if (!this.MatchActivateBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.HSMDEACTIVATE)) {
				if (!this.MatchDeactivateBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.ENUM)) {
				if (!this.MatchEnumDefinitionBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.HASH_ENUM)) {
				if (!this.MatchHashEnumDefinitionBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.STRICT_ENUM)) {
				if (!this.MatchStrictEnumDefinitionBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.GLOBALS)) {
				if (!this.MatchGlobalsDefinitionBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.SCRIPT)) {
				if (!this.MatchScriptBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.PROC)) {
				if (!this.MatchProcDefinitionBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.HSMEVENT)) {
				if (!this.MatchEventDefinitionBlockStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.FUNC)) {
				if (!this.MatchFuncDefinitionBlockStatement(out stmt))
					return false;
			}
			else
				return false;
			return true;
		}

		/// <summary>
		/// Matches a <c>Statement</c> non-terminal.
		/// </summary>
		/// <returns><c>true</c> if the <c>Statement</c> was matched successfully; otherwise, <c>false</c>.</returns>
		/// <remarks>
		/// The non-terminal can start with: <c>Identifier</c>, <c>UserIdentifier</c>, <c>NativeIdentifier</c>, <c>TypedefIdentifier</c>, <c>BOOL</c>, <c>FLOAT</c>, <c>TWEAK_FLOAT</c>, <c>INT</c>, <c>TWEAK_INT</c>, <c>VECTOR</c>, <c>STRING</c>, <c>VARARGS</c>, <c>TEXTLABEL</c>, <c>TEXTLABELTHREE</c>, <c>TEXTLABELSEVEN</c>, <c>TEXTLABELFIFTEEN</c>, <c>TEXTLABELTWENTYTHREE</c>, <c>TEXTLABELTHIRTYONE</c>, <c>TEXTLABELSIXTYTHREE</c>, <c>ENUMTOINTdatatype</c>, <c>StructArgument</c>, <c>DEFAULT</c>, <c>CALL</c>, <c>PoundIf</c>, <c>PoundEndif</c>, <c>CONSTINT</c>, <c>CONSTFLOAT</c>, <c>Increment</c>, <c>Decrement</c>, <c>HSMSUPER</c>, <c>FORWARD</c>, <c>ENUM</c>, <c>STRUCT</c>, <c>USING</c>, <c>THROW</c>, <c>GOTO</c>, <c>GOTOHSMSTATE</c>, <c>DEFAULTHSMSTATE</c>, <c>HSMHISTORY</c>, <c>BREAK</c>, <c>FALLTHRU</c>, <c>RETURN</c>, <c>EXIT</c>, <c>BREAKLOOP</c>, <c>RELOOP</c>, <c>IF</c>, <c>ELIF</c>, <c>ELSE</c>, <c>WHILE</c>, <c>REPEAT</c>, <c>FOR</c>, <c>CASE</c>, <c>SWITCH</c>, <c>HSMSTATE</c>, <c>HSMACTIVATE</c>, <c>HSMDEACTIVATE</c>, <c>HASH_ENUM</c>, <c>STRICT_ENUM</c>, <c>GLOBALS</c>, <c>SCRIPT</c>, <c>PROC</c>, <c>HSMEVENT</c>, <c>FUNC</c>, <c>NATIVE</c>, <c>TYPEDEF</c>, <c>MultiLineCommentStart</c>, <c>SingleLineCommentStart</c>.
		/// </remarks>
		protected virtual bool MatchStatement(out Statement stmt) {
			stmt = null;
			if (((this.TokenIs(this.LookAheadToken, SanScriptTokenID.CONSTINT)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.CONSTFLOAT)))) {
				if (!this.MatchConstDefinitionStatement(out stmt))
					return false;
			}
			else if (((this.TokenIs(this.LookAheadToken, SanScriptTokenID.Increment)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.Decrement)))) {
				if (!this.MatchPreAdditiveStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.HSMSUPER)) {
				if (!this.MatchSuperSubroutineAccessStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.FORWARD)) {
				if (!this.MatchForwardDeclarationStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.USING)) {
				if (!this.MatchUsingStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.THROW)) {
				if (!this.MatchThrowStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.GOTO)) {
				if (!this.MatchGotoStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.GOTOHSMSTATE)) {
				if (!this.MatchGotoStateStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.DEFAULTHSMSTATE)) {
				if (!this.MatchDefaultStateStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.HSMHISTORY)) {
				if (!this.MatchHistoryStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.BREAK)) {
				if (!this.MatchBreakStatement(out stmt))
					return false;
				else {
					++m_scopeLevel; this.ReportSyntaxError( stmt.TextRange, "Extra or misplaced BREAK statement." );
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.FALLTHRU)) {
				if (!this.MatchFallthruStatement(out stmt))
					return false;
				else {
					++m_scopeLevel; this.ReportSyntaxError( stmt.TextRange, "Extra or misplaced FALLTHRU statement." );
				}
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.RETURN)) {
				if (!this.MatchReturnStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.BREAKLOOP)) {
				if (!this.MatchBreakLoopStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.RELOOP)) {
				if (!this.MatchReLoopStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.EXIT)) {
				if (!this.MatchExitStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.NATIVE)) {
				if (!this.MatchNativeStatement(out stmt))
					return false;
			}
			else if (this.TokenIs(this.LookAheadToken, SanScriptTokenID.TYPEDEF)) {
				if (!this.MatchTypedefStatement(out stmt))
					return false;
			}
			else if (((this.TokenIs(this.LookAheadToken, SanScriptTokenID.PoundIf)) || (this.TokenIs(this.LookAheadToken, SanScriptTokenID.PoundEndif)))) {
				if (!this.MatchPreprocessorMacroStatement(out stmt))
					return false;
			}
			else if (this.IsInMultiMatchSet(4, this.LookAheadToken)) {
				if (!this.MatchIdentifierStatement(out stmt))
					return false;
			}
			else if (this.IsInMultiMatchSet(8, this.LookAheadToken)) {
				if (!this.MatchNativeVariableDeclarationStatement(out stmt))
					return false;
			}
			else if (this.IsInMultiMatchSet(11, this.LookAheadToken)) {
				if (!this.MatchBlockStatement(out stmt))
					return false;
			}
			else
				return false;
			return true;
		}

		/// <summary>
		/// Gets the multi-match sets array.
		/// </summary>
		/// <value>The multi-match sets array.</value>
		protected override bool[,] MultiMatchSets {
			get {
				return multiMatchSets;
			}
		}

		private const bool Y = true;
		private const bool n = false;
		private static bool[,] multiMatchSets = {
			// 0: Identifier UserIdentifier NativeIdentifier TypedefIdentifier BOOL FLOAT TWEAK_FLOAT INT TWEAK_INT VECTOR STRING VARARGS TEXTLABEL TEXTLABELTHREE TEXTLABELSEVEN TEXTLABELFIFTEEN TEXTLABELTWENTYTHREE TEXTLABELTHIRTYONE TEXTLABELSIXTYTHREE ENUMTOINTdatatype StructArgument DEFAULT CALL PoundIf PoundEndif CONSTINT CONSTFLOAT Increment Decrement HSMSUPER FORWARD ENUM STRUCT USING THROW GOTO GOTOHSMSTATE DEFAULTHSMSTATE HSMHISTORY BREAK FALLTHRU RETURN EXIT BREAKLOOP RELOOP IF ELIF ELSE WHILE REPEAT FOR CASE SWITCH HSMSTATE HSMACTIVATE HSMDEACTIVATE HASH_ENUM STRICT_ENUM GLOBALS SCRIPT PROC HSMEVENT FUNC NATIVE TYPEDEF MultiLineCommentStart SingleLineCommentStart 
			{n,n,n,n,n,Y,n,n,Y,n,n,n,n,n,n,n,Y,Y,Y,Y,Y,n,n,n,Y,Y,Y,Y,n,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,n,n,n,n,Y,n,Y,n,n,n,n,n,n,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,n,n,Y,n,Y,Y,Y,Y,Y,Y,n,n,Y,n,Y,Y,n,Y,n,Y,n,n,n,Y,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,Y,n,n,n,n,n,n,n,n},
			// 1: Identifier UserIdentifier NativeIdentifier TypedefIdentifier DEFAULT OpenVector OpenString TRUE FALSE IntegerValue FloatValue NULL OpenParenthesis CALL COUNTOF ENUMTOINT INTTOENUM INTTONATIVE NATIVETOINT SIZEOF HASH CATCH TIMESTEP PoundIf NOT PoundEndif Ampersand Subtraction AltNOT 
			{n,n,n,n,n,n,n,n,n,n,n,Y,Y,n,n,n,Y,Y,Y,Y,n,n,n,n,n,n,n,n,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,Y,n,Y,n,Y,n,n,n,Y,n,n,n,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,Y,n,Y,n,n,n,n,n,n,Y,Y,n,Y,n,n,Y,n,n,n,Y,n,Y,n,Y,n,n,n,Y,n,n,Y,n,Y,n,n,n,n,n,Y,n,n,n,n,n,n,n,n,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,n,n,n},
			// 2: Identifier UserIdentifier NativeIdentifier TypedefIdentifier 
			{n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,Y,Y,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n},
			// 3: DEFAULT OpenVector OpenString TRUE FALSE IntegerValue FloatValue NULL 
			{n,n,n,n,n,n,n,n,n,n,n,Y,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,Y,n,n,n,n,n,n,n,n,n,n,n,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,n,n,n,n,n,Y,n,n,n,n,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n},
			// 4: Identifier UserIdentifier NativeIdentifier TypedefIdentifier CALL 
			{n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,Y,Y,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n},
			// 5: Subtraction Addition PlusTimestepMultipliedBy MinusTimestepMultipliedBy 
			{n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,Y,n,n,n,n,n,n,n,Y,Y,n,n,n,n,n,n,n,n,n,n},
			// 6: AssignmentOrEquality Inequality AltInequality LessThanEqualTo LessThan GreaterThanEqualTo GreaterThan 
			{n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,Y,n,n,n,n,Y,Y,Y,Y,n,n,n,n,n,n,n,n,n,n,Y,n,n},
			// 7: AssignmentOrEquality PlusEquals MinusEquals MultiplyEquals DivideEquals Increment Decrement Colon 
			{n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,n,n,n,n,Y,n,n,n,n,n,n,n,n,n,n,n,n,Y,Y,Y,Y,Y,Y,n,n,n,n},
			// 8: BOOL FLOAT TWEAK_FLOAT INT TWEAK_INT VECTOR STRING VARARGS TEXTLABEL TEXTLABELTHREE TEXTLABELSEVEN TEXTLABELFIFTEEN TEXTLABELTWENTYTHREE TEXTLABELTHIRTYONE TEXTLABELSIXTYTHREE ENUMTOINTdatatype StructArgument 
			{n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,n,n,n,Y,n,n,Y,n,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n},
			// 9: Identifier UserIdentifier NativeIdentifier TypedefIdentifier 
			{n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,Y,Y,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n},
			// 10: Identifier UserIdentifier NativeIdentifier TypedefIdentifier BOOL FLOAT TWEAK_FLOAT INT TWEAK_INT VECTOR STRING VARARGS TEXTLABEL TEXTLABELTHREE TEXTLABELSEVEN TEXTLABELFIFTEEN TEXTLABELTWENTYTHREE TEXTLABELTHIRTYONE TEXTLABELSIXTYTHREE ENUMTOINTdatatype StructArgument 
			{n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,Y,Y,Y,Y,n,n,n,Y,n,n,Y,n,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n},
			// 11: DEFAULT ENUM STRUCT IF ELIF ELSE WHILE REPEAT FOR CASE SWITCH HSMSTATE HSMACTIVATE HSMDEACTIVATE HASH_ENUM STRICT_ENUM GLOBALS SCRIPT PROC HSMEVENT FUNC MultiLineCommentStart SingleLineCommentStart 
			{n,n,n,n,n,Y,n,n,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n}
		};

	}
	#endregion

}
