namespace SanScriptParser
{
    partial class SanScriptCompilingSettingsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ragScriptEditorShared.CompilingSettings compilingSettings1 = new ragScriptEditorShared.CompilingSettings();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SanScriptCompilingSettingsControl));
            this.debugParserCheckBox = new System.Windows.Forms.CheckBox();
            this.dumpThreadStateCheckBox = new System.Windows.Forms.CheckBox();
            this.displayDisassemblyCheckBox = new System.Windows.Forms.CheckBox();
            this.enableHSMCheckBox = new System.Windows.Forms.CheckBox();
            this.dependecyFileCheckBox = new System.Windows.Forms.CheckBox();
            this.dependencyFileTextBox = new System.Windows.Forms.TextBox();
            this.nativeFileCheckBox = new System.Windows.Forms.CheckBox();
            this.nativeFileTextBox = new System.Windows.Forms.TextBox();
            this.globalsFileLabel = new System.Windows.Forms.Label();
            this.globalsFileTextBox = new System.Windows.Forms.TextBox();
            this.globalsFileBrowseButton = new System.Windows.Forms.Button();
            this.customLabel = new System.Windows.Forms.Label();
            this.customTextBox = new System.Windows.Forms.TextBox();
            this.commandLineRichTextBox = new System.Windows.Forms.RichTextBox();
            this.globalsOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.warningsAsErrorsCheckBox = new System.Windows.Forms.CheckBox();
            this.compileToConfigurationFolderCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            //
            // debugParserCheckBox
            //
            this.debugParserCheckBox.AutoSize = true;
            this.debugParserCheckBox.Location = new System.Drawing.Point(2, 229);
            this.debugParserCheckBox.Name = "debugParserCheckBox";
            this.debugParserCheckBox.Size = new System.Drawing.Size(91, 17);
            this.debugParserCheckBox.TabIndex = 12;
            this.debugParserCheckBox.Text = "Debug Parser";
            this.debugParserCheckBox.UseVisualStyleBackColor = true;
            this.debugParserCheckBox.CheckedChanged += new System.EventHandler(this.SanScriptSettingChanged);
            //
            // dumpThreadStateCheckBox
            //
            this.dumpThreadStateCheckBox.AutoSize = true;
            this.dumpThreadStateCheckBox.Location = new System.Drawing.Point(125, 229);
            this.dumpThreadStateCheckBox.Name = "dumpThreadStateCheckBox";
            this.dumpThreadStateCheckBox.Size = new System.Drawing.Size(119, 17);
            this.dumpThreadStateCheckBox.TabIndex = 13;
            this.dumpThreadStateCheckBox.Text = "Dump Thread State";
            this.dumpThreadStateCheckBox.UseVisualStyleBackColor = true;
            this.dumpThreadStateCheckBox.CheckedChanged += new System.EventHandler(this.SanScriptSettingChanged);
            //
            // displayDisassemblyCheckBox
            //
            this.displayDisassemblyCheckBox.AutoSize = true;
            this.displayDisassemblyCheckBox.Location = new System.Drawing.Point(268, 229);
            this.displayDisassemblyCheckBox.Name = "displayDisassemblyCheckBox";
            this.displayDisassemblyCheckBox.Size = new System.Drawing.Size(121, 17);
            this.displayDisassemblyCheckBox.TabIndex = 14;
            this.displayDisassemblyCheckBox.Text = "Display Disassembly";
            this.displayDisassemblyCheckBox.UseVisualStyleBackColor = true;
            this.displayDisassemblyCheckBox.CheckedChanged += new System.EventHandler(this.SanScriptSettingChanged);
            //
            // enableHSMCheckBox
            //
            this.enableHSMCheckBox.AutoSize = true;
            this.enableHSMCheckBox.Location = new System.Drawing.Point(2, 256);
            this.enableHSMCheckBox.Name = "enableHSMCheckBox";
            this.enableHSMCheckBox.Size = new System.Drawing.Size(86, 17);
            this.enableHSMCheckBox.TabIndex = 15;
            this.enableHSMCheckBox.Text = "Enable HSM";
            this.enableHSMCheckBox.UseVisualStyleBackColor = true;
            this.enableHSMCheckBox.CheckedChanged += new System.EventHandler(this.SanScriptSettingChanged);
            //
            // dependecyFileCheckBox
            //
            this.dependecyFileCheckBox.AutoSize = true;
            this.dependecyFileCheckBox.Location = new System.Drawing.Point(2, 283);
            this.dependecyFileCheckBox.Name = "dependecyFileCheckBox";
            this.dependecyFileCheckBox.Size = new System.Drawing.Size(106, 17);
            this.dependecyFileCheckBox.TabIndex = 16;
            this.dependecyFileCheckBox.Text = "Dependency File";
            this.dependecyFileCheckBox.UseVisualStyleBackColor = true;
            this.dependecyFileCheckBox.CheckedChanged += new System.EventHandler(this.dependecyFileCheckBox_CheckedChanged);
            //
            // dependencyFileTextBox
            //
            this.dependencyFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dependencyFileTextBox.Location = new System.Drawing.Point(125, 281);
            this.dependencyFileTextBox.Name = "dependencyFileTextBox";
            this.dependencyFileTextBox.Size = new System.Drawing.Size(372, 20);
            this.dependencyFileTextBox.TabIndex = 17;
            this.dependencyFileTextBox.TextChanged += new System.EventHandler(this.SanScriptSettingChanged);
            //
            // nativeFileCheckBox
            //
            this.nativeFileCheckBox.AutoSize = true;
            this.nativeFileCheckBox.Location = new System.Drawing.Point(2, 310);
            this.nativeFileCheckBox.Name = "nativeFileCheckBox";
            this.nativeFileCheckBox.Size = new System.Drawing.Size(76, 17);
            this.nativeFileCheckBox.TabIndex = 18;
            this.nativeFileCheckBox.Text = "Native File";
            this.nativeFileCheckBox.UseVisualStyleBackColor = true;
            this.nativeFileCheckBox.CheckedChanged += new System.EventHandler(this.nativeFileCheckBox_CheckedChanged);
            //
            // nativeFileTextBox
            //
            this.nativeFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nativeFileTextBox.Location = new System.Drawing.Point(125, 307);
            this.nativeFileTextBox.Name = "nativeFileTextBox";
            this.nativeFileTextBox.Size = new System.Drawing.Size(372, 20);
            this.nativeFileTextBox.TabIndex = 19;
            this.nativeFileTextBox.TextChanged += new System.EventHandler(this.SanScriptSettingChanged);
            //
            // globalsFileLabel
            //
            this.globalsFileLabel.AutoSize = true;
            this.globalsFileLabel.Location = new System.Drawing.Point(58, 337);
            this.globalsFileLabel.Name = "globalsFileLabel";
            this.globalsFileLabel.Size = new System.Drawing.Size(61, 13);
            this.globalsFileLabel.TabIndex = 20;
            this.globalsFileLabel.Text = "Globals File";
            //
            // globalsFileTextBox
            //
            this.globalsFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.globalsFileTextBox.Location = new System.Drawing.Point(125, 334);
            this.globalsFileTextBox.Name = "globalsFileTextBox";
            this.globalsFileTextBox.Size = new System.Drawing.Size(341, 20);
            this.globalsFileTextBox.TabIndex = 21;
            this.globalsFileTextBox.TextChanged += new System.EventHandler(this.SanScriptSettingChanged);
            //
            // globalsFileBrowseButton
            //
            this.globalsFileBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.globalsFileBrowseButton.Location = new System.Drawing.Point(472, 332);
            this.globalsFileBrowseButton.Name = "globalsFileBrowseButton";
            this.globalsFileBrowseButton.Size = new System.Drawing.Size(25, 23);
            this.globalsFileBrowseButton.TabIndex = 22;
            this.globalsFileBrowseButton.Text = "...";
            this.globalsFileBrowseButton.UseVisualStyleBackColor = true;
            this.globalsFileBrowseButton.Click += new System.EventHandler(this.globalsFileBrowseButton_Click);
            //
            // customLabel
            //
            this.customLabel.AutoSize = true;
            this.customLabel.Location = new System.Drawing.Point(77, 363);
            this.customLabel.Name = "customLabel";
            this.customLabel.Size = new System.Drawing.Size(42, 13);
            this.customLabel.TabIndex = 23;
            this.customLabel.Text = "Custom";
            //
            // customTextBox
            //
            this.customTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customTextBox.Location = new System.Drawing.Point(125, 360);
            this.customTextBox.Name = "customTextBox";
            this.customTextBox.Size = new System.Drawing.Size(372, 20);
            this.customTextBox.TabIndex = 24;
            this.customTextBox.TextChanged += new System.EventHandler(this.SanScriptSettingChanged);
            //
            // commandLineRichTextBox
            //
            this.commandLineRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.commandLineRichTextBox.Location = new System.Drawing.Point(2, 386);
            this.commandLineRichTextBox.Name = "commandLineRichTextBox";
            this.commandLineRichTextBox.ReadOnly = true;
            this.commandLineRichTextBox.Size = new System.Drawing.Size(495, 189);
            this.commandLineRichTextBox.TabIndex = 25;
            this.commandLineRichTextBox.Text = "";
            //
            // globalsOpenFileDialog
            //
            this.globalsOpenFileDialog.Filter = "San Script Globals Files (*.sgv)|*.sgv|All Files (*.*)|*.*";
            this.globalsOpenFileDialog.RestoreDirectory = true;
            this.globalsOpenFileDialog.Title = "Select the Globals File";
            //
            // warningsAsErrorsCheckBox
            //
            this.warningsAsErrorsCheckBox.AutoSize = true;
            this.warningsAsErrorsCheckBox.Location = new System.Drawing.Point(125, 256);
            this.warningsAsErrorsCheckBox.Name = "warningsAsErrorsCheckBox";
            this.warningsAsErrorsCheckBox.Size = new System.Drawing.Size(139, 17);
            this.warningsAsErrorsCheckBox.TabIndex = 26;
            this.warningsAsErrorsCheckBox.Text = "Treat warnings as errors";
            this.warningsAsErrorsCheckBox.UseVisualStyleBackColor = true;
            this.warningsAsErrorsCheckBox.CheckedChanged += new System.EventHandler(this.SanScriptSettingChanged);
            //
            // compileToConfigurationFolderCheckBox
            //
            this.compileToConfigurationFolderCheckBox.AutoSize = true;
            this.compileToConfigurationFolderCheckBox.Location = new System.Drawing.Point(268, 258);
            this.compileToConfigurationFolderCheckBox.Name = "compileToConfigurationFolderCheckBox";
            this.compileToConfigurationFolderCheckBox.Size = new System.Drawing.Size(168, 17);
            this.compileToConfigurationFolderCheckBox.TabIndex = 27;
            this.compileToConfigurationFolderCheckBox.Text = "Compile to configuration folder";
            this.compileToConfigurationFolderCheckBox.UseVisualStyleBackColor = true;
            this.compileToConfigurationFolderCheckBox.CheckedChanged += new System.EventHandler(this.compileToConfigurationFolderCheckBox_CheckedChanged);
            //
            // SanScriptCompilingSettingsControl
            //
            this.Controls.Add(this.compileToConfigurationFolderCheckBox);
            this.Controls.Add(this.warningsAsErrorsCheckBox);
            this.Controls.Add(this.debugParserCheckBox);
            this.Controls.Add(this.enableHSMCheckBox);
            this.Controls.Add(this.displayDisassemblyCheckBox);
            this.Controls.Add(this.dumpThreadStateCheckBox);
            this.Controls.Add(this.dependencyFileTextBox);
            this.Controls.Add(this.dependecyFileCheckBox);
            this.Controls.Add(this.nativeFileTextBox);
            this.Controls.Add(this.nativeFileCheckBox);
            this.Controls.Add(this.globalsFileTextBox);
            this.Controls.Add(this.globalsFileLabel);
            this.Controls.Add(this.globalsFileBrowseButton);
            this.Controls.Add(this.customTextBox);
            this.Controls.Add(this.commandLineRichTextBox);
            this.Controls.Add(this.customLabel);
            this.Name = "SanScriptCompilingSettingsControl";
            this.Size = new System.Drawing.Size(504, 578);
            compilingSettings1.CompilerExecutable = "";
            compilingSettings1.ConfigurationName = null;
            compilingSettings1.Custom = "";
            compilingSettings1.IncludeFile = "";
            compilingSettings1.IncludePath = "";
            compilingSettings1.IncludePaths = ((System.Collections.Generic.List<string>)(resources.GetObject("compilingSettings1.IncludePaths")));
            compilingSettings1.OutputDirectory = "";
            compilingSettings1.PostCompileCommand = "";
            compilingSettings1.BuildDirectory = "";
            compilingSettings1.ResourceExecutable = "";
            compilingSettings1.CompilerExecutable = "";
            compilingSettings1.XmlIncludeFile = "";
            this.TabSettings = compilingSettings1;
            this.Controls.SetChildIndex(this.customLabel, 0);
            this.Controls.SetChildIndex(this.commandLineRichTextBox, 0);
            this.Controls.SetChildIndex(this.customTextBox, 0);
            this.Controls.SetChildIndex(this.globalsFileBrowseButton, 0);
            this.Controls.SetChildIndex(this.globalsFileLabel, 0);
            this.Controls.SetChildIndex(this.globalsFileTextBox, 0);
            this.Controls.SetChildIndex(this.nativeFileCheckBox, 0);
            this.Controls.SetChildIndex(this.nativeFileTextBox, 0);
            this.Controls.SetChildIndex(this.dependecyFileCheckBox, 0);
            this.Controls.SetChildIndex(this.dependencyFileTextBox, 0);
            this.Controls.SetChildIndex(this.dumpThreadStateCheckBox, 0);
            this.Controls.SetChildIndex(this.displayDisassemblyCheckBox, 0);
            this.Controls.SetChildIndex(this.enableHSMCheckBox, 0);
            this.Controls.SetChildIndex(this.debugParserCheckBox, 0);
            this.Controls.SetChildIndex(this.warningsAsErrorsCheckBox, 0);
            this.Controls.SetChildIndex(this.compileToConfigurationFolderCheckBox, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox debugParserCheckBox;
        private System.Windows.Forms.CheckBox dumpThreadStateCheckBox;
        private System.Windows.Forms.CheckBox displayDisassemblyCheckBox;
        private System.Windows.Forms.CheckBox enableHSMCheckBox;
        private System.Windows.Forms.CheckBox dependecyFileCheckBox;
        private System.Windows.Forms.TextBox dependencyFileTextBox;
        private System.Windows.Forms.CheckBox nativeFileCheckBox;
        private System.Windows.Forms.TextBox nativeFileTextBox;
        private System.Windows.Forms.Label globalsFileLabel;
        private System.Windows.Forms.TextBox globalsFileTextBox;
        private System.Windows.Forms.Button globalsFileBrowseButton;
        private System.Windows.Forms.Label customLabel;
        private System.Windows.Forms.TextBox customTextBox;
        private System.Windows.Forms.RichTextBox commandLineRichTextBox;
        private System.Windows.Forms.OpenFileDialog globalsOpenFileDialog;
        private System.Windows.Forms.CheckBox warningsAsErrorsCheckBox;
		private System.Windows.Forms.CheckBox compileToConfigurationFolderCheckBox;
    }
}
