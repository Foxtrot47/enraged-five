//#define OUTPUT_SEMANTIC_PARSE_TIMES
//#define DELAYED_INTELLIPROMPT

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ActiproSoftware.SyntaxEditor;
using ragScriptEditorShared;
using RSG.Base.Extensions;

namespace SanScriptParser
{

    /// <summary>
    /// Represents a <c>Simple</c> language definition.
    /// </summary>
    public class SanScriptSyntaxLanguage : MergableSyntaxLanguage, ISemanticParserServiceProcessor 
    {
    	/// <summary>
		/// Initializes a new instance of the <c>SanScriptSyntaxLanguage</c> class.
		/// </summary>
        public SanScriptSyntaxLanguage()
            : base( "SanScript" ) 
        {
#if DELAYED_INTELLIPROMPT
            this.UserInterfaceUpdateDelay = 50;
#endif

            StringBuilder text = new StringBuilder();
            text.Append( "USING \"..\\..\\scripting\\builtins.sc\"\n" );
            text.Append( "\n" );
            text.Append( "SCRIPT\n" );
            text.Append( "\tACTOR theGent = FindNamedActor(\"gent\")\n" );
            text.Append( "\tBOOL done = FALSE\n" );
            text.Append( "\n" );
            text.Append( "\tVOLUME fz = FindNamedVolume(\"TheForbiddenZone\")\n" );
            text.Append( "\tVOLUME teleFrom = FindNamedVolume(\"TeleportFrom\")\n" );
            text.Append( "\tVECTOR teleTo\n" );
            text.Append( "\tGetVolumeCenter(FindNamedVolume(\"TeleportTo\"), teleTo)\n" );
            text.Append( "\n" );
            text.Append( "\t// check it out:\n" );
            text.Append( "\tIF NOT IS_ACTOR_DEAD(theGent)\n" );
            text.Append( "\t\tPRINT_BIG(\"you're awesome\",10.0)\n" );
            text.Append( "\t\tINCREASE_ACTOR_MAX_HEALTH(theGent,100.0)\n" );
            text.Append( "\tENDIF	\n" );
            text.Append( "\n" );
            text.Append( "\tWHILE NOT done\n" );
            text.Append( "\t\tIF IsActorInVolume(theGent, fz)\n" );
            text.Append( "\t\t\tKillActor(theGent)\n" );
            text.Append( "\t\t\tPRINTFLOAT(GET_ACTOR_HEALTH(theGent))\n" );
            text.Append( "\t\t\tSET done = TRUE\n" );
            text.Append( "\t\tENDIF\n" );
            text.Append( "\n" );
            text.Append( "\t\tIF IS_ACTOR_DEAD(theGent)\n" );
            text.Append( "\t\t\tPRINT_BIG(\"you suck\",10.0)\n" );
            text.Append( "\t\tENDIF	\n" );
            text.Append( "\n" );
            text.Append( "\t\tIF IsActorInVolume(theGent, teleFrom)\n" );
            text.Append( "\t\t\tVECTOR offset\n" );
            text.Append( "\t\t\tVECTOR fromCtr\n" );
            text.Append( "\t\t\tGetVolumeCenter(teleFrom, fromCtr)\n" );
            text.Append( "\t\t\tVECTOR gentPos\n" );
            text.Append( "\t\t\tGetPosition(theGent, gentPos)\n" );
            text.Append( "\t\t\tVSUBTRACT(gentPos, fromCtr, offset)\n" );
            text.Append( "\t\t\tVADD(offset, teleTo, offset)\n" );
            text.Append( "\t\t\tTeleportActor(theGent, offset)\n" );
            text.Append( "\t\tENDIF\n" );
            text.Append( "\n" );
            text.Append( "\t\tWAIT( 0 )\n" );
            text.Append( "\n" );
            text.Append( "\tENDWHILE\n" );
            text.Append( "\n" );
            text.Append( "ENDSCRIPT\n" );
            this.ExampleText = text.ToString();

			// Initialize highlighting styles
			this.HighlightingStyles.Add( new HighlightingStyle( c_reservedWordStyleKey, null, Color.Blue, Color.Empty ) );
			this.HighlightingStyles.Add( new HighlightingStyle( c_nativeTypeStyleKey, null, Color.Blue, Color.Empty ) );
			this.HighlightingStyles.Add( new HighlightingStyle( c_operatorStyleKey, null, Color.Black, Color.Empty ) );
            this.HighlightingStyles.Add( new HighlightingStyle( c_reservedWordOperatorStyleKey, null, Color.Blue, Color.Empty ) );
            this.HighlightingStyles.Add( new HighlightingStyle( c_numberStyleKey, null, Color.Black, Color.Empty ) );
            this.HighlightingStyles.Add( new HighlightingStyle( c_stringDelimeterStyleKey, null, Color.Black, Color.Empty ) );
            this.HighlightingStyles.Add( new HighlightingStyle( c_stringDefaultStyleKey, null, Color.Gray, Color.Empty ) );
            this.HighlightingStyles.Add( new HighlightingStyle( c_commentDelimeterStyleKey, null, Color.Green, Color.Empty ) );
            this.HighlightingStyles.Add( new HighlightingStyle( c_commentDefaultStyleKey, null, Color.Green, Color.Empty ) );
            this.HighlightingStyles.Add( new HighlightingStyle( c_builtInWordStyleKey, null, Color.Brown, Color.Empty ) );
            this.HighlightingStyles.Add( new HighlightingStyle( c_userWordStyleKey, null, Color.Teal, Color.Empty ) );
            this.HighlightingStyles.Add( new HighlightingStyle( c_preprocessorMacroStyleKey, null, Color.Blue, Color.Empty ) );
            this.HighlightingStyles.Add( new HighlightingStyle( c_preprocessorDisabledStyleKey, null, Color.Gray, Color.Empty ) );
            this.HighlightingStyles.Add( new HighlightingStyle( c_bracketHighlightingStyleKey, null, Color.Empty, Color.Silver ) );

			// Initialize lexical states
			this.LexicalStates.Add( new DefaultLexicalState( SanScriptLexicalStateID.Default, c_defaultStateKey ) );
            this.LexicalStates.Add( new DefaultLexicalState( SanScriptLexicalStateID.StringState, c_stringStateKey ) );
            this.LexicalStates.Add( new DefaultLexicalState( SanScriptLexicalStateID.CommentState, c_commentStateKey ) );
            this.LexicalStates.Add( new DefaultLexicalState( SanScriptLexicalStateID.MultiLineCommentState, c_multiLineCommentStateKey ) );
            
            this.LexicalStates[c_stringStateKey].LexicalScopes.Add( new ProgrammaticLexicalScope(
                new ProgrammaticLexicalScopeMatchDelegate( m_lexicalParser.IsStringStateScopeStart ),
                new ProgrammaticLexicalScopeMatchDelegate( m_lexicalParser.IsStringStateScopeEnd ) ) );
            this.LexicalStates[c_commentStateKey].LexicalScopes.Add( new ProgrammaticLexicalScope(
                new ProgrammaticLexicalScopeMatchDelegate( m_lexicalParser.IsCommentStateScopeStart ),
                new ProgrammaticLexicalScopeMatchDelegate( m_lexicalParser.IsCommentStateScopeEnd ) ) );
            this.LexicalStates[c_multiLineCommentStateKey].LexicalScopes.Add( new ProgrammaticLexicalScope(
                new ProgrammaticLexicalScopeMatchDelegate( m_lexicalParser.IsMultiLineCommentStateScopeStart ),
                new ProgrammaticLexicalScopeMatchDelegate( m_lexicalParser.IsMultiLineCommentStateScopeEnd ) ) );

            this.LexicalStates[c_defaultStateKey].DefaultHighlightingStyle = this.HighlightingStyles[c_defaultStyleKey];
            this.LexicalStates[c_stringStateKey].DefaultHighlightingStyle = this.HighlightingStyles[c_stringDefaultStyleKey];
            this.LexicalStates[c_commentStateKey].DefaultHighlightingStyle = this.HighlightingStyles[c_commentDefaultStyleKey];
            this.LexicalStates[c_multiLineCommentStateKey].DefaultHighlightingStyle = this.HighlightingStyles[c_commentDefaultStyleKey];

			this.DefaultLexicalState = this.LexicalStates[c_defaultStateKey];
            this.LexicalStates[c_defaultStateKey].ChildLexicalStates.Add( this.LexicalStates[c_stringStateKey] );
            this.LexicalStates[c_defaultStateKey].ChildLexicalStates.Add( this.LexicalStates[c_commentStateKey] );
            this.LexicalStates[c_defaultStateKey].ChildLexicalStates.Add( this.LexicalStates[c_multiLineCommentStateKey] );

            // initialize keyword identifiers
            SanScriptIdentifierFile idFile =
                SanScriptProjectResolver.IdentifierCache.GetIdentifierFileByName(SanScriptProjectResolver.KeywordIdentifiersFileName);
            idFile.Lock.EnterWriteLock();
            try
            {
                idFile.Reset();
                int keywordsHashcode = idFile.FilenameHashCode;

                foreach (KeyValuePair<string, int> pair in this.m_lexicalParser.Keywords)
                {
                    switch (pair.Value)
                    {
                        case SanScriptTokenID.COUNTOF:
                        {
                            SanScriptKeywordSubroutine sub = new SanScriptKeywordSubroutine(pair.Key, pair.Value, keywordsHashcode,
                                CountOfExpression.DefinitionText, CountOfExpression.DefinitionMarkupText);
                            sub.ArgumentsMarkupText.AddRange(CountOfExpression.DefinitionArgumentsMarkupText);

                            SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, sub);
                        }
                            break;
                        case SanScriptTokenID.HASH:
                        {
                            SanScriptKeywordSubroutine sub = new SanScriptKeywordSubroutine(pair.Key, pair.Value, keywordsHashcode,
                                HashExpression.DefinitionText, HashExpression.DefinitionMarkupText);
                            sub.ArgumentsMarkupText.AddRange(HashExpression.DefinitionArgumentsMarkupText);

                            SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, sub);
                        }
                            break;
                        case SanScriptTokenID.ENUMTOINT:
                        {
                            SanScriptKeywordSubroutine sub = new SanScriptKeywordSubroutine(pair.Key, pair.Value, keywordsHashcode,
                                EnumToIntExpression.DefinitionText, EnumToIntExpression.DefinitionMarkupText);
                            sub.ArgumentsMarkupText.AddRange(EnumToIntExpression.DefinitionArgumentsMarkupText);

                            SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, sub);
                        }
                            break;
                        case SanScriptTokenID.INTTOENUM:
                        {
                            SanScriptKeywordSubroutine sub = new SanScriptKeywordSubroutine(pair.Key, pair.Value, keywordsHashcode,
                                IntToEnumExpression.DefinitionText, IntToEnumExpression.DefinitionMarkupText);
                            sub.ArgumentsMarkupText.AddRange(IntToEnumExpression.DefinitionArgumentsMarkupText);

                            SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, sub);
                        }
                            break;
                        case SanScriptTokenID.INTTONATIVE:
                        {
                            SanScriptKeywordSubroutine sub = new SanScriptKeywordSubroutine(pair.Key, pair.Value, keywordsHashcode,
                                IntToNativeExpression.DefinitionText, IntToNativeExpression.DefinitionMarkupText);
                            sub.ArgumentsMarkupText.AddRange(IntToNativeExpression.DefinitionArgumentsMarkupText);

                            SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, sub);
                        }
                            break;
                        case SanScriptTokenID.NATIVETOINT:
                        {
                            SanScriptKeywordSubroutine sub = new SanScriptKeywordSubroutine(pair.Key, pair.Value, keywordsHashcode,
                                NativeToIntExpression.DefinitionText, NativeToIntExpression.DefinitionMarkupText);
                            sub.ArgumentsMarkupText.AddRange(NativeToIntExpression.DefinitionArgumentsMarkupText);

                            SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, sub);
                        }
                            break;
                        case SanScriptTokenID.SIZEOF:
                        {
                            SanScriptKeywordSubroutine sub = new SanScriptKeywordSubroutine(pair.Key, pair.Value, keywordsHashcode,
                                SizeOfExpression.DefinitionText, SizeOfExpression.DefinitionMarkupText);
                            sub.ArgumentsMarkupText.AddRange(SizeOfExpression.DefinitionArgumentsMarkupText);

                            SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, sub);
                        }
                            break;
                        case SanScriptTokenID.THROW:
                        {
                            SanScriptKeywordSubroutine sub = new SanScriptKeywordSubroutine(pair.Key, pair.Value, keywordsHashcode,
                                ThrowStatement.DefinitionText, ThrowStatement.DefinitionMarkupText);
                            sub.ArgumentsMarkupText.AddRange(ThrowStatement.DefinitionArgumentsMarkupText);

                            SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, sub);
                        }
                            break;
                        case SanScriptTokenID.TIMESTEP:
                        {
                            SanScriptKeywordSubroutine sub = new SanScriptKeywordSubroutine(pair.Key, pair.Value, keywordsHashcode,
                                TimestepExpression.DefinitionText, TimestepExpression.DefinitionMarkupText);

                            SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, sub);
                        }
                            break;
                        default:
                        {
                            if ( (pair.Value > SanScriptTokenID.WordOperatorStart) && (pair.Value < SanScriptTokenID.WordOperatorEnd) )
                            {
                                SanScriptWordOperator word = new SanScriptWordOperator(pair.Key, pair.Value, keywordsHashcode, string.Empty, string.Empty);
                                SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, word);
                            }
                            else
                            {
                                SanScriptKeyword word = new SanScriptKeyword(pair.Key, pair.Value, keywordsHashcode, string.Empty, string.Empty);
                                SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, word);
                            }
                        }
                            break;
                    }

                    SanScriptProjectResolver.IdentifierCache.FinishUpdatingIdentifierFile(idFile);
                }
            }
            finally
            {
                idFile.Lock.ExitWriteLock();
            }
        }

        #region Constants
        private const string c_defaultStyleKey = "DefaultStyle";
        private const string c_reservedWordStyleKey = "ReservedWordStyle";
        private const string c_nativeTypeStyleKey = "NativeTypeStyle";
        private const string c_operatorStyleKey = "OperatorStyle";
        private const string c_reservedWordOperatorStyleKey = "ReservedWordOperatorStyle";
        private const string c_numberStyleKey = "NumberStyle";
        private const string c_stringDelimeterStyleKey = "StringDelimiterStyle";
        private const string c_stringDefaultStyleKey = "StringDefaultStyle";
        private const string c_commentDelimeterStyleKey = "CommentDelimiterStyle";
        private const string c_commentDefaultStyleKey = "CommentDefaultStyle";
        private const string c_builtInWordStyleKey = "BuiltInWordStyle";
        private const string c_userWordStyleKey = "UserWordStyle";
        private const string c_preprocessorMacroStyleKey = "PreprocessorMacroStyle";
        private const string c_preprocessorDisabledStyleKey = "PreprocessorDisabledStyle";
        private const string c_bracketHighlightingStyleKey = "BracketHighlightingStyle";

        private const string c_defaultStateKey = "DefaultState";
        private const string c_stringStateKey = "StringState";
        private const string c_commentStateKey = "CommentState";
        private const string c_multiLineCommentStateKey = "MultiLineCommentState";
        #endregion

        #region Variables
        private InfoTipTextDelegate m_infoTipTextDel = null;
   		private SanScriptLexicalParser m_lexicalParser = new SanScriptLexicalParser();

        private bool m_autocompleteEnabled = true;
        private bool m_autocompleteWithBracketsEnabled = true;
        private bool m_showItemsThatContainCurrentString = true;
        private bool m_addIncludePathSymbols = true;
        private bool m_addProjectSymbols = true;
        private bool m_highlightQuickInfoPopups = true;
        private bool m_highlightDisabledCodeBlocks = true;
        private bool m_completeWordOnFirstMatch = true;
        private bool m_completeWordOnExactMatch = true;

        private readonly List<IntelliPromptMemberListItem> m_fullIntelliPromptMemberList = new List<IntelliPromptMemberListItem>();

#if OUTPUT_SEMANTIC_PARSE_TIMES
        private int m_semanticParseTicks = 0;
        private int m_updateCacheTicks = 0;
        private int m_fixupTicks = 0;
#endif

#if DELAYED_INTELLIPROMPT
        private bool m_checkToShowIntelliPrompt = false;
        private bool m_checkToAbortIntelliPrompt = false;
        private bool m_checkToAbortIsBackspace = false;
#endif

        private bool m_reflectionIconsEnabled = true;
        #endregion

        #region Properties
        public InfoTipTextDelegate InfoTip
        {
            set
            {
                m_infoTipTextDel = value;
            }
        }

        public Dictionary<string, int> LanguageKeywords
        {
            get
            {
                return m_lexicalParser.Keywords;
            }
        }

        public bool AutocompleteEnabled
        {
            get
            {
                return m_autocompleteEnabled;
            }
            set
            {
                m_autocompleteEnabled = value;
            }
        }

        public bool AutocompleteWithBracketsEnabled
        {
            get
            {
                return m_autocompleteWithBracketsEnabled;
            }
            set
            {
                m_autocompleteWithBracketsEnabled = value;
            }
        }

        public bool ShowItemsThatContainCurrentString
        {
            get
            {
                return m_showItemsThatContainCurrentString;
            }
            set
            {
                m_showItemsThatContainCurrentString = value;
            }
        }

        public bool AddIncludePathSymbols
        {
            get
            {
                return m_addIncludePathSymbols;
            }
            set
            {
                m_addIncludePathSymbols = value;
            }
        }

        public bool AddProjectSymbols
        {
            get
            {
                return m_addProjectSymbols;
            }
            set
            {
                m_addProjectSymbols = value;
            }
        }

        public bool HighlightQuickInfoPopups
        {
            get
            {
                return m_highlightQuickInfoPopups;
            }
            set
            {
                m_highlightQuickInfoPopups = value;
            }
        }

        public bool HighlightDisabledCodeBlocks
        {
            get
            {
                return m_highlightDisabledCodeBlocks;
            }
            set
            {
                m_highlightDisabledCodeBlocks = value;
            }
        }

        public bool CompleteWordOnFirstMatch
        {
            get
            {
                return m_completeWordOnFirstMatch;
            }
            set
            {
                m_completeWordOnFirstMatch = value;
            }
        }

        public bool CompleteWordOnExactMatch
        {
            get
            {
                return m_completeWordOnExactMatch;
            }
            set
            {
                m_completeWordOnExactMatch = value;
            }
        }
        #endregion

        #region ISemanticParserServiceProcessor Interface
        /// <summary>
        /// Performs a semantic parsing operation using the data in the <see cref="SemanticParserServiceRequest"/>.
        /// </summary>
        /// <param name="semRequest">A <see cref="SemanticParserServiceRequest"/> containing data about what to parse.</param>
        void ISemanticParserServiceProcessor.Process( SemanticParserServiceRequest semRequest )
        {
            semRequest.SemanticParseData = MergableLexicalParserManager.PerformSemanticParse( this, semRequest.TextBufferReader, semRequest.Filename ) as ISemanticParseData;
            this.UpdateIdentifierCache(semRequest.SemanticParseData as CompilationUnit);
        }
        #endregion

        #region SyntaxLanguage Overrides
        /// <summary>
		/// Resets the <see cref="AutomaticOutliningBehavior"/> property to its default value.
		/// </summary>
		public override void ResetAutomaticOutliningBehavior() 
        {
			this.AutomaticOutliningBehavior = ActiproSoftware.SyntaxEditor.AutomaticOutliningBehavior.SemanticParseDataChange;
		}

        /// <summary>
		/// Indicates whether the <see cref="AutomaticOutliningBehavior"/> property should be persisted.
		/// </summary>
		/// <returns>
		/// <c>true</c> if the property value has changed from its default; otherwise, <c>false</c>.
		/// </returns>
		public override bool ShouldSerializeAutomaticOutliningBehavior() 
        {
			return (this.AutomaticOutliningBehavior != ActiproSoftware.SyntaxEditor.AutomaticOutliningBehavior.SemanticParseDataChange);
		}

		/// <summary>
		/// Creates an <see cref="IToken"/> that represents the end of a document.
		/// </summary>
		/// <param name="startOffset">The start offset of the <see cref="IToken"/>.</param>
		/// <param name="lexicalState">The <see cref="ILexicalState"/> that contains the token.</param>
		/// <returns>An <see cref="IToken"/> that represents the end of a document.</returns>
		public override IToken CreateDocumentEndToken( int startOffset, ILexicalState lexicalState ) 
        {
            return new SanScriptToken( startOffset, 0, LexicalParseFlags.None, null, 
                new LexicalStateAndIDTokenLexicalParseData( lexicalState, SanScriptTokenID.DocumentEnd ) );
		}

		/// <summary>
		/// Creates an <see cref="IToken"/> that represents an invalid range of text.
		/// </summary>
		/// <param name="startOffset">The start offset of the <see cref="IToken"/>.</param>
		/// <param name="length">The length of the <see cref="IToken"/>.</param>
		/// <param name="lexicalState">The <see cref="ILexicalState"/> that contains the token.</param>
		/// <returns>An <see cref="IToken"/> that represents an invalid range of text.</returns>
		public override IToken CreateInvalidToken( int startOffset, int length, ILexicalState lexicalState ) 
        {
            return new SanScriptToken( startOffset, length, LexicalParseFlags.None, null,
                new LexicalStateAndIDTokenLexicalParseData( lexicalState, SanScriptTokenID.Invalid ) );
		}

        /// <summary>
        /// Creates an <see cref="IToken"/> that represents the range of text with the specified lexical parse data.
        /// </summary>
        /// <param name="startOffset">The start offset of the token.</param>
        /// <param name="length">The length of the token.</param>
        /// <param name="lexicalParseFlags">The <see cref="LexicalParseFlags"/> for the token.</param>
        /// <param name="parentToken">The <see cref="IToken"/> that starts the current state scope specified by the <see cref="ITokenLexicalParseData.LexicalState"/> property.</param>
        /// <param name="lexicalParseData">The <see cref="ITokenLexicalParseData"/> that contains lexical parse information about the token.</param>
        /// <returns></returns>
        public override IToken CreateToken( int startOffset, int length, LexicalParseFlags lexicalParseFlags, IToken parentToken, ITokenLexicalParseData lexicalParseData )
        {
            return new SanScriptToken( startOffset, length, lexicalParseFlags, parentToken, lexicalParseData );
        }

		/// <summary>
		/// Resets the <see cref="ErrorDisplayEnabled"/> property to its default value.
		/// </summary>
		public override void ResetErrorDisplayEnabled() 
        {
			this.ErrorDisplayEnabled = true;
		}
		
        /// <summary>
		/// Indicates whether the <see cref="ErrorDisplayEnabled"/> property should be persisted.
		/// </summary>
		/// <returns>
		/// <c>true</c> if the property value has changed from its default; otherwise, <c>false</c>.
		/// </returns>
		public override bool ShouldSerializeErrorDisplayEnabled() 
        {
			return !this.ErrorDisplayEnabled;
		}

		/// <summary>
		/// Returns the <see cref="HighlightingStyle"/> for the specified <see cref="IToken"/>.
		/// </summary>
		/// <param name="token">The <see cref="IToken"/> to examine.</param>
		/// <returns>The <see cref="HighlightingStyle"/> for the specified <see cref="IToken"/>.</returns>
		public override HighlightingStyle GetHighlightingStyle( IToken token ) 
        {
			switch ( token.ID ) 
            {
				case SanScriptTokenID.IntegerValue:
                case SanScriptTokenID.FloatValue:
					return this.HighlightingStyles[c_numberStyleKey];
                
                case SanScriptTokenID.StringValue:
                    return this.HighlightingStyles[c_stringDefaultStyleKey];
                
                case SanScriptTokenID.OpenString:
                case SanScriptTokenID.CloseString:
                    return this.HighlightingStyles[c_stringDelimeterStyleKey];
				
                case SanScriptTokenID.SingleLineComment:
				case SanScriptTokenID.MultiLineComment:
					return this.HighlightingStyles[c_commentDefaultStyleKey];
                
                case SanScriptTokenID.SingleLineCommentStart:
                case SanScriptTokenID.SingleLineCommentEnd:
                case SanScriptTokenID.MultiLineCommentStart:
                case SanScriptTokenID.MultiLineCommentEnd:
                    return this.HighlightingStyles[c_commentDelimeterStyleKey];
                
                case SanScriptTokenID.NativeIdentifier:
                    return this.HighlightingStyles[c_builtInWordStyleKey];
                
                case SanScriptTokenID.TypedefIdentifier:
                case SanScriptTokenID.UserIdentifier:
                    return this.HighlightingStyles[c_userWordStyleKey];
                
                case SanScriptTokenID.StructArgument:
                    return this.HighlightingStyles[c_nativeTypeStyleKey];
                
                case SanScriptTokenID.Disabled:
                    return this.HighlightingStyles[c_preprocessorDisabledStyleKey];
				
                default:
					if ( (token.ID > SanScriptTokenID.KeywordStart) && (token.ID < SanScriptTokenID.KeywordEnd) )
                    {
                        if ( (token.ID > SanScriptTokenID.NativeTypeStart) && (token.ID < SanScriptTokenID.NativeTypeEnd) )
                        {
                            return this.HighlightingStyles[c_nativeTypeStyleKey];
                        }
                        else if ( (token.ID > SanScriptTokenID.WordOperatorStart) && (token.ID < SanScriptTokenID.WordOperatorEnd) )
                        {
                            return this.HighlightingStyles[c_reservedWordOperatorStyleKey];
                        }
                        else
                        {
                            return this.HighlightingStyles[c_reservedWordStyleKey];
                        }
                    }
                    else if ( (token.ID > SanScriptTokenID.OperatorStart) && (token.ID < SanScriptTokenID.OperatorEnd) )
                    {
                        return this.HighlightingStyles[c_operatorStyleKey];
                    }
                    else if ( (token.ID > SanScriptTokenID.PreprocessorStart) && (token.ID < SanScriptTokenID.PreprocessorEnd) )
                    {
                        return this.HighlightingStyles[c_preprocessorMacroStyleKey];
                    }
                    else
                    {
                        switch ( token.LexicalStateID )
                        {
                            case SanScriptLexicalStateID.Default:
                                return this.LexicalStates[c_defaultStateKey].DefaultHighlightingStyle;
                            case SanScriptLexicalStateID.StringState:
                                return this.LexicalStates[c_stringStateKey].DefaultHighlightingStyle;
                            case SanScriptLexicalStateID.CommentState:
                                return this.LexicalStates[c_commentStateKey].DefaultHighlightingStyle;
                            case SanScriptLexicalStateID.MultiLineCommentState:
                                return this.LexicalStates[c_multiLineCommentStateKey].DefaultHighlightingStyle;
                            default:
                                break;
                        }
                    }
                    break;
			}

            return null;
		}
			
		/// <summary>
		/// Gets the token string representation for the specified token ID.
		/// </summary>
		/// <param name="tokenID">The ID of the token to examine.</param>
		/// <returns>The token string representation for the specified token ID.</returns>
		public override string GetTokenString( int tokenID ) 
        {
            return m_lexicalParser.GetTokenString( tokenID );
		}
		
		/// <summary>
		/// Performs an auto-complete if the <see cref="SyntaxEditor"/> context with which the IntelliPrompt member list is initialized causes a single selection.
		/// Otherwise, displays a member list in the <see cref="SyntaxEditor"/>.
		/// </summary>
		/// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> that will display the IntelliPrompt member list.</param>
		/// <returns>
		/// <c>true</c> if an auto-complete occurred or if an IntelliPrompt member list is displayed; otherwise, <c>false</c>.
		/// </returns>
		public override bool IntelliPromptCompleteWord( SyntaxEditor syntaxEditor ) 
        {
            return this.ShowIntelliPromptMemberList( syntaxEditor, true );
		}
		
		/// <summary>
		/// Gets whether IntelliPrompt member list features are supported by the language.
		/// </summary>
		/// <value>
		/// <c>true</c> if IntelliPrompt member list features are supported by the language; otherwise, <c>false</c>.
		/// </value>
		/// <remarks>
		/// If this property is <c>true</c>, then the <see cref="IntelliPromptCompleteWord"/> and <see cref="ShowIntelliPromptMemberList"/> methods may be used.
		/// </remarks>
		public override bool IntelliPromptMemberListSupported 
        {
			get 
            {
				return true;
			}
		}
			
		/// <summary>
		/// Gets whether IntelliPrompt parameter info features are supported by the language.
		/// </summary>
		/// <value>
		/// <c>true</c> if IntelliPrompt parameter info features are supported by the language; otherwise, <c>false</c>.
		/// </value>
		/// <remarks>
		/// If this property is <c>true</c>, then the <see cref="ShowIntelliPromptParameterInfo"/> method may be used.
		/// </remarks>
		public override bool IntelliPromptParameterInfoSupported 
        {
			get 
            {
				return true;
			}
		}

		/// <summary>
		/// Gets whether IntelliPrompt quick info features are supported by the language.
		/// </summary>
		/// <value>
		/// <c>true</c> if IntelliPrompt quick info features are supported by the language; otherwise, <c>false</c>.
		/// </value>
		/// <remarks>
		/// If this property is <c>true</c>, then the <see cref="ShowIntelliPromptQuickInfo"/> method may be used.
		/// </remarks>
		public override bool IntelliPromptQuickInfoSupported 
        {
			get 
            {
				return true;
			}
		}

		/// <summary>
		/// Resets the <see cref="SyntaxLanguage.LineCommentDelimiter"/> property to its default value.
		/// </summary>
		public override void ResetLineCommentDelimiter() 
        {
			this.LineCommentDelimiter = "//";
		}
		
        /// <summary>
		/// Indicates whether the <see cref="SyntaxLanguage.LineCommentDelimiter"/> property should be persisted.
		/// </summary>
		/// <returns>
		/// <c>true</c> if the property value has changed from its default; otherwise, <c>false</c>.
		/// </returns>
		public override bool ShouldSerializeLineCommentDelimiter() 
        {
			return this.LineCommentDelimiter != "//";
		}

        /// <summary>
        /// Gets the <see cref="IMergableLexicalParser"/> that can be used for lexical parsing of the language.
        /// </summary>
        /// <value>The <see cref="IMergableLexicalParser"/> that can be used for lexical parsing of the language.</value>
        protected override IMergableLexicalParser MergableLexicalParser
        {
            get
            {
                return m_lexicalParser;
            }
        }

		/// <summary>
		/// Occurs when a description tip is about to be displayed for the selected IntelliPrompt member list item,
		/// but the item has no description set.
		/// </summary>
		/// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> that will raise the event.</param>
		/// <param name="e">An <c>EventArgs</c> that contains the event data.</param>
		protected override void OnSyntaxEditorIntelliPromptMemberListItemDescriptionRequested( SyntaxEditor syntaxEditor, EventArgs e )
        {
			// Get the selected item
			IntelliPromptMemberListItem item = syntaxEditor.IntelliPrompt.MemberList.SelectedItem;
			if ( (item == null) || (item.Tag == null) )
            {
                return;
            }

            ISet<int> dependenciesHashCodes = SanScriptProjectResolver.GetDependenciesHashCodes(
                syntaxEditor.Document.Filename,
                syntaxEditor.Document.LanguageData as SanScriptProjectResolver);
            item.Description = this.GetQuickInfoForIdentifier( syntaxEditor, item.Tag, true, dependenciesHashCodes);
		}
		
		/// <summary>
		/// Occurs after the parameter index of the IntelliPrompt parameter info is changed while the parameter info is visible.
		/// </summary>
		/// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> that will raise the event.</param>
		/// <param name="e">An <c>EventArgs</c> that contains the event data.</param>
		protected override void OnSyntaxEditorIntelliPromptParameterInfoParameterIndexChanged( SyntaxEditor syntaxEditor, EventArgs e )
        {
            if ( this.UpdateParameterInfoSelectedText( syntaxEditor, true ) )
            {
                syntaxEditor.IntelliPrompt.ParameterInfo.MeasureAndResize( syntaxEditor.IntelliPrompt.ParameterInfo.Bounds.Location );
            }
		}

        protected override void OnSyntaxEditorUserInterfaceUpdate( SyntaxEditor syntaxEditor, EventArgs e )
        {
#if DELAYED_INTELLIPROMPT
            if ( m_checkToShowIntelliPrompt )
            {
                if ( this.ShowIntelliPromptAutocompleteList( syntaxEditor ) )
                {
                    this.CheckToAbortIntelliPromptMemberList( syntaxEditor, false );
                }

                m_checkToShowIntelliPrompt = false;
            }
            else if ( m_checkToAbortIntelliPrompt )
            {
                this.CheckToAbortIntelliPromptMemberList( syntaxEditor, m_checkToAbortIsBackspace );
                m_checkToAbortIntelliPrompt = false;
                m_checkToAbortIsBackspace = false;
            }
#endif

            base.OnSyntaxEditorUserInterfaceUpdate( syntaxEditor, e );
        }

    	/// <summary>
		/// Occurs before a <see cref="SyntaxEditor.KeyTyped"/> event is raised 
		/// for a <see cref="SyntaxEditor"/> that has a <see cref="Document"/> using this language.
		/// </summary>
		/// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> that will raise the event.</param>
		/// <param name="e">An <c>KeyTypedEventArgs</c> that contains the event data.</param>
		protected override void OnSyntaxEditorKeyTyped( SyntaxEditor syntaxEditor, KeyTypedEventArgs e )
        {
            IToken token = syntaxEditor.SelectedView.GetCurrentToken();
            if ( (token == null) || (syntaxEditor.Document.ReadOnly) )
            {
                return;
            }

            if ( syntaxEditor.IntelliPrompt.MemberList.Visible )
            {
                // If the key is a special character not allowed in an identifier, close the list
                if ( e.KeyChar != '_' && syntaxEditor.IntelliPrompt.MemberList.AllowedCharacters.Contains( e.KeyChar ) )
                {
                    syntaxEditor.IntelliPrompt.MemberList.Abort();
                    syntaxEditor.IntelliPrompt.MemberList.Clear();
                }
            }

            if ( token.LexicalStateID == SanScriptLexicalStateID.CommentState )
            {
                if ( (e.KeyChar == '/') && syntaxEditor.Document.SemanticParsingEnabled )
                {
                    IToken previousToken = syntaxEditor.SelectedView.GetPreviousToken();
                    if ( (previousToken != null) && (previousToken.ID != SanScriptTokenID.SingleLineCommentStart) )
                    {
                        this.InsertPurposeComment( syntaxEditor );
                    }
                }
            }
            else if ( token.LexicalStateID == SanScriptLexicalStateID.StringState )
            {
                if ( e.KeyChar == '\"' && m_autocompleteWithBracketsEnabled )
                {
                    IToken previousToken = syntaxEditor.SelectedView.GetPreviousToken();
                    if ( (previousToken != null) && (previousToken.ID == SanScriptTokenID.OpenString) )
                    {
                        syntaxEditor.SelectedView.InsertSurroundingTextSafe( "", "\"" );
                    }
                }
            }
            else if (token.LexicalStateID == SanScriptLexicalStateID.Default)
            {
                switch (e.KeyChar)
                {
                    case '(':
                        {
                            if (m_autocompleteWithBracketsEnabled)
                            {
                                syntaxEditor.SelectedView.InsertSurroundingTextSafe("", ")");
                            }

                            if (syntaxEditor.Document.SemanticParsingEnabled && (syntaxEditor.Caret.Offset > 1))
                            {
                                // Show the parameter info for code
                                this.ShowParameterInfoCore(syntaxEditor, syntaxEditor.Caret.Offset);
                            }
                        }
                        break;
                    case ')':
                        {
                            if (syntaxEditor.Document.SemanticParsingEnabled && (syntaxEditor.Caret.Offset > 1))
                            {
                                // Show the parameter info for the parent context level if there is one
                                syntaxEditor.IntelliPrompt.ParameterInfo.ParameterIndex = -1;
                                this.ShowIntelliPromptParameterInfo(syntaxEditor);
                            }
                        }
                        break;
                    case ',':
                        {
                            if (syntaxEditor.Document.SemanticParsingEnabled && !syntaxEditor.IntelliPrompt.ParameterInfo.Visible
                                && (syntaxEditor.Caret.Offset > 1))
                            {
                                // Show the parameter info for the context level if parameter info is not already displayed
                                this.ShowIntelliPromptParameterInfo(syntaxEditor);
                            }
                        }
                        break;
                    case '.':
                        {
                            if (syntaxEditor.Document.SemanticParsingEnabled && !syntaxEditor.IntelliPrompt.MemberList.Visible
                                && (syntaxEditor.Caret.Offset > 1))
                            {
                                this.ShowIntelliPromptMemberList(syntaxEditor, false);
                            }
                        }
                        break;
                    case '[':
                        {
                            if (m_autocompleteWithBracketsEnabled)
                            {
                                syntaxEditor.SelectedView.InsertSurroundingTextSafe("", "]");
                            }
                        }
                        break;
                    case '<':
                        {
                            if (m_autocompleteWithBracketsEnabled)
                            {
                                IToken previousToken = syntaxEditor.SelectedView.GetPreviousToken();
                                if ((previousToken != null) && (previousToken.ID != SanScriptTokenID.LessThan))
                                {
                                    syntaxEditor.SelectedView.InsertSurroundingTextSafe("", ">>");
                                }
                            }
                        }
                        break;
                    default:
                        if (this.AutocompleteEnabled && (syntaxEditor.Caret.Offset > 1))
                        {
                            if ((Char.IsLetterOrDigit(e.KeyChar) || (e.KeyChar == '_') || (e.KeyChar == '#')))
                            {
#if DELAYED_INTELLIPROMPT
                                if ( syntaxEditor.IntelliPrompt.MemberList.Visible )
                                {
                                    m_checkToShowIntelliPrompt = false;
                                    this.ShowIntelliPromptAutocompleteList( syntaxEditor );
                                }
                                else
                                {
                                    m_checkToShowIntelliPrompt = true;
                                }
#else
                                this.ShowIntelliPromptAutocompleteList(syntaxEditor);
#endif
                            }
                        }
                        break;
                }

                if ((e.KeyData.HasFlag(Keys.Back) || Char.IsLetterOrDigit(e.KeyChar) || e.KeyChar == '_' || e.KeyChar == '#')
                    && syntaxEditor.Caret.Offset > 0 && syntaxEditor.IntelliPrompt.MemberList.Visible)
                {
#if DELAYED_INTELLIPROMPT
                    m_checkToAbortIntelliPrompt = true;
                    m_checkToAbortIsBackspace = e.KeyData == Keys.Back;
#else
                    CheckToAbortIntelliPromptMemberList(syntaxEditor, e.KeyData.HasFlag(Keys.Back));
#endif
                }
                else if (e.KeyData == Keys.Return)
                {
                    this.InsertPurposeCommentLine(syntaxEditor);
                }
            }
		}

		/// <summary>
		/// Occurs when the mouse is hovered over an <see cref="EditorView"/>.
		/// </summary>
		/// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> that will raise the event.</param>
		/// <param name="e">An <c>EditorViewMouseEventArgs</c> that contains the event data.</param>
		protected override void OnSyntaxEditorViewMouseHover( SyntaxEditor syntaxEditor, EditorViewMouseEventArgs e )
        {
            if (e.HitTestResult.Token == null)
            {
                return;
            }

            if (e.ToolTipText != null)
            {
                // This is seen, for example, for collapsed outlining nodes.
                e.ToolTipText = this.BrightenTextForTooltip(e.ToolTipText);
                return;
            }

            int offset = e.HitTestResult.Token.StartOffset;
            e.ToolTipText = this.GetQuickInfo( syntaxEditor, ref offset );
		}

		/// <summary>
		/// Performs automatic outlining over the specified <see cref="TextRange"/> of the <see cref="Document"/>.
		/// </summary>
		/// <param name="document">The <see cref="Document"/> to examine.</param>
		/// <param name="parseTextRange">A <see cref="TextRange"/> indicating the offset range to parse.</param>
		/// <returns>A <see cref="TextRange"/> containing the offset range that was modified by outlining.</returns>
		public override TextRange PerformAutomaticOutlining( Document document, TextRange parseTextRange )
        {
			return new CollapsibleNodeOutliningParser().UpdateOutlining( 
                document, parseTextRange, document.SemanticParseData as CompilationUnit );
		}

        /// <summary>
		/// Semantically parses the specified <see cref="TextRange"/> of the <see cref="Document"/>.
		/// </summary>
		/// <param name="document">The <see cref="Document"/> to examine.</param>
		/// <param name="parseTextRange">A <see cref="TextRange"/> indicating the offset range to parse.</param>
        /// <param name="flags">A <see cref="SemanticParseFlags"/> that indicates semantic parse flags.</param>
		public override void PerformSemanticParse( Document document, TextRange parseTextRange, SemanticParseFlags flags )
        {
            if (!SemanticParserService.IsRunning)
            {
                return;
            }

            string filename = document.Filename.ToLower();

            SemanticParserServiceRequest request = new SemanticParserServiceRequest( SemanticParserServiceRequest.MediumPriority,
                document, parseTextRange, flags, this, document);
            SemanticParserService.Parse( request );
        }

        /// <summary>
        /// Semantically parses the text in the <see cref="MergableLexicalParserManager"/>.
        /// </summary>
        /// <param name="manager">The <see cref="MergableLexicalParserManager"/> that is managing the mergable language and the text to parse.</param>
        /// <returns>An object that contains the results of the semantic parsing operation.</returns>
        protected override object PerformSemanticParse( MergableLexicalParserManager manager )
        {
            SanScriptRecursiveDescentLexicalParser lexicalParser = new SanScriptRecursiveDescentLexicalParser( this, manager );
            lexicalParser.InitializeTokens();
            SanScriptSemanticParser semanticParser = new SanScriptSemanticParser( lexicalParser );
            semanticParser.Parse();

            return semanticParser.CompilationUnit;
        }

        /// <summary>
        /// Sets the <see cref="CollapsedText"/> property for the specified <see cref="OutliningNode"/> prior to the node being collapsed.
        /// </summary>
        /// <param name="node">The <see cref="OutliningNode"/> that is requesting collapsed text.</param>
        public override void SetOutliningNodeCollapsedText( OutliningNode node )
        {
            if (node?.Document?.SemanticParseData == null)
            {
                base.SetOutliningNodeCollapsedText(node);
                return;
            }

            CompilationUnit unit = node.Document.SemanticParseData as CompilationUnit;
            
            // find the AstNode
            AstNode astNode = unit.RootNode.FindNodeRecursive( node.StartOffset ) as AstNode;

            // find the statement it belongs to
            while ( (astNode != null) && !(astNode is Statement) )
            {
                astNode = astNode.ParentNode as AstNode;
            }

            if ( astNode != null )
            {
                // special case, have to manually check the children.
                if ( ((astNode.StartOffset != node.StartOffset)) && (astNode.NodeType == SanScriptNodeType.CaseBlockStatement) )
                {
                    astNode = GetAstNodeInCaseStatementBlock( astNode as CaseBlockStatement, node.StartOffset );
                }

                // if the offsets are the same, use the AstNode's DisplayText
                if ( (astNode != null) && (astNode.StartOffset == node.StartOffset) )
                {
                    if ( astNode.NodeType == SanScriptNodeType.PurposeCommentBlock )
                    {
                        node.CollapsedText = "/// PURPOSE...";
                    }
                    else
                    {
                        string displayText = astNode.DisplayText;
                        int indexOf = displayText.IndexOf( '\n' );
                        if ( indexOf > -1 )
                        {
                            node.CollapsedText = displayText.Substring( 0, indexOf );
                        }
                        else
                        {
                            node.CollapsedText = displayText;
                        }
                    }
                }
                else
                {
                    base.SetOutliningNodeCollapsedText( node );
                }
            }
            else
            {
                base.SetOutliningNodeCollapsedText( node );
            }
        }

		/// <summary>
		/// Displays an IntelliPrompt member list in a <see cref="SyntaxEditor"/> based on the current context.
		/// </summary>
		/// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> that will display the IntelliPrompt member list.</param>
		/// <returns>
		/// <c>true</c> if an IntelliPrompt member list is displayed; otherwise, <c>false</c>.
		/// </returns>
		/// <remarks>
		/// Only call this method if the <see cref="IntelliPromptMemberListSupported"/> property is set to <c>true</c>.
		/// </remarks>
		public override bool ShowIntelliPromptMemberList( SyntaxEditor syntaxEditor ) 
        {
			return this.ShowIntelliPromptMemberList( syntaxEditor, false );
		}
		
		/// <summary>
		/// Displays IntelliPrompt parameter info in a <see cref="SyntaxEditor"/> based on the current context.
		/// </summary>
		/// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> that will display the IntelliPrompt parameter info.</param>
		/// <returns>
		/// <c>true</c> if IntelliPrompt parameter info is displayed; otherwise, <c>false</c>.
		/// </returns>
		/// <remarks>
		/// Only call this method if the <see cref="IntelliPromptParameterInfoSupported"/> property is set to <c>true</c>.
		/// </remarks>
        public override bool ShowIntelliPromptParameterInfo( SyntaxEditor syntaxEditor )
        {
            // Determine the maximum number of characters to look back at
            int minOffset = Math.Max( 0, syntaxEditor.Caret.Offset - 500 );

            // Try and find an open '('
            int targetOffset = -1;
            TextStream stream = syntaxEditor.Document.GetTextStream( syntaxEditor.Caret.Offset );
            stream.GoToCurrentTokenStart();

            bool exitLoop = false;
            int openParenOffset = -1;
            int closeParenOffset = -1;
            while ( stream.Offset > minOffset )
            {
                IToken prevToken = stream.Token;
                switch ( stream.Token.ID )
                {
                    case SanScriptTokenID.CloseParenthesis:
                        closeParenOffset = stream.Token.EndOffset;
                        stream.GoToPreviousMatchingToken( stream.Token );
                        break;
                    case SanScriptTokenID.OpenParenthesis:
                        openParenOffset = stream.Token.StartOffset;
                        targetOffset = stream.Token.EndOffset;
                        exitLoop = true;
                        break;
                    default:
                        stream.ReadTokenReverse();
                        break;
                }
                
                if ( exitLoop || (prevToken == stream.Token) )
                {
                    break;
                }
            }

            if ( (targetOffset != -1) && (syntaxEditor.Caret.Offset >= openParenOffset)
                && ((closeParenOffset == -1) || (syntaxEditor.Caret.Offset <= closeParenOffset)) )
            {
                return this.ShowParameterInfoCore( syntaxEditor, targetOffset );
            }
            else
            {
                return false;
            }
        }
		
		/// <summary>
		/// Displays IntelliPrompt quick info in a <see cref="SyntaxEditor"/> based on the current context.
		/// </summary>
		/// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> that will display the IntelliPrompt quick info.</param>
		/// <returns>
		/// <c>true</c> if IntelliPrompt quick info is displayed; otherwise, <c>false</c>.
		/// </returns>
		/// <remarks>
		/// Only call this method if the <see cref="IntelliPromptQuickInfoSupported"/> property is set to <c>true</c>.
		/// </remarks>
		public override bool ShowIntelliPromptQuickInfo( SyntaxEditor syntaxEditor ) 
        {
			int offset = syntaxEditor.Caret.Offset;

			// Get the info for the context at the caret
			string quickInfo = this.GetQuickInfo( syntaxEditor, ref offset );

			// No info was found... try the offset right before the caret
			if ( (quickInfo == null) && (offset > 0) ) 
            {
				offset = syntaxEditor.Caret.Offset - 1;
				quickInfo = this.GetQuickInfo( syntaxEditor, ref offset );
			}

			// Show the quick info if there is any
			if ( quickInfo != null )
            {
				syntaxEditor.IntelliPrompt.QuickInfo.Show( offset, quickInfo );
				return true;
			}

			return false;
		}

        /// <summary>
        /// Occurs before a <see cref="ViewBracketHighlightingUpdate"/> event is raised for a <see cref="SyntaxEditor"/> 
        /// that has a <see cref="Document"/> using this language. 
        /// </summary>
        /// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> that will raise the event.</param>
        /// <param name="e">An <see cref="EditorViewBracketHighlightingEventArgs"/> that contains the event data.</param>
        protected override void OnSyntaxEditorViewBracketHighlightingUpdate( SyntaxEditor syntaxEditor, 
            EditorViewBracketHighlightingEventArgs e )
        {
            if ( e.BracketToken == null )
            {
                base.OnSyntaxEditorViewBracketHighlightingUpdate( syntaxEditor, e );
                return;
            }

            SanScriptToken token = e.BracketToken as SanScriptToken;
            switch ( token.ID )
            {
                case SanScriptTokenID.IF:
                case SanScriptTokenID.CASE:
                case SanScriptTokenID.DEFAULT:
                case SanScriptTokenID.ELIF:
                case SanScriptTokenID.ELSE:
                case SanScriptTokenID.ENDIF:
                case SanScriptTokenID.FALLTHRU:
                case SanScriptTokenID.BREAK:
                case SanScriptTokenID.EXIT:
                case SanScriptTokenID.RETURN:
                case SanScriptTokenID.BREAKLOOP:
                case SanScriptTokenID.RELOOP:
                case SanScriptTokenID.PoundIf:
                case SanScriptTokenID.PoundEndif:
                    e.MatchingBracketToken = FindMatchingPair( syntaxEditor.Document, token, 
                        syntaxEditor.SelectedView.Selection.StartOffset );
                    break;
                default:
                    base.OnSyntaxEditorViewBracketHighlightingUpdate( syntaxEditor, e );
                    break;
            } 

        }
        #endregion

        #region Intellisense Functions	
		/// <summary>
		/// Returns the quick info for the <see cref="SyntaxEditor"/> at the specified offset.
		/// </summary>
		/// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> to examine.</param>
		/// <param name="offset">The offset to examine.  The offset is updated to the start of the context.</param>
		/// <returns>The quick info for the <see cref="SyntaxEditor"/> at the specified offset.</returns>
		private string GetQuickInfo( SyntaxEditor syntaxEditor, ref int offset ) 
        {
            SemanticParserService.WaitForParse( SemanticParserServiceRequest.GetParseHashKey( syntaxEditor.Document, syntaxEditor.Document ) );

			// Get the identifier at the offset, if any
			TextStream stream = syntaxEditor.Document.GetTextStream( offset );
			if ( !stream.IsAtTokenStart )
			{
                stream.GoToCurrentTokenStart();
            }

			offset = stream.Offset;
			if ( (stream.Token.ID != SanScriptTokenID.Identifier) 
                && (stream.Token.ID != SanScriptTokenID.UserIdentifier) 
                && (stream.Token.ID != SanScriptTokenID.NativeIdentifier)
                && (stream.Token.ID != SanScriptTokenID.TypedefIdentifier) 
                && (stream.Token.ID != SanScriptTokenID.COUNTOF)
                && (stream.Token.ID != SanScriptTokenID.HASH)
                && (stream.Token.ID != SanScriptTokenID.ENUMTOINT)
                && (stream.Token.ID != SanScriptTokenID.INTTOENUM)
                && (stream.Token.ID != SanScriptTokenID.INTTONATIVE)
                && (stream.Token.ID != SanScriptTokenID.NATIVETOINT)
                && (stream.Token.ID != SanScriptTokenID.SIZEOF)
                && (stream.Token.ID != SanScriptTokenID.TIMESTEP )
                && (stream.Token.ID != SanScriptTokenID.THROW) )
            {
                return null;
            }
			
			CompilationUnit compilationUnit = syntaxEditor.Document.SemanticParseData as CompilationUnit;
			
			// Get the containing node
            AstNode containingNode = null;
			if ( compilationUnit != null )
            {
                containingNode = compilationUnit.FindNodeRecursive( stream.Offset ) as AstNode;

                // We get into this situation when we have multiple variable declarations on one line
                if ( containingNode is BlockStatement )
                {
                    AstNodeList childNodes = containingNode.ChildNodes;
                    foreach ( AstNode childNode in childNodes )
                    {
                        bool found = false;

                        if ( childNode is VariableDeclarationStatement )
                        {
                            VariableDeclarationStatement decl = childNode as VariableDeclarationStatement;
                            while ( decl != null )
                            {
                                if ( decl.Contains( offset ) )
                                {
                                    containingNode = decl;
                                    found = true;
                                    break;
                                }

                                decl = decl.SubDeclaration as VariableDeclarationStatement;
                            }
                        }

                        if ( found )
                        {
                            containingNode = childNode.FindNodeRecursive( offset ) as AstNode;
                            break;
                        }
                    }
                }

                while ( (containingNode != null) && !(containingNode is Expression) && !(containingNode is Statement) )
                {
                    containingNode = containingNode.ParentNode as AstNode;
                }
            }

			if ( containingNode != null ) 
            {
                string filename = syntaxEditor.Document.Filename.ToLower();
                string text = stream.TokenText.ToUpper();
                Dictionary<string, int> foundIdentifiers = new Dictionary<string, int>();

                ISet<int> dependenciesHashCodes = SanScriptProjectResolver
                    .GetDependenciesHashCodes(syntaxEditor.Document.Filename, syntaxEditor.Document.LanguageData as SanScriptProjectResolver);

                IToken searchToken = null;
                string displayText = null;
                string markupText = null;

                switch ( containingNode.NodeType )
                {
                    case SanScriptNodeType.CountOfExpression:
                        {
                            if ( containingNode.StartOffset == offset )
                            {
                                displayText = CountOfExpression.DefinitionText;
                                markupText = ColorizeMarkupText( CountOfExpression.DefinitionMarkupText,
                                    filename, dependenciesHashCodes, foundIdentifiers );
                            }
                            else
                            {
                                CountOfExpression expr = containingNode as CountOfExpression;
                                Expression name = expr.Argument;
                                if ( name != null )
                                {
                                    searchToken = syntaxEditor.Document.Tokens.GetTokenAtOffset( name.StartOffset );
                                }
                            }
                        }
                        break;
                    case SanScriptNodeType.EnumToIntExpression:
                        displayText = EnumToIntExpression.DefinitionText;
                        markupText = ColorizeMarkupText( EnumToIntExpression.DefinitionMarkupText,
                            filename, dependenciesHashCodes, foundIdentifiers );
                        break;
                    case SanScriptNodeType.IntToEnumExpression:
                        {
                            if ( containingNode.StartOffset == offset )
                            {
                                displayText = IntToEnumExpression.DefinitionText;
                                markupText = ColorizeMarkupText( IntToEnumExpression.DefinitionMarkupText,
                                    filename, dependenciesHashCodes, foundIdentifiers );
                            }
                            else
                            {
                                IntToEnumExpression expr = containingNode as IntToEnumExpression;
                                Identifier name = expr.EnumType;
                                if ( name != null )
                                {
                                    searchToken = syntaxEditor.Document.Tokens.GetTokenAtOffset( name.StartOffset );
                                }
                            }
                        }
                        break;
                    case SanScriptNodeType.IntToNativeExpression:
                        {
                            if ( containingNode.StartOffset == offset )
                            {
                                displayText = IntToNativeExpression.DefinitionText;
                                markupText = ColorizeMarkupText( IntToNativeExpression.DefinitionMarkupText,
                                    filename, dependenciesHashCodes, foundIdentifiers );
                            }
                            else
                            {
                                IntToNativeExpression expr = containingNode as IntToNativeExpression;
                                Identifier name = expr.NativeType;
                                if ( name != null )
                                {
                                    searchToken = syntaxEditor.Document.Tokens.GetTokenAtOffset( name.StartOffset );
                                }
                            }
                        }
                        break;
                    case SanScriptNodeType.NativeToIntExpression:
                        displayText = NativeToIntExpression.DefinitionText;
                        markupText = ColorizeMarkupText( NativeToIntExpression.DefinitionMarkupText,
                            filename, dependenciesHashCodes, foundIdentifiers );
                        break;
                    case SanScriptNodeType.SizeOfExpression:
                        displayText = SizeOfExpression.DefinitionText;
                        markupText = ColorizeMarkupText( SizeOfExpression.DefinitionMarkupText,
                            filename, dependenciesHashCodes, foundIdentifiers );
                        break;
                    case SanScriptNodeType.HashExpression:
                        displayText = HashExpression.DefinitionText;
                        markupText = ColorizeMarkupText(HashExpression.DefinitionMarkupText,
                            filename, dependenciesHashCodes, foundIdentifiers);
                        break;
                    case SanScriptNodeType.ThrowStatement:
                        displayText = ThrowStatement.DefinitionText;
                        markupText = ColorizeMarkupText( ThrowStatement.DefinitionMarkupText,
                            filename, dependenciesHashCodes, foundIdentifiers );
                        break;
                    case SanScriptNodeType.TimestepExpression:
                        displayText = TimestepExpression.DefinitionText;
                        markupText = ColorizeMarkupText( TimestepExpression.DefinitionMarkupText,
                            filename, dependenciesHashCodes, foundIdentifiers );
                        break;
                    case SanScriptNodeType.ArrayAccessExpression:
                    case SanScriptNodeType.StructMemberAccessExpression:
                    case SanScriptNodeType.SubroutineAccessExpression:
                    case SanScriptNodeType.VariableExpression:
                    case SanScriptNodeType.PreprocessorMacroExpression:
                        {
                            IdentifierExpression expr = containingNode as IdentifierExpression;
                            Identifier name = expr.Name;
                            if ( name != null )
                            {
                                searchToken = syntaxEditor.Document.Tokens.GetTokenAtOffset( name.StartOffset );
                            }
                        }
                        break;
                    case SanScriptNodeType.DefaultStateStatement:
                    case SanScriptNodeType.GotoStateStatement:
                    case SanScriptNodeType.CallStatement:
                    case SanScriptNodeType.SubroutineAccessStatement:     
                    case SanScriptNodeType.PreprocessorMacroStatement:
                        {
                            IdentifierStatement stmt = containingNode as IdentifierStatement;
                            Identifier name = stmt.Name;
                            if ( name != null )
                            {
                                searchToken = syntaxEditor.Document.Tokens.GetTokenAtOffset( name.StartOffset );
                            }
                        }
                        break;
                    case SanScriptNodeType.VariableArrayDeclarationStatement:
                    case SanScriptNodeType.VariableNonArrayDeclarationStatement:
                        {
                            VariableDeclarationStatement stmt = containingNode as VariableDeclarationStatement;
                            Identifier name = stmt.Type;
                            if ( name != null )
                            {
                                if ( name.Contains( offset ) )
                                {
                                    searchToken = syntaxEditor.Document.Tokens.GetTokenAtOffset( name.StartOffset );
                                }
                            }
                        }
                        break;
                    case SanScriptNodeType.TypedefFuncDeclarationStatement:
                    case SanScriptNodeType.TypedefProcDeclarationStatement:
                        {
                            TypedefSubroutineDeclarationStatement stmt = containingNode as TypedefSubroutineDeclarationStatement;
                            Identifier name = stmt.Name;
                            if ( name != null )
                            {
                                if ( name.Contains( offset ) )
                                {
                                    searchToken = syntaxEditor.Document.Tokens.GetTokenAtOffset( name.StartOffset );
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }

                if ( searchToken != null )
                {
                    object foundObject = null;
                    string foundFilename = null;
                    if ( FindDefinition( syntaxEditor.Document, searchToken, true, false,
                        dependenciesHashCodes, ref foundObject, ref foundFilename) )
                    {
                        markupText = GetQuickInfoForIdentifier( syntaxEditor, foundObject, true, dependenciesHashCodes);

                        if ( foundObject is AstNode )
                        {
                            displayText = ((AstNode)foundObject).DisplayText;
                        }
                        else if ( foundObject is SanScriptIdentifier )
                        {
                            if ( (foundObject is SanScriptKeyword) || (foundObject is SanScriptWordOperator) )
                            {
                                return null;
                            }

                            displayText = ((SanScriptIdentifier)foundObject).DisplayText;
                        }
                    }
                }

                if ( (m_infoTipTextDel != null) && (displayText != null) )
                {
                    m_infoTipTextDel( displayText, null );
                }

                return markupText;
			}

			return null;
		}

        /// <summary>
        /// Gets the formatted text to display in quick info for the specified <see cref="FunctionDeclaration"/>, bolding the 
        /// selected parameter.
        /// </summary>
        /// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> that is requesting the quick info.</param>
        /// <param name="obj">The <see cref="AstNode"/> or <see cref="SanScriptIdentifier"/> representing a function to be examined.</param>
        /// <param name="parameterIndex">The index of the selected parameter.</param>
        /// <param name="dependenciesHashCodes">Hash codes of the filename's dependencies</param>
        /// <returns>The formatted text to display in quick info for the specified <see cref="FunctionDeclaration"/>.</returns>
        private string GetQuickInfoForSubroutine(SyntaxEditor syntaxEditor, object obj, int parameterIndex, ISet<int> dependenciesHashCodes)
        {
            StringBuilder result = new StringBuilder();
            string markupText = null;
            string[] argsMarkupText = null;

            if ( obj is AstNode )
            {
                AstNode node = obj as AstNode;
                
                if ( m_reflectionIconsEnabled && (node.ImageIndex != -1) )
                {
                    ActiproSoftware.Products.SyntaxEditor.IconResource imageIndex = (ActiproSoftware.Products.SyntaxEditor.IconResource)node.ImageIndex;
                    string imageName = Enum.GetName( typeof( ActiproSoftware.Products.SyntaxEditor.IconResource ), imageIndex );

                    result.Append( "<img src=\"resource:" );
                    result.Append( imageName );
                    result.Append( "\" align=\"absbottom\"/> " );
                }
                
                switch ( node.NodeType )
                {
                    case SanScriptNodeType.CountOfExpression:
                        markupText = CountOfExpression.DefinitionMarkupText;
                        argsMarkupText = CountOfExpression.DefinitionArgumentsMarkupText;
                        break;
                    case SanScriptNodeType.EnumToIntExpression:
                        markupText = EnumToIntExpression.DefinitionMarkupText;
                        argsMarkupText = EnumToIntExpression.DefinitionArgumentsMarkupText;
                        break;
                    case SanScriptNodeType.IntToEnumExpression:
                        markupText = IntToEnumExpression.DefinitionMarkupText;
                        argsMarkupText = IntToEnumExpression.DefinitionArgumentsMarkupText;
                        break;
                    case SanScriptNodeType.IntToNativeExpression:
                        markupText = IntToNativeExpression.DefinitionMarkupText;
                        argsMarkupText = IntToNativeExpression.DefinitionArgumentsMarkupText;
                        break;
                    case SanScriptNodeType.NativeToIntExpression:
                        markupText = NativeToIntExpression.DefinitionMarkupText;
                        argsMarkupText = NativeToIntExpression.DefinitionArgumentsMarkupText;
                        break;
                    case SanScriptNodeType.SizeOfExpression:
                        markupText = SizeOfExpression.DefinitionMarkupText;
                        argsMarkupText = SizeOfExpression.DefinitionArgumentsMarkupText;
                        break;
                    case SanScriptNodeType.HashExpression:
                        markupText = HashExpression.DefinitionMarkupText;
                        argsMarkupText = HashExpression.DefinitionArgumentsMarkupText;
                        break;
                    case SanScriptNodeType.ThrowStatement:
                        markupText = ThrowStatement.DefinitionMarkupText;
                        argsMarkupText = ThrowStatement.DefinitionArgumentsMarkupText;
                        break;
                    case SanScriptNodeType.TimestepExpression:
                        markupText = TimestepExpression.DefinitionMarkupText;
                        break;
                    case SanScriptNodeType.EventDefinitionBlockStatement:
                    case SanScriptNodeType.FuncDefinitionBlockStatement:
                    case SanScriptNodeType.ProcDefinitionBlockStatement:
                        markupText = node.MarkupText;
                        argsMarkupText = ((SubroutineDefinitionBlockStatement)node).ArgumentsMarkupText;
                        break;
                    case SanScriptNodeType.NativeEventDeclarationStatement:
                    case SanScriptNodeType.NativeFuncDeclarationStatement:
                    case SanScriptNodeType.NativeProcDeclarationStatement:
                        markupText = node.MarkupText;
                        argsMarkupText = ((NativeSubroutineDeclarationStatement)node).ArgumentsMarkupText;
                        break;
                    case SanScriptNodeType.TypedefFuncDeclarationStatement:
                    case SanScriptNodeType.TypedefProcDeclarationStatement:
                        markupText = node.MarkupText;
                        argsMarkupText = ((TypedefSubroutineDeclarationStatement)node).ArgumentsMarkupText;
                        break;
                    default:
                        break;
                }
            }
            else if ( (obj is SanScriptNativeSubroutine) || (obj is SanScriptSubroutine) || (obj is SanScriptTypedefSubroutine) )
            {
                SanScriptIdentifier id = obj as SanScriptIdentifier;
                
                if ( m_reflectionIconsEnabled && (id.ImageIndex != -1) )
                {
                    ActiproSoftware.Products.SyntaxEditor.IconResource imageIndex = (ActiproSoftware.Products.SyntaxEditor.IconResource)id.ImageIndex;
                    string imageName = Enum.GetName( typeof( ActiproSoftware.Products.SyntaxEditor.IconResource ), imageIndex );

                    result.Append( "<img src=\"resource:" );
                    result.Append( imageName );
                    result.Append( "\" align=\"absbottom\"/> " );
                }

                markupText = id.MarkupText;
                List<string> argsMarkup = (obj is SanScriptNativeSubroutine) ? ((SanScriptNativeSubroutine)id).ArgumentsMarkupText
                    : ((obj is SanScriptSubroutine) ? ((SanScriptSubroutine)id).ArgumentsMarkupText : ((SanScriptTypedefSubroutine)id).ArgumentsMarkupText);

                argsMarkupText = new string[argsMarkup.Count];
                argsMarkup.CopyTo( argsMarkupText );
            }

            if ( markupText != null )
            {
                if ( (argsMarkupText != null) && (argsMarkupText.Length > 0) 
                    && (parameterIndex > -1) && (parameterIndex < argsMarkupText.Length) )
                {
                    StringBuilder text = new StringBuilder();
                    text.Append( "<b>" );
                    text.Append( argsMarkupText[parameterIndex] );
                    text.Append( "</b>" );

                    markupText = markupText.Replace( argsMarkupText[parameterIndex], text.ToString() );
                }

                Dictionary<string, int> foundIdentifiers = new Dictionary<string, int>();
                markupText = ColorizeMarkupText( markupText, syntaxEditor.Document.Filename.ToLower(), 
                    dependenciesHashCodes, foundIdentifiers );

                result.Append( markupText );
                return result.ToString();
            }

            return null;
		}

        /// <summary>
		/// Gets the formatted text to display in quick info for the specified keyword.
		/// </summary>
		/// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> that is requesting the quick info.</param>
		/// <param name="keyword">The keyword to examine.</param>
        /// <param name="addMembers">When <c>true</c> and the object represents the definition of a Structure or Enum, its members will be added to the text.</param>
        /// <param name="dependenciesHashCodes">Hash codes of this file's dependencies.</param>
		/// <returns>The formatted text to display in quick info for the specified keyword.</returns>
		private string GetQuickInfoForIdentifier( SyntaxEditor syntaxEditor, object tag, bool addMembers, ISet<int> dependenciesHashCodes) 
        {
            StringBuilder result = new StringBuilder();

            Dictionary<string, int> foundIdentifiers = new Dictionary<string, int>();

            if ( tag is AstNode )
            {
                AstNode node = tag as AstNode;

                if ( m_reflectionIconsEnabled && (node.ImageIndex != -1) )
                {
                    ActiproSoftware.Products.SyntaxEditor.IconResource imageIndex = (ActiproSoftware.Products.SyntaxEditor.IconResource)node.ImageIndex;
                    string imageName = Enum.GetName( typeof( ActiproSoftware.Products.SyntaxEditor.IconResource ), imageIndex );

                    result.Append( "<img src=\"resource:" );
                    result.Append( imageName );
                    result.Append( "\" align=\"absbottom\"/> " );
                }

                result.Append( ColorizeMarkupText( node.MarkupText, syntaxEditor.Document.Filename.ToLower(), 
                    dependenciesHashCodes, foundIdentifiers ) );

                if ( addMembers )
                {
                    IAstNodeList members = null;
                    if ( node.NodeType == SanScriptNodeType.StructDefinitionBlockStatement )
                    {
                        members = ((StructDefinitionBlockStatement)node).Statements;
                    }
                    else if (node.NodeType == SanScriptNodeType.EnumDefinitionBlockStatement)
                    {
                        members = ((EnumDefinitionBlockStatement)node).Statements;
                    }
                    else if (node.NodeType == SanScriptNodeType.HashEnumDefinitionBlockStatement)
                    {
                        members = ((HashEnumDefinitionBlockStatement)node).Statements;
                    }
                    else if (node.NodeType == SanScriptNodeType.StrictEnumDefinitionBlockStatement)
                    {
                        members = ((StrictEnumDefinitionBlockStatement)node).Statements;
                    }

                    if ( members != null )
                    {
                        if ( members.Count > 0 )
                        {
                            result.Append( "<br/>" );

                            for ( int i = 0; (i < members.Count) && (i < 50); ++i )
                            {
                                result.Append( "<br/>&nbsp;&nbsp;&nbsp;&nbsp;" );
                                result.Append(GetQuickInfoForIdentifier(syntaxEditor, members[i], false, dependenciesHashCodes));
                            }

                            if ( members.Count >= 50 )
                            {
                                result.Append( "<br/>&nbsp;&nbsp;&nbsp;&nbsp;..." );
                            }
                        }
                    }
                }
            }
            else if ( tag is SanScriptIdentifier )
            {
                if ( (tag is SanScriptKeyword) || (tag is SanScriptWordOperator) )
                {
                    return string.Empty;
                }

                SanScriptIdentifier id = tag as SanScriptIdentifier;

                if ( m_reflectionIconsEnabled && (id.ImageIndex != -1) )
                {
                    ActiproSoftware.Products.SyntaxEditor.IconResource imageIndex = (ActiproSoftware.Products.SyntaxEditor.IconResource)id.ImageIndex;
                    string imageName = Enum.GetName( typeof( ActiproSoftware.Products.SyntaxEditor.IconResource ), imageIndex );

                    result.Append( "<img src=\"resource:" );
                    result.Append( imageName );
                    result.Append( "\" align=\"absbottom\"/> " );
                }

                result.Append( ColorizeMarkupText( id.MarkupText, syntaxEditor.Document.Filename.ToLower(), 
                    dependenciesHashCodes, foundIdentifiers ) );

                if ( addMembers )
                {
                    if ( id is SanScriptStructure )
                    {
                        List<SanScriptStructMember> members = ((SanScriptStructure)id).Members;
                        if ( members.Count > 0 )
                        {
                            result.Append( "<br/>" );

                            for ( int i = 0; (i < members.Count) && (i < 50); ++i )
                            {
                                result.Append( "<br/>&nbsp;&nbsp;&nbsp;&nbsp;" );
                                result.Append(GetQuickInfoForIdentifier(syntaxEditor, members[i], false, dependenciesHashCodes));
                            }

                            if ( members.Count >= 50 )
                            {
                                result.Append( "<br/>&nbsp;&nbsp;&nbsp;&nbsp;..." );
                            }
                        }

                    }
                    else if ( id is SanScriptEnum )
                    {
                        List<SanScriptEnumeration> members = ((SanScriptEnum)id).Enums;
                        if ( members.Count > 0 )
                        {
                            result.Append( "<br/>" );

                            for ( int i = 0; (i < members.Count) && (i < 50); ++i )
                            {
                                result.Append( "<br/>&nbsp;&nbsp;&nbsp;&nbsp;" );
                                result.Append(GetQuickInfoForIdentifier(syntaxEditor, members[i], false, dependenciesHashCodes));
                            }

                            if ( members.Count >= 50 )
                            {
                                result.Append( "<br/>&nbsp;&nbsp;&nbsp;&nbsp;..." );
                            }
                        }
                    }
                }
            }

            return result.ToString();
        }
		
		/// <summary>
		/// Provides the core functionality to show an IntelliPrompt member list based on the current context in a <see cref="SyntaxEditor"/>.
		/// </summary>
		/// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> that will display the IntelliPrompt member list.</param>
		/// <param name="completeWord">Whether to complete the word.</param>
		/// <returns>
		/// <c>true</c> if an auto-complete occurred or if an IntelliPrompt member list is displayed; otherwise, <c>false</c>.
		/// </returns>
		private bool ShowIntelliPromptMemberList( SyntaxEditor syntaxEditor, bool completeWord ) 
        {            
			// Get the target text range
            TextStream stream = syntaxEditor.Document.GetTextStream( Math.Max( 0, syntaxEditor.Caret.Offset - 1 ) );
            TextRange targetTextRange = stream.Token.TextRange;
            if ( !stream.IsAtTokenStart )
            {
                stream.GoToCurrentTokenStart();
                targetTextRange = stream.Token.TextRange;
            }

			// build the list
			if ( BuildIntelliPromptMemberList( syntaxEditor, stream.Token ) )
            {
                // show the list
                if ( targetTextRange.IsDeleted || (stream.TokenText.Trim() == string.Empty) || (stream.TokenText == ".") )
                {
                    syntaxEditor.IntelliPrompt.MemberList.Show();
                }
                else if ( completeWord )
                {
                    syntaxEditor.IntelliPrompt.MemberList.CompleteWord( targetTextRange.StartOffset, targetTextRange.Length );
                }
                else
                {
                    syntaxEditor.IntelliPrompt.MemberList.Show( targetTextRange.StartOffset, targetTextRange.Length );
                }

				return true;
			}

			return false;
		}

        /// <summary>
        /// Shows an autocomplete member list which will be a list of all language keywords, user words and native words
        /// </summary>
        /// <param name="SyntaxEditor"></param>
        /// <returns>True if shown</returns>
        private bool ShowIntelliPromptAutocompleteList( SyntaxEditor syntaxEditor )
        {
            // get the target token
            TextStream stream = syntaxEditor.Document.GetTextStream( Math.Max( 0, syntaxEditor.Caret.Offset - 1 ) );
            TextRange targetTextRange = stream.Token.TextRange;
            if ( !stream.IsAtTokenStart )
            {
                stream.GoToCurrentTokenStart();
                targetTextRange = stream.Token.TextRange;
            }

            if ( syntaxEditor.IntelliPrompt.MemberList.Visible )
            {
                if (CullIntelliPromptMemberList(syntaxEditor, stream.Token)
                || BuildIntelliPromptMemberList( syntaxEditor, stream.Token ) )
                {
                    syntaxEditor.IntelliPrompt.MemberList.Show(targetTextRange.StartOffset, targetTextRange.Length);
                    return true;
                }
            }
            else if (targetTextRange.Length > 2 && BuildIntelliPromptMemberList( syntaxEditor, stream.Token ))
            {
                syntaxEditor.IntelliPrompt.MemberList.Show( targetTextRange.StartOffset, targetTextRange.Length );
                return true;
            }

            return false;
        }
        
        /// <summary>
        /// Builds an <see cref="IntelliPromptMemberList"/> full of the autocomplete keywords or struct members for the current context.
        /// </summary>
        /// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> to build the list for.</param>
        /// <param name="token">The <see cref="IToken"/> that provides the context.</param>
        /// <returns><c>true</c> if the MemberList should be shown, otherwise <c>false</c>.</returns>
        private bool BuildIntelliPromptMemberList( SyntaxEditor syntaxEditor, IToken token )
        {
            if ( (token.ID == SanScriptTokenID.IntegerValue) || (token.ID == SanScriptTokenID.FloatValue) )
            {
                return false;
            }

            string text = syntaxEditor.Document.GetTokenText(token).Trim();

            // Store all the items in a copy of the list. The Intellisense member list is too slow to hold all the items.
            this.m_fullIntelliPromptMemberList.Clear();

            try
            {
                // see if we are in a struct member list
                IToken structNameToken = null;
                bool structArray = false;
                int tokenIndex = syntaxEditor.Document.Tokens.IndexOf(token.StartOffset);
                if (tokenIndex > 1)
                {
                    bool foundPeriod = false;

                    --tokenIndex;
                    if (token.ID != SanScriptTokenID.Period)
                    {
                        IToken previousToken = syntaxEditor.Document.Tokens[tokenIndex];
                        if (previousToken.ID == SanScriptTokenID.Period)
                        {
                            --tokenIndex;
                            foundPeriod = true;
                        }
                    }
                    else
                    {
                        foundPeriod = true;
                    }

                    if (foundPeriod)
                    {
                        // search backwards for the name of the structure, skipping past any array accessors
                        TextStream stream = syntaxEditor.Document.GetTextStream(syntaxEditor.Document.Tokens[tokenIndex].StartOffset);

                        int minOffset = Math.Max(0, syntaxEditor.Caret.Offset - 500);

                        bool exitLoop = false;
                        while (stream.Offset > minOffset)
                        {
                            IToken prevToken = stream.Token;
                            switch (stream.Token.ID)
                            {
                                case SanScriptTokenID.CloseBracket:
                                    stream.GoToPreviousMatchingToken(stream.Token);
                                    structArray = true;
                                    break;
                                case SanScriptTokenID.Identifier:
                                case SanScriptTokenID.UserIdentifier:
                                case SanScriptTokenID.NativeIdentifier:
                                case SanScriptTokenID.TypedefIdentifier:
                                    structNameToken = stream.Token;

                                    if (text == ".")
                                    {
                                        // clear this so we'll display all members
                                        text = string.Empty;
                                    }

                                    exitLoop = true;
                                    break;
                                default:
                                    stream.ReadTokenReverse();
                                    break;
                            }

                            if (exitLoop || (prevToken == stream.Token))
                            {
                                break;
                            }
                        }
                    }
                }

                ISet<int> dependenciesHashCodes = SanScriptProjectResolver
                    .GetDependenciesHashCodes(syntaxEditor.Document.Filename, syntaxEditor.Document.LanguageData as SanScriptProjectResolver);

                // regular autocomplete list
                if (structNameToken == null)
                {
                    // First get a list of identifiers within this block statement and ancestors if any. This way we include in-scope local variables
                    // and argument list variables.
                    IEnumerable<VariableDeclarationStatement> localDeclarations = this.FindDeclaredVariablesContaining(syntaxEditor.Document, token, text, this.m_showItemsThatContainCurrentString);

                    this.m_fullIntelliPromptMemberList.AddRange(
                        localDeclarations.Select(declaration => new IntelliPromptMemberListItem(declaration.Name.Text, declaration.ImageIndex, declaration)));

                    // Now find external identifiers from the cache.
                    IEnumerable<SanScriptIdentifier> identifiers
                        = SanScriptProjectResolver.FindIdentifiersAccessibleFromFileStartingWith(syntaxEditor.Document.Filename, text,
                            m_showItemsThatContainCurrentString, dependenciesHashCodes);

                    this.m_fullIntelliPromptMemberList.AddRange(identifiers.Select(
                        id =>
                        {
                            // special case hack
                            IntelliPromptMemberListItem item;
                            if (id.Name.Equals(SanScriptLexicalParser.EnumToIntDatatypeName, StringComparison.OrdinalIgnoreCase))
                            {
                                item = new IntelliPromptMemberListItem(SanScriptLexicalParser.EnumToIntDatatypeName, id.ImageIndex, id);
                                item.AutoCompletePreText = SanScriptLexicalParser.EnumToIntDatatypePreText;
                            }
                            else
                            {
                                item = new IntelliPromptMemberListItem(id.Name, id.ImageIndex, id);
                            }

                            if (m_autocompleteWithBracketsEnabled)
                            {
                                if ((id is SanScriptSubroutine) || (id is SanScriptNativeSubroutine))
                                {
                                    item.AutoCompletePreText += "(";
                                    item.AutoCompletePostText = ")";
                                }
                                else if (id is SanScriptKeyword)
                                {
                                    int matchingTokenID = SanScriptToken.GetMatchingTokenID(id.TokenID);
                                    if ((matchingTokenID != SanScriptTokenID.Invalid) && SanScriptToken.GetIsPairedEnd(matchingTokenID))
                                    {
                                        item.AutoCompletePreText += " ";
                                        item.AutoCompletePostText = BuildTextForColumn(
                                            "\n", SanScriptTokenID.GetTokenKey(matchingTokenID),
                                            syntaxEditor.Caret.CharacterColumn - 1);
                                    }
                                    else if (id.TokenID == SanScriptTokenID.FORWARD)
                                    {
                                        item.AutoCompletePreText += " ENUM ";
                                    }
                                }
                            }

                            return item;
                        }));
                }
                // list members
                else if (syntaxEditor.Document.SemanticParseData != null)
                {
                    string filename = syntaxEditor.Document.Filename.ToLower();
                    object foundObject = null;
                    string foundFilename = null;

                    // search for the declaration
                    if (FindDefinition(syntaxEditor.Document, structNameToken, true, false, dependenciesHashCodes,
                        ref foundObject, ref foundFilename))
                    {
                        if (foundObject is VariableDeclarationStatement)
                        {
                            // make sure we're not *in* the declaration right now
                            VariableDeclarationStatement decl = foundObject as VariableDeclarationStatement;
                            if (decl.IntersectsWith(token.StartOffset - 1))
                            {
                                return false;
                            }

                            // validate struct member access
                            if (((decl.NodeType == SanScriptNodeType.VariableArrayDeclarationStatement) && !structArray)
                                || ((decl.NodeType == SanScriptNodeType.VariableNonArrayDeclarationStatement) && structArray))
                            {
                                return false;
                            }

                            // now search for the definition of the structure
                            TextStream stream = syntaxEditor.Document.GetTextStream(decl.Type.StartOffset);
                            if (!FindDefinition(syntaxEditor.Document, stream.Token, true, false, dependenciesHashCodes,
                                ref foundObject, ref foundFilename))
                            {
                                return false;
                            }
                        }
                        else if ((foundObject is SanScriptStaticVariable) || (foundObject is SanScriptStructMember))
                        {
                            // validate struct member access
                            bool isArray = ((SanScriptIdentifier)foundObject).DisplayText.EndsWith("]");
                            if ((isArray && !structArray) || (!isArray && structArray))
                            {
                                return false;
                            }

                            string dataType;
                            if (foundObject is SanScriptStaticVariable)
                            {
                                dataType = ((SanScriptStaticVariable)foundObject).Type;
                            }
                            else
                            {
                                dataType = ((SanScriptStructMember)foundObject).Type;
                            }

                            // now search for the definition of the structure
                            foundObject = SanScriptProjectResolver.FindIdentifierAccessibleFromFileExact(foundFilename, dataType, dependenciesHashCodes);
                            if (foundObject == null)
                            {
                                return false;
                            }
                        }

                        if (foundObject is StructDefinitionBlockStatement)
                        {
                            IAstNodeList members = ((StructDefinitionBlockStatement)foundObject).Statements;
                            foreach (AstNode member in members)
                            {
                                if (!(member is VariableDeclarationStatement))
                                {
                                    continue;
                                }

                                VariableDeclarationStatement var = member as VariableDeclarationStatement;
                                while (var != null)
                                {
                                    Identifier name = var.Name;
                                    if (name != null)
                                    {
                                        this.m_fullIntelliPromptMemberList.Add(new IntelliPromptMemberListItem(name.Text, member.ImageIndex, member));
                                    }

                                    var = var.SubDeclaration as VariableDeclarationStatement;
                                }
                            }
                        }
                        else if (foundObject is SanScriptStructure)
                        {
                            Dictionary<string, int> foundIdentifiers = new Dictionary<string, int>();

                            SanScriptStructure structure = foundObject as SanScriptStructure;
                            List<SanScriptStructMember> members = structure.Members;

                            this.m_fullIntelliPromptMemberList.AddRange(members.Select(
                                member => new IntelliPromptMemberListItem(member.Name, member.ImageIndex, member))
                                .OrderBy(li => li.Text));
                        }
                    }
                }

                // check if we have completed the word ourselves
                if (this.m_fullIntelliPromptMemberList.Count == 1)
                {
                    IntelliPromptMemberListItem item = this.m_fullIntelliPromptMemberList[0];
                    if (item.Text.Equals(text, StringComparison.OrdinalIgnoreCase))
                    {
                        this.m_fullIntelliPromptMemberList.Clear();
                        return false;
                    }
                }

                return this.m_fullIntelliPromptMemberList.Count > 0;
            }
            finally
            {
                IntelliPromptMemberList memberList = syntaxEditor.IntelliPrompt.MemberList;
                if (memberList.ImageList == null)
                {
                    memberList.ImageList = SyntaxEditor.ReflectionImageList;
                }

                memberList.Sorted = false;
                memberList.CaseSensitiveMatch = false;
                memberList.MatchBasedOnItemPreText = false;
                memberList.SetListSafe(this.m_fullIntelliPromptMemberList);
            }
        }

        /// <summary>
        /// Removes MemberList items that don't match the <see cref="IToken"/> text.
        /// </summary>
        /// <param name="syntaxEditor">The <see cref=""/> that contains the <see cref="IntelliPromptMemberList"/>.</param>
        /// <param name="token">The <see cref="IToken"/> of the search text.</param>
        /// <returns><c>true</c> if the MemberList should be shown, otherwise <c>false</c>.</returns>
        private bool CullIntelliPromptMemberList( SyntaxEditor syntaxEditor, IToken token )
        {
            if ( (token.ID == SanScriptTokenID.IntegerValue) || (token.ID == SanScriptTokenID.FloatValue) )
            {
                return false;
            }

            IntelliPromptMemberList memberList = syntaxEditor.IntelliPrompt.MemberList;

            if (memberList.ImageList == null)
            {
                memberList.ImageList = SyntaxEditor.ReflectionImageList;
            }

            string text = syntaxEditor.Document.GetTokenText(token).Trim();

            this.m_fullIntelliPromptMemberList.RemoveAll(
                item =>
                {
                    if (item.Text.StartsWith(text, StringComparison.OrdinalIgnoreCase))
                    {
                        return false;
                    }

                    if (!this.ShowItemsThatContainCurrentString)
                    {
                        return true;
                    }

                    if (text.Contains('_') && item.Text.IndexOf(text, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        return false;
                    }

                    string matchWordText = text.Trim( new char[] { '_' } );
                    string[] split = item.Text.Split( new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries );
                    for ( int j = 1; j < split.Length; ++j )
                    {
                        if ( split[j].StartsWith( matchWordText, StringComparison.OrdinalIgnoreCase ) )
                        {
                            return false;
                        }
                    }

                    return true;
                });

            memberList.SetListSafe(this.m_fullIntelliPromptMemberList);

            return syntaxEditor.IntelliPrompt.MemberList.Count > 0;
        }

		/// <summary>
		/// Provides the core functionality to shows parameter info based on the current context in a <see cref="SyntaxEditor"/>.
		/// </summary>
		/// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> that will display the parameter info.</param>
		/// <param name="offset">The offset to examine.</param>
		/// <returns>
		/// <c>true</c> if parameter info is displayed; otherwise, <c>false</c>.
		/// </returns>
		private bool ShowParameterInfoCore( SyntaxEditor syntaxEditor, int offset ) 
        {
            // Initialize the parameter info
			syntaxEditor.IntelliPrompt.ParameterInfo.Hide();
			syntaxEditor.IntelliPrompt.ParameterInfo.Info.Clear();
			syntaxEditor.IntelliPrompt.ParameterInfo.SelectedIndex = 0;

            if ( syntaxEditor.Document.SemanticParseData == null )
            {
                return false;
            }
			
			// Get the identifier at the offset, if any
			TextStream stream = syntaxEditor.Document.GetTextStream( offset );
			if ( !stream.IsAtTokenStart )
            {
                stream.GoToCurrentTokenStart();
            }
			
            stream.GoToPreviousToken();			
            if ( stream.Token.ID != SanScriptTokenID.OpenParenthesis )
			{
                return false;
            }
			
            stream.GoToPreviousToken();

            ISet<int> dependenciesHashCodes = SanScriptProjectResolver
                .GetDependenciesHashCodes(syntaxEditor.Document.Filename, syntaxEditor.Document.LanguageData as SanScriptProjectResolver);

            // find the AstNode or SanScriptIdentifier definition
            object foundObject = null;
            switch ( stream.Token.ID )
            {
                case SanScriptTokenID.COUNTOF:
                    foundObject = new CountOfExpression();  // HACK
                    break;
                case SanScriptTokenID.HASH:
                    foundObject = new HashExpression(); // HACK
                    break;
                case SanScriptTokenID.ENUMTOINT:
                    foundObject = new EnumToIntExpression();  // HACK
                    break;
                case SanScriptTokenID.INTTOENUM:
                    foundObject = new IntToEnumExpression();  // HACK
                    break;
                case SanScriptTokenID.INTTONATIVE:
                    foundObject = new IntToNativeExpression();  // HACK
                    break;
                case SanScriptTokenID.NATIVETOINT:
                    foundObject = new NativeToIntExpression();  // HACK
                    break;
                case SanScriptTokenID.SIZEOF:
                    foundObject = new SizeOfExpression();  // HACK
                    break;
                case SanScriptTokenID.THROW:
                    foundObject = new ThrowStatement();  // HACK
                    break;
                case SanScriptTokenID.TIMESTEP:
                    foundObject = new TimestepExpression();  // HACK
                    break;

                case SanScriptTokenID.Identifier:
                case SanScriptTokenID.UserIdentifier:
                case SanScriptTokenID.NativeIdentifier:
                case SanScriptTokenID.TypedefIdentifier:
                    {
                        string foundFilename = null;
                        if ( !FindDefinition( syntaxEditor.Document, stream.Token, true, false, dependenciesHashCodes,
                            ref foundObject, ref foundFilename ) )
                        {
                            return false;
                        }
                    }
                    break;
                default:
                    return false;
            }

            // find out if we have parameters
            bool haveParameters = false;
            if ( foundObject is AstNode )
            {
                AstNode node = foundObject as AstNode;
                switch ( node.NodeType )
                {
                    case SanScriptNodeType.CountOfExpression:
                    case SanScriptNodeType.EnumToIntExpression:
                    case SanScriptNodeType.IntToEnumExpression:
                    case SanScriptNodeType.IntToNativeExpression:
                    case SanScriptNodeType.NativeToIntExpression:
                    case SanScriptNodeType.SizeOfExpression:
                    case SanScriptNodeType.HashExpression:
                    case SanScriptNodeType.ThrowStatement:
                        haveParameters = true;
                        break;
                    case SanScriptNodeType.EventDefinitionBlockStatement:
                    case SanScriptNodeType.FuncDefinitionBlockStatement:
                    case SanScriptNodeType.ProcDefinitionBlockStatement:
                        {
                            IAstNodeList args = ((SubroutineDefinitionBlockStatement)foundObject).Arguments;
                            haveParameters = (args != null) && (args.Count > 0);
                        }
                        break;
                    case SanScriptNodeType.NativeEventDeclarationStatement:
                    case SanScriptNodeType.NativeFuncDeclarationStatement:
                    case SanScriptNodeType.NativeProcDeclarationStatement:
                        {
                            IAstNodeList args = ((NativeSubroutineDeclarationStatement)foundObject).Arguments;
                            haveParameters = (args != null) && (args.Count > 0);
                        }
                        break;
                    case SanScriptNodeType.TypedefFuncDeclarationStatement:
                    case SanScriptNodeType.TypedefProcDeclarationStatement:
                        {
                            IAstNodeList args = ((TypedefSubroutineDeclarationStatement)foundObject).Arguments;
                            haveParameters = (args != null) && (args.Count > 0);
                        }
                        break;
                    case SanScriptNodeType.VariableArrayDeclarationStatement:
                    case SanScriptNodeType.VariableNonArrayDeclarationStatement:
                        {
                            // check for a TYPEDEF
                            VariableDeclarationStatement varStmt = node as VariableDeclarationStatement;
                            if ( varStmt.Type != null )
                            {
                                TextStream varStream = syntaxEditor.Document.GetTextStream( varStmt.Type.StartOffset );
                                if ( !varStream.IsAtTokenStart )
                                {
                                    varStream.GoToCurrentTokenStart();
                                }

                                string foundFilename = null;
                                if ( !FindDefinition( syntaxEditor.Document, varStream.Token, true, false, dependenciesHashCodes,
                                    ref foundObject, ref foundFilename ) )
                                {
                                    return false;
                                }

                                if ( foundObject is AstNode )
                                {
                                    AstNode foundNode = foundObject as AstNode;
                                    if ( (foundNode.NodeType == SanScriptNodeType.TypedefFuncDeclarationStatement) || 
                                        (foundNode.NodeType == SanScriptNodeType.TypedefProcDeclarationStatement) )
                                    {
                                        IAstNodeList args = ((TypedefSubroutineDeclarationStatement)foundObject).Arguments;
                                        haveParameters = (args != null) && (args.Count > 0);
                                    }
                                }
                                else if ( foundObject is SanScriptTypedefSubroutine )
                                {
                                    SanScriptTypedefSubroutine subroutine = foundObject as SanScriptTypedefSubroutine;
                                    haveParameters = subroutine.ArgumentsMarkupText.Count > 0;
                                }
                            }
                        }
                        break;
                    default:
                        return false;
                }
            }
            else if ( foundObject is SanScriptSubroutine )
            {
                SanScriptSubroutine subroutine = foundObject as SanScriptSubroutine;
                haveParameters = subroutine.ArgumentsMarkupText.Count > 0;
            }
            else if ( foundObject is SanScriptNativeSubroutine )
            {
                SanScriptNativeSubroutine subroutine = foundObject as SanScriptNativeSubroutine;
                haveParameters = subroutine.ArgumentsMarkupText.Count > 0;
            }
            else if ( foundObject is SanScriptTypedefSubroutine )
            {
                SanScriptTypedefSubroutine subroutine = foundObject as SanScriptTypedefSubroutine;
                haveParameters = subroutine.ArgumentsMarkupText.Count > 0;
            }

            if ( !haveParameters ) 
            {
                return false;
            }

			// Determine the parameter text range
			TextRange parameterTextRange = new TextRange( offset );
			stream.Offset = offset - 1;
            if ( stream.GoToNextMatchingToken( stream.Token ) )
            {
                parameterTextRange = new TextRange( offset, stream.Offset + 1 );
            }
            else
            {
                parameterTextRange = new TextRange( offset, offset + 1 );
            }

			// Configure the parameter info
			syntaxEditor.IntelliPrompt.ParameterInfo.ValidTextRange = parameterTextRange;
			syntaxEditor.IntelliPrompt.ParameterInfo.CloseDelimiterCharacter = ')';

			// Update the parameter index
			syntaxEditor.IntelliPrompt.ParameterInfo.UpdateParameterIndex();

            // Add an item
            syntaxEditor.IntelliPrompt.ParameterInfo.Info.Add(this.GetQuickInfoForSubroutine(syntaxEditor, foundObject,
                syntaxEditor.IntelliPrompt.ParameterInfo.ParameterIndex, dependenciesHashCodes));

            // Store the function declaration in the context
            syntaxEditor.IntelliPrompt.ParameterInfo.Context = foundObject;

			// Show the parameter info
			syntaxEditor.IntelliPrompt.ParameterInfo.Show( offset );

            if ( m_infoTipTextDel != null )
            {
                if ( foundObject is AstNode )
                {
                    m_infoTipTextDel( ((AstNode)foundObject).DisplayText, null );
                }
                else if ( foundObject is SanScriptIdentifier )
                {
                    m_infoTipTextDel( ((SanScriptIdentifier)foundObject).DisplayText, null );
                }
            }

			return true;
		}
				
		/// <summary>
		/// Updates the parameter info's selected item's text.
		/// </summary>
		/// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> to examine.</param>
		/// <param name="force">Whether to force the update.  Otherwise the update only occurs if the text is not yet initialized.</param>
		/// <returns>
		/// <c>true</c> if an update occurred; otherwise, <c>false</c>.
		/// </returns>
		private bool UpdateParameterInfoSelectedText( SyntaxEditor syntaxEditor, bool force ) 
        {
            // Skip if the parameter info is not visible or there is no context available...
            if (!syntaxEditor.IntelliPrompt.ParameterInfo.Visible || syntaxEditor.IntelliPrompt.ParameterInfo.Context == null)
            {
                return false;
            }

            // Skip if the parameter info item doesn't need markup...
            if (!force && syntaxEditor.IntelliPrompt.ParameterInfo.Info[syntaxEditor.IntelliPrompt.ParameterInfo.SelectedIndex] != null)
            {
                return false;
            }

            // Update the parameter info item
            syntaxEditor.IntelliPrompt.ParameterInfo.Info[syntaxEditor.IntelliPrompt.ParameterInfo.SelectedIndex] =
                this.GetQuickInfoForSubroutine( syntaxEditor, syntaxEditor.IntelliPrompt.ParameterInfo.Context, 
                    syntaxEditor.IntelliPrompt.ParameterInfo.ParameterIndex,
                    SanScriptProjectResolver.GetDependenciesHashCodes(syntaxEditor.Document.Filename, syntaxEditor.Document.LanguageData as SanScriptProjectResolver));
            return true;

        }

        /// <summary>
        /// Converts the <see cref="Color"/> into a xml-approved string format.  If State.HighlightQuickInfoPopups, will always
        /// return Black.
        /// </summary>
        /// <param name="c">The <see cref="Color"/>.</param>
        /// <returns>The <c>string</c> representation of the color.</returns>
        private string GetColorSpanForQuickInfo( Color c )
        {
            c = this.HighlightQuickInfoPopups ? BrightenColorForTooltip(c) : Color.Black;
            return $"<span style=\"color: {ToHex(c)};\">";
        }

        /// <summary>
        /// Brighten HTML color tags in the provided text if they match the hightlighting style settings, so they can be seen in the yellow tooltip.
        /// </summary>
        /// <param name="text">The text to brighten.</param>
        /// <returns>The brightened text.</returns>
        private string BrightenTextForTooltip(string text)
        {
            foreach (HighlightingStyle style in this.HighlightingStyles)
            {
                // Replace any dark highlighting style colors in the text with brightened counterparts.
                string hex = ToHex(style.ForeColor);
                string brightHex = ToHex(BrightenColorForTooltip(style.ForeColor));
                if (!hex.Equals(brightHex))
                {
                    text = text.Replace(hex, brightHex);
                }
            }

            return text;
        }

        private static string ToHex(Color color)
        {
            return string.Format(
                "#{0:X2}{1:X2}{2:X2}",
                color.R,
                color.G,
                color.B);
        }

        /// <summary>
        /// Make sure the theme we have is compatible with the (unchangeable) yellow background. If it's too light (user using a dark theme),
        /// darken the color.
        /// </summary>
        /// <param name="c">The original color.</param>
        /// <returns>The lightened color.</returns>
        private static Color BrightenColorForTooltip(Color c)
        {
            if (c.GetBrightness() > 0.3f)
            {
                // Darken a bit
                float darkenFactor = 1.0f - (c.GetBrightness() - 0.3f);
                c = Color.FromArgb(c.A,
                    (int)(c.R * darkenFactor), (int)(c.G * darkenFactor), (int)(c.B * darkenFactor));
            }

            return c;
        }

        /// <summary>
        /// Changes the color macro style tags into actual color style tags.
        /// </summary>
        /// <param name="text">The marked up text containing color macro style tags.</param>
        /// <param name="filename">The filename where it originates</param>
        /// <param name="dependenciesHashCodes">Hash codes of this file's dependencies.</param>
        /// <param name="foundIdentifiers">A <see cref="Dictionary"/> of identifier IDs to speed up the process.</param>
        /// <returns>The converted <c>string</c>.</returns>
        private string ColorizeMarkupText( string text, string filename, ISet<int> dependenciesHashCodes, 
            Dictionary<string, int> foundIdentifiers )
        {
            string defaultStyle = this.GetColorSpanForQuickInfo( this.HighlightingStyles[c_defaultStyleKey].ForeColor );
            string reservedWordStyle = this.GetColorSpanForQuickInfo( this.HighlightingStyles[c_reservedWordStyleKey].ForeColor );
            string nativeTypeStyle = this.GetColorSpanForQuickInfo( this.HighlightingStyles[c_nativeTypeStyleKey].ForeColor );
            string operatorStyle = this.GetColorSpanForQuickInfo( this.HighlightingStyles[c_operatorStyleKey].ForeColor );
            string wordOperatorStyle = this.GetColorSpanForQuickInfo( this.HighlightingStyles[c_reservedWordOperatorStyleKey].ForeColor );
            string numberStyle = this.GetColorSpanForQuickInfo( this.HighlightingStyles[c_numberStyleKey].ForeColor );
            string stringDelimStyle = this.GetColorSpanForQuickInfo( this.HighlightingStyles[c_stringDelimeterStyleKey].ForeColor );
            string stringStyle = this.GetColorSpanForQuickInfo( this.HighlightingStyles[c_stringDefaultStyleKey].ForeColor );
            string commentDelimStyle = this.GetColorSpanForQuickInfo( this.HighlightingStyles[c_commentDelimeterStyleKey].ForeColor );
            string commentStyle = this.GetColorSpanForQuickInfo( this.HighlightingStyles[c_commentDefaultStyleKey].ForeColor );
            string nativeStyle = this.GetColorSpanForQuickInfo( this.HighlightingStyles[c_builtInWordStyleKey].ForeColor );
            string userStyle = this.GetColorSpanForQuickInfo( this.HighlightingStyles[c_userWordStyleKey].ForeColor );
            string preprocessorStyle = this.GetColorSpanForQuickInfo( this.HighlightingStyles[c_preprocessorMacroStyleKey].ForeColor );

            // decode the end tags
            text = text.Replace( "</c>", "</span>" );

            // decode the colors
            text = text.Replace( "<c=text>", defaultStyle );
            text = text.Replace( "<c=reserved>", reservedWordStyle );
            text = text.Replace( "<c=nativetype>", nativeTypeStyle );
            text = text.Replace( "<c=operator>", operatorStyle );
            text = text.Replace( "<c=wordoperator>", wordOperatorStyle );
            text = text.Replace( "<c=number>", numberStyle );
            text = text.Replace( "<c=stringdelim>", stringDelimStyle );
            text = text.Replace( "<c=string>", stringStyle );
            text = text.Replace( "<c=commentdelim>", commentDelimStyle );
            text = text.Replace( "<c=comment>", commentStyle );
            text = text.Replace( "<c=native>", nativeStyle );
            text = text.Replace( "<c=user>", userStyle );
            text = text.Replace( "<c=macro>", preprocessorStyle );

            // decode the locals
            string[] split = text.Split( new string[] { "<c=local>" }, StringSplitOptions.None );
            if ( split.Length > 0 )
            {
                StringBuilder builder = new StringBuilder( split[0] );
                for ( int i = 1; i < split.Length; ++i )
                {
                    bool appended = false;

                    int nameEnd = split[i].IndexOf( '<' );
                    if ( nameEnd > 0 )
                    {
                        string name = split[i].Substring( 0, nameEnd ).Trim().ToUpper();

                        if ( name != string.Empty )
                        {
                            Identifier.DeclarationType declareType
                                = this.FindIdentifierDeclareType( filename, name, dependenciesHashCodes, foundIdentifiers );
                            switch ( declareType )
                            {
                                case Identifier.DeclarationType.Local:
                                    builder.Append( defaultStyle );
                                    break;
                                case Identifier.DeclarationType.Native:
                                    builder.Append( nativeStyle );
                                    break;
                                case Identifier.DeclarationType.NativeType:
                                    builder.Append( nativeTypeStyle );
                                    break;
                                case Identifier.DeclarationType.User:
                                    builder.Append( userStyle );
                                    break;
                            }

                            appended = true;
                        }
                    }
                    
                    if ( !appended )
                    {
                        // apply default color
                        builder.Append( defaultStyle );
                    }

                    builder.Append( split[i] );
                }

                text = builder.ToString();
            }

            return text;
        }

        /// <summary>
        /// Checks if the previous token is whitespace, and aborts the <see cref="IntelliPromptMemberList"/>.
        /// </summary>
        /// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> to examine.</param>
        private void CheckToAbortIntelliPromptMemberList( SyntaxEditor syntaxEditor, bool isBackSpace )
        {
            // Get the target text range
            TextStream stream = syntaxEditor.Document.GetTextStream( Math.Max( 0, syntaxEditor.Caret.Offset - 1 ) );
            TextRange targetTextRange = stream.Token.TextRange;
            if ( !stream.IsAtTokenStart )
            {
                stream.GoToCurrentTokenStart();
                targetTextRange = stream.Token.TextRange;
            }

            if ( isBackSpace )
            {
                if (stream.Token.ID == SanScriptTokenID.Whitespace || stream.Token.ID == SanScriptTokenID.LineTerminator || targetTextRange.Length <= 2)
                {
                    syntaxEditor.IntelliPrompt.MemberList.Abort();
                }
                else if (BuildIntelliPromptMemberList(syntaxEditor, stream.Token))
                {
                    syntaxEditor.IntelliPrompt.MemberList.Show( stream.Token.StartOffset, stream.Token.Length );
                }
            }
            else
            {
                IntelliPromptMemberListItem selectedItem = syntaxEditor.IntelliPrompt.MemberList.SelectedItem;
                if (selectedItem == null)
                {
                    return;
                }

                if (syntaxEditor.IntelliPrompt.MemberList.Count == 1 &&
                     (this.CompleteWordOnFirstMatch ||
                     (this.CompleteWordOnExactMatch && selectedItem.Text.Equals(stream.TokenText, StringComparison.OrdinalIgnoreCase))))
                {
                    syntaxEditor.IntelliPrompt.MemberList.Abort();
                    syntaxEditor.IntelliPrompt.MemberList.CompleteWord( stream.Token.StartOffset, stream.Token.Length );
                }
            }
        }
        #endregion

        #region Event Handlers
        public void editor_DocumentTextChanged( object sender, DocumentModificationEventArgs e )
        {
            SyntaxEditor syntaxEditor = sender as SyntaxEditor;
            if ( syntaxEditor == null )
            {
                return;
            }

            if ( (e.Modification.Type == DocumentModificationType.AutoComplete) && m_autocompleteWithBracketsEnabled
                && e.Document.SemanticParsingEnabled && (syntaxEditor.Caret.Offset > 1) 
                && e.Modification.InsertedText.EndsWith( ")" ) )
            {
				// Show the parameter info for code
                this.ShowParameterInfoCore( syntaxEditor, syntaxEditor.Caret.Offset - 1 );
            }
        }

        private void editorDocument_SemanticParseDataWorker(object sender, DoWorkEventArgs e)
        {
            Document doc = e.Argument as Document;
            if (doc?.SemanticParseData == null)
            {
                return;
            }

            // Fixup the identifier tokens so they get colorized properly
            this.FixupIdentifierTokenIDs(doc);

            // These lines will force a refresh so the new colors appear.
            doc.InvalidatePaint();
        }

        public void editorDocument_SemanticParseDataChanged( object sender, EventArgs e )
        {
            BackgroundWorker parserWorker = new BackgroundWorker();
            parserWorker.DoWork += this.editorDocument_SemanticParseDataWorker;
            parserWorker.RunWorkerAsync(sender);
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Searches the <see cref="SyntaxEditor"/> and its dependencies for the definition of whatever <see cref="Identifier"/> 
        /// is found at the <see cref="Caret"/>'s position.
        /// </summary>
        /// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> that contains the <see cref="Document"/> to start the search from.</param>
        /// <param name="foundObject">Return by reference the <see cref="AstNode"/> or <see cref="SanScriptIdentifier"/> found.</param>
        /// <param name="foundFilename">Return by reference the file in which the definition was found.</param>
        /// <returns><c>true</c> if found, otherwise <c>false</c>.</returns>
        public bool FindDefinition( SyntaxEditor syntaxEditor, ref object foundObject, ref string foundFilename )
        {
            TextStream stream = syntaxEditor.Document.GetTextStream( Math.Max( 0, syntaxEditor.Caret.Offset - 1 ) );

            try
            {
                if ( !stream.IsAtTokenStart )
                {
                    stream.GoToCurrentTokenStart();
                }
            }
            catch ( Exception e )
            {
                ApplicationSettings.Log.ToolException(e, "Exception going to current token start");
            }

            return FindDefinition( syntaxEditor.Document, stream.Token, true, false,
                SanScriptProjectResolver.GetDependenciesHashCodes(syntaxEditor.Document.Filename, syntaxEditor.Document.LanguageData as SanScriptProjectResolver),
                ref foundObject, ref foundFilename );
        }
        #endregion

        #region Private Functions
                /// <summary>
        /// Search the <see cref="CompilationUnit"/> for valid keywords and stores them in the <see cref="SanScriptIdentifierCache"/>.
        /// </summary>
        /// <param name="unit">The <see cref="CompilationUnit"/> to search.</param>
        private void UpdateIdentifierCache(CompilationUnit unit)
        {
            if (unit == null)
            {
                return;
            }

            SanScriptIdentifierFile idFile = SanScriptProjectResolver.IdentifierCache.GetIdentifierFileByName( unit.SourceKey );
            string sourceDirectory = Path.GetDirectoryName(unit.SourceKey);
            List<string> usings = new List<string>();

            idFile.Lock.EnterWriteLock();
            try
            {
                idFile.Reset();
                Dictionary<string, int> foundIdentifiers = new Dictionary<string, int>();

                UpdateIdentifierCache(unit.Constants, idFile, foundIdentifiers, false, false, false);
                UpdateIdentifierCache(unit.DataTypes, idFile, foundIdentifiers, false, false, false);
                UpdateIdentifierCache(unit.GlobalsSection, idFile, foundIdentifiers, false, false, false);
                UpdateIdentifierCache(unit.States, idFile, foundIdentifiers, false, false, true);
                UpdateIdentifierCache(unit.StaticVariables, idFile, foundIdentifiers, false, false, false);
                UpdateIdentifierCache(unit.Subroutines, idFile, foundIdentifiers, false, false, false);

                foreach (AstNode node in unit.UsingStatements)
                {
                    if (node.NodeType != SanScriptNodeType.UsingStatement)
                    {
                        continue;
                    }

                    string fullPathName = this.GetUsingFilename(node as UsingStatement, sourceDirectory);
                    if (fullPathName == null)
                    {
                        continue;
                    }

                    SanScriptProjectResolver.IdentifierCache.AddUsingFile(idFile, fullPathName);
                    usings.Add(fullPathName);
                }

                foreach (AstNode node in unit.GlobalsSection)
                {
                    if (node.NodeType != SanScriptNodeType.GlobalsDefinitionBlockStatement)
                    {
                        continue;
                    }

                    GlobalsDefinitionBlockStatement globalsBlock = node as GlobalsDefinitionBlockStatement;
                    foreach (AstNode stmt in globalsBlock.Statements)
                    {
                        if (stmt.NodeType != SanScriptNodeType.UsingStatement)
                        {
                            continue;
                        }

                        string fullPathName = this.GetUsingFilename(stmt as UsingStatement, sourceDirectory);
                        if (fullPathName == null)
                        {
                            continue;
                        }

                        SanScriptProjectResolver.IdentifierCache.AddUsingFile(idFile, fullPathName);
                        usings.Add(fullPathName);
                    }
                }

                foreach (AstNode node in unit.OtherStatements)
                {
                    if (node.NodeType == SanScriptNodeType.GlobalsStatement)
                    {
                        idFile.ContainsGlobals = true;
                        break;
                    }
                }

                SanScriptProjectResolver.IdentifierCache.FinishUpdatingIdentifierFile(idFile);
                SanScriptProjectResolver.IdentifierCache.SerializeFile(idFile, unit.SourceKey);
            }
            finally
            {
                idFile.Lock.ExitWriteLock();
            }

            string[] usingFilenames = usings.Select(u => SanScriptProjectResolver.ParserPlugin.GetFullPathName(u, sourceDirectory)).ToArray();
            string idFilePath = SanScriptProjectResolver.IdentifierCache.GetFilename(idFile.FilenameHashCode);
            SanScriptProjectResolver.ParseCoordinator.RequestIdentifiersForFiles(idFilePath, usingFilenames);
        }

        /// <summary>
        /// Searches the <see cref="IAstNodeList"/> for important identifiers and adds them to the <see cref="SanScriptIdentifierCache"/>
        /// as a <see cref="SanScriptIdentifier"/>.
        /// </summary>
        /// <param name="nodeList">The <see cref="IAstNodeList"/> to search.</param>
        /// <param name="idFile">The ID file to update.</param>
        /// <param name="projectResolver">The <see cref="SanScriptProjectResolver"/> that manages the identifiers.</param>
        /// <param name="foundIdentifiers">A <see cref="Dictionary"/> of identifiers that have already been located to speed up the search.</param>
        /// <param name="inGlobals"><c>true</c> if we're looking through a GLOBALS <see cref="IAstNodeList"/>.</param>
        /// <param name="inStates"><c>true</c> if we're looking through a STATE's <see cref="IAstNodeList"/>. </param>
        /// <param name="recurseStates"><c>true</c> if we should recursively search a STATE's <see cref="IAstNodeList"/>.</param>
        private void UpdateIdentifierCache( IAstNodeList nodeList, SanScriptIdentifierFile idFile,
            Dictionary<string, int> foundIdentifiers, bool inGlobals, bool inStates, bool recurseStates )
        {
            int filenameHashCode = idFile.FilenameHashCode;

            foreach ( AstNode node in nodeList )
            {
                if ( inGlobals && !CompilationUnit.AllowedInGlobalsSection( node ) )
                {
                    continue;
                }

                if ( inStates && (node.NodeType != SanScriptNodeType.StateDefinitionBlockStatement)
                    && (node.NodeType != SanScriptNodeType.EventDefinitionBlockStatement) )
                {
                    continue;
                }

                switch ( node.NodeType )
                {
                    case SanScriptNodeType.ConstDefinitionStatement:
                        {
                            ConstDefinitionStatement stmt = node as ConstDefinitionStatement;
                            Identifier name = stmt.Name;
                            if ( name != null )
                            {
                                SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, new SanScriptConstant( name.Text,
                                    filenameHashCode, stmt.StartOffset, stmt.DisplayText, stmt.MarkupText ) );
                            }
                        }
                        break;
                    case SanScriptNodeType.NativeTypeDeclarationStatement:
                        {
                            NativeTypeDeclarationStatement stmt = node as NativeTypeDeclarationStatement;
                            Identifier name = stmt.Name;
                            if ( name != null )
                            {
                                SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, new SanScriptNativeType( name.Text,
                                    filenameHashCode, stmt.StartOffset, stmt.DisplayText, stmt.MarkupText ) );
                            }
                        }
                        break;
                    case SanScriptNodeType.TypedefFuncDeclarationStatement:
                    case SanScriptNodeType.TypedefProcDeclarationStatement:
                        {
                            TypedefSubroutineDeclarationStatement stmt = node as TypedefSubroutineDeclarationStatement;
                            Identifier name = stmt.Name;
                            if ( name != null )
                            {
                                SanScriptTypedefSubroutine subroutine = new SanScriptTypedefSubroutine( name.Text,
                                    filenameHashCode, stmt.StartOffset, stmt.DisplayText, stmt.MarkupText );                                

                                IAstNodeList args = stmt.Arguments;
                                foreach ( AstNode arg in args )
                                {
                                    if ( arg is VariableDeclarationStatement )
                                    {
                                        VariableDeclarationStatement var = arg as VariableDeclarationStatement;
                                        subroutine.ArgumentsMarkupText.Add( var.MarkupText );
                                    }
                                }

                                SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, subroutine );
                            }
                        }
                        break;
                    case SanScriptNodeType.ForwardEnumDeclarationStatement:
                        {
                            ForwardEnumDeclarationStatement stmt = node as ForwardEnumDeclarationStatement;
                            Identifier name = stmt.Name;
                            if ( name != null )
                            {
                                SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, new SanScriptEnum( name.Text,
                                    filenameHashCode, stmt.StartOffset, stmt.DisplayText, stmt.MarkupText ) );
                            }
                        }
                        break;
                    case SanScriptNodeType.ForwardStructDeclarationStatement:
                        {
                            ForwardStructDeclarationStatement stmt = node as ForwardStructDeclarationStatement;
                            Identifier name = stmt.Name;
                            if (name != null)
                            {
                                SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, new SanScriptEnum(name.Text,
                                    filenameHashCode, stmt.StartOffset, stmt.DisplayText, stmt.MarkupText));
                            }
                        }
                        break;
                    case SanScriptNodeType.StructDefinitionBlockStatement:
                        {
                            StructDefinitionBlockStatement stmt = node as StructDefinitionBlockStatement;
                            Identifier name = stmt.Name;
                            if ( name != null )
                            {
                                SanScriptStructure structure = new SanScriptStructure( name.Text, filenameHashCode,
                                    stmt.StartOffset, stmt.DisplayText, stmt.MarkupText );

                                IAstNodeList members = stmt.Statements;
                                foreach ( AstNode member in members )
                                {
                                    if ( member is VariableDeclarationStatement )
                                    {
                                        VariableDeclarationStatement var = member as VariableDeclarationStatement;
                                        while ( var != null )
                                        {
                                            Identifier varName = var.Name;
                                            if ( var.Name != null )
                                            {
                                                Identifier dataType = var.Type;
                                                structure.Members.Add( new SanScriptStructMember( varName.Text,
                                                    dataType == null ? null : dataType.Text, structure,
                                                    filenameHashCode, var.StartOffset, var.DisplayText, var.MarkupText ) );
                                            }

                                            var = var.SubDeclaration as VariableDeclarationStatement;
                                        }
                                    }
                                }

                                SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, structure );
                            }
                        }
                        break;
                    case SanScriptNodeType.EnumDefinitionBlockStatement:
                        {
                            EnumDefinitionBlockStatement stmt = node as EnumDefinitionBlockStatement;
                            Identifier name = stmt.Name;
                            if (name != null)
                            {
                                SanScriptEnum enumType = new SanScriptEnum(name.Text, filenameHashCode,
                                    stmt.StartOffset, stmt.DisplayText, stmt.MarkupText);

                                IAstNodeList enums = stmt.Statements;
                                foreach (AstNode enumeration in enums)
                                {
                                    if (enumeration is VariableDeclarationStatement)
                                    {
                                        VariableDeclarationStatement var = enumeration as VariableDeclarationStatement;
                                        Identifier varName = var.Name;
                                        if (var.Name != null)
                                        {
                                            SanScriptEnumeration e = new SanScriptEnumeration(varName.Text, enumType, filenameHashCode,
                                                var.StartOffset, var.DisplayText, var.MarkupText);

                                            enumType.Enums.Add(e);
                                            SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, e);
                                        }
                                    }
                                }

                                SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, enumType);
                            }
                        }
                        break;
                    case SanScriptNodeType.HashEnumDefinitionBlockStatement:
                        {
                            HashEnumDefinitionBlockStatement stmt = node as HashEnumDefinitionBlockStatement;
                            Identifier name = stmt.Name;
                            if (name != null)
                            {
                                SanScriptEnum enumType = new SanScriptEnum(name.Text, filenameHashCode,
                                    stmt.StartOffset, stmt.DisplayText, stmt.MarkupText);

                                IAstNodeList enums = stmt.Statements;
                                foreach (AstNode enumeration in enums)
                                {
                                    if (enumeration is VariableDeclarationStatement)
                                    {
                                        VariableDeclarationStatement var = enumeration as VariableDeclarationStatement;
                                        Identifier varName = var.Name;
                                        if (var.Name != null)
                                        {
                                            SanScriptEnumeration e = new SanScriptEnumeration(varName.Text, enumType, filenameHashCode,
                                                var.StartOffset, var.DisplayText, var.MarkupText);

                                            enumType.Enums.Add(e);
                                            SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, e);
                                        }
                                    }
                                }

                                SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, enumType);
                            }
                        }
                        break;
                    case SanScriptNodeType.StrictEnumDefinitionBlockStatement:
                        {
                            StrictEnumDefinitionBlockStatement stmt = node as StrictEnumDefinitionBlockStatement;
                            Identifier name = stmt.Name;
                            if (name != null)
                            {
                                SanScriptEnum enumType = new SanScriptEnum(name.Text, filenameHashCode,
                                    stmt.StartOffset, stmt.DisplayText, stmt.MarkupText);

                                IAstNodeList enums = stmt.Statements;
                                foreach (AstNode enumeration in enums)
                                {
                                    if (enumeration is VariableDeclarationStatement)
                                    {
                                        VariableDeclarationStatement var = enumeration as VariableDeclarationStatement;
                                        Identifier varName = var.Name;
                                        if (var.Name != null)
                                        {
                                            SanScriptEnumeration e = new SanScriptEnumeration(varName.Text, enumType, filenameHashCode,
                                                var.StartOffset, var.DisplayText, var.MarkupText);

                                            enumType.Enums.Add(e);
                                            SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, e);
                                        }
                                    }
                                }

                                SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, enumType);
                            }
                        }
                        break;
                    case SanScriptNodeType.VariableArrayDeclarationStatement:
                    case SanScriptNodeType.VariableNonArrayDeclarationStatement:
                        {
                            VariableDeclarationStatement var = node as VariableDeclarationStatement;
                            while ( var != null )
                            {
                                Identifier name = var.Name;
                                if ( name != null )
                                {
                                    Identifier dataType = var.Type;
                                    SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, new SanScriptStaticVariable( name.Text,
                                        dataType == null ? null : dataType.Text, filenameHashCode, var.StartOffset,
                                        var.DisplayText, var.MarkupText ) );
                                }

                                var = var.SubDeclaration as VariableDeclarationStatement;
                            }
                        }
                        break;
                    case SanScriptNodeType.NativeEventDeclarationStatement:
                    case SanScriptNodeType.NativeFuncDeclarationStatement:
                    case SanScriptNodeType.NativeProcDeclarationStatement:
                        {
                            NativeSubroutineDeclarationStatement stmt = node as NativeSubroutineDeclarationStatement;
                            Identifier name = stmt.Name;
                            if ( name != null )
                            {
                                SanScriptNativeSubroutine subroutine = new SanScriptNativeSubroutine( name.Text,
                                    filenameHashCode, stmt.StartOffset, stmt.DisplayText, stmt.MarkupText );

                                IAstNodeList args = stmt.Arguments;
                                foreach ( AstNode arg in args )
                                {
                                    if ( arg is VariableDeclarationStatement )
                                    {
                                        VariableDeclarationStatement var = arg as VariableDeclarationStatement;
                                        subroutine.ArgumentsMarkupText.Add( var.MarkupText );
                                    }
                                }

                                SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, subroutine );
                            }
                        }
                        break;
                    case SanScriptNodeType.EventDefinitionBlockStatement:
                    case SanScriptNodeType.FuncDefinitionBlockStatement:
                    case SanScriptNodeType.ProcDefinitionBlockStatement:
                        {
                            SubroutineDefinitionBlockStatement stmt = node as SubroutineDefinitionBlockStatement;
                            Identifier name = stmt.Name;
                            if ( name != null )
                            {
                                SanScriptSubroutine subroutine = new SanScriptSubroutine( name.Text,
                                    filenameHashCode, stmt.StartOffset, stmt.DisplayText, stmt.MarkupText );

                                IAstNodeList args = stmt.Arguments;
                                foreach ( AstNode arg in args )
                                {
                                    if ( arg is VariableDeclarationStatement )
                                    {
                                        VariableDeclarationStatement var = arg as VariableDeclarationStatement;
                                        subroutine.ArgumentsMarkupText.Add( var.MarkupText );
                                    }
                                }

                                SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, subroutine );
                            }
                        }
                        break;
                    case SanScriptNodeType.StateDefinitionBlockStatement:
                        {
                            StateDefinitionBlockStatement stmt = node as StateDefinitionBlockStatement;
                            Identifier name = stmt.Name;
                            if ( name != null )
                            {
                                SanScriptProjectResolver.IdentifierCache.AddIdentifier(idFile, new SanScriptState( name.Text,
                                    filenameHashCode, stmt.StartOffset, stmt.DisplayText, stmt.MarkupText ) );

                                if ( recurseStates )
                                {
                                    UpdateIdentifierCache( stmt.Statements, idFile, foundIdentifiers,
                                        false, true, true );
                                }
                            }
                        }
                        break;
                    case SanScriptNodeType.GlobalsDefinitionBlockStatement:
                        {
                            GlobalsDefinitionBlockStatement stmt = node as GlobalsDefinitionBlockStatement;
                            UpdateIdentifierCache( stmt.Statements, idFile, foundIdentifiers,
                                true, false, false );
                            idFile.ContainsGlobals = true;
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Run through all of the Identifier <see cref="IToken"/>s in the <see cref="Document"/>, find their definitions,
        /// and override their ID with the correct one from the Identifier Cache.
        /// </summary>
        /// <param name="doc">The <see cref="Document"/> containing the token collection.</param>
        private void FixupIdentifierTokenIDs( Document doc )
        {
            CompilationUnit unit = doc.SemanticParseData as CompilationUnit;
            SanScriptProjectResolver projectResolver = doc.LanguageData as SanScriptProjectResolver;

            Dictionary<string, int> foundIdentifiers = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

            bool isFinalBuild = SanScriptProjectResolver.ParserPlugin.Custom.IndexOf("-final", StringComparison.OrdinalIgnoreCase) >= 0;

            ISet<int> dependenciesHashCodes = SanScriptProjectResolver.GetDependenciesHashCodes(doc.Filename, projectResolver);
            try
            {
                int disabledBlockDepth = 0;

                // Set to true if we're between #IF and its associated identifier
                bool definingPoundIf = false;

                // True if the #IF is checking a truth rather than a NOT. If the define is #IF NOT, then this is false.
                bool checkingTrue = true;

                foreach ( SanScriptToken token in doc.Tokens )
                {
                    token.SetEnabled( true );

                    if ( !this.HighlightDisabledCodeBlocks && disabledBlockDepth > 0 )
                    {
                        if ( token.ID == SanScriptTokenID.PoundEndif )
                        {
                            disabledBlockDepth = Math.Max(0, disabledBlockDepth - 1);
                            if (disabledBlockDepth > 0)
                            {
                                token.SetEnabled( false );
                            }
                        }
                        else
                        {
                            switch ( token.ID )
                            {
                                case SanScriptTokenID.Invalid:
                                case SanScriptTokenID.DocumentEnd:
                                case SanScriptTokenID.Whitespace:
                                case SanScriptTokenID.LineTerminator:
                                    break;

                                case SanScriptTokenID.PoundIf:
                                    {
                                        disabledBlockDepth++;
                                        token.SetEnabled( false );
                                    }
                                    break;

                                default:
                                    {
                                        token.SetEnabled( false );
                                    }
                                    break;
                            }                            
                        }
                    }
                    else
                    {
                        switch ( token.ID )
                        {
                            case SanScriptTokenID.Identifier:
                            case SanScriptTokenID.NativeIdentifier:
                            case SanScriptTokenID.TypedefIdentifier:
                            case SanScriptTokenID.UserIdentifier:
                                {
                                    String tokenText = doc.GetTokenText(token);
                                    int id = FindIdentifierTokenID( doc.Filename, tokenText, dependenciesHashCodes, foundIdentifiers );
                                    token.SetOverrideID( id );

                                    if (definingPoundIf)
                                    {
                                        if (tokenText.Equals("IS_DEBUG_BUILD", StringComparison.OrdinalIgnoreCase))
                                        {
                                            bool isDebugBuild = !isFinalBuild;
                                            if (isDebugBuild != checkingTrue)
                                            {
                                                disabledBlockDepth++;
                                            }
                                        }
                                        else if (tokenText.Equals("IS_FINAL_BUILD", StringComparison.OrdinalIgnoreCase))
                                        {
                                            if (isFinalBuild != checkingTrue)
                                            {
                                                disabledBlockDepth++;
                                            }
                                        }
                                    }

                                    definingPoundIf = false;
                                }
                                break;

                            case SanScriptTokenID.NOT:
                                if (definingPoundIf)
                                {
                                    checkingTrue = false;
                                }
                                break;

                            case SanScriptTokenID.PoundIf:
                                definingPoundIf = true;
                                checkingTrue = true;
                                break;

                            default:
                                break;
                        }
                    }
                }
            }
            catch ( Exception e )
            {
                // this operation is not thread-safe
                ApplicationSettings.Log.ToolException(e, "The operation on '{0}' is not thread-safe", doc.Filename);
            }
        }

        /// <summary>
        /// Searches for the <see cref="SanScriptTokenID"/> of the given identifier name.
        /// </summary>
        /// <param name="filename">The file that initiates the search.</param>
        /// <param name="name">The keyword to search for.</param>
        /// <param name="dependenciesHashCodes">Hash codes of this file's dependencies.</param>
        /// <param name="foundIdentifiers">A <see cref="Dictionary"/> of identifiers that have already been found.</param>
        /// <returns>The <see cref="SanScriptTokenID"/> for name.</returns>
        private int FindIdentifierTokenID( string filename, string name, 
            ISet<int> dependenciesHashCodes, Dictionary<string, int> foundIdentifiers )
        {
            int tokenID = SanScriptTokenID.Invalid;
            if ( !foundIdentifiers.TryGetValue( name, out tokenID ) )
            {
                SanScriptIdentifier identifier = SanScriptProjectResolver.FindIdentifierAccessibleFromFileExact(filename, name, dependenciesHashCodes);
                if (identifier != null)
                {
                    tokenID = identifier.TokenID;
                }

                foundIdentifiers.Add( name, tokenID );
            }

            return tokenID;
        }

        /// <summary>
        /// Searches for the <see cref="Identifier"/>.<see cref="DeclarationType"/> of the given identifier name
        /// </summary>
        /// <param name="filename">The file that initiates the search.</param>
        /// <param name="name">The keyword to search for.</param>
        /// <param name="dependenciesHashCodes">Hash codes of this file's dependencies.</param>
        /// <param name="foundIdentifiers">A <see cref="Dictionary"/> of identifiers that have already been found.</param>
        /// <returns>The <see cref="Identifier"/>.<see cref="DeclarationType"/> for name.</returns>
        private Identifier.DeclarationType FindIdentifierDeclareType( string filename, string name, 
            ISet<int> dependenciesHashCodes, Dictionary<string, int> foundIdentifiers )
        {
            int tokenID = FindIdentifierTokenID( filename, name, dependenciesHashCodes, foundIdentifiers );
            switch ( tokenID )
            {
                case SanScriptTokenID.NativeIdentifier:
                    return Identifier.DeclarationType.Native;
                
                case SanScriptTokenID.TypedefIdentifier:
                case SanScriptTokenID.UserIdentifier:
                    return Identifier.DeclarationType.User;
                
                case SanScriptTokenID.Identifier:
                    return Identifier.DeclarationType.Local;
                
                default:
                    if ( (tokenID > SanScriptTokenID.NativeTypeStart) && (tokenID < SanScriptTokenID.NativeTypeEnd) )
                    {
                        return Identifier.DeclarationType.NativeType;
                    }
                    return Identifier.DeclarationType.Local;
            }
        }

        /// <summary>
        /// Locates a <see cref="SanScriptToken"/>'s matching pair, where the token's ID is IF, ELIF, ELSE, ENDIF, 
        /// CASE, DEFAULT, BREAK, or FALLTHRU.
        /// </summary>
        /// <param name="doc">The <see cref="Document"/> containing the <see cref="Token"/> collection and <see cref="SemanticParseData"/>.</param>
        /// <param name="token">The <see cref="SanScriptToken"/> for which to look for its match.</param>
        /// <param name="caretOffset">The current caret position in the <see cref="SyntaxEditor"/>'s selected view.</param>
        /// <returns>The <see cref="SanScriptToken"/> that matches the token, <c>null</c> if no matches found.</returns>
        private SanScriptToken FindMatchingPair( Document doc, SanScriptToken token, int caretOffset )
        {
            CompilationUnit unit = doc.SemanticParseData as CompilationUnit;
            if ( unit == null )
            {
                return null;
            }

            // find the node and the statement that contains the token
            AstNode node = unit.FindDescendant(typeof(Statement), token.StartOffset ) as AstNode;
            if (node == null)
            {
                return null;
            }

            if (node.NodeType == SanScriptNodeType.ExitStatement || node.NodeType == SanScriptNodeType.ReturnStatement)
            {
                // look for a parent proc or func block statement
                while (node != null && node.NodeType != SanScriptNodeType.FuncDefinitionBlockStatement && node.NodeType != SanScriptNodeType.ProcDefinitionBlockStatement)
                {
                    node = node.ParentNode as AstNode;
                }
            }
            else if (node.NodeType == SanScriptNodeType.FallthruStatement)
            {
                // Look for parent case block
                while (node != null && node.NodeType != SanScriptNodeType.CaseBlockStatement)
                {
                    node = node.ParentNode as AstNode;
                }
            }
            else if (node.NodeType == SanScriptNodeType.BreakStatement)
            {
                // Look for parent while, for, repeat, case blocks
                // Viv: I'm unsure if the BREAK statement works on loops, or if you *have* to use BREAKLOOP.
                // Assuming BREAK works for loops and other statements and BREAKLOOP *only* works on loops.
                while (node != null && node.NodeType != SanScriptNodeType.WhileBlockStatement && node.NodeType != SanScriptNodeType.ForBlockStatement &&
                    node.NodeType != SanScriptNodeType.RepeatBlockStatement && node.NodeType != SanScriptNodeType.CaseBlockStatement)
                {
                    node = node.ParentNode as AstNode;
                }
            }
            else if (node.NodeType == SanScriptNodeType.BreakLoopStatement || node.NodeType == SanScriptNodeType.ReLoopStatement)
            {
                // Look for parent loop
                while (node != null && node.NodeType != SanScriptNodeType.WhileBlockStatement && node.NodeType != SanScriptNodeType.ForBlockStatement &&
                    node.NodeType != SanScriptNodeType.RepeatBlockStatement)
                {
                    node = node.ParentNode as AstNode;
                }
            }

            if ( node == null || !node.IntersectsWith( token.StartOffset ) )
            {
                return null;
            }

            int offset = token.StartOffset + (token.Length / 2);
            bool firstHalfOfWord = caretOffset < offset;

            int findStartTokenID = SanScriptTokenID.Invalid;
            int findEndTokenID = SanScriptTokenID.Invalid;

            if ( node.NodeType == SanScriptNodeType.IfBlockStatement )
            {
                IfBlockStatement stmt = node as IfBlockStatement;

                if ( (token.ID == SanScriptTokenID.IF
                    || (token.ID == SanScriptTokenID.ELIF || token.ID == SanScriptTokenID.ELSE) && !firstHalfOfWord)
                    && stmt.EndTokenID != SanScriptTokenID.Invalid )
                {
                    findEndTokenID = stmt.EndTokenID;
                }
                else if ( (token.ID == SanScriptTokenID.ELIF || token.ID == SanScriptTokenID.ELSE) && firstHalfOfWord
                    && stmt.StartTokenID != SanScriptTokenID.Invalid )
                {
                    // get the previous IfBlockStatement
                    AstNode parentNode = stmt.ParentNode as AstNode;
                    if ( parentNode == null )
                    {
                        return null;
                    }

                    int indexOf = parentNode.ChildNodes.IndexOf( stmt );
                    if ( indexOf > 0 )
                    {
                        node = parentNode.ChildNodes[indexOf - 1] as AstNode;
                        if ( node.NodeType != SanScriptNodeType.IfBlockStatement )
                        {
                            return null;
                        }

                        stmt = node as IfBlockStatement;
                    }

                    findStartTokenID = stmt.StartTokenID;
                }
                else if ( token.ID == SanScriptTokenID.ENDIF )
                {
                    findStartTokenID = stmt.StartTokenID;
                }
            }
            else if ( node.NodeType == SanScriptNodeType.CaseBlockStatement )
            {
                // Go to the first case in this chain, if we're in a chain
                while (node.ParentNode is CaseBlockStatement)
                {
                    node = (AstNode)node.ParentNode;
                }

                CaseBlockStatement stmt = node as CaseBlockStatement;
                if ( (token.ID == SanScriptTokenID.CASE || token.ID == SanScriptTokenID.DEFAULT)
                    && stmt.EndTokenID != SanScriptTokenID.Invalid )
                {
                    findEndTokenID = stmt.EndTokenID;
                }
                else if ( (token.ID == SanScriptTokenID.BREAK || token.ID == SanScriptTokenID.FALLTHRU)
                    && stmt.StartTokenID != SanScriptTokenID.Invalid )
                {
                    findStartTokenID = stmt.StartTokenID;
                }
            }
            else if (node.NodeType == SanScriptNodeType.WhileBlockStatement || node.NodeType == SanScriptNodeType.ForBlockStatement ||
                node.NodeType == SanScriptNodeType.RepeatBlockStatement || node.NodeType == SanScriptNodeType.FuncDefinitionBlockStatement ||
                node.NodeType == SanScriptNodeType.ProcDefinitionBlockStatement)
            {
                // If it's one of these block types, we have iterated upwards from a BREAKLOOP, RELOOP, EXIT, RETURN, BREAK, or FALLTHRU.
                // We can simply return this token. Matching start and end pairs of these is done by the library outside of this method.
                return doc.Tokens.GetTokenAtOffset( node.StartOffset ) as SanScriptToken;
            }
            else if ( node.NodeType == SanScriptNodeType.PreprocessorMacroStatement )
            {
                PreprocessorMacroStatement stmt = node as PreprocessorMacroStatement;
                node = node.ParentNode as AstNode;

                IAstNodeList childNodes = node.ChildNodes;
                int indexOf = childNodes.IndexOf( stmt );

                if ( stmt.TokenID == SanScriptTokenID.PoundIf )
                {
                    ++indexOf;

                    int scopeLevel = 0;
                    while ( indexOf < childNodes.Count )
                    {
                        AstNode n = childNodes[indexOf] as AstNode;
                        if ( n.NodeType == SanScriptNodeType.PreprocessorMacroStatement )
                        {
                            stmt = n as PreprocessorMacroStatement;
                            if ( stmt.TokenID == SanScriptTokenID.PoundEndif )
                            {
                                if ( scopeLevel == 0 )
                                {
                                    node = n;
                                    findEndTokenID = stmt.TokenID;
                                    break;
                                }

                                --scopeLevel;
                            }
                            else
                            {
                                ++scopeLevel;
                            }
                        }

                        ++indexOf;
                    }
                }
                else
                {
                    --indexOf;

                    int scopeLevel = 0;
                    while ( indexOf >= 0 )
                    {
                        AstNode n = childNodes[indexOf] as AstNode;
                        if ( n.NodeType == SanScriptNodeType.PreprocessorMacroStatement )
                        {
                            stmt = n as PreprocessorMacroStatement;
                            if ( stmt.TokenID == SanScriptTokenID.PoundEndif )
                            {
                                --scopeLevel;
                            }
                            else
                            {
                                if ( scopeLevel == 0 )
                                {
                                    node = n;
                                    findStartTokenID = stmt.TokenID;
                                    break;
                                }

                                ++scopeLevel;
                            }
                        }

                        --indexOf;
                    }
                }
            }

            if ( findStartTokenID != SanScriptTokenID.Invalid )
            {
                // find the start token
                SanScriptToken startToken = doc.Tokens.GetTokenAtOffset( node.StartOffset ) as SanScriptToken;
                if ( (startToken != null) && (startToken.ID == findStartTokenID) )
                {
                    return startToken;
                }
            }
            else if ( findEndTokenID != SanScriptTokenID.Invalid )
            {
                // find the end token
                SanScriptToken endToken = doc.Tokens.GetTokenAtOffset( node.EndOffset - 1 ) as SanScriptToken;

                if ( endToken != null )
                {
                    if ( node.NodeType == SanScriptNodeType.IfBlockStatement )
                    {
                        TextStream stream = doc.GetTextStream( node.EndOffset );
                        while ( !stream.IsAtDocumentEnd && (endToken.ID != findEndTokenID) )
                        {
                            endToken = stream.ReadToken() as SanScriptToken;
                        }
                    }
                    else if ( node.NodeType == SanScriptNodeType.CaseBlockStatement )
                    {
                        if ( findEndTokenID == SanScriptTokenID.RETURN || findEndTokenID == SanScriptTokenID.EXIT)
                        {
                            TextStream stream = doc.GetTextStream( node.EndOffset );
                            while ( !stream.IsAtDocumentStart && (endToken.ID != findEndTokenID) )
                            {
                                endToken = stream.ReadTokenReverse() as SanScriptToken;
                            }
                        }
                    }

                    if ( (endToken != null) && (endToken.ID == findEndTokenID) )
                    {
                        return endToken;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Given a document, a token and its text, find declared variables in scope from the token's position.
        /// </summary>
        /// <param name="doc">The document.</param>
        /// <param name="token">The token containing the text.</param>
        /// <param name="text">The text.</param>
        /// <param name="contains">If true, use contains, rather than StartsWith.</param>
        /// <returns>A list of declarations containing <paramref name="text"/> in scope from <paramref name="token"/>'s position</returns>
        private IEnumerable<VariableDeclarationStatement> FindDeclaredVariablesContaining(Document doc, IToken token, string text, bool contains)
        {
            // Dictionary with key = statement, value = matching substring index. The list will be ordered by the value.
            ICollection<KeyValuePair<VariableDeclarationStatement, int>> localDeclarations = new List<KeyValuePair<VariableDeclarationStatement, int>>();
            CompilationUnit unit = doc.SemanticParseData as CompilationUnit;
            if (unit == null)
            {
                return Enumerable.Empty<VariableDeclarationStatement>();
            }

            // First get a list of identifiers within this block statement and ancestors if any. This way we include in-scope local variables
            // and argument list variables.
            IAstNode node = unit.FindNodeRecursive(token.StartOffset);
            while (node != null)
            {
                if (node is BlockStatement)
                {
                    // Get variable declarations
                    // Keep drilling into children declarations (e.g. INT i, j, k) -- these are nested inside each other.
                    IEnumerable<VariableDeclarationStatement> variableDecls = node.ChildNodes.OfType<VariableDeclarationStatement>().ToList();
                    while (variableDecls.Any())
                    {
                        foreach (VariableDeclarationStatement stmt in variableDecls)
                        {
                            int indexOf = contains
                                ? stmt.Name.Text.IndexOf(text, StringComparison.OrdinalIgnoreCase)
                                : stmt.Name.Text.StartsWith(text, StringComparison.OrdinalIgnoreCase)
                                ? 0
                                : -1;
                            if (indexOf >= 0)
                            {
                                localDeclarations.Add(new KeyValuePair<VariableDeclarationStatement, int>(stmt, indexOf));
                            }
                        }

                        variableDecls = variableDecls.SelectMany(d => d.ChildNodes.OfType<VariableDeclarationStatement>()).ToList();
                    }
                }

                node = node.FindAncestor(typeof(BlockStatement));
            }

            return localDeclarations
                .Distinct(kvp => kvp.Key.Name.Text)
                .OrderBy(kvp => kvp.Value)
                .ThenBy(kvp => kvp.Key.Name.Text)
                .Select(kvp => kvp.Key)
                .ToList();
        }

        /// <summary>
        /// Searches for the statement that defines the text identifier.
        /// </summary>
        /// <param name="doc">The <see cref="Document"/> that initiates the search.</param>
        /// <param name="text">The <c>string</c> to search for.</param>
        /// <param name="lookForStructMemberAccess"><c>true</c> to look in the local file for struct member access expressions.</param>
        /// <param name="includeDeclarations"><c>true</c> to return the declaration if that is where the token is at.</param>
        /// <param name="dependenciesHashCodes">All the filename hash codes for this file, pre-calculated.</param>
        /// <param name="foundObject">Return by reference the <see cref="AstNode"/> or <see cref="SanScriptIdentifier"/> found.</param>
        /// <param name="foundFilename">The file where it was found.</param>
        /// <returns><c>true</c> if found, otherwise, <c>false</c>.</returns>
        private bool FindDefinition( Document doc, IToken token, bool lookForStructMemberAccess, bool includeDeclarations, 
            ISet<int> dependenciesHashCodes, ref object foundObject, ref string foundFilename)
        {
            if ( token.ID == SanScriptTokenID.Disabled )
            {
                return false;
            }

            // look for a scoped variable first
            if ( doc.SemanticParseData != null )
            {
                AstNode foundNode = null;
                if ( FindDefinition( doc, token, lookForStructMemberAccess, includeDeclarations, ref foundNode ) )
                {
                    if ( foundNode == null )
                    {
                        return false;
                    }

                    switch ( foundNode.NodeType )
                    {
                        case SanScriptNodeType.UsingStatement:
                            {
                                Expression expr = ((UsingStatement)foundNode).FileName;
                                if ( (expr != null) && (expr.NodeType == SanScriptNodeType.StringExpression) )
                                {
                                    string filename = ((StringExpression)expr).StringValue;
                                    if ( filename != null )
                                    {
                                        foundObject = foundNode;
                                        foundFilename = filename.ToLower();
                                        return true;
                                    }
                                }

                                return false;
                            }
                        case SanScriptNodeType.ArrayAccessExpression:
                        case SanScriptNodeType.StructMemberAccessExpression:
                            if ( FindStructMemberDefinition( doc, token, foundNode, dependenciesHashCodes, ref foundObject, ref foundFilename ) )
                            {
                                return true;
                            }
                            break;
                        default:
                            foundObject = foundNode;
                            foundFilename = doc.Filename.ToLower();
                            return true;
                    }
                }
            }

            string text = doc.GetTokenText(token);

            // look in the cache
            IEnumerable<SanScriptIdentifier> identifiers = SanScriptProjectResolver.FindIdentifiersAccessibleFromFileExact( doc.Filename, text, dependenciesHashCodes );
            if ( identifiers.Any())
            {
                int filenameHashCode = SanScriptIdentifierCache.GetFilenameHashCode(doc.Filename);
                foreach ( SanScriptIdentifier id in identifiers )
                {
                    // only search in other files since the FindDefinition called before has already checked the current one,
                    // unless we don't have SemanticParseData and therefor couldn't search the current file.
                    if ( (id.FilenameHashCode != filenameHashCode) || (doc.SemanticParseData == null) )
                    {
                        foundObject = id;
                        foundFilename = SanScriptProjectResolver.IdentifierCache.GetFilename( id.FilenameHashCode );
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Searches for the statement that defines the text identifier in the local scope and working upwards.
        /// </summary>
        /// <param name="doc">The <see cref="Document"/> that initiates the search.</param>
        /// <param name="text">The <c>string</c> to search for.</param>
        /// <param name="lookForStructMemberAccess"><c>true</c> to look in the local file for struct member access expressions.</param>
        /// <param name="includeDeclarations"><c>true</c> to return the declaration if that is where the token is at.</param>
        /// <param name="foundNode">The <see cref="AstNode"/> statement that defined the keyword.  Can be <c>null</c>.</param>
        /// <returns><c>true</c> if we should continue searching, otherwise, <c>false</c>.</returns>
        private bool FindDefinition( Document doc, IToken token, bool lookForStructMemberAccess, bool includeDeclarations, 
            ref AstNode foundNode )
        {
            CompilationUnit unit = doc.SemanticParseData as CompilationUnit;
            
            string text = doc.GetTokenText( token ).ToUpper();

            // start with the current scope and work our way up.
            AstNode node = unit.FindNodeRecursive( token.StartOffset ) as AstNode;

            bool inVariableExpression = false;
            if ( (node != null) && (node.ParentNode != null) && (node.ParentNode is VariableExpression) )
            {
                inVariableExpression = true;
            }
            
            bool inGotoStatement = false;

            while ( node != null )
            {
                switch ( node.NodeType )
                {
                    case SanScriptNodeType.GotoStatement:
                        inGotoStatement = true;
                        break;
                    case SanScriptNodeType.UsingStatement:
                        foundNode = node;
                        return true;
                    case SanScriptNodeType.StructMemberAccessExpression:
                        if ( lookForStructMemberAccess )
                        {
                            foundNode = node;
                            return true;
                        }
                        break;
                    case SanScriptNodeType.ArrayAccessExpression:
                        {
                            if ( lookForStructMemberAccess )
                            {
                                Expression subExpr = ((ArrayAccessExpression)node).AccessSubExpression;
                                if ( (subExpr != null) && (subExpr.NodeType == SanScriptNodeType.StructMemberAccessExpression) )
                                {
                                    bool intersectsWith = subExpr.IntersectsWith( token.StartOffset );
                                    if ( inVariableExpression == intersectsWith )
                                    {
                                        foundNode = node;
                                        return true;
                                    }
                                }
                            }

                            Expression accessExpr = ((ArrayAccessExpression)node).AccessExpression;
                            if ( accessExpr != null )
                            {
                                bool intersectsWith = accessExpr.IntersectsWith( token.StartOffset );
                                if ( intersectsWith )
                                {
                                    lookForStructMemberAccess = false;
                                }
                            }
                        }
                        break;
                    default:
                        if ( node is BlockStatement )
                        {
                            IAstNodeList nodeList = node.ChildNodes;
                            foreach ( AstNode n in nodeList )
                            {
                                if (!inGotoStatement && (n.StartOffset > token.StartOffset))
                                {
                                    // Can't break here as the child nodes are not always in order. Statements can come before arguments.
                                    continue;
                                }

                                if (this.FindDeclaration(text, token.StartOffset, n, includeDeclarations, inGotoStatement, ref foundNode))
                                {
                                    return true;
                                }
                            }
                        }
                        break;
                }

                node = node.ParentNode as AstNode;
            }

            // search other identifiers in the current file
            SanScriptIdentifier identifier = SanScriptProjectResolver.IdentifierCache.FindIdentifierInFileExact( doc.Filename, text );
            if ( identifier != null )
            {
                return FindStatementWithStartOffset( doc, unit, identifier.StartOffset, token.StartOffset, includeDeclarations, ref foundNode );
            }
            
            return false;
        }

        /// <summary>
        /// Determines if the <see cref="AstNode"/> is a Variable or Label declaration matching text
        /// </summary>
        /// <param name="text">The text to match.</param>
        /// <param name="startOffset">The startOffset of the text.</param>
        /// <param name="node">The <see cref="AstNode"/> to compare against.</param>
        /// <param name="includeDeclarations">Whether the startOffset can be contained in the <see cref="AstNode"/> or not.</param>
        /// <param name="inGotoStatement">Whether the startOffset is inside a GotoStatement or not.</param>
        /// <param name="foundNode">Return by reference the <see cref="AstNode"/> of the declaration.</param>
        /// <returns><c>true</c> if found, otherwise <c>false</c>.</returns>
        private bool FindDeclaration( string text, int startOffset, AstNode node, bool includeDeclarations, bool inGotoStatement, 
            ref AstNode foundNode )
        {
            if ( node is VariableDeclarationStatement )
            {
                VariableDeclarationStatement var = node as VariableDeclarationStatement;
                while ( var != null )
                {
                    Identifier name = var.Name;
                    if ( (name != null && name.Text != null) && (name.Text.ToUpper() == text) )
                    {
                        if ( var.IntersectsWith( startOffset ) )
                        {
                            // we're at the declaration, search no more by returning true.
                            foundNode = includeDeclarations ? var : null;
                        }
                        else
                        {
                            foundNode = var;
                        }

                        return true;
                    }

                    var = var.SubDeclaration as VariableDeclarationStatement;
                }
            }
            else if ( inGotoStatement && (node is LabelDeclarationStatement) )
            {
                LabelDeclarationStatement label = node as LabelDeclarationStatement;
                Identifier name = label.Name;
                if ( (name != null) && (name.Text.ToUpper() == text) )
                {
                    if ( label.IntersectsWith( startOffset ) )
                    {
                        // we're at the declaration, search no more by returning true.
                        foundNode = includeDeclarations ? label : null;
                    }
                    else
                    {
                        foundNode = label;
                    }

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Starting with the node at the given startOffset, searches for its parent <see cref="Statement"/>.
        /// </summary>
        /// <param name="doc">The <see cref="Document"/> containing the <see cref="CompilationUnit"/>.</param>
        /// <param name="unit">The <see cref="CompilationUnit"/> to search.</param>
        /// <param name="startOffset">The offset of the node to search for.</param>
        /// <param name="caretOffset">Used to test for intersection with the <see cref="Statement"/> node.</param>
        /// <param name="allowIntersections"><c>true</c> to allow for the caretOffset to intersect with the <see cref="Statement"/> node.</param>
        /// <param name="foundNode">Return by reference the <see cref="AstNode"/> found.  Can be <c>null</c>, even if we return <c>true</c>.</param>
        /// <returns><c>true</c> if we should continue searching, otherwise <c>false</c></returns>
        private bool FindStatementWithStartOffset( Document doc, CompilationUnit unit, int startOffset, int caretOffset, 
            bool allowIntersections, ref AstNode foundNode )
        {
            AstNode node = unit.FindNodeRecursive( startOffset ) as AstNode;
            if ( node == null )
            {
                // this probably means we have a multiple variable declaration
                IAstNodeList staticVariables = unit.StaticVariables;
                foreach ( VariableDeclarationStatement decl in staticVariables )
                {
                    bool found = false;

                    VariableDeclarationStatement var = decl;
                    while ( var != null )
                    {
                        if ( var.Contains( startOffset ) )
                        {
                            node = var;
                            found = true;
                            break;
                        }

                        var = var.SubDeclaration as VariableDeclarationStatement;
                    }

                    if ( found )
                    {
                        break;
                    }
                }

                if ( node == null )
                {
                    return false;
                }
            }
            
            if ( node.StartOffset == startOffset )
            {
                // back up to the Statement
                while ( (node != null) && !(node is Statement) )
                {
                    node = node.ParentNode as AstNode;
                }

                if ( node != null )
                {
                    if ( node.IntersectsWith( caretOffset ) )
                    {
                        // we're at the declaration, search no more by returning true.
                        foundNode = allowIntersections ? node : null;
                    }
                    else
                    {
                        foundNode = node;
                    }
                }

                return true;
            }
            else
            {
                IAstNodeList childNodes = node.ChildNodes;

                int i = 0;
                while ( (childNodes != null) && (i < childNodes.Count) )
                {
                    if ( childNodes[i].StartOffset == startOffset )
                    {
                        // back up to the Statement
                        while ( (node != null) && !(node is Statement) )
                        {
                            node = node.ParentNode as AstNode;
                        }

                        if ( node != null )
                        {
                            if ( node.IntersectsWith( caretOffset ) )
                            {
                                // we're at the declaration, search no more by returning true.
                                foundNode = allowIntersections ? node : null;
                            }
                            else
                            {
                                foundNode = node;
                            }
                        }

                        return true;
                    }

                    if ( childNodes[i].Contains( startOffset ) )
                    {
                        childNodes = childNodes[i].ChildNodes;
                        i = 0;
                    }
                    else
                    {
                        ++i;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Searches for the definition of the struct member variable given by the <see cref="IToken"/>.
        /// </summary>
        /// <param name="doc">The <see cref="Document"/> to search.</param>
        /// <param name="token">The <see cref="IToken"/> that defines what to search for.</param>
        /// <param name="accessExpr">The <see cref="StructMemberAccessExpression"/> or <see cref="ArrayAccessExpression"/> node to start the search from.</param>
        /// <param name="dependenciesHashCodes">Set of hash codes for this file's dependencies.</param>
        /// <param name="foundObject">Return by reference the <see cref="AstNode"/> or <see>SanScriptIdentifier</see> found.</param>
        /// <param name="foundFilename">The file in which it can be found.</param>
        /// <returns><c>true</c> if found, otherwise <c>false</c>.</returns>
        private bool FindStructMemberDefinition( Document doc, IToken token, AstNode accessExpr, 
            ISet<int> dependenciesHashCodes, ref object foundObject, ref string foundFilename )
        {
            // collect names of the members to look for
            List<string> memberAccess = new List<string>();

            string text = doc.GetTokenText( token ).ToUpper();
            memberAccess.Add( text );

            IToken topmostStructToken = null;

            // search for parent nodes that are StructMemberAccessExpressions or ArrayAccessExpressions and add 
            // their name to our memberAccess list
            AstNode node = accessExpr;
            while ( (node != null) && !(node is Statement) )
            {
                if ( node.NodeType == SanScriptNodeType.StructMemberAccessExpression )
                {
                    Identifier name = ((StructMemberAccessExpression)node).Name;
                    if ( name != null )
                    {
                        if ( name.StartOffset != token.StartOffset )
                        {
                            memberAccess.Add( name.Text.ToUpper() );
                        }

                        topmostStructToken = doc.Tokens.GetTokenAtOffset( name.StartOffset );
                    }

                    node = node.ParentNode as AstNode;
                }
                else if ( node.NodeType == SanScriptNodeType.ArrayAccessExpression )
                {
                    ArrayAccessExpression expr = node as ArrayAccessExpression;
                    AstNode arrayAccess = expr.AccessExpression;
                    if ( (topmostStructToken != null) && (arrayAccess != null) 
                        && arrayAccess.IntersectsWith( topmostStructToken.StartOffset ) )
                    {
                        // we're in the array indexer, so bail out
                        break;
                    }

                    Identifier name = expr.Name;
                    if ( name != null )
                    {
                        if ( name.StartOffset != token.StartOffset )
                        {
                            memberAccess.Add( name.Text.ToUpper() );
                        }

                        topmostStructToken = doc.Tokens.GetTokenAtOffset( name.StartOffset );
                    }

                    node = node.ParentNode as AstNode;
                }
                else
                {
                    // we're some other type of node, so bail out
                    break;
                }
            }

            if ( topmostStructToken == null )
            {
                // couldn't find the topmost structure in the expression
                return false;
            }

            // remove last item, it is the topmost structure
            memberAccess.RemoveAt( memberAccess.Count - 1 );

            // find the top most structure's declaration
            object structObject = null;
            string structFilename = null;
            if ( !FindDefinition( doc, topmostStructToken, false, false, dependenciesHashCodes, ref structObject, ref structFilename ) )
            {
                return false;
            }

            if ( topmostStructToken.StartOffset == token.StartOffset )
            {
                // there's no struct access, so just find where the structure is declared
                foundObject = structObject;
                foundFilename = structFilename;
                return true;
            }

            // get it's type
            string structTypeName = null;
            if ( structObject is VariableDeclarationStatement )
            {
                VariableDeclarationStatement stmt = structObject as VariableDeclarationStatement;
                Identifier dataType = stmt.Type;
                if ( dataType != null )
                {
                    structTypeName = dataType.Text;
                }
            }
            else if ( structObject is SanScriptStaticVariable )
            {
                SanScriptStaticVariable var = structObject as SanScriptStaticVariable;
                if ( (var.Type != null) && (var.Type != string.Empty) )
                {
                    structTypeName = var.Type;
                }
            }

            if ( structTypeName == null )
            {
                // could not determine the type of the structure
                return false;
            }

            // search down the access tree to the end
            SanScriptStructMember idMember = null;
            for ( int i = memberAccess.Count - 1; i >= 0; --i )
            {
                idMember = FindStructMemberDefinition(doc.LanguageData as SanScriptProjectResolver, structFilename,
                    structTypeName, memberAccess[i], dependenciesHashCodes);
                if ( (idMember == null) || (idMember.Type == null) || (idMember.Type == string.Empty) )
                {
                    return false;
                }

                structFilename = SanScriptProjectResolver.IdentifierCache.GetFilename( idMember.FilenameHashCode );
                structTypeName = idMember.Type;
            }

            foundObject = idMember;
            foundFilename = structFilename;
            return true;
        }

        /// <summary>
        /// Searches for the definition of the member variable in the given structurre
        /// </summary>
        /// <param name="projectResolver">The <see cref="SanScriptProjectResolver"/> that should contain the definition.</param>
        /// <param name="filename">The file initiating the search.</param>
        /// <param name="structTypeName">The name of the struct type to search for.</param>
        /// <param name="structMemberName">The name of the member variable to search for.</param>
        /// <param name="dependenciesHashCodes">Collection of hash codes for this file's dependencies</param>
        /// <returns>The <see cref="SanScriptStructMember"/> found, or <c>null</c> if not found.</returns>
        private SanScriptStructMember FindStructMemberDefinition( SanScriptProjectResolver projectResolver, string filename,
            string structTypeName, string structMemberName, ISet<int> dependenciesHashCodes)
        {
            IEnumerable<SanScriptIdentifier> identifiers = SanScriptProjectResolver.FindIdentifiersAccessibleFromFileExact( filename, structTypeName, dependenciesHashCodes);
            foreach ( SanScriptIdentifier id in identifiers )
            {
                if ( id is SanScriptStructure )
                {
                    SanScriptStructure idStruct = id as SanScriptStructure;
                    foreach ( SanScriptStructMember member in idStruct.Members )
                    {
                        if ( member.Name.Equals(structMemberName, StringComparison.OrdinalIgnoreCase) )
                        {
                            return member;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Examines the line at the given offset and determines if it is a PURPOSE comment line.  Returns the <see cref="IToken"/> that is
        /// the SingleLineCommentStart.
        /// </summary>
        /// <param name="syntaxEditor"></param>
        /// <param name="offset">Offset that the line contains.</param>
        /// <returns>The <see cref="IToken"/>.</returns>
        private IToken GetPurposeComment( SyntaxEditor syntaxEditor, int offset )
        {
            // Check that we're not already in a PURPOSE comment block
            TextStream stream = syntaxEditor.Document.GetTextStream( offset );
            DocumentLine prevLine = stream.DocumentLine;

            stream.Offset = prevLine.EndOffset;
            stream.GoToCurrentTokenStart();

            bool isPurposeComment = false;

            IToken prevToken = stream.Token;
            while ( !stream.IsAtDocumentStart && (prevToken != null) && (prevToken.StartOffset >= prevLine.StartOffset) )
            {
                stream.ReadTokenReverse();
                prevToken = stream.Token;

                if ( prevToken.ID == SanScriptTokenID.SingleLineComment )
                {
                    if ( stream.TokenText[0] == '/' )
                    {
                        isPurposeComment = true;
                    }
                    else
                    {
                        break;  // this should mean that I can't add a purpose comment block
                    }
                }
                else if ( prevToken.ID == SanScriptTokenID.SingleLineCommentStart )
                {
                    if ( isPurposeComment )
                    {
                        return prevToken;
                    }

                    break;  // this should mean that I can add a purpose comment block
                }
            }

            return null;
        }

        /// <summary>
        /// Examines the current cursor position and tries to insert a PURPOSE comment block that is appropriate
        /// for the given context.
        /// </summary>
        /// <param name="syntaxEditor">The <see cref="SyntaxEditor"/> on which to attempt to insert the comment.</param>
        /// <returns><c>true</c> if a comment was inserted, otherwise <c>false</c>.</returns>
        private bool InsertPurposeComment( SyntaxEditor syntaxEditor )
        {
            // Try and ensure the compilation unit is up-to-date
            SemanticParserService.WaitForParse(
                SemanticParserServiceRequest.GetParseHashKey( syntaxEditor.Document, syntaxEditor.Document ) );

            if ( syntaxEditor.Document.SemanticParseData == null )
            {
                return false;
            }
            
            TextStream stream = syntaxEditor.Document.GetTextStream( syntaxEditor.Caret.Offset );
            DocumentLine currLine = stream.DocumentLine;

            if ( GetPurposeComment( syntaxEditor, syntaxEditor.SelectedView.CurrentDocumentLine.StartOffset - 1 ) != null )
            {
                if ( currLine.Text.Trim() == "///" )
                {
                    // just insert spaces for consistency.
                    syntaxEditor.SelectedView.InsertSurroundingTextSafe( "    ", string.Empty );
                }

                return false;
            }

            if ( (stream.Token.ID != SanScriptTokenID.SingleLineCommentEnd) || !currLine.Text.Trim().StartsWith( "///" ) )
            {
                return false;
            }
            
            // find the next non-whitespace token  
            stream.ReadToken();
            while ( !stream.IsAtDocumentEnd && ((stream.Token.ID == SanScriptTokenID.Whitespace) || (stream.Token.ID == SanScriptTokenID.DEBUGONLY) ) )
            {
                stream.ReadToken();
            }

            if ( stream.Token != null )
            {
                CompilationUnit unit = syntaxEditor.Document.SemanticParseData as CompilationUnit;
                AstNode node = unit.FindNodeRecursive( stream.Token.StartOffset ) as AstNode;

                // get the parent statement
                while ( (node != null) && !(node is Statement) )
                {
                    node = node.ParentNode as AstNode;
                }

                if ( (node == null) || (stream.Token.StartOffset > node.StartOffset) )
                {
                    return false;
                }

                string preString = null;
                IAstNodeList arguments = null;
                string returns = null;

                // build the comment 
                switch ( node.NodeType )
                {
                    case SanScriptNodeType.EventDefinitionBlockStatement:
                        {
                            preString = BuildTextForColumn( " PURPOSE:\n", "///    ", syntaxEditor.Caret.CharacterColumn - 3 );
                            arguments = ((EventDefinitionBlockStatement)node).Arguments;
                        }
                        break;
                    case SanScriptNodeType.FuncDefinitionBlockStatement:
                        {
                            preString = BuildTextForColumn( " PURPOSE:\n", "///    ", syntaxEditor.Caret.CharacterColumn - 3 );
                            arguments = ((FuncDefinitionBlockStatement)node).Arguments;
                            returns = ((FuncDefinitionBlockStatement)node).ReturnType.Text;
                        }
                        break;
                    case SanScriptNodeType.ProcDefinitionBlockStatement:
                        {
                            preString = BuildTextForColumn( " PURPOSE:\n", "///    ", syntaxEditor.Caret.CharacterColumn - 3 );
                            arguments = ((ProcDefinitionBlockStatement)node).Arguments;
                        }
                        break;
                    case SanScriptNodeType.NativeEventDeclarationStatement:
                        {
                            preString = BuildTextForColumn( " PURPOSE:\n", "///    ", syntaxEditor.Caret.CharacterColumn - 3 );
                            arguments = ((NativeEventDeclarationStatement)node).Arguments;
                        }
                        break;
                    case SanScriptNodeType.NativeFuncDeclarationStatement:
                        {
                            preString = BuildTextForColumn( " PURPOSE:\n", "///    ", syntaxEditor.Caret.CharacterColumn - 3 );
                            arguments = ((NativeFuncDeclarationStatement)node).Arguments;
                            returns = ((NativeFuncDeclarationStatement)node).ReturnType.Text;
                        }
                        break;
                    case SanScriptNodeType.NativeProcDeclarationStatement:
                        {
                            preString = BuildTextForColumn( " PURPOSE:\n", "///    ", syntaxEditor.Caret.CharacterColumn - 3 );
                            arguments = ((NativeProcDeclarationStatement)node).Arguments;
                        }
                        break;
                    case SanScriptNodeType.TypedefFuncDeclarationStatement:
                        {
                            preString = BuildTextForColumn( " PURPOSE:\n", "///    ", syntaxEditor.Caret.CharacterColumn - 3 );
                            arguments = ((TypedefFuncDeclarationStatement)node).Arguments;
                            returns = ((TypedefFuncDeclarationStatement)node).ReturnType.Text;
                        }
                        break;
                    case SanScriptNodeType.TypedefProcDeclarationStatement:
                        {
                            preString = BuildTextForColumn( " PURPOSE:\n", "///    ", syntaxEditor.Caret.CharacterColumn - 3 );
                            arguments = ((TypedefProcDeclarationStatement)node).Arguments;
                        }
                        break;
                    case SanScriptNodeType.ScriptBlockStatement:
                        {
                            preString = BuildTextForColumn( " PURPOSE:\n", "///    ", syntaxEditor.Caret.CharacterColumn - 3 );

                            Statement arg = ((ScriptBlockStatement)node).Argument;
                            if ( arg != null )
                            {
                                arguments = new AstNodeList( null );
                                arguments.Add( arg );
                            }
                        }
                        break;
                    case SanScriptNodeType.ConstDefinitionStatement:
                    case SanScriptNodeType.EnumDefinitionBlockStatement:
                    case SanScriptNodeType.HashEnumDefinitionBlockStatement:
                    case SanScriptNodeType.StrictEnumDefinitionBlockStatement:
                    case SanScriptNodeType.NativeTypeDeclarationStatement:
                    case SanScriptNodeType.StateDefinitionBlockStatement:
                    case SanScriptNodeType.StructDefinitionBlockStatement:
                        {
                            preString = " PURPOSE: ";
                        }
                        break;
                    default:
                        break;
                }

                if ( preString != null )
                {
                    StringBuilder postString = new StringBuilder();

                    if ( (arguments != null) && (arguments.Count > 0) )
                    {
                        postString.Append( BuildTextForColumn( "\n", "/// PARAMS:", syntaxEditor.Caret.CharacterColumn - 3 ) );

                        foreach ( AstNode arg in arguments )
                        {
                            if ( arg is VariableDeclarationStatement )
                            {
                                VariableDeclarationStatement var = arg as VariableDeclarationStatement;
                                Identifier name = var.Name;
                                if ( name != null )
                                {
                                    postString.Append( BuildTextForColumn( "\n", "///    ", syntaxEditor.Caret.CharacterColumn - 3 ) );
                                    postString.Append( name.Text );
                                    postString.Append( " - " );
                                }
                            }
                        }
                    }

                    if ( returns != null )
                    {
                        postString.Append( BuildTextForColumn( "\n", "/// RETURNS:", syntaxEditor.Caret.CharacterColumn - 3 ) );
                        postString.Append( BuildTextForColumn( "\n", "///    ", syntaxEditor.Caret.CharacterColumn - 3 ) );
                    }

                    syntaxEditor.SelectedView.InsertSurroundingTextSafe( preString, postString.ToString() );
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Attempts to insert a new purpose comment line at the current caret position
        /// </summary>
        /// <param name="syntaxEditor"></param>
        /// <returns></returns>
        private bool InsertPurposeCommentLine( SyntaxEditor syntaxEditor )
        {
            IToken prevToken = GetPurposeComment( syntaxEditor, syntaxEditor.SelectedView.CurrentDocumentLine.StartOffset - 1 );
            if ( prevToken != null )
            {
                int prevLineIndex = syntaxEditor.Document.Lines.IndexOf( prevToken.StartOffset );
                DocumentLine prevLine = syntaxEditor.Document.Lines[prevLineIndex];
                
                int col = prevToken.StartOffset - prevLine.StartOffset;
                string text = BuildTextForColumn( "///    ", string.Empty, col );
                syntaxEditor.SelectedView.InsertSurroundingTextSafe( text, string.Empty );

                return true;
            }

            return false;
        }

        /// <summary>
        /// Builds a string that starts with prefix, followed by the specified number of spaces, and ends with postfix.
        /// </summary>
        /// <param name="prefix">The start of the <c>string</c>.</param>
        /// <param name="postfix">The end of the <c>string</c>.</param>
        /// <param name="col">The current column which tells us how many spaces to add.</param>
        /// <returns>The formatting <c>string</c>.</returns>
        private string BuildTextForColumn( string prefix, string postfix, int col )
        {
            StringBuilder text = new StringBuilder( prefix );

            int numTabs = col / 4;
            for ( int i = 0; i < numTabs; ++i )
            {
                text.Append( '\t' );
            }

            int numSpaces = col % 4;
            for ( int i = 0; i < numSpaces; ++i )
            {
                text.Append( ' ' );
            }
            
            text.Append( postfix );

            return text.ToString();
        }

        /// <summary>
        /// Because we're building <see cref="CaseStatementBlock"/>s with fallthru's as a hierarchy where the parent's
        /// <see cref="TextRange"/>s doesn't include any child <see cref="CaseStatementBlock"/>s, we have to search the children
        /// manually to find the startOffset.
        /// </summary>
        /// <param name="node">The <see cref="AstNode"/> to search.</param>
        /// <param name="startOffset">The offset to search for.</param>
        /// <returns>The <see cref="AstNode"/> found, or null if none found.</returns>
        private AstNode GetAstNodeInCaseStatementBlock( AstNode node, int startOffset )
        {
            if ( node.StartOffset == startOffset )
            {
                return node;
            }

            IAstNodeList nodes = node.ChildNodes;
            foreach ( AstNode n in nodes )
            {
                AstNode foundNode = n.FindNodeRecursive( startOffset ) as AstNode;
                if ( foundNode != null )
                {
                    return foundNode;
                }

                if ( n.NodeType == SanScriptNodeType.CaseBlockStatement )
                {
                    foundNode = GetAstNodeInCaseStatementBlock( n, startOffset );
                    if ( foundNode != null )
                    {
                        return foundNode;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Retrieves the filename contained in the <see cref="UsingStatement"/> AstNode.
        /// </summary>
        /// <param name="usingStmt">The <see cref="UsingStatement"/> containing the filename.</param>
        /// <param name="sourceDirectory">The directory of the file containing the USING statement.</param>
        /// <returns>The fully-qualified path name.</returns>
        private string GetUsingFilename( UsingStatement usingStmt, string sourceDirectory )
        {
            AstNode fileNameNode = usingStmt.FileName;
            if ((fileNameNode == null) || (fileNameNode.NodeType != SanScriptNodeType.StringExpression))
            {
                return null;
            }

            string value = ((StringExpression)fileNameNode).StringValue;
            return !string.IsNullOrEmpty(value) ? SanScriptProjectResolver.ParserPlugin.GetFullPathName(value, sourceDirectory) : null;
        }
        #endregion
    }
}
