//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptEditorSettings.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2019-2021. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace SanScriptParser
{
    using ActiproSoftware.SyntaxEditor;
    using System.Linq;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;
    using System.Xml;
    using System.Xml.Serialization;
    using ragScriptEditorShared;
    using RSG.Configuration;
    using RSG.Platform;

    public class SanScriptCompilingSettings : CompilingSettings
    {
        public SanScriptCompilingSettings()
            : base( "SanScript" )
        {
            m_includeFileOpenFileDialogFilter = "Script Header Files (*.sch)|*.sch";
            m_compilerExecutableOpenFileDialogFilter = "Executable Files (*.exe;*.bat)|*.exe;*.bat|All Files (*.*)|*.*";
        }

        #region Variables
        private bool m_debugParser;
        private bool m_dumpThreadState;
        private bool m_displayDisassembly;
        private bool m_hsmEnabled;
        private bool m_outputDependencyFile;
        private bool m_outputNativeFile;
        private bool m_warningsAsErrors;

        private string m_globals;
//         private string m_custom;

        private string m_dependencyFile;
        private string m_nativeFile;
        #endregion

        #region Properties
        public bool DebugParser
        {
            get
            {
                return m_debugParser;
            }
            set
            {
                m_debugParser = value;
            }
        }

        public bool DumpThreadState
        {
            get
            {
                return m_dumpThreadState;
            }
            set
            {
                m_dumpThreadState = value;
            }
        }

        public bool DisplayDisassembly
        {
            get
            {
                return m_displayDisassembly;
            }
            set
            {
                m_displayDisassembly = value;
            }
        }

        public bool HsmEnabled
        {
            get
            {
                return m_hsmEnabled;
            }
            set
            {
                m_hsmEnabled = value;
            }
        }

        public bool OutputDependencyFile
        {
            get
            {
                return m_outputDependencyFile;
            }
            set
            {
                m_outputDependencyFile = value;
            }
        }

        public bool OutputNativeFile
        {
            get
            {
                return m_outputNativeFile;
            }
            set
            {
                m_outputNativeFile = value;
            }
        }

        public bool WarningsAsErrors
        {
            get
            {
                return m_warningsAsErrors;
            }
            set
            {
                m_warningsAsErrors = value;
            }
        }

        public string Globals
        {
            get
            {
                return m_globals;
            }
            set
            {
                m_globals = value;
            }
        }

//         public string Custom
//         {
//             get
//             {
//                 return m_custom;
//             }
//             set
//             {
//                 m_custom = value;
//             }
//         }

        public string DependencyFile
        {
            get
            {
                return m_dependencyFile;
            }
            set
            {
                m_dependencyFile = value;
            }
        }

        public string NativeFile
        {
            get
            {
                return m_nativeFile;
            }
            set
            {
                m_nativeFile = value;
            }
        }
        #endregion

        #region Overrides
        /// <inheritdoc />
        public override string GetResourcePlatformExtension(Platform platform)
        {
            return FileType.Script.GetPlatformExtension(platform);
        }

        public override string BuildCompileScriptCommand(string scriptFilename, Platform? currentPlatform)
        {
            StringBuilder cmd = new StringBuilder();

            IEnvironment env = ApplicationSettings.Environment;
            env.Push();
            try
            {
                this.Import(env, currentPlatform);

                string text = ApplicationSettings.GetAbsolutePath(this.CompilerExecutable.Trim());
                if (text.Length > 0)
                {
                    cmd.Append(text);
                    cmd.Append(" ");
                    cmd.Append(ApplicationSettings.BuildPathForCommandLine(scriptFilename));
                }

                text = (ApplicationSettings.Environment?.Subst(this.Custom) ?? this.Custom).Trim();
                if (text.Length > 0)
                {
                    cmd.Append(" ");
                    cmd.Append(text);
                }

                text = ApplicationSettings.BuildPathForCommandLine(this.XmlIncludeFile);
                if (text.Length > 0)
                {
                    cmd.Append(" -include ");
                    cmd.Append(text);
                }

                text = ApplicationSettings.BuildPathForCommandLine(this.IncludePath);
                if (text.Length > 0)
                {
                    cmd.Append(" -ipath ");
                    cmd.Append(text);
                }

                text = ApplicationSettings.GetAbsolutePath(OutputDirectory.Trim());
                if (text.Length > 0)
                {
                    cmd.Append(" -output ");

                    if (scriptFilename == "%1")
                    {
                        cmd.Append(ApplicationSettings.BuildPathForCommandLine(BuildOutputFilename("%~p1%~nx1")));
                    }
                    else
                    {
                        cmd.Append(ApplicationSettings.BuildPathForCommandLine(BuildOutputFilename(scriptFilename)));
                    }
                }

                text = this.Globals.Trim();
                if (text.Length > 0)
                {
                    cmd.Append(" -globals ");
                    cmd.Append(ApplicationSettings.BuildPathForCommandLine(BuildGlobalsFilename(scriptFilename)));
                }

                if (this.DebugParser)
                {
                    cmd.Append(" -debugparser");
                }

                if (this.DumpThreadState)
                {
                    cmd.Append(" -dump");
                }

                if (this.DisplayDisassembly)
                {
                    cmd.Append(" -dis");
                }

                if (this.HsmEnabled)
                {
                    cmd.Append(" -hsm");
                }

                if (this.OutputDependencyFile)
                {
                    cmd.Append(" -depfile ");

                    text = this.DependencyFile.Trim();
                    if (text != string.Empty)
                    {
                        cmd.Append(ApplicationSettings.BuildPathForCommandLine(BuildDependencyFilename(scriptFilename)));
                    }
                }

                if (this.OutputNativeFile)
                {
                    cmd.Append(" -native ");

                    text = this.NativeFile.Trim();
                    if (text != string.Empty)
                    {
                        // GTL: A bit of a hack--remove the .native.txt from the extension when passing to the executable, since the executable adds it for us (to avoid .native.txt.native.txt)
                        cmd.Append(ApplicationSettings.BuildPathForCommandLine(BuildNativeFilename(scriptFilename).Replace(".native.txt", "")));
                    }
                }

                if (this.WarningsAsErrors)
                {
                    cmd.Append(" -werror");
                }

                // do a replace on everything
                return ApplicationSettings.DereferenceFileArguments(cmd.ToString().Trim(), scriptFilename);
            }
            finally
            {
                env.Pop();
            }
        }

        public override string ResourceCompileScriptCommand(string scoFilename, string outputFilename, Platform currentPlatform)
        {
            StringBuilder cmd = new StringBuilder();

            string resourceExecutable = ApplicationSettings.GetAbsolutePath(this.ResourceExecutable.Trim());
            if (String.IsNullOrEmpty(resourceExecutable) == false)
            {
                cmd.Append(resourceExecutable);
                cmd.Append(" ");
                cmd.Append(ApplicationSettings.BuildPathForCommandLine(scoFilename));
                cmd.Append(" ");
                cmd.Append(ApplicationSettings.BuildPathForCommandLine(outputFilename));
            }

            // do a replace on everything
            return cmd.ToString();
        }

        public override IEnumerable<string> BuildOutputFilenames(string scriptFilename, Platform platform)
        {
            // This basically changes the path of the file and puts it in the OutputDirectory or the project's directory.
            string filename = BuildOutputFilename( scriptFilename );

            // object file
            yield return Path.ChangeExtension(filename, ".sco");

            // debugging file
            yield return Path.ChangeExtension(filename, ".scd");

            if (platform != Platform.Independent && String.IsNullOrEmpty(this.ResourceExecutable) == false)
            {
                //If the resource executable has been specified, add dependencies on the resource files.
                string outputDirectory = Path.GetDirectoryName(filename);
                string fileName = Path.GetFileNameWithoutExtension(filename);
                string platformOutputFilename = Path.Combine(outputDirectory, platform.ToString(), $"{fileName}{this.GetResourcePlatformExtension(platform)}");
                yield return platformOutputFilename;
            }

            // globals file
            yield return Path.ChangeExtension(filename, ".global_block.txt");

            // dependency file
            if ( this.OutputDependencyFile )
            {
                yield return BuildDependencyFilename(scriptFilename);
            }

            // native file
            if ( this.OutputNativeFile )
            {
                yield return BuildNativeFilename(scriptFilename);
            }
        }

        public override SettingsBase Clone()
        {
            SanScriptCompilingSettings c = new SanScriptCompilingSettings();
            c.Copy( this );

            return c;
        }

        public override void Copy( SettingsBase t )
        {
            if ( t is SanScriptCompilingSettings )
            {
                Copy( t as SanScriptCompilingSettings );
            }
            else if ( t is CompilingSettings )
            {
                base.Copy( t as CompilingSettings );
            }
        }

        public override void Copy( CompilingSettings c )
        {
            if ( c is SanScriptCompilingSettings )
            {
                Copy( c as SanScriptCompilingSettings );
            }
            else
            {
                base.Copy( c );
            }
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new SanScriptCompilingSettingsControl();
        }

        public override bool Equals( object obj )
        {
            SanScriptCompilingSettings c = obj as SanScriptCompilingSettings;
            if ( (object)c == null )
            {
                return false;
            }

            return (this.Custom == c.Custom)
                && (this.DebugParser == c.DebugParser)
                && (this.DependencyFile == c.DependencyFile)
                && (this.DisplayDisassembly == c.DisplayDisassembly)
                && (this.DumpThreadState == c.DumpThreadState)
                && (this.Globals == c.Globals)
                && (this.HsmEnabled == c.HsmEnabled)
                && (this.WarningsAsErrors == c.WarningsAsErrors)
                && (this.NativeFile == c.NativeFile)
                && (this.OutputDependencyFile == c.OutputDependencyFile)
                && (this.OutputNativeFile == c.OutputNativeFile)
                && base.Equals( c );
        }

        public override bool Equals( CompilingSettings c )
        {
            if ( (object)c == null )
            {
                return false;
            }

            if ( c is SanScriptCompilingSettings )
            {
                return Equals( c as SanScriptCompilingSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            StringBuilder s = new StringBuilder();
            s.Append( this.Custom );
            s.Append( this.DebugParser );
            s.Append( this.DependencyFile );
            s.Append( this.DisplayDisassembly );
            s.Append( this.DumpThreadState );
            s.Append( this.Globals );
            s.Append( this.HsmEnabled );
            s.Append( this.WarningsAsErrors );
            foreach (string path in this.IncludePaths)
            {
                s.Append(path);
            }
            s.Append( this.Language );
            s.Append( this.Name );
            s.Append( this.NativeFile );
            s.Append( this.OutputDependencyFile );
            s.Append( this.OutputDirectory );
            s.Append( this.OutputNativeFile );
            s.Append( this.CompilerExecutable );
            s.Append( this.ResourceExecutable );
            s.Append( this.XmlIncludeFile );
            return s.ToString().GetHashCode();
        }

        [XmlIgnore]
        public override bool LanguageIsCompilable
        {
            get
            {
                return true;
            }
        }

        public override void Reset()
        {
            base.Reset();

            this.CompilerExecutable = String.Empty;

//             this.Custom = string.Empty;
            this.DebugParser = false;
            this.DependencyFile = "%~n1.d";
            this.DisplayDisassembly = false;
            this.DumpThreadState = false;
            this.Globals = string.Empty;
            this.HsmEnabled = true;
            this.NativeFile = "%~n1.native.txt";
            this.OutputDependencyFile = true;
            this.OutputNativeFile = true;
            this.WarningsAsErrors = false;
        }

        public override void SetValue( string key, string value )
        {
            base.SetValue( key, value );

            switch ( key )
            {
                case "debugparser":
                    this.DebugParser = bool.Parse( value );
                    break;
                case "dump":
                    this.DumpThreadState = bool.Parse( value );
                    break;
                case "dis":
                    this.DisplayDisassembly = bool.Parse( value );
                    break;
                case "werror":
                    this.WarningsAsErrors = bool.Parse( value );
                    break;
                case "globals":
                    this.Globals = value;
                    break;
                case "custom":
                    this.Custom = value;
                    break;
            }
        }
        #endregion

        #region Public Functions
        public bool Equals( SanScriptCompilingSettings c )
        {
            if ( (object)c == null )
            {
                return false;
            }

            return (this.Custom == c.Custom)
                && (this.DebugParser == c.DebugParser)
                && (this.DependencyFile == c.DependencyFile)
                && (this.DisplayDisassembly == c.DisplayDisassembly)
                && (this.DumpThreadState == c.DumpThreadState)
                && (this.Globals == c.Globals)
                && (this.HsmEnabled == c.HsmEnabled)
                && (this.WarningsAsErrors == c.WarningsAsErrors)
                && (this.NativeFile == c.NativeFile)
                && (this.OutputDependencyFile == c.OutputDependencyFile)
                && (this.OutputNativeFile == c.OutputNativeFile)
                && base.Equals( c );
        }

        public void Copy( SanScriptCompilingSettings c )
        {
            if ( c == null )
            {
                return;
            }

            base.Copy( c );

//             this.Custom = c.Custom;
            this.DebugParser = c.DebugParser;
            this.DependencyFile = c.DependencyFile;
            this.DisplayDisassembly = c.DisplayDisassembly;
            this.DumpThreadState = c.DumpThreadState;
            this.Globals = c.Globals;
            this.HsmEnabled = c.HsmEnabled;
            this.NativeFile = c.NativeFile;
            this.OutputDependencyFile = c.OutputDependencyFile;
            this.OutputNativeFile = c.OutputNativeFile;
            this.WarningsAsErrors = c.WarningsAsErrors;
        }
        #endregion

        #region Private Functions
        /// <summary>
        ///
        /// </summary>
        /// <param name="fullPath"></param>
        /// <param name="projectFilename"></param>
        /// <returns></returns>
        public string BuildGlobalsFilename( string fullPath )
        {
            return ApplicationSettings.BuildAndDereferenceFilenameInOutputDirectory( ApplicationSettings.Environment.Subst(this.Globals),
                ApplicationSettings.GetAbsolutePath(this.OutputDirectory), fullPath);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="fullPath"></param>
        /// <param name="projectFilename"></param>
        /// <returns></returns>
        public string BuildDependencyFilename( string fullPath )
        {
            return ApplicationSettings.BuildAndDereferenceFilenameInOutputDirectory(ApplicationSettings.Environment?.Subst(this.DependencyFile) ?? this.DependencyFile,
                ApplicationSettings.GetAbsolutePath(this.OutputDirectory), fullPath);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="fullPath"></param>
        /// <param name="projectFilename"></param>
        /// <returns></returns>
        public string BuildNativeFilename( string fullPath )
        {
			return ApplicationSettings.BuildAndDereferenceFilenameInOutputDirectory(this.NativeFile, ApplicationSettings.GetAbsolutePath(this.OutputDirectory), fullPath);
        }

        /// <summary>
        /// Checks whether the -noconfig commandline parameter was passed in.
        /// </summary>
        /// <returns></returns>
        private bool UseToolsConfig()
        {
            bool useConfig = true;

            string[] args = Environment.GetCommandLineArgs();
            foreach (String str in args)
            {
                if (str == "-noconfig")
                {
                    useConfig = false;
                    break;
                }
            }
            return useConfig;
        }
        #endregion
    }

    public class SanScriptLanguageSettings : LanguageSettings
    {
        public SanScriptLanguageSettings()
            : base( "SanScript" )
        {

        }

        #region Variables
        private string m_languageHelpFile;
        private string m_referenceHelpFile;
        #endregion

        #region Properties
        [XmlIgnore]
        public string LanguageHelpFile
        {
            get
            {
                if ( String.IsNullOrEmpty( m_languageHelpFile ) )
                {
                    return m_languageHelpFile;
                }

                ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( m_languageHelpFile );
                if ( psInfo != null )
                {
                    if ( m_languageHelpFile.StartsWith( "\"" ) )
                    {
                        return "\"" + psInfo.FileName + "\" " + psInfo.Arguments;
                    }

                    return psInfo.FileName + " " + psInfo.Arguments;
                }

                return null;
            }
            set
            {
                m_languageHelpFile = value;
            }
        }

        [XmlElement( "LanguageHelpFile" )]
        public string XmlLanguageHelpFile
        {
            get
            {
                return m_languageHelpFile;
            }
            set
            {
                m_languageHelpFile = value;
            }
        }

        [XmlIgnore]
        public string ReferenceHelpFile
        {
            get
            {
                if ( String.IsNullOrEmpty( m_referenceHelpFile ) )
                {
                    return m_referenceHelpFile;
                }

                ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( m_referenceHelpFile );
                if ( psInfo != null )
                {
                    if ( m_referenceHelpFile.StartsWith( "\"" ) )
                    {
                        return "\"" + psInfo.FileName + "\" " + psInfo.Arguments;
                    }

                    return psInfo.FileName + " " + psInfo.Arguments;
                }

                return null;
            }
            set
            {
                m_referenceHelpFile = value;
            }
        }

        [XmlElement( "ReferenceHelpFile" )]
        public string XmlReferenceHelpFile
        {
            get
            {
                return m_referenceHelpFile;
            }
            set
            {
                m_referenceHelpFile = value;
            }
        }
        #endregion

        #region Overrides
        public override SettingsBase Clone()
        {
            SanScriptLanguageSettings l = new SanScriptLanguageSettings();
            l.Copy( this );

            return l;
        }

        public override void Copy( SettingsBase t )
        {
            if ( t is SanScriptLanguageSettings )
            {
                Copy( t as SanScriptLanguageSettings );
            }
            else if ( t is LanguageSettings )
            {
                base.Copy( t as LanguageSettings );
            }
        }

        public override void Copy( LanguageSettings t )
        {
            if ( t is SanScriptLanguageSettings )
            {
                Copy( t as SanScriptLanguageSettings );
            }
            else
            {
                base.Copy( t );
            }
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new SanScriptLanguageSettingsControl();
        }

        public override bool Equals( object obj )
        {
            SanScriptLanguageSettings l = obj as SanScriptLanguageSettings;
            if ( (object)l == null )
            {
                return false;
            }

            return (this.XmlLanguageHelpFile == l.XmlLanguageHelpFile)
                && (this.XmlReferenceHelpFile == l.XmlReferenceHelpFile)
                && base.Equals( l );
        }

        public override bool Equals( LanguageSettings l )
        {
            if ( (object)l == null )
            {
                return false;
            }

            if ( l is SanScriptLanguageSettings )
            {
                return Equals( l as SanScriptLanguageSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            StringBuilder s = new StringBuilder();
            s.Append( this.CacheDirectory );
            s.Append( this.CodeSnippetPath );
            s.Append( this.Language );
            s.Append( this.Name );
            s.Append( this.XmlLanguageHelpFile );
            s.Append( this.XmlReferenceHelpFile );
            return s.ToString().GetHashCode();
        }

        public override List<ToolStripMenuItem> GetHelpMenuItems()
        {
            List<ToolStripMenuItem> menuItems = new List<ToolStripMenuItem>();

            ToolStripMenuItem languageHelpMenuItem = new ToolStripMenuItem();
            languageHelpMenuItem.Name = "SanScriptLanguageHelpMenuItem";
            languageHelpMenuItem.Text = "SanScript Language Help";
            languageHelpMenuItem.Click += new EventHandler( languageHelpMenuItem_Click );

            ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( m_languageHelpFile );
            languageHelpMenuItem.Enabled = (psInfo != null) ? File.Exists( psInfo.FileName ) : false;

            ToolStripMenuItem referenceHelpMenuItem = new ToolStripMenuItem();
            referenceHelpMenuItem.Name = "SanScriptReferenceHelpMenuItem";
            referenceHelpMenuItem.Text = "SanScript Reference Help";
            referenceHelpMenuItem.Click += new EventHandler( referenceHelpMenuItem_Click );

            psInfo = ApplicationSettings.BuildProcessInfoFromCommand( m_referenceHelpFile );
            referenceHelpMenuItem.Enabled = (psInfo != null) ? File.Exists( psInfo.FileName ) : false;

            menuItems.Add( languageHelpMenuItem );
            menuItems.Add( referenceHelpMenuItem );

            return menuItems;
        }

        public override void Reset()
        {
            base.Reset();

            this.LanguageHelpFile = @"chm\script.chm";
            this.ReferenceHelpFile = string.Empty;
        }

        public override void Upgrade( LanguageTabSettings l )
        {
            base.Upgrade( l );

            this.LanguageHelpFile = l.HelpLanguageFileRaw;
            this.ReferenceHelpFile = l.HelpReferenceFileRaw;
        }

        public override bool Validate( out string error )
        {
            if ( !base.Validate( out error ) )
            {
                return false;
            }

            if ( !String.IsNullOrEmpty( this.LanguageHelpFile ) )
            {
                ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( m_languageHelpFile );
                if ( psInfo == null )
                {
                    error = "Could not create ProcessStartInfo for Language Help File '" + m_languageHelpFile + "'.";
                    return false;
                }

                if ( !File.Exists( psInfo.FileName ) )
                {
                    error = "Language Help File '" + psInfo.FileName + "' does not exist.";
                    return false;
                }
            }

            if ( !String.IsNullOrEmpty( this.ReferenceHelpFile ) )
            {
                ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( m_referenceHelpFile );
                if ( psInfo == null )
                {
                    error = "Could not create ProcessStartInfo for Reference Help File '" + m_referenceHelpFile + "'.";
                    return false;
                }

                if ( !File.Exists( psInfo.FileName ) )
                {
                    error = "Reference Help File '" + psInfo.FileName + "' does not exist.";
                    return false;
                }
            }

            error = null;
            return true;
        }
        #endregion

        #region Public Functions
        public void Copy( SanScriptLanguageSettings l )
        {
            if ( l == null )
            {
                return;
            }

            base.Copy( l );

            this.LanguageHelpFile = l.XmlLanguageHelpFile;
            this.ReferenceHelpFile = l.XmlReferenceHelpFile;
        }

        public bool Equals( SanScriptLanguageSettings l )
        {
            if ( (object)l == null )
            {
                return false;
            }

            return (this.XmlLanguageHelpFile == l.XmlLanguageHelpFile)
                && (this.XmlReferenceHelpFile == l.XmlReferenceHelpFile)
                && base.Equals( l );
        }
        #endregion

        #region Event Handlers
        private void languageHelpMenuItem_Click( object sender, EventArgs e )
        {
            if ( !String.IsNullOrEmpty( this.LanguageHelpFile ) )
            {
                try
                {
                    ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( this.LanguageHelpFile );
                    if ( (psInfo != null) && File.Exists( psInfo.FileName ) )
                    {
                        Process.Start( psInfo );
                    }
                }
                catch ( Exception ex )
                {
                    ApplicationSettings.ShowMessage( null, String.Format( "Could not load SanScript Language Help File.\n\n{0}", ex.Message ),
                        "Help File Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, DialogResult.OK );
                }
            }
        }

        private void referenceHelpMenuItem_Click( object sender, EventArgs e )
        {
            if ( !String.IsNullOrEmpty( this.ReferenceHelpFile ) )
            {
                try
                {
                    ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( this.ReferenceHelpFile );
                    if ( (psInfo != null) && File.Exists( psInfo.FileName ) )
                    {
                        Process.Start( psInfo );
                    }
                }
                catch ( Exception ex )
                {
                    ApplicationSettings.ShowMessage( null, String.Format( "Could not load SanScript Reference Help File.\n\n{0}", ex.Message ),
                        "Help File Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, DialogResult.OK );
                }
            }
        }
        #endregion
    }


    public class SanScriptLanguageDefaultEditorSettings : LanguageDefaultEditorSettingsBase
    {
        public SanScriptLanguageDefaultEditorSettings()
            : base( "SanScript", SanScriptLanguageDefaultEditorSettings.SettingsVersion )
        {

        }

        #region Constants
        public const float SettingsVersion = 1.0f;
        #endregion

        #region Variables
        private List<NativeItem> m_specialIdentifiers = new List<NativeItem>();
        #endregion

        #region Properties
        [XmlIgnore]
        public List<NativeItem> SpecialIdentifiers
        {
            get
            {
                return m_specialIdentifiers;
            }
            set
            {
                m_specialIdentifiers = value;
            }
        }

        [XmlArray( "SpecialIdentifiers" )]
        public NativeItem[] XmlSpecialIdentifiers
        {
            get
            {
                return m_specialIdentifiers.ToArray();
            }
            set
            {
                m_specialIdentifiers.Clear();

                if ( value != null )
                {
                    m_specialIdentifiers.AddRange( value );
                }
            }
        }
        #endregion

        #region Overrides
        public override void Copy( EditorSettingsBase settings )
        {
            base.Copy( settings );

            if ( settings is SanScriptLanguageDefaultEditorSettings )
            {
                SanScriptLanguageDefaultEditorSettings sanScriptSettings = settings as SanScriptLanguageDefaultEditorSettings;

                m_specialIdentifiers.Clear();
                foreach ( NativeItem item in sanScriptSettings.SpecialIdentifiers )
                {
                    m_specialIdentifiers.Add( item );
                }
            }
        }

        public override CompilingSettings CreateCompilingTabSettings()
        {
            return new SanScriptCompilingSettings();
        }

        public override LanguageSettings CreateLanguageTabSettings()
        {
            return new SanScriptLanguageSettings();
        }

        public override Type GetCompilingSettingsDerivedType()
        {
            return typeof( SanScriptCompilingSettings );
        }

        public override Type GetLanguageSettingsDerivedType()
        {
            return typeof( SanScriptLanguageSettings );
        }
        #endregion
    }

    #region NativeItems
    public class NativeItem
    {
        public NativeItem()
        {

        }

        public NativeItem( string name, DeclarationType declareType )
        {
            m_Name = name;
            m_declareType = declareType;
            m_imageIndex = (int)ActiproSoftware.Products.SyntaxEditor.IconResource.Keyword;
        }

        #region Enum
        public enum DeclarationType
        {
            Local,
            User,
            Native,
            NativeType
        }
        #endregion

        #region Variables
        protected string m_Name;
        protected string m_FileName;
        protected int m_FileLineNum;
        protected DeclarationType m_declareType = DeclarationType.Local;
        protected Comment m_Comment;
        protected int m_imageIndex = -1;
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return m_Name;
            }
        }

        public string FileName
        {
            get
            {
                return m_FileName;
            }
        }

        public int FileLineNumber
        {
            get
            {
                return m_FileLineNum;
            }
        }

        public DeclarationType DeclareType
        {
            get
            {
                return m_declareType;
            }
        }

        public Comment Comment
        {
            get
            {
                return m_Comment;
            }
        }

        public int ImageIndex
        {
            get
            {
                return m_imageIndex;
            }
        }

        public virtual string DisplayText
        {
            get
            {
                return m_Name;
            }
        }

        public virtual string MarkupText
        {
            get
            {
                StringBuilder text = new StringBuilder();

                switch ( this.DeclareType )
                {
                    case DeclarationType.Local:
                        text.Append( "<span style=\"color:local;\">" );
                        break;
                    case DeclarationType.User:
                        text.Append( "<span style=\"color:user;\">" );
                        break;
                    case DeclarationType.Native:
                        text.Append( "<span style=\"color:native;\">" );
                        break;
                    case DeclarationType.NativeType:
                        text.Append( "<span style=\"color:nativetype;\">" );
                        break;
                }

                text.Append( m_Name );
                text.Append( "</span>" );

                return text.ToString();
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            NativeItem item = obj as NativeItem;
            if ( item == null )
            {
                return false;
            }

            return this.GetHashCode() == item.GetHashCode();
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder hashBuilder = new System.Text.StringBuilder( m_Name );

            if ( m_FileName != null )
            {
                hashBuilder.Append( m_FileName );
            }

            string hashstring = hashBuilder.ToString();
            return hashstring.GetHashCode();
        }
        #endregion

        #region Virtual Functions
        public virtual NativeItem Clone()
        {
            NativeItem item = new NativeItem();
            item.Copy( this );

            return item;
        }

        public virtual void Copy( NativeItem item )
        {
            if ( item == null )
            {
                return;
            }

            m_Comment = item.Comment;
            m_declareType = item.DeclareType;
            m_FileLineNum = item.FileLineNumber;
            m_FileName = item.FileName;
            m_imageIndex = item.ImageIndex;
            m_Name = item.Name;
        }

        public virtual void GetInfoTipStrings( out string tip, out string infoText, out string purpose )
        {
            System.Text.StringBuilder tipBuilder = new System.Text.StringBuilder( "<b>" );
            tipBuilder.Append( m_Name );
            tipBuilder.Append( @"</b><br/>" );
            tip = tipBuilder.ToString();
            purpose = "";
            if ( (m_Comment != null) && (m_Comment.m_Purpose != null) && (m_Comment.m_Purpose != string.Empty) )
            {
                purpose = m_Comment.m_Purpose;
                tipBuilder.Append( purpose );
                infoText = tipBuilder.ToString();
            }
            else
            {
                infoText = tip;
            }
        }

        public virtual void ParseXml( XmlTextReader reader )
        {
            if ( !reader.IsEmptyElement )
            {
                if ( reader.Name == "Comments" )
                {
                    m_Comment = new Comment();
                    m_Comment.ParseXml( reader );
                }
                else if ( reader.Name == "Source" )
                {
                    m_FileName = reader["file"];
                    m_FileLineNum = (reader["line"] != null) ? Convert.ToInt32( reader["line"] ) : 0;
                }
            }
        }

        public virtual void SaveXml( XmlTextWriter writer )
        {
            if ( m_Comment != null )
            {
                writer.WriteStartElement( "Comments" );
                m_Comment.SaveXml( writer );
                writer.WriteEndElement();
            }

            if ( m_FileName != null )
            {
                writer.WriteStartElement( "Source" );
                writer.WriteAttributeString( "file", m_FileName );
                writer.WriteAttributeString( "line", m_FileLineNum.ToString() );
                writer.WriteEndElement();
            }
        }
        #endregion

        #region Static Functions
        public static string[] SplitByBrTag( string s )
        {
            string tmp = s.Replace( "<br/>", "\n" );
            string[] split = tmp.Split( '\n' );

            for ( int i = 0; i < split.Length; ++i )
            {
                split[i] = split[i].Trim();
            }

            return split;
        }
        #endregion
    }

    public class StructItem : NativeItem
    {
        public StructItem()
        {
            m_imageIndex = (int)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicStructure;
        }

        public StructItem( string name )
            : base( name, NativeItem.DeclarationType.User )
        {
            m_imageIndex = (int)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicStructure;
        }

        #region Variables
        private string m_Contents;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the declarations for the member variables
        /// </summary>
        public string[] Contents
        {
            get
            {
                if ( m_Contents != null )
                {
                    return SplitByBrTag( m_Contents );
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the names of the member variables
        /// </summary>
        public string[] ContentNames
        {
            get
            {
                string[] contents = this.Contents;
                if ( contents == null )
                {
                    return null;
                }

                for ( int i = 0; i < contents.Length; ++i )
                {
                    StringBuilder text = new StringBuilder();

                    string[] split = contents[i].Split( ' ' );
                    if ( split.Length > 1 )
                    {
                        contents[i] = split[1];
                    }
                }

                return contents;
            }
        }

        /// <summary>
        /// Gets the data types of the membe variables
        /// </summary>
        public string[] ContentTypes
        {
            get
            {
                string[] contents = this.Contents;
                if ( contents == null )
                {
                    return null;
                }

                for ( int i = 0; i < contents.Length; ++i )
                {
                    StringBuilder text = new StringBuilder();

                    string[] split = contents[i].Split( ' ' );
                    if ( split.Length > 0 )
                    {
                        contents[i] = split[0];
                    }
                }

                return contents;
            }
        }

        public string[] MarkupContents
        {
            get
            {
                string[] types = this.ContentTypes;
                string[] names = this.ContentNames;

                string[] markupContents = new string[names.Length];
                for ( int i = 0; i < markupContents.Length; ++i )
                {
                    StringBuilder text = new StringBuilder();

                    if ( CompilationUnit.IsNativeType( types[i] ) )
                    {
                        text.Append( "<span style=\"color:nativetype;\">" );
                    }
                    else
                    {
                        text.Append( "<span style=\"color:local;\">" );
                    }

                    text.Append( types[i] );
                    text.Append( " </span>" );

                    text.Append( "<span style=\"color:text;\">" );
                    text.Append( names[i] );
                    text.Append( "</span>" );

                    markupContents[i] = text.ToString();
                }

                return markupContents;
            }
        }
        #endregion

        #region NativeItem Overrides
        public override string DisplayText
        {
            get
            {
                StringBuilder text = new StringBuilder();

                text.Append( "STRUCT " );

                text.Append( base.DisplayText );

                if ( this.Comment != null )
                {
                    text.Append( "\n\n" );
                    text.Append( this.Comment.DisplayText );
                }

                return text.ToString();
            }
        }

        public override string MarkupText
        {
            get
            {
                StringBuilder text = new StringBuilder();

                text.Append( "<span style=\"color:reserved;\">STRUCT</span>" );

                if ( m_Name != null )
                {
                    text.Append( " " );
                    text.Append( base.MarkupText );
                }

                if ( this.Comment != null )
                {
                    text.Append( "<br/><br/>" );
                    text.Append( this.Comment.MarkupText );
                }

                return text.ToString();
            }
        }

        public override NativeItem Clone()
        {
            StructItem item = new StructItem();
            item.Copy( this );

            return item;
        }

        public override void Copy( NativeItem item )
        {
            base.Copy( item );

            if ( item is StructItem )
            {
                StructItem s = item as StructItem;

                m_Contents = s.m_Contents;
            }
        }

        public override void ParseXml( XmlTextReader reader )
        {
            string parentName = reader.Name;

            m_declareType = DeclarationType.User;

            m_Name = reader["name"];

            System.Text.StringBuilder contents = new System.Text.StringBuilder();

            if ( !reader.IsEmptyElement )
            {
                while ( reader.Read() )
                {
                    if ( !reader.IsStartElement() )
                    {
                        if ( reader.Name == parentName )
                        {
                            break;	// we're done
                        }
                    }
                    else if ( reader.Name == "Contents" )
                    {
                        contents.Append( reader.ReadString() );
                        contents.Append( "<br/>" );
                    }
                    else
                    {
                        base.ParseXml( reader );
                    }
                }
            }

            if ( contents.Length > 0 )
            {
                contents.Remove( contents.Length - 5, 5 );
            }

            m_Contents = contents.ToString();
        }

        public override void SaveXml( XmlTextWriter writer )
        {
            writer.WriteAttributeString( "name", m_Name );

            if ( m_Contents != string.Empty )
            {
                string[] contents = this.Contents;
                foreach ( string c in contents )
                {
                    writer.WriteStartElement( "Contents" );
                    writer.WriteString( c );
                    writer.WriteEndElement();
                }
            }

            base.SaveXml( writer );
        }
        #endregion
    }

    public class ConstOrEnumItem : NativeItem
    {
        public ConstOrEnumItem()
        {
            m_imageIndex = (int)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicConstant;
        }

        public ConstOrEnumItem( string type, string name, string theValue )
            : base( name, NativeItem.DeclarationType.User )
        {
            m_Type = type;
            m_Value = theValue;

            type = m_Type.ToUpper();
            if ( (type == "CONST_FLOAT") || (type == "CONST_INT") )
            {
                m_imageIndex = (int)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicConstant;
            }
            else
            {
                m_imageIndex = (int)ActiproSoftware.Products.SyntaxEditor.IconResource.EnumerationItem;
            }
        }

        #region Variables
        private string m_Value;
        private string m_Type;
        #endregion

        #region Properties
        public string Type
        {
            get
            {
                return m_Type;
            }
        }

        public string Value
        {
            get
            {
                return m_Value;
            }
        }
        #endregion

        #region NativeItem Overrides
        public override string DisplayText
        {
            get
            {
                StringBuilder text = new StringBuilder();

                text.Append( m_Type );

                text.Append( " " );
                text.Append( base.DisplayText );

                if ( (m_Value != null) && (m_Value != string.Empty) )
                {
                    text.Append( " " );
                    text.Append( m_Value );
                }

                if ( this.Comment != null )
                {
                    text.Append( "\n\n" );
                    text.Append( this.Comment.DisplayText );
                }

                return text.ToString();
            }
        }

        public override string MarkupText
        {
            get
            {
                StringBuilder text = new StringBuilder();

                string type = m_Type.ToUpper();
                if ( (type == "CONST_FLOAT") || (type == "CONST_INT") )
                {
                    text.Append( "<span style=\"color:nativetype;\">" );
                }
                else
                {
                    text.Append( "<span style=\"color:user;\">" );
                    type = m_Type;
                }

                text.Append( type );
                text.Append( "</span>" );

                text.Append( " " );
                text.Append( base.MarkupText );

                if ( (m_Value != null) && (m_Value != string.Empty) )
                {
                    text.Append( " " );

                    if ( Char.IsDigit( m_Value[0] ) )
                    {
                        text.Append( "<span style=\"color:number;\">" );
                    }
                    else
                    {
                        text.Append( "<span style=\"color:local;\">" );
                    }

                    text.Append( m_Value );
                    text.Append( "</span>" );
                }

                if ( this.Comment != null )
                {
                    text.Append( "<br/><br/>" );
                    text.Append( this.Comment.MarkupText );
                }

                return text.ToString();
            }
        }

        public override NativeItem Clone()
        {
            ConstOrEnumItem item = new ConstOrEnumItem();
            item.Copy( this );

            return item;
        }

        public override void Copy( NativeItem item )
        {
            base.Copy( item );

            if ( item is ConstOrEnumItem )
            {
                ConstOrEnumItem c = item as ConstOrEnumItem;

                m_Type = c.Type;
                m_Value = c.Value;
            }
        }

        public override void ParseXml( XmlTextReader reader )
        {
            string parentName = reader.Name;

            m_declareType = DeclarationType.User;

            m_Type = reader["type"];
            m_Name = reader["name"];
            m_Value = reader["value"];

            if ( !reader.IsEmptyElement )
            {
                while ( reader.Read() )
                {
                    if ( !reader.IsStartElement() )
                    {
                        if ( reader.Name == parentName )
                        {
                            break;	// we're done
                        }
                    }
                    else
                    {
                        base.ParseXml( reader );
                    }
                }
            }
        }

        public override void SaveXml( XmlTextWriter writer )
        {
            writer.WriteAttributeString( "type", m_Type );
            writer.WriteAttributeString( "name", m_Name );
            writer.WriteAttributeString( "value", m_Value );

            base.SaveXml( writer );
        }
        #endregion
    }

    public class NativeProcOrFunc : NativeItem
    {
        public NativeProcOrFunc()
        {
            m_imageIndex = (int)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicMethod;
        }

        public NativeProcOrFunc( string name, string args, string returnValue, NativeItem.DeclarationType declareType )
            : base( name, declareType )
        {
            m_Args = args;
            m_ReturnValue = returnValue;
            m_imageIndex = (int)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicMethod;
        }

        #region Variables
        public string m_Args;
        public string m_ReturnValue;
        #endregion

        #region Properties
        public string[] Arguments
        {
            get
            {
                if ( (m_Args != null) && (m_Args != string.Empty) )
                {
                    // FIXME: This won't work correctly if we have a VECTOR with a default value
                    string[] argsSplit = m_Args.Split( new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries );
                    for ( int i = 0; i < argsSplit.Length; ++i )
                    {
                        argsSplit[i] = argsSplit[i].Trim();
                    }

                    return argsSplit;
                }

                return null;
            }
        }

        public string[] ArgumentsMarkupText
        {
            get
            {
                string[] args = this.Arguments;
                if ( args != null )
                {
                    for ( int i = 0; i < args.Length; ++i )
                    {
                        string[] argSplit = args[i].Split( new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries );

                        if ( argSplit.Length > 1 )
                        {
                            StringBuilder text = new StringBuilder();
                            if ( CompilationUnit.IsNativeType( argSplit[0].ToUpper() ) )
                            {
                                text.Append( "<span style=\"color:nativetype;\">" );
                            }
                            else
                            {
                                text.Append( "<span style=\"color:local;\">" );
                            }

                            text.Append( argSplit[0] );
                            text.Append( "</span>" );

                            for ( int j = 1; j < argSplit.Length; ++j )
                            {
                                text.Append( " " );

                                if ( Char.IsDigit( argSplit[j][0] ) )
                                {
                                    text.Append( "<span style=\"color:number;\">" );
                                }
                                else if ( Char.IsPunctuation( argSplit[j][0] ) )
                                {
                                    text.Append( "<span style=\"color:text;\">" );
                                }
                                else
                                {
                                    text.Append( "<span style=\"color:local;\">" );
                                }

                                text.Append( argSplit[j] );
                                text.Append( "</span>" );
                            }

                            args[i] = text.ToString();
                        }
                    }
                }

                return args;
            }
        }

        public string ReturnValue
        {
            get
            {
                return m_ReturnValue;
            }
        }
        #endregion

        #region NativeItem Overrides
        public override string DisplayText
        {
            get
            {
                StringBuilder text = new StringBuilder();

                if ( m_ReturnValue != string.Empty )
                {
                    text.Append( "FUNC " );
                    text.Append( m_ReturnValue );
                    text.Append( " " );
                }
                else
                {
                    text.Append( "PROC " );
                }

                text.Append( base.DisplayText );

                text.Append( "(" );

                if ( (m_Args != null) && (m_Args != string.Empty) )
                {
                    text.Append( " " );
                    text.Append( m_Args );
                    text.Append( " " );
                }

                text.Append( ")" );

                if ( this.Comment != null )
                {
                    text.Append( "\n\n" );
                    text.Append( this.Comment.DisplayText );
                }

                return text.ToString();
            }
        }

        public override string MarkupText
        {
            get
            {
                StringBuilder text = new StringBuilder();

                if ( m_ReturnValue != string.Empty )
                {
                    text.Append( "<span style=\"color:reserved;\">FUNC </span>" );

                    if ( CompilationUnit.IsNativeType( m_ReturnValue ) )
                    {
                        text.Append( "<span style=\"color:nativetype;\">" );
                    }
                    else
                    {
                        text.Append( "<span style=\"color:local;\">" );
                    }

                    text.Append( m_ReturnValue );
                    text.Append( " </span>" );
                }
                else
                {
                    text.Append( "<span style=\"color:reserved;\">PROC </span>" );
                }

                text.Append( base.MarkupText );

                text.Append( "<span style=\"color:text;\">(" );

                string[] args = this.ArgumentsMarkupText;
                if ( (args != null) && (args.Length > 0) )
                {
                    text.Append( " " );

                    for ( int i = 0; i < args.Length; ++i )
                    {
                        if ( i > 0 )
                        {
                            text.Append( "<span style=\"color:text;\">, </span>" );
                        }

                        text.Append( args[i] );
                    }

                    text.Append( " " );
                }

                text.Append( ")</span>" );

                if ( this.Comment != null )
                {
                    text.Append( "<br/><br/>" );
                    text.Append( this.Comment.MarkupText );
                }

                return text.ToString();
            }
        }

        public override NativeItem Clone()
        {
            NativeProcOrFunc p = new NativeProcOrFunc();
            p.Copy( this );

            return p;
        }

        public override void Copy( NativeItem item )
        {
            base.Copy( item );

            if ( item is NativeProcOrFunc )
            {
                NativeProcOrFunc p = item as NativeProcOrFunc;

                m_Args = p.m_Args;
                m_ReturnValue = p.ReturnValue;
            }
        }

        public override void GetInfoTipStrings( out string tip, out string infoText, out string purpose )
        {
            string args = m_Args;
            args = args.Replace( "&", "&amp;" );

            System.Text.StringBuilder tipBuilder = new System.Text.StringBuilder( m_ReturnValue );
            tipBuilder.Append( @" <b>" );
            tipBuilder.Append( m_Name );
            tipBuilder.Append( @"</b>(" );
            tipBuilder.Append( args.Trim() );
            tipBuilder.Append( @")<br/>" );
            tip = tipBuilder.ToString();

            if ( (m_Comment != null) && (m_Comment.m_Purpose != null) && (m_Comment.m_Purpose != string.Empty) )
            {
                purpose = m_Comment.m_Purpose;
                tipBuilder.Append( purpose );
                infoText = tipBuilder.ToString();
            }
            else
            {
                purpose = "";
                infoText = tip;
            }
        }

        public override void ParseXml( XmlTextReader reader )
        {
            string parentName = reader.Name;

            m_declareType = DeclarationType.User;

            m_Name = reader["name"];
            m_Args = (reader["args"] != null) ? reader["args"] : string.Empty;

            string rtnType = reader["returnType"];
            if ( (rtnType != null) && (rtnType.ToLower() != "void") )
            {
                m_ReturnValue = rtnType;
            }
            else
            {
                m_ReturnValue = string.Empty;
            }

            if ( !reader.IsEmptyElement )
            {
                while ( reader.Read() )
                {
                    if ( !reader.IsStartElement() )
                    {
                        if ( reader.Name == parentName )
                        {
                            break;	// we're done
                        }
                    }
                    else
                    {
                        base.ParseXml( reader );
                    }
                }
            }
        }

        public override void SaveXml( XmlTextWriter writer )
        {
            writer.WriteAttributeString( "name", m_Name );
            writer.WriteAttributeString( "args", m_Args );
            writer.WriteAttributeString( "returnType", m_ReturnValue );

            base.SaveXml( writer );
        }
        #endregion
    }

    public class Comment
    {
        public Comment()
        {

        }

        public Comment( string purpose, string arguments, string returns, string notes )
        {
            m_Purpose = purpose;
            m_Params = arguments;
            m_Returns = returns;
            m_Notes = notes;
        }

        #region Variables
        public string m_Purpose;
        public string m_Params;
        public string m_Returns;
        public string m_Notes;
        #endregion

        #region Properties
        public string Purpose
        {
            get
            {
                return m_Purpose;
            }
        }

        public string Params
        {
            get
            {
                return m_Purpose;
            }
        }

        public string Returns
        {
            get
            {
                return m_Returns;
            }
        }

        public string Notes
        {
            get
            {
                return m_Notes;
            }
        }

        public string DisplayText
        {
            get
            {
                StringBuilder text = new StringBuilder();

                if ( m_Purpose != null )
                {
                    text.Append( PrepareDisplayText( "PURPOSE", m_Purpose ) );

                    if ( m_Params != null )
                    {
                        text.Append( "\n" );
                        text.Append( PrepareDisplayText( "PARAMS", "<br/>" + m_Params ) );
                    }

                    if ( m_Returns != null )
                    {
                        text.Append( "\n" );
                        text.Append( PrepareDisplayText( "RETURNS", m_Returns ) );
                    }

                    if ( m_Notes != null )
                    {
                        text.Append( "\n" );
                        text.Append( PrepareDisplayText( "NOTES", m_Notes ) );
                    }
                }

                return text.ToString();
            }
        }

        public string MarkupText
        {
            get
            {
                StringBuilder text = new StringBuilder();

                if ( m_Purpose != null )
                {
                    text.Append( EscapeMarkupText( "PURPOSE", m_Purpose ) );

                    if ( m_Params != null )
                    {
                        text.Append( "<br/>" );
                        text.Append( EscapeMarkupText( "PARAMS", "<br/>" + m_Params ) );
                    }

                    if ( m_Returns != null )
                    {
                        text.Append( "<br/>" );
                        text.Append( EscapeMarkupText( "RETURNS", m_Returns ) );
                    }

                    if ( m_Notes != null )
                    {
                        text.Append( "<br/>" );
                        text.Append( EscapeMarkupText( "NOTES", m_Notes ) );
                    }
                }

                return text.ToString();
            }
        }
        #endregion

        #region Public Functions
        public Comment Clone()
        {
            Comment c = new Comment();
            c.Copy( this );

            return c;
        }

        public void Copy( Comment c )
        {
            if ( c == null )
            {
                return;
            }

            m_Purpose = c.Purpose;
            m_Params = c.Params;
            m_Returns = c.Returns;
            m_Notes = c.Notes;
        }

        public void ParseXml( XmlTextReader reader )
        {
            System.Text.StringBuilder purpose = new System.Text.StringBuilder();
            System.Text.StringBuilder parms = new System.Text.StringBuilder();
            System.Text.StringBuilder returns = new System.Text.StringBuilder();
            System.Text.StringBuilder notes = new System.Text.StringBuilder();

            while ( reader.Read() )
            {
                if ( !reader.IsStartElement() )
                {
                    if ( reader.Name == "Comments" )
                    {
                        break;
                    }
                }
                else if ( !reader.IsEmptyElement )
                {
                    if ( reader.Name == "Purpose" )
                    {
                        purpose.Append( reader.ReadString() );
                        purpose.Append( "<br/>" );
                    }
                    else if ( reader.Name == "Params" )
                    {
                        parms.Append( reader.ReadString() );
                        parms.Append( "<br/>" );
                    }
                    else if ( reader.Name == "Returns" )
                    {
                        returns.Append( reader.ReadString() );
                        returns.Append( "<br/>" );
                    }
                    else if ( reader.Name == "Notes" )
                    {
                        notes.Append( reader.ReadString() );
                        notes.Append( "<br/>" );
                    }
                }
            }

            if ( purpose.Length > 0 )
            {
                purpose.Remove( purpose.Length - 5, 5 );
                m_Purpose = purpose.ToString();
            }

            if ( parms.Length > 0 )
            {
                parms.Remove( parms.Length - 5, 5 );
                m_Params = parms.ToString();
            }

            if ( returns.Length > 0 )
            {
                returns.Remove( returns.Length - 5, 5 );
                m_Returns = returns.ToString();
            }

            if ( notes.Length > 0 )
            {
                notes.Remove( notes.Length - 5, 5 );
                m_Notes = notes.ToString();
            }
        }

        public void SaveXml( XmlTextWriter writer )
        {
            if ( m_Purpose != null )
            {
                string[] purpose = NativeItem.SplitByBrTag( m_Purpose );
                foreach ( string p in purpose )
                {
                    writer.WriteStartElement( "Purpose" );
                    writer.WriteString( p );
                    writer.WriteEndElement();
                }
            }

            if ( m_Params != null )
            {
                string[] parms = NativeItem.SplitByBrTag( m_Params );
                foreach ( string p in parms )
                {
                    writer.WriteStartElement( "Params" );
                    writer.WriteString( p );
                    writer.WriteEndElement();
                }
            }

            if ( m_Returns != null )
            {
                string[] returns = NativeItem.SplitByBrTag( m_Returns );
                foreach ( string r in returns )
                {
                    writer.WriteStartElement( "Returns" );
                    writer.WriteString( r );
                    writer.WriteEndElement();
                }
            }

            if ( m_Notes != null )
            {
                string[] notes = NativeItem.SplitByBrTag( m_Notes );
                foreach ( string n in notes )
                {
                    writer.WriteStartElement( "Notes" );
                    writer.WriteString( n );
                    writer.WriteEndElement();
                }
            }
        }
        #endregion

        #region Private Functions
        private string PrepareDisplayText( string prefix, string text )
        {
            StringBuilder str = new StringBuilder();

            str.Append( "/// " );
            str.Append( prefix );
            str.Append( ": " );

            string[] split = NativeItem.SplitByBrTag( text );
            for ( int i = 0; i < split.Length; ++i )
            {
                if ( i > 0 )
                {
                    str.Append( "\n///    " );
                }

                str.Append( split[i] );
            }

            return str.ToString();
        }

        private string EscapeMarkupText( string prefix, string text )
        {
            StringBuilder str = new StringBuilder();

            str.Append( "<span style=\"color:commentdelim;\">//</span>" );
            str.Append( "<span style=\"color:comment;\">/ " );
            str.Append( prefix );
            str.Append( ": " );

            string[] split = NativeItem.SplitByBrTag( text );
            for ( int i = 0; i < split.Length; ++i )
            {
                if ( i > 0 )
                {
                    str.Append( "<br/><span style=\"color:commentdelim;\">//</span><span style=\"color:comment;\">/&nbsp;&nbsp;&nbsp;&nbsp;" );
                }

                str.Append( IntelliPrompt.EscapeMarkupText( split[i] ) );
                str.Append( "</span>" );
            }

            return str.ToString();
        }
        #endregion
    }

    public class NativeComparer : IComparer<NativeItem>
    {
        public int Compare( NativeItem x, NativeItem y )
        {
            if ( (x != null) && (y != null) )
            {
                return String.Compare( x.Name, y.Name );
            }
            else
            {
                return 0;
            }
        }
    }
    #endregion
}
