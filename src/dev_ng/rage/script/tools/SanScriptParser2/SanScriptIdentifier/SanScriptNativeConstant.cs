//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptNativeConstant.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace SanScriptParser
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Holds CONST_NATIVE declaration.
    /// </summary>
    [Serializable]
    public class SanScriptNativeConstant : SanScriptIdentifier
    {
        protected SanScriptNativeConstant()
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicConstant;
        }

        public SanScriptNativeConstant( string name, string nativeType )
            : base( name, SanScriptTokenID.UserIdentifier )
        {
            this.m_nativeType = nativeType;
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicConstant;
        }

        public SanScriptNativeConstant( string name, string nativeType, int filenameHashCode, int startOffset,
            string displayText, string markupText )
            : base( name, SanScriptTokenID.UserIdentifier, (int)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicConstant,
                filenameHashCode, startOffset, displayText, markupText )
        {
            this.m_nativeType = nativeType;
        }

        #region Constants
        public const string sm_HelpTypeGroupName = "const_or_enum";
        #endregion

        #region Variables
        [OptionalField]
        private string m_nativeType;
        #endregion
 
        #region Properties
        public string NativeType
        {
            get
            {
                return this.m_nativeType;
            }
        }
        #endregion

        #region SanScriptIdentifier Overrides
        public override string HelpTypeGroupName
        {
            get
            {
                return sm_HelpTypeGroupName;
            }
        }

        public override SanScriptIdentifierType IdentifierType
        {
            get
            {
                // Reusing the constant type... should be okay?
                return SanScriptIdentifierType.Constant;
            }
        }
        #endregion
    }
}
