using System;
using System.IO;
using System.Text;

namespace SanScriptParser
{
    /// <summary>
    /// A class for holding Intellisense related-information for an Identifier.
    /// </summary>   
    [Serializable]
    public abstract class SanScriptIdentifier
    {
        protected SanScriptIdentifier()
        {
        }
        
        public SanScriptIdentifier( string name, int tokenID )
        {
            this.m_name = name;
            this.m_tokenID = Convert.ToByte( tokenID );
        }

        public SanScriptIdentifier( string name, int tokenID, int imageIndex, int filenameHashCode, int startOffset,
            string displayText, string markupText )
            : this( name, tokenID )
        {
            this.m_imageIndex = (byte)imageIndex;
            this.m_filenameHashCode = filenameHashCode;
            this.m_startOffset = startOffset;
            this.m_encodedDisplayText = this.EncodeDisplayText( displayText );
            this.m_encodedMarkupText = this.EncodeMarkupText( markupText );
        }

        #region Variables
        protected string m_name;
        protected byte m_imageIndex = 255;
        protected byte m_tokenID = SanScriptTokenID.Invalid;
        protected int m_filenameHashCode;
        protected int m_startOffset;
        protected string m_encodedDisplayText;
        protected string m_encodedMarkupText;
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return m_name;
            }
        }

        public int ImageIndex
        {
            get
            {
                if ( m_imageIndex == 255 )
                {
                    return -1;
                }

                return (int)m_imageIndex;
            }
        }

        public int TokenID
        {
            get
            {
                return (int)m_tokenID;
            }
        }

        public int FilenameHashCode
        {
            get
            {
                return m_filenameHashCode;
            }
        }

        public int StartOffset
        {
            get
            {
                return m_startOffset;
            }
        }

        public virtual string DisplayText
        {
            get
            {
                return DecodeDisplayText( m_encodedDisplayText );
            }
        }

        public virtual string MarkupText
        {
            get
            {
                return DecodeMarkupText( m_encodedMarkupText );
            }
        }

        public virtual string HelpTypeGroupName
        {
            get
            {
                return null;
            }
        }

        public virtual SanScriptIdentifierType IdentifierType
        {
            get
            {
                return SanScriptIdentifierType.Identifier;
            }
        }
        #endregion

        #region Public Functions
        public void WriteHelpText( string helpGroupText, StreamWriter writer )
        {
            if ( this.HelpTypeGroupName == null )
            {
                return;
            }

            writer.WriteLine( "//@@" + m_name );
            if ( (helpGroupText != null) && (helpGroupText != string.Empty) )
            {
                System.Text.StringBuilder group = new System.Text.StringBuilder( "//<GROUP " );
                group.Append( helpGroupText );
                group.Append( "_" );
                group.Append( this.HelpTypeGroupName );
                group.Append( ">" );
                
                writer.WriteLine( group.ToString() );
            }

            writer.WriteLine( "//SIGNATURE:<CODE>" );
            writer.WriteLine( GetDisplayTextForHelp() );
            writer.WriteLine( "//</CODE>" );
            writer.WriteLine( "//@DOC_END\n" );
        }

        public virtual string GetDisplayTextForHelp()
        {
            string displayText = this.DisplayText;

            StringBuilder helpText = new StringBuilder();

            string[] lines = displayText.Split( new char[] { '\n' } );
            for ( int i = 0; i < lines.Length; ++i )
            {
                if ( i > 0 )
                {
                    helpText.Append( "\n" );
                }

                if ( lines[i].StartsWith( "///" ) )
                {
                    helpText.Append( "//" );    // fixup
                }

                helpText.Append( lines[i] );
            }

            return helpText.ToString();
        }
        #endregion

        #region Protected Functions
        protected string EncodeDisplayText( string text )
        {
            // FIXME
            return text;
        }

        protected string DecodeDisplayText( string text )
        {
            // FIXME
            return text;
        }

        protected string EncodeMarkupText( string text )
        {
            // FIXME
            return text;
        }

        protected string DecodeMarkupText( string text )
        {
            // FIXME
            return text;
        }
        #endregion
    }
}
