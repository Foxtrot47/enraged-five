//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptNativeType.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;

namespace SanScriptParser
{
    /// <summary>
    /// Holds a NATIVE type declaration.
    /// </summary>
    [Serializable]
    public class SanScriptNativeType : SanScriptIdentifier
    {
        protected SanScriptNativeType()
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicClass;
        }

        public SanScriptNativeType( string name )
            : base( name, SanScriptTokenID.UserIdentifier )
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicClass;
        }

        public SanScriptNativeType( string name, int filenameHashCode, int startOffset,
            string displayText, string markupText )
            : base( name, SanScriptTokenID.NativeIdentifier, (int)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicClass,
            filenameHashCode, startOffset, displayText, markupText )
        {

        }

        #region SanScriptIdentifier Overrides
        public override SanScriptIdentifierType IdentifierType
        {
            get
            {
                return SanScriptIdentifierType.NativeType;
            }
        }
        #endregion
    }
}
