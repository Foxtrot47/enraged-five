//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptWordOperator.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;

namespace SanScriptParser
{
    /// <summary>
    /// Holds a language word operator
    /// </summary>
    [Serializable]
    public class SanScriptWordOperator : SanScriptIdentifier
    {
        protected SanScriptWordOperator()
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.Operator;
        }

        public SanScriptWordOperator( string name, int tokenID )
            : base( name, tokenID )
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.Operator;
        }

        public SanScriptWordOperator( string name, int tokenID, int filenameHashCode, string displayText, string markupText )
            : base( name, tokenID, (int)ActiproSoftware.Products.SyntaxEditor.IconResource.Operator,
            filenameHashCode, -1, displayText, markupText )
        {

        }

        #region SanScriptIdentifier Overrides
        public override SanScriptIdentifierType IdentifierType
        {
            get
            {
                return SanScriptIdentifierType.WordOperator;
            }
        }
        #endregion
    }
}
