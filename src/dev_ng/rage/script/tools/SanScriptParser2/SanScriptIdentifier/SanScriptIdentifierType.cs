//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptIdentifierType.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;

namespace SanScriptParser
{
    [Flags]
    public enum SanScriptIdentifierType
    {
        Identifier = 1,
        Constant = 2,
        NativeType = 4,
        StructType = 8,
        StructMember = 16,
        EnumType = 32,
        Enum = 64,
        StaticVariable = 128,
        Subroutine = 256,
        NativeSubroutine = 512,
        State = 1024,
        Keyword = 2048,
        WordOperator = 4096,
        KeywordSubroutine = 8192,
        TypedefType = 16384
    }
}