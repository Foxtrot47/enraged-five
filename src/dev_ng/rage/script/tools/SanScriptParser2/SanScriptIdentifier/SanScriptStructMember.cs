//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptStructMember.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Runtime.Serialization;

namespace SanScriptParser
{
    /// <summary>
    /// Holds a struct member.
    /// </summary>
    [Serializable]
    public class SanScriptStructMember : SanScriptIdentifier
    {
        protected SanScriptStructMember()
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicField;
        }

        public SanScriptStructMember( string name )
            : base( name, SanScriptTokenID.UserIdentifier )
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicField;
        }

        public SanScriptStructMember( string name, string type, SanScriptStructure parentStruct, int filenameHashCode, int startOffset,
            string displayText, string markupText )
            : base( name, SanScriptTokenID.UserIdentifier, (int)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicField,
            filenameHashCode, startOffset, displayText, markupText )
        {
            m_type = type;
            m_parentStruct = parentStruct;
        }

        #region Variables
        [OptionalField]
        private string m_type;
        
        private SanScriptStructure m_parentStruct;
        #endregion
 
        #region Properties
        public string Type
        {
            get
            {
                return m_type;
            }
        }

        public SanScriptStructure ParentStruct
        {
            get
            {
                return m_parentStruct;
            }
        }
        #endregion

        #region SanScriptIdentifier Overrides
        public override SanScriptIdentifierType IdentifierType
        {
            get
            {
                return SanScriptIdentifierType.StructMember;
            }
        }
        #endregion
    }
}
