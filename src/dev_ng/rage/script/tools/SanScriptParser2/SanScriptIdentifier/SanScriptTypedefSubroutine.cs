//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptTypedefSubroutine.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace SanScriptParser
{
    /// <summary>
    /// Holds a TYPEDEF FUNC or PROC declaration.
    /// </summary>
    [Serializable]
    public class SanScriptTypedefSubroutine : SanScriptIdentifier
    {
        protected SanScriptTypedefSubroutine()
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicClass;
        }

        public SanScriptTypedefSubroutine( string name )
            : base( name, SanScriptTokenID.TypedefIdentifier )
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicClass;
        }

        public SanScriptTypedefSubroutine( string name,
            int filenameHashCode, int startOffset, string displayText, string markupText )
            : base( name, SanScriptTokenID.TypedefIdentifier, (int)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicClass,
            filenameHashCode, startOffset, displayText, markupText )
        {
        }

        #region Variables
        private List<string> m_argsMarkupText = new List<string>();
        #endregion

        #region Properties
        public List<string> ArgumentsMarkupText
        {
            get
            {
                return m_argsMarkupText;
            }
        }
        #endregion

        #region SanScriptIdentifier Overrides
        public override SanScriptIdentifierType IdentifierType
        {
            get
            {
                return SanScriptIdentifierType.TypedefType;
            }
        }

        public override string HelpTypeGroupName
        {
            get
            {
                return base.HelpTypeGroupName;
            }
        }
        #endregion
    }
}
