//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptState.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;

namespace SanScriptParser
{
    /// <summary>
    /// Holds a STATE name.
    /// </summary>
    [Serializable]
    public class SanScriptState : SanScriptIdentifier
    {
        protected SanScriptState()
        {
        }

        public SanScriptState( string name )
            : base( name, SanScriptTokenID.UserIdentifier )
        {
        }

        public SanScriptState( string name, int filenameHashCode, int startOffset,
            string displayText, string markupText )
            : base( name, SanScriptTokenID.UserIdentifier, 255,
            filenameHashCode, startOffset, displayText, markupText )
        {

        }

        #region Constants
        public const string sm_HelpTypeGroupName = "state";
        #endregion

        #region SanScriptIdentifier Overrides
        public override SanScriptIdentifierType IdentifierType
        {
            get
            {
                return SanScriptIdentifierType.State;
            }
        }

        public override string HelpTypeGroupName
        {
            get
            {
                return sm_HelpTypeGroupName;
            }
        }
        #endregion
    }
}
