//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptEnum.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace SanScriptParser
{
    /// <summary>
    /// Holds a user-defined ENUM type or FORWARD-declared ENUM type.
    /// </summary>
    [Serializable]
    public class SanScriptEnum : SanScriptIdentifier
    {
        protected SanScriptEnum()
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicEnumeration;
        }

        public SanScriptEnum( string name )
            : base( name, SanScriptTokenID.UserIdentifier )
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicEnumeration;
        }

        public SanScriptEnum( string name, int filenameHashCode, int startOffset,
            string displayText, string markupText )
            : base( name, SanScriptTokenID.UserIdentifier, (int)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicEnumeration,
            filenameHashCode, startOffset, displayText, markupText )
        {
        }

        #region Constants
        public const string sm_HelpTypeGroupName = "const_or_enum";
        #endregion

        #region Variables
        private List<SanScriptEnumeration> m_enums = new List<SanScriptEnumeration>();
        #endregion

        #region Properties
        public List<SanScriptEnumeration> Enums
        {
            get
            {
                return m_enums;
            }
        }
        #endregion

        #region SanScriptIdentifier Overrides
        public override string HelpTypeGroupName
        {
            get
            {
                return sm_HelpTypeGroupName;
            }
        }

        public override SanScriptIdentifierType IdentifierType
        {
            get
            {
                return SanScriptIdentifierType.EnumType;
            }
        }

        public override string GetDisplayTextForHelp()
        {
            StringBuilder helpText = new StringBuilder( base.GetDisplayTextForHelp() );

            if ( m_enums.Count > 0 )
            {
                helpText.Append( "\n" );
            }

            foreach ( SanScriptEnumeration e in m_enums )
            {
                helpText.Append( "\n\t" );
                helpText.Append( e.GetDisplayTextForHelp() );
            }

            return helpText.ToString();
        }
        #endregion
    }
}
