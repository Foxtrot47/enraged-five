//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptIdentifierFile.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Threading;

namespace SanScriptParser
{
    internal class SanScriptIdentifierFile
    {
        public SanScriptIdentifierFile( int filenameHashCode )
        {
            this.FilenameHashCode = filenameHashCode;
        }

        #region Variables

        private readonly ICollection<string> m_usings = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        private readonly ICollection<int> m_usingsHashCodes = new HashSet<int>();
        private byte m_containsGlobals = 0;
        #endregion

        #region Properties
        public int FilenameHashCode { get; } = 0;

        public IEnumerable<string> Usings => this.m_usings;

        public IEnumerable<int> UsingsHashCodes => this.m_usingsHashCodes;

        public IDictionary<char, ICollection<SanScriptIdentifier>> FirstLetterIdentifiers { get; } =
            new Dictionary<char, ICollection<SanScriptIdentifier>>();

        public ReaderWriterLockSlim Lock { get; } = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);

        /// <summary>
        /// A dictionary mapping 'words' to their identifiers, where a 'word' is denoted by a segment of an identifier's name AFTER the first underscore
        /// (GET_SOME_HASH would have SOME and HASH). This is used to allow searching for strings that match in the middle of an identifier.
        /// The first letter dictionary handles the first word, so we don't need to store that.
        /// </summary>
        public IDictionary<string, ICollection<SanScriptIdentifier>> WordIdentifiers { get; } =
            new Dictionary<string, ICollection<SanScriptIdentifier>>(StringComparer.OrdinalIgnoreCase);

        public DateTime LastUpdated { get; set; } = DateTime.MinValue;

        public bool ContainsGlobals
        {
            get
            {
                return ((this.m_containsGlobals & 0x01) == 0x01) ? true : false;
            }
            set
            {
                if ( value )
                {
                    this.m_containsGlobals |= 0x01;
                }
                else
                {
                    this.m_containsGlobals &= 0xfe;
                }
            }
        }

        public bool ContainsGlobalsStatement
        {
            get
            {
                return ((this.m_containsGlobals & 0x02) == 0x02);
            }
            set
            {
                if ( value )
                {
                    this.m_containsGlobals |= 0x02;
                }
                else
                {
                    this.m_containsGlobals &= 0xfd;
                }
            }
        }
        #endregion

        #region Public Functions
        public void AddUsing( string filename, int filenameHashCode )
        {
            this.Lock.EnterWriteLock();
            try
            {
                this.m_usings.Add(filename);
                this.m_usingsHashCodes.Add(filenameHashCode);
            }
            finally
            {
                this.Lock.ExitWriteLock();
            }
        }

        public void Reset()
        {
            this.Lock.EnterWriteLock();
            try
            {
                this.m_usings.Clear();
                this.m_usingsHashCodes.Clear();
                this.FirstLetterIdentifiers.Clear();
                this.WordIdentifiers.Clear();
                this.m_containsGlobals = 0;
            }
            finally
            {
                this.Lock.ExitWriteLock();
            }
        }
        #endregion
    }
}