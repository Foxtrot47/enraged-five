//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptIdentifierSerializerBinder.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;

namespace SanScriptParser
{
    sealed class SanScriptIdentifierSerializerBinder : SerializationBinder
    {
        public override Type BindToType( string assemblyName, string typeName )
        {
            Type typeToDeserialize = null;

            string theNamespace = "SanScriptParser.";
            int indexOfNamespace = typeName.IndexOf( theNamespace );
            if ( indexOfNamespace > -1 )
            {
                if ( typeName.Contains( "System.Collections.Generic.List" ) )
                {
                    // pull out type name
                    string name = typeName.Substring( indexOfNamespace + theNamespace.Length );
                    int indexOfComma = name.IndexOf( ',' );
                    if ( indexOfComma > -1 )
                    {
                        name = name.Substring( 0, indexOfComma );
                    }

                    switch ( name )
                    {
                        case "SanScriptIdentifier":
                        {
                            List<SanScriptIdentifier> id = new List<SanScriptIdentifier>();
                            typeToDeserialize = id.GetType();
                        }
                            break;
                        case "SanScriptConstant":
                        {
                            List<SanScriptConstant> id = new List<SanScriptConstant>();
                            typeToDeserialize = id.GetType();
                        }
                            break;
                        case "SanScriptNativeType":
                        {
                            List<SanScriptNativeType> id = new List<SanScriptNativeType>();
                            typeToDeserialize = id.GetType();
                        }
                            break;
                        case "SanScriptTypedefSubroutine":
                        {
                            List<SanScriptTypedefSubroutine> id = new List<SanScriptTypedefSubroutine>();
                            typeToDeserialize = id.GetType();
                        }
                            break;
                        case "SanScriptStructure":
                        {
                            List<SanScriptStructure> id = new List<SanScriptStructure>();
                            typeToDeserialize = id.GetType();
                        }
                            break;
                        case "SanScriptStructMember":
                        {
                            List<SanScriptStructMember> id = new List<SanScriptStructMember>();
                            typeToDeserialize = id.GetType();
                        }
                            break;
                        case "SanScriptEnum":
                        {
                            List<SanScriptEnum> id = new List<SanScriptEnum>();
                            typeToDeserialize = id.GetType();
                        }
                            break;
                        case "SanScriptEnumeration":
                        {
                            List<SanScriptEnumeration> id = new List<SanScriptEnumeration>();
                            typeToDeserialize = id.GetType();
                        }
                            break;
                        case "SanScriptStaticVariable":
                        {
                            List<SanScriptStaticVariable> id = new List<SanScriptStaticVariable>();
                            typeToDeserialize = id.GetType();
                        }
                            break;
                        case "SanScriptSubroutine":
                        {
                            List<SanScriptSubroutine> id = new List<SanScriptSubroutine>();
                            typeToDeserialize = id.GetType();
                        }
                            break;
                        case "SanScriptNativeSubroutine":
                        {
                            List<SanScriptNativeSubroutine> id = new List<SanScriptNativeSubroutine>();
                            typeToDeserialize = id.GetType();
                        }
                            break;
                        case "SanScriptState":
                        {
                            List<SanScriptState> id = new List<SanScriptState>();
                            typeToDeserialize = id.GetType();
                        }
                            break;
                    }
                }
                else
                {
                    string correctAssemblyName = Assembly.GetExecutingAssembly().FullName;
                    typeToDeserialize = Type.GetType( string.Format( "{0}, {1}", typeName, correctAssemblyName ) );
                }
            }
            else
            {
                typeToDeserialize = Type.GetType( typeName );
            }

            return typeToDeserialize;
        }
    }
}
