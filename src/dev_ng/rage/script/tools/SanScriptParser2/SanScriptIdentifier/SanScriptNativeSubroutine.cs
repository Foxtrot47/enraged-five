//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptNativeSubroutine.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace SanScriptParser
{
    /// <summary>
    /// Holds a NATIVE EVENT, FUNC, or PROC declaration.
    /// </summary>
    [Serializable]
    public class SanScriptNativeSubroutine : SanScriptIdentifier
    {
        protected SanScriptNativeSubroutine()
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicMethod;
        }

        public SanScriptNativeSubroutine( string name )
            : base( name, SanScriptTokenID.NativeIdentifier )
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicMethod;
        }

        public SanScriptNativeSubroutine( string name,
            int filenameHashCode, int startOffset, string displayText, string markupText )
            : base( name, SanScriptTokenID.NativeIdentifier, (int)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicMethod,
            filenameHashCode, startOffset, displayText, markupText )
        {
        }

        #region Constants
        public const string sm_HelpTypeGroupName = "proc_or_func";
        #endregion

        #region Variables
        private List<string> m_argsMarkupText = new List<string>();
        #endregion

        #region Properties
        public List<string> ArgumentsMarkupText
        {
            get
            {
                return m_argsMarkupText;
            }
        }
        #endregion

        #region SanScriptIdentifier Overrides
        public override SanScriptIdentifierType IdentifierType
        {
            get
            {
                return SanScriptIdentifierType.NativeSubroutine;
            }
        }

        public override string HelpTypeGroupName
        {
            get
            {
                return sm_HelpTypeGroupName;
            }
        }
        #endregion
    }
}
