//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptConstant.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;

namespace SanScriptParser
{
    /// <summary>
    /// Holds CONST_INT or CONST_FLOAT declaration.
    /// </summary>
    [Serializable]
    public class SanScriptConstant : SanScriptIdentifier
    {
        protected SanScriptConstant()
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicConstant;
        }

        public SanScriptConstant( string name )
            : base( name, SanScriptTokenID.UserIdentifier )
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicConstant;
        }

        public SanScriptConstant( string name, int filenameHashCode, int startOffset,
            string displayText, string markupText )
            : base( name, SanScriptTokenID.UserIdentifier, (int)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicConstant,
            filenameHashCode, startOffset, displayText, markupText )
        {

        }

        #region Constants
        public const string sm_HelpTypeGroupName = "const_or_enum";
        #endregion

        #region SanScriptIdentifier Overrides
        public override string HelpTypeGroupName
        {
            get
            {
                return sm_HelpTypeGroupName;
            }
        }

        public override SanScriptIdentifierType IdentifierType
        {
            get
            {
                return SanScriptIdentifierType.Constant;
            }
        }
        #endregion
    }
}
