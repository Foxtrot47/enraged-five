//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptKeywordSubroutine.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;

namespace SanScriptParser
{
    /// <summary>
    /// Holds a language subroutine
    /// </summary>
    public class SanScriptKeywordSubroutine : SanScriptSubroutine
    {
        protected SanScriptKeywordSubroutine()
            : base()
        {
            
        }

        public SanScriptKeywordSubroutine( string name, int tokenID )
            : base( name )
        {
            m_tokenID = Convert.ToByte( tokenID );
        }

        public SanScriptKeywordSubroutine( string name, int tokenID,
            int filenameHashCode, string displayText, string markupText )
            : base( name, filenameHashCode, -1, displayText, markupText )
        {
            m_tokenID = Convert.ToByte( tokenID );
        }

        #region SanScriptIdentifier Overrides
        public override SanScriptIdentifierType IdentifierType
        {
            get
            {
                return SanScriptIdentifierType.KeywordSubroutine;
            }
        }

        public override string HelpTypeGroupName
        {
            get
            {
                return base.HelpTypeGroupName;
            }
        }
        #endregion
    }
}
