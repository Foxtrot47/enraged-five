//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptEnumeration.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;

namespace SanScriptParser
{
    /// <summary>
    /// Holds an ENUM value.
    /// </summary>
    [Serializable]
    public class SanScriptEnumeration : SanScriptIdentifier
    {
        protected SanScriptEnumeration()
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.EnumerationItem;
        }

        public SanScriptEnumeration( string name )
            : base( name, SanScriptTokenID.UserIdentifier )
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.EnumerationItem;
        }

        public SanScriptEnumeration( string name, SanScriptEnum parentEnum, int filenameHashCode, int startOffset,
            string displayText, string markupText )
            : base( name, SanScriptTokenID.UserIdentifier, (int)ActiproSoftware.Products.SyntaxEditor.IconResource.EnumerationItem,
            filenameHashCode, startOffset, displayText, markupText )
        {
            m_parentEnum = parentEnum;
        }

        #region Variables
        private SanScriptEnum m_parentEnum;
        #endregion

        #region Properties
        public SanScriptEnum ParentEnum
        {
            get
            {
                return m_parentEnum;
            }
        }
        #endregion

        #region SanScriptIdentifier Overrides
        public override SanScriptIdentifierType IdentifierType
        {
            get
            {
                return SanScriptIdentifierType.Enum;
            }
        }
        #endregion
    }
}
