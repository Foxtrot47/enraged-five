//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptKeyword.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;

namespace SanScriptParser
{
    /// <summary>
    /// Holds a language keyword that isn't a subroutine
    /// </summary>
    [Serializable]
    public class SanScriptKeyword : SanScriptIdentifier
    {
        protected SanScriptKeyword()
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.Keyword;
        }

        public SanScriptKeyword( string name, int tokenID )
            : base( name, tokenID )
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.Keyword;
        }

        public SanScriptKeyword( string name, int tokenID, int filenameHashCode, string displayText, string markupText )
            : base( name, tokenID, (int)ActiproSoftware.Products.SyntaxEditor.IconResource.Keyword,
            filenameHashCode, -1, displayText, markupText )
        {

        }

        #region SanScriptIdentifier Overrides
        public override SanScriptIdentifierType IdentifierType
        {
            get
            {
                return SanScriptIdentifierType.Keyword;
            }
        }
        #endregion
    }
}
