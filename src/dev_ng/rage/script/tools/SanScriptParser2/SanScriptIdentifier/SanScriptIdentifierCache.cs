//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptIdentifierCache.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using ragScriptEditorShared;
using System.Runtime.Serialization.Formatters;
using RSG.Base.Extensions;

namespace SanScriptParser
{
    /// <summary>
    /// A class for managing a collection of SanScriptIdentifiers
    /// </summary>
    public class SanScriptIdentifierCache
    {
        #region Variables
        private readonly ConcurrentDictionary<int, string> m_filenamesByHashCode = new ConcurrentDictionary<int, string>();
        private readonly ConcurrentDictionary<int, SanScriptIdentifierFile> m_identifierFilesByHashCode = new ConcurrentDictionary<int, SanScriptIdentifierFile>();
        #endregion

        #region Public Functions

        /// <summary>
        /// Given a source file, return its hash code. This assumes the path is already fully canonicalized.
        /// </summary>
        /// <param name="sourcePath">The source path</param>
        /// <returns></returns>
        public static int GetFilenameHashCode(string sourcePath)
        {
            return StringComparer.OrdinalIgnoreCase.GetHashCode(sourcePath);
        }

        /// <summary>
        /// Turns a fully-qualified path name into an output filename for the cache.
        /// </summary>
        /// <param name="sourcePath">Path to the original source file</param>
        /// <returns>The output file name.</returns>
        public static string GetCacheOutputFileName(string sourcePath)
        {
            // add the hash code in case we have files with the same names but different paths
            int hashCode = GetFilenameHashCode(sourcePath);
            string outputFilename = $"{Path.GetFileNameWithoutExtension(sourcePath)}{hashCode}.bin";
            string filenameWithBranchInfo = ApplicationSettings.PrependProjectRoot(outputFilename);
            string pathRoot = Path.GetPathRoot(filenameWithBranchInfo);
            if (!string.IsNullOrWhiteSpace(pathRoot))
            {
                filenameWithBranchInfo = filenameWithBranchInfo.Replace(pathRoot, string.Empty);
            }

            return Path.Combine(SanScriptProjectResolver.ParserPlugin.CurrentLanguageSettings.CacheDirectory, filenameWithBranchInfo);
        }

        /// <summary>
        /// Retrieves the string filename that corresponds to the given filenameHashCode
        /// </summary>
        /// <param name="filenameHashCode">The hash code.</param>
        /// <returns>A <c>string</c>.</returns>
        public string GetFilename( int filenameHashCode )
        {
            string filename;
            return this.m_filenamesByHashCode.TryGetValue( filenameHashCode, out filename ) ? filename : null;
        }

        /// <summary>
        /// Retrieves the last <see cref="DateTime"/> that the cache data for this file was updated.
        /// </summary>
        /// <param name="filenameHashCode">The hash code of the file to examine.</param>
        /// <returns>The last <see cref="DateTime"/> an update was performed.</returns>
        public DateTime GetLastUpdateTime( int filenameHashCode )
        {
            SanScriptIdentifierFile idFile;
            return this.m_identifierFilesByHashCode.TryGetValue( filenameHashCode, out idFile ) ? idFile.LastUpdated : DateTime.MinValue;
        }

        /// <summary>
        /// Returns whether the file contains a "GLOBALS TRUE" statement or not.
        /// </summary>
        /// <param name="filename">The file to check.</param>
        /// <returns><c>true</c> if it contains "GLOBALS TRUE", otherwise <c>false</c>.</returns>
        public bool ContainsGlobalsStatement( string filename )
        {
            return this.ContainsGlobalsStatement( GetFilenameHashCode(filename) );
        }

        /// <summary>
        /// Returns whether the file contains a "GLOBALS TRUE" statement or not.
        /// </summary>
        /// <param name="filenameHashCode">The hashcode of the file to check.</param>
        /// <returns><c>true</c> if it contains "GLOBALS TRUE", otherwise <c>false</c>.</returns>
        public bool ContainsGlobalsStatement( int filenameHashCode )
        {
            SanScriptIdentifierFile idFile;
            return this.m_identifierFilesByHashCode.TryGetValue( filenameHashCode, out idFile ) && idFile.ContainsGlobalsStatement;
        }

        /// <summary>
        /// Retrieves the list of USINGs that a file has.
        /// </summary>
        /// <param name="filename">The file.</param>
        /// <returns>A <see cref="List{T}"/> of filenames.</returns>
        public IEnumerable<string> GetUsings( string filename )
        {
            return this.GetUsings(GetFilenameHashCode(filename));
        }

        /// <summary>
        /// Retrieves the list of USINGs that a file has.
        /// </summary>
        /// <param name="filenameHashCode">The hash code of the file.</param>
        /// <returns>A <see cref="List{T}"/> of filenames.</returns>
        public IEnumerable<string> GetUsings( int filenameHashCode )
        {
            SanScriptIdentifierFile idFile;
            if (!this.m_identifierFilesByHashCode.TryGetValue(filenameHashCode, out idFile))
            {
                return Enumerable.Empty<String>();
            }

            idFile.Lock.EnterReadLock();
            try
            {
                return idFile.Usings.ToList();
            }
            finally
            {
                idFile.Lock.ExitReadLock();
            }
        }

        /// <summary>
        /// Retrieves the list of USINGs HashCodes that a file has.
        /// </summary>
        /// <param name="filename">The file to search for.</param>
        /// <returns>A list of <c>ints</c>s.</returns>
        public IEnumerable<int> GetUsingsHashCodes( string filename )
        {
            return this.GetUsingsHashCodes(GetFilenameHashCode(filename));
        }

        /// <summary>
        /// Retrieves the list of USINGs HashCodes that a file has.
        /// </summary>
        /// <param name="filename">The file to search for.</param>
        /// <returns>A list of <c>ints</c>s.</returns>
        public IEnumerable<int> GetUsingsHashCodes( int filenameHashCode )
        {
            SanScriptIdentifierFile idFile;
            if (!this.m_identifierFilesByHashCode.TryGetValue(filenameHashCode, out idFile))
            {
                return Enumerable.Empty<int>();
            }

            idFile.Lock.EnterReadLock();
            try
            {
                return idFile.UsingsHashCodes.ToList();
            }
            finally
            {
                idFile.Lock.ExitReadLock();
            }
        }

        /// <summary>
        /// Retrieves the list of USINGs that a file has, includes all of the USINGSs' USINGs, in order
        /// </summary>
        /// <param name="filename">The file in question.</param>
        /// <returns>A <see cref="Dictionary{TKey,TValue}"/> containing the file's dependencies.  The key is the filepath, the value is the index.</returns>
        public IDictionary<string, int> GetAllUsings( string filename )
        {
            IDictionary<string, int> usingFiles = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
            this.GetAllUsingsInternal(filename, usingFiles, filename);
            return usingFiles;
        }

        /// <summary>
        /// Retrieves the list of USINGs that a file has, includes all of the USINGSs' USINGs, in order
        /// </summary>
        /// <param name="filenameHashCode">The hash code of the file in question.</param>
        /// <returns>A <see cref="Dictionary{TKey,TValue}"/> containing the file's dependencies.  The key is the filepath, the value is the index.</returns>
        public IDictionary<string, int> GetAllUsings( int filenameHashCode )
        {
            IDictionary<string, int> usingFiles = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
            string filename;
            if ( !this.m_filenamesByHashCode.TryGetValue( filenameHashCode, out filename ) )
            {
                return usingFiles;
            }

            this.GetAllUsingsInternal(filename, usingFiles, filename);
            return usingFiles;
        }

        /// <summary>
        /// Retrieves the list of USINGs that a file has, includes all of the USINGSs' USINGs
        /// </summary>
        /// <param name="filenameHashCode">The hash code of the file in question.</param>
        /// <param name="usingsHashCodes">A <see cref="ISet{T}"/> containing the file's dependencies.</param>
        public void GetAllUsingsHashCodes( int filenameHashCode, ISet<int> usingsHashCodes )
        {
            this.GetAllUsingsHashCodesInternal( filenameHashCode, usingsHashCodes, filenameHashCode );
        }

        /// <summary>
        /// Gets a list of all the identifiers for the given file hash.
        /// </summary>
        /// <param name="filenameHashCode">The hash code of the file to search.</param>
        /// <param name="identifierTypes">A bit mask for which <see cref="SanScriptIdentifier"/> types to add to the list.</param>
        /// <returns>A <see cref="List{T}"/> of <see cref="SanScriptIdentifier"/>s.  Empty if nothing found.</returns>
        public IEnumerable<SanScriptIdentifier> FindIdentifiersInFileHash( int filenameHashCode, SanScriptIdentifierType identifierTypes = (SanScriptIdentifierType)0xffff)
        {
            SanScriptIdentifierFile idFile;
            return this.m_identifierFilesByHashCode.TryGetValue(filenameHashCode, out idFile)
                ? this.FindIdentifiersInFile(idFile, identifierTypes)
                : Enumerable.Empty<SanScriptIdentifier>();
        }

        /// <summary>
        /// Gets all of the identifiers for a given file that start with the same letter as matchText, and when 
        /// includeWordDictionary is true, also start with matchText.
        /// </summary>
        /// <param name="filenameHashCode">The hash code of the file to search.</param>
        /// <param name="matchText">The text to match.  Must not be string.Empty.</param>
        /// <param name="includeInnerWords">When <c>true</c>, includes <see cref="SanScriptIdentifier"/>s that contain matchText inside them.</param>
        /// <param name="identifierTypes">A bit mask for which <see cref="SanScriptIdentifier"/> types to add to the list.</param>
        /// <returns>A <see cref="List{T}"/> of <see cref="SanScriptIdentifier"/>s.  Empty if nothing found.</returns>
        public IEnumerable<SanScriptIdentifier> FindIdentifiersInFileHashStartingWith( int filenameHashCode, string matchText, bool includeInnerWords, 
            SanScriptIdentifierType identifierTypes = (SanScriptIdentifierType)0xffff )
        {
            ISet<SanScriptIdentifier> identifiers = new HashSet<SanScriptIdentifier>();
            if (matchText.Length <= 0)
            {
                return identifiers;
            }

            SanScriptIdentifierFile idFile;
            if (!this.m_identifierFilesByHashCode.TryGetValue(filenameHashCode, out idFile))
            {
                return identifiers;
            }

            idFile.Lock.EnterReadLock();
            try
            {
                ICollection<SanScriptIdentifier> letterList;
                char ch = char.ToUpper(matchText[0]);
                if (idFile.FirstLetterIdentifiers.TryGetValue(ch, out letterList))
                {
                    foreach (SanScriptIdentifier id in letterList)
                    {
                        if (!identifierTypes.HasFlag(id.IdentifierType))
                        {
                            continue;
                        }

                        if (matchText.Length == 1 || id.Name.StartsWith(matchText, StringComparison.OrdinalIgnoreCase))
                        {
                            identifiers.Add(id);
                        }
                    }
                }

                if (!includeInnerWords)
                {
                    return identifiers;
                }

                string matchWordText = matchText.Trim('_');

                // check the word dictionary
                foreach (KeyValuePair<string, ICollection<SanScriptIdentifier>> pair in idFile.WordIdentifiers)
                {
                    if (pair.Key.StartsWith(matchWordText, StringComparison.OrdinalIgnoreCase))
                    {
                        identifiers.UnionWith(pair.Value);
                    }
                }

                return identifiers;
            }
            finally
            {
                idFile.Lock.ExitReadLock();
            }
        }

        /// <summary>
        /// Locates the first <see cref="SanScriptIdentifier"/> in the cache for the specified file that matches the name exactly.
        /// </summary>
        /// <param name="filename">The file to search.</param>
        /// <param name="name">The <c>string</c> to match.</param>
        /// <param name="identifierTypes">Types of identifier to return.</param>
        /// <returns>The <see cref="SanScriptIdentifier"/>.  <c>null</c> if nothing found.</returns>
        public SanScriptIdentifier FindIdentifierInFileExact(string filename, string name, SanScriptIdentifierType identifierTypes = (SanScriptIdentifierType)0xffff)
        {
            SanScriptIdentifier identifier = null;
            if (name.Length <= 0)
            {
                return identifier;
            }

            int filenameHashCode = GetFilenameHashCode(filename);
            return this.FindIdentifierInFileHashExact(filenameHashCode, name, identifierTypes);
        }

        /// <summary>
        /// Locates the first <see cref="SanScriptIdentifier"/> in the cache for the specified file hash that matches the name exactly.
        /// </summary>
        /// <param name="filenameHashCode"></param>
        /// <param name="name"></param>
        /// <param name="identifierTypes"></param>
        /// <returns></returns>
        public SanScriptIdentifier FindIdentifierInFileHashExact(int filenameHashCode, string name, SanScriptIdentifierType identifierTypes = (SanScriptIdentifierType)0xffff)
        {
            SanScriptIdentifierFile idFile;
            if (!this.m_identifierFilesByHashCode.TryGetValue(filenameHashCode, out idFile))
            {
                return null;
            }

            idFile.Lock.EnterReadLock();
            try
            {
                ICollection<SanScriptIdentifier> firstLetterList;
                char ch = char.ToUpper(name[0]);
                if (idFile.FirstLetterIdentifiers.TryGetValue(ch, out firstLetterList))
                {
                    return firstLetterList.FirstOrDefault(id => identifierTypes.HasFlag(id.IdentifierType) &&
                                                                id.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
                }
            }
            finally
            {
                idFile.Lock.ExitReadLock();
            }

            return null;
        }

        /// <summary>
        /// Locates the first <see cref="SanScriptIdentifier"/> in the cache for the specified file hash that matches the name exactly.
        /// </summary>
        /// <param name="filenameHashCode"></param>
        /// <param name="name"></param>
        /// <param name="identifierTypes"></param>
        /// <returns></returns>
        public IEnumerable<SanScriptIdentifier> FindIdentifiersInFileHashExact(int filenameHashCode, string name, SanScriptIdentifierType identifierTypes = (SanScriptIdentifierType)0xffff)
        {
            SanScriptIdentifierFile idFile;
            if (!this.m_identifierFilesByHashCode.TryGetValue(filenameHashCode, out idFile))
            {
                return Enumerable.Empty<SanScriptIdentifier>();
            }

            idFile.Lock.EnterReadLock();
            try
            {
                ICollection<SanScriptIdentifier> firstLetterList;
                char ch = char.ToUpper(name[0]);
                if (idFile.FirstLetterIdentifiers.TryGetValue(ch, out firstLetterList))
                {
                    return firstLetterList
                        .Where(id => identifierTypes.HasFlag(id.IdentifierType) &&
                                     id.Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                        .ToList();
                }

                return Enumerable.Empty<SanScriptIdentifier>();
            }
            finally
            {
                idFile.Lock.ExitReadLock();
            }
        }

        /// <summary>
        /// Gets a list of all the identifiers for the given file hash.
        /// </summary>
        /// <param name="idFile">The identifier file</param>
        /// <param name="identifierTypes">A bit mask for which <see cref="SanScriptIdentifier"/> types to add to the list.</param>
        /// <returns>A <see cref="List{T}"/> of <see cref="SanScriptIdentifier"/>s.  Empty if nothing found.</returns>
        internal IEnumerable<SanScriptIdentifier> FindIdentifiersInFile(SanScriptIdentifierFile idFile, SanScriptIdentifierType identifierTypes = (SanScriptIdentifierType)0xffff)
        {
            idFile.Lock.EnterReadLock();
            try
            {
                return idFile.FirstLetterIdentifiers
                    .SelectMany(kvp => kvp.Value)
                    .Where(id => identifierTypes.HasFlag(id.IdentifierType))
                    .ToArray();
            }
            finally
            {
                idFile.Lock.ExitReadLock();
            }
        }

        /// <summary>
        /// Either create a new identifier file for the filename, if none exists, or reset the existing one.
        /// </summary>
        /// <param name="filename">The file to reset.</param>
        /// <returns>The identifier file. This may be a new file if the file was never in the cache.</returns>
        internal SanScriptIdentifierFile GetIdentifierFileByName( string filename )
        {
            int updateFilenameHashCode = GetFilenameHashCode(filename);
            SanScriptIdentifierFile idFile =
                this.m_identifierFilesByHashCode.GetOrAdd(
                    updateFilenameHashCode,
                    h =>
                    {
                        ApplicationSettings.Log.Debug("creating new cache file {0}", filename);
                        return new SanScriptIdentifierFile(h);
                    });

            this.m_filenamesByHashCode.TryAdd(updateFilenameHashCode, filename);
            return idFile;
        }

        /// <summary>
        /// Called after updating a file's <see cref="SanScriptIdentifiers"/>.
        /// </summary>
        /// <param name="idFile"></param>
        internal void FinishUpdatingIdentifierFile(SanScriptIdentifierFile idFile)
        {
            idFile.LastUpdated = DateTime.Now;
        }

        /// <summary>
        /// Called anytime between a BeginUpdate and an EndUpdate to add a USING file to the file that is being updated.
        /// </summary>
        /// <param name="filenames"></param>
        internal void AddUsingFile(SanScriptIdentifierFile idFile, string filename )
        {
            int filenameHashCode = GetFilenameHashCode(filename);

            this.m_identifierFilesByHashCode.GetOrAdd(filenameHashCode, h =>
            {
                ApplicationSettings.Log.Debug("Creating new identifier file as using dep: {0}", filename);
                SanScriptIdentifierFile f = new SanScriptIdentifierFile(h);
                return f;
            });
            this.m_filenamesByHashCode.TryAdd(filenameHashCode, filename);
            idFile.AddUsing(filename, filenameHashCode);
        }

        /// <summary>
        /// Called anytime between a BeginUpdate and an EndUpdate to add an identifier to the file that is being updated.
        /// </summary>
        /// <param name="idFile"></param>
        /// <param name="identifier"></param>
        internal void AddIdentifier(SanScriptIdentifierFile idFile, SanScriptIdentifier identifier )
        {
            // add to first letter dictionary
            char ch = char.ToUpper(identifier.Name[0]);
            idFile.Lock.EnterWriteLock();
            try
            {
                ICollection<SanScriptIdentifier> letterList;
                if (!idFile.FirstLetterIdentifiers.TryGetValue(ch, out letterList))
                {
                    letterList = new List<SanScriptIdentifier>();
                    idFile.FirstLetterIdentifiers.Add(ch, letterList);
                }

                letterList.Add(identifier);
            }
            finally
            {
                idFile.Lock.ExitWriteLock();
            }

            // add to word dictionary, starting with the second word
            string[] split = identifier.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 1; i < split.Length; ++i)
            {
                if (Char.IsDigit(split[i][0]))
                {
                    continue;
                }

                idFile.Lock.EnterWriteLock();
                try
                {
                    ICollection<SanScriptIdentifier> wordList;
                    if (!idFile.WordIdentifiers.TryGetValue(split[i], out wordList))
                    {
                        wordList = new List<SanScriptIdentifier>();
                        idFile.WordIdentifiers.Add(split[i], wordList);
                    }

                    wordList.Add(identifier);
                }
                finally
                {
                    idFile.Lock.ExitWriteLock();
                }
            }
        }

        /// <summary>
        /// Remove a list of files and all of their dependencies from the cache if any of them are unused.
        /// </summary>
        /// <param name="filenames"></param>
        public void RemoveFiles(IEnumerable<string> filenames)
        {
            this.RemoveFiles(filenames.Select(GetFilenameHashCode));
        }

        /// <summary>
        /// Remove a list of files (by hash code) and all their dependencies from the cache if any of them are unused.
        /// </summary>
        /// <param name="filenameHashCodes"></param>
        public void RemoveFiles(IEnumerable<int> filenameHashCodes)
        {
            ISet<int> hashesToRemove = new HashSet<int>(filenameHashCodes.Where(this.m_identifierFilesByHashCode.ContainsKey));
            if (!hashesToRemove.Any())
            {
                return;
            }

            // Get all the recursive usings of the files we want to remove.
            // (GetAllUsingsHashCodes builds up the usingHashes collection internally)
            ISet<int> usingHashes = new HashSet<int>();
            hashesToRemove.ForEach(h => this.GetAllUsingsHashCodes(h, usingHashes));
            hashesToRemove.UnionWith(usingHashes);

            // Get all the recursive usings of all files OTHER THAN the above collection.
            usingHashes.Clear();
            this.m_identifierFilesByHashCode.Keys
                .Where(h => !hashesToRemove.Contains(h))
                .ForEach(h => this.GetAllUsingsHashCodes(h, usingHashes));

            // Remove all of the files that were dependencies of outside files. They are still being used.
            hashesToRemove.ExceptWith(usingHashes);

            // Actually remove these files from the dicts now.
            foreach (int hashToRemove in hashesToRemove)
            {
                SanScriptIdentifierFile file;
                this.m_identifierFilesByHashCode.TryRemove(hashToRemove, out file);

                string filename;
                this.m_filenamesByHashCode.TryRemove(hashToRemove, out filename);
            }
        }

        /// <summary>
        /// Clears all files from the cache.
        /// </summary>
        public void Clear()
        {
            this.m_filenamesByHashCode.Clear();
            this.m_identifierFilesByHashCode.Clear();
        }

        /// <summary>
        /// Serializes a file's cache data.
        /// </summary>
        /// <param name="idFile">The file to serialize.</param>
        /// <param name="sourcePath">The source path</param>
        /// <returns></returns>
        internal bool SerializeFile(SanScriptIdentifierFile idFile, string sourcePath)
        {
            idFile.Lock.EnterWriteLock();
            try
            {
                SanScriptIdentifierSerializer serializer = new SanScriptIdentifierSerializer(idFile.FilenameHashCode,
                    this.FindIdentifiersInFile(idFile), idFile.Usings,
                    idFile.ContainsGlobals, idFile.ContainsGlobalsStatement);

                string outputFilename = GetCacheOutputFileName(sourcePath);

                try
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(outputFilename));

                    using (FileStream stream = new FileStream(outputFilename, FileMode.Create, FileAccess.Write,
                        FileShare.None))
                    {
                        BinaryFormatter formatter = new BinaryFormatter
                        {
                            AssemblyFormat = FormatterAssemblyStyle.Simple
                        };
                        formatter.Serialize(stream, serializer);
                        return true;
                    }
                }
                catch (Exception e)
                {
                    ApplicationSettings.Log.ToolException(e, "SerializeFile Exception for '{0}'", outputFilename);
                    return false;
                }
            }
            finally
            {
                idFile.Lock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Attempts to find a cached version of the source file and load it from disk.
        /// </summary>
        /// <param name="sourceFilename">The source file to deserialize</param>
        /// <returns>Whether the deserialization succeeded.</returns>
        public bool DeserializeFile(string sourceFilename)
        {
            string outputFilename = GetCacheOutputFileName(sourceFilename);

            SanScriptIdentifierFile idFile = this.GetIdentifierFileByName( sourceFilename );
            idFile.Lock.EnterWriteLock();
            try
            {
                idFile.Reset();

                if (!File.Exists(outputFilename))
                {
                    ApplicationSettings.Log.Error("DeserializeFile: {0} does not exist.", outputFilename);
                    return false;
                }

                FileStream stream = null;
                SanScriptIdentifierSerializer serializer;
                try
                {
                    stream = new FileStream(outputFilename, FileMode.Open, FileAccess.Read, FileShare.Read);

                    BinaryFormatter formatter = new BinaryFormatter
                    {
                        AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
                    };

                    // Special Hack to retrieve the Type information due to the Deserializer's inability to properly resolve
                    // assemblies that are manually loaded from a directory that is different from the executable.
                    if (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Debugger)
                    {
                        formatter.Binder = new SanScriptIdentifierSerializerBinder();
                    }

                    serializer = formatter.Deserialize(stream) as SanScriptIdentifierSerializer;

                }
                catch (Exception e)
                {
                    ApplicationSettings.Log.ToolException(e, "DeserializeFile Exception for '{0}'", outputFilename);
                    return false;
                }
                finally
                {
                    stream?.Close();
                }

                if (serializer == null)
                {
                    return false;
                }

                if (serializer.FileNameHashCode != idFile.FilenameHashCode)
                {
                    ApplicationSettings.Log.Error("DeserializeFile: FileNameHashCodes do not match!");
                    return false;
                }

                foreach (SanScriptIdentifier identifier in serializer.Identifiers)
                {
                    this.AddIdentifier(idFile, identifier);
                }

                foreach (string u in serializer.Usings)
                {
                    this.AddUsingFile(idFile, u);
                }

                idFile.ContainsGlobals = serializer.ContainsGlobals;
                idFile.ContainsGlobalsStatement = serializer.ContainsGlobalsStatement;

                this.FinishUpdatingIdentifierFile(idFile);
            }
            finally
            {
                idFile.Lock.ExitWriteLock();
            }

            return true;
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Builds a list of dependencies for the given filename
        /// </summary>
        /// <param name="filename">The name of the file in question.</param>
        /// <param name="usingFiles">A <see cref="Dictionary{TKey,TValue}"/> of the dependencies.  The key is the filename, the value is the index.</param>
        /// <param name="topFilename"></param>
        private void GetAllUsingsInternal( string filename, IDictionary<string, int> usingFiles, string topFilename )
        {
            SanScriptIdentifierFile idFile;
            if (!this.m_identifierFilesByHashCode.TryGetValue(GetFilenameHashCode(filename),
                out idFile))
            {
                return;
            }

            idFile.Lock.EnterReadLock();
            try
            {
                foreach (string usingFile in idFile.Usings)
                {
                    if (usingFiles.ContainsKey(usingFile) || usingFile.Equals(topFilename, StringComparison.OrdinalIgnoreCase))
                    {
                        continue;
                    }

                    usingFiles.Add(usingFile, usingFiles.Count);
                    this.GetAllUsingsInternal(usingFile, usingFiles, topFilename);
                }
            }
            finally
            {
                idFile.Lock.ExitReadLock();
            }
        }

        /// <summary>
        /// Builds a list of dependencies for the given filenameHashCode
        /// </summary>
        /// <param name="filenameHashCode">The hash code of the file in question.</param>
        /// <param name="usingsHashCodes">A <see cref="ISet{T}"/> of the dependencies.</param>
        /// <param name="topFilenameHashCode"></param>
        private void GetAllUsingsHashCodesInternal( int filenameHashCode, ISet<int> usingsHashCodes, int topFilenameHashCode )
        {
            SanScriptIdentifierFile idFile;
            if (!this.m_identifierFilesByHashCode.TryGetValue(filenameHashCode, out idFile))
            {
                return;
            }

            idFile.Lock.EnterReadLock();
            try
            {
                foreach (int hashCode in idFile.UsingsHashCodes)
                {
                    if (usingsHashCodes.Contains(hashCode) || hashCode == topFilenameHashCode)
                    {
                        continue;
                    }

                    usingsHashCodes.Add(hashCode);
                    this.GetAllUsingsHashCodesInternal(hashCode, usingsHashCodes, topFilenameHashCode);
                }
            }
            finally
            {
                idFile.Lock.ExitReadLock();
            }
        }
        #endregion
    }
}