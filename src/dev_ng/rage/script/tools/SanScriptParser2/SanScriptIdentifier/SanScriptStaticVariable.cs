//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptStaticVariable.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;

namespace SanScriptParser
{
    /// <summary>
    /// Holds a static variable declaration.
    /// </summary>
    [Serializable]
    public class SanScriptStaticVariable : SanScriptIdentifier
    {
        protected SanScriptStaticVariable()
        {
        }

        public SanScriptStaticVariable( string name )
            : base( name, SanScriptTokenID.UserIdentifier )
        {
        }

        public SanScriptStaticVariable( string name, string type, int filenameHashCode, int startOffset,
            string displayText, string markupText )
            : base( name, SanScriptTokenID.UserIdentifier, 255,
            filenameHashCode, startOffset, displayText, markupText )
        {
            m_type = type;
        }

        #region Constants
        public const string sm_HelpTypeGroupName = "variable";
        #endregion

        #region Variables
        private string m_type;
        #endregion

        #region Properties
        public string Type
        {
            get
            {
                return m_type;
            }
        }
        #endregion

        #region SanScriptIdentifier Overrides
        public override SanScriptIdentifierType IdentifierType
        {
            get
            {
                return SanScriptIdentifierType.StaticVariable;
            }
        }

        public override string HelpTypeGroupName
        {
            get
            {
                return sm_HelpTypeGroupName;
            }
        }
        #endregion
    }
}
