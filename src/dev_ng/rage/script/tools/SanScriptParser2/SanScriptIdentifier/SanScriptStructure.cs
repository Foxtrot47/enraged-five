//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptStructure.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace SanScriptParser
{
   /// <summary>
    /// Holds a STRUCT definition.
    /// </summary>
    [Serializable]
    public class SanScriptStructure : SanScriptIdentifier
    {
        protected SanScriptStructure()
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicStructure;
        }
        
        public SanScriptStructure( string name )
            : base( name, SanScriptTokenID.UserIdentifier )
        {
            m_imageIndex = (byte)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicStructure;
        }

        public SanScriptStructure( string name, int filenameHashCode, int startOffset,
            string displayText, string markupText )
            : base( name, SanScriptTokenID.UserIdentifier, (int)ActiproSoftware.Products.SyntaxEditor.IconResource.PublicStructure, 
            filenameHashCode, startOffset, displayText, markupText )
        {
        }

        #region Constants
        public const string sm_HelpTypeGroupName = "struct";
        #endregion

        #region Variables
        private List<SanScriptStructMember> m_members = new List<SanScriptStructMember>();
        #endregion

        #region Properties
        public List<SanScriptStructMember> Members
        {
            get
            {
                return m_members;
            }
        }
        #endregion

        #region SanScriptIdentifier Overrides
        public override string HelpTypeGroupName
        {
            get
            {
                return sm_HelpTypeGroupName;
            }
        }

        public override SanScriptIdentifierType IdentifierType
        {
            get
            {
                return SanScriptIdentifierType.StructType;
            }
        }

        public override string GetDisplayTextForHelp()
        {
            StringBuilder helpText = new StringBuilder( base.GetDisplayTextForHelp() );

            if ( m_members.Count > 0 )
            {
                helpText.Append( "\n" );
            }

            foreach ( SanScriptStructMember member in m_members )
            {
                helpText.Append( "\n\t" );
                helpText.Append( member.GetDisplayTextForHelp() );
            }

            return helpText.ToString();
        }
        #endregion
    }
}
