//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptIdentifierSerializer.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;

namespace SanScriptParser
{
    [Serializable]
    public class SanScriptIdentifierSerializer
    {
        public SanScriptIdentifierSerializer()
        {

        }

        public SanScriptIdentifierSerializer( int filenameHashCode, IEnumerable<SanScriptIdentifier> identifiers, IEnumerable<string> usings,
            bool containsGlobals, bool containsGlobalsStatement )
        {
            this.m_filenameHashCode = filenameHashCode;
            this.m_identifiers = identifiers.ToList();
            this.m_usings = usings.ToList();
            this.m_containsGlobals = containsGlobals;
            this.m_containsGlobalsStatement = containsGlobalsStatement;
        }

        #region Variables
        private int m_filenameHashCode = 0;
        private List<SanScriptIdentifier> m_identifiers = new List<SanScriptIdentifier>();
        private List<string> m_usings = new List<string>();
        private bool m_containsGlobals = false;
        private bool m_containsGlobalsStatement = false;
        #endregion

        #region Properties
        public int FileNameHashCode
        {
            get
            {
                return this.m_filenameHashCode;
            }
            set
            {
                this.m_filenameHashCode = value;
            }
        }

        public List<SanScriptIdentifier> Identifiers
        {
            get
            {
                return this.m_identifiers;
            }
            set
            {
                this.m_identifiers.Clear();
                if ( value != null )
                {
                    this.m_identifiers.AddRange( value );
                }
            }
        }

        public List<string> Usings
        {
            get
            {
                return this.m_usings;
            }
            set
            {
                this.m_usings.Clear();
                if ( value != null )
                {
                    this.m_usings.AddRange( value );
                }
            }
        }

        public bool ContainsGlobals
        {
            get
            {
                return this.m_containsGlobals;
            }
            set
            {
                this.m_containsGlobals = value;
            }
        }

        public bool ContainsGlobalsStatement
        {
            get
            {
                return this.m_containsGlobalsStatement;
            }
            set
            {
                this.m_containsGlobalsStatement = value;
            }
        }
        #endregion
    }
}