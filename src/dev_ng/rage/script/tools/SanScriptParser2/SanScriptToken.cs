using System;

using ActiproSoftware.SyntaxEditor;
using ragScriptEditorShared;

namespace SanScriptParser
{
    /// <summary>
    /// Represents a <c>Simple</c> <see cref="IToken"/>.
    /// </summary>
    internal class SanScriptToken : MergableToken
    {
        /// <summary>
        /// Initializes a new instance of the <c>SimpleToken</c> class.
        /// </summary>
        /// <param name="startOffset">The start offset of the token.</param>
        /// <param name="length">The length of the token.</param>
        /// <param name="lexicalParseFlags">The <see cref="LexicalParseFlags"/> for the token.</param>
        /// <param name="parentToken">The <see cref="IToken"/> that starts the current state scope specified by the <see cref="IToken.LexicalState"/> property.</param>
        /// <param name="lexicalParseData">The <see cref="ITokenLexicalParseData"/> that contains lexical parse information about the token.</param>
        public SanScriptToken( int startOffset, int length, LexicalParseFlags lexicalParseFlags,
            IToken parentToken, ITokenLexicalParseData lexicalParseData )
            : base( startOffset, length, lexicalParseFlags, parentToken, lexicalParseData )
        {
            this.SetFlag( LexicalParseFlags.ScopeStart, this.IsPairedStart );
            this.SetFlag( LexicalParseFlags.ScopeEnd, this.IsPairedEnd );
        }

        #region Variables
        private byte m_overrideID = (byte)(SanScriptTokenID.Invalid | 0x80);
        #endregion

        /// <summary>
        /// Clones the data in the <see cref="IToken"/>.
        /// </summary>
        /// <param name="startOffset">The <see cref="IToken.StartOffset"/> of the cloned object.</param>
        /// <param name="length">The length of the cloned object.</param>
        /// <returns>The <see cref="IToken"/> that was created.</returns>
        public override IToken Clone( int startOffset, int length )
        {
            return new SanScriptToken( startOffset, length, this.LexicalParseFlags, this.ParentToken, this.LexicalParseData );
        }

        #region MergableToken Overrides
        /// <summary>
        /// Gets or sets the type of content divider to apply to the token. 
        /// </summary>
        /// <value>
        /// A <see cref="TokenContentDividerType"/> indicating the type of content divider to apply to the token.
        /// </value>
        public override TokenContentDividerType ContentDividerType
        {
            get
            {
                switch ( this.ID )
                {
                    case SanScriptTokenID.ENDENUM:
                    case SanScriptTokenID.ENDHSMEVENT:
                    case SanScriptTokenID.ENDFUNC:
                    case SanScriptTokenID.ENDGLOBALS:
                    case SanScriptTokenID.ENDPROC:
                    case SanScriptTokenID.ENDSCRIPT:
                    case SanScriptTokenID.ENDHSMSTATE:
                    case SanScriptTokenID.ENDSTRUCT:
                        return TokenContentDividerType.After;
                    default:
                        return TokenContentDividerType.None;
                }
            }
            set
            {
                // do nothing
            }
        }

        /// <summary>
        /// Gets the resolved type of content divider to apply to the token.
        /// </summary>
        /// <value>
        /// A <see cref="TokenContentDividerType"/> indicating the type of content divider to apply to the token.
        /// </value>
        public override TokenContentDividerType ContentDividerTypeResolved
        {
            get 
            {
                return this.ContentDividerType;
            }
        }

        /// <summary>
        /// Gets the ID assigned to the token.
        /// </summary>
        /// <value>
        /// The ID assigned to the token.
        /// </value>
        public override int ID
        {
            get
            {
                byte enabled = (byte)(m_overrideID & 0x80);
                byte overrideID = (byte)(m_overrideID & 0x3f);
                if ( enabled == 0x80 )
                {
                    if ( (int)overrideID != SanScriptTokenID.Invalid )
                    {
                        return (int)overrideID;
                    }

                    return base.ID;
                }
                else
                {
                    return SanScriptTokenID.Disabled;
                }
            }
        }
        /// <summary>
        /// Gets whether the token represents a comment.
        /// </summary>
        /// <value>
        /// <c>true</c> if the token represents a comment; otherwise <c>false</c>.
        /// </value>
        public override bool IsComment
        {
            get
            {
                switch ( this.ID )
                {
                    case SanScriptTokenID.SingleLineComment:
                    case SanScriptTokenID.SingleLineCommentEnd:
                    case SanScriptTokenID.SingleLineCommentStart:
                    case SanScriptTokenID.MultiLineComment:
                    case SanScriptTokenID.MultiLineCommentEnd:
                    case SanScriptTokenID.MultiLineCommentStart:
                        return true;
                    default:
                        return false;
                }
            }
        }

        /// <summary>
        /// Gets whether the token marks the end of the document.
        /// </summary>
        /// <value>
        /// <c>true</c> if the token marks the end of the document; otherwise <c>false</c>.
        /// </value>
        public override bool IsDocumentEnd
        {
            get
            {
                return (this.ID == SanScriptTokenID.DocumentEnd);
            }
        }

        /// <summary>
        /// Gets whether the token marks an invalid range of text.
        /// </summary>
        /// <value>
        /// <c>true</c> if the token marks invalid range of text; otherwise <c>false</c>.
        /// </value>
        public override bool IsInvalid
        {
            get
            {
                return (this.ID == SanScriptTokenID.Invalid);
            }
        }

        /// <summary>
        /// Gets whether the <see cref="IToken"/> is the end <see cref="IToken"/> of an <see cref="IToken"/> pair.
        /// </summary>
        /// <value>
        /// <c>true</c> if the <see cref="IToken"/> is the end <see cref="IToken"/> of an <see cref="IToken"/> pair; otherwise, <c>false</c>.
        /// </value>
        public override bool IsPairedEnd
        {
            get
            {
                return SanScriptToken.GetIsPairedEnd( this.ID );
            }
        }

        /// <summary>
        /// Gets whether the <see cref="IToken"/> is the start <see cref="IToken"/> of an <see cref="IToken"/> pair.
        /// </summary>
        /// <value>
        /// <c>true</c> if the <see cref="IToken"/> is the start <see cref="IToken"/> of an <see cref="IToken"/> pair; otherwise, <c>false</c>.
        /// </value>
        /// <remarks>
        /// A token pair is generally a pair of brackets.
        /// </remarks>
        public override bool IsPairedStart
        {
            get
            {
                return SanScriptToken.GetIsPairedStart( this.ID );
            }
        }

        /// <summary>
        /// Gets whether the token represents whitespace.
        /// </summary>
        /// <value>
        /// <c>true</c> if the token represents whitespace; otherwise <c>false</c>.
        /// </value>
        public override bool IsWhitespace
        {
            get
            {
                switch ( this.ID )
                {
                    case SanScriptTokenID.Whitespace:
                    case SanScriptTokenID.LineTerminator:
                    case SanScriptTokenID.DocumentEnd:
                        return true;
                    default:
                        return false;
                }
            }
        }

        /// <summary>
        /// Gets the key assigned to the token.
        /// </summary>
        /// <value>The key assigned to the token.</value>
        public override string Key
        {
            get
            {
                return SanScriptTokenID.GetTokenKey( this.ID );
            }
        }

        /// <summary>
        /// Gets the ID of the <see cref="IToken"/> that matches this <see cref="IToken"/> if this token is paired.
        /// </summary>
        /// <value>The ID of the <see cref="IToken"/> that matches this <see cref="IToken"/> if this token is paired.</value>
        public override int MatchingTokenID
        {
            get
            {
                return SanScriptToken.GetMatchingTokenID( this.ID );
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Overrides the original ID of this token.  Setting back to SanScriptTokenID.Invalid will cancel the override.
        /// </summary>
        /// <param name="tokenID">The new ID of the token.</param>
        public void SetOverrideID( int tokenID )
        {
            m_overrideID = (byte)((byte)tokenID | (m_overrideID & 0x80));

            this.SetFlag( LexicalParseFlags.ScopeStart, this.IsPairedStart );
            this.SetFlag( LexicalParseFlags.ScopeEnd, this.IsPairedEnd );
        }

        /// <summary>
        /// Overrides the original and the override ID to make this token Disabled
        /// </summary>
        /// <param name="enable"></param>
        public void SetEnabled( bool enable )
        {
            m_overrideID = (byte)((m_overrideID & 0x3f) | (enable ? 0x80 : 0x00));
        }
        #endregion

        #region Static Functions
        public static bool GetIsPairedStart( int tokenID )
        {
            switch ( tokenID )
            {
                case SanScriptTokenID.HSMACTIVATE:
                case SanScriptTokenID.HSMDEACTIVATE:
                case SanScriptTokenID.ENUM:
                case SanScriptTokenID.HASH_ENUM:
                case SanScriptTokenID.STRICT_ENUM:
                case SanScriptTokenID.HSMEVENT:
                case SanScriptTokenID.FOR:
                case SanScriptTokenID.FUNC:
                case SanScriptTokenID.GLOBALS:
                case SanScriptTokenID.PROC:
                case SanScriptTokenID.REPEAT:
                case SanScriptTokenID.HSMSTATE:
                case SanScriptTokenID.SCRIPT:
                case SanScriptTokenID.STRUCT:
                case SanScriptTokenID.SWITCH:
                case SanScriptTokenID.WHILE:
                case SanScriptTokenID.OpenBracket:
                case SanScriptTokenID.OpenParenthesis:
                case SanScriptTokenID.OpenString:
                case SanScriptTokenID.OpenVector:
                    return true;

                case SanScriptTokenID.IF:
                    return true;

                case SanScriptTokenID.CASE:
                case SanScriptTokenID.DEFAULT:
                case SanScriptTokenID.ELIF:
                case SanScriptTokenID.ELSE:
                    return true;

                case SanScriptTokenID.PoundIf:
                    return true;

                default:
                    return false;
            }
        }

        public static bool GetIsPairedEnd( int tokenID )
        {
            switch ( tokenID )
            {
                case SanScriptTokenID.ENDGLOBALS:
                case SanScriptTokenID.ENDHSMACTIVATE:
                case SanScriptTokenID.ENDHSMDEACTIVATE:
                case SanScriptTokenID.ENDENUM:
                case SanScriptTokenID.ENDHSMEVENT:
                case SanScriptTokenID.ENDFOR:
                case SanScriptTokenID.ENDFUNC:
                case SanScriptTokenID.ENDIF:
                case SanScriptTokenID.ENDPROC:
                case SanScriptTokenID.ENDREPEAT:
                case SanScriptTokenID.ENDSCRIPT:
                case SanScriptTokenID.ENDHSMSTATE:
                case SanScriptTokenID.ENDSTRUCT:
                case SanScriptTokenID.ENDSWITCH:
                case SanScriptTokenID.ENDWHILE:
                case SanScriptTokenID.CloseBracket:
                case SanScriptTokenID.CloseParenthesis:
                case SanScriptTokenID.CloseString:
                case SanScriptTokenID.CloseVector:
                    return true;

                case SanScriptTokenID.BREAK:
                case SanScriptTokenID.BREAKLOOP:
                case SanScriptTokenID.RELOOP:
                case SanScriptTokenID.FALLTHRU:
                case SanScriptTokenID.EXIT:
                case SanScriptTokenID.RETURN:
                case SanScriptTokenID.ELIF:
                case SanScriptTokenID.ELSE:
                    return true;

                case SanScriptTokenID.PoundEndif:
                    return true;

                default:
                    return false;
            }
        }

        public static int GetMatchingTokenID( int tokenID )
        {
            switch ( tokenID )
            {
                case SanScriptTokenID.GLOBALS:
                    return SanScriptTokenID.ENDGLOBALS;
                case SanScriptTokenID.HSMACTIVATE:
                    return SanScriptTokenID.ENDHSMACTIVATE;
                case SanScriptTokenID.HSMDEACTIVATE:
                    return SanScriptTokenID.ENDHSMDEACTIVATE;
                case SanScriptTokenID.ENUM:
                    return SanScriptTokenID.ENDENUM;
                case SanScriptTokenID.HASH_ENUM:
                    return SanScriptTokenID.ENDENUM;
                case SanScriptTokenID.STRICT_ENUM:
                    return SanScriptTokenID.ENDENUM;
                case SanScriptTokenID.HSMEVENT:
                    return SanScriptTokenID.ENDHSMEVENT;
                case SanScriptTokenID.FOR:
                    return SanScriptTokenID.ENDFOR;
                case SanScriptTokenID.FUNC:
                    return SanScriptTokenID.ENDFUNC;
                case SanScriptTokenID.PROC:
                    return SanScriptTokenID.ENDPROC;
                case SanScriptTokenID.REPEAT:
                    return SanScriptTokenID.ENDREPEAT;
                case SanScriptTokenID.HSMSTATE:
                    return SanScriptTokenID.ENDHSMSTATE;
                case SanScriptTokenID.SCRIPT:
                    return SanScriptTokenID.ENDSCRIPT;
                case SanScriptTokenID.STRUCT:
                    return SanScriptTokenID.ENDSTRUCT;
                case SanScriptTokenID.SWITCH:
                    return SanScriptTokenID.ENDSWITCH;
                case SanScriptTokenID.WHILE:
                    return SanScriptTokenID.ENDWHILE;
                case SanScriptTokenID.ENDGLOBALS:
                    return SanScriptTokenID.GLOBALS;
                case SanScriptTokenID.ENDHSMACTIVATE:
                    return SanScriptTokenID.HSMACTIVATE;
                case SanScriptTokenID.ENDHSMDEACTIVATE:
                    return SanScriptTokenID.HSMDEACTIVATE;
                case SanScriptTokenID.ENDENUM:
                    return SanScriptTokenID.ENUM;
                case SanScriptTokenID.ENDHSMEVENT:
                    return SanScriptTokenID.HSMEVENT;
                case SanScriptTokenID.ENDFOR:
                    return SanScriptTokenID.FOR;
                case SanScriptTokenID.ENDFUNC:
                    return SanScriptTokenID.FUNC;
                case SanScriptTokenID.ENDPROC:
                    return SanScriptTokenID.PROC;
                case SanScriptTokenID.ENDREPEAT:
                    return SanScriptTokenID.REPEAT;
                case SanScriptTokenID.ENDSCRIPT:
                    return SanScriptTokenID.SCRIPT;
                case SanScriptTokenID.ENDHSMSTATE:
                    return SanScriptTokenID.HSMSTATE;
                case SanScriptTokenID.ENDSTRUCT:
                    return SanScriptTokenID.STRUCT;
                case SanScriptTokenID.ENDSWITCH:
                    return SanScriptTokenID.SWITCH;
                case SanScriptTokenID.ENDWHILE:
                    return SanScriptTokenID.WHILE;
                case SanScriptTokenID.CloseBracket:
                    return SanScriptTokenID.OpenBracket;
                case SanScriptTokenID.CloseParenthesis:
                    return SanScriptTokenID.OpenParenthesis;
                case SanScriptTokenID.CloseString:
                    return SanScriptTokenID.OpenString;
                case SanScriptTokenID.CloseVector:
                    return SanScriptTokenID.OpenVector;
                case SanScriptTokenID.OpenBracket:
                    return SanScriptTokenID.CloseBracket;
                case SanScriptTokenID.OpenParenthesis:
                    return SanScriptTokenID.CloseParenthesis;
                case SanScriptTokenID.OpenString:
                    return SanScriptTokenID.CloseString;
                case SanScriptTokenID.OpenVector:
                    return SanScriptTokenID.CloseVector;

                /* We programatically determine the actual matching token in the SyntaxLanguage
                case SanScriptTokenID.IF:
                case SanScriptTokenID.BREAK:
                case SanScriptTokenID.CASE:
                case SanScriptTokenID.DEFAULT:
                case SanScriptTokenID.FALLTHRU:
                case SanScriptTokenID.EXIT:
                case SanScriptTokenID.RETURN:
                case SanScriptTokenID.ELIF:
                case SanScriptTokenID.ELSE:
                case SanScriptTokenID.ENDIF:
                case SanScriptTokenID.PoundEndif:
                case SanScriptTokenID.PoundIfdef:
                case SanScriptTokenID.PoundIfndef:
                */

                default:
                    return SanScriptTokenID.Invalid;
            }
        }
        #endregion
    }
}
