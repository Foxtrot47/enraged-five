namespace SanScriptParser
{
    partial class SanScriptLanguageSettingsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ragScriptEditorShared.LanguageSettings languageSettings1 = new ragScriptEditorShared.LanguageSettings();
            this.languageHelpFileLabel = new System.Windows.Forms.Label();
            this.languageHelpFileTextBox = new System.Windows.Forms.TextBox();
            this.languageHelpFileBrowseButton = new System.Windows.Forms.Button();
            this.referenceHelpFileLabel = new System.Windows.Forms.Label();
            this.referenceHelpFileTextBox = new System.Windows.Forms.TextBox();
            this.referenceHelpFileBrowseButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // languageHelpFileLabel
            // 
            this.languageHelpFileLabel.AutoSize = true;
            this.languageHelpFileLabel.Location = new System.Drawing.Point( 5, 111 );
            this.languageHelpFileLabel.Name = "languageHelpFileLabel";
            this.languageHelpFileLabel.Size = new System.Drawing.Size( 99, 13 );
            this.languageHelpFileLabel.TabIndex = 3;
            this.languageHelpFileLabel.Text = "Language Help File";
            // 
            // languageHelpFileTextBox
            // 
            this.languageHelpFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.languageHelpFileTextBox.Location = new System.Drawing.Point( 110, 108 );
            this.languageHelpFileTextBox.Name = "languageHelpFileTextBox";
            this.languageHelpFileTextBox.Size = new System.Drawing.Size( 334, 20 );
            this.languageHelpFileTextBox.TabIndex = 4;
            this.languageHelpFileTextBox.TextChanged += new System.EventHandler( this.SanScriptSettingChanged );
            // 
            // languageHelpFileBrowseButton
            // 
            this.languageHelpFileBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.languageHelpFileBrowseButton.Location = new System.Drawing.Point( 450, 106 );
            this.languageHelpFileBrowseButton.Name = "languageHelpFileBrowseButton";
            this.languageHelpFileBrowseButton.Size = new System.Drawing.Size( 25, 23 );
            this.languageHelpFileBrowseButton.TabIndex = 5;
            this.languageHelpFileBrowseButton.Text = "...";
            this.languageHelpFileBrowseButton.UseVisualStyleBackColor = true;
            this.languageHelpFileBrowseButton.Click += new System.EventHandler( this.languageHelpFileBrowseButton_Click );
            // 
            // referenceHelpFileLabel
            // 
            this.referenceHelpFileLabel.AutoSize = true;
            this.referenceHelpFileLabel.Location = new System.Drawing.Point( 3, 137 );
            this.referenceHelpFileLabel.Name = "referenceHelpFileLabel";
            this.referenceHelpFileLabel.Size = new System.Drawing.Size( 101, 13 );
            this.referenceHelpFileLabel.TabIndex = 6;
            this.referenceHelpFileLabel.Text = "Reference Help File";
            // 
            // referenceHelpFileTextBox
            // 
            this.referenceHelpFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.referenceHelpFileTextBox.Location = new System.Drawing.Point( 110, 134 );
            this.referenceHelpFileTextBox.Name = "referenceHelpFileTextBox";
            this.referenceHelpFileTextBox.Size = new System.Drawing.Size( 334, 20 );
            this.referenceHelpFileTextBox.TabIndex = 7;
            this.referenceHelpFileTextBox.TextChanged += new System.EventHandler( this.SanScriptSettingChanged );
            // 
            // referenceHelpFileBrowseButton
            // 
            this.referenceHelpFileBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.referenceHelpFileBrowseButton.Location = new System.Drawing.Point( 450, 135 );
            this.referenceHelpFileBrowseButton.Name = "referenceHelpFileBrowseButton";
            this.referenceHelpFileBrowseButton.Size = new System.Drawing.Size( 25, 23 );
            this.referenceHelpFileBrowseButton.TabIndex = 8;
            this.referenceHelpFileBrowseButton.Text = "...";
            this.referenceHelpFileBrowseButton.UseVisualStyleBackColor = true;
            this.referenceHelpFileBrowseButton.Click += new System.EventHandler( this.referenceHelpFileBrowseButton_Click );
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // SanScriptLanguageSettingsControl
            // 
            this.Controls.Add( this.languageHelpFileBrowseButton );
            this.Controls.Add( this.languageHelpFileTextBox );
            this.Controls.Add( this.referenceHelpFileTextBox );
            this.Controls.Add( this.referenceHelpFileLabel );
            this.Controls.Add( this.referenceHelpFileBrowseButton );
            this.Controls.Add( this.languageHelpFileLabel );
            this.Name = "SanScriptLanguageSettingsControl";
            this.Size = new System.Drawing.Size( 478, 169 );
            languageSettings1.CacheDirectory = "";
            languageSettings1.CodeSnippetPath = "";
            languageSettings1.CodeSnippetPaths = new string[0];
            this.TabSettings = languageSettings1;
            this.Controls.SetChildIndex( this.languageHelpFileLabel, 0 );
            this.Controls.SetChildIndex( this.referenceHelpFileBrowseButton, 0 );
            this.Controls.SetChildIndex( this.referenceHelpFileLabel, 0 );
            this.Controls.SetChildIndex( this.referenceHelpFileTextBox, 0 );
            this.Controls.SetChildIndex( this.languageHelpFileTextBox, 0 );
            this.Controls.SetChildIndex( this.languageHelpFileBrowseButton, 0 );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label languageHelpFileLabel;
        private System.Windows.Forms.TextBox languageHelpFileTextBox;
        private System.Windows.Forms.Button languageHelpFileBrowseButton;
        private System.Windows.Forms.Label referenceHelpFileLabel;
        private System.Windows.Forms.TextBox referenceHelpFileTextBox;
        private System.Windows.Forms.Button referenceHelpFileBrowseButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}
