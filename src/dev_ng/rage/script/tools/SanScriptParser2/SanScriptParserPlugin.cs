//#define OUTPUT_CLEAR_PARSE_REQUESTS

using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

using ragScriptEditorShared;
using ActiproSoftware.SyntaxEditor;

namespace SanScriptParser
{
    public class SanScriptParserPlugin : IParserPlugin
    {
        public SanScriptParserPlugin()
        {
            m_extensions.Add( ".sc" );
            m_extensions.Add( ".sch" );

            m_compilableExtensions.Add( ".sc" );

            SanScriptProjectResolver.ParserPlugin = this;
        }

        #region Constants
        private const string c_settingsFileName = "SanScript.Options.xml";
        #endregion

        #region Variables
        private Guid m_guid = Guid.NewGuid();
        private string m_directory = string.Empty;
        private List<string> m_extensions = new List<string>();
        private List<string> m_compilableExtensions = new List<string>();

        private string m_includeFile = string.Empty;
        private IDictionary<string, string> m_includePathFiles = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        private ISet<string> m_includePaths = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        SanScriptLanguageSettings m_currentLanguageSettings = new SanScriptLanguageSettings();
        SanScriptLanguageDefaultEditorSettings m_defaultEditorSettings = new SanScriptLanguageDefaultEditorSettings();

        List<IParserProjectResolver> m_projectResolvers = new List<IParserProjectResolver>();

        private string m_custom = string.Empty;
        #endregion

        #region IParserPlugin overrides
        public List<string> CompilableExtensions
        {
            get
            {
                return m_compilableExtensions;
            }
        }

        public LanguageSettings CurrentLanguageSettings
        {
            get
            {
                return m_currentLanguageSettings;
            }
        }

        public LanguageDefaultEditorSettingsBase DefaultEditorSettings
        {
            get
            {
                return m_defaultEditorSettings;
            }
        }

        public string IncludeFile
        {
            get
            {
                if ( (m_includeFile == null) || (m_includeFile == string.Empty) )
                {
                    return m_includeFile;
                }

                string fullPathName = GetFullPathName( m_includeFile );
                if ( fullPathName != null )
                {
                    return fullPathName;
                }

                return m_includeFile;
            }
            set
            {
                string includeFilePath = this.IncludeFile;
                if ( !string.IsNullOrEmpty(includeFilePath)
                    && (!includeFilePath.Equals(value, StringComparison.OrdinalIgnoreCase)))
                {
                    if ( (includeFilePath != null) && !IsAProjectFile( includeFilePath )
                        && !m_includePathFiles.Values.Contains( includeFilePath ) )
                    {
                        SanScriptProjectResolver.ParseCoordinator.RemoveFilesFromCache( includeFilePath );
                    }
                }

                if ( value != null )
                {
                    m_includeFile = value.ToLower();
                    includeFilePath = this.IncludeFile;
                    if ( !string.IsNullOrEmpty(includeFilePath) && !IsAProjectFile( includeFilePath )
                        && !m_includePathFiles.Values.Contains( includeFilePath ) )
                    {
                        SanScriptProjectResolver.ParseCoordinator.RequestIdentifiersForFiles(includeFilePath);
                    }
                }
                else
                {
                    m_includeFile = null;
                }
            }
        }

        public IEnumerable<string> IncludePathFiles
        {
            get
            {
                return m_includePathFiles.Values;
            }
        }

        private object _includePathLockObj = new object();

        public IEnumerable<string> IncludePaths
        {
            get
            {
                return m_includePaths;
            }
            set
            {
                IEnumerable<string> oldIncludePathFiles = m_includePathFiles.Values.ToArray();
                m_includePathFiles.Clear();

                lock (_includePathLockObj)
                {
                    m_includePaths.Clear();
                    foreach (string includePath in value)
                    {
                        string fullPath = ApplicationSettings.GetAbsolutePath(includePath);
                        if (Directory.Exists(fullPath))
                        {
                            m_includePaths.Add(fullPath);
                        }
                        else
                        {
                            m_includePaths.Add(includePath);
                        }
                    }

                    // collect include files
                    foreach (string path in m_includePaths)
                    {
                        if (Directory.Exists(path))
                        {
                            PopulateIncludePathFiles(path, m_includePathFiles);
                        }
                    }
                }

                this.IncludeFile = m_includeFile;   // re-evaluate the include file, in case it is path-relative
                string includeFilePath = this.IncludeFile;

                // remove old references
                string[] oldIncludePathFilesToRemoveFromCache = oldIncludePathFiles
                    .Where(f => !m_includePathFiles.Values.Contains(f) && !IsAProjectFile(f)
                        && !f.Equals(includeFilePath, StringComparison.OrdinalIgnoreCase)).ToArray();

                if (oldIncludePathFilesToRemoveFromCache.Any())
                {
                    SanScriptProjectResolver.ParseCoordinator.RemoveFilesFromCache(oldIncludePathFilesToRemoveFromCache);
                }

                // add files in again. Files already in the list need to be re-evaluated in case the cache is newer
                String[] includeFilesToAddToCache = m_includePathFiles.Values
                    .Where(f => !IsAProjectFile(f) && !f.Equals(includeFilePath, StringComparison.OrdinalIgnoreCase)).ToArray();

                if (includeFilesToAddToCache.Any())
                {
                    SanScriptProjectResolver.ParseCoordinator.RequestIdentifiersForFiles(includeFilesToAddToCache);
                }
            }
        }

        public List<string> Extensions
        {
            get
            {
                return m_extensions;
            }
        }

        public string LanguageName
        {
            get
            {
                return "SanScript";
            }
        }

        public List<IParserProjectResolver> ProjectResolvers
        {
            get
            {
                return m_projectResolvers;
            }
        }

        public string Custom
        {
            get { return m_custom; }
            set { m_custom = value; }
        }

        public IParserProjectResolver CreateProjectResolver( List<string> projectFiles )
        {
            SanScriptProjectResolver projectResolver = new SanScriptProjectResolver();
            projectResolver.ProjectFiles = projectFiles;

            m_projectResolvers.Add( projectResolver );

            return projectResolver;
        }

        public IParserProjectResolver CreateProjectResolver( string filename )
        {
            List<string> filenames = new List<string>();
            filenames.Add( filename );
            return CreateProjectResolver( filenames );
        }

        public void DestroyProjectResolver( IParserProjectResolver projectResolver )
        {
            m_projectResolvers.Remove( projectResolver );

            // Remove all cached files that aren't associated with *other* project resolvers.
            SanScriptProjectResolver.ParseCoordinator.RemoveFilesFromCache(projectResolver.ProjectFiles.Where(f => !IsAProjectFile(f)).ToArray());

            DisposeProjectResolver( projectResolver );
        }

        public void InitPlugin( string directory )
        {
            m_directory = directory;

            // set Rage defaults
            m_defaultEditorSettings.CompilingSettingsList[0].Reset();
            m_defaultEditorSettings.Language.Reset();

            LoadSettings();
        }

        public bool LoadSettings()
        {
            string filename = Path.Combine( m_directory, c_settingsFileName );
            if ( !File.Exists( filename ) )
            {
                return false;
            }

            // see if it's the old layout file
            XmlTextReader reader = null;
            float version = 0.0f;
            try
            {
                reader = new XmlTextReader( filename );

                while ( reader.Read() )
                {
                    if ( reader.Name == "Version" )
                    {
                        if ( reader.IsStartElement() )
                        {
                            version = float.Parse( reader.ReadString() );
                        }

                        break;
                    }
                }

                reader.Close();
            }
            catch ( Exception e )
            {
                if ( reader != null )
                {
                    reader.Close();
                }

                throw e;
            }

            if ( version == SanScriptLanguageDefaultEditorSettings.SettingsVersion )
            {
                // if we got here, we have a new layout file
                string error = null;
                if ( !m_defaultEditorSettings.LoadFile( filename, out error ) )
                {
                    throw (new Exception( "Error Loading Settings File " + error ));
                }
            }
            else
            {
                throw (new Exception( "Unable to load a settings file version " + version + "." ));
            }

            SanScriptProjectResolver.SpecialIdentifiers = m_defaultEditorSettings.SpecialIdentifiers;

            // save default to our temporaries
            this.IncludePaths = m_defaultEditorSettings.CompilingSettingsList[0].IncludePaths;
            this.IncludeFile = m_defaultEditorSettings.CompilingSettingsList[0].XmlIncludeFile;
            this.Custom = m_defaultEditorSettings.CompilingSettingsList[0].Custom;
            this.CurrentLanguageSettings.Copy(m_defaultEditorSettings.Language);

            return true;
        }

        public bool SaveSettings()
        {
            // bring our temporaries back to our permanent
            (m_defaultEditorSettings.CompilingSettingsList[0] as SanScriptCompilingSettings).IncludeFile = m_includeFile;
            (m_defaultEditorSettings.CompilingSettingsList[0] as SanScriptCompilingSettings).IncludePaths = m_includePaths.ToList();
            (m_defaultEditorSettings.CompilingSettingsList[0] as SanScriptCompilingSettings).Custom = m_custom;
            m_defaultEditorSettings.Language.Copy(m_currentLanguageSettings);

            m_defaultEditorSettings.SpecialIdentifiers = SanScriptProjectResolver.SpecialIdentifiers;

            string filename = Path.Combine( m_directory, c_settingsFileName );
            if ( File.Exists( filename ) )
            {
                FileAttributes atts = File.GetAttributes( filename );
                if ( (atts & FileAttributes.ReadOnly) == FileAttributes.ReadOnly )
                {
                    try
                    {
                        File.SetAttributes( filename, atts & (~FileAttributes.ReadOnly) );
                    }
                    catch
                    {
                    }
                }
            }

            string error;
            if ( !m_defaultEditorSettings.SaveFile( filename, out error ) )
            {
                ApplicationSettings.Log.Error( $"Error saving SanScript default editor settings file: {error}" );

                return false;
            }

            return true;
        }

        public void ShutdownPlugin()
        {
            SanScriptProjectResolver.ParseCoordinator.ClearAllParseRequests();

            foreach ( IParserProjectResolver projectResolver in m_projectResolvers )
            {
                DisposeProjectResolver( projectResolver );
            }

            m_projectResolvers.Clear();
        }

        public bool UpdateDocumentOutline( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, TreeView treeView )
        {
            if ( editor.Document.SemanticParseData is ICompilationUnit )
            {
                // Load the document outline 
                ICompilationUnit compilationUnit = (ICompilationUnit)editor.Document.SemanticParseData;
                SanScriptDocumentOutlineUpdater.Process(editor.Document.Guid, treeView, compilationUnit );

                // Print the tree of the AST (for debugging only)
                // Trace.WriteLine(compilationUnit.ToStringTree());
                return true;
            }

            return false;
        }

        public void OnLanguageSettingsUpdated()
        {
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Turns filename into a fully-qualified path name that exists in any of the include paths.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="sourceDirectory">The directory to search relative to.</param>
        /// <returns>Null if file does not exist, otherwise the lowercase full path name.</returns>
        public string GetFullPathName( string filePath, string sourceDirectory = null)
        {
            string fullPathName = filePath.Replace( "/", @"\" );

            if ( fullPathName.StartsWith( ".\\" ) )
            {
                fullPathName = fullPathName.Remove( 0, 2 );
            }

            if ( fullPathName.IndexOfAny( Path.GetInvalidPathChars() ) != -1 )
            {
                return null;
            }

            if ( !Path.IsPathRooted( fullPathName ) )
            {
                // add the include path:
                lock (_includePathLockObj)
                {
                    // Check our list of include paths first, since that's a quick lookup.
                    String fileName = Path.GetFileName(fullPathName);
                    String foundPath;
                    if (m_includePathFiles.TryGetValue(fileName, out foundPath))
                    {
                        return Path.GetFullPath(foundPath).ToLower();
                    }

                    // If we didn't find it, attempt to construct some paths manually.

                    // First, check relative path.
                    if (sourceDirectory != null)
                    {
                        // This handles source/../../filename as well as just source/filename
                        string pathFromSource = Path.GetFullPath(Path.Combine(sourceDirectory, filePath));
                        if (File.Exists(pathFromSource))
                        {
                            return pathFromSource.ToLower();
                        }
                    }

                    // Final fallback for case when include cache can't find the file.
                    // Go through all include paths and attempt to construct a path for this file.
                    foreach (string path in m_includePaths)
                    {
                        if (path == ".")
                        {
                            // This was already handled above in sourceDirectory check.
                            continue;
                        }

                        string fullName = Path.Combine(path, fullPathName);
                        if (File.Exists(fullName))
                        {
                            return Path.GetFullPath(fullName).ToLower();
                        }
                    }
                }
            }
            else if ( File.Exists( fullPathName ) )
            {
                return Path.GetFullPath( fullPathName ).ToLower();
            }

            return null;
        }
        #endregion

        #region Private Functions
        private void PopulateIncludePathFiles( string directory, IDictionary<string, string> includeFiles )
        {
            foreach ( string ext in m_extensions )
            {
                IEnumerable<string> files = Directory.EnumerateFiles( directory, String.Format("*{0}", ext));
                foreach ( string file in files )
                {
                    String fullPath = Path.GetFullPath(file).ToLower();
                    String fileName = Path.GetFileName(fullPath);
                    if (!includeFiles.ContainsKey(fileName))
                    {
                        includeFiles.Add(fileName, fullPath);
                    }
                }
            }
        }

        private bool IsAProjectFile( string filename )
        {
            foreach ( SanScriptProjectResolver projectResolver in m_projectResolvers )
            {
                if ( projectResolver.ProjectFiles.Contains( filename ) )
                {
                    return true;
                }
            }

            return false;
        }

        private void DisposeProjectResolver( IParserProjectResolver projectResolver )
        {
            SanScriptProjectResolver.ParseCoordinator.ClearAllParseRequests();
            projectResolver.Dispose();
        }
        #endregion
    }
}
