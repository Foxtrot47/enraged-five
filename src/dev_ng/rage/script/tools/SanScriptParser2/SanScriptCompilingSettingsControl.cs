//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptCompilingSettingsControl.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2019-2021. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace SanScriptParser
{
    using System;
    using System.Windows.Forms;
    using ragScriptEditorShared;

    public partial class SanScriptCompilingSettingsControl : CompilingSettingsControl
    {
        public SanScriptCompilingSettingsControl()
        {
            InitializeComponent();

            // register these events so we know when to update the command line text box
            base.SettingsChanged += new EventHandler( SanScriptCompilingTabSettingsControl_SettingsChanged );
            base.ProjectFilenameChanged += new EventHandler( SanScriptCompilingTabSettingsControl_SettingsChanged );
        }

        #region Overrides
        public override SettingsBase TabSettings
        {
            get
            {
                SanScriptCompilingSettings compilingSettings = new SanScriptCompilingSettings();

                SettingsBase settings = base.TabSettings;
                if ( settings is CompilingSettings )
                {
                    compilingSettings.Copy( settings as CompilingSettings );
                }

                compilingSettings.Custom = this.customTextBox.Text.Trim();
                compilingSettings.DebugParser = this.debugParserCheckBox.Checked;
                compilingSettings.DependencyFile = this.dependencyFileTextBox.Text.Trim();
                compilingSettings.DisplayDisassembly = this.displayDisassemblyCheckBox.Checked;
                compilingSettings.DumpThreadState = this.dumpThreadStateCheckBox.Checked;
                compilingSettings.Globals = this.globalsFileTextBox.Text.Trim();
                compilingSettings.HsmEnabled = this.enableHSMCheckBox.Checked;
                compilingSettings.NativeFile = this.nativeFileTextBox.Text.Trim();
                compilingSettings.OutputDependencyFile = this.dependecyFileCheckBox.Checked;
                compilingSettings.OutputNativeFile = this.nativeFileCheckBox.Checked;
                compilingSettings.WarningsAsErrors = this.warningsAsErrorsCheckBox.Checked;

                return compilingSettings;
            }
            set
            {
                base.TabSettings = value;

                if ( value is SanScriptCompilingSettings )
                {
                    m_invokeSettingsChanged = false;

                    SanScriptCompilingSettings compilingSettings = value as SanScriptCompilingSettings;

                    this.customTextBox.Text = compilingSettings.Custom;
                    this.debugParserCheckBox.Checked = compilingSettings.DebugParser;
                    this.dependencyFileTextBox.Text = compilingSettings.DependencyFile;
                    this.displayDisassemblyCheckBox.Checked = compilingSettings.DisplayDisassembly;
                    this.dumpThreadStateCheckBox.Checked = compilingSettings.DumpThreadState;
                    this.globalsFileTextBox.Text = compilingSettings.Globals;
                    this.enableHSMCheckBox.Checked = compilingSettings.HsmEnabled;
                    this.nativeFileTextBox.Text = compilingSettings.NativeFile;
                    this.dependecyFileCheckBox.Checked = compilingSettings.OutputDependencyFile;
                    this.nativeFileCheckBox.Checked = compilingSettings.OutputNativeFile;
                    this.warningsAsErrorsCheckBox.Checked = compilingSettings.WarningsAsErrors;

                    // call these to initialize some other items
                    dependecyFileCheckBox_CheckedChanged( this, EventArgs.Empty );
                    nativeFileCheckBox_CheckedChanged( this, EventArgs.Empty );
                    SanScriptCompilingTabSettingsControl_SettingsChanged( this, EventArgs.Empty );

                    m_invokeSettingsChanged = true;
                }
            }
        }
        #endregion

        #region Event Handlers
        private void SanScriptCompilingTabSettingsControl_SettingsChanged( object sender, EventArgs e )
        {
            SanScriptCompilingSettings compilingSettings = (SanScriptCompilingSettings)this.TabSettings;
            this.commandLineRichTextBox.Text = compilingSettings.BuildCompileScriptCommand("%1", null);
        }

        private void SanScriptSettingChanged( object sender, EventArgs e )
        {
            base.SettingChanged( sender, e );
        }

        private void dependecyFileCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.dependencyFileTextBox.Enabled = this.dependecyFileCheckBox.Checked;

            base.SettingChanged( sender, e );
        }

        private void nativeFileCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.nativeFileTextBox.Enabled = this.nativeFileCheckBox.Checked;

            base.SettingChanged( sender, e );
        }

        private void globalsFileBrowseButton_Click( object sender, EventArgs e )
        {
            DialogResult result = this.globalsOpenFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.globalsFileTextBox.Text = this.globalsOpenFileDialog.FileName;
            }
        }
        #endregion

		private void compileToConfigurationFolderCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if(compileToConfigurationFolderCheckBox.Checked)
			{
				OutputDirectory = "$(ConfigurationName)";
			}
			OutputDirectoryEnabled = !compileToConfigurationFolderCheckBox.Checked;
		}
    }
}

