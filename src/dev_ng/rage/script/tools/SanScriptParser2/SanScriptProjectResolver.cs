//---------------------------------------------------------------------------------------------
// <copyright file="SanScriptProjectResolver.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2018-2021. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text;

using ActiproSoftware.SyntaxEditor;

using ragScriptEditorShared;
using RSG.Platform;
using RSG.Base.Extensions;

namespace SanScriptParser
{
    public class SanScriptProjectResolver : ActiproSoftware.ComponentModel.DisposableObject, IParserProjectResolver
    {
        #region Constants
        public const string SpecialIdentifiersFileName = "<special>";
        public const string KeywordIdentifiersFileName = "<keywords>";
        #endregion

        #region Static Variables

        private static Dictionary<string, NativeItem> sm_specialIdentifiers = new Dictionary<string, NativeItem>();

        private static SanScriptIdentifierType sm_identifierTypes = (SanScriptIdentifierType)0xffff;
        #endregion

        #region Static Properties
        public static SanScriptParserPlugin ParserPlugin { get; set; } = null;

        public static SanScriptIdentifierCache IdentifierCache { get; } = new SanScriptIdentifierCache();

        public static ParseCoordinator ParseCoordinator { get; } = new ParseCoordinator(IdentifierCache);

        public static List<NativeItem> SpecialIdentifiers
        {
            get
            {
                List<NativeItem> items = new List<NativeItem>();
                foreach ( KeyValuePair<string, NativeItem> pair in sm_specialIdentifiers )
                {
                    items.Add( pair.Value );
                }

                return items;
            }
            set
            {
                sm_specialIdentifiers.Clear();

                SanScriptIdentifierFile idFile = IdentifierCache.GetIdentifierFileByName(SpecialIdentifiersFileName);
                idFile.Lock.EnterWriteLock();
                try
                {
                    idFile.Reset();
                    int filenameHashCode = idFile.FilenameHashCode;
                    foreach (NativeItem item in value)
                    {
                        if (item is StructItem)
                        {
                            StructItem sItem = item as StructItem;

                            sm_specialIdentifiers.Add(sItem.Name.ToUpper(), sItem);

                            SanScriptStructure structure = new SanScriptStructure(sItem.Name, filenameHashCode, 0,
                                sItem.DisplayText, sItem.MarkupText);

                            string[] memberNames = sItem.ContentNames;
                            string[] memberTypes = sItem.ContentTypes;
                            string[] memberMarkupTexts = sItem.MarkupContents;
                            for (int i = 0; i < memberNames.Length; ++i)
                            {
                                structure.Members.Add(new SanScriptStructMember(memberNames[i], memberTypes[i], structure,
                                    filenameHashCode, 0, string.Empty, memberMarkupTexts[i]));
                            }

                            IdentifierCache.AddIdentifier(idFile, structure);
                        }
                        else if (item is ConstOrEnumItem)
                        {
                            ConstOrEnumItem cItem = item as ConstOrEnumItem;

                            sm_specialIdentifiers.Add(cItem.Name.ToUpper(), cItem);

                            string dataType = cItem.Type.ToUpper();
                            if ((dataType == "CONST_INT") || (dataType == "CONST_FLOAT"))
                            {
                                IdentifierCache.AddIdentifier(idFile, new SanScriptConstant(cItem.Name, filenameHashCode, 0,
                                    cItem.DisplayText, cItem.MarkupText));
                            }
                            else
                            {
                                SanScriptEnum enumId = IdentifierCache.FindIdentifierInFileExact(
                                    SanScriptProjectResolver.SpecialIdentifiersFileName, cItem.Type, SanScriptIdentifierType.EnumType) as SanScriptEnum;
                                if (enumId == null)
                                {
                                    string displayText = "ENUM " + cItem.Type;

                                    StringBuilder markupText = new StringBuilder();
                                    markupText.Append("<span style=\"color:reserved;\">ENUM </span><span style=\"color:user;\">");
                                    markupText.Append(cItem.Type);
                                    markupText.Append("</span>");

                                    enumId = new SanScriptEnum(cItem.Type, filenameHashCode, 0, displayText, markupText.ToString());
                                    IdentifierCache.AddIdentifier(idFile, enumId);
                                }

                                enumId.Enums.Add(new SanScriptEnumeration(cItem.Name, enumId, filenameHashCode, 0,
                                    cItem.DisplayText, cItem.MarkupText));
                            }
                        }
                        else if (item is NativeProcOrFunc)
                        {
                            NativeProcOrFunc nItem = item as NativeProcOrFunc;

                            sm_specialIdentifiers.Add(nItem.Name.ToUpper(), nItem);

                            if (nItem.DeclareType == NativeItem.DeclarationType.Native)
                            {
                                IdentifierCache.AddIdentifier(idFile, new SanScriptNativeSubroutine(nItem.Name, filenameHashCode, 0,
                                    nItem.DisplayText, nItem.MarkupText));
                            }
                            else
                            {
                                IdentifierCache.AddIdentifier(idFile, new SanScriptSubroutine(nItem.Name, filenameHashCode, 0,
                                    nItem.DisplayText, nItem.MarkupText));
                            }
                        }
                    }

                    IdentifierCache.FinishUpdatingIdentifierFile(idFile);
                }
                finally
                {
                    idFile.Lock.ExitWriteLock();
                }
            }
        }
        #endregion

        #region Variables
        private List<string> m_projectFiles = new List<string>();
        private Guid m_guid = Guid.NewGuid();

        private Dictionary<string, bool> m_needsToBeCompiled = new Dictionary<string, bool>();
        private Dictionary<string, DateTime> m_lastWriteTime = new Dictionary<string, DateTime>();
        private Dictionary<string, DateTime> m_lastCompileTime = new Dictionary<string, DateTime>();

        /// <summary>
        /// Time when a particular file was last resourced for the specified platform.
        /// </summary>
        private Dictionary<Tuple<String, Platform>, DateTime> m_lastResourceTime = new Dictionary<Tuple<String, Platform>, DateTime>();
        #endregion

        public static SanScriptSyntaxLanguage Language { get; } = new SanScriptSyntaxLanguage();

        #region IParserProjectResolver interface

        SyntaxLanguage IParserProjectResolver.Language => Language;

        public List<string> ProjectFiles
        {
            get
            {
                return m_projectFiles;
            }
            set
            {
                string includeFilePath = ParserPlugin.IncludeFile;

                // remove old references
                string[] projectFilesToRemoveFromCache = m_projectFiles
                    .Where(f => !value.Contains(f) &&
                        !f.Equals(includeFilePath, StringComparison.OrdinalIgnoreCase) &&
                        !ParserPlugin.IncludePathFiles.Contains(f)).ToArray();
                if (projectFilesToRemoveFromCache.Any())
                {
                    ParseCoordinator.RemoveFilesFromCache(projectFilesToRemoveFromCache);
                }

                string[] projectFilesToAddToCache = value
                    .Where(f => !f.Equals(includeFilePath, StringComparison.OrdinalIgnoreCase))
                    .ToArray();

                m_projectFiles = value;
                if (projectFilesToAddToCache.Any())
                {
                    ParseCoordinator.RequestIdentifiersForFiles(projectFilesToAddToCache);
                }
            }
        }

        public bool GetImplementationInformation( SyntaxEditor editor, out string[] fileNames, out TextRange[] textRanges )
        {
            fileNames = null;
            textRanges = null;

            object foundObject = null;
            string foundFilename = null;

            if ( Language.FindDefinition( editor, ref foundObject, ref foundFilename ) )
            {
                if ( (foundFilename == SpecialIdentifiersFileName)
                    || (foundFilename == KeywordIdentifiersFileName) )
                {
                    return false;
                }

                string fullPathName = ParserPlugin.GetFullPathName(foundFilename, Path.GetDirectoryName(editor.Document.Filename));
                if ( fullPathName == null )
                {
                    return false;
                }

                fileNames = new string[1];
                textRanges = new TextRange[1];

                if ( foundObject is AstNode )
                {
                    fileNames[0] = fullPathName;

                    AstNode node = foundObject as AstNode;
                    switch ( node.NodeType )
                    {
                        case SanScriptNodeType.VariableArrayDeclarationStatement:
                        case SanScriptNodeType.VariableNonArrayDeclarationStatement:
                            textRanges[0] = node.TextRange;
                            break;
                        case SanScriptNodeType.UsingStatement:
                            textRanges[0] = new TextRange( -1, -1 );
                            break;
                        default:
                            textRanges[0] = new TextRange( node.TextRange.StartOffset, -1 );
                            break;
                    }
                }
                else if ( foundObject is SanScriptIdentifier )
                {
                    fileNames[0] = fullPathName;

                    SanScriptIdentifier id = foundObject as SanScriptIdentifier;
                    textRanges[0] = new TextRange( id.StartOffset, -1 );
                }

                return true;
            }

            return false;
        }

        public void PreCompile()
        {
            m_needsToBeCompiled.Clear();
            m_lastWriteTime.Clear();
        }

        public bool CheckDependency(string inputFilename, string outputFilename, string executablePath)
        {
            DateTime fileTime = File.GetLastWriteTime(inputFilename);

            if (File.Exists(outputFilename))
            {
                DateTime outputTime = File.GetLastWriteTime(outputFilename);

                if (fileTime > outputTime)
                {
                    ApplicationSettings.Log.Debug("\tOUTPUT FILE is older");
                    // must compile
                    m_needsToBeCompiled.Add(inputFilename.ToLower(), true);
                    return true;
                }

                if (String.IsNullOrWhiteSpace(executablePath) == false)
                {
                    DateTime executableDateTime = File.GetLastWriteTime(executablePath);

                    if (executableDateTime > outputTime)
                    {
                        ApplicationSettings.Log.Debug("\tOUTPUT FILE is older than compiler");
                        // must compile
                        m_needsToBeCompiled.Add(inputFilename.ToLower(), true);
                        return true;
                    }
                }
            }
            else
            {
                ApplicationSettings.Log.Debug("\tOUTPUT FILE doesn't exist");
                // must compile
                m_needsToBeCompiled.Add(inputFilename.ToLower(), true);
                return true;
            }

            return false;
        }

        public bool NeedsToBeCompiled( string filename, string projectFile, CompilingSettings compilingSettings )
        {
            SanScriptCompilingSettings sanScriptCompilingSettings = compilingSettings as SanScriptCompilingSettings;
            if (sanScriptCompilingSettings == null)
            {
                ApplicationSettings.Log.ToolError( "Expected SanScriptCompilingSettings." );
                return false;
            }

            ApplicationSettings.Log.Debug("NeedsToBeCompiled '{0}':", filename);
            bool needsCompile;
            if ( m_needsToBeCompiled.TryGetValue( filename.ToLower(), out needsCompile ) )
            {
                ApplicationSettings.Log.Debug("\tFROM CACHE: {0}", needsCompile ? "Compile" : "Skip");
                return needsCompile;
            }

            DateTime fileTime = File.GetLastWriteTime( filename );
            DateTime outputTime = DateTime.MaxValue;

            // check against the last compile time
            DateTime lastFileTime;
            if ( m_lastCompileTime.TryGetValue( filename.ToLower(), out lastFileTime ) )
            {
                if ( lastFileTime != fileTime )
                {
                    m_needsToBeCompiled.Add( filename.ToLower(), true );
                    return true;
                }
            }

            // check .sco
            string scoFilename = Path.ChangeExtension(sanScriptCompilingSettings.BuildOutputFilename(filename), ".sco");

            if (CheckDependency(filename, scoFilename, ApplicationSettings.GetAbsolutePath(sanScriptCompilingSettings.CompilerExecutable)) == true)
            {
                //Needs to be updated.
                return true;
            }

            if (File.Exists(scoFilename))
            {
                outputTime = File.GetLastWriteTime(scoFilename);
            }

            // check .sgv
            if ( !String.IsNullOrEmpty( sanScriptCompilingSettings.Globals ) )
            {
                if ( IdentifierCache.ContainsGlobalsStatement( filename ) )
                {
                    string globalsFilename = sanScriptCompilingSettings.BuildGlobalsFilename(filename);
                    if ( !String.IsNullOrEmpty( globalsFilename ) && !File.Exists( globalsFilename ) )
                    {
                        ApplicationSettings.Log.Debug("\tGLOBALS FILE doesn't exist");
                        // must compile
                        m_needsToBeCompiled.Add( filename.ToLower(), true );
                        return true;
                    }
                }
            }

            Dictionary<string, bool> checkedFiles = new Dictionary<string, bool>();

            // check include file
            string includeFilePathName = ParserPlugin.IncludeFile;
            if ( !String.IsNullOrEmpty( includeFilePathName ) )
            {
                if ( filename != includeFilePathName )
                {
                    if ( includeFilePathName != null )
                    {
                        ApplicationSettings.Log.Debug("\tINCLUDE FILE {0}:", includeFilePathName);
                        DateTime includeTime = GetLatestWriteTime( includeFilePathName, checkedFiles, 2 );
                        if ( includeTime > outputTime )
                        {
                            ApplicationSettings.Log.Debug("\tINCLUDE FILE is newer: Compile");
                            // must compile
                            m_needsToBeCompiled.Add( filename.ToLower(), true );
                            return true;
                        }
                    }
                    else
                    {
                        ApplicationSettings.Log.Debug("\tINCLUDE FILE {0}: not found", ParserPlugin.IncludeFile);
                    }
                }
            }

            // check usings
            DateTime usingTime = GetLatestWriteTime( filename, checkedFiles, 1 );
            if ( usingTime > outputTime )
            {
                ApplicationSettings.Log.Debug("\tUSING FILE is newer: Compile");
                // must compile
                m_needsToBeCompiled.Add( filename.ToLower(), true );
                return true;
            }

            // no compile necessary
            ApplicationSettings.Log.Debug("\tFILE {0}: Skip", filename);
            m_needsToBeCompiled.Add( filename.ToLower(), false );
            return false;
        }

        public void FileCompiled( string filename )
        {
            m_needsToBeCompiled[filename] = false;
            m_lastCompileTime[filename] = File.GetLastWriteTime( filename );
        }

        public void PostCompile()
        {
            m_needsToBeCompiled.Clear();
            m_lastWriteTime.Clear();
        }

        /// <summary>
        /// Return the list of include paths for the file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public IEnumerable<String> GetIncludesForFile(String filename)
        {
            return IdentifierCache.GetAllUsings(filename).Keys;
        }

        /// <summary>
        /// Determines if resourcing is necessary for the file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="platform"></param>
        /// <param name="projectFile">The project we are compiling, if any.</param>
        /// <param name="compilingSettings">The CompilingSettings we will be using for the compile.</param>
        /// <returns>True to recompile</returns>
        public bool NeedsToBeResourced(String filename, Platform platform, String projectFile, CompilingSettings compilingSettings)
        {
            SanScriptCompilingSettings sanScriptCompilingSettings = compilingSettings as SanScriptCompilingSettings;
            if (sanScriptCompilingSettings == null)
            {
                ApplicationSettings.Log.ToolError("Expected SanScriptCompilingSettings.");
                return false;
            }

            // Only sc files need to be resourced.
            if (0 != String.Compare(Path.GetExtension(filename), ".sc", true))
            {
                return false;
            }

            DateTime currentFileTime = File.GetLastWriteTime(filename);

            // Check against the last resource time for this platform from this script editor session.
            DateTime lastResourceTime;
            if (m_lastResourceTime.TryGetValue(Tuple.Create(filename.ToLower(), platform), out lastResourceTime))
            {
                if (lastResourceTime != currentFileTime)
                {
                    return true;
                }
            }

            // Determine the resulting resourced file path.
            String platformExt = FileType.Script.GetPlatformExtension(platform);
            String dirName = platform.ToString().ToLower();

            String resourceFilename = Path.GetFileName(Path.ChangeExtension(filename, platformExt));
            String resourceFilepath = Path.Combine(sanScriptCompilingSettings.OutputDirectory, dirName, resourceFilename);

            // If the file doesn't exist, we need to resource it.
            if (!File.Exists(resourceFilepath))
            {
                return true;
            }

            // Check whether the file has been modified since we last resourced it.
            lastResourceTime = File.GetLastWriteTime(resourceFilepath);
            if (currentFileTime > lastResourceTime)
            {
                return true;
            }

            // Has the sco file been modified since the last resource?
            String scoFilename = Path.ChangeExtension(sanScriptCompilingSettings.BuildOutputFilename(filename), ".sco");
            if (!File.Exists(scoFilename))
            {
                return true;
            }
            else
            {
                DateTime independentTime = File.GetLastWriteTime(scoFilename);
                if (independentTime > lastResourceTime)
                {
                    return true;
                }
            }

            // Check the last write time for the resource file against the script resource executable.
            String resourceExePath = ApplicationSettings.GetAbsolutePath(sanScriptCompilingSettings.ResourceExecutable);
            if (!String.IsNullOrWhiteSpace(resourceExePath))
            {
                DateTime executableDateTime = File.GetLastWriteTime(resourceExePath);
                if (executableDateTime > lastResourceTime)
                {
                    return true;
                }
            }

            // Local cache.
            Dictionary<String, bool> checkedFiles = new Dictionary<String, bool>();

            // check include files.
            String includeFilePathName = ParserPlugin.IncludeFile;
            if (!String.IsNullOrEmpty(includeFilePathName))
            {
                if (filename != includeFilePathName)
                {
                    if (includeFilePathName != null)
                    {
                        DateTime includeTime = GetLatestWriteTime(includeFilePathName, checkedFiles, 2);
                        if (includeTime > lastResourceTime)
                        {
                            // Include file has been modified since the file was resourced.
                            return true;
                        }
                    }
                }
            }

            // check usings.
            DateTime usingTime = GetLatestWriteTime(filename, checkedFiles, 1);
            if (usingTime > lastResourceTime)
            {
                // One of the usings has been modified since the file was resourced.
                return true;
            }

            // File shouldn't require resourcing.
            return false;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="platform"></param>
        public void FileResourced(String filename, Platform platform)
        {
            m_lastResourceTime[Tuple.Create(filename, platform)] = File.GetLastWriteTime(filename);
        }


        public void SetLanguage( SyntaxEditor editor, InfoTipTextDelegate infoTipTextDel )
        {
            Language.InfoTip = infoTipTextDel;

            editor.DocumentTextChanged += new DocumentModificationEventHandler( Language.editor_DocumentTextChanged );

            editor.Document.Language = Language;
            editor.Document.LanguageData = this;

            editor.Document.SemanticParseDataChanged += new EventHandler( Language.editorDocument_SemanticParseDataChanged );

            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( ' ' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '`' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '~' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '!' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '@' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '#' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '$' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '%' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '^' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '&' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '*' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '(' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( ')' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '-' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '=' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '+' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '[' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '{' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( ']' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '}' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '\\' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '|' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( ';' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( ':' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '\'' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '"' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( ',' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '<' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '.' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '>' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '/' );
            editor.IntelliPrompt.MemberList.AllowedCharacters.Add( '?' );

            // FIXME: maybe add code snippet folders here
            // FIXME: maybe verify that we are a ProjectFile or a IncludePathFile
        }

        public void ClearLanguage( SyntaxEditor editor )
        {
            editor.Document.Language = SyntaxLanguage.PlainText;
            editor.Document.LanguageData = null;

            editor.DocumentTextChanged -= Language.editor_DocumentTextChanged;

            editor.Document.SemanticParseDataChanged -= Language.editorDocument_SemanticParseDataChanged;
        }

        public IntellisenseSettingsChangedEventHandler IntellisenseSettingsChangedEventHandler => this.IntellisenseSettingsChanged;

        public void RefreshIntellisense()
        {

            // check for updates
            string includeFile = ParserPlugin.IncludeFile;
            if ( !string.IsNullOrEmpty(includeFile) )
            {
                ParseCoordinator.RequestIdentifiersForFiles(includeFile);
            }

            // this bit of reflection will cause us to re-examine the directories for new or deleted files
            ParserPlugin.IncludePaths = ParserPlugin.IncludePaths.ToArray();

            ParseCoordinator.RequestIdentifiersForFiles(ParserPlugin.IncludePathFiles.ToArray());
            ParseCoordinator.RequestIdentifiersForFiles(m_projectFiles.ToArray());
        }

        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                Language?.Dispose();
            }

            base.Dispose( disposing );
        }
        #endregion

        #region Public Static Functions
        /// <summary>
        /// Retrieves all <see cref="SanScriptIdentifier"/>s that matching matchText that are accessible from filename.
        /// </summary>
        /// <param name="filename">The file, and all of its dependencies, to look through.</param>
        /// <param name="matchText">The text to match.</param>
        /// <param name="includeInnerWords">Whether to include inner-word matches or not.</param>
        /// <param name="dependenciesHashCodes">Set of hash codes for this file. Should be precalculated by calling <see cref="GetDependenciesHashCodes"/></param>
        /// <returns>An <see cref="IEnumerable{T}"/> of all <see cref="SanScriptIdentifier"/>s found.</returns>
        public static IEnumerable<SanScriptIdentifier> FindIdentifiersAccessibleFromFileStartingWith( string filename, string matchText,
            bool includeInnerWords, ISet<int> dependenciesHashCodes)
        {
            List<SanScriptIdentifier> identifiers = new List<SanScriptIdentifier>();

            // now get the matching identifiers from the includes
            bool isEmpty = matchText == string.Empty;
            foreach (int hashCode in dependenciesHashCodes)
            {
                if ( isEmpty )
                {
                    // Don't return all identifier types--otherwise the results would be 200k+ and lag the editor for 30 seconds.
                    identifiers.AddRange(IdentifierCache.FindIdentifiersInFileHash(
                        hashCode,
                        SanScriptIdentifierType.Keyword |
                            SanScriptIdentifierType.KeywordSubroutine |
                            SanScriptIdentifierType.WordOperator));
                }
                else
                {
                    identifiers.AddRange(IdentifierCache.FindIdentifiersInFileHashStartingWith(hashCode, matchText, includeInnerWords, sm_identifierTypes));
                }
            }

            return identifiers
                .Distinct(i => i.Name)
                .OrderBy(i => i.Name)
                .ToList();
        }

        /// <summary>
        /// Locates the first <see cref="SanScriptIdentifier"/> matching name exactly that is accessible from the filename. Includes usings and globals.
        /// </summary>
        /// <param name="filename">The file, and all of its dependencies, to look through.</param>
        /// <param name="name">The name of the identifier to search for</param>
        /// <param name="dependenciesHashCodes">Set of hash codes for this file. Should be precalculated by calling <see cref="GetDependenciesHashCodes"/></param>
        /// <returns>The identifier if it was found.</returns>
        public static SanScriptIdentifier FindIdentifierAccessibleFromFileExact(string filename, string name, ISet<int> dependenciesHashCodes)
        {
            foreach (int hashCode in dependenciesHashCodes)
            {
                SanScriptIdentifier identifier = IdentifierCache.FindIdentifierInFileHashExact(hashCode, name);
                if (identifier != null)
                {
                    return identifier;
                }
            }

            return null;
        }

        /// <summary>
        /// Locates all <see cref="SanScriptIdentifier"/>s matching name exactly that are accessible from the filename. Includes usings and globals.
        /// </summary>
        /// <param name="filename">The file, and all of its dependencies, to look through.</param>
        /// <param name="name">The name of the identifier to search for</param>
        /// <param name="dependenciesHashCodes">Set of hash codes for this file. Should be precalculated by calling <see cref="GetDependenciesHashCodes"/></param>
        /// <returns>The identifiers if any were found.</returns>
        public static IEnumerable<SanScriptIdentifier> FindIdentifiersAccessibleFromFileExact(string filename, string name, ISet<int> dependenciesHashCodes)
        {
            List<SanScriptIdentifier> identifiers = new List<SanScriptIdentifier>();

            foreach (int hashCode in dependenciesHashCodes)
            {
                identifiers.AddRange(IdentifierCache.FindIdentifiersInFileHashExact(hashCode, name));
            }

            return identifiers;
        }

        /// <summary>
        /// Builds a ordered dictionary of a file's dependencies
        /// </summary>
        /// <param name="filename">The file in question.</param>
        /// <param name="projectResolver">The file's <see cref="SanScriptProjectResolver"/>.</param>
        /// <returns>A <see cref="Dictionary"/> of the dependencies.  The key is the hash code, the value is the index.</returns>
        public static ISet<int> GetDependenciesHashCodes( string filename, SanScriptProjectResolver projectResolver )
        {
            ISet<int> dependencies = new HashSet<int>();

            dependencies.Add(SanScriptIdentifierCache.GetFilenameHashCode(KeywordIdentifiersFileName));

            string includeFile = ParserPlugin.IncludeFile;
            if ( !string.IsNullOrEmpty(includeFile) )
            {
                int includeFileHashCode = SanScriptIdentifierCache.GetFilenameHashCode(includeFile);
                dependencies.Add(includeFileHashCode);

                IdentifierCache.GetAllUsingsHashCodes(includeFileHashCode, dependencies);
            }

            int filenameHashCode = SanScriptIdentifierCache.GetFilenameHashCode(filename);
            if ( !dependencies.Contains( filenameHashCode ) )
            {
                dependencies.Add(filenameHashCode);

                IdentifierCache.GetAllUsingsHashCodes(filenameHashCode, dependencies);
            }

            if ( Language.AddIncludePathSymbols )
            {
                foreach ( string file in ParserPlugin.IncludePathFiles )
                {
                    filenameHashCode = SanScriptIdentifierCache.GetFilenameHashCode(file);
                    if (!dependencies.Contains(filenameHashCode))
                    {
                        dependencies.Add(filenameHashCode);

                        IdentifierCache.GetAllUsingsHashCodes(filenameHashCode, dependencies);
                    }
                }
            }

            if ( Language.AddProjectSymbols && (projectResolver != null ) )
            {
                foreach ( string file in projectResolver.ProjectFiles )
                {
                    filenameHashCode = SanScriptIdentifierCache.GetFilenameHashCode(file);
                    if ( !dependencies.Contains( filenameHashCode ) )
                    {
                        dependencies.Add(filenameHashCode);

                        IdentifierCache.GetAllUsingsHashCodes(filenameHashCode, dependencies);
                    }
                }
            }

            dependencies.Add(SanScriptIdentifierCache.GetFilenameHashCode(SpecialIdentifiersFileName));
            return dependencies;
        }

        #endregion

        #region Private Functions
        /// <summary>
        /// Returns the latest write time of the given filename and all of its dependencies
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="checkedFiles"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        private DateTime GetLatestWriteTime( string filename, Dictionary<string, bool> checkedFiles, int depth )
        {
            DateTime fileTime;
            if ( m_lastWriteTime.TryGetValue( filename.ToLower(), out fileTime ) )
            {
                ApplicationSettings.Log.Debug("{0}GetLatestWriteTime '{0}': Return {1} FROM CACHE", new String('\t', depth), filename, fileTime.ToString());
                return fileTime;
            }

            if (!File.Exists(filename))
            {
                ApplicationSettings.Log.Debug("{0}FILE {1}: not found", new String('\t', depth), filename);
                return fileTime;
            }

            fileTime = File.GetLastWriteTime( filename );
            checkedFiles[filename.ToLower()] = true;

            IEnumerable<string> usingFiles = IdentifierCache.GetUsings( filename );
            if ( usingFiles.Any())
            {
                ApplicationSettings.Log.Debug("{0}GetLatestWriteTime '{0}' USING FILES:", new String('\t', depth), filename);
            }

            foreach ( string usingFile in usingFiles )
            {
                string usingFilePathName = ParserPlugin.GetFullPathName( usingFile, Path.GetDirectoryName( filename ) );
                if ( usingFilePathName != null )
                {
                    usingFilePathName = usingFilePathName.ToLower();
                    DateTime usingTime = DateTime.MinValue;
                    if ( !m_lastWriteTime.TryGetValue( usingFilePathName, out usingTime ) )
                    {
                        // prevent circular dependencies
                        if ( checkedFiles.ContainsKey( usingFilePathName ) )
                        {
                            if (!File.Exists(usingFilePathName))
                            {
                                ApplicationSettings.Log.Debug("{0}FILE {1}: not found", new String('\t', depth + 1), usingFilePathName);
                                continue;
                            }

                            usingTime = File.GetLastWriteTime( usingFilePathName );
                        }
                        else
                        {
                            usingTime = GetLatestWriteTime( usingFilePathName, checkedFiles, depth + 1 );
                        }
                    }
                    else
                    {
                        ApplicationSettings.Log.Debug("{0}GetLatestWriteTime '{1}': {2} FROM CACHE", new String('\t', depth), usingFilePathName, usingTime.ToString());
                    }

                    if ( usingTime > fileTime )
                    {
                        fileTime = usingTime;
                    }
                }
                else
                {
                    ApplicationSettings.Log.Debug("{0}USING FILE {1}: path invalid", new String('\t', depth), usingFile);
                }
            }

            ApplicationSettings.Log.Debug("{0}GetLatestWriteTime '{1}' SET CACHE TO {2}", new String('\t', depth), filename, fileTime.ToString());
            m_lastWriteTime[filename.ToLower()] = fileTime;
            return fileTime;
        }
        #endregion

        #region Event Handlers
        private void IntellisenseSettingsChanged( object sender, IntellisenseSettingsChangedEventArgs e )
        {
            Language.AutocompleteEnabled = e.IntellisenseSettings.PerformSemanticParsing && e.IntellisenseSettings.Autocomplete;
            Language.AutocompleteWithBracketsEnabled = Language.AutocompleteEnabled && e.IntellisenseSettings.AutocompleteWithBrackets;
            Language.AddIncludePathSymbols = Language.AutocompleteEnabled && e.IntellisenseSettings.AddIncludePathSymbols;
            Language.AddProjectSymbols = Language.AutocompleteEnabled && e.IntellisenseSettings.AddProjectSymbols;
            Language.ShowItemsThatContainCurrentString = e.IntellisenseSettings.PerformSemanticParsing && e.IntellisenseSettings.ShowItemsThatContainCurrentString;
            Language.HighlightQuickInfoPopups = e.IntellisenseSettings.HighlightQuickInfoPopups;
            Language.HighlightDisabledCodeBlocks = e.IntellisenseSettings.HighlightDisabledCodeBlocks;
            Language.CompleteWordOnExactMatch = e.IntellisenseSettings.CompleteWordOnExactMatch;
            Language.CompleteWordOnFirstMatch = e.IntellisenseSettings.CompleteWordOnFirstMatch;

            SanScriptDocumentOutlineUpdater.VerboseDocumentOutline = e.IntellisenseSettings.VerboseDocumentOutline;

            if ( e.Editor.Document.Language.Key == Language.Key )
            {
                e.Editor.Document.SemanticParsingEnabled = e.IntellisenseSettings.PerformSemanticParsing;
            }

            sm_identifierTypes = SanScriptIdentifierType.Identifier |
                                 SanScriptIdentifierType.Keyword |
                                 SanScriptIdentifierType.KeywordSubroutine |
                                 SanScriptIdentifierType.WordOperator |
                                 SanScriptIdentifierType.StructMember |
                                 SanScriptIdentifierType.TypedefType;

            if ( e.IntellisenseSettings.AddConstants )
            {
                sm_identifierTypes |= SanScriptIdentifierType.Constant;
            }

            if ( e.IntellisenseSettings.AddEnums )
            {
                sm_identifierTypes |= SanScriptIdentifierType.Enum;
            }

            if ( e.IntellisenseSettings.AddEnumTypes )
            {
                sm_identifierTypes |= SanScriptIdentifierType.EnumType;
            }

            if ( e.IntellisenseSettings.AddNativeSubroutines )
            {
                sm_identifierTypes |= SanScriptIdentifierType.NativeSubroutine;
            }

            if ( e.IntellisenseSettings.AddNativeTypes )
            {
                sm_identifierTypes |= SanScriptIdentifierType.NativeType;
            }

            if ( e.IntellisenseSettings.AddStates )
            {
                sm_identifierTypes |= SanScriptIdentifierType.State;
            }

            if ( e.IntellisenseSettings.AddStaticVariables )
            {
                sm_identifierTypes |= SanScriptIdentifierType.StaticVariable;
            }

            if ( e.IntellisenseSettings.AddStructTypes )
            {
                sm_identifierTypes |= SanScriptIdentifierType.StructType;
            }

            if ( e.IntellisenseSettings.AddSubroutines )
            {
                sm_identifierTypes |= SanScriptIdentifierType.Subroutine;
            }
        }
        #endregion
    }
}
