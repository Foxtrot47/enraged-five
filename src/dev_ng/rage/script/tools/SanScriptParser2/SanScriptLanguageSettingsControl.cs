using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using ragScriptEditorShared;

namespace SanScriptParser
{
    public partial class SanScriptLanguageSettingsControl : ragScriptEditorShared.LanguageSettingsControl
    {
        public SanScriptLanguageSettingsControl()
        {
            InitializeComponent();
        }

        #region Overrides
        public override SettingsBase TabSettings
        {
            get
            {
                SettingsBase settings = base.TabSettings;
                if ( settings is LanguageSettings )
                {
                    SanScriptLanguageSettings languageSettings = new SanScriptLanguageSettings();
                    languageSettings.Copy( settings as LanguageSettings );

                    languageSettings.LanguageHelpFile = this.languageHelpFileTextBox.Text.Trim();
                    languageSettings.ReferenceHelpFile = this.referenceHelpFileTextBox.Text.Trim();

                    return languageSettings;
                }

                return null;
            }
            set
            {
                base.TabSettings = value;

                if ( value is SanScriptLanguageSettings )
                {
                    m_invokeSettingsChanged = false;

                    SanScriptLanguageSettings languageSettings = value as SanScriptLanguageSettings;

                    this.languageHelpFileTextBox.Text = languageSettings.XmlLanguageHelpFile;
                    this.referenceHelpFileTextBox.Text = languageSettings.XmlReferenceHelpFile;

                    m_invokeSettingsChanged = true;
                }
            }
        }
        #endregion

        #region Event Handlers
        private void SanScriptSettingChanged( object sender, EventArgs e )
        {
            OnSettingsChanged();
        }

        private void languageHelpFileBrowseButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog1.Title = "Select Language Help File";
            
            DialogResult result = this.openFileDialog1.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.languageHelpFileTextBox.Text = this.openFileDialog1.FileName;
            }
        }

        private void referenceHelpFileBrowseButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog1.Title = "Select Reference Help File";

            DialogResult result = this.openFileDialog1.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.referenceHelpFileTextBox.Text = this.openFileDialog1.FileName;
            }
        }
        #endregion
    }
}

