namespace ScriptLineCounter
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.okButton = new System.Windows.Forms.Button();
            this.projectTypeCombo = new System.Windows.Forms.ComboBox();
            this.programCounterTextBox = new System.Windows.Forms.TextBox();
            this.projectTypeLabel = new System.Windows.Forms.Label();
            this.scriptFileLabel = new System.Windows.Forms.Label();
            this.scriptFileCombo = new System.Windows.Forms.ComboBox();
            this.programCounterLabel = new System.Windows.Forms.Label();
            this.detailsTextBox = new System.Windows.Forms.TextBox();
            this.detailsLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Enabled = false;
            this.okButton.Location = new System.Drawing.Point(273, 61);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(163, 23);
            this.okButton.TabIndex = 3;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // projectTypeCombo
            // 
            this.projectTypeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.projectTypeCombo.Enabled = false;
            this.projectTypeCombo.FormattingEnabled = true;
            this.projectTypeCombo.Location = new System.Drawing.Point(12, 23);
            this.projectTypeCombo.Name = "projectTypeCombo";
            this.projectTypeCombo.Size = new System.Drawing.Size(233, 21);
            this.projectTypeCombo.TabIndex = 0;
            this.projectTypeCombo.SelectedIndexChanged += new System.EventHandler(this.projectTypeCombo_SelectedIndexChanged);
            // 
            // programCounterTextBox
            // 
            this.programCounterTextBox.Enabled = false;
            this.programCounterTextBox.Location = new System.Drawing.Point(273, 24);
            this.programCounterTextBox.Name = "programCounterTextBox";
            this.programCounterTextBox.Size = new System.Drawing.Size(163, 20);
            this.programCounterTextBox.TabIndex = 2;
            this.programCounterTextBox.TextChanged += new System.EventHandler(this.programCounterTextBox_TextChanged);
            // 
            // projectTypeLabel
            // 
            this.projectTypeLabel.AutoSize = true;
            this.projectTypeLabel.Enabled = false;
            this.projectTypeLabel.Location = new System.Drawing.Point(9, 6);
            this.projectTypeLabel.Name = "projectTypeLabel";
            this.projectTypeLabel.Size = new System.Drawing.Size(67, 13);
            this.projectTypeLabel.TabIndex = 0;
            this.projectTypeLabel.Text = "Project Type";
            // 
            // scriptFileLabel
            // 
            this.scriptFileLabel.AutoSize = true;
            this.scriptFileLabel.Enabled = false;
            this.scriptFileLabel.Location = new System.Drawing.Point(9, 47);
            this.scriptFileLabel.Name = "scriptFileLabel";
            this.scriptFileLabel.Size = new System.Drawing.Size(53, 13);
            this.scriptFileLabel.TabIndex = 1;
            this.scriptFileLabel.Text = "Script File";
            // 
            // scriptFileCombo
            // 
            this.scriptFileCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.scriptFileCombo.Enabled = false;
            this.scriptFileCombo.FormattingEnabled = true;
            this.scriptFileCombo.Location = new System.Drawing.Point(12, 63);
            this.scriptFileCombo.Name = "scriptFileCombo";
            this.scriptFileCombo.Size = new System.Drawing.Size(233, 21);
            this.scriptFileCombo.TabIndex = 1;
            this.scriptFileCombo.SelectedIndexChanged += new System.EventHandler(this.scriptFileCombo_SelectedIndexChanged);
            // 
            // programCounterLabel
            // 
            this.programCounterLabel.AutoSize = true;
            this.programCounterLabel.Enabled = false;
            this.programCounterLabel.Location = new System.Drawing.Point(270, 6);
            this.programCounterLabel.Name = "programCounterLabel";
            this.programCounterLabel.Size = new System.Drawing.Size(86, 13);
            this.programCounterLabel.TabIndex = 2;
            this.programCounterLabel.Text = "Program Counter";
            // 
            // detailsTextBox
            // 
            this.detailsTextBox.Enabled = false;
            this.detailsTextBox.Location = new System.Drawing.Point(12, 103);
            this.detailsTextBox.Multiline = true;
            this.detailsTextBox.Name = "detailsTextBox";
            this.detailsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.detailsTextBox.Size = new System.Drawing.Size(424, 62);
            this.detailsTextBox.TabIndex = 4;
            this.detailsTextBox.TabStop = false;
            // 
            // detailsLabel
            // 
            this.detailsLabel.AutoSize = true;
            this.detailsLabel.Enabled = false;
            this.detailsLabel.Location = new System.Drawing.Point(9, 87);
            this.detailsLabel.Name = "detailsLabel";
            this.detailsLabel.Size = new System.Drawing.Size(39, 13);
            this.detailsLabel.TabIndex = 9;
            this.detailsLabel.Text = "Details";
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 178);
            this.Controls.Add(this.detailsLabel);
            this.Controls.Add(this.detailsTextBox);
            this.Controls.Add(this.programCounterLabel);
            this.Controls.Add(this.scriptFileLabel);
            this.Controls.Add(this.scriptFileCombo);
            this.Controls.Add(this.projectTypeLabel);
            this.Controls.Add(this.programCounterTextBox);
            this.Controls.Add(this.projectTypeCombo);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Line Counter";
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.ComboBox projectTypeCombo;
        private System.Windows.Forms.TextBox programCounterTextBox;
        private System.Windows.Forms.Label projectTypeLabel;
        private System.Windows.Forms.Label scriptFileLabel;
        private System.Windows.Forms.ComboBox scriptFileCombo;
        private System.Windows.Forms.Label programCounterLabel;
        private System.Windows.Forms.TextBox detailsTextBox;
        private System.Windows.Forms.Label detailsLabel;
    }
}

