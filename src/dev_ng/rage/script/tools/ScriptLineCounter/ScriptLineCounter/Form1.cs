// ScriptLineCounter
// Simon Lashley - 19 / 05 / 2009
// Updated from VBScript
// 
// Now parses the script project file for sco/scd directories.
// It presents these as a combo box with the project type, debug, japanese etc...
// 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Diagnostics;
using System.IO;


namespace ScriptLineCounter
{

    public partial class mainForm : Form
    {

        // xml tags from the scproj file (write in lowercase)
        const string SCPROJ_CONFIG_NAME = "configurationname";
        const string SCPROJ_OUTPUT_DIR = "outputdirectory";

        // form objects, used for quick disabling later on
        const int FORM_OBJ_SCRIPT_FILE = 0;
        const int FORM_OBJ_PROGRAM_COUNTER = 1;
        const int FORM_OBJ_OK_BUTTON = 2;
        const int FORM_OBJ_DETAILS_TEXT = 3;

        // states for parsing the scd file
        const int SCD_STATE_LINEINFO = 0;
        const int SCD_STATE_FIND_COUNTER = 1;
        const int SCD_STATE_FIND_FILE_LIST = 2;
        const int SCD_STATE_FIND_COUNTER_FILE = 3;

        const int SCD_STATE_FINISHED = 5;

        const int SCD_STATE_FAIL = 6;

        // =============================================================================================
        // Form Events
        // =============================================================================================

        // initialsation of the form, if you need to alter the
        // start up state of the form (enable combo boxes etc...)
        // do it in the InitializeComponent function.
        public mainForm()
        {
            InitializeComponent();
        }

        // on the form loading grab the command line arguments and see
        // if a scproj file was passed.
        private void mainForm_Load(object sender, EventArgs e)
        {

            // get the command line arguments passed, the first one
            // will always be the path to the ScriptLineCounter.exe file
            string[] args = System.Environment.GetCommandLineArgs();

            if (args.Length == 2)
            {
                string scriptProjectPath = args[1].ToString().ToLower();

                // quick check to see if the file passed is an scproj file.
                if (scriptProjectPath.EndsWith(".scproj"))
                    {setupProjectTypeCombo(scriptProjectPath);}
                else
                    {printMessageAndExit("File passed, does not seem to be a .scproj file.");}
            }
            else
                { printMessageAndExit("Wrong number of arguments passed, supply a project path."); }

        }

        private void projectTypeCombo_SelectedIndexChanged(object sender, EventArgs e)
        {

            String scriptSCDPath = ((ComboBoxItem)projectTypeCombo.SelectedItem).FilePath;

            if (Directory.Exists(scriptSCDPath))
            {

                string[] fileNames = Directory.GetFiles(((ComboBoxItem)projectTypeCombo.SelectedItem).FilePath, "*.scd", System.IO.SearchOption.AllDirectories);

                scriptFileCombo.Items.Clear();

                if (fileNames.Length == 0)
                {
                    disableAndClearFormObjects(FORM_OBJ_SCRIPT_FILE);
                    MessageBox.Show("No SCD Files exist for " + projectTypeCombo.Text + ", please re-compile or choose another Project Type.");
                }
                else
                {
                    for (int i = 0; i < fileNames.Length; i++)
                    {
                        scriptFileCombo.Items.Add(new ComboBoxItem(Path.GetFileNameWithoutExtension(fileNames[i].ToString()), fileNames[i].ToString()));
                    }
                    scriptFileCombo.Enabled = true;
                    scriptFileLabel.Enabled = true;
                    scriptFileCombo.SelectedItem = scriptFileCombo.Items[0];
                }
            }
            else
            {
                disableAndClearFormObjects(FORM_OBJ_SCRIPT_FILE);
                MessageBox.Show("No SCD Files exist for " + projectTypeCombo.Text + ", please re-compile or choose another Project Type.");
            }

        }

        private void scriptFileCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            programCounterLabel.Enabled = true;
            programCounterTextBox.Enabled = true;

            disableAndClearFormObjects(FORM_OBJ_DETAILS_TEXT);
        }

        private void programCounterTextBox_TextChanged(object sender, EventArgs e)
        {
            if ((programCounterTextBox.Text != null) && (programCounterTextBox.Text != ""))
            {
                try
                {
                    int conversionTest = (programCounterTextBox.Text.Length > 2 && (programCounterTextBox.Text[1] == 'x' || programCounterTextBox.Text[1] == 'X')) ? Convert.ToInt32(programCounterTextBox.Text.Substring(3), 16) : Convert.ToInt32(programCounterTextBox.Text, 10);
                    if (conversionTest >= 0) { okButton.Enabled = true; }
                    else { disableAndClearFormObjects(FORM_OBJ_OK_BUTTON); }
                }
                catch { disableAndClearFormObjects(FORM_OBJ_OK_BUTTON); }
            }
            else { disableAndClearFormObjects(FORM_OBJ_OK_BUTTON);}
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            string scriptSCDPath = ((ComboBoxItem)scriptFileCombo.SelectedItem).FilePath;

            try
            {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.
                using (StreamReader scdFile = new StreamReader(scriptSCDPath))
                {

                    // convert the program counter string to an int for 
                    // comparison
                    int programCounterInt = (programCounterTextBox.Text.Length > 2 && (programCounterTextBox.Text[1] == 'x' || programCounterTextBox.Text[1] == 'X')) ? Convert.ToInt32(programCounterTextBox.Text.Substring(3), 16) : Convert.ToInt32(programCounterTextBox.Text, 10);
                    
                    // initial state for the file parse is to find [LINEINFO]
                    int scdParseState = SCD_STATE_LINEINFO; 

                    // program counter details that will be parsed, counter, line number,
                    // script number.
                    string[] programCounterDetails = new string[3];

                    // program counter details that will be parsed, counter, line number,
                    // script number.
                    string[] detailsTextLines = new string[4];
                    int currentDetailTextLine = 0;

                    // used for counting through the scripts at the end of the
                    // file.
                    int scriptCounter = 0;

                    // Read and parse lines from the file until the end of 
                    // the file is reached.
                    string currentLine;
                    while ((currentLine = scdFile.ReadLine()) != null)
                    {
                        switch (scdParseState)
                        {

                            case SCD_STATE_LINEINFO:
                                {
                                    if (currentLine.ToLower().StartsWith("[lineinfo]"))
                                        { scdParseState = SCD_STATE_FIND_COUNTER; }
                                }
                                break;

                            case SCD_STATE_FIND_COUNTER:
                                {
                                    // split string with ":" should be 3 values.
                                    string[] tempSplit = currentLine.Split(Convert.ToChar(":"));

                                    if (tempSplit.Length == 3)
                                    {
                                        if (Convert.ToInt32(tempSplit[0]) > programCounterInt)
                                        {
                                            scdParseState = SCD_STATE_FIND_FILE_LIST;
                                        }
                                        else
                                        {
                                            for (int i = 0; i < tempSplit.Length; i++)
                                                {programCounterDetails[i] = tempSplit[i];}
                                        }
                                    }
                                    else
                                    {
                                        // if the program counter details are not
                                        // null we have reached the end of the file.
                                        if (programCounterDetails[0]!=null)
                                        {
                                            detailsTextLines[currentDetailTextLine] = "Warning! Last file in SCD info could be wrong.";
                                            currentDetailTextLine++;

                                            scdParseState = SCD_STATE_FIND_COUNTER_FILE;
                                        }
                                    }
                                }
                                break;

                            case SCD_STATE_FIND_FILE_LIST:
                                {
                                    if (!currentLine.Contains(":"))
                                        {scdParseState = SCD_STATE_FIND_COUNTER_FILE;}
                                }
                                break;

                            case SCD_STATE_FIND_COUNTER_FILE:
                                {
                                    if (!currentLine.ToLower().StartsWith("[eof]"))
                                    {
                                        if (scriptCounter == Convert.ToInt32(programCounterDetails[2]))
                                        {

                                            detailsTextLines[currentDetailTextLine] = "Line " + programCounterDetails[1];
                                            currentDetailTextLine++;
                                            detailsTextLines[currentDetailTextLine] = currentLine;

                                            detailsTextBox.Lines = detailsTextLines;

                                            scdParseState = SCD_STATE_FINISHED;
                                        }
                                        else { scriptCounter++; }
                                    }
                                    else { scdParseState = SCD_STATE_FAIL; }
                                }
                                break;
                                
                        }
                    }

                    if (scdParseState != SCD_STATE_FINISHED)
                        { detailsTextBox.Text = "Error getting information."; }
                }
            }
            catch
            {
                detailsTextBox.Text = "Error getting information.";
            }

            
            detailsLabel.Enabled = true;
            detailsTextBox.Enabled = true;
        }


        // =============================================================================================
        // Custom Functions
        // =============================================================================================
        
        // this function will check the scproj file exists,
        // if it does it will parse the file for the configuration names
        // and sco output paths and add these to the project type combo box.
        private void setupProjectTypeCombo(string scriptProjectPath)
        {

            string comboString = "Debug";

            try
            {
                if (File.Exists(scriptProjectPath))
                {
                    XmlTextReader xmlReader = new XmlTextReader(scriptProjectPath);
                    xmlReader.WhitespaceHandling = WhitespaceHandling.All;

                    while (xmlReader.Read())
                    {
                        if (xmlReader.NodeType == XmlNodeType.Element)
                        {
                            switch (xmlReader.Name.ToLower())
                            {
                                case SCPROJ_CONFIG_NAME:
                                    {comboString = xmlReader.ReadInnerXml();}
                                    break;
                                case SCPROJ_OUTPUT_DIR:
                                    string dir = xmlReader.ReadInnerXml();
                                    if (dir == "$(ConfigurationName)")
                                    {
                                        dir = "t:\\rdr2\\assets\\content\\" + comboString;
                                    }
                                    {projectTypeCombo.Items.Add(new ComboBoxItem(comboString, dir));}
                                    break;
                            }
                        }
                    }

                    // unload xml file
                    if (xmlReader.ReadState != ReadState.Closed)
                        xmlReader.Close();

                }
                else
                    {printMessageAndExit(scriptProjectPath + " does not exist.");}
            }
            catch (Exception assert)
                {printMessageAndExit(assert.Message);}

            // enable the combo box if it has items added,
            // else quit out as something in the xml may have changed.
            if (projectTypeCombo.Items.Count != 0)
            {
                projectTypeLabel.Enabled = true;
                projectTypeCombo.Enabled = true;
                projectTypeCombo.SelectedItem = projectTypeCombo.Items[0];
            }
            else { printMessageAndExit(scriptProjectPath + ", does not contain correct xml tags."); }

        }
        
        private void printMessageAndExit(string printMessage)
        {
            // disable everything on the form?
            MessageBox.Show(printMessage);
            Application.Exit();
        }

        private void disableAndClearFormObjects(int objectID)
        {
            switch(objectID)
            {

              case FORM_OBJ_SCRIPT_FILE:
                {
                    scriptFileCombo.Enabled = false;
                    scriptFileLabel.Enabled = false;

                    scriptFileCombo.Items.Clear();

                    goto case FORM_OBJ_PROGRAM_COUNTER;
                }
              case FORM_OBJ_PROGRAM_COUNTER:
                {
                    programCounterTextBox.Enabled = false;
                    programCounterLabel.Enabled = false;

                    programCounterTextBox.Clear();

                    goto case FORM_OBJ_OK_BUTTON;
                }
                

              case FORM_OBJ_OK_BUTTON:
                {
                    okButton.Enabled = false;
                    goto case FORM_OBJ_DETAILS_TEXT;
                }
               

              case FORM_OBJ_DETAILS_TEXT:
                {
                    detailsTextBox.Enabled = false;
                    detailsLabel.Enabled = false;

                    detailsTextBox.Clear();
                }
                break; 
            }
        }

        // =============================================================================================
        // ComboBoxItem Class
        // =============================================================================================

        // this class is used for the combo boxes so they can store
        // the text to display and the path of the file they represent
        class ComboBoxItem
        {
            // string that appears in combo.
            public string Name;
            // string that stores path to file, not visible on combo.
            public string FilePath;
            
            // used for adding comboItem to comboBox
            public ComboBoxItem(string Name, string FilePath)
            {
                this.Name = Name;
                this.FilePath = FilePath;
            }

            // override ToString() function, so that it only displays 
            // whats contained in the Name string.
            public override string ToString()
            {
                return this.Name;
            }
        }
    }
}