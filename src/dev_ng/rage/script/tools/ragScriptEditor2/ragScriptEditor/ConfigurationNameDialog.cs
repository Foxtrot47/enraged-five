using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragScriptEditor
{
    public partial class ConfigurationNameDialog : Form
    {
        public ConfigurationNameDialog()
        {
            InitializeComponent();
        }

        #region Properties
        public string ConfigurationName
        {
            get
            {
                return this.configurationNameTextBox.Text.Trim();
            }
            set
            {
                this.configurationNameTextBox.Text = value;
            }
        }
        #endregion

        #region Event Handlers
        private void configurationNameTextBox_TextChanged( object sender, EventArgs e )
        {
            this.okButton.Enabled = this.configurationNameTextBox.Text.Trim().Length > 0;
        }
        #endregion
    }
}