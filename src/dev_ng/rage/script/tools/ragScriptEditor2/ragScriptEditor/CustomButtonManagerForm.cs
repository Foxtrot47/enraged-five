using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Command;
using RSG.Base.Forms;
using ragScriptEditorShared;

namespace ragScriptEditor
{
    public partial class CustomButtonManagerForm : Form
    {
        public CustomButtonManagerForm()
        {
            InitializeComponent();
        }

        public CustomButtonManagerForm( EditorToolbar toolbar )
        {
            InitializeComponent();

            m_editorToolbar = toolbar;
        }
        
        public DialogResult ShowIt( System.Windows.Forms.Form masterForm )
        {
            this.buttonsListView.SmallImageList.Images.Clear();
            this.buttonsListView.Items.Clear();

            foreach ( ToolbarButtonSetting customButton in m_editorToolbar.CustomButtonSettings )
            {
                int imageIndex = this.iconImageList.Images.IndexOfKey( customButton.IconFile );
                if ( imageIndex == -1 )
                {
                    imageIndex = LoadIconIntoImageList( customButton.IconFile );
                    if ( (imageIndex > -1) && (customButton.Icon == null) )
                    {
                        customButton.Icon = this.iconImageList.Images[imageIndex];
                    }
                }

                // create ListView entry
                ListViewItem item = new ListViewItem( customButton.Text, imageIndex );

                // add to list view
                this.buttonsListView.Items.Add( item );

                // add to hash table
                m_customButtonsHashTable.Add( item, customButton );
            }

            // get correct set buttons enabled
            EnableButtons( false );

            DialogResult result = this.ShowDialog( masterForm );

            if ( this.StartPosition == FormStartPosition.CenterParent )
            {
                // so the next time we open the window, it will be in the same position as before
                this.StartPosition = FormStartPosition.Manual;
            }

            return result;
        }

        #region Variables
        private Dictionary<ListViewItem, ToolbarButtonSetting> m_customButtonsHashTable = new Dictionary<ListViewItem, ToolbarButtonSetting>();
        private bool m_editModeOn = false;
        private EditorToolbar m_editorToolbar = null;
        private ToolbarButtonSetting m_currentCustomButton = null;
        private string m_iconFile;        
        #endregion

        #region Event Handlers
        private void editButton_Click( object sender, EventArgs e )
        {
            if ( m_currentCustomButton == null )
            {
                return;
            }

            // enable correct set of buttons
            EnableButtons( true );

            // fill fields
            SetEditButtonFields( m_currentCustomButton );
        }

        private void newButton_Click( object sender, EventArgs e )
        {
            // so we know this is a new button
            m_currentCustomButton = null;

            // enable correct set of buttons
            EnableButtons( true );

            // clear the fields
            SetEditButtonFields( null );
        }

        private void deleteButton_Click( object sender, EventArgs e )
        {
            ListViewItem item = this.buttonsListView.SelectedItems[0];

            DialogResult result = rageMessageBox.ShowWarning( this, String.Format( "Are you sure you want to delete '{0}'?", item.Text ), 
                "Confirm Delete", MessageBoxButtons.YesNo );
            if ( result == DialogResult.Yes )
            {
                DeleteCustomButton( item );
            }
        }

        private void moveUpButton_Click( object sender, EventArgs e )
        {
            ListViewItem item = this.buttonsListView.SelectedItems[0];
            ToolbarButtonSetting customButton = m_customButtonsHashTable[item];

            // adjust order in list view
            int oldIndex = item.Index;
            this.buttonsListView.Items.Remove( item );
            this.buttonsListView.Items.Insert( oldIndex - 1, item );

            // adjust order in editor toolbar
            m_editorToolbar.MoveCustomButton( -1, customButton );

            this.buttonsListView.Focus();
            item.Focused = true;
        }

        private void moveDownButton_Click( object sender, EventArgs e )
        {
            ListViewItem item = this.buttonsListView.SelectedItems[0];
            ToolbarButtonSetting customButton = m_customButtonsHashTable[item];

            // adjust order in list view
            int oldIndex = item.Index;
            this.buttonsListView.Items.Remove( item );
            this.buttonsListView.Items.Insert( oldIndex + 1, item );

            // adjust order in editor toolbar
            m_editorToolbar.MoveCustomButton( 1, customButton );

            this.buttonsListView.Focus();
            item.Focused = true;
        }

        private void acceptButton_Click( object sender, EventArgs e )
        {
            // save custom button
            if ( SaveCustomButton( ref m_currentCustomButton ) )
            {
                // enable correct set of buttons
                EnableButtons( false );
            }
        }

        private void cancelButton_Click( object sender, EventArgs e )
        {
            bool dirty = false;
            if ( m_currentCustomButton != null )
            {
                // edit                
                if ( (m_currentCustomButton.Text != this.nameTextBox.Text)
                    /*|| (m_currentCustomButton.Icon != this.iconPictureBox.Image.)*/
                    || (m_currentCustomButton.CommandLine != this.commandTextBox.Text) )
                {
                    dirty = true;
                }
            }
            else
            {
                // new                
                if ( (this.nameTextBox.Text != "")
                    /*|| (m_currentCustomButton.Icon != this.iconPictureBox.Image.)*/
                    || (this.commandTextBox.Text != "") )
                {
                    dirty = true;
                }
            }

            DialogResult result = DialogResult.Yes;
            if ( dirty )
            {
                result = rageMessageBox.ShowWarning( this, "Are you sure you want to abandon your changes?", "Confirm Cancel",
                    MessageBoxButtons.YesNo );
            }

            if ( result == DialogResult.Yes )
            {
                EnableButtons( false );
                SetEditButtonFields( m_currentCustomButton );
                return;
            }
        }

        private void browseIconButton_Click( object sender, EventArgs e )
        {
            DialogResult result = this.browseIconOpenFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                if ( !LoadIconIntoPictureBox( this.browseIconOpenFileDialog.FileName ) )
                {
                    // load failed
                    rageMessageBox.ShowError( this, String.Format( "'{0}' failed to load.", m_iconFile ), "Bad Bitmap File" );
                    m_iconFile = "";
                }
            }
        }

        private void browseCommandButton_Click( object sender, EventArgs e )
        {            
            DialogResult result = this.browseCommandOpenFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.commandTextBox.Text = this.browseCommandOpenFileDialog.FileName;
            }
        }

        private void buttonsListViewContextMenuStrip_Opening( object sender, CancelEventArgs e )
        {
            // enable/disable options
            if ( m_currentCustomButton != null )
            {
                this.deleteToolStripMenuItem.Enabled = true;

                int index = this.buttonsListView.Items.IndexOf( this.buttonsListView.SelectedItems[0] );
                this.moveUpToolStripMenuItem.Enabled = index > 0;
                this.moveDownToolStripMenuItem.Enabled = index < this.buttonsListView.Items.Count - 1;
            }
            else
            {
                this.moveUpToolStripMenuItem.Enabled = false;
                this.moveDownToolStripMenuItem.Enabled = false;
                this.deleteToolStripMenuItem.Enabled = false;
            }
        }

        private void buttonsListView_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.buttonsListView.SelectedItems.Count > 0 )
            {
                // fill fields
                ListViewItem item = this.buttonsListView.SelectedItems[0];
                m_currentCustomButton = m_customButtonsHashTable[item];
                SetEditButtonFields( m_currentCustomButton );
            }
            else
            {
                SetEditButtonFields( null );
            }

            EnableButtons( false );
        }
        #endregion

        #region Misc
        private void GetEditButtonFields( ref ToolbarButtonSetting customButton )
		{
			// set customButton settings
			customButton.Tag = ToolbarButtonSetting.ConvertNameToTag( this.nameTextBox.Text );
			customButton.Text = this.nameTextBox.Text;			
			customButton.CommandLine = this.commandTextBox.Text;

			// update the associated ButtonItem, if any
			if ( customButton.ButtonItem == null )
			{
				// go ahead and load the icon specified
				customButton.IconFile = m_iconFile;

                int imageIndex = LoadIconIntoImageList( m_iconFile );
                if ( imageIndex != -1 )
                {
                    customButton.Icon = this.iconImageList.Images[imageIndex];
                }
			}
			else
			{
				customButton.ButtonItem.ToolTipText = this.nameTextBox.Text;

				if ( customButton.IconFile != m_iconFile )
				{
                    int usage_count = 0;
                    foreach ( KeyValuePair<ListViewItem, ToolbarButtonSetting> pair in m_customButtonsHashTable )
                    {
                        if ( pair.Value.IconFile == customButton.IconFile )
                        {
                            ++usage_count;
                        }
                    }

                    if ( usage_count == 1 )
                    {
                        RemoveIconFromImageList( customButton.IconFile );
                    }

					customButton.IconFile = m_iconFile;

                    int imageIndex = LoadIconIntoImageList( customButton.IconFile );
                    if ( imageIndex != -1 )
                    {
                        customButton.Icon = this.iconImageList.Images[imageIndex];
                    }
				}

                customButton.MenuItem.Text = this.nameTextBox.Text;
			}
			
			// update list view item
			if ( m_customButtonsHashTable.ContainsValue( customButton ) )
			{
				// look for matching list view item
                foreach ( KeyValuePair<ListViewItem, ToolbarButtonSetting> pair in m_customButtonsHashTable )
                {
                    if ( pair.Value == customButton )
                    {
                        pair.Key.Text = customButton.Text;
                        pair.Key.ImageIndex = this.iconImageList.Images.IndexOfKey( customButton.IconFile );
                        break;
                    }
                }
			}
		}

		private void SetEditButtonFields( ToolbarButtonSetting customButton )
		{
			if ( customButton != null )
			{
				this.nameTextBox.Text = customButton.Text;
				this.commandTextBox.Text = customButton.CommandLine;

				LoadIconIntoPictureBox( customButton.IconFile );
			}
			else
			{
				this.nameTextBox.Text = "";
				this.commandTextBox.Text = "";

				LoadIconIntoPictureBox( "" );
			}
		}

		private void EnableButtons( bool edit )
		{
			m_editModeOn = edit;

			this.buttonsListView.Enabled = !m_editModeOn;

			bool selected = (this.buttonsListView.SelectedItems.Count > 0);
			if ( !selected && this.buttonsListView.Enabled )
			{                                
				this.moveUpButton.Enabled = false;                
				this.moveDownButton.Enabled = false;                
				this.editButton.Enabled = false;                
				this.newButton.Enabled = true;                
				this.deleteButton.Enabled = false;                
				this.closeButton.Enabled = true;
                
				this.acceptButton.Enabled = false;                
				this.cancelButton.Enabled = false;
				this.nameTextBox.ReadOnly = true;                
				this.browseIconButton.Enabled = false;
				this.commandTextBox.ReadOnly = true;                
				this.browseCommandButton.Enabled = false;
			}
			else
			{
				this.moveUpButton.Enabled = !m_editModeOn && selected && (this.buttonsListView.SelectedIndices[0] > 0);
				this.moveDownButton.Enabled = !m_editModeOn && selected && (this.buttonsListView.Items.Count > 1) && (this.buttonsListView.SelectedIndices[0] < this.buttonsListView.Items.Count-1);
				this.editButton.Enabled = !m_editModeOn && selected;
				this.newButton.Enabled = !m_editModeOn;
				this.deleteButton.Enabled = !m_editModeOn && selected;
				this.closeButton.Enabled = !m_editModeOn;

				this.acceptButton.Enabled = m_editModeOn;
				this.cancelButton.Enabled = m_editModeOn;
				this.nameTextBox.ReadOnly = !m_editModeOn;
				this.browseIconButton.Enabled = m_editModeOn;
				this.commandTextBox.ReadOnly = !m_editModeOn;
				this.browseCommandButton.Enabled = m_editModeOn;
			}
		}

        private bool SaveCustomButton( ref ToolbarButtonSetting customButton )
        {
            // need a name
            if ( this.nameTextBox.Text == "" )
            {
                rageMessageBox.ShowExclamation( this, "A button name is required.", "Invalid Name" );
                return false;
            }

            // need a command
            if ( this.commandTextBox.Text == "" )
            {
                rageMessageBox.ShowExclamation( this, "A command to execute is required.", "Invalid Command" );
                return false;
            }

            // validate name when creating a new button
            if ( (m_currentCustomButton == null) && !m_editorToolbar.IsValidButtonName( this.nameTextBox.Text ) )
            {
                rageMessageBox.ShowExclamation( this, "Button names must be unique.  Please use a different name.", "Invalid Name" );
                return false;
            }

            // make sure we have an icon
            if ( m_iconFile == "" )
            {
                rageMessageBox.ShowExclamation( this, "Please select an icon.", "Icon Required" );
                return false;
            }

            bool addButton = false;
            if ( customButton == null )
            {
                // new
                customButton = new ToolbarButtonSetting();
                customButton.IsCustomButton = true;
                addButton = true;
            }

            GetEditButtonFields( ref customButton );

            // new
            if ( addButton )
            {
                // create ListView entry
                ListViewItem item = new ListViewItem( customButton.Text, this.iconImageList.Images.IndexOfKey(customButton.IconFile) );

                // add to list view
                this.buttonsListView.Items.Add( item );

                // add to hash table
                m_customButtonsHashTable.Add( item, customButton );

                // add to the editor toolbar
                m_editorToolbar.AddCustomButton( customButton );
            }

            return true;
        }

        private void DeleteCustomButton( ListViewItem item )
        {
            // remove from list view
            this.buttonsListView.Items.Remove( item );

            ToolbarButtonSetting customButton;
            if ( !m_customButtonsHashTable.TryGetValue( item, out customButton ) )
            {
                return;
            }

            // remove from hash table
            m_customButtonsHashTable.Remove( item );

            // remove from the editor toolbar
            m_editorToolbar.RemoveCustomButton( customButton );
        }

        private bool GetThumbnailImageAbortCallback()
        {
            return false;
        }

        private int LoadIconIntoImageList( string file )
        {
            System.Drawing.Image img = System.Drawing.Image.FromFile( file );

            if ( img != null )
            {
                int count = this.iconImageList.Images.Count;
                this.iconImageList.Images.Add( file, img );
                return count;
            }

            return -1;
        }

        private void RemoveIconFromImageList( string file )
        {
            if ( this.iconImageList.Images.ContainsKey(file) )
            {
                this.iconImageList.Images.RemoveByKey( file );
            }
        }

        private bool LoadIconIntoPictureBox( string file )
        {
            m_iconFile = file;
            
            // load the icon into this.iconPictureBox
            if ( this.iconPictureBox.Image != null )
            {
                this.iconPictureBox.Image.Dispose();
                this.iconPictureBox.Image = null;
            }

            if ( m_iconFile == "" )
            {
                return true;
            }

            System.Drawing.Image img = null;
            try
            {
                img = System.Drawing.Image.FromFile( m_iconFile );
            }
            catch
            {
                rageMessageBox.ShowError( this, String.Format( "The icon file '{0}' failed to load.", m_iconFile ), "Invalid file" );
                return false;
            }

            if ( img != null )
            {
                // resize if needed
                if ( (img.Height > this.iconPictureBox.Height)
                    || (img.Width > this.iconPictureBox.Width) )
                {
                    img = img.GetThumbnailImage( this.iconPictureBox.Width - 4, this.iconPictureBox.Height - 4,
                        new Image.GetThumbnailImageAbort( GetThumbnailImageAbortCallback ), System.IntPtr.Zero );
                }

                this.iconPictureBox.Image = img;
                return true;
            }

            return false;
        }
        #endregion
    }
}