//#define OUTPUT_PROJECT_PARSE_TIME

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using ActiproSoftware.SyntaxEditor;
using ragScriptEditor.Properties;
using ragScriptEditorShared;
using RSG.Base;
using RSG.Base.Drawing;
using RSG.Base.Forms;
using RSG.Configuration;
using RSG.Pipeline.BuildScript;
using RSG.Platform;
using SanScriptParser;
using TD.SandDock;
using RSG.Base.Logging;
using RSG.Base.Extensions;

namespace ragScriptEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            InitializeAdditionalComponents();

            OnNotifyIncredibuildState(false);
        }

        #region Enums
        private enum SavingAndCompiling
		{
			SaveBeforeCompile,
			DontSaveBeforeCompile,
			SaveAllBeforeCompile
		}
        #endregion

        #region Helper Classes
        private class TabbedDocumentInfo
        {
            public TabbedDocumentInfo( Control parent, ActiproSoftware.SyntaxEditor.SyntaxEditor editor,
                TD.SandDock.DockControl dockControl )
            {
                m_parent = parent;
                m_editor = editor;
                m_dockControl = dockControl;
            }

            #region Delegates
            public delegate void AttributesChangedEventHandler( object sender, EventArgs e );
            #endregion

            #region Events
            public event AttributesChangedEventHandler AttributesChanged;
            #endregion

            #region Variables
            private Control m_parent;
            private ActiproSoftware.SyntaxEditor.SyntaxEditor m_editor;
            private TD.SandDock.DockControl m_dockControl;
            private FileSystemWatcher m_watcher;
            private bool m_isClosing = false;
            #endregion

            #region Properties
            public ActiproSoftware.SyntaxEditor.SyntaxEditor Editor
            {
                get
                {
                    return m_editor;
                }
            }

            public TD.SandDock.DockControl DockControl
            {
                get
                {
                    return m_dockControl;
                }
            }

            public bool FileSystemWatcherEnabled
            {
                get
                {
                    if ( m_watcher != null )
                    {
                        return m_watcher.EnableRaisingEvents;
                    }

                    return false;
                }
                set
                {
                    if ( m_watcher != null )
                    {
                        m_watcher.EnableRaisingEvents = value;
                    }
                }
            }

            public bool IsClosing
            {
                get
                {
                    return m_isClosing;
                }
                set
                {
                    m_isClosing = value;
                }
            }
            #endregion

            #region Public Functions
            public void AddFileSystemWatcher()
            {
                if ( m_watcher == null )
                {
                    string filename = null;
                    try
                    {
                        filename = Path.GetFileName( m_editor.Document.Filename );
                    }
                    catch
                    {
                        // we might have illegal characters in the filename
                        return;
                    }

                    int indexOf = filename.IndexOfAny( Path.GetInvalidFileNameChars() );
                    if ( indexOf == -1 )
                    {
                        m_watcher = new FileSystemWatcher( Path.GetDirectoryName( m_editor.Document.Filename ), filename );
                        m_watcher.NotifyFilter = NotifyFilters.Attributes;
                        m_watcher.Changed += new FileSystemEventHandler( watcher_Changed );
                        m_watcher.EnableRaisingEvents = true;

                        OnAttributesChanged();
                    }
                }
            }

            public void RemoveFileSystemWatcher()
            {
                if ( m_watcher != null )
                {
                    m_watcher.EnableRaisingEvents = false;
                    m_watcher = null;
                }
            }
            #endregion

            #region Private Functions
            private void OnAttributesChanged()
            {
                if ( m_parent != null )
                {
                    object[] args = new object[] { this, EventArgs.Empty };
                    m_parent.Invoke( this.AttributesChanged, args );
                }
                else
                {
                    this.AttributesChanged( this, EventArgs.Empty );
                }
            }
            #endregion

            #region Event Handlers
            private void watcher_Changed( object sender, FileSystemEventArgs e )
            {
                if ( e.ChangeType == WatcherChangeTypes.Changed )
                {
                    OnAttributesChanged();
                }
            }
            #endregion
        }
        #endregion

        #region Variables
        private bool m_closed = false;

        private ScriptEditor m_ScriptEditor = null;
        private EditorToolbar m_editorToolbar = null;
        private ProjectExplorer m_ProjectExplorer = null;
        private UserEditorSettings m_userEditorSettings = null;
        private Compiler m_compiler = null;
        private SourceControl m_sourceControl = null;

        private SavingAndCompiling m_SavingAndCompiling = SavingAndCompiling.SaveBeforeCompile;
        private string m_helpFile = @"chm\overview_scripteditor.chm";

        private bool m_startDebugAfterCompile = false;
        private int m_TabIndex = 0;

        private Dictionary<TD.SandDock.DockControl, TabbedDocumentInfo> m_dockControlToTabbedDocumentInfoMap
            = new Dictionary<DockControl,TabbedDocumentInfo>();
        private Dictionary<ActiproSoftware.SyntaxEditor.SyntaxEditor, TabbedDocumentInfo> m_editorToTabbedDocumentInfoMap
            = new Dictionary<SyntaxEditor,TabbedDocumentInfo>();

        private Regex m_rExpFind = new Regex( "(?<File>[a-zA-Z_0-9\\x2F\\x2E\\x3A\\x5C\\x20]*)\\x28(?<lineNum>[0-9]+)(,[0-9]+)?\\x29", RegexOptions.Compiled );

        private TextBackgroundColorSettings m_textBackgroundColorSettings = new TextBackgroundColorSettings();

        private int m_CurrentErrorNumber = 0;
        private int m_CurrentFindResultNumber = 0;

        private bool m_useNewFindInFilesDialog = true;
        private FindReplaceInFilesForm m_findReplaceInFilesForm = null;
        private SearchReplaceInFilesDialog m_searchReplaceInFilesDialog = null;
        private RichTextBoxSearchReplaceDialog m_richTextBoxSearchReplaceDialog = null;
        private SettingsForm m_settingsForm = null;

        private Lazy<StyleEditorForm> m_styleEditorFormLazy = null;
        private CustomButtonManagerForm m_customButtomManagerForm = null;

        private TD.SandDock.DockControl m_SelectedDockControl = null;
        private RichTextBox m_selectedRichTextBox = null;

        private List<TextEditorSettings> m_textEditorSettings = new List<TextEditorSettings>();

        private Point m_location = new Point(0, 0);
        private FormWindowState m_windowState = FormWindowState.Normal;

        private System.Windows.Forms.Timer m_parseStatusUpdateTimer = new System.Windows.Forms.Timer();

        private Queue<String> m_outputQueue = new Queue<string>();
        private System.Windows.Forms.Timer m_outputUpdateTimer = new System.Windows.Forms.Timer();

        public ILog Log
        {
            get
            {
                return LogFactory.ApplicationLog;
            }
        }
        #endregion

        #region Initialization
        private void InitializeAdditionalComponents()
        {
            m_editorToolbar = new EditorToolbar( this );
            m_userEditorSettings = new UserEditorSettings();
            m_sourceControl = new SourceControl( this, m_userEditorSettings.SourceControl2 );
            m_ProjectExplorer = new ProjectExplorer( this );
            m_searchReplaceInFilesDialog = new SearchReplaceInFilesDialog();
            m_ScriptEditor = new ScriptEditor( this, m_userEditorSettings, m_ProjectExplorer, m_editorToolbar, m_searchReplaceInFilesDialog );
            m_findReplaceInFilesForm = new FindReplaceInFilesForm( this, m_ScriptEditor.FindReplaceOptions, m_sourceControl );
            m_richTextBoxSearchReplaceDialog = new RichTextBoxSearchReplaceDialog();
            m_settingsForm = new SettingsForm();

            // this form has been seen to cause performance issues on boot (+700ms)
            m_styleEditorFormLazy  = new Lazy<StyleEditorForm>(() =>
            {
                StyleEditorForm form = new StyleEditorForm();
                form.ApplyStyles += this.StyleEditorForm_ApplyStyles;
                return form;
            });

            m_customButtomManagerForm = new CustomButtonManagerForm( m_editorToolbar );
            m_compiler = new Compiler( this, m_userEditorSettings );

            // Source Control
            m_sourceControl.ShowPromptDialog = new SourceControl.ShowPromptDialogDelegate( SourceControl_ShowPromptDialog );
            m_sourceControl.SaveNewFile = new SourceControl.SaveNewFileDelegate( m_ScriptEditor.SaveFileAs );
            m_sourceControl.PreSync = new SourceControl.PreSyncDelegate( SourceControl_PreSync );
            m_sourceControl.PostSync = new SourceControl.PostSyncDelegate( SourceControl_PostSync );

            // Project Explorer
            m_ProjectExplorer.LoadScriptFile = new ProjectExplorer.LoadScriptFileDelegate( m_ScriptEditor.LoadFile );
            m_ProjectExplorer.ReloadProjectFile = new ProjectExplorer.ReloadProjectDelegate( ReloadProjectFile );
            m_ProjectExplorer.ExecuteCompileAction = new ProjectExplorer.CompileActionDelegate( ProjectExplorer_ExecuteCompileAction );
            m_ProjectExplorer.FilesAdded += new ProjectExplorer.UpdateProjectFilesEventHandler( ProjectExplorer_FilesAdded );
            m_ProjectExplorer.FilesRemoved += new ProjectExplorer.UpdateProjectFilesEventHandler( ProjectExplorer_FilesRemoved );
            this.projectDockableWindow.Controls.Add( m_ProjectExplorer );
            m_ProjectExplorer.Dock = DockStyle.Fill;

            // Script Editor
            m_ScriptEditor.GetMenuItemByTag = new ScriptEditor.GetMenuItemByTagDelegate( ScriptEditor_GetMenuItemByTag );
            m_ScriptEditor.GetToolstripItemByTag = new ScriptEditor.GetToolstripItemByTagDelegate( ScriptEditor_GetToolbarItemByTag );
            m_ScriptEditor.InfoTipTextDel = new InfoTipTextDelegate( ScriptEditor_InfoTipText );
            m_ScriptEditor.HighlightLanguage += new ScriptEditor.HighlightLanguageEventHandler( ScriptEditor_HighlightLanguage );
            m_ScriptEditor.FileClosing += new ScriptEditor.FileClosingEventHandler( ScriptEditor_FileClosing );
            m_ScriptEditor.FileClosed += new ScriptEditor.FileClosedEventHandler( ScriptEditor_FileClosed );
            m_ScriptEditor.FileClosingAll += new ScriptEditor.FileAllPreOperationEventHandler( ScriptEditor_FileClosingAll );
            m_ScriptEditor.FileLoading += new ScriptEditor.FileLoadingEventHandler( ScriptEditor_FileLoading );
            m_ScriptEditor.FileLoaded += new ScriptEditor.FileLoadedEventHandler( ScriptEditor_FileLoaded );
            m_ScriptEditor.FileSaving += new ScriptEditor.FileSavingEventHandler( ScriptEditor_FileSaving );
            m_ScriptEditor.FileSaved += new ScriptEditor.FileSavedEventHandler( ScriptEditor_FileSaved );
            m_ScriptEditor.FileSavingAll += new ScriptEditor.FileAllPreOperationEventHandler( ScriptEditor_FileSavingAll );
            m_ScriptEditor.FileSavedAll += new ScriptEditor.FileAllPostOperationEventHandler( ScriptEditor_FileSavedAll );

            // Find Replace Form
            m_ScriptEditor.FindReplaceForm.ReplaceAllWarning = new ragScriptEditorShared.FindReplaceForm.ReplaceAllWarningDelegate( FindReplace_ReplaceAllWarning );
            m_ScriptEditor.FindReplaceForm.SourceControl = m_sourceControl;
            m_ScriptEditor.FindReplaceForm.SearchBegin = new FindReplace.SearchBeginDelegate( FindReplace_SearchBegin );
            m_ScriptEditor.FindReplaceForm.SearchComplete = new FindReplace.SearchCompleteDelegate( FindReplace_SearchComplete );

            // Search Replace Dialog
            m_ScriptEditor.SearchReplaceDialog.SearchBegin += new SearchBeginEventHandler( SearchReplaceDialog_SearchBegin );
            m_ScriptEditor.SearchReplaceDialog.SearchComplete += new SearchCompleteEventHandler( SearchReplaceDialog_SearchComplete );

            // Find/Replace in Files Form
            m_findReplaceInFilesForm.LoadFile = new FindReplace.LoadFileDelegate( m_ScriptEditor.LoadFile );
            m_findReplaceInFilesForm.ReloadFile = new FindReplace.ReloadFileDelegate( m_ScriptEditor.ReloadFile );
            m_findReplaceInFilesForm.DisableFileWatch = new FindReplace.DisableFileWatcherDelegate( m_ScriptEditor.DisableFileWatcher );
            m_findReplaceInFilesForm.EnableFileWatch = new FindReplace.EnableFileWatcherDelegate( m_ScriptEditor.EnableFileWatcher );
            m_findReplaceInFilesForm.InitSearchDocs = new FindReplaceInFilesForm.InitSearchDocsDelegate( m_ScriptEditor.findReplace_InitSearchDocuments );
            m_findReplaceInFilesForm.CancelIntellisense = new FindReplaceInFilesForm.CancelActiveIntellisenseDelegate( m_ScriptEditor.CancelActiveIntellisense );

            m_findReplaceInFilesForm.FoundString = new FindReplace.AddFoundStringDelegate( FindReplace_FoundString );
            m_findReplaceInFilesForm.ConsoleOutput = new FindReplaceInFilesForm.ConsoleOutputDelegate( FindReplaceInFiles_ConsoleOutput );
            m_findReplaceInFilesForm.OpenModifiedFilesAfteReplace = new FindReplaceInFilesForm.OpenModifiedFilesAfterReplaceAllDelegate( FindReplaceInFiles_OpenModifiedFilesAfterReplaceAll );
            m_findReplaceInFilesForm.ReplaceAllWarning = new FindReplaceInFilesForm.ReplaceAllWarningDelegate( FindReplace_ReplaceAllWarning );
            m_findReplaceInFilesForm.SearchBegin = new FindReplace.SearchBeginDelegate( FindReplace_SearchBegin );
            m_findReplaceInFilesForm.SearchComplete = new FindReplace.SearchCompleteDelegate( FindReplace_SearchComplete );

            // Search Replace In Files Dialog
            m_searchReplaceInFilesDialog.Init( this, m_ScriptEditor.FindReplaceOptions );
            m_searchReplaceInFilesDialog.CancelIntellisense += new EventHandler( searchReplaceInFilesDialog_CancelIntellisense );
            m_searchReplaceInFilesDialog.FindCurrentFilename += new FindCurrentFilenameEventHandler( searchReplaceInFilesDialog_FindCurrentFilename );
            m_searchReplaceInFilesDialog.FindOpenFilenames += new FindOpenFilenamesEventHandler( searchReplaceInFilesDialog_FindOpenFilenames );
            m_searchReplaceInFilesDialog.FindProjectFilenames += new FindProjectFilenamesEventHandler( searchReplaceInFilesDialog_FindProjectFilenames );
            m_searchReplaceInFilesDialog.FindSyntaxEditor += new SyntaxEditorEventHandler( searchReplaceInFilesDialog_FindSyntaxEditor );
            m_searchReplaceInFilesDialog.FoundString += new FoundStringEventHandler( searchReplaceInFilesDialog_FoundString );
            m_searchReplaceInFilesDialog.LoadFile += new LoadFileEventHandler( searchReplaceInFilesDialog_LoadFile );
            m_searchReplaceInFilesDialog.ReportProgress += new ReportProgressEventHandler( searchReplaceInFilesDialog_ReportProgress );
            m_searchReplaceInFilesDialog.SaveDocument += new SaveDocumentEventHandler( searchReplaceInFilesDialog_SaveDocument );
            m_searchReplaceInFilesDialog.SearchBegin += new SearchBeginEventHandler( searchReplaceInFilesDialog_SearchBegin );
            m_searchReplaceInFilesDialog.SearchComplete += new SearchCompleteEventHandler( searchReplaceInFilesDialog_SearchComplete );

            // Rich Text Box Search Replace Dialog
            m_richTextBoxSearchReplaceDialog.CanReplace = false;
            m_richTextBoxSearchReplaceDialog.CanFindAll = false;
            m_richTextBoxSearchReplaceDialog.CanReplaceAll = false;
            m_richTextBoxSearchReplaceDialog.FindCurrentRichTextBox += new rageSearchReplaceRichTextBoxEventHandler( richTextBoxSearchReplaceDialog_FindCurrentRichTextBox );

            // Settings Form
            m_settingsForm.ClearRecentFilesList += new EventHandler( settingsForm_ClearRecentFilesList );
            m_settingsForm.ClearRecentProjectsList += new EventHandler( settingsForm_ClearRecentProjectsList );
            m_settingsForm.LoadDefaultSettings += new SettingsForm.LanguageDefaultSettingsEventHandler( settingsForm_LoadDefaultSettings );
            m_settingsForm.SaveDefaultSettings += new SettingsForm.LanguageDefaultSettingsEventHandler( settingsForm_SaveDefaultSettings );
            m_settingsForm.SaveSettings += new SettingsForm.SaveSettingsEventHandler( settingsForm_SaveSettings );

            // Compiler
            m_compiler.PreCompileStep = new Compiler.PreCompileStepDelegate( this.PrepareCompile );
            m_compiler.BuildCompilerCommand = new Compiler.BuildCompilerCommandDelegate( this.BuildCompilerCommand );
            m_compiler.ResourceCompilerCommand = new Compiler.ResourceCompilerCommandDelegate(this.ResourceCompilerCommand);
            m_compiler.OkToCompile = new Compiler.OkToCompileDelegate( m_sourceControl.MakeFileWritableOnCompile );
            m_compiler.CompileComplete = new Compiler.CompileCompleteDelegate( this.CompileComplete );
            m_compiler.CompileCancelled = new Compiler.CompileCancelledDelegate( this.CompileCancelled );
            m_compiler.BreakOnError = new Compiler.BreakOnErrorDelegate( this.BreakOnCompileError );
            m_compiler.CompilerFailure = new Compiler.CompilerFailureDelegate( this.CompilerFailure );
            m_compiler.FilesNotFound = new Compiler.FilesNotFoundDelegate( this.CompileFilesNotFound );
            m_compiler.FileUpToDate = new Compiler.FileUpToDateDelegate( this.CompileFileUpToDate );
            m_compiler.ListUpToDate = new Compiler.ListUpToDateDelegate( this.CompileListUpToDate );
            m_compiler.NothingToCompile = new Compiler.NothingToCompileDelegate( this.NothingToCompile );
            m_compiler.ProcessOutputString = new Compiler.ProcessOutputStringDelegate( this.ProcessCompilerOutputString );
            m_compiler.IsFileCompilable = new Compiler.IsFileCompilableDelegate( m_ScriptEditor.IsFileCompilable );
            m_compiler.FileNeedsToBeCompiled = new Compiler.FileNeedsToBeCompiledDelegate(m_ScriptEditor.FileNeedsToBeCompiled);
            m_compiler.FileNeedsToBeResourced = new Compiler.FileNeedsToBeResourcedDelegate(m_ScriptEditor.FileNeedsToBeResourced);

            // Editor Toolbar
            m_editorToolbar.ToolbarButtonClicked += new EditorToolbar.ToolbarButtonClickedEventHandler( editorToolbar_ButtonClick );
            m_editorToolbar.ToolbarAppActionButtonClicked += new EventHandler( ExecuteAppAction_Click );
            m_editorToolbar.DebuggerOptionsDropDownMenuOpening += new EditorToolbar.DebuggerOptionsDropDownMenuOpeningEventHandler( editorToolbar_DebuggerOptionsDropDownMenuOpening );
            m_editorToolbar.OpenCustomButtonManager = new EventHandler( customButtonManagerToolStripMenuItem_Click );
            m_editorToolbar.ShowLineNumbersChanged += new EditorToolbar.ShowLineNumbersChangedEventHandler( editorToolbar_ShowLineNumbersChanged );
            m_editorToolbar.HighlightEnumsChanged += new EditorToolbar.HighlightEnumsChangedEventHandler( editorToolbar_HighlightEnumsChanged );
            m_editorToolbar.HighlightQuickInfoPopupsChanged += new EditorToolbar.HighlightQuickInfoPopupsChangedEventHandler( editorToolbar_HighlightQuickInfoPopupsChanged );
            m_editorToolbar.HighlightDisabledCodeBlocksChanged += new EditorToolbar.HighlightDisabledCodeBlocksChangedEventHandler( editorToolbar_HighlightDisabledCodeBlocksChanged );
            m_editorToolbar.CompilingConfigurationChanged += new EditorToolbar.CompilingConfigurationChangedEventHandler( editorToolbar_CompilingConfigurationChanged );
            m_editorToolbar.PlatformChanged += new EditorToolbar.PlatformChangedEventHandler(editorToolbar_PlatformChanged);

            m_parseStatusUpdateTimer.Tick += new EventHandler(this.UpdateParseStatus);
            m_parseStatusUpdateTimer.Interval = 250; // milliseconds
            m_parseStatusUpdateTimer.Start();

            m_outputUpdateTimer.Tick += new EventHandler(this.UpdateOutputTextBox);
            m_outputUpdateTimer.Interval = 50; // milliseconds
            m_outputUpdateTimer.Start();
            this.errorsDockableWindow.Load += new EventHandler(errorsDockableWindow_Load);

            // Document Outline
            this.documentOutlineTreeView.ImageList = SyntaxEditor.ReflectionImageList;

            // Tag the View menu items
            this.showProjectExplorerToolStripMenuItem.Tag = this.projectDockableWindow;
            this.centerProjectExplorerToolStripMenuItem.Tag = this.projectDockableWindow;
            this.dockProjectExplorerToolStripMenuItem.Tag = this.projectDockableWindow;
            this.floatProjectExplorerToolStripMenuItem.Tag = this.projectDockableWindow;

            this.showDocumentOutlineToolStripMenuItem.Tag = this.documentOutlineDockableWindow;
            this.centerDocumentOutlineToolStripMenuItem.Tag = this.documentOutlineDockableWindow;
            this.dockDocumentOutlineToolStripMenuItem.Tag = this.documentOutlineDockableWindow;
            this.floatDocumentOutlineToolStripMenuItem.Tag = this.documentOutlineDockableWindow;

            this.showOutputToolStripMenuItem.Tag = this.outputDockableWindow;
            this.centerOutputToolStripMenuItem.Tag = this.outputDockableWindow;
            this.dockOutputToolStripMenuItem.Tag = this.outputDockableWindow;
            this.floatOutputToolStripMenuItem.Tag = this.outputDockableWindow;

            this.showFindResultsToolStripMenuItem.Tag = this.findResultsDockableWindow;
            this.centerFindResultsToolStripMenuItem.Tag = this.findResultsDockableWindow;
            this.dockFindResultsToolStripMenuItem.Tag = this.findResultsDockableWindow;
            this.floatFindResultsToolStripMenuItem.Tag = this.findResultsDockableWindow;

            this.showErrorsToolStripMenuItem.Tag = this.errorsDockableWindow;
            this.centerErrorsToolStripMenuItem.Tag = this.errorsDockableWindow;
            this.dockErrorsToolStripMenuItem.Tag = this.errorsDockableWindow;
            this.floatErrorsToolStripMenuItem.Tag = this.errorsDockableWindow;

            this.showHelpToolStripMenuItem.Tag = this.helpDockableWindow;
            this.centerHelpToolStripMenuItem.Tag = this.helpDockableWindow;
            this.dockHelpToolStripMenuItem.Tag = this.helpDockableWindow;
            this.floatHelpToolStripMenuItem.Tag = this.helpDockableWindow;
        }

        private void SetMenuItemImageFromToolbarItem( ScriptEditor.AppAction appAction )
        {
            ToolStripItem item = ScriptEditor_GetToolbarItemByTag( appAction );
            if ( item != null )
            {
                ToolStripItem menuItem = ScriptEditor_GetMenuItemByTag( appAction );
                if ( menuItem != null )
                {
                    menuItem.Image = item.Image.Clone() as Image;
                    menuItem.ImageTransparentColor = item.ImageTransparentColor;
                }
            }
        }

        public bool Init( string[] args )
        {
            // setup command line arguments
            rageCommandLineParser cmdParser = new rageCommandLineParser();
            rageCommandLineItem filesItem = cmdParser.AddItem( new rageCommandLineItem( "", rageCommandLineItem.NumValues.OneOrMore, false,
                "projectFile|scriptFile...", "One project file and/or any number of script files to load." ) );
            rageCommandLineItem fileItem = cmdParser.AddItem( new rageCommandLineItem( "file", rageCommandLineItem.NumValues.OneOrMore, false,
                "scriptFile...", "One or more files to load." ) );
            rageCommandLineItem projectItem = cmdParser.AddItem( new rageCommandLineItem( "project", 1, false,
                "projectFile", "One project to load." ) );
            rageCommandLineItem adminItem = cmdParser.AddItem( new rageCommandLineItem( "admin", rageCommandLineItem.NumValues.None, false,
                "", "Enables admin mode so one can modify the Default Settings that get loaded by the Parser Plugin DLL." ) );
            rageCommandLineItem buildItem = cmdParser.AddItem( new rageCommandLineItem( "build", rageCommandLineItem.NumValues.OneOrMore, false,
                "projectFile|scriptFile...", "One project file and/or any number of script files to compile.  Call 'ragScriptCompiler.exe' directly for more compiling options." ) );
            rageCommandLineItem cleanItem = cmdParser.AddItem( new rageCommandLineItem( "clean", rageCommandLineItem.NumValues.None, false,
                "", "Clean the file(s) before compiling." ) );
            rageCommandLineItem logItem = cmdParser.AddItem( new rageCommandLineItem( "log", rageCommandLineItem.NumValues.NoneOrOne, false,
                "[textFile]", "Save output to a file.  Without a value, defaults to 'log.txt' in the same directory as the project or the current directory." ) );
            rageCommandLineItem guiNameItem = cmdParser.AddItem( new rageCommandLineItem( "guiName", 1, false,
                "name", "Needed by compiles that use the Default Compiling option.  If not specified, we attempt to find an executable name that contains 'ScriptEditor' located in the same directory as the ScriptCompiler." ) );
            rageCommandLineItem versionItem = cmdParser.AddItem( new rageCommandLineItem( "version", rageCommandLineItem.NumValues.NoneOrOne, false,
                "[textFile]", "Displays the version number or writes it to the text file when specified." ) );
            rageCommandLineItem usageItem = cmdParser.AddItem( new rageCommandLineItem( "usage", rageCommandLineItem.NumValues.None, false,
                "", "Displays this usage information." ) );
            rageCommandLineItem cacheDirItem = cmdParser.AddItem( new rageCommandLineItem( "cacheDir", rageCommandLineItem.NumValues.OneOrMore, false,
                "languageName directory", "Overrides the saved or default Cache Directory for the given language." ) );
            rageCommandLineItem configItem = cmdParser.AddItem( new rageCommandLineItem( "config", 1, false,
                "configuration name", "Specifies the configuration on which to perform the build, rebuild or clean operation.  Without this option, the last active configuration is used." ) );

            if ( !cmdParser.Parse( args ) )
            {
                DialogResult result = rageMessageBox.ShowError( this, cmdParser.Error + "\n\nWould you like to continue?",
                    ApplicationSettings.ProductName + " Command Line Error", MessageBoxButtons.YesNo );
                if ( result == DialogResult.No )
                {
                    return false;
                }
            }

            if ( usageItem.WasSet )
            {
                rageMessageBox.ShowInformation( this, cmdParser.Usage, ApplicationSettings.ProductName + " Command Line Usage" );
                return false;
            }

            // version number t
            if ( versionItem.WasSet )
            {
                string textFile = versionItem.Value as string;
                if ( textFile != string.Empty )
                {
                    try
                    {
                        StreamWriter writer = File.CreateText( textFile );
                        writer.WriteLine( "Version = " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() );
                        writer.Close();
                    }
                    catch ( Exception e )
                    {
                        rageMessageBox.ShowError( this, e.Message, "Error Creating Version File" );
                    }
                }
                else
                {
                    rageMessageBox.ShowInformation( this, System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(),
                        "Script Editor Version" );
                    return false;
                }
            }

            // pull out build
            if ( buildItem.WasSet )
            {
                try
                {
                    ProcessStartInfo psInfo = new ProcessStartInfo( "ragScriptCompiler.exe" );

                    System.Text.StringBuilder compilerArgs = new System.Text.StringBuilder();

                    // add files
                    string[] files = buildItem.Value as string[];
                    foreach ( string file in files )
                    {
                        if ( file.IndexOf( " " ) > -1 )
                        {
                            compilerArgs.Append( "\"" );
                            compilerArgs.Append( file );
                            compilerArgs.Append( "\" " );
                        }
                        else
                        {
                            compilerArgs.Append( file );
                            compilerArgs.Append( " " );
                        }
                    }

                    // add clean
                    if ( cleanItem.WasSet )
                    {
                        compilerArgs.Append( "-" );
                        compilerArgs.Append( cleanItem.Name );
                        compilerArgs.Append( " " );
                    }

                    // add log
                    if ( logItem.WasSet )
                    {
                        compilerArgs.Append( "-" );
                        compilerArgs.Append( logItem.Name );
                        compilerArgs.Append( " " );

                        string logFile = logItem.Value as string;
                        if ( logFile != string.Empty )
                        {
                            if ( logFile.IndexOf( " " ) > -1 )
                            {
                                compilerArgs.Append( "\"" );
                                compilerArgs.Append( logFile );
                                compilerArgs.Append( "\" " );
                            }
                            else
                            {
                                compilerArgs.Append( logFile );
                                compilerArgs.Append( " " );
                            }
                        }
                    }

                    // add guiName
                    if ( guiNameItem.WasSet )
                    {
                        compilerArgs.Append( "-" );
                        compilerArgs.Append( guiNameItem.Name );
                        compilerArgs.Append( " " );

                        string guiName = guiNameItem.Value as string;
                        if ( guiName.IndexOf( " " ) > -1 )
                        {
                            compilerArgs.Append( "\"" );
                            compilerArgs.Append( guiName );
                            compilerArgs.Append( "\" " );
                        }
                        else
                        {
                            compilerArgs.Append( guiName );
                            compilerArgs.Append( " " );
                        }
                    }

                    // add cacheDir
                    if ( cacheDirItem.WasSet )
                    {
                        compilerArgs.Append( "-" );
                        compilerArgs.Append( cacheDirItem.Name );
                        compilerArgs.Append( " " );

                        string[] vals = cacheDirItem.Value as string[];
                        foreach ( string val in vals )
                        {
                            compilerArgs.Append( " " );
                            compilerArgs.Append( val );
                        }
                    }

                    // add config
                    if ( configItem.WasSet )
                    {
                        compilerArgs.Append( "-" );
                        compilerArgs.Append( configItem.Name );
                        compilerArgs.Append( " " );
                        compilerArgs.Append( configItem.Value as string );
                    }

                    psInfo.Arguments = compilerArgs.ToString();
                    psInfo.UseShellExecute = false;

                    Process.Start( psInfo );
                }
                catch ( Exception e )
                {
                    rageMessageBox.ShowError( this, e.Message, String.Format( "{0} Command Line Build Error", ApplicationSettings.ProductName ) );
                }
                return false;
            }

            ScriptEditor.LoadPlugins( ApplicationSettings.ExecutablePath, this );

            m_ProjectExplorer.LanguageNames = m_ScriptEditor.LanguageNames;
            m_ProjectExplorer.OpenFileDialogFilter = m_ScriptEditor.BuildScriptFileDialogFilter();

            string layout = LoadLayout();

            // this will start us off right
            m_userEditorSettings.Validate();

            this.sandDockManager1.DocumentOverflow
                = m_userEditorSettings.Files.DocumentOverflowScrollable ? DocumentOverflowMode.Scrollable : DocumentOverflowMode.Menu;

            if ( String.IsNullOrEmpty( m_userEditorSettings.CurrentCompilingLanguage ) )
            {
                // this will start us off right
                m_userEditorSettings.CurrentCompilingLanguage = "SanScript";
                m_userEditorSettings.CurrentCompilingSettingsIndex = 0;
            }

            this.findResultsRichTextBox.WordWrap = m_userEditorSettings.FindReplaceSettings.UseWordWrapping;

            m_ScriptEditor.FindReplaceForm.SetUIFromSettings( m_userEditorSettings.FindReplaceSettings );
            m_ScriptEditor.SearchReplaceDialog.SetUIFromSettings( m_userEditorSettings.FindReplaceSettings );

            foreach ( LanguageSettings l in m_userEditorSettings.LanguageSettingsList )
            {
                m_ScriptEditor.SetParserPluginLanguageSettings( l );
            }

            EnableSemanticParsingService( false );

            foreach ( CompilingSettingsCollection collection in m_userEditorSettings.LanguageCompilingSettings.Values )
            {
                m_ScriptEditor.SetParserPluginCompilingSettings( collection.CurrentSettings );
            }

            EnableSemanticParsingService( m_userEditorSettings.Files.ParsingServiceEnabled );

            m_searchReplaceInFilesDialog.AddScriptFileExtensions( m_ScriptEditor.BuildScriptFileExtensionsList() );

            // override cacheDir
            if ( cacheDirItem.WasSet )
            {
                string[] vals = cacheDirItem.Value as string[];

                if ( vals.Length % 2 != 0 )
                {
                    DialogResult result = rageMessageBox.ShowWarning( this,
                        String.Format( "Command Line Item '{0}' does not contain an even number of items.\n\nWould you like to continue?",
                        cacheDirItem.Name ), ApplicationSettings.ProductName + " Invalid CacheDir Argument", MessageBoxButtons.YesNo );
                    if ( result == DialogResult.No )
                    {
                        return false;
                    }
                }

                for ( int i = 1; i < vals.Length; i += 2 )
                {
                    if ( !Path.IsPathRooted( vals[i] ) || (vals[i].IndexOfAny( Path.GetInvalidPathChars() ) > -1) )
                    {
                        DialogResult result = rageMessageBox.ShowWarning( this,
                            String.Format( "Unable to set {0} Cache Directory.  '{1}' is not a valid directory.\n\nWould you like to continue?", vals[i - 1], vals[i] ),
                            String.Format( "{0} Invalid Cache Directory", ApplicationSettings.ProductName ), MessageBoxButtons.YesNo );
                        if ( result == DialogResult.No )
                        {
                            return false;
                        }
                    }
                    else if ( !m_ScriptEditor.SetLanguageCacheDirectory( vals[i - 1], vals[i] ) )
                    {
                        DialogResult result = rageMessageBox.ShowWarning( this,
                            String.Format( "Unable to set {0} Cache Directory.  '{1}' is not a valid language name.\n\nWould you like to continue?", vals[i - 1], vals[i - 1] ),
                            String.Format( "{0} Invalid Cache Directory", ApplicationSettings.ProductName ), MessageBoxButtons.YesNo );
                        if ( result == DialogResult.No )
                        {
                            return false;
                        }
                    }
                }
            }

            this.sandDockManager1.AllowMiddleButtonClosure = m_userEditorSettings.Files.MiddleClickClose;

            m_editorToolbar.Init( m_userEditorSettings.Toolbar, this.toolStripContainer1,
                m_userEditorSettings.Files.ShowLineNumbers, m_userEditorSettings.IntellisenseSettings.HighlightEnums,
                m_userEditorSettings.IntellisenseSettings.HighlightQuickInfoPopups,
                m_userEditorSettings.IntellisenseSettings.HighlightDisabledCodeBlocks );

            SetToolbarCompilingConfigurations();
            SetToolbarPlatforms();

            // Disable appropriate toolbar and menu items.
            m_ScriptEditor.SetCurrentEditorFromTabControl( null );

            // pull all of our menu icons from ragScriptEditorShared
            ScriptEditor.AppAction[] actions = Enum.GetValues( typeof( ScriptEditor.AppAction ) ) as ScriptEditor.AppAction[];
            foreach ( ScriptEditor.AppAction action in actions )
            {
                SetMenuItemImageFromToolbarItem( action );
            }

            List<string> fileNames = new List<string>();
            string projectFile = null;

            // pull out the file(s) we're going to load
            if ( filesItem.WasSet )
            {
                string[] files = filesItem.Value as string[];
                foreach ( string file in files )
                {
                    string fullPathFile = Path.GetFullPath( file );

                    if ( file.EndsWith( "proj" ) && (projectFile == null) )
                    {
                        projectFile = fullPathFile;
                    }
                    else if ( m_ScriptEditor.IsScriptFile( fullPathFile ) )
                    {
                        fileNames.Add( fullPathFile.ToLower() );
                    }
                }
            }

            // pull out the -file(s) to load
            if ( fileItem.WasSet )
            {
                string[] files = fileItem.Value as string[];
                foreach ( string file in files )
                {
                    string fullPathFile = Path.GetFullPath( file );

                    if ( m_ScriptEditor.IsScriptFile( fullPathFile ) )
                    {
                        fileNames.Add( fullPathFile.ToLower() );
                    }
                }
            }

            // pull out the project to load
            if ( projectItem.WasSet && (projectFile == null) )
            {
                string proj = projectItem.Value as string;
                if ( proj.EndsWith( "proj" ) )
                {
                    projectFile = Path.GetFullPath( proj );
                }
            }

            // pull out admin
            if ( adminItem.WasSet )
            {
                ApplicationSettings.AdminMode = true;
                m_userEditorSettings.IntellisenseSettings.VerboseDocumentOutline = true;
            }

            // load previous items if we're not on the command line
            if ( (projectFile == null) && (fileNames.Count == 0) )
            {
                // load last open project
                if ( Settings.Default.ReloadLastProject )
                {
                    projectFile = Settings.Default.LastProject;
                    if ( !String.IsNullOrEmpty( projectFile ) )
                    {
                        if ( LoadProjectFile( projectFile ) )
                        {
                            projectFile = null;
                        }
                    }
                    else
                    {
                        projectFile = null;
                    }
                }

                // load non-project files we found in the layout
                if ( m_textEditorSettings.Count > 0 )
                {
                    m_ScriptEditor.LoadFiles( m_textEditorSettings );
                    m_textEditorSettings.Clear();
                }
            }

            // now we can set the layout
            if ( !String.IsNullOrEmpty( layout ) )
            {
                try
                {
                    this.sandDockManager1.SetLayout( layout );
                }
                catch ( Exception e )
                {
                    rageMessageBox.ShowExclamation( this,
                        String.Format( "Unable to restore the SandDock Layout.  The program will continue but with the default layout.\n\n{0}", e.ToString() ),
                        "Set Layout Problem" );
                }
                finally
                {
                    // look for windows that were disposed
                    List<SyntaxEditor> editors = new List<SyntaxEditor>();
                    List<DockControl> windows = new List<DockControl>();
                    foreach ( KeyValuePair<TD.SandDock.DockControl, TabbedDocumentInfo> pair in m_dockControlToTabbedDocumentInfoMap )
                    {
                        if ( pair.Key.IsDisposed || pair.Value.Editor.IsDisposed )
                        {
                            editors.Add( pair.Value.Editor );
                            windows.Add( pair.Key );
                        }
                    }

                    foreach ( SyntaxEditor editor in editors )
                    {
                        if ( !m_ScriptEditor.CloseFile( editor ) )
                        {
                            Log.Error( "Failed to close a file that wasn't in the SandDock layout." );
                        }

                        m_editorToTabbedDocumentInfoMap.Remove( editor );
                    }

                    foreach ( DockControl window in windows )
                    {
                        m_dockControlToTabbedDocumentInfoMap.Remove( window );
                    }
                }

                // ensure all windows are at least partially visible on screen
                DockControl[] dockControls = this.sandDockManager1.GetDockControls();
                foreach ( DockControl dockControl in dockControls )
                {
                    Point loc = dockControl.FloatingLocation;
                    Size size = dockControl.FloatingSize;
                    rageImage.AdjustForResolution( Settings.Default.ScreenSize, rageImage.GetScreenBounds(),
                        ref loc, ref size );

                    dockControl.FloatingLocation = loc;
                    dockControl.FloatingSize = size;
                }
            }

            // load project we found on the command line
            if ( projectFile != null )
            {
                LoadProjectFile( projectFile );
            }

            // load additional files we found on the command line
            if ( fileNames.Count > 0 )
            {
                m_ScriptEditor.LoadFiles( fileNames );
            }

            // create new file if we didn't load anything
            if ( (m_ScriptEditor.CurrentSyntaxEditor == null) && !m_ProjectExplorer.IsLoaded )
            {
                m_ScriptEditor.ExecuteAppAction( ScriptEditor.AppAction.FileNew );
            }

            return true;
        }
        #endregion

        #region Event Handlers
        private void ExecuteAppAction_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            if ( menuItem != null )
            {
                ScriptEditor.AppAction action = (ScriptEditor.AppAction)Enum.Parse( typeof( ScriptEditor.AppAction ), menuItem.Tag.ToString() );

                if (action == ScriptEditor.AppAction.IncredibuildBuild)
                {
                    buildProjectToolStripMenuItem_Click(this, EventArgs.Empty);
                }
                else if (action == ScriptEditor.AppAction.IncredibuildRebuild)
                {
                    rebuildProjectToolStripMenuItem_Click(this, EventArgs.Empty);
                }
                else if (action == ScriptEditor.AppAction.IncredibuildClean)
                {
                    cleanProjectToolStripMenuItem_Click(this, EventArgs.Empty);
                }
                else if (action == ScriptEditor.AppAction.IncredibuildStop)
                {
                    stopBuildToolStripMenuItem_Click(this, EventArgs.Empty);
                }
                else if (action == ScriptEditor.AppAction.FileSaveProject)
                {
                    saveProjectToolStripMenuItem_Click(this, EventArgs.Empty);
                }
                else if (action == ScriptEditor.AppAction.FileOpenProject)
                {
                    openProjectToolStripMenuItem_Click(this, EventArgs.Empty);
                }
                else if (action == ScriptEditor.AppAction.FileCloseProject)
                {
                    closeProjectToolStripMenuItem_Click(this, EventArgs.Empty);
                }
                else
                {
                    m_ScriptEditor.ExecuteAppAction(action);
                }
            }
        }

        #region Main Menu
        private void fileToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            RefreshFileMenuItems();
        }

        #region File Menu
        private void newProjectToolStripMenuItem_Click( object sender, EventArgs e )
        {
            // take care of closing an existing project.
            if ( m_ProjectExplorer.IsLoaded )
            {
                closeProjectToolStripMenuItem_Click( sender, e );
            }

            if ( !m_ProjectExplorer.IsLoaded && m_ProjectExplorer.CreateNewProject() )
            {
                m_ScriptEditor.ProjectLoaded( new List<string>() );
            }
        }

        private void openProjectToolStripMenuItem_Click( object sender, EventArgs e )
        {
            // select project to load
            DialogResult result = this.projectOpenFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                LoadProjectFile( this.projectOpenFileDialog.FileName );
            }
        }

        private void closeProjectToolStripMenuItem_Click( object sender, EventArgs e )
        {
            DialogResult result = DialogResult.None;

            if ( m_ProjectExplorer.IsModified )
            {
                result = rageMessageBox.ShowWarning( this,
                    String.Format( "{0}\r\nThis file has been modified. Do you wish to save it before closing?\r\n", m_ProjectExplorer.ProjectFileName ),
                    "Project modified", MessageBoxButtons.YesNoCancel );
            }

            if ( result == DialogResult.Yes )
            {
                SaveProjectFile();
            }

            if ( (result == DialogResult.None) || (result == DialogResult.No) || (result == DialogResult.Yes) )
            {
                AddRecentProject( m_ProjectExplorer.ProjectFileName );
                CloseProjectFile();
            }
        }

        private void saveProjectToolStripMenuItem_Click( object sender, EventArgs e )
        {
            SaveProjectFile();
        }

        private void recentFilesItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            if ( !m_ScriptEditor.LoadFile( menuItem.Text ) )
            {
                this.recentFilesToolStripMenuItem.DropDownItems.Remove( menuItem );
            }
        }

        private void recentProjectsItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            if ( !LoadProjectFile( menuItem.Text ) )
            {
                this.recentProjectsToolStripMenuItem.DropDownItems.Remove( menuItem );
            }
        }

        private void exitToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.Close();
        }
        #endregion

        private void editToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.showLineNumbersToolStripMenuItem.Checked = m_userEditorSettings.Files.ShowLineNumbers;
        }

        #region Edit Menu
        private void showLineNumbersToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.Files.ShowLineNumbers = this.showLineNumbersToolStripMenuItem.Checked;
            m_ScriptEditor.SetLineNumberMarginVisible( m_userEditorSettings.Files.ShowLineNumbers );
        }
        #endregion

        #region View Menu
        private void showToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( sender is ToolStripMenuItem )
            {
                DockControl window = (sender as ToolStripMenuItem).Tag as DockControl;
                if ( window != null )
                {
                    window.Open( WindowOpenMethod.OnScreenActivate );
                }
            }
        }

        private void centerToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( sender is ToolStripMenuItem )
            {
                DockControl window = (sender as ToolStripMenuItem).Tag as DockControl;
                if ( window != null )
                {
                    if ( window.DockSituation == DockSituation.Floating )
                    {
                        Rectangle rect = this.Bounds;

                        int x = (rect.Width / 2) + rect.Left - (window.FloatingSize.Width / 2);
                        int y = (rect.Height / 2) + rect.Top - (window.FloatingSize.Height / 2);

                        window.FloatingLocation = new Point( x, y );
                    }
                }
            }
        }

        private void dockToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( sender is ToolStripMenuItem )
            {
                DockControl window = (sender as ToolStripMenuItem).Tag as DockControl;
                if ( window != null )
                {
                    window.OpenDocked();
                }
            }
        }

        private void floatToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( sender is ToolStripMenuItem )
            {
                DockControl window = (sender as ToolStripMenuItem).Tag as DockControl;
                if ( window != null )
                {
                    window.OpenFloating();
                }
            }
        }
        #endregion

        private void compilingToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            RefreshCompilingMenuItems();

            if ( m_ProjectExplorer.IsLoaded )
            {
                this.compileProjectToolStripMenuItem.Text = "Compile Project";
                this.recompileProjectToolStripMenuItem.Text = "Recompile Project";
            }
            else
            {
                this.compileProjectToolStripMenuItem.Text = "Compile All Files";
                this.recompileProjectToolStripMenuItem.Text = "Recompile All Files";
            }

            this.saveBeforeCompileToolStripMenuItem.Checked = m_SavingAndCompiling == SavingAndCompiling.SaveBeforeCompile;
            this.dontSaveBeforeCompileToolStripMenuItem.Checked = m_SavingAndCompiling == SavingAndCompiling.DontSaveBeforeCompile;
            this.saveAllBeforeCompileToolStripMenuItem.Checked = m_SavingAndCompiling == SavingAndCompiling.SaveAllBeforeCompile;
        }

        #region Compiling Menu
        private void compileProjectToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( m_ScriptEditor.IsProjectCompilable() )
            {
                m_ProjectExplorer.CompileProject( false );
            }
            else if ( m_ScriptEditor.AnyOpenFilesCompilable() )
            {
                m_compiler.CompileFileList(m_ScriptEditor.GetAllOpenTabFilenames(), m_ProjectExplorer.UserProjectSettings.SelectedPlatform);
            }
        }

        private void recompileProjectToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( m_ScriptEditor.IsProjectCompilable() )
            {
                m_ProjectExplorer.CompileProject( true );
            }
            else if ( m_ScriptEditor.AnyOpenFilesCompilable() )
            {
                m_compiler.RecompileFileList(m_ScriptEditor.GetAllOpenTabFilenames(), m_ProjectExplorer.UserProjectSettings.SelectedPlatform);
            }
        }

        private void compileFileToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( m_ScriptEditor.CurrentSyntaxEditor == null )
            {
                RefreshCompilingMenuItems();
                return;
            }

            string filename = m_ScriptEditor.CurrentSyntaxEditor.Document.Filename;
            if ( filename != string.Empty )
            {
                m_compiler.CompileFile(filename, m_ProjectExplorer.UserProjectSettings.SelectedPlatform);
            }
        }

        private void stopCompileToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_compiler.StopCompile = true;
        }

        private void gotoNextErrorToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( m_compileResultOffsets.Count == 0 )
            {
                return;
            }

            int nextResult = m_CurrentErrorNumber + 1;
            if ( nextResult == m_compileResultOffsets.Count )
            {
                nextResult = 0;
            }

            while ( nextResult != m_CurrentErrorNumber )
            {
                if ( SelectLineFromCharIndex( m_compileResultOffsets.ElementAt(nextResult), Compiler.CompileErrorRegex, this.outputRichTextBox, true,
                    m_compileResultOffsets, ref nextResult ) )
                {
                    m_CurrentErrorNumber = nextResult;
                    break;
                }

                ++nextResult;
                if ( nextResult == m_compileResultOffsets.Count )
                {
                    nextResult = 0;
                }
            }
        }

        private void saveBeforeCompileToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_SavingAndCompiling = SavingAndCompiling.SaveBeforeCompile;
        }

        private void dontSaveBeforeCompileToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_SavingAndCompiling = SavingAndCompiling.DontSaveBeforeCompile;
        }

        private void saveAllBeforeCompileToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_SavingAndCompiling = SavingAndCompiling.SaveAllBeforeCompile;
        }

        private void startDebugToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( !m_ProjectExplorer.IsLoaded )
            {
                return;
            }

            bool compile = false;

            // check if we need to compile before we start debugging
            string listname = GetCompiledProjectFilename( m_ProjectExplorer.ProjectFileName );
            if ( File.Exists( listname ) )
            {
                DateTime listDate = File.GetLastWriteTime( listname );

                List<string> projectFiles = m_ProjectExplorer.GetProjectFilenames();

                // see if any files are newer than the last compile file list
                foreach ( string file in projectFiles )
                {
                    DateTime fileDate = File.GetLastWriteTime( file );
                    if ( listDate < fileDate )
                    {
                        compile = true;
                        break;
                    }
                }
            }
            else
            {
                compile = true;
            }

            if ( compile )
            {
                m_startDebugAfterCompile = true;
                m_ProjectExplorer.CompileProject( false );
            }
            else
            {
                ExecuteRunCommand();
            }
        }
        #endregion

        private void intellisenseToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.intellisenseEnabledToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.PerformSemanticParsing;

            this.autocompleteToolStripMenuItem.Enabled = m_userEditorSettings.IntellisenseSettings.PerformSemanticParsing;
            this.autocompleteToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.Autocomplete;

            this.autocompleteWithBracketsToolStripMenuItem.Enabled = m_userEditorSettings.IntellisenseSettings.PerformSemanticParsing && m_userEditorSettings.IntellisenseSettings.Autocomplete;
            this.autocompleteWithBracketsToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AutocompleteWithBrackets;

            this.completeWordOnFirstMatchToolStripMenuItem.Enabled = m_userEditorSettings.IntellisenseSettings.PerformSemanticParsing && m_userEditorSettings.IntellisenseSettings.Autocomplete;
            this.completeWordOnExactMatchToolStripMenuItem.Enabled = m_userEditorSettings.IntellisenseSettings.PerformSemanticParsing && m_userEditorSettings.IntellisenseSettings.Autocomplete;
            this.completeWordDisabledToolStripMenuItem.Enabled = m_userEditorSettings.IntellisenseSettings.PerformSemanticParsing && m_userEditorSettings.IntellisenseSettings.Autocomplete;

            if ( m_userEditorSettings.IntellisenseSettings.CompleteWordOnExactMatch )
            {
                this.completeWordOnFirstMatchToolStripMenuItem.Checked = false;
                this.completeWordOnExactMatchToolStripMenuItem.Checked = true;
                this.completeWordDisabledToolStripMenuItem.Checked = false;
            }
            else if ( m_userEditorSettings.IntellisenseSettings.CompleteWordOnFirstMatch )
            {
                this.completeWordOnFirstMatchToolStripMenuItem.Checked = true;
                this.completeWordOnExactMatchToolStripMenuItem.Checked = false;
                this.completeWordDisabledToolStripMenuItem.Checked = false;
            }
            else
            {
                this.completeWordOnFirstMatchToolStripMenuItem.Checked = false;
                this.completeWordOnExactMatchToolStripMenuItem.Checked = false;
                this.completeWordDisabledToolStripMenuItem.Checked = true;
            }

            this.showItemsThatContainCurrentStringToolStripMenuItem.Enabled = m_userEditorSettings.IntellisenseSettings.PerformSemanticParsing;
            this.showItemsThatContainCurrentStringToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.ShowItemsThatContainCurrentString;

            this.addIncludePathToolStripMenuItem.Enabled = m_userEditorSettings.IntellisenseSettings.PerformSemanticParsing && m_userEditorSettings.IntellisenseSettings.Autocomplete;
            this.addIncludePathToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddIncludePathSymbols;

            this.addProjectFilesToolStripMenuItem.Enabled = m_userEditorSettings.IntellisenseSettings.PerformSemanticParsing && m_userEditorSettings.IntellisenseSettings.Autocomplete;
            this.addProjectFilesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddProjectSymbols;

            this.highlightEnumsToolStripMenuItem.Enabled = m_userEditorSettings.IntellisenseSettings.PerformSemanticParsing;
            this.highlightEnumsToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.HighlightEnums;

            this.highlightQuickInfoPopupsToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.HighlightQuickInfoPopups;
            this.highlightDisabledCodeBlocksToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.HighlightDisabledCodeBlocks;

            this.filterToolStripMenuItem.Enabled = m_userEditorSettings.IntellisenseSettings.PerformSemanticParsing && m_userEditorSettings.IntellisenseSettings.Autocomplete;
        }

        #region Intellisense Menu
        private void intellisenseEnabledToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.PerformSemanticParsing = this.intellisenseEnabledToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: true );
        }

        private void autocompleteToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.Autocomplete = this.autocompleteToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void autocompleteWithBracketsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.AutocompleteWithBrackets = this.autocompleteWithBracketsToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void completeWordOnFirstMatchToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.CompleteWordOnFirstMatch = true;
            m_userEditorSettings.IntellisenseSettings.CompleteWordOnExactMatch = false;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void completeWordOnExactMatchToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.CompleteWordOnExactMatch = true;
            m_userEditorSettings.IntellisenseSettings.CompleteWordOnFirstMatch = false;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void completeWordDisabledToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.CompleteWordOnFirstMatch = false;
            m_userEditorSettings.IntellisenseSettings.CompleteWordOnExactMatch = false;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void filterToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.showConstantsToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddConstants;
            this.showEnumsToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddEnums;
            this.showEnumTypesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddEnumTypes;
            this.showNativeSubroutinesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddNativeSubroutines;
            this.showNativeTypesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddNativeTypes;
            this.showStatesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddStates;
            this.showStaticVariablesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddStaticVariables;
            this.showStructTypesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddStructTypes;
            this.showSubroutinesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddSubroutines;
        }

        #region Filter Menu
        private void selectAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.showConstantsToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddConstants = true;
            this.showEnumsToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddEnums = true;
            this.showEnumTypesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddEnumTypes = true;
            this.showNativeSubroutinesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddNativeSubroutines = true;
            this.showNativeTypesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddNativeTypes = true;
            this.showStatesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddStates = true;
            this.showStaticVariablesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddStaticVariables = true;
            this.showStructTypesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddStructTypes = true;
            this.showSubroutinesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddSubroutines = true;

            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void selectNoneToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.showConstantsToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddConstants = false;
            this.showEnumsToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddEnums = false;
            this.showEnumTypesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddEnumTypes = false;
            this.showNativeSubroutinesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddNativeSubroutines = false;
            this.showNativeTypesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddNativeTypes = false;
            this.showStatesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddStates = false;
            this.showStaticVariablesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddStaticVariables = false;
            this.showStructTypesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddStructTypes = false;
            this.showSubroutinesToolStripMenuItem.Checked = m_userEditorSettings.IntellisenseSettings.AddSubroutines = false;

            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void showConstantsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.AddConstants = this.showConstantsToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void showNativeTypesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.AddNativeTypes = this.showNativeTypesToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void showStructTypesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.AddStructTypes = this.showStructTypesToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void showEnumTypesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.AddEnumTypes = this.showEnumTypesToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void showEnumsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.AddEnums = this.showEnumsToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void showStaticVariablesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.AddStaticVariables = this.showStaticVariablesToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void showSubroutinesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.AddSubroutines = this.showSubroutinesToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void showNativeSubroutinesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.AddNativeSubroutines = this.showNativeSubroutinesToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void showStatesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.AddStates = this.showStaticVariablesToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }
        #endregion

        private void showItemsThatContainCurrentStringToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.ShowItemsThatContainCurrentString = this.showItemsThatContainCurrentStringToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void addProjectFilesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.AddProjectSymbols = this.addProjectFilesToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: true );
        }

        private void addIncludePathToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.AddIncludePathSymbols = this.addIncludePathToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: true );
        }

        private void highlightEnumsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.HighlightEnums = this.highlightEnumsToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: true );
        }

        private void highlightQuickInfoPopupsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.HighlightQuickInfoPopups = this.highlightQuickInfoPopupsToolStripMenuItem.Checked;

            foreach ( SyntaxEditor editor in m_editorToTabbedDocumentInfoMap.Keys )
            {
                editor.IntelliPrompt.QuickInfo.CollapsedOutliningNodeSyntaxHighlightingEnabled
                    = m_userEditorSettings.IntellisenseSettings.HighlightQuickInfoPopups;
            }

            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void highlightDisabledCodeBlocksToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.HighlightDisabledCodeBlocks = this.highlightDisabledCodeBlocksToolStripMenuItem.Checked;
            m_ScriptEditor.SetIntellisenseOptions( reparse: true );
        }
        #endregion

        private void searchToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.enableWordWrappingInFindResultsToolStripMenuItem.Checked = this.findResultsRichTextBox.WordWrap;
        }

        #region Search Menu
        private void useNewFindReplaceDialogToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( m_userEditorSettings.FindReplaceSettings.UseNewDialog )
            {
                if ( m_ScriptEditor.SearchReplaceDialog.Visible )
                {
                    m_ScriptEditor.SearchReplaceDialog.CancelSearchReplace();

                    m_ScriptEditor.SearchReplaceDialog.Close();
                    m_ScriptEditor.FindReplaceForm.Show( this );
                }
            }
            else
            {
                if ( m_ScriptEditor.FindReplaceForm.Visible )
                {
                    m_ScriptEditor.FindReplaceForm.Close();
                    m_ScriptEditor.SearchReplaceDialog.Show( this );
                }
            }

            m_userEditorSettings.FindReplaceSettings.UseNewDialog = this.useNewFindReplaceDialogToolStripMenuItem.Checked;
            m_ScriptEditor.SetupSearchCommandLinks( m_userEditorSettings.FindReplaceSettings.UseNewDialog );
        }

        private void useNewFindReplaceInFilesDialogToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( m_useNewFindInFilesDialog )
            {
                if ( m_searchReplaceInFilesDialog.Visible )
                {
                    m_searchReplaceInFilesDialog.CancelSearchReplace();
                    m_searchReplaceInFilesDialog.Close();

                    m_findReplaceInFilesForm.ShowIt( this, m_ScriptEditor.BuildScriptFileExtensionsList() );
                }
            }
            else
            {
                if ( m_findReplaceInFilesForm.Visible )
                {
                    m_findReplaceInFilesForm.StopThread();
                    m_findReplaceInFilesForm.Close();

                    m_searchReplaceInFilesDialog.ShowDialog( this );
                }
            }

            m_useNewFindInFilesDialog = this.useNewFindReplaceInFilesDialogToolStripMenuItem.Checked;
        }

        private void findReplaceInFilesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( m_userEditorSettings.FindReplaceSettings.UseNewDialog )
            {
                if ( m_ScriptEditor.SearchReplaceDialog.Visible )
                {
                    m_ScriptEditor.SearchReplaceDialog.Close();
                }
            }
            else if ( m_ScriptEditor.FindReplaceForm.Visible )
            {
                m_ScriptEditor.FindReplaceForm.Close();
            }

            m_ScriptEditor.CancelActiveIntellisense();

            // Show the find/replace form
            this.findResultsDockableWindow.Open();

            if ( m_useNewFindInFilesDialog )
            {
                m_searchReplaceInFilesDialog.InitSearchText( m_ScriptEditor.CurrentSyntaxEditor );

                m_searchReplaceInFilesDialog.ShowDialog( this );

                if ( m_searchReplaceInFilesDialog.StartPosition == FormStartPosition.CenterParent )
                {
                    // so the next time we open the window, it will be in the same position as before
                    m_searchReplaceInFilesDialog.StartPosition = FormStartPosition.Manual;
                }
            }
            else
            {
                m_findReplaceInFilesForm.InitSearchText( m_ScriptEditor.CurrentSyntaxEditor );

                m_findReplaceInFilesForm.ShowIt( this, m_ScriptEditor.BuildScriptFileExtensionsList() );
            }
        }

        private void gotoNextFindResultToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( m_findResultOffsets.Count == 0 )
            {
                return;
            }

            int nextResult = m_CurrentFindResultNumber + 1;
            if ( nextResult == m_findResultOffsets.Count )
            {
                nextResult = 0;
            }

            while ( nextResult != m_CurrentFindResultNumber )
            {
                if ( SelectLineFromCharIndex( m_findResultOffsets.ElementAt(nextResult), m_rExpFind, this.findResultsRichTextBox, false,
                    m_findResultOffsets, ref nextResult ) )
                {
                    m_CurrentFindResultNumber = nextResult;
                    break;
                }

                ++nextResult;
                if ( nextResult == m_findResultOffsets.Count )
                {
                    nextResult = 0;
                }
            }
        }
        #endregion

        private void outliningToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            if ( m_ScriptEditor.CurrentSyntaxEditor == null )
            {
                this.startAutomaticOutliningToolStripMenuItem.Enabled = false;
                this.startManualOutliningToolStripMenuItem.Enabled = false;
                this.stopOutliningToolStripMenuItem.Enabled = false;

                this.hideCurrentSelectionToolStripMenuItem.Enabled = false;
                this.stopHidingCurrentSelectionToolStripMenuItem.Enabled = false;

                this.toggleOutliningExpansionToolStripMenuItem.Enabled = false;
                this.toggleAllOutliningToolStripMenuItem.Enabled = false;
            }
            else
            {
                this.startAutomaticOutliningToolStripMenuItem.Enabled = (m_ScriptEditor.CurrentSyntaxEditor.Document.Outlining.Mode != OutliningMode.Automatic);
                this.startManualOutliningToolStripMenuItem.Enabled = (m_ScriptEditor.CurrentSyntaxEditor.Document.Outlining.Mode != OutliningMode.Manual);
                this.stopOutliningToolStripMenuItem.Enabled = (m_ScriptEditor.CurrentSyntaxEditor.Document.Outlining.Mode != OutliningMode.None);

                this.hideCurrentSelectionToolStripMenuItem.Enabled = (m_ScriptEditor.CurrentSyntaxEditor.Document.Outlining.Mode == OutliningMode.Manual);
                this.stopHidingCurrentSelectionToolStripMenuItem.Enabled = (m_ScriptEditor.CurrentSyntaxEditor.Document.Outlining.Mode == OutliningMode.Manual);

                this.toggleOutliningExpansionToolStripMenuItem.Enabled = (m_ScriptEditor.CurrentSyntaxEditor.Document.Outlining.Mode != OutliningMode.None);
                this.toggleAllOutliningToolStripMenuItem.Enabled = (m_ScriptEditor.CurrentSyntaxEditor.Document.Outlining.Mode != OutliningMode.None);
            }
        }

        private void toolsToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
        }

        #region Tools Menu
        private void settingsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            UserEditorSettings currentSettings = new UserEditorSettings();
            currentSettings.Copy( m_userEditorSettings );

            DialogResult result = m_settingsForm.ShowIt( this, currentSettings, m_ScriptEditor.DefaultEditorSettings,
                m_ProjectExplorer.ProjectFileName, ApplicationSettings.AdminMode );

            //if ( result == DialogResult.OK )
            //{
            //    m_ScriptEditor.RefreshIntellisenseQuick();
            //}

            RefreshCompilingMenuItems();
        }

        private void highlightingStyleEditorToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Font f = m_userEditorSettings.LanguageHighlightingStylesSettings.Font;
            Color c = m_userEditorSettings.LanguageHighlightingStylesSettings.TextBackgroundColor;

            DialogResult result = m_styleEditorFormLazy.Value.ShowIt( this, m_ScriptEditor.CurrentSyntaxEditor,
                m_userEditorSettings.LanguageHighlightingStylesSettings.LanguageHighlightingStyles, ref f, ref c );
            if ( result == DialogResult.OK )
            {
                m_userEditorSettings.LanguageHighlightingStylesSettings.Font = f;
                m_userEditorSettings.LanguageHighlightingStylesSettings.TextBackgroundColor = c;

                m_ScriptEditor.HighlightAllEditors();
            }
        }

        private void customButtonManagerToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_customButtomManagerForm.ShowIt( this );
        }

        private void viewLogToolStripMenuItem_Click( object sender, EventArgs e )
        {
            LogFactory.FlushApplicationLog();

            if ( File.Exists(LogFactory.ApplicationLogFilename) )
            {
                try
                {
                    Process.Start(LogFactory.ApplicationLogFilename);
                }
                catch ( Exception ex )
                {
                    rageMessageBox.ShowError( this,
                        String.Format( "Log file '{0}':\n\n{1}", LogFactory.ApplicationLogFilename, ex.ToString() ),
                        "Problem Viewing Log File" );
                }
            }
        }
        #endregion

        private void windowToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            if ( m_ScriptEditor.CurrentSyntaxEditor == null )
            {
                this.splitHorizontallyToolStripMenuItem.Enabled = false;
                this.splitVerticallyToolStripMenuItem.Enabled = false;
                this.fourWaySplitToolStripMenuItem.Enabled = false;
                this.noSplitsToolStripMenuItem.Enabled = false;

                this.splitHorizontallyToolStripMenuItem.Checked = false;
                this.splitVerticallyToolStripMenuItem.Checked = false;
                this.fourWaySplitToolStripMenuItem.Checked = false;
                this.noSplitsToolStripMenuItem.Checked = false;
            }
            else
            {
                this.splitHorizontallyToolStripMenuItem.Enabled = true;
                this.splitVerticallyToolStripMenuItem.Enabled = true;
                this.fourWaySplitToolStripMenuItem.Enabled = true;
                this.noSplitsToolStripMenuItem.Enabled = true;

                this.splitHorizontallyToolStripMenuItem.Checked = m_ScriptEditor.CurrentSyntaxEditor.SplitType == SyntaxEditorSplitType.DualHorizontal;
                this.splitVerticallyToolStripMenuItem.Checked = m_ScriptEditor.CurrentSyntaxEditor.SplitType == SyntaxEditorSplitType.DualVertical;
                this.fourWaySplitToolStripMenuItem.Checked = m_ScriptEditor.CurrentSyntaxEditor.SplitType == SyntaxEditorSplitType.FourWay;
                this.noSplitsToolStripMenuItem.Checked = m_ScriptEditor.CurrentSyntaxEditor.SplitType == SyntaxEditorSplitType.None;
            }
        }

        private void helpToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            // remove old menu items
            while ( this.helpToolStripMenuItem.DropDownItems.Count > 5 )
            {
                this.helpToolStripMenuItem.DropDownItems.RemoveAt( 2 );
            }

            // add menu items
            SortedDictionary<string, List<ToolStripMenuItem>> menuItemsByLanguage = new SortedDictionary<string, List<ToolStripMenuItem>>();
            foreach ( LanguageSettings l in m_userEditorSettings.LanguageSettingsList )
            {
                List<ToolStripMenuItem> items = l.GetHelpMenuItems();
                if ( items != null )
                {
                    menuItemsByLanguage.Add( l.Language, items );
                }
            }

            int i = 2;
            foreach ( KeyValuePair<string, List<ToolStripMenuItem>> pair in menuItemsByLanguage )
            {
                foreach ( ToolStripMenuItem menuItem in pair.Value )
                {
                    this.helpToolStripMenuItem.DropDownItems.Insert( i, menuItem );
                    ++i;
                }
            }
        }

        #region Help Menu
        private void contentsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                Process.Start( Path.Combine( ApplicationSettings.ExecutablePath, m_helpFile ) );
            }
            catch
            {
            }
        }

        private void aboutToolStripMenuItem_Click( object sender, EventArgs e )
        {
            rageMessageBox.ShowInformation( this, String.Format("Please direct questions, comments, and problems to the '{0}' mailing list.", ApplicationSettings.CommandOptions.Project.ToolsEmailAddresses.First().DisplayName),
                "About..." );
        }
        #endregion

        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {
            // NOTE: We stored the location and window state of Form1 when we loaded
            // the layout file. We wait to apply them until the Form1 is loaded because
            // SandDock doesn't play nice with multiple monitors, and tends to put it on
            // the primary monitor no matter what.
            this.Location = m_location;
            this.WindowState = m_windowState;
            this.Location = m_location;
            this.WindowState = m_windowState;
        }

        private void Form1_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( m_closed )
            {
                // toggle for when Application.Exit fails the first time
                return;
            }

            m_closed = true;

            m_findReplaceInFilesForm.StopThread();
            m_searchReplaceInFilesDialog.CancelSearchReplace();

            List<ActiproSoftware.SyntaxEditor.SyntaxEditor> unsavedEditors = m_ScriptEditor.GetModifiedEditors();

            // list of files and project that are modified
            List<string> unsavedFiles = new List<string>();
            foreach ( ActiproSoftware.SyntaxEditor.SyntaxEditor editor in unsavedEditors )
            {
                unsavedFiles.Add( editor.Document.Filename );
            }

            if ( m_ProjectExplorer.IsModified )
            {
                unsavedFiles.Insert( 0, m_ProjectExplorer.ProjectFileName );
            }

            // we have unsaved files
            if ( unsavedFiles.Count > 0 )
            {
                if ( !SaveForm.ShowIt( this, unsavedFiles, new SaveForm.SaveFilesEventHandler( this.SaveForm_SaveAllFiles ) ) )
                {
                    e.Cancel = true;
                    m_closed = false;
                    return;
                }
            }

            // The XmlDocument doesn't save properly in this scenario, so we're not going to try
            if ( e.CloseReason != CloseReason.WindowsShutDown )
            {
                SaveLayout();

                // make sure they're all marked unmodified so that CloseProjectFile won't ask us to save them again.
                foreach ( ActiproSoftware.SyntaxEditor.SyntaxEditor editor in unsavedEditors )
                {
                    editor.Document.Modified = false;
                }

                // call this so we can save the project settings
                CloseProjectFile();
            }

            ScriptEditor.ShutdownPlugins( this );
        }

        private void Form1_DragDrop( object sender, DragEventArgs e )
        {
            object files = e.Data.GetData( DataFormats.FileDrop );
            if ( (files is string[]) && (((string[])files).Length > 0) )
            {
                string[] filenames = files as string[];
                foreach ( string filename in filenames )
                {
                    LoadScriptFile( filename );
                }
            }
        }

        private void Form1_DragEnter( object sender, DragEventArgs e )
        {
            object files = e.Data.GetData( DataFormats.FileDrop );
            if ( (files is string[]) && (((string[])files).Length > 0) )
            {
                e.Effect = DragDropEffects.Move;
            }
        }
        #endregion

        #region Script File and Project File Handling
        public bool LoadScriptFile( string filename )
        {
            return m_ScriptEditor.LoadFile( filename );
        }

        private void SaveProjectFile()
        {
            if ( !m_ProjectExplorer.IsModified )
            {
                return;
            }

            // get list of open files
            CollectOpenProjectFiles( m_ProjectExplorer.UserProjectSettings.OpenTextEditors );

            // always prompt for projects
            SourceControlSettings2.SaveAction saveAction = m_userEditorSettings.SourceControl2.OnSave;
            m_userEditorSettings.SourceControl2.OnSave = SourceControlSettings2.SaveAction.Prompt;

            m_sourceControl.ResetSaveAction();

            if ( m_sourceControl.MakeFileWritableOnSave( m_ProjectExplorer.ProjectFileName, false ) )
            {
                // save the project and project settings
                m_ProjectExplorer.SaveProject();
            }

            Log.Message( "SaveProjectFile {0}", m_ProjectExplorer.ProjectFileName);

            RefreshFileMenuItems();
            RefreshCompilingMenuItems();

            m_sourceControl.LogoffSourceControl();

            m_userEditorSettings.SourceControl2.OnSave = saveAction;
        }

        public bool LoadProjectFile( string filename )
        {
            if ( m_ProjectExplorer.IsModified )
            {
                // save modified project
                DialogResult result = rageMessageBox.ShowWarning( this,
                    String.Format( "{0}\r\nThis file has been modified. Do you wish to save it before closing?\r\n", m_ProjectExplorer.ProjectFileName ),
                    "Project modified", MessageBoxButtons.YesNoCancel );
                if ( result == DialogResult.Yes )
                {
                    SaveProjectFile();
                }

                if ( result == DialogResult.Cancel )
                {
                    return false;
                }
            }

            if ( m_ProjectExplorer.IsLoaded )
            {
                AddRecentProject( m_ProjectExplorer.ProjectFileName );
                CloseProjectFile();
            }

            this.statusToolStripStatusLabel.Text = "Loading Project";
            this.Refresh();

            if ( m_ProjectExplorer.LoadProject( filename ) )
            {
                Log.Message( "LoadProjectFile {0}", m_ProjectExplorer.ProjectFileName );

                this.Text = String.Format( "{0} - {1} - Script Editor", filename, ApplicationSettings.Environment.Subst("$(branch)") );

                // override the user's settings with those from the current project
                m_userEditorSettings.Copy( m_ProjectExplorer.ProjectEditorSetting );
                m_userEditorSettings.CurrentPlatform = m_ProjectExplorer.UserProjectSettings.SelectedPlatform;

                SetToolbarCompilingConfigurations();
                SetToolbarPlatforms();

                CacheSplashForm splashForm = new CacheSplashForm( filename );
                splashForm.Start += this.splashForm_Start;
                splashForm.Stop += this.splashForm_Stop;
                splashForm.ShowDialog( this );

                // reloaded last open files
                if ( Settings.Default.ReloadLastFiles && (m_ProjectExplorer.UserProjectSettings.OpenTextEditors != null)
                    && (m_ProjectExplorer.UserProjectSettings.OpenTextEditors.Count > 0) )
                {
                    string projectDirectory = Path.GetDirectoryName( m_ProjectExplorer.ProjectFileName );
                    string originalProjectDirectory = Path.GetDirectoryName( m_ProjectExplorer.ProjectEditorSetting.OriginalFilenameExpanded );

                    // get TextEditorSettings for our project files
                    List<TextEditorSettings> settings = new List<TextEditorSettings>();
                    foreach ( TextEditorSettings textSettings in m_ProjectExplorer.UserProjectSettings.OpenTextEditors )
                    {
                        // Use the TextEditorSettings from our last session, if we have one
                        TextEditorSettings t = GetTextEditorSettingsForFileFromLayout( textSettings.Filename );
                        if ( t != null )
                        {
                            settings.Add( t );
                        }
                        else
                        {
                            // Otherwise use the project settings one.
                            settings.Add( textSettings );
                        }
                    }
                    m_textEditorSettings.Clear();

                    m_ScriptEditor.LoadFiles( settings );
                }

                RefreshCompilingMenuItems();

                this.statusToolStripStatusLabel.Text = "Ready";
                return true;
            }

            RemoveRecentProject( filename );

            this.statusToolStripStatusLabel.Text = "Ready";
            return false;
        }

        private void ReloadProjectFile()
        {
            List<string> filenames = m_ProjectExplorer.GetProjectFilenames();

            this.statusToolStripStatusLabel.Text = "Reloading Project";
            this.Refresh();

            if ( m_ProjectExplorer.ReloadProject() )
            {
                Log.Message( "ReloadProjectFile {0}", m_ProjectExplorer.ProjectFileName );

                // override the user's settings with those from the current project
                m_userEditorSettings.Copy( m_ProjectExplorer.ProjectEditorSetting );

                SetToolbarCompilingConfigurations();
                SetToolbarPlatforms();

                m_ProjectExplorer.LockProjectFile = true;

                EnableSemanticParsingService( (m_userEditorSettings.Project as NonProjectProjectSettings).ParsingServiceEnabled );

                m_ScriptEditor.SetParserPluginCompilingSettings( m_ProjectExplorer.ProjectEditorSetting.CurrentCompilingSettings );

                m_ScriptEditor.SetProjectParserProjectFilenames( m_ProjectExplorer.GetProjectFilenames() );

                // update the parser
                m_ScriptEditor.ProjectReloaded( filenames, m_ProjectExplorer.GetProjectFilenames() );

                m_ProjectExplorer.LockProjectFile = false;

                EnableSemanticParsingService( m_userEditorSettings.Files.ParsingServiceEnabled );
            }
            else
            {
                rageMessageBox.ShowError( this, "The current project failed to reload.", "Project Reload Failed" );
            }

            RefreshFileMenuItems();
            RefreshCompilingMenuItems();

            this.statusToolStripStatusLabel.Text = "Ready";
        }

        private void CloseProjectFile()
        {
            if ( m_ProjectExplorer.IsLoaded )
            {
                this.statusToolStripStatusLabel.Text = "Closing Project";
                this.Refresh();

                List<string> filenames = m_ProjectExplorer.GetProjectFilenames();

                // remember open files
                CollectOpenProjectFiles( m_ProjectExplorer.UserProjectSettings.OpenTextEditors );

                // Collect the filenames to close now, because the ProjectExplorer is going to make them relative-pathed before saving
                List<string> filesToClose = new List<string>();
                foreach ( TextEditorSettings t in m_ProjectExplorer.UserProjectSettings.OpenTextEditors )
                {
                    filesToClose.Add( t.Filename );
                }

                // close the project, saving its settings
                m_ProjectExplorer.CloseProject();
                m_ScriptEditor.CloseFiles( filesToClose );

                // update the includes parser
                m_ScriptEditor.ProjectClosed( filenames );

                //m_ScriptEditor.RefreshIntellisenseQuick();

                Log.Message( "CloseProjectFile {0}", m_ProjectExplorer.ProjectFileName );

                RefreshFileMenuItems();
                RefreshCompilingMenuItems();

                this.Text = "Script Editor";
                this.statusToolStripStatusLabel.Text = "Ready";
            }
        }
        #endregion

        #region Misc
        private void AddRecentFile( string filename )
		{
			string fullPathName = Path.GetFullPath( filename ).ToLower();

			bool found = false;
			foreach (ToolStripMenuItem item in this.recentFilesToolStripMenuItem.DropDownItems)
			{
				if ( item.Text == fullPathName )
				{
					found = true;
					break;
				}
			}

			if ( !found )
			{
                if ( this.recentFilesToolStripMenuItem.DropDownItems.Count >= 10 )
				{
                    this.recentFilesToolStripMenuItem.DropDownItems.RemoveAt( 0 );
				}

                this.recentFilesToolStripMenuItem.DropDownItems.Add( fullPathName, null, new EventHandler(recentFilesItem_Click) );
			}
		}

        private void RemoveRecentFile( string filename )
        {
            string fullPathName = Path.GetFullPath( filename ).ToLower();
            for ( int i = 0; i < this.recentFilesToolStripMenuItem.DropDownItems.Count; ++i )
            {
                if ( this.recentFilesToolStripMenuItem.DropDownItems[i].Text == fullPathName )
                {
                    this.recentFilesToolStripMenuItem.DropDownItems.RemoveAt( i );
                }
            }
        }

		private void AddRecentProject( string filename )
		{
            string fullPathName = Path.GetFullPath( filename ).ToLower();

            bool found = false;
            foreach ( ToolStripMenuItem item in this.recentProjectsToolStripMenuItem.DropDownItems )
            {
                if ( item.Text == fullPathName )
                {
                    found = true;
                    break;
                }
            }

            if ( !found )
            {
                if ( this.recentProjectsToolStripMenuItem.DropDownItems.Count >= 10 )
                {
                    this.recentProjectsToolStripMenuItem.DropDownItems.RemoveAt( 0 );
                }

                this.recentProjectsToolStripMenuItem.DropDownItems.Add( fullPathName, null, new EventHandler( recentProjectsItem_Click ) );
            }
        }

        private void RemoveRecentProject( string filename )
        {
            string fullPathName = Path.GetFullPath( filename ).ToLower();
            for ( int i = 0; i < this.recentProjectsToolStripMenuItem.DropDownItems.Count; ++i )
            {
                if ( this.recentProjectsToolStripMenuItem.DropDownItems[i].Text == fullPathName )
                {
                    this.recentProjectsToolStripMenuItem.DropDownItems.RemoveAt( i );
                }
            }
        }

        private void RefreshFileMenuItems()
        {
            m_ScriptEditor.RefreshFileMenuItems();

            bool generalSavingEnabled = m_ScriptEditor.CurrentCompileState == ScriptEditor.CompileState.Stopped;
            this.recentFilesToolStripMenuItem.Enabled = generalSavingEnabled && this.recentFilesToolStripMenuItem.DropDownItems.Count > 0;
            this.recentProjectsToolStripMenuItem.Enabled = generalSavingEnabled && this.recentProjectsToolStripMenuItem.DropDownItems.Count > 0;
        }

        private void RefreshCompilingMenuItems()
        {
            m_ScriptEditor.RefreshCompilingMenuItems();

            bool compiling = m_ScriptEditor.CurrentCompileState != ScriptEditor.CompileState.Stopped;
            this.startDebugToolStripMenuItem.Enabled = !compiling && !String.IsNullOrEmpty(m_userEditorSettings.Project.StartDebugCommand);
            this.gotoNextErrorToolStripMenuItem.Enabled = !compiling && m_compileResultOffsets.Count > 0;
        }

        /// <summary>
        /// Adds open files that are project files to the openFileCollection list
        /// </summary>
        /// <param name="openFileCollection"></param>
        private void CollectOpenProjectFiles( List<TextEditorSettings> openProjectTextEditors )
        {
            openProjectTextEditors.Clear();

            List<string> projectFiles = m_ProjectExplorer.GetProjectFilenames();
            List<string> openFiles = m_ScriptEditor.GetAllOpenTabFilenames();

            foreach ( string file in openFiles )
            {
                int indexOf = projectFiles.IndexOf( file.ToLower() );
                if ( indexOf > -1 )
                {
                    // locate the dockcontrol and editor
                    foreach ( KeyValuePair<TD.SandDock.DockControl, TabbedDocumentInfo> pair in m_dockControlToTabbedDocumentInfoMap )
                    {
                        if ( pair.Value.Editor.Document.Filename.ToLower() == projectFiles[indexOf] )
                        {
                            // create a TextEditorSettings object for the file
                            TextEditorSettings t = CreateTextEditorSettings( pair.Value.Editor, pair.Key.Guid );
                            if ( t != null )
                            {
                                openProjectTextEditors.Add( t );
                            }

                            break;
                        }
                    }
                }
            }
        }

        private const int WM_VSCROLL = 277; // Vertical scroll
        private const int SB_BOTTOM = 7; // Scroll to bottom

        [DllImport( "user32.dll", CharSet = CharSet.Auto )]
        private static extern int SendMessage( IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam );

        /// <summary>
        /// Add text to the output text box, for the given error type, and output to the log as well.
        /// </summary>
        /// <param name="errorType">The <see cref="ErrorType"/> for the error.</param>
        /// <param name="text">The text.</param>
        /// <param name="immediate">
        /// Whether or not to update the text box immediately. Set this to true if a long running operation
        /// immediately follows the output and it should be displayed before the hang.
        /// Ideally we wouldn't have hangs, but we do.
        /// </param>
        private void AddText(ErrorType errorType, string text, bool immediate = false)
        {
            switch (errorType)
            {
                case ErrorType.Info:
                    Log.Message(text);
                    break;

                case ErrorType.Warning:
                    Log.Warning(text);
                    break;

                case ErrorType.Errors:
                    Log.Error(text);
                    break;

                default:
                    Log.ToolError("Unhandled ErrorType {0}, assuming error", errorType.ToString());
                    Log.Error(text);
                    break;
            }

            if (immediate)
            {
                this.UpdateOutputTextBoxImmediately(text);
                return;
            }

            this.m_outputQueue.Enqueue(text);
            if (!this.m_outputUpdateTimer.Enabled)
            {
                this.m_outputUpdateTimer.Start();
            }
        }

        /// <summary>
        /// The output textbox can get bogged down by rendering if the output is coming in too quickly.
        /// To avoid this, we batch the output messages and flush them on a (frequent) timer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateOutputTextBox(Object sender, EventArgs e)
        {
            if (this.m_outputQueue.None())
            {
                this.m_outputUpdateTimer.Stop();
                return;
            }

            int textLength = this.outputRichTextBox.TextLength;
            StringBuilder sb = new StringBuilder();
            while (this.m_outputQueue.Any())
            {
                this.m_compileResultOffsets.Add(textLength);
                String output = $"{this.m_outputQueue.Dequeue()}\n";
                sb.Append(output);
                textLength += output.Length;
            }

            this.outputRichTextBox.AppendText(sb.ToString());
            SendMessage(this.outputRichTextBox.Handle, WM_VSCROLL, (IntPtr)SB_BOTTOM, IntPtr.Zero);
        }

        /// <summary>
        /// Add the text to the output box immediately, refresh the widget, and scroll.
        /// </summary>
        /// <param name="text">The text to add to the output box.</param>
        private void UpdateOutputTextBoxImmediately(string text)
        {
            this.outputRichTextBox.AppendText($"{text}\n");

            // +1 for newline
            this.m_compileResultOffsets.Add(text.Length + 1);
            SendMessage( this.outputRichTextBox.Handle, WM_VSCROLL, (IntPtr)SB_BOTTOM, IntPtr.Zero );
            this.outputRichTextBox.Refresh();
        }

        private void ClearOutputTextBox()
        {
            this.m_outputQueue.Clear();
            this.m_outputUpdateTimer.Stop();
            this.outputRichTextBox.Clear();
            this.m_compileResultOffsets.Clear();
            this.gotoNextErrorToolStripMenuItem.Enabled = false;
            this.m_CurrentErrorNumber = 0;
        }

        private void UpdateParseStatus(Object sender, EventArgs e)
        {
            ParseCoordinator.Stats stats = SanScriptProjectResolver.ParseCoordinator.Statistics;

            if (stats.NumPendingFiles <= 0)
            {
                this.parseStatusToolStripProgressBar.Visible = false;
                this.parseStatusToolStripStatusLabel.Visible = false;
                return;
            }

            this.parseStatusToolStripProgressBar.Visible = true;
            this.parseStatusToolStripStatusLabel.Visible = true;

            // NumProcessedFiles should not be larger than TotalNumFiles, but guard against an exception just in case.
            this.parseStatusToolStripProgressBar.Maximum = Math.Max(stats.NumProcessedFiles, stats.TotalNumFiles);
            this.parseStatusToolStripProgressBar.Value = stats.NumProcessedFiles;
        }

        /// <summary>
        /// Enables or disables the <see cref="SemanticParserService"/>.
        /// </summary>
        /// <param name="enable"><c>true</c> will enable the service, if it is not already started.  <c>false</c> will stop it if it is running.</param>
        private void EnableSemanticParsingService( bool enable )
        {
            if ( !enable )
            {
                if ( SemanticParserService.IsRunning )
                {
                    SemanticParserService.Stop();

                    Log.Message( "Stopped the SemanticParsingService." );
                }
            }
            else
            {
                if ( !SemanticParserService.IsRunning )
                {
                    SemanticParserService.Start();

                    Log.Message( "Started the SemanticParsingService." );
                }
            }
        }

        /// <summary>
        /// Returns the TextEditorSettings with a matching filename from the list that was loaded with the layout.
        /// If found, removes it from our load list.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns>TextEditorSettings, or <c>null</c> if not found.</returns>
        private TextEditorSettings GetTextEditorSettingsForFileFromLayout( string filename )
        {
            foreach ( TextEditorSettings t in m_textEditorSettings )
            {
                if ( t.Filename.Equals(filename, StringComparison.OrdinalIgnoreCase))
                {
                    return t;
                }
            }

            return null;
        }

        private TextEditorSettings CreateTextEditorSettings( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, Guid guid )
        {
            TextEditorSettings t = new TextEditorSettings( editor.Document.Filename, guid );

            if ( editor.HasHorizontalSplit )
            {
                t.HorizontalSplitPosition = editor.HorizontalSplitPosition;
            }

            if ( editor.HasVerticalSplit )
            {
                t.VerticalSplitPosition = editor.VerticalSplitPosition;
            }

            if ( !ScriptEditor.IsANewFileName( t.Filename ) )
            {
                if ( File.Exists( t.Filename ) )
                {
                    t.FileDateTime = File.GetLastWriteTime( t.Filename ).ToFileTime();
                }

                t.OutliningMode = editor.Document.Outlining.Mode;

                if ( t.OutliningMode == OutliningMode.Automatic )
                {
                    m_ScriptEditor.CollectAutomaticOutliningNodeInfo( t.OutlineNodeInfos, editor.Document.Outlining.RootNode );
                }
                else if ( t.OutliningMode == OutliningMode.Manual )
                {
                    m_ScriptEditor.CollectManualOutliningNodeInfo( t.OutlineNodeInfos, editor.Document.Outlining.RootNode );
                }
            }

            return t;
        }

        private void SetToolbarCompilingConfigurations()
        {
            List<string> configurationNames = new List<string>();

            if (m_userEditorSettings.CurrentCompilingSettingsList != null)
            {
                foreach (CompilingSettings compilingSettings in m_userEditorSettings.CurrentCompilingSettingsList)
                {
                    configurationNames.Add(compilingSettings.ConfigurationName);
                }
            }

            m_editorToolbar.SetCompilingConfigurations( configurationNames, m_userEditorSettings.CurrentCompilingSettingsIndex );
        }

        private void SetToolbarPlatforms()
        {
            //Set the first enabled platform in the toolbar.
            Platform selectedPlatform = m_userEditorSettings.CurrentPlatform;
            if (m_userEditorSettings.CurrentPlatform == Platform.Independent)
            {
                foreach (KeyValuePair<RSG.Platform.Platform, ITarget> target in ApplicationSettings.CommandOptions.Branch.Targets)
                {
                    if (target.Value.Enabled)
                    {
                        selectedPlatform = target.Key;
                        break;
                    }
                }
            }

            m_editorToolbar.SetPlatforms(m_userEditorSettings.PlatformSettings.Platforms.ToList(), selectedPlatform);
        }
        #endregion

        #region Compiling
        private ICollection<int> m_compileResultOffsets = new SortedSet<int>();

        private bool ParseOutputLine( Regex regEx, string line, ref string filename, ref int lineNumber )
        {
            Match match = regEx.Match( line );
            if ( match.Success )
            {
                string lineNumStr = match.Result( "${lineNum}" );
                lineNumber = Convert.ToInt32( lineNumStr ) - 1;

                filename = match.Result( "${File}" );
                filename = Path.GetFullPath( filename );

                return true;
            }

            return false;
        }

        private void PrepareCompile()
        {
            this.outputDockableWindow.Activate();

            if ( m_SavingAndCompiling == SavingAndCompiling.SaveAllBeforeCompile )
            {
                m_ScriptEditor.ExecuteAppAction( ScriptEditor.AppAction.FileSaveAll );
            }
            else if ( m_SavingAndCompiling == SavingAndCompiling.SaveBeforeCompile )
            {
                m_ScriptEditor.ExecuteAppAction( ScriptEditor.AppAction.FileSave );
            }

            this.ClearOutputTextBox();
            this.errorsListView.Items.Clear();

            this.statusToolStripStatusLabel.Text = "Compiling...";

            m_ScriptEditor.CurrentCompileState = ScriptEditor.CompileState.Compiling;
            m_ScriptEditor.PreCompile();

            RefreshFileMenuItems();
            RefreshCompilingMenuItems();

            m_sourceControl.ResetCompileAction();
        }

        private string BuildCompilerCommand(string filename, Platform platform)
        {
            return m_userEditorSettings.BuildCompileScriptCommand(filename, platform);
        }

        private string ResourceCompilerCommand(string filename, string outputFilename, Platform platform)
        {
            return m_userEditorSettings.ResourceCompileScriptCommand(filename, outputFilename, platform);
        }

        private void CompileComplete( int numErrors, string projectListFileName )
        {
            AddText(ErrorType.Info, $"---------------- [{DateTime.Now}] Compiled with {numErrors} error(s) ----------------");

            this.statusToolStripStatusLabel.Text = "Ready";

            // signals that we have just completed a project compile
            if ( numErrors == 0 && projectListFileName != null )
            {
                StartPostCompileCommand(projectListFileName);
            }
            else
            {
                PostBuildComplete();
            }
        }

        private void PostBuildComplete()
        {
            //  B*13564 - Always show errors window following compile, if there are any messages in the list
            if (this.errorsListView.Items.Count > 0)
            {
                if (!this.errorsDockableWindow.IsOpen)
                {
                    this.errorsDockableWindow.Activate();
                }
            }

            EndCompile();

            m_sourceControl.LogoffSourceControl();
        }

        private void CompileCancelled( int numErrors )
        {
            AddText(ErrorType.Warning, $"---------------- [{DateTime.Now}] Compile canceled with {numErrors} error(s) ----------------");

            this.statusToolStripStatusLabel.Text = "Ready";

            //  B*13564 - Always show errors window following compile, if there are any messages in the list
            if (this.errorsListView.Items.Count > 0)
            {
                if ( !this.errorsDockableWindow.IsOpen )
                {
                    this.errorsDockableWindow.Activate();
                }
            }

            EndCompile();

            m_sourceControl.LogoffSourceControl();
        }

        private void BreakOnCompileError( int numErrors )
        {
            AddText(ErrorType.Errors, $"---------------- [{DateTime.Now}] Stop compile.  {numErrors} error(s) found. ----------------");

            this.statusToolStripStatusLabel.Text = "Ready";

            //  B*13564 - Always show errors window following compile, if there are any messages in the list
            if (this.errorsListView.Items.Count > 0)
            {
                if ( !this.errorsDockableWindow.IsOpen )
                {
                    this.errorsDockableWindow.Activate();
                }
            }

            EndCompile();

            m_sourceControl.LogoffSourceControl();
        }

        private void CompilerFailure( string error )
        {
            if ( String.IsNullOrEmpty(error) == false )
                rageMessageBox.ShowError( this, error, "Compiler Failure" );

            this.statusToolStripStatusLabel.Text = "Ready";

            EndCompile();

            m_sourceControl.LogoffSourceControl();
        }

        private void CompileFilesNotFound( IEnumerable<string> filenames )
        {
            if (!filenames.Any())
            {
                return;
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("The following {0} file(s) were not found on disk during compile:\r\n", filenames.Count());
            foreach (String filename in filenames)
            {
                sb.AppendFormat("   {0}\r\n", filename);
                AddText(ErrorType.Errors, $"Error: '{filename}' was not found on disk during compile.");
            }
            rageMessageBox.ShowError( this, sb.ToString(), "File(s) Not Found" );
        }

        private void CompileFileUpToDate( string filename )
        {
            AddText(ErrorType.Info, $"'{filename}' is up to date.");
        }

        private void CompileListUpToDate( string projectListFileName )
        {
            this.statusToolStripStatusLabel.Text = "Ready";

            if (projectListFileName != null)
            {
                AddText(ErrorType.Info, $"---------------- [{DateTime.Now}] Project is up to date ----------------");

                // signals that we have just completed a project compile
                StartPostCompileCommand(projectListFileName);
            }
            else
            {
                PostBuildComplete();
            }
        }

        private void NothingToCompile()
        {
            AddText(ErrorType.Info, $"---------------- [{DateTime.Now}] Nothing to compile ----------------");

            this.statusToolStripStatusLabel.Text = "Ready";

            EndCompile();

            m_sourceControl.LogoffSourceControl();
        }


        private void EndCompile()
        {
            m_ScriptEditor.CurrentCompileState = ScriptEditor.CompileState.Stopped;

            m_ScriptEditor.PostCompile();

            RefreshFileMenuItems();
            RefreshCompilingMenuItems();
        }

        private int ProcessCompilerOutputString( string output )
        {
            int numErrors = 0;
            string filename = null;

            bool foundTraceback = false;

            this.errorsListView.BeginUpdate();

            string[] lines = output.Split( new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries );
            for ( int i = 0; i < lines.Length; ++i )
            {
                string line = lines[i];

                ErrorType type = ErrorType.Info;
                string errorFilename = null;
                int lineNumber = -1;
                string text = null;
                if ( m_compiler.ParseCompileOutputLine( line, ref type, ref errorFilename, ref lineNumber, ref text ) )
                {
                    if (type == ErrorType.Errors)
                    {
                        m_ScriptEditor.AddCompileErrorIndicator( errorFilename, lineNumber, Settings.Default.OpenFilesWithCompileErrors );
                        ++numErrors;
                    }

                    AddErrorsListViewItem(errorFilename, lineNumber, type, text);
                }
                else if ( line.StartsWith( "[traceback]" ) || line.Contains( "ExceptMain: Abnormal exit." ) )
                {
                    if ( !foundTraceback )
                    {
                        foundTraceback = line.StartsWith( "[traceback]" );
                        type = ErrorType.Errors;

                        AddErrorsListViewItem(filename, -1, type, "Script Compiler crash");
                        ++numErrors;
                    }
                }
                else
                {
                    m_compiler.ParseCompileFileLine( line, ref filename );
                }

                AddText(type, line);
            }

            if ( (numErrors == 0) && (filename != null) )
            {
                m_ScriptEditor.SetFileCompiled( filename.ToLower() );
                m_ScriptEditor.SetFileResourced(filename.ToLower(), m_userEditorSettings.CurrentPlatform);
            }

            this.errorsListView.EndUpdate();

            return numErrors;
        }

        private void AddErrorsListViewItem(string filename, int lineNumber, ErrorType errorType, string text)
        {
            CompileErrorInfo errorInfo = new CompileErrorInfo(filename.ToLower(), lineNumber);

            int imageIndex = (int)errorType;

            ListViewItem listViewItem = new ListViewItem(text, imageIndex);
            listViewItem.SubItems.Add(filename);
            listViewItem.SubItems.Add(String.Format("{0:n0}", lineNumber + 1));
            listViewItem.Tag = errorInfo;

            this.errorsListView.Items.Add(listViewItem);
        }

        /// <summary>

        /// <summary>
        /// Output handler for the post compile step.
        /// </summary>
        /// <param name="sendingProcess"></param>
        /// <param name="outLine"></param>
        private void PostCompileOutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    this.PostCompileOutputHandler(sendingProcess, outLine);
                });
                return;
            }

            // Collect the sort command output.
            if (!String.IsNullOrEmpty(outLine.Data))
            {
                AddText(ErrorType.Info, outLine.Data);
            }
        }

        private void PostCompileExitedHandler(object sendingProcess, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    this.PostCompileExitedHandler(sendingProcess, e);
                });
                return;
            }

            AddText(ErrorType.Info, "Post-Compile Event Complete.");
            if (m_startDebugAfterCompile)
            {
                ExecuteRunCommand();
            }

            PostBuildComplete();
        }

        /// <summary>
        /// Starts the process defined by CurrentCompilingSettings.PostCompileCommand, providing listname as the last parameter.
        /// Redirects standard output to our Output Window.
        /// </summary>
        /// <param name="listname"></param>
        private void StartPostCompileCommand( string listname )
        {
            if ( m_userEditorSettings.CurrentCompilingSettings.PostCompileCommand == "" )
            {
                return;
            }

            string commandLine = m_userEditorSettings.CurrentCompilingSettings.PostCompileCommand + " " + listname;
            IEnvironment env = ApplicationSettings.Environment;
            env.Push();
            try
            {
                this.m_userEditorSettings.CurrentCompilingSettings.Import(env, this.m_userEditorSettings.CurrentPlatform);
                commandLine = env.Subst(commandLine);
            }
            finally
            {
                env.Pop();
            }

            ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( commandLine );
            if ( psInfo != null )
            {
                psInfo.RedirectStandardOutput = true;
                psInfo.RedirectStandardError = true;
                psInfo.UseShellExecute = false;
                psInfo.CreateNoWindow = true;

                if (this.InvokeRequired == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        AddText(ErrorType.Info, "Performing Post-Compile Event...");
                    });
                }
                else
                {
                    AddText(ErrorType.Info, "Performing Post-Compile Event...");
                }

                Process p = null;
                try
                {
                    p = new Process();
                    p.StartInfo = psInfo;
                    p.EnableRaisingEvents = true;
                    p.OutputDataReceived += new DataReceivedEventHandler(PostCompileOutputHandler);
                    p.ErrorDataReceived += new DataReceivedEventHandler(PostCompileOutputHandler);
                    p.Exited += new EventHandler(PostCompileExitedHandler);
                    p.Start();
                    p.BeginOutputReadLine();
                    p.BeginErrorReadLine();
                }
                catch ( Exception exception )
                {
                    rageMessageBox.ShowError( this,
                        String.Format( "There was an error executing '{0}'\n{1}", m_userEditorSettings.CurrentCompilingSettings.PostCompileCommand, exception.ToString() ),
                        "Post Build Event Error" );
                }
            }
            else
            {
                rageMessageBox.ShowError( this, String.Format( "Unable to execute '{0}'.  Improperly formatted command.", commandLine ),
                    "Post-Compile Command Error" );
            }
        }

        /// <summary>
        /// Starts the process by RunCommand
        /// </summary>
        private void ExecuteRunCommand()
        {
            if ( m_userEditorSettings.Project.StartDebugCommand == "" )
            {
                return;
            }

            ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( m_userEditorSettings.Project.StartDebugCommand );
            if ( psInfo != null )
            {
                psInfo.UseShellExecute = false;

                Process p = null;
                try
                {
                    p = Process.Start( psInfo );
                }
                catch ( Exception exception )
                {
                    rageMessageBox.ShowError( this,
                        String.Format( "There was an error executing '{0}'\n{1}", m_userEditorSettings.Project.StartDebugCommand, exception.ToString() ),
                        "Run Error" );
                }
            }
            else
            {
                rageMessageBox.ShowError( this,
                    String.Format( "Unable to execute '{0}'.  Improperly formatted command.", m_userEditorSettings.Project.StartDebugCommand ),
                    "Post-Compile Command Error" );
            }
        }

        /// <summary>
        /// Converts a project file name to the compiled project files list name
        /// </summary>
        /// <param name="projectFile"></param>
        /// <returns></returns>
        private string GetCompiledProjectFilename( string projectFile )
        {
            string filename = projectFile.Replace( ".scproj", "" );
            filename += ".scolist";

            return filename;
        }
        #endregion

        #region Save/Load Layout
        /// <summary>
        /// Loads the layout and retrieves the SandDock layout string
        /// </summary>
        /// <returns>null on error</returns>
        private string LoadLayout()
        {
            // ensure the window handle has been created before setting the window position
            int handle = (int)this.Handle;

            // decide when to upgrade
            if ( Settings.Default.UpgradeNeeded == string.Empty )
            {
                Settings.Default.Upgrade();
            }

            // Form
            Point loc = Settings.Default.FormLocation;
            Size size = Settings.Default.FormSize;
            rageImage.AdjustForResolution( Settings.Default.ScreenSize, rageImage.GetScreenBounds(), ref loc, ref size );

            m_location = loc;
            this.Size = size;
            m_windowState = Settings.Default.FormWindowState;

            // TextEditorSettings
            System.Collections.Specialized.StringCollection textEditorSettings = Settings.Default.TextEditorSettings;
            if ( textEditorSettings != null )
            {
                m_textEditorSettings.Clear();

                foreach ( string serializedText in textEditorSettings )
                {
                    TextEditorSettings t = new TextEditorSettings();
                    if ( t.Deserialize( serializedText ) )
                    {
                        m_textEditorSettings.Add( t );
                    }
                }
            }

            // ErrorsWindowColumnWidths
            System.Collections.Specialized.StringCollection errorsWindowColumnWidths = Settings.Default.ErrorsWindowColumnWidths;
            if ( errorsWindowColumnWidths != null )
            {
                for ( int i = 0; i < errorsWindowColumnWidths.Count && i < this.errorsListView.Columns.Count; ++i )
                {
                    this.errorsListView.Columns[i].Width = int.Parse( errorsWindowColumnWidths[i] );
                }
            }

            // SavingAndCompiling
            string savingAndCompiling = Settings.Default.SavingAndCompiling;
            if ( !String.IsNullOrEmpty( savingAndCompiling ) )
            {
                try
                {
                    m_SavingAndCompiling = (SavingAndCompiling)Enum.Parse( typeof( SavingAndCompiling ), savingAndCompiling );
                }
                catch
                {

                }
            }

            // FindReplaceAll
            string findReplaceAllSerializedText = Settings.Default.FindReplaceAll;
            if ( !String.IsNullOrEmpty( findReplaceAllSerializedText ) )
            {
                FindReplaceAllSettings fSettings = new FindReplaceAllSettings();
                if ( fSettings.Deserialize( findReplaceAllSerializedText ) )
                {
                    m_useNewFindInFilesDialog = fSettings.UseNewDialog;

                    m_findReplaceInFilesForm.SetUIFromSettings( fSettings );
                    m_searchReplaceInFilesDialog.SetUIFromSettings( fSettings );
                }
            }

            // RichTextBoxFindReplace
            string richTextBoxFindReplaceSerializedText = Settings.Default.RichTextBoxFindReplace;
            if ( !String.IsNullOrEmpty( richTextBoxFindReplaceSerializedText ) )
            {
                rageRichTextBoxSearchReplaceSettings s = new rageRichTextBoxSearchReplaceSettings();
                if ( s.Deserialize( richTextBoxFindReplaceSerializedText ) )
                {
                    m_richTextBoxSearchReplaceDialog.SetUIFromSettings( s );
                }
            }

            // Layout
            string layout = Settings.Default.SandDockLayout;

            // Recent Files
            System.Collections.Specialized.StringCollection recentFiles = Settings.Default.RecentFiles;
            if ( recentFiles != null )
            {
                foreach ( string recentFile in recentFiles )
                {
                    AddRecentFile( recentFile );
                }
            }

            // Recent Projects
            System.Collections.Specialized.StringCollection recentProjects = Settings.Default.RecentProjects;
            if ( recentProjects != null )
            {
                foreach ( string recentProject in recentProjects )
                {
                    AddRecentProject( recentProject );
                }
            }

            // Break On Compile Error
            m_compiler.BreakOnCompileError = Settings.Default.BreakOnCompileError;

            // load the shared settings file
            string filename = ApplicationSettings.GetCommonSettingsFilename( false );
            if ( String.IsNullOrEmpty( filename ) )
            {
                return layout;
            }

            // see if it's the old layout file
            XmlTextReader reader = null;
            float version = 0.0f;
            try
            {
                reader = new XmlTextReader( filename );

                while ( reader.Read() )
                {
                    if ( reader.Name == "Version" )
                    {
                        if ( reader.IsStartElement() )
                        {
                            version = float.Parse( reader.ReadString() );
                        }

                        break;
                    }
                }

                reader.Close();
            }
            catch ( Exception e )
            {
                if ( reader != null )
                {
                    reader.Close();
                }

                rageMessageBox.ShowExclamation( this,
                    String.Format( "Could not parse layout file '{0}' to retrieve the version number.\n\n{1}\n\nThe application will continue but not all of your last session's settings can be restored.", filename, e.ToString() ),
                    "Problem Parsing Layout File!" );

                return layout;
            }

            if ( version == UserEditorSettings.SettingsVersion )
            {
                // if we got here, we have the latest version of the settings file
                string error = null;
                if ( !m_userEditorSettings.LoadFile( filename, out error ) )
                {
                    rageMessageBox.ShowExclamation( this,
                        String.Format( "{0}\n\nThe application will continue but not all of your last session's settings can be restored.", error ),
                        String.Format( "Unable to Load Settings File Version {0}!", UserEditorSettings.SettingsVersion ) );
                    return layout;
                }

                // <HACK>
                // ALupinacci 6/1/09 - Copy the PostCompileCommand from the ProjectSettings (which is
                // no longer used) to each of the CurrentCompilingSettings and then wipe the ProjectSetting's
                // copy of the PostCompileCommand.
                if ( !String.IsNullOrEmpty( m_userEditorSettings.Project.PostCompileCommand ) )
                {
                    foreach ( CompilingSettings compilingSettings in m_userEditorSettings.CurrentCompilingSettingsList )
                    {
                        compilingSettings.PostCompileCommand = m_userEditorSettings.Project.PostCompileCommand;
                    }

                    m_userEditorSettings.Project.PostCompileCommand = String.Empty;
                }
                // </HACK>
            }
            else
            {
                rageMessageBox.ShowExclamation( this,
                    String.Format( "Unable to load the settings from '{0}' version {1}.\n\nThe application will continue but not all of your last session's settings can be restored.", filename, version ),
                    "Unknown Settings File Version" );
            }

            return layout;
        }

		private bool SaveLayout()
		{
            // set this so we know when we can call Upgrade()
            Settings.Default.UpgradeNeeded = "no";

            // Form
            Settings.Default.ScreenSize = rageImage.GetScreenBounds().Size;
            Rectangle screenRect = new Rectangle( new Point( 0, 0 ), Settings.Default.ScreenSize );

            Point p = this.Location;
            Size s = this.Size;
            Rectangle windowRect = new Rectangle( p, s );

            if ( windowRect.IntersectsWith( screenRect ) )
            {
                Settings.Default.FormLocation = p;
                Settings.Default.FormSize = s;
            }
            else
            {
                Log.Warning( "The main window is off screen.  Storing the previous size and location." );
            }

            Settings.Default.FormWindowState = this.WindowState;

            // TextEditorSettings
            System.Collections.Specialized.StringCollection textEditorSettings = new System.Collections.Specialized.StringCollection();
            foreach ( KeyValuePair<TD.SandDock.DockControl, TabbedDocumentInfo> pair in m_dockControlToTabbedDocumentInfoMap )
            {
                TextEditorSettings t = CreateTextEditorSettings( pair.Value.Editor, pair.Key.Guid );
                if ( t != null )
                {
                    string serializedText = t.Serialize();
                    if ( serializedText != null )
                    {
                        textEditorSettings.Add( serializedText );
                    }
                }
            }

            Settings.Default.TextEditorSettings = textEditorSettings;

            // ErrorsWindowColumnWidths
            System.Collections.Specialized.StringCollection errorsWindowColumnWidths = new System.Collections.Specialized.StringCollection();
            for ( int i = 0; i < this.errorsListView.Columns.Count; ++i )
            {
                errorsWindowColumnWidths.Add( this.errorsListView.Columns[i].Width.ToString() );
            }

            // SavingAndCompiling
            Settings.Default.SavingAndCompiling = m_SavingAndCompiling.ToString();

            // FindReplaceAll
            FindReplaceAllSettings fSettings = new FindReplaceAllSettings();
            fSettings.UseNewDialog = m_useNewFindInFilesDialog;
            if ( m_useNewFindInFilesDialog )
            {
                m_searchReplaceInFilesDialog.SetSettingsFromUI( fSettings );
            }
            else
            {
                m_findReplaceInFilesForm.SetSettingsFromUI( fSettings );
            }
            Settings.Default.FindReplaceAll = fSettings.Serialize();

            // RichTextBoxFindReplace
            rageRichTextBoxSearchReplaceSettings rtbSettings = new rageRichTextBoxSearchReplaceSettings();
            m_richTextBoxSearchReplaceDialog.SetSettingsFromUI( rtbSettings );
            Settings.Default.RichTextBoxFindReplace = rtbSettings.Serialize();

            // LastProject loaded
            Settings.Default.LastProject = m_ProjectExplorer.ProjectFileName;

            // Layout
            Settings.Default.SandDockLayout = this.sandDockManager1.GetLayout();

            // Recent Files (collect in reverse order because of how AddRecentFile works)
            System.Collections.Specialized.StringCollection recentFiles = new System.Collections.Specialized.StringCollection();
            for ( int i = this.recentFilesToolStripMenuItem.DropDownItems.Count - 1; i >= 0; --i )
            {
                recentFiles.Add( this.recentFilesToolStripMenuItem.DropDownItems[i].Text );
            }
            Settings.Default.RecentFiles = recentFiles;

            // Recent Projects (collect in reverse order because of how AddRecentProject works)
            System.Collections.Specialized.StringCollection recentProjects = new System.Collections.Specialized.StringCollection();
            for ( int i = this.recentProjectsToolStripMenuItem.DropDownItems.Count - 1; i >= 0; --i )
            {
                recentProjects.Add( this.recentProjectsToolStripMenuItem.DropDownItems[i].Text );
            }
            Settings.Default.RecentProjects = recentProjects;

            // now save it
            int retryCount = 10;
            do
            {
                try
                {
                    Settings.Default.Save();
                    break;
                }
                catch
                {
                    --retryCount;
                    Thread.Sleep( 100 );
                }
            }
            while ( retryCount > 0 );

            // now save the common settings file
            m_userEditorSettings.Toolbar.ToolbarButtonSettings = m_editorToolbar.ButtonSettings;
            m_userEditorSettings.Toolbar.ToolStripLocations = m_editorToolbar.GetToolStripLocations();

            if ( m_userEditorSettings.FindReplaceSettings.UseNewDialog )
            {
                m_ScriptEditor.SearchReplaceDialog.SetSettingsFromUI( m_userEditorSettings.FindReplaceSettings );
            }
            else
            {
                m_ScriptEditor.FindReplaceForm.SetSettingsFromUI( m_userEditorSettings.FindReplaceSettings );
            }

            m_userEditorSettings.FindReplaceSettings.UseWordWrapping = this.findResultsRichTextBox.WordWrap;

            string filename = ApplicationSettings.GetCommonSettingsFilename( true );
            if ( filename == null )
            {
                return false;
            }

            if ( !ApplicationSettings.AdminMode )
            {
                // clean up old settings files, if any
                Log.Message( "Cleaning up old settings files." );

                List<string> filenames = ApplicationSettings.GetCommonSettingsFilenames( true );
                foreach ( string file in filenames )
                {
                    if ( file == filename )
                    {
                        continue;
                    }

                    try
                    {
                        if ( File.Exists( file ) )
                        {
                            // make writable
                            FileAttributes atts = File.GetAttributes( file );
                            if ( (atts & FileAttributes.ReadOnly) == FileAttributes.ReadOnly )
                            {
                                try
                                {
                                    File.SetAttributes( file, atts & (~FileAttributes.ReadOnly) );
                                }
                                catch
                                {
                                }
                            }

                            File.Delete( file );
                        }
                    }
                    catch (System.Exception e)
                    {
                        Log.ToolException(e, "Exception cleaning up settings files");
                    }
                }
            }

            string error = null;
            while ( true )
            {
                if ( !m_userEditorSettings.SaveFile( filename, out error ) )
                {
                    DialogResult result = rageMessageBox.ShowExclamation(
                        String.Format( "There was a problem saving the settings file '{0}'.\n\n{1}\n\nSelect 'Retry' to attempt to save it again, or 'Abort' to report the error.", filename, error ),
                        "Error Saving Settings File!", MessageBoxButtons.AbortRetryIgnore );
                    if ( result == DialogResult.Abort )
                    {
                        throw (new IOException( String.Format( "Could not save settings file '{0}':\n\n{1}", filename, error )) );
                    }
                    else if ( result == DialogResult.Ignore )
                    {
                        break;
                    }
                }
                else
                {
                    // verify the file was saved
                    UserEditorSettings tempUserSettings = new UserEditorSettings();
                    if ( !tempUserSettings.LoadFile( filename, out error ) )
                    {
                        DialogResult result = rageMessageBox.ShowExclamation(
                            String.Format( "Could not verify the contents of the settings file '{0}'.\n\n{1}\n\nSelect 'Retry' to attempt to save it again, or 'Abort' to report the error.", filename, error ),
                            "Unable to Verify Settings File!", MessageBoxButtons.AbortRetryIgnore );
                        if ( result == DialogResult.Abort )
                        {
                            throw (new IOException( String.Format( "The settings file '{0}' could not be verified.\n\n{1}", filename, error ) ));
                        }
                        else if ( result == DialogResult.Ignore )
                        {
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return true;
		}

        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        /// <param name="reader"></param>
        private void DeserializeWindow( XmlTextReader reader )
        {
            int left, top, width, height;

            left = Convert.ToInt32( reader["left"] );
            top = Convert.ToInt32( reader["top"] );
            width = Convert.ToInt32( reader["width"] );
            height = Convert.ToInt32( reader["height"] );

            // ensure the window handle has been created before setting the window position
            int handle = (int)this.Handle;

            this.SetBounds( left, top, width, height );

            string windowState = reader["windowstate"];
            if ( windowState != null )
            {
                if ( windowState == FormWindowState.Maximized.ToString() )
                {
                    this.WindowState = FormWindowState.Maximized;
                }
                else if ( windowState == FormWindowState.Minimized.ToString() )
                {
                    this.WindowState = FormWindowState.Minimized;
                }
                else
                {
                    this.WindowState = FormWindowState.Normal;
                }
            }
        }

        /// <summary>
        /// Kept for backwards compatibility
        /// </summary>
        /// <param name="reader"></param>
        private void DeserializeTextEditors( XmlTextReader reader )
        {
            string parentName = reader.Name;

            m_textEditorSettings.Clear();

            if ( reader.IsEmptyElement )
            {
                return;
            }

            while ( reader.Read() )
            {
                if ( !reader.IsStartElement() )
                {
                    if ( reader.Name == parentName )
                    {
                        break; // we're done
                    }
                }
                else if ( reader.Name == "TextWindow" )
                {
                    string guid = reader["guid"];
                    string filename = reader["filename"];
                    string linePosition = reader["lineposition"];
                    string horzSplit = reader["horzsplitpos"];
                    string vertSplit = reader["vertsplitpos"];

                    if ( (filename != null) && (guid != null) )
                    {
                        TextEditorSettings t = new TextEditorSettings( filename, new System.Guid( guid ) );

                        if ( horzSplit != null )
                        {
                            t.HorizontalSplitPosition = int.Parse( horzSplit );
                        }

                        if ( vertSplit != null )
                        {
                            t.VerticalSplitPosition = int.Parse( vertSplit );
                        }

                        t.LinePosition = Convert.ToInt32( linePosition );

                        m_textEditorSettings.Add( t );
                    }
                }
            }

            return;
        }
        #endregion

        #region SandDockManager Event Handlers
        private void sandDockManager1_ActiveTabbedDocumentChanged( object sender, EventArgs e )
        {
            // WARNING: don't use ActiveTabbedDocument if we enable floating tabbed documents
            m_ScriptEditor.SetCurrentEditorFromTabControl( this.sandDockManager1.ActiveTabbedDocument );

            if ( m_userEditorSettings.IntellisenseSettings.UpdateDocumentOutline && ( this.Handle != IntPtr.Zero )
                && !m_ScriptEditor.UpdateDocumentOutline( this.documentOutlineTreeView ) )
            {
                this.documentOutlineTreeView.Nodes.Clear();
            }

            m_editorToolbar.Editor = m_ScriptEditor.CurrentSyntaxEditor;

            RefreshCompilingMenuItems();
        }

        private class ActiveFileToolStripMenuItem : ToolStripMenuItem
        {
            public ActiveFileToolStripMenuItem( DockControl dockControl, string filename )
                : base( dockControl.TabText )
            {
                m_dockControl = dockControl;

                this.ToolTipText = filename;
            }

            #region Variables
            private DockControl m_dockControl;
            #endregion

            protected override void OnClick( EventArgs e )
            {
                m_dockControl.Activate();
            }
        }

        private void sandDockManager1_ShowActiveFilesList( object sender, ActiveFilesListEventArgs e )
        {
            this.activeFilesContextMenuStrip.Items.Clear();
            foreach ( DockControl dockControl in e.Windows )
            {
                string filename = string.Empty;
                TabbedDocumentInfo tdInfo;
                if ( m_dockControlToTabbedDocumentInfoMap.TryGetValue( dockControl, out tdInfo ) )
                {
                    filename = tdInfo.Editor.Document.Filename;
                }

                this.activeFilesContextMenuStrip.Items.Add( new ActiveFileToolStripMenuItem( dockControl, filename ) );
            }

            this.activeFilesContextMenuStrip.Show( e.Control, e.Position );
        }

        private void sandDockManager1_ShowControlContextMenu( object sender, ShowControlContextMenuEventArgs e )
        {
            if ( (e.Context == ContextMenuContext.RightClick) && (e.DockControl is TabbedDocument) )
            {
                m_SelectedDockControl = e.DockControl;

                TabbedDocumentInfo tdInfo;
                if ( m_dockControlToTabbedDocumentInfoMap.TryGetValue( e.DockControl, out tdInfo ) )
                {
                    this.fileNameToolStripMenuItem.Text = tdInfo.Editor.Document.Filename;
                }
                else
                {
                    this.fileNameToolStripMenuItem.Text = e.DockControl.TabText;
                }

                this.documentTabContextMenuStrip.Show( this, this.PointToClient( Control.MousePosition ) );
            }
        }
        #endregion

        #region Syntax Editor Event Handlers
        private void editor_DocumentTextChanged( object sender, DocumentModificationEventArgs e )
        {
            SyntaxEditor editor = sender as SyntaxEditor;
            if ( editor == null )
            {
                return;
            }

            this.DocumentModified(editor);
        }

        private void editor_DocumentModifiedChanged(object sender, EventArgs e)
        {
            SyntaxEditor editor = sender as SyntaxEditor;
            if (editor == null)
            {
                return;
            }

            this.DocumentModified(editor);
        }

        private void DocumentModified(SyntaxEditor editor)
        {
            TabbedDocumentInfo tdInfo;
            if (m_editorToTabbedDocumentInfoMap.TryGetValue(editor, out tdInfo))
            {
                if (!tdInfo.DockControl.TabText.EndsWith("*") && editor.Document.Modified)
                {
                    tdInfo.DockControl.TabText = tdInfo.DockControl.TabText + "*";

                    m_sourceControl.ResetEditAction();
                    m_sourceControl.MakeFileWritableOnEdit(editor.Document.Filename, false);
                    m_sourceControl.LogoffSourceControl();
                }
                else if (tdInfo.DockControl.TabText.EndsWith("*") && !editor.Document.Modified)
                {
                    tdInfo.DockControl.TabText = tdInfo.DockControl.TabText.TrimEnd('*');
                }
            }
        }

        private void editor_SelectionChanged( object sender, SelectionEventArgs e )
        {
            this.cursorPositionToolStripStatusLabel.Text = "Ln " + e.DisplayCaretDocumentPosition.Line
                + " Col " + e.DisplayCaretCharacterColumn;
        }

        private void editor_GotFocus( object sender, EventArgs e )
        {
            ActiproSoftware.SyntaxEditor.SyntaxEditor editor = sender as ActiproSoftware.SyntaxEditor.SyntaxEditor;
            if ( (editor != null) && (editor.SelectedView != null) && (editor.SelectedView.Selection != null) && (editor.Document != null) )
            {
                int character = editor.SelectedView.Selection.EndDocumentPosition.Character + 1;
                int line = editor.SelectedView.Selection.EndDocumentPosition.Line + 1;
                this.cursorPositionToolStripStatusLabel.Text = "Ln " + line + " Col " + character;
            }
        }

        private void editorDocument_SemanticParseDataChanged( object sender, EventArgs e )
        {
            if ( m_userEditorSettings.IntellisenseSettings.UpdateDocumentOutline && (m_ScriptEditor.CurrentSyntaxEditor != null)
                && (m_ScriptEditor.CurrentSyntaxEditor.Document == sender) )
            {
                if ( !m_ScriptEditor.UpdateDocumentOutline( this.documentOutlineTreeView ) )
                {
                    this.documentOutlineTreeView.Nodes.Clear();
                }
            }
        }
        #endregion

        #region Script Editor Event Handlers
        private void ScriptEditor_InfoTipText( string signature, string comment )
        {
            StringBuilder text = new StringBuilder();

            text.Append( @"{\rtf1\ansi " );

            if ( signature != null )
            {
                text.Append( signature.Replace( "\n", @"\line" ) );
            }

            if ( comment != null )
            {
                text.Append( @" \line\line " );
                text.Append( comment.Replace( "\n", @"\line" ) );
            }

            text.Append( "}" );

            this.helpRichTextBox.Rtf = text.ToString();
        }

        private ToolStripItem ScriptEditor_GetMenuItemByTag( ScriptEditor.AppAction appAction )
        {
            foreach ( ToolStripMenuItem menuItem in menuStrip1.Items )
            {
                ToolStripItem item = GetMenuItemByTag( appAction, menuItem );
                if ( item != null )
                {
                    return item;
                }
            }

            return null;
        }

        private ToolStripItem GetMenuItemByTag( ScriptEditor.AppAction appAction, ToolStripItem item )
        {
            if ( (item.Tag != null) && !(item.Tag is DockControl) && (item.Tag.ToString() != string.Empty) )
            {
                ScriptEditor.AppAction tagAction
                    = (ScriptEditor.AppAction)Enum.Parse( typeof( ScriptEditor.AppAction ), item.Tag.ToString() );
                if ( tagAction == appAction )
                {
                    return item;
                }
            }

            if ( item is ToolStripMenuItem )
            {
                ToolStripMenuItem menuItem = item as ToolStripMenuItem;

                foreach ( ToolStripItem dropDownItem in menuItem.DropDownItems )
                {
                    ToolStripItem foundItem = GetMenuItemByTag( appAction, dropDownItem );
                    if ( foundItem != null )
                    {
                        return foundItem;
                    }
                }
            }

            return null;
        }

        private ToolStripItem ScriptEditor_GetToolbarItemByTag( ScriptEditor.AppAction appAction )
        {
            List<ToolStrip> toolStrips = m_editorToolbar.ToolStrips;
            foreach ( ToolStrip toolStrip in toolStrips )
            {
                foreach ( ToolStripItem toolStripItem in toolStrip.Items )
                {
                    if ( (toolStripItem.Tag != null) && (toolStripItem.Tag is ToolbarButtonSetting) )
                    {
                        ToolbarButtonSetting bs = toolStripItem.Tag as ToolbarButtonSetting;
                        if ( !bs.IsCustomButton )
                        {
                            ScriptEditor.AppAction tagAction = (ScriptEditor.AppAction)Enum.Parse( typeof( ScriptEditor.AppAction ), bs.Tag );
                            if ( tagAction == appAction )
                            {
                                return toolStripItem;
                            }
                        }
                    }
                }
            }

            return null;
        }

        private void ScriptEditor_HighlightLanguage( object sender, SyntaxEditor editor )
        {
            // share some code by calling our other language highlighter
            StyleSettingsEventArgs e = new StyleSettingsEventArgs( editor, m_userEditorSettings.LanguageHighlightingStylesSettings.LanguageHighlightingStyles,
                m_userEditorSettings.LanguageHighlightingStylesSettings.Font, m_userEditorSettings.LanguageHighlightingStylesSettings.TextBackgroundColor );
            StyleEditorForm_ApplyStyles( sender, e );
        }

        private void ScriptEditor_FileClosing( object sender, FileClosingEventArgs e )
        {
            Debug.Assert( m_dockControlToTabbedDocumentInfoMap.ContainsKey( e.DockControl ),
                "m_MapDockControlToEditor.ContainsKey( " + e.DockControl.TabText + " )" );

            // remove the file system watcher early so it can't mess with any save-on-close operations
            TabbedDocumentInfo tdInfo;
            if ( m_dockControlToTabbedDocumentInfoMap.TryGetValue( e.DockControl, out tdInfo ) )
            {
                tdInfo.RemoveFileSystemWatcher();
            }

            tdInfo.IsClosing = true;

            if ( e.Editor.Document != null )
            {
                Log.Message( "FileClosing {0}", e.Editor.Document.Filename );
            }
        }

        private void ScriptEditor_FileClosed( object sender, FileClosedEventArgs e )
        {
            if ( !e.Cancelled )
            {
                m_dockControlToTabbedDocumentInfoMap.Remove( e.DockControl );
                m_editorToTabbedDocumentInfoMap.Remove( e.Editor );

                if ( e.Editor.Document != null )
                {
                    m_sourceControl.RemoveSkippedFile( e.Editor.Document.Filename );

                    if ( !ScriptEditor.IsANewFileName( e.Editor.Document.Filename ) )
                    {
                        AddRecentFile( e.Editor.Document.Filename );
                    }

                    Log.Message( "FileClosed {0}", e.Editor.Document.Filename);
                }
            }
            else
            {
                if ( !String.IsNullOrEmpty( e.Reason ) )
                {
                    rageMessageBox.ShowError( this, String.Format( "'{0}'\n\n{1}", e.Editor.Document.Filename, e.Reason ), "Close Error" );
                }

                // we cancelled it, so add the file system watcher back on
                TabbedDocumentInfo tdInfo;
                if ( m_dockControlToTabbedDocumentInfoMap.TryGetValue( e.DockControl, out tdInfo ) )
                {
                    tdInfo.AddFileSystemWatcher();
                    tdInfo.IsClosing = false;
                }
            }

            this.Refresh();
        }

        private void ScriptEditor_FileClosingAll( object sender, FileAllPreOperationEventArgs e )
        {
            foreach ( string file in e.Files )
            {
                m_ScriptEditor.ClearParseRequests( null, file );
            }
        }

        private void ScriptEditor_FileLoading( object sender, FileLoadingEventArgs e )
        {
            TabbedDocument tabDoc = new VisualStudioStyleTabbedDocument( this.sandDockManager1, e.Editor, e.Editor.Document.Filename );
            tabDoc.Guid = e.Guid;
            tabDoc.TabIndex = m_TabIndex++;
            tabDoc.PersistState = true;

            if ( e.IsNewFile )
            {
                tabDoc.TabText = e.Editor.Document.Filename;
            }
            else
            {
                tabDoc.TabText = Path.GetFileName( e.Editor.Document.Filename );
            }

            e.Editor.DocumentTextChanged += new DocumentModificationEventHandler( editor_DocumentTextChanged );
            e.Editor.DocumentModifiedChanged += new EventHandler(editor_DocumentModifiedChanged);
            e.Editor.SelectionChanged += new SelectionEventHandler( editor_SelectionChanged );
            e.Editor.GotFocus += new EventHandler( editor_GotFocus );
            e.Editor.Document.SemanticParseDataChanged += new EventHandler( editorDocument_SemanticParseDataChanged );

            e.DockControl = tabDoc;

            Log.Message( "FileLoading {0}", e.Editor.Document.Filename );

            this.statusToolStripStatusLabel.Text = "Loading " + e.Editor.Document.Filename;
            this.Refresh();
        }

        private void ScriptEditor_FileLoaded( object sender, FileLoadedEventArgs e )
        {
            if ( !e.Cancelled )
            {
                this.tabbedDocument1.Close();

                TabbedDocumentInfo tdInfo = new TabbedDocumentInfo( this, e.Editor, e.DockControl );
                tdInfo.AttributesChanged += new TabbedDocumentInfo.AttributesChangedEventHandler( TabbedDocumentInfo_AttributesChanged );

                if ( !m_dockControlToTabbedDocumentInfoMap.ContainsKey( e.DockControl ) )
                {
                    Log.Message( "FileLoaded {0}", e.Editor.Document.Filename );

                    m_dockControlToTabbedDocumentInfoMap.Add( e.DockControl, tdInfo );
                }

                if ( !m_editorToTabbedDocumentInfoMap.ContainsKey( e.Editor ) )
                {
                    m_editorToTabbedDocumentInfoMap.Add( e.Editor, tdInfo );
                }

                if ( e.IsNewFile )
                {
                    if ( !e.DockControl.TabText.EndsWith( "*" ) )
                    {
                        e.DockControl.TabText += "*";
                    }
                }
                else
                {
                    tdInfo.AddFileSystemWatcher();
                }

                // make sure our styles are available
                if ( !m_userEditorSettings.LanguageHighlightingStylesSettings.LanguageHighlightingStyles.ContainsKey( e.Editor.Document.Language.Key ) )
                {
                    LanguageHighlightingStyle langStyle = new LanguageHighlightingStyle();
                    langStyle.LanguageKey = e.Editor.Document.Language.Key;
                    foreach ( ActiproSoftware.SyntaxEditor.HighlightingStyle style in e.Editor.Document.Language.HighlightingStyles )
                    {
                        langStyle.HighlightingStyles.Add( style.Clone() );
                    }

                    m_userEditorSettings.LanguageHighlightingStylesSettings.LanguageHighlightingStyles.Add( langStyle.LanguageKey, langStyle );
                }

                RefreshCompilingMenuItems();
            }
            else
            {
                if ( e.Editor != null )
                {
                    RemoveRecentFile( e.Editor.Document.Filename );
                }

                rageMessageBox.ShowError( this, e.Reason, "Load Error" );
            }

            this.statusToolStripStatusLabel.Text = "Ready";
        }

        private void TabbedDocumentInfo_AttributesChanged( object sender, EventArgs e )
        {
            TabbedDocumentInfo tdInfo = sender as TabbedDocumentInfo;
            if ( (tdInfo != null) && !tdInfo.IsClosing )
            {
                try
                {
                    if ( (tdInfo.Editor.Document != null) && File.Exists( tdInfo.Editor.Document.Filename ) )
                    {
                        FileAttributes attr = File.GetAttributes( tdInfo.Editor.Document.Filename );
                        if ( (attr & FileAttributes.ReadOnly) == FileAttributes.ReadOnly )
                        {
                            tdInfo.DockControl.TabImage = Properties.Resources.locked.Clone() as Bitmap;
                        }
                        else if ( tdInfo.DockControl.TabImage != null )
                        {
                            tdInfo.DockControl.TabImage.Dispose();
                            tdInfo.DockControl.TabImage = null;
                        }
                    }
                }
                catch ( Exception ex )
                {
                    Log.ToolException(ex, "Exception handling TabbedDocumentInfo attribute change");
                }
            }
        }

        private void ScriptEditor_FileSaving( object sender, FileSavingEventArgs e )
        {
            if ( !e.IsSaveAll && !e.IsSaveAs )
            {
                m_sourceControl.ResetSaveAction();
            }

            this.statusToolStripStatusLabel.Text = "Saving " + e.Filename;
            this.Refresh();
            if ( !e.IsSaveAs )
            {
                // If editor is null, assume we are doing a replace all in files operation
                bool saveAll = e.Editor == null;
                if (!m_sourceControl.MakeFileWritableOnSave(e.Filename, saveAll))
                {
                    e.Cancel = true;
                }
            }

            Log.Message( "FileSaving {0}", e.Filename );
        }

        private void ScriptEditor_FileSaved( object sender, FileSavedEventArgs e )
        {
            if ( !e.IsSaveAll )
            {
                m_sourceControl.LogoffSourceControl();
            }

            if ( e.Cancelled )
            {
                rageMessageBox.ShowError( this, $"'{e.Filename}'\r\n\r\n{e.Reason}", "Save Error" );
            }
            else if ( e.Editor != null )
            {
                TabbedDocumentInfo tdInfo;
                if ( m_editorToTabbedDocumentInfoMap.TryGetValue( e.Editor, out tdInfo ) )
                {
                    tdInfo.DockControl.TabText = tdInfo.DockControl.TabText.Trim( new char[] { '*' } );
                }

                Log.Message( "FileSaved {0}", e.Filename );
            }

            this.statusToolStripStatusLabel.Text = "Ready";
        }

        private void ScriptEditor_FileSavingAll( object sender, FileAllPreOperationEventArgs e )
        {
            SaveProjectFile();

            m_sourceControl.ResetSaveAction();
        }

        private void ScriptEditor_FileSavedAll( object sender, FileAllPostOperationEventArgs e )
        {
            RefreshFileMenuItems();
            RefreshCompilingMenuItems();

            m_sourceControl.LogoffSourceControl();
        }
        #endregion

        #region StyleEditorForm Event Handlers
        private void StyleEditorForm_ApplyStyles( object sender, StyleSettingsEventArgs e )
        {
            e.Editor.Font = e.Font;

            // editor.BackColor = m_userEditorSettings.LanguageHighlightingStylesSettings.TextBackgroundColor;
            VisualStudio2005SyntaxEditorRenderer renderer = e.Editor.RendererResolved as VisualStudio2005SyntaxEditorRenderer;
            if ( renderer != null )
            {
                renderer.TextAreaBackgroundFill
                    = new ActiproSoftware.Drawing.SolidColorBackgroundFill( e.TextBackgroundColor );
                renderer.LineNumberMarginBackgroundFill
                    = new ActiproSoftware.Drawing.SolidColorBackgroundFill( e.TextBackgroundColor );
                renderer.SelectionMarginBackgroundFill
                    = new ActiproSoftware.Drawing.SolidColorBackgroundFill( e.TextBackgroundColor );
            }

            SyntaxLanguageCollection languages = SyntaxLanguage.GetLanguageCollection( e.Editor.Document.Language );

            if ( languages != null )
            {
                lock (e.Editor.Document.Language)
                {
                    // First determine if any extra styles need to be added to the current document's language
                    // Marking IsUpdating false is expensive if we have a lot of documents open, this avoids that annoying situation.
                    bool updateCurLang = false;
                    LanguageHighlightingStyle curLangStyle;
                    if (e.Styles.TryGetValue(e.Editor.Document.Language.Key, out curLangStyle))
                    {
                        foreach (HighlightingStyle style in curLangStyle.HighlightingStyles)
                        {
                            if (!e.Editor.Document.Language.HighlightingStyles.Contains(style.Key))
                            {
                                updateCurLang = true;
                                break;
                            }
                        }
                    }

                    if (updateCurLang)
                    {
                        e.Editor.Document.Language.IsUpdating = true;
                    }

                    // Loop through each language and update the highlighting styles
                    foreach (SyntaxLanguage language in languages)
                    {
                        LanguageHighlightingStyle langStyle;
                        if (e.Styles.TryGetValue(language.Key, out langStyle))
                        {
                            foreach (HighlightingStyle style in langStyle.HighlightingStyles)
                            {
                                language.HighlightingStyles.Add(style);
                            }
                        }
                    }

                    // Mark batch update complete
                    if (updateCurLang)
                    {
                        e.Editor.Document.Language.IsUpdating = false;
                    }
                }
            }
        }
        #endregion

        #region Tab Context Menu Event Handlers
        private void documentTabContextMenuStrip_Closed( object sender, ToolStripDropDownClosedEventArgs e )
        {
            if ( e.CloseReason != ToolStripDropDownCloseReason.ItemClicked )
            {
                m_SelectedDockControl = null;
            }
        }

        private void fileNameToolStripMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                if ( !String.IsNullOrEmpty( this.fileNameToolStripMenuItem.Text ) )
                {
                    Clipboard.SetText( this.fileNameToolStripMenuItem.Text );
                }
            }
            catch ( Exception ex )
            {
                Log.ToolException(ex, "Exception copying text from filename in context menu");
            }
        }

        private void openContainingFolderToolStripMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                if ( !String.IsNullOrEmpty( this.fileNameToolStripMenuItem.Text ) && File.Exists( this.fileNameToolStripMenuItem.Text ) )
                {
                    Process.Start( "explorer", String.Format( "/select,\"{0}\"", this.fileNameToolStripMenuItem.Text ) );
                }
            }
            catch ( Exception ex )
            {
                Log.ToolException(ex, "Exception opening containing folder");
            }
        }

        private void newTabToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_ScriptEditor.ExecuteAppAction( ScriptEditor.AppAction.FileNew );
            m_SelectedDockControl = null;
        }

        private void closeTabToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( m_SelectedDockControl != null )
            {
                m_SelectedDockControl.Close();
                m_SelectedDockControl = null;
            }
        }

        private void closeOtherTabsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            List<ActiproSoftware.SyntaxEditor.SyntaxEditor> editors = new List<SyntaxEditor>();
            foreach ( KeyValuePair<TD.SandDock.DockControl, TabbedDocumentInfo> pair in m_dockControlToTabbedDocumentInfoMap )
            {
                if ( pair.Key != m_SelectedDockControl )
                {
                    editors.Add( pair.Value.Editor );
                }
            }

            m_ScriptEditor.CloseFiles( editors );
            m_SelectedDockControl = null;
        }

        private void closeAllTabsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_ScriptEditor.ExecuteAppAction( ScriptEditor.AppAction.FileCloseAll );
            m_SelectedDockControl = null;
        }

        private void closeTabsToRightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_SelectedDockControl == null)
            {
                return;
            }

            List<SyntaxEditor> editors = new List<SyntaxEditor>();
            bool closeTab = false;
            foreach (DockControl control in m_SelectedDockControl.LayoutSystem.Controls)
            {
                TabbedDocumentInfo docInfo;
                if (closeTab && m_dockControlToTabbedDocumentInfoMap.TryGetValue(control, out docInfo))
                {
                    editors.Add(docInfo.Editor);
                }

                // Close all tabs after selected control.
                if (control == m_SelectedDockControl)
                {
                    closeTab = true;
                }
            }

            m_ScriptEditor.CloseFiles(editors);
        }
        #endregion

        #region Project Explorer Event Handlers
        void ProjectExplorer_ExecuteCompileAction( ProjectExplorer.ProjectCompileAction action, List<string> filenames, Platform platform )
        {
            switch ( action )
            {
                case ProjectExplorer.ProjectCompileAction.CompleFile:
                    m_compiler.CompileFile(filenames[0] as string, platform);
                    break;
                case ProjectExplorer.ProjectCompileAction.CompileFiles:
                    m_compiler.CompileProject(m_ProjectExplorer.ProjectFileName, filenames, platform);
                    break;
                case ProjectExplorer.ProjectCompileAction.RecompileFiles:
                    m_compiler.RecompileProject(m_ProjectExplorer.ProjectFileName, filenames, platform);
                    break;
            }
        }

        void ProjectExplorer_FilesAdded( object sender, ProjectFilesEventArgs e )
        {
            m_ScriptEditor.SetProjectParserProjectFilenames( m_ProjectExplorer.GetProjectFilenames() );

            m_ScriptEditor.ProjectFilesAdded( e.Filenames );

            //m_ScriptEditor.RefreshIntellisenseQuick();
        }

        void ProjectExplorer_FilesRemoved( object sender, ProjectFilesEventArgs e )
        {
            m_ScriptEditor.SetProjectParserProjectFilenames( m_ProjectExplorer.GetProjectFilenames() );

            m_ScriptEditor.ProjectFilesRemoved( e.Filenames );

            //m_ScriptEditor.RefreshIntellisenseQuick();
        }
        #endregion

        #region FindReplace Event Handlers
        private ICollection<int> m_findResultOffsets = new SortedSet<int>();

        private void FindReplace_FoundString( String filename, int lineNum, String line )
        {
            m_findResultOffsets.Add( this.findResultsRichTextBox.TextLength );

            this.findResultsRichTextBox.AppendText( String.Format( "{0}({1}): {2}\n", filename, lineNum, line ) );
        }

        private void FindReplaceInFiles_ConsoleOutput( String str, bool clear )
        {
            if ( clear )
            {
                // start error # from beginning if output is cleared:
                m_CurrentFindResultNumber = 0;
                m_findResultOffsets.Clear();

                this.findResultsRichTextBox.Text = "";
                this.gotoNextFindResultToolStripMenuItem.Enabled = false;
            }

            m_findResultOffsets.Add( this.findResultsRichTextBox.TextLength );

            this.findResultsRichTextBox.Text += str;
        }

        private void FindReplaceInFiles_OpenModifiedFilesAfterReplaceAll()
        {
            Dictionary<string, string> openedFiles = new Dictionary<string, string>();

            for ( int i = 1; i <= m_findResultOffsets.Count; ++i )
            {
                int startOffset = m_findResultOffsets.ElementAt(i-1);
                int endOffset = (i == m_findResultOffsets.Count) ? this.findResultsRichTextBox.TextLength : m_findResultOffsets.ElementAt(i);
                string line = this.findResultsRichTextBox.Text.Substring( startOffset, endOffset - startOffset );

                string filename = null;
                int lineNumber = -1;
                if ( ParseOutputLine( m_rExpFind, line, ref filename, ref lineNumber ) )
                {
                    if ( !openedFiles.ContainsKey( filename ) )
                    {
                        m_ScriptEditor.LoadFile( filename );

                        openedFiles[filename] = filename;
                    }
                }
            }
        }

        private bool FindReplace_ReplaceAllWarning()
        {
            if ( m_ScriptEditor.GetModifiedEditors().Count > 0 )
            {
                // warn user that this operation cannot be undone, and give them the option to save modified files
                DialogResult result = rageMessageBox.ShowQuestion( this,
                    "This operation cannot be undone and may cause you to lose unsaved changes.\n\nDo you wish save all files before proceeding?",
                    "Replace All in Files", MessageBoxButtons.YesNoCancel );

                return result != DialogResult.Cancel;
            }
            else
            {
                // warn user that this operation cannot be undone
                DialogResult result = rageMessageBox.ShowQuestion( this, "This operation cannot be undone.\n\nDo you wish to proceed?",
                    "Replace All in Files", MessageBoxButtons.YesNo );

                return result == DialogResult.Yes;
            }
        }

        private void FindReplace_SearchBegin( bool replace )
        {
            if ( replace )
            {
                this.statusToolStripStatusLabel.Text = "Replacing...";
            }
            else
            {
                this.statusToolStripStatusLabel.Text = "Searching...";
            }
        }

        private void FindReplace_SearchComplete( int totalFound, int matchingFiles, int totalFilesSearched,
            FindReplace.EFindNextReturnTypes err, string errorString1, string errorString2 )
        {
            this.statusToolStripStatusLabel.Text = "Ready";

            if ( totalFound > 0 )
            {
                this.gotoNextFindResultToolStripMenuItem.Enabled = true;
            }
        }

        private void SearchReplaceDialog_SearchBegin( object sender, SearchReplaceEventArgs e )
        {
            switch ( e.Search )
            {
                case SearchReplaceType.FindAllInFile:
                case SearchReplaceType.FindAllInFiles:
                case SearchReplaceType.FindInFile:
                case SearchReplaceType.FindInFiles:
                case SearchReplaceType.MarkAllInFile:
                case SearchReplaceType.MarkAllInFiles:
                    {
                        this.statusToolStripStatusLabel.Text = "Searching... 0% Complete";
                    }
                    break;
                default:
                    {
                        this.statusToolStripStatusLabel.Text = "Replacing... 0% Complete";
                    }
                    break;
            }
        }

        private void SearchReplaceDialog_SearchComplete( object sender, SearchCompleteEventArgs e )
        {
            this.statusToolStripStatusLabel.Text = "Ready";
        }

        private void searchReplaceInFilesDialog_CancelIntellisense( object sender, EventArgs e )
        {
            m_ScriptEditor.CancelActiveIntellisense();
        }

        private void searchReplaceInFilesDialog_FindCurrentFilename( object sender, SearchReplaceFilenameEventArgs e )
        {
            if ( m_ScriptEditor.CurrentSyntaxEditor != null )
            {
                e.Filename = m_ScriptEditor.CurrentSyntaxEditor.Document.Filename.ToLower();
            }
            else
            {
                e.Filename = null;
            }
        }

        private void searchReplaceInFilesDialog_FindOpenFilenames( object sender, SearchReplaceFilenamesEventArgs e )
        {
            e.Filenames = m_ScriptEditor.GetAllOpenTabFilenames();
        }

        private void searchReplaceInFilesDialog_FindProjectFilenames( object sender, SearchReplaceFilenamesEventArgs e )
        {
            e.Filenames = m_ScriptEditor.GetProjectAndOpenFilenames();
        }

        private void searchReplaceInFilesDialog_FindSyntaxEditor( object sender, SearchReplaceSyntaxEditorEventArgs e )
        {
            e.Editor = m_ScriptEditor.GetSyntaxEditor( e.Filename );
        }

        private void searchReplaceInFilesDialog_FoundString( object sender, SearchReplaceFoundStringEventArgs e )
        {
            bool search = (e.Search == SearchReplaceType.FindAllInFile) || (e.Search == SearchReplaceType.FindAllInFiles);
            bool replace = (e.Search == SearchReplaceType.ReplaceAllInFile) || (e.Search == SearchReplaceType.ReplaceAllInFiles);

            if ( search || replace )
            {
                m_findResultOffsets.Add( this.findResultsRichTextBox.TextLength );

                // add 1 to the line numbers because they are 0-based.
                // add 1 to column index because they are 0-based.
                this.findResultsRichTextBox.AppendText( String.Format( "{0}({1},{2}): {3}\n", e.Filename, e.Line + 1, e.Index + 1, e.Text ) );
            }
        }

        private void searchReplaceInFilesDialog_LoadFile( object sender, SearchReplaceLoadFileEventArgs e )
        {
            e.Success = m_ScriptEditor.LoadFile( e.Filename );
        }

        private void searchReplaceInFilesDialog_ReportProgress( object sender, SearchReplaceReportProgressEventArgs e )
        {
            bool search = (e.Search == SearchReplaceType.FindAllInFile) || (e.Search == SearchReplaceType.FindAllInFiles);
            bool replace = (e.Search == SearchReplaceType.ReplaceAllInFile) || (e.Search == SearchReplaceType.ReplaceAllInFiles);

            if ( search || replace )
            {
                if ( !String.IsNullOrEmpty( e.State ) )
                {
                    if ( e.ProgressPercentage < 0 )
                    {
                        this.statusToolStripStatusLabel.Text = String.Format( "{0}... {1}",
                            replace ? "Replacing" : "Searching", e.State );
                    }
                    else
                    {
                        this.statusToolStripStatusLabel.Text = String.Format( "{0}... {1} {2}% Complete",
                            replace ? "Replacing" : "Searching", e.State, e.ProgressPercentage );
                    }
                }
                else
                {
                    if ( e.ProgressPercentage < 0 )
                    {
                        this.statusToolStripStatusLabel.Text = replace ? "Replacing..." : "Searching...";
                    }
                    else
                    {
                        this.statusToolStripStatusLabel.Text = String.Format( "{0}... {1}% Complete",
                            replace ? "Replacing" : "Searching", e.ProgressPercentage );
                    }
                }
            }
        }

        private void searchReplaceInFilesDialog_SaveDocument( object sender, SearchReplaceSaveTextEventArgs e )
        {
            e.Success = m_ScriptEditor.SaveFile( e.Text, e.Filename );
        }

        private void searchReplaceInFilesDialog_SearchBegin( object sender, SearchReplaceEventArgs e )
        {
            bool search = (e.Search == SearchReplaceType.FindAllInFile) || (e.Search == SearchReplaceType.FindAllInFiles);
            bool replace = (e.Search == SearchReplaceType.ReplaceAllInFile) || (e.Search == SearchReplaceType.ReplaceAllInFiles);

            if ( search || replace )
            {
                m_CurrentFindResultNumber = 0;
                m_findResultOffsets.Clear();
                this.findResultsRichTextBox.Clear();
                this.gotoNextFindResultToolStripMenuItem.Enabled = false;

                this.statusToolStripStatusLabel.Text = replace ? "Replacing..." : "Searching...";

                m_findResultOffsets.Add( this.findResultsRichTextBox.TextLength );

                if ( replace )
                {
                    this.findResultsRichTextBox.AppendText( String.Format( "Replace All in Files \"{0}\" with \"{1}\", {2}, \"{3}\"\n",
                        m_searchReplaceInFilesDialog.FindWhat, m_searchReplaceInFilesDialog.ReplaceWith,
                        m_searchReplaceInFilesDialog.LookIn, m_searchReplaceInFilesDialog.FileType ) );
                }
                else
                {
                    this.findResultsRichTextBox.AppendText( String.Format( "Find All in Files \"{0}\", {1}, \"{2}\"\n",
                        m_searchReplaceInFilesDialog.FindWhat, m_searchReplaceInFilesDialog.LookIn,
                        m_searchReplaceInFilesDialog.FileType ) );
                }
            }
        }

        private void searchReplaceInFilesDialog_SearchComplete( object sender, SearchCompleteEventArgs e )
        {
            bool search = (e.Search == SearchReplaceType.FindAllInFile) || (e.Search == SearchReplaceType.FindAllInFiles);
            bool replace = (e.Search == SearchReplaceType.ReplaceAllInFile) || (e.Search == SearchReplaceType.ReplaceAllInFiles);

            if ( search || replace )
            {
                this.statusToolStripStatusLabel.Text = "Ready";

                m_findResultOffsets.Add( this.findResultsRichTextBox.TextLength );

                if ( (e.Result == SearchReplaceResult.Error) || (e.Result == SearchReplaceResult.Cancelled) )
                {
                    this.findResultsRichTextBox.AppendText( e.ResultMessage );
                }
                else
                {
                    this.findResultsRichTextBox.AppendText( String.Format(
                        "Total {0}: {1}    Matching files: {2}    Total files searched: {3}\n",
                        replace ? "replaced" : "found", e.NumFound, e.NumFilesFound, e.NumFilesSearched ) );
                }

                if ( e.NumFound > 0 )
                {
                    this.gotoNextFindResultToolStripMenuItem.Enabled = true;
                }
            }
        }

        private void richTextBoxSearchReplaceDialog_FindCurrentRichTextBox( object sender, rageSearchReplaceRichTextBoxEventArgs e )
        {
            e.RichTextBox = m_selectedRichTextBox;
        }
        #endregion

        #region Settings Form Event Handlers
        private void settingsForm_ClearRecentFilesList( object sender, EventArgs e )
        {
            this.recentFilesToolStripMenuItem.DropDownItems.Clear();
            this.recentFilesToolStripMenuItem.Enabled = false;
        }

        private void settingsForm_ClearRecentProjectsList( object sender, EventArgs e )
        {
            this.recentProjectsToolStripMenuItem.DropDownItems.Clear();
            this.recentProjectsToolStripMenuItem.Enabled = false;
        }

        private void settingsForm_LoadDefaultSettings( object sender, LanguageDefaultSettingsEventArgs e )
        {
            e.Success = m_ScriptEditor.LoadDllOptionsFile( this, e.Language );
        }

        private void settingsForm_SaveDefaultSettings( object sender, LanguageDefaultSettingsEventArgs e )
        {
            e.Success = m_ScriptEditor.SaveDllOptionsFile( this, e.Language );
        }

        private void settingsForm_SaveSettings( object sender, UserEditorSettingsEventArgs e )
        {
            bool dirtyProject = !m_userEditorSettings.Project.Equals( e.UserSettings.Project );
            bool dirtyFiles = !m_userEditorSettings.Files.Equals( e.UserSettings.Files );
            bool dirtyToolbar = !m_userEditorSettings.Toolbar.Equals( e.UserSettings.Toolbar );
            bool dirtyPlatforms = !m_userEditorSettings.PlatformSettings.Equals(e.UserSettings.PlatformSettings);

            bool dirtyCompiling = false;
            if ( m_userEditorSettings.CurrentCompilingSettingsList.Count != e.UserSettings.CurrentCompilingSettingsList.Count )
            {
                dirtyCompiling = true;
            }
            else
            {
                for ( int i = 0; i < m_userEditorSettings.CurrentCompilingSettingsList.Count; ++i )
                {
                    if ( !m_userEditorSettings.CurrentCompilingSettingsList[i].Equals( e.UserSettings.CurrentCompilingSettingsList[i] ) )
                    {
                        dirtyCompiling = true;
                        break;
                    }
                }
            }

            bool dirtyLanguage = !m_userEditorSettings.CurrentLanguageSettings.Equals( e.UserSettings.CurrentLanguageSettings );

            Debug.Assert( e.UserSettings.SourceControl2 != null, "e.UserSettings.SourceControl != null" );

            bool dirtySourceControl = !m_userEditorSettings.SourceControl2.Equals( e.UserSettings.SourceControl2 );

            Debug.Assert( m_userEditorSettings.CurrentCompilingSettings != null, "m_userEditorSettings.CurrentCompilingSettings != null" );

            string selectedConfiguration = m_userEditorSettings.CurrentCompilingSettings.ConfigurationName;

            m_userEditorSettings.Copy( e.UserSettings );

            // try to restore the same configuration we had selected before changing the settings
            for ( int i = 0; i < m_userEditorSettings.CurrentCompilingSettingsList.Count; ++i )
            {
                if ( m_userEditorSettings.CurrentCompilingSettingsList[i].ConfigurationName == selectedConfiguration )
                {
                    m_userEditorSettings.CurrentCompilingSettingsIndex = i;
                    break;
                }
            }

            SetToolbarCompilingConfigurations();

            if (dirtyPlatforms)
            {
                SetToolbarPlatforms();
            }

            if ( dirtyToolbar )
            {
                m_editorToolbar.ResetToolbarToDefaults( m_userEditorSettings.Toolbar.ToolbarButtonSettings, false );
            }

            if (m_ProjectExplorer.IsLoaded && (dirtyProject || dirtyCompiling || dirtySourceControl || dirtyPlatforms))
            {
                bool dirtyProjectProject = !m_userEditorSettings.Project.Equals( m_ProjectExplorer.ProjectEditorSetting.Project );

                Debug.Assert( m_ProjectExplorer.ProjectEditorSetting.CompilingSettingsList != null, "m_ProjectExplorer.ProjectEditorSetting.CompilingSettingsList != null" );

                bool dirtyCompilingProject = false;
                if ( m_userEditorSettings.CurrentCompilingSettingsList.Count != m_ProjectExplorer.ProjectEditorSetting.CompilingSettingsList.Count )
                {
                    dirtyCompilingProject = true;
                }
                else
                {
                    for ( int i = 0; i < m_userEditorSettings.CurrentCompilingSettingsList.Count; ++i )
                    {
                        if ( !m_userEditorSettings.CurrentCompilingSettingsList[i].Equals( m_ProjectExplorer.ProjectEditorSetting.CompilingSettingsList[i] ) )
                        {
                            dirtyCompilingProject = true;
                            break;
                        }
                    }
                }

                if (dirtyProjectProject || dirtyCompilingProject || dirtyPlatforms)
                {
                    m_ProjectExplorer.ProjectEditorSetting.Copy( m_userEditorSettings );
                    m_ProjectExplorer.IsModified = true;
                }
            }

            if ( dirtyFiles )
            {
                this.sandDockManager1.AllowMiddleButtonClosure = m_userEditorSettings.Files.MiddleClickClose;

                EnableSemanticParsingService( m_userEditorSettings.Files.ParsingServiceEnabled );
            }

            if ( dirtyCompiling )
            {
                EnableSemanticParsingService( (m_userEditorSettings.Project as NonProjectProjectSettings).ParsingServiceEnabled );

                foreach ( CompilingSettingsCollection collection in m_userEditorSettings.LanguageCompilingSettings.Values )
                {
                    m_ScriptEditor.SetParserPluginCompilingSettings( collection.CurrentSettings );
                }

                EnableSemanticParsingService( m_userEditorSettings.Files.ParsingServiceEnabled );

                // we could call RefreshIntellisenseQuick here, but we'll wait until the user has closed the settings form
            }

            if ( dirtyLanguage )
            {
                foreach ( LanguageSettings l in m_userEditorSettings.LanguageSettingsList )
                {
                    m_ScriptEditor.SetParserPluginLanguageSettings( l );
                }
            }

            if ( dirtySourceControl )
            {
                m_sourceControl.SettingsSaved();
            }

            // Keep this compiler setting up to date with the UI.
            m_compiler.BreakOnCompileError = Settings.Default.BreakOnCompileError;

            m_ScriptEditor.SetLineNumberMarginVisible( m_userEditorSettings.Files.ShowLineNumbers );

            this.sandDockManager1.DocumentOverflow
                = m_userEditorSettings.Files.DocumentOverflowScrollable ? DocumentOverflowMode.Scrollable : DocumentOverflowMode.Menu;
        }
        #endregion

        #region Regex Text Box Functions

        /// <summary>
        /// New function
        /// </summary>
        /// <param name="charIndex"></param>
        /// <param name="regex"></param>
        /// <param name="textBox"></param>
        /// <param name="error"></param>
        /// <param name="offsets"></param>
        /// <param name="parsedOffsetIndex"></param>
        /// <returns></returns>
        private bool SelectLineFromCharIndex( int charIndex, Regex regex, RichTextBox textBox, bool error, IEnumerable<int> offsets, ref int parsedOffsetIndex )
        {
            if (offsets.None())
            {
                return false;
            }

            // Find the index AFTER the one we're looking for, then subtract one.
            int offsetIndex = offsets.TakeWhile(o => o <= charIndex).Count() - 1;
            if (offsetIndex < 0)
            {
                return false;
            }

            // End offset is end of textbox if offsetIndex is the last index.
            int endOffset = Math.Min(offsetIndex < offsets.Count() - 1 ? offsets.ElementAt(offsetIndex + 1) : textBox.TextLength, textBox.TextLength);
            int startOffset = Math.Min(offsets.ElementAt(offsetIndex), endOffset);
            string result = textBox.Text.Substring( startOffset, endOffset - startOffset );

            string filename = null;
            int lineNumber = 0;
            if (!this.ParseOutputLine(regex, result, ref filename, ref lineNumber))
            {
                return false;
            }

            parsedOffsetIndex = offsetIndex;

            textBox.Select( startOffset, endOffset - startOffset );

            DockControl control = this.m_ScriptEditor.SelectLine( filename, lineNumber,
                error ? this.CollectCompileErrors( filename ) : null ) as DockControl;
            control?.Activate();

            return true;

        }
        #endregion

        #region Output Context Menu Event Handlers
        private void outputRichTextBox_MouseDoubleClick( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                int charIndex = this.outputRichTextBox.GetCharIndexFromPosition( new Point( e.X, e.Y ) );
                SelectLineFromCharIndex( charIndex, Compiler.CompileErrorRegex, this.outputRichTextBox,
                    true, m_compileResultOffsets, ref m_CurrentErrorNumber );
            }
        }

        private void outputRichTextBox_MouseEnter( object sender, EventArgs e )
        {
            m_selectedRichTextBox = this.outputRichTextBox;

            m_richTextBoxSearchReplaceDialog.Text = "Find in Output";
        }

        private void goToErrorOutputToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Point p = new Point( this.outputContextMenuStrip.Left, this.outputContextMenuStrip.Top );
            p = this.outputRichTextBox.PointToClient( p );

            int charIndex = this.outputRichTextBox.GetCharIndexFromPosition( p );
            SelectLineFromCharIndex( charIndex, Compiler.CompileErrorRegex, this.outputRichTextBox,
                true, m_compileResultOffsets, ref m_CurrentErrorNumber );
        }

        private void copyOutputToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.outputRichTextBox.SelectedText != "" )
            {
                Clipboard.SetDataObject( this.outputRichTextBox.SelectedText );
            }
        }

        private void selectAllOutputToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.outputRichTextBox.SelectAll();
        }

        private void clearAllOutputToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.ClearOutputTextBox();
        }

        private void findOutputToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_selectedRichTextBox = this.outputRichTextBox;

            ShowRichTextBoxSearchReplaceDialog();
        }

        private void searchAgainDownOutputToolStripMenuItem_Click( object sender, EventArgs e )
        {
            SearchAgainDownRichTextBoxSearchReplaceDialog();
        }

        private void searchAgainUpOutputToolStripMenuItem_Click( object sender, EventArgs e )
        {
            SearchAgainUpRichTextBoxSearchReplaceDialog();
        }

        private void searchHighlightedDownOutputToolStripMenuItem_Click( object sender, EventArgs e )
        {
            SearchHighlightedDownRichTextBoxSearchReplaceDialog();
        }

        private void searchHighlightedUpOutputToolStripMenuItem_Click( object sender, EventArgs e )
        {
            SearchHighlightedUpRichTextBoxSearchReplaceDialog();
        }
        #endregion

        #region Find Results Context Menu Event Handlers
        private void findResultsRichTextBox_MouseDoubleClick( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                int charIndex = this.findResultsRichTextBox.GetCharIndexFromPosition( new Point( e.X, e.Y ) );
                SelectLineFromCharIndex( charIndex, m_rExpFind, this.findResultsRichTextBox, false, m_findResultOffsets, ref m_CurrentFindResultNumber );
            }
        }

        private void findResultsRichTextBox_MouseEnter( object sender, EventArgs e )
        {
            m_selectedRichTextBox = this.findResultsRichTextBox;

            m_richTextBoxSearchReplaceDialog.Text = "Find in Find Results";
        }

        private void goToFindResultToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Point p = new Point( this.findResultsContextMenuStrip.Left, this.findResultsContextMenuStrip.Top );
            p = this.findResultsRichTextBox.PointToClient( p );

            int charIndex = this.findResultsRichTextBox.GetCharIndexFromPosition( p );
            SelectLineFromCharIndex( charIndex, m_rExpFind, this.findResultsRichTextBox, false, m_findResultOffsets, ref m_CurrentFindResultNumber );
        }

        private void copyfindResultsToolStripMenuItem1_Click( object sender, EventArgs e )
        {
            if ( this.findResultsRichTextBox.SelectedText != "" )
            {
                Clipboard.SetDataObject( this.findResultsRichTextBox.SelectedText );
            }
        }

        private void selectAllFindResultsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.findResultsRichTextBox.SelectAll();
        }

        private void clearAllFindResultsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_CurrentFindResultNumber = 0;
            this.m_findResultOffsets.Clear();
            this.findResultsRichTextBox.Clear();
            this.gotoNextFindResultToolStripMenuItem.Enabled = false;
        }

        private void findFindResultsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_selectedRichTextBox = this.findResultsRichTextBox;

            ShowRichTextBoxSearchReplaceDialog();
        }

        private void searchAgainDownFindResultsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            SearchAgainDownRichTextBoxSearchReplaceDialog();
        }

        private void searchAgainUpFindResultsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            SearchAgainUpRichTextBoxSearchReplaceDialog();
        }

        private void searchHighlightedDownFindResultsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            SearchHighlightedDownRichTextBoxSearchReplaceDialog();
        }

        private void searchHighlightedUpFindResultsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            SearchHighlightedUpRichTextBoxSearchReplaceDialog();
        }
        #endregion

        #region RichTextBox SearchReplace Helper Functions
        private void ShowRichTextBoxSearchReplaceDialog()
        {
            if ( m_selectedRichTextBox != null )
            {
                if ( !m_richTextBoxSearchReplaceDialog.LocationSet )
                {
                    ApplicationSettings.CenterLocation( this, m_richTextBoxSearchReplaceDialog );
                    m_richTextBoxSearchReplaceDialog.LocationSet = true;
                }

                if ( !m_richTextBoxSearchReplaceDialog.Visible )
                {
                    m_richTextBoxSearchReplaceDialog.Show( this );
                }

                m_richTextBoxSearchReplaceDialog.Focus();

                m_richTextBoxSearchReplaceDialog.InitSearchText( m_selectedRichTextBox );
            }
        }

        private void SearchAgainDownRichTextBoxSearchReplaceDialog()
        {
            if ( m_selectedRichTextBox != null )
            {
                m_richTextBoxSearchReplaceDialog.SearchAgainDown( m_selectedRichTextBox );
            }
        }

        private void SearchAgainUpRichTextBoxSearchReplaceDialog()
        {
            if ( m_selectedRichTextBox != null )
            {
                m_richTextBoxSearchReplaceDialog.SearchAgainUp( m_selectedRichTextBox );
            }
        }

        private void SearchHighlightedDownRichTextBoxSearchReplaceDialog()
        {
            if ( m_selectedRichTextBox != null )
            {
                m_richTextBoxSearchReplaceDialog.SearchHighlightedDown( m_selectedRichTextBox );
            }
        }

        private void SearchHighlightedUpRichTextBoxSearchReplaceDialog()
        {
            if ( m_selectedRichTextBox != null )
            {
                m_richTextBoxSearchReplaceDialog.SearchHighlightedUp( m_selectedRichTextBox );
            }
        }
        #endregion

        #region Errors Context Menu Event Handlers
        private void errorsListView_MouseDoubleClick( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                if ( this.errorsListView.SelectedItems.Count > 0 )
                {
                    SelectError( this.errorsListView.SelectedItems[0].Tag as CompileErrorInfo );
                }
            }
        }

        private void errorsListView_MouseClick( object sender, MouseEventArgs e )
        {
        }

        private void errorsContextMenuStrip_Opening( object sender, CancelEventArgs e )
        {
            Point p = new Point( this.errorsContextMenuStrip.Left, this.errorsContextMenuStrip.Top );
            p = this.errorsListView.PointToClient( p );

            ListViewItem item = this.errorsListView.GetItemAt( p.X, p.Y );

            this.copyErrorsToolStripMenuItem.Enabled = (this.errorsListView.SelectedItems.Count > 0);
            this.gotoErrorErrorsToolStripMenuItem.Enabled = (item != null) && (this.errorsListView.SelectedItems.Count > 0);

            if ( !this.copyErrorsToolStripMenuItem.Enabled && !this.gotoErrorErrorsToolStripMenuItem.Enabled )
            {
                e.Cancel = true;
            }
        }

        private void copyErrorsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.errorsListView.SelectedItems.Count > 0 )
            {
                StringBuilder copyString = new StringBuilder();
                for ( int i = 0; i < this.errorsListView.SelectedItems.Count; ++i )
                {
                    if ( i > 0 )
                    {
                        copyString.Append( '\n' );
                    }

                    for ( int j = 0; j < this.errorsListView.SelectedItems[i].SubItems.Count; ++j )
                    {
                        if ( j > 0 )
                        {
                            copyString.Append( '\t' );
                        }

                        copyString.Append( this.errorsListView.SelectedItems[i].SubItems[j].Text );
                    }
                }

                Clipboard.SetText( copyString.ToString(), TextDataFormat.Text );
            }
        }

        private void gotoErrorErrorsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Point p = new Point( this.errorsContextMenuStrip.Left, this.errorsContextMenuStrip.Top );
            p = this.errorsListView.PointToClient( p );

            ListViewItem item = this.errorsListView.GetItemAt( p.X, p.Y );
            if ( (item != null) && (this.errorsListView.SelectedItems.Count > 0) )
            {
                this.errorsListView.SelectedIndices.Clear();
                this.errorsListView.SelectedIndices.Add( this.errorsListView.Items.IndexOf( item ) );

                SelectError( item.Tag as CompileErrorInfo );
            }
        }

        private void selectAllErrorsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.errorsListView.SelectedIndices.Clear();
            for ( int i = 0; i < this.errorsListView.Items.Count; ++i )
            {
                this.errorsListView.SelectedIndices.Add( i );
            }
        }

        private void SelectError( CompileErrorInfo errorInfo )
        {
            TD.SandDock.DockControl control = m_ScriptEditor.SelectLine( errorInfo.FileName, errorInfo.LineNumber, CollectCompileErrors( errorInfo.FileName ) ) as TD.SandDock.DockControl;
            if ( control != null )
            {
                control.Activate();
            }
        }

        private List<CompileErrorInfo> CollectCompileErrors( string filename )
        {
            List<CompileErrorInfo> errors = new List<CompileErrorInfo>();
            foreach ( ListViewItem item in this.errorsListView.Items )
            {
                CompileErrorInfo info = item.Tag as CompileErrorInfo;
                if ( info.FileName == filename.ToLower() )
                {
                    errors.Add( info );
                }
            }

            return errors;
        }
        #endregion

        #region Save Form Event Handlers
        public void SaveForm_SaveAllFiles( object sender, List<string> filenames )
        {
            if ( m_ProjectExplorer.IsModified )
            {
                int index = filenames.IndexOf( m_ProjectExplorer.ProjectFileName );
                if ( index > -1 )
                {
                    filenames.RemoveAt( index );
                }

                SaveProjectFile();
            }

            if ( filenames.Count > 0 )
            {
                m_ScriptEditor.SaveFiles( filenames );
            }
        }
        #endregion

        #region Editor Toolbar Event Handlers
        private void editorToolbar_ButtonClick( object sender, ToolStripItemClickedEventArgs e )
        {
            ToolbarButtonSetting bs = e.ClickedItem.Tag as ToolbarButtonSetting;
            if ( bs != null )
            {
                ScriptEditor.AppAction action = (ScriptEditor.AppAction)Enum.Parse( typeof( ScriptEditor.AppAction ), bs.Tag );

                if (action == ScriptEditor.AppAction.IncredibuildBuild)
                {
                    buildProjectToolStripMenuItem_Click(this, EventArgs.Empty);
                }
                else if (action == ScriptEditor.AppAction.IncredibuildRebuild)
                {
                    rebuildProjectToolStripMenuItem_Click(this, EventArgs.Empty);
                }
                else if (action == ScriptEditor.AppAction.IncredibuildClean)
                {
                    cleanProjectToolStripMenuItem_Click(this, EventArgs.Empty);
                }
                else if (action == ScriptEditor.AppAction.IncredibuildStop)
                {
                    stopBuildToolStripMenuItem_Click(this, EventArgs.Empty);
                }
                else if (action == ScriptEditor.AppAction.FileSaveProject)
                {
                    saveProjectToolStripMenuItem_Click(this, EventArgs.Empty);
                }
                else if (action == ScriptEditor.AppAction.FileOpenProject)
                {
                    openProjectToolStripMenuItem_Click(this, EventArgs.Empty);
                }
                else if (action == ScriptEditor.AppAction.FileCloseProject)
                {
                    closeProjectToolStripMenuItem_Click(this, EventArgs.Empty);
                }
                else
                {
                    m_ScriptEditor.ExecuteAppAction(action);
                }
            }
        }

        private void editorToolbar_DebuggerOptionsDropDownMenuOpening( object sender, DebuggerOptionsEventArgs e )
        {
            if ( m_ScriptEditor.CurrentSyntaxEditor != null )
            {
                e.DropDownButton.DropDownItems[(int)EditorToolbar.DebuggerOptions.StartAutomaticOutlining].Enabled
                    = (m_ScriptEditor.CurrentSyntaxEditor.Document.Outlining.Mode != OutliningMode.Automatic);
                e.DropDownButton.DropDownItems[(int)EditorToolbar.DebuggerOptions.StartManualOutlining].Enabled
                    = (m_ScriptEditor.CurrentSyntaxEditor.Document.Outlining.Mode != OutliningMode.Manual);
                e.DropDownButton.DropDownItems[(int)EditorToolbar.DebuggerOptions.StopOutlining].Enabled
                    = (m_ScriptEditor.CurrentSyntaxEditor.Document.Outlining.Mode != OutliningMode.None);
            }
            else
            {
                e.DropDownButton.DropDownItems[(int)EditorToolbar.DebuggerOptions.StartAutomaticOutlining].Enabled = false;
                e.DropDownButton.DropDownItems[(int)EditorToolbar.DebuggerOptions.StartManualOutlining].Enabled = false;
                e.DropDownButton.DropDownItems[(int)EditorToolbar.DebuggerOptions.StopOutlining].Enabled = false;
            }
        }

        private void editorToolbar_ShowLineNumbersChanged( object sender, ShowLineNumbersChangedEventArgs e )
        {
            m_userEditorSettings.Files.ShowLineNumbers = e.ShowLineNumbers;
            this.showLineNumbersToolStripMenuItem.Checked = e.ShowLineNumbers;
            m_ScriptEditor.SetLineNumberMarginVisible( e.ShowLineNumbers );
        }

        private void editorToolbar_HighlightEnumsChanged( object sender, HighlightEnumsChangedEventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.HighlightEnums = e.HighlightEnums;
            this.highlightEnumsToolStripMenuItem.Checked = e.HighlightEnums;
            m_ScriptEditor.SetIntellisenseOptions( reparse: true );
        }

        private void editorToolbar_HighlightQuickInfoPopupsChanged( object sender, HighlightQuickInfoPopupsChangedEventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.HighlightQuickInfoPopups = e.HighlightQuickInfoPopups;
            this.highlightQuickInfoPopupsToolStripMenuItem.Checked = e.HighlightQuickInfoPopups;

            foreach ( SyntaxEditor editor in m_editorToTabbedDocumentInfoMap.Keys )
            {
                editor.IntelliPrompt.QuickInfo.CollapsedOutliningNodeSyntaxHighlightingEnabled = e.HighlightQuickInfoPopups;
            }

            m_ScriptEditor.SetIntellisenseOptions( reparse: false );
        }

        private void editorToolbar_HighlightDisabledCodeBlocksChanged( object sender, HighlightDisabledCodeBlocksChangedEventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.HighlightDisabledCodeBlocks = e.HighlightDisabledCodeBlocks;
            this.highlightDisabledCodeBlocksToolStripMenuItem.Checked = e.HighlightDisabledCodeBlocks;

            m_ScriptEditor.SetIntellisenseOptions( reparse: true );
        }

        private void editorToolbar_CompilingConfigurationChanged( object sender, CompilingConfigurationChangedEventArgs e )
        {
            if ( m_userEditorSettings.CurrentCompilingSettings.ConfigurationName != e.ConfigurationName )
            {
                for ( int i = 0; i < m_userEditorSettings.CurrentCompilingSettingsList.Count; ++i )
                {
                    if ( m_userEditorSettings.CurrentCompilingSettingsList[i].ConfigurationName == e.ConfigurationName )
                    {
                        m_userEditorSettings.CurrentCompilingSettingsIndex = i;
                        break;
                    }
                }
            }

            // keep project in sync with user settings
            if ( m_ProjectExplorer.IsLoaded )
            {
                if ( m_ProjectExplorer.ProjectEditorSetting.CurrentCompilingSettings.ConfigurationName != e.ConfigurationName )
                {
                    for ( int i = 0; i < m_ProjectExplorer.ProjectEditorSetting.CompilingSettingsList.Count; ++i )
                    {
                        if ( m_ProjectExplorer.ProjectEditorSetting.CompilingSettingsList[i].ConfigurationName == e.ConfigurationName )
                        {
                            m_ProjectExplorer.ProjectEditorSetting.CurrentCompilingSettingsIndex = i;
                            break;
                        }
                    }
                }
            }

            // in case IncludeFile and/or IncludePaths changed
            m_ScriptEditor.SetParserPluginCompilingSettings( m_userEditorSettings.CurrentCompilingSettings );

            EnableSemanticParsingService(m_userEditorSettings.Files.ParsingServiceEnabled);
        }

        private void editorToolbar_PlatformChanged(object sender, PlatformChangedEventArgs e)
        {
            if (m_userEditorSettings.CurrentPlatform != e.Platform)
            {
                m_userEditorSettings.CurrentPlatform = e.Platform;
            }

            if (m_ProjectExplorer.IsLoaded &&
                m_ProjectExplorer.UserProjectSettings.SelectedPlatform != e.Platform)
            {
                m_ProjectExplorer.UserProjectSettings.SelectedPlatform = e.Platform;
            }
        }
        #endregion

        #region CacheSplashForm Event Handlers
        private void splashForm_Start( object sender, EventArgs e )
        {
            m_ProjectExplorer.LockProjectFile = true;

            EnableSemanticParsingService( (m_userEditorSettings.Project as NonProjectProjectSettings).ParsingServiceEnabled );

            m_ScriptEditor.ProjectLoaded( m_ProjectExplorer.GetProjectFilenames() );
        }

        private void splashForm_Stop( object sender, EventArgs e )
        {
            m_ProjectExplorer.LockProjectFile = false;

            EnableSemanticParsingService( m_userEditorSettings.Files.ParsingServiceEnabled );
        }
        #endregion

        #region Document Outline
        private void documentOutlineTreeView_DoubleClick( object sender, EventArgs e )
        {
            if ( (documentOutlineTreeView.SelectedNode != null) && (m_ScriptEditor.CurrentSyntaxEditor != null) )
            {
                documentOutlineTreeView.SelectedNode.Expand();
                IAstNode node = (IAstNode)documentOutlineTreeView.SelectedNode.Tag;
                m_ScriptEditor.CurrentSyntaxEditor.Caret.Offset = node.NavigationOffset;
                m_ScriptEditor.CurrentSyntaxEditor.Focus();
            }
        }

        private void updateDocumentOutlineCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            m_userEditorSettings.IntellisenseSettings.UpdateDocumentOutline = this.updateDocumentOutlineCheckBox.Checked;

            if ( m_userEditorSettings.IntellisenseSettings.UpdateDocumentOutline )
            {
                if ( (this.Handle != IntPtr.Zero) && !m_ScriptEditor.UpdateDocumentOutline( this.documentOutlineTreeView ) )
                {
                    this.documentOutlineTreeView.Nodes.Clear();
                }
            }
            else
            {
                this.documentOutlineTreeView.Nodes.Clear();
            }
        }

        private void documentOutlineContextMenuStrip_Opening( object sender, CancelEventArgs e )
        {
            e.Cancel = !this.updateDocumentOutlineCheckBox.Checked;
        }

        private void documentOutlineCollapseAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.documentOutlineTreeView.CollapseAll();
        }

        private void documentOutlineExpandAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.documentOutlineTreeView.ExpandAll();
        }
        #endregion

        #region Source Control Event Handlers
        private SourceControl.Action SourceControl_ShowPromptDialog( string filename, SourceControl.PromptType promptType )
        {
            return SourceControlForm.ShowIt( this, filename, promptType );
        }

        private bool SourceControl_PreSync( string filename, SyncFileDialog.SyncFileAction action )
        {
            switch ( action )
            {
                case SyncFileDialog.SyncFileAction.CancelCheckOut:
                    return false;

                case SyncFileDialog.SyncFileAction.DiscardSyncThenCheckOut:
                    m_ScriptEditor.DisableFileWatcher( filename );
                    break;

                case SyncFileDialog.SyncFileAction.DoNotSyncBeforeCheckOut:
                    return false;

                case SyncFileDialog.SyncFileAction.SaveAsSyncThenCheckOut:
                    if ( !m_ScriptEditor.SaveFileAs( filename, false ) )
                    {
                        return false;
                    }

                    m_ScriptEditor.DisableFileWatcher( filename );
                    break;
            }

            return true;
        }

        private bool SourceControl_PostSync( string filename, SyncFileDialog.SyncFileAction action )
        {
            switch ( action )
            {
                case SyncFileDialog.SyncFileAction.CancelCheckOut:
                    break;

                case SyncFileDialog.SyncFileAction.DiscardSyncThenCheckOut:
                    m_ScriptEditor.EnableFileWatcher( filename );
                    return m_ScriptEditor.ReloadFile( filename, false );

                case SyncFileDialog.SyncFileAction.DoNotSyncBeforeCheckOut:
                    break;

                case SyncFileDialog.SyncFileAction.SaveAsSyncThenCheckOut:
                    m_ScriptEditor.EnableFileWatcher( filename );
                    return m_ScriptEditor.ReloadFile( filename, false );
            }

            return true;
        }
        #endregion

        private void buildProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_ScriptEditor.IsProjectCompilable() && IncrediBuild.IsInstalled && !IncrediBuild.IsRunning)
            {
                if (m_SavingAndCompiling == SavingAndCompiling.SaveAllBeforeCompile)
                {
                    m_ScriptEditor.ExecuteAppAction(ScriptEditor.AppAction.FileSaveAll);
                }
                else if (m_SavingAndCompiling == SavingAndCompiling.SaveBeforeCompile)
                {
                    m_ScriptEditor.ExecuteAppAction(ScriptEditor.AppAction.FileSave);
                }

                IncrediBuild.NotifyUIIncredibuildStateChangeDelegate = OnNotifyIncredibuildState;
                IncrediBuild.NotifyUIIncredibuildCompletionDelegate = OnNotifyIncredibuildComplete;
                IncrediBuild.BuildOutput = OnIncredibuildOutputReceived;
                IncrediBuild.BuildErrors = OnIncredibuildErrorsReceived;

                IncrediBuild.CreateBuildConfiguration(new ScriptEditorProjectData(m_ProjectExplorer, m_ScriptEditor));
                IncrediBuild.RunBuildConfiguration("Build");
            }
        }

        private void rebuildProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_ScriptEditor.IsProjectCompilable() && IncrediBuild.IsInstalled && !IncrediBuild.IsRunning)
            {
                if (m_SavingAndCompiling == SavingAndCompiling.SaveAllBeforeCompile)
                {
                    m_ScriptEditor.ExecuteAppAction(ScriptEditor.AppAction.FileSaveAll);
                }
                else if (m_SavingAndCompiling == SavingAndCompiling.SaveBeforeCompile)
                {
                    m_ScriptEditor.ExecuteAppAction(ScriptEditor.AppAction.FileSave);
                }

                IncrediBuild.NotifyUIIncredibuildStateChangeDelegate = OnNotifyIncredibuildState;
                IncrediBuild.NotifyUIIncredibuildCompletionDelegate = OnNotifyIncredibuildComplete;
                IncrediBuild.BuildOutput = OnIncredibuildOutputReceived;
                IncrediBuild.BuildErrors = OnIncredibuildErrorsReceived;

                clearAllOutputToolStripMenuItem_Click(null, new EventArgs());

                IncrediBuild.CreateBuildConfiguration(new ScriptEditorProjectData( m_ProjectExplorer, null));
                IncrediBuild.RunBuildConfiguration("Rebuild");
            }
        }

        private void cleanProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_ScriptEditor.IsProjectCompilable() && IncrediBuild.IsInstalled && !IncrediBuild.IsRunning)
            {
                IncrediBuild.NotifyUIIncredibuildStateChangeDelegate = OnNotifyIncredibuildState;
                IncrediBuild.NotifyUIIncredibuildCompletionDelegate = null;
                IncrediBuild.BuildOutput = null;
                IncrediBuild.BuildErrors = null;

                IncrediBuild.CreateBuildConfiguration(new ScriptEditorProjectData(m_ProjectExplorer, null));
                IncrediBuild.RunBuildConfiguration("Clean");
            }
        }

        private void stopBuildToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IncrediBuild.IsInstalled && IncrediBuild.IsRunning)
            {
                IncrediBuild.CreateBuildConfiguration(new ScriptEditorProjectData(m_ProjectExplorer, null));
                IncrediBuild.RunBuildConfiguration("Stop");
                OnNotifyIncredibuildState(false);
            }
        }


        public delegate void OnNotifyUIDelegate(bool running);
        public delegate void OnNotifyUICompletionDelegate(bool success);
        public static OnNotifyUIDelegate InvokeNotifyUI;
        public static OnNotifyUICompletionDelegate InvokeNotifyUISuccess;
        public static OnLogMessageReceivedDelegate InvokeDataReceived;

        private void OnNotifyIncredibuildState(bool running)
        {
            if (this.InvokeRequired)
            {
                object[] args = new object[1];
                args[0] = running;

                InvokeNotifyUI = OnNotifyIncredibuildState;

                this.Invoke(InvokeNotifyUI, args);
            }
            else
            {
                if (running)
                {
                    if (m_ScriptEditor.CurrentCompileState != ScriptEditor.CompileState.BuildingIncredibuild)
                    {
                        m_ScriptEditor.CurrentCompileState = ScriptEditor.CompileState.BuildingIncredibuild;
                        this.statusToolStripStatusLabel.Text = "Building scripts...";

                        this.ClearOutputTextBox();

                        // If we have a lot of errors they refresh this listview too much. Just stop drawing it until the compile finishes.
                        //
                        // Viv: Edge case--if the dockable window was not created yet (user hasn't opened the tab yet) then BeginUpdate will have no effect!
                        // So only call BeginUpdate if the dockable window was created. We also hook to the creation event and call BeginUpdate there, if
                        // compiling is happening during that time. Very ugly, but it's hard to think of a better solution.
                        if (this.errorsDockableWindow.Created)
                        {
                            this.errorsListView.BeginUpdate();
                        }
                        this.errorsListView.Items.Clear();
                    }
                }
                else
                {
                    if (m_ScriptEditor.CurrentCompileState != ScriptEditor.CompileState.Stopped)
                    {
                        m_ScriptEditor.CurrentCompileState = ScriptEditor.CompileState.Stopped;

                        if (this.errorsDockableWindow.Created)
                        {
                            this.errorsListView.EndUpdate();
                        }
                    }
                }

                RefreshCompilingMenuItems();
                RefreshFileMenuItems();
            }
        }

        private void OnNotifyIncredibuildComplete(bool success)
        {
            if (this.InvokeRequired)
            {
                InvokeNotifyUISuccess = OnNotifyIncredibuildComplete;

                this.Invoke(InvokeNotifyUISuccess, success);
            }
            else
            {
                String status = String.Format("Build completed {0}", success ? "successfully" : "with errors");

                this.statusToolStripStatusLabel.Text = status;

                ErrorType errorType = success ? ErrorType.Info : ErrorType.Errors;
                AddText(errorType, $"----- [{DateTime.Now}] {status} -----");

                if (this.errorsListView.Items.Count > 0)
                {
                    if (!this.errorsDockableWindow.IsOpen)
                    {
                        this.errorsDockableWindow.Activate();
                    }
                }
            }
        }

        /// <summary>
        /// Callback for receiving log from the IB build.
        /// </summary>
        /// <param name="message"></param>
        private void OnIncredibuildOutputReceived(string message)
        {
            if (this.InvokeRequired)
            {
                InvokeDataReceived = OnIncredibuildOutputReceived;
                this.Invoke(InvokeDataReceived, message);
            }
            else
            {
                ErrorType type = ErrorType.Info;
                string errorFilename = string.Empty;
                int lineNumber = -1;
                string text = string.Empty;
                // Parse the output to get script compiler Info: and Warning: blocks.
                if (m_compiler.ParseCompileOutputLine(message, ref type, ref errorFilename, ref lineNumber, ref text))
                {
                    // Generally this wouldn't happen, but just in case.
                    if (type == ErrorType.Errors)
                    {
                        m_ScriptEditor.AddCompileErrorIndicator(errorFilename, lineNumber, Settings.Default.OpenFilesWithCompileErrors);
                    }

                    AddErrorsListViewItem(errorFilename, lineNumber, type, text);
                }

                AddText(type, message);
            }
        }

        /// <summary>
        /// Callback for receiving stderr data from the IB build.
        /// </summary>
        /// <param name="message"></param>
        private void OnIncredibuildErrorsReceived(String message)
        {
            if (this.InvokeRequired)
            {
                InvokeDataReceived = OnIncredibuildErrorsReceived;
                this.Invoke(InvokeDataReceived, message);
            }
            else
            {
                AddText(ErrorType.Errors, message);

                ErrorType type = ErrorType.Errors;
                string errorFilename = string.Empty;
                int lineNumber = -1;
                string text = string.Empty;
                if (m_compiler.ParseCompileOutputLine(message, ref type, ref errorFilename, ref lineNumber, ref text))
                {
                    if (type == ErrorType.Errors)
                    {
                        m_ScriptEditor.AddCompileErrorIndicator(errorFilename, lineNumber, Settings.Default.OpenFilesWithCompileErrors);
                    }

                    AddErrorsListViewItem(errorFilename, lineNumber, type, text);
                }
                else
                {
                    AddErrorsListViewItem(string.Empty, -1, ErrorType.Errors, message.Trim());
                }
            }
        }

        private void compileFileForPreviewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_ScriptEditor.CurrentSyntaxEditor == null)
            {
                RefreshCompilingMenuItems();
                return;
            }

            //  Compile the file first
            string filePath = m_ScriptEditor.CurrentSyntaxEditor.Document.Filename;
            if (String.IsNullOrEmpty(filePath))
            {
                return;
            }

            if (m_SavingAndCompiling == SavingAndCompiling.SaveAllBeforeCompile)
            {
                m_ScriptEditor.ExecuteAppAction(ScriptEditor.AppAction.FileSaveAll);
            }
            else if (m_SavingAndCompiling == SavingAndCompiling.SaveBeforeCompile)
            {
                m_ScriptEditor.ExecuteAppAction(ScriptEditor.AppAction.FileSave);
            }

            //  Set the output folder
            string outputDir = ApplicationSettings.GetAbsolutePath(m_ProjectExplorer.ProjectEditorSetting.CurrentCompilingSettings.OutputDirectory);

            String ext = Path.GetExtension(filePath);
            if (ext.Equals(".xml", StringComparison.OrdinalIgnoreCase))
            {
                AddText(ErrorType.Info, $"------ [{DateTime.Now}] Copy data file to preview folder -------", true);
                CopyOutputFileToPreviewFolder(Path.GetDirectoryName(filePath), Path.GetFileName(filePath));
            }
            else if (ext.Equals(".sc", StringComparison.OrdinalIgnoreCase))
            {
                Platform platform = m_ProjectExplorer.UserProjectSettings.SelectedPlatform;

                AddText(ErrorType.Info, $"---------------- [{DateTime.Now}] Compile file for preview -----------------", true);
                this.statusToolStripStatusLabel.Text = "Compiling for preview...";
                this.statusStrip1.Refresh();

                try
                {
                    if (!m_compiler.CompileFileNow(filePath, platform))
                    {
                        AddText(ErrorType.Errors, $"---------------- [{DateTime.Now}] Compile for preview failed -----------------");
                        return;
                    }

                }
                finally
                {
                    this.statusToolStripStatusLabel.Text = "Ready";
                    this.statusStrip1.Refresh();
                }

                // Get the name of the file with no path or extension
                string outputFileNameNoExt = Path.GetFileNameWithoutExtension(filePath);

                //  Copy the output files to the preview folders
                AddText(ErrorType.Info, $"------- [{DateTime.Now}] Copying files to preview folder -------");
                CopyOutputFileToPreviewFolder(outputDir, $"{outputFileNameNoExt}.sco");

                ScriptEditorProjectData data = new ScriptEditorProjectData(m_ProjectExplorer, m_ScriptEditor);
                String platformExt = data.GetPlatformExtension(platform);

                if (platformExt != null)
                {
                    CopyOutputFileToPreviewFolder(Path.Combine(outputDir, platform.ToString()), $"{outputFileNameNoExt}{platformExt}");
                }
                else
                {
                    AddText(ErrorType.Errors, $"Platform extension for {platform} is unknown.  Declare the proper extension in the project properties.");
                }
            }
        }

        private void CopyOutputFileToPreviewFolder(string outputDir, string outputFile)
        {
            string outputFullPath = Path.Combine(outputDir, outputFile);
            String previewDirectory = Path.Combine(ApplicationSettings.GetAbsolutePath(m_ProjectExplorer.ProjectEditorSetting.CurrentCompilingSettings.BuildDirectory), "preview");
            if (String.IsNullOrEmpty(previewDirectory))
            {
                AddText(ErrorType.Errors, $"------ [{DateTime.Now}] ERROR: Preview directory not defined in the compilation settings ------");
                return;
            }

            try
            {
                // Create directories for preview path.
                Directory.CreateDirectory(previewDirectory);

                //  Make sure we have an output file first
                if (File.Exists(outputFullPath))
                {
                    // Copy the output file to the preview folders
                    String previewFilepath = Path.Combine(previewDirectory, outputFile);

                    // If it already exists, make file in preview folder writable before we attempt to copy over it
                    if (File.Exists(previewFilepath))
                    {
                        File.SetAttributes(previewFilepath, File.GetAttributes(previewFilepath) & ~FileAttributes.ReadOnly);
                    }

                    File.Copy(outputFullPath, previewFilepath, true);
                    AddText(ErrorType.Info, $"------ [{DateTime.Now}] '{outputFullPath}' copied to '{previewFilepath}' ------");
                }
                else
                {
                    AddText(ErrorType.Errors, $"------ [{DateTime.Now}] ERROR: Cannot find '{outputFullPath}' to copy! ------");
                }
            }
            catch (Exception ex)
            {
                Log.ToolException(ex, $"Exception copying output file '{outputFullPath}' to preview directory: '{previewDirectory}'");
                AddText(ErrorType.Errors, $"------ [{DateTime.Now}] ERROR: Cannot copy '{outputFullPath}' to preview directory: '{previewDirectory}' ------");
            }
        }

        private void enableWordWrappingInFindResultsDialogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.findResultsRichTextBox.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate() { enableWordWrappingInFindResultsDialogToolStripMenuItem_Click(sender, e); }));
            }
            else
            {
                this.findResultsRichTextBox.WordWrap = this.enableWordWrappingInFindResultsToolStripMenuItem.Checked;
                this.enableWordWrappingInFindResultsToolStripMenuItem.Checked = !this.enableWordWrappingInFindResultsToolStripMenuItem.Checked;
            }
        }

        private void errorsDockableWindow_Load(Object sender, EventArgs e)
        {
            // XXX Viv: Edge case -- If we are actively compiling and the dockable window is opened, we need to call BeginUpdate on the errors list view.
            // This is because BeginUpdate won't apply until the dockable window is created--so we would end up with slow rerendering if lots of errors are
            // being processed while the error window is opened.
            if (this.m_ScriptEditor.CurrentCompileState == ScriptEditor.CompileState.BuildingIncredibuild)
            {
                this.errorsListView.BeginUpdate();
            }
        }
    }
}
