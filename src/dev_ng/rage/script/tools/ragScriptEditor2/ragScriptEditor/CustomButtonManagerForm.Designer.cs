namespace ragScriptEditor
{
    partial class CustomButtonManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonsListView = new System.Windows.Forms.ListView();
            this.buttonsListViewContextMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iconImageList = new System.Windows.Forms.ImageList( this.components );
            this.moveUpButton = new System.Windows.Forms.Button();
            this.moveDownButton = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.newButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.editGroupBox = new System.Windows.Forms.GroupBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.acceptButton = new System.Windows.Forms.Button();
            this.browseCommandButton = new System.Windows.Forms.Button();
            this.commandTextBox = new System.Windows.Forms.TextBox();
            this.iconSizeLabel = new System.Windows.Forms.Label();
            this.browseIconButton = new System.Windows.Forms.Button();
            this.iconPictureBox = new System.Windows.Forms.PictureBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.commandLabel = new System.Windows.Forms.Label();
            this.iconLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.browseIconOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.browseCommandOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.buttonsListViewContextMenuStrip.SuspendLayout();
            this.editGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonsListView
            // 
            this.buttonsListView.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.buttonsListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonsListView.AutoArrange = false;
            this.buttonsListView.ContextMenuStrip = this.buttonsListViewContextMenuStrip;
            this.buttonsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.buttonsListView.HideSelection = false;
            this.buttonsListView.Location = new System.Drawing.Point( 13, 13 );
            this.buttonsListView.MultiSelect = false;
            this.buttonsListView.Name = "buttonsListView";
            this.buttonsListView.Size = new System.Drawing.Size( 400, 124 );
            this.buttonsListView.SmallImageList = this.iconImageList;
            this.buttonsListView.TabIndex = 0;
            this.buttonsListView.UseCompatibleStateImageBehavior = false;
            this.buttonsListView.View = System.Windows.Forms.View.List;
            this.buttonsListView.SelectedIndexChanged += new System.EventHandler( this.buttonsListView_SelectedIndexChanged );
            // 
            // buttonsListViewContextMenuStrip
            // 
            this.buttonsListViewContextMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.editToolStripMenuItem,
            this.moveUpToolStripMenuItem,
            this.moveDownToolStripMenuItem,
            this.deleteToolStripMenuItem} );
            this.buttonsListViewContextMenuStrip.Name = "buttonsListViewContextMenuStrip";
            this.buttonsListViewContextMenuStrip.ShowImageMargin = false;
            this.buttonsListViewContextMenuStrip.Size = new System.Drawing.Size( 117, 114 );
            this.buttonsListViewContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler( this.buttonsListViewContextMenuStrip_Opening );
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size( 116, 22 );
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler( this.newButton_Click );
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size( 116, 22 );
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler( this.editButton_Click );
            // 
            // moveUpToolStripMenuItem
            // 
            this.moveUpToolStripMenuItem.Name = "moveUpToolStripMenuItem";
            this.moveUpToolStripMenuItem.Size = new System.Drawing.Size( 116, 22 );
            this.moveUpToolStripMenuItem.Text = "Move Up";
            this.moveUpToolStripMenuItem.Click += new System.EventHandler( this.moveUpButton_Click );
            // 
            // moveDownToolStripMenuItem
            // 
            this.moveDownToolStripMenuItem.Name = "moveDownToolStripMenuItem";
            this.moveDownToolStripMenuItem.Size = new System.Drawing.Size( 116, 22 );
            this.moveDownToolStripMenuItem.Text = "Move Down";
            this.moveDownToolStripMenuItem.Click += new System.EventHandler( this.moveDownButton_Click );
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size( 116, 22 );
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler( this.deleteButton_Click );
            // 
            // iconImageList
            // 
            this.iconImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.iconImageList.ImageSize = new System.Drawing.Size( 16, 16 );
            this.iconImageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // moveUpButton
            // 
            this.moveUpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.moveUpButton.Location = new System.Drawing.Point( 419, 44 );
            this.moveUpButton.Name = "moveUpButton";
            this.moveUpButton.Size = new System.Drawing.Size( 75, 23 );
            this.moveUpButton.TabIndex = 1;
            this.moveUpButton.Text = "Move &Up";
            this.moveUpButton.UseVisualStyleBackColor = true;
            this.moveUpButton.Click += new System.EventHandler( this.moveUpButton_Click );
            // 
            // moveDownButton
            // 
            this.moveDownButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.moveDownButton.Location = new System.Drawing.Point( 419, 73 );
            this.moveDownButton.Name = "moveDownButton";
            this.moveDownButton.Size = new System.Drawing.Size( 75, 23 );
            this.moveDownButton.TabIndex = 2;
            this.moveDownButton.Text = "&Move Down";
            this.moveDownButton.UseVisualStyleBackColor = true;
            this.moveDownButton.Click += new System.EventHandler( this.moveDownButton_Click );
            // 
            // editButton
            // 
            this.editButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.editButton.Location = new System.Drawing.Point( 12, 143 );
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size( 75, 23 );
            this.editButton.TabIndex = 3;
            this.editButton.Text = "&Edit";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler( this.editButton_Click );
            // 
            // newButton
            // 
            this.newButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.newButton.Location = new System.Drawing.Point( 147, 143 );
            this.newButton.Name = "newButton";
            this.newButton.Size = new System.Drawing.Size( 75, 23 );
            this.newButton.TabIndex = 4;
            this.newButton.Text = "&New";
            this.newButton.UseVisualStyleBackColor = true;
            this.newButton.Click += new System.EventHandler( this.newButton_Click );
            // 
            // deleteButton
            // 
            this.deleteButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.deleteButton.Location = new System.Drawing.Point( 282, 143 );
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size( 75, 23 );
            this.deleteButton.TabIndex = 5;
            this.deleteButton.Text = "&Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler( this.deleteButton_Click );
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.closeButton.Location = new System.Drawing.Point( 419, 143 );
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size( 75, 23 );
            this.closeButton.TabIndex = 6;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // editGroupBox
            // 
            this.editGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.editGroupBox.Controls.Add( this.cancelButton );
            this.editGroupBox.Controls.Add( this.acceptButton );
            this.editGroupBox.Controls.Add( this.browseCommandButton );
            this.editGroupBox.Controls.Add( this.commandTextBox );
            this.editGroupBox.Controls.Add( this.iconSizeLabel );
            this.editGroupBox.Controls.Add( this.browseIconButton );
            this.editGroupBox.Controls.Add( this.iconPictureBox );
            this.editGroupBox.Controls.Add( this.nameTextBox );
            this.editGroupBox.Controls.Add( this.commandLabel );
            this.editGroupBox.Controls.Add( this.iconLabel );
            this.editGroupBox.Controls.Add( this.nameLabel );
            this.editGroupBox.Location = new System.Drawing.Point( 12, 172 );
            this.editGroupBox.Name = "editGroupBox";
            this.editGroupBox.Size = new System.Drawing.Size( 482, 100 );
            this.editGroupBox.TabIndex = 7;
            this.editGroupBox.TabStop = false;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.Location = new System.Drawing.Point( 401, 45 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler( this.cancelButton_Click );
            // 
            // acceptButton
            // 
            this.acceptButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.acceptButton.Location = new System.Drawing.Point( 401, 17 );
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size( 75, 23 );
            this.acceptButton.TabIndex = 9;
            this.acceptButton.Text = "&Accept";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler( this.acceptButton_Click );
            // 
            // browseCommandButton
            // 
            this.browseCommandButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.browseCommandButton.Location = new System.Drawing.Point( 371, 72 );
            this.browseCommandButton.Name = "browseCommandButton";
            this.browseCommandButton.Size = new System.Drawing.Size( 24, 23 );
            this.browseCommandButton.TabIndex = 8;
            this.browseCommandButton.Text = "...";
            this.browseCommandButton.UseVisualStyleBackColor = true;
            this.browseCommandButton.Click += new System.EventHandler( this.browseCommandButton_Click );
            // 
            // commandTextBox
            // 
            this.commandTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.commandTextBox.Location = new System.Drawing.Point( 62, 74 );
            this.commandTextBox.Name = "commandTextBox";
            this.commandTextBox.Size = new System.Drawing.Size( 303, 20 );
            this.commandTextBox.TabIndex = 7;
            // 
            // iconSizeLabel
            // 
            this.iconSizeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.iconSizeLabel.AutoSize = true;
            this.iconSizeLabel.Location = new System.Drawing.Point( 122, 50 );
            this.iconSizeLabel.Name = "iconSizeLabel";
            this.iconSizeLabel.Size = new System.Drawing.Size( 236, 13 );
            this.iconSizeLabel.TabIndex = 6;
            this.iconSizeLabel.Text = "Icons larger than 16x16 may not display properly.";
            // 
            // browseIconButton
            // 
            this.browseIconButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.browseIconButton.Location = new System.Drawing.Point( 92, 45 );
            this.browseIconButton.Name = "browseIconButton";
            this.browseIconButton.Size = new System.Drawing.Size( 24, 23 );
            this.browseIconButton.TabIndex = 5;
            this.browseIconButton.Text = "...";
            this.browseIconButton.UseVisualStyleBackColor = true;
            this.browseIconButton.Click += new System.EventHandler( this.browseIconButton_Click );
            // 
            // iconPictureBox
            // 
            this.iconPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.iconPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iconPictureBox.Location = new System.Drawing.Point( 62, 45 );
            this.iconPictureBox.Name = "iconPictureBox";
            this.iconPictureBox.Size = new System.Drawing.Size( 24, 24 );
            this.iconPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.iconPictureBox.TabIndex = 4;
            this.iconPictureBox.TabStop = false;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.nameTextBox.Location = new System.Drawing.Point( 62, 19 );
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size( 333, 20 );
            this.nameTextBox.TabIndex = 3;
            // 
            // commandLabel
            // 
            this.commandLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.commandLabel.AutoSize = true;
            this.commandLabel.Location = new System.Drawing.Point( 7, 77 );
            this.commandLabel.Name = "commandLabel";
            this.commandLabel.Size = new System.Drawing.Size( 54, 13 );
            this.commandLabel.TabIndex = 2;
            this.commandLabel.Text = "Command";
            this.commandLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // iconLabel
            // 
            this.iconLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.iconLabel.AutoSize = true;
            this.iconLabel.Location = new System.Drawing.Point( 28, 50 );
            this.iconLabel.Name = "iconLabel";
            this.iconLabel.Size = new System.Drawing.Size( 28, 13 );
            this.iconLabel.TabIndex = 1;
            this.iconLabel.Text = "Icon";
            this.iconLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // nameLabel
            // 
            this.nameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point( 21, 22 );
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size( 35, 13 );
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "Name";
            this.nameLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // browseIconOpenFileDialog
            // 
            this.browseIconOpenFileDialog.Filter = "Bitmap Files (*.bmp;*.gif;*.jpg;*.jpeg;*.png;*.ico)|*.bmp;*.gif;*.jpg;*.jpeg;*.pn" +
                "g;*.ico";
            this.browseIconOpenFileDialog.RestoreDirectory = true;
            this.browseIconOpenFileDialog.Title = "Select Icon";
            // 
            // browseCommandOpenFileDialog
            // 
            this.browseCommandOpenFileDialog.Filter = "All files (*.*)|*.*";
            this.browseCommandOpenFileDialog.RestoreDirectory = true;
            // 
            // CustomButtonManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size( 506, 284 );
            this.Controls.Add( this.editGroupBox );
            this.Controls.Add( this.closeButton );
            this.Controls.Add( this.deleteButton );
            this.Controls.Add( this.newButton );
            this.Controls.Add( this.editButton );
            this.Controls.Add( this.moveDownButton );
            this.Controls.Add( this.moveUpButton );
            this.Controls.Add( this.buttonsListView );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CustomButtonManagerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Custom Button Manager";
            this.buttonsListViewContextMenuStrip.ResumeLayout( false );
            this.editGroupBox.ResumeLayout( false );
            this.editGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox)).EndInit();
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.ListView buttonsListView;
        private System.Windows.Forms.Button moveUpButton;
        private System.Windows.Forms.Button moveDownButton;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button newButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.GroupBox editGroupBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label commandLabel;
        private System.Windows.Forms.Label iconLabel;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.PictureBox iconPictureBox;
        private System.Windows.Forms.Button browseIconButton;
        private System.Windows.Forms.Button browseCommandButton;
        private System.Windows.Forms.TextBox commandTextBox;
        private System.Windows.Forms.Label iconSizeLabel;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.OpenFileDialog browseIconOpenFileDialog;
        private System.Windows.Forms.OpenFileDialog browseCommandOpenFileDialog;
        private System.Windows.Forms.ContextMenuStrip buttonsListViewContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ImageList iconImageList;
    }
}