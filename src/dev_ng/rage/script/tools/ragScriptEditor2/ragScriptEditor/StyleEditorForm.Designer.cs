namespace ragScriptEditor
{
    partial class StyleEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textStylesGroupBox = new System.Windows.Forms.GroupBox();
            this.fontSizeLabel = new System.Windows.Forms.Label();
            this.fontSizeComboBox = new System.Windows.Forms.ComboBox();
            this.fontDropDownList = new ActiproSoftware.SyntaxEditor.FontDropDownList();
            this.fontLabel = new System.Windows.Forms.Label();
            this.textBackgroundColorButton = new ragScriptEditor.ColorButton();
            this.displayItemsGroupBox = new System.Windows.Forms.GroupBox();
            this.textStylePreviewLabel = new System.Windows.Forms.Label();
            this.backColorButton = new ragScriptEditor.ColorButton();
            this.foreColorButton = new ragScriptEditor.ColorButton();
            this.boldCheckBox = new System.Windows.Forms.CheckBox();
            this.italicCheckBox = new System.Windows.Forms.CheckBox();
            this.underlineCheckBox = new System.Windows.Forms.CheckBox();
            this.backgroundColorLabel = new System.Windows.Forms.Label();
            this.foregroundColorLabel = new System.Windows.Forms.Label();
            this.textStylePreview = new ActiproSoftware.SyntaxEditor.TextStylePreview();
            this.itemsListBox = new System.Windows.Forms.ListBox();
            this.languageComboBox = new System.Windows.Forms.ComboBox();
            this.languagesLabel = new System.Windows.Forms.Label();
            this.textBackgroundColorLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.applyButton = new System.Windows.Forms.Button();
            this.exportButton = new System.Windows.Forms.Button();
            this.textStylesGroupBox.SuspendLayout();
            this.displayItemsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // textStylesGroupBox
            // 
            this.textStylesGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textStylesGroupBox.Controls.Add(this.fontSizeLabel);
            this.textStylesGroupBox.Controls.Add(this.fontSizeComboBox);
            this.textStylesGroupBox.Controls.Add(this.fontDropDownList);
            this.textStylesGroupBox.Controls.Add(this.fontLabel);
            this.textStylesGroupBox.Controls.Add(this.textBackgroundColorButton);
            this.textStylesGroupBox.Controls.Add(this.displayItemsGroupBox);
            this.textStylesGroupBox.Controls.Add(this.languageComboBox);
            this.textStylesGroupBox.Controls.Add(this.languagesLabel);
            this.textStylesGroupBox.Controls.Add(this.textBackgroundColorLabel);
            this.textStylesGroupBox.Location = new System.Drawing.Point(12, 12);
            this.textStylesGroupBox.Name = "textStylesGroupBox";
            this.textStylesGroupBox.Size = new System.Drawing.Size(429, 348);
            this.textStylesGroupBox.TabIndex = 0;
            this.textStylesGroupBox.TabStop = false;
            this.textStylesGroupBox.Text = "Text Styles";
            // 
            // fontSizeLabel
            // 
            this.fontSizeLabel.AutoSize = true;
            this.fontSizeLabel.Location = new System.Drawing.Point(302, 72);
            this.fontSizeLabel.Name = "fontSizeLabel";
            this.fontSizeLabel.Size = new System.Drawing.Size(27, 13);
            this.fontSizeLabel.TabIndex = 7;
            this.fontSizeLabel.Text = "Size";
            // 
            // fontSizeComboBox
            // 
            this.fontSizeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fontSizeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fontSizeComboBox.FormattingEnabled = true;
            this.fontSizeComboBox.Items.AddRange(new object[] {
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26"});
            this.fontSizeComboBox.Location = new System.Drawing.Point(302, 91);
            this.fontSizeComboBox.Name = "fontSizeComboBox";
            this.fontSizeComboBox.Size = new System.Drawing.Size(121, 21);
            this.fontSizeComboBox.TabIndex = 8;
            this.fontSizeComboBox.SelectedIndexChanged += new System.EventHandler(this.font_SelectedIndexChanged);
            // 
            // fontDropDownList
            // 
            this.fontDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fontDropDownList.FormattingEnabled = true;
            this.fontDropDownList.Location = new System.Drawing.Point(9, 91);
            this.fontDropDownList.Name = "fontDropDownList";
            this.fontDropDownList.Size = new System.Drawing.Size(287, 21);
            this.fontDropDownList.TabIndex = 6;
            this.fontDropDownList.SelectedIndexChanged += new System.EventHandler(this.font_SelectedIndexChanged);
            // 
            // fontLabel
            // 
            this.fontLabel.AutoSize = true;
            this.fontLabel.Location = new System.Drawing.Point(13, 75);
            this.fontLabel.Name = "fontLabel";
            this.fontLabel.Size = new System.Drawing.Size(204, 13);
            this.fontLabel.TabIndex = 5;
            this.fontLabel.Text = "Font (bold type indicates fixed-width fonts)";
            // 
            // textBackgroundColorButton
            // 
            this.textBackgroundColorButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBackgroundColorButton.BackColor = System.Drawing.SystemColors.Window;
            this.textBackgroundColorButton.Color = System.Drawing.Color.White;
            this.textBackgroundColorButton.Location = new System.Drawing.Point(128, 44);
            this.textBackgroundColorButton.Name = "textBackgroundColorButton";
            this.textBackgroundColorButton.Size = new System.Drawing.Size(294, 21);
            this.textBackgroundColorButton.TabIndex = 4;
            this.textBackgroundColorButton.UseVisualStyleBackColor = true;
            this.textBackgroundColorButton.ColorChanged += new System.EventHandler(this.textBackgroundColorButton_ColorChanged);
            // 
            // displayItemsGroupBox
            // 
            this.displayItemsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.displayItemsGroupBox.Controls.Add(this.textStylePreviewLabel);
            this.displayItemsGroupBox.Controls.Add(this.backColorButton);
            this.displayItemsGroupBox.Controls.Add(this.foreColorButton);
            this.displayItemsGroupBox.Controls.Add(this.boldCheckBox);
            this.displayItemsGroupBox.Controls.Add(this.italicCheckBox);
            this.displayItemsGroupBox.Controls.Add(this.underlineCheckBox);
            this.displayItemsGroupBox.Controls.Add(this.backgroundColorLabel);
            this.displayItemsGroupBox.Controls.Add(this.foregroundColorLabel);
            this.displayItemsGroupBox.Controls.Add(this.textStylePreview);
            this.displayItemsGroupBox.Controls.Add(this.itemsListBox);
            this.displayItemsGroupBox.Location = new System.Drawing.Point(9, 118);
            this.displayItemsGroupBox.Name = "displayItemsGroupBox";
            this.displayItemsGroupBox.Size = new System.Drawing.Size(413, 224);
            this.displayItemsGroupBox.TabIndex = 9;
            this.displayItemsGroupBox.TabStop = false;
            this.displayItemsGroupBox.Text = "Display Items";
            // 
            // textStylePreviewLabel
            // 
            this.textStylePreviewLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textStylePreviewLabel.AutoSize = true;
            this.textStylePreviewLabel.Location = new System.Drawing.Point(207, 150);
            this.textStylePreviewLabel.Name = "textStylePreviewLabel";
            this.textStylePreviewLabel.Size = new System.Drawing.Size(45, 13);
            this.textStylePreviewLabel.TabIndex = 18;
            this.textStylePreviewLabel.Text = "Preview";
            // 
            // backColorButton
            // 
            this.backColorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.backColorButton.BackColor = System.Drawing.SystemColors.Window;
            this.backColorButton.Color = System.Drawing.Color.Red;
            this.backColorButton.Location = new System.Drawing.Point(208, 79);
            this.backColorButton.Name = "backColorButton";
            this.backColorButton.Size = new System.Drawing.Size(143, 21);
            this.backColorButton.TabIndex = 14;
            this.backColorButton.Text = "colorButton2";
            this.backColorButton.UseVisualStyleBackColor = true;
            this.backColorButton.ColorChanged += new System.EventHandler(this.backColorButton_ColorChanged);
            // 
            // foreColorButton
            // 
            this.foreColorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.foreColorButton.BackColor = System.Drawing.SystemColors.Window;
            this.foreColorButton.Color = System.Drawing.Color.Red;
            this.foreColorButton.Location = new System.Drawing.Point(208, 35);
            this.foreColorButton.Name = "foreColorButton";
            this.foreColorButton.Size = new System.Drawing.Size(143, 21);
            this.foreColorButton.TabIndex = 12;
            this.foreColorButton.Text = "colorButton1";
            this.foreColorButton.UseVisualStyleBackColor = true;
            this.foreColorButton.ColorChanged += new System.EventHandler(this.foreColorButton_ColorChanged);
            // 
            // boldCheckBox
            // 
            this.boldCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.boldCheckBox.AutoSize = true;
            this.boldCheckBox.Location = new System.Drawing.Point(211, 114);
            this.boldCheckBox.Name = "boldCheckBox";
            this.boldCheckBox.Size = new System.Drawing.Size(47, 17);
            this.boldCheckBox.TabIndex = 15;
            this.boldCheckBox.Text = "Bold";
            this.boldCheckBox.UseVisualStyleBackColor = true;
            this.boldCheckBox.CheckedChanged += new System.EventHandler(this.boldCheckBox_CheckedChanged);
            // 
            // italicCheckBox
            // 
            this.italicCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.italicCheckBox.AutoSize = true;
            this.italicCheckBox.Location = new System.Drawing.Point(273, 114);
            this.italicCheckBox.Name = "italicCheckBox";
            this.italicCheckBox.Size = new System.Drawing.Size(48, 17);
            this.italicCheckBox.TabIndex = 16;
            this.italicCheckBox.Text = "Italic";
            this.italicCheckBox.UseVisualStyleBackColor = true;
            this.italicCheckBox.CheckedChanged += new System.EventHandler(this.italicCheckBox_CheckedChanged);
            // 
            // underlineCheckBox
            // 
            this.underlineCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.underlineCheckBox.AutoSize = true;
            this.underlineCheckBox.Location = new System.Drawing.Point(336, 114);
            this.underlineCheckBox.Name = "underlineCheckBox";
            this.underlineCheckBox.Size = new System.Drawing.Size(71, 17);
            this.underlineCheckBox.TabIndex = 17;
            this.underlineCheckBox.Text = "Underline";
            this.underlineCheckBox.UseVisualStyleBackColor = true;
            this.underlineCheckBox.CheckedChanged += new System.EventHandler(this.underlineCheckBox_CheckedChanged);
            // 
            // backgroundColorLabel
            // 
            this.backgroundColorLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.backgroundColorLabel.AutoSize = true;
            this.backgroundColorLabel.Location = new System.Drawing.Point(208, 63);
            this.backgroundColorLabel.Name = "backgroundColorLabel";
            this.backgroundColorLabel.Size = new System.Drawing.Size(92, 13);
            this.backgroundColorLabel.TabIndex = 13;
            this.backgroundColorLabel.Text = "Background Color";
            // 
            // foregroundColorLabel
            // 
            this.foregroundColorLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.foregroundColorLabel.AutoSize = true;
            this.foregroundColorLabel.Location = new System.Drawing.Point(208, 19);
            this.foregroundColorLabel.Name = "foregroundColorLabel";
            this.foregroundColorLabel.Size = new System.Drawing.Size(88, 13);
            this.foregroundColorLabel.TabIndex = 11;
            this.foregroundColorLabel.Text = "Foreground Color";
            // 
            // textStylePreview
            // 
            this.textStylePreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textStylePreview.Location = new System.Drawing.Point(207, 166);
            this.textStylePreview.Name = "textStylePreview";
            this.textStylePreview.Size = new System.Drawing.Size(200, 52);
            this.textStylePreview.TabIndex = 19;
            // 
            // itemsListBox
            // 
            this.itemsListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.itemsListBox.DisplayMember = "Name";
            this.itemsListBox.FormattingEnabled = true;
            this.itemsListBox.Location = new System.Drawing.Point(7, 20);
            this.itemsListBox.Name = "itemsListBox";
            this.itemsListBox.Size = new System.Drawing.Size(194, 199);
            this.itemsListBox.TabIndex = 10;
            this.itemsListBox.SelectedIndexChanged += new System.EventHandler(this.itemsListBox_SelectedIndexChanged);
            // 
            // languageComboBox
            // 
            this.languageComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.languageComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.languageComboBox.Location = new System.Drawing.Point(128, 13);
            this.languageComboBox.Name = "languageComboBox";
            this.languageComboBox.Size = new System.Drawing.Size(294, 21);
            this.languageComboBox.TabIndex = 2;
            this.languageComboBox.SelectedIndexChanged += new System.EventHandler(this.languageComboBox_SelectedIndexChanged);
            // 
            // languagesLabel
            // 
            this.languagesLabel.AutoSize = true;
            this.languagesLabel.Location = new System.Drawing.Point(62, 16);
            this.languagesLabel.Name = "languagesLabel";
            this.languagesLabel.Size = new System.Drawing.Size(60, 13);
            this.languagesLabel.TabIndex = 1;
            this.languagesLabel.Text = "Languages";
            // 
            // textBackgroundColorLabel
            // 
            this.textBackgroundColorLabel.AutoSize = true;
            this.textBackgroundColorLabel.Location = new System.Drawing.Point(6, 49);
            this.textBackgroundColorLabel.Name = "textBackgroundColorLabel";
            this.textBackgroundColorLabel.Size = new System.Drawing.Size(116, 13);
            this.textBackgroundColorLabel.TabIndex = 3;
            this.textBackgroundColorLabel.Text = "Text Background Color";
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(204, 366);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 20;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(285, 366);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 21;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // applyButton
            // 
            this.applyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.applyButton.Enabled = false;
            this.applyButton.Location = new System.Drawing.Point(366, 366);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(75, 23);
            this.applyButton.TabIndex = 22;
            this.applyButton.Text = "Apply";
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
            // 
            // exportButton
            // 
            this.exportButton.Location = new System.Drawing.Point(12, 366);
            this.exportButton.Name = "exportButton";
            this.exportButton.Size = new System.Drawing.Size(109, 23);
            this.exportButton.TabIndex = 23;
            this.exportButton.Text = "E&xport Settings...";
            this.exportButton.UseVisualStyleBackColor = true;
            this.exportButton.Click += new System.EventHandler(this.exportButton_Click);
            // 
            // StyleEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(453, 401);
            this.Controls.Add(this.exportButton);
            this.Controls.Add(this.applyButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.textStylesGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StyleEditorForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Highlighting Style Editor";
            this.textStylesGroupBox.ResumeLayout(false);
            this.textStylesGroupBox.PerformLayout();
            this.displayItemsGroupBox.ResumeLayout(false);
            this.displayItemsGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox textStylesGroupBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button applyButton;
        private System.Windows.Forms.Label textBackgroundColorLabel;
        private System.Windows.Forms.Label languagesLabel;
        private System.Windows.Forms.ComboBox languageComboBox;
        private System.Windows.Forms.GroupBox displayItemsGroupBox;
        private ActiproSoftware.SyntaxEditor.TextStylePreview textStylePreview;
        private System.Windows.Forms.ListBox itemsListBox;
        private System.Windows.Forms.Label foregroundColorLabel;
        private System.Windows.Forms.Label backgroundColorLabel;
        private System.Windows.Forms.CheckBox boldCheckBox;
        private System.Windows.Forms.CheckBox italicCheckBox;
        private System.Windows.Forms.CheckBox underlineCheckBox;
        private ColorButton textBackgroundColorButton;
        private ColorButton backColorButton;
        private ColorButton foreColorButton;
        private System.Windows.Forms.Label textStylePreviewLabel;
        private System.Windows.Forms.Label fontLabel;
        private ActiproSoftware.SyntaxEditor.FontDropDownList fontDropDownList;
        private System.Windows.Forms.Label fontSizeLabel;
        private System.Windows.Forms.ComboBox fontSizeComboBox;
        private System.Windows.Forms.Button exportButton;
    }
}