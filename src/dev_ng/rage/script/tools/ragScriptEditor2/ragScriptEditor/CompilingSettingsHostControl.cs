using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Command;
using RSG.Base.Forms;
using ragScriptEditorShared;

namespace ragScriptEditor
{
    public partial class CompilingSettingsHostControl : SettingsHostControlBase
    {
        public CompilingSettingsHostControl()
        {
            InitializeComponent();
        }

        #region Variables
        private int m_originalCurrentIndex = 0;
        private string m_projectFilename;
        #endregion

        #region Properties
        public CompilingSettingsCollection CompilingSettings
        {
            get
            {
                CompilingSettingsCollection collection = new CompilingSettingsCollection();
                foreach ( CompilingSettings compilingSetting in this.configurationComboBox.Items )
                {
                    collection.Add( compilingSetting );
                }

                // read back any current changes
                collection[this.configurationComboBox.SelectedIndex] = this.TabSettings as CompilingSettings;

                if ( m_originalCurrentIndex < collection.Count )
                {
                    collection.CurrentIndex = m_originalCurrentIndex;
                }
                else
                {
                    collection.CurrentIndex = collection.Count - 1;
                }

                return collection;
            }
            set
            {
                m_originalCurrentIndex = value.CurrentIndex;

                m_invokeSettingsChanged = false;

                this.configurationComboBox.Items.Clear();
                this.configurationComboBox.Items.AddRange( value.ToArray() );

                // clear the current selection
                this.configurationComboBox.SelectedIndex = -1;
                
                this.configurationComboBox.SelectedIndex = value.CurrentIndex;

                m_invokeSettingsChanged = true;
            }
        }

        public bool BreakOnCompileErrors
        {
            get
            {
                return this.breakOnErrorCheckBox.Checked;
            }
            set
            {
                m_invokeSettingsChanged = false;
                this.breakOnErrorCheckBox.Checked = value;
                m_invokeSettingsChanged = true;
            }
        }

        public bool OpenFilesWithCompilerErrors
        {
            get
            {
                return this.openFilesWithErrorsCheckBox.Checked;
            }
            set
            {
                m_invokeSettingsChanged = false;
                this.openFilesWithErrorsCheckBox.Checked = value;
                m_invokeSettingsChanged = true;
            }
        }

        public string ProjectFilename
        {
            get
            {
                return m_projectFilename;
            }
            set
            {
                m_projectFilename = value;

                if ( m_hostedControl != null )
                {
                    CompilingSettingsControl compilingSettingsCtrl = m_hostedControl as CompilingSettingsControl;
                    compilingSettingsCtrl.ProjectFilename = value;
                }
            }
        }
        #endregion

        #region Overrides
        public override Control ControlToReplace
        {
            get
            {
                return this.groupBox1;
            }
        }

        public override SettingsBase TabSettings
        {
            get
            {
                return base.TabSettings;
            }
            set
            {
                base.TabSettings = value;

                CompilingSettingsControl compilingSettingsCtrl = m_hostedControl as CompilingSettingsControl;
                compilingSettingsCtrl.ProjectFilename = m_projectFilename;
            }
        }

        public override bool ValidateSettings( out string error )
        {
            if (String.IsNullOrWhiteSpace(this.ProjectFilename))
            {
                error = null;
                return true;
            }

            if ( this.configurationComboBox.SelectedIndex != -1 )
            {
                this.configurationComboBox.Items[this.configurationComboBox.SelectedIndex] = this.TabSettings;
            }

            Dictionary<string, string> outputDirs = new Dictionary<string, string>();

            foreach ( CompilingSettings c in this.configurationComboBox.Items )
            {
                if ( !c.Validate( m_projectFilename, out error ) )
                {
                    return false;
                }

                if ( c.LanguageIsCompilable )
                {
                    string configName;
                    string expandedOutputDirectory = ApplicationSettings.GetAbsolutePath(c.OutputDirectory);
                    if ( outputDirs.TryGetValue( expandedOutputDirectory, out configName ) )
                    {
                        error = "The configuration named '" + c.ConfigurationName + "' has the same output directory as '" + configName + "'.";
                        return false;
                    }

                    outputDirs.Add(expandedOutputDirectory, c.ConfigurationName);
                }
            }

            error = null;
            return true;
        }
        #endregion

        #region Events
        public event BooleanValueChangedEventHandler BreakOnCompileErrorsChanged;

        public event BooleanValueChangedEventHandler OpenFilesWithCompilerErrorsChanged;
        #endregion

        #region Event Dispatchers
        private void OnBreakOnCompileErrorsChanged( bool newValue )
        {
            if ( m_invokeSettingsChanged && (this.BreakOnCompileErrorsChanged != null) )
            {
                this.BreakOnCompileErrorsChanged( this, new BooleanValueChangedEventArgs( newValue ) );
            }
        }

        private void OnOpenFilesWithCompilerErrorsChanged( bool newValue )
        {
            if ( m_invokeSettingsChanged && (this.OpenFilesWithCompilerErrorsChanged != null) )
            {
                this.OpenFilesWithCompilerErrorsChanged( this, new BooleanValueChangedEventArgs( newValue ) );
            }
        }
        #endregion

        #region Event Handlers
        private void breakOnErrorCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            OnBreakOnCompileErrorsChanged( this.breakOnErrorCheckBox.Checked );
            OnSettingsChanged();
        }

        private void openFilesWithErrorsCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            OnOpenFilesWithCompilerErrorsChanged( this.openFilesWithErrorsCheckBox.Checked );
            OnSettingsChanged();
        }

        private void configurationComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            CompilingSettings compilingSettings = this.configurationComboBox.SelectedItem as CompilingSettings;
            if ( compilingSettings != null )
            {
                // this will update the UI with the hosted control
                this.TabSettings = compilingSettings;
            }
        }

        private void addConfigurationButton_Click( object sender, EventArgs e )
        {
            ConfigurationNameDialog dlg = new ConfigurationNameDialog();

            DialogResult result;
            bool nameExists;
            do
            {
                result = DialogResult.Cancel;
                nameExists = false;

                result = dlg.ShowDialog( this );
                if ( result == DialogResult.OK )
                {
                    // see if the Configuration Name already exists
                    foreach ( CompilingSettings c in this.configurationComboBox.Items )
                    {
                        if ( dlg.ConfigurationName == c.ConfigurationName )
                        {
                            rageMessageBox.ShowExclamation( this, "A configuration with that name already exists.  Please select another.",
                                "Configuration Name Error" );
                            nameExists = true;
                            break;
                        }
                    }
                }
            }
            while ( nameExists && (result == DialogResult.OK) );

            if ( result == DialogResult.OK )
            {                
                // make a copy so the user starts with something familiar
                CompilingSettings compilingSettings = this.configurationComboBox.SelectedItem as CompilingSettings;
                compilingSettings = compilingSettings.Clone() as CompilingSettings;

                // set the configuration name
                compilingSettings.ConfigurationName = dlg.ConfigurationName;

                this.configurationComboBox.Items.Add( compilingSettings );

                // select the new configuration
                this.configurationComboBox.SelectedIndex = this.configurationComboBox.Items.Count - 1;

                OnSettingsChanged();
            }
        }

        private void removeConfigurationButton_Click( object sender, EventArgs e )
        {
            CompilingSettings compilingSettings = this.configurationComboBox.SelectedItem as CompilingSettings;
            int index = this.configurationComboBox.SelectedIndex;

            DialogResult result = rageMessageBox.ShowWarning( this, 
                String.Format( "Are you sure you want to delete the configuration named '{0}'?\nThis operation cannot be undone.", compilingSettings.ConfigurationName ),
                "Confirm Delete", MessageBoxButtons.YesNo );
            if ( result == DialogResult.Yes )
            {
                // clear the current selection
                this.configurationComboBox.SelectedIndex = -1;

                // remove it from our lists
                this.configurationComboBox.Items.RemoveAt( index );

                // set the new selection
                if ( (index == 0) && this.configurationComboBox.Items.Count > 0 )
                {
                    this.configurationComboBox.SelectedIndex = 0;
                }
                else
                {
                    this.configurationComboBox.SelectedIndex = index - 1;
                }

                OnSettingsChanged();
            }
        }
        #endregion
    }
}
