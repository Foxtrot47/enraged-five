//#define DISABLE_HYPER_THREADING
//#define SINGLE_INSTANCE_APPLICATION

using Microsoft.VisualBasic.ApplicationServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

using RSG.Base.Forms;
using RSG.Base.IO;
using RSG.Base.Logging;
using RSG.Configuration;

using ragScriptEditor.Properties;
using vbAccelerator.Components.Win32;
using ragScriptEditorShared;
using TD.SandDock;

namespace ragScriptEditor
{
#if SINGLE_INSTANCE_APPLICATION
    
    public class SingleInstanceApplication : WindowsFormsApplicationBase
    {
        public SingleInstanceApplication()
        {
            base.IsSingleInstance = true;

            this.MainForm = new Form1();
            this.Startup += new StartupEventHandler( SingleInstanceApplication_Startup );
            this.StartupNextInstance += new StartupNextInstanceEventHandler( SingleInstanceApplication_StartupNextInstance );
        }

        #region Event Handlers
        private void SingleInstanceApplication_Startup( object sender, StartupEventArgs e )
        {
            Form1 mainForm = this.MainForm as Form1;
            string[] args = new string[e.CommandLine.Count];
            e.CommandLine.CopyTo( args, 0 );

            if ( !mainForm.Init( args ) )
            {
                e.Cancel = true;
            }
        }

        private void SingleInstanceApplication_StartupNextInstance( object sender, StartupNextInstanceEventArgs e )
        {
            // look for any .scproj, and launch in new instance.  all subsequent .sc get passed to Init
            // otherwise, all .sc get loaded in current instance

            string projectFile = null;
            List<string> scriptFiles = new List<string>();
            for ( int i = 1; i < e.CommandLine.Count; ++i )
            {
                string cmd = e.CommandLine[i];
                if ( (projectFile == null) && cmd.EndsWith( "proj" ) )
                {
                    projectFile = cmd;
                }
                else
                {
                    scriptFiles.Add( cmd );
                }
            }

            Form1 mainForm = this.MainForm as Form1;
            bool loaded = false;

            if ( projectFile != null )
            {
                if ( mainForm.LoadProjectFile( projectFile ) )
                {
                    loaded = true;
                }
            }

            foreach ( string file in scriptFiles )
            {
                if ( mainForm.LoadScriptFile( file ) )
                {
                    loaded = true;
                }
            }

            if ( loaded )
            {
                mainForm.Focus();
            }
        }
        #endregion
    }

#else

    public class MultipleInstanceProgram : WindowsFormsApplicationBase
    {
        public MultipleInstanceProgram()
        {
            sm_logFilename = Path.Combine( Environment.GetEnvironmentVariable( "TEMP" ), "ragScriptEditor" );
            if ( !Directory.Exists( sm_logFilename ) )
            {
                Directory.CreateDirectory( sm_logFilename );
            }
            
            sm_logFilename = Path.Combine( sm_logFilename, 
                String.Format( "ragScriptEditor_{0}.html", rageFileUtilities.GenerateTimeBasedFilename() ) );

            Log.Message( "Script Editor Version: {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() );

            this.MainForm = new Form1();
            this.Startup += new StartupEventHandler( MultipleInstanceProgram_Startup );

            m_mutex = new Mutex( true, "RageScriptEditor", out m_mutexIsNew );

            // Assign the handle:
            m_copyData.AssignHandle( this.MainForm.Handle );

            // Create the named channels to send and receive on.  It is based on the number of Script Editors running.
            Process[] processes = Process.GetProcessesByName( Path.GetFileNameWithoutExtension( Application.ExecutablePath ) );
            m_channelNumber = processes.Length;
            m_copyData.Channels.Add( this.ChannelName );

            // Hook up event notifications whenever a message is received:
            m_copyData.DataReceived += new vbAccelerator.Components.Win32.DataReceivedEventHandler( copyData_DataReceived );
        }

        #region Variables
        private Mutex m_mutex;
        private bool m_mutexIsNew = false;
        private string m_channelBaseName = "ScriptEditorFileLoad";
        private int m_channelNumber = 0;
        private vbAccelerator.Components.Win32.CopyData m_copyData = new vbAccelerator.Components.Win32.CopyData();

        private static string sm_logFilename = string.Empty;
        #endregion

        #region Properties
        public string ChannelName
        {
            get
            {
                return m_channelBaseName + m_channelNumber;
            }
        }

        public ILog Log
        {
            get
            {
                return LogFactory.ApplicationLog;
            }
        }
        #endregion

        #region Event Handlers
        private void MultipleInstanceProgram_Startup( object sender, StartupEventArgs e )
        {
            string[] fullCommandLine = Environment.GetCommandLineArgs();                
            if ( !m_mutexIsNew )
            {
                bool isNonCommandLine = false;

                List<string> potentialFilenames = new List<string>();
                foreach ( string cmd in e.CommandLine )
                {
                    if ( (cmd[0] == '-') || cmd.EndsWith( "proj" ) )
                    {
                        isNonCommandLine = false;
                        break;
                    }
                    else
                    {
                        if ( File.Exists( cmd ) )
                        {
                            potentialFilenames.Add( Path.GetFullPath( cmd ) );
                        }

                        isNonCommandLine = true;
                    }
                }

                // see if we can open the script file(s) in an existing instance
                if ( isNonCommandLine && (potentialFilenames.Count > 0) )
                {
                    try
                    {
                        // Create the named channel to send to.  Let's just use the name of the first one started.
                        string sendChannelName = m_channelBaseName + 1;
                        m_copyData.Channels.Add( sendChannelName );

                        // send new file names over message queue:
                        m_copyData.Channels[sendChannelName].Send( potentialFilenames.ToArray() );

                        e.Cancel = true;
                    }
                    catch ( Exception ex )
                    {
                        Log.ToolException(ex, "Exception attempting to connect to another script editor instance");
                    }
                }
            }

            // init ourselves as a new instance
            Form1 mainForm = this.MainForm as Form1;
            string[] args = new string[e.CommandLine.Count];
            e.CommandLine.CopyTo( args, 0 );

            if ( !mainForm.Init( args ) )
            {
                e.Cancel = true;
            }
        }

        private void copyData_DataReceived( object sender, vbAccelerator.Components.Win32.DataReceivedEventArgs e )
        {
            if ( e.ChannelName.Equals( this.ChannelName ) )
            {
                string[] filenames = e.Data as string[];

                Form1 mainForm = this.MainForm as Form1;
                foreach ( string filename in filenames )
                {
                    mainForm.LoadScriptFile( filename );
                }

                mainForm.BringToFront();
                mainForm.Focus();
                mainForm.Show();
                mainForm.Activate();
            }
        }
        #endregion
    }

#endif

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main( string[] args )
        {
#if DISABLE_HYPER_THREADING
            IntPtr handle = (IntPtr)Process.GetCurrentProcess().Handle;
            UIntPtr ProcAffinityMask = UIntPtr.Zero;
            UIntPtr SysAffinityMask = UIntPtr.Zero;
            
            if ( ApplicationSettings.GetProcessAffinityMask( handle, out ProcAffinityMask, out SysAffinityMask ) )
            {
                if ( (uint)SysAffinityMask == 3 ) // 1 proc, 2 logical CPUs
                {
                    if ( ApplicationSettings.SetProcessAffinityMask( handle, new UIntPtr( (uint)1 ) ) )
                    {
                        Console.WriteLine( "SetProcessAffinityMask(1)" );
                    }
                    else
                    {
                        Console.WriteLine( "SetProcessAffinityMask(1) failed {0}", Marshal.GetLastWin32Error() );
                    }
                }
                else if ( (uint)SysAffinityMask == 15 )  //dual proc, 4 virtual CPUs
                {
                    if ( ApplicationSettings.SetProcessAffinityMask( handle, new UIntPtr( (uint)3 ) ) )
                    {
                        Console.WriteLine( "SetProcessAffinityMask(3)" );
                    }
                    else
                    {
                        Console.WriteLine( "SetProcessAffinityMask(3) failed {0}", Marshal.GetLastWin32Error() );
                    }
                }
            }
            else
            {
                Console.WriteLine( "GetProcessAffinityMask failed {0}", Marshal.GetLastWin32Error() );
            }
#endif
            ToolsConfig toolsConfig = new ToolsConfig();
            LogFactory.Initialize();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            Application.ThreadException += new ThreadExceptionEventHandler( Application_ThreadException );

        //    SandDockManager.ActivateProduct( "1029|mNHCocHlEeYJt/q8SORWBb15rDw=" ); // 3.0.4.1

            ApplicationSettings.ExecutableFullPathName = Application.ExecutablePath;
            ApplicationSettings.ProductName = Application.ProductName;
            ApplicationSettings.Mode = ApplicationSettings.ApplicationMode.Editor;

            Application.DoEvents();

#if SINGLE_INSTANCE_APPLICATION
            SingleInstanceApplication program = new SingleInstanceApplication();
            program.Run( args );
#else
            try
            {
                MultipleInstanceProgram program = new MultipleInstanceProgram();
                program.Run(args);
            }
            catch (Exception e)
            {
                // this will handle exceptions that are thrown before the Application.ThreadException is active
                Application_ThreadException(null, new ThreadExceptionEventArgs(e));
            }
            LogFactory.ApplicationShutdown();
#endif
        }

        static void Application_ThreadException( object sender, ThreadExceptionEventArgs e )
        {
            List<string> attachments = new List<string>();

            LogFactory.ApplicationLog.ToolException(e.Exception, "Top-level application exception encountered.");
            LogFactory.FlushApplicationLog();

            if (File.Exists(LogFactory.ApplicationLogFilename))
            {
                attachments.Add(LogFactory.ApplicationLogFilename);
            }

            string userSettingsFilename = ApplicationSettings.GetCommonSettingsFilename( false );
            if ( !String.IsNullOrEmpty( userSettingsFilename ) && File.Exists( userSettingsFilename ) )
            {
                attachments.Add( userSettingsFilename );
            }

            string projectSettingsFilename = ApplicationSettings.LastProjectSettingsFile;
            if ( !String.IsNullOrEmpty( projectSettingsFilename ) && File.Exists( projectSettingsFilename ) )
            {
                attachments.Add( projectSettingsFilename );
            }

            string userConfigFilename = rageFileUtilities.GetRageApplicationUserConfigFilename( "ragScriptEditor.exe" );
            if ( !String.IsNullOrEmpty( userConfigFilename ) && File.Exists( userConfigFilename ) )
            {
                attachments.Add( userConfigFilename );
            }

            IConfig config = ConfigFactory.CreateConfig(LogFactory.ApplicationLog);

            UnhandledExceptionDialog eForm = new UnhandledExceptionDialog( "Script Editor", attachments, 
                config.Project.ToolsEmailAddresses.First().Address, e.Exception );
            eForm.ShowDialog();

            ApplicationSettings.ExitingAfterException = true;

            try
            {
                Application.Exit();
            }
            catch ( Exception ex )
            {
                LogFactory.ApplicationLog.ToolException(ex, "Application.Exit() failed");
                Application.Exit();
            }
        }
    }
}