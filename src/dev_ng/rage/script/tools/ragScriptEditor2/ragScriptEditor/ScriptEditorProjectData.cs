﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ragScriptEditorShared;
using RSG.Configuration;
using RSG.Pipeline.BuildScript;
using RSG.Platform;

namespace ragScriptEditor
{
    class ScriptEditorProjectData : IProjectData
    {
        #region Private member fields

        private ProjectExplorer m_projExplorer;
        private ScriptEditor m_scriptEditor;
        private CompilingSettings m_compileSettings;
        private List<Platform> m_enabledPlatforms;
        private IEnvironment m_environment;

        /// <summary>
        /// Hack solution for handling the 64-bit resources in the interim until we can move to AP3.
        /// </summary>
        private const string ResourceExecutable64Name = "scriptrc_x64.exe";

        #endregion

        #region Public properties

        public string OutputDirectory => m_compileSettings.OutputDirectory;

        public string BuildDirectory => m_compileSettings.BuildDirectory;

        public string ProjectFileName => m_projExplorer.ProjectFileName;

        public string Custom => m_compileSettings.Custom;

        public string XmlIncludeFile => m_compileSettings.XmlIncludeFile;

        public List<string> IncludePaths => m_compileSettings.IncludePaths;

        public string ConfigurationName => m_compileSettings.ConfigurationName;

        public string CompilerExecutable => m_compileSettings.CompilerExecutable;

        public string ResourceExecutable => m_compileSettings.ResourceExecutable;

        public string ResourceExecutable64 => Path.Combine(Path.GetDirectoryName(m_compileSettings.ResourceExecutable), ResourceExecutable64Name);
        public string PostCompileCommand => m_compileSettings.PostCompileCommand;

        public bool WarningsAsErrors { get; }

        /// <summary>
        /// Always a full build because we decide what files to build.
        /// </summary>
        public bool FullBuild => true;

        public bool UseIdeMonitor => false;

        public bool UsesPostBuild => false;

        public IList<Platform> EnabledPlatforms => m_enabledPlatforms;

        public IEnvironment Environment => m_environment;

        public bool Nopopups => false;

        #endregion

        #region Constructor

        internal ScriptEditorProjectData(ProjectExplorer projectExplorer, ScriptEditor scriptEditor)
        {
            m_projExplorer = projectExplorer;
            m_scriptEditor = scriptEditor;
            m_compileSettings = projectExplorer.ProjectEditorSetting.CurrentCompilingSettings;
            m_enabledPlatforms = new List<Platform>();
            m_enabledPlatforms.Add(projectExplorer.UserProjectSettings.SelectedPlatform);
            WarningsAsErrors = false;

            CompilingSettings compileSettings = projectExplorer.ProjectEditorSetting.CurrentCompilingSettings;
            if (compileSettings is SanScriptParser.SanScriptCompilingSettings)
            {
                WarningsAsErrors = (compileSettings as SanScriptParser.SanScriptCompilingSettings).WarningsAsErrors;
            }

            // Copy environment from app settings.
            m_environment = ConfigFactory.CreateEnvironment();
            foreach (KeyValuePair<String,String> kvp in ApplicationSettings.Environment)
            {
                m_environment.Add(kvp.Key.Trim('$', '(', ')'), kvp.Value);
            }

            // Add project-specfic vars.
            m_environment.Add("OutputDirectory", OutputDirectory);
            m_environment.Add("BuildDir", BuildDirectory);
            m_environment.Add("ProjectFileName", ProjectFileName);
            m_environment.Add("Custom", Custom);
            m_environment.Add("XmlIncludeFile", XmlIncludeFile);
            m_environment.Add("ConfigurationName", ConfigurationName);
            m_environment.Add("CompilerExecutable", CompilerExecutable);
            m_environment.Add("ResourceExecutable", ResourceExecutable);
            m_environment.Add("ResourceExecutable64", ResourceExecutable64);
            m_environment.Add("PostCompileCommand", PostCompileCommand);
            // Creates "Win64,XboxOne" string
            m_environment.Add("PlatformName",
                String.Format("\"{0}\"",
                    String.Join(",", EnabledPlatforms.Select(p => p.ToString()))));
        }

        #endregion

        #region Public methods

        public IList<string> GetFilesToCompile()
        {
            IList<String> m_projectFilenames = m_projExplorer.GetProjectFilenames();

            if (m_scriptEditor != null)
            {
                m_scriptEditor.PreCompile();
            }

            return m_projectFilenames;
        }

        public IEnumerable<string> GetIncludesForFile(String scriptFile)
        {
            if (m_scriptEditor == null)
            {
                // No script editor instance, so we don't have parser info, so no knowledge of include files (and we don't support incremental mode).
                return new String[0];
            }

            return m_scriptEditor.GetIncludesForFile(scriptFile);
        }

        public IList<String> GetFilesToResource(RSG.Platform.Platform platform)
        {
            var m_projectFilenames = new List<string>();
            List<string> filenames = m_projExplorer.GetProjectFilenames();

            string projectFileName = m_projExplorer.ProjectFileName;
            string personalFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
            int lastSlash = projectFileName.LastIndexOf('\\') + 1;
            string projectName = projectFileName.Substring(lastSlash, projectFileName.Length - lastSlash);
            projectName = projectName.Substring(0, projectName.LastIndexOf('.'));

            if (m_scriptEditor != null)
            {
                foreach (string file in filenames)
                {
                    if (m_scriptEditor.FileNeedsToBeResourced(file, platform, projectFileName, m_projExplorer.ProjectEditorSetting.CurrentCompilingSettings))
                    {
                        m_projectFilenames.Add(file);
                    }
                }
            }
            else
            {
                m_projectFilenames = filenames;
            }

            return m_projectFilenames;
        }

        public void PreBuild()
        {

        }

        public void PostBuild(int exitCode)
        {

        }


        /// <summary>
        /// Acquires the platform extension based on the project properties.
        /// </summary>
        public string GetPlatformExtension(Platform platform) => FileType.Script.GetPlatformExtension(platform);
        #endregion
    }
}
