using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using ragScriptEditor.Properties;
using ActiproSoftware.SyntaxEditor;
using ragScriptEditorShared;
using RSG.Base.Command;
using RSG.Base.Forms;
using RSG.Base.IO;
using RSG.Base.Logging;

namespace ragScriptEditor
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();

            m_numCtrlsAtStart = this.groupBox1.Parent.Controls.Count;
        }

        public DialogResult ShowIt( Control parent, UserEditorSettings currentUserSettings, 
            Dictionary<string, LanguageDefaultEditorSettingsBase> languageDefaultSettings, string projectFile, bool admin )
        {
            m_currentUserEditorSettings = currentUserSettings;
            m_languageDefaultEditorSettings = languageDefaultSettings;
            m_projectFile = projectFile;
            m_admin = admin;

            SetUIFromUserEditorSettings( m_currentUserEditorSettings, projectFile, admin );

            this.applyButton.Enabled = false;

            DialogResult result = ShowDialog( parent );

            m_currentUserEditorSettings = null;
            m_languageDefaultEditorSettings = null;
            return result;
        }

        #region Delegates
        public delegate void SaveSettingsEventHandler( object sender, UserEditorSettingsEventArgs e );
        public delegate void LanguageDefaultSettingsEventHandler( object sender, LanguageDefaultSettingsEventArgs e );
        #endregion

        #region Variables
        private int m_numCtrlsAtStart = 0;
        private UserEditorSettings m_currentUserEditorSettings = null;
        private Dictionary<string, LanguageDefaultEditorSettingsBase> m_languageDefaultEditorSettings = null;
        private string m_projectFile = string.Empty;
        private bool m_admin = false;
        #endregion

        #region Events
        public event SaveSettingsEventHandler SaveSettings;
        public event EventHandler ClearRecentProjectsList;
        public event EventHandler ClearRecentFilesList;
        public event LanguageDefaultSettingsEventHandler SaveDefaultSettings;
        public event LanguageDefaultSettingsEventHandler LoadDefaultSettings;
        #endregion

        #region Overrides
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( components != null )
                {
                    components.Dispose();
                }

                SettingsControlBase settingsCtrl = this.treeView1.Nodes[0].Tag as SettingsControlBase;
                settingsCtrl.Dispose();

                settingsCtrl = this.treeView1.Nodes[1].Tag as SettingsControlBase;
                settingsCtrl.Dispose();

                settingsCtrl = this.treeView1.Nodes[2].Tag as SettingsControlBase;
                settingsCtrl.Dispose();

                foreach ( TreeNode node in this.treeView1.Nodes[3].Nodes )
                {
                    settingsCtrl = node.Tag as SettingsControlBase;
                    settingsCtrl.Dispose();
                }

                foreach ( TreeNode node in this.treeView1.Nodes[4].Nodes )
                {
                    settingsCtrl = node.Tag as SettingsControlBase;
                    settingsCtrl.Dispose();
                }

                settingsCtrl = this.treeView1.Nodes[5].Tag as SettingsControlBase;
                settingsCtrl.Dispose();
            }
            base.Dispose( disposing );
        }
        #endregion

        #region Event Handlers
        private void treeView1_AfterSelect( object sender, TreeViewEventArgs e )
        {
            if ( (e.Node != null) && (e.Node.Tag != null) )
            {
                this.SuspendLayout();

                if ( this.groupBox1.Parent.Controls.Count > m_numCtrlsAtStart )
                {
                    this.groupBox1.Parent.Controls.RemoveAt( this.groupBox1.Parent.Controls.Count - 1 );
                }

                Control ctrl = e.Node.Tag as Control;
                //ctrl.Size = this.groupBox1.Size;
                this.groupBox1.Parent.Controls.Add( ctrl );

                this.ResumeLayout( true );
            }
        }

        private void resetToDefaultsButton_Click( object sender, EventArgs e )
        {
            if ( (this.treeView1.SelectedNode != null) && (this.treeView1.SelectedNode.Tag != null) )
            {
                SettingsControlBase settingsCtrl = this.treeView1.SelectedNode.Tag as SettingsControlBase;
                SettingsBase settings = settingsCtrl.TabSettings;

                // find the language default settings
                LanguageDefaultEditorSettingsBase defaultSettings = null;
                if ( settingsCtrl is CompilingSettingsHostControl )
                {
                    CompilingSettings c = settings as CompilingSettings;
                    if ( !m_languageDefaultEditorSettings.TryGetValue( c.Language, out defaultSettings ) )
                    {
                        // error message?
                        return;
                    }
                }
                else if ( settings is LanguageSettings )
                {
                    LanguageSettings l = settings as LanguageSettings;
                    if ( !m_languageDefaultEditorSettings.TryGetValue( l.Language, out defaultSettings ) )
                    {
                        // error message?
                        return;
                    }
                }
                else
                {
                    if ( !m_languageDefaultEditorSettings.TryGetValue( m_currentUserEditorSettings.CurrentCompilingLanguage,
                        out defaultSettings ) )
                    {
                        // error message?
                        return;
                    }
                }

                DialogResult result = rageMessageBox.ShowQuestion( this,
                    String.Format( "You are about to reset the {0} Settings to its\n{1}-specific default state.  The change will not become\npermanent until you click OK or Apply.\n\nDo you wish to proceed?", settingsCtrl.TabSettings.Name, defaultSettings.LanguageName ),
                    String.Format( "Reset {0} Settings", settingsCtrl.TabSettings.Name ) );
                if ( result == DialogResult.Yes )
                {
                    // copy over the set of items we're resetting
                    if ( settingsCtrl is CompilingSettingsHostControl )
                    {
                        CompilingSettings c = settings as CompilingSettings;
                        CompilingSettingsHostControl hostCtrl = settingsCtrl as CompilingSettingsHostControl;
                        
                        CompilingSettingsCollection collection = new CompilingSettingsCollection();
                        collection.AddRange( defaultSettings.CompilingSettingsList.ToArray() );                       
                        collection.CurrentIndex = 0;

                        hostCtrl.CompilingSettings = collection;
                    }                    
                    else
                    {
                        if ( settings is ProjectSettings )
                        {
                            settings.Copy( defaultSettings.Project );
                            settingsCtrl.TabSettings = settings;
                        }
                        else if ( settings is FileSettings )
                        {
                            settings.Copy( defaultSettings.Files );
                            settingsCtrl.TabSettings = settings;
                        }
                        else if ( settings is ToolbarSettings )
                        {
                            settings.Copy( defaultSettings.Toolbar );
                            settingsCtrl.TabSettings = settings;
                        }
                        else if ( settings is LanguageSettings )
                        {
                            settings.Copy( defaultSettings.Language );
                            settingsCtrl.TabSettings = settings;
                        }
                        else if ( settings is SourceControlSettings2 )
                        {
                            settings.Copy( defaultSettings.SourceControl2 );
                            settingsCtrl.TabSettings = settings;
                        }
                        else if (settings is PlatformSettings)
                        {
                            settings.Copy(defaultSettings.PlatformSettings);
                            settingsCtrl.TabSettings = settings;
                        }
                    }

                    this.applyButton.Enabled = true;
                }
            }
        }

        private void okButton_Click( object sender, EventArgs e )
        {
            if ( !this.applyButton.Enabled || OnSaveSettings() )
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void applyButton_Click( object sender, EventArgs e )
        {
            if ( OnSaveSettings() )
            {
                this.applyButton.Enabled = false;
            }
        }

        #region TabSettingsControl Event Handlers
        private void tabSettingsCtrl_SettingsChanged( object sender, EventArgs e )
        {
            this.applyButton.Enabled = true;
        }

        private void projectSettingsCtrl_ClearRecentProjects( object sender, EventArgs e )
        {
            DialogResult result = rageMessageBox.ShowQuestion( this, "Are you sure you want to clear\nthe Recent Projects list?",
                "Clear Recent Projects");
            if ( result == DialogResult.Yes )
            {
                OnClearRecentProjectsList();
            }
        }

        private void fileSettingsCtrl_ClearRecentFiles( object sender, EventArgs e )
        {
            DialogResult result = rageMessageBox.ShowQuestion( this, "Are you sure you want to clear\nthe Recent Files list?",
                "Clear Recent Files" );
            if ( result == DialogResult.Yes )
            {
                OnClearRecentFilesList();
            }
        }

        private void compilingSettingsCtrl_BreakOnCompileErrorsChanged( object sender, BooleanValueChangedEventArgs e )
        {
            // update each CompilingTabSettingsHostControl
            foreach ( TreeNode t in this.treeView1.Nodes[3].Nodes )
            {
                if ( sender != t.Tag )
                {
                    CompilingSettingsHostControl compilingSettingsCtrl = t.Tag as CompilingSettingsHostControl;
                    compilingSettingsCtrl.BreakOnCompileErrors = e.NewValue;
                }
            }
        }

        private void compilingSettingsCtrl_OpenFilesWithCompilerErrorsChanged( object sender, BooleanValueChangedEventArgs e )
        {
            // update each CompilingTabSettingsHostControl
            foreach ( TreeNode t in this.treeView1.Nodes[3].Nodes )
            {
                if ( sender != t.Tag )
                {
                    CompilingSettingsHostControl compilingSettingsCtrl = t.Tag as CompilingSettingsHostControl;
                    compilingSettingsCtrl.OpenFilesWithCompilerErrors = e.NewValue;
                }
            }
        }

        private void languageSettingsCtrl_SaveDefaultSettings( object sender, EventArgs e )
        {
            // Comment in if we allow non-admins to change this.
            //DialogResult result = rageMessageBox.Show( this, "Are you sure you want to save the Default Settings file for your game-specific Parser?\nThis should typically be done only by your game's lead Scripter.",
            //	"Saving the Default Settings File", MessageBoxButtons.YesNo, MessageBoxIcon.Question );
            //if ( result == DialogResult.Yes )
            {
                SettingsControlBase settingsCtrl = sender as SettingsControlBase;
                SettingsBase settings = settingsCtrl.TabSettings;

                if ( settings is LanguageSettings )
                {
                    LanguageSettings languageSettings = settings as LanguageSettings;
                    if ( OnSaveDefaultSettings( languageSettings.Language ) )
                    {
                        rageMessageBox.ShowNone( this, String.Format( "File for the {0} language saved successfully!", languageSettings.Language ),
                        "Default Settings File" );
                    }
                    else
                    {
                        rageMessageBox.ShowError( this, 
                            String.Format( "Unable to save the default settings file for the {0} language.", languageSettings.Language ),
                            "File Save Error" );
                    }
                }
            }
        }

        private void languageSettingsCtrl_LoadDefaultSettings( object sender, EventArgs e )
        {
            SettingsControlBase settingsCtrl = sender as SettingsControlBase;
            SettingsBase settings = settingsCtrl.TabSettings;

            if ( settings is LanguageSettings )
            {
                LanguageSettings languageSettings = settings as LanguageSettings;
                if ( OnLoadDefaultSettings( languageSettings.Language ) )
                {
                    StringBuilder msg = new StringBuilder();
                    msg.Append( "File Loaded successfully!\n" );
                    msg.Append( "\n" );
                    msg.Append( "The settings from this file can be used to overwrite the current\n" );
                    msg.Append( "Projects, Files and Toolbar Settings, as well as the " + languageSettings.Language + "\n" );
                    msg.Append( "Compiling and " + languageSettings.Language + " Language Settings.\n" );
                    msg.Append( "\n" );                    
                    msg.Append( "Would you like to overwrite the current settings?" );

                    DialogResult result = rageMessageBox.ShowQuestion( this, msg.ToString(), "Default Settings File" );
                    if ( result == DialogResult.Yes )
                    {
                        LanguageDefaultEditorSettingsBase defaultSettings;
                        if ( m_languageDefaultEditorSettings.TryGetValue( languageSettings.Language, out defaultSettings ) )
                        {
                            m_currentUserEditorSettings.Copy( defaultSettings );

                            SetUIFromUserEditorSettings( m_currentUserEditorSettings, m_projectFile, m_admin );
                        }
                    }
                }
                else
                {
                    rageMessageBox.ShowError( this, 
                        String.Format( "Unable to load the default settings file for the {0} language.", languageSettings.Language ),
                        "File Load Error" );
                }
            }
        }
        #endregion

        #endregion

        #region Event Dispatchers
        private bool OnSaveSettings()
        {
            if ( ValidateSettings() )
            {
                if ( m_currentUserEditorSettings != null )
                {
                    SetUserEditorSettingsFromUI( m_currentUserEditorSettings );
                }

                if ( this.SaveSettings != null )
                {
                    this.SaveSettings( this, new UserEditorSettingsEventArgs( m_currentUserEditorSettings ) );
                }

                return true;
            }

            return false;
        }

        private void OnClearRecentProjectsList()
        {
            if ( this.ClearRecentProjectsList != null )
            {
                this.ClearRecentProjectsList( this, EventArgs.Empty );
            }
        }
        
        private void OnClearRecentFilesList()
        {
            if ( this.ClearRecentFilesList != null )
            {
                this.ClearRecentFilesList( this, EventArgs.Empty );
            }
        }
        
        private bool OnSaveDefaultSettings( string language )
        {
            if ( this.SaveDefaultSettings != null )
            {
                LanguageDefaultSettingsEventArgs e = new LanguageDefaultSettingsEventArgs( language );
                this.SaveDefaultSettings( this, e );

                return e.Success;
            }

            return false;
        }

        private bool OnLoadDefaultSettings( string language )
        {
            if ( this.LoadDefaultSettings != null )
            {
                LanguageDefaultSettingsEventArgs e = new LanguageDefaultSettingsEventArgs( language );
                this.LoadDefaultSettings( this, e );

                return e.Success;
            }

            return false;
        }
        #endregion

        #region Private Functions
        private void SetUIFromUserEditorSettings( UserEditorSettings userSettings, string projectFile, bool admin )
        {
            ProjectSettingsHostControl projectSettingsCtrl = null;
            FileSettingsHostControl fileSettingsCtrl = null;
            ToolbarSettingsControl toolbarSettingsCtrl = null;
            SourceControlSettingsControl srcCtrlSettingsCtrl = null;
            PlatformSettingsControl platformSettingsCtrl = null;

            bool firstTime = false;
            if ( this.treeView1.Nodes.Count == 0 )
            {
                firstTime = true;

                // create ProjectTabSettingsHostControl
                projectSettingsCtrl = new ProjectSettingsHostControl();
                projectSettingsCtrl.ClearRecentProjects += new EventHandler( projectSettingsCtrl_ClearRecentProjects );
                
                TreeNode node = CreateTreeNode( "Projects", projectSettingsCtrl );
                this.treeView1.Nodes.Add( node );

                // create FileTabSettingsHostControl
                fileSettingsCtrl = new FileSettingsHostControl();
                fileSettingsCtrl.ClearRecentFiles += new EventHandler( fileSettingsCtrl_ClearRecentFiles );
                
                node = CreateTreeNode( "Files", fileSettingsCtrl );
                this.treeView1.Nodes.Add( node );

                // create ToolbarTabSettingsControl
                toolbarSettingsCtrl = userSettings.Toolbar.CreateSettingsControl() as ToolbarSettingsControl;

                node = CreateTreeNode( "Toolbars", toolbarSettingsCtrl );
                this.treeView1.Nodes.Add( node );

                // create Compiling Node
                this.treeView1.Nodes.Add( "Compiling" );

                // create a CompilingTabSettingsHostControl for each language
                SortedDictionary<string, TreeNode> nodes = new SortedDictionary<string, TreeNode>();
                foreach ( KeyValuePair<string, CompilingSettingsCollection> pair in userSettings.LanguageCompilingSettings )
                {
                    if ( String.IsNullOrEmpty( pair.Key ) )
                    {
                        LogFactory.ApplicationLog.Error( "Unable to create Compiling Settings Tree Node" );
                        continue;
                    }

                    CompilingSettingsHostControl compilingSettingsCtrl = new CompilingSettingsHostControl();
                    compilingSettingsCtrl.BreakOnCompileErrorsChanged += new BooleanValueChangedEventHandler( compilingSettingsCtrl_BreakOnCompileErrorsChanged );
                    compilingSettingsCtrl.OpenFilesWithCompilerErrorsChanged += new BooleanValueChangedEventHandler( compilingSettingsCtrl_OpenFilesWithCompilerErrorsChanged );

                    node = CreateTreeNode( pair.Key, compilingSettingsCtrl );
                    nodes.Add( pair.Key, node );                    
                }

                foreach ( TreeNode t in nodes.Values )
                {
                    this.treeView1.Nodes[3].Nodes.Add( t );
                }

                this.treeView1.Nodes.Add( "Languages" );

                // create a LanguageTabSettingsControl for each language
                nodes.Clear();
                foreach ( LanguageSettings l in userSettings.LanguageSettingsList )
                {
                    if ( String.IsNullOrEmpty( l.Language ) )
                    {
                        LogFactory.ApplicationLog.Error( "Unable to create Language Settings Tree Node" );
                        continue;
                    }

                    LanguageSettingsControl languageSettingsCtrl = l.CreateSettingsControl() as LanguageSettingsControl;
                    languageSettingsCtrl.LoadDefaultSettings += new EventHandler( languageSettingsCtrl_LoadDefaultSettings );
                    languageSettingsCtrl.SaveDefaultSettings += new EventHandler( languageSettingsCtrl_SaveDefaultSettings );

                    node = CreateTreeNode( l.Language, languageSettingsCtrl );
                    nodes.Add( l.Language, node );
                }

                foreach ( TreeNode t in nodes.Values )
                {
                    this.treeView1.Nodes[4].Nodes.Add( t );
                }

                // create a Source Control node
                srcCtrlSettingsCtrl = userSettings.SourceControl2.CreateSettingsControl()
                    as SourceControlSettingsControl;

                node = CreateTreeNode( "Source Control", srcCtrlSettingsCtrl );
                this.treeView1.Nodes.Add( node );

                // Create a platforms node
                platformSettingsCtrl = userSettings.PlatformSettings.CreateSettingsControl() as PlatformSettingsControl;
                node = CreateTreeNode("Platforms", platformSettingsCtrl);
                this.treeView1.Nodes.Add(node);
            }

            // set ProjectTabSettingsHostControl
            projectSettingsCtrl = this.treeView1.Nodes[0].Tag as ProjectSettingsHostControl;
            if ( projectSettingsCtrl != null )
            {
                projectSettingsCtrl.TabSettings = userSettings.Project;                

                projectSettingsCtrl.ReloadLastProject = Settings.Default.ReloadLastProject;

                if ( firstTime )
                {
                    SetMinimumSize( projectSettingsCtrl );
                }
            }

            // set FileTabSettingsHostControl
            fileSettingsCtrl = this.treeView1.Nodes[1].Tag as FileSettingsHostControl;
            if ( fileSettingsCtrl != null )
            {
                fileSettingsCtrl.TabSettings = userSettings.Files;

                fileSettingsCtrl.ReloadFiles = Settings.Default.ReloadLastFiles;

                if ( firstTime )
                {
                    SetMinimumSize( fileSettingsCtrl );
                }
            }

            // set ToolbarTabSettingsControl
            toolbarSettingsCtrl = this.treeView1.Nodes[2].Tag as ToolbarSettingsControl;
            if ( toolbarSettingsCtrl != null )
            {
                toolbarSettingsCtrl.TabSettings = userSettings.Toolbar;

                if ( firstTime )
                {
                    SetMinimumSize( toolbarSettingsCtrl );
                }
            }

            // set the CompilingTabSettingsHostControl for each language
            foreach ( TreeNode t in this.treeView1.Nodes[3].Nodes )
            {
                CompilingSettingsHostControl compilingSettingsCtrl = t.Tag as CompilingSettingsHostControl;
                compilingSettingsCtrl.CompilingSettings = userSettings.LanguageCompilingSettings[t.Text];
                
                compilingSettingsCtrl.BreakOnCompileErrors = Settings.Default.BreakOnCompileError;
                compilingSettingsCtrl.OpenFilesWithCompilerErrors = Settings.Default.OpenFilesWithCompileErrors;
                compilingSettingsCtrl.ProjectFilename = projectFile;

                if ( firstTime )
                {
                    SetMinimumSize( compilingSettingsCtrl );
                }
            }

            // set the LanguageTabSettingsControl for each language
            foreach ( TreeNode t in this.treeView1.Nodes[4].Nodes )
            {
                foreach ( LanguageSettings l in userSettings.LanguageSettingsList )
                {
                    if ( l.Language == t.Text )
                    {
                        LanguageSettingsControl languageSettingsCtrl = t.Tag as LanguageSettingsControl;
                        languageSettingsCtrl.TabSettings = l;
                        languageSettingsCtrl.SaveDefaultSettingsEnabled = admin;

                        if ( firstTime )
                        {
                            SetMinimumSize( languageSettingsCtrl );
                        }

                        break;
                    }
                }
            }

            // set the SourceControlTabSettingsControl
            srcCtrlSettingsCtrl = this.treeView1.Nodes[5].Tag as SourceControlSettingsControl;
            if ( srcCtrlSettingsCtrl != null )
            {
                srcCtrlSettingsCtrl.TabSettings = userSettings.SourceControl2;

                if ( firstTime )
                {
                    SetMinimumSize( srcCtrlSettingsCtrl );
                }
            }

            platformSettingsCtrl = this.treeView1.Nodes[6].Tag as PlatformSettingsControl;
            if (platformSettingsCtrl != null)
            {
                platformSettingsCtrl.TabSettings = userSettings.PlatformSettings;

                if (firstTime)
                {
                    SetMinimumSize(platformSettingsCtrl);
                }
            }
        }

        private void SetUserEditorSettingsFromUI( UserEditorSettings userSettings )
        {
            ProjectSettingsHostControl projectSettingsCtrl = null;
            FileSettingsHostControl fileSettingsCtrl = null;
            ToolbarSettingsControl toolbarSettingsCtrl = null;
            SourceControlSettingsControl srcCtrlSettingsCtrl = null;
            PlatformSettingsControl platformSettingsCtrl = null;

            // get ProjectTabSettingsHostControl
            projectSettingsCtrl = this.treeView1.Nodes[0].Tag as ProjectSettingsHostControl;
            if ( projectSettingsCtrl != null )
            {
                userSettings.Project.Copy( projectSettingsCtrl.TabSettings );

                Settings.Default.ReloadLastProject = projectSettingsCtrl.ReloadLastProject;
            }

            // get FileTabSettingsHostControl
            fileSettingsCtrl = this.treeView1.Nodes[1].Tag as FileSettingsHostControl;
            if ( fileSettingsCtrl != null )
            {
                userSettings.Files.Copy( fileSettingsCtrl.TabSettings );

                Settings.Default.ReloadLastFiles = fileSettingsCtrl.ReloadFiles;
            }

            // get ToolbarTabSettingsControl
            toolbarSettingsCtrl = this.treeView1.Nodes[2].Tag as ToolbarSettingsControl;
            if ( toolbarSettingsCtrl != null )
            {
                userSettings.Toolbar.Copy( toolbarSettingsCtrl.TabSettings );
            }

            // get the CompilingTabSettingsHostControl for each language
            if (!String.IsNullOrWhiteSpace(this.m_projectFile))
            {
                foreach (TreeNode t in this.treeView1.Nodes[3].Nodes)
                {
                    CompilingSettingsHostControl compilingSettingsCtrl = t.Tag as CompilingSettingsHostControl;

                    CompilingSettingsCollection collection;
                    if (userSettings.LanguageCompilingSettings.TryGetValue(t.Text, out collection))
                    {
                        collection.Clear();
                        collection.AddRange(compilingSettingsCtrl.CompilingSettings);
                        collection.CurrentIndex = compilingSettingsCtrl.CompilingSettings.CurrentIndex;
                    }

                    Settings.Default.BreakOnCompileError = compilingSettingsCtrl.BreakOnCompileErrors;
                    Settings.Default.OpenFilesWithCompileErrors = compilingSettingsCtrl.OpenFilesWithCompilerErrors;
                }
            }

            // get the LanguageTabSettingsControl for each language
            foreach ( TreeNode t in this.treeView1.Nodes[4].Nodes )
            {
                foreach ( LanguageSettings l in userSettings.LanguageSettingsList )
                {
                    if ( l.Language == t.Text )
                    {
                        LanguageSettingsControl languageSettingsCtrl = t.Tag as LanguageSettingsControl;
                        LanguageSettings languageSettings = languageSettingsCtrl.TabSettings as LanguageSettings;

                        string cacheDirectory = languageSettings.CacheDirectory.Trim();
                        string previousCacheDirectory = l.CacheDirectory.Trim();
                        if ( cacheDirectory != previousCacheDirectory )
                        {
                            if ( !Directory.Exists( cacheDirectory ) )
                            {
                                Directory.CreateDirectory( cacheDirectory );
                            }

                            if ( Directory.Exists( previousCacheDirectory ) )
                            {
                                DialogResult result = rageMessageBox.ShowQuestion( this,
                                    String.Format( "The {0} Cache Directory has changed. Would you like to move the cache files over to the new directory?", l.Language ),
                                    "Cache Directory Changed" );
                                if ( result == DialogResult.Yes )
                                {
                                    MoveCacheDirectory( previousCacheDirectory, cacheDirectory );
                                }
                            }
                        }

                        l.Copy( languageSettings );
                        break;
                    }
                }
            }

            // get the SourceControlTabSettingsControl
            srcCtrlSettingsCtrl = this.treeView1.Nodes[5].Tag as SourceControlSettingsControl;
            if ( srcCtrlSettingsCtrl != null )
            {
                userSettings.SourceControl2.Copy( srcCtrlSettingsCtrl.TabSettings );
            }

            // get the PlatformSettingsControl
            platformSettingsCtrl = this.treeView1.Nodes[6].Tag as PlatformSettingsControl;
            if (platformSettingsCtrl != null)
            {
                userSettings.PlatformSettings.Copy(platformSettingsCtrl.TabSettings);
            }
        }

        private void MoveCacheDirectory( string srcDirectory, string destDirectory )
        {
            while ( true )
            {
                rageStatus status;
                rageFileUtilities.CopyLocalFolder( srcDirectory, destDirectory, out status );
                if ( !status.Success() )
                {
                    DialogResult result = rageMessageBox.ShowError( this,
                        String.Format( "Could not copy '{0}' to '{1}'\n\n{2}", srcDirectory, destDirectory, status.ErrorString ),
                       "Copy Error", MessageBoxButtons.AbortRetryIgnore );

                    if ( result == DialogResult.Abort )
                    {
                        return;
                    }
                    else if ( result == DialogResult.Retry )
                    {
                        continue;
                    }
                }

                break;
            }

            while ( true )
            {
                rageStatus status;
                rageFileUtilities.DeleteLocalFolder( srcDirectory, out status );
                if ( !status.Success() )
                {
                    DialogResult result = rageMessageBox.ShowError( this,
                        String.Format( "Could not delete '{0}'\n\n{1}", srcDirectory, status.ErrorString ),
                       "Delete Error", MessageBoxButtons.AbortRetryIgnore );

                    if ( result == DialogResult.Retry )
                    {
                        continue;
                    }
                }

                break;
            }
        }

        private TreeNode CreateTreeNode( string name, SettingsControlBase settingsCtrl )
        {
            settingsCtrl.Anchor = this.groupBox1.Anchor;
            settingsCtrl.Dock = this.groupBox1.Dock;
            settingsCtrl.Location = this.groupBox1.Location;
            settingsCtrl.TabIndex = this.groupBox1.TabIndex + 1;
            settingsCtrl.SettingsChanged += new EventHandler( tabSettingsCtrl_SettingsChanged );

            TreeNode node = new TreeNode( name );
            node.Tag = settingsCtrl;

            return node;
        }

        private void SetMinimumSize( SettingsControlBase settingsCtrl )
        {
            // make sure we are at least large enough to display the entire control
            int width = this.Size.Width;
            if ( settingsCtrl.Size.Width > this.groupBox1.Size.Width )
            {
                width += (settingsCtrl.Size.Width - this.groupBox1.Size.Width);
            }

            int height = this.Size.Height;
            if ( settingsCtrl.Size.Height > this.groupBox1.Size.Height )
            {
                height += (settingsCtrl.Size.Height - this.groupBox1.Size.Height);
            }

            this.MinimumSize = new Size( width, height );
        }

        private bool ValidateSettings()
        {
            string error;

            List<SettingsControlBase> settingsCtrls = new List<SettingsControlBase>();

            ProjectSettingsHostControl projectSettingsCtrl = null;
            FileSettingsHostControl fileSettingsCtrl = null;
            ToolbarSettingsControl toolbarSettingsCtrl = null;
            SourceControlSettingsControl srcCtrlSettingsCtrl = null;
            PlatformSettingsControl platformSettingsCtrl = null;

            // validate ProjectTabSettingsHostControl
            projectSettingsCtrl = this.treeView1.Nodes[0].Tag as ProjectSettingsHostControl;
            if ( projectSettingsCtrl != null )
            {
                if ( !projectSettingsCtrl.ValidateSettings( out error ) )
                {
                    DialogResult result = rageMessageBox.ShowExclamation( this, error, "Invalid Project Settings", 
                        MessageBoxButtons.AbortRetryIgnore );
                    if ( result == DialogResult.Abort )
                    {
                        return false;
                    }
                    else if ( result == DialogResult.Retry )
                    {
                        return ValidateSettings();
                    }
                }
            }

            // validate FileTabSettingsHostControl
            fileSettingsCtrl = this.treeView1.Nodes[1].Tag as FileSettingsHostControl;
            if ( fileSettingsCtrl != null )
            {
                if ( !fileSettingsCtrl.ValidateSettings( out error ) )
                {
                    DialogResult result = rageMessageBox.ShowExclamation( this, error, "Invalid File Settings",
                        MessageBoxButtons.AbortRetryIgnore );
                    if ( result == DialogResult.Abort )
                    {
                        return false;
                    }
                    else if ( result == DialogResult.Retry )
                    {
                        return ValidateSettings();
                    }
                }
            }

            // validate ToolbarTabSettingsControl
            toolbarSettingsCtrl = this.treeView1.Nodes[2].Tag as ToolbarSettingsControl;
            if ( toolbarSettingsCtrl != null )
            {
                if ( !toolbarSettingsCtrl.ValidateSettings( out error ) )
                {
                    DialogResult result = rageMessageBox.ShowExclamation( this, error, "Invalid Toolbar Settings",
                        MessageBoxButtons.AbortRetryIgnore );
                    if ( result == DialogResult.Abort )
                    {
                        return false;
                    }
                    else if ( result == DialogResult.Retry )
                    {
                        return ValidateSettings();
                    }
                }
            }

            // validate the CompilingTabSettingsHostControl for each language
            foreach ( TreeNode t in this.treeView1.Nodes[3].Nodes )
            {
                CompilingSettingsHostControl compilingSettingsCtrl = t.Tag as CompilingSettingsHostControl;
                if ( !compilingSettingsCtrl.ValidateSettings( out error ) )
                {
                    DialogResult result = rageMessageBox.ShowExclamation( this, error, 
                        String.Format( "Invalid {0} Compiling Settings", t.Text ), MessageBoxButtons.AbortRetryIgnore );
                    if ( result == DialogResult.Abort )
                    {
                        return false;
                    }
                    else if ( result == DialogResult.Retry )
                    {
                        return ValidateSettings();
                    }
                }
            }

            // validate the LanguageTabSettingsControl for each language
            foreach ( TreeNode t in this.treeView1.Nodes[4].Nodes )
            {
                LanguageSettingsControl languageSettingsCtrl = t.Tag as LanguageSettingsControl;
                if ( !languageSettingsCtrl.ValidateSettings( out error ) )
                {
                    DialogResult result = rageMessageBox.ShowExclamation( this, error,
                        String.Format( "Invalid {0} Language Settings", t.Text ), MessageBoxButtons.AbortRetryIgnore );
                    if ( result == DialogResult.Abort )
                    {
                        return false;
                    }
                    else if ( result == DialogResult.Retry )
                    {
                        return ValidateSettings();
                    }
                }
            }

            // validate the SourceControlTabSettingsControl
            srcCtrlSettingsCtrl = this.treeView1.Nodes[5].Tag as SourceControlSettingsControl;
            if ( srcCtrlSettingsCtrl != null )
            {
                if ( !srcCtrlSettingsCtrl.ValidateSettings( out error ) )
                {
                    DialogResult result = rageMessageBox.ShowExclamation( this, error, "Invalid Source Control Settings",
                        MessageBoxButtons.AbortRetryIgnore );
                    if ( result == DialogResult.Abort )
                    {
                        return false;
                    }
                    else if ( result == DialogResult.Retry )
                    {
                        return ValidateSettings();
                    }
                }
            }

            // validate the PlatformSettingsControl
            platformSettingsCtrl = this.treeView1.Nodes[6].Tag as PlatformSettingsControl;
            if (platformSettingsCtrl != null)
            {
                if (!platformSettingsCtrl.ValidateSettings(out error))
                {
                    DialogResult result = rageMessageBox.ShowExclamation(this, error, "Invalid Platform Settings",
                        MessageBoxButtons.AbortRetryIgnore);
                    if (result == DialogResult.Abort)
                    {
                        return false;
                    }
                    else if (result == DialogResult.Retry)
                    {
                        return ValidateSettings();
                    }
                }
            }

            return true;
        }
        #endregion
    }

    #region EventArgs
    public class UserEditorSettingsEventArgs : EventArgs
    {
        public UserEditorSettingsEventArgs( UserEditorSettings userSettings )
        {
            m_userSettings = userSettings;
        }

        #region Variables
        private UserEditorSettings m_userSettings;
        #endregion

        #region Properties
        public UserEditorSettings UserSettings
        {
            get
            {
                return m_userSettings;
            }
        }
        #endregion
    }

    public class LanguageDefaultSettingsEventArgs : EventArgs
    {
        public LanguageDefaultSettingsEventArgs( string language )
        {
            m_language = language;
        }

        #region Variables
        private string m_language;
        private bool m_success = true;
        #endregion

        #region Properties
        public string Language
        {
            get
            {
                return m_language;
            }
        }

        public bool Success
        {
            get
            {
                return m_success;
            }
            set
            {
                m_success = value;
            }
        }
        #endregion
    };
    #endregion
}