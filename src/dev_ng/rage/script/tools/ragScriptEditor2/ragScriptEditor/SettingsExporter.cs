﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ragScriptEditorShared;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

namespace ragScriptEditor
{
    /// <summary>
    /// Settings export wrapper.
    /// </summary>
    static class SettingsExporter
    {
        /// <summary>
        /// Export the settings file.
        /// </summary>
        /// <param name="language">Language.</param>
        /// <param name="langStyle">Language style.</param>
        /// <returns>True if the settings file was written to disk.</returns>
        public static bool Export(string language, LanguageHighlightingStyle langStyle)
        {
            if (language != "SanScript")
            {
                MessageBox.Show("Export only works on SanScript colour scheme.");
                return false;
            }

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Visual Studio Settings Files(*.vssettings)|*.vssettings";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string pathToFile = Path.Combine(System.Environment.GetEnvironmentVariable("RS_TOOLSROOT"), @"etc\ragScriptEditor\vssettings.template");
                VsSettingsExport export = new VsSettingsExport(pathToFile);


                var map = CreateMappings();
                foreach (var setting in map)
                {
                    CreateColour(setting, langStyle, export);
                }

                return export.WriteSettings(sfd.FileName) == VsSettingsExport.Status.Success;
            }

            return false;
        }

        /// <summary>
        /// Create a colour in the export settings.
        /// </summary>
        /// <param name="setting">Setting names.</param>
        /// <param name="langStyle">Language style.</param>
        /// <param name="export">Settings exporter.</param>
        private static void CreateColour(KeyValuePair<string, string> setting, LanguageHighlightingStyle langStyle, VsSettingsExport export)
        {
            var item = langStyle.XmlHighlightingStyles.FirstOrDefault(style => style.Name == setting.Value);
            if (item != null)
            {
                export.SetColour(setting.Key, VsSettingsExport.ColourPlacement.Foreground, item.ForeColor.R, item.ForeColor.G, item.ForeColor.B);
            }
        }

        /// <summary>
        /// Create the name mapping between the old editor and VS settings names.
        /// </summary>
        /// <returns>The mappings.</returns>
        private static Dictionary<string, string> CreateMappings()
        {
            Dictionary<string, string> map = new Dictionary<string, string>();
            map.Add("SanScript.Default", "Text");
            map.Add("SanScript.Identifier", "User Word");
            map.Add("SanScript.Keyword", "Reserved Word");
            map.Add("SanScript.Method", "User Word");
            map.Add("SanScript.String", "String Default");
            map.Add("SanScript.VectorValue", "Number");

            return map;
        }
    }
}
