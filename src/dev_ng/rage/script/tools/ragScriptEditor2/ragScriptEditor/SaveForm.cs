using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ragScriptEditorShared;

namespace ragScriptEditor
{
    public partial class SaveForm : Form
    {
        public SaveForm()
        {
            InitializeComponent();
        }

        public SaveForm( Form mainForm, List<string> unsavedFilenames )
        {
            InitializeComponent();

            m_mainForm = mainForm;

            this.unsavedFilesListBox.Items.Clear();
            foreach ( string file in unsavedFilenames )
            {
                this.unsavedFilesListBox.Items.Add( file );
            }

            this.saveSelectedButton.Enabled = false;

            this.cancelButton.Enabled = ApplicationSettings.ExitingAfterException == false;
            this.ControlBox = this.cancelButton.Enabled;
        }

        /// <summary>
        /// Instantiates a SaveForm and launches it as a dialog
        /// </summary>
        /// <param name="unsavedFilenames"></param>
        /// <returns>false on cancel</returns>
        public static bool ShowIt( Form mainForm, List<string> unsavedFilenames, SaveFilesEventHandler saveFilesDel )
        {
            SaveForm saveForm = new SaveForm( mainForm, unsavedFilenames );

            if ( saveFilesDel != null )
            {
                saveForm.SaveFiles += saveFilesDel;
            }

            DialogResult result = saveForm.ShowDialog( mainForm );
            if ( result == DialogResult.OK )
            {
                return true;
            }

            return false;
        }

        #region Delegates
        public delegate void SaveFilesEventHandler( object sender, List<string> filenames );
        #endregion

        #region Events
        public event SaveFilesEventHandler SaveFiles;
        #endregion

        #region Variables
        private Form m_mainForm;
        #endregion

        #region Event Handlers
        private void saveAllButton_Click( object sender, EventArgs e )
        {
            List<string> filenames = new List<string>();

            foreach ( string file in this.unsavedFilesListBox.Items )
            {
                filenames.Add( file );
            }

            OnSaveFiles( filenames );
        }

        private void saveSelectedButton_Click( object sender, EventArgs e )
        {
            List<string> filenames = new List<string>();

            foreach ( string file in this.unsavedFilesListBox.SelectedItems )
            {
                filenames.Add( file );
            }

            OnSaveFiles( filenames );
        }

        private void OnSaveFiles( List<string> filenames )
        {
            if ( SaveFiles != null )
            {
                if ( m_mainForm != null )
                {
                    object[] args = new object[2];
                    args[0] = this;
                    args[1] = filenames;

                    m_mainForm.Invoke( SaveFiles, args );
                }
                else
                {
                    SaveFiles( this, filenames );
                }
            }
        }

        private void unsavedFilesListBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            this.saveSelectedButton.Enabled = this.unsavedFilesListBox.SelectedItems.Count > 0;
        }
        #endregion
    }
}