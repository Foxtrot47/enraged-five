namespace ragScriptEditor
{
    partial class SourceControlForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileLabel = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.overwriteAllButton = new System.Windows.Forms.Button();
            this.overwriteButton = new System.Windows.Forms.Button();
            this.checkOutAllButton = new System.Windows.Forms.Button();
            this.checkOutButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // fileLabel
            // 
            this.fileLabel.AutoSize = true;
            this.fileLabel.Location = new System.Drawing.Point( 13, 13 );
            this.fileLabel.Name = "fileLabel";
            this.fileLabel.Size = new System.Drawing.Size( 0, 13 );
            this.fileLabel.TabIndex = 0;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.AutoSize = true;
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cancelButton.Location = new System.Drawing.Point( 441, 66 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler( this.cancelButton_Click );
            // 
            // overwriteAllButton
            // 
            this.overwriteAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.overwriteAllButton.AutoSize = true;
            this.overwriteAllButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.overwriteAllButton.Location = new System.Drawing.Point( 361, 66 );
            this.overwriteAllButton.Name = "overwriteAllButton";
            this.overwriteAllButton.Size = new System.Drawing.Size( 76, 23 );
            this.overwriteAllButton.TabIndex = 2;
            this.overwriteAllButton.Text = "Overwrite All";
            this.overwriteAllButton.UseVisualStyleBackColor = true;
            this.overwriteAllButton.Click += new System.EventHandler( this.overwriteAllButton_Click );
            // 
            // overwriteButton
            // 
            this.overwriteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.overwriteButton.AutoSize = true;
            this.overwriteButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.overwriteButton.Location = new System.Drawing.Point( 282, 66 );
            this.overwriteButton.Name = "overwriteButton";
            this.overwriteButton.Size = new System.Drawing.Size( 75, 23 );
            this.overwriteButton.TabIndex = 3;
            this.overwriteButton.Text = "Overwrite";
            this.overwriteButton.UseVisualStyleBackColor = true;
            this.overwriteButton.Click += new System.EventHandler( this.overwriteButton_Click );
            // 
            // checkOutAllButton
            // 
            this.checkOutAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkOutAllButton.AutoSize = true;
            this.checkOutAllButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.checkOutAllButton.Location = new System.Drawing.Point( 196, 66 );
            this.checkOutAllButton.Name = "checkOutAllButton";
            this.checkOutAllButton.Size = new System.Drawing.Size( 82, 23 );
            this.checkOutAllButton.TabIndex = 4;
            this.checkOutAllButton.Text = "Check Out All";
            this.checkOutAllButton.UseVisualStyleBackColor = true;
            this.checkOutAllButton.Click += new System.EventHandler( this.checkOutAllButton_Click );
            // 
            // checkOutButton
            // 
            this.checkOutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkOutButton.AutoSize = true;
            this.checkOutButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.checkOutButton.Location = new System.Drawing.Point( 117, 66 );
            this.checkOutButton.Name = "checkOutButton";
            this.checkOutButton.Size = new System.Drawing.Size( 75, 23 );
            this.checkOutButton.TabIndex = 5;
            this.checkOutButton.Text = "Check Out";
            this.checkOutButton.UseVisualStyleBackColor = true;
            this.checkOutButton.Click += new System.EventHandler( this.checkOutButton_Click );
            // 
            // SourceControlForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 528, 101 );
            this.ControlBox = false;
            this.Controls.Add( this.checkOutButton );
            this.Controls.Add( this.checkOutAllButton );
            this.Controls.Add( this.overwriteButton );
            this.Controls.Add( this.overwriteAllButton );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.fileLabel );
            this.Name = "SourceControlForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "File Checked-In Under Source Control";
            this.Activated += new System.EventHandler( this.SourceControlForm_Activated );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label fileLabel;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button overwriteAllButton;
        private System.Windows.Forms.Button overwriteButton;
        private System.Windows.Forms.Button checkOutAllButton;
        private System.Windows.Forms.Button checkOutButton;
    }
}