using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ragScriptEditorShared;

namespace ragScriptEditor
{
    /// <summary>
    /// Base class for a SettingsControlBase control that must merge editor-only settings (i.e. things stored in 
    /// Properites.Settings.Default) with settings from a SettingsBase class.
    /// </summary>
    public partial class SettingsHostControlBase : ragScriptEditorShared.SettingsControlBase
    {
        public SettingsHostControlBase()
        {
            InitializeComponent();
        }

        #region Variables
        protected SettingsControlBase m_hostedControl = null;
        #endregion

        #region Overrides
        public override SettingsBase TabSettings
        {
            get
            {
                if ( m_hostedControl != null )
                {
                    return m_hostedControl.TabSettings;
                }

                return null;
            }
            set
            {
                m_invokeSettingsChanged = false;

                // verify we have the same project class
                if ( (m_hostedControl != null) && m_hostedControl.TabSettings.GetType().Equals( value.GetType() ) )
                {
                    m_hostedControl.TabSettings = value;   // this will update the values in the UI
                }
                else
                {
                    // create the control
                    m_hostedControl = value.CreateSettingsControl();
                    m_hostedControl.TabSettings = value;
                    m_hostedControl.SettingsChanged += new EventHandler( hostedControl_SettingsChanged );

                    // use the ControlToReplace as a template
                    if ( this.ControlToReplace != null )
                    {
                        // make sure we are at least large enough to display the entire control
                        int width = this.Size.Width;
                        if ( m_hostedControl.Size.Width > this.ControlToReplace.Size.Width )
                        {
                            width += (m_hostedControl.Size.Width - this.ControlToReplace.Size.Width);
                        }

                        int height = this.Size.Height;
                        if ( m_hostedControl.Size.Height > this.ControlToReplace.Size.Height )
                        {
                            height += (m_hostedControl.Size.Height - this.ControlToReplace.Size.Height);
                        }

                        this.MinimumSize = new Size( width, height );

                        m_hostedControl.Anchor = this.ControlToReplace.Anchor;
                        m_hostedControl.Dock = this.ControlToReplace.Dock;
                        m_hostedControl.Location = this.ControlToReplace.Location;
                        m_hostedControl.TabIndex = this.ControlToReplace.TabIndex + 1;
                    
                        // hide the control
                        this.ControlToReplace.Visible = false;
                        
                        // add the control to the ControlToReplace's parent, if possible
                        if ( this.ControlToReplace.Parent != null )
                        {
                            this.ControlToReplace.Parent.Controls.Add( m_hostedControl );
                        }
                        else
                        {
                            this.Controls.Add( m_hostedControl );
                        }
                    }
                    else
                    {
                        // make sure we are at least large enough to display the entire control
                        int width = this.Size.Width;
                        if ( m_hostedControl.Size.Width > this.Size.Width )
                        {
                            width += (m_hostedControl.Size.Width - this.Size.Width);
                        }

                        int height = this.Size.Height;
                        if ( m_hostedControl.Size.Height > this.Size.Height )
                        {
                            height += (m_hostedControl.Size.Height - this.Size.Height);
                        }

                        this.MinimumSize = new Size( width, height );

                        this.Refresh();

                        this.Controls.Add( m_hostedControl );
                    }
                }

                m_invokeSettingsChanged = true;
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Overridden by the derived class to indicate which control the hosted control should lay on top of.
        /// </summary>
        public virtual Control ControlToReplace
        {
            get
            {
                return null;
            }
        }
        #endregion

        #region Event Handlers
        protected void hostedControl_SettingsChanged( object sender, EventArgs e )
        {
            OnSettingsChanged();
        }
        #endregion
    }
}

