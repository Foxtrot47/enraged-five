namespace ragScriptEditor
{
    partial class CacheSplashForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.buildingCacheLabel = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.remainingOverTotalLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.Location = new System.Drawing.Point(12, 17);
            this.descriptionLabel.Margin = new System.Windows.Forms.Padding(50);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(351, 29);
            this.descriptionLabel.TabIndex = 0;
            this.descriptionLabel.Text = "If this is the first time loading this project then it may take a couple of minut" +
    "es, but will be much faster the next time the project loads.";
            this.descriptionLabel.UseWaitCursor = true;
            // 
            // buildingCacheLabel
            // 
            this.buildingCacheLabel.Location = new System.Drawing.Point(12, 51);
            this.buildingCacheLabel.Name = "buildingCacheLabel";
            this.buildingCacheLabel.Size = new System.Drawing.Size(96, 16);
            this.buildingCacheLabel.TabIndex = 1;
            this.buildingCacheLabel.Text = "Building Cache...";
            this.buildingCacheLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buildingCacheLabel.UseWaitCursor = true;
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 70);
            this.progressBar.MarqueeAnimationSpeed = 30;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(351, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 2;
            this.progressBar.UseWaitCursor = true;
            // 
            // remainingOverTotalLabel
            // 
            this.remainingOverTotalLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.remainingOverTotalLabel.Location = new System.Drawing.Point(197, 51);
            this.remainingOverTotalLabel.Name = "remainingOverTotalLabel";
            this.remainingOverTotalLabel.Size = new System.Drawing.Size(166, 15);
            this.remainingOverTotalLabel.TabIndex = 3;
            this.remainingOverTotalLabel.Text = "0 / 0";
            this.remainingOverTotalLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.remainingOverTotalLabel.UseWaitCursor = true;
            // 
            // CacheSplashForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(378, 105);
            this.ControlBox = false;
            this.Controls.Add(this.remainingOverTotalLabel);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.buildingCacheLabel);
            this.Controls.Add(this.descriptionLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "CacheSplashForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Loading Script Project...";
            this.UseWaitCursor = true;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CacheSplashForm_FormClosed);
            this.Load += new System.EventHandler(this.CacheSplashForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.Label buildingCacheLabel;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label remainingOverTotalLabel;
    }
}