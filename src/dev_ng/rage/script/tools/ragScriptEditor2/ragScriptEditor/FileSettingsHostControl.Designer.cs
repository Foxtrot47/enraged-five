namespace ragScriptEditor
{
    partial class FileSettingsHostControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.reloadFilesCheckBox = new System.Windows.Forms.CheckBox();
            this.clearRecentFilesButton = new System.Windows.Forms.Button();
            this.lineSeparatorGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lineSeparatorGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // reloadFilesCheckBox
            // 
            this.reloadFilesCheckBox.AutoSize = true;
            this.reloadFilesCheckBox.Location = new System.Drawing.Point( 3, 7 );
            this.reloadFilesCheckBox.Name = "reloadFilesCheckBox";
            this.reloadFilesCheckBox.Size = new System.Drawing.Size( 147, 17 );
            this.reloadFilesCheckBox.TabIndex = 0;
            this.reloadFilesCheckBox.Text = "Reload last files at startup";
            this.reloadFilesCheckBox.UseVisualStyleBackColor = true;
            this.reloadFilesCheckBox.CheckedChanged += new System.EventHandler( this.reloadFilesCheckBox_CheckedChanged );
            // 
            // clearRecentFilesButton
            // 
            this.clearRecentFilesButton.AutoSize = true;
            this.clearRecentFilesButton.Location = new System.Drawing.Point( 394, 3 );
            this.clearRecentFilesButton.Name = "clearRecentFilesButton";
            this.clearRecentFilesButton.Size = new System.Drawing.Size( 103, 23 );
            this.clearRecentFilesButton.TabIndex = 1;
            this.clearRecentFilesButton.Text = "Clear Recent Files";
            this.clearRecentFilesButton.UseVisualStyleBackColor = true;
            this.clearRecentFilesButton.Click += new System.EventHandler( this.clearRecentFilesButton_Click );
            // 
            // lineSeparatorGroupBox
            // 
            this.lineSeparatorGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lineSeparatorGroupBox.Controls.Add( this.groupBox1 );
            this.lineSeparatorGroupBox.Location = new System.Drawing.Point( -6, 32 );
            this.lineSeparatorGroupBox.Name = "lineSeparatorGroupBox";
            this.lineSeparatorGroupBox.Size = new System.Drawing.Size( 517, 209 );
            this.lineSeparatorGroupBox.TabIndex = 2;
            this.lineSeparatorGroupBox.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point( 9, 19 );
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size( 491, 180 );
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            this.groupBox1.Visible = false;
            // 
            // FileTabSettingsHostControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.lineSeparatorGroupBox );
            this.Controls.Add( this.clearRecentFilesButton );
            this.Controls.Add( this.reloadFilesCheckBox );
            this.Name = "FileTabSettingsHostControl";
            this.Size = new System.Drawing.Size( 500, 234 );
            this.lineSeparatorGroupBox.ResumeLayout( false );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox reloadFilesCheckBox;
        private System.Windows.Forms.Button clearRecentFilesButton;
        private System.Windows.Forms.GroupBox lineSeparatorGroupBox;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
