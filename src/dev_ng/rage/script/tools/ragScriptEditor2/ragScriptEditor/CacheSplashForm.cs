//---------------------------------------------------------------------------------------------
// <copyright file="CacheSplashForm.cs" company="Rockstar Games">
//     Copyright � Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace ragScriptEditor
{
    using System;
    using System.IO;
    using System.Windows.Forms;
    using ragScriptEditorShared;
    using SanScriptParser;

    public partial class CacheSplashForm : Form
    {
        public CacheSplashForm(String projectPath )
        {
            this.InitializeComponent();
            
            string projectFilename = Path.GetFileName(projectPath);
            string projectBranch = ApplicationSettings.Environment.Subst("$(branch)");
            this.Text = $"Loading {projectFilename} ({projectBranch})...";
        }

        #region Variables
        private bool _calledStart = false;
        #endregion

        #region Events
        public event EventHandler Start;
        public event EventHandler Stop;
        #endregion

        #region Event Dispatchers
        private void OnStart()
        {
            this.Start?.Invoke( this, EventArgs.Empty );
        }

        private void OnStop()
        {
            this.Stop?.Invoke( this, EventArgs.Empty );
        }
        #endregion

        #region Event Handlers
        private void CacheSplashForm_Load( object sender, EventArgs e )
        {
            this.timer.Start();
        }

        private void CacheSplashForm_FormClosed( object sender, FormClosedEventArgs e )
        {
            this.OnStop();
        }

        private void timer_Tick( object sender, EventArgs e )
        {
            if ( !this._calledStart )
            {
                this.timer.Stop();
                this._calledStart = true;
                this.OnStart();
                this.timer.Start();
            }

            ParseCoordinator.Stats stats = SanScriptProjectResolver.ParseCoordinator.Statistics;

            this.remainingOverTotalLabel.Text = $"{stats.NumProcessedFiles} / {stats.TotalNumFiles}";
            this.progressBar.Style = stats.TotalNumFiles > 5 && stats.NumProcessedFiles > 0
                ? ProgressBarStyle.Continuous
                : ProgressBarStyle.Marquee;

            // NumProcessedFiles should not be larger than TotalNumFiles, but guard against an exception just in case.
            this.progressBar.Maximum = Math.Max(stats.NumProcessedFiles, stats.TotalNumFiles);
            this.progressBar.Value = stats.NumProcessedFiles;

            if (stats.NumPendingFiles <= 0)
            {
                this.timer.Stop();
                this.Close();
            }
        }
        #endregion
    }
}