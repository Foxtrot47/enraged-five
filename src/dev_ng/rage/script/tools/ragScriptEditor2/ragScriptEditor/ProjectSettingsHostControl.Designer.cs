namespace ragScriptEditor
{
    partial class ProjectSettingsHostControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lineSeparatorGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.reloadLastProjectCheckBox = new System.Windows.Forms.CheckBox();
            this.clearRecentProjectsButton = new System.Windows.Forms.Button();
            this.lineSeparatorGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // lineSeparatorGroupBox
            // 
            this.lineSeparatorGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lineSeparatorGroupBox.Controls.Add(this.groupBox1);
            this.lineSeparatorGroupBox.Location = new System.Drawing.Point(-10, 32);
            this.lineSeparatorGroupBox.Name = "lineSeparatorGroupBox";
            this.lineSeparatorGroupBox.Size = new System.Drawing.Size(522, 220);
            this.lineSeparatorGroupBox.TabIndex = 0;
            this.lineSeparatorGroupBox.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Location = new System.Drawing.Point(13, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(494, 186);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            this.groupBox1.Visible = false;
            // 
            // reloadLastProjectCheckBox
            // 
            this.reloadLastProjectCheckBox.AutoSize = true;
            this.reloadLastProjectCheckBox.Location = new System.Drawing.Point(3, 7);
            this.reloadLastProjectCheckBox.Name = "reloadLastProjectCheckBox";
            this.reloadLastProjectCheckBox.Size = new System.Drawing.Size(114, 17);
            this.reloadLastProjectCheckBox.TabIndex = 1;
            this.reloadLastProjectCheckBox.Text = "Reload last project";
            this.reloadLastProjectCheckBox.UseVisualStyleBackColor = true;
            this.reloadLastProjectCheckBox.CheckedChanged += new System.EventHandler(this.reloadLastProjectCheckBox_CheckedChanged);
            // 
            // clearRecentProjectsButton
            // 
            this.clearRecentProjectsButton.AutoSize = true;
            this.clearRecentProjectsButton.Location = new System.Drawing.Point(377, 3);
            this.clearRecentProjectsButton.Name = "clearRecentProjectsButton";
            this.clearRecentProjectsButton.Size = new System.Drawing.Size(120, 23);
            this.clearRecentProjectsButton.TabIndex = 3;
            this.clearRecentProjectsButton.Text = "Clear Recent Projects";
            this.clearRecentProjectsButton.UseVisualStyleBackColor = true;
            this.clearRecentProjectsButton.Click += new System.EventHandler(this.clearRecentProjectsButton_Click);
            // 
            // ProjectSettingsHostControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.clearRecentProjectsButton);
            this.Controls.Add(this.reloadLastProjectCheckBox);
            this.Controls.Add(this.lineSeparatorGroupBox);
            this.Name = "ProjectSettingsHostControl";
            this.Size = new System.Drawing.Size(500, 240);
            this.lineSeparatorGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox lineSeparatorGroupBox;
        private System.Windows.Forms.CheckBox reloadLastProjectCheckBox;
        private System.Windows.Forms.Button clearRecentProjectsButton;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
