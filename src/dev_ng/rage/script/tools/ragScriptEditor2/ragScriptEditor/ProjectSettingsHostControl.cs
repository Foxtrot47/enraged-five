using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Forms;
using ragScriptEditorShared;

namespace ragScriptEditor
{
    /// <summary>
    /// This class is hosts a ProjectTabSettingsControl, adding some editor-specific options relating to projects but that are
    /// not contained in the ProjectSettings class.
    /// </summary>
    public partial class ProjectSettingsHostControl : SettingsHostControlBase
    {
        public ProjectSettingsHostControl()
        {
            InitializeComponent();
        }

        #region Properties
        public bool ReloadLastProject
        {
            get
            {
                return this.reloadLastProjectCheckBox.Checked;
            }
            set
            {
                m_invokeSettingsChanged = false;
                this.reloadLastProjectCheckBox.Checked = value;
                m_invokeSettingsChanged = true;
            }
        }
        #endregion

        #region Overrides
        public override Control ControlToReplace
        {
            get
            {
                return this.groupBox1;
            }
        }
        #endregion

        #region Events
        public event BooleanValueChangedEventHandler ReloadLastProjectChanged;

        public event BooleanValueChangedEventHandler CloseProjectFilesChanged;

        public event EventHandler ClearRecentProjects;
        #endregion

        #region Event Dispatchers
        private void OnReloadLastProjectChanged( bool newValue )
        {
            if ( m_invokeSettingsChanged && (this.ReloadLastProjectChanged != null) )
            {
                this.ReloadLastProjectChanged( this, new BooleanValueChangedEventArgs( newValue ) );
            }
        }

        private void OnCloseProjectFilesChanged( bool newValue )
        {
            if ( m_invokeSettingsChanged && (this.CloseProjectFilesChanged != null) )
            {
                this.CloseProjectFilesChanged( this, new BooleanValueChangedEventArgs( newValue ) );
            }
        }

        private void OnClearRecentProjects()
        {
            if ( this.ClearRecentProjects != null )
            {
                this.ClearRecentProjects( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Event Handlers
        private void reloadLastProjectCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            OnReloadLastProjectChanged( this.reloadLastProjectCheckBox.Checked );
            OnSettingsChanged();
        }

        private void clearRecentProjectsButton_Click( object sender, EventArgs e )
        {
            DialogResult result = rageMessageBox.ShowQuestion( this, "Are you sure you want to clear\nthe Recent Projects list?",
                "Clear Recent Projects" );
            if ( result == DialogResult.Yes )
            {
                OnClearRecentProjects();
            }
        }
        #endregion
    }
}
