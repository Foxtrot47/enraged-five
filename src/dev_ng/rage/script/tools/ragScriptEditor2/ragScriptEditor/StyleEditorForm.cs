using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ActiproSoftware.SyntaxEditor;
using ragScriptEditorShared;
using System.IO;

namespace ragScriptEditor
{
    public partial class StyleEditorForm : Form
    {
        public StyleEditorForm()
        {
            InitializeComponent();
        }

        public DialogResult ShowIt( Form masterForm, SyntaxEditor editor, Dictionary<string, LanguageHighlightingStyle> currentLangStyles, 
            ref Font font, ref Color textBackgroundColor )
        {
            m_editor = editor;
            m_currentLanguageHighlightingStyles = currentLangStyles;
            m_font = font;
            m_textBackgroundColor = textBackgroundColor;

            Init();

            DialogResult result = this.ShowDialog( masterForm );

            if ( this.StartPosition == FormStartPosition.CenterParent )
            {
                // so the next time we open the window, it will be in the same position as before
                this.StartPosition = FormStartPosition.Manual;
            }

            if ( result == DialogResult.OK )
            {
                font = m_font;
                textBackgroundColor = m_textBackgroundColor;
            }            

            return result;
        }

        #region Delegates
        public delegate void ApplyStylesEventHandler( object sender, StyleSettingsEventArgs e );
        #endregion

        #region Events
        public event ApplyStylesEventHandler ApplyStyles;
        #endregion

        #region Variables
        private SyntaxEditor m_editor;
        private bool m_ignoreUpdateRequest;
        private SyntaxLanguageCollection m_languages;
        private HighlightingStyle m_selectedHighlightingStyle;
        private Dictionary<string, LanguageHighlightingStyle> m_currentLanguageHighlightingStyles = null;
        private Dictionary<string, LanguageHighlightingStyle> m_languageHighlightingStyles = null;

        private Font m_font;
        private Color m_textBackgroundColor;
        #endregion

        #region Event Handlers
        private void languageComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            this.LoadLanguage( this.languageComboBox.SelectedIndex );
        }

        private void textBackgroundColorButton_ColorChanged( object sender, EventArgs e )
        {
            EnableApplyButton();
        }

        private void font_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( (this.fontDropDownList.SelectedItem != null) && (this.fontSizeComboBox.SelectedItem != null) )
            {
                textStylePreview.Font = new Font( ((FontDropDownList.FontFamilyData)this.fontDropDownList.SelectedItem).Name,
                    Convert.ToSingle( this.fontSizeComboBox.SelectedItem ) );

                EnableApplyButton();
            }
        }

        private void foreColorButton_ColorChanged( object sender, EventArgs e )
        {
            if ( UpdateHighlightingStyle() )
            {
                EnableApplyButton();
            }
        }

        private void backColorButton_ColorChanged( object sender, EventArgs e )
        {
            if ( UpdateHighlightingStyle() )
            {
                EnableApplyButton();
            }
        }

        private void boldCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( UpdateHighlightingStyle() )
            {
                EnableApplyButton();
            }
        }

        private void italicCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( UpdateHighlightingStyle() )
            {
                EnableApplyButton();
            }
        }

        private void underlineCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( UpdateHighlightingStyle() )
            {
                EnableApplyButton();
            }
        }

        private void okButton_Click( object sender, EventArgs e )
        {
            ApplyChanges();
        }

        private void applyButton_Click( object sender, EventArgs e )
        {
            ApplyChanges();
            this.applyButton.Enabled = false;
        }

        private void itemsListBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            this.UpdateHighlightingStyle();
        }
        #endregion

        #region Private Functions
        private void Init()
        {
            this.languageComboBox.Items.Clear();

            // copy the current styles
            m_languageHighlightingStyles = new Dictionary<string, LanguageHighlightingStyle>();
            if ( m_editor != null )
            {
                m_languages = SyntaxLanguage.GetLanguageCollection( m_editor.Document.Language );
                foreach ( SyntaxLanguage language in m_languages )
                {
                    LanguageHighlightingStyle langStyle = new LanguageHighlightingStyle();

                    langStyle.LanguageKey = language.Key;
                    foreach ( ActiproSoftware.SyntaxEditor.HighlightingStyle style in language.HighlightingStyles )
                    {
                        langStyle.HighlightingStyles.Add( style.Clone() );
                    }

                    m_languageHighlightingStyles.Add( langStyle.LanguageKey, langStyle );
                    this.languageComboBox.Items.Add( langStyle.LanguageKey );
                }
            }

            // look for additional ones
            foreach ( KeyValuePair<string, LanguageHighlightingStyle> pair in m_currentLanguageHighlightingStyles )
            {
                LanguageHighlightingStyle langStyle = new LanguageHighlightingStyle();

                langStyle.LanguageKey = pair.Value.LanguageKey;
                foreach ( ActiproSoftware.SyntaxEditor.HighlightingStyle style in pair.Value.HighlightingStyles )
                {
                    langStyle.HighlightingStyles.Add( style.Clone() );
                }

                if ( !m_languageHighlightingStyles.ContainsKey( langStyle.LanguageKey ) )
                {
                    m_languageHighlightingStyles.Add( langStyle.LanguageKey, langStyle );
                    this.languageComboBox.Items.Add( langStyle.LanguageKey );
                }
            }

            this.fontDropDownList.SelectedIndex = this.fontDropDownList.FindStringExact( m_font.Name );
            this.fontSizeComboBox.SelectedIndex = this.fontSizeComboBox.FindStringExact( Math.Round( m_font.SizeInPoints ).ToString() );
            this.textBackgroundColorButton.Color = m_textBackgroundColor;

            if ( this.languageComboBox.Items.Count > 0 )
            {
                this.languageComboBox.SelectedIndex = 0;
            }
            else
            {
                this.languageComboBox.Enabled = false;
                this.displayItemsGroupBox.Enabled = false;
            }

            // set back to false because changing the above items will enable it
            this.applyButton.Enabled = false;
        }

        private void EnableApplyButton()
        {
            this.applyButton.Enabled = true;
        }

        private void ApplyChanges()
        {
            // Loop through each of the editor's languages and update the highlighting styles
            if ( m_editor != null )
            {
                OnApplyStyles();
            }

            // Set as current settings
            m_currentLanguageHighlightingStyles.Clear();
            foreach ( KeyValuePair<string, LanguageHighlightingStyle> pair in m_languageHighlightingStyles )
            {
                m_currentLanguageHighlightingStyles.Add( pair.Key, pair.Value );
            }
            m_font = this.textStylePreview.Font;
            m_textBackgroundColor = textBackgroundColorButton.Color;
        }

        /// <summary>
        /// Loads the highlighting styles from the specified language into the m_editor.
        /// </summary>
        /// <param name="index">The index of the language to load.</param>
        private void LoadLanguage( int index )
        {
            m_selectedHighlightingStyle = null;

            string language = this.languageComboBox.SelectedItem as string;
            LanguageHighlightingStyle langStyle = m_languageHighlightingStyles[language];

            this.itemsListBox.Items.Clear();
            foreach ( HighlightingStyle style in langStyle.HighlightingStyles )
            {
                this.itemsListBox.Items.Add( style );
            }

            this.itemsListBox.SelectedIndex = 0;
        }

        /// <summary>
        /// Updates the selected style with the current settings and updates the controls.
        /// </summary>
        private bool UpdateHighlightingStyle()
        {
            if ( m_ignoreUpdateRequest )
            {
                return false;
            }

            // Update old selected style
            if ( m_selectedHighlightingStyle != null )
            {
                m_selectedHighlightingStyle.ForeColor = this.foreColorButton.Color;
                m_selectedHighlightingStyle.BackColor = this.backColorButton.Color;
                m_selectedHighlightingStyle.Bold = this.boldCheckBox.Checked
                    ? ActiproSoftware.ComponentModel.DefaultableBoolean.True : ActiproSoftware.ComponentModel.DefaultableBoolean.False;
                m_selectedHighlightingStyle.Italic = this.italicCheckBox.Checked
                    ? ActiproSoftware.ComponentModel.DefaultableBoolean.True : ActiproSoftware.ComponentModel.DefaultableBoolean.False;
                m_selectedHighlightingStyle.UnderlineStyle = this.underlineCheckBox.Checked
                    ? ActiproSoftware.SyntaxEditor.HighlightingStyleLineStyle.Solid : ActiproSoftware.SyntaxEditor.HighlightingStyleLineStyle.Default;
            }

            if ( this.itemsListBox.SelectedIndex == -1 )
            {
                return false;
            }

            m_ignoreUpdateRequest = true;

            // Update controls
            if ( this.itemsListBox.SelectedItem != null )
            {
                m_selectedHighlightingStyle = this.itemsListBox.SelectedItem as HighlightingStyle;
                foreColorButton.Color = m_selectedHighlightingStyle.ForeColor;
                backColorButton.Color = m_selectedHighlightingStyle.BackColor;
                this.boldCheckBox.Checked
                    = (m_selectedHighlightingStyle.Bold == ActiproSoftware.ComponentModel.DefaultableBoolean.True) ? true : false;
                this.italicCheckBox.Checked
                    = (m_selectedHighlightingStyle.Italic == ActiproSoftware.ComponentModel.DefaultableBoolean.True) ? true : false;
                this.underlineCheckBox.Checked
                    = (m_selectedHighlightingStyle.UnderlineStyle == ActiproSoftware.SyntaxEditor.HighlightingStyleLineStyle.Default) ? false : true;

                // Update sample
                this.textStylePreview.HighlightingStyle = m_selectedHighlightingStyle;
            }

            m_ignoreUpdateRequest = false;
            return true;
        }

        private void OnApplyStyles()
        {
            if ( ApplyStyles != null )
            {
                StyleSettingsEventArgs e = new StyleSettingsEventArgs( m_editor, m_languageHighlightingStyles, 
                    this.textStylePreview.Font, textBackgroundColorButton.Color );
                ApplyStyles( this, e );
            }
        }
        #endregion

        private void exportButton_Click(object sender, EventArgs e)
        {
            string language = this.languageComboBox.SelectedItem as string;
            LanguageHighlightingStyle langStyle = m_languageHighlightingStyles[language];

            SettingsExporter.Export(language, langStyle);
        }
    }

    public class StyleSettingsEventArgs : EventArgs
    {
        public StyleSettingsEventArgs( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, Dictionary<string, LanguageHighlightingStyle> styles, 
            Font font, Color backgroundColor )
        {
            m_editor = editor;
            m_styles = styles;
            m_font = font;
            m_backgroundColor = backgroundColor;
        }

        #region Properties
        public ActiproSoftware.SyntaxEditor.SyntaxEditor Editor
        {
            get
            {
                return m_editor;
            }
        }

        public Dictionary<string, LanguageHighlightingStyle> Styles
        {
            get
            {
                return m_styles;
            }
        }

        public Font Font
        {
            get
            {
                return m_font;
            }
        }

        public Color TextBackgroundColor
        {
            get
            {
                return m_backgroundColor;
            }
        }
        #endregion

        #region Variables
        private ActiproSoftware.SyntaxEditor.SyntaxEditor m_editor;
        private Dictionary<string, LanguageHighlightingStyle> m_styles;
        private Font m_font;
        private Color m_backgroundColor;
        #endregion
    }
}