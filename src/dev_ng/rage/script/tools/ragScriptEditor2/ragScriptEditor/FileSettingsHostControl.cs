using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Command;
using RSG.Base.Forms;
using ragScriptEditorShared;

namespace ragScriptEditor
{
    /// <summary>
    /// This class is hosts a FileTabSettingsControl, adding some editor-specific options relating to files but that are
    /// not contained in the FileSettings class.
    /// </summary>
    public partial class FileSettingsHostControl : SettingsHostControlBase
    {
        public FileSettingsHostControl()
        {
            InitializeComponent();
        }

        #region Properties
        public bool ReloadFiles
        {
            get
            {
                return this.reloadFilesCheckBox.Checked;
            }
            set
            {
                m_invokeSettingsChanged = false;
                this.reloadFilesCheckBox.Checked = value;
                m_invokeSettingsChanged = true;
            }
        }
        #endregion

        #region Overrides
        public override Control ControlToReplace
        {
            get
            {
                return this.groupBox1;
            }
        }
        #endregion

        #region Events
        public event BooleanValueChangedEventHandler ReloadFilesChanged;

        public event EventHandler ClearRecentFiles;
        #endregion

        #region Event Dispatchers
        private void OnReloadFilesChanged( bool newValue )
        {
            if ( m_invokeSettingsChanged && (this.ReloadFilesChanged != null) )
            {
                this.ReloadFilesChanged( this, new BooleanValueChangedEventArgs( newValue ) );
            }
        }

        private void OnClearRecentFiles()
        {
            if ( this.ClearRecentFiles != null )
            {
                this.ClearRecentFiles( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Event Handlers
        private void reloadFilesCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            OnSettingsChanged();
        }

        private void clearRecentFilesButton_Click( object sender, EventArgs e )
        {
            DialogResult result = rageMessageBox.ShowQuestion( this, "Are you sure you want to clear\nthe Recent Files list?", 
                "Clear Recent Files" );
            if ( result == DialogResult.Yes )
            {
                OnClearRecentFiles();
            }
        }
        #endregion
    }
}
