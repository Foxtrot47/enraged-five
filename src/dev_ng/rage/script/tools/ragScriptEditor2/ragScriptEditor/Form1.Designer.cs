namespace ragScriptEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            TD.SandDock.DocumentContainer documentContainer1;
            TD.SandDock.DockContainer dockContainer1;
            TD.SandDock.DockContainer dockContainer2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabbedDocument1 = new TD.SandDock.TabbedDocument();
            this.sandDockManager1 = new TD.SandDock.SandDockManager();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.parseStatusToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.parseStatusToolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.cursorPositionToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.projectDockableWindow = new TD.SandDock.DockableWindow();
            this.documentOutlineDockableWindow = new TD.SandDock.DockableWindow();
            this.updateDocumentOutlineCheckBox = new System.Windows.Forms.CheckBox();
            this.documentOutlineTreeView = new System.Windows.Forms.TreeView();
            this.documentOutlineContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.documentOutlineCollapseAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.documentOutlineExpandAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputDockableWindow = new TD.SandDock.DockableWindow();
            this.outputRichTextBox = new System.Windows.Forms.RichTextBox();
            this.outputContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.goToErrorOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator28 = new System.Windows.Forms.ToolStripSeparator();
            this.copyOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearAllOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator29 = new System.Windows.Forms.ToolStripSeparator();
            this.findOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAgainDownOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAgainUpOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchHighlightedDownOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchHighlightedUpOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findResultsDockableWindow = new TD.SandDock.DockableWindow();
            this.findResultsRichTextBox = new System.Windows.Forms.RichTextBox();
            this.findResultsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.goToFindResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator31 = new System.Windows.Forms.ToolStripSeparator();
            this.copyfindResultsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllFindResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearAllFindResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator33 = new System.Windows.Forms.ToolStripSeparator();
            this.findFindResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAgainDownFindResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAgainUpFindResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchHighlightedDownFindResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchHighlightedUpFindResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.errorsDockableWindow = new TD.SandDock.DockableWindow();
            this.errorsListView = new System.Windows.Forms.ListView();
            this.errorDescriptionColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.errorFileColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.errorLineColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.errorsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyErrorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gotoErrorErrorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllErrorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.errorImageList = new System.Windows.Forms.ImageList(this.components);
            this.helpDockableWindow = new TD.SandDock.DockableWindow();
            this.helpRichTextBox = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.openProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pageSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.recentFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recentProjectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.indentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.backToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.forwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commentSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uncommentSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.makeUppercaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeLowercaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteHorizontalWhitespaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bookmarksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toggleBookmarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.previousBookmarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nextBookmarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearBookmarksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showLineNumbersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.insertCodeSnippetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewProjectExplorerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showProjectExplorerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.centerProjectExplorerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dockProjectExplorerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.floatProjectExplorerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewDocumentOutlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showDocumentOutlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.centerDocumentOutlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dockDocumentOutlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.floatDocumentOutlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.centerOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dockOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.floatOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewFindResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showFindResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.centerFindResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dockFindResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.floatFindResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewErrorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showErrorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.centerErrorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dockErrorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.floatErrorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.centerHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dockHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.floatHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compilingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compileProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recompileProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compileFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compileFileForPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopCompileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.gotoNextErrorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.saveBeforeCompileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dontSaveBeforeCompileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAllBeforeCompileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.startDebugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator30 = new System.Windows.Forms.ToolStripSeparator();
            this.incrediBuildToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buildProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rebuildProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cleanProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator32 = new System.Windows.Forms.ToolStripSeparator();
            this.stopBuildToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intellisenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshIntellisenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.intellisenseEnabledToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autocompleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autocompleteWithBracketsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator24 = new System.Windows.Forms.ToolStripSeparator();
            this.completeWordOnFirstMatchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.completeWordOnExactMatchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.completeWordDisabledToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.showItemsThatContainCurrentStringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addProjectFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addIncludePathToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectNoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator22 = new System.Windows.Forms.ToolStripSeparator();
            this.showConstantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showNativeTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showStructTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showEnumTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showEnumsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showStaticVariablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showSubroutinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showNativeSubroutinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showStatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.highlightEnumsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.highlightQuickInfoPopupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.highlightDisabledCodeBlocksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.useNewFindReplaceDialogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findReplaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.useNewFindReplaceInFilesDialogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findReplaceInFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gotoNextFindResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator34 = new System.Windows.Forms.ToolStripSeparator();
            this.enableWordWrappingInFindResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.incrementalSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reverseIncrementalSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
            this.goToLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goToDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goToProgramCounterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outliningToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startAutomaticOutliningToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startManualOutliningToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopOutliningToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.hideCurrentSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopHidingCurrentSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator21 = new System.Windows.Forms.ToolStripSeparator();
            this.toggleOutliningExpansionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toggleAllOutliningToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordTemporaryMacroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playTemporaryMacroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelRecordingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator23 = new System.Windows.Forms.ToolStripSeparator();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.highlightingStyleEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customButtonManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator27 = new System.Windows.Forms.ToolStripSeparator();
            this.viewLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitHorizontallyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitVerticallyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fourWaySplitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noSplitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator25 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.documentTabContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.fileNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openContainingFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator26 = new System.Windows.Forms.ToolStripSeparator();
            this.newTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeOtherTabsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeTabsToRightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllTabsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.activeFilesContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            documentContainer1 = new TD.SandDock.DocumentContainer();
            dockContainer1 = new TD.SandDock.DockContainer();
            dockContainer2 = new TD.SandDock.DockContainer();
            documentContainer1.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            dockContainer1.SuspendLayout();
            this.documentOutlineDockableWindow.SuspendLayout();
            this.documentOutlineContextMenuStrip.SuspendLayout();
            dockContainer2.SuspendLayout();
            this.outputDockableWindow.SuspendLayout();
            this.outputContextMenuStrip.SuspendLayout();
            this.findResultsDockableWindow.SuspendLayout();
            this.findResultsContextMenuStrip.SuspendLayout();
            this.errorsDockableWindow.SuspendLayout();
            this.errorsContextMenuStrip.SuspendLayout();
            this.helpDockableWindow.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.documentTabContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // documentContainer1
            // 
            documentContainer1.ContentSize = 471;
            documentContainer1.Controls.Add(this.tabbedDocument1);
            documentContainer1.LayoutSystem = new TD.SandDock.SplitLayoutSystem(new System.Drawing.SizeF(736F, 471F), System.Windows.Forms.Orientation.Horizontal, new TD.SandDock.LayoutSystemBase[] {
            ((TD.SandDock.LayoutSystemBase)(new TD.SandDock.DocumentLayoutSystem(new System.Drawing.SizeF(736F, 471F), new TD.SandDock.DockControl[] {
                        ((TD.SandDock.DockControl)(this.tabbedDocument1))}, this.tabbedDocument1)))});
            documentContainer1.Location = new System.Drawing.Point(254, 0);
            documentContainer1.Manager = this.sandDockManager1;
            documentContainer1.Name = "documentContainer1";
            documentContainer1.Size = new System.Drawing.Size(738, 471);
            documentContainer1.TabIndex = 3;
            // 
            // tabbedDocument1
            // 
            this.tabbedDocument1.FloatingSize = new System.Drawing.Size(550, 400);
            this.tabbedDocument1.Guid = new System.Guid("14365f66-26e7-4efb-b7a8-458deba520b1");
            this.tabbedDocument1.Location = new System.Drawing.Point(1, 21);
            this.tabbedDocument1.Name = "tabbedDocument1";
            this.tabbedDocument1.Size = new System.Drawing.Size(736, 449);
            this.tabbedDocument1.TabIndex = 0;
            this.tabbedDocument1.Text = "tabbedDocument1";
            // 
            // sandDockManager1
            // 
            this.sandDockManager1.DockSystemContainer = this.toolStripContainer1.ContentPanel;
            this.sandDockManager1.DocumentOverflow = TD.SandDock.DocumentOverflowMode.Menu;
            this.sandDockManager1.MaximumDockContainerSize = 1000;
            this.sandDockManager1.OwnerForm = this;
            this.sandDockManager1.SerializeTabbedDocuments = true;
            this.sandDockManager1.ShowControlContextMenu += new TD.SandDock.ShowControlContextMenuEventHandler(this.sandDockManager1_ShowControlContextMenu);
            this.sandDockManager1.ActiveTabbedDocumentChanged += new System.EventHandler(this.sandDockManager1_ActiveTabbedDocumentChanged);
            this.sandDockManager1.ShowActiveFilesList += new TD.SandDock.ActiveFilesListEventHandler(this.sandDockManager1_ShowActiveFilesList);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            this.toolStripContainer1.BottomToolStripPanel.Tag = "bottom";
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(documentContainer1);
            this.toolStripContainer1.ContentPanel.Controls.Add(dockContainer1);
            this.toolStripContainer1.ContentPanel.Controls.Add(dockContainer2);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(992, 725);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // toolStripContainer1.LeftToolStripPanel
            // 
            this.toolStripContainer1.LeftToolStripPanel.Tag = "left";
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            // 
            // toolStripContainer1.RightToolStripPanel
            // 
            this.toolStripContainer1.RightToolStripPanel.Tag = "right";
            this.toolStripContainer1.Size = new System.Drawing.Size(992, 773);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.menuStrip1);
            this.toolStripContainer1.TopToolStripPanel.Tag = "top";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusToolStripStatusLabel,
            this.parseStatusToolStripProgressBar,
            this.parseStatusToolStripStatusLabel,
            this.cursorPositionToolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(992, 24);
            this.statusStrip1.TabIndex = 0;
            // 
            // statusToolStripStatusLabel
            // 
            this.statusToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.statusToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedInner;
            this.statusToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.statusToolStripStatusLabel.Name = "statusToolStripStatusLabel";
            this.statusToolStripStatusLabel.Size = new System.Drawing.Size(870, 19);
            this.statusToolStripStatusLabel.Spring = true;
            this.statusToolStripStatusLabel.Text = "Ready";
            this.statusToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            //
            // parseStatusToolStripStatusLabel
            //
            this.parseStatusToolStripStatusLabel.AutoSize = false;
            this.parseStatusToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.parseStatusToolStripStatusLabel.Name = "parseStatusToolStripProgressBar";
            this.parseStatusToolStripStatusLabel.Size = new System.Drawing.Size(100, 19);
            this.parseStatusToolStripStatusLabel.Text = "Building cache...";
            this.parseStatusToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            //
            // parseStatusToolStripProgressBar
            //
            this.parseStatusToolStripProgressBar.AutoSize = false;
            this.parseStatusToolStripProgressBar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.parseStatusToolStripProgressBar.Name = "parseStatusToolStripProgressBar";
            this.parseStatusToolStripProgressBar.Size = new System.Drawing.Size(150, 19);
            this.parseStatusToolStripProgressBar.Visible = false;
            // 
            // cursorPositionToolStripStatusLabel
            // 
            this.cursorPositionToolStripStatusLabel.AutoSize = false;
            this.cursorPositionToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.cursorPositionToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedInner;
            this.cursorPositionToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cursorPositionToolStripStatusLabel.Name = "cursorPositionToolStripStatusLabel";
            this.cursorPositionToolStripStatusLabel.Size = new System.Drawing.Size(107, 19);
            this.cursorPositionToolStripStatusLabel.Text = "Ln 00000 Col 00000";
            this.cursorPositionToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dockContainer1
            // 
            dockContainer1.ContentSize = 250;
            dockContainer1.Controls.Add(this.projectDockableWindow);
            dockContainer1.Controls.Add(this.documentOutlineDockableWindow);
            dockContainer1.Dock = System.Windows.Forms.DockStyle.Left;
            dockContainer1.LayoutSystem = new TD.SandDock.SplitLayoutSystem(new System.Drawing.SizeF(250F, 473F), System.Windows.Forms.Orientation.Horizontal, new TD.SandDock.LayoutSystemBase[] {
            ((TD.SandDock.LayoutSystemBase)(new TD.SandDock.ControlLayoutSystem(new System.Drawing.SizeF(250F, 473F), new TD.SandDock.DockControl[] {
                        ((TD.SandDock.DockControl)(this.projectDockableWindow)),
                        ((TD.SandDock.DockControl)(this.documentOutlineDockableWindow))}, this.projectDockableWindow)))});
            dockContainer1.Location = new System.Drawing.Point(0, 0);
            dockContainer1.Manager = this.sandDockManager1;
            dockContainer1.Name = "dockContainer1";
            dockContainer1.Size = new System.Drawing.Size(254, 471);
            dockContainer1.TabIndex = 2;
            // 
            // projectDockableWindow
            // 
            this.projectDockableWindow.Guid = new System.Guid("d96b9cdc-50b1-4c80-8d28-7b41bdd2d710");
            this.projectDockableWindow.Location = new System.Drawing.Point(0, 23);
            this.projectDockableWindow.Name = "projectDockableWindow";
            this.projectDockableWindow.ShowOptions = false;
            this.projectDockableWindow.Size = new System.Drawing.Size(250, 424);
            this.projectDockableWindow.TabImage = global::ragScriptEditor.Properties.Resources.project;
            this.projectDockableWindow.TabIndex = 0;
            this.projectDockableWindow.Text = "Project Explorer";
            // 
            // documentOutlineDockableWindow
            // 
            this.documentOutlineDockableWindow.Controls.Add(this.updateDocumentOutlineCheckBox);
            this.documentOutlineDockableWindow.Controls.Add(this.documentOutlineTreeView);
            this.documentOutlineDockableWindow.Guid = new System.Guid("1618b199-387a-4f6b-9b09-6ccb7bfc86ae");
            this.documentOutlineDockableWindow.Location = new System.Drawing.Point(0, 0);
            this.documentOutlineDockableWindow.Name = "documentOutlineDockableWindow";
            this.documentOutlineDockableWindow.ShowOptions = false;
            this.documentOutlineDockableWindow.Size = new System.Drawing.Size(250, 433);
            this.documentOutlineDockableWindow.TabImage = global::ragScriptEditor.Properties.Resources.outline;
            this.documentOutlineDockableWindow.TabIndex = 0;
            this.documentOutlineDockableWindow.Text = "Document Outline";
            // 
            // updateDocumentOutlineCheckBox
            // 
            this.updateDocumentOutlineCheckBox.AutoSize = true;
            this.updateDocumentOutlineCheckBox.Checked = true;
            this.updateDocumentOutlineCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.updateDocumentOutlineCheckBox.Location = new System.Drawing.Point(3, 5);
            this.updateDocumentOutlineCheckBox.Name = "updateDocumentOutlineCheckBox";
            this.updateDocumentOutlineCheckBox.Size = new System.Drawing.Size(149, 17);
            this.updateDocumentOutlineCheckBox.TabIndex = 1;
            this.updateDocumentOutlineCheckBox.Text = "Update Document Outline";
            this.updateDocumentOutlineCheckBox.UseVisualStyleBackColor = true;
            this.updateDocumentOutlineCheckBox.CheckedChanged += new System.EventHandler(this.updateDocumentOutlineCheckBox_CheckedChanged);
            // 
            // documentOutlineTreeView
            // 
            this.documentOutlineTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.documentOutlineTreeView.ContextMenuStrip = this.documentOutlineContextMenuStrip;
            this.documentOutlineTreeView.HideSelection = false;
            this.documentOutlineTreeView.Location = new System.Drawing.Point(0, 28);
            this.documentOutlineTreeView.Name = "documentOutlineTreeView";
            this.documentOutlineTreeView.Size = new System.Drawing.Size(250, 405);
            this.documentOutlineTreeView.TabIndex = 0;
            this.documentOutlineTreeView.DoubleClick += new System.EventHandler(this.documentOutlineTreeView_DoubleClick);
            // 
            // documentOutlineContextMenuStrip
            // 
            this.documentOutlineContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.documentOutlineCollapseAllToolStripMenuItem,
            this.documentOutlineExpandAllToolStripMenuItem});
            this.documentOutlineContextMenuStrip.Name = "documentOutlineContextMenuStrip";
            this.documentOutlineContextMenuStrip.ShowImageMargin = false;
            this.documentOutlineContextMenuStrip.Size = new System.Drawing.Size(112, 48);
            this.documentOutlineContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.documentOutlineContextMenuStrip_Opening);
            // 
            // documentOutlineCollapseAllToolStripMenuItem
            // 
            this.documentOutlineCollapseAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.documentOutlineCollapseAllToolStripMenuItem.Name = "documentOutlineCollapseAllToolStripMenuItem";
            this.documentOutlineCollapseAllToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.documentOutlineCollapseAllToolStripMenuItem.Text = "&Collapse All";
            this.documentOutlineCollapseAllToolStripMenuItem.Click += new System.EventHandler(this.documentOutlineCollapseAllToolStripMenuItem_Click);
            // 
            // documentOutlineExpandAllToolStripMenuItem
            // 
            this.documentOutlineExpandAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.documentOutlineExpandAllToolStripMenuItem.Name = "documentOutlineExpandAllToolStripMenuItem";
            this.documentOutlineExpandAllToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.documentOutlineExpandAllToolStripMenuItem.Text = "&Expand All";
            this.documentOutlineExpandAllToolStripMenuItem.Click += new System.EventHandler(this.documentOutlineExpandAllToolStripMenuItem_Click);
            // 
            // dockContainer2
            // 
            dockContainer2.ContentSize = 250;
            dockContainer2.Controls.Add(this.outputDockableWindow);
            dockContainer2.Controls.Add(this.findResultsDockableWindow);
            dockContainer2.Controls.Add(this.errorsDockableWindow);
            dockContainer2.Controls.Add(this.helpDockableWindow);
            dockContainer2.Dock = System.Windows.Forms.DockStyle.Bottom;
            dockContainer2.LayoutSystem = new TD.SandDock.SplitLayoutSystem(new System.Drawing.SizeF(992F, 250F), System.Windows.Forms.Orientation.Vertical, new TD.SandDock.LayoutSystemBase[] {
            ((TD.SandDock.LayoutSystemBase)(new TD.SandDock.ControlLayoutSystem(new System.Drawing.SizeF(992F, 250F), new TD.SandDock.DockControl[] {
                        ((TD.SandDock.DockControl)(this.outputDockableWindow)),
                        ((TD.SandDock.DockControl)(this.findResultsDockableWindow)),
                        ((TD.SandDock.DockControl)(this.errorsDockableWindow)),
                        ((TD.SandDock.DockControl)(this.helpDockableWindow))}, this.findResultsDockableWindow)))});
            dockContainer2.Location = new System.Drawing.Point(0, 471);
            dockContainer2.Manager = this.sandDockManager1;
            dockContainer2.Name = "dockContainer2";
            dockContainer2.Size = new System.Drawing.Size(992, 254);
            dockContainer2.TabIndex = 1;
            // 
            // outputDockableWindow
            // 
            this.outputDockableWindow.Controls.Add(this.outputRichTextBox);
            this.outputDockableWindow.Guid = new System.Guid("9355fa8e-b46e-4c6e-906a-67399d7afab9");
            this.outputDockableWindow.Location = new System.Drawing.Point(0, 0);
            this.outputDockableWindow.Name = "outputDockableWindow";
            this.outputDockableWindow.ShowOptions = false;
            this.outputDockableWindow.Size = new System.Drawing.Size(992, 208);
            this.outputDockableWindow.TabImage = global::ragScriptEditor.Properties.Resources.output;
            this.outputDockableWindow.TabIndex = 0;
            this.outputDockableWindow.Text = "Output";
            // 
            // outputRichTextBox
            // 
            this.outputRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.outputRichTextBox.ContextMenuStrip = this.outputContextMenuStrip;
            this.outputRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.outputRichTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputRichTextBox.HideSelection = false;
            this.outputRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.outputRichTextBox.Name = "outputRichTextBox";
            this.outputRichTextBox.ReadOnly = true;
            this.outputRichTextBox.Size = new System.Drawing.Size(992, 208);
            this.outputRichTextBox.TabIndex = 0;
            this.outputRichTextBox.Text = "";
            this.outputRichTextBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.outputRichTextBox_MouseDoubleClick);
            this.outputRichTextBox.MouseEnter += new System.EventHandler(this.outputRichTextBox_MouseEnter);
            // 
            // outputContextMenuStrip
            // 
            this.outputContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goToErrorOutputToolStripMenuItem,
            this.toolStripSeparator28,
            this.copyOutputToolStripMenuItem,
            this.selectAllOutputToolStripMenuItem,
            this.clearAllOutputToolStripMenuItem,
            this.toolStripSeparator29,
            this.findOutputToolStripMenuItem,
            this.searchAgainDownOutputToolStripMenuItem,
            this.searchAgainUpOutputToolStripMenuItem,
            this.searchHighlightedDownOutputToolStripMenuItem,
            this.searchHighlightedUpOutputToolStripMenuItem});
            this.outputContextMenuStrip.Name = "outputContextMenuStrip";
            this.outputContextMenuStrip.ShowImageMargin = false;
            this.outputContextMenuStrip.Size = new System.Drawing.Size(247, 214);
            // 
            // goToErrorOutputToolStripMenuItem
            // 
            this.goToErrorOutputToolStripMenuItem.Name = "goToErrorOutputToolStripMenuItem";
            this.goToErrorOutputToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.goToErrorOutputToolStripMenuItem.Text = "&Go To Error/Tag";
            this.goToErrorOutputToolStripMenuItem.Click += new System.EventHandler(this.goToErrorOutputToolStripMenuItem_Click);
            // 
            // toolStripSeparator28
            // 
            this.toolStripSeparator28.Name = "toolStripSeparator28";
            this.toolStripSeparator28.Size = new System.Drawing.Size(243, 6);
            // 
            // copyOutputToolStripMenuItem
            // 
            this.copyOutputToolStripMenuItem.Name = "copyOutputToolStripMenuItem";
            this.copyOutputToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyOutputToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.copyOutputToolStripMenuItem.Text = "&Copy";
            this.copyOutputToolStripMenuItem.Click += new System.EventHandler(this.copyOutputToolStripMenuItem_Click);
            // 
            // selectAllOutputToolStripMenuItem
            // 
            this.selectAllOutputToolStripMenuItem.Name = "selectAllOutputToolStripMenuItem";
            this.selectAllOutputToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllOutputToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.selectAllOutputToolStripMenuItem.Text = "&Select All";
            this.selectAllOutputToolStripMenuItem.Visible = false;
            this.selectAllOutputToolStripMenuItem.Click += new System.EventHandler(this.selectAllOutputToolStripMenuItem_Click);
            // 
            // clearAllOutputToolStripMenuItem
            // 
            this.clearAllOutputToolStripMenuItem.Name = "clearAllOutputToolStripMenuItem";
            this.clearAllOutputToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.clearAllOutputToolStripMenuItem.Text = "Clear &All";
            this.clearAllOutputToolStripMenuItem.Click += new System.EventHandler(this.clearAllOutputToolStripMenuItem_Click);
            // 
            // toolStripSeparator29
            // 
            this.toolStripSeparator29.Name = "toolStripSeparator29";
            this.toolStripSeparator29.Size = new System.Drawing.Size(243, 6);
            // 
            // findOutputToolStripMenuItem
            // 
            this.findOutputToolStripMenuItem.Name = "findOutputToolStripMenuItem";
            this.findOutputToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.findOutputToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.findOutputToolStripMenuItem.Text = "&Find";
            this.findOutputToolStripMenuItem.Click += new System.EventHandler(this.findOutputToolStripMenuItem_Click);
            // 
            // searchAgainDownOutputToolStripMenuItem
            // 
            this.searchAgainDownOutputToolStripMenuItem.Name = "searchAgainDownOutputToolStripMenuItem";
            this.searchAgainDownOutputToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.searchAgainDownOutputToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.searchAgainDownOutputToolStripMenuItem.Text = "Search Again Down";
            this.searchAgainDownOutputToolStripMenuItem.Visible = false;
            this.searchAgainDownOutputToolStripMenuItem.Click += new System.EventHandler(this.searchAgainDownOutputToolStripMenuItem_Click);
            // 
            // searchAgainUpOutputToolStripMenuItem
            // 
            this.searchAgainUpOutputToolStripMenuItem.Name = "searchAgainUpOutputToolStripMenuItem";
            this.searchAgainUpOutputToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F3)));
            this.searchAgainUpOutputToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.searchAgainUpOutputToolStripMenuItem.Text = "Search Again Up";
            this.searchAgainUpOutputToolStripMenuItem.Visible = false;
            this.searchAgainUpOutputToolStripMenuItem.Click += new System.EventHandler(this.searchAgainUpOutputToolStripMenuItem_Click);
            // 
            // searchHighlightedDownOutputToolStripMenuItem
            // 
            this.searchHighlightedDownOutputToolStripMenuItem.Name = "searchHighlightedDownOutputToolStripMenuItem";
            this.searchHighlightedDownOutputToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F3)));
            this.searchHighlightedDownOutputToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.searchHighlightedDownOutputToolStripMenuItem.Text = "Search Highlighted Down";
            this.searchHighlightedDownOutputToolStripMenuItem.Visible = false;
            this.searchHighlightedDownOutputToolStripMenuItem.Click += new System.EventHandler(this.searchHighlightedDownOutputToolStripMenuItem_Click);
            // 
            // searchHighlightedUpOutputToolStripMenuItem
            // 
            this.searchHighlightedUpOutputToolStripMenuItem.Name = "searchHighlightedUpOutputToolStripMenuItem";
            this.searchHighlightedUpOutputToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F3)));
            this.searchHighlightedUpOutputToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.searchHighlightedUpOutputToolStripMenuItem.Text = "Search Highlighted Up";
            this.searchHighlightedUpOutputToolStripMenuItem.Visible = false;
            this.searchHighlightedUpOutputToolStripMenuItem.Click += new System.EventHandler(this.searchHighlightedUpOutputToolStripMenuItem_Click);
            // 
            // findResultsDockableWindow
            // 
            this.findResultsDockableWindow.Controls.Add(this.findResultsRichTextBox);
            this.findResultsDockableWindow.Guid = new System.Guid("f05d6b83-46a5-41dc-b512-1879442b15db");
            this.findResultsDockableWindow.Location = new System.Drawing.Point(0, 27);
            this.findResultsDockableWindow.Name = "findResultsDockableWindow";
            this.findResultsDockableWindow.ShowOptions = false;
            this.findResultsDockableWindow.Size = new System.Drawing.Size(992, 203);
            this.findResultsDockableWindow.TabImage = global::ragScriptEditor.Properties.Resources.findResults;
            this.findResultsDockableWindow.TabIndex = 0;
            this.findResultsDockableWindow.Text = "Find Results";
            // 
            // findResultsRichTextBox
            // 
            this.findResultsRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.findResultsRichTextBox.ContextMenuStrip = this.findResultsContextMenuStrip;
            this.findResultsRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.findResultsRichTextBox.HideSelection = false;
            this.findResultsRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.findResultsRichTextBox.Name = "findResultsRichTextBox";
            this.findResultsRichTextBox.ReadOnly = true;
            this.findResultsRichTextBox.Size = new System.Drawing.Size(992, 203);
            this.findResultsRichTextBox.TabIndex = 0;
            this.findResultsRichTextBox.Text = "";
            this.findResultsRichTextBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.findResultsRichTextBox_MouseDoubleClick);
            this.findResultsRichTextBox.MouseEnter += new System.EventHandler(this.findResultsRichTextBox_MouseEnter);
            // 
            // findResultsContextMenuStrip
            // 
            this.findResultsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goToFindResultToolStripMenuItem,
            this.toolStripSeparator31,
            this.copyfindResultsToolStripMenuItem1,
            this.selectAllFindResultsToolStripMenuItem,
            this.clearAllFindResultsToolStripMenuItem,
            this.toolStripSeparator33,
            this.findFindResultsToolStripMenuItem,
            this.searchAgainDownFindResultsToolStripMenuItem,
            this.searchAgainUpFindResultsToolStripMenuItem,
            this.searchHighlightedDownFindResultsToolStripMenuItem,
            this.searchHighlightedUpFindResultsToolStripMenuItem});
            this.findResultsContextMenuStrip.Name = "contextMenuStrip2";
            this.findResultsContextMenuStrip.ShowImageMargin = false;
            this.findResultsContextMenuStrip.Size = new System.Drawing.Size(247, 214);
            // 
            // goToFindResultToolStripMenuItem
            // 
            this.goToFindResultToolStripMenuItem.Name = "goToFindResultToolStripMenuItem";
            this.goToFindResultToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.goToFindResultToolStripMenuItem.Text = "Go To Error/Tag";
            this.goToFindResultToolStripMenuItem.Click += new System.EventHandler(this.goToFindResultToolStripMenuItem_Click);
            // 
            // toolStripSeparator31
            // 
            this.toolStripSeparator31.Name = "toolStripSeparator31";
            this.toolStripSeparator31.Size = new System.Drawing.Size(243, 6);
            // 
            // copyfindResultsToolStripMenuItem1
            // 
            this.copyfindResultsToolStripMenuItem1.Name = "copyfindResultsToolStripMenuItem1";
            this.copyfindResultsToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyfindResultsToolStripMenuItem1.Size = new System.Drawing.Size(246, 22);
            this.copyfindResultsToolStripMenuItem1.Text = "Copy";
            this.copyfindResultsToolStripMenuItem1.Click += new System.EventHandler(this.copyfindResultsToolStripMenuItem1_Click);
            // 
            // selectAllFindResultsToolStripMenuItem
            // 
            this.selectAllFindResultsToolStripMenuItem.Name = "selectAllFindResultsToolStripMenuItem";
            this.selectAllFindResultsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllFindResultsToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.selectAllFindResultsToolStripMenuItem.Text = "Select All";
            this.selectAllFindResultsToolStripMenuItem.Visible = false;
            this.selectAllFindResultsToolStripMenuItem.Click += new System.EventHandler(this.selectAllFindResultsToolStripMenuItem_Click);
            // 
            // clearAllFindResultsToolStripMenuItem
            // 
            this.clearAllFindResultsToolStripMenuItem.Name = "clearAllFindResultsToolStripMenuItem";
            this.clearAllFindResultsToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.clearAllFindResultsToolStripMenuItem.Text = "Clear All";
            this.clearAllFindResultsToolStripMenuItem.Click += new System.EventHandler(this.clearAllFindResultsToolStripMenuItem_Click);
            // 
            // toolStripSeparator33
            // 
            this.toolStripSeparator33.Name = "toolStripSeparator33";
            this.toolStripSeparator33.Size = new System.Drawing.Size(243, 6);
            // 
            // findFindResultsToolStripMenuItem
            // 
            this.findFindResultsToolStripMenuItem.Name = "findFindResultsToolStripMenuItem";
            this.findFindResultsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.findFindResultsToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.findFindResultsToolStripMenuItem.Text = "&Find";
            this.findFindResultsToolStripMenuItem.Click += new System.EventHandler(this.findFindResultsToolStripMenuItem_Click);
            // 
            // searchAgainDownFindResultsToolStripMenuItem
            // 
            this.searchAgainDownFindResultsToolStripMenuItem.Name = "searchAgainDownFindResultsToolStripMenuItem";
            this.searchAgainDownFindResultsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.searchAgainDownFindResultsToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.searchAgainDownFindResultsToolStripMenuItem.Text = "Search Again Down";
            this.searchAgainDownFindResultsToolStripMenuItem.Visible = false;
            this.searchAgainDownFindResultsToolStripMenuItem.Click += new System.EventHandler(this.searchAgainDownFindResultsToolStripMenuItem_Click);
            // 
            // searchAgainUpFindResultsToolStripMenuItem
            // 
            this.searchAgainUpFindResultsToolStripMenuItem.Name = "searchAgainUpFindResultsToolStripMenuItem";
            this.searchAgainUpFindResultsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F3)));
            this.searchAgainUpFindResultsToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.searchAgainUpFindResultsToolStripMenuItem.Text = "Search Again Up";
            this.searchAgainUpFindResultsToolStripMenuItem.Visible = false;
            this.searchAgainUpFindResultsToolStripMenuItem.Click += new System.EventHandler(this.searchAgainUpFindResultsToolStripMenuItem_Click);
            // 
            // searchHighlightedDownFindResultsToolStripMenuItem
            // 
            this.searchHighlightedDownFindResultsToolStripMenuItem.Name = "searchHighlightedDownFindResultsToolStripMenuItem";
            this.searchHighlightedDownFindResultsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F3)));
            this.searchHighlightedDownFindResultsToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.searchHighlightedDownFindResultsToolStripMenuItem.Text = "Search Highlighted Down";
            this.searchHighlightedDownFindResultsToolStripMenuItem.Visible = false;
            this.searchHighlightedDownFindResultsToolStripMenuItem.Click += new System.EventHandler(this.searchHighlightedDownFindResultsToolStripMenuItem_Click);
            // 
            // searchHighlightedUpFindResultsToolStripMenuItem
            // 
            this.searchHighlightedUpFindResultsToolStripMenuItem.Name = "searchHighlightedUpFindResultsToolStripMenuItem";
            this.searchHighlightedUpFindResultsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F3)));
            this.searchHighlightedUpFindResultsToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.searchHighlightedUpFindResultsToolStripMenuItem.Text = "Search Highlighted Up";
            this.searchHighlightedUpFindResultsToolStripMenuItem.Visible = false;
            this.searchHighlightedUpFindResultsToolStripMenuItem.Click += new System.EventHandler(this.searchHighlightedUpFindResultsToolStripMenuItem_Click);
            // 
            // errorsDockableWindow
            // 
            this.errorsDockableWindow.Controls.Add(this.errorsListView);
            this.errorsDockableWindow.Guid = new System.Guid("f588708d-76b0-480c-9595-9df2fc52396d");
            this.errorsDockableWindow.Location = new System.Drawing.Point(0, 0);
            this.errorsDockableWindow.Name = "errorsDockableWindow";
            this.errorsDockableWindow.ShowOptions = false;
            this.errorsDockableWindow.Size = new System.Drawing.Size(992, 208);
            this.errorsDockableWindow.TabImage = global::ragScriptEditor.Properties.Resources.stop;
            this.errorsDockableWindow.TabIndex = 0;
            this.errorsDockableWindow.Text = "Errors";
            // 
            // errorsListView
            // 
            this.errorsListView.Activation = System.Windows.Forms.ItemActivation.TwoClick;
            this.errorsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.errorDescriptionColumnHeader,
            this.errorFileColumnHeader,
            this.errorLineColumnHeader});
            this.errorsListView.ContextMenuStrip = this.errorsContextMenuStrip;
            this.errorsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.errorsListView.FullRowSelect = true;
            this.errorsListView.GridLines = true;
            this.errorsListView.HideSelection = false;
            this.errorsListView.Location = new System.Drawing.Point(0, 0);
            this.errorsListView.Name = "errorsListView";
            this.errorsListView.Size = new System.Drawing.Size(992, 208);
            this.errorsListView.SmallImageList = this.errorImageList;
            this.errorsListView.TabIndex = 0;
            this.errorsListView.UseCompatibleStateImageBehavior = false;
            this.errorsListView.View = System.Windows.Forms.View.Details;
            this.errorsListView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.errorsListView_MouseClick);
            this.errorsListView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.errorsListView_MouseDoubleClick);
            // 
            // errorDescriptionColumnHeader
            // 
            this.errorDescriptionColumnHeader.Text = "Description";
            this.errorDescriptionColumnHeader.Width = 547;
            // 
            // errorFileColumnHeader
            // 
            this.errorFileColumnHeader.Text = "File";
            this.errorFileColumnHeader.Width = 384;
            // 
            // errorLineColumnHeader
            // 
            this.errorLineColumnHeader.Text = "Line";
            this.errorLineColumnHeader.Width = 56;
            // 
            // errorsContextMenuStrip
            // 
            this.errorsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyErrorsToolStripMenuItem,
            this.gotoErrorErrorsToolStripMenuItem,
            this.selectAllErrorsToolStripMenuItem});
            this.errorsContextMenuStrip.Name = "errorsContextMenuStrip";
            this.errorsContextMenuStrip.ShowImageMargin = false;
            this.errorsContextMenuStrip.Size = new System.Drawing.Size(140, 70);
            this.errorsContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.errorsContextMenuStrip_Opening);
            // 
            // copyErrorsToolStripMenuItem
            // 
            this.copyErrorsToolStripMenuItem.Name = "copyErrorsToolStripMenuItem";
            this.copyErrorsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyErrorsToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.copyErrorsToolStripMenuItem.Text = "&Copy";
            this.copyErrorsToolStripMenuItem.Click += new System.EventHandler(this.copyErrorsToolStripMenuItem_Click);
            // 
            // gotoErrorErrorsToolStripMenuItem
            // 
            this.gotoErrorErrorsToolStripMenuItem.Name = "gotoErrorErrorsToolStripMenuItem";
            this.gotoErrorErrorsToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.gotoErrorErrorsToolStripMenuItem.Text = "&Go to Error/Tag";
            this.gotoErrorErrorsToolStripMenuItem.Click += new System.EventHandler(this.gotoErrorErrorsToolStripMenuItem_Click);
            // 
            // selectAllErrorsToolStripMenuItem
            // 
            this.selectAllErrorsToolStripMenuItem.Name = "selectAllErrorsToolStripMenuItem";
            this.selectAllErrorsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllErrorsToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.selectAllErrorsToolStripMenuItem.Text = "&Select All";
            this.selectAllErrorsToolStripMenuItem.Visible = false;
            this.selectAllErrorsToolStripMenuItem.Click += new System.EventHandler(this.selectAllErrorsToolStripMenuItem_Click);
            // 
            // errorImageList
            // 
            this.errorImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("errorImageList.ImageStream")));
            this.errorImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.errorImageList.Images.SetKeyName(0, "info.png");
            this.errorImageList.Images.SetKeyName(1, "warning.png");
            this.errorImageList.Images.SetKeyName(2, "error.png");
            // 
            // helpDockableWindow
            // 
            this.helpDockableWindow.Controls.Add(this.helpRichTextBox);
            this.helpDockableWindow.Guid = new System.Guid("59786554-df26-42b5-a3ab-7c7c26978409");
            this.helpDockableWindow.Location = new System.Drawing.Point(0, 0);
            this.helpDockableWindow.Name = "helpDockableWindow";
            this.helpDockableWindow.ShowOptions = false;
            this.helpDockableWindow.Size = new System.Drawing.Size(992, 210);
            this.helpDockableWindow.TabImage = global::ragScriptEditor.Properties.Resources.help;
            this.helpDockableWindow.TabIndex = 0;
            this.helpDockableWindow.Text = "Help";
            // 
            // helpRichTextBox
            // 
            this.helpRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.helpRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.helpRichTextBox.Name = "helpRichTextBox";
            this.helpRichTextBox.Size = new System.Drawing.Size(992, 210);
            this.helpRichTextBox.TabIndex = 0;
            this.helpRichTextBox.Text = "";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.compilingToolStripMenuItem,
            this.intellisenseToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.outliningToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.windowToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(992, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.closeFileToolStripMenuItem,
            this.closeAllFilesToolStripMenuItem,
            this.toolStripSeparator,
            this.openProjectToolStripMenuItem,
            this.closeProjectToolStripMenuItem,
            this.toolStripSeparator6,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.saveAllToolStripMenuItem,
            this.saveProjectToolStripMenuItem,
            this.toolStripSeparator1,
            this.printToolStripMenuItem,
            this.pageSetupToolStripMenuItem,
            this.printPreviewToolStripMenuItem,
            this.toolStripSeparator7,
            this.recentFilesToolStripMenuItem,
            this.recentProjectsToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            this.fileToolStripMenuItem.DropDownOpening += new System.EventHandler(this.fileToolStripMenuItem_DropDownOpening);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newFileToolStripMenuItem,
            this.newProjectToolStripMenuItem});
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.newToolStripMenuItem.Text = "&New";
            // 
            // newFileToolStripMenuItem
            // 
            this.newFileToolStripMenuItem.Name = "newFileToolStripMenuItem";
            this.newFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newFileToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.newFileToolStripMenuItem.Tag = "FileNew";
            this.newFileToolStripMenuItem.Text = "&File";
            this.newFileToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // newProjectToolStripMenuItem
            // 
            this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
            this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.newProjectToolStripMenuItem.Text = "&Project";
            this.newProjectToolStripMenuItem.Click += new System.EventHandler(this.newProjectToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.openToolStripMenuItem.Tag = "FileOpen";
            this.openToolStripMenuItem.Text = "&Open File...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // closeFileToolStripMenuItem
            // 
            this.closeFileToolStripMenuItem.Name = "closeFileToolStripMenuItem";
            this.closeFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.closeFileToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.closeFileToolStripMenuItem.Tag = "FileClose";
            this.closeFileToolStripMenuItem.Text = "&Close File";
            this.closeFileToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // closeAllFilesToolStripMenuItem
            // 
            this.closeAllFilesToolStripMenuItem.Name = "closeAllFilesToolStripMenuItem";
            this.closeAllFilesToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.closeAllFilesToolStripMenuItem.Tag = "FileCloseAll";
            this.closeAllFilesToolStripMenuItem.Text = "Close All F&iles";
            this.closeAllFilesToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(184, 6);
            // 
            // openProjectToolStripMenuItem
            // 
            this.openProjectToolStripMenuItem.Name = "openProjectToolStripMenuItem";
            this.openProjectToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.openProjectToolStripMenuItem.Tag = "FileOpenProject";
            this.openProjectToolStripMenuItem.Text = "Open P&roject";
            this.openProjectToolStripMenuItem.Click += new System.EventHandler(this.openProjectToolStripMenuItem_Click);
            // 
            // closeProjectToolStripMenuItem
            // 
            this.closeProjectToolStripMenuItem.Name = "closeProjectToolStripMenuItem";
            this.closeProjectToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.closeProjectToolStripMenuItem.Tag = "FileCloseProject";
            this.closeProjectToolStripMenuItem.Text = "Close Proj&ect";
            this.closeProjectToolStripMenuItem.Click += new System.EventHandler(this.closeProjectToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(184, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.saveToolStripMenuItem.Tag = "FileSave";
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.saveAsToolStripMenuItem.Tag = "FileSaveAs";
            this.saveAsToolStripMenuItem.Text = "Save &As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // saveAllToolStripMenuItem
            // 
            this.saveAllToolStripMenuItem.Name = "saveAllToolStripMenuItem";
            this.saveAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAllToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.saveAllToolStripMenuItem.Tag = "FileSaveAll";
            this.saveAllToolStripMenuItem.Text = "Save A&ll";
            this.saveAllToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // saveProjectToolStripMenuItem
            // 
            this.saveProjectToolStripMenuItem.Name = "saveProjectToolStripMenuItem";
            this.saveProjectToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.saveProjectToolStripMenuItem.Tag = "FileSaveProject";
            this.saveProjectToolStripMenuItem.Text = "Save Proje&ct";
            this.saveProjectToolStripMenuItem.Click += new System.EventHandler(this.saveProjectToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(184, 6);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.printToolStripMenuItem.Tag = "FilePrint";
            this.printToolStripMenuItem.Text = "&Print";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // pageSetupToolStripMenuItem
            // 
            this.pageSetupToolStripMenuItem.Name = "pageSetupToolStripMenuItem";
            this.pageSetupToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.pageSetupToolStripMenuItem.Tag = "FilePageSetup";
            this.pageSetupToolStripMenuItem.Text = "Page Set&up...";
            this.pageSetupToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // printPreviewToolStripMenuItem
            // 
            this.printPreviewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
            this.printPreviewToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.printPreviewToolStripMenuItem.Tag = "FilePrintPreview";
            this.printPreviewToolStripMenuItem.Text = "Print Pre&view";
            this.printPreviewToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(184, 6);
            // 
            // recentFilesToolStripMenuItem
            // 
            this.recentFilesToolStripMenuItem.Name = "recentFilesToolStripMenuItem";
            this.recentFilesToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.recentFilesToolStripMenuItem.Text = "Recent &Files";
            // 
            // recentProjectsToolStripMenuItem
            // 
            this.recentProjectsToolStripMenuItem.Name = "recentProjectsToolStripMenuItem";
            this.recentProjectsToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.recentProjectsToolStripMenuItem.Text = "Recent Pro&jects";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(184, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator3,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.toolStripSeparator4,
            this.indentToolStripMenuItem,
            this.outdentToolStripMenuItem,
            this.toolStripSeparator8,
            this.backToolStripMenuItem,
            this.forwardToolStripMenuItem,
            this.commentSelectionToolStripMenuItem,
            this.uncommentSelectionToolStripMenuItem,
            this.toolStripSeparator9,
            this.makeUppercaseToolStripMenuItem,
            this.makeLowercaseToolStripMenuItem,
            this.toolStripSeparator10,
            this.deleteHorizontalWhitespaceToolStripMenuItem,
            this.bookmarksToolStripMenuItem,
            this.showLineNumbersToolStripMenuItem,
            this.toolStripSeparator11,
            this.insertCodeSnippetToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            this.editToolStripMenuItem.DropDownOpening += new System.EventHandler(this.editToolStripMenuItem_DropDownOpening);
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Z";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.undoToolStripMenuItem.Tag = "EditUndo";
            this.undoToolStripMenuItem.Text = "&Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Y";
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.redoToolStripMenuItem.Tag = "EditRedo";
            this.redoToolStripMenuItem.Text = "&Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(267, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+X";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.cutToolStripMenuItem.Tag = "EditCut";
            this.cutToolStripMenuItem.Text = "Cu&t";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+C";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.copyToolStripMenuItem.Tag = "EditCopy";
            this.copyToolStripMenuItem.Text = "&Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+V";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.pasteToolStripMenuItem.Tag = "EditPaste";
            this.pasteToolStripMenuItem.Text = "&Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeyDisplayString = "Del";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.deleteToolStripMenuItem.Tag = "EditDelete";
            this.deleteToolStripMenuItem.Text = "&Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(267, 6);
            // 
            // indentToolStripMenuItem
            // 
            this.indentToolStripMenuItem.Name = "indentToolStripMenuItem";
            this.indentToolStripMenuItem.ShortcutKeyDisplayString = "Tab";
            this.indentToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.indentToolStripMenuItem.Tag = "EditIndent";
            this.indentToolStripMenuItem.Text = "&Indent";
            this.indentToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // outdentToolStripMenuItem
            // 
            this.outdentToolStripMenuItem.Name = "outdentToolStripMenuItem";
            this.outdentToolStripMenuItem.ShortcutKeyDisplayString = "Shift+Tab";
            this.outdentToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.outdentToolStripMenuItem.Tag = "EditOutdent";
            this.outdentToolStripMenuItem.Text = "&Outdent";
            this.outdentToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(267, 6);
            // 
            // commentSelectionToolStripMenuItem
            // 
            this.commentSelectionToolStripMenuItem.Name = "commentSelectionToolStripMenuItem";
            this.commentSelectionToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.C)));
            this.commentSelectionToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.commentSelectionToolStripMenuItem.Tag = "EditCommentSelection";
            this.commentSelectionToolStripMenuItem.Text = "Co&mment Selection";
            this.commentSelectionToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // uncommentSelectionToolStripMenuItem
            // 
            this.uncommentSelectionToolStripMenuItem.Name = "uncommentSelectionToolStripMenuItem";
            this.uncommentSelectionToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.N)));
            this.uncommentSelectionToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.uncommentSelectionToolStripMenuItem.Tag = "EditUncommentSelection";
            this.uncommentSelectionToolStripMenuItem.Text = "U&ncomment Selection";
            this.uncommentSelectionToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(267, 6);
            // 
            // makeUppercaseToolStripMenuItem
            // 
            this.makeUppercaseToolStripMenuItem.Name = "makeUppercaseToolStripMenuItem";
            this.makeUppercaseToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+U";
            this.makeUppercaseToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.makeUppercaseToolStripMenuItem.Tag = "EditMakeUppercase";
            this.makeUppercaseToolStripMenuItem.Text = "Ma&ke Uppercase";
            this.makeUppercaseToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // makeLowercaseToolStripMenuItem
            // 
            this.makeLowercaseToolStripMenuItem.Name = "makeLowercaseToolStripMenuItem";
            this.makeLowercaseToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+U";
            this.makeLowercaseToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.makeLowercaseToolStripMenuItem.Tag = "EditMakeLowercase";
            this.makeLowercaseToolStripMenuItem.Text = "Make &Lowercase";
            this.makeLowercaseToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(267, 6);
            // 
            // deleteHorizontalWhitespaceToolStripMenuItem
            // 
            this.deleteHorizontalWhitespaceToolStripMenuItem.Name = "deleteHorizontalWhitespaceToolStripMenuItem";
            this.deleteHorizontalWhitespaceToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.K)));
            this.deleteHorizontalWhitespaceToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.deleteHorizontalWhitespaceToolStripMenuItem.Tag = "EditDeleteHorizontalWhitespace";
            this.deleteHorizontalWhitespaceToolStripMenuItem.Text = "Delete &Horizontal Whitespace";
            this.deleteHorizontalWhitespaceToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // bookmarksToolStripMenuItem
            // 
            this.bookmarksToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toggleBookmarkToolStripMenuItem,
            this.previousBookmarkToolStripMenuItem,
            this.nextBookmarkToolStripMenuItem,
            this.clearBookmarksToolStripMenuItem});
            this.bookmarksToolStripMenuItem.Name = "bookmarksToolStripMenuItem";
            this.bookmarksToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.bookmarksToolStripMenuItem.Text = "&Bookmarks";
            // 
            // toggleBookmarkToolStripMenuItem
            // 
            this.toggleBookmarkToolStripMenuItem.Name = "toggleBookmarkToolStripMenuItem";
            this.toggleBookmarkToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F2)));
            this.toggleBookmarkToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.toggleBookmarkToolStripMenuItem.Tag = "EditToggleBookmark";
            this.toggleBookmarkToolStripMenuItem.Text = "&Toggle Bookmark";
            this.toggleBookmarkToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // previousBookmarkToolStripMenuItem
            // 
            this.previousBookmarkToolStripMenuItem.Name = "previousBookmarkToolStripMenuItem";
            this.previousBookmarkToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+,";
            this.previousBookmarkToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Oemcomma)));
            this.previousBookmarkToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.previousBookmarkToolStripMenuItem.Tag = "EditPreviousBookmark";
            this.previousBookmarkToolStripMenuItem.Text = "&Previous Bookmark";
            this.previousBookmarkToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // nextBookmarkToolStripMenuItem
            // 
            this.nextBookmarkToolStripMenuItem.Name = "nextBookmarkToolStripMenuItem";
            this.nextBookmarkToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+.";
            this.nextBookmarkToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.OemPeriod)));
            this.nextBookmarkToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.nextBookmarkToolStripMenuItem.Tag = "EditNextBookmark";
            this.nextBookmarkToolStripMenuItem.Text = "&Next Bookmark";
            this.nextBookmarkToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // clearBookmarksToolStripMenuItem
            // 
            this.clearBookmarksToolStripMenuItem.Name = "clearBookmarksToolStripMenuItem";
            this.clearBookmarksToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.L)));
            this.clearBookmarksToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.clearBookmarksToolStripMenuItem.Tag = "EditClearBookmarks";
            this.clearBookmarksToolStripMenuItem.Text = "&Clear Bookmarks";
            this.clearBookmarksToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // showLineNumbersToolStripMenuItem
            // 
            this.showLineNumbersToolStripMenuItem.CheckOnClick = true;
            this.showLineNumbersToolStripMenuItem.Name = "showLineNumbersToolStripMenuItem";
            this.showLineNumbersToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.showLineNumbersToolStripMenuItem.Text = "Sho&w Line Numbers";
            this.showLineNumbersToolStripMenuItem.Click += new System.EventHandler(this.showLineNumbersToolStripMenuItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(267, 6);
            // 
            // insertCodeSnippetToolStripMenuItem
            // 
            this.insertCodeSnippetToolStripMenuItem.Name = "insertCodeSnippetToolStripMenuItem";
            this.insertCodeSnippetToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.Insert)));
            this.insertCodeSnippetToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.insertCodeSnippetToolStripMenuItem.Tag = "EditInsertCodeSnippet";
            this.insertCodeSnippetToolStripMenuItem.Text = "Insert Code &Snippet...";
            this.insertCodeSnippetToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewProjectExplorerToolStripMenuItem,
            this.viewDocumentOutlineToolStripMenuItem,
            this.viewOutputToolStripMenuItem,
            this.viewFindResultsToolStripMenuItem,
            this.viewErrorsToolStripMenuItem,
            this.viewHelpToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "&View";
            // 
            // viewProjectExplorerToolStripMenuItem
            // 
            this.viewProjectExplorerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showProjectExplorerToolStripMenuItem,
            this.centerProjectExplorerToolStripMenuItem,
            this.dockProjectExplorerToolStripMenuItem,
            this.floatProjectExplorerToolStripMenuItem});
            this.viewProjectExplorerToolStripMenuItem.Image = global::ragScriptEditor.Properties.Resources.project;
            this.viewProjectExplorerToolStripMenuItem.Name = "viewProjectExplorerToolStripMenuItem";
            this.viewProjectExplorerToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.viewProjectExplorerToolStripMenuItem.Text = "&Project Explorer";
            // 
            // showProjectExplorerToolStripMenuItem
            // 
            this.showProjectExplorerToolStripMenuItem.Name = "showProjectExplorerToolStripMenuItem";
            this.showProjectExplorerToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.showProjectExplorerToolStripMenuItem.Text = "&Show";
            this.showProjectExplorerToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // centerProjectExplorerToolStripMenuItem
            // 
            this.centerProjectExplorerToolStripMenuItem.Name = "centerProjectExplorerToolStripMenuItem";
            this.centerProjectExplorerToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.centerProjectExplorerToolStripMenuItem.Text = "&Center";
            this.centerProjectExplorerToolStripMenuItem.Click += new System.EventHandler(this.centerToolStripMenuItem_Click);
            // 
            // dockProjectExplorerToolStripMenuItem
            // 
            this.dockProjectExplorerToolStripMenuItem.Name = "dockProjectExplorerToolStripMenuItem";
            this.dockProjectExplorerToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.dockProjectExplorerToolStripMenuItem.Text = "&Dock";
            this.dockProjectExplorerToolStripMenuItem.Click += new System.EventHandler(this.dockToolStripMenuItem_Click);
            // 
            // floatProjectExplorerToolStripMenuItem
            // 
            this.floatProjectExplorerToolStripMenuItem.Name = "floatProjectExplorerToolStripMenuItem";
            this.floatProjectExplorerToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.floatProjectExplorerToolStripMenuItem.Text = "&Float";
            this.floatProjectExplorerToolStripMenuItem.Click += new System.EventHandler(this.floatToolStripMenuItem_Click);
            // 
            // viewDocumentOutlineToolStripMenuItem
            // 
            this.viewDocumentOutlineToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showDocumentOutlineToolStripMenuItem,
            this.centerDocumentOutlineToolStripMenuItem,
            this.dockDocumentOutlineToolStripMenuItem,
            this.floatDocumentOutlineToolStripMenuItem});
            this.viewDocumentOutlineToolStripMenuItem.Image = global::ragScriptEditor.Properties.Resources.outline;
            this.viewDocumentOutlineToolStripMenuItem.Name = "viewDocumentOutlineToolStripMenuItem";
            this.viewDocumentOutlineToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.viewDocumentOutlineToolStripMenuItem.Text = "&Document Outline";
            // 
            // showDocumentOutlineToolStripMenuItem
            // 
            this.showDocumentOutlineToolStripMenuItem.Name = "showDocumentOutlineToolStripMenuItem";
            this.showDocumentOutlineToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.showDocumentOutlineToolStripMenuItem.Text = "&Show";
            this.showDocumentOutlineToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // centerDocumentOutlineToolStripMenuItem
            // 
            this.centerDocumentOutlineToolStripMenuItem.Name = "centerDocumentOutlineToolStripMenuItem";
            this.centerDocumentOutlineToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.centerDocumentOutlineToolStripMenuItem.Text = "&Center";
            this.centerDocumentOutlineToolStripMenuItem.Click += new System.EventHandler(this.centerToolStripMenuItem_Click);
            // 
            // dockDocumentOutlineToolStripMenuItem
            // 
            this.dockDocumentOutlineToolStripMenuItem.Name = "dockDocumentOutlineToolStripMenuItem";
            this.dockDocumentOutlineToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.dockDocumentOutlineToolStripMenuItem.Text = "&Dock";
            this.dockDocumentOutlineToolStripMenuItem.Click += new System.EventHandler(this.dockToolStripMenuItem_Click);
            // 
            // floatDocumentOutlineToolStripMenuItem
            // 
            this.floatDocumentOutlineToolStripMenuItem.Name = "floatDocumentOutlineToolStripMenuItem";
            this.floatDocumentOutlineToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.floatDocumentOutlineToolStripMenuItem.Text = "&Float";
            this.floatDocumentOutlineToolStripMenuItem.Click += new System.EventHandler(this.dockToolStripMenuItem_Click);
            // 
            // viewOutputToolStripMenuItem
            // 
            this.viewOutputToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showOutputToolStripMenuItem,
            this.centerOutputToolStripMenuItem,
            this.dockOutputToolStripMenuItem,
            this.floatOutputToolStripMenuItem});
            this.viewOutputToolStripMenuItem.Image = global::ragScriptEditor.Properties.Resources.output;
            this.viewOutputToolStripMenuItem.Name = "viewOutputToolStripMenuItem";
            this.viewOutputToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.viewOutputToolStripMenuItem.Text = "&Output";
            // 
            // showOutputToolStripMenuItem
            // 
            this.showOutputToolStripMenuItem.Name = "showOutputToolStripMenuItem";
            this.showOutputToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.showOutputToolStripMenuItem.Text = "&Show";
            this.showOutputToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // centerOutputToolStripMenuItem
            // 
            this.centerOutputToolStripMenuItem.Name = "centerOutputToolStripMenuItem";
            this.centerOutputToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.centerOutputToolStripMenuItem.Text = "&Center";
            this.centerOutputToolStripMenuItem.Click += new System.EventHandler(this.centerToolStripMenuItem_Click);
            // 
            // dockOutputToolStripMenuItem
            // 
            this.dockOutputToolStripMenuItem.Name = "dockOutputToolStripMenuItem";
            this.dockOutputToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.dockOutputToolStripMenuItem.Text = "&Dock";
            this.dockOutputToolStripMenuItem.Click += new System.EventHandler(this.dockToolStripMenuItem_Click);
            // 
            // floatOutputToolStripMenuItem
            // 
            this.floatOutputToolStripMenuItem.Name = "floatOutputToolStripMenuItem";
            this.floatOutputToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.floatOutputToolStripMenuItem.Text = "&Float";
            this.floatOutputToolStripMenuItem.Click += new System.EventHandler(this.floatToolStripMenuItem_Click);
            // 
            // viewFindResultsToolStripMenuItem
            // 
            this.viewFindResultsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showFindResultsToolStripMenuItem,
            this.centerFindResultsToolStripMenuItem,
            this.dockFindResultsToolStripMenuItem,
            this.floatFindResultsToolStripMenuItem});
            this.viewFindResultsToolStripMenuItem.Image = global::ragScriptEditor.Properties.Resources.findResults;
            this.viewFindResultsToolStripMenuItem.Name = "viewFindResultsToolStripMenuItem";
            this.viewFindResultsToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.viewFindResultsToolStripMenuItem.Text = "&Find Results";
            // 
            // showFindResultsToolStripMenuItem
            // 
            this.showFindResultsToolStripMenuItem.Name = "showFindResultsToolStripMenuItem";
            this.showFindResultsToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.showFindResultsToolStripMenuItem.Text = "&Show";
            this.showFindResultsToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // centerFindResultsToolStripMenuItem
            // 
            this.centerFindResultsToolStripMenuItem.Name = "centerFindResultsToolStripMenuItem";
            this.centerFindResultsToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.centerFindResultsToolStripMenuItem.Text = "&Center";
            this.centerFindResultsToolStripMenuItem.Click += new System.EventHandler(this.centerToolStripMenuItem_Click);
            // 
            // dockFindResultsToolStripMenuItem
            // 
            this.dockFindResultsToolStripMenuItem.Name = "dockFindResultsToolStripMenuItem";
            this.dockFindResultsToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.dockFindResultsToolStripMenuItem.Text = "&Dock";
            this.dockFindResultsToolStripMenuItem.Click += new System.EventHandler(this.dockToolStripMenuItem_Click);
            // 
            // floatFindResultsToolStripMenuItem
            // 
            this.floatFindResultsToolStripMenuItem.Name = "floatFindResultsToolStripMenuItem";
            this.floatFindResultsToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.floatFindResultsToolStripMenuItem.Text = "&Float";
            this.floatFindResultsToolStripMenuItem.Click += new System.EventHandler(this.floatToolStripMenuItem_Click);
            // 
            // viewErrorsToolStripMenuItem
            // 
            this.viewErrorsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showErrorsToolStripMenuItem,
            this.centerErrorsToolStripMenuItem,
            this.dockErrorsToolStripMenuItem,
            this.floatErrorsToolStripMenuItem});
            this.viewErrorsToolStripMenuItem.Image = global::ragScriptEditor.Properties.Resources.stop;
            this.viewErrorsToolStripMenuItem.Name = "viewErrorsToolStripMenuItem";
            this.viewErrorsToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.viewErrorsToolStripMenuItem.Text = "&Errors";
            // 
            // showErrorsToolStripMenuItem
            // 
            this.showErrorsToolStripMenuItem.Name = "showErrorsToolStripMenuItem";
            this.showErrorsToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.showErrorsToolStripMenuItem.Text = "&Show";
            this.showErrorsToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // centerErrorsToolStripMenuItem
            // 
            this.centerErrorsToolStripMenuItem.Name = "centerErrorsToolStripMenuItem";
            this.centerErrorsToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.centerErrorsToolStripMenuItem.Text = "&Center";
            this.centerErrorsToolStripMenuItem.Click += new System.EventHandler(this.centerToolStripMenuItem_Click);
            // 
            // dockErrorsToolStripMenuItem
            // 
            this.dockErrorsToolStripMenuItem.Name = "dockErrorsToolStripMenuItem";
            this.dockErrorsToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.dockErrorsToolStripMenuItem.Text = "&Dock";
            this.dockErrorsToolStripMenuItem.Click += new System.EventHandler(this.dockToolStripMenuItem_Click);
            // 
            // floatErrorsToolStripMenuItem
            // 
            this.floatErrorsToolStripMenuItem.Name = "floatErrorsToolStripMenuItem";
            this.floatErrorsToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.floatErrorsToolStripMenuItem.Text = "&Float";
            this.floatErrorsToolStripMenuItem.Click += new System.EventHandler(this.floatToolStripMenuItem_Click);
            // 
            // viewHelpToolStripMenuItem
            // 
            this.viewHelpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showHelpToolStripMenuItem,
            this.centerHelpToolStripMenuItem,
            this.dockHelpToolStripMenuItem,
            this.floatHelpToolStripMenuItem});
            this.viewHelpToolStripMenuItem.Image = global::ragScriptEditor.Properties.Resources.help;
            this.viewHelpToolStripMenuItem.Name = "viewHelpToolStripMenuItem";
            this.viewHelpToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.viewHelpToolStripMenuItem.Text = "&Help";
            // 
            // showHelpToolStripMenuItem
            // 
            this.showHelpToolStripMenuItem.Name = "showHelpToolStripMenuItem";
            this.showHelpToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.showHelpToolStripMenuItem.Text = "&Show";
            this.showHelpToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // centerHelpToolStripMenuItem
            // 
            this.centerHelpToolStripMenuItem.Name = "centerHelpToolStripMenuItem";
            this.centerHelpToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.centerHelpToolStripMenuItem.Text = "&Center";
            this.centerHelpToolStripMenuItem.Click += new System.EventHandler(this.centerToolStripMenuItem_Click);
            // 
            // dockHelpToolStripMenuItem
            // 
            this.dockHelpToolStripMenuItem.Name = "dockHelpToolStripMenuItem";
            this.dockHelpToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.dockHelpToolStripMenuItem.Text = "&Dock";
            this.dockHelpToolStripMenuItem.Click += new System.EventHandler(this.dockToolStripMenuItem_Click);
            // 
            // floatHelpToolStripMenuItem
            // 
            this.floatHelpToolStripMenuItem.Name = "floatHelpToolStripMenuItem";
            this.floatHelpToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.floatHelpToolStripMenuItem.Text = "&Float";
            this.floatHelpToolStripMenuItem.Click += new System.EventHandler(this.floatToolStripMenuItem_Click);
            // 
            // compilingToolStripMenuItem
            // 
            this.compilingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compileProjectToolStripMenuItem,
            this.recompileProjectToolStripMenuItem,
            this.compileFileToolStripMenuItem,
            this.compileFileForPreviewToolStripMenuItem,
            this.stopCompileToolStripMenuItem,
            this.toolStripSeparator14,
            this.gotoNextErrorToolStripMenuItem,
            this.toolStripSeparator12,
            this.saveBeforeCompileToolStripMenuItem,
            this.dontSaveBeforeCompileToolStripMenuItem,
            this.saveAllBeforeCompileToolStripMenuItem,
            this.toolStripSeparator13,
            this.startDebugToolStripMenuItem,
            this.toolStripSeparator30,
            this.incrediBuildToolStripMenuItem});
            this.compilingToolStripMenuItem.Name = "compilingToolStripMenuItem";
            this.compilingToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.compilingToolStripMenuItem.Text = "&Compiling";
            this.compilingToolStripMenuItem.DropDownOpening += new System.EventHandler(this.compilingToolStripMenuItem_DropDownOpening);
            // 
            // compileProjectToolStripMenuItem
            // 
            this.compileProjectToolStripMenuItem.Name = "compileProjectToolStripMenuItem";
            this.compileProjectToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.compileProjectToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.compileProjectToolStripMenuItem.Tag = "CompileProject";
            this.compileProjectToolStripMenuItem.Text = "Compile &Project";
            this.compileProjectToolStripMenuItem.Click += new System.EventHandler(this.compileProjectToolStripMenuItem_Click);
            // 
            // recompileProjectToolStripMenuItem
            // 
            this.recompileProjectToolStripMenuItem.Name = "recompileProjectToolStripMenuItem";
            this.recompileProjectToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F7)));
            this.recompileProjectToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.recompileProjectToolStripMenuItem.Tag = "RecompileProject";
            this.recompileProjectToolStripMenuItem.Text = "&Recompile Project";
            this.recompileProjectToolStripMenuItem.Click += new System.EventHandler(this.recompileProjectToolStripMenuItem_Click);
            // 
            // compileFileToolStripMenuItem
            // 
            this.compileFileToolStripMenuItem.Name = "compileFileToolStripMenuItem";
            this.compileFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F7)));
            this.compileFileToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.compileFileToolStripMenuItem.Tag = "CompileFile";
            this.compileFileToolStripMenuItem.Text = "&Compile File";
            this.compileFileToolStripMenuItem.Click += new System.EventHandler(this.compileFileToolStripMenuItem_Click);
            // 
            // compileFileForPreviewToolStripMenuItem
            // 
            this.compileFileForPreviewToolStripMenuItem.Name = "compileFileForPreviewToolStripMenuItem";
            this.compileFileForPreviewToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+F8";
            this.compileFileForPreviewToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F8)));
            this.compileFileForPreviewToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.compileFileForPreviewToolStripMenuItem.Tag = "CompileFileForPreview";
            this.compileFileForPreviewToolStripMenuItem.Text = "Compile File for Preview";
            this.compileFileForPreviewToolStripMenuItem.Click += new System.EventHandler(this.compileFileForPreviewToolStripMenuItem_Click);
            // 
            // stopCompileToolStripMenuItem
            // 
            this.stopCompileToolStripMenuItem.Name = "stopCompileToolStripMenuItem";
            this.stopCompileToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.stopCompileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F7)));
            this.stopCompileToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.stopCompileToolStripMenuItem.Tag = "StopCompile";
            this.stopCompileToolStripMenuItem.Text = "St&op Compile";
            this.stopCompileToolStripMenuItem.Click += new System.EventHandler(this.stopCompileToolStripMenuItem_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(245, 6);
            // 
            // gotoNextErrorToolStripMenuItem
            // 
            this.gotoNextErrorToolStripMenuItem.Enabled = false;
            this.gotoNextErrorToolStripMenuItem.Name = "gotoNextErrorToolStripMenuItem";
            this.gotoNextErrorToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.gotoNextErrorToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.gotoNextErrorToolStripMenuItem.Text = "Goto Next &Error";
            this.gotoNextErrorToolStripMenuItem.Click += new System.EventHandler(this.gotoNextErrorToolStripMenuItem_Click);
            // 
            // backToolStripMenuItem
            // 
            this.backToolStripMenuItem.Name = "backToolStripMenuItem";
            this.backToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.OemMinus));
            this.backToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+-";
            this.backToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.backToolStripMenuItem.Tag = "GoBack";
            this.backToolStripMenuItem.Text = "Go Back";
            this.backToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // forwardToolStripMenuItem
            // 
            this.forwardToolStripMenuItem.Name = "forwardToolStripMenuItem";
            this.forwardToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.OemMinus));
            this.forwardToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+-";
            this.forwardToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.forwardToolStripMenuItem.Tag = "GoForward";
            this.forwardToolStripMenuItem.Text = "Go Forward";
            this.forwardToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(245, 6);
            // 
            // saveBeforeCompileToolStripMenuItem
            // 
            this.saveBeforeCompileToolStripMenuItem.CheckOnClick = true;
            this.saveBeforeCompileToolStripMenuItem.Name = "saveBeforeCompileToolStripMenuItem";
            this.saveBeforeCompileToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.saveBeforeCompileToolStripMenuItem.Text = "&Save Before Compile";
            this.saveBeforeCompileToolStripMenuItem.Click += new System.EventHandler(this.saveBeforeCompileToolStripMenuItem_Click);
            // 
            // dontSaveBeforeCompileToolStripMenuItem
            // 
            this.dontSaveBeforeCompileToolStripMenuItem.CheckOnClick = true;
            this.dontSaveBeforeCompileToolStripMenuItem.Name = "dontSaveBeforeCompileToolStripMenuItem";
            this.dontSaveBeforeCompileToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.dontSaveBeforeCompileToolStripMenuItem.Text = "&Don\'t Save Before Compile";
            this.dontSaveBeforeCompileToolStripMenuItem.Click += new System.EventHandler(this.dontSaveBeforeCompileToolStripMenuItem_Click);
            // 
            // saveAllBeforeCompileToolStripMenuItem
            // 
            this.saveAllBeforeCompileToolStripMenuItem.Checked = true;
            this.saveAllBeforeCompileToolStripMenuItem.CheckOnClick = true;
            this.saveAllBeforeCompileToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.saveAllBeforeCompileToolStripMenuItem.Name = "saveAllBeforeCompileToolStripMenuItem";
            this.saveAllBeforeCompileToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.saveAllBeforeCompileToolStripMenuItem.Text = "Save &All Before Compile";
            this.saveAllBeforeCompileToolStripMenuItem.Click += new System.EventHandler(this.saveAllBeforeCompileToolStripMenuItem_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(245, 6);
            // 
            // startDebugToolStripMenuItem
            // 
            this.startDebugToolStripMenuItem.Name = "startDebugToolStripMenuItem";
            this.startDebugToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.startDebugToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.startDebugToolStripMenuItem.Text = "Start &Debug";
            this.startDebugToolStripMenuItem.Click += new System.EventHandler(this.startDebugToolStripMenuItem_Click);
            // 
            // toolStripSeparator30
            // 
            this.toolStripSeparator30.Name = "toolStripSeparator30";
            this.toolStripSeparator30.Size = new System.Drawing.Size(245, 6);
            // 
            // incrediBuildToolStripMenuItem
            // 
            this.incrediBuildToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buildProjectToolStripMenuItem,
            this.rebuildProjectToolStripMenuItem,
            this.cleanProjectToolStripMenuItem,
            this.toolStripSeparator32,
            this.stopBuildToolStripMenuItem});
            this.incrediBuildToolStripMenuItem.Name = "incrediBuildToolStripMenuItem";
            this.incrediBuildToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.incrediBuildToolStripMenuItem.Text = "IncrediBuild";
            // 
            // buildProjectToolStripMenuItem
            // 
            this.buildProjectToolStripMenuItem.Name = "buildProjectToolStripMenuItem";
            this.buildProjectToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.buildProjectToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.buildProjectToolStripMenuItem.Tag = "IncredibuildBuild";
            this.buildProjectToolStripMenuItem.Text = "Build Project";
            this.buildProjectToolStripMenuItem.Click += new System.EventHandler(this.buildProjectToolStripMenuItem_Click);
            // 
            // rebuildProjectToolStripMenuItem
            // 
            this.rebuildProjectToolStripMenuItem.Name = "rebuildProjectToolStripMenuItem";
            this.rebuildProjectToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F6)));
            this.rebuildProjectToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.rebuildProjectToolStripMenuItem.Tag = "IncredibuildRebuild";
            this.rebuildProjectToolStripMenuItem.Text = "Rebuild Project";
            this.rebuildProjectToolStripMenuItem.Click += new System.EventHandler(this.rebuildProjectToolStripMenuItem_Click);
            // 
            // cleanProjectToolStripMenuItem
            // 
            this.cleanProjectToolStripMenuItem.Name = "cleanProjectToolStripMenuItem";
            this.cleanProjectToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F9)));
            this.cleanProjectToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.cleanProjectToolStripMenuItem.Tag = "IncredibuildClean";
            this.cleanProjectToolStripMenuItem.Text = "Clean Project";
            this.cleanProjectToolStripMenuItem.Click += new System.EventHandler(this.cleanProjectToolStripMenuItem_Click);
            // 
            // toolStripSeparator32
            // 
            this.toolStripSeparator32.Name = "toolStripSeparator32";
            this.toolStripSeparator32.Size = new System.Drawing.Size(219, 6);
            // 
            // stopBuildToolStripMenuItem
            // 
            this.stopBuildToolStripMenuItem.Name = "stopBuildToolStripMenuItem";
            this.stopBuildToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F6)));
            this.stopBuildToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.stopBuildToolStripMenuItem.Tag = "IncredibuildStop";
            this.stopBuildToolStripMenuItem.Text = "Stop Build";
            this.stopBuildToolStripMenuItem.Click += new System.EventHandler(this.stopBuildToolStripMenuItem_Click);
            // 
            // intellisenseToolStripMenuItem
            // 
            this.intellisenseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshIntellisenseToolStripMenuItem,
            this.toolStripSeparator15,
            this.intellisenseEnabledToolStripMenuItem,
            this.autocompleteToolStripMenuItem,
            this.autocompleteWithBracketsToolStripMenuItem,
            this.toolStripSeparator24,
            this.completeWordOnFirstMatchToolStripMenuItem,
            this.completeWordOnExactMatchToolStripMenuItem,
            this.completeWordDisabledToolStripMenuItem,
            this.toolStripSeparator17,
            this.showItemsThatContainCurrentStringToolStripMenuItem,
            this.addProjectFilesToolStripMenuItem,
            this.addIncludePathToolStripMenuItem,
            this.filterToolStripMenuItem,
            this.toolStripSeparator16,
            this.highlightEnumsToolStripMenuItem,
            this.highlightQuickInfoPopupsToolStripMenuItem,
            this.highlightDisabledCodeBlocksToolStripMenuItem});
            this.intellisenseToolStripMenuItem.Name = "intellisenseToolStripMenuItem";
            this.intellisenseToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.intellisenseToolStripMenuItem.Text = "&Intellisense";
            this.intellisenseToolStripMenuItem.DropDownOpening += new System.EventHandler(this.intellisenseToolStripMenuItem_DropDownOpening);
            // 
            // refreshIntellisenseToolStripMenuItem
            // 
            this.refreshIntellisenseToolStripMenuItem.Name = "refreshIntellisenseToolStripMenuItem";
            this.refreshIntellisenseToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.refreshIntellisenseToolStripMenuItem.Tag = "RefreshIntellisense";
            this.refreshIntellisenseToolStripMenuItem.Text = "&Refresh Intellisense";
            this.refreshIntellisenseToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(281, 6);
            // 
            // intellisenseEnabledToolStripMenuItem
            // 
            this.intellisenseEnabledToolStripMenuItem.Checked = true;
            this.intellisenseEnabledToolStripMenuItem.CheckOnClick = true;
            this.intellisenseEnabledToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.intellisenseEnabledToolStripMenuItem.Name = "intellisenseEnabledToolStripMenuItem";
            this.intellisenseEnabledToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.intellisenseEnabledToolStripMenuItem.Text = "Intellisense &Enabled";
            this.intellisenseEnabledToolStripMenuItem.Click += new System.EventHandler(this.intellisenseEnabledToolStripMenuItem_Click);
            // 
            // autocompleteToolStripMenuItem
            // 
            this.autocompleteToolStripMenuItem.Checked = true;
            this.autocompleteToolStripMenuItem.CheckOnClick = true;
            this.autocompleteToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autocompleteToolStripMenuItem.Name = "autocompleteToolStripMenuItem";
            this.autocompleteToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.autocompleteToolStripMenuItem.Text = "&Autocomplete Enabled";
            this.autocompleteToolStripMenuItem.Click += new System.EventHandler(this.autocompleteToolStripMenuItem_Click);
            // 
            // autocompleteWithBracketsToolStripMenuItem
            // 
            this.autocompleteWithBracketsToolStripMenuItem.Checked = true;
            this.autocompleteWithBracketsToolStripMenuItem.CheckOnClick = true;
            this.autocompleteWithBracketsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autocompleteWithBracketsToolStripMenuItem.Name = "autocompleteWithBracketsToolStripMenuItem";
            this.autocompleteWithBracketsToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.autocompleteWithBracketsToolStripMenuItem.Text = "Autocomplete With &Brackets";
            this.autocompleteWithBracketsToolStripMenuItem.Click += new System.EventHandler(this.autocompleteWithBracketsToolStripMenuItem_Click);
            // 
            // toolStripSeparator24
            // 
            this.toolStripSeparator24.Name = "toolStripSeparator24";
            this.toolStripSeparator24.Size = new System.Drawing.Size(281, 6);
            // 
            // completeWordOnFirstMatchToolStripMenuItem
            // 
            this.completeWordOnFirstMatchToolStripMenuItem.CheckOnClick = true;
            this.completeWordOnFirstMatchToolStripMenuItem.Name = "completeWordOnFirstMatchToolStripMenuItem";
            this.completeWordOnFirstMatchToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.completeWordOnFirstMatchToolStripMenuItem.Text = "Complete Word On First &Match";
            this.completeWordOnFirstMatchToolStripMenuItem.Click += new System.EventHandler(this.completeWordOnFirstMatchToolStripMenuItem_Click);
            // 
            // completeWordOnExactMatchToolStripMenuItem
            // 
            this.completeWordOnExactMatchToolStripMenuItem.Checked = true;
            this.completeWordOnExactMatchToolStripMenuItem.CheckOnClick = true;
            this.completeWordOnExactMatchToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.completeWordOnExactMatchToolStripMenuItem.Name = "completeWordOnExactMatchToolStripMenuItem";
            this.completeWordOnExactMatchToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.completeWordOnExactMatchToolStripMenuItem.Text = "Complete &Word On Exact Match";
            this.completeWordOnExactMatchToolStripMenuItem.Click += new System.EventHandler(this.completeWordOnExactMatchToolStripMenuItem_Click);
            // 
            // completeWordDisabledToolStripMenuItem
            // 
            this.completeWordDisabledToolStripMenuItem.CheckOnClick = true;
            this.completeWordDisabledToolStripMenuItem.Name = "completeWordDisabledToolStripMenuItem";
            this.completeWordDisabledToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.completeWordDisabledToolStripMenuItem.Text = "Complete Word &Disabled";
            this.completeWordDisabledToolStripMenuItem.Click += new System.EventHandler(this.completeWordDisabledToolStripMenuItem_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(281, 6);
            // 
            // showItemsThatContainCurrentStringToolStripMenuItem
            // 
            this.showItemsThatContainCurrentStringToolStripMenuItem.Checked = true;
            this.showItemsThatContainCurrentStringToolStripMenuItem.CheckOnClick = true;
            this.showItemsThatContainCurrentStringToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showItemsThatContainCurrentStringToolStripMenuItem.Name = "showItemsThatContainCurrentStringToolStripMenuItem";
            this.showItemsThatContainCurrentStringToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.showItemsThatContainCurrentStringToolStripMenuItem.Text = "Show Items That &Contain Current String";
            this.showItemsThatContainCurrentStringToolStripMenuItem.Click += new System.EventHandler(this.showItemsThatContainCurrentStringToolStripMenuItem_Click);
            // 
            // addProjectFilesToolStripMenuItem
            // 
            this.addProjectFilesToolStripMenuItem.Checked = true;
            this.addProjectFilesToolStripMenuItem.CheckOnClick = true;
            this.addProjectFilesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.addProjectFilesToolStripMenuItem.Name = "addProjectFilesToolStripMenuItem";
            this.addProjectFilesToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.addProjectFilesToolStripMenuItem.Text = "Add &Project Files";
            this.addProjectFilesToolStripMenuItem.Click += new System.EventHandler(this.addProjectFilesToolStripMenuItem_Click);
            // 
            // addIncludePathToolStripMenuItem
            // 
            this.addIncludePathToolStripMenuItem.Checked = true;
            this.addIncludePathToolStripMenuItem.CheckOnClick = true;
            this.addIncludePathToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.addIncludePathToolStripMenuItem.Name = "addIncludePathToolStripMenuItem";
            this.addIncludePathToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.addIncludePathToolStripMenuItem.Text = "Add &IncludePath Files";
            this.addIncludePathToolStripMenuItem.Click += new System.EventHandler(this.addIncludePathToolStripMenuItem_Click);
            // 
            // filterToolStripMenuItem
            // 
            this.filterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllToolStripMenuItem,
            this.selectNoneToolStripMenuItem,
            this.toolStripSeparator22,
            this.showConstantsToolStripMenuItem,
            this.showNativeTypesToolStripMenuItem,
            this.showStructTypesToolStripMenuItem,
            this.showEnumTypesToolStripMenuItem,
            this.showEnumsToolStripMenuItem,
            this.showStaticVariablesToolStripMenuItem,
            this.showSubroutinesToolStripMenuItem,
            this.showNativeSubroutinesToolStripMenuItem,
            this.showStatesToolStripMenuItem});
            this.filterToolStripMenuItem.Name = "filterToolStripMenuItem";
            this.filterToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.filterToolStripMenuItem.Text = "&Filter";
            this.filterToolStripMenuItem.DropDownOpening += new System.EventHandler(this.filterToolStripMenuItem_DropDownOpening);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.selectAllToolStripMenuItem.Text = "Select &All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // selectNoneToolStripMenuItem
            // 
            this.selectNoneToolStripMenuItem.Name = "selectNoneToolStripMenuItem";
            this.selectNoneToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.selectNoneToolStripMenuItem.Text = "Select &None";
            this.selectNoneToolStripMenuItem.Click += new System.EventHandler(this.selectNoneToolStripMenuItem_Click);
            // 
            // toolStripSeparator22
            // 
            this.toolStripSeparator22.Name = "toolStripSeparator22";
            this.toolStripSeparator22.Size = new System.Drawing.Size(203, 6);
            // 
            // showConstantsToolStripMenuItem
            // 
            this.showConstantsToolStripMenuItem.Checked = true;
            this.showConstantsToolStripMenuItem.CheckOnClick = true;
            this.showConstantsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showConstantsToolStripMenuItem.Name = "showConstantsToolStripMenuItem";
            this.showConstantsToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.showConstantsToolStripMenuItem.Text = "Show &Constants";
            this.showConstantsToolStripMenuItem.Click += new System.EventHandler(this.showConstantsToolStripMenuItem_Click);
            // 
            // showNativeTypesToolStripMenuItem
            // 
            this.showNativeTypesToolStripMenuItem.Checked = true;
            this.showNativeTypesToolStripMenuItem.CheckOnClick = true;
            this.showNativeTypesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showNativeTypesToolStripMenuItem.Name = "showNativeTypesToolStripMenuItem";
            this.showNativeTypesToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.showNativeTypesToolStripMenuItem.Text = "Show N&ative Types";
            this.showNativeTypesToolStripMenuItem.Click += new System.EventHandler(this.showNativeTypesToolStripMenuItem_Click);
            // 
            // showStructTypesToolStripMenuItem
            // 
            this.showStructTypesToolStripMenuItem.Checked = true;
            this.showStructTypesToolStripMenuItem.CheckOnClick = true;
            this.showStructTypesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showStructTypesToolStripMenuItem.Name = "showStructTypesToolStripMenuItem";
            this.showStructTypesToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.showStructTypesToolStripMenuItem.Text = "Show &Struct Types";
            this.showStructTypesToolStripMenuItem.Click += new System.EventHandler(this.showStructTypesToolStripMenuItem_Click);
            // 
            // showEnumTypesToolStripMenuItem
            // 
            this.showEnumTypesToolStripMenuItem.Checked = true;
            this.showEnumTypesToolStripMenuItem.CheckOnClick = true;
            this.showEnumTypesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showEnumTypesToolStripMenuItem.Name = "showEnumTypesToolStripMenuItem";
            this.showEnumTypesToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.showEnumTypesToolStripMenuItem.Text = "Show &Enum Types";
            this.showEnumTypesToolStripMenuItem.Click += new System.EventHandler(this.showEnumTypesToolStripMenuItem_Click);
            // 
            // showEnumsToolStripMenuItem
            // 
            this.showEnumsToolStripMenuItem.Checked = true;
            this.showEnumsToolStripMenuItem.CheckOnClick = true;
            this.showEnumsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showEnumsToolStripMenuItem.Name = "showEnumsToolStripMenuItem";
            this.showEnumsToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.showEnumsToolStripMenuItem.Text = "Show Enu&ms";
            this.showEnumsToolStripMenuItem.Click += new System.EventHandler(this.showEnumsToolStripMenuItem_Click);
            // 
            // showStaticVariablesToolStripMenuItem
            // 
            this.showStaticVariablesToolStripMenuItem.Checked = true;
            this.showStaticVariablesToolStripMenuItem.CheckOnClick = true;
            this.showStaticVariablesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showStaticVariablesToolStripMenuItem.Name = "showStaticVariablesToolStripMenuItem";
            this.showStaticVariablesToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.showStaticVariablesToolStripMenuItem.Text = "Show Static &Variables";
            this.showStaticVariablesToolStripMenuItem.Click += new System.EventHandler(this.showStaticVariablesToolStripMenuItem_Click);
            // 
            // showSubroutinesToolStripMenuItem
            // 
            this.showSubroutinesToolStripMenuItem.Checked = true;
            this.showSubroutinesToolStripMenuItem.CheckOnClick = true;
            this.showSubroutinesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showSubroutinesToolStripMenuItem.Name = "showSubroutinesToolStripMenuItem";
            this.showSubroutinesToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.showSubroutinesToolStripMenuItem.Text = "Show S&ubroutines";
            this.showSubroutinesToolStripMenuItem.Click += new System.EventHandler(this.showSubroutinesToolStripMenuItem_Click);
            // 
            // showNativeSubroutinesToolStripMenuItem
            // 
            this.showNativeSubroutinesToolStripMenuItem.Checked = true;
            this.showNativeSubroutinesToolStripMenuItem.CheckOnClick = true;
            this.showNativeSubroutinesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showNativeSubroutinesToolStripMenuItem.Name = "showNativeSubroutinesToolStripMenuItem";
            this.showNativeSubroutinesToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.showNativeSubroutinesToolStripMenuItem.Text = "Show Native Su&broutines";
            this.showNativeSubroutinesToolStripMenuItem.Click += new System.EventHandler(this.showNativeSubroutinesToolStripMenuItem_Click);
            // 
            // showStatesToolStripMenuItem
            // 
            this.showStatesToolStripMenuItem.Checked = true;
            this.showStatesToolStripMenuItem.CheckOnClick = true;
            this.showStatesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showStatesToolStripMenuItem.Name = "showStatesToolStripMenuItem";
            this.showStatesToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.showStatesToolStripMenuItem.Text = "Show S&tates";
            this.showStatesToolStripMenuItem.Click += new System.EventHandler(this.showStatesToolStripMenuItem_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(281, 6);
            // 
            // highlightEnumsToolStripMenuItem
            // 
            this.highlightEnumsToolStripMenuItem.Checked = true;
            this.highlightEnumsToolStripMenuItem.CheckOnClick = true;
            this.highlightEnumsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.highlightEnumsToolStripMenuItem.Enabled = false;
            this.highlightEnumsToolStripMenuItem.Name = "highlightEnumsToolStripMenuItem";
            this.highlightEnumsToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.highlightEnumsToolStripMenuItem.Text = "&Highlight Enums";
            this.highlightEnumsToolStripMenuItem.Click += new System.EventHandler(this.highlightEnumsToolStripMenuItem_Click);
            // 
            // highlightQuickInfoPopupsToolStripMenuItem
            // 
            this.highlightQuickInfoPopupsToolStripMenuItem.Checked = true;
            this.highlightQuickInfoPopupsToolStripMenuItem.CheckOnClick = true;
            this.highlightQuickInfoPopupsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.highlightQuickInfoPopupsToolStripMenuItem.Name = "highlightQuickInfoPopupsToolStripMenuItem";
            this.highlightQuickInfoPopupsToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.highlightQuickInfoPopupsToolStripMenuItem.Text = "Highlight &QuickInfo Popups";
            this.highlightQuickInfoPopupsToolStripMenuItem.Click += new System.EventHandler(this.highlightQuickInfoPopupsToolStripMenuItem_Click);
            // 
            // highlightDisabledCodeBlocksToolStripMenuItem
            // 
            this.highlightDisabledCodeBlocksToolStripMenuItem.Checked = true;
            this.highlightDisabledCodeBlocksToolStripMenuItem.CheckOnClick = true;
            this.highlightDisabledCodeBlocksToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.highlightDisabledCodeBlocksToolStripMenuItem.Name = "highlightDisabledCodeBlocksToolStripMenuItem";
            this.highlightDisabledCodeBlocksToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.highlightDisabledCodeBlocksToolStripMenuItem.Text = "Highlight Disabled Code B&locks";
            this.highlightDisabledCodeBlocksToolStripMenuItem.Click += new System.EventHandler(this.highlightDisabledCodeBlocksToolStripMenuItem_Click);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.useNewFindReplaceDialogToolStripMenuItem,
            this.findReplaceToolStripMenuItem,
            this.useNewFindReplaceInFilesDialogToolStripMenuItem,
            this.findReplaceInFilesToolStripMenuItem,
            this.gotoNextFindResultToolStripMenuItem,
            this.toolStripSeparator34,
            this.enableWordWrappingInFindResultsToolStripMenuItem,
            this.toolStripSeparator18,
            this.incrementalSearchToolStripMenuItem,
            this.reverseIncrementalSearchToolStripMenuItem,
            this.toolStripSeparator19,
            this.backToolStripMenuItem,
            this.forwardToolStripMenuItem,
            this.goToLineToolStripMenuItem,
            this.goToDefinitionToolStripMenuItem,
            this.goToProgramCounterToolStripMenuItem});
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.searchToolStripMenuItem.Text = "&Search";
            this.searchToolStripMenuItem.DropDownOpening += new System.EventHandler(this.searchToolStripMenuItem_DropDownOpening);
            // 
            // useNewFindReplaceDialogToolStripMenuItem
            // 
            this.useNewFindReplaceDialogToolStripMenuItem.Checked = true;
            this.useNewFindReplaceDialogToolStripMenuItem.CheckOnClick = true;
            this.useNewFindReplaceDialogToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useNewFindReplaceDialogToolStripMenuItem.Name = "useNewFindReplaceDialogToolStripMenuItem";
            this.useNewFindReplaceDialogToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.useNewFindReplaceDialogToolStripMenuItem.Text = "Use new Find/Replace dialog";
            this.useNewFindReplaceDialogToolStripMenuItem.Click += new System.EventHandler(this.useNewFindReplaceDialogToolStripMenuItem_Click);
            // 
            // findReplaceToolStripMenuItem
            // 
            this.findReplaceToolStripMenuItem.Name = "findReplaceToolStripMenuItem";
            this.findReplaceToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+F";
            this.findReplaceToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.findReplaceToolStripMenuItem.Tag = "SearchFindReplace";
            this.findReplaceToolStripMenuItem.Text = "&Find/Replace...";
            this.findReplaceToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // useNewFindReplaceInFilesDialogToolStripMenuItem
            // 
            this.useNewFindReplaceInFilesDialogToolStripMenuItem.Checked = true;
            this.useNewFindReplaceInFilesDialogToolStripMenuItem.CheckOnClick = true;
            this.useNewFindReplaceInFilesDialogToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useNewFindReplaceInFilesDialogToolStripMenuItem.Name = "useNewFindReplaceInFilesDialogToolStripMenuItem";
            this.useNewFindReplaceInFilesDialogToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.useNewFindReplaceInFilesDialogToolStripMenuItem.Text = "Use new Find/Replace in Files dialog";
            this.useNewFindReplaceInFilesDialogToolStripMenuItem.Click += new System.EventHandler(this.useNewFindReplaceInFilesDialogToolStripMenuItem_Click);
            // 
            // findReplaceInFilesToolStripMenuItem
            // 
            this.findReplaceInFilesToolStripMenuItem.Name = "findReplaceInFilesToolStripMenuItem";
            this.findReplaceInFilesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F)));
            this.findReplaceInFilesToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.findReplaceInFilesToolStripMenuItem.Text = "Find/Replace in F&iles...";
            this.findReplaceInFilesToolStripMenuItem.Click += new System.EventHandler(this.findReplaceInFilesToolStripMenuItem_Click);
            // 
            // gotoNextFindResultToolStripMenuItem
            // 
            this.gotoNextFindResultToolStripMenuItem.Enabled = false;
            this.gotoNextFindResultToolStripMenuItem.Name = "gotoNextFindResultToolStripMenuItem";
            this.gotoNextFindResultToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F4)));
            this.gotoNextFindResultToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.gotoNextFindResultToolStripMenuItem.Text = "Goto &Next Find Result";
            this.gotoNextFindResultToolStripMenuItem.Click += new System.EventHandler(this.gotoNextFindResultToolStripMenuItem_Click);
            // 
            // toolStripSeparator34
            // 
            this.toolStripSeparator34.Name = "toolStripSeparator34";
            this.toolStripSeparator34.Size = new System.Drawing.Size(284, 6);
            // 
            // enableWordWrappingInFindResultsToolStripMenuItem
            // 
            this.enableWordWrappingInFindResultsToolStripMenuItem.Checked = true;
            this.enableWordWrappingInFindResultsToolStripMenuItem.CheckOnClick = true;
            this.enableWordWrappingInFindResultsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enableWordWrappingInFindResultsToolStripMenuItem.Name = "enableWordWrappingInFindResultsToolStripMenuItem";
            this.enableWordWrappingInFindResultsToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.enableWordWrappingInFindResultsToolStripMenuItem.Text = "Enable Word Wrapping in Find Results ";
            this.enableWordWrappingInFindResultsToolStripMenuItem.Click += new System.EventHandler(this.enableWordWrappingInFindResultsDialogToolStripMenuItem_Click);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(284, 6);
            // 
            // incrementalSearchToolStripMenuItem
            // 
            this.incrementalSearchToolStripMenuItem.Name = "incrementalSearchToolStripMenuItem";
            this.incrementalSearchToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+I";
            this.incrementalSearchToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.incrementalSearchToolStripMenuItem.Tag = "SearchReverseIncremental";
            this.incrementalSearchToolStripMenuItem.Text = "Incremental &Search";
            this.incrementalSearchToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // reverseIncrementalSearchToolStripMenuItem
            // 
            this.reverseIncrementalSearchToolStripMenuItem.Name = "reverseIncrementalSearchToolStripMenuItem";
            this.reverseIncrementalSearchToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Shift+I";
            this.reverseIncrementalSearchToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.reverseIncrementalSearchToolStripMenuItem.Tag = "SearchReverseIncremental";
            this.reverseIncrementalSearchToolStripMenuItem.Text = "&Reverse Incremental Search";
            this.reverseIncrementalSearchToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolStripSeparator19
            // 
            this.toolStripSeparator19.Name = "toolStripSeparator19";
            this.toolStripSeparator19.Size = new System.Drawing.Size(284, 6);
            // 
            // goToLineToolStripMenuItem
            // 
            this.goToLineToolStripMenuItem.Name = "goToLineToolStripMenuItem";
            this.goToLineToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+G";
            this.goToLineToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.goToLineToolStripMenuItem.Tag = "SearchGoToLine";
            this.goToLineToolStripMenuItem.Text = "&Go To Line...";
            this.goToLineToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // goToDefinitionToolStripMenuItem
            // 
            this.goToDefinitionToolStripMenuItem.Name = "goToDefinitionToolStripMenuItem";
            this.goToDefinitionToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.goToDefinitionToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.G)));
            this.goToDefinitionToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.goToDefinitionToolStripMenuItem.Tag = "SearchGoToDefinition";
            this.goToDefinitionToolStripMenuItem.Text = "Go To &Definition";
            this.goToDefinitionToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // goToProgramCounterToolStripMenuItem
            // 
            this.goToProgramCounterToolStripMenuItem.Name = "goToProgramCounterToolStripMenuItem";
            this.goToProgramCounterToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.G)));
            this.goToProgramCounterToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.goToProgramCounterToolStripMenuItem.Tag = "SearchGoToProgramCounter";
            this.goToProgramCounterToolStripMenuItem.Text = "Go To Program Counter...";
            this.goToProgramCounterToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // outliningToolStripMenuItem
            // 
            this.outliningToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startAutomaticOutliningToolStripMenuItem,
            this.startManualOutliningToolStripMenuItem,
            this.stopOutliningToolStripMenuItem,
            this.toolStripSeparator20,
            this.hideCurrentSelectionToolStripMenuItem,
            this.stopHidingCurrentSelectionToolStripMenuItem,
            this.toolStripSeparator21,
            this.toggleOutliningExpansionToolStripMenuItem,
            this.toggleAllOutliningToolStripMenuItem});
            this.outliningToolStripMenuItem.Name = "outliningToolStripMenuItem";
            this.outliningToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.outliningToolStripMenuItem.Text = "&Outlining";
            this.outliningToolStripMenuItem.DropDownOpening += new System.EventHandler(this.outliningToolStripMenuItem_DropDownOpening);
            // 
            // startAutomaticOutliningToolStripMenuItem
            // 
            this.startAutomaticOutliningToolStripMenuItem.Name = "startAutomaticOutliningToolStripMenuItem";
            this.startAutomaticOutliningToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.startAutomaticOutliningToolStripMenuItem.Tag = "OutliningStartAutomaticOutlining";
            this.startAutomaticOutliningToolStripMenuItem.Text = "Start &Automatic Outlining";
            this.startAutomaticOutliningToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // startManualOutliningToolStripMenuItem
            // 
            this.startManualOutliningToolStripMenuItem.Name = "startManualOutliningToolStripMenuItem";
            this.startManualOutliningToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.startManualOutliningToolStripMenuItem.Tag = "OutliningStartManualOutlining";
            this.startManualOutliningToolStripMenuItem.Text = "Start &Manual Outlining";
            this.startManualOutliningToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // stopOutliningToolStripMenuItem
            // 
            this.stopOutliningToolStripMenuItem.Name = "stopOutliningToolStripMenuItem";
            this.stopOutliningToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.stopOutliningToolStripMenuItem.Tag = "OutliningStopOutlining";
            this.stopOutliningToolStripMenuItem.Text = "&Stop Outlining";
            this.stopOutliningToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(228, 6);
            // 
            // hideCurrentSelectionToolStripMenuItem
            // 
            this.hideCurrentSelectionToolStripMenuItem.Name = "hideCurrentSelectionToolStripMenuItem";
            this.hideCurrentSelectionToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.hideCurrentSelectionToolStripMenuItem.Tag = "OutliningHideSelection";
            this.hideCurrentSelectionToolStripMenuItem.Text = "&Hide Current Selection";
            this.hideCurrentSelectionToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // stopHidingCurrentSelectionToolStripMenuItem
            // 
            this.stopHidingCurrentSelectionToolStripMenuItem.Name = "stopHidingCurrentSelectionToolStripMenuItem";
            this.stopHidingCurrentSelectionToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.stopHidingCurrentSelectionToolStripMenuItem.Tag = "OutliningStopHidingCurrent";
            this.stopHidingCurrentSelectionToolStripMenuItem.Text = "Sto&p Hiding Current Selection";
            this.stopHidingCurrentSelectionToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolStripSeparator21
            // 
            this.toolStripSeparator21.Name = "toolStripSeparator21";
            this.toolStripSeparator21.Size = new System.Drawing.Size(228, 6);
            // 
            // toggleOutliningExpansionToolStripMenuItem
            // 
            this.toggleOutliningExpansionToolStripMenuItem.Name = "toggleOutliningExpansionToolStripMenuItem";
            this.toggleOutliningExpansionToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.toggleOutliningExpansionToolStripMenuItem.Tag = "OutliningToggleOutliningExpansion";
            this.toggleOutliningExpansionToolStripMenuItem.Text = "&Toggle Outlining Expansion";
            this.toggleOutliningExpansionToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toggleAllOutliningToolStripMenuItem
            // 
            this.toggleAllOutliningToolStripMenuItem.Name = "toggleAllOutliningToolStripMenuItem";
            this.toggleAllOutliningToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.toggleAllOutliningToolStripMenuItem.Tag = "OutliningToggleAllOutlining";
            this.toggleAllOutliningToolStripMenuItem.Text = "T&oggle All Outlining";
            this.toggleAllOutliningToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.recordTemporaryMacroToolStripMenuItem,
            this.playTemporaryMacroToolStripMenuItem,
            this.cancelRecordingToolStripMenuItem,
            this.toolStripSeparator23,
            this.settingsToolStripMenuItem,
            this.highlightingStyleEditorToolStripMenuItem,
            this.customButtonManagerToolStripMenuItem,
            this.toolStripSeparator27,
            this.viewLogToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            this.toolsToolStripMenuItem.DropDownOpening += new System.EventHandler(this.toolsToolStripMenuItem_DropDownOpening);
            // 
            // recordTemporaryMacroToolStripMenuItem
            // 
            this.recordTemporaryMacroToolStripMenuItem.Name = "recordTemporaryMacroToolStripMenuItem";
            this.recordTemporaryMacroToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.R)));
            this.recordTemporaryMacroToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.recordTemporaryMacroToolStripMenuItem.Tag = "ToolsMacrosRecord";
            this.recordTemporaryMacroToolStripMenuItem.Text = "&Record Temporary Macro";
            this.recordTemporaryMacroToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // playTemporaryMacroToolStripMenuItem
            // 
            this.playTemporaryMacroToolStripMenuItem.Name = "playTemporaryMacroToolStripMenuItem";
            this.playTemporaryMacroToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.P)));
            this.playTemporaryMacroToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.playTemporaryMacroToolStripMenuItem.Tag = "ToolsMacrosRun";
            this.playTemporaryMacroToolStripMenuItem.Text = "&Play Temporary Macro";
            this.playTemporaryMacroToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // cancelRecordingToolStripMenuItem
            // 
            this.cancelRecordingToolStripMenuItem.Name = "cancelRecordingToolStripMenuItem";
            this.cancelRecordingToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.cancelRecordingToolStripMenuItem.Tag = "ToolsMacrosCancel";
            this.cancelRecordingToolStripMenuItem.Text = "C&ancel Recording";
            this.cancelRecordingToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolStripSeparator23
            // 
            this.toolStripSeparator23.Name = "toolStripSeparator23";
            this.toolStripSeparator23.Size = new System.Drawing.Size(278, 6);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.settingsToolStripMenuItem.Text = "Settin&gs...";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // highlightingStyleEditorToolStripMenuItem
            // 
            this.highlightingStyleEditorToolStripMenuItem.Name = "highlightingStyleEditorToolStripMenuItem";
            this.highlightingStyleEditorToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.highlightingStyleEditorToolStripMenuItem.Text = "Highlighting &Style Editor...";
            this.highlightingStyleEditorToolStripMenuItem.Click += new System.EventHandler(this.highlightingStyleEditorToolStripMenuItem_Click);
            // 
            // customButtonManagerToolStripMenuItem
            // 
            this.customButtonManagerToolStripMenuItem.Name = "customButtonManagerToolStripMenuItem";
            this.customButtonManagerToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.customButtonManagerToolStripMenuItem.Text = "&Custom Button Manager...";
            this.customButtonManagerToolStripMenuItem.Click += new System.EventHandler(this.customButtonManagerToolStripMenuItem_Click);
            // 
            // toolStripSeparator27
            // 
            this.toolStripSeparator27.Name = "toolStripSeparator27";
            this.toolStripSeparator27.Size = new System.Drawing.Size(278, 6);
            // 
            // viewLogToolStripMenuItem
            // 
            this.viewLogToolStripMenuItem.Name = "viewLogToolStripMenuItem";
            this.viewLogToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.viewLogToolStripMenuItem.Text = "&View Log";
            this.viewLogToolStripMenuItem.Click += new System.EventHandler(this.viewLogToolStripMenuItem_Click);
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.splitHorizontallyToolStripMenuItem,
            this.splitVerticallyToolStripMenuItem,
            this.fourWaySplitToolStripMenuItem,
            this.noSplitsToolStripMenuItem});
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.windowToolStripMenuItem.Text = "&Window";
            this.windowToolStripMenuItem.DropDownOpening += new System.EventHandler(this.windowToolStripMenuItem_DropDownOpening);
            // 
            // splitHorizontallyToolStripMenuItem
            // 
            this.splitHorizontallyToolStripMenuItem.CheckOnClick = true;
            this.splitHorizontallyToolStripMenuItem.Name = "splitHorizontallyToolStripMenuItem";
            this.splitHorizontallyToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.splitHorizontallyToolStripMenuItem.Tag = "WindowSplitHorizontally";
            this.splitHorizontallyToolStripMenuItem.Text = "Split &Horizontally";
            this.splitHorizontallyToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // splitVerticallyToolStripMenuItem
            // 
            this.splitVerticallyToolStripMenuItem.CheckOnClick = true;
            this.splitVerticallyToolStripMenuItem.Name = "splitVerticallyToolStripMenuItem";
            this.splitVerticallyToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.splitVerticallyToolStripMenuItem.Tag = "WindowSplitVertically";
            this.splitVerticallyToolStripMenuItem.Text = "Split &Vertically";
            this.splitVerticallyToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // fourWaySplitToolStripMenuItem
            // 
            this.fourWaySplitToolStripMenuItem.CheckOnClick = true;
            this.fourWaySplitToolStripMenuItem.Name = "fourWaySplitToolStripMenuItem";
            this.fourWaySplitToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.fourWaySplitToolStripMenuItem.Tag = "WindowSplitFourWay";
            this.fourWaySplitToolStripMenuItem.Text = "&Four-Way Split";
            this.fourWaySplitToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // noSplitsToolStripMenuItem
            // 
            this.noSplitsToolStripMenuItem.Checked = true;
            this.noSplitsToolStripMenuItem.CheckOnClick = true;
            this.noSplitsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.noSplitsToolStripMenuItem.Name = "noSplitsToolStripMenuItem";
            this.noSplitsToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.noSplitsToolStripMenuItem.Tag = "WindowNoSplits";
            this.noSplitsToolStripMenuItem.Text = "&No Splits";
            this.noSplitsToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.toolStripSeparator5,
            this.toolStripSeparator25,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            this.helpToolStripMenuItem.DropDownOpening += new System.EventHandler(this.helpToolStripMenuItem_DropDownOpening);
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.contentsToolStripMenuItem.Text = "&Contents";
            this.contentsToolStripMenuItem.Click += new System.EventHandler(this.contentsToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(138, 6);
            // 
            // toolStripSeparator25
            // 
            this.toolStripSeparator25.Name = "toolStripSeparator25";
            this.toolStripSeparator25.Size = new System.Drawing.Size(138, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // documentTabContextMenuStrip
            // 
            this.documentTabContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileNameToolStripMenuItem,
            this.openContainingFolderToolStripMenuItem,
            this.toolStripSeparator26,
            this.newTabToolStripMenuItem,
            this.closeTabToolStripMenuItem,
            this.closeOtherTabsToolStripMenuItem,
            this.closeTabsToRightToolStripMenuItem,
            this.closeAllTabsToolStripMenuItem});
            this.documentTabContextMenuStrip.Name = "documentTabContextMenuStrip";
            this.documentTabContextMenuStrip.ShowImageMargin = false;
            this.documentTabContextMenuStrip.Size = new System.Drawing.Size(177, 164);
            // 
            // fileNameToolStripMenuItem
            // 
            this.fileNameToolStripMenuItem.Name = "fileNameToolStripMenuItem";
            this.fileNameToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.fileNameToolStripMenuItem.Text = "File Name";
            this.fileNameToolStripMenuItem.ToolTipText = "Click to copy the full path name to the clipboard.";
            this.fileNameToolStripMenuItem.Click += new System.EventHandler(this.fileNameToolStripMenuItem_Click);
            // 
            // openContainingFolderToolStripMenuItem
            // 
            this.openContainingFolderToolStripMenuItem.Name = "openContainingFolderToolStripMenuItem";
            this.openContainingFolderToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.openContainingFolderToolStripMenuItem.Text = "Open Containing &Folder";
            this.openContainingFolderToolStripMenuItem.Click += new System.EventHandler(this.openContainingFolderToolStripMenuItem_Click);
            // 
            // toolStripSeparator26
            // 
            this.toolStripSeparator26.Name = "toolStripSeparator26";
            this.toolStripSeparator26.Size = new System.Drawing.Size(173, 6);
            // 
            // newTabToolStripMenuItem
            // 
            this.newTabToolStripMenuItem.Name = "newTabToolStripMenuItem";
            this.newTabToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.newTabToolStripMenuItem.Text = "&New Tab";
            this.newTabToolStripMenuItem.Click += new System.EventHandler(this.newTabToolStripMenuItem_Click);
            // 
            // closeTabToolStripMenuItem
            // 
            this.closeTabToolStripMenuItem.Name = "closeTabToolStripMenuItem";
            this.closeTabToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.closeTabToolStripMenuItem.Text = "&Close Tab";
            this.closeTabToolStripMenuItem.Click += new System.EventHandler(this.closeTabToolStripMenuItem_Click);
            // 
            // closeOtherTabsToolStripMenuItem
            // 
            this.closeOtherTabsToolStripMenuItem.Name = "closeOtherTabsToolStripMenuItem";
            this.closeOtherTabsToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.closeOtherTabsToolStripMenuItem.Text = "Close &Other Tabs";
            this.closeOtherTabsToolStripMenuItem.Click += new System.EventHandler(this.closeOtherTabsToolStripMenuItem_Click);
            // 
            // closeTabsToRightToolStripMenuItem
            // 
            this.closeTabsToRightToolStripMenuItem.Name = "closeTabsToRightToolStripMenuItem";
            this.closeTabsToRightToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.closeTabsToRightToolStripMenuItem.Text = "Close Tabs to &Right";
            this.closeTabsToRightToolStripMenuItem.Click += new System.EventHandler(this.closeTabsToRightToolStripMenuItem_Click);
            // 
            // closeAllTabsToolStripMenuItem
            // 
            this.closeAllTabsToolStripMenuItem.Name = "closeAllTabsToolStripMenuItem";
            this.closeAllTabsToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.closeAllTabsToolStripMenuItem.Text = "Close &All Tabs";
            this.closeAllTabsToolStripMenuItem.Click += new System.EventHandler(this.closeAllTabsToolStripMenuItem_Click);
            // 
            // projectOpenFileDialog
            // 
            this.projectOpenFileDialog.DefaultExt = "scproj";
            this.projectOpenFileDialog.Filter = "Script Project Files (*.scproj)|*.scproj|All Files (*.*)|*.*";
            this.projectOpenFileDialog.Title = "Open Script Project File";
            // 
            // activeFilesContextMenuStrip
            // 
            this.activeFilesContextMenuStrip.Name = "activeFilesContextMenuStrip";
            this.activeFilesContextMenuStrip.ShowImageMargin = false;
            this.activeFilesContextMenuStrip.Size = new System.Drawing.Size(36, 4);
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 773);
            this.Controls.Add(this.toolStripContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(1000, 800);
            this.Name = "Form1";
            this.Text = "Script Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form1_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Form1_DragEnter);
            documentContainer1.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            dockContainer1.ResumeLayout(false);
            this.documentOutlineDockableWindow.ResumeLayout(false);
            this.documentOutlineDockableWindow.PerformLayout();
            this.documentOutlineContextMenuStrip.ResumeLayout(false);
            dockContainer2.ResumeLayout(false);
            this.outputDockableWindow.ResumeLayout(false);
            this.outputContextMenuStrip.ResumeLayout(false);
            this.findResultsDockableWindow.ResumeLayout(false);
            this.findResultsContextMenuStrip.ResumeLayout(false);
            this.errorsDockableWindow.ResumeLayout(false);
            this.errorsContextMenuStrip.ResumeLayout(false);
            this.helpDockableWindow.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.documentTabContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem saveAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pageSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem recentFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recentProjectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem forwardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commentSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uncommentSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeUppercaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeLowercaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteHorizontalWhitespaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bookmarksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showLineNumbersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertCodeSnippetToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem toggleBookmarkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nextBookmarkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem previousBookmarkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearBookmarksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem highlightingStyleEditorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customButtonManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compilingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compileProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recompileProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compileFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopCompileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem gotoNextErrorToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem saveBeforeCompileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dontSaveBeforeCompileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAllBeforeCompileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem startDebugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem intellisenseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshIntellisenseToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem autocompleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autocompleteWithBracketsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showItemsThatContainCurrentStringToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripMenuItem addProjectFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addIncludePathToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findReplaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findReplaceInFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gotoNextFindResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripMenuItem incrementalSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reverseIncrementalSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator19;
        private System.Windows.Forms.ToolStripMenuItem goToLineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outliningToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startAutomaticOutliningToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startManualOutliningToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopOutliningToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripMenuItem hideCurrentSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopHidingCurrentSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator21;
        private System.Windows.Forms.ToolStripMenuItem toggleOutliningExpansionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toggleAllOutliningToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem splitHorizontallyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem splitVerticallyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fourWaySplitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noSplitsToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel parseStatusToolStripStatusLabel;
        private System.Windows.Forms.ToolStripProgressBar parseStatusToolStripProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel cursorPositionToolStripStatusLabel;
        private TD.SandDock.DockableWindow projectDockableWindow;
        private TD.SandDock.SandDockManager sandDockManager1;
        private TD.SandDock.DockableWindow outputDockableWindow;
        private TD.SandDock.DockableWindow findResultsDockableWindow;
        private TD.SandDock.DockableWindow errorsDockableWindow;
        private TD.SandDock.TabbedDocument tabbedDocument1;
        private System.Windows.Forms.RichTextBox outputRichTextBox;
        private System.Windows.Forms.RichTextBox findResultsRichTextBox;
        private System.Windows.Forms.ListView errorsListView;
        private System.Windows.Forms.ColumnHeader errorDescriptionColumnHeader;
        private TD.SandDock.DockableWindow helpDockableWindow;
        private System.Windows.Forms.RichTextBox helpRichTextBox;
        private System.Windows.Forms.ContextMenuStrip documentTabContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem closeTabToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeOtherTabsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllTabsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeTabsToRightToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newTabToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog projectOpenFileDialog;
        private System.Windows.Forms.ContextMenuStrip outputContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearAllOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToErrorOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllOutputToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip findResultsContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyfindResultsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem clearAllFindResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToFindResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllFindResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem highlightEnumsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recordTemporaryMacroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playTemporaryMacroToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator23;
        private System.Windows.Forms.ToolStripMenuItem cancelRecordingToolStripMenuItem;
        private TD.SandDock.DockableWindow documentOutlineDockableWindow;
        private System.Windows.Forms.TreeView documentOutlineTreeView;
        private System.Windows.Forms.CheckBox updateDocumentOutlineCheckBox;
        private System.Windows.Forms.ToolStripMenuItem intellisenseEnabledToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem highlightQuickInfoPopupsToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader errorFileColumnHeader;
        private System.Windows.Forms.ColumnHeader errorLineColumnHeader;
        private System.Windows.Forms.ContextMenuStrip errorsContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyErrorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gotoErrorErrorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllErrorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripMenuItem completeWordOnExactMatchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectNoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator22;
        private System.Windows.Forms.ToolStripMenuItem showConstantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showNativeTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showStructTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showEnumTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showEnumsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showStaticVariablesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showSubroutinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showNativeSubroutinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showStatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator24;
        private System.Windows.Forms.ToolStripMenuItem completeWordOnFirstMatchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem completeWordDisabledToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip documentOutlineContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem documentOutlineCollapseAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem documentOutlineExpandAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator25;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewProjectExplorerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewDocumentOutlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewFindResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewErrorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showProjectExplorerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem centerProjectExplorerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem floatProjectExplorerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dockProjectExplorerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showDocumentOutlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem centerDocumentOutlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem floatDocumentOutlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dockDocumentOutlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem centerOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem floatOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dockOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showFindResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem centerFindResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem floatFindResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dockFindResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showErrorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem centerErrorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dockErrorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem floatErrorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem centerHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dockHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem floatHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator26;
        private System.Windows.Forms.ToolStripMenuItem highlightDisabledCodeBlocksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem useNewFindReplaceDialogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem useNewFindReplaceInFilesDialogToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator27;
        private System.Windows.Forms.ToolStripMenuItem viewLogToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip activeFilesContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem openContainingFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAgainDownOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAgainUpOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchHighlightedDownOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchHighlightedUpOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator28;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator29;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator31;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator33;
        private System.Windows.Forms.ToolStripMenuItem findFindResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAgainDownFindResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAgainUpFindResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchHighlightedDownFindResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchHighlightedUpFindResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator30;
        private System.Windows.Forms.ToolStripMenuItem incrediBuildToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buildProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rebuildProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cleanProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator32;
        private System.Windows.Forms.ToolStripMenuItem stopBuildToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compileFileForPreviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator34;
        private System.Windows.Forms.ToolStripMenuItem enableWordWrappingInFindResultsToolStripMenuItem;
        private System.Windows.Forms.ImageList errorImageList;
        private System.Windows.Forms.ToolStripMenuItem goToProgramCounterToolStripMenuItem;
    }
}

