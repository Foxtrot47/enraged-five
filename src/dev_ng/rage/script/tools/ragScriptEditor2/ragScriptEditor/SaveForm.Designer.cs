namespace ragScriptEditor
{
    partial class SaveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.unsavedFilesListBox = new System.Windows.Forms.ListBox();
            this.saveAllButton = new System.Windows.Forms.Button();
            this.saveSelectedButton = new System.Windows.Forms.Button();
            this.saveNoneButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // unsavedFilesListBox
            // 
            this.unsavedFilesListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.unsavedFilesListBox.FormattingEnabled = true;
            this.unsavedFilesListBox.Location = new System.Drawing.Point( 12, 12 );
            this.unsavedFilesListBox.Name = "unsavedFilesListBox";
            this.unsavedFilesListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.unsavedFilesListBox.Size = new System.Drawing.Size( 370, 186 );
            this.unsavedFilesListBox.TabIndex = 0;
            this.unsavedFilesListBox.SelectedIndexChanged += new System.EventHandler( this.unsavedFilesListBox_SelectedIndexChanged );
            // 
            // saveAllButton
            // 
            this.saveAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.saveAllButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.saveAllButton.Location = new System.Drawing.Point( 388, 12 );
            this.saveAllButton.Name = "saveAllButton";
            this.saveAllButton.Size = new System.Drawing.Size( 87, 23 );
            this.saveAllButton.TabIndex = 1;
            this.saveAllButton.Text = "Save All";
            this.saveAllButton.UseVisualStyleBackColor = true;
            this.saveAllButton.Click += new System.EventHandler( this.saveAllButton_Click );
            // 
            // saveSelectedButton
            // 
            this.saveSelectedButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.saveSelectedButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.saveSelectedButton.Location = new System.Drawing.Point( 388, 41 );
            this.saveSelectedButton.Name = "saveSelectedButton";
            this.saveSelectedButton.Size = new System.Drawing.Size( 87, 23 );
            this.saveSelectedButton.TabIndex = 2;
            this.saveSelectedButton.Text = "Save Selected";
            this.saveSelectedButton.UseVisualStyleBackColor = true;
            this.saveSelectedButton.Click += new System.EventHandler( this.saveSelectedButton_Click );
            // 
            // saveNoneButton
            // 
            this.saveNoneButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.saveNoneButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.saveNoneButton.Location = new System.Drawing.Point( 388, 70 );
            this.saveNoneButton.Name = "saveNoneButton";
            this.saveNoneButton.Size = new System.Drawing.Size( 87, 23 );
            this.saveNoneButton.TabIndex = 3;
            this.saveNoneButton.Text = "Save None";
            this.saveNoneButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 388, 99 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 87, 23 );
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // SaveForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size( 487, 210 );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.saveNoneButton );
            this.Controls.Add( this.saveSelectedButton );
            this.Controls.Add( this.saveAllButton );
            this.Controls.Add( this.unsavedFilesListBox );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SaveForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Exiting With Modified Files";
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.ListBox unsavedFilesListBox;
        private System.Windows.Forms.Button saveAllButton;
        private System.Windows.Forms.Button saveSelectedButton;
        private System.Windows.Forms.Button saveNoneButton;
        private System.Windows.Forms.Button cancelButton;

    }
}