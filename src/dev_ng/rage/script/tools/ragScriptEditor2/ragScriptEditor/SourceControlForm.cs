using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ragScriptEditorShared;

namespace ragScriptEditor
{
    public partial class SourceControlForm : Form
    {
        public SourceControlForm()
        {
            InitializeComponent();
        }

        public SourceControlForm( string filename, SourceControl.PromptType type )
        {
            InitializeComponent();

            m_filename = filename;
            m_promptType = type;
        }
        
        /// <summary>
        /// Creates a SourceControlForm dialog and shows it.  Returns the result
        /// </summary>
        /// <param name="parentForm"></param>
        /// <param name="filename"></param>
        /// <param name="type">determines which buttons are available</param>
        /// <returns>what action was selected</returns>
        static public SourceControl.Action ShowIt( System.Windows.Forms.Form parentForm, string filename, SourceControl.PromptType type )
        {
            SourceControlForm prompt = new SourceControlForm( filename, type );

            DialogResult result = prompt.ShowDialog( parentForm );
            if ( result == DialogResult.OK )
            {
                return prompt.m_sourceControlAction;
            }

            return SourceControl.Action.None;
        }

        #region Variables
        private string m_filename;
        private SourceControl.PromptType m_promptType;
        private SourceControl.Action m_sourceControlAction = SourceControl.Action.None;
        #endregion

        #region Event Handlers
        private void SourceControlForm_Activated( object sender, EventArgs e )
        {
            System.Text.StringBuilder text = new System.Text.StringBuilder();
            text.Append( "'" );
            text.Append( m_filename );
            text.Append( "' is checked in under Source Control.\n\nWould you like to check it out?" );
            this.fileLabel.Text = text.ToString();

            switch ( m_promptType )
            {
            case SourceControl.PromptType.Compile:
                this.checkOutAllButton.Enabled = true;
                this.overwriteButton.Enabled = true;
                this.overwriteAllButton.Enabled = true;
                this.cancelButton.Enabled = false;
                this.cancelButton.Text = "Cancel";
                break;
            case SourceControl.PromptType.Edit:
                this.checkOutAllButton.Enabled = false;
                this.overwriteButton.Enabled = false;
                this.overwriteAllButton.Enabled = false;
                this.cancelButton.Enabled = true;
                this.cancelButton.Text = "Skip";
                break;
            case SourceControl.PromptType.EditAll:
                this.checkOutAllButton.Enabled = true;
                this.overwriteButton.Enabled = true;
                this.overwriteAllButton.Enabled = true;
                this.cancelButton.Enabled = true;
                this.cancelButton.Text = "Skip";
                break;
            case SourceControl.PromptType.Save:
                this.checkOutAllButton.Enabled = false;
                this.overwriteButton.Enabled = true;
                this.overwriteAllButton.Enabled = false;
                this.cancelButton.Enabled = true;
                this.cancelButton.Text = "Cancel";
                break;
            case SourceControl.PromptType.SaveAll:
                this.checkOutAllButton.Enabled = true;
                this.overwriteButton.Enabled = true;
                this.overwriteAllButton.Enabled = true;
                this.cancelButton.Enabled = true;
                this.cancelButton.Text = "Cancel";
                break;
            }
        }

        private void checkOutButton_Click( object sender, EventArgs e )
        {
            m_sourceControlAction = SourceControl.Action.CheckOut;
        }

        private void checkOutAllButton_Click( object sender, EventArgs e )
        {
            m_sourceControlAction = SourceControl.Action.CheckOutAll;
        }

        private void overwriteButton_Click( object sender, EventArgs e )
        {
            m_sourceControlAction = SourceControl.Action.Overwrite;
        }

        private void overwriteAllButton_Click( object sender, EventArgs e )
        {
            m_sourceControlAction = SourceControl.Action.OverwriteAll;
        }

        private void cancelButton_Click( object sender, EventArgs e )
        {
            m_sourceControlAction = SourceControl.Action.Cancel;
        }        
        #endregion
    }       
}