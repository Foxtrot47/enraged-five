namespace ragScriptEditor
{
    partial class CompilingSettingsHostControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.configurationLabel = new System.Windows.Forms.Label();
            this.configurationComboBox = new System.Windows.Forms.ComboBox();
            this.addConfigurationButton = new System.Windows.Forms.Button();
            this.removeConfigurationButton = new System.Windows.Forms.Button();
            this.currentConfigurationGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.breakOnErrorCheckBox = new System.Windows.Forms.CheckBox();
            this.openFilesWithErrorsCheckBox = new System.Windows.Forms.CheckBox();
            this.configurationGroupBox = new System.Windows.Forms.GroupBox();
            this.currentConfigurationGroupBox.SuspendLayout();
            this.configurationGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // configurationLabel
            // 
            this.configurationLabel.AutoSize = true;
            this.configurationLabel.Location = new System.Drawing.Point( 11, 16 );
            this.configurationLabel.Name = "configurationLabel";
            this.configurationLabel.Size = new System.Drawing.Size( 69, 13 );
            this.configurationLabel.TabIndex = 0;
            this.configurationLabel.Text = "Configuration";
            // 
            // configurationComboBox
            // 
            this.configurationComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.configurationComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.configurationComboBox.FormattingEnabled = true;
            this.configurationComboBox.Location = new System.Drawing.Point( 86, 13 );
            this.configurationComboBox.Name = "configurationComboBox";
            this.configurationComboBox.Size = new System.Drawing.Size( 367, 21 );
            this.configurationComboBox.TabIndex = 1;
            this.configurationComboBox.SelectedIndexChanged += new System.EventHandler( this.configurationComboBox_SelectedIndexChanged );
            // 
            // addConfigurationButton
            // 
            this.addConfigurationButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addConfigurationButton.Location = new System.Drawing.Point( 459, 11 );
            this.addConfigurationButton.Name = "addConfigurationButton";
            this.addConfigurationButton.Size = new System.Drawing.Size( 20, 23 );
            this.addConfigurationButton.TabIndex = 2;
            this.addConfigurationButton.Text = "+";
            this.addConfigurationButton.UseVisualStyleBackColor = true;
            this.addConfigurationButton.Click += new System.EventHandler( this.addConfigurationButton_Click );
            // 
            // removeConfigurationButton
            // 
            this.removeConfigurationButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.removeConfigurationButton.Location = new System.Drawing.Point( 485, 11 );
            this.removeConfigurationButton.Name = "removeConfigurationButton";
            this.removeConfigurationButton.Size = new System.Drawing.Size( 20, 23 );
            this.removeConfigurationButton.TabIndex = 3;
            this.removeConfigurationButton.Text = "-";
            this.removeConfigurationButton.UseVisualStyleBackColor = true;
            this.removeConfigurationButton.Click += new System.EventHandler( this.removeConfigurationButton_Click );
            // 
            // currentConfigurationGroupBox
            // 
            this.currentConfigurationGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.currentConfigurationGroupBox.Controls.Add( this.groupBox1 );
            this.currentConfigurationGroupBox.Location = new System.Drawing.Point( 14, 40 );
            this.currentConfigurationGroupBox.Name = "currentConfigurationGroupBox";
            this.currentConfigurationGroupBox.Size = new System.Drawing.Size( 494, 250 );
            this.currentConfigurationGroupBox.TabIndex = 4;
            this.currentConfigurationGroupBox.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point( 3, 16 );
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size( 488, 231 );
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            this.groupBox1.Visible = false;
            // 
            // breakOnErrorCheckBox
            // 
            this.breakOnErrorCheckBox.AutoSize = true;
            this.breakOnErrorCheckBox.Location = new System.Drawing.Point( 9, 3 );
            this.breakOnErrorCheckBox.Name = "breakOnErrorCheckBox";
            this.breakOnErrorCheckBox.Size = new System.Drawing.Size( 93, 17 );
            this.breakOnErrorCheckBox.TabIndex = 5;
            this.breakOnErrorCheckBox.Text = "Break on error";
            this.breakOnErrorCheckBox.UseVisualStyleBackColor = true;
            this.breakOnErrorCheckBox.CheckedChanged += new System.EventHandler( this.breakOnErrorCheckBox_CheckedChanged );
            // 
            // openFilesWithErrorsCheckBox
            // 
            this.openFilesWithErrorsCheckBox.AutoSize = true;
            this.openFilesWithErrorsCheckBox.Location = new System.Drawing.Point( 136, 3 );
            this.openFilesWithErrorsCheckBox.Name = "openFilesWithErrorsCheckBox";
            this.openFilesWithErrorsCheckBox.Size = new System.Drawing.Size( 124, 17 );
            this.openFilesWithErrorsCheckBox.TabIndex = 6;
            this.openFilesWithErrorsCheckBox.Text = "Open files with errors";
            this.openFilesWithErrorsCheckBox.UseVisualStyleBackColor = true;
            this.openFilesWithErrorsCheckBox.CheckedChanged += new System.EventHandler( this.openFilesWithErrorsCheckBox_CheckedChanged );
            // 
            // configurationGroupBox
            // 
            this.configurationGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.configurationGroupBox.Controls.Add( this.configurationLabel );
            this.configurationGroupBox.Controls.Add( this.configurationComboBox );
            this.configurationGroupBox.Controls.Add( this.addConfigurationButton );
            this.configurationGroupBox.Controls.Add( this.currentConfigurationGroupBox );
            this.configurationGroupBox.Controls.Add( this.removeConfigurationButton );
            this.configurationGroupBox.Location = new System.Drawing.Point( -9, 26 );
            this.configurationGroupBox.Name = "configurationGroupBox";
            this.configurationGroupBox.Size = new System.Drawing.Size( 520, 304 );
            this.configurationGroupBox.TabIndex = 7;
            this.configurationGroupBox.TabStop = false;
            // 
            // CompilingTabSettingsHostControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.openFilesWithErrorsCheckBox );
            this.Controls.Add( this.configurationGroupBox );
            this.Controls.Add( this.breakOnErrorCheckBox );
            this.Name = "CompilingTabSettingsHostControl";
            this.Size = new System.Drawing.Size( 500, 319 );
            this.currentConfigurationGroupBox.ResumeLayout( false );
            this.configurationGroupBox.ResumeLayout( false );
            this.configurationGroupBox.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label configurationLabel;
        private System.Windows.Forms.ComboBox configurationComboBox;
        private System.Windows.Forms.Button addConfigurationButton;
        private System.Windows.Forms.Button removeConfigurationButton;
        private System.Windows.Forms.GroupBox currentConfigurationGroupBox;
        private System.Windows.Forms.CheckBox breakOnErrorCheckBox;
        private System.Windows.Forms.CheckBox openFilesWithErrorsCheckBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox configurationGroupBox;
    }
}
