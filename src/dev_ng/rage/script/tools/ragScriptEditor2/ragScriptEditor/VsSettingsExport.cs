﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace ragScriptEditor
{
    /// <summary>
    /// Writes out a Visual Studio style settings file.
    /// </summary>
    public class VsSettingsExport
    {
        #region Constants

        /// <summary>
        /// Default background colour. 
        /// </summary>
        static readonly string c_defaultBackground = "0x02000000";

        /// <summary>
        /// Default font weight.
        /// </summary>
        static readonly string c_fontBold = "No";

        #endregion

        #region Private member fields

        private string m_settingsTemplate;
        private Dictionary<string, Dictionary<ColourPlacement, string>> m_colours;

        #endregion

        #region Public enumerations

        /// <summary>
        /// Colour placement.
        /// </summary>
        public enum ColourPlacement
        {
            /// <summary>
            /// Colour represents the foreground.
            /// </summary>
            Foreground,

            /// <summary>
            /// Colour represents the background.
            /// </summary>
            Background
        }

        /// <summary>
        /// Status writing out to the settings file.
        /// </summary>
        public enum Status
        {
            /// <summary>
            /// Operation was successful.
            /// </summary>
            Success,

            /// <summary>
            /// The template could not be found.
            /// </summary>
            TemplateNotFound,

            /// <summary>
            /// Problem writing to the settings file.
            /// </summary>
            ErrorWriting
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="settingsTemplate">Path to the settings template.</param>
        public VsSettingsExport(string settingsTemplate)
        {
            m_settingsTemplate = settingsTemplate;
            m_colours = new Dictionary<string, Dictionary<ColourPlacement, string>>();
        }

        #endregion

        /// <summary>
        /// Set the colour value.
        /// </summary>
        /// <param name="colourName">Colour name. E.g. SanScript.Default.</param>
        /// <param name="colourPlacement">Colour placement; foreground or background colour.</param>
        /// <param name="red">Colour's red component.</param>
        /// <param name="green">Colour's green component.</param>
        /// <param name="blue">Colour's blue component.</param>
        public void SetColour(string colourName, ColourPlacement colourPlacement, int red, int green, int blue)
        {
            Dictionary<ColourPlacement, string> colour = null;
            if (!m_colours.TryGetValue(colourName, out colour))
            {
                m_colours[colourName] = new Dictionary<ColourPlacement, string>();
                colour = m_colours[colourName];
            }

            m_colours[colourName][colourPlacement] = ColourToHex(red, green, blue);
        }

        /// <summary>
        /// Write the settings file to disk.
        /// </summary>
        /// <param name="destination">Destination.</param>
        /// <returns>The status of the operation.</returns>
        public Status WriteSettings(string destination)
        {
            if (!File.Exists(m_settingsTemplate))
            {
                return Status.TemplateNotFound;
            }

            try
            {
                if (File.Exists(destination))
                {
                    File.Delete(destination);
                }
            }
            catch
            {
                return Status.ErrorWriting;
            }

            return WriteToSettings(destination);
        }

        #region Private helper methods

        /// <summary>
        /// Write to the settings file.
        /// </summary>
        /// <param name="destination"></param>
        /// <returns></returns>
        private Status WriteToSettings(string destination)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(m_settingsTemplate);

            foreach (XmlNode node in doc.SelectNodes("//Item"))
            {
                foreach (string key in m_colours.Keys)
                {
                    if (node.Attributes["Name"].Value == key)
                    {
                        node.Attributes.Remove(node.Attributes["BoldFont"]);
                        node.Attributes.Remove(node.Attributes["Background"]);
                        node.Attributes.Remove(node.Attributes["Foreground"]);

                        foreach (ColourPlacement placement in m_colours[key].Keys)
                        {
                            XmlAttribute attr = doc.CreateAttribute(placement.ToString());
                            attr.Value = m_colours[key][placement];
                            node.Attributes.Append(attr);
                        }

                        if (node.Attributes["Background"] == null)
                        {
                            XmlAttribute attr = doc.CreateAttribute("Background");
                            attr.Value = c_defaultBackground;
                            node.Attributes.Append(attr);
                        }

                        XmlAttribute fontBold = doc.CreateAttribute("BoldFont");
                        fontBold.Value = c_fontBold;
                        node.Attributes.Append(fontBold);
                    }
                }
            }

            doc.Save(destination);
            return Status.Success;
        }

        /// <summary>
        /// Convert the colour's components to a hex string value.
        /// </summary>
        /// <param name="r">Colour's red component.</param>
        /// <param name="g">Colour's green component.</param>
        /// <param name="b">Colour's blue component.</param>
        /// <returns>Hex string value of the colour in the format ABGR</returns>
        private string ColourToHex(int r, int g, int b)
        {
            return String.Format("0x00{0:X2}{1:X2}{2:X2}", b, g, r);
        }

        #endregion
    }
}
