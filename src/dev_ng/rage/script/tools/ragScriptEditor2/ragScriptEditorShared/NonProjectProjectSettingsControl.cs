using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragScriptEditorShared
{
    public partial class NonProjectProjectSettingsControl : ragScriptEditorShared.ProjectSettingsControl
    {
        public NonProjectProjectSettingsControl()
        {
            InitializeComponent();
        }

        #region Overrides
        public override SettingsBase TabSettings
        {
            get
            {
                SettingsBase settings = base.TabSettings;

                NonProjectProjectSettings projectSettings = new NonProjectProjectSettings();
                projectSettings.Copy( settings as ProjectSettings );

                projectSettings.ParsingServiceEnabled = this.enableSemanticParsingCheckBox.Checked;

                return base.TabSettings;
            }
            set
            {
                base.TabSettings = value;

                if ( value is NonProjectProjectSettings )
                {
                    m_invokeSettingsChanged = false;

                    NonProjectProjectSettings projectSettings = value as NonProjectProjectSettings;

                    this.enableSemanticParsingCheckBox.Checked = projectSettings.ParsingServiceEnabled;

                    m_invokeSettingsChanged = true;
                }
            }
        }
        #endregion

        #region Event Handlers
        private void enableSemanticParsingCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            OnSettingsChanged();
        }
        #endregion
    }
}

