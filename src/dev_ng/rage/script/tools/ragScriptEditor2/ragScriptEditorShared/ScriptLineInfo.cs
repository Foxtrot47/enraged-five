﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Base.Logging;

namespace ragScriptEditorShared
{
    /// <summary>
    /// Represents the LINEINFO information for a provided scd file. Mapping program counter values to filenames, lines, and columns.
    /// </summary>
    public class ScriptLineInfo
    {
        private ScriptLineInfo(String filename, int lineNumber)
        {
            Filename = filename;
            LineNumber = lineNumber;
        }

        /// <summary>
        /// The filename the script counter refers to.
        /// </summary>
        public String Filename
        {
            get;
        }

        /// <summary>
        /// The (zero-indexed) line number corresponding to the file.
        /// </summary>
        public int LineNumber
        {
            get;
        }

        /// <summary>
        /// Load an SCD file and extract file/line info for a specific program counter.
        /// </summary>
        /// <param name="scdFile"></param>
        /// <param name="programCounter"></param>
        /// <returns></returns>
        public static ScriptLineInfo Load(ILog log, String scdFile, int programCounter)
        {
            try
            {
                IEnumerable<string> lines = File.ReadLines(scdFile);
                IEnumerable<string> lineInfoBlock = lines.SkipWhile(l => !l.Trim().Equals("[LINEINFO]", StringComparison.OrdinalIgnoreCase)).Skip(1);
                if (!lineInfoBlock.Any())
                {
                    return null;
                }

                int numLineinfoLines = int.Parse(lineInfoBlock.First());
                IEnumerable<string> lineInfoLines = lineInfoBlock.Skip(1).Take(numLineinfoLines).ToList();

                if (lineInfoLines.Count() != numLineinfoLines)
                {
                    return null;
                }

                IEnumerable<string> fileInfoBlock = lineInfoBlock.Skip(numLineinfoLines + 1);
                if (!fileInfoBlock.Any())
                {
                    return null;
                }

                int numFiles = int.Parse(fileInfoBlock.First());
                IEnumerable<string> files = fileInfoBlock.Skip(1).Take(numFiles).ToList();
                if (files.Count() != numFiles)
                {
                    return null;
                }

                int[] matchingLineInfo = lineInfoLines.Select(
                    l => l.Split(':')
                        .Select(s => int.Parse(s))
                        .ToArray())
                    .LastOrDefault(li => li[0] <= programCounter);
                if (matchingLineInfo == null)
                {
                    return null;
                }

                string filename = files.ElementAtOrDefault(matchingLineInfo[2]);
                if (filename == null)
                {
                    return null;
                }

                filename = Path.GetFullPath(filename);

                // Note: We're using zero-indexed line numbers, so subtract 1.
                int lineNumber = matchingLineInfo[1] - 1;

                return new ScriptLineInfo(filename, lineNumber);
            }
            catch (Exception ex)
            {
                log.ToolException(ex, $"Failed to load {scdFile} to get program counter {programCounter}");
                return null;
            }
        }
    }
}
