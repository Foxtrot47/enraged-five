namespace ragScriptEditorShared
{
    partial class CompilingSettingsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.includeFileLabel = new System.Windows.Forms.Label();
            this.includeFileTextBox = new System.Windows.Forms.TextBox();
            this.includeFileBrowseButton = new System.Windows.Forms.Button();
            this.includePathLabel = new System.Windows.Forms.Label();
            this.includePathTextBox = new System.Windows.Forms.TextBox();
            this.includePathBrowseButton = new System.Windows.Forms.Button();
            this.compilerExecutableLabel = new System.Windows.Forms.Label();
            this.compilerExecutableTextBox = new System.Windows.Forms.TextBox();
            this.compilerExecutableBrowseButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.outputDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.outputDirectoryBrowseButton = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.postCompileCommandBrowseButton = new System.Windows.Forms.Button();
            this.postCompileCommandTextBox = new System.Windows.Forms.TextBox();
            this.postCompileCommandLabel = new System.Windows.Forms.Label();
            this.resourceExecutableBrowseButton = new System.Windows.Forms.Button();
            this.resourceExecutableTextBox = new System.Windows.Forms.TextBox();
            this.resourceExecutableLabel = new System.Windows.Forms.Label();
            this.buildDirectoryBrowseButton = new System.Windows.Forms.Button();
            this.buildDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // includeFileLabel
            // 
            this.includeFileLabel.AutoSize = true;
            this.includeFileLabel.Location = new System.Drawing.Point(58, 6);
            this.includeFileLabel.Name = "includeFileLabel";
            this.includeFileLabel.Size = new System.Drawing.Size(61, 13);
            this.includeFileLabel.TabIndex = 0;
            this.includeFileLabel.Text = "Include File";
            // 
            // includeFileTextBox
            // 
            this.includeFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.includeFileTextBox.Location = new System.Drawing.Point(125, 3);
            this.includeFileTextBox.Name = "includeFileTextBox";
            this.includeFileTextBox.Size = new System.Drawing.Size(345, 20);
            this.includeFileTextBox.TabIndex = 1;
            this.includeFileTextBox.TextChanged += new System.EventHandler(this.SettingChanged);
            // 
            // includeFileBrowseButton
            // 
            this.includeFileBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.includeFileBrowseButton.Location = new System.Drawing.Point(476, 1);
            this.includeFileBrowseButton.Name = "includeFileBrowseButton";
            this.includeFileBrowseButton.Size = new System.Drawing.Size(25, 23);
            this.includeFileBrowseButton.TabIndex = 2;
            this.includeFileBrowseButton.Text = "...";
            this.includeFileBrowseButton.UseVisualStyleBackColor = true;
            this.includeFileBrowseButton.Click += new System.EventHandler(this.includeFileBrowseButton_Click);
            // 
            // includePathLabel
            // 
            this.includePathLabel.AutoSize = true;
            this.includePathLabel.Location = new System.Drawing.Point(47, 32);
            this.includePathLabel.Name = "includePathLabel";
            this.includePathLabel.Size = new System.Drawing.Size(72, 13);
            this.includePathLabel.TabIndex = 3;
            this.includePathLabel.Text = "Include Paths";
            // 
            // includePathTextBox
            // 
            this.includePathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.includePathTextBox.Location = new System.Drawing.Point(125, 29);
            this.includePathTextBox.Multiline = true;
            this.includePathTextBox.Name = "includePathTextBox";
            this.includePathTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.includePathTextBox.Size = new System.Drawing.Size(345, 60);
            this.includePathTextBox.TabIndex = 4;
            this.includePathTextBox.WordWrap = false;
            this.includePathTextBox.TextChanged += new System.EventHandler(this.SettingChanged);
            // 
            // includePathBrowseButton
            // 
            this.includePathBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.includePathBrowseButton.Location = new System.Drawing.Point(476, 27);
            this.includePathBrowseButton.Name = "includePathBrowseButton";
            this.includePathBrowseButton.Size = new System.Drawing.Size(25, 23);
            this.includePathBrowseButton.TabIndex = 5;
            this.includePathBrowseButton.Text = "...";
            this.includePathBrowseButton.UseVisualStyleBackColor = true;
            this.includePathBrowseButton.Click += new System.EventHandler(this.includePathBrowseButton_Click);
            // 
            // compilerExecutableLabel
            // 
            this.compilerExecutableLabel.AutoSize = true;
            this.compilerExecutableLabel.Location = new System.Drawing.Point(16, 98);
            this.compilerExecutableLabel.Name = "compilerExecutableLabel";
            this.compilerExecutableLabel.Size = new System.Drawing.Size(103, 13);
            this.compilerExecutableLabel.TabIndex = 6;
            this.compilerExecutableLabel.Text = "Compiler Executable";
            // 
            // compilerExecutableTextBox
            // 
            this.compilerExecutableTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.compilerExecutableTextBox.Location = new System.Drawing.Point(125, 95);
            this.compilerExecutableTextBox.Name = "compilerExecutableTextBox";
            this.compilerExecutableTextBox.Size = new System.Drawing.Size(345, 20);
            this.compilerExecutableTextBox.TabIndex = 7;
            this.compilerExecutableTextBox.TextChanged += new System.EventHandler(this.SettingChanged);
            // 
            // compilerExecutableBrowseButton
            // 
            this.compilerExecutableBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.compilerExecutableBrowseButton.Location = new System.Drawing.Point(476, 93);
            this.compilerExecutableBrowseButton.Name = "compilerExecutableBrowseButton";
            this.compilerExecutableBrowseButton.Size = new System.Drawing.Size(25, 23);
            this.compilerExecutableBrowseButton.TabIndex = 8;
            this.compilerExecutableBrowseButton.Text = "...";
            this.compilerExecutableBrowseButton.UseVisualStyleBackColor = true;
            this.compilerExecutableBrowseButton.Click += new System.EventHandler(this.compilerExecutableBrowseButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Output Directory";
            // 
            // outputDirectoryTextBox
            // 
            this.outputDirectoryTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputDirectoryTextBox.Location = new System.Drawing.Point(125, 146);
            this.outputDirectoryTextBox.Name = "outputDirectoryTextBox";
            this.outputDirectoryTextBox.Size = new System.Drawing.Size(345, 20);
            this.outputDirectoryTextBox.TabIndex = 13;
            this.outputDirectoryTextBox.TextChanged += new System.EventHandler(this.SettingChanged);
            // 
            // outputDirectoryBrowseButton
            // 
            this.outputDirectoryBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.outputDirectoryBrowseButton.Location = new System.Drawing.Point(476, 144);
            this.outputDirectoryBrowseButton.Name = "outputDirectoryBrowseButton";
            this.outputDirectoryBrowseButton.Size = new System.Drawing.Size(25, 23);
            this.outputDirectoryBrowseButton.TabIndex = 14;
            this.outputDirectoryBrowseButton.Text = "...";
            this.outputDirectoryBrowseButton.UseVisualStyleBackColor = true;
            this.outputDirectoryBrowseButton.Click += new System.EventHandler(this.outputDirectoryBrowseButton_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.RestoreDirectory = true;
            // 
            // postCompileCommandBrowseButton
            // 
            this.postCompileCommandBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.postCompileCommandBrowseButton.Location = new System.Drawing.Point(476, 196);
            this.postCompileCommandBrowseButton.Name = "postCompileCommandBrowseButton";
            this.postCompileCommandBrowseButton.Size = new System.Drawing.Size(25, 23);
            this.postCompileCommandBrowseButton.TabIndex = 16;
            this.postCompileCommandBrowseButton.Text = "...";
            this.postCompileCommandBrowseButton.UseVisualStyleBackColor = true;
            this.postCompileCommandBrowseButton.Click += new System.EventHandler(this.postCompileCommandBrowseButton_Click);
            // 
            // postCompileCommandTextBox
            // 
            this.postCompileCommandTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.postCompileCommandTextBox.Location = new System.Drawing.Point(125, 198);
            this.postCompileCommandTextBox.Name = "postCompileCommandTextBox";
            this.postCompileCommandTextBox.Size = new System.Drawing.Size(345, 20);
            this.postCompileCommandTextBox.TabIndex = 15;
            this.postCompileCommandTextBox.TextChanged += new System.EventHandler(this.SettingChanged);
            // 
            // postCompileCommandLabel
            // 
            this.postCompileCommandLabel.AutoSize = true;
            this.postCompileCommandLabel.Location = new System.Drawing.Point(1, 201);
            this.postCompileCommandLabel.Name = "postCompileCommandLabel";
            this.postCompileCommandLabel.Size = new System.Drawing.Size(118, 13);
            this.postCompileCommandLabel.TabIndex = 14;
            this.postCompileCommandLabel.Text = "Post-Compile Command";
            // 
            // resourceExecutableBrowseButton
            // 
            this.resourceExecutableBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.resourceExecutableBrowseButton.Location = new System.Drawing.Point(476, 119);
            this.resourceExecutableBrowseButton.Name = "resourceExecutableBrowseButton";
            this.resourceExecutableBrowseButton.Size = new System.Drawing.Size(25, 23);
            this.resourceExecutableBrowseButton.TabIndex = 11;
            this.resourceExecutableBrowseButton.Text = "...";
            this.resourceExecutableBrowseButton.UseVisualStyleBackColor = true;
            this.resourceExecutableBrowseButton.Click += new System.EventHandler(this.resourceExecutableBrowseButton_Click);
            // 
            // resourceExecutableTextBox
            // 
            this.resourceExecutableTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resourceExecutableTextBox.Location = new System.Drawing.Point(125, 121);
            this.resourceExecutableTextBox.Name = "resourceExecutableTextBox";
            this.resourceExecutableTextBox.Size = new System.Drawing.Size(345, 20);
            this.resourceExecutableTextBox.TabIndex = 10;
            this.resourceExecutableTextBox.TextChanged += new System.EventHandler(this.SettingChanged);
            // 
            // resourceExecutableLabel
            // 
            this.resourceExecutableLabel.AutoSize = true;
            this.resourceExecutableLabel.Location = new System.Drawing.Point(10, 124);
            this.resourceExecutableLabel.Name = "resourceExecutableLabel";
            this.resourceExecutableLabel.Size = new System.Drawing.Size(109, 13);
            this.resourceExecutableLabel.TabIndex = 9;
            this.resourceExecutableLabel.Text = "Resource Executable";
            // 
            // buildDirectoryBrowseButton
            // 
            this.buildDirectoryBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buildDirectoryBrowseButton.Location = new System.Drawing.Point(476, 170);
            this.buildDirectoryBrowseButton.Name = "buildDirectoryBrowseButton";
            this.buildDirectoryBrowseButton.Size = new System.Drawing.Size(25, 23);
            this.buildDirectoryBrowseButton.TabIndex = 19;
            this.buildDirectoryBrowseButton.Text = "...";
            this.buildDirectoryBrowseButton.UseVisualStyleBackColor = true;
            this.buildDirectoryBrowseButton.Click += new System.EventHandler(this.buildDirectoryBrowseButton_Click);
            // 
            // buildDirectoryTextBox
            // 
            this.buildDirectoryTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buildDirectoryTextBox.Location = new System.Drawing.Point(125, 172);
            this.buildDirectoryTextBox.Name = "buildDirectoryTextBox";
            this.buildDirectoryTextBox.Size = new System.Drawing.Size(345, 20);
            this.buildDirectoryTextBox.TabIndex = 18;
            this.buildDirectoryTextBox.TextChanged += new System.EventHandler(this.SettingChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 175);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Build Directory";
            // 
            // CompilingSettingsControl
            // 
            this.Controls.Add(this.buildDirectoryBrowseButton);
            this.Controls.Add(this.buildDirectoryTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.resourceExecutableBrowseButton);
            this.Controls.Add(this.resourceExecutableTextBox);
            this.Controls.Add(this.resourceExecutableLabel);
            this.Controls.Add(this.postCompileCommandBrowseButton);
            this.Controls.Add(this.postCompileCommandTextBox);
            this.Controls.Add(this.postCompileCommandLabel);
            this.Controls.Add(this.outputDirectoryBrowseButton);
            this.Controls.Add(this.outputDirectoryTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.compilerExecutableBrowseButton);
            this.Controls.Add(this.compilerExecutableTextBox);
            this.Controls.Add(this.compilerExecutableLabel);
            this.Controls.Add(this.includePathBrowseButton);
            this.Controls.Add(this.includePathTextBox);
            this.Controls.Add(this.includePathLabel);
            this.Controls.Add(this.includeFileBrowseButton);
            this.Controls.Add(this.includeFileTextBox);
            this.Controls.Add(this.includeFileLabel);
            this.Name = "CompilingSettingsControl";
            this.Size = new System.Drawing.Size(504, 223);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label includeFileLabel;
        private System.Windows.Forms.TextBox includeFileTextBox;
        private System.Windows.Forms.Button includeFileBrowseButton;
        private System.Windows.Forms.Label includePathLabel;
        private System.Windows.Forms.TextBox includePathTextBox;
        private System.Windows.Forms.Button includePathBrowseButton;
        private System.Windows.Forms.Label compilerExecutableLabel;
        private System.Windows.Forms.TextBox compilerExecutableTextBox;
        private System.Windows.Forms.Button compilerExecutableBrowseButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox outputDirectoryTextBox;
        private System.Windows.Forms.Button outputDirectoryBrowseButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button postCompileCommandBrowseButton;
        private System.Windows.Forms.TextBox postCompileCommandTextBox;
        private System.Windows.Forms.Label postCompileCommandLabel;
        private System.Windows.Forms.Button resourceExecutableBrowseButton;
        private System.Windows.Forms.TextBox resourceExecutableTextBox;
        private System.Windows.Forms.Label resourceExecutableLabel;
        private System.Windows.Forms.Button buildDirectoryBrowseButton;
        private System.Windows.Forms.TextBox buildDirectoryTextBox;
        private System.Windows.Forms.Label label1;
    }
}
