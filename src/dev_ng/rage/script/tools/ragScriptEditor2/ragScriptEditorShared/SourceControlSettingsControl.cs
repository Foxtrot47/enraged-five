using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Forms.SCM;

namespace ragScriptEditorShared
{
    public partial class SourceControlSettingsControl : ragScriptEditorShared.SettingsControlBase
    {
        public SourceControlSettingsControl()
        {
            InitializeComponent();
        }

        #region Overrides 
        public override SettingsBase TabSettings
        {
            get
            {
                SourceControlSettings2 srcCtrlSettings = new SourceControlSettings2();
                srcCtrlSettings.Enabled = this.enabledCheckBox.Checked;
                srcCtrlSettings.OnCompile = (SourceControlSettings2.CompileAction)this.onCompileComboBox.SelectedIndex;
                srcCtrlSettings.OnEdit = (SourceControlSettings2.EditAction)this.onEditComboBox.SelectedIndex;
                srcCtrlSettings.OnSave = (SourceControlSettings2.SaveAction)this.onSaveComboBox.SelectedIndex;

                if ( this.sourceControlProviderControl.Providers.Count == srcCtrlSettings.SourceControlProviderSettings.Providers.Count )
                {
                    for ( int i = 0; i < srcCtrlSettings.SourceControlProviderSettings.Providers.Count; ++i )
                    {
                        srcCtrlSettings.SourceControlProviderSettings.Providers[i].CopyFrom( this.sourceControlProviderControl.Providers[i] );
                    }

                    srcCtrlSettings.SourceControlProviderSettings.SelectedIndex = this.sourceControlProviderControl.SelectedIndex;
                }

                return srcCtrlSettings;
            }
            set
            {
                if ( value is SourceControlSettings2 )
                {
                    m_invokeSettingsChanged = false;

                    SourceControlSettings2 srcCtrlSettings = value as SourceControlSettings2;

                    this.enabledCheckBox.Checked = srcCtrlSettings.Enabled;
                    this.onCompileComboBox.SelectedIndex = (int)srcCtrlSettings.OnCompile;
                    this.onEditComboBox.SelectedIndex = (int)srcCtrlSettings.OnEdit;
                    this.onSaveComboBox.SelectedIndex = (int)srcCtrlSettings.OnSave;

                    List<rageSourceControlProvider> providers = new List<rageSourceControlProvider>();
                    foreach ( rageSourceControlProvider provider in srcCtrlSettings.SourceControlProviderSettings.Providers )
                    {
                        providers.Add( provider.Clone() );
                    }

                    this.sourceControlProviderControl.Providers = providers;
                    this.sourceControlProviderControl.SelectedIndex = srcCtrlSettings.SourceControlProviderSettings.SelectedIndex;

                    enabledCheckBox_CheckedChanged( this, EventArgs.Empty );

                    m_invokeSettingsChanged = true;
                }
            }
        }
        #endregion

        #region Event Handlers
        private void enabledCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.enabledGroupBox.Enabled = this.enabledCheckBox.Checked;

            OnSettingsChanged();
        }

        private void SettingChanged( object sender, EventArgs e )
        {
            OnSettingsChanged();
        }
        #endregion
    }
}

