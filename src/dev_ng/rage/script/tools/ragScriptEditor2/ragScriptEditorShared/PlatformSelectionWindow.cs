﻿//---------------------------------------------------------------------------------------------
// <copyright file="PlatformSelectionWindow.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2019. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace ragScriptEditorShared
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using RSG.Platform;
    using System.Windows.Forms;

    public partial class PlatformSelectionWindow : Form
    {
        public PlatformSelectionWindow()
        {
            InitializeComponent();

            foreach (Platform platform in Enum.GetValues(typeof(Platform)))
            {
                if (platform == Platform.Independent)
                {
                    continue;
                }

                CheckBox platformCheckbox = new CheckBox();
                platformCheckbox.Text = platform.PlatformToFriendlyName();
                platformCheckbox.Tag = platform;
                platformCheckbox.AutoSize = true;

                this.flowLayoutPanel1.Controls.Add(platformCheckbox);
            }
        }

        /// <summary>
        /// Gets or sets the input window's text contents
        /// </summary>
        public IEnumerable<Platform> SupportedPlatforms
        {
            get
            {
                return this.flowLayoutPanel1
                    .Controls
                    .OfType<CheckBox>()
                    .Where(c => c.Checked)
                    .Select(c => (Platform)c.Tag)
                    .ToArray();
            }
            set
            {
                foreach (CheckBox c in this.flowLayoutPanel1.Controls)
                {
                    c.Checked = value.Contains((Platform)c.Tag);
                }
            }
        }
    }
}
