using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace ragScriptEditorShared
{
    /// <summary>
    /// Base class for settings class that intends to use any of the UITypeEditors found in this file.
    /// </summary>
    public abstract class PropertyGridSettingsBase
    {
        public PropertyGridSettingsBase()
        {

        }

        #region Variables
        protected Dictionary<string, string> m_fileBrowserTitles = new Dictionary<string, string>();
        protected Dictionary<string, string> m_fileBrowserFilters = new Dictionary<string, string>();
        protected Dictionary<string, string> m_folderBrowserDescriptions = new Dictionary<string, string>();
        protected Dictionary<string, bool> m_folderBrowserShowNewFolderButtons = new Dictionary<string, bool>();
        #endregion

        #region Properties
        [Browsable( false )]
        public Dictionary<string, string> FileBrowserTitles
        {
            get
            {
                return m_fileBrowserTitles;
            }
        }

        [Browsable( false )]
        public Dictionary<string, string> FileBrowserFilters
        {
            get
            {
                return m_fileBrowserFilters;
            }
        }

        [Browsable( false )]
        public Dictionary<string, string> FolderBrowserDescriptions
        {
            get
            {
                return m_folderBrowserDescriptions;
            }
        }

        [Browsable( false )]
        public Dictionary<string, bool> FolderBrowserShowNewFolderButtons
        {
            get
            {
                return m_folderBrowserShowNewFolderButtons;
            }
        }
        #endregion
    };

    /// <summary>
    /// A UITypeEditor for a PropertyGrid item that represents a path and filename 
    /// </summary>
    [System.Security.Permissions.PermissionSet( System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust" )]
    public class FileBrowserEditor : System.Drawing.Design.UITypeEditor
    {
        public FileBrowserEditor()
        {

        }

        public override System.Drawing.Design.UITypeEditorEditStyle GetEditStyle( ITypeDescriptorContext context )
        {
            return System.Drawing.Design.UITypeEditorEditStyle.Modal;
        }

        public override object EditValue( ITypeDescriptorContext context, IServiceProvider provider, object value )
        {
            // Return the value if the value is not of type string
            if ( (value != null) && (value.GetType() != typeof( string )) )
            {
                return value;
            }

            // Uses the IWindowsFormsEditorService to display a drop-down UI in the Properties window.
            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService( typeof( IWindowsFormsEditorService ) );
            if ( edSvc != null )
            {
                string title = "Select File";
                string filter = "All Files (*.*)|*.*";
                if ( context.Instance is PropertyGridSettingsBase )
                {
                    PropertyGridSettingsBase settings = context.Instance as PropertyGridSettingsBase;

                    settings.FileBrowserFilters.TryGetValue( context.PropertyDescriptor.Name, out title );
                    settings.FileBrowserFilters.TryGetValue( context.PropertyDescriptor.Name, out filter );
                }

                FileBrowserEditControl ctrl = new FileBrowserEditControl( title, filter );
                edSvc.DropDownControl( ctrl );

                return ctrl.Filename;
            }

            return value;
        }
    };

    /// <summary>
    /// A UITypeEditor for a PropertyGrid item that represents a path and filename 
    /// </summary>
    [System.Security.Permissions.PermissionSet( System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust" )]
    public class FolderBrowserEditor : System.Drawing.Design.UITypeEditor
    {
        public FolderBrowserEditor()
        {

        }

        public override System.Drawing.Design.UITypeEditorEditStyle GetEditStyle( ITypeDescriptorContext context )
        {
            return System.Drawing.Design.UITypeEditorEditStyle.Modal;
        }

        public override object EditValue( ITypeDescriptorContext context, IServiceProvider provider, object value )
        {
            // Return the value if the value is not of type string
            if ( (value != null) && (value.GetType() != typeof( string )) )
            {
                return value;
            }

            // Uses the IWindowsFormsEditorService to display a drop-down UI in the Properties window.
            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService( typeof( IWindowsFormsEditorService ) );
            if ( edSvc != null )
            {
                string description = "Select Folder";
                bool showNewFolderButton = false;
                if ( context.Instance is PropertyGridSettingsBase )
                {
                    PropertyGridSettingsBase settings = context.Instance as PropertyGridSettingsBase;

                    settings.FolderBrowserDescriptions.TryGetValue( context.PropertyDescriptor.Name, out description );
                    settings.FolderBrowserShowNewFolderButtons.TryGetValue( context.PropertyDescriptor.Name, out showNewFolderButton );
                }

                FolderBrowserEditControl ctrl = new FolderBrowserEditControl( description, showNewFolderButton );
                edSvc.DropDownControl( ctrl );

                return ctrl.Folder;
            }

            return value;
        }
    };

    /// <summary>
    /// A PropertyGrid converter for an array of paths.
    /// </summary>
    public class FolderArrayConverter : TypeConverter
    {
        public FolderArrayConverter()
        {

        }

        public override bool CanConvertTo( ITypeDescriptorContext context, Type destinationType )
        {
            if ( destinationType == typeof( string[] ) )
            {
                return true;
            }

            return base.CanConvertTo( context, destinationType );
        }

        public override object ConvertTo( ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType )
        {
            if ( (destinationType == typeof( string )) && (value is string[]) )
            {
                StringBuilder text = new StringBuilder();
                string[] arr = value as string[];
                for ( int i = 0; i < arr.Length; ++i )
                {
                    if ( i > 0 )
                    {
                        text.Append( Path.PathSeparator );
                    }

                    text.Append( arr[i] );
                }

                return text.ToString();
            }

            return base.ConvertTo( context, culture, value, destinationType );
        }

        public override bool CanConvertFrom( ITypeDescriptorContext context, Type sourceType )
        {
            if ( sourceType == typeof( string ) )
            {
                return true;
            }

            return base.CanConvertFrom( context, sourceType );
        }

        public override object ConvertFrom( ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value )
        {
            if ( value is string )
            {
                string str = value as string;
                return str.Split( new char[] { Path.PathSeparator } );
            }

            return base.ConvertFrom( context, culture, value );
        }
    };

    /// <summary>
    /// A PropertyGrid converter for a list of paths
    /// </summary>
    public class FolderListConverter : TypeConverter
    {
        public FolderListConverter()
        {

        }

        public override bool CanConvertTo( ITypeDescriptorContext context, Type destinationType )
        {
            if ( destinationType == typeof( List<string> ) )
            {
                return true;
            }

            return base.CanConvertTo( context, destinationType );
        }

        public override object ConvertTo( ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType )
        {
            if ( (destinationType == typeof( string )) && value is List<string> )
            {
                StringBuilder text = new StringBuilder();
                List<string> list = value as List<string>;
                for ( int i = 0; i < list.Count; ++i )
                {
                    if ( i > 0 )
                    {
                        text.Append( Path.PathSeparator );
                    }

                    text.Append( list[i] );
                }

                return text.ToString();
            }

            return base.ConvertTo( context, culture, value, destinationType );
        }

        public override bool CanConvertFrom( ITypeDescriptorContext context, Type sourceType )
        {
            if ( sourceType == typeof( string ) )
            {
                return true;
            }

            return base.CanConvertFrom( context, sourceType );
        }

        public override object ConvertFrom( ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value )
        {
            if ( value is string )
            {
                string str = value as string;
                return new List<string>( str.Split( new char[] { Path.PathSeparator } ) );
            }

            return base.ConvertFrom( context, culture, value );
        }
    };

    /// <summary>
    /// A UITypeEditor for a PropertyGrid item that represents a list of paths.
    /// </summary>
    [System.Security.Permissions.PermissionSet( System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust" )]
    public class FolderListBrowserEditor : System.Drawing.Design.UITypeEditor
    {
        public FolderListBrowserEditor()
        {

        }

        public override System.Drawing.Design.UITypeEditorEditStyle GetEditStyle( ITypeDescriptorContext context )
        {
            return System.Drawing.Design.UITypeEditorEditStyle.Modal;
        }

        public override object EditValue( ITypeDescriptorContext context, IServiceProvider provider, object value )
        {
            // Return the value if the value is not of type string
            if ( (value != null) && (value.GetType() != typeof( string[] )) && (value.GetType() != typeof( List<string> )) )
            {
                return value;
            }

            // Uses the IWindowsFormsEditorService to display a drop-down UI in the Properties window.
            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService( typeof( IWindowsFormsEditorService ) );
            if ( edSvc != null )
            {
                string description = "Select Folder";
                bool showNewFolderButton = false;
                if ( context.Instance is PropertyGridSettingsBase )
                {
                    PropertyGridSettingsBase settings = context.Instance as PropertyGridSettingsBase;

                    settings.FolderBrowserDescriptions.TryGetValue( context.PropertyDescriptor.Name, out description );
                    settings.FolderBrowserShowNewFolderButtons.TryGetValue( context.PropertyDescriptor.Name, out showNewFolderButton );
                }

                FolderListBrowserEditControl ctrl = new FolderListBrowserEditControl( description, showNewFolderButton );
                edSvc.DropDownControl( ctrl );

                if ( value.GetType() == typeof( string[] ) )
                {
                    return ctrl.Folders.ToArray();
                }
                else if ( value.GetType() == typeof( List<string> ) )
                {
                    return ctrl.Folders;
                }
            }

            return value;
        }
    };
}
