namespace ragScriptEditorShared
{
    partial class FindReplaceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.findWhatLabel = new System.Windows.Forms.Label();
            this.findWhatTextBox = new System.Windows.Forms.TextBox();
            this.replaceWithLabel = new System.Windows.Forms.Label();
            this.replaceWithTextBox = new System.Windows.Forms.TextBox();
            this.matchCaseCheckBox = new System.Windows.Forms.CheckBox();
            this.matchWholeWordCheckBox = new System.Windows.Forms.CheckBox();
            this.searchUpCheckBox = new System.Windows.Forms.CheckBox();
            this.useCheckBox = new System.Windows.Forms.CheckBox();
            this.useComboBox = new System.Windows.Forms.ComboBox();
            this.markWtihBookmarksCheckBox = new System.Windows.Forms.CheckBox();
            this.searchGroupBox = new System.Windows.Forms.GroupBox();
            this.entireProjectRadioButton = new System.Windows.Forms.RadioButton();
            this.allOpenDocumentsRadioButton = new System.Windows.Forms.RadioButton();
            this.currentDocumentRadioButton = new System.Windows.Forms.RadioButton();
            this.currentSelectionRadioButton = new System.Windows.Forms.RadioButton();
            this.findNextButton = new System.Windows.Forms.Button();
            this.replaceButton = new System.Windows.Forms.Button();
            this.replaceAllButton = new System.Windows.Forms.Button();
            this.markAllButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.searchGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // findWhatLabel
            // 
            this.findWhatLabel.AutoSize = true;
            this.findWhatLabel.Location = new System.Drawing.Point( 12, 9 );
            this.findWhatLabel.Name = "findWhatLabel";
            this.findWhatLabel.Size = new System.Drawing.Size( 56, 13 );
            this.findWhatLabel.TabIndex = 0;
            this.findWhatLabel.Text = "Fi&nd what:";
            // 
            // findWhatTextBox
            // 
            this.findWhatTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.findWhatTextBox.Location = new System.Drawing.Point( 91, 6 );
            this.findWhatTextBox.Name = "findWhatTextBox";
            this.findWhatTextBox.Size = new System.Drawing.Size( 335, 20 );
            this.findWhatTextBox.TabIndex = 1;
            this.findWhatTextBox.TextChanged += new System.EventHandler( this.findWhatTextBox_TextChanged );
            // 
            // replaceWithLabel
            // 
            this.replaceWithLabel.AutoSize = true;
            this.replaceWithLabel.Location = new System.Drawing.Point( 12, 36 );
            this.replaceWithLabel.Name = "replaceWithLabel";
            this.replaceWithLabel.Size = new System.Drawing.Size( 72, 13 );
            this.replaceWithLabel.TabIndex = 2;
            this.replaceWithLabel.Text = "Re&place with:";
            // 
            // replaceWithTextBox
            // 
            this.replaceWithTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceWithTextBox.Location = new System.Drawing.Point( 91, 33 );
            this.replaceWithTextBox.Name = "replaceWithTextBox";
            this.replaceWithTextBox.Size = new System.Drawing.Size( 335, 20 );
            this.replaceWithTextBox.TabIndex = 3;
            // 
            // matchCaseCheckBox
            // 
            this.matchCaseCheckBox.AutoSize = true;
            this.matchCaseCheckBox.Location = new System.Drawing.Point( 15, 59 );
            this.matchCaseCheckBox.Name = "matchCaseCheckBox";
            this.matchCaseCheckBox.Size = new System.Drawing.Size( 82, 17 );
            this.matchCaseCheckBox.TabIndex = 4;
            this.matchCaseCheckBox.Text = "Match &case";
            this.matchCaseCheckBox.UseVisualStyleBackColor = true;
            // 
            // matchWholeWordCheckBox
            // 
            this.matchWholeWordCheckBox.AutoSize = true;
            this.matchWholeWordCheckBox.Location = new System.Drawing.Point( 15, 83 );
            this.matchWholeWordCheckBox.Name = "matchWholeWordCheckBox";
            this.matchWholeWordCheckBox.Size = new System.Drawing.Size( 113, 17 );
            this.matchWholeWordCheckBox.TabIndex = 5;
            this.matchWholeWordCheckBox.Text = "Match &whole word";
            this.matchWholeWordCheckBox.UseVisualStyleBackColor = true;
            // 
            // searchUpCheckBox
            // 
            this.searchUpCheckBox.AutoSize = true;
            this.searchUpCheckBox.Location = new System.Drawing.Point( 15, 107 );
            this.searchUpCheckBox.Name = "searchUpCheckBox";
            this.searchUpCheckBox.Size = new System.Drawing.Size( 75, 17 );
            this.searchUpCheckBox.TabIndex = 6;
            this.searchUpCheckBox.Text = "S&earch up";
            this.searchUpCheckBox.UseVisualStyleBackColor = true;
            // 
            // useCheckBox
            // 
            this.useCheckBox.AutoSize = true;
            this.useCheckBox.Location = new System.Drawing.Point( 15, 131 );
            this.useCheckBox.Name = "useCheckBox";
            this.useCheckBox.Size = new System.Drawing.Size( 45, 17 );
            this.useCheckBox.TabIndex = 7;
            this.useCheckBox.Text = "&Use";
            this.useCheckBox.UseVisualStyleBackColor = true;
            this.useCheckBox.CheckedChanged += new System.EventHandler( this.useCheckBox_CheckedChanged );
            // 
            // useComboBox
            // 
            this.useComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.useComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.useComboBox.FormattingEnabled = true;
            this.useComboBox.Items.AddRange( new object[] {
            "Regular Expressions"} );
            this.useComboBox.Location = new System.Drawing.Point( 66, 131 );
            this.useComboBox.Name = "useComboBox";
            this.useComboBox.Size = new System.Drawing.Size( 222, 21 );
            this.useComboBox.TabIndex = 8;
            // 
            // markWtihBookmarksCheckBox
            // 
            this.markWtihBookmarksCheckBox.AutoSize = true;
            this.markWtihBookmarksCheckBox.Location = new System.Drawing.Point( 15, 155 );
            this.markWtihBookmarksCheckBox.Name = "markWtihBookmarksCheckBox";
            this.markWtihBookmarksCheckBox.Size = new System.Drawing.Size( 127, 17 );
            this.markWtihBookmarksCheckBox.TabIndex = 9;
            this.markWtihBookmarksCheckBox.Text = "Mark with &bookmarks";
            this.markWtihBookmarksCheckBox.UseVisualStyleBackColor = true;
            // 
            // searchGroupBox
            // 
            this.searchGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchGroupBox.Controls.Add( this.entireProjectRadioButton );
            this.searchGroupBox.Controls.Add( this.allOpenDocumentsRadioButton );
            this.searchGroupBox.Controls.Add( this.currentDocumentRadioButton );
            this.searchGroupBox.Controls.Add( this.currentSelectionRadioButton );
            this.searchGroupBox.Location = new System.Drawing.Point( 294, 59 );
            this.searchGroupBox.Name = "searchGroupBox";
            this.searchGroupBox.Size = new System.Drawing.Size( 132, 124 );
            this.searchGroupBox.TabIndex = 10;
            this.searchGroupBox.TabStop = false;
            this.searchGroupBox.Text = "Search";
            // 
            // entireProjectRadioButton
            // 
            this.entireProjectRadioButton.AutoSize = true;
            this.entireProjectRadioButton.Location = new System.Drawing.Point( 7, 92 );
            this.entireProjectRadioButton.Name = "entireProjectRadioButton";
            this.entireProjectRadioButton.Size = new System.Drawing.Size( 87, 17 );
            this.entireProjectRadioButton.TabIndex = 3;
            this.entireProjectRadioButton.TabStop = true;
            this.entireProjectRadioButton.Text = "Entire pro&ject";
            this.entireProjectRadioButton.UseVisualStyleBackColor = true;
            this.entireProjectRadioButton.Click += new System.EventHandler( this.entireProjectRadioButton_Click );
            // 
            // allOpenDocumentsRadioButton
            // 
            this.allOpenDocumentsRadioButton.AutoSize = true;
            this.allOpenDocumentsRadioButton.Location = new System.Drawing.Point( 7, 68 );
            this.allOpenDocumentsRadioButton.Name = "allOpenDocumentsRadioButton";
            this.allOpenDocumentsRadioButton.Size = new System.Drawing.Size( 118, 17 );
            this.allOpenDocumentsRadioButton.TabIndex = 2;
            this.allOpenDocumentsRadioButton.TabStop = true;
            this.allOpenDocumentsRadioButton.Text = "All &open documents";
            this.allOpenDocumentsRadioButton.UseVisualStyleBackColor = true;
            this.allOpenDocumentsRadioButton.Click += new System.EventHandler( this.allOpenDocumentsRadioButton_Click );
            // 
            // currentDocumentRadioButton
            // 
            this.currentDocumentRadioButton.AutoSize = true;
            this.currentDocumentRadioButton.Location = new System.Drawing.Point( 7, 44 );
            this.currentDocumentRadioButton.Name = "currentDocumentRadioButton";
            this.currentDocumentRadioButton.Size = new System.Drawing.Size( 109, 17 );
            this.currentDocumentRadioButton.TabIndex = 1;
            this.currentDocumentRadioButton.TabStop = true;
            this.currentDocumentRadioButton.Text = "Current &document";
            this.currentDocumentRadioButton.UseVisualStyleBackColor = true;
            this.currentDocumentRadioButton.Click += new System.EventHandler( this.currentDocumentRadioButton_Click );
            // 
            // currentSelectionRadioButton
            // 
            this.currentSelectionRadioButton.AutoSize = true;
            this.currentSelectionRadioButton.Location = new System.Drawing.Point( 7, 20 );
            this.currentSelectionRadioButton.Name = "currentSelectionRadioButton";
            this.currentSelectionRadioButton.Size = new System.Drawing.Size( 104, 17 );
            this.currentSelectionRadioButton.TabIndex = 0;
            this.currentSelectionRadioButton.TabStop = true;
            this.currentSelectionRadioButton.Text = "Current &selection";
            this.currentSelectionRadioButton.UseVisualStyleBackColor = true;
            this.currentSelectionRadioButton.Click += new System.EventHandler( this.currentSelectionRadioButton_Click );
            // 
            // findNextButton
            // 
            this.findNextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.findNextButton.Location = new System.Drawing.Point( 432, 4 );
            this.findNextButton.Name = "findNextButton";
            this.findNextButton.Size = new System.Drawing.Size( 75, 23 );
            this.findNextButton.TabIndex = 11;
            this.findNextButton.Text = "&Find Next";
            this.findNextButton.UseVisualStyleBackColor = true;
            this.findNextButton.Click += new System.EventHandler( this.findNextButton_Click );
            // 
            // replaceButton
            // 
            this.replaceButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceButton.Location = new System.Drawing.Point( 433, 36 );
            this.replaceButton.Name = "replaceButton";
            this.replaceButton.Size = new System.Drawing.Size( 75, 23 );
            this.replaceButton.TabIndex = 12;
            this.replaceButton.Text = "&Replace";
            this.replaceButton.UseVisualStyleBackColor = true;
            this.replaceButton.Click += new System.EventHandler( this.replaceButton_Click );
            // 
            // replaceAllButton
            // 
            this.replaceAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceAllButton.Location = new System.Drawing.Point( 433, 66 );
            this.replaceAllButton.Name = "replaceAllButton";
            this.replaceAllButton.Size = new System.Drawing.Size( 75, 23 );
            this.replaceAllButton.TabIndex = 13;
            this.replaceAllButton.Text = "Replace &All";
            this.replaceAllButton.UseVisualStyleBackColor = true;
            this.replaceAllButton.Click += new System.EventHandler( this.replaceAllButton_Click );
            // 
            // markAllButton
            // 
            this.markAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.markAllButton.Location = new System.Drawing.Point( 432, 96 );
            this.markAllButton.Name = "markAllButton";
            this.markAllButton.Size = new System.Drawing.Size( 75, 23 );
            this.markAllButton.TabIndex = 14;
            this.markAllButton.Text = "&Mark All";
            this.markAllButton.UseVisualStyleBackColor = true;
            this.markAllButton.Click += new System.EventHandler( this.markAllButton_Click );
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point( 432, 126 );
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size( 75, 23 );
            this.closeButton.TabIndex = 15;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler( this.closeButton_Click );
            // 
            // FindReplaceForm
            // 
            this.AcceptButton = this.findNextButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size( 520, 195 );
            this.Controls.Add( this.closeButton );
            this.Controls.Add( this.markAllButton );
            this.Controls.Add( this.replaceAllButton );
            this.Controls.Add( this.replaceButton );
            this.Controls.Add( this.findNextButton );
            this.Controls.Add( this.searchGroupBox );
            this.Controls.Add( this.markWtihBookmarksCheckBox );
            this.Controls.Add( this.useComboBox );
            this.Controls.Add( this.useCheckBox );
            this.Controls.Add( this.searchUpCheckBox );
            this.Controls.Add( this.matchWholeWordCheckBox );
            this.Controls.Add( this.matchCaseCheckBox );
            this.Controls.Add( this.replaceWithTextBox );
            this.Controls.Add( this.replaceWithLabel );
            this.Controls.Add( this.findWhatTextBox );
            this.Controls.Add( this.findWhatLabel );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FindReplaceForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Find/Replace";
            this.Activated += new System.EventHandler( this.FindReplaceForm_Activated );
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.FindReplaceForm_FormClosing );
            this.searchGroupBox.ResumeLayout( false );
            this.searchGroupBox.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label findWhatLabel;
        private System.Windows.Forms.TextBox findWhatTextBox;
        private System.Windows.Forms.Label replaceWithLabel;
        private System.Windows.Forms.TextBox replaceWithTextBox;
        private System.Windows.Forms.CheckBox matchCaseCheckBox;
        private System.Windows.Forms.CheckBox matchWholeWordCheckBox;
        private System.Windows.Forms.CheckBox searchUpCheckBox;
        private System.Windows.Forms.CheckBox useCheckBox;
        private System.Windows.Forms.ComboBox useComboBox;
        private System.Windows.Forms.CheckBox markWtihBookmarksCheckBox;
        private System.Windows.Forms.GroupBox searchGroupBox;
        private System.Windows.Forms.RadioButton currentSelectionRadioButton;
        private System.Windows.Forms.RadioButton entireProjectRadioButton;
        private System.Windows.Forms.RadioButton allOpenDocumentsRadioButton;
        private System.Windows.Forms.RadioButton currentDocumentRadioButton;
        private System.Windows.Forms.Button findNextButton;
        private System.Windows.Forms.Button replaceButton;
        private System.Windows.Forms.Button replaceAllButton;
        private System.Windows.Forms.Button markAllButton;
        private System.Windows.Forms.Button closeButton;
    }
}