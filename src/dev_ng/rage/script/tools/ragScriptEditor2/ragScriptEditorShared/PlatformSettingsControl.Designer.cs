﻿namespace ragScriptEditorShared
{
    partial class PlatformSettingsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.configPlatformMessage = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // configPlatformMessage
            // 
            this.configPlatformMessage.AutoSize = true;
            this.configPlatformMessage.Location = new System.Drawing.Point(3, 0);
            this.configPlatformMessage.Name = "configPlatformMessage";
            this.configPlatformMessage.Size = new System.Drawing.Size(331, 13);
            this.configPlatformMessage.TabIndex = 1;
            this.configPlatformMessage.Text = "Platforms can also be enabled for specific compilation configurations.";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 25);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(554, 266);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // PlatformSettingsControl
            // 
            this.Controls.Add(this.configPlatformMessage);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "PlatformSettingsControl";
            this.Size = new System.Drawing.Size(557, 294);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label configPlatformMessage;
    }
}
