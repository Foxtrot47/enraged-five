using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using ActiproSoftware.SyntaxEditor;
using RSG.Platform;

namespace ragScriptEditorShared
{
    public delegate void InfoTipTextDelegate( string signature, string comment );

    /// <summary>
    /// Interface class for a Project Resolver that is stored in 
    /// SyntaxEditor.Document.LanguageData.  Essentially a Project Resolver is able 
    /// to take the symbols from all files in the Project and put them together for 
    /// Intellisense purposes.  The Script Editor will create one of these for every 
    /// "project" loaded.  Each file not part of the current project will get its 
    /// own instance of this, a project of 1.
    /// </summary>
    public interface IParserProjectResolver : IDisposable
    {
        /// <summary>
        /// Gets the language in use
        /// </summary>
        ActiproSoftware.SyntaxEditor.SyntaxLanguage Language { get; }

        /// <summary>
        /// Set the list of project files for this language
        /// </summary>
        List<string> ProjectFiles { get; set; }

        /// <summary>
        /// Should set editor.Document.Language, optionally set editor.Document.LanguageData,
        /// and add a ViewMouseHover event handler (for ToolTip popups) and other handlers, if desired.
        /// </summary>
        /// <param name="editor"></param>
        /// <param name="infoTipTextDel">Used to fill the Script Editor's Help Window, such as whenever a ToolTip is set</param>
        void SetLanguage( SyntaxEditor editor, InfoTipTextDelegate infoTipTextDel );

        /// <summary>
        /// Should set editor.Document.Language to the default and clear editor.Document.LanguageData if it was set
        /// and remove the ViewMouseHover event handler and any other handlers
        /// </summary>
        /// <param name="editor"></param>
        void ClearLanguage( SyntaxEditor editor );

        /// <summary>
        /// Retrieves the filename(s) and startOffset(s) for the location of the current keyword's definition
        /// </summary>
        /// <param name="editor"></param>
        /// <param name="fileNames"></param>
        /// <param name="textRanges"></param>
        /// <returns>True if something was found</returns>
        bool GetImplementationInformation( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, out string[] fileNames, out TextRange[] textRanges );

        /// <summary>
        /// Called before a compile is executed
        /// </summary>
        void PreCompile();

        /// <summary>
        /// Determines if a compile is necessary for the file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="projectFile">The project we are compiling, if any.</param>
        /// <param name="compilingSettings">The CompilingSettings we will be using for the compile.</param>
        /// <returns>True to recompile</returns>
        bool NeedsToBeCompiled( string filename, string projectFile, CompilingSettings compilingSettings );

        /// <summary>
        /// Called during a compile when a file has been compiled
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        void FileCompiled( string filename );

        /// <summary>
        /// Determines if resourcing is necessary for the file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="platform"></param>
        /// <param name="projectFile">The project we are compiling, if any.</param>
        /// <param name="compilingSettings">The CompilingSettings we will be using for the compile.</param>
        /// <returns>True to recompile</returns>
        bool NeedsToBeResourced(String filename, Platform platform, String projectFile, CompilingSettings compilingSettings);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="platform"></param>
        void FileResourced(String filename, Platform platform);

        /// <summary>
        /// Called after a compile has finished
        /// </summary>
        void PostCompile();

        /// <summary>
        /// This callback will be registered by the ScriptEditor class and invoked when Intellisense options change.
        /// </summary>
        IntellisenseSettingsChangedEventHandler IntellisenseSettingsChangedEventHandler { get; }
        
        /// <summary>
        /// Executed whenever the Refresh Intellisense button is pressed
        /// </summary>
        void RefreshIntellisense();

        /// <summary>
        /// Return the list of include paths that this file includes.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        IEnumerable<String> GetIncludesForFile(String filename);
    }

    public class IntellisenseSettingsChangedEventArgs : EventArgs
    {
        public IntellisenseSettingsChangedEventArgs( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, IntellisenseSettings settings )
        {
            m_editor = editor;
            m_intellisenseSettings = settings;
        }

        #region Variables
        private ActiproSoftware.SyntaxEditor.SyntaxEditor m_editor;
        private IntellisenseSettings m_intellisenseSettings;
        #endregion

        #region Properties
        public ActiproSoftware.SyntaxEditor.SyntaxEditor Editor
        {
            get
            {
                return m_editor;
            }
        }

        public IntellisenseSettings IntellisenseSettings
        {
            get
            {
                return m_intellisenseSettings;
            }
        }
        #endregion
    };

    public delegate void IntellisenseSettingsChangedEventHandler( object sender, IntellisenseSettingsChangedEventArgs e );
}
