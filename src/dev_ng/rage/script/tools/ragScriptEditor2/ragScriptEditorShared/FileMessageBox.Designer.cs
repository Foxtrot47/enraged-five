﻿namespace ragScriptEditorShared
{
    partial class FileMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.YesButton = new System.Windows.Forms.Button();
            this.YesToAllButton = new System.Windows.Forms.Button();
            this.NoButton = new System.Windows.Forms.Button();
            this.NoToAllButton = new System.Windows.Forms.Button();
            this.ModifiedFileLabel = new System.Windows.Forms.Label();
            this.ModifyNotificationLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // YesButton
            // 
            this.YesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.YesButton.Location = new System.Drawing.Point(82, 70);
            this.YesButton.Name = "YesButton";
            this.YesButton.Size = new System.Drawing.Size(105, 25);
            this.YesButton.TabIndex = 0;
            this.YesButton.Text = "Yes";
            this.YesButton.UseVisualStyleBackColor = true;
            this.YesButton.Click += new System.EventHandler(this.OnYesButtonClick);
            // 
            // YesToAllButton
            // 
            this.YesToAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.YesToAllButton.Location = new System.Drawing.Point(193, 70);
            this.YesToAllButton.Name = "YesToAllButton";
            this.YesToAllButton.Size = new System.Drawing.Size(105, 25);
            this.YesToAllButton.TabIndex = 1;
            this.YesToAllButton.Text = "Yes to All";
            this.YesToAllButton.UseVisualStyleBackColor = true;
            this.YesToAllButton.Click += new System.EventHandler(this.OnYesToAllButtonClick);
            // 
            // NoButton
            // 
            this.NoButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.NoButton.Location = new System.Drawing.Point(304, 70);
            this.NoButton.Name = "NoButton";
            this.NoButton.Size = new System.Drawing.Size(105, 25);
            this.NoButton.TabIndex = 2;
            this.NoButton.Text = "No";
            this.NoButton.UseVisualStyleBackColor = true;
            this.NoButton.Click += new System.EventHandler(this.OnNoButtonClick);
            // 
            // NoToAllButton
            // 
            this.NoToAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.NoToAllButton.Location = new System.Drawing.Point(415, 70);
            this.NoToAllButton.Name = "NoToAllButton";
            this.NoToAllButton.Size = new System.Drawing.Size(105, 25);
            this.NoToAllButton.TabIndex = 3;
            this.NoToAllButton.Text = "No to All";
            this.NoToAllButton.UseVisualStyleBackColor = true;
            this.NoToAllButton.Click += new System.EventHandler(this.OnNoToAllButtonClick);
            // 
            // ModifiedFileLabel
            // 
            this.ModifiedFileLabel.AutoSize = true;
            this.ModifiedFileLabel.Location = new System.Drawing.Point(12, 9);
            this.ModifiedFileLabel.Name = "ModifiedFileLabel";
            this.ModifiedFileLabel.Size = new System.Drawing.Size(100, 13);
            this.ModifiedFileLabel.TabIndex = 4;
            this.ModifiedFileLabel.Text = "<modified filename>";
            // 
            // ModifyNotificationLabel
            // 
            this.ModifyNotificationLabel.AutoSize = true;
            this.ModifyNotificationLabel.Location = new System.Drawing.Point(12, 34);
            this.ModifyNotificationLabel.Name = "ModifyNotificationLabel";
            this.ModifyNotificationLabel.Size = new System.Drawing.Size(231, 26);
            this.ModifyNotificationLabel.TabIndex = 5;
            this.ModifyNotificationLabel.Text = "This file has been modified outside of the editor.\r\nDo you want to reload it?";
            // 
            // FileMessageBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(532, 105);
            this.ControlBox = false;
            this.Controls.Add(this.ModifyNotificationLabel);
            this.Controls.Add(this.ModifiedFileLabel);
            this.Controls.Add(this.NoToAllButton);
            this.Controls.Add(this.NoButton);
            this.Controls.Add(this.YesToAllButton);
            this.Controls.Add(this.YesButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FileMessageBox";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Externally Modified File(s)";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button YesButton;
        private System.Windows.Forms.Button YesToAllButton;
        private System.Windows.Forms.Button NoButton;
        private System.Windows.Forms.Button NoToAllButton;
        private System.Windows.Forms.Label ModifiedFileLabel;
        private System.Windows.Forms.Label ModifyNotificationLabel;
    }
}