using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using ActiproSoftware.SyntaxEditor;

namespace ragScriptEditorShared
{
    public partial class SearchReplaceComponent : Component
    {
        public SearchReplaceComponent()
        {
            InitializeComponent();
        }

        public SearchReplaceComponent( IContainer container )
        {
            container.Add( this );

            InitializeComponent();
        }

        public void Init( Control parent )
        {
            m_parentControl = parent;
        }

        #region Variables
        private Control m_parentControl;
        private string m_title = "Search/Replace";

        private SearchReplaceType m_searchType;
        private static bool m_isBusy = false;

        private int m_numFilesSearched = 0;
        private int m_totalTickCount = 0;
        private int m_numFilesFound = 0;
        private int m_numFound = 0;
        private SearchReplaceResult m_result = SearchReplaceResult.Found;
        private string m_resultMessage = null;
        private string m_resultTitle = null;

        private string m_lastFilenameSearched = null;
        private int m_lastStartOffset = 0;
        #endregion

        #region Properties
        public string Title
        {
            get
            {
                return m_title;
            }
            set
            {
                m_title = value;
            }
        }
        #endregion

        #region Delegates
        private delegate List<SearchReplaceResultData> FindReplaceOptionsDelegate( FindReplaceOptions options );
        private delegate SearchReplaceResultData FindReplaceOptionsStartOffsetDelegate( FindReplaceOptions options, int startOffset );
        private delegate List<SearchReplaceResultData> FindReplaceOptionsMarkWithBookmarksDelegate( FindReplaceOptions options, bool markWithBookmarks );
        private delegate void GoToTopOfFileDelegate( SyntaxEditor editor );
        #endregion

        #region Events
        public event SearchBeginEventHandler SearchBegin;
        public event SearchCompleteEventHandler SearchComplete;
        public event FoundStringEventHandler FoundString;
        public event ReportProgressEventHandler ReportProgress;

        public event SyntaxEditorEventHandler FindSyntaxEditor;
        public event LoadFileEventHandler LoadFile;
        public event SaveDocumentEventHandler SaveDocument;
        #endregion

        #region Event Dispatchers
        protected void OnSearchBegin( SearchReplaceEventArgs e )
        {
            m_isBusy = true;
            m_searchType = e.Search;
            
            if ( this.SearchBegin != null )
            {
                if ( m_parentControl?.InvokeRequired ?? false)
                {
                    m_parentControl.Invoke( this.SearchBegin, new object[] { this, e } );
                }
                else
                {
                    this.SearchBegin( this, e );
                }
            }

            m_result = SearchReplaceResult.NotFound;
            m_resultMessage = "The specified text was not found.";
            m_resultTitle = this.Title;

            m_numFilesSearched = 0;
            m_totalTickCount = 0;
            m_numFilesFound = 0;
            m_numFound = 0;
        }

        protected void OnSearchComplete( SearchCompleteEventArgs e )
        {
            m_isBusy = false;

            if ( this.SearchComplete != null )
            {
                if ( m_parentControl?.InvokeRequired ?? false)
                {
                    m_parentControl.Invoke( this.SearchComplete, new object[] { this, e } );
                }
                else
                {
                    this.SearchComplete( this, e );
                }
            }

            //float avg = (m_numFilesFound > 0) ? (float)m_totalTickCount / (float)m_numFilesFound : 0.0f;
            //Console.WriteLine( "{0} AvgTickCount={1}", e.Search.ToString(), avg );
        }

        protected void OnFoundString( SearchReplaceFoundStringEventArgs e )
        {
            if ( this.FoundString != null )
            {
                if ( m_parentControl?.InvokeRequired ?? false)
                {
                    m_parentControl.Invoke( this.FoundString, new object[] { this, e } );
                }
                else
                {
                    this.FoundString( this, e );
                }
            }
        }

        protected void OnReportProgress( SearchReplaceReportProgressEventArgs e )
        {
            if ( this.ReportProgress != null )
            {
                if ( m_parentControl?.InvokeRequired ?? false)
                {
                    m_parentControl.Invoke( this.ReportProgress, new object[] { this, e } );
                }
                else
                {
                    this.ReportProgress( this, e );
                }
            }
        }

        protected SyntaxEditor OnFindSyntaxEditor( SearchReplaceSyntaxEditorEventArgs e )
        {
            if ( this.FindSyntaxEditor != null )
            {
                if ( m_parentControl?.InvokeRequired ?? false)
                {
                    m_parentControl.Invoke( this.FindSyntaxEditor, new object[] { this, e } );
                }
                else
                {
                    this.FindSyntaxEditor( this, e );
                }

                return e.Editor;
            }

            return null;
        }

        protected bool OnLoadFile( SearchReplaceLoadFileEventArgs e )
        {
            if ( this.LoadFile != null )
            {
                if ( m_parentControl?.InvokeRequired ?? false)
                {
                    m_parentControl.Invoke( this.LoadFile, new object[] { this, e } );
                }
                else
                {
                    this.LoadFile( this, e );
                }

                if ( !e.Success )
                {
                    m_result = SearchReplaceResult.Error;
                    m_resultMessage = String.Format( "An error occurred:  Unable to open the file '{0}'.", e.Filename );
                    m_resultTitle = String.Format( "{0} Error", this.Title );
                    return false;
                }
            }

            return true;
        }

        protected bool OnSaveDocument( SearchReplaceSaveTextEventArgs e )
        {
            if ( this.SaveDocument != null )
            {
                if ( m_parentControl?.InvokeRequired ?? false)
                {
                    m_parentControl.Invoke( this.SaveDocument, new object[] { this, e } );
                }
                else
                {
                    this.SaveDocument( this, e );
                }

                if ( !e.Success )
                {
                    m_result = SearchReplaceResult.Error;
                    m_resultMessage = String.Format( "An error occurred:  Unable to save the file '{0}'.", e.Filename );
                    m_resultTitle = String.Format( "{0} Error", this.Title );
                    return false;
                }
            }

            return true;
        }
        #endregion

        #region Invokers
        private SearchReplaceResultData FindReplaceNext( SearchFile searchFile, FindReplaceOptions options, bool replace, int startOffset )
        {
            if ( replace )
            {
                if ( m_parentControl?.InvokeRequired ?? false)
                {
                    FindReplaceOptionsStartOffsetDelegate del = new FindReplaceOptionsStartOffsetDelegate( searchFile.ReplaceNext );
                    return m_parentControl.Invoke( del, new object[] { options, startOffset } ) as SearchReplaceResultData;
                }
                else
                {
                    return searchFile.ReplaceNext( options, startOffset );
                }
            }
            else
            {
                if ( m_parentControl?.InvokeRequired ?? false)
                {
                    FindReplaceOptionsStartOffsetDelegate del = new FindReplaceOptionsStartOffsetDelegate( searchFile.FindNext );
                    return m_parentControl.Invoke( del, new object[] { options, startOffset } ) as SearchReplaceResultData;
                }
                else
                {
                    return searchFile.FindNext( options, startOffset );
                }
            }
        }

        private List<SearchReplaceResultData> FindReplaceAll( SearchFile searchFile, FindReplaceOptions options, bool replace )
        {
            if ( replace )
            {
                if ( m_parentControl?.InvokeRequired ?? false)
                {
                    FindReplaceOptionsDelegate del = new FindReplaceOptionsDelegate( searchFile.ReplaceAll );
                    return m_parentControl.Invoke( del, new object[] { options } ) as List<SearchReplaceResultData>;
                }
                else
                {
                    return searchFile.ReplaceAll( options );
                }
            }
            else
            {
                if ( m_parentControl?.InvokeRequired ?? false)
                {
                    FindReplaceOptionsDelegate del = new FindReplaceOptionsDelegate( searchFile.FindAll );
                    return m_parentControl.Invoke( del, new object[] { options } ) as List<SearchReplaceResultData>;
                }
                else
                {
                    return searchFile.FindAll( options );
                }
            }
        }
        
        private List<SearchReplaceResultData> MarkAll( SearchFile searchFile, FindReplaceOptions options, bool markWithBookmarks )
        {
            if ( m_parentControl?.InvokeRequired ?? false)
            {
                FindReplaceOptionsMarkWithBookmarksDelegate del = new FindReplaceOptionsMarkWithBookmarksDelegate( searchFile.MarkAll );
                return m_parentControl.Invoke( del, new object[] { options, markWithBookmarks } ) as List<SearchReplaceResultData>;
            }
            else
            {
                return searchFile.MarkAll( options, markWithBookmarks );
            }
        }

        private void GoToTop( SearchOpenFile searchOpenFile )
        {
            // reset the start of the search to the top of the file
            if ( m_parentControl?.InvokeRequired ?? false)
            {
                GoToTopOfFileDelegate del = new GoToTopOfFileDelegate( GoToTopOfFile );
                m_parentControl.Invoke( del, new object[] { searchOpenFile.Editor } );
            }
            else
            {
                GoToTopOfFile( searchOpenFile.Editor );
            }
        }
        #endregion

        #region Event Handlers
        private void backgroundWorker_DoWork( object sender, DoWorkEventArgs e )
        {
            //int tickStart = Environment.TickCount;

            // give the main thread a moment to close the search dialog, if needed
            Thread.Sleep( 100 );

            SearchReplaceBackgroundWorkerArgument arg = e.Argument as SearchReplaceBackgroundWorkerArgument;

            switch ( m_searchType )
            {
                case SearchReplaceType.FindInFile:
                    {
                        // do nothing
                    }
                    break;
                case SearchReplaceType.ReplaceInFile:
                    {
                        // do nothing
                    }
                    break;
                case SearchReplaceType.FindInFiles:
                    {
                        if ( arg.IsDirectorySearchReplace )
                        {
                            string currentFileDirectory = arg.CurrentFilename;
                            if ( String.IsNullOrEmpty( arg.CurrentFilename ) || (arg.CurrentFilename.IndexOf( arg.Directory ) == -1) )
                            {
                                // CurrentFilename is either empty or not in the search directory
                                currentFileDirectory = arg.Directory;
                            }
                            else
                            {
                                currentFileDirectory = Path.GetDirectoryName( arg.CurrentFilename );
                            }

                            FindReplaceNextInDirectoryInternal( arg.Directory, arg.RecurseSubDirectories, arg.Extensions, arg.CurrentFilename,
                                arg.FindReplaceOptions, arg.OpenFile, false, currentFileDirectory, true );
                            if ( m_result == SearchReplaceResult.NotFound )
                            {
                                // loop around to beginning
                                FindReplaceNextInDirectoryInternal( arg.Directory, arg.RecurseSubDirectories, arg.Extensions, string.Empty,
                                    arg.FindReplaceOptions, arg.OpenFile, false, arg.Directory, true );
                            }
                        }
                        else
                        {
                            FindReplaceNextInFilesInternal( arg.Filenames, arg.CurrentFilename, 
                                arg.FindReplaceOptions, arg.OpenFile, false );
                        }
                    }
                    break;
                case SearchReplaceType.ReplaceInFiles:
                    {
                        if ( arg.IsDirectorySearchReplace )
                        {
                            string currentFileDirectory = arg.CurrentFilename;
                            if ( String.IsNullOrEmpty( arg.CurrentFilename ) || (arg.CurrentFilename.IndexOf( arg.Directory ) == -1) )
                            {
                                // CurrentFilename is either empty or not in the search directory
                                currentFileDirectory = string.Empty;
                                currentFileDirectory = arg.Directory;
                            }
                            else
                            {
                                currentFileDirectory = Path.GetDirectoryName( arg.CurrentFilename );
                            }

                            FindReplaceNextInDirectoryInternal( arg.Directory, arg.RecurseSubDirectories, arg.Extensions, arg.CurrentFilename, 
                                arg.FindReplaceOptions, arg.OpenFile, true, currentFileDirectory, true );
                            if ( m_result == SearchReplaceResult.NotFound )
                            {
                                // loop around to beginning
                                FindReplaceNextInDirectoryInternal( arg.Directory, arg.RecurseSubDirectories, arg.Extensions, string.Empty,
                                    arg.FindReplaceOptions, arg.OpenFile, true, arg.Directory, true );
                            }
                        }
                        else
                        {
                            FindReplaceNextInFilesInternal( arg.Filenames, arg.CurrentFilename, 
                                arg.FindReplaceOptions, arg.OpenFile, true );
                        }
                    }
                    break;
                case SearchReplaceType.FindAllInFile:
                    {
                        // do nothing
                    }
                    break;
                case SearchReplaceType.ReplaceAllInFile:
                    {
                        // do nothing
                    }
                    break;
                case SearchReplaceType.FindAllInFiles:
                    {
                        if ( arg.IsDirectorySearchReplace )
                        {
                            FindReplaceAllInDirectoryInternal( arg.Directory, arg.RecurseSubDirectories, arg.Extensions, 
                                arg.FindReplaceOptions, arg.OpenFile, false, false, false );
                        }
                        else
                        {
                            FindReplaceAllInFilesInternal( arg.Filenames, 
                                arg.FindReplaceOptions, arg.OpenFile, false, false, false );
                        }
                    }
                    break;
                case SearchReplaceType.ReplaceAllInFiles:
                    {
                        if ( arg.IsDirectorySearchReplace )
                        {
                            FindReplaceAllInDirectoryInternal( arg.Directory, arg.RecurseSubDirectories, arg.Extensions,
                                arg.FindReplaceOptions, arg.OpenFile, true, false, false );
                        }
                        else
                        {
                            FindReplaceAllInFilesInternal( arg.Filenames, 
                                arg.FindReplaceOptions, arg.OpenFile, true, false, false );
                        }
                    }
                    break;
                case SearchReplaceType.MarkAllInFile:
                    {
                        // do nothing
                    }
                    break;
                case SearchReplaceType.MarkAllInFiles:
                    {
                        if ( arg.IsDirectorySearchReplace )
                        {
                            FindReplaceAllInDirectoryInternal( arg.Directory, arg.RecurseSubDirectories, arg.Extensions,
                                arg.FindReplaceOptions, arg.OpenFile, false, true, arg.MarkWithBookmarks );
                        }
                        else
                        {
                            FindReplaceAllInFilesInternal( arg.Filenames, 
                                arg.FindReplaceOptions, arg.OpenFile, false, true, arg.MarkWithBookmarks );
                        }
                    }
                    break;
            }

            //int tickEnd = Environment.TickCount;
            //Console.WriteLine( "SearchReplaceComponent {0} took {1} ticks.", m_searchType.ToString(), tickEnd - tickStart );

            GC.Collect();

            e.Result = m_result;
        }

        private void backgroundWorker_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            Progress p = e.UserState as Progress;
            switch ( p.Type )
            {
                case Progress.ProgressType.FoundString:
                    {
                        OnFoundString( p.EventArgs as SearchReplaceFoundStringEventArgs );
                    }
                    break;
                case Progress.ProgressType.ReportProgress:
                    {
                        OnReportProgress( p.EventArgs as SearchReplaceReportProgressEventArgs );
                    }
                    break;
            }
        }

        private void backgroundWorker_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            if ( e.Cancelled )
            {
                m_result = SearchReplaceResult.Cancelled;
                m_resultMessage = String.Format( "{0} was stopped in progress.", this.Title );
                m_resultTitle = this.Title;
            }

            OnSearchComplete( new SearchCompleteEventArgs( m_numFilesSearched, m_numFilesFound, m_numFound,
                    m_result, m_resultMessage, m_resultTitle, m_searchType ) );
        }
        #endregion

        #region Public Functions
        public bool IsBusy()
        {
            return m_isBusy;
        }
        
        public void CancelSearchReplace()
        {
            if ( this.backgroundWorker.IsBusy )
            {
                this.backgroundWorker.CancelAsync();
            }
        }

        public string DetermineSearchText( string filename, int offset )
        {
            SearchFile searchFile = CreateSearchFile( filename );
            if ( searchFile == null )
            {
                return null;
            }
            
            string text = searchFile.GetSelectedText( offset );
            
            searchFile.Dispose();

            return text;
        }

        public SyntaxEditor GetSyntaxEditor( string filename )
        {
            return OnFindSyntaxEditor( new SearchReplaceSyntaxEditorEventArgs( filename ) );
        }

        public void FindNextInFile( string filename, FindReplaceOptions options, bool openFile )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.FindInFile ) );

            FindReplaceNextInFileInternal( filename, options, openFile, false, false, true );

            OnSearchComplete( new SearchCompleteEventArgs( m_numFilesSearched, m_numFilesFound, m_numFound,
                    m_result, m_resultMessage, m_resultTitle, m_searchType ) );
        }

        public void ReplaceNextInFile( string filename, FindReplaceOptions options, bool openFile )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.ReplaceInFile ) );

            FindReplaceNextInFileInternal( filename, options, openFile, false, true, true );

            OnSearchComplete( new SearchCompleteEventArgs( m_numFilesSearched, m_numFilesFound, m_numFound,
                    m_result, m_resultMessage, m_resultTitle, m_searchType ) );
        }

        public void FindNextInFiles( List<string> filenames, string currentFilename, FindReplaceOptions options, bool openFile )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.FindInFiles ) );

            this.backgroundWorker.RunWorkerAsync( new SearchReplaceBackgroundWorkerArgument( filenames, currentFilename, 
                options, openFile, false ) );
        }

        public void FindNextInFiles( string directory, bool recurseSubDirectories, string searchPattern, string currentFilename, 
            FindReplaceOptions options, bool openFile )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.FindInFiles ) );

            this.backgroundWorker.RunWorkerAsync( new SearchReplaceBackgroundWorkerArgument( directory, recurseSubDirectories, searchPattern, 
                currentFilename, options, openFile, false ) );
        }

        public void ReplaceNextInFiles( List<string> filenames, string currentFilename, FindReplaceOptions options, bool openFile )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.ReplaceInFiles ) );            

            this.backgroundWorker.RunWorkerAsync( new SearchReplaceBackgroundWorkerArgument( filenames, currentFilename,
                options, openFile, false ) );
        }

        public void ReplaceNextInFiles( string directory, bool recurseSubDirectories, string searchPattern, string currentFilename, 
            FindReplaceOptions options, bool openFile )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.ReplaceInFiles ) );

            this.backgroundWorker.RunWorkerAsync( new SearchReplaceBackgroundWorkerArgument( directory, recurseSubDirectories, searchPattern, 
                currentFilename, options, openFile, false ) );
        }

        public void FindAllInFile( string filename, FindReplaceOptions options, bool openFile )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.FindAllInFile ) );

            FindReplaceAllInFileInternal( filename, options, openFile, true, false, false, false, true );

            OnSearchComplete( new SearchCompleteEventArgs( m_numFilesSearched, m_numFilesFound, m_numFound,
                    m_result, m_resultMessage, m_resultTitle, m_searchType ) );
        }

        public void ReplaceAllInFile( string filename, FindReplaceOptions options, bool openFile )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.ReplaceAllInFile ) );

            FindReplaceAllInFileInternal( filename, options, openFile, true, true, false, false, true );

            OnSearchComplete( new SearchCompleteEventArgs( m_numFilesSearched, m_numFilesFound, m_numFound,
                    m_result, m_resultMessage, m_resultTitle, m_searchType ) );
        }

        public void MarkAllInFile( string filename, FindReplaceOptions options, bool openFile, bool markWithBookmarks )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.MarkAllInFile ) );

            FindReplaceAllInFileInternal( filename, options, openFile, true, false, true, markWithBookmarks, true );

            OnSearchComplete( new SearchCompleteEventArgs( m_numFilesSearched, m_numFilesFound, m_numFound,
                    m_result, m_resultMessage, m_resultTitle, m_searchType ) );
        }

        public void FindAllInFiles( List<string> filenames, FindReplaceOptions options, bool openFile )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.FindAllInFiles ) );

            this.backgroundWorker.RunWorkerAsync( new SearchReplaceBackgroundWorkerArgument( filenames, string.Empty, 
                options, openFile, false ) );
        }

        public void FindAllInFiles( string directory, bool recurseSubDirectories, string searchPattern, FindReplaceOptions options, bool openFile )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.FindAllInFiles ) );

            this.backgroundWorker.RunWorkerAsync( new SearchReplaceBackgroundWorkerArgument( directory, recurseSubDirectories, searchPattern, 
                string.Empty, options, openFile, false ) );
        }

        public void ReplaceAllInFiles( List<string> filenames, FindReplaceOptions options, bool openFile )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.ReplaceAllInFiles ) );

            this.backgroundWorker.RunWorkerAsync( new SearchReplaceBackgroundWorkerArgument( filenames, string.Empty, 
                options, openFile, false ) );
        }

        public void ReplaceAllInFiles( string directory, bool recurseSubDirectories, string searchPattern, FindReplaceOptions options, bool openFile )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.ReplaceAllInFiles ) );

            this.backgroundWorker.RunWorkerAsync( new SearchReplaceBackgroundWorkerArgument( directory, recurseSubDirectories, searchPattern, 
                string.Empty, options, openFile, false ) );
        }

        public void MarkAllInFiles( List<string> filenames, FindReplaceOptions options, bool openFile, bool markWithBookmarks )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.MarkAllInFiles ) );

            this.backgroundWorker.RunWorkerAsync( new SearchReplaceBackgroundWorkerArgument( filenames, string.Empty,
                options, openFile, markWithBookmarks ) );
        }

        public void MarkAllInFiles( string directory, bool recurseSubDirectories, string searchPattern, FindReplaceOptions options, bool openFile, bool markWithBookmarks )
        {
            if ( m_isBusy )
            {
                return;
            }

            OnSearchBegin( new SearchReplaceEventArgs( SearchReplaceType.MarkAllInFiles ) );

            this.backgroundWorker.RunWorkerAsync( new SearchReplaceBackgroundWorkerArgument( directory, recurseSubDirectories, searchPattern, 
                string.Empty, options, openFile, markWithBookmarks ) );
        }
        #endregion

        #region Private Functions
        private SearchFile CreateSearchFile( string filename )
        {
            m_lastFilenameSearched = filename;

            SyntaxEditor editor = OnFindSyntaxEditor( new SearchReplaceSyntaxEditorEventArgs( filename ) );

            if ( editor != null )
            {
                return new SearchOpenFile( editor );
            }
            else
            {
                return new SearchClosedFile( filename );
            }
        }

        private void DetermineResult( SearchReplaceResultData resultData )
        {
            m_result = SearchReplaceResult.Found;
            m_resultMessage = string.Empty;
            m_resultTitle = this.Title;

            if ( !resultData.Found )
            {
                if ( resultData.PastStartOfSearch )
                {
                    m_result = SearchReplaceResult.PastStartOfSearch;
                    m_resultMessage = "Find reached the starting point of the search.";
                }
                else if ( resultData.PastEndOfFile )
                {
                    m_result = SearchReplaceResult.PastEndOfFile;
                    m_resultMessage = "Find reached end of document.";
                }
                else
                {
                    m_result = SearchReplaceResult.NotFound;
                    m_resultMessage = "The specified text was not found.";
                }
            }
        }

        private void GoToTopOfFile( SyntaxEditor editor )
        {
            editor.SelectedView.GoToLine( 0 );
            editor.SelectedView.FirstVisibleX = 0;
        }

        private void FindReplaceNextInFileInternal( string filename, FindReplaceOptions options, 
            bool openFile, bool searchFromTop, bool replace, bool tallyResults )
        {
            if ( filename != m_lastFilenameSearched )
            {
                m_lastStartOffset = 0;
            }

            SearchFile searchFile = CreateSearchFile( filename );
            Debug.Assert( searchFile != null, "CreateSearchFile should never return null." );

            ++m_numFilesSearched;

            int startTicks = Environment.TickCount;

            if ( searchFromTop && searchFile.IsOpen )
            {
                GoToTop( searchFile as SearchOpenFile );
            }

            SearchReplaceResultData resultData = null;
            try
            {
                resultData = FindReplaceNext( searchFile, options, replace, m_lastStartOffset );
            }
            catch ( System.Exception e )
            {
                m_result = SearchReplaceResult.Error;
                m_resultMessage = String.Format( "An error occurred:{0}{1}", Environment.NewLine, e.Message );
                m_resultTitle = String.Format( "{0} Error", this.Title );

                searchFile.Dispose();
                return;
            }

            DetermineResult( resultData );

            if ( m_result == SearchReplaceResult.Found )
            {
                if ( tallyResults )
                {
                    m_lastStartOffset = resultData.EndOffset;

                    SearchReplaceFoundStringEventArgs e = new SearchReplaceFoundStringEventArgs( searchFile.Filename, 
                        resultData.Line, resultData.Index, resultData.Text, m_searchType );
                    if ( this.backgroundWorker.IsBusy )
                    {
                        Progress p = new Progress( Progress.ProgressType.FoundString, e );
                        this.backgroundWorker.ReportProgress( -1, p );
                    }
                    else
                    {
                        OnFoundString( e );
                    }

                    ++m_numFound;
                    ++m_numFilesFound;
                }

                if ( openFile )
                {
                    // load the file or give it focus
                    if ( !OnLoadFile( new SearchReplaceLoadFileEventArgs( filename ) ) )
                    {
                        searchFile.Dispose();
                        return;
                    }

                    // if we actually opened it just now, we need to perform the replace again to highlight the text
                    if ( !searchFile.IsOpen )
                    {
                        --m_numFilesSearched; // fixup
                        FindReplaceNextInFileInternal( filename, options, true, true, replace, false );                        

                        searchFile.Dispose();
                        return;
                    }
                }
                else if ( replace && !searchFile.IsOpen )
                {
                    // we need to save the file now
                    SearchClosedFile searchClosedFile = searchFile as SearchClosedFile;

                    if ( !OnSaveDocument( new SearchReplaceSaveTextEventArgs( searchClosedFile.Text, searchClosedFile.Filename ) ) )
                    {
                        searchFile.Dispose();
                        return;
                    }
                }
            }
            else
            {
                m_lastStartOffset = 0;
            }

            searchFile.Dispose();

            int endTicks = Environment.TickCount;
            m_totalTickCount += (endTicks - startTicks);
        }

        private void FindReplaceNextInFilesInternal( List<string> filenames, string currentFilename, FindReplaceOptions options,
            bool openFile, bool replace )
        {
            if ( filenames.Count == 0 )
            {
                m_result = SearchReplaceResult.NotFound;
                m_resultMessage = "There are no files to search.";
                m_resultTitle = this.Title;
                return;
            }

            int indexOf = 0;
            if ( !String.IsNullOrEmpty( currentFilename ) )
            {
                indexOf = filenames.IndexOf( currentFilename );
                if ( indexOf == -1 )
                {
                    indexOf = 0;
                }
            }

            bool changedFile = false;
            int startingIndex = indexOf;
            while ( true )
            {
                if ( this.backgroundWorker.IsBusy && this.backgroundWorker.CancellationPending )
                {
                    m_result = SearchReplaceResult.Cancelled;
                    m_resultMessage = String.Format( "{0} was stopped in progress.", this.Title );
                    m_resultTitle = this.Title;
                    break;
                }

                FindReplaceNextInFileInternal( filenames[indexOf], options, openFile, changedFile, replace, true );

                if ( options.SearchUp )
                {
                    --indexOf;
                    if ( indexOf < 0 )
                    {
                        indexOf = filenames.Count - 1;
                    }
                }
                else
                {
                    ++indexOf;
                    if ( indexOf >= filenames.Count )
                    {
                        indexOf = 0;
                    }
                }

                if ( (m_result == SearchReplaceResult.Error) || (m_result == SearchReplaceResult.Found)
                    || (indexOf == startingIndex) )
                {
                    break;
                }

                changedFile = true;
            }
        }

        private void FindReplaceNextInDirectoryInternal( string directory, bool recurseSubDirectories, List<string> extensions, string currentFilename, 
            FindReplaceOptions options, bool openFile, bool replace, string currentFileDirectory, bool diveToCurrentFile )
        {
            if ( this.backgroundWorker.IsBusy )
            {
                Progress p = new Progress( Progress.ProgressType.ReportProgress,
                    new SearchReplaceReportProgressEventArgs( -1, directory, m_searchType ) );
                this.backgroundWorker.ReportProgress( -1, p );
            }

            // we're still searching for our currentFileDirectory, or we've moved on
            if ( ((directory == currentFileDirectory) && diveToCurrentFile) || !diveToCurrentFile )
            {
                try
                {
                    bool searchFiles = String.IsNullOrEmpty( currentFilename ) || !diveToCurrentFile;

                    foreach ( string extension in extensions )
                    {
                        string[] filenames = Directory.GetFiles( directory, extension );

                        if ( this.backgroundWorker.IsBusy && this.backgroundWorker.CancellationPending )
                        {
                            m_result = SearchReplaceResult.Cancelled;
                            m_resultMessage = String.Format( "{0} was stopped in progress.", this.Title );
                            m_resultTitle = this.Title;
                            return;
                        }

                        foreach ( string filename in filenames )
                        {
                            if ( this.backgroundWorker.IsBusy && this.backgroundWorker.CancellationPending )
                            {
                                m_result = SearchReplaceResult.Cancelled;
                                m_resultMessage = String.Format( "{0} was stopped in progress.", this.Title );
                                m_resultTitle = this.Title;
                                return;
                            }

                            if ( searchFiles )
                            {
                                FindReplaceNextInFileInternal( filename, options, openFile, false, replace, true );
                                if ( (m_result == SearchReplaceResult.Error) || (m_result == SearchReplaceResult.Found) )
                                {
                                    return;
                                }
                            }

                            if ( filename.ToLower() == currentFilename.ToLower() )
                            {
                                searchFiles = true;
                                diveToCurrentFile = false;
                            }
                        }
                    }
                }
                catch ( Exception e )
                {
                    m_result = SearchReplaceResult.Error;
                    m_resultMessage = String.Format( "An error occurred:\n{0}", e.Message );
                    m_resultTitle = String.Format( "{0} Error", this.Title );
                    return;
                }
            }

            if ( !recurseSubDirectories && !diveToCurrentFile )
            {
                // don't look in sub-folders
                return;
            }

            m_result = SearchReplaceResult.NotFound;
            m_resultMessage = "The specified text was not found.";
            m_resultTitle = this.Title;

            // Recursively look through all the files in the current directory's subdirectories.
            try
            {
                string[] dirs = Directory.GetDirectories( directory );

                if ( this.backgroundWorker.IsBusy && this.backgroundWorker.CancellationPending )
                {
                    m_result = SearchReplaceResult.Cancelled;
                    m_resultMessage = String.Format( "{0} was stopped in progress.", this.Title );
                    m_resultTitle = this.Title;
                    return;
                }

                foreach ( string dir in dirs )
                {
                    if ( this.backgroundWorker.IsBusy && this.backgroundWorker.CancellationPending )
                    {
                        m_result = SearchReplaceResult.Cancelled;
                        m_resultMessage = String.Format( "{0} was stopped in progress.", this.Title );
                        m_resultTitle = this.Title;
                        return;
                    }

                    // search sub-folders if we're diving to current file and the currentFilename is in the folder 'dir'
                    // or if we're not diving and we need to search sub-folders
                    if ( (diveToCurrentFile && (currentFileDirectory.IndexOf( dir ) != -1))
                        || (!diveToCurrentFile && recurseSubDirectories) )
                    {
                        FindReplaceNextInDirectoryInternal( dir, recurseSubDirectories, extensions, currentFilename,
                            options, openFile, replace, currentFileDirectory, diveToCurrentFile );
                        if ( (m_result == SearchReplaceResult.Error) || (m_result == SearchReplaceResult.Found) )
                        {
                            return;
                        }
                    }
                }
            }
            catch ( Exception e )
            {
                m_result = SearchReplaceResult.Error;
                m_resultMessage = String.Format( "An error occurred:\n{0}", e.Message );
                m_resultTitle = String.Format( "{0} Error", this.Title );
                return;
            }
        }

        private void FindReplaceAllInFileInternal( string filename, FindReplaceOptions options,
            bool openFile, bool searchFromTop, bool replace, bool mark, bool markWithBookmarks, bool tallyResults )
        {
            if ( filename != m_lastFilenameSearched )
            {
                m_lastStartOffset = 0;
            }

            SearchFile searchFile = CreateSearchFile( filename );
            Debug.Assert( searchFile != null, "CreateSearchFile should never return null." );

            ++m_numFilesSearched;

            int startTicks = Environment.TickCount;

            List<SearchReplaceResultData> results = null;
            try
            {
                if ( mark )
                {
                    results = MarkAll( searchFile, options, markWithBookmarks );
                }
                else
                {
                    results = FindReplaceAll( searchFile, options, replace );
                }
            }
            catch ( System.Exception e )
            {
                m_result = SearchReplaceResult.Error;
                m_resultMessage = String.Format( "An error occurred:{0}{1}", Environment.NewLine, e.Message );
                m_resultTitle = String.Format( "{0} Error", this.Title );
                
                searchFile.Dispose();
                return;
            }

            bool foundSomething = false;
            foreach ( SearchReplaceResultData resultData in results )
            {
                DetermineResult( resultData );

                if ( m_result == SearchReplaceResult.Found )
                {
                    if ( tallyResults )
                    {
                        m_lastStartOffset = resultData.EndOffset;

                        SearchReplaceFoundStringEventArgs e = new SearchReplaceFoundStringEventArgs( searchFile.Filename, 
                            resultData.Line, resultData.Index, resultData.Text, m_searchType );
                        if ( this.backgroundWorker.IsBusy )
                        {
                            Progress p = new Progress( Progress.ProgressType.FoundString, e );
                            this.backgroundWorker.ReportProgress( -1, p );                            
                        }
                        else
                        {
                            OnFoundString( e );
                        }

                        ++m_numFound;

                        foundSomething = true;
                    }

                    if ( openFile )
                    {
                        // load the file or give it focus
                        if ( !OnLoadFile( new SearchReplaceLoadFileEventArgs( filename ) ) )
                        {
                            searchFile.Dispose();
                            return;
                        }

                        // if we actually opened it just now, we need to perform the replace again to highlight the text
                        if ( !searchFile.IsOpen )
                        {
                            --m_numFilesSearched; // fixup
                            FindReplaceAllInFileInternal( filename, options, false, false, replace, mark, markWithBookmarks, false );

                            if ( foundSomething )
                            {
                                ++m_numFilesFound;
                            }

                            searchFile.Dispose();
                            return;
                        }

                        openFile = false;   // so we only do this once
                    }
                    else if ( replace && !searchFile.IsOpen )
                    {
                        // we need to save the file now
                        SearchClosedFile searchClosedFile = searchFile as SearchClosedFile;

                        if ( !OnSaveDocument( new SearchReplaceSaveTextEventArgs( searchClosedFile.Text, searchClosedFile.Filename ) ) )
                        {
                            searchFile.Dispose();
                            return;
                        }
                    }
                }
                else
                {
                    m_lastStartOffset = 0;
                }                
            }

            if ( foundSomething )
            {
                ++m_numFilesFound;
            }

            int endTicks = Environment.TickCount;
            m_totalTickCount += (endTicks - startTicks);

            searchFile.Dispose();
        }

        private void FindReplaceAllInFilesInternal( List<string> filenames, FindReplaceOptions options, bool openFile, bool replace,
            bool mark, bool markWithBookmarks )
        {
            int count = 0;
            foreach ( string filename in filenames )
            {
                if ( this.backgroundWorker.IsBusy && this.backgroundWorker.CancellationPending )
                {
                    m_result = SearchReplaceResult.Cancelled;
                    m_resultMessage = String.Format( "{0} was stopped in progress.", this.Title );
                    m_resultTitle = this.Title;
                    break;
                }

                FindReplaceAllInFileInternal( filename, options, openFile,
                    true, replace, mark, markWithBookmarks, true );

                if ( m_result == SearchReplaceResult.Error )
                {
                    break;
                }

                ++count;

                if ( this.backgroundWorker.IsBusy )
                {
                    int progress = (int)(((float)count / (float)filenames.Count) * 100.0f);
                    Progress p = new Progress( Progress.ProgressType.ReportProgress,
                        new SearchReplaceReportProgressEventArgs( progress, string.Empty, m_searchType ) );

                    this.backgroundWorker.ReportProgress( progress, p );
                    
                    Thread.Sleep( 2 );
                }
            }

            if ( m_result != SearchReplaceResult.Error )
            {
                if ( m_numFound > 0 )
                {
                    m_result = SearchReplaceResult.Found;
                }
                else
                {
                    m_result = SearchReplaceResult.NotFound;
                    m_resultMessage = "The specified text was not found.";
                }
            }
        }

        private void FindReplaceAllInDirectoryInternal( string directory, bool recurseSubDirectories, List<string> extensions, 
            FindReplaceOptions options, bool openFile, bool replace, bool mark, bool markWithBookmarks )
        {
            if ( this.backgroundWorker.IsBusy )
            {
                Progress p = new Progress( Progress.ProgressType.ReportProgress,
                    new SearchReplaceReportProgressEventArgs( -1, directory, m_searchType ) );
                this.backgroundWorker.ReportProgress( -1, p );
            }

            try
            {
                foreach ( string extension in extensions )
                {
                    string[] filenames = Directory.GetFiles( directory, extension );

                    if ( this.backgroundWorker.IsBusy && this.backgroundWorker.CancellationPending )
                    {
                        m_result = SearchReplaceResult.Cancelled;
                        m_resultMessage = String.Format( "{0} was stopped in progress.", this.Title );
                        m_resultTitle = this.Title;
                        return;
                    }

                    foreach ( string filename in filenames )
                    {
                        if ( this.backgroundWorker.IsBusy && this.backgroundWorker.CancellationPending )
                        {
                            m_result = SearchReplaceResult.Cancelled;
                            m_resultMessage = String.Format( "{0} was stopped in progress.", this.Title );
                            m_resultTitle = this.Title;
                            return;
                        }

                        FindReplaceAllInFileInternal( filename.ToLower(), options, openFile, true, replace, mark, markWithBookmarks, true );

                        if ( m_result == SearchReplaceResult.Error )
                        {
                            return;
                        }

                        if ( this.backgroundWorker.IsBusy )
                        {
                            Thread.Sleep( 1 );
                        }
                    }
                }
            }
            catch ( Exception e )
            {
                m_result = SearchReplaceResult.Error;
                m_resultMessage = String.Format( "An error occurred:\n{0}", e.Message );
                m_resultTitle = String.Format( "{0} Error", this.Title );
                return;
            }

            if ( !recurseSubDirectories )
            {
                return;
            }

            // Recursively add all the files in the current directory's subdirectories.
            try
            {
                string[] dirs = Directory.GetDirectories( directory );

                if ( this.backgroundWorker.IsBusy && this.backgroundWorker.CancellationPending )
                {
                    m_result = SearchReplaceResult.Cancelled;
                    m_resultMessage = String.Format( "{0} was stopped in progress.", this.Title );
                    m_resultTitle = this.Title;
                    return;
                }

                foreach ( string dir in dirs )
                {
                    if ( this.backgroundWorker.IsBusy && this.backgroundWorker.CancellationPending )
                    {
                        m_result = SearchReplaceResult.Cancelled;
                        m_resultMessage = String.Format( "{0} was stopped in progress.", this.Title );
                        m_resultTitle = this.Title;
                        return;
                    }

                    FindReplaceAllInDirectoryInternal( dir, true, extensions, options, openFile, replace, mark, markWithBookmarks );
                    if ( m_result == SearchReplaceResult.Error )
                    {
                        return;
                    }
                }
            }
            catch ( Exception e )
            {
                m_result = SearchReplaceResult.Error;
                m_resultMessage = String.Format( "An error occurred:\n{0}", e.Message );
                m_resultTitle = String.Format( "{0} Error", this.Title );
                return;
            }

            if ( m_result != SearchReplaceResult.Error )
            {
                if ( m_numFound > 0 )
                {
                    m_result = SearchReplaceResult.Found;
                }
                else
                {
                    m_result = SearchReplaceResult.NotFound;
                    m_resultMessage = "The specified text was not found.";
                }
            }
        }
        #endregion

        #region Helper Classes
        private class Progress
        {
            public Progress( ProgressType progressType, EventArgs e )
            {
                m_progressType = progressType;
                m_eventArgs = e;
            }

            #region Enums
            public enum ProgressType
            {
                ReportProgress,
                FoundString,
            }
            #endregion

            #region Variables
            private ProgressType m_progressType;
            private EventArgs m_eventArgs;
            #endregion

            #region Properties
            public ProgressType Type
            {
                get
                {
                    return m_progressType;
                }
            }

            public EventArgs EventArgs
            {
                get
                {
                    return m_eventArgs;
                }
            }
            #endregion
        }
        #endregion
    }
}
