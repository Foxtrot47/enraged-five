using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ragScriptEditorShared
{
    public partial class FolderListBrowserEditControl : UserControl
    {
        protected FolderListBrowserEditControl()
        {
            InitializeComponent();
        }

        public FolderListBrowserEditControl( string description, bool showNewFolderButton )
            : this()
        {
            m_folderBrowserDialog = new FolderBrowserDialog( description, showNewFolderButton );
        }

        #region Variables
        FolderBrowserDialog m_folderBrowserDialog;
        #endregion

        #region Properties
        public List<string> Folders
        {
            get
            {
                return m_folderBrowserDialog.Folders;
            }
        }
        #endregion

        #region Event Handlers
        private void FolderBrowserEditControl_Load( object sender, EventArgs e )
        {
            m_folderBrowserDialog.ShowDialog( this );
        }
        #endregion
    }
}
