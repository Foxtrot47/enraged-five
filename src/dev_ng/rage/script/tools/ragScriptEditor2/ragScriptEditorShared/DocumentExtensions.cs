﻿//---------------------------------------------------------------------------------------------
// <copyright file="DocumentExtensions.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2019. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace ragScriptEditorShared
{
    using System;
    using System.IO;
    using System.Text;
    using ActiproSoftware.SyntaxEditor;

    /// <summary>
    /// Extension methods for <see cref="Document"/>.
    /// </summary>
    public static class DocumentExtensions
    {
        /// <summary>
        /// Save the file to the provided filename, using buffering to avoid copying the contents of the file to a string.
        /// This is an attempt to reduce memory issues when saving very large files.
        /// </summary>
        /// <param name="doc">The document.</param>
        /// <param name="filename">The filename to save to.</param>
        /// <param name="lineTerminator">The line terminator to use.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="doc"/> or <paramref name="filename"/> is null.</exception>
        public static void SaveFileBuffered(this Document doc, string filename, LineTerminator lineTerminator)
        {
            // Static instance Encoding.UTF8 writes a BOM to the file, so we're making our own instance.
            doc.SaveFileBuffered(filename, new UTF8Encoding(false, false), lineTerminator);
        }

        /// <summary>
        /// Save the file to the provided filename, using buffering to avoid copying the contents of the file to a string.
        /// This overload allows specifying a custom encoding.
        /// This is an attempt to reduce memory issues when saving very large files.
        /// </summary>
        /// <param name="doc">The document.</param>
        /// <param name="filename">The filename to save to.</param>
        /// <param name="encoding">The text encoding.</param>
        /// <param name="lineTerminator">The line terminator to use.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="doc"/>, <paramref name="filename"/>, or <paramref name="encoding"/> is null.</exception>
        public static void SaveFileBuffered(this Document doc, string filename, Encoding encoding, LineTerminator lineTerminator)
        {
            if (filename == null)
            {
                throw new ArgumentNullException(nameof(filename));
            }

            using (Stream stream = File.Create(filename))
            {
                doc.SaveFileBuffered(stream, encoding, lineTerminator);
            }
        }

        /// <summary>
        /// Save the file to the provided stream, using buffering to avoid copying the contents of the file to a string.
        /// This is an attempt to reduce memory issues when saving very large files.
        /// </summary>
        /// <param name="doc">The document.</param>
        /// <param name="stream">The stream to write to. Stream will not be disposed.</param>
        /// <param name="encoding">The text encoding.</param>
        /// <param name="lineTerminator">The line terminator to use.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="doc"/>, <paramref name="stream"/>, or <paramref name="encoding"/> is null.</exception>
        /// <exception cref="ArgumentException">Thrown if <paramref name="stream"/> is not writable.</exception>
        public static void SaveFileBuffered(this Document doc, Stream stream, Encoding encoding, LineTerminator lineTerminator)
        {
            if (doc == null)
            {
                throw new ArgumentNullException(nameof(doc));
            }

            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }

            StringBuilder sb = doc.GetCoreTextBuffer();
            StreamWriter writer = new StreamWriter(stream, encoding)
            {
                AutoFlush = true
            };

            const int bufSize = 4096;
            for (int i = 0; i < sb.Length; i += bufSize)
            {
                string buf = sb.ToString(i, Math.Min(bufSize, sb.Length - i));
                switch (lineTerminator)
                {
                    case LineTerminator.CarriageReturn:
                        buf = buf.Replace("\n", "\r");
                        break;
                    case LineTerminator.CarriageReturnNewline:
                        buf = buf.Replace("\n", "\r\n");
                        break;
                }

                writer.Write(buf);
            }
        }
    }
}
