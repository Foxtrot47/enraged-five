﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ragScriptEditorShared
{
    /// <summary>
    /// Type of message to show in the error box.
    /// </summary>
    public enum ErrorType
    {
        Info,
        Warning,
        Errors,
    }
}
