using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragScriptEditorShared
{
    public partial class FolderBrowserDialog : Form
    {
        protected FolderBrowserDialog()
        {
            InitializeComponent();
        }

        public FolderBrowserDialog( string description, bool showNewFolderButton )
            : this()
        {
            this.folderBrowserDialog1.Description = description;
            this.folderBrowserDialog1.ShowNewFolderButton = showNewFolderButton;
        }

        #region Properties
        public List<string> Folders
        {
            get
            {
                string[] split = this.foldersTextBox.Text.Split( new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries );
                return new List<string>( split );
            }
        }
        #endregion

        #region Event Handlers
        private void browseFolderButton_Click( object sender, EventArgs e )
        {
            this.folderBrowserDialog1.ShowDialog( this );
        }
        #endregion
    }
}