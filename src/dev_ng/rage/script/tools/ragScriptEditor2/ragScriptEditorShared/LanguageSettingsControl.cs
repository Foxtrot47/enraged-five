using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Command;
using RSG.Base.Forms;
using RSG.Base.IO;

namespace ragScriptEditorShared
{
    public partial class LanguageSettingsControl : ragScriptEditorShared.SettingsControlBase
    {
        public LanguageSettingsControl()
        {
            InitializeComponent();
        }

        #region Properties
        public bool SaveDefaultSettingsEnabled
        {
            get
            {
                return this.saveDefaultSettingsButton.Enabled;
            }
            set
            {
                this.saveDefaultSettingsButton.Enabled = value;
            }
        }
        #endregion

        #region Overrides
        public override SettingsBase TabSettings
        {
            get
            {
                LanguageSettings languageSettings = new LanguageSettings();
                languageSettings.CacheDirectory = this.cacheDirectoryTextBox.Text.Trim();
                languageSettings.CodeSnippetPath = this.codeSnippetPathTextBox.Text.Trim();

                return languageSettings;
            }
            set
            {
                if ( value is LanguageSettings )
                {
                    m_invokeSettingsChanged = false;

                    LanguageSettings languageSettings = value as LanguageSettings;

                    this.cacheDirectoryTextBox.Text = languageSettings.CacheDirectory;
                    this.codeSnippetPathTextBox.Text = languageSettings.CodeSnippetPath;

                    m_invokeSettingsChanged = true;
                }
            }
        }
        #endregion

        #region Events
        public event EventHandler SaveDefaultSettings;

        public event EventHandler LoadDefaultSettings;
        #endregion

        #region Event Dispatchers
        private void OnSaveDefaultSettings()
        {
            if ( this.SaveDefaultSettings != null )
            {
                this.SaveDefaultSettings( this, EventArgs.Empty );
            }
        }

        private void OnLoadDefaultSettings()
        {
            if ( this.LoadDefaultSettings != null )
            {
                this.LoadDefaultSettings( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Event Handlers
        protected void SettingChanged( object sender, EventArgs e )
        {
            OnSettingsChanged();
        }

        private void saveDefaultSettingsButton_Click( object sender, EventArgs e )
        {
            OnSaveDefaultSettings();
        }

        private void loadDefaultSettingsButton_Click( object sender, EventArgs e )
        {
            OnLoadDefaultSettings();
        }

        private void clearCacheButton_Click( object sender, EventArgs e )
        {
            string directory = this.cacheDirectoryTextBox.Text.Trim();

            bool exists = false;
            try
            {
                exists = !String.IsNullOrEmpty( directory ) && Directory.Exists( directory );
            }
            catch ( Exception ex )
            {
                rageMessageBox.ShowError( 
                    String.Format( "{0}\n\nThe directory is invalid.  Please change it and try again.", ex.Message ),
                    "Invalid Directory" );
                return;
            }

            if ( exists )
            {
                DialogResult result = rageMessageBox.ShowWarning(
                    "Are you sure you want to delete the cache?  This operation cannot be undone.",
                    "Clear Cache", MessageBoxButtons.YesNo );
                if ( result == DialogResult.Yes )
                {
                    rageStatus status;
                    rageFileUtilities.DeleteFilesFromLocalFolder( directory, "*.*", out status );
                    if ( !status.Success() )
                    {
                        rageMessageBox.ShowError( status.ErrorString, "Clear Cache Failed" );
                    }
                }
            }
        }

        private void cacheDirectoryBrowseButton_Click( object sender, EventArgs e )
        {
            this.folderBrowserDialog.Description = "Select Cache Directory";
            this.folderBrowserDialog.ShowNewFolderButton = true;

            DialogResult result = this.folderBrowserDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.cacheDirectoryTextBox.Text = this.folderBrowserDialog.SelectedPath;
            }
        }

        private void codeSnippetPathBrowseButton_Click( object sender, EventArgs e )
        {
            this.folderBrowserDialog.Description = "Add Code Snippet Folder";
            this.folderBrowserDialog.ShowNewFolderButton = false;

            DialogResult result = this.folderBrowserDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                string path = this.codeSnippetPathTextBox.Text.Trim();
                if ( !path.EndsWith( Path.PathSeparator.ToString() ) )
                {
                    path += Path.PathSeparator;
                }

                path += this.folderBrowserDialog.SelectedPath;

                this.codeSnippetPathTextBox.Text = path;
            }
        }
        #endregion
    }
}

