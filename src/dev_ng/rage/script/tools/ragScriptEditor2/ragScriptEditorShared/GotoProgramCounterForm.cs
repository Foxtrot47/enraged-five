using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ragScriptEditorShared
{
    public partial class GotoProgramCounterForm : Form
    {
        private static Regex CounterRegex = new Regex(@"Script Name \= (?<ScriptName>\w+) : Program Counter \= (?<Counter>\d+)", RegexOptions.IgnoreCase);

        private String _lastTextValue = String.Empty;

        public GotoProgramCounterForm()
        {
            InitializeComponent();
        }

        public GotoProgramCounterForm(IEnumerable<String> allScripts, string currentScript)
        {
            InitializeComponent();

            this.programCombo.Items.AddRange(allScripts.ToArray());
            this.programCombo.SelectedItem = currentScript;

            this.RefreshOkButtonState();
        }

        /// <summary>
        /// Instantiates GotoScriptCounterForm and launches it as a dialog.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="allScriptNames">Script names with debug info.</param>
        /// <param name="selectedScript">Name of the currently selected script. Value will change if user changes it.</param>
        /// <returns>-1 on cancelled</returns>
        public static int ShowIt( Control parent, IEnumerable<String> allScriptNames, ref String selectedScript)
        {
            GotoProgramCounterForm gotoCounterForm = new GotoProgramCounterForm(allScriptNames, selectedScript);

            DialogResult result = gotoCounterForm.ShowDialog( parent );
            if ( result == DialogResult.OK )
            {
                selectedScript = gotoCounterForm.Script;
                return gotoCounterForm.ProgramCounter;
            }

            return -1;
        }

        #region Properties
        private int ProgramCounter
        {
            get
            {
                int counter = -1;
                if (String.IsNullOrWhiteSpace(this.counterTextBox.Text) ||
                    !Int32.TryParse(this.counterTextBox.Text.Trim(), out counter))
                {
                    return -1;
                }

                return counter;
            }
        }

        private String Script
        {
            get
            {
                return this.programCombo.Text;
            }
        }
        #endregion

        #region Event Handlers
        private void counterTextBox_TextChanged(Object sender, EventArgs e)
        {
            int dummy;
            Match match = CounterRegex.Match(this.counterTextBox.Text);
            if (match.Success)
            {
                int counter;
                if (int.TryParse(match.Groups["Counter"].Value, out counter))
                {
                    this.counterTextBox.Text = counter.ToString();
                }

                String scriptName = match.Groups["ScriptName"].Value;
                if (this.programCombo.Items
                    .OfType<String>()
                    .Contains(scriptName, StringComparer.OrdinalIgnoreCase))
                {
                    this.programCombo.SelectedItem = scriptName;
                }
            }
            else if (!String.IsNullOrWhiteSpace(this.counterTextBox.Text) && !int.TryParse(this.counterTextBox.Text, out dummy))
            {
                // If this didn't parse as debug output OR an int, don't change the text.
                this.counterTextBox.Text = this._lastTextValue;
            }

            this._lastTextValue = this.counterTextBox.Text;
            this.RefreshOkButtonState();
        }

        private void programCombo_TextUpdate(Object sender, EventArgs e)
        {
            this.RefreshOkButtonState();
        }

        private void programCombo_SelectedIndexChanged(Object sender, EventArgs e)
        {
            this.RefreshOkButtonState();
        }
        #endregion

        private void RefreshOkButtonState()
        {
            int dummy;
            this.okButton.Enabled = this.programCombo.Text != null && 
                this.programCombo.Items
                    .OfType<String>()
                    .Contains((String)this.programCombo.Text, StringComparer.OrdinalIgnoreCase) &&
                !String.IsNullOrWhiteSpace(this.counterTextBox.Text) && 
                int.TryParse(this.counterTextBox.Text.Trim(), out dummy);
        }
    }
}
