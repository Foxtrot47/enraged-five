using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using ActiproSoftware.SyntaxEditor;
using RSG.Base.Forms;
using RSG.Platform;

namespace ragScriptEditorShared
{
    public partial class EditorToolbar : Component
    {
        public EditorToolbar()
        {
            InitializeComponent();
        }

        public EditorToolbar( IContainer container )
            : this()
        {
            container.Add( this );
        }

        public EditorToolbar( Control parentControl )
            : this()
        {
            m_parentControl = parentControl;
        }

        #region Enums
        public enum DebuggerOptions
        {
            ShowLineNumbers = 0,
            HighlightEnums = 2,
            StartAutomaticOutlining = 4,
            StartManualOutlining,
            StopOutlining,
        }
        #endregion

        #region Constants
        const int c_maxUndoRedoDropDownItems = 10;
        const int c_toolStripSpacing = 3;
        #endregion

        #region Variables
        private Control m_parentControl;
        private List<ToolbarButtonSetting> m_buttonSettings = new List<ToolbarButtonSetting>();
        private List<ToolbarButtonSetting> m_customButtonSettings = new List<ToolbarButtonSetting>();
        #endregion

        #region Delegates
        public delegate void ToolbarButtonClickedEventHandler( object sender, ToolStripItemClickedEventArgs e );
        public delegate void DebuggerOptionsDropDownMenuOpeningEventHandler( object sender, DebuggerOptionsEventArgs e );
        public delegate void CompilingConfigurationChangedEventHandler( object sender, CompilingConfigurationChangedEventArgs e );
        public delegate void PlatformChangedEventHandler(object sender, PlatformChangedEventArgs e);
        public delegate void ShowLineNumbersChangedEventHandler( object sender, ShowLineNumbersChangedEventArgs e );
        public delegate void HighlightEnumsChangedEventHandler( object sender, HighlightEnumsChangedEventArgs e );
        public delegate void HighlightQuickInfoPopupsChangedEventHandler( object sender, HighlightQuickInfoPopupsChangedEventArgs e );
        public delegate void HighlightDisabledCodeBlocksChangedEventHandler( object sender, HighlightDisabledCodeBlocksChangedEventArgs e );
        #endregion

        #region Events
        public event ToolbarButtonClickedEventHandler ToolbarButtonClicked;
        public event EventHandler ToolbarAppActionButtonClicked;
        public event DebuggerOptionsDropDownMenuOpeningEventHandler DebuggerOptionsDropDownMenuOpening;
        public event CompilingConfigurationChangedEventHandler CompilingConfigurationChanged;
        public event PlatformChangedEventHandler PlatformChanged;
        public event ShowLineNumbersChangedEventHandler ShowLineNumbersChanged;
        public event HighlightEnumsChangedEventHandler HighlightEnumsChanged;
        public event HighlightQuickInfoPopupsChangedEventHandler HighlightQuickInfoPopupsChanged;
        public event HighlightDisabledCodeBlocksChangedEventHandler HighlightDisabledCodeBlocksChanged;
        public event EventHandler ChangeIncludesClicked;
        #endregion

        #region Properties
        public EventHandler OpenCustomButtonManager;

        public List<ToolStrip> ToolStrips
        {
            get
            {
                List<ToolStrip> strips = new List<ToolStrip>();
                strips.Add( this.editToolStrip );
                strips.Add( this.codeToolStrip );

                if ( ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor )
                {
                    strips.Add( this.macroToolStrip );
                    strips.Add( this.customToolStrip );
                    strips.Add( this.incredibuildToolStrip );
                }

                if ( ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Debugger )
                {
                    strips.Add( this.debuggerToolStrip );
                }

                return strips;
            }
        }

        public List<ToolbarButtonSetting> ButtonSettings
        {
            get
            {
                return m_buttonSettings;
            }
        }

        public List<ToolbarButtonSetting> CustomButtonSettings
        {
            get
            {
                return m_customButtonSettings;
            }
        }

        public ActiproSoftware.SyntaxEditor.SyntaxEditor Editor;
        #endregion

        #region Initialization
        public List<ToolStrip> Init( ToolbarSettings toolbarSettings, ToolStripContainer toolStripContainer,
            bool showLineNumbers, bool highlightEnums, bool highlightQuickInfoPopups, bool highlightDisabledCodeBlocks )
        {
            m_buttonSettings = toolbarSettings.ToolbarButtonSettings;

            if ( ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Debugger )
            {
                this.newToolStripButton.Enabled = false;
                this.saveAllToolStripButton.Enabled = false;
                this.compilingConfigurationToolStripComboBox.Enabled = false;

                this.showLineNumbersToolStripMenuItem.Checked = showLineNumbers;
                this.debuggerHighlightEnumsToolStripMenuItem.Checked = highlightEnums;
                this.debuggerHighlightQuickInfoPopupsToolStripMenuItem.Checked = highlightQuickInfoPopups;
                this.debuggerHighlightDisabledCodeBlocksToolStripMenuItem.Checked = highlightDisabledCodeBlocks;
            }
            else
            {
                AddCustomButtonsFromToolbarButtonSettings( m_buttonSettings );
            }

            if ( OpenCustomButtonManager != null )
            {
                this.customizeToolStripMenuItem.Click += OpenCustomButtonManager;
            }
            else
            {
                this.customizeToolStripMenuItem.Enabled = false;
            }

            LinkButtonSettingsToToolStripItems( m_buttonSettings );

            ResetToolbarVisiblity( m_buttonSettings,
                (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Debugger) ? true : !toolbarSettings.RememberButtonVisiblity );

            SetToolStripLocations( toolStripContainer, toolbarSettings.ToolStripLocations );

            return this.ToolStrips;
        }
        #endregion

        #region Event Handlers
        private void ToolStripButton_Click( object sender, EventArgs e )
        {
            ToolStripItem item = sender as ToolStripItem;
            if ( (item == null) || (item.Tag == null) )
            {
                return;
            }

            if ( item is ToolStripSplitButton )
            {
                ToolStripSplitButton button = item as ToolStripSplitButton;
                if ( button.DropDownButtonPressed )
                {
                    return;
                }
            }

            ToolbarButtonSetting bs = item.Tag as ToolbarButtonSetting;
            Debug.Assert( bs != null, "bs != null" );
            Debug.Assert( !bs.IsCustomButton, "!bs.IsCustomButton" );

            if ( ToolbarButtonClicked != null )
            {
                ToolStripItemClickedEventArgs clickedEvent = new ToolStripItemClickedEventArgs( item );
                ToolbarButtonClicked( this, clickedEvent );
            }
        }

        private void compilingConfigurationToolStripComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.compilingConfigurationToolStripComboBox.SelectedIndex > -1 )
            {
                if ( this.CompilingConfigurationChanged != null )
                {
                    this.CompilingConfigurationChanged( this, new CompilingConfigurationChangedEventArgs( this.compilingConfigurationToolStripComboBox.SelectedItem as string ) );
                }
            }
        }

        private void platformToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (platformToolStripComboBox.SelectedIndex > -1)
            {
                if (PlatformChanged != null)
                {
                    Platform platform = PlatformUtils.PlatformFromFriendlyName((String)platformToolStripComboBox.SelectedItem);
                    PlatformChanged(this, new PlatformChangedEventArgs(platform));
                }
            }
        }

        private void ToolStripCustomButton_Click( object sender, EventArgs e )
        {
            ToolStripItem item = sender as ToolStripItem;
            if ( (item == null) || (item.Tag == null) )
            {
                return;
            }

            ToolbarButtonSetting bs = item.Tag as ToolbarButtonSetting;
            Debug.Assert( bs != null, "bs != null" );
            Debug.Assert( bs.IsCustomButton, "bs.IsCustomButton" );

            ExecuteCommandLine( bs.CommandLine );
        }

        private void debuggerOptionsToolStripDropDownButton_DropDownOpening( object sender, EventArgs e )
        {
            if ( DebuggerOptionsDropDownMenuOpening != null )
            {
                DebuggerOptionsDropDownMenuOpening( sender, new DebuggerOptionsEventArgs( this.debuggerOptionsToolStripDropDownButton ) );
            }
        }

        private void ExecuteAppAction_Click( object sender, EventArgs e )
        {
            if ( ToolbarAppActionButtonClicked != null )
            {
                ToolbarAppActionButtonClicked( sender, e );
            }
        }

        private void debuggerHighlightEnumsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( HighlightEnumsChanged != null )
            {
                HighlightEnumsChanged( sender, new HighlightEnumsChangedEventArgs( this.debuggerHighlightEnumsToolStripMenuItem.Checked ) );
            }
        }

        private void debuggerHighlightQuickInfoPopupsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( HighlightQuickInfoPopupsChanged != null )
            {
                HighlightQuickInfoPopupsChanged( sender, new HighlightQuickInfoPopupsChangedEventArgs( this.debuggerHighlightQuickInfoPopupsToolStripMenuItem.Checked ) );
            }
        }

        private void debuggerHighlightDisabledCodeBlocksToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( HighlightDisabledCodeBlocksChanged != null )
            {
                HighlightDisabledCodeBlocksChanged( sender, new HighlightDisabledCodeBlocksChangedEventArgs( this.debuggerHighlightDisabledCodeBlocksToolStripMenuItem.Checked ) );
            }
        }

        private void showLineNumbersToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( ShowLineNumbersChanged != null )
            {
                ShowLineNumbersChanged( sender, new ShowLineNumbersChangedEventArgs( this.showLineNumbersToolStripMenuItem.Checked ) );
            }
        }

        private void AddRemoveContextMenuStrip_ItemClicked( object sender, ToolStripItemClickedEventArgs e )
        {
            if ( e.ClickedItem.Tag == null )
            {
                return;
            }

            ToolbarButtonSetting bs = e.ClickedItem.Tag as ToolbarButtonSetting;
            Debug.Assert( bs != null, "bs != null" );

            ToolStripMenuItem item = e.ClickedItem as ToolStripMenuItem;
            bs.ButtonItem.Visible = !item.Checked;   // HACK this is called before the checked state of the menu item is changed
            bs.Visible = bs.ButtonItem.Visible;
        }
        
        private void redoToolStripSplitButton_DropDownOpening( object sender, EventArgs e )
        {
            if ( Editor != null )
            {
                this.redoToolStripSplitButton.DropDownItems.Clear();

                int redoCount = Editor.Document.UndoRedo.RedoStack.Count;
                for ( int i = 0; i < c_maxUndoRedoDropDownItems && i < redoCount; ++i )
                {
                    this.redoToolStripSplitButton.DropDownItems.Add( Editor.Document.UndoRedo.RedoStack.GetName( redoCount - 1 - i ) );
                }
            }
        }

        private void redoToolStripSplitButton_DropDownItemClicked( object sender, ToolStripItemClickedEventArgs e )
        {
            if ( ToolbarButtonClicked != null )
            {
                int indexOf = this.redoToolStripSplitButton.DropDownItems.IndexOf( e.ClickedItem );
                for ( int i = 0; i <= indexOf; ++i )
                {
                    ToolbarButtonClicked( sender, new ToolStripItemClickedEventArgs( this.redoToolStripSplitButton ) );
                }
            }
        }

        private void undoToolStripSplitButton_DropDownOpening( object sender, EventArgs e )
        {
            if ( Editor != null )
            {
                this.undoToolStripSplitButton.DropDownItems.Clear();

                int undoCount = Editor.Document.UndoRedo.UndoStack.Count;
                for ( int i = 0; i < c_maxUndoRedoDropDownItems && i < undoCount; ++i )
                {
                    this.undoToolStripSplitButton.DropDownItems.Add( Editor.Document.UndoRedo.UndoStack.GetName( undoCount - 1 - i ) );
                }
            }
        }

        private void undoToolStripSplitButton_DropDownItemClicked( object sender, ToolStripItemClickedEventArgs e )
        {
            if ( ToolbarButtonClicked != null )
            {
                int indexOf = this.undoToolStripSplitButton.DropDownItems.IndexOf( e.ClickedItem );
                for ( int i = 0; i <= indexOf; ++i )
                {
                    ToolbarButtonClicked( sender, new ToolStripItemClickedEventArgs( this.undoToolStripSplitButton ) );
                }
            }
        }

        private void changeIncludesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( ChangeIncludesClicked != null )
            {
                ChangeIncludesClicked( sender, e );
            }
        }
        #endregion

        #region Custom Buttons
        private ToolbarButtonSetting GetCustomButtonSetting( string tag )
        {
            foreach ( ToolbarButtonSetting button in m_customButtonSettings )
            {
                if ( button.Tag == tag )
                {
                    return button;
                }
            }

            return null;
        }

        public void AddCustomButton( ToolbarButtonSetting customButton )
        {
            customButton.Tag = ToolbarButtonSetting.ConvertNameToTag( customButton.Text );

            // create toolbar button
            customButton.ButtonItem = new ToolStripButton();
            customButton.ButtonItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            customButton.ButtonItem.Image = customButton.Icon;
            customButton.ButtonItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            customButton.ButtonItem.Name = customButton.Text;
            customButton.ButtonItem.Size = new System.Drawing.Size( 23, 22 );
            customButton.ButtonItem.Tag = customButton;
            customButton.ButtonItem.Text = customButton.Text;
            customButton.ButtonItem.ToolTipText = customButton.Text;
            customButton.ButtonItem.Click += new EventHandler( ToolStripCustomButton_Click );
            this.customToolStrip.Items.Insert( customToolStrip.Items.Count - 1, customButton.ButtonItem );  // add right before ToolStripDropDownButton

            // create visibility button
            customButton.MenuItem = new ToolStripMenuItem();
            customButton.MenuItem.Checked = true;
            customButton.MenuItem.CheckOnClick = true;
            customButton.MenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            customButton.MenuItem.Image = customButton.Icon;
            customButton.MenuItem.Name = "visible" + customButton.Text;
            customButton.MenuItem.Size = new System.Drawing.Size( 195, 22 );
            customButton.MenuItem.Tag = customButton;
            customButton.MenuItem.Text = customButton.Text;
            this.customAddRemoveContextMenuStrip.Items.Add( customButton.MenuItem );

            // now add the new one
            m_customButtonSettings.Add( customButton );

            if ( m_buttonSettings.IndexOf(customButton) == -1 )
            {
                m_buttonSettings.Add( customButton );
            }
        }

        public void DeleteCustomButtons()
        {
            while ( m_customButtonSettings.Count > 0 )
            {
                RemoveCustomButton( m_customButtonSettings[0] );
            }
        }

        public void RemoveCustomButton( ToolbarButtonSetting customButton )
        {
            if ( customButton.ButtonItem != null )
            {
                this.customToolStrip.Items.Remove( customButton.ButtonItem );
            }

            if ( customButton.MenuItem != null )
            {
                this.customAddRemoveContextMenuStrip.Items.Remove( customButton.MenuItem );
            }

            m_buttonSettings.Remove( customButton );
            m_customButtonSettings.Remove( customButton );
        }

        public void MoveCustomButton( int direction, ToolbarButtonSetting customButton )
        {
            if ( customButton.ButtonItem == null )
            {
                return;
            }

            // move in toolbar
            int index = customToolStrip.Items.IndexOf( customButton.ButtonItem );
            this.customToolStrip.Items.Remove( customButton.ButtonItem );
            this.customToolStrip.Items.Insert( index + direction, customButton.ButtonItem );

            index = customAddOrRemoveToolStripMenuItem.DropDownItems.IndexOf( customButton.MenuItem );
            this.customAddRemoveContextMenuStrip.Items.Remove( customButton.MenuItem );
            this.customAddRemoveContextMenuStrip.Items.Insert( index + direction, customButton.MenuItem );

            // move in array
            index = m_customButtonSettings.IndexOf( customButton );
            m_customButtonSettings.Remove( customButton );
            m_customButtonSettings.Insert( index + direction, customButton );

            index = m_buttonSettings.IndexOf( customButton );
            m_buttonSettings.Remove( customButton );
            m_buttonSettings.Insert( index + direction, customButton );
        }

        public bool IsValidButtonName( string name )
        {
            foreach ( ToolbarButtonSetting button in m_customButtonSettings )
            {
                if ( button.Text == name )
                {
                    return false;
                }
            }

            return true;
        }

        private void ExecuteCommandLine( string commandLine )
        {
            ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( commandLine );
            if ( psInfo != null )
            {
                psInfo.UseShellExecute = false;

                Process p = null;
                try
                {
                    p = Process.Start( psInfo );
                }
                catch ( Exception exception )
                {
                    rageMessageBox.ShowError( m_parentControl, 
                        String.Format( "There was an error executing '{0}'\n{1}", commandLine, exception.ToString() ),
                        "Custom Button Error" );
                }
            }
            else
            {
                rageMessageBox.ShowExclamation( m_parentControl, 
                    String.Format( "Unable to execute '{0}'.  Improperly formatted command.", commandLine ), "Custom Button Command Error" );
            }
        }        
        #endregion

        #region Settings
        //-START Old format, but kept for compatibility
        public bool ReadXmlSettings( XmlTextReader xmlReader )
        {
            string parentName = xmlReader.Name;

            m_buttonSettings.Clear();

            while ( xmlReader.Read() )
            {
                if ( !xmlReader.IsStartElement() )
                {
                    if ( xmlReader.Name == parentName )
                    {
                        break; // we're done
                    }
                }
                else if ( xmlReader.Name == "button" )
                {
                    // read attributes
                    string tag = xmlReader["name"];
                    string visible = xmlReader["visible"];
                    string custom = xmlReader["custom"];
                    string icon = xmlReader["icon"];

                    if ( (tag == null) || (tag == "") )
                    {
                        continue;
                    }

                    // default is visible
                    if ( visible == null )
                    {
                        visible = "true";
                    }

                    // no duplicates
                    bool found = false;
                    foreach ( ToolbarButtonSetting b in m_buttonSettings)
                    {
                        if ( b.Tag == tag )
                        {
                            found = true;
                            break;
                        }
                    }

                    // add to hash tables
                    if ( !found )
                    {
                        ToolbarButtonSetting bs = new ToolbarButtonSetting();
                        bs.Tag = tag;
                        bs.Visible = Convert.ToBoolean( visible );

                        if ( (custom != null) && (custom != "") )
                        {
                            bs.IsCustomButton = true;
                            bs.Text = ToolbarButtonSetting.ConvertTagToName( bs.Tag );
                            bs.IconFile = icon;
                            bs.CommandLine = custom;
                        }

                        m_buttonSettings.Add( bs );
                    }
                }
            }

            return true;
        }
        //-STOP        
        #endregion

        #region Misc
        private void AddCustomButtonsFromToolbarButtonSettings( List<ToolbarButtonSetting> buttonSettings )
        {
            // look for custom buttons
            foreach ( ToolbarButtonSetting bs in buttonSettings )
            {
                if ( !bs.IsCustomButton )
                {
                    continue;
                }

                try
                {
                    bs.Icon = System.Drawing.Image.FromFile( bs.IconFile );
                }
                catch ( Exception e )
                {
                    rageMessageBox.ShowError( m_parentControl, 
                        String.Format( "Unable to load the icon file '{0}'.\n\n{1}", bs.IconFile, e.Message ), "Icon File Load Error" );
                    continue;
                }

                AddCustomButton( bs );
            }
        }

        private void LinkButtonSettingsToToolStripItems( List<ToolbarButtonSetting> buttonSettings )
        {
            LinkButtonSettingsToToolStripItems( buttonSettings, this.editToolStrip, this.editAddRemoveContextMenuStrip );
            LinkButtonSettingsToToolStripItems( buttonSettings, this.codeToolStrip, this.codeAddRemoveContextMenuStrip );
            LinkButtonSettingsToToolStripItems( buttonSettings, this.customToolStrip, this.customAddRemoveContextMenuStrip );
            LinkButtonSettingsToToolStripItems( buttonSettings, this.macroToolStrip, null );
            LinkButtonSettingsToToolStripItems( buttonSettings, this.debuggerToolStrip, null );
            LinkButtonSettingsToToolStripItems( buttonSettings, this.incredibuildToolStrip, null );
        }

        /// <summary>
        /// We're going to change the ToolStripButton and ToolStripMenuItem tags from strings to ToolbarButtonSetting
        /// </summary>
        /// <param name="buttonSettings"></param>
        /// <param name="strip"></param>
        /// <param name="contextStrip"></param>
        private void LinkButtonSettingsToToolStripItems( List<ToolbarButtonSetting> buttonSettings, ToolStrip strip,
            ContextMenuStrip contextStrip )
        {
            foreach ( ToolStripItem item in strip.Items )
            {
                if ( !(item is ToolStripButton) && !(item is ToolStripSplitButton) && !(item is ToolStripComboBox) )
                {
                    continue;
                }

                if ( !(item.Tag is string) )
                {
                    continue;
                }

                bool found = false;
                foreach ( ToolbarButtonSetting bs in buttonSettings )
                {
                    if ( bs.Tag == item.Tag.ToString() )
                    {
                        bs.ButtonItem = item;
                        bs.ButtonItem.Tag = bs;
                        found = true;
                        break;
                    }
                }

                // make sure we have a ToolbarButtonSetting for everyone
                if ( !found )
                {
                    ToolbarButtonSetting bs = new ToolbarButtonSetting();
                    bs.ButtonItem = item;

                    bs.Tag = item.Tag.ToString();
                    bs.ButtonItem.Tag = bs;

                    bs.Text = (item is ToolStripComboBox) ? item.ToolTipText : item.Text;
                    bs.Visible = true;//bs.Visible = item.Visible;

                    buttonSettings.Add( bs );
                }
            }

            if ( contextStrip == null )
            {
                return;
            }

            foreach ( ToolStripItem item in contextStrip.Items )
            {
                if ( !(item is ToolStripMenuItem) )
                {
                    continue;
                }

                if ( !(item.Tag is string) )
                {
                    continue;
                }

                bool found = false;
                foreach ( ToolbarButtonSetting bs in buttonSettings )
                {
                    if ( bs.Tag == item.Tag.ToString() )
                    {
                        bs.MenuItem = item as ToolStripMenuItem;
                        item.Tag = bs;
                        found = true;
                        break;
                    }
                }

                // make sure we have a ToolbarButtonSetting for everyone
                if ( !found )
                {
                    ToolbarButtonSetting bs = new ToolbarButtonSetting();
                    bs.MenuItem = item as ToolStripMenuItem;

                    bs.Tag = item.Tag.ToString();
                    bs.MenuItem.Tag = bs;

                    bs.Text = item.Text;
                    bs.Visible = item.Visible;

                    buttonSettings.Add( bs );
                }
            }
        }

        public void ResetToolbarVisiblity( bool forceVisible )
        {
            ResetToolbarVisiblity( m_buttonSettings, forceVisible );
        }

        private void ResetToolbarVisiblity( List<ToolbarButtonSetting> buttonSettings, bool forceVisible )
        {
            List<ToolStrip> strips = this.ToolStrips;
            foreach ( ToolStrip strip in strips )
            {
                ResetToolbarVisiblity( strip, buttonSettings, forceVisible );
            }
        }

        private void ResetToolbarVisiblity( ToolStrip strip, List<ToolbarButtonSetting> buttonSettings, bool forceVisible )
        {
            foreach ( ToolbarButtonSetting bs in buttonSettings )
            {
                if ( bs.ButtonItem == null )
                {
                    // manual fix up for new tool strip item
                    if ( bs.Tag == "CompilingConfigurations" )
                    {
                        bs.ButtonItem = this.compilingConfigurationToolStripComboBox;
                        bs.Visible = true;
                    }
                    else
                    {
                        ApplicationSettings.Log.Error( $"Error with ToolbarButtonSetting {bs.Text}: does not have a valid ButtonItem." );
                        continue;
                    }
                }

                if ( strip.Items.Contains( bs.ButtonItem ) )
                {
                    bs.ButtonItem.Visible = forceVisible ? true : bs.Visible;

                    if ( bs.MenuItem != null )
                    {
                        bs.MenuItem.Checked = forceVisible ? true : bs.Visible;
                    }
                }
            }
        }

        public void ResetToolbarToDefaults( List<ToolbarButtonSetting> buttonSettings, bool forceVisible )
        {
            DeleteCustomButtons();
            AddCustomButtonsFromToolbarButtonSettings( buttonSettings );
            LinkButtonSettingsToToolStripItems( buttonSettings );
            ResetToolbarVisiblity( buttonSettings, forceVisible );
        }

        public void SetToolStripLocations( ToolStripContainer toolStripContainer, List<ToolStripLocation> locations )
        {
            Dictionary<ToolStrip, Point> locationDictionary = new Dictionary<ToolStrip, Point>();
            SortedList<ToolStrip, ToolStrip> topPanelToolStrips = new SortedList<ToolStrip, ToolStrip>( new ToolStripHorizontalSortComparer() );
            SortedList<ToolStrip, ToolStrip> leftPanelToolStrips = new SortedList<ToolStrip, ToolStrip>( new ToolStripVerticalSortComparer() );
            SortedList<ToolStrip, ToolStrip> rightPanelToolStrips = new SortedList<ToolStrip, ToolStrip>( new ToolStripVerticalSortComparer() );
            SortedList<ToolStrip, ToolStrip> bottomPanelToolStrips = new SortedList<ToolStrip, ToolStrip>( new ToolStripHorizontalSortComparer() );
            List<ToolStrip> unknownStrips = new List<ToolStrip>();

            // find the location and add to the appropriate panel list
            List<ToolStrip> strips = this.ToolStrips;
            foreach ( ToolStrip strip in strips )
            {
                bool found = false;
                foreach ( ToolStripLocation loc in locations )
                {
                    if ( strip.Name == loc.Name )
                    {
                        found = true;

                        strip.Location = loc.Location;
                        locationDictionary.Add( strip, loc.Location );

                        switch ( loc.ToolStripPanel )
                        {
                            case "top":
                                topPanelToolStrips.Add( strip, strip );
                                break;
                            case "left":
                                leftPanelToolStrips.Add( strip, strip );
                                break;
                            case "right":
                                rightPanelToolStrips.Add( strip, strip );
                                break;
                            case "bottom":
                                bottomPanelToolStrips.Add( strip, strip );
                                break;
                            default:
                                found = false;
                                break;
                        }

                        break;
                    }
                }

                if ( !found )
                {
                    unknownStrips.Add( strip );
                }
            }

            DetermineToolStripBounds( toolStripContainer.TopToolStripPanel, topPanelToolStrips, locationDictionary );
            DetermineToolStripBounds( toolStripContainer.LeftToolStripPanel, leftPanelToolStrips, locationDictionary );
            DetermineToolStripBounds( toolStripContainer.RightToolStripPanel, rightPanelToolStrips, locationDictionary );
            DetermineToolStripBounds( toolStripContainer.BottomToolStripPanel, bottomPanelToolStrips, locationDictionary );

            // place unknown strips in the top panel at the end
            if ( unknownStrips.Count > 0 )
            {
                Point lastP;
                int lastWidth;
                if ( topPanelToolStrips.Count > 0 )
                {
                    ToolStrip lastStrip = topPanelToolStrips.Values[topPanelToolStrips.Count - 1];
                    lastP = lastStrip.Location;
                    lastWidth = lastStrip.Width;
                }
                else
                {
                    lastP = new Point();
                    lastP.Y = 24;   // try to place it on the 2nd row
                    lastWidth = 0;
                }

                foreach ( ToolStrip strip in unknownStrips )
                {
                    Point p = new Point( lastP.X + lastWidth + c_toolStripSpacing, lastP.Y );
                    DetermineToolStripBounds( toolStripContainer.TopToolStripPanel, strip, p );

                    topPanelToolStrips.Add( strip, strip );

                    lastP = p;
                    lastWidth = strip.Width + c_toolStripSpacing;
                }
            }

            // now add each ToolStrip in turn
            AddToolStripsToPanel( toolStripContainer.TopToolStripPanel, topPanelToolStrips );
            AddToolStripsToPanel( toolStripContainer.LeftToolStripPanel, leftPanelToolStrips );
            AddToolStripsToPanel( toolStripContainer.RightToolStripPanel, rightPanelToolStrips );
            AddToolStripsToPanel( toolStripContainer.BottomToolStripPanel, bottomPanelToolStrips );
        }

        private void DetermineToolStripBounds( ToolStripPanel panel, SortedList<ToolStrip, ToolStrip> strips,
            Dictionary<ToolStrip, Point> locations )
        {
            // Seems the only way to set the bounds of a strip after items have been made invisible
            // is to add it to a panel.
            foreach ( ToolStrip strip in strips.Values )
            {
                DetermineToolStripBounds( panel, strip, locations[strip] );
            }
        }

        private void DetermineToolStripBounds( ToolStripPanel panel, ToolStrip strip, Point location )
        {
            panel.Controls.Add( strip );
            panel.Controls.Remove( strip );
            strip.Location = location;
        }

        private void AddToolStripsToPanel( ToolStripPanel panel, SortedList<ToolStrip, ToolStrip> strips )
        {
            if ( strips.Count == 0 )
            {
                return;
            }

            // first, fixup any overlaps
            for ( int i = 1; i < strips.Count; ++i )
            {
                ToolStrip strip1 = strips.Values[i - 1];
                ToolStrip strip2 = strips.Values[i];

                if ( strip2.Bounds.IntersectsWith( strip1.Bounds ) )
                {
                    int xDiff = 0;
                    if ( panel.Orientation == Orientation.Horizontal )
                    {
                        xDiff = strip1.Bounds.Right - strip2.Bounds.X + c_toolStripSpacing;
                    }

                    int yDiff = 0;
                    if ( panel.Orientation == Orientation.Vertical )
                    {
                        yDiff = strip1.Bounds.Bottom - strip2.Bounds.Y + c_toolStripSpacing;
                    }

                    // now push everything out
                    for ( int j = i; j < strips.Count; ++j )
                    {
                        ToolStrip strip3 = strips.Values[j];
                        strip3.Bounds = new Rectangle( strip3.Bounds.X + xDiff, strip3.Bounds.Y + yDiff,
                            strip3.Bounds.Width, strip3.Bounds.Height );
                    }
                }
            }

            int tabIndex = 0;

            // now add to the panel
            panel.SuspendLayout();
            foreach ( ToolStrip strip in strips.Values )
            {
                strip.TabIndex = tabIndex++;
                panel.Controls.Add( strip );
            }
            panel.ResumeLayout( false );
        }

        public List<ToolStripLocation> GetToolStripLocations()
        {
            List<ToolStripLocation> locations = new List<ToolStripLocation>();

            ToolStripLocation editLoc = new ToolStripLocation();
            editLoc.Name = this.editToolStrip.Name;
            editLoc.Location = this.editToolStrip.Location;
            if ( this.editToolStrip.Parent != null )
            {
                editLoc.ToolStripPanel = this.editToolStrip.Parent.Tag as string;
            }
            locations.Add( editLoc );

            ToolStripLocation codeLoc = new ToolStripLocation();
            codeLoc.Name = this.codeToolStrip.Name;
            codeLoc.Location = this.codeToolStrip.Location;
            if ( this.codeToolStrip.Parent != null )
            {
                codeLoc.ToolStripPanel = this.codeToolStrip.Parent.Tag as string;
            }
            locations.Add( codeLoc );

            ToolStripLocation macrosLoc = new ToolStripLocation();
            macrosLoc.Name = this.macroToolStrip.Name;
            macrosLoc.Location = this.macroToolStrip.Location;
            if ( this.macroToolStrip.Parent != null )
            {
                macrosLoc.ToolStripPanel = this.macroToolStrip.Parent.Tag as string;
            }
            locations.Add( macrosLoc );

            ToolStripLocation customLoc = new ToolStripLocation();
            customLoc.Name = this.customToolStrip.Name;
            customLoc.Location = this.customToolStrip.Location;
            if ( this.customToolStrip.Parent != null )
            {
                customLoc.ToolStripPanel = this.customToolStrip.Parent.Tag as string;
            }
            locations.Add( customLoc );

            ToolStripLocation debuggerLoc = new ToolStripLocation();
            debuggerLoc.Name = this.debuggerToolStrip.Name;
            debuggerLoc.Location = this.debuggerToolStrip.Location;
            if ( this.debuggerToolStrip.Parent != null )
            {
                debuggerLoc.ToolStripPanel = this.debuggerToolStrip.Parent.Tag as string;
            }
            locations.Add(debuggerLoc);

            ToolStripLocation incredibuildLoc = new ToolStripLocation();
            incredibuildLoc.Name = this.incredibuildToolStrip.Name;
            incredibuildLoc.Location = this.incredibuildToolStrip.Location;
            if (this.incredibuildToolStrip.Parent != null)
            {
                incredibuildLoc.ToolStripPanel = this.incredibuildToolStrip.Parent.Tag as string;
            }
            locations.Add(incredibuildLoc);

            return locations;
        }

        public void AddMouseUp( MouseEventHandler eh )
        {
            List<ToolStrip> strips = this.ToolStrips;
            foreach ( ToolStrip strip in strips )
            {
                strip.MouseUp += eh;
            }
        }

        public void AddMouseDown( MouseEventHandler eh )
        {
            List<ToolStrip> strips = this.ToolStrips;
            foreach ( ToolStrip strip in strips )
            {
                strip.MouseDown += eh;
            }
        }

        public void SetCompilingConfigurations( List<string> configurationNames, int selectedIndex )
        {
            this.compilingConfigurationToolStripComboBox.Items.Clear();
            foreach ( string configurationName in configurationNames )
            {
                this.compilingConfigurationToolStripComboBox.Items.Add( configurationName );
            }

            if ( (selectedIndex >= -1) && (selectedIndex < configurationNames.Count) )
            {
                this.compilingConfigurationToolStripComboBox.SelectedIndex = selectedIndex;
            }
        }

        public void SetPlatforms(IList<Platform> platforms, Platform selectedPlatform)
        {
            platformToolStripComboBox.Items.Clear();
            foreach (Platform platform in platforms)
            {
                platformToolStripComboBox.Items.Add(platform.PlatformToFriendlyName());
            }

            if (selectedPlatform != Platform.Independent)
            {
                platformToolStripComboBox.SelectedIndex = platforms.IndexOf(selectedPlatform);
            }

            if (platformToolStripComboBox.SelectedIndex == -1 &&
                platformToolStripComboBox.Items.Count > 0)
            {
                platformToolStripComboBox.SelectedIndex = 0;
            }
        }
        #endregion
    }

    #region ToolStrips IComparer classes
    internal class ToolStripHorizontalSortComparer : IComparer<ToolStrip>
    {
        public int Compare( ToolStrip x, ToolStrip y )
        {
            if ( x.Location.Y < y.Location.Y )
            {
                return -1;
            }
            else if ( x.Location.Y > y.Location.Y )
            {
                return 1;
            }
            else
            {
                if ( x.Location.X < y.Location.X )
                {
                    return -1;
                }
                else if ( x.Location.X > y.Location.X )
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }

    internal class ToolStripVerticalSortComparer : IComparer<ToolStrip>
    {
        public int Compare( ToolStrip x, ToolStrip y )
        {
            if ( x.Location.X < y.Location.X )
            {
                return -1;
            }
            else if ( x.Location.X > y.Location.X )
            {
                return 1;
            }
            else
            {
                if ( x.Location.Y < y.Location.Y )
                {
                    return -1;
                }
                else if ( x.Location.Y > y.Location.Y )
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
    #endregion

    #region EventArg classes
    public class DebuggerOptionsEventArgs : EventArgs
    {
        public DebuggerOptionsEventArgs( ToolStripDropDownButton dropDownButton )
        {
            m_dropDownButton = dropDownButton;
        }

        #region Variables
        private ToolStripDropDownButton m_dropDownButton;
        #endregion

        #region Properties
        public ToolStripDropDownButton DropDownButton
        {
            get
            {
                return m_dropDownButton;
            }
        }
        #endregion
    }

    public class CompilingConfigurationChangedEventArgs : EventArgs
    {
        public CompilingConfigurationChangedEventArgs( string configurationName )
        {
            m_configurationName = configurationName;
        }

        #region Variables
        private string m_configurationName;
        #endregion

        #region Properties
        public string ConfigurationName
        {
            get
            {
                return m_configurationName;
            }
        }
        #endregion
    };

    public class PlatformChangedEventArgs : EventArgs
    {
        public PlatformChangedEventArgs(Platform platform)
        {
            _platform = platform;
        }

        #region Variables
        private Platform _platform;
        #endregion

        #region Properties
        public Platform Platform
        {
            get
            {
                return _platform;
            }
        }
        #endregion
    }

    public class ShowLineNumbersChangedEventArgs : EventArgs
    {
        public ShowLineNumbersChangedEventArgs( bool showLineNumbers )
        {
            m_showLineNumbers = showLineNumbers;
        }

        #region Variables
        private bool m_showLineNumbers;
        #endregion

        #region Properties
        public bool ShowLineNumbers
        {
            get
            {
                return m_showLineNumbers;
            }
        }
        #endregion
    };

    public class HighlightEnumsChangedEventArgs : EventArgs
    {
        public HighlightEnumsChangedEventArgs( bool highlightEnums )
        {
            m_highlightEnums = highlightEnums;
        }

        #region Variables
        private bool m_highlightEnums;
        #endregion

        #region Properties
        public bool HighlightEnums
        {
            get
            {
                return m_highlightEnums;
            }
        }
        #endregion
    };

    public class HighlightQuickInfoPopupsChangedEventArgs : EventArgs
    {
        public HighlightQuickInfoPopupsChangedEventArgs( bool highlightQuickInfoPopups )
        {
            m_highlightQuickInfoPopups = highlightQuickInfoPopups;
        }

        #region Variables
        private bool m_highlightQuickInfoPopups;
        #endregion

        #region Properties
        public bool HighlightQuickInfoPopups
        {
            get
            {
                return m_highlightQuickInfoPopups;
            }
        }
        #endregion
    };

    public class HighlightDisabledCodeBlocksChangedEventArgs : EventArgs
    {
        public HighlightDisabledCodeBlocksChangedEventArgs( bool highlightDisabledCodeBlocks )
        {
            m_highlightDisabledCodeBlocks = highlightDisabledCodeBlocks;
        }

        #region Variables
        private bool m_highlightDisabledCodeBlocks;
        #endregion

        #region Properties
        public bool HighlightDisabledCodeBlocks
        {
            get
            {
                return m_highlightDisabledCodeBlocks;
            }
        }
        #endregion
    };
    #endregion
}
