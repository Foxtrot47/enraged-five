using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ragScriptEditorShared
{
    public partial class FileBrowserEditControl : UserControl
    {
        protected FileBrowserEditControl()
        {
            InitializeComponent();
        }

        public FileBrowserEditControl( string title, string filter )
            : this()
        {
            this.openFileDialog1.Title = title;
            this.openFileDialog1.Filter = filter;
        }

        #region Properties
        public string Filename
        {
            get
            {
                return this.openFileDialog1.FileName;
            }
        }
        #endregion

        #region Event Handlers
        private void FileBrowserEditControl_Load( object sender, EventArgs e )
        {
            this.openFileDialog1.ShowDialog( this );
        }
        #endregion
    }
}
