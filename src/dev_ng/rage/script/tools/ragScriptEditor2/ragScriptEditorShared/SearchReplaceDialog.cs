using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ActiproSoftware.SyntaxEditor;
using RSG.Base.Forms;

namespace ragScriptEditorShared
{
    public partial class SearchReplaceDialog : Form
    {
        public SearchReplaceDialog()
        {
            InitializeComponent();

            this.useComboBox.SelectedIndex = 0;
        }

        public void Init( Control parent, FindReplaceOptions options )
        {
            m_parentControl = parent;
            m_findReplaceOptions = options;

            SetUIFromFindReplaceOptions();

            this.searchReplaceComponent.Init( m_parentControl );
        }

        #region Variables
        private bool m_locationSet = false;
        private Control m_parentControl = null;
        private FindReplaceOptions m_findReplaceOptions = null;

        private FindReplaceSearchRange m_lastSearchIn = FindReplaceSearchRange.CurrentDocument;
        private bool m_enableSearching = true;
        private bool m_canReplace = true;
        #endregion

        #region Events
        public event EventHandler CancelIntellisense;
        public event FindCurrentFilenameEventHandler FindCurrentFilename;
        public event FindOpenFilenamesEventHandler FindOpenFilenames;
        public event FindProjectFilenamesEventHandler FindProjectFilenames;

        public event FoundStringEventHandler FoundString
        {
            add
            {
                this.searchReplaceComponent.FoundString += value;
            }
            remove
            {
                this.searchReplaceComponent.FoundString -= value;
            }
        }

        public event LoadFileEventHandler LoadFile
        {
            add
            {
                this.searchReplaceComponent.LoadFile += value;
            }
            remove
            {
                this.searchReplaceComponent.LoadFile -= value;
            }
        }

        public event ReportProgressEventHandler ReportProgress
        {
            add
            {
                this.searchReplaceComponent.ReportProgress += value;
            }
            remove
            {
                this.searchReplaceComponent.ReportProgress -= value;
            }
        }

        public event SaveDocumentEventHandler SaveDocument
        {
            add
            {
                this.searchReplaceComponent.SaveDocument += value;
            }
            remove
            {
                this.searchReplaceComponent.SaveDocument -= value;
            }
        }

        public event SearchBeginEventHandler SearchBegin
        {
            add
            {
                this.searchReplaceComponent.SearchBegin += value;
            }
            remove
            {
                this.searchReplaceComponent.SearchBegin -= value;
            }
        }

        public event SearchCompleteEventHandler SearchComplete
        {
            add
            {
                this.searchReplaceComponent.SearchComplete += value;
            }
            remove
            {
                this.searchReplaceComponent.SearchComplete -= value;
            }
        }

        public event SyntaxEditorEventHandler FindSyntaxEditor
        {
            add
            {
                this.searchReplaceComponent.FindSyntaxEditor += value;
            }
            remove
            {
                this.searchReplaceComponent.FindSyntaxEditor -= value;
            }
        }
        #endregion

        #region Event Dispatchers
        protected void OnCancelIntellisense()
        {
            if ( this.CancelIntellisense != null )
            {
                if ( m_parentControl != null )
                {
                    m_parentControl.Invoke( this.CancelIntellisense, new object[] { this, EventArgs.Empty } );
                }
                else
                {
                    this.CancelIntellisense( this, EventArgs.Empty );
                }
            }
        }

        protected string OnFindCurrentFilename()
        {
            if ( this.FindCurrentFilename != null )
            {
                SearchReplaceFilenameEventArgs e = new SearchReplaceFilenameEventArgs();
                if ( m_parentControl != null )
                {
                    m_parentControl.Invoke( this.FindCurrentFilename, new object[] { this, e } );
                }
                else
                {
                    this.FindCurrentFilename( this, e );
                }

                return e.Filename;
            }

            return null;
        }

        protected List<string> OnFindOpenFilenames()
        {
            if ( this.FindOpenFilenames != null )
            {
                SearchReplaceFilenamesEventArgs e = new SearchReplaceFilenamesEventArgs();
                if ( m_parentControl != null )
                {
                    m_parentControl.Invoke( this.FindOpenFilenames, new object[] { this, e } );
                }
                else
                {
                    this.FindOpenFilenames( this, e );
                }

                return e.Filenames;
            }

            return null;
        }

        protected List<string> OnFindProjectFilenames()
        {
            if ( this.FindProjectFilenames != null )
            {
                SearchReplaceFilenamesEventArgs e = new SearchReplaceFilenamesEventArgs();
                if ( m_parentControl != null )
                {
                    m_parentControl.Invoke( this.FindProjectFilenames, new object[] { this, e } );
                }
                else
                {
                    this.FindProjectFilenames( this, e );
                }

                return e.Filenames;
            }

            return null;
        }
        #endregion

        #region Properties
        public Control ParentControl
        {
            get
            {
                return m_parentControl;
            }
        }

        public bool CanReplace
        {
            get
            {
                return m_canReplace;
            }
            set
            {
                m_canReplace = value;
            }
        }

        public bool LocationSet
        {
            get
            {
                return m_locationSet;
            }
            set

            {
                m_locationSet = value;
            }
        }

        public string FindWhat
        {
            get
            {
                return this.findWhatComboBox.Text;
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ) )
                {
                    int indexOf = this.findWhatComboBox.Items.IndexOf( value );
                    if ( indexOf != -1 )
                    {
                        this.findWhatComboBox.SelectedIndex = indexOf;
                    }
                    else
                    {
                        this.findWhatComboBox.SelectedIndex = -1;
                        this.findWhatComboBox.Text = value;
                    }
                }
                else
                {
                    this.findWhatComboBox.SelectedIndex = -1;
                    this.findWhatComboBox.Text = string.Empty;
                }
            }
        }

        public List<string> FindHistory
        {
            get
            {
                List<string> findHistory = new List<string>();
                foreach ( object item in this.findWhatComboBox.Items )
                {
                    findHistory.Add( item.ToString() );
                }

                return findHistory;
            }
            set
            {
                this.findWhatComboBox.Items.Clear();
                if ( value != null )
                {
                    foreach ( string item in value )
                    {
                        this.findWhatComboBox.Items.Add( item );
                    }
                }
            }
        }

        public string ReplaceWith
        {
            get
            {
                return this.replaceWithComboBox.Text;
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ) )
                {
                    int indexOf = this.replaceWithComboBox.Items.IndexOf( value );
                    if ( indexOf != -1 )
                    {
                        this.replaceWithComboBox.SelectedIndex = indexOf;
                    }
                    else
                    {
                        this.replaceWithComboBox.SelectedIndex = -1;
                        this.replaceWithComboBox.Text = value;
                    }
                }
                else
                {
                    this.replaceWithComboBox.SelectedIndex = -1;
                    this.replaceWithComboBox.Text = string.Empty;
                }
            }
        }

        public List<string> ReplaceHistory
        {
            get
            {
                List<string> replaceHistory = new List<string>();
                foreach ( object item in this.replaceWithComboBox.Items )
                {
                    replaceHistory.Add( item.ToString() );
                }

                return replaceHistory;
            }
            set
            {
                this.replaceWithComboBox.Items.Clear();
                if ( value != null )
                {
                    foreach ( string item in value )
                    {
                        this.replaceWithComboBox.Items.Add( item );
                    }
                }
            }
        }

        public bool MatchCase
        {
            get
            {
                return this.matchCaseCheckBox.Checked;
            }
            set
            {
                this.matchCaseCheckBox.Checked = value;
            }
        }

        public bool MatchWholeWord
        {
            get
            {
                return this.matchWholeWordCheckBox.Checked;
            }
            set
            {
                this.matchWholeWordCheckBox.Checked = value;
            }
        }

        public bool SearchUp
        {
            get
            {
                return this.searchUpCheckBox.Checked;
            }
            set
            {
                this.searchUpCheckBox.Checked = value;
            }
        }

        public bool UseEnabled
        {
            get
            {
                return this.useCheckBox.Checked;
            }
            set
            {
                this.useComboBox.Enabled = this.useCheckBox.Checked = value;
            }
        }

        public FindReplaceSearchType SearchType
        {
            get
            {
                if ( this.useCheckBox.Checked )
                {
                    if ( this.useComboBox.SelectedIndex == 0 )
                    {
                        return FindReplaceSearchType.RegularExpression;
                    }
                    else
                    {
                        return FindReplaceSearchType.Wildcard;
                    }
                }
                else
                {
                    return FindReplaceSearchType.Normal;
                }
            }
            set
            {
                switch ( value )
                {
                    case FindReplaceSearchType.Normal:
                        {
                            this.UseEnabled = false;
                        }
                        break;
                    case FindReplaceSearchType.RegularExpression:
                        {
                            this.UseEnabled = true;
                            this.useComboBox.SelectedIndex = 0;
                        }
                        break;
                    case FindReplaceSearchType.Wildcard:
                        {
                            this.UseEnabled = true;
                            this.useComboBox.SelectedIndex = 1;
                        }
                        break;
                }
            }
        }

        public bool MarkWithBookmarks
        {
            get
            {
                return this.markWithBookmarksCheckBox.Checked;
            }
            set
            {
                this.markWithBookmarksCheckBox.Checked = value;
            }
        }

        public FindReplaceSearchRange SearchIn
        {
            get
            {
                if ( this.currentSelectionRadioButton.Checked )
                {
                    return FindReplaceSearchRange.CurrentSelection;
                }
                else if ( this.allOpenDocumentsRadioButton.Checked )
                {
                    return FindReplaceSearchRange.AllOpenDocuments;
                }
                else if ( this.entireProjectRadioButton.Checked )
                {
                    return FindReplaceSearchRange.EntireProject;
                }
                else
                {
                    return FindReplaceSearchRange.CurrentDocument;
                }
            }
            set
            {
                switch ( value )
                {
                    case FindReplaceSearchRange.AllOpenDocuments:
                        this.allOpenDocumentsRadioButton.Checked = true;
                        break;
                    case FindReplaceSearchRange.CurrentDocument:
                        this.currentDocumentRadioButton.Checked = true;
                        break;
                    case FindReplaceSearchRange.CurrentSelection:
                        this.currentSelectionRadioButton.Checked = true;
                        break;
                    case FindReplaceSearchRange.EntireProject:
                        this.entireProjectRadioButton.Checked = true;
                        break;
                }
            }
        }
        #endregion

        #region Event Handlers
        private void SearchReplaceDialog_Activated( object sender, EventArgs e )
        {
            CancelActiveIntellisense();

            string filename = OnFindCurrentFilename();
            List<string> openFilenames = OnFindOpenFilenames();
            List<string> projectFilenames = OnFindProjectFilenames();

            this.currentSelectionRadioButton.Enabled = !String.IsNullOrEmpty(filename);
            this.currentDocumentRadioButton.Enabled = !String.IsNullOrEmpty(filename);
            this.allOpenDocumentsRadioButton.Enabled = (openFilenames != null) && (openFilenames.Count > 0);
            this.entireProjectRadioButton.Enabled = (projectFilenames != null) && (projectFilenames.Count > 0);

            if ( this.searchGroupBox.Enabled && !this.currentSelectionRadioButton.Enabled && !this.currentDocumentRadioButton.Enabled
                && !this.allOpenDocumentsRadioButton.Enabled && !this.entireProjectRadioButton.Enabled )
            {
                m_enableSearching = false;
            }
            else
            {
                m_enableSearching = true;

                if ( this.entireProjectRadioButton.Checked && !this.entireProjectRadioButton.Enabled )
                {
                    if ( this.currentDocumentRadioButton.Enabled )
                    {
                        this.currentDocumentRadioButton.Checked = true;
                    }
                    else if ( this.allOpenDocumentsRadioButton.Enabled )
                    {
                        this.allOpenDocumentsRadioButton.Checked = true;
                    }
                }
                else if ( this.allOpenDocumentsRadioButton.Checked && !this.allOpenDocumentsRadioButton.Enabled )
                {
                    if ( this.currentDocumentRadioButton.Enabled )
                    {
                        this.currentDocumentRadioButton.Checked = true;
                    }
                    else if ( this.entireProjectRadioButton.Enabled )
                    {
                        this.entireProjectRadioButton.Checked = true;
                    }
                }
                else if ( (this.currentSelectionRadioButton.Checked && !this.currentSelectionRadioButton.Enabled)
                    || (this.currentDocumentRadioButton.Checked && !this.currentDocumentRadioButton.Enabled) )
                {
                    if ( this.allOpenDocumentsRadioButton.Enabled )
                    {
                        this.allOpenDocumentsRadioButton.Checked = true;
                    }
                    else if ( this.entireProjectRadioButton.Enabled )
                    {
                        this.entireProjectRadioButton.Checked = true;
                    }
                }
            }

            SelectNextControl( this.findNextButton, false, true, true, true );
        }

        private void SearchReplaceDialog_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( e.CloseReason == CloseReason.UserClosing )
            {
                e.Cancel = true;
                this.Hide();

                string filename = OnFindCurrentFilename();
                if ( !String.IsNullOrEmpty( filename ) )
                {
                    SyntaxEditor editor = this.searchReplaceComponent.GetSyntaxEditor( filename );
                    if ( editor != null )
                    {
                        editor.Focus();
                    }
                }
            }
        }

        private void searchReplaceComponent_SearchComplete( object sender, SearchCompleteEventArgs e )
        {
            switch ( e.Search )
            {
                case SearchReplaceType.FindInFile:
                case SearchReplaceType.ReplaceInFile:
                case SearchReplaceType.FindInFiles:
                case SearchReplaceType.ReplaceInFiles:
                    {
                        switch ( e.Result )
                        {
                            case SearchReplaceResult.Error:
                                {
                                    rageMessageBox.ShowError( this, e.ResultMessage, e.ResultTitle );
                                }
                                break;
                            case SearchReplaceResult.NotFound:
                            case SearchReplaceResult.PastEndOfFile:
                            case SearchReplaceResult.PastStartOfSearch:
                                {
                                    rageMessageBox.ShowInformation( this, e.ResultMessage, e.ResultTitle );
                                }
                                break;
                            default:
                                break;
                        }

                        if ( (e.Search == SearchReplaceType.ReplaceInFile) || (e.Search == SearchReplaceType.ReplaceInFiles) )
                        {
                            StoreReplaceText();
                        }
                    }
                    break;
                case SearchReplaceType.ReplaceAllInFile:
                case SearchReplaceType.MarkAllInFile:
                    {
                        switch ( e.Result )
                        {
                            case SearchReplaceResult.Found:
                                {
                                    rageMessageBox.ShowInformation( this, String.Format( "Total {0}: {1}",
                                        (e.Search == SearchReplaceType.MarkAllInFile) ? "marked" : "replaced", e.NumFound ),
                                        e.ResultTitle );
                                }
                                break;
                            case SearchReplaceResult.Error:
                                {
                                    rageMessageBox.ShowError( this, e.ResultMessage, e.ResultTitle );
                                }
                                break;
                            default:
                                {
                                    rageMessageBox.ShowInformation( this, e.ResultMessage, e.ResultTitle );
                                }
                                break;

                        }

                        if ( e.Search == SearchReplaceType.ReplaceAllInFile )
                        {
                            StoreReplaceText();
                        }
                    }
                    break;                
                case SearchReplaceType.ReplaceAllInFiles:
                case SearchReplaceType.MarkAllInFiles:
                    {
                        switch ( e.Result )
                        {
                            case SearchReplaceResult.Found:
                                {
                                    rageMessageBox.ShowInformation( this, 
                                        String.Format( "Total {0}: {1}\nMatching files: {2}\nTotal files searched: {3}",
                                        (e.Search == SearchReplaceType.MarkAllInFiles) ? "marked" : "replaced",
                                        e.NumFound, e.NumFilesFound, e.NumFilesSearched ), e.ResultTitle );
                                }
                                break;
                            case SearchReplaceResult.Error:
                                {
                                    rageMessageBox.ShowError( this, e.ResultMessage, e.ResultTitle );
                                }
                                break;
                            default:
                                {
                                    rageMessageBox.ShowInformation( this, e.ResultMessage, e.ResultTitle );
                                }
                                break;

                        }

                        if ( e.Search == SearchReplaceType.ReplaceAllInFiles )
                        {
                            StoreReplaceText();
                        }
                    }
                    break;
                case SearchReplaceType.FindAllInFile:
                case SearchReplaceType.FindAllInFiles:
                    {
                        // not supported on this dialog
                    }
                    break;
            }

            StoreFindText();

            EnableButtons();

            SelectNextControl( this.findNextButton, false, true, true, true );

            if ( this.Visible )
            {
                this.Focus();
            }
        }

        private void findWhatComboBox_TextUpdate( object sender, EventArgs e )
        {
            EnableButtons();
        }

        private void useCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.useComboBox.Enabled = this.useCheckBox.Checked;
        }

        private void currentSelectionRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            this.markAllButton.Enabled = this.findNextButton.Enabled;
        }

        private void currentDocumentRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            this.markAllButton.Enabled = this.findNextButton.Enabled;
            m_lastSearchIn = FindReplaceSearchRange.CurrentDocument;
        }

        private void allOpenDocumentsRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            this.markAllButton.Enabled = this.findNextButton.Enabled;
            m_lastSearchIn = FindReplaceSearchRange.AllOpenDocuments;
        }

        private void entireProjectRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            this.markAllButton.Enabled = false;
            m_lastSearchIn = FindReplaceSearchRange.EntireProject;
        }

        private void findNextButton_Click( object sender, EventArgs e )
        {
            List<string> filenames;
            if ( !PrepareSearch( out filenames ) )
            {
                return;
            }

            DisableButtons();

            if ( (this.SearchIn == FindReplaceSearchRange.CurrentSelection) || (this.SearchIn == FindReplaceSearchRange.CurrentDocument) )
            {
                this.searchReplaceComponent.FindNextInFile( filenames[0], m_findReplaceOptions, true );
            }
            else
            {
                this.searchReplaceComponent.FindNextInFiles( filenames, OnFindCurrentFilename(), m_findReplaceOptions, true );
            }
        }

        private void replaceButton_Click( object sender, EventArgs e )
        {
            List<string> filenames;
            if ( !PrepareSearch( out filenames ) )
            {
                return;
            }

            DisableButtons();

            if ( (this.SearchIn == FindReplaceSearchRange.CurrentSelection) || (this.SearchIn == FindReplaceSearchRange.CurrentDocument) )
            {
                this.searchReplaceComponent.ReplaceNextInFile( filenames[0], m_findReplaceOptions, true );
            }
            else
            {
                this.searchReplaceComponent.ReplaceNextInFiles( filenames, OnFindCurrentFilename(), m_findReplaceOptions, true );
            }
        }

        private void replaceAllButton_Click( object sender, EventArgs e )
        {
            List<string> filenames;
            if ( !PrepareSearch( out filenames ) )
            {
                return;
            }
            
            DisableButtons();

            if ( (this.SearchIn == FindReplaceSearchRange.CurrentSelection) || (this.SearchIn == FindReplaceSearchRange.CurrentDocument) )
            {
                this.searchReplaceComponent.ReplaceAllInFile( filenames[0], m_findReplaceOptions, true );
            }
            else
            {
                this.searchReplaceComponent.ReplaceAllInFiles( filenames, m_findReplaceOptions, true );
            }
        }

        private void markAllButton_Click( object sender, EventArgs e )
        {
            List<string> filenames;
            if ( !PrepareSearch( out filenames ) )
            {
                return;
            }

            DisableButtons();

            if ( (this.SearchIn == FindReplaceSearchRange.CurrentSelection) || (this.SearchIn == FindReplaceSearchRange.CurrentDocument) )
            {
                this.searchReplaceComponent.MarkAllInFile( filenames[0], m_findReplaceOptions, true, this.MarkWithBookmarks );
            }
            else
            {
                this.searchReplaceComponent.MarkAllInFiles( filenames, m_findReplaceOptions, true, this.MarkWithBookmarks );
            }
        }

        private void closeButton_Click( object sender, EventArgs e )
        {
            this.Close();
        }
        #endregion

        #region Public Functions
        public void CancelActiveIntellisense()
        {
            OnCancelIntellisense();
        }

        public void CancelSearchReplace()
        {
            this.searchReplaceComponent.CancelSearchReplace();
        }

        public void SetUIFromSettings( FindReplaceSettings settings )
        {
            this.MatchCase = settings.MatchCase;
            this.MatchWholeWord = settings.MatchWholeWord;
            this.SearchUp = settings.SearchUp;

            if ( settings.UseRegularExpressions )
            {
                this.SearchType = FindReplaceSearchType.RegularExpression;
            }
            else if ( settings.UseWildcards )
            {
                this.SearchType = FindReplaceSearchType.Wildcard;
            }
            else
            {
                this.SearchType = FindReplaceSearchType.Normal;
            }

            this.MarkWithBookmarks = settings.MarkWithBookmarks;
            this.SearchIn = settings.SearchIn;

            this.FindHistory = settings.FindHistory;
            this.ReplaceHistory = settings.ReplaceHistory;
        }

        public void SetSettingsFromUI( FindReplaceSettings settings )
        {
            settings.MatchCase = this.MatchCase;
            settings.MatchWholeWord = this.MatchWholeWord;
            settings.SearchUp = this.SearchUp;
            
            switch ( this.SearchType )
            {
                case FindReplaceSearchType.Normal:
                    {
                        settings.UseRegularExpressions = false;
                        settings.UseWildcards = false;
                    }
                    break;
                case FindReplaceSearchType.RegularExpression:
                    {
                        settings.UseRegularExpressions = true;
                        settings.UseWildcards = false;
                    }
                    break;
                case FindReplaceSearchType.Wildcard:
                    {
                        settings.UseRegularExpressions = false;
                        settings.UseWildcards = true;
                    }
                    break;
            }

            settings.MarkWithBookmarks = this.MarkWithBookmarks;
            settings.SearchIn = this.SearchIn;

            settings.FindHistory = this.FindHistory;
            settings.ReplaceHistory = this.ReplaceHistory;
        }

        public bool InitSearchText( SyntaxEditor editor )
        {
            if ( this.searchReplaceComponent.IsBusy() )
            {
                // don't initialize text if we are in the middle of a search
                return false;
            }

            if ( editor == null )
            {
                // we need a syntax editor to set the search text
                return false;
            }

            string searchText = this.searchReplaceComponent.DetermineSearchText( editor.Document.Filename,
                editor.SelectedView.Selection.StartOffset );

            if ( String.IsNullOrEmpty( searchText ) )
            {
                return false;
            }

            int indexOf = this.findWhatComboBox.Items.IndexOf( searchText );
            if ( indexOf != -1 )
            {
                this.findWhatComboBox.SelectedIndex = indexOf;
            }
            else
            {
                this.findWhatComboBox.SelectedIndex = -1;
                this.findWhatComboBox.Text = searchText;
            }

            this.findWhatComboBox.SelectAll();

            m_findReplaceOptions.FindText = searchText;

            // make sure our buttons get enabled appropriately
            EnableButtons();

            return true;
        }

        public void SearchAgainDown( SyntaxEditor editor )
        {
            if ( String.IsNullOrEmpty( m_findReplaceOptions.FindText ))
            {
                return;
            }

            bool oldSearchUp = m_findReplaceOptions.SearchUp;
            this.searchUpCheckBox.Checked = false;
            this.findNextButton_Click( editor, new System.EventArgs() );
            this.searchUpCheckBox.Checked = oldSearchUp;

            // make sure this form doesn't steal focus
            editor.Focus();
        }

        public void SearchAgainUp( SyntaxEditor editor )
        {
            if (String.IsNullOrEmpty(m_findReplaceOptions.FindText))
            {
                return;
            }

            bool oldSearchUp = m_findReplaceOptions.SearchUp;
            this.searchUpCheckBox.Checked = true;
            this.findNextButton_Click( editor, new System.EventArgs() );
            this.searchUpCheckBox.Checked = oldSearchUp;

            // make sure this form doesn't steal focus
            editor.Focus();
        }

        public void SearchHighlightedDown( SyntaxEditor editor )
        {
            if ( InitSearchText( editor ) )
            {
                SearchAgainDown( editor );
            }            
        }

        public void SearchHighlightedUp( SyntaxEditor editor )
        {
            if ( InitSearchText( editor ) )
            {
                SearchAgainUp( editor );
            }

        }
        #endregion

        #region Private Functions
        private void StoreFindText()
        {
            if ( this.findWhatComboBox.Items.Count == 20 )
            {
                this.findWhatComboBox.Items.RemoveAt( 19 );
            }

            string text = this.findWhatComboBox.Text;
            int indexOf = this.findWhatComboBox.Items.IndexOf( this.findWhatComboBox.Text );
            if ( indexOf != -1 )
            {
                this.findWhatComboBox.Items.RemoveAt( indexOf );
            }

            this.findWhatComboBox.Items.Insert( 0, text );
            this.findWhatComboBox.SelectedIndex = 0;
        }

        private void StoreReplaceText()
        {
            if ( this.replaceWithComboBox.Items.Count == 20 )
            {
                this.replaceWithComboBox.Items.RemoveAt( 19 );
            }

            string text = this.replaceWithComboBox.Text;
            int indexOf = this.replaceWithComboBox.Items.IndexOf( this.replaceWithComboBox.Text );
            if ( indexOf != -1 )
            {
                this.replaceWithComboBox.Items.RemoveAt( indexOf );
            }

            this.replaceWithComboBox.Items.Insert( 0, text );
            this.replaceWithComboBox.SelectedIndex = 0;
        }

        private void SetUIFromFindReplaceOptions()
        {
            this.FindWhat = m_findReplaceOptions.FindText;
            this.MatchCase = m_findReplaceOptions.MatchCase;
            this.MatchWholeWord = m_findReplaceOptions.MatchWholeWord;
            this.ReplaceWith = m_findReplaceOptions.ReplaceText;
            
            if ( m_findReplaceOptions.SearchInSelection )
            {
                this.SearchIn = FindReplaceSearchRange.CurrentSelection;
            }
            else
            {
                this.SearchIn = FindReplaceSearchRange.CurrentDocument;
            }

            this.SearchType = m_findReplaceOptions.SearchType;
            this.SearchUp = m_findReplaceOptions.SearchUp;
        }

        private void SetFindReplaceOptionsFromUI()
        {
            m_findReplaceOptions.ChangeSelection = true;
            m_findReplaceOptions.FindText = this.FindWhat;
            m_findReplaceOptions.MatchCase = this.MatchCase;
            m_findReplaceOptions.MatchWholeWord = this.MatchWholeWord;
            m_findReplaceOptions.ReplaceText = this.ReplaceWith;
            m_findReplaceOptions.SearchInSelection = this.SearchIn == FindReplaceSearchRange.CurrentSelection;
            m_findReplaceOptions.SearchType = this.SearchType;
            m_findReplaceOptions.SearchUp = this.SearchUp;
        }

        private bool PrepareSearch( out List<string> filenames )
        {
            filenames = null;
            if (this.searchReplaceComponent.IsBusy())
                return false;

            SetFindReplaceOptionsFromUI();

            DisableButtons();

            switch ( this.SearchIn )
            {
                case FindReplaceSearchRange.CurrentSelection:
                case FindReplaceSearchRange.CurrentDocument:
                    {
                        string filename = OnFindCurrentFilename();
                        if ( !String.IsNullOrEmpty( filename ) )
                        {
                            filenames = new List<string>();
                            filenames.Add( filename );
                        }
                        else
                        {
                            rageMessageBox.ShowError( this, "Could not determine the current filename.", this.searchReplaceComponent.Title );
                            return false;
                        }
                    }
                    break;
                case FindReplaceSearchRange.AllOpenDocuments:
                    {
                        filenames = OnFindOpenFilenames();
                    }
                    break;
                case FindReplaceSearchRange.EntireProject:
                    {
                        filenames = OnFindProjectFilenames();
                    }
                    break;
            }

            return true;
        }

        private void DisableButtons()
        {
            this.findNextButton.Enabled = false;
            this.findWhatComboBox.Enabled = false;
            this.markAllButton.Enabled = false;
            this.markWithBookmarksCheckBox.Enabled = false;
            this.matchCaseCheckBox.Enabled = false;
            this.matchWholeWordCheckBox.Enabled = false;
            this.replaceAllButton.Enabled = false;
            this.replaceButton.Enabled = false;
            this.replaceWithComboBox.Enabled = false;
            this.searchGroupBox.Enabled = false;
            this.searchUpCheckBox.Enabled = false;
            this.useCheckBox.Enabled = false;
            this.useComboBox.Enabled = false;
        }

        private void EnableButtons()
        {
            bool canSearch = m_enableSearching && !String.IsNullOrEmpty( this.FindWhat.Trim() );

            this.findNextButton.Enabled = canSearch;
            this.findWhatComboBox.Enabled = true;
            this.markAllButton.Enabled = this.entireProjectRadioButton.Checked ? true : canSearch;
            this.markWithBookmarksCheckBox.Enabled = true;
            this.matchCaseCheckBox.Enabled = true;
            this.matchWholeWordCheckBox.Enabled = true;
            this.replaceAllButton.Enabled = canSearch && this.CanReplace;
            this.replaceButton.Enabled = canSearch && this.CanReplace;
            this.replaceWithComboBox.Enabled = true;
            this.searchGroupBox.Enabled = true;
            this.searchUpCheckBox.Enabled = true;
            this.useCheckBox.Enabled = true;
            this.useComboBox.Enabled = this.useCheckBox.Checked;
        }
        #endregion
    }
}