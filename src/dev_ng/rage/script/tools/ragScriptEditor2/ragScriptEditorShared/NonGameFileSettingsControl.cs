using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragScriptEditorShared
{
    public partial class NonGameFileSettingsControl : ragScriptEditorShared.FileSettingsControl
    {
        public NonGameFileSettingsControl()
        {
            InitializeComponent();
        }

        #region Overrides
        public override SettingsBase TabSettings
        {
            get
            {
                SettingsBase settings = base.TabSettings;

                NonGameFileSettings fileSettings = new NonGameFileSettings();
                fileSettings.Copy( settings );

                fileSettings.ShowLineNumbers = this.showLineNumbersCheckBox.Checked;
                fileSettings.DocumentOverflowScrollable = this.scrollButtonsRadioButton.Checked;

                return fileSettings;
            }
            set
            {
                base.TabSettings = value;

                if ( value is NonGameFileSettings )
                {
                    m_invokeSettingsChanged = false;

                    NonGameFileSettings fileSettings = value as NonGameFileSettings;

                    this.showLineNumbersCheckBox.Checked = fileSettings.ShowLineNumbers;
                    this.scrollButtonsRadioButton.Checked = fileSettings.DocumentOverflowScrollable;
                    this.dropDownMenuRadioButton.Checked = !fileSettings.DocumentOverflowScrollable;

                    m_invokeSettingsChanged = true;
                }
            }
        }
        #endregion

        #region Event Handlers
        private void showLineNumbersCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            OnSettingsChanged();
        }

        private void dropDownMenuRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            OnSettingsChanged();
        }

        private void scrollButtonsRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            OnSettingsChanged();
        }
        #endregion
    }
}

