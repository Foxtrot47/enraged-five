using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragScriptEditorShared
{
    public partial class GotoLineNumberForm : Form
    {
        public GotoLineNumberForm()
        {
            InitializeComponent();
        }

        public GotoLineNumberForm( string labelName, int currentline, int? maximum )
        {
            InitializeComponent();

            // set the max line number
            if (maximum.HasValue)
            {
                m_maximum = maximum.Value;
                this.lineRangeLabel.Text = $"{labelName} (1 - {m_maximum}):";
            }
            else
            {
                m_maximum = int.MaxValue;
                this.lineRangeLabel.Text = $"{labelName}:";
            }

            this.Text = $"Go to {labelName}";

            // setup the mask
            string maxStr = m_maximum.ToString();
            this.lineNumberMaskedTextBox.Mask = $"0{new string('9', maxStr.Length-1)}";

            // set the text to the current line
            this.lineNumberMaskedTextBox.Text = currentline.ToString();
        }

        /// <summary>
        /// Instantiates GotoLineNumberForm and launches it as a dialog.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="labelName">The type of label to show (Line Number or Program Counter for example)</param>
        /// <param name="currentline"></param>
        /// <param name="maximum">The max number, if one is specified.</param>
        /// <returns>0 on cancelled</returns>
        public static int ShowIt( Control parent, string labelName, int currentline, int? maximum )
        {
            GotoLineNumberForm gotoLineForm = new GotoLineNumberForm( labelName, currentline, maximum );

            DialogResult result = gotoLineForm.ShowDialog( parent );
            if ( result == DialogResult.OK )
            {
                return gotoLineForm.LineNumber;
            }

            return 0;
        }

        #region Variables
        private int m_maximum = 1;
        #endregion

        #region Properties
        public int LineNumber
        {
            get
            {
                int line = 1;
                if (!Int32.TryParse(this.lineNumberMaskedTextBox.Text, out line) || line <= 0)
                {
                    return 1;
                }

                if ( line > m_maximum )
                {
                    return m_maximum;
                }

                return line;
            }
        }
        #endregion

        #region Event Handlers
        private void GotoLineNumberForm_Activated( object sender, EventArgs e )
        {
            this.lineNumberMaskedTextBox.SelectAll();
        }
        #endregion
    }
}