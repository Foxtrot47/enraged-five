using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ragScriptEditorShared
{
    public partial class FolderBrowserEditControl : UserControl
    {
        protected FolderBrowserEditControl()
        {
            InitializeComponent();
        }

        public FolderBrowserEditControl( string description, bool showNewFolderButton )
            : this()
        {
            this.folderBrowserDialog1.Description = description;
            this.folderBrowserDialog1.ShowNewFolderButton = showNewFolderButton;
        }

        #region Properties
        public string Folder
        {
            get
            {
                return this.folderBrowserDialog1.SelectedPath;
            }
        }
        #endregion

        #region Event Handlers
        private void FolderBrowserEditControl_Load( object sender, EventArgs e )
        {
            this.folderBrowserDialog1.ShowDialog( this );
        }
        #endregion
    }
}
