﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ragScriptEditorShared
{
    public partial class FileMessageBox : Form
    {
        public enum ResultType
        {
            None,
            Yes,
            YesToAll,
            No,
            NoToAll
        }

        public String Filename;
        public ResultType Result;

        public FileMessageBox()
        {
            InitializeComponent();
            Result = ResultType.None;
        }

        /// <summary>
        /// Initialize the dialog labels, result.
        /// </summary>
        /// <param name="filename"></param>
        public void Initialize(String filename)
        {
            Filename = filename;
            ModifiedFileLabel.Text = Filename;
            Result = ResultType.None;
        }

        /// <summary>
        /// Event handler for the "Yes" button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnYesButtonClick(object sender, EventArgs e)
        {
            Result = ResultType.Yes;
            Close();
        }

        /// <summary>
        /// Event handler for the "Yes to All" button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnYesToAllButtonClick(object sender, EventArgs e)
        {
            Result = ResultType.YesToAll;
            Close();
        }

        /// <summary>
        /// Event handler for the "No" button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnNoButtonClick(object sender, EventArgs e)
        {
            Result = ResultType.No;
            Close();
        }

        /// <summary>
        /// Event handler for the "No To All" button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnNoToAllButtonClick(object sender, EventArgs e)
        {
            Result = ResultType.NoToAll;
            Close();
        }
    }
}
