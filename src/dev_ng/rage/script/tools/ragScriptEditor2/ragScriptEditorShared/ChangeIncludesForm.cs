using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Forms;

namespace ragScriptEditorShared
{
    public partial class ChangeIncludesForm : Form
    {
        public ChangeIncludesForm()
        {
            InitializeComponent();
        }

        public static DialogResult ShowIt( System.Windows.Forms.Form masterForm, string scriptFileDialogFilter,
            CompilingSettings currentSettings, CompilingSettings defaultSettings, SaveSettingsDelegate saveDelegate )
        {
            ChangeIncludesForm form = new ChangeIncludesForm();
            form.SaveSettings = saveDelegate;

            form.m_scriptFileFilter = scriptFileDialogFilter;

            form.m_currentEditorSettings = currentSettings;
            form.m_defaultEditorSettings = defaultSettings;

            form.SetUIFromEditorSettings( form.m_currentEditorSettings );
            form.applyButton.Enabled = false;

            return form.ShowDialog( masterForm );
        }

        #region Delegates
        public delegate void SaveSettingsDelegate( CompilingSettings settings );
        #endregion

        #region Variables
        private CompilingSettings m_currentEditorSettings = null;
        private CompilingSettings m_defaultEditorSettings = null;
        private string m_scriptFileFilter = "";
        #endregion

        #region Properties
        public SaveSettingsDelegate SaveSettings = null;
        #endregion

        #region Event Handlers
        private void browseIncludeFileButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Filter = m_scriptFileFilter;
            this.openFileDialog.Multiselect = false;
            this.openFileDialog.Title = "Select Include File";

            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.includeFileTextBox.Text = this.openFileDialog.FileName;
            }
        }

        private void browseIncludePathButton_Click( object sender, EventArgs e )
        {
            this.folderBrowserDialog.Description = "Add Include Path";
            this.folderBrowserDialog.ShowNewFolderButton = false;

            DialogResult result = this.folderBrowserDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.includePathTextBox.Text = this.includePathTextBox.Text.Trim();

                if ( this.includePathTextBox.TextLength > 0 )
                {
                    this.includePathTextBox.Text += Path.PathSeparator;
                }

                this.includePathTextBox.Text += this.folderBrowserDialog.SelectedPath.Trim();
            }
        }

        private void resetToDefaultsButton_Click( object sender, EventArgs e )
        {
            DialogResult result = rageMessageBox.ShowWarning( this,
                "Resetting the Include options to their game-specific\ndefault state cannot be undone.",
                "Reset Include Settings", MessageBoxButtons.OKCancel );
            if ( result == DialogResult.OK )
            {
                if ( (m_currentEditorSettings != null) && (m_defaultEditorSettings != null) )
                {
                    m_currentEditorSettings.Copy( m_defaultEditorSettings );
                    SetUIFromEditorSettings( m_currentEditorSettings );
                }

                if ( OnSaveSettings() )
                {
                    this.applyButton.Enabled = false;
                }
            }
        }

        private void okButton_Click( object sender, EventArgs e )
        {
            if ( !this.applyButton.Enabled || OnSaveSettings() )
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void applyButton_Click( object sender, EventArgs e )
        {
            if ( OnSaveSettings() )
            {
                this.applyButton.Enabled = false;
            }
        }

        private void EnableApplyButtonCheck( object sender, System.EventArgs e )
        {
            this.applyButton.Enabled = true;
        }
        #endregion

        #region Private Functions
        private bool OnSaveSettings()
        {
            if ( m_currentEditorSettings != null )
            {
                SetEditorSettingsFromUI( m_currentEditorSettings );
            }

            if ( SaveSettings != null )
            {
                SaveSettings( m_currentEditorSettings );
            }

            return true;
        }

        /// <summary>
        /// Retrieves items from UserEditorSettings and puts them in the from
        /// </summary>
        /// <param name="settings"></param>
        private void SetUIFromEditorSettings( CompilingSettings settings )
        {
            // Compiling            
            this.includeFileTextBox.Text = settings.IncludeFile;
            this.includePathTextBox.Text = settings.IncludePath;
        }

        /// <summary>
        /// Retrieves items from the form and puts them in UserEditorSettings
        /// </summary>
        /// <param name="settings"></param>
        private void SetEditorSettingsFromUI( CompilingSettings settings )
        {
            settings.IncludeFile = this.includeFileTextBox.Text.Trim();
            settings.IncludePath = this.includePathTextBox.Text.Trim();
        }
        #endregion
    }
}