namespace ragScriptEditorShared
{
    partial class FileSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.middleClickCloseCheckBox = new System.Windows.Forms.CheckBox();
            this.enableSemanticParsingCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // middleClickCloseCheckBox
            // 
            this.middleClickCloseCheckBox.AutoSize = true;
            this.middleClickCloseCheckBox.Location = new System.Drawing.Point( 3, 3 );
            this.middleClickCloseCheckBox.Name = "middleClickCloseCheckBox";
            this.middleClickCloseCheckBox.Size = new System.Drawing.Size( 243, 17 );
            this.middleClickCloseCheckBox.TabIndex = 3;
            this.middleClickCloseCheckBox.Text = "Middle Button Click on File Tab closes the File";
            this.middleClickCloseCheckBox.UseVisualStyleBackColor = true;
            this.middleClickCloseCheckBox.CheckedChanged += new System.EventHandler( this.SettingChanged );
            // 
            // enableSemanticParsingCheckBox
            // 
            this.enableSemanticParsingCheckBox.AutoSize = true;
            this.enableSemanticParsingCheckBox.Location = new System.Drawing.Point( 3, 26 );
            this.enableSemanticParsingCheckBox.Name = "enableSemanticParsingCheckBox";
            this.enableSemanticParsingCheckBox.Size = new System.Drawing.Size( 265, 17 );
            this.enableSemanticParsingCheckBox.TabIndex = 4;
            this.enableSemanticParsingCheckBox.Text = "Enable Semantic Parsing Service during file editing";
            this.enableSemanticParsingCheckBox.UseVisualStyleBackColor = true;
            this.enableSemanticParsingCheckBox.CheckedChanged += new System.EventHandler( this.SettingChanged );
            // 
            // FileSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.middleClickCloseCheckBox );
            this.Controls.Add( this.enableSemanticParsingCheckBox );
            this.Name = "FileSettingsControl";
            this.Size = new System.Drawing.Size( 381, 57 );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox middleClickCloseCheckBox;
        private System.Windows.Forms.CheckBox enableSemanticParsingCheckBox;
    }
}
