using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using ActiproSoftware.SyntaxEditor;

namespace ragScriptEditorShared
{
    public interface ITabSettings
    {
        string Name { get; }

        bool SaveXml( XmlTextWriter writer );

        bool LoadXml( XmlTextReader reader );

        void CopyFrom( ITabSettings ts );

        void CopyFrom( EditorSettingsBase settings );

        ITabSettings Clone();

        void Reset();
    }

    public interface ITabSettingsControl
    {
        ITabSettings TabSettings { get; set; }
    }

    public class ProjectTabSettings : ITabSettings
    {
        public ProjectTabSettings( bool projectSpecific, bool gameSpecific )
        {
            Reset();
            this.ProjectSpecific = projectSpecific;
            this.GameSpecific = gameSpecific;
        }

        #region Properties
        public bool ProjectSpecific;
        public bool GameSpecific;
        public bool ReloadLastProject;
        public bool CloseProjectFiles;
        public bool ParsingServiceEnabled;
        public bool AutomaticSorting;
        public string LastProject;
        public List<string> RecentProjects = new List<string>();
        public string PostCompileCommand;
        public string StartDebugCommand;
        #endregion

        #region Overrides
        public override bool Equals( System.Object obj )
        {
            // If parameter is null return false.
            if ( obj == null )
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ProjectTabSettings p = obj as ProjectTabSettings;
            if ( (System.Object)p == null )
            {
                return false;
            }

            bool apples2apples = (this.GameSpecific == p.GameSpecific) && (this.ProjectSpecific == p.ProjectSpecific);

            // Return true if the fields match:
            return (!apples2apples || ((this.ReloadLastProject == p.ReloadLastProject) && (this.CloseProjectFiles == p.CloseProjectFiles) && (this.ParsingServiceEnabled == p.ParsingServiceEnabled)))
                && (this.AutomaticSorting == p.AutomaticSorting)
                && (this.PostCompileCommand == p.PostCompileCommand)
                && (this.StartDebugCommand == p.StartDebugCommand);
        }

        public bool Equals( ProjectTabSettings p )
        {
            // If parameter is null return false:
            if ( (object)p == null )
            {
                return false;
            }

            bool apples2apples = (this.GameSpecific == p.GameSpecific) && (this.ProjectSpecific == p.ProjectSpecific);

            // Return true if the fields match:
            return (!apples2apples || ((this.ReloadLastProject == p.ReloadLastProject) && (this.CloseProjectFiles == p.CloseProjectFiles) && (this.ParsingServiceEnabled == p.ParsingServiceEnabled)))
                && (this.AutomaticSorting == p.AutomaticSorting)
                && (this.PostCompileCommand == p.PostCompileCommand)
                && (this.StartDebugCommand == p.StartDebugCommand);
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.GameSpecific );
            s.Append( this.ProjectSpecific );
            s.Append( this.ReloadLastProject );
            s.Append( this.CloseProjectFiles );
            s.Append( this.ParsingServiceEnabled );
            s.Append( this.AutomaticSorting );
            s.Append( this.PostCompileCommand );
            s.Append( this.StartDebugCommand );
            return s.ToString().GetHashCode();
        }
        #endregion

        #region ITabSettings
        [Browsable( false )]
        public string Name
        {
            get
            {
                return "Project Settings";
            }
        }

        /// <summary>
        /// Writes project-specific items to the XmlTextWriter
        /// </summary>
        /// <param name="writer"></param>
        /// <returns></returns>
        public bool SaveXml( XmlTextWriter writer )
        {
            if ( !ProjectSpecific || GameSpecific )
            {
                writer.WriteAttributeString( "ReloadLastProject", Convert.ToString( ReloadLastProject ) );
                writer.WriteAttributeString( "CloseProjectFiles", Convert.ToString( CloseProjectFiles ) );
            }

            if ( !ProjectSpecific )
            {
                writer.WriteAttributeString( "LastProject", LastProject );
            }

            if ( !ProjectSpecific && !GameSpecific )
            {
                writer.WriteAttributeString( "ParsingServiceEnabled", Convert.ToString( ParsingServiceEnabled ) );
            }

            writer.WriteAttributeString( "AutomaticSorting", Convert.ToString( AutomaticSorting ) );
            writer.WriteStartElement( "PostCompileCommand" );
            writer.WriteString( PostCompileCommand );
            writer.WriteEndElement();

            writer.WriteStartElement( "StartDebugCommand" );
            writer.WriteString( StartDebugCommand );
            writer.WriteEndElement();

            if ( !ProjectSpecific && !GameSpecific )
            {
                writer.WriteStartElement( "RecentProjects" );
                foreach ( string project in RecentProjects )
                {
                    writer.WriteStartElement( "Project" );
                    writer.WriteString( project );
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }

            return true;
        }

        /// <summary>
        /// Reads project-specific items from the XmlTextReader
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public bool LoadXml( XmlTextReader reader )
        {
            string parentName = reader.Name;

            Reset();

            string reloadLastProj = reader["ReloadLastProject"];
            if ( reloadLastProj != null )
            {
                ReloadLastProject = Convert.ToBoolean( reloadLastProj );
            }

            string closeProjFiles = reader["CloseProjectFiles"];
            if ( closeProjFiles != null )
            {
                CloseProjectFiles = Convert.ToBoolean( closeProjFiles );
            }

            string parsingServiceEnabled = reader["ParsingServiceEnabled"];
            if ( parsingServiceEnabled != null )
            {
                ParsingServiceEnabled = Convert.ToBoolean( parsingServiceEnabled );
            }

            string autoSort = reader["AutomaticSorting"];
            if ( autoSort != null )
            {
                AutomaticSorting = Convert.ToBoolean( autoSort );
            }

            string lastProj = reader["LastProject"];
            if ( lastProj != null )
            {
                LastProject = lastProj;
            }

            while ( reader.Read() )
            {
                if ( !reader.IsStartElement() || reader.IsEmptyElement )
                {
                    if ( reader.Name == parentName )
                    {
                        break;	// we're done
                    }
                }
                else if ( reader.Name == "PostCompileCommand" )
                {
                    PostCompileCommand = reader.ReadString();
                }
                else if ( (reader.Name == "StartDebugCommand") || (reader.Name == "RunCommand") )
                {
                    StartDebugCommand = reader.ReadString();
                }
                else if ( reader.Name == "RecentProjects" )
                {
                    RecentProjects.Clear();

                    if ( !reader.IsEmptyElement )
                    {
                        while ( reader.Read() )
                        {
                            if ( !reader.IsStartElement() )
                            {
                                if ( reader.Name == "RecentProjects" )
                                {
                                    break; // we're done
                                }
                            }
                            else if ( reader.Name == "Project" )
                            {
                                string project = reader.ReadString();
                                if ( (project != null) && (project != "") )
                                {
                                    RecentProjects.Add( project );
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }

        public void CopyFrom( ITabSettings ts )
        {
            ProjectTabSettings p = ts as ProjectTabSettings;
            if ( p == null )
            {
                return;
            }

            if ( (!p.ProjectSpecific && !this.ProjectSpecific) || (p.GameSpecific && this.GameSpecific) )
            {
                this.ReloadLastProject = p.ReloadLastProject;
                this.CloseProjectFiles = p.CloseProjectFiles;
            }

            if ( !p.ProjectSpecific && !this.ProjectSpecific )
            {
                this.LastProject = p.LastProject;
                this.ParsingServiceEnabled = p.ParsingServiceEnabled;
            }

            this.AutomaticSorting = p.AutomaticSorting;
            this.PostCompileCommand = p.PostCompileCommand;
            this.StartDebugCommand = p.StartDebugCommand;

            if ( !p.ProjectSpecific && !this.ProjectSpecific && !p.GameSpecific && !this.GameSpecific )
            {
                this.LastProject = p.LastProject;
                this.ReloadLastProject = p.ReloadLastProject;
                this.CloseProjectFiles = p.CloseProjectFiles;

                this.RecentProjects.Clear();
                foreach ( string project in p.RecentProjects )
                {
                    this.RecentProjects.Add( project );
                }
            }
        }

        public void CopyFrom( EditorSettingsBase settings )
        {
            this.CopyFrom( settings.ProjectSettings );
        }

        public ITabSettings Clone()
        {
            ProjectTabSettings p = new ProjectTabSettings( this.ProjectSpecific, this.GameSpecific );
            p.CopyFrom( this );
            return p;
        }

        public void Reset()
        {
            this.ReloadLastProject = true;
            this.CloseProjectFiles = true;
            this.AutomaticSorting = false;
            this.LastProject = string.Empty;
            this.PostCompileCommand = string.Empty;
            this.StartDebugCommand = string.Empty;

            IntPtr handle = (IntPtr)Process.GetCurrentProcess().Handle;
            UIntPtr ProcAffinityMask = UIntPtr.Zero;
            UIntPtr SysAffinityMask = UIntPtr.Zero;

            if ( ApplicationSettings.GetProcessAffinityMask( handle, out ProcAffinityMask, out SysAffinityMask ) && ((uint)ProcAffinityMask != 1) )
            {
                // start the parsing service
                this.ParsingServiceEnabled = true;
            }
            else
            {
                this.ParsingServiceEnabled = false;
            }
        }
        #endregion

        #region Public Functions
        public void CopyFrom( OldProjectTabSettings p )
        {
            if ( p == null )
            {
                return;
            }

            if ( (!p.ProjectSpecific && !this.ProjectSpecific) || (p.GameSpecific && this.GameSpecific) )
            {
                this.ReloadLastProject = p.ReloadLastProject;
                this.CloseProjectFiles = p.CloseProjectFiles;
            }

            if ( !p.ProjectSpecific && !this.ProjectSpecific )
            {
                this.LastProject = p.LastProject;
                this.ParsingServiceEnabled = p.ParsingServiceEnabled;
            }

            this.AutomaticSorting = p.AutomaticSorting;
            this.PostCompileCommand = p.PostCompileCommand;
            this.StartDebugCommand = p.StartDebugCommand;

            if ( !p.ProjectSpecific && !this.ProjectSpecific && !p.GameSpecific && !this.GameSpecific )
            {
                this.LastProject = p.LastProject;
                this.ReloadLastProject = p.ReloadLastProject;
                this.CloseProjectFiles = p.CloseProjectFiles;

                this.RecentProjects.Clear();
                foreach ( string project in p.RecentProjects )
                {
                    this.RecentProjects.Add( project );
                }
            }
        }
        #endregion
    }

    public class FileTabSettings : ITabSettings
    {
        public FileTabSettings( bool gameSpecific )
        {
            Reset();
            this.GameSpecific = gameSpecific;
        }

        #region Properties
        public bool GameSpecific;
        public bool ReloadLastFiles;
        public bool MiddleClickClose;
        public bool ParsingServiceEnabled;
        public List<string> RecentFiles = new List<string>();
        #endregion

        #region Overrides
        public override bool Equals( System.Object obj )
        {
            // If parameter is null return false.
            if ( obj == null )
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            FileTabSettings f = obj as FileTabSettings;
            if ( (System.Object)f == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (this.ReloadLastFiles == f.ReloadLastFiles)
                && (this.MiddleClickClose == f.MiddleClickClose)
                && (this.ParsingServiceEnabled == f.ParsingServiceEnabled);
        }

        public bool Equals( FileTabSettings f )
        {
            // If parameter is null return false:
            if ( (object)f == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (this.ReloadLastFiles == f.ReloadLastFiles)
                && (this.MiddleClickClose == f.MiddleClickClose)
                && (this.ParsingServiceEnabled == f.ParsingServiceEnabled);
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.ReloadLastFiles );
            s.Append( this.MiddleClickClose );
            s.Append( this.ParsingServiceEnabled );
            return s.ToString().GetHashCode();
        }
        #endregion

        #region ITabSettings
        [Browsable( false )]
        public string Name
        {
            get
            {
                return "File Settings";
            }
        }

        /// <summary>
        /// Write file-specific settings to the XmlTextWriter
        /// </summary>
        /// <param name="writer"></param>
        /// <returns></returns>
        public bool SaveXml( XmlTextWriter writer )
        {
            writer.WriteAttributeString( "ReloadLastFiles", Convert.ToString( this.ReloadLastFiles ) );
            writer.WriteAttributeString( "MiddleClickClose", Convert.ToString( this.MiddleClickClose ) );
            writer.WriteAttributeString( "ParsingServiceEnabled", Convert.ToString( this.ParsingServiceEnabled ) );

            if ( !GameSpecific )
            {
                writer.WriteStartElement( "RecentFiles" );
                foreach ( string file in this.RecentFiles )
                {
                    writer.WriteStartElement( "File" );
                    writer.WriteString( file );
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }

            return true;
        }

        /// <summary>
        /// Read file-specific settings from the XmlTextReader
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public bool LoadXml( XmlTextReader reader )
        {
            string parentName = reader.Name;

            Reset();

            string reloadLastFiles = reader["ReloadLastFiles"];
            if ( reloadLastFiles != null )
            {
                this.ReloadLastFiles = Convert.ToBoolean( reloadLastFiles );
            }

            string middleClick = reader["MiddleClickClose"];
            if ( middleClick != null )
            {
                this.MiddleClickClose = Convert.ToBoolean( middleClick );
            }

            string parsingServiceEnabled = reader["ParsingServiceEnabled"];
            if ( parsingServiceEnabled != null )
            {
                this.ParsingServiceEnabled = Convert.ToBoolean( parsingServiceEnabled );
            }

            while ( reader.Read() )
            {
                if ( !reader.IsStartElement() || reader.IsEmptyElement )
                {
                    if ( reader.Name == parentName )
                    {
                        break; // we're done 
                    }
                }
                else if ( reader.Name == "RecentFiles" )
                {
                    this.RecentFiles.Clear();

                    if ( !reader.IsEmptyElement )
                    {
                        while ( reader.Read() )
                        {
                            if ( !reader.IsStartElement() )
                            {
                                if ( reader.Name == "RecentFiles" )
                                {
                                    break; // we're done
                                }
                            }
                            else if ( reader.Name == "File" )
                            {
                                string file = reader.ReadString();
                                if ( (file != null) && (file != "") )
                                {
                                    this.RecentFiles.Add( file );
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }

        public void CopyFrom( ITabSettings ts )
        {
            FileTabSettings f = ts as FileTabSettings;
            if ( f == null )
            {
                return;
            }

            this.ReloadLastFiles = f.ReloadLastFiles;
            this.MiddleClickClose = f.MiddleClickClose;
            this.ParsingServiceEnabled = f.ParsingServiceEnabled;

            if ( !this.GameSpecific )
            {
                this.RecentFiles.Clear();
                foreach ( string file in f.RecentFiles )
                {
                    this.RecentFiles.Add( file );
                }
            }
        }

        public void CopyFrom( EditorSettingsBase settings )
        {
            this.CopyFrom( settings.FileSettings );
        }

        public ITabSettings Clone()
        {
            FileTabSettings f = new FileTabSettings( this.GameSpecific );
            f.CopyFrom( this );
            return f;
        }

        public void Reset()
        {
            this.ReloadLastFiles = true;
            this.MiddleClickClose = true;
            this.ParsingServiceEnabled = true;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( OldFileTabSettings f )
        {
            if ( f == null )
            {
                return;
            }

            this.ReloadLastFiles = f.ReloadLastFiles;
            this.MiddleClickClose = f.MiddleClickClose;
            this.ParsingServiceEnabled = f.ParsingServiceEnabled;

            if ( !this.GameSpecific )
            {
                this.RecentFiles.Clear();
                foreach ( string file in f.RecentFiles )
                {
                    this.RecentFiles.Add( file );
                }
            }
        }
        #endregion
    }

    public class ToolbarTabSettings : ITabSettings
    {
        public ToolbarTabSettings()
        {
            Reset();
        }

        #region Properties
        public bool RememberButtonVisiblity;
        public List<ToolbarButtonSetting> ToolbarButtonSettings = new List<ToolbarButtonSetting>();
        public List<ToolStripLocation> ToolStripLocations = new List<ToolStripLocation>();
        #endregion

        #region Overrides
        public override bool Equals( System.Object obj )
        {
            // If parameter is null return false.
            if ( obj == null )
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ToolbarTabSettings t = obj as ToolbarTabSettings;
            if ( (System.Object)t == null )
            {
                return false;
            }

            // Return true if the fields match:
            if ( (this.RememberButtonVisiblity == t.RememberButtonVisiblity)
                && (this.ToolbarButtonSettings.Count == t.ToolbarButtonSettings.Count)
                && (this.ToolStripLocations.Count == t.ToolStripLocations.Count) )
            {
                int count = this.ToolbarButtonSettings.Count;
                for ( int i = 0; i < count; ++i )
                {
                    if ( !this.ToolbarButtonSettings[i].Equals( t.ToolbarButtonSettings[i] ) )
                    {
                        return false;
                    }
                }

                count = this.ToolStripLocations.Count;
                for ( int i = 0; i < count; ++i )
                {
                    if ( !this.ToolStripLocations[i].Equals( t.ToolStripLocations[i] ) )
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public bool Equals( ToolbarTabSettings t )
        {
            // If parameter is null return false:
            if ( (object)t == null )
            {
                return false;
            }

            // Return true if the fields match:
            if ( (this.RememberButtonVisiblity == t.RememberButtonVisiblity)
                && (this.ToolbarButtonSettings.Count == t.ToolbarButtonSettings.Count)
                && (this.ToolStripLocations.Count == t.ToolStripLocations.Count) )
            {
                int count = this.ToolbarButtonSettings.Count;
                for ( int i = 0; i < count; ++i )
                {
                    if ( this.ToolbarButtonSettings[i] != t.ToolbarButtonSettings[i] )
                    {
                        return false;
                    }
                }

                count = this.ToolStripLocations.Count;
                for ( int i = 0; i < count; ++i )
                {
                    if ( this.ToolStripLocations[i] != t.ToolStripLocations[i] )
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.RememberButtonVisiblity.ToString() );
            s.Append( this.ToolbarButtonSettings.Count.ToString() );
            foreach ( ToolbarButtonSetting b in this.ToolbarButtonSettings )
            {
                s.Append( b.ToString() );
            }
            foreach ( ToolStripLocation l in this.ToolStripLocations )
            {
                s.Append( l.ToString() );
            }

            return s.ToString().GetHashCode();
        }
        #endregion

        #region ITabSettings
        [Browsable( false )]
        public string Name
        {
            get
            {
                return "Toolbar Settings";
            }
        }

        /// <summary>
        /// Write toolbar-specific settings to the XmlTextWriter
        /// </summary>
        /// <param name="writer"></param>
        /// <returns></returns>
        public bool SaveXml( XmlTextWriter writer )
        {
            writer.WriteAttributeString( "RememberButtonVisibility", Convert.ToString( this.RememberButtonVisiblity ) );

            writer.WriteStartElement( "ToolbarButtons" );
            foreach ( ToolbarButtonSetting button in this.ToolbarButtonSettings )
            {
                writer.WriteStartElement( "Button" );
                button.SaveButton( writer );
                writer.WriteEndElement();
            }
            writer.WriteEndElement();

            writer.WriteStartElement( "ToolStripLocations" );
            foreach ( ToolStripLocation loc in this.ToolStripLocations )
            {
                writer.WriteStartElement( "ToolStripLocation" );
                loc.SaveLocation( writer );
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            return true;
        }

        /// <summary>
        /// Read toolbar-specific settings from the XmlTextReader
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public bool LoadXml( XmlTextReader reader )
        {
            string parentName = reader.Name;

            Reset();

            string rememberVis = reader["RememberButtonVisibility"];
            if ( rememberVis != null )
            {
                this.RememberButtonVisiblity = Convert.ToBoolean( rememberVis );
            }

            while ( reader.Read() )
            {
                if ( !reader.IsStartElement() || reader.IsEmptyElement )
                {
                    if ( reader.Name == parentName )
                    {
                        break; // we're done 
                    }
                }
                else if ( reader.Name == "ToolbarButtons" )
                {
                    this.ToolbarButtonSettings.Clear();

                    while ( reader.Read() )
                    {
                        if ( !reader.IsStartElement() )
                        {
                            if ( reader.Name == "ToolbarButtons" )
                            {
                                break; // we're done
                            }
                        }
                        else if ( reader.Name == "Button" )
                        {
                            ToolbarButtonSetting button = new ToolbarButtonSetting();
                            if ( !button.LoadButton( reader ) )
                            {
                                return false;
                            }
                            else
                            {
                                this.ToolbarButtonSettings.Add( button );
                            }
                        }
                    }
                }
                else if ( reader.Name == "ToolStripLocations" )
                {
                    this.ToolStripLocations.Clear();

                    while ( reader.Read() )
                    {
                        if ( !reader.IsStartElement() )
                        {
                            if ( reader.Name == "ToolStripLocations" )
                            {
                                break; // we're done
                            }
                        }
                        else if ( reader.Name == "ToolStripLocation" )
                        {
                            ToolStripLocation loc = new ToolStripLocation();
                            if ( !loc.LoadLocation( reader ) )
                            {
                                return false;
                            }
                            else
                            {
                                this.ToolStripLocations.Add( loc );
                            }
                        }
                    }
                }
            }

            return true;
        }

        public void CopyFrom( ITabSettings ts )
        {
            ToolbarTabSettings t = ts as ToolbarTabSettings;
            if ( t == null )
            {
                return;
            }

            this.RememberButtonVisiblity = t.RememberButtonVisiblity;

            this.ToolbarButtonSettings.Clear();
            foreach ( ToolbarButtonSetting button in t.ToolbarButtonSettings )
            {
                this.ToolbarButtonSettings.Add( button );
            }

            this.ToolStripLocations.Clear();
            foreach ( ToolStripLocation location in t.ToolStripLocations )
            {
                this.ToolStripLocations.Add( location );
            }
        }

        public void CopyFrom( EditorSettingsBase settings )
        {
            this.CopyFrom( settings.ToolbarSettings );
        }

        public ITabSettings Clone()
        {
            ToolbarTabSettings t = new ToolbarTabSettings();
            t.CopyFrom( this );
            return t;
        }

        public void Reset()
        {
            this.RememberButtonVisiblity = true;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( OldToolbarTabSettings t )
        {
            if ( t == null )
            {
                return;
            }

            this.RememberButtonVisiblity = t.RememberButtonVisiblity;

            this.ToolbarButtonSettings.Clear();
            foreach ( ToolbarButtonSetting button in t.ToolbarButtonSettings )
            {
                this.ToolbarButtonSettings.Add( button );
            }

            this.ToolStripLocations.Clear();
            foreach ( ToolStripLocation location in t.ToolStripLocations )
            {
                this.ToolStripLocations.Add( location );
            }
        }
        #endregion
    }

    public abstract class CompilingTabSettingsBase : PropertyGridSettingsBase, ITabSettings
    {
        public CompilingTabSettingsBase( string language, string configurationName, bool projectSpecific )
        {
            m_language = language;
            m_projectSpecific = projectSpecific;
            m_configurationName = configurationName;

            Reset();

            m_fileBrowserTitles.Add( "CompilerExecutable", "Select Compiler Executable" );
            m_fileBrowserFilters.Add( "CompilerExecutable", "Executables (*.exe;*.bat)|*.exe;*.bat|All Files (*.*)|*.*" );

            m_folderBrowserDescriptions.Add( "OutputDirectory", "Select Output Directory" );
            m_folderBrowserShowNewFolderButtons.Add( "OutputDirectory", false );
        }

        #region Variables
        protected string m_language = string.Empty;
        protected bool m_projectSpecific = false;
        protected string m_configurationName = "Debug";
        protected bool m_breakOnErrors = false;
        protected bool m_openFilesWithErrors = false;
        protected string m_compilerExecutable = string.Empty;
        protected string m_outputDirectory = string.Empty;
        #endregion

        #region Properties
        [Browsable( false )]
        public string Language
        {
            get
            {
                return m_language;
            }
            set
            {
                m_language = value;
            }
        }

        [Browsable( false )]
        public bool ProjectSpecific
        {
            get
            {
                return m_projectSpecific;
            }
            set
            {
                m_projectSpecific = value;
            }
        }

        [Browsable( false )]
        public string ConfigurationName
        {
            get
            {
                return m_configurationName;
            }
        }
        
        [DisplayName( "Break On Errors" ),
        Description( "Halts the compiling process on the first file with errors found." ),
        DefaultValue( false ),
        Category( "Options" )]
        public bool BreakOnErrors
        {
            get
            {
                return m_breakOnErrors;
            }
            set
            {
                m_breakOnErrors = value;
            }
        }

        [DisplayName( "Open Files With Errors" ),
        Description( "When enabled, files that contain errors will be opened and their errors highlighted during the compile process." ),
        DefaultValue( false ),
        Category( "Options" )]
        public bool OpenFilesWithErrors
        {
            get
            {
                return m_openFilesWithErrors;
            }
            set
            {
                m_openFilesWithErrors = value;
            }
        }

        [Browsable( false )]
        public string CompilerExecutableRaw
        {
            get
            {
                return m_compilerExecutable;
            }
        }

        [DisplayName( "Compiler Executable" ),
        Description( "The executable that compiles a single file." ),
        DefaultValue( "" ),
        Editor( typeof( FileBrowserEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
        public string CompilerExecutable
        {
            get
            {
                if ( string.IsNullOrEmpty( m_compilerExecutable ) )
                {
                    return m_compilerExecutable;
                }

                return Path.Combine( ApplicationSettings.ExecutablePath, m_compilerExecutable );
            }
            set
            {
                m_compilerExecutable = value;
            }
        }

        [DisplayName( "Output Directory" ),
        Description( "The directory where the compiler output files are placed.  This can be relative to the project or path-rooted." ),
        DefaultValue( "" ),
        Category( "Output" ),
        Editor( typeof( FolderBrowserEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
        public string OutputDirectory
        {
            get
            {
                return m_outputDirectory;
            }
            set
            {
                m_outputDirectory = value;
            }
        }
        #endregion

        #region Virtual Functions
        public virtual bool Equals( CompilingTabSettingsBase c )
        {
            // If parameter is null return false:
            if ( (object)c == null )
            {
                return false;
            }

            bool apples2apples = this.ProjectSpecific == c.ProjectSpecific;

            // Return true if the fields match:
            return (!apples2apples || ((this.BreakOnErrors == c.BreakOnErrors) && (this.OpenFilesWithErrors == c.OpenFilesWithErrors) && (this.Language == c.Language)))
                && (m_compilerExecutable == c.m_compilerExecutable)
                && (this.OutputDirectory == c.OutputDirectory);
        }

        public virtual bool Validate( string projectFile )
        {
            string cmd = BuildCompileScriptCommand( "%1", projectFile );
            if ( cmd == string.Empty )
            {
                return true;
            }

            string[] cmdSplit = cmd.Split( ' ' );
            int i = 0;
            while ( (i < cmdSplit.Length) && (cmdSplit[i].Length == 0) )
            {
                ++i;
            }

            if ( (i < cmdSplit.Length) && !File.Exists( cmdSplit[i] ) )
            {
                return false;
            }

            return true;
        }

        public virtual void CopyFrom( OldCompilingTabSettings c )
        {
            if ( c == null )
            {
                return;
            }

            m_language = c.Language;
            this.OutputDirectory = c.OutputDirectory;
            m_configurationName = "Debug";
            this.CompilerExecutable = c.ScriptCompilerRaw;

            if ( !this.ProjectSpecific && !c.ProjectSpecific )
            {
                this.BreakOnErrors = c.BreakOnErrors;
                this.OpenFilesWithErrors = c.OpenFilesWithErrors;
            }
        }

        public virtual string BuildCompileScriptCommand( string sourceFilename, string projectFilename )
        {
            return string.Empty;
        }

        public virtual List<string> GetOutputFilenames( string sourceFilename, string projectFilename )
        {
            return null;
        }

        protected virtual void ResetInternal()
        {

        }

        /// <summary>
        /// Override in a derived class to copy additional data to the <see cref="CompilingTabSettingsBase"/>.
        /// </summary>
        /// <param name="c"></param>
        protected virtual void CopyFromInternal( CompilingTabSettingsBase c )
        {

        }

        protected virtual CompilingTabSettingsBase CloneInternal()
        {
            return null;
        }

        /// <summary>
        /// Override in a derived class to save additional data to the <see cref="XmlTextWriter"/>.
        /// </summary>
        /// <param name="writer"></param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        protected virtual bool SaveXmlInternal( XmlTextWriter writer )
        {
            return true;
        }

        /// <summary>
        /// Override in a derived class to load additional data from the <see cref="XmlTextReader"/>.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        protected virtual bool LoadXmlInternal( XmlTextReader reader )
        {
            return true;
        }
        #endregion

        #region ITabSettings
        [Browsable( false )]
        public string Name
        {
            get
            {
                return "Compiling Settings";
            }
        }

        /// <summary>
        /// Resets the data in this object to their default values.
        /// </summary>
        public void Reset()
        {
            m_configurationName = "Debug";
            this.BreakOnErrors = false;
            this.OpenFilesWithErrors = false;
			this.CompilerExecutable = string.Empty;
			this.OutputDirectory = string.Empty;

            ResetInternal();
        }

        /// <summary>
        /// Copies the data from this object to the <see cref="CompilingTabSettingsBase"/>.
        /// </summary>
        /// <param name="c"></param>
        public void CopyFrom( ITabSettings ts )
        {
            CompilingTabSettingsBase c = ts as CompilingTabSettingsBase;
            if ( c == null )
            {
                return;
            }

            m_language = c.Language;
            this.OutputDirectory = c.OutputDirectory;
            m_configurationName = "Debug";
            this.CompilerExecutable = c.m_compilerExecutable;

            if ( !this.ProjectSpecific && !c.ProjectSpecific )
            {
                this.BreakOnErrors = c.BreakOnErrors;
                this.OpenFilesWithErrors = c.OpenFilesWithErrors;
            }

            CopyFromInternal( c );
        }

        public void CopyFrom( EditorSettingsBase settings )
        {
            this.CopyFrom( settings.CompilingSettings );
        }

        public ITabSettings Clone()
        {
            return CloneInternal();
        }

        /// <summary>
        /// Saves the data from this object to the <see cref="XmlTextWriter"/>.
        /// </summary>
        /// <param name="writer"></param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        public bool SaveXml( XmlTextWriter writer )
        {
            writer.WriteAttributeString( "Language", this.Language );
            if ( !this.ProjectSpecific )
            {
                writer.WriteAttributeString( "BreakOnErrors", Convert.ToString( BreakOnErrors ) );
                writer.WriteAttributeString( "OpenFilesWithErrors", Convert.ToString( OpenFilesWithErrors ) );
            }

            writer.WriteAttributeString( "CompilerExecutable", this.CompilerExecutable );

            return SaveXmlInternal( writer );
        }

        /// <summary>
        /// Loads the data to the object from the <see cref="XmlTextReader"/>.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        public bool LoadXml( XmlTextReader reader )
        {
            Reset();

            string language = reader["Language"];
            if ( language != null )
            {
                if ( language.ToLower() != this.Language.ToLower() )
                {
                    return false;
                }
            }

            string breakOnErr = reader["BreakOnErrors"];
            if ( breakOnErr != null )
            {
                this.BreakOnErrors = bool.Parse( breakOnErr );
            }

            string openOnErr = reader["OpenFilesWithErrors"];
            if ( openOnErr != null )
            {
                this.OpenFilesWithErrors = bool.Parse( openOnErr );
            }

            string exe = reader["CompilerExecutable"];
            if ( exe != null )
            {
                this.CompilerExecutable = exe;
            }

            return LoadXmlInternal( reader );
        }
        #endregion    

        #region Public Functions
        /// <summary>
        /// Calls the other BuildOutputFilename with the current value of OutputDirectory
        /// </summary>
        /// <param name="fullPath"></param>
        /// <param name="projectFile"></param>
        /// <returns></returns>
        public string BuildOutputFilename( string fullPath, string projectFile )
        {
            return BuildOutputFilename( OutputDirectory, fullPath, projectFile );
        } 
        #endregion

        #region Public Static Functions
        /// <summary>
        /// Effectively changes the path of fullPath to use the outputDirectory and projectFile, if either exist.
        /// </summary>
        /// <param name="outputDirectory"></param>
        /// <param name="fullPath"></param>
        /// <param name="projectFile"></param>
        /// <returns></returns>
        public static string BuildOutputFilename( string outputDirectory, string fullPath, string projectFile )
        {
            bool isFileArg = fullPath.IndexOf( "%" ) > -1;
            System.Text.StringBuilder outputFilename = new System.Text.StringBuilder();

            if ( outputDirectory != string.Empty )
            {
                // we've set an output directory
                if ( Path.IsPathRooted( outputDirectory ) )
                {
                    // absolute path for output files
                    outputFilename.Append( outputDirectory );
                }
                else
                {
                    if ( (projectFile != null) && (projectFile != string.Empty) )
                    {
                        // project-relative path for output file
                        outputFilename.Append( Path.GetDirectoryName( projectFile ) );
                    }
                    else
                    {
                        // file-relative path for output file
                        if ( isFileArg || !Path.IsPathRooted( fullPath ) )
                        {
                            outputFilename.Append( "%~dp1" );
                        }
                        else
                        {
                            outputFilename.Append( Path.GetDirectoryName( fullPath ) );
                        }
                    }

                    // the final outputDirectory is a subfolder of what we built above
                    outputFilename.Append( @"\" );
                    outputFilename.Append( outputDirectory );
                }
            }
            else
            {
                // no output directory, so use file-relative path for the output file
                if ( isFileArg || !Path.IsPathRooted( fullPath ) )
                {
                    outputFilename.Append( "%~dp1" );

                    if ( fullPath.StartsWith( "%1" ) )
                    {
                        fullPath = "%~n1" + fullPath.Substring( 2 );
                    }
                }
                else
                {
                    outputFilename.Append( Path.GetDirectoryName( fullPath ) );
                }
            }

            // add the filename
            outputFilename.Append( @"\" );
            if ( isFileArg )
            {
                outputFilename.Append( fullPath );
            }
            else
            {
                outputFilename.Append( Path.GetFileName( fullPath ) );
            }

            return outputFilename.ToString();
        }
        
        /// <summary>
        /// Builds the file name from a partial path name so that it resides in the outputDirectory or projectFile path.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="outputDirectory"></param>
        /// <param name="fullPath"></param>
        /// <param name="projectFile"></param>
        /// <returns></returns>
        static public string BuildFilename( string name, string outputDirectory, string fullPath, string projectFile )
        {
            string filename = name.Trim();
            if ( filename.Length <= 0 )
            {
                return string.Empty;
            }

            if ( !Path.IsPathRooted( filename ) )
            {
                filename = BuildOutputFilename( outputDirectory, filename, projectFile );
            }

            return DereferenceFileArguments( filename, fullPath );
        }
        #endregion

        #region Protected Static Functions

        /// <summary>
        /// If path contains a space, places quotation marks around path and returns the result.  Otherwise, just returns path.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        static protected string BuildPathForCommandLine( string path )
        {
            path = path.Trim();
            if ( (path.IndexOf( "\"" ) == -1) && ((path.IndexOf( " " ) > -1) || (path.IndexOf( "%" ) > -1)) )
            {
                System.Text.StringBuilder quotedPath = new System.Text.StringBuilder( "\"" );
                quotedPath.Append( path );
                quotedPath.Append( "\"" );
                return quotedPath.ToString();
            }

            return path;
        }


        /// <summary>
        /// Searches for all pairs of '%' and '1' (indicating a file argument), and determines the actual value
        /// using fullPath
        /// </summary>
        /// <param name="arg"></param>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        static protected string DereferenceFileArguments( string arg, string fullPath )
        {
            if ( fullPath.IndexOf( "%" ) > -1 )
            {
                return arg;
            }

            int indexOfPercent = arg.IndexOf( "%" );
            while ( indexOfPercent > -1 )
            {
                int indexOfOne = arg.IndexOf( "1", indexOfPercent );
                if ( indexOfOne > -1 )
                {
                    string argRef = arg.Substring( indexOfPercent, indexOfOne - indexOfPercent + 1 );
                    if ( argRef.Length > 0 )
                    {
                        string deref = DereferenceFileArgument( argRef, fullPath );
                        arg = arg.Replace( argRef, deref );
                    }
                }

                indexOfPercent = arg.IndexOf( "%", indexOfPercent + 1 );
            }

            return arg;
        }

        /// <summary>
        /// Determines the value of supported file arguments
        /// </summary>
        /// <param name="reference"></param>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        static protected string DereferenceFileArgument( string reference, string fullPath )
        {
            switch ( reference )
            {
                case "%1":
                    return fullPath;
                case "%~1":
                    // remove quotes
                    return fullPath.Replace( "\"", "" );
                case "%~f1":
                    // expands %1 to a fully qualified path name
                    return Path.GetFullPath( fullPath );
                case "%~d1":
                    // expands %1 to a drive letter only
                    return Directory.GetDirectoryRoot( fullPath );
                case "%~p1":
                    // expands %1 to a path only
                    string path = Path.GetDirectoryName( fullPath );
                    int indexOf = path.IndexOf( ":" );
                    if ( indexOf > -1 )
                    {
                        // remove drive letter
                        return path.Substring( indexOf + 1 );
                    }
                    return @"\";
                case "%~n1":
                    // expands %1 to a file name only
                    return Path.GetFileNameWithoutExtension( fullPath );
                case "%~x1":
                    // expands %1 to a file extension only
                    return Path.GetExtension( fullPath );
                case "%~s1":
                    // expanded path contains short names only
                    return string.Empty;
                case "%~a1":
                    // expands %1 to file attributes
                    if ( File.Exists( fullPath ) )
                    {
                        return File.GetAttributes( fullPath ).ToString();
                    }
                    return string.Empty;
                case "%~t1":
                    // expands %1 to date/time of file
                    if ( File.Exists( fullPath ) )
                    {
                        return File.GetLastWriteTime( fullPath ).ToString();
                    }
                    return string.Empty;
                case "%~z1":
                    // expands %1 to size of file
                    if ( File.Exists( fullPath ) )
                    {
                        System.IO.FileStream stream = File.Open( fullPath, System.IO.FileMode.Open );
                        string rtn = stream.Length.ToString();
                        stream.Close();
                        return rtn;
                    }
                    return string.Empty;
                case "%~$PATH:1":
                    // searches the directories listed in the PATH environment variable and expands %1 to the fully
                    // qualified name of the first one found. If the environment variable name is not defined or the
                    // file is not found by the search, then this modifier expands to the empty string
                    return string.Empty;
                case "%~dp1":
                    // expands %1 to a drive letter and path only
                    return Path.GetDirectoryName( fullPath );
                case "%~nx1":
                    // expands %1 to a file name and extension only
                    return Path.GetFileName( fullPath );
                case "%~dp$PATH:1":
                    // searches the directories listed in the PATH environment variable for %1 and expands to the
                    // drive letter and path of the first one found.
                    return string.Empty;
                case "%~ftza1":
                    // expands %1 to a DIR-like output line
                    System.Text.StringBuilder dirLikeOutput = new System.Text.StringBuilder();
                    dirLikeOutput.Append( File.GetLastWriteTime( fullPath ).ToString() );
                    dirLikeOutput.Append( " " );
                    dirLikeOutput.Append( File.GetLastWriteTime( fullPath ).ToString() );
                    dirLikeOutput.Append( " " );

                    if ( File.Exists( fullPath ) )
                    {
                        System.IO.FileStream stream = File.Open( fullPath, System.IO.FileMode.Open );
                        dirLikeOutput.Append( stream.Length.ToString() );
                        dirLikeOutput.Append( " " );
                        stream.Close();
                    }

                    dirLikeOutput.Append( Path.GetFullPath( fullPath ) );
                    return dirLikeOutput.ToString();
                default:
                    break;
            }

            return fullPath;
        }
        #endregion

    };

    public abstract class LanguageTabSettingsBase : PropertyGridSettingsBase, ITabSettings
    {
        public LanguageTabSettingsBase( string language, bool gameSpecific )
        {
            m_gameSpecific = gameSpecific;
            m_language = language;

            Reset();

            m_fileBrowserTitles.Add( "HelpReferenceFile", "Select Reference Help File" );
            m_fileBrowserFilters.Add( "HelpReferenceFile", "Help Files (*.chm)|*.chm|All Files (*.*)|*.*" );

            m_fileBrowserTitles.Add( "HelpLanguageFile", "Select Language Help File" );
            m_fileBrowserFilters.Add( "HelpLanguageFile", "Help Files (*.chm)|*.chm|All Files (*.*)|*.*" );

            m_folderBrowserDescriptions.Add( "CacheDirectory", "Select the Directory to Store Cache Files" );
            m_folderBrowserShowNewFolderButtons.Add( "CacheDirectory", true );

            m_folderBrowserDescriptions.Add( "CodeSnippetPaths", "Add Code Snippet Path" );
            m_folderBrowserShowNewFolderButtons.Add( "CodeSnippetPaths", false );
        }

        #region Variables
        protected bool m_gameSpecific;
        protected string m_language;
        protected string m_cacheDirectory;
        protected string m_helpReferenceFile;
        protected string m_helpLanguageFile;
        protected string m_codeSnippetPath;
        protected List<string> m_codeSnippetPaths = new List<string>();
        protected ActiproSoftware.SyntaxEditor.CodeSnippetFolder m_codeSnippetFolder;
        #endregion

        #region Properties
        [Browsable( false )]
        public string Language
        {
            get
            {
                return m_language;
            }
            set
            {
                m_language = value;
            }
        }

        [Browsable( false )]
        public bool GameSpecific
        {
            get
            {
                return m_gameSpecific;
            }
        }

        [DisplayName( "Cache Directory" ),
        Description( "The directory where Intellisense Parsing Cache files will be stored." ),
        DefaultValue( "" ),
        Editor( typeof( FolderBrowserEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
        public string CacheDirectory
        {
            get
            {
                return m_cacheDirectory;
            }
            set
            {
                if ( value != m_cacheDirectory )
                {
                    if ( (m_cacheDirectory != null) && (m_cacheDirectory != string.Empty)
                        && Directory.Exists( m_cacheDirectory ) )
                    {
                        try
                        {
                            Directory.Delete( m_cacheDirectory );
                        }
                        catch ( Exception e )
                        {
                            Console.WriteLine( "Attempt to delete the old CacheDirectory failed:\n" + e.Message );
                        }
                    }
                }

                m_cacheDirectory = value;

                if ( (m_cacheDirectory != null) && (m_cacheDirectory != string.Empty)
                    && Path.IsPathRooted( m_cacheDirectory ) && (m_cacheDirectory.IndexOfAny( Path.GetInvalidPathChars() ) == -1) )
                {
                    if ( !Directory.Exists( m_cacheDirectory ) )
                    {
                        Directory.CreateDirectory( m_cacheDirectory );
                    }
                }
            }
        }

        [Browsable( false )]
        public string HelpReferenceFileRaw
        {
            get
            {
                return m_helpReferenceFile;
            }
        }

        [DisplayName( "Reference Help File" ),
        Description( "The Help File for Native Command References." ),
        DefaultValue( "" ),
        Category( "Help" ),
        Editor( typeof( FileBrowserEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
        public string HelpReferenceFile
        {
            get
            {
                if ( (m_helpReferenceFile == null) || (m_helpReferenceFile == string.Empty) )
                {
                    return m_helpReferenceFile;
                }

                return Path.Combine( ApplicationSettings.ExecutablePath, m_helpReferenceFile );
            }
            set
            {
                m_helpReferenceFile = value;
            }
        }

        [Browsable( false )]
        public string HelpLanguageFileRaw
        {
            get
            {
                return m_helpLanguageFile;
            }
        }

        [DisplayName( "Language Help File" ),
        Description( "The Help File for the Language." ),
        DefaultValue( "" ),
        Category( "Help" ),
        Editor( typeof( FileBrowserEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
        public string HelpLanguageFile
        {
            get
            {
                if ( (m_helpLanguageFile == null) || (m_helpLanguageFile == string.Empty) )
                {
                    return m_helpLanguageFile;
                }

                return Path.Combine( ApplicationSettings.ExecutablePath, m_helpLanguageFile );
            }
            set
            {
                m_helpLanguageFile = value;
            }
        }

        [Browsable( false )]
        public string CodeSnippetPath
        {
            get
            {
                return m_codeSnippetPath;
            }
            set
            {
                m_codeSnippetPath = value;

                m_codeSnippetFolder = null;
                List<string> snippetPaths = EditorSettingsBase.SplitPathsString( m_codeSnippetPath );
                if ( (snippetPaths != null) && (snippetPaths.Count > 0) )
                {
                    foreach ( string snippetPath in snippetPaths )
                    {
                        try
                        {
                            if ( m_codeSnippetFolder == null )
                            {
                                m_codeSnippetFolder = new CodeSnippetFolder( Path.GetFileName( snippetPath ), snippetPath );
                            }
                            else
                            {
                                CodeSnippetFolder folder = new CodeSnippetFolder( Path.GetFileName( snippetPath ), snippetPath );
                                m_codeSnippetFolder.Merge( folder, true );
                            }
                        }
                        catch ( System.Exception )
                        {
                            // don't load folder if error
                        }
                    }
                }
            }
        }

        [DisplayName( "Code Snippet Path" ),
        Description( "" ),
        DefaultValue( "" ),
        TypeConverter( typeof( FolderListConverter ) ),
        Editor( typeof( FolderListBrowserEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
        public List<string> CodeSnippetPaths
        {
            get
            {
                return m_codeSnippetPaths;
            }
            set
            {
                m_codeSnippetPaths = value;
                this.CodeSnippetPath = EditorSettingsBase.ConcatenatePathsString( m_codeSnippetPaths );
            }
        }

        [Browsable( false )]
        public ActiproSoftware.SyntaxEditor.CodeSnippetFolder CodeSnippetFolder
        {
            get
            {
                return m_codeSnippetFolder;
            }
        }
        #endregion

        #region Virtual Functions
        public virtual bool Equals( LanguageTabSettingsBase l )
        {
            if ( l == null )
            {
                return false;
            }

            bool apples2apples = this.GameSpecific && l.GameSpecific;

            // Return true if the fields match:
            return (!apples2apples || (this.CacheDirectory == l.CacheDirectory))
                && (m_helpReferenceFile == l.m_helpReferenceFile)
                && (m_helpLanguageFile == l.m_helpLanguageFile)
                && (this.CodeSnippetPath == l.CodeSnippetPath)
                && (this.Language == l.Language);
        }

        public virtual bool Validate()
        {
            if ( !String.IsNullOrEmpty( this.CacheDirectory ) )
            {
                if ( !Directory.Exists( this.CacheDirectory ) )
                {
                    return false;
                }
            }

            foreach ( string path in this.CodeSnippetPaths )
            {
                if ( !Directory.Exists( path ) )
                {
                    return false;
                }
            }

            if ( !String.IsNullOrEmpty( this.HelpLanguageFile ) )
            {
                if ( !File.Exists( this.HelpLanguageFile ) )
                {
                    return false;
                }
            }

            if ( !String.IsNullOrEmpty( this.HelpReferenceFile ) )
            {
                if ( !File.Exists( this.HelpReferenceFile ) )
                {
                    return false;
                }
            }

            return true;
        }

        public virtual void CopyFrom( OldLanguageTabSettings l )
        {
            if ( l == null )
            {
                return;
            }

            this.m_language = l.Language;
            this.CacheDirectory = l.CacheDirectory;

            if ( this.GameSpecific && l.GameSpecific )
            {
                this.HelpLanguageFile = l.HelpLanguageFileRaw;
                this.HelpReferenceFile = l.HelpReferenceFileRaw;
                this.CodeSnippetPath = l.CodeSnippetPath;
            }
        }

        protected virtual void ResetInternal()
        {

        }

        /// <summary>
        /// Override in a derived class to copy additional data to the <see cref="CompilingTabSettingsBase"/>.
        /// </summary>
        /// <param name="c"></param>
        protected virtual void CopyFromInternal( LanguageTabSettingsBase c )
        {

        }

        protected virtual LanguageTabSettingsBase CloneInternal()
        {
            return null;
        }

        /// <summary>
        /// Override in a derived class to save additional data to the <see cref="XmlTextWriter"/>.
        /// </summary>
        /// <param name="writer"></param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        protected virtual bool SaveXmlInternal( XmlTextWriter writer )
        {
            return true;
        }

        /// <summary>
        /// Override in a derived class to load additional data from the <see cref="XmlTextReader"/>.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        protected virtual bool LoadXmlInternal( XmlTextReader reader )
        {
            return true;
        }
        #endregion

        #region ITabSettings
        [Browsable( false )]
        public string Name
        {
            get
            {
                return "Language Settings";
            }
        }

        /// <summary>
        /// Resets the data in this object to their default values.
        /// </summary>
        public void Reset()
        {
            m_cacheDirectory = string.Empty;

            string error = null;
            string userprefdir = ApplicationSettings.UserPrefDir( out error );
            if ( error == null )
            {
                this.CacheDirectory = Path.Combine( userprefdir, m_language + "IdentifierCache" );
            }
            else
            {
                this.CacheDirectory = Path.Combine( Environment.GetEnvironmentVariable( "TEMP" ), m_language + "IdentifierCache" );
                Console.WriteLine( error );
            }

            this.HelpReferenceFile = string.Empty;
            this.HelpLanguageFile = string.Empty;
            this.CodeSnippetPath = string.Empty;

            ResetInternal();
        }

        /// <summary>
        /// Copies the data from this object to the <see cref="CompilingTabSettingsBase"/>.
        /// </summary>
        /// <param name="c"></param>
        public void CopyFrom( ITabSettings ts )
        {
            LanguageTabSettingsBase l = ts as LanguageTabSettingsBase;
            if ( l == null )
            {
                return;
            }

            this.m_language = l.Language;
            this.CacheDirectory = l.CacheDirectory;

            if ( this.GameSpecific && l.GameSpecific )
            {
                this.HelpLanguageFile = l.m_helpLanguageFile;
                this.HelpReferenceFile = l.m_helpReferenceFile;
                this.CodeSnippetPath = l.CodeSnippetPath;
            }

            CopyFromInternal( l );
        }

        public void CopyFrom( EditorSettingsBase settings )
        {
            this.CopyFrom( settings.LanguageSettings );
        }

        public ITabSettings Clone()
        {
            return CloneInternal();
        }

        /// <summary>
        /// Saves the data from this object to the <see cref="XmlTextWriter"/>.
        /// </summary>
        /// <param name="writer"></param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        public bool SaveXml( XmlTextWriter writer )
        {
            writer.WriteAttributeString( "Language", this.Language );
            writer.WriteAttributeString( "CacheDirectory", this.CacheDirectory );

            if ( this.GameSpecific )
            {
                writer.WriteAttributeString( "HelpReferenceFile", m_helpReferenceFile );
                writer.WriteAttributeString( "HelpLanguageFile", m_helpLanguageFile );
                writer.WriteAttributeString( "CodeSnippets", this.CodeSnippetPath );
            }

            return SaveXmlInternal( writer );
        }

        /// <summary>
        /// Loads the data to the object from the <see cref="XmlTextReader"/>.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
        public bool LoadXml( XmlTextReader reader )
        {
            Reset();

            string language = reader["Language"];
            if ( language != null )
            {
                if ( language.ToLower() != this.Language.ToLower() )
                {
                    return false;
                }
            }

            string cacheDir = reader["CacheDirectory"];
            if ( cacheDir != null )
            {
                this.CacheDirectory = cacheDir;
            }

            string helpRef = reader["HelpReferenceFile"];
            if ( helpRef != null )
            {
                this.HelpReferenceFile = helpRef;
            }

            string helpLang = reader["HelpLanguageFile"];
            if ( helpLang != null )
            {
                this.HelpLanguageFile = helpLang;
            }

            string snippets = reader["CodeSnippets"];
            if ( snippets != null )
            {
                this.CodeSnippetPath = snippets;
            }

            return LoadXmlInternal( reader );
        }
        #endregion

        #region Protected Functions
        #endregion

        #region Overrides
        public override bool Equals( System.Object obj )
        {
            // If parameter is null return false.
            if ( obj == null )
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            LanguageTabSettingsBase l = obj as LanguageTabSettingsBase;
            if ( (System.Object)l == null )
            {
                return false;
            }

            bool apples2apples = this.GameSpecific && l.GameSpecific;

            // Return true if the fields match:
            return (!apples2apples || (this.CacheDirectory == l.CacheDirectory))
                && (m_helpReferenceFile == l.m_helpReferenceFile)
                && (m_helpLanguageFile == l.m_helpLanguageFile)
                && (this.CodeSnippetPath == l.CodeSnippetPath)
                && (this.Language == l.Language);
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.GameSpecific );
            s.Append( this.Language );
            s.Append( m_helpReferenceFile );
            s.Append( m_helpLanguageFile );
            s.Append( this.CodeSnippetPath );
            s.Append( this.CacheDirectory );
            return s.ToString().GetHashCode();
        }
        #endregion
    };

    public class SourceControlTabSettings : ITabSettings
    {
        public SourceControlTabSettings()
        {
            Reset();
        }

        #region Enums
        public enum Provider
        {
            Alienbrain,
            Perforce
        }

        public enum EditAction
        {
            Prompt,
            CheckOut,
            DoNothing
        };

        public enum SaveAction
        {
            Prompt,
            CheckOut,
            SaveAs
        };

        public enum CompileAction
        {
            Prompt,
            CheckOut,
            Overwrite
        };
        #endregion

        #region Properties
        public bool Enabled;
        public string Server;
        public string Project;
        public string LoginName;
        public Provider SourceControlProvider;
        public EditAction OnEdit;
        public SaveAction OnSave;
        public CompileAction OnCompile;
        #endregion

        #region Overrides
        public override bool Equals( System.Object obj )
        {
            // If parameter is null return false.
            if ( obj == null )
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            SourceControlTabSettings s = obj as SourceControlTabSettings;
            if ( (System.Object)s == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (this.SourceControlProvider == s.SourceControlProvider)
                && (this.Enabled == s.Enabled)
                && (this.Server == s.Server)
                && (this.Project == s.Project)
                && (this.LoginName == s.LoginName)
                && (this.OnEdit == s.OnEdit)
                && (this.OnSave == s.OnSave)
                && (this.OnCompile == s.OnCompile);
        }

        public bool Equals( SourceControlTabSettings s )
        {
            // If parameter is null return false:
            if ( (object)s == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (this.SourceControlProvider == s.SourceControlProvider)
                && (this.Enabled == s.Enabled)
                && (this.Server == s.Server)
                && (this.Project == s.Project)
                && (this.LoginName == s.LoginName)
                && (this.OnEdit == s.OnEdit)
                && (this.OnSave == s.OnSave)
                && (this.OnCompile == s.OnCompile);
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.SourceControlProvider );
            s.Append( this.Enabled );
            s.Append( this.Server );
            s.Append( this.Project );
            s.Append( this.LoginName );
            s.Append( this.OnEdit );
            s.Append( this.OnSave );
            s.Append( this.OnCompile );
            return s.ToString().GetHashCode();
        }
        #endregion

        #region ITabSettings
        [Browsable( false )]
        public string Name
        {
            get
            {
                return "Source Control Settings";
            }
        }

        public bool SaveXml( XmlTextWriter writer )
        {
            string provider = null;
            switch ( this.SourceControlProvider )
            {
                case Provider.Alienbrain:
                    provider = "Alienbrain";
                    break;
                case Provider.Perforce:
                    provider = "Perforce";
                    break;
            }

            string edit = null;
            switch ( this.OnEdit )
            {
                case EditAction.Prompt:
                    edit = "Prompt";
                    break;
                case EditAction.CheckOut:
                    edit = "CheckOut";
                    break;
                case EditAction.DoNothing:
                    edit = "DoNothing";
                    break;
            }

            string save = null;
            switch ( this.OnSave )
            {
                case SaveAction.Prompt:
                    save = "Prompt";
                    break;
                case SaveAction.CheckOut:
                    save = "CheckOut";
                    break;
                case SaveAction.SaveAs:
                    save = "SaveAs";
                    break;
            }

            string compile = null;
            switch ( this.OnCompile )
            {
                case CompileAction.Prompt:
                    compile = "Prompt";
                    break;
                case CompileAction.CheckOut:
                    compile = "CheckOut";
                    break;
                case CompileAction.Overwrite:
                    compile = "Overwrite";
                    break;
            }

            writer.WriteAttributeString( "Provider", provider );
            writer.WriteAttributeString( "enabled", this.Enabled.ToString() );
            writer.WriteAttributeString( "editAction", edit );
            writer.WriteAttributeString( "saveAction", save );
            writer.WriteAttributeString( "compileAction", compile );

            writer.WriteStartElement( "Server" );
            writer.WriteString( this.Server );
            writer.WriteEndElement();

            writer.WriteStartElement( "Project" );
            writer.WriteString( this.Project );
            writer.WriteEndElement();

            writer.WriteStartElement( "LoginName" );
            writer.WriteString( this.LoginName );
            writer.WriteEndElement();

            return true;
        }

        public bool LoadXml( XmlTextReader reader )
        {
            string parentName = reader.Name;

            Reset();

            string enable = reader["enabled"];
            if ( enable != null )
            {
                this.Enabled = Convert.ToBoolean( enable );
            }

            string provider = reader["Provider"];
            if ( provider != null )
            {
                provider = provider.ToLower();
                if ( provider == "alienbrain" )
                {
                    this.SourceControlProvider = Provider.Alienbrain;
                }
                else if ( provider == "perforce" )
                {
                    this.SourceControlProvider = Provider.Perforce;
                }
            }

            string edit = reader["editAction"];
            if ( edit != null )
            {
                edit = edit.ToLower();
                if ( edit == "prompt" )
                {
                    this.OnEdit = EditAction.Prompt;
                }
                else if ( edit == "checkout" )
                {
                    this.OnEdit = EditAction.CheckOut;
                }
                else if ( edit == "donothing" )
                {
                    this.OnEdit = EditAction.DoNothing;
                }
            }

            string save = reader["saveAction"];
            if ( save != null )
            {
                save = save.ToLower();
                if ( save == "prompt" )
                {
                    this.OnSave = SaveAction.Prompt;
                }
                else if ( save == "checkout" )
                {
                    this.OnSave = SaveAction.CheckOut;
                }
                else if ( save == "saveas" )
                {
                    this.OnSave = SaveAction.SaveAs;
                }
            }

            string compile = reader["compileAction"];
            if ( compile != null )
            {
                compile = compile.ToLower();
                if ( compile == "prompt" )
                {
                    this.OnCompile = CompileAction.Prompt;
                }
                else if ( compile == "checkout" )
                {
                    this.OnCompile = CompileAction.CheckOut;
                }
                else if ( compile == "overwrite" )
                {
                    this.OnCompile = CompileAction.Overwrite;
                }
            }

            while ( reader.Read() )
            {
                if ( !reader.IsStartElement() || reader.IsEmptyElement )
                {
                    if ( reader.Name == parentName )
                    {
                        break; // we're done
                    }
                }
                else if ( reader.Name == "Server" )
                {
                    this.Server = reader.ReadString();
                }
                else if ( reader.Name == "Project" )
                {
                    this.Project = reader.ReadString();
                }
                else if ( reader.Name == "LoginName" )
                {
                    this.LoginName = reader.ReadString();
                }
            }

            return true;
        }

        public void CopyFrom( ITabSettings ts )
        {
            SourceControlTabSettings s = ts as SourceControlTabSettings;
            if ( s == null )
            {
                return;
            }

            this.SourceControlProvider = s.SourceControlProvider;
            this.Enabled = s.Enabled;
            this.Server = s.Server;
            this.Project = s.Project;
            this.LoginName = s.LoginName;
            this.OnEdit = s.OnEdit;
            this.OnSave = s.OnSave;
            this.OnCompile = s.OnCompile;
        }

        public void CopyFrom( EditorSettingsBase settings )
        {
            this.CopyFrom( settings.SourceControlSettings );
        }

        public ITabSettings Clone()
        {
            SourceControlTabSettings s = new SourceControlTabSettings();
            s.CopyFrom( this );
            return s;
        }

        public void Reset()
        {
            this.SourceControlProvider = Provider.Alienbrain;
            this.Enabled = false;
            this.Server = "";
            this.Project = "";
            this.LoginName = "ReadOnly";
            this.OnEdit = EditAction.DoNothing;
            this.OnSave = SaveAction.Prompt;
            this.OnCompile = CompileAction.Overwrite;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( OldSourceControlTabSettings s )
        {
            if ( s == null )
            {
                return;
            }

            this.SourceControlProvider = (Provider)s.SourceControlProvider;
            this.Enabled = s.Enabled;
            this.Server = s.Server;
            this.Project = s.Project;
            this.LoginName = s.LoginName;
            this.OnEdit = (EditAction)s.OnEdit;
            this.OnSave = (SaveAction)s.OnSave;
            this.OnCompile = (CompileAction)s.OnCompile;
        }

        public string GetSourceControlFileName( string filename, bool fullname )
        {
            if ( this.SourceControlProvider == Provider.Perforce )
            {
                return filename;
            }
            else if ( (this.Server == string.Empty) || (this.Project == string.Empty) )
            {
                return string.Empty;
            }

            // Example: alienbrain://SANRAGE/rage/assets/HDRScene/entity.type

            System.Text.StringBuilder abFileName = new System.Text.StringBuilder();

            if ( fullname )
            {
                abFileName.Append( "alienbrain://" );
                abFileName.Append( this.Server );
                abFileName.Append( "/" );
            }

            abFileName.Append( this.Project );
            abFileName.Append( "/" );

            int indexOf = filename.IndexOf( this.Project );
            abFileName.Append( filename.Substring( indexOf + this.Project.Length + 1 ) );

            return abFileName.ToString().Replace( @"\", "/" );
        }
        #endregion
    }

    public class EditorSettingsBase
    {
        public EditorSettingsBase()
        {
        }

        #region Variables
        protected ProjectTabSettings m_projectSettings = null;
        protected CompilingTabSettingsBase m_compilingSettings = null;
        protected FileTabSettings m_fileSettings = null;
        protected ToolbarTabSettings m_toolbarSettings = null;
        protected LanguageTabSettingsBase m_languageSettings = null;
        protected SourceControlTabSettings m_sourceControlSettings = null;
        protected string m_name = "EditorSettingsBase";
        #endregion

        #region Properties
        public ProjectTabSettings ProjectSettings
        {
            get
            {
                return m_projectSettings;
            }
        }

        public CompilingTabSettingsBase CompilingSettings
        {
            get
            {
                return m_compilingSettings;
            }
            set
            {
                m_compilingSettings = value;
            }
        }

        public FileTabSettings FileSettings
        {
            get
            {
                return m_fileSettings;
            }
        }

        public ToolbarTabSettings ToolbarSettings
        {
            get
            {
                return m_toolbarSettings;
            }
        }

        public LanguageTabSettingsBase LanguageSettings
        {
            get
            {
                return m_languageSettings;
            }
            set
            {
                m_languageSettings = value;
            }
        }

        public SourceControlTabSettings SourceControlSettings
        {
            get
            {
                return m_sourceControlSettings;
            }
        }

        public string Name
        {
            get
            {
                return m_name;
            }
        }
        #endregion

        #region Public Functions
        public bool SaveXml( XmlTextWriter writer )
        {
            if ( ProjectSettings != null )
            {
                writer.WriteStartElement( "ProjectSettings" );
                ProjectSettings.SaveXml( writer );
                writer.WriteEndElement();
            }

            if ( FileSettings != null )
            {
                writer.WriteStartElement( "FileSettings" );
                FileSettings.SaveXml( writer );
                writer.WriteEndElement();
            }

            if ( ToolbarSettings != null )
            {
                writer.WriteStartElement( "ToolbarSettings" );
                ToolbarSettings.SaveXml( writer );
                writer.WriteEndElement();
            }

            if ( CompilingSettings != null )
            {
                writer.WriteStartElement( "CompileSettings" );
                CompilingSettings.SaveXml( writer );
                writer.WriteEndElement();
            }

            if ( LanguageSettings != null )
            {
                writer.WriteStartElement( "LanguageSettings" );
                LanguageSettings.SaveXml( writer );
                writer.WriteEndElement();
            }

            if ( SourceControlSettings != null )
            {
                writer.WriteStartElement( "SourceControlSettings" );
                SourceControlSettings.SaveXml( writer );
                writer.WriteEndElement();
            }

            return true;
        }

        public bool LoadXml( XmlTextReader reader )
        {
            string parentName = reader.Name;

            try
            {

                while ( reader.Read() )
                {
                    if ( !reader.IsStartElement() )
                    {
                        if ( reader.Name == parentName )
                        {
                            break;	// we're done
                        }
                    }
                    else if ( reader.Name == "ProjectSettings" )
                    {
                        if ( !reader.IsEmptyElement && !ProjectSettings.LoadXml( reader ) )
                        {
                            return false;
                        }
                    }
                    else if ( reader.Name == "FileSettings" )
                    {
                        if ( !reader.IsEmptyElement && !FileSettings.LoadXml( reader ) )
                        {
                            return false;
                        }
                    }
                    else if ( reader.Name == "ToolbarSettings" )
                    {
                        if ( !reader.IsEmptyElement && !ToolbarSettings.LoadXml( reader ) )
                        {
                            return false;
                        }
                    }
                    else if ( reader.Name == "CompileSettings" )
                    {
                        if ( !reader.IsEmptyElement && !CompilingSettings.LoadXml( reader ) )
                        {
                            return false;
                        }
                    }
                    else if ( reader.Name == "LanguageSettings" )
                    {
                        if ( !reader.IsEmptyElement && !LanguageSettings.LoadXml( reader ) )
                        {
                            return false;
                        }
                    }
                    else if ( reader.Name == "SourceControlSettings" )
                    {
                        if ( !reader.IsEmptyElement && !SourceControlSettings.LoadXml( reader ) )
                        {
                            return false;
                        }
                    }
                    /*-START Old format kept for compatibility
                    else
                    {
                        if ( reader.Name == "reloadLastProject" )
                        {
                            if ( !reader.IsEmptyElement )
                            {
                                ProjectSettings.ReloadLastProject = Convert.ToBoolean(reader.ReadString());
                            }
                            else
                            {
                                ProjectSettings.ReloadLastProject = true;
                            }
                        }
                        else if ( reader.Name == "reloadLastFiles" )
                        {
                            if ( !reader.IsEmptyElement )
                            {
                                FileSettings.ReloadLastFiles = Convert.ToBoolean(reader.ReadString());
                            }
                            else
                            {
                                FileSettings.ReloadLastFiles = true;
                            }
                        }
                        else if ( reader.Name == "rememberButtonVisiblity" )
                        {
                            if ( !reader.IsEmptyElement )
                            {
                                ToolbarSettings.RememberButtonVisiblity = Convert.ToBoolean(reader.ReadString());
                            }
                            else
                            {
                                ToolbarSettings.RememberButtonVisiblity = true;
                            }
                        }						
                        else if ( reader.Name == "project" )
                        {
                            if ( !reader.IsEmptyElement )
                            {
                                ProjectSettings.LastProject = reader.ReadString();
                            }
                            else
                            {
                                ProjectSettings.LastProject = "";
                            }
                        }
                        else if ( reader.Name == "postCompileCmd" )
                        {
                            if ( !reader.IsEmptyElement )
                            {
                                ProjectSettings.PostCompileCommand = reader.ReadString();
                            }
                            else
                            {
                                ProjectSettings.PostCompileCommand = "";
                            }
                        }
                        else if ( reader.Name == "runCmd" )
                        {
                            if ( !reader.IsEmptyElement )
                            {
                                ProjectSettings.StartDebugCommand = reader.ReadString();
                            }
                            else
                            {
                                ProjectSettings.StartDebugCommand = "";
                            }
                        }
                        else if ( reader.Name == "compiling" )
                        {
                            CompilingSettings.Reset();

                            if ( !reader.IsEmptyElement )
                            {
                                string att = reader.GetAttribute( "override" );
                                if ( att != null )
                                {
                                    CompilingSettings.OverrideDefaultCompileScript = Convert.ToBoolean(att);
                                }
							
                                att = reader.GetAttribute( "debugParser" );
                                if ( att != null )
                                {
                                    CompilingSettings.DebugParser = Convert.ToBoolean(att);
                                }

                                att = reader.GetAttribute( "dumpThreadState" );
                                if ( att != null )
                                {
                                    CompilingSettings.DumpThreadState = Convert.ToBoolean(att);
                                }

                                att = reader.GetAttribute( "displayDisassembly" );
                                if ( att != null )
                                {
                                    CompilingSettings.DisplayDisassembly = Convert.ToBoolean(att);
                                }

                                while ( reader.Read() )
                                {
                                    if ( !reader.IsStartElement() || reader.IsEmptyElement )
                                    {
                                        if ( reader.Name == "compiling" )
                                        {
                                            break;
                                        }
                                    }
                                    else if ( reader.Name == "scriptCompiler" )
                                    {
                                        CompilingSettings.CompilerExecutable = reader.ReadString();
                                    }
                                    else if ( reader.Name == "includeFile" )
                                    {
                                        CompilingSettings.IncludeFile = reader.ReadString();
                                    }
                                    else if ( reader.Name == "includePath" )
                                    {
                                        CompilingSettings.IncludePath = reader.ReadString();
                                    }
                                    else if ( reader.Name == "globals" )
                                    {
                                        CompilingSettings.GlobalsFile = reader.ReadString();
                                    }
                                    else if ( reader.Name == "custom" )
                                    {
                                        CompilingSettings.Custom = reader.ReadString();
                                    }
                                }
                            }
                        }
                        else if ( reader.Name == "HelpReferenceFile" )
                        {
                            LanguageSettings.HelpReferenceFile = reader["name"];
                        }
                        else if ( reader.Name == "HelpLanguageFile" )
                        {
                            LanguageSettings.HelpLanguageFile = reader["name"];
                        }
                        else if ( reader.Name == "CodeSnippets" )
                        {
                            LanguageSettings.CodeSnippetPath = reader["folders"];
                        }
                    }
                    //-STOP*/
                }
            }
            catch ( Exception e )
            {
                Console.WriteLine( "EXCEPTION: " + e.ToString() );
            }
            return true;
        }

        /// <summary>
        /// Saves the xml filename with all of the settings
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool SaveFile( string filename )
        {
            if ( File.Exists( filename ) )
            {
                // make writable
                FileAttributes atts = File.GetAttributes( filename );
                if ( (atts & FileAttributes.ReadOnly) == FileAttributes.ReadOnly )
                {
                    try
                    {
                        File.SetAttributes( filename, atts & (~FileAttributes.ReadOnly) );
                    }
                    catch
                    {
                    }
                }
            }

            XmlTextWriter writer = new XmlTextWriter( filename, null );
            writer.Formatting = Formatting.Indented;

            writer.WriteStartDocument();

            writer.WriteStartElement( this.Name );
            if ( !SaveXml( writer ) )
            {
                writer.Close();
                return false;
            }
            writer.WriteEndDocument();

            writer.Close();

            return true;
        }

        /// <summary>
        /// Load the xml filename with all of the settings
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool LoadFile( string filename )
        {
            XmlTextReader reader = new XmlTextReader( filename );

            while ( reader.Read() )
            {
                if ( !reader.IsStartElement() )
                {
                    if ( reader.Name == this.Name )
                    {
                        break; // we're done
                    }
                }
                else
                {
                    if ( !LoadXml( reader ) )
                    {
                        reader.Close();
                        return false;
                    }
                }
            }

            reader.Close();
            return true;
        }

        public string BuildCompileScriptCommand( string scriptname, string projectFile )
        {
            if ( this.CompilingSettings == null )
            {
                return string.Empty;
            }

            return this.CompilingSettings.BuildCompileScriptCommand( scriptname, projectFile );
        }

        public void CopyFrom( ITabSettings t )
        {
            if ( t is ProjectTabSettings )
            {
                if ( this.ProjectSettings != null )
                {
                    this.ProjectSettings.CopyFrom( t );
                }
            }
            else if ( t is CompilingTabSettingsBase )
            {
                if ( (this.CompilingSettings != null) && (t != null)
                    && this.CompilingSettings.GetType().Equals( t.GetType() ) )
                {
                    this.CompilingSettings.CopyFrom( t );
                }
            }
            else if ( t is FileTabSettings )
            {
                if ( this.FileSettings != null )
                {
                    this.FileSettings.CopyFrom( t );
                }
            }
            else if ( t is ToolbarTabSettings )
            {
                if ( this.ToolbarSettings != null )
                {
                    this.ToolbarSettings.CopyFrom( t );
                }
            }
            else if ( t is LanguageTabSettingsBase )
            {
                if ( (this.LanguageSettings != null) && (t != null)
                    && this.LanguageSettings.GetType().Equals( t.GetType() ) )
                {
                    this.LanguageSettings.CopyFrom( t );
                }
            }
            else if ( t is SourceControlTabSettings )
            {
                if ( this.SourceControlSettings != null )
                {
                    this.SourceControlSettings.CopyFrom( t );
                }
            }
        }

        public void CopyFrom( OldEditorSettingsBase settings )
        {
            if ( this.ProjectSettings != null )
            {
                this.ProjectSettings.CopyFrom( settings.ProjectSettings );
            }

            if ( this.FileSettings != null )
            {
                this.FileSettings.CopyFrom( settings.FileSettings );
            }

            if ( this.CompilingSettings != null )
            {
                this.CompilingSettings.CopyFrom( settings.CompilingSettings );
            }

            if ( this.ToolbarSettings != null )
            {
                this.ToolbarSettings.CopyFrom( settings.ToolbarSettings );
            }

            if ( this.LanguageSettings != null )
            {
                this.LanguageSettings.CopyFrom( settings.LanguageSettings );
            }

            if ( this.SourceControlSettings != null )
            {
                this.SourceControlSettings.CopyFrom( settings.SourceControlSettings );
            }
        }
        #endregion

        #region Virtual Functions
        public virtual void CopyFrom( EditorSettingsBase settings )
        {
            if ( this.ProjectSettings != null )
            {
                this.ProjectSettings.CopyFrom( settings.ProjectSettings );
            }

            if ( this.FileSettings != null )
            {
                this.FileSettings.CopyFrom( settings.FileSettings );
            }

            if ( (this.CompilingSettings != null) && (settings.CompilingSettings != null)
                && this.CompilingSettings.GetType().Equals( settings.CompilingSettings.GetType() ) )
            {
                this.CompilingSettings.CopyFrom( settings.CompilingSettings );
            }

            if ( this.ToolbarSettings != null )
            {
                this.ToolbarSettings.CopyFrom( settings.ToolbarSettings );
            }

            if ( (this.LanguageSettings != null) && (settings.LanguageSettings != null)
                && this.LanguageSettings.GetType().Equals( settings.LanguageSettings.GetType() ) )
            {
                this.LanguageSettings.CopyFrom( settings.LanguageSettings );
            }

            if ( this.SourceControlSettings != null )
            {
                this.SourceControlSettings.CopyFrom( settings.SourceControlSettings );
            }
        }
        #endregion

        #region Public Static Functions
        public static string ConcatenatePathsString( List<string> pathStrings )
        {
            StringBuilder pathBuilder = new StringBuilder();

            if ( pathStrings == null )
            {
                return string.Empty;
            }

            foreach ( string path in pathStrings )
            {
                pathBuilder.Append( path );
                pathBuilder.Append( Path.PathSeparator );
            }

            string pathString = pathBuilder.ToString();
            return pathString.Trim( new char[] { Path.PathSeparator } );
        }

        public static List<string> SplitPathsString( string pathString )
        {
            if ( pathString == null )
            {
                return null;
            }

            pathString = pathString.Replace( "/", "\\" );

            List<string> results = new List<string>();

            // only add valid paths:
            string[] paths = pathString.Split( new char[] { Path.PathSeparator } );
            foreach ( string path in paths )
            {
                string p = path.Trim();
                if ( p != string.Empty )
                {
                    results.Add( p );
                }
            }

            return results;
        }
        #endregion
    }

    /// <summary>
    /// Class for holding the project-specific settings that come from the SettingsForm.cs
    /// </summary>
    public class ProjectEditorSettings : EditorSettingsBase
    {
        public ProjectEditorSettings()
        {
            m_projectSettings = new ProjectTabSettings( true, false );
            m_sourceControlSettings = new SourceControlTabSettings();
            m_name = "NewProjectEditorSettings";
        }

        public void CopyCompilingSettings( CompilingTabSettingsBase c )
        {
            m_compilingSettings = c.Clone() as CompilingTabSettingsBase;
            m_compilingSettings.ProjectSpecific = true;
        }
    }

    /// <summary>
    /// Class for holding all of the settings that are saved as each user's preferences
    /// </summary>
    public class UserEditorSettings : EditorSettingsBase
    {
        public UserEditorSettings()
        {
            m_projectSettings = new ProjectTabSettings( false, false );
            m_fileSettings = new FileTabSettings( false );
            m_toolbarSettings = new ToolbarTabSettings();
            m_sourceControlSettings = new SourceControlTabSettings();
            m_name = "NewEditorSettings";
        }

        #region Public Static Functions
        static public ProcessStartInfo BuildProcessInfoFromCommand( string cmd )
        {
            ProcessStartInfo psInfo = new ProcessStartInfo();

            string program = cmd.Trim();
            string args = null;

            int firstQuote = program.IndexOf( "\"" );
            int firstSpace = program.IndexOf( " " );

            if ( firstQuote == 0 )
            {
                int secondQuote = program.IndexOf( "\"", 1 );
                if ( secondQuote == -1 )
                {
                    return null;
                }

                if ( secondQuote + 1 < program.Length )
                {
                    args = program.Substring( secondQuote + 1 ).Trim();
                    if ( args.Length == 0 )
                    {
                        args = null;
                    }
                }

                program = program.Substring( 1, secondQuote - 1 );
            }
            else if ( firstSpace > -1 )
            {
                args = program.Substring( firstSpace ).Trim();
                if ( args.Length == 0 )
                {
                    args = null;
                }

                program = program.Substring( 0, firstSpace );
            }

            psInfo.FileName = program;
            psInfo.Arguments = args;

            return psInfo;
        }
        #endregion
    }
}
