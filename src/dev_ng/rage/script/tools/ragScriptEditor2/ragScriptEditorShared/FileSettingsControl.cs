using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ragScriptEditorShared
{
    public partial class FileSettingsControl : SettingsControlBase
    {
        public FileSettingsControl()
        {
            InitializeComponent();
        }

        #region Overrides
        public override SettingsBase TabSettings
        {
            get
            {
                FileSettings fileSettings = new FileSettings();
                fileSettings.MiddleClickClose = this.middleClickCloseCheckBox.Checked;
                fileSettings.ParsingServiceEnabled = this.enableSemanticParsingCheckBox.Checked;

                return fileSettings;
            }
            set
            {
                if ( value is FileSettings )
                {
                    m_invokeSettingsChanged = false;

                    FileSettings fileSettings = value as FileSettings;
                    this.middleClickCloseCheckBox.Checked = fileSettings.MiddleClickClose;
                    this.enableSemanticParsingCheckBox.Checked = fileSettings.ParsingServiceEnabled;

                    m_invokeSettingsChanged = true;
                }
            }
        }
        #endregion

        #region Event Handlers
        protected void SettingChanged( object sender, EventArgs e )
        {
            OnSettingsChanged();
        }
        #endregion
    }
}
