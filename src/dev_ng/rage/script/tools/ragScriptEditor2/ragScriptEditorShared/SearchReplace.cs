using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;

using ActiproSoftware.SyntaxEditor;

namespace ragScriptEditorShared
{
    #region Enums
    public enum SearchReplaceType
    {
        FindInFile,
        ReplaceInFile,
        FindInFiles,
        ReplaceInFiles,
        FindAllInFile,
        ReplaceAllInFile,
        FindAllInFiles,
        ReplaceAllInFiles,
        MarkAllInFile,
        MarkAllInFiles
    }

    public enum SearchReplaceResult
    {
        Found,
        Error,
        PastEndOfFile,
        PastStartOfSearch,
        NotFound,
        Cancelled
    };
    #endregion

    #region Helper Classes
    public class SearchReplaceResultData
    {
        public SearchReplaceResultData( int line, int index, string text, bool found, bool pastStartOfSearch, bool pastEndOfFile, int endOffset )
        {
            m_line = line;
            m_index = index;
            m_text = text;
            m_found = found;
            m_pastStartOfSearch = pastStartOfSearch;
            m_pastEndOfFile = pastEndOfFile;
            m_endOffset = endOffset;
        }

        #region Variables
        private int m_line;
        private int m_index;
        private string m_text;
        private bool m_found = false;
        private bool m_pastStartOfSearch = false;
        private bool m_pastEndOfFile = false;
        private int m_endOffset = 0;
        #endregion

        #region Properties
        public int Line
        {
            get
            {
                return m_line;
            }
        }

        public int Index
        {
            get
            {
                return m_index;
            }
        }

        public string Text
        {
            get
            {
                return m_text;
            }
            set
            {
                m_text = value;
            }
        }

        public bool Found
        {
            get
            {
                return m_found;
            }
        }

        public bool PastStartOfSearch
        {
            get
            {
                return m_pastStartOfSearch;
            }
            set
            {
                m_pastStartOfSearch = value;
            }
        }

        public bool PastEndOfFile
        {
            get
            {
                return m_pastEndOfFile;
            }
            set
            {
                m_pastEndOfFile = value;
            }
        }

        public int EndOffset
        {
            get
            {
                return m_endOffset;
            }
            set
            {
                m_endOffset = value;
            }
        }
        #endregion
    }

    public abstract class SearchFile : IDisposable
    {
        ~SearchFile()
        {
            Dispose( false );
        }

        #region Properties
        public abstract string Filename { get; }
        public abstract bool IsOpen { get; }
        #endregion

        #region Functions
        public abstract string GetSelectedText( int offset );
        public abstract SearchReplaceResultData FindNext( FindReplaceOptions options, int startOffset );
        public abstract SearchReplaceResultData ReplaceNext( FindReplaceOptions options, int startOffset );
        public abstract List<SearchReplaceResultData> FindAll( FindReplaceOptions options );
        public abstract List<SearchReplaceResultData> ReplaceAll( FindReplaceOptions options );
        public abstract List<SearchReplaceResultData> MarkAll( FindReplaceOptions options, bool markWithBookmarks );

        public void Dispose()
        {
            Dispose( true );
            GC.SuppressFinalize( this );
        }
        #endregion

        #region Protected Functions
        protected string GetSelectedText( DocumentLine line, int offset )
        {
            string lineText = line.Text;

            int colStart = offset - line.StartOffset;
            int colEnd = colStart;

            if ( colStart > lineText.Length )
            {
                // used to happen when intellisense was active
                return null;
            }
            else if ( (colStart == lineText.Length) || Char.IsWhiteSpace( lineText[colStart] ) )
            {
                // allow us to look behind
                --colStart;
                --colEnd;
            }

            if ( (lineText.Length > 0) && (colStart >= 0)
                && (Char.IsLetterOrDigit( lineText[colStart] ) || Char.IsSymbol( lineText[colStart] ) || (lineText[colStart] == '_')) )
            {
                while ( (colStart >= 0)
                    && (Char.IsLetterOrDigit( lineText[colStart] ) || Char.IsSymbol( lineText[colStart] ) || (lineText[colStart] == '_')) )
                {
                    --colStart;
                }

                while ( (colEnd < lineText.Length)
                    && (Char.IsLetterOrDigit( lineText[colEnd] ) || Char.IsSymbol( lineText[colEnd] ) || (lineText[colEnd] == '_')) )
                {
                    ++colEnd;
                }

                return lineText.Substring( colStart + 1, colEnd - colStart - 1 );
            }

            return null;
        }

        protected int GetColumnIndex( DocumentLine line, int offset )
        {
            int index = offset - line.StartOffset;
            return GetColumnIndex( line.Text, index );
        }

        protected int GetColumnIndex( string line, int index )
        {
            int colIndex = index;

            for ( int i = 0; i < index; ++i )
            {
                if ( line[i] == '\t' )
                {
                    colIndex += (4 - (i % 4));
                }
            }

            return colIndex - 1;    // make it 0-based
        }

        protected abstract void Dispose( bool disposing );
        #endregion
    }

    public class SearchOpenFile : SearchFile
    {
        public SearchOpenFile( SyntaxEditor editor )
        {
            m_editor = editor;
        }

        #region Variables
        private SyntaxEditor m_editor;
        private bool m_disposed = false;
        #endregion

        #region Properties
        public override string Filename
        {
            get
            {
                return m_editor.Document.Filename.ToLower();
            }
        }

        public override bool IsOpen
        {
            get
            {
                return true;
            }
        }

        public SyntaxEditor Editor
        {
            get
            {
                return m_editor;
            }
        }
        #endregion

        #region Public Functions
        public override string GetSelectedText( int offset )
        {
            string selectedText = this.Editor.SelectedView.SelectedText;
            if ( selectedText.Length > 0 )
            {
                if ( selectedText.IndexOf( "\n" ) < 0 )
                {
                    return selectedText;
                }
            }
            else
            {
                return GetSelectedText( this.Editor.SelectedView.CurrentDocumentLine, this.Editor.SelectedView.Selection.StartOffset );
            }

            return null;
        }

        public override SearchReplaceResultData FindNext( FindReplaceOptions options, int startOffset )
        {
            FindReplaceResultSet resultSet = m_editor.SelectedView.FindReplace.Find( options );
            if ( resultSet != null )
            {
                int line = -1;
                int index = -1;
                string text = string.Empty;
                if ( resultSet.Count > 0 )
                {
                    FindReplaceResult result = resultSet[0];
                    line = m_editor.Document.Lines.IndexOf( result.StartOffset );
                    if ( line != -1 )
                    {
                        index = GetColumnIndex( m_editor.Document.Lines[line], result.StartOffset );
                        text = m_editor.Document.Lines[line].Text;
                    }
                }

                return new SearchReplaceResultData( line, index, text, resultSet.Count > 0, 
                    resultSet.PastSearchStartOffset, resultSet.PastDocumentEnd, 
                    (resultSet.Count > 0) ? resultSet[resultSet.Count - 1].EndOffset : 0 );
            }

            return new SearchReplaceResultData( -1, -1, string.Empty, false, false, false, 0 );
        }

        public override SearchReplaceResultData ReplaceNext( FindReplaceOptions options, int startOffset )
        {
            FindReplaceResultSet resultSet = m_editor.SelectedView.FindReplace.Replace( options );
            if ( resultSet != null )
            {
                int line = -1;
                int index = -1;
                string text = string.Empty;
                if ( resultSet.Count > 0 )
                {
                    FindReplaceResult result = resultSet[0];
                    line = m_editor.Document.Lines.IndexOf( result.StartOffset );
                    if ( line != -1 )
                    {
                        index = GetColumnIndex( m_editor.Document.Lines[line], result.StartOffset );
                        text = m_editor.Document.Lines[line].Text;
                    }
                }

                return new SearchReplaceResultData( line, index, text, resultSet.Count > 0, resultSet.PastSearchStartOffset, resultSet.PastDocumentEnd,
                    (resultSet.Count > 0) ? resultSet[resultSet.Count - 1].EndOffset : 0 );
            }

            return new SearchReplaceResultData( -1, -1, string.Empty, false, false, false, 0 );
        }

        public override List<SearchReplaceResultData> FindAll( FindReplaceOptions options )
        {
            List<SearchReplaceResultData> results = new List<SearchReplaceResultData>();

            FindReplaceResultSet resultSet = m_editor.Document.FindReplace.FindAll( options );
            if ( resultSet != null )
            {
                foreach ( FindReplaceResult result in resultSet )
                {
                    int line = m_editor.Document.Lines.IndexOf( result.StartOffset );
                    int index = -1;
                    string text = string.Empty;
                    if ( line != -1 )
                    {
                        index = GetColumnIndex( m_editor.Document.Lines[line], result.StartOffset );
                        text = m_editor.Document.Lines[line].Text;
                    }

                    results.Add( new SearchReplaceResultData( line, index, text, resultSet.Count > 0, 
                        resultSet.PastSearchStartOffset, resultSet.PastDocumentEnd,
                        (resultSet.Count > 0) ? resultSet[resultSet.Count - 1].EndOffset : 0 ) );
                }
            }

            return results;
        }        

        public override List<SearchReplaceResultData> ReplaceAll( FindReplaceOptions options )
        {
            List<SearchReplaceResultData> results = new List<SearchReplaceResultData>();

            FindReplaceResultSet resultSet = m_editor.SelectedView.FindReplace.ReplaceAll( options );
            if ( resultSet != null )
            {
                foreach ( FindReplaceResult result in resultSet )
                {
                    int line = m_editor.Document.Lines.IndexOf( result.StartOffset );
                    int index = -1;
                    string text = string.Empty;
                    if ( line != -1 )
                    {
                        index = GetColumnIndex( m_editor.Document.Lines[line], result.StartOffset );
                        text = m_editor.Document.Lines[line].Text;
                    }

                    results.Add( new SearchReplaceResultData( line, index, text, resultSet.Count > 0,
                        resultSet.PastSearchStartOffset, resultSet.PastDocumentEnd,
                        (resultSet.Count > 0) ? resultSet[resultSet.Count - 1].EndOffset : 0 ) );
                }
            }

            return results;
        }

        public override List<SearchReplaceResultData> MarkAll( FindReplaceOptions options, bool markWithBookmarks )
        {
            List<SearchReplaceResultData> results = new List<SearchReplaceResultData>();

            FindReplaceResultSet resultSet = null;
            if ( markWithBookmarks )
            {
                resultSet = m_editor.SelectedView.FindReplace.MarkAll( options );
            }
            else
            {
                resultSet = m_editor.SelectedView.FindReplace.MarkAll( options, typeof( GrammarErrorSpanIndicator ) );

            }

            if ( resultSet != null )
            {
                foreach ( FindReplaceResult result in resultSet )
                {
                    int line = m_editor.Document.Lines.IndexOf( result.StartOffset );
                    int index = -1;
                    string text = string.Empty;
                    if ( line != -1 )
                    {
                        index = GetColumnIndex( m_editor.Document.Lines[line], result.StartOffset );
                        text = m_editor.Document.Lines[line].Text;
                    }

                    results.Add( new SearchReplaceResultData( line, index, text, resultSet.Count > 0,
                        resultSet.PastSearchStartOffset, resultSet.PastDocumentEnd,
                        (resultSet.Count > 0) ? resultSet[resultSet.Count - 1].EndOffset : 0 ) );
                }
            }

            return results;
        }
        #endregion

        #region Protected Functions
        protected override void Dispose( bool disposing )
        {
            if ( !m_disposed )
            {
                if ( disposing )
                {
                    // cleanup managed resources
                }

                // cleanup unmanaged resources
                m_editor = null;
            }

            m_disposed = true;
        }
        #endregion
    }

    public class SearchClosedFile : SearchFile
    {
        /// <summary>
        /// Constructor for a closed document search/replace
        /// </summary>
        /// <param name="filename"></param>
        public SearchClosedFile( string filename )
        {
            m_filename = filename;
        }

        #region Variables
        private string m_filename;
        private Document m_document = null;
        private StringBuilder m_text = null;
        private bool m_disposed = false;
        #endregion

        #region Properties
        public override string Filename
        {
            get
            {
                return m_filename;
            }
        }

        public override bool IsOpen
        {
            get
            {
                return false;
            }
        }

        public Document Document
        {
            get
            {
                return m_document;
            }
        }

        public string Text
        {
            get
            {
                if ( m_document != null )
                {
                    return m_document.Text;
                }
                else if ( m_text != null )
                {
                    return m_text.ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        #endregion

        #region Public Functions
        public override string GetSelectedText( int offset )
        {
            if ( m_document == null )
            {
                m_document = new Document();
                m_document.LoadFile( m_filename );
            }

            int indexOf = m_document.Lines.IndexOf( offset );
            if ( indexOf != -1 )
            {
                return GetSelectedText( m_document.Lines[indexOf], offset );
            }

            return null;
        }

        public override SearchReplaceResultData FindNext( FindReplaceOptions options, int startOffset )
        {
            if ( options.SearchType == FindReplaceSearchType.Normal )
            {
                return FindReplaceNext( false, options, startOffset );
            }
            else
            {
                if ( m_document == null )
                {
                    m_document = new Document();
                    m_document.LoadFile( m_filename );
                }

                FindReplaceResultSet resultSet = m_document.FindReplace.Find( options, startOffset );
                if ( resultSet != null )
                {
                    int line = -1;
                    int index = -1;
                    string text = string.Empty;
                    if ( resultSet.Count > 0 )
                    {
                        FindReplaceResult result = resultSet[0];
                        line = m_document.Lines.IndexOf( result.StartOffset );
                        if ( line != -1 )
                        {
                            index = GetColumnIndex( m_document.Lines[line], result.StartOffset );
                            text = m_document.Lines[line].Text;
                        }
                    }

                    return new SearchReplaceResultData( line, index, text, resultSet.Count > 0,
                        resultSet.PastSearchStartOffset, resultSet.PastDocumentEnd,
                        (resultSet.Count > 0) ? resultSet[resultSet.Count - 1].EndOffset : 0 );
                }
            }

            return new SearchReplaceResultData( -1, -1, string.Empty, false, false, false, 0 );
        }

        public override SearchReplaceResultData ReplaceNext( FindReplaceOptions options, int startOffset )
        {
            if ( options.SearchType == FindReplaceSearchType.Normal )
            {
                return FindReplaceNext( true, options, startOffset );
            }
            else
            {
                if ( m_document == null )
                {
                    m_document = new Document();
                    m_document.LoadFile( m_filename );
                }

                // first find it
                FindReplaceResultSet resultSet = m_document.FindReplace.Find( options, startOffset );
                if ( resultSet == null )
                {
                    return new SearchReplaceResultData( -1, -1, string.Empty, false, true, false, 0 );
                }
                else if ( resultSet.Count == 0 )
                {
                    return new SearchReplaceResultData( -1, -1, string.Empty, false, resultSet.PastSearchStartOffset, resultSet.PastDocumentEnd, 0 );
                }

                // then replace it
                resultSet = m_document.FindReplace.Replace( options, resultSet[0] );
                if ( resultSet != null )
                {
                    int line = -1;
                    int index = -1;
                    string text = string.Empty;
                    if ( resultSet.Count > 0 )
                    {
                        FindReplaceResult result = resultSet[0];
                        line = m_document.Lines.IndexOf( result.StartOffset );
                        if ( line != -1 )
                        {
                            index = GetColumnIndex( m_document.Lines[line], result.StartOffset );
                            text = m_document.Lines[line].Text;
                        }
                    }

                    return new SearchReplaceResultData( line, index, text, resultSet.Count > 0,
                        resultSet.PastSearchStartOffset, resultSet.PastDocumentEnd,
                        (resultSet.Count > 0) ? resultSet[resultSet.Count - 1].EndOffset : 0 );
                }
            }

            return new SearchReplaceResultData( -1, -1, string.Empty, false, false, false, 0 );
        }

        public override List<SearchReplaceResultData> FindAll( FindReplaceOptions options )
        {
            List<SearchReplaceResultData> results = new List<SearchReplaceResultData>();

            if ( options.SearchType == FindReplaceSearchType.Normal )
            {
                results = FindReplaceAll( false, options );
            }
            else
            {
                if ( m_document == null )
                {
                    m_document = new Document();
                    m_document.LoadFile( m_filename );
                }

                FindReplaceResultSet resultSet = m_document.FindReplace.FindAll( options );
                if ( resultSet != null )
                {
                    foreach ( FindReplaceResult result in resultSet )
                    {
                        int line = m_document.Lines.IndexOf( result.StartOffset );
                        int index = -1;
                        string text = string.Empty;
                        if ( line != -1 )
                        {
                            index = GetColumnIndex( m_document.Lines[line], result.StartOffset );
                            text = m_document.Lines[line].Text;
                        }

                        results.Add( new SearchReplaceResultData( line, index, text, resultSet.Count > 0,
                            resultSet.PastSearchStartOffset, resultSet.PastDocumentEnd,
                            (resultSet.Count > 0) ? resultSet[resultSet.Count - 1].EndOffset : 0 ) );
                    }
                }
            }

            return results;
        }

        public override List<SearchReplaceResultData> ReplaceAll( FindReplaceOptions options )
        {
            List<SearchReplaceResultData> results = new List<SearchReplaceResultData>();

            if ( options.SearchType == FindReplaceSearchType.Normal )
            {
                results = FindReplaceAll( true, options );
            }
            else
            {
                if ( m_document == null )
                {
                    m_document = new Document();
                    m_document.LoadFile( m_filename );
                }

                FindReplaceResultSet resultSet = m_document.FindReplace.ReplaceAll( options );
                if ( resultSet != null )
                {
                    foreach ( FindReplaceResult result in resultSet )
                    {
                        int line = m_document.Lines.IndexOf( result.StartOffset );
                        int index = -1;
                        string text = string.Empty;
                        if ( line != -1 )
                        {
                            index = GetColumnIndex( m_document.Lines[line], result.StartOffset );
                            text = m_document.Lines[line].Text;
                        }

                        results.Add( new SearchReplaceResultData( line, index, text, resultSet.Count > 0,
                            resultSet.PastSearchStartOffset, resultSet.PastDocumentEnd,
                            (resultSet.Count > 0) ? resultSet[resultSet.Count - 1].EndOffset : 0 ) );
                    }
                }
            }

            return results;
        }

        public override List<SearchReplaceResultData> MarkAll( FindReplaceOptions options, bool markWithBookmarks )
        {
            List<SearchReplaceResultData> results = new List<SearchReplaceResultData>();

            if ( options.SearchType == FindReplaceSearchType.Normal )
            {
                results = FindReplaceAll( false, options );
            }
            else
            {
                if ( m_document == null )
                {
                    m_document = new Document();
                    m_document.LoadFile( m_filename );
                }

                FindReplaceResultSet resultSet = null;
                if ( markWithBookmarks )
                {
                    resultSet = m_document.FindReplace.MarkAll( options );
                }
                else
                {
                    resultSet = m_document.FindReplace.MarkAll( options, typeof( GrammarErrorSpanIndicator ) );
                }

                if ( resultSet != null )
                {
                    foreach ( FindReplaceResult result in resultSet )
                    {
                        int line = m_document.Lines.IndexOf( result.StartOffset );
                        int index = -1;
                        string text = string.Empty;
                        if ( line != -1 )
                        {
                            index = GetColumnIndex( m_document.Lines[line], result.StartOffset );
                            text = m_document.Lines[line].Text;
                        }

                        results.Add( new SearchReplaceResultData( line, index, text, resultSet.Count > 0,
                            resultSet.PastSearchStartOffset, resultSet.PastDocumentEnd,
                            (resultSet.Count > 0) ? resultSet[resultSet.Count - 1].EndOffset : 0 ) );
                    }
                }
            }

            return results;
        }
        #endregion

        #region Protected Functions
        protected override void Dispose( bool disposing )
        {
            if ( !m_disposed )
            {
                if ( disposing )
                {
                    // cleanup managed resources
                }

                // cleanup unmanaged resources
                if ( (m_document != null) && !m_document.IsDisposed )
                {
                    m_document.Dispose();
                    m_document = null;
                }
            }

            m_disposed = true;
        }
        #endregion

        #region Private Functions
        private void LoadText( out List<string> lines, out List<int> offsets )
        {
            lines = new List<string>();
            offsets = new List<int>();

            // first, read in the file line by line and record the offsets
            StreamReader reader = null;
            try
            {
                using (reader = new StreamReader(m_filename))
                {
                    int offset = 0;
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        lines.Add(line);
                        offsets.Add(offset);

                        offset += line.Length + 2;  // add 2 for "\r\n";

                        line = reader.ReadLine();
                    }

                    offsets.Add(offset);
                }
            }
            catch ( Exception e )
            {
                ApplicationSettings.Log.ToolException( e, "Exception attempting to read {0}", m_filename );
            }
        }

        private bool DetermineStartPosition( int startOffset, bool searchUp, List<int> offsets, out int lineIndex, out int startIndex )
        {
            if ( searchUp && (startOffset == 0) )
            {
                if ( offsets.Count >= 2 )
                {
                    lineIndex = offsets.Count - 2;  // we have one more offset than lines
                    startIndex = offsets[offsets.Count - 1] - offsets[offsets.Count - 2] - 3;    // last position in the last line (minus "\r\n")
                    return true;
                }

                lineIndex = -1;
                startIndex = -1;
                return false;
            }

            lineIndex = -1;
            startIndex = -1;
            for ( int i = 1; i < offsets.Count; ++i )
            {
                if ( (startOffset >= offsets[i - 1]) && (startOffset < offsets[i]) )
                {
                    lineIndex = i - 1;
                    startIndex = startOffset - offsets[lineIndex];
                    break;
                }
            }

            return (lineIndex >= 0) && (startIndex >= 0);
        }

        private void SaveText( List<string> lines )
        {
            m_text = new StringBuilder();

            foreach ( string line in lines )
            {
                m_text.Append( line );
                m_text.Append( "\r\n" );
            }
        }

        private SearchReplaceResultData FindReplaceOne( bool replace, FindReplaceOptions options, List<string> lines, List<int> offsets,
            int lineIndex, int startIndex )
        {
            StringComparison comp = options.MatchCase ? StringComparison.CurrentCulture : StringComparison.OrdinalIgnoreCase;

            if ( lines.Count > 0 )
            {
                int i = lineIndex;
                do
                {
                    string line = lines[i];

                    int index = startIndex;
                    if ( i != lineIndex )
                    {
                        // after the first time, we can search each line from its beginning or end, depending on the direction of the search
                        index = options.SearchUp ? line.Length - 1 : 0;
                    }

                    int foundAtIndex = -1;
                    if ( options.SearchUp )
                    {
                        foundAtIndex = line.LastIndexOf( options.FindText, index, comp);
                    }
                    else
                    {
                        foundAtIndex = line.IndexOf( options.FindText, index, comp);
                    }

                    if ( (foundAtIndex != -1) && options.MatchWholeWord )
                    {
                        // Check character before and after the substring.
                        // If either one is an alpha-numeric character, then don't count this as a match.

                        int indexBefore = foundAtIndex - 1;
                        int indexAfter = foundAtIndex + options.FindText.Length;

                        if ( ((indexBefore >= 0)
                            && ((char.IsLetterOrDigit( line, indexBefore ) || char.IsSymbol( line, indexBefore )) || (line[indexBefore] == '_')))
                            || ((indexAfter < line.Length)
                            && ((char.IsLetterOrDigit( line, indexAfter ) || char.IsSymbol( line, indexAfter )) || (line[indexAfter] == '_'))) )
                        {
                            foundAtIndex = -1;
                        }
                    }

                    if ( foundAtIndex != -1 )
                    {
                        if ( replace )
                        {
                            // add 2 for "\r\n";
                            int orginalLength = lines[i].Length + 2;
                            lines[i] = lines[i].Remove( foundAtIndex, options.FindText.Length );
                            lines[i] = lines[i].Insert( foundAtIndex, options.ReplaceText );

                            // add 2 for "\r\n";
                            int newlength = lines[i].Length + 2;

                            // adjust the offsets
                            int offset = newlength - orginalLength;
                            if ( offset != 0 )
                            {
                                for ( int j = i + 1; j < offsets.Count; ++j )
                                {
                                    offsets[j] += offset;
                                }
                            }
                        }

                        int endOffset = offsets[i] + foundAtIndex;
                        if ( !options.SearchUp )
                        {
                            endOffset += replace ? options.ReplaceText.Length : options.FindText.Length;
                        }

                        int colIndex = GetColumnIndex( lines[i], foundAtIndex );

                        return new SearchReplaceResultData( i, colIndex, lines[i], true, false, false, endOffset );
                    }

                    i += options.SearchUp ? -1 : 1;
                } while (options.SearchUp ? i >= 0 : i < lines.Count);
            }

            return new SearchReplaceResultData( -1, -1, string.Empty, false, false, false, 0 );
        }

        private SearchReplaceResultData FindReplaceNext( bool replace, FindReplaceOptions options, int startOffset )
        {
            List<string> lines;
            List<int> offsets;
            LoadText( out lines, out offsets );

            int lineIndex;
            int startIndex;            
            if ( !DetermineStartPosition( startOffset, options.SearchUp, offsets, out lineIndex, out startIndex ) )
            {
                return new SearchReplaceResultData( -1, -1, string.Empty, false, false, false, 0 );
            }

            SearchReplaceResultData resultData = FindReplaceOne( replace, options, lines, offsets, lineIndex, startIndex );
            if ( (resultData != null) && replace && resultData.Found )
            {
                SaveText( lines );
            }

            return resultData;
        }

        private List<SearchReplaceResultData> FindReplaceAll( bool replace, FindReplaceOptions options )
        {
            List<SearchReplaceResultData> results = new List<SearchReplaceResultData>();

            List<string> lines;
            List<int> offsets;
            LoadText( out lines, out offsets );

            int lineIndex;
            int startIndex;
            if ( !DetermineStartPosition( 0, options.SearchUp, offsets, out lineIndex, out startIndex ) )
            {
                results.Add( new SearchReplaceResultData( -1, -1, string.Empty, false, false, false, 0 ) );
                return results;
            }

            SearchReplaceResultData resultData = null;
            while ( true )
            {
                resultData = FindReplaceOne( replace, options, lines, offsets, lineIndex, startIndex );
                if ( resultData.Found )
                {
                    if ( results.Count > 0 )
                    {
                        SearchReplaceResultData firstResultData = results[0];
                        if ( (resultData.Line == firstResultData.Line) && (resultData.EndOffset == firstResultData.EndOffset) )
                        {
                            // we've come full circle
                            break;
                        }                       
                    }

                    results.Add( resultData );

                    // determine where to go next
                    lineIndex = resultData.Line;
                    startIndex = resultData.EndOffset - offsets[lineIndex];
                }
                else
                {
                    break;
                }
            }

            if ( (results.Count > 0) && replace && results[0].Found )
            {
                SaveText( lines );
            }

            return results;
        }
        #endregion
    }

    public class SearchReplaceBackgroundWorkerArgument
    {
        public SearchReplaceBackgroundWorkerArgument( List<string> filenames, string currentFilename,
            FindReplaceOptions options, bool openFile, bool markWithBookmarks )
        {
            m_filenames.AddRange( filenames );
            m_currentFilename = currentFilename;
            m_options = options;
            m_openFile = openFile;
            m_markWithBookmarks = markWithBookmarks;
        }

        public SearchReplaceBackgroundWorkerArgument( string directory, bool recurseSubDirectories, string searchPattern, string currentFilename,
            FindReplaceOptions options, bool openFile, bool markWithBookmarks )
        {
            m_directory = directory.ToLower();
            m_recurseSubDirectories = recurseSubDirectories;

            if ( String.IsNullOrEmpty( searchPattern ) )
            {
                m_extensions.Add( "*.*" );
            }
            else
            {
                m_extensions.AddRange( searchPattern.Split( new char[] { ';' } ) );
            }

            m_currentFilename = currentFilename.ToLower();
            m_options = options;
            m_openFile = openFile;
            m_markWithBookmarks = markWithBookmarks;
        }

        #region Variables
        private List<string> m_filenames = new List<string>();
        private string m_directory = string.Empty;
        private bool m_recurseSubDirectories = true;
        private List<string> m_extensions = new List<string>();
        private string m_currentFilename = string.Empty;
        private FindReplaceOptions m_options;
        private bool m_openFile;
        private bool m_markWithBookmarks;
        #endregion

        #region Properties
        public bool IsDirectorySearchReplace
        {
            get
            {
                return !String.IsNullOrEmpty( this.Directory );
            }
        }

        public List<string> Filenames
        {
            get
            {
                return m_filenames;
            }
        }

        public string Directory
        {
            get
            {
                return m_directory;
            }
        }

        public bool RecurseSubDirectories
        {
            get
            {
                return m_recurseSubDirectories;
            }
        }

        public List<string> Extensions
        {
            get
            {
                return m_extensions;
            }
        }

        public string CurrentFilename
        {
            get
            {
                return m_currentFilename;
            }
        }

        public FindReplaceOptions FindReplaceOptions
        {
            get
            {
                return m_options;
            }
        }

        public bool OpenFile
        {
            get
            {
                return m_openFile;
            }
        }

        public bool MarkWithBookmarks
        {
            get
            {
                return m_markWithBookmarks;
            }
        }
        #endregion
    }
    #endregion

    #region Event Args
    public class SearchReplaceFilenameEventArgs : EventArgs
    {
        public SearchReplaceFilenameEventArgs()
        {

        }

        #region Variables
        private string m_filename;
        #endregion

        #region Properties
        public string Filename
        {
            get
            {
                return m_filename;
            }
            set
            {
                m_filename = value;
            }
        }
        #endregion
    }

    public delegate void FindCurrentFilenameEventHandler( object sender, SearchReplaceFilenameEventArgs e );

    public class SearchReplaceFilenamesEventArgs : EventArgs
    {
        public SearchReplaceFilenamesEventArgs()
        {

        }

        #region Variables
        private List<string> m_filenames = new List<string>();
        #endregion

        #region Properties
        public List<string> Filenames
        {
            get
            {
                return m_filenames;
            }
            set
            {
                m_filenames.Clear();
                if ( value != null )
                {
                    m_filenames.AddRange( value );
                }
            }
        }
        #endregion
    }

    public delegate void FindOpenFilenamesEventHandler( object sender, SearchReplaceFilenamesEventArgs e );
    public delegate void FindProjectFilenamesEventHandler( object sender, SearchReplaceFilenamesEventArgs e );

    public class SearchReplaceLoadFileEventArgs : EventArgs
    {
        public SearchReplaceLoadFileEventArgs( string filename )
        {
            m_filename = filename;
        }

        #region Variables
        private string m_filename;
        private bool m_success = true;
        #endregion

        #region Properties
        public string Filename
        {
            get
            {
                return m_filename;
            }
        }

        public bool Success
        {
            get
            {
                return m_success;
            }
            set
            {
                m_success = value;
            }
        }
        #endregion
    }

    public delegate void LoadFileEventHandler( object sender, SearchReplaceLoadFileEventArgs e );

    public class SearchReplaceSaveTextEventArgs : EventArgs
    {
        public SearchReplaceSaveTextEventArgs( string text, string filename )
        {
            m_text = text;
            m_filename = filename;
        }

        #region Variables
        private string m_text;
        private string m_filename;
        private bool m_success = true;
        #endregion

        #region Properties
        public string Text
        {
            get
            {
                return m_text;
            }
        }

        public string Filename
        {
            get
            {
                return m_filename;
            }
        }

        public bool Success
        {
            get
            {
                return m_success;
            }
            set
            {
                m_success = value;
            }
        }
        #endregion
    }

    public delegate void SaveDocumentEventHandler( object sender, SearchReplaceSaveTextEventArgs e );

    public class SearchReplaceSyntaxEditorEventArgs : EventArgs
    {
        public SearchReplaceSyntaxEditorEventArgs( string filename )
        {
            m_filename = filename;
        }

        #region Variables
        private string m_filename;
        private SyntaxEditor m_syntaxEditor;
        #endregion

        #region Properties
        public string Filename
        {
            get
            {
                return m_filename;
            }
        }

        public SyntaxEditor Editor
        {
            get
            {
                return m_syntaxEditor;
            }
            set
            {
                m_syntaxEditor = value;
            }
        }
        #endregion
    }

    public delegate void SyntaxEditorEventHandler( object sender, SearchReplaceSyntaxEditorEventArgs e );

    public class SearchReplaceEventArgs : EventArgs
    {
        public SearchReplaceEventArgs( SearchReplaceType searchType )
        {
            m_searchType = searchType;
        }

        #region Variables
        private SearchReplaceType m_searchType;
        #endregion

        #region Properties
        public SearchReplaceType Search
        {
            get
            {
                return m_searchType;
            }
        }
        #endregion
    }

    public delegate void SearchBeginEventHandler( object sender, SearchReplaceEventArgs e );

    public class SearchCompleteEventArgs : SearchReplaceEventArgs
    {
        public SearchCompleteEventArgs( int numFilesSearched, int numFilesFound, int numFound, SearchReplaceResult result,
            string resultMessage, string resultTitle, SearchReplaceType searchType )
            : base( searchType )
        {
            m_numFilesSearched = numFilesSearched;
            m_numFilesFound = numFilesFound;
            m_numFound = numFound;
            m_result = result;
            m_resultMessage = resultMessage;
            m_resultTitle = resultTitle;
        }

        #region Variables
        private int m_numFilesSearched;
        private int m_numFilesFound;
        private int m_numFound;
        private SearchReplaceResult m_result;
        private string m_resultMessage;
        private string m_resultTitle;
        #endregion

        #region Properties
        public int NumFilesSearched
        {
            get
            {
                return m_numFilesSearched;
            }
        }

        public int NumFilesFound
        {
            get
            {
                return m_numFilesFound;
            }
        }
        
        public int NumFound
        {
            get
            {
                return m_numFound;
            }
        }

        public SearchReplaceResult Result
        {
            get
            {
                return m_result;
            }
        }
        
        public string ResultMessage
        {
            get
            {
                return m_resultMessage;
            }
        }

        public string ResultTitle
        {
            get
            {
                return m_resultTitle;
            }
        }
        #endregion
    }

    public delegate void SearchCompleteEventHandler( object sender, SearchCompleteEventArgs e );

    public class SearchReplaceFoundStringEventArgs : SearchReplaceEventArgs
    {
        public SearchReplaceFoundStringEventArgs( string filename, int line, int index, string text, SearchReplaceType searchType )
            : base( searchType )
        {
            m_filename = filename;
            m_line = line;
            m_index = index;
            m_text = text;
        }

        #region Variables
        private string m_filename;
        private int m_line;
        private int m_index;
        private string m_text;
        #endregion

        #region Properties
        public string Filename
        {
            get
            {
                return m_filename;
            }
        }

        public int Line
        {
            get
            {
                return m_line;
            }
        }

        public int Index
        {
            get
            {
                return m_index;
            }
        }

        public string Text
        {
            get
            {
                return m_text;
            }
        }
        #endregion
    }

    public delegate void FoundStringEventHandler( object sender, SearchReplaceFoundStringEventArgs e );

    public class SearchReplaceReportProgressEventArgs : SearchReplaceEventArgs
    {
        public SearchReplaceReportProgressEventArgs( int progressPercentage, string state, SearchReplaceType searchType )
            : base( searchType )
        {
            m_progressPercentage = progressPercentage;
            m_state = state;
        }

        #region Variables
        private int m_progressPercentage;
        private string m_state;
        #endregion

        #region Properties
        public int ProgressPercentage
        {
            get
            {
                return m_progressPercentage;
            }
        }

        public string State
        {
            get
            {
                return m_state;
            }
        }
        #endregion
    }

    public delegate void ReportProgressEventHandler( object sender, SearchReplaceReportProgressEventArgs e );
    #endregion
}
