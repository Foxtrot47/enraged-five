namespace ragScriptEditorShared
{
    partial class ProjectSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startDebugCommandBrowseButton = new System.Windows.Forms.Button();
            this.startDebugCommandTextBox = new System.Windows.Forms.TextBox();
            this.startDebugCommandLabel = new System.Windows.Forms.Label();
            this.automaticSortngCheckBox = new System.Windows.Forms.CheckBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // startDebugCommandBrowseButton
            // 
            this.startDebugCommandBrowseButton.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.startDebugCommandBrowseButton.AutoSize = true;
            this.startDebugCommandBrowseButton.Location = new System.Drawing.Point( 317, 24 );
            this.startDebugCommandBrowseButton.Name = "startDebugCommandBrowseButton";
            this.startDebugCommandBrowseButton.Size = new System.Drawing.Size( 26, 23 );
            this.startDebugCommandBrowseButton.TabIndex = 13;
            this.startDebugCommandBrowseButton.Text = "...";
            this.startDebugCommandBrowseButton.UseVisualStyleBackColor = true;
            // 
            // startDebugCommandTextBox
            // 
            this.startDebugCommandTextBox.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
                        | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.startDebugCommandTextBox.Location = new System.Drawing.Point( 127, 26 );
            this.startDebugCommandTextBox.Name = "startDebugCommandTextBox";
            this.startDebugCommandTextBox.Size = new System.Drawing.Size( 184, 20 );
            this.startDebugCommandTextBox.TabIndex = 12;
            this.startDebugCommandTextBox.TextChanged += new System.EventHandler( this.SettingChanged );
            // 
            // startDebugCommandLabel
            // 
            this.startDebugCommandLabel.AutoSize = true;
            this.startDebugCommandLabel.Location = new System.Drawing.Point( 3, 29 );
            this.startDebugCommandLabel.Name = "startDebugCommandLabel";
            this.startDebugCommandLabel.Size = new System.Drawing.Size( 114, 13 );
            this.startDebugCommandLabel.TabIndex = 11;
            this.startDebugCommandLabel.Text = "Start Debug Command";
            // 
            // automaticSortngCheckBox
            // 
            this.automaticSortngCheckBox.AutoSize = true;
            this.automaticSortngCheckBox.Location = new System.Drawing.Point( 3, 3 );
            this.automaticSortngCheckBox.Name = "automaticSortngCheckBox";
            this.automaticSortngCheckBox.Size = new System.Drawing.Size( 198, 17 );
            this.automaticSortngCheckBox.TabIndex = 7;
            this.automaticSortngCheckBox.Text = "Automatic Sorting of Project Explorer";
            this.automaticSortngCheckBox.UseVisualStyleBackColor = true;
            this.automaticSortngCheckBox.CheckedChanged += new System.EventHandler( this.SettingChanged );
            // 
            // openFileDialog
            // 
            this.openFileDialog.RestoreDirectory = true;
            // 
            // ProjectSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.startDebugCommandBrowseButton );
            this.Controls.Add( this.startDebugCommandTextBox );
            this.Controls.Add( this.startDebugCommandLabel );
            this.Controls.Add( this.automaticSortngCheckBox );
            this.Name = "ProjectSettingsControl";
            this.Size = new System.Drawing.Size( 346, 52 );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startDebugCommandBrowseButton;
        private System.Windows.Forms.TextBox startDebugCommandTextBox;
        private System.Windows.Forms.Label startDebugCommandLabel;
        private System.Windows.Forms.CheckBox automaticSortngCheckBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog;

    }
}
