namespace ragScriptEditorShared
{
    partial class ToolbarSettingsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rememberVisibilityCheckBox = new System.Windows.Forms.CheckBox();
            this.clearCustomButtonsButton = new System.Windows.Forms.Button();
            this.resetVisibilityButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rememberVisibilityCheckBox
            // 
            this.rememberVisibilityCheckBox.AutoSize = true;
            this.rememberVisibilityCheckBox.Location = new System.Drawing.Point( 3, 3 );
            this.rememberVisibilityCheckBox.Name = "rememberVisibilityCheckBox";
            this.rememberVisibilityCheckBox.Size = new System.Drawing.Size( 148, 17 );
            this.rememberVisibilityCheckBox.TabIndex = 0;
            this.rememberVisibilityCheckBox.Text = "Remember button visibility";
            this.rememberVisibilityCheckBox.UseVisualStyleBackColor = true;
            this.rememberVisibilityCheckBox.CheckedChanged += new System.EventHandler( this.rememberVisibilityCheckBox_CheckedChanged );
            // 
            // clearCustomButtonsButton
            // 
            this.clearCustomButtonsButton.AutoSize = true;
            this.clearCustomButtonsButton.Location = new System.Drawing.Point( 3, 55 );
            this.clearCustomButtonsButton.Name = "clearCustomButtonsButton";
            this.clearCustomButtonsButton.Size = new System.Drawing.Size( 122, 23 );
            this.clearCustomButtonsButton.TabIndex = 3;
            this.clearCustomButtonsButton.Text = "Clear Custom Buttons";
            this.clearCustomButtonsButton.UseVisualStyleBackColor = true;
            this.clearCustomButtonsButton.Click += new System.EventHandler( this.clearCustomButtonsButton_Click );
            // 
            // resetVisibilityButton
            // 
            this.resetVisibilityButton.Location = new System.Drawing.Point( 3, 26 );
            this.resetVisibilityButton.Name = "resetVisibilityButton";
            this.resetVisibilityButton.Size = new System.Drawing.Size( 122, 23 );
            this.resetVisibilityButton.TabIndex = 2;
            this.resetVisibilityButton.Text = "Reset Button Visibility";
            this.resetVisibilityButton.UseVisualStyleBackColor = true;
            this.resetVisibilityButton.Click += new System.EventHandler( this.resetVisibilityButton_Click );
            // 
            // ToolbarSettingsControl
            // 
            this.Controls.Add( this.clearCustomButtonsButton );
            this.Controls.Add( this.resetVisibilityButton );
            this.Controls.Add( this.rememberVisibilityCheckBox );
            this.Name = "ToolbarSettingsControl";
            this.Size = new System.Drawing.Size( 250, 91 );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox rememberVisibilityCheckBox;
        private System.Windows.Forms.Button clearCustomButtonsButton;
        private System.Windows.Forms.Button resetVisibilityButton;
    }
}
