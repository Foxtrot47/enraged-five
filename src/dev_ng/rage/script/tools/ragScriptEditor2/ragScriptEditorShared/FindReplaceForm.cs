using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

using ActiproSoftware.SyntaxEditor;
using RSG.Base.Forms;

namespace ragScriptEditorShared
{
    public partial class FindReplaceForm : Form
    {
        public FindReplaceForm()
        {
            InitializeComponent();
        }

        public FindReplaceForm( Control parentControl, FindReplaceOptions options )
        {
            InitializeComponent();

            m_parentControl = parentControl;
            m_FindReplaceOptions = options;
            m_FindReplace = new FindReplace( parentControl, null );

            m_ReplaceDoneDel = new FindReplace.SearchCompleteDelegate( this.FindReplace_ReplaceDone );
            
            this.currentDocumentRadioButton.Checked = true;
            m_lastSearchIn = FindReplaceSearchRange.CurrentDocument;

            // Select the first search type            
            this.useComboBox.SelectedIndex = 0;

            this.replaceWithTextBox.Enabled = ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor;

            // Update options            
            GetFindReplaceOptions();
        }

        #region Delegates
        public delegate void InitSearchDocsDelegate( ref SyntaxEditor currSyntaxEditor,
            ref Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openDocs, ref List<string> projectDocs );
        public delegate void CancelActiveIntellisenseDelegate();
        public delegate bool LogoffSourceControlDelegate();        
        public delegate bool ReplaceAllWarningDelegate();
        public delegate void MarkAllWithBookmarksDelegate();
        #endregion

        #region Variables
        private Control m_parentControl;
        private FindReplaceOptions m_FindReplaceOptions;
        private SourceControl m_sourceControl = null;
        private FindReplace m_FindReplace = null;
        private FindReplace.SearchCompleteDelegate m_ReplaceDoneDel = null;

        System.Threading.Thread m_TheThread;
        private SyntaxEditor m_SyntaxEditor = null;
        private Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> m_OpenFiles
            = new Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor>();
        private List<string> m_ProjectFiles = new List<string>();
        private FindReplaceSearchRange m_lastSearchIn;
        private bool m_enableSearching = true;
        #endregion

        #region Properties
        public Control ParentControl
        {
            get
            {
                return m_parentControl;
            }
        }

        public bool LocationSet = false;

        public bool MatchCase
        {
            get
            {
                return this.matchCaseCheckBox.Checked;
            }
            set
            {
                this.matchCaseCheckBox.Checked = value;
                m_FindReplaceOptions.MatchCase = value;
            }
        }

        public bool MatchWholeWord
        {
            get
            {
                return this.matchWholeWordCheckBox.Checked;
            }
            set
            {
                this.matchWholeWordCheckBox.Checked = value;
                m_FindReplaceOptions.MatchWholeWord = value;
            }
        }

        public bool SearchUp
        {
            get
            {
                return this.searchUpCheckBox.Checked;
            }
            set
            {
                this.searchUpCheckBox.Checked = value;
                m_FindReplaceOptions.SearchUp = value;
            }
        }

        public bool UseBox
        {
            get
            {
                return this.useCheckBox.Checked;
            }
            set
            {
                this.useComboBox.Enabled = this.useCheckBox.Checked = value;
                m_FindReplaceOptions.SearchType = (value ? FindReplaceSearchType.RegularExpression : FindReplaceSearchType.Normal);
            }
        }

        public bool MarkWithBookmarks
        {
            get
            {
                return this.markWtihBookmarksCheckBox.Checked;
            }
            set
            {
                this.markWtihBookmarksCheckBox.Checked = value;
            }
        }

        public SourceControl SourceControl
        {
            set
            {
                m_sourceControl = value;
                m_FindReplace.SourceControl = value;

            }
        }

        public FindReplaceSearchRange CurrentSearchIn
		{
			get
			{                
				if ( this.currentSelectionRadioButton.Checked )
				{
                    return FindReplaceSearchRange.CurrentSelection;
				}                    
				else if ( this.allOpenDocumentsRadioButton.Checked )
				{
                    return FindReplaceSearchRange.AllOpenDocuments;
				}                    
				else if ( this.entireProjectRadioButton.Checked )
				{
                    return FindReplaceSearchRange.EntireProject;
				}
				else
				{
                    return FindReplaceSearchRange.CurrentDocument;
				}
			}
            set
            {
                switch ( value )
                {
                    case FindReplaceSearchRange.AllOpenDocuments:
                        this.allOpenDocumentsRadioButton.Checked = true;
                        break;
                    case FindReplaceSearchRange.CurrentDocument:
                        this.currentDocumentRadioButton.Checked = true;
                        break;
                    case FindReplaceSearchRange.CurrentSelection:
                        this.currentSelectionRadioButton.Checked = true;
                        break;
                    case FindReplaceSearchRange.EntireProject:
                        this.entireProjectRadioButton.Checked = true;
                        break;
                }
            }
		}

        public FindReplace.SearchBeginDelegate SearchBegin = null;
        public FindReplace.SearchCompleteDelegate SearchComplete = null;
        public FindReplace.LoadFileDelegate LoadFile;
        public FindReplace.ReloadFileDelegate ReloadFile = null;
        public FindReplace.DisableFileWatcherDelegate DisableFileWatch = null;
        public FindReplace.EnableFileWatcherDelegate EnableFileWatch = null;

        public InitSearchDocsDelegate InitSearchDocs = null;
        public CancelActiveIntellisenseDelegate CancelIntellisense = null;
        public ReplaceAllWarningDelegate ReplaceAllWarning = null;
        public MarkAllWithBookmarksDelegate MarkAllWithBookmarks = null;
        #endregion

        #region Event Handlers
        private void FindReplaceForm_Activated( object sender, EventArgs e )
        {
            if ( CancelIntellisense != null )
            {
                CancelIntellisense();
            }

            if ( InitSearchDocs != null )
            {
                // see which search types should be available

                ActiproSoftware.SyntaxEditor.SyntaxEditor editor = null;
                Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openFiles = null;
                List<string> projectFiles = null;

                InitSearchDocs( ref editor, ref openFiles, ref projectFiles );

                this.currentSelectionRadioButton.Enabled = editor != null;
                this.currentDocumentRadioButton.Enabled = editor != null;
                this.allOpenDocumentsRadioButton.Enabled = (openFiles != null) && (openFiles.Count > 0);
                this.entireProjectRadioButton.Enabled = (projectFiles != null) && (projectFiles.Count > 0);

                if ( !this.currentSelectionRadioButton.Enabled && !this.currentDocumentRadioButton.Enabled
                    && !this.allOpenDocumentsRadioButton.Enabled && !this.entireProjectRadioButton.Enabled )
                {
                    m_enableSearching = false;
                }
                else
                {
                    m_enableSearching = true;

                    if ( this.entireProjectRadioButton.Checked && !this.entireProjectRadioButton.Enabled )
                    {
                        if ( this.currentDocumentRadioButton.Enabled )
                        {
                            this.currentDocumentRadioButton.Checked = true;
                        }
                        else if ( this.allOpenDocumentsRadioButton.Enabled )
                        {
                            this.allOpenDocumentsRadioButton.Checked = true;
                        }
                    }
                    else if ( this.allOpenDocumentsRadioButton.Checked && !this.allOpenDocumentsRadioButton.Enabled )
                    {
                        if ( this.currentDocumentRadioButton.Enabled )
                        {
                            this.currentDocumentRadioButton.Checked = true;
                        }
                        else if ( this.entireProjectRadioButton.Enabled )
                        {
                            this.entireProjectRadioButton.Checked = true;
                        }
                    }
                    else if ( (this.currentSelectionRadioButton.Checked && !this.currentSelectionRadioButton.Enabled)
                        || (this.currentDocumentRadioButton.Checked && !this.currentDocumentRadioButton.Enabled) )
                    {
                        if ( this.allOpenDocumentsRadioButton.Enabled )
                        {
                            this.allOpenDocumentsRadioButton.Checked = true;
                        }
                        else if ( this.entireProjectRadioButton.Enabled )
                        {
                            this.entireProjectRadioButton.Checked = true;
                        }
                    }
                }
            }

            SelectNextControl( this.replaceWithTextBox, false, true, true, true );            
        }

        private void findNextButton_Click( object sender, EventArgs e )
        {
            // Update find/replace options
            SetFindReplaceOptions();

            string errorString1 = "";
            string errorString2 = "";

            FindReplace.EFindNextReturnTypes err = FindReplace.EFindNextReturnTypes.E_NO_ERROR;
            SyntaxEditor currSyntaxEditor = m_SyntaxEditor;
            
            if ( this.allOpenDocumentsRadioButton.Checked )
            {
                lock (m_OpenFiles)
                {
                    err = m_FindReplace.FindNextInOpenDocs(m_OpenFiles, m_FindReplaceOptions,
                        ref m_SyntaxEditor, ref errorString1, ref errorString2, LoadFile);
                }
            }
            else if ( this.entireProjectRadioButton.Checked )
            {
                // look in the open docs first
                lock (m_OpenFiles)
                {
                    err = m_FindReplace.FindNextInOpenDocs(m_OpenFiles, m_FindReplaceOptions,
                        ref m_SyntaxEditor, ref errorString1, ref errorString2, LoadFile);
                }

                // if not found, no more found, or we've reached the start of the open doc search
                if ( (err == FindReplace.EFindNextReturnTypes.E_NOT_FOUND)
                    || (err == FindReplace.EFindNextReturnTypes.E_PAST_START_OF_SEARCH) )
                {
                    // build list of project files, minus open files because we've already searched those
                    List<string> allFiles = BuildProjectListWithoutOpenDocs();

                    // look for next file that has a match
                    foreach ( string file in allFiles )
                    {
                        // if the file contains the search text, open the file
                        if ( m_FindReplace.IsInClosedDoc( file, m_FindReplaceOptions )
                            && (LoadFile != null) && LoadFile( file ) )
                        {
                            // find the first occurrence
                            m_FindReplaceOptions.ChangeSelection = true;
                            err = m_FindReplace.FindNextInOpenDoc( m_SyntaxEditor, m_FindReplaceOptions,
                                ref errorString1, ref errorString2 );
                            break;
                        }
                    }
                }
            }
            else
            {
                m_FindReplaceOptions.ChangeSelection = true;
                err = m_FindReplace.FindNextInOpenDoc( m_SyntaxEditor, m_FindReplaceOptions,
                    ref errorString1, ref errorString2 );
            }

            // make sure we retain focus when switching documents
            if ( currSyntaxEditor != m_SyntaxEditor )
            {
                this.Focus();
            }
            
            switch ( err )
            {
            case FindReplace.EFindNextReturnTypes.E_ERROR:
                rageMessageBox.ShowError( this, errorString1, errorString2 );
                break;

            case FindReplace.EFindNextReturnTypes.E_NOT_FOUND:
            case FindReplace.EFindNextReturnTypes.E_PAST_START_OF_SEARCH:
                rageMessageBox.ShowInformation( this, errorString1, errorString2, MessageBoxButtons.OK );
                break;

            default:
                RestoreLastSearchIn();
                break;
            }

            SelectNextControl( this.replaceWithTextBox, false, true, true, true );
        }

        private void replaceButton_Click( object sender, EventArgs e )
        {
            // Update find/replace options
            SetFindReplaceOptions();

            string errorString1 = "";
            string errorString2 = "";

            OnResetSourceControlEditAction();

            FindReplace.EFindNextReturnTypes err = FindReplace.EFindNextReturnTypes.E_NO_ERROR;
            SyntaxEditor currSyntaxEditor = m_SyntaxEditor;
            
            if ( this.allOpenDocumentsRadioButton.Checked )
            {
                lock (m_OpenFiles)
                {
                    err = m_FindReplace.ReplaceInOpenDocs(m_OpenFiles, m_FindReplaceOptions, ref m_SyntaxEditor,
                        ref errorString1, ref errorString2);
                }
            }
            else if ( this.entireProjectRadioButton.Checked )
            {
                // replace in the open docs first
                lock (m_OpenFiles)
                {
                    err = m_FindReplace.ReplaceInOpenDocs(m_OpenFiles, m_FindReplaceOptions, ref m_SyntaxEditor,
                        ref errorString1, ref errorString2);
                }

                // if not found, no more found, or we've reached the start of the open doc search
                if ( (err == FindReplace.EFindNextReturnTypes.E_NOT_FOUND)
                    || (err == FindReplace.EFindNextReturnTypes.E_PAST_START_OF_SEARCH) )
                {
                    // build list of project files, minus open files because we've already searched those
                    List<string> allFiles = BuildProjectListWithoutOpenDocs();

                    // look for next file that has a match
                    foreach ( string file in allFiles )
                    {
                        // if the file contains the search text, open the file
                        if ( m_FindReplace.IsInClosedDoc( file, m_FindReplaceOptions )
                            && (LoadFile != null) && LoadFile( file ) )
                        {
                            // replace the first occurrence
                            m_FindReplaceOptions.ChangeSelection = true;
                            err = m_FindReplace.ReplaceInOpenDoc( m_SyntaxEditor, m_FindReplaceOptions,
                                ref errorString1, ref errorString2, false );
                            break;
                        }
                    }
                }
            }
            else
            {
                m_FindReplaceOptions.ChangeSelection = true;
                err = m_FindReplace.ReplaceInOpenDoc( m_SyntaxEditor, m_FindReplaceOptions,
                    ref errorString1, ref errorString2, false );
            }

            // make sure we retain focus when switching documents
            if ( currSyntaxEditor != m_SyntaxEditor )
            {
                this.Focus();
            }

            switch ( err )
            {
            case FindReplace.EFindNextReturnTypes.E_ERROR:
                rageMessageBox.ShowError( this, errorString1, errorString2 );
                break;

            case FindReplace.EFindNextReturnTypes.E_NOT_FOUND:
            case FindReplace.EFindNextReturnTypes.E_PAST_EOF:
            case FindReplace.EFindNextReturnTypes.E_PAST_START_OF_SEARCH:
                rageMessageBox.ShowInformation( this, errorString1, errorString2 );
                break;

            default:
                RestoreLastSearchIn();
                break;
            }

            SelectNextControl( this.replaceWithTextBox, false, true, true, true );

            OnLogoffSourceControl();
        }

        private void replaceAllButton_Click( object sender, EventArgs e )
        {
            // Update find/replace options
            SetFindReplaceOptions();

            string errorString1 = "";
            string errorString2 = "";

            FindReplace.EFindNextReturnTypes err = FindReplace.EFindNextReturnTypes.E_NO_ERROR;
            if ( this.allOpenDocumentsRadioButton.Checked )
            {
                OnResetSourceControlEditAction();

                lock (m_OpenFiles)
                {
                    err = m_FindReplace.ReplaceAllInOpenDocs(m_OpenFiles, m_FindReplaceOptions,
                        ref errorString1, ref errorString2);
                }
            }
            else if ( this.entireProjectRadioButton.Checked )
            {
                if ( !OnReplaceAllWarning() )
                {
                    return;
                }

                List<string> allFiles = new List<string>();
                foreach ( string file in m_ProjectFiles )
                {
                    allFiles.Add( file );
                }

                lock (m_OpenFiles)
                {
                    IDictionaryEnumerator enumerator = m_OpenFiles.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        SyntaxEditor syntaxEditor = enumerator.Value as SyntaxEditor;
                        if (!allFiles.Contains(syntaxEditor.Document.Filename))
                        {
                            allFiles.Add(syntaxEditor.Document.Filename);
                        }
                    }
                }

                m_FindReplace.InitNewClosedDocsSearch( allFiles, "*.*", m_FindReplaceOptions,
                    null, SearchBegin, m_ReplaceDoneDel, ReloadFile, DisableFileWatch, EnableFileWatch );
                OnResetSourceControlSaveAction();

                try
                {                    
                    this.findNextButton.Enabled = false;

                    m_TheThread = new Thread( new ThreadStart( m_FindReplace.ClosedDocsSearchReplace ) );
                    m_TheThread.Priority = ThreadPriority.Lowest;
                    m_TheThread.Name = "Find replace thread";
                    m_TheThread.Start();
                }
                catch ( Exception ex )
                {
                    rageMessageBox.ShowError( this, String.Format( "An error occurred:\r\n{0}", ex.Message ), "Find/Replace Error" );
                    RestoreLastSearchIn();
                    return;
                }
                return;
            }
            else
            {
                OnResetSourceControlEditAction();

                err = m_FindReplace.ReplaceAllInOpenDoc( m_SyntaxEditor, m_FindReplaceOptions,
                    ref errorString1, ref errorString2, false );
            }

            if ( err == FindReplace.EFindNextReturnTypes.E_ERROR )
            {
                rageMessageBox.ShowError( this, errorString1, errorString2 );
            }
            else
            {
                rageMessageBox.ShowInformation( this, errorString1, errorString2 );
                RestoreLastSearchIn();
            }

            SelectNextControl( this.replaceWithTextBox, false, true, true, true );

            OnLogoffSourceControl();
        }

        private void markAllButton_Click( object sender, EventArgs e )
        {
            // Update find/replace options
            SetFindReplaceOptions();

            string errorString1 = "";
            string errorString2 = "";

            FindReplace.EFindNextReturnTypes err = FindReplace.EFindNextReturnTypes.E_NO_ERROR;
            if ( this.allOpenDocumentsRadioButton.Checked )
            {
                lock (m_OpenFiles)
                {
                    err = m_FindReplace.MarkAllInOpenDocs(m_OpenFiles, m_FindReplaceOptions, this.markWtihBookmarksCheckBox.Checked,
                        ref errorString1, ref errorString2);
                }
            }
            else if ( this.entireProjectRadioButton.Checked )
            {
                // option not allowed
                return;
            }
            else
            {
                err = m_FindReplace.MarkAllInOpenDoc( m_SyntaxEditor, m_FindReplaceOptions, this.markWtihBookmarksCheckBox.Checked,
                    ref errorString1, ref errorString2 );
            }

            switch ( err )
            {
            case FindReplace.EFindNextReturnTypes.E_ERROR:
                rageMessageBox.ShowError( this, errorString1, errorString2 );
                break;

            case FindReplace.EFindNextReturnTypes.E_NOT_FOUND:
            case FindReplace.EFindNextReturnTypes.E_PAST_EOF:
            case FindReplace.EFindNextReturnTypes.E_PAST_START_OF_SEARCH:
                rageMessageBox.ShowInformation( this, errorString1, errorString2 );
                break;

            default:
                OnMarkAllWithBookmarks();
                RestoreLastSearchIn();
                break;
            }

            SelectNextControl( this.replaceWithTextBox, false, true, true, true );
        }

        private void closeButton_Click( object sender, EventArgs e )
        {
            this.Close();
        }

        private void findWhatTextBox_TextChanged( object sender, EventArgs e )
        {
            this.findNextButton.Enabled = m_enableSearching && (this.findWhatTextBox.TextLength > 0);
            this.replaceButton.Enabled = this.findNextButton.Enabled && (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor);
            this.replaceAllButton.Enabled = this.replaceButton.Enabled;
            this.markAllButton.Enabled = this.entireProjectRadioButton.Checked ? false : this.findNextButton.Enabled;
        }

        private void useCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.useComboBox.Enabled = this.useCheckBox.Checked;
        }

        private void FindReplaceForm_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( e.CloseReason == CloseReason.UserClosing )
            {
                e.Cancel = true;
                this.Hide();

                m_parentControl.Focus();
            }
        }

        private void currentSelectionRadioButton_Click( object sender, EventArgs e )
        {
            this.markAllButton.Enabled = this.findNextButton.Enabled;
        }

        private void currentDocumentRadioButton_Click( object sender, EventArgs e )
        {
            this.markAllButton.Enabled = this.findNextButton.Enabled;
            m_lastSearchIn = FindReplaceSearchRange.CurrentDocument;
        }

        private void allOpenDocumentsRadioButton_Click( object sender, EventArgs e )
        {
            this.markAllButton.Enabled = this.findNextButton.Enabled;
            m_lastSearchIn = FindReplaceSearchRange.AllOpenDocuments;
        }

        private void entireProjectRadioButton_Click( object sender, EventArgs e )
        {
            this.markAllButton.Enabled = false;
            m_lastSearchIn = FindReplaceSearchRange.EntireProject;
        }

        public void FindReplace_ReplaceDone( int totalFound, int matchingFiles, int totalFilesSearched,
            FindReplace.EFindNextReturnTypes err, string errorString1, string errorString2 )
        {
            this.findNextButton.Enabled = m_enableSearching;
            m_TheThread = null;

            if ( (totalFound == 0) || (matchingFiles == 0) || (err == FindReplace.EFindNextReturnTypes.E_ERROR) )
            {
                rageMessageBox.ShowError( this, String.Format( "\n{0}\t{1}\n", errorString1, errorString2 ), "Replace All In Project" );
            }
            else
            {
                rageMessageBox.ShowInformation( this,
                    String.Format( "Total replaced: {0}\nMatching files: {1}\nTotal files searched: {2}", totalFound, matchingFiles, totalFilesSearched ),
                    "Replace All In Project" );
            }

            SelectNextControl( this.replaceWithTextBox, false, true, true, true );

            OnSearchComplete( totalFound, matchingFiles, totalFilesSearched, err, errorString1, errorString2 );

            OnLogoffSourceControl();
        }
        #endregion

        #region Event Dispatchers
        private void OnResetSourceControlSaveAction()
        {
            if ( m_sourceControl != null )
            {
                m_sourceControl.ResetSaveAction();
            }
        }

        private void OnResetSourceControlEditAction()
        {
            if ( m_sourceControl != null )
            {
                m_sourceControl.ResetEditAction();
            }
        }

        private void OnSearchComplete( int totalFound, int matchingFiles, int totalFilesSearched,
            FindReplace.EFindNextReturnTypes err, string errorString1, string errorString2 )
        {
            if ( SearchComplete != null )
            {
                if ( m_parentControl != null )
                {
                    object[] args = new object[6];
                    args[0] = totalFound;
                    args[1] = matchingFiles;
                    args[2] = totalFilesSearched;
                    args[3] = err;
                    args[4] = errorString1;
                    args[5] = errorString2;

                    m_parentControl.Invoke( SearchComplete, args );
                }
                else
                {
                    SearchComplete( totalFound, matchingFiles, totalFilesSearched, err, errorString1, errorString2 );
                }
            }
        }

        private void OnLogoffSourceControl()
        {
            if ( m_sourceControl != null )
            {
                m_sourceControl.LogoffSourceControl();
            }
        }

        private bool OnReplaceAllWarning()
        {
            if ( ReplaceAllWarning != null )
            {
                return ReplaceAllWarning();
            }

            return true;
        }

        private void OnMarkAllWithBookmarks()
        {
            if ( this.MarkAllWithBookmarks != null )
            {
                this.MarkAllWithBookmarks();
            }
        }
        #endregion

        #region Misc
        public void CancelActiveIntellisense()
        {
            if ( this.CancelIntellisense != null )
            {
                this.CancelIntellisense();
            }
        }

        public bool InitSearchText( SyntaxEditor currSyntaxEditor )
        {
            if ( m_TheThread != null )
            {
                // don't set initial search text when already performing a search
                return false;
            }

            if ( currSyntaxEditor == null )
            {
                // we need a syntax editor to set the search text
                return false;
            }

            // try to set initial search text
            string selectedText = currSyntaxEditor.SelectedView.SelectedText;
            if ( selectedText.Length > 0 )
            {
                if ( selectedText.IndexOf( "\n" ) < 0 )
                {
                    this.findWhatTextBox.Text = selectedText;
                    this.findWhatTextBox.SelectAll();
                    return true;
                }
            }
            else
            {
                DocumentLine line = currSyntaxEditor.SelectedView.CurrentDocumentLine;
                string lineText = line.Text;

                int colStart = currSyntaxEditor.SelectedView.Selection.StartOffset - line.StartOffset;
                int colEnd = colStart;

                if ( colStart > lineText.Length )
                {
                    // used to happen when intellisense was active
                    return false;
                }
                else if ( (colStart == lineText.Length) || Char.IsWhiteSpace( lineText[colStart] ) )
                {
                    // allow us to look behind
                    --colStart;
                    --colEnd;
                }

                if ( (lineText.Length > 0) && (colStart >= 0)
                    && (Char.IsLetterOrDigit( lineText[colStart] ) || Char.IsSymbol( lineText[colStart] ) || (lineText[colStart] == '_')) )
                {
                    while ( (colStart >= 0)
                        && (Char.IsLetterOrDigit( lineText[colStart] ) || Char.IsSymbol( lineText[colStart] ) || (lineText[colStart] == '_')) )
                    {
                        --colStart;
                    }

                    while ( (colEnd < lineText.Length)
                        && (Char.IsLetterOrDigit( lineText[colEnd] ) || Char.IsSymbol( lineText[colEnd] ) || (lineText[colEnd] == '_')) )
                    {
                        ++colEnd;
                    }

                    this.findWhatTextBox.Text = lineText.Substring( colStart + 1, colEnd - colStart - 1 );
                    this.findWhatTextBox.SelectAll();
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Helper function for findNextButton_Click and replaceButton_Click
        /// </summary>
        /// <param name="allFiles"></param>
        private List<string> BuildProjectListWithoutOpenDocs()
        {
            List<string> allFiles = new List<string>();

            // build list of project files, minus open files because we've already searched those
            foreach ( string file in m_ProjectFiles )
            {
                allFiles.Add( file );
            }

            lock (m_OpenFiles)
            {
                IDictionaryEnumerator enumerator = m_OpenFiles.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    SyntaxEditor syntaxEditor = enumerator.Value as SyntaxEditor;
                    if (allFiles.Contains(syntaxEditor.Document.Filename.ToLower()))	// remove open files
                    {
                        allFiles.Remove(syntaxEditor.Document.Filename.ToLower());
                    }
                }
            }

            return allFiles;
        }

        private void SetFindReplaceOptions()
        {
            m_FindReplaceOptions.FindText = this.findWhatTextBox.Text;
            m_FindReplaceOptions.ReplaceText 
                = (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor) ? this.replaceWithTextBox.Text : string.Empty;
            m_FindReplaceOptions.MatchCase = this.matchCaseCheckBox.Checked;
            m_FindReplaceOptions.MatchWholeWord = this.matchWholeWordCheckBox.Checked;
            m_FindReplaceOptions.SearchUp = this.searchUpCheckBox.Checked;
            m_FindReplaceOptions.SearchType = (this.useCheckBox.Checked ? FindReplaceSearchType.RegularExpression : FindReplaceSearchType.Normal);
            m_FindReplaceOptions.SearchInSelection = this.currentSelectionRadioButton.Checked;

            if ( InitSearchDocs != null )
            {
                lock (m_OpenFiles)
                {
                    InitSearchDocs(ref m_SyntaxEditor, ref m_OpenFiles, ref m_ProjectFiles);
                }
            }
        }

        private void GetFindReplaceOptions()
        {
            this.findWhatTextBox.Text = m_FindReplaceOptions.FindText;
            this.replaceWithTextBox.Text 
                = (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor) ? m_FindReplaceOptions.ReplaceText : string.Empty;
            this.matchCaseCheckBox.Checked = m_FindReplaceOptions.MatchCase;
            this.matchWholeWordCheckBox.Checked = m_FindReplaceOptions.MatchWholeWord;
            this.searchUpCheckBox.Checked = m_FindReplaceOptions.SearchUp;

            this.useCheckBox.Checked = m_FindReplaceOptions.SearchType == FindReplaceSearchType.RegularExpression;
            this.useComboBox.Enabled = this.useCheckBox.Checked;

            this.currentSelectionRadioButton.Checked = m_FindReplaceOptions.SearchInSelection;
        }

        public void SearchAgainDown( SyntaxEditor editor )
        {
            if ( (m_FindReplaceOptions.FindText == null) || (m_FindReplaceOptions.FindText == "") )
            {
                return;
            }

            bool oldSearchUp = m_FindReplaceOptions.SearchUp;
            this.searchUpCheckBox.Checked = false;
            this.findNextButton_Click( m_parentControl, new System.EventArgs() );
            this.searchUpCheckBox.Checked = oldSearchUp;

            editor.Focus();	// make sure this form doesn't steal focus
        }

        public void SearchAgainUp( SyntaxEditor editor )
        {
            if ( (m_FindReplaceOptions.FindText == null) || (m_FindReplaceOptions.FindText == "") )
            {
                return;
            }

            bool oldSearchUp = m_FindReplaceOptions.SearchUp;
            this.searchUpCheckBox.Checked = true;
            this.findNextButton_Click( m_parentControl, new System.EventArgs() );
            this.searchUpCheckBox.Checked = oldSearchUp;

            editor.Focus();	// make sure this form doesn't steal focus
        }

        public void SearchHighlightedDown( SyntaxEditor editor )
        {
            if ( !InitSearchText( editor ) )
            {
                return;
            }

            m_FindReplaceOptions.FindText = this.findWhatTextBox.Text;
            SearchAgainDown( editor );
        }

        public void SearchHighlightedUp( SyntaxEditor editor )
        {
            if ( !InitSearchText( editor ) )
            {
                return;
            }

            m_FindReplaceOptions.FindText = this.findWhatTextBox.Text;
            SearchAgainUp( editor );
        }

        private void RestoreLastSearchIn()
        {
            if ( this.CurrentSearchIn == FindReplaceSearchRange.CurrentSelection )
            {
                if ( m_lastSearchIn == FindReplaceSearchRange.CurrentSelection )
                {
                    this.CurrentSearchIn = FindReplaceSearchRange.CurrentDocument;
                }
                else
                {
                    this.CurrentSearchIn = m_lastSearchIn;
                }
            }
        }
        #endregion

        #region Saving & Loading
        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public bool LoadXml( XmlReader reader )
        {
            string matchcase = reader["matchcase"];
            if ( matchcase != null )
            {
                this.MatchCase = bool.Parse( matchcase );
            }

            string matchword = reader["matchword"];
            if ( matchword != null )
            {
                this.MatchWholeWord = bool.Parse( matchword );
            }

            string up = reader["up"];
            if ( up != null )
            {
                this.SearchUp = bool.Parse( up );
            }

            string use = reader["use"];
            if ( use != null )
            {
                this.UseBox = bool.Parse( use );
            }

            string mark = reader["mark"];
            if ( mark != null )
            {
                this.MarkWithBookmarks = bool.Parse( mark );
            }

            string searchin = reader["searchin"];
            if ( searchin != null )
            {
                this.CurrentSearchIn = (FindReplaceSearchRange)Enum.Parse( typeof( FindReplaceSearchRange ), searchin );
            }

            return true;
        }

        public void SetUIFromSettings( FindReplaceSettings settings )
        {
            this.MatchCase = settings.MatchCase;
            this.MatchWholeWord = settings.MatchWholeWord;
            this.SearchUp = settings.SearchUp;
            this.UseBox = settings.UseRegularExpressions;
            this.MarkWithBookmarks = settings.MarkWithBookmarks;
            this.CurrentSearchIn = settings.SearchIn;
        }

        public void SetSettingsFromUI( FindReplaceSettings settings )
        {
            settings.MatchCase = this.MatchCase;
            settings.MatchWholeWord = this.MatchWholeWord;
            settings.SearchUp = this.SearchUp;
            settings.UseRegularExpressions = this.UseBox;
            settings.MarkWithBookmarks = this.MarkWithBookmarks;
            settings.SearchIn = this.CurrentSearchIn;
        }
        #endregion
    }
}