namespace ragScriptEditorShared
{
    partial class FindReplaceInFilesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.findWhatLabel = new System.Windows.Forms.Label();
            this.findWhatTextBox = new System.Windows.Forms.TextBox();
            this.replaceWithLabel = new System.Windows.Forms.Label();
            this.replaceWithTextBox = new System.Windows.Forms.TextBox();
            this.matchCaseCheckBox = new System.Windows.Forms.CheckBox();
            this.matchWholeWordCheckBox = new System.Windows.Forms.CheckBox();
            this.useCheckBox = new System.Windows.Forms.CheckBox();
            this.useComboBox = new System.Windows.Forms.ComboBox();
            this.openModifiedFilesCheckBox = new System.Windows.Forms.CheckBox();
            this.lookInLabel = new System.Windows.Forms.Label();
            this.lookInComboBox = new System.Windows.Forms.ComboBox();
            this.lookInButton = new System.Windows.Forms.Button();
            this.lookInSubfoldersCheckBox = new System.Windows.Forms.CheckBox();
            this.fileTypesLabel = new System.Windows.Forms.Label();
            this.fileTypesComboBox = new System.Windows.Forms.ComboBox();
            this.findButton = new System.Windows.Forms.Button();
            this.findNextButton = new System.Windows.Forms.Button();
            this.replaceButton = new System.Windows.Forms.Button();
            this.replaceAllButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // findWhatLabel
            // 
            this.findWhatLabel.AutoSize = true;
            this.findWhatLabel.Location = new System.Drawing.Point( 13, 13 );
            this.findWhatLabel.Name = "findWhatLabel";
            this.findWhatLabel.Size = new System.Drawing.Size( 56, 13 );
            this.findWhatLabel.TabIndex = 0;
            this.findWhatLabel.Text = "Fi&nd what:";
            // 
            // findWhatTextBox
            // 
            this.findWhatTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.findWhatTextBox.Location = new System.Drawing.Point( 91, 10 );
            this.findWhatTextBox.Name = "findWhatTextBox";
            this.findWhatTextBox.Size = new System.Drawing.Size( 325, 20 );
            this.findWhatTextBox.TabIndex = 1;
            this.findWhatTextBox.TextChanged += new System.EventHandler( this.findWhatTextBox_TextChanged );
            // 
            // replaceWithLabel
            // 
            this.replaceWithLabel.AutoSize = true;
            this.replaceWithLabel.Location = new System.Drawing.Point( 13, 39 );
            this.replaceWithLabel.Name = "replaceWithLabel";
            this.replaceWithLabel.Size = new System.Drawing.Size( 72, 13 );
            this.replaceWithLabel.TabIndex = 2;
            this.replaceWithLabel.Text = "Re&place with:";
            // 
            // replaceWithTextBox
            // 
            this.replaceWithTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceWithTextBox.Location = new System.Drawing.Point( 91, 36 );
            this.replaceWithTextBox.Name = "replaceWithTextBox";
            this.replaceWithTextBox.Size = new System.Drawing.Size( 325, 20 );
            this.replaceWithTextBox.TabIndex = 3;
            // 
            // matchCaseCheckBox
            // 
            this.matchCaseCheckBox.AutoSize = true;
            this.matchCaseCheckBox.Location = new System.Drawing.Point( 16, 63 );
            this.matchCaseCheckBox.Name = "matchCaseCheckBox";
            this.matchCaseCheckBox.Size = new System.Drawing.Size( 82, 17 );
            this.matchCaseCheckBox.TabIndex = 4;
            this.matchCaseCheckBox.Text = "Match &case";
            this.matchCaseCheckBox.UseVisualStyleBackColor = true;
            // 
            // matchWholeWordCheckBox
            // 
            this.matchWholeWordCheckBox.AutoSize = true;
            this.matchWholeWordCheckBox.Location = new System.Drawing.Point( 16, 90 );
            this.matchWholeWordCheckBox.Name = "matchWholeWordCheckBox";
            this.matchWholeWordCheckBox.Size = new System.Drawing.Size( 113, 17 );
            this.matchWholeWordCheckBox.TabIndex = 5;
            this.matchWholeWordCheckBox.Text = "Match &whole word";
            this.matchWholeWordCheckBox.UseVisualStyleBackColor = true;
            // 
            // useCheckBox
            // 
            this.useCheckBox.AutoSize = true;
            this.useCheckBox.Location = new System.Drawing.Point( 135, 63 );
            this.useCheckBox.Name = "useCheckBox";
            this.useCheckBox.Size = new System.Drawing.Size( 45, 17 );
            this.useCheckBox.TabIndex = 6;
            this.useCheckBox.Text = "&Use";
            this.useCheckBox.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.useCheckBox.UseVisualStyleBackColor = true;
            this.useCheckBox.Visible = false;
            this.useCheckBox.CheckedChanged += new System.EventHandler( this.useCheckBox_CheckedChanged );
            // 
            // useComboBox
            // 
            this.useComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.useComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.useComboBox.FormattingEnabled = true;
            this.useComboBox.Items.AddRange( new object[] {
            "Regular Expressions"} );
            this.useComboBox.Location = new System.Drawing.Point( 186, 63 );
            this.useComboBox.Name = "useComboBox";
            this.useComboBox.Size = new System.Drawing.Size( 185, 21 );
            this.useComboBox.TabIndex = 7;
            this.useComboBox.Visible = false;
            // 
            // openModifiedFilesCheckBox
            // 
            this.openModifiedFilesCheckBox.AutoSize = true;
            this.openModifiedFilesCheckBox.Location = new System.Drawing.Point( 135, 90 );
            this.openModifiedFilesCheckBox.Name = "openModifiedFilesCheckBox";
            this.openModifiedFilesCheckBox.Size = new System.Drawing.Size( 196, 17 );
            this.openModifiedFilesCheckBox.TabIndex = 8;
            this.openModifiedFilesCheckBox.Text = "&Open modified files after Replace All";
            this.openModifiedFilesCheckBox.UseVisualStyleBackColor = true;
            // 
            // lookInLabel
            // 
            this.lookInLabel.AutoSize = true;
            this.lookInLabel.Location = new System.Drawing.Point( 16, 131 );
            this.lookInLabel.Name = "lookInLabel";
            this.lookInLabel.Size = new System.Drawing.Size( 45, 13 );
            this.lookInLabel.TabIndex = 9;
            this.lookInLabel.Text = "&Look in:";
            // 
            // lookInComboBox
            // 
            this.lookInComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lookInComboBox.FormattingEnabled = true;
            this.lookInComboBox.Items.AddRange( new object[] {
            "Current Document",
            "All Open Documents",
            "Entire Project"} );
            this.lookInComboBox.Location = new System.Drawing.Point( 91, 128 );
            this.lookInComboBox.Name = "lookInComboBox";
            this.lookInComboBox.Size = new System.Drawing.Size( 280, 21 );
            this.lookInComboBox.TabIndex = 10;
            this.lookInComboBox.TextChanged += new System.EventHandler( this.lookInComboBox_TextChanged );
            // 
            // lookInButton
            // 
            this.lookInButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lookInButton.Location = new System.Drawing.Point( 377, 126 );
            this.lookInButton.Name = "lookInButton";
            this.lookInButton.Size = new System.Drawing.Size( 27, 23 );
            this.lookInButton.TabIndex = 11;
            this.lookInButton.Text = "&...";
            this.lookInButton.UseVisualStyleBackColor = true;
            this.lookInButton.Click += new System.EventHandler( this.lookInButton_Click );
            // 
            // lookInSubfoldersCheckBox
            // 
            this.lookInSubfoldersCheckBox.AutoSize = true;
            this.lookInSubfoldersCheckBox.Location = new System.Drawing.Point( 91, 155 );
            this.lookInSubfoldersCheckBox.Name = "lookInSubfoldersCheckBox";
            this.lookInSubfoldersCheckBox.Size = new System.Drawing.Size( 112, 17 );
            this.lookInSubfoldersCheckBox.TabIndex = 12;
            this.lookInSubfoldersCheckBox.Text = "Look in s&ubfolders";
            this.lookInSubfoldersCheckBox.UseVisualStyleBackColor = true;
            // 
            // fileTypesLabel
            // 
            this.fileTypesLabel.AutoSize = true;
            this.fileTypesLabel.Location = new System.Drawing.Point( 16, 183 );
            this.fileTypesLabel.Name = "fileTypesLabel";
            this.fileTypesLabel.Size = new System.Drawing.Size( 54, 13 );
            this.fileTypesLabel.TabIndex = 13;
            this.fileTypesLabel.Text = "File &types:";
            // 
            // fileTypesComboBox
            // 
            this.fileTypesComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.fileTypesComboBox.FormattingEnabled = true;
            this.fileTypesComboBox.Items.AddRange( new object[] {
            "*.*"} );
            this.fileTypesComboBox.Location = new System.Drawing.Point( 91, 180 );
            this.fileTypesComboBox.Name = "fileTypesComboBox";
            this.fileTypesComboBox.Size = new System.Drawing.Size( 280, 21 );
            this.fileTypesComboBox.TabIndex = 14;
            // 
            // findButton
            // 
            this.findButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.findButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.findButton.Location = new System.Drawing.Point( 422, 10 );
            this.findButton.Name = "findButton";
            this.findButton.Size = new System.Drawing.Size( 75, 23 );
            this.findButton.TabIndex = 15;
            this.findButton.Text = "&Find";
            this.findButton.UseVisualStyleBackColor = true;
            this.findButton.Click += new System.EventHandler( this.findButton_Click );
            // 
            // findNextButton
            // 
            this.findNextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.findNextButton.Location = new System.Drawing.Point( 422, 41 );
            this.findNextButton.Name = "findNextButton";
            this.findNextButton.Size = new System.Drawing.Size( 75, 23 );
            this.findNextButton.TabIndex = 16;
            this.findNextButton.Text = "Find Ne&xt";
            this.findNextButton.UseVisualStyleBackColor = true;
            this.findNextButton.Click += new System.EventHandler( this.findNextButton_Click );
            // 
            // replaceButton
            // 
            this.replaceButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceButton.Location = new System.Drawing.Point( 422, 70 );
            this.replaceButton.Name = "replaceButton";
            this.replaceButton.Size = new System.Drawing.Size( 75, 23 );
            this.replaceButton.TabIndex = 17;
            this.replaceButton.Text = "&Replace";
            this.replaceButton.UseVisualStyleBackColor = true;
            this.replaceButton.Click += new System.EventHandler( this.replaceButton_Click );
            // 
            // replaceAllButton
            // 
            this.replaceAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceAllButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.replaceAllButton.Location = new System.Drawing.Point( 422, 99 );
            this.replaceAllButton.Name = "replaceAllButton";
            this.replaceAllButton.Size = new System.Drawing.Size( 75, 23 );
            this.replaceAllButton.TabIndex = 18;
            this.replaceAllButton.Text = "Replace &All";
            this.replaceAllButton.UseVisualStyleBackColor = true;
            this.replaceAllButton.Click += new System.EventHandler( this.replaceAllButton_Click );
            // 
            // stopButton
            // 
            this.stopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.stopButton.Location = new System.Drawing.Point( 422, 128 );
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size( 75, 23 );
            this.stopButton.TabIndex = 19;
            this.stopButton.Text = "&Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler( this.stopButton_Click );
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point( 422, 178 );
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size( 75, 23 );
            this.closeButton.TabIndex = 20;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.ShowNewFolderButton = false;
            // 
            // FindReplaceInFilesForm
            // 
            this.AcceptButton = this.findButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size( 509, 213 );
            this.Controls.Add( this.closeButton );
            this.Controls.Add( this.stopButton );
            this.Controls.Add( this.replaceAllButton );
            this.Controls.Add( this.replaceButton );
            this.Controls.Add( this.findNextButton );
            this.Controls.Add( this.findButton );
            this.Controls.Add( this.fileTypesComboBox );
            this.Controls.Add( this.fileTypesLabel );
            this.Controls.Add( this.lookInSubfoldersCheckBox );
            this.Controls.Add( this.lookInButton );
            this.Controls.Add( this.lookInComboBox );
            this.Controls.Add( this.lookInLabel );
            this.Controls.Add( this.openModifiedFilesCheckBox );
            this.Controls.Add( this.useComboBox );
            this.Controls.Add( this.useCheckBox );
            this.Controls.Add( this.matchWholeWordCheckBox );
            this.Controls.Add( this.matchCaseCheckBox );
            this.Controls.Add( this.replaceWithTextBox );
            this.Controls.Add( this.replaceWithLabel );
            this.Controls.Add( this.findWhatTextBox );
            this.Controls.Add( this.findWhatLabel );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FindReplaceInFilesForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Find/Replace In Files";
            this.Activated += new System.EventHandler( this.FindReplaceInFilesForm_Activated );
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.FindReplaceInFilesForm_FormClosing );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label findWhatLabel;
        private System.Windows.Forms.TextBox findWhatTextBox;
        private System.Windows.Forms.Label replaceWithLabel;
        private System.Windows.Forms.TextBox replaceWithTextBox;
        private System.Windows.Forms.CheckBox matchCaseCheckBox;
        private System.Windows.Forms.CheckBox matchWholeWordCheckBox;
        private System.Windows.Forms.CheckBox useCheckBox;
        private System.Windows.Forms.ComboBox useComboBox;
        private System.Windows.Forms.CheckBox openModifiedFilesCheckBox;
        private System.Windows.Forms.Label lookInLabel;
        private System.Windows.Forms.ComboBox lookInComboBox;
        private System.Windows.Forms.Button lookInButton;
        private System.Windows.Forms.CheckBox lookInSubfoldersCheckBox;
        private System.Windows.Forms.Label fileTypesLabel;
        private System.Windows.Forms.ComboBox fileTypesComboBox;
        private System.Windows.Forms.Button findButton;
        private System.Windows.Forms.Button findNextButton;
        private System.Windows.Forms.Button replaceButton;
        private System.Windows.Forms.Button replaceAllButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
    }
}