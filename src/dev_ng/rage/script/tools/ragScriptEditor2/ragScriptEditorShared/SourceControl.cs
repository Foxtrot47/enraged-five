using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Command;
using RSG.Base.Forms;
using ragScriptEditorShared;

namespace ragScriptEditorShared
{
    public class SourceControl
    {
        public SourceControl( Form mainForm, SourceControlSettings2 srcCtrlSettings )
        {
            m_mainForm = mainForm;
            m_sourceControlSettings = srcCtrlSettings;
        }

        #region Enums
        public enum PromptType
        {
            Compile,
            Edit,
            EditAll,
            Save,
            SaveAll
        }

        public enum Action
        {
            None,
            CheckOut,
            CheckOutAll,
            Overwrite,
            OverwriteAll,
            Cancel
        }
        #endregion

        #region Delegates
        public delegate Action ShowPromptDialogDelegate( string filename, PromptType promptType );
        public delegate bool SaveNewFileDelegate( string filename );
        public delegate bool PreSyncDelegate( string filename, SyncFileDialog.SyncFileAction action );
        public delegate bool PostSyncDelegate( string filename, SyncFileDialog.SyncFileAction action );
        #endregion

        #region Properties
        public ShowPromptDialogDelegate ShowPromptDialog = null;
        public SaveNewFileDelegate SaveNewFile = null;
        public PreSyncDelegate PreSync = null;
        public PostSyncDelegate PostSync = null;
        #endregion

        #region Variables
        private Form m_mainForm = null;
        private SourceControlSettings2 m_sourceControlSettings;

        private Dictionary<string, object> m_skipPromptOnEdit = new Dictionary<string, object>();
        private Action m_editAction = Action.None;
        private Action m_saveAction = Action.None;
        private Action m_compileAction = Action.None;
        #endregion

        #region Public Functions
        public bool MakeFileWritableOnSave( string filename, bool saveAll )
        {
            // see if it's read only:
            if ( !File.Exists( filename ) )
            {
                return true;
            }

            // see if it's writable
            FileAttributes attributes = File.GetAttributes( filename );
            if ( (attributes & FileAttributes.ReadOnly) != FileAttributes.ReadOnly )
            {
                return true;
            }

            // we're using source control
            if ( m_sourceControlSettings.Enabled )
            {
                if ( saveAll && (m_saveAction == Action.Cancel) )
                {
                    // kinda klugey but makes sure we cancel everything
                    return false;
                }

                rageStatus status;
                if ( m_sourceControlSettings.SourceControlProviderSettings.IsSourceControlled( filename, out status ) )
                {
                    // the file is in source control, so determine what we should do with it
                    switch ( m_saveAction )
                    {
                        case Action.None:
                            switch ( m_sourceControlSettings.OnSave )
                            {
                                case SourceControlSettings2.SaveAction.CheckOut:
                                    m_saveAction = Action.CheckOutAll;
                                    break;

                                case SourceControlSettings2.SaveAction.Prompt:
                                    m_saveAction = OnShowPromptDialog( filename, saveAll ? PromptType.SaveAll : PromptType.Save );
                                    break;

                                case SourceControlSettings2.SaveAction.SaveAs:
                                    OnSaveNewFile( filename );
                                    return false;
                            }

                            if ( m_saveAction == Action.CheckOut )
                            {
                                m_saveAction = Action.None;   // reset for next time
                                goto case Action.CheckOut;
                            }
                            else if ( m_saveAction == Action.CheckOutAll )
                            {
                                goto case Action.CheckOutAll;
                            }
                            else if ( m_saveAction == Action.Overwrite )
                            {
                                m_saveAction = Action.None;   // reset for next time
                                goto case Action.Overwrite;
                            }
                            else if ( m_saveAction == Action.OverwriteAll )
                            {
                                goto case Action.OverwriteAll;
                            }
                            else if ( m_saveAction == Action.Cancel )
                            {
                                return false; // don't allow us to save
                            }
                            break;

                        case Action.CheckOut:
                            {
                                if ( !m_sourceControlSettings.SourceControlProviderSettings.HaveLatestVersion( filename, out status ) )
                                {
                                    SyncFileDialog.SyncFileAction action = SyncFileDialog.ShowIt( m_mainForm, filename );

                                    if ( OnPreSync( filename, action ) )
                                    {
                                        m_sourceControlSettings.SourceControlProviderSettings.Sync( filename, out status );

                                        if ( !OnPostSync( filename, action ) )
                                        {
                                            if ( (action == SyncFileDialog.SyncFileAction.DiscardSyncThenCheckOut)
                                                || (action == SyncFileDialog.SyncFileAction.SaveAsSyncThenCheckOut) )
                                            {
                                                // reload failed.
                                                return false;
                                            }
                                        }
                                    }
                                    else if ( action != SyncFileDialog.SyncFileAction.DoNotSyncBeforeCheckOut )
                                    {
                                        // nothing more to do
                                        return false;
                                    }
                                }

                                // check out the file
                                m_sourceControlSettings.SourceControlProviderSettings.CheckOut( filename, out status );
                                if ( !status.Success() )
                                {
                                    rageMessageBox.ShowError( m_mainForm, 
                                        String.Format( "Unable to check out '{0}'.\r\n\r\n{1}", filename, status.GetErrorString().Replace("\n", "\r\n")),
                                        "Check Out File Error" );
                                    return false;
                                }
                            }
                            return true;

                        case Action.CheckOutAll:
                            goto case Action.CheckOut;

                        case Action.Overwrite:
                            return MakeFileWritable( filename, true );	// silently make it writable

                        case Action.OverwriteAll:
                            goto case Action.Overwrite;
                    }
                }
                else if (!status.Success())
                {
                    if (!HandleConnectionFailure(filename))
                    {
                        return false;
                    }
                }
                else
                {
                    ApplicationSettings.Log.Message( $"'{filename}' is not in source control.");
                }
            }

            // prompt if they want to make it writable
            return MakeFileWritable( filename, false );
        }

        public bool MakeFileWritableOnEdit( string filename, bool editAll )
        {
            // see if it's read only:
            if ( !File.Exists( filename ) )
            {
                return true;
            }

            // see if it's writable
            FileAttributes attributes = File.GetAttributes( filename );
            if ( (attributes & FileAttributes.ReadOnly) != FileAttributes.ReadOnly )
            {
                return true;
            }

            // we're using source control
            if ( m_sourceControlSettings.Enabled
                && (m_sourceControlSettings.OnEdit != SourceControlSettings2.EditAction.DoNothing) )
            {
                if ( (m_sourceControlSettings.OnEdit == SourceControlSettings2.EditAction.Prompt)
                    && m_skipPromptOnEdit.ContainsKey( filename.ToLower() ) )
                {
                    return true;
                }

                if ( editAll && (m_editAction == Action.Cancel) )
                {
                    // kinda klugey but makes sure we cancel everything
                    return false;
                }

                rageStatus status;
                if ( m_sourceControlSettings.SourceControlProviderSettings.IsSourceControlled( filename, out status ) )
                {
                    // the file is in source control, so determine what we should do with it
                    switch ( m_editAction )
                    {
                        case Action.None:
                            switch ( m_sourceControlSettings.OnEdit )
                            {
                                case SourceControlSettings2.EditAction.CheckOut:
                                    m_editAction = Action.CheckOut;
                                    break;
                                case SourceControlSettings2.EditAction.Prompt:
                                    m_editAction = OnShowPromptDialog( filename, editAll ? PromptType.EditAll : PromptType.Edit );
                                    break;
                                case SourceControlSettings2.EditAction.DoNothing:
                                    break; // fall through to the end of this function which will tell us it's ok to edit
                            }

                            if ( m_editAction == Action.CheckOut )
                            {
                                m_editAction = Action.None;   // reset for next time
                                goto case Action.CheckOut;
                            }
                            else if ( m_editAction == Action.CheckOutAll )
                            {
                                goto case Action.CheckOutAll;
                            }
                            else if ( m_editAction == Action.Overwrite )
                            {
                                m_editAction = Action.None;   // reset for next time
                                goto case Action.Overwrite;
                            }
                            else if ( m_editAction == Action.OverwriteAll )
                            {
                                goto case Action.OverwriteAll;
                            }
                            else if ( m_editAction == Action.Cancel )
                            {
                                goto case Action.Cancel;
                            }
                            break;

                        case Action.CheckOut:
                            // sync first
                            if ( !m_sourceControlSettings.SourceControlProviderSettings.HaveLatestVersion( filename, out status ) )
                            {
                                SyncFileDialog.SyncFileAction action = SyncFileDialog.ShowIt( m_mainForm, filename );

                                if ( OnPreSync( filename, action ) )
                                {
                                    m_sourceControlSettings.SourceControlProviderSettings.Sync( filename, out status );
                                    if ( !OnPostSync( filename, action ) )
                                    {
                                        if ( (action == SyncFileDialog.SyncFileAction.DiscardSyncThenCheckOut)
                                            || (action == SyncFileDialog.SyncFileAction.SaveAsSyncThenCheckOut) )
                                        {
                                            // reload failed.
                                            return false;
                                        }
                                    }
                                }
                                else if ( action != SyncFileDialog.SyncFileAction.DoNotSyncBeforeCheckOut )
                                {
                                    // nothing more to do
                                    return false;
                                }
                            }

                            // now check out
                            m_sourceControlSettings.SourceControlProviderSettings.CheckOut( filename, out status );
                            if ( !status.Success() )
                            {
                                rageMessageBox.ShowError( m_mainForm,
                                    String.Format( "Unable to check out '{0}'.\r\n\r\n{1}", filename, status.GetErrorString().Replace("\n", "\r\n")),
                                    "Sync File Error" );
                                return false;
                            }
                            return true;

                        case Action.CheckOutAll:
                            goto case Action.CheckOut;

                        case Action.Overwrite:
                            return MakeFileWritable( filename, true );	// silently make it writable

                        case Action.OverwriteAll:
                            goto case Action.Overwrite;

                        case Action.Cancel:
                            // remember that we chose not to check out
                            if ( !m_skipPromptOnEdit.ContainsKey( filename.ToLower() ) )
                            {
                                m_skipPromptOnEdit.Add( filename.ToLower(), null );
                            }
                            return true;	// do nothing but continue normally
                    }
                }
                else if (!status.Success())
                {
                    if (!HandleConnectionFailure(filename))
                    {
                        return false;
                    }
                }
                else
                {
                    ApplicationSettings.Log.Message( $"'{filename}' is not in source control." );
                }
            }

            // in the default case, we leave it alone and proceed normally
            return true;
        }

        public bool MakeFileWritableOnCompile( string filename )
        {
            // see if it's read only:
            if ( !File.Exists( filename ) )
            {
                return true;
            }

            // see if it's writable
            FileAttributes attributes = File.GetAttributes( filename );
            if ( (attributes & FileAttributes.ReadOnly) != FileAttributes.ReadOnly )
            {
                return true;
            }

            // we're using source control
            if ( m_sourceControlSettings.Enabled )
            {
                rageStatus status;
                if (m_sourceControlSettings.SourceControlProviderSettings.IsSourceControlled(filename, out status))
                {
                    switch (m_compileAction)
                    {
                        case Action.None:
                            switch (m_sourceControlSettings.OnCompile)
                            {
                                case SourceControlSettings2.CompileAction.CheckOut:
                                    m_compileAction = Action.CheckOutAll;
                                    break;

                                case SourceControlSettings2.CompileAction.Overwrite:
                                    m_compileAction = Action.OverwriteAll;
                                    break;

                                case SourceControlSettings2.CompileAction.Prompt:
                                    m_compileAction = OnShowPromptDialog(filename, PromptType.Compile);
                                    break;
                            }

                            if (m_compileAction == Action.CheckOut)
                            {
                                m_compileAction = Action.None;  // reset for next time
                                goto case Action.CheckOut;
                            }
                            else if (m_compileAction == Action.CheckOutAll)
                            {
                                goto case Action.CheckOutAll;
                            }
                            else if (m_compileAction == Action.Overwrite)
                            {
                                m_compileAction = Action.None;  // reset for next time
                                goto case Action.Overwrite;
                            }
                            else if (m_compileAction == Action.OverwriteAll)
                            {
                                goto case Action.OverwriteAll;
                            }
                            break;

                        case Action.CheckOut:
                            // sync first
                            if (!m_sourceControlSettings.SourceControlProviderSettings.HaveLatestVersion(filename, out status))
                            {
                                m_sourceControlSettings.SourceControlProviderSettings.Sync(filename, out status);
                            }

                            // now check out
                            m_sourceControlSettings.SourceControlProviderSettings.CheckOut(filename, out status);
                            if (!status.Success())
                            {
                                rageMessageBox.ShowError(m_mainForm,
                                    String.Format("Unable to check out '{0}'.\r\n\r\n{1}", filename, status.GetErrorString().Replace("\n", "\r\n")),
                                    "Check Out File Error");
                                return false;
                            }
                            return true;

                        case Action.CheckOutAll:
                            goto case Action.CheckOut;

                        case Action.Overwrite:
                            break; // fall through to the end of this function which will silently make it writable

                        case Action.OverwriteAll:
                            break; // fall through to the end of this function which will silently make it writable
                    }
                }
                else if (!status.Success())
                {
                    if (!HandleConnectionFailure(filename))
                    {
                        return false;
                    }
                }
                else
                {
                    ApplicationSettings.Log.Message($"'{filename}' is not in source control.");
                }
            }

            // silently make it writable
            return MakeFileWritable( filename, true );
        }

        public bool LogoffSourceControl()
        {
            return true;
        }

        public void ResetEditAction()
        {
            m_editAction = Action.None;
        }

        public void ResetSaveAction()
        {
            m_saveAction = Action.None;
        }

        public void ResetCompileAction()
        {
            m_compileAction = Action.None;
        }

        public void RemoveSkippedFile( string filename )
        {
            if ( m_skipPromptOnEdit.ContainsKey( filename.ToLower() ) )
            {
                m_skipPromptOnEdit.Remove( filename.ToLower() );
            }
        }

        public void SettingsSaved()
        {
            if ( m_sourceControlSettings.OnEdit != SourceControlSettings2.EditAction.Prompt )
            {
                m_skipPromptOnEdit.Clear();
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Helper function to make a file writable after we already know it exists and that it is read-only
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="silent"></param>
        /// <returns>true if it is ok to save</returns>
        private bool MakeFileWritable( string filename, bool silent )
        {
            DialogResult result = DialogResult.Yes;

            if ( !silent )
            {
                result = rageMessageBox.ShowQuestion( m_mainForm, 
                    String.Format( "'{0}' is read-only.  Shall I attempt to remove the write protection and save the file?", filename ),
                    "File Read Only" );
            }

            try
            {
                if ( result == DialogResult.Yes )
                {
                    File.SetAttributes( filename, File.GetAttributes( filename ) & (~FileAttributes.ReadOnly) );
                    return true;
                }
            }
            catch ( Exception e )
            {
                rageMessageBox.ShowError( m_mainForm, 
                    String.Format( "Unable to make '{0}' writable.\n\n{1}", filename, e.Message ), "File Make Writable Error" );
            }

            return false;
        }

        /// <summary>
        /// Show a connection failure dialog (given a filename attempted to check out).
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private bool HandleConnectionFailure(String filename)
        {
            // The query failed, perhaps the connection is invalid.
            if (rageMessageBox.ShowError(m_mainForm,
                $"Unable to connect to source control ({m_sourceControlSettings.SourceControlProviderSettings.SelectedProvider.Name}) to check out '{filename}'. " +
                    "Connection information for the server could be invalid, or the server could be down.\r\n\r\n" +
                    "Disable source control?\r\n\r\n" +
                    $"Server: {m_sourceControlSettings.SourceControlProviderSettings.SelectedProvider.ServerOrPort}\r\n" +
                    $"Username: {m_sourceControlSettings.SourceControlProviderSettings.SelectedProvider.LoginOrUsername}\r\n" +
                    $"Workspace: {m_sourceControlSettings.SourceControlProviderSettings.SelectedProvider.ProjectOrWorkspace}",
                "Source Control Error", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                // Disable source control, and continue below (to make file writable manually).
                m_sourceControlSettings.SourceControlProviderSettings.Enabled = false;
                return true;
            }

            // Cancel, if user wants option to fix SCM settings.
            return false;
        }
        #endregion

        #region Event Dispatchers
        private Action OnShowPromptDialog( string filename, PromptType promptType )
        {
            object[] args = new Object[2];
            args[0] = filename;
            args[1] = promptType;

            object result = m_mainForm.Invoke( ShowPromptDialog, args );
            return (Action)(result);
        }

        private void OnSaveNewFile( string filename )
        {
            object[] args = new Object[1];
            args[0] = filename;

            m_mainForm.Invoke( SaveNewFile, args );
        }

        private bool OnPreSync( string filename, SyncFileDialog.SyncFileAction action )
        {
            if ( this.PreSync != null )
            {
                object[] args = new object[2];
                args[0] = filename;
                args[1] = action;

                object rtn = m_mainForm.Invoke( this.PreSync, args );
                return rtn.ToString() == Boolean.TrueString;
            }

            return true;
        }

        private bool OnPostSync( string filename, SyncFileDialog.SyncFileAction action )
        {
            if ( this.PostSync != null )
            {
                object[] args = new object[2];
                args[0] = filename;
                args[1] = action;
                args[1] = action;

                object rtn = m_mainForm.Invoke( this.PostSync, args );
                return rtn.ToString() == Boolean.TrueString;
            }

            return true;
        }
        #endregion
    }
}
