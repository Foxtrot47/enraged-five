using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Forms;

namespace ragScriptEditorShared
{
    public partial class ToolbarSettingsControl : ragScriptEditorShared.SettingsControlBase
    {
        public ToolbarSettingsControl()
        {
            InitializeComponent();
        }

        #region Variables
        private ToolbarSettings m_toolbarSettings = new ToolbarSettings();
        #endregion

        #region Overrides
        public override SettingsBase TabSettings
        {
            get
            {
                m_toolbarSettings.RememberButtonVisiblity = this.rememberVisibilityCheckBox.Checked;

                return m_toolbarSettings;
            }
            set
            {
                if ( value is ToolbarSettings )
                {
                    m_invokeSettingsChanged = false;

                    // clone it
                    m_toolbarSettings = value.Clone() as ToolbarSettings;

                    this.rememberVisibilityCheckBox.Checked = m_toolbarSettings.RememberButtonVisiblity;

                    m_invokeSettingsChanged = true;
                }
            }
        }
        #endregion

        #region Event Handlers
        private void rememberVisibilityCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            OnSettingsChanged();
        }
        
        private void resetVisibilityButton_Click( object sender, EventArgs e )
        {
            DialogResult result = rageMessageBox.ShowQuestion( this, "Are you sure you want to make all of the Toolbar buttons visible?",
                "Reset Tool Bar" );
            if ( result == DialogResult.Yes )
            {
                foreach ( ToolbarButtonSetting bs in m_toolbarSettings.ToolbarButtonSettings )
                {
                    bs.Visible = true;
                }

                OnSettingsChanged();
            }
        }

        private void clearCustomButtonsButton_Click( object sender, EventArgs e )
        {
            DialogResult result = rageMessageBox.ShowWarning( this, "Are you sure you want to permanently\ndelete your custom buttons?",
                "Clear Custom Buttons", MessageBoxButtons.YesNo );
            if ( result == DialogResult.Yes )
            {
                for ( int i = 0; i < m_toolbarSettings.ToolbarButtonSettings.Count; )
                {
                    if ( m_toolbarSettings.ToolbarButtonSettings[i].IsCustomButton )
                    {
                        m_toolbarSettings.ToolbarButtonSettings.RemoveAt( i );
                    }
                    else
                    {
                        ++i;
                    }
                }

                OnSettingsChanged();
            }
        }
        #endregion
    }
}

