//---------------------------------------------------------------------------------------------
// <copyright file="Compiler.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2019-2021. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace ragScriptEditorShared
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Platform;

    /// <summary>
	/// This class holds all of the basic functionality for compiling a file, a list of files, or a project.
	/// </summary>
	public class Compiler
	{
		public Compiler( System.Windows.Forms.Form theForm, UserEditorSettings userSettings )
		{
			m_callingForm = theForm;
			m_userEditorSettings = userSettings;

            m_compileFileBackgroundWorker.DoWork += new DoWorkEventHandler( compileFileBackgroundWorker_DoWork );
            m_compileFileBackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler( backgroundWorker_RunWorkerCompleted );
            m_compileFileBackgroundWorker.WorkerReportsProgress = false;
            m_compileFileBackgroundWorker.WorkerSupportsCancellation = true;

            m_compileListBackgroundWorker.DoWork += new DoWorkEventHandler( compileListBackgroundWorker_DoWork );
            m_compileListBackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler( backgroundWorker_RunWorkerCompleted );
            m_compileListBackgroundWorker.WorkerReportsProgress = false;
            m_compileListBackgroundWorker.WorkerSupportsCancellation = true;
        }

        #region Delegates
        public delegate void PreCompileStepDelegate();
		public delegate string BuildCompilerCommandDelegate(string filename, Platform platform);
        public delegate string ResourceCompilerCommandDelegate(string filename, string outputFilename, Platform platform);
		public delegate bool OkToCompileDelegate( string filename );
		public delegate void CompileCompleteDelegate( int numErrors, string projectListFileName );
        public delegate void CompileCancelledDelegate( int numErrors );
        public delegate void BreakOnErrorDelegate( int numErrors );
		public delegate void CompilerFailureDelegate( string error );
		public delegate void FilesNotFoundDelegate( IEnumerable<string> filenames );
		public delegate void FileUpToDateDelegate( string filename );
		public delegate void ListUpToDateDelegate( string projectListFileName );
		public delegate void NothingToCompileDelegate();
		public delegate int ProcessOutputStringDelegate( string output );

        public delegate bool IsFileCompilableDelegate( string filename );
        public delegate bool FileNeedsToBeCompiledDelegate( string filename, string projectFile, CompilingSettings compilingSettings );
        public delegate bool FileNeedsToBeResourcedDelegate(string filename, Platform platform, string projectFile, CompilingSettings compilingSettings);

		private delegate void OutputCompileStringDelegate( string output );
        #endregion

        #region Enums
        private enum CompileResult
        {
            Nothing,
            NothingToCompile,
            ListUpToDate,
            Cancelled,
            Failure,
            BreakOnError,
            Complete,
        }
        #endregion

        #region Properties
        public PreCompileStepDelegate PreCompileStep = null;
		public BuildCompilerCommandDelegate BuildCompilerCommand = null;
        public ResourceCompilerCommandDelegate ResourceCompilerCommand = null;
		public OkToCompileDelegate OkToCompile = null;
		public CompileCompleteDelegate CompileComplete = null;
        public CompileCancelledDelegate CompileCancelled = null;
        public BreakOnErrorDelegate BreakOnError = null;
		public CompilerFailureDelegate CompilerFailure = null;
		public FilesNotFoundDelegate FilesNotFound = null;
		public FileUpToDateDelegate FileUpToDate = null;
		public ListUpToDateDelegate ListUpToDate = null;
		public NothingToCompileDelegate NothingToCompile = null;
		public ProcessOutputStringDelegate ProcessOutputString = null;
        public IsFileCompilableDelegate IsFileCompilable = null;
        public FileNeedsToBeCompiledDelegate FileNeedsToBeCompiled = null;
        public FileNeedsToBeResourcedDelegate FileNeedsToBeResourced = null;

		public string OutputDirectory = string.Empty;

        public bool StopCompile
        {
            get
            {
                if ( m_compileFileBackgroundWorker.IsBusy )
                {
                    return m_compileFileBackgroundWorker.CancellationPending;
                }
                else if ( m_compileListBackgroundWorker.IsBusy )
                {
                    return m_compileListBackgroundWorker.CancellationPending;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                if ( value )
                {
                    if ( m_compileFileBackgroundWorker.IsBusy )
                    {
                        m_compileFileBackgroundWorker.CancelAsync();
                    }
                    else if ( m_compileListBackgroundWorker.IsBusy )
                    {
                        m_compileListBackgroundWorker.CancelAsync();
                    }
                }
            }
        }

        public bool IsCompiling
        {
            get
            {
                return (m_compileFileBackgroundWorker.IsBusy || m_compileListBackgroundWorker.IsBusy);
            }
        }

        public static readonly Regex CompileErrorRegex = new Regex("^\\s*(?<Type>(Error:|Warning:|Info:))\\s*(\\[(?<Timestamp>[0-9]+)\\])?\\s*(\\[(?<LogChannel>[a-zA-Z_\\-]+)\\])?\\s*(?<File>[a-zA-Z_0-9\\-\\\\/:\\. ]*)\\((?<lineNum>[0-9]+)\\)\\s*:\\s*(?<Text>.*)$", RegexOptions.Compiled );
        public static readonly Regex IncludedFromHereRegex = new Regex("^\\s*(?<File>[\\a-zA-Z_0-9\\-\\\\/:\\. ]*)\\((?<lineNum>[0-9]+)\\)\\s*:\\s*(included from here)$", RegexOptions.Compiled);

        public bool RunAsync = true;

        public bool BreakOnCompileError = false;
        #endregion

        #region Variables
        private string m_compileProjectFile = null;
		private int m_numCompileErrors = 0;
        private BackgroundWorkerStart m_backgroundWorkerStart;

		private System.Windows.Forms.Form m_callingForm = null;
		private UserEditorSettings m_userEditorSettings = null;
        private Regex m_filenameRegex = new Regex( "\\A\\s*compiling\\s*\\x27(?<File>[\\a-zA-Z_0-9\\x2F\\x2E\\x3A\\x5C\\x20]*)\\x27", RegexOptions.Compiled );

        private BackgroundWorker m_compileFileBackgroundWorker = new BackgroundWorker();
        private BackgroundWorker m_compileListBackgroundWorker = new BackgroundWorker();
        #endregion

        #region Public Functions
        /// <summary>
        /// Compiles one file now without using background tasks
        /// </summary>
        /// <param name="filename">The file to compile</param>
        /// <param name="platform">The platform.</param>
        public bool CompileFileNow(string filename, Platform platform)
        {
            this.m_numCompileErrors = 0;
            return CompileOneFile(filename, platform) == CompileResult.Complete;
        }

        /// <summary>
		/// Compiles one file
		/// </summary>
		/// <param name="filename"></param>
        public void CompileFile(string filename, Platform platform)
		{
            if ( m_compileFileBackgroundWorker.IsBusy || m_compileListBackgroundWorker.IsBusy )
            {
                return;
            }

            List<string> filenames = new List<string>();
            filenames.Add( filename );
            BackgroundWorkerStart bwStart = new BackgroundWorkerStart( filenames, platform, true );

            if ( this.RunAsync )
            {
                m_compileFileBackgroundWorker.RunWorkerAsync( bwStart );
            }
            else
            {
                DoWorkEventArgs doWork = new DoWorkEventArgs( bwStart );
                compileFileBackgroundWorker_DoWork( this, doWork );

                RunWorkerCompletedEventArgs runWorkerCompleted = new RunWorkerCompletedEventArgs( doWork.Result, null, false );
                backgroundWorker_RunWorkerCompleted( this, runWorkerCompleted );
            }
		}

		/// <summary>
		/// Compiles a list of files that aren't considered project files.
		/// </summary>
		/// <param name="filenames"></param>
        public void CompileFileList(List<string> filenames, Platform platform)
		{
            if ( m_compileFileBackgroundWorker.IsBusy || m_compileListBackgroundWorker.IsBusy )
            {
                return;
            }

			m_compileProjectFile = null;

			//CompileList( filenames, false );
            BackgroundWorkerStart bwStart = new BackgroundWorkerStart( filenames, platform, false );

            if ( this.RunAsync )
            {
                m_compileListBackgroundWorker.RunWorkerAsync( bwStart );
            }
            else
            {
                DoWorkEventArgs doWork = new DoWorkEventArgs( bwStart );
                compileListBackgroundWorker_DoWork( this, doWork );

                RunWorkerCompletedEventArgs runWorkerCompleted = new RunWorkerCompletedEventArgs( doWork.Result, null, false );
                backgroundWorker_RunWorkerCompleted( this, runWorkerCompleted );
            }
		}

		/// <summary>
		/// Cleans, then compiles a list of files that aren't considered project files
		/// </summary>
		/// <param name="filenames"></param>
        public void RecompileFileList(List<string> filenames, Platform platform)
		{
            if ( m_compileFileBackgroundWorker.IsBusy || m_compileListBackgroundWorker.IsBusy )
            {
                return;
            }

			m_compileProjectFile = null;

			//CompileList( filenames, true );
            BackgroundWorkerStart bwStart = new BackgroundWorkerStart(filenames, platform, true);

            if ( this.RunAsync )
            {
                m_compileListBackgroundWorker.RunWorkerAsync( bwStart );
            }
            else
            {
                DoWorkEventArgs doWork = new DoWorkEventArgs( bwStart );
                compileListBackgroundWorker_DoWork( this, doWork );

                RunWorkerCompletedEventArgs runWorkerCompleted = new RunWorkerCompletedEventArgs( doWork.Result, null, false );
                backgroundWorker_RunWorkerCompleted( this, runWorkerCompleted );
            }
        }

		/// <summary>
		/// Compiles a list of project files
		/// </summary>
		/// <param name="project"></param>
		/// <param name="filenames"></param>
        public void CompileProject(string project, List<string> filenames, Platform platform)
		{
            if ( m_compileFileBackgroundWorker.IsBusy || m_compileListBackgroundWorker.IsBusy )
            {
                return;
            }

			m_compileProjectFile = project;

			//CompileList( filenames, false );
            BackgroundWorkerStart bwStart = new BackgroundWorkerStart(filenames, platform, false);

            if ( this.RunAsync )
            {
                m_compileListBackgroundWorker.RunWorkerAsync( bwStart );
            }
            else
            {
                DoWorkEventArgs doWork = new DoWorkEventArgs( bwStart );
                compileListBackgroundWorker_DoWork( this, doWork );

                RunWorkerCompletedEventArgs runWorkerCompleted = new RunWorkerCompletedEventArgs( doWork.Result, null, false );
                backgroundWorker_RunWorkerCompleted( this, runWorkerCompleted );
            }
        }

		/// <summary>
		/// Cleans then compiles a list of project files
		/// </summary>
		/// <param name="project"></param>
		/// <param name="filenames"></param>
        public void RecompileProject(string project, List<string> filenames, Platform platform)
		{
            if ( m_compileFileBackgroundWorker.IsBusy || m_compileListBackgroundWorker.IsBusy )
            {
                return;
            }

			m_compileProjectFile = project;

			//CompileList( filenames, true );
            BackgroundWorkerStart bwStart = new BackgroundWorkerStart(filenames, platform, true);

            if ( this.RunAsync )
            {
                m_compileListBackgroundWorker.RunWorkerAsync( bwStart );
            }
            else
            {
                DoWorkEventArgs doWork = new DoWorkEventArgs( bwStart );
                compileListBackgroundWorker_DoWork( this, doWork );

                RunWorkerCompletedEventArgs runWorkerCompleted = new RunWorkerCompletedEventArgs( doWork.Result, null, false );
                backgroundWorker_RunWorkerCompleted( this, runWorkerCompleted );
            }
        }

		/// <summary>
		/// Converts a project file name to the compiled project files list name
		/// </summary>
		/// <param name="projectFile"></param>
		/// <returns></returns>
		public string BuildProjectListFileName( string projectFile )
		{
			string filename = projectFile.Replace( ".scproj", "" );
			filename += ".scolist";
			return filename;
		}

		/// <summary>
		/// Converts a project file name to the 'compile_one' batch file name
		/// </summary>
		/// <param name="projectFile"></param>
		/// <returns></returns>
		public string BuildProjectBatchFileName( string projectFile )
		{
			string path = Path.GetDirectoryName( projectFile );
			string shortName = Path.GetFileNameWithoutExtension( projectFile );
			string filename = path + @"\compile_one." + shortName + ".bat";
			return filename;
        }

        public bool ParseCompileFileLine( string line, ref string filename )
        {
            Match match = this.m_filenameRegex.Match( line );
            if ( match.Success )
            {
                filename = match.Result( "${File}" );
                filename = Path.GetFullPath( filename );

                return true;
            }

            return false;
        }

        public bool ParseCompileOutputLine( string line, ref ErrorType type, ref string filename, ref int lineNumber, ref string text )
        {
            Match compileErrorMatch = CompileErrorRegex.Match( line );
            if (compileErrorMatch.Success)
            {
                string typeStr = compileErrorMatch.Result("${Type}");
                if (!Enum.TryParse(typeStr.TrimEnd(':'), true, out type))
                {
                    type = ErrorType.Errors;
                }

                filename = compileErrorMatch.Result("${File}");

                try
                {
                    filename = Path.GetFullPath(filename);

                    string lineNumStr = compileErrorMatch.Result("${lineNum}");
                    lineNumber = Convert.ToInt32(lineNumStr) - 1;

                    text = compileErrorMatch.Result("${Text}").Trim();
                    string logChannelStr = compileErrorMatch.Result("${LogChannel}");
                    if (!String.IsNullOrEmpty(logChannelStr))
                    {
                        text = $"[{logChannelStr}]: {text}";
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    ApplicationSettings.Log.Warning($"Caught an exception while parsing error line. Giving up on parse. Regex may not have matched properly. {ex.ToString()}");
                    return false;
                }
            }
            else
            {
                Match includedFromHereMatch = IncludedFromHereRegex.Match(line);
                if (includedFromHereMatch.Success)
                {
                    type = ErrorType.Info;
                    filename = includedFromHereMatch.Result("${File}");

                    try
                    {
                        filename = Path.GetFullPath(filename);

                        string lineNumStr = includedFromHereMatch.Result("${lineNum}");
                        lineNumber = Convert.ToInt32(lineNumStr) - 1;

                        text = "        ... included from here";

                        return true;
                    }
                    catch
                    {
                        // Don't worry about this if it fails, not that important.
                        return false;
                    }
                }
            }

            return false;
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Compiles one file using the BuildCompilerCommand, sending the resulting output to the event handlers
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="platform">The platform to resource.</param>
        /// <returns><see cref="CompileResult"/></returns>
        private CompileResult CompileOneFile( string filename, Platform platform )
        {
            if (String.IsNullOrEmpty(filename) == true)
                return CompileResult.NothingToCompile;

            if (this.BuildCompilerCommand == null)
                return CompileResult.NothingToCompile;

            string cmd = this.OnBuildCompilerCommand(filename, platform);
            if ( String.IsNullOrEmpty( cmd ) )
            {
                OnCompilerFailure( "Cannot compile a file of type '" + Path.GetExtension( filename ) + "'." );
                return CompileResult.Failure;
            }

            // setup the process start info
            ProcessStartInfo startInfo = ApplicationSettings.BuildProcessInfoFromCommand( cmd );
            if ( startInfo == null )
            {
                OnCompilerFailure( "Invalid compiler command:\n\n" + cmd );
                return CompileResult.Failure;
            }

            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.CreateNoWindow = true;

            // check if file exists before proceeding
            if ( !File.Exists( startInfo.FileName ) )
            {
                OnCompilerFailure( "The compiler executable '" + startInfo.FileName + "' does not exist." );
                return CompileResult.Failure;
            }
            else
            {
                // make sure destination directory exists
                string outputFilename = m_userEditorSettings.CurrentCompilingSettings.BuildOutputFilename(filename);

                //HACK: Change extension to .sco since the above function doesn't appear to have any knowledge to do that.
                if (0 == String.Compare(Path.GetExtension(outputFilename), ".sc", true))
                {
                    outputFilename = Path.ChangeExtension(outputFilename, ".sco");
                }

                if ( Path.GetDirectoryName( filename ).ToLower() != Path.GetDirectoryName( outputFilename ).ToLower() )
                {
                    try
                    {
                        if ( !Directory.Exists( Path.GetDirectoryName( outputFilename ) ) )
                        {
                            Directory.CreateDirectory( Path.GetDirectoryName( outputFilename ) );
                        }
                    }
                    catch ( Exception e )
                    {
                        OnCompilerFailure( "Could not create Output Directory\n\n" + e.ToString() );
                        return CompileResult.Failure;
                    }
                }

                if ( Directory.Exists( Path.GetDirectoryName( filename ) ) )
                {
                    startInfo.WorkingDirectory = Path.GetDirectoryName( filename );
                }

                // try and start the compiling process:
                string result = string.Empty;
                try
                {
                    // must start after read thread has been started!!!
                    Process p = Process.Start( startInfo );

                    result = p.StandardOutput.ReadToEnd();

                    p.WaitForExit();
                    if (p.ExitCode != 0)
                    {
                        m_numCompileErrors++;
                    }
                }
                catch ( Exception e )
                {
                    OnCompilerFailure( "Unable to start compiling process.\n\n" + e.ToString() );
                    return CompileResult.Failure;
                }

                int errors = OnProcessOutputString( result );
                m_numCompileErrors += errors;

                //After compilation, ensure that the scripts are resourced.
                if (errors == 0 && String.IsNullOrWhiteSpace(m_userEditorSettings.CurrentCompilingSettings.ResourceExecutable) == false)
                {
                     // NOTE: Route the currently generated .sco file as an input to the resource compiler, which will output a file
                    // in <output_directory>\<platform_folder>\<script_name>.<platform_extension>.
                    string outputDirectory = Path.GetDirectoryName(outputFilename);
                    string fileName = Path.GetFileNameWithoutExtension(outputFilename);
                    string platformExtension = this.m_userEditorSettings.CurrentCompilingSettings.GetResourcePlatformExtension(platform);
                    string platformOutputPath = Path.Combine(outputDirectory, platform.ToString(), $"{fileName}{platformExtension}");

                    cmd = this.OnResourceCompilerCommand(outputFilename, platformOutputPath, platform);

                    if (!String.IsNullOrEmpty(cmd))
                    {
                        // setup the process start info
                        startInfo = ApplicationSettings.BuildProcessInfoFromCommand(cmd);
                        if (startInfo == null)
                        {
                            OnCompilerFailure($"Invalid resource compiler command:\n\n{cmd}");
                            return CompileResult.Failure;
                        }

                        startInfo.UseShellExecute = false;
                        startInfo.RedirectStandardOutput = true;
                        startInfo.RedirectStandardError = true;
                        startInfo.CreateNoWindow = true;

                        // check if file exists before proceeding
                        if (!File.Exists(startInfo.FileName))
                        {
                            OnCompilerFailure($"The resource compiler executable '{startInfo.FileName}' does not exist.");
                            return CompileResult.Failure;
                        }

                        // try and start the compiling process:
                        try
                        {
                            // must start after read thread has been started!!!
                            Process p = Process.Start(startInfo);

                            result = p.StandardOutput.ReadToEnd();
                        }
                        catch (Exception e)
                        {
                            OnCompilerFailure($"Unable to start compiling process.\n\n{e}");
                            return CompileResult.Failure;
                        }

                        // To have gotten to this point, we must have had no errors to begin with.
                        errors = OnProcessOutputString(result);
                        m_numCompileErrors += errors;
                    }
                }

                if (m_numCompileErrors > 0)
                    return CompileResult.BreakOnError;

                return CompileResult.Complete;
            }
        }

        /// <summary>
        /// Advances the compile file index for a list of files, skipping files
        /// that are not compilable and files that don't need to be recompiled.
        /// </summary>
        /// <param name="platform">The platform to resource.</param>
        /// <returns></returns>
        private CompileResult ProcessFiles(List<string> filenames, Platform platform, ref int currentIndex)
        {
            if ( filenames == null )
            {
                return CompileResult.Nothing;
            }

            ICollection<string> notFoundFilenames = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            bool upToDate = true;
            while ( currentIndex < filenames.Count )
            {
                // skip non-compiling files and files older than the last time we compiled
                string file = filenames[currentIndex] as string;
                if ( !File.Exists( file ) )
                {
                    notFoundFilenames.Add(file);
                    ++currentIndex;
                    continue;
                }
                else if ( !OnIsFileCompilable( file ) )
                {
                    ++currentIndex;
                    continue;
                }
                else if (!OnFileNeedsToBeCompiled(file) && !OnFileNeedsToBeResourced(file, platform))
                {
                    OnFileUpToDate( file );

                    ++currentIndex;
                    continue;
                }

                //Verify that the outputs are there.
                string filename = filenames[currentIndex] as string;
                IEnumerable<string> outputFilenames = m_userEditorSettings.CurrentCompilingSettings.BuildOutputFilenames(filename, platform);
                if (outputFilenames != null)
                {
                    bool compile = true;
                    foreach (string outputFilename in outputFilenames)
                    {
                        if (!OnOkToCompile(outputFilename))
                        {
                            //Was unable to verify that it's okay to compile this file.
                            compile = false;
                        }
                    }

                    if (compile == true)
                    {
                        upToDate = false;
                        CompileResult compileResult = CompileOneFile(filenames[currentIndex], platform);

                        // Can only stop the compile in between files
                        if (this.StopCompile)
                        {
                            OnFilesNotFound( notFoundFilenames );
                            return CompileResult.Cancelled;
                        }
                        else if (this.BreakOnCompileError && compileResult == CompileResult.BreakOnError)
                        {
                            OnFilesNotFound( notFoundFilenames );
                            return CompileResult.BreakOnError;
                        }
                    }

                    ++currentIndex;
                }
            }

            OnFilesNotFound( notFoundFilenames );

            if (upToDate == true)
                return CompileResult.ListUpToDate;
            else
                return CompileResult.Complete;
        }

        /// <summary>
        /// Saves the batch file and list file for a project that was successfully compiled
        /// </summary>
        /// <param name="listname"></param>
        /// <param name="compiledFiles"></param>
        /// <param name="batchname"></param>
        private void SaveProjectCompiledFiles( string listname, List<string> compiledFiles, string batchname )
        {
            if ( BuildCompilerCommand == null )
            {
                return;
            }

            // save .scolist
            if ( OnOkToCompile( listname ) )
            {
                System.IO.StreamWriter listWriter = new StreamWriter( listname );
                foreach ( string file in compiledFiles )
                {
                    if ( OnIsFileCompilable( file ) )
                    {
                        string outputFile = m_userEditorSettings.CurrentCompilingSettings.BuildOutputFilename(file);
                        listWriter.WriteLine( outputFile + "o" );
                    }
                }
                listWriter.Close();
            }

            // save .bat
            if ( OnOkToCompile( batchname ) )
            {
                string cmd = this.OnBuildCompilerCommand( "%1", this.m_userEditorSettings.CurrentPlatform);
                System.Diagnostics.Debug.Assert( cmd != null, "cmd != null" );

                System.IO.StreamWriter batWriter = new StreamWriter( batchname );
                batWriter.WriteLine( "@echo off" );
                batWriter.WriteLine( cmd );
                batWriter.Close();
            }
        }

        /// <summary>
        /// Checks whether the -noconfig commandline parameter was passed in.
        /// </summary>
        /// <returns></returns>
        private bool UseToolsConfig()
        {
            bool useConfig = true;

            string[] args = Environment.GetCommandLineArgs();
            foreach (String str in args)
            {
                if (str == "-noconfig")
                {
                    useConfig = false;
                    break;
                }
            }
            return useConfig;
        }
        #endregion

        #region BackgroundWorker Event Handlers
        private void compileFileBackgroundWorker_DoWork( object sender, DoWorkEventArgs e )
        {
            e.Result = CompileResult.Nothing;

            BackgroundWorkerStart bwStart = e.Argument as BackgroundWorkerStart;
            if ( (bwStart != null) && (bwStart.Filenames.Count > 0) )
            {
                m_backgroundWorkerStart = bwStart;

                if ( !OnIsFileCompilable( bwStart.Filenames[0] ) )
                {
                    e.Result = CompileResult.NothingToCompile;
                    return;
                }

                // perform the precompile step
                OnPreCompileStep();

                bool ok = OnOkToCompile( bwStart.Filenames[0] + "o" ) && OnOkToCompile( bwStart.Filenames[0] + "d" );
                if ( ok )
                {
                    e.Result = CompileOneFile( bwStart.Filenames[0], bwStart.Platform );
                }
            }
        }

        private void compileListBackgroundWorker_DoWork( object sender, DoWorkEventArgs e )
        {
            e.Result = CompileResult.Nothing;

            BackgroundWorkerStart bwStart = e.Argument as BackgroundWorkerStart;
            if ( (bwStart != null) && (bwStart.Filenames.Count > 0) )
            {
                m_backgroundWorkerStart = bwStart;

                // perform the precompile step
                OnPreCompileStep();

                // build list of the compiler output files
                List<string> outputFilenames = new List<string>();
                foreach (string file in bwStart.Filenames)
                {
                    IEnumerable<string> filenames = m_userEditorSettings.CurrentCompilingSettings.BuildOutputFilenames(file, bwStart.Platform);

                    if (filenames != null)
                    {
                        outputFilenames.AddRange(filenames);
                    }
                }

                if (m_compileProjectFile != null)
                {
                    outputFilenames.Add(this.BuildProjectBatchFileName(m_compileProjectFile));
                    outputFilenames.Add(this.BuildProjectListFileName(m_compileProjectFile));
                }

                String outputDir = ApplicationSettings.GetAbsolutePath(m_userEditorSettings.CurrentCompilingSettings.OutputDirectory);

                // clean
                if ( bwStart.Rebuild )
                {
                    // look for the compiler output files and delete them
                    foreach ( string file in outputFilenames )
                    {
                        if ( OnOkToCompile( file ) && File.Exists( file ) )
                        {
                            try
                            {
                                File.Delete( file );
                            }
                            catch ( Exception ex )
                            {
                                LogFactory.ApplicationLog.ToolException(ex, "File Delete Error");
                            }
                        }
                    }

                    if (Directory.Exists(outputDir))
                    {
                        //Clear out all other remaining files.  Not just the ones we're outputting.
                        IEnumerable<String> remainingFiles = Directory.EnumerateFiles(outputDir);
                        foreach (String remainingFile in remainingFiles)
                        {
                            try
                            {
                                File.Delete(remainingFile);
                            }
                            catch (Exception ex)
                            {
                                LogFactory.ApplicationLog.ToolException(ex, "Directory Delete Error");
                            }
                        }
                    }
                }
                else if (Directory.Exists(outputDir))
                {
                    // Determine which pre-existing output files have been removed from the project, and delete them. This way they won't be packaged into the final payload.
                    IEnumerable<String> extraFilesInOutputDirectory = Directory.EnumerateFiles(outputDir)
                        .Except(outputFilenames, StringComparer.OrdinalIgnoreCase);
                    foreach (String extraFile in extraFilesInOutputDirectory)
                    {
                        try
                        {
                            File.Delete(extraFile);
                        }
                        catch (Exception ex)
                        {
                            LogFactory.ApplicationLog.ToolException(ex, "Directory Delete Error");
                        }
                    }
                }

                // Process all current files.
                int currentIndex = 0;
                e.Result = ProcessFiles(bwStart.Filenames, bwStart.Platform, ref currentIndex);
            }
        }

        private void backgroundWorker_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            CompileResult result = (CompileResult)e.Result;

            if ( e.Error != null )
            {
                OnCompilerFailure($"Unhandled exception\n\n{e.Error.ToString()}");
            }
            else if ( e.Cancelled )
            {
                OnCompileCancelled( m_numCompileErrors );
            }
            else
            {
                switch ( result )
                {
                    case CompileResult.BreakOnError:
                        OnBreakOnError( m_numCompileErrors );
                        break;
                    case CompileResult.Cancelled:
                        OnCompileCancelled( m_numCompileErrors );
                        break;
                    case CompileResult.Complete:
                        {
                            string listname = null;
                            if ( m_compileProjectFile != null )
                            {
                                listname = BuildProjectListFileName( m_compileProjectFile );

                                // Save compiled files list if there were no errors
                                if ( m_numCompileErrors == 0 )
                                {
                                    string batchname = BuildProjectBatchFileName( m_compileProjectFile );
                                    SaveProjectCompiledFiles( listname, m_backgroundWorkerStart.Filenames, batchname );
                                }
                            }

                            OnCompileComplete( m_numCompileErrors, listname );
                        }
                        break;
                    case CompileResult.Failure:
                        OnCompilerFailure(null);
                        break;
                    case CompileResult.ListUpToDate:
                        OnListUpToDate( m_compileProjectFile != null ? BuildProjectListFileName( m_compileProjectFile ) : null );
                        break;
                    case CompileResult.Nothing:
                        // do nothing
                        break;
                    case CompileResult.NothingToCompile:
                        OnNothingToCompile();
                        break;
                }
            }

            m_numCompileErrors = 0;
            m_compileProjectFile = null;
            m_backgroundWorkerStart = null;
        }
        #endregion

        #region Event Dispatchers
        private void OnPreCompileStep()
		{
			if ( PreCompileStep != null )
			{
				if ( m_callingForm != null )
				{
					m_callingForm.Invoke( PreCompileStep );
				}
				else
				{
					PreCompileStep();
				}
			}
		}

		private string OnBuildCompilerCommand(string filename, Platform platform)
		{
			if ( BuildCompilerCommand != null )
			{
				if ( m_callingForm != null )
				{
					object[] args = new object[2];
					args[0] = filename;
                    args[1] = platform;
					return m_callingForm.Invoke( BuildCompilerCommand, args ) as string;
				}
				else
				{
					return BuildCompilerCommand(filename, platform);
				}
			}

			return null;
		}

        private string OnResourceCompilerCommand(string filename, string outputFilename, Platform platform)
        {
            if (ResourceCompilerCommand != null)
            {
                if (m_callingForm != null)
                {
                    object[] args = new object[3];
                    args[0] = filename;
                    args[1] = outputFilename;
                    args[2] = platform;
                    return m_callingForm.Invoke(ResourceCompilerCommand, args) as string;
                }
                else
                {
                    return ResourceCompilerCommand(filename, outputFilename, platform);
                }
            }

            return null;
        }

		private bool OnOkToCompile( string filename )
		{
			if ( OkToCompile != null )
			{
				if ( m_callingForm != null )
				{
					object[] args = new object[1];
					args[0] = filename;
					object obj = m_callingForm.Invoke( OkToCompile, args );
					return obj.ToString() == System.Boolean.TrueString;
				}
				else
				{
					return OkToCompile( filename );
				}
			}

			return true;
		}

        private void OnCompileComplete( int numErrors, string projectListFileName )
		{
			if ( CompileComplete != null )
			{
				if ( m_callingForm != null )
				{
					object[] args = new object[2];
					args[0] = numErrors;
					args[1] = projectListFileName;
					m_callingForm.Invoke( CompileComplete, args );
				}
				else
				{
					CompileComplete( numErrors, projectListFileName );
				}
			}
		}

        private void OnCompileCancelled( int numErrors )
        {
            if ( CompileCancelled != null )
            {
                if ( m_callingForm != null )
                {
                    object[] args = new object[1];
                    args[0] = numErrors;
                    m_callingForm.Invoke( CompileCancelled, args );
                }
                else
                {
                    CompileCancelled( numErrors );
                }
            }
        }

        private void OnBreakOnError( int numErrors )
        {
            if ( BreakOnError != null )
            {
                if ( m_callingForm != null )
                {
                    object[] args = new object[1];
                    args[0] = numErrors;
                    m_callingForm.Invoke( BreakOnError, args );
                }
                else
                {
                    BreakOnError( numErrors );
                }
            }
        }

        private void OnCompilerFailure( string error )
		{
			if ( CompilerFailure != null )
			{
				if ( m_callingForm != null )
				{
					object[] args = new string[1];
					args[0] = error;
					m_callingForm.Invoke( CompilerFailure, args );
				}
				else
				{
					CompilerFailure( error );
				}
			}
		}

		private void OnFilesNotFound( IEnumerable<string> filenames )
		{
			if ( FilesNotFound != null )
			{
				if ( m_callingForm != null )
				{
					object[] args = new object[1];
					args[0] = filenames;
                    m_callingForm.Invoke( FilesNotFound, args );
				}
				else
				{
                    FilesNotFound( filenames );
				}
			}
		}

		private void OnFileUpToDate( string filename )
		{
			if ( FileUpToDate != null )
			{
				if ( m_callingForm != null )
				{
					object[] args = new string[1];
					args[0] = filename;
					m_callingForm.Invoke( FileUpToDate, args );
				}
				else
				{
					FileUpToDate( filename );
				}
			}
		}

		private void OnListUpToDate( string projectListFileName )
		{
			if ( ListUpToDate != null )
			{
				if ( m_callingForm != null )
				{
					object[] args = new string[1];
					args[0] = projectListFileName;
					m_callingForm.Invoke( ListUpToDate, args );
				}
				else
				{
					ListUpToDate( projectListFileName );
				}
			}
		}

		private void OnNothingToCompile()
		{
			if ( NothingToCompile != null )
			{
				if ( m_callingForm != null )
				{
					m_callingForm.Invoke( NothingToCompile );
				}
				else
				{
					NothingToCompile();
				}
			}
		}

		private int OnProcessOutputString( string output )
		{
			if ( ProcessOutputString != null )
			{
				if ( m_callingForm != null )
				{
					object[] args = new object[1];
					args[0] = output;
					object obj = m_callingForm.Invoke( ProcessOutputString, args );
					return Convert.ToInt32( obj.ToString() );
				}
				else
				{
					return ProcessOutputString( output );
				}
			}

			return 0;
		}

        private bool OnIsFileCompilable( string filename )
        {
            if ( IsFileCompilable != null )
            {
                if ( m_callingForm != null )
                {
                    object[] args = new object[1];
                    args[0] = filename;
                    object obj = m_callingForm.Invoke( IsFileCompilable, args );
                    return obj.ToString() == System.Boolean.TrueString;
                }
                else
                {
                    return IsFileCompilable( filename );
                }
            }

            return false;
        }

        private bool OnFileNeedsToBeCompiled( string filename )
        {
            if ( FileNeedsToBeCompiled != null )
            {
                if ( m_callingForm != null )
                {
                    object[] args = new object[3];
                    args[0] = filename;
                    args[1] = m_compileProjectFile;
                    args[2] = m_userEditorSettings.CurrentCompilingSettings;
                    object obj = m_callingForm.Invoke( FileNeedsToBeCompiled, args );
                    return obj.ToString() == System.Boolean.TrueString;
                }
                else
                {
                    return FileNeedsToBeCompiled( filename, m_compileProjectFile, m_userEditorSettings.CurrentCompilingSettings );
                }
            }

            return false;
        }

        private bool OnFileNeedsToBeResourced(String filename, Platform platform)
        {
            if (FileNeedsToBeResourced != null)
            {
                if (m_callingForm != null)
                {
                    object[] args = new object[4];
                    args[0] = filename;
                    args[1] = platform;
                    args[2] = m_compileProjectFile;
                    args[3] = m_userEditorSettings.CurrentCompilingSettings;
                    object obj = m_callingForm.Invoke(FileNeedsToBeResourced, args);
                    return obj.ToString() == System.Boolean.TrueString;
                }
                else
                {
                    return FileNeedsToBeResourced(filename, platform, m_compileProjectFile, m_userEditorSettings.CurrentCompilingSettings);
                }
            }

            return false;
        }
        #endregion

        #region Helper Classes
        class BackgroundWorkerStart
        {
            public BackgroundWorkerStart( List<string> filenames, Platform platform, bool rebuild )
            {
                // convert to the full path
                foreach ( string filename in filenames )
                {
                    m_filenames.Add( Path.GetFullPath( filename ) );
                }

                m_platform = platform;
                m_rebuild = rebuild;
            }

            #region Variables
            private List<string> m_filenames = new List<string>();
            private Platform m_platform;
            private bool m_rebuild;
            #endregion

            #region Properties
            public List<string> Filenames
            {
                get
                {
                    return m_filenames;
                }
            }

            public Platform Platform
            {
                get { return m_platform; }
            }

            public bool Rebuild
            {
                get
                {
                    return m_rebuild;
                }
            }
            #endregion
        };
        #endregion
    }
}
