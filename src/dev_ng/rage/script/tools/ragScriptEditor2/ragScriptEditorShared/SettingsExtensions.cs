﻿// ---------------------------------------------------------------------------------------------
// <copyright file="SettingsExtensions.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2022. All rights reserved.
// </copyright>
// ---------------------------------------------------------------------------------------------

namespace ragScriptEditorShared
{
    using RSG.Configuration;
    using RSG.Platform;

    /// <summary>
    /// Extension methods for settings classes.
    /// </summary>
    public static class SettingsExtensions
    {
        /// <summary>
        /// Import the settings into the given environment for the currently selected platform.
        /// </summary>
        /// <remarks>If platform is null, it will not be imported.</remarks>
        public static void Import(this CompilingSettings settings, IEnvironment env, Platform? platform)
        {
            env.Add("OutputDirectory", settings.OutputDirectory);
            env.Add("BuildDir", settings.BuildDirectory);
            env.Add("Custom", settings.Custom);
            env.Add("XmlIncludeFile", settings.XmlIncludeFile);
            env.Add("ConfigurationName", settings.ConfigurationName);
            env.Add("CompilerExecutable", settings.CompilerExecutable);
            env.Add("PostCompileCommand", settings.PostCompileCommand);

            if (platform.HasValue)
            {
                env.Add("PlatformName", platform.ToString());
            }
        }
    }
}
