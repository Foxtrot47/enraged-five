namespace ragScriptEditorShared
{
    partial class NonProjectProjectSettingsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ragScriptEditorShared.ProjectSettings projectSettings1 = new ragScriptEditorShared.ProjectSettings();
            this.enableSemanticParsingCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // enableSemanticParsingCheckBox
            // 
            this.enableSemanticParsingCheckBox.AutoSize = true;
            this.enableSemanticParsingCheckBox.Location = new System.Drawing.Point( 3, 78 );
            this.enableSemanticParsingCheckBox.Name = "enableSemanticParsingCheckBox";
            this.enableSemanticParsingCheckBox.Size = new System.Drawing.Size( 273, 17 );
            this.enableSemanticParsingCheckBox.TabIndex = 14;
            this.enableSemanticParsingCheckBox.Text = "Enable Semantic Parsing Service during project load";
            this.enableSemanticParsingCheckBox.UseVisualStyleBackColor = true;
            this.enableSemanticParsingCheckBox.CheckedChanged += new System.EventHandler( this.enableSemanticParsingCheckBox_CheckedChanged );
            // 
            // NonProjectProjectSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.Controls.Add( this.enableSemanticParsingCheckBox );
            this.Name = "NonProjectProjectSettingsControl";
            this.Size = new System.Drawing.Size( 346, 104 );
            projectSettings1.AutomaticSorting = false;
            projectSettings1.PostCompileCommand = "";
            projectSettings1.StartDebugCommand = "";
            this.TabSettings = projectSettings1;
            this.Controls.SetChildIndex( this.enableSemanticParsingCheckBox, 0 );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox enableSemanticParsingCheckBox;
    }
}
