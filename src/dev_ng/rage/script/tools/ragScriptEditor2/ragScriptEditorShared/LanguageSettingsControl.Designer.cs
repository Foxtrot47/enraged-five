namespace ragScriptEditorShared
{
    partial class LanguageSettingsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveDefaultSettingsButton = new System.Windows.Forms.Button();
            this.loadDefaultSettingsButton = new System.Windows.Forms.Button();
            this.lineSeparatorGroupBox = new System.Windows.Forms.GroupBox();
            this.codeSnippetPathBrowseButton = new System.Windows.Forms.Button();
            this.codeSnippetPathTextBox = new System.Windows.Forms.TextBox();
            this.codeSnippetPathLabel = new System.Windows.Forms.Label();
            this.cacheDirectoryBrowseButton = new System.Windows.Forms.Button();
            this.cacheDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.cacheDirectoryLabel = new System.Windows.Forms.Label();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.clearCacheButton = new System.Windows.Forms.Button();
            this.lineSeparatorGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // saveDefaultSettingsButton
            // 
            this.saveDefaultSettingsButton.AutoSize = true;
            this.saveDefaultSettingsButton.Location = new System.Drawing.Point( 3, 3 );
            this.saveDefaultSettingsButton.Name = "saveDefaultSettingsButton";
            this.saveDefaultSettingsButton.Size = new System.Drawing.Size( 120, 23 );
            this.saveDefaultSettingsButton.TabIndex = 0;
            this.saveDefaultSettingsButton.Text = "Save Default Settings";
            this.saveDefaultSettingsButton.UseVisualStyleBackColor = true;
            this.saveDefaultSettingsButton.Click += new System.EventHandler( this.saveDefaultSettingsButton_Click );
            // 
            // loadDefaultSettingsButton
            // 
            this.loadDefaultSettingsButton.AutoSize = true;
            this.loadDefaultSettingsButton.Location = new System.Drawing.Point( 129, 3 );
            this.loadDefaultSettingsButton.Name = "loadDefaultSettingsButton";
            this.loadDefaultSettingsButton.Size = new System.Drawing.Size( 119, 23 );
            this.loadDefaultSettingsButton.TabIndex = 1;
            this.loadDefaultSettingsButton.Text = "Load Default Settings";
            this.loadDefaultSettingsButton.UseVisualStyleBackColor = true;
            this.loadDefaultSettingsButton.Click += new System.EventHandler( this.loadDefaultSettingsButton_Click );
            // 
            // lineSeparatorGroupBox
            // 
            this.lineSeparatorGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lineSeparatorGroupBox.Controls.Add( this.codeSnippetPathBrowseButton );
            this.lineSeparatorGroupBox.Controls.Add( this.codeSnippetPathTextBox );
            this.lineSeparatorGroupBox.Controls.Add( this.codeSnippetPathLabel );
            this.lineSeparatorGroupBox.Controls.Add( this.cacheDirectoryBrowseButton );
            this.lineSeparatorGroupBox.Controls.Add( this.cacheDirectoryTextBox );
            this.lineSeparatorGroupBox.Controls.Add( this.cacheDirectoryLabel );
            this.lineSeparatorGroupBox.Location = new System.Drawing.Point( -5, 32 );
            this.lineSeparatorGroupBox.Name = "lineSeparatorGroupBox";
            this.lineSeparatorGroupBox.Size = new System.Drawing.Size( 493, 69 );
            this.lineSeparatorGroupBox.TabIndex = 2;
            this.lineSeparatorGroupBox.TabStop = false;
            // 
            // codeSnippetPathBrowseButton
            // 
            this.codeSnippetPathBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.codeSnippetPathBrowseButton.Location = new System.Drawing.Point( 455, 39 );
            this.codeSnippetPathBrowseButton.Name = "codeSnippetPathBrowseButton";
            this.codeSnippetPathBrowseButton.Size = new System.Drawing.Size( 25, 23 );
            this.codeSnippetPathBrowseButton.TabIndex = 5;
            this.codeSnippetPathBrowseButton.Text = "...";
            this.codeSnippetPathBrowseButton.UseVisualStyleBackColor = true;
            this.codeSnippetPathBrowseButton.Click += new System.EventHandler( this.codeSnippetPathBrowseButton_Click );
            // 
            // codeSnippetPathTextBox
            // 
            this.codeSnippetPathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.codeSnippetPathTextBox.Location = new System.Drawing.Point( 110, 39 );
            this.codeSnippetPathTextBox.Name = "codeSnippetPathTextBox";
            this.codeSnippetPathTextBox.Size = new System.Drawing.Size( 339, 20 );
            this.codeSnippetPathTextBox.TabIndex = 4;
            this.codeSnippetPathTextBox.TextChanged += new System.EventHandler( this.SettingChanged );
            // 
            // codeSnippetPathLabel
            // 
            this.codeSnippetPathLabel.AutoSize = true;
            this.codeSnippetPathLabel.Location = new System.Drawing.Point( 8, 44 );
            this.codeSnippetPathLabel.Name = "codeSnippetPathLabel";
            this.codeSnippetPathLabel.Size = new System.Drawing.Size( 96, 13 );
            this.codeSnippetPathLabel.TabIndex = 3;
            this.codeSnippetPathLabel.Text = "Code Snippet Path";
            // 
            // cacheDirectoryBrowseButton
            // 
            this.cacheDirectoryBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cacheDirectoryBrowseButton.Location = new System.Drawing.Point( 455, 11 );
            this.cacheDirectoryBrowseButton.Name = "cacheDirectoryBrowseButton";
            this.cacheDirectoryBrowseButton.Size = new System.Drawing.Size( 25, 23 );
            this.cacheDirectoryBrowseButton.TabIndex = 2;
            this.cacheDirectoryBrowseButton.Text = "...";
            this.cacheDirectoryBrowseButton.UseVisualStyleBackColor = true;
            this.cacheDirectoryBrowseButton.Click += new System.EventHandler( this.cacheDirectoryBrowseButton_Click );
            // 
            // cacheDirectoryTextBox
            // 
            this.cacheDirectoryTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cacheDirectoryTextBox.Location = new System.Drawing.Point( 110, 13 );
            this.cacheDirectoryTextBox.Name = "cacheDirectoryTextBox";
            this.cacheDirectoryTextBox.Size = new System.Drawing.Size( 339, 20 );
            this.cacheDirectoryTextBox.TabIndex = 1;
            this.cacheDirectoryTextBox.TextChanged += new System.EventHandler( this.SettingChanged );
            // 
            // cacheDirectoryLabel
            // 
            this.cacheDirectoryLabel.AutoSize = true;
            this.cacheDirectoryLabel.Location = new System.Drawing.Point( 21, 16 );
            this.cacheDirectoryLabel.Name = "cacheDirectoryLabel";
            this.cacheDirectoryLabel.Size = new System.Drawing.Size( 83, 13 );
            this.cacheDirectoryLabel.TabIndex = 0;
            this.cacheDirectoryLabel.Text = "Cache Directory";
            // 
            // clearCacheButton
            // 
            this.clearCacheButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.clearCacheButton.AutoSize = true;
            this.clearCacheButton.Location = new System.Drawing.Point( 400, 3 );
            this.clearCacheButton.Name = "clearCacheButton";
            this.clearCacheButton.Size = new System.Drawing.Size( 75, 23 );
            this.clearCacheButton.TabIndex = 3;
            this.clearCacheButton.Text = "Clear Cache";
            this.clearCacheButton.UseVisualStyleBackColor = true;
            this.clearCacheButton.Click += new System.EventHandler( this.clearCacheButton_Click );
            // 
            // LanguageSettingsControl
            // 
            this.Controls.Add( this.clearCacheButton );
            this.Controls.Add( this.lineSeparatorGroupBox );
            this.Controls.Add( this.loadDefaultSettingsButton );
            this.Controls.Add( this.saveDefaultSettingsButton );
            this.Name = "LanguageSettingsControl";
            this.Size = new System.Drawing.Size( 478, 110 );
            this.lineSeparatorGroupBox.ResumeLayout( false );
            this.lineSeparatorGroupBox.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button saveDefaultSettingsButton;
        private System.Windows.Forms.Button loadDefaultSettingsButton;
        private System.Windows.Forms.GroupBox lineSeparatorGroupBox;
        private System.Windows.Forms.Button codeSnippetPathBrowseButton;
        private System.Windows.Forms.TextBox codeSnippetPathTextBox;
        private System.Windows.Forms.Label codeSnippetPathLabel;
        private System.Windows.Forms.Button cacheDirectoryBrowseButton;
        private System.Windows.Forms.TextBox cacheDirectoryTextBox;
        private System.Windows.Forms.Label cacheDirectoryLabel;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button clearCacheButton;


    }
}
