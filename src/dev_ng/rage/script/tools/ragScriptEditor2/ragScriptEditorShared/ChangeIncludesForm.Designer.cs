namespace ragScriptEditorShared
{
    partial class ChangeIncludesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.includeFileLabel = new System.Windows.Forms.Label();
            this.includeFileTextBox = new System.Windows.Forms.TextBox();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.applyButton = new System.Windows.Forms.Button();
            this.browseIncludeFileButton = new System.Windows.Forms.Button();
            this.includePathLabel = new System.Windows.Forms.Label();
            this.includePathTextBox = new System.Windows.Forms.TextBox();
            this.browseIncludePathButton = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.resetToDefaultsButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // includeFileLabel
            // 
            this.includeFileLabel.AutoSize = true;
            this.includeFileLabel.Location = new System.Drawing.Point( 18, 15 );
            this.includeFileLabel.Name = "includeFileLabel";
            this.includeFileLabel.Size = new System.Drawing.Size( 61, 13 );
            this.includeFileLabel.TabIndex = 0;
            this.includeFileLabel.Text = "Include File";
            // 
            // includeFileTextBox
            // 
            this.includeFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.includeFileTextBox.Location = new System.Drawing.Point( 85, 12 );
            this.includeFileTextBox.Name = "includeFileTextBox";
            this.includeFileTextBox.Size = new System.Drawing.Size( 368, 20 );
            this.includeFileTextBox.TabIndex = 1;
            this.includeFileTextBox.TextChanged += new System.EventHandler( this.EnableApplyButtonCheck );
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.Location = new System.Drawing.Point( 249, 69 );
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size( 75, 23 );
            this.okButton.TabIndex = 6;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler( this.okButton_Click );
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 330, 69 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // applyButton
            // 
            this.applyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.applyButton.Location = new System.Drawing.Point( 411, 69 );
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size( 75, 23 );
            this.applyButton.TabIndex = 8;
            this.applyButton.Text = "Apply";
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler( this.applyButton_Click );
            // 
            // browseIncludeFileButton
            // 
            this.browseIncludeFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseIncludeFileButton.Location = new System.Drawing.Point( 459, 10 );
            this.browseIncludeFileButton.Name = "browseIncludeFileButton";
            this.browseIncludeFileButton.Size = new System.Drawing.Size( 27, 23 );
            this.browseIncludeFileButton.TabIndex = 2;
            this.browseIncludeFileButton.Text = "...";
            this.browseIncludeFileButton.UseVisualStyleBackColor = true;
            this.browseIncludeFileButton.Click += new System.EventHandler( this.browseIncludeFileButton_Click );
            // 
            // includePathLabel
            // 
            this.includePathLabel.AutoSize = true;
            this.includePathLabel.Location = new System.Drawing.Point( 12, 43 );
            this.includePathLabel.Name = "includePathLabel";
            this.includePathLabel.Size = new System.Drawing.Size( 67, 13 );
            this.includePathLabel.TabIndex = 3;
            this.includePathLabel.Text = "Include Path";
            // 
            // includePathTextBox
            // 
            this.includePathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.includePathTextBox.Location = new System.Drawing.Point( 85, 40 );
            this.includePathTextBox.Name = "includePathTextBox";
            this.includePathTextBox.Size = new System.Drawing.Size( 368, 20 );
            this.includePathTextBox.TabIndex = 4;
            this.includePathTextBox.TextChanged += new System.EventHandler( this.EnableApplyButtonCheck );
            // 
            // browseIncludePathButton
            // 
            this.browseIncludePathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseIncludePathButton.Location = new System.Drawing.Point( 459, 38 );
            this.browseIncludePathButton.Name = "browseIncludePathButton";
            this.browseIncludePathButton.Size = new System.Drawing.Size( 27, 23 );
            this.browseIncludePathButton.TabIndex = 5;
            this.browseIncludePathButton.Text = "...";
            this.browseIncludePathButton.UseVisualStyleBackColor = true;
            this.browseIncludePathButton.Click += new System.EventHandler( this.browseIncludePathButton_Click );
            // 
            // openFileDialog
            // 
            this.openFileDialog.RestoreDirectory = true;
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.ShowNewFolderButton = false;
            // 
            // resetToDefaultsButton
            // 
            this.resetToDefaultsButton.Location = new System.Drawing.Point( 12, 69 );
            this.resetToDefaultsButton.Name = "resetToDefaultsButton";
            this.resetToDefaultsButton.Size = new System.Drawing.Size( 101, 23 );
            this.resetToDefaultsButton.TabIndex = 9;
            this.resetToDefaultsButton.Text = "Reset to Defaults";
            this.resetToDefaultsButton.UseVisualStyleBackColor = true;
            this.resetToDefaultsButton.Click += new System.EventHandler( this.resetToDefaultsButton_Click );
            // 
            // ChangeIncludesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size( 498, 104 );
            this.Controls.Add( this.resetToDefaultsButton );
            this.Controls.Add( this.browseIncludePathButton );
            this.Controls.Add( this.includePathTextBox );
            this.Controls.Add( this.includePathLabel );
            this.Controls.Add( this.browseIncludeFileButton );
            this.Controls.Add( this.applyButton );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.okButton );
            this.Controls.Add( this.includeFileTextBox );
            this.Controls.Add( this.includeFileLabel );
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangeIncludesForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Change Includes";
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label includeFileLabel;
        private System.Windows.Forms.TextBox includeFileTextBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button applyButton;
        private System.Windows.Forms.Button browseIncludeFileButton;
        private System.Windows.Forms.Label includePathLabel;
        private System.Windows.Forms.TextBox includePathTextBox;
        private System.Windows.Forms.Button browseIncludePathButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button resetToDefaultsButton;
    }
}