using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using ActiproSoftware.SyntaxEditor;
using RSG.Base.Forms;

namespace ragScriptEditorShared
{
    public partial class SearchReplaceInFilesDialog : Form
    {
        public SearchReplaceInFilesDialog()
        {
            InitializeComponent();

            this.lookInComboBox.Items.Clear();
            this.lookInComboBox.Items.Add( c_currentDocument );
            this.lookInComboBox.Items.Add( c_allOpenDocuments );
            this.lookInComboBox.Items.Add( c_entireProject );
            this.lookInComboBox.SelectedIndex = 2;

            this.useComboBox.SelectedIndex = 0;
        }

        public void Init( Control parent, FindReplaceOptions options )
        {
            m_parentControl = parent;
            m_findReplaceOptions = options;

            SetUIFromFindReplaceOptions();

            this.searchReplaceComponent.Init( m_parentControl );
        }

        #region Constants
        private const string c_currentDocument = "Current Document";
        private const string c_allOpenDocuments = "All Open Documents";
        private const string c_entireProject = "Entire Project";
        #endregion

        #region Variables
        private bool m_locationSet = false;
        private Control m_parentControl = null;
        private FindReplaceOptions m_findReplaceOptions = null;

        private string m_lastDirectory = string.Empty;

        private bool m_enableCurrentDocumentSearching = true;
        private bool m_enableAllOpenDocumentsSearching = true;
        private bool m_enableEntireProjectSearching = true;
        private bool m_enableSearching = true;
        #endregion

        #region Events
        public event EventHandler CancelIntellisense;
        public event FindCurrentFilenameEventHandler FindCurrentFilename;
        public event FindOpenFilenamesEventHandler FindOpenFilenames;
        public event FindProjectFilenamesEventHandler FindProjectFilenames;

        public event FoundStringEventHandler FoundString
        {
            add
            {
                this.searchReplaceComponent.FoundString += value;
            }
            remove
            {
                this.searchReplaceComponent.FoundString -= value;
            }
        }

        public event LoadFileEventHandler LoadFile
        {
            add
            {
                this.searchReplaceComponent.LoadFile += value;
            }
            remove
            {
                this.searchReplaceComponent.LoadFile -= value;
            }
        }

        public event ReportProgressEventHandler ReportProgress
        {
            add
            {
                this.searchReplaceComponent.ReportProgress += value;
            }
            remove
            {
                this.searchReplaceComponent.ReportProgress -= value;
            }
        }

        public event SaveDocumentEventHandler SaveDocument
        {
            add
            {
                this.searchReplaceComponent.SaveDocument += value;
            }
            remove
            {
                this.searchReplaceComponent.SaveDocument -= value;
            }
        }

        public event SearchBeginEventHandler SearchBegin
        {
            add
            {
                this.searchReplaceComponent.SearchBegin += value;
            }
            remove
            {
                this.searchReplaceComponent.SearchBegin -= value;
            }
        }

        public event SearchCompleteEventHandler SearchComplete
        {
            add
            {
                this.searchReplaceComponent.SearchComplete += value;
            }
            remove
            {
                this.searchReplaceComponent.SearchComplete -= value;
            }
        }

        public event SyntaxEditorEventHandler FindSyntaxEditor
        {
            add
            {
                this.searchReplaceComponent.FindSyntaxEditor += value;
            }
            remove
            {
                this.searchReplaceComponent.FindSyntaxEditor -= value;
            }
        }
        #endregion

        #region Event Dispatchers
        protected void OnCancelIntellisense()
        {
            if ( this.CancelIntellisense != null )
            {
                if ( m_parentControl != null )
                {
                    m_parentControl.Invoke( this.CancelIntellisense, new object[] { this, EventArgs.Empty } );
                }
                else
                {
                    this.CancelIntellisense( this, EventArgs.Empty );
                }
            }
        }

        protected string OnFindCurrentFilename()
        {
            if ( this.FindCurrentFilename != null )
            {
                SearchReplaceFilenameEventArgs e = new SearchReplaceFilenameEventArgs();
                if ( m_parentControl != null )
                {
                    m_parentControl.Invoke( this.FindCurrentFilename, new object[] { this, e } );
                }
                else
                {
                    this.FindCurrentFilename( this, e );
                }

                return e.Filename;
            }

            return null;
        }

        protected List<string> OnFindOpenFilenames()
        {
            if ( this.FindOpenFilenames != null )
            {
                SearchReplaceFilenamesEventArgs e = new SearchReplaceFilenamesEventArgs();
                if ( m_parentControl != null )
                {
                    m_parentControl.Invoke( this.FindOpenFilenames, new object[] { this, e } );
                }
                else
                {
                    this.FindOpenFilenames( this, e );
                }

                return e.Filenames;
            }

            return null;
        }

        protected List<string> OnFindProjectFilenames()
        {
            if ( this.FindProjectFilenames != null )
            {
                SearchReplaceFilenamesEventArgs e = new SearchReplaceFilenamesEventArgs();
                if ( m_parentControl != null )
                {
                    m_parentControl.Invoke( this.FindProjectFilenames, new object[] { this, e } );
                }
                else
                {
                    this.FindProjectFilenames( this, e );
                }

                return e.Filenames;
            }

            return null;
        }
        #endregion

        #region Properties
        public Control ParentControl
        {
            get
            {
                return m_parentControl;
            }
        }

        public bool LocationSet
        {
            get
            {
                return m_locationSet;
            }
            set
            {
                m_locationSet = value;
            }
        }

        public string FindWhat
        {
            get
            {
                return this.findWhatComboBox.Text;
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ) )
                {
                    int indexOf = this.findWhatComboBox.Items.IndexOf( value );
                    if ( indexOf != -1 )
                    {
                        this.findWhatComboBox.SelectedIndex = indexOf;
                    }
                    else
                    {
                        this.findWhatComboBox.SelectedIndex = -1;
                        this.findWhatComboBox.Text = value;
                    }
                }
                else
                {
                    this.findWhatComboBox.SelectedIndex = -1;
                    this.findWhatComboBox.Text = string.Empty;
                }
            }
        }

        public List<string> FindHistory
        {
            get
            {
                List<string> findHistory = new List<string>();
                foreach ( object item in this.findWhatComboBox.Items )
                {
                    findHistory.Add( item.ToString() );
                }

                return findHistory;
            }
            set
            {
                this.findWhatComboBox.Items.Clear();
                if ( value != null )
                {
                    foreach ( string item in value )
                    {
                        this.findWhatComboBox.Items.Add( item );
                    }
                }
            }
        }

        public string ReplaceWith
        {
            get
            {
                return this.replaceWithComboBox.Text;
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ) )
                {
                    int indexOf = this.replaceWithComboBox.Items.IndexOf( value );
                    if ( indexOf != -1 )
                    {
                        this.replaceWithComboBox.SelectedIndex = indexOf;
                    }
                    else
                    {
                        this.replaceWithComboBox.SelectedIndex = -1;
                        this.replaceWithComboBox.Text = value;
                    }
                }
                else
                {
                    this.replaceWithComboBox.SelectedIndex = -1;
                    this.replaceWithComboBox.Text = string.Empty;
                }
            }
        }

        public List<string> ReplaceHistory
        {
            get
            {
                List<string> replaceHistory = new List<string>();
                foreach ( object item in this.replaceWithComboBox.Items )
                {
                    replaceHistory.Add( item.ToString() );
                }

                return replaceHistory;
            }
            set
            {
                this.replaceWithComboBox.Items.Clear();
                if ( value != null )
                {
                    foreach ( string item in value )
                    {
                        this.replaceWithComboBox.Items.Add( item );
                    }
                }
            }
        }

        public bool MatchCase
        {
            get
            {
                return this.matchCaseCheckBox.Checked;
            }
            set
            {
                this.matchCaseCheckBox.Checked = value;
            }
        }

        public bool MatchWholeWord
        {
            get
            {
                return this.matchWholeWordCheckBox.Checked;
            }
            set
            {
                this.matchWholeWordCheckBox.Checked = value;
            }
        }

        public bool UseEnabled
        {
            get
            {
                return this.useCheckBox.Checked;
            }
            set
            {
                this.useComboBox.Enabled = this.useCheckBox.Checked = value;
            }
        }

        public FindReplaceSearchType SearchType
        {
            get
            {
                if ( this.useCheckBox.Checked )
                {
                    if ( this.useComboBox.SelectedIndex == 0 )
                    {
                        return FindReplaceSearchType.RegularExpression;
                    }
                    else
                    {
                        return FindReplaceSearchType.Wildcard;
                    }
                }
                else
                {
                    return FindReplaceSearchType.Normal;
                }
            }
            set
            {
                switch ( value )
                {
                    case FindReplaceSearchType.Normal:
                        {
                            this.UseEnabled = false;
                        }
                        break;
                    case FindReplaceSearchType.RegularExpression:
                        {
                            this.UseEnabled = true;
                            this.useComboBox.SelectedIndex = 0;
                        }
                        break;
                    case FindReplaceSearchType.Wildcard:
                        {
                            this.UseEnabled = true;
                            this.useComboBox.SelectedIndex = 1;
                        }
                        break;
                }
            }
        }

        public bool KeepModifiedFilesOpenAfterReplaceAll
        {
            get
            {
                return this.keepModifiedFilesOpenAfterReplaceAllCheckBox.Checked;
            }
            set
            {
                this.keepModifiedFilesOpenAfterReplaceAllCheckBox.Checked = value;
            }
        }

        public string LookIn
        {
            get
            {
                return this.lookInComboBox.Text;
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ) )
                {
                    int indexOf = this.lookInComboBox.Items.IndexOf( value );
                    if ( indexOf != -1 )
                    {
                        this.lookInComboBox.SelectedIndex = indexOf;
                    }
                    else
                    {
                        this.lookInComboBox.Text = value;
                    }

                    this.folderBrowserDialog.SelectedPath = value;
                }                
            }
        }

        public List<string> LookInPaths
        {
            get
            {
                // don't return the 3 default items
                List<string> lookInPaths = new List<string>();
                for ( int i = 3; i < this.lookInComboBox.Items.Count; ++i )
                {
                    lookInPaths.Add( this.lookInComboBox.Items[i].ToString() );
                }

                return lookInPaths;
            }
            set
            {
                // remove all but the 3 default items
                while ( this.lookInComboBox.Items.Count > 3 )
                {
                    this.lookInComboBox.Items.RemoveAt( this.lookInComboBox.Items.Count - 1 );
                }

                if ( value != null )
                {
                    foreach ( string item in value )
                    {
                        if ( !this.lookInComboBox.Items.Contains( item ) )
                        {
                            this.lookInComboBox.Items.Add( item );
                        }
                    }
                }
            }
        }

        public bool LookInSubfolders
        {
            get
            {
                return this.lookInSubfoldersCheckBox.Checked;
            }
            set
            {
                this.lookInSubfoldersCheckBox.Checked = value;
            }
        }

        public string FileType
        {
            get
            {
                return this.fileTypesComboBox.Text.Trim();
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ) )
                {
                    int indexOf = this.fileTypesComboBox.Items.IndexOf( value );
                    if ( indexOf != -1 )
                    {
                        this.fileTypesComboBox.SelectedIndex = indexOf;
                    }
                    else
                    {
                        this.fileTypesComboBox.Text = value;
                    }
                }
                else if ( this.fileTypesComboBox.Items.Count > 0 )
                {
                    this.fileTypesComboBox.SelectedIndex = 0;
                }
            }
        }

        public List<string> FileTypes
        {
            get
            {
                List<string> fileTypes = new List<string>();
                foreach ( object item in this.fileTypesComboBox.Items )
                {
                    fileTypes.Add( item.ToString() );
                }

                return fileTypes;
            }
            set
            {
                this.fileTypesComboBox.Items.Clear();
                if ( value != null )
                {
                    foreach ( string item in value )
                    {
                        this.fileTypesComboBox.Items.Add( item );
                    }
                }
            }
        }
        #endregion

        #region Event Handlers
        private void SearchReplaceInFilesDialog_Activated( object sender, EventArgs e )
        {
            CancelActiveIntellisense();

            string filename = OnFindCurrentFilename();
            List<string> openFilenames = OnFindOpenFilenames();
            List<string> projectFilenames = OnFindProjectFilenames();

            m_enableCurrentDocumentSearching = !String.IsNullOrEmpty( filename );
            m_enableAllOpenDocumentsSearching = (openFilenames != null) && (openFilenames.Count > 0);
            m_enableEntireProjectSearching = (projectFilenames != null) && (projectFilenames.Count > 0);

            if ( !m_enableCurrentDocumentSearching && !m_enableAllOpenDocumentsSearching && !m_enableEntireProjectSearching )
            {
                if ( this.lookInComboBox.Items.Count == 3 )
                {
                    m_enableSearching = false;
                    this.lookInComboBox.Text = string.Empty;
                }
                else
                {
                    if ( (this.lookInComboBox.Text == c_currentDocument) || (this.lookInComboBox.Text == c_allOpenDocuments)
                        || (this.lookInComboBox.Text == c_entireProject) )
                    {
                        this.lookInComboBox.SelectedIndex = 0;
                    }

                    m_enableSearching = true;
                    this.lookInComboBox.Enabled = !this.searchReplaceComponent.IsBusy();
                }
            }
            else
            {
                m_enableSearching = true;
                this.lookInComboBox.Enabled = !this.searchReplaceComponent.IsBusy();

                if ( (this.lookInComboBox.Text == c_entireProject) && !m_enableEntireProjectSearching )
                {
                    if ( m_enableCurrentDocumentSearching )
                    {
                        this.lookInComboBox.Text = c_currentDocument;
                    }
                    else if ( m_enableAllOpenDocumentsSearching )
                    {
                        this.lookInComboBox.Text = c_allOpenDocuments;
                    }
                }
                else if ( (this.lookInComboBox.Text == c_allOpenDocuments) && !m_enableAllOpenDocumentsSearching )
                {
                    if ( m_enableCurrentDocumentSearching )
                    {
                        this.lookInComboBox.Text = c_currentDocument;
                    }
                    else if ( m_enableEntireProjectSearching )
                    {
                        this.lookInComboBox.Text = c_entireProject;
                    }
                }
                else if ( (this.lookInComboBox.Text == c_currentDocument) && !m_enableCurrentDocumentSearching )
                {
                    if ( m_enableAllOpenDocumentsSearching )
                    {
                        this.lookInComboBox.Text = c_allOpenDocuments;
                    }
                    else if ( m_enableEntireProjectSearching )
                    {
                        this.lookInComboBox.Text = c_entireProject;
                    }
                }
            }

            SelectNextControl( this.findButton, false, true, true, true );
        }

        private void SearchReplaceInFilesDialog_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( e.CloseReason == CloseReason.UserClosing )
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        private void searchReplaceComponent_SearchComplete( object sender, SearchCompleteEventArgs e )
        {
            if ( (e.Search != SearchReplaceType.MarkAllInFile) && (e.Search != SearchReplaceType.MarkAllInFiles) )
            {
                if ( e.Result == SearchReplaceResult.Error )
                {
                    rageMessageBox.ShowError( this, e.ResultMessage, e.ResultTitle );
                }

                if ( (e.Search == SearchReplaceType.ReplaceInFile) || (e.Search == SearchReplaceType.ReplaceInFiles)
                    || (e.Search == SearchReplaceType.ReplaceAllInFile) || (e.Search == SearchReplaceType.ReplaceAllInFiles) )
                {
                    StoreReplaceText();
                }
            }

            StoreFindText();
            StoreLookInText();
            StoreFileTypeText();

            EnableButtons();

            SelectNextControl( this.findButton, false, true, true, true );
        }

        private void findWhatComboBox_TextUpdate( object sender, EventArgs e )
        {
            EnableButtons();           
        }

        private void useCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.useComboBox.Enabled = this.useCheckBox.Checked;
        }

        private void lookInComboBox_TextUpdate( object sender, EventArgs e )
        {
            if ( ((this.lookInComboBox.Text == c_currentDocument) && !m_enableCurrentDocumentSearching)
                || ((this.lookInComboBox.Text == c_allOpenDocuments) && !m_enableAllOpenDocumentsSearching)
                || ((this.lookInComboBox.Text == c_entireProject) && !m_enableEntireProjectSearching) )
            {
                this.lookInComboBox.Text = m_lastDirectory;
                folderBrowserDialog.SelectedPath = this.lookInComboBox.Text;
            }
            else
            {
                m_lastDirectory = this.lookInComboBox.Text;
                folderBrowserDialog.SelectedPath = this.lookInComboBox.Text;
            }
        }

        private void browseLookInButton_Click( object sender, EventArgs e )
        {
            DialogResult result = this.folderBrowserDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.LookIn = this.folderBrowserDialog.SelectedPath;
            }
        }

        private void findButton_Click( object sender, EventArgs e )
        {
            List<string> filenames;
            if ( !PrepareSearch( out filenames ) )
            {
                return;
            }

            DisableButtons();

            if ( this.LookIn == c_currentDocument )
            {
                this.searchReplaceComponent.FindAllInFile( filenames[0], m_findReplaceOptions, false );
            }
            else if ( (this.LookIn == c_allOpenDocuments) || (this.LookIn == c_entireProject) )
            {
                this.searchReplaceComponent.FindAllInFiles( filenames, m_findReplaceOptions, false );
            }
            else
            {
                this.searchReplaceComponent.FindAllInFiles( this.LookIn, this.LookInSubfolders, this.FileType, m_findReplaceOptions, false );
            }
        }

        private void findNextButton_Click( object sender, EventArgs e )
        {
            List<string> filenames;
            if ( !PrepareSearch( out filenames ) )
            {
                return;
            }

            DisableButtons();

            if ( this.LookIn == c_currentDocument )
            {
                this.searchReplaceComponent.FindNextInFile( filenames[0], m_findReplaceOptions, true );
            }
            else if ( (this.LookIn == c_allOpenDocuments) || (this.LookIn == c_entireProject) )
            {
                this.searchReplaceComponent.FindNextInFiles( filenames, OnFindCurrentFilename(), m_findReplaceOptions, true );
            }
            else
            {
                this.searchReplaceComponent.FindNextInFiles( this.LookIn, this.LookInSubfolders, this.FileType, 
                    OnFindCurrentFilename(), m_findReplaceOptions, true );
            }
        }

        private void replaceButton_Click( object sender, EventArgs e )
        {
            List<string> filenames;
            if ( !PrepareSearch( out filenames ) )
            {
                return;
            }

            DisableButtons();

            if ( this.LookIn == c_currentDocument )
            {
                this.searchReplaceComponent.ReplaceNextInFile( filenames[0], m_findReplaceOptions, true );
            }
            else if ( (this.LookIn == c_allOpenDocuments) || (this.LookIn == c_entireProject) )
            {
                this.searchReplaceComponent.ReplaceNextInFiles( filenames, OnFindCurrentFilename(), m_findReplaceOptions, true );
            }
            else
            {
                this.searchReplaceComponent.ReplaceNextInFiles( this.LookIn, this.LookInSubfolders, this.FileType, 
                    OnFindCurrentFilename(), m_findReplaceOptions, true );
            }
        }

        private void replaceAllButton_Click( object sender, EventArgs e )
        {
            List<string> filenames;
            if ( !PrepareSearch( out filenames ) )
            {
                return;
            }

            DisableButtons();

            if ( this.LookIn == c_currentDocument )
            {
                this.searchReplaceComponent.ReplaceAllInFile( filenames[0], m_findReplaceOptions, this.KeepModifiedFilesOpenAfterReplaceAll );
            }
            else if ( (this.LookIn == c_allOpenDocuments) || (this.LookIn == c_entireProject) )
            {
                this.searchReplaceComponent.ReplaceAllInFiles( filenames, m_findReplaceOptions, this.KeepModifiedFilesOpenAfterReplaceAll );
            }
            else
            {
                this.searchReplaceComponent.ReplaceAllInFiles( this.LookIn, this.LookInSubfolders, this.FileType, 
                    m_findReplaceOptions, this.KeepModifiedFilesOpenAfterReplaceAll );
            }
        }

        private void stopButton_Click( object sender, EventArgs e )
        {
            CancelSearchReplace();
        }
        #endregion

        #region Public Functions
        public void AddScriptFileExtensions( List<string> scriptExtensions )
        {
            List<string> fileTypes = this.FileTypes;
            foreach ( string scriptExtension in scriptExtensions )
            {
                if ( !fileTypes.Contains( scriptExtension ) )
                {
                    fileTypes.Add( scriptExtension );
                }
            }

            string currentFileType = this.FileType;
            this.FileTypes = fileTypes;
            this.FileType = currentFileType;
        }

        public void CancelActiveIntellisense()
        {
            OnCancelIntellisense();
        }

        public void CancelSearchReplace()
        {
            this.searchReplaceComponent.CancelSearchReplace();
        }

        public void SetUIFromSettings( FindReplaceAllSettings settings )
        {
            this.MatchCase = settings.MatchCase;
            this.MatchWholeWord = settings.MatchWholeWord;
            this.KeepModifiedFilesOpenAfterReplaceAll = settings.OpenFilesAfterReplace;

            if ( settings.UseRegularExpressions )
            {
                this.SearchType = FindReplaceSearchType.RegularExpression;
            }
            else if ( settings.UseWildcards )
            {
                this.SearchType = FindReplaceSearchType.Wildcard;
            }
            else
            {
                this.SearchType = FindReplaceSearchType.Normal;
            }
            
            this.LookInSubfolders = settings.SearchSubFolders;

            this.LookInPaths = settings.LookInPaths;
            this.LookIn = settings.SelectedPath;
            
            this.FileTypes = settings.FileTypes;
            this.FileType = settings.SelectedFileType;

            this.FindHistory = settings.FindHistory;
            this.ReplaceHistory = settings.ReplaceHistory;
        }

        public void SetSettingsFromUI( FindReplaceAllSettings settings )
        {
            settings.MatchCase = this.MatchCase;
            settings.MatchWholeWord = this.MatchWholeWord;
            settings.OpenFilesAfterReplace = this.KeepModifiedFilesOpenAfterReplaceAll;

            switch ( this.SearchType )
            {
                case FindReplaceSearchType.Normal:
                    {
                        settings.UseRegularExpressions = false;
                        settings.UseWildcards = false;
                    }
                    break;
                case FindReplaceSearchType.RegularExpression:
                    {
                        settings.UseRegularExpressions = true;
                        settings.UseWildcards = false;
                    }
                    break;
                case FindReplaceSearchType.Wildcard:
                    {
                        settings.UseRegularExpressions = false;
                        settings.UseWildcards = true;
                    }
                    break;
            }

            settings.SearchSubFolders = this.LookInSubfolders;

            settings.LookInPaths = this.LookInPaths;
            settings.SelectedPath = this.LookIn;

            settings.FileTypes = this.FileTypes;
            settings.SelectedFileType = this.FileType;

            settings.FindHistory = this.FindHistory;
            settings.ReplaceHistory = this.ReplaceHistory;
        }

        public bool InitSearchText( SyntaxEditor editor )
        {
            if ( this.searchReplaceComponent.IsBusy() )
            {
                // don't initialize text if we are in the middle of a search
                return false;
            }

            if ( editor == null )
            {
                // we need a syntax editor to set the search text
                return false;
            }

            string searchText = this.searchReplaceComponent.DetermineSearchText( editor.Document.Filename,
                editor.SelectedView.Selection.StartOffset );

            if ( String.IsNullOrEmpty( searchText ) )
            {
                return false;
            }

            int indexOf = this.findWhatComboBox.Items.IndexOf( searchText );
            if ( indexOf != -1 )
            {
                this.findWhatComboBox.SelectedIndex = indexOf;
            }
            else
            {
                this.findWhatComboBox.SelectedIndex = -1;
                this.findWhatComboBox.Text = searchText;
            }

            this.findWhatComboBox.SelectAll();

            m_findReplaceOptions.FindText = searchText;

            // make sure our buttons get enabled appropriately
            EnableButtons();

            return true;
        }
        #endregion

        #region Private Functions
        private void StoreFindText()
        {
            string text = this.findWhatComboBox.Text;
            int indexOf = this.findWhatComboBox.Items.IndexOf(text);
            if (indexOf != -1)
            {
                this.findWhatComboBox.Items.RemoveAt(indexOf);
            }

            if ( this.findWhatComboBox.Items.Count == 20 )
            {
                this.findWhatComboBox.Items.RemoveAt( 19 );
            }

            this.findWhatComboBox.Items.Insert( 0, text );
            this.findWhatComboBox.SelectedIndex = 0;
        }

        private void StoreReplaceText()
        {
            string text = this.replaceWithComboBox.Text;
            int indexOf = this.replaceWithComboBox.Items.IndexOf(text);
            if (indexOf != -1)
            {
                this.replaceWithComboBox.Items.RemoveAt(indexOf);
            }

            if ( this.replaceWithComboBox.Items.Count == 20 )
            {
                this.replaceWithComboBox.Items.RemoveAt( 19 );
            }

            this.replaceWithComboBox.Items.Insert( 0, text );
            this.replaceWithComboBox.SelectedIndex = 0;
        }

        private void StoreLookInText()
        {
            string text = this.lookInComboBox.Text;
            if ( (text == c_currentDocument) || (text == c_allOpenDocuments) || (text == c_entireProject) )
            {
                return;
            }

            int indexOf = this.lookInComboBox.Items.IndexOf(text);
            if (indexOf != -1)
            {
                this.lookInComboBox.Items.RemoveAt(indexOf);
            }

            if ( this.lookInComboBox.Items.Count == 20 )
            {
                this.lookInComboBox.Items.RemoveAt( 19 );
            }

            this.lookInComboBox.Items.Insert( 3, text );   // insert after the 3 default values
            this.lookInComboBox.SelectedIndex = 3;
        }

        private void StoreFileTypeText()
        {
            if ( this.fileTypesComboBox.Items.Count == 20 )
            {
                this.fileTypesComboBox.Items.RemoveAt( 19 );
            }

            string text = this.fileTypesComboBox.Text;
            int indexOf = this.fileTypesComboBox.Items.IndexOf( text );
            if ( indexOf != -1 )
            {
                this.fileTypesComboBox.Items.RemoveAt( indexOf );
            }

            this.fileTypesComboBox.Items.Insert( 0, text );
            this.fileTypesComboBox.SelectedIndex = 0;
        }

        private void SetUIFromFindReplaceOptions()
        {
            this.FindWhat = m_findReplaceOptions.FindText;
            this.MatchCase = m_findReplaceOptions.MatchCase;
            this.MatchWholeWord = m_findReplaceOptions.MatchWholeWord;
            this.ReplaceWith = m_findReplaceOptions.ReplaceText;
            this.SearchType = m_findReplaceOptions.SearchType;
        }

        private void SetFindReplaceOptionsFromUI()
        {
            m_findReplaceOptions.ChangeSelection = true;
            m_findReplaceOptions.FindText = this.FindWhat;
            m_findReplaceOptions.MatchCase = this.MatchCase;
            m_findReplaceOptions.MatchWholeWord = this.MatchWholeWord;
            m_findReplaceOptions.ReplaceText = this.ReplaceWith;
            m_findReplaceOptions.SearchInSelection = false;
            m_findReplaceOptions.SearchType = this.SearchType;
            m_findReplaceOptions.SearchUp = false;
        }

        private bool PrepareSearch( out List<string> filenames )
        {
            filenames = null;

            if (this.searchReplaceComponent.IsBusy())
                return false;

            SetFindReplaceOptionsFromUI();

            if ( this.LookIn == c_currentDocument )
            {
                string filename = OnFindCurrentFilename();
                if ( !String.IsNullOrEmpty( filename ) )
                {
                    filenames = new List<string>();
                    filenames.Add( filename );
                }
                else
                {
                    rageMessageBox.ShowError( this, "Could not determine the current filename.", this.searchReplaceComponent.Title );
                    return false;
                }
            }
            else if ( this.LookIn == c_allOpenDocuments )
            {
                filenames = OnFindOpenFilenames();
                if ( filenames != null )
                {
                    RemoveUnwantedFilenames( filenames );
                }
            }
            else if ( this.LookIn == c_entireProject )
            {
                filenames = OnFindProjectFilenames();
                if ( filenames != null )
                {
                    RemoveUnwantedFilenames( filenames );
                }
            }
            else
            {
                if ( !Directory.Exists( this.LookIn ) )
                {
                    rageMessageBox.ShowExclamation( this, String.Format( "Directory '{0}' doesn't exist.", this.LookIn ), 
                        this.searchReplaceComponent.Title );
                    return false;
                }

                // No need to fill the filenames list.  We will call a different function on the SearchReplaceComponent 
                // for doing the directory search/replace.
            }

            if ( filenames == null )
            {
                filenames = new List<string>();
            }

            return true;
        }

        private List<string> GetFileTypeExtensions()
        {
            List<string> extensions = new List<string>( this.FileType.Split( new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries ) );

            for ( int i = 0; i < extensions.Count; ++i )
            {
                extensions[i] = extensions[i].Trim();
                extensions[i] = extensions[i].ToLower();
            }

            return extensions;
        }

        private bool FileHasExtension( string filename, List<string> extensions )
        {
            if ( (extensions.Count == 0) || extensions.Contains( "*.*" ) )
            {
                return true;
            }

            string lowercaseFilename = filename.ToLower();
            foreach ( string extension in extensions )
            {                
                // removing the starting asterisk
                string ext = extension.Remove( 0, 1 );
                if ( lowercaseFilename.EndsWith( ext ) )
                {
                    return true;
                }
            }

            return false;
        }

        private void RemoveUnwantedFilenames( List<string> filenames )
        {
            List<string> extensions = GetFileTypeExtensions();
            if ( extensions.Count == 0 )
            {
                // same as "*.*"
                return;
            }

            for ( int i = 0; i < filenames.Count; )
            {
                if ( !FileHasExtension( filenames[i], extensions ) )
                {
                    filenames.RemoveAt( i );
                }
                else
                {
                    ++i;
                }
            }
        }

        private void DisableButtons()
        {            
            this.browseLookInButton.Enabled = false;
            this.fileTypesComboBox.Enabled = false;
            this.findButton.Enabled = false;
            this.findNextButton.Enabled = false;
            this.findWhatComboBox.Enabled = false;
            this.keepModifiedFilesOpenAfterReplaceAllCheckBox.Enabled = false;
            this.lookInComboBox.Enabled = false;
            this.lookInSubfoldersCheckBox.Enabled = false;
            this.matchCaseCheckBox.Enabled = false;
            this.matchWholeWordCheckBox.Enabled = false;
            this.replaceAllButton.Enabled = false;
            this.replaceButton.Enabled = false;
            this.replaceWithComboBox.Enabled = false;
            this.useCheckBox.Enabled = false;
            this.useComboBox.Enabled = false;
        }

        private void EnableButtons()
        {
            bool canSearch = m_enableSearching && !String.IsNullOrEmpty( this.findWhatComboBox.Text.Trim() );

            this.browseLookInButton.Enabled = true;
            this.fileTypesComboBox.Enabled = true;
            this.findButton.Enabled = canSearch;
            this.findNextButton.Enabled = canSearch;
            this.findWhatComboBox.Enabled = true;
            this.keepModifiedFilesOpenAfterReplaceAllCheckBox.Enabled = true;
            this.lookInComboBox.Enabled = true;
            this.lookInSubfoldersCheckBox.Enabled = true;
            this.matchCaseCheckBox.Enabled = true;
            this.matchWholeWordCheckBox.Enabled = true;
            this.replaceAllButton.Enabled = canSearch;
            this.replaceButton.Enabled = canSearch;
            this.replaceWithComboBox.Enabled = true;
            this.useCheckBox.Enabled = true;
            this.useComboBox.Enabled = this.useCheckBox.Checked;
        }
        #endregion
    }
}