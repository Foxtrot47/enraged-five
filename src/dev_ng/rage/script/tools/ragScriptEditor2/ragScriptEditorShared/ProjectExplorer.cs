using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

using RSG.Base.Forms;
using RSG.Base.Forms.SCM;
using TreeViewDragDrop;
using RSG.Platform;

namespace ragScriptEditorShared
{
    public partial class ProjectExplorer : UserControl
    {
        protected ProjectExplorer()
        {
            InitializeComponent();
        }
        
        public ProjectExplorer( Control parentControl )
        {
            InitializeComponent();

            m_parentControl = parentControl;

            // This will start us off right
            m_projectEditorSettings.Validate();
        }

        #region Enums
        public enum ProjectCompileAction
        {
            CompleFile,
            CompileFiles,
            RecompileFiles
        }

        public enum Icons
        {
            Folder,
            File,
            MissingFile
        }
        #endregion

        #region Delegates
        public delegate bool LoadScriptFileDelegate( string filename );
        public delegate void CompileActionDelegate( ProjectCompileAction action, List<string> filenames, Platform platform );
        public delegate void ReloadProjectDelegate();

        public delegate void UpdateProjectFilesEventHandler( object sender, ProjectFilesEventArgs e );
        
        private delegate void ShowExternallyModifiedDelegate();
        #endregion

        #region Events
        public event UpdateProjectFilesEventHandler FilesAdded;
        public event UpdateProjectFilesEventHandler FilesRemoved;
        #endregion

        #region Variables
        private ProjectExplorerProjectNode m_ProjectNode;
        private ProjectEditorSettings m_projectEditorSettings = new ProjectEditorSettings();
        private UserProjectSettings m_userProjectSettings = new UserProjectSettings();

        private TreeNode m_DragNode = null;
        private TreeNode m_TempDropNode = null;

        private FileSystemWatcher m_FileSystemWatcher = null;
        private DateTime m_LastWatcherDateTime;
        private FileStream m_fileStream = null;

        private Control m_parentControl = null;

        private bool m_modified = false;
        private List<string> m_languages = new List<string>();
        #endregion

        #region Properties
		public string ProjectFileName
		{
			get
			{
				return m_projectEditorSettings.Filename;
			}
		}

        public UserProjectSettings UserProjectSettings
		{
			get
			{
				return m_userProjectSettings;
			}
		}

        public ProjectEditorSettings ProjectEditorSetting
		{
			get
			{
				return m_projectEditorSettings;
			}
		}

		public string OpenFileDialogFilter
		{
			set
			{                
				this.openFileDialog.Filter = value;
			}
		}

        public bool IsLoaded
        {
            get
            {
                return m_projectEditorSettings.IsLoaded && !String.IsNullOrEmpty( m_projectEditorSettings.Filename );
            }
        }

        public bool IsModified
        {
            get
            {
                return IsLoaded && m_modified;
            }
            set
            {
                m_modified = value;
            }
        }

        public LoadScriptFileDelegate LoadScriptFile;
        public CompileActionDelegate ExecuteCompileAction;
        public ReloadProjectDelegate ReloadProjectFile;

        public string[] LanguageNames
        {
            set
            {
                m_languages.Clear();
                foreach ( string language in value )
                {
                    m_languages.Add( language );
                }
            }
        }

        public bool LockProjectFile
        {
            set
            {
                if ( this.IsLoaded )
                {
                    if ( value )
                    {
                        try
                        {
                            if ( m_fileStream == null )
                            {
                                m_fileStream = new FileStream( this.ProjectFileName, FileMode.Open, FileAccess.ReadWrite, FileShare.Read );
                            }
                        }
                        catch ( Exception e )
                        {
                            ApplicationSettings.ShowMessage( m_parentControl, 
                                String.Format( "Unable to lock '{0}':\n\n{1}", this.ProjectFileName,  e.Message ),
                                "Project File Error", MessageBoxButtons.OK, MessageBoxIcon.Warning, DialogResult.OK );
                        }
                    }
                    else
                    {
                        if ( m_fileStream != null )
                        {
                            m_fileStream.Close();
                            m_fileStream = null;
                        }
                    }
                }
            }
        }
        #endregion

        #region Event Handlers
        private void sortButton_Click( object sender, EventArgs e )
        {
            this.treeView.TreeViewNodeSorter = new ProjectExplorerTreeNodeComparer();
            this.treeView.TreeViewNodeSorter = null;

            m_modified = true;

            this.treeView.Sorted = false;
//             this.sortButton.Enabled = false;
        }

        #region TreeView
        private void treeView_DragOver( object sender, DragEventArgs e )
        {
            // Compute drag position and move image
            Point formP = this.PointToClient( new Point( e.X, e.Y ) );
            DragHelper.ImageList_DragMove( formP.X - this.treeView.Left, formP.Y - this.treeView.Top );

            // Get actual drop node
            TreeNode dropNode = this.treeView.GetNodeAt( this.treeView.PointToClient( new Point( e.X, e.Y ) ) );
            if ( dropNode == null )
            {
                e.Effect = DragDropEffects.None;
                return;
            }

            e.Effect = DragDropEffects.Move;

            // if mouse is on a new node select it
            if ( this.m_TempDropNode != dropNode )
            {
                DragHelper.ImageList_DragShowNolock( false );
                this.treeView.SelectedNode = dropNode;
                DragHelper.ImageList_DragShowNolock( true );
                m_TempDropNode = dropNode;
            }

            // Avoid that drop node
            if ( m_projectEditorSettings.Project.AutomaticSorting )
            {
                if ( m_DragNode.Parent == dropNode.Parent )
                {
                    if ( !(dropNode is ProjectExplorerFolderNode) )
                    {
                        e.Effect = DragDropEffects.None;
                        return;
                    }
                }
                else if ( m_DragNode.Parent == dropNode )
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
            }
            
            // Avoid that drop node is child of drag node 
            TreeNode tmpNode = dropNode;
            while ( tmpNode.Parent != null )
            {
                if ( tmpNode.Parent == this.m_DragNode )
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }

                tmpNode = tmpNode.Parent;
            }
        }

        private void treeView_DragEnter( object sender, DragEventArgs e )
        {
            DragHelper.ImageList_DragEnter( this.treeView.Handle, e.X - this.treeView.Left, e.Y - this.treeView.Top );
        }

        private void treeView_ItemDrag( object sender, ItemDragEventArgs e )
        {
            // Get drag node and select it
            this.m_DragNode = (TreeNode)e.Item;
            this.treeView.SelectedNode = this.m_DragNode;

            // Reset image list used for drag image            
            this.dragDropImageList.Images.Clear();
            // 256 is max width allowed by the imagelist
            int width = Math.Min(256, this.m_DragNode.Bounds.Size.Width + this.treeView.Indent);
            this.dragDropImageList.ImageSize = new Size( width, this.m_DragNode.Bounds.Height );

            // Create new bitmap
            // This bitmap will contain the tree node image to be dragged
            Bitmap bmp = new Bitmap( width, this.m_DragNode.Bounds.Height );

            // Get graphics from bitmap
            Graphics gfx = Graphics.FromImage( bmp );

            // Draw node icon into the bitmap
            gfx.DrawImage( this.treeViewImageList.Images[this.m_DragNode.ImageIndex], 0, 0 );

            // Draw node label into bitmap
            gfx.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            gfx.DrawString( this.m_DragNode.Text, this.treeView.Font, new SolidBrush( this.treeView.ForeColor ), (float)this.treeView.Indent, 1.0f );

            // Add bitmap to imagelist
            this.dragDropImageList.Images.Add( bmp );

            // Get mouse position in client coordinates
            Point p = this.treeView.PointToClient( Control.MousePosition );

            // Compute delta between mouse position and node bounds
            int dx = p.X + this.treeView.Indent - this.m_DragNode.Bounds.Left;
            int dy = p.Y - this.m_DragNode.Bounds.Top;

            // Begin dragging image
            if ( DragHelper.ImageList_BeginDrag( this.dragDropImageList.Handle, 0, dx, dy ) )
            {
                // Begin dragging
                this.treeView.DoDragDrop( bmp, DragDropEffects.Move );
                // End dragging image
                DragHelper.ImageList_EndDrag();
            }
        }

        private void treeView_DragLeave( object sender, EventArgs e )
        {
            DragHelper.ImageList_DragLeave( this.treeView.Handle );
        }

        private void treeView_DragDrop( object sender, DragEventArgs e )
        {
            // Unlock updates
            DragHelper.ImageList_DragLeave( this.treeView.Handle );

            // Get drop node
            TreeNode dropNode = this.treeView.GetNodeAt( this.treeView.PointToClient( new Point( e.X, e.Y ) ) );

            // If drop node isn't equal to drag node, add drag node as child of drop node
            if ( m_DragNode != null )
            {
                if ( (m_DragNode != dropNode) && !(m_DragNode is ProjectExplorerProjectNode) )
                {
                    MoveNode( m_DragNode, dropNode );

                    m_modified = true;

                    // Set drag node to null
                    m_DragNode = null;
                }                
            }
            else
            {
                object files = e.Data.GetData( DataFormats.FileDrop );
                if ( (files is string[]) && (((string[])files).Length > 0) )
                {
                    string[] filenames = files as string[];
                    foreach ( string filename in filenames )
                    {
                        ProjectExplorerFileNode fileNode = new ProjectExplorerFileNode( filename );
                        MoveNode( fileNode, dropNode );
                    }

                    m_modified = true;
                }
            }
        }

        private void treeView_GiveFeedback( object sender, GiveFeedbackEventArgs e )
        {
            if ( e.Effect == DragDropEffects.Move )
            {
                // Show pointer cursor while dragging
                e.UseDefaultCursors = false;
                this.treeView.Cursor = Cursors.Default;
            }
            else
            {
                e.UseDefaultCursors = true;
            }
        }

        private void treeView_AfterLabelEdit( object sender, NodeLabelEditEventArgs e )
        {
            if ( (e.Label != null) && (e.Label != string.Empty) )
            {
                // Stop editing without canceling the label change.
                e.Node.EndEdit( false );

                e.Node.Text = e.Label;
                ProjectExplorerTreeNode projNode = e.Node as ProjectExplorerTreeNode;
                projNode.ItemName = e.Label;

                treeView.LabelEdit = false;

                m_modified = true;
            }
            else
            {
                e.CancelEdit = true;
            }
        }

        private void treeView_MouseClick( object sender, MouseEventArgs e )
        {
            // updates our selected node before opening the ContextMenuStrip
            if ( e.Button == MouseButtons.Right )
            {
                this.treeView.SelectedNode = this.treeView.GetNodeAt( e.X, e.Y );
            }
        }

        private void treeView_NodeMouseDoubleClick( object sender, TreeNodeMouseClickEventArgs e )
        {
            if ( (e.Button == MouseButtons.Left) && (e.Node is ProjectExplorerFileNode) )
            {
                ProjectExplorerFileNode fileNode = e.Node as ProjectExplorerFileNode;
                OnLoadScriptFile( fileNode.ItemName );
            }
        }

        private void treeView_KeyDown( object sender, KeyEventArgs e )
        {
            if ( (e.KeyCode == Keys.Enter) && (this.treeView.SelectedNode != null) )
            {
                if ( this.treeView.SelectedNode is ProjectExplorerFileNode )
                {
                    ProjectExplorerFileNode fileNode = this.treeView.SelectedNode as ProjectExplorerFileNode;
                    OnLoadScriptFile( fileNode.ItemName );
                }
                else
                {
                    this.treeView.SelectedNode.Toggle();
                }
            }
        }
        #endregion

        #region ContextMenuStrip
        private void contextMenuStrip_Opening( object sender, CancelEventArgs e )
        {
            if ( !this.IsLoaded )
            {
                e.Cancel = true;
                return;
            }

            if ( this.treeView.SelectedNode != null )
            {
                ProjectExplorerFileNode fileNode = this.treeView.SelectedNode as ProjectExplorerFileNode;
                ProjectExplorerFolderNode folderNode = this.treeView.SelectedNode as ProjectExplorerFolderNode;
                ProjectExplorerProjectNode projectNode = this.treeView.SelectedNode as ProjectExplorerProjectNode;

                bool missingFile = (fileNode != null) && (fileNode.ImageIndex == (int)Icons.MissingFile);

                this.openToolStripMenuItem.Enabled = !missingFile && (fileNode != null);
                this.compileFileToolStripMenuItem.Enabled = !missingFile && (fileNode != null);
                this.addExistingFilesToolStripMenuItem.Enabled = (folderNode != null) || (projectNode != null);
                this.addNewFolderToolStripMenuItem.Enabled = (folderNode != null) || (projectNode != null);
                this.reloadMissingFileToolStripMenuItem.Enabled = missingFile;
                this.locateMissingFileToolStripMenuItem.Enabled = missingFile;
                this.renameToolStripMenuItem.Enabled = folderNode != null;
                this.removeToolStripMenuItem.Enabled = (fileNode != null) || (folderNode != null);
            }
            else
            {
                this.openToolStripMenuItem.Enabled = false;
                this.compileFileToolStripMenuItem.Enabled = false;
                this.addExistingFilesToolStripMenuItem.Enabled = false;
                this.addNewFolderToolStripMenuItem.Enabled = false;
                this.reloadMissingFileToolStripMenuItem.Enabled = false;
                this.locateMissingFileToolStripMenuItem.Enabled = false;
                this.renameToolStripMenuItem.Enabled = false;
                this.removeToolStripMenuItem.Enabled = false;
            }
        }

        private void openToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( (this.treeView.SelectedNode != null) && (this.treeView.SelectedNode is ProjectExplorerFileNode) )
            {
                ProjectExplorerFileNode fileNode = this.treeView.SelectedNode as ProjectExplorerFileNode;
                OnLoadScriptFile( fileNode.ItemName );
            }
        }

        private void compileFileToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( (this.treeView.SelectedNode != null) && (this.treeView.SelectedNode is ProjectExplorerFileNode) )
            {
                ProjectExplorerFileNode fileNode = this.treeView.SelectedNode as ProjectExplorerFileNode;

                List<string> filenames = new List<string>();
                filenames.Add( fileNode.ItemName );
                
                OnCompileAction( ProjectCompileAction.CompleFile, filenames );
            }
        }

        private void compileAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            CompileProject( false );
        }

        private void rebuildAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            CompileProject( true );
        }

        private void addExistingFilesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.treeView.SelectedNode != null )
            {
                this.openFileDialog.Title = "Add Existing Item";
                this.openFileDialog.Multiselect = true;
                DialogResult result = this.openFileDialog.ShowDialog( m_parentControl );
                if ( (result != DialogResult.Cancel) && (this.openFileDialog.FileNames.Length > 0) )
                {
                    List<string> projectFilenames = GetProjectFilenames();                    

                    List<string> filenames = new List<string>();
                    foreach ( string filename in this.openFileDialog.FileNames )
                    {
                        if ( projectFilenames.Contains( filename.ToLower() ) )
                        {
                            rageMessageBox.ShowInformation( m_parentControl, 
                                String.Format( "'{0}' is already in the project and will be skipped.", filename ), "Add Exiting Files" );
                            continue;
                        }

                        filenames.Add( filename );

                        ProjectExplorerFileNode fileNode = new ProjectExplorerFileNode( filename );
                        MoveNode( fileNode, this.treeView.SelectedNode );
                    }

                    if ( filenames.Count > 0 )
                    {
                        // expand on the first node added
                        if ( this.treeView.SelectedNode.Nodes.Count == 1 )
                        {
                            this.treeView.SelectedNode.Expand();
                        }

                        OnFilesAdded( filenames );
                        m_modified = true;
                    }
                }
            }
        }

        private void addNewFolderToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( (this.treeView.SelectedNode != null)
                && ((this.treeView.SelectedNode is ProjectExplorerFolderNode)
                || (this.treeView.SelectedNode is ProjectExplorerProjectNode)) )
            {
                string newFolderName = "NewFolder";
                int number = 1;

                ProjectExplorerFolderNode folderNode = this.treeView.SelectedNode as ProjectExplorerFolderNode;
                ProjectExplorerProjectNode projectNode = this.treeView.SelectedNode as ProjectExplorerProjectNode;

                // make sure we're creating a unique name:
                for ( int i = 0; i < this.treeView.SelectedNode.Nodes.Count; i++ )
                {
                    ProjectExplorerTreeNode item = this.treeView.SelectedNode.Nodes[i] as ProjectExplorerTreeNode;
                    if ( item.ToString() == newFolderName )
                    {
                        i = 0;
                        newFolderName = "NewFolder" + number++;
                    }
                }

                // turn on label edit so the user can change the name
                treeView.LabelEdit = true;

                // create the new folder:
                ProjectExplorerTreeNode treeNode = this.treeView.SelectedNode as ProjectExplorerTreeNode;
                ProjectExplorerFolderNode newFolder = treeNode.AddNewFolder( newFolderName );

                // make sure we can see the folder we're editing
                this.treeView.SelectedNode.Expand();    

                m_modified = true;

                // start editing the name
                if ( !newFolder.IsEditing )
                {
                    newFolder.BeginEdit();
                }
            }
        }

        private void reloadMissingFileToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( (this.treeView.SelectedNode != null) && (this.treeView.SelectedNode is ProjectExplorerFileNode) )
            {
                ProjectExplorerFileNode fileNode = this.treeView.SelectedNode as ProjectExplorerFileNode;
                if ( File.Exists( fileNode.ItemName ) )
                {
                    fileNode.SelectedImageIndex = fileNode.ImageIndex = (int)Icons.File;
                }
                else
                {
                    rageMessageBox.ShowError( m_parentControl,
                        String.Format( "Unable to locate missing file '{0}'.", fileNode.ItemName ), "File Not Found" );
                }
            }
        }

        private void locateMissingFileToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( (this.treeView.SelectedNode != null) && (this.treeView.SelectedNode is ProjectExplorerFileNode) )
            {
                ProjectExplorerFileNode fileNode = this.treeView.SelectedNode as ProjectExplorerFileNode;

                this.openFileDialog.Title = "Locate Missing Item '" + fileNode.ToString() + "'";
                this.openFileDialog.Multiselect = false;
                DialogResult result = this.openFileDialog.ShowDialog( m_parentControl );
                if ( (result != DialogResult.Cancel) && (this.openFileDialog.FileNames.Length > 0) )
                {
                    string filename = this.openFileDialog.FileNames[0];

                    fileNode.ItemName = filename;
                    fileNode.SelectedImageIndex = fileNode.ImageIndex = (int)Icons.File;
                    m_modified = true;
                }
            }
        }

        private void renameToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( (this.treeView.SelectedNode != null) && (this.treeView.SelectedNode is ProjectExplorerFolderNode) )
            {
                ProjectExplorerFolderNode folderNode = this.treeView.SelectedNode as ProjectExplorerFolderNode;

                m_modified = true;

                treeView.LabelEdit = true;
                folderNode.BeginEdit();
            }
        }

        private void removeToolStripMenuItem_Click( object sender, EventArgs e )
        {            
            if ( this.treeView.SelectedNode != null )
            {
                ProjectExplorerTreeNode treeNode = this.treeView.SelectedNode as ProjectExplorerTreeNode;
                treeNode.LeaveParent();

                if ( treeNode is ProjectExplorerFileNode )
                {
                    ProjectExplorerFileNode fileNode = treeNode as ProjectExplorerFileNode;

                    List<string> filenames = new List<string>();
                    filenames.Add( fileNode.ItemName );

                    OnFilesRemoved( filenames );
                }
                
                m_modified = true;
            }
        }
        #endregion

        #endregion

        #region Project Handling
        public bool CreateNewProject()
		{
            DialogResult result = this.projectFolderBrowserDialog.ShowDialog( m_parentControl );
            if ( result == DialogResult.Cancel )
            {
                return false;
            }

            string directory = this.projectFolderBrowserDialog.SelectedPath.Trim();
            directory = Path.GetFullPath( directory );
            if ( !directory.EndsWith("\\") )
            {
                directory += "\\";
            }

            string projectName = string.Empty;
            string languageName = string.Empty;
            if ( !NewProjectForm.ShowIt( m_parentControl, directory, m_languages, ref projectName, ref languageName ) )
            {
                return false;
            }

            m_projectEditorSettings.Filename = directory + projectName;
            if ( !m_projectEditorSettings.Filename.EndsWith(".scproj") )
            {
                m_projectEditorSettings.Filename += ".scproj";
            }

            m_projectEditorSettings.Language = languageName;
            m_projectEditorSettings.Validate();
            
			treeView.Nodes.Clear();
			m_ProjectNode = new ProjectExplorerProjectNode( Path.GetFileName(Path.ChangeExtension(m_projectEditorSettings.Filename,null)) );
			treeView.Nodes.Add( m_ProjectNode );
			SaveProject();
			CreateFileSystemWatcher();
			return true;
		}

		public bool LoadProject( string filename )
		{
			if ( !File.Exists(filename) )
			{
                DialogResult result = ApplicationSettings.ShowMessage( m_parentControl, 
                    String.Format( "Project file '{0}' does not exist.", filename ), "Problem Opening Project File",
					MessageBoxButtons.RetryCancel, MessageBoxIcon.Error, DialogResult.Cancel );
				if ( result == DialogResult.Retry )
				{
					return LoadProject( filename );
				}

				return false;
			}
			
			m_projectEditorSettings.Filename = Path.GetFullPath( filename );

			treeView.Nodes.Clear();
			
			m_ProjectNode = new ProjectExplorerProjectNode( Path.GetFileNameWithoutExtension( filename ) );
			treeView.Nodes.Add( m_ProjectNode );

            Directory.SetCurrentDirectory( Path.GetDirectoryName( filename ) );

			// load project
			bool success = LoadProjectFile( filename, m_ProjectNode.Nodes );

            // this will start us off right
            m_projectEditorSettings.Validate();
		
			// load project settings
            if ( LoadUserProjectSettingsFile( GetUserProjectSettingsFilename( filename ) ) && success )
			{
				// expand folders
				ExpandFolders( m_ProjectNode, m_userProjectSettings.OpenFolders );

                // set the current configuration
                if ( !String.IsNullOrEmpty( m_userProjectSettings.SelectedConfigurationName ) )
                {
                    for ( int i = 0; i < m_projectEditorSettings.CompilingSettingsList.Count; ++i )
                    {
                        if ( m_projectEditorSettings.CompilingSettingsList[i].ConfigurationName == m_userProjectSettings.SelectedConfigurationName )
                        {
                            m_projectEditorSettings.CurrentCompilingSettingsIndex = i;
                            break;
                        }
                    }
                }
                else if ( m_projectEditorSettings.CompilingSettingsList.Count > 0 )
                {
                    m_projectEditorSettings.CurrentCompilingSettingsIndex = 0;
                }
                else
                {
                    m_projectEditorSettings.CurrentCompilingSettingsIndex = -1;
                }
                /*
                if (m_userProjectSettings.SelectedPlatform != Platform.Independent)
                {
                    m_projectEditorSettings.PlatformSettings.CurrentPlatform = m_userProjectSettings.SelectedPlatform;
                }
                else if (m_projectEditorSettings.PlatformSettings.Platforms.Count > 0)
                {
                    m_projectEditorSettings.PlatformSettings.CurrentPlatform = m_projectEditorSettings.PlatformSettings.Platforms.First();
                }
                else
                {
                    m_projectEditorSettings.PlatformSettings.CurrentPlatform = Platform.Independent;
                }
                */
            }

            // re-validate
            m_projectEditorSettings.Validate();

//             this.sortButton.Enabled = !TreeViewIsSorted();

			CreateFileSystemWatcher();

            if ( !success )
            {
                CloseProject();
            }

			return success;
		}

		public bool ReloadProject()
		{
			FindOpenFolders( m_ProjectNode, m_userProjectSettings.OpenFolders );

			treeView.Nodes.Clear();

            m_ProjectNode = new ProjectExplorerProjectNode( Path.GetFileName( Path.ChangeExtension( m_projectEditorSettings.Filename, null ) ) );
			treeView.Nodes.Add( m_ProjectNode );

            Directory.SetCurrentDirectory( Path.GetDirectoryName( m_projectEditorSettings.Filename ) );

			// load project
            bool success = LoadProjectFile( m_projectEditorSettings.Filename, m_ProjectNode.Nodes );
            
            // this will start us off right
            m_projectEditorSettings.Validate();
			
            if ( success )
			{
				// expand folders
				ExpandFolders( m_ProjectNode, m_userProjectSettings.OpenFolders );
			}
            else
            {
                CloseProject();
            }

			return success;
		}

        public bool CloseProject()
        {
            SaveUserProjectSettingsFile( GetUserProjectSettingsFilename( m_projectEditorSettings.Filename ) );

            m_projectEditorSettings.Filename = string.Empty;
            m_projectEditorSettings.IsLoaded = false;
            treeView.Nodes.Clear();
            m_ProjectNode = null;

            if ( m_fileStream != null )
            {
                m_fileStream.Close();
                
                m_fileStream = null;
            }

            if ( m_FileSystemWatcher != null )
            {
                m_FileSystemWatcher.EnableRaisingEvents = false;
                m_FileSystemWatcher.Dispose();
                m_FileSystemWatcher = null;
            }

            return true;
        }

        public void SaveProject()
        {
            // save project settings
            SaveUserProjectSettingsFile( GetUserProjectSettingsFilename( m_projectEditorSettings.Filename ) );
            
            // save project
            SaveProjectFile( m_projectEditorSettings.Filename, m_ProjectNode.Nodes );
        }
        #endregion

        #region Save/Load Project File
        private bool LoadProjectFile( string filename, TreeNodeCollection nodes )
        {
            ApplicationSettings.SetBranchFrom(filename);

            // see if it's the old layout file
            XmlTextReader reader = null;
            float version = 0.0f;
            try
            {
                if ( m_fileStream != null )
                {
                    m_fileStream.Close();
                }

                m_fileStream = new FileStream( filename, FileMode.Open, FileAccess.Read, FileShare.Read );

                reader = new XmlTextReader( m_fileStream );

                int numElementsToCheck = 12;
                while ( reader.Read() && (numElementsToCheck > 0) )
                {
                    if (reader.Name == "Version")
                    {
                        if ( reader.IsStartElement() )
                        {
                            version = float.Parse( reader.ReadString() );
                        }

                        break;
                    }

                    --numElementsToCheck;
                }
            }
            catch ( Exception e )
            {
                ApplicationSettings.ShowMessage( m_parentControl,
                    String.Format( "Could not parse old project file:\n{0}", e.ToString()), "Problem Parsing Project File!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error, DialogResult.OK );
                return false;
            }

            if ( version == ProjectEditorSettings.SettingsVersion )
            {
                // if we got here, we have a new layout file
                string error = null;
                m_projectEditorSettings.IsLoaded = false;
                if ( !m_projectEditorSettings.LoadFile( filename, out error ) )
                {
                    ApplicationSettings.ShowMessage( m_parentControl, error, 
                        String.Format( "Error Loading Project File Version {0}!", ProjectEditorSettings.SettingsVersion ), 
                        MessageBoxButtons.OK, MessageBoxIcon.Error, DialogResult.OK );
                    return false;
                }
                m_projectEditorSettings.Filename = filename;

                // <HACK>
                // ALupinacci 6/1/09 - Copy the PostCompileCommand from the ProjectSettings (which is
                // no longer used) to each of the CurrentCompilingSettings and then wipe the ProjectSetting's
                // copy of the PostCompileCommand.
                if ( !String.IsNullOrEmpty( m_projectEditorSettings.Project.PostCompileCommand ) )
                {
                    foreach ( CompilingSettings compilingSettings in m_projectEditorSettings.CompilingSettingsList )
                    {
                        compilingSettings.PostCompileCommand = m_projectEditorSettings.Project.PostCompileCommand;
                    }

                    m_projectEditorSettings.Project.PostCompileCommand = String.Empty;
                }
                // </HACK>
            }
            else
            {
                ApplicationSettings.ShowMessage( m_parentControl, String.Format( "Unable to load a project file version {0}", version ),
                    "Unknown Project File Version", MessageBoxButtons.OK, MessageBoxIcon.Warning, DialogResult.OK );
                return false;
            }

            string originalProjectDirectory = Path.GetDirectoryName( m_projectEditorSettings.OriginalFilenameExpanded ).ToLower();
            string projectDirectory = Path.GetDirectoryName( filename ).ToLower();

            // build the ProjectExplorer Tree
            treeView.BeginUpdate();
            foreach ( ProjectExplorerItem item in m_projectEditorSettings.ProjectExplorerItems )
            {
                if ( item is ProjectExplorerFile )
                {
                    ProjectExplorerFileNode fileNode = new ProjectExplorerFileNode( 
                        ApplicationSettings.GetAbsolutePath(item.Name) );
                    nodes.Add( fileNode );
                }
                else if ( item is ProjectExplorerFolder )
                {
                    ProjectExplorerFolderNode folderNode = new ProjectExplorerFolderNode( item.Name );                                        
                    
                    ProjectExplorerFolder folder = item as ProjectExplorerFolder;
                    foreach ( ProjectExplorerItem subItem in folder.Items )
                    {
                        folderNode.AddItem( subItem, originalProjectDirectory, projectDirectory );
                    }

                    nodes.Add( folderNode );
                }
            }
            treeView.EndUpdate();

            m_modified = version != ProjectEditorSettings.SettingsVersion;

            m_projectEditorSettings.IsLoaded = true;

            return true;
        }

        private bool SaveProjectFile( string filename, TreeNodeCollection nodes )
        {
            string projectDirectory = Path.GetDirectoryName( filename );

            // get the current project tree
            m_projectEditorSettings.ProjectExplorerItems.Clear();
            foreach ( TreeNode node in nodes )
            {
                ProjectExplorerTreeNode projectExplorerTreeNode = node as ProjectExplorerTreeNode;

                if ( projectExplorerTreeNode is ProjectExplorerFileNode )
                {
                    ProjectExplorerFile file = new ProjectExplorerFile( 
                        ApplicationSettings.GetRelativePath(projectExplorerTreeNode.ItemName ) );
                    m_projectEditorSettings.ProjectExplorerItems.Add( file );
                }
                else if ( projectExplorerTreeNode is ProjectExplorerFolderNode )
                {
                    ProjectExplorerFolder folder = new ProjectExplorerFolder( projectExplorerTreeNode.ItemName );

                    ProjectExplorerFolderNode folderNode = projectExplorerTreeNode as ProjectExplorerFolderNode;
                    folderNode.GetItems( folder, projectDirectory );

                    m_projectEditorSettings.ProjectExplorerItems.Add( folder );
                }
            }

            // clear ALL source control settings from project file (now stored in user editor settings).
            m_projectEditorSettings.SourceControl2 = null;

            // set the original filename
            m_projectEditorSettings.OriginalFilename = ApplicationSettings.GetRelativePath(filename);

            // turn off the file watcher
            if ( m_FileSystemWatcher != null )
            {
                m_FileSystemWatcher.EnableRaisingEvents = false;
            }

            // save the project file
            string error = null;
            if ( !m_projectEditorSettings.SaveFile( filename, out error ) )
            {
                rageMessageBox.ShowError( m_parentControl, error, "Error Saving Project File!" );

                if ( m_FileSystemWatcher != null )
                {
                    m_FileSystemWatcher.EnableRaisingEvents = true;
                }

                return false;
            }

            // turn the file watch back on
            if ( m_FileSystemWatcher != null )
            {
                m_FileSystemWatcher.EnableRaisingEvents = true;
            }

            m_modified = false;

            return true;
        }
        #endregion

        #region Save/Load User Settings File
        public bool SaveUserProjectSettingsFile( string filename )
        {            
            m_userProjectSettings.OpenFolders.Clear();
            FindOpenFolders( m_ProjectNode, m_userProjectSettings.OpenFolders );

            if ( m_projectEditorSettings.CurrentCompilingSettings != null )
            {
                m_userProjectSettings.SelectedConfigurationName = m_projectEditorSettings.CurrentCompilingSettings.ConfigurationName;
            }

            //m_userProjectSettings.SelectedPlatform = m_projectEditorSettings.PlatformSettings.CurrentPlatform;

            string projectDirectory = Path.GetDirectoryName( this.ProjectFileName );

            // Make sure all Open Text Editors have expanded, full paths for user settings file.
            foreach ( TextEditorSettings t in m_userProjectSettings.OpenTextEditors )
            {
                t.Filename = ApplicationSettings.GetAbsolutePath(t.Filename);
            }

            string error;
            if ( !m_userProjectSettings.SaveFile( filename, out error ) )
            {
                return false;
            }

            ApplicationSettings.LastProjectSettingsFile = filename;
            return true;
        }

        public bool LoadUserProjectSettingsFile( string filename )
        {
            if ( !File.Exists( filename ) )
            {
                m_userProjectSettings.OpenTextEditors.Clear();
                m_userProjectSettings.OpenFolders.Clear();
                return false;
            }

            bool oldFormat = false;

            // see if it's the old format
            XmlTextReader reader = null;
            try
            {
                using (reader = new XmlTextReader(filename))
                {
                    while (reader.Read())
                    {
                        if (reader.Name == "scproj")
                        {
                            if (!reader.IsStartElement())
                            {
                                reader.Read();
                                break; // we're done
                            }

                            reader.Close();
                            oldFormat = true;
                        }
                    }
                }
            }
            catch ( Exception e )
            {
                ApplicationSettings.Log.ToolException(e, "Exception opening/parsing {0} to determine format", filename);
            }

            // if we got to here, it's the new format
            string error;
            if ( oldFormat && !m_userProjectSettings.LoadOldFile( filename, Path.GetDirectoryName( filename ) ) )
            {
                return false;
            }
            else if ( !m_userProjectSettings.LoadFile( filename, out error ) )
            {
                return false;
            }

            string originalProjectDirectory = Path.GetDirectoryName( m_projectEditorSettings.OriginalFilenameExpanded );
            string projectDirectory = Path.GetDirectoryName( this.ProjectFileName );

            ApplicationSettings.LastProjectSettingsFile = filename;
            return true;
        }

		private void ExpandFolders( TreeNode parentNode, List<string> openFolders )
		{
			// check current node
			if ( openFolders.Contains(parentNode.FullPath) )
			{
				parentNode.Expand();
			}

			// check sub nodes
			foreach (TreeNode node in parentNode.Nodes)
			{
				if ( node is ProjectExplorerFolderNode )
				{
					// recursive
					ExpandFolders( node, openFolders );
				}
			}
		}

        private void FindOpenFolders( ProjectExplorerTreeNode parentNode, List<string> openFolders )
        {
            if ( parentNode.IsExpanded )
            {
                openFolders.Add( parentNode.FullPath );
            }

            foreach ( ProjectExplorerTreeNode node in parentNode.Nodes )
            {
                if ( node is ProjectExplorerFolderNode )
                {
                    FindOpenFolders( node, openFolders );
                }
            }
        }
        #endregion

        #region Misc
        private string GetUserProjectSettingsFilename( string projectFile )
        {
            string filename = null;

            int indexOf = projectFile.IndexOf( ".scproj" );
            if ( indexOf > -1 )
            {
                filename = projectFile + ".settings";
            }
            else
            {
                filename = projectFile + ".scproj.settings";
            }

            return filename;
        }

        public void CompileProject( bool rebuild )
		{
            List<string> filenames = new List<string>();
			m_ProjectNode.CollectFileList( filenames );

            if ( filenames.Count > 0 )
			{
				if ( rebuild )
				{
                    OnCompileAction( ProjectCompileAction.RecompileFiles, filenames );
				}
				else
				{
                    OnCompileAction( ProjectCompileAction.CompileFiles, filenames );
				}
			}
		}

        public List<string> GetProjectFilenames()
        {
            if ( m_ProjectNode != null )
            {
                List<string> filenames = new List<string>();
                m_ProjectNode.CollectFileList( filenames );
                return filenames;
            }

            return new List<string>();
        }

        public bool IsFileInProject( string filename )
        {
            List<string> filenames = GetProjectFilenames();

            return filenames.Contains( filename );
        }

        private void MoveNode( TreeNode dragNode, TreeNode dropNode )
        {
            // remove from old parent
            TreeNode dragNodeParent = dragNode.Parent;
            if ( dragNodeParent == null )
            {
                this.treeView.Nodes.Remove( dragNode );
            }
            else
            {
                dragNodeParent.Nodes.Remove( dragNode );
            }

            if ( m_projectEditorSettings.Project.AutomaticSorting )
            {
                // find the node collection we're going to add to
                TreeNodeCollection nodes;
                if ( dropNode is ProjectExplorerFileNode )
                {
                    TreeNode dropNodeParent = dropNode.Parent;
                    if ( dropNodeParent == null )
                    {
                        nodes = this.treeView.Nodes;
                        this.treeView.ExpandAll();
                    }
                    else
                    {
                        nodes = dropNodeParent.Nodes;
                    }
                }
                else
                {
                    nodes = dropNode.Nodes;
                }

                int index = -1;
                if ( (dragNode is ProjectExplorerProjectNode) || (dragNode is ProjectExplorerFolderNode) )
                {
                    // find the index among the folder nodes that we should insert at
                    for ( int i = 0; i < nodes.Count; ++i )
                    {
                        if ( nodes[i] is ProjectExplorerFileNode )
                        {
                            index = i; // insert before
                            break;
                        }

                        string dragText = dragNode.Text.ToLower();
                        string nodeText = nodes[i].Text.ToLower();
                        int cmp = dragText.CompareTo( nodeText );
                        if ( cmp <= 0 )
                        {
                            index = i; // insert before
                            break;
                        }
                    }
                }
                else
                {
                    // find the index among the file nodes that we should insert at
                    for ( int i = 0; i < nodes.Count; ++i )
                    {
                        if ( (nodes[i] is ProjectExplorerProjectNode) || (nodes[i] is ProjectExplorerFolderNode) )
                        {
                            continue;
                        }

                        string dragText = dragNode.Text.ToLower();
                        string nodeText = nodes[i].Text.ToLower();
                        int cmp = dragText.CompareTo( nodeText );
                        if ( cmp <= 0 )
                        {
                            index = i; // insert before
                            break;
                        }
                    }
                }

                if ( index == -1 )
                {
                    nodes.Add( dragNode );
                }
                else
                {
                    nodes.Insert( index, dragNode );
                }
            }
            else
            {
                // add to new parent
                if ( dropNode is ProjectExplorerFileNode )
                {
                    TreeNode dropNodeParent = dropNode.Parent;
                    if ( dropNodeParent == null )
                    {
                        int index = this.treeView.Nodes.IndexOf( dropNode );
                        this.treeView.Nodes.Insert( index, dragNode );
                        this.treeView.ExpandAll();
                    }
                    else
                    {
                        int index = dropNodeParent.Nodes.IndexOf( dropNode );
                        dropNodeParent.Nodes.Insert( index, dragNode );
                    }
                }
                else
                {
                    dropNode.Nodes.Add( dragNode );
                }
            }

            this.treeView.SelectedNode = dragNode;

//             this.sortButton.Enabled = !TreeViewIsSorted();
        }

        private bool TreeViewIsSorted()
        {
            if ( !this.IsLoaded )
            {
                return true;
            }

            IComparer comparer = new ProjectExplorerTreeNodeComparer();
            for ( int i = 1; i < m_ProjectNode.Nodes.Count; ++i )
            {
                if ( !TreeViewIsSorted( comparer, m_ProjectNode.Nodes[i - 1], m_ProjectNode.Nodes[i] ) )
                {
                    return false;
                }
            }

            return true;
        }

        private bool TreeViewIsSorted( IComparer comparer, TreeNode x, TreeNode y )
        {
            if ( comparer.Compare( x, y ) >= 0 )
            {
                return false;
            }

            for ( int i = 1; i < x.Nodes.Count; ++i )
            {
                if ( !TreeViewIsSorted( comparer, x.Nodes[i - 1], x.Nodes[i] ) )
                {
                    return false;
                }
            }

            return true;
        }
        #endregion

        #region Event Dispatchers
        private void OnLoadScriptFile( string filename )
        {
            if ( LoadScriptFile != null )
            {
                if ( m_parentControl != null )
                {
                    object[] args = new object[1];
                    args[0] = filename;

                    m_parentControl.Invoke( LoadScriptFile, args );
                }
                else
                {
                    LoadScriptFile( filename );
                }
            }
        }

        private void OnReloadProject()
        {
            if ( ReloadProjectFile != null )
            {
                DialogResult result = rageMessageBox.ShowQuestion( m_parentControl, 
                    "The current project was modified outside of the editor.\r\nWould you like to reload it now (and lose any current changes)?\r\n",
                    "Project Externally Modified" );
                if ( result == DialogResult.Yes )
                {
                    if ( m_parentControl != null )
                    {
                        m_parentControl.Invoke( ReloadProjectFile );
                    }
                    else
                    {
                        ReloadProjectFile();
                    }
                }
            }
        }

        private void OnFilesAdded( List<string> filenames )
        {
            if ( FilesAdded != null )
            {
                ProjectFilesEventArgs e = new ProjectFilesEventArgs( filenames );
                if ( m_parentControl != null )
                {
                    object[] args = new object[2];
                    args[0] = this;
                    args[1] = e;

                    m_parentControl.Invoke( FilesAdded, args );
                }
                else
                {
                    FilesAdded( this, e );
                }
            }
        }

        private void OnFilesRemoved( List<string> filenames )
        {
            if ( FilesRemoved != null )
            {
                ProjectFilesEventArgs e = new ProjectFilesEventArgs( filenames );
                if ( m_parentControl != null )
                {
                    object[] args = new object[2];
                    args[0] = this;
                    args[1] = e;

                    m_parentControl.Invoke( FilesRemoved, args );
                }
                else
                {
                    FilesRemoved( this, e );
                }
            }
        }

        private void OnCompileAction( ProjectCompileAction action, List<string> filenames )
        {
            if ( ExecuteCompileAction != null )
            {
                if ( m_parentControl != null )
                {
                    object[] args = new object[3];
                    args[0] = action;
                    args[1] = filenames;
                    args[2] = UserProjectSettings.SelectedPlatform;

                    m_parentControl.Invoke( ExecuteCompileAction, args );
                }
                else
                {
                    ExecuteCompileAction( action, filenames, UserProjectSettings.SelectedPlatform );
                }
            }
        }
        #endregion

        #region File Watcher
        private void ShowExternallyModifiedDialog()
        {
            if ( m_FileSystemWatcher != null )
            {
                m_FileSystemWatcher.EnableRaisingEvents = false;
            }

            OnReloadProject();
            
            if ( m_FileSystemWatcher != null )
            {
                m_FileSystemWatcher.EnableRaisingEvents = true;
            }
        }

        private void Watcher_Changed( object sender, FileSystemEventArgs e )
        {
            ShowExternallyModifiedDelegate showDialogDel = new ShowExternallyModifiedDelegate( ShowExternallyModifiedDialog );
            if ( m_parentControl != null )
            {
                m_parentControl.Invoke( showDialogDel );
            }
            else
            {
                this.Invoke( showDialogDel );
            }

            m_LastWatcherDateTime = DateTime.Now;
        }

        private void Watcher_Renamed( object source, RenamedEventArgs e )
        {
            if ( e.FullPath.ToLower() == m_projectEditorSettings.Filename.ToLower() )
            {
                ShowExternallyModifiedDelegate showDialogDel = new ShowExternallyModifiedDelegate( ShowExternallyModifiedDialog );
                if ( m_parentControl != null )
                {
                    m_parentControl.Invoke( showDialogDel );
                }
                else
                {
                    this.Invoke( showDialogDel );
                }
            }

            m_LastWatcherDateTime = DateTime.Now;
        }
       
        private void CreateFileSystemWatcher()
        {
            if ( ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor )
            {
                m_FileSystemWatcher = new FileSystemWatcher( Path.GetDirectoryName( m_projectEditorSettings.Filename ), Path.GetFileName( m_projectEditorSettings.Filename ) );
                m_FileSystemWatcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
                m_FileSystemWatcher.Changed += new FileSystemEventHandler( Watcher_Changed );
                m_FileSystemWatcher.Renamed += new RenamedEventHandler( Watcher_Renamed );
                m_FileSystemWatcher.EnableRaisingEvents = true;

                m_LastWatcherDateTime = DateTime.Now;
            }
        }
        #endregion

        #region ProjectExplorerTreeNodeComparer
        private class ProjectExplorerTreeNodeComparer : IComparer
        {
            public int Compare( object x, object y )
            {
                if ( ((x is ProjectExplorerFileNode) && (y is ProjectExplorerFileNode))
                    || ((x is ProjectExplorerFolderNode) && (y is ProjectExplorerFolderNode)) )
                {
                    string textX = ((ProjectExplorerTreeNode)x).Text.ToLower();
                    string textY = ((ProjectExplorerTreeNode)y).Text.ToLower();

                    return textX.CompareTo( textY );
                }
                else if ( x is ProjectExplorerFolderNode ) // and y is ProjectExplorerFileNode
                {
                    return -1;
                }
                else // x is ProjectExplorerFileNode and y is ProjectExplorerFolderNode
                {
                    return 1;
                }
            }
        };
        #endregion
    }

    #region ProjectExplorerTreeNode
    abstract class ProjectExplorerTreeNode : System.Windows.Forms.TreeNode
    {
        public ProjectExplorerTreeNode( string name )
        {
            m_Name = name;
            this.Text = this.ToString();
        }

        #region Variables
        protected string m_Name;
        #endregion

        #region Properties
        public virtual string ItemName
        {
            get
            {
                return m_Name;
            }
            set
            {
                m_Name = value;
            }
        }
        #endregion

        #region Overrides
        public override string ToString()
        {
            return m_Name;
        }
        #endregion

        #region Public Functions
        public virtual void Write( XmlTextWriter xmlWriter, string projectDirectory )
        {
        }

        public virtual ProjectExplorerFolderNode AddNewFolder( string name )
        {
            ProjectExplorerFolderNode folderNode = new ProjectExplorerFolderNode( name );
            this.Nodes.Add( folderNode );
            return folderNode;
        }

        public virtual void CollectFileList( List<string> filenames )
        {
            foreach ( ProjectExplorerTreeNode item in Nodes )
            {
                item.CollectFileList( filenames );
            }
        }
        
        public void LeaveParent()
        {
            if ( Parent != null )
            {
                Parent.Nodes.Remove( this );
            }
        }
        #endregion
    }    

    class ProjectExplorerProjectNode : ProjectExplorerTreeNode
    {
        public ProjectExplorerProjectNode( string name )
            : base( name )
        {
            this.SelectedImageIndex = this.ImageIndex = (int)ProjectExplorer.Icons.Folder;
            this.Text = "Project '" + name + "'";
        }
    }

    class ProjectExplorerFileNode : ProjectExplorerTreeNode
    {
        public ProjectExplorerFileNode( string filename )
            : base( filename )
        {
            if ( File.Exists( m_Name ) )
            {
                this.SelectedImageIndex = this.ImageIndex = (int)ProjectExplorer.Icons.File;
            }
            else
            {
                this.SelectedImageIndex = this.ImageIndex = (int)ProjectExplorer.Icons.MissingFile;
            }
        }

        #region Overrides
        public override string ItemName
        {
            get
            {
                return base.ItemName;
            }
            set
            {
                base.ItemName = value;
                this.Text = this.ToString();
            }
        }

        public override void Write( XmlTextWriter xmlWriter, string projectDirectory )
        {
            xmlWriter.WriteStartElement( "File" );
            xmlWriter.WriteAttributeString( "name", ApplicationSettings.GetRelativePath( m_Name ) );
            xmlWriter.WriteEndElement();
        }

        public override ProjectExplorerFolderNode AddNewFolder( string name )
        {
            return null;
        }

        public override void CollectFileList( List<string> filenames )
        {
            filenames.Add( m_Name.ToLower() );
        }
        
        public override string ToString()
        {
            return Path.GetFileName( m_Name );
        }
        #endregion

        #region Public Static Functions
        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="originalProjectDirectory"></param>
        /// <param name="projectDirectory"></param>
        /// <returns></returns>
        public static ProjectExplorerFileNode Read( XmlTextReader reader, string originalProjectDirectory, string projectDirectory )
        {
            ProjectExplorerFileNode newFile = null;
            string filename = reader["name"];
            if (String.IsNullOrEmpty(filename))
            {
                newFile = new ProjectExplorerFileNode( 
                    ApplicationSettings.GetAbsolutePath(filename) );
            }

            return newFile;
        }
        #endregion
    }

    class ProjectExplorerFolderNode : ProjectExplorerTreeNode
    {
        public ProjectExplorerFolderNode( string name )
            : base( name )
        {
            this.SelectedImageIndex = this.ImageIndex = (int)ProjectExplorer.Icons.Folder;
        }

        #region Public Functions
        /// <summary>
        /// Creates and adds <see cref="ProjectExplorerTreeNode"/>s as subnodes of this <see cref="TreeNode"/> for the <see cref="ProjectExplorerItem"/> passed in.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="originalProjectDirectory"></param>
        /// <param name="projectDirectory"></param>
        public void AddItem( ProjectExplorerItem item, string originalProjectDirectory, string projectDirectory )
        {
            if ( item is ProjectExplorerFile )
            {
                ProjectExplorerFileNode fileNode = new ProjectExplorerFileNode( 
                    ApplicationSettings.GetAbsolutePath(item.Name) );
                this.Nodes.Add( fileNode );
            }
            else if ( item is ProjectExplorerFolder )
            {
                ProjectExplorerFolderNode folderNode = new ProjectExplorerFolderNode( item.Name );

                ProjectExplorerFolder folder = item as ProjectExplorerFolder;
                foreach ( ProjectExplorerItem subItem in folder.Items )
                {
                    folderNode.AddItem( subItem, originalProjectDirectory, projectDirectory );
                }

                this.Nodes.Add( folderNode );
            }
        }

        /// <summary>
        /// For every subnode of this <see cref="TreeNode"/>, creates and adds a <see cref="ProjectExplorerItem"/> as subnodes of the <see cref="ProjectExplorerFolder"/> passed in.
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="projectDirectory"></param>
        public void GetItems( ProjectExplorerFolder folder, string projectDirectory )
        {
            foreach ( TreeNode node in this.Nodes )
            {
                ProjectExplorerTreeNode projectExplorerTreeNode = node as ProjectExplorerTreeNode;

                if ( projectExplorerTreeNode is ProjectExplorerFileNode )
                {
                    ProjectExplorerFile file = new ProjectExplorerFile(
                        ApplicationSettings.GetRelativePath( projectExplorerTreeNode.ItemName ) );
                    folder.Items.Add( file );
                }
                else if ( projectExplorerTreeNode is ProjectExplorerFolderNode )
                {
                    ProjectExplorerFolder subFolder = new ProjectExplorerFolder( projectExplorerTreeNode.ItemName );

                    ProjectExplorerFolderNode folderNode = projectExplorerTreeNode as ProjectExplorerFolderNode;
                    folderNode.GetItems( subFolder, projectDirectory );

                    folder.Items.Add( subFolder );
                }
            }
        }
        #endregion

        #region Public Static Functions
        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="nodes"></param>
        /// <param name="originalProjectDirectory"></param>
        /// <param name="projectDirectory"></param>
        public static void ReadFolder( XmlTextReader reader, TreeNodeCollection nodes, string originalProjectDirectory, string projectDirectory )
        {
            string parentName = reader.Name;

            string folderName = reader["name"];
            if ( (folderName == null) || (folderName == "") )
            {
                return;
            }

            ProjectExplorerFolderNode newFolder = new ProjectExplorerFolderNode( folderName );
            nodes.Add( newFolder );

            if ( reader.IsEmptyElement )
            {
                return;
            }

            while ( reader.Read() )
            {
                if ( !reader.IsStartElement() )
                {
                    if ( reader.Name == parentName )
                    {
                        break; // we're done
                    }
                }
                else if ( reader.Name == "File" )
                {
                    ProjectExplorerFileNode newFile = ProjectExplorerFileNode.Read( reader, originalProjectDirectory, projectDirectory );
                    if ( newFile != null )
                    {
                        newFolder.Nodes.Add( newFile );
                    }
                }
                else if ( reader.Name == "Folder" )
                {
                    ReadFolder( reader, newFolder.Nodes, originalProjectDirectory, projectDirectory );
                }
            }
        }
        #endregion
    }
    #endregion

    #region UserProjectSettings
    public class UserProjectSettings
    {
        public UserProjectSettings()
        {
        }

        #region Constants
        public const float UserProjectSettingsVersion = 1.2f;
        #endregion

        #region Variables
        private string m_selectedConfigurationName = string.Empty;
        private Platform m_selectedPlatform = Platform.Win64;
        private float m_version = UserProjectSettings.UserProjectSettingsVersion;
        private float m_loadedVersion = 0.0f;
        private List<string> m_openFolders = new List<string>();
        private List<TextEditorSettings> m_openTextEditors = new List<TextEditorSettings>();

        private SourceControlSettings2 m_sourceControlSettings = new SourceControlSettings2();
        #endregion

        #region Properties
        public float Version
        {
            get
            {
                return m_version;
            }
            set
            {
                m_version = value;
            }
        }

        [XmlIgnore]
        public float LoadedVersion
        {
            get
            {
                return m_loadedVersion;
            }
        }

        public string SelectedConfigurationName
        {
            get
            {
                return m_selectedConfigurationName;
            }
            set
            {
                m_selectedConfigurationName = value;
            }
        }

        public Platform SelectedPlatform
        {
            get { return m_selectedPlatform; }
            set { m_selectedPlatform = value; }
        }

        public List<string> OpenFolders
        {
            get
            {
                return m_openFolders;
            }
            set
            {
                m_openFolders = value;
            }
        }

        public List<TextEditorSettings> OpenTextEditors
        {
            get
            {
                return m_openTextEditors;
            }
            set
            {
                m_openTextEditors = value;
            }
        }

        /// <summary>
        /// deprecated
        /// </summary>
        public SourceControlSettings SourceControl
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        /// <summary>
        /// Also deprecated! It's a deprecation party!
        /// (scm settings are now stored in User*Editor*Settings, not User*Project*Settings)
        /// </summary>
        public SourceControlSettings2 SourceControl2
        {
            get
            {
                return null;
            }
            set
            {
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="projectDirectory"></param>
        /// <returns></returns>
        public bool LoadOldXml( XmlTextReader reader, string projectDirectory )
        {
            m_openFolders.Clear();
            m_openTextEditors.Clear();

            while ( !reader.EOF )
            {
                if ( reader.Name == "scproj" )
                {
                    if ( !reader.IsStartElement() )
                    {
                        reader.Read();
                        break; // we're done
                    }

                    m_selectedConfigurationName = reader["SelectedConfiguration"];
                    reader.Read();
                }
                else if ( reader.Name == "OpenFolders" && !reader.IsEmptyElement )
                {
                    while ( !reader.EOF )
                    {
                        if ( reader.Name == "OpenFolders" && !reader.IsStartElement() )
                        {
                            reader.Read();
                            break; // we're done
                        }
                        else if ( reader.Name == "folder" )
                        {
                            if ( reader.IsStartElement() )
                            {
                                reader.Read();
                                string folder = reader.ReadString();
                                m_openFolders.Add( folder );
                            }

                            reader.Read();
                        }
                        else
                        {
                            reader.Read();
                        }
                    }
                }
                else if ( reader.Name == "OpenFiles" && !reader.IsEmptyElement )
                {
                    while ( !reader.EOF )
                    {
                        if ( reader.Name == "OpenFiles" && !reader.IsStartElement() )
                        {
                            reader.Read();
                            break; // we're done
                        }
                        else if ( reader.Name == "file" )
                        {
                            if ( reader.IsStartElement() )
                            {
                                reader.Read();
                                string file = reader.ReadString();

                                if ( !String.IsNullOrEmpty( file ) )
                                {
                                    TextEditorSettings t = new TextEditorSettings( 
                                        Path.GetFullPath( Path.Combine( projectDirectory, file ) ), Guid.NewGuid() );
                                    m_openTextEditors.Add( t );
                                }
                            }

                            reader.Read();
                        }
                        else
                        {
                            reader.Read();
                        }
                    }
                }
                else
                {
                    reader.Read();
                }
            }

            m_loadedVersion = 0.0f;
            return true;
        }

        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="projectDirectory"></param>
        /// <returns></returns>
        public bool LoadOldFile( string filename, string projectDirectory )
        {
            XmlTextReader reader = new XmlTextReader( filename );
            reader.Read();

            bool success = LoadOldXml( reader, projectDirectory );

            reader.Close();
            return success;
        }

        /// <summary>
        /// Saves the settings to the specified file.
        /// </summary>
        /// <param name="filename">The file to save.</param>
        /// <param name="error">Out parameter with error information if this function returns <c>false</c>.</param>
        /// <returns><c>true</c> on success.  If <c>false</c>, look at 'error' for more information.</returns>
        public bool SaveFile( string filename, out string error )
        {
            error = null;

            if ( File.Exists( filename ) )
            {
                // make writable
                FileAttributes atts = File.GetAttributes( filename );
                if ( (atts & FileAttributes.ReadOnly) == FileAttributes.ReadOnly )
                {
                    try
                    {
                        File.SetAttributes( filename, atts & (~FileAttributes.ReadOnly) );
                    }
                    catch
                    {
                    }
                }
            }

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );

                XmlSerializer serializer = new XmlSerializer( this.GetType() );
                serializer.Serialize( writer, this );

                writer.Close();
            }
            catch ( Exception e )
            {
                if ( writer != null )
                {
                    writer.Close();
                }

                error = "Error serializing '" + filename + "'\n\n" + e.Message;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Load the xml filename with all of the settings
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public bool LoadFile( string filename, out string error )
        {
            error = null;

            FileStream stream = null;
            try
            {
                stream = new FileStream( filename, FileMode.Open, FileAccess.Read );

                XmlSerializer serializer = new XmlSerializer( this.GetType() );
                UserProjectSettings settings = serializer.Deserialize( stream ) as UserProjectSettings;

                stream.Close();

                if ( settings == null )
                {
                    error = "Unable to deserialize '" + filename + "'";
                    return false;
                }

                this.Copy( settings );
                
                // carry the version # over
                m_loadedVersion = settings.Version;
            }
            catch ( Exception e )
            {
                if ( stream != null )
                {
                    stream.Close();
                }

                error = "Error deserializing '" + filename + "'\n\n" + e.Message;
                return false;
            }

            return true;
        }

        public void Copy( UserProjectSettings u )
        {
            if ( u == null )
            {
                return;
            }

            this.SelectedConfigurationName = u.SelectedConfigurationName;
            this.SelectedPlatform = u.SelectedPlatform;
            
            this.OpenTextEditors.Clear();
            foreach ( TextEditorSettings t in u.OpenTextEditors )
            {
                TextEditorSettings copyT = new TextEditorSettings();
                copyT.Copy( t );

                this.OpenTextEditors.Add( copyT );
            }

            this.OpenFolders.Clear();
            this.OpenFolders.AddRange( u.OpenFolders );
        }
        #endregion
    }
    #endregion

    #region Event Args
    public class ProjectFilesEventArgs : EventArgs
    {
        public ProjectFilesEventArgs( List<string> filenames )
        {
            m_filenames = filenames;
        }

        #region Properties
        public List<string> Filenames
        {
            get
            {
                return m_filenames;
            }
        }
        #endregion

        #region Variables
        private List<string> m_filenames;
        #endregion
    }
    #endregion
}
