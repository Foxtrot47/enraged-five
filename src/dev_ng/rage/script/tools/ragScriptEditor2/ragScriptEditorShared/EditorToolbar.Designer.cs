namespace ragScriptEditorShared
{
    partial class EditorToolbar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorToolbar));
            this.editToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator22 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator23 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator24 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator25 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.compilingConfigurationToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.platformToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.editAddRemoveContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.compilingConfigurationsVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.codeToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator26 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator27 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator28 = new System.Windows.Forms.ToolStripSeparator();
            this.codeAddRemoveContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.customToolStrip = new System.Windows.Forms.ToolStrip();
            this.customAddRemoveContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.debuggerToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.macroToolStrip = new System.Windows.Forms.ToolStrip();
            this.incredibuildToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveAllToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.printPreviewToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.cutToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.deleteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.undoToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.redoToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.indentToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.outdentToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.findReplaceToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.editToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.editAddOrRemoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAllVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdentVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indentVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findReplaceVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.forwardToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.commentToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.uncommentToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.refreshIntellisenseToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.bookmarkToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.previousBookmarkToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.nextBookmarkToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.clearBookmarksToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.hideSelectionToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.unhideSelectionToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toggleOutliningToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toggleAllOutliningToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.codeToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.codeAddOrRemoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commentVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uncommentVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshIntellisenseVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bookmarkVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.previousBookmarkVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nextBookmarkVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearBookmarksVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hideVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unhideVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toggleOutliningVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toggleAllOutliningVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.customAddOrRemoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.customizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toggleBreakpointToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toggleBreakpointAllToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toggleBreakpointStopGameToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toggleBreakpointAllStopToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.playToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pauseToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.stepIntoToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.stepOverToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.stepOutToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.debuggerOptionsToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.showLineNumbersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.debuggerHighlightEnumsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debuggerHighlightQuickInfoPopupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debuggerHighlightDisabledCodeBlocksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.startAutomaticOutliningToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startManualOutliningToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopOutliningToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.changeIncludesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordMacroToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pauseMacroToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.stopMacroToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.cancelMacroToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.playMacroToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.incredibuildBuildToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.incredibuildRebuildToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.incredibuildCleanToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.incredibuildStopToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.editToolStrip.SuspendLayout();
            this.editAddRemoveContextMenuStrip.SuspendLayout();
            this.codeToolStrip.SuspendLayout();
            this.codeAddRemoveContextMenuStrip.SuspendLayout();
            this.customToolStrip.SuspendLayout();
            this.debuggerToolStrip.SuspendLayout();
            this.macroToolStrip.SuspendLayout();
            this.incredibuildToolStrip.SuspendLayout();
            // 
            // editToolStrip
            // 
            this.editToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.editToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.saveAllToolStripButton,
            this.toolStripSeparator22,
            this.printToolStripButton,
            this.printPreviewToolStripButton,
            this.toolStripSeparator23,
            this.cutToolStripButton,
            this.copyToolStripButton,
            this.pasteToolStripButton,
            this.deleteToolStripButton,
            this.toolStripSeparator24,
            this.undoToolStripSplitButton,
            this.redoToolStripSplitButton,
            this.toolStripSeparator25,
            this.indentToolStripButton,
            this.outdentToolStripButton,
            this.toolStripSeparator10,
            this.compilingConfigurationToolStripComboBox,
            this.platformToolStripComboBox,
            this.toolStripSeparator15,
            this.findReplaceToolStripButton,
            this.editToolStripDropDownButton});
            this.editToolStrip.Location = new System.Drawing.Point(3, 24);
            this.editToolStrip.Name = "editToolStrip";
            this.editToolStrip.Size = new System.Drawing.Size(416, 25);
            this.editToolStrip.TabIndex = 1;
            this.editToolStrip.Text = "editToolStrip";
            // 
            // toolStripSeparator22
            // 
            this.toolStripSeparator22.Name = "toolStripSeparator22";
            this.toolStripSeparator22.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator23
            // 
            this.toolStripSeparator23.Name = "toolStripSeparator23";
            this.toolStripSeparator23.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator24
            // 
            this.toolStripSeparator24.Name = "toolStripSeparator24";
            this.toolStripSeparator24.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator25
            // 
            this.toolStripSeparator25.Name = "toolStripSeparator25";
            this.toolStripSeparator25.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 25);
            // 
            // compilingConfigurationToolStripComboBox
            // 
            this.compilingConfigurationToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.compilingConfigurationToolStripComboBox.Name = "compilingConfigurationToolStripComboBox";
            this.compilingConfigurationToolStripComboBox.Size = new System.Drawing.Size(121, 23);
            this.compilingConfigurationToolStripComboBox.Tag = "CompilingConfigurations";
            this.compilingConfigurationToolStripComboBox.ToolTipText = "Compiling Configurations";
            this.compilingConfigurationToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.compilingConfigurationToolStripComboBox_SelectedIndexChanged);
            // 
            // platformToolStripComboBox
            // 
            this.platformToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.platformToolStripComboBox.Name = "platformToolStripComboBox";
            this.platformToolStripComboBox.Size = new System.Drawing.Size(121, 23);
            this.platformToolStripComboBox.ToolTipText = "Build Platforms";
            this.platformToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.platformToolStripComboBox_SelectedIndexChanged);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(6, 6);
            // 
            // editAddRemoveContextMenuStrip
            // 
            this.editAddRemoveContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newVisibleToolStripMenuItem,
            this.openVisibleToolStripMenuItem,
            this.saveVisibleToolStripMenuItem,
            this.saveAllVisibleToolStripMenuItem,
            this.toolStripSeparator2,
            this.printVisibleToolStripMenuItem,
            this.printPreviewVisibleToolStripMenuItem,
            this.toolStripSeparator3,
            this.cutVisibleToolStripMenuItem,
            this.copyVisibleToolStripMenuItem,
            this.pasteVisibleToolStripMenuItem,
            this.deleteVisibleToolStripMenuItem,
            this.toolStripSeparator4,
            this.undoVisibleToolStripMenuItem,
            this.redoVisibleToolStripMenuItem,
            this.toolStripSeparator5,
            this.outdentVisibleToolStripMenuItem,
            this.indentVisibleToolStripMenuItem,
            this.toolStripSeparator11,
            this.compilingConfigurationsVisibleToolStripMenuItem,
            this.toolStripSeparator16,
            this.findReplaceVisibleToolStripMenuItem});
            this.editAddRemoveContextMenuStrip.Name = "editAddRemoveContextMenuStrip";
            this.editAddRemoveContextMenuStrip.OwnerItem = this.editAddOrRemoveToolStripMenuItem;
            this.editAddRemoveContextMenuStrip.ShowCheckMargin = true;
            this.editAddRemoveContextMenuStrip.ShowItemToolTips = false;
            this.editAddRemoveContextMenuStrip.Size = new System.Drawing.Size(235, 392);
            this.editAddRemoveContextMenuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.AddRemoveContextMenuStrip_ItemClicked);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(231, 6);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(231, 6);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(231, 6);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(231, 6);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(231, 6);
            // 
            // compilingConfigurationsVisibleToolStripMenuItem
            // 
            this.compilingConfigurationsVisibleToolStripMenuItem.Checked = true;
            this.compilingConfigurationsVisibleToolStripMenuItem.CheckOnClick = true;
            this.compilingConfigurationsVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.compilingConfigurationsVisibleToolStripMenuItem.Name = "compilingConfigurationsVisibleToolStripMenuItem";
            this.compilingConfigurationsVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.compilingConfigurationsVisibleToolStripMenuItem.Tag = "CompilingConfigurations";
            this.compilingConfigurationsVisibleToolStripMenuItem.Text = "Compiling Configurations";
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(231, 6);
            // 
            // codeToolStrip
            // 
            this.codeToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.codeToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backToolStripButton,
            this.forwardToolStripButton,
            this.toolStripSeparator18,
            this.commentToolStripButton,
            this.uncommentToolStripButton,
            this.toolStripSeparator26,
            this.refreshIntellisenseToolStripButton,
            this.toolStripSeparator27,
            this.bookmarkToolStripButton,
            this.previousBookmarkToolStripButton,
            this.nextBookmarkToolStripButton,
            this.clearBookmarksToolStripButton,
            this.toolStripSeparator28,
            this.hideSelectionToolStripButton,
            this.unhideSelectionToolStripButton,
            this.toggleOutliningToolStripButton,
            this.toggleAllOutliningToolStripButton,
            this.codeToolStripDropDownButton});
            this.codeToolStrip.Location = new System.Drawing.Point(363, 24);
            this.codeToolStrip.Name = "codeToolStrip";
            this.codeToolStrip.Size = new System.Drawing.Size(312, 25);
            this.codeToolStrip.TabIndex = 2;
            this.codeToolStrip.Text = "codeToolStrip";
            // 
            // toolStripSeparator26
            // 
            this.toolStripSeparator26.Name = "toolStripSeparator26";
            this.toolStripSeparator26.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator27
            // 
            this.toolStripSeparator27.Name = "toolStripSeparator27";
            this.toolStripSeparator27.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator28
            // 
            this.toolStripSeparator28.Name = "toolStripSeparator28";
            this.toolStripSeparator28.Size = new System.Drawing.Size(6, 25);
            // 
            // codeAddRemoveContextMenuStrip
            // 
            this.codeAddRemoveContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.commentVisibleToolStripMenuItem,
            this.uncommentVisibleToolStripMenuItem,
            this.toolStripSeparator6,
            this.refreshIntellisenseVisibleToolStripMenuItem,
            this.toolStripSeparator7,
            this.bookmarkVisibleToolStripMenuItem,
            this.previousBookmarkVisibleToolStripMenuItem,
            this.nextBookmarkVisibleToolStripMenuItem,
            this.clearBookmarksVisibleToolStripMenuItem,
            this.toolStripSeparator9,
            this.hideVisibleToolStripMenuItem,
            this.unhideVisibleToolStripMenuItem,
            this.toggleOutliningVisibleToolStripMenuItem,
            this.toggleAllOutliningVisibleToolStripMenuItem});
            this.codeAddRemoveContextMenuStrip.Name = "codeAddRemoveContextMenuStrip";
            this.codeAddRemoveContextMenuStrip.OwnerItem = this.codeAddOrRemoveToolStripMenuItem;
            this.codeAddRemoveContextMenuStrip.ShowCheckMargin = true;
            this.codeAddRemoveContextMenuStrip.ShowItemToolTips = false;
            this.codeAddRemoveContextMenuStrip.Size = new System.Drawing.Size(242, 264);
            this.codeAddRemoveContextMenuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.AddRemoveContextMenuStrip_ItemClicked);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(238, 6);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(238, 6);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(238, 6);
            // 
            // customToolStrip
            // 
            this.customToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customToolStripDropDownButton});
            this.customToolStrip.Location = new System.Drawing.Point(0, 0);
            this.customToolStrip.Name = "customToolStrip";
            this.customToolStrip.Size = new System.Drawing.Size(23, 25);
            this.customToolStrip.TabIndex = 0;
            this.customToolStrip.Text = "customToolStrip";
            // 
            // customAddRemoveContextMenuStrip
            // 
            this.customAddRemoveContextMenuStrip.Name = "contextMenuStrip3";
            this.customAddRemoveContextMenuStrip.OwnerItem = this.customAddOrRemoveToolStripMenuItem;
            this.customAddRemoveContextMenuStrip.ShowCheckMargin = true;
            this.customAddRemoveContextMenuStrip.ShowItemToolTips = false;
            this.customAddRemoveContextMenuStrip.Size = new System.Drawing.Size(83, 4);
            this.customAddRemoveContextMenuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.AddRemoveContextMenuStrip_ItemClicked);
            // 
            // debuggerToolStrip
            // 
            this.debuggerToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toggleBreakpointToolStripButton,
            this.toggleBreakpointAllToolStripButton,
            this.toggleBreakpointStopGameToolStripButton,
            this.toggleBreakpointAllStopToolStripButton,
            this.toolStripSeparator17,
            this.playToolStripButton,
            this.pauseToolStripButton,
            this.stepIntoToolStripButton,
            this.stepOverToolStripButton,
            this.stepOutToolStripButton,
            this.toolStripSeparator8,
            this.debuggerOptionsToolStripDropDownButton});
            this.debuggerToolStrip.Location = new System.Drawing.Point(0, 0);
            this.debuggerToolStrip.Name = "debuggerToolStrip";
            this.debuggerToolStrip.Size = new System.Drawing.Size(131, 25);
            this.debuggerToolStrip.TabIndex = 0;
            this.debuggerToolStrip.Text = "debuggerToolStrip";
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // macroToolStrip
            // 
            this.macroToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.recordMacroToolStripButton,
            this.pauseMacroToolStripButton,
            this.stopMacroToolStripButton,
            this.cancelMacroToolStripButton,
            this.playMacroToolStripButton});
            this.macroToolStrip.Location = new System.Drawing.Point(0, 0);
            this.macroToolStrip.Name = "macroToolStrip";
            this.macroToolStrip.Size = new System.Drawing.Size(125, 25);
            this.macroToolStrip.TabIndex = 0;
            this.macroToolStrip.Text = "macroToolStrip";
            // 
            // incredibuildToolStrip
            // 
            this.incredibuildToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.incredibuildBuildToolStripButton,
            this.incredibuildRebuildToolStripButton,
            this.incredibuildCleanToolStripButton,
            this.incredibuildStopToolStripButton});
            this.incredibuildToolStrip.Location = new System.Drawing.Point(0, 0);
            this.incredibuildToolStrip.Name = "incredibuildToolStrip";
            this.incredibuildToolStrip.Size = new System.Drawing.Size(125, 25);
            this.incredibuildToolStrip.TabIndex = 0;
            this.incredibuildToolStrip.Text = "incredibuildToolStrip";
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(6, 25);
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.newToolStripButton.Tag = "FileNew";
            this.newToolStripButton.Text = "&New";
            this.newToolStripButton.ToolTipText = "New";
            this.newToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.open;
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Tag = "FileOpen";
            this.openToolStripButton.Text = "&Open";
            this.openToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.save;
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Tag = "FileSave";
            this.saveToolStripButton.Text = "&Save";
            this.saveToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // saveAllToolStripButton
            // 
            this.saveAllToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveAllToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveAllToolStripButton.Image")));
            this.saveAllToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveAllToolStripButton.Name = "saveAllToolStripButton";
            this.saveAllToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveAllToolStripButton.Tag = "FileSaveAll";
            this.saveAllToolStripButton.Text = "Save &All";
            this.saveAllToolStripButton.ToolTipText = "Save All";
            this.saveAllToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.print;
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printToolStripButton.Tag = "FilePrint";
            this.printToolStripButton.Text = "&Print";
            this.printToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // printPreviewToolStripButton
            // 
            this.printPreviewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printPreviewToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.print_preview;
            this.printPreviewToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printPreviewToolStripButton.Name = "printPreviewToolStripButton";
            this.printPreviewToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printPreviewToolStripButton.Tag = "FilePrintPreview";
            this.printPreviewToolStripButton.Text = "Print Pre&view";
            this.printPreviewToolStripButton.ToolTipText = "Print Preview";
            this.printPreviewToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // cutToolStripButton
            // 
            this.cutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cutToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.cut;
            this.cutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripButton.Name = "cutToolStripButton";
            this.cutToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.cutToolStripButton.Tag = "EditCut";
            this.cutToolStripButton.Text = "C&ut";
            this.cutToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // copyToolStripButton
            // 
            this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.copy;
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.copyToolStripButton.Tag = "EditCopy";
            this.copyToolStripButton.Text = "&Copy";
            this.copyToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // pasteToolStripButton
            // 
            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.paste;
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.pasteToolStripButton.Tag = "EditPaste";
            this.pasteToolStripButton.Text = "&Paste";
            this.pasteToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // deleteToolStripButton
            // 
            this.deleteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("deleteToolStripButton.Image")));
            this.deleteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteToolStripButton.Name = "deleteToolStripButton";
            this.deleteToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.deleteToolStripButton.Tag = "EditDelete";
            this.deleteToolStripButton.Text = "&Delete";
            this.deleteToolStripButton.ToolTipText = "Delete";
            this.deleteToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // undoToolStripSplitButton
            // 
            this.undoToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.undoToolStripSplitButton.Image = global::ragScriptEditorShared.Properties.Resources.undo;
            this.undoToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.undoToolStripSplitButton.Name = "undoToolStripSplitButton";
            this.undoToolStripSplitButton.Size = new System.Drawing.Size(32, 22);
            this.undoToolStripSplitButton.Tag = "EditUndo";
            this.undoToolStripSplitButton.Text = "&Undo";
            this.undoToolStripSplitButton.DropDownOpening += new System.EventHandler(this.undoToolStripSplitButton_DropDownOpening);
            this.undoToolStripSplitButton.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.undoToolStripSplitButton_DropDownItemClicked);
            this.undoToolStripSplitButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // redoToolStripSplitButton
            // 
            this.redoToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.redoToolStripSplitButton.Image = global::ragScriptEditorShared.Properties.Resources.redo;
            this.redoToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.redoToolStripSplitButton.Name = "redoToolStripSplitButton";
            this.redoToolStripSplitButton.Size = new System.Drawing.Size(32, 22);
            this.redoToolStripSplitButton.Tag = "EditRedo";
            this.redoToolStripSplitButton.Text = "&Redo";
            this.redoToolStripSplitButton.DropDownOpening += new System.EventHandler(this.redoToolStripSplitButton_DropDownOpening);
            this.redoToolStripSplitButton.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.redoToolStripSplitButton_DropDownItemClicked);
            this.redoToolStripSplitButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // indentToolStripButton
            // 
            this.indentToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.indentToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.indent;
            this.indentToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.indentToolStripButton.Name = "indentToolStripButton";
            this.indentToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.indentToolStripButton.Tag = "EditIndent";
            this.indentToolStripButton.Text = "&Indent";
            this.indentToolStripButton.ToolTipText = "Indent";
            this.indentToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // outdentToolStripButton
            // 
            this.outdentToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.outdentToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.outdent;
            this.outdentToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.outdentToolStripButton.Name = "outdentToolStripButton";
            this.outdentToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.outdentToolStripButton.Tag = "EditOutdent";
            this.outdentToolStripButton.Text = "&Outdent";
            this.outdentToolStripButton.ToolTipText = "Outdent";
            this.outdentToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // findReplaceToolStripButton
            // 
            this.findReplaceToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.findReplaceToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.find;
            this.findReplaceToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.findReplaceToolStripButton.Name = "findReplaceToolStripButton";
            this.findReplaceToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.findReplaceToolStripButton.Tag = "SearchFindReplace";
            this.findReplaceToolStripButton.Text = "&Find/Replace";
            this.findReplaceToolStripButton.ToolTipText = "Find/Replace";
            this.findReplaceToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // editToolStripDropDownButton
            // 
            this.editToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.editToolStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editAddOrRemoveToolStripMenuItem});
            this.editToolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("editToolStripDropDownButton.Image")));
            this.editToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editToolStripDropDownButton.Name = "editToolStripDropDownButton";
            this.editToolStripDropDownButton.Size = new System.Drawing.Size(13, 4);
            // 
            // editAddOrRemoveToolStripMenuItem
            // 
            this.editAddOrRemoveToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.editAddOrRemoveToolStripMenuItem.DropDown = this.editAddRemoveContextMenuStrip;
            this.editAddOrRemoveToolStripMenuItem.Name = "editAddOrRemoveToolStripMenuItem";
            this.editAddOrRemoveToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.editAddOrRemoveToolStripMenuItem.Text = "Add or Remove Items";
            // 
            // newVisibleToolStripMenuItem
            // 
            this.newVisibleToolStripMenuItem.Checked = true;
            this.newVisibleToolStripMenuItem.CheckOnClick = true;
            this.newVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.newVisibleToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newVisibleToolStripMenuItem.Image")));
            this.newVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newVisibleToolStripMenuItem.Name = "newVisibleToolStripMenuItem";
            this.newVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.newVisibleToolStripMenuItem.Tag = "FileNew";
            this.newVisibleToolStripMenuItem.Text = "New File";
            // 
            // openVisibleToolStripMenuItem
            // 
            this.openVisibleToolStripMenuItem.Checked = true;
            this.openVisibleToolStripMenuItem.CheckOnClick = true;
            this.openVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.openVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.open;
            this.openVisibleToolStripMenuItem.Name = "openVisibleToolStripMenuItem";
            this.openVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.openVisibleToolStripMenuItem.Tag = "FileOpen";
            this.openVisibleToolStripMenuItem.Text = "Open File";
            // 
            // saveVisibleToolStripMenuItem
            // 
            this.saveVisibleToolStripMenuItem.Checked = true;
            this.saveVisibleToolStripMenuItem.CheckOnClick = true;
            this.saveVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.saveVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.save;
            this.saveVisibleToolStripMenuItem.Name = "saveVisibleToolStripMenuItem";
            this.saveVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.saveVisibleToolStripMenuItem.Tag = "FileSave";
            this.saveVisibleToolStripMenuItem.Text = "Save File";
            // 
            // saveAllVisibleToolStripMenuItem
            // 
            this.saveAllVisibleToolStripMenuItem.Checked = true;
            this.saveAllVisibleToolStripMenuItem.CheckOnClick = true;
            this.saveAllVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.saveAllVisibleToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveAllVisibleToolStripMenuItem.Image")));
            this.saveAllVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveAllVisibleToolStripMenuItem.Name = "saveAllVisibleToolStripMenuItem";
            this.saveAllVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.saveAllVisibleToolStripMenuItem.Tag = "FileSaveAll";
            this.saveAllVisibleToolStripMenuItem.Text = "Save All";
            // 
            // printVisibleToolStripMenuItem
            // 
            this.printVisibleToolStripMenuItem.Checked = true;
            this.printVisibleToolStripMenuItem.CheckOnClick = true;
            this.printVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.printVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.print;
            this.printVisibleToolStripMenuItem.Name = "printVisibleToolStripMenuItem";
            this.printVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.printVisibleToolStripMenuItem.Tag = "FilePrint";
            this.printVisibleToolStripMenuItem.Text = "Print";
            // 
            // printPreviewVisibleToolStripMenuItem
            // 
            this.printPreviewVisibleToolStripMenuItem.Checked = true;
            this.printPreviewVisibleToolStripMenuItem.CheckOnClick = true;
            this.printPreviewVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.printPreviewVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.print_preview;
            this.printPreviewVisibleToolStripMenuItem.Name = "printPreviewVisibleToolStripMenuItem";
            this.printPreviewVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.printPreviewVisibleToolStripMenuItem.Tag = "FilePrintPreview";
            this.printPreviewVisibleToolStripMenuItem.Text = "Print Preview";
            // 
            // cutVisibleToolStripMenuItem
            // 
            this.cutVisibleToolStripMenuItem.Checked = true;
            this.cutVisibleToolStripMenuItem.CheckOnClick = true;
            this.cutVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cutVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.cut;
            this.cutVisibleToolStripMenuItem.Name = "cutVisibleToolStripMenuItem";
            this.cutVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.cutVisibleToolStripMenuItem.Tag = "EditCut";
            this.cutVisibleToolStripMenuItem.Text = "Cut";
            // 
            // copyVisibleToolStripMenuItem
            // 
            this.copyVisibleToolStripMenuItem.Checked = true;
            this.copyVisibleToolStripMenuItem.CheckOnClick = true;
            this.copyVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.copyVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.copy;
            this.copyVisibleToolStripMenuItem.Name = "copyVisibleToolStripMenuItem";
            this.copyVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.copyVisibleToolStripMenuItem.Tag = "EditCopy";
            this.copyVisibleToolStripMenuItem.Text = "Copy";
            // 
            // pasteVisibleToolStripMenuItem
            // 
            this.pasteVisibleToolStripMenuItem.Checked = true;
            this.pasteVisibleToolStripMenuItem.CheckOnClick = true;
            this.pasteVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.pasteVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.paste;
            this.pasteVisibleToolStripMenuItem.Name = "pasteVisibleToolStripMenuItem";
            this.pasteVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.pasteVisibleToolStripMenuItem.Tag = "EditPaste";
            this.pasteVisibleToolStripMenuItem.Text = "Paste";
            // 
            // deleteVisibleToolStripMenuItem
            // 
            this.deleteVisibleToolStripMenuItem.Checked = true;
            this.deleteVisibleToolStripMenuItem.CheckOnClick = true;
            this.deleteVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.deleteVisibleToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("deleteVisibleToolStripMenuItem.Image")));
            this.deleteVisibleToolStripMenuItem.Name = "deleteVisibleToolStripMenuItem";
            this.deleteVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.deleteVisibleToolStripMenuItem.Tag = "EditDelete";
            this.deleteVisibleToolStripMenuItem.Text = "Delete";
            // 
            // undoVisibleToolStripMenuItem
            // 
            this.undoVisibleToolStripMenuItem.Checked = true;
            this.undoVisibleToolStripMenuItem.CheckOnClick = true;
            this.undoVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.undoVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.undo;
            this.undoVisibleToolStripMenuItem.Name = "undoVisibleToolStripMenuItem";
            this.undoVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.undoVisibleToolStripMenuItem.Tag = "EditUndo";
            this.undoVisibleToolStripMenuItem.Text = "Undo";
            // 
            // redoVisibleToolStripMenuItem
            // 
            this.redoVisibleToolStripMenuItem.Checked = true;
            this.redoVisibleToolStripMenuItem.CheckOnClick = true;
            this.redoVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.redoVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.redo;
            this.redoVisibleToolStripMenuItem.Name = "redoVisibleToolStripMenuItem";
            this.redoVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.redoVisibleToolStripMenuItem.Tag = "EditRedo";
            this.redoVisibleToolStripMenuItem.Text = "Redo";
            // 
            // outdentVisibleToolStripMenuItem
            // 
            this.outdentVisibleToolStripMenuItem.Checked = true;
            this.outdentVisibleToolStripMenuItem.CheckOnClick = true;
            this.outdentVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.outdentVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.outdent;
            this.outdentVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.outdentVisibleToolStripMenuItem.Name = "outdentVisibleToolStripMenuItem";
            this.outdentVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.outdentVisibleToolStripMenuItem.Tag = "EditOutdent";
            this.outdentVisibleToolStripMenuItem.Text = "Outdent";
            // 
            // indentVisibleToolStripMenuItem
            // 
            this.indentVisibleToolStripMenuItem.Checked = true;
            this.indentVisibleToolStripMenuItem.CheckOnClick = true;
            this.indentVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.indentVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.indent;
            this.indentVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.indentVisibleToolStripMenuItem.Name = "indentVisibleToolStripMenuItem";
            this.indentVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.indentVisibleToolStripMenuItem.Tag = "EditIndent";
            this.indentVisibleToolStripMenuItem.Text = "Indent";
            // 
            // findReplaceVisibleToolStripMenuItem
            // 
            this.findReplaceVisibleToolStripMenuItem.Checked = true;
            this.findReplaceVisibleToolStripMenuItem.CheckOnClick = true;
            this.findReplaceVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.findReplaceVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.find;
            this.findReplaceVisibleToolStripMenuItem.Name = "findReplaceVisibleToolStripMenuItem";
            this.findReplaceVisibleToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.findReplaceVisibleToolStripMenuItem.Tag = "SearchFindReplace";
            this.findReplaceVisibleToolStripMenuItem.Text = "Find/Replace";
            // 
            // backToolStripButton
            // 
            this.backToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.backToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.back;
            this.backToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.backToolStripButton.Name = "backToolStripButton";
            this.backToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.backToolStripButton.Tag = "GoBack";
            this.backToolStripButton.Text = "Go Back";
            this.backToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // forwardToolStripButton
            // 
            this.forwardToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.forwardToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.forward;
            this.forwardToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.forwardToolStripButton.Name = "forwardToolStripButton";
            this.forwardToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.forwardToolStripButton.Tag = "GoForward";
            this.forwardToolStripButton.Text = "Go Forward";
            this.forwardToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // commentToolStripButton
            // 
            this.commentToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.commentToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.comment;
            this.commentToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.commentToolStripButton.Name = "commentToolStripButton";
            this.commentToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.commentToolStripButton.Tag = "EditCommentSelection";
            this.commentToolStripButton.Text = "Comment Selection";
            this.commentToolStripButton.ToolTipText = "Comment Selection";
            this.commentToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // uncommentToolStripButton
            // 
            this.uncommentToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.uncommentToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.uncomment;
            this.uncommentToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uncommentToolStripButton.Name = "uncommentToolStripButton";
            this.uncommentToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.uncommentToolStripButton.Tag = "EditUncommentSelection";
            this.uncommentToolStripButton.Text = "Uncomment Selection";
            this.uncommentToolStripButton.ToolTipText = "Uncomment Selection";
            this.uncommentToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // refreshIntellisenseToolStripButton
            // 
            this.refreshIntellisenseToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.refreshIntellisenseToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.refresh_intellisense;
            this.refreshIntellisenseToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshIntellisenseToolStripButton.Name = "refreshIntellisenseToolStripButton";
            this.refreshIntellisenseToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.refreshIntellisenseToolStripButton.Tag = "RefreshIntellisense";
            this.refreshIntellisenseToolStripButton.Text = "Refresh Intellisense";
            this.refreshIntellisenseToolStripButton.ToolTipText = "Refresh Intellisense";
            this.refreshIntellisenseToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // bookmarkToolStripButton
            // 
            this.bookmarkToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bookmarkToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.bookmark;
            this.bookmarkToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bookmarkToolStripButton.Name = "bookmarkToolStripButton";
            this.bookmarkToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.bookmarkToolStripButton.Tag = "EditToggleBookmark";
            this.bookmarkToolStripButton.Text = "Toggle Bookmark";
            this.bookmarkToolStripButton.ToolTipText = "Toggle Bookmark";
            this.bookmarkToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // previousBookmarkToolStripButton
            // 
            this.previousBookmarkToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.previousBookmarkToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.previous_bookmark;
            this.previousBookmarkToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.previousBookmarkToolStripButton.Name = "previousBookmarkToolStripButton";
            this.previousBookmarkToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.previousBookmarkToolStripButton.Tag = "EditPreviousBookmark";
            this.previousBookmarkToolStripButton.Text = "Goto Previous Bookmark";
            this.previousBookmarkToolStripButton.ToolTipText = "Goto Previous Bookmark";
            this.previousBookmarkToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // nextBookmarkToolStripButton
            // 
            this.nextBookmarkToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.nextBookmarkToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.next_bookmark;
            this.nextBookmarkToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.nextBookmarkToolStripButton.Name = "nextBookmarkToolStripButton";
            this.nextBookmarkToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.nextBookmarkToolStripButton.Tag = "EditNextBookmark";
            this.nextBookmarkToolStripButton.Text = "Goto Next Bookmark";
            this.nextBookmarkToolStripButton.ToolTipText = "Goto Next Bookmark";
            this.nextBookmarkToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // clearBookmarksToolStripButton
            // 
            this.clearBookmarksToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.clearBookmarksToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.clear_bookmarks;
            this.clearBookmarksToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clearBookmarksToolStripButton.Name = "clearBookmarksToolStripButton";
            this.clearBookmarksToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.clearBookmarksToolStripButton.Tag = "EditClearBookmarks";
            this.clearBookmarksToolStripButton.Text = "Clear Bookmarks";
            this.clearBookmarksToolStripButton.ToolTipText = "Clear Bookmarks";
            this.clearBookmarksToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // hideSelectionToolStripButton
            // 
            this.hideSelectionToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.hideSelectionToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.outline_hide;
            this.hideSelectionToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.hideSelectionToolStripButton.Name = "hideSelectionToolStripButton";
            this.hideSelectionToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.hideSelectionToolStripButton.Tag = "OutliningHideSelection";
            this.hideSelectionToolStripButton.Text = "Hide Selection";
            this.hideSelectionToolStripButton.ToolTipText = "Hide Selection";
            this.hideSelectionToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // unhideSelectionToolStripButton
            // 
            this.unhideSelectionToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.unhideSelectionToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.outline_unhide;
            this.unhideSelectionToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.unhideSelectionToolStripButton.Name = "unhideSelectionToolStripButton";
            this.unhideSelectionToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.unhideSelectionToolStripButton.Tag = "OutliningStopHidingCurrent";
            this.unhideSelectionToolStripButton.Text = "Unhide Selection";
            this.unhideSelectionToolStripButton.ToolTipText = "Unhide Selection";
            this.unhideSelectionToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // toggleOutliningToolStripButton
            // 
            this.toggleOutliningToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toggleOutliningToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.outline_toggle;
            this.toggleOutliningToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toggleOutliningToolStripButton.Name = "toggleOutliningToolStripButton";
            this.toggleOutliningToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.toggleOutliningToolStripButton.Tag = "OutliningToggleOutliningExpansion";
            this.toggleOutliningToolStripButton.Text = "Toggle Outlining Expansion";
            this.toggleOutliningToolStripButton.ToolTipText = "Toggle Outlining Expansion";
            this.toggleOutliningToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // toggleAllOutliningToolStripButton
            // 
            this.toggleAllOutliningToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toggleAllOutliningToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.outline_toggle_all;
            this.toggleAllOutliningToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toggleAllOutliningToolStripButton.Name = "toggleAllOutliningToolStripButton";
            this.toggleAllOutliningToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.toggleAllOutliningToolStripButton.Tag = "OutliningToggleAllOutlining";
            this.toggleAllOutliningToolStripButton.Text = "Toggle All Outlining";
            this.toggleAllOutliningToolStripButton.ToolTipText = "Toggle All Outlining";
            this.toggleAllOutliningToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // codeToolStripDropDownButton
            // 
            this.codeToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.codeToolStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.codeAddOrRemoveToolStripMenuItem});
            this.codeToolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("codeToolStripDropDownButton.Image")));
            this.codeToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.codeToolStripDropDownButton.Name = "codeToolStripDropDownButton";
            this.codeToolStripDropDownButton.Size = new System.Drawing.Size(13, 22);
            // 
            // codeAddOrRemoveToolStripMenuItem
            // 
            this.codeAddOrRemoveToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.codeAddOrRemoveToolStripMenuItem.DropDown = this.codeAddRemoveContextMenuStrip;
            this.codeAddOrRemoveToolStripMenuItem.Name = "codeAddOrRemoveToolStripMenuItem";
            this.codeAddOrRemoveToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.codeAddOrRemoveToolStripMenuItem.Text = "Add or Remove Items";
            // 
            // commentVisibleToolStripMenuItem
            // 
            this.commentVisibleToolStripMenuItem.Checked = true;
            this.commentVisibleToolStripMenuItem.CheckOnClick = true;
            this.commentVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.commentVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.comment;
            this.commentVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.commentVisibleToolStripMenuItem.Name = "commentVisibleToolStripMenuItem";
            this.commentVisibleToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.commentVisibleToolStripMenuItem.Tag = "EditCommentSelection";
            this.commentVisibleToolStripMenuItem.Text = "Comment Selection";
            // 
            // uncommentVisibleToolStripMenuItem
            // 
            this.uncommentVisibleToolStripMenuItem.Checked = true;
            this.uncommentVisibleToolStripMenuItem.CheckOnClick = true;
            this.uncommentVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.uncommentVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.uncomment;
            this.uncommentVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uncommentVisibleToolStripMenuItem.Name = "uncommentVisibleToolStripMenuItem";
            this.uncommentVisibleToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.uncommentVisibleToolStripMenuItem.Tag = "EditUncommentSelection";
            this.uncommentVisibleToolStripMenuItem.Text = "Uncomment Selection";
            // 
            // refreshIntellisenseVisibleToolStripMenuItem
            // 
            this.refreshIntellisenseVisibleToolStripMenuItem.Checked = true;
            this.refreshIntellisenseVisibleToolStripMenuItem.CheckOnClick = true;
            this.refreshIntellisenseVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.refreshIntellisenseVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.refresh_intellisense;
            this.refreshIntellisenseVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshIntellisenseVisibleToolStripMenuItem.Name = "refreshIntellisenseVisibleToolStripMenuItem";
            this.refreshIntellisenseVisibleToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.refreshIntellisenseVisibleToolStripMenuItem.Tag = "RefreshIntellisense";
            this.refreshIntellisenseVisibleToolStripMenuItem.Text = "Refresh Intellisense";
            // 
            // bookmarkVisibleToolStripMenuItem
            // 
            this.bookmarkVisibleToolStripMenuItem.Checked = true;
            this.bookmarkVisibleToolStripMenuItem.CheckOnClick = true;
            this.bookmarkVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.bookmarkVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.bookmark;
            this.bookmarkVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bookmarkVisibleToolStripMenuItem.Name = "bookmarkVisibleToolStripMenuItem";
            this.bookmarkVisibleToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.bookmarkVisibleToolStripMenuItem.Tag = "EditToggleBookmark";
            this.bookmarkVisibleToolStripMenuItem.Text = "Toggle Bookmark";
            // 
            // previousBookmarkVisibleToolStripMenuItem
            // 
            this.previousBookmarkVisibleToolStripMenuItem.Checked = true;
            this.previousBookmarkVisibleToolStripMenuItem.CheckOnClick = true;
            this.previousBookmarkVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.previousBookmarkVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.previous_bookmark;
            this.previousBookmarkVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.previousBookmarkVisibleToolStripMenuItem.Name = "previousBookmarkVisibleToolStripMenuItem";
            this.previousBookmarkVisibleToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.previousBookmarkVisibleToolStripMenuItem.Tag = "EditPreviousBookmark";
            this.previousBookmarkVisibleToolStripMenuItem.Text = "Previous Bookmark";
            // 
            // nextBookmarkVisibleToolStripMenuItem
            // 
            this.nextBookmarkVisibleToolStripMenuItem.Checked = true;
            this.nextBookmarkVisibleToolStripMenuItem.CheckOnClick = true;
            this.nextBookmarkVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.nextBookmarkVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.next_bookmark;
            this.nextBookmarkVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.nextBookmarkVisibleToolStripMenuItem.Name = "nextBookmarkVisibleToolStripMenuItem";
            this.nextBookmarkVisibleToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.nextBookmarkVisibleToolStripMenuItem.Tag = "EditNextBookmark";
            this.nextBookmarkVisibleToolStripMenuItem.Text = "Next Bookmark";
            // 
            // clearBookmarksVisibleToolStripMenuItem
            // 
            this.clearBookmarksVisibleToolStripMenuItem.Checked = true;
            this.clearBookmarksVisibleToolStripMenuItem.CheckOnClick = true;
            this.clearBookmarksVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.clearBookmarksVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.clear_bookmarks;
            this.clearBookmarksVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clearBookmarksVisibleToolStripMenuItem.Name = "clearBookmarksVisibleToolStripMenuItem";
            this.clearBookmarksVisibleToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.clearBookmarksVisibleToolStripMenuItem.Tag = "EditClearBookmarks";
            this.clearBookmarksVisibleToolStripMenuItem.Text = "Clear Bookmarks";
            // 
            // hideVisibleToolStripMenuItem
            // 
            this.hideVisibleToolStripMenuItem.Checked = true;
            this.hideVisibleToolStripMenuItem.CheckOnClick = true;
            this.hideVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.hideVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.outline_hide;
            this.hideVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.hideVisibleToolStripMenuItem.Name = "hideVisibleToolStripMenuItem";
            this.hideVisibleToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.hideVisibleToolStripMenuItem.Tag = "OutliningHideSelection";
            this.hideVisibleToolStripMenuItem.Text = "Hide Selection";
            // 
            // unhideVisibleToolStripMenuItem
            // 
            this.unhideVisibleToolStripMenuItem.Checked = true;
            this.unhideVisibleToolStripMenuItem.CheckOnClick = true;
            this.unhideVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.unhideVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.outline_unhide;
            this.unhideVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.unhideVisibleToolStripMenuItem.Name = "unhideVisibleToolStripMenuItem";
            this.unhideVisibleToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.unhideVisibleToolStripMenuItem.Tag = "OutliningStopHidingCurrent";
            this.unhideVisibleToolStripMenuItem.Text = "Stop Hiding Current";
            // 
            // toggleOutliningVisibleToolStripMenuItem
            // 
            this.toggleOutliningVisibleToolStripMenuItem.Checked = true;
            this.toggleOutliningVisibleToolStripMenuItem.CheckOnClick = true;
            this.toggleOutliningVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toggleOutliningVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.outline_toggle;
            this.toggleOutliningVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toggleOutliningVisibleToolStripMenuItem.Name = "toggleOutliningVisibleToolStripMenuItem";
            this.toggleOutliningVisibleToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.toggleOutliningVisibleToolStripMenuItem.Tag = "OutliningToggleOutliningExpansion";
            this.toggleOutliningVisibleToolStripMenuItem.Text = "Toggle Outlining Expansion";
            // 
            // toggleAllOutliningVisibleToolStripMenuItem
            // 
            this.toggleAllOutliningVisibleToolStripMenuItem.Checked = true;
            this.toggleAllOutliningVisibleToolStripMenuItem.CheckOnClick = true;
            this.toggleAllOutliningVisibleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toggleAllOutliningVisibleToolStripMenuItem.Image = global::ragScriptEditorShared.Properties.Resources.outline_toggle_all;
            this.toggleAllOutliningVisibleToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toggleAllOutliningVisibleToolStripMenuItem.Name = "toggleAllOutliningVisibleToolStripMenuItem";
            this.toggleAllOutliningVisibleToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.toggleAllOutliningVisibleToolStripMenuItem.Tag = "OutliningToggleAllOutlining";
            this.toggleAllOutliningVisibleToolStripMenuItem.Text = "Toggle All Outlining";
            // 
            // customToolStripDropDownButton
            // 
            this.customToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.customToolStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customAddOrRemoveToolStripMenuItem,
            this.toolStripSeparator1,
            this.customizeToolStripMenuItem});
            this.customToolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("customToolStripDropDownButton.Image")));
            this.customToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.customToolStripDropDownButton.Name = "customToolStripDropDownButton";
            this.customToolStripDropDownButton.Size = new System.Drawing.Size(13, 22);
            // 
            // customAddOrRemoveToolStripMenuItem
            // 
            this.customAddOrRemoveToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.customAddOrRemoveToolStripMenuItem.DropDown = this.customAddRemoveContextMenuStrip;
            this.customAddOrRemoveToolStripMenuItem.Name = "customAddOrRemoveToolStripMenuItem";
            this.customAddOrRemoveToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.customAddOrRemoveToolStripMenuItem.Text = "Add or Remove Items";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(185, 6);
            // 
            // customizeToolStripMenuItem
            // 
            this.customizeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.customizeToolStripMenuItem.Name = "customizeToolStripMenuItem";
            this.customizeToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.customizeToolStripMenuItem.Text = "Customize...";
            // 
            // toggleBreakpointToolStripButton
            // 
            this.toggleBreakpointToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toggleBreakpointToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.breakpoint;
            this.toggleBreakpointToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toggleBreakpointToolStripButton.Name = "toggleBreakpointToolStripButton";
            this.toggleBreakpointToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.toggleBreakpointToolStripButton.Tag = "DebugToggleBreakpoint";
            this.toggleBreakpointToolStripButton.Text = "Toggle Breakpoint (F9)";
            this.toggleBreakpointToolStripButton.ToolTipText = "Toggle Breakpoint (F9)";
            this.toggleBreakpointToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // toggleBreakpointAllToolStripButton
            // 
            this.toggleBreakpointAllToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toggleBreakpointAllToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.breakpoint_all;
            this.toggleBreakpointAllToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toggleBreakpointAllToolStripButton.Name = "toggleBreakpointAllToolStripButton";
            this.toggleBreakpointAllToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.toggleBreakpointAllToolStripButton.Tag = "DebugToggleBreakpointAllThreads";
            this.toggleBreakpointAllToolStripButton.Text = "Toggle Breakpoint All Threads (Shift + F9)";
            this.toggleBreakpointAllToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // toggleBreakpointStopGameToolStripButton
            // 
            this.toggleBreakpointStopGameToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toggleBreakpointStopGameToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.breakpoint_stop_game;
            this.toggleBreakpointStopGameToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toggleBreakpointStopGameToolStripButton.Name = "toggleBreakpointStopGameToolStripButton";
            this.toggleBreakpointStopGameToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.toggleBreakpointStopGameToolStripButton.Tag = "DebugToggleBreakpointStopGame";
            this.toggleBreakpointStopGameToolStripButton.Text = "Toggle Breakpoint Stop Game (Ctrl + F9)";
            this.toggleBreakpointStopGameToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // toggleBreakpointAllStopToolStripButton
            // 
            this.toggleBreakpointAllStopToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toggleBreakpointAllStopToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.breakpoint_all_stop_game;
            this.toggleBreakpointAllStopToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toggleBreakpointAllStopToolStripButton.Name = "toggleBreakpointAllStopToolStripButton";
            this.toggleBreakpointAllStopToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.toggleBreakpointAllStopToolStripButton.Tag = "DebugToggleBreakpointAllThreadsStopGame";
            this.toggleBreakpointAllStopToolStripButton.Text = "Toggle Breakpoint All Threads Stop Game (Ctrl + Shift + F9)";
            this.toggleBreakpointAllStopToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // playToolStripButton
            // 
            this.playToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.playToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.play;
            this.playToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.playToolStripButton.Name = "playToolStripButton";
            this.playToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.playToolStripButton.Tag = "DebugContinue";
            this.playToolStripButton.Text = "Continue (F5)";
            this.playToolStripButton.ToolTipText = "Continue (F5)";
            this.playToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // pauseToolStripButton
            // 
            this.pauseToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pauseToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.pause;
            this.pauseToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pauseToolStripButton.Name = "pauseToolStripButton";
            this.pauseToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.pauseToolStripButton.Tag = "DebugPause";
            this.pauseToolStripButton.Text = "Pause (F12)";
            this.pauseToolStripButton.ToolTipText = "Pause (F12)";
            this.pauseToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // stepIntoToolStripButton
            // 
            this.stepIntoToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stepIntoToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.step;
            this.stepIntoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stepIntoToolStripButton.Name = "stepIntoToolStripButton";
            this.stepIntoToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.stepIntoToolStripButton.Tag = "DebugStepInto";
            this.stepIntoToolStripButton.Text = "Step Into (F11)";
            this.stepIntoToolStripButton.ToolTipText = "Step Into (F11)";
            this.stepIntoToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // stepOverToolStripButton
            // 
            this.stepOverToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stepOverToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.step_over;
            this.stepOverToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stepOverToolStripButton.Name = "stepOverToolStripButton";
            this.stepOverToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.stepOverToolStripButton.Tag = "DebugStepOver";
            this.stepOverToolStripButton.Text = "Step Over (F10)";
            this.stepOverToolStripButton.ToolTipText = "Step Over (F10)";
            this.stepOverToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // stepOutToolStripButton
            // 
            this.stepOutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stepOutToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.step_out;
            this.stepOutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stepOutToolStripButton.Name = "stepOutToolStripButton";
            this.stepOutToolStripButton.Size = new System.Drawing.Size(23, 20);
            this.stepOutToolStripButton.Tag = "DebugStepOut";
            this.stepOutToolStripButton.Text = "Step Out";
            this.stepOutToolStripButton.ToolTipText = "Step Out (Shift + F11)";
            this.stepOutToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // debuggerOptionsToolStripDropDownButton
            // 
            this.debuggerOptionsToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.debuggerOptionsToolStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showLineNumbersToolStripMenuItem,
            this.toolStripSeparator13,
            this.debuggerHighlightEnumsToolStripMenuItem,
            this.debuggerHighlightQuickInfoPopupsToolStripMenuItem,
            this.debuggerHighlightDisabledCodeBlocksToolStripMenuItem,
            this.toolStripSeparator12,
            this.startAutomaticOutliningToolStripMenuItem,
            this.startManualOutliningToolStripMenuItem,
            this.stopOutliningToolStripMenuItem,
            this.toolStripSeparator14,
            this.changeIncludesToolStripMenuItem});
            this.debuggerOptionsToolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("debuggerOptionsToolStripDropDownButton.Image")));
            this.debuggerOptionsToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.debuggerOptionsToolStripDropDownButton.Name = "debuggerOptionsToolStripDropDownButton";
            this.debuggerOptionsToolStripDropDownButton.Size = new System.Drawing.Size(62, 19);
            this.debuggerOptionsToolStripDropDownButton.Text = "Options";
            this.debuggerOptionsToolStripDropDownButton.DropDownOpening += new System.EventHandler(this.debuggerOptionsToolStripDropDownButton_DropDownOpening);
            // 
            // showLineNumbersToolStripMenuItem
            // 
            this.showLineNumbersToolStripMenuItem.Checked = true;
            this.showLineNumbersToolStripMenuItem.CheckOnClick = true;
            this.showLineNumbersToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showLineNumbersToolStripMenuItem.Name = "showLineNumbersToolStripMenuItem";
            this.showLineNumbersToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.showLineNumbersToolStripMenuItem.Tag = "";
            this.showLineNumbersToolStripMenuItem.Text = "Sho&w Line Numbers";
            this.showLineNumbersToolStripMenuItem.Click += new System.EventHandler(this.showLineNumbersToolStripMenuItem_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(237, 6);
            // 
            // debuggerHighlightEnumsToolStripMenuItem
            // 
            this.debuggerHighlightEnumsToolStripMenuItem.Checked = true;
            this.debuggerHighlightEnumsToolStripMenuItem.CheckOnClick = true;
            this.debuggerHighlightEnumsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.debuggerHighlightEnumsToolStripMenuItem.Name = "debuggerHighlightEnumsToolStripMenuItem";
            this.debuggerHighlightEnumsToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.debuggerHighlightEnumsToolStripMenuItem.Text = "Highlight &Enums";
            this.debuggerHighlightEnumsToolStripMenuItem.Click += new System.EventHandler(this.debuggerHighlightEnumsToolStripMenuItem_Click);
            // 
            // debuggerHighlightQuickInfoPopupsToolStripMenuItem
            // 
            this.debuggerHighlightQuickInfoPopupsToolStripMenuItem.Checked = true;
            this.debuggerHighlightQuickInfoPopupsToolStripMenuItem.CheckOnClick = true;
            this.debuggerHighlightQuickInfoPopupsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.debuggerHighlightQuickInfoPopupsToolStripMenuItem.Name = "debuggerHighlightQuickInfoPopupsToolStripMenuItem";
            this.debuggerHighlightQuickInfoPopupsToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.debuggerHighlightQuickInfoPopupsToolStripMenuItem.Text = "Highlight &QuickInfo Popups";
            this.debuggerHighlightQuickInfoPopupsToolStripMenuItem.Click += new System.EventHandler(this.debuggerHighlightQuickInfoPopupsToolStripMenuItem_Click);
            // 
            // debuggerHighlightDisabledCodeBlocksToolStripMenuItem
            // 
            this.debuggerHighlightDisabledCodeBlocksToolStripMenuItem.Checked = true;
            this.debuggerHighlightDisabledCodeBlocksToolStripMenuItem.CheckOnClick = true;
            this.debuggerHighlightDisabledCodeBlocksToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.debuggerHighlightDisabledCodeBlocksToolStripMenuItem.Name = "debuggerHighlightDisabledCodeBlocksToolStripMenuItem";
            this.debuggerHighlightDisabledCodeBlocksToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.debuggerHighlightDisabledCodeBlocksToolStripMenuItem.Text = "Highlight &Disabled Code Blocks";
            this.debuggerHighlightDisabledCodeBlocksToolStripMenuItem.Click += new System.EventHandler(this.debuggerHighlightDisabledCodeBlocksToolStripMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(237, 6);
            // 
            // startAutomaticOutliningToolStripMenuItem
            // 
            this.startAutomaticOutliningToolStripMenuItem.Name = "startAutomaticOutliningToolStripMenuItem";
            this.startAutomaticOutliningToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.startAutomaticOutliningToolStripMenuItem.Tag = "OutliningStartAutomaticOutlining";
            this.startAutomaticOutliningToolStripMenuItem.Text = "Start &Automatic Outlining";
            this.startAutomaticOutliningToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // startManualOutliningToolStripMenuItem
            // 
            this.startManualOutliningToolStripMenuItem.Name = "startManualOutliningToolStripMenuItem";
            this.startManualOutliningToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.startManualOutliningToolStripMenuItem.Tag = "OutliningStartManualOutlining";
            this.startManualOutliningToolStripMenuItem.Text = "Start &Manual Outlining";
            this.startManualOutliningToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // stopOutliningToolStripMenuItem
            // 
            this.stopOutliningToolStripMenuItem.Name = "stopOutliningToolStripMenuItem";
            this.stopOutliningToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.stopOutliningToolStripMenuItem.Tag = "OutliningStopOutlining";
            this.stopOutliningToolStripMenuItem.Text = "&Stop Outlining";
            this.stopOutliningToolStripMenuItem.Click += new System.EventHandler(this.ExecuteAppAction_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(237, 6);
            // 
            // changeIncludesToolStripMenuItem
            // 
            this.changeIncludesToolStripMenuItem.Name = "changeIncludesToolStripMenuItem";
            this.changeIncludesToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.changeIncludesToolStripMenuItem.Text = "Change &Includes";
            this.changeIncludesToolStripMenuItem.Click += new System.EventHandler(this.changeIncludesToolStripMenuItem_Click);
            // 
            // recordMacroToolStripButton
            // 
            this.recordMacroToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.recordMacroToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.record_macro;
            this.recordMacroToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.recordMacroToolStripButton.Name = "recordMacroToolStripButton";
            this.recordMacroToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.recordMacroToolStripButton.Tag = "ToolsMacrosRecord";
            this.recordMacroToolStripButton.Text = "Record Macro";
            this.recordMacroToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // pauseMacroToolStripButton
            // 
            this.pauseMacroToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pauseMacroToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.pause_macro;
            this.pauseMacroToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pauseMacroToolStripButton.Name = "pauseMacroToolStripButton";
            this.pauseMacroToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.pauseMacroToolStripButton.Tag = "ToolsMacrosPause";
            this.pauseMacroToolStripButton.Text = "Pause Macro Recording";
            this.pauseMacroToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // stopMacroToolStripButton
            // 
            this.stopMacroToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stopMacroToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.stop_macro;
            this.stopMacroToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stopMacroToolStripButton.Name = "stopMacroToolStripButton";
            this.stopMacroToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.stopMacroToolStripButton.Tag = "ToolsMacrosStop";
            this.stopMacroToolStripButton.Text = "Stop Macro Recording";
            this.stopMacroToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // cancelMacroToolStripButton
            // 
            this.cancelMacroToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cancelMacroToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.delete_macro;
            this.cancelMacroToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cancelMacroToolStripButton.Name = "cancelMacroToolStripButton";
            this.cancelMacroToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.cancelMacroToolStripButton.Tag = "ToolsMacrosCancel";
            this.cancelMacroToolStripButton.Text = "Cancel Macro Recording";
            this.cancelMacroToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // playMacroToolStripButton
            // 
            this.playMacroToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.playMacroToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.play_macro;
            this.playMacroToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.playMacroToolStripButton.Name = "playMacroToolStripButton";
            this.playMacroToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.playMacroToolStripButton.Tag = "ToolsMacrosRun";
            this.playMacroToolStripButton.Text = "Play Macro";
            this.playMacroToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // incredibuildBuildToolStripButton
            // 
            this.incredibuildBuildToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.incredibuildBuildToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.incredibuildBuild16;
            this.incredibuildBuildToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.incredibuildBuildToolStripButton.Name = "incredibuildBuildToolStripButton";
            this.incredibuildBuildToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.incredibuildBuildToolStripButton.Tag = "IncredibuildBuild";
            this.incredibuildBuildToolStripButton.Text = "Incredibuild Build";
            this.incredibuildBuildToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // incredibuildRebuildToolStripButton
            // 
            this.incredibuildRebuildToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.incredibuildRebuildToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.incredibuildRebuild16;
            this.incredibuildRebuildToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.incredibuildRebuildToolStripButton.Name = "incredibuildRebuildToolStripButton";
            this.incredibuildRebuildToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.incredibuildRebuildToolStripButton.Tag = "IncredibuildRebuild";
            this.incredibuildRebuildToolStripButton.Text = "Incredibuild Rebuild";
            this.incredibuildRebuildToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // incredibuildCleanToolStripButton
            // 
            this.incredibuildCleanToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.incredibuildCleanToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.incredibuildClean16;
            this.incredibuildCleanToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.incredibuildCleanToolStripButton.Name = "incredibuildCleanToolStripButton";
            this.incredibuildCleanToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.incredibuildCleanToolStripButton.Tag = "IncredibuildClean";
            this.incredibuildCleanToolStripButton.Text = "Incredibuild Clean";
            this.incredibuildCleanToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            // 
            // incredibuildStopToolStripButton
            // 
            this.incredibuildStopToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.incredibuildStopToolStripButton.Image = global::ragScriptEditorShared.Properties.Resources.incredibuildStop16;
            this.incredibuildStopToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.incredibuildStopToolStripButton.Name = "incredibuildStopToolStripButton";
            this.incredibuildStopToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.incredibuildStopToolStripButton.Tag = "IncredibuildStop";
            this.incredibuildStopToolStripButton.Text = "Incredibuild Stop";
            this.incredibuildStopToolStripButton.Click += new System.EventHandler(this.ToolStripButton_Click);
            this.editToolStrip.ResumeLayout(false);
            this.editToolStrip.PerformLayout();
            this.editAddRemoveContextMenuStrip.ResumeLayout(false);
            this.codeToolStrip.ResumeLayout(false);
            this.codeToolStrip.PerformLayout();
            this.codeAddRemoveContextMenuStrip.ResumeLayout(false);
            this.customToolStrip.ResumeLayout(false);
            this.customToolStrip.PerformLayout();
            this.debuggerToolStrip.ResumeLayout(false);
            this.debuggerToolStrip.PerformLayout();
            this.macroToolStrip.ResumeLayout(false);
            this.macroToolStrip.PerformLayout();
            this.incredibuildToolStrip.ResumeLayout(false);
            this.incredibuildToolStrip.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip editToolStrip;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripButton saveAllToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator22;
        private System.Windows.Forms.ToolStripButton printToolStripButton;
        private System.Windows.Forms.ToolStripButton printPreviewToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator23;
        private System.Windows.Forms.ToolStripButton cutToolStripButton;
        private System.Windows.Forms.ToolStripButton copyToolStripButton;
        private System.Windows.Forms.ToolStripButton pasteToolStripButton;
        private System.Windows.Forms.ToolStripButton deleteToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator24;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator25;
        private System.Windows.Forms.ToolStripButton indentToolStripButton;
        private System.Windows.Forms.ToolStripButton outdentToolStripButton;
        private System.Windows.Forms.ToolStrip codeToolStrip;
        private System.Windows.Forms.ToolStripButton commentToolStripButton;
        private System.Windows.Forms.ToolStripButton uncommentToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator26;
        private System.Windows.Forms.ToolStripButton refreshIntellisenseToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator27;
        private System.Windows.Forms.ToolStripButton bookmarkToolStripButton;
        private System.Windows.Forms.ToolStripButton previousBookmarkToolStripButton;
        private System.Windows.Forms.ToolStripButton nextBookmarkToolStripButton;
        private System.Windows.Forms.ToolStripButton clearBookmarksToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator28;
        private System.Windows.Forms.ToolStripButton hideSelectionToolStripButton;
        private System.Windows.Forms.ToolStripButton unhideSelectionToolStripButton;
        private System.Windows.Forms.ToolStripButton toggleOutliningToolStripButton;
        private System.Windows.Forms.ToolStripButton toggleAllOutliningToolStripButton;
        private System.Windows.Forms.ToolStrip customToolStrip;
        private System.Windows.Forms.ToolStrip debuggerToolStrip;
        private System.Windows.Forms.ToolStripButton playToolStripButton;
        private System.Windows.Forms.ToolStripButton pauseToolStripButton;
        private System.Windows.Forms.ToolStripButton stepIntoToolStripButton;
        private System.Windows.Forms.ToolStripButton toggleBreakpointToolStripButton;
        private System.Windows.Forms.ToolStripDropDownButton editToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem editAddOrRemoveToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton customToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem customAddOrRemoveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem customizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton codeToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem codeAddOrRemoveToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip editAddRemoveContextMenuStrip;
        private System.Windows.Forms.ContextMenuStrip codeAddRemoveContextMenuStrip;
        private System.Windows.Forms.ContextMenuStrip customAddRemoveContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem newVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAllVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem printVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printPreviewVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cutVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem undoVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem outdentVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indentVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commentVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uncommentVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem refreshIntellisenseVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem bookmarkVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem previousBookmarkVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nextBookmarkVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearBookmarksVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem hideVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unhideVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toggleOutliningVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toggleAllOutliningVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripDropDownButton debuggerOptionsToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem debuggerHighlightEnumsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSplitButton redoToolStripSplitButton;
        private System.Windows.Forms.ToolStripSplitButton undoToolStripSplitButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton findReplaceToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem findReplaceVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStrip macroToolStrip;
        private System.Windows.Forms.ToolStripButton recordMacroToolStripButton;
        private System.Windows.Forms.ToolStripButton pauseMacroToolStripButton;
        private System.Windows.Forms.ToolStripButton cancelMacroToolStripButton;
        private System.Windows.Forms.ToolStripButton playMacroToolStripButton;
        private System.Windows.Forms.ToolStripButton stopMacroToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem showLineNumbersToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem startAutomaticOutliningToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startManualOutliningToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopOutliningToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debuggerHighlightQuickInfoPopupsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem changeIncludesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripComboBox compilingConfigurationToolStripComboBox;
        private System.Windows.Forms.ToolStripMenuItem compilingConfigurationsVisibleToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripMenuItem debuggerHighlightDisabledCodeBlocksToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton stepOverToolStripButton;
        private System.Windows.Forms.ToolStripButton stepOutToolStripButton;
        private System.Windows.Forms.ToolStripButton toggleBreakpointAllToolStripButton;
        private System.Windows.Forms.ToolStripButton toggleBreakpointStopGameToolStripButton;
        private System.Windows.Forms.ToolStripButton toggleBreakpointAllStopToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripComboBox platformToolStripComboBox;
        private System.Windows.Forms.ToolStrip incredibuildToolStrip;
        private System.Windows.Forms.ToolStripButton incredibuildBuildToolStripButton;
        private System.Windows.Forms.ToolStripButton incredibuildRebuildToolStripButton;
        private System.Windows.Forms.ToolStripButton incredibuildCleanToolStripButton;
        private System.Windows.Forms.ToolStripButton incredibuildStopToolStripButton;
        private System.Windows.Forms.ToolStripButton backToolStripButton;
        private System.Windows.Forms.ToolStripButton forwardToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
    }
}
