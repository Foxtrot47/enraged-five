namespace ragScriptEditorShared
{
    partial class ProjectExplorer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( ProjectExplorer ) );
            this.treeView = new System.Windows.Forms.TreeView();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compileFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compileAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rebuildAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.addExistingFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.reloadMissingFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.locateMissingFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.treeViewImageList = new System.Windows.Forms.ImageList( this.components );
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.openProjectDialog = new System.Windows.Forms.OpenFileDialog();
            this.dragDropImageList = new System.Windows.Forms.ImageList( this.components );
            this.projectFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.sortButton = new System.Windows.Forms.Button();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView
            // 
            this.treeView.AllowDrop = true;
            this.treeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeView.ContextMenuStrip = this.contextMenuStrip;
            this.treeView.ImageIndex = 0;
            this.treeView.ImageList = this.treeViewImageList;
            this.treeView.Location = new System.Drawing.Point( 0, 29 );
            this.treeView.Name = "treeView";
            this.treeView.SelectedImageIndex = 0;
            this.treeView.Size = new System.Drawing.Size( 320, 398 );
            this.treeView.TabIndex = 0;
            this.treeView.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler( this.treeView_NodeMouseDoubleClick );
            this.treeView.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler( this.treeView_GiveFeedback );
            this.treeView.MouseClick += new System.Windows.Forms.MouseEventHandler( this.treeView_MouseClick );
            this.treeView.DragLeave += new System.EventHandler( this.treeView_DragLeave );
            this.treeView.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler( this.treeView_AfterLabelEdit );
            this.treeView.DragDrop += new System.Windows.Forms.DragEventHandler( this.treeView_DragDrop );
            this.treeView.DragEnter += new System.Windows.Forms.DragEventHandler( this.treeView_DragEnter );
            this.treeView.KeyDown += new System.Windows.Forms.KeyEventHandler( this.treeView_KeyDown );
            this.treeView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler( this.treeView_ItemDrag );
            this.treeView.DragOver += new System.Windows.Forms.DragEventHandler( this.treeView_DragOver );
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.compileFileToolStripMenuItem,
            this.compileAllToolStripMenuItem,
            this.rebuildAllToolStripMenuItem,
            this.toolStripSeparator1,
            this.addExistingFilesToolStripMenuItem,
            this.addNewFolderToolStripMenuItem,
            this.toolStripSeparator2,
            this.reloadMissingFileToolStripMenuItem,
            this.locateMissingFileToolStripMenuItem,
            this.toolStripSeparator3,
            this.renameToolStripMenuItem,
            this.removeToolStripMenuItem} );
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.ShowImageMargin = false;
            this.contextMenuStrip.Size = new System.Drawing.Size( 144, 242 );
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler( this.contextMenuStrip_Opening );
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size( 143, 22 );
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler( this.openToolStripMenuItem_Click );
            // 
            // compileFileToolStripMenuItem
            // 
            this.compileFileToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.compileFileToolStripMenuItem.Name = "compileFileToolStripMenuItem";
            this.compileFileToolStripMenuItem.Size = new System.Drawing.Size( 143, 22 );
            this.compileFileToolStripMenuItem.Text = "&Compile File";
            this.compileFileToolStripMenuItem.Click += new System.EventHandler( this.compileFileToolStripMenuItem_Click );
            // 
            // compileAllToolStripMenuItem
            // 
            this.compileAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.compileAllToolStripMenuItem.Name = "compileAllToolStripMenuItem";
            this.compileAllToolStripMenuItem.Size = new System.Drawing.Size( 143, 22 );
            this.compileAllToolStripMenuItem.Text = "Compile &All";
            this.compileAllToolStripMenuItem.Click += new System.EventHandler( this.compileAllToolStripMenuItem_Click );
            // 
            // rebuildAllToolStripMenuItem
            // 
            this.rebuildAllToolStripMenuItem.Name = "rebuildAllToolStripMenuItem";
            this.rebuildAllToolStripMenuItem.Size = new System.Drawing.Size( 143, 22 );
            this.rebuildAllToolStripMenuItem.Text = "Re&build All";
            this.rebuildAllToolStripMenuItem.Click += new System.EventHandler( this.rebuildAllToolStripMenuItem_Click );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 140, 6 );
            // 
            // addExistingFilesToolStripMenuItem
            // 
            this.addExistingFilesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.addExistingFilesToolStripMenuItem.Name = "addExistingFilesToolStripMenuItem";
            this.addExistingFilesToolStripMenuItem.Size = new System.Drawing.Size( 143, 22 );
            this.addExistingFilesToolStripMenuItem.Text = "Add Existing &File(s)";
            this.addExistingFilesToolStripMenuItem.Click += new System.EventHandler( this.addExistingFilesToolStripMenuItem_Click );
            // 
            // addNewFolderToolStripMenuItem
            // 
            this.addNewFolderToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.addNewFolderToolStripMenuItem.Name = "addNewFolderToolStripMenuItem";
            this.addNewFolderToolStripMenuItem.Size = new System.Drawing.Size( 143, 22 );
            this.addNewFolderToolStripMenuItem.Text = "Add New F&older";
            this.addNewFolderToolStripMenuItem.Click += new System.EventHandler( this.addNewFolderToolStripMenuItem_Click );
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 140, 6 );
            // 
            // reloadMissingFileToolStripMenuItem
            // 
            this.reloadMissingFileToolStripMenuItem.Name = "reloadMissingFileToolStripMenuItem";
            this.reloadMissingFileToolStripMenuItem.Size = new System.Drawing.Size( 143, 22 );
            this.reloadMissingFileToolStripMenuItem.Text = "Reload &Missing File";
            this.reloadMissingFileToolStripMenuItem.Click += new System.EventHandler( this.reloadMissingFileToolStripMenuItem_Click );
            // 
            // locateMissingFileToolStripMenuItem
            // 
            this.locateMissingFileToolStripMenuItem.Name = "locateMissingFileToolStripMenuItem";
            this.locateMissingFileToolStripMenuItem.Size = new System.Drawing.Size( 143, 22 );
            this.locateMissingFileToolStripMenuItem.Text = "&Locate Missing File";
            this.locateMissingFileToolStripMenuItem.Click += new System.EventHandler( this.locateMissingFileToolStripMenuItem_Click );
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size( 140, 6 );
            // 
            // renameToolStripMenuItem
            // 
            this.renameToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
            this.renameToolStripMenuItem.Size = new System.Drawing.Size( 143, 22 );
            this.renameToolStripMenuItem.Text = "Re&name";
            this.renameToolStripMenuItem.Click += new System.EventHandler( this.renameToolStripMenuItem_Click );
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size( 143, 22 );
            this.removeToolStripMenuItem.Text = "&Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler( this.removeToolStripMenuItem_Click );
            // 
            // treeViewImageList
            // 
            this.treeViewImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject( "treeViewImageList.ImageStream" )));
            this.treeViewImageList.TransparentColor = System.Drawing.Color.Magenta;
            this.treeViewImageList.Images.SetKeyName( 0, "projectFolder.gif" );
            this.treeViewImageList.Images.SetKeyName( 1, "projectFile.gif" );
            this.treeViewImageList.Images.SetKeyName( 2, "projectFileMissing.gif" );
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "sc";
            this.openFileDialog.Multiselect = true;
            this.openFileDialog.RestoreDirectory = true;
            this.openFileDialog.Title = "Add Existing Item";
            // 
            // openProjectDialog
            // 
            this.openProjectDialog.DefaultExt = "scproj";
            this.openProjectDialog.Filter = "Script Project Files (*.scproj)|*.scproj|All Files (*.*)|*.*";
            this.openProjectDialog.Title = "Open Project File";
            // 
            // dragDropImageList
            // 
            this.dragDropImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.dragDropImageList.ImageSize = new System.Drawing.Size( 16, 16 );
            this.dragDropImageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // projectFolderBrowserDialog
            // 
            this.projectFolderBrowserDialog.Description = "Find the Directory Where The New Project Will Be Created";
            // 
            // sortButton
            // 
            this.sortButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.sortButton.Location = new System.Drawing.Point( 0, 0 );
            this.sortButton.Name = "sortButton";
            this.sortButton.Size = new System.Drawing.Size( 320, 23 );
            this.sortButton.TabIndex = 1;
            this.sortButton.Text = "Sort";
            this.sortButton.UseVisualStyleBackColor = true;
            this.sortButton.Click += new System.EventHandler( this.sortButton_Click );
            // 
            // ProjectExplorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.sortButton );
            this.Controls.Add( this.treeView );
            this.Name = "ProjectExplorer";
            this.Size = new System.Drawing.Size( 320, 427 );
            this.contextMenuStrip.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.ImageList treeViewImageList;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.OpenFileDialog openProjectDialog;
        private System.Windows.Forms.ImageList dragDropImageList;
        private System.Windows.Forms.FolderBrowserDialog projectFolderBrowserDialog;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compileFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compileAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem addExistingFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rebuildAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reloadMissingFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem locateMissingFileToolStripMenuItem;
        private System.Windows.Forms.Button sortButton;
    }
}
