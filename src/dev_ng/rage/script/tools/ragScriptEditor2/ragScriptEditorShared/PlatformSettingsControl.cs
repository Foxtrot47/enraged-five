﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RSG.Platform;

namespace ragScriptEditorShared
{
    /// <summary>
    /// 
    /// </summary>
    public partial class PlatformSettingsControl : ragScriptEditorShared.SettingsControlBase
    {
        public PlatformSettingsControl()
        {
            InitializeComponent();

            foreach (Platform platform in Enum.GetValues(typeof(Platform)))
            {
                if (platform != Platform.Independent)
                {
                    CheckBox platformCheckbox = new CheckBox();
                    platformCheckbox.Text = platform.PlatformToFriendlyName();
                    platformCheckbox.CheckedChanged += PlatformCheckBox_CheckedChanged;
                    platformCheckbox.Tag = platform;
                    platformCheckbox.AutoSize = true;

                    flowLayoutPanel1.Controls.Add(platformCheckbox);
                }
            }
        }

        
        #region Overrides 
        public override SettingsBase TabSettings
        {
            get
            {
                PlatformSettings settings = new PlatformSettings();
                settings.Platforms.Clear();
                foreach (CheckBox platformCheckbox in flowLayoutPanel1.Controls)
                {
                    if (platformCheckbox.Checked)
                    {
                        Platform platform = (Platform)platformCheckbox.Tag;
                        settings.Platforms.Add(platform);
                    }
                }
                return settings;
            }
            set
            {
                PlatformSettings settings = value as PlatformSettings;
                if (settings != null)
                {
                    foreach (CheckBox platformCheckbox in flowLayoutPanel1.Controls)
                    {
                        Platform platform = (Platform)platformCheckbox.Tag;
                        if (settings.Platforms.Contains(platform))
                        {
                            platformCheckbox.Checked = true;
                        }
                    }
                }
            }
        }

        private void PlatformCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            OnSettingsChanged();
        }
        #endregion // Overrides
    }
}
