namespace ragScriptEditorShared
{
    partial class SearchReplaceDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.findWhatLabel = new System.Windows.Forms.Label();
            this.findNextButton = new System.Windows.Forms.Button();
            this.replaceWithLabel = new System.Windows.Forms.Label();
            this.replaceButton = new System.Windows.Forms.Button();
            this.matchCaseCheckBox = new System.Windows.Forms.CheckBox();
            this.matchWholeWordCheckBox = new System.Windows.Forms.CheckBox();
            this.searchUpCheckBox = new System.Windows.Forms.CheckBox();
            this.useCheckBox = new System.Windows.Forms.CheckBox();
            this.useComboBox = new System.Windows.Forms.ComboBox();
            this.findWhatComboBox = new System.Windows.Forms.ComboBox();
            this.replaceWithComboBox = new System.Windows.Forms.ComboBox();
            this.markWithBookmarksCheckBox = new System.Windows.Forms.CheckBox();
            this.searchGroupBox = new System.Windows.Forms.GroupBox();
            this.entireProjectRadioButton = new System.Windows.Forms.RadioButton();
            this.allOpenDocumentsRadioButton = new System.Windows.Forms.RadioButton();
            this.currentDocumentRadioButton = new System.Windows.Forms.RadioButton();
            this.currentSelectionRadioButton = new System.Windows.Forms.RadioButton();
            this.replaceAllButton = new System.Windows.Forms.Button();
            this.markAllButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.searchReplaceComponent = new ragScriptEditorShared.SearchReplaceComponent( this.components );
            this.searchGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // findWhatLabel
            // 
            this.findWhatLabel.AutoSize = true;
            this.findWhatLabel.Location = new System.Drawing.Point( 18, 15 );
            this.findWhatLabel.Name = "findWhatLabel";
            this.findWhatLabel.Size = new System.Drawing.Size( 56, 13 );
            this.findWhatLabel.TabIndex = 0;
            this.findWhatLabel.Text = "Fi&nd what:";
            // 
            // findNextButton
            // 
            this.findNextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.findNextButton.Location = new System.Drawing.Point( 413, 10 );
            this.findNextButton.Name = "findNextButton";
            this.findNextButton.Size = new System.Drawing.Size( 75, 23 );
            this.findNextButton.TabIndex = 2;
            this.findNextButton.Text = "&Find Next";
            this.findNextButton.UseVisualStyleBackColor = true;
            this.findNextButton.Click += new System.EventHandler( this.findNextButton_Click );
            // 
            // replaceWithLabel
            // 
            this.replaceWithLabel.AutoSize = true;
            this.replaceWithLabel.Location = new System.Drawing.Point( 2, 42 );
            this.replaceWithLabel.Name = "replaceWithLabel";
            this.replaceWithLabel.Size = new System.Drawing.Size( 72, 13 );
            this.replaceWithLabel.TabIndex = 3;
            this.replaceWithLabel.Text = "Re&place with:";
            // 
            // replaceButton
            // 
            this.replaceButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceButton.Location = new System.Drawing.Point( 413, 37 );
            this.replaceButton.Name = "replaceButton";
            this.replaceButton.Size = new System.Drawing.Size( 75, 23 );
            this.replaceButton.TabIndex = 5;
            this.replaceButton.Text = "&Replace";
            this.replaceButton.UseVisualStyleBackColor = true;
            this.replaceButton.Click += new System.EventHandler( this.replaceButton_Click );
            // 
            // matchCaseCheckBox
            // 
            this.matchCaseCheckBox.AutoSize = true;
            this.matchCaseCheckBox.Location = new System.Drawing.Point( 5, 66 );
            this.matchCaseCheckBox.Name = "matchCaseCheckBox";
            this.matchCaseCheckBox.Size = new System.Drawing.Size( 82, 17 );
            this.matchCaseCheckBox.TabIndex = 9;
            this.matchCaseCheckBox.Text = "Match &case";
            this.matchCaseCheckBox.UseVisualStyleBackColor = true;
            // 
            // matchWholeWordCheckBox
            // 
            this.matchWholeWordCheckBox.AutoSize = true;
            this.matchWholeWordCheckBox.Location = new System.Drawing.Point( 5, 89 );
            this.matchWholeWordCheckBox.Name = "matchWholeWordCheckBox";
            this.matchWholeWordCheckBox.Size = new System.Drawing.Size( 113, 17 );
            this.matchWholeWordCheckBox.TabIndex = 10;
            this.matchWholeWordCheckBox.Text = "Match &whole word";
            this.matchWholeWordCheckBox.UseVisualStyleBackColor = true;
            // 
            // searchUpCheckBox
            // 
            this.searchUpCheckBox.AutoSize = true;
            this.searchUpCheckBox.Location = new System.Drawing.Point( 5, 112 );
            this.searchUpCheckBox.Name = "searchUpCheckBox";
            this.searchUpCheckBox.Size = new System.Drawing.Size( 77, 17 );
            this.searchUpCheckBox.TabIndex = 11;
            this.searchUpCheckBox.Text = "S&earch Up";
            this.searchUpCheckBox.UseVisualStyleBackColor = true;
            // 
            // useCheckBox
            // 
            this.useCheckBox.AutoSize = true;
            this.useCheckBox.Location = new System.Drawing.Point( 5, 137 );
            this.useCheckBox.Name = "useCheckBox";
            this.useCheckBox.Size = new System.Drawing.Size( 45, 17 );
            this.useCheckBox.TabIndex = 12;
            this.useCheckBox.Text = "&Use";
            this.useCheckBox.UseVisualStyleBackColor = true;
            this.useCheckBox.CheckedChanged += new System.EventHandler( this.useCheckBox_CheckedChanged );
            // 
            // useComboBox
            // 
            this.useComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.useComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.useComboBox.FormattingEnabled = true;
            this.useComboBox.Items.AddRange( new object[] {
            "Regular expressions",
            "Wildcards"} );
            this.useComboBox.Location = new System.Drawing.Point( 56, 135 );
            this.useComboBox.Name = "useComboBox";
            this.useComboBox.Size = new System.Drawing.Size( 216, 21 );
            this.useComboBox.TabIndex = 13;
            // 
            // findWhatComboBox
            // 
            this.findWhatComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.findWhatComboBox.FormattingEnabled = true;
            this.findWhatComboBox.Location = new System.Drawing.Point( 80, 12 );
            this.findWhatComboBox.Name = "findWhatComboBox";
            this.findWhatComboBox.Size = new System.Drawing.Size( 327, 21 );
            this.findWhatComboBox.TabIndex = 1;
            this.findWhatComboBox.TextUpdate += new System.EventHandler( this.findWhatComboBox_TextUpdate );
            // 
            // replaceWithComboBox
            // 
            this.replaceWithComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceWithComboBox.FormattingEnabled = true;
            this.replaceWithComboBox.Location = new System.Drawing.Point( 80, 39 );
            this.replaceWithComboBox.Name = "replaceWithComboBox";
            this.replaceWithComboBox.Size = new System.Drawing.Size( 327, 21 );
            this.replaceWithComboBox.TabIndex = 4;
            // 
            // markWithBookmarksCheckBox
            // 
            this.markWithBookmarksCheckBox.AutoSize = true;
            this.markWithBookmarksCheckBox.Location = new System.Drawing.Point( 5, 162 );
            this.markWithBookmarksCheckBox.Name = "markWithBookmarksCheckBox";
            this.markWithBookmarksCheckBox.Size = new System.Drawing.Size( 127, 17 );
            this.markWithBookmarksCheckBox.TabIndex = 14;
            this.markWithBookmarksCheckBox.Text = "Mark with &bookmarks";
            this.markWithBookmarksCheckBox.UseVisualStyleBackColor = true;
            // 
            // searchGroupBox
            // 
            this.searchGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.searchGroupBox.Controls.Add( this.entireProjectRadioButton );
            this.searchGroupBox.Controls.Add( this.allOpenDocumentsRadioButton );
            this.searchGroupBox.Controls.Add( this.currentDocumentRadioButton );
            this.searchGroupBox.Controls.Add( this.currentSelectionRadioButton );
            this.searchGroupBox.Location = new System.Drawing.Point( 278, 66 );
            this.searchGroupBox.Name = "searchGroupBox";
            this.searchGroupBox.Size = new System.Drawing.Size( 129, 113 );
            this.searchGroupBox.TabIndex = 15;
            this.searchGroupBox.TabStop = false;
            this.searchGroupBox.Text = "Search";
            // 
            // entireProjectRadioButton
            // 
            this.entireProjectRadioButton.AutoSize = true;
            this.entireProjectRadioButton.Location = new System.Drawing.Point( 6, 88 );
            this.entireProjectRadioButton.Name = "entireProjectRadioButton";
            this.entireProjectRadioButton.Size = new System.Drawing.Size( 87, 17 );
            this.entireProjectRadioButton.TabIndex = 3;
            this.entireProjectRadioButton.TabStop = true;
            this.entireProjectRadioButton.Text = "Entire pro&ject";
            this.entireProjectRadioButton.UseVisualStyleBackColor = true;
            this.entireProjectRadioButton.CheckedChanged += new System.EventHandler( this.entireProjectRadioButton_CheckedChanged );
            // 
            // allOpenDocumentsRadioButton
            // 
            this.allOpenDocumentsRadioButton.AutoSize = true;
            this.allOpenDocumentsRadioButton.Location = new System.Drawing.Point( 6, 65 );
            this.allOpenDocumentsRadioButton.Name = "allOpenDocumentsRadioButton";
            this.allOpenDocumentsRadioButton.Size = new System.Drawing.Size( 118, 17 );
            this.allOpenDocumentsRadioButton.TabIndex = 2;
            this.allOpenDocumentsRadioButton.TabStop = true;
            this.allOpenDocumentsRadioButton.Text = "All &open documents";
            this.allOpenDocumentsRadioButton.UseVisualStyleBackColor = true;
            this.allOpenDocumentsRadioButton.CheckedChanged += new System.EventHandler( this.allOpenDocumentsRadioButton_CheckedChanged );
            // 
            // currentDocumentRadioButton
            // 
            this.currentDocumentRadioButton.AutoSize = true;
            this.currentDocumentRadioButton.Location = new System.Drawing.Point( 6, 42 );
            this.currentDocumentRadioButton.Name = "currentDocumentRadioButton";
            this.currentDocumentRadioButton.Size = new System.Drawing.Size( 109, 17 );
            this.currentDocumentRadioButton.TabIndex = 1;
            this.currentDocumentRadioButton.TabStop = true;
            this.currentDocumentRadioButton.Text = "Current &document";
            this.currentDocumentRadioButton.UseVisualStyleBackColor = true;
            this.currentDocumentRadioButton.CheckedChanged += new System.EventHandler( this.currentDocumentRadioButton_CheckedChanged );
            // 
            // currentSelectionRadioButton
            // 
            this.currentSelectionRadioButton.AutoSize = true;
            this.currentSelectionRadioButton.Location = new System.Drawing.Point( 6, 19 );
            this.currentSelectionRadioButton.Name = "currentSelectionRadioButton";
            this.currentSelectionRadioButton.Size = new System.Drawing.Size( 104, 17 );
            this.currentSelectionRadioButton.TabIndex = 0;
            this.currentSelectionRadioButton.TabStop = true;
            this.currentSelectionRadioButton.Text = "Current &selection";
            this.currentSelectionRadioButton.UseVisualStyleBackColor = true;
            this.currentSelectionRadioButton.CheckedChanged += new System.EventHandler( this.currentSelectionRadioButton_CheckedChanged );
            // 
            // replaceAllButton
            // 
            this.replaceAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceAllButton.Location = new System.Drawing.Point( 413, 66 );
            this.replaceAllButton.Name = "replaceAllButton";
            this.replaceAllButton.Size = new System.Drawing.Size( 75, 23 );
            this.replaceAllButton.TabIndex = 6;
            this.replaceAllButton.Text = "Replace &All";
            this.replaceAllButton.UseVisualStyleBackColor = true;
            this.replaceAllButton.Click += new System.EventHandler( this.replaceAllButton_Click );
            // 
            // markAllButton
            // 
            this.markAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.markAllButton.Location = new System.Drawing.Point( 413, 95 );
            this.markAllButton.Name = "markAllButton";
            this.markAllButton.Size = new System.Drawing.Size( 75, 23 );
            this.markAllButton.TabIndex = 7;
            this.markAllButton.Text = "&Mark All";
            this.markAllButton.UseVisualStyleBackColor = true;
            this.markAllButton.Click += new System.EventHandler( this.markAllButton_Click );
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point( 413, 124 );
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size( 75, 23 );
            this.closeButton.TabIndex = 8;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler( this.closeButton_Click );
            // 
            // searchReplaceComponent
            // 
            this.searchReplaceComponent.Title = "Find/Replace";
            this.searchReplaceComponent.SearchComplete += new ragScriptEditorShared.SearchCompleteEventHandler( this.searchReplaceComponent_SearchComplete );
            // 
            // SearchReplaceDialog
            // 
            this.AcceptButton = this.findNextButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size( 500, 191 );
            this.Controls.Add( this.closeButton );
            this.Controls.Add( this.markAllButton );
            this.Controls.Add( this.replaceAllButton );
            this.Controls.Add( this.searchGroupBox );
            this.Controls.Add( this.markWithBookmarksCheckBox );
            this.Controls.Add( this.replaceWithComboBox );
            this.Controls.Add( this.findWhatComboBox );
            this.Controls.Add( this.useComboBox );
            this.Controls.Add( this.useCheckBox );
            this.Controls.Add( this.searchUpCheckBox );
            this.Controls.Add( this.matchWholeWordCheckBox );
            this.Controls.Add( this.matchCaseCheckBox );
            this.Controls.Add( this.replaceButton );
            this.Controls.Add( this.replaceWithLabel );
            this.Controls.Add( this.findNextButton );
            this.Controls.Add( this.findWhatLabel );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SearchReplaceDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Find/Replace *NEW*";
            this.Activated += new System.EventHandler( this.SearchReplaceDialog_Activated );
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.SearchReplaceDialog_FormClosing );
            this.searchGroupBox.ResumeLayout( false );
            this.searchGroupBox.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label findWhatLabel;
        private System.Windows.Forms.Button findNextButton;
        private System.Windows.Forms.Label replaceWithLabel;
        private System.Windows.Forms.Button replaceButton;
        private System.Windows.Forms.CheckBox matchCaseCheckBox;
        private System.Windows.Forms.CheckBox matchWholeWordCheckBox;
        private System.Windows.Forms.CheckBox searchUpCheckBox;
        private System.Windows.Forms.CheckBox useCheckBox;
        private System.Windows.Forms.ComboBox useComboBox;
        private System.Windows.Forms.ComboBox findWhatComboBox;
        private System.Windows.Forms.ComboBox replaceWithComboBox;
        private System.Windows.Forms.CheckBox markWithBookmarksCheckBox;
        private System.Windows.Forms.GroupBox searchGroupBox;
        private System.Windows.Forms.RadioButton entireProjectRadioButton;
        private System.Windows.Forms.RadioButton allOpenDocumentsRadioButton;
        private System.Windows.Forms.RadioButton currentDocumentRadioButton;
        private System.Windows.Forms.RadioButton currentSelectionRadioButton;
        private System.Windows.Forms.Button replaceAllButton;
        private System.Windows.Forms.Button markAllButton;
        private System.Windows.Forms.Button closeButton;
        private SearchReplaceComponent searchReplaceComponent;
    }
}