using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

using ActiproSoftware.SyntaxEditor;

namespace ragScriptEditorShared
{
    /// <summary>
    /// Interface class for initializing a Script Language Parser Plugin.  There should be one
    /// instance of this for every Language in the Plugin.
    /// </summary>
    public interface IParserPlugin
    {
        /// <summary>
        /// Get:  The name of this language
        /// </summary>
        string LanguageName { get; }

        /// <summary>
        /// Retrieves a list of all of the file extensions for this language
        /// </summary>
        List<string> Extensions { get; }

        /// <summary>
        /// Retrieves a list of all of the file extensions for this language that can be compiled
        /// </summary>
        List<string> CompilableExtensions { get; }

        /// <summary>
        /// Get:  Retrieves the Default Editor Settings that were set or loaded by LoadSettings()
        /// </summary>
        LanguageDefaultEditorSettingsBase DefaultEditorSettings { get; }

        /// <summary>
        /// Gets or sets the file that is implicitly included in all files in this language
        /// </summary>
        string IncludeFile { get; set; }

        /// <summary>
        /// Gets or sets the include path.  When set, should resolve IncludePathFiles.
        /// </summary>
        IEnumerable<string> IncludePaths { get; set; }

        /// <summary>
        /// Gets the files in the IncludePath that are part of this language.  Should be set when
        /// IncludePaths is set.
        /// </summary>
        IEnumerable<string> IncludePathFiles { get; }

        /// <summary>
        /// Get:  Gets the current language settings that are used by this plugin
        /// </summary>
        LanguageSettings CurrentLanguageSettings { get; }

        /// <summary>
        /// Get a list of all Project Resolvers that have been created by this plugin
        /// </summary>
        List<IParserProjectResolver> ProjectResolvers { get; }

        /// <summary>
        /// Gets or sets the custom line from compile settings
        /// </summary>
        string Custom { get; set; }

        /// <summary>
        /// Initialize the plugin.  Could involve calling LoadSettings()
        /// </summary>
        /// <param name="directory"></param>
        void InitPlugin( string directory );

        /// <summary>
        /// Shutdown the plugin and dispose of any unmanaged objects
        /// </summary>
        void ShutdownPlugin();

        /// <summary>
        /// Loads the Editor Settings file for this language into DefaultEditorSettings
        /// </summary>
        /// <returns>True on success</returns>
        bool LoadSettings();

        /// <summary>
        /// Saves DefaultEditorSettings to the Editor Settings file.  The Script Editor will handle
        /// copying CurrentLanguageSettings to DefaultEditorSettings.Language before calling 
        /// this function.
        /// </summary>
        /// <returns>True on success</returns>
        bool SaveSettings();

        /// <summary>
        /// Creates a new Project Resolver.  One of these will be created
        /// for an entire project.
        /// </summary>
        /// <param name="projectFiles"></param>
        /// <returns></returns>
        IParserProjectResolver CreateProjectResolver( List<string> projectFiles );

        /// <summary>
        /// Creates a new Project Resolver.  One of these will be created
        /// for each file that is not part of a project
        /// </summary>
        /// <param name="includeFile"></param>
        /// <param name="includePaths"></param>
        /// <returns></returns>
        IParserProjectResolver CreateProjectResolver( string filename );

        /// <summary>
        /// Removes the projectResolver from the list returned by ProjectResolvers and calls Dispose() if necessary
        /// </summary>
        /// <param name="projectResolver"></param>
        void DestroyProjectResolver( IParserProjectResolver projectResolver );

        /// <summary>
        /// Called by the main form when the active syntax editor is changed.
        /// </summary>
        /// <param name="editor"></param>
        /// <param name="treeView"></param>
        /// <returns></returns>
        bool UpdateDocumentOutline( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, TreeView treeView );

        /// <summary>
        /// Called by the main form when the <see cref="LanguageSettings"/> are updated.
        /// </summary>
        void OnLanguageSettingsUpdated();
    }
}
