//#define WIP
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using ActiproSoftware.SyntaxEditor;
using RSG.Base;
using RSG.Base.Command;
using RSG.Base.Extensions;
using RSG.Base.Forms;
using RSG.Base.Logging;
using RSG.Base.Forms.SCM;
using RSG.Configuration;
using RSG.Platform;

namespace ragScriptEditorShared
{
    public struct ApplicationSettings
    {
        #region Enums
        public enum ApplicationMode
        {
            Editor,
            Debugger,
            Compiler,
            HelpParser
        }

        public enum ColorFormat
        {
            NamedColor,
            ARGBColor
        }
        #endregion

        #region Static Properties

        /// <summary>
        /// Static constructor that ensures that m_config is initialized on first use. Gets
        /// around optimized code that may call this  before LogFactory is initialized.
        /// </summary>
        static ApplicationSettings()
        {
            ToolsConfig toolsConfig = new ToolsConfig();
            LogFactory.Initialize();

            m_commandOptions = new CommandOptions(new String[0], new RSG.Base.OS.LongOption[0]);
            m_config = m_commandOptions.Config;
        }

        private static readonly char[] InvalidPathChars = Path.GetInvalidFileNameChars()
            .Except(new[]
            {
                Path.DirectorySeparatorChar,
                Path.AltDirectorySeparatorChar,
                Path.VolumeSeparatorChar
            })
            .ToArray();

        private static IConfig m_config;
        private static IEnvironment m_environment = null;
        private static String m_scriptProjectDir = null;
        private static CommandOptions m_commandOptions = null;
        public static bool AdminMode = false;

        public static string ProductName = null;
        public static string ExecutableName = null;
		public static string ExecutablePath = null;

        /// <summary>
        /// Environment, potentially taken from tools config.
        /// </summary>
        public static IEnvironment Environment
        {
            get { return m_environment; }
        }

        public static CommandOptions CommandOptions
        {
            get { return m_commandOptions; }
        }

        public static string ExecutableFullPathName
        {
            get
            {
                return Path.Combine( ExecutablePath, ExecutableName );
            }
            set
            {
                ExecutableName = Path.GetFileName( value );
                ExecutablePath = Path.GetDirectoryName( value );
            }
        }

        public static ILog Log
        {
            get
            {
                return LogFactory.ApplicationLog;
            }
        }

        public const string OldLayoutVersionString = "Version 1.3";

        public static ApplicationMode Mode = ApplicationMode.Editor;

        public const string OldLastProjectVersionString = "Version 1.0";
        public const string OldProjectVersionString = "Version 1.1";

        public static bool ExitingAfterException = false;
        public static string LastProjectSettingsFile = null;
        #endregion

        #region Public Static Functions
        public static void SetBranchFrom(string filename)
        {
            m_environment = ConfigFactory.CreateEnvironment();
            filename = Path.GetFullPath(filename);
            m_scriptProjectDir = Path.GetDirectoryName(filename);

            // Order by descending order--because the branch with the longest path should be matched first.
            // Example:
            //   x:/project/script/dev_live/some/file.scproj would match both 'x:/project/script/dev' and 'x:/project/script/dev_live'
            //   but we want it to match dev_live first.
            foreach (IBranch branch in m_config.Project.Branches.Values.OrderByDescending(b => b.Script.Length))
            {
                String scriptDir = Path.GetFullPath(branch.Script) + Path.DirectorySeparatorChar;
                if (filename.StartsWith(scriptDir, StringComparison.OrdinalIgnoreCase))
                {
                    branch.Import(m_environment);
                    m_scriptProjectDir = Path.GetDirectoryName(filename);
                    return;
                }
            }

            // Fallback to installed branch (this is a last resort)
            m_commandOptions.Branch.Import(m_environment);

            string installedBranchNames = String.Join("\r\n", m_config.Project.Branches.Values.Select(b => $"\t{b.Name}"));
            // At this point we haven't found a matching branch, so inform the user
            rageMessageBox.ShowWarning($"Unable to locate a tools branch for the project '{filename}'.\r\nYour tools installation ({m_commandOptions.Branch.Name}) may not define this game branch.\r\n\r\nAvailable branches:\r\n{installedBranchNames}\r\n\r\nYou should install tools to the appropriate branch to avoid issues loading files.", "Unable to find project branch!");

            m_environment.Add("script", Path.GetDirectoryName(filename));
            m_scriptProjectDir = Path.GetDirectoryName(filename);
        }

        /// <summary>
        /// If the provided filename is not rooted, add the expanded project root (X:/project/scripts for example) to the front.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static string PrependProjectRoot(string path)
        {
            if (path.StartsWith("<new file", StringComparison.OrdinalIgnoreCase) ||
                !IsPathValid(path))
            {
                return path;
            }

            if (Path.IsPathRooted(path))
            {
                return Path.GetFullPath(path);
            }

            if (m_scriptProjectDir == null)
            {
                return path;
            }

            return Path.GetFullPath(Path.Combine(m_scriptProjectDir, path));
        }

        public static string OldUserPrefDir( out string error )
        {
            try
            {
                String homeDrive = System.Environment.GetEnvironmentVariable( "HOMEDRIVE" );
                String homePath = System.Environment.GetEnvironmentVariable( "HOMEPATH" );
                String fullPath = homeDrive + homePath;
                if ( !Directory.Exists( fullPath ) )
                {
                    error = "Your home path " + fullPath + " doesn't exist. Defaulting to c:\\. ";
                    return @"c:\";
                }
                else
                {
                    error = null;
                    return fullPath;
                }
            }
            catch ( SystemException e )
            {
                error = "Couldn't find the path to your home directory:\n" + e.Message + "\nDefaulting to c:\\. ";
                return @"c:\";
            }
        }

        public static string UserPrefDir( out string error )
        {
            try
            {
                string defaultPrefDir = Path.Combine( System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments), "ragScriptEditor" );

                error = null;
                return defaultPrefDir;
            }
            catch (System.Exception e)
            {
                error = "There was an error determining the user pref directory:\n" + e.Message + "\nDefaulting to c:\\. ";
                return @"c:\";
            }
        }

        public static List<string> GetCommonSettingsFilenames( bool okIfDoesNotExist )
        {
            List<string> filenames = new List<string>();

            string error;
            string userprefdir = ApplicationSettings.UserPrefDir( out error );
            string olduserprefdir = ApplicationSettings.OldUserPrefDir( out error );

            // build a list of all filenames we should look for
            List<string> strVersions = new List<string>();
            strVersions.Add( UserEditorSettings.SettingsVersion.ToString().Replace( '.', '_' ) );
            strVersions.Add( "2" );
            strVersions.Add( string.Empty );

            foreach ( string strVersion in strVersions )
            {
                // look for the latest version of the settings file that we have
                string filename = GetCommonSettingsFilename( userprefdir, strVersion );
                if ( File.Exists( filename ) || okIfDoesNotExist )
                {
                    filenames.Add( filename );
                }

                // look for the latest version of the settings file that we have
                filename = GetCommonSettingsFilename( olduserprefdir, strVersion );
                if ( File.Exists( filename ) || okIfDoesNotExist )
                {
                    filenames.Add( filename );
                }
            }

            return filenames;
        }

        /// <summary>
        /// Returns the name and path of the shared settings file using the OldUserPrefDir.  Depending on the value of
        /// 'okIfDoesNotExist', the filename returned could be for the new or old layout file.
        /// </summary>
        /// <param name="okIfDoesNotExist"><c>true</c> if the file must exist in order to be returned, <c>false</c> otherwise.</param>
        /// <returns><c>null</c> if file doesn't exist and 'okIfDoesNotExist' was <c>false</c>, otherwise the filename.</returns>
        public static string GetCommonSettingsFilename( bool okIfDoesNotExist )
        {
            List<string> filenames = GetCommonSettingsFilenames( okIfDoesNotExist );
            if ( filenames.Count > 0 )
            {
                return filenames[0];
            }

            return null;
        }

        [DllImport( "kernel32.dll", SetLastError = true )]
        public static extern bool GetProcessAffinityMask( IntPtr hProcess, out UIntPtr lpProcessAffinityMask, out UIntPtr lpSystemAffinityMask );

        [DllImport( "kernel32.dll", SetLastError = true )]
        public static extern bool SetProcessAffinityMask( IntPtr hProcess, UIntPtr dwProcessAffinityMask );

        public static void CenterLocation( Control parent, Form form )
        {
            Rectangle rect = parent.Bounds;
            form.StartPosition = FormStartPosition.Manual;

            int x = (rect.Width / 2) + rect.Left - (form.Width / 2);
            int y = (rect.Height / 2) + rect.Top - (form.Height / 2);

            form.Location = new Point( x, y );
        }

        /// <summary>
        /// Returns the relative path of an absolute path.
        /// </summary>
        /// <param name="path">The path to determine the relative path for.</param>
        /// <returns></returns>
        public static string GetRelativePath(string path)
        {
            if (String.IsNullOrEmpty(path))
            {
                return path;
            }

            return m_environment?.ReverseSubst(path) ?? path;
        }

        /// <summary>
        /// Expand the absolute path given a relative path.
        /// </summary>
        /// <param name="path">The relative path</param>
        /// <returns></returns>
        public static string GetAbsolutePath(string path)
        {
            if (String.IsNullOrEmpty(path))
            {
                return path;
            }

            // Some paths may be missing the branch root--add it if the path isn't rooted after substitution.
            return PrependProjectRoot(m_environment?.Subst(path) ?? path);
        }

        public static string SerializeColor( Color color )
        {
            if ( color.IsNamedColor )
            {
                return string.Format( "{0}:{1}", ColorFormat.NamedColor, color.Name );
            }
            else
            {
                return string.Format( "{0}:{1}:{2}:{3}:{4}", ColorFormat.ARGBColor, color.A, color.R, color.G, color.B );
            }
        }

        public static Color DeserializeColor( string color )
        {
            byte a, r, g, b;

            string[] pieces = color.Split( new char[] { ':' } );

            ColorFormat colorType = (ColorFormat)Enum.Parse( typeof( ColorFormat ), pieces[0], true );

            switch ( colorType )
            {
                case ColorFormat.NamedColor:
                    return Color.FromName( pieces[1] );

                case ColorFormat.ARGBColor:
                    a = byte.Parse( pieces[1] );
                    r = byte.Parse( pieces[2] );
                    g = byte.Parse( pieces[3] );
                    b = byte.Parse( pieces[4] );

                    if ( a == 0 && r == 0 && g == 0 && b == 0 )
                    {
                        return Color.Empty;
                    }
                    else
                    {
                        return Color.FromArgb( a, r, g, b );
                    }
            }

            return Color.Empty;
        }

        public static string ConcatenatePathsString( IEnumerable<string> pathStrings, bool substPaths )
        {
            StringBuilder pathBuilder = new StringBuilder();

            if ( pathStrings == null )
            {
                return string.Empty;
            }

            foreach ( string path in pathStrings )
            {
                String translatedPath = substPaths ? ApplicationSettings.GetAbsolutePath(path) : path;
                pathBuilder.Append(translatedPath);
                pathBuilder.Append( Path.PathSeparator );
            }

            string pathString = pathBuilder.ToString();
            return pathString.Trim( new char[] { Path.PathSeparator } );
        }

        public static List<string> SplitPathsString( string pathString, bool reverseSubstPaths )
        {
            if ( pathString == null )
            {
                return null;
            }

            pathString = pathString.Replace( "/", "\\" );

            List<string> results = new List<string>();

            // only add valid paths:
            string[] paths = pathString.Split( new char[] { Path.PathSeparator } );
            foreach ( string path in paths )
            {
                string p = path.Trim();
                if ( p != string.Empty )
                {
                    results.Add( reverseSubstPaths && IsPathValid(p) ? ApplicationSettings.Environment.ReverseSubst(p) : p );
                }
            }

            return results;
        }

        /// <summary>
        /// If path contains a space, places quotation marks around path and returns the result.  Otherwise, just returns path.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string BuildPathForCommandLine( string path )
        {
            path = path.Trim();
            if ( (path.IndexOf( "\"" ) == -1) && ((path.IndexOf( " " ) > -1) || (path.IndexOf( "%" ) > -1)) )
            {
                System.Text.StringBuilder quotedPath = new System.Text.StringBuilder( "\"" );
                quotedPath.Append( path );
                quotedPath.Append( "\"" );
                return quotedPath.ToString();
            }

            return path;
        }

        /// <summary>
        /// Effectively changes the path of fullPath to use the outputDirectory.
        /// </summary>
        /// <param name="outputDirectory"></param>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public static string BuildFilenameInOutputDirectory( string outputDirectory, string fullPath)
        {
            bool isFileArg = fullPath.IndexOf( "%" ) > -1;
            System.Text.StringBuilder outputFilename = new System.Text.StringBuilder();

            if (!String.IsNullOrWhiteSpace(outputDirectory) && IsPathValid(outputDirectory))
            {
                // we've set an output directory
                if ( Path.IsPathRooted( outputDirectory ) )
                {
                    // absolute path for output files
                    outputFilename.Append( outputDirectory );
                }
                else
                {
                    // file-relative path for output file
                    if (isFileArg || !Path.IsPathRooted(fullPath))
                    {
                        outputFilename.Append("%~dp1");
                    }
                    else
                    {
                        outputFilename.Append(Path.GetDirectoryName(fullPath));
                    }

                    // the final outputDirectory is a subfolder of what we built above
                    outputFilename.Append( @"\" );
                    outputFilename.Append( outputDirectory );
                }
            }
            else
            {
                // no output directory, so use file-relative path for the output file
                if ( isFileArg || !Path.IsPathRooted( fullPath ) )
                {
                    outputFilename.Append( "%~dp1" );

                    if ( fullPath.StartsWith( "%1" ) )
                    {
                        fullPath = "%~n1" + fullPath.Substring( 2 );
                    }
                }
                else
                {
                    outputFilename.Append( Path.GetDirectoryName( fullPath ) );
                }
            }

            // add the filename
            outputFilename.Append( @"\" );
            if ( isFileArg )
            {
                outputFilename.Append( fullPath );
            }
            else
            {
                outputFilename.Append( Path.GetFileName( fullPath ) );
            }

            return outputFilename.ToString();
        }

        /// <summary>
        /// Builds the file name from a partial path name so that it resides in the outputDirectory or projectFile path.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="outputDirectory"></param>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public static string BuildAndDereferenceFilenameInOutputDirectory(string name, string outputDirectory, string fullPath)
        {
            string filename = name.Trim();
            if ( filename.Length <= 0 || !IsPathValid(filename) )
            {
                return string.Empty;
            }

            if ( !Path.IsPathRooted( filename ) )
            {
                filename = ApplicationSettings.BuildFilenameInOutputDirectory( outputDirectory, filename );
            }

            return ApplicationSettings.DereferenceFileArguments( filename, fullPath );
        }

        /// <summary>
        /// Searches for all pairs of '%' and '1' (indicating a file argument), and determines the actual value
        /// using fullPath
        /// </summary>
        /// <param name="arg"></param>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public static string DereferenceFileArguments( string arg, string fullPath )
        {
            if ( fullPath.IndexOf( "%" ) > -1 )
            {
                return arg;
            }

            int indexOfPercent = arg.IndexOf( "%" );
            while ( indexOfPercent > -1 )
            {
                int indexOfOne = arg.IndexOf( "1", indexOfPercent );
                if ( indexOfOne > -1 )
                {
                    string argRef = arg.Substring( indexOfPercent, indexOfOne - indexOfPercent + 1 );
                    if ( argRef.Length > 0 )
                    {
                        string deref = DereferenceFileArgument( argRef, fullPath );
                        arg = arg.Replace( argRef, deref );
                    }
                }

                indexOfPercent = arg.IndexOf( "%", indexOfPercent + 1 );
            }

            return arg;
        }

        /// <summary>
        /// Determines the value of supported file arguments
        /// </summary>
        /// <param name="reference"></param>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public static string DereferenceFileArgument( string reference, string fullPath )
        {
            switch ( reference )
            {
                case "%1":
                    return fullPath;
                case "%~1":
                    // remove quotes
                    return fullPath.Replace( "\"", "" );
                case "%~f1":
                    // expands %1 to a fully qualified path name
                    return Path.GetFullPath( fullPath );
                case "%~d1":
                    // expands %1 to a drive letter only
                    return Directory.GetDirectoryRoot( fullPath );
                case "%~p1":
                    // expands %1 to a path only
                    string path = Path.GetDirectoryName( fullPath );
                    int indexOf = path.IndexOf( ":" );
                    if ( indexOf > -1 )
                    {
                        // remove drive letter
                        return path.Substring( indexOf + 1 );
                    }
                    return @"\";
                case "%~n1":
                    // expands %1 to a file name only
                    return Path.GetFileNameWithoutExtension( fullPath );
                case "%~x1":
                    // expands %1 to a file extension only
                    return Path.GetExtension( fullPath );
                case "%~s1":
                    // expanded path contains short names only
                    return string.Empty;
                case "%~a1":
                    // expands %1 to file attributes
                    if ( File.Exists( fullPath ) )
                    {
                        return File.GetAttributes( fullPath ).ToString();
                    }
                    return string.Empty;
                case "%~t1":
                    // expands %1 to date/time of file
                    if ( File.Exists( fullPath ) )
                    {
                        return File.GetLastWriteTime( fullPath ).ToString();
                    }
                    return string.Empty;
                case "%~z1":
                    // expands %1 to size of file
                    if ( File.Exists( fullPath ) )
                    {
                        System.IO.FileStream stream = File.Open( fullPath, System.IO.FileMode.Open );
                        string rtn = stream.Length.ToString();
                        stream.Close();
                        return rtn;
                    }
                    return string.Empty;
                case "%~$PATH:1":
                    // searches the directories listed in the PATH environment variable and expands %1 to the fully
                    // qualified name of the first one found. If the environment variable name is not defined or the
                    // file is not found by the search, then this modifier expands to the empty string
                    return string.Empty;
                case "%~dp1":
                    // expands %1 to a drive letter and path only
                    return Path.GetDirectoryName( fullPath );
                case "%~nx1":
                    // expands %1 to a file name and extension only
                    return Path.GetFileName( fullPath );
                case "%~dp$PATH:1":
                    // searches the directories listed in the PATH environment variable for %1 and expands to the
                    // drive letter and path of the first one found.
                    return string.Empty;
                case "%~ftza1":
                    // expands %1 to a DIR-like output line
                    System.Text.StringBuilder dirLikeOutput = new System.Text.StringBuilder();
                    dirLikeOutput.Append( File.GetLastWriteTime( fullPath ).ToString() );
                    dirLikeOutput.Append( " " );
                    dirLikeOutput.Append( File.GetLastWriteTime( fullPath ).ToString() );
                    dirLikeOutput.Append( " " );

                    if ( File.Exists( fullPath ) )
                    {
                        System.IO.FileStream stream = File.Open( fullPath, System.IO.FileMode.Open );
                        dirLikeOutput.Append( stream.Length.ToString() );
                        dirLikeOutput.Append( " " );
                        stream.Close();
                    }

                    dirLikeOutput.Append( Path.GetFullPath( fullPath ) );
                    return dirLikeOutput.ToString();
                default:
                    break;
            }

            return fullPath;
        }

        public static ProcessStartInfo BuildProcessInfoFromCommand( string cmd )
        {
            ProcessStartInfo psInfo = new ProcessStartInfo();

            string program = cmd.Trim();
            string args = null;

            int firstQuote = program.IndexOf( "\"" );
            int firstSpace = program.IndexOf( " " );

            if ( firstQuote == 0 )
            {
                int secondQuote = program.IndexOf( "\"", 1 );
                if ( secondQuote == -1 )
                {
                    return null;
                }

                if ( secondQuote + 1 < program.Length )
                {
                    args = program.Substring( secondQuote + 1 ).Trim();
                    if ( args.Length == 0 )
                    {
                        args = null;
                    }
                }

                program = program.Substring( 1, secondQuote - 1 );
            }
            else if ( firstSpace > -1 )
            {
                args = program.Substring( firstSpace ).Trim();
                if ( args.Length == 0 )
                {
                    args = null;
                }

                program = program.Substring( 0, firstSpace );
            }

            psInfo.FileName = Path.GetFullPath( Path.Combine( ApplicationSettings.ExecutablePath, program ) );
            if ( !File.Exists( psInfo.FileName ) )
            {
                psInfo.FileName = program;
            }

            psInfo.Arguments = args;

            return psInfo;
        }

        public static DialogResult ShowMessage( Control parent, string message, string title,
            MessageBoxButtons buttons, MessageBoxIcon icon, DialogResult defaultResult )
        {
            if ( (ApplicationSettings.Mode == ApplicationMode.Compiler)
                || (ApplicationSettings.Mode == ApplicationMode.HelpParser) )
            {
                Log.Message( "{0}: {1} - {2}", icon.ToString(), title, message );
                return defaultResult;
            }
            else
            {
                switch ( icon )
                {
                    case MessageBoxIcon.Asterisk:
                    //case MessageBoxIcon.Information:
                        return rageMessageBox.ShowInformation( parent, message, title, buttons );

                    case MessageBoxIcon.Error:
                    //case MessageBoxIcon.Hand:
                    //case MessageBoxIcon.Stop:
                        return rageMessageBox.ShowError( parent, message, title, buttons );

                    //case MessageBoxIcon.Exclamation:
                    case MessageBoxIcon.Warning:
                        return rageMessageBox.ShowWarning( parent, message, title, buttons );

                    case MessageBoxIcon.Question:
                        return rageMessageBox.ShowQuestion( parent, message, title, buttons );

                    //case MessageBoxIcon.None:
                    default:
                        return rageMessageBox.ShowNone( parent, message, title, buttons );
                }
            }
        }

        /// <summary>
        /// Returns true if the path contains valid characters (using <see cref="Path.GetInvalidFileNameChars"/>).
        /// </summary>
        /// <param name="path">The path to validate</param>
        /// <returns>Valid status</returns>
        public static bool IsPathValid(string path)
        {
            if (String.IsNullOrWhiteSpace(path))
            {
                return false;
            }

            // This list unfortunately isn't enough to determine if the path will be valid
            // but it can at least early out to avoid unnecessary exception throwing.
            if (InvalidPathChars.Any(path.Contains))
            {
                return false;
            }

            try
            {
                // Ensure getfullpath doesn't throw.
                Path.GetFullPath(path);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Private Functions
        private static string GetCommonSettingsFilename( string userprefdir, string strVersion )
        {
#if WIP
            string filename = filename = Path.Combine( userprefdir, ApplicationSettings.ProductName + strVersion + "WIP.xml" );
            if ( !File.Exists( filename ) )
            {
                filename = Path.Combine( userprefdir, ApplicationSettings.ProductName + strVersion + ".xml" );
            }
#else
            string filename = Path.Combine( userprefdir, ApplicationSettings.ProductName + strVersion + ".xml" );
#endif

            return filename;
        }

        #endregion
    }

    #region Tab Settings Control
    public class SettingsControlBase : UserControl
    {
        #region Protected Variables
        /// <summary>
        /// Use this to control when the SettingsChanged event gets invoked.
        /// </summary>
        protected bool m_invokeSettingsChanged = true;
        #endregion

        #region Virtual Properties
        /// <summary>
        /// Get: Should create an instance of the appropriate TabSettingsBase class and fill it from the UI.
        /// Set: Should fill the UI from the TabSettingsBase instance.
        /// </summary>
        public virtual SettingsBase TabSettings
        {
            get
            {
                //throw (new NotImplementedException( "TabSettings.get() has not been implemented." ));
                return null;
            }
            set
            {
                //throw (new NotImplementedException( "TabSettings.set() has not been implemented." ));
            }
        }
        #endregion

        #region Virtual Functions
        public virtual bool ValidateSettings( out string error )
        {
            SettingsBase settings = this.TabSettings;
            if ( settings != null )
            {
                return settings.Validate( out error );
            }
            else
            {
                error = null;
                return true;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// The EditorSettingsForm will register with this event handler so that it can be notified when the Apply button should be enabled.
        /// </summary>
        public event EventHandler SettingsChanged;
        #endregion

        #region Protected Functions
        /// <summary>
        /// Invokes the SettingsChanged event if m_invokeSettingsChanged is <c>true</c>.
        /// </summary>
        protected void OnSettingsChanged()
        {
            if ( m_invokeSettingsChanged && (this.SettingsChanged != null) )
            {
                this.SettingsChanged( this, EventArgs.Empty );
            }
        }
        #endregion
    }

    public class BooleanValueChangedEventArgs : EventArgs
    {
        public BooleanValueChangedEventArgs( bool newValue )
        {
            m_newValue = newValue;
        }

        #region Variables
        private bool m_newValue;
        #endregion

        #region Properties
        public bool NewValue
        {
            get
            {
                return m_newValue;
            }
        }
        #endregion
    }

    public delegate void BooleanValueChangedEventHandler( object sender, BooleanValueChangedEventArgs e );
    #endregion

    #region Tab Settings
    /// <summary>
    /// Kept for backwards compatibility.
    /// </summary>
    public abstract class TabSettingsBase
    {
        protected TabSettingsBase( string name )
        {
            m_name = name;

            Reset();
        }

        #region Variables
        protected string m_name = string.Empty;
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return m_name;
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            TabSettingsBase t = obj as TabSettingsBase;
            if ( (System.Object)t == null )
            {
                return false;
            }

            return m_name.Equals( t.Name );
        }

        public override int GetHashCode()
        {
            return m_name.GetHashCode();
        }

        public override string ToString()
        {
            return m_name;
        }
        #endregion

        #region Public Functions
        public bool Equals( TabSettingsBase t )
        {
            if ( (object)t == null )
            {
                return false;
            }

            return m_name.Equals( t.Name )
                && base.Equals( t );
        }
        #endregion

        #region Abstract Functions
        public abstract void Copy( TabSettingsBase t );

        public abstract TabSettingsBase Clone();

        public abstract void Reset();
        #endregion

        #region Virtual Functions
        public virtual bool Validate( out string error )
        {
            error = null;
            return true;
        }
        #endregion
    }

    public abstract class SettingsBase
    {
        protected SettingsBase( string name )
        {
            m_name = name;

            Reset();
        }

        #region Variables
        protected string m_name = string.Empty;
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return m_name;
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            SettingsBase s = obj as SettingsBase;
            if ( (System.Object)s == null )
            {
                return false;
            }

            return m_name.Equals( s.Name );
        }

        public override int GetHashCode()
        {
            return m_name.GetHashCode();
        }

        public override string ToString()
        {
            return m_name;
        }
        #endregion

        #region Public Functions
        public bool Equals( SettingsBase s )
        {
            if ( (object)s == null )
            {
                return false;
            }

            return m_name.Equals( s.Name );
        }
        #endregion

        #region Abstract Functions
        public abstract void Copy( SettingsBase t );

        public abstract SettingsBase Clone();

        public abstract SettingsControlBase CreateSettingsControl();

        public abstract void Reset();
        #endregion

        #region Virtual Functions
        public virtual bool Validate( out string error )
        {
            error = null;
            return true;
        }
        #endregion
    }

    #region Project Settings
    /// <summary>
    /// Kept for backwards compatibility
    /// </summary>
    public class ProjectTabSettings : TabSettingsBase
    {
        public ProjectTabSettings()
            : this( false )
        {

        }

        public ProjectTabSettings( bool projectSpecific )
            : base( "Projects" )
		{
			this.ProjectSpecific = projectSpecific;
        }

        #region Variables
        private bool m_projectSpecific;
        private bool m_reloadLastProject;
        private bool m_closeProjectFiles;
        private bool m_parsingServiceEnabled;
        private bool m_automaticSorting;
        private string m_lastProject;
        private List<string> m_recentProjects = new List<string>();
        private string m_postCompileCommand;
        private string m_startDebugCommand;
        #endregion

        #region Properties
        public bool ProjectSpecific
        {
            get
            {
                return m_projectSpecific;
            }
            set
            {
                m_projectSpecific = value;
            }
        }

        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        [XmlIgnore]
        public bool ReloadLastProject
        {
            get
            {
                return m_reloadLastProject;
            }
            set
            {
                m_reloadLastProject = value;
            }
        }

        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        [XmlIgnore]
        public bool CloseProjectFiles
        {
            get
            {
                return m_closeProjectFiles;
            }
            set
            {
                m_closeProjectFiles = value;
            }
        }

        public bool ParsingServiceEnabled
        {
            get
            {
                return m_parsingServiceEnabled;
            }
            set
            {
                m_parsingServiceEnabled = value;
            }
        }

        public bool AutomaticSorting
        {
            get
            {
                return m_automaticSorting;
            }
            set
            {
                m_automaticSorting = value;
            }
        }

        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        [XmlIgnore]
        public string LastProject
        {
            get
            {
                return m_lastProject;
            }
            set
            {
                m_lastProject = value;
            }
        }

        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        [XmlIgnore]
        public List<string> RecentProjects
        {
            get
            {
                return m_recentProjects;
            }
        }

        public string PostCompileCommand
        {
            get
            {
                return m_postCompileCommand;
            }
            set
            {
                m_postCompileCommand = value;
            }
        }

        public string StartDebugCommand
        {
            get
            {
                return m_startDebugCommand;
            }
            set
            {
                m_startDebugCommand = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( System.Object obj )
        {
            // If parameter cannot be cast to Point return false.
            ProjectTabSettings p = obj as ProjectTabSettings;
            if ( (System.Object)p == null )
            {
                return false;
            }

            bool apples2apples = this.ProjectSpecific == p.ProjectSpecific;

            // Return true if the fields match:
            return (!apples2apples || (ParsingServiceEnabled == p.ParsingServiceEnabled))
                && (this.AutomaticSorting == p.AutomaticSorting)
                && (this.PostCompileCommand == p.PostCompileCommand)
                && (this.StartDebugCommand == p.StartDebugCommand)
                && base.Equals( p );
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.ProjectSpecific.ToString() );
            s.Append( this.ParsingServiceEnabled.ToString() );
            s.Append( this.AutomaticSorting.ToString() );
            s.Append( this.PostCompileCommand );
            s.Append( this.StartDebugCommand );
            s.Append( this.Name );
            return s.ToString().GetHashCode();
        }

        public override void Copy( TabSettingsBase t )
        {
            if ( t is ProjectTabSettings )
            {
                Copy( t as ProjectTabSettings );
            }
        }

        public override TabSettingsBase Clone()
        {
            ProjectTabSettings p = new ProjectTabSettings( this.ProjectSpecific );
            p.Copy( this );

            return p;
        }

        public override void Reset()
        {
            this.ReloadLastProject = true;
            this.CloseProjectFiles = true;
            this.AutomaticSorting = false;
            this.LastProject = "";
            this.PostCompileCommand = "";
            this.StartDebugCommand = "";

            IntPtr handle = (IntPtr)Process.GetCurrentProcess().Handle;
            UIntPtr ProcAffinityMask = UIntPtr.Zero;
            UIntPtr SysAffinityMask = UIntPtr.Zero;

            if ( ApplicationSettings.GetProcessAffinityMask( handle, out ProcAffinityMask, out SysAffinityMask ) && ((uint)ProcAffinityMask != 1) )
            {
                // start the parsing service
                this.ParsingServiceEnabled = true;
            }
            else
            {
                this.ParsingServiceEnabled = false;
            }
        }
        #endregion

        #region Public Functions
        public void Copy( ProjectTabSettings p )
        {
            if ( p == null )
            {
                return;
            }

            if ( !this.ProjectSpecific && !p.ProjectSpecific )
            {
                this.ParsingServiceEnabled = p.ParsingServiceEnabled;
            }

            this.AutomaticSorting = p.AutomaticSorting;
            this.PostCompileCommand = p.PostCompileCommand;
            this.StartDebugCommand = p.StartDebugCommand;
        }

        public bool Equals( ProjectTabSettings p )
        {
            // If parameter is null return false:
            if ( (object)p == null )
            {
                return false;
            }

            bool apples2apples = this.ProjectSpecific == p.ProjectSpecific;

            // Return true if the fields match:
            return (!apples2apples || (this.ParsingServiceEnabled == p.ParsingServiceEnabled))
                && (this.AutomaticSorting == p.AutomaticSorting)
                && (this.PostCompileCommand == p.PostCompileCommand)
                && (this.StartDebugCommand == p.StartDebugCommand)
                && base.Equals( p );
        }

        /// <summary>
        /// Reads project-specific items from the XmlTextReader.  Kept for backwards compatibility.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public bool LoadXml( XmlTextReader reader )
        {
            string parentName = reader.Name;

            Reset();

            string reloadLastProj = reader["ReloadLastProject"];
            if ( reloadLastProj != null )
            {
                ReloadLastProject = Convert.ToBoolean( reloadLastProj );
            }

            string closeProjFiles = reader["CloseProjectFiles"];
            if ( closeProjFiles != null )
            {
                CloseProjectFiles = Convert.ToBoolean( closeProjFiles );
            }

            string parsingServiceEnabled = reader["ParsingServiceEnabled"];
            if ( parsingServiceEnabled != null )
            {
                ParsingServiceEnabled = Convert.ToBoolean( parsingServiceEnabled );
            }

            string autoSort = reader["AutomaticSorting"];
            if ( autoSort != null )
            {
                AutomaticSorting = Convert.ToBoolean( autoSort );
            }

            string lastProj = reader["LastProject"];
            if ( lastProj != null )
            {
                LastProject = lastProj;
            }

            while ( reader.Read() )
            {
                if ( !reader.IsStartElement() || reader.IsEmptyElement )
                {
                    if ( reader.Name == parentName )
                    {
                        break;	// we're done
                    }
                }
                else if ( reader.Name == "PostCompileCommand" )
                {
                    PostCompileCommand = reader.ReadString();
                }
                else if ( (reader.Name == "StartDebugCommand") || (reader.Name == "RunCommand") )
                {
                    StartDebugCommand = reader.ReadString();
                }
                else if ( reader.Name == "RecentProjects" )
                {
                    RecentProjects.Clear();

                    if ( !reader.IsEmptyElement )
                    {
                        while ( reader.Read() )
                        {
                            if ( !reader.IsStartElement() )
                            {
                                if ( reader.Name == "RecentProjects" )
                                {
                                    break; // we're done
                                }
                            }
                            else if ( reader.Name == "Project" )
                            {
                                string project = reader.ReadString();
                                if ( (project != null) && (project != "") )
                                {
                                    RecentProjects.Add( project );
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }
        #endregion
    }

    [XmlInclude( typeof(NonProjectProjectSettings) )]
    public class ProjectSettings : SettingsBase
    {
        public ProjectSettings()
            : base( "Projects" )
		{

        }

        #region Variables
        protected bool m_automaticSorting;
        protected string m_postCompileCommand;
        protected string m_startDebugCommand;
        #endregion

        #region Properties
        public bool AutomaticSorting
        {
            get
            {
                return m_automaticSorting;
            }
            set
            {
                m_automaticSorting = value;
            }
        }

        // NOTE: This is deprecated.  PostCompileCommand in CompileSettings is used instead
        // to give users configuration-level control over post-compile commands that are run
        // instead of project-level control.
        public string PostCompileCommand
        {
            get
            {
                return m_postCompileCommand;
            }
            set
            {
                m_postCompileCommand = value;
            }
        }

        public string StartDebugCommand
        {
            get
            {
                return m_startDebugCommand;
            }
            set
            {
                m_startDebugCommand = value;
            }
        }
        #endregion

        #region Overrides
        public override SettingsBase Clone()
        {
            ProjectSettings p = new ProjectSettings();
            p.Copy( this );

            return p;
        }

        public override void Copy( SettingsBase s )
        {
            if ( s is ProjectSettings )
            {
                Copy( s as ProjectSettings );
            }
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new ProjectSettingsControl();
        }

        public override bool Equals( System.Object obj )
        {
            // If parameter cannot be cast to Point return false.
            ProjectSettings p = obj as ProjectSettings;
            if ( (System.Object)p == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (this.AutomaticSorting == p.AutomaticSorting)
                && (this.PostCompileCommand == p.PostCompileCommand)
                && (this.StartDebugCommand == p.StartDebugCommand)
                && base.Equals( p );
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.AutomaticSorting.ToString() );
            s.Append( this.PostCompileCommand );
            s.Append( this.StartDebugCommand );
            s.Append( this.Name );
            return s.ToString().GetHashCode();
        }

        public override void Reset()
        {
            this.AutomaticSorting = false;
            this.PostCompileCommand = string.Empty;
            this.StartDebugCommand = string.Empty;
        }
        #endregion

        #region Virtual Functions
        public virtual void Copy( ProjectSettings p )
        {
            if ( p == null )
            {
                return;
            }

            this.AutomaticSorting = p.AutomaticSorting;
            this.PostCompileCommand = p.PostCompileCommand;
            this.StartDebugCommand = p.StartDebugCommand;
        }

        public virtual bool Equals( ProjectSettings p )
        {
            // If parameter is null return false:
            if ( (object)p == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (this.AutomaticSorting == p.AutomaticSorting)
                && (this.PostCompileCommand == p.PostCompileCommand)
                && (this.StartDebugCommand == p.StartDebugCommand)
                && base.Equals( p );
        }

        public virtual void Upgrade( ProjectTabSettings oldSettings )
        {
            this.AutomaticSorting = oldSettings.AutomaticSorting;
            this.PostCompileCommand = oldSettings.PostCompileCommand;
            this.StartDebugCommand = oldSettings.StartDebugCommand;
        }
        #endregion
    }

    public class NonProjectProjectSettings : ProjectSettings
    {
        public NonProjectProjectSettings()
        {

        }

        #region Variables
        private bool m_parsingServiceEnabled;
        #endregion

        #region Properties
        public bool ParsingServiceEnabled
        {
            get
            {
                return m_parsingServiceEnabled;
            }
            set
            {
                m_parsingServiceEnabled = value;
            }
        }
        #endregion

        #region Overrides
        public override void Copy( SettingsBase s )
        {
            if ( s is NonProjectProjectSettings )
            {
                Copy( s as NonProjectProjectSettings );
            }
            else if ( s is ProjectSettings )
            {
                base.Copy( s as ProjectSettings );
            }
        }

        public override void Copy( ProjectSettings p )
        {
            if ( p is NonProjectProjectSettings )
            {
                Copy( p as NonProjectProjectSettings );
            }
            else
            {
                base.Copy( p );
            }
        }

        public override SettingsBase Clone()
        {
            NonProjectProjectSettings p = new NonProjectProjectSettings();
            p.Copy( this );

            return p;
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new NonProjectProjectSettingsControl();
        }

        public override bool Equals( System.Object obj )
        {
            // If parameter cannot be cast to Point return false.
            NonProjectProjectSettings p = obj as NonProjectProjectSettings;
            if ( (System.Object)p == null )
            {
                ProjectSettings ps = obj as ProjectSettings;
                if ( (System.Object)ps == null )
                {
                    return false;
                }
                else
                {
                    return base.Equals( ps );
                }
            }

            // Return true if the fields match:
            return (ParsingServiceEnabled == p.ParsingServiceEnabled)
                && base.Equals( p );
        }

        public override bool Equals( ProjectSettings p )
        {
            if ( (object)p == null )
            {
                return false;
            }

            if ( p is NonProjectProjectSettings )
            {
                return Equals( p as NonProjectProjectSettings );
            }

            return base.Equals( p as ProjectSettings );
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.ParsingServiceEnabled.ToString() );
            s.Append( this.AutomaticSorting.ToString() );
            s.Append( this.PostCompileCommand );
            s.Append( this.StartDebugCommand );
            s.Append( this.Name );
            return s.ToString().GetHashCode();
        }

        public override void Reset()
        {
            base.Reset();

            IntPtr handle = (IntPtr)Process.GetCurrentProcess().Handle;
            UIntPtr ProcAffinityMask = UIntPtr.Zero;
            UIntPtr SysAffinityMask = UIntPtr.Zero;

            if ( ApplicationSettings.GetProcessAffinityMask( handle, out ProcAffinityMask, out SysAffinityMask ) && ((uint)ProcAffinityMask != 1) )
            {
                // start the parsing service
                this.ParsingServiceEnabled = true;
            }
            else
            {
                this.ParsingServiceEnabled = false;
            }
        }

        public override void Upgrade( ProjectTabSettings oldSettings )
        {
            base.Upgrade( oldSettings );

            this.ParsingServiceEnabled = oldSettings.ParsingServiceEnabled;
        }
        #endregion

        #region Public Functions
        public void Copy( NonProjectProjectSettings p )
        {
            if ( p == null )
            {
                return;
            }

            base.Copy( p );

            this.ParsingServiceEnabled = p.ParsingServiceEnabled;
        }

        public bool Equals( NonProjectProjectSettings p )
        {
            // If parameter is null return false:
            if ( (object)p == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (this.ParsingServiceEnabled == p.ParsingServiceEnabled)
                && base.Equals( p );
        }
        #endregion
    }
    #endregion

    #region File Settings
    /// <summary>
    /// Kept for backwards compatibility.
    /// </summary>
    public class FileTabSettings : TabSettingsBase
	{
        public FileTabSettings()
            : this( false )
        {

        }

        public FileTabSettings( bool gameSpecific )
            : base( "Files" )
		{
			this.GameSpecific = gameSpecific;
        }

        #region Variables
        private bool m_gameSpecific;
        private bool m_reloadLastFiles;
        private bool m_middleClickClose;
        private bool m_parsingServiceEnabled;
        private List<string> m_recentFiles = new List<string>();
        private bool m_showLineNumbers = false;
        #endregion

        #region Properties
        public bool GameSpecific
        {
            get
            {
                return m_gameSpecific;
            }
            set
            {
                m_gameSpecific = value;
            }
        }

        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        [XmlIgnore]
        public bool ReloadLastFiles
        {
            get
            {
                return m_reloadLastFiles;
            }
            set
            {
                m_reloadLastFiles = value;
            }
        }

        public bool MiddleClickClose
        {
            get
            {
                return m_middleClickClose;
            }
            set
            {
                m_middleClickClose = value;
            }
        }

        public bool ParsingServiceEnabled
        {
            get
            {
                return m_parsingServiceEnabled;
            }
            set
            {
                m_parsingServiceEnabled = value;
            }
        }

        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        [XmlIgnore]
        public List<string> RecentFiles
        {
            get
            {
                return m_recentFiles;
            }
        }

        public bool ShowLineNumbers
        {
            get
            {
                return m_showLineNumbers;
            }
            set
            {
                m_showLineNumbers = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( System.Object obj )
        {
            // If parameter cannot be cast to Point return false.
            FileTabSettings f = obj as FileTabSettings;
            if ( (System.Object)f == null )
            {
                return false;
            }

            bool apples2apples = this.GameSpecific && f.GameSpecific;

            // Return true if the fields match:
            return (!apples2apples || (this.ShowLineNumbers == f.ShowLineNumbers))
                && (this.MiddleClickClose == f.MiddleClickClose)
                && (this.ParsingServiceEnabled == f.ParsingServiceEnabled)
                && base.Equals( f );
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.MiddleClickClose.ToString() );
            s.Append( this.ParsingServiceEnabled.ToString() );
            s.Append( this.Name );
            return s.ToString().GetHashCode();
        }

        public override void Copy( TabSettingsBase t )
        {
            if ( t is FileTabSettings )
            {
                Copy( t as FileTabSettings );
            }
        }

        public override TabSettingsBase Clone()
        {
            FileTabSettings f = new FileTabSettings( this.GameSpecific );
            f.Copy( this );

            return f;
        }

        public override void Reset()
        {
            this.ReloadLastFiles = true;
            this.MiddleClickClose = true;
            this.ParsingServiceEnabled = true;
            this.ShowLineNumbers = false;
        }
        #endregion

        #region Public Functions
        public void Copy( FileTabSettings f )
        {
            if ( f == null )
            {
                return;
            }

            this.MiddleClickClose = f.MiddleClickClose;
            this.ParsingServiceEnabled = f.ParsingServiceEnabled;

            if ( !this.GameSpecific )
            {
                this.ShowLineNumbers = f.ShowLineNumbers;
            }
        }

        public bool Equals( FileTabSettings f )
        {
            // If parameter is null return false:
            if ( (object)f == null )
            {
                return false;
            }

            bool apples2apples = this.GameSpecific && f.GameSpecific;

            // Return true if the fields match:
            return (!apples2apples || (this.ShowLineNumbers == f.ShowLineNumbers))
                && (this.MiddleClickClose == f.MiddleClickClose)
                && (this.ParsingServiceEnabled == f.ParsingServiceEnabled)
                && base.Equals( f );
        }

		/// <summary>
        /// Read file-specific settings from the XmlTextReader.  Kept for backwards compatibility.
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		public bool LoadXml( XmlTextReader reader )
		{
			string parentName = reader.Name;

			Reset();

			string reloadLastFiles = reader["ReloadLastFiles"];
			if ( reloadLastFiles != null )
			{
				ReloadLastFiles = Convert.ToBoolean(reloadLastFiles);
			}

            string middleClick = reader["MiddleClickClose"];
            if ( middleClick != null )
            {
                MiddleClickClose = Convert.ToBoolean( middleClick );
            }

            string parsingServiceEnabled = reader["ParsingServiceEnabled"];
            if ( parsingServiceEnabled != null )
            {
                ParsingServiceEnabled = Convert.ToBoolean( parsingServiceEnabled );
            }

            while ( reader.Read() )
			{
				if ( !reader.IsStartElement() || reader.IsEmptyElement )
				{
					if ( reader.Name == parentName )
					{
						break; // we're done
					}
				}
				else if ( reader.Name == "RecentFiles" )
				{
					RecentFiles.Clear();

					if ( !reader.IsEmptyElement )
					{
						while ( reader.Read() )
						{
							if ( !reader.IsStartElement() )
							{
								if ( reader.Name == "RecentFiles" )
								{
									break; // we're done
								}
							}
							else if ( reader.Name == "File" )
							{
								string file = reader.ReadString();
								if ( (file != null) && (file != "") )
								{
									RecentFiles.Add( file );
								}
							}
						}
					}
				}
			}

			return true;
        }
        #endregion
    }

    public class FileSettings : SettingsBase
    {
        public FileSettings()
            : base( "Files" )
        {
        }

        #region Variables
        private bool m_middleClickClose;
        private bool m_parsingServiceEnabled;
        #endregion

        #region Properties
        public bool MiddleClickClose
        {
            get
            {
                return m_middleClickClose;
            }
            set
            {
                m_middleClickClose = value;
            }
        }

        public bool ParsingServiceEnabled
        {
            get
            {
                return m_parsingServiceEnabled;
            }
            set
            {
                m_parsingServiceEnabled = value;
            }
        }
        #endregion

        #region Overrides
        public override SettingsBase Clone()
        {
            FileSettings f = new FileSettings();
            f.Copy( this );

            return f;
        }

        public override void Copy( SettingsBase s )
        {
            if ( s is FileSettings )
            {
                Copy( s as FileSettings );
            }
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new FileSettingsControl();
        }

        public override bool Equals( System.Object obj )
        {
            // If parameter cannot be cast to Point return false.
            FileSettings f = obj as FileSettings;
            if ( (System.Object)f == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (this.MiddleClickClose == f.MiddleClickClose)
                && (this.ParsingServiceEnabled == f.ParsingServiceEnabled)
                && base.Equals( f );
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.MiddleClickClose.ToString() );
            s.Append( this.ParsingServiceEnabled.ToString() );
            s.Append( this.Name );
            return s.ToString().GetHashCode();
        }

        public override void Reset()
        {
            this.MiddleClickClose = true;
            this.ParsingServiceEnabled = true;
        }
        #endregion

        #region Virtual Functions
        public virtual void Copy( FileSettings f )
        {
            if ( f == null )
            {
                return;
            }

            this.MiddleClickClose = f.MiddleClickClose;
            this.ParsingServiceEnabled = f.ParsingServiceEnabled;
        }

        public virtual bool Equals( FileSettings f )
        {
            // If parameter is null return false:
            if ( (object)f == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (this.MiddleClickClose == f.MiddleClickClose)
                && (this.ParsingServiceEnabled == f.ParsingServiceEnabled)
                && base.Equals( f );
        }

        public virtual void Upgrade( FileTabSettings oldSettings )
        {
            this.MiddleClickClose = oldSettings.MiddleClickClose;
            this.ParsingServiceEnabled = oldSettings.ParsingServiceEnabled;
        }
        #endregion
    }

    public class NonGameFileSettings : FileSettings
    {
        public NonGameFileSettings()
        {

        }

        #region Variables
        private bool m_showLineNumbers = false;
        private bool m_documentOverflowScrollable = false;
        #endregion

        #region Properties
        public bool ShowLineNumbers
        {
            get
            {
                return m_showLineNumbers;
            }
            set
            {
                m_showLineNumbers = value;
            }
        }

        public bool DocumentOverflowScrollable
        {
            get
            {
                return m_documentOverflowScrollable;
            }
            set
            {
                m_documentOverflowScrollable = value;
            }
        }
        #endregion

        #region Overrides
        public override SettingsBase Clone()
        {
            NonGameFileSettings f = new NonGameFileSettings();
            f.Copy( this );

            return f;
        }

        public override void Copy( SettingsBase s )
        {
            if ( s is NonGameFileSettings )
            {
                Copy( s as NonGameFileSettings );
            }
            else if ( s is FileSettings )
            {
                base.Copy( s as FileSettings );
            }
        }

        public override void Copy( FileSettings f )
        {
            if ( f is NonGameFileSettings )
            {
                Copy( f as NonGameFileSettings );
            }
            else
            {
                base.Copy( f );
            }
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new NonGameFileSettingsControl();
        }

        public override bool Equals( System.Object obj )
        {
            // If parameter cannot be cast to Point return false.
            NonGameFileSettings f = obj as NonGameFileSettings;
            if ( (System.Object)f == null )
            {
                FileSettings fs = obj as FileSettings;
                if ( (System.Object)fs == null )
                {
                    return false;
                }
                else
                {
                    return base.Equals( fs );
                }
            }

            // Return true if the fields match:
            return (this.ShowLineNumbers == f.ShowLineNumbers)
                && (this.DocumentOverflowScrollable == f.DocumentOverflowScrollable)
                && base.Equals( f );
        }

        public override bool Equals( FileSettings f )
        {
            if ( (object)f == null )
            {
                return false;
            }

            if ( f is NonGameFileSettings )
            {
                return Equals( f as NonGameFileSettings );
            }

            return base.Equals( f as FileSettings );
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.MiddleClickClose );
            s.Append( this.ParsingServiceEnabled );
            s.Append( this.Name );
            s.Append( this.ShowLineNumbers );
            s.Append( this.DocumentOverflowScrollable );
            return s.ToString().GetHashCode();
        }

        public override void Reset()
        {
            base.Reset();

            this.ShowLineNumbers = false;
            this.DocumentOverflowScrollable = false;
        }

        public override void Upgrade( FileTabSettings oldSettings )
        {
            base.Upgrade( oldSettings );

            this.ShowLineNumbers = oldSettings.ShowLineNumbers;
        }
        #endregion

        #region Public Functions
        public void Copy( NonGameFileSettings f )
        {
            if ( f == null )
            {
                return;
            }

            base.Copy( f );

            this.ShowLineNumbers = f.ShowLineNumbers;
            this.DocumentOverflowScrollable = f.DocumentOverflowScrollable;
        }

        public bool Equals( NonGameFileSettings f )
        {
            // If parameter is null return false:
            if ( (object)f == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (this.ShowLineNumbers == f.ShowLineNumbers)
                && (this.DocumentOverflowScrollable == f.DocumentOverflowScrollable)
                && base.Equals( f );
        }
        #endregion
    }
    #endregion

    #region Toolbar Settings
    /// <summary>
    /// Kept for backwards compatibility
    /// </summary>
    public class ToolbarTabSettings : TabSettingsBase
	{
		public ToolbarTabSettings()
            : base( "Toolbar" )
		{

        }

        #region Variables
        private bool m_rememberButtonVisiblity;
        private List<ToolbarButtonSetting> m_toolbarButtonSettings = new List<ToolbarButtonSetting>();
        private List<ToolStripLocation> m_toolStripLocations = new List<ToolStripLocation>();
        #endregion

        #region Properties
        public bool RememberButtonVisiblity
        {
            get
            {
                return m_rememberButtonVisiblity;
            }
            set
            {
                m_rememberButtonVisiblity = value;
            }
        }

        [XmlIgnore]
        public List<ToolbarButtonSetting> ToolbarButtonSettings
        {
            get
            {
                return m_toolbarButtonSettings;
            }
            set
            {
                m_toolbarButtonSettings = value;
            }
        }

        [XmlArray( "ToolbarButtonSettings" )]
        public ToolbarButtonSetting[] XmlToolbarButtonSettings
        {
            get
            {
                return m_toolbarButtonSettings.ToArray();
            }
            set
            {
                m_toolbarButtonSettings.Clear();
                if ( value != null )
                {
                    m_toolbarButtonSettings.AddRange( value );
                }
            }
        }

        [XmlIgnore]
        public List<ToolStripLocation> ToolStripLocations
        {
            get
            {
                return m_toolStripLocations;
            }
            set
            {
                m_toolStripLocations = value;
            }
        }

        [XmlArray( "ToolStripLocations" )]
        public ToolStripLocation[] XmlToolStripLocations
        {
            get
            {
                return m_toolStripLocations.ToArray();
            }
            set
            {
                m_toolStripLocations.Clear();
                if ( value != null )
                {
                    m_toolStripLocations.AddRange( value );
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( System.Object obj )
        {
            // If parameter cannot be cast to Point return false.
            ToolbarTabSettings t = obj as ToolbarTabSettings;
            if ( (System.Object)t == null )
            {
                return false;
            }

            // Return true if the fields match:
            if ( (this.RememberButtonVisiblity == t.RememberButtonVisiblity)
                && (this.ToolbarButtonSettings.Count == t.ToolbarButtonSettings.Count)
                && (this.ToolStripLocations.Count == t.ToolStripLocations.Count)
                && base.Equals( t ))
            {
                int count = this.ToolbarButtonSettings.Count;
                for ( int i = 0; i < count; ++i )
                {
                    if ( this.ToolbarButtonSettings[i] != t.ToolbarButtonSettings[i] )
                    {
                        return false;
                    }
                }

                count = this.ToolStripLocations.Count;
                for ( int i = 0; i < count; ++i )
                {
                    if ( this.ToolStripLocations[i] != t.ToolStripLocations[i] )
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.RememberButtonVisiblity.ToString() );
            s.Append( this.ToolbarButtonSettings.Count.ToString() );
            foreach ( ToolbarButtonSetting b in this.ToolbarButtonSettings )
            {
                s.Append( b.ToString() );
            }
            foreach ( ToolStripLocation l in this.ToolStripLocations )
            {
                s.Append( l.ToString() );
            }

            s.Append( this.Name );

            return s.ToString().GetHashCode();
        }

        public override void Copy( TabSettingsBase t )
        {
            if ( t is ToolbarTabSettings )
            {
                Copy( t as ToolbarTabSettings );
            }
        }

        public override TabSettingsBase Clone()
        {
            ToolbarTabSettings t = new ToolbarTabSettings();
            t.Copy( this );

            return t;
        }

        public override void Reset()
        {
            this.RememberButtonVisiblity = true;
        }
        #endregion

        #region Public Functions
        public void Copy( ToolbarTabSettings t )
        {
            if ( t == null )
            {
                return;
            }

            this.RememberButtonVisiblity = t.RememberButtonVisiblity;

            this.ToolbarButtonSettings.Clear();
            foreach ( ToolbarButtonSetting button in t.ToolbarButtonSettings )
            {
                this.ToolbarButtonSettings.Add( button );
            }

            this.ToolStripLocations.Clear();
            foreach ( ToolStripLocation location in t.ToolStripLocations )
            {
                this.ToolStripLocations.Add( location );
            }
        }

        public bool Equals( ToolbarTabSettings t )
        {
            // If parameter is null return false:
            if ( (object)t == null )
            {
                return false;
            }

            // Return true if the fields match:
            if ( (this.RememberButtonVisiblity == t.RememberButtonVisiblity)
                && (this.ToolbarButtonSettings.Count == t.ToolbarButtonSettings.Count)
                && (this.ToolStripLocations.Count == t.ToolStripLocations.Count)
                && base.Equals( t ) )
            {
                int count = this.ToolbarButtonSettings.Count;
                for ( int i = 0; i < count; ++i )
                {
                    if ( this.ToolbarButtonSettings[i] != t.ToolbarButtonSettings[i] )
                    {
                        return false;
                    }
                }

                count = this.ToolStripLocations.Count;
                for ( int i = 0; i < count; ++i )
                {
                    if ( this.ToolStripLocations[i] != t.ToolStripLocations[i] )
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

		/// <summary>
        /// Read toolbar-specific settings from the XmlTextReader.  Kept for backwards compatibility.
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		public bool LoadXml( XmlTextReader reader )
		{
			string parentName = reader.Name;

			Reset();

			string rememberVis = reader["RememberButtonVisibility"];
			if ( rememberVis != null )
			{
				RememberButtonVisiblity = Convert.ToBoolean(rememberVis);
			}

			while ( reader.Read() )
			{
				if ( !reader.IsStartElement() || reader.IsEmptyElement )
				{
					if ( reader.Name == parentName )
					{
						break; // we're done
					}
				}
				else if ( reader.Name == "ToolbarButtons" )
				{
					ToolbarButtonSettings.Clear();

					while ( reader.Read() )
					{
						if ( !reader.IsStartElement() )
						{
							if ( reader.Name == "ToolbarButtons" )
							{
								break; // we're done
							}
						}
						else if ( reader.Name == "Button" )
						{
							ToolbarButtonSetting button = new ToolbarButtonSetting();
							if ( !button.LoadButton(reader) )
							{
								return false;
							}
							else
							{
								ToolbarButtonSettings.Add( button );
							}
						}
					}
				}
                else if ( reader.Name == "ToolStripLocations" )
                {
                    ToolStripLocations.Clear();

                    while ( reader.Read() )
                    {
                        if ( !reader.IsStartElement() )
                        {
                            if ( reader.Name == "ToolStripLocations" )
                            {
                                break; // we're done
                            }
                        }
                        else if ( reader.Name == "ToolStripLocation" )
                        {
                            ToolStripLocation loc = new ToolStripLocation();
                            if ( !loc.LoadLocation( reader ) )
                            {
                                return false;
                            }
                            else
                            {
                                ToolStripLocations.Add( loc );
                            }
                        }
                    }
                }
			}

			return true;
        }
        #endregion
    }

    public class ToolbarSettings : SettingsBase
    {
        public ToolbarSettings()
            : base( "Toolbar" )
        {

        }

        #region Variables
        private bool m_rememberButtonVisiblity;
        private List<ToolbarButtonSetting> m_toolbarButtonSettings = new List<ToolbarButtonSetting>();
        private List<ToolStripLocation> m_toolStripLocations = new List<ToolStripLocation>();
        #endregion

        #region Properties
        public bool RememberButtonVisiblity
        {
            get
            {
                return m_rememberButtonVisiblity;
            }
            set
            {
                m_rememberButtonVisiblity = value;
            }
        }

        [XmlIgnore]
        public List<ToolbarButtonSetting> ToolbarButtonSettings
        {
            get
            {
                return m_toolbarButtonSettings;
            }
            set
            {
                m_toolbarButtonSettings = value;
            }
        }

        [XmlArray( "ToolbarButtonSettings" )]
        public ToolbarButtonSetting[] XmlToolbarButtonSettings
        {
            get
            {
                return m_toolbarButtonSettings.ToArray();
            }
            set
            {
                m_toolbarButtonSettings.Clear();
                if ( value != null )
                {
                    m_toolbarButtonSettings.AddRange( value );
                }
            }
        }

        [XmlIgnore]
        public List<ToolStripLocation> ToolStripLocations
        {
            get
            {
                return m_toolStripLocations;
            }
            set
            {
                m_toolStripLocations = value;
            }
        }

        [XmlArray( "ToolStripLocations" )]
        public ToolStripLocation[] XmlToolStripLocations
        {
            get
            {
                return m_toolStripLocations.ToArray();
            }
            set
            {
                m_toolStripLocations.Clear();
                if ( value != null )
                {
                    m_toolStripLocations.AddRange( value );
                }
            }
        }
        #endregion

        #region Overrides
        public override SettingsBase Clone()
        {
            ToolbarSettings t = new ToolbarSettings();
            t.Copy( this );

            return t;
        }

        public override void Copy( SettingsBase s )
        {
            if ( s is ToolbarSettings )
            {
                Copy( s as ToolbarSettings );
            }
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new ToolbarSettingsControl();
        }

        public override bool Equals( System.Object obj )
        {
            // If parameter cannot be cast to Point return false.
            ToolbarSettings t = obj as ToolbarSettings;
            if ( (System.Object)t == null )
            {
                return false;
            }

            // Return true if the fields match:
            if ( (this.RememberButtonVisiblity == t.RememberButtonVisiblity)
                && (this.ToolbarButtonSettings.Count == t.ToolbarButtonSettings.Count)
                && (this.ToolStripLocations.Count == t.ToolStripLocations.Count)
                && base.Equals( t ) )
            {
                int count = this.ToolbarButtonSettings.Count;
                for ( int i = 0; i < count; ++i )
                {
                    if ( this.ToolbarButtonSettings[i] != t.ToolbarButtonSettings[i] )
                    {
                        return false;
                    }
                }

                count = this.ToolStripLocations.Count;
                for ( int i = 0; i < count; ++i )
                {
                    if ( this.ToolStripLocations[i] != t.ToolStripLocations[i] )
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.RememberButtonVisiblity.ToString() );
            s.Append( this.ToolbarButtonSettings.Count.ToString() );
            foreach ( ToolbarButtonSetting b in this.ToolbarButtonSettings )
            {
                s.Append( b.ToString() );
            }
            foreach ( ToolStripLocation l in this.ToolStripLocations )
            {
                s.Append( l.ToString() );
            }

            s.Append( this.Name );

            return s.ToString().GetHashCode();
        }

        public override void Reset()
        {
            this.RememberButtonVisiblity = true;
        }
        #endregion

        #region Public Functions
        public void Copy( ToolbarSettings t )
        {
            if ( t == null )
            {
                return;
            }

            this.RememberButtonVisiblity = t.RememberButtonVisiblity;

            this.ToolbarButtonSettings.Clear();
            foreach ( ToolbarButtonSetting button in t.ToolbarButtonSettings )
            {
                this.ToolbarButtonSettings.Add( button );
            }

            this.ToolStripLocations.Clear();
            foreach ( ToolStripLocation location in t.ToolStripLocations )
            {
                this.ToolStripLocations.Add( location );
            }
        }

        public bool Equals( ToolbarSettings t )
        {
            // If parameter is null return false:
            if ( (object)t == null )
            {
                return false;
            }

            // Return true if the fields match:
            if ( (this.RememberButtonVisiblity == t.RememberButtonVisiblity)
                && (this.ToolbarButtonSettings.Count == t.ToolbarButtonSettings.Count)
                && (this.ToolStripLocations.Count == t.ToolStripLocations.Count)
                && base.Equals( t ) )
            {
                int count = this.ToolbarButtonSettings.Count;
                for ( int i = 0; i < count; ++i )
                {
                    if ( this.ToolbarButtonSettings[i] != t.ToolbarButtonSettings[i] )
                    {
                        return false;
                    }
                }

                count = this.ToolStripLocations.Count;
                for ( int i = 0; i < count; ++i )
                {
                    if ( this.ToolStripLocations[i] != t.ToolStripLocations[i] )
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public void Upgrade( ToolbarTabSettings oldSettings )
        {
            if ( oldSettings == null )
            {
                return;
            }

            this.RememberButtonVisiblity = oldSettings.RememberButtonVisiblity;

            this.ToolbarButtonSettings.Clear();
            foreach ( ToolbarButtonSetting button in oldSettings.ToolbarButtonSettings )
            {
                this.ToolbarButtonSettings.Add( button );
            }

            this.ToolStripLocations.Clear();
            foreach ( ToolStripLocation location in oldSettings.ToolStripLocations )
            {
                this.ToolStripLocations.Add( location );
            }
        }
        #endregion
    }

    public class ToolbarButtonSetting
    {
        public ToolbarButtonSetting()
        {
        }

        #region Variables
        private string m_tag = "";
        private string m_text = "";
        private bool m_visible = true;
        private bool m_isCustomButton = false;
        private string m_commandLine = "";
        private string m_iconFile = "";
        private System.Drawing.Image m_icon = null;
        private ToolStripItem m_buttonItem = null;
        private ToolStripMenuItem m_menuItem = null;
        #endregion

        #region Properties
        public bool IsCustomButton
        {
            get
            {
                return m_isCustomButton;
            }
            set
            {
                m_isCustomButton = value;
            }
        }

        public string Tag
        {
            get
            {
                return m_tag;
            }
            set
            {
                m_tag = value;

                if ( this.IsCustomButton && !String.IsNullOrEmpty( m_tag ) )
                {
                    if ( !m_tag.StartsWith( "custom" ) )
                    {
                        m_tag = ConvertNameToTag( m_tag );
                    }

                    m_text = ConvertTagToName( m_tag );
                }
            }
        }

        public string Text
        {
            get
            {
                return m_text;
            }
            set
            {
                m_text = value;

                if ( this.IsCustomButton && !String.IsNullOrEmpty( m_tag ) )
                {
                    m_text = ConvertTagToName( m_tag );
                }
            }
        }

        public bool Visible
        {
            get
            {
                return m_visible;
            }
            set
            {
                m_visible = value;
            }
        }

        public string CommandLine
        {
            get
            {
                return m_commandLine;
            }
            set
            {
                m_commandLine = value;
            }
        }

        public string IconFile
        {
            get
            {
                return m_iconFile;
            }
            set
            {
                m_iconFile = value;
            }
        }

        [XmlIgnore]
        public System.Drawing.Image Icon
        {
            get
            {
                return m_icon;
            }
            set
            {
                m_icon = value;
            }
        }

        [XmlIgnore]
        public ToolStripItem ButtonItem
        {
            get
            {
                return m_buttonItem;
            }
            set
            {
                m_buttonItem = value;
            }
        }

        [XmlIgnore]
        public ToolStripMenuItem MenuItem
        {
            get
            {
                return m_menuItem;
            }
            set
            {
                m_menuItem = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( System.Object obj )
        {
            // If parameter is null return false.
            if ( obj == null )
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ToolbarButtonSetting b = obj as ToolbarButtonSetting;
            if ( (System.Object)b == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (Tag == b.Tag)
                && (Text == b.Text)
                && (Visible == b.Visible)
                && (IsCustomButton == b.IsCustomButton)
                && (CommandLine == b.CommandLine)
                && (IconFile == b.IconFile);
        }

        public bool Equals( ToolbarButtonSetting b )
        {
            // If parameter is null return false:
            if ( (object)b == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (Tag == b.Tag)
                && (Text == b.Text)
                && (Visible == b.Visible)
                && (IsCustomButton == b.IsCustomButton)
                && (CommandLine == b.CommandLine)
                && (IconFile == b.IconFile);
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( Tag );
            s.Append( Text );
            s.Append( Visible.ToString() );
            s.Append( IsCustomButton.ToString() );
            s.Append( CommandLine );
            s.Append( IconFile );
            return s.ToString().GetHashCode();
        }
        #endregion

        #region Save/Load
        public bool LoadButton( XmlTextReader reader )
        {
            string parentName = reader.Name;

            string att = reader["name"];
            if ( att != null )
            {
                m_tag = att;
            }

            att = reader["visible"];
            if ( att != null )
            {
                m_visible = Convert.ToBoolean( att );
            }

            att = reader["custom"];
            if ( (att != null) && (att != "") )
            {
                m_isCustomButton = true;

                if ( !m_tag.StartsWith( "custom" ) )
                {
                    m_tag = ConvertNameToTag( m_tag );
                }

                m_text = ConvertTagToName( m_tag );

                m_commandLine = att;

                att = reader["icon"];
                if ( att != null )
                {
                    m_iconFile = att;
                }
            }

            return true;
        }
        #endregion

        #region Misc
        static public string ConvertNameToTag( string name )
        {
            string tag = name.Replace( " ", "_" );
            tag = "custom" + tag;
            return tag;
        }

        static public string ConvertTagToName( string tag )
        {
            string name = tag;
            if ( tag.StartsWith( "custom" ) )
            {
                name = tag.Remove( 0, 6 );
            }
            name = name.Replace( "_", " " );
            return name;
        }
        #endregion
    }

    public class ToolStripLocation
    {
        public ToolStripLocation()
        {
        }

        #region Variables
        private string m_name;
        private string m_toolStripPanel;
        private Point m_location;
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }

        public string ToolStripPanel
        {
            get
            {
                return m_toolStripPanel;
            }
            set
            {
                m_toolStripPanel = value;
            }
        }

        public Point Location
        {
            get
            {
                return m_location;
            }
            set
            {
                m_location = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( System.Object obj )
        {
            // If parameter is null return false.
            if ( obj == null )
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ToolStripLocation l = obj as ToolStripLocation;
            if ( (System.Object)l == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (Name == l.Name)
                && (ToolStripPanel == l.ToolStripPanel)
                && (Location == l.Location);
        }

        public bool Equals( ToolStripLocation l )
        {
            // If parameter is null return false:
            if ( (object)l == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (Name == l.Name)
                && (ToolStripPanel == l.ToolStripPanel)
                && (Location == l.Location);
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( Name );
            s.Append( ToolStripPanel );
            s.Append( Location.ToString() );
            return s.ToString().GetHashCode();
        }
        #endregion

        #region Save/Load
        public bool LoadLocation( XmlTextReader reader )
        {
            Name = reader["Name"];
            ToolStripPanel = reader["ToolStripPanel"];

            string x = reader["X"];
            if ( x != null )
            {
                m_location.X = Int32.Parse( x );
            }
            else
            {
                m_location.X = 0;
            }

            string y = reader["Y"];
            if ( y != null )
            {
                m_location.Y = Int32.Parse( y );
            }
            else
            {
                m_location.Y = 0;
            }

            return true;
        }
        #endregion
    }
    #endregion

    #region Compiling Settings
    /// <summary>
    /// Each Parser Plugin should derive from this class to add the options that are unique to the compiler for that language.
    /// </summary>
    public class CompilingSettings : SettingsBase
    {
        public CompilingSettings()
            : base( "CompilingSettings" )
        {

        }

        protected CompilingSettings( string language )
            : base( language + "CompilingSettings" )
        {
            m_language = language;
        }

        #region Variables


        protected string m_language = string.Empty;
        protected string m_includeFileOpenFileDialogFilter = "All Files (*.*)|*.*";
        protected string m_compilerExecutableOpenFileDialogFilter = "All Files (*.*)|*.*";
        protected string m_resourceExecutableOpenFileDialogFilter = "All Files (*.*)|*.*";

        protected string m_configurationName = "Debug";
        protected string m_compilerExecutable = string.Empty;
        protected string m_resourceExecutable = string.Empty;
        protected string m_outputDirectory = string.Empty;
        protected string m_buildDirectory = string.Empty;
        protected string m_postCompileCommand = string.Empty;

        protected string m_includeFile = string.Empty;
        protected List<string> m_includePaths = new List<string>();

        protected string m_custom = string.Empty;

        #endregion

        #region Properties
        [XmlIgnore]
        public string Language
        {
            get
            {
                return m_language;
            }
        }

        [XmlIgnore]
        public string IncludeFileOpenFileDialogFilter
        {
            get
            {
                return m_includeFileOpenFileDialogFilter;
            }
        }

        [XmlIgnore]
        public string CompilerExecutableOpenFileDialogFilter
        {
            get
            {
                return m_compilerExecutableOpenFileDialogFilter;
            }
        }

        [XmlIgnore]
        public string ResourceExecutableOpenFileDialogFilter
        {
            get
            {
                return m_resourceExecutableOpenFileDialogFilter;
            }
        }

        public string ConfigurationName
        {
            get
            {
                return m_configurationName;
            }
            set
            {
                m_configurationName = value;
            }
        }

        public string CompilerExecutable
        {
            get
            {
                return m_compilerExecutable;
            }
            set
            {
                m_compilerExecutable = value;
            }
        }

        [XmlElement("ResourceExecutable")]
        public string ResourceExecutable
        {
            get
            {
                return m_resourceExecutable;
            }
            set
            {
                m_resourceExecutable = value;
            }
        }

        public string OutputDirectory
        {
            get
            {
                return m_outputDirectory;
            }
            set
            {
                m_outputDirectory = value;
            }
        }

        public string BuildDirectory
        {
            get { return m_buildDirectory; }
            set { m_buildDirectory = value; }
        }

		public string PostCompileCommand
        {
            get
            {
                return m_postCompileCommand;
            }
            set
            {
                m_postCompileCommand = value;
            }
        }

        /// <summary>
        /// Get the substituted, absolute path of the include file.
        /// </summary>
        [XmlIgnore]
        public string IncludeFile
        {
            get
            {
                if ( Path.IsPathRooted( m_includeFile ) )
                {
                    return m_includeFile;
                }
                else
                {
                    string fullPathName;
                    foreach ( string path in m_includePaths )
                    {
                        String translated = ApplicationSettings.GetAbsolutePath(path);

                        fullPathName = Path.Combine(translated, m_includeFile);
                        if ( File.Exists( fullPathName ) )
                        {
                            return fullPathName;
                        }
                    }

                    fullPathName = Path.Combine( Directory.GetCurrentDirectory(), m_includeFile );
                    if ( File.Exists( fullPathName ) )
                    {
                        return fullPathName;
                    }

                    return m_includeFile;
                }
            }
            set
            {
                m_includeFile = value;
            }
        }

        /// <summary>
        /// Get the raw, unsubstituted include file (a master file containing includes to use).
        /// </summary>
        [XmlElement( "IncludeFile" )]
        public string XmlIncludeFile
        {
            get
            {
                return m_includeFile;
            }
            set
            {
                m_includeFile = value;
            }
        }

        /// <summary>
        /// Gets an include paths string (semicolon-separated list of includes) from the underlying array of include paths.
        /// Sets the underlying array based on the provided string of semicolon-separated includes.
        /// </summary>
        [XmlIgnore]
        public string IncludePath
        {
            get
            {
                return ApplicationSettings.ConcatenatePathsString(IncludePaths, substPaths: true);
            }
            set
            {
                IncludePaths = ApplicationSettings.SplitPathsString( value, reverseSubstPaths: true );
            }
        }

        /// <summary>
        /// Gets/sets the array of include paths.
        /// </summary>
        [XmlIgnore]
        public List<string> IncludePaths
        {
            get
            {
                return m_includePaths;
            }
            set
            {
                m_includePaths = value ?? new List<string>();
            }
        }

        [XmlArray( "IncludePaths" )]
        public string[] XmlIncludePaths
        {
            get
            {
                return IncludePaths.ToArray();
            }
            set
            {
                IncludePaths = value?.ToList();
            }
        }

        [XmlElement("Custom")]
        public string Custom
        {
            get
            {
                return m_custom;
            }
            set
            {
                m_custom = value;
            }
        }
        #endregion

        #region Overrides
        public override SettingsBase Clone()
        {
            CompilingSettings c = new CompilingSettings();
            c.Copy( this );

            return c;
        }

        public override void Copy( SettingsBase s )
        {
            if ( s is CompilingSettings )
            {
                Copy( s as CompilingSettings );
            }
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new CompilingSettingsControl();
        }

        public override bool Equals( object obj )
        {
            CompilingSettings c = obj as CompilingSettings;
            if ( (object)c == null )
            {
                return false;
            }

            return this.Equals( c );
        }

        public override int GetHashCode()
        {
            StringBuilder s = new StringBuilder();
            s.Append( this.Language );
            s.Append( this.ConfigurationName );
            s.Append( this.CompilerExecutable );
            s.Append( this.ResourceExecutable );
            s.Append( this.OutputDirectory );
            s.Append( this.BuildDirectory );
            s.Append( this.PostCompileCommand );
            s.Append( this.Name );
            s.Append( this.XmlIncludeFile );
            foreach (string path in this.IncludePaths)
            {
                s.Append(path);
            }
            s.Append( this.Custom );
            return s.ToString().GetHashCode();
        }

        public override void Reset()
        {
            this.ConfigurationName = "Debug";
            this.CompilerExecutable = string.Empty;
            this.OutputDirectory = string.Empty;
            this.BuildDirectory = string.Empty;
            this.IncludePaths.Clear();
            this.PostCompileCommand = string.Empty;
            this.IncludeFile = string.Empty;
        }

        public override string ToString()
        {
            return this.ConfigurationName;
        }

        public override bool Validate( out string error )
        {
            StringBuilder errors = new StringBuilder();

            if (!String.IsNullOrEmpty(this.CompilerExecutable) && !ApplicationSettings.IsPathValid(this.CompilerExecutable))
            {
                errors.AppendLine($"Compiler Executable '{this.CompilerExecutable}' has invalid path characters.");
            }

            if (!String.IsNullOrEmpty(this.ResourceExecutable) && !ApplicationSettings.IsPathValid(this.ResourceExecutable))
            {
                errors.AppendLine($"Resource Executable '{this.ResourceExecutable}' has invalid path characters.");
            }

            if (!String.IsNullOrEmpty(this.OutputDirectory) && !ApplicationSettings.IsPathValid(this.OutputDirectory))
            {
                errors.AppendLine($"Output Directory '{this.OutputDirectory}' has invalid path characters.");
            }

            if (!String.IsNullOrEmpty(this.BuildDirectory) && !ApplicationSettings.IsPathValid(this.BuildDirectory))
            {
                errors.AppendLine($"Build Directory '{this.BuildDirectory}' has invalid path characters.");
            }

            if (!String.IsNullOrEmpty(this.XmlIncludeFile) && !ApplicationSettings.IsPathValid(this.XmlIncludeFile))
            {
                errors.AppendLine($"Include File '{this.XmlIncludeFile}' has invalid path characters.");
            }

            foreach (string includePath in this.IncludePaths)
            {
                if (!ApplicationSettings.IsPathValid(includePath))
                {
                    errors.AppendLine($"Include Path '{includePath}' has invalid path characters.");
                }
            }

            string compilerExecutable = ApplicationSettings.GetAbsolutePath(this.CompilerExecutable);
            if ( compilerExecutable == string.Empty || errors.Length > 0 )
            {
                error = errors.ToString();
                return errors.Length <= 0;
            }

            if ( this.LanguageIsCompilable && !File.Exists( compilerExecutable ) )
            {
                errors.AppendLine("Compiler Executable does not exist.");
            }

            error = errors.ToString();
            return errors.Length <= 0;
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Get the file extension for the output resource and platform, if the language is a compilable language.
        /// Otherwise, <see cref="string.Empty"/> is returned.
        /// </summary>
        /// <param name="platform">The platform being built.</param>
        /// <returns>The resource's extension for this platform, if this is a compilable language. Otherwise <see cref="string.Empty"/>.</returns>
        public virtual string GetResourcePlatformExtension(Platform platform)
        {
            return string.Empty;
        }

        /// <summary>
        /// Calls the other BuildFilenameInOutputOrProjectDirectory with the current value of OutputDirectory
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public string BuildOutputFilename( string fullPath )
        {
            return ApplicationSettings.BuildFilenameInOutputDirectory(ApplicationSettings.GetAbsolutePath(this.OutputDirectory), fullPath);
        }
        #endregion

        #region Virtual Properties
        [XmlIgnore]
        public virtual bool LanguageIsCompilable
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region Virtual Functions
        public virtual string BuildCompileScriptCommand(string scriptFilename, Platform? currentPlatform)
        {
            return string.Empty;
        }

        public virtual string ResourceCompileScriptCommand(string scriptFilename, string outputFilename, Platform currentPlatform)
        {
            return string.Empty;
        }

        public virtual IEnumerable<string> BuildOutputFilenames( string scriptFilename, Platform platform )
        {
            yield break;
        }

        public virtual void Copy( CompilingSettings c )
        {
            if ( c == null )
            {
                return;
            }
            this.CompilerExecutable = c.CompilerExecutable;
            this.ResourceExecutable = c.ResourceExecutable;
            this.ConfigurationName = c.ConfigurationName;
            this.OutputDirectory = c.OutputDirectory;
            this.BuildDirectory = c.BuildDirectory;
            this.PostCompileCommand = c.PostCompileCommand;
            this.CompilerExecutable = c.CompilerExecutable;
            this.ResourceExecutable = c.ResourceExecutable;
            this.XmlIncludeFile = c.XmlIncludeFile;
            this.IncludePaths = c.IncludePaths.ToList();
            this.Custom = c.Custom;
        }

        public virtual bool Equals( CompilingSettings c )
        {
            // If parameter is null return false:
            if ( (object)c == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (this.Language == c.Language)
                && (this.ConfigurationName == c.ConfigurationName)
                && (this.CompilerExecutable == c.CompilerExecutable)
                && (this.ResourceExecutable == c.ResourceExecutable)
                && (this.OutputDirectory == c.OutputDirectory)
                && (this.BuildDirectory == c.BuildDirectory)
                && (this.PostCompileCommand == c.PostCompileCommand)
                && (this.IncludePaths.SequenceEqual(c.IncludePaths))
                && (this.XmlIncludeFile == c.XmlIncludeFile)
                && (this.Custom == c.Custom)
                && base.Equals( c );
        }

        /// <summary>
        /// Used for setting options on the command line compiler.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public virtual void SetValue( string key, string value )
        {
            switch ( key )
            {
                case "include":
                    this.IncludeFile = value;
                    break;
                case "ipath":
                    this.IncludePath = value;
                    break;
                case "scriptCompiler":
                case "compiler":
                    this.CompilerExecutable = value;
                    break;
                case "outputDirectory":
                    this.OutputDirectory = value;
                    break;
                case "postCompileCommand":
                    this.PostCompileCommand = value;
                    break;
            }
        }

        /// <summary>
        /// Indicates whether the current settings produce a valid compiling command.
        /// </summary>
        /// <param name="projectFilename">The name of the project that would be compiled.</param>
        /// <returns><c>true</c> if valid, otherwise <c>false</c>.</returns>
        public virtual bool Validate( string projectFilename, out string error )
        {
            if ( !Validate( out error ) )
            {
                return false;
            }

            string cmd = this.BuildCompileScriptCommand("%1", null);
            if ( String.IsNullOrEmpty( cmd ) )
            {
                error = null;
                return true;
            }

            ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( cmd );
            if ( psInfo == null )
            {
                error = "Unable to build ProcessStartInfo for compiler command.";
                return false;
            }

            if ( this.LanguageIsCompilable && !File.Exists( psInfo.FileName ) )
            {
                error = "Compiler Executable does not exist.";
                return false;
            }

            error = null;
            return true;
        }
        #endregion
    }

    public class CompilingSettingsCollection : List<CompilingSettings>
    {
        public CompilingSettingsCollection()
        {

        }

        #region Variables
        private int m_currentIndex = -1;
        #endregion

        #region Properties
        public int CurrentIndex
        {
            get
            {
                return m_currentIndex;
            }
            set
            {
                m_currentIndex = value;
            }
        }

        public CompilingSettings CurrentSettings
        {
            get
            {
                if ( (m_currentIndex >= 0) && (m_currentIndex < this.Count) )
                {
                    return this[m_currentIndex];
                }

                return null;
            }
            set
            {
                if ( value != null )
                {
                    m_currentIndex = this.IndexOf( value );
                }
                else
                {
                    m_currentIndex = -1;
                }
            }
        }
        #endregion
    };
    #endregion

    #region Language Settings
    /// <summary>
    /// Kept for backwards compatibility.
    /// </summary>
	public class LanguageTabSettings
	{
        public LanguageTabSettings()
            : this( "SanScript", false )
        {

        }

        public LanguageTabSettings( string language, bool gameSpecific )
        {
            GameSpecific = gameSpecific;
            Reset( language );
        }

        #region Variables
        private bool m_gameSpecific;
        private string m_language;

        private string m_helpReferenceFile;
        private string m_helpLanguageFile;
        private string m_codeSnippetPath;
        private ActiproSoftware.SyntaxEditor.CodeSnippetFolder m_codeSnippetFolder;
        private string m_cacheDirectory;
        #endregion

        #region Properties
        public bool GameSpecific
        {
            get
            {
                return m_gameSpecific;
            }
            set
            {
                m_gameSpecific = value;
            }
        }

        public string Language
        {
            get
            {
                return m_language;
            }
            set
            {
                m_language = value;
            }
        }

        [XmlIgnore]
        public string HelpReferenceFile
        {
            get
            {
                if ( (m_helpReferenceFile == null) || (m_helpReferenceFile == string.Empty) )
                {
                    return m_helpReferenceFile;
                }

                return Path.Combine( ApplicationSettings.ExecutablePath, m_helpReferenceFile );
            }
            set
            {
                m_helpReferenceFile = value;
            }
        }

        [XmlElement( "HelpReferenceFile" )]
        public string HelpReferenceFileRaw
        {
            get
            {
                return m_helpReferenceFile;
            }
            set
            {
                m_helpReferenceFile = value;
            }
        }

        [XmlIgnore]
        public string HelpLanguageFile
        {
            get
            {
                if ( (m_helpLanguageFile == null) || (m_helpLanguageFile == string.Empty) )
                {
                    return m_helpLanguageFile;
                }

                return Path.Combine( ApplicationSettings.ExecutablePath, m_helpLanguageFile );
            }
            set
            {
                m_helpLanguageFile = value;
            }
        }

        [XmlElement( "HelpLanguageFile" )]
        public string HelpLanguageFileRaw
        {
            get
            {
                return m_helpLanguageFile;
            }
            set
            {
                m_helpLanguageFile = value;
            }
        }

        [XmlIgnore]
        public ActiproSoftware.SyntaxEditor.CodeSnippetFolder CodeSnippetFolder
        {
            get
            {
                return m_codeSnippetFolder;
            }
        }

        public string CodeSnippetPath
        {
            get
            {
                return m_codeSnippetPath;
            }
            set
            {
                m_codeSnippetPath = value;

                m_codeSnippetFolder = null;
                List<string> snippetPaths = ApplicationSettings.SplitPathsString( m_codeSnippetPath, reverseSubstPaths: false );
                if ( (snippetPaths != null) && (snippetPaths.Count > 0) )
                {
                    foreach ( string snippetPath in snippetPaths )
                    {
                        try
                        {
                            if ( m_codeSnippetFolder == null )
                            {
                                m_codeSnippetFolder = new CodeSnippetFolder( Path.GetFileName( snippetPath ), snippetPath );
                            }
                            else
                            {
                                CodeSnippetFolder folder = new CodeSnippetFolder( Path.GetFileName( snippetPath ), snippetPath );
                                m_codeSnippetFolder.Merge( folder, true );
                            }
                        }
                        catch ( System.Exception e )
                        {
                            // don't load folder if error
                            ApplicationSettings.Log.ToolException(e, "Exception creating code snippet folder");
                        }
                    }
                }
            }
        }

        public string CacheDirectory
        {
            get
            {
                return m_cacheDirectory;
            }
            set
            {
                m_cacheDirectory = value;
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( System.Object obj )
        {
            // If parameter is null return false.
            if ( obj == null )
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            LanguageTabSettings l = obj as LanguageTabSettings;
            if ( (System.Object)l == null )
            {
                return false;
            }

            bool apples2apples = this.GameSpecific && l.GameSpecific;

            // Return true if the fields match:
            return (m_helpReferenceFile == l.m_helpReferenceFile)
                && (m_helpLanguageFile == l.m_helpLanguageFile)
                && (CodeSnippetPath == l.CodeSnippetPath)
                && (!apples2apples || (CacheDirectory == l.CacheDirectory));
        }

        public bool Equals( LanguageTabSettings l )
        {
            // If parameter is null return false:
            if ( (object)l == null )
            {
                return false;
            }

            bool apples2apples = this.GameSpecific && l.GameSpecific;

            // Return true if the fields match:
            return /*(Language == l.Language)
                &&*/ (m_helpReferenceFile == l.m_helpReferenceFile)
                && (m_helpLanguageFile == l.m_helpLanguageFile)
                && (CodeSnippetPath == l.CodeSnippetPath)
                && (!apples2apples || (CacheDirectory == l.CacheDirectory));
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.GameSpecific );
            s.Append( Language );
            s.Append( m_helpReferenceFile );
            s.Append( m_helpLanguageFile );
            s.Append( CodeSnippetPath );
            s.Append( CacheDirectory );
            return s.ToString().GetHashCode();
        }
        #endregion

        #region Save/Load
        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
		public bool LoadXml( XmlTextReader reader )
		{
			string parentName = reader.Name;

			Reset();

			while ( reader.Read() )
			{
				if ( !reader.IsStartElement() )
				{
					if ( reader.Name == parentName )
					{
						break; // we're done
					}
				}
                else if ( reader.Name == "Language" )
                {
                    Language = reader["name"];
                }
                else if ( reader.Name == "HelpReferenceFile" )
				{
                    HelpReferenceFile = reader["name"];
				}
				else if ( reader.Name == "HelpLanguageFile" )
				{
                    HelpLanguageFile = reader["name"];
				}
				else if ( reader.Name == "CodeSnippets" )
				{
					CodeSnippetPath = reader["folders"];
				}
                else if ( reader.Name == "CacheDirectory" )
                {
                    CacheDirectory = reader["folder"];
                }
            }

			return true;
        }
        #endregion

        #region Misc
        public void Copy( LanguageTabSettings l )
        {
            if ( l == null )
            {
                return;
            }

            Language = l.Language;

            if ( this.GameSpecific && l.GameSpecific )
            {
                HelpLanguageFile = l.m_helpLanguageFile;
                HelpReferenceFile = l.m_helpReferenceFile;
                CodeSnippetPath = l.CodeSnippetPath;
            }

            CacheDirectory = l.CacheDirectory;
        }

		public void Reset()
		{
            Reset( Language );
        }

        private void Reset( string language )
        {
            Language = language;
            HelpReferenceFile = String.Empty;

            if (language.Equals("SanScript", StringComparison.OrdinalIgnoreCase))
            {
                HelpLanguageFile = Path.Combine( Path.GetDirectoryName( Application.ExecutablePath ), "chm", "script.chm" );
            }
            else
            {
                HelpLanguageFile = String.Empty;
            }

            CodeSnippetPath = String.Empty;
            m_cacheDirectory = Path.Combine(Path.GetTempPath(), $"{language}IdentifierCache");
        }
        #endregion
	}

    /// <summary>
    /// Each Parser Plugin should derive from this class to add the options that are unique to the language settings for that language.
    /// </summary>
    public class LanguageSettings : SettingsBase
    {
        public LanguageSettings()
            : base( "Language" )
        {

        }

        protected LanguageSettings( string language )
            : base( language + "LanguageSettings" )
        {
            m_language = language;
            Reset();
        }

        #region Variables
        protected string m_language = string.Empty;
        protected List<string> m_codeSnippetPaths = new List<string>();
        protected string m_cacheDirectory = string.Empty;
        protected ActiproSoftware.SyntaxEditor.CodeSnippetFolder m_codeSnippetFolder;
        #endregion

        #region Properties
        [XmlIgnore]
        public string Language
        {
            get
            {
                return m_language;
            }
        }

        [XmlIgnore]
        public ActiproSoftware.SyntaxEditor.CodeSnippetFolder CodeSnippetFolder
        {
            get
            {
                return m_codeSnippetFolder;
            }
        }

        [XmlIgnore]
        public string CodeSnippetPath
        {
            get
            {
                return ApplicationSettings.ConcatenatePathsString( new List<string>( this.CodeSnippetPaths ), substPaths: false );
            }
            set
            {
                if ( value == null )
                {
                    this.CodeSnippetPaths = null;
                }
                else
                {
                    this.CodeSnippetPaths = ApplicationSettings.SplitPathsString( value, reverseSubstPaths: false ).ToArray();
                }
            }
        }

        [XmlArray( "CodeSnippetPaths" )]
        public string[] CodeSnippetPaths
        {
            get
            {
                return m_codeSnippetPaths.ToArray();
            }
            set
            {
                m_codeSnippetPaths.Clear();
                m_codeSnippetFolder = null;

                if ( value != null )
                {
                    m_codeSnippetPaths.AddRange( value );

                    foreach ( string snippetPath in m_codeSnippetPaths )
                    {
                        try
                        {
                            if ( m_codeSnippetFolder == null )
                            {
                                m_codeSnippetFolder = new CodeSnippetFolder( Path.GetFileName( snippetPath ), snippetPath );
                            }
                            else
                            {
                                CodeSnippetFolder folder = new CodeSnippetFolder( Path.GetFileName( snippetPath ), snippetPath );
                                m_codeSnippetFolder.Merge( folder, true );
                            }
                        }
                        catch ( System.Exception e )
                        {
                            // don't load folder if error
                            ApplicationSettings.Log.ToolException(e, "Code snippet folder creation failed");
                        }
                    }
                }
            }
        }

        public string CacheDirectory
        {
            get
            {
                return m_cacheDirectory;
            }
            set
            {
                // Backwards compatibility -- Previously the entire path was stored here, now we construct it based on the project name
                // This prevents the situation where we have the cache in a project directory mismatched with the current project.
                if (value.StartsWith(DefaultCacheDirectory, StringComparison.OrdinalIgnoreCase))
                {
                    m_cacheDirectory = DefaultCacheDirectory;
                }
                else
                {
                    m_cacheDirectory = value;
                }
            }
        }

        private String DefaultCacheDirectory
        {
            get
            {
                return Path.Combine(Path.GetTempPath(), $"{m_language}IdentifierCache");
            }
        }
        #endregion

        #region Overrides
        public override SettingsBase Clone()
        {
            LanguageSettings l = new LanguageSettings( this.Language );
            l.Copy( this );

            return l;
        }

        public override void Copy( SettingsBase s )
        {
            if ( s is LanguageSettings )
            {
                Copy( s as LanguageSettings );
            }
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new LanguageSettingsControl();
        }

        public override bool Equals( object obj )
        {
            LanguageSettings l = obj as LanguageSettings;
            return this.Equals(l);
        }

        public override int GetHashCode()
        {
            StringBuilder s = new StringBuilder();
            s.Append( this.CacheDirectory.ToLower() );
            s.Append( this.CodeSnippetPath.ToLower() );
            s.Append( this.Language );
            s.Append( this.Name );

            return s.ToString().GetHashCode();
        }

        public override void Reset()
        {
            this.CodeSnippetPath = string.Empty;
            m_cacheDirectory = DefaultCacheDirectory;
        }

        public override bool Validate( out string error )
        {
            if ( !String.IsNullOrEmpty( this.CacheDirectory ) )
            {
                if (!ApplicationSettings.IsPathValid(this.CacheDirectory))
                {
                    error = $"The Cache Directory '{this.CacheDirectory} is not a valid path.";
                    return false;
                }
            }

            foreach ( string path in this.CodeSnippetPaths )
            {
                if (!ApplicationSettings.IsPathValid(path))
                {
                    error = $"The Code Snippet Directory '{path}' is not a valid path.";
                    return false;
                }

                if ( !Directory.Exists( path ) )
                {
                    error = $"The Code Snippet Directory '{path}' does not exist.";
                    return false;
                }
            }

            error = null;
            return true;
        }
        #endregion

        #region Virtual Functions
        /// <summary>
        /// Gets a list of items to add to the Help menu for launching help file(s).  Return an empty list or <c>null</c> if there aren't any.
        /// </summary>
        /// <returns>List of <see cref="ToolStripMenuItem"/>s.  Can be <c>null</c> or an empty list.</returns>
        public virtual List<ToolStripMenuItem> GetHelpMenuItems()
        {
            return null;
        }

        public virtual void Upgrade( LanguageTabSettings l )
        {
            if ( l == null )
            {
                return;
            }

            this.CacheDirectory = l.CacheDirectory;
            this.CodeSnippetPath = l.CodeSnippetPath;
        }
        #endregion

        #region Public Virtual Functions
        public virtual void Copy( LanguageSettings l )
        {
            if ( l == null )
            {
                return;
            }

            this.CacheDirectory = l.CacheDirectory;
            this.CodeSnippetPath = l.CodeSnippetPath;
        }

        public virtual bool Equals( LanguageSettings l )
        {
            if ( l == null )
            {
                return false;
            }

            return String.Equals(this.CacheDirectory, l.CacheDirectory, StringComparison.OrdinalIgnoreCase)
                && String.Equals(this.CodeSnippetPath, l.CodeSnippetPath, StringComparison.OrdinalIgnoreCase)
                && this.Language == l.Language
                && base.Equals(l);
        }
        #endregion
    }
    #endregion

    #region SourceControl Settings
    /// <summary>
    /// Kept for backwards compatibility
    /// </summary>
    public class SourceControlTabSettings : TabSettingsBase
    {
        public SourceControlTabSettings()
            : base( "Source Control" )
        {
            Reset();
        }

        #region Enums
        [XmlType( "SourceControlTabSettings.Provider" )]
        public enum Provider
        {
            Alienbrain,
            Perforce
        }

        [XmlType( "SourceControlTabSettings.EditAction" )]
		public enum EditAction
		{
			Prompt,
			CheckOut,
			DoNothing
		};

        [XmlType( "SourceControlTabSettings.SaveAction" )]
		public enum SaveAction
		{
			Prompt,
			CheckOut,
			SaveAs
		};

        [XmlType( "SourceControlTabSettings.CompileAction" )]
		public enum CompileAction
		{
			Prompt,
			CheckOut,
			Overwrite
		};
        #endregion

        #region Variables
        private bool m_enabled;
        private string m_server;
        private string m_project;
        private string m_loginName;
        private Provider m_sourceControlProvider;
        private EditAction m_onEdit;
        private SaveAction m_onSave;
        private CompileAction m_onCompile;
        #endregion

        #region Properties
        public bool Enabled
        {
            get
            {
                return m_enabled;
            }
            set
            {
                m_enabled = value;
            }
        }

        public string Server
        {
            get
            {
                return m_server;
            }
            set
            {
                m_server = value;
            }
        }

        public string Project
        {
            get
            {
                return m_project;
            }
            set
            {
                m_project = value;
            }
        }

		public string LoginName
        {
            get
            {
                return m_loginName;
            }
            set
            {
                m_loginName = value;
            }
        }

        public Provider SourceControlProvider
        {
            get
            {
                return m_sourceControlProvider;
            }
            set
            {
                m_sourceControlProvider = value;
            }
        }

		public EditAction OnEdit
        {
            get
            {
                return m_onEdit;
            }
            set
            {
                m_onEdit = value;
            }
        }

		public SaveAction OnSave
        {
            get
            {
                return m_onSave;
            }
            set
            {
                m_onSave = value;
            }
        }

        public CompileAction OnCompile
        {
            get
            {
                return m_onCompile;
            }
            set
            {
                m_onCompile = value;
            }
        }
        #endregion

        #region Overrides
        public override TabSettingsBase Clone()
        {
            SourceControlTabSettings s = new SourceControlTabSettings();
            s.Copy( this );

            return s;
        }

        public override void Copy( TabSettingsBase t )
        {
            if ( t is SourceControlTabSettings )
            {
                Copy( t as SourceControlTabSettings );
            }
        }

        public override bool Equals( System.Object obj )
        {
            // If parameter cannot be cast to Point return false.
            SourceControlTabSettings s = obj as SourceControlTabSettings;
            if ( (System.Object)s == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (this.SourceControlProvider == s.SourceControlProvider)
                && (this.Enabled == s.Enabled)
                && (this.Server == s.Server)
                && (this.Project == s.Project)
                && (this.LoginName == s.LoginName)
                && (this.OnEdit == s.OnEdit)
                && (this.OnSave == s.OnSave)
                && (this.OnCompile == s.OnCompile)
                && base.Equals( s );
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.SourceControlProvider.ToString() );
            s.Append( this.Enabled.ToString() );
            s.Append( this.Server );
            s.Append( this.Project );
            s.Append( this.LoginName );
            s.Append( this.OnEdit.ToString() );
            s.Append( this.OnSave.ToString() );
            s.Append( this.OnCompile.ToString() );
            s.Append( this.Name );
            return s.ToString().GetHashCode();
        }

        public override void Reset()
        {
            this.SourceControlProvider = Provider.Alienbrain;
            this.Enabled = false;
            this.Server = string.Empty;
            this.Project = string.Empty;
            this.LoginName = "ReadOnly";
            this.OnEdit = EditAction.DoNothing;
            this.OnSave = SaveAction.Prompt;
            this.OnCompile = CompileAction.Overwrite;
        }
        #endregion

        #region Public Functions
        public void Copy( SourceControlTabSettings s )
        {
            if ( s == null )
            {
                return;
            }

            this.SourceControlProvider = s.SourceControlProvider;
            this.Enabled = s.Enabled;
            this.Server = s.Server;
            this.Project = s.Project;
            this.LoginName = s.LoginName;
            this.OnEdit = s.OnEdit;
            this.OnSave = s.OnSave;
            this.OnCompile = s.OnCompile;
        }

        public bool Equals( SourceControlTabSettings s )
        {
            // If parameter is null return false:
            if ( (object)s == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (SourceControlProvider == s.SourceControlProvider)
                && (Enabled == s.Enabled)
                && (Server == s.Server)
                && (Project == s.Project)
                && (LoginName == s.LoginName)
                && (OnEdit == s.OnEdit)
                && (OnSave == s.OnSave)
                && (OnCompile == s.OnCompile)
                && base.Equals( s );
        }

		public string GetSourceControlFileName( string filename, bool fullname )
		{
            if ( SourceControlProvider == Provider.Perforce )
            {
                return filename;
            }
            else if ( (Server == string.Empty) || (Project == string.Empty) )
			{
				return string.Empty;
			}

			// Example: alienbrain://SANRAGE/rage/assets/HDRScene/entity.type

			System.Text.StringBuilder abFileName = new System.Text.StringBuilder();

			if ( fullname )
			{
				abFileName.Append( "alienbrain://" );
				abFileName.Append( Server );
				abFileName.Append( "/" );
			}

			abFileName.Append( Project );
			abFileName.Append( "/" );

			int indexOf = filename.IndexOf( Project );
			abFileName.Append( filename.Substring(indexOf+Project.Length+1) );

			return abFileName.ToString().Replace( @"\", "/" );
        }

        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
		public bool LoadXml( XmlTextReader reader )
		{
			string parentName = reader.Name;

			Reset();

			string enable = reader["enabled"];
			if ( enable != null )
			{
				Enabled = Convert.ToBoolean( enable );
			}

            string provider = reader["Provider"];
            if ( provider != null )
            {
                provider = provider.ToLower();
                if ( provider == "alienbrain" )
                {
                    SourceControlProvider = Provider.Alienbrain;
                }
                else if ( provider == "perforce" )
                {
                    SourceControlProvider = Provider.Perforce;
                }
            }

			string edit = reader["editAction"];
			if ( edit != null )
			{
				edit = edit.ToLower();
				if ( edit == "prompt" )
				{
					OnEdit = EditAction.Prompt;
				}
				else if ( edit == "checkout" )
				{
					OnEdit = EditAction.CheckOut;
				}
				else if ( edit == "donothing" )
				{
					OnEdit = EditAction.DoNothing;
				}
			}

			string save = reader["saveAction"];
			if ( save != null )
			{
				save = save.ToLower();
				if ( save == "prompt" )
				{
					OnSave = SaveAction.Prompt;
				}
				else if ( save == "checkout" )
				{
					OnSave = SaveAction.CheckOut;
				}
				else if ( save == "saveas" )
				{
					OnSave = SaveAction.SaveAs;
				}
			}

			string compile = reader["compileAction"];
			if ( compile != null )
			{
				compile = compile.ToLower();
				if ( compile == "prompt" )
				{
					OnCompile = CompileAction.Prompt;
				}
				else if ( compile == "checkout" )
				{
					OnCompile = CompileAction.CheckOut;
				}
				else if ( compile == "overwrite" )
				{
					OnCompile = CompileAction.Overwrite;
				}
			}

			while ( reader.Read() )
			{
				if ( !reader.IsStartElement() || reader.IsEmptyElement )
				{
					if ( reader.Name == parentName )
					{
						break; // we're done
					}
				}
				else if ( reader.Name == "Server" )
				{
					Server = reader.ReadString();
				}
				else if ( reader.Name == "Project" )
				{
					Project = reader.ReadString();
				}
				else if ( reader.Name == "LoginName" )
				{
					LoginName = reader.ReadString();
				}
			}

			return true;
        }
        #endregion
    }

    public class SourceControlSettings : SettingsBase
    {
        public SourceControlSettings()
            : base( "Source Control" )
        {
            Reset();
        }

        #region Enums
        public enum Provider
        {
            Alienbrain,
            Perforce
        }

        public enum EditAction
        {
            Prompt,
            CheckOut,
            DoNothing
        };

        public enum SaveAction
        {
            Prompt,
            CheckOut,
            SaveAs
        };

        public enum CompileAction
        {
            Prompt,
            CheckOut,
            Overwrite
        };
        #endregion

        #region Variables
        private bool m_enabled;
        private string m_server;
        private string m_project;
        private string m_loginName;
        private Provider m_sourceControlProvider;
        private EditAction m_onEdit;
        private SaveAction m_onSave;
        private CompileAction m_onCompile;
        #endregion

        #region Properties
        public bool Enabled
        {
            get
            {
                return m_enabled;
            }
            set
            {
                m_enabled = value;
            }
        }

        public string Server
        {
            get
            {
                return m_server;
            }
            set
            {
                m_server = value;
            }
        }

        public string Project
        {
            get
            {
                return m_project;
            }
            set
            {
                m_project = value;
            }
        }

        public string LoginName
        {
            get
            {
                return m_loginName;
            }
            set
            {
                m_loginName = value;
            }
        }

        public Provider SourceControlProvider
        {
            get
            {
                return m_sourceControlProvider;
            }
            set
            {
                m_sourceControlProvider = value;
            }
        }

        public EditAction OnEdit
        {
            get
            {
                return m_onEdit;
            }
            set
            {
                m_onEdit = value;
            }
        }

        public SaveAction OnSave
        {
            get
            {
                return m_onSave;
            }
            set
            {
                m_onSave = value;
            }
        }

        public CompileAction OnCompile
        {
            get
            {
                return m_onCompile;
            }
            set
            {
                m_onCompile = value;
            }
        }
        #endregion

        #region Overrides
        public override SettingsBase Clone()
        {
            SourceControlSettings s = new SourceControlSettings();
            s.Copy( this );

            return s;
        }

        public override void Copy( SettingsBase s )
        {
            if ( s is SourceControlSettings )
            {
                Copy( s as SourceControlSettings );
            }
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return null; // no longer supported in the UI
        }

        public override bool Equals( System.Object obj )
        {
            // If parameter cannot be cast to Point return false.
            SourceControlSettings s = obj as SourceControlSettings;
            if ( (System.Object)s == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (this.SourceControlProvider == s.SourceControlProvider)
                && (this.Enabled == s.Enabled)
                && (this.Server == s.Server)
                && (this.Project == s.Project)
                && (this.LoginName == s.LoginName)
                && (this.OnEdit == s.OnEdit)
                && (this.OnSave == s.OnSave)
                && (this.OnCompile == s.OnCompile)
                && base.Equals( s );
        }

        public override int GetHashCode()
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            s.Append( this.SourceControlProvider.ToString() );
            s.Append( this.Enabled.ToString() );
            s.Append( this.Server );
            s.Append( this.Project );
            s.Append( this.LoginName );
            s.Append( this.OnEdit.ToString() );
            s.Append( this.OnSave.ToString() );
            s.Append( this.OnCompile.ToString() );
            s.Append( this.Name );
            return s.ToString().GetHashCode();
        }

        public override void Reset()
        {
            this.SourceControlProvider = Provider.Alienbrain;
            this.Enabled = false;
            this.Server = string.Empty;
            this.Project = string.Empty;
            this.LoginName = "ReadOnly";
            this.OnEdit = EditAction.DoNothing;
            this.OnSave = SaveAction.Prompt;
            this.OnCompile = CompileAction.Overwrite;
        }
        #endregion

        #region Public Functions
        public void Copy( SourceControlSettings s )
        {
            if ( s == null )
            {
                return;
            }

            this.SourceControlProvider = s.SourceControlProvider;
            this.Enabled = s.Enabled;
            this.Server = s.Server;
            this.Project = s.Project;
            this.LoginName = s.LoginName;
            this.OnEdit = s.OnEdit;
            this.OnSave = s.OnSave;
            this.OnCompile = s.OnCompile;
        }

        public bool Equals( SourceControlSettings s )
        {
            // If parameter is null return false:
            if ( (object)s == null )
            {
                return false;
            }

            // Return true if the fields match:
            return (SourceControlProvider == s.SourceControlProvider)
                && (Enabled == s.Enabled)
                && (Server == s.Server)
                && (Project == s.Project)
                && (LoginName == s.LoginName)
                && (OnEdit == s.OnEdit)
                && (OnSave == s.OnSave)
                && (OnCompile == s.OnCompile)
                && base.Equals( s );
        }

        public string GetSourceControlFileName( string filename, bool fullname )
        {
            if ( SourceControlProvider == Provider.Perforce )
            {
                return filename;
            }
            else if ( (Server == string.Empty) || (Project == string.Empty) )
            {
                return string.Empty;
            }

            // Example: alienbrain://SANRAGE/rage/assets/HDRScene/entity.type

            System.Text.StringBuilder abFileName = new System.Text.StringBuilder();

            if ( fullname )
            {
                abFileName.Append( "alienbrain://" );
                abFileName.Append( Server );
                abFileName.Append( "/" );
            }

            abFileName.Append( Project );
            abFileName.Append( "/" );

            int indexOf = filename.IndexOf( Project );
            abFileName.Append( filename.Substring( indexOf + Project.Length + 1 ) );

            return abFileName.ToString().Replace( @"\", "/" );
        }

        public void Upgrade( SourceControlTabSettings oldSettings )
        {
            if ( oldSettings == null )
            {
                return;
            }

            this.Enabled = oldSettings.Enabled;
            this.LoginName = oldSettings.LoginName;
            this.OnCompile = (CompileAction)oldSettings.OnCompile;
            this.OnEdit = (EditAction)oldSettings.OnEdit;
            this.OnSave = (SaveAction)oldSettings.OnSave;
            this.Project = oldSettings.Project;
            this.Server = oldSettings.Server;
            this.SourceControlProvider = (Provider)oldSettings.SourceControlProvider;
        }
        #endregion
    }

    public class PlatformSettings : SettingsBase
    {
        private ISet<Platform> _platforms = new HashSet<Platform>();
        private Platform _currentPlatform = Platform.Independent;

        public PlatformSettings()
            : base( "Platforms" )
        {
            Reset();
        }

        [XmlIgnore]
        public ISet<Platform> Platforms
        {
            get { return _platforms; }
        }

        // Note: stored as String so that we can persist settings file across project boundaries and discard
        // unknown platforms without crashing.
        [XmlArray("Platforms")]
        [XmlArrayItem("Platform")]
        public string[] XmlPlatforms
        {
            get { return _platforms.Select(p => p.ToString()).ToArray(); }
            set
            {
                _platforms.Clear();
                foreach (string platStr in value)
                {
                    Platform platform;
                    if (Enum.TryParse(platStr, out platform))
                    {
                        this._platforms.Add(platform);
                    }
                }
            }
        }

        #region Overrides
        public override SettingsBase Clone()
        {
            PlatformSettings s = new PlatformSettings();
            s.Copy(this);
            return s;
        }

        public override void Copy(SettingsBase t)
        {
            if (t == null)
            {
                return;
            }

            PlatformSettings s = t as PlatformSettings;
            if (s == null)
            {
                return;
            }

            Copy(s);
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new PlatformSettingsControl();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            PlatformSettings s = obj as PlatformSettings;
            if (s == null)
            {
                return false;
            }

            return Equals(s);
        }

        public override int GetHashCode()
        {
            return String.Format("{0}:{1}", _currentPlatform, String.Join(",", _platforms)).GetHashCode();
        }

        public override void Reset()
        {
            this.Platforms.Clear();
            foreach (Platform platform in Enum.GetValues(typeof(Platform)))
            {
                if (platform != Platform.Independent)
                {
                    this.Platforms.Add(platform);
                }
            }
        }

        public override bool Validate(out string error)
        {
            if (!this.Platforms.Any())
            {
                error = "At least one platform must be selected";
            }
            else
            {
                error = "";
            }
            return String.IsNullOrEmpty(error);
        }
        #endregion

        #region Public Functions
        public void Copy(PlatformSettings s)
        {
            this.Platforms.Clear();
            this.Platforms.AddRange(s.Platforms);
        }

        public bool Equals(PlatformSettings s)
        {
            if (s == null)
            {
                return false;
            }

            return this.Platforms.Count == s.Platforms.Count &&
                !this.Platforms.Except(s.Platforms).Any();
        }
        #endregion
    }

    public class SourceControlSettings2 : SettingsBase
    {
        public SourceControlSettings2()
            : base( "Source Control" )
        {
            Reset();
        }

        #region Enums
        [XmlType( "SourceControlSettings2.EditAction" )]    // so the XmlSerializer can tell the difference between these and those that belong to SourceControlSettings.
        public enum EditAction
        {
            Prompt,
            CheckOut,
            DoNothing
        };

        [XmlType( "SourceControlSettings2.SaveAction" )]    // so the XmlSerializer can tell the difference between these and those that belong to SourceControlSettings.
        public enum SaveAction
        {
            Prompt,
            CheckOut,
            SaveAs
        };

        [XmlType( "SourceControlSettings2.CompileAction" )]    // so the XmlSerializer can tell the difference between these and those that belong to SourceControlSettings.
        public enum CompileAction
        {
            Prompt,
            CheckOut,
            Overwrite
        };
        #endregion

        #region Variables
        private rageSourceControlProviderSettings m_sourceControlProviderSettings = new rageSourceControlProviderSettings();
        private EditAction m_onEdit;
        private SaveAction m_onSave;
        private CompileAction m_onCompile;
        #endregion

        #region Properties
        [XmlIgnore]
        public bool Enabled
        {
            get
            {
                return m_sourceControlProviderSettings.Enabled;
            }
            set
            {
                m_sourceControlProviderSettings.Enabled = value;
            }
        }

        [XmlIgnore]
        public string Server
        {
            get
            {
                rageSourceControlProvider provider = m_sourceControlProviderSettings.SelectedProvider;
                if ( provider != null )
                {
                    return provider.ServerOrPort;
                }

                return string.Empty;
            }
        }

        [XmlIgnore]
        public string Project
        {
            get
            {
                rageSourceControlProvider provider = m_sourceControlProviderSettings.SelectedProvider;
                if ( provider != null )
                {
                    return provider.ProjectOrWorkspace;
                }

                return string.Empty;
            }
        }

        [XmlIgnore]
        public string LoginName
        {
            get
            {
                rageSourceControlProvider provider = m_sourceControlProviderSettings.SelectedProvider;
                if ( provider != null )
                {
                    return provider.LoginOrUsername;
                }

                return string.Empty;
            }
        }

        [XmlIgnore]
        public rageSourceControlProvider SourceControlProvider
        {
            get
            {
                return m_sourceControlProviderSettings.SelectedProvider;
            }
        }

        public EditAction OnEdit
        {
            get
            {
                return m_onEdit;
            }
            set
            {
                m_onEdit = value;
            }
        }

        public SaveAction OnSave
        {
            get
            {
                return m_onSave;
            }
            set
            {
                m_onSave = value;
            }
        }

        public CompileAction OnCompile
        {
            get
            {
                return m_onCompile;
            }
            set
            {
                m_onCompile = value;
            }
        }

        public rageSourceControlProviderSettings SourceControlProviderSettings
        {
            get
            {
                return m_sourceControlProviderSettings;
            }
            set
            {
                m_sourceControlProviderSettings = value;
            }
        }
        #endregion

        #region Overrides
        public override SettingsBase Clone()
        {
            SourceControlSettings2 s = new SourceControlSettings2();
            s.Copy( this );
            return s;
        }

        public override void Copy( SettingsBase t )
        {
            if ( t == null )
            {
                return;
            }

            SourceControlSettings2 s = t as SourceControlSettings2;
            if ( s == null )
            {
                return;
            }

            Copy( s );
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new SourceControlSettingsControl();
        }

        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            SourceControlSettings2 s = obj as SourceControlSettings2;
            if ( s == null )
            {
                return false;
            }

            return Equals( s );
        }

        public override int GetHashCode()
        {
            StringBuilder s = new StringBuilder();
            s.Append( this.OnCompile.ToString() );
            s.Append( this.OnEdit.ToString() );
            s.Append( this.OnSave.ToString() );
            s.Append( this.SourceControlProviderSettings.GetHashCode() );
            return s.ToString().GetHashCode();
        }

        public override void Reset()
        {
            this.Enabled = false;

            List<rageSourceControlProvider> providers = this.SourceControlProviderSettings.Providers;
            foreach ( rageSourceControlProvider provider in providers )
            {
                provider.LoginOrUsername = string.Empty;
                provider.ProjectOrWorkspace = string.Empty;
                provider.ServerOrPort = string.Empty;
            }

            this.OnCompile = CompileAction.CheckOut;
            this.OnEdit = EditAction.CheckOut;
            this.OnSave = SaveAction.Prompt;
        }

        public override bool Validate( out string error )
        {
            rageStatus status;
            this.SourceControlProviderSettings.Validate( out status );
            error = status.ErrorString;
            return status.Success();
        }
        #endregion

        #region Public Functions
        public void Copy( SourceControlSettings2 s )
        {
            this.OnCompile = s.OnCompile;
            this.OnEdit = s.OnEdit;
            this.OnSave = s.OnSave;
            this.SourceControlProviderSettings.CopyFrom( s.SourceControlProviderSettings );
        }

        public bool Equals( SourceControlSettings2 s )
        {
            if ( s == null )
            {
                return false;
            }

            return (this.OnCompile == s.OnCompile)
                && (this.OnEdit == s.OnEdit)
                && (this.OnSave == s.OnSave)
                && (this.SourceControlProviderSettings == s.SourceControlProviderSettings);
        }

        public void Upgrade( SourceControlSettings oldSettings )
        {
            this.Enabled = oldSettings.Enabled;
            this.OnCompile = (SourceControlSettings2.CompileAction)oldSettings.OnCompile;
            this.OnEdit = (SourceControlSettings2.EditAction)oldSettings.OnEdit;
            this.OnSave = (SourceControlSettings2.SaveAction)oldSettings.OnSave;

            List<rageSourceControlProvider> providers = this.SourceControlProviderSettings.Providers;
            int selectedIndex = -1;
            switch ( oldSettings.SourceControlProvider )
            {
                case SourceControlSettings.Provider.Perforce:
                    {
                        for ( int i = 0; i < providers.Count; ++i )
                        {
                            if ( providers[i] is ragePerforceSourceControlProvider )
                            {
                                selectedIndex = i;
                                break;
                            }
                        }
                    }
                    break;
            }

            if ( selectedIndex != -1 )
            {
                this.SourceControlProviderSettings.SelectedIndex = selectedIndex;

                this.SourceControlProvider.LoginOrUsername = oldSettings.LoginName;
                this.SourceControlProvider.ProjectOrWorkspace = oldSettings.Project;
                this.SourceControlProvider.ServerOrPort = oldSettings.Server;
            }
            else
            {
                this.SourceControlProviderSettings.SelectedIndex = 0;
            }
        }
        #endregion
    }
    #endregion
    #endregion

    #region LanguageHighlightingStylesSettings
    public class LanguageHighlightingStylesSettings
	{
		public LanguageHighlightingStylesSettings()
		{
			Reset();
        }

        #region Variables
        private Font m_font;
        private Color m_textBackgroundColor;
        private Dictionary<string, LanguageHighlightingStyle> m_languageStyles = new Dictionary<string, LanguageHighlightingStyle>();
        #endregion

        #region Properties
        [XmlIgnore]
        public Font Font
        {
            get
            {
                return m_font;
            }
            set
            {
                if (m_font != null)
                {
                    m_font.Dispose();
                }
                m_font = value.Clone() as Font;
            }
        }

        [XmlElement( "Font" )]
        public XmlFont XmlFont
        {
            get
            {
                return new XmlFont( m_font );
            }
            set
            {
                if (m_font != null)
                {
                    m_font.Dispose();
                }
                m_font = value.ToFont();
            }
        }

        [XmlIgnore]
        public Color TextBackgroundColor
        {
            get
            {
                return m_textBackgroundColor;
            }
            set
            {
                m_textBackgroundColor = value;
            }
        }

        [XmlElement( "TextBackgroundColor" )]
        public string XmlTextBackgroundColor
        {
            get
            {
                return ApplicationSettings.SerializeColor( m_textBackgroundColor );
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ) )
                {
                    m_textBackgroundColor = ApplicationSettings.DeserializeColor( value );
                }
                else
                {
                    m_textBackgroundColor = Color.Empty;
                }
            }
        }

        [XmlIgnore]
        public Dictionary<string, LanguageHighlightingStyle> LanguageHighlightingStyles
        {
            get
            {
                return m_languageStyles;
            }
        }

        [XmlArray( "LanguageHighlightingStyles" )]
        public LanguageHighlightingStyle[] XmlLanguageHighlightingStyles
        {
            get
            {
                List<LanguageHighlightingStyle> langStyles = new List<LanguageHighlightingStyle>();
                foreach ( KeyValuePair<string, LanguageHighlightingStyle> pair in m_languageStyles )
                {
                    langStyles.Add( pair.Value );
                }

                return langStyles.ToArray();
            }
            set
            {
                m_languageStyles.Clear();
                if ( value != null )
                {
                    foreach ( LanguageHighlightingStyle langStyle in value )
                    {
                        m_languageStyles.Add( langStyle.LanguageKey, langStyle );
                    }
                }
            }
        }
        #endregion

        #region Save/Load
        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
		public bool LoadXml( XmlTextReader reader )
		{
			string parentName = reader.Name;

			Reset();

			if ( reader.IsEmptyElement )
			{
				return true;
			}

            string key = reader["key"];

            string mainFont = reader["font"];
            string mainSize = reader["size"];
            if ( (mainFont != null) && (mainSize != null) )
            {
                Font = new Font( mainFont, float.Parse( mainSize ) );
            }

            string color = reader["backgroundColor"];
            if ( color != null )
            {
                TextBackgroundColor = ApplicationSettings.DeserializeColor( color );
            }

			// deserialize text styles:
			while ( reader.Read() )
			{
				if ( !reader.IsStartElement() )
				{
					if ( reader.Name == parentName )
					{
						break;	// we're done
					}
				}
				else if ( reader.Name == "Styles" )
				{
					if ( reader["key"] != null )
					{
						key = reader["key"];
					}

					if ( key != null )
					{
						LanguageHighlightingStyle langStyle = new LanguageHighlightingStyle();
						langStyle.LanguageKey = key;

						// read language styles:
						while ( reader.Read() )
						{
							if ( !reader.IsStartElement() )
							{
								if ( reader.Name == "Styles" )
								{
									break; // we're done
								}
							}
							else if ( reader.Name == "Style" )
							{
								// highlighting style editor will complain if I don't don't and in 255 alpha:
                                string styleKey = reader["key"];
                                string name = reader["name"];
                                Color foreColor = ApplicationSettings.DeserializeColor( reader["foreColor"] );
                                Color backColor = ApplicationSettings.DeserializeColor( reader["backColor"] );
                                ActiproSoftware.ComponentModel.DefaultableBoolean bold = (reader["bold"] == bool.TrueString) ?
                                    ActiproSoftware.ComponentModel.DefaultableBoolean.True
                                    : ActiproSoftware.ComponentModel.DefaultableBoolean.False;
                                ActiproSoftware.ComponentModel.DefaultableBoolean italic = (reader["italic"] == bool.TrueString) ?
                                    ActiproSoftware.ComponentModel.DefaultableBoolean.True
                                    : ActiproSoftware.ComponentModel.DefaultableBoolean.False;
                                ActiproSoftware.SyntaxEditor.HighlightingStyleLineStyle underline = (reader["underline"] == bool.TrueString) ?
                                    ActiproSoftware.SyntaxEditor.HighlightingStyleLineStyle.Solid
                                    : ActiproSoftware.SyntaxEditor.HighlightingStyleLineStyle.Default;

                                langStyle.HighlightingStyles.Add( new HighlightingStyle( styleKey, name, foreColor, backColor, bold, italic, underline ) );
							}
						}

                        if ( !LanguageHighlightingStyles.ContainsKey( key ) )
                        {
                            LanguageHighlightingStyles.Add( key, langStyle );
                        }
					}
				}
			}

			return true;
        }
        #endregion

        #region Misc
        public void Reset()
        {
            Font = new Font( "Courier New", 10.0f );
            TextBackgroundColor = SystemColors.Window;
            LanguageHighlightingStyles.Clear();
        }

        public void Copy( LanguageHighlightingStylesSettings h )
        {
            if ( h == null )
            {
                return;
            }

            this.Font = h.Font;
            this.TextBackgroundColor = h.TextBackgroundColor;


            this.LanguageHighlightingStyles.Clear();
            foreach ( KeyValuePair<string, LanguageHighlightingStyle> pair in h.LanguageHighlightingStyles )
            {
                LanguageHighlightingStyle langStyle = new LanguageHighlightingStyle();
                langStyle.Copy( pair.Value );

                this.LanguageHighlightingStyles.Add( pair.Key, langStyle );
            }
        }
        #endregion
    }

    public class XmlFont
    {
        public XmlFont()
        {

        }

        public XmlFont( Font f )
        {
            this.FontFamily = f.FontFamily.Name;
            this.GraphicsUnit = f.Unit;
            this.Size = f.Size;
            this.Style = f.Style;
        }

        #region Properties
        public string FontFamily;
        public GraphicsUnit GraphicsUnit;
        public float Size;
        public FontStyle Style;
        #endregion

        public Font ToFont()
        {
            return new Font( this.FontFamily, this.Size, this.Style, this.GraphicsUnit );
        }
    }

    public class LanguageHighlightingStyle
    {
        public LanguageHighlightingStyle()
        {
        }

        #region Variables
        private string m_languageKey;
        private List<ActiproSoftware.SyntaxEditor.HighlightingStyle> m_styles = new List<ActiproSoftware.SyntaxEditor.HighlightingStyle>();
        #endregion

        #region Properties
        public string LanguageKey
        {
            get
            {
                return m_languageKey;
            }
            set
            {
                m_languageKey = value;
            }
        }

        [XmlIgnore]
        public List<ActiproSoftware.SyntaxEditor.HighlightingStyle> HighlightingStyles
        {
            get
            {
                return m_styles;
            }
        }

        [XmlArray( "HighlightingStyles" )]
        public XmlHighlightingStyle[] XmlHighlightingStyles
        {
            get
            {
                List<XmlHighlightingStyle> styles = new List<XmlHighlightingStyle>();

                if ( m_styles != null )
                {
                    foreach ( ActiproSoftware.SyntaxEditor.HighlightingStyle style in m_styles )
                    {
                        styles.Add( new XmlHighlightingStyle( style ) );
                    }
                }

                return styles.ToArray();
            }
            set
            {
                m_styles.Clear();

                if ( value != null )
                {
                    foreach ( XmlHighlightingStyle style in value )
                    {
                        m_styles.Add( style.ToHighlightingStyle() );
                    }
                }
            }
        }
        #endregion

        #region Misc
        public void Copy( LanguageHighlightingStyle h )
        {
            if ( h == null )
            {
                return;
            }

            this.LanguageKey = h.LanguageKey;

            this.HighlightingStyles.Clear();
            foreach ( ActiproSoftware.SyntaxEditor.HighlightingStyle style in h.HighlightingStyles )
            {
                this.HighlightingStyles.Add( style.Clone() );
            }
        }
        #endregion
    }

    public class XmlHighlightingStyle
    {
        public XmlHighlightingStyle()
        {

        }

        public XmlHighlightingStyle( ActiproSoftware.SyntaxEditor.HighlightingStyle s )
        {
            this.Key = s.Key;
            this.Name = s.Name;
            m_foreColor = s.ForeColor;
            m_backColor = s.BackColor;
            this.Bold = s.Bold == ActiproSoftware.ComponentModel.DefaultableBoolean.True;
            this.Italic = s.Italic == ActiproSoftware.ComponentModel.DefaultableBoolean.True;
            this.Underline = s.UnderlineStyle != HighlightingStyleLineStyle.Default;
        }

        #region Variables
        private Color m_foreColor;
        private Color m_backColor;
        #endregion

        #region Properties
        public string Key;
        public string Name;

        [XmlIgnore]
        public Color ForeColor
        {
            get
            {
                return m_foreColor;
            }
            set
            {
                m_foreColor = value;
            }
        }

        [XmlElement( "ForeColor" )]
        public string XmlForeColor
        {
            get
            {
                return ApplicationSettings.SerializeColor( m_foreColor );
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ) )
                {
                    m_foreColor = ApplicationSettings.DeserializeColor( value );
                }
                else
                {
                    m_foreColor = Color.Empty;
                }
            }
        }

        [XmlIgnore]
        public Color BackColor
        {
            get
            {
                return m_backColor;
            }
            set
            {
                m_backColor = value;
            }
        }

        [XmlElement( "BackColor" )]
        public string XmlBackColor
        {
            get
            {
                return ApplicationSettings.SerializeColor( m_backColor );
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ) )
                {
                    m_backColor = ApplicationSettings.DeserializeColor( value );
                }
                else
                {
                    m_backColor = Color.Empty;
                }
            }
        }

        public bool Bold;
        public bool Italic;
        public bool Underline;
        #endregion

        public ActiproSoftware.SyntaxEditor.HighlightingStyle ToHighlightingStyle()
        {
            return new ActiproSoftware.SyntaxEditor.HighlightingStyle( this.Key, this.Name, this.ForeColor, this.BackColor,
                this.Bold ? ActiproSoftware.ComponentModel.DefaultableBoolean.True : ActiproSoftware.ComponentModel.DefaultableBoolean.False,
                this.Italic ? ActiproSoftware.ComponentModel.DefaultableBoolean.True : ActiproSoftware.ComponentModel.DefaultableBoolean.False,
                this.Underline ? ActiproSoftware.SyntaxEditor.HighlightingStyleLineStyle.Solid : ActiproSoftware.SyntaxEditor.HighlightingStyleLineStyle.Default );
        }
    };

	public class TextBackgroundColorSettings
	{
		public TextBackgroundColorSettings()
		{
			Reset();
        }

        #region Variables
        private Color m_backgroundColor;
        #endregion

        #region Properties
        public Color BackgroundColor
        {
            get
            {
                return m_backgroundColor;
            }
            set
            {
                m_backgroundColor = value;
            }
        }
        #endregion

        #region Save/Load
        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
		public bool LoadXml( XmlTextReader reader )
		{
			Reset();

			string color = reader["color"];
			if ( color != null )
			{
                BackgroundColor = ApplicationSettings.DeserializeColor( color );
			}

			return true;
        }
        #endregion

        #region Misc
        public void Reset()
		{
			BackgroundColor = Color.White;
        }
        #endregion
    }
    #endregion

    #region TextEditorSettings
    public class TextEditorSettings
    {
        public TextEditorSettings()
        {
        }

        public TextEditorSettings( string filename, System.Guid guid )
        {
            m_filename = filename;
            m_guid = guid;
        }

        #region Variables
        private string m_filename;
        private System.Guid m_guid;
        private int m_horizontalSplitPosition = -1;
        private int m_verticalSplitPosition = -1;
        private int m_linePosition;
        private long m_fileDateTime = 0;
        private ActiproSoftware.SyntaxEditor.OutliningMode m_outliningMode = ActiproSoftware.SyntaxEditor.OutliningMode.None;
        private List<OutlineNodeInfo> m_outlineNodeInfo = new List<OutlineNodeInfo>();
        #endregion

        #region Properties
        public string Filename
        {
            get
            {
                return m_filename;
            }
            set
            {
                // For some reason our user settings file stored local paths. Expand this to the full branch if that's the case.
                m_filename = ApplicationSettings.PrependProjectRoot(value);
            }
        }

        public System.Guid Guid
        {
            get
            {
                return m_guid;
            }
            set
            {
                m_guid = value;
            }
        }

        public int HorizontalSplitPosition
        {
            get
            {
                return m_horizontalSplitPosition;
            }
            set
            {
                m_horizontalSplitPosition = value;
            }
        }

        public int VerticalSplitPosition
        {
            get
            {
                return m_verticalSplitPosition;
            }
            set
            {
                m_verticalSplitPosition = value;
            }
        }

        public int LinePosition
        {
            get
            {
                return m_linePosition;
            }
            set
            {
                m_linePosition = value;
            }
        }

        public ActiproSoftware.SyntaxEditor.OutliningMode OutliningMode
        {
            get
            {
                return m_outliningMode;
            }
            set
            {
                m_outliningMode = value;
            }
        }

        public long FileDateTime
        {
            get
            {
                return m_fileDateTime;
            }
            set
            {
                m_fileDateTime = value;
            }
        }

        [XmlIgnore]
        public List<OutlineNodeInfo> OutlineNodeInfos
        {
            get
            {
                return m_outlineNodeInfo;
            }
        }

        [XmlArray( "OutlineNodeInfos" )]
        public OutlineNodeInfo[] XmlOutlineNodeInfos
        {
            get
            {
                return m_outlineNodeInfo.ToArray();
            }
            set
            {
                m_outlineNodeInfo.Clear();

                if ( value != null )
                {
                    m_outlineNodeInfo.AddRange( value );
                }
            }
        }

        // Do not use. Kept for backwards compatibility with save data
        [XmlArray( "DocumentOutlineNodeInfos" )]
        public OutlineNodeInfo[] XmlDocumentOutlineNodeInfos
        {
            get
            {
                return new OutlineNodeInfo[0];
            }
            set
            {
                // Removed
            }
        }
        #endregion

        #region Public Functions
        public void Copy( TextEditorSettings t )
        {
            if ( t == null )
            {
                return;
            }

            this.Filename = t.Filename;
            this.Guid = t.Guid;
            this.HorizontalSplitPosition = t.HorizontalSplitPosition;
            this.LinePosition = t.LinePosition;
            this.VerticalSplitPosition = t.VerticalSplitPosition;
            this.FileDateTime = t.FileDateTime;
            this.OutliningMode = t.OutliningMode;

            this.OutlineNodeInfos.Clear();
            foreach ( OutlineNodeInfo o in t.OutlineNodeInfos )
            {
                OutlineNodeInfo copyO = new OutlineNodeInfo();
                copyO.Copy( o );
                this.OutlineNodeInfos.Add( copyO );
            }
        }

        public string Serialize()
        {
            string tempFilename = Path.GetTempFileName();

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( tempFilename );
                XmlSerializer serializer = new XmlSerializer( this.GetType() );
                serializer.Serialize( writer, this );

                writer.Close();
            }
            catch ( Exception e )
            {
                if ( writer != null )
                {
                    writer.Close();
                }

                ApplicationSettings.Log.ToolException(e, "TextEditorSettings Error serializing for '{0}' to tempfile '{0}'", this.Filename, tempFilename);
                File.Delete( tempFilename );
                return null;
            }

            string serializedText = null;
            TextReader reader = null;
            try
            {
                reader = new StreamReader( tempFilename );

                serializedText = reader.ReadToEnd();

                reader.Close();
            }
            catch ( Exception e )
            {
                if ( reader != null )
                {
                    reader.Close();
                }

                ApplicationSettings.Log.ToolException( e, "TextEditorSettings Error loading tempfile '{0}': {1}", tempFilename);
                File.Delete( tempFilename );
                return null;
            }

            File.Delete( tempFilename );
            return serializedText;
        }

        public bool Deserialize( string serializedText )
        {
            string tempFilename = Path.GetTempFileName();

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( tempFilename );

                writer.Write( serializedText );

                writer.Close();
            }
            catch ( Exception e )
            {
                if ( writer != null )
                {
                    writer.Close();
                }

                ApplicationSettings.Log.ToolException(e, "TextEditorSettings Error writing tempfile '{0}'", tempFilename);
                File.Delete( tempFilename );
                return false;
            }

            FileStream stream = null;
            try
            {
                stream = new FileStream( tempFilename, FileMode.Open, FileAccess.Read );

                XmlSerializer serializer = new XmlSerializer( this.GetType() );
                TextEditorSettings settings = serializer.Deserialize( stream ) as TextEditorSettings;

                stream.Close();

                if ( settings == null )
                {
                    ApplicationSettings.Log.Error( "TextEditorSettings unable to deserialize '{0}'", tempFilename );
                    File.Delete( tempFilename );
                    return false;
                }

                this.Copy( settings );
            }
            catch ( Exception e )
            {
                if ( stream != null )
                {
                    stream.Close();
                }

                ApplicationSettings.Log.ToolException(e, "TextEditorSettings Error deserializing '{0}'", tempFilename);
                File.Delete( tempFilename );
                return false;
            }

            File.Delete( tempFilename );
            return true;
        }
        #endregion
    }

    public class OutlineNodeInfo
    {
        public OutlineNodeInfo()
        {

        }

        public OutlineNodeInfo( int startOffset )
        {
            m_startOffset = startOffset;
        }

        public OutlineNodeInfo( int startOffset, int endOffset, bool expanded )
            : this( startOffset )
        {
            m_endOffset = endOffset;
            m_expanded = expanded;
        }

        #region Variables
        private int m_startOffset = -1;
        private int m_endOffset = -1;
        private bool m_expanded = false;
        #endregion

        #region Properties
        public int StartOffset
        {
            get
            {
                return m_startOffset;
            }
            set
            {
                m_startOffset = value;
            }
        }

        public int EndOffset
        {
            get
            {
                return m_endOffset;
            }
            set
            {
                m_endOffset = value;
            }
        }

        public bool Expanded
        {
            get
            {
                return m_expanded;
            }
            set
            {
                m_expanded = value;
            }
        }
        #endregion

        #region Public Functions
        public void Copy( OutlineNodeInfo o )
        {
            if ( o == null )
            {
                return;
            }

            this.EndOffset = o.EndOffset;
            this.Expanded = o.Expanded;
            this.StartOffset = o.StartOffset;
        }
        #endregion
    }
    #endregion

    #region FindReplace Settings
    #region Enums
    public enum FindReplaceSearchRange
    {
        CurrentDocument,
        AllOpenDocuments,
        EntireProject,
        CurrentSelection
    }
    #endregion

    public class FindReplaceSettings
    {
        public FindReplaceSettings()
        {
            Reset();
        }

        #region Variables
        private bool m_useNewDialog;
        private bool m_matchCase;
        private bool m_matchWholeWord;
        private bool m_searchUp;
        private bool m_useRegularExpressions;
        private bool m_useWildcards;
        private bool m_markWithBookmarks;
        private bool m_useWordWrapping;
        private FindReplaceSearchRange m_searchIn;

        private List<string> m_findHistory = new List<string>();
        private List<string> m_replaceHistory = new List<string>();
        #endregion

        #region Properties
        public bool UseNewDialog
        {
            get
            {
                return m_useNewDialog;
            }
            set
            {
                m_useNewDialog = value;
            }
        }

        public bool MatchCase
        {
            get
            {
                return m_matchCase;
            }
            set
            {
                m_matchCase = value;
            }
        }

        public bool MatchWholeWord
        {
            get
            {
                return m_matchWholeWord;
            }
            set
            {
                m_matchWholeWord = value;
            }
        }

        public bool SearchUp
        {
            get
            {
                return m_searchUp;
            }
            set
            {
                m_searchUp = value;
            }
        }

        public bool UseRegularExpressions
        {
            get
            {
                return m_useRegularExpressions;
            }
            set
            {
                m_useRegularExpressions = value;
            }
        }

        public bool UseWildcards
        {
            get
            {
                return m_useWildcards;
            }
            set
            {
                m_useWildcards = value;
            }
        }

        public bool MarkWithBookmarks
        {
            get
            {
                return m_markWithBookmarks;
            }
            set
            {
                m_markWithBookmarks = value;
            }
        }

        public FindReplaceSearchRange SearchIn
        {
            get
            {
                return m_searchIn;
            }
            set
            {
                m_searchIn = value;
            }
        }

        [XmlIgnore]
        public List<string> FindHistory
        {
            get
            {
                return m_findHistory;
            }
            set
            {
                m_findHistory = value;
            }
        }

        [XmlArray( "FindHistory" )]
        public string[] FindHistoryXml
        {
            get
            {
                return m_findHistory.ToArray();
            }
            set
            {
                m_findHistory.Clear();
                if ( value != null )
                {
                    m_findHistory.AddRange( value );
                }
            }
        }

        [XmlIgnore]
        public List<string> ReplaceHistory
        {
            get
            {
                return m_replaceHistory;
            }
            set
            {
                m_replaceHistory = value;
            }
        }

        [XmlArray( "ReplaceHistory" )]
        public string[] ReplaceHistoryXml
        {
            get
            {
                return m_replaceHistory.ToArray();
            }
            set
            {
                m_replaceHistory.Clear();
                if ( value != null )
                {
                    m_replaceHistory.AddRange( value );
                }
            }
        }

        public bool UseWordWrapping
        {
            get
            {
                return m_useWordWrapping;
            }
            set
            {
                m_useWordWrapping = value;
            }
        }

        #endregion

        #region Public Functions
        public void Reset()
        {
            m_useNewDialog = true;
            m_matchCase = false;
            m_matchWholeWord = false;
            m_searchUp = false;
            m_useRegularExpressions = false;
            m_useWildcards = false;
            m_markWithBookmarks = false;
            m_useWordWrapping = true;
            m_searchIn = FindReplaceSearchRange.CurrentDocument;
            m_findHistory.Clear();
            m_replaceHistory.Clear();
        }

        public void Copy( FindReplaceSettings f )
        {
            if ( f == null )
            {
                return;
            }

            this.MarkWithBookmarks = f.MarkWithBookmarks;
            this.MatchCase = f.MatchCase;
            this.MatchWholeWord = f.MatchWholeWord;
            this.SearchIn = f.SearchIn;
            this.SearchUp = f.SearchUp;
            this.UseNewDialog = f.UseNewDialog;
            this.UseRegularExpressions = f.UseRegularExpressions;
            this.UseWildcards = f.UseWildcards;
            this.UseWordWrapping = f.UseWordWrapping;

            this.FindHistory.Clear();
            this.FindHistory.AddRange( f.FindHistory );

            this.ReplaceHistory.Clear();
            this.ReplaceHistory.AddRange( f.ReplaceHistory );
        }
        #endregion
    }

    public class FindReplaceAllSettings
    {
        public FindReplaceAllSettings()
        {
            Reset();
        }

        #region Variables
        private bool m_useNewDialog;
        private bool m_matchCase;
        private bool m_matchWholeWord;
        private bool m_useRegularExpressions;
        private bool m_useWildcards;
        private bool m_openFilesAfterReplace;
        private string m_selectedPath;
        private bool m_searchSubFolders;
        private string m_selectedFileType;
        private List<string> m_lookInPaths = new List<string>();
        private List<string> m_fileTypes = new List<string>();

        private List<string> m_findHistory = new List<string>();
        private List<string> m_replaceHistory = new List<string>();
        #endregion

        #region Properties
        public bool UseNewDialog
        {
            get
            {
                return m_useNewDialog;
            }
            set
            {
                m_useNewDialog = value;
            }
        }

        public bool MatchCase
        {
            get
            {
                return m_matchCase;
            }
            set
            {
                m_matchCase = value;
            }
        }

        public bool MatchWholeWord
        {
            get
            {
                return m_matchWholeWord;
            }
            set
            {
                m_matchWholeWord = value;
            }
        }

        public bool UseRegularExpressions
        {
            get
            {
                return m_useRegularExpressions;
            }
            set
            {
                m_useRegularExpressions = value;
            }
        }

        public bool UseWildcards
        {
            get
            {
                return m_useWildcards;
            }
            set
            {
                m_useWildcards = value;
            }
        }

        public bool OpenFilesAfterReplace
        {
            get
            {
                return m_openFilesAfterReplace;
            }
            set
            {
                m_openFilesAfterReplace = value;
            }
        }

        public string SelectedPath
        {
            get
            {
                return m_selectedPath;
            }
            set
            {
                m_selectedPath = value;
            }
        }

        public bool SearchSubFolders
        {
            get
            {
                return m_searchSubFolders;
            }
            set
            {
                m_searchSubFolders = value;
            }
        }

        public string SelectedFileType
        {
            get
            {
                return m_selectedFileType;
            }
            set
            {
                m_selectedFileType = value;
            }
        }

        [XmlIgnore]
        public List<string> LookInPaths
        {
            get
            {
                return m_lookInPaths;
            }
            set
            {
                m_lookInPaths = value;
            }
        }

        [XmlArray( "LookInPaths" )]
        public string[] XmlLookInPaths
        {
            get
            {
                return m_lookInPaths.ToArray();
            }
            set
            {
                m_lookInPaths.Clear();

                if ( value != null )
                {
                    m_lookInPaths.AddRange( value );
                }
            }
        }

        [XmlIgnore]
        public List<string> FileTypes
        {
            get
            {
                return m_fileTypes;
            }
            set
            {
                m_fileTypes = value;
            }
        }

        [XmlArray( "FileTypes" )]
        public string[] XmlFileTypes
        {
            get
            {
                return m_fileTypes.ToArray();
            }
            set
            {
                m_fileTypes.Clear();

                if ( value != null )
                {
                    m_fileTypes.AddRange( value );
                }
            }
        }

        [XmlIgnore]
        public List<string> FindHistory
        {
            get
            {
                return m_findHistory;
            }
            set
            {
                m_findHistory = value;
            }
        }

        [XmlArray( "FindHistory" )]
        public string[] FindHistoryXml
        {
            get
            {
                return m_findHistory.ToArray();
            }
            set
            {
                m_findHistory.Clear();
                if ( value != null )
                {
                    m_findHistory.AddRange( value );
                }
            }
        }

        [XmlIgnore]
        public List<string> ReplaceHistory
        {
            get
            {
                return m_replaceHistory;
            }
            set
            {
                m_replaceHistory = value;
            }
        }

        [XmlArray( "ReplaceHistory" )]
        public string[] ReplaceHistoryXml
        {
            get
            {
                return m_replaceHistory.ToArray();
            }
            set
            {
                m_replaceHistory.Clear();
                if ( value != null )
                {
                    m_replaceHistory.AddRange( value );
                }
            }
        }

        #endregion

        #region Public Functions
        public void Reset()
        {
            m_useNewDialog = true;
            m_matchCase = false;
            m_matchWholeWord = false;
            m_useRegularExpressions = false;
            m_useWildcards = false;
            m_openFilesAfterReplace = false;
            m_selectedPath = string.Empty;
            m_searchSubFolders = false;
            m_selectedFileType = string.Empty;
            m_lookInPaths.Clear();
            m_fileTypes.Clear();
            m_findHistory.Clear();
            m_replaceHistory.Clear();
        }

        public void Copy( FindReplaceAllSettings f )
        {
            if ( f == null )
            {
                return;
            }

            this.MatchCase = f.MatchCase;
            this.MatchWholeWord = f.MatchWholeWord;
            this.OpenFilesAfterReplace = f.OpenFilesAfterReplace;
            this.SearchSubFolders = f.SearchSubFolders;
            this.SelectedFileType = f.SelectedFileType;
            this.SelectedPath = f.SelectedPath;
            this.UseRegularExpressions = f.UseRegularExpressions;
            this.UseNewDialog = f.UseNewDialog;
            this.UseWildcards = f.UseWildcards;

            this.FileTypes.Clear();
            this.FileTypes.AddRange( f.FileTypes.ToArray() );

            this.LookInPaths.Clear();
            this.LookInPaths.AddRange( f.LookInPaths.ToArray() );

            this.FindHistory.Clear();
            this.FindHistory.AddRange( f.FindHistory.ToArray() );

            this.ReplaceHistory.Clear();
            this.ReplaceHistory.AddRange( f.ReplaceHistory.ToArray() );
        }

        public string Serialize()
        {
            string tempFilename = Path.GetTempFileName();

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( tempFilename );
                XmlSerializer serializer = new XmlSerializer( this.GetType() );
                serializer.Serialize( writer, this );

                writer.Close();
            }
            catch ( Exception e )
            {
                if ( writer != null )
                {
                    writer.Close();
                }

                ApplicationSettings.Log.ToolException(e, "FindReplaceAllSettings Error serializing to tempfile '{0}'", tempFilename);
                File.Delete( tempFilename );
                return null;
            }

            string serializedText = null;
            TextReader reader = null;
            try
            {
                reader = new StreamReader( tempFilename );

                serializedText = reader.ReadToEnd();

                reader.Close();
            }
            catch ( Exception e )
            {
                if ( reader != null )
                {
                    reader.Close();
                }

                ApplicationSettings.Log.ToolException(e, "FindReplaceAllSettings Error loading tempfile '{0}'", tempFilename);
                File.Delete( tempFilename );
                return null;
            }

            File.Delete( tempFilename );
            return serializedText;
        }

        public bool Deserialize( string serializedText )
        {
            string tempFilename = Path.GetTempFileName();

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( tempFilename );

                writer.Write( serializedText );

                writer.Close();
            }
            catch ( Exception e )
            {
                if ( writer != null )
                {
                    writer.Close();
                }

                ApplicationSettings.Log.ToolException(e, "FindReplaceAllSettings Error writing tempfile '{0}'", tempFilename);
                File.Delete( tempFilename );
                return false;
            }

            FileStream stream = null;
            try
            {
                stream = new FileStream( tempFilename, FileMode.Open, FileAccess.Read );

                XmlSerializer serializer = new XmlSerializer( this.GetType() );
                FindReplaceAllSettings settings = serializer.Deserialize( stream ) as FindReplaceAllSettings;

                stream.Close();

                if ( settings == null )
                {
                    ApplicationSettings.Log.Error( "FindReplaceAllSettings unable to deserialize '{0}'", tempFilename );
                    File.Delete( tempFilename );
                    return false;
                }

                this.Copy( settings );
            }
            catch ( Exception e )
            {
                if ( stream != null )
                {
                    stream.Close();
                }

                ApplicationSettings.Log.ToolException(e, "FindReplaceAllSettings Error deserializing '{0}'", tempFilename);
                File.Delete( tempFilename );
                return false;
            }

            File.Delete( tempFilename );
            return true;
        }
        #endregion
    }
    #endregion

    #region Intellisense Settings
    public class IntellisenseSettings
    {
        public IntellisenseSettings()
        {
            Reset();
        }

        #region Variables
        private bool m_autocomplete;
        private bool m_autocompleteWithBrackets;
        private bool m_showItemsThatContainCurrentString;
        private bool m_completeWordOnFirstMatch;
        private bool m_completeWordOnExactMatch;
        private bool m_highlightEnums;
        private bool m_addProjectSymbols;
        private bool m_addIncludePathSymbols;
        private bool m_updateDocumentOutline;
        private bool m_verboseDocumentOutline;
        private bool m_performSemanticParsing;
        private bool m_highlightQuickInfoPopups;
        private bool m_highlightDisabledCodeBlocks;

        private bool m_addConstants;
        private bool m_addNativeTypes;
        private bool m_addStructTypes;
        private bool m_addEnumTypes;
        private bool m_addEnums;
        private bool m_addStaticVariables;
        private bool m_addSubroutines;
        private bool m_addNativeSubroutines;
        private bool m_addStates;

        private string m_xmlSectionName = "Intellisense";
        #endregion

        #region Properties
        public bool Autocomplete
        {
            get
            {
                return (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor) ? m_autocomplete : false;
            }
            set
            {
                m_autocomplete = value;
            }
        }

        public bool AutocompleteWithBrackets
        {
            get
            {
                return (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor) ? m_autocompleteWithBrackets : false;
            }
            set
            {
                m_autocompleteWithBrackets = value;
            }
        }

        public bool ShowItemsThatContainCurrentString
        {
            get
            {
                return (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor) ? m_showItemsThatContainCurrentString : false;
            }
            set
            {
                m_showItemsThatContainCurrentString = value;
            }
        }

        public bool CompleteWordOnFirstMatch
        {
            get
            {
                return (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor) ? m_completeWordOnFirstMatch : false;
            }
            set
            {
                m_completeWordOnFirstMatch = value;
            }
        }

        public bool CompleteWordOnExactMatch
        {
            get
            {
                return (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor) ? m_completeWordOnExactMatch : false;
            }
            set
            {
                m_completeWordOnExactMatch = value;
            }
        }

        public bool HighlightEnums
        {
            get
            {
                return m_highlightEnums;
            }
            set
            {
                m_highlightEnums = value;
            }
        }

        public bool AddProjectSymbols
        {
            get
            {
                return (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor) ? m_addProjectSymbols : false;
            }
            set
            {
                m_addProjectSymbols = value;
            }
        }

        public bool AddIncludePathSymbols
        {
            get
            {
                return (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor) ? m_addIncludePathSymbols : false;
            }
            set
            {
                m_addIncludePathSymbols = value;
            }
        }

        public bool UpdateDocumentOutline
        {
            get
            {
                return m_updateDocumentOutline;
            }
            set
            {
                m_updateDocumentOutline = value;
            }
        }

        public bool VerboseDocumentOutline
        {
            get
            {
                return m_verboseDocumentOutline;
            }
            set
            {
                m_verboseDocumentOutline = value;
            }
        }

        public bool PerformSemanticParsing
        {
            get
            {
                return m_performSemanticParsing;
            }
            set
            {
                m_performSemanticParsing = value;
            }
        }

        public bool HighlightQuickInfoPopups
        {
            get
            {
                return m_highlightQuickInfoPopups;
            }
            set
            {
                m_highlightQuickInfoPopups = value;
            }
        }

        public bool HighlightDisabledCodeBlocks
        {
            get
            {
                return m_highlightDisabledCodeBlocks;
            }
            set
            {
                m_highlightDisabledCodeBlocks = value;
            }
        }

        public bool AddConstants
        {
            get
            {
                return m_addConstants;
            }
            set
            {
                m_addConstants = value;
            }
        }

        public bool AddNativeTypes
        {
            get
            {
                return m_addNativeTypes;
            }
            set
            {
                m_addNativeTypes = value;
            }
        }

        public bool AddStructTypes
        {
            get
            {
                return m_addStructTypes;
            }
            set
            {
                m_addStructTypes = value;
            }
        }

        public bool AddEnumTypes
        {
            get
            {
                return m_addEnumTypes;
            }
            set
            {
                m_addEnumTypes = value;
            }
        }

        public bool AddEnums
        {
            get
            {
                return m_addEnums;
            }
            set
            {
                m_addEnums = value;
            }
        }

        public bool AddStaticVariables
        {
            get
            {
                return m_addStaticVariables;
            }
            set
            {
                m_addStaticVariables = value;
            }
        }

        public bool AddSubroutines
        {
            get
            {
                return m_addSubroutines;
            }
            set
            {
                m_addSubroutines = value;
            }
        }

        public bool AddNativeSubroutines
        {
            get
            {
                return m_addNativeSubroutines;
            }
            set
            {
                m_addNativeSubroutines = value;
            }
        }

        public bool AddStates
        {
            get
            {
                return m_addStates;
            }
            set
            {
                m_addStates = value;
            }
        }

        [XmlIgnore]
        public string XmlSectionName
        {
            get
            {
                return m_xmlSectionName;
            }
        }
        #endregion

        #region Save/Load
        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public bool LoadXml( XmlTextReader reader )
        {
            string theValue = reader["value"];
            if ( theValue != null )
            {
                this.Autocomplete = bool.Parse( theValue );
            }

            string brackets = reader["brackets"];
            if ( brackets != null )
            {
                this.AutocompleteWithBrackets = bool.Parse( brackets );
            }

            string containCurrentString = reader["showitemsthatcontaincurrentstring"];
            if ( containCurrentString != null )
            {
                this.ShowItemsThatContainCurrentString = bool.Parse( containCurrentString );
            }

            string highlightEnums = reader["HighlightEnums"];
            if ( highlightEnums != null )
            {
                this.HighlightEnums = bool.Parse( highlightEnums );
            }

            string addProjectSymbols = reader["AddProjectSymbols"];
            if ( addProjectSymbols != null )
            {
                this.AddProjectSymbols = bool.Parse( addProjectSymbols );
            }

            string addIncludePathSymbols = reader["AddIncludePathSymbols"];
            if ( addIncludePathSymbols != null )
            {
                this.AddIncludePathSymbols = bool.Parse( addIncludePathSymbols );
            }

            string updateDocumentOutline = reader["UpdateDocumentOutline"];
            if ( updateDocumentOutline != null )
            {
                this.UpdateDocumentOutline = bool.Parse( updateDocumentOutline );
            }

            string performSemanticParsing = reader["PerformSemanticParsing"];
            if ( performSemanticParsing != null )
            {
                this.PerformSemanticParsing = bool.Parse( performSemanticParsing );
            }

            string highlightQuickInfoPopups = reader["HighlightQuickInfoPopups"];
            if ( highlightQuickInfoPopups != null )
            {
                this.HighlightQuickInfoPopups = bool.Parse( highlightQuickInfoPopups );
            }

            string highlightDisabledCodeBlocks = reader["HighlightDisabledCodeBlocks"];
            if ( highlightDisabledCodeBlocks != null )
            {
                this.HighlightDisabledCodeBlocks = bool.Parse( highlightDisabledCodeBlocks );
            }

            string completeWordOnFirstMatch = reader["CompleteWordOnFirstMatch"];
            if ( completeWordOnFirstMatch != null )
            {
                this.CompleteWordOnFirstMatch = bool.Parse( completeWordOnFirstMatch );
            }

            string completeWordOnExactMatch = reader["CompleteWordOnExactMatch"];
            if ( completeWordOnExactMatch != null )
            {
                this.CompleteWordOnExactMatch = bool.Parse( completeWordOnExactMatch );
            }

            string addConstants = reader["AddConstants"];
            if ( addConstants != null )
            {
                this.AddConstants = bool.Parse( addConstants );
            }

            string addNativeTypes = reader["AddNativeTypes"];
            if ( addNativeTypes != null )
            {
                this.AddNativeTypes = bool.Parse( addNativeTypes );
            }

            string addStructTypes = reader["AddStructTypes"];
            if ( addStructTypes != null )
            {
                this.AddStructTypes = bool.Parse( addStructTypes );
            }

            string addEnumTypes = reader["AddEnumTypes"];
            if ( addEnumTypes != null )
            {
                this.AddEnumTypes = bool.Parse( addEnumTypes );
            }

            string addEnums = reader["AddEnums"];
            if ( addEnums != null )
            {
                this.AddEnums = bool.Parse( addEnums );
            }

            string addStaticVariables = reader["AddStaticVariables"];
            if ( addStaticVariables != null )
            {
                this.AddStaticVariables = bool.Parse( addStaticVariables );
            }

            string addSubroutines = reader["AddSubroutines"];
            if ( addSubroutines != null )
            {
                this.AddSubroutines = bool.Parse( addSubroutines );
            }

            string addNativeSubroutines = reader["AddNativeSubroutines"];
            if ( addNativeSubroutines != null )
            {
                this.AddNativeSubroutines = bool.Parse( addNativeSubroutines );
            }

            string addStates = reader["AddStates"];
            if ( addStates != null )
            {
                this.AddStates = bool.Parse( addStates );
            }

            return true;
        }
        #endregion

        #region Misc
        public void Reset()
        {
            m_autocomplete = true;
            m_autocompleteWithBrackets = true;
            m_showItemsThatContainCurrentString = true;
            m_completeWordOnFirstMatch = false;
            m_completeWordOnExactMatch = true;
            m_highlightEnums = true;
            m_addProjectSymbols = false;
            m_addIncludePathSymbols = false;
            m_updateDocumentOutline = true;
            m_verboseDocumentOutline = false;
            m_performSemanticParsing = true;
            m_highlightQuickInfoPopups = true;

            m_addConstants = true;
            m_addNativeTypes = true;
            m_addStructTypes = true;
            m_addEnumTypes = true;
            m_addEnums = true;
            m_addStaticVariables = true;
            m_addSubroutines = true;
            m_addNativeSubroutines = true;
            m_addStates = true;
        }

        public void Copy( IntellisenseSettings i )
        {
            if ( i == null )
            {
                return;
            }

            this.Autocomplete = i.Autocomplete;
            this.AutocompleteWithBrackets = i.AutocompleteWithBrackets;
            this.ShowItemsThatContainCurrentString = i.ShowItemsThatContainCurrentString;
            this.CompleteWordOnFirstMatch = i.CompleteWordOnFirstMatch;
            this.CompleteWordOnExactMatch = i.CompleteWordOnExactMatch;
            this.HighlightEnums = i.HighlightEnums;
            this.AddProjectSymbols = i.AddProjectSymbols;
            this.AddIncludePathSymbols = i.AddIncludePathSymbols;
            this.UpdateDocumentOutline = i.UpdateDocumentOutline;
            this.VerboseDocumentOutline = i.VerboseDocumentOutline;
            this.PerformSemanticParsing = i.PerformSemanticParsing;
            this.HighlightQuickInfoPopups = i.HighlightQuickInfoPopups;
            this.HighlightDisabledCodeBlocks = i.HighlightDisabledCodeBlocks;

            this.AddConstants = i.AddConstants;
            this.AddNativeTypes = i.AddNativeTypes;
            this.AddStructTypes = i.AddStructTypes;
            this.AddEnumTypes = i.AddEnumTypes;
            this.AddEnums = i.AddEnums;
            this.AddStaticVariables = i.AddStaticVariables;
            this.AddSubroutines = i.AddSubroutines;
            this.AddNativeSubroutines = i.AddNativeSubroutines;
            this.AddStates = i.AddStates;
        }
        #endregion
    }
    #endregion

    #region ProjectExplorerItem
    [XmlInclude( typeof( ProjectExplorerFile ) ),
    XmlInclude( typeof(ProjectExplorerFolder) )]
    public class ProjectExplorerItem
    {
        public ProjectExplorerItem()
            : this( "<none>" )
        {

        }

        public ProjectExplorerItem( string name )
        {
            m_name = name;
        }

        #region Variables
        protected string m_name;
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }
        #endregion

        #region Public Functions
        public virtual ProjectExplorerItem Clone()
        {
            return null;
        }
        #endregion
    }

    public class ProjectExplorerFile : ProjectExplorerItem
    {
        public ProjectExplorerFile()
            : base( "<new file>" )
        {

        }

        public ProjectExplorerFile( string filename )
            : base(filename)
        {

        }

        #region ProjectExplorerItem
        public override ProjectExplorerItem Clone()
        {
            return new ProjectExplorerFile( this.Name );
        }
        #endregion
    }

    public class ProjectExplorerFolder : ProjectExplorerItem
    {
        public ProjectExplorerFolder()
            : base( "<new folder>" )
        {

        }

        public ProjectExplorerFolder( string folderName )
            : base( folderName )
        {

        }

        #region Variables
        private List<ProjectExplorerItem> m_items = new List<ProjectExplorerItem>();
        #endregion

        #region Properties
        [XmlIgnore]
        public List<ProjectExplorerItem> Items
        {
            get
            {
                return m_items;
            }
            set
            {
                m_items = value;
            }
        }

        [XmlArray( "Items" )]
        public ProjectExplorerItem[] XmlItems
        {
            get
            {
                return m_items.ToArray();
            }
            set
            {
                m_items.Clear();

                if ( value != null )
                {
                    m_items.AddRange( value );
                }
            }
        }
        #endregion

        #region ProjectExplorerItem
        public override ProjectExplorerItem Clone()
        {
            ProjectExplorerFolder folder = new ProjectExplorerFolder( this.Name );
            foreach ( ProjectExplorerItem item in this.Items )
            {
                folder.Items.Add( item.Clone() );
            }

            return folder;
        }
        #endregion
    }
    #endregion

    #region New EditorSettings
    [XmlRoot( "EditorSettingsBaseVer3_0" )]
    public abstract class EditorSettingsBase
    {
        protected EditorSettingsBase( string name, float version )
        {
            m_name = name;
            m_version = version;
        }

        #region Variables
        private string m_name = "EditorSettingsBaseVer2_1";
        private float m_version = 0.0f;
        private float m_loadedVersion = 0.0f;

        protected ProjectSettings m_projectSettings = new NonProjectProjectSettings();
        protected SourceControlSettings2 m_sourceControlSettings = new SourceControlSettings2();
        protected PlatformSettings m_platformSettings = new PlatformSettings();
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }

        public float Version
        {
            get
            {
                return m_version;
            }
            set
            {
                m_version = value;
            }
        }

        [XmlIgnore]
        public float LoadedVersion
        {
            get
            {
                return m_loadedVersion;
            }
        }

        public ProjectSettings Project
        {
            get
            {
                return m_projectSettings;
            }
            set
            {
                m_projectSettings = value;
            }
        }

        public SourceControlSettings SourceControl
        {
            get
            {
                return null;
            }
            set
            {
                if ( value != null )
                {
                    m_sourceControlSettings.Upgrade( value );
                }
                else
                {
                    m_sourceControlSettings.Reset();
                }
            }
        }

        /// <summary>
        /// NOTE: This is only kept in the base class for backwards compatibility in deserialization.
        /// UserEditorSettings is the only child class that should actually use this.
        /// </summary>
        public SourceControlSettings2 SourceControl2
        {
            get
            {
                return m_sourceControlSettings;
            }
            set
            {
                m_sourceControlSettings = value;
            }
        }

        public PlatformSettings PlatformSettings
        {
            get { return m_platformSettings; }
            set { m_platformSettings = value; }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Saves the settings to the specified file.
        /// </summary>
        /// <param name="filename">The file to save.</param>
        /// <param name="error">Out parameter with error information if this function returns <c>false</c>.</param>
        /// <returns><c>true</c> on success.  If <c>false</c>, look at 'error' for more information.</returns>
        public bool SaveFile( string filename, out string error )
        {
            error = null;

            if ( File.Exists( filename ) )
            {
                // make writable
                FileAttributes atts = File.GetAttributes( filename );
                if ( (atts & FileAttributes.ReadOnly) == FileAttributes.ReadOnly )
                {
                    try
                    {
                        File.SetAttributes( filename, atts & (~FileAttributes.ReadOnly) );
                    }
                    catch
                    {
                    }
                }

                ApplicationSettings.Log.Message( "Backing up the previous settings file {0}.", filename );

                try
                {
                    string backupFilename = Path.ChangeExtension( filename, ".bak" );

                    // make writable
                    if ( File.Exists( backupFilename ) )
                    {
                        atts = File.GetAttributes( backupFilename );
                        if ( (atts & FileAttributes.ReadOnly) == FileAttributes.ReadOnly )
                        {
                            try
                            {
                                File.SetAttributes( backupFilename, atts & (~FileAttributes.ReadOnly) );
                            }
                            catch
                            {
                            }
                        }
                    }

                    File.Copy( filename, backupFilename, true );
                }
                catch ( System.Exception e )
                {
                    ApplicationSettings.Log.ToolException( e, "Exception creating backup file for {0}", filename );
                }
            }
            else
            {
                string directory = Path.GetDirectoryName( filename );
                if ( !Directory.Exists( directory ) )
                {
                    Directory.CreateDirectory( directory );
                }
            }

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );

                XmlSerializer serializer = new XmlSerializer( this.GetType(), GetDerivedSettingsTypes() );
                serializer.Serialize( writer, this );

                writer.Close();
            }
            catch ( Exception e )
            {
                if ( writer != null )
                {
                    writer.Close();
                }

                error = "Error serializing '" + filename + "'\n\n" + e.Message;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Load the xml filename with all of the settings
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public bool LoadFile( string filename, out string error )
        {
            error = null;

            FileStream stream = null;
            try
            {
                stream = new FileStream( filename, FileMode.Open, FileAccess.Read );

                XmlSerializer serializer = new XmlSerializer( this.GetType(), GetDerivedSettingsTypes() );
                EditorSettingsBase settings = serializer.Deserialize( stream ) as EditorSettingsBase;

                stream.Close();

                if ( settings == null )
                {
                    // try to load the backup, if it exists.
                    string backupFilename = Path.ChangeExtension( filename, ".bak" );
                    if ( (filename != backupFilename) && File.Exists( backupFilename ) )
                    {
                        return LoadFile( backupFilename, out error );
                    }

                    error = "Unable to deserialize '" + filename + "'";
                    return false;
                }

                this.Copy( settings );

                m_loadedVersion = settings.Version;

                Validate();
            }
            catch ( Exception e )
            {
                if ( stream != null )
                {
                    stream.Close();
                }

                // try to load the backup, if it exists.
                string backupFilename = Path.ChangeExtension( filename, ".bak" );
                if ( (filename != backupFilename) && File.Exists( backupFilename ) )
                {
                    return LoadFile( backupFilename, out error );
                }

                error = "Error deserializing '" + filename + "'\n\n" + e.Message;
                return false;
            }

            return true;
        }
        #endregion

        #region Virtual Functions
        public virtual string BuildCompileScriptCommand(string scriptFilename, Platform currentPlatform)
        {
            return string.Empty;
        }

        public virtual string ResourceCompileScriptCommand(string scriptFilename, string projectFilename, Platform currentPlatform)
        {
            return string.Empty;
        }

        public virtual void Copy( EditorSettingsBase settings )
        {
            if ( this.Project != null )
            {
                this.Project.Copy( settings.Project );
            }

            if (this.PlatformSettings != null)
            {
                this.PlatformSettings.Copy(settings.PlatformSettings);
            }
        }

        public virtual void Validate()
        {
            if ( m_projectSettings == null )
            {
                m_projectSettings = new NonProjectProjectSettings();
            }
        }
        #endregion

        #region Delegates
        public delegate CompilingSettings CreateCompilingSettingsDelegate();
        public delegate LanguageSettings CreateLanguageSettingsDelegate();
        #endregion

        #region Static Variables
        private static Dictionary<string, Type> sm_compilingSettingDerivedTypes = new Dictionary<string, Type>();
        private static Dictionary<string, Type> sm_languageSettingsDerivedTypes = new Dictionary<string, Type>();
        #endregion

        #region Static Properties
        public static List<string> RegisteredCompilingLanguages
        {
            get
            {
                return new List<string>( sm_compilingSettingDerivedTypes.Keys );
            }
        }

        public static List<string> RegisteredLanguageLanguages
        {
            get
            {
                return new List<string>( sm_languageSettingsDerivedTypes.Keys );
            }
        }
        #endregion

        #region Static Functions
        public static void AddCompilingSettingsDerivedType( string language, Type t )
        {
            sm_compilingSettingDerivedTypes[language] = t;
        }

        public static void AddLanguageSettingsCreatorDelegate( string language, Type t )
        {
            sm_languageSettingsDerivedTypes[language] = t;
        }

        public static Type GetCompilingSettingsDerivedTypeForLangauge( string language )
        {
            Type t;
            if ( sm_compilingSettingDerivedTypes.TryGetValue( language, out t ) )
            {
                return t;
            }

            return null;
        }

        public static CompilingSettings CreateCompilingSettingsForLangauge( string language )
        {
            Type t = GetCompilingSettingsDerivedTypeForLangauge( language );
            if ( t != null )
            {
                return t.GetConstructor( Type.EmptyTypes ).Invoke( null ) as CompilingSettings;
            }

            return null;
        }

        public static void CreateCompilingSettingsForAllLanguages( List<CompilingSettings> compilingSettingsList )
        {
            foreach ( string language in sm_compilingSettingDerivedTypes.Keys )
            {
                compilingSettingsList.Add( CreateCompilingSettingsForLangauge( language ) );
            }
        }

        public static Type GetLanguageSettingsDerivedTypeForLangauge( string language )
        {
            Type t;
            if ( sm_languageSettingsDerivedTypes.TryGetValue( language, out t ) )
            {
                return t;
            }

            return null;
        }

        public static LanguageSettings CreateLanguageSettingsForLangauge( string language )
        {
            Type t = GetLanguageSettingsDerivedTypeForLangauge( language );
            if ( t != null )
            {
                return t.GetConstructor( Type.EmptyTypes ).Invoke( null ) as LanguageSettings;
            }

            return null;
        }

        public static void CreateLanguageSettingsForAllLanguages( List<LanguageSettings> languageSettingsList )
        {
            foreach ( string language in sm_languageSettingsDerivedTypes.Keys )
            {
                languageSettingsList.Add( CreateLanguageSettingsForLangauge( language ) );
            }
        }
        #endregion

        #region Private Functions
        private Type[] GetDerivedSettingsTypes()
        {
            List<Type> types = new List<Type>();

            types.AddRange( sm_compilingSettingDerivedTypes.Values );
            types.AddRange( sm_languageSettingsDerivedTypes.Values );

            return types.ToArray();
        }
        #endregion
    }

    /// <summary>
    /// Class for holding the project-specific settings that come from the SettingsForm.cs
    /// </summary>
    [XmlRoot( "ProjectEditorSettingsVer3_0" )]
    public class ProjectEditorSettings : EditorSettingsBase
    {
        public ProjectEditorSettings()
            : base( "ProjectEditorSettings", ProjectEditorSettings.SettingsVersion )
        {
            // re-allocate with the class we really want
            m_projectSettings = new ProjectSettings();
        }

        #region Constants
        public const float SettingsVersion = 3.0f;
        #endregion

        #region Variables
        private string m_originalFilename;
        private string m_filename;
        private string m_language;
        private bool m_isLoaded = false;

        private CompilingSettingsCollection m_compilingSettingsList = new CompilingSettingsCollection();

        private List<ProjectExplorerItem> m_projectExplorerItems = new List<ProjectExplorerItem>();

        #endregion

        #region Properties
        /// <summary>
        /// The OriginalFilename. This value may contain environment variables so must be subst'd as needed.
        /// </summary>
        public string OriginalFilename
        {
            get
            {
                return m_originalFilename;
            }
            set
            {
                m_originalFilename = value;
            }
        }

        [XmlIgnore]
        public string OriginalFilenameExpanded
        {
            get
            {
                return ApplicationSettings.GetAbsolutePath(OriginalFilename);
            }
        }

        [XmlIgnore]
        public bool IsLoaded
        {
            get
            {
                return m_isLoaded;
            }
            set
            {
                m_isLoaded = value;
            }
        }

        [XmlIgnore]
        public string Filename
        {
            get
            {
                return m_filename;
            }
            set
            {
                m_filename = value;
            }
        }

        public string Language
        {
            get
            {
                return m_language;
            }
            set
            {
                m_language = value;
            }
        }

        public int CurrentCompilingSettingsIndex
        {
            get
            {
                return m_compilingSettingsList.CurrentIndex;
            }
            set
            {
                m_compilingSettingsList.CurrentIndex = value;
            }
        }

        [XmlIgnore]
        public CompilingSettings CurrentCompilingSettings
        {
            get
            {
                return m_compilingSettingsList.CurrentSettings;
            }
            set
            {
                m_compilingSettingsList.CurrentSettings = value;
            }
        }

        [XmlIgnore]
        public List<CompilingSettings> CompilingSettingsList
        {
            get
            {
                return m_compilingSettingsList;
            }
        }

        [XmlArray( "CompilingSettingsList" )]
        public CompilingSettings[] XmlCompilingSettingsList
        {
            get
            {
                return m_compilingSettingsList.ToArray();
            }
            set
            {
                m_compilingSettingsList.Clear();
                m_compilingSettingsList.CurrentIndex = -1;

                if ( value != null )
                {
                    m_compilingSettingsList.AddRange( value );

                    if ( m_compilingSettingsList.Count > 0 )
                    {
                        m_compilingSettingsList.CurrentIndex = 0;
                    }
                }
            }
        }

        [XmlIgnore]
        public List<ProjectExplorerItem> ProjectExplorerItems
        {
            get
            {
                return m_projectExplorerItems;
            }
            set
            {
                m_projectExplorerItems = value;
            }
        }

        [XmlArray( "ProjectExplorerItems" )]
        public ProjectExplorerItem[] XmlProjectExplorerItems
        {
            get
            {
                return m_projectExplorerItems.ToArray();
            }
            set
            {
                m_projectExplorerItems.Clear();

                if ( value != null )
                {
                    m_projectExplorerItems.AddRange( value );
                }
            }
        }
        #endregion

        #region Overrides
        public override void Copy( EditorSettingsBase settings )
        {
            base.Copy( settings );

            if ( settings is ProjectEditorSettings )
            {
                ProjectEditorSettings projectSettings = settings as ProjectEditorSettings;

                this.OriginalFilename = projectSettings.OriginalFilename;
                this.Filename = projectSettings.Filename;
                this.Language = projectSettings.Language;

                this.XmlCompilingSettingsList = null;   // quick way to clear and reset the CurrentIndex
                foreach ( CompilingSettings c in projectSettings.CompilingSettingsList )
                {
                    // clone it directly because we're loading a project for a specific language
                    CompilingSettings copyC = c.Clone() as CompilingSettings;

                    this.CompilingSettingsList.Add( copyC );
                }

                this.CurrentCompilingSettingsIndex = projectSettings.CurrentCompilingSettingsIndex;

                this.ProjectExplorerItems.Clear();
                foreach ( ProjectExplorerItem item in projectSettings.ProjectExplorerItems )
                {
                    this.ProjectExplorerItems.Add( item.Clone() );
                }
            }
            else if ( settings is UserEditorSettings )
            {
                UserEditorSettings userSettings = settings as UserEditorSettings;

                // only copy the compiling settings for this language
                string language = userSettings.CurrentCompilingLanguage;
                userSettings.CurrentCompilingLanguage = this.Language;

                this.XmlCompilingSettingsList = null;   // quick way to clear and reset the CurrentIndex
                foreach ( CompilingSettings c in userSettings.CurrentCompilingSettingsList )
                {
                    CompilingSettings copyC = c.Clone() as CompilingSettings;
                    this.CompilingSettingsList.Add( copyC );
                }

                this.CurrentCompilingSettingsIndex = userSettings.CurrentCompilingSettingsIndex;

                userSettings.CurrentCompilingLanguage = language;
            }
            else if ( settings is LanguageDefaultEditorSettingsBase )
            {
                LanguageDefaultEditorSettingsBase defaultSettings = settings as LanguageDefaultEditorSettingsBase;

                // only copy if the languages match
                if ( this.Language == defaultSettings.LanguageName )
                {
                    this.XmlCompilingSettingsList = null;   // quick way to clear and reset the CurrentIndex
                    foreach ( CompilingSettings c in defaultSettings.CompilingSettingsList )
                    {
                        CompilingSettings copyC = c.Clone() as CompilingSettings;
                        this.CompilingSettingsList.Add( copyC );
                    }

                    this.CurrentCompilingSettingsIndex = 0;
                }
            }
        }

        public override string BuildCompileScriptCommand(string scriptFilename, Platform currentPlatform)
        {
            if ( this.CurrentCompilingSettings == null )
            {
                return string.Empty;
            }

            return this.CurrentCompilingSettings.BuildCompileScriptCommand(scriptFilename, currentPlatform);
        }

        public override void Validate()
        {
            bool reallocateProjectSettings = m_projectSettings == null;

            base.Validate();

            if ( reallocateProjectSettings )
            {
                m_projectSettings = new ProjectSettings();
            }

            if ( m_compilingSettingsList == null )
            {
                m_compilingSettingsList = new CompilingSettingsCollection();
            }

            if ( !String.IsNullOrEmpty( this.Language ) && (m_compilingSettingsList.Count == 0) )
            {
                CompilingSettings c = EditorSettingsBase.CreateCompilingSettingsForLangauge( this.Language );
                if ( c != null )
                {
                    if ( m_compilingSettingsList == null )
                    {
                        m_compilingSettingsList = new CompilingSettingsCollection();
                    }

                    m_compilingSettingsList.Add( c );
                    m_compilingSettingsList.CurrentIndex = 0;
                }
            }

            if ( m_projectExplorerItems == null )
            {
                m_projectExplorerItems = new List<ProjectExplorerItem>();
            }
        }
        #endregion // Overrides
    }

    /// <summary>
    /// Class for holding all of the settings that are saved as each user's preferences
    /// </summary>
    [XmlRoot( "UserEditorSettingsVer3_0" )]
    public class UserEditorSettings : EditorSettingsBase
    {
        public UserEditorSettings()
            : base( "UserEditorSettings", UserEditorSettings.SettingsVersion )
        {

        }

        #region Constants
        public const float SettingsVersion = 3.0f;
        #endregion

        #region Variables
        private NonGameFileSettings m_fileSettings = new NonGameFileSettings();
        private ToolbarSettings m_toolbarSettings = new ToolbarSettings();
        private Dictionary<string, CompilingSettingsCollection> m_languageCompilingSettings = new Dictionary<string, CompilingSettingsCollection>();
        private List<LanguageSettings> m_languageSettingsList = new List<LanguageSettings>();
        private LanguageHighlightingStylesSettings m_languageHighlightingStylesSettings = new LanguageHighlightingStylesSettings();
        private FindReplaceSettings m_findReplaceSettings = new FindReplaceSettings();
        private IntellisenseSettings m_intellisenseSettings = new IntellisenseSettings();

        private string m_currentCompilingLanguage;
        private Platform m_currentPlatform;
        #endregion

        #region Properties
        public NonGameFileSettings Files
        {
            get
            {
                return m_fileSettings;
            }
            set
            {
                m_fileSettings = value;
            }
        }

        public ToolbarSettings Toolbar
        {
            get
            {
                return m_toolbarSettings;
            }
            set
            {
                m_toolbarSettings = value;
            }
        }

        public string CurrentCompilingLanguage
        {
            get
            {
                return m_currentCompilingLanguage;
            }
            set
            {
                m_currentCompilingLanguage = value;
            }
        }


        public Platform CurrentPlatform
        {
            get { return m_currentPlatform; }
            set { m_currentPlatform = value; }
        }

        public int CurrentCompilingSettingsIndex
        {
            get
            {
                CompilingSettingsCollection collection;
                if ( m_languageCompilingSettings.TryGetValue( m_currentCompilingLanguage, out collection ) )
                {
                    return collection.CurrentIndex;
                }

                return -1;
            }
            set
            {
                CompilingSettingsCollection collection;
                if ( m_languageCompilingSettings.TryGetValue( m_currentCompilingLanguage, out collection ) )
                {
                    collection.CurrentIndex = value;
                }
            }
        }

        [XmlIgnore]
        public CompilingSettings CurrentCompilingSettings
        {
            get
            {
                CompilingSettingsCollection collection;
                if ( m_languageCompilingSettings.TryGetValue( m_currentCompilingLanguage, out collection ) )
                {
                    return collection.CurrentSettings;
                }

                return null;
            }
            set
            {
                CompilingSettingsCollection collection;
                if ( m_languageCompilingSettings.TryGetValue( m_currentCompilingLanguage, out collection ) )
                {
                    collection.CurrentSettings = value;
                }
            }
        }

        [XmlIgnore]
        public List<CompilingSettings> CurrentCompilingSettingsList
        {
            get
            {
                CompilingSettingsCollection collection;
                if ( m_languageCompilingSettings.TryGetValue( m_currentCompilingLanguage, out collection ) )
                {
                    return collection;
                }

                return null;
            }
        }

        [XmlIgnore]
        public LanguageSettings CurrentLanguageSettings
        {
            get
            {
                foreach ( LanguageSettings l in m_languageSettingsList )
                {
                    if ( l.Language == m_currentCompilingLanguage )
                    {
                        return l;
                    }
                }

                return null;
            }
            set
            {
                if ( value != null )
                {
                    for ( int i = 0; i < m_languageSettingsList.Count; ++i )
                    {
                        if ( value.Language == m_languageSettingsList[i].Language )
                        {
                            m_languageSettingsList[i] = value;
                            m_currentCompilingLanguage = value.Language;
                            break;
                        }
                    }
                }
            }
        }

        //[XmlArray( "CompilingSettingsList" )]
        [XmlIgnore]
        public CompilingSettings[] XmlCompilingSettingsList
        {
            get
            {
                List<CompilingSettings> compilingSettingsList = new List<CompilingSettings>();
                foreach ( CompilingSettingsCollection collection in m_languageCompilingSettings.Values )
                {
                    compilingSettingsList.AddRange( collection );
                }

                return compilingSettingsList.ToArray();
            }
            set
            {
                int currentIndex = this.CurrentCompilingSettingsIndex;

                m_languageCompilingSettings.Clear();

                if ( value != null )
                {
                    foreach ( CompilingSettings c in value )
                    {
                        CompilingSettingsCollection collection = null;
                        if ( !m_languageCompilingSettings.TryGetValue( c.Language, out collection ) )
                        {
                            collection = new CompilingSettingsCollection();
                            m_languageCompilingSettings.Add( c.Language, collection );
                        }

                        collection.Add( c );
                    }
                }

                this.CurrentCompilingSettingsIndex = currentIndex;
            }
        }

        [XmlIgnore]
        public List<LanguageSettings> LanguageSettingsList
        {
            get
            {
                return m_languageSettingsList;
            }
            set
            {
                m_languageSettingsList = value;
            }
        }

        [XmlArray( "LanguageSettingsList" )]
        public LanguageSettings[] XmlLanguageSettingsList
        {
            get
            {
                return m_languageSettingsList.ToArray();
            }
            set
            {
                m_languageSettingsList.Clear();

                if ( value != null )
                {
                    m_languageSettingsList.AddRange( value );
                }
            }
        }

        [XmlIgnore]
        public Dictionary<string, CompilingSettingsCollection> LanguageCompilingSettings
        {
            get
            {
                return m_languageCompilingSettings;
            }
        }

        public LanguageHighlightingStylesSettings LanguageHighlightingStylesSettings
        {
            get
            {
                return m_languageHighlightingStylesSettings;
            }
            set
            {
                m_languageHighlightingStylesSettings = value;
            }
        }

        public FindReplaceSettings FindReplaceSettings
        {
            get
            {
                return m_findReplaceSettings;
            }
            set
            {
                m_findReplaceSettings = value;
            }
        }

        public IntellisenseSettings IntellisenseSettings
        {
            get
            {
                return m_intellisenseSettings;
            }
            set
            {
                m_intellisenseSettings = value;
            }
        }
        #endregion

        #region Overrides
        public override string BuildCompileScriptCommand(string scriptFilename, Platform currentPlatform)
        {
            if ( this.CurrentCompilingSettings == null )
            {
                return string.Empty;
            }

            return this.CurrentCompilingSettings.BuildCompileScriptCommand(scriptFilename, currentPlatform);
        }

        public override string ResourceCompileScriptCommand(string scriptFilename, string outputFilename, Platform currentPlatform)
        {
            if (this.CurrentCompilingSettings == null)
            {
                return string.Empty;
            }

            return this.CurrentCompilingSettings.ResourceCompileScriptCommand(scriptFilename, outputFilename, currentPlatform);
        }

        public override void Copy( EditorSettingsBase settings )
        {
            base.Copy( settings );

            if ( settings != null )
            {
                if ( settings is UserEditorSettings )
                {
                    UserEditorSettings userSettings = settings as UserEditorSettings;

                    if (this.SourceControl2 != null)
                    {
                        if (settings.SourceControl2 != null)
                        {
                            this.SourceControl2.Copy(settings.SourceControl2);
                        }
                        else if (settings.SourceControl != null)
                        {
                            this.SourceControl2.Upgrade(settings.SourceControl);
                        }
                    }

                    if ( this.Files != null )
                    {
                        this.Files.Copy( userSettings.Files );
                    }

                    if ( this.Toolbar != null )
                    {
                        this.Toolbar.Copy( userSettings.Toolbar );
                    }

                    // CompilingSettings for each language
                    m_languageCompilingSettings.Clear();
                    foreach ( KeyValuePair<string, CompilingSettingsCollection> pair in userSettings.m_languageCompilingSettings )
                    {
                        CompilingSettingsCollection copyCollection = new CompilingSettingsCollection();

                        foreach ( CompilingSettings c in pair.Value )
                        {
                            // make a straight up clone because we're copying the same type (UserEditorSettings)
                            CompilingSettings copyC = c.Clone() as CompilingSettings;
                            copyCollection.Add( copyC );
                        }

                        copyCollection.CurrentIndex = (pair.Value.CurrentIndex == -1) ? 0 : pair.Value.CurrentIndex;

                        m_languageCompilingSettings[pair.Key] = copyCollection;
                    }

                    m_currentCompilingLanguage = userSettings.CurrentCompilingLanguage;
                    m_currentPlatform = userSettings.CurrentPlatform;

                    // LanguageSettings for each language
                    this.LanguageSettingsList.Clear();
                    foreach ( LanguageSettings l in userSettings.LanguageSettingsList )
                    {
                        // make a straight up clone because we're copying the same type (UserEditorSettings)
                        LanguageSettings copyL = l.Clone() as LanguageSettings;

                        this.LanguageSettingsList.Add( copyL );
                    }

                    if ( userSettings.FindReplaceSettings != null )
                    {
                        if ( this.FindReplaceSettings == null )
                        {
                            this.FindReplaceSettings = new FindReplaceSettings();
                        }

                        this.FindReplaceSettings.Copy( userSettings.FindReplaceSettings );
                    }

                    if ( userSettings.LanguageHighlightingStylesSettings != null )
                    {
                        if ( this.LanguageHighlightingStylesSettings == null )
                        {
                            this.LanguageHighlightingStylesSettings = new LanguageHighlightingStylesSettings();
                        }

                        this.LanguageHighlightingStylesSettings.Copy( userSettings.LanguageHighlightingStylesSettings );
                    }

                    if ( userSettings.IntellisenseSettings != null )
                    {
                        if ( this.IntellisenseSettings == null )
                        {
                            this.IntellisenseSettings = new IntellisenseSettings();
                        }

                        this.IntellisenseSettings.Copy( userSettings.IntellisenseSettings );
                    }
                }
                else if ( settings is ProjectEditorSettings )
                {
                    ProjectEditorSettings projectSettings = settings as ProjectEditorSettings;

                    // make sure we have a CompilingSettingsCollection for the language, and copy just that
                    CompilingSettingsCollection collection = null;
                    if ( !m_languageCompilingSettings.TryGetValue( projectSettings.Language, out collection ) )
                    {
                        collection = new CompilingSettingsCollection();
                        m_languageCompilingSettings.Add( projectSettings.Language, collection );
                    }

                    collection.Clear();
                    foreach ( CompilingSettings c in projectSettings.CompilingSettingsList )
                    {
                        CompilingSettings copyC = c.Clone() as CompilingSettings;
                        collection.Add( copyC );
                    }

                    collection.CurrentIndex = projectSettings.CurrentCompilingSettingsIndex;
                }
                else if ( settings is LanguageDefaultEditorSettingsBase )
                {
                    LanguageDefaultEditorSettingsBase defaultSettings = settings as LanguageDefaultEditorSettingsBase;

                    if ( this.Files != null )
                    {
                        this.Files.Copy( defaultSettings.Files );
                    }

                    if ( this.Toolbar != null )
                    {
                        this.Toolbar.Copy( defaultSettings.Toolbar );
                    }

                    // make sure we have a CompilingSettingsCollection for the language, and copy just that
                    CompilingSettingsCollection collection = null;
                    if ( !m_languageCompilingSettings.TryGetValue( defaultSettings.LanguageName, out collection ) )
                    {
                        collection = new CompilingSettingsCollection();
                        m_languageCompilingSettings.Add( defaultSettings.LanguageName, collection );
                    }

                    collection.Clear();
                    foreach ( CompilingSettings c in defaultSettings.CompilingSettingsList )
                    {
                        CompilingSettings copyC = c.Clone() as CompilingSettings;
                        collection.Add( copyC );
                    }

                    collection.CurrentIndex = 0;

                    // make sure we have a LanguageSettings for the language, and copy just that
                    LanguageSettings language = null;
                    foreach ( LanguageSettings l in this.LanguageSettingsList )
                    {
                        if ( l.Language == defaultSettings.Language.Language )
                        {
                            language = l;
                            break;
                        }
                    }

                    if ( language == null )
                    {
                        language = defaultSettings.Language.Clone() as LanguageSettings;
                        this.LanguageSettingsList.Add( language );
                    }
                    else
                    {
                        language.Copy( defaultSettings.Language );
                    }
                }
            }
        }
        public override void Validate()
        {
            base.Validate();

            if (m_sourceControlSettings == null)
            {
                m_sourceControlSettings = new SourceControlSettings2();
            }
            else if (m_sourceControlSettings.SourceControlProviderSettings == null)
            {
                m_sourceControlSettings.SourceControlProviderSettings = new rageSourceControlProviderSettings();
            }

            // Remove duplicate source control providers
            for (int i = 0; i < m_sourceControlSettings.SourceControlProviderSettings.Providers.Count; ++i)
            {
                for (int j = i + 1; j < m_sourceControlSettings.SourceControlProviderSettings.Providers.Count;)
                {
                    if (m_sourceControlSettings.SourceControlProviderSettings.Providers[j].ToString()
                        == m_sourceControlSettings.SourceControlProviderSettings.Providers[i].ToString())
                    {
                        ApplicationSettings.Log.Message("Removing duplicate rageSourceControlProvider '{0}'.",
                            m_sourceControlSettings.SourceControlProviderSettings.Providers[j].ToString());
                        m_sourceControlSettings.SourceControlProviderSettings.Providers.RemoveAt(j);
                    }
                    else
                    {
                        ++j;
                    }
                }
            }

            if ( m_fileSettings == null )
            {
                m_fileSettings = new NonGameFileSettings();
            }

            if ( m_toolbarSettings == null )
            {
                m_toolbarSettings = new ToolbarSettings();
            }

            if ( m_languageCompilingSettings == null )
            {
                m_languageCompilingSettings = new Dictionary<string, CompilingSettingsCollection>();
            }

            if ( m_languageCompilingSettings.Count == 0 )
            {
                // create all
                List<CompilingSettings> compilingSettingsList = new List<CompilingSettings>();
                EditorSettingsBase.CreateCompilingSettingsForAllLanguages( compilingSettingsList );

                foreach ( CompilingSettings c in compilingSettingsList )
                {
                    CompilingSettingsCollection collection = new CompilingSettingsCollection();
                    collection.Add( c );
                    collection.CurrentIndex = 0;

                    m_languageCompilingSettings.Add( c.Language, collection );
                }
            }

            if ( m_languageSettingsList == null )
            {
                m_languageSettingsList = new List<LanguageSettings>();
            }

            if ( m_languageSettingsList.Count == 0 )
            {
                // create all
                EditorSettingsBase.CreateLanguageSettingsForAllLanguages( m_languageSettingsList );
            }

            if ( m_languageHighlightingStylesSettings == null )
            {
                m_languageHighlightingStylesSettings = new LanguageHighlightingStylesSettings();
            }

            if ( m_findReplaceSettings == null )
            {
                m_findReplaceSettings = new FindReplaceSettings();
            }

            if ( m_intellisenseSettings == null )
            {
                m_intellisenseSettings = new IntellisenseSettings();
            }
        }
        #endregion
    }

    /// <summary>
    /// Class for holding all of the settings that get loaded as part of a Parser Plugin DLL
    /// Also, these are all editable on the SettingsForm.
    /// </summary>
    [XmlRoot( "LanguageDefaultEditorSettingsBaseVer0_0" )]
    public abstract class LanguageDefaultEditorSettingsBase : EditorSettingsBase
    {
        protected LanguageDefaultEditorSettingsBase( string language, float version )
            : base( language + "LanguageDefaultEditorSettings", version )
        {
            m_languageName = language;

            // make sure we have a compiling settings and a language settings
            this.CompilingSettingsList.Add( CreateCompilingTabSettings() );
            this.Language = CreateLanguageTabSettings();
        }

        #region Variables
        protected string m_languageName = string.Empty;
        protected FileSettings m_fileSettings = new FileSettings();
        protected ToolbarSettings m_toolbarSettings = new ToolbarSettings();
        protected List<CompilingSettings> m_compilingSettingsList = new List<CompilingSettings>();
        protected LanguageSettings m_languageSettings = null;
        #endregion

        #region Properties
        [XmlIgnore]
        public string LanguageName
        {
            get
            {
                return m_languageName;
            }
        }

        public FileSettings Files
        {
            get
            {
                return m_fileSettings;
            }
            set
            {
                m_fileSettings = value;
            }
        }

        public ToolbarSettings Toolbar
        {
            get
            {
                return m_toolbarSettings;
            }
            set
            {
                m_toolbarSettings = value;
            }
        }

        [XmlIgnore]
        public List<CompilingSettings> CompilingSettingsList
        {
            get
            {
                return m_compilingSettingsList;
            }
            set
            {
                m_compilingSettingsList = value;
            }
        }

        [XmlArray( "CompilingSettingsList" )]
        public CompilingSettings[] XmlCompilingSettingsList
        {
            get
            {
                return m_compilingSettingsList.ToArray();
            }
            set
            {
                m_compilingSettingsList.Clear();

                if ( value != null )
                {
                    m_compilingSettingsList.AddRange( value );
                }
            }
        }

        public LanguageSettings Language
        {
            get
            {
                return m_languageSettings;
            }
            set
            {
                m_languageSettings = value;
            }
        }
        #endregion

        #region Overrides
        public override void Copy( EditorSettingsBase settings )
        {
            base.Copy( settings );

            if ( settings != null )
            {
                if ( settings is LanguageDefaultEditorSettingsBase )
                {
                    LanguageDefaultEditorSettingsBase defaultSettings = settings as LanguageDefaultEditorSettingsBase;

                    if ( this.Files != null )
                    {
                        this.Files.Copy( defaultSettings.Files );
                    }

                    if ( this.Toolbar != null )
                    {
                        this.Toolbar.Copy( defaultSettings.Toolbar );
                    }

                    // only copy if the languages match
                    if ( this.LanguageName == defaultSettings.LanguageName )
                    {
                        this.CompilingSettingsList.Clear();
                        foreach ( CompilingSettings c in defaultSettings.CompilingSettingsList )
                        {
                            CompilingSettings copyC = c.Clone() as CompilingSettings;
                            this.CompilingSettingsList.Add( copyC );
                        }

                        if ( this.Language != null )
                        {
                            this.Language.Copy( defaultSettings.Language );
                        }
                    }
                }
                else if ( settings is ProjectEditorSettings )
                {
                    ProjectEditorSettings projectSettings = settings as ProjectEditorSettings;

                    // only copy if the languages match
                    if ( this.LanguageName == projectSettings.Language )
                    {
                        this.CompilingSettingsList.Clear();
                        foreach ( CompilingSettings c in projectSettings.CompilingSettingsList )
                        {
                            CompilingSettings copyC = c.Clone() as CompilingSettings;
                            this.CompilingSettingsList.Add( copyC );
                        }
                    }
                }
                else if ( settings is UserEditorSettings )
                {
                    UserEditorSettings userSettings = settings as UserEditorSettings;

                    if ( this.Files != null )
                    {
                        this.Files.Copy( userSettings.Files );
                    }

                    if ( this.Toolbar != null )
                    {
                        this.Toolbar.Copy( userSettings.Toolbar );
                    }

                    // only copy the compiling settings for this language
                    string language = userSettings.CurrentCompilingLanguage;
                    userSettings.CurrentCompilingLanguage = this.LanguageName;

                    this.CompilingSettingsList.Clear();
                    foreach ( CompilingSettings c in userSettings.CurrentCompilingSettingsList )
                    {
                        CompilingSettings copyC = c.Clone() as CompilingSettings;
                        this.CompilingSettingsList.Add( copyC );
                    }

                    userSettings.CurrentCompilingLanguage = language;

                    if ( this.Language != null )
                    {
                        // only copy the language settings for this language
                        foreach ( LanguageSettings l in userSettings.LanguageSettingsList )
                        {
                            if ( l.Language == this.LanguageName )
                            {
                                this.Language.Copy( l );
                                break;
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Abstract Functions
        public abstract Type GetCompilingSettingsDerivedType();

        public abstract CompilingSettings CreateCompilingTabSettings();

        public abstract Type GetLanguageSettingsDerivedType();

        public abstract LanguageSettings CreateLanguageTabSettings();
        #endregion
    }
    #endregion
}
