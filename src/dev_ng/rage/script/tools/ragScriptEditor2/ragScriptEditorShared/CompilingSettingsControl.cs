using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace ragScriptEditorShared
{
    public partial class CompilingSettingsControl : ragScriptEditorShared.SettingsControlBase
    {
        public CompilingSettingsControl()
        {
            InitializeComponent();

            m_textInputWindow = new SimpleTextInputWindow();
        }

        #region Variables
        private string m_configurationName;        
        private string m_includeFileOpenFileDialogFilter;
        private string m_compilerExecutableOpenFileDialogFilter;
        private string m_resourceExecutableOpenFileDialogFilter;

        private string m_projectFilename;

        private SimpleTextInputWindow m_textInputWindow;
        #endregion

        #region Properties
        public string ProjectFilename
        {
            get
            {
                return m_projectFilename;
            }
            set
            {
                if ( m_projectFilename != value )
                {
                    m_projectFilename = value;

                    OnProjectFilenameChanged();
                }
            }
        }

		public bool OutputDirectoryEnabled
		{
			get
			{
				return outputDirectoryTextBox.Enabled;
			}
			set
			{
				label4.Enabled = value;
				outputDirectoryTextBox.Enabled = value;
				outputDirectoryBrowseButton.Enabled = value;
			}
		}

		public string OutputDirectory
		{
			get
			{
				return outputDirectoryTextBox.Text;
			}
			set
			{
				outputDirectoryTextBox.Text = value;
			}
		}

		#endregion

        #region Overrides
        public override SettingsBase TabSettings
        {
            get
            {
                CompilingSettings compilingSettings = new CompilingSettings();

                compilingSettings.CompilerExecutable = this.compilerExecutableTextBox.Text.Trim();
                compilingSettings.ResourceExecutable = this.resourceExecutableTextBox.Text.Trim();
                compilingSettings.ConfigurationName = m_configurationName;
                compilingSettings.IncludeFile = this.includeFileTextBox.Text.Trim();
                compilingSettings.IncludePath = this.includePathTextBox.Text.Trim();
                compilingSettings.OutputDirectory = this.outputDirectoryTextBox.Text.Trim();
                compilingSettings.BuildDirectory = this.buildDirectoryTextBox.Text.Trim();
                compilingSettings.PostCompileCommand = this.postCompileCommandTextBox.Text.Trim();

                return compilingSettings;
            }
            set
            {
                if ( value is CompilingSettings )
                {
                    m_invokeSettingsChanged = false;

                    CompilingSettings compilingSettings = value as CompilingSettings;

                    m_configurationName = compilingSettings.ConfigurationName;
                    m_includeFileOpenFileDialogFilter = compilingSettings.IncludeFileOpenFileDialogFilter;
                    m_compilerExecutableOpenFileDialogFilter = compilingSettings.CompilerExecutableOpenFileDialogFilter;
                    m_resourceExecutableOpenFileDialogFilter = compilingSettings.ResourceExecutableOpenFileDialogFilter;

                    this.includeFileTextBox.Text = compilingSettings.XmlIncludeFile;
                    
                    if ( String.IsNullOrEmpty( m_includeFileOpenFileDialogFilter ) )
                    {
                        this.includeFileTextBox.Enabled = false;
                        this.includeFileBrowseButton.Enabled = false;
                    }
                    else
                    {
                        this.includeFileTextBox.Enabled = true;
                        this.includeFileBrowseButton.Enabled = true;
                    }

                    this.compilerExecutableTextBox.Text = compilingSettings.CompilerExecutable;
                    this.outputDirectoryTextBox.Text = compilingSettings.OutputDirectory;
                    this.buildDirectoryTextBox.Text = compilingSettings.BuildDirectory;
                    this.postCompileCommandTextBox.Text = compilingSettings.PostCompileCommand;
                    
                    if ( String.IsNullOrEmpty( m_compilerExecutableOpenFileDialogFilter ) )
                    {
                        this.compilerExecutableTextBox.Enabled = false;
                        this.compilerExecutableBrowseButton.Enabled = false;

                        this.outputDirectoryTextBox.Enabled = false;
                        this.outputDirectoryBrowseButton.Enabled = false;
                    }
                    else
                    {
                        this.compilerExecutableTextBox.Enabled = true;
                        this.compilerExecutableBrowseButton.Enabled = true;

                        this.outputDirectoryTextBox.Enabled = true;
                        this.outputDirectoryBrowseButton.Enabled = true;
                    }

                    this.resourceExecutableTextBox.Text = compilingSettings.ResourceExecutable;

                    if (String.IsNullOrEmpty(m_resourceExecutableOpenFileDialogFilter))
                    {
                        this.resourceExecutableTextBox.Enabled = false;
                        this.resourceExecutableBrowseButton.Enabled = false;
                    }
                    else
                    {
                        this.resourceExecutableTextBox.Enabled = true;
                        this.resourceExecutableBrowseButton.Enabled = true;
                    }
                   
                    this.includePathTextBox.Text = compilingSettings.IncludePath;

                    m_invokeSettingsChanged = true;
                }
            }
        }
        #endregion

        #region Events
        public event EventHandler ProjectFilenameChanged;
        #endregion

        #region Event Dispatchers
        private void OnProjectFilenameChanged()
        {
            if ( m_invokeSettingsChanged && ( this.ProjectFilenameChanged != null ) )
            {
                this.ProjectFilenameChanged( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Event Handlers
        protected void SettingChanged( object sender, EventArgs e )
        {
            OnSettingsChanged();
        }

        private void includeFileBrowseButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Filter = m_includeFileOpenFileDialogFilter;
            this.openFileDialog.Title = "Select Include File";

            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.includeFileTextBox.Text = this.openFileDialog.FileName;
            }
        }

        private void includePathBrowseButton_Click( object sender, EventArgs e )
        {
            this.m_textInputWindow.Text = "Edit Includes";
            this.m_textInputWindow.TextContents = this.includePathTextBox.Text;

            DialogResult result = this.m_textInputWindow.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.includePathTextBox.Text = this.m_textInputWindow.TextContents;
            }
        }

        private void compilerExecutableBrowseButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Filter = m_compilerExecutableOpenFileDialogFilter;
            this.openFileDialog.Title = "Select Compiler Executable";

            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.compilerExecutableTextBox.Text = this.openFileDialog.FileName;
            }
        }

        private void resourceExecutableBrowseButton_Click(object sender, EventArgs e)
        {
            this.openFileDialog.Filter = m_resourceExecutableOpenFileDialogFilter;
            this.openFileDialog.Title = "Select Resource Executable";

            DialogResult result = this.openFileDialog.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                this.resourceExecutableTextBox.Text = this.openFileDialog.FileName;
            }
        }

        private void outputDirectoryBrowseButton_Click( object sender, EventArgs e )
        {
            this.folderBrowserDialog.Description = "Select Output Directory";
            this.folderBrowserDialog.ShowNewFolderButton = true;

            DialogResult result = this.folderBrowserDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.outputDirectoryTextBox.Text = this.folderBrowserDialog.SelectedPath;
            }
        }

        private void buildDirectoryBrowseButton_Click(object sender, EventArgs e)
        {
            this.folderBrowserDialog.Description = "Select Preview Directory";
            this.folderBrowserDialog.ShowNewFolderButton = true;

            DialogResult result = this.folderBrowserDialog.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                this.buildDirectoryTextBox.Text = this.folderBrowserDialog.SelectedPath;
            }
        }

        private void postCompileCommandBrowseButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Filter = "Executable Files (*.exe;*.bat)|*.exe;*.bat|All Files (*.*)|*.*";
            this.openFileDialog.Title = "Select Post-Compile Command Executable";

            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.postCompileCommandTextBox.Text = this.openFileDialog.FileName;
            }
        }
        #endregion
    }
}

