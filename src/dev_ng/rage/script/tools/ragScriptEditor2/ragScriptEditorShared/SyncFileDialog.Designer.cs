namespace ragScriptEditorShared
{
    partial class SyncFileDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( SyncFileDialog ) );
            this.discardChangesButton = new System.Windows.Forms.Button();
            this.doNotSyncButton = new System.Windows.Forms.Button();
            this.saveAsButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.fileLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // discardChangesButton
            // 
            this.discardChangesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.discardChangesButton.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.discardChangesButton.Location = new System.Drawing.Point( 320, 113 );
            this.discardChangesButton.Name = "discardChangesButton";
            this.discardChangesButton.Size = new System.Drawing.Size( 75, 23 );
            this.discardChangesButton.TabIndex = 0;
            this.discardChangesButton.Text = "Discard";
            this.discardChangesButton.UseVisualStyleBackColor = true;
            // 
            // doNotSyncButton
            // 
            this.doNotSyncButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.doNotSyncButton.DialogResult = System.Windows.Forms.DialogResult.Ignore;
            this.doNotSyncButton.Location = new System.Drawing.Point( 401, 113 );
            this.doNotSyncButton.Name = "doNotSyncButton";
            this.doNotSyncButton.Size = new System.Drawing.Size( 75, 23 );
            this.doNotSyncButton.TabIndex = 1;
            this.doNotSyncButton.Text = "Don\'t Sync";
            this.doNotSyncButton.UseVisualStyleBackColor = true;
            // 
            // saveAsButton
            // 
            this.saveAsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveAsButton.DialogResult = System.Windows.Forms.DialogResult.Retry;
            this.saveAsButton.Location = new System.Drawing.Point( 482, 113 );
            this.saveAsButton.Name = "saveAsButton";
            this.saveAsButton.Size = new System.Drawing.Size( 75, 23 );
            this.saveAsButton.TabIndex = 2;
            this.saveAsButton.Text = "Save As...";
            this.saveAsButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 563, 113 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.AutoSize = true;
            this.descriptionLabel.Location = new System.Drawing.Point( 12, 32 );
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size( 630, 78 );
            this.descriptionLabel.TabIndex = 4;
            this.descriptionLabel.Text = resources.GetString( "descriptionLabel.Text" );
            // 
            // fileLabel
            // 
            this.fileLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.fileLabel.Location = new System.Drawing.Point( 13, 4 );
            this.fileLabel.Name = "fileLabel";
            this.fileLabel.Size = new System.Drawing.Size( 625, 28 );
            this.fileLabel.TabIndex = 5;
            this.fileLabel.Text = "The file \'%s\' is newer on the source control server than your local copy.";
            // 
            // SyncFileDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size( 650, 148 );
            this.Controls.Add( this.fileLabel );
            this.Controls.Add( this.descriptionLabel );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.saveAsButton );
            this.Controls.Add( this.doNotSyncButton );
            this.Controls.Add( this.discardChangesButton );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SyncFileDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "File is Newer";
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button discardChangesButton;
        private System.Windows.Forms.Button doNotSyncButton;
        private System.Windows.Forms.Button saveAsButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.Label fileLabel;
    }
}