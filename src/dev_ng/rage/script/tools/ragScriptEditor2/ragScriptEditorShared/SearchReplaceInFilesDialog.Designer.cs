namespace ragScriptEditorShared
{
    partial class SearchReplaceInFilesDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.findWhatLabel = new System.Windows.Forms.Label();
            this.findWhatComboBox = new System.Windows.Forms.ComboBox();
            this.replaceWithLabel = new System.Windows.Forms.Label();
            this.replaceWithComboBox = new System.Windows.Forms.ComboBox();
            this.findButton = new System.Windows.Forms.Button();
            this.findNextButton = new System.Windows.Forms.Button();
            this.replaceButton = new System.Windows.Forms.Button();
            this.replaceAllButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.matchCaseCheckBox = new System.Windows.Forms.CheckBox();
            this.matchWholeWordCheckBox = new System.Windows.Forms.CheckBox();
            this.useCheckBox = new System.Windows.Forms.CheckBox();
            this.useComboBox = new System.Windows.Forms.ComboBox();
            this.keepModifiedFilesOpenAfterReplaceAllCheckBox = new System.Windows.Forms.CheckBox();
            this.lookInLabel = new System.Windows.Forms.Label();
            this.lookInComboBox = new System.Windows.Forms.ComboBox();
            this.lookInSubfoldersCheckBox = new System.Windows.Forms.CheckBox();
            this.fileTypesLabel = new System.Windows.Forms.Label();
            this.fileTypesComboBox = new System.Windows.Forms.ComboBox();
            this.browseLookInButton = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.searchReplaceComponent = new ragScriptEditorShared.SearchReplaceComponent( this.components );
            this.SuspendLayout();
            // 
            // findWhatLabel
            // 
            this.findWhatLabel.AutoSize = true;
            this.findWhatLabel.Location = new System.Drawing.Point( 28, 15 );
            this.findWhatLabel.Name = "findWhatLabel";
            this.findWhatLabel.Size = new System.Drawing.Size( 56, 13 );
            this.findWhatLabel.TabIndex = 0;
            this.findWhatLabel.Text = "Fi&nd what:";
            // 
            // findWhatComboBox
            // 
            this.findWhatComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.findWhatComboBox.FormattingEnabled = true;
            this.findWhatComboBox.Location = new System.Drawing.Point( 90, 12 );
            this.findWhatComboBox.Name = "findWhatComboBox";
            this.findWhatComboBox.Size = new System.Drawing.Size( 317, 21 );
            this.findWhatComboBox.TabIndex = 1;
            this.findWhatComboBox.TextUpdate += new System.EventHandler( this.findWhatComboBox_TextUpdate );
            // 
            // replaceWithLabel
            // 
            this.replaceWithLabel.AutoSize = true;
            this.replaceWithLabel.Location = new System.Drawing.Point( 12, 42 );
            this.replaceWithLabel.Name = "replaceWithLabel";
            this.replaceWithLabel.Size = new System.Drawing.Size( 72, 13 );
            this.replaceWithLabel.TabIndex = 3;
            this.replaceWithLabel.Text = "Re&place with:";
            // 
            // replaceWithComboBox
            // 
            this.replaceWithComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceWithComboBox.FormattingEnabled = true;
            this.replaceWithComboBox.Location = new System.Drawing.Point( 90, 39 );
            this.replaceWithComboBox.Name = "replaceWithComboBox";
            this.replaceWithComboBox.Size = new System.Drawing.Size( 317, 21 );
            this.replaceWithComboBox.TabIndex = 4;
            // 
            // findButton
            // 
            this.findButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.findButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.findButton.Location = new System.Drawing.Point( 413, 10 );
            this.findButton.Name = "findButton";
            this.findButton.Size = new System.Drawing.Size( 75, 23 );
            this.findButton.TabIndex = 2;
            this.findButton.Text = "&Find";
            this.findButton.UseVisualStyleBackColor = true;
            this.findButton.Click += new System.EventHandler( this.findButton_Click );
            // 
            // findNextButton
            // 
            this.findNextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.findNextButton.Location = new System.Drawing.Point( 413, 37 );
            this.findNextButton.Name = "findNextButton";
            this.findNextButton.Size = new System.Drawing.Size( 75, 23 );
            this.findNextButton.TabIndex = 5;
            this.findNextButton.Text = "Find Ne&xt";
            this.findNextButton.UseVisualStyleBackColor = true;
            this.findNextButton.Click += new System.EventHandler( this.findNextButton_Click );
            // 
            // replaceButton
            // 
            this.replaceButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceButton.Location = new System.Drawing.Point( 413, 66 );
            this.replaceButton.Name = "replaceButton";
            this.replaceButton.Size = new System.Drawing.Size( 75, 23 );
            this.replaceButton.TabIndex = 6;
            this.replaceButton.Text = "&Replace";
            this.replaceButton.UseVisualStyleBackColor = true;
            this.replaceButton.Click += new System.EventHandler( this.replaceButton_Click );
            // 
            // replaceAllButton
            // 
            this.replaceAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceAllButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.replaceAllButton.Location = new System.Drawing.Point( 413, 95 );
            this.replaceAllButton.Name = "replaceAllButton";
            this.replaceAllButton.Size = new System.Drawing.Size( 75, 23 );
            this.replaceAllButton.TabIndex = 7;
            this.replaceAllButton.Text = "Replace &All";
            this.replaceAllButton.UseVisualStyleBackColor = true;
            this.replaceAllButton.Click += new System.EventHandler( this.replaceAllButton_Click );
            // 
            // stopButton
            // 
            this.stopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.stopButton.Location = new System.Drawing.Point( 413, 124 );
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size( 75, 23 );
            this.stopButton.TabIndex = 8;
            this.stopButton.Text = "&Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler( this.stopButton_Click );
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point( 413, 174 );
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size( 75, 23 );
            this.closeButton.TabIndex = 9;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // matchCaseCheckBox
            // 
            this.matchCaseCheckBox.AutoSize = true;
            this.matchCaseCheckBox.Location = new System.Drawing.Point( 12, 70 );
            this.matchCaseCheckBox.Name = "matchCaseCheckBox";
            this.matchCaseCheckBox.Size = new System.Drawing.Size( 82, 17 );
            this.matchCaseCheckBox.TabIndex = 10;
            this.matchCaseCheckBox.Text = "Match &case";
            this.matchCaseCheckBox.UseVisualStyleBackColor = true;
            // 
            // matchWholeWordCheckBox
            // 
            this.matchWholeWordCheckBox.AutoSize = true;
            this.matchWholeWordCheckBox.Location = new System.Drawing.Point( 12, 99 );
            this.matchWholeWordCheckBox.Name = "matchWholeWordCheckBox";
            this.matchWholeWordCheckBox.Size = new System.Drawing.Size( 113, 17 );
            this.matchWholeWordCheckBox.TabIndex = 11;
            this.matchWholeWordCheckBox.Text = "Match &whole word";
            this.matchWholeWordCheckBox.UseVisualStyleBackColor = true;
            // 
            // useCheckBox
            // 
            this.useCheckBox.AutoSize = true;
            this.useCheckBox.Location = new System.Drawing.Point( 148, 70 );
            this.useCheckBox.Name = "useCheckBox";
            this.useCheckBox.Size = new System.Drawing.Size( 45, 17 );
            this.useCheckBox.TabIndex = 12;
            this.useCheckBox.Text = "&Use";
            this.useCheckBox.UseVisualStyleBackColor = true;
            this.useCheckBox.CheckedChanged += new System.EventHandler( this.useCheckBox_CheckedChanged );
            // 
            // useComboBox
            // 
            this.useComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.useComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.useComboBox.FormattingEnabled = true;
            this.useComboBox.Items.AddRange( new object[] {
            "Regular expressions",
            "Wildcards"} );
            this.useComboBox.Location = new System.Drawing.Point( 199, 68 );
            this.useComboBox.Name = "useComboBox";
            this.useComboBox.Size = new System.Drawing.Size( 165, 21 );
            this.useComboBox.TabIndex = 13;
            // 
            // keepModifiedFilesOpenAfterReplaceAllCheckBox
            // 
            this.keepModifiedFilesOpenAfterReplaceAllCheckBox.AutoSize = true;
            this.keepModifiedFilesOpenAfterReplaceAllCheckBox.Location = new System.Drawing.Point( 148, 99 );
            this.keepModifiedFilesOpenAfterReplaceAllCheckBox.Name = "keepModifiedFilesOpenAfterReplaceAllCheckBox";
            this.keepModifiedFilesOpenAfterReplaceAllCheckBox.Size = new System.Drawing.Size( 222, 17 );
            this.keepModifiedFilesOpenAfterReplaceAllCheckBox.TabIndex = 14;
            this.keepModifiedFilesOpenAfterReplaceAllCheckBox.Text = "Keep &modified files open after Replace All";
            this.keepModifiedFilesOpenAfterReplaceAllCheckBox.UseVisualStyleBackColor = true;
            // 
            // lookInLabel
            // 
            this.lookInLabel.AutoSize = true;
            this.lookInLabel.Location = new System.Drawing.Point( 39, 129 );
            this.lookInLabel.Name = "lookInLabel";
            this.lookInLabel.Size = new System.Drawing.Size( 45, 13 );
            this.lookInLabel.TabIndex = 15;
            this.lookInLabel.Text = "&Look in:";
            // 
            // lookInComboBox
            // 
            this.lookInComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lookInComboBox.FormattingEnabled = true;
            this.lookInComboBox.Location = new System.Drawing.Point( 90, 126 );
            this.lookInComboBox.Name = "lookInComboBox";
            this.lookInComboBox.Size = new System.Drawing.Size( 274, 21 );
            this.lookInComboBox.TabIndex = 16;
            this.lookInComboBox.TextUpdate += new System.EventHandler( this.lookInComboBox_TextUpdate );
            // 
            // lookInSubfoldersCheckBox
            // 
            this.lookInSubfoldersCheckBox.AutoSize = true;
            this.lookInSubfoldersCheckBox.Location = new System.Drawing.Point( 90, 153 );
            this.lookInSubfoldersCheckBox.Name = "lookInSubfoldersCheckBox";
            this.lookInSubfoldersCheckBox.Size = new System.Drawing.Size( 112, 17 );
            this.lookInSubfoldersCheckBox.TabIndex = 18;
            this.lookInSubfoldersCheckBox.Text = "Look in s&ubfolders";
            this.lookInSubfoldersCheckBox.UseVisualStyleBackColor = true;
            // 
            // fileTypesLabel
            // 
            this.fileTypesLabel.AutoSize = true;
            this.fileTypesLabel.Location = new System.Drawing.Point( 30, 179 );
            this.fileTypesLabel.Name = "fileTypesLabel";
            this.fileTypesLabel.Size = new System.Drawing.Size( 54, 13 );
            this.fileTypesLabel.TabIndex = 19;
            this.fileTypesLabel.Text = "File &types:";
            // 
            // fileTypesComboBox
            // 
            this.fileTypesComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.fileTypesComboBox.FormattingEnabled = true;
            this.fileTypesComboBox.Location = new System.Drawing.Point( 90, 176 );
            this.fileTypesComboBox.Name = "fileTypesComboBox";
            this.fileTypesComboBox.Size = new System.Drawing.Size( 274, 21 );
            this.fileTypesComboBox.TabIndex = 20;
            // 
            // browseLookInButton
            // 
            this.browseLookInButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseLookInButton.Location = new System.Drawing.Point( 370, 124 );
            this.browseLookInButton.Name = "browseLookInButton";
            this.browseLookInButton.Size = new System.Drawing.Size( 24, 23 );
            this.browseLookInButton.TabIndex = 17;
            this.browseLookInButton.Text = "&...";
            this.browseLookInButton.UseVisualStyleBackColor = true;
            this.browseLookInButton.Click += new System.EventHandler( this.browseLookInButton_Click );
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.Description = "Specify the folder to search from.";
            this.folderBrowserDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.folderBrowserDialog.ShowNewFolderButton = false;
            // 
            // searchReplaceComponent
            // 
            this.searchReplaceComponent.Title = "Find/Replace In Files";
            this.searchReplaceComponent.SearchComplete += new ragScriptEditorShared.SearchCompleteEventHandler( this.searchReplaceComponent_SearchComplete );
            // 
            // SearchReplaceInFilesDialog
            // 
            this.AcceptButton = this.findButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size( 500, 209 );
            this.Controls.Add( this.browseLookInButton );
            this.Controls.Add( this.fileTypesComboBox );
            this.Controls.Add( this.fileTypesLabel );
            this.Controls.Add( this.lookInSubfoldersCheckBox );
            this.Controls.Add( this.lookInComboBox );
            this.Controls.Add( this.lookInLabel );
            this.Controls.Add( this.keepModifiedFilesOpenAfterReplaceAllCheckBox );
            this.Controls.Add( this.useComboBox );
            this.Controls.Add( this.useCheckBox );
            this.Controls.Add( this.matchWholeWordCheckBox );
            this.Controls.Add( this.matchCaseCheckBox );
            this.Controls.Add( this.closeButton );
            this.Controls.Add( this.stopButton );
            this.Controls.Add( this.replaceAllButton );
            this.Controls.Add( this.replaceButton );
            this.Controls.Add( this.findNextButton );
            this.Controls.Add( this.findButton );
            this.Controls.Add( this.replaceWithComboBox );
            this.Controls.Add( this.replaceWithLabel );
            this.Controls.Add( this.findWhatComboBox );
            this.Controls.Add( this.findWhatLabel );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SearchReplaceInFilesDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Find/Replace In Files *NEW*";
            this.Activated += new System.EventHandler( this.SearchReplaceInFilesDialog_Activated );
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.SearchReplaceInFilesDialog_FormClosing );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label findWhatLabel;
        private System.Windows.Forms.ComboBox findWhatComboBox;
        private System.Windows.Forms.Label replaceWithLabel;
        private System.Windows.Forms.ComboBox replaceWithComboBox;
        private System.Windows.Forms.Button findButton;
        private System.Windows.Forms.Button findNextButton;
        private System.Windows.Forms.Button replaceButton;
        private System.Windows.Forms.Button replaceAllButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.CheckBox matchCaseCheckBox;
        private System.Windows.Forms.CheckBox matchWholeWordCheckBox;
        private System.Windows.Forms.CheckBox useCheckBox;
        private System.Windows.Forms.ComboBox useComboBox;
        private System.Windows.Forms.CheckBox keepModifiedFilesOpenAfterReplaceAllCheckBox;
        private System.Windows.Forms.Label lookInLabel;
        private System.Windows.Forms.ComboBox lookInComboBox;
        private System.Windows.Forms.CheckBox lookInSubfoldersCheckBox;
        private System.Windows.Forms.Label fileTypesLabel;
        private System.Windows.Forms.ComboBox fileTypesComboBox;
        private System.Windows.Forms.Button browseLookInButton;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private SearchReplaceComponent searchReplaceComponent;
    }
}