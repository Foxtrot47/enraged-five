using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ragScriptEditorShared
{
    public partial class ProjectSettingsControl : SettingsControlBase
    {
        public ProjectSettingsControl()
        {
            InitializeComponent();
        }

        #region Overrides
        public override SettingsBase TabSettings
        {
            get
            {
                ProjectSettings projectSettings = new ProjectSettings();
                projectSettings.AutomaticSorting = this.automaticSortngCheckBox.Checked;
                projectSettings.StartDebugCommand = this.startDebugCommandTextBox.Text;

                return projectSettings;
            }
            set
            {
                if ( value is ProjectSettings )
                {
                    m_invokeSettingsChanged = false;

                    ProjectSettings projectSettings = value as ProjectSettings;

                    this.automaticSortngCheckBox.Checked = projectSettings.AutomaticSorting;
                    this.startDebugCommandTextBox.Text = projectSettings.StartDebugCommand;

                    m_invokeSettingsChanged = true;
                }
            }
        }
        #endregion

        #region Event Handlers
        protected void SettingChanged( object sender, EventArgs e )
        {
            OnSettingsChanged();
        }

        private void startDebugCommandBrowseButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Filter = "Executable Files (*.exe;*.bat)|*.exe;*.bat|All Files (*.*)|*.*";
            this.openFileDialog.Title = "Select Start Debug Command Executable";

            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.startDebugCommandTextBox.Text = this.openFileDialog.FileName;
            }
        }
        #endregion
    }
}
