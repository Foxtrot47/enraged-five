﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ragScriptEditorShared
{
    public partial class SimpleTextInputWindow : Form
    {
        public SimpleTextInputWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the input window's text contents
        /// </summary>
        public String TextContents
        {
            get
            {
                return this.textContents.Text;
            }
            set
            {
                this.textContents.Text = value;
            }
        }
    }
}
