using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Forms;

namespace ragScriptEditorShared 
{
    public partial class NewProjectForm : Form
    {
        protected NewProjectForm()
        {
            InitializeComponent();
        }

        public NewProjectForm( string directory, List<string> languages )
        {
            InitializeComponent();

            m_directory = directory;
            if ( !m_directory.EndsWith( "\\" ) )
            {
                m_directory += "\\";
            }

            m_languages = languages;
        }

        /// <summary>
        /// Instantiates NewProjectForm and launches it as a dialog.
        /// </summary>
        /// <param name="parentControl"></param>
        /// <param name="directory"></param>
        /// <param name="languages"></param>
        /// <param name="projectName">return by reference the new project's name</param>
        /// <param name="languageName">return by reference the new project's language</param>
        /// <returns>false if cancelled</returns>
        public static bool ShowIt( Control parentControl, string directory, List<string> languages, ref string projectName, ref string languageName )
        {
            NewProjectForm newProjForm = new NewProjectForm( directory, languages );

            DialogResult result = newProjForm.ShowDialog( parentControl );
            if ( result == DialogResult.OK )
            {
                projectName = newProjForm.ProjectName;
                languageName = newProjForm.LanguageName;
                return true;
            }

            return false;
        }

        #region Variables
        private string m_directory = string.Empty;
        private List<string> m_languages;
        #endregion

        #region Properties
        public string ProjectName
        {
            get
            {
                return this.projectNameTextBox.Text.Trim();
            }
        }

        public string LanguageName
        {
            get
            {
                return this.languagesComboBox.Text;
            }
        }
        #endregion        

        #region Event Handlers
        private void okButton_Click( object sender, EventArgs e )
        {
            string name = projectNameTextBox.Text.Trim();
            if ( name == string.Empty )
            {
                this.DialogResult = DialogResult.None;
            }

            if ( name.IndexOfAny( Path.GetInvalidFileNameChars() ) > -1 )
            {
                rageMessageBox.ShowExclamation( this,
                    String.Format( "There are invalid characters in the project file name.\nThe following characters are not allowed:\n{0}", Path.GetInvalidFileNameChars() ),
                    "Invalid File Name" );
                this.DialogResult = DialogResult.None;
            }

            string filename = String.Format( "{0}{1}.scproj ", m_directory, name );
            if ( File.Exists( filename ) )
            {
                rageMessageBox.ShowExclamation( this, "A file by that name already exists.  Please type another project name", "File Exists" );
                this.DialogResult = DialogResult.None;
            }
        }

        private void projectNameTextBox_TextChanged( object sender, EventArgs e )
        {
            this.infoLabel.Text = String.Format( "'{0}{1}.scproj' will be created.", m_directory, this.projectNameTextBox.Text.Trim() );
        }
        #endregion

        private void NewProjectForm_Load( object sender, EventArgs e )
        {
            this.infoLabel.Text = "'" + m_directory + ".scproj' will be created.";

            this.languagesComboBox.Items.Clear();
            foreach ( string language in m_languages )
            {
                this.languagesComboBox.Items.Add( language );
            }

            this.languagesComboBox.SelectedIndex = 0;
        }
    }
}