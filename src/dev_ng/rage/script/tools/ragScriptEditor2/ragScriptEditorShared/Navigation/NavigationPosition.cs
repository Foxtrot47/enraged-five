﻿//---------------------------------------------------------------------------------------------
// <copyright file="NavigationPosition.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace ragScriptEditorShared.Navigation
{
    using System;
    using ActiproSoftware.SyntaxEditor;
    
    /// <summary>
    /// A file and its location. Representing the spot in the codebase where the cursor was.
    /// </summary>
    public class NavigationPosition : IEquatable<NavigationPosition>
    {
        /// <summary>
        /// Gets the filename where the caret was positioned.
        /// </summary>
        public string Filename { get; }

        /// <summary>
        /// Gets the document position of the caret.
        /// </summary>
        public int Offset { get; }

        /// <summary>
        /// Create a new position object from a <see cref="SyntaxEditor"/> instance.
        /// </summary>
        /// <param name="editor"></param>
        public NavigationPosition(SyntaxEditor editor)
            : this(editor.Document.Filename, editor.Caret.Offset)
        {
        }

        /// <summary>
        /// Create a new position object from its parts.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="offset"></param>
        public NavigationPosition(string filename, int offset)
        {
            if (filename == null)
            {
                throw new ArgumentNullException(nameof(filename));
            }

            this.Filename = filename;
            this.Offset = offset;
        }

        /// <inheritdoc />
        public bool Equals(NavigationPosition other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return string.Equals(this.Filename, other.Filename, StringComparison.OrdinalIgnoreCase) && this.Offset == other.Offset;
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return this.Equals((NavigationPosition) obj);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                return (StringComparer.OrdinalIgnoreCase.GetHashCode(this.Filename) * 397) ^ this.Offset.GetHashCode();
            }
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.Filename}({this.Offset})";
        }
    }
}
