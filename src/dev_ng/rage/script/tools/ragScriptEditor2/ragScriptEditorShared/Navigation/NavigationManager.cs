﻿//---------------------------------------------------------------------------------------------
// <copyright file="NavigationManager.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace ragScriptEditorShared.Navigation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Stores a list of locations so the user can navigate forwards and backwards after taking
    /// a major action such as jumping to a new file or navigating away from the last location.
    /// </summary>
    public class NavigationManager
    {
        // Arbitrary, change if necessary.
        private const int MaxNumPositions = 64;

        private readonly IList<NavigationPosition> _positions;
        private int _index;
        private int _scopeDepth;

        /// <summary>
        /// Gets a value indicating whether the user can navigate forward [need one extra slot as we are looking forward when we go forward]
        /// </summary>
        public bool CanNavigateForward => this._index < this._positions.Count - 1;

        /// <summary>
        /// Gets a value indicating whether the user can navigate backward.
        /// </summary>
        public bool CanNavigateBack => this._index > 0;

        /// <summary>
        /// Gets a value indicating whether the manager is currently in a navigation scope. If true,
        /// no position markers will be added.
        /// </summary>
        public bool InNavigationScope => this._scopeDepth > 0;

        /// <summary>
        /// Event handler for the <see cref="OnNavigationChanged"/> event.
        /// </summary>
        /// <param name="source">The manager</param>
        /// <param name="e">Event</param>
        public delegate void NavigationChangedEventHandler(Object source, EventArgs e);

        /// <summary>
        /// Navigation change event. This event fires whenever a) nav items are modified or b) navigation occurs.
        /// It can be used to change the enabled state of buttons as it always fires when a button's state could change.
        /// </summary>
        public event NavigationChangedEventHandler OnNavigationChanged;

        /// <summary>
        /// Construct a new navigation manager.
        /// </summary>
        public NavigationManager()
        {
            this._positions = new List<NavigationPosition>();
        }

        /// <summary>
        /// Push a new position into the navigation stack. This will do nothing if already inside a navigation scope.
        /// </summary>
        /// <param name="position"></param>
        public bool AddPositionMarker(NavigationPosition position)
        {
            if (this.InNavigationScope || position == null)
            {
                return false;
            }

            // Avoid duplicates next to each other
            if (this._positions.Any() && this._index > 0 && this._positions[this._index - 1].Equals(position))
            {
                return false;
            }

            // Delete all positions after and including the current position. Adding a marker destroys the forward history.
            while (this._index < this._positions.Count)
            {
                this._positions.RemoveAt(this._positions.Count - 1);
            }

            // Delete all positions older than a maximum count and offset the index so it still points to the same item.
            while (this._positions.Count > MaxNumPositions)
            {
                this._positions.RemoveAt(0);

                // Due to above while loop index is always at the final position so below zero should never happen, but clamping regardless
                this._index = Math.Max(-1, this._index - 1);
            }

            this._positions.Add(position);
            this._index = this._positions.Count;
            this.OnNavigationChanged?.Invoke(this, EventArgs.Empty);
            return true;
        }

        /// <summary>
        /// Move the internal navigation position index forward and return the position.
        /// </summary>
        /// <returns>The position moved forward to.</returns>
        public NavigationPosition NavigateForward()
        {
            if (!this.CanNavigateForward)
            {
                return null;
            }

            NavigationPosition newPos;
            do
            {
                newPos = this._positions[this._index + 1];
                this._index++;
            } // Skip until we hit an existant file
            while (this.CanNavigateForward && !System.IO.File.Exists(newPos.Filename));

            this.OnNavigationChanged?.Invoke(this, EventArgs.Empty);
            return newPos;
        }

        /// <summary>
        /// Move the internal navigation position index backward and return the position.
        /// </summary>
        /// <param name="currentPosition">The current position. If we are at the end of the nav list, going back adds a marker at this position.
        /// This can be null to disable this functionality.</param>
        /// <returns>The position moved backward to.</returns>
        public NavigationPosition NavigateBack(NavigationPosition currentPosition)
        {
            if (!this.CanNavigateBack)
            {
                return null;
            }

            // If we are at the end of the history, add an extra marker at current pos and
            // silently move back to the same index as before.
            if (!this.CanNavigateForward && this.AddPositionMarker(currentPosition))
            {
                this._index--;
            }

            NavigationPosition newPos;
            do
            {
                newPos = this._positions[this._index - 1];
                this._index--;
            } // Skip until we hit an existant file
            while (this.CanNavigateBack && !System.IO.File.Exists(newPos.Filename));

            this.OnNavigationChanged?.Invoke(this, EventArgs.Empty);
            return newPos;
        }

        /// <summary>
        /// Register a scope with the manager. Until <see cref="PopScope"/> is called, all attempts to record a navigation position will be ignored.
        /// This prevents nested callbacks from registering duplicate navigation events.
        /// Recommend using <see cref="NavigationScope"/> for an <see cref="IDisposable"/> which can be managed with the 'using' keyword.
        /// </summary>
        public void PushScope()
        {
            this._scopeDepth++;
        }

        /// <summary>
        /// Unregister a scope with the manager.
        /// </summary>
        /// <param name="position"></param>
        public void PopScope(NavigationPosition position)
        {
            this._scopeDepth = Math.Max(0, this._scopeDepth - 1);

            // Internally will only add if scope is <= 0
            this.AddPositionMarker(position);
        }
    }
}
