﻿//---------------------------------------------------------------------------------------------
// <copyright file="NavigationScope.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2018. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace ragScriptEditorShared.Navigation
{
    using System;

    /// <summary>
    /// Represents a navigation scope. A navigation event will only be recorded once the outermost scope is left.
    /// This allows multiple callbacks to signal *intent* to record a navigation event, and be discarded if a scope already exists.
    /// This object is in charge of automatically calling <see cref="NavigationManager.PushScope"/> and
    /// <see cref="NavigationManager.PopScope"/> on construction and disposal.
    /// </summary>
    public class NavigationScope : IDisposable
    {
        private readonly NavigationManager _manager;
        private NavigationPosition _position;

        /// <summary>
        /// Create a new navigation scope, pushing the desired position to the manager inside a scope using <see cref="NavigationManager.PushScope"/>.
        /// The scope will be popped (and the position potentially recorded) when this object is disposed.
        /// </summary>
        /// <param name="manager">The manager.</param>
        /// <param name="position">The position to register with the scope.</param>
        public NavigationScope(NavigationManager manager, NavigationPosition position)
            : this(manager)
        {
            this._position = position;
        }

        /// <summary>
        /// Create a new navigation scope with no explicitly desired position using <see cref="NavigationManager.PushScope"/>.
        /// The scope will be popped when this object is disposed. Use this to disable navigation.
        /// </summary>
        /// <param name="manager">The manager.</param>
        public NavigationScope(NavigationManager manager)
        {
            this._manager = manager;
            this._manager.PushScope();
        }

        /// <summary>
        /// Cancels the registering of the position. When the scope is popped no position will be registered.
        /// For use in error cases when the navigation action was interrupted.
        /// </summary>
        public void Cancel()
        {
            this._position = null;
        }

        /// <summary>
        /// Dispose the scope. This internally calls <see cref="NavigationManager.PopScope"/> with the position.
        /// </summary>
        public void Dispose()
        {
            this._manager.PopScope(this._position);
        }
    }
}
