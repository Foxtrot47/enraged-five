namespace ragScriptEditorShared
{
    partial class SourceControlSettingsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.enabledCheckBox = new System.Windows.Forms.CheckBox();
            this.enabledGroupBox = new System.Windows.Forms.GroupBox();
            this.sourceControlProviderControl = new RSG.Base.Forms.SourceControlProviderControl();
            this.onCompileLabel = new System.Windows.Forms.Label();
            this.onCompileComboBox = new System.Windows.Forms.ComboBox();
            this.onSaveLabel = new System.Windows.Forms.Label();
            this.onSaveComboBox = new System.Windows.Forms.ComboBox();
            this.onEditLabel = new System.Windows.Forms.Label();
            this.onEditComboBox = new System.Windows.Forms.ComboBox();
            this.enabledGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // enabledCheckBox
            // 
            this.enabledCheckBox.AutoSize = true;
            this.enabledCheckBox.Location = new System.Drawing.Point( 3, 3 );
            this.enabledCheckBox.Name = "enabledCheckBox";
            this.enabledCheckBox.Size = new System.Drawing.Size( 65, 17 );
            this.enabledCheckBox.TabIndex = 0;
            this.enabledCheckBox.Text = "Enabled";
            this.enabledCheckBox.UseVisualStyleBackColor = true;
            this.enabledCheckBox.CheckedChanged += new System.EventHandler( this.enabledCheckBox_CheckedChanged );
            // 
            // enabledGroupBox
            // 
            this.enabledGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.enabledGroupBox.Controls.Add( this.sourceControlProviderControl );
            this.enabledGroupBox.Controls.Add( this.onCompileLabel );
            this.enabledGroupBox.Controls.Add( this.onCompileComboBox );
            this.enabledGroupBox.Controls.Add( this.onSaveLabel );
            this.enabledGroupBox.Controls.Add( this.onSaveComboBox );
            this.enabledGroupBox.Controls.Add( this.onEditLabel );
            this.enabledGroupBox.Controls.Add( this.onEditComboBox );
            this.enabledGroupBox.Location = new System.Drawing.Point( 3, 26 );
            this.enabledGroupBox.Name = "enabledGroupBox";
            this.enabledGroupBox.Size = new System.Drawing.Size( 428, 237 );
            this.enabledGroupBox.TabIndex = 1;
            this.enabledGroupBox.TabStop = false;
            // 
            // sourceControlProviderControl
            // 
            this.sourceControlProviderControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.sourceControlProviderControl.Location = new System.Drawing.Point( 7, 19 );
            this.sourceControlProviderControl.Name = "sourceControlProviderControl";
            this.sourceControlProviderControl.Size = new System.Drawing.Size( 415, 108 );
            this.sourceControlProviderControl.TabIndex = 0;
            this.sourceControlProviderControl.SettingChanged += new System.EventHandler( this.SettingChanged );
            // 
            // onCompileLabel
            // 
            this.onCompileLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.onCompileLabel.AutoSize = true;
            this.onCompileLabel.Location = new System.Drawing.Point( 213, 202 );
            this.onCompileLabel.Name = "onCompileLabel";
            this.onCompileLabel.Size = new System.Drawing.Size( 209, 13 );
            this.onCompileLabel.TabIndex = 6;
            this.onCompileLabel.Text = "when checked-in output files are compiled.";
            // 
            // onCompileComboBox
            // 
            this.onCompileComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.onCompileComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.onCompileComboBox.FormattingEnabled = true;
            this.onCompileComboBox.Items.AddRange( new object[] {
            "Prompt",
            "Automatically check out",
            "Overwrite"} );
            this.onCompileComboBox.Location = new System.Drawing.Point( 9, 199 );
            this.onCompileComboBox.Name = "onCompileComboBox";
            this.onCompileComboBox.Size = new System.Drawing.Size( 198, 21 );
            this.onCompileComboBox.TabIndex = 5;
            this.onCompileComboBox.SelectedIndexChanged += new System.EventHandler( this.SettingChanged );
            // 
            // onSaveLabel
            // 
            this.onSaveLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.onSaveLabel.AutoSize = true;
            this.onSaveLabel.Location = new System.Drawing.Point( 213, 175 );
            this.onSaveLabel.Name = "onSaveLabel";
            this.onSaveLabel.Size = new System.Drawing.Size( 163, 13 );
            this.onSaveLabel.TabIndex = 4;
            this.onSaveLabel.Text = "when checked-in files are saved.";
            // 
            // onSaveComboBox
            // 
            this.onSaveComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.onSaveComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.onSaveComboBox.FormattingEnabled = true;
            this.onSaveComboBox.Items.AddRange( new object[] {
            "Prompt",
            "Automatically check out",
            "Save As"} );
            this.onSaveComboBox.Location = new System.Drawing.Point( 9, 172 );
            this.onSaveComboBox.Name = "onSaveComboBox";
            this.onSaveComboBox.Size = new System.Drawing.Size( 198, 21 );
            this.onSaveComboBox.TabIndex = 3;
            this.onSaveComboBox.SelectedIndexChanged += new System.EventHandler( this.SettingChanged );
            // 
            // onEditLabel
            // 
            this.onEditLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.onEditLabel.AutoSize = true;
            this.onEditLabel.Location = new System.Drawing.Point( 213, 148 );
            this.onEditLabel.Name = "onEditLabel";
            this.onEditLabel.Size = new System.Drawing.Size( 163, 13 );
            this.onEditLabel.TabIndex = 2;
            this.onEditLabel.Text = "when checked-in files are edited.";
            // 
            // onEditComboBox
            // 
            this.onEditComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.onEditComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.onEditComboBox.FormattingEnabled = true;
            this.onEditComboBox.Items.AddRange( new object[] {
            "Prompt",
            "Automatically check out",
            "Do nothing"} );
            this.onEditComboBox.Location = new System.Drawing.Point( 9, 145 );
            this.onEditComboBox.Name = "onEditComboBox";
            this.onEditComboBox.Size = new System.Drawing.Size( 198, 21 );
            this.onEditComboBox.TabIndex = 1;
            this.onEditComboBox.SelectedIndexChanged += new System.EventHandler( this.SettingChanged );
            // 
            // SourceControlSettingsControl
            // 
            this.Controls.Add( this.enabledGroupBox );
            this.Controls.Add( this.enabledCheckBox );
            this.Name = "SourceControlSettingsControl";
            this.Size = new System.Drawing.Size( 434, 266 );
            this.enabledGroupBox.ResumeLayout( false );
            this.enabledGroupBox.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox enabledCheckBox;
        private System.Windows.Forms.GroupBox enabledGroupBox;
        private System.Windows.Forms.Label onCompileLabel;
        private System.Windows.Forms.ComboBox onCompileComboBox;
        private System.Windows.Forms.Label onSaveLabel;
        private System.Windows.Forms.ComboBox onSaveComboBox;
        private System.Windows.Forms.Label onEditLabel;
        private System.Windows.Forms.ComboBox onEditComboBox;
        private RSG.Base.Forms.SourceControlProviderControl sourceControlProviderControl;
    }
}
