namespace ragScriptEditorShared
{
    partial class NonGameFileSettingsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.showLineNumbersCheckBox = new System.Windows.Forms.CheckBox();
            this.documentOverviewGroupBox = new System.Windows.Forms.GroupBox();
            this.dropDownMenuRadioButton = new System.Windows.Forms.RadioButton();
            this.scrollButtonsRadioButton = new System.Windows.Forms.RadioButton();
            this.documentOverviewGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // showLineNumbersCheckBox
            // 
            this.showLineNumbersCheckBox.AutoSize = true;
            this.showLineNumbersCheckBox.Location = new System.Drawing.Point( 3, 49 );
            this.showLineNumbersCheckBox.Name = "showLineNumbersCheckBox";
            this.showLineNumbersCheckBox.Size = new System.Drawing.Size( 115, 17 );
            this.showLineNumbersCheckBox.TabIndex = 5;
            this.showLineNumbersCheckBox.Text = "Show line numbers";
            this.showLineNumbersCheckBox.UseVisualStyleBackColor = true;
            this.showLineNumbersCheckBox.CheckedChanged += new System.EventHandler( this.showLineNumbersCheckBox_CheckedChanged );
            // 
            // documentOverviewGroupBox
            // 
            this.documentOverviewGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.documentOverviewGroupBox.Controls.Add( this.scrollButtonsRadioButton );
            this.documentOverviewGroupBox.Controls.Add( this.dropDownMenuRadioButton );
            this.documentOverviewGroupBox.Location = new System.Drawing.Point( 3, 72 );
            this.documentOverviewGroupBox.Name = "documentOverviewGroupBox";
            this.documentOverviewGroupBox.Size = new System.Drawing.Size( 375, 49 );
            this.documentOverviewGroupBox.TabIndex = 6;
            this.documentOverviewGroupBox.TabStop = false;
            this.documentOverviewGroupBox.Text = "Document Overflow";
            // 
            // dropDownMenuRadioButton
            // 
            this.dropDownMenuRadioButton.AutoSize = true;
            this.dropDownMenuRadioButton.Location = new System.Drawing.Point( 6, 20 );
            this.dropDownMenuRadioButton.Name = "dropDownMenuRadioButton";
            this.dropDownMenuRadioButton.Size = new System.Drawing.Size( 109, 17 );
            this.dropDownMenuRadioButton.TabIndex = 0;
            this.dropDownMenuRadioButton.TabStop = true;
            this.dropDownMenuRadioButton.Text = "Drop Down Menu";
            this.dropDownMenuRadioButton.UseVisualStyleBackColor = true;
            this.dropDownMenuRadioButton.CheckedChanged += new System.EventHandler( this.dropDownMenuRadioButton_CheckedChanged );
            // 
            // scrollButtonsRadioButton
            // 
            this.scrollButtonsRadioButton.AutoSize = true;
            this.scrollButtonsRadioButton.Location = new System.Drawing.Point( 153, 19 );
            this.scrollButtonsRadioButton.Name = "scrollButtonsRadioButton";
            this.scrollButtonsRadioButton.Size = new System.Drawing.Size( 90, 17 );
            this.scrollButtonsRadioButton.TabIndex = 1;
            this.scrollButtonsRadioButton.TabStop = true;
            this.scrollButtonsRadioButton.Text = "Scroll Buttons";
            this.scrollButtonsRadioButton.UseVisualStyleBackColor = true;
            this.scrollButtonsRadioButton.CheckedChanged += new System.EventHandler( this.scrollButtonsRadioButton_CheckedChanged );
            // 
            // NonGameFileSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.Controls.Add( this.documentOverviewGroupBox );
            this.Controls.Add( this.showLineNumbersCheckBox );
            this.Name = "NonGameFileSettingsControl";
            this.Size = new System.Drawing.Size( 381, 124 );
            this.Controls.SetChildIndex( this.showLineNumbersCheckBox, 0 );
            this.Controls.SetChildIndex( this.documentOverviewGroupBox, 0 );
            this.documentOverviewGroupBox.ResumeLayout( false );
            this.documentOverviewGroupBox.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox showLineNumbersCheckBox;
        private System.Windows.Forms.GroupBox documentOverviewGroupBox;
        private System.Windows.Forms.RadioButton scrollButtonsRadioButton;
        private System.Windows.Forms.RadioButton dropDownMenuRadioButton;
    }
}
