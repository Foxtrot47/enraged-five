//#define OUTPUT_CLEAR_PARSE_REQUESTS

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using ActiproSoftware.SyntaxEditor;
using ActiproSoftware.SyntaxEditor.Commands;
using RSG.Base.Extensions;
using RSG.Base.Forms;
using RSG.Base.IO;
using RSG.Platform;
using RSG.Pipeline.BuildScript;
using TD.SandDock;
using ragScriptEditorShared.Navigation;

namespace ragScriptEditorShared
{
    public partial class ScriptEditor : UserControl
    {
        private bool m_openingFile = false;
        private Timer m_refreshTimer;

        protected ScriptEditor()
        {
            InitializeComponent();

            InitializeAdditionalComponents();
        }

        public ScriptEditor( Control parentControl, UserEditorSettings editorSettings, ProjectExplorer projectExplorer, EditorToolbar toolBar,
            SearchReplaceInFilesDialog searchReplaceInFilesDialog )
        {
            InitializeComponent();

            m_parentControl = parentControl;
            m_userEditorSettings = editorSettings;
            m_projectExplorer = projectExplorer;
            m_ToolBar = toolBar;

            // Hook up some events for the replace in files dialog (owned by Form1.. ugh)
            if (searchReplaceInFilesDialog != null)
            {
                searchReplaceInFilesDialog.SearchBegin += new SearchBeginEventHandler(searchReplaceDialog_SearchBegin);
                searchReplaceInFilesDialog.SearchComplete += new SearchCompleteEventHandler(searchReplaceDialog_SearchComplete);
            }

            m_refreshTimer = new Timer();
            m_refreshTimer.Interval = 3 * 1000; //3 seconds.
            m_refreshTimer.Tick += OnRefreshTimerElapsed;
            m_refreshTimer.Start();

            InitializeAdditionalComponents();
        }

        #region Init
        private void InitializeAdditionalComponents()
        {
            // FindReplace Form
            m_findReplaceForm = new FindReplaceForm( m_parentControl, m_FindReplaceOptions );
            m_findReplaceForm.DisableFileWatch = new FindReplace.DisableFileWatcherDelegate( DisableFileWatcher );
            m_findReplaceForm.EnableFileWatch = new FindReplace.EnableFileWatcherDelegate( EnableFileWatcher );
            m_findReplaceForm.InitSearchDocs = new FindReplaceForm.InitSearchDocsDelegate( findReplace_InitSearchDocuments );
            m_findReplaceForm.CancelIntellisense = new FindReplaceForm.CancelActiveIntellisenseDelegate( CancelActiveIntellisense );
            m_findReplaceForm.LoadFile = new FindReplace.LoadFileDelegate( LoadFile );
            m_findReplaceForm.ReloadFile = new FindReplace.ReloadFileDelegate( ReloadFile );


            // SearchReplace Dialog
            m_searchReplaceDialog = new SearchReplaceDialog();
            m_searchReplaceDialog.Init( this.ParentControl, this.FindReplaceOptions );
            m_searchReplaceDialog.CancelIntellisense += new EventHandler( searchReplaceDialog_CancelIntellisense );
            m_searchReplaceDialog.CanReplace = ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor;
            m_searchReplaceDialog.FindCurrentFilename += new FindCurrentFilenameEventHandler( searchReplaceDialog_FindCurrentFilename );
            m_searchReplaceDialog.FindOpenFilenames += new FindOpenFilenamesEventHandler( searchReplaceDialog_FindOpenFilenames );
            m_searchReplaceDialog.FindProjectFilenames += new FindProjectFilenamesEventHandler( searchReplaceDialog_FindProjectFilenames );
            m_searchReplaceDialog.FindSyntaxEditor += new SyntaxEditorEventHandler( searchReplaceDialog_FindSyntaxEditor );
            m_searchReplaceDialog.LoadFile += new LoadFileEventHandler( searchReplaceDialog_LoadFile );
            m_searchReplaceDialog.SaveDocument += new SaveDocumentEventHandler( searchReplaceDialog_SaveDocument );

            this.m_navManager.OnNavigationChanged += new NavigationManager.NavigationChangedEventHandler(OnNavigationChanged);

            // custom context menu items
            m_insertCodeSnippetMenuItem = new MenuItem( "Insert Code Snippet", new EventHandler( editor_ExecuteAppAction ) );
            m_insertCodeSnippetMenuItem.Tag = "EditInsertCodeSnippet";

            m_gotoDefinitionMenuItem = new MenuItem( "Go To Definition", new EventHandler( editor_ExecuteAppAction ) );
            m_gotoDefinitionMenuItem.Tag = "SearchGoToDefinition";

            m_outliningMenuItem = new MenuItem( "Outlining" );

            MenuItem menuItem = new MenuItem( "Start Automatic Outlining", new EventHandler( editor_ExecuteAppAction ) );
            menuItem.Tag = "OutliningStartAutomaticOutlining";
            m_outliningMenuItem.MenuItems.Add( menuItem );

            menuItem = new MenuItem( "Start Manual Outlining", new EventHandler( editor_ExecuteAppAction ) );
            menuItem.Tag = "OutliningStartManualOutlining";
            m_outliningMenuItem.MenuItems.Add( menuItem );

            menuItem = new MenuItem( "Stop Outlining", new EventHandler( editor_ExecuteAppAction ) );
            menuItem.Tag = "OutliningStopOutlining";
            m_outliningMenuItem.MenuItems.Add( menuItem );

            menuItem = new MenuItem( "-" );
            m_outliningMenuItem.MenuItems.Add( menuItem );

            menuItem = new MenuItem( "Hide Selection", new EventHandler( editor_ExecuteAppAction ) );
            menuItem.Tag = "OutliningHideSelection";
            m_outliningMenuItem.MenuItems.Add( menuItem );

            menuItem = new MenuItem( "Unhide Selection", new EventHandler( editor_ExecuteAppAction ) );
            menuItem.Tag = "OutliningStopHidingCurrent";
            m_outliningMenuItem.MenuItems.Add( menuItem );

            menuItem = new MenuItem( "-" );
            m_outliningMenuItem.MenuItems.Add( menuItem );

            menuItem = new MenuItem( "Toggle Outlining Expansion", new EventHandler( editor_ExecuteAppAction ) );
            menuItem.Tag = "OutliningToggleOutliningExpansion";
            m_outliningMenuItem.MenuItems.Add( menuItem );

            menuItem = new MenuItem( "Toggle All Outlining", new EventHandler( editor_ExecuteAppAction ) );
            menuItem.Tag = "OutliningToggleAllOutlining";
            m_outliningMenuItem.MenuItems.Add( menuItem );
        }
        #endregion

        #region Constants
        private const string c_newFileNamePrefix = "<new file";
        private const string c_compileErrorKey = "Compile error";
        private const int c_compileErrorDisplayPriority = 1000;
        #endregion

        #region Enums
        public enum DebuggerState
        {
            Stopped,
            Paused,
            Running
        }

        public enum CompileState
        {
            /// <summary>
            /// Stopped building.
            /// </summary>
            Stopped,
            /// <summary>
            /// Compiling with slow compiler
            /// </summary>
            Compiling,
            /// <summary>
            /// Building with incredibuild (parallel)
            /// </summary>
            BuildingIncredibuild,

        }
        #endregion

        #region Delegates
        public delegate ToolStripItem GetMenuItemByTagDelegate( AppAction appAction );
        public delegate ToolStripItem GetToolstripItemByTagDelegate( AppAction appAction );

        public delegate void DragDropFileDelegate();

        public delegate void HighlightLanguageEventHandler( object sender, SyntaxEditor editor );

        public delegate void FileLoadingEventHandler( object sender, FileLoadingEventArgs e );
        public delegate void FileLoadedEventHandler( object sender, FileLoadedEventArgs e );
        public delegate void FileSavingEventHandler( object sender, FileSavingEventArgs e );
        public delegate void FileSavedEventHandler( object sender, FileSavedEventArgs e );
        public delegate void FileClosingEventHandler( object sender, FileClosingEventArgs e );
        public delegate void FileClosedEventHandler( object sender, FileClosedEventArgs e );
        public delegate void FileAllPreOperationEventHandler( object sender, FileAllPreOperationEventArgs e );
        public delegate void FileAllPostOperationEventHandler( object sender, FileAllPostOperationEventArgs e );

        public delegate void ToggleBreakpointEventHandler( object sender, BreakpointEventArgs e );
        public delegate void ContinueDebuggerEventHandler( object sender, DebuggerEventArgs e );
        public delegate void PauseDebuggerEventHandler( object sender, DebuggerEventArgs e );
        public delegate void StepIntoDebuggerEventHandler( object sender, DebuggerEventArgs e );
        public delegate void StepOverDebuggerEventHandler( object sender, DebuggerEventArgs e );
        public delegate void StepOutDebuggerEventHandler( object sender, DebuggerEventArgs e );

        private delegate bool UpdateDocumentOutlineDelegate( SyntaxEditor editor, TreeView treeView );
        private delegate void CollapsedTreeViewNodesDelegate( TreeView treeView, List<OutlineNodeInfo> collapsedNodes );
        #endregion

        #region Events
        public event HighlightLanguageEventHandler HighlightLanguage;

        public event FileLoadingEventHandler FileLoading;
        public event FileLoadedEventHandler FileLoaded;
        public event FileAllPreOperationEventHandler FileLoadingAll;
        public event FileAllPostOperationEventHandler FileLoadedAll;
        public event FileSavingEventHandler FileSaving;
        public event FileSavedEventHandler FileSaved;
        public event FileAllPreOperationEventHandler FileSavingAll;
        public event FileAllPostOperationEventHandler FileSavedAll;
        public event FileClosingEventHandler FileClosing;
        public event FileClosedEventHandler FileClosed;
        public event FileAllPreOperationEventHandler FileClosingAll;
        public event FileAllPostOperationEventHandler FileClosedAll;

        public event ToggleBreakpointEventHandler ToggleBreakpoint;
        public event ContinueDebuggerEventHandler ContinueDebugger;
        public event PauseDebuggerEventHandler PauseDebugger;
        public event StepIntoDebuggerEventHandler StepIntoDebugger;
        public event StepOverDebuggerEventHandler StepOverDebugger;
        public event StepOutDebuggerEventHandler StepOutDebugger;

        private event IntellisenseSettingsChangedEventHandler IntellisenseSettingsChanged;
        #endregion

        #region Properties
        public Control ParentControl
        {
            get
            {
                return m_parentControl;
            }
        }

        public EditorToolbar ToolBar
        {
            get
            {
                return m_ToolBar;
            }
        }

        public ActiproSoftware.SyntaxEditor.SyntaxEditor CurrentSyntaxEditor
        {
            get
            {
                return m_CurrentSyntaxEditor;
            }
        }

        public FindReplaceForm FindReplaceForm
        {
            get
            {
                return m_findReplaceForm;
            }
        }

        public SearchReplaceDialog SearchReplaceDialog
        {
            get
            {
                return m_searchReplaceDialog;
            }
        }

        public FindReplaceOptions FindReplaceOptions
        {
            get
            {
                return m_FindReplaceOptions;
            }
        }

        public CompileState CurrentCompileState = CompileState.Stopped;

        public ragScriptEditorShared.InfoTipTextDelegate InfoTipTextDel
        {
            set
            {
                m_InfoTipTextDel = value;
            }
        }

        public DebuggerState DebugState
        {
            get
            {
                return m_debuggerState;
            }
            set
            {
                if ( this.CurrentSyntaxEditor != null )
                {
                    m_debuggerState = value;

                    bool debugging = ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Debugger;

                    EnableMenuItem( AppAction.DebugToggleBreakpoint, debugging && (m_debuggerState != DebuggerState.Stopped) );
                    EnableMenuItem( AppAction.DebugToggleBreakpointAllThreads, debugging && (m_debuggerState != DebuggerState.Stopped) );
                    EnableMenuItem( AppAction.DebugToggleBreakpointStopGame, debugging && (m_debuggerState != DebuggerState.Stopped) );
                    EnableMenuItem( AppAction.DebugToggleBreakpointAllThreadsStopGame, debugging && (m_debuggerState != DebuggerState.Stopped) );
                    EnableMenuItem( AppAction.DebugContinue, debugging && (m_debuggerState == DebuggerState.Paused) );
                    EnableMenuItem( AppAction.DebugPause, debugging && (m_debuggerState == DebuggerState.Running) );
                    EnableMenuItem( AppAction.DebugStepInto, debugging && (m_debuggerState == DebuggerState.Paused) );
                    EnableMenuItem( AppAction.DebugStepOver, debugging && (m_debuggerState == DebuggerState.Paused) );
                    EnableMenuItem( AppAction.DebugStepOut, debugging && (m_debuggerState == DebuggerState.Paused) );
                }
                else
                {
                    EnableMenuItem( AppAction.DebugToggleBreakpoint, false );
                    EnableMenuItem( AppAction.DebugToggleBreakpointAllThreads, false );
                    EnableMenuItem( AppAction.DebugToggleBreakpointStopGame, false );
                    EnableMenuItem( AppAction.DebugToggleBreakpointAllThreadsStopGame, false );
                    EnableMenuItem( AppAction.DebugContinue, false );
                    EnableMenuItem( AppAction.DebugPause, false );
                    EnableMenuItem( AppAction.DebugStepInto, false );
                    EnableMenuItem( AppAction.DebugStepOver, false );
                    EnableMenuItem( AppAction.DebugStepOut, false );
                }
            }
        }

        public bool AllowBreakpoints
        {
            get
            {
                return m_AllowBreakpoints;
            }

            set
            {
                m_AllowBreakpoints = value;
            }
        }

        public bool BreakPointsEnabled
        {
            get
            {
                return m_BreakPointsEnabled && (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Debugger);
            }

            set
            {
                m_BreakPointsEnabled = value;
            }
        }

        public LanguageDefaultEditorSettingsBase CurrentLanguageDefaultEditorSettings
        {
            get
            {
                if ( m_projectParserPlugin != null )
                {
                    return m_projectParserPlugin.DefaultEditorSettings;
                }

                IParserPlugin parserPlugin;
                if ( (this.CurrentSyntaxEditor != null) && !IsANewFileName( this.CurrentSyntaxEditor.Document.Filename )
                    && sm_parserPluginsByExtension.TryGetValue( Path.GetExtension( this.CurrentSyntaxEditor.Document.Filename ).ToLower(), out parserPlugin ) )
                {
                    return parserPlugin.DefaultEditorSettings;
                }

                return null;
            }
        }

        public Dictionary<string, LanguageDefaultEditorSettingsBase> DefaultEditorSettings
        {
            get
            {
                Dictionary<string, LanguageDefaultEditorSettingsBase> defaultSettings = new Dictionary<string, LanguageDefaultEditorSettingsBase>();
                foreach ( IParserPlugin parserPlugin in sm_parserPluginsByLanguage.Values )
                {
                    defaultSettings.Add( parserPlugin.DefaultEditorSettings.LanguageName, parserPlugin.DefaultEditorSettings );
                }

                return defaultSettings;
            }
        }

        public LanguageSettings CurrentLanguageSettings
        {
            get
            {
                if ( m_projectParserPlugin != null )
                {
                    return m_projectParserPlugin.CurrentLanguageSettings;
                }

                IParserPlugin parserPlugin;
                if ( (this.CurrentSyntaxEditor != null) && !IsANewFileName( this.CurrentSyntaxEditor.Document.Filename )
                    && sm_parserPluginsByExtension.TryGetValue( Path.GetExtension( this.CurrentSyntaxEditor.Document.Filename ).ToLower(), out parserPlugin ) )
                {
                    return parserPlugin.CurrentLanguageSettings;
                }

                return null;
            }
        }

        public List<string> ScriptFileExtensions
        {
            get
            {
                List<string> scriptExtensions = new List<string>();
                foreach ( IParserPlugin plugin in sm_parserPluginsByLanguage.Values )
                {
                    if ( plugin.Extensions.Count > 0 )
                    {
                        StringBuilder extensions = new StringBuilder( plugin.Extensions[0] );

                        for ( int i = 1; i < plugin.Extensions.Count; ++i )
                        {
                            extensions.Append( ';' );
                            extensions.Append( plugin.Extensions[i] );
                        }

                        scriptExtensions.Add( extensions.ToString() );
                    }
                }

                return scriptExtensions;
            }
        }

        public string[] LanguageNames
        {
            get
            {
                string[] languages = new string[sm_parserPlugins.Count];
                for ( int i = 0; i < languages.Length; ++i )
                {
                    languages[i] = sm_parserPlugins[i].LanguageName;
                }
                return languages;
            }
        }

        public GetMenuItemByTagDelegate GetMenuItemByTag;
        public GetToolstripItemByTagDelegate GetToolstripItemByTag;
        #endregion

        #region Overrides
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( components != null )
                {
                    components.Dispose();
                }

                m_findReplaceForm.DisableFileWatch = null;
                m_findReplaceForm.EnableFileWatch = null;
                m_findReplaceForm.InitSearchDocs = null;
                m_findReplaceForm.CancelIntellisense = null;
                m_findReplaceForm.LoadFile = null;
                m_findReplaceForm.ReloadFile = null;

                m_findReplaceForm.Dispose();

                m_searchReplaceDialog.CancelIntellisense -= new EventHandler( searchReplaceDialog_CancelIntellisense );
                m_searchReplaceDialog.FindCurrentFilename -= new FindCurrentFilenameEventHandler( searchReplaceDialog_FindCurrentFilename );
                m_searchReplaceDialog.FindOpenFilenames -= new FindOpenFilenamesEventHandler( searchReplaceDialog_FindOpenFilenames );
                m_searchReplaceDialog.FindProjectFilenames -= new FindProjectFilenamesEventHandler( searchReplaceDialog_FindProjectFilenames );
                m_searchReplaceDialog.FindSyntaxEditor -= new SyntaxEditorEventHandler( searchReplaceDialog_FindSyntaxEditor );
                m_searchReplaceDialog.LoadFile -= new LoadFileEventHandler( searchReplaceDialog_LoadFile );
                m_searchReplaceDialog.SaveDocument -= new SaveDocumentEventHandler( searchReplaceDialog_SaveDocument );

                this.m_navManager.OnNavigationChanged -= new NavigationManager.NavigationChangedEventHandler(OnNavigationChanged);

                m_searchReplaceDialog.Dispose();

                CloseAllFiles();
            }
            base.Dispose( disposing );
        }
        #endregion

        #region Static Variables
        private static int sm_NewFileNum = 0;
        private static Dictionary<string, Assembly> sm_pluginAssemblies = new Dictionary<string, Assembly>();
        private static List<IParserPlugin> sm_parserPlugins = new List<IParserPlugin>();
        private static Dictionary<string, IParserPlugin> sm_parserPluginsByLanguage = new Dictionary<string, IParserPlugin>();
        private static Dictionary<string, IParserPlugin> sm_parserPluginsByExtension = new Dictionary<string, IParserPlugin>();
        private static bool sm_InRefreshTimer = false;
        #endregion

        #region Variables
        private Control m_parentControl;
        private UserEditorSettings m_userEditorSettings = null;
        private ProjectExplorer m_projectExplorer = null;
        private EditorToolbar m_ToolBar = null;

        private FindReplaceForm m_findReplaceForm = null;
        private SearchReplaceDialog m_searchReplaceDialog = null;
        private FindReplaceOptions m_FindReplaceOptions = new FindReplaceOptions();

        private IParserPlugin m_projectParserPlugin = null;
        private IParserProjectResolver m_projectParserProjectResolver = null;

        private Dictionary<string, EditorInfo> m_Editors = new Dictionary<string, EditorInfo>();
        private Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> m_TabPageToEditorMap
            = new Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor>();
        private ActiproSoftware.SyntaxEditor.SyntaxEditor m_CurrentSyntaxEditor;
        private Dictionary<string, WatcherInfo> m_WatcherMap = new Dictionary<string, WatcherInfo>();

        private int m_TabIndex = 0;
        private ragScriptEditorShared.InfoTipTextDelegate m_InfoTipTextDel;
        private bool m_performingMultiFileOperation = false;
        private ActiproSoftware.SyntaxEditor.Commands.MacroCommand m_lastMacroCommand = null;

        private Dictionary<string, List<OutlineNodeInfo>> m_automaticOutliningNodeInfos = new Dictionary<string, List<OutlineNodeInfo>>();

        private bool m_AllowBreakpoints = true;
        private bool m_BreakPointsEnabled = true;

        private DebuggerState m_debuggerState = DebuggerState.Stopped;

        private MenuItem m_insertCodeSnippetMenuItem;
        private MenuItem m_gotoDefinitionMenuItem;
        private MenuItem m_outliningMenuItem;

        private List<String> m_modifiedFiles = new List<String>();

        private NavigationManager m_navManager = new NavigationManager();
        #endregion

        #region Parser Plugins
        public static void LoadPlugins( string dllDirectory, Form masterForm )
        {
            if ( (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor)
                || (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Debugger) )
            {
                // start the parsing service
                SemanticParserService.Start();
            }

            // find dlls:
            string[] dllNames = Directory.GetFiles( dllDirectory, "Parser.*.dll" );
            foreach ( string dll in dllNames )
            {
                Assembly assembly = LoadDll( dll.ToLower() );
                if ( assembly != null )
                {
                    try
                    {
                        // Check for special types in the DLL.
                        foreach ( Type t in assembly.GetTypes() )
                        {
                            if ( t.IsAbstract )
                            {
                                continue; // skip abstract classes
                            }

                            // register any IWidgetPlugIn types
                            if ( t.GetInterface( "ragScriptEditorShared.IParserPlugin" ) != null )
                            {
                                // construct
                                IParserPlugin parserPlugin = t.GetConstructor( System.Type.EmptyTypes ).Invoke( null ) as IParserPlugin;

                                if ( (parserPlugin != null) && !sm_parserPlugins.Contains( parserPlugin ) )
                                {
                                    // add to our plugin list
                                    sm_parserPlugins.Add( parserPlugin );

                                    // add to our language dictionary
                                    if ( !sm_parserPluginsByLanguage.ContainsKey( parserPlugin.LanguageName.ToLower() ) )
                                    {
                                        sm_parserPluginsByLanguage.Add( parserPlugin.LanguageName.ToLower(), parserPlugin );
                                    }

                                    // add to our extension dictionary
                                    List<string> extensions = parserPlugin.Extensions;
                                    foreach ( string ext in extensions )
                                    {
                                        if ( !sm_parserPluginsByExtension.ContainsKey( ext.ToLower() ) )
                                        {
                                            sm_parserPluginsByExtension.Add( ext.ToLower(), parserPlugin );
                                        }
                                    }

                                    // add the Compiling and Language creators
                                    UserEditorSettings.AddCompilingSettingsDerivedType( parserPlugin.LanguageName,
                                        parserPlugin.DefaultEditorSettings.GetCompilingSettingsDerivedType() );
                                    UserEditorSettings.AddLanguageSettingsCreatorDelegate( parserPlugin.LanguageName,
                                        parserPlugin.DefaultEditorSettings.GetLanguageSettingsDerivedType() );

                                }
                            }
                        }
                    }
                    catch ( System.Reflection.ReflectionTypeLoadException loadException )
                    {
                        StringBuilder msg = new StringBuilder();
                        msg.Append( "Could not load " );
                        msg.Append( dll );
                        msg.Append( "\n" );

                        foreach ( Exception e in loadException.LoaderExceptions )
                        {
                            msg.Append( e.Message );
                            msg.Append( "\n" );
                        }

                        ApplicationSettings.ShowMessage( masterForm, 
                            String.Format( "{0} {1}", msg.ToString(), loadException.StackTrace ),
                            "Error Loading Plugin", MessageBoxButtons.OK, MessageBoxIcon.Warning, DialogResult.OK );
                    }
                    catch ( Exception e )
                    {
                        ApplicationSettings.ShowMessage( masterForm,
                            String.Format( "Could not load {0}\n{1}", dll, e.ToString() ),
                            "Error Loading Plugin", MessageBoxButtons.OK, MessageBoxIcon.Warning, DialogResult.OK );
                    }
                }
            }

            foreach ( IParserPlugin parserPlugin in sm_parserPlugins )
            {
                try
                {
                    parserPlugin.InitPlugin( dllDirectory );
                }
                catch ( Exception e )
                {
                    ShowPluginExceptionMessage( e, parserPlugin.LanguageName, masterForm );
                }
            }
        }

        public static void ShutdownPlugins( Form masterForm )
        {
            foreach ( IParserPlugin parserPlugin in sm_parserPlugins )
            {
                try
                {
                    parserPlugin.ShutdownPlugin();
                }
                catch ( Exception e )
                {
                    ShowPluginExceptionMessage( e, parserPlugin.LanguageName, masterForm );
                }
            }

            // stop the parsing service
            if ( SemanticParserService.IsRunning )
            {
                SemanticParserService.Stop();
            }
        }

        private static Assembly LoadDll( string path )
        {
            // check to see if it's already loaded:
            Assembly assembly;
            if ( sm_pluginAssemblies.TryGetValue( path, out assembly ) )
            {
                return assembly;
            }

            ApplicationSettings.Log.Message( $"Loading assembly {path}..." );

            assembly = Assembly.LoadFrom( path );
            if ( assembly != null )
            {
                sm_pluginAssemblies.Add( path, assembly );
            }

            return assembly;
        }

        private static void ShowPluginExceptionMessage( Exception e, string plugin, Form masterForm )
        {
            StringBuilder msg = new StringBuilder( e.ToString() );

            Exception inner = e.InnerException;
            while ( inner != null )
            {
                msg.Append( "\n\n" );
                msg.Append( inner.ToString() );
            }

            ApplicationSettings.ShowMessage( masterForm, msg.ToString(), String.Format( "Plugin exception for {0}", plugin ), 
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation, DialogResult.OK );
        }
        #endregion

        #region Event Handlers
        private void dockControl_Closing( object sender, TD.SandDock.DockControlClosingEventArgs e )
        {
            ActiproSoftware.SyntaxEditor.SyntaxEditor editor;
            if ( m_TabPageToEditorMap.TryGetValue( e.DockControl, out editor ) )
            {
                if ( !CloseFile( editor ) )
                {
                    e.Cancel = true;
                }
            }
        }

        private void editor_ExecuteAppAction( object sender, EventArgs e )
        {
            MenuItem menuItem = sender as MenuItem;
            if ( menuItem != null )
            {
                ScriptEditor.AppAction action = (ScriptEditor.AppAction)Enum.Parse( typeof( ScriptEditor.AppAction ), menuItem.Tag.ToString() );
                ExecuteAppAction( action );
            }
        }

        private void editor_viewMouseDown( object sender, EditorViewMouseEventArgs e )
        {
            if ( (e.Button == MouseButtons.Left) && (e.HitTestResult.Target == SyntaxEditorHitTestTarget.IndicatorMargin) 
                && this.BreakPointsEnabled && this.AllowBreakpoints )
            {
                SyntaxEditor editor = sender as SyntaxEditor;
                RequestBreakpoint( editor.Document.Filename, e.HitTestResult.DisplayLineIndex );
            }
        }

        private void editor_PasteDragDrop( object sender, ActiproSoftware.SyntaxEditor.PasteDragDropEventArgs e )
        {
            // Allow file name drops from Windows Explorer
            if ( e.DataObject.GetDataPresent( DataFormats.FileDrop ) )
            {
                object files = e.DataObject.GetData( DataFormats.FileDrop );
                if ( (files is string[]) && (((string[])files).Length > 0) )
                {
                    string[] fileNames = files as string[];
                    foreach ( string file in fileNames )
                    {
                        switch ( e.Source )
                        {
                            case PasteDragDropSource.DragEnter:
                                if ( File.Exists( file ) )
                                {
                                    e.Text = "";
                                }
                                break;
                            case PasteDragDropSource.DragDrop:
                                // If performing a drop of a .snippet file, see if it contains any code snippets and if so, 
                                //    activate the first one that is found
                                if ( (Path.GetExtension( file ).ToLower() == ".snippet") && (this.CurrentSyntaxEditor != null) )
                                {
                                    CodeSnippet[] codeSnippets = CodeSnippet.LoadFromXml( file );
                                    if ( codeSnippets.Length > 0 )
                                    {
                                        e.Text = string.Empty;
                                        this.CurrentSyntaxEditor.IntelliPrompt.CodeSnippets.Activate( codeSnippets[0] );
                                        return;
                                    }
                                }
                                // load the dragged file:
                                else
                                {
                                    LoadFile( file );
                                    e.Text = null;
                                }
                                break;
                        }
                    }
                }
            }
        }

        private void editor_DocumentIndicatorAdded( object sender, IndicatorEventArgs e )
        {
            if ( e.Indicator is BookmarkLineIndicator )
            {
                if ( sender is SyntaxEditor )
                {
                    SyntaxEditor editor = sender as SyntaxEditor;
                    if ( editor == this.CurrentSyntaxEditor )
                    {
                        EnableMenuItem( AppAction.EditClearBookmarks, true );
                        EnableMenuItem( AppAction.EditEnableBookmark, true );
                        EnableMenuItem( AppAction.EditNextBookmark, true );
                        EnableMenuItem( AppAction.EditPreviousBookmark, true );
                    }
                }
            }
        }

        private void editor_DocumentIndicatorRemoved( object sender, IndicatorEventArgs e )
        {
            if ( e.Indicator is BookmarkLineIndicator )
            {
                if ( sender is SyntaxEditor )
                {
                    SyntaxEditor editor = sender as SyntaxEditor;
                    if ( editor == this.CurrentSyntaxEditor )
                    {
                        bool haveBookmarks = false;
                        if ( editor.Document.LineIndicators.Contains( BookmarkLineIndicator.DefaultName ) )
                        {
                            int count = 0;
                            foreach ( Indicator i in editor.Document.LineIndicators )
                            {
                                if ( i is BookmarkLineIndicator )
                                {
                                    ++count;
                                }
                            }

                            haveBookmarks = count > 1;  // because we haven't yet removed the one that triggered this event
                        }

                        if ( !haveBookmarks )
                        {
                            EnableMenuItem( AppAction.EditClearBookmarks, false );
                            EnableMenuItem( AppAction.EditEnableBookmark, false );
                            EnableMenuItem( AppAction.EditNextBookmark, false );
                            EnableMenuItem( AppAction.EditPreviousBookmark, false );
                        }
                    }
                }
            }
        }

        private void editor_DocumentTextChanged( object sender, DocumentModificationEventArgs e )
        {
            RefreshFileMenuItems();
        }

        private void editor_DocumentModifiedChanged(object sender, EventArgs e)
        {
            RefreshFileMenuItems();
        }

        private void editor_MacroRecordingStateChanged( object sender, EventArgs e )
        {
            ActiproSoftware.SyntaxEditor.SyntaxEditor editor = sender as ActiproSoftware.SyntaxEditor.SyntaxEditor;
            if ( editor != null )
            {
                if ( editor == this.CurrentSyntaxEditor )
                {
                    bool debugging = ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Debugger;
                    bool recording = editor.MacroRecording.State == MacroRecordingState.Recording;
                    bool paused = editor.MacroRecording.State == MacroRecordingState.Paused;
                    bool stopped = editor.MacroRecording.State == MacroRecordingState.Stopped;
                    bool haveMacro = (this.CurrentSyntaxEditor.MacroRecording.LastMacroCommand != null) || (m_lastMacroCommand != null);

                    EnableMenuItem( AppAction.ToolsMacrosRecord, !debugging && !recording && !paused );
                    EnableMenuItem( AppAction.ToolsMacrosPause, !debugging && !stopped );
                    EnableMenuItem( AppAction.ToolsMacrosStop, !debugging && !stopped );
                    EnableMenuItem( AppAction.ToolsMacrosCancel, !debugging && !stopped );
                    EnableMenuItem( AppAction.ToolsMacrosRun, !debugging && !recording && !paused && haveMacro );
                }
            }
            else
            {
                EnableMenuItem( AppAction.ToolsMacrosRecord, false );
                EnableMenuItem( AppAction.ToolsMacrosPause, false );
                EnableMenuItem( AppAction.ToolsMacrosStop, false );
                EnableMenuItem( AppAction.ToolsMacrosCancel, false );
                EnableMenuItem( AppAction.ToolsMacrosRun, false );
            }
        }

        private void editorDocument_OutliningModeChanged( object sender, EventArgs e )
        {
            ActiproSoftware.SyntaxEditor.Document doc = sender as ActiproSoftware.SyntaxEditor.Document;
            if ( doc != null )
            {
                if ( (this.CurrentSyntaxEditor != null) && (doc == this.CurrentSyntaxEditor.Document) )
                {
                    bool manualOutliningOn = doc.Outlining.Mode == ActiproSoftware.SyntaxEditor.OutliningMode.Manual;
                    bool automaticOutliningOn = doc.Outlining.Mode == ActiproSoftware.SyntaxEditor.OutliningMode.Automatic;

                    EnableMenuItem( AppAction.OutliningHideSelection, manualOutliningOn );
                    EnableMenuItem( AppAction.OutliningToggleOutliningExpansion, manualOutliningOn || automaticOutliningOn );
                    EnableMenuItem( AppAction.OutliningToggleAllOutlining, manualOutliningOn || automaticOutliningOn );
                    EnableMenuItem( AppAction.OutliningStopOutlining, manualOutliningOn || automaticOutliningOn );
                    EnableMenuItem( AppAction.OutliningStopHidingCurrent, manualOutliningOn );
                    EnableMenuItem( AppAction.OutliningStartAutomaticOutlining, !automaticOutliningOn );
                    EnableMenuItem( AppAction.OutliningStartManualOutlining, !manualOutliningOn );
                }
            }
            else
            {
                EnableMenuItem( AppAction.OutliningHideSelection, false );
                EnableMenuItem( AppAction.OutliningToggleOutliningExpansion, false );
                EnableMenuItem( AppAction.OutliningToggleAllOutlining, false );
                EnableMenuItem( AppAction.OutliningStopOutlining, false );
                EnableMenuItem( AppAction.OutliningStopHidingCurrent, false );
                EnableMenuItem( AppAction.OutliningStartAutomaticOutlining, false );
                EnableMenuItem( AppAction.OutliningStartManualOutlining, false );
            }
        }

        private void editorDocument_UndoRedoStateChanged( object sender, UndoRedoStateChangedEventArgs e )
        {
            Document doc = sender as Document;
            if ( doc != null )
            {
                bool overrideMod = false;
                if (!doc.Modified && (e.ChangeType == UndoRedoStateChangeType.Undo || e.ChangeType == UndoRedoStateChangeType.Redo))
                {
                    // Vivi: This is a HACK because the Actipro undo/redo support is wonky and sometimes doesn't work.
                    // If we're not modified, there might still be situations where we should be if Actipro's undo stack glitches out.
                    // for url:bugstar:3433658, with fixes for url:bugstar:3541114

                    // Essentially, we're checksumming the file/buffer manually to see if they really do match, or if they differ.
                    byte[] memMD5;
                    byte[] fileMD5;
                    fileMD5 = FileMD5.ComputeMD5(doc.Filename);
                    using (Stream stream = new MemoryStream(Encoding.ASCII.GetBytes(doc.Text)))
                    {
                        memMD5 = FileMD5.ComputeMD5(stream);
                    }

                    if (!memMD5.SequenceEqual(fileMD5))
                    {
                        overrideMod = true;
                    }
                }

                if (overrideMod)
                {
                    doc.Modified = true;
                }

                if ( (this.CurrentSyntaxEditor != null) && (doc == this.CurrentSyntaxEditor.Document) )
                {
                    bool debugging = ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Debugger;

                    EnableMenuItem( AppAction.EditUndo, doc.UndoRedo.CanUndo && !debugging );
                    EnableMenuItem( AppAction.EditRedo, doc.UndoRedo.CanRedo && !debugging );

                    if (overrideMod)
                    {
                        // Tagging along to the above HACK, also have to refresh save status, because the TextChange event happens before this, and the modified wasn't set until now.
                        RefreshFileMenuItems();
                    }
                }
            }
            else
            {
                EnableMenuItem( AppAction.EditUndo, false );
                EnableMenuItem( AppAction.EditRedo, false );
            }
        }

        private void editorDocument_AutomaticOutliningComplete( object sender, DocumentModificationEventArgs e )
        {
            if ( e.Document.Filename == null )
            {
                return;
            }

            // Restore the automatic outlining node states on the first chance we can after loading the file
            if ( e.IsProgrammaticTextReplacement && (e.DirtyTextRange.StartOffset > -1) && (e.DirtyTextRange.EndOffset > -1) )
            {
                string filename = e.Document.Filename.ToLower();

                List<OutlineNodeInfo> infos = null;
                if ( m_automaticOutliningNodeInfos.TryGetValue( filename, out infos ) )
                {
                    RestoreAutomaticOutliningNodeState( infos, e.Document.Outlining.RootNode );

                    m_automaticOutliningNodeInfos.Remove( filename );
                }

                // remove the event handler
                e.Document.AutomaticOutliningComplete -= new DocumentModificationEventHandler( editorDocument_AutomaticOutliningComplete );
            }
        }

        private void saveFileDialog_FileOk( object sender, CancelEventArgs e )
        {
            if ( File.Exists( this.saveFileDialog.FileName ) )
            {
                FileAttributes attributes = File.GetAttributes( this.saveFileDialog.FileName );
                if ( (attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly )
                {
                    if ( m_parentControl != null )
                    {
                        rageMessageBox.ShowError( m_parentControl, String.Format( 
                            "Cannot save the file '{0}'. The file exists and is marked Read-Only. Save the file with another file name or to another location.",
                            Path.GetFileName( this.saveFileDialog.FileName ) ), ApplicationSettings.ExecutableName );
                    }
                    else
                    {
                        rageMessageBox.ShowError( String.Format( 
                            "Cannot save the file '{0}'. The file exists and is marked Read-Only. Save the file with another file name or to another location.",
                            Path.GetFileName( this.saveFileDialog.FileName ) ), ApplicationSettings.ExecutableName );
                    }

                    e.Cancel = true;
                }
            }
        }
        #endregion

        #region Parser DLL functions
        public bool LoadDllOptionsFile( Form masterForm, string language )
        {
            IParserPlugin parserPlugin;
            if ( !sm_parserPluginsByLanguage.TryGetValue( language.ToLower(), out parserPlugin ) )
            {
                return false;
            }

            bool success = false;
            try
            {
                success = parserPlugin.LoadSettings();
            }
            catch ( Exception e )
            {
                ShowPluginExceptionMessage( e, parserPlugin.LanguageName, masterForm );
            }

            return success;
        }

        public bool SaveDllOptionsFile( Form masterForm, string language )
        {
            IParserPlugin parserPlugin;
            if ( !sm_parserPluginsByLanguage.TryGetValue( language.ToLower(), out parserPlugin ) )
            {
                return false;
            }

            bool success = false;
            try
            {
                success = parserPlugin.SaveSettings();
            }
            catch ( Exception e )
            {
                ShowPluginExceptionMessage( e, parserPlugin.LanguageName, masterForm );
            }

            return success;
        }

        public void ProjectFilesAdded( List<string> filenames )
        {
            Debug.Assert( m_projectParserProjectResolver != null, "m_projectParserProjectResolver != null" );

            foreach ( string filename in filenames )
            {
                string lowercaseName = filename.ToLower();

                EditorInfo eInfo;
                if ( !m_Editors.TryGetValue( lowercaseName, out eInfo )
                    || (eInfo.Editor.Document.LanguageData == m_projectParserProjectResolver) )
                {
                    continue;
                }

                if ( m_projectParserProjectResolver.ProjectFiles.Contains( lowercaseName )
                    && m_projectParserPlugin.Extensions.Contains( Path.GetExtension( lowercaseName ) ) )
                {
                    // destroy old project resolver
                    IParserPlugin plugin;
                    if ( (eInfo.Editor.Document.LanguageData != null)
                        && sm_parserPluginsByExtension.TryGetValue( Path.GetExtension( lowercaseName ), out plugin ) )
                    {
                        // clear the old project resolver from the SyntaxEditor
                        IParserProjectResolver projectResolver = eInfo.Editor.Document.LanguageData as IParserProjectResolver;
                        projectResolver.ClearLanguage( eInfo.Editor );
                        this.IntellisenseSettingsChanged -= projectResolver.IntellisenseSettingsChangedEventHandler;

                        plugin.DestroyProjectResolver( projectResolver );
                    }

                    // use the current project resolver
                    m_projectParserProjectResolver.SetLanguage( eInfo.Editor, m_InfoTipTextDel );
                }
            }

            RefreshFileMenuItems();
        }

        public void ProjectFilesRemoved( List<string> filenames )
        {
            Debug.Assert( m_projectParserProjectResolver != null, "m_projectParserProjectResolver != null" );

            foreach ( string filename in filenames )
            {
                string lowercaseName = filename.ToLower();

                EditorInfo eInfo;
                if ( !m_Editors.TryGetValue( lowercaseName, out eInfo )
                    || (eInfo.Editor.Document.LanguageData != m_projectParserProjectResolver) )
                {
                    continue;
                }

                if ( !m_projectParserProjectResolver.ProjectFiles.Contains( lowercaseName ) )
                {
                    // clear the old project resolver from the SyntaxEditor
                    m_projectParserProjectResolver.ClearLanguage( eInfo.Editor );

                    // assign a new project resolver
                    IParserProjectResolver projectResolver = m_projectParserPlugin.CreateProjectResolver( filename );
                    projectResolver.SetLanguage( eInfo.Editor, m_InfoTipTextDel );
                    this.IntellisenseSettingsChanged += projectResolver.IntellisenseSettingsChangedEventHandler;
                }
            }

            RefreshFileMenuItems();
        }

        /// <summary>
        /// After a project is loaded, determines its m_projectParserPlugin and creates the m_projectParserProjectResolver
        /// </summary>
        public void ProjectLoaded( List<string> filenames )
        {
            Debug.Assert( (m_projectExplorer != null) && m_projectExplorer.IsLoaded, "(m_projectExplorer != null) && m_projectExplorer.IsLoaded" );
            Debug.Assert( m_projectParserPlugin == null, "m_projectParserPlugin == null" );
            Debug.Assert( m_projectParserProjectResolver == null, "m_projectParserProjectResolver == null" );

            if ( m_projectExplorer == null )
            {
                return;
            }

            IParserPlugin parserPlugin = null;
            if ( !sm_parserPluginsByLanguage.TryGetValue( m_projectExplorer.ProjectEditorSetting.CurrentCompilingSettings.Language.ToLower(), out parserPlugin ) )
            {
                ApplicationSettings.Log.Error( "Could not determine IParserPlugin for current project" );
                return;
            }

            m_projectParserPlugin = parserPlugin;

            parserPlugin.IncludePaths = m_projectExplorer.ProjectEditorSetting.CurrentCompilingSettings.IncludePaths.Select(ApplicationSettings.GetAbsolutePath).ToList();
            parserPlugin.IncludeFile = m_projectExplorer.ProjectEditorSetting.CurrentCompilingSettings.XmlIncludeFile;
            parserPlugin.Custom = m_projectExplorer.ProjectEditorSetting.CurrentCompilingSettings.Custom;

            m_projectParserProjectResolver = parserPlugin.CreateProjectResolver(filenames);
            this.IntellisenseSettingsChanged += m_projectParserProjectResolver.IntellisenseSettingsChangedEventHandler;

            ProjectFilesAdded( filenames );
        }

        /// <summary>
        /// After a project is reloaded, resets the Project Resolver settings, and ensures that every open file
        /// has the correct IParserProjectResolver.
        /// </summary>
        public void ProjectReloaded( List<string> preloadFilenames, List<string> postloadFilenames )
        {
            List<string> removedFiles = new List<string>();
            foreach ( string filename in preloadFilenames )
            {
                if ( !postloadFilenames.Contains( filename ) )
                {
                    removedFiles.Add( filename.ToLower() );
                }
            }

            ProjectFilesRemoved( removedFiles );

            List<string> addedFiles = new List<string>();
            foreach ( string filename in postloadFilenames )
            {
                if ( !preloadFilenames.Contains( filename ) )
                {
                    addedFiles.Add( filename.ToLower() );
                }
            }

            ProjectFilesAdded( addedFiles );
        }

        /// <summary>
        /// After a project and all of its files are closed, ensures that every open file has the correct IParserProjectResolver.
        /// Clears the project's IParserPlugin and disposes of its IParserProjectResolver.
        /// </summary>
        public void ProjectClosed( List<string> filenames )
        {
            Debug.Assert( (m_projectExplorer != null) && !m_projectExplorer.IsLoaded, "(m_projectExplorer != null) && !m_projectExplorer.IsLoaded" );

            if ( m_projectExplorer == null )
            {
                return;
            }

            if ( (m_projectParserPlugin == null) || (m_projectParserProjectResolver == null) )
            {
                return;
            }

            ProjectFilesRemoved( filenames );

            this.IntellisenseSettingsChanged -= m_projectParserProjectResolver.IntellisenseSettingsChangedEventHandler;

            m_projectParserPlugin.DestroyProjectResolver( m_projectParserProjectResolver );
            m_projectParserPlugin = null;
            m_projectParserProjectResolver = null;

            RefreshFileMenuItems();
            RefreshCompilingMenuItems();
        }

        /// <summary>
        /// Update the IParserPlugin associated with the given language with the settings passed in.
        /// </summary>
        /// <param name="compilingSettings"></param>
        public void SetParserPluginCompilingSettings( CompilingSettings compilingSettings )
        {
            IParserPlugin parserPlugin;
            if ( sm_parserPluginsByLanguage.TryGetValue( compilingSettings.Language.ToLower(), out parserPlugin ) )
            {
                parserPlugin.IncludePaths = compilingSettings.IncludePaths.Select(ApplicationSettings.GetAbsolutePath).ToList();
                parserPlugin.IncludeFile = compilingSettings.XmlIncludeFile;
                parserPlugin.Custom = compilingSettings.Custom;

                foreach ( IParserProjectResolver projectResolver in parserPlugin.ProjectResolvers )
                {
                    foreach ( string filename in projectResolver.ProjectFiles )
                    {
                        EditorInfo eInfo;
                        if ( m_Editors.TryGetValue( filename, out eInfo ) )
                        {
                            RefreshIntellisense( eInfo.Editor );
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Updates the IParserPlugin associated with the given language with the settings passed in.
        /// </summary>
        /// <param name="languageSettings"></param>
        public void SetParserPluginLanguageSettings( LanguageSettings languageSettings )
        {
            IParserPlugin parserPlugin;
            if ( sm_parserPluginsByLanguage.TryGetValue( languageSettings.Language.ToLower(), out parserPlugin ) )
            {
                parserPlugin.CurrentLanguageSettings.Copy( languageSettings );
                parserPlugin.OnLanguageSettingsUpdated();
            }

            if ( this.CurrentSyntaxEditor == null )
            {
                EnableMenuItem( AppAction.EditInsertCodeSnippet, false );
            }
            else
            {
                EnableMenuItem( AppAction.EditInsertCodeSnippet, this.IsCodeSnippetPathValid()
                    && (ApplicationSettings.Mode != ApplicationSettings.ApplicationMode.Debugger) );
            }
        }

        /// <summary>
        /// Sets the language's cache directory.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="directory"></param>
        /// <returns><c>true</c> if the langauge is valid and the cache directory was set, otherwise <c>false</c>.</returns>
        public bool SetLanguageCacheDirectory( string language, string directory )
        {
            IParserPlugin parserPlugin;
            if ( sm_parserPluginsByLanguage.TryGetValue( language.ToLower(), out parserPlugin ) )
            {
                parserPlugin.CurrentLanguageSettings.CacheDirectory = directory;
                parserPlugin.OnLanguageSettingsUpdated();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Updates every IParserProjectResolver with the new includeFile and includePath, except if null.  If projectFiles
        /// is not null, updates the project's IParserProjectResolver with the new projectFiles.
        /// </summary>
        /// <param name="includeFile"></param>
        /// <param name="includePath"></param>
        /// <param name="projectFiles"></param>
        public void SetProjectParserProjectFilenames( List<string> projectFiles )
        {
            if ( m_projectParserProjectResolver != null )
            {
                m_projectParserProjectResolver.ProjectFiles = projectFiles;
            }

            //RefreshIntellisenseQuick();
        }

        /// <summary>
        /// Get the list of the file's includes/usings.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public IEnumerable<String> GetIncludesForFile( string filename )
        {
            if (IsANewFileName(filename))
            {
                return new String[0];
            }

            return m_projectParserProjectResolver.GetIncludesForFile(filename);
        }

        /// <summary>
        /// Called when a compile is executed to see if we can compile the file
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool IsFileCompilable( string filename )
        {
            if (String.IsNullOrEmpty(filename))
            {
                return false;
            }

            if ( IsANewFileName( filename ) )
            {
                return false;
            }

            string ext = Path.GetExtension( filename ).ToLower();
            IParserPlugin parserPlugin;
            if ( !sm_parserPluginsByExtension.TryGetValue( ext, out parserPlugin ) )
            {
                return false;
            }

            return parserPlugin.CompilableExtensions.Contains( ext );
        }

        public bool IsProjectCompilable()
        {
            if ( (m_projectExplorer == null) || !m_projectExplorer.IsLoaded )
            {
                return false;
            }

            IParserPlugin parserPlugin;
            if ( sm_parserPluginsByLanguage.TryGetValue( m_projectExplorer.ProjectEditorSetting.CurrentCompilingSettings.Language.ToLower(), out parserPlugin ) )
            {
                return parserPlugin.CompilableExtensions.Count > 0;
            }

            return false;
        }

        public bool AnyOpenFilesCompilable()
        {
            foreach ( KeyValuePair<string, EditorInfo> pair in m_Editors )
            {
                if ( IsFileCompilable( pair.Key ) )
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsScriptFile( string filename )
        {
            if ( IsANewFileName( filename ) )
            {
                return false;
            }

            return sm_parserPluginsByExtension.ContainsKey( Path.GetExtension( filename ).ToLower() );
        }

        /// <summary>
        /// Called when a compile is executed to check if the compiler output files are out of date
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="outputDirectory"></param>
        /// <param name="globals"></param>
        /// <param name="projectFile"></param>
        /// <returns></returns>
        public void PreCompile()
        {
            RefreshFileMenuItems();
            RefreshCompilingMenuItems();
            RemoveAllCompileErrorIndicators();

            foreach ( IParserPlugin parserPlugin in sm_parserPlugins )
            {
                List<IParserProjectResolver> projectResolvers = parserPlugin.ProjectResolvers;
                foreach ( IParserProjectResolver projectResolver in projectResolvers )
                {
                    projectResolver.PreCompile();
                }
            }
        }

        public bool FileNeedsToBeCompiled( string filename, string projectFile, CompilingSettings compilingSettings )
        {
            filename = filename.ToLower();

            if ( IsANewFileName( filename ) )
            {
                return false;
            }

            IParserPlugin parserPlugin;
            if ( !sm_parserPluginsByExtension.TryGetValue( Path.GetExtension( filename ).ToLower(), out parserPlugin ) )
            {
                return true;
            }

            List<IParserProjectResolver> projectResolvers = parserPlugin.ProjectResolvers;
            foreach ( IParserProjectResolver projectResolver in projectResolvers )
            {
                if ( projectResolver.ProjectFiles.Contains( filename ) )
                {
                    return projectResolver.NeedsToBeCompiled( filename, projectFile, compilingSettings );
                }
            }

            return true;
        }

        public bool FileNeedsToBeResourced(String filename, Platform platform, String projectFile, CompilingSettings compilingSettings)
        {
            filename = filename.ToLower();

            if (IsANewFileName(filename))
            {
                return false;
            }

            IParserPlugin parserPlugin;
            if (!sm_parserPluginsByExtension.TryGetValue(Path.GetExtension(filename).ToLower(), out parserPlugin))
            {
                return true;
            }

            List<IParserProjectResolver> projectResolvers = parserPlugin.ProjectResolvers;
            foreach (IParserProjectResolver projectResolver in projectResolvers)
            {
                if (projectResolver.ProjectFiles.Contains(filename))
                {
                    return projectResolver.NeedsToBeResourced(filename, platform, projectFile, compilingSettings);
                }
            }

            return true;
        }

        public void SetFileCompiled( string filename )
        {
            filename = filename.ToLower();

            if ( IsANewFileName( filename ) )
            {
                return;
            }

            IParserPlugin parserPlugin;
            if ( !sm_parserPluginsByExtension.TryGetValue( Path.GetExtension( filename ), out parserPlugin ) )
            {
                return;
            }

            List<IParserProjectResolver> projectResolvers = parserPlugin.ProjectResolvers;
            foreach ( IParserProjectResolver projectResolver in projectResolvers )
            {
                if ( projectResolver.ProjectFiles.Contains( filename ) )
                {
                    projectResolver.FileCompiled( filename );
                    break;
                }
            }
        }

        public void SetFileResourced(String filename, Platform platform)
        {
            filename = filename.ToLower();

            if (IsANewFileName(filename))
            {
                return;
            }

            IParserPlugin parserPlugin;
            if (!sm_parserPluginsByExtension.TryGetValue(Path.GetExtension(filename), out parserPlugin))
            {
                return;
            }

            List<IParserProjectResolver> projectResolvers = parserPlugin.ProjectResolvers;
            foreach (IParserProjectResolver projectResolver in projectResolvers)
            {
                if (projectResolver.ProjectFiles.Contains(filename))
                {
                    projectResolver.FileResourced(filename, platform);
                    break;
                }
            }
        }

        public void PostCompile()
        {
            foreach ( IParserPlugin parserPlugin in sm_parserPlugins )
            {
                List<IParserProjectResolver> projectResolvers = parserPlugin.ProjectResolvers;
                foreach ( IParserProjectResolver projectResolver in projectResolvers )
                {
                    projectResolver.PostCompile();
                }
            }

            RefreshFileMenuItems();
            RefreshCompilingMenuItems();
        }
        #endregion

        #region File Handling
        public SyntaxEditor GetSyntaxEditor( string filename )
        {
            string lowercaseName = filename.ToLower();
            EditorInfo eInfo = null;
            if ( m_Editors.TryGetValue( lowercaseName, out eInfo ) )
            {
                return eInfo.Editor;
            }

            return null;
        }

        public bool CreateNewFile()
        {
            return CreateNewFile( new TextEditorSettings( GetNewFileName(), System.Guid.NewGuid() ) );
        }

        public bool CreateNewFile( TextEditorSettings t )
        {
            using (NavigationScope navScope = this.CurrentSyntaxEditor != null ?
                new NavigationScope(this.m_navManager, new NavigationPosition(this.CurrentSyntaxEditor)) : null)
            {
                // create editor
                ActiproSoftware.SyntaxEditor.SyntaxEditor editor;
                SearchCommandLinks cmdLinks;
                CreateEditor(t, out editor, out cmdLinks);

                editor.Document.Filename = t.Filename;
                editor.Document.Modified = true;

                // prepare new file index for next one
                int fileIndex = GetNewFileNameIndex(t.Filename);
                if (sm_NewFileNum < fileIndex)
                {
                    sm_NewFileNum = fileIndex;
                }

                // get a dock control
                TD.SandDock.DockControl dockControl = OnFileLoading(editor, t.Guid, true, false);
                if (dockControl == null)
                {
                    // cancelled
                    editor.Dispose();
                    navScope.Cancel();
                    return false;
                }

                dockControl.Closing += new TD.SandDock.DockControlClosingEventHandler(dockControl_Closing);

                // turn off parsing until we're ready
                editor.Document.LexicalParsingEnabled = false;
                editor.Document.SemanticParsingEnabled = false;

                editor.Document.ResetLanguage();

                // set the color styles
                if (HighlightLanguage != null)
                {
                    HighlightLanguage(this, editor);
                }

                // now we're ready to turn on parsing
                editor.Document.LexicalParsingEnabled = true;
                editor.Document.SemanticParsingEnabled = m_userEditorSettings.IntellisenseSettings.PerformSemanticParsing;

                EditorInfo eInfo = new EditorInfo(editor, dockControl, cmdLinks);
                m_Editors.Add(editor.Document.Filename.ToLower(), eInfo);
                m_TabPageToEditorMap.Add(dockControl, editor);

                // signal successful creation
                OnFileLoaded(editor, t.Guid, true, false, dockControl, false, string.Empty);

                dockControl.Open(TD.SandDock.WindowOpenMethod.OnScreenActivate);

                // only refresh if it wasn't done when the tab page was selected
                //if ( !m_userEditorSettings.IntellisenseSettings.RefreshIntellisenseOnSelect )
                //{
                //    RefreshIntellisenseQuick();
                //}

                return true;
            }
        }

        public bool LoadFile( string filename )
        {
            return LoadFile( new TextEditorSettings( filename, System.Guid.NewGuid() ) );
        }

        public bool LoadFile(TextEditorSettings t)
        {
            bool isNewFile = IsANewFileName(t.Filename);
            if (!isNewFile && !File.Exists(t.Filename))
            {
                // signal load failure
                OnFileLoaded(null, t.Guid, false, m_performingMultiFileOperation, null, true, "'" + t.Filename + "' doesn't exist.");
                return false;
            }

            using (NavigationScope navScope = this.CurrentSyntaxEditor != null ?
                new NavigationScope(this.m_navManager, new NavigationPosition(this.CurrentSyntaxEditor)) : null)
            {
                // see if we've already loaded the file:
                string lowercaseName = t.Filename.ToLower();
                EditorInfo eInfo = null;
                if (m_Editors.TryGetValue(lowercaseName, out eInfo))
                {
                    eInfo.TabPage.Open();

                    // signal successful (re)load
                    OnFileLoaded(eInfo.Editor, t.Guid, false, m_performingMultiFileOperation, eInfo.TabPage, false, string.Empty);
                    return true;
                }
                else if (isNewFile)
                {
                    OnFileLoaded(null, t.Guid, false, m_performingMultiFileOperation, null, true, "'" + t.Filename + "' is a new filename and cannot be loaded.");
                    navScope?.Cancel();
                    return false;
                }

                ActiproSoftware.SyntaxEditor.SyntaxEditor editor;
                SearchCommandLinks cmdLinks;
                CreateEditor(t, out editor, out cmdLinks);

                // set up our custom context menu
                ContextMenu menu = editor.ContextMenu;
                if (menu == null)
                {
                    menu = editor.GetDefaultContextMenu();

                    foreach (MenuItem item in menu.MenuItems)
                    {
                        switch (item.Text)
                        {
                            case "Undo":
                                item.Tag = "EditUndo";
                                break;
                            case "Redo":
                                item.Tag = "EditRedo";
                                break;
                            case "Cut":
                                item.Tag = "EditCut";
                                break;
                            case "Copy":
                                item.Tag = "EditCopy";
                                break;
                            case "Paste":
                                item.Tag = "EditPaste";
                                break;
                            case "Delete":
                                item.Tag = "EditDelete";
                                break;
                            case "Select All":
                                item.Tag = "EditSelectAll";
                                break;
                        }
                    }

                    editor.ContextMenu = menu;
                }

                MenuItem menuItem = new MenuItem("-");
                menu.MenuItems.Add(menuItem);

                menuItem = m_insertCodeSnippetMenuItem.CloneMenu();
                menuItem.Tag = m_insertCodeSnippetMenuItem.Tag;
                menu.MenuItems.Add(menuItem);

                menuItem = m_gotoDefinitionMenuItem.CloneMenu();
                menuItem.Tag = m_gotoDefinitionMenuItem.Tag;
                menu.MenuItems.Add(menuItem);

                menuItem = new MenuItem("-");
                menu.MenuItems.Add(menuItem);

                menuItem = m_outliningMenuItem.CloneMenu();
                for (int i = 0; i < m_outliningMenuItem.MenuItems.Count; ++i)
                {
                    menuItem.MenuItems[i].Tag = m_outliningMenuItem.MenuItems[i].Tag;
                }

                menu.MenuItems.Add(menuItem);

                if (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Debugger)
                {
                    editor.ViewMouseDown += new EditorViewMouseEventHandler(editor_viewMouseDown);
                }
                else if (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor)
                {
                    // If the last OutliningMode was automatic.
                    if (t.OutliningMode == OutliningMode.Automatic)
                    {
                        // If it is still the same file (or very likely to be), we can restore the OutliningNodes.
                        if (File.GetLastWriteTime(t.Filename).ToFileTime() == t.FileDateTime)
                        {
                            // save the list of nodes, because we can't collapse any until the first AutomaticOutliningComplete event.
                            Debug.Assert(!m_automaticOutliningNodeInfos.ContainsKey(lowercaseName), "!m_automaticOutliningNodeInfos.ContainsKey( " + lowercaseName + " )");
                            m_automaticOutliningNodeInfos.Add(lowercaseName, t.OutlineNodeInfos);

                            editor.Document.AutomaticOutliningComplete += new DocumentModificationEventHandler(editorDocument_AutomaticOutliningComplete);

                            // In any case, we can restore the OutliningMode.
                            editor.Document.Outlining.Mode = OutliningMode.Automatic;
                        }
                    }
                }

                // set the current directory to the file:
                string filePath = Path.GetDirectoryName(t.Filename);
                if (!Directory.Exists(filePath))
                {
                    t.Filename = Path.Combine(Directory.GetCurrentDirectory(), t.Filename);
                }

                editor.Document.Filename = t.Filename;

                // get a dock control
                TD.SandDock.DockControl dockControl = OnFileLoading(editor, t.Guid, false, m_performingMultiFileOperation);
                if (dockControl == null)
                {
                    m_automaticOutliningNodeInfos.Remove(lowercaseName);

                    // cancelled
                    editor.Dispose();
                    navScope?.Cancel();
                    return false;
                }

                dockControl.Closing += new TD.SandDock.DockControlClosingEventHandler(dockControl_Closing);

                // load the file:
                try
                {
                    editor.Document.LoadFile(t.Filename);
                    editor.Document.Modified = false; // mark false because we marked it true on load
                }
                catch (Exception e)
                {
                    // signal load failure
                    OnFileLoaded(editor, t.Guid, false, m_performingMultiFileOperation, dockControl, true, "'" + t.Filename + "' failed to load:" + e.Message + "\n" + e.StackTrace);

                    m_automaticOutliningNodeInfos.Remove(lowercaseName);

                    editor.Dispose();
                    navScope?.Cancel();
                    return false;
                }

                // turn off parsing until we're ready
                editor.Document.LexicalParsingEnabled = false;
                editor.Document.SemanticParsingEnabled = false;

                // Set the language
                IParserProjectResolver projectResolver = null;
                if ((m_projectExplorer != null) && m_projectExplorer.IsLoaded && (m_projectParserProjectResolver != null)
                    && m_projectParserProjectResolver.ProjectFiles.Contains(lowercaseName)
                    && m_projectParserPlugin.Extensions.Contains(Path.GetExtension(lowercaseName)))
                {
                    projectResolver = m_projectParserProjectResolver;
                }
                else
                {
                    if (IsANewFileName(lowercaseName))
                    {
                        editor.Document.ResetLanguage();
                    }
                    else
                    {
                        IParserPlugin parserPlugin;
                        if (sm_parserPluginsByExtension.TryGetValue(Path.GetExtension(lowercaseName), out parserPlugin))
                        {
                            parserPlugin.IncludePaths = m_userEditorSettings.CurrentCompilingSettings.IncludePaths.Select(ApplicationSettings.GetAbsolutePath).ToList();
                            parserPlugin.IncludeFile = m_userEditorSettings.CurrentCompilingSettings.XmlIncludeFile;
                            parserPlugin.Custom = m_userEditorSettings.CurrentCompilingSettings.Custom;

                            projectResolver = parserPlugin.CreateProjectResolver(lowercaseName);
                            this.IntellisenseSettingsChanged += projectResolver.IntellisenseSettingsChangedEventHandler;
                        }
                        else
                        {
                            editor.Document.ResetLanguage();
                        }
                    }
                }

                // set the language
                if (projectResolver != null)
                {
                    projectResolver.SetLanguage(editor, m_InfoTipTextDel);
                }

                // set the color styles
                if (HighlightLanguage != null)
                {
                    HighlightLanguage(this, editor);
                }

                if (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor)
                {
                    WatcherInfo wInfo = new WatcherInfo(t.Filename);
                    wInfo.FileExternallyModified += new WatcherInfo.FileExternallyModifiedEventHandler(watcherInfo_FileExternallyModified);
                    wInfo.FileExternallyRenamed += new WatcherInfo.FileExternallyRenamedEventHandler(watcherInfo_FileExternallyRenamed);

                    Debug.Assert(!m_WatcherMap.ContainsKey(lowercaseName), "!m_WatcherMap.ContainsKey( " + lowercaseName + " )");
                    m_WatcherMap.Add(lowercaseName, wInfo);

                    // If the last OutliningMode was manual
                    if (t.OutliningMode == OutliningMode.Manual)
                    {
                        // We can always restore the OutliningMode.
                        editor.Document.Outlining.Mode = OutliningMode.Manual;

                        // If it is still the same file (or very likely to be), we can restore the OutliningNodes.
                        // Must wait until initial semantic parse finishes, though. Schedule a one-time hook against the semantic parse data changing.
                        if (File.GetLastWriteTime(t.Filename).ToFileTime() == t.FileDateTime)
                        {
                            EventHandler restoreOutlining = null;
                            restoreOutlining = (object obj, EventArgs e) =>
                            {
                                Document document = (Document) obj;
                                RestoreManualOutliningNodeInfo(t.OutlineNodeInfos, document);
                                document.SemanticParseDataChanged -= restoreOutlining;
                            };
                            editor.Document.SemanticParseDataChanged += restoreOutlining;
                        }
                    }
                }

                // now we're ready to turn on parsing and set the intellisense options
                editor.Document.LexicalParsingEnabled = true;
                editor.Document.SemanticParsingEnabled = m_userEditorSettings.IntellisenseSettings.PerformSemanticParsing;
                OnIntellisenseSettingsChanged(editor);

                eInfo = new EditorInfo(editor, dockControl, cmdLinks);

                Debug.Assert(!m_Editors.ContainsKey(editor.Document.Filename.ToLower()), "!m_Editors.ContainsKey( " + editor.Document.Filename.ToLower() + " )");
                m_Editors.Add(editor.Document.Filename.ToLower(), eInfo);

                Debug.Assert(!m_TabPageToEditorMap.ContainsKey(dockControl), "!m_TabPageToEditorMap.ContainsKey( " + dockControl.TabText + " )");
                m_TabPageToEditorMap.Add(dockControl, editor);

                // signal successful load
                OnFileLoaded(editor, t.Guid, false, m_performingMultiFileOperation, dockControl, false, string.Empty);

                dockControl.Open(TD.SandDock.WindowOpenMethod.OnScreenActivate);

                return true;
            }
        }

        /// <summary>
        /// Timed event that will push a message box to refresh files if any are queued.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnRefreshTimerElapsed(object sender, EventArgs e)
        {
            Timer timer = (Timer)sender;
            try
            {
                timer.Stop();
                // Breaking if m_modified files is empty inside lock, below
                while (true)
                {
                    String firstModifiedFile;
                    lock (m_modifiedFiles)
                    {
                        firstModifiedFile = m_modifiedFiles.FirstOrDefault();
                        if (firstModifiedFile == null)
                        {
                            break;
                        }
                    }

                    // Note that we do not lock m_modifiedFiles here, so that it can be populated with more items while the dialog is open
                    FileMessageBox messageBox = new FileMessageBox();
                    messageBox.Initialize(firstModifiedFile);
                    messageBox.ShowDialog(m_parentControl);

                    lock (m_modifiedFiles)
                    {
                        switch (messageBox.Result)
                        {
                            case FileMessageBox.ResultType.YesToAll:
                                {
                                    foreach (String modifiedFile in m_modifiedFiles)
                                    {
                                        ReloadFile(modifiedFile);
                                    }

                                    m_modifiedFiles.Clear();
                                }
                                break;
                            case FileMessageBox.ResultType.Yes:
                                {
                                    ReloadFile(firstModifiedFile);

                                    // Should be safe to assume 0 still contains this file as we only ever add to the list outside this thread.
                                    m_modifiedFiles.RemoveAt(0);
                                }
                                break;
                            case FileMessageBox.ResultType.NoToAll:
                                {
                                    foreach (String filename in m_modifiedFiles)
                                    {
                                        // The user selected "No to All" which means that the memory copies won't match disk.
                                        // So, mark the documents modified. This may trigger p4 operations now or later on
                                        EditorInfo eInfo;
                                        if (m_Editors.TryGetValue(filename.ToLower(), out eInfo))
                                        {
                                            eInfo.Editor.Document.Modified = true;
                                        }
                                    }

                                    m_modifiedFiles.Clear();
                                }
                                break;
                            case FileMessageBox.ResultType.No:
                                {
                                    // The user selected "No" which means that the memory copy won't match disk.
                                    // So, mark the document modified. This may trigger p4 operations now or later on.
                                    // Should be safe to assume 0 still contains this file as we only ever add to the list outside this thread.
                                    EditorInfo eInfo;
                                    if (m_Editors.TryGetValue(m_modifiedFiles[0].ToLower(), out eInfo))
                                    {
                                        eInfo.Editor.Document.Modified = true;
                                    }

                                    m_modifiedFiles.RemoveAt(0);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            finally
            {
                timer.Start();
            }
        }

        public bool LoadFiles( List<string> filenames )
        {
            List<TextEditorSettings> settings = new List<TextEditorSettings>();
            foreach ( string file in filenames )
            {
                settings.Add( new TextEditorSettings( file, System.Guid.NewGuid() ) );
            }

            return LoadFiles( settings );
        }

        public bool LoadFiles( List<TextEditorSettings> settings )
        {
            m_performingMultiFileOperation = true;

            List<string> filenames = new List<string>();
            foreach ( TextEditorSettings t in settings )
            {
                filenames.Add( t.Filename );
            }

            if ( !OnFileLoadingAll( filenames ) )
            {
                m_performingMultiFileOperation = false;
                return false;
            }

            List<string> failedFiles = new List<string>();
            List<string> reasons = new List<string>();

            foreach ( TextEditorSettings t in settings )
            {
                if ( IsANewFileName( t.Filename ) )
                {
                    if ( !CreateNewFile( t ) )
                    {
                        failedFiles.Add( t.Filename );
                        reasons.Add( "Load failed." );
                    }
                }
                else
                {
                    if ( !LoadFile( t ) )
                    {
                        failedFiles.Add( t.Filename );
                        reasons.Add( "Load failed." );
                    }
                }
            }

            OnFileLoadedAll( filenames, failedFiles, reasons );

            m_performingMultiFileOperation = false;

            //RefreshIntellisenseQuick();

            return true;
        }

        public bool ReloadFile( string filename )
        {
            return ReloadFile( filename, true );
        }

        public bool ReloadFile( string filename, bool silent )
        {
            EditorInfo eInfo;
            if ( m_Editors.TryGetValue( filename.ToLower(), out eInfo ) )
            {
                return ReloadFile( eInfo.Editor, silent );
            }

            return false;
        }

        public bool ReloadFile( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, bool silent )
        {
            string lowercaseName = editor.Document.Filename.ToLower();

            try
            {
                editor.Document.LoadFile( editor.Document.Filename );

                //RefreshIntellisenseQuick();

                return true;
            }
            catch ( Exception e )
            {
                if ( !silent )
                {
                    EditorInfo eInfo;
                    if ( m_Editors.TryGetValue( lowercaseName, out eInfo ) )
                    {
                        OnFileLoaded( editor, eInfo.TabPage.Guid, IsANewFileName( editor.Document.Filename ), false, eInfo.TabPage, true,
                            "Failed to reload: " + e.Message + "\n" + e.StackTrace );
                    }
                }
                else
                {
                    ApplicationSettings.Log.ToolException( e, "Exception attempting to reload {0}", editor.Document.Filename );
                }
            }

            return false;
        }

        public bool SaveFile( string filename )
        {
            EditorInfo eInfo;
            if ( m_Editors.TryGetValue( filename.ToLower(), out eInfo ) )
            {
                return SaveFile( eInfo.Editor );
            }

            return false;
        }

        public bool SaveFile( ActiproSoftware.SyntaxEditor.SyntaxEditor editor )
        {
            if ( IsANewFileName( editor.Document.Filename ) )
            {
                return SaveFileAs( editor );
            }
            else
            {
                return SaveFile( editor, editor.Document.Filename );
            }
        }

        public bool SaveFile( string text, string filename )
        {
            if ( !OnFileSaving( null, filename, m_performingMultiFileOperation ) )
            {
                // cancelled
                return false;
            }

            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false );

                writer.Write( text );

                writer.Close();

                OnFileSaved( null, filename, m_performingMultiFileOperation, false, string.Empty );
                return true;
            }
            catch ( Exception e )
            {
                // report error
                OnFileSaved( null, filename, m_performingMultiFileOperation, true, "'" + filename + "' failed to save: " + e.Message + "\n" + e.StackTrace );
            }

            return false;
        }

        public bool SaveFileAs( string filename )
        {
            EditorInfo eInfo;
            if ( m_Editors.TryGetValue( filename.ToLower(), out eInfo ) )
            {
                return SaveFileAs( eInfo.Editor );
            }

            return false;
        }

        public bool SaveFileAs( string filename, bool closeAndReopen )
        {
            EditorInfo eInfo;
            if ( m_Editors.TryGetValue( filename.ToLower(), out eInfo ) )
            {
                return SaveFileAs( eInfo.Editor, closeAndReopen );
            }

            return false;
        }

        public bool SaveFileAs( ActiproSoftware.SyntaxEditor.SyntaxEditor editor )
        {
            return SaveFileAs( editor, true );
        }

        public bool SaveAllFiles()
        {
            List<ActiproSoftware.SyntaxEditor.SyntaxEditor> editors = this.GetModifiedEditors();
            List<string> filenames = new List<string>();
            foreach ( ActiproSoftware.SyntaxEditor.SyntaxEditor editor in editors )
            {
                filenames.Add( editor.Document.Filename );
            }

            return SaveFiles( filenames );
        }

        public bool SaveFiles( List<string> files )
        {
            m_performingMultiFileOperation = true;

            if ( !OnFileSavingAll( files ) )
            {
                m_performingMultiFileOperation = false;
                return false;
            }

            List<string> filesNotSaved = new List<string>();
            List<string> reasons = new List<string>();

            foreach ( string file in files )
            {
                EditorInfo eInfo;
                if ( m_Editors.TryGetValue( file.ToLower(), out eInfo ) )
                {
                    if ( IsANewFileName( file ) )
                    {
                        if ( !SaveFileAs( eInfo.Editor, true ) )
                        {
                            filesNotSaved.Add( file );
                            reasons.Add( "Save failed or was cancelled." );
                        }
                    }
                    else
                    {
                        if ( !SaveFile( eInfo.Editor, file ) )
                        {
                            filesNotSaved.Add( file );
                            reasons.Add( "Save failed or was cancelled." );
                        }
                    }
                }
                else
                {
                    filesNotSaved.Add( file );
                    reasons.Add( "File not located in Editor map." );
                }
            }

            OnFileSavedAll( files, filesNotSaved, reasons );
            m_performingMultiFileOperation = false;
            return true;
        }

        public bool SaveFiles( List<ActiproSoftware.SyntaxEditor.SyntaxEditor> editors )
        {
            List<string> files = new List<string>();
            foreach ( ActiproSoftware.SyntaxEditor.SyntaxEditor editor in editors )
            {
                files.Add( editor.Document.Filename );
            }

            return SaveFiles( files );
        }

        public bool CloseFile( string filename )
        {
            EditorInfo eInfo;
            if ( m_Editors.TryGetValue( filename.ToLower(), out eInfo ) )
            {
                return CloseFile( eInfo.Editor );
            }

            return false;
        }

        public bool CloseFile( ActiproSoftware.SyntaxEditor.SyntaxEditor editor )
        {
            string filename = null;
            if ( editor.Document == null )
            {
                foreach ( KeyValuePair<string, EditorInfo> pair in m_Editors )
                {
                    if ( pair.Value.Editor == editor )
                    {
                        filename = pair.Key;
                        break;
                    }
                }
            }
            else
            {
                filename = editor.Document.Filename;
            }

            if ( filename == null )
            {
                return false;
            }

            bool newFile = IsANewFileName( filename );
            string lowercaseName = filename.ToLower();

            // locate EditorInfo so we have the dock control
            EditorInfo eInfo;
            if ( !m_Editors.TryGetValue( lowercaseName, out eInfo ) )
            {
                OnFileClosed( editor, newFile, null, m_performingMultiFileOperation, true, "File not found in Editor Map." );
                return false;
            }

            bool reparseAndReturnFalse = false;

            // check if we can close the file
            if ( !OnFileClosing( editor, newFile, eInfo.TabPage, m_performingMultiFileOperation ) )
            {
                // cancelled
                reparseAndReturnFalse = true;
            }
            else if ( (editor.Document != null) && editor.Document.Modified )
            {
                DialogResult result = rageMessageBox.ShowWarning( m_parentControl, 
                    String.Format ("{0}\r\nThis file has been modified. Do you wish to save it before closing?\r\n", filename  ), "File modified",
                    MessageBoxButtons.YesNoCancel );

                if ( result == DialogResult.Cancel )
                {
                    // cancelled
                    OnFileClosed( editor, newFile, eInfo.TabPage, m_performingMultiFileOperation, true, String.Empty );
                    reparseAndReturnFalse = true;
                }
                else if ( result == DialogResult.Yes )
                {
                    if ( ScriptEditor.IsANewFileName( filename ) )
                    {
                        if ( !SaveFileAs( editor, false ) )
                        {
                            OnFileClosed( editor, newFile, eInfo.TabPage, m_performingMultiFileOperation, true, "Unable to Save As" );
                            reparseAndReturnFalse = true;
                        }
                    }
                    else
                    {
                        if ( !SaveFile( editor ) )
                        {
                            OnFileClosed( editor, newFile, eInfo.TabPage, m_performingMultiFileOperation, true, "Unable to Save" );
                            reparseAndReturnFalse = true;
                        }
                    }
                }
            }

            if ( reparseAndReturnFalse )
            {                
                // reparse the file because we may have cancelled its requests
                if ( m_performingMultiFileOperation && (editor.Document != null) )
                {
                    RefreshIntellisense( editor );
                }

                return false;
            }

            WatcherInfo wInfo;
            if ( m_WatcherMap.TryGetValue( lowercaseName, out wInfo ) )
            {
                m_WatcherMap.Remove( lowercaseName );
                wInfo.Remove();
            }

            if ( editor == this.CurrentSyntaxEditor )
            {
                m_CurrentSyntaxEditor = null;
            }

            m_TabPageToEditorMap.Remove( eInfo.TabPage );
            m_Editors.Remove( lowercaseName );

            editor.DocumentIndicatorAdded -= new IndicatorEventHandler( editor_DocumentIndicatorAdded );
            editor.DocumentIndicatorRemoved -= new IndicatorEventHandler( editor_DocumentIndicatorRemoved );
            editor.DocumentTextChanged -= new DocumentModificationEventHandler( editor_DocumentTextChanged );
            editor.DocumentModifiedChanged -= new EventHandler(editor_DocumentModifiedChanged);
            editor.MacroRecordingStateChanged -= new EventHandler( editor_MacroRecordingStateChanged );
            editor.PasteDragDrop -= new PasteDragDropEventHandler( editor_PasteDragDrop );

            if ( editor.Document != null )
            {
                ClearParseRequests( editor.Document, null );

                // dispose of unused project resolver
                IParserProjectResolver projectResolver = editor.Document.LanguageData as IParserProjectResolver;
                if ( projectResolver != null )
                {
                    projectResolver.ClearLanguage( editor );

                    if ( projectResolver != m_projectParserProjectResolver )
                    {
                        IParserPlugin plugin;
                        if ( (editor.Document.LanguageData != null)
                            && sm_parserPluginsByExtension.TryGetValue( Path.GetExtension( lowercaseName ), out plugin ) )
                        {
                            IParserProjectResolver editorProjectResolver = editor.Document.LanguageData as IParserProjectResolver;
                            this.IntellisenseSettingsChanged -= editorProjectResolver.IntellisenseSettingsChangedEventHandler;

                            plugin.DestroyProjectResolver( editorProjectResolver );
                        }
                    }

                    editor.Document.LanguageData = null;
                }

                editor.Document.OutliningModeChanged -= new EventHandler( editorDocument_OutliningModeChanged );
                editor.Document.UndoRedoStateChanged -= new UndoRedoStateChangedEventHandler( editorDocument_UndoRedoStateChanged );
            }
            else
            {
                ClearParseRequests( null, lowercaseName );
            }

            m_automaticOutliningNodeInfos.Remove( lowercaseName );

            // close the tab doc here
            OnFileClosed( editor, newFile, eInfo.TabPage, m_performingMultiFileOperation, false, string.Empty );

            if ( editor.Document != null )
            {
                editor.Document.Dispose();
            }

            editor.Dispose();
            return true;
        }

        public bool CloseAllFiles()
        {
            List<string> files = GetAllOpenTabFilenames();
            return CloseFiles( files );
        }

        public bool CloseFiles( List<string> files )
        {
            m_performingMultiFileOperation = true;

            if ( !OnFileClosingAll( files ) )
            {
                m_performingMultiFileOperation = false;
                return false;
            }

            List<string> failedFiles = new List<string>();
            List<string> reasons = new List<string>();

            foreach ( string file in files )
            {
                EditorInfo eInfo;
                if ( m_Editors.TryGetValue( file.ToLower(), out eInfo ) )
                {
                    eInfo.TabPage.Close();
                }
            }

            OnFileClosedAll( files, failedFiles, reasons );

            m_performingMultiFileOperation = false;

            //RefreshIntellisenseQuick();

            return true;
        }

        public bool CloseFiles( List<ActiproSoftware.SyntaxEditor.SyntaxEditor> editors )
        {
            List<string> files = new List<string>();
            foreach ( ActiproSoftware.SyntaxEditor.SyntaxEditor editor in editors )
            {
                files.Add( editor.Document.Filename );
            }

            return CloseFiles( files );
        }

        private bool SaveFileAs( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, bool closeAndReopen )
        {
            // setup the save dialog
            saveFileDialog.Filter = BuildScriptFileDialogFilter();
            saveFileDialog.FileName = editor.Document.Filename.Trim( Path.GetInvalidFileNameChars() );

            saveFileDialog.InitialDirectory = ApplicationSettings.PrependProjectRoot(String.Empty);
            String currentDir = Path.GetDirectoryName(saveFileDialog.FileName);
            if (!String.IsNullOrEmpty(currentDir))
            {
                saveFileDialog.InitialDirectory = currentDir;
            }

            if ( saveFileDialog.ShowDialog( m_parentControl ) == DialogResult.OK )
            {
                // retrieve eInfo before we change the name
                EditorInfo eInfo = null;
                m_Editors.TryGetValue( editor.Document.Filename.ToLower(), out eInfo );

                // save the new file
                if ( !SaveFile( editor, saveFileDialog.FileName ) )
                {
                    return false;
                }

                if ( closeAndReopen )
                {
                    // close the old file
                    if ( eInfo != null )
                    {
                        eInfo.TabPage.Close();
                    }

                    // reload it so it gets the correct language and syntax highlighting
                    return LoadFile( saveFileDialog.FileName );
                }
                else
                {
                    return true;
                }
            }

            return false;
        }

        private bool SaveFile( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, string filename )
        {
            if ( !OnFileSaving( editor, filename, m_performingMultiFileOperation ) )
            {
                // cancelled
                return false;
            }

            //  Make a copy first if the file already exists
            if (File.Exists(filename))
            {
                string personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                DateTime date = DateTime.Now;
                string dateFolder = date.ToString("yyyy_MM_dd");
                string folderName = personalFolder + "\\ragScriptEditor\\Backups\\" + dateFolder + "\\";
                string newFileName = folderName + Path.GetFileNameWithoutExtension(filename) + ".bak";

                //  Create the directory
                Directory.CreateDirectory(folderName);
                //  Check for read only flag on existing backup file
                if (File.Exists(newFileName))
                {
                    //  Make writable
                    FileAttributes atts = File.GetAttributes(newFileName);
                    if ((atts & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        try
                        {
                            File.SetAttributes(newFileName, atts & (~FileAttributes.ReadOnly));
                        }
                        catch
                        {
                        }
                    }
                }
                //  Copy the file there
                File.Copy(filename, newFileName, true);
            }

            try
            {
                DisableFileWatcher( editor.Document.Filename );
                editor.Document.SaveFileBuffered( filename, ActiproSoftware.SyntaxEditor.LineTerminator.CarriageReturnNewline );
                editor.Document.Modified = false;
                EnableFileWatcher( editor.Document.Filename );

                OnFileSaved( editor, filename, m_performingMultiFileOperation, false, string.Empty );
                return true;
            }
            catch ( Exception e )
            {
                // report error
                OnFileSaved( editor, filename, m_performingMultiFileOperation, true, "'" + filename + "' failed to save: " + e.Message + "\n" + e.StackTrace );
            }

            return false;
        }

        private void CreateEditor( TextEditorSettings t, out SyntaxEditor editor, out SearchCommandLinks cmdLinks )
        {
            editor = new SaferSyntaxEditor();
            editor.ContentDividersVisible = true;
            editor.Dock = System.Windows.Forms.DockStyle.Fill;
            editor.Location = new System.Drawing.Point( 0, 0 );
            editor.Name = "syntaxEditor" + m_TabIndex++;
            editor.Size = new System.Drawing.Size( 608, 578 );
            editor.TabIndex = 1;
            editor.BracketHighlightingVisible = true;
            editor.BracketHighlightingInclusive = true;
            editor.LineNumberMarginVisible = m_userEditorSettings.Files.ShowLineNumbers;
            editor.AllowDrop = true;

            editor.DocumentIndicatorAdded += new IndicatorEventHandler( editor_DocumentIndicatorAdded );
            editor.DocumentIndicatorRemoved += new IndicatorEventHandler( editor_DocumentIndicatorRemoved );
            editor.DocumentTextChanged += new DocumentModificationEventHandler( editor_DocumentTextChanged );
            editor.DocumentModifiedChanged += new EventHandler(editor_DocumentModifiedChanged);
            editor.MacroRecordingStateChanged += new EventHandler( editor_MacroRecordingStateChanged );
            editor.PasteDragDrop += new PasteDragDropEventHandler( editor_PasteDragDrop );

            editor.IntelliPrompt.QuickInfo.CollapsedOutliningNodeSyntaxHighlightingEnabled = m_userEditorSettings.IntellisenseSettings.HighlightQuickInfoPopups;

            editor.Document = new ActiproSoftware.SyntaxEditor.Document();
            editor.Document.ReadOnly = ApplicationSettings.Mode != ApplicationSettings.ApplicationMode.Editor;
            editor.Document.LineModificationMarkingEnabled = true;
            editor.Document.UndoRedo.UndoStack.Capacity = 200;  // North designers requested that the undo stack be larger:            

            editor.Document.OutliningModeChanged += new EventHandler( editorDocument_OutliningModeChanged );
            editor.Document.UndoRedoStateChanged += new UndoRedoStateChangedEventHandler( editorDocument_UndoRedoStateChanged );

            // remove some duplicate bindings so we can assign them to other commands
            for ( int i = 0; i < editor.CommandLinks.Count; )
            {
                if ( (editor.CommandLinks[i].Command is CutToClipboardCommand) || (editor.CommandLinks[i].Command is PasteFromClipboardCommand) )
                {
                    if ( editor.CommandLinks[i].KeyBinding.Modifiers == ActiproSoftware.WinUICore.Input.ModifierKeys.Shift )
                    {
                        editor.CommandLinks.RemoveAt( i );
                    }
                    else
                    {
                        ++i;
                    }
                }
                else if ( editor.CommandLinks[i].Command is CopyToClipboardCommand )
                {
                    if ( editor.CommandLinks[i].KeyBinding.Key != Keys.C )
                    {
                        editor.CommandLinks.RemoveAt( i );
                    }
                    else
                    {
                        ++i;
                    }
                }
                else if ( (editor.CommandLinks[i].Command is CodeBlockSelectionContractCommand) || (editor.CommandLinks[i].Command is CodeBlockSelectionExpandCommand) )
                {
                    ++i;
                }
                else
                {
                    ++i;
                }
            }

            editor.CommandLinks.Add( new ActiproSoftware.WinUICore.Commands.CommandLink( new GetImplementationInformationCommand( this ),
                new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.Alt, Keys.G ) ) );
            editor.CommandLinks.Add( new ActiproSoftware.WinUICore.Commands.CommandLink( new GoToLineCommand( this ),
                new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.Control, Keys.G ) ) );
            
            editor.CommandLinks.Add( new ActiproSoftware.WinUICore.Commands.CommandLink( new DebugBreakPointCommand( this ),
                new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.None, Keys.F9 ) ) );
            editor.CommandLinks.Add( new ActiproSoftware.WinUICore.Commands.CommandLink( new DebugBreakPointAllThreadsCommand( this ),
                new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.Shift, Keys.F9 ) ) );
            editor.CommandLinks.Add( new ActiproSoftware.WinUICore.Commands.CommandLink( new DebugBreakPointStopGameCommand( this ),
                new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.Control, Keys.F9 ) ) );
            editor.CommandLinks.Add( new ActiproSoftware.WinUICore.Commands.CommandLink( new DebugBreakPointAllThreadsStopGameCommand( this ),
                new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.ControlShift, Keys.F9 ) ) );

            editor.CommandLinks.Add( new ActiproSoftware.WinUICore.Commands.CommandLink( new DebugContinueCommand( this ),
                new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.None, Keys.F5 ) ) );
            editor.CommandLinks.Add( new ActiproSoftware.WinUICore.Commands.CommandLink( new DebugPauseCommand( this ),
                new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.None, Keys.F12 ) ) );
            editor.CommandLinks.Add( new ActiproSoftware.WinUICore.Commands.CommandLink( new DebugStepIntoCommand( this ),
                new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.None, Keys.F11 ) ) );
            editor.CommandLinks.Add( new ActiproSoftware.WinUICore.Commands.CommandLink( new DebugStepOverCommand( this ),
                new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.None, Keys.F10 ) ) );
            editor.CommandLinks.Add( new ActiproSoftware.WinUICore.Commands.CommandLink( new DebugStepOutCommand( this ),
                new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.Shift, Keys.F11 ) ) );

            cmdLinks = new SearchCommandLinks();
            cmdLinks.SetupSearchCommandLinks( editor, m_searchReplaceDialog, m_findReplaceForm, 
                m_userEditorSettings.FindReplaceSettings.UseNewDialog );

            // set line position;
            DocumentLine line = null;
            if ( t.LinePosition < editor.Document.Lines.Count )
            {
                line = editor.Document.Lines[t.LinePosition];
            }

            if ( line != null )
            {
                editor.SelectedView.Selection.StartOffset = line.StartOffset;
                editor.SelectedView.Selection.EndOffset = line.StartOffset;
            }

            if ( (t.HorizontalSplitPosition > -1) && (t.VerticalSplitPosition > -1) )
            {
                editor.SplitType = SyntaxEditorSplitType.FourWay;
                editor.HorizontalSplitPosition = t.HorizontalSplitPosition;
                editor.VerticalSplitPosition = t.VerticalSplitPosition;
            }
            else if ( t.HorizontalSplitPosition > -1 )
            {
                editor.SplitType = SyntaxEditorSplitType.DualHorizontal;
                editor.HorizontalSplitPosition = t.HorizontalSplitPosition;
            }
            else if ( t.VerticalSplitPosition > -1 )
            {
                editor.SplitType = SyntaxEditorSplitType.DualVertical;
                editor.VerticalSplitPosition = t.VerticalSplitPosition;
            }
            else
            {
                editor.SplitType = SyntaxEditorSplitType.None;
            }
        }

        private void SelectionChanged(Object sender, SelectionEventArgs e)
        {
            const int MinDiffToAddMarker = 11;
            if (Math.Abs(e.CaretDocumentPosition.Line - e.LastCaretDocumentPosition.Line) > MinDiffToAddMarker)
            {
                SyntaxEditor editor = (SyntaxEditor)sender;
                NavigationPosition pos = new NavigationPosition(editor.Document.Filename, editor.Document.PositionToOffset(e.LastCaretDocumentPosition));
                this.m_navManager.AddPositionMarker(pos);
            }
        }

        private TD.SandDock.DockControl OnFileLoading( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, System.Guid guid,
            bool newFile, bool loadMultiple )
        {
            editor.SelectionChanged += new SelectionEventHandler(this.SelectionChanged);

            if ( this.FileLoading != null )
            {
                FileLoadingEventArgs e = new FileLoadingEventArgs( editor, guid, newFile, loadMultiple );
                this.FileLoading( this, e );

                if ( !e.Cancel )
                {
                    Debug.Assert( e.DockControl != null, "e.DockControl != null" );
                    return e.DockControl;
                }
            }

            return null;
        }

        private void OnFileLoaded( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, System.Guid guid, bool newFile, bool loadMultiple,
            TD.SandDock.DockControl dockControl, bool cancelled, string reason )
        {
            if ( this.FileLoaded != null )
            {
                FileLoadedEventArgs e = new FileLoadedEventArgs( editor, guid, newFile, loadMultiple, dockControl, cancelled, reason );
                this.FileLoaded( this, e );
            }
        }

        private bool OnFileLoadingAll( List<string> files )
        {
            if ( this.FileLoadingAll != null )
            {
                FileAllPreOperationEventArgs e = new FileAllPreOperationEventArgs( files );
                this.FileLoadingAll( this, e );
                return !e.Cancel;
            }

            return true;
        }

        private void OnFileLoadedAll( List<string> files, List<string> failedFiles, List<string> reasons )
        {
            if ( this.FileLoadedAll != null )
            {
                FileAllPostOperationEventArgs e = new FileAllPostOperationEventArgs( files, failedFiles, reasons );
                this.FileLoadedAll( this, e );
            }
        }

        private bool OnFileSaving( SyntaxEditor editor, string filename, bool saveAll )
        {
            if ( this.FileSaving != null )
            {
                FileSavingEventArgs e = new FileSavingEventArgs( editor, filename, saveAll );
                this.FileSaving( this, e );
                return !e.Cancel;
            }

            return true;
        }

        private void OnFileSaved( SyntaxEditor editor, string filename, bool saveAll,
            bool cancelled, string reason )
        {
            if ( this.FileSaved != null )
            {
                FileSavedEventArgs e = new FileSavedEventArgs( editor, filename, saveAll, cancelled, reason );
                this.FileSaved( this, e );
            }

            RefreshFileMenuItems();
        }

        private bool OnFileSavingAll( List<string> files )
        {
            if ( this.FileSavingAll != null )
            {
                FileAllPreOperationEventArgs e = new FileAllPreOperationEventArgs( files );
                this.FileSavingAll( this, e );
                return !e.Cancel;
            }

            return true;
        }

        private void OnFileSavedAll( List<string> files, List<string> failedFiles, List<string> reasons )
        {
            if ( this.FileSavedAll != null )
            {
                FileAllPostOperationEventArgs e = new FileAllPostOperationEventArgs( files, failedFiles, reasons );
                this.FileSavedAll( this, e );
            }

            RefreshFileMenuItems();
        }

        private bool OnFileClosing( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, bool newFile,
            TD.SandDock.DockControl dockControl, bool closeMultiple )
        {
            if ( this.FileClosing != null )
            {
                FileClosingEventArgs e = new FileClosingEventArgs( editor, newFile, dockControl, closeMultiple );
                this.FileClosing( this, e );
                return !e.Cancel;
            }

            return true;
        }

        private void OnFileClosed( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, bool newFile,
            TD.SandDock.DockControl dockControl, bool closeMultiple, bool cancelled, string reason )
        {
            if ( this.FileClosed != null )
            {
                FileClosedEventArgs e = new FileClosedEventArgs( editor, newFile, dockControl, closeMultiple, cancelled, reason );
                this.FileClosed( this, e );
            }
        }

        private bool OnFileClosingAll( List<string> files )
        {
            if ( this.FileClosingAll != null )
            {
                FileAllPreOperationEventArgs e = new FileAllPreOperationEventArgs( files );
                this.FileClosingAll( this, e );
                return !e.Cancel;
            }

            return true;
        }

        private void OnFileClosedAll( List<string> files, List<string> failedFiles, List<string> reasons )
        {
            if ( this.FileClosedAll != null )
            {
                FileAllPostOperationEventArgs e = new FileAllPostOperationEventArgs( files, failedFiles, reasons );
                this.FileClosedAll( this, e );
            }
        }
        #endregion

        #region File Watcher
        private void watcherInfo_FileExternallyModified( object sender, FileSystemEventArgs e )
        {
            WatcherInfo wInfo = sender as WatcherInfo;
            wInfo.EnableRaisingEvents = false;

            lock (m_modifiedFiles)
            {
                if (!m_modifiedFiles.Contains(wInfo.FileName, StringComparer.OrdinalIgnoreCase))
                {
                    m_modifiedFiles.Add(wInfo.FileName);
                }
            }

            wInfo.EnableRaisingEvents = true;
        }

        private void watcherInfo_FileExternallyRenamed( object sender, RenamedEventArgs e )
        {
            if ( m_Editors.ContainsKey( e.FullPath.ToLower() ) )
            {
                ApplicationSettings.Log.Message( "Rename {0} -> {1}, (probably Perforce syncing/merging)", e.OldFullPath, e.FullPath);

                FileSystemEventArgs args = new FileSystemEventArgs( e.ChangeType, Path.GetDirectoryName( e.FullPath ), e.FullPath );
                watcherInfo_FileExternallyModified( sender, args );
            }
        }

        public void DisableFileWatcher( string fileName )
        {
            WatcherInfo wInfo;
            if ( m_WatcherMap.TryGetValue( fileName.ToLower(), out wInfo ) )
            {
                wInfo.EnableRaisingEvents = false;
            }
        }

        public void EnableFileWatcher( string fileName )
        {
            WatcherInfo wInfo;
            if ( m_WatcherMap.TryGetValue( fileName.ToLower(), out wInfo ) )
            {
                wInfo.EnableRaisingEvents = true;
            }
        }
        #endregion

        #region Misc
        public List<ActiproSoftware.SyntaxEditor.SyntaxEditor> GetModifiedEditors()
        {
            List<ActiproSoftware.SyntaxEditor.SyntaxEditor> editors = new List<ActiproSoftware.SyntaxEditor.SyntaxEditor>();

            foreach ( KeyValuePair<string, EditorInfo> pair in m_Editors )
            {
                if ( (pair.Value.Editor != null) && (pair.Value.Editor.Document != null)
                    && pair.Value.Editor.Document.Modified )
                {
                    editors.Add( pair.Value.Editor );
                }
            }

            return editors;
        }

        private static string GetNewFileName()
        {
            sm_NewFileNum++;
            return c_newFileNamePrefix + " " + sm_NewFileNum + ">";
        }

        public static bool IsANewFileName( string fileName )
        {
            return fileName?.StartsWith( c_newFileNamePrefix ) ?? false;
        }

        private int GetNewFileNameIndex( string filename )
        {
            string s = filename.Replace( c_newFileNamePrefix, "" );
            s = s.Replace( ">", "" ).Trim();
            return int.Parse( s );
        }

        public void HighlightAllEditors()
        {
            if ( HighlightLanguage == null )
            {
                return;
            }

            foreach ( KeyValuePair<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> pair in m_TabPageToEditorMap )
            {
                HighlightLanguage( this, pair.Value );
            }
        }

        public void SetLineNumberMarginVisible( bool flag )
        {
            foreach ( KeyValuePair<string, EditorInfo> pair in m_Editors )
            {
                pair.Value.Editor.LineNumberMarginVisible = flag;
            }
        }

        public List<string> GetAllOpenTabFilenames()
        {
            if ( this.CurrentSyntaxEditor == null )
            {
                return new List<string>( m_Editors.Keys );
            }

            EditorInfo currentEInfo;
            if ( !m_Editors.TryGetValue( this.CurrentSyntaxEditor.Document.Filename.ToLower(), out currentEInfo ) )
            {
                return new List<string>( m_Editors.Keys );
            }

            SandDockManager mgr = currentEInfo.TabPage.Manager;
            if ( mgr == null )
            {
                return new List<string>( m_Editors.Keys );
            }

            DockContainer container = mgr.FindDockContainer( ContainerDockLocation.Center );
            if ( container == null )
            {
                return new List<string>( m_Editors.Keys );
            }

            SplitLayoutSystem splitLayoutSystem = container.LayoutSystem as SplitLayoutSystem;
            if ( splitLayoutSystem == null )
            {
                return new List<string>( m_Editors.Keys );
            }

            List<string> filenames = new List<string>();
            GetAllOpenTabFilenamesRecurse( splitLayoutSystem, filenames );

            return filenames;
        }

        private void GetAllOpenTabFilenamesRecurse( LayoutSystemBase layoutSystem, List<string> filenames )
        {
            if ( layoutSystem is ControlLayoutSystem )
            {
                ControlLayoutSystem controlLayoutSystem = layoutSystem as ControlLayoutSystem;
                foreach ( DockControl ctrl in controlLayoutSystem.Controls )
                {
                    SyntaxEditor editor;
                    if ( m_TabPageToEditorMap.TryGetValue( ctrl, out editor ) )
                    {
                        string filename = editor.Document.Filename.ToLower();
                        if ( !filenames.Contains( filename ) )
                        {
                            filenames.Add( filename );
                        }
                    }
                }
            }
            else if ( layoutSystem is SplitLayoutSystem )
            {
                SplitLayoutSystem splitLayoutSystem = layoutSystem as SplitLayoutSystem;
                foreach ( LayoutSystemBase layoutSubSystem in splitLayoutSystem.LayoutSystems )
                {
                    GetAllOpenTabFilenamesRecurse( layoutSubSystem, filenames );
                }
            }
        }

        public List<string> GetProjectAndOpenFilenames()
        {
            List<string> openFilenames = GetAllOpenTabFilenames();

            List<string> filenames = new List<string>();
            filenames.AddRange( openFilenames );

            if ( m_projectExplorer != null )
            {
                List<string> projectFilenames = m_projectExplorer.GetProjectFilenames();
                foreach ( string projectFilename in projectFilenames )
                {
                    if ( !openFilenames.Contains( projectFilename ) )
                    {
                        filenames.Add( projectFilename );
                    }
                }
            }

            return filenames;
        }

        public bool IsCodeSnippetPathValid()
        {
            LanguageSettings languageSettings = this.CurrentLanguageSettings;
            if ( languageSettings != null )
            {
                return languageSettings.CodeSnippetFolder != null;
            }

            return false;
        }

        private void OnNavigationChanged(Object sender, EventArgs e)
        {
            NavigationManager manager = (NavigationManager)sender;
            this.EnableMenuItem(AppAction.GoForward, manager.CanNavigateForward);
            this.EnableMenuItem(AppAction.GoBack, manager.CanNavigateBack);
        }

        /// <summary>
        /// Called when a new tabbed document is selected
        /// </summary>
        /// <param name="dockControl"></param>
        /// <param name="treeView"></param>
        public void SetCurrentEditorFromTabControl( TD.SandDock.DockControl dockControl )
        {
            if ( this.CurrentSyntaxEditor != null )
            {
                // fix for refresh problem with many files open
                this.CurrentSyntaxEditor.ResetDoubleBufferCanvas( false );

                // fix for parameter info not disappearing when we switch tabs
                this.CurrentSyntaxEditor.IntelliPrompt.ParameterInfo.Hide();

                this.m_navManager.AddPositionMarker(new NavigationPosition(this.CurrentSyntaxEditor));
            }

            if ( dockControl == null )
            {
                m_CurrentSyntaxEditor = null;

                bool debugging = ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Debugger;

                RefreshFileMenuItems();
                EnableMenuItem( AppAction.FileSaveAsHtml, false );
                EnableMenuItem( AppAction.FileSaveAsRtf, false );
                EnableMenuItem( AppAction.FileClose, false );
                EnableMenuItem( AppAction.FileCloseAll, false );
                EnableMenuItem( AppAction.FilePrint, false );
                EnableMenuItem( AppAction.FilePrintPreview, false );
                EnableMenuItem( AppAction.EditCut, false );
                EnableMenuItem( AppAction.EditCutAndAppend, false );
                EnableMenuItem( AppAction.EditCopy, false );
                EnableMenuItem( AppAction.EditCopyAndAppend, false );
                EnableMenuItem( AppAction.EditPaste, false );
                EnableMenuItem( AppAction.EditDelete, false );
                EnableMenuItem( AppAction.EditUndo, false );
                EnableMenuItem( AppAction.EditRedo, false );
                EnableMenuItem( AppAction.EditIndent, false );
                EnableMenuItem( AppAction.EditOutdent, false );
                EnableMenuItem( AppAction.EditSelectAll, false );
                EnableMenuItem( AppAction.EditConvertTabsToSpaces, false );
                EnableMenuItem( AppAction.EditConvertSpacesToTabs, false );
                EnableMenuItem( AppAction.EditTabifySelectedLines, false );
                EnableMenuItem( AppAction.EditUntabifySelectedLines, false );
                EnableMenuItem( AppAction.EditCommentSelection, false );
                EnableMenuItem( AppAction.EditUncommentSelection, false );
                EnableMenuItem( AppAction.EditMakeLowercase, false );
                EnableMenuItem( AppAction.EditMakeUppercase, false );
                EnableMenuItem( AppAction.EditToggleCase, false );
                EnableMenuItem( AppAction.EditCapitalize, false );
                EnableMenuItem( AppAction.EditDeleteHorizontalWhitespace, false );
                EnableMenuItem( AppAction.EditTrimTrailingWhitespace, false );
                EnableMenuItem( AppAction.EditDeleteLine, false );
                EnableMenuItem( AppAction.EditDuplicateLine, false );
                EnableMenuItem( AppAction.EditDeleteBlankLines, false );
                EnableMenuItem( AppAction.EditTransposeLines, false );
                EnableMenuItem( AppAction.EditTransposeWords, false );
                EnableMenuItem( AppAction.EditTransposeCharacters, false );
                EnableMenuItem( AppAction.EditToggleBookmark, false );
                EnableMenuItem( AppAction.EditEnableBookmark, false );
                EnableMenuItem( AppAction.EditNextBookmark, false );
                EnableMenuItem( AppAction.EditPreviousBookmark, false );
                EnableMenuItem( AppAction.EditClearBookmarks, false );
                EnableMenuItem( AppAction.EditInsertFile, false );
                EnableMenuItem( AppAction.EditListMembers, false );
                EnableMenuItem( AppAction.EditParameterInfo, false );
                EnableMenuItem( AppAction.EditQuickInfo, false );
                EnableMenuItem( AppAction.EditCompleteWord, false );
                EnableMenuItem( AppAction.EditInsertCodeSnippet, false );
                EnableMenuItem( AppAction.OutliningHideSelection, false );
                EnableMenuItem( AppAction.OutliningToggleOutliningExpansion, false );
                EnableMenuItem( AppAction.OutliningToggleAllOutlining, false );
                EnableMenuItem( AppAction.OutliningStopOutlining, false );
                EnableMenuItem( AppAction.OutliningStopHidingCurrent, false );
                EnableMenuItem( AppAction.OutliningStartAutomaticOutlining, false );
                EnableMenuItem( AppAction.OutliningStartManualOutlining, false );
                EnableMenuItem( AppAction.SearchAgainDown, false );
                EnableMenuItem( AppAction.SearchAgainUp, false );
                EnableMenuItem( AppAction.SearchHighlightedDown, false );
                EnableMenuItem( AppAction.SearchHighlightedUp, false );
                EnableMenuItem( AppAction.SearchIncremental, false );
                EnableMenuItem( AppAction.SearchReverseIncremental, false );
                EnableMenuItem( AppAction.SearchGoToLine, false );
                EnableMenuItem( AppAction.SearchGoToDefinition, false );
                EnableMenuItem( AppAction.SearchGoToProgramCounter, false );
                EnableMenuItem( AppAction.ToolsMacrosRecord, false );
                EnableMenuItem( AppAction.ToolsMacrosPause, false );
                EnableMenuItem( AppAction.ToolsMacrosStop, false );
                EnableMenuItem( AppAction.ToolsMacrosCancel, false );
                EnableMenuItem( AppAction.ToolsMacrosRun, false );
                EnableMenuItem( AppAction.ToolsTextStatistics, false );
                EnableMenuItem( AppAction.ToolsSpellCheck, false );
                EnableMenuItem( AppAction.WindowNoSplits, false );
                EnableMenuItem( AppAction.WindowSplitFourWay, false );
                EnableMenuItem( AppAction.WindowSplitHorizontally, false );
                EnableMenuItem( AppAction.WindowSplitVertically, false );
                EnableMenuItem( AppAction.DebugToggleBreakpoint, false );
                EnableMenuItem( AppAction.DebugToggleBreakpointAllThreads, false );
                EnableMenuItem( AppAction.DebugToggleBreakpointStopGame, false );
                EnableMenuItem( AppAction.DebugToggleBreakpointAllThreadsStopGame, false );
                EnableMenuItem( AppAction.DebugContinue, false );
                EnableMenuItem( AppAction.DebugPause, false );
                EnableMenuItem( AppAction.DebugStepInto, false );
                EnableMenuItem( AppAction.DebugStepOver, false );
                EnableMenuItem( AppAction.DebugStepOut, false );
                EnableMenuItem( AppAction.CancelIntellisense, false );
                EnableMenuItem( AppAction.RefreshIntellisense, false );
            }
            else
            {
                ActiproSoftware.SyntaxEditor.SyntaxEditor editor;
                if ( m_TabPageToEditorMap.TryGetValue( dockControl, out editor ) )
                {
                    m_CurrentSyntaxEditor = editor;

                    bool debugging = ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Debugger;
                    bool manualOutliningOn = editor.Document.Outlining.Mode == ActiproSoftware.SyntaxEditor.OutliningMode.Manual;
                    bool automaticOutliningOn = editor.Document.Outlining.Mode == ActiproSoftware.SyntaxEditor.OutliningMode.Automatic;
                    bool recording = editor.MacroRecording.State == MacroRecordingState.Recording;
                    bool paused = editor.MacroRecording.State == MacroRecordingState.Paused;
                    bool stopped = editor.MacroRecording.State == MacroRecordingState.Stopped;
                    bool haveMacro = (this.CurrentSyntaxEditor.MacroRecording.LastMacroCommand != null) || (m_lastMacroCommand != null);
                    bool haveBookmark = this.CurrentSyntaxEditor.Document.LineIndicators.Contains( ActiproSoftware.SyntaxEditor.BookmarkLineIndicator.DefaultName );

                    RefreshFileMenuItems();
                    EnableMenuItem( AppAction.FileSaveAsHtml, !debugging );
                    EnableMenuItem( AppAction.FileSaveAsRtf, !debugging );
                    EnableMenuItem( AppAction.FileClose, true );
                    EnableMenuItem( AppAction.FileCloseAll, true );
                    EnableMenuItem( AppAction.FilePrint, true );
                    EnableMenuItem( AppAction.FilePrintPreview, true );
                    EnableMenuItem( AppAction.EditCut, !debugging );
                    EnableMenuItem( AppAction.EditCutAndAppend, !debugging );
                    EnableMenuItem( AppAction.EditCopy, true );
                    EnableMenuItem( AppAction.EditCopyAndAppend, true );
                    EnableMenuItem( AppAction.EditPaste, !debugging );
                    EnableMenuItem( AppAction.EditDelete, !debugging );
                    EnableMenuItem( AppAction.EditUndo, editor.Document.UndoRedo.CanUndo && !debugging );
                    EnableMenuItem( AppAction.EditRedo, editor.Document.UndoRedo.CanRedo && !debugging );
                    EnableMenuItem( AppAction.EditIndent, !debugging );
                    EnableMenuItem( AppAction.EditOutdent, !debugging );
                    EnableMenuItem( AppAction.EditSelectAll, true );
                    EnableMenuItem( AppAction.EditConvertTabsToSpaces, !debugging );
                    EnableMenuItem( AppAction.EditConvertSpacesToTabs, !debugging );
                    EnableMenuItem( AppAction.EditTabifySelectedLines, !debugging );
                    EnableMenuItem( AppAction.EditUntabifySelectedLines, !debugging );
                    EnableMenuItem( AppAction.EditCommentSelection, !debugging );
                    EnableMenuItem( AppAction.EditUncommentSelection, !debugging );
                    EnableMenuItem( AppAction.EditMakeLowercase, !debugging );
                    EnableMenuItem( AppAction.EditMakeUppercase, !debugging );
                    EnableMenuItem( AppAction.EditToggleCase, !debugging );
                    EnableMenuItem( AppAction.EditCapitalize, !debugging );
                    EnableMenuItem( AppAction.EditDeleteHorizontalWhitespace, !debugging );
                    EnableMenuItem( AppAction.EditTrimTrailingWhitespace, !debugging );
                    EnableMenuItem( AppAction.EditDeleteLine, !debugging );
                    EnableMenuItem( AppAction.EditDuplicateLine, !debugging );
                    EnableMenuItem( AppAction.EditDeleteBlankLines, !debugging );
                    EnableMenuItem( AppAction.EditTransposeLines, !debugging );
                    EnableMenuItem( AppAction.EditTransposeWords, !debugging );
                    EnableMenuItem( AppAction.EditTransposeCharacters, !debugging );
                    EnableMenuItem( AppAction.EditToggleBookmark, true );
                    EnableMenuItem( AppAction.EditEnableBookmark, haveBookmark );
                    EnableMenuItem( AppAction.EditNextBookmark, haveBookmark );
                    EnableMenuItem( AppAction.EditPreviousBookmark, haveBookmark );
                    EnableMenuItem( AppAction.EditClearBookmarks, haveBookmark );
                    EnableMenuItem( AppAction.EditInsertFile, !debugging );
                    EnableMenuItem( AppAction.EditListMembers, !debugging );
                    EnableMenuItem( AppAction.EditParameterInfo, !debugging );
                    EnableMenuItem( AppAction.EditQuickInfo, !debugging );
                    EnableMenuItem( AppAction.EditCompleteWord, !debugging );
                    EnableMenuItem( AppAction.EditInsertCodeSnippet, this.IsCodeSnippetPathValid() && !debugging );
                    EnableMenuItem( AppAction.OutliningHideSelection, manualOutliningOn );
                    EnableMenuItem( AppAction.OutliningToggleOutliningExpansion, manualOutliningOn || automaticOutliningOn );
                    EnableMenuItem( AppAction.OutliningToggleAllOutlining, manualOutliningOn || automaticOutliningOn );
                    EnableMenuItem( AppAction.OutliningStopOutlining, manualOutliningOn || automaticOutliningOn );
                    EnableMenuItem( AppAction.OutliningStopHidingCurrent, manualOutliningOn );
                    EnableMenuItem( AppAction.OutliningStartAutomaticOutlining, !manualOutliningOn );
                    EnableMenuItem( AppAction.OutliningStartManualOutlining, !automaticOutliningOn );
                    EnableMenuItem( AppAction.SearchAgainDown, true );
                    EnableMenuItem( AppAction.SearchAgainUp, true );
                    EnableMenuItem( AppAction.SearchHighlightedDown, true );
                    EnableMenuItem( AppAction.SearchHighlightedUp, true );
                    EnableMenuItem( AppAction.SearchIncremental, true );
                    EnableMenuItem( AppAction.SearchReverseIncremental, true );
                    EnableMenuItem( AppAction.SearchGoToLine, true );
                    EnableMenuItem( AppAction.SearchGoToDefinition, true );
                    EnableMenuItem( AppAction.SearchGoToProgramCounter, true );
                    EnableMenuItem( AppAction.ToolsMacrosRecord, !debugging && !recording && !paused );
                    EnableMenuItem( AppAction.ToolsMacrosPause, !debugging && !stopped );
                    EnableMenuItem( AppAction.ToolsMacrosStop, !debugging && !stopped );
                    EnableMenuItem( AppAction.ToolsMacrosCancel, !debugging && !stopped );
                    EnableMenuItem( AppAction.ToolsMacrosRun, !debugging && !recording && !paused && haveMacro );
                    EnableMenuItem( AppAction.ToolsTextStatistics, true );
                    EnableMenuItem( AppAction.ToolsSpellCheck, !debugging );
                    EnableMenuItem( AppAction.WindowNoSplits, true );
                    EnableMenuItem( AppAction.WindowSplitFourWay, true );
                    EnableMenuItem( AppAction.WindowSplitHorizontally, true );
                    EnableMenuItem( AppAction.WindowSplitVertically, true );
                    EnableMenuItem( AppAction.DebugToggleBreakpoint, debugging && (m_debuggerState != DebuggerState.Stopped) );
                    EnableMenuItem( AppAction.DebugToggleBreakpointAllThreads, debugging && (m_debuggerState != DebuggerState.Stopped) );
                    EnableMenuItem( AppAction.DebugToggleBreakpointStopGame, debugging && (m_debuggerState != DebuggerState.Stopped) );
                    EnableMenuItem( AppAction.DebugToggleBreakpointAllThreadsStopGame, debugging && (m_debuggerState != DebuggerState.Stopped) );
                    EnableMenuItem( AppAction.DebugContinue, debugging && (m_debuggerState == DebuggerState.Paused) );
                    EnableMenuItem( AppAction.DebugPause, debugging && (m_debuggerState == DebuggerState.Running) );
                    EnableMenuItem( AppAction.DebugStepInto, debugging && (m_debuggerState == DebuggerState.Paused) );
                    EnableMenuItem( AppAction.DebugStepOver, debugging && (m_debuggerState == DebuggerState.Paused) );
                    EnableMenuItem( AppAction.DebugStepOut, debugging && (m_debuggerState == DebuggerState.Paused) );
                    EnableMenuItem( AppAction.CancelIntellisense, true );
                    EnableMenuItem( AppAction.RefreshIntellisense, true );

                    //if ( !m_performingMultiFileOperation && m_userEditorSettings.IntellisenseSettings.RefreshIntellisenseOnSelect )
                    //{
                    //    RefreshIntellisenseQuick();
                    //}
                }
            }
        }

        public bool UpdateDocumentOutline( TreeView treeView )
        {
            if ( (this.CurrentSyntaxEditor != null) && !IsANewFileName( this.CurrentSyntaxEditor.Document.Filename ) )
            {
                string lowercaseName = this.CurrentSyntaxEditor.Document.Filename.ToLower();

                IParserPlugin parserPlugin;
                if ( sm_parserPluginsByExtension.TryGetValue( Path.GetExtension( lowercaseName ), out parserPlugin ) )
                {
                    if ( m_parentControl != null )
                    {
                        object[] args = new object[2];
                        args[0] = this.CurrentSyntaxEditor;
                        args[1] = treeView;

                        object rtn = m_parentControl.Invoke( new UpdateDocumentOutlineDelegate( parserPlugin.UpdateDocumentOutline ), args );
                        if ( (rtn.ToString() == Boolean.TrueString) && (treeView.Nodes.Count > 0) )
                        {
                            return true;
                        }
                    }
                    else if ( parserPlugin.UpdateDocumentOutline( this.CurrentSyntaxEditor, treeView ) && (treeView.Nodes.Count > 0) )
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void RefreshIntellisense( SyntaxEditor editor )
        {
            List<OutlineNodeInfo> nodeInfos = new List<OutlineNodeInfo>();
            if ( editor.Document.Outlining.Mode == OutliningMode.Automatic )
            {
                CollectAutomaticOutliningNodeInfo( nodeInfos, editor.Document.Outlining.RootNode );
            }
            else if ( editor.Document.Outlining.Mode == OutliningMode.Manual )
            {
                CollectManualOutliningNodeInfo( nodeInfos, editor.Document.Outlining.RootNode );
            }

            editor.Document.Reparse();

            if ( editor.Document.Outlining.Mode == OutliningMode.Automatic )
            {
                RestoreAutomaticOutliningNodeState( nodeInfos, editor.Document.Outlining.RootNode );
            }
            else if ( editor.Document.Outlining.Mode == OutliningMode.Manual )
            {
                RestoreManualOutliningNodeInfo( nodeInfos, editor.Document );
                editor.RefreshOutlining();
            }
        }

        public void SetIntellisenseOptions( bool reparse )
        {
            foreach ( KeyValuePair<string, EditorInfo> pair in m_Editors )
            {
                if ( (pair.Value.Editor.Document.LanguageData != null)
                    && (pair.Value.Editor.Document.LanguageData is IParserProjectResolver) )
                {
                    OnIntellisenseSettingsChanged( pair.Value.Editor );

                    if ( (ApplicationSettings.Mode == ApplicationSettings.ApplicationMode.Editor) && reparse )
                    {
                        // trigger reparse so we clear the colorization
                        RefreshIntellisense( pair.Value.Editor );
                    }
                }
            }
        }

        public void SetupSearchCommandLinks( bool useNewDialog )
        {
            foreach ( EditorInfo eInfo in m_Editors.Values )
            {
                eInfo.CommandLinks.SetupSearchCommandLinks( eInfo.Editor, m_searchReplaceDialog, m_findReplaceForm, useNewDialog );
            }
        }

        private void OnIntellisenseSettingsChanged( ActiproSoftware.SyntaxEditor.SyntaxEditor editor )
        {
            if ( this.IntellisenseSettingsChanged != null )
            {
                this.IntellisenseSettingsChanged( this, new IntellisenseSettingsChangedEventArgs( editor, m_userEditorSettings.IntellisenseSettings ) );
            }
        }

        public void RefreshFileMenuItems()
        {
            bool generalSavingEnabled = CurrentCompileState == CompileState.Stopped &&
                ApplicationSettings.Mode != ApplicationSettings.ApplicationMode.Debugger;

            bool haveOpenFile = this.CurrentSyntaxEditor != null;
            bool canSaveFile = generalSavingEnabled &&
                haveOpenFile &&
                this.CurrentSyntaxEditor.Document.Modified;

            EnableMenuItem(AppAction.FileSave, canSaveFile);
            EnableMenuItem(AppAction.FileSaveAs, generalSavingEnabled && haveOpenFile);

            bool projectExplorerLoaded = m_projectExplorer != null && m_projectExplorer.IsLoaded;
            EnableMenuItem(AppAction.FileSaveAll, canSaveFile || (generalSavingEnabled && projectExplorerLoaded));

            EnableMenuItem(AppAction.FileSaveProject, CurrentCompileState == CompileState.Stopped && projectExplorerLoaded && m_projectExplorer.IsModified);
            EnableMenuItem(AppAction.FileOpenProject, CurrentCompileState == CompileState.Stopped);
            EnableMenuItem(AppAction.FileCloseProject, CurrentCompileState == CompileState.Stopped && projectExplorerLoaded);
        }

        public void RefreshCompilingMenuItems()
        {
            bool compiling = this.CurrentCompileState != CompileState.Stopped;
            String currentFilePath = CurrentSyntaxEditor?.Document?.Filename;

            bool canCompileFile = !compiling &&
                IsFileCompilable(currentFilePath);

            // Specifically allowing xml files to be "compiled for preview" by just being copied to preview folder.
            bool canCompileForPreview = canCompileFile ||
                (!compiling &&
                    currentFilePath != null &&
                    !IsANewFileName(currentFilePath) &&
                    Path.GetExtension(currentFilePath).Equals(".xml", StringComparison.OrdinalIgnoreCase));

            bool canCompileProject = !compiling &&
                (IsProjectCompilable() || AnyOpenFilesCompilable());

            EnableMenuItem(AppAction.IncredibuildBuild, canCompileProject);
            EnableMenuItem(AppAction.IncredibuildRebuild, canCompileProject);
            EnableMenuItem(AppAction.IncredibuildClean, canCompileProject);
            EnableMenuItem(AppAction.IncredibuildStop, this.CurrentCompileState == CompileState.BuildingIncredibuild);
            EnableMenuItem(AppAction.CompileFile, canCompileFile);
            EnableMenuItem(AppAction.CompileFileForPreview, canCompileForPreview);
            EnableMenuItem(AppAction.CompileProject, canCompileProject);
            EnableMenuItem(AppAction.RecompileProject, canCompileProject);
            EnableMenuItem(AppAction.StopCompile, this.CurrentCompileState == CompileState.Compiling);
        }

        public void EnableMenuItem( AppAction appAction, bool enable )
        {
            if ( GetMenuItemByTag != null )
            {
                ToolStripItem menuItem = GetMenuItemByTag( appAction );
                if ( menuItem != null )
                {
                    menuItem.Enabled = enable;
                }
            }

            if ( GetToolstripItemByTag != null )
            {
                ToolStripItem item = GetToolstripItemByTag( appAction );
                if ( item != null )
                {
                    item.Enabled = enable;
                }
            }

            if ( (this.CurrentSyntaxEditor != null) && (this.CurrentSyntaxEditor.ContextMenu != null) )
            {
                foreach ( MenuItem item in this.CurrentSyntaxEditor.ContextMenu.MenuItems )
                {
                    if ( item.Tag != null )
                    {
                        AppAction action = (AppAction)Enum.Parse( typeof( AppAction ), item.Tag.ToString() );
                        if ( action == appAction )
                        {
                            item.Enabled = enable;
                            break;
                        }
                    }
                    else
                    {
                        bool found = false;
                        foreach ( MenuItem subItem in item.MenuItems )
                        {
                            if ( subItem.Tag != null )
                            {
                                AppAction action = (AppAction)Enum.Parse( typeof( AppAction ), subItem.Tag.ToString() );
                                if ( action == appAction )
                                {
                                    subItem.Enabled = enable;

                                    found = true;
                                    break;
                                }
                            }
                        }

                        if ( found )
                        {
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adds a span indicator to the specified editor.
        /// </summary>
        /// <param name="editor">editor to add the span indicator to</param>
        /// <param name="layerKey">The key of the layer that will add the span indicator.</param>
        /// <param name="layerDisplayPriority">The display priority of the layer.</param>
        /// <param name="indicator">The <see cref="SpanIndicator"/> to add.</param>
        /// <param name="textRange">The text range over which to add the indicator.</param>
        private void AddSpanIndicator( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, string layerKey,
            int layerDisplayPriority, ActiproSoftware.SyntaxEditor.SpanIndicator indicator,
            ActiproSoftware.SyntaxEditor.TextRange textRange )
        {
            // Ensure there is a selection
            if ( textRange.AbsoluteLength == 0 )
            {
                rageMessageBox.ShowNone( m_parentControl, "Please make a selection first.", string.Empty );
                return;
            }

            // Ensure that a syntax error layer is created...
            SpanIndicatorLayer layer = editor.Document.SpanIndicatorLayers[layerKey];
            if (layer == null)
            {
                layer = new SpanIndicatorLayer( layerKey, layerDisplayPriority );
                editor.Document.SpanIndicatorLayers.Add( layer);
            }

            // Add the indicator (don't throw an exception when the text ranges overlap, not a problem for us).
            layer.Add(indicator, textRange, throwOnTextRangeException: false);
        }

        public void findReplace_InitSearchDocuments( ref SyntaxEditor currSyntaxEditor,
            ref Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openDocs, ref List<string> projectDocs )
        {
            currSyntaxEditor = this.CurrentSyntaxEditor;

            openDocs = m_TabPageToEditorMap;

            projectDocs = (m_projectExplorer != null) ? m_projectExplorer.GetProjectFilenames() : null;
        }

        private void searchReplaceDialog_CancelIntellisense( object sender, EventArgs e )
        {
            CancelActiveIntellisense();
        }

        private void searchReplaceDialog_FindCurrentFilename( object sender, SearchReplaceFilenameEventArgs e )
        {
            if ( this.CurrentSyntaxEditor != null )
            {
                e.Filename = this.CurrentSyntaxEditor.Document.Filename.ToLower();
            }
            else
            {
                e.Filename = null;
            }
        }

        private void searchReplaceDialog_FindOpenFilenames( object sender, SearchReplaceFilenamesEventArgs e )
        {
            e.Filenames = GetAllOpenTabFilenames();
        }

        private void searchReplaceDialog_FindProjectFilenames( object sender, SearchReplaceFilenamesEventArgs e )
        {
            e.Filenames = GetProjectAndOpenFilenames();
        }

        private void searchReplaceDialog_FindSyntaxEditor( object sender, SearchReplaceSyntaxEditorEventArgs e )
        {
            EditorInfo eInfo;
            if ( m_Editors.TryGetValue( e.Filename.ToLower(), out eInfo ) )
            {
                e.Editor = eInfo.Editor;
            }
            else
            {
                e.Editor = null;
            }
        }

        private void searchReplaceDialog_SearchBegin(object sender, SearchReplaceEventArgs e)
        {
            // VTL: This logic works in the presumed situation where we don't have other parallel tasks that are setting this during a search...
            m_performingMultiFileOperation = true;
        }

        private void searchReplaceDialog_SearchComplete(object sender, SearchCompleteEventArgs e)
        {
            // VTL: This logic works in the presumed situation where we don't have other parallel tasks that are setting this during a search...
            m_performingMultiFileOperation = false;
        }

        private void searchReplaceDialog_LoadFile( object sender, SearchReplaceLoadFileEventArgs e )
        {
            e.Success = LoadFile( e.Filename );
        }

        private void searchReplaceDialog_SaveDocument( object sender, SearchReplaceSaveTextEventArgs e )
        {
            e.Success = SaveFile( e.Text );
        }

        public void CancelActiveIntellisense()
        {
            // cancel intellisense if it is active
            if ( (this.CurrentSyntaxEditor != null) && this.CurrentSyntaxEditor.IntelliPrompt.MemberList.Visible )
            {
                this.CurrentSyntaxEditor.IntelliPrompt.MemberList.Abort();
                this.CurrentSyntaxEditor.IntelliPrompt.MemberList.Clear();
            }
        }

        public List<string> BuildScriptFileExtensionsList()
        {
            List<string> scriptExtensions = this.ScriptFileExtensions;

            List<string> asteriskExtensions = new List<string>();
            foreach ( string scriptExtension in scriptExtensions )
            {
                asteriskExtensions.Add( String.Format( "*{0}", scriptExtension.Replace( ";", ";*" ) ) );
            }

            StringBuilder allExtensions = new StringBuilder();
            if ( asteriskExtensions.Count > 0 )
            {
                allExtensions.Append( asteriskExtensions[0] );

                for ( int i = 1; i < asteriskExtensions.Count; ++i )
                {
                    allExtensions.Append( ';' );
                    allExtensions.Append( asteriskExtensions[i] );
                }
            }

            List<string> scriptFileTypes = new List<string>();
            scriptFileTypes.Add( "*.*" );
            scriptFileTypes.Add( allExtensions.ToString() );
            scriptFileTypes.AddRange( asteriskExtensions.ToArray() );

            return scriptFileTypes;
        }

        public string BuildScriptFileDialogFilter()
        {
            List<string> scriptExtensions = this.ScriptFileExtensions;

            List<string> asteriskExtensions = new List<string>();
            foreach ( string scriptExtension in scriptExtensions )
            {
                asteriskExtensions.Add( String.Format( "*{0}", scriptExtension.Replace( ";", ";*" ) ) );
            }

            StringBuilder allExtensions = new StringBuilder();
            if ( asteriskExtensions.Count > 0 )
            {
                allExtensions.Append( asteriskExtensions[0] );

                for ( int i = 1; i < asteriskExtensions.Count; ++i )
                {
                    allExtensions.Append( ';' );
                    allExtensions.Append( asteriskExtensions[i] );
                }
            }

            System.Text.StringBuilder filter = new System.Text.StringBuilder();

            if ( allExtensions.Length > 0 )
            {
                filter.Append( "Script Files (" );
                filter.Append( allExtensions.ToString() );
                filter.Append( ")|" );
                filter.Append( allExtensions.ToString() );
                filter.Append( "|" );
            }

            filter.Append( "Text Files (*.txt)|*.txt|" );
            filter.Append( "All Files (*.*)|*.*" );

            return filter.ToString();
        }

        public void ClearParseRequests( Document doc, string filename )
        {
#if OUTPUT_CLEAR_PARSE_REQUESTS
                Console.WriteLine( "Clearing Parse Requests for '{0}'", (doc != null) ? doc.Filename : filename );
#endif

            List<string> parseHashKeys = new List<string>();
            SemanticParserServiceRequest[] requests = SemanticParserService.GetPendingRequests;
            foreach ( SemanticParserServiceRequest request in requests )
            {
                if ( ((doc != null) && (request.SemanticParseDataTarget == doc))
                    || ((filename != null) && (request.Filename.ToLower() == filename)) )
                {
                    int count = SemanticParserService.RemovePendingRequests( request.ParseHashKey );
                    if ( count == 0 )
                    {
                        parseHashKeys.Add( request.ParseHashKey );
                    }
#if OUTPUT_CLEAR_PARSE_REQUESTS
                    else
                    {
                        Console.WriteLine( "\tRemoved {0} requests {1}, {2} remaining", count, request.ParseHashKey, 
                            SemanticParserService.PendingRequestCount );
                    }
#endif
                }
            }

            string key = string.Empty;
            if ( doc != null )
            {
                key = SemanticParserServiceRequest.GetParseHashKey( doc, doc );
            }

            foreach ( string parseHashKey in parseHashKeys )
            {
                if ( parseHashKey != key )
                {
#if OUTPUT_CLEAR_PARSE_REQUESTS
                    Console.WriteLine( "\tWaiting for parse we couldn't remove {0}", parseHashKey );
#endif
                    SemanticParserService.WaitForParse( parseHashKey );
                }
            }

            if ( (key != string.Empty) && !parseHashKeys.Contains( key ) )
            {
#if OUTPUT_CLEAR_PARSE_REQUESTS
                Console.WriteLine( "\tWaiting for parse in progress {0}", key );
#endif
                SemanticParserService.WaitForParse( key );
            }
        }

        public List<LanguageSettings> GetAllLanguageSettings()
        {
            List<LanguageSettings> settings = new List<LanguageSettings>();

            foreach ( KeyValuePair<string, IParserPlugin> pair in sm_parserPluginsByLanguage )
            {
                settings.Add( pair.Value.CurrentLanguageSettings );
            }

            return settings;
        }

        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static bool LoadLanguageSettings( XmlTextReader reader )
        {
            string parentName = reader.Name;

            while ( reader.Read() )
            {
                if ( !reader.IsStartElement() )
                {
                    if ( reader.Name == parentName )
                    {
                        break;	// we're done
                    }
                }
                else if ( reader.Name == "LanguageSettings" )
                {
                    LanguageTabSettings settings = new LanguageTabSettings( string.Empty, false );
                    if ( !reader.IsEmptyElement && !settings.LoadXml( reader ) )
                    {
                        return false;
                    }

                    IParserPlugin parserPlugin = null;
                    if ( sm_parserPluginsByLanguage.TryGetValue( settings.Language.ToLower(), out parserPlugin ) )
                    {
                        parserPlugin.CurrentLanguageSettings.Upgrade( settings );
                        parserPlugin.OnLanguageSettingsUpdated();
                    }
                }
            }

            return true;
        }
        #endregion

        #region AppAction
        /// <summary>
        /// Indicates a type of application action.
        /// </summary>
        /// <remarks>
        /// This is simply used to have all menu items and toolbars call the same centralized procedure.
        /// </remarks>
        public enum AppAction
        {
            FileNew,
            FileOpen,
            FileOpenProject,
            FileSave,
            FileSaveAll,
            FileSaveAs,
            FileSaveAsHtml,
            FileSaveAsRtf,
            FileSaveProject,
            FileClose,
            FileCloseAll,
            FileCloseProject,
            FilePrint,
            FilePageSetup,
            FilePrintPreview,
            EditCut,
            EditCutAndAppend,
            EditCopy,
            EditCopyAndAppend,
            EditPaste,
            EditDelete,
            EditUndo,
            EditRedo,
            EditIndent,
            EditOutdent,
            EditSelectAll,
            EditConvertTabsToSpaces,
            EditConvertSpacesToTabs,
            EditTabifySelectedLines,
            EditUntabifySelectedLines,
            EditCommentSelection,
            EditUncommentSelection,
            EditMakeLowercase,
            EditMakeUppercase,
            EditToggleCase,
            EditCapitalize,
            EditDeleteHorizontalWhitespace,
            EditTrimTrailingWhitespace,
            EditDeleteLine,
            EditDuplicateLine,
            EditDeleteBlankLines,
            EditTransposeLines,
            EditTransposeWords,
            EditTransposeCharacters,
            EditToggleBookmark,
            EditEnableBookmark,
            EditNextBookmark,
            EditPreviousBookmark,
            EditClearBookmarks,
            EditInsertFile,
            EditListMembers,
            EditParameterInfo,
            EditQuickInfo,
            EditCompleteWord,
            EditInsertCodeSnippet,
            OutliningHideSelection,
            OutliningToggleOutliningExpansion,
            OutliningToggleAllOutlining,
            OutliningStopOutlining,
            OutliningStopHidingCurrent,
            OutliningStartAutomaticOutlining,
            OutliningStartManualOutlining,
            SearchFindReplace,
            SearchAgainDown,
            SearchAgainUp,
            SearchHighlightedDown,
            SearchHighlightedUp,
            SearchIncremental,
            SearchReverseIncremental,
            SearchGoToLine,
            SearchGoToDefinition,
            SearchGoToProgramCounter,
            ToolsHighlightingStyleEditor,
            ToolsMacrosRecord,
            ToolsMacrosPause,
            ToolsMacrosStop,
            ToolsMacrosCancel,
            ToolsMacrosRun,
            ToolsTextStatistics,
            ToolsSpellCheck,
            WindowNoSplits,
            WindowSplitFourWay,
            WindowSplitHorizontally,
            WindowSplitVertically,
            DebugToggleBreakpoint,
            DebugToggleBreakpointAllThreads,
            DebugToggleBreakpointStopGame,
            DebugToggleBreakpointAllThreadsStopGame,
            DebugContinue,
            DebugPause,
            DebugStepInto,
            DebugStepOver,
            DebugStepOut,
            CancelIntellisense,
            RefreshIntellisense,
            CompilingConfigurations,
            CompileProject,
            RecompileProject,
            CompileFile,
            CompileFileForPreview,
            StopCompile,
            IncredibuildBuild,
            IncredibuildRebuild,
            IncredibuildClean,
            IncredibuildStop,
            GoForward,
            GoBack,
        }

        /// <summary>
        /// Execute an application action.
        /// </summary>
        /// <param name="action">
        /// An <c>AppAction</c> specifying the type of action to take.
        /// </param>
        /// <remarks>
        /// This procedure is a centralized place for handling menu and toolbar commands.
        /// </remarks>
        public void ExecuteAppAction( AppAction action )
        {
            // Based on the action passed, do something...
            switch ( action )
            {
                case AppAction.FileNew:
                    {
                        CreateNewFile();
                    }
                    break;
                case AppAction.FileOpen:
                    {
                        // Open a document
                        if (!m_openingFile)
                        {
                            m_openingFile = true;

                            openFileDialog.Filter = BuildScriptFileDialogFilter();
                            openFileDialog.Multiselect = true;

                            openFileDialog.InitialDirectory = ApplicationSettings.PrependProjectRoot(String.Empty);
                            if (this.CurrentSyntaxEditor != null)
                            {
                                String currentDir = Path.GetDirectoryName(this.CurrentSyntaxEditor.Document.Filename.Trim(Path.GetInvalidFileNameChars()));
                                if (!String.IsNullOrEmpty(currentDir))
                                {
                                    openFileDialog.InitialDirectory = currentDir;
                                }
                            }

                            if (openFileDialog.ShowDialog(m_parentControl) == DialogResult.Cancel)
                            {
                                m_openingFile = false;
                                break;
                            }

                            string[] filenames = openFileDialog.FileNames;
                            List<string> files = new List<string>();
                            foreach (string file in filenames)
                            {
                                files.Add(file);
                            }

                            if (LoadFiles(files) && (this.CurrentSyntaxEditor != null))
                            {
                                this.CurrentSyntaxEditor.Focus();
                            }

                            m_openingFile = false;
                        }
                    }
                    break;
                case AppAction.FileSave:
                    {
                        if ( (this.CurrentSyntaxEditor != null) && this.CurrentSyntaxEditor.Document.Modified )
                        {
                            SaveFile( this.CurrentSyntaxEditor );
                        }
                    }
                    break;
                case AppAction.FileSaveAll:
                    {
                        SaveAllFiles();
                    }
                    break;
                case AppAction.FileSaveAs:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            SaveFileAs( this.CurrentSyntaxEditor );
                        }
                    }
                    break;
                case AppAction.FileSaveAsHtml:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            // Export to HTML and view it
                            string filename = Path.GetDirectoryName( this.CurrentSyntaxEditor.Document.Filename ) + @"\export.html";
                            this.CurrentSyntaxEditor.Document.SaveFileAsHtml( filename, HtmlExportType.Inline, LineTerminator.CarriageReturnNewline );
                            System.Diagnostics.Process.Start( filename );
                        }
                    }
                    break;
                case AppAction.FileSaveAsRtf:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            // Export to RTF and view it
                            string filename = Path.GetDirectoryName( this.CurrentSyntaxEditor.Document.Filename ) + @"\export.rtf";
                            this.CurrentSyntaxEditor.Document.SaveFileAsRtf( filename, this.CurrentSyntaxEditor.Font, LineTerminator.CarriageReturnNewline );
                            System.Diagnostics.Process.Start( filename );
                        }
                    }
                    break;
                case AppAction.FileClose:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            EditorInfo eInfo;
                            if ( m_Editors.TryGetValue( this.CurrentSyntaxEditor.Document.Filename.ToLower(), out eInfo ) )
                            {
                                eInfo.TabPage.Close();
                            }
                        }
                    }
                    break;
                case AppAction.FileCloseAll:
                    {
                        this.CloseAllFiles();
                    }
                    break;
                case AppAction.FilePrint:
                    {
                        // Implement to use the DocumentTitle property to make the current filename appear at the top of each page
                        // this.CurrentSyntaxEditor.PrintSettings.DocumentTitle = "<filename>";  

                        // Show the print dialog and print
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.Print( true );
                        }
                    }
                    break;
                case AppAction.FilePageSetup:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            // Show the page setup
                            this.CurrentSyntaxEditor.ShowPageSetupForm( m_parentControl );
                        }
                    }
                    break;
                case AppAction.FilePrintPreview:
                    {
                        // Implement to use the DocumentTitle property to make the current filename appear at the top of each page
                        // this.CurrentSyntaxEditor.PrintSettings.DocumentTitle = "<filename>";  

                        // Show the print preview dialog
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.PrintPreview();
                        }
                    }
                    break;
                case AppAction.EditCut:
                    {
                        // Cut the currently selected text
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.CutToClipboard();
                        }
                    }
                    break;
                case AppAction.EditCutAndAppend:
                    {
                        // Cut and append the currently selected text
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.CutAndAppendToClipboard();
                        }
                    }
                    break;
                case AppAction.EditCopy:
                    {
                        // Copy the currently selected text
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.CopyToClipboard();
                        }
                    }
                    break;
                case AppAction.EditCopyAndAppend:
                    {
                        // Copy and append the currently selected text
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.CopyAndAppendToClipboard();
                        }
                    }
                    break;
                case AppAction.EditPaste:
                    {
                        // Paste text from the clipboard
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.PasteFromClipboard();
                        }
                    }
                    break;
                case AppAction.EditDelete:
                    {
                        // Delete the currently selected text
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.Delete();
                        }
                    }
                    break;
                case AppAction.EditUndo:
                    {
                        // Perform an undo action
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.Document.UndoRedo.Undo();
                        }
                    }
                    break;
                case AppAction.EditRedo:
                    {
                        // Perform a redo action
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.Document.UndoRedo.Redo();
                        }
                    }
                    break;
                case AppAction.EditIndent:
                    {
                        // Indent the currently selected text
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.Indent();
                        }
                    }
                    break;
                case AppAction.EditOutdent:
                    {
                        // Outdent the currently selected text
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.Outdent();
                        }
                    }
                    break;
                case AppAction.EditSelectAll:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.Selection.SelectAll();
                        }
                    }
                    break;
                case AppAction.EditConvertTabsToSpaces:
                    {
                        // Convert tabs to spaces
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.ConvertTabsToSpaces();
                        }
                    }
                    break;
                case AppAction.EditConvertSpacesToTabs:
                    {
                        // Convert spaces to tabs
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.ConvertSpacesToTabs();
                        }
                    }
                    break;
                case AppAction.EditTabifySelectedLines:
                    {
                        // Tabify selected lines
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.TabifySelectedLines();
                        }
                    }
                    break;
                case AppAction.EditUntabifySelectedLines:
                    {
                        // Untabify selected lines
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.UntabifySelectedLines();
                        }
                    }
                    break;
                case AppAction.EditCommentSelection:
                    {
                        // Comment the currently selected text
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.CommentLines();
                        }
                    }
                    break;
                case AppAction.EditUncommentSelection:
                    {
                        // Uncomment the currently selected text
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.UncommentLines();
                        }
                    }
                    break;
                case AppAction.EditMakeLowercase:
                    {
                        // Make lowercase
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.ChangeCharacterCasing( CharacterCasing.Lower );
                        }
                    }
                    break;
                case AppAction.EditMakeUppercase:
                    {
                        // Make uppercase
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.ChangeCharacterCasing( CharacterCasing.Upper );
                        }
                    }
                    break;
                case AppAction.EditToggleCase:
                    {
                        // Toggle the character casing
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.ToggleCharacterCasing();
                        }
                    }
                    break;
                case AppAction.EditCapitalize:
                    {
                        // Capitalize words
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.ChangeCharacterCasing( CharacterCasing.Normal );
                        }
                    }
                    break;
                case AppAction.EditDeleteHorizontalWhitespace:
                    {
                        // Delete the horizontal whitespace
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.DeleteHorizontalWhitespace();
                        }
                    }
                    break;
                case AppAction.EditTrimTrailingWhitespace:
                    {
                        // Trim the trailing whitespace 
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.TrimTrailingWhitespace();
                        }
                    }
                    break;
                case AppAction.EditDeleteLine:
                    {
                        // Delete the current line
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.DeleteLine();
                        }
                    }
                    break;
                case AppAction.EditDuplicateLine:
                    {
                        // Duplicate the current line
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.DuplicateLine();
                        }
                    }
                    break;
                case AppAction.EditDeleteBlankLines:
                    {
                        // Delete blank lines
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.DeleteBlankLines();
                        }
                    }
                    break;
                case AppAction.EditTransposeLines:
                    {
                        // Transpose lines
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.TransposeLines();
                        }
                    }
                    break;
                case AppAction.EditTransposeWords:
                    {
                        // Transpose words
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.TransposeWords();
                        }
                    }
                    break;
                case AppAction.EditTransposeCharacters:
                    {
                        // Transpose characters
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.TransposeCharacters();
                        }
                    }
                    break;
                case AppAction.EditToggleBookmark:
                    {
                        // Toggle the bookmark at the current line
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            if ( this.CurrentSyntaxEditor.SelectedView.CurrentDocumentLine.LineIndicators.Contains( BookmarkLineIndicator.DefaultName ) )
                            {
                                // A bookmark is already on the line... remove all bookmarks from the line
                                this.CurrentSyntaxEditor.SelectedView.CurrentDocumentLine.LineIndicators.Clear( BookmarkLineIndicator.DefaultName );
                            }
                            else
                            {
                                // Add a bookmark to the line
                                this.CurrentSyntaxEditor.Document.LineIndicators.Add( new BookmarkLineIndicator(),
                                    this.CurrentSyntaxEditor.Caret.DocumentPosition.Line );
                            }
                        }
                    }
                    break;
                case AppAction.EditEnableBookmark:
                    {
                        // Toggle the enabled state of the bookmark
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            int index = this.CurrentSyntaxEditor.SelectedView.CurrentDocumentLine.LineIndicators.IndexOf( BookmarkLineIndicator.DefaultName );
                            if ( index != -1 )
                            {
                                ((BookmarkLineIndicator)this.CurrentSyntaxEditor.SelectedView.CurrentDocumentLine.LineIndicators[index]).CanNavigateTo =
                                    !((BookmarkLineIndicator)this.CurrentSyntaxEditor.SelectedView.CurrentDocumentLine.LineIndicators[index]).CanNavigateTo;
                                this.CurrentSyntaxEditor.Invalidate();
                            }
                        }
                    }
                    break;
                case AppAction.EditNextBookmark:
                    {
                        // Move to the next bookmark
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.GotoNextLineIndicator( ActiproSoftware.SyntaxEditor.BookmarkLineIndicator.DefaultName );
                        }
                    }
                    break;

                case AppAction.EditPreviousBookmark:
                    {
                        // Move to the previous bookmark
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.GotoPreviousLineIndicator( ActiproSoftware.SyntaxEditor.BookmarkLineIndicator.DefaultName );
                        }
                    }
                    break;
                case AppAction.EditClearBookmarks:
                    {
                        // Clear all bookmarks
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.Document.LineIndicators.Clear( ActiproSoftware.SyntaxEditor.BookmarkLineIndicator.DefaultName );
                        }
                    }
                    break;
                case AppAction.EditInsertFile:
                    {
                        // Show the open file dialog
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            openFileDialog.Filter = "Text Documents (*.txt)|*.txt|All Documents (*.*)|*.*";
                            if ( openFileDialog.ShowDialog( m_parentControl ) == DialogResult.Cancel ) break;
                            {
                                this.CurrentSyntaxEditor.SelectedView.InsertFile( openFileDialog.FileName );
                            }
                        }
                    }
                    break;
                case AppAction.EditListMembers:
                    {
                        // Show a member list
                        if ( (this.CurrentSyntaxEditor != null)
                            && this.CurrentSyntaxEditor.SelectedView.GetCurrentLanguageForContext().IntelliPromptMemberListSupported )
                        {
                            this.CurrentSyntaxEditor.SelectedView.GetCurrentLanguageForContext().ShowIntelliPromptMemberList( this.CurrentSyntaxEditor );
                        }
                    }
                    break;
                case AppAction.EditParameterInfo:
                    {
                        // Show parameter info
                        if ( (this.CurrentSyntaxEditor != null)
                            && this.CurrentSyntaxEditor.SelectedView.GetCurrentLanguageForContext().IntelliPromptParameterInfoSupported )
                        {
                            this.CurrentSyntaxEditor.SelectedView.GetCurrentLanguageForContext().ShowIntelliPromptParameterInfo( this.CurrentSyntaxEditor );
                        }
                    }
                    break;
                case AppAction.EditQuickInfo:
                    {
                        // Show quick info
                        if ( (this.CurrentSyntaxEditor != null)
                            && this.CurrentSyntaxEditor.SelectedView.GetCurrentLanguageForContext().IntelliPromptQuickInfoSupported )
                        {
                            this.CurrentSyntaxEditor.SelectedView.GetCurrentLanguageForContext().ShowIntelliPromptQuickInfo( this.CurrentSyntaxEditor );
                        }
                    }
                    break;
                case AppAction.EditCompleteWord:
                    {
                        // Show a member list
                        if ( (this.CurrentSyntaxEditor != null)
                            && this.CurrentSyntaxEditor.SelectedView.GetCurrentLanguageForContext().IntelliPromptMemberListSupported )
                        {
                            this.CurrentSyntaxEditor.SelectedView.GetCurrentLanguageForContext().IntelliPromptCompleteWord( this.CurrentSyntaxEditor );
                        }
                    }
                    break;
                case AppAction.EditInsertCodeSnippet:
                    {
                        LanguageSettings languageSettings = this.CurrentLanguageSettings;
                        if ( languageSettings != null )
                        {
                            ActiproSoftware.SyntaxEditor.CodeSnippetFolder snippetFolder = languageSettings.CodeSnippetFolder;
                            if ( snippetFolder != null )
                            {
                                this.CurrentSyntaxEditor.IntelliPrompt.CodeSnippets.ShowInsertSnippetPopup(
                                    this.CurrentSyntaxEditor.Caret.Offset, "Insert Snippet:", snippetFolder, CodeSnippetTypes.Expansion );
                            }
                        }
                    }
                    break;
                case AppAction.OutliningHideSelection:
                    {
                        // Hide the current selection in an outlining node
                        if ( (this.CurrentSyntaxEditor != null)
                            && (this.CurrentSyntaxEditor.Document.Outlining.Mode == OutliningMode.Manual) )
                        {
                            OutliningNode node = this.CurrentSyntaxEditor.SelectedView.CreateOutliningNodeFromSelection( null );
                            if ( node != null )
                            {
                                this.CurrentSyntaxEditor.SelectedView.Selection.StartOffset = node.EndOffset + 1;
                                node.Expanded = false;
                            }
                        }
                    }
                    break;
                case AppAction.OutliningToggleOutliningExpansion:
                    {
                        // Toggle outlining expansion
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            OutliningNode node = this.CurrentSyntaxEditor.Document.Outlining.RootNode.FindNodeRecursive( this.CurrentSyntaxEditor.Caret.Offset );
                            if ( node != null )
                            {
                                node.ToggleExpansion();
                            }
                            else if ( this.CurrentSyntaxEditor.Caret.Offset > 0 )
                            {
                                // Since no node was found at the caret's offset, check to see if there is a node ending right before it
                                node = this.CurrentSyntaxEditor.Document.Outlining.RootNode.FindNodeRecursive( this.CurrentSyntaxEditor.Caret.Offset - 1 );
                                if ( node != null )
                                {
                                    node.ToggleExpansion();
                                }
                            }
                        }
                    }
                    break;
                case AppAction.OutliningToggleAllOutlining:
                    {
                        // Toggle all outliing
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            bool collapse = true;
                            if ( this.CurrentSyntaxEditor.Document.Outlining.RootNode.Count > 0 )
                            {
                                collapse = this.CurrentSyntaxEditor.Document.Outlining.RootNode[0].Expanded;
                            }

                            OutliningNode node = this.CurrentSyntaxEditor.Document.Outlining.RootNode.FindNodeRecursive( this.CurrentSyntaxEditor.Caret.Offset );
                            if ( node != null )
                            {
                                collapse = node.Expanded;
                            }
                            else if ( this.CurrentSyntaxEditor.Caret.Offset > 0 )
                            {
                                // Since no node was found at the caret's offset, check to see if there is a node ending right before it
                                node = this.CurrentSyntaxEditor.Document.Outlining.RootNode.FindNodeRecursive( this.CurrentSyntaxEditor.Caret.Offset - 1 );
                                if ( node != null )
                                {
                                    collapse = node.Expanded;
                                }
                            }

                            if ( collapse )
                            {
                                this.CurrentSyntaxEditor.Document.Outlining.RootNode.CollapseDescendants();
                            }
                            else
                            {
                                this.CurrentSyntaxEditor.Document.Outlining.RootNode.ExpandDescendants();
                            }
                        }
                    }
                    break;
                case AppAction.OutliningStopOutlining:
                    {
                        // Clear the outlining hierarchy and turn outlining off
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.Document.Outlining.Clear();
                            this.CurrentSyntaxEditor.Document.Outlining.Mode = OutliningMode.None;
                            this.CurrentSyntaxEditor.RefreshOutlining();
                        }
                    }
                    break;
                case AppAction.OutliningStopHidingCurrent:
                    {
                        // Remove the current outlining node
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            OutliningNode node = this.CurrentSyntaxEditor.Document.Outlining.RootNode.FindNodeRecursive( this.CurrentSyntaxEditor.Caret.Offset );
                            if ( node != null )
                            {
                                TextRange refreshTextRange = new TextRange( node.StartOffset, node.EndOffset );
                                this.CurrentSyntaxEditor.Document.Outlining.Remove( node, false );
                                this.CurrentSyntaxEditor.RefreshOutlining( refreshTextRange );
                            }
                        }
                    }
                    break;
                case AppAction.OutliningStartAutomaticOutlining:
                    {
                        // Switch to automatic mode
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.Document.Outlining.Mode = OutliningMode.Automatic;
                        }
                    }
                    break;
                case AppAction.OutliningStartManualOutlining:
                    {
                        // Switch to manual mode
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.Document.Outlining.Mode = OutliningMode.Manual;
                            this.CurrentSyntaxEditor.RefreshOutlining();
                        }
                    }
                    break;
                case AppAction.SearchFindReplace:
                    {
                        ActiproSoftware.WinUICore.Commands.CommandLink cmdLink = null;
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            EditorInfo eInfo;
                            if ( m_Editors.TryGetValue( this.CurrentSyntaxEditor.Document.Filename.ToLower(), out eInfo ) )
                            {
                                cmdLink = eInfo.CommandLinks.ControlFCommandLink;
                            }
                        }

                        if ( cmdLink != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( cmdLink.Command as ActiproSoftware.SyntaxEditor.Commands.EditCommand );
                        }
                        else
                        {
                            if ( m_userEditorSettings.FindReplaceSettings.UseNewDialog )
                            {
                                SearchReplaceCommand command = new SearchReplaceCommand( m_searchReplaceDialog );
                                command.Execute( null );
                            }
                            else
                            {
                                FindReplaceCommand command = new FindReplaceCommand( m_findReplaceForm );
                                command.Execute( null );
                            }
                        }
                    }
                    break;
                case AppAction.SearchAgainDown:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            EditorInfo eInfo;
                            if ( m_Editors.TryGetValue( this.CurrentSyntaxEditor.Document.Filename.ToLower(), out eInfo ) )
                            {
                                this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( 
                                    eInfo.CommandLinks.F3CommandLink.Command as ActiproSoftware.SyntaxEditor.Commands.EditCommand );
                            }
                        }
                    }
                    break;
                case AppAction.SearchAgainUp:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            EditorInfo eInfo;
                            if ( m_Editors.TryGetValue( this.CurrentSyntaxEditor.Document.Filename.ToLower(), out eInfo ) )
                            {
                                this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand(
                                    eInfo.CommandLinks.ShiftF3CommandLink.Command as ActiproSoftware.SyntaxEditor.Commands.EditCommand );
                            }
                        }
                    }
                    break;
                case AppAction.SearchHighlightedDown:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            EditorInfo eInfo;
                            if ( m_Editors.TryGetValue( this.CurrentSyntaxEditor.Document.Filename.ToLower(), out eInfo ) )
                            {
                                this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand(
                                    eInfo.CommandLinks.ControlF3CommandLink.Command as ActiproSoftware.SyntaxEditor.Commands.EditCommand );
                            }
                        }
                    }
                    break;
                case AppAction.SearchHighlightedUp:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            EditorInfo eInfo;
                            if ( m_Editors.TryGetValue( this.CurrentSyntaxEditor.Document.Filename.ToLower(), out eInfo ) )
                            {
                                this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand(
                                    eInfo.CommandLinks.ControlShiftF3CommandLink.Command as ActiproSoftware.SyntaxEditor.Commands.EditCommand );
                            }
                        }
                    }
                    break;
                case AppAction.SearchIncremental:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.FindReplace.IncrementalSearch.SearchUp = true;

                            if ( !this.CurrentSyntaxEditor.SelectedView.FindReplace.IncrementalSearch.Active )
                            {
                                // Start incremental search
                                this.CurrentSyntaxEditor.SelectedView.FindReplace.IncrementalSearch.Active = true;
                            }
                            else
                            {
                                // Perform another incremental search
                                this.CurrentSyntaxEditor.SelectedView.FindReplace.IncrementalSearch.PerformSearch();
                            }
                        }
                    }
                    break;
                case AppAction.SearchReverseIncremental:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.FindReplace.IncrementalSearch.SearchUp = false;

                            if ( !this.CurrentSyntaxEditor.SelectedView.FindReplace.IncrementalSearch.Active )
                            {
                                // Start incremental search
                                this.CurrentSyntaxEditor.SelectedView.FindReplace.IncrementalSearch.Active = true;
                            }
                            else
                            {
                                // Perform another incremental search
                                this.CurrentSyntaxEditor.SelectedView.FindReplace.IncrementalSearch.PerformSearch();
                            }
                        }
                    }
                    break;
                case AppAction.SearchGoToLine:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( new GoToLineCommand( this ) );
                        }
                    }
                    break;
                case AppAction.SearchGoToDefinition:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( new GetImplementationInformationCommand( this ) );
                        }
                    }
                    break;
                case AppAction.SearchGoToProgramCounter:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand(new GoToProgramCounterCommand(this));
                        }
                    }
                    break;
                case AppAction.ToolsHighlightingStyleEditor:
#if CRAP
				{
					// Show the style this.CurrentScriptEditor
					StylesyntaxEditor1Form form = new StylesyntaxEditor1Form(this.CurrentScriptEditor);
					form.ShowDialog( m_parentControl );
				}
				break;
#endif
                case AppAction.ToolsMacrosRecord:
                    {
                        if ( (this.CurrentSyntaxEditor != null) && (this.CurrentSyntaxEditor.MacroRecording.State == MacroRecordingState.Stopped) )
                        {
                            this.CurrentSyntaxEditor.MacroRecording.Record();
                        }
                    }
                    break;
                case AppAction.ToolsMacrosPause:
                    {
                        if ( (this.CurrentSyntaxEditor != null) && (this.CurrentSyntaxEditor.MacroRecording.State != MacroRecordingState.Stopped) )
                        {
                            if ( this.CurrentSyntaxEditor.MacroRecording.State == MacroRecordingState.Paused )
                            {
                                this.CurrentSyntaxEditor.MacroRecording.Resume();
                            }
                            else
                            {
                                this.CurrentSyntaxEditor.MacroRecording.Pause();
                            }
                        }
                    }
                    break;
                case AppAction.ToolsMacrosStop:
                    {
                        if ( (this.CurrentSyntaxEditor != null) && (this.CurrentSyntaxEditor.MacroRecording.State != MacroRecordingState.Stopped) )
                        {
                            this.CurrentSyntaxEditor.MacroRecording.Stop();
                            m_lastMacroCommand = this.CurrentSyntaxEditor.MacroRecording.LastMacroCommand;
                        }
                    }
                    break;
                case AppAction.ToolsMacrosCancel:
                    {
                        if ( (this.CurrentSyntaxEditor != null) && (this.CurrentSyntaxEditor.MacroRecording.State != MacroRecordingState.Stopped) )
                        {
                            this.CurrentSyntaxEditor.MacroRecording.Cancel();
                        }
                    }
                    break;
                case AppAction.ToolsMacrosRun:
                    {
                        if ( (this.CurrentSyntaxEditor != null) && (this.CurrentSyntaxEditor.MacroRecording.State == MacroRecordingState.Stopped) )
                        {
                            if ( m_lastMacroCommand != null )
                            {
                                this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( m_lastMacroCommand );
                            }
                            else
                            {
                                this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( new RunMacroCommand() );
                            }
                        }
                    }
                    break;
                case AppAction.ToolsTextStatistics:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            // Show the text statistics
                            if ( this.CurrentSyntaxEditor.SelectedView.Selection.Length > 0 )
                            {
                                this.CurrentSyntaxEditor.SelectedView.GetTextStatisticsForSelectedText().Show( m_parentControl, "Selected Text Statistics" );
                            }
                            else
                            {
                                this.CurrentSyntaxEditor.Document.GetTextStatistics().Show( m_parentControl, "Document Statistics" );
                            }
                        }
                    }
                    break;
                case AppAction.ToolsSpellCheck:
                    {
                        // FIXME: new SpellCheckForm( editor ).CheckSpelling();
                    }
                    break;
                case AppAction.WindowSplitHorizontally:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SplitType = SyntaxEditorSplitType.DualHorizontal;

                            if ( !this.CurrentSyntaxEditor.HasHorizontalSplit )
                            {
                                this.CurrentSyntaxEditor.HorizontalSplitPosition = (int)(this.CurrentSyntaxEditor.ClientRectangle.Height / 2); ;
                            }

                            if ( this.CurrentSyntaxEditor.HasVerticalSplit )
                            {
                                this.CurrentSyntaxEditor.VerticalSplitPosition = 0;
                            }
                        }
                    }
                    break;
                case AppAction.WindowSplitVertically:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SplitType = SyntaxEditorSplitType.DualVertical;

                            if ( this.CurrentSyntaxEditor.HasHorizontalSplit )
                            {
                                this.CurrentSyntaxEditor.HorizontalSplitPosition = 0;
                            }

                            if ( !this.CurrentSyntaxEditor.HasVerticalSplit )
                            {
                                this.CurrentSyntaxEditor.VerticalSplitPosition = (int)(this.CurrentSyntaxEditor.ClientRectangle.Width / 2); ;
                            }
                        }
                    }
                    break;
                case AppAction.WindowSplitFourWay:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SplitType = SyntaxEditorSplitType.FourWay;

                            if ( !this.CurrentSyntaxEditor.HasHorizontalSplit )
                            {
                                this.CurrentSyntaxEditor.HorizontalSplitPosition = (int)(this.CurrentSyntaxEditor.ClientRectangle.Height / 2); ;
                            }

                            if ( !this.CurrentSyntaxEditor.HasVerticalSplit )
                            {
                                this.CurrentSyntaxEditor.VerticalSplitPosition = (int)(this.CurrentSyntaxEditor.ClientRectangle.Width / 2); ;
                            }
                        }
                    }
                    break;
                case AppAction.WindowNoSplits:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SplitType = SyntaxEditorSplitType.None;

                            if ( this.CurrentSyntaxEditor.HasHorizontalSplit )
                            {
                                this.CurrentSyntaxEditor.HorizontalSplitPosition = 0;
                            }

                            if ( this.CurrentSyntaxEditor.HasVerticalSplit )
                            {
                                this.CurrentSyntaxEditor.VerticalSplitPosition = 0;
                            }
                        }
                    }
                    break;
                case AppAction.DebugToggleBreakpoint:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( new DebugBreakPointCommand( this ) );
                        }
                    }
                    break;
                case AppAction.DebugToggleBreakpointAllThreads:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( new DebugBreakPointAllThreadsCommand( this ) );
                        }
                    }
                    break;
                case AppAction.DebugToggleBreakpointStopGame:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( new DebugBreakPointStopGameCommand( this ) );
                        }
                    }
                    break;
                case AppAction.DebugToggleBreakpointAllThreadsStopGame:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( new DebugBreakPointAllThreadsStopGameCommand( this ) );
                        }
                    }
                    break;
                case AppAction.DebugContinue:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( new DebugContinueCommand( this ) );
                        }
                    }
                    break;
                case AppAction.DebugPause:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( new DebugPauseCommand( this ) );
                        }
                    }
                    break;
                case AppAction.DebugStepInto:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( new DebugStepIntoCommand( this ) );
                        }
                    }
                    break;
                case AppAction.DebugStepOver:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( new DebugStepOverCommand( this ) );
                        }
                    }
                    break;
                case AppAction.DebugStepOut:
                    {
                        if ( this.CurrentSyntaxEditor != null )
                        {
                            this.CurrentSyntaxEditor.SelectedView.RaiseEditCommand( new DebugStepOutCommand( this ) );
                        }
                    }
                    break;
                case AppAction.CancelIntellisense:
                    {
                        CancelActiveIntellisense();
                    }
                    break;
                case AppAction.RefreshIntellisense:
                    {
                        foreach ( IParserPlugin parserPlugin in sm_parserPlugins )
                        {
                            foreach ( IParserProjectResolver projectResolver in parserPlugin.ProjectResolvers )
                            {
                                projectResolver.RefreshIntellisense();
                            }
                        }

                        foreach ( KeyValuePair<string, EditorInfo> pair in m_Editors )
                        {
                            RefreshIntellisense( pair.Value.Editor );
                        }
                    }
                    break;
                case AppAction.GoForward:
                    {
                        NavigationPosition pos = this.m_navManager.NavigateForward();
                        if (pos == null)
                        {
                            return;
                        }

                        using (new NavigationScope(this.m_navManager))
                        {
                            this.SelectOffset(pos.Filename, pos.Offset, pos.Offset);
                        }

                        break;
                    }
                case AppAction.GoBack:
                {
                        NavigationPosition currentPos = this.CurrentSyntaxEditor != null ? new NavigationPosition(this.CurrentSyntaxEditor) : null;
                        NavigationPosition pos = this.m_navManager.NavigateBack(currentPos);
                        if (pos == null)
                        {
                            return;
                        }

                        using (new NavigationScope(this.m_navManager))
                        {
                            this.SelectOffset(pos.Filename, pos.Offset, pos.Offset);
                        }

                        break;
                    }
                default:
                    {

                    }
                    break;
            }
        }
        #endregion

        #region Compiling
        public void AddCompileErrorIndicator( string fileName, int lineNum, bool forceOpen )
        {
            if ( lineNum >= 0 )
            {
                string lowercaseName = fileName.ToLower();
                EditorInfo eInfo = null;
                m_Editors.TryGetValue( lowercaseName, out eInfo );

                // if the file's not there, load it:
                if ( (eInfo == null) && forceOpen )
                {
                    if ( LoadFile( fileName ) )
                    {
                        eInfo = m_Editors[lowercaseName];
                    }
                }

                // now try to add the error indicator
                if ( (eInfo != null) && (lineNum < eInfo.Editor.Document.Lines.Count) )
                {
                    DocumentLine docLine = eInfo.Editor.Document.Lines[lineNum];
                    ActiproSoftware.SyntaxEditor.TextRange tRange = docLine.TextRange;
                    if ( tRange.AbsoluteLength > 0 )
                    {
                        AddSpanIndicator( eInfo.Editor, c_compileErrorKey, c_compileErrorDisplayPriority,
                            new ActiproSoftware.SyntaxEditor.WaveLineSpanIndicator( Color.Blue ), tRange );
                    }
                }
            }
        }

        public bool SelectOffset( string filename, int startOffset, int endOffset )
        {
            string lowercaseName = filename.ToLower();

            using (NavigationScope navScope = this.CurrentSyntaxEditor != null ?
                new NavigationScope(this.m_navManager, new NavigationPosition(this.CurrentSyntaxEditor)) : null)
            {
                // if the file's not there, load it:
                EditorInfo eInfo = null;
                if (!m_Editors.TryGetValue(lowercaseName, out eInfo))
                {
                    if (File.Exists(filename) && LoadFile(filename))
                    {
                        eInfo = m_Editors[lowercaseName];
                    }
                }

                if (eInfo != null)
                {
                    if (startOffset < eInfo.Editor.Document.Length)
                    {
                        DocumentLine line = null;
                        if (startOffset > -1)
                        {
                            TextStream stream = eInfo.Editor.Document.GetTextStream(startOffset);
                            line = stream.DocumentLine;
                        }

                        // semi hack - we check if it's a dock control so that way we can make sure that it has focus:
                        TD.SandDock.DockControl dockControl = eInfo.TabPage as TD.SandDock.DockControl;
                        if (dockControl != null)
                        {
                            dockControl.Open();
                        }

                        if (line != null)
                        {
                            eInfo.Editor.SelectedView.Selection.StartOffset = (endOffset == -1) ? line.EndOffset : (endOffset < eInfo.Editor.Document.Length) ? endOffset : eInfo.Editor.Document.Length;
                            eInfo.Editor.SelectedView.Selection.EndOffset = startOffset;
                        }

                        eInfo.TabPage.Show();
                        eInfo.TabPage.BringToFront();
                        eInfo.TabPage.Focus();

                        return true;
                    }
                    else
                    {
                        rageMessageBox.ShowError(m_parentControl,
                            String.Format("StartOffset {0} in '{1}' doesn't exist!", startOffset, filename), "StartOffset Doesn't Exist");
                    }
                }

                navScope?.Cancel();
                return false;
            }
        }

        public Control SelectLine( string filename, int lineNum, List<CompileErrorInfo> errors )
        {
            string lowercaseName = filename.ToLower();

            using (NavigationScope navScope = this.CurrentSyntaxEditor != null ?
                new NavigationScope(this.m_navManager, new NavigationPosition(this.CurrentSyntaxEditor)) : null)
            {
                // if the file's not there, load it:
                EditorInfo eInfo = null;
                if (!m_Editors.TryGetValue(lowercaseName, out eInfo))
                {
                    if (File.Exists(filename) && LoadFile(filename))
                    {
                        eInfo = m_Editors[lowercaseName];
                    }

                    // add all compile error indicators
                    if (errors != null)
                    {
                        foreach (CompileErrorInfo error in errors)
                        {
                            AddCompileErrorIndicator(error.FileName, error.LineNumber, false);
                        }
                    }
                }

                if (eInfo == null)
                {
                    navScope?.Cancel();
                    return null;
                }

                // semi hack - we check if it's a dock control so that way we can make sure that it has focus:
                TD.SandDock.DockControl dockControl = eInfo.TabPage as TD.SandDock.DockControl;
                if (dockControl != null)
                {
                    dockControl.Open();
                }

                if (lineNum >= 0 && lineNum < eInfo.Editor.Document.Lines.Count)
                {
                    DocumentLine line = eInfo.Editor.Document.Lines[lineNum];
                    eInfo.Editor.SelectedView.Selection.StartOffset = line.EndOffset;
                    eInfo.Editor.SelectedView.Selection.EndOffset = line.StartOffset;
                }
                else
                {
                    ApplicationSettings.Log.Warning($"Cannot seek to line {lineNum + 1} of {filename} as it's out of range of the document.");
                }

                eInfo.TabPage.Show();
                eInfo.TabPage.BringToFront();
                eInfo.TabPage.Focus();

                return eInfo.TabPage;
            }
        }

        public void RemoveAllCompileErrorIndicators()
        {
            foreach ( KeyValuePair<string, EditorInfo> pair in m_Editors )
            {
                SpanIndicatorLayer layer = pair.Value.Editor.Document.SpanIndicatorLayers[c_compileErrorKey];
                if ( layer != null )
                {
                    layer.Clear();
                }
            }
        }

        /// <summary>
        /// Go to a specific file/line specified by the provided program counter for the filename.
        /// This won't necessarily be in the file specified--might be in a header.
        /// </summary>
        /// <param name="programCounter"></param>
        public void GoToScriptCounter(string filename, int programCounter)
        {
            string scriptName = Path.GetFileNameWithoutExtension(filename);
            string outputDir = ApplicationSettings.GetAbsolutePath(m_projectExplorer.ProjectEditorSetting.CurrentCompilingSettings.OutputDirectory);
            string scdPath = Path.Combine(outputDir, Path.ChangeExtension(scriptName, ".scd"));

            ScriptLineInfo lineInfo = ScriptLineInfo.Load(ApplicationSettings.Log, scdPath, programCounter);
            if (lineInfo != null)
            {
                SelectLine(lineInfo.Filename, lineInfo.LineNumber, errors: null);
            }
        }

        /// <summary>
        /// Get the full list of script names that currently contain debug information, based on output scd files.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<String> GetScriptNamesWithDebugInfo()
        {
            string outputDir = ApplicationSettings.GetAbsolutePath(m_projectExplorer.ProjectEditorSetting.CurrentCompilingSettings.OutputDirectory);
            if (!Directory.Exists(outputDir))
            {
                return Enumerable.Empty<String>();
            }

            return Directory.GetFiles(outputDir, "*.scd").Select(p => Path.GetFileNameWithoutExtension(p));
        }

        #endregion

        #region Debugger
        public void RequestBreakpoint( string filename, int line )
        {
            bool enabled = false;
            bool breakOnAllThreads = false;
            bool breakStopsGame = false;

            SpanIndicator indicator = GetSpanIndicator( filename, line, SpanIndicatorLayer.BreakpointDisplayPriority );
            if ( indicator != null )
            {
                enabled = true;

                BreakpointSpanIndicator breakpoint = indicator as BreakpointSpanIndicator;
                breakOnAllThreads = breakpoint.ForeColor == Color.Yellow;
                breakStopsGame = breakpoint.BackColor == Color.Red;
            }

            if ( enabled )
            {
                if ( breakOnAllThreads || breakStopsGame )
                {
                    // change
                    breakOnAllThreads = false;
                    breakStopsGame = false;
                }
                else
                {
                    // remove
                    enabled = false;
                }
            }
            else
            {
                // add
                enabled = true;
            }

            OnToggleBreakpoint( filename, line, enabled, breakOnAllThreads, breakStopsGame );
        }

        public void RequestBreakpointAllThreads( string filename, int line )
        {
            bool enabled = false;
            bool breakOnAllThreads = false;
            bool breakStopsGame = false;

            SpanIndicator indicator = GetSpanIndicator( filename, line, SpanIndicatorLayer.BreakpointDisplayPriority );
            if ( indicator != null )
            {
                enabled = true;

                BreakpointSpanIndicator breakpoint = indicator as BreakpointSpanIndicator;
                breakOnAllThreads = breakpoint.ForeColor == Color.Yellow;
                breakStopsGame = breakpoint.BackColor == Color.Red;
            }

            if ( enabled )
            {
                if ( !breakOnAllThreads || breakStopsGame )
                {
                    // change
                    breakOnAllThreads = true;
                    breakStopsGame = false;
                }
                else
                {
                    // remove
                    enabled = false;
                }                
            }
            else
            {
                // add
                enabled = true;
                breakOnAllThreads = true;
                breakStopsGame = false;
            }

            OnToggleBreakpoint( filename, line, enabled, breakOnAllThreads, breakStopsGame );
        }

        public void RequestBreakpointStopGame( string filename, int line )
        {
            bool enabled = false;
            bool breakOnAllThreads = false;
            bool breakStopsGame = false;

            SpanIndicator indicator = GetSpanIndicator( filename, line, SpanIndicatorLayer.BreakpointDisplayPriority );
            if ( indicator != null )
            {
                enabled = true;

                BreakpointSpanIndicator breakpoint = indicator as BreakpointSpanIndicator;
                breakOnAllThreads = breakpoint.ForeColor == Color.Yellow;
                breakStopsGame = breakpoint.BackColor == Color.Red;
            }

            if ( enabled )
            {
                if ( breakOnAllThreads || !breakStopsGame )
                {
                    // change
                    breakOnAllThreads = false;
                    breakStopsGame = true;
                }
                else
                {
                    // remove
                    enabled = false;
                }
            }
            else
            {
                // add
                enabled = true;
                breakOnAllThreads = false;
                breakStopsGame = true;
            }

            OnToggleBreakpoint( filename, line, enabled, breakOnAllThreads, breakStopsGame );
        }

        public void RequestBreakpointAllThreadsStopGame( string filename, int line )
        {
            bool enabled = false;
            bool breakOnAllThreads = false;
            bool breakStopsGame = false;

            SpanIndicator indicator = GetSpanIndicator( filename, line, SpanIndicatorLayer.BreakpointDisplayPriority );
            if ( indicator != null )
            {
                enabled = true;

                BreakpointSpanIndicator breakpoint = indicator as BreakpointSpanIndicator;
                breakOnAllThreads = breakpoint.ForeColor == Color.Yellow;
                breakStopsGame = breakpoint.BackColor == Color.Red;
            }

            if ( enabled )
            {
                if ( !breakOnAllThreads || !breakStopsGame )
                {
                    // change
                    breakOnAllThreads = true;
                    breakStopsGame = true;
                }
                else
                {
                    // remove
                    enabled = false;
                }
            }
            else
            {
                // add
                enabled = true;
                breakOnAllThreads = true;
                breakStopsGame = true;
            }

            OnToggleBreakpoint( filename, line, enabled, breakOnAllThreads, breakStopsGame );
        }

        public void RequestContinue()
        {
            OnContinueDebugger();
        }

        public void RequestPause()
        {
            OnPauseDebugger();
        }

        public void RequestStepInto()
        {
            OnStepIntoDebugger();
        }

        public void RequestStepOver()
        {
            OnStepOverDebugger();
        }

        public void RequestStepOut()
        {
            OnStepOutDebugger();
        }

        /// <summary>
        /// Called when we need to enable or disable a breakpoint.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="line"></param>
        /// <param name="enable"></param>
        /// <param name="breakOnAllThreads"></param>
        public void SetBreakpoint( string filename, int line, bool enable, bool breakOnAllThreads, bool breakStopsGame )
        {
            if ( enable )
            {
                if ( this.LoadFile( filename ) )
                {
                    AddBreakpointSpanIndicator( filename, line, breakOnAllThreads, breakStopsGame );
                }
            }
            else
            {
                RemoveBreakpointSpanIndicator( filename, line );
            }

            this.AllowBreakpoints = true;
        }

        /// <summary>
        /// Called when we need to enable to disable a program counter
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="line"></param>
        /// <param name="enable"></param>
        public void SetProgramCounter( string filename, int line, bool enable )
        {
            if ( enable )
            {
                if ( this.LoadFile( filename ) )
                {
                    AddCurrentStatementSpanIndicator( filename, line );
                }
            }
            else
            {
                RemoveCurrentStatementSpanIndicator( filename, line );
            }

            this.AllowBreakpoints = true;
        }

        /// <summary>
        /// Called as a result of pressing Play
        /// </summary>
        public void Play()
        {
            if ( this.DebugState == DebuggerState.Running )
            {
                return;
            }

            ClearCurrentStatementSpanIndicators();

            this.DebugState = DebuggerState.Running;
            this.AllowBreakpoints = true;
        }

        /// <summary>
        /// Calls as a result of the Pause button being hit, or the game has hit a breakpoint
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="line"></param>
        public void Pause( string filename, int line )
        {
            if ( this.DebugState == DebuggerState.Paused )
            {
                return;
            }

            SetProgramCounter( filename, line, true );

            this.DebugState = DebuggerState.Paused;
            this.AllowBreakpoints = true;
        }

        /// <summary>
        /// Called as a result of pressing the StepInto button
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="line"></param>
        public void StepInto( string filename, int line )
        {
            if ( this.DebugState != DebuggerState.Paused )
            {
                return;
            }

            SetProgramCounter( filename, line, true );
            
            this.AllowBreakpoints = true;
        }

        /// <summary>
        /// Called as a result of pressing the StepOver button
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="line"></param>
        public void StepOver( string filename, int line )
        {
            if ( this.DebugState != DebuggerState.Paused )
            {
                return;
            }

            SetProgramCounter( filename, line, true );
            
            this.AllowBreakpoints = true;
        }

        /// <summary>
        /// Called as a result of pressing the StepOut button
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="line"></param>
        public void StepOut( string filename, int line )
        {
            if ( this.DebugState != DebuggerState.Paused )
            {
                return;
            }

            SetProgramCounter( filename, line, true );

            this.AllowBreakpoints = true;
        }

        /// <summary>
        /// Called when the game tells us the program has exited.
        /// </summary>
        /// <param name="normalExit"><c>true</c> if the program exited normally, otherwise <c>false</c>.</param>
        public void Stop( bool normalExit )
        {
            if ( normalExit )
            {
                ClearCurrentStatementSpanIndicators();
            }

            this.DebugState = DebuggerState.Stopped;
            this.AllowBreakpoints = true;
        }

        private void OnToggleBreakpoint( string filename, int line, bool enable, bool breakOnAllThreads, bool breakStopsGame )
        {
            this.AllowBreakpoints = false;

            bool cancel = false;
            if ( this.ToggleBreakpoint != null )
            {
                BreakpointEventArgs e = new BreakpointEventArgs( filename.ToLower(), line, enable, breakOnAllThreads, breakStopsGame );
                this.ToggleBreakpoint( this, e );
                cancel = e.Cancel;
            }

            if ( !cancel )
            {
                SetBreakpoint( filename, line, enable, breakOnAllThreads, breakStopsGame );
            }
        }

        private void OnContinueDebugger()
        {
            this.AllowBreakpoints = false;

            bool cancel = false;
            if ( this.ContinueDebugger != null )
            {
                DebuggerEventArgs e = new DebuggerEventArgs();
                this.ContinueDebugger( this, e );
                cancel = e.Cancel;
            }

            if ( !cancel )
            {
                Play();
            }
        }

        private void OnPauseDebugger()
        {
            this.AllowBreakpoints = false;

            bool cancel = false;
            if ( this.PauseDebugger != null )
            {
                DebuggerEventArgs e = new DebuggerEventArgs();
                this.PauseDebugger( this, e );
                cancel = e.Cancel;
            }

            if ( !cancel )
            {
                Pause( this.CurrentSyntaxEditor.Document.Filename, -1 );
            }
        }

        private void OnStepIntoDebugger()
        {
            this.AllowBreakpoints = false;

            bool cancel = false;
            if ( this.StepIntoDebugger != null )
            {
                DebuggerEventArgs e = new DebuggerEventArgs();
                this.StepIntoDebugger( this, e );
                cancel = e.Cancel;
            }

            if ( !cancel )
            {
                StepInto( this.CurrentSyntaxEditor.Document.Filename, -1 );
            }
        }

        private void OnStepOverDebugger()
        {
            this.AllowBreakpoints = false;

            bool cancel = false;
            if ( this.StepOverDebugger != null )
            {
                DebuggerEventArgs e = new DebuggerEventArgs();
                this.StepOverDebugger( this, e );
                cancel = e.Cancel;
            }

            if ( !cancel )
            {
                StepOver( this.CurrentSyntaxEditor.Document.Filename, -1 );
            }
        }

        private void OnStepOutDebugger()
        {
            this.AllowBreakpoints = false;

            bool cancel = false;
            if ( this.StepOutDebugger != null )
            {
                DebuggerEventArgs e = new DebuggerEventArgs();
                this.StepOutDebugger( this, e );
                cancel = e.Cancel;
            }

            if ( !cancel )
            {
                StepOut( this.CurrentSyntaxEditor.Document.Filename, -1 );
            }
        }

        private void AddCurrentStatementSpanIndicator( string filename, int line )
        {
            if ( HasSpanIndicator( filename, line, SpanIndicatorLayer.CurrentStatementDisplayPriority ) )
            {
                return;
            }

            // there can be only one
            ClearCurrentStatementSpanIndicators();

            AddSpanIndicator( filename, line, SpanIndicatorLayer.CurrentStatementKey, SpanIndicatorLayer.CurrentStatementDisplayPriority,
                new CurrentStatementSpanIndicator() );

            string lowercaseFilename = filename.ToLower();
            EditorInfo eInfo;
            if (m_Editors.TryGetValue(lowercaseFilename, out eInfo))
            {
                eInfo.Editor.SelectedView.GoToLine(line);
                eInfo.Editor.SelectedView.ScrollLineToVisibleMiddle();
            }
        }

        private void RemoveCurrentStatementSpanIndicator( string filename, int line )
        {
            if ( !HasSpanIndicator( filename, line, SpanIndicatorLayer.CurrentStatementDisplayPriority ) )
            {
                return;
            }

            RemoveSpanIndicator( filename, line, SpanIndicatorLayer.CurrentStatementKey, SpanIndicatorLayer.CurrentStatementDisplayPriority );
        }

        private void ClearCurrentStatementSpanIndicators()
        {
            ClearSpanIndicators( SpanIndicatorLayer.CurrentStatementKey );
        }

        private void AddBreakpointSpanIndicator( string filename, int line, bool breakOnAnyThread, bool breakStopsGame )
        {
            BreakpointSpanIndicator breakpoint;

            SpanIndicator indicator = GetSpanIndicator( filename, line, SpanIndicatorLayer.BreakpointDisplayPriority );
            if ( indicator != null )                
            {
                breakpoint = indicator as BreakpointSpanIndicator;
                if ( ((breakpoint.ForeColor == Color.Yellow) && !breakOnAnyThread)
                    || ((breakpoint.ForeColor != Color.Yellow) && breakOnAnyThread)
                    || ((breakpoint.BackColor == Color.Red) && !breakStopsGame)
                    || ((breakpoint.BackColor != Color.Red) && breakStopsGame) )
                {
                    // The ForeColor or BackColor of the breakpoint is changing, but we have to remove it 
                    // so that the display can get updated properly.
                    RemoveSpanIndicator( filename, line, SpanIndicatorLayer.BreakpointKey, SpanIndicatorLayer.BreakpointDisplayPriority );
                }
                else
                {
                    // nothing to do
                    return;
                }
            }

            breakpoint = new BreakpointSpanIndicator();
            if ( breakOnAnyThread )
            {
                breakpoint.ForeColor = Color.Yellow;
            }

            if ( breakStopsGame )
            {
                breakpoint.BackColor = Color.Red;
            }

            AddSpanIndicator( filename, line, SpanIndicatorLayer.BreakpointKey, SpanIndicatorLayer.BreakpointDisplayPriority, breakpoint );
        }

        private void RemoveBreakpointSpanIndicator( string filename, int line )
        {
            if ( !HasSpanIndicator( filename, line, SpanIndicatorLayer.BreakpointDisplayPriority ) )
            {
                return;
            }

            RemoveSpanIndicator( filename, line, SpanIndicatorLayer.BreakpointKey, SpanIndicatorLayer.BreakpointDisplayPriority );
        }

        private void ClearBreakpointSpanIndicators()
        {
            ClearSpanIndicators( SpanIndicatorLayer.BreakpointKey );
        }

        private SpanIndicator GetSpanIndicator( string filename, int line, int layerDisplayPriority )
        {
            string lowercaseFilename = filename.ToLower();
            EditorInfo eInfo;
            if ( m_Editors.TryGetValue( lowercaseFilename, out eInfo ) )
            {
                if (eInfo.Editor.Document.Lines.Count > line)
                {
                    DocumentLine docLine = eInfo.Editor.Document.Lines[line];
                    foreach (SpanIndicator indicator in docLine.SpanIndicators)
                    {
                        if (indicator.DisplayPriority == layerDisplayPriority)
                        {
                            return indicator;
                        }
                    }
                }
            }

            return null;
        }

        private bool HasSpanIndicator( string filename, int line, int layerDisplayPriority )
        {
            return GetSpanIndicator( filename, line, layerDisplayPriority ) != null;
        }

        private void AddSpanIndicator( string filename, int line, string layerKey, int layerDisplayPriority, SpanIndicator indicator )
        {
            string lowercaseFilename = filename.ToLower();
            EditorInfo eInfo;
            if ( m_Editors.TryGetValue( lowercaseFilename, out eInfo ) )
            {
                SpanIndicatorLayer layer = eInfo.Editor.Document.SpanIndicatorLayers[layerKey];
                if ( layer == null )
                {
                    layer = new SpanIndicatorLayer( layerKey, layerDisplayPriority );
                    eInfo.Editor.Document.SpanIndicatorLayers.Add( layer );
                }

                DocumentLine docLine = eInfo.Editor.Document.Lines[line];
                layer.Add( indicator, docLine.TextRange );
            }
        }

        private void RemoveSpanIndicator( string filename, int line, string layerKey, int layerDisplayPriority )
        {
            string lowercaseFilename = filename.ToLower();
            EditorInfo eInfo;
            if ( m_Editors.TryGetValue( lowercaseFilename, out eInfo ) )
            {
                SpanIndicatorLayer layer = eInfo.Editor.Document.SpanIndicatorLayers[layerKey];
                if ( layer != null )
                {
                    DocumentLine docLine = eInfo.Editor.Document.Lines[line];
                    SpanIndicator[] indicators = layer.GetIndicatorsForTextRange( docLine.TextRange );
                    for ( int i = 0; i < indicators.Length; ++i )
                    {
                        if ( indicators[i].DisplayPriority == layerDisplayPriority )
                        {
                            layer.Remove( indicators[i] );
                        }
                    }
                }
            }
        }

        private void ClearSpanIndicators( string layerKey )
        {
            foreach ( EditorInfo eInfo in m_Editors.Values )
            {
                SpanIndicatorLayer layer = eInfo.Editor.Document.SpanIndicatorLayers[layerKey];
                if ( layer != null )
                {
                    // Removal of items appears not to be synchronized properly, others are synced.
                    lock (eInfo.Editor.Document.SpanIndicatorLayers.SyncRoot)
                    {
                        eInfo.Editor.Document.SpanIndicatorLayers.Remove(layer);
                    }
                }
            }
        }
        #endregion

        #region Outlining Data
        public void CollectAutomaticOutliningNodeInfo( List<OutlineNodeInfo> nodeInfos, ActiproSoftware.SyntaxEditor.OutliningNode node )
        {
            if ( (node.StartOffset > -1) && !node.Expanded )
            {
                OutlineNodeInfo info = new OutlineNodeInfo( node.StartOffset );
                nodeInfos.Add( info );
            }

            for ( int i = 0; i < node.Count; ++i )
            {
                CollectAutomaticOutliningNodeInfo( nodeInfos, node[i] );
            }
        }

        public void RestoreAutomaticOutliningNodeState( List<OutlineNodeInfo> nodeInfos, ActiproSoftware.SyntaxEditor.OutliningNode rootNode )
        {
            foreach ( OutlineNodeInfo info in nodeInfos )
            {
                ActiproSoftware.SyntaxEditor.OutliningNode node = rootNode.FindNodeRecursive( info.StartOffset );
                if ( node != null )
                {
                    node.Expanded = false;
                }
            }
        }

        public void CollectManualOutliningNodeInfo( List<OutlineNodeInfo> nodeInfos, ActiproSoftware.SyntaxEditor.OutliningNode node )
        {
            if ( node.StartOffset > -1 )
            {
                OutlineNodeInfo info = new OutlineNodeInfo( node.StartOffset, node.EndOffset, node.Expanded );
                nodeInfos.Add( info );
            }

            for ( int i = 0; i < node.Count; ++i )
            {
                CollectManualOutliningNodeInfo( nodeInfos, node[i] );
            }
        }

        public void RestoreManualOutliningNodeInfo( List<OutlineNodeInfo> nodeInfos, ActiproSoftware.SyntaxEditor.Document document )
        {
            foreach ( OutlineNodeInfo info in nodeInfos )
            {
                ActiproSoftware.SyntaxEditor.OutliningNode node = document.Outlining.Add( null, info.StartOffset, info.EndOffset );
                if ( node != null )
                {
                    node.Expanded = info.Expanded;
                }
            }
        }
        #endregion
    }

    #region class WatcherInfo
    internal class WatcherInfo
    {
        public WatcherInfo( string fileName )
        {
            m_FileName = fileName;

            m_Watcher = new FileSystemWatcher( Path.GetDirectoryName( m_FileName ), Path.GetFileName( m_FileName ) );
            m_Watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            m_Watcher.Changed += new FileSystemEventHandler( Watcher_Changed );
            m_Watcher.Renamed += new RenamedEventHandler( Watcher_Renamed );
            m_Watcher.EnableRaisingEvents = true;

            m_LastDateTime = DateTime.Now;
        }

        #region Delegates
        public delegate void FileExternallyModifiedEventHandler( object sender, FileSystemEventArgs e );
        public delegate void FileExternallyRenamedEventHandler( object sender, RenamedEventArgs e );
        #endregion

        #region Events
        public event FileExternallyModifiedEventHandler FileExternallyModified;
        public event FileExternallyRenamedEventHandler FileExternallyRenamed;
        #endregion

        #region Variables
        private FileSystemWatcher m_Watcher = null;
        private string m_FileName = null;
        private DateTime m_LastDateTime = DateTime.MinValue;
        #endregion

        #region Properties
        public string FileName
        {
            get
            {
                return m_FileName;
            }
        }

        public bool EnableRaisingEvents
        {
            set
            {
                m_Watcher.EnableRaisingEvents = value;
            }
        }
        #endregion

        #region Public Functions
        public void Remove()
        {
            if ( m_Watcher != null )
            {
                m_Watcher.EnableRaisingEvents = false;
                m_Watcher.Dispose();
                m_Watcher = null;
            }
        }
        #endregion

        #region Event Handlers
        private void Watcher_Changed( object sender, FileSystemEventArgs e )
        {
            if ( FileExternallyModified != null )
            {
                FileExternallyModified( this, e );
            }

            m_LastDateTime = DateTime.Now;
        }

        private void Watcher_Renamed( object source, RenamedEventArgs e )
        {
            if ( FileExternallyRenamed != null )
            {
                FileExternallyRenamed( this, e );
            }

            m_LastDateTime = DateTime.Now;
        }

        #endregion
    }
    #endregion

    #region class EditorInfo
    internal class SearchCommandLinks
    {
        public SearchCommandLinks()
        {

        }

        #region Variables
        private ActiproSoftware.WinUICore.Commands.CommandLink m_ControlFCommandLink = null;
        private ActiproSoftware.WinUICore.Commands.CommandLink m_F3CommandLink = null;
        private ActiproSoftware.WinUICore.Commands.CommandLink m_ShiftF3CommandLink = null;
        private ActiproSoftware.WinUICore.Commands.CommandLink m_ControlF3CommandLink = null;
        private ActiproSoftware.WinUICore.Commands.CommandLink m_ControlShiftF3CommandLink = null;
        #endregion

        #region Properties
        /// <summary>
        /// Opens the Find/Replace or Search/Replace dialog
        /// </summary>
        public ActiproSoftware.WinUICore.Commands.CommandLink ControlFCommandLink
        {
            get
            {
                return m_ControlFCommandLink;
            }
        }

        /// <summary>
        /// Search Again Down
        /// </summary>
        public ActiproSoftware.WinUICore.Commands.CommandLink F3CommandLink
        {
            get
            {
                return m_F3CommandLink;
            }
        }

        /// <summary>
        /// Search Again Up
        /// </summary>
        public ActiproSoftware.WinUICore.Commands.CommandLink ShiftF3CommandLink
        {
            get
            {
                return m_ShiftF3CommandLink;
            }
        }

        /// <summary>
        /// Search Highlighted Down
        /// </summary>
        public ActiproSoftware.WinUICore.Commands.CommandLink ControlF3CommandLink
        {
            get
            {
                return m_ControlF3CommandLink;
            }
        }

        /// <summary>
        /// Search Highlighted Up
        /// </summary>
        public ActiproSoftware.WinUICore.Commands.CommandLink ControlShiftF3CommandLink
        {
            get
            {
                return m_ControlShiftF3CommandLink;
            }
        }
        #endregion

        #region Public Functions
        public void SetupSearchCommandLinks( SyntaxEditor editor, SearchReplaceDialog newDialog, FindReplaceForm oldDialog, bool useNewDialog )
        {
            if ( m_ControlFCommandLink != null )
            {
                editor.CommandLinks.Remove( m_ControlFCommandLink );
            }

            if ( m_F3CommandLink != null )
            {
                editor.CommandLinks.Remove( m_F3CommandLink );
            }

            if ( m_ShiftF3CommandLink != null )
            {
                editor.CommandLinks.Remove( m_ShiftF3CommandLink );
            }

            if ( m_ControlF3CommandLink != null )
            {
                editor.CommandLinks.Remove( m_ControlF3CommandLink );
            }

            if ( m_ControlShiftF3CommandLink != null )
            {
                editor.CommandLinks.Remove( m_ControlShiftF3CommandLink );
            }

            if ( useNewDialog )
            {
                m_ControlFCommandLink = new ActiproSoftware.WinUICore.Commands.CommandLink( new SearchReplaceCommand( newDialog ),
                    new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.Control, Keys.F ) );
                m_F3CommandLink = new ActiproSoftware.WinUICore.Commands.CommandLink( new SearchAgainDownCommand( newDialog ),
                    new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.None, Keys.F3 ) );
                m_ShiftF3CommandLink = new ActiproSoftware.WinUICore.Commands.CommandLink( new SearchAgainUpCommand( newDialog ),
                    new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.Shift, Keys.F3 ) );
                m_ControlF3CommandLink = new ActiproSoftware.WinUICore.Commands.CommandLink( new SearchHighlightedDownCommand( newDialog ),
                    new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.Control, Keys.F3 ) );
                m_ControlShiftF3CommandLink = new ActiproSoftware.WinUICore.Commands.CommandLink( new SearchHighlightedUpCommand( newDialog ),
                    new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.ControlShift, Keys.F3 ) );
            }
            else
            {
                m_ControlFCommandLink = new ActiproSoftware.WinUICore.Commands.CommandLink( new FindReplaceCommand( oldDialog ),
                    new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.Control, Keys.F ) );
                m_F3CommandLink = new ActiproSoftware.WinUICore.Commands.CommandLink( new FindAgainDownCommand( oldDialog ),
                    new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.None, Keys.F3 ) );
                m_ShiftF3CommandLink = new ActiproSoftware.WinUICore.Commands.CommandLink( new FindAgainUpCommand( oldDialog ),
                    new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.Shift, Keys.F3 ) );
                m_ControlF3CommandLink = new ActiproSoftware.WinUICore.Commands.CommandLink( new FindHighlightedDownCommand( oldDialog ),
                    new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.Control, Keys.F3 ) );
                m_ControlShiftF3CommandLink = new ActiproSoftware.WinUICore.Commands.CommandLink( new FindHighlightedUpCommand( oldDialog ),
                    new ActiproSoftware.WinUICore.Commands.KeyBinding( ActiproSoftware.WinUICore.Input.ModifierKeys.ControlShift, Keys.F3 ) );
            }

            editor.CommandLinks.Add( m_ControlFCommandLink );
            editor.CommandLinks.Add( m_F3CommandLink );
            editor.CommandLinks.Add( m_ShiftF3CommandLink );
            editor.CommandLinks.Add( m_ControlF3CommandLink );
            editor.CommandLinks.Add( m_ControlShiftF3CommandLink );
        }
        #endregion
    }

    internal class EditorInfo
    {
        protected EditorInfo()
        {
        }

        public EditorInfo( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, TD.SandDock.DockControl tabPage, SearchCommandLinks cmdLinks )
        {
            m_Editor = editor;
            m_TabPage = tabPage;
            m_commandLinks = cmdLinks;
        }

        #region Variables
        private ActiproSoftware.SyntaxEditor.SyntaxEditor m_Editor;
        private TD.SandDock.DockControl m_TabPage;
        private SearchCommandLinks m_commandLinks;
        private List<OutlineNodeInfo> m_CollapsedOutliningNodes = new List<OutlineNodeInfo>();
        #endregion

        #region Properties
        public ActiproSoftware.SyntaxEditor.SyntaxEditor Editor
        {
            get
            {
                return m_Editor;
            }
        }

        public TD.SandDock.DockControl TabPage
        {
            get
            {
                return m_TabPage;
            }
        }

        public SearchCommandLinks CommandLinks
        {
            get
            {
                return m_commandLinks;
            }
        }

        public List<OutlineNodeInfo> CollapsedOutliningNodes
        {
            get
            {
                return m_CollapsedOutliningNodes;
            }
        }
        #endregion
    }

    /// <summary>
    /// Bit of a hack: Wrapping Actipro's SyntaxEditor so that events that are known to cause crashes have try/catch around them. Fixes issue with clipboard.
    /// </summary>
    internal class SaferSyntaxEditor : SyntaxEditor
    {
        /// <summary>
        /// Construct a "safe" syntax editor that wraps crashing events in try/catch.
        /// </summary>
        public SaferSyntaxEditor()
            : base()
        {
        }

        /// <summary>
        /// Override OnKeyDown. If we catch an ExternalException, we assume that the clipboard is causing havok. Instead of completely crashing,
        /// we retry a few times, eventually absorbing the exception. Resolves url:bugstar:2426503
        /// </summary>
        /// <param name="ev"></param>
        protected override void OnKeyDown(KeyEventArgs ev)
        {
            try
            {
                base.OnKeyDown(ev);
            }
            catch (System.Runtime.InteropServices.ExternalException ex)
            {
                const int numRetries = 4;
                for (int i = 0; i < numRetries; i++)
                {
                    // CLIPBRD_E_CANT_OPEN
                    if ((uint)ex.HResult == 0x800401d0)
                    {
                        ApplicationSettings.Log.Warning($"Caught a clipboard exception. Trying again ({i}/{numRetries}); most of the time it recovers after a failure... :: {ex.ToString()}");
                        try
                        {
                            base.OnKeyDown(ev);
                            return;
                        }
                        catch (System.Runtime.InteropServices.ExternalException e)
                        {
                            ex = e;
                        }
                    }
                }
                ApplicationSettings.Log.ToolException(ex, "Actipro glitch, caught ExternalException in OnKeyDown.");
            }
            catch (Exception ex)
            {
                ApplicationSettings.Log.ToolException(ex, "Actipro glitch, caught exception in OnKeyDown");
            }
        }

        /// <summary>
        /// Override OnKeyPress. Likely 'resolves' url:bugstar:3400049 the best we can.
        /// </summary>
        /// <param name="ev"></param>
        protected override void OnKeyPress(KeyPressEventArgs ev)
        {
            try
            {
                base.OnKeyPress(ev);
            }
            catch (Exception ex)
            {
                // See url:bugstar:3400049
                ApplicationSettings.Log.ToolException(ex, "Actipro glitch, caught exception in OnKeyPress.");
            }
        }
    }

    #endregion

    #region class CompileErrorInfo
    public class CompileErrorInfo
    {
        public CompileErrorInfo( string filename, int lineNumber )
        {
            m_filename = filename;
            m_lineNumber = lineNumber;
        }

        #region Properties
        public string FileName
        {
            get
            {
                return m_filename;
            }
        }

        public int LineNumber
        {
            get
            {
                return m_lineNumber;
            }
        }
        #endregion

        #region Variables
        private string m_filename;
        private int m_lineNumber;
        #endregion
    }
    #endregion

    #region Event Args
    /// <summary>
    /// Not to be instantiated
    /// </summary>
    public class FileLoadEventArgs : EventArgs
    {
        public FileLoadEventArgs( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, System.Guid guid, bool newFile, bool loadMultiple )
        {
            m_editor = editor;
            m_guid = guid;
            m_newFile = newFile;
            m_loadingMultiple = loadMultiple;
        }

        #region Properties
        /// <summary>
        /// Gets the editor
        /// </summary>
        public ActiproSoftware.SyntaxEditor.SyntaxEditor Editor
        {
            get
            {
                return m_editor;
            }
        }

        /// <summary>
        /// Get the guid for this file
        /// </summary>
        public System.Guid Guid
        {
            get
            {
                return m_guid;
            }
        }

        /// <summary>
        /// Returns true when a new file is/was created.
        /// </summary>
        public bool IsNewFile
        {
            get
            {
                return m_newFile;
            }
        }

        /// <summary>
        /// Returns true when this is part of a LoadFiles operation
        /// </summary>
        public bool IsLoadMultiple
        {
            get
            {
                return m_loadingMultiple;
            }
        }
        #endregion

        #region Variables
        protected ActiproSoftware.SyntaxEditor.SyntaxEditor m_editor;
        protected System.Guid m_guid;
        protected bool m_newFile;
        protected bool m_loadingMultiple;
        #endregion
    }

    public class FileLoadingEventArgs : FileLoadEventArgs
    {
        public FileLoadingEventArgs( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, System.Guid guid, bool newFile, bool loadMultiple )
            : base( editor, guid, newFile, loadMultiple )
        {
            Cancel = false;
            DockControl = null;
        }

        #region Properties
        /// <summary>
        /// Set the dock control that contains the Editor
        /// </summary>
        public TD.SandDock.DockControl DockControl;

        /// <summary>
        /// Set to true to cancel the load
        /// </summary>
        public bool Cancel;
        #endregion
    }

    public class FileLoadedEventArgs : FileLoadEventArgs
    {
        public FileLoadedEventArgs( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, System.Guid guid, bool newFile, bool loadMultiple,
            TD.SandDock.DockControl dockControl, bool cancelled, string reason )
            : base( editor, guid, newFile, loadMultiple )
        {
            m_dockControl = dockControl;
            m_cancelled = cancelled;
            m_reason = reason;
        }

        #region Properties
        /// <summary>
        /// Get the dock control that contains the Editor
        /// </summary>
        public TD.SandDock.DockControl DockControl
        {
            get
            {
                return m_dockControl;
            }
        }

        /// <summary>
        /// Gets whether the load was cancelled due to an error or other reason
        /// </summary>
        public bool Cancelled
        {
            get
            {
                return m_cancelled;
            }
        }

        /// <summary>
        /// Gets the reason it was cancelled
        /// </summary>
        public string Reason
        {
            get
            {
                return m_reason;
            }
        }
        #endregion

        #region Variables
        private TD.SandDock.DockControl m_dockControl;
        private bool m_cancelled;
        private string m_reason;
        #endregion
    }

    /// <summary>
    /// Not to be instantiated
    /// </summary>
    public class FileSaveEventArgs : EventArgs
    {
        public FileSaveEventArgs( SyntaxEditor editor, string filename, bool saveAll )
        {
            m_editor = editor;
            m_filename = filename;
            m_saveAll = saveAll;
        }

        #region Properties
        /// <summary>
        /// The editor being saved
        /// </summary>
        public SyntaxEditor Editor
        {
            get
            {
                return m_editor;
            }
        }

        /// <summary>
        /// If this is a save as operation
        /// </summary>
        public bool IsSaveAs
        {
            get
            {
                if ( this.Editor != null )
                {
                    return m_filename.ToLower() != this.Editor.Document.Filename.ToLower();
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the name of the file saved
        /// </summary>
        public string Filename
        {
            get
            {
                return m_filename;
            }
        }

        /// <summary>
        /// If this is a save all operation
        /// </summary>
        public bool IsSaveAll
        {
            get
            {
                return m_saveAll;
            }
        }
        #endregion

        #region Variables
        protected SyntaxEditor m_editor;
        protected string m_filename;
        protected bool m_saveAll;
        #endregion
    }

    public class FileSavingEventArgs : FileSaveEventArgs
    {
        public FileSavingEventArgs( SyntaxEditor editor, string filename, bool saveAll )
            : base( editor, filename, saveAll )
        {
            Cancel = false;
        }

        #region Properties
        /// <summary>
        /// Set to true to cancel the save
        /// </summary>
        public bool Cancel;
        #endregion
    }

    public class FileSavedEventArgs : FileSaveEventArgs
    {
        public FileSavedEventArgs( SyntaxEditor editor, string filename, bool saveAll,
            bool cancelled, string reason )
            : base( editor, filename, saveAll )
        {
            m_cancelled = cancelled;
            m_reason = reason;
        }

        #region Properties
        /// <summary>
        /// Gets whether the load was cancelled due to an error or other reason
        /// </summary>
        public bool Cancelled
        {
            get
            {
                return m_cancelled;
            }
        }

        /// <summary>
        /// Gets the reason it was cancelled
        /// </summary>
        public string Reason
        {
            get
            {
                return m_reason;
            }
        }
        #endregion

        #region Variables
        private bool m_cancelled;
        private string m_reason;
        #endregion
    }

    /// <summary>
    /// Not to be instantiated
    /// </summary>
    public class FileCloseEventArgs : EventArgs
    {
        public FileCloseEventArgs( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, bool newFile, TD.SandDock.DockControl dockControl,
            bool closeMultiple )
        {
            m_editor = editor;
            m_newFile = newFile;
            m_dockControl = dockControl;
            m_closingMultiple = closeMultiple;
        }

        #region Properties
        /// <summary>
        /// Get the editor that is being closed
        /// </summary>
        public ActiproSoftware.SyntaxEditor.SyntaxEditor Editor
        {
            get
            {
                return m_editor;
            }
        }

        /// <summary>
        /// Returns true when a new file is being closed
        /// </summary>
        public bool IsNewFile
        {
            get
            {
                return m_newFile;
            }
        }

        /// <summary>
        /// Gets the tab doc that is being closed
        /// </summary>
        public TD.SandDock.DockControl DockControl
        {
            get
            {
                return m_dockControl;
            }
        }

        /// <summary>
        /// Returns true when part of a CloseFiles operation
        /// </summary>
        public bool IsCloseMultiple
        {
            get
            {
                return m_closingMultiple;
            }
        }
        #endregion

        #region Variables
        protected ActiproSoftware.SyntaxEditor.SyntaxEditor m_editor;
        protected bool m_newFile;
        protected TD.SandDock.DockControl m_dockControl;
        protected bool m_closingMultiple;
        #endregion
    }

    public class FileClosingEventArgs : FileCloseEventArgs
    {
        public FileClosingEventArgs( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, bool newFile, TD.SandDock.DockControl dockControl,
            bool closeMultiple )
            : base( editor, newFile, dockControl, closeMultiple )
        {
            Cancel = false;
        }

        #region Properties
        /// <summary>
        /// Set to true to cancel the close
        /// </summary>
        public bool Cancel;
        #endregion
    }

    public class FileClosedEventArgs : FileCloseEventArgs
    {
        public FileClosedEventArgs( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, bool newFile, TD.SandDock.DockControl dockControl,
            bool closeMultiple, bool cancelled, string reason )
            : base( editor, newFile, dockControl, closeMultiple )
        {
            m_cancelled = cancelled;
            m_reason = reason;
        }

        #region Properties
        /// <summary>
        /// Gets whether the close was cancelled due to an error or other reason
        /// </summary>
        public bool Cancelled
        {
            get
            {
                return m_cancelled;
            }
        }

        /// <summary>
        /// Gets the reason it was cancelled
        /// </summary>
        public string Reason
        {
            get
            {
                return m_reason;
            }
        }
        #endregion

        #region Variables
        private bool m_cancelled;
        private string m_reason;
        #endregion
    }

    /// <summary>
    /// Not to be instantiated
    /// </summary>
    public class FileAllOperationEventArgs : EventArgs
    {
        public FileAllOperationEventArgs( List<string> files )
        {
            m_files = files;
        }

        #region Properties
        /// <summary>
        /// Gets a list of the files saved
        /// </summary>
        public string[] Files
        {
            get
            {
                return m_files.ToArray(); ;
            }
        }
        #endregion

        #region Variables
        protected List<string> m_files;
        #endregion
    }

    public class FileAllPreOperationEventArgs : FileAllOperationEventArgs
    {
        public FileAllPreOperationEventArgs( List<string> files )
            : base( files )
        {
            Cancel = false;
        }

        #region Properties
        /// <summary>
        /// Set to true to cancel the operation
        /// </summary>
        public bool Cancel;
        #endregion
    }

    public class FileAllPostOperationEventArgs : FileAllOperationEventArgs
    {
        public FileAllPostOperationEventArgs( List<string> files, List<string> filesNotSaved, List<string> reasons )
            : base( files )
        {
            m_filesNotSaved = filesNotSaved;
            m_reasons = reasons;
        }

        #region Properties
        /// <summary>
        /// Returns true if not all of the files were saved
        /// </summary>
        public bool Cancellations
        {
            get
            {
                return (m_filesNotSaved != null) && (m_filesNotSaved.Count > 0);
            }
        }

        /// <summary>
        /// Gets a list of the files not saved
        /// </summary>
        public string[] FailedFiles
        {
            get
            {
                string[] files = new string[m_files.Count];
                m_files.CopyTo( files, 0 );
                return files;
            }
        }

        /// <summary>
        /// Gets a list of the reasons why they were not saved
        /// </summary>
        public string[] Reasons
        {
            get
            {
                string[] reasons = new string[m_reasons.Count];
                m_reasons.CopyTo( reasons, 0 );
                return reasons;
            }
        }
        #endregion

        #region Variables
        protected List<string> m_filesNotSaved;
        private List<string> m_reasons;
        #endregion
    }

    public class DebuggerEventArgs : EventArgs
    {
        public DebuggerEventArgs()
        {

        }

        #region Properties
        protected bool m_cancel = false;
        #endregion

        #region Variables
        /// <summary>
        /// Set by the event handler, indicates whether to proceed with the request.  When <c>false</c>,
        /// the operation is cancelled, but the user may later call the operation manually.  The idea is
        /// the interface triggers the event, the sends a message about the event to the game, the game
        /// responds with what to do, then finally, we execute the operation.
        /// </summary>
        public bool Cancel
        {
            get
            {
                return m_cancel;
            }
            set
            {
                m_cancel = value;
            }
        }
        #endregion
    }

    public class BreakpointEventArgs : DebuggerEventArgs
    {
        public BreakpointEventArgs( string filename, int line, bool enable, bool breakOnAllThreads, bool breakStopsGame )
        {
            m_filename = filename;
            m_line = line;
            m_enable = enable;
            m_breakOnAllThreads = breakOnAllThreads;
            m_breakStopsGame = breakStopsGame;
        }

        #region Properties
        /// <summary>
        /// Gets the filename that's trying to toggle the breakpoint
        /// </summary>
        public string FileName
        {
            get
            {
                return m_filename;
            }
        }

        /// <summary>
        /// Get the line number on which the breakpoint may be placed.
        /// </summary>
        public int LineNumber
        {
            get
            {
                return m_line;
            }
        }

        /// <summary>
        /// Indicates whether we are toggling a breakpoint on or off
        /// </summary>
        public bool Enable
        {
            get
            {
                return m_enable;
            }
        }

        public bool BreakOnAllThreads
        {
            get
            {
                return m_breakOnAllThreads;
            }
        }

        public bool BreakStopsGame
        {
            get
            {
                return m_breakStopsGame;
            }
        }
        #endregion

        #region Variables
        private string m_filename;
        private int m_line;
        private bool m_enable;
        private bool m_breakOnAllThreads;
        private bool m_breakStopsGame;
        #endregion
    }
    #endregion

    #region SyntaxEditor Commands
    internal class GoToLineCommand : ActiproSoftware.SyntaxEditor.Commands.EditCommand
    {
        public GoToLineCommand( ScriptEditor scriptEditor )
        {
            m_scriptEditor = scriptEditor;
        }

        #region Overides
        public override bool CanRecordInMacro
        {
            get
            {
                return false;
            }
        }

        public override void Execute( EditCommandContext context )
        {
            int currentline = context.Document.Lines.IndexOf( context.Selection.EndOffset ) + 1;
            int maximum = context.Document.Lines.Count;

            int linenumber = GotoLineNumberForm.ShowIt( m_scriptEditor.ParentControl, "Line Number", currentline, maximum );
            if ( (linenumber > 0) && (linenumber <= maximum) )
            {
                context.SyntaxEditor.SelectedView.GoToLine( linenumber - 1 );
            }
        }
        #endregion

        #region Variables
        private ScriptEditor m_scriptEditor;
        #endregion
    }

    internal class GetImplementationInformationCommand : ActiproSoftware.SyntaxEditor.Commands.EditCommand
    {
        public GetImplementationInformationCommand( ScriptEditor scriptEditor )
        {
            m_scriptEditor = scriptEditor;
        }

        #region Overides
        public override bool CanRecordInMacro
        {
            get
            {
                return false;
            }
        }

        public override void Execute( EditCommandContext context )
        {
            string[] fileNames = null;
            TextRange[] fileTextRanges = null;

            IParserProjectResolver projectResolver = context.Document.LanguageData as IParserProjectResolver;
            if ( projectResolver != null )
            {
                projectResolver.GetImplementationInformation( context.SyntaxEditor, out fileNames, out fileTextRanges );
            }

            if ( (fileNames != null) && (fileTextRanges != null) )
            {
                for ( int i = 0; i < fileNames.Length; ++i )
                {
                    // break out if selection was successful:
                    if ( m_scriptEditor.SelectOffset( fileNames[i], fileTextRanges[i].StartOffset, fileTextRanges[i].EndOffset ) )
                    {
                        break;
                    }
                }
            }
        }
        #endregion

        #region Variables
        private ScriptEditor m_scriptEditor;
        #endregion
    }

    internal class GoToProgramCounterCommand : ActiproSoftware.SyntaxEditor.Commands.EditCommand
    {
        public GoToProgramCounterCommand(ScriptEditor scriptEditor)
        {
            m_scriptEditor = scriptEditor;
        }

        #region Overides
        public override bool CanRecordInMacro
        {
            get
            {
                return false;
            }
        }

        public override void Execute(EditCommandContext context)
        {
            IEnumerable<String> allScriptNames = m_scriptEditor.GetScriptNamesWithDebugInfo();
            if (allScriptNames.None())
            {
                rageMessageBox.ShowError(
                    this.m_scriptEditor.ParentForm,
                    "No debug symbol files were found in build directory. Program counter locations cannot be retrieved.\r\n\r\nPlease compile scripts in debug mode to generate .scd files.",
                    "Debug symbols unavailable");
                return;
            }

            String selectedScriptName = Path.GetFileNameWithoutExtension(context.Document.Filename);
            if (!allScriptNames.Contains(selectedScriptName, StringComparer.OrdinalIgnoreCase))
            {
                selectedScriptName = null;
            }

            int counterValue = GotoProgramCounterForm.ShowIt(m_scriptEditor.ParentControl, allScriptNames, ref selectedScriptName);
            if (counterValue > 0 && selectedScriptName != null)
            {
                m_scriptEditor.GoToScriptCounter(selectedScriptName, counterValue);
            }
        }
        #endregion

        #region Variables
        private ScriptEditor m_scriptEditor;
        #endregion
    }

    internal class DebugContinueCommand : ActiproSoftware.SyntaxEditor.Commands.EditCommand
    {
        public DebugContinueCommand( ScriptEditor scriptEditor )
        {
            m_scriptEditor = scriptEditor;
        }

        #region Overides
        public override bool CanRecordInMacro
        {
            get
            {
                return false;
            }
        }

        public override void Execute( EditCommandContext context )
        {
            if ( !m_scriptEditor.BreakPointsEnabled || !m_scriptEditor.AllowBreakpoints )
            {
                return;
            }

            m_scriptEditor.RequestContinue();
        }
        #endregion

        #region Variables
        private ScriptEditor m_scriptEditor;
        #endregion
    }

    internal class DebugPauseCommand : ActiproSoftware.SyntaxEditor.Commands.EditCommand
    {
        public DebugPauseCommand( ScriptEditor scriptEditor )
        {
            m_scriptEditor = scriptEditor;
        }

        #region Overides
        public override bool CanRecordInMacro
        {
            get
            {
                return false;
            }
        }

        public override void Execute( EditCommandContext context )
        {
            if ( !m_scriptEditor.BreakPointsEnabled || !m_scriptEditor.AllowBreakpoints )
            {
                return;
            }

            m_scriptEditor.RequestPause();
        }
        #endregion

        #region Variables
        private ScriptEditor m_scriptEditor;
        #endregion
    }

    internal class DebugBreakPointCommand : ActiproSoftware.SyntaxEditor.Commands.EditCommand
    {
        public DebugBreakPointCommand( ScriptEditor scriptEditor )
        {
            m_scriptEditor = scriptEditor;
        }

        #region Overides
        public override bool CanRecordInMacro
        {
            get
            {
                return false;
            }
        }

        public override void Execute( EditCommandContext context )
        {
            if ( !m_scriptEditor.BreakPointsEnabled || !m_scriptEditor.AllowBreakpoints )
            {
                return;
            }

            int line = context.SyntaxEditor.SelectedView.Selection.LastDocumentPosition.Line;
            if ( line > 0 )
            {
                m_scriptEditor.RequestBreakpoint( context.Document.Filename, line );
            }
        }
        #endregion

        #region Variables
        private ScriptEditor m_scriptEditor;
        #endregion
    }

    internal class DebugBreakPointAllThreadsCommand : ActiproSoftware.SyntaxEditor.Commands.EditCommand
    {
        public DebugBreakPointAllThreadsCommand( ScriptEditor scriptEditor )
        {
            m_scriptEditor = scriptEditor;
        }

        #region Overides
        public override bool CanRecordInMacro
        {
            get
            {
                return false;
            }
        }

        public override void Execute( EditCommandContext context )
        {
            if ( !m_scriptEditor.BreakPointsEnabled || !m_scriptEditor.AllowBreakpoints )
            {
                return;
            }

            int line = context.SyntaxEditor.SelectedView.Selection.LastDocumentPosition.Line;
            if ( line > 0 )
            {
                m_scriptEditor.RequestBreakpointAllThreads( context.Document.Filename, line );
            }
        }
        #endregion

        #region Variables
        private ScriptEditor m_scriptEditor;
        #endregion
    }

    internal class DebugBreakPointStopGameCommand : ActiproSoftware.SyntaxEditor.Commands.EditCommand
    {
        public DebugBreakPointStopGameCommand( ScriptEditor scriptEditor )
        {
            m_scriptEditor = scriptEditor;
        }

        #region Overides
        public override bool CanRecordInMacro
        {
            get
            {
                return false;
            }
        }

        public override void Execute( EditCommandContext context )
        {
            if ( !m_scriptEditor.BreakPointsEnabled || !m_scriptEditor.AllowBreakpoints )
            {
                return;
            }

            int line = context.SyntaxEditor.SelectedView.Selection.LastDocumentPosition.Line;
            if ( line > 0 )
            {
                m_scriptEditor.RequestBreakpointStopGame( context.Document.Filename, line );
            }
        }
        #endregion

        #region Variables
        private ScriptEditor m_scriptEditor;
        #endregion
    }

    internal class DebugBreakPointAllThreadsStopGameCommand : ActiproSoftware.SyntaxEditor.Commands.EditCommand
    {
        public DebugBreakPointAllThreadsStopGameCommand( ScriptEditor scriptEditor )
        {
            m_scriptEditor = scriptEditor;
        }

        #region Overides
        public override bool CanRecordInMacro
        {
            get
            {
                return false;
            }
        }

        public override void Execute( EditCommandContext context )
        {
            if ( !m_scriptEditor.BreakPointsEnabled || !m_scriptEditor.AllowBreakpoints )
            {
                return;
            }

            int line = context.SyntaxEditor.SelectedView.Selection.LastDocumentPosition.Line;
            if ( line > 0 )
            {
                m_scriptEditor.RequestBreakpointAllThreadsStopGame( context.Document.Filename, line );
            }
        }
        #endregion

        #region Variables
        private ScriptEditor m_scriptEditor;
        #endregion
    }

    internal class DebugStepIntoCommand : ActiproSoftware.SyntaxEditor.Commands.EditCommand
    {
        public DebugStepIntoCommand( ScriptEditor scriptEditor )
        {
            m_scriptEditor = scriptEditor;
        }

        #region Overides
        public override bool CanRecordInMacro
        {
            get
            {
                return false;
            }
        }

        public override void Execute( EditCommandContext context )
        {
            if ( !m_scriptEditor.BreakPointsEnabled || !m_scriptEditor.AllowBreakpoints )
            {
                return;
            }

            m_scriptEditor.RequestStepInto();
        }
        #endregion

        #region Variables
        private ScriptEditor m_scriptEditor;
        #endregion
    }

    internal class DebugStepOverCommand : ActiproSoftware.SyntaxEditor.Commands.EditCommand
    {
        public DebugStepOverCommand( ScriptEditor scriptEditor )
        {
            m_scriptEditor = scriptEditor;
        }

        #region Overides
        public override bool CanRecordInMacro
        {
            get
            {
                return false;
            }
        }

        public override void Execute( EditCommandContext context )
        {
            if ( !m_scriptEditor.BreakPointsEnabled || !m_scriptEditor.AllowBreakpoints )
            {
                return;
            }

            m_scriptEditor.RequestStepOver();
        }
        #endregion

        #region Variables
        private ScriptEditor m_scriptEditor;
        #endregion
    }

    internal class DebugStepOutCommand : ActiproSoftware.SyntaxEditor.Commands.EditCommand
    {
        public DebugStepOutCommand( ScriptEditor scriptEditor )
        {
            m_scriptEditor = scriptEditor;
        }

        #region Overides
        public override bool CanRecordInMacro
        {
            get
            {
                return false;
            }
        }

        public override void Execute( EditCommandContext context )
        {
            if ( !m_scriptEditor.BreakPointsEnabled || !m_scriptEditor.AllowBreakpoints )
            {
                return;
            }

            m_scriptEditor.RequestStepOut();
        }
        #endregion

        #region Variables
        private ScriptEditor m_scriptEditor;
        #endregion
    }

    internal abstract class FindCommand : ActiproSoftware.SyntaxEditor.Commands.EditCommand
    {
        public FindCommand( FindReplaceForm form )
        {
            m_findReplaceForm = form;
        }

        #region Overides
        public override bool CanRecordInMacro
        {
            get
            {
                return true;
            }
        }
        #endregion

        #region Variables
        protected FindReplaceForm m_findReplaceForm;
        #endregion
    }

    internal class FindReplaceCommand : FindCommand
    {
        public FindReplaceCommand( FindReplaceForm form )
            : base( form )
        {
            
        }

        #region Overides
        public override void Execute( EditCommandContext context )
        {
            if ( m_findReplaceForm != null )
            {
                // cancel intellisense if it is active
                m_findReplaceForm.CancelActiveIntellisense();

                // provide a default location, if none has been provided
                if ( !m_findReplaceForm.LocationSet && (m_findReplaceForm.ParentControl != null) )
                {
                    ApplicationSettings.CenterLocation( m_findReplaceForm.ParentControl, m_findReplaceForm );
                    m_findReplaceForm.LocationSet = true;
                }

                // Show the find/replace form
                if ( !m_findReplaceForm.Visible )
                {
                    if ( m_findReplaceForm.ParentControl != null )
                    {
                        m_findReplaceForm.Show( m_findReplaceForm.ParentControl );
                    }
                    else
                    {
                        m_findReplaceForm.Show();
                    }
                }

                m_findReplaceForm.Focus();

                m_findReplaceForm.InitSearchText( (context != null) ? context.SyntaxEditor : null );
            }
        }
        #endregion
    }

    internal class FindHighlightedUpCommand : FindCommand
    {
        public FindHighlightedUpCommand( FindReplaceForm form )
            : base( form )
        {
            
        }

        #region Overides
        public override void Execute( EditCommandContext context )
        {
            m_findReplaceForm.SearchHighlightedUp( context.SyntaxEditor );
        }
        #endregion
    }

    internal class FindHighlightedDownCommand : FindCommand
    {
        public FindHighlightedDownCommand( FindReplaceForm form )
            : base( form )
        {
            
        }

        #region Overides
        public override void Execute( EditCommandContext context )
        {
            m_findReplaceForm.SearchHighlightedDown( context.SyntaxEditor );
        }
        #endregion
    }

    internal class FindAgainUpCommand : FindCommand
    {
        public FindAgainUpCommand( FindReplaceForm form )
            : base( form )
        {
            
        }

        #region Overides
        public override void Execute( EditCommandContext context )
        {
            m_findReplaceForm.SearchAgainUp( context.SyntaxEditor );
        }
        #endregion
    }

    internal class FindAgainDownCommand : FindCommand
    {
        public FindAgainDownCommand( FindReplaceForm form )
            : base( form )
        {
            
        }

        #region Overides
        public override void Execute( EditCommandContext context )
        {
            m_findReplaceForm.SearchAgainDown( context.SyntaxEditor );
        }
        #endregion
    }

    internal abstract class SearchCommand : ActiproSoftware.SyntaxEditor.Commands.EditCommand
    {
        public SearchCommand( SearchReplaceDialog dialog )
        {
            m_searchReplaceDialog = dialog;
        }

        #region Overides
        public override bool CanRecordInMacro
        {
            get
            {
                return true;
            }
        }
        #endregion

        #region Variables
        protected SearchReplaceDialog m_searchReplaceDialog;
        #endregion
    }

    internal class SearchReplaceCommand : SearchCommand
    {
        public SearchReplaceCommand( SearchReplaceDialog dialog )
            : base( dialog )
        {
            
        }

        #region Overides
        public override void Execute( EditCommandContext context )
        {
            if ( m_searchReplaceDialog != null )
            {
                // cancel intellisense if it is active
                m_searchReplaceDialog.CancelActiveIntellisense();

                // provide a default location, if none has been provided
                if ( !m_searchReplaceDialog.LocationSet && (m_searchReplaceDialog.ParentControl != null) )
                {
                    ApplicationSettings.CenterLocation( m_searchReplaceDialog.ParentControl, m_searchReplaceDialog );
                    m_searchReplaceDialog.LocationSet = true;
                }

                // Show the find/replace form
                if ( !m_searchReplaceDialog.Visible )
                {
                    if ( m_searchReplaceDialog.ParentControl != null )
                    {
                        m_searchReplaceDialog.Show( m_searchReplaceDialog.ParentControl );
                    }
                    else
                    {
                        m_searchReplaceDialog.Show();
                    }
                }

                m_searchReplaceDialog.Focus();

                m_searchReplaceDialog.InitSearchText( (context != null) ? context.SyntaxEditor : null );
            }
        }
        #endregion
    }

    internal class SearchHighlightedUpCommand : SearchCommand
    {
        public SearchHighlightedUpCommand( SearchReplaceDialog dialog )
            : base( dialog )
        {
            
        }

        #region Overides
        public override void Execute( EditCommandContext context )
        {
            m_searchReplaceDialog.SearchHighlightedUp( context.SyntaxEditor );
        }
        #endregion
    }

    internal class SearchHighlightedDownCommand : SearchCommand
    {
        public SearchHighlightedDownCommand( SearchReplaceDialog dialog )
            : base( dialog )
        {
            
        }

        #region Overides
        public override void Execute( EditCommandContext context )
        {
            m_searchReplaceDialog.SearchHighlightedDown( context.SyntaxEditor );
        }
        #endregion
    }

    internal class SearchAgainUpCommand : SearchCommand
    {
        public SearchAgainUpCommand( SearchReplaceDialog dialog )
            : base( dialog )
        {
            
        }

        #region Overides
        public override void Execute( EditCommandContext context )
        {
            m_searchReplaceDialog.SearchAgainUp( context.SyntaxEditor );
        }
        #endregion
    }

    internal class SearchAgainDownCommand : SearchCommand
    {
        public SearchAgainDownCommand( SearchReplaceDialog dialog )
            : base( dialog )
        {
            
        }

        #region Overides
        public override void Execute( EditCommandContext context )
        {
            m_searchReplaceDialog.SearchAgainDown( context.SyntaxEditor );
        }
        #endregion
    }
    #endregion

    #region VisualStudioStyleTabbedDocument
    public class VisualStudioStyleTabbedDocument : TabbedDocument
    {
        public VisualStudioStyleTabbedDocument()
            : base()
        {

        }

        public VisualStudioStyleTabbedDocument( SandDockManager manager, Control control, string text )
            : base( manager, control, text )
        {

        }

        protected override bool AllowKeyboardNavigation
        {
            get
            {
                return false;
            }
        }
    };
    #endregion
}
