using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Threading;
using ActiproSoftware.SyntaxEditor;
using System.Collections;

namespace ragScriptEditorShared
{
	public class FindReplace
	{
        public FindReplace( Control parentControl, SourceControl srcCtrl )
        {
            m_parentControl = parentControl;
            m_sourceControl = srcCtrl;
        }

        #region Enums
        public enum EFindNextReturnTypes
		{
			E_NO_ERROR=0,
			E_ERROR,
			E_PAST_EOF,
			E_PAST_START_OF_SEARCH,
			E_NOT_FOUND,
			E_NUM_FIND_NEXT_RETURN_TYPES
		};
        #endregion

        #region Delegates
        public delegate void AddFoundStringDelegate( string fileName, int fileLine, string lineStr );
        public delegate void SearchBeginDelegate( bool replace );
		public delegate void SearchCompleteDelegate( int totalFound, int matchingFiles, int totalFilesSearched, 
			EFindNextReturnTypes err, string errorString1, string errorString2 );
		public delegate bool LoadFileDelegate( string fileName );
		public delegate bool ReloadFileDelegate( string fileName );
		public delegate void DisableFileWatcherDelegate( string fileName );
		public delegate void EnableFileWatcherDelegate( string fileName );
        #endregion

        #region Variables
        private SourceControl m_sourceControl = null;
        private Control m_parentControl;

		private string m_FileName;
		private FindReplaceOptions m_FindReplaceOptions;

		private string m_Dir;
		private string m_LastFile;
		private string m_SearchPattern;
		private bool m_RecurseSubdir;

        private AddFoundStringDelegate m_FoundStringDel = null;
        private SearchBeginDelegate m_SearchBeginDel = null;
        private SearchCompleteDelegate m_SearchDoneDel = null;
		private ReloadFileDelegate m_reloadFileDel = null;
		private DisableFileWatcherDelegate m_disableFileWatcherDel = null;
		private EnableFileWatcherDelegate m_enableFileWatcherDel = null;

		private List<string> m_FileNames;

		private int m_TotalFound;
		private int m_MatchingFiles;
		private int m_TotalFilesSearched;
        private int m_TotalTickCount;

		private StreamReader m_StreamReader = null;
		private StreamWriter m_StreamWriter = null;
		private FileInfo m_WriterFileInfo = null;
		#endregion

        #region Properties
        public SourceControl SourceControl
        {
            set
            {
                m_sourceControl = value;
            }
        }

        public int TotalFound
        {
            get
            {
                return m_TotalFound;
            }
        }

        public int MatchingFiles
        {
            get
            {
                return m_MatchingFiles;
            }
        }

        public int TotalFilesSearched
        {
            get
            {
                return m_TotalFilesSearched;
            }
        }
        #endregion

        #region Init Functions
        /// <summary>
        /// Initializes FindReplace to perform a directory search as a thread.
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="recurseSubdir"></param>
        /// <param name="searchPattern"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="foundStringDel"></param>
        /// <param name="searchBeginDel"></param>
        /// <param name="searchDoneDel"></param>
        /// <param name="reloadFileDel"></param>
        /// <param name="disableFileWatcherDel"></param>
        /// <param name="enableFileWatcherDel"></param>
        /// <param name="lastFile"></param>
		public void InitNewDirectorySearch( string dir, bool recurseSubdir, string searchPattern, FindReplaceOptions findReplaceOptions,
			AddFoundStringDelegate foundStringDel, SearchBeginDelegate searchBeginDel, SearchCompleteDelegate searchDoneDel,
			ReloadFileDelegate reloadFileDel, DisableFileWatcherDelegate disableFileWatcherDel, 
			EnableFileWatcherDelegate enableFileWatcherDel, string lastFile )
		{
			m_TotalFound = 0;
			m_MatchingFiles = 0;
			m_TotalFilesSearched = 0;
            m_TotalTickCount = 0;

			m_Dir = dir;
			m_RecurseSubdir = recurseSubdir;
			m_SearchPattern = searchPattern;
			m_FindReplaceOptions = findReplaceOptions;
			m_FoundStringDel = foundStringDel;
            m_SearchBeginDel = searchBeginDel;
			m_SearchDoneDel = searchDoneDel;
			m_reloadFileDel = reloadFileDel;
			m_disableFileWatcherDel = disableFileWatcherDel;
			m_enableFileWatcherDel = enableFileWatcherDel;

			m_LastFile = lastFile;

			StopSearch();
		}

		/// <summary>
        /// Initializes FindReplace to perform a closed document search as a thread
		/// </summary>
		/// <param name="fileName"></param>
		/// <param name="findReplaceOptions"></param>
		/// <param name="foundStringDel"></param>
		/// <param name="searchBeginDel"></param>
		/// <param name="searchDoneDel"></param>
		/// <param name="reloadFileDel"></param>
		/// <param name="disableFileWatcherDel"></param>
		/// <param name="enableFileWatcherDel"></param>
		public void InitNewClosedDocSearch( string fileName, FindReplaceOptions findReplaceOptions,
            AddFoundStringDelegate foundStringDel, SearchBeginDelegate searchBeginDel, SearchCompleteDelegate searchDoneDel,
			ReloadFileDelegate reloadFileDel, DisableFileWatcherDelegate disableFileWatcherDel, 
			EnableFileWatcherDelegate enableFileWatcherDel )
		{
			m_TotalFound = 0;
			m_MatchingFiles = 0;
			m_TotalFilesSearched = 0;
            m_TotalTickCount = 0;

			m_FileName = fileName;
			m_FindReplaceOptions = findReplaceOptions;
			m_FoundStringDel = foundStringDel;
            m_SearchBeginDel = searchBeginDel;
			m_SearchDoneDel = searchDoneDel;
			m_reloadFileDel = reloadFileDel;
			m_disableFileWatcherDel = disableFileWatcherDel;
			m_enableFileWatcherDel = enableFileWatcherDel;

			StopSearch();
		}

		/// <summary>
        /// Initializes FindReplace to perform a closed documents search as a thread
		/// </summary>
		/// <param name="fileNames"></param>
		/// <param name="searchPattern"></param>
		/// <param name="findReplaceOptions"></param>
		/// <param name="foundStringDel"></param>
		/// <param name="searchBeginDel"></param>
		/// <param name="searchDoneDel"></param>
		/// <param name="reloadFileDel"></param>
		/// <param name="disableFileWatcherDel"></param>
		/// <param name="enableFileWatcherDel"></param>
		public void InitNewClosedDocsSearch( List<string> fileNames, string searchPattern, FindReplaceOptions findReplaceOptions,
            AddFoundStringDelegate foundStringDel, SearchBeginDelegate searchBeginDel, SearchCompleteDelegate searchDoneDel,
			ReloadFileDelegate reloadFileDel, DisableFileWatcherDelegate disableFileWatcherDel, 
			EnableFileWatcherDelegate enableFileWatcherDel )
		{
			m_TotalFound = 0;
			m_MatchingFiles = 0;
			m_TotalFilesSearched = 0;
            m_TotalTickCount = 0;

			m_FileNames = fileNames;
			m_SearchPattern = searchPattern;
			m_FindReplaceOptions = findReplaceOptions;
			m_FoundStringDel = foundStringDel;
            m_SearchBeginDel = searchBeginDel;
			m_SearchDoneDel = searchDoneDel;
			m_reloadFileDel = reloadFileDel;
			m_disableFileWatcherDel = disableFileWatcherDel;
			m_enableFileWatcherDel = enableFileWatcherDel;

			StopSearch();
		}
        #endregion

        #region Find Functions
        /// <summary>
        /// Finds the next occurrence of the specified text in the open document associated with the syntaxEditor
        /// </summary>
        /// <param name="syntaxEditor"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <returns>Error code.  The error strings will include more detailed information.</returns>
        public EFindNextReturnTypes FindNextInOpenDoc( SyntaxEditor syntaxEditor, FindReplaceOptions findReplaceOptions,
            ref string errorString1, ref string errorString2 )
        {
            if ( syntaxEditor == null )
            {
                errorString1 = "No Syntax Editor loaded";
                errorString2 = "Find/Replace Error";
                return EFindNextReturnTypes.E_ERROR;
            }            

            // Perform a find operation
            FindReplaceResultSet resultSet;
            try
            {
                resultSet = syntaxEditor.SelectedView.FindReplace.Find( findReplaceOptions );
            }
            catch ( Exception ex )
            {
                errorString1 = "An error occurred:\r\n" + ex.Message;
                errorString2 = "Find/Replace Error";
                return EFindNextReturnTypes.E_ERROR;
            }

            // Find if the search went past the starting point
            if ( resultSet.PastSearchStartOffset )
            {
                errorString1 = "Find reached the starting point of the search.";
                errorString2 = ApplicationSettings.ProductName;
                return EFindNextReturnTypes.E_PAST_START_OF_SEARCH;
            }

            // If no matches were found...			
            if ( resultSet.Count == 0 )
            {
                errorString1 = "The specified text was not found.";
                errorString2 = ApplicationSettings.ProductName;
                return EFindNextReturnTypes.E_NOT_FOUND;
            }

            if ( resultSet.PastDocumentEnd )
            {
                errorString1 = "Find reached end of document.";
                errorString2 = ApplicationSettings.ProductName;
                return EFindNextReturnTypes.E_PAST_EOF;
            }

            return EFindNextReturnTypes.E_NO_ERROR;
        }

        /// <summary>
        /// Finds the next occurrence of the specified text in all open documents associated with SyntaxEditors in the Dictionary
        /// </summary>
        /// <param name="openDocuments"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="currentSyntaxEditor"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <returns>Error code.  The error strings will include more detailed information.</returns>
        public EFindNextReturnTypes FindNextInOpenDocs( Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openDocuments, 
            FindReplaceOptions findReplaceOptions, ref SyntaxEditor currentSyntaxEditor,
            ref string errorString1, ref string errorString2, LoadFileDelegate loadFileDel )
        {
            if ( openDocuments.Count == 0 )
            {
                errorString1 = "No Syntax Editors loaded";
                errorString2 = "Find/Replace Error";
                return EFindNextReturnTypes.E_ERROR;
            }

            if ( !openDocuments.ContainsValue( currentSyntaxEditor ) )
            {
                if ( currentSyntaxEditor != null )
                {
                    errorString1 = "Could not determine active tab: " + currentSyntaxEditor.Document.Filename;
                }
                else
                {
                    errorString1 = "currentSyntaxEditor is null: ";
                }

                errorString2 = ApplicationSettings.ProductName;
                return EFindNextReturnTypes.E_ERROR;
            }

            // we want to start with the current document, then iterate through the rest in order
            int docIndex = 0;
            if ( currentSyntaxEditor != null )
            {
                IDictionaryEnumerator e = openDocuments.GetEnumerator();
                while ( e.MoveNext() )
                {
                    SyntaxEditor syntaxEditor = e.Value as SyntaxEditor;
                    if ( syntaxEditor == currentSyntaxEditor )
                    {
                        break;
                    }

                    ++docIndex;
                }
            }

            bool changedFile = false;
            findReplaceOptions.ChangeSelection = false;

            for ( int i = 0; i < openDocuments.Count; ++i )
            {
                // get the document we're searching from the hash table
                int j = docIndex;
                IDictionaryEnumerator e = openDocuments.GetEnumerator();
                while ( j >= 0 )
                {
                    e.MoveNext();
                    --j;
                }

                // get its syntax editor
                SyntaxEditor syntaxEditor = e.Value as SyntaxEditor;

                if ( changedFile )
                {
                    syntaxEditor.SelectedView.GoToLine( 0 );
                    syntaxEditor.SelectedView.FirstVisibleX = 0;
                }

                // first, see if our searchText is in the document
                EFindNextReturnTypes err = FindNextInOpenDoc( syntaxEditor, findReplaceOptions, ref errorString1, ref errorString2 );
                switch ( err )
                {
                case EFindNextReturnTypes.E_NO_ERROR:
                    {
                        // found it in a new document
                        if ( changedFile )
                        {
                            // make next doc the active one
                            if ( loadFileDel != null )
                            {
                                object[] args = new object[1] { syntaxEditor.Document.Filename };
                                m_parentControl.Invoke( loadFileDel, args );
                            }

                            currentSyntaxEditor = syntaxEditor;
                            changedFile = false;
                        }

                        // highlight the text that was found
                        findReplaceOptions.ChangeSelection = true;
                        FindNextInOpenDoc( syntaxEditor, findReplaceOptions, ref errorString1, ref errorString2 );
                        findReplaceOptions.ChangeSelection = false;
                    }
                    return err;

                case EFindNextReturnTypes.E_ERROR:
                    return err;

                case EFindNextReturnTypes.E_PAST_EOF:
                    {
                        // we still found one, so highlight the text that was found
                        findReplaceOptions.ChangeSelection = true;
                        FindNextInOpenDoc( syntaxEditor, findReplaceOptions, ref errorString1, ref errorString2 );
                        findReplaceOptions.ChangeSelection = false;
                    }
                    goto case EFindNextReturnTypes.E_NOT_FOUND;

                case EFindNextReturnTypes.E_PAST_START_OF_SEARCH:
                case EFindNextReturnTypes.E_NOT_FOUND:
                    {
                        // get ready to check the next document
                        ++docIndex;
                        if ( docIndex >= openDocuments.Count )
                        {
                            docIndex = 0;
                        }

                        changedFile = true;
                    }
                    break;

                default:
                    break;
                }
            }

            return EFindNextReturnTypes.E_NOT_FOUND;
        }

        /// <summary>
        /// Finds all occurrences of the specified text in the closed file using the helper function SearchInClosedDoc
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="theForm"></param>
        /// <param name="foundStringDel"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <param name="searchDoneDel">Null when this function is used for searching multiple documents</param>
        /// <returns>Returns E_NO_ERROR if match found, E_NOT_FOUND otherwise.</returns>
        public EFindNextReturnTypes FindAllInClosedDoc( string fileName, FindReplaceOptions findReplaceOptions,
            AddFoundStringDelegate foundStringDel, ref string errorString1, ref string errorString2,
            SearchCompleteDelegate searchDoneDel )
        {
            OnSearchBegin( false );

            EFindNextReturnTypes err = EFindNextReturnTypes.E_NO_ERROR;
            int numFound = SearchInClosedDoc( fileName, findReplaceOptions, foundStringDel, false );
            if ( numFound > 0 )
            {
                m_TotalFound += numFound;
                ++m_MatchingFiles;

                errorString1 = "" + numFound + " occurrence(s) found in " + fileName + ".";
                errorString2 = ApplicationSettings.ProductName;
            }
            else
            {
                errorString1 = "The specified text was not found.";
                errorString2 = ApplicationSettings.ProductName;
                err = EFindNextReturnTypes.E_NOT_FOUND;
            }

            OnSearchComplete( searchDoneDel, m_TotalFound, m_MatchingFiles, 1, err, errorString1, errorString2 );

            return err;
        }

        /// <summary>
        /// Tells us if the specified text can be found within the closed document using the helper function SearchInClosedDoc
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="findReplaceOptions"></param>
        /// <returns>True if match found, false otherwise.</returns>
        public bool IsInClosedDoc( string fileName, FindReplaceOptions findReplaceOptions )
        {
            int numFound = SearchInClosedDoc( fileName, findReplaceOptions, null, true );
            return (numFound > 0);
        }

        /// <summary>
        /// Helper function that searches for the specified text in the closed document and can call the AddFoundStringDelegate function
        /// for printing the results to the Output window
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="theForm"></param>
        /// <param name="foundStringDel"></param>
        /// <param name="rtnOnFirstFind"></param>
        /// <returns>Number of lines which contained a match.</returns>
        private int SearchInClosedDoc( string fileName, FindReplaceOptions findReplaceOptions,
            AddFoundStringDelegate foundStringDel, bool rtnOnFirstFind )
        {
            if ( findReplaceOptions.FindText.Length == 0 )
            {
                return 0;
            }

            m_StreamReader = null;
            try
            {
                FileInfo fInfo = new FileInfo( fileName );
                FileStream fStream = fInfo.OpenRead();
                m_StreamReader = new StreamReader( fStream );
            }
            catch ( Exception e )
            {
                ApplicationSettings.Log.ToolException( e, "Exception opening {0} for read", fileName );
                return 0;
            }

            int numFound = 0;
            if ( m_StreamReader != null )
            {
                string text = findReplaceOptions.FindText;
                if ( !findReplaceOptions.MatchCase )
                {
                    text = text.ToLower();
                }

                int lineNum = 1;
                string line = m_StreamReader.ReadLine();
                while ( line != null )
                {
                    int foundAtIndex = -1;

                    if ( !findReplaceOptions.MatchCase )
                    {
                        foundAtIndex = line.ToLower().IndexOf( text );
                    }
                    else
                    {
                        foundAtIndex = line.IndexOf( text );
                    }

                    if ( (foundAtIndex >= 0) && findReplaceOptions.MatchWholeWord )
                    {
                        // Check character before and after the substring.
                        // If either one is an alpha-numeric character, then don't count this as a match.

                        int indexBefore = foundAtIndex - 1;
                        int indexAfter = foundAtIndex + text.Length;

                        if ( ((indexBefore >= 0)
                            && ((char.IsLetterOrDigit( line, indexBefore ) || char.IsSymbol( line, indexBefore )) || (line[indexBefore] == '_')))
                            || ((indexAfter < line.Length)
                            && ((char.IsLetterOrDigit( line, indexAfter ) || char.IsSymbol( line, indexAfter )) || (line[indexAfter] == '_'))) )
                        {
                            foundAtIndex = -1;
                        }
                    }

                    if ( foundAtIndex >= 0 )
                    {
                        if ( rtnOnFirstFind )
                        {
                            return 1;
                        }

                        numFound++;
                        if ( foundStringDel != null )
                        {
                            m_parentControl.Invoke( foundStringDel, new object[] { fileName, lineNum, line } );
                        }
                    }
                    lineNum++;

                    line = m_StreamReader.ReadLine();
                }

                m_StreamReader.Close();
                m_StreamReader = null;
            }

            return numFound;
        }

        /// <summary>
        /// Finds all occurrences of the specified text in all the closed documents in the directory and their subdirectories
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="lastFile"></param>
        /// <param name="recurseSubdir"></param>
        /// <param name="searchPattern"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="theForm"></param>
        /// <param name="foundStringDel"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <param name="searchDoneDel"></param>
        /// <returns>Error code.  E_NO_ERROR on success.</returns>
        public EFindNextReturnTypes FindNextInDirectory( string dir, string lastFile, bool recurseSubdir, string searchPattern,
            FindReplaceOptions findReplaceOptions, ref string errorString1, ref string errorString2, SearchCompleteDelegate searchDoneDel )
        {
            EFindNextReturnTypes err = EFindNextReturnTypes.E_NO_ERROR;

            OnSearchBegin( false );

            try
            {
                Regex FileExtensionDelim = new Regex( ";" );
                string[] FileExtensions = FileExtensionDelim.Split( searchPattern );

                for ( int i = 0; i < FileExtensions.Length; i++ )
                {
                    FileExtensions[i] = FileExtensions[i].Trim();
                }

                string lastFileDir = lastFile;
                if ( (lastFile == "") || (lastFile.IndexOf( dir ) == -1) )
                {
                    // lastFile is either empty or not in the search directory
                    lastFile = "";
                    lastFileDir = dir;
                }
                else
                {
                    // remove file name from lastFileDir
                    int i = lastFileDir.LastIndexOf( '\\' );
                    lastFileDir = lastFileDir.Remove( i, lastFileDir.Length - i );
                }

                err = FindNextInDirectoryRecurse( dir, dir, lastFileDir, lastFile, recurseSubdir,
                    FileExtensions, findReplaceOptions, true, ref errorString1, ref errorString2 );

                // loop around to beginning
                if ( err == EFindNextReturnTypes.E_NOT_FOUND )
                {
                    err = FindNextInDirectoryRecurse( dir, dir, dir, "", recurseSubdir,
                        FileExtensions, findReplaceOptions, true, ref errorString1, ref errorString2 );
                }
            }
            catch ( Exception ex )
            {
                errorString1 = "An error occurred:\r\n" + ex.Message;
                errorString2 = "Find/Replace in Files Error";
                err = EFindNextReturnTypes.E_ERROR;
            }

            OnSearchComplete( searchDoneDel, 1, 1, m_TotalFilesSearched, err, errorString1, errorString2 );

            return err;
        }

        /// <summary>
        /// Finds all occurrences of the specified text in all the closed documents in the directory and their subdirectories
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="recurseSubdir"></param>
        /// <param name="searchPattern"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="theForm"></param>
        /// <param name="foundStringDel"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <param name="searchDoneDel"></param>
        /// <returns>Error code.  E_NO_ERROR on success.</returns>
        public EFindNextReturnTypes FindAllInDirectory( string dir, bool recurseSubdir, string searchPattern,
            FindReplaceOptions findReplaceOptions, AddFoundStringDelegate foundStringDel,
            ref string errorString1, ref string errorString2, SearchCompleteDelegate searchDoneDel )
        {
            EFindNextReturnTypes err = EFindNextReturnTypes.E_NO_ERROR;

            OnSearchBegin( false );

            try
            {
                Regex FileExtensionDelim = new Regex( ";" );
                string[] FileExtensions = FileExtensionDelim.Split( searchPattern );

                for ( int i = 0; i < FileExtensions.Length; i++ )
                {
                    FileExtensions[i] = FileExtensions[i].Trim();
                }

                err = FindAllInDirectoryRecurse( dir, dir, recurseSubdir, FileExtensions, findReplaceOptions,
                    foundStringDel, ref errorString1, ref errorString2 );
            }
            catch ( Exception ex )
            {
                errorString1 = "An error occurred:\r\n" + ex.Message;
                errorString2 = "Find/Replace in Files Error";
                err = EFindNextReturnTypes.E_ERROR;
            }

            OnSearchComplete( searchDoneDel, m_TotalFound, m_MatchingFiles, m_TotalFilesSearched, err, errorString1, errorString2 );

            return err;
        }

        /// <summary>
        /// Finds all occurrences of the specified text in the list of files.
        /// Updates m_TotalFilesSearched.
        /// </summary>
        /// <param name="files"></param>
        /// <param name="searchPattern"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="theForm"></param>
        /// <param name="foundStringDel"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <param name="searchDoneDel"></param>
        /// <returns>Error code.  E_NO_ERROR on success.</returns>
        public EFindNextReturnTypes FindAllInClosedDocs( List<string> files, string searchPattern, FindReplaceOptions findReplaceOptions,
            AddFoundStringDelegate foundStringDel, ref string errorString1, ref string errorString2,
            SearchCompleteDelegate searchDoneDel )
        {
            EFindNextReturnTypes err = EFindNextReturnTypes.E_NO_ERROR;

            OnSearchBegin( false );

            try
            {
                Regex FileExtensionDelim = new Regex( ";" );
                string[] FileExtensions = FileExtensionDelim.Split( searchPattern );

                for ( int i = 0; i < FileExtensions.Length; i++ )
                {
                    FileExtensions[i] = FileExtensions[i].Trim();
                }

                foreach ( string file in files )
                {
                    foreach ( string fileExtension in FileExtensions )
                    {
                        string ext = fileExtension.Remove( 0, 1 );	// remove asterisk
                        if ( (ext == ".*") || file.EndsWith( ext ) )
                        {
                            int startTicks = Environment.TickCount;

                            err = FindAllInClosedDoc( file, findReplaceOptions, foundStringDel,
                                ref errorString1, ref errorString2, null );

                            if ( err == EFindNextReturnTypes.E_ERROR )
                            {
                                break;
                            }

                            int endTicks = Environment.TickCount;
                            m_TotalTickCount += (endTicks - startTicks);

                            ++m_TotalFilesSearched;
                        }
                    }

                    if ( err == EFindNextReturnTypes.E_ERROR )
                    {
                        break;
                    }
                }
            }
            catch ( Exception ex )
            {
                errorString1 = "An error occurred:\r\n" + ex.Message;
                errorString2 = "Find/Replace in Files Error";
                err = EFindNextReturnTypes.E_ERROR;
            }

            OnSearchComplete( searchDoneDel, m_TotalFound, m_MatchingFiles, m_TotalFilesSearched, err, errorString1, errorString2 );

            return err;
        }

        /// <summary>
        /// Recursive function for searching all the files in a directory for the next file that contains the search text.
        /// </summary>
        /// <param name="RootDir"></param>
        /// <param name="dir"></param>
        /// <param name="lastFileDir"></param>
        /// <param name="lastFile"></param>
        /// <param name="recurseSubdir"></param>
        /// <param name="FileExtensions"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="theForm"></param>
        /// <param name="foundStringDel"></param>
        /// <param name="errorString1">Will contain the file name with the next match.</param>
        /// <param name="errorString2"></param>
        /// <returns>Error code.  E_NO_ERROR on success.</returns>
        private EFindNextReturnTypes FindNextInDirectoryRecurse( string RootDir, string dir, string lastFileDir, string lastFile,
            bool recurseSubdir, string[] FileExtensions, FindReplaceOptions findReplaceOptions,
            bool diveToLastFile, ref string errorString1, ref string errorString2 )
        {
            // we're still searching for our lastFileDir, or we've moved on
            if ( ((dir == lastFileDir) && diveToLastFile) || !diveToLastFile )
            {
                try
                {
                    bool searchFiles = (lastFile.Length <= 0) || !diveToLastFile;

                    foreach ( string FileExtension in FileExtensions )
                    {
                        foreach ( string file in Directory.GetFiles( dir, FileExtension ) )
                        {
                            if ( searchFiles )
                            {
                                ++m_TotalFilesSearched;

                                if ( IsInClosedDoc( file, findReplaceOptions ) )
                                {
                                    errorString1 = file;
                                    return EFindNextReturnTypes.E_NO_ERROR;
                                }
                            }

                            if ( file == lastFile )
                            {
                                searchFiles = true;
                                diveToLastFile = false;
                            }
                        }
                    }
                }
                catch ( Exception ex )
                {
                    errorString1 = "An error occurred:\r\n" + ex.Message;
                    errorString2 = "Find/Replace in Files Error";
                    return EFindNextReturnTypes.E_ERROR;
                }
            }

            if ( !recurseSubdir && !diveToLastFile )
            {
                // don't look in sub-folders
                return EFindNextReturnTypes.E_NO_ERROR;
            }

            EFindNextReturnTypes err = EFindNextReturnTypes.E_NOT_FOUND;
            errorString1 = "The specified text was not found.";
            errorString2 = ApplicationSettings.ProductName;

            // Recursively look through all the files in the
            // current directory's subdirectories.
            try
            {
                foreach ( string dirIter in Directory.GetDirectories( dir ) )
                {
                    // search sub-folders if we're diving to last file and the lastFile is in the folder dirIter
                    // or if we're not diving and we need to search sub-folders
                    if ( (diveToLastFile && (lastFileDir.IndexOf( dirIter ) != -1))
                        || (!diveToLastFile && recurseSubdir) )
                    {
                        err = FindNextInDirectoryRecurse( RootDir, dirIter, lastFileDir, lastFile,
                            recurseSubdir, FileExtensions, findReplaceOptions, diveToLastFile, ref errorString1, ref errorString2 );

                        switch ( err )
                        {
                        case EFindNextReturnTypes.E_NO_ERROR:
                        case EFindNextReturnTypes.E_ERROR:
                            return err;

                        default:
                            break;
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                errorString1 = "An error occurred:\r\n" + ex.Message;
                errorString2 = "Find/Replace Error";
                return EFindNextReturnTypes.E_ERROR;
            }

            return err;
        }

        /// <summary>
        /// Recursive function for searching all the files in a directory for the specified text.
        /// Updates m_TotalFilesSearched
        /// </summary>
        /// <param name="RootDir"></param>
        /// <param name="dir"></param>
        /// <param name="recurseSubdir"></param>
        /// <param name="FileExtensions"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="theForm"></param>
        /// <param name="foundStringDel"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <returns>Error code.  E_NO_ERROR on success.</returns>
        private EFindNextReturnTypes FindAllInDirectoryRecurse( string RootDir, string dir, bool recurseSubdir, string[] FileExtensions,
            FindReplaceOptions findReplaceOptions, AddFoundStringDelegate foundStringDel,
            ref string errorString1, ref string errorString2 )
        {
            try
            {
                foreach ( string FileExtension in FileExtensions )
                {
                    foreach ( string file in Directory.GetFiles( dir, FileExtension ) )
                    {
                        int startTicks = Environment.TickCount;

                        EFindNextReturnTypes err = FindAllInClosedDoc( file, findReplaceOptions, foundStringDel,
                            ref errorString1, ref errorString2, null );

                        if ( err == EFindNextReturnTypes.E_ERROR )
                        {
                            return err;
                        }

                        int endTicks = Environment.TickCount;
                        m_TotalTickCount += (endTicks - startTicks);

                        ++m_TotalFilesSearched;
                    }
                }
            }
            catch ( Exception ex )
            {
                errorString1 = "An error occurred:\r\n" + ex.Message;
                errorString2 = "Find/Replace in Files Error";
                return EFindNextReturnTypes.E_ERROR;
            }

            if ( !recurseSubdir )
            {
                // don't look in sub-folders
                return EFindNextReturnTypes.E_NO_ERROR;
            }

            // Recursively add all the files in the
            // current directory's subdirectories.
            try
            {
                foreach ( string dirIter in Directory.GetDirectories( dir ) )
                {
                    EFindNextReturnTypes err = FindAllInDirectoryRecurse( RootDir, dirIter, recurseSubdir, FileExtensions,
                        findReplaceOptions, foundStringDel, ref errorString1, ref errorString2 );

                    if ( err == EFindNextReturnTypes.E_ERROR )
                    {
                        return err;
                    }
                }
            }
            catch ( Exception ex )
            {
                errorString1 = "An error occurred:\r\n" + ex.Message;
                errorString2 = "Find/Replace Error";
                return EFindNextReturnTypes.E_ERROR;
            }

            return EFindNextReturnTypes.E_NO_ERROR;
        }
        #endregion

        #region Replace Functions
        /// <summary>
        /// Finds the next occurrence of the specified text in the open document associated with the syntaxEditor, and replaces it
        /// </summary>
        /// <param name="syntaxEditor"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <param name="okToReplaceInOpenDocDel"></param>
        /// <param name="multipleDocs"></param>
        /// <returns>Error code. Ref strings will contain more info.</returns>
        public EFindNextReturnTypes ReplaceInOpenDoc( SyntaxEditor syntaxEditor, FindReplaceOptions findReplaceOptions,
            ref string errorString1, ref string errorString2, bool multipleDocs )
        {
            if ( syntaxEditor == null )
            {
                errorString1 = "No Syntax Editor loaded";
                errorString2 = "Find/Replace Error";
                return EFindNextReturnTypes.E_ERROR;
            }

            // Perform a find operation
            FindReplaceResultSet resultSet;
            try
            {
                resultSet = syntaxEditor.SelectedView.FindReplace.Replace( findReplaceOptions );
            }
            catch ( Exception ex )
            {
                errorString1 = "An error occurred:\r\n" + ex.Message;
                errorString2 = "Find/Replace Error";
                return EFindNextReturnTypes.E_ERROR;
            }

            if ( resultSet.PastSearchStartOffset )
            {
                errorString1 = "Find and Replace reached the starting point of the search.";
                errorString2 = ApplicationSettings.ProductName;
                return EFindNextReturnTypes.E_PAST_START_OF_SEARCH;
            }

            // If no matches were found...			
            if ( resultSet.Count == 0 )
            {
                errorString1 = "The specified text was not found.";
                errorString2 = ApplicationSettings.ProductName;
                return EFindNextReturnTypes.E_NOT_FOUND;
            }

            if ( resultSet.PastDocumentEnd )
            {
                errorString1 = "Find and Replace reached end of document.";
                errorString2 = ApplicationSettings.ProductName;
                return EFindNextReturnTypes.E_PAST_EOF;
            }

            if ( resultSet.ReplaceOccurred && !OnOkToReplaceInOpenDoc( syntaxEditor.Document.Filename, multipleDocs ) )
            {
                // if we're not ok to do the replace, we must Undo the replacement
                syntaxEditor.Document.UndoRedo.Undo();

                errorString1 = "Unable to check out file from source control.";
                errorString2 = ApplicationSettings.ProductName;
                return EFindNextReturnTypes.E_NOT_FOUND;
            }

            return EFindNextReturnTypes.E_NO_ERROR;
        }

        /// <summary>
        /// Finds the next occurrence of the specified text in all open documents associated with a syntaxEditor in the Dictionary, and replaces it
        /// </summary>
        /// <param name="openDocuments"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="currentSyntaxEditor"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <returns>Error code.  The error strings will include more detailed information.</returns>
        public EFindNextReturnTypes ReplaceInOpenDocs( Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openDocuments, 
            FindReplaceOptions findReplaceOptions, ref SyntaxEditor currentSyntaxEditor,
            ref string errorString1, ref string errorString2 )
        {
            if ( openDocuments.Count == 0 )
            {
                errorString1 = "No Syntax Editors loaded";
                errorString2 = "Find/Replace Error";
                return EFindNextReturnTypes.E_ERROR;
            }

            if ( !openDocuments.ContainsValue( currentSyntaxEditor ) )
            {
                if ( currentSyntaxEditor != null )
                {
                    errorString1 = "Could not determine active tab: " + currentSyntaxEditor.Document.Filename;
                }
                else
                {
                    errorString1 = "currentSyntaxEditor is null: ";
                }

                errorString2 = ApplicationSettings.ProductName;
                return EFindNextReturnTypes.E_ERROR;
            }

            // we want to start with the current document, then iterate through the rest in order
            int docIndex = 0;
            if ( currentSyntaxEditor != null )
            {
                IDictionaryEnumerator e = openDocuments.GetEnumerator();
                while ( e.MoveNext() )
                {
                    SyntaxEditor syntaxEditor = e.Value as SyntaxEditor;
                    if ( syntaxEditor == currentSyntaxEditor )
                    {
                        break;
                    }

                    ++docIndex;
                }
            }

            bool changedFile = false;
            findReplaceOptions.ChangeSelection = false;

            for ( int i = 0; i < openDocuments.Count; ++i )
            {
                // get the document we're searching from the hash table
                int j = docIndex;
                IDictionaryEnumerator e = openDocuments.GetEnumerator();
                while ( j >= 0 )
                {
                    e.MoveNext();
                    --j;
                }

                // get its syntax editor
                SyntaxEditor syntaxEditor = e.Value as SyntaxEditor;

                if ( changedFile )
                {
                    syntaxEditor.SelectedView.GoToLine( 0 );
                    syntaxEditor.SelectedView.FirstVisibleX = 0;
                }

                // first, see if our searchText is in the document
                EFindNextReturnTypes err = FindNextInOpenDoc( syntaxEditor, findReplaceOptions, ref errorString1, ref errorString2 );
                switch ( err )
                {
                case EFindNextReturnTypes.E_NO_ERROR:
                    {
                        // found it in a new document
                        if ( changedFile )
                        {
                            // make next doc the active one
                            TD.SandDock.DockControl dockPage = e.Key as TD.SandDock.DockControl;
                            dockPage.Open();
                            dockPage.BringToFront();

                            currentSyntaxEditor = syntaxEditor;
                            changedFile = false;
                        }

                        // replace/highlight the text that was found
                        findReplaceOptions.ChangeSelection = true;
                        ReplaceInOpenDoc( syntaxEditor, findReplaceOptions, ref errorString1, ref errorString2, false );
                        findReplaceOptions.ChangeSelection = false;
                    }
                    return err;

                case EFindNextReturnTypes.E_ERROR:
                    return err;

                case EFindNextReturnTypes.E_PAST_EOF:
                    {
                        // we still found one, so replace/highlight the text that was found
                        findReplaceOptions.ChangeSelection = true;
                        ReplaceInOpenDoc( syntaxEditor, findReplaceOptions, ref errorString1, ref errorString2, false );
                        findReplaceOptions.ChangeSelection = false;
                    }
                    goto case EFindNextReturnTypes.E_NOT_FOUND;

                case EFindNextReturnTypes.E_PAST_START_OF_SEARCH:
                case EFindNextReturnTypes.E_NOT_FOUND:
                    {
                        // get ready to check the next document
                        ++docIndex;
                        if ( docIndex >= openDocuments.Count )
                        {
                            docIndex = 0;
                        }

                        changedFile = true;
                    }
                    break;

                default:
                    break;
                }
            }

            return EFindNextReturnTypes.E_NOT_FOUND;
        }

        /// <summary>
        /// Finds all occurrences of the specified text in the open document associated with the syntaxEditor, and replaces them
        /// </summary>
        /// <param name="syntaxEditor"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <param name="okToReplaceInOpenDocDel"></param>
        /// <param name="multipleDocs"></param>
        /// <returns>Error code</returns>
        public EFindNextReturnTypes ReplaceAllInOpenDoc( SyntaxEditor syntaxEditor, FindReplaceOptions findReplaceOptions,
            ref string errorString1, ref string errorString2, bool multipleDocs )
        {
            if ( syntaxEditor == null )
            {
                errorString1 = "No Syntax Editor loaded";
                errorString2 = "Find/Replace Error";
                return EFindNextReturnTypes.E_ERROR;
            }

            findReplaceOptions.ChangeSelection = true;

            // Perform a mark all operation
            FindReplaceResultSet resultSet;
            try
            {
                resultSet = syntaxEditor.SelectedView.FindReplace.ReplaceAll( findReplaceOptions );
            }
            catch ( Exception ex )
            {
                errorString1 = "An error occurred:\r\n" + ex.Message;
                errorString2 = "Find/Replace Error";
                return EFindNextReturnTypes.E_ERROR;
            }

            // If no matches were found...
            if ( resultSet.Count == 0 )
            {
                errorString1 = "The specified text was not found.";
                errorString2 = ApplicationSettings.ProductName;
                return EFindNextReturnTypes.E_NOT_FOUND;
            }

            if ( resultSet.ReplaceOccurred && !OnOkToReplaceInOpenDoc( syntaxEditor.Document.Filename, multipleDocs ) )
            {
                // if we're not ok to do the replace, we must Undo the replacement
                syntaxEditor.Document.UndoRedo.Undo();

                errorString1 = "Unable to check out file from source control.";
                errorString2 = ApplicationSettings.ProductName;
                return EFindNextReturnTypes.E_NOT_FOUND;
            }

            // Display the number of replacements
            errorString1 = "" + resultSet.Count + " occurrence(s) replaced.";
            errorString2 = ApplicationSettings.ProductName;
            return EFindNextReturnTypes.E_NO_ERROR;
        }

        /// <summary>
        /// Finds all occurrences of the specified text in all open documents associated with a syntaxEditor in the Dictionary, and replaces them
        /// </summary>
        /// <param name="openDocuments"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <param name="okToReplaceInOpenDocDel"></param>
        /// <returns>Error code</returns>
        public EFindNextReturnTypes ReplaceAllInOpenDocs( Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openDocuments, 
            FindReplaceOptions findReplaceOptions, ref string errorString1, ref string errorString2 )
        {
            if ( openDocuments.Count == 0 )
            {
                errorString1 = "No Syntax Editors loaded";
                errorString2 = "Find/Replace Error";
                return EFindNextReturnTypes.E_ERROR;
            }

            int replaceCount = 0;
            int fileCount = 0;
            IDictionaryEnumerator e = openDocuments.GetEnumerator();
            while ( e.MoveNext() )
            {
                SyntaxEditor syntaxEditor = e.Value as SyntaxEditor;
                EFindNextReturnTypes err = ReplaceAllInOpenDoc( syntaxEditor, findReplaceOptions, ref errorString1, ref errorString2, true );
                switch ( err )
                {
                case EFindNextReturnTypes.E_ERROR:
                    return err;

                case EFindNextReturnTypes.E_NO_ERROR:
                    {
                        // strip off first "word" which is the count of how many were replaced
                        string[] split = errorString1.Split( new char[] { ' ' } );
                        replaceCount += System.Convert.ToInt32( split[0] );
                        ++fileCount;
                    }
                    break;

                default:
                    break;
                }
            }

            errorString1 = "" + replaceCount + " occurrence(s) in " + fileCount + "file(s) replaced.";
            errorString2 = ApplicationSettings.ProductName;
            return EFindNextReturnTypes.E_NO_ERROR;
        }

        /// <summary>
        /// Searches for all occurrences of the specified text in all closed documents in the directory and replaces them
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="searchPattern"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="theForm"></param>
        /// <param name="foundStringDel"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <param name="searchDoneDel"></param>
        /// <returns></returns>
        public EFindNextReturnTypes ReplaceAllInDirectory( string dir, bool recurseSubdir, string searchPattern,
            FindReplaceOptions findReplaceOptions, AddFoundStringDelegate foundStringDel,
            ref string errorString1, ref string errorString2, SearchCompleteDelegate searchDoneDel )
        {
            EFindNextReturnTypes err = EFindNextReturnTypes.E_NO_ERROR;

            OnSearchBegin( true );

            try
            {
                Regex FileExtensionDelim = new Regex( ";" );
                string[] FileExtensions = FileExtensionDelim.Split( searchPattern );

                for ( int i = 0; i < FileExtensions.Length; i++ )
                {
                    FileExtensions[i] = FileExtensions[i].Trim();
                }

                err = ReplaceAllInDirectoryRecurse( dir, dir, recurseSubdir, FileExtensions, findReplaceOptions,
                    foundStringDel, ref errorString1, ref errorString2 );
            }
            catch ( Exception ex )
            {
                errorString1 = "An error occurred:\r\n" + ex.Message;
                errorString2 = "Find/Replace in Files Error";
                err = EFindNextReturnTypes.E_ERROR;
            }

            OnSearchComplete( searchDoneDel, m_TotalFound, m_MatchingFiles, m_TotalFilesSearched, err, errorString1, errorString2 );

            return err;
        }

        /// <summary>
        /// Finds all occurrences of the specified text in the closed file using the helper function SearchInClosedDoc and replaces it
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="theForm"></param>
        /// <param name="foundStringDel"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <param name="searchDoneDel">Null when this function is used for searching multiple documents</param>
        /// <returns>Returns E_NO_ERROR if match found, E_NOT_FOUND otherwise.</returns>
        public EFindNextReturnTypes ReplaceAllInClosedDoc( string fileName, FindReplaceOptions findReplaceOptions,
            AddFoundStringDelegate foundStringDel, ref string errorString1, ref string errorString2,
            SearchCompleteDelegate searchDoneDel, bool multipleDocs )
        {
            OnSearchBegin( true );

            EFindNextReturnTypes err = EFindNextReturnTypes.E_NO_ERROR;
            int numFound = ReplaceInClosedDoc( fileName, findReplaceOptions, foundStringDel, multipleDocs );
            if ( numFound > 0 )
            {
                m_TotalFound += numFound;
                ++m_MatchingFiles;

                errorString1 = "" + numFound + " occurrence(s) replaced in " + fileName + ".";
                errorString2 = ApplicationSettings.ProductName;
            }
            else
            {
                errorString1 = "The specified text was not found.";
                errorString2 = ApplicationSettings.ProductName;
                err = EFindNextReturnTypes.E_NOT_FOUND;
            }

            OnSearchComplete( searchDoneDel, m_TotalFound, m_MatchingFiles, 1, err, errorString1, errorString2 );

            return err;
        }

        /// <summary>
        /// Helper function that searches for the specified text in the closed document, replacing them, and can call 
        /// the AddFoundStringDelegate function for printing the results to the Output window.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="theForm"></param>
        /// <param name="foundStringDel"></param>
        /// <returns>Number of lines which contained a match.</returns>
        private int ReplaceInClosedDoc( string fileName, FindReplaceOptions findReplaceOptions,
            AddFoundStringDelegate foundStringDel, bool multipleDocs )
        {
            if ( findReplaceOptions.FindText.Length == 0 )
            {
                return 0;
            }

            m_StreamReader = null;
            try
            {
                FileInfo readerfInfo = new FileInfo( fileName );
                FileStream readerfStream = readerfInfo.OpenRead();
                m_StreamReader = new StreamReader( readerfStream );
            }
            catch ( Exception e )
            {
                ApplicationSettings.Log.ToolException( e, "Exception opening {0} for read", fileName );
                return 0;
            }

            m_StreamWriter = null;
            m_WriterFileInfo = new FileInfo( "c:\\temp_file_search.txt" );
            m_WriterFileInfo.Delete();
            try
            {
                FileStream writerfStream = m_WriterFileInfo.OpenWrite();
                m_StreamWriter = new StreamWriter( writerfStream );
            }
            catch ( Exception e )
            {
                ApplicationSettings.Log.ToolException( e, "Exception opening {0} for write", m_WriterFileInfo.FullName);
                return 0;
            }

            int numFound = 0;
            if ( (m_StreamReader != null) && (m_StreamWriter != null) )
            {
                string text = findReplaceOptions.FindText;
                if ( !findReplaceOptions.MatchCase )
                {
                    text = text.ToLower();
                }

                int lineNum = 1;
                string line = m_StreamReader.ReadLine();
                while ( line != null )
                {
                    int foundAtIndex = -1;

                    if ( !findReplaceOptions.MatchCase )
                    {
                        foundAtIndex = line.ToLower().IndexOf( text );
                    }
                    else
                    {
                        foundAtIndex = line.IndexOf( text );
                    }

                    if ( (foundAtIndex >= 0) && findReplaceOptions.MatchWholeWord )
                    {
                        // Check character before and after the substring.
                        // If either one is an alpha-numeric character, then don't count this as a match.

                        int indexBefore = foundAtIndex - 1;
                        int indexAfter = foundAtIndex + text.Length;

                        if ( ((indexBefore >= 0)
                            && (char.IsLetterOrDigit( line, indexBefore ) || char.IsSymbol( line, indexBefore ) || (line[indexBefore] == '_')))
                            || ((indexAfter < line.Length)
                            && (char.IsLetterOrDigit( line, indexAfter ) || char.IsSymbol( line, indexAfter ) || (line[indexAfter] == '_'))) )
                        {
                            foundAtIndex = -1;
                        }
                    }

                    if ( foundAtIndex >= 0 )
                    {
                        if ( !OnOkToReplaceInClosedDoc( fileName, multipleDocs ) )
                        {
                            break;
                        }

                        numFound++;

                        // replace all instances of the search text
                        int lastFoundAtIndex = -1;
                        while ( (foundAtIndex > lastFoundAtIndex) && (foundAtIndex >= 0) )
                        {
                            line = line.Remove( foundAtIndex, text.Length );
                            line = line.Insert( foundAtIndex, findReplaceOptions.ReplaceText );

                            lastFoundAtIndex = foundAtIndex;

                            int startIndex = foundAtIndex + findReplaceOptions.ReplaceText.Length;
                            if ( startIndex < line.Length )
                            {
                                if ( !findReplaceOptions.MatchCase )
                                {
                                    foundAtIndex = line.ToLower().IndexOf( text, startIndex );
                                }
                                else
                                {
                                    foundAtIndex = line.IndexOf( text, startIndex );
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        if ( foundStringDel != null )
                        {
                            m_parentControl.Invoke( foundStringDel, new object[] { fileName, lineNum, line } );
                        }

                        m_StreamWriter.WriteLine( line );
                    }
                    else
                    {
                        m_StreamWriter.WriteLine( line );
                    }

                    lineNum++;

                    line = m_StreamReader.ReadLine();
                }

                m_StreamReader.Close();
                m_StreamReader = null;

                m_StreamWriter.Flush();
                m_StreamWriter.Close();
                m_StreamWriter = null;

                if ( numFound > 0 )
                {
                    if ( m_disableFileWatcherDel != null )
                    {
                        object[] args = new object[1] { fileName };
                        m_parentControl.Invoke( m_disableFileWatcherDel, args );
                    }

                    m_WriterFileInfo.CopyTo( fileName, true );

                    if ( m_reloadFileDel != null )
                    {
                        object[] args = new object[1] { fileName };
                        m_parentControl.Invoke( m_reloadFileDel, args );
                    }

                    if ( m_enableFileWatcherDel != null )
                    {
                        object[] args = new object[1] { fileName };
                        m_parentControl.Invoke( m_enableFileWatcherDel, args );
                    }
                }

                m_WriterFileInfo.Delete();
                m_WriterFileInfo = null;
            }

            return numFound;
        }

        /// <summary>
        /// Searches for all occurrences of the specified text in the list of closed documents and replaces them
        /// </summary>
        /// <param name="files"></param>
        /// <param name="searchPattern"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="theForm"></param>
        /// <param name="foundStringDel"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <param name="searchDoneDel"></param>
        /// <returns></returns>
        public EFindNextReturnTypes ReplaceAllInClosedDocs( List<string> files, string searchPattern, FindReplaceOptions findReplaceOptions,
            AddFoundStringDelegate foundStringDel, ref string errorString1, ref string errorString2,
            SearchCompleteDelegate searchDoneDel )
        {
            EFindNextReturnTypes err = EFindNextReturnTypes.E_NO_ERROR;

            OnSearchBegin( true );

            try
            {
                Regex FileExtensionDelim = new Regex( ";" );
                string[] extensions = FileExtensionDelim.Split( searchPattern.ToLower() );

                for ( int i = 0; i < extensions.Length; i++ )
                {
                    extensions[i] = extensions[i].Trim();
                    extensions[i] = extensions[i].Remove( 0, 1 );	// remove asterisk
                }

                foreach ( string file in files )
                {
                    bool checkFile = false;
                    string lowerFile = file.ToLower();
                    foreach ( string ext in extensions )
                    {
                        if ( (ext == ".*") || lowerFile.EndsWith( ext ) )
                        {
                            checkFile = true;
                            break;
                        }
                    }

                    if ( checkFile )
                    {
                        int startTicks = Environment.TickCount;

                        err = ReplaceAllInClosedDoc( file, findReplaceOptions, foundStringDel,
                            ref errorString1, ref errorString2, null, true );

                        if ( err == EFindNextReturnTypes.E_ERROR )
                        {
                            break;
                        }

                        int endTicks = Environment.TickCount;
                        m_TotalTickCount += (endTicks - startTicks);

                        ++m_TotalFilesSearched;
                    }

                    if ( err == EFindNextReturnTypes.E_ERROR )
                    {
                        break;
                    }
                }
            }
            catch ( Exception ex )
            {
                errorString1 = "An error occurred:\r\n" + ex.Message;
                errorString2 = "Find/Replace in Files Error";
                err = EFindNextReturnTypes.E_ERROR;
            }

            OnSearchComplete( searchDoneDel, m_TotalFound, m_MatchingFiles, m_TotalFilesSearched, err, errorString1, errorString2 );

            return err;
        }

        /// <summary>
        /// Recursive function for searching all the files in a directory for the specified text and replacing it.
        /// Updates m_TotalFilesSearched
        /// </summary>
        /// <param name="RootDir"></param>
        /// <param name="dir"></param>
        /// <param name="recurseSubdir"></param>
        /// <param name="FileExtensions"></param>
        /// <param name="findReplaceOptions"></param>
        /// <param name="theForm"></param>
        /// <param name="foundStringDel"></param>
        /// <param name="errorString1"></param>
        /// <param name="errorString2"></param>
        /// <returns>Error code.  E_NO_ERROR on success.</returns>
        private EFindNextReturnTypes ReplaceAllInDirectoryRecurse( string RootDir, string dir, bool recurseSubdir, string[] FileExtensions,
            FindReplaceOptions findReplaceOptions, AddFoundStringDelegate foundStringDel,
            ref string errorString1, ref string errorString2 )
        {
            try
            {
                foreach ( string FileExtension in FileExtensions )
                {
                    foreach ( string file in Directory.GetFiles( dir, FileExtension ) )
                    {
                        int startTicks = Environment.TickCount;

                        EFindNextReturnTypes err = ReplaceAllInClosedDoc( file, findReplaceOptions, foundStringDel,
                            ref errorString1, ref errorString2, null, true );

                        if ( err == EFindNextReturnTypes.E_ERROR )
                        {
                            return err;
                        }

                        int endTicks = Environment.TickCount;
                        m_TotalTickCount += (endTicks - startTicks);

                        ++m_TotalFilesSearched;
                    }
                }
            }
            catch ( Exception ex )
            {
                errorString1 = "An error occurred:\r\n" + ex.Message;
                errorString2 = "Find/Replace in Files Error";
                return EFindNextReturnTypes.E_ERROR;
            }

            if ( !recurseSubdir )
            {
                // don't look in sub-folders
                return EFindNextReturnTypes.E_NO_ERROR;
            }

            // Recursively add all the files in the
            // current directory's subdirectories.
            try
            {
                foreach ( string dirIter in Directory.GetDirectories( dir ) )
                {
                    EFindNextReturnTypes err = ReplaceAllInDirectoryRecurse( RootDir, dirIter, recurseSubdir, FileExtensions,
                        findReplaceOptions, foundStringDel, ref errorString1, ref errorString2 );

                    if ( err == EFindNextReturnTypes.E_ERROR )
                    {
                        return err;
                    }
                }
            }
            catch ( Exception ex )
            {
                errorString1 = "An error occurred:\r\n" + ex.Message;
                errorString2 = "Find/Replace Error";
                return EFindNextReturnTypes.E_ERROR;
            }

            return EFindNextReturnTypes.E_NO_ERROR;
        }
        #endregion

        #region Mark Functions
        /// <summary>
		/// Finds all occurrences of the specified text in the open document associated with the SyntaxEditor, and marks the lines they are on
		/// </summary>
		/// <param name="syntaxEditor"></param>
		/// <param name="findReplaceOptions"></param>
		/// <param name="markWith"></param>
		/// <param name="errorString1"></param>
		/// <param name="errorString2"></param>
		/// <returns>Error code.  The error strings will include more detailed information.</returns>
		public EFindNextReturnTypes MarkAllInOpenDoc( SyntaxEditor syntaxEditor, FindReplaceOptions findReplaceOptions, bool markWith,
			ref string errorString1, ref string errorString2 )
		{
            if ( syntaxEditor == null )
            {
                errorString1 = "No Syntax Editor loaded";
                errorString2 = "Find/Replace Error";
                return EFindNextReturnTypes.E_ERROR;
            }

			// Perform a find operation
			FindReplaceResultSet resultSet;
			try 
			{
				if ( markWith )
				{
					resultSet = syntaxEditor.SelectedView.FindReplace.MarkAll( findReplaceOptions );
				}
				else
				{
					resultSet = syntaxEditor.SelectedView.FindReplace.MarkAll( findReplaceOptions, typeof(GrammarErrorSpanIndicator) );
				}
			}
			catch ( Exception ex ) 
			{
				errorString1 = "An error occurred:\r\n" + ex.Message;
				errorString2 = "Find/Replace Error";
				return EFindNextReturnTypes.E_ERROR;
			}

			if ( resultSet.PastSearchStartOffset )
			{
				errorString1 = "Find and Replace reached the starting point of the search.";
				errorString2 = ApplicationSettings.ProductName;
				return EFindNextReturnTypes.E_PAST_START_OF_SEARCH;
			}

			// If no matches were found...			
			if ( resultSet.Count == 0 )
			{
				errorString1 = "The specified text was not found.";
				errorString2 = ApplicationSettings.ProductName;
				return EFindNextReturnTypes.E_NOT_FOUND;
			}

			errorString1 = "" + resultSet.Count + " occurrence(s) marked.";
			errorString2 = ApplicationSettings.ProductName;
			return EFindNextReturnTypes.E_NO_ERROR;
		}

		/// <summary>
		/// Finds all occurrences of the specified text in all open documents associated with a SyntaxEditor in the Dictionary, and marks the lines they are on
		/// </summary>
		/// <param name="openDocuments"></param>
		/// <param name="findReplaceOptions"></param>
		/// <param name="markWith"></param>
		/// <param name="errorString1"></param>
		/// <param name="errorString2"></param>
		/// <returns>Error code.  The error strings will include more detailed information.</returns>
        public EFindNextReturnTypes MarkAllInOpenDocs( Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openDocuments, 
            FindReplaceOptions findReplaceOptions, bool markWith, ref string errorString1, ref string errorString2 )
		{
            if ( openDocuments.Count == 0 )
            {
                errorString1 = "No Syntax Editors loaded";
                errorString2 = "Find/Replace Error";
                return EFindNextReturnTypes.E_ERROR;
            }

			int replaceCount = 0;
			int fileCount = 0;
			IDictionaryEnumerator e = openDocuments.GetEnumerator();
			while ( e.MoveNext() )
			{
				SyntaxEditor syntaxEditor = e.Value as SyntaxEditor;
				EFindNextReturnTypes err = MarkAllInOpenDoc( syntaxEditor, findReplaceOptions, markWith, ref errorString1, ref errorString2 );
				switch ( err )
				{
				case EFindNextReturnTypes.E_ERROR:
					return err;

				case EFindNextReturnTypes.E_NO_ERROR:
					{
						// strip off first "word" which is the count of how many were replaced
						string []split = errorString1.Split( new char[]{' '} );
						replaceCount += System.Convert.ToInt32( split[0] );
						++fileCount;
					}
					break;

				default:
					break;
				}
			}

			errorString1 = "" + replaceCount + " occurrence(s) in " + fileCount + "file(s) marked.";
			errorString2 = ApplicationSettings.ProductName;
			return EFindNextReturnTypes.E_NO_ERROR;
		}
        #endregion

        #region Threading Functions
        /// <summary>
        /// Thread function for directory searches
        /// </summary>
        public void DirectorySearch()
        {
            string errorString1 = "";
            string errorString2 = "";

            EFindNextReturnTypes err = FindAllInDirectory( m_Dir, m_RecurseSubdir, m_SearchPattern, m_FindReplaceOptions,
                m_FoundStringDel, ref errorString1, ref errorString2, m_SearchDoneDel );
        }

        public void DirectorySearchNext()
        {
            string errorString1 = "";
            string errorString2 = "";

            EFindNextReturnTypes err = FindNextInDirectory( m_Dir, m_LastFile, m_RecurseSubdir, m_SearchPattern, m_FindReplaceOptions,
                ref errorString1, ref errorString2, m_SearchDoneDel );
        }

        /// <summary>
        /// Thread function for closed document searches
        /// </summary>
        public void ClosedDocSearch()
        {
            string errorString1 = "";
            string errorString2 = "";

            EFindNextReturnTypes err = FindAllInClosedDoc( m_FileName, m_FindReplaceOptions,
                m_FoundStringDel, ref errorString1, ref errorString2, m_SearchDoneDel );
        }

        /// <summary>
        /// Thread function for closed documents searches
        /// </summary>
        public void ClosedDocsSearch()
        {
            string errorString1 = "";
            string errorString2 = "";

            EFindNextReturnTypes err = FindAllInClosedDocs( m_FileNames, m_SearchPattern, m_FindReplaceOptions,
                m_FoundStringDel, ref errorString1, ref errorString2, m_SearchDoneDel );
        }

        /// <summary>
        /// Thread function for directory search and replace
        /// </summary>
        public void DirectorySearchReplace()
        {
            string errorString1 = "";
            string errorString2 = "";

            EFindNextReturnTypes err = ReplaceAllInDirectory( m_Dir, m_RecurseSubdir, m_SearchPattern, m_FindReplaceOptions,
                m_FoundStringDel, ref errorString1, ref errorString2, m_SearchDoneDel );
        }

        /// <summary>
        /// Thread function for closed document search and replace
        /// </summary>
        public void ClosedDocSearchReplace()
        {
            string errorString1 = "";
            string errorString2 = "";

            EFindNextReturnTypes err = ReplaceAllInClosedDoc( m_FileName, m_FindReplaceOptions,
                m_FoundStringDel, ref errorString1, ref errorString2, m_SearchDoneDel, false );
        }

        /// <summary>
        /// Thread function for closed documents search and replace
        /// </summary>
        public void ClosedDocsSearchReplace()
        {
            string errorString1 = "";
            string errorString2 = "";

            EFindNextReturnTypes err = ReplaceAllInClosedDocs( m_FileNames, m_SearchPattern, m_FindReplaceOptions,
                m_FoundStringDel, ref errorString1, ref errorString2, m_SearchDoneDel );
        }
        #endregion

        #region Event Handlers
        private void OnSearchBegin( bool replace )
        {
            if ( m_SearchBeginDel != null )
            {
                if ( m_parentControl != null )
                {
                    object[] args = new object[1];
                    args[0] = replace;

                    m_parentControl.Invoke( m_SearchBeginDel, args );
                }
                else
                {
                    m_SearchBeginDel( replace );
                }
            }
        }
        
        private void OnSearchComplete( SearchCompleteDelegate searchDoneDel, int totalFound, int matchingFiles, int totalFilesSearched,
            EFindNextReturnTypes err, string errorString1, string errorString2 )
        {
            if ( searchDoneDel == null )
            {
                return;
            }

            if ( m_parentControl != null )
            {
                object[] args = new object[6];
                args[0] = totalFound;
                args[1] = matchingFiles;
                args[2] = totalFilesSearched;
                args[3] = err;
                args[4] = errorString1;
                args[5] = errorString2;

                m_parentControl.Invoke( searchDoneDel, args );
            }
            else
            {
                searchDoneDel( totalFound, matchingFiles, totalFilesSearched, err, errorString1, errorString2 );
            }

            //float avg = (totalFilesSearched > 0) ? (float)m_TotalTickCount / (float)totalFilesSearched : 0.0f;
            //Console.WriteLine( "FindReplace AvgTickCount={0}", avg );
        }

        private bool OnOkToReplaceInClosedDoc( string fileName, bool multipleDocs )
        {
            if ( m_sourceControl != null )
            {
                return m_sourceControl.MakeFileWritableOnSave( fileName, multipleDocs );
            }

            return true;
        }

        private bool OnOkToReplaceInOpenDoc( string fileName, bool multipleDocs )
        {
            if ( m_sourceControl != null )
            {
                return m_sourceControl.MakeFileWritableOnEdit( fileName, multipleDocs );
            }

            return true;
        }
        #endregion        			

        #region Misc
        /// <summary>
		/// Does some cleanup when a search/replace in a closed document is stopped.
		/// </summary>
        public void StopSearch()
        {
            if ( m_StreamReader != null )
            {
                m_StreamReader.Close();
                m_StreamReader = null;
            }

            if ( m_StreamWriter != null )
            {
                m_StreamWriter.Close();
                m_StreamWriter = null;
            }

            if ( m_WriterFileInfo != null )
            {
                m_WriterFileInfo.Delete();
                m_WriterFileInfo = null;
            }
        }
        #endregion
    }
}
