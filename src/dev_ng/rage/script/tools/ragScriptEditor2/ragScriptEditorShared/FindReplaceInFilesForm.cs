using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

using ActiproSoftware.SyntaxEditor;
using RSG.Base.Forms;

namespace ragScriptEditorShared
{
    public partial class FindReplaceInFilesForm : Form
    {
        public FindReplaceInFilesForm()
        {
            InitializeComponent();
        }

        public FindReplaceInFilesForm( Form form, FindReplaceOptions findReplaceOptions, SourceControl srcCtrl ) 
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// Add any constructor code after InitializeComponent call
			//
			m_MainForm = form;
			m_FindReplaceOptions = findReplaceOptions;
            m_sourceControl = srcCtrl;
            m_FindReplace = new FindReplace( form, srcCtrl );

			m_SearchDoneDel = new FindReplace.SearchCompleteDelegate( this.FindReplaceInFiles_SearchDone );
            m_ReplaceDoneDel = new FindReplace.SearchCompleteDelegate( this.FindReplaceInFiles_ReplaceDone );
            m_FindNextDoneDel = new FindReplace.SearchCompleteDelegate( this.FindReplaceInFiles_FindNextDone );

			m_LastFile = string.Empty;
            m_lastDirectory = string.Empty;

	        // Select the first search type            
		    this.useComboBox.SelectedIndex = 0;

			// Update options            
            GetFindReplaceOptions();            

			this.folderBrowserDialog.SelectedPath = this.LookInSelectedPath;
			this.folderBrowserDialog.Description = "Specify the folder to search from.";

            this.findWhatTextBox_TextChanged( null, null );

			this.findWhatTextBox.Focus();
		}

        public DialogResult ShowIt( System.Windows.Forms.Form form, List<string> extensions )
		{
            // build look in combo box options
            if ( m_lookInPaths != null )
            {
                for ( int i = m_lookInPaths.Count - 1; i >= 0; --i )
                {
                    SaveDirectory( m_lookInPaths[i] );
                }
            }

            if ( this.lookInComboBox.Text == string.Empty )
            {
                this.lookInComboBox.Text = this.lookInComboBox.Items[0] as string;
            }

            List<string> fileTypes = this.FileTypes;
            foreach ( string extension in extensions )
            {
                if ( !fileTypes.Contains( extension ) )
                {
                    fileTypes.Add( extension );
                }
            }

            string fileTypeSelected = this.FileTypeSelected;
            this.FileTypes = fileTypes;
            this.FileTypeSelected = fileTypeSelected;

            if ( this.fileTypesComboBox.Text == string.Empty )
            {
                this.fileTypesComboBox.Text = this.fileTypesComboBox.Items[0] as string;
            }
          
			// now show the dialog
			DialogResult result = ShowDialog( form );

            if ( this.StartPosition == FormStartPosition.CenterParent )
            {
                // so the next time we open the window, it will be in the same position as before
                this.StartPosition = FormStartPosition.Manual;
            }

            return result;
		}

        #region Delegates
        public delegate void ConsoleOutputDelegate( string str, bool clear );
        public delegate void InitSearchDocsDelegate( ref SyntaxEditor currSyntaxEditor, 
            ref Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openDocs, ref List<string> projectDocs );
        public delegate void CancelActiveIntellisenseDelegate();
        public delegate void OpenModifiedFilesAfterReplaceAllDelegate();
        public delegate bool LogoffSourceControlDelegate();
        public delegate bool ReplaceAllWarningDelegate();
        #endregion

        #region Constants
        private const string c_currentDocument = "Current Document";
        private const string c_allOpenDocuments = "All Open Documents";
        private const string c_entireProject = "Entire Project";
        #endregion

        #region Variables
        private System.Windows.Forms.Form m_MainForm;
        private SourceControl m_sourceControl = null;
        private FindReplace m_FindReplace = null;
        private FindReplace.SearchCompleteDelegate m_SearchDoneDel = null;
        private FindReplace.SearchCompleteDelegate m_ReplaceDoneDel = null;
        private FindReplace.SearchCompleteDelegate m_FindNextDoneDel = null;

        System.Threading.Thread m_TheThread;
        private FindReplaceOptions m_FindReplaceOptions;

        private string m_lastDirectory = string.Empty;
        private string m_LastFile = string.Empty;

        private SyntaxEditor m_SyntaxEditor = null;
        private Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> m_OpenFiles 
            = new Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor>();
        private List<string> m_ProjectFiles = new List<string>();

        private List<string> m_lookInPaths = null;
        private List<string> m_fileTypes = null;
        private bool m_enableCurrentDocumentSearching = true;
        private bool m_enableAllOpenDocumentsSearching = true;
        private bool m_enableEntireProjectSearching = true;
        private bool m_enableSearching = true;
        #endregion

        #region Properties
        public bool MatchCase
        {
            get
            {
                return this.matchCaseCheckBox.Checked;
            }
            set
            {
                this.matchCaseCheckBox.Checked = value;
                m_FindReplaceOptions.MatchCase = value;
            }
        }

        public bool MatchWholeWord
        {
            get
            {
                return this.matchWholeWordCheckBox.Checked;
            }
            set
            {
                this.matchWholeWordCheckBox.Checked = value;
                m_FindReplaceOptions.MatchWholeWord = value;
            }
        }

        public bool UseBox
        {
            get
            {
                return this.useCheckBox.Checked;
            }
            set
            {
                this.useComboBox.Enabled = this.useCheckBox.Checked = value;
                m_LastFile = string.Empty;
                m_FindReplaceOptions.SearchType = (value ? FindReplaceSearchType.RegularExpression : FindReplaceSearchType.Normal);
            }
        }

        public bool OpenModifiedFiles
        {
            get
            {
                return this.openModifiedFilesCheckBox.Checked;
            }
            set
            {
                this.openModifiedFilesCheckBox.Checked = value;
            }
        }

        public string LookInSelectedPath
		{
			get
			{
                return this.folderBrowserDialog.SelectedPath;
			}
			set
			{
				this.folderBrowserDialog.SelectedPath = value;
				this.lookInComboBox.Text = value;
			}
		}

        public List<string> LookInPaths
        {
            get
            {
                List<string> paths = new List<string>();
                foreach ( string path in this.lookInComboBox.Items )
                {
                    paths.Add( path );
                }
                return paths;
            }
            set
            {
                m_lookInPaths = value;
            }
        }

        public bool LookInSubfolders
        {
            get
            {
                return this.lookInSubfoldersCheckBox.Checked;
            }
            set
            {
                this.lookInSubfoldersCheckBox.Checked = value;
            }
        }

        public string FileTypeSelected
        {
            get
            {
                return this.fileTypesComboBox.Text;
            }
            set
            {
                this.fileTypesComboBox.Text = value;
            }
        }

        public List<string> FileTypes
        {
            get
            {
                List<string> types = new List<string>();
                foreach ( string type in this.fileTypesComboBox.Items )
                {
                    types.Add( type );
                }
                return types;
            }
            set
            {
                m_fileTypes = value;
            }
        }

        public FindReplace.AddFoundStringDelegate FoundString = null;
        public FindReplace.SearchBeginDelegate SearchBegin = null;
        public FindReplace.SearchCompleteDelegate SearchComplete = null;
        public FindReplace.LoadFileDelegate LoadFile = null;
        public FindReplace.ReloadFileDelegate ReloadFile = null;
        public FindReplace.DisableFileWatcherDelegate DisableFileWatch = null;
        public FindReplace.EnableFileWatcherDelegate EnableFileWatch = null;

        public ConsoleOutputDelegate ConsoleOutput = null;
        public InitSearchDocsDelegate InitSearchDocs = null;
        public CancelActiveIntellisenseDelegate CancelIntellisense = null;
        public OpenModifiedFilesAfterReplaceAllDelegate OpenModifiedFilesAfteReplace = null;
        public ReplaceAllWarningDelegate ReplaceAllWarning = null;
        #endregion

        #region Event Handlers
        private void FindReplaceInFilesForm_Activated( object sender, EventArgs e )
        {
            OnCancelActiveIntellisense();

            // see which search types should be available

            ActiproSoftware.SyntaxEditor.SyntaxEditor editor = null;
            Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openFiles = null;
            List<string> projectFiles = null;

            OnInitSearchDocs( ref editor, ref openFiles, ref projectFiles );

            m_enableCurrentDocumentSearching = editor != null;
            m_enableAllOpenDocumentsSearching = (openFiles != null) && (openFiles.Count > 0);
            m_enableEntireProjectSearching = (projectFiles != null) && (projectFiles.Count > 0);

            if ( !m_enableCurrentDocumentSearching && !m_enableAllOpenDocumentsSearching && !m_enableEntireProjectSearching )
            {
                if ( this.lookInComboBox.Items.Count == 3 )
                {
                    m_enableSearching = false;
                    this.lookInComboBox.Text = string.Empty;
                }
                else
                {
                    if ( (this.lookInComboBox.Text == c_currentDocument) || (this.lookInComboBox.Text == c_allOpenDocuments) 
                        || (this.lookInComboBox.Text == c_entireProject) )
                    {
                        this.lookInComboBox.SelectedIndex = 0;
                    }

                    m_enableSearching = true;
                    this.lookInComboBox.Enabled = true;
                }
            }
            else
            {
                m_enableSearching = true;
                this.lookInComboBox.Enabled = true;

                if ( (this.lookInComboBox.Text == c_entireProject) && !m_enableEntireProjectSearching )
                {
                    if ( m_enableCurrentDocumentSearching )
                    {
                        this.lookInComboBox.Text = c_currentDocument;
                    }
                    else if ( m_enableAllOpenDocumentsSearching )
                    {
                        this.lookInComboBox.Text = c_allOpenDocuments;
                    }
                }
                else if ( (this.lookInComboBox.Text == c_allOpenDocuments) && !m_enableAllOpenDocumentsSearching )
                {
                    if ( m_enableCurrentDocumentSearching )
                    {
                        this.lookInComboBox.Text = c_currentDocument;
                    }
                    else if ( m_enableEntireProjectSearching )
                    {
                        this.lookInComboBox.Text = c_entireProject;
                    }
                }
                else if ( (this.lookInComboBox.Text == c_currentDocument) && !m_enableCurrentDocumentSearching )
                {
                    if ( m_enableAllOpenDocumentsSearching )
                    {
                        this.lookInComboBox.Text = c_allOpenDocuments;
                    }
                    else if ( m_enableEntireProjectSearching )
                    {
                        this.lookInComboBox.Text = c_entireProject;
                    }
                }
            }
            
            SelectNextControl( this.replaceWithTextBox, false, true, true, true );
        }

        private void FindReplaceInFilesForm_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( e.CloseReason == CloseReason.UserClosing )
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        private void findButton_Click( object sender, EventArgs e )
        {
            // Update find/replace options
            SetFindReplaceOptions();
            
            string directory = this.lookInComboBox.Text;
            if ( (directory != c_currentDocument) && (directory != c_allOpenDocuments)
                && (directory != c_entireProject) && !Directory.Exists( directory ) )
            {
                rageMessageBox.ShowExclamation( this, String.Format( "Directory '{0}' doesn't exist", directory ), "Directory doesn't exist" );
                return;
            }

            // clear out the console output:
            OnConsoleOutput( "", true );

            string extension = this.fileTypesComboBox.Text;

            if ( directory == c_currentDocument )
            {
                string filename = (m_SyntaxEditor != null) ? m_SyntaxEditor.Document.Filename : string.Empty;
                m_FindReplace.InitNewClosedDocSearch( filename, m_FindReplaceOptions,
                    FoundString, SearchBegin, m_SearchDoneDel, null, null, null );
            }
            else if ( directory == c_allOpenDocuments )
            {
                List<string> theFiles = new List<string>();
                IDictionaryEnumerator enumerator = m_OpenFiles.GetEnumerator();
                while ( enumerator.MoveNext() )
                {
                    SyntaxEditor syntaxEditor = enumerator.Value as SyntaxEditor;
                    theFiles.Add( syntaxEditor.Document.Filename );
                }

                m_FindReplace.InitNewClosedDocsSearch( theFiles, extension, m_FindReplaceOptions,
                    FoundString, SearchBegin, m_SearchDoneDel, null, null, null );
            }
            else if ( directory == c_entireProject )	// includes open files not in the project as well, just like Visual Studio
            {
                List<string> allFiles = BuildProjectDocsList();

                m_FindReplace.InitNewClosedDocsSearch( allFiles, extension, m_FindReplaceOptions,
                    FoundString, SearchBegin, m_SearchDoneDel, null, null, null );
            }
            else
            {
                SaveDirectory( directory );

                m_FindReplace.InitNewDirectorySearch( directory, this.lookInSubfoldersCheckBox.Checked, extension, m_FindReplaceOptions,
                    FoundString, SearchBegin, m_SearchDoneDel, null, null, null, null );
            }

            // Perform a find operation
            try
            {
                SetButtonsEnabledOnThreadStart();

                OnConsoleOutput( "Find all \"" + m_FindReplaceOptions.FindText + "\", " + directory + ", \"" + extension + "\"\n", false );

                if ( directory == c_currentDocument )
                {
                    m_TheThread = new Thread( new ThreadStart( m_FindReplace.ClosedDocSearch ) );
                }
                else if ( (directory == c_allOpenDocuments) || (directory == c_entireProject) )
                {
                    m_TheThread = new Thread( new ThreadStart( m_FindReplace.ClosedDocsSearch ) );
                }
                else
                {
                    m_TheThread = new Thread( new ThreadStart( m_FindReplace.DirectorySearch ) );
                }

                m_TheThread.Priority = ThreadPriority.Lowest;
                m_TheThread.Start();
            }
            catch ( Exception ex )
            {
                rageMessageBox.ShowError( this, String.Format( "An error occurred:\r\n{0}", ex.Message ), "Find/Replace Error" );
                return;
            }
        }

        private void findNextButton_Click( object sender, EventArgs e )
        {
            // Update find/replace options
            SetFindReplaceOptions();

            string directory = this.lookInComboBox.Text;
            string extension = this.fileTypesComboBox.Text;

            string errorString1 = "";
            string errorString2 = "";

            FindReplace.EFindNextReturnTypes err = FindReplace.EFindNextReturnTypes.E_NO_ERROR;
            SyntaxEditor currSyntaxEditor = m_SyntaxEditor;

            if ( directory == c_currentDocument )
            {
                m_FindReplaceOptions.ChangeSelection = true;
                err = m_FindReplace.FindNextInOpenDoc( m_SyntaxEditor, m_FindReplaceOptions,
                    ref errorString1, ref errorString2 );
            }
            else if ( directory == c_allOpenDocuments )
            {
                Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openFiles = BuildOpenDocsList( extension );

                err = m_FindReplace.FindNextInOpenDocs( openFiles, m_FindReplaceOptions,
                    ref m_SyntaxEditor, ref errorString1, ref errorString2, LoadFile );
            }
            else if ( directory == c_entireProject )
            {
                Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openFiles = BuildOpenDocsList( extension );

                // look in the open docs first
                err = m_FindReplace.FindNextInOpenDocs( openFiles, m_FindReplaceOptions,
                    ref m_SyntaxEditor, ref errorString1, ref errorString2, LoadFile );

                // if not found, no more found, or we've reached the start of the open doc search
                if ( (err == FindReplace.EFindNextReturnTypes.E_NOT_FOUND)
                    || (err == FindReplace.EFindNextReturnTypes.E_PAST_START_OF_SEARCH) )
                {
                    // build list of project files, minus open files because we've already searched those
                    List<string> allFiles = BuildProjectListWithoutOpenDocs( extension );

                    // look for next file that has a match
                    foreach ( string file in allFiles )
                    {
                        // if the file contains the search text, open the file
                        if ( m_FindReplace.IsInClosedDoc( file, m_FindReplaceOptions )
                            && OnLoadFile( file ) )
                        {
                            OnInitSearchDocs( ref m_SyntaxEditor, ref m_OpenFiles, ref m_ProjectFiles );

                            // find the first occurrence
                            m_FindReplaceOptions.ChangeSelection = true;
                            err = m_FindReplace.FindNextInOpenDoc( m_SyntaxEditor, m_FindReplaceOptions,
                                ref errorString1, ref errorString2 );
                            break;
                        }
                    }
                }
            }
            else
            {
                SaveDirectory( directory );

                string filename = (m_SyntaxEditor != null) ? m_SyntaxEditor.Document.Filename : string.Empty;
                bool isInDir = filename.IndexOf( directory ) != -1;
                if ( isInDir )
                {
                    m_FindReplaceOptions.ChangeSelection = true;
                    err = m_FindReplace.FindNextInOpenDoc( m_SyntaxEditor, m_FindReplaceOptions,
                        ref errorString1, ref errorString2 );

                    m_LastFile = filename;
                }

                if ( !isInDir || (err == FindReplace.EFindNextReturnTypes.E_NOT_FOUND)
                    || (err == FindReplace.EFindNextReturnTypes.E_PAST_EOF)
                    || (err == FindReplace.EFindNextReturnTypes.E_PAST_START_OF_SEARCH) )
                {
                    m_FindReplace.InitNewDirectorySearch( directory, this.lookInSubfoldersCheckBox.Checked,
                        extension, m_FindReplaceOptions, null, SearchBegin, m_FindNextDoneDel, null, null, null, m_LastFile );

                    try
                    {
                        SetButtonsEnabledOnThreadStart();

                        m_TheThread = new Thread( new ThreadStart( m_FindReplace.DirectorySearchNext ) );
                        m_TheThread.Priority = ThreadPriority.Lowest;
                        m_TheThread.Start();
                    }
                    catch ( Exception ex )
                    {
                        rageMessageBox.ShowError( this, String.Format( "An error occurred:\r\n{0}", ex.Message ), "Find/Replace Error" );
                        return;
                    }

                    return;
                }
            }

            // make sure we retain focus when switching documents
            if ( currSyntaxEditor != m_SyntaxEditor )
            {
                this.Focus();
            }

            switch ( err )
            {
            case FindReplace.EFindNextReturnTypes.E_ERROR:
                rageMessageBox.ShowError( this, errorString1, errorString2 );
                break;

            case FindReplace.EFindNextReturnTypes.E_NOT_FOUND:
            case FindReplace.EFindNextReturnTypes.E_PAST_START_OF_SEARCH:
                rageMessageBox.ShowInformation( this, errorString1, errorString2 );
                break;

            default:
                break;
            }

            SelectNextControl( this.replaceWithTextBox, false, true, true, true );
        }

        private void replaceButton_Click( object sender, EventArgs e )
        {
            // Update find/replace options
            SetFindReplaceOptions();

            string directory = this.lookInComboBox.Text;
            string extension = this.fileTypesComboBox.Text;

            string errorString1 = "";
            string errorString2 = "";

            OnResetSourceControlEditAction();

            FindReplace.EFindNextReturnTypes err = FindReplace.EFindNextReturnTypes.E_NO_ERROR;
            SyntaxEditor currSyntaxEditor = m_SyntaxEditor;

            if ( directory == c_currentDocument )
            {
                m_FindReplaceOptions.ChangeSelection = true;
                err = m_FindReplace.ReplaceInOpenDoc( m_SyntaxEditor, m_FindReplaceOptions,
                    ref errorString1, ref errorString2, false );
            }
            else if ( directory == c_allOpenDocuments )
            {
                Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openFiles = BuildOpenDocsList( extension );

                err = m_FindReplace.ReplaceInOpenDocs( openFiles, m_FindReplaceOptions, ref m_SyntaxEditor,
                    ref errorString1, ref errorString2 );
            }
            else if ( directory == c_entireProject )
            {
                Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openFiles = BuildOpenDocsList( extension );

                // replace in the open docs first
                err = m_FindReplace.ReplaceInOpenDocs( openFiles, m_FindReplaceOptions, ref m_SyntaxEditor,
                    ref errorString1, ref errorString2 );

                // if not found, no more found, or we've reached the start of the open doc search
                if ( (err == FindReplace.EFindNextReturnTypes.E_NOT_FOUND)
                    || (err == FindReplace.EFindNextReturnTypes.E_PAST_START_OF_SEARCH) )
                {
                    // build list of project files, minus open files because we've already searched those
                    List<string> allFiles = BuildProjectListWithoutOpenDocs( extension );

                    // look for next file that has a match
                    foreach ( string file in allFiles )
                    {
                        // if the file contains the search text, open the file
                        if ( m_FindReplace.IsInClosedDoc( file, m_FindReplaceOptions )
                            && OnLoadFile( file ) )
                        {
                            OnInitSearchDocs( ref m_SyntaxEditor, ref m_OpenFiles, ref m_ProjectFiles );

                            // replace the first occurrence
                            m_FindReplaceOptions.ChangeSelection = true;
                            err = m_FindReplace.ReplaceInOpenDoc( m_SyntaxEditor, m_FindReplaceOptions,
                                ref errorString1, ref errorString2, false );
                            break;
                        }
                    }
                }
            }
            else
            {
                SaveDirectory( directory );

                string filename = (m_SyntaxEditor != null) ? m_SyntaxEditor.Document.Filename : string.Empty;
                bool isInDir = filename.IndexOf( directory ) != -1;
                if ( isInDir )
                {
                    m_FindReplaceOptions.ChangeSelection = true;
                    err = m_FindReplace.ReplaceInOpenDoc( m_SyntaxEditor, m_FindReplaceOptions,
                        ref errorString1, ref errorString2, false );

                    m_LastFile = filename;
                }

                if ( !isInDir || (err == FindReplace.EFindNextReturnTypes.E_NOT_FOUND)
                    || (err == FindReplace.EFindNextReturnTypes.E_PAST_EOF)
                    || (err == FindReplace.EFindNextReturnTypes.E_PAST_START_OF_SEARCH) )
                {
                    m_FindReplace.InitNewDirectorySearch( directory, this.lookInSubfoldersCheckBox.Checked,
                        extension, m_FindReplaceOptions, null, SearchBegin, m_FindNextDoneDel, null, null, null, m_LastFile );

                    try
                    {
                        SetButtonsEnabledOnThreadStart();

                        m_TheThread = new Thread( new ThreadStart( m_FindReplace.DirectorySearchNext ) );
                        m_TheThread.Priority = ThreadPriority.Lowest;
                        m_TheThread.Start();
                    }
                    catch ( Exception ex )
                    {
                        rageMessageBox.ShowError( this, String.Format( "An error occurred:\r\n{0}", ex.Message ), "Find/Replace Error" );
                        return;
                    }

                    return;
                }
            }

            // make sure we retain focus when switching documents
            if ( currSyntaxEditor != m_SyntaxEditor )
            {
                this.Focus();
            }

            switch ( err )
            {
            case FindReplace.EFindNextReturnTypes.E_ERROR:
                rageMessageBox.ShowError( this, errorString1, errorString2 );
                break;

            case FindReplace.EFindNextReturnTypes.E_NOT_FOUND:
            case FindReplace.EFindNextReturnTypes.E_PAST_EOF:
            case FindReplace.EFindNextReturnTypes.E_PAST_START_OF_SEARCH:
                rageMessageBox.ShowInformation( this, errorString1, errorString2 );
                break;

            default:
                break;
            }

            SelectNextControl( this.replaceWithTextBox, false, true, true, true );

            OnLogoffSourceControl();
        }

        private void replaceAllButton_Click( object sender, EventArgs e )
        {
            // Update find/replace options
            SetFindReplaceOptions();

            string directory = this.lookInComboBox.Text;
            if ( (directory != c_currentDocument) && (directory != c_allOpenDocuments)
                && (directory != c_entireProject) && !Directory.Exists( directory ) )
            {
                rageMessageBox.ShowExclamation( this, String.Format( "Directory '{0}' doesn't exist", directory ), "Directory doesn't exist" );
                return;
            }

            if ( !OnReplaceAllWarning() )
            {
                return;
            }

            // clear out the console output:
            OnConsoleOutput( "", true );

            string extension = this.fileTypesComboBox.Text;

            OnResetSourceControlSaveAction();

            if ( directory == c_currentDocument )
            {
                string filename = (m_SyntaxEditor != null) ? m_SyntaxEditor.Document.Filename : string.Empty;
                m_FindReplace.InitNewClosedDocSearch( filename, m_FindReplaceOptions,
                    FoundString, SearchBegin, m_ReplaceDoneDel, ReloadFile, DisableFileWatch, EnableFileWatch );
            }
            else if ( directory == c_allOpenDocuments )
            {
                Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openFiles = BuildOpenDocsList( extension );

                List<string> theFiles = new List<string>();
                IDictionaryEnumerator enumerator = openFiles.GetEnumerator();
                while ( enumerator.MoveNext() )
                {
                    SyntaxEditor syntaxEditor = enumerator.Value as SyntaxEditor;
                    theFiles.Add( syntaxEditor.Document.Filename );
                }

                // then get ready to perform the search on the replaced text in order to fill up the Output window
                m_FindReplace.InitNewClosedDocsSearch( theFiles, extension, m_FindReplaceOptions,
                    FoundString, SearchBegin, m_ReplaceDoneDel, ReloadFile, DisableFileWatch, EnableFileWatch );
            }
            else if ( directory == c_entireProject )	// includes open files not in the project as well, just like .NET
            {
                List<string> allFiles = BuildProjectDocsList();

                m_FindReplace.InitNewClosedDocsSearch( allFiles, extension, m_FindReplaceOptions,
                    FoundString, SearchBegin, m_ReplaceDoneDel, ReloadFile, DisableFileWatch, EnableFileWatch );
            }
            else
            {
                SaveDirectory( directory );

                m_FindReplace.InitNewDirectorySearch( directory, this.lookInSubfoldersCheckBox.Checked, extension, m_FindReplaceOptions,
                    FoundString, SearchBegin, m_ReplaceDoneDel, ReloadFile, DisableFileWatch, EnableFileWatch, null );
            }

            // Perform a find operation
            try
            {
                SetButtonsEnabledOnThreadStart();

                OnConsoleOutput( "Replace all \"" + m_FindReplaceOptions.FindText + "\" with \""
                    + m_FindReplaceOptions.ReplaceText + "\", " + directory + ", \"" + extension + "\"\n", false );

                if ( directory == c_currentDocument )
                {
                    m_TheThread = new Thread( new ThreadStart( m_FindReplace.ClosedDocSearchReplace ) );
                }
                else if ( (directory == c_allOpenDocuments) || (directory == c_entireProject) )
                {
                    m_TheThread = new Thread( new ThreadStart( m_FindReplace.ClosedDocsSearchReplace ) );
                }
                else
                {
                    m_TheThread = new Thread( new ThreadStart( m_FindReplace.DirectorySearchReplace ) );
                }

                m_TheThread.Priority = ThreadPriority.Lowest;
                m_TheThread.Start();
            }
            catch ( Exception ex )
            {
                rageMessageBox.ShowError( this, String.Format( "An error occurred:\r\n{0}", ex.Message ), "Find/Replace Error" );
                return;
            }
        }

        private void stopButton_Click( object sender, EventArgs e )
        {
            if ( m_TheThread != null )
            {
                m_TheThread.Abort();
                while ( m_TheThread.IsAlive )
                {
                    Thread.Sleep( 200 );
                    m_TheThread.Abort();
                }

                m_TheThread = null;

                m_FindReplace.StopSearch();

                SetButtonsEnabledOnThreadStop();

                OnConsoleOutput( "Find was stopped in progress.", false );

                if ( (m_OpenFiles != null) && (EnableFileWatch != null) )
                {
                    IDictionaryEnumerator enumerator = m_OpenFiles.GetEnumerator();
                    while ( enumerator.MoveNext() )
                    {
                        SyntaxEditor syntaxEditor = enumerator.Value as SyntaxEditor;
                        OnEnableFileWatcher( syntaxEditor.Document.Filename );
                    }
                }
            }
        }

        private void lookInButton_Click( object sender, EventArgs e )
        {
            DialogResult result = this.folderBrowserDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.lookInComboBox.Text = this.folderBrowserDialog.SelectedPath;
            }
        }

        private void findWhatTextBox_TextChanged( object sender, EventArgs e )
        {
            this.findButton.Enabled = (this.findWhatTextBox.TextLength > 0) && m_enableSearching;            
            this.findNextButton.Enabled = this.findButton.Enabled;            
            this.replaceButton.Enabled = this.findButton.Enabled;            
            this.replaceAllButton.Enabled = this.findButton.Enabled;
            m_LastFile = string.Empty;
        }

        private void useCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.useComboBox.Enabled = this.useCheckBox.Checked;
            m_LastFile = string.Empty;
        }

        public void FindReplaceInFiles_SearchDone( int totalFound, int matchingFiles, int totalFilesSearched,
            FindReplace.EFindNextReturnTypes err, string errorString1, string errorString2 )
        {
            m_TheThread = null;

            SetButtonsEnabledOnThreadStop();

            if ( (totalFound == 0) || (matchingFiles == 0) || (err == FindReplace.EFindNextReturnTypes.E_ERROR) )
            {
                OnConsoleOutput( errorString1 + "\t" + errorString2 + "\n", false );
            }
            else
            {
                OnConsoleOutput( "Total found: " + totalFound + "\tMatching files: " + matchingFiles + "\tTotal files searched: " + totalFilesSearched + "\n", false );
            }

            SelectNextControl( this.replaceWithTextBox, false, true, true, true );

            OnSearchComplete( totalFound, matchingFiles, totalFilesSearched, err, errorString1, errorString2 );
        }

        public void FindReplaceInFiles_ReplaceDone( int totalFound, int matchingFiles, int totalFilesSearched,
            FindReplace.EFindNextReturnTypes err, string errorString1, string errorString2 )
        {
            m_TheThread = null;

            SetButtonsEnabledOnThreadStop();

            if ( (totalFound == 0) || (matchingFiles == 0) || (err == FindReplace.EFindNextReturnTypes.E_ERROR) )
            {
                OnConsoleOutput( errorString1 + "\t" + errorString2 + "\n", false );
            }
            else
            {
                OnConsoleOutput( "Total replaced: " + totalFound + "\tMatching files: " + matchingFiles + "\tTotal files searched: " + totalFilesSearched + "\n", false );
            }

            SelectNextControl( this.replaceWithTextBox, false, true, true, true );
            
            if ( this.openModifiedFilesCheckBox.Checked )
            {
                OnOpenModifiedFiles();
            }

            OnSearchComplete( totalFound, matchingFiles, totalFilesSearched, err, errorString1, errorString2 );

            OnLogoffSourceControl();
        }

        public void FindReplaceInFiles_FindNextDone( int totalFound, int matchingFiles, int totalFilesSearched,
            FindReplace.EFindNextReturnTypes err, string errorString1, string errorString2 )
        {
            m_TheThread = null;

            SetButtonsEnabledOnThreadStop();

            switch ( err )
            {
            case FindReplace.EFindNextReturnTypes.E_ERROR:
                rageMessageBox.ShowError( this, errorString1, errorString2 );
                break;

            case FindReplace.EFindNextReturnTypes.E_NO_ERROR:
                {
                    m_LastFile = errorString1;

                    // load file
                    if ( !OnLoadFile( m_LastFile ) )
                    {
                        break;
                    }

                    OnInitSearchDocs( ref m_SyntaxEditor, ref m_OpenFiles, ref m_ProjectFiles );

                    // find first occurrence
                    m_FindReplaceOptions.ChangeSelection = true;
                    err = m_FindReplace.FindNextInOpenDoc( m_SyntaxEditor, m_FindReplaceOptions,
                        ref errorString1, ref errorString1 );

                    // make sure we retain focus after opening a new document
                    this.Focus();

                    switch ( err )
                    {
                    case FindReplace.EFindNextReturnTypes.E_ERROR:
                        rageMessageBox.ShowError( this, errorString1, errorString2 );
                        break;

                    case FindReplace.EFindNextReturnTypes.E_NOT_FOUND:
                    case FindReplace.EFindNextReturnTypes.E_PAST_START_OF_SEARCH:
                        rageMessageBox.ShowInformation( this, errorString1, errorString2 );
                        break;

                    default:
                        break;
                    }

                    break;
                }

            default:
                rageMessageBox.ShowInformation( this, errorString1, errorString2 );
                break;
            }

            SelectNextControl( this.replaceWithTextBox, false, true, true, true );

            OnSearchComplete( totalFound, matchingFiles, totalFilesSearched, err, errorString1, errorString2 );

            OnLogoffSourceControl();
        }

        private void lookInComboBox_TextChanged( object sender, EventArgs e )
        {
            if ( ((this.lookInComboBox.Text == c_currentDocument) && !m_enableCurrentDocumentSearching)
                || ((this.lookInComboBox.Text == c_allOpenDocuments) && !m_enableAllOpenDocumentsSearching)
                || ((this.lookInComboBox.Text == c_entireProject) && !m_enableEntireProjectSearching) )
            {
                this.lookInComboBox.Text = m_lastDirectory;
                folderBrowserDialog.SelectedPath = this.lookInComboBox.Text;
            }
            else
            {
                m_lastDirectory = this.lookInComboBox.Text;
                folderBrowserDialog.SelectedPath = this.lookInComboBox.Text;
            }            
        }
        #endregion

        #region Event Dispatchers
        private bool OnLoadFile( string filename )
        {
            if ( LoadFile != null )
            {
                if ( m_MainForm != null )
                {
                    object[] args = new object[1];
                    args[0] = filename;
                    object obj = m_MainForm.Invoke( LoadFile, args );
                    return obj.ToString() == System.Boolean.TrueString;
                }
                else
                {
                    return LoadFile( filename );
                }
            }

            return false;
        }

        private void OnEnableFileWatcher( string filename )
        {
            if ( EnableFileWatch != null )
            {
                if ( m_MainForm != null )
                {
                    object[] args = new object[1];
                    args[0] = filename;
                    m_MainForm.Invoke( EnableFileWatch, args );
                }
                else
                {
                    EnableFileWatch( filename );
                }
            }
        }

        private void OnConsoleOutput( string text, bool clear )
        {
            if ( ConsoleOutput != null )
            {
                if ( m_MainForm != null )
                {
                    object[] args = new object[2];
                    args[0] = text;
                    args[1] = clear;
                    m_MainForm.Invoke( ConsoleOutput, args );
                }
                else
                {
                    ConsoleOutput( text, clear );
                }
            }
        }

        private void OnInitSearchDocs( ref SyntaxEditor currSyntaxEditor,
            ref Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openDocs, ref List<string> projectDocs )
        {
            if ( InitSearchDocs != null )
            {
                if ( m_MainForm != null )
                {
                    object[] args = new object[3];
                    args[0] = currSyntaxEditor;
                    args[1] = openDocs;
                    args[2] = projectDocs;
                    m_MainForm.Invoke( InitSearchDocs, args );

                    currSyntaxEditor = args[0] as SyntaxEditor;
                    openDocs = args[1] as Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor>;
                    projectDocs = args[2] as List<string>;
                }
                else
                {
                    InitSearchDocs( ref currSyntaxEditor, ref openDocs, ref projectDocs );
                }
            }
        }

        private void OnCancelActiveIntellisense()
        {
            if ( CancelIntellisense != null )
            {
                if ( m_MainForm != null )
                {
                    m_MainForm.Invoke( CancelIntellisense );
                }
                else
                {
                    CancelIntellisense();
                }
            }
        }

        private void OnOpenModifiedFiles()
        {
            if ( OpenModifiedFilesAfteReplace != null )
            {
                if ( m_MainForm != null )
                {
                    m_MainForm.Invoke( OpenModifiedFilesAfteReplace );
                }
                else
                {
                    OpenModifiedFilesAfteReplace();
                }
            }
        }

        private void OnResetSourceControlSaveAction()
        {
            if ( m_sourceControl != null )
            {
                m_sourceControl.ResetSaveAction();
            }
        }

        private void OnResetSourceControlEditAction()
        {
            if ( m_sourceControl != null )
            {
                m_sourceControl.ResetEditAction();
            }
        }

        private void OnSearchComplete( int totalFound, int matchingFiles, int totalFilesSearched,
            FindReplace.EFindNextReturnTypes err, string errorString1, string errorString2 )
        {
            if ( SearchComplete != null )
            {
                if ( m_MainForm != null )
                {
                    object[] args = new object[6];
                    args[0] = totalFound;
                    args[1] = matchingFiles;
                    args[2] = totalFilesSearched;
                    args[3] = err;
                    args[4] = errorString1;
                    args[5] = errorString2;

                    m_MainForm.Invoke( SearchComplete, args );
                }
                else
                {
                    SearchComplete( totalFound, matchingFiles, totalFilesSearched, err, errorString1, errorString2 );
                }
            }
        }

        private void OnLogoffSourceControl()
        {
            if ( m_sourceControl != null )
            {
                m_sourceControl.LogoffSourceControl();
            }
        }

        private bool OnReplaceAllWarning()
        {
            if ( ReplaceAllWarning != null )
            {
                if ( m_MainForm != null )
                {
                    object result = m_MainForm.Invoke( ReplaceAllWarning );
                    return result.ToString() == bool.TrueString;
                }
                else
                {
                    return ReplaceAllWarning();
                }
            }

            return true;
        }
        #endregion

        #region Misc
        public void InitSearchText( SyntaxEditor currSyntaxEditor )
		{
			if ( m_TheThread != null )
			{
				// don't set initial search text when already performing a search
				return;
			}

            if ( currSyntaxEditor == null )
            {
                return;
            }

			// try to set initial search text
			string selectedText = currSyntaxEditor.SelectedView.SelectedText;				
			if ( selectedText.Length > 0 )
			{
				if ( selectedText.IndexOf( "\n" ) < 0 )
				{
					this.findWhatTextBox.Text = selectedText;
                    this.findWhatTextBox.SelectAll();
				}
			}
			else
			{
                DocumentLine line = currSyntaxEditor.SelectedView.CurrentDocumentLine;
                string lineText = line.Text;

                int colStart = currSyntaxEditor.SelectedView.Selection.StartOffset - line.StartOffset;
                int colEnd = colStart;

                if ( colStart > lineText.Length )
                {
                    // used to happen when intellisense was active
                    return;
                }
                else if ( (colStart == lineText.Length) || Char.IsWhiteSpace( lineText[colStart] ) )
                {
                    // allow us to look behind
                    --colStart;
                    --colEnd;
                }

                if ( (lineText.Length > 0) && (colStart >= 0)
                    && (Char.IsLetterOrDigit( lineText[colStart] ) || Char.IsSymbol( lineText[colStart] ) || (lineText[colStart] == '_')) )
                {
                    while ( (colStart >= 0)
                        && (Char.IsLetterOrDigit( lineText[colStart] ) || Char.IsSymbol( lineText[colStart] ) || (lineText[colStart] == '_')) )
                    {
                        --colStart;
                    }

                    while ( (colEnd < lineText.Length)
                        && (Char.IsLetterOrDigit( lineText[colEnd] ) || Char.IsSymbol( lineText[colEnd] ) || (lineText[colEnd] == '_')) )
                    {
                        ++colEnd;
                    }

                    this.findWhatTextBox.Text = lineText.Substring( colStart + 1, colEnd - colStart - 1 );
                    this.findWhatTextBox.SelectAll();
                }
			}
		}

        private void SetFindReplaceOptions()
        {
            m_FindReplaceOptions.FindText = this.findWhatTextBox.Text;
            m_FindReplaceOptions.ReplaceText = this.replaceWithTextBox.Text;
            m_FindReplaceOptions.MatchCase = this.matchCaseCheckBox.Checked;
            m_FindReplaceOptions.MatchWholeWord = this.matchWholeWordCheckBox.Checked;
            m_FindReplaceOptions.SearchType = (this.useCheckBox.Checked ? FindReplaceSearchType.RegularExpression : FindReplaceSearchType.Normal);

            SaveFileType( this.fileTypesComboBox.Text );

            OnInitSearchDocs( ref m_SyntaxEditor, ref m_OpenFiles, ref m_ProjectFiles );
        }

        private void GetFindReplaceOptions()
        {
            this.findWhatTextBox.Text = m_FindReplaceOptions.FindText;
            this.replaceWithTextBox.Text = m_FindReplaceOptions.ReplaceText;
            this.matchCaseCheckBox.Checked = m_FindReplaceOptions.MatchCase;
            this.matchWholeWordCheckBox.Checked = m_FindReplaceOptions.MatchWholeWord;

            this.useCheckBox.Checked = m_FindReplaceOptions.SearchType == FindReplaceSearchType.RegularExpression;
            this.useComboBox.Enabled = this.useCheckBox.Checked;
        }

        public void StopThread()
        {
            if ( m_TheThread != null )
            {
                m_TheThread.Abort();
                m_TheThread = null;
            }

            m_FindReplace.StopSearch();
        }

        /// <summary>
        /// Helper function for taking the open docs and culling it down to just the ones that match the search pattern.
        /// </summary>
        /// <param name="searchPattern"></param>
        /// <returns></returns>
        private Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> BuildOpenDocsList( string searchPattern )
        {
            Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> openFiles 
                = new Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor>();

            foreach ( KeyValuePair<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> pair in m_OpenFiles )
            {
                if ( FileHasExtension( pair.Value.Document.Filename, searchPattern ) )
                {
                    openFiles.Add( pair.Key, pair.Value );
                }
            }

            return openFiles;
        }

        /// <summary>
        /// Helper function for taking the project docs and the open docs
        /// </summary>
        /// <returns></returns>
        private List<string> BuildProjectDocsList()
        {
            List<string> allFiles = new List<string>();

            IDictionaryEnumerator enumerator = m_OpenFiles.GetEnumerator();
            while ( enumerator.MoveNext() )
            {
                SyntaxEditor syntaxEditor = enumerator.Value as SyntaxEditor;
                allFiles.Add( syntaxEditor.Document.Filename.ToLower() );
            }

            foreach ( string file in m_ProjectFiles )
            {
                if ( !allFiles.Contains( file ) )
                {
                    allFiles.Add( file );
                }
            }

            return allFiles;
        }

        /// <summary>
        /// Helper function for taking the project docs and culling it down to just the ones that match the search pattern and that aren't in the open docs list.
        /// </summary>
        /// <param name="searchPattern"></param>
        /// <returns></returns>
        private List<string> BuildProjectListWithoutOpenDocs( string searchPattern )
        {
            List<string> allFiles = new List<string>();

            // build list of project files, minus open files because we've already searched those
            foreach ( string file in m_ProjectFiles )
            {
                if ( FileHasExtension( file, searchPattern ) )
                {
                    allFiles.Add( file );
                }
            }

            IDictionaryEnumerator enumerator = m_OpenFiles.GetEnumerator();
            while ( enumerator.MoveNext() )
            {
                SyntaxEditor syntaxEditor = enumerator.Value as SyntaxEditor;
                if ( allFiles.Contains( syntaxEditor.Document.Filename.ToLower() ) )	// remove open files
                {
                    allFiles.Remove( syntaxEditor.Document.Filename.ToLower() );
                }
            }

            return allFiles;
        }

        /// <summary>
        /// Helper function that tells us if the file name has an extension in the search pattern
        /// </summary>
        /// <param name="file"></param>
        /// <param name="searchPattern"></param>
        /// <returns>True if valid extension, false otherwise.</returns>
        private bool FileHasExtension( string file, string searchPattern )
        {
            if ( (searchPattern == null) || (searchPattern.IndexOf( "*.*" ) != -1) )
            {
                return true;
            }

            Regex fileExtensionDelim = new Regex( ";" );
            string[] fileExtensions = fileExtensionDelim.Split( searchPattern.ToLower() );

            for ( int i = 0; i < fileExtensions.Length; i++ )
            {
                fileExtensions[i] = fileExtensions[i].Trim();
            }

            foreach ( string fileExtension in fileExtensions )
            {
                string ext = fileExtension.Remove( 0, 1 );	// remove asterisk
                if ( file.ToLower().EndsWith( ext ) )
                {
                    return true;
                }
            }

            return false;
        }

        private void SetButtonsEnabledOnThreadStart()
        {            
            this.stopButton.Enabled = true;

            this.findButton.Enabled = false;
            this.findNextButton.Enabled = false;
            this.replaceButton.Enabled = false;
            this.replaceAllButton.Enabled = false;
        }

        private void SetButtonsEnabledOnThreadStop()
        {
            this.stopButton.Enabled = false;

            this.findButton.Enabled = m_enableSearching;
            this.findNextButton.Enabled = m_enableSearching;
            this.replaceButton.Enabled = m_enableSearching;
            this.replaceAllButton.Enabled = m_enableSearching;
        }

        private void SaveDirectory( string directory )
        {
            // if directory doesn't already exist, add it to the combo box
            if ( !this.lookInComboBox.Items.Contains( directory ) )
            {
                if ( this.lookInComboBox.Items.Count == 10 )
                {
                    this.lookInComboBox.Items.RemoveAt( 6 );    // the last 3 are always going to be our defaults
                }

                this.lookInComboBox.Items.Insert( 0, directory );
            }
        }

        private void SaveFileType( string type )
        {
            if ( !this.fileTypesComboBox.Items.Contains( type ) )
            {
                if ( this.fileTypesComboBox.Items.Count == 10 )
                {
                    this.fileTypesComboBox.Items.RemoveAt( 7 ); // the last 2 are always going to be our defaults
                }

                this.fileTypesComboBox.Items.Insert( 0, type );
            }
        }
        #endregion

        #region Saving & Loading
        /// <summary>
        /// Kept for backwards compatibility.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public bool LoadXml( XmlReader reader )
        {
            string matchcase = reader["matchcase"];
            if ( matchcase != null )
            {
                this.MatchCase = bool.Parse( matchcase );
            }

            string matchword = reader["matchword"];
            if ( matchword != null )
            {
                this.MatchWholeWord = bool.Parse( matchword );
            }

            string use = reader["use"];
            if ( use != null )
            {
                this.UseBox = bool.Parse( use );
            }

            string open = reader["open"];
            if ( open != null )
            {
                this.OpenModifiedFiles = bool.Parse( open );
            }

            string selectedPath = reader["selectedpath"];
            if ( selectedPath != null )
            {
                this.LookInSelectedPath = selectedPath;
            }

            string subfolders = reader["subfolders"];
            if ( subfolders != null )
            {
                this.LookInSubfolders = bool.Parse( subfolders );
            }

            string selectedFileType = reader["selectedtype"];
            if ( selectedFileType != null )
            {
                this.FileTypeSelected = selectedFileType;
            }

            if ( !reader.IsEmptyElement )
            {
                while ( reader.Read() )
                {
                    if ( !reader.IsStartElement() )
                    {
                        break;
                    }
                    else if ( reader.Name == "LookInPaths" )
                    {
                        List<string> paths = new List<string>();

                        while ( reader.Read() )
                        {
                            if ( !reader.IsStartElement() )
                            {
                                break;
                            }
                            else if ( reader.Name == "Path" )
                            {
                                string path = reader.ReadElementString();
                                if ( (path != null) && (path != string.Empty) )
                                {
                                    paths.Add( path );
                                }
                            }
                        }

                        this.LookInPaths = paths;
                    }
                    else if ( reader.Name == "FileTypes" )
                    {
                        List<string> types = new List<string>();

                        while ( reader.Read() )
                        {
                            if ( !reader.IsStartElement() )
                            {
                                break;
                            }
                            else if ( reader.Name == "Type" )
                            {
                                string type = reader.ReadElementString();
                                if ( (type != null) && (type != string.Empty) )
                                {
                                    types.Add( type );
                                }
                            }
                        }

                        this.FileTypes = types;
                    }
                }
            }

            return true;
        }

        public void SetUIFromSettings( FindReplaceAllSettings settings )
        {
            if ( this.FileTypes == null )
            {
                this.FileTypes = new List<string>();
            }
            else
            {
                this.FileTypes.Clear();
            }

            this.FileTypes.AddRange( settings.FileTypes.ToArray() );

            if ( this.LookInPaths == null )
            {
                this.LookInPaths = new List<string>();
            }
            else
            {
                this.LookInPaths.Clear();
            }

            this.LookInPaths.AddRange( settings.LookInPaths.ToArray() );

            this.MatchCase = settings.MatchCase;
            this.MatchWholeWord = settings.MatchWholeWord;
            this.OpenModifiedFiles = settings.OpenFilesAfterReplace;
            this.LookInSubfolders = settings.SearchSubFolders;
            this.FileTypeSelected = settings.SelectedFileType;
            this.LookInSelectedPath = settings.SelectedPath;
            this.UseBox = settings.UseRegularExpressions;
        }

        public void SetSettingsFromUI( FindReplaceAllSettings settings )
        {
            settings.FileTypes.Clear();
            if ( this.FileTypes != null )
            {
                settings.FileTypes.AddRange( this.FileTypes.ToArray() );
            }

            settings.LookInPaths.Clear();
            if ( this.LookInPaths != null )
            {
                settings.LookInPaths.AddRange( this.LookInPaths.ToArray() );
            }

            settings.MatchCase = this.MatchCase;
            settings.MatchWholeWord = this.MatchWholeWord;
            settings.OpenFilesAfterReplace = this.OpenModifiedFiles;
            settings.SearchSubFolders = this.LookInSubfolders;
            settings.SelectedFileType = this.FileTypeSelected;
            settings.SelectedPath = this.LookInSelectedPath;
            settings.UseRegularExpressions = this.UseBox;
        }
        #endregion
    }
}