using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragScriptEditorShared
{
    public partial class SyncFileDialog : Form
    {
        protected SyncFileDialog()
        {
            InitializeComponent();
        }

        #region Enums
        public enum SyncFileAction
        {
            DiscardSyncThenCheckOut,
            DoNotSyncBeforeCheckOut,
            SaveAsSyncThenCheckOut,
            CancelCheckOut
        }
        #endregion

        public static SyncFileAction ShowIt( Control parent, string filename )
        {
            SyncFileDialog dlg = new SyncFileDialog();
            dlg.fileLabel.Text = dlg.fileLabel.Text.Replace( "%s", filename );

            DialogResult result = dlg.ShowDialog( parent );
            switch ( result )
            {
                case DialogResult.Abort:
                    return SyncFileAction.DiscardSyncThenCheckOut;

                case DialogResult.Ignore:
                    return SyncFileAction.DoNotSyncBeforeCheckOut;

                case DialogResult.Retry:
                    return SyncFileAction.SaveAsSyncThenCheckOut;

                default:
                    return SyncFileAction.CancelCheckOut;
            }
        }
    }
}