using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Xml;

using ActiproSoftware.SyntaxEditor;
using RSG.Base;
using RSG.Base.Extensions;
using ragScriptEditorShared;
using RSG.Platform;

namespace ragScriptCompiler
{
	/// <summary>
	/// Summary description for ScriptCompiler.
	/// </summary>
	class ScriptCompiler
	{
        public ScriptCompiler(Platform platform)
        {
            m_userEditorSettings = new UserEditorSettings();
            m_ProjectExplorer = new ProjectExplorer( null );
            m_compiler = new Compiler( null, m_userEditorSettings );
            m_compiler.RunAsync = false;
            m_ScriptEditor = new ScriptEditor( null, m_userEditorSettings, m_ProjectExplorer, null, null );
            m_platform = platform;

            // Project Explorer
            m_ProjectExplorer.ExecuteCompileAction += new ProjectExplorer.CompileActionDelegate( ProjectExplorer_ExecuteCompileAction );

            // Compiler
            m_compiler.PreCompileStep = new Compiler.PreCompileStepDelegate( this.PrepareCompile );
            m_compiler.BuildCompilerCommand = new Compiler.BuildCompilerCommandDelegate( this.BuildCompilerCommand );
            m_compiler.ResourceCompilerCommand = new Compiler.ResourceCompilerCommandDelegate(this.ResourceCompilerCommand);
            m_compiler.OkToCompile = new Compiler.OkToCompileDelegate( this.MakeFileWritableOnCompile );
            m_compiler.CompileComplete = new Compiler.CompileCompleteDelegate( this.CompileComplete );
            m_compiler.BreakOnError = new Compiler.BreakOnErrorDelegate( this.BreakOnCompileError );
            m_compiler.CompilerFailure = new Compiler.CompilerFailureDelegate( this.CompilerFailure );
            m_compiler.FilesNotFound = new Compiler.FilesNotFoundDelegate( this.CompileFilesNotFound );
            m_compiler.FileUpToDate = new Compiler.FileUpToDateDelegate( this.CompileFileUpToDate );
            m_compiler.ListUpToDate = new Compiler.ListUpToDateDelegate( this.CompileListUpToDate );
            m_compiler.NothingToCompile = new Compiler.NothingToCompileDelegate( this.NothingToCompile );
            m_compiler.ProcessOutputString = new Compiler.ProcessOutputStringDelegate( this.ProcessCompilerOutputString );
            m_compiler.IsFileCompilable = new Compiler.IsFileCompilableDelegate( m_ScriptEditor.IsFileCompilable );
            m_compiler.FileNeedsToBeCompiled = new Compiler.FileNeedsToBeCompiledDelegate( m_ScriptEditor.FileNeedsToBeCompiled );
            m_compiler.FileNeedsToBeResourced = new Compiler.FileNeedsToBeResourcedDelegate(m_ScriptEditor.FileNeedsToBeResourced);
        }

        private enum CommandItems
        {
            CompileItem,
            Clean,
            LogFile,
            GuiName,
            OverrideDefaultCompileSettings,
            DebugParser,
            DumpThreadState,
            DisplayDisassembly,
            WarningsAsErrors,
            IncludeFile,
            IncludePath,
            Globals,
            Custom,
            ScriptCompiler,
            OutputDirectory,
            BreakOnCompileError,
            Version,
            OverrideCacheDirectory,
            ConfigurationName,
            PlatformName,

            //This must always be the last CommandItem enumeration.
            NumCommandItems
        }

        public int Run( string[] args )
        {
            // setup command line arguments
            rageCommandLineParser cmdParser = new rageCommandLineParser();
            List<rageCommandLineItem> commands = new List<rageCommandLineItem>();
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "", rageCommandLineItem.NumValues.OneOrMore, true,
                "projectFile|scriptFile...", "One project file and/or any number of script files to compile." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "clean", rageCommandLineItem.NumValues.NoneOrOne, false,
                "", "Clean the file(s) before compiling." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "log", rageCommandLineItem.NumValues.NoneOrOne, false,
                "[textFile]", "Save output to a file.  Without a value, defaults to 'log.txt' in the same directory as the project or the current directory." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "guiName", 1, false,
                "name", "Needed by compiles that use the Default Compiling option.  If not specified, we attempt to find an executable name that contains 'ScriptEditor' located in the same directory as the ScriptCompiler." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "override", rageCommandLineItem.NumValues.NoneOrOne, false,
                "[true|false]", "Overrides the current OverrideDefaultCompilerSettings value.  Without a value, defaults to true." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "debugparser", rageCommandLineItem.NumValues.NoneOrOne, false,
                "[true|false]", "Overrides the current DebugParser value.  Without a value, defaults to true." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "dump", rageCommandLineItem.NumValues.NoneOrOne, false,
                "[true|false]", "Overrides the current DumpThreadState value.  Without a value, defaults to true." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "dis", rageCommandLineItem.NumValues.NoneOrOne, false,
                "[true|false]", "Overrides the current DisplayDisassembly value.  Without a value, defaults to true." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "werror", rageCommandLineItem.NumValues.NoneOrOne, false,
                "[true|false]", "Overrides the current WarningsAsErrors value.  Without a value, defaults to true." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "include", 1, false,
                "scriptFile", "Overrides the current IncludeFile value." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "ipath", 1, false,
                "path", "Overrides the current IncludePath value." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "globals", 1, false,
                "scriptFile", "Overrides the current Globals value." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "custom", 1, false,
                "text", "Overrides the current Custom value." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "scriptCompiler", 1, false,
                "executable", "Overrides the current ScriptCompiler value." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "outputDirectory", 1, false,
                "path", "Overrides the current OutputDirectory value." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "break", rageCommandLineItem.NumValues.NoneOrOne, false,
                "[true|false]", "Break the compile process if any errors are found." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "version", rageCommandLineItem.NumValues.NoneOrOne, false,
                "[textFile]", "Displays the version number or writes it to the text file when specified." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "cacheDir", rageCommandLineItem.NumValues.OneOrMore, false,
                "languageName directory", "Overrides the saved or default Cache Directory for the given language.  May consist of multiple pairs." ) ) );
            commands.Add( cmdParser.AddItem( new rageCommandLineItem( "config", 1, false,
                "configuration name", "Specifies the configuration on which to perform the build, rebuild or clean operation.  Without this option, the last active configuration is used." ) ) );
            commands.Add(cmdParser.AddItem(new rageCommandLineItem("platform", 1, false,
                "platform name", "Specifies the platform on which to perform the build, rebuild or clean operation.  Without this option, the last active platform is used.")));

            if (commands.Count+1 == (int)CommandItems.NumCommandItems)
            {
                Console.Out.WriteLine("An incorrect number of command items specified. There are " + commands.Count + " command line arguments to be specified when there are " + CommandItems.NumCommandItems + " expected.  Consult a programmer.");
                return 1;
            }

            // parse the command line
            if ( !cmdParser.Parse( args ) )
            {
                Console.Out.Write( cmdParser.Error );
                return 1;
            }

            // version number info
            rageCommandLineItem versionItem = commands[(int)CommandItems.Version];
            if ( versionItem.WasSet )
            {
                string versionStr = "Version = " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

                string textFile = versionItem.Value as string;
                if ( textFile != string.Empty )
                {
                    try
                    {
                        StreamWriter writer = File.CreateText( textFile );
                        writer.WriteLine( versionStr );
                        writer.Close();
                    }
                    catch ( Exception e )
                    {
                        Console.WriteLine( "Error creating version file: " + e.Message );
                    }
                }
                else
                {
                    Console.WriteLine( versionStr );
                }

                return 1;
            }

            // initialize our editor and compiling/intellisense settings
            ScriptEditor.LoadPlugins( ApplicationSettings.ExecutablePath, null );

            LoadLayout();

            // this will start us off right
            m_userEditorSettings.Validate();

            if ( String.IsNullOrEmpty( m_userEditorSettings.CurrentCompilingLanguage ) )
            {
                // this will start us off right
                m_userEditorSettings.CurrentCompilingLanguage = "SanScript";
                m_userEditorSettings.CurrentCompilingSettingsIndex = 0;
            }

            // override cacheDir
            rageCommandLineItem cacheDirItem = commands[(int)CommandItems.OverrideCacheDirectory];
            if ( cacheDirItem.WasSet )
            {
                string[] vals = cacheDirItem.Value as string[];

                if ( vals.Length % 2 != 0 )
                {
                    WriteOutputString( "Command Line Item '" + cacheDirItem.Name + "' does not contain an even number of items." );
                }

                for ( int i = 1; i < vals.Length; i += 2 )
                {
                    if ( !Path.IsPathRooted( vals[i] ) || (vals[i].IndexOfAny( Path.GetInvalidPathChars() ) > -1) )
                    {
                        WriteOutputString( "Unable to set " + vals[i - 1] + " Cache Directory.  '" + vals[i] + "' is not a valid directory." );
                    }
                    else if ( !m_ScriptEditor.SetLanguageCacheDirectory( vals[i - 1], vals[i] ) )
                    {
                        WriteOutputString( "Unable to set " + vals[i - 1] + " Cache Directory.  '" + vals[i] + "' is not a valid language name." );
                    }
                }
            }

            // Turn off source control so we automatically overwrite all output files
            m_userEditorSettings.SourceControl2.Enabled = false;

            // pull out the file(s) we're going to compile
            List<string> fileNames = new List<string>();
            string projectFile = null;

            rageCommandLineItem filesItem = commands[(int)CommandItems.CompileItem];
            if ( filesItem.WasSet )
            {
                string[] files = filesItem.Value as string[];
                foreach ( string file in files )
                {
                    string fullPathFile = Path.GetFullPath( file );

                    if ( file.EndsWith( "proj" ) && (projectFile == null) )
                    {
                        projectFile = fullPathFile;
                    }
                    else if ( m_ScriptEditor.IsFileCompilable( fullPathFile ) )
                    {
                        fileNames.Add( fullPathFile );
                    }
                }
            }

            bool clean = false;
            rageCommandLineItem cleanItem = commands[(int)CommandItems.Clean];
            if ( cleanItem.WasSet )
            {
                clean = true;
            }

            rageCommandLineItem logItem = commands[(int)CommandItems.LogFile];
            if ( logItem.WasSet )
            {
                m_logFile = logItem.Value as string;
                if ( m_logFile == string.Empty )
                {
                    m_logFile = "log.txt";
                }
            }

            bool logDeleted = false;
            if ( projectFile != null )
            {
                if ( m_logFile != null )
                {
                    m_logFile = Path.Combine( Path.GetDirectoryName( projectFile ), m_logFile );
                    if ( File.Exists( m_logFile ) )
                    {
                        File.Delete( m_logFile );
                        logDeleted = true;
                    }
                }

                if ( LoadProjectFile( projectFile ) )
                {
                    const string m_AllKeyword = "all";
                    IList<Platform> platforms = new List<Platform>();
                    rageCommandLineItem platformItem = commands[(int)CommandItems.PlatformName];
                    if (platformItem.WasSet)
                    {
                        String platformName = (String)platformItem.Value;

                        if (String.Compare(platformName, m_AllKeyword, true) == 0)
                        {
                            platforms.AddRange(m_userEditorSettings.PlatformSettings.Platforms);
                        }
                        else
                        {
                            Platform platform;
                            if (Enum.TryParse(platformName, out platform))
                            {
                                if (m_userEditorSettings.PlatformSettings.Platforms.Contains(platform))
                                {
                                    platforms.Add(platform);
                                }
                                else
                                {
                                    Console.Out.WriteLine("Platform \"{0}\" isn't enabled for this project!  Aborting...", platformName);
                                    return 2;
                                }
                            }
                            else
                            {
                                Console.Out.WriteLine("Platform \"{0}\" isn't a valid platform!  Aborting...", platformName);
                                return 2;
                            }
                        }
                    }
                    else
                    {
                        platforms.Add(this.m_ProjectExplorer.UserProjectSettings.SelectedPlatform);
                    }

                    rageCommandLineItem configItem = commands[(int)CommandItems.ConfigurationName];
                    if ( configItem.WasSet && String.Compare(configItem.Value as string, m_AllKeyword, true) == 0 )
                    {
                        //Compile all configurations.
                        string configName = configItem.Value as string;
                        for (int i = 0; i < m_userEditorSettings.CurrentCompilingSettingsList.Count; ++i)
                        {
                            foreach (Platform platform in platforms)
                            {
                                m_userEditorSettings.CurrentCompilingSettingsIndex = i;
                                OverrideCompilingSettings(commands);
                                this.m_ProjectExplorer.UserProjectSettings.SelectedPlatform = platform;
                                m_ProjectExplorer.CompileProject(clean);
                            }
                        }
                    }
                    else if ( configItem.WasSet )
                    {
                        //Compile the configuration specified.
                        string configName = configItem.Value as string;
                        bool bFoundConfiguration = false;
                        for ( int i = 0; i < m_userEditorSettings.CurrentCompilingSettingsList.Count; ++i )
                        {
                            if ( String.Compare(m_userEditorSettings.CurrentCompilingSettingsList[i].ConfigurationName, configName, true) == 0)
                            {
                                m_userEditorSettings.CurrentCompilingSettingsIndex = i;
                                bFoundConfiguration = true;
                                break;
                            }
                        }

                        if(bFoundConfiguration == false)
                        {
                            Console.Out.WriteLine("Configuration \"{0}\" was not found!  Aborting...", configName);
                            return 2;
                        }

                        foreach (Platform platform in platforms)
                        {
                            OverrideCompilingSettings(commands);
                            this.m_ProjectExplorer.UserProjectSettings.SelectedPlatform = platform;
                            m_ProjectExplorer.CompileProject(clean);
                        }
                    }
                    else
                    {
                        //No configuration specified; default to the first configuration.
                        OverrideCompilingSettings(commands);
                        m_ProjectExplorer.CompileProject(clean);
                    }

                    do
                    {
                        // nothing
                    } while ( m_compiling );
                }
            }

            if ( (m_numCompilerErrors == 0) && (fileNames.Count > 0) )
            {
                while ( SemanticParserService.PendingRequestCount > 0 )
                {
                    // wait for existing requests to finish
                }

                if ( m_logFile != null )
                {
                    m_logFile = Path.Combine( Path.GetDirectoryName( projectFile ), m_logFile );
                    if ( !logDeleted && File.Exists( m_logFile ) )
                    {
                        File.Delete( m_logFile );
                        logDeleted = true;
                    }
                }

                rageCommandLineItem configItem = commands[(int)CommandItems.ConfigurationName];
                if ( configItem.WasSet )
                {
                    string configName = configItem.Value as string;
                    for ( int i = 0; i < m_userEditorSettings.CurrentCompilingSettingsList.Count; ++i )
                    {
                        if ( String.Compare(m_userEditorSettings.CurrentCompilingSettingsList[i].ConfigurationName, configName, true) == 0)
                        {
                            m_userEditorSettings.CurrentCompilingSettingsIndex = i;
                            break;
                        }
                    }
                }

                OverrideCompilingSettings( commands );

                if ( clean )
                {
                    m_compiler.RecompileFileList( fileNames, this.m_ProjectExplorer.UserProjectSettings.SelectedPlatform );
                }
                else
                {
                    m_compiler.CompileFileList(fileNames, this.m_ProjectExplorer.UserProjectSettings.SelectedPlatform);
                }

                do
                {
                    // nothing
                } while ( m_compiling );
            }

            if ( m_logWriter != null )
            {
                m_logWriter.Close();
            }

            ScriptEditor.ShutdownPlugins( null );
            return m_numCompilerErrors;
        }

        #region Variables
        private readonly Platform m_platform;
        private Compiler m_compiler;
        private ScriptEditor m_ScriptEditor;
        private ProjectExplorer m_ProjectExplorer;
        private UserEditorSettings m_userEditorSettings = new UserEditorSettings();
        private string m_logFile = null;
        private StreamWriter m_logWriter = null;
        private int m_numCompilerErrors = 0;
        private bool m_compiling = false;
        #endregion

        #region File Handling
        private bool LoadProjectFile( string filename )
		{
            if ( m_ProjectExplorer.LoadProject( filename ) )
            {
                // override the user's settings with those from the current project
                m_userEditorSettings.Copy( m_ProjectExplorer.ProjectEditorSetting );

                while ( SemanticParserService.PendingRequestCount > 0 )
                {
                    // wait for existing requests to finish
                }

                m_ProjectExplorer.LockProjectFile = true;

                // update the parser
                m_ScriptEditor.ProjectLoaded( m_ProjectExplorer.GetProjectFilenames() );

                m_userEditorSettings.CurrentCompilingLanguage = m_ProjectExplorer.ProjectEditorSetting.Language;

                while ( SemanticParserService.PendingRequestCount > 0 )
                {
                    // wait for existing requests to finish
                }

                m_ProjectExplorer.LockProjectFile = false;

                return true;
            }

            return false;
        }
        #endregion

        #region Compiling
        private void PrepareCompile()
        {
            m_compiling = true;
            m_numCompilerErrors = 0;

            if ( (m_logFile != null) && (m_logWriter == null) )
            {
                if ( File.Exists( m_logFile ) )
                {
                    // make writable
                    FileAttributes atts = File.GetAttributes( m_logFile );
                    if ( (atts & FileAttributes.ReadOnly) == FileAttributes.ReadOnly )
                    {
                        try
                        {
                            File.SetAttributes( m_logFile, atts & (~FileAttributes.ReadOnly) );
                        }
                        catch
                        {
                        }
                    }
                }
                else
                {
                    string logPath = Path.GetDirectoryName(m_logFile);
                    if (Directory.Exists(logPath) == false)
                        Directory.CreateDirectory(logPath);
                }

                m_logWriter = new StreamWriter( m_logFile, true );
            }

            WriteOutputString( "" );
            WriteOutputString( "Command Sent to Compiler:" );
            WriteOutputString( "\t" + this.BuildCompilerCommand("%1", this.m_platform) );
            WriteOutputString( "Current Directory: " + Directory.GetCurrentDirectory() );
            WriteOutputString( "" );

            m_ScriptEditor.PreCompile();
        }

        private string BuildCompilerCommand(string filename, Platform platform)
        {
            return m_userEditorSettings.BuildCompileScriptCommand(filename, platform);
        }

        private string ResourceCompilerCommand(string filename, string outputFilename, Platform platform)
        {
            return m_userEditorSettings.ResourceCompileScriptCommand(filename, outputFilename, platform);
        }

        private void CompileComplete( int numErrors, string projectListFileName )
        {
            WriteOutputString( "---------------- Compiled with " + numErrors + " error(s) ----------------" );

            m_ScriptEditor.PostCompile();

            if ( numErrors == 0 )
            {
                // signals that we have just completed a project compile
                if ( projectListFileName != null )
                {
                    ExecutePostCompileCommand( projectListFileName );
                }
            }

            m_numCompilerErrors = numErrors;
            m_compiling = false;
        }

        private void BreakOnCompileError( int numErrors )
        {
            WriteOutputString( "---------------- Stop compile.  " + numErrors + " error(s) found. ----------------" );

            m_ScriptEditor.PostCompile();

            m_numCompilerErrors = numErrors;
            m_compiling = false;
        }

        private void CompilerFailure( string error )
        {
            WriteOutputString( "Compiler Failure: " + error );

            m_ScriptEditor.PostCompile();

            m_numCompilerErrors = 1;	// so we return an error code
            m_compiling = false;
        }

        private void CompileFilesNotFound( IEnumerable<string> filenames )
        {
            foreach (string filename in filenames)
            {
                WriteOutputString( "'" + filename + "' doesn't exist." );
            }
        }

        private void CompileFileUpToDate( string filename )
        {
            WriteOutputString( "'" + filename + "' is up to date." );
        }

        private void CompileListUpToDate( string projectListFileName )
        {
            m_ScriptEditor.PostCompile();

            if ( projectListFileName != null )
            {
                WriteOutputString( "---------------- Project is up to date ----------------" );

                ExecutePostCompileCommand( projectListFileName );
            }

            m_compiling = false;
        }

        private void NothingToCompile()
        {
            WriteOutputString( "---------------- Nothing to compile ----------------" );

            m_ScriptEditor.PostCompile();

            m_compiling = false;
        }

        private int ProcessCompilerOutputString( string output )
        {
            int numErrors = 0;
            string filename = null;

            bool foundTraceback = false;

            string[] lines = output.Split( '\n' );
            foreach ( string line in lines )
            {
                ErrorType type = ErrorType.Errors;
                string errorFilename = null;
                int lineNumber = -1;
                string text = null;
                if ( m_compiler.ParseCompileOutputLine( line, ref type, ref errorFilename, ref lineNumber, ref text ) )
                {
                    if ( lineNumber > -1 )
                    {
                        if (type == ErrorType.Errors)
                        {
                            ++numErrors;
                        }
                    }
                }
                else if ( line.StartsWith( "[traceback]" ) || line.Contains( "ExceptMain: Abnormal exit." ) )
                {
                    if ( !foundTraceback )
                    {
                        foundTraceback = line.StartsWith( "[traceback]" );

                        ++numErrors;
                    }
                }
                else
                {
                    m_compiler.ParseCompileFileLine( line, ref filename );
                }
            }

            if ( (numErrors == 0) && (filename != null) )
            {
                m_ScriptEditor.SetFileCompiled(filename.ToLower());
                m_ScriptEditor.SetFileResourced(filename.ToLower(), this.m_ProjectExplorer.UserProjectSettings.SelectedPlatform);
            }

            WriteOutputString( output );

            return numErrors;
        }

        /// <summary>
        /// Starts the process defined by CurrentCompilingSettings.PostCompileCommand, providing listname as the last parameter.
        /// Redirects standard ouput to our Output Window.
        /// </summary>
        /// <param name="listname"></param>
        private void ExecutePostCompileCommand( string listname )
        {
            if ( m_userEditorSettings.CurrentCompilingSettings.PostCompileCommand == "" )
            {
                return;
            }

            ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand(
                ApplicationSettings.Environment.Subst( m_userEditorSettings.CurrentCompilingSettings.PostCompileCommand ) + " " + listname );
            if ( psInfo != null )
            {
                psInfo.RedirectStandardOutput = true;
                psInfo.UseShellExecute = false;
                psInfo.CreateNoWindow = true;

                WriteOutputString( "Performing Post-Compile Event..." );

                Process p = null;
                try
                {
                    p = Process.Start( psInfo );

                    // redirect standard output to our Output Window
                    WriteOutputString( p.StandardOutput.ReadToEnd() );

                    // wait for the process to end
                    p.WaitForExit();
                }
                catch ( Exception exception )
                {
                    WriteOutputString( $"There was an error executing '{psInfo.FileName} {psInfo.Arguments}':\n{exception.ToString()}");
                }

                WriteOutputString( "Post-Compile Event Complete." );
            }
        }

        /// <summary>
		///
		/// </summary>
		/// <param name="filename"></param>
		/// <returns>true of its ok to save</returns>
		public bool MakeFileWritableOnCompile( string filename )
		{
			// see if exists
			if ( !File.Exists(filename) )
			{
				return true;
			}

			// see if it's already writable
			FileAttributes attributes = File.GetAttributes( filename );
			if ( (attributes & FileAttributes.ReadOnly) != FileAttributes.ReadOnly )
			{
				return true;
			}

			// make writable
			try
			{
				File.SetAttributes( filename, attributes&(~FileAttributes.ReadOnly) );
				return true;
			}
			catch (Exception e)
			{
				WriteOutputString( $"Could not make '{filename}' writable before compile:\n{e.Message}");
			}

			return false;
        }
        #endregion

        #region Load Layout
        /// <summary>
        /// Loads the layout
        /// </summary>
        /// <returns><c>false</c> on error.</returns>
        private bool LoadLayout()
        {
            // load the shared settings file
            string filename = ApplicationSettings.GetCommonSettingsFilename( false );
            if ( filename == null )
            {
                return false;
            }

            // see if it's the old layout file
            XmlTextReader reader = null;
            float version = 0.0f;
            try
            {
                reader = new XmlTextReader( filename );

                while ( reader.Read() )
                {
                    if ( reader.Name == "Version" )
                    {
                        if ( reader.IsStartElement() )
                        {
                            version = float.Parse( reader.ReadString() );
                        }

                        break;
                    }
                }

                reader.Close();
            }
            catch ( Exception e )
            {
                if ( reader != null )
                {
                    reader.Close();
                }

                WriteOutputString( $"Could not parse old layout file:\n{e.ToString()}");

                Application.Exit();
                return false;
            }

            if ( version == UserEditorSettings.SettingsVersion )
            {
                // if we got here, we have a new layout file
                string error = null;
                if ( !m_userEditorSettings.LoadFile( filename, out error ) )
                {
                    WriteOutputString($"Error Loading Settings File: {error}");

                    Application.Exit();
                    return false;
                }

                // <HACK>
                // ALupinacci 6/1/09 - Copy the PostCompileCommand from the ProjectSettings (which is
                // no longer used) to each of the CurrentCompilingSettings and then wipe the ProjectSetting's
                // copy of the PostCompileCommand.
                if ( !String.IsNullOrEmpty( m_userEditorSettings.Project.PostCompileCommand ) )
                {
                    foreach ( CompilingSettings compilingSettings in m_userEditorSettings.CurrentCompilingSettingsList )
                    {
                        compilingSettings.PostCompileCommand = m_userEditorSettings.Project.PostCompileCommand;
                    }

                    m_userEditorSettings.Project.PostCompileCommand = String.Empty;
                }
                // </HACK>
            }

            foreach ( LanguageSettings l in m_userEditorSettings.LanguageSettingsList )
            {
                m_ScriptEditor.SetParserPluginLanguageSettings( l );
            }

            foreach ( CompilingSettingsCollection collection in m_userEditorSettings.LanguageCompilingSettings.Values )
            {
                m_ScriptEditor.SetParserPluginCompilingSettings( collection.CurrentSettings );
            }

            return true;
        }
        #endregion

        #region Misc
        private void OverrideCompilingSettings( List<rageCommandLineItem> commands )
        {
            foreach ( rageCommandLineItem item in commands )
            {
                if ( !item.WasSet )
                {
                    // only override those items that were set
                    continue;
                }

                string val = item.Value as string;

                if ( item.Name == "break" )
                {
                    if ( val == string.Empty )
                    {
                        m_compiler.BreakOnCompileError = true;
                    }
                    else
                    {
                        m_compiler.BreakOnCompileError = Boolean.Parse( val );
                    }
                }
                else
                {
                    if ( val == string.Empty )
                    {
                        m_userEditorSettings.CurrentCompilingSettings.SetValue( item.Name, "True" );
                    }
                    else
                    {
                        m_userEditorSettings.CurrentCompilingSettings.SetValue( item.Name, val );
                    }
                }
            }

            // update the includes parser
            m_ScriptEditor.SetParserPluginCompilingSettings( m_userEditorSettings.CurrentCompilingSettings );
        }

        private void WriteOutputString( string output )
        {
            string text = output.Replace( "\r\n\r\r", "" );
            string[] lines = text.Split( '\n' );

            foreach ( string line in lines )
            {
                if ( m_logWriter != null )
                {
                    m_logWriter.WriteLine( line );
                }
                else
                {
                    Console.Out.WriteLine( line );
                    Console.Out.Flush();
                }
            }
        }
        #endregion

        #region Project Explorer Event Handlers
        void ProjectExplorer_ExecuteCompileAction( ProjectExplorer.ProjectCompileAction action, List<string> filenames, Platform platform )
        {
            switch ( action )
            {
                case ProjectExplorer.ProjectCompileAction.CompleFile:
                    m_compiler.CompileFile(filenames[0] as string, platform);
                    break;
                case ProjectExplorer.ProjectCompileAction.CompileFiles:
                    m_compiler.CompileProject(m_ProjectExplorer.ProjectFileName, filenames, platform);
                    break;
                case ProjectExplorer.ProjectCompileAction.RecompileFiles:
                    m_compiler.RecompileProject(m_ProjectExplorer.ProjectFileName, filenames, platform);
                    break;
            }
        }
        #endregion
    }
}
