using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using ragScriptEditorShared;

using RSG.Base.Forms;
using RSG.Base.Logging;
using RSG.Configuration;
using RSG.Platform;

namespace ragScriptCompiler
{
    class Program
    {
        const string ArgPlatformName = "platformName";
        const string ArgGuiName = "guiName";

        static int Main( string[] args )
        {
            ToolsConfig toolsConfig = new ToolsConfig();
            LogFactory.Initialize();

            ApplicationSettings.ProductName = string.Empty;
            ApplicationSettings.ExecutableFullPathName = Application.ExecutablePath;
            ApplicationSettings.Mode = ApplicationSettings.ApplicationMode.Compiler;

            string platformName = null;

            try
            {
                // scan for -productName
                bool foundProductName = false;
                for ( int i = 0; i < args.Length; ++i )
                {
                    string name = args[i].Trim();
                    if (name.StartsWith($"-{ArgGuiName}"))
                    {
                        if (name.StartsWith($"{ArgGuiName}=") )
                        {
                            ApplicationSettings.ProductName = name.Substring($"{ArgGuiName}=".Length);
                            foundProductName = true;
                        }
                        else if ( i + 1 < args.Length )
                        {
                            i++;
                            ApplicationSettings.ProductName = args[i];
                            foundProductName = true;
                        }
                    }

                    if (name.StartsWith($"-{ArgPlatformName}"))
                    {
                        if (name.StartsWith($"-{ArgPlatformName}="))
                        {
                            platformName = name.Substring($"-{ArgPlatformName}=".Length);
                        }
                        else if (i + 1 < args.Length)
                        {
                            i++;
                            platformName = args[i];
                        }
                    }
                }

                if ( !foundProductName )
                {
                    // try to locate default ProductName
                    string[] files = Directory.GetFiles( ApplicationSettings.ExecutablePath, "*.exe" );
                    foreach ( string file in files )
                    {
                        string name = Path.GetFileNameWithoutExtension( file );
                        if ( name.EndsWith( "ScriptEditor" ) )
                        {
                            ApplicationSettings.ProductName = name;
                            break;
                        }
                    }
                }

                if ( ApplicationSettings.ProductName == string.Empty )
                {
                    Console.Out.WriteLine( "Error: " + Application.ProductName + " unable to determine guiName." );
                    return 1;
                }

                if (string.IsNullOrWhiteSpace(platformName))
                {
                    Console.Out.WriteLine($"Error: arg -{ArgPlatformName} not set.");
                    return 1;
                }

                if (!Enum.TryParse(platformName, out Platform platform))
                {
                    Console.Out.WriteLine($"Error: can't parse -{ArgPlatformName} value {platformName} into a platform.");
                    return 1;
                }

                ScriptCompiler c = new ScriptCompiler(platform);
                return c.Run( args );
            }
            catch ( Exception e )
            {
                List<string> attachments = new List<string>();

                string userSettingsFilename = ApplicationSettings.GetCommonSettingsFilename( false );
                if ( !String.IsNullOrEmpty( userSettingsFilename ) && File.Exists( userSettingsFilename ) )
                {
                    attachments.Add( userSettingsFilename );
                }

                string projectSettingsFilename = ApplicationSettings.LastProjectSettingsFile;
                if ( !String.IsNullOrEmpty( projectSettingsFilename ) && File.Exists( projectSettingsFilename ) )
                {
                    attachments.Add( projectSettingsFilename );
                }

                IConfig config = ConfigFactory.CreateConfig(LogFactory.ApplicationLog);
                UnhandledExceptionDialog eForm = new UnhandledExceptionDialog( "Script Compiler", attachments,
                    config.Project.ToolsEmailAddresses.First().Address, e );

                try
                {
                    eForm.SendErrorReport();
                }
                catch ( Exception ex )
                {
                    ApplicationSettings.ShowMessage( null, ex.Message, "SendEmail failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Error, DialogResult.OK );
                }
                return 1;
            }
        }
    }
}
