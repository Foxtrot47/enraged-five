using System;

using ragScriptEditorShared;
using ragCore;

namespace ragScriptDebugger
{
	/// <summary>
	/// Summary description for External.
	/// </summary>
	public class External : ragCore.IWidgetPlugIn
	{
		public External()
		{
			//
			// TODO: Add constructor logic here
			//
		}

        public void AddHandlers( BankRemotePacketProcessor packetProcessor )
		{
			WidgetScriptEditor.AddHandlers(packetProcessor);
		}

		public void RemoveHandlers( BankRemotePacketProcessor packetProcessor )
		{
			WidgetScriptEditor.RemoveHandlers( packetProcessor );
		}
	}
}
