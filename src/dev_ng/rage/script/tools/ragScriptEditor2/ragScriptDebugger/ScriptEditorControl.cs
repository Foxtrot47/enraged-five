//#define WIP

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using ActiproSoftware.SyntaxEditor;
using RSG.Base.Forms;
using ragScriptEditorShared;
using TD.SandDock;

namespace ragScriptDebugger
{
    public partial class ScriptEditorControl : UserControl
    {
        public ScriptEditorControl( Control parentControl )
        {
            // find the top-level form
            Control parent = parentControl;
            while ( (parent != null) && !(parent is Form) )
            {
                parent = parent.Parent;
            }

            m_parentControl = parent;

            InitializeComponent();
            InitializeAdditionalComponents();
        }

        #region Init
        private void InitializeAdditionalComponents()
        {
            this.sandDockManager1.DocumentOverflow = sm_userEditorSettings.Files.DocumentOverflowScrollable
                ? DocumentOverflowMode.Scrollable : DocumentOverflowMode.Menu;

            m_editorToolbar = new EditorToolbar( m_parentControl );
            m_ScriptEditor = new ScriptEditor( m_parentControl, sm_userEditorSettings, null, m_editorToolbar, null );

            // Script Editor
            m_ScriptEditor.GetToolstripItemByTag = new ScriptEditor.GetToolstripItemByTagDelegate( ScriptEditor_GetToolbarItemByTag );
            m_ScriptEditor.HighlightLanguage += new ScriptEditor.HighlightLanguageEventHandler( ScriptEditor_HighlightLanguage );
            m_ScriptEditor.FileClosing += new ScriptEditor.FileClosingEventHandler( ScriptEditor_FileClosing );
            m_ScriptEditor.FileClosed += new ScriptEditor.FileClosedEventHandler( ScriptEditor_FileClosed );
            m_ScriptEditor.FileClosingAll += new ScriptEditor.FileAllPreOperationEventHandler( ScriptEditor_FileClosingAll );
            m_ScriptEditor.FileLoading += new ScriptEditor.FileLoadingEventHandler( ScriptEditor_FileLoading );
            m_ScriptEditor.FileLoaded += new ScriptEditor.FileLoadedEventHandler( ScriptEditor_FileLoaded );
            m_ScriptEditor.FileSaving += new ScriptEditor.FileSavingEventHandler( ScriptEditor_FileSaving );
            m_ScriptEditor.FileSavingAll += new ScriptEditor.FileAllPreOperationEventHandler( ScriptEditor_FileSavingAll );

            // Editor Toolbar
            m_editorToolbar.ToolbarButtonClicked += new EditorToolbar.ToolbarButtonClickedEventHandler( editorToolBar_ButtonClick );
            m_editorToolbar.ToolbarAppActionButtonClicked += new EventHandler( editorToolbar_ToolbarAppActionButtonClicked );
            m_editorToolbar.DebuggerOptionsDropDownMenuOpening += new EditorToolbar.DebuggerOptionsDropDownMenuOpeningEventHandler( editorToolbar_DebuggerOptionsDropDownMenuOpening );
            m_editorToolbar.ShowLineNumbersChanged += new EditorToolbar.ShowLineNumbersChangedEventHandler( editorToolbar_ShowLineNumbersChanged );
            m_editorToolbar.HighlightEnumsChanged += new EditorToolbar.HighlightEnumsChangedEventHandler( editorToolbar_HighlightEnumsChanged );
            m_editorToolbar.HighlightQuickInfoPopupsChanged += new EditorToolbar.HighlightQuickInfoPopupsChangedEventHandler( editorToolbar_HighlightQuickInfoPopupsChanged );
            m_editorToolbar.HighlightDisabledCodeBlocksChanged += new EditorToolbar.HighlightDisabledCodeBlocksChangedEventHandler( editorToolbar_HighlightDisabledCodeBlocksChanged );
            m_editorToolbar.ChangeIncludesClicked += new EventHandler( editorToolbar_ChangeIncludesClicked );

            foreach ( LanguageSettings l in sm_userEditorSettings.LanguageSettingsList )
            {
                m_ScriptEditor.SetParserPluginLanguageSettings( l );
            }

            EnableSemanticParsingService( false );

            foreach ( CompilingSettingsCollection collection in sm_userEditorSettings.LanguageCompilingSettings.Values )
            {
                m_ScriptEditor.SetParserPluginCompilingSettings( collection.CurrentSettings );
            }

            EnableSemanticParsingService( sm_userEditorSettings.Files.ParsingServiceEnabled );

            this.sandDockManager1.AllowMiddleButtonClosure = sm_userEditorSettings.Files.MiddleClickClose;

            m_editorToolbar.Init( sm_userEditorSettings.Toolbar, this.toolStripContainer1, 
                sm_userEditorSettings.Files.ShowLineNumbers, sm_userEditorSettings.IntellisenseSettings.HighlightEnums,
                sm_userEditorSettings.IntellisenseSettings.HighlightQuickInfoPopups,
                sm_userEditorSettings.IntellisenseSettings.HighlightDisabledCodeBlocks );

            // Disable appropriate toolbar and menu items.
            m_ScriptEditor.SetCurrentEditorFromTabControl( null );

            this.tabbedDocument1.Close();
        }
        #endregion

        #region Static Variables
        private static UserEditorSettings sm_userEditorSettings = new UserEditorSettings();
        private static TextBackgroundColorSettings sm_textBackgroundColorSettings = new TextBackgroundColorSettings();
        #endregion

        #region Variables
        private Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> m_MapDockControlToEditor
            = new Dictionary<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor>();
        private Dictionary<ActiproSoftware.SyntaxEditor.SyntaxEditor, TD.SandDock.DockControl> m_MapEditorToDockControl
            = new Dictionary<ActiproSoftware.SyntaxEditor.SyntaxEditor, TD.SandDock.DockControl>();

        private TD.SandDock.DockControl m_SelectedDockControl = null;

        private Control m_parentControl;
        private ScriptEditor m_ScriptEditor;
        private EditorToolbar m_editorToolbar;

        private int m_TabIndex = 0;
        #endregion

        #region Properties
        public ScriptEditor ScriptEditor
        {
            get
            {
                return m_ScriptEditor;
            }
        }

        public List<Control> ContextMenuControls
        {
            get
            {
                List<Control> controls = new List<Control>();
                controls.Add( this.panel1 );
                controls.Add( this.label1 );
                return controls;
            }
        }

        public static UserEditorSettings EditorSettings
        {
            get
            {
                return sm_userEditorSettings;
            }
        }
        #endregion      

        #region Overrides
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( components != null )
                {
                    components.Dispose();
                }

                m_ScriptEditor.GetToolstripItemByTag = null;
                m_ScriptEditor.HighlightLanguage -= new ScriptEditor.HighlightLanguageEventHandler( ScriptEditor_HighlightLanguage );
                m_ScriptEditor.FileClosing -= new ScriptEditor.FileClosingEventHandler( ScriptEditor_FileClosing );
                m_ScriptEditor.FileClosed -= new ScriptEditor.FileClosedEventHandler( ScriptEditor_FileClosed );
                m_ScriptEditor.FileClosingAll -= new ScriptEditor.FileAllPreOperationEventHandler( ScriptEditor_FileClosingAll );
                m_ScriptEditor.FileLoading -= new ScriptEditor.FileLoadingEventHandler( ScriptEditor_FileLoading );
                m_ScriptEditor.FileLoaded -= new ScriptEditor.FileLoadedEventHandler( ScriptEditor_FileLoaded );
                m_ScriptEditor.FileSaving -= new ScriptEditor.FileSavingEventHandler( ScriptEditor_FileSaving );
                m_ScriptEditor.FileSavingAll -= new ScriptEditor.FileAllPreOperationEventHandler( ScriptEditor_FileSavingAll );

                // Editor Toolbar
                m_editorToolbar.ToolbarButtonClicked -= new EditorToolbar.ToolbarButtonClickedEventHandler( editorToolBar_ButtonClick );
                m_editorToolbar.ToolbarAppActionButtonClicked -= new EventHandler( editorToolbar_ToolbarAppActionButtonClicked );
                m_editorToolbar.DebuggerOptionsDropDownMenuOpening -= new EditorToolbar.DebuggerOptionsDropDownMenuOpeningEventHandler( editorToolbar_DebuggerOptionsDropDownMenuOpening );
                m_editorToolbar.ShowLineNumbersChanged -= new EditorToolbar.ShowLineNumbersChangedEventHandler( editorToolbar_ShowLineNumbersChanged );
                m_editorToolbar.HighlightEnumsChanged -= new EditorToolbar.HighlightEnumsChangedEventHandler( editorToolbar_HighlightEnumsChanged );
                m_editorToolbar.HighlightQuickInfoPopupsChanged -= new EditorToolbar.HighlightQuickInfoPopupsChangedEventHandler( editorToolbar_HighlightQuickInfoPopupsChanged );
                m_editorToolbar.HighlightDisabledCodeBlocksChanged -= new EditorToolbar.HighlightDisabledCodeBlocksChangedEventHandler( editorToolbar_HighlightDisabledCodeBlocksChanged );
                m_editorToolbar.ChangeIncludesClicked -= new EventHandler( editorToolbar_ChangeIncludesClicked );

                m_editorToolbar.Dispose();
                m_ScriptEditor.Dispose();
            }
            base.Dispose( disposing );
        }
        #endregion

        #region Toolbar Event Handlers
        private void editorToolBar_ButtonClick( object sender, ToolStripItemClickedEventArgs e )
        {
            ToolbarButtonSetting bs = e.ClickedItem.Tag as ToolbarButtonSetting;
            if ( bs != null )
            {
                ScriptEditor.AppAction action = (ScriptEditor.AppAction)Enum.Parse( typeof( ScriptEditor.AppAction ), bs.Tag );
                m_ScriptEditor.ExecuteAppAction( action );
            }
        }

        private void editorToolbar_ToolbarAppActionButtonClicked( object sender, EventArgs e )
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            if ( menuItem != null )
            {
                ScriptEditor.AppAction action = (ScriptEditor.AppAction)Enum.Parse( typeof( ScriptEditor.AppAction ), menuItem.Tag.ToString() );
                m_ScriptEditor.ExecuteAppAction( action );
            }
        }

        private void editorToolbar_DebuggerOptionsDropDownMenuOpening( object sender, DebuggerOptionsEventArgs e )
        {
            if ( m_ScriptEditor.CurrentSyntaxEditor != null )
            {
                e.DropDownButton.DropDownItems[(int)EditorToolbar.DebuggerOptions.StartAutomaticOutlining].Enabled
                    = (m_ScriptEditor.CurrentSyntaxEditor.Document.Outlining.Mode != OutliningMode.Automatic);
                e.DropDownButton.DropDownItems[(int)EditorToolbar.DebuggerOptions.StartManualOutlining].Enabled
                    = (m_ScriptEditor.CurrentSyntaxEditor.Document.Outlining.Mode != OutliningMode.Manual);
                e.DropDownButton.DropDownItems[(int)EditorToolbar.DebuggerOptions.StopOutlining].Enabled
                    = (m_ScriptEditor.CurrentSyntaxEditor.Document.Outlining.Mode != OutliningMode.None);
            }
            else
            {
                e.DropDownButton.DropDownItems[(int)EditorToolbar.DebuggerOptions.StartAutomaticOutlining].Enabled = false;
                e.DropDownButton.DropDownItems[(int)EditorToolbar.DebuggerOptions.StartManualOutlining].Enabled = false;
                e.DropDownButton.DropDownItems[(int)EditorToolbar.DebuggerOptions.StopOutlining].Enabled = false;
            }
        }

        private void editorToolbar_ShowLineNumbersChanged( object sender, ShowLineNumbersChangedEventArgs e )
        {
            sm_userEditorSettings.Files.ShowLineNumbers = e.ShowLineNumbers;

            m_ScriptEditor.SetLineNumberMarginVisible( e.ShowLineNumbers );
        }

        private void editorToolbar_HighlightEnumsChanged( object sender, HighlightEnumsChangedEventArgs e )
        {
            sm_userEditorSettings.IntellisenseSettings.HighlightEnums = e.HighlightEnums;
            m_ScriptEditor.SetIntellisenseOptions( true );
        }

        private void editorToolbar_HighlightQuickInfoPopupsChanged( object sender, HighlightQuickInfoPopupsChangedEventArgs e )
        {
            sm_userEditorSettings.IntellisenseSettings.HighlightQuickInfoPopups = e.HighlightQuickInfoPopups;

            foreach ( SyntaxEditor editor in m_MapEditorToDockControl.Keys )
            {
                editor.IntelliPrompt.QuickInfo.CollapsedOutliningNodeSyntaxHighlightingEnabled = e.HighlightQuickInfoPopups;
            }

            m_ScriptEditor.SetIntellisenseOptions( false );
        }

        private void editorToolbar_HighlightDisabledCodeBlocksChanged( object sender, HighlightDisabledCodeBlocksChangedEventArgs e )
        {
            sm_userEditorSettings.IntellisenseSettings.HighlightDisabledCodeBlocks = e.HighlightDisabledCodeBlocks;
            m_ScriptEditor.SetIntellisenseOptions( true );
        }

        private void editorToolbar_ChangeIncludesClicked( object sender, EventArgs e )
        {
            CompilingSettings currentSettings = sm_userEditorSettings.CurrentCompilingSettings.Clone() as CompilingSettings;

            ChangeIncludesForm.ShowIt( m_parentControl as Form, m_ScriptEditor.BuildScriptFileDialogFilter(), currentSettings,
                m_ScriptEditor.CurrentLanguageDefaultEditorSettings.CompilingSettingsList[0], 
                new ChangeIncludesForm.SaveSettingsDelegate( editorToolbar_SaveSettings ) );

            //if ( result == DialogResult.OK )
            //{
            //    m_ScriptEditor.RefreshIntellisenseQuick();
            //}
        }

        private void editorToolbar_SaveSettings( CompilingSettings compilingSettings )
        {
            bool dirtyCompiling = !sm_userEditorSettings.CurrentCompilingSettings.Equals( compilingSettings );

            sm_userEditorSettings.CurrentCompilingSettings.Copy( compilingSettings );

            if ( dirtyCompiling )
            {
                EnableSemanticParsingService( (sm_userEditorSettings.Project as NonProjectProjectSettings).ParsingServiceEnabled );

                m_ScriptEditor.SetParserPluginCompilingSettings( compilingSettings );

                EnableSemanticParsingService( sm_userEditorSettings.Files.ParsingServiceEnabled );

                // we could call RefreshIntellisenseQuick here, but we'll wait until the user has closed the settings form
            }
        }
        #endregion

        #region Misc
        public void AddMouseUp( MouseEventHandler eh )
        {
            this.MouseUp += eh;
            m_ScriptEditor.MouseUp += eh;
            m_editorToolbar.AddMouseUp( eh );
        }

        public void RemoveMouseUp( MouseEventHandler eh )
        {
            this.MouseUp -= eh;
            m_ScriptEditor.MouseUp -= eh;
            m_editorToolbar.AddMouseUp( eh );
        }

        public void AddMouseDown( MouseEventHandler eh )
        {
            this.MouseDown += eh;
            m_ScriptEditor.MouseDown += eh;
            m_editorToolbar.AddMouseDown( eh );
        }

        public void RemoveMouseDown( MouseEventHandler eh )
        {
            this.MouseDown -= eh;
            m_ScriptEditor.MouseDown -= eh;
            m_editorToolbar.AddMouseDown( eh );
        }

        /// <summary>
        /// Enables or disables the <see cref="SemanticParserService"/>.
        /// </summary>
        /// <param name="enable"><c>true</c> will enable the service, if it is not already started.  <c>false</c> will stop it if it is running.</param>
        private void EnableSemanticParsingService( bool enable )
        {
            if ( !enable )
            {
                if ( SemanticParserService.IsRunning )
                {
                    SemanticParserService.Stop();
                }
            }
            else
            {
                if ( !SemanticParserService.IsRunning )
                {
                    SemanticParserService.Start();
                }
            }
        }
        #endregion

        #region Sand Dock Manager Event Handlers
        private void sandDockManager1_ActiveTabbedDocumentChanged( object sender, EventArgs e )
        {
            // WARNING: don't use ActiveTabbedDocument if we enable floating tabbed documents
            m_ScriptEditor.SetCurrentEditorFromTabControl( this.sandDockManager1.ActiveTabbedDocument );

            m_editorToolbar.Editor = m_ScriptEditor.CurrentSyntaxEditor;
        }

        private void sandDockManager1_ShowControlContextMenu( object sender, ShowControlContextMenuEventArgs e )
        {
            m_SelectedDockControl = e.DockControl;

            this.documentTabContextMenuStrip.Show( this, this.PointToClient( Control.MousePosition ) );
        }
        #endregion

        #region Script Editor Event Handlers
        ToolStripItem ScriptEditor_GetToolbarItemByTag( ScriptEditor.AppAction appAction )
        {
            List<ToolStrip> toolStrips = m_editorToolbar.ToolStrips;
            foreach ( ToolStrip toolStrip in toolStrips )
            {
                foreach ( ToolStripItem toolStripItem in toolStrip.Items )
                {
                    if ( (toolStripItem.Tag != null) && (toolStripItem.Tag is ToolbarButtonSetting) )
                    {
                        ToolbarButtonSetting bs = toolStripItem.Tag as ToolbarButtonSetting;
                        if ( !bs.IsCustomButton )
                        {
                            ScriptEditor.AppAction tagAction = (ScriptEditor.AppAction)Enum.Parse( typeof( ScriptEditor.AppAction ), bs.Tag );
                            if ( tagAction == appAction )
                            {
                                return toolStripItem;
                            }
                        }
                    }
                }
            }

            return null;
        }

        void ScriptEditor_HighlightLanguage( object sender, SyntaxEditor editor )
        {
            // share some code by calling our other language highlighter
            StyleEditorForm_ApplyStyles( editor, sm_userEditorSettings.LanguageHighlightingStylesSettings.LanguageHighlightingStyles,
                sm_userEditorSettings.LanguageHighlightingStylesSettings.Font, sm_userEditorSettings.LanguageHighlightingStylesSettings.TextBackgroundColor );
        }

        void ScriptEditor_FileClosing( object sender, FileClosingEventArgs e )
        {
            Debug.Assert( m_MapDockControlToEditor.ContainsKey( e.DockControl ), "m_MapDockControlToEditor.ContainsKey( " + e.DockControl.TabText + " )" );
        }

        void ScriptEditor_FileClosed( object sender, FileClosedEventArgs e )
        {
            if ( !e.Cancelled )
            {
                m_MapDockControlToEditor.Remove( e.DockControl );
                m_MapEditorToDockControl.Remove( e.Editor );
            }
            else
            {
                if ( !String.IsNullOrEmpty( e.Reason ) )
                {
                    rageMessageBox.ShowError( m_parentControl, 
                        String.Format( "'{0}'\n\n{1}", e.Editor.Document.Filename, e.Reason ), "Close Error" );
                }
            }
        }

        void ScriptEditor_FileClosingAll( object sender, FileAllPreOperationEventArgs e )
        {
            foreach ( string file in e.Files )
            {                    
                m_ScriptEditor.ClearParseRequests( null, file );
            }
        }

        void ScriptEditor_FileLoading( object sender, FileLoadingEventArgs e )
        {
            TabbedDocument tabDoc = new VisualStudioStyleTabbedDocument( this.sandDockManager1, e.Editor, e.Editor.Document.Filename );
            tabDoc.Guid = e.Guid;
            tabDoc.TabIndex = m_TabIndex++;
            tabDoc.PersistState = true;

            Debug.Assert( !e.IsNewFile, "!e.IsNewFile" );
            tabDoc.TabText = Path.GetFileName( e.Editor.Document.Filename );

            e.DockControl = tabDoc;
        }

        void ScriptEditor_FileLoaded( object sender, FileLoadedEventArgs e )
        {
            if ( !e.Cancelled )
            {
                this.tabbedDocument1.Close();

                if ( !m_MapDockControlToEditor.ContainsKey( e.DockControl ) )
                {
                    m_MapDockControlToEditor.Add( e.DockControl, e.Editor );
                }

                if ( !m_MapEditorToDockControl.ContainsKey( e.Editor ) )
                {
                    m_MapEditorToDockControl.Add( e.Editor, e.DockControl );
                }

                if ( m_MapDockControlToEditor.Count == 1 )
                {
                    e.DockControl.AllowClose = false;
                }
            }
            else
            {
                rageMessageBox.ShowError( m_parentControl, e.Reason, "Load Error" );
            }
        }

        void ScriptEditor_FileSaving( object sender, FileSavingEventArgs e )
        {
            // do not allow saving
            e.Cancel = true;
        }

        void ScriptEditor_FileSavingAll( object sender, FileAllPreOperationEventArgs e )
        {
            // do not allowing saving
            e.Cancel = true;
        }
        #endregion

        #region StyleEditorForm Event Handlers
        private void StyleEditorForm_ApplyStyles( ActiproSoftware.SyntaxEditor.SyntaxEditor editor,
            Dictionary<string, LanguageHighlightingStyle> langStyles, Font font, Color textBackgroundColor )
        {
            editor.Font = font;

            // editor.BackColor = sm_userEditorSettings.LanguageHighlightingStylesSettings.TextBackgroundColor;
            VisualStudio2005SyntaxEditorRenderer renderer = editor.RendererResolved as VisualStudio2005SyntaxEditorRenderer;
            if ( renderer != null )
            {
                renderer.TextAreaBackgroundFill
                    = new ActiproSoftware.Drawing.SolidColorBackgroundFill( textBackgroundColor );
                renderer.LineNumberMarginBackgroundFill
                    = new ActiproSoftware.Drawing.SolidColorBackgroundFill( textBackgroundColor );
                renderer.SelectionMarginBackgroundFill
                    = new ActiproSoftware.Drawing.SolidColorBackgroundFill( textBackgroundColor );
            }

            //Disabled - This causes the language to be re-evaluated, losing its highlighting for a period of time where
            //people think the feature has broken.
            /*SyntaxLanguageCollection languages = SyntaxLanguage.GetLanguageCollection( editor.Document.Language );

            if ( languages != null )
            {
                editor.Document.Language.IsUpdating = true;

                // Loop through each language and update the highlighting langStyles
                foreach ( SyntaxLanguage language in languages )
                {
                    LanguageHighlightingStyle langStyle;
                    if ( langStyles.TryGetValue( language.Key, out langStyle ) )
                    {
                        foreach ( HighlightingStyle style in langStyle.HighlightingStyles )
                        {
                            language.HighlightingStyles.Add( style );
                        }
                    }
                }

                // Mark batch update complete
                editor.Document.Language.IsUpdating = false;
            }*/
        }
        #endregion

        #region Tab Context Menu Event Handlers
        private void documentTabContextMenuStrip_Closed( object sender, ToolStripDropDownClosedEventArgs e )
        {
            if ( e.CloseReason != ToolStripDropDownCloseReason.ItemClicked )
            {
                m_SelectedDockControl = null;
            }
        }

        private void closeTabToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( (m_SelectedDockControl != null) && m_SelectedDockControl.AllowClose )
            {
                m_SelectedDockControl.Close();
                m_SelectedDockControl = null;
            }
        }

        private void closeOtherTabsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            List<ActiproSoftware.SyntaxEditor.SyntaxEditor> editors = new List<SyntaxEditor>();
            foreach ( KeyValuePair<TD.SandDock.DockControl, ActiproSoftware.SyntaxEditor.SyntaxEditor> pair in m_MapDockControlToEditor )
            {
                if ( (pair.Key != m_SelectedDockControl) && pair.Key.AllowClose )
                {
                    editors.Add( pair.Value );
                }
            }

            m_ScriptEditor.CloseFiles( editors );
            m_SelectedDockControl = null;
        }

        private void closeAllTabsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_SelectedDockControl = null;

            closeOtherTabsToolStripMenuItem_Click( sender, e );
        }
        #endregion

        #region Static Functions
        /// <summary>
        /// Loads the layout
        /// </summary>
        /// <returns><c>false</c> on error.</returns>
        public static bool LoadLayout()
        {
            // load the shared settings file
            string filename = ApplicationSettings.GetCommonSettingsFilename( false );
            if ( filename == null )
            {
                return false;
            }

            // see if it's the old layout file
            XmlTextReader reader = null;
            float version = 0.0f;
            try
            {
                reader = new XmlTextReader( filename );

                while ( reader.Read() )
                {
                    if ( reader.Name == "Version" )
                    {
                        if ( reader.IsStartElement() )
                        {
                            version = float.Parse( reader.ReadString() );
                        }

                        break;
                    }
                }

                reader.Close();
            }
            catch ( Exception e )
            {
                if ( reader != null )
                {
                    reader.Close();
                }

                rageMessageBox.ShowExclamation( 
                    String.Format( "Could not parse layout file '{0}' to retrieve the version number.\n\n{1}\n\nThe application will continue but not all of your last session's settings can be restored.", filename, e.ToString() ),
                    "Problem Parsing Layout File!" );
                return false;
            }

            if ( version == UserEditorSettings.SettingsVersion )
            {
                // if we got here, we have a new layout file
                string error = null;
                if ( !sm_userEditorSettings.LoadFile( filename, out error ) )
                {
                    rageMessageBox.ShowExclamation( String.Format( "{0}\n\nThe application will continue but not all of your last session's settings can be restored.", error ),
                        String.Format( "Unable to Load Settings File Version {0}!", UserEditorSettings.SettingsVersion ) );
                    return false;
                }

                // <HACK>
                // ALupinacci 6/1/09 - Copy the PostCompileCommand from the ProjectSettings (which is
                // no longer used) to each of the CurrentCompilingSettings and then wipe the ProjectSetting's
                // copy of the PostCompileCommand.
                if ( !String.IsNullOrEmpty( sm_userEditorSettings.Project.PostCompileCommand ) )
                {
                    foreach ( CompilingSettings compilingSettings in sm_userEditorSettings.CurrentCompilingSettingsList )
                    {
                        compilingSettings.PostCompileCommand = sm_userEditorSettings.Project.PostCompileCommand;
                    }

                    sm_userEditorSettings.Project.PostCompileCommand = String.Empty;
                }
                // </HACK>
            }            
            else
            {
                rageMessageBox.ShowExclamation( 
                    String.Format( "Unable to load the settings from '{0}' version {1}.\n\nThe application will continue but not all of your last session's settings can be restored.", filename, version ),
                    "Unknown Settings File Version" );
                return false;
            }

            return true;
        }
        #endregion
    }
}
