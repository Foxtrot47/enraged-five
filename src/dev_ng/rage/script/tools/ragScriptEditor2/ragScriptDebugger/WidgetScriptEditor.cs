using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using ragCore;
using ragScriptEditorShared;
using ragWidgets;

namespace ragScriptDebugger
{
    /// <summary>
    /// Summary description for WidgetScriptEditor.
    /// </summary>
    public class WidgetScriptEditor : Widget
    {
        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static WidgetScriptEditor()
        {
            Assembly ass = Assembly.GetAssembly(typeof(WidgetScriptEditor));
            ApplicationSettings.ExecutableFullPathName = ass.Location;
            ApplicationSettings.ProductName = "ragScriptEditor";
            ApplicationSettings.Mode = ApplicationSettings.ApplicationMode.Debugger;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="pipe"></param>
        /// <param name="title"></param>
        /// <param name="memo"></param>
        /// <param name="scriptFile"></param>
        /// <param name="debugState"></param>
        /// <param name="fillColor"></param>
        public WidgetScriptEditor(BankPipe pipe, uint id, string title, string memo, string scriptFile, ScriptEditor.DebuggerState debugState, Color fillColor)
            : base(pipe, id, title, memo, fillColor, false)
        {
            m_scriptFileName = scriptFile;
            m_debuggerState = debugState;

            this.Persist = true;    // maybe make this an option set by the game
        }
        #endregion // Constructor(s)

        #region Variables
        private string m_scriptFileName;
        private ScriptEditor.DebuggerState m_debuggerState;
        private Mutex m_mutex = new Mutex();

        private Dictionary<string, BreakpointInfo> m_programCountersByFilename
            = new Dictionary<string, BreakpointInfo>();
        private Dictionary<string, Dictionary<int, BreakpointInfo>> m_breakpointsByFilename
            = new Dictionary<string, Dictionary<int, BreakpointInfo>>();

        public event GenericValueEventHandler Played; // I don't like this event name ;)
        public event GenericValueEventHandler Paused;
        public event GenericValueEventHandler Stopped;
        public event GenericValueEventHandler BreakpointAdded;
        public event GenericValueEventHandler BreakpointsAllowed;
        public event GenericValueEventHandler DebuggerSteppedInto;
        public event GenericValueEventHandler DebuggerSteppedOut;
        public event GenericValueEventHandler DebuggerSteppedOver;
        public event GenericValueEventHandler ProgramCounterSet;

        /// <summary>
        /// Thread synchronisation object.
        /// </summary>
        private static object s_syncObj = new object();

        /// <summary>
        /// Reference count for number of times this object has been initialised.
        /// </summary>
        private static int s_refCount = 0;
        #endregion

        #region Properties
        public string ScriptFileName
        {
            get
            {
                return m_scriptFileName;
            }
        }

        public ScriptEditor.DebuggerState DebugState
        {
            get
            {
                return m_debuggerState;
            }
            set
            {
                m_debuggerState = value;
            }
        }

        public Dictionary<string, Dictionary<int, BreakpointInfo>> BreakpointsInFiles
        {
            get
            {
                return m_breakpointsByFilename;
            }
        }
        #endregion

        #region Widget Overrides
        public override bool AllowPersist
        {
            get
            {
                return false;
            }
        }

        public override int GetWidgetTypeGUID() 
        { 
            return GetStaticGUID(); 
        }
        
        public static int GetStaticGUID() 
        { 
            return ComputeGUID( 's', 'c', 'e', 'd' ); 
        }
        
        public override void DeSerialize( System.Xml.XmlTextReader reader ) 
        {
            string name = reader.Name;

            while ( reader.Read() )
            {
                if ( !reader.IsStartElement() )
                {
                    if ( reader.Name == name )
                    {
                        break; // we're done
                    }
                }
                else if ( reader.Name == "File" )
                {
                    string filename = reader["name"];
                    if ( filename != null )
                    {
                        while ( reader.Read() )
                        {
                            if ( !reader.IsStartElement() )
                            {
                                if ( reader.Name == "File" )
                                {
                                    break; // we're done
                                }
                            }
                            else if ( reader.Name == "Breakpoint" )
                            {
                                string lineString = reader["line"];
                                if ( lineString != null )
                                {
                                    int line = int.Parse( lineString );                                    
                                    bool breakOnAllThreads = false;
                                    bool stopGame = false;

                                    string breakOnAllThreadsString = reader["breakOnAllThreads"];
                                    if ( breakOnAllThreadsString != null )
                                    {
                                        breakOnAllThreads = bool.Parse( breakOnAllThreadsString );
                                    }

                                    string stopGameString = reader["stopGame"];
                                    if ( stopGameString != null )
                                    {
                                        stopGame = bool.Parse( stopGameString );
                                    }

                                    BreakpointEventArgs e = new BreakpointEventArgs( filename, line, true, breakOnAllThreads, stopGame );
                                    ScriptEditor_ToggleBreakpoint( this, e );
                                }
                            }
                        }
                    }
                }
            }

            DeSerializeShared();
        }
        
        public override void Serialize( System.Xml.XmlTextWriter writer, string path ) 
        {
            if ( SerializeStart( writer, ref path ) )
            {
                foreach ( KeyValuePair<string, Dictionary<int, BreakpointInfo>> pair in m_breakpointsByFilename )
                {
                    writer.WriteStartElement( "File" );
                    writer.WriteAttributeString( "name", pair.Key );
                    {
                        foreach ( KeyValuePair<int, BreakpointInfo> infoPair in pair.Value )
                        {
                            if ( infoPair.Value.HasBreakpoint )
                            {
                                writer.WriteStartElement( "Breakpoint" );
                                writer.WriteAttributeString( "line", infoPair.Key.ToString() );

                                if ( infoPair.Value.BreakOnAllThreads )
                                {
                                    writer.WriteAttributeString( "breakOnAllThreads", infoPair.Value.BreakOnAllThreads.ToString() );
                                }

                                if ( infoPair.Value.BreakStopsGame )
                                {
                                    writer.WriteAttributeString( "breakStopsGame", infoPair.Value.BreakStopsGame.ToString() );
                                }

                                writer.WriteEndElement();
                            }
                        }
                    }
                    writer.WriteEndElement();
                }
            }
        }

        public static WidgetVisible ShowVisible(Widget widget, IWidgetView widgetView, Control parent, String hashKey, bool forceOpen, ToolTip tooltip)
        {
            WidgetScriptEditor widgetEditor = (WidgetScriptEditor)widget;
            WidgetScriptEditorVisible visible = new WidgetScriptEditorVisible( widgetView, widgetEditor, parent );

            widgetEditor.SetAllBreakpoints( visible.EditorControl.ScriptEditor,
                widgetEditor.m_breakpointsByFilename );
            visible.EditorControl.ScriptEditor.DebugState = widgetEditor.m_debuggerState;

            WidgetVisualiser.WidgetShowVisibleSetup( widgetEditor, visible, hashKey );

            return visible;
        }

        public override void UpdateRemote()
        {

        }
        #endregion

        #region Script Editor Event Handlers
        public void ScriptEditor_ToggleBreakpoint( object sender, BreakpointEventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(e.Enable ? BankRemotePacket.EnumPacketType.USER_0 : BankRemotePacket.EnumPacketType.USER_1, GetStaticGUID(), Id);
            p.Write_const_char( e.FileName.Replace( '\\', '/' ) );
            p.Write_s32( e.LineNumber );
            p.Write_bool( e.BreakOnAllThreads );
            p.Write_bool( e.BreakStopsGame );
            p.Send();

            // cancel so we can send the message and wait for the callback
            e.Cancel = true;
        }

        public void ScriptEditor_ContinueDebugger( object sender, DebuggerEventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_2, GetStaticGUID(), Id);
            p.Send();

            // cancel so we can send the message and wait for the callback
            e.Cancel = true;
        }

        public void ScriptEditor_PauseDebugger( object sender, DebuggerEventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_3, GetStaticGUID(), Id);
            p.Send();

            // cancel so we can send the message and wait for the callback
            e.Cancel = true;
        }

        public void ScriptEditor_StepIntoDebugger( object sender, DebuggerEventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_4, GetStaticGUID(), Id);
            p.Send();

            // cancel so we can send the message and wait for the callback
            e.Cancel = true;
        }

        public void ScriptEditor_StepOverDebugger( object sender, DebuggerEventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_7, GetStaticGUID(), Id);
            p.Send();

            // cancel so we can send the message and wait for the callback
            e.Cancel = true;
        }

        public void ScriptEditor_StepOutDebugger( object sender, DebuggerEventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_8, GetStaticGUID(), Id);
            p.Send();

            // cancel so we can send the message and wait for the callback
            e.Cancel = true;
        }
        #endregion

        #region Static Functions
        [DllImport( "user32.dll" )]
        public static extern bool MessageBeep( uint a );

        private static bool RemoteHandler( BankRemotePacket packet, out String errorMessage )
        {
            bool handled = false;

            switch ( packet.BankCommand )
            {
                case BankRemotePacket.EnumPacketType.CREATE:
                    {
                        packet.Begin();
                        uint remoteParent = packet.Read_bkWidget();
                        string title = packet.Read_const_char();
                        string memo = packet.Read_const_char();
                        Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                        packet.Read_bool();     // Read-only currently unsupported.
                        string scriptFile = packet.Read_const_char().Replace( '/', '\\' ).ToLower();
                        BankRemotePacket.EnumPacketType threadState = (BankRemotePacket.EnumPacketType)packet.Read_s32();
                        packet.End();

                        ScriptEditor.DebuggerState debugState;
                        switch ( threadState )
                        {
                            case BankRemotePacket.EnumPacketType.USER_3:
                                debugState = ScriptEditor.DebuggerState.Paused;
                                break;
                            case BankRemotePacket.EnumPacketType.USER_6:
                                debugState = ScriptEditor.DebuggerState.Stopped;
                                break;
                            default:
                                debugState = ScriptEditor.DebuggerState.Running;
                                break;
                        }
                        
                        Widget parent;
                        if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                        {
                            WidgetScriptEditor local = new WidgetScriptEditor(packet.BankPipe, packet.Id, title, memo, scriptFile, debugState, fillColor);
                            packet.BankPipe.Associate(packet.Id, local);
                            parent.AddChild(local);
                            handled = true;
                        }
                    }
                    break;

                // enable/disable breakpoint
                case BankRemotePacket.EnumPacketType.USER_0:
                case BankRemotePacket.EnumPacketType.USER_1:
                    {
                        packet.Begin();
                        
                        // find the local version of the the remote widget:
                        WidgetScriptEditor local;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                        {
                            string filename = packet.Read_const_char().Replace('/', '\\').ToLower();
                            int line = packet.Read_s32();
                            bool allow = packet.Read_bool();
                            bool breakOnAllThreads = packet.Read_bool();
                            bool stopGame = packet.Read_bool();

                            packet.End();

                            if (allow)
                            {
                                bool setOrUnSet = packet.BankCommand == BankRemotePacket.EnumPacketType.USER_0;

                                local.SetBreakpoint(filename, line, setOrUnSet, breakOnAllThreads, stopGame);

                                if (local.BreakpointAdded != null)
                                {
                                    local.BreakpointAdded(local, new GenericValueArgs(Tuple.Create(filename, line, setOrUnSet, breakOnAllThreads, stopGame)));
                                }
                            }
                            else
                            {
                                MessageBeep(0xFFFFFFFF);

                                if (local.BreakpointsAllowed != null)
                                {
                                    local.BreakpointsAllowed(local, null);
                                }
                            }
                            handled = true;
                        }
                    }
                    break;

                // continue
                case BankRemotePacket.EnumPacketType.USER_2:
                    {
                        packet.Begin();
                        
                        // find the local version of the the remote widget:
                        WidgetScriptEditor local;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                        {
                            packet.End();

                            local.Play();

                            if (local.Played != null)
                            {
                                local.Played(local, null);
                            }
                            handled = true;
                        }
                    }
                    break;

                // pause
                case BankRemotePacket.EnumPacketType.USER_3:
                    {
                        packet.Begin();
                                                
                        // find the local version of the the remote widget:
                        WidgetScriptEditor local;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                        {
                            string filename = packet.Read_const_char().Replace('/', '\\').ToLower();
                            int line = packet.Read_s32();

                            packet.End();

                            local.Pause(filename, line);

                            if (local.Paused != null)
                            {
                                local.Paused(local, new GenericValueArgs(Tuple.Create(filename, line)));
                            }
                            handled = true;
                        }
                    }
                    break;

                // step into
                case BankRemotePacket.EnumPacketType.USER_4:
                    {
                        packet.Begin();
                        
                        // find the local version of the the remote widget:
                        WidgetScriptEditor local;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                        {
                            string filename = packet.Read_const_char().Replace('/', '\\').ToLower();
                            int line = packet.Read_s32();

                            packet.End();

                            local.StepInto(filename, line);

                            if (local.DebuggerSteppedInto != null)
                            {
                                local.DebuggerSteppedInto(local, new GenericValueArgs(Tuple.Create(filename, line)));
                            }
                            handled = true;
                        }
                    }
                    break;

                // set program counter
                case BankRemotePacket.EnumPacketType.USER_5:
                    {
                        packet.Begin();
                        
                        // find the local version of the the remote widget:
                        WidgetScriptEditor local;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                        {
                            string filename = packet.Read_const_char().Replace('/', '\\').ToLower();
                            int line = packet.Read_s32();

                            packet.End();

                            if (local.m_scriptFileName == null)
                            {
                                // The first one is the script that was launched.
                                local.m_scriptFileName = filename;
                            }

                            local.SetProgramCounter(filename, line, true);

                            if (local.ProgramCounterSet != null)
                            {
                                local.ProgramCounterSet(local, new GenericValueArgs(Tuple.Create(filename, line, true)));
                            }
                            handled = true;
                        }
                    }
                    break;

                // exit
                case BankRemotePacket.EnumPacketType.USER_6:
                    {
                        packet.Begin();
                        
                        // find the local version of the the remote widget:
                        WidgetScriptEditor local;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                        {
                            bool normalExit = packet.Read_bool();
                            packet.End();

                            local.Stop(normalExit);

                            if (local.Stopped != null)
                            {
                                local.Stopped(local, new GenericValueArgs(Tuple.Create(normalExit)));
                            }
                            handled = true;
                        }
                    }
                    break;

                // step over
                case BankRemotePacket.EnumPacketType.USER_7:
                    {
                        packet.Begin();
                        
                        // find the local version of the the remote widget:
                        WidgetScriptEditor local;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                        {
                            string filename = packet.Read_const_char().Replace('/', '\\').ToLower();
                            int line = packet.Read_s32();

                            packet.End();

                            local.StepOver(filename, line);

                            if (local.DebuggerSteppedOver != null)
                            {
                                local.DebuggerSteppedOver(local, new GenericValueArgs(Tuple.Create(filename, line)));
                            }
                            handled = true;
                        }
                    }
                    break;

                // step out
                case BankRemotePacket.EnumPacketType.USER_8:
                    {
                        packet.Begin();
                        
                        // find the local version of the the remote widget:
                        WidgetScriptEditor local;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                        {
                            string filename = packet.Read_const_char().Replace('/', '\\').ToLower();
                            int line = packet.Read_s32();

                            packet.End();

                            local.StepOut(filename, line);

                            if (local.DebuggerSteppedOut != null)
                            {
                                local.DebuggerSteppedOut(local, new GenericValueArgs(Tuple.Create(filename, line)));
                            }
                            handled = true;
                        }
                    }
                    break;

                // operation failed
                case BankRemotePacket.EnumPacketType.USER_9:
                    {
                        packet.Begin();
                        
                        // find the local version of the the remote widget:
                        WidgetScriptEditor local;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                        {
                            packet.End();

                            MessageBeep(0xFFFFFFFF);

                            if (local.BreakpointsAllowed != null)
                            {
                                local.BreakpointsAllowed(local, null);
                            }
                            handled = true;
                        }
                    }
                    break;

                default:
                    handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
                    break;
            }
            return handled;
        }

        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            lock (s_syncObj)
            {
                // Check whether the script editor is currently set up.
                if (s_refCount == 0)
                {
                    ScriptEditor.LoadPlugins(ApplicationSettings.ExecutablePath, null);

                    ScriptEditorControl.LoadLayout();

                    // this will start us off right
                    ScriptEditorControl.EditorSettings.Validate();

                    if (String.IsNullOrEmpty(ScriptEditorControl.EditorSettings.CurrentCompilingLanguage))
                    {
                        // this will start us off right
                        ScriptEditorControl.EditorSettings.CurrentCompilingLanguage = "SanScript";
                        ScriptEditorControl.EditorSettings.CurrentCompilingSettingsIndex = 0;
                    }

                    WidgetVisualiser.AddHandlerShowVisible(typeof(WidgetScriptEditor), WidgetScriptEditor.ShowVisible);
                    WidgetVisualiser.AddDefaultHandlerHideVisible(typeof(WidgetScriptEditor));
                }

                ++s_refCount;
            }

            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }

        public static void RemoveHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.RemoveType( GetStaticGUID() );

            lock (s_syncObj)
            {
                --s_refCount;

                if (s_refCount == 0)
                {
                    ScriptEditor.ShutdownPlugins(null);
                    WidgetVisualiser.RemoveHandlers(typeof(WidgetScriptEditor));
                }
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Sets all breakpoints and program counters in the <see cref="ScriptEditor"/> using the data stored in breakpointsByFilename.
        /// </summary>
        /// <param name="editor"></param>
        /// <param name="breakpointsByFilename"></param>
        private void SetAllBreakpoints( ScriptEditor editor, Dictionary<string, Dictionary<int, BreakpointInfo>> breakpointsByFilename )
        {
            m_mutex.WaitOne();

            foreach ( KeyValuePair<string, Dictionary<int, BreakpointInfo>> pair in breakpointsByFilename )
            {
                if ( editor.LoadFile( pair.Key ) )
                {
                    foreach ( KeyValuePair<int, BreakpointInfo> infoPair in pair.Value )
                    {
                        if ( infoPair.Value.HasBreakpoint )
                        {
                            editor.SetBreakpoint( pair.Key, infoPair.Key, true, infoPair.Value.BreakOnAllThreads, infoPair.Value.BreakStopsGame );
                        }

                        if ( infoPair.Value.IsProgramCounter )
                        {
                            editor.SetProgramCounter( pair.Key, infoPair.Key, true );
                        }
                    }
                }
            }

            m_mutex.ReleaseMutex();
        }

        /// <summary>
        /// Called when we need to enable or disable a breakpoint.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="line"></param>
        /// <param name="enable"></param>
        /// <param name="breakOnAllThreads"></param>
        /// <param name="stopGame"></param>
        private void SetBreakpoint( string filename, int line, bool enable, bool breakOnAllThreads, bool breakStopsGame )
        {
            m_mutex.WaitOne();

            if ( enable )
            {
                // create a new breakpoint
                Dictionary<int, BreakpointInfo> breakpoints = null;
                BreakpointInfo bInfo = FindBreakpointInfo( filename, line, ref breakpoints );
                if ( bInfo != null )
                {
                    SetBreakpointInfo( bInfo, filename, true, breakOnAllThreads, breakStopsGame, false );
                }
                else
                {
                    CreateBreakpointInfo( filename, line, breakpoints, true, breakOnAllThreads, breakStopsGame, false );
                }
            }
            else
            {
                // remove the old breakpoint
                RemoveBreakpointInfo( filename, line, true, false );
            }

            m_mutex.ReleaseMutex();
        }

        /// <summary>
        /// Called when we need to enable to disable a program counter
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="line"></param>
        /// <param name="enable"></param>
        private void SetProgramCounter( string filename, int line, bool enable )
        {
            m_mutex.WaitOne();

            // remove the old program counter
            RemoveBreakpointInfo( filename, -1, false, true );

            if ( enable )
            {
                // create a new program counter
                Dictionary<int, BreakpointInfo> breakpoints = null;
                BreakpointInfo bInfo = FindBreakpointInfo( filename, line, ref breakpoints );
                if ( bInfo != null )
                {
                    SetBreakpointInfo( bInfo, filename, false, false, false, true );
                }
                else
                {
                    CreateBreakpointInfo( filename, line, breakpoints, false, false, false, true );
                }
            }

            m_mutex.ReleaseMutex();
        }

        /// <summary>
        /// Called as a result of pressing Play
        /// </summary>
        private void Play()
        {
            m_mutex.WaitOne();

            if ( this.DebugState == ScriptEditor.DebuggerState.Running )
            {
                return;
            }

            // remove all program counters
            string[] filenames = new string[m_programCountersByFilename.Keys.Count];
            m_programCountersByFilename.Keys.CopyTo( filenames, 0 );
            foreach ( string filename in filenames )
            {
                RemoveBreakpointInfo( filename, -1, false, true );
            }

            this.DebugState = ScriptEditor.DebuggerState.Running;

            m_mutex.ReleaseMutex();
        }

        /// <summary>
        /// Calls as a result of the Pause button being hit, or the game has hit a breakpoint
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="line"></param>
        private void Pause( string filename, int line )
        {
            if ( this.DebugState == ScriptEditor.DebuggerState.Paused )
            {
                return;
            }

            SetProgramCounter( filename, line, true );

            this.DebugState = ScriptEditor.DebuggerState.Paused;
        }

        /// <summary>
        /// Called as a result of pressing the StepInto button
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="line"></param>
        private void StepInto( string filename, int line )
        {
            if ( this.DebugState != ScriptEditor.DebuggerState.Paused )
            {
                return;
            }

            SetProgramCounter( filename, line, true );
        }

        /// <summary>
        /// Called as a result of pressing the StepOver button
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="line"></param>
        private void StepOver( string filename, int line )
        {
            if ( this.DebugState != ScriptEditor.DebuggerState.Paused )
            {
                return;
            }

            SetProgramCounter( filename, line, true );
        }

        /// <summary>
        /// Called as a result of pressing the StepOut button
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="line"></param>
        private void StepOut( string filename, int line )
        {
            if ( this.DebugState != ScriptEditor.DebuggerState.Paused )
            {
                return;
            }

            SetProgramCounter( filename, line, true );
        }

        /// <summary>
        /// Called when the game tells us the program has exited.
        /// </summary>
        private void Stop( bool normalExit )
        {
            m_mutex.WaitOne();

            if ( this.DebugState == ScriptEditor.DebuggerState.Stopped )
            {
                return;
            }

            if ( normalExit )
            {
                // remove all program counters
                string[] filenames = new string[m_programCountersByFilename.Keys.Count];
                m_programCountersByFilename.Keys.CopyTo( filenames, 0 );
                foreach ( string filename in filenames )
                {
                    RemoveBreakpointInfo( filename, -1, false, true );
                }
            }

            this.DebugState = ScriptEditor.DebuggerState.Stopped;

            m_mutex.ReleaseMutex();
        }

        /// <summary>
        /// Tries to locate the BreakpointInfo in the given filename and on the given line.  Sets or creates
        /// the breakpoints dictionary.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="line"></param>
        /// <param name="breakpoints">Return by reference the breakpoint dictionary for the file.</param>
        /// <returns><c>null</c> if not found</returns>
        private BreakpointInfo FindBreakpointInfo( string filename, int line, ref Dictionary<int, BreakpointInfo> breakpoints )
        {
            string lowercaseFilename = filename.ToLower();
            if ( !m_breakpointsByFilename.TryGetValue( lowercaseFilename, out breakpoints ) )
            {
                breakpoints = new Dictionary<int, BreakpointInfo>();
                m_breakpointsByFilename.Add( lowercaseFilename, breakpoints );
            }

            BreakpointInfo bInfo = null;
            if ( line == -1 )
            {
                m_programCountersByFilename.TryGetValue( lowercaseFilename, out bInfo );
            }
            else
            {
                breakpoints.TryGetValue( line, out bInfo );
            }

            return bInfo;
        }

        /// <summary>
        /// Changes the properties of the BreakpointInfo.  Adds it to the m_programCountersByFilename dictionary
        /// if it is now a Program Counter.
        /// </summary>
        /// <param name="bInfo"></param>
        /// <param name="filename"></param>
        /// <param name="makeBreakpoint"></param>
        /// <param name="breakOnAllThreads"></param>
        /// <param name="makeProgramCounter"></param>
        private void SetBreakpointInfo( BreakpointInfo bInfo, string filename, bool makeBreakpoint, 
            bool breakOnAllThreads, bool breakStopsGame, bool makeProgramCounter )
        {
            if ( makeBreakpoint )
            {
                bInfo.HasBreakpoint = true;

                bInfo.BreakOnAllThreads = breakOnAllThreads;
                bInfo.BreakStopsGame = breakStopsGame;
            }

            if ( makeProgramCounter )
            {
                bInfo.IsProgramCounter = true;
            }

            if ( bInfo.IsProgramCounter )
            {
                m_programCountersByFilename[filename.ToLower()] = bInfo;
            }
        }

        /// <summary>
        /// Creates a new BreakpointInfo with the given options and adds it to the breakpoints dictionary with the
        /// given line as a key.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="breakpoints"></param>
        /// <param name="line"></param>
        /// <param name="makeBreakpoint"></param>
        /// <param name="breakOnAllThreads"></param>
        /// <param name="makeProgramCounter"></param>
        /// <returns></returns>
        private BreakpointInfo CreateBreakpointInfo( string filename, int line, Dictionary<int, BreakpointInfo> breakpoints, 
            bool makeBreakpoint, bool breakOnAllThreads, bool breakStopsGame, bool makeProgramCounter )
        {
            BreakpointInfo bInfo = new BreakpointInfo( filename, line );

            SetBreakpointInfo( bInfo, filename, makeBreakpoint, breakOnAllThreads, breakStopsGame, makeProgramCounter );

            breakpoints.Add( line, bInfo );

            return bInfo;
        }

        /// <summary>
        /// Removes the BreakpointInfo in the given filename at the given line
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="line">If -1, we don't know the line, so use the program counter, but don't modify m_programCountersByFilename</param>
        /// <param name="removeBreakpoint"></param>
        /// <param name="removeProgramCounter"></param>
        private void RemoveBreakpointInfo( string filename, int line, bool removeBreakpoint, bool removeProgramCounter )
        {
            // find the bInfo
            Dictionary<int, BreakpointInfo> breakpoints = null;
            BreakpointInfo bInfo = FindBreakpointInfo( filename, line, ref breakpoints );

            // check to remove the bInfo, or just change properties
            bool doRemoveBreakpoint = false;
            bool doRemoveProgramCounter = false;
            if ( bInfo != null )
            {
                if ( removeBreakpoint && removeProgramCounter )
                {
                    doRemoveBreakpoint = true;

                    if ( bInfo.IsProgramCounter )
                    {
                        // remove the program counter
                        doRemoveProgramCounter = true;
                    }
                }
                else if ( removeProgramCounter && bInfo.IsProgramCounter )
                {
                    if ( bInfo.HasBreakpoint )
                    {
                        // don't remove the breakpoint, just disable the program counter
                        bInfo.IsProgramCounter = false;
                    }
                    else
                    {
                        doRemoveBreakpoint = true;
                    }

                    // remove the program counter
                    doRemoveProgramCounter = true;
                }
                else if ( removeBreakpoint && bInfo.HasBreakpoint )
                {
                    if ( bInfo.IsProgramCounter )
                    {
                        // don't remove the program counter, just disable breakpoint
                        bInfo.HasBreakpoint = false;
                    }
                    else
                    {
                        doRemoveBreakpoint = true;
                    }
                }

                string lowercaseFilename = filename.ToLower();

                if ( doRemoveProgramCounter )
                {
                    m_programCountersByFilename.Remove( lowercaseFilename );
                }

                if ( doRemoveBreakpoint )
                {
                    if ( line == -1 )
                    {
                        foreach ( KeyValuePair<int, BreakpointInfo> pair in breakpoints )
                        {
                            if ( pair.Value == bInfo )
                            {
                                line = pair.Key;
                                break;
                            }
                        }
                    }
                    
                    if ( line != -1 )
                    {
                        breakpoints.Remove( line );
                    }
                }
            }
        }
        #endregion
    }

    #region class BreakpointInfo
    public class BreakpointInfo
    {
        public BreakpointInfo( string filename, int line )
        {
            m_filename = filename;
            m_line = line;
            m_breakOnAnyThread = false;
        }

        public BreakpointInfo( string filename, int line, bool isBreakpoint, bool isProgramCounter )
            : this( filename, line )
        {
            m_hasBreakpoint = isBreakpoint;
            m_breakOnAnyThread = false;
            m_isProgramCounter = isProgramCounter;
        }

        #region Variables
        private string m_filename;
        private int m_line;
        private bool m_hasBreakpoint;
        private bool m_breakOnAnyThread;
        private bool m_breakStopsGame;
        private bool m_isProgramCounter;
        #endregion

        #region Properties
        public string FileName
        {
            get
            {
                return m_filename;
            }
        }

        public int LineNumber
        {
            get
            {
                return m_line;
            }
        }

        public bool HasBreakpoint
        {
            get
            {
                return m_hasBreakpoint;
            }
            set
            {
                m_hasBreakpoint = value;
                
                if ( !this.HasBreakpoint )
                {
                    this.BreakOnAllThreads = false;
                    this.BreakStopsGame = false;
                }
            }
        }

        public bool BreakOnAllThreads
        {
            get
            {
                return m_breakOnAnyThread;
            }
            set
            {
                m_breakOnAnyThread = value;
            }
        }

        public bool BreakStopsGame
        {
            get
            {
                return m_breakStopsGame;
            }
            set
            {
                m_breakStopsGame = value;
            }
        }

        public bool IsProgramCounter
        {
            get
            {
                return m_isProgramCounter;
            }
            set
            {
                m_isProgramCounter = value;
            }
        }

        public string Key
        {
            get
            {
                return BreakpointInfo.GetKey( m_filename, m_line );
            }
        }
        #endregion

        #region Public Static Functions
        public static string GetKey( string filename, int line )
        {
            StringBuilder key = new StringBuilder( filename.ToLower() );
            key.Append( "<<" );
            key.Append( line );
            key.Append( ">>" );
            return key.ToString();
        }
        #endregion
    }
    #endregion
}
