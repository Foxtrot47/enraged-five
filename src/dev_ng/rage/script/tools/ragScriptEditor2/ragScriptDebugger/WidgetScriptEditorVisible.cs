using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

using ragCore;
using ragScriptEditorShared;
using ragWidgets;

namespace ragScriptDebugger
{
	/// <summary>
	/// Summary description for WidgetScriptVisible.
	/// </summary>
    public class WidgetScriptEditorVisible : WidgetVisible
    {
        public WidgetScriptEditorVisible( IWidgetView widgetView, WidgetScriptEditor scriptEditor, Control parent )
            : base( widgetView, parent, scriptEditor )
        {
            m_Widget = scriptEditor;
            m_Widget.Played += widget_Played;
            m_Widget.Paused += widget_Paused;
            m_Widget.Stopped += widget_Stopped;
            m_Widget.BreakpointAdded += widget_BreakpointAdded;
            m_Widget.BreakpointsAllowed += widget_BreakpointsAllowed;
            m_Widget.DebuggerSteppedInto += widget_DebuggerSteppedInto;
            m_Widget.DebuggerSteppedOut += widget_DebuggerSteppedOut;
            m_Widget.DebuggerSteppedOver += widget_DebuggerSteppedOver;
            m_Widget.ProgramCounterSet += widget_ProgramCounterSet;

            m_ScriptEditorControl = new ScriptEditorControl( parent );
            m_ScriptEditorControl.Name = m_Widget.Title;
            m_ScriptEditorControl.Text = m_Widget.Title;            
            m_ScriptEditorControl.ScriptEditor.ToggleBreakpoint += new ScriptEditor.ToggleBreakpointEventHandler( m_Widget.ScriptEditor_ToggleBreakpoint );
            m_ScriptEditorControl.ScriptEditor.ContinueDebugger += new ScriptEditor.ContinueDebuggerEventHandler( m_Widget.ScriptEditor_ContinueDebugger );
            m_ScriptEditorControl.ScriptEditor.PauseDebugger += new ScriptEditor.PauseDebuggerEventHandler( m_Widget.ScriptEditor_PauseDebugger );
            m_ScriptEditorControl.ScriptEditor.StepIntoDebugger += new ScriptEditor.StepIntoDebuggerEventHandler( m_Widget.ScriptEditor_StepIntoDebugger );
            m_ScriptEditorControl.ScriptEditor.StepOverDebugger += new ScriptEditor.StepOverDebuggerEventHandler( m_Widget.ScriptEditor_StepOverDebugger );
            m_ScriptEditorControl.ScriptEditor.StepOutDebugger += new ScriptEditor.StepOutDebuggerEventHandler( m_Widget.ScriptEditor_StepOutDebugger );

            parent.Controls.Add( m_ScriptEditorControl );
            AddMouseEnter( m_ScriptEditorControl );
            m_ScriptEditorControl.AddMouseUp( new MouseEventHandler( OnMouseUp ) );
            m_ScriptEditorControl.AddMouseDown( new MouseEventHandler( OnMouseDown ) );
            SetDragDropHandlers( m_ScriptEditorControl );

            if ( m_Widget.ScriptFileName != null )
            {
                // load this first so that AllowClose is false
                m_ScriptEditorControl.ScriptEditor.LoadFile( m_Widget.ScriptFileName );
            }

            List<Control> controls = m_ScriptEditorControl.ContextMenuControls;
            foreach ( Control control in controls )
            {
                base.AddContextMenu( control );
            }
        }

        #region EventHandlers

        private void widget_Played( object sender, GenericValueArgs args )
        {
            EditorControl.ScriptEditor.Play();
        }

        private void widget_Paused( object sender, GenericValueArgs args )
        {
            var t = (Tuple<string, int>)args.Value;
            EditorControl.ScriptEditor.Pause(t.Item1, t.Item2);
        }

        private void widget_Stopped( object sender, GenericValueArgs args )
        {
            var t = (Tuple<bool>)args.Value;
            EditorControl.ScriptEditor.Stop( t.Item1 );
        }

        private void widget_BreakpointAdded( object sender, GenericValueArgs args )
        {
            var t = (Tuple<string, int, bool, bool, bool>)args.Value;
            string filename = t.Item1;
            int line = t.Item2;
            bool setOrUnSet = t.Item3;
            bool breakOnAllThreads = t.Item4;
            bool stopGame = t.Item5;

            EditorControl.ScriptEditor.SetBreakpoint( filename, line, setOrUnSet, breakOnAllThreads, stopGame );
        }

        private void widget_BreakpointsAllowed( object sender, GenericValueArgs args )
        {
            EditorControl.ScriptEditor.AllowBreakpoints = true;
        }

        private void widget_DebuggerSteppedInto( object sender, GenericValueArgs args )
        {
            var t = (Tuple<string, int>)args.Value;
            EditorControl.ScriptEditor.StepInto( t.Item1, t.Item2 );
        }

        private void widget_DebuggerSteppedOut( object sender, GenericValueArgs args )
        {
            var t = (Tuple<string, int>)args.Value;
            EditorControl.ScriptEditor.StepOut( t.Item1, t.Item2 );
        }

        private void widget_DebuggerSteppedOver( object sender, GenericValueArgs args )
        {
            var t = (Tuple<string, int>)args.Value;
            EditorControl.ScriptEditor.StepOver( t.Item1, t.Item2 );
        }

        private void widget_ProgramCounterSet( object sender, GenericValueArgs args )
        {
            var t = (Tuple<string, int, bool>)args.Value;
            EditorControl.ScriptEditor.SetProgramCounter( t.Item1, t.Item2, t.Item3 );
        }

        #endregion

        #region Variables
        private ScriptEditorControl m_ScriptEditorControl;
        private WidgetScriptEditor m_Widget;
        #endregion

        #region Properties
        public ScriptEditorControl EditorControl
        {
            get
            {
                return m_ScriptEditorControl;
            }
        }
        #endregion

        #region WidgetVisible Overrides
        public override Control Control
        {
            get
            {
                return m_ScriptEditorControl;
            }
        }

        public override Control DragDropControl
        {
            get
            {
                // for now!
                return null;
            }
        }

        public override Widget Widget
        {
            get
            {
                return m_Widget;
            }
        }

        public override void Destroy()
        {
            base.Destroy();

            m_Widget = null;
            m_ScriptEditorControl.Dispose();
            m_ScriptEditorControl = null;
        }
        
        public override void Remove()
        {
            m_Parent.Controls.Remove( m_ScriptEditorControl );

            m_ScriptEditorControl.ScriptEditor.ToggleBreakpoint -= new ScriptEditor.ToggleBreakpointEventHandler( m_Widget.ScriptEditor_ToggleBreakpoint );
            m_ScriptEditorControl.ScriptEditor.ContinueDebugger -= new ScriptEditor.ContinueDebuggerEventHandler( m_Widget.ScriptEditor_ContinueDebugger );
            m_ScriptEditorControl.ScriptEditor.PauseDebugger -= new ScriptEditor.PauseDebuggerEventHandler( m_Widget.ScriptEditor_PauseDebugger );
            m_ScriptEditorControl.ScriptEditor.StepIntoDebugger -= new ScriptEditor.StepIntoDebuggerEventHandler( m_Widget.ScriptEditor_StepIntoDebugger );
            m_ScriptEditorControl.ScriptEditor.StepOverDebugger -= new ScriptEditor.StepOverDebuggerEventHandler( m_Widget.ScriptEditor_StepOverDebugger );
            m_ScriptEditorControl.ScriptEditor.StepOutDebugger -= new ScriptEditor.StepOutDebuggerEventHandler( m_Widget.ScriptEditor_StepOutDebugger );

            RemoveMouseEnter( m_ScriptEditorControl );
            m_ScriptEditorControl.RemoveMouseUp( new MouseEventHandler( OnMouseUp ) );
            m_ScriptEditorControl.RemoveMouseDown( new MouseEventHandler( OnMouseDown ) );
            RemoveDragDropHandlers( m_ScriptEditorControl );
        }

        public override void ResizeAndReposition( int yLocation )
        {
            m_ScriptEditorControl.Location = new System.Drawing.Point( 0, yLocation );
            m_ScriptEditorControl.Size = new System.Drawing.Size( ParentWidth, GetHeight() );
        }

        public override int GetHeight()
        {
            return 300;
        }

        public override void UpdatePersist( bool visible )
        {

        }
        #endregion
    }
}
