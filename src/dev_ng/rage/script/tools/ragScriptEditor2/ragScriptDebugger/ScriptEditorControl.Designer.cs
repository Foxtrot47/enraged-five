namespace ragScriptDebugger
{
    partial class ScriptEditorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            TD.SandDock.DocumentContainer documentContainer1;
            this.tabbedDocument1 = new TD.SandDock.TabbedDocument();
            this.sandDockManager1 = new TD.SandDock.SandDockManager();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.documentTabContextMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.closeTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeOtherTabsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllTabsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            documentContainer1 = new TD.SandDock.DocumentContainer();
            documentContainer1.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.documentTabContextMenuStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // documentContainer1
            // 
            documentContainer1.ContentSize = 623;
            documentContainer1.Controls.Add( this.tabbedDocument1 );
            documentContainer1.LayoutSystem = new TD.SandDock.SplitLayoutSystem( new System.Drawing.SizeF( 909F, 623F ), System.Windows.Forms.Orientation.Horizontal, new TD.SandDock.LayoutSystemBase[] {
            ((TD.SandDock.LayoutSystemBase)(new TD.SandDock.DocumentLayoutSystem(new System.Drawing.SizeF(909F, 623F), new TD.SandDock.DockControl[] {
                        ((TD.SandDock.DockControl)(this.tabbedDocument1))}, this.tabbedDocument1)))} );
            documentContainer1.Location = new System.Drawing.Point( 0, 0 );
            documentContainer1.Manager = this.sandDockManager1;
            documentContainer1.Name = "documentContainer1";
            documentContainer1.Size = new System.Drawing.Size( 911, 625 );
            documentContainer1.TabIndex = 0;
            // 
            // tabbedDocument1
            // 
            this.tabbedDocument1.FloatingSize = new System.Drawing.Size( 550, 400 );
            this.tabbedDocument1.Guid = new System.Guid( "67612556-d19c-450a-bc4b-31e666d5c9c3" );
            this.tabbedDocument1.Location = new System.Drawing.Point( 1, 21 );
            this.tabbedDocument1.Name = "tabbedDocument1";
            this.tabbedDocument1.Size = new System.Drawing.Size( 909, 603 );
            this.tabbedDocument1.TabIndex = 0;
            this.tabbedDocument1.Text = "tabbedDocument1";
            // 
            // sandDockManager1
            // 
            this.sandDockManager1.DockSystemContainer = this.toolStripContainer1.ContentPanel;
            this.sandDockManager1.OwnerForm = null;
            this.sandDockManager1.SerializeTabbedDocuments = true;
            this.sandDockManager1.ShowControlContextMenu += new TD.SandDock.ShowControlContextMenuEventHandler( this.sandDockManager1_ShowControlContextMenu );
            this.sandDockManager1.ActiveTabbedDocumentChanged += new System.EventHandler( this.sandDockManager1_ActiveTabbedDocumentChanged );
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add( documentContainer1 );
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size( 911, 625 );
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point( 0, 23 );
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size( 911, 650 );
            this.toolStripContainer1.TabIndex = 2;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // documentTabContextMenuStrip
            // 
            this.documentTabContextMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.closeTabToolStripMenuItem,
            this.closeOtherTabsToolStripMenuItem,
            this.closeAllTabsToolStripMenuItem} );
            this.documentTabContextMenuStrip.Name = "documentTabContextMenuStrip";
            this.documentTabContextMenuStrip.ShowImageMargin = false;
            this.documentTabContextMenuStrip.Size = new System.Drawing.Size( 144, 70 );
            // 
            // closeTabToolStripMenuItem
            // 
            this.closeTabToolStripMenuItem.Name = "closeTabToolStripMenuItem";
            this.closeTabToolStripMenuItem.Size = new System.Drawing.Size( 143, 22 );
            this.closeTabToolStripMenuItem.Text = "&Close Tab";
            this.closeTabToolStripMenuItem.Click += new System.EventHandler( this.closeTabToolStripMenuItem_Click );
            // 
            // closeOtherTabsToolStripMenuItem
            // 
            this.closeOtherTabsToolStripMenuItem.Name = "closeOtherTabsToolStripMenuItem";
            this.closeOtherTabsToolStripMenuItem.Size = new System.Drawing.Size( 143, 22 );
            this.closeOtherTabsToolStripMenuItem.Text = "Close &Other Tabs";
            this.closeOtherTabsToolStripMenuItem.Click += new System.EventHandler( this.closeOtherTabsToolStripMenuItem_Click );
            // 
            // closeAllTabsToolStripMenuItem
            // 
            this.closeAllTabsToolStripMenuItem.Name = "closeAllTabsToolStripMenuItem";
            this.closeAllTabsToolStripMenuItem.Size = new System.Drawing.Size( 143, 22 );
            this.closeAllTabsToolStripMenuItem.Text = "Close &All Tabs";
            this.closeAllTabsToolStripMenuItem.Click += new System.EventHandler( this.closeAllTabsToolStripMenuItem_Click );
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add( this.label1 );
            this.panel1.Controls.Add( this.panel2 );
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point( 0, 0 );
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size( 911, 23 );
            this.panel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point( 4, 5 );
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size( 67, 13 );
            this.label1.TabIndex = 3;
            this.label1.Text = "Script Editor:";
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point( 0, 30 );
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size( 324, 243 );
            this.panel2.TabIndex = 2;
            // 
            // ScriptEditorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.toolStripContainer1 );
            this.Controls.Add( this.panel1 );
            this.Location = new System.Drawing.Point( 25, 0 );
            this.Name = "ScriptEditorControl";
            this.Size = new System.Drawing.Size( 911, 673 );
            documentContainer1.ResumeLayout( false );
            this.toolStripContainer1.ContentPanel.ResumeLayout( false );
            this.toolStripContainer1.ResumeLayout( false );
            this.toolStripContainer1.PerformLayout();
            this.documentTabContextMenuStrip.ResumeLayout( false );
            this.panel1.ResumeLayout( false );
            this.panel1.PerformLayout();
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip documentTabContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem closeTabToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeOtherTabsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllTabsToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private TD.SandDock.TabbedDocument tabbedDocument1;
        private TD.SandDock.SandDockManager sandDockManager1;
        private System.Windows.Forms.Label label1;
    }
}
