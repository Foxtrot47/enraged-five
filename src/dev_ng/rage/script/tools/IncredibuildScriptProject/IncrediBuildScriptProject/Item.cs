﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IncrediBuildScriptProject
{
    public class Item
    {
        private string m_filename;      //  The filename of the item on disk
        private string m_projectFolder; //  The project folder this item appears in

        public Item(string filename, string projectFolder)
        {
            //  Store the filename and project folder
            m_filename = filename;
            m_projectFolder = projectFolder;
        }

        public string GetFilename()
        {
            return m_filename;
        }

        public string GetProjectFolder()
        {
            return m_projectFolder;
        }
    }
}
