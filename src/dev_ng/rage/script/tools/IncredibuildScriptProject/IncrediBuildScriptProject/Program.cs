﻿//---------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2019. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace IncrediBuildScriptProject
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Xml;
    using RSG.Base.Extensions;
    using RSG.Base.OS;
    using RSG.Configuration;
    using RSG.Editor.Controls;
    using RSG.Pipeline.BuildScript;
    using RSG.Platform;

    /// <summary>
    /// IncrediBuildScriptProject is an app/library that creates an IB configuration based on a SanScript project file and then runs the
    /// configuration and postbuild steps.
    /// </summary>
    class Program : RsConsoleApplication
    {
        #region Variables
        private XmlTextReader XmlReader;
        private List<Item> Items;
        private List<String> Folders;
        private Dictionary<String, Config> BuildConfigs;
        private List<Platform> Platforms;

        private String ConfigName;
        private String ProjectFileName;
        private bool Rebuild = false;
        private bool Build = false;
        #endregion // Variables

        #region Constants
        private const String LOG_CTX = "IncredibuildScriptProject";

        private const String OPT_REBUILD = "Rebuild";
        private const String OPT_BUILD = "Build";
        private const String OPT_PROJECT_PATH = "scproj";
        private const String OPT_BUILD_CONFIGURATION = "config";
        private const String OPT_PRINT_LINE_INFO = "print-line-info";

        private const String SCRIPT_PROJECT_EXT = ".scproj";

        private static readonly Regex LINE_INFO_REGEX = new Regex(@"([\w\\/:\.]+)\((\d+)\)");

        private static readonly LongOption[] OPTIONS = new LongOption[]
        {
            new LongOption(OPT_REBUILD , LongOption.ArgType.None,
                "Does a build including the postbuild step. This includes packaging and zipping the .rpf ontop of compiling all project."),
            new LongOption(OPT_BUILD , LongOption.ArgType.None,
                "Does an incremental build including the postbuild step."),
            new LongOption(OPT_PROJECT_PATH , LongOption.ArgType.Required,
                "Path to the script project."),
            new LongOption(OPT_BUILD_CONFIGURATION , LongOption.ArgType.Required,
                "Configuration to build the project."),
            new LongOption(OPT_PRINT_LINE_INFO, LongOption.ArgType.None,
                "If specified, parse errors and open up the file to print the line where the error occurs for additional information on the error."),
        };
        #endregion // Constants

        #region Entry-Point/Constructor
        /// <summary>
        /// Main entry point function
        /// </summary>
        /// <param name="args">Arguments passed in</param>
        [STAThread]
        static int Main(string[] args)
        {
            Program app = new Program();
            return app.Run();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        private Program()
        {
            foreach (LongOption o in OPTIONS)
                this.RegisterCommandlineArg(o);
        }
        #endregion // Entry-Point/Constructor

        #region Main
        /// <summary>
        /// Main program execution
        /// </summary>
        /// <returns></returns>
        protected override int ConsoleMain()
        {
            int result = 0;

            Rebuild = CommandOptions.ContainsOption(OPT_REBUILD);
            // TODO: Add Build functionality. Using xgConsole.exe /Build seems to always do rebuilds
            if (CommandOptions.ContainsOption(OPT_BUILD))
            {
                Log.WarningCtx(LOG_CTX, "Functionality does not properly handle incremental builds. Will be running as a rebuild.");
                Rebuild = true;
            }

            if (!CommandOptions.ContainsOption(OPT_BUILD_CONFIGURATION))
            {
                Log.ErrorCtx(LOG_CTX, $"Option {OPT_BUILD_CONFIGURATION} is mandatory.");
                return ExitCode.Failure;
            }

            if (!CommandOptions.ContainsOption(OPT_PROJECT_PATH))
            {
                Log.ErrorCtx(LOG_CTX, $"Option {OPT_PROJECT_PATH} is mandatory.");
                return ExitCode.Failure;
            }

            ConfigName = (String)CommandOptions[OPT_BUILD_CONFIGURATION];
            ProjectFileName = CommandOptions.Branch.Environment.Subst(Environment.ExpandEnvironmentVariables((String)CommandOptions[OPT_PROJECT_PATH]));

            if (!ProjectFileName.EndsWith(SCRIPT_PROJECT_EXT))
            {
                Log.ErrorCtx(LOG_CTX, "Invalid input file type, please specify a '{0}' file.", SCRIPT_PROJECT_EXT);
                return ExitCode.Failure;
            }

            // Determine the list of platforms to use.
            List<Platform> enabledPlatforms = new List<Platform>();
            if (CommandOptions.TrailingArguments.Any())
            {
                foreach (String platformArg in CommandOptions.TrailingArguments)
                {
                    Platform platform;
                    if (Enum.TryParse(platformArg, out platform))
                    {
                        enabledPlatforms.Add(platform);
                    }
                    else
                    {
                        Log.ErrorCtx(LOG_CTX, "Got platform that did not parse to supported platform. Check platform type for: {0}.", platformArg);
                    }
                }
            }
            else
            {
                foreach (KeyValuePair<Platform, ITarget> kvp in CommandOptions.Branch.Targets)
                {
                    if (kvp.Value.Enabled)
                    {
                        enabledPlatforms.Add(kvp.Key);
                    }
                }
            }

            //  Read the items from the original project file
            XmlReader = new XmlTextReader(ProjectFileName);
            ReadItems();
            XmlReader.Close();
            Log.MessageCtx(LOG_CTX, "Read {0} files and {1} folders.", Items.Count, Folders.Count);

            if (!BuildConfigs.ContainsKey(ConfigName))
            {
                Log.ErrorCtx(LOG_CTX, "Config '{0}' not found in project!", ConfigName);
                return ExitCode.Failure;
            }

            Config matchedConfig = BuildConfigs[ConfigName];

            //  Create the XML file
            if (IncrediBuild.IsInstalled)
            {
                ProjectData projectData = new ProjectData(ProjectFileName, enabledPlatforms, matchedConfig, Items, Build || Rebuild, CommandOptions.NoPopups);
                IncrediBuild.CreateBuildConfiguration(projectData);
                Log.MessageCtx(LOG_CTX, "XGE configuration created.");

                if (Build || Rebuild)
                {
                    result = CompileScripts();
                }
            }
            return result;
        }
        #endregion // Main

        #region Private Methods
        /// <summary>
        /// Run a script compile synchronously, logging output.
        /// </summary>
        /// <returns>Exit code based on successful compile</returns>
        private int CompileScripts()
        {
            bool buildSuccessful = true;
            ManualResetEvent buildFinishedReset = new ManualResetEvent(false);
            IncrediBuild.NotifyUIIncredibuildCompletionDelegate += delegate(bool success)
            {
                buildSuccessful &= success;
                buildFinishedReset.Set();
            };

            IncrediBuild.BuildOutput += (string message) =>
            {
                Log.MessageCtx(LOG_CTX, "{0}", message);
            };

            IncrediBuild.BuildErrors += (string message) =>
            {
                Log.ErrorCtx(LOG_CTX, "{0}", message);
                buildSuccessful = false;

                if (this.CommandOptions.ContainsOption(OPT_PRINT_LINE_INFO))
                {
                    Match match = LINE_INFO_REGEX.Match(message);
                    if (match.Success)
                    {
                        String fileName = match.Groups[1].Value;
                        if (!System.IO.File.Exists(fileName))
                        {
                            return;
                        }

                        int lineNum;
                        if (!int.TryParse(match.Groups[2].Value, out lineNum) || lineNum < 1)
                        {
                            return;
                        }

                        String line = System.IO.File.ReadLines(fileName).Skip(lineNum-1).FirstOrDefault();
                        if (line != null)
                        {
                            Log.ErrorCtx(LOG_CTX, "LINE INFO: {0}", line);
                        }
                    }
                }
            };


            // We currently only support rebuilds through this tool
            Log.MessageCtx(LOG_CTX, "Performing a rebuild via incredibuild.");
            IncrediBuild.RunBuildConfiguration("Rebuild");

            // Wait until the rebuild process completes.
            buildFinishedReset.WaitOne();

            if (!buildSuccessful)
            {
                Log.ErrorCtx(LOG_CTX, "Compile Failed! Check logs.");
                return ExitCode.Failure;
            }
            else
            {
                Log.MessageCtx(LOG_CTX, "Completed successfully!");
            }
            return ExitCode.Success;
        }

        /// <summary>
        /// Reads the items from the original project file
        /// </summary>
        private void ReadItems()
        {
            if (XmlReader != null)
            {
                //  Create the empty lists of items and folders and configs
                Items = new List<Item>();
                Folders = new List<string>();
                BuildConfigs = new Dictionary<String, Config>();
                Platforms = new List<Platform>();

                //  Read through the whole file
                while (XmlReader.Read())
                {
                    //  Move to the first element
                    XmlReader.MoveToElement();
                    //  Only interested in ProjectExplorerItems and CompilingSettings
                    if (IsProjectItem())
                    {
                        string type = XmlReader.GetAttribute("xsi:type");
                        switch (type)
                        {
                            case "ProjectExplorerFolder":
                            {
                                ReadFolder("");
                            }
                            break;
                            case "ProjectExplorerFile":
                            {
                                ReadFile("");
                            }
                            break;
                        }
                    }
                    else if (IsConfigItem())
                    {
                        Config config = new Config(CommandOptions.Branch);
                        config.ReadFromXml(XmlReader);
                        //  Add the new config to the list
                        BuildConfigs.Add(config.Name, config);
                    }
                    else if (IsPlatformItem())
                    {
                        // read text
                        XmlReader.Read();
                        Platforms.Add((Platform)Enum.Parse(typeof(Platform), XmlReader.Value));
                        // read end element
                        XmlReader.Read();
                    }
                }
            }
        }

        /// <summary>
        /// Reads in a file item
        /// </summary>
        /// <param name="currentFolder">The current folder this file is inside</param>
        private void ReadFile(string currentFolder)
        {
            //  Read the elements
            while (XmlReader.Read())
            {
                //  Move to the first element
                XmlReader.MoveToElement();
                if (XmlReader.NodeType == XmlNodeType.Element &&
                    XmlReader.Name == "Name")
                {
                    //  Read in the text
                    XmlReader.Read();
                    if (XmlReader.NodeType == XmlNodeType.Text)
                    {
                        //  Add the file to the list of files

                        string file = XmlReader.Value;
                        file = CommandOptions.Branch.Environment.Subst(file);
                        Items.Add(new Item(file, currentFolder));
                    }
                    //  Read the end element
                    XmlReader.Read();
                }
                else if (XmlReader.NodeType == XmlNodeType.EndElement &&
                        XmlReader.Name == "ProjectExplorerItem")
                {
                    //  The end of this file item
                    return;
                }
            }
        }

        /// <summary>
        /// Reads in a folder and folder items inside it
        /// </summary>
        /// <param name="currentFolder">The current folder this folder is inside</param>
        private void ReadFolder(string currentFolder)
        {
            string folderName = currentFolder;
            //  Read the elements
            while (XmlReader.Read())
            {
                //  Move to the first element
                XmlReader.MoveToElement();
                if (XmlReader.NodeType == XmlNodeType.Element &&
                    XmlReader.Name == "Name")
                {
                    //  Read in the text
                    XmlReader.Read();
                    if (XmlReader.NodeType == XmlNodeType.Text)
                    {
                        if (folderName == "")
                        {
                            folderName += XmlReader.Value;
                        }
                        else
                        {
                            folderName += "\\" + XmlReader.Value;
                        }
                        //  Add the folder to the list
                        Folders.Add(folderName);
                    }
                    //  Read the end element
                    XmlReader.Read();
                }
                else if (XmlReader.NodeType == XmlNodeType.EndElement &&
                        XmlReader.Name == "ProjectExplorerItem")
                {
                    //  The end of this folder item
                    return;
                }
                else if (IsProjectItem())
                {
                    string type = XmlReader.GetAttribute("xsi:type");
                    switch (type)
                    {
                        case "ProjectExplorerFolder":
                        {
                            ReadFolder(folderName);
                        }
                        break;
                        case "ProjectExplorerFile":
                        {
                            ReadFile(folderName);
                        }
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// This checks to see if the current node is a project item
        /// </summary>
        /// <returns>true if a project item, otherwise false</returns>
        private bool IsProjectItem()
        {
            return (XmlReader.NodeType == XmlNodeType.Element &&
                    XmlReader.Name == "ProjectExplorerItem");
        }

        /// <summary>
        /// This checks to see if the current node is a config item
        /// </summary>
        /// <returns>true if a config item, otherwise false</returns>
        private bool IsConfigItem()
        {
            return (XmlReader.NodeType == XmlNodeType.Element &&
                    XmlReader.Name == "CompilingSettings");
        }

        /// <summary>
        /// Checks whether the current node is a platform item.
        /// </summary>
        /// <returns></returns>
        private bool IsPlatformItem()
        {
            return (XmlReader.NodeType == XmlNodeType.Element &&
                    XmlReader.Name == "Platform");
        }
        #endregion // Private Methods
    }
}
