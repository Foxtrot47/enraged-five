﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Configuration;
using RSG.Pipeline.BuildScript;
using RSG.Platform;

namespace IncrediBuildScriptProject
{
    /// <summary>
    /// Project configuration data.
    /// </summary>
    class ProjectData : IProjectData
    {
        #region Private member fields

        private Config m_config;
        private List<Item> m_items;
        private string m_projFolder;
        private List<Platform> m_enabledPlatforms;
        private IEnvironment m_environment;
        private bool m_nopopups;

        /// <summary>
        /// Hack solution for handling the 64-bit resources in the interim until we can move to AP3.
        /// </summary>
        private const string ResourceExecutable64Name = "scriptrc_x64.exe";

        #endregion

        #region Public properties

        public string OutputDirectory => this.m_config.OutputPath;

        public string BuildDirectory => this.m_config.BuildPath;

        public string ProjectFileName { get; }

        public string Custom => this.m_config.CmdLine;

        public string XmlIncludeFile => this.m_config.Include;

        public List<string> IncludePaths => new List<string>() { this.m_config.IncludeDirectories };

        public string ConfigurationName => this.m_config.Name;

        public string CompilerExecutable => this.m_config.CompilerExe;

        public string ResourceExecutable => this.m_config.ResourceExe;

        public string ResourceExecutable64 => Path.Combine(Path.GetDirectoryName(m_config.ResourceExe), ResourceExecutable64Name);

        public string PostCompileCommand => this.m_config.PostCompile;

        public bool WarningsAsErrors => this.m_config.WarningsAsErrors;

        public bool FullBuild { get; }

        public bool UseIdeMonitor => false;

        public bool HasErrors { get; private set; }

        public bool UsesPostBuild => false;

        public IList<Platform> EnabledPlatforms => m_enabledPlatforms;

        public IEnvironment Environment => m_environment;

        public bool Nopopups => m_nopopups;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="projectFileName">Project file name.</param>
        /// <param name="enabledPlatforms"></param>
        /// <param name="config">Configuration data.</param>
        /// <param name="items">Items to build.</param>
        /// <param name="fullBuild"></param>
        /// <param name="nopopups"></param>
        public ProjectData(string projectFileName, List<Platform> enabledPlatforms, Config config, List<Item> items, bool fullBuild, bool nopopups)
        {
            m_config = config;
            m_items = items;
            m_projFolder = Path.GetDirectoryName(projectFileName);
            m_enabledPlatforms = enabledPlatforms;
            m_nopopups = nopopups;

            ProjectFileName = projectFileName;
            FullBuild = fullBuild;
            // Add environment for substitution
            m_environment = config.m_Branch.Environment;
            m_environment.Add("OutputDirectory", OutputDirectory);
            m_environment.Add("BuildDir", BuildDirectory);
            m_environment.Add("ProjectFileName", ProjectFileName);
            m_environment.Add("Custom", Custom);
            m_environment.Add("XmlIncludeFile", XmlIncludeFile);
            m_environment.Add("ConfigurationName", ConfigurationName);
            m_environment.Add("CompilerExecutable", CompilerExecutable);
            m_environment.Add("ResourceExecutable", ResourceExecutable);
            m_environment.Add("ResourceExecutable64", ResourceExecutable64);
            m_environment.Add("PostCompileCommand", PostCompileCommand);
            // Creates "Win64,XboxOne" string
            m_environment.Add("PlatformName",
                String.Format("\"{0}\"",
                    String.Join(",", EnabledPlatforms.Select(p => p.ToString()))));
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Get the list of files to build.
        /// </summary>
        /// <returns>The list of files to build.</returns>
        public IList<string> GetFilesToCompile()
        {
            List<string> files = new List<string>();

            foreach (var item in m_items)
            {
                files.Add(Path.Combine(m_projFolder, item.GetFilename()));
            }

            return files;
        }

        /// <summary>
        /// Gets the list of files that need to be resourced for the specified platform.
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public IList<String> GetFilesToResource(RSG.Platform.Platform platform)
        {
            return GetFilesToCompile();
        }


        /// <summary>
        /// Perform pre-build actions.
        /// </summary>
        public void PreBuild() { }

        /// <summary>
        /// Perform post-build actions.
        /// </summary>
        /// <param name="exitCode">Exit code.</param>
        public void PostBuild(int exitCode)
        {
            HasErrors = exitCode != 0;
        }

        /// <summary>
        /// Acquires the platform extension based on the project properties.
        /// </summary>
        public string GetPlatformExtension(Platform platform) => FileType.Script.GetPlatformExtension(platform);

        public IEnumerable<string> GetIncludesForFile(string scriptPath)
        {
            yield break;
        }
        #endregion
    }
}
