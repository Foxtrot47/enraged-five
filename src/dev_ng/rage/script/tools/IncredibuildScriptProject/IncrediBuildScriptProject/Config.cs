﻿//---------------------------------------------------------------------------------------------
// <copyright file="Config.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2019. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace IncrediBuildScriptProject
{
    using System;
    using System.Collections.Generic;
    using RSG.Platform;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using RSG.Configuration;

    public class Config
    {
        public IBranch m_Branch;

        public Config(IBranch branch)
        {
            m_Branch = branch;
        }

        public string Name { get; private set; } = string.Empty;
        public string CompilerExe { get; private set; } = string.Empty;
        public string ResourceExe { get; private set; } = string.Empty;
        public string OutputPath { get; private set; } = string.Empty;
        public string BuildPath { get; private set; } = string.Empty;
        public string PostCompile { get; private set; } = string.Empty;
        public string CmdLine { get; private set; } = string.Empty;
        public string Include { get; private set; } = string.Empty;
        public string IncludeDirectories { get; private set; } = string.Empty;
        public bool WarningsAsErrors { get; private set; }
        public IEnumerable<Platform> SupportedPlatforms { get; private set; } = new Platform[0];

        public void ReadFromXml(XmlReader reader)
        {
            XElement elem;
            using (XmlReader subReader = reader.ReadSubtree())
            {
                elem = XElement.Load(subReader);
            }

            this.Name = elem.Element("ConfigurationName")?.Value ?? this.Name;
            this.CompilerExe = this.ToBranchSpec(elem.Element("CompilerExecutable")?.Value ?? this.CompilerExe);
            this.ResourceExe = this.ToBranchSpec(elem.Element("ResourceExecutable")?.Value ?? this.ResourceExe);
            this.OutputPath = this.ToBranchSpec(elem.Element("OutputDirectory")?.Value ?? this.OutputPath);
            this.BuildPath = elem.Element("BuildDirectory")?.Value ?? this.BuildPath;
            this.PostCompile = this.m_Branch.Environment.Subst(elem.Element("PostCompileCommand")?.Value ?? this.PostCompile);
            this.Include = this.m_Branch.Environment.Subst(elem.Element("IncludeFile")?.Value ?? this.Include);

            this.IncludeDirectories = string.Join(
                ";",
                elem.Element("IncludePaths")?
                    .Elements("string")
                    .Select(e => this.m_Branch.Environment.Subst(e.Value)) ?? new string[0]);

            bool temp;
            bool.TryParse(elem.Element("WarningsAsErrors")?.Value ?? "false", out temp);
            this.WarningsAsErrors = temp;

            this.CmdLine = elem.Element("Custom")?.Value ?? this.CmdLine;

            this.SupportedPlatforms = elem.Element("SupportedPlatforms")?
                .Elements("Platform")
                .Select(
                    e =>
                    {
                        Platform p;
                        Enum.TryParse(e.Value, true, out p);
                        return p;
                    })
                .Where(p => p != Platform.Independent)
                .Distinct()
                .ToArray() ?? new Platform[0];
        }

        /// <summary>
        /// Convert a string from using an absolute path to one using a relative-to-solution-directory path.
        /// </summary>
        /// <param name="fullPath">Full path.</param>
        /// <returns>The path relative to the solution directory.</returns>
        private string ToBranchSpec(string fullPath)
        {
            // Assuming that our input .scproj is dev and we're in the release branch:
            string substFullPath = m_Branch.Environment.Subst(fullPath);
            return substFullPath;
        }

    }
}
