﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.BuildScript
{
    /// <summary>
    /// Notify the UI on completion.
    /// </summary>
    public delegate void OnNotifyUICompletionDelegate(bool success);
}
