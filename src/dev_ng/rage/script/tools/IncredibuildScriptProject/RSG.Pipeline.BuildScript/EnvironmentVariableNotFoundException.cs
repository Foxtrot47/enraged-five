﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.BuildScript
{
    /// <summary>
    /// Environment variable not found exception.
    /// </summary>
    public class EnvironmentVariableNotFoundException : Exception
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="msg">Message.</param>
        public EnvironmentVariableNotFoundException(string msg)
            : base(msg)
        {

        }
    }
}
