﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.BuildScript
{
    /// <summary>
    /// Simple visitor to get the list of files from the project data.
    /// </summary>
    class IncrediBuildVisitor : IBuildVisitor
    {
        #region Public properties

        /// <summary>
        /// The list of files to build.
        /// </summary>
        public IList<string> Files { get; private set; }

        #endregion

        #region Public methods

        /// <summary>
        /// Visit the visitable project data.
        /// </summary>
        /// <param name="container">Project data.</param>
        public void Visit(IProjectData container)
        {
            Files = container.GetFiles();
        }

        #endregion
    }
}
