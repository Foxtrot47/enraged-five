﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Text;
using System.Xml.Linq;
using Microsoft.Win32;
using RSG.Base.Extensions;
using RSG.Platform;

namespace RSG.Pipeline.BuildScript
{
    /// <summary>
    /// IncrediBuild Wrapper class.
    /// </summary>
    public static class IncrediBuild
    {
        #region Constants
        private static readonly Regex ERRORS_REGEX = new Regex("COMPILE FAILED|Error:.*", RegexOptions.IgnoreCase);
        private static readonly String[] _outputExtensions = new String[] { ".sco", ".scd" };
        #endregion // Constants

        #region Private member fields
        //  Registry variables
        private static string m_registryKey = "Software\\Xoreax\\IncrediBuild\\Builder";
        private static string m_regexpVersion = "VersionText";
        private static string m_regexpInstallDir = "Folder";
        //  XML variables
        private static IProjectData m_currentProject;
        private static int m_xmlFormatVersion = 1;
        private static string m_tempXMLFileName = null;

        private static bool m_Running = false;
        private static object m_Lock = new object();
        #endregion // Private member fields

        #region Public events
        public static OnNotifyUIDelegate NotifyUIIncredibuildStateChangeDelegate;
        public static OnNotifyUICompletionDelegate NotifyUIIncredibuildCompletionDelegate;
        public static OnLogMessageReceivedDelegate BuildOutput;
        public static OnLogMessageReceivedDelegate BuildErrors;
        #endregion // Public events

        #region Properties
        /// <summary>
        /// Temporary log file name.
        /// </summary>
        public static string TempLogFileName { get; private set; }

        /// <summary>
        /// True if IncrediBuild is installed.
        /// </summary>
        public static bool IsInstalled
        {
            get
            {
                return (Version.Length > 0);
            }
        }

        /// <summary>
        /// True if there is a build running.
        /// </summary>
        public static bool IsRunning
        {
            get
            {
                return m_Running;
            }
        }

        /// <summary>
        /// IncrediBuild version number.
        /// </summary>
        public static string Version
        {
            get
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(m_registryKey);
                string version = string.Empty;

                if (key != null)
                {
                    version = key.GetValue(m_regexpVersion).ToString();
                    key.Close();
                }

                return version;
            }
        }

        /// <summary>
        /// IncrediBuild installation directory.
        /// </summary>
        public static string InstallDir
        {
            get
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(m_registryKey);
                string installDir = string.Empty;

                if (key != null)
                {
                    installDir = key.GetValue(m_regexpInstallDir).ToString();
                    key.Close();
                }

                return installDir;
            }
        }
        #endregion // Properties

        #region Public Static Methods
        /// <summary>
        /// Stop the current build.
        /// </summary>
        public static void Stop()
        {
            RunBuildConfiguration("Stop");
        }

        /// <summary>
        /// Perform a clean on the project outputs.
        /// </summary>
        public static void Clean()
        {
            CleanDirectory(m_currentProject.Environment.Subst(m_currentProject.OutputDirectory));

            // Clean script-related stuff from the "preview" directory, which contains files compiled standalone and compiled temporarily.
            String previewBuildDir = Path.Combine(m_currentProject.Environment.Subst(m_currentProject.BuildDirectory), "preview");
            CleanDirectory(previewBuildDir, "*.*sc");
            CleanDirectory(previewBuildDir, "*.sco");
            CleanDirectory(previewBuildDir, "*.xml");
        }

        /// <summary>
        /// Run the previously created configuration.
        /// </summary>
        /// <param name="instruction">Instruction; e.g. Clean, Rebuild.</param>
        public static void RunBuildConfiguration(string instruction)
        {
            if (instruction.Equals("Clean", StringComparison.OrdinalIgnoreCase))
            {
                // Quick delete, run right away in-line
                Clean();
                return;
            }

            String normalisedOutputDirectory = Path.GetFullPath(m_currentProject.Environment.Subst(m_currentProject.OutputDirectory));
            if (instruction.Equals("Rebuild", StringComparison.OrdinalIgnoreCase))
            {
                // Clean first
                Clean();
            }

            ProcessStartInfo psInfo = new ProcessStartInfo(Path.Combine(InstallDir, "xgConsole.exe"));
            psInfo.CreateNoWindow = true;
            psInfo.UseShellExecute = false;

            if (instruction.Equals("Stop", StringComparison.OrdinalIgnoreCase))
            {
                // If we're stopping, run immediately and don't raise events. The stop command is fast.
                psInfo.Arguments = "/Stop";

                Process process = new Process();
                process.StartInfo = psInfo;

                process.Start();
                process.WaitForExit();
            }
            else
            {
                // Clean out any output files that don't match the list of expected outputs (in case .sc files have been deleted)
                CleanStrayOutputFiles(m_currentProject);

                // It's long-running. Log stuff and run concurrently.
                m_Running = true;
                if (NotifyUIIncredibuildStateChangeDelegate != null)
                {
                    NotifyUIIncredibuildStateChangeDelegate(true);
                }

                StringBuilder sb = new StringBuilder();
                sb.Append($"\"{m_tempXMLFileName}\" /{instruction}");
                if (!m_currentProject.Nopopups)
                {
                    if (m_currentProject.UseIdeMonitor)
                    {
                        sb.Append(" /UseIDEMonitor");
                    }
                    else
                    {
                        sb.Append(" /OpenMonitor");
                    }
                }

                psInfo.Arguments = sb.ToString();

                psInfo.RedirectStandardOutput = true;
                psInfo.RedirectStandardError = true;

                Process process = new Process();
                process.StartInfo = psInfo;
                process.EnableRaisingEvents = true;
                process.OutputDataReceived += new DataReceivedEventHandler(Process_OutputDataReceived);
                process.ErrorDataReceived += new DataReceivedEventHandler(Process_ErrorDataReceived);
                process.Exited += new EventHandler(OnIncredibuildProcess_Exited);

                process.Start();
                process.BeginErrorReadLine();
                process.BeginOutputReadLine();
            }

        }

        /// <summary>
        /// Create a temporary XML build configuration file.
        /// </summary>
        /// <param name="projectData">Project data.</param>
        public static void CreateBuildConfiguration(IProjectData projectData)
        {
            m_currentProject = projectData;
            projectData.PreBuild();

            string projectFileName = projectData.ProjectFileName.Replace("/", "\\");
            string personalFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
            int lastSlash = projectFileName.LastIndexOf('\\') + 1;
            string projectName = projectFileName.Substring(lastSlash, projectFileName.Length - lastSlash);
            projectName = projectName.Substring(0, projectName.LastIndexOf('.'));

            //  Create the directory in case it doesn't exist
            Directory.CreateDirectory(personalFolder + "\\ragScriptEditor\\XGE\\");
            //  Create the new XML writer
            m_tempXMLFileName = $"{personalFolder}\\ragScriptEditor\\XGE\\{projectName}.xml";
            TempLogFileName = $"{personalFolder}\\ragScriptEditor\\XGE\\{projectName}.log";

            XDocument xmlDocument = new XDocument(new XElement(
                "BuildSet",
                new XAttribute("FormatVersion", m_xmlFormatVersion),
                CreateEnvironmentsXML(),
                CreateProjectXML(projectName, projectData)));

            xmlDocument.Save(m_tempXMLFileName);
        }
        #endregion // Public Static Methods

        #region Private Static Methods
        /// <summary>
        /// Build process output event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private static void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                if (ERRORS_REGEX.IsMatch(e.Data))
                {
                    BuildErrors?.Invoke(e.Data);
                }
                else
                {
                    BuildOutput?.Invoke(e.Data);
                }
            }
        }

        /// <summary>
        /// Build process output event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private static void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                BuildErrors?.Invoke(e.Data);
            }
        }

        /// <summary>
        /// Exited event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private static void OnIncredibuildProcess_Exited(object sender, EventArgs e)
        {
            lock (m_Lock)
            {
                Process senderProcess = (Process)sender;
                bool success = (senderProcess.ExitCode == 0);

                // Sleep to allow logs to flush...
                Thread.Sleep(250);

                NotifyUIIncredibuildCompletionDelegate?.Invoke(success);
                NotifyUIIncredibuildStateChangeDelegate?.Invoke(false);

                m_Running = false;
            }
        }

        /// <summary>
        /// Write the environment data.
        /// </summary>
        private static XElement CreateEnvironmentsXML() => new XElement("Environments", CreateEnvironmentXML());

        /// <summary>
        /// Write a single piece of environment data.
        /// </summary>
        private static XElement CreateEnvironmentXML()
        {
            StringBuilder commandLine = new StringBuilder(); // TODO: Once the sc.exe's command line option has been added "-noprefix ";

            if (m_currentProject.Custom.Length > 0)
            {
                commandLine.Append(m_currentProject.Environment.Subst(m_currentProject.Custom));
            }
            //  Include file
            if (m_currentProject.XmlIncludeFile.Length > 0)
            {
                commandLine.Append($" -include {m_currentProject.Environment.Subst(m_currentProject.XmlIncludeFile)}");
            }
            //  Include directories
            if (m_currentProject.IncludePaths.Any())
            {
                commandLine.Append($" -ipath {String.Join(";", m_currentProject.IncludePaths.Select(m_currentProject.Environment.Subst))}");
            }

            if (m_currentProject.WarningsAsErrors)
            {
                commandLine.Append(" -werror");
            }
			
            // (Always use nopopups for XGE or else you get stalled processes!)
            commandLine.Append(" -nopopups");

            string outputFileMasks = String.Join(",", _outputExtensions.Select(ext => $"*{ext}"));

            ICollection<XElement> tools = new List<XElement>();

            //  Write the compiler tool
            tools.Add(new XElement(
                "Tool",
                new XAttribute("Name", "Compiler"),
                new XAttribute("AllowRemote", "True"),
                new XAttribute("GroupPrefix", "Compiling..."),
                new XAttribute("Params", $"\"$(SourcePath)\" {commandLine}"),
                new XAttribute("Path", m_currentProject.Environment.Subst(m_currentProject.CompilerExecutable)),
                new XAttribute("OutputFileMasks", outputFileMasks)));

            //  Write the resource compiler tool
            foreach (RSG.Platform.Platform enabledPlatform in m_currentProject.EnabledPlatforms)
            {
                string platformName = PlatformUtils.PlatformToFriendlyName(enabledPlatform);
                string platformExt = m_currentProject.GetPlatformExtension(enabledPlatform);
                string resourceExecutable = m_currentProject.ResourceExecutable;
                if (enabledPlatform.Is64BitArchitecture())
                {
                    resourceExecutable = m_currentProject.ResourceExecutable64;
                }

                tools.Add(new XElement(
                    "Tool",
                    new XAttribute("Name", $"{enabledPlatform}Resource"),
                    new XAttribute("AllowRemote", "True"),
                    new XAttribute("GroupPrefix", $"Compiling {platformName} Resources..."),
                    new XAttribute("Params", $"\"{m_currentProject.OutputDirectory}\\{enabledPlatform}\\$(SourceName).sco\""),
                    new XAttribute("Path", resourceExecutable),
                    new XAttribute("OutputFileMasks", $"*{platformExt}")));
            }

            if (m_currentProject.FullBuild)
            {
                //Split the command from its arguments.
                int endCommandIndex = -1;
                string postCompileCommand = m_currentProject.PostCompileCommand;
                string postCompileArguments = "";

                if (m_currentProject.PostCompileCommand.StartsWith("\"") == true)
                {
                    endCommandIndex = m_currentProject.PostCompileCommand.IndexOf('\"', 1);
                }
                else
                {
                    endCommandIndex = m_currentProject.PostCompileCommand.IndexOf(' ', 0);
                }

                if (endCommandIndex > -1)
                {
                    postCompileCommand = m_currentProject.PostCompileCommand.Substring(0, endCommandIndex);
                    postCompileArguments = m_currentProject.PostCompileCommand.Substring(endCommandIndex + 1, m_currentProject.PostCompileCommand.Length - (endCommandIndex + 1));
                }

                foreach (RSG.Platform.Platform enabledPlatform in m_currentProject.EnabledPlatforms)
                {
                    String platformPostCompileArgs = postCompileArguments;
                    platformPostCompileArgs = platformPostCompileArgs.Replace("$(ConfigurationName)", m_currentProject.ConfigurationName);
                    platformPostCompileArgs = platformPostCompileArgs.Replace("$(PlatformName)", enabledPlatform.ToString());
                    platformPostCompileArgs = platformPostCompileArgs.Replace("$(BuildDir)", m_currentProject.BuildDirectory);

                    tools.Add(new XElement(
                        "Tool",
                        new XAttribute("Name", $"Post{enabledPlatform}Compile"),
                        new XAttribute("AllowRemote", "False"),
                        new XAttribute("GroupPrefix", "Post Compile Step..."),
                        new XAttribute("Path", postCompileCommand),
                        new XAttribute("Params", platformPostCompileArgs)));
                }
            }

            return new XElement("Environment", new XAttribute("Name", m_currentProject.ConfigurationName), new XElement("Tools", tools));
        }

        /// <summary>
        /// Write the project details.
        /// </summary>
        /// <param name="projectName">Project name.</param>
        private static XElement CreateProjectXML(string projectName, IProjectData projectData)
        {
            //  Write the compile tasks
            ICollection<XElement> tasks = new List<XElement>();
            foreach( RSG.Platform.Platform enabledPlatform in m_currentProject.EnabledPlatforms )
            {
                tasks.Add( CreateCompileTasksXML( projectData, enabledPlatform ) );
            }

            if (!m_currentProject.UsesPostBuild)
            {
                //  Write the resource tasks
                foreach (RSG.Platform.Platform enabledPlatform in m_currentProject.EnabledPlatforms)
                {
                    tasks.Add(CreateResourceTasksXML(projectData, enabledPlatform));
                }
            }

            //  Write the post compile task
            if (m_currentProject.FullBuild)
            {
                tasks.AddRange(CreatePostCompileTasksXML());
            }

            return new XElement(
                "Project",
                new XAttribute("Name", projectName),
                new XAttribute("Env", m_currentProject.ConfigurationName),
                new XElement("TaskGroup", new XAttribute("Name", "Build"), new XAttribute("Env", m_currentProject.ConfigurationName), tasks));
        }

        /// <summary>
        /// Write the compile tasks XML.
        /// </summary>
        private static XElement CreateCompileTasksXML(IProjectData projectData, Platform.Platform platform)
        {
            //  Write a task for each file
            ICollection<XElement> tasks = new List<XElement>();
            IList<String> projectFilenames = projectData.GetFilesToCompile();
            string platformName = platform.ToString();
            foreach( string file in projectFilenames.Where(f => Path.GetExtension(f).Equals(".sc", StringComparison.OrdinalIgnoreCase)))
            {
                String outputWithoutExtension = Path.Combine(projectData.Environment.Subst(projectData.OutputDirectory), platformName, Path.GetFileNameWithoutExtension(file));

                tasks.Add(
                    new XElement(
                        "Task",
                        new XAttribute("SourceFile", file),
                        new XAttribute("Params", $"$(inherited:params) -PlatformName {platformName} -output {Path.ChangeExtension(outputWithoutExtension, ".sco")}"),
                        new XAttribute("InputFiles", String.Join(",", projectData.GetIncludesForFile(file))),
                        new XAttribute("OutputFiles", String.Join(",", _outputExtensions.Select(ext => Path.ChangeExtension(outputWithoutExtension, ext))))));
            }
            return new XElement("TaskGroup", new XAttribute("Name", $"CompileSourceFiles{platformName}"), new XAttribute("Tool", "Compiler"), tasks);
        }

        /// <summary>
        /// Mark directory files as writable and delete them.
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="wildcard">Wildcard for what paths to clear out.</param>
        private static void CleanDirectory(String directory, String wildcard = "*.*")
        {
            try
            {
                String normalisedOutputDirectory = Path.GetFullPath(directory);
                if (!Directory.Exists(normalisedOutputDirectory))
                {
                    return;
                }

                BuildOutput?.Invoke($"Clearing {Path.Combine(directory, wildcard)}");

                // Clear all readonly files in directory
                DirectoryInfo di = new DirectoryInfo(normalisedOutputDirectory);
                di.Attributes &= ~FileAttributes.ReadOnly;
                foreach (FileSystemInfo info in di.GetFileSystemInfos(wildcard, SearchOption.AllDirectories))
                {
                    try
                    {
                        info.Attributes &= ~FileAttributes.ReadOnly;
                        info.Delete();
                    }
                    catch (Exception ex)
                    {
                        BuildOutput?.Invoke($"** WARNING: Deletion of {info.FullName} failed due to an exception: {ex.Message}");
                    }
                }
            }
            catch (Exception ex)
            {
                BuildOutput?.Invoke($"** WARNING: Cleaning of outputs in {directory} failed due to an exception: {ex.Message}");
            }
        }

        /// <summary>
        /// Build a list of expected output files.
        /// </summary>
        private static IEnumerable<String> GetExpectedOutputFiles(IProjectData project)
        {
            ICollection<String> expectedOutputFiles = new List<String>();
            foreach (String inputFile in m_currentProject.GetFilesToCompile())
            {
                String outputFile = Path.Combine(project.Environment.Subst(project.OutputDirectory), Path.GetFileName(inputFile));
                foreach (String ext in _outputExtensions)
                {
                    expectedOutputFiles.Add(Path.ChangeExtension(outputFile, ext));
                }
            }
            return expectedOutputFiles;
        }

        /// <summary>
        /// Clean any files in the output directory that are unexpected to be built.
        /// </summary>
        /// <param name="project"></param>
        private static void CleanStrayOutputFiles(IProjectData project)
        {
            IEnumerable<String> expectedOutputFiles = GetExpectedOutputFiles(project);

            if (!Directory.Exists(project.Environment.Subst(project.OutputDirectory)))
            {
                return;
            }

            foreach (String unexpectedFile in Directory.EnumerateFiles(project.Environment.Subst(project.OutputDirectory))
                .Except(expectedOutputFiles, StringComparer.OrdinalIgnoreCase))
            {
                CleanFile(unexpectedFile);
            }
        }

        /// <summary>
        /// Mark single file as writable and delete it.
        /// </summary>
        /// <param name="filename"></param>
        private static void CleanFile(String filename)
        {
            try
            {
                String normalizedFilename = Path.GetFullPath(filename);
                if (!File.Exists(normalizedFilename))
                {
                    return;
                }

                FileInfo fi = new FileInfo(normalizedFilename);
                fi.Attributes &= ~FileAttributes.ReadOnly;

                fi.Delete();
            }
            catch (Exception ex)
            {
                BuildOutput?.Invoke($"** WARNING: Cleaning of output {filename} failed due to an exception: {ex.Message}");
            }
        }

        private static XElement CreateResourceTasksXML(IProjectData projectData, RSG.Platform.Platform platform)
        {
            string platformExt = m_currentProject.GetPlatformExtension(platform);
            String dirName = platform.ToString().ToLower();
            string platformName = platform.ToString();

            //  Write a task for each file
            ICollection<XElement> tasks = new List<XElement>();
            IList<String> projectFilenames = projectData.GetFilesToResource(platform);
            foreach (string file in projectFilenames.Where(f => Path.GetExtension(f).Equals(".sc", StringComparison.OrdinalIgnoreCase)))
            {
                String scoFilename = Path.Combine(projectData.OutputDirectory, platformName, Path.GetFileNameWithoutExtension(file)) + ".sco";
                String resourceFilename = Path.Combine(projectData.OutputDirectory, dirName, Path.GetFileNameWithoutExtension(file)) + platformExt;

                tasks.Add(new XElement(
                    "Task",
                    new XAttribute("SourceFile", scoFilename),
                    new XAttribute("Params", $"$(inherited:params) {resourceFilename}"),
                    new XAttribute("OutputFiles", resourceFilename)));
            }
            return new XElement(
                "TaskGroup",
                new XAttribute("SkipIfProjectFailed", "true"),
                new XAttribute("Name", $"Compile{platform}Resource"),
                new XAttribute("Tool", $"{platform}Resource"),
                new XAttribute("DependsOn", $"CompileSourceFiles{platformName}"),
                tasks);
        }


        /// <summary>
        /// Write the post compile task.
        /// </summary>
        private static IEnumerable<XElement> CreatePostCompileTasksXML()
        {
            if (m_currentProject.UsesPostBuild)
            {
                yield break;
            }

            foreach (RSG.Platform.Platform enabledPlatform in m_currentProject.EnabledPlatforms)
            {
	            string platformName = enabledPlatform.ToString();

                yield return new XElement(
                    "Task",
                    new XAttribute("SkipIfProjectFailed", "true"),
                    new XAttribute("Name", $"PostCompileTask{platformName}"),
                    new XAttribute("DependsOn", $"CompileSourceFiles{platformName}, Compile{enabledPlatform}Resource"),
                    new XAttribute("Tool", $"Post{enabledPlatform}Compile"));
            }
        }
        #endregion // Private Static Members
    }
}
