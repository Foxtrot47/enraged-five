﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.BuildScript
{
    /// <summary>
    /// Notify the UI of log messages received.
    /// </summary>
    /// <param name="message"></param>
    public delegate void OnLogMessageReceivedDelegate(string message);
}
