﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace RSG.Pipeline.BuildScript
{
    /// <summary>
    /// Branch helper.
    /// </summary>
    public class BranchHelper
    {
        #region Public properties

        /// <summary>
        /// The branch. E.g. 'dev', 'release'.
        /// </summary>
        public string Branch { get; private set; }

        //True if a file's branch spec was altered.
        public bool FileAltered { get; private set; }

        #endregion

        #region Public methods

        /// <summary>
        /// Set the current branch using the project file as a location reference.
        /// </summary>
        /// <param name="projectFile">Project file.</param>
        public void SetBranchFrom(string projectFile)
        {
            string root = Environment.GetEnvironmentVariable("RS_PROJROOT");
            if (String.IsNullOrEmpty(root))
            {
                throw new EnvironmentVariableNotFoundException("RS_PROJROOT has not been set.");
            }

            string scriptRoot = Path.Combine(root, "script");
            string relPath = projectFile.Substring(scriptRoot.Length + 1); // +1 for the '\' character
            int slashPos = relPath.IndexOf("\\");
            Branch = relPath.Substring(0, slashPos);
            FileAltered = false;
        }

        /// <summary>
        /// Get the branch spec path from the given path.
        /// </summary>
        /// <param name="fullPath">Path.</param>
        /// <returns>Branch spec path.</returns>
        public string GetBranchSpecPath(string fullPath)
        {
            if (String.IsNullOrEmpty(fullPath) || String.IsNullOrEmpty(Branch))
            {
                return fullPath;
            }

            string scriptRoot = Path.Combine(Environment.GetEnvironmentVariable("RS_PROJROOT"), "script");
            if (String.IsNullOrEmpty(scriptRoot))
            {
                throw new EnvironmentVariableNotFoundException("RS_PROJROOT has not been set.");
            }

            // Assuming that our input .scproj is dev and we're in the release branch:
                                                                        // X:\gta5\script\dev\singleplayer\include\blah.sch
            string relPath = String.Empty;

            if (Path.IsPathRooted(fullPath))
            {
                relPath = fullPath.Substring(scriptRoot.Length + 1);    //                dev\singleplayer\include\blah.sch
                int idxSlash = relPath.IndexOf("\\");
                relPath = relPath.Substring(idxSlash + 1);              //                    singleplayer\include\blah.sch
            }
            else
            {
                // It's already a relative-to-branchspec-path
                relPath = fullPath;
            }

            string branchPath = Path.Combine(scriptRoot, Branch, relPath);    // X:\gta5\script\release\singleplayer\include\blah.h

            if (branchPath.IndexOf(fullPath, StringComparison.OrdinalIgnoreCase) == -1 && Path.IsPathRooted(fullPath))
            {
                FileAltered = true;
            }

            return branchPath;
        }

        /// <summary>
        /// Convert a collection of file paths to the current branch.
        /// </summary>
        /// <param name="files">File path collection.</param>
        /// <returns>Corrected list of file paths.</returns>
        public List<string> ConvertAll(IEnumerable<string> files)
        {
            List<string> filesOut = new List<string>();
            foreach (var s in files)
            {
                filesOut.Add(GetBranchSpecPath(s));
            }

            return filesOut;
        }

        #endregion
        
    }
}
