namespace CSharpParser
{
    partial class CSharpCompilingSettingsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ragScriptEditorShared.CompilingSettings compilingSettings1 = new ragScriptEditorShared.CompilingSettings();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CSharpCompilingSettingsControl));
            this.commandLineRichTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            //
            // commandLineRichTextBox
            //
            this.commandLineRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.commandLineRichTextBox.Location = new System.Drawing.Point(6, 230);
            this.commandLineRichTextBox.Name = "commandLineRichTextBox";
            this.commandLineRichTextBox.ReadOnly = true;
            this.commandLineRichTextBox.Size = new System.Drawing.Size(495, 72);
            this.commandLineRichTextBox.TabIndex = 12;
            this.commandLineRichTextBox.Text = "";
            //
            // CSharpCompilingSettingsControl
            //
            this.Controls.Add(this.commandLineRichTextBox);
            this.Name = "CSharpCompilingSettingsControl";
            this.Size = new System.Drawing.Size(504, 260);
            compilingSettings1.CompilerExecutable = "";
            compilingSettings1.ConfigurationName = null;
            compilingSettings1.Custom = "";
            compilingSettings1.IncludeFile = "";
            compilingSettings1.IncludePath = "";
            compilingSettings1.IncludePaths = ((System.Collections.Generic.List<string>)(resources.GetObject("compilingSettings1.IncludePaths")));
            compilingSettings1.OutputDirectory = "";
            compilingSettings1.PostCompileCommand = "";
            compilingSettings1.BuildDirectory = "";
            compilingSettings1.ResourceExecutable = "";
            compilingSettings1.CompilerExecutable = "";
            compilingSettings1.XmlIncludeFile = "";
            this.TabSettings = compilingSettings1;
            this.Controls.SetChildIndex(this.commandLineRichTextBox, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox commandLineRichTextBox;
    }
}
