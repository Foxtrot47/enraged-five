using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

using ragScriptEditorShared;
using RSG.Platform;

namespace CSharpParser
{
    public class CSharpCompilingSettings : CompilingSettings
    {
        public CSharpCompilingSettings()
            : base( "CSharp" )
        {
            m_includeFileOpenFileDialogFilter = string.Empty;

            // for now, disable this option because compiling C# is not yet allowed
            //m_compilerExecutableOpenFileDialogFilter = "Executable Files (*.exe;*.com;*.bat)|*.exe;*.com;*.bat|All Files (*.*)|*.*";
            m_compilerExecutableOpenFileDialogFilter = string.Empty;
        }

        #region Overrides
        /// <remarks>If platform is null, it will not be substituted. This is for settings dialogs where the platform is not defined.</remarks>
        public override string BuildCompileScriptCommand(string scriptFilename, Platform? currentPlatform)
        {
            // can't compile at this time
            return string.Empty;
        }

        public override IEnumerable<string> BuildOutputFilenames(string scriptFilename, Platform platform)
        {
            // can't compile at this time
            yield break;
        }

        public override SettingsBase Clone()
        {
            CSharpCompilingSettings c = new CSharpCompilingSettings();
            c.Copy( this );

            return c;
        }

        public override void Copy( SettingsBase t )
        {
            if ( t is CSharpCompilingSettings )
            {
                Copy( t as CSharpCompilingSettings );
            }
            else if ( t is CompilingSettings )
            {
                base.Copy( t as CompilingSettings );
            }
        }

        public override void Copy( CompilingSettings c )
        {
            if ( c is CSharpCompilingSettings )
            {
                Copy( c as CSharpCompilingSettings );
            }
            else
            {
                base.Copy( c );
            }
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new CSharpCompilingSettingsControl();
        }

        public override bool Equals( object obj )
        {
            CSharpCompilingSettings c = obj as CSharpCompilingSettings;
            if ( (object)c == null )
            {
                return false;
            }

            return base.Equals( c );
        }

        public override bool Equals( CompilingSettings c )
        {
            if ( (object)c == null )
            {
                return false;
            }

            if ( c is CSharpCompilingSettings )
            {
                return Equals( c as CSharpCompilingSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override void Reset()
        {
            base.Reset();

            m_compilerExecutable = @"C:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.com";
        }
        #endregion

        #region Public Functions
        public void Copy( CSharpCompilingSettings c )
        {
            if ( c == null )
            {
                return;
            }

            base.Copy( c );

            this.IncludePath = c.IncludePath;
        }

        public bool Equals( CSharpCompilingSettings c )
        {
            if ( (object)c == null )
            {
                return false;
            }

            return (this.IncludePath == c.IncludePath)
                && base.Equals( c );
        }
        #endregion
    }

    public class CSharpLanguageSettings : LanguageSettings
    {
        public CSharpLanguageSettings()
            : base( "CSharp" )
        {

        }

        #region Variables
        private string m_languageHelpFile = string.Empty;
        #endregion

        #region Properties
        [XmlIgnore]
        public string LanguageHelpFile
        {
            get
            {
                if ( String.IsNullOrEmpty( m_languageHelpFile ) )
                {
                    return m_languageHelpFile;
                }

                ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( m_languageHelpFile );
                if ( psInfo != null )
                {
                    if ( m_languageHelpFile.StartsWith( "\"" ) )
                    {
                        return "\"" + psInfo.FileName + "\" " + psInfo.Arguments;
                    }

                    return psInfo.FileName + " " + psInfo.Arguments;
                }

                return null;
            }
            set
            {
                m_languageHelpFile = value;
            }
        }

        [XmlElement( "LanguageHelpFile" )]
        public string XmlLanguageHelpFile
        {
            get
            {
                return m_languageHelpFile;
            }
            set
            {
                m_languageHelpFile = value;
            }
        }
        #endregion

        #region Overrides
        public override SettingsBase Clone()
        {
            CSharpLanguageSettings l = new CSharpLanguageSettings();
            l.Copy( this );

            return l;
        }

        public override void Copy( SettingsBase t )
        {
            if ( t is CSharpLanguageSettings )
            {
                Copy( t as CSharpLanguageSettings );
            }
            else if ( t is LanguageSettings )
            {
                base.Copy( t as LanguageSettings );
            }
        }

        public override void Copy( LanguageSettings l )
        {
            if ( l is CSharpLanguageSettings )
            {
                Copy( l as CSharpLanguageSettings );
            }
            else
            {
                base.Copy( l );
            }
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new CSharpLanguageSettingsControl();
        }

        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            CSharpLanguageSettings l = obj as CSharpLanguageSettings;
            if ( (object)l == null )
            {
                return false;
            }

            return (this.XmlLanguageHelpFile == l.XmlLanguageHelpFile)
                && base.Equals( obj );
        }

        public override bool Equals( LanguageSettings l )
        {
            if ( (object)l == null )
            {
                return false;
            }

            if ( l is CSharpLanguageSettings )
            {
                return Equals( l as CSharpLanguageSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            StringBuilder s = new StringBuilder();
            s.Append( this.CacheDirectory );
            s.Append( this.CodeSnippetPath );
            s.Append( this.Language );
            s.Append( this.Name );
            s.Append( this.XmlLanguageHelpFile );

            return s.ToString().GetHashCode();
        }

        public override List<ToolStripMenuItem> GetHelpMenuItems()
        {
            List<ToolStripMenuItem> menuItems = new List<ToolStripMenuItem>();

            ToolStripMenuItem languageHelpMenuItem = new ToolStripMenuItem();
            languageHelpMenuItem.Name = "CSharpLanguageHelpMenuItem";
            languageHelpMenuItem.Text = "CSharp Language Help";
            languageHelpMenuItem.Click += new EventHandler( languageHelpMenuItem_Click );

            ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( m_languageHelpFile );
            languageHelpMenuItem.Enabled = (psInfo != null) ? File.Exists( psInfo.FileName ) : false;

            menuItems.Add( languageHelpMenuItem );

            return menuItems;
        }

        public override void Reset()
        {
            base.Reset();

            this.LanguageHelpFile = string.Empty;
            try
            {
                if ( File.Exists( "C:\\Program Files\\Common Files\\Microsoft Shared\\Help 8\\dexplore.exe" ) )
                {
                    this.LanguageHelpFile = "\"C:\\Program Files\\Common Files\\Microsoft Shared\\Help 8\\dexplore.exe\" /helpcol ms-help://ms.vscc.v80 /LaunchNamedUrlTopic DefaultPage /usehelpsettings VisualStudio.8.0";
                }
            }
            catch
            {

            }
        }

        public override bool Validate( out string error )
        {
            if ( !base.Validate( out error ) )
            {
                return false;
            }

            if ( !String.IsNullOrEmpty( this.LanguageHelpFile ) )
            {
                ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( m_languageHelpFile );
                if ( psInfo == null )
                {
                    error = "Could not create ProcessStartInfo for Language Help File '" + this.LanguageHelpFile + "'.";
                    return false;
                }

                if ( !File.Exists( psInfo.FileName ) )
                {
                    error = "Language Help File '" + psInfo.FileName + "' does not exist.";
                    return false;
                }
            }

            error = null;
            return true;
        }
        #endregion

        #region Public Functions
        public void Copy( CSharpLanguageSettings l )
        {
            if ( l == null )
            {
                return;
            }

            base.Copy( l );

            this.LanguageHelpFile = l.XmlLanguageHelpFile;
        }

        public bool Equals( CSharpLanguageSettings l )
        {
            if ( (object)l == null )
            {
                return false;
            }

            return (this.XmlLanguageHelpFile == l.XmlLanguageHelpFile)
                && base.Equals( l );
        }
        #endregion

        #region Event Handlers
        private void languageHelpMenuItem_Click( object sender, EventArgs e )
        {
            if ( !String.IsNullOrEmpty( this.LanguageHelpFile ) )
            {
                try
                {
                    ProcessStartInfo psInfo = ApplicationSettings.BuildProcessInfoFromCommand( this.LanguageHelpFile );
                    if ( (psInfo != null) && File.Exists( psInfo.FileName ) )
                    {
                        Process.Start( psInfo );
                    }
                }
                catch ( Exception ex )
                {
                    ApplicationSettings.ShowMessage( null,
                        String.Format( "Could not load CSharp Language Help File.\n\n{0}", ex.Message ),
                        "Help File Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, DialogResult.OK );
                }
            }
        }
        #endregion
    }

    public class CSharpLanguageDefaultEditorSettings : LanguageDefaultEditorSettingsBase
    {
        public CSharpLanguageDefaultEditorSettings()
            : base( "CSharp", CSharpLanguageDefaultEditorSettings.SettingsVersion )
        {

        }

        #region Constants
        public const float SettingsVersion = 1.0f;
        #endregion

        #region Overrides
        public override CompilingSettings CreateCompilingTabSettings()
        {
            return new CSharpCompilingSettings();
        }

        public override LanguageSettings CreateLanguageTabSettings()
        {
            return new CSharpLanguageSettings();
        }

        public override Type GetCompilingSettingsDerivedType()
        {
            return typeof( CSharpCompilingSettings );
        }

        public override Type GetLanguageSettingsDerivedType()
        {
            return typeof( CSharpLanguageSettings );
        }
        #endregion
    }
}
