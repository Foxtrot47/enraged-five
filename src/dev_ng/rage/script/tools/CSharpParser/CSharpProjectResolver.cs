using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

using ragScriptEditorShared;
using ActiproSoftware.SyntaxEditor;
using ActiproSoftware.SyntaxEditor.Addons.CSharp;
using ActiproSoftware.SyntaxEditor.Addons.DotNet.Ast;
using ActiproSoftware.SyntaxEditor.Addons.DotNet.Dom;
using RSG.Platform;

namespace CSharpParser
{
    public class CSharpProjectResolver : DotNetProjectResolver, IParserProjectResolver
    {
        public CSharpProjectResolver( CSharpParserPlugin plugin )
        {
            m_parserPlugin = plugin;

            this.CachePath = m_parserPlugin.CurrentLanguageSettings.CacheDirectory;
            this.AddExternalReferenceForMSCorLib();
            this.AddExternalReferenceForSystemAssembly( "System" );
            this.AddExternalReferenceForSystemAssembly( "System.Data" );
            this.AddExternalReferenceForSystemAssembly( "System.Drawing" );
            this.AddExternalReferenceForSystemAssembly( "System.Windows.Forms" );
        }

        #region Static Variables
        private static SyntaxLanguage sm_language = null;
        #endregion

        #region Variables
        private CSharpParserPlugin m_parserPlugin;
        private List<string> m_projectFiles = new List<string>();
        #endregion

        #region IParserProjectResolver interface
        public ActiproSoftware.SyntaxEditor.SyntaxLanguage Language 
        { 
            get
            {
                return sm_language;
            }
        }

        public List<string> ProjectFiles 
        {
            get
            {
                return m_projectFiles;
            }
            set
            {
                // remove old references
                foreach ( string projectFile in m_projectFiles )
                {
                    if ( !value.Contains( projectFile ) )
                    {
                        this.SourceProjectContent.Clear( projectFile );
                    }
                }

                // add new references
                if ( (sm_language != null) && (sm_language is CSharpSyntaxLanguage) )
                {
                    foreach ( string file in value )
                    {
                        if ( !m_projectFiles.Contains( file ) )
                        {
                            this.SourceProjectContent.LoadForFile( sm_language as CSharpSyntaxLanguage, file );
                        } 
                    }
                }

                m_projectFiles = value;
            }
        }

        public bool GetImplementationInformation( SyntaxEditor editor, out string[] fileNames, out TextRange[] textRanges )
        {
            fileNames = null;
            textRanges = null;
            return false;
        }

        public void PreCompile()
        {

        }

        public bool NeedsToBeCompiled( string filename, string projectFile, CompilingSettings compilingSettings )
        {
            // we have no integrated compiler for CSharp
            return false;
        }

        public bool NeedsToBeResourced(string filename, string projectFile, CompilingSettings compilingSettings)
        {
            // we have no integrated compiler for CSharp
            return false;
        }

        public void FileCompiled( string filename )
        {

        }

        /// <summary>
        /// Determines if resourcing is necessary for the file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="platform"></param>
        /// <param name="projectFile">The project we are compiling, if any.</param>
        /// <param name="compilingSettings">The CompilingSettings we will be using for the compile.</param>
        /// <returns>True to recompile</returns>
        public bool NeedsToBeResourced(String filename, Platform platform, String projectFile, CompilingSettings compilingSettings)
        {
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="platform"></param>
        public void FileResourced(String filename, Platform platform)
        {
        }
        public void PostCompile()
        {

        }        
        public void SetLanguage( SyntaxEditor editor, InfoTipTextDelegate infoTipTextDel )
        {
            if ( sm_language == null )
            {
                try
                {
                    // Since we know we don't have the license key, just throw the exception and load the xml version
                    //sm_language = new CSharpSyntaxLanguage();
                    throw (new System.ComponentModel.LicenseException( typeof( CSharpSyntaxLanguage ) ));
                }
                catch ( System.ComponentModel.LicenseException )
                {
                    Stream stream = null;
                    try
                    {
                        stream = GetType().Module.Assembly.GetManifestResourceStream( "CSharpParser.ActiproSoftware.CSharp.xml" );
                    }
                    catch ( Exception e )
                    {
                        ApplicationSettings.ShowMessage( null, e.ToString(), "GetManifestResourceStream Error", 
                            MessageBoxButtons.OK, MessageBoxIcon.Error, DialogResult.OK );
                        return;
                    }

                    sm_language = ActiproSoftware.SyntaxEditor.Addons.Dynamic.DynamicSyntaxLanguage.LoadFromXml( stream, 0 );
                }
            }

            editor.Document.Language = sm_language;
            editor.Document.LanguageData = this;

            // FIXME: maybe add code snippet folders here
            // FIXME: maybe verify that we are a ProjectFile or a IncludePathFile            
        }

        public void ClearLanguage( SyntaxEditor editor )
        {
            editor.Document.Language = ActiproSoftware.SyntaxEditor.Addons.PlainText.PlainTextSyntaxLanguage.PlainText;
            editor.Document.LanguageData = null;
        }

        public IntellisenseSettingsChangedEventHandler IntellisenseSettingsChangedEventHandler 
        { 
            get
            {
                return new IntellisenseSettingsChangedEventHandler( IntellisenseSettingsChanged );
            }
        }

        public void RefreshIntellisense()
        {
            // do nothing
        }

        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( sm_language != null )
                {
                    sm_language.Dispose();
                }
            }
            
            base.Dispose( disposing );

            if ( disposing )
            {
                this.PruneCache();
            }
        }

        public IEnumerable<string> GetIncludesForFile(string filename)
        {
            yield break;
        }
        #endregion

        #region Event Handlers
        private void IntellisenseSettingsChanged( object sender, IntellisenseSettingsChangedEventArgs e )
        {
            if ( (sm_language != null) && (sm_language is CSharpSyntaxLanguage) )
            {
                CSharpSyntaxLanguage cSharp = sm_language as CSharpSyntaxLanguage;
                cSharp.IntelliPromptMemberListEnabled = e.IntellisenseSettings.PerformSemanticParsing;
                cSharp.IntelliPromptParameterInfoEnabled = e.IntellisenseSettings.PerformSemanticParsing;
                cSharp.IntelliPromptQuickInfoEnabled = e.IntellisenseSettings.PerformSemanticParsing;
            }

            if ( (e.Editor.Document.Language != null) && (this.Language != null)
                && (e.Editor.Document.Language.Key == this.Language.Key) )
            {
                e.Editor.Document.SemanticParsingEnabled = e.IntellisenseSettings.PerformSemanticParsing;
            }
        }
        #endregion
    }
}
