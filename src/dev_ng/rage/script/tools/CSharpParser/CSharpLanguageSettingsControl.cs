using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ragScriptEditorShared;

namespace CSharpParser
{
    public partial class CSharpLanguageSettingsControl : ragScriptEditorShared.LanguageSettingsControl
    {
        public CSharpLanguageSettingsControl()
        {
            InitializeComponent();
        }

        #region Overrides
        public override SettingsBase TabSettings
        {
            get
            {
                SettingsBase settings = base.TabSettings;
                if ( settings is LanguageSettings )
                {
                    CSharpLanguageSettings languageSettings = new CSharpLanguageSettings();
                    languageSettings.Copy( settings as LanguageSettings );

                    languageSettings.LanguageHelpFile = this.languageHelpFileTextBox.Text.Trim();

                    return languageSettings;
                }

                return null;
            }
            set
            {
                base.TabSettings = value;

                if ( value is CSharpLanguageSettings )
                {
                    m_invokeSettingsChanged = false;

                    CSharpLanguageSettings languageSettings = value as CSharpLanguageSettings;

                    this.languageHelpFileTextBox.Text = languageSettings.XmlLanguageHelpFile;

                    m_invokeSettingsChanged = true;
                }
            }
        }
        #endregion

        #region Event Handlers
        private void CSharpSettingsChanged( object sender, EventArgs e )
        {
            OnSettingsChanged();
        }

        private void languageHelpFileBrowseButton_Click( object sender, EventArgs e )
        {
            DialogResult result = this.openFileDialog1.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.languageHelpFileTextBox.Text = this.openFileDialog1.FileName;
            }
        }
        #endregion
    }
}

