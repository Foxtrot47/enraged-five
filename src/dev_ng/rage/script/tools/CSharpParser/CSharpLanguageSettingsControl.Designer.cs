namespace CSharpParser
{
    partial class CSharpLanguageSettingsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ragScriptEditorShared.LanguageSettings languageSettings1 = new ragScriptEditorShared.LanguageSettings();
            this.languageHelpFileLabel = new System.Windows.Forms.Label();
            this.languageHelpFileTextBox = new System.Windows.Forms.TextBox();
            this.languageHelpFileBrowseButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // languageHelpFileLabel
            // 
            this.languageHelpFileLabel.AutoSize = true;
            this.languageHelpFileLabel.Location = new System.Drawing.Point( 3, 112 );
            this.languageHelpFileLabel.Name = "languageHelpFileLabel";
            this.languageHelpFileLabel.Size = new System.Drawing.Size( 99, 13 );
            this.languageHelpFileLabel.TabIndex = 3;
            this.languageHelpFileLabel.Text = "Language Help File";
            // 
            // languageHelpFileTextBox
            // 
            this.languageHelpFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.languageHelpFileTextBox.Location = new System.Drawing.Point( 108, 109 );
            this.languageHelpFileTextBox.Name = "languageHelpFileTextBox";
            this.languageHelpFileTextBox.Size = new System.Drawing.Size( 336, 20 );
            this.languageHelpFileTextBox.TabIndex = 4;
            this.languageHelpFileTextBox.TextChanged += new System.EventHandler( this.CSharpSettingsChanged );
            // 
            // languageHelpFileBrowseButton
            // 
            this.languageHelpFileBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.languageHelpFileBrowseButton.Location = new System.Drawing.Point( 450, 107 );
            this.languageHelpFileBrowseButton.Name = "languageHelpFileBrowseButton";
            this.languageHelpFileBrowseButton.Size = new System.Drawing.Size( 25, 23 );
            this.languageHelpFileBrowseButton.TabIndex = 5;
            this.languageHelpFileBrowseButton.Text = "...";
            this.languageHelpFileBrowseButton.UseVisualStyleBackColor = true;
            this.languageHelpFileBrowseButton.Click += new System.EventHandler( this.languageHelpFileBrowseButton_Click );
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.RestoreDirectory = true;
            this.openFileDialog1.Title = "Select Language Help File";
            // 
            // CSharpLanguageSettingsControl
            // 
            this.Controls.Add( this.languageHelpFileBrowseButton );
            this.Controls.Add( this.languageHelpFileTextBox );
            this.Controls.Add( this.languageHelpFileLabel );
            this.Name = "CSharpLanguageSettingsControl";
            this.Size = new System.Drawing.Size( 478, 146 );
            languageSettings1.CacheDirectory = "";
            languageSettings1.CodeSnippetPath = "";
            languageSettings1.CodeSnippetPaths = new string[0];
            this.TabSettings = languageSettings1;
            this.Controls.SetChildIndex( this.languageHelpFileLabel, 0 );
            this.Controls.SetChildIndex( this.languageHelpFileTextBox, 0 );
            this.Controls.SetChildIndex( this.languageHelpFileBrowseButton, 0 );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label languageHelpFileLabel;
        private System.Windows.Forms.TextBox languageHelpFileTextBox;
        private System.Windows.Forms.Button languageHelpFileBrowseButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}
