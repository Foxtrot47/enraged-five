using System;
using ragScriptEditorShared;

namespace CSharpParser
{
    public partial class CSharpCompilingSettingsControl : CompilingSettingsControl
    {
        public CSharpCompilingSettingsControl()
        {
            InitializeComponent();

            base.SettingsChanged += new EventHandler( CSharpCompilingTabSettingsControl_SettingsChanged );
            base.ProjectFilenameChanged += new EventHandler( CSharpCompilingTabSettingsControl_SettingsChanged );
        }

        #region Overrides
        public override SettingsBase TabSettings
        {
            get
            {
                SettingsBase settings = base.TabSettings;
                if ( settings != null )
                {
                    CSharpCompilingSettings compilingSettings = new CSharpCompilingSettings();
                    compilingSettings.Copy( settings );

                    return compilingSettings;
                }

                return null;
            }
            set
            {
                base.TabSettings = value;
            }
        }
        #endregion

        #region Event Handlers
        private void CSharpCompilingTabSettingsControl_SettingsChanged( object sender, EventArgs e )
        {
            CSharpCompilingSettings compilingSettings = (CSharpCompilingSettings)this.TabSettings;
            string cmd = compilingSettings.BuildCompileScriptCommand( "%1", null);
            this.commandLineRichTextBox.Text = (cmd != null) ? cmd : string.Empty;
        }
        #endregion
    }
}

