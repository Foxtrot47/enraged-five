using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

using ragScriptEditorShared;
using ActiproSoftware.SyntaxEditor;

namespace CSharpParser
{
    public class CSharpParserPlugin : IParserPlugin
    {
        public CSharpParserPlugin()
        {
            m_extensions.Add( ".cs" );
        }

        #region Variables
        private List<string> m_extensions = new List<string>();

        private string m_includeFile = string.Empty;
        private List<string> m_includePathFiles = new List<string>();
        private List<string> m_includePaths = new List<string>();
        CSharpLanguageSettings m_currentLanguageSettings = new CSharpLanguageSettings();
        CSharpLanguageDefaultEditorSettings m_defaultEditorSettings = new CSharpLanguageDefaultEditorSettings();

        List<IParserProjectResolver> m_projectResolvers = new List<IParserProjectResolver>();
        private string m_custom;
        #endregion

        #region IParserPlugin overrides
        public List<string> CompilableExtensions 
        {
            get
            {
                // nothing can be compiled at this time
                return new List<string>();
            }
        }

        public LanguageDefaultEditorSettingsBase DefaultEditorSettings
        {
            get
            {
                return m_defaultEditorSettings;
            }
        }

        public string IncludeFile
        {
            get
            {
                return m_includeFile;
            }
            set
            {
                if ( (m_includeFile != null) && (m_includeFile != string.Empty)
                    && (m_includeFile != value.ToLower()) )
                {
                    string includePathFile = GetFullPathName( m_includeFile, null );
                    foreach ( CSharpProjectResolver projectResolver in m_projectResolvers )
                    {
                        projectResolver.SourceProjectContent.Clear( includePathFile.ToLower() );
                    }
                }

                if ( value != null )
                {
                    m_includeFile = value.ToLower();
                    string includePathFile = GetFullPathName( m_includeFile, null );

                    if ( value != string.Empty )
                    {
                        foreach ( CSharpProjectResolver projectResolver in m_projectResolvers )
                        {
                            if ( projectResolver.Language != null )
                            {
                                projectResolver.SourceProjectContent.LoadForFile( projectResolver.Language as ISemanticParserServiceProcessor,
                                   includePathFile );
                            }
                        }
                    }
                }
                else
                {
                    m_includeFile = null;
                }
            }
        }

        public IEnumerable<string> IncludePathFiles
        {
            get
            {
                return m_includePathFiles;
            }
        }

        public IEnumerable<string> IncludePaths
        {
            get
            {
                return m_includePaths;
            }
            set
            {
                m_includePaths = value.ToList();

                // collect include files
                List<string> includeFiles = new List<string>();
                foreach ( string path in m_includePaths )
                {
                    if (!Directory.Exists(path))
                        continue;

                    foreach ( string ext in m_extensions )
                    {
                        string[] files = Directory.GetFiles( path, "*" + ext );
                        foreach ( string file in files )
                        {
                            includeFiles.Add( file.ToLower() );
                        }
                    }
                }

                // remove old references
                foreach ( string includePathFile in m_includePathFiles )
                {
                    if ( !includeFiles.Contains( includePathFile ) )
                    {
                        foreach ( CSharpProjectResolver projectResolver in m_projectResolvers )
                        {
                            projectResolver.SourceProjectContent.Clear( includePathFile );
                        }
                    }
                }

                // add new references
                foreach ( string file in includeFiles )
                {
                    if ( !m_includePathFiles.Contains( file ) )
                    {
                        foreach ( CSharpProjectResolver projectResolver in m_projectResolvers )
                        {
                            if ( projectResolver.Language != null )
                            {
                                projectResolver.SourceProjectContent.LoadForFile( projectResolver.Language as ISemanticParserServiceProcessor,
                                    file );
                            }
                        }
                    }
                }

                m_includePathFiles = includeFiles;
            }
        }

        public LanguageSettings CurrentLanguageSettings
        {
            get
            {
                return m_currentLanguageSettings;
            }
        }

        public List<string> Extensions 
        { 
            get
            {
                return m_extensions;
            }
        }

        public string LanguageName 
        {
            get
            {
                return "CSharp";
            }
        }

        public List<IParserProjectResolver> ProjectResolvers 
        {
            get
            {
                return m_projectResolvers;
            }
        }

        public string Custom
        {
            get { return m_custom; }
            set { m_custom = value; }
        }

        public IParserProjectResolver CreateProjectResolver( List<string> projectFiles )
        {
            CSharpProjectResolver projectResolver = new CSharpProjectResolver( this );
            projectResolver.ProjectFiles = projectFiles;

            m_projectResolvers.Add( projectResolver );

            return projectResolver;
        }
        
        public IParserProjectResolver CreateProjectResolver( string filename )
        {
            List<string> filenames = new List<string>();
            filenames.Add( filename );
            return CreateProjectResolver( filenames );
        }

        public void DestroyProjectResolver( IParserProjectResolver projectResolver )
        {
            m_projectResolvers.Remove( projectResolver );
            projectResolver.Dispose();
        }
        
        public void InitPlugin( string directory )
        {
            LoadSettings();
        }
        
        public bool LoadSettings()
        {
            // save defaults to our temporaries
            this.IncludePaths = (m_defaultEditorSettings.CompilingSettingsList[0] as CSharpCompilingSettings).IncludePaths;
            this.IncludeFile = string.Empty;
            this.Custom = string.Empty;
            this.CurrentLanguageSettings.Copy( m_defaultEditorSettings.Language );

            return true;
        }
        
        public bool SaveSettings()
        {
            // bring our temporaries back to our permanent
            (m_defaultEditorSettings.CompilingSettingsList[0] as CSharpCompilingSettings).IncludePaths = m_includePaths;
            m_defaultEditorSettings.Language.Copy( m_currentLanguageSettings );
            
            return true;
        }
        
        public void ShutdownPlugin()
        {
            foreach ( IParserProjectResolver projectResolver in m_projectResolvers )
            {
                projectResolver.Dispose();
            }

            m_projectResolvers.Clear();
        }

        public bool UpdateDocumentOutline( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, TreeView treeView )
        {
            if ( editor.Document.SemanticParseData is ICompilationUnit )
            {
                // Load the document outline 
                ICompilationUnit compilationUnit = (ICompilationUnit)editor.Document.SemanticParseData;
                CSharpDocumentOutlineUpdater docOutliner = new CSharpDocumentOutlineUpdater( editor.Document.Guid, treeView );
                docOutliner.Process( compilationUnit );

                // Print the tree of the AST (for debugging only)
                // Trace.WriteLine(compilationUnit.ToStringTree());
                return true;
            }

            return false;
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Turns filename into a fully-qualified path name that exists in the relativeDirectory directory or one of the include paths.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="relativeDirectory"></param>
        /// <returns>Null if file does not exist, otherwise the lowercase full path name.</returns>
        public string GetFullPathName( string filename, string relativeDirectory )
        {
            string fullPathName = filename.Replace( "/", @"\" );

            if ( fullPathName.StartsWith( ".\\" ) )
            {
                fullPathName = fullPathName.Remove( 0, 2 );
            }

            if ( fullPathName.IndexOfAny( Path.GetInvalidPathChars() ) != -1 )
            {
                return null;
            }

            string currentDirectory = Directory.GetCurrentDirectory();
            if ( relativeDirectory != null )
            {
                Directory.SetCurrentDirectory( relativeDirectory );
            }

            if ( !Path.IsPathRooted( fullPathName ) )
            {
                // add the include path:
                foreach ( string path in m_includePaths )
                {
                    if ( path == "." )
                    {
                        // let current directory check down there handle this
                        continue;
                    }

                    string fullName = Path.Combine( path, fullPathName );
                    if ( File.Exists( fullName ) )
                    {
                        Directory.SetCurrentDirectory( currentDirectory );
                        return Path.GetFullPath( fullName ).ToLower();
                    }
                }

                // check current directory
                string localName = Path.Combine( Directory.GetCurrentDirectory(), fullPathName );
                if ( File.Exists( localName ) )
                {
                    Directory.SetCurrentDirectory( currentDirectory );
                    return Path.GetFullPath( localName ).ToLower();
                }
            }
            else if ( File.Exists( fullPathName ) )
            {
                Directory.SetCurrentDirectory( currentDirectory );
                return Path.GetFullPath( fullPathName ).ToLower();
            }

            return null;
        }

        public void OnLanguageSettingsUpdated()
        {
            foreach ( CSharpProjectResolver projectResolver in m_projectResolvers )
            {
                projectResolver.CachePath = this.CurrentLanguageSettings.CacheDirectory;
            }
        }
        #endregion
    }
}
