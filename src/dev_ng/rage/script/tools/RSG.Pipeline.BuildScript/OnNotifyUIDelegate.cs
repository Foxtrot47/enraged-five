﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.BuildScript
{
    /// <summary>
    /// Notify the UI.
    /// </summary>
    /// <param name="running">True if the process is running.</param>
    public delegate void OnNotifyUIDelegate(bool running);
}
