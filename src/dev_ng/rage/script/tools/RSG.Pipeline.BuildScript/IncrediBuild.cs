﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Text;
using Microsoft.Win32;

namespace RSG.Pipeline.BuildScript
{
    /// <summary>
    /// IncrediBuild Wrapper class. 
    /// </summary>
    public static class IncrediBuild
    {
        #region Constants
        private static readonly Regex ERRORS_REGEX = new Regex("COMPILE FAILED|Error:.*", RegexOptions.IgnoreCase);
        private static readonly String[] _outputExtensions = new String[] { ".sco", ".scd", ".global_block.txt" };
        #endregion // Constants

        #region Private member fields
        //  Registry variables
        private static string m_registryKey = "Software\\Xoreax\\IncrediBuild\\Builder";
        private static string m_regexpVersion = "VersionText";
        private static string m_regexpInstallDir = "Folder";
        //  XML variables
        private static StreamWriter m_xmlWriter = null;
        private static IProjectData m_currentProject;
        private static int m_xmlFormatVersion = 1;
        private static string m_tempXMLFileName = null;

        private static bool m_Running = false;
        private static object m_Lock = new object();
        #endregion // Private member fields

        #region Public events
        public static OnNotifyUIDelegate NotifyUIIncredibuildStateChangeDelegate;
        public static OnNotifyUICompletionDelegate NotifyUIIncredibuildCompletionDelegate;
        public static OnLogMessageReceivedDelegate BuildOutput;
        public static OnLogMessageReceivedDelegate BuildErrors;
        #endregion // Public events

        #region Properties
        /// <summary>
        /// Temporary log file name.
        /// </summary>
        public static string TempLogFileName { get; private set; }

        /// <summary>
        /// True if IncrediBuild is installed.
        /// </summary>
        public static bool IsInstalled
        {
            get
            {
                return (Version.Length > 0);
            }
        }

        /// <summary>
        /// True if there is a build running.
        /// </summary>
        public static bool IsRunning
        {
            get
            {
                return m_Running;
            }
        }

        /// <summary>
        /// IncrediBuild version number.
        /// </summary>
        public static string Version
        {
            get
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(m_registryKey);
                string version = string.Empty;

                if (key != null)
                {
                    version = key.GetValue(m_regexpVersion).ToString();
                    key.Close();
                }

                return version;
            }
        }

        /// <summary>
        /// IncrediBuild installation directory.
        /// </summary>
        public static string InstallDir
        {
            get
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(m_registryKey);
                string installDir = string.Empty;

                if (key != null)
                {
                    installDir = key.GetValue(m_regexpInstallDir).ToString();
                    key.Close();
                }

                return installDir;
            }
        }
        #endregion // Properties

        #region Public Static Methods
        /// <summary>
        /// Stop the current build.
        /// </summary>
        public static void Stop()
        {
            RunBuildConfiguration("Stop");
        }

        /// <summary>
        /// Perform a clean on the project outputs.
        /// </summary>
        public static void Clean()
        {
            CleanDirectory(m_currentProject.Environment.Subst(m_currentProject.OutputDirectory));

            // Clean script-related stuff from the "preview" directory, which contains files compiled standalone and compiled temporarily.
            String previewBuildDir = Path.Combine(m_currentProject.Environment.Subst(m_currentProject.BuildDirectory), "preview");
            CleanDirectory(previewBuildDir, "*.*sc");
            CleanDirectory(previewBuildDir, "*.sco");
            CleanDirectory(previewBuildDir, "*.xml");
        }

        /// <summary>
        /// Run the previously created configuration.
        /// </summary>
        /// <param name="instruction">Instruction; e.g. Clean, Rebuild.</param>
        public static void RunBuildConfiguration(string instruction)
        {
            if (instruction.Equals("Clean", StringComparison.OrdinalIgnoreCase))
            {
                // Quick delete, run right away in-line
                Clean();
                return;
            }

            String normalisedOutputDirectory = Path.GetFullPath(m_currentProject.Environment.Subst(m_currentProject.OutputDirectory));
            if (instruction.Equals("Rebuild", StringComparison.OrdinalIgnoreCase))
            {
                // Clean first
                Clean();
            }

            ProcessStartInfo psInfo = new ProcessStartInfo(Path.Combine(InstallDir, "xgConsole.exe"));
            psInfo.CreateNoWindow = true;
            psInfo.UseShellExecute = false;

            if (instruction.Equals("Stop", StringComparison.OrdinalIgnoreCase))
            {
                // If we're stopping, run immediately and don't raise events. The stop command is fast.
                psInfo.Arguments = "/Stop";

                Process process = new Process();
                process.StartInfo = psInfo;

                process.Start();
                process.WaitForExit();
            }
            else
            {
                // Clean out any output files that don't match the list of expected outputs (in case .sc files have been deleted)
                CleanStrayOutputFiles(m_currentProject);

                // It's long-running. Log stuff and run concurrently.
                m_Running = true;
                if (NotifyUIIncredibuildStateChangeDelegate != null)
                {
                    NotifyUIIncredibuildStateChangeDelegate(true);
                }

                StringBuilder sb = new StringBuilder();
                sb.Append($"\"{m_tempXMLFileName}\" /{instruction}");
                if (!m_currentProject.Nopopups)
                {
                    if (m_currentProject.UseIdeMonitor)
                    {
                        sb.Append(" /UseIDEMonitor");
                    }
                    else
                    {
                        sb.Append(" /OpenMonitor");
                    }
                }

                psInfo.Arguments = sb.ToString();

                psInfo.RedirectStandardOutput = true;
                psInfo.RedirectStandardError = true;

                Process process = new Process();
                process.StartInfo = psInfo;
                process.EnableRaisingEvents = true;
                process.OutputDataReceived += new DataReceivedEventHandler(Process_OutputDataReceived);
                process.ErrorDataReceived += new DataReceivedEventHandler(Process_ErrorDataReceived);
                process.Exited += new EventHandler(OnIncredibuildProcess_Exited);

                process.Start();
                process.BeginErrorReadLine();
                process.BeginOutputReadLine();
            }

        }

        /// <summary>
        /// Create a temporary XML build configuration file.
        /// </summary>
        /// <param name="projectData">Project data.</param>
        public static void CreateBuildConfiguration(IProjectData projectData)
        {
            m_currentProject = projectData;
            projectData.PreBuild();

            string projectFileName = projectData.ProjectFileName.Replace("/", "\\");
            string personalFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
            int lastSlash = projectFileName.LastIndexOf('\\') + 1;
            string projectName = projectFileName.Substring(lastSlash, projectFileName.Length - lastSlash);
            projectName = projectName.Substring(0, projectName.LastIndexOf('.'));

            //  Create the directory in case it doesn't exist
            Directory.CreateDirectory(personalFolder + "\\ragScriptEditor\\XGE\\");
            //  Create the new XML writer
            m_tempXMLFileName = personalFolder + "\\ragScriptEditor\\XGE\\" + projectName + ".xml";
            TempLogFileName = personalFolder + "\\ragScriptEditor\\XGE\\" + projectName + ".log";
            m_xmlWriter = new StreamWriter(m_tempXMLFileName, false);
            //  Write the build set
            m_xmlWriter.WriteLine("<BuildSet FormatVersion=\"" + m_xmlFormatVersion + "\">");
            //  Write the environments
            WriteEnvironmentsXML();
            //  Write the project
            WriteProjectXML(projectName, projectData);
            //  Write the build set end
            m_xmlWriter.WriteLine("</BuildSet>");
            //  Close the XML writer and file
            m_xmlWriter.Close();
        }
        #endregion // Public Static Methods

        #region Private Static Methods
        /// <summary>
        /// Build process output event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private static void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                if (ERRORS_REGEX.IsMatch(e.Data))
                {
                    BuildErrors?.Invoke(e.Data);
                }
                else
                {
                    BuildOutput?.Invoke(e.Data);
                }
            }
        }

        /// <summary>
        /// Build process output event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private static void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                BuildErrors?.Invoke(e.Data);
            }
        }

        /// <summary>
        /// Exited event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private static void OnIncredibuildProcess_Exited(object sender, EventArgs e)
        {
            lock (m_Lock)
            {
                Process senderProcess = (Process)sender;
                bool success = (senderProcess.ExitCode == 0);

                // If the build failed stop now
                if (!success)
                {
                    // Sleep to allow logs to flush...
                    Thread.Sleep(250);

                    // Terminate early before doing postbuild, and inform delegates.
                    NotifyUIIncredibuildCompletionDelegate?.Invoke(success);
                    NotifyUIIncredibuildStateChangeDelegate?.Invoke(false);

                    m_Running = false;

                    return;
                }

                // Otherwise the build was successful, and we need to do postbuild now.
                String postCompile = m_currentProject.Environment.Subst(m_currentProject.PostCompileCommand);

                // Start with cmd /c as it may be a batch script or a direct executable, and it may have args
                ProcessStartInfo psInfo = new ProcessStartInfo("cmd");
                psInfo.Arguments = String.Format("/C {0}", postCompile);

                psInfo.UseShellExecute = false;
                psInfo.CreateNoWindow = true;
                psInfo.RedirectStandardOutput = true;
                psInfo.RedirectStandardError = true;

                Process process = new Process();
                process.StartInfo = psInfo;
                process.EnableRaisingEvents = true;
                process.OutputDataReceived += new DataReceivedEventHandler(Process_OutputDataReceived);
                process.ErrorDataReceived += new DataReceivedEventHandler(Process_ErrorDataReceived);
                process.Exited += new EventHandler(OnPostbuildProcess_Exited);
                process.Start();
                process.BeginErrorReadLine();
                process.BeginOutputReadLine();
            }
        }

        private static void OnPostbuildProcess_Exited(object sender, EventArgs e)
        {
            lock (m_Lock)
            {
                Process senderProcess = (Process)sender;
                bool success = (senderProcess.ExitCode == 0);

                // Sleep to allow logs to flush...
                Thread.Sleep(250);

                NotifyUIIncredibuildCompletionDelegate?.Invoke(success);
                NotifyUIIncredibuildStateChangeDelegate?.Invoke(false);

                m_Running = false;
            }
        }

        /// <summary>
        /// Write the environment data.
        /// </summary>
        private static void WriteEnvironmentsXML()
        {
            //  Write the environments
            m_xmlWriter.WriteLine("\t<Environments>");
            //  Write the current environment
            WriteEnvironmentXML();
            //  Write the environments end
            m_xmlWriter.WriteLine("\t</Environments>");
        }

        /// <summary>
        /// Write a single piece of environment data.
        /// </summary>
        private static void WriteEnvironmentXML()
        {
            string commandLine = ""; // TODO: Once the sc.exe's command line option has been added "-noprefix ";

            if (m_currentProject.Custom.Length > 0)
            {
                commandLine += m_currentProject.Environment.Subst(m_currentProject.Custom);
            }
            //  Include file
            if (m_currentProject.XmlIncludeFile.Length > 0)
            {
                commandLine += " -include " + m_currentProject.Environment.Subst(m_currentProject.XmlIncludeFile);
            }
            //  Include directories
            if (m_currentProject.IncludePaths.Count() > 0)
            {
                commandLine += " -ipath ";
                commandLine += String.Join(";", m_currentProject.IncludePaths.Select(m_currentProject.Environment.Subst));
            }
            
            if (m_currentProject.WarningsAsErrors == true)
            {
                commandLine += " -werror";
            }

            // (Always use nopopups for XGE or else you get stalled processes!)
            commandLine += " -nopopups";

            string outputFileMasks = String.Join(",", _outputExtensions.Select(ext => $"*{ext}"));

            m_xmlWriter.WriteLine("\t\t<Environment Name=\"" + m_currentProject.ConfigurationName + "\">");
            //  Write the tools for this environment
            m_xmlWriter.WriteLine("\t\t\t<Tools>");
            //  Write the compiler tool
            m_xmlWriter.WriteLine("\t\t\t\t<Tool Name=\"Compiler\" AllowRemote=\"True\" GroupPrefix=\"Compiling...\" Params=\"&quot;$(SourcePath)&quot; " +
                                    commandLine + "\" Path=\"" +
                                    m_currentProject.Environment.Subst(m_currentProject.CompilerExecutable) +
                                    "\" OutputFileMasks=\"" + outputFileMasks + "\"/>");

            m_xmlWriter.WriteLine("\t\t\t</Tools>");
            m_xmlWriter.WriteLine("\t\t</Environment>");
        }

        /// <summary>
        /// Write the project details.
        /// </summary>
        /// <param name="projectName">Project name.</param>
        private static void WriteProjectXML(string projectName, IProjectData projectData)
        {
            //  Write the project
            m_xmlWriter.WriteLine("\t<Project Name=\"{0}\" Env=\"{1}\">", projectName, m_currentProject.ConfigurationName);
            //  Write the build task group
            m_xmlWriter.WriteLine("\t\t<TaskGroup Name=\"Build\" Env=\"{0}\">", m_currentProject.ConfigurationName);

            //  Write the compile tasks
            WriteCompileTasksXML(projectData);
            
            //  Write the build task group end
            m_xmlWriter.WriteLine("\t\t</TaskGroup>");
            //  Write the project end
            m_xmlWriter.WriteLine("\t</Project>");
        }

        /// <summary>
        /// Write the compile tasks XML.
        /// </summary>
        private static void WriteCompileTasksXML(IProjectData projectData)
        {
            //  Write the compile task group
            m_xmlWriter.WriteLine("\t\t\t<TaskGroup Name=\"CompileSourceFiles\" Tool=\"Compiler\">");
            //  Write a task for each file
            IList<String> projectFilenames = projectData.GetFilesToCompile();
            foreach (string file in projectFilenames)
            {
                if (0 == String.Compare(Path.GetExtension(file), ".sc", true))
                {
                    String outputWithoutExtension = Path.Combine(projectData.Environment.Subst(projectData.OutputDirectory), Path.GetFileNameWithoutExtension(file));

                    m_xmlWriter.WriteLine("\t\t\t\t<Task SourceFile=\"{0}\" Params=\"$(inherited:params) -output {1}\" InputFiles=\"{2}\" OutputFiles=\"{3}\"/>",
                        file,
                        Path.ChangeExtension(outputWithoutExtension, ".sco"),
                        String.Join(",", projectData.GetIncludesForFile(file)),
                        String.Join(",", _outputExtensions.Select(ext => Path.ChangeExtension(outputWithoutExtension, ext))));
                }
            }
            //  Write the compile task group end
            m_xmlWriter.WriteLine("\t\t\t</TaskGroup>");
        }

        /// <summary>
        /// Mark directory files as writable and delete them.
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="wildcard">Wildcard for what paths to clear out.</param>
        private static void CleanDirectory(String directory, String wildcard = "*.*")
        {
            try
            {
                String normalisedOutputDirectory = Path.GetFullPath(directory);
                if (!Directory.Exists(normalisedOutputDirectory))
                {
                    return;
                }

                BuildOutput?.Invoke($"Clearing {Path.Combine(directory, wildcard)}");

                // Clear all readonly files in directory
                DirectoryInfo di = new DirectoryInfo(normalisedOutputDirectory);
                di.Attributes &= ~FileAttributes.ReadOnly;
                foreach (FileSystemInfo info in di.GetFileSystemInfos(wildcard, SearchOption.AllDirectories))
                {
                    try
                    {
                        info.Attributes &= ~FileAttributes.ReadOnly;
                        info.Delete();
                    }
                    catch (Exception ex)
                    {
                        BuildOutput?.Invoke($"** WARNING: Deletion of {info.FullName} failed due to an exception: {ex.Message}");
                    }
                }
            }
            catch (Exception ex)
            {
                BuildOutput?.Invoke($"** WARNING: Cleaning of outputs in {directory} failed due to an exception: {ex.Message}");
            }
        }

        /// <summary>
        /// Build a list of expected output files.
        /// </summary>
        private static IEnumerable<String> GetExpectedOutputFiles(IProjectData project)
        {
            ICollection<String> expectedOutputFiles = new List<String>();
            foreach (String inputFile in m_currentProject.GetFilesToCompile())
            {
                String outputFile = Path.Combine(project.Environment.Subst(project.OutputDirectory), Path.GetFileName(inputFile));
                foreach (String ext in _outputExtensions)
                {
                    expectedOutputFiles.Add(Path.ChangeExtension(outputFile, ext));
                }
            }
            return expectedOutputFiles;
        }

        /// <summary>
        /// Clean any files in the output directory that are unexpected to be built.
        /// </summary>
        /// <param name="project"></param>
        private static void CleanStrayOutputFiles(IProjectData project)
        {
            IEnumerable<String> expectedOutputFiles = GetExpectedOutputFiles(project);

            if (!Directory.Exists(project.Environment.Subst(project.OutputDirectory)))
            {
                return;
            }

            foreach (String unexpectedFile in Directory.EnumerateFiles(project.Environment.Subst(project.OutputDirectory))
                .Except(expectedOutputFiles, StringComparer.OrdinalIgnoreCase))
            {
                CleanFile(unexpectedFile);
            }
        }

        /// <summary>
        /// Mark single file as writable and delete it.
        /// </summary>
        /// <param name="filename"></param>
        private static void CleanFile(String filename)
        {
            try
            {
                String normalizedFilename = Path.GetFullPath(filename);
                if (!File.Exists(normalizedFilename))
                {
                    return;
                }

                FileInfo fi = new FileInfo(normalizedFilename);
                fi.Attributes &= ~FileAttributes.ReadOnly;

                fi.Delete();
            }
            catch (Exception ex)
            {
                BuildOutput?.Invoke($"** WARNING: Cleaning of output {filename} failed due to an exception: {ex.Message}");
            }
        }
        #endregion // Private Static Methods
    }
}
