﻿using System;
using System.Collections.Generic;
using RSG.Base;

namespace RSG.Pipeline.BuildScript
{
    /// <summary>
    /// Project data interface. 
    /// </summary>
    public interface IProjectData
    {
        #region Properties

        /// <summary>
        /// Output directory. 
        /// </summary>
        string OutputDirectory { get; }

        /// <summary>
        /// Build directory.
        /// </summary>
        string BuildDirectory { get; }

        /// <summary>
        /// Project file name.
        /// </summary>
        string ProjectFileName { get; }

        /// <summary>
        /// Custom command line options.
        /// </summary>
        string Custom { get; }

        /// <summary>
        /// Include file.
        /// </summary>
        string XmlIncludeFile { get; }

        /// <summary>
        /// Include paths.
        /// </summary>
        List<string> IncludePaths { get; }

        /// <summary>
        /// Configuration name (e.g. Release, Debug)
        /// </summary>
        string ConfigurationName { get; }

        /// <summary>
        /// Compiler executable.
        /// </summary>
        string CompilerExecutable { get; }

        /// <summary>
        /// Resource executable.
        /// </summary>
        string ResourceExecutable { get; }

        /// <summary>
        /// 64-Bit Resource executable.
        /// </summary>
        string ResourceExecutable64 { get; }

        /// <summary>
        /// Post compile command.
        /// </summary>
        string PostCompileCommand { get; }

        /// <summary>
        /// Returns true if warnings should be treated as errors.
        /// </summary>
        bool WarningsAsErrors { get; }

        /// <summary>
        /// Returns true if the command is building the scripts
        /// </summary>
        bool CompileScripts { get; }

        /// <summary>
        /// Returns true if the IDE build monitor is to be used.
        /// </summary>
        bool UseIdeMonitor { get; }

        /// <summary>
        /// Returns true if the builder uses the PostBuild method.
        /// </summary>
        bool UsesPostBuild { get; }

        /// <summary>
        /// Returns a list of all enabled platforms.
        /// </summary>
        IList<RSG.Platform.Platform> EnabledPlatforms { get; }

        /// <summary>
        /// Returns the environment for substitution.
        /// </summary>
        IEnvironment Environment { get; }

        /// <summary>
        /// If true, don't pop up the XGE monitor.
        /// </summary>
        bool Nopopups { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the list of files that need to be compiled.
        /// </summary>
        /// <returns></returns>
        IList<String> GetFilesToCompile();

        /// <summary>
        /// Gets the full list of include files for a specified script, including global includes.
        /// </summary>
        /// <param name="scriptPath"></param>
        /// <returns></returns>
        IEnumerable<String> GetIncludesForFile(String scriptPath);

        /// <summary>
        /// Gets the list of files that need to be resourced for the specified platform.
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        IList<String> GetFilesToResource(RSG.Platform.Platform platform);

        /// <summary>
        /// Perform pre-build actions.
        /// </summary>
        void PreBuild();

        /// <summary>
        /// Perform post-build actions.
        /// </summary>
        /// <param name="exitCode">Exit code.</param>
        void PostBuild(int exitCode);

        /// <summary>
        /// Get the platform extension.
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        string GetPlatformExtension(RSG.Platform.Platform platform);

        #endregion
    }
}
