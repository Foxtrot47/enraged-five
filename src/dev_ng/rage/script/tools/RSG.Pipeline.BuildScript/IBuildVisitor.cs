﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.BuildScript
{
    /// <summary>
    /// Build visitor interface.
    /// </summary>
    public interface IBuildVisitor
    {
        /// <summary>
        /// Visit a file container.
        /// </summary>
        /// <param name="container">File container.</param>
        void Visit(IProjectData container);
    }
}
