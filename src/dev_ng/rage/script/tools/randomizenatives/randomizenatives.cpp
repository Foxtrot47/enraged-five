// Build Release|Tool configuration.

#include <stdlib.h>
#include <map>
#include <string>

#include "file/device.h"
#include "file/stream.h"
#include "script/program.h"

#include "system/xtl.h"

#pragma comment(lib,"bcrypt.lib")

using namespace rage;

// hack until we can really get rid of STLport on VNG
namespace stlpx_std {
	void __stl_throw_length_error(const char*) { }
}

u64 random64() {
	u64 buffer = 0;
	// make sure upper 32 bits aren't clear (extremely unlikely, but let's make sure)
	while ((unsigned)buffer == buffer)
		BCryptGenRandom(NULL, (PUCHAR)&buffer, sizeof(buffer), 0x2 /*BCRYPT_USE_SYSTEM_PREFERRED_RNG*/);
	return buffer;
}

// void* operator new(size_t size,size_t /*align*/) { return ::operator new(size); }
// void* operator new[](size_t size,size_t /*align*/) { return ::operator new[](size); }

std::map<std::string,u64> name_to_hash;

void add_native(const char *name,u64 hash) {
	// Displayf("Adding '%s' with hash \"0x%016I64x\".",name,hash);
	name_to_hash[name] = hash;
}

int main(int argc,char **argv)
{
	int returnCode = 0;
	int hashes_updated = 0, signatures_updated = 0;

	if (argc != 3) {
		Errorf("usage: %s file-of-sch-files-to-process file-of-cpp-files-to-process",argv[0]);
		return 1;
	}

	fiStream *sch = fiStream::Open(argv[1]);
	if (!sch) {
		Errorf("Unable to open SCH response file '%s'",argv[1]);
		return 1;
	}

	fiStream *cpp = fiStream::Open(argv[2]);
	if (!cpp) {
		Errorf("Unable to open CPP response file '%s'",argv[2]);
		return 1;
	}

	char filename[256];
	while (fgetline(filename,sizeof(filename),sch)) {
		fiStream *f = fiStream::Open(filename);
		if (!f) {
			Errorf("Unable to open input file %s",filename);
			returnCode = 1;
		}
		else {
			fiStream *o = fiStream::Create("random.tmp");
			if (!o) {
				Errorf("Unable to create temporary output file");
				returnCode = 1;
			}
			char linebuf[4096], parsetmp[4096];
			bool touched = false;
			while (fgets(linebuf,sizeof(linebuf),f)) {
				strcpy(parsetmp,linebuf);
				char *native = strtok(parsetmp,"\t ");
				char *funcOrProc = strtok(NULL,"\t ");
				if (funcOrProc && !strcmp(funcOrProc,"DEBUGONLY"))
					funcOrProc = strtok(NULL,"\t ");
				if (funcOrProc && !strcmp(funcOrProc,"FUNC"))
					strtok(NULL,"\t ");	// skip return type
				char *name = strtok(NULL,"\t (");
				if (native && !strcmp(native,"NATIVE") && funcOrProc && name && (!strcmp(funcOrProc,"FUNC") || !strcmp(funcOrProc,"PROC"))) {
					char *close = strrchr(linebuf,')');
					if (!close && !strchr(linebuf,'(') && strrchr(linebuf,'='))
						close = strrchr(linebuf,'=') - 1;
					while (!close && o->Write(linebuf,strlen(linebuf)) && fgets(linebuf,sizeof(linebuf),f))
						close = strrchr(linebuf,')');
					if (close) {
						++close;
						while (close[0]==' ' || close[0]=='\t')
							++close;
						if (close[0] == '=') {
							++close;
							while (close[0]==' ' || close[0]=='\t')
								++close;
							u64 hash = _strtoui64(close+1,NULL,16);		// it's quoted
							if (name_to_hash.find(name) != name_to_hash.end()) {
								if (name_to_hash[name] != hash) {
									Errorf("NATIVE '%s' was previously registered elsewhere under a different value.",name);
									returnCode = 1;
								}
							}
							else
								add_native(name,hash);
						}
						else {
							// If the same NATIVE was previously found, make sure we generate the same number.
							u64 hash = name_to_hash.find(name) != name_to_hash.end()? name_to_hash[name] : random64();
							sprintf(close," = \"0x%016I64x\"\r\n",hash);
							add_native(name,hash);
							touched = true;
							hashes_updated++;
						}
					}
				}
				o->Write(linebuf,strlen(linebuf));
			}
			f->Close();
			o->Close();
			if (touched) {
				sprintf(linebuf,"p4 edit %s",filename);
				system(linebuf);
				sprintf(linebuf,"move random.tmp %s",filename);
				system(linebuf);
			}
		}
	}
	sch->Close();

	while (fgetline(filename,sizeof(filename),cpp)) {
		fiStream *f = fiStream::Open(filename);
		if (!f) {
			Errorf("Unable to open input file %s",filename);
			returnCode = 1;
		}
		else {
			fiStream *o = fiStream::Create("random.tmp");
			if (!o) {
				Errorf("Unable to create temporary output file");
				returnCode = 1;
			}
			char linebuf[4096];
			bool touched = false;
			while (fgets(linebuf,sizeof(linebuf),f)) {
				char *tag = strstr(linebuf,"SCR_REGISTER_");
				if (tag) {

					char *exclusion = strstr(linebuf,"EXCLUDE_FROM_RANDOMIZATION");
					// Only continue if we do NOT find the exclusion string
					if(exclusion == NULL) {

					tag = strchr(tag,'(');
					if (tag) {
						++tag;
						while (tag[0]==' ' || tag[0] == '\t')
							++tag;
						char *name = tag;
						while (tag[0] != ' ' && tag[0] != '\t' && tag[0] != ',' && tag[0])
							++tag;
						char old = *tag;
						*tag = '\0';
						if (name_to_hash.find(name) != name_to_hash.end()) {
							u64 hash = name_to_hash[name];
							*tag = old;
							while (tag[0] != ',' && tag[0])
								++tag;
							if (tag[0]) {
								++tag;
								char *start = tag;
								while (tag[0] != ',' && tag[0])
									++tag;
								u64 oldhash = _strtoui64(start,NULL,16);
								if (oldhash != hash) {
									char newbuf[19];
									sprintf(newbuf,"0x%016I64x",hash);
									memmove(start+18,tag,strlen(tag)+1);
									memcpy(start,newbuf,18);
									// Displayf("Changing old hash code %I64x to new code %I64x",oldhash,hash);
									signatures_updated++;
									touched = true;
								}
							}
						}
						else {
							Warningf("Unknown NATIVE '%s' registered by C++ code?",name);
							*tag = old;
						}
					}

					}

				}
				else if ((tag = strstr(linebuf,"SCRHASH(")) != NULL) {
					char *start = tag+9;
					if (start) {
						char *end = strchr(start,'"');
						if (end && end[1]==',') {
							u64 oldhash = _strtoui64(end+2,NULL,16);
							*end = '\0';
							char *name = start;
							if (name_to_hash.find(name) !=  name_to_hash.end()) {
								u64 hash = name_to_hash[name];
								if (oldhash != hash && end[20]==')') {
									char newbuf[19];
									sprintf(newbuf,"0x%016I64x",hash);
									memcpy(end+2,newbuf,18);
									touched = true;
								}
							}
							*end='"';
						}
					}
				}
				o->Write(linebuf,strlen(linebuf));
			}
			f->Close();
			o->Close();
			if (touched) {
				sprintf(linebuf,"p4 edit %s",filename);
				system(linebuf);
				sprintf(linebuf,"move random.tmp %s",filename);
				system(linebuf);
			}
		}
	}
	cpp->Close();

	Displayf("Updated %d SCH NATIVE definitions and %d SCR_REGISTER_... calls.",hashes_updated,signatures_updated);
	return returnCode;
}
