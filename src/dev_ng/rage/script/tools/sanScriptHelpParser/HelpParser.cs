using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Windows.Forms;

using ActiproSoftware.SyntaxEditor;
using ragScriptEditorShared;
using SanScriptParser;

namespace sanScriptHelpParser
{
    /// <summary>
    /// Summary description for HelpParser.
    /// </summary>
    class HelpParser : ISemanticParseDataTarget
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main( string[] args )
        {
            if ( args.Length < 2 )
            {
                Console.WriteLine( "Please specify input and output file names." );
                return;
            }

            ApplicationSettings.ExecutableFullPathName = Application.ExecutablePath;
            ApplicationSettings.ProductName = Application.ProductName;
            ApplicationSettings.Mode = ApplicationSettings.ApplicationMode.HelpParser;

            HelpParser parser = new HelpParser( args[0] );

            // Don't start the service so processing occurs in the main thread.
            //SemanticParserService.Start();

            parser.ParseFile( args[0], args[1], args.Length >= 3 ? args[2] : null );

            while ( !parser.Done )
            {
                // do nothing
            }

            // Don't stop the service so processing occurs in the main thread.
            //SemanticParserService.Stop();
        }

        HelpParser( string filename )
        {
            m_parserPlugin.InitPlugin( ApplicationSettings.ExecutablePath );

            LoadLayout();

            m_projectResolver = m_parserPlugin.CreateProjectResolver( filename );
        }

        #region Variables
        private UserEditorSettings m_userEditorSettings = new UserEditorSettings();
        private IParserPlugin m_parserPlugin = new SanScriptParserPlugin();
        private IParserProjectResolver m_projectResolver;

        private string m_inputFile = null;
        private string m_outputFile = null;
        private string m_masterGroup = null;
        
        private Guid m_guid = Guid.NewGuid();
        private bool m_done = false;
        #endregion

        #region Properties
        public bool Done
        {
            get
            {
                return m_done;
            }
        }
        #endregion

        #region ISemanticParseDataTarget interface
        string ISemanticParseDataTarget.Guid
        {
            get
            {
                return m_guid.ToString();
            }
        }

        void ISemanticParseDataTarget.NotifySemanticParseComplete( SemanticParserServiceRequest request )
        {
            if ( request.SemanticParseData != null )
            {
                SanScriptProjectResolver.AddCompilationUnitToCache( request.SemanticParseData as CompilationUnit, this );
                OuputHelpFile();
                m_done = true;
            }
        }
        #endregion

        #region Public Functions
        public void ParseFile( string inputFile, string outputFile, string masterGroup )
        {
            m_inputFile = Path.GetFullPath( inputFile );
            m_outputFile = Path.GetFullPath( outputFile );
            m_masterGroup = masterGroup;

            if ( SanScriptProjectResolver.AddFileToCache( m_inputFile, this, true ) )
            {
                OuputHelpFile();
                m_done = true;
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Loads the layout and retrieves the SandDock layout string (which we are going to ignore)
        /// </summary>
        /// <returns>null on error</returns>
        public bool LoadLayout()
        {
            string filename = ApplicationSettings.GetCommonSettingsFilename( false );
            if ( filename == null )
            {
                return false;
            }

            // see if it's the old layout file
            XmlTextReader reader = null;
            float version = 0.0f;
            try
            {
                reader = new XmlTextReader( filename );

                while ( reader.Read() )
                {
                    if ( reader.Name == "ScriptEditor" )
                    {
                        if ( !reader.IsStartElement() )
                        {
                            break; // we're done
                        }

                        reader.Close();

                        return LoadOldLayout( filename );
                    }
                    else if ( reader.Name == "Version" )
                    {
                        if ( reader.IsStartElement() )
                        {
                            version = float.Parse( reader.ReadString() );
                        }

                        break;
                    }
                }

                reader.Close();
            }
            catch ( Exception e )
            {
                if ( reader != null )
                {
                    reader.Close();
                }

                ApplicationSettings.ShowMessage( null, String.Format( "Could not parse old layout file\n{0}", e.ToString() ), 
                    "Problem Parsing Layout File!", MessageBoxButtons.OK, MessageBoxIcon.Error, DialogResult.OK );

                Application.Exit();
                return false;
            }

            if ( version == UserEditorSettings.SettingsVersion )
            {
                // if we got here, we have the latest version of the settings file
                string error = null;
                if ( !m_userEditorSettings.LoadFile( filename, out error ) )
                {
                    ApplicationSettings.ShowMessage( null, error, 
                        String.Format( "Error Loading Settings File Version {0}!", UserEditorSettings.SettingsVersion ),
                        MessageBoxButtons.OK, MessageBoxIcon.Error, DialogResult.OK );

                    Application.Exit();
                    return false;
                }
            }
            else if ( version == UserEditorSettingsVer2_1.SettingsVersion )
            {
                UserEditorSettingsVer2_1 userSettings = new UserEditorSettingsVer2_1();

                string error = null;
                if ( !userSettings.LoadFile( filename, out error ) )
                {
                    ApplicationSettings.ShowMessage( null, error,
                        String.Format( "Error Loading Settings File Version {0}!", UserEditorSettingsVer2_1.SettingsVersion ),
                        MessageBoxButtons.OK, MessageBoxIcon.Error, DialogResult.OK );

                    Application.Exit();
                    return false;
                }

                m_userEditorSettings.Upgrade( userSettings );
            }
            else
            {
                ApplicationSettings.ShowMessage( null, String.Format( "Unable to load a settings file version {0}.", version ),
                    "Unknown Settings File Version",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning, DialogResult.OK );
            }

            foreach ( LanguageSettings l in m_userEditorSettings.LanguageSettingsList )
            {
                if ( l.Language == m_parserPlugin.LanguageName )
                {
                    m_parserPlugin.CurrentLanguageSettings.Copy( l );
                    m_parserPlugin.OnLanguageSettingsUpdated();
                    break;
                }
            }

            m_parserPlugin.IncludePaths = m_userEditorSettings.CurrentCompilingSettings.IncludePaths;
            m_parserPlugin.IncludeFile = m_userEditorSettings.CurrentCompilingSettings.XmlIncludeFile;

            return true;
        }

        private bool LoadOldLayout( string filename )
        {
            // parse the layout file:
            XmlTextReader reader = null;
            bool foundVersion = false;

            try
            {
                reader = new XmlTextReader( filename );

                while ( reader.Read() )
                {
                    if ( !reader.IsStartElement() )
                    {
                        if ( reader.Name == "ScriptEditor" )
                        {
                            break; // we're done
                        }
                    }
                    else if ( reader.Name == "Version" )
                    {
                        reader.Read();
                        string versionString = reader.Value;

                        // make sure versions match:
                        if ( (versionString == null) || (versionString != ApplicationSettings.OldLayoutVersionString) )
                        {
                            Console.WriteLine( "application version and save file versions don't match." );
                            reader.Close();
                            return false;
                        }

                        foundVersion = true;
                    }
                    else if ( reader.Name == m_userEditorSettings.IntellisenseSettings.XmlSectionName )
                    {
                        if ( !foundVersion )
                        {
                            reader.Close();
                            return false;
                        }

                        if ( m_userEditorSettings.IntellisenseSettings.LoadXml( reader ) )
                        {
                        }
                    }
                    else if ( (reader.Name == "UserEditorSettings") || (reader.Name == "EditorSettings") )
                    {
                        if ( !foundVersion )
                        {
                            reader.Close();
                            return false;
                        }

                        UserEditorSettingsVer2_1 userSettings = new UserEditorSettingsVer2_1();

                        if ( !userSettings.LoadXml( reader ) )
                        {
                            reader.Close();
                            return false;
                        }

                        m_userEditorSettings.Upgrade( userSettings );
                    }
                }

                reader.Close();
            }
            catch ( Exception e )
            {
                if ( reader != null )
                {
                    reader.Close();
                }

                Console.WriteLine( "ERROR: Could not parse layout file\n" + e.ToString() );
                return false;
            }

            return true;
        }

        private bool OuputHelpFile()
        {
            StreamWriter writer = null;
            try
            {
                writer = File.CreateText( m_outputFile );
            }
            catch ( Exception e )
            {
                Console.WriteLine( "Couldn't open file '" + m_outputFile + "' because: '" + e + "'" );
                return false;
            }

            string groupName = Path.GetFileNameWithoutExtension( m_outputFile );

            // add the master groups if passed in:
            writer.WriteLine( "//@@" + groupName );
            if ( (m_masterGroup != null) && (m_masterGroup != string.Empty) )
            {
                writer.WriteLine( "//<GROUP " + m_masterGroup + ">" );
            }

            writer.WriteLine( "" );

            writer.WriteLine( "//@@" + groupName + "_" + SanScriptParser.SanScriptConstant.sm_HelpTypeGroupName );
            writer.WriteLine( "//<TITLE Constants And Enums>" );
            writer.WriteLine( "//<GROUP " + groupName + ">" );

            writer.WriteLine( "//@@" + groupName + "_" + SanScriptParser.SanScriptStaticVariable.sm_HelpTypeGroupName );
            writer.WriteLine( "//<TITLE Global Variables>" );
            writer.WriteLine( "//<GROUP " + groupName + ">" );

            writer.WriteLine( "//@@" + groupName + "_" + SanScriptParser.SanScriptSubroutine.sm_HelpTypeGroupName );
            writer.WriteLine( "//<TITLE Functions And Procedures>" );
            writer.WriteLine( "//<GROUP " + groupName + ">" );
            
            writer.WriteLine( "//@@" + groupName + "_" + SanScriptParser.SanScriptStructure.sm_HelpTypeGroupName );
            writer.WriteLine( "//<TITLE Structures>" );
            writer.WriteLine( "//<GROUP " + groupName + ">" );

            writer.WriteLine( "//@@" + groupName + "_" + SanScriptParser.SanScriptState.sm_HelpTypeGroupName );
            writer.WriteLine( "//<TITLE States>" );
            writer.WriteLine( "//<GROUP " + groupName + ">" );

            writer.WriteLine( "//@DOC_END\n" );

            List<SanScriptIdentifier> identifiers = SanScriptProjectResolver.IdentifierCache.GetIdentifiers( m_inputFile, (SanScriptIdentifierType)0xffff );

            // sort the identifiers
            SortedDictionary<string, SanScriptIdentifier> sortedIdentifiers = new SortedDictionary<string, SanScriptIdentifier>();
            foreach ( SanScriptIdentifier id in identifiers )
            {
                sortedIdentifiers.Add( id.Name.ToUpper(), id );
            }

            // write them to the writer
            foreach ( KeyValuePair<string, SanScriptIdentifier> pair in sortedIdentifiers )
            {
                pair.Value.WriteHelpText( groupName, writer );
            }

            writer.Close();
            return true;
        }
        #endregion
    }
}
