// SanScriptIsolatedShell.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "stdio.h"
#include "SanScriptIsolatedShell.h"

#define MAX_LOADSTRING 100

typedef int (__cdecl  *StartWithCommandLineFCN)(LPWSTR, LPWSTR, int, GUID *, WCHAR *pszSettings);

void ShowStartWarning(LPWSTR startingFile)
{
	MessageBoxW(NULL, startingFile, L"Starting", MB_OK|MB_ICONWARNING);
}

void ShowFinishedWarning(int returnValue)
{
	char * dest = new char[256];
	sprintf_s(dest, 256, "%d", returnValue);

	MessageBoxA(NULL, dest, "Finishing", MB_OK|MB_ICONWARNING);
}

void ShowNoComponentError(HINSTANCE hInstance, int callNumber, LPCWSTR errorStr)
{
	switch (callNumber)
	{
		case 1:
		{
			MessageBoxW(NULL, errorStr, L"Cannot find file!", MB_OK|MB_ICONERROR);
		}
		break;
		case 2:
		{
			MessageBoxW(NULL, errorStr, L"Cannot load dll!", MB_OK|MB_ICONERROR);
		}
		break;
		case 3:
		{
			MessageBoxW(NULL, errorStr, L"Cannot find entry point!", MB_OK|MB_ICONERROR);
		}
		break;
	}

}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	int nRetVal = -1;
	WCHAR szExeFilePath[MAX_PATH];
	HKEY hKeyAppEnv10Hive = NULL;

	if(RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"Software\\Microsoft\\AppEnv\\10.0", 0, KEY_READ, &hKeyAppEnv10Hive) == ERROR_SUCCESS)
	{
		DWORD dwType;
		DWORD dwSize = MAX_PATH;
		RegQueryValueExW(hKeyAppEnv10Hive, L"AppenvStubDLLInstallPath", NULL, &dwType, (LPBYTE)szExeFilePath, &dwSize);
		RegCloseKey(hKeyAppEnv10Hive);
	}

	if(GetFileAttributesW(szExeFilePath) == INVALID_FILE_ATTRIBUTES)
	{
		//If we cannot find it at a registered location, then try in the same directory as the application
		GetModuleFileNameW(NULL, szExeFilePath, MAX_PATH);
		WCHAR *pszStartOfFileName = wcsrchr(szExeFilePath, '\\');
		if(!pszStartOfFileName)
		{
			return -1;
		}
		*pszStartOfFileName = 0;
		wcscat_s(szExeFilePath, MAX_PATH, L"\\appenvstub.dll");

		if(GetFileAttributesW(szExeFilePath) == INVALID_FILE_ATTRIBUTES)
		{
			//If the file cannot be found in the same directory as the calling exe, then error out.
			ShowNoComponentError(hInstance, 1, szExeFilePath);
			return -1;
		}
	}

	//Get full Unicode command line to pass to StartWithCommandLine function.
	LPWSTR lpwCmdLine = GetCommandLineW();

	//ShowStartWarning(szExeFilePath);

	HMODULE hModStubDLL = LoadLibraryW(szExeFilePath);
	if(!hModStubDLL)
	{
		ShowNoComponentError(hInstance, 2, szExeFilePath);
		return -1;
	}

	//ShowStartWarning(lpwCmdLine);

	StartWithCommandLineFCN StartWithCommandLine = (StartWithCommandLineFCN)GetProcAddress(hModStubDLL, "Start");
	if(!StartWithCommandLine)
	{
		ShowNoComponentError(hInstance, 3, L"Start");
		return -1;
	}

	nRetVal = StartWithCommandLine(lpwCmdLine, L"SanScriptIsolatedShell", nCmdShow, NULL, NULL);

	//ShowFinishedWarning(nRetVal);

	FreeLibrary(hModStubDLL);

	return nRetVal;
}
