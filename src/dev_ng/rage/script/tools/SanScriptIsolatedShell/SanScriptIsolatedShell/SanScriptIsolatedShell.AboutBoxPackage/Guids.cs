﻿// Guids.cs
// MUST match guids.h
using System;

namespace SanScriptIsolatedShell.AboutBoxPackage
{
    static class GuidList
    {
        public const string guidAboutBoxPackagePkgString = "0a8391f0-c2c9-4504-b443-dea74139477b";
        public const string guidAboutBoxPackageCmdSetString = "68a6ce11-00ca-450c-a2d6-4fa30b3f875d";

        public static readonly Guid guidAboutBoxPackageCmdSet = new Guid(guidAboutBoxPackageCmdSetString);
    };
}