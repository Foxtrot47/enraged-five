﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Text;
using System.Xml;

namespace SanScriptProjectConvert
{
    public enum eCONVERT_ERROR
    {
        ERROR_NO_ARGS, 
        ERROR_INVALID_INPUT, 
        ERROR_UNAUTHORISED_ACCESS,
        ERROR_BRANCH_NOTFOUND
    };

    class Program
    {
        private static XmlTextReader m_xmlReader;
        private static StreamWriter m_xmlWriter;
        private static List<Item> m_items;
        private static List<string> m_folders;
        private static List<Config> m_configs;
        private static string m_projectName;
        private static string m_branch;

        static void Main(string[] args)
        {
            //  Clear the screen
            ClearScreen();
            //  Banner info
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("**  Convert old '.scproj' SanScript project to new '.ssproj' Visual Studio   **");
            Console.WriteLine("**  SanScript project.                                                       **");
            Console.WriteLine("*******************************************************************************\n");
            //  Make sure we have an argument to work with
            if (args.Length == 0)
            {
                Error(eCONVERT_ERROR.ERROR_NO_ARGS);
            }
            //  Check the extension for a valid one
            if (!args[0].EndsWith(".scproj"))
            {
                Error(eCONVERT_ERROR.ERROR_INVALID_INPUT);
            }
            //  Get the project name, first by stripping any path
            m_projectName = args[0];
            if (m_projectName.LastIndexOf('\\') >= 0)
            {
                m_projectName = m_projectName.Substring(m_projectName.LastIndexOf('\\') + 1);
            }
            else if (m_projectName.LastIndexOf('/') >= 0)
            {
                m_projectName = m_projectName.Substring(m_projectName.LastIndexOf('/') + 1);
            }
            //  Then remove the extension
            m_projectName = m_projectName.Substring(0, m_projectName.LastIndexOf('.'));
            m_branch = GetBranch(args[0]);

            //  Update status
            Console.WriteLine("***** Reading original project file.");
            //  Read the items from the original project file
            m_xmlReader = new XmlTextReader(args[0]);
            ReadItems();
            //  Close the file
            m_xmlReader.Close();
            //  Update status
            Console.WriteLine("***** Read {0} files and {1} folders.\n", m_items.Count, m_folders.Count);
            Console.WriteLine("***** Writing out new Visual Studio project file.");
            //  Get the filename for the new project
            string newProject = args[0].Substring(0, args[0].LastIndexOf('.')) + ".ssproj";
            //  Write out the new project file
            try
            {
                m_xmlWriter = new StreamWriter(newProject);
                WriteProject();
                //  Close the file
                m_xmlWriter.Close();
                //  Update status
                Console.WriteLine("***** Project '{0}' created.\n", newProject);

                //  Show conversion complete message
                Console.WriteLine("*******************************************************************************");
                Console.WriteLine("**  Conversion completed successfully.                                       **");
                Console.WriteLine("*******************************************************************************\n");
            }
            catch (System.UnauthorizedAccessException ex)
            {
                Error(eCONVERT_ERROR.ERROR_UNAUTHORISED_ACCESS);            	
            }

            //  Pause before quitting
            Pause();
            //  Exit the app normally
            Environment.Exit(0);
        }

        /// <summary>
        /// Try and determine the branch that the project file is located.
        /// </summary>
        /// <param name="scprojFile">Project file.</param>
        /// <returns>The branch. E.g. dev, release.</returns>
        private static string GetBranch(string scprojFile)
        {
            string root = Environment.GetEnvironmentVariable("RS_PROJROOT");
            if(String.IsNullOrEmpty(root))
            {
                Error(eCONVERT_ERROR.ERROR_BRANCH_NOTFOUND);
                return "";
            }

            string scriptRoot = Path.Combine(root, "script");
            string relPath = scprojFile.Substring(scriptRoot.Length + 1); // +1 for the '\' character
            int slashPos = relPath.IndexOf("\\");
            if(slashPos>=0)
            {
                return relPath.Substring(0, slashPos);
            }


            Error(eCONVERT_ERROR.ERROR_BRANCH_NOTFOUND);
            return "";
        }

        /// <summary>
        /// Writes out the new format project
        /// </summary>
        private static void WriteProject()
        {
            //  Write the basic project top of the file
            WriteBasicProjectTop();
            //  Write the files first
            WriteFiles();
            //  Write the folders
            WriteFolders();
            //  Write the basic project bottom of the file
            WriteBasicProjectBottom();
        }

        /// <summary>
        /// Writes the folders to the new project
        /// </summary>
        private static void WriteFolders()
        {
            m_xmlWriter.WriteLine("\t<ItemGroup>");
            foreach (string s in m_folders)
            {
                m_xmlWriter.WriteLine("\t\t<Folder Include=\"{0}\" />", s);
            }
            m_xmlWriter.WriteLine("\t</ItemGroup>");
        }

        /// <summary>
        /// Writes the files to the new project
        /// </summary>
        private static void WriteFiles()
        {
            m_xmlWriter.WriteLine("\t<ItemGroup>");
            foreach (Item i in m_items)
            {
                m_xmlWriter.WriteLine("\t\t<Compile Include=\"{0}\">", i.GetFilename());
                m_xmlWriter.WriteLine("\t\t\t<Folder>{0}</Folder>", i.GetProjectFolder());
                m_xmlWriter.WriteLine("\t\t</Compile>");
            }
            m_xmlWriter.WriteLine("\t</ItemGroup>");
        }

        /// <summary>
        /// Write the basic project top of the file
        /// </summary>
        private static void WriteBasicProjectTop()
        {
            m_xmlWriter.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            m_xmlWriter.WriteLine("<Project DefaultTargets=\"Build\" xmlns=\"http://schemas.microsoft.com/developer/msbuild/2003\">");
            m_xmlWriter.WriteLine("\t<PropertyGroup>");
            m_xmlWriter.WriteLine("\t\t<Configuration Condition=\" '$(Configuration)' == '' \">" + m_configs[0].GetName() + "</Configuration>");
            m_xmlWriter.WriteLine("\t\t<SchemaVersion>2.0</SchemaVersion>");
            m_xmlWriter.WriteLine("\t\t<ProjectGuid>{0}</ProjectGuid>", "{" + Guid.NewGuid().ToString() + "}");
            m_xmlWriter.WriteLine("\t\t<OutputType>Exe</OutputType>");
            m_xmlWriter.WriteLine("\t\t<EnableUnmanagedDebugging>false</EnableUnmanagedDebugging>");
            m_xmlWriter.WriteLine("\t\t<AssemblyName>{0}</AssemblyName>", m_projectName);
            m_xmlWriter.WriteLine("\t\t<Name>{0}</Name>", m_projectName);
            m_xmlWriter.WriteLine("\t\t<RootNamespace>{0}</RootNamespace>", m_projectName);
            m_xmlWriter.WriteLine("\t</PropertyGroup>");
            m_xmlWriter.WriteLine("\t<PropertyGroup>");
            m_xmlWriter.WriteLine("\t\t<!-- Set the SacScriptPath property by checking where it could be installed -->");
            m_xmlWriter.WriteLine("\t\t<!-- Experimental LocalAppData -->");
            m_xmlWriter.WriteLine("\t\t<SanScriptPath Condition=\" '$(SanScriptPath)' == '' AND Exists('$(LocalAppData)\\Microsoft\\VisualStudio\\10.0Exp\\Extensions\\Rockstar\\SanScript Project\\1.0\\SanScriptProject.targets')\">$(LocalAppData)\\Microsoft\\VisualStudio\\10.0Exp\\Extensions\\Rockstar\\SanScript Project\\1.0</SanScriptPath>");
            m_xmlWriter.WriteLine("\t\t<!-- Regular Install Extensions Folder -->");
            m_xmlWriter.WriteLine("\t\t<SanScriptPath Condition=\" '$(SanScriptPath)' == '' AND Exists('C:\\Program Files (x86)\\Microsoft Visual Studio 10.0\\Common7\\IDE\\CommonExtensions\\Rockstar\\SanScript Project\\1.0\\SanScriptProject.targets')\">C:\\Program Files (x86)\\Microsoft Visual Studio 10.0\\Common7\\IDE\\CommonExtensions\\Rockstar\\SanScript Project\\1.0</SanScriptPath>");
            m_xmlWriter.WriteLine("\t\t<!-- Compiler exes -->");

            //TODO:  Project-generic paths.
            m_xmlWriter.WriteLine("\t\t<CompilerExe>" + m_configs[0].GetCompilerExe() + "</CompilerExe>");
            m_xmlWriter.WriteLine("\t\t<ResourceExe>" + m_configs[0].GetResourceExe() + "</ResourceExe>");
            m_xmlWriter.WriteLine("\t\t<ResourceExe64>" + m_configs[0].GetResourceExe64() + "</ResourceExe64>");
            
            //The names of these platforms are to be the same as the RSG.Platform.Platform's Ragebuilder platform strings.
            m_xmlWriter.WriteLine("\t\t<PS3ResourceExtension>.csc</PS3ResourceExtension>");
            m_xmlWriter.WriteLine("\t\t<Win32ResourceExtension>.wsc</Win32ResourceExtension>");
            m_xmlWriter.WriteLine("\t\t<Win64ResourceExtension>.ysc</Win64ResourceExtension>");
            m_xmlWriter.WriteLine("\t\t<Xbox360ResourceExtension>.xsc</Xbox360ResourceExtension>");
            m_xmlWriter.WriteLine("\t\t<RagebuilderTempPath>$(RS_TOOLSROOT)\\tmp\\script_xge\\$(RS_PROJECT)\\dev\\convert</RagebuilderTempPath>");
            m_xmlWriter.WriteLine("\t\t<RagebuilderScriptExtension>.rbs</RagebuilderScriptExtension>");
            m_xmlWriter.WriteLine("\t</PropertyGroup>");
            //  Write the configs
            for (int i = 0; i < m_configs.Count; i++)
            {
                m_configs[i].WriteToXml(m_xmlWriter);
            }
        }

        /// <summary>
        /// Write the basic project bottom of the file
        /// </summary>
        private static void WriteBasicProjectBottom()
        {
            m_xmlWriter.WriteLine("<Import Project=\"$(SanScriptPath)\\SanScriptProject.targets\" />");
            m_xmlWriter.WriteLine("</Project>");
        }

        /// <summary>
        /// Reads the items from the original project file
        /// </summary>
        private static void ReadItems()
        {
            if (m_xmlReader != null)
            {
                //  Create the empty lists of items and folders and configs
                m_items = new List<Item>();
                m_folders = new List<string>();
                m_configs = new List<Config>();

                //  Read through the whole file
                while (m_xmlReader.Read())
                {
                    //  Move to the first element
                    m_xmlReader.MoveToElement();
                    //  Only interested in ProjectExplorerItems and CompilingSettings
                    if (IsProjectItem())
                    {
                        string type = m_xmlReader.GetAttribute("xsi:type");
                        switch (type)
                        {
                            case "ProjectExplorerFolder":
                            {
                                ReadFolder("");
                            }
                            break;
                            case "ProjectExplorerFile":
                            {
                                ReadFile("");
                            }
                            break;
                        }
                    }
                    else if (IsConfigItem())
                    {
                        Config config = new Config();
                        config.Branch = m_branch;
                        config.OverridePostCompileCommand = @"$(RS_TOOLSROOT)\bin\RageScriptEditor\PostCompile.bat $(Configuration)";
                        config.ReadFromXml(m_xmlReader);
                        //  Add the new config to the list
                        m_configs.Add(config);
                    }
                }
            }
        }

        /// <summary>
        /// Reads in a file item
        /// </summary>
        /// <param name="currentFolder">The current folder this file is inside</param>
        private static void ReadFile(string currentFolder)
        {
            //  Read the elements
            while (m_xmlReader.Read())
            {
                //  Move to the first element
                m_xmlReader.MoveToElement();
                if (m_xmlReader.NodeType == XmlNodeType.Element &&
                    m_xmlReader.Name == "Name")
                {
                    //  Read in the text
                    m_xmlReader.Read();
                    if (m_xmlReader.NodeType == XmlNodeType.Text)
                    {
                        //  Add the file to the list of files

                        string file = m_xmlReader.Value;
                        if (file.Contains("build\\" + m_branch))
                        {
                            // SPECIAL CASE: THIS IS FOR THE NETWORKOPTIONS.XML. It's stored in the BUILD branch
                            int buildDevPos = file.IndexOf("build\\" + m_branch, StringComparison.OrdinalIgnoreCase);
                            file = file.Substring(buildDevPos + ("build\\" + m_branch).Length);
                            file = "$(RS_BUILDBRANCH)" + file;
                        }

                        m_items.Add(new Item(file, currentFolder));
                    }
                    //  Read the end element
                    m_xmlReader.Read();
                }
                else if (m_xmlReader.NodeType == XmlNodeType.EndElement &&
                        m_xmlReader.Name == "ProjectExplorerItem")
                {
                    //  The end of this file item
                    return;
                }
            }
        }

        /// <summary>
        /// Reads in a folder and folder items inside it
        /// </summary>
        /// <param name="currentFolder">The current folder this folder is inside</param>
        private static void ReadFolder(string currentFolder)
        {
            string folderName = currentFolder;
            //  Read the elements
            while (m_xmlReader.Read())
            {
                //  Move to the first element
                m_xmlReader.MoveToElement();
                if (m_xmlReader.NodeType == XmlNodeType.Element &&
                    m_xmlReader.Name == "Name")
                {
                    //  Read in the text
                    m_xmlReader.Read();
                    if (m_xmlReader.NodeType == XmlNodeType.Text)
                    {
                        if (folderName == "")
                        {
                            folderName += m_xmlReader.Value;
                        }
                        else
                        {
                            folderName += "\\" + m_xmlReader.Value;
                        }
                        //  Add the folder to the list
                        m_folders.Add(folderName);
                    }
                    //  Read the end element
                    m_xmlReader.Read();
                }
                else if (m_xmlReader.NodeType == XmlNodeType.EndElement &&
                        m_xmlReader.Name == "ProjectExplorerItem")
                {
                    //  The end of this folder item
                    return;
                }
                else if (IsProjectItem())
                {
                    string type = m_xmlReader.GetAttribute("xsi:type");
                    switch (type)
                    {
                        case "ProjectExplorerFolder":
                        {
                            ReadFolder(folderName);
                        }
                        break;
                        case "ProjectExplorerFile":
                        {
                            ReadFile(folderName);
                        }
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// This checks to see if the current node is a project item
        /// </summary>
        /// <returns>true if a project item, otherwise false</returns>
        private static bool IsProjectItem()
        {
            return (m_xmlReader.NodeType == XmlNodeType.Element && 
                    m_xmlReader.Name == "ProjectExplorerItem");
        }

        /// <summary>
        /// This checks to see if the current node is a config item
        /// </summary>
        /// <returns>true if a config item, otherwise false</returns>
        private static bool IsConfigItem()
        {
            return (m_xmlReader.NodeType == XmlNodeType.Element && 
                    m_xmlReader.Name == "CompilingSettings");
        }

        /// <summary>
        /// Clears the cmd screen
        /// </summary>
        private static void ClearScreen()
        {
            Console.Clear();
        }

        /// <summary>
        /// Prints an error to cmd window and exits after a pause
        /// </summary>
        /// <param name="error">The error to report</param>
        private static void Error(eCONVERT_ERROR error)
        {
            switch (error)
            {
                case eCONVERT_ERROR.ERROR_NO_ARGS:
                {
                    Console.WriteLine("Error: You must supply a '.scproj' file to convert.");
                    Console.WriteLine("Usage: SanScriptProjectConvert.exe oldProject.scproj\n");
                }
                break;
                case eCONVERT_ERROR.ERROR_INVALID_INPUT:
                {
                    Console.WriteLine("Error: Invalid input file type, please specify a '.scproj' file.\n");
                }
                break;
                case eCONVERT_ERROR.ERROR_UNAUTHORISED_ACCESS:
                {
                    Console.WriteLine("Error: Unauthorised access when writing '.ssproj' output file.");
                    Console.WriteLine("Do you have the file checked out in Perforce?\n");
                }
                break;
                case eCONVERT_ERROR.ERROR_BRANCH_NOTFOUND:
                {
                    Console.WriteLine("Error: Cannot determine branch. Is the RS_PROJROOT environment variable set correctly?");
                }
                break;
            }

            //  Pause before quitting
            Pause();
            //  Exit the app with an error
            Environment.Exit(-1);
        }

        /// <summary>
        /// Does the same as cmd pause
        /// </summary>
        private static void Pause()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey(true);
        }
    }
}
