﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Xml;

namespace SanScriptProjectConvert
{
    public class Config
    {
        private string m_name = "";
        private string m_compilerExe = "";
        private string m_resourceExe = "";
        private string m_resourceExe64 = "";
        private string m_outputDir = "";
        private string m_postCompile = "";
        private string m_include = "";
        private string m_includeDirectories = "";
        private string m_warningsAsErrors = "false";
        private string m_cmdLine = "";

        public Config()
        {

        }

        /// <summary>
        /// PostCompile override.
        /// </summary>
        public string OverridePostCompileCommand { get; set; }

        /// <summary>
        /// The branch that the project file. 
        /// </summary>
        public string Branch { get; set; }

        /// <summary>
        /// Get the configuration name.
        /// </summary>
        /// <returns>Configuration name.</returns>
        public string GetName()
        {
            return m_name;
        }

        /// <summary>
        /// Get the relative path to the compiler executable.
        /// </summary>
        /// <returns>The relative path to the compiler executable.</returns>
        public string GetCompilerExe()
        {
            return m_compilerExe;
        }

        /// <summary>
        /// Get the relative path to the resource executable.
        /// </summary>
        /// <returns>The relative path to the resource executable.</returns>
        public string GetResourceExe()
        {
            return m_resourceExe;
        }

        /// <summary>
        /// Get the relative path to the 64-bit resource executable.
        /// </summary>
        /// <returns>The relative path to the 64-bit resource executable.</returns>
        public string GetResourceExe64()
        {
            return m_resourceExe64;
        }

        public void WriteToXml(StreamWriter writer)
        {
            writer.WriteLine("\t<PropertyGroup Condition=\" '$(Configuration)' == '" + m_name + "' \">");
            writer.WriteLine("\t\t<DebugSymbols>" + (m_name == "Debug" ? "true" : "false") + "</DebugSymbols>");
            writer.WriteLine("\t\t<OutputPath>" + m_outputDir + "\\</OutputPath>");
            writer.WriteLine("\t\t<CmdLineParams>" + m_cmdLine + "</CmdLineParams>");
            writer.WriteLine("\t\t<Include>-include " + m_include + "</Include>");
            writer.WriteLine("\t\t<IncludePath>-ipath " + m_includeDirectories + "</IncludePath>");
            writer.WriteLine("\t\t<PostCompilePath>" + m_postCompile + "</PostCompilePath>");
            writer.WriteLine("\t\t<WarningsAsErrors>" + m_warningsAsErrors + "</WarningsAsErrors>");
            writer.WriteLine("\t</PropertyGroup>");
        }

        public void ReadFromXml(XmlTextReader reader)
        {
            //  Read the elements
            while (reader.Read())
            {
                //  Move to the first element
                reader.MoveToElement();
                if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        case "ConfigurationName":
                        {
                            //  Read in the text
                            reader.Read();
                            //  Get the text
                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                m_name = reader.Value;
                            }
                            //  Read the end element
                            reader.Read();
                        }
                        break;
                        case "CompilerExecutable":
                        {
                            //  Read in the text
                            reader.Read();
                            //  Get the text
                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                m_compilerExe = ToSolutionDir(reader.Value);
                            }
                            //  Read the end element
                            reader.Read();
                        }
                        break;
                        case "ResourceExecutable":
                        {
                            //  Read in the text
                            reader.Read();
                            //  Get the text
                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                string resExe = reader.Value;

                                string folder = Path.GetDirectoryName(resExe);
                                string filename = Path.GetFileNameWithoutExtension(resExe);
                                filename += "_x64.exe";
                                m_resourceExe64 = Path.Combine(folder, filename);

                                m_resourceExe = ToSolutionDir(reader.Value);
                                m_resourceExe64 = ToSolutionDir(m_resourceExe64);
                            }
                            //  Read the end element
                            reader.Read();
                        }
                        break;
                        case "OutputDirectory":
                        {
                            //  Read in the text
                            reader.Read();
                            //  Get the text
                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                m_outputDir = ToSolutionDir(reader.Value);
                            }
                            //  Read the end element
                            reader.Read();
                        }
                        break;
                        case "PostCompileCommand":
                        {
                            //  Read in the text
                            reader.Read();
                            //  Get the text
                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                m_postCompile = String.IsNullOrEmpty(OverridePostCompileCommand) ? reader.Value : OverridePostCompileCommand;
                            }
                            //  Read the end element
                            reader.Read();
                        }
                        break;
                        case "IncludeFile":
                        {
                            //  Read in the text
                            reader.Read();
                            //  Get the text
                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                m_include = reader.Value;
                            }
                            //  Read the end element
                            reader.Read();
                        }
                        break;
                        case "IncludePaths":
                        {
                            //  Read in each path
                            while (reader.Read())
                            {
                                //  Move to the first element
                                reader.MoveToElement();
                                if (reader.NodeType == XmlNodeType.Element && 
                                    reader.Name == "string")
                                {
                                    //  Read the value
                                    reader.Read();
                                    //  Append it to the directories list
                                    if (m_includeDirectories.Length > 0)
                                    {
                                        m_includeDirectories += ";" + reader.Value;
                                    }
                                    else
                                    {
                                        m_includeDirectories = reader.Value;
                                    }
                                    //  Read the end element
                                    reader.Read();
                                }
                                else if (reader.NodeType == XmlNodeType.EndElement &&
                                    reader.Name == "IncludePaths")
                                {
                                    //  That's the last of the directories
                                    break;
                                }
                            }
                        }
                        break;
                        case "WarningsAsErrors":
                        {
                            //  Read in the text
                            reader.Read();
                            //  Get the text
                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                m_warningsAsErrors = reader.Value == "true" ? "-werror" : "";
                            }
                            //  Read the end element
                            reader.Read();
                        }
                        break;
                        case "Custom":
                        {
                            //  Read in the text
                            reader.Read();
                            //  Get the text
                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                m_cmdLine = reader.Value;
                            }
                            //  Read the end element
                            reader.Read();
                        }
                        break;
                    }
                }
                else if (reader.NodeType == XmlNodeType.EndElement &&
                    reader.Name == "CompilingSettings")
                {
                    //  End of the config

                    string[] dirs = m_includeDirectories.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    List<string> cleaned = new List<string>();

                    foreach (string dir in dirs)
                    {
                        cleaned.Add(ToSolutionDir(dir));
                    }

                    m_includeDirectories = String.Join(";", cleaned.ToArray());

                    return;
                }
            }
        }

        /// <summary>
        /// Convert a string from using an absolute path to one using a relative-to-solution-directory path.
        /// </summary>
        /// <param name="fullPath">Full path.</param>
        /// <returns>The path relative to the solution directory.</returns>
        private string ToSolutionDir(string fullPath)
        {
            int devIdx = fullPath.IndexOf(Branch + "\\", StringComparison.OrdinalIgnoreCase);
            if (devIdx >= 0)
            {
                devIdx += (Branch + "\\").Length;
                string temp = fullPath.Substring(devIdx);

				//To handle how solutions want to be set up based on project, until the RS_SCRIPTBRANCH variable 
				//is pushed through all projects in the installer.
                if (Environment.GetEnvironmentVariable("RS_SCRIPTBRANCH") != null)
                    temp = "$(RS_SCRIPTBRANCH)\\" + temp;
                else if (String.Compare(Environment.GetEnvironmentVariable("RS_PROJECT"), "rdr3", true) == 0)
                    temp = "$(SolutionDir)\\" + temp;
                else
                    temp = "$(SolutionDir)..\\" + temp;
                    
                return temp;
            }

            return fullPath;
        }

    }
}
