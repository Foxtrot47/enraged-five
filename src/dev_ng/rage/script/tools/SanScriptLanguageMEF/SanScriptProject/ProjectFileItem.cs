﻿namespace Rockstar.SanScriptProject
{
    /// <summary>
    /// Class representing a single file in the project.
    /// </summary>
    class ProjectFileItem
    {
        #region Properties

        /// <summary>
        /// The name of the file.
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Full path to the file.
        /// </summary>
        public string FullPath { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="filename">The name of the file.</param>
        /// <param name="fullpath">The full path to the file.</param>
        public ProjectFileItem(string filename, string fullpath)
        {
            Filename = filename;
            FullPath = fullpath;
        }

        #endregion
    }
}
