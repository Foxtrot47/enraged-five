﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.ComponentModel.Design;

using Microsoft.VisualStudio;
using Microsoft.VisualStudio.CommandBars;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.OLE.Interop;
using Microsoft.VisualStudio.Shell;

using Microsoft.VisualStudio.Project;

using EnvDTE;
using Microsoft.VisualStudio.TextManager.Interop;
using Rockstar.SanScriptProject.Navigation;
using RSG.Pipeline.BuildScript;
using Rockstar.SanScriptProject.View;

namespace Rockstar.SanScriptProject
{
    /// <summary>
    /// This is the class that implements the package exposed by this assembly.
    ///
    /// The minimum requirement for a class to be considered a valid package for Visual Studio
    /// is to implement the IVsPackage interface and register itself with the shell.
    /// This package uses the helper classes defined inside the Managed Package Framework (MPF)
    /// to do it: it derives from the Package class that provides the implementation of the 
    /// IVsPackage interface and uses the registration attributes defined in the framework to 
    /// register itself and its components with the shell.
    /// </summary>
    // This attribute tells the PkgDef creation utility (CreatePkgDef.exe) that this class is
    // a package.
    [PackageRegistration(UseManagedResourcesOnly = true)]
    // This attribute is used to register the informations needed to show the this package
    // in the Help/About dialog of Visual Studio.
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
    [ProvideProjectFactory(
        typeof(SanScriptProjectFactory),                    //  Factory type
        null,                                               //  Name
        "SanScript Project Files (*.ssproj);*.ssproj",      //  Project file extension for Open/Save dialogs
        "ssproj", "ssproj",                                 //  Default and possible project extensions
        ".\\NullPath",                                      //  Project template directory
        LanguageVsTemplate = "SanScript")]                  //  Project template
    [Guid(GuidList.guidSanScriptProjectPkgString)]
    [ProvideObject(typeof(SanScriptGeneralPropertyPage))]
    [ProvideObject(typeof(SanScriptProjectPropertyPage))]
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [ProvideLanguageService(typeof(SanScriptLanguageInfo), "SanScript", 106, RequestStockColors = true, ShowSmartIndent = true, ShowCompletion = true, DefaultToInsertSpaces = true, HideAdvancedMembersByDefault = false, EnableAdvancedMembersOption = true, ShowDropDownOptions = true)]
    [ProvideLanguageExtension(typeof(SanScriptLanguageInfo), ".sc")]
    [ProvideLanguageExtension(typeof(SanScriptLanguageInfo), ".sch")]
    public sealed class SanScriptProjectPackage : ProjectPackage
    {
        private string m_lastOpenFileSearch;

        public static SanScriptProjectPackage Instance;

        private OleMenuCommand m_buildMenuCommand = null;
        private OleMenuCommand m_rebuildMenuCommand = null;
        private OleMenuCommand m_cleanMenuCommand = null;
        private OleMenuCommand m_stopBuildMenuCommand = null;
        private OleMenuCommand m_buildFileMenuCommand = null;
        private OleMenuCommand m_buildFileForPreviewMenuCommand = null;
        private OleMenuCommand m_openHeaderFileCommand = null;
        private OleMenuCommand m_openFileInSolutionCommand = null;

        /// <summary>
        /// LanguagePreferences.
        /// </summary>
        internal Rockstar.SanScriptProject.Navigation.LanguagePreferences LanguagePreferences { get; private set; }

        /// <summary>
        /// MEF Menu handler.
        /// </summary>
        internal event EventHandler MEFMenuHandler;

        /// <summary>
        /// Contains using event handler.
        /// </summary>
        internal event ContainsUsingEventHandler ContainsUsing;

        /// <summary>
        /// Default constructor of the package.
        /// Inside this method you can place any initialization code that does not require 
        /// any Visual Studio service because at this point the package object is created but 
        /// not sited yet inside Visual Studio environment. The place to do all the other 
        /// initialization is the Initialize method.
        /// </summary>
        public SanScriptProjectPackage()
        {
            Trace.WriteLine(string.Format(CultureInfo.CurrentCulture, "Entering constructor for: {0}", this.ToString()));
            Instance = this;
        }
        
        /// <summary>
        /// This function is the callback used to execute a command when the a menu item is clicked.
        /// See the Initialize method to see how the menu item is associated to this function using
        /// the OleMenuCommandService service and the MenuCommand class.
        /// </summary>
        private void MenuItemCallback(object sender, EventArgs e)
        {
            OleMenuCommand command = sender as OleMenuCommand;

            if (command != null && command.CommandID != null)
            {
                switch (command.CommandID.ID)
                {
                    case (int)PkgCmdIDList.cmdidBuildProject:
                        {
                        if (IncrediBuild.IsInstalled)
                        {
                            IncredibuildOptions options = new IncredibuildOptions(false, "Rebuild");
                            BackgroundWorker worker = new BackgroundWorker();
                            worker.DoWork += new DoWorkEventHandler(BuildProject);
                            worker.RunWorkerAsync(options);
                        }
                    }
                    break;
                    case (int)PkgCmdIDList.cmdidRebuildProject:
                    {
                        if (IncrediBuild.IsInstalled)
                        {
                            IncredibuildOptions options = new IncredibuildOptions(true, "Rebuild");
                            BackgroundWorker worker = new BackgroundWorker();
                            worker.DoWork += new DoWorkEventHandler(BuildProject);
                            worker.RunWorkerAsync(options);
                        }
                    }
                    break;
                    case (int)PkgCmdIDList.cmdidCleanProject:
                    {
                        if (IncrediBuild.IsInstalled)
                        {
                            IncredibuildOptions options = new IncredibuildOptions(true, "Clean");
                            BackgroundWorker worker = new BackgroundWorker();
                            worker.DoWork += new DoWorkEventHandler(BuildProject);
                            worker.RunWorkerAsync(options);
                        }
                    }
                    break;
                    case (int)PkgCmdIDList.cmdidStopBuild:
                    {
                        if (IncrediBuild.IsInstalled)
                        {
                            IncredibuildOptions options = new IncredibuildOptions(true, "Stop");
                            BackgroundWorker worker = new BackgroundWorker();
                            worker.DoWork += new DoWorkEventHandler(StopBuildProject);
                            worker.RunWorkerAsync(options);
                        }
                    }
                    break;
                    case (int)PkgCmdIDList.cmdidBuildFile:
                    case (int)PkgCmdIDList.cmdidBuildFileForPreview:
                    {
                        BackgroundWorker worker = new BackgroundWorker();
                        worker.DoWork += new DoWorkEventHandler(BuildFile);
                        worker.RunWorkerAsync((command.CommandID.ID == (int)PkgCmdIDList.cmdidBuildFileForPreview));
                    }
                    break;
                    case (int)PkgCmdIDList.cmdidOpenHeaderFile:
                    {
                        IVsTextManager textMgr = (IVsTextManager)GetService(typeof(SVsTextManager));
                        IVsTextView view = null;
                        textMgr.GetActiveView(1, null, out view);

                        if (MEFMenuHandler != null)
                        {
                            MEFMenuHandler(sender, e);
                        }

                    }
                    break;
                    case (int)PkgCmdIDList.cmdidOpenFileInSolution:
                    {
                        DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
                        ProjectFileCollector collector = new ProjectFileCollector(dte);

                        var vm = new ProjectFileCollectionViewModel(collector);
                        OpenFileInSolutionWindow openInWindow = new OpenFileInSolutionWindow(vm);
                        vm.Filter = m_lastOpenFileSearch;
                        if (openInWindow.ShowDialog().GetValueOrDefault(false))
                        {
                            m_lastOpenFileSearch = vm.Filter;

                            if (vm.SelectedItem != null)
                            {
                                dte.ItemOperations.OpenFile(vm.SelectedItem.FullPath, EnvDTE.Constants.vsViewKindTextView);
                            }
                        }
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// Background Worker function for compiling a file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildFile(object sender, DoWorkEventArgs e)
        {
            bool preview = (bool)e.Argument;
            SanScriptCompileTask compileTask = new SanScriptCompileTask();

            ToggleMenuState(false);
            compileTask.CompileCurrentFile(preview);
            ToggleMenuState(true);
        }

        /// <summary>
        /// Background Worker function for compiling the project.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuildProject(object sender, DoWorkEventArgs e)
        {
            IncredibuildOptions options = (IncredibuildOptions)e.Argument;

            ToggleMenuState(false);
            IncrediBuildTask ibTask = new IncrediBuildTask();
            ibTask.Run(options);
            ToggleMenuState(true);
        }

        /// <summary>
        /// Background Worker function for stopping any project compilation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopBuildProject(object sender, DoWorkEventArgs e)
        {
            IncredibuildOptions options = (IncredibuildOptions)e.Argument;

            ToggleMenuState(false);
            IncrediBuildTask ibTask = new IncrediBuildTask();
            ibTask.Stop();
            ToggleMenuState(true);
        }

        /// <summary>
        /// Change the enabled state of the Open File In Solution command dependent
        /// on whether there is an active solution or not.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void QueryStatusOpenFileInSolution(object sender, EventArgs e)
        {
            OleMenuCommand command = sender as OleMenuCommand;
            if (command != null && command == m_openFileInSolutionCommand)
            {
                command.Enabled = false;
                DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
                command.Enabled = dte.Solution != null && dte.Solution.IsOpen;
            }
        }

        private void OnBeforeQueryStatus(object sender, EventArgs e)
        {
            OleMenuCommand command = sender as OleMenuCommand;

            if (command != null)
            {
                DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
                //  Get the top level command bar holding all the other menus
                CommandBar menuCommandBar = ((CommandBars)dte.CommandBars)["MenuBar"];
                //  Try and find the Build menu item
                CommandBarControl buildControl = null;

                m_openHeaderFileCommand.Visible = true;

                if (ContainsUsing != null)
                {
                    IVsTextManager textMgr = (IVsTextManager)GetService(typeof(SVsTextManager));
                    IVsTextView view = null;
                    textMgr.GetActiveView(1, null, out view);

                    ContainsUsingEventArguments args = new ContainsUsingEventArguments(view);
                    ContainsUsing(this, args);
                    m_openHeaderFileCommand.Enabled = args.LineContainsUsingStatement;
                }

                try
                {
                    buildControl = menuCommandBar.Controls["Project"];
                    m_buildMenuCommand.Visible = buildControl.Visible;
                    m_rebuildMenuCommand.Visible = buildControl.Visible;
                    m_cleanMenuCommand.Visible = buildControl.Visible;
                    m_stopBuildMenuCommand.Visible = buildControl.Visible;
                    m_buildFileMenuCommand.Visible = buildControl.Visible;
                    m_buildFileForPreviewMenuCommand.Visible = buildControl.Visible;
                }
                catch (System.Exception)
                {
                    m_buildMenuCommand.Visible = false;
                    m_rebuildMenuCommand.Visible = false;
                    m_cleanMenuCommand.Visible = false;
                    m_stopBuildMenuCommand.Visible = false;
                    m_buildFileMenuCommand.Visible = false;
                    m_buildFileForPreviewMenuCommand.Visible = false;
                }
            }
        }

        /// <summary>
        /// Changes the state of the Visual Studio build menus.
        /// </summary>
        /// <param name="enabled"></param>
        private void ToggleMenuState(bool enabled)
        {
            m_buildMenuCommand.Enabled = enabled;
            m_rebuildMenuCommand.Enabled = enabled;
            m_cleanMenuCommand.Enabled = enabled;
            m_stopBuildMenuCommand.Enabled = !enabled;
            m_buildFileMenuCommand.Enabled = enabled;
            m_buildFileForPreviewMenuCommand.Enabled = enabled;
        }

        /////////////////////////////////////////////////////////////////////////////
        // Overridden Package Implementation
        #region Package Members

        /// <summary>
        /// Initialization of the package; this method is called right after the package is sited, so this is the place
        /// where you can put all the initilaization code that rely on services provided by VisualStudio.
        /// </summary>
        protected override void Initialize()
        {
            Trace.WriteLine (string.Format(CultureInfo.CurrentCulture, "Entering Initialize() of: {0}", this.ToString()));
            base.Initialize();

            m_lastOpenFileSearch = "";
            
            // Add our command handlers for menu (commands must exist in the .vsct file)
            OleMenuCommandService mcs = GetService(typeof(IMenuCommandService)) as OleMenuCommandService;

            if (null != mcs)
            {
                // Create the command for the menu items.
                CommandID menuCommandID = new CommandID(GuidList.guidSanScriptProjectCmdSet, (int)PkgCmdIDList.cmdidBuildProject);
                m_buildMenuCommand = new OleMenuCommand(MenuItemCallback, menuCommandID);
                m_buildMenuCommand.BeforeQueryStatus += new EventHandler(OnBeforeQueryStatus);
                m_buildMenuCommand.Visible = false;
                mcs.AddCommand(m_buildMenuCommand);

                menuCommandID = new CommandID(GuidList.guidSanScriptProjectCmdSet, (int)PkgCmdIDList.cmdidRebuildProject);
                m_rebuildMenuCommand = new OleMenuCommand(MenuItemCallback, menuCommandID);
                m_rebuildMenuCommand.BeforeQueryStatus += new EventHandler(OnBeforeQueryStatus);
                m_rebuildMenuCommand.Visible = false;
                mcs.AddCommand(m_rebuildMenuCommand);

                menuCommandID = new CommandID(GuidList.guidSanScriptProjectCmdSet, (int)PkgCmdIDList.cmdidCleanProject);
                m_cleanMenuCommand = new OleMenuCommand(MenuItemCallback, menuCommandID);
                m_cleanMenuCommand.BeforeQueryStatus += new EventHandler(OnBeforeQueryStatus);
                m_cleanMenuCommand.Visible = false;
                mcs.AddCommand(m_cleanMenuCommand);

                menuCommandID = new CommandID(GuidList.guidSanScriptProjectCmdSet, (int)PkgCmdIDList.cmdidStopBuild);
                m_stopBuildMenuCommand = new OleMenuCommand(MenuItemCallback, menuCommandID);
                m_stopBuildMenuCommand.BeforeQueryStatus += new EventHandler(OnBeforeQueryStatus);
                m_stopBuildMenuCommand.Visible = false;
                mcs.AddCommand(m_stopBuildMenuCommand);

                menuCommandID = new CommandID(GuidList.guidSanScriptProjectCmdSet, (int)PkgCmdIDList.cmdidBuildFile);
                m_buildFileMenuCommand = new OleMenuCommand(MenuItemCallback, menuCommandID);
                m_buildFileMenuCommand.BeforeQueryStatus += new EventHandler(OnBeforeQueryStatus);
                m_buildFileMenuCommand.Visible = false;
                mcs.AddCommand(m_buildFileMenuCommand);

                menuCommandID = new CommandID(GuidList.guidSanScriptProjectCmdSet, (int)PkgCmdIDList.cmdidBuildFileForPreview);
                m_buildFileForPreviewMenuCommand = new OleMenuCommand(MenuItemCallback, menuCommandID);
                m_buildFileForPreviewMenuCommand.BeforeQueryStatus += new EventHandler(OnBeforeQueryStatus);
                m_buildFileForPreviewMenuCommand.Visible = false;
                mcs.AddCommand(m_buildFileForPreviewMenuCommand);

                menuCommandID = new CommandID(GuidList.guidSanScriptMenuProjectCmdSet, (int)PkgCmdIDList.cmdidOpenHeaderFile);
                m_openHeaderFileCommand = new OleMenuCommand(MenuItemCallback, menuCommandID);
                m_openHeaderFileCommand.BeforeQueryStatus += new EventHandler(OnBeforeQueryStatus);
                m_openHeaderFileCommand.Visible = false;
                mcs.AddCommand(m_openHeaderFileCommand);

                menuCommandID = new CommandID(GuidList.guidTopLevelRockStarMenu, (int)PkgCmdIDList.cmdidOpenFileInSolution);
                m_openFileInSolutionCommand = new OleMenuCommand(MenuItemCallback, menuCommandID);
                m_openFileInSolutionCommand.BeforeQueryStatus += new EventHandler(QueryStatusOpenFileInSolution);
                mcs.AddCommand(m_openFileInSolutionCommand);
            }


            var langService = new SanScriptLanguageInfo(this);
            ((IServiceContainer)this).AddService(langService.GetType(), langService, true);

            var solution = (IVsSolution)Package.GetGlobalService(typeof(SVsSolution));
            uint cookie;
            ErrorHandler.ThrowOnFailure(solution.AdviseSolutionEvents(new SolutionAdvisor(), out cookie));

            IVsTextManager textMgr = (IVsTextManager)GetService(typeof(SVsTextManager));
            var langPrefs = new LANGPREFERENCES[1];
            langPrefs[0].guidLang = typeof(SanScriptLanguageInfo).GUID;
            ErrorHandler.ThrowOnFailure(textMgr.GetUserPreferences(null, null, langPrefs, null));
            var _langPrefs = new Navigation.LanguagePreferences(langPrefs[0]);

            this.LanguagePreferences = _langPrefs;

            Guid guid = typeof(IVsTextManagerEvents2).GUID;
            IConnectionPoint connectionPoint;
            ((IConnectionPointContainer)textMgr).FindConnectionPoint(ref guid, out connectionPoint);
            connectionPoint.Advise(_langPrefs, out cookie);

            //  Register our project factory with this package
            RegisterProjectFactory(new SanScriptProjectFactory(this));
        }

        public override string ProductUserContext
        {
            get
            {
                return null;
            }
        }

        public object GetVsService(Type serviceType)
        {
            return GetService(serviceType);
        }

        #endregion

    }
}
