﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using Microsoft.VisualStudio.Project;

namespace Rockstar.SanScriptProject
{
    public class SanScriptFileNode : FileNode
    {
        internal int imageIndex;
        public override int ImageIndex
        {
            get
            {
                return imageIndex;
            }
        }

        public string SubType
        {
            get
            {
                return ItemNode.GetMetadata(ProjectFileConstants.SubType);
            }
            set
            {
                ItemNode.SetMetadata(ProjectFileConstants.SubType, value);
            }
        }

        public SanScriptFileNode(ProjectNode root, ProjectElement e) : base(root, e)
        {
            string extension = Path.GetExtension(e.GetMetadata(ProjectFileConstants.Include)).ToLower();

            if (extension == ".sch")
            {
                imageIndex = root.ImageIndex + 2;
            }
            else if (extension == ".sc")
            {
                imageIndex = root.ImageIndex + 1;
            }
            else
            {
                imageIndex = base.ImageIndex;
            }
        }

    }
}
