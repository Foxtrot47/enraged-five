﻿using System;
using System.Collections.Generic;
using EnvDTE;
using Microsoft.VisualStudio.Project.Automation;
using System.IO;

namespace Rockstar.SanScriptProject
{
    /// <summary>
    /// Collects the list of files contained within the solution's projects.
    /// </summary>
    class ProjectFileCollector
    {
        #region Constants taken from http://msdn.microsoft.com/en-us/library/z4bcch80(v=vs.80).aspx

        private const string FILE = "{6BB5F8EE-4483-11D3-8BCF-00C04F8EC28C}";
        private const string FOLDER = "{6BB5F8EF-4483-11D3-8BCF-00C04F8EC28C}";
        private const string VIRTFOLDER = "{6BB5F8F0-4483-11D3-8BCF-00C04F8EC28C}";
        private const string SUBPROJECT = "{EA6618E8-6E24-4528-94BE-6889FE16485C}";

        #endregion

        #region Private member fields

        private List<ProjectFileItem> m_items;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="dte">DTE.</param>
        public ProjectFileCollector(DTE dte)
        {
            m_items = new List<ProjectFileItem>();

            Array activeSolutionProjects = dte.ActiveSolutionProjects as Array;
            foreach(var project in activeSolutionProjects)
            {
                ParseProject(project as EnvDTE.Project);
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Get the items contained within the solution's projects.
        /// </summary>
        /// <returns>Enumerated list of project items.</returns>
        public IEnumerable<ProjectFileItem> GetItems()
        {
            foreach (var item in m_items)
            {
                yield return item;
            }
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Traverse the project tree to capture all the files within.
        /// </summary>
        /// <param name="project">Project.</param>
        private void ParseProject(Project project)
        {
            foreach (var item in project.ProjectItems)
            {
                var projItem = item as EnvDTE.ProjectItem;
                switch (projItem.Kind.ToUpper())
                {
                    case FILE:
                        AddFile(projItem as OAFileItem);
                        break;
                    case FOLDER:
                    case VIRTFOLDER:
                        ParseFolder(projItem as OAFolderItem);
                        break;
                }
            }
        }

        /// <summary>
        /// Parse the folder inside a project.
        /// </summary>
        /// <param name="folder">Folder.</param>
        private void ParseFolder(OAFolderItem folder)
        {
            if (folder == null)
            {
                return;
            }

            foreach (var item in folder.ProjectItems)
            {
                if (item is OAFolderItem)
                {
                    ParseFolder(item as OAFolderItem);
                }
                else if (item is OAFileItem)
                {
                    AddFile(item as OAFileItem);
                }
            }
        }

        /// <summary>
        /// Add a file to the internal list.
        /// </summary>
        /// <param name="file">File.</param>
        private void AddFile(OAFileItem file)
        {
            string fullPath = file.get_FileNames(0);
            string filename = Path.GetFileName(fullPath);

            var item = new ProjectFileItem(filename, fullPath);
            m_items.Add(item);
        }

        #endregion
    }
}
