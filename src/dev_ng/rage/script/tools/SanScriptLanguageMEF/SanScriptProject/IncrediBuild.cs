﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using Microsoft.Win32;
using Microsoft.VisualStudio.CommandBars;
using Microsoft.VisualStudio.Shell;
using EnvDTE80;
using EnvDTE;
using RSG.Base.Configuration;
using RSG.Pipeline.BuildScript;

namespace Rockstar.SanScriptProject
{
    /// <summary>
    /// Helper structure for passing an Incredibuild instruction.
    /// </summary>
    public struct IncredibuildOptions
    {
        public IncredibuildOptions(bool prebuild, string instruction)
        {
            Prebuild = prebuild;
            Instruction = instruction;
        }

        public bool Prebuild;
        public string Instruction;
    }

    public class IncrediBuildTask : ToolTask
    {
        private static string m_command = "Incredibuild";

        #region Overrides

        protected override string ToolName
        {
            get
            {
                return m_command;
            }
        }

        protected override string GenerateFullPathToTool()
        {
            return m_command;
        }
        #endregion


        #region XML Functions

        /// <summary>
        /// Stops any current instance of Incredibuild.
        /// </summary>
        public void Stop()
        {
            IncrediBuild.Stop();
        }

        #endregion

        public void Run(IncredibuildOptions options)
        {
            List<RSG.Platform.Platform> enabledPlatforms = new List<RSG.Platform.Platform>();
            foreach (KeyValuePair<RSG.Platform.Platform, RSG.Base.Configuration.ITarget> pair in Configuration.Instance.Config.Project.DefaultBranch.Targets)
            {
                if (pair.Value.Enabled == true)
                {
                    enabledPlatforms.Add(pair.Key); 
                }
            }

            SanScriptBuildData data = new SanScriptBuildData(options.Prebuild, enabledPlatforms);
            IncrediBuild.CreateBuildConfiguration(data);
            IncrediBuild.BuildOutput = Incredibuild_BuildOutput;
            IncrediBuild.RunBuildConfiguration(options.Instruction);
        }

        private void Incredibuild_BuildOutput(object sender, DataReceivedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.Data))
            {
                Log.LogMessage(e.Data);
            }
        }
    }
}
