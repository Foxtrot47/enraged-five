﻿using System;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Project;
using System.ComponentModel;

namespace Rockstar.SanScriptProject
{
    [ComVisible(true)]
    [Guid("47013BD6-A2C9-4B0A-925A-3DC42071B652")]
    public class SanScriptGeneralPropertyPage : SettingsPage
    {
        private string m_assemblyName;
        private string m_compilerExe;
        private string m_resourceExe;
        private string m_resourceExe64;

        public SanScriptGeneralPropertyPage()
        {
            Name = "General";
        }

        [Category("General")]
        [DisplayName("Project Name")]
        [Description("The name of the project.")]
        public string AssemblyName
        {
            get { return m_assemblyName; }
        }

        [Category("Executables")]
        [DisplayName("Compiler Executable")]
        [Description("The script compiler program.")]
        public string CompilerExe
        {
            get { return m_compilerExe; }
            set { m_compilerExe = value; IsDirty = true; }
        }

        [Category("Executables")]
        [DisplayName("Resource Executable")]
        [Description("The script resource compiler program.")]
        public string ResourceExe
        {
            get { return m_resourceExe; }
            set { m_resourceExe = value; IsDirty = true; }
        }

        [Category("Executables")]
        [DisplayName("Resource Executable")]
        [Description("The 64-bit script resource compiler program.")]
        public string ResourceExe64
        {
            get { return m_resourceExe64; }
            set { m_resourceExe64 = value; IsDirty = true; }
        }

        protected override void BindProperties()
        {
            m_assemblyName = ProjectMgr.GetProjectProperty("AssemblyName", true);
            m_compilerExe = ProjectMgr.GetProjectProperty("CompilerExe", true);
            m_resourceExe = ProjectMgr.GetProjectProperty("ResourceExe", true);
            m_resourceExe64 = ProjectMgr.GetProjectProperty("ResourceExe64", true);
        }

        protected override int ApplyChanges()
        {
            ProjectMgr.SetProjectProperty("AssemblyName", m_assemblyName);
            ProjectMgr.SetProjectProperty("CompilerExe", m_compilerExe);
            ProjectMgr.SetProjectProperty("ResourceExe", m_resourceExe);
            ProjectMgr.SetProjectProperty("ResourceExe64", m_resourceExe64);
            this.IsDirty = false;

            return VSConstants.S_OK;
        }
    }
}
