﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rockstar.SanScriptProject
{
    public enum PkgCmdIDList
    {
        cmdidBuildProject = 0x0100, 
        cmdidRebuildProject = 0x0101, 
        cmdidCleanProject = 0x0102, 
        cmdidStopBuild = 0x0200,
        cmdidBuildFile = 0x300,
        cmdidBuildFileForPreview = 0x301,
        cmdidOpenHeaderFile = 0x302,
        cmdidOpenFileInSolution = 0x1026
    }
}
