﻿using System;

namespace Rockstar.SanScriptProject
{
    static class GuidList
    {
        //  List of GUIDs in string format, created from Tools->Create GUID
        public const string guidSanScriptProjectPkgString = "5ABE954E-8464-47C5-9502-7FDBC6F4021F";
        public const string guidSanScriptProjectCmdSetString = "EB9501FE-EB04-415B-B9D7-27C562C51A11";
        public const string guidSanScriptProjectFactoryString = "FD9DCD24-FF43-4B02-AC76-12444CB05BBC";
        public const string guidSanScriptMenuProjectCmdSetString = "3813439F-4765-4B27-87CD-A1CD48EFAC31";
        public const string guidTopLevelMenuCmdSet = "77225ee9-0b83-403d-b576-16f04d0ac090";
        //  GUIDs in Guid format
        public static readonly Guid guidSanScriptProjectCmdSet = new Guid(guidSanScriptProjectCmdSetString);
        public static readonly Guid guidSanScriptMenuProjectCmdSet = new Guid(guidSanScriptMenuProjectCmdSetString);
        public static readonly Guid guidSanScriptProjectFactory = new Guid(guidSanScriptProjectFactoryString);
        public static readonly Guid guidTopLevelRockStarMenu = new Guid(guidTopLevelMenuCmdSet);
    };
}