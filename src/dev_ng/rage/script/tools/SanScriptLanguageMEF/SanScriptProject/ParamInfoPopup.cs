﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using SanScriptLanguageMEF;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.TextManager.Interop;
using System.Runtime.InteropServices;

namespace Rockstar.SanScriptProject
{
    /// <summary>
    /// Parameter info window factory class. Contains a single Create() method that creates
    /// an instance of an IParamInfoPopup class.
    /// </summary>
    [Export(typeof(IParamInfoPopupFactory))]
    public class ParamInfoFactory : IParamInfoPopupFactory
    {
        /// <summary>
        /// Create a new parameter information pop-up.
        /// </summary>
        /// <param name="textView">Text view.</param>
        /// <returns>Parameter information pop-up.</returns>
        public IParamInfoPopup Create(IPopupHelper popupHelper, IVsTextView textView)
        {
            return new ParamInfoPopup(popupHelper, textView);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ParamInfoPopup : IParamInfoPopup, IVsMethodData
    {
        #region Private member fields

        private IVsTextView m_textView;
        private IPopupHelper m_popupHelper;
        private IVsMethodTipWindow m_window;

        #endregion

        #region Public properties

        public bool IsShowing { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="popupHelper">Pop-up helper.</param>
        /// <param name="textView">Text view.</param>
        public ParamInfoPopup(IPopupHelper popupHelper, IVsTextView textView)
        {
            m_popupHelper = popupHelper;
            m_textView = textView;
            Guid riid = typeof(IVsMethodTipWindow).GUID;
            Guid clsid = typeof(VsMethodTipWindowClass).GUID;
            m_window = (IVsMethodTipWindow)SanScriptProjectPackage.Instance.CreateInstance(ref clsid, ref riid, typeof(IVsMethodTipWindow));
            m_window.SetMethodData(this);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Refresh the Parameter Info Tooltips window.
        /// </summary>
        public void Refresh()
        {
            m_popupHelper.Update();
            m_textView.UpdateTipWindow(m_window, (uint)(TipWindowFlags.UTW_CONTENTCHANGED | TipWindowFlags.UTW_CONTEXTCHANGED));
            IsShowing = true;
        }
        
        /// <summary>
        /// Returns the position and length of the relevant data in the current text buffer. 
        /// This instructs the IDE to not obscure that data with the tooltip window.
        /// </summary>
        /// <param name="piPos">Position.</param>
        /// <param name="piLength">Length.</param>
        /// <returns></returns>
        public int GetContextStream(out int piPos, out int piLength)
        {
            piPos = m_popupHelper.Position;
            piLength = 1;
            return 0;
        }
        
        /// <summary>
        /// Returns the method number (zero-based index) you want to be displayed initially. 
        /// For example, if you return zero, then the first overloaded method is initially presented.
        /// </summary>
        /// <returns>Returns the method number (zero-based index) you want to be displayed initially.</returns>
        public int GetCurMethod()
        {
            return 0;
        }

        public int GetCurrentParameter(int iMethod)
        {
            return m_popupHelper.CurrentParameter;
        }

        /// <summary>
        /// The text of the Parameter Info tooltip is constructed during several calls to the GetMethodText and GetParameterText methods.
        /// </summary>
        /// <param name="iMethod">Method index. For overridden methods.</param>
        /// <param name="type">Method text type.</param>
        /// <returns>Pointer to the requested method text.</returns>
        public IntPtr GetMethodText(int iMethod, MethodTextType type)
        {
            if (String.IsNullOrEmpty(m_popupHelper.MethodName))
            {
                return IntPtr.Zero;
            }
            else
            {
                switch (type)
                {
                    case MethodTextType.MTT_NAME:
                        return Marshal.StringToHGlobalUni(m_popupHelper.MethodName);
                    case MethodTextType.MTT_DESCRIPTION:
                        return Marshal.StringToHGlobalUni(m_popupHelper.Signature);
                    default:
                        return IntPtr.Zero;
                }
            }
        }

        public int GetOverloadCount()
        {
            return m_popupHelper.OverloadCount;
        }

        /// <summary>
        /// Returns the number of parameters to display in the method.
        /// </summary>
        /// <param name="iMethod">Method index.</param>
        /// <returns>The number of parameters to display in the method</returns>
        public int GetParameterCount(int iMethod)
        {
            return m_popupHelper.ParameterCount;
        }

        /// <summary>
        /// If you return a method number corresponding with the overload you want displayed, this method is called, 
        /// followed by a call to the UpdateView method.
        /// </summary>
        /// <param name="iMethod">Method index.</param>
        /// <param name="iParm">Parameter index.</param>
        /// <param name="type">Parameter text type.</param>
        /// <returns>The text for the provided parameter text type.</returns>
        public IntPtr GetParameterText(int iMethod, int iParm, ParameterTextType type)
        {
            switch(type)
            {
                case ParameterTextType.PTT_NAME:
                    return Marshal.StringToHGlobalUni(m_popupHelper.GetParameterName(iParm));
                case ParameterTextType.PTT_DECLARATION:
                    string parmType = m_popupHelper.GetParameterType(iParm);
                    string parmName = m_popupHelper.GetParameterName(iParm);
                    return Marshal.StringToHGlobalUni(parmType + " " + parmName);
                default:
                    return IntPtr.Zero;
            }
        }

        /// <summary>
        /// Next method (for method overrides).
        /// </summary>
        /// <returns>Success code.</returns>
        public int NextMethod()
        {
            return 0;
        }

        /// <summary>
        /// Dismiss event.
        /// </summary>
        public void OnDismiss()
        {
            IsShowing = false;
        }

        /// <summary>
        /// Previous method (for method overrides).
        /// </summary>
        /// <returns>Success code.</returns>
        public int PrevMethod()
        {
            return 0;
        }

        /// <summary>
        /// Update the view.
        /// </summary>
        public void UpdateView()
        {
            Refresh();
        }

        #endregion
    }
}
