﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Build.Framework;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace Rockstar.SanScriptProject
{
    public class SanScriptBuildEngine : IBuildEngine
    {
        private List<BuildErrorEventArgs> LogErrorEvents = new List<BuildErrorEventArgs>();
        private List<BuildMessageEventArgs> LogMessageEvents = new List<BuildMessageEventArgs>();
        private List<CustomBuildEventArgs> LogCustomEvents = new List<CustomBuildEventArgs>();
        private List<BuildWarningEventArgs> LogWarningEvents = new List<BuildWarningEventArgs>();

        private IVsOutputWindowPane m_buildPane = null;

        private string m_projectFile = string.Empty;
        private int m_columnNumber = 0;
        private int m_lineNumber = 0;

        public SanScriptBuildEngine(string projectFile)
        {
            //  Store the project filename
            m_projectFile = projectFile;

            //  Get the output window
            IVsOutputWindow m_outputWindow = Package.GetGlobalService(typeof(SVsOutputWindow)) as IVsOutputWindow;
            Guid guidBuild = Microsoft.VisualStudio.VSConstants.OutputWindowPaneGuid.BuildOutputPane_guid;

            //  Try and get the Build pane
            int hr = m_outputWindow.GetPane(guidBuild, out m_buildPane);
            if (hr != VSConstants.S_OK)
            {
                //  Build pane doesn't exist, so create it
                m_outputWindow.CreatePane(guidBuild, "Build", 1, 0);
                //  Get the pane
                hr = m_outputWindow.GetPane(guidBuild, out m_buildPane);
            }

            //  Clear the build pane
            m_buildPane.Clear();
        }

        public bool BuildProjectFile(string projectFileName, string[] targetNames, System.Collections.IDictionary globalProperties, System.Collections.IDictionary targetOutputs)
        {
            throw new NotImplementedException();
        }

        public int ColumnNumberOfTaskNode
        {
            get { return m_columnNumber; }
        }

        public bool ContinueOnError
        {
            get { throw new NotImplementedException(); }
        }

        public int LineNumberOfTaskNode
        {
            get { return m_lineNumber; }
        }

        public void LogCustomEvent(CustomBuildEventArgs e)
        {
            //  Add the event to the list, in case we need it later
            LogCustomEvents.Add(e);

            //  Set the line and column numbers
            m_lineNumber = 0;
            m_columnNumber = 0;

            //  Activate the build pane and write the message into it
            m_buildPane.Activate();
            m_buildPane.OutputString(e.Message + "\n");
            //  Refresh task list
            m_buildPane.FlushToTaskList();
        }

        public void LogErrorEvent(BuildErrorEventArgs e)
        {
            //  Add the error to the list, in case we need it later
            LogErrorEvents.Add(e);

            //  Store the line and column numbers
            m_lineNumber = e.LineNumber;
            m_columnNumber = e.ColumnNumber;

            //  Activate the build pane and write the message into it
            m_buildPane.Activate();
            string errLine = string.Format("{0}({1},{2}): error {3}: {4}\n", e.File, e.LineNumber, e.ColumnNumber, e.Code, e.Message);

            IVsLaunchPad launch = Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof(SVsLaunchPad)) as IVsLaunchPad;

            uint[] priority = new uint[1];
            string[] filename = new string[1];
            uint[] lineNum = new uint[1];
            string[] taskItemText = new string[1];
            int[] taskItemFound = new int[1];
            //  Parse the error string to get values
            launch.ParseOutputStringForTaskItem(errLine, priority, filename, lineNum, taskItemText, taskItemFound);
            //  Output the error message to the output window and the Error task list
            m_buildPane.OutputTaskItemStringEx(errLine,
                                        (VSTASKPRIORITY)priority[0], 
                                        VSTASKCATEGORY.CAT_BUILDCOMPILE, 
                                        "Error", 
                                        0,
                                        filename[0],
                                        lineNum[0],
                                        taskItemText[0], 
                                        null);
            //  Refresh task list
            m_buildPane.FlushToTaskList();
        }

        public void LogMessageEvent(BuildMessageEventArgs e)
        {
            //  Add the event to the list, in case we need it later
            LogMessageEvents.Add(e);

            //  Set the line and column numbers
            m_lineNumber = 0;
            m_columnNumber = 0;

            //  Activate the build pane and write the message into it
            m_buildPane.Activate();
            m_buildPane.OutputString(e.Message + "\n");
            //  Refresh task list
            m_buildPane.FlushToTaskList();
        }

        public void LogWarningEvent(BuildWarningEventArgs e)
        {
            //  Add the warning to the list, in case we need it later
            LogWarningEvents.Add(e);

            //  Store the line and column numbers
            m_lineNumber = e.LineNumber;
            m_columnNumber = e.ColumnNumber;

            //  Activate the build pane and write the message into it
            m_buildPane.Activate();
            string warningLine = string.Format("{0} : warning : {1}\n", e.File, e.Message);

            IVsLaunchPad launch = Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof(SVsLaunchPad)) as IVsLaunchPad;

            uint[] priority = new uint[1];
            string[] filename = new string[1];
            uint[] lineNum = new uint[1];
            string[] taskItemText = new string[1];
            int[] taskItemFound = new int[1];
            //  Parse the warning string to get values
            launch.ParseOutputStringForTaskItem(warningLine, priority, filename, lineNum, taskItemText, taskItemFound);
            //  Output the warning message to the output window and the Warning task list
            m_buildPane.OutputTaskItemStringEx(warningLine,
                                        (VSTASKPRIORITY)priority[0],
                                        VSTASKCATEGORY.CAT_BUILDCOMPILE,
                                        "Warning",
                                        0,
                                        filename[0],
                                        lineNum[0],
                                        taskItemText[0],
                                        null);
            //  Refresh task list
            m_buildPane.FlushToTaskList();
        }

        public string ProjectFileOfTaskNode
        {
            get { return m_projectFile; }
        }
    }
}
