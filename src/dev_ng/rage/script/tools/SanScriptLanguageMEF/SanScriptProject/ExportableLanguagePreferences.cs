﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SanScriptLanguageMEF;
using System.ComponentModel.Composition;

namespace Rockstar.SanScriptProject
{
    [Export(typeof(ILanguagePreferences))]
    public class ExportableLanguagePreferences : ILanguagePreferences
    {
        public bool AutoListMembers
        {
            get { return SanScriptProjectPackage.Instance.LanguagePreferences.AutoListMembers; }
        }
    }
}
