﻿using System;
using System.IO;

namespace Rockstar.SanScriptProject.View
{
    /// <summary>
    /// View model for a project item (file).
    /// </summary>
    class ProjectItemViewModel : ViewModelBase
    {
        #region Properties

        /// <summary>
        /// The name of the file.
        /// </summary>
        public string Filename
        {
            get { return m_filename; }
            set
            {
                m_filename = value;
                OnPropertyChanged("Filename");
            }
        }
        private string m_filename;

        /// <summary>
        /// The full path to the file.
        /// </summary>
        public string FullPath
        {
            get { return m_fullPath; }
            set
            {
                m_fullPath = value;
                OnPropertyChanged("FullPath");
            }
        }
        private string m_fullPath;

        /// <summary>
        /// The date the file was last modified.
        /// </summary>
        public DateTime Modified
        {
            get;
            private set;
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="item">Project file item.</param>
        public ProjectItemViewModel(ProjectFileItem item)
        {
            Filename = item.Filename;
            FullPath = item.FullPath;
            Modified = File.GetLastWriteTime(item.FullPath);
        }

        #endregion
    }
}
