﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Rockstar.SanScriptProject.View
{
    /// <summary>
    /// Interaction logic for OpenFileInSolutionWindow.xaml.
    /// </summary>
    public partial class OpenFileInSolutionWindow : Window
    {
        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        public OpenFileInSolutionWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="viewModel">View model for data context.</param>
        internal OpenFileInSolutionWindow(ProjectFileCollectionViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        #endregion

        #region Overrides

        /// <summary>
        /// If the user clicked the X-button at the top-right of the form instead of the OK/Cancel
        /// buttons, set the DialogResult to false.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (!DialogResult.HasValue)
            {
                DialogResult = false;
            }
        }

        /// <summary>
        /// When the form is activated, give focus to the search box.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            Keyboard.Focus(searchBox);
            searchBox.SelectAll();
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Give the search box focus.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Label_MouseDown(object sender, MouseButtonEventArgs e)
        {
            searchBox.Focus();
        }

        /// <summary>
        /// Toggle the hint visibility based upon the focus of the search box.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void searchBox_Focus(object sender, RoutedEventArgs e)
        {
            ToggleHintVisibility();
        }
        
        /// <summary>
        /// Toggle the hint visibility based upon the contents of the search box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ToggleHintVisibility();
        }

        /// <summary>
        /// Handle the OK button click event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        /// <summary>
        /// Handle the Cancel button click event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Double click is the same as hitting the return key. 
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void DataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Toggles the visibility of the hint text based upon the contents of the search box.
        /// </summary>
        private void ToggleHintVisibility()
        {
            hintLabel.Visibility = String.IsNullOrEmpty(searchBox.Text.Trim()) ? Visibility.Visible : Visibility.Hidden;
        }

        #endregion
    }
}
