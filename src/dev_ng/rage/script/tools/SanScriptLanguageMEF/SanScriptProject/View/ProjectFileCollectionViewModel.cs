﻿using System;
using System.Collections.Generic;
using System.Windows.Data;

namespace Rockstar.SanScriptProject.View
{
    /// <summary>
    /// Project collection view model.
    /// </summary>
    class ProjectFileCollectionViewModel : ViewModelBase
    {
        #region Private member fields

        private int m_fileCount;

        #endregion

        #region Public properties

        /// <summary>
        /// Search box filter criteria.
        /// </summary>
        public string Filter
        {
            get { return m_filter; }
            set
            {
                m_filter = value;
                OnPropertyChanged("Filter");
                UpdateFilter();
            }
        }
        private string m_filter;

        /// <summary>
        /// The list of project files.
        /// </summary>
        public ListCollectionView ProjectFiles
        {
            get { return m_projectFiles; }
            set
            {
                m_projectFiles = value;
                OnPropertyChanged("ProjectFiles");
            }
        }
        private ListCollectionView m_projectFiles;

        /// <summary>
        /// The selected item.
        /// </summary>
        public ProjectItemViewModel SelectedItem
        {
            get { return m_selectedItem; }
            set
            {
                m_selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }
        private ProjectItemViewModel m_selectedItem;

        /// <summary>
        /// The window title.
        /// </summary>
        public string Title
        {
            get { return m_title; }
            set
            {
                m_title = value;
                OnPropertyChanged("Title");
            }
        }
        private string m_title;

        /// <summary>
        /// The search hint that appears over the search box to give users an indicator on how to use the window.
        /// </summary>
        public string SearchHint
        {
            get { return "enter search strings separated by spaces (e.g. sub string. Type \\ first to search full path)"; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="collector"></param>
        public ProjectFileCollectionViewModel(ProjectFileCollector collector)
        {
            Filter = String.Empty;
            FillProjectFiles(collector);
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Builds the view models for the project files.
        /// </summary>
        /// <param name="collector">Project file collector.</param>
        private void FillProjectFiles(ProjectFileCollector collector)
        {
            m_fileCount = 0;

            List<ProjectItemViewModel> items = new List<ProjectItemViewModel>();
            foreach (var item in collector.GetItems())
            {
                ProjectItemViewModel vm = new ProjectItemViewModel(item);
                items.Add(vm);
                m_fileCount++;
            }
            ProjectFiles = new ListCollectionView(items);

            UpdateTitle();
        }

        /// <summary>
        /// Update the window title.
        /// </summary>
        private void UpdateTitle()
        {
            int projFileCount = ProjectFiles == null ? m_fileCount : ProjectFiles.Count;
            Title = String.Format("Open File In Solution [{0} of {1}]", projFileCount, m_fileCount);
        }

        /// <summary>
        /// Update the filter on the 
        /// </summary>
        private void UpdateFilter()
        {
            if (m_projectFiles != null)
            {
                if (!String.IsNullOrEmpty(m_filter))
                {
                    string searchCriteria = m_filter.Trim();
                    bool searchFullPath = false;
                    if (searchCriteria.StartsWith("\\"))
                    {
                        searchCriteria = searchCriteria.Substring(1);
                        searchFullPath = true;
                    }

                    string[] args = searchCriteria.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                    ProjectFiles.Filter = (o) =>
                    {
                        int numMatches = 0;

                        foreach (string s in args)
                        {
                            var vm = o as ProjectItemViewModel;
                            string filePath = searchFullPath ? vm.FullPath : vm.Filename;

                            if (filePath.IndexOf(s, StringComparison.OrdinalIgnoreCase) >= 0)
                            {
                                numMatches++;
                            }
                        }

                        return numMatches == args.Length;
                    };
                }
                else
                {
                    ProjectFiles.Filter = null;
                }
            }

            UpdateTitle();
        }

        #endregion
    }
}
