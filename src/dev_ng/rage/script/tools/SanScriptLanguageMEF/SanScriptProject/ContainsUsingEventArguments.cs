﻿using System;
using Microsoft.VisualStudio.TextManager.Interop;

namespace Rockstar.SanScriptProject
{
    /// <summary>
    /// Event arguments for the <seealso cref="ContainsUsingEventHandler"/> delegate.
    /// </summary>
    public class ContainsUsingEventArguments : EventArgs
    {
        #region Public properties

        /// <summary>
        /// Get the text view associated with the event.
        /// </summary>
        public IVsTextView View
        {
            get;
            private set;
        }

        /// <summary>
        /// Set to true if the line contains a USING statement.
        /// </summary>
        public bool LineContainsUsingStatement
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="view">Text view.</param>
        public ContainsUsingEventArguments(IVsTextView view)
        {
            View = view;
            LineContainsUsingStatement = false;
        }

        #endregion
    }
}
