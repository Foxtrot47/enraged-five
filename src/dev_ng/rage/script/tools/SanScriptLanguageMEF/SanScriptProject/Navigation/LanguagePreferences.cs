﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.TextManager.Interop;

namespace Rockstar.SanScriptProject.Navigation
{
    /// <summary>
    /// Simple implementation of language preferences.
    /// </summary>
    class LanguagePreferences : IVsTextManagerEvents2
    {
        LANGPREFERENCES _preferences;

        public LanguagePreferences(LANGPREFERENCES preferences)
        {
            _preferences = preferences;
        }

        #region IVsTextManagerEvents2 Members

        public int OnRegisterMarkerType(int iMarkerType)
        {
            return VSConstants.S_OK;
        }

        public int OnRegisterView(IVsTextView pView)
        {
            return VSConstants.S_OK;
        }

        public int OnReplaceAllInFilesBegin()
        {
            return VSConstants.S_OK;
        }

        public int OnReplaceAllInFilesEnd()
        {
            return VSConstants.S_OK;
        }

        public int OnUnregisterView(IVsTextView pView)
        {
            return VSConstants.S_OK;
        }

        public int OnUserPreferencesChanged2(VIEWPREFERENCES2[] pViewPrefs, FRAMEPREFERENCES2[] pFramePrefs, LANGPREFERENCES2[] pLangPrefs, FONTCOLORPREFERENCES2[] pColorPrefs)
        {
            if (pLangPrefs != null)
            {
                _preferences.IndentStyle = pLangPrefs[0].IndentStyle;
                _preferences.fAutoListMembers = pLangPrefs[0].fAutoListMembers;
                _preferences.fAutoListParams = pLangPrefs[0].fAutoListParams;
                if (_preferences.fDropdownBar != (_preferences.fDropdownBar = pLangPrefs[0].fDropdownBar))
                {
                    SanScriptCodeWindowManager.ToggleNavigationBar(_preferences.fDropdownBar != 0);
                }
                if (_preferences.fHideAdvancedAutoListMembers != (_preferences.fHideAdvancedAutoListMembers = pLangPrefs[0].fHideAdvancedAutoListMembers))
                {
                    //SanScriptProjectPackage.Instance.RuntimeHost.HideAdvancedMembers = HideAdvancedMembers;
                }
            }
            return VSConstants.S_OK;
        }

        #endregion

        #region Options

        public vsIndentStyle IndentMode
        {
            get
            {
                return _preferences.IndentStyle;
            }
        }

        /// <summary>
        /// Indentation size.
        /// </summary>
        public uint IndentSize
        {
            get { return _preferences.uIndentSize; }
        }

        public bool NavigationBar
        {
            get
            {
                // TODO: When this value changes we need to update all our views
                return _preferences.fDropdownBar != 0;
            }
        }

        public bool HideAdvancedMembers
        {
            get
            {
                return _preferences.fHideAdvancedAutoListMembers != 0;
            }
        }

        public bool AutoListMembers
        {
            get
            {
                return _preferences.fAutoListMembers != 0;
            }
        }

        public bool AutoListParams
        {
            get
            {
                return _preferences.fAutoListParams != 0;
            }
        }


        #endregion
    }
}
