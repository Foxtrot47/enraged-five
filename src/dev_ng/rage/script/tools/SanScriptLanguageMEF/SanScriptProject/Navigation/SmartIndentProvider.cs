﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.Text.Editor;
using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Utilities;

namespace Rockstar.SanScriptProject.Navigation
{
    /// <summary>
    /// Smart indentation provider.
    /// </summary>
    [Export(typeof(ISmartIndentProvider))]
    [ContentType("SanScript")]
    public sealed class SmartIndentProvider : ISmartIndentProvider
    {
        /// <summary>
        /// Create the smart indentor.
        /// </summary>
        /// <param name="textView">Text view.</param>
        /// <returns>A new instance of the smart indentor.</returns>
        public ISmartIndent CreateSmartIndent(ITextView textView)
        {
            return new Indent(textView);
        }

    }
}
