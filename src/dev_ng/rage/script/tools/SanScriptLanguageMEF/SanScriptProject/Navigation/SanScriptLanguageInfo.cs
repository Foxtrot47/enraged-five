﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextManager.Interop;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Editor;
using Microsoft.VisualStudio.ComponentModelHost;
using Microsoft.VisualStudio.Package;

namespace Rockstar.SanScriptProject.Navigation
{
    /// <summary>
    /// Minimal language service.  Implemented directly rather than using the Managed Package
    /// Framework because we don't want to provide colorization services.  Instead we use the
    /// new Visual Studio 2010 APIs to provide these services.  But we still need this to
    /// provide a code window manager so that we can have a navigation bar (actually we don't, this
    /// should be switched over to using our TextViewCreationListener instead).
    /// </summary>
    internal sealed class SanScriptLanguageInfo : IVsLanguageInfo
    {
        #region Private member fields

        private readonly IServiceProvider _serviceProvider;
        private readonly IComponentModel _componentModel;

        #endregion

        #region Public properties

        /// <summary>
        /// Service provider.
        /// </summary>
        public IServiceProvider ServiceProvider { get { return _serviceProvider; } }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="serviceProvider">Service provider.</param>
        public SanScriptLanguageInfo(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _componentModel = serviceProvider.GetService(typeof(SComponentModel)) as IComponentModel;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Adds the navigation area drop-down comboboxes to the code view window.
        /// </summary>
        /// <param name="pCodeWin">Code window.</param>
        /// <param name="ppCodeWinMgr">Code window manager.</param>
        /// <returns>Error code.</returns>
        public int GetCodeWindowManager(IVsCodeWindow pCodeWin, out IVsCodeWindowManager ppCodeWinMgr)
        {
            var model = _serviceProvider.GetService(typeof(SComponentModel)) as IComponentModel;
            var service = model.GetService<IVsEditorAdaptersFactoryService>();

            IVsTextView textView;
            if (ErrorHandler.Succeeded(pCodeWin.GetPrimaryView(out textView)))
            {
                ppCodeWinMgr = new SanScriptCodeWindowManager(pCodeWin, service.GetWpfTextView(textView), _componentModel);

                return VSConstants.S_OK;
            }

            ppCodeWinMgr = null;
            return VSConstants.E_FAIL;
        }

        /// <summary>
        /// Get the file extensions allowed for a code file.
        /// </summary>
        /// <param name="pbstrExtensions">The allowed extensions.</param>
        /// <returns>Error code.</returns>
        public int GetFileExtensions(out string pbstrExtensions)
        {
            // This is the same extension the language service was
            // registered as supporting.
            pbstrExtensions = ".sch;.sc";
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Gets the language name.
        /// </summary>
        /// <param name="bstrName">The language name.</param>
        /// <returns>Error code.</returns>
        public int GetLanguageName(out string bstrName)
        {
            // This is the same name the language service was registered with.
            bstrName = "SanScript";
            return VSConstants.S_OK;
        }

        /// <summary>
        /// GetColorizer is not implemented because we implement colorization using the new managed APIs.
        /// </summary>
        public int GetColorizer(IVsTextLines pBuffer, out IVsColorizer ppColorizer)
        {
            ppColorizer = null;
            return VSConstants.E_FAIL;
        }

        #endregion
    }
}
