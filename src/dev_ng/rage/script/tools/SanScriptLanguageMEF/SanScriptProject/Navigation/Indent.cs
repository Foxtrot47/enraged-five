﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.TextManager.Interop;
using Microsoft.VisualStudio.Text;


namespace Rockstar.SanScriptProject.Navigation
{
    /// <summary>
    /// Smart indentation.
    /// </summary>
    public sealed class Indent : ISmartIndent
    {
        #region Private member fields

        private readonly ITextView m_textView;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="textView">Text view.</param>
        public Indent(ITextView textView)
        {
            m_textView = textView;
        }

        #endregion

        /// <summary>
        /// Get the desired indentation from the previous lines of code.
        /// </summary>
        /// <param name="line">Current line.</param>
        /// <returns>The desired indentation (in columns).</returns>
        public int? GetDesiredIndentation(ITextSnapshotLine line)
        {
            if (SanScriptProjectPackage.Instance.LanguagePreferences.IndentMode == vsIndentStyle.vsIndentStyleSmart)
            {
                string text = SkipToPreviousContent(line);

                if (String.IsNullOrEmpty(text))
                {
                    return 0;
                }

                int indentSize = (int)SanScriptProjectPackage.Instance.LanguagePreferences.IndentSize;
                int count = 0;
                int idx = 0;
                while (idx < text.Length && (text[idx] == ' ' || text[idx] == '\t'))
                {
                    if (text[idx] == ' ')
                    {
                        count++;
                    }
                    else
                    {
                        count += indentSize;
                    }

                    idx++;
                }

                return count;
            }

            return null;
        }

        public void Dispose()
        {

        }

        #region Private helper methods

        /// <summary>
        /// Skip to the previous content (if any) to find text to indent the current line with.
        /// </summary>
        /// <param name="line">Line.</param>
        /// <returns>The previous content. Will return an empty string if no content found.</returns>
        private string SkipToPreviousContent(ITextSnapshotLine line)
        {
            int currentLine = line.LineNumber - 1;
            while (currentLine > 1)
            {
                var previousLine = line.Snapshot.GetLineFromLineNumber(currentLine);
                if (!String.IsNullOrEmpty(previousLine.GetText()))
                {
                    return previousLine.GetText();
                }
                currentLine--;
            }

            return String.Empty;
        }


        #endregion
    }
}
