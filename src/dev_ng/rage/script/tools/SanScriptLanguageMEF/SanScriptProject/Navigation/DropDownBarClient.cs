﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextManager.Interop;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text;
using SanScriptLanguageMEF;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.Text.Tagging;
using System.Windows.Threading;

namespace Rockstar.SanScriptProject.Navigation
{
    class DropDownBarClient : IVsDropdownBarClient
    {
        private const int ModuleComboBoxId = 0;
        private const int MethodComboBoxId = 1;

        private ISanScriptClassifier m_classifier;
        private SortedList<string, ITagSpan<TokenTag>> m_tags;
        private List<ITagSpan<TokenTag>> m_tagSpans;
        private Dispatcher m_dispatcher;
        private IWpfTextView m_textView;

        private delegate void BlankDelegate();

        public DropDownBarClient(IWpfTextView view, ISanScriptClassifier classifier)
        {
            m_textView = view;
            view.Caret.PositionChanged += new EventHandler<CaretPositionChangedEventArgs>(Caret_PositionChanged);
            m_classifier = classifier;
            m_dispatcher = Dispatcher.CurrentDispatcher;
            m_classifier.TagsChanged += new EventHandler<SnapshotSpanEventArgs>(Classifier_TagsChanged);
        }

        void Classifier_TagsChanged(object sender, SnapshotSpanEventArgs e)
        {
            m_tagSpans = SanScriptParser.GetTagSpans(m_classifier.Buffer);
            m_tags = CalculateEntries(m_tagSpans);
            m_dispatcher.BeginInvoke(new BlankDelegate(RefreshCombo), DispatcherPriority.Background);
        }

        /// <summary>
        /// Refresh the combo box.
        /// </summary>
        private void RefreshCombo()
        {
            if (m_dropDownBar != null)
            {
                m_dropDownBar.RefreshCombo(MethodComboBoxId, -1);
            }
        }

        private void Caret_PositionChanged(object sender, CaretPositionChangedEventArgs e)
        {
            if (m_tags == null)
            {
                return;
            }

            for (int i = 0; i < m_tags.Count; i++)
            {
                var span = m_tags.Values[i];

                var endSpan = FindEndTag(span.Span);
                if (endSpan.Start == 0 && endSpan.End == 0)
                {
                    continue;
                }
                    
                try
                {
                    SnapshotSpan functionArea = new SnapshotSpan(e.TextView.TextSnapshot, new Span(span.Span.Start.Position, (endSpan.Start - span.Span.Start.Position) + endSpan.Length));

                    if (functionArea.Contains(new SnapshotSpan(e.NewPosition.BufferPosition, 1)))
                    {
                        m_dispatcher.BeginInvoke(new BlankDelegate(RefreshCombo), DispatcherPriority.Background);
                        break;
                    }
                }
                catch
                {
                }
            }
        }

        private Span FindEndTag(Span span)
        {
            Span foundSpan = Span.FromBounds(0, 0);

            for (int i = 0; i < m_tagSpans.Count;i++ )
            {
                var taggedSpan = m_tagSpans[i];
                if (taggedSpan.Span == span)
                {
                    i++;
                    while (i < m_tagSpans.Count)
                    {
                        taggedSpan = m_tagSpans[i];
                        if (taggedSpan.Tag.m_type == TokenTypes.TOKEN_TYPE_KEYWORD)
                        {
                            string loweredText = taggedSpan.Span.GetText().ToLower();
                            if (loweredText == "endproc" || loweredText == "endfunc" || loweredText == "endscript")
                            {
                                foundSpan = taggedSpan.Span;
                                break;
                            }
                        }
                        i++;
                    }
                }
            }

            return foundSpan;
        }

        private IVsDropdownBar m_dropDownBar;

        public int GetComboAttributes(int iCombo, out uint pcEntries, out uint puEntryType, out IntPtr phImageList)
        {
            if (iCombo == MethodComboBoxId)
            {
                if (m_tags == null)
                {
                    m_tags = CalculateEntries(SanScriptParser.GetTagSpans(m_classifier.Buffer));
                }

                phImageList = IntPtr.Zero;
                puEntryType = (uint)(DROPDOWNENTRYTYPE.ENTRY_TEXT); // | DROPDOWNENTRYTYPE.ENTRY_IMAGE | DROPDOWNENTRYTYPE.ENTRY_ATTR);
                pcEntries = m_tags == null ? 0 : (uint)m_tags.Count;
            }
            else
            {
                phImageList = IntPtr.Zero;
                puEntryType = (uint)(DROPDOWNENTRYTYPE.ENTRY_TEXT); // | DROPDOWNENTRYTYPE.ENTRY_IMAGE | DROPDOWNENTRYTYPE.ENTRY_ATTR);
                pcEntries = 1;
            }

            return VSConstants.S_OK;
        }

        public int GetComboTipText(int iCombo, out string pbstrText)
        {
            pbstrText = null;
            return VSConstants.S_OK;
        }

        public int GetEntryAttributes(int iCombo, int iIndex, out uint pAttr)
        {
            pAttr = 0;

            if (iCombo == MethodComboBoxId)
            {
                pAttr = (uint)(DROPDOWNENTRYTYPE.ENTRY_TEXT); // | DROPDOWNENTRYTYPE.ENTRY_IMAGE | DROPDOWNENTRYTYPE.ENTRY_ATTR);
            }

            return VSConstants.S_OK;
        }

        public int GetEntryImage(int iCombo, int iIndex, out int piImageIndex)
        {
            piImageIndex = 0;
            return VSConstants.S_OK;
        }

        public int GetEntryText(int iCombo, int iIndex, out string ppszText)
        {
            ppszText = null;

            if (iCombo == MethodComboBoxId)
            {
                if (m_tags.Count > 0)
                {
                    ppszText = m_tags.Keys[iIndex];
                }
            }
            else
            {
                ppszText = "(Global Scope)";
            }

            return VSConstants.S_OK;
        }

        public int OnComboGetFocus(int iCombo)
        {
            return VSConstants.S_OK;
        }

        public int OnItemChosen(int iCombo, int iIndex)
        {
            if (iCombo == MethodComboBoxId)
            {
                //m_textView.Caret.MoveTo(m_tags.Values[iIndex].Span.Start);
                CentreAndFocus(m_tags.Values[iIndex].Span.Start);
            }

            return VSConstants.S_OK;
        }

        public int OnItemSelected(int iCombo, int iIndex)
        {
            return VSConstants.S_OK;
        }

        public int SetDropdownBar(IVsDropdownBar pDropdownBar)
        {
            m_dropDownBar = pDropdownBar;
            return VSConstants.S_OK;
        }

        internal void Unregister()
        {

        }

        private void CentreAndFocus(SnapshotPoint snapshot)
        {
            m_textView.Caret.MoveTo(snapshot);
            //m_textView.Caret.MoveTo(new SnapshotPoint(m_textView.TextBuffer.CurrentSnapshot, index));

            m_textView.ViewScroller.EnsureSpanVisible(
                //new SnapshotSpan(m_textView.TextBuffer.CurrentSnapshot, index, 1),
                new SnapshotSpan(snapshot.Snapshot, snapshot.Position, 1),
                EnsureSpanVisibleOptions.AlwaysCenter
            );

            ((System.Windows.Controls.Control)m_textView).Focus();
        }

        /// <summary>
        /// Helper function for calculating all of the drop down entries that are available
        /// in the given suite statement.  Called to calculate both the members of top-level
        /// code and class bodies.
        /// </summary>
        private static SortedList<string, ITagSpan<TokenTag>> CalculateEntries(List<ITagSpan<TokenTag>> tags)
        {
            try
            {
                SortedList<string, ITagSpan<TokenTag>> sortedList = new SortedList<string, ITagSpan<TokenTag>>();
                if (tags != null)
                {
                    for (int i = 0; i < tags.Count; i++)
                    {
                        var tag = tags[i];
                        string text = tag.Span.GetText().ToLower();
                        bool isProcOrFunc = tag.Tag.m_type == TokenTypes.TOKEN_TYPE_KEYWORD && (text == "proc" || text == "func");
                        bool isScript = tag.Tag.m_type == TokenTypes.TOKEN_TYPE_KEYWORD && text == "script";

                        if (isProcOrFunc)
                        {
                            if (tags[i + 1].Tag.m_type == TokenTypes.TOKEN_TYPE_KEYWORD ||
                                tags[i + 1].Tag.m_type == TokenTypes.TOKEN_TYPE_NATIVETYPE)
                            {
                                try
                                {
                                    sortedList.Add(tags[i + 2].Span.GetText(), tag);
                                    i += 2;
                                }
                                catch
                                {
                                    
                                }
                            }
                            else
                            {
                                sortedList.Add(tags[i + 1].Span.GetText(), tag);
                                i++;
                            }
                        }
                        else if (isScript)
                        {
                            sortedList.Add(tag.Span.GetText().ToUpper() + " LOOP", tag);
                        }
                    }
                }

                return sortedList;
            }
            catch
            {
                return new SortedList<string, ITagSpan<TokenTag>>();
            }
        }
    }
}
