﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextManager.Interop;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Package;
using Microsoft.VisualStudio.Text.Operations;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.ComponentModelHost;

namespace Rockstar.SanScriptProject.Navigation
{
    /// <summary>
    /// Implementation of a code window manager that allows us to place drop-down combo boxes to display function names in the code view windows.
    /// </summary>
    class SanScriptCodeWindowManager : IVsCodeWindowManager
    {
        #region Private member fields

        private readonly IVsCodeWindow _window;
        private readonly IWpfTextView _textView;
        private readonly IEditorOperationsFactoryService _editorOperationsFactory;
        private static readonly Dictionary<IWpfTextView, SanScriptCodeWindowManager> _windows = new Dictionary<IWpfTextView, SanScriptCodeWindowManager>();
        private DropDownBarClient _client;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="codeWindow">Code window.</param>
        /// <param name="textView">Text view.</param>
        /// <param name="componentModel">Component model.</param>
        public SanScriptCodeWindowManager(IVsCodeWindow codeWindow, IWpfTextView textView, IComponentModel componentModel)
        {
            _window = codeWindow;
            _textView = textView;
            _editorOperationsFactory = componentModel.GetService<IEditorOperationsFactoryService>();
        }

        #endregion

        #region IVsCodeWindowManager Members

        /// <summary>
        /// Add adorners to the code view window.
        /// </summary>
        /// <returns>Error code.</returns>
        public int AddAdornments()
        {
            _windows[_textView] = this;

            IVsTextView textView;

            if (ErrorHandler.Succeeded(_window.GetPrimaryView(out textView)))
            {
                OnNewView(textView);
            }

            if (ErrorHandler.Succeeded(_window.GetSecondaryView(out textView)))
            {
                OnNewView(textView);
            }

            return AddDropDownBar();
        }

        /// <summary>
        /// When a new view is created.
        /// </summary>
        /// <param name="pView">The view.</param>
        /// <returns>Error code.</returns>
        public int OnNewView(IVsTextView pView)
        {
            // TODO: We pass _textView which may not be right for split buffers, we need
            // to test the case where we split a text file and save it as an existing file?
            //new EditFilter(_analyzer, _textView, pView);
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Remove all adorners.
        /// </summary>
        /// <returns>Error code.</returns>
        public int RemoveAdornments()
        {
            _windows.Remove(_textView);
            return RemoveDropDownBar();
        }

        /// <summary>
        /// Toggle the visibility of the navigation bar.
        /// </summary>
        /// <param name="showNavigationBar">True if the navigation bar is to be visible.</param>
        public static void ToggleNavigationBar(bool showNavigationBar)
        {
            foreach (var keyValue in _windows)
            {
                if (showNavigationBar)
                {
                    ErrorHandler.ThrowOnFailure(keyValue.Value.AddDropDownBar());
                }
                else
                {
                    ErrorHandler.ThrowOnFailure(keyValue.Value.RemoveDropDownBar());
                }
            }
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Add the drop-down combo boxes.
        /// </summary>
        /// <returns>Error code.</returns>
        private int AddDropDownBar()
        {
            var classifier = _textView.TextBuffer.GetAnalysis();
            if (classifier == null)
            {
                return VSConstants.E_FAIL;
            }

            DropDownBarClient dropDown = _client = new DropDownBarClient(_textView, classifier);
            IVsDropdownBarManager manager = (IVsDropdownBarManager)_window;

            IVsDropdownBar dropDownBar;
            int hr = manager.GetDropdownBar(out dropDownBar);
            if (ErrorHandler.Succeeded(hr) && dropDownBar != null)
            {
                hr = manager.RemoveDropdownBar();
                if (!ErrorHandler.Succeeded(hr))
                {
                    return hr;
                }
            }

            return manager.AddDropdownBar(2, dropDown);
        }

        /// <summary>
        /// Remove the drop down bar.
        /// </summary>
        /// <returns></returns>
        private int RemoveDropDownBar()
        {
            if (_client != null)
            {
                IVsDropdownBarManager manager = (IVsDropdownBarManager)_window;
                _client.Unregister();
                _client = null;
                return manager.RemoveDropdownBar();
            }
            return VSConstants.S_OK;
        }

        #endregion
    }
}
