﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;

namespace Rockstar.SanScriptProject
{
    #region Game configuration helper class

    /// <summary>
    /// Game configuration helper class
    /// </summary>
    public class ConfigGameView
    {
        private IConfig m_config;

        /// <summary>
        /// Build directory for the default branch.
        /// </summary>
        public string BuildDir { get { return m_config.Project.DefaultBranch.Build; } }

        /// <summary>
        /// Shader directory for the default branch.
        /// </summary>
        public string ShaderDir { get { return m_config.Project.DefaultBranch.Shaders; } }
        
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="config">Configuration.</param>
        public ConfigGameView(IConfig config)
        {
            m_config = config;
        }
    }

    #endregion

    #region RageBuilder information helper class

    /// <summary>
    /// RageBuilder information helper class
    /// </summary>
    public class ConfigRagebuilderInfo
    {
        /// <summary>
        /// Path to the RageBuilder executable.
        /// </summary>
        public string Executable { get; set; }

        /// <summary>
        /// Options.
        /// </summary>
        public string Options { get; set; }

        /// <summary>
        /// Target platform.
        /// </summary>
        public string Platform { get; set; }
    }

    #endregion

    /// <summary>
    /// Configuration class. 
    /// </summary>
    public class Configuration
    {
        #region Singleton pattern

        /// <summary>
        /// Instance.
        /// </summary>
        public static Configuration Instance { get; private set; }

        static Configuration()
        {
            Instance = new Configuration();
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Configuration.
        /// </summary>
        public IConfig Config { get; private set; }

        /// <summary>
        /// Game-specific folder information.
        /// </summary>
        public ConfigGameView ConfigGameView { get; private set; }

        /// <summary>
        /// RageBuilder information.
        /// </summary>
        public List<ConfigRagebuilderInfo> ConfigRagebuilderInfo { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        private Configuration()
        {
            Config = ConfigFactory.CreateConfig();

            ConfigGameView = new ConfigGameView(Config);
            ConfigRagebuilderInfo = new List<ConfigRagebuilderInfo>();

            foreach (var target in Config.Project.DefaultBranch.Targets)
            {
                if (target.Value.Enabled)
                {
                    foreach (var kvp in Config.Project.DefaultBranch.PlatformConversionTools)
                    {
                        if (kvp.Value.Platform == target.Key)
                        {
                            ConfigRagebuilderInfo info = new ConfigRagebuilderInfo();
                            info.Platform = kvp.Key.ToString();
                            info.Executable = kvp.Value.ExecutablePath;
                            info.Options = kvp.Value.Arguments;
                            ConfigRagebuilderInfo.Add(info);
                        }
                    }
                }
            }


        }

        #endregion
    }
}
