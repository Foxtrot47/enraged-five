﻿namespace Rockstar.SanScriptProject
{
    /// <summary>
    /// The ContainsUsingEventHandler allows this project to get information from the parser project to determine
    /// if the line where the caret is located contains a USING statement.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Event arguments.</param>
    internal delegate void ContainsUsingEventHandler(object sender, ContainsUsingEventArguments e);
}
