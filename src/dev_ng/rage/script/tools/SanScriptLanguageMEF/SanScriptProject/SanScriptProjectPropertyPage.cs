﻿using System;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Project;
using System.ComponentModel;

namespace Rockstar.SanScriptProject
{
    [ComVisible(true)]
    [Guid("A2F59A82-C68A-40FF-92A0-9B2FEA92AFF8")]
    public class SanScriptProjectPropertyPage : SettingsPage
    {
        private string m_outputPath;
        private string m_commandLine;
        private string m_include;
        private string m_includeDirectories;
        private string m_postCompile;
        private bool m_warningsAsErrors;

        public SanScriptProjectPropertyPage()
        {
            Name = "SanScript Project";
        }

        [Category("Output")]
        [DisplayName("Output Path")]
        [Description("Path to contain compiled scripts.")]
        public string OutputPath
        {
            get { return m_outputPath; }
            set { m_outputPath = value; IsDirty = true; }
        }

        [Category("Build")]
        [DisplayName("Command Line")]
        [Description("Command line parameters for compile.")]
        public string CmdLineParam
        {
            get { return m_commandLine; }
            set { m_commandLine = value; IsDirty = true; }
        }

        [Category("Build")]
        [DisplayName("Include File")]
        [Description("Include file.")]
        public string Include
        {
            get { return m_include; }
            set { m_include = value; IsDirty = true; }
        }

        [Category("Build")]
        [DisplayName("Include Directories")]
        [Description("Include directories, separated by ';' character.")]
        public string IncludePath
        {
            get { return m_includeDirectories; }
            set { m_includeDirectories = value; IsDirty = true; }
        }

        [Category("Build")]
        [DisplayName("Warnings as Errors")]
        [Description("Treat warnings as errors.")]
        public bool WarningsAsErrors
        {
            get { return m_warningsAsErrors; }
            set { m_warningsAsErrors = value; IsDirty = true; }
        }

        [Category("Post-Build")]
        [DisplayName("Post-Compile Command")]
        [Description("Path to post-compile batch file.")]
        public string PostCompilePath
        {
            get { return m_postCompile; }
            set { m_postCompile = value; IsDirty = true; }
        }

        protected override void BindProperties()
        {
            m_outputPath = GetConfigProperty("OutputPath");
            m_commandLine = GetConfigProperty("CmdLineParams");

            m_include = GetConfigProperty("Include");
            //  Remove the -include from the m_include variable
            if (m_include.StartsWith("-include"))
            {
                m_include = m_include.Substring("-include".Length);
                //  Also remove any leading spaces
                m_include = m_include.TrimStart(' ');
            }
            m_includeDirectories = GetConfigProperty("IncludePath");
            //  Remove the -ipath from the m_includeDirectories variable
            if (m_includeDirectories.StartsWith("-ipath"))
            {
                m_includeDirectories = m_includeDirectories.Substring("-ipath".Length);
                //  Also remove any leading spaces
                m_includeDirectories = m_includeDirectories.TrimStart(' ');
            }

            m_postCompile = GetConfigProperty("PostCompilePath");
            m_warningsAsErrors = (GetConfigProperty("WarningsAsErrors") == "-werror" ? true : false);
        }

        protected override int ApplyChanges()
        {
            SetConfigProperty("OutputPath", m_outputPath);
            SetConfigProperty("CmdLineParams", m_commandLine);

            //  If the m_include variable does not already contain it, add -include to the start
            if (!m_include.StartsWith("-include"))
            {
                SetConfigProperty("Include", "-include " + m_include);
            }
            else
            {
                SetConfigProperty("Include", m_include);
            }
            //  If the m_includeDirectories variable does not already contain it, add -ipath to the start
            if (!m_includeDirectories.StartsWith("-ipath"))
            {
                SetConfigProperty("IncludePath", "-ipath " + m_includeDirectories);
            }
            else
            {
                SetConfigProperty("IncludePath", m_includeDirectories);
            }

            SetConfigProperty("PostCompilePath", m_postCompile);
            SetConfigProperty("WarningsAsErrors", m_warningsAsErrors ? "-werror" : "");

            IsDirty = false;

            return VSConstants.S_OK;
        }

    }
}