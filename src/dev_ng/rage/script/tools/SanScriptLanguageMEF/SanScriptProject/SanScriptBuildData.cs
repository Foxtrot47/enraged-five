﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;
using Microsoft.VisualStudio.Shell;
using Microsoft.Build.Framework;
using System.Diagnostics;
using Microsoft.Build.Utilities;
using Microsoft.VisualStudio.CommandBars;
using RSG.Pipeline.BuildScript;

namespace Rockstar.SanScriptProject
{
    class SanScriptBuildData : IProjectData
    {
        #region Private member fields

        private SanScriptCompileTask m_compileTask;
        private bool m_cleanBuild;
        private List<RSG.Platform.Platform> m_enabledPlatforms;
        private Dictionary<RSG.Platform.Platform, string> m_cachedProperties;

        #endregion

        #region Public properties

        public IBuildEngine BuildEngine { get; private set; }

        public string OutputDirectory { get; private set; }

        public string ProjectFileName { get; private set; }

        public string Custom { get; private set; }

        public string XmlIncludeFile { get { return String.Empty; } }

        public string[] XmlIncludePaths { get { return new string[] { }; } }

        public string ConfigurationName { get; private set; }

        public string CompilerExecutable { get; private set; }

        public string ResourceExecutable { get; private set; }

        public string ResourceExecutable64 { get; private set; }

        public string PostCompileCommand { get; private set; }

        public bool WarningsAsErrors { get { return false; } }

        public bool FullBuild { get { return true; } }

        public bool UsesPostBuild { get { return false; } }

        public List<RSG.Platform.Platform> EnabledPlatforms  { get { return m_enabledPlatforms; } }

        public bool UseIdeMonitor
        {
            get
            {
                try
                {
                    DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
                    CommandBar menuCommandBar = ((CommandBars)dte.CommandBars)["MenuBar"];
                    var incrediBuildControl = menuCommandBar.Controls["IncrediBuild"];
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        #endregion

        #region Constructor

        internal SanScriptBuildData(bool cleanBuild, List<RSG.Platform.Platform> enabledPlatforms)
        {
            m_cleanBuild = cleanBuild;
            m_enabledPlatforms = enabledPlatforms;
            m_cachedProperties = new Dictionary<RSG.Platform.Platform, string>();
        }

        #endregion

        #region Public methods

        public void Accept(IBuildVisitor visitor)
        {
            visitor.Visit(this);
        }

        public IList<string> GetFiles()
        {
            return m_compileTask.FilesToBuild();
        }

        public void PreBuild()
        {
            DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
            Array activeSolutionProjects = dte.ActiveSolutionProjects as Array;
            Project project = null;

            if (activeSolutionProjects != null && activeSolutionProjects.Length > 0)
            {
                project = activeSolutionProjects.GetValue(0) as Project;
                if (project != null)
                {
                    SanScriptProjectNode projectNode = project.Object as SanScriptProjectNode;
                    if (projectNode != null)
                    {
                        m_compileTask = new SanScriptCompileTask();
                        ProjectFileName = project.FullName;
                        ConfigurationName = project.ConfigurationManager.ActiveConfiguration.ConfigurationName;
                        ResourceExecutable = projectNode.GetProjectProperty("ResourceExe", true);
                        ResourceExecutable64 = projectNode.GetProjectProperty("ResourceExe64", true);
                        CompilerExecutable = projectNode.GetProjectProperty("CompilerExe", true);
                        PostCompileCommand = projectNode.GetProjectProperty("PostCompilePath", true);
                        OutputDirectory = projectNode.GetProjectProperty("OutputPath", true);

                        m_compileTask.Command = projectNode.GetProjectProperty("CompilerExe", true);
                        m_compileTask.OutputPath = OutputDirectory;
                        m_compileTask.Arguments = projectNode.GetProjectProperty("CmdLineParams", true);
                        m_compileTask.Arguments += " " + projectNode.GetProjectProperty("Include", true);
                        m_compileTask.Arguments += " " + projectNode.GetProjectProperty("IncludePath", true);
                        m_compileTask.Arguments += " " + projectNode.GetProjectProperty("WarningsAsErrors", true);
                        Custom = m_compileTask.Arguments;

                        //  Have to set the dummy build engine or the logging falls over since we aren't running from MSBuild.
                        BuildEngine = new SanScriptBuildEngine(ProjectFileName);
                        m_compileTask.BuildEngine = BuildEngine;

                        m_compileTask.PreBuild(m_cleanBuild);
                    }
                }
            }
        }

        public void PostBuild(int exitCode)
        {
            if (exitCode == 0)
            {
                //Execute the post compile step for the project.
                ProcessStartInfo psInfo = new ProcessStartInfo(PostCompileCommand);
                psInfo.Arguments = ProjectFileName + " -copycache";
                psInfo.UseShellExecute = false;
                psInfo.CreateNoWindow = true;
                psInfo.RedirectStandardOutput = true;
                psInfo.RedirectStandardError = true;

                System.Diagnostics.Process postCompileStep = new System.Diagnostics.Process();
                postCompileStep.StartInfo = psInfo;
                postCompileStep.OutputDataReceived += new DataReceivedEventHandler(PostCompileOutputHandler);
                postCompileStep.ErrorDataReceived += new DataReceivedEventHandler(PostCompileOutputHandler);

                m_compileTask.Log.LogMessage("Executing post-compile step...");
                postCompileStep.Start();

                postCompileStep.BeginErrorReadLine();
                postCompileStep.BeginOutputReadLine();

                postCompileStep.WaitForExit();
                m_compileTask.Log.LogMessage("Post-compile step has completed.");
            }
            else
            {
                if (exitCode == 3)
                {
                    m_compileTask.Log.LogMessage("Build has been aborted.");
                }
                else
                {
                    m_compileTask.Log.LogMessage("There were errors generated during compilation.");
                }
            }
        }

        /// <summary>
        /// Acquires the platform extension based on the project properties.
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public string GetPlatformExtension(RSG.Platform.Platform platform)
        {
            if (m_cachedProperties.ContainsKey(platform) == true)
            {
                return m_cachedProperties[platform];
            }
            else
            {
                DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
                Array activeSolutionProjects = dte.ActiveSolutionProjects as Array;
                Project project = activeSolutionProjects.GetValue(0) as Project;
                if (project != null)
                {
                    SanScriptProjectNode projectNode = project.Object as SanScriptProjectNode;
                    if (projectNode != null)
                    {
                        string platformName = platform.ToString();
                        m_cachedProperties.Add(platform, projectNode.GetProjectProperty(platformName + "ResourceExtension", true));
                        return m_cachedProperties[platform];
                    }
                }
            }

            return null;
        }
        
        #endregion

        #region Private event handlers


        /// <summary>
        /// Output handler for the post compile step.
        /// </summary>
        /// <param name="sendingProcess"></param>
        /// <param name="outLine"></param>
        private void PostCompileOutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            // Collect the sort command output. 
            if (!String.IsNullOrEmpty(outLine.Data))
            {
                string output = outLine.Data;  //Add a new line.
                m_compileTask.Log.LogMessage(output);
            }
        }

        #endregion
    }
}
