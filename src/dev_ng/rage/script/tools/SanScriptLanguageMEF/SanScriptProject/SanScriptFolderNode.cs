﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell.Interop;
using VsCommands = Microsoft.VisualStudio.VSConstants.VSStd97CmdID;

namespace Rockstar.SanScriptProject
{
    public class SanScriptFolderNode : FolderNode
    {
        public SanScriptFolderNode(SanScriptProjectNode root, string relativePath, ProjectElement element) : base(root, relativePath, element)
        {

        }

        protected override int AddNewFolder()
        {
            // Check out the project file.
            if (!this.ProjectMgr.QueryEditProjectFile(false))
            {
                throw Marshal.GetExceptionForHR(VSConstants.OLE_E_PROMPTSAVECANCELLED);
            }

            try
            {
                // Generate a new folder name
                string newFolderName;
                ErrorHandler.ThrowOnFailure(this.ProjectMgr.GenerateUniqueItemName(this.ID, String.Empty, String.Empty, out newFolderName));

                // create the project part of it, the project file
                HierarchyNode child = this.ProjectMgr.CreateFolderNodes(Path.Combine(this.VirtualNodeName, newFolderName));

                // If we are in automation mode then skip the ui part which is about renaming the folder
                if (!Utilities.IsInAutomationFunction(this.ProjectMgr.Site))
                {
                    IVsUIHierarchyWindow uiWindow = UIHierarchyUtilities.GetUIHierarchyWindow(this.ProjectMgr.Site, SolutionExplorer);
                    // This happens in the context of adding a new folder.
                    // Since we are already in solution explorer, it is extremely unlikely that we get a null return.
                    // If we do, the newly created folder will not be selected, and we will not attempt the rename
                    // command (since we are selecting the wrong item).                        
                    if (uiWindow != null)
                    {
                        // we need to get into label edit mode now...
                        // so first select the new guy...
                        ErrorHandler.ThrowOnFailure(uiWindow.ExpandItem(this.ProjectMgr, child.ID, EXPANDFLAGS.EXPF_SelectItem));
                        // them post the rename command to the shell. Folder verification and creation will
                        // happen in the setlabel code...
                        IVsUIShell shell = this.ProjectMgr.Site.GetService(typeof(SVsUIShell)) as IVsUIShell;

                        Debug.Assert(shell != null, "Could not get the ui shell from the project");
                        if (shell == null)
                        {
                            return VSConstants.E_FAIL;
                        }

                        object dummy = null;
                        Guid cmdGroup = Microsoft.VisualStudio.Shell.VsMenus.guidStandardCommandSet97;
                        ErrorHandler.ThrowOnFailure(shell.PostExecCommand(ref cmdGroup, (uint)VsCommands.Rename, 0, ref dummy));
                    }
                }
            }
            catch (COMException e)
            {
                Trace.WriteLine("Exception : " + e.Message);
                return e.ErrorCode;
            }

            return VSConstants.S_OK;
        }

        /// <summary>
        /// Override this method to add ability to add files as links
        /// </summary>
        /// <param name="addType">Add type (add new or add existing)</param>
        /// <returns>VSConstants.S_OK if it was successful</returns>
        protected override int AddItemToHierarchy(HierarchyAddType addType)
        {
            CCITracing.TraceCall();
            IVsAddProjectItemDlg addItemDialog;

            string strFilter = "SanScript Script Files (*.sc);*.sc";
            int iDontShowAgain;
            uint uiFlags;
            IVsProject3 project = (IVsProject3)this.ProjectMgr;

            string strBrowseLocations = Path.GetDirectoryName(this.ProjectMgr.BaseURI.Uri.LocalPath);

            Guid projectGuid = this.ProjectMgr.ProjectGuid;

            addItemDialog = this.GetService(typeof(IVsAddProjectItemDlg)) as IVsAddProjectItemDlg;

            if (addType == HierarchyAddType.AddExistingItem)
            {
                uiFlags = (uint)(__VSADDITEMFLAGS.VSADDITEM_AddExistingItems |
                                    __VSADDITEMFLAGS.VSADDITEM_AllowMultiSelect |
                                    __VSADDITEMFLAGS.VSADDITEM_AllowStickyFilter |
                                    __VSADDITEMFLAGS.VSADDITEM_ShowLocationField);
            }
            else
            {
                uiFlags = (uint)(__VSADDITEMFLAGS.VSADDITEM_AddNewItems |
                                __VSADDITEMFLAGS.VSADDITEM_SuggestTemplateName |
                                __VSADDITEMFLAGS.VSADDITEM_AllowHiddenTreeView |
                                __VSADDITEMFLAGS.VSADDITEM_ShowLocationField);
            }

            int hResult = addItemDialog.AddProjectItemDlg(this.ID, ref projectGuid, project,
                uiFlags, null, null, ref strBrowseLocations, ref strFilter, out iDontShowAgain);

            ErrorHandler.ThrowOnFailure(hResult);

            return VSConstants.S_OK;
        }
    }
}
