﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using EnvDTE;

using Microsoft.Build.Evaluation;
using MSBuild = Microsoft.Build.Evaluation;

using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using VsCommands = Microsoft.VisualStudio.VSConstants.VSStd97CmdID;

using RSG.Base.Configuration;

namespace Rockstar.SanScriptProject
{
    public class SanScriptProjectNode : ProjectNode
    {
        internal static int imageIndex;
        public override int ImageIndex
        {
            get
            {
                return imageIndex;
            }
        }

        private static string addFileDir = null;

        private SanScriptProjectPackage m_package;
        private static ImageList m_imageList;
        
        static SanScriptProjectNode()
        {
            m_imageList = new ImageList();
            m_imageList.Images.Add("SanScriptProject", new Bitmap(typeof(SanScriptProjectNode).Assembly.GetManifestResourceStream("Rockstar.SanScriptProject.Resources.SanScriptProjectNode.bmp")));
            m_imageList.Images.Add("SanScriptSource", new Bitmap(typeof(SanScriptProjectNode).Assembly.GetManifestResourceStream("Rockstar.SanScriptProject.Resources.SanScriptSourceNode.bmp")));
            m_imageList.Images.Add("SanScriptHeader", new Bitmap(typeof(SanScriptProjectNode).Assembly.GetManifestResourceStream("Rockstar.SanScriptProject.Resources.SanScriptHeaderNode.bmp")));
        }

        public SanScriptProjectNode(SanScriptProjectPackage package)
        {
            m_package = package;
            imageIndex = ImageHandler.ImageList.Images.Count;

            foreach (Image img in m_imageList.Images)
            {
                ImageHandler.AddImage(img);
            }

            //  Enable deleting files in project
            CanProjectDeleteItems = true;
        }

        public override Guid ProjectGuid
        {
            get
            {
                return GuidList.guidSanScriptProjectFactory;
            }
        }

        public override string ProjectType
        {
            get
            {
                return "SanScriptProjectType";
            }
        }

        public override void OnOpenItem(string fullPathToSourceFile)
        {

        }

        public override void AddFileFromTemplate(string source, string target)
        {
            this.FileTemplateProcessor.UntokenFile(source, target);
            this.FileTemplateProcessor.Reset();
        }
        
        /// <summary>
        /// Tells us if the passed in filename is a script file
        /// </summary>
        /// <param name="filename">The filename to check</param>
        /// <returns>true if a script file, otherwise false</returns>
        private bool IsScriptFile(string filename)
        {
            return (Path.GetExtension(filename).ToLower() == ".sch" ||
                    Path.GetExtension(filename).ToLower() == ".sc");
        }

        /// <summary>
        /// Override this function to provide ItemType and SubType for files
        /// as doing this in the vstemplate doesn't seem to work.
        /// </summary>
        /// <param name="file">The complete file name, with path</param>
        /// <returns>A new ProjectElement to add to the project</returns>
        protected override ProjectElement AddFileToMsBuild(string file)
        {
            ProjectElement newItem;
            //  Make filename relative
            string itemPath = BaseURI.MakeRelative(new Url(file));

            //  Script files set to Compile, everything else set to None
            if (IsScriptFile(itemPath))
            {
                newItem = this.CreateMsBuildFileItem(itemPath, ProjectFileConstants.Compile);
                newItem.SetMetadata(ProjectFileConstants.SubType, null);
            }
            else
            {
                newItem = this.CreateMsBuildFileItem(itemPath, ProjectFileConstants.None);
                newItem.SetMetadata(ProjectFileConstants.SubType, null);
            }

            return newItem;
        }

        protected override ProjectElement AddFolderToMsBuild(string folder)
        {
            ProjectElement newItem = CreateMsBuildFileItem(folder, ProjectFileConstants.Folder);

            return newItem;
        }

        public override HierarchyNode CreateFolderNodes(string path)
        {
            if (String.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            return GetParentNode(path);
        }

        private HierarchyNode GetParentNode(string path)
        {
            string[] parts;
            HierarchyNode curParent;

            parts = path.Split(Path.DirectorySeparatorChar);
            path = String.Empty;
            curParent = this;

            // now we have an array of subparts....
            for (int i = 0; i < parts.Length; i++)
            {
                if (parts[i].Length > 0)
                {
                    path += parts[i] + "\\";
                    curParent = VerifySubFolderExists(path, curParent);
                }
            }

            return curParent;
        }

        protected override FolderNode VerifySubFolderExists(string path, HierarchyNode parent)
        {
            FolderNode folderNode = null;

            //  Check if the folder exists already
            ProjectElement element = null;
            foreach (MSBuild.ProjectItem folder in BuildProject.GetItems(ProjectFileConstants.Folder))
            {
                if (String.Compare(folder.EvaluatedInclude.TrimEnd('\\'), path.TrimEnd('\\'), StringComparison.OrdinalIgnoreCase) == 0)
                {
                    element = new ProjectElement(this, folder, false);
                    break;
                }
            }

            if (element == null)
            {
                //  Folder does not exist so create it in the project .ssproj file
                element = AddFolderToMsBuild(path);
            }

            //  Try and find the folder in the solution project
            folderNode = parent.FindChildByProjectElement(element) as FolderNode;
            //  If we can't find it we need to add it
            if (folderNode == null)
            {
                folderNode = CreateFolderNode(path, element);
                parent.AddChild(folderNode);
            }

            return folderNode;
        }

        protected override int AddNewFolder()
        {
            // Check out the project file.
            if (!this.ProjectMgr.QueryEditProjectFile(false))
            {
                throw Marshal.GetExceptionForHR(VSConstants.OLE_E_PROMPTSAVECANCELLED);
            }

            try
            {
                // Generate a new folder name
                string newFolderName;
                ErrorHandler.ThrowOnFailure(this.ProjectMgr.GenerateUniqueItemName(this.ID, String.Empty, String.Empty, out newFolderName));

                // create the project part of it, the project file
                HierarchyNode child = this.ProjectMgr.CreateFolderNodes(Path.Combine(this.VirtualNodeName, newFolderName));

                // If we are in automation mode then skip the ui part which is about renaming the folder
                if (!Utilities.IsInAutomationFunction(this.ProjectMgr.Site))
                {
                    IVsUIHierarchyWindow uiWindow = UIHierarchyUtilities.GetUIHierarchyWindow(this.ProjectMgr.Site, SolutionExplorer);
                    // This happens in the context of adding a new folder.
                    // Since we are already in solution explorer, it is extremely unlikely that we get a null return.
                    // If we do, the newly created folder will not be selected, and we will not attempt the rename
                    // command (since we are selecting the wrong item).                        
                    if (uiWindow != null)
                    {
                        // we need to get into label edit mode now...
                        // so first select the new guy...
                        ErrorHandler.ThrowOnFailure(uiWindow.ExpandItem(this.ProjectMgr, child.ID, EXPANDFLAGS.EXPF_SelectItem));
                        // them post the rename command to the shell. Folder verification and creation will
                        // happen in the setlabel code...
                        IVsUIShell shell = this.ProjectMgr.Site.GetService(typeof(SVsUIShell)) as IVsUIShell;

                        Debug.Assert(shell != null, "Could not get the ui shell from the project");
                        if (shell == null)
                        {
                            return VSConstants.E_FAIL;
                        }

                        object dummy = null;
                        Guid cmdGroup = Microsoft.VisualStudio.Shell.VsMenus.guidStandardCommandSet97;
                        ErrorHandler.ThrowOnFailure(shell.PostExecCommand(ref cmdGroup, (uint)VsCommands.Rename, 0, ref dummy));
                    }
                }
            }
            catch (COMException e)
            {
                Trace.WriteLine("Exception : " + e.Message);
                return e.ErrorCode;
            }

            return VSConstants.S_OK;
        }

        protected internal override FolderNode CreateFolderNode(string path, ProjectElement element)
        {
            SanScriptFolderNode newFolderNode = new SanScriptFolderNode(this, path, element);
            return newFolderNode;
        }

        public override FileNode CreateFileNode(ProjectElement item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            SanScriptFileNode newFileNode = new SanScriptFileNode(this, item);

            return newFileNode;
        }

        /// <summary>
        /// Add a file node to the hierarchy
        /// </summary>
        /// <param name="item">msbuild item to add</param>
        /// <param name="parentNode">Parent Node</param>
        /// <returns>Added node</returns>
        private HierarchyNode AddFileNodeToNode(MSBuild.ProjectItem item)
        {
            ProjectElement element = new ProjectElement(this, item, false);
            FileNode node = this.CreateFileNode(element);
            HierarchyNode parentNode = this;

            //  If this item specifies a folder then add it to that parent
            string folder = element.GetMetadata(ProjectFileConstants.Folder);

            if (folder == null || folder.Length == 0)
            {
                //  No folder specified, add as child of passed in parent node
                parentNode.AddChild(node);
            }
            else
            {
                //  Need to find the folder
                HierarchyNode folderNode = GetParentNode(folder);
                if (folderNode != null)
                {
                    //  Add to the folder if it was found
                    folderNode.AddChild(node);
                }
                else
                {
                    //  If the folder was not found then just add it to the passed in parent node
                    parentNode.AddChild(node);
                }
            }

            return node;
        }

        /// <summary>
        /// Override this method to add ability to add files as links
        /// </summary>
        /// <param name="addType">Add type (add new or add existing)</param>
        /// <returns>VSConstants.S_OK if it was successful</returns>
        protected override int AddItemToHierarchy(HierarchyAddType addType)
        {
            CCITracing.TraceCall();
            IVsAddProjectItemDlg addItemDialog;

            string strFilter = "SanScript Script Files (*.sc);*.sc";
            int iDontShowAgain;
            uint uiFlags;
            IVsProject3 project = (IVsProject3)this.ProjectMgr;

            string strBrowseLocations = Path.GetDirectoryName(this.ProjectMgr.BaseURI.Uri.LocalPath);

            Guid projectGuid = this.ProjectMgr.ProjectGuid;

            addItemDialog = this.GetService(typeof(IVsAddProjectItemDlg)) as IVsAddProjectItemDlg;

            if (addType == HierarchyAddType.AddExistingItem)
            {
                uiFlags = (uint)(__VSADDITEMFLAGS.VSADDITEM_AddExistingItems |
                                __VSADDITEMFLAGS.VSADDITEM_AllowMultiSelect |
                                __VSADDITEMFLAGS.VSADDITEM_AllowStickyFilter |
                                __VSADDITEMFLAGS.VSADDITEM_ShowLocationField);
            }
            else
            {
                uiFlags = (uint)(__VSADDITEMFLAGS.VSADDITEM_AddNewItems |
                                __VSADDITEMFLAGS.VSADDITEM_SuggestTemplateName | 
                                __VSADDITEMFLAGS.VSADDITEM_AllowHiddenTreeView | 
                                __VSADDITEMFLAGS.VSADDITEM_ShowLocationField);
            }

            int hResult = addItemDialog.AddProjectItemDlg(this.ID, ref projectGuid, project,
                uiFlags, null, null, ref strBrowseLocations, ref strFilter, out iDontShowAgain);

            ErrorHandler.ThrowOnFailure(hResult);

            return VSConstants.S_OK;
        }

        /// <summary>
        /// Executes a wizard.
        /// </summary>
        /// <param name="parentNode">The node to which the wizard should add item(s).</param>
        /// <param name="itemName">The name of the file that the user typed in.</param>
        /// <param name="wizardToRun">The name of the wizard to run.</param>
        /// <param name="dlgOwner">The owner of the dialog box.</param>
        /// <returns>A VSADDRESULT enum value describing success or failure.</returns>
        public override VSADDRESULT RunWizard(HierarchyNode parentNode, string itemName, string wizardToRun, IntPtr dlgOwner)
        {
            Debug.Assert(!String.IsNullOrEmpty(itemName), "The Add item dialog was passing in a null or empty item to be added to the hierrachy.");
            Debug.Assert(!String.IsNullOrEmpty(this.ProjectFolder), "The Project Folder is not specified for this project.");

            if (parentNode == null)
            {
                throw new ArgumentNullException("parentNode");
            }

            if (String.IsNullOrEmpty(itemName))
            {
                throw new ArgumentNullException("itemName");
            }

            // We just validate for length, since we assume other validation has been performed by the dlgOwner.
            if (itemName.Length + 1 > NativeMethods.MAX_PATH)
            {
                string errorMessage = String.Format(CultureInfo.CurrentCulture, SR.GetString(SR.PathTooLong, CultureInfo.CurrentUICulture), itemName);
                if (!Utilities.IsInAutomationFunction(this.Site))
                {
                    string title = null;
                    OLEMSGICON icon = OLEMSGICON.OLEMSGICON_CRITICAL;
                    OLEMSGBUTTON buttons = OLEMSGBUTTON.OLEMSGBUTTON_OK;
                    OLEMSGDEFBUTTON defaultButton = OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST;
                    VsShellUtilities.ShowMessageBox(this.Site, title, errorMessage, icon, buttons, defaultButton);
                    return VSADDRESULT.ADDRESULT_Failure;
                }
                else
                {
                    throw new InvalidOperationException(errorMessage);
                }
            }


            // Build up the ContextParams safearray
            //  [0] = Wizard type guid  (bstr)
            //  [1] = Project name  (bstr)
            //  [2] = ProjectItems collection (bstr)
            //  [3] = Local Directory (bstr)
            //  [4] = Filename the user typed (bstr)
            //  [5] = Product install Directory (bstr)
            //  [6] = Run silent (bool)

            object[] contextParams = new object[7];
            contextParams[0] = EnvDTE.Constants.vsWizardAddItem;
            contextParams[1] = this.Caption;
            object automationObject = parentNode.GetAutomationObject();
            if (automationObject is EnvDTE.Project)
            {
                EnvDTE.Project project = (EnvDTE.Project)automationObject;
                contextParams[2] = project.ProjectItems;
            }
            else
            {
                // This would normally be a folder unless it is an item with subitems
                EnvDTE.ProjectItem item = (EnvDTE.ProjectItem)automationObject;
                contextParams[2] = item.ProjectItems;
            }

            contextParams[3] = itemName.Substring(0, itemName.LastIndexOf("\\"));

            contextParams[4] = itemName;

            object objInstallationDir = null;
            IVsShell shell = (IVsShell)this.GetService(typeof(IVsShell));
            ErrorHandler.ThrowOnFailure(shell.GetProperty((int)__VSSPROPID.VSSPROPID_InstallDirectory, out objInstallationDir));
            string installDir = (string)objInstallationDir;

            // append a '\' to the install dir to mimic what the shell does (though it doesn't add one to destination dir)
            if (!installDir.EndsWith("\\", StringComparison.Ordinal))
            {
                installDir += "\\";
            }

            contextParams[5] = installDir;

            contextParams[6] = true;

            IVsExtensibility3 ivsExtensibility = this.GetService(typeof(IVsExtensibility)) as IVsExtensibility3;
            Debug.Assert(ivsExtensibility != null, "Failed to get IVsExtensibility3 service");
            if (ivsExtensibility == null)
            {
                return VSADDRESULT.ADDRESULT_Failure;
            }

            // Determine if we have the trust to run this wizard.
            IVsDetermineWizardTrust wizardTrust = this.GetService(typeof(SVsDetermineWizardTrust)) as IVsDetermineWizardTrust;
            if (wizardTrust != null)
            {
                Guid guidProjectAdding = Guid.Empty;
                ErrorHandler.ThrowOnFailure(wizardTrust.OnWizardInitiated(wizardToRun, ref guidProjectAdding));
            }

            int wizResultAsInt;
            try
            {
                Array contextParamsAsArray = contextParams;

                int result = ivsExtensibility.RunWizardFile(wizardToRun, (int)dlgOwner, ref contextParamsAsArray, out wizResultAsInt);
            }
            finally
            {
                if (wizardTrust != null)
                {
                    ErrorHandler.ThrowOnFailure(wizardTrust.OnWizardCompleted());
                }
            }

            EnvDTE.wizardResult wizardResult = (EnvDTE.wizardResult)wizResultAsInt;

            return VSADDRESULT.ADDRESULT_Success;
        }

        public override int AddItemWithSpecific(uint itemIdLoc, VSADDITEMOPERATION op, string itemName, uint filesToOpen, string[] files, IntPtr dlgOwner, uint editorFlags, ref Guid editorType, string physicalView, ref Guid logicalView, VSADDRESULT[] result)
        {
            bool openFiles = false;

            if (files == null || result == null || files.Length == 0 || result.Length == 0)
            {
                return VSConstants.E_INVALIDARG;
            }

            // Locate the node to be the container node for the file(s) being added
            // only projectnode or foldernode and file nodes are valid container nodes
            // We need to locate the parent since the item wizard expects the parent to be passed.
            HierarchyNode n = this.NodeFromItemId(itemIdLoc);
            if (n == null)
            {
                return VSConstants.E_INVALIDARG;
            }

            while ((!(n is ProjectNode)) && (!(n is FolderNode)) && (!this.CanFileNodesHaveChilds || !(n is FileNode)))
            {
                n = n.Parent;
            }
            Debug.Assert(n != null, "We should at this point have either a ProjectNode or FolderNode or a FileNode as a container for the new filenodes");

            if (op == VSADDITEMOPERATION.VSADDITEMOP_OPENFILE)
            {
                //  Add the files
                foreach (string file in files)
                {
                    //  Add it to the hierarchy
                    AddNewFileNodeToHierarchy(n, file);
                    result[0] = VSADDRESULT.ADDRESULT_Success;
                }
            }
            else if (op == VSADDITEMOPERATION.VSADDITEMOP_RUNWIZARD)
            {
                addFileDir = itemName.Substring(0, itemName.LastIndexOf("\\") + 1);
                result[0] = RunWizard(n, itemName, files[0], dlgOwner);
                //  Open the files when done
                openFiles = true;
            }
            else
            {
                //  Create the file from the template
                AddFileFromTemplate(files[0], addFileDir + itemName);
                //  Add it to the hierarchy
                AddNewFileNodeToHierarchy(n, addFileDir + itemName);
                result[0] = VSADDRESULT.ADDRESULT_Success;
                return VSConstants.S_OK;
            }

            //  Notify listeners that items were added
            n.OnItemsAppended(n);

            // Open files if they are new
            if (openFiles)
            {
                if (!String.IsNullOrEmpty(itemName))
                {
                    HierarchyNode child = this.FindChild(itemName);
                    Debug.Assert(child != null, "We should have been able to find the new element in the hierarchy");
                    if (child != null)
                    {
                        IVsWindowFrame frame;
                        if (editorType == Guid.Empty)
                        {
                            Guid view = Guid.Empty;
                            ErrorHandler.ThrowOnFailure(this.OpenItem(child.ID, ref view, IntPtr.Zero, out frame));
                        }
                        else
                        {
                            ErrorHandler.ThrowOnFailure(this.OpenItemWithSpecific(child.ID, editorFlags, ref editorType, physicalView, ref logicalView, IntPtr.Zero, out frame));
                        }

                        // Show the window frame in the UI and make it the active window
                        if (frame != null)
                        {
                            ErrorHandler.ThrowOnFailure(frame.Show());
                        }
                    }
                }
            }

            return VSConstants.S_OK;
        }

        /// <summary>
        /// Adds a new file node to the hierarchy.
        /// </summary>
        /// <param name="parentNode">The parent of the new fileNode</param>
        /// <param name="fileName">The file name</param>
        protected override void AddNewFileNodeToHierarchy(HierarchyNode parentNode, string fileName)
        {
            if (parentNode == null)
            {
                throw new ArgumentNullException("parentNode");
            }

            HierarchyNode child;

            //Create and add new filenode to the project
            child = this.CreateFileNode(fileName);
            if (parentNode.ItemNode != null)
            {
                child.ItemNode.SetMetadata(ProjectFileConstants.Folder, parentNode.ItemNode.GetMetadata(ProjectFileConstants.Include));
            }

            parentNode.AddChild(child);

            this.Tracker.OnItemAdded(fileName, VSADDFILEFLAGS.VSADDFILEFLAGS_NoFlags);
        }

        /// <summary>
        /// Loads file items from the project file into the hierarchy.
        /// </summary>
        protected internal override void ProcessFiles()
        {
            List<String> subitemsKeys = new List<String>();
            Dictionary<String, MSBuild.ProjectItem> subitems = new Dictionary<String, MSBuild.ProjectItem>();

            // Define a set for our build items. The value does not really matter here.
            Dictionary<String, MSBuild.ProjectItem> items = new Dictionary<String, MSBuild.ProjectItem>();

            // Process Files
            foreach (MSBuild.ProjectItem item in this.BuildProject.Items)
            {
                // Ignore the item if it is a reference or folder
                if (this.FilterItemTypeToBeAddedToHierarchy(item.ItemType))
                    continue;

                // MSBuilds tasks/targets can create items (such as object files),
                // such items are not part of the project per say, and should not be displayed.
                // so ignore those items.
                if (!this.IsItemTypeFileType(item.ItemType))
                    continue;

                // If the item is already contained do nothing.
                // TODO: possibly report in the error list that the the item is already contained in the project file similar to Language projects.
                if (items.ContainsKey(item.EvaluatedInclude.ToUpperInvariant()))
                    continue;

                // Make sure that we do not want to add the item, dependent, or independent twice to the ui hierarchy
                items.Add(item.EvaluatedInclude.ToUpperInvariant(), item);

                AddFileNodeToNode(item);
            }
        }

        #region Property Page Stuff

        protected override Guid[] GetConfigurationDependentPropertyPages()
        {
            Guid[] result = new Guid[1];
            result[0] = typeof(SanScriptProjectPropertyPage).GUID;
            return result;
        }

        protected override Guid[] GetConfigurationIndependentPropertyPages()
        {
            Guid[] result = new Guid[1];
            result[0] = typeof(SanScriptGeneralPropertyPage).GUID;
            return result;
        }

        protected override Guid[] GetPriorityProjectDesignerPages()
        {
            //  No designer property pages
            return new Guid[1];
        }

        #endregion
    }
}