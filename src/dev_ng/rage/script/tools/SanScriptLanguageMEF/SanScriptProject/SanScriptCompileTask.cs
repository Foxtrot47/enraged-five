﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Diagnostics;
using System.IO;
using Microsoft.VisualStudio.Shell;
using EnvDTE80;
using EnvDTE;

using RSG.Platform;

namespace Rockstar.SanScriptProject
{
    public class SanScriptCompileTask : ToolTask
    {
        #region Enumerations

        /// <summary>
        /// File state.
        /// </summary>
        private enum FileState
        {
            /// <summary>
            /// File has not changed.
            /// </summary>
            NotChanged,
            /// <summary>
            /// File has changed.
            /// </summary>
            Changed,

            /// <summary>
            /// File was not found.
            /// </summary>
            FileNotFound
        }

        #endregion

        #region Constants

        public static readonly string c_compilerError = "{0}: error CS1002: {1}";
        public static readonly string c_fileNotFoundError = "{0}: error CS0014: {1}";

        #endregion

        #region Parameters

        private string m_command;
        [Required]
        public string Command
        {
            get { return m_command; }
            set { m_command = value; }
        }

        private string m_outputPath;
        [Required]
        public string OutputPath
        {
            get { return m_outputPath; }
            set { m_outputPath = value; }
        }

        private string m_arguments;
        [Required]
        public string Arguments
        {
            get { return m_arguments; }
            set { m_arguments = value; }
        }

        protected override string ToolName
        {
            get
            {
                int toolNameStart = m_command.LastIndexOf('\\') + 1;
                string toolNameStr = m_command.Substring(toolNameStart, m_command.Length - toolNameStart);
                return toolNameStr;
            }
        }

        #endregion

        #region Constructors
        public SanScriptCompileTask()
        {
            m_outputDirectories = new Dictionary<RSG.Platform.Platform, string>();
            m_outputDirectories.Add(RSG.Platform.Platform.Win32, "win32");
            m_outputDirectories.Add(RSG.Platform.Platform.Win64, "win64");
            m_outputDirectories.Add(RSG.Platform.Platform.PS3, "ps3");
            m_outputDirectories.Add(RSG.Platform.Platform.Xbox360, "xbox360");
        }
        #endregion

        #region Variables

        private Dictionary<RSG.Platform.Platform, string> m_outputDirectories;
        private Dictionary<string, string> m_headerNames = null;
        private Dictionary<string, bool> m_headersChecked = null;
        private List<string> m_sourceFiles = null;
        private List<string> m_filesToBuild = null;
        private bool m_buildCanceled = false;


        #endregion

        #region Helper Structures
        protected override bool ValidateParameters()
        {
            //  Just check if the actual file exists for now
//             if (!File.Exists(FileName))
//             {
//                 Log.LogError(null, null, null, FileName, 0, 0, 0, 0, "File '{0}' does not exist!", FileName);
//                 return false;
//             }

            return base.ValidateParameters();
        }

        protected override string GenerateCommandLineCommands()
        {
            return Arguments;
        }

        protected override string GenerateFullPathToTool()
        {
            return m_command;
        }

        private void GetIncludeFile()
        {
            int index = Arguments.IndexOf("-include");

            //  Add the include file if it exists
            if (index >= 0)
            {
                int endIndex = Arguments.IndexOf(".sch", index);
                if (endIndex >= 0)
                {
                    //  Move past the end of the header extension
                    endIndex += ".sch".Length;
                    //  Adjust the start
                    index += "-include".Length;
                    //  Get the include header filename
                    string includeHeader = Arguments.Substring(index, endIndex - index).Trim(' ');

                    //  Now we have the header name, search for it's path
                    index = Arguments.IndexOf("-ipath");

                    if (index >= 0)
                    {
                        //  Move past the ipath bit
                        index += "-ipath".Length;
                        //  Get an end point
                        endIndex = Arguments.IndexOf("-werror");
                        if (endIndex < 0)
                        {
                            endIndex = Arguments.Length;
                        }
                        //  Get the include directories into a single string
                        string includeDirsString = Arguments.Substring(index, endIndex - index).Trim(' ');
                        //  Split the directories into strings
                        List<string> includeDirs = new List<string>();
                        includeDirs.AddRange(includeDirsString.Split(';'));
                        //  Find the first place the include header exists
                        foreach (string path in includeDirs)
                        {
                            string fullPath = path;
                            if (fullPath.EndsWith("\\"))
                            {
                                fullPath += includeHeader;
                            }
                            else
                            {
                                fullPath += "\\" + includeHeader;
                            }

                            if (File.Exists(fullPath))
                            {
                                //  Add the include header to the header list
                                m_headerNames.Add(includeHeader, fullPath);
                                //  Break out of the loop
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void BuildProjectFileNamesList(ProjectItems items)
        {
            //  Add the project files
            foreach (ProjectItem item in items)
            {
                if (item.ProjectItems != null)
                {
                    BuildProjectFileNamesList(item.ProjectItems);
                }
                else
                {
                    string fileName = item.get_FileNames(1);

                    if (!File.Exists(fileName))
                    {
                        Log.LogMessageFromText(String.Format(c_fileNotFoundError, fileName, "Cannot locate the specified file."), MessageImportance.High);
                        continue;
                    }

                    if (item.Name.EndsWith(".sch"))
                    {
                        //  Add to headers dictionary (filename and full path)
                        if (!m_headerNames.TryGetValue(item.Name, out fileName))
                        {
                            m_headerNames.Add(item.Name, item.get_FileNames(1));
                        }
                    }
                    else if (item.Name.EndsWith(".sc"))
                    {
                        if (!m_sourceFiles.Contains(fileName))
                        {
                            //  Add to the source files list (full path)
                            m_sourceFiles.Add(item.get_FileNames(1));
                        }
                    }
                }
            }
        }

        private FileState SourceContainsChangedHeader(string sourcefile, string outputFile)
        {
            FileState found = FileState.NotChanged;

            if (File.Exists(sourcefile))
            {
                //  Open the source file
                using (StreamReader file = new StreamReader(sourcefile))
                {
                    //  Read the whole thing into a string
                    string wholeFile = file.ReadToEnd();
                    //  Find the first using statement
                    int index = wholeFile.IndexOf("USING \"");

                    //  Loop through the file looking at the using statements
                    while (index >= 0 && found == FileState.NotChanged)
                    {
                        //  Trim the 'USING "' and any following spaces
                        wholeFile = wholeFile.Substring(index + "USING \"".Length).TrimStart(' ');
                        //  Get the header filename and trim any spaces
                        string header = wholeFile.Substring(0, wholeFile.IndexOf('\"'));
                        header = header.Trim(' ');
                        //  If the header name is in our list of already checked files then we don't need to check again
                        if (m_headersChecked.ContainsKey(header))
                        {
                            found = m_headersChecked[header] ? FileState.Changed : FileState.NotChanged;
                        }
                        else if (m_headerNames.ContainsKey(header))
                        {
                            //  Add this header to the list of already checked files
                            m_headersChecked.Add(header, false);
                            //  Check if the header file is newer than the output file
                            if (File.GetLastWriteTime(m_headerNames[header]) > File.GetLastWriteTime(outputFile))
                            {
                                //  Found it and it's newer
                                found = FileState.Changed;
                            }
                            //  It's not so go into that header to see if there are other headers included inside it
                            else
                            {
                                found = SourceContainsChangedHeader(m_headerNames[header], outputFile);
                            }
                            //  Set the found status of this header
                            m_headersChecked[header] = (found == FileState.Changed);
                        }
                        //  Move to the next using statement
                        index = wholeFile.IndexOf("USING \"");
                    }
                }
            }
            else
            {
                found = FileState.FileNotFound;
            }

            return found;
        }

        private void GetFilesToBuild()
        {
            m_filesToBuild = new List<string>();

            //  Add the sourcefiles if they are newer than their output .sco files
            foreach (string file in m_sourceFiles)
            {
                string outputFile = OutputPath + Path.GetFileName(file) + "o";

                if (!File.Exists(outputFile) || File.GetLastWriteTime(file) > File.GetLastWriteTime(outputFile))
                {
                    //  File is newer than output .sco file
                    m_filesToBuild.Add(file);
                }
                else
                {
                    //  File is not newer, check if headers are newer
                    FileState fileState = SourceContainsChangedHeader(file, outputFile);
                    if (fileState == FileState.Changed)
                    {
                        //  One of the included headers is newer so this file needs building
                        m_filesToBuild.Add(file);
                    }
                }
            }
        }

        public List<string> FilesToBuild()
        {
            return m_filesToBuild;
        }

        public override void Cancel()
        {
            //  Set our own tool canceled bool
            m_buildCanceled = true;
            //  Continue on to the base cancel function
            base.Cancel();
        }

        public bool CompileCurrentFile(bool forPreview)
        {
            DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
            Array activeSolutionProjects = dte.ActiveSolutionProjects as Array;
            Project project = null;

            //  Save the file first if we need to
            if (dte.ActiveDocument != null && !dte.ActiveDocument.Saved)
            {
                dte.ActiveDocument.Save();
            }

            if (activeSolutionProjects != null && activeSolutionProjects.Length > 0)
            {
                project = activeSolutionProjects.GetValue(0) as Project;
                if (project != null)
                {
                    SanScriptProjectNode projectNode = project.Object as SanScriptProjectNode;
                    if (projectNode != null)
                    {
                        string projectFile = project.Name;
                        string configName = project.ConfigurationManager.ActiveConfiguration.ConfigurationName;
                        string resourceExe = projectNode.GetProjectProperty("ResourceExe", true);
                        string resourceExe64 = projectNode.GetProjectProperty("ResourceExe64", true);
                        string postCompile = projectNode.GetProjectProperty("PostCompilePath", true);
                        string outputPath = projectNode.GetProjectProperty("OutputPath", true);

                        Command = projectNode.GetProjectProperty("CompilerExe", true);
                        OutputPath = outputPath;
                        Arguments = projectNode.GetProjectProperty("CmdLineParams", true);
                        Arguments += " " + projectNode.GetProjectProperty("Include", true);
                        Arguments += " " + projectNode.GetProjectProperty("IncludePath", true);
                        Arguments += " " + projectNode.GetProjectProperty("WarningsAsErrors", true);

                        //  Create the output path if we need to
                        if (!Directory.Exists(outputPath))
                        {
                            Directory.CreateDirectory(outputPath);
                        }

                        if (ValidateParameters())
                        {
                            ProcessStartInfo startInfo = new ProcessStartInfo();
                            //  Set the exe filename and the arguments
                            startInfo.FileName = GenerateFullPathToTool();
                            //  Turn off shell execute and don't create a window
                            startInfo.UseShellExecute = false;
                            startInfo.CreateNoWindow = true;
                            //  We want to redirect the standard output so we can parse for errors
                            startInfo.RedirectStandardOutput = true;

                            //  Have to set the dummy build engine or the logging falls over cos we arent running from MSBuild
                            this.BuildEngine = new SanScriptBuildEngine(projectFile);

                            //  Get the current file
                            if (dte.ActiveDocument != null)
                            {
                                string file = dte.ActiveDocument.FullName;

                                if (file.EndsWith(".sc", StringComparison.OrdinalIgnoreCase))
                                {
                                    startInfo.Arguments = "\"" + file + "\" " + GenerateCommandLineCommands() + " -output \"" + OutputPath + Path.GetFileName(file) + "o\"";

                                    // Compile the file
                                    using (System.Diagnostics.Process process = System.Diagnostics.Process.Start(startInfo))
                                    {
                                        using (StreamReader reader = process.StandardOutput)
                                        {
                                            string result = reader.ReadToEnd();
                                            ParseResult(result, file);
                                        }
                                    }

                                    // Resource the file.
                                    List<RSG.Platform.Platform> enabledPlatforms = new List<RSG.Platform.Platform>();
                                    foreach (KeyValuePair<RSG.Platform.Platform, RSG.Base.Configuration.ITarget> pair in Configuration.Instance.Config.Project.DefaultBranch.Targets)
                                    {
                                        if (pair.Value.Enabled == true)
                                            enabledPlatforms.Add(pair.Key);
                                    }

                                    SanScriptBuildData data = new SanScriptBuildData(false, enabledPlatforms);
                                    string fileNoExt = Path.GetFileNameWithoutExtension(dte.ActiveDocument.Name);
                                    List<string> resourceFiles = new List<string>();

                                    foreach (RSG.Platform.Platform platform in enabledPlatforms)
                                    {
                                        string platformName = platform.ToString();
                                        string platformExt = data.GetPlatformExtension(platform);

                                        if (platformExt != null && m_outputDirectories.ContainsKey(platform))
                                        {
                                            string resourcePath = Path.Combine(OutputPath, m_outputDirectories[platform], fileNoExt + platformExt);
                                            resourceFiles.Add("\"" + resourcePath + "\"");
                                        }
                                        else
                                        {
                                            Log.LogWarning("Platform extension for {0} is unknown.  Declare the proper extension in the project properties.", platform);
                                        }
                                    }

                                    foreach (string resource in resourceFiles)
                                    {
                                        //  Compile the resource files
                                        startInfo = new ProcessStartInfo();
                                        startInfo.FileName = resourceExe;
                                        if (resource.EndsWith(".ysc\"") == true)
                                        {
                                            startInfo.FileName = resourceExe64;
                                        }

                                        startInfo.UseShellExecute = false;
                                        startInfo.CreateNoWindow = true;
                                        startInfo.RedirectStandardOutput = true;

                                        startInfo.Arguments = "\"" + OutputPath + fileNoExt + "\" " + resource;
                                        Log.LogMessage(MessageImportance.High, "Compiling resource: \"{0}\"", resource + "...");

                                        //  Compile the file
                                        using (System.Diagnostics.Process process = System.Diagnostics.Process.Start(startInfo))
                                        {
                                            using (StreamReader reader = process.StandardOutput)
                                            {
                                                string result = reader.ReadToEnd();
                                                if (result.Length == 0)
                                                {
                                                    result = "Done.";
                                                }
                                                Log.LogMessage(MessageImportance.High, result);
                                            }
                                        }
                                    }


                                    if (forPreview)
                                    {
                                        //  Copy the files to the preview folder
                                        //  Get the filename minus the extension
                                        Log.LogMessage(MessageImportance.High, "Copying files to preview folders...\n");
                                        CopyOutputFileToPreviewFolders(projectNode, OutputPath, fileNoExt + ".sco");
                                        foreach (KeyValuePair<RSG.Platform.Platform, RSG.Base.Configuration.ITarget> pair in Configuration.Instance.Config.Project.DefaultBranch.Targets)
                                        {
                                            if (pair.Value.Enabled == true)
                                            {
                                                string platform = pair.Key.ToString();
                                                string platformExt = data.GetPlatformExtension(pair.Key);

                                                if (platformExt != null)
                                                {
                                                    CopyOutputFileToPreviewFolders(projectNode, Path.Combine(OutputPath, m_outputDirectories[pair.Key]), fileNoExt + platformExt);
                                                }
                                                else
                                                {
                                                    Log.LogWarning("Platform extension for {0} is unknown.  Declare the proper extension in the project properties.", platform);
                                                }

                                            }
                                        }
                                    }

                                    Log.LogMessage("Compile completed.");
                                }

                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Copies the specified file into the preview folders for both the dev and release branches.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="outputDir"></param>
        /// <param name="outputFile"></param>
        private void CopyOutputFileToPreviewFolders(SanScriptProjectNode project, string outputDir, string outputFile)
        {
            string previewDevFolder = Path.Combine(Configuration.Instance.Config.Project.Root, @"build\dev\preview\");
            string previewReleaseFolder = Path.Combine(Configuration.Instance.Config.Project.Root, @"build\release\preview\");

            //  Create the preview directories if they do not exist already
            if (!Directory.Exists(previewDevFolder))
            {
                Directory.CreateDirectory(previewDevFolder);
            }
            if (!Directory.Exists(previewReleaseFolder))
            {
                Directory.CreateDirectory(previewReleaseFolder);
            }

            //  Make sure we have an output file first
            string outputFilePath = Path.Combine(outputDir, outputFile);
            if (File.Exists(outputFilePath))
            {
                //  Copy the output file to the preview folders
                File.Copy(outputFilePath, Path.Combine(previewDevFolder, outputFile), true);
                File.Copy(outputFilePath, Path.Combine(previewReleaseFolder, outputFile), true);
                Log.LogMessage(MessageImportance.High, "'" + outputFilePath + "' copied to preview folders\n");
            }
            else
            {
                Log.LogMessage(MessageImportance.High, "ERROR: Cannot find '" + outputFilePath + "' to copy!\n");
            }
        }

        private string GetOutputFile(string inputFile)
        {
            return Path.Combine(OutputPath, Path.GetFileNameWithoutExtension(inputFile) + ".sco");
        }

        private bool CompileFiles()
        {
            if (ValidateParameters())
            {
                m_buildCanceled = false;

                ProcessStartInfo startInfo = new ProcessStartInfo();
                //  Set the exe filename and the arguments
                startInfo.FileName = GenerateFullPathToTool();
                //  Turn off shell execute and don't create a window
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;
                //  We want to redirect the standard output so we can parse for errors
                startInfo.RedirectStandardOutput = true;

                bool compilationSuccessful = true;

                foreach (string file in m_filesToBuild)
                {
                    if (m_buildCanceled)
                    {
                        m_buildCanceled = false;
                        break;
                    }

                    startInfo.Arguments = "\"" + file + "\" " + GenerateCommandLineCommands() + " -output \"" + GetOutputFile(file) + "\"";

                    using (System.Diagnostics.Process process = System.Diagnostics.Process.Start(startInfo))
                    {
                        using (StreamReader reader = process.StandardOutput)
                        {
                            string result = reader.ReadToEnd();
                            ParseResult(result, file);
                        }

                        if (process.ExitCode != 0)
                        {
                            compilationSuccessful = false;
                        }
                    }
                }

                return compilationSuccessful;
            }
            else
            {
                return false;
            }
        }

        public bool PreBuild(bool clean)
        {
            DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
            Array activeSolutionProjects = dte.ActiveSolutionProjects as Array;
            Project project = null;

            Log.LogMessage(MessageImportance.Normal, "Starting Pre-Build...");

            if (activeSolutionProjects != null && activeSolutionProjects.Length > 0)
            {
                project = activeSolutionProjects.GetValue(0) as Project;
                if (project != null)
                {
                    m_sourceFiles = new List<string>();
                    m_headerNames = new Dictionary<string, string>();
                    m_headersChecked = new Dictionary<string, bool>();

                    GetIncludeFile();
                    BuildProjectFileNamesList(project.ProjectItems);
                    if (clean)
                    {
                        m_filesToBuild = m_sourceFiles;
                    }
                    else
                    {
                        GetFilesToBuild();
                    }

                    Log.LogMessage(MessageImportance.Normal, "Finishing Pre-Build.");

                    return true;
                }
            }

            Log.LogMessage(MessageImportance.High, "There are no active projects.");
            return false;
        }

        public override bool Execute()
        {
            if (PreBuild(false))
            {
                return CompileFiles();
            }

            return false;
        }

        private void ParseResult(string result, string filename)
        {
            //  Split the result text into lines
            string[] output = result.Split('\n');

            string msgFormat = String.Empty;
            string file = String.Empty;
            string message = String.Empty;
            string compiling = String.Empty;
            MessageImportance importance = MessageImportance.High;

            foreach (string s in output)
            {
                if (s.StartsWith("ERROR", StringComparison.OrdinalIgnoreCase))
                {
                    msgFormat = c_compilerError;
                    message = s.Substring("error: ".Length);

                    if (s.IndexOf(") :") >= 0) // in the format Error: file name(xx) : error description 
                    {                          // (where xx is the line number)
                        string actualFile = s.Substring("error: ".Length);
                        file = actualFile.Substring(0, actualFile.IndexOf(")") + 1);
                    }
                    else
                    {
                        int firstApos = s.IndexOf("'");
                        int secondApos = s.IndexOf("'", firstApos + 1);

                        if (firstApos > 0 && secondApos > firstApos)
                        {
                            file = s.Substring(firstApos + 1, secondApos - firstApos);
                        }
                    }
                }
                else if (s.StartsWith("WARNING", StringComparison.OrdinalIgnoreCase))
                {
                    msgFormat = "{0} : warning : {1}";
                    message = s.Substring("warning: ".Length);
                }
                else if (s.StartsWith("INFO", StringComparison.OrdinalIgnoreCase))
                {
                    msgFormat = "{0} : info warning : {1}";
                    message = s.Substring("info: ".Length);
                }
                else if (s.StartsWith("compiling", StringComparison.OrdinalIgnoreCase) || s.StartsWith("compile ", StringComparison.OrdinalIgnoreCase))
                {
                    compiling += s;
                }
            }

            if (String.IsNullOrEmpty(msgFormat))
            {
                Log.LogMessage(MessageImportance.High, compiling);
            }
            else
            {
                Log.LogMessageFromText(String.Format(msgFormat, file, message), importance);
            }
        }
        
        #endregion
    }
}
