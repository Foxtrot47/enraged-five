﻿using System;
using SanScriptLanguageMEF;
using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.TextManager.Interop;
using EnvDTE;

namespace Rockstar.SanScriptProject
{
    /// <summary>
    /// Context menu actions class allows a link between context menu commands and the MEF project.
    /// </summary>
    [Export(typeof(IContextMenuActions))]
    public class ContextMenuActions : IContextMenuActions
    {
        #region MEF Imports

        /// <summary>
        /// MEF import for Document Actions. 
        /// </summary>
        [Import(typeof(IDocumentActions))]
        public IDocumentActions DocumentActions { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public ContextMenuActions()
        {
            SanScriptProjectPackage.Instance.MEFMenuHandler += new EventHandler(Instance_MEFMenuHandler);
            SanScriptProjectPackage.Instance.ContainsUsing += new ContainsUsingEventHandler(Instance_ContainsUsing);
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Check that the item is enabled event handler. 
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        void Instance_ContainsUsing(object sender, ContainsUsingEventArguments e)
        {
            e.LineContainsUsingStatement = DocumentActions.LineContainsUsing(e.View);
        }

        /// <summary>
        /// The menu item click event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        void Instance_MEFMenuHandler(object sender, EventArgs e)
        {
            OleMenuCommand command = sender as OleMenuCommand;

            if (command != null && command.CommandID != null)
            {
                switch (command.CommandID.ID)
                {
                    case (int)PkgCmdIDList.cmdidOpenHeaderFile:
                        {
                            IVsTextManager textMgr = (IVsTextManager)SanScriptProjectPackage.GetGlobalService(typeof(SVsTextManager));
                            IVsTextView view = null;
                            textMgr.GetActiveView(1, null, out view);
                            if (view != null)
                            {
                                string fullPath = DocumentActions.GetHeaderFullPath(view);
                                if (!String.IsNullOrEmpty(fullPath))
                                {
                                    DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
                                    dte.ItemOperations.OpenFile(fullPath);
                                }
                            }
                        }
                        break;
                }
            }
        }

        #endregion
    }
}
