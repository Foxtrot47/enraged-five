﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell.Interop;

using IOleServiceProvider = Microsoft.VisualStudio.OLE.Interop.IServiceProvider;

namespace Rockstar.SanScriptProject
{
    [Guid(GuidList.guidSanScriptProjectFactoryString)]
    class SanScriptProjectFactory : ProjectFactory
    {
        private SanScriptProjectPackage m_package;

        public SanScriptProjectFactory(SanScriptProjectPackage package) : base(package)
        {
            m_package = package;
        }

        protected override ProjectNode CreateProject()
        {
            SanScriptProjectNode project = new SanScriptProjectNode(m_package);

            project.SetSite((IOleServiceProvider)((IServiceProvider)m_package).GetService(typeof(IOleServiceProvider)));

            return project;
        }

        protected override void CreateProject(string fileName, string location, string name, uint flags, ref Guid projectGuid, out IntPtr project, out int canceled)
        {
            //  Just alter flags for now
            flags |= (uint)__VSCREATEPROJFLAGS.CPF_NONLOCALSTORE;
            //  Call down to base class method
            base.CreateProject(fileName, location, name, flags, ref projectGuid, out project, out canceled);
        }

        protected override bool CanCreateProject(string fileName, uint flags)
        {
            //  Try sticking upgrade stuff in here for old script projects
            return base.CanCreateProject(fileName, flags);
        }
    }
}
