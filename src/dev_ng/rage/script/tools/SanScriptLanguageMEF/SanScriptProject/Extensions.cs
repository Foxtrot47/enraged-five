﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.Text;
using SanScriptLanguageMEF;

namespace Rockstar.SanScriptProject
{
    static class Extensions
    {
        internal static ISanScriptClassifier GetAnalysis(this ITextBuffer buffer)
        {
            ISanScriptClassifier res;
            buffer.Properties.TryGetProperty<ISanScriptClassifier>(typeof(ISanScriptClassifier), out res);
            return res;
        }
    }
}
