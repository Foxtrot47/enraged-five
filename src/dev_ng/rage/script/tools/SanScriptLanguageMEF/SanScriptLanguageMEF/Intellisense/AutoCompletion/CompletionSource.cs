﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.Text;

namespace SanScriptLanguageMEF
{
    class CompletionSource : ICompletionSource
    {
        private ITextBuffer m_buffer = null;
        private bool m_disposed = false;
        private IGlyphService m_glyphService = null;

        /// <summary>
        /// CompletionSource class constructor
        /// </summary>
        /// <param name="buffer">The text containing the word to try and complete</param>
        public CompletionSource(ITextBuffer buffer, IGlyphService glyphService)
        {
            //  Store the text buffer
            m_buffer = buffer;
            m_glyphService = glyphService;
        }

        /// <summary>
        /// Compile the list of possible completions and set the text to try and complete
        /// </summary>
        /// <param name="session">This intellisense session</param>
        /// <param name="completionSets">The completion sets list to add completions to</param>
        public void AugmentCompletionSession(ICompletionSession session, IList<CompletionSet> completionSets)
        {
            //  Make sure this instance hasn't been disposed
            if (m_disposed)
            {
                throw new ObjectDisposedException("CompletionSource");
            }

            //  Get a snapshot of the text
            ITextSnapshot snapshot = m_buffer.CurrentSnapshot;
            //  Find where the intellisense was triggered
            var triggerPoint = (SnapshotPoint)session.GetTriggerPoint(snapshot);
            //  End function if no trigger point found
            if (triggerPoint == null)
            {
                return;
            }

            var line = triggerPoint.GetContainingLine();
            //  Set snapshot point to the position of the trigger
            SnapshotPoint start = triggerPoint;

            if (line.GetText().Trim().StartsWith("//"))
            {
                // Early out if the line starts with a comment. 
                return;
            }

            if ((start - 1).GetChar() == '.')
            {
                AugmentCompletionSessionMember(session, completionSets);
            }
            else
            {
                AugmentCompletionSessionIdentifier(session, completionSets);
            }
        }

        /// <summary>
        /// Suggestions for identifiers.
        /// </summary>
        /// <param name="session">Session.</param>
        /// <param name="completionSets">Completion sets.</param>
        private void AugmentCompletionSessionIdentifier(ICompletionSession session, IList<CompletionSet> completionSets)
        {
            //  Create a list of completions
            List<Completion> completions = new List<Completion>();
            //  Fill them in using the parser
            List<string> keywords = SanScriptParser.GetKeywords();
            foreach (string entry in keywords)
            {
                Completion keyWordCompletion = new Completion(entry);

                keyWordCompletion.IconSource = m_glyphService.GetGlyph(StandardGlyphGroup.GlyphKeyword, StandardGlyphItem.GlyphItemPublic);

                completions.Add(keyWordCompletion);
            }
            Dictionary<string, List<Identifier>> identifiers = SanScriptParser.GetIdentifiers();
            foreach (KeyValuePair<string, List<Identifier>> entry in identifiers)
            {
                //  Just use the first entry for now
                //  TODO: We will eventually need a more accurate match in case a variable and 
                //  function have the same name (shouldn't happen, but *could*)
                Identifier identifier = entry.Value[0];

                Completion identifierCompletion = new Completion(identifier.GetName());

                identifierCompletion.IconSource = identifier.GetIcon(m_glyphService);

                completions.Add(identifierCompletion);
            }
            Dictionary<string, List<Identifier>> includedIdentifiers = SanScriptParser.GetIncludedIdentifiers();
            foreach (KeyValuePair<string, List<Identifier>> entry in includedIdentifiers)
            {
                //  Just use the first entry for now
                //  TODO: We will eventually need a more accurate match in case a variable and 
                //  function have the same name (shouldn't happen, but *could*)
                Identifier identifier = entry.Value[0];

                Completion identifierCompletion = new Completion(identifier.GetName());

                identifierCompletion.IconSource = identifier.GetIcon(m_glyphService);

                completions.Add(identifierCompletion);
            }

            //  Get a snapshot of the text
            ITextSnapshot snapshot = m_buffer.CurrentSnapshot;
            //  Find where the intellisense was triggered
            var triggerPoint = (SnapshotPoint)session.GetTriggerPoint(snapshot);
            //  End function if no trigger point found
            if (triggerPoint == null)
            {
                return;
            }
            //  Get the line the trigger point is on
            var line = triggerPoint.GetContainingLine();
            //  Set snapshot point to the position of the trigger
            SnapshotPoint start = triggerPoint;
            //  Track backwards to the start of the line or previous whitespace or delimiter
            while (start > line.Start && !char.IsWhiteSpace((start - 1).GetChar()))
            {
                // If we have a punctuation symbol (except for underscore because that can be part
                // of an identifier) we don't include that in the section of text that will be replaced
                // by the user clicking an item in the Intellisense drop-down list.
                if (char.IsPunctuation((start - 1).GetChar()) && (start - 1).GetChar() != '_')
                {
                    break;
                }
                start -= 1;
            }

            SnapshotPoint end = triggerPoint;

            while (end < line.End && !char.IsWhiteSpace((end + 1).GetChar()))
            {
                // If we have a punctuation symbol (except for underscore because that can be part
                // of an identifier) we don't include that in the section of text that will be replaced
                // by the user clicking an item in the Intellisense drop-down list.
                if (char.IsPunctuation(end.GetChar()) && end.GetChar() != '_')
                {
                    break;
                }
                end += 1;
            }

            //  Get the applicable span (the area we will change if we accept an auto complete)
            var applicableTo = snapshot.CreateTrackingSpan(new SnapshotSpan(start, end), SpanTrackingMode.EdgePositive);
            //  Add it to the 'All' completion set
            completionSets.Add(new CompletionSet("All", "All", applicableTo, completions, Enumerable.Empty<Completion>()));
        }

        /// <summary>
        /// Suggestions for member fields.
        /// </summary>
        /// <param name="session">Session.</param>
        /// <param name="completionSets">Completion sets.</param>
        private void AugmentCompletionSessionMember(ICompletionSession session, IList<CompletionSet> completionSets)
        {
            ITextSnapshot snapshot = m_buffer.CurrentSnapshot;
            var triggerPoint = (SnapshotPoint)session.GetTriggerPoint(snapshot);
            if (triggerPoint == null)
            {
                return;
            }

            var line = triggerPoint.GetContainingLine();
            var stackONames = GetIdentifiers(triggerPoint);
            
            string name = stackONames.Pop();
            var structId = SanScriptParser.FindIdentifier(name);

            if (structId == null)
            {
                return;
            }

            string structTypeName = SanScriptParser.GetVariablesType(structId.GetName()); 
            int count = 0;

            while (stackONames.Count > 0)
            {
                foreach (string s in SanScriptParser.GetMemberFields(name))
                {
                    string[] split = s.Split("!".ToCharArray());
                    if (split[0] == stackONames.Peek())
                    {
                        structTypeName = split[1];
                        name = stackONames.Pop();
                        break;
                    }
                }

                count++;
                if (count > stackONames.Count)
                {
                    // Let's not get caught in an infinite loop if we can't correctly parse the line, it
                    // could be malformed (double dots, bad names etc.)
                    break;
                }
            }

            List<Completion> completions = new List<Completion>();
            foreach (string s in SanScriptParser.GetStructMembers(structTypeName))
            {
                Identifier identifier = SanScriptParser.FindIdentifier(s);
                Completion identifierCompletion = new Completion(s);
                identifierCompletion.IconSource = identifier.GetIcon(m_glyphService);
                completions.Add(identifierCompletion);
            }

            if (completions.Count == 0)
            {
                // Probably an enumeration, so lets try and get enum members
                List<string> members = SanScriptParser.FindEnumMembers(structId.GetName());
                foreach(string s in members)
                {
                    Identifier identifier = SanScriptParser.FindIdentifier(s);
                    Completion identifierCompletion = new Completion(s);
                    identifierCompletion.IconSource = identifier.GetIcon(m_glyphService);
                    completions.Add(identifierCompletion);
                }
            }

            var applicableTo = snapshot.CreateTrackingSpan(new SnapshotSpan(triggerPoint, triggerPoint), SpanTrackingMode.EdgeInclusive);
            completionSets.Add(new CompletionSet("All", "All", applicableTo, completions, Enumerable.Empty<Completion>()));
        }

        /// <summary>
        /// Parse the current line and get the list of identifiers. For example:
        /// foo.bar[50].sub would return a stack containing sub, bar and foo.
        /// </summary>
        /// <param name="point">Snapshot point.</param>
        /// <returns>Stack of identifiers.</returns>
        private Stack<string> GetIdentifiers(SnapshotPoint point)
        {
            Stack<string> names = new Stack<string>();

            var line = point.GetContainingLine();
            SnapshotPoint start = point;
            start -= 1;

            SnapshotPoint previousPoint = start - 1;

            while (start > line.Start) 
            {
                start -= 1;
                if (start.GetChar() == '.')
                {
                    int length = (previousPoint - start);
                    string name = line.GetText().Substring(1 + (start.Position - line.Start), length);
                    previousPoint = start - 1;

                    name = CleanName(name);
                    names.Push(name);
                }
            }

            int len = 1 + (previousPoint - start);
            string nam = line.GetText().Substring(start.Position - line.Start, len);
            nam = CleanName(nam);
            names.Push(nam);

            return names;
        }

        /// <summary>
        /// Clean the identifier name.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <returns>Cleaned identifier.</returns>
        private string CleanName(string name)
        {
            if (name.Contains('['))
            {
                int startBracket = name.IndexOf('[');
                int endBracket = name.IndexOf(']');
                if (endBracket < 0)
                {
                    name = name.Remove(startBracket);
                }
                else
                {
                    name = name.Remove(startBracket, 1 + (endBracket - startBracket));
                }
            }

            if (name.Contains('='))
            {
                int equalSign = name.IndexOf('=');
                if (equalSign < name.Length - 1)
                {
                    name = name.Substring(equalSign + 1).Trim();
                }
                else
                {
                    name = name.Substring(0, equalSign);
                }
            }

            return name;
        }

        /// <summary>
        /// Sets the disposed flag when the instance should be disposed of
        /// </summary>
        public void Dispose()
        {
            m_disposed = true;
        }
    }
}
