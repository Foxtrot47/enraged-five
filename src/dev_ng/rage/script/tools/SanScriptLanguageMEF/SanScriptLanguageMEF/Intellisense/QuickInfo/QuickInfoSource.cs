﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Operations;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Utilities;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using SanScriptLanguageMEF.Popups;

namespace SanScriptLanguageMEF
{
    internal class QuickInfoSource : IQuickInfoSource
    {
        private QuickInfoSourceProvider m_provider;
        private ITextBuffer m_subjectBuffer;

        private bool m_isDisposed;

        public QuickInfoSource(QuickInfoSourceProvider provider, ITextBuffer subjectBuffer)
        {
            m_provider = provider;
            m_subjectBuffer = subjectBuffer;
        }

        public void AugmentQuickInfoSession(IQuickInfoSession session, IList<object> qiContent, out ITrackingSpan applicableToSpan)
        {
            // Map the trigger point down to our buffer.
            SnapshotPoint? subjectTriggerPoint = session.GetTriggerPoint(m_subjectBuffer.CurrentSnapshot);
            if (!subjectTriggerPoint.HasValue)
            {
                applicableToSpan = null;
                return;
            }

            ITextSnapshot currentSnapshot = subjectTriggerPoint.Value.Snapshot;
            SnapshotSpan querySpan = new SnapshotSpan(subjectTriggerPoint.Value, 0);

            ITextStructureNavigator navigator = m_provider.NavigatorService.GetTextStructureNavigator(m_subjectBuffer);
            TextExtent extent = navigator.GetExtentOfWord(subjectTriggerPoint.Value);
            string searchText = extent.Span.GetText();
            Identifier identifier = SanScriptParser.FindIdentifier(searchText);

            if (identifier != null)
            {
                applicableToSpan = null;

                switch (identifier.GetIdentifierType())
                {
                    case Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_FUNCTION:
                    case Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_NATIVEFUNCTION:
                        applicableToSpan = currentSnapshot.CreateTrackingSpan(querySpan.Start.Add(0).Position, identifier.GetName().Length, SpanTrackingMode.EdgeInclusive);
                        qiContent.Add(CreateFunctionPopup(identifier.GetSignature()));
                        break;
                    case Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_STRUCT:
                        applicableToSpan = currentSnapshot.CreateTrackingSpan(querySpan.Start.Add(0).Position, identifier.GetName().Length, SpanTrackingMode.EdgeInclusive);
                        var structToolTip = new StructMembersToolTip();
                        var viewModel = new StructViewModel(identifier);
                        structToolTip.DataContext = viewModel;
                        qiContent.Add(structToolTip);
                        break;
                    case Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_ENUM_TYPE:
                        applicableToSpan = currentSnapshot.CreateTrackingSpan(querySpan.Start.Add(0).Position, identifier.GetName().Length, SpanTrackingMode.EdgeInclusive);

                        var enumToolTip = new StructMembersToolTip();
                        var enumViewModel = new EnumViewModel(identifier);
                        enumToolTip.DataContext = enumViewModel;
                        qiContent.Add(enumToolTip);
                        break;
                    case Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_CONST_VARIABLE:
                        applicableToSpan = currentSnapshot.CreateTrackingSpan(querySpan.Start.Add(0).Position, identifier.GetName().Length, SpanTrackingMode.EdgeInclusive);
                        string[] split = identifier.GetSignature().Split("=".ToCharArray());
                        ConstValueViewModel vm = new ConstValueViewModel(split[0], split[1]);
                        ConstValueToolTip constValue = new ConstValueToolTip();
                        constValue.DataContext = vm;
                        qiContent.Add(constValue);
                        break;
                }

                return;
            }

            applicableToSpan = null;
        }

        /// <summary>
        /// Create a pop-up tool tip for a function / procedure / native function.
        /// </summary>
        /// <param name="signature">Function signature. Last line is always the signature, preceding lines are comments.</param>
        /// <returns>A stack panel containing the function comments (if any) and the signature.</returns>
        private StackPanel CreateFunctionPopup(string signature)
        {
            string[] lines = signature.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            StackPanel panel = new StackPanel();

            for (int i = 0; i < lines.Length; i++)
            {
                if (i < lines.Length - 1)
                {
                    panel.Children.Add(new TextBlock() { Text = lines[i], Foreground = Brushes.Green });
                }
                else
                {
                    panel.Children.Add(new TextBlock() { Text = lines[i], Foreground = Brushes.Black });
                }
            }

            return panel;
        }

        private TextBlock CreateTextBlock(string contents, Brush foreground)
        {
            TextBlock block = new TextBlock() { Text = contents, Foreground = foreground };
            return block;
        }

        public void Dispose()
        {
            if (!m_isDisposed)
            {
                GC.SuppressFinalize(this);
                m_isDisposed = true;
            }
        }
    }
}
