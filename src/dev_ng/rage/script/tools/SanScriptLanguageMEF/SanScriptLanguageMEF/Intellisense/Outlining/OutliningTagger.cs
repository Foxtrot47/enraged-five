﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

using Microsoft.VisualStudio.Text.Outlining;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Utilities;
using Microsoft.VisualStudio.Text;

namespace SanScriptLanguageMEF
{
    internal sealed class OutliningTagger : ITagger<IOutliningRegionTag>
    {
        #region Constants

        static readonly string m_hoverText = "Collapsed code (click the '+' symbol to expand)";  //  The string for the tooltip on the collapsed span

        #endregion

        #region Private member fields

        private ITextBuffer m_buffer;
        private ITextSnapshot m_snapshot;

        #endregion

        #region Events

        /// <summary>
        /// Tags collection changed event.
        /// </summary>
        public event EventHandler<SnapshotSpanEventArgs> TagsChanged;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="buffer">Text buffer.</param>
        public OutliningTagger(ITextBuffer buffer)
        {
            m_buffer = buffer;
            m_snapshot = buffer.CurrentSnapshot;
            m_buffer.Changed += BufferChanged;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Get the outline region tags for the specified span collection.
        /// </summary>
        /// <param name="spans">Span collection.</param>
        /// <returns>Enumeration of outlined regions.</returns>
        public IEnumerable<ITagSpan<IOutliningRegionTag>> GetTags(NormalizedSnapshotSpanCollection spans)
        {
            if (spans.Count == 0)
            {
                yield break;
            }

            if (!SanScriptParser.RegionMap.ContainsKey(m_buffer) || !SanScriptParser.SnapshotMap.ContainsKey(m_buffer))
            {
                yield break;
            }

            List<Region> currentRegions = SanScriptParser.RegionMap[m_buffer];
            ITextSnapshot currentSnapshot = SanScriptParser.SnapshotMap[m_buffer];
            SnapshotSpan entire;

            try
            {
                entire = new SnapshotSpan(spans[0].Start, spans[spans.Count - 1].End).TranslateTo(currentSnapshot, SpanTrackingMode.EdgeExclusive);
            }
            catch (ArgumentException)
            {
                yield break;
            }

            int startLineNumber = entire.Start.GetContainingLine().LineNumber;
            int endLineNumber = entire.End.GetContainingLine().LineNumber;

            foreach (var region in currentRegions)
            {
                if (region.StartLine <= endLineNumber &&
                    region.EndLine >= startLineNumber)
                {
                    var startLine = currentSnapshot.GetLineFromLineNumber(region.StartLine);
                    var endLine = currentSnapshot.GetLineFromLineNumber(region.EndLine);

                    //  The region starts at the beginning of our m_startHide and goes until the 
                    //  end of the line that contains the m_endHide
                    yield return new TagSpan<IOutliningRegionTag>(
                        new SnapshotSpan(startLine.Start + region.StartOffset, endLine.End),
                        new OutliningRegionTag(false, false, region.CollapsedString, m_hoverText));
                }
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Buffer changed event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        void BufferChanged(object sender, TextContentChangedEventArgs e)
        {
            //  If this isn't the most up to date version of the buffer then ignore 
            //  it for now, there will be another change event eventually
            if (e.After != m_buffer.CurrentSnapshot)
            {
                return;
            }
        }

        #endregion
    }
}
