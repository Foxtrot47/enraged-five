﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SanScriptLanguageMEF
{
    class PartialRegion
    {
        public int StartLine { get; set; }
        public int StartOffset { get; set; }
        public int Level { get; set; }
        public PartialRegion PartialParent { get; set; }

        private string m_collapsedString = "...";
        public string CollapsedString
        {
            get
            {
                return m_collapsedString;
            }
            set
            {
                //  Remove any leading or trailing whitespace
                m_collapsedString = value.Trim();
            }
        }

        public OutliningPair RegionPair { get; set; }
    }
}
