﻿using System;
using System.Runtime.InteropServices;

using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.OLE.Interop;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio;
using SanScriptLanguageMEF.Intellisense.AutoCompletion;
using Microsoft.VisualStudio.TextManager.Interop;
using System.IO.Packaging;
using System.ComponentModel.Composition;
using System.Text;
using System.Collections.Generic;

namespace SanScriptLanguageMEF
{
    public sealed class CommandFilter : IOleCommandTarget
    {
        ICompletionSession m_currentSession;
        IParamInfoPopup m_popup;

        public CommandFilter(IWpfTextView textView, ICompletionBroker broker, IParamInfoPopup popup)
        {
            m_currentSession = null;
            m_popup = popup;
            TextView = textView;
            Broker = broker;
        }

        public IWpfTextView TextView { get; private set; }
        public ICompletionBroker Broker { get; private set; }
        public IOleCommandTarget Next { get; set; }

        private char GetTypeChar(IntPtr pvaIn)
        {
            return (char)(ushort)Marshal.GetObjectForNativeVariant(pvaIn);
        }

        public int Exec(ref Guid pGuidCmdGroup, uint nCmdID, uint nCmdExecOpt, IntPtr pvaIn, IntPtr pvaOut)
        {
            bool handled = false;
            int hresult = VSConstants.S_OK;

            if (pGuidCmdGroup == VSConstants.VSStd2K)
            {
                switch ((VSConstants.VSStd2KCmdID)nCmdID)
                {
                    case VSConstants.VSStd2KCmdID.AUTOCOMPLETE:
                    case VSConstants.VSStd2KCmdID.COMPLETEWORD:
                    {
                        handled = StartSession();
                    }
                    break;
                    case VSConstants.VSStd2KCmdID.RETURN:
                    {
                        handled = Complete(false);
                    }
                    break;
                    case VSConstants.VSStd2KCmdID.TAB:
                    {
                        handled = Complete(true);
                    }
                    break;
                    case VSConstants.VSStd2KCmdID.CANCEL:
                    {
                        handled = Cancel();
                    }
                    break;
                    case VSConstants.VSStd2KCmdID.COMMENT_BLOCK:
                    case VSConstants.VSStd2KCmdID.COMMENTBLOCK:
                    {
                        Commenter.ToggleComment(TextView, true);
                        handled = true;
                    }
                    break;
                    case VSConstants.VSStd2KCmdID.UNCOMMENT_BLOCK:
                    case VSConstants.VSStd2KCmdID.UNCOMMENTBLOCK:
                    {
                        Commenter.ToggleComment(TextView, false);
                        handled = true;
                    }
                    break;
                    case VSConstants.VSStd2KCmdID.PARAMINFO:
                    {
                        m_popup.Refresh();
                        handled = true;
                    }
                    break;
                    case VSConstants.VSStd2KCmdID.TYPECHAR:
                    {
                        handled = GenerateComment(GetTypeChar(pvaIn));
                    }
                    break;
                }

            }

            if (!handled)
            {
                if (m_popup.IsShowing)
                {
                    m_popup.Refresh();
                }

                hresult = Next.Exec(pGuidCmdGroup, nCmdID, nCmdExecOpt, pvaIn, pvaOut);
            }

            if (ErrorHandler.Succeeded(hresult))
            {
                if (pGuidCmdGroup == VSConstants.VSStd2K)
                {
                    switch ((VSConstants.VSStd2KCmdID)nCmdID)
                    {
                        case VSConstants.VSStd2KCmdID.TYPECHAR:
                        {
                            char ch = GetTypeChar(pvaIn);

                            if (ch.Equals('.'))
                            {
                                handled = StartSession();
                            }
                            else if (!char.IsWhiteSpace(ch))
                            {
                                Filter();
                            }
                            else
                            {
                                Cancel();
                            }
                        }
                        break;
                        case VSConstants.VSStd2KCmdID.BACKSPACE:
                        {
                            Filter();
                        }
                        break;
                    }
                }
            }

            return hresult;
        }

        /// <summary>
        /// Generate the procedure / function signature comment.
        /// </summary>
        /// <param name="lastChar">The last character typed by the user.</param>
        /// <returns>True if a comment was inserted.</returns>
        private bool GenerateComment(char lastChar)
        {
            SnapshotPoint caret = TextView.Caret.Position.BufferPosition;
            int lineNumber = caret.GetContainingLine().LineNumber;
            string line = caret.GetContainingLine().GetText().Trim();

            if (lineNumber >= TextView.TextBuffer.CurrentSnapshot.LineCount - 1)
            {
                return false;
            }

            if (lineNumber >= 2)
            {
                if (TextView.TextBuffer.CurrentSnapshot.GetLineFromLineNumber(lineNumber - 1).GetText().Trim().StartsWith("///"))
                {
                    return false;
                }
            }

            string nextLine = TextView.TextBuffer.CurrentSnapshot.GetLineFromLineNumber(lineNumber + 1).GetText();

            if (line == "//" && lastChar == '/')
            {
                List<string> paramNames = new List<string>();
                bool hasReturn = false;
                string numSpaces = String.Empty;

                if (GetProcDetails(nextLine, paramNames, out hasReturn, out numSpaces))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine(numSpaces+ "/// PURPOSE:");
                    sb.Append(numSpaces + "///\t\t");

                    if (paramNames.Count > 0)
                    {
                        sb.AppendLine();
                        sb.AppendLine(numSpaces + "/// PARAMS:");

                        int paramCount = paramNames.Count;

                        foreach (var paramName in paramNames)
                        {
                            paramCount--;
                            if (paramCount > 0)
                            {
                                sb.AppendLine(String.Format("{0}///\t\t{1} - ", numSpaces, paramName));
                            }
                            else
                            {
                                sb.Append(String.Format("{0}///\t\t{1} - ", numSpaces, paramName));
                            }
                        }
                    }

                    if (hasReturn)
                    {
                        sb.AppendLine();
                        sb.AppendLine(numSpaces + "/// RETURN:");
                        sb.Append(numSpaces + "///\t\t");
                    }

                    ITextEdit edit = TextView.TextBuffer.CreateEdit();
                    edit.Replace(caret.GetContainingLine().Start.Position, caret.GetContainingLine().GetText().Length, sb.ToString());
                    edit.Apply();
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Get the procedure / function details from the given procLine.
        /// </summary>
        /// <param name="procLine">The line of text that contains the procedure / function signature.</param>
        /// <param name="paramNames">The names of the parameters.</param>
        /// <param name="hasReturn">True if this is a function signature.</param>
        /// <param name="numSpaces">The string that makes up the whitespaces used at the start of the line. This will indicate the indentation for the comment.</param>
        /// <returns>True if the line contains a valid procedure / function signature.</returns>
        private bool GetProcDetails(string procLine, List<string> paramNames, out bool hasReturn, out string numSpaces)
        {
            paramNames.Clear();
            hasReturn = procLine.IndexOf("FUNC ", StringComparison.OrdinalIgnoreCase) >= 0;
            numSpaces = "";

            if (procLine.IndexOf("FUNC ", StringComparison.OrdinalIgnoreCase) < 0 && procLine.IndexOf("PROC ", StringComparison.OrdinalIgnoreCase) < 0)
            {
                return false;
            }

            int startBracket = procLine.IndexOf('(');
            int endBracket = procLine.IndexOf(')');

            if (startBracket < 0 || endBracket < 0)
            {
                return false;
            }

            FillArgNames(procLine, startBracket, endBracket, paramNames);
            numSpaces = GetLeadingSpaces(procLine);

            return true;
        }

        /// <summary>
        /// Get the leading spaces from the given line.
        /// </summary>
        /// <param name="line">Line of text that may contain leading white space characters.</param>
        /// <returns>The leading whitespace characters from the given string. May be a blank string.</returns>
        private string GetLeadingSpaces(string line)
        {
            string returnString = "";

            for (int i = 0; i < line.Length; i++)
            {
                if (char.IsWhiteSpace(line[i]))
                {
                    returnString += line[i];
                }
                else
                {
                    break;
                }
            }

            return returnString;
        }

        /// <summary>
        /// Fill the argument names from the given line.
        /// </summary>
        /// <param name="line">Line of text containing a procedure / function signature.</param>
        /// <param name="startBracket">Start bracket position.</param>
        /// <param name="endBracket">End bracket position.</param>
        /// <param name="paramNames">The list of parameter names.</param>
        private void FillArgNames(string line, int startBracket, int endBracket, List<string> paramNames)
        {
            string procArgs = line.Substring(startBracket + 1, (endBracket - startBracket) - 1);
            string[] args = procArgs.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (var arg in args)
            {
                string[] argSplit = arg.Split(" ".ToCharArray());
                paramNames.Add(argSplit[argSplit.Length - 1]);
            }
        }

        private void Filter()
        {
            //  If we dont have a current session, start one
            if (m_currentSession == null)
            {
                StartSession();
            }

            if (m_currentSession.SelectedCompletionSet == null)
            {
                Cancel();
                return;
            }

            //  Filter the list so we only show the matching entries
            m_currentSession.SelectedCompletionSet.Filter();
            //  Select the best match from the list of possible completions
            m_currentSession.SelectedCompletionSet.SelectBestMatch();
            m_currentSession.SelectedCompletionSet.Recalculate();
            //  If no match is selected then get rid of the list
            if (!m_currentSession.SelectedCompletionSet.SelectionStatus.IsSelected)
            {
                Cancel();
            }
        }

        bool Cancel()
        {
            //  Dont do anything if we dont have a current session
            if (m_currentSession == null)
            {
                return false;
            }

            //  Dismiss the current session
            m_currentSession.Dismiss();
            return true;
        }

        bool Complete(bool force)
        {
            //  Dont do anything if we dont have a current session
            if (m_currentSession == null || m_currentSession.SelectedCompletionSet == null)
            {
                return false;
            }

            if (!m_currentSession.SelectedCompletionSet.SelectionStatus.IsSelected && !force)
            {
                m_currentSession.Dismiss();
                return false;
            }
            else
            {
                m_currentSession.Commit();
                return true;
            }
        }

        bool StartSession()
        {
            //  Don't do anything if we already have a current session
            bool startSession = true;

            try
            {
                startSession = !(m_currentSession != null && m_currentSession.CompletionSets != null);
            }
            catch
            {
                if (m_currentSession != null)
                {
                    m_currentSession.Dismiss();
                }

                startSession = true;
            }

            if (!startSession)
            {
                return false;
            }

            SnapshotPoint caret = TextView.Caret.Position.BufferPosition;
            ITextSnapshot snapshot = caret.Snapshot;

            if (!Broker.IsCompletionActive(TextView))
            {
                m_currentSession = Broker.CreateCompletionSession(TextView, snapshot.CreateTrackingPoint(caret, PointTrackingMode.Positive), true);
            }
            else
            {
                m_currentSession = Broker.GetSessions(TextView)[0];
            }

            m_currentSession.Start();

            m_currentSession.Dismissed += (sender, args) => m_currentSession = null;

            return true;
        }

        public int QueryStatus(ref Guid pGuidCmdGroup, uint cCmds, OLECMD[] prgCmds, IntPtr pCmdText)
        {
            if (pGuidCmdGroup == VSConstants.VSStd2K)
            {
                switch ((VSConstants.VSStd2KCmdID)prgCmds[0].cmdID)
                {
                    case VSConstants.VSStd2KCmdID.AUTOCOMPLETE:
                    case VSConstants.VSStd2KCmdID.COMPLETEWORD:
                    case VSConstants.VSStd2KCmdID.COMMENT_BLOCK:
                    case VSConstants.VSStd2KCmdID.COMMENTBLOCK:
                    case VSConstants.VSStd2KCmdID.UNCOMMENT_BLOCK:
                    case VSConstants.VSStd2KCmdID.UNCOMMENTBLOCK:
                    case VSConstants.VSStd2KCmdID.PARAMINFO:
                    case VSConstants.VSStd2KCmdID.TYPECHAR:
                    {
                        prgCmds[0].cmdf = (uint)OLECMDF.OLECMDF_ENABLED | (uint)OLECMDF.OLECMDF_SUPPORTED;
                        return VSConstants.S_OK;
                    }
                }
            }

            return Next.QueryStatus(pGuidCmdGroup, cCmds, prgCmds, pCmdText);
        }

        /// <summary>
        /// Get the full path to the header.
        /// </summary>
        public string GetHeaderFullPath()
        {
            SnapshotPoint caret = TextView.Caret.Position.BufferPosition;
            string usingStatement = caret.GetContainingLine().GetText().Trim();
            string[] split = usingStatement.Split(" ".ToCharArray());
            string fileName = String.Empty;

            bool containsUsing = false;

            foreach (string token in split)
            {
                if (token.StartsWith("\"") && token.EndsWith("\""))
                {
                    fileName = token.Substring(1, token.Length - 2);
                    break;
                }
                else if (token.Equals("USING"))
                {
                    if (containsUsing)
                    {
                        containsUsing = false;
                        break;
                    }

                    containsUsing = true;
                }
            }

            if (!String.IsNullOrEmpty(fileName) && containsUsing)
            {
                return SanScriptParser.GetIncludeFullPath(fileName);
            }

            return String.Empty;
        }
        
        /// <summary>
        /// Returns true if the line starts with USING.
        /// </summary>
        /// <returns>True if the line starts with USING.</returns>
        public bool LineContainsUsing()
        {
            return !String.IsNullOrEmpty(GetHeaderFullPath());
        }
    }
}
