﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.Text;

namespace SanScriptLanguageMEF
{
    internal class ParameterHelpSource : ISignatureHelpSource
    {
        private ITextBuffer m_textBuffer;
        private bool m_isDisposed;

        public ParameterHelpSource(ITextBuffer textBuffer)
        {
            //  Store the text buffer
            m_textBuffer = textBuffer;
        }

        public void Dispose()
        {
            if (!m_isDisposed)
            {
                GC.SuppressFinalize(this);
                m_isDisposed = true;
            }
        }

        public void AugmentSignatureHelpSession(ISignatureHelpSession session, IList<ISignature> signatures)
        {
            ITextSnapshot snapshot = m_textBuffer.CurrentSnapshot;
            int position = session.GetTriggerPoint(m_textBuffer).GetPosition(snapshot);

            ITrackingSpan applicableToSpan = m_textBuffer.CurrentSnapshot.CreateTrackingSpan(new Span(position, 0), SpanTrackingMode.EdgeInclusive, 0);

            //  Get the start of the function being typed
            string functionName = snapshot.GetText().Substring(0, position);
            int lastWhitespace = functionName.LastIndexOfAny(new char[] { ' ', '\n' });
            functionName = functionName.Substring(lastWhitespace + 1);

            //  Add all our parsed method signatures here that match the function name
            Dictionary<string, List<Identifier>> dict = SanScriptParser.GetIdentifiers();

            if (dict.ContainsKey(functionName))
            {
                foreach (KeyValuePair<string, List<Identifier>> pair in dict)
                {
                    List<Identifier> list = pair.Value;

                    foreach (Identifier identifier in list)
                    {
                        if (identifier.GetSignature() != null && pair.Key.Equals(functionName, StringComparison.OrdinalIgnoreCase))
                        {
                            string documentation = null;
                            MethodSignature newSignature = CreateSignature(m_textBuffer, identifier.GetSignature(), documentation, applicableToSpan);

                            if (newSignature != null)
                            {
                                signatures.Add(newSignature);
                            }
                        }
                    }
                }
            }

            dict = SanScriptParser.GetIncludedIdentifiers();

            if (dict.ContainsKey(functionName))
            {
                foreach (KeyValuePair<string, List<Identifier>> pair in dict)
                {
                    List<Identifier> list = pair.Value;

                    foreach (Identifier identifier in list)
                    {
                        if (identifier.GetSignature() != null && pair.Key.Equals(functionName, StringComparison.OrdinalIgnoreCase))
                        {
                            string documentation = null;
                            MethodSignature newSignature = CreateSignature(m_textBuffer, identifier.GetSignature(), documentation, applicableToSpan);
                            
                            if (newSignature != null)
                            {
                                signatures.Add(newSignature);
                            }
                        }
                    }
                }
            }
        }

        private MethodSignature CreateSignature(ITextBuffer textBuffer, string methodSig, string methodDoc, ITrackingSpan span)
        {
            MethodSignature sig = new MethodSignature(textBuffer, methodSig, methodDoc, null);
            textBuffer.Changed += new EventHandler<TextContentChangedEventArgs>(sig.OnSubjectBufferChanged);

            //  Find the parameters in the method signature
            string[] parameters = methodSig.Split(new char[] { '(', ',', ')' });
            List<IParameter> paramList = new List<IParameter>();

            int locusStartSearch = 0;
            for (int i = 1; i < parameters.Length; i++)
            {
                string param = parameters[i].Trim();

                if (string.IsNullOrEmpty(param))
                {
                    continue;
                }

                //  Find where this parameter is located in the method signature
                int locusStart = methodSig.IndexOf(param, locusStartSearch);
                if (locusStartSearch >= 0)
                {
                    //  TODO: Get parameter documentation from any comments if we have any
                    string parameterDocumentation = null;
                    Span locus = new Span(locusStart, param.Length);
                    locusStartSearch = locusStart + param.Length;
                    paramList.Add(new MethodParameter(parameterDocumentation, locus, param, sig));
                }
            }

            sig.Parameters = new ReadOnlyCollection<IParameter>(paramList);
            sig.ApplicableToSpan = span;
            sig.ComputeCurrentParameter();

            return sig;
        }

        //  Need to implement this for the interface, but never seems to get called :S
        public ISignature GetBestMatch(ISignatureHelpSession session)
        {
            if (session.Signatures.Count > 0)
            {
                ITrackingSpan applicableToSpan = session.Signatures[0].ApplicableToSpan;
                string text = applicableToSpan.GetText(applicableToSpan.TextBuffer.CurrentSnapshot);

                //  Just get first one
                return session.Signatures[0];
            }

            return null;
        }
    }
}
