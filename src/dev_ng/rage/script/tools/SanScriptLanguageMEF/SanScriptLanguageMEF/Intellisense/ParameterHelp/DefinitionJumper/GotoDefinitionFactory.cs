﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SanScriptLanguageMEF.Intellisense.ParameterHelp.DefinitionJumper
{
    /// <summary>
    /// Go to definition factory.
    /// </summary>
    public static class GotoDefinitionFactory
    {
        /// <summary>
        /// Show the "Go To" dialog.
        /// </summary>
        /// <param name="identifiers">List of identifiers.</param>
        /// <returns>The selected identifier, or null if the Cancel button was selected.</returns>
        public static Identifier ShowDialog(List<Identifier> identifiers)
        {
            GotoDefinitionForm form = new GotoDefinitionForm();
            Controller ctrl = new Controller(form.lstDefinitions, identifiers);
            if (form.ShowDialog() == DialogResult.OK)
            {
                return ctrl.Selected;
            }

            return null;
        }
    }
}
