﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Microsoft.VisualStudio.TextManager.Interop;
using Microsoft.VisualStudio.Editor;
using Microsoft.VisualStudio.Text.Editor;

namespace SanScriptLanguageMEF.Intellisense.AutoCompletion
{
    /// <summary>
    /// Class containing document specific actions.
    /// </summary>
    [Export(typeof(IDocumentActions))]
    public class DocumentActions : IDocumentActions
    {
        #region Private member fields

        private Dictionary<IWpfTextView, CommandFilter> m_cmdFilters;

        #endregion

        #region MEF Imports

        /// <summary>
        /// MEF import for the editor adapter factory service. Used to get the WPF text view from the supplied IVsTextView.
        /// </summary>
        [Import]
        internal IVsEditorAdaptersFactoryService FactoryService { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public DocumentActions()
        {
            m_cmdFilters = new Dictionary<IWpfTextView, CommandFilter>();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Register a text view with a command filter.
        /// </summary>
        /// <param name="textView">Text view.</param>
        /// <param name="cmdFilter">Command filter.</param>
        public void RegisterFilter(IWpfTextView textView, CommandFilter cmdFilter)
        {
            m_cmdFilters[textView] = cmdFilter;
        }

        /// <summary>
        /// Get the full path to the given header. The current line (where the system caret is located) will contain a valid USING statement
        /// and this will be used to generate the full path.
        /// </summary>
        /// <param name="view">Text view.</param>
        /// <returns>The string containing the full path to the header file. Will contain an empty string if the full path cannot be determined.</returns>
        public string GetHeaderFullPath(IVsTextView view)
        {
            IWpfTextView textView = FactoryService.GetWpfTextView(view);
            if (textView != null)
            {
                CommandFilter cmdFilter = null;
                if (m_cmdFilters.TryGetValue(textView, out cmdFilter))
                {
                    return cmdFilter.GetHeaderFullPath();
                }
            }

            return String.Empty;
        }

        /// <summary>
        /// Checks the line at the system caret and determines whether it contains a USING statement or not.
        /// </summary>
        /// <param name="view">Text view.</param>
        /// <returns>True if the current line at the system caret contains a valid USING statement.</returns>
        public bool LineContainsUsing(IVsTextView view)
        {
            IWpfTextView textView = FactoryService.GetWpfTextView(view);
            if (textView == null)
            {
                return false;
            }

            CommandFilter cmdFilter = null;
            if (m_cmdFilters.TryGetValue(textView, out cmdFilter))
            {
                return cmdFilter.LineContainsUsing();
            }

            return false;
        }

        #endregion
    }
}
