﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.Text;

namespace SanScriptLanguageMEF
{
    internal class MethodParameter : IParameter
    {
        public string Documentation { get; private set; }
        public Span Locus { get; private set; }
        public string Name { get; private set; }
        public ISignature Signature { get; private set; }
        public Span PrettyPrintedLocus { get; private set; }

        public MethodParameter(string documentation, Span locus, string name, ISignature signature)
        {
            //  Store the properties
            Documentation = documentation;
            Locus = locus;
            Name = name;
            Signature = signature;
        }
    }
}
