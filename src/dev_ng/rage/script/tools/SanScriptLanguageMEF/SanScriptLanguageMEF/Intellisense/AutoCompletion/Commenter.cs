﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text;

namespace SanScriptLanguageMEF.Intellisense.AutoCompletion
{
    /// <summary>
    /// Comments/un-comments out lines.
    /// </summary>
    public static class Commenter
    {
        /// <summary>
        /// Toggle comment on / off.
        /// </summary>
        /// <param name="textView">Text view.</param>
        /// <param name="commentOut">True if the block is to be commented.</param>
        public static void ToggleComment(ITextView textView, bool commentOut)
        {
            SnapshotPoint start;
            SnapshotPoint end;
            SnapshotPoint? mappedStart;
            SnapshotPoint? mappedEnd;

            if (textView.Selection.IsActive && !textView.Selection.IsEmpty)
            {
                start = textView.Selection.Start.Position;
                end = textView.Selection.End.Position;
                mappedStart = MapToFirstMatch(textView, start);

                var endLine = end.GetContainingLine();
                if (endLine.Start == end)
                {
                    end = end.Snapshot.GetLineFromLineNumber(endLine.LineNumber - 1).End;
                }

                mappedEnd = MapToFirstMatch(textView, end);
            }
            else
            {
                start = end = textView.Caret.Position.BufferPosition;
                mappedStart = mappedEnd = MapToFirstMatch(textView, start);
            }

            if (mappedStart != null && mappedEnd != null && mappedStart.Value <= mappedEnd.Value)
            {
                if (commentOut)
                {
                    Comment(mappedStart.Value, mappedEnd.Value);
                }
                else
                {
                    UnComment(mappedStart.Value, mappedEnd.Value);
                }
            }
        }

        /// <summary>
        /// Find the column to insert the comment. This makes the output tidy.
        /// </summary>
        /// <param name="start">Start.</param>
        /// <param name="end">End.</param>
        /// <returns>The column number.</returns>
        private static int CalcInsertColumn(SnapshotPoint start, SnapshotPoint end)
        {
            int insertColumn = Int32.MaxValue;
            
            for (int i = start.GetContainingLine().LineNumber; i <= end.GetContainingLine().LineNumber; i++)
            {
                var curLine = start.Snapshot.GetLineFromLineNumber(i);
                var text = curLine.GetText();

                int firstNonWhitespace = text.IndexOfNonWhitespace();
                if (firstNonWhitespace >= 0 && firstNonWhitespace < insertColumn)
                {
                    insertColumn = firstNonWhitespace;
                }
            }

            return insertColumn;
        }

        /// <summary>
        /// Comment out the block.
        /// </summary>
        /// <param name="start">Start.</param>
        /// <param name="end">End.</param>
        private static void Comment(SnapshotPoint start, SnapshotPoint end)
        {
            var snapshot = start.Snapshot;
            int insertColumn = CalcInsertColumn(start, end);

            int startLine = start.GetContainingLine().LineNumber;
            int endLine = end.GetContainingLine().LineNumber;

            using (var textEdit = snapshot.TextBuffer.CreateEdit())
            {
                for (int i = startLine; i <= endLine; i++)
                {
                    var curLine = snapshot.GetLineFromLineNumber(i);
                    if (String.IsNullOrWhiteSpace(curLine.GetText()))
                    {
                        continue;
                    }

                    textEdit.Insert(curLine.Start.Position + insertColumn, "//");
                }

                textEdit.Apply();
            }
        }

        /// <summary>
        /// Un-comment the block.
        /// </summary>
        /// <param name="start">Start.</param>
        /// <param name="end">End.</param>
        private static void UnComment(SnapshotPoint start, SnapshotPoint end)
        {
            System.Diagnostics.Debug.Assert(start.Snapshot == end.Snapshot);
            var snapshot = start.Snapshot;

            using (var textEdit = snapshot.TextBuffer.CreateEdit())
            {
                for (int i = start.GetContainingLine().LineNumber; i <= end.GetContainingLine().LineNumber; i++)
                {
                    var curLine = snapshot.GetLineFromLineNumber(i);
                    RemoveComments(textEdit, curLine);
                }

                textEdit.Apply();
            }
        }

        /// <summary>
        /// Find the SanScript in the text buffer.
        /// </summary>
        /// <param name="view">View.</param>
        /// <param name="point">Starting point.</param>
        /// <returns>A point in a snapshot of the target buffer, or null if position does not map down to any buffer that contains SanScript.</returns>
        private static SnapshotPoint? MapToFirstMatch(ITextView textView, SnapshotPoint point)
        {
            return textView.BufferGraph.MapDownToFirstMatch(point, PointTrackingMode.Positive, IsSanScriptContent, PositionAffinity.Successor);
        }

        /// <summary>
        /// Checks the content of the buffer and returns true if it contains SanScript text.
        /// </summary>
        /// <param name="buffer">Buffer.</param>
        /// <returns>True if the buffer contains SanScript text.</returns>
        private static bool IsSanScriptContent(ITextSnapshot buffer)
        {
            return buffer.ContentType.IsOfType("SanScript");
        }

        /// <summary>
        /// Delete the comments from the line.
        /// </summary>
        /// <param name="textEdit">Text edit.</param>
        /// <param name="snapshotLine">Snapshot line.</param>
        private static void RemoveComments(ITextEdit textEdit, ITextSnapshotLine snapshotLine)
        {
            var text = snapshotLine.GetText();
            for (int i = 0; i < text.Length; i++)
            {
                if (!Char.IsWhiteSpace(text[i]) && (i < text.Length - 2 && !Char.IsWhiteSpace(text[i + 1])))
                {
                    if ((text[i] == '/' && text[i + 1] == '/') || (text[i] == '/' && text[i + 1] == '*'))
                    {
                        textEdit.Delete(snapshotLine.Start.Position + i, 2);
                    }
                    break;
                }
            }

            for (int i = text.Length - 1; i >= 0; i--)
            {
                if (!Char.IsWhiteSpace(text[i]) && (i > 0 && !Char.IsWhiteSpace(text[i - 1])))
                {
                    if ((text[i] == '/' && text[i - 1] == '*'))
                    {
                        textEdit.Delete(snapshotLine.Start.Position + (i - 1), 2);
                    }
                    break;
                }
            }
        }
    }

    #region String extensions

    /// <summary>
    /// String extension methods.
    /// </summary>
    internal static class StringExtensions
    {
        /// <summary>
        /// Index of non-whitespace character.
        /// </summary>
        /// <param name="text">Text.</param>
        /// <returns>The index of the first non-whitespace character, or -1 if there are no non-white space characters.</returns>
        public static int IndexOfNonWhitespace(this string text)
        {
            int index = -1;

            for (int i = 0; i < text.Length; i++)
            {
                if (!Char.IsWhiteSpace(text[i]))
                {
                    index = i;
                    break;
                }
            }

            return index;
        }
    }

#endregion
}
