﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SanScriptLanguageMEF
{
    public class OutliningPair
    {
        private string m_start;     //  The string that starts the outlining region
        private string m_end;       //  The string that ends the outlining region

        public OutliningPair(string start, string end)
        {
            m_start = start;
            m_end = end;
        }

        public string GetStart()
        {
            return m_start;
        }

        public string GetEnd()
        {
            return m_end;
        }
    }
}
