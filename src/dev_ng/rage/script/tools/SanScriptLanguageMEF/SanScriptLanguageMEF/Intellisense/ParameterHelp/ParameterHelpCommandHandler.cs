﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.OLE.Interop;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Operations;
using Microsoft.VisualStudio.TextManager.Interop;
using SanScriptLanguageMEF.Intellisense.ParameterHelp.DefinitionJumper;

namespace SanScriptLanguageMEF
{
    internal sealed class ParameterHelpCommandHandler : IOleCommandTarget
    {
        IOleCommandTarget m_nextCommandHandler;
        ITextView m_textView;
        ISignatureHelpBroker m_broker;
        ISignatureHelpSession m_session;
        ITextStructureNavigator m_navigator;

        internal ParameterHelpCommandHandler(IVsTextView textViewAdapter, ITextView textView, ITextStructureNavigator nav, ISignatureHelpBroker broker)
        {
            m_textView = textView;
            m_broker = broker;
            m_navigator = nav;

            //  Add this to the filter chain
            textViewAdapter.AddCommandFilter(this, out m_nextCommandHandler);
        }

        public int Exec(ref Guid pGuidCmdGroup, uint nCmdID, uint nCmdExecOpt, IntPtr pvaIn, IntPtr pvaOut)
        {
            char typedChar = char.MinValue;

            if (pGuidCmdGroup == VSConstants.VSStd2K)
            {
                switch ((VSConstants.VSStd2KCmdID)nCmdID)
                {
                    case VSConstants.VSStd2KCmdID.TYPECHAR:
                    {
                        typedChar = (char)(ushort)Marshal.GetObjectForNativeVariant(pvaIn);
                        if (typedChar.Equals('('))
                        {
                            AddSession();
                        }
                        else if (typedChar.Equals(')') && m_session != null)
                        {
                            Cancel();
                        }
                        else if (typedChar.Equals(',') && m_session != null)
                        {
                            MethodSignature sig = m_session.SelectedSignature as MethodSignature;
                            int currentParameter = sig.ComputeCurrentParameter() + 1;
                            if (currentParameter >= sig.Parameters.Count)
                            {
                                foreach (ISignature newSig in m_session.Signatures)
                                {
                                    if (newSig.Parameters.Count > currentParameter)
                                    {
                                        m_session.SelectedSignature = newSig;
                                    }
                                }
                            }
                        }
                    }
                    break;
                    case VSConstants.VSStd2KCmdID.BACKSPACE:
                    {
                        if (m_session != null)
                        {
                            SnapshotPoint point = m_textView.Caret.Position.BufferPosition - 1;
                            TextExtent extent = m_navigator.GetExtentOfWord(point);
                            string word = extent.Span.GetText();

                            //  If we are deleting the open parenthesis then cancel the current session
                            if (word.Equals("("))
                            {
                                Cancel();
                            }
                        }
                    }
                    break;
                }
            }
            else if (pGuidCmdGroup == VSConstants.CMDSETID.StandardCommandSet97_guid)
            {
                switch ((VSConstants.VSStd97CmdID)nCmdID)
                {
                    case VSConstants.VSStd97CmdID.GotoDefn:
                    {
                        //  Get the word at the current cursor position
                        SnapshotPoint point = m_textView.Caret.Position.BufferPosition;
                        TextExtent extent = m_navigator.GetExtentOfWord(point);
                        string word = extent.Span.GetText();

                        //  Try and find the word from parsed files
                        List<Identifier> identifiers = SanScriptParser.FindIdentifiers(word);
                        List<Identifier> prunedList = Prune(identifiers);

                        if (prunedList.Count == 1)
                        {
                            prunedList[0].GotoDefinition();
                        }
                        else if (prunedList.Count > 0)
                        {
                            Identifier identifier = GotoDefinitionFactory.ShowDialog(prunedList);
                            if (identifier != null)
                            {
                                identifier.GotoDefinition();
                            }
                        }

                        //  Indicate that we handled the Exec so the shell doesn't continue looking for handlers
                        return (int)VSConstants.S_OK;
                    }
                }
            }

            return m_nextCommandHandler.Exec(ref pGuidCmdGroup, nCmdID, nCmdExecOpt, pvaIn, pvaOut);
        }

        /// <summary>
        /// Prune the list.
        /// </summary>
        /// <param name="identifiers">Identifiers.</param>
        /// <returns>Pruned identifiers list.</returns>
        private List<Identifier> Prune(List<Identifier> identifiers)
        {
            List<Identifier> prunedList = new List<Identifier>();
            if (identifiers.Count > 0)
            {
                prunedList.Add(identifiers[0]);
                foreach (var ident in identifiers)
                {
                    var existing = prunedList.FirstOrDefault(id => ident.ContainingFile.ToLower() == id.ContainingFile.ToLower() && ident.Line == id.Line && ident.Column == id.Column);
                    if (existing == null)
                    {
                        prunedList.Add(ident);
                    }
                }
            }

            return prunedList;
        }

        bool AddSession()
        {
            //  Create a new session
            m_session = m_broker.TriggerSignatureHelp(m_textView);

            return true;
        }

        bool Cancel()
        {
            //  Dont do anything if we dont have a current session
            if (m_session == null)
            {
                return false;
            }

            //  Dismiss the current session
            m_session.Dismiss();
            m_session = null;

            return true;
        }

        public int QueryStatus(ref Guid pGuidCmdGroup, uint cCmds, OLECMD[] prgCmds, IntPtr pCmdText)
        {
            if (pGuidCmdGroup == VSConstants.CMDSETID.StandardCommandSet97_guid)
            {
                switch ((VSConstants.VSStd97CmdID)prgCmds[0].cmdID)
                {
                    case VSConstants.VSStd97CmdID.GotoDefn:
                    {
                        //  Say that the Alt+G (Goto Definition) command is supported and enabled
                        prgCmds[0].cmdf = (uint)(OLECMDF.OLECMDF_ENABLED | OLECMDF.OLECMDF_SUPPORTED);
                        //  Indicate that we handled the QueryStatus, so the shell doesn't continue looking for handlers
                        return VSConstants.S_OK;
                    }
                    break;
                }
            }

            return m_nextCommandHandler.QueryStatus(ref pGuidCmdGroup, cCmds, prgCmds, pCmdText);
        }
    }
}
