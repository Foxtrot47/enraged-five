﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

using Microsoft.VisualStudio.Utilities;
using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.Text;

namespace SanScriptLanguageMEF
{
    [Export(typeof(ISignatureHelpSourceProvider))]
    [ContentType("SanScript")]
    [Name("SanScriptParameterHelp")]
    [Order(Before = "default")]
    class ParameterHelpSourceProvider : ISignatureHelpSourceProvider
    {
        public ISignatureHelpSource TryCreateSignatureHelpSource(ITextBuffer textBuffer)
        {
            return new ParameterHelpSource(textBuffer);
        }
    }
}
