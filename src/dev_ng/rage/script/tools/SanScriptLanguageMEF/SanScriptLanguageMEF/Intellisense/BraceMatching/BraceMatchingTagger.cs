﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.Composition;

//using Microsoft.VisualStudio.Editor;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Operations;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Utilities;

namespace SanScriptLanguageMEF
{
    internal class BraceMatchingTagger : ITagger<TextMarkerTag>
    {
        ITextView View { get; set; }
        ITextBuffer SourceBuffer { get; set; }
        SnapshotPoint? CurrentString { get; set; }

        private BraceMatchingTaggerProvider m_provider = null;

        public event EventHandler<SnapshotSpanEventArgs> TagsChanged;

        internal BraceMatchingTagger(ITextView view, ITextBuffer sourceBuffer, BraceMatchingTaggerProvider provider)
        {
            this.View = view;
            this.SourceBuffer = sourceBuffer;
            this.CurrentString = null;

            this.m_provider = provider;

            this.View.Caret.PositionChanged += CaretPositionChanged;
            this.View.LayoutChanged += ViewLayoutChanged;
        }

        void ViewLayoutChanged(object sender, TextViewLayoutChangedEventArgs e)
        {
            if (e.NewSnapshot != e.OldSnapshot) //make sure that there has really been a change
            {
                UpdateAtCaretPosition(View.Caret.Position);
            }
        }

        void CaretPositionChanged(object sender, CaretPositionChangedEventArgs e)
        {
            UpdateAtCaretPosition(e.NewPosition);
        }

        void UpdateAtCaretPosition(CaretPosition caretPosition)
        {
            CurrentString = caretPosition.Point.GetPoint(SourceBuffer, caretPosition.Affinity);

            if (!CurrentString.HasValue)
            {
                return;
            }

            var tempEvent = TagsChanged;
            if (tempEvent != null)
            {
                tempEvent(this, new SnapshotSpanEventArgs(new SnapshotSpan(SourceBuffer.CurrentSnapshot, 0,
                    SourceBuffer.CurrentSnapshot.Length)));
            }
        }

        public IEnumerable<ITagSpan<TextMarkerTag>> GetTags(NormalizedSnapshotSpanCollection spans)
        {
            if (spans.Count == 0)   //there is no content in the buffer
            {
                yield break;
            }

            //don't do anything if the current SnapshotPoint is not initialized or at the end of the buffer
            if (!CurrentString.HasValue || CurrentString.Value.Position >= CurrentString.Value.Snapshot.Length)
            {
                yield break;
            }

            //hold on to a snapshot of the current string
            SnapshotPoint currentString = CurrentString.Value;

            //if the requested snapshot isn't the same as the one the brace is on, translate our spans to the expected snapshot
            if (spans[0].Snapshot != currentString.Snapshot)
            {
                currentString = currentString.TranslateTo(spans[0].Snapshot, PointTrackingMode.Positive);
            }

            //  Try and get the word/character the caret is on
            ITextStructureNavigator navigator = m_provider.NavigatorService.GetTextStructureNavigator(SourceBuffer);
            TextExtent extent = navigator.GetExtentOfWord(CurrentString.Value);
            string currentText = extent.Span.GetText();

            SnapshotSpan pairSpan;
            KeyValuePair<string, string> matchingPair;
            int offset;

            //  Need to do the closer before the opener, or things like 'IF'->'ENDIF' cause problems
            if (SanScriptParser.FindBraceMatchingPairFromValue(currentText, currentString.Position - extent.Span.Start.Position, out matchingPair, out offset))
            {
                SnapshotPoint newPoint = new SnapshotPoint(extent.Span.Snapshot, extent.Span.Start.Position + offset);
                if (BraceMatchingTagger.FindMatchingOpenString(newPoint, matchingPair.Key, matchingPair.Value, View.TextViewLines.Count, out pairSpan) == true)
                {
                    //  Opener
                    yield return new TagSpan<TextMarkerTag>(pairSpan, new TextMarkerTag("bracehighlight"));
                    //  Closer
                    yield return new TagSpan<TextMarkerTag>(new SnapshotSpan(newPoint, matchingPair.Value.Length), new TextMarkerTag("bracehighlight"));
                }
            }
            else if (SanScriptParser.FindBraceMatchingPairFromKey(currentText, currentString.Position - extent.Span.Start.Position, out matchingPair, out offset))
            {
                SnapshotPoint newPoint = new SnapshotPoint(extent.Span.Snapshot, extent.Span.Start.Position + offset);
                if (BraceMatchingTagger.FindMatchingCloseString(newPoint, matchingPair.Key, matchingPair.Value, View.TextViewLines.Count, out pairSpan) == true)
                {
                    //  Opener
                    yield return new TagSpan<TextMarkerTag>(new SnapshotSpan(newPoint, matchingPair.Key.Length), new TextMarkerTag("bracehighlight"));
                    //  Closer
                    yield return new TagSpan<TextMarkerTag>(pairSpan, new TextMarkerTag("bracehighlight"));
                }
            }
        }

        private static bool FindMatchingCloseString(SnapshotPoint startPoint, string open, string close, int maxLines, out SnapshotSpan pairSpan)
        {
            pairSpan = new SnapshotSpan(startPoint.Snapshot, 1, 1);
            ITextSnapshotLine line = startPoint.GetContainingLine();
            string lineText = line.GetText();
            int lineNumber = line.LineNumber;
            int offset = startPoint.Position - line.Start.Position + 1;

            int stopLineNumber = startPoint.Snapshot.LineCount - 1;
            if (maxLines > 0)
            {
                stopLineNumber = Math.Min(stopLineNumber, lineNumber + maxLines);
            }

            int openCount = 0;
            while (true)
            {
                //  Find the right closer
                int startOffset = offset;
                int endOffset = offset;
                while (startOffset >= 0 && startOffset < line.Length)
                {
                    endOffset = lineText.IndexOf(close, startOffset);
                    startOffset = lineText.IndexOf(open, startOffset);

                    if (startOffset >= 0 && (startOffset < endOffset || endOffset < 0))
                    {
                        //  This is another opener
                        openCount++;
                        startOffset++;
                    }
                    else
                    {
                        //  Possible closer
                        if (endOffset >= 0)
                        {
                            if (openCount > 0)
                            {
                                //  This is a closer to another opener, not the one we want
                                openCount--;
                                startOffset = endOffset + close.Length;
                            }
                            else
                            {
                                //  This must be the one we want
                                pairSpan = new SnapshotSpan(startPoint.Snapshot, line.Start + endOffset, close.Length);
                                return true;
                            }
                        }
                    }
                }

                //  Move on to the next line
                if (++lineNumber > stopLineNumber)
                {
                    break;
                }

                line = line.Snapshot.GetLineFromLineNumber(lineNumber);
                lineText = line.GetText();
                offset = 0;
            }

            return false;
        }

        private static bool FindMatchingOpenString(SnapshotPoint startPoint, string open, string close, int maxLines, out SnapshotSpan pairSpan)
        {
            pairSpan = new SnapshotSpan(startPoint, startPoint);
            ITextSnapshotLine line = startPoint.GetContainingLine();
            int lineNumber = line.LineNumber;
            int offset = startPoint - line.Start;

            string lineText = line.GetText();

            int stopLineNumber = 0;
            if (maxLines > 0)
            {
                stopLineNumber = Math.Max(stopLineNumber, lineNumber - maxLines);
            }

            int closeCount = 0;
            while (true)
            {
                //  Find the right opener
                int startOffset = 0;
                int endOffset = 0;

                Dictionary<int, string>itemOffsetList = new Dictionary<int, string>();

                //  Find all the openers and closers on the line
                while (startOffset >= 0 || endOffset >= 0)
                {
                    endOffset = lineText.IndexOf(close, endOffset, StringComparison.OrdinalIgnoreCase);
                    startOffset = lineText.IndexOf(open, startOffset, StringComparison.OrdinalIgnoreCase);

                    if (endOffset >= 0 && (endOffset < startOffset || startOffset < 0))
                    {
                        //  Do the closer first
                        if (endOffset < offset)
                        {
                            //  Add it to the list
                            itemOffsetList.Add(endOffset, close);
                        }
                        //  Move past it
                        endOffset += close.Length;
                        startOffset = endOffset;
                    }
                    else if (startOffset >= 0)
                    {
                        //  Do the opener first
                        if (startOffset < offset)
                        {
                            //  Add it to the list
                            itemOffsetList.Add(startOffset, open);
                        }
                        //  Move past it
                        startOffset += open.Length;
                        endOffset = startOffset;
                    }
                }

                for (int i = itemOffsetList.Count - 1; i >= 0; i--)
                {
                    if (itemOffsetList.ElementAt(i).Value == close)
                    {
                        //  This is a closer for a different opener
                        closeCount++;
                    }
                    else if (itemOffsetList.ElementAt(i).Value == open)
                    {
                        //  If we have other closers then this will be an opener to one of those
                        if (closeCount > 0)
                        {
                            closeCount--;
                        }
                        else
                        {
                            //  This must be the opener we are looking for
                            pairSpan = new SnapshotSpan(startPoint.Snapshot, line.Start + itemOffsetList.ElementAt(i).Key, open.Length);
                            return true;
                        }
                    }
                }

                //  Move on to the previous line
                if (lineNumber-- < stopLineNumber)
                {
                    break;
                }

                line = line.Snapshot.GetLineFromLineNumber(lineNumber);
                lineText = line.GetText();
                offset = line.Length - 1;
            }
            return false;
        }
    }
}
