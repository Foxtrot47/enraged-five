﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.Shell;

using EnvDTE;

namespace SanScriptLanguageMEF
{
    public class Identifier
    {
        public enum eIDENTIFIER_TYPE
        {
            IDENTIFIER_TYPE_VARIABLE = 0, 
            IDENTIFIER_TYPE_CONST_VARIABLE, 
            IDENTIFIER_TYPE_NATIVEFUNCTION,
            IDENTIFIER_TYPE_FUNCTION, 
            IDENTIFIER_TYPE_ENUM_TYPE, 
            IDENTIFIER_TYPE_ENUM_MEMBER, 
            IDENTIFIER_TYPE_USER_TYPE, 
            IDENTIFIER_TYPE_STRUCT
        };

        private string m_name = null;
        private eIDENTIFIER_TYPE m_type = eIDENTIFIER_TYPE.IDENTIFIER_TYPE_VARIABLE;
        private string m_signature = null;
        private string m_containingFile = null;
        private int m_containingFileLine = 0;
        private int m_containingFileColumn = 0;

        /// <summary>
        /// The containing file.
        /// </summary>
        public string ContainingFile
        {
            get { return m_containingFile; }
        }

        /// <summary>
        /// The line the identifier is defined.
        /// </summary>
        public int Line
        {
            get { return m_containingFileLine; }
        }

        /// <summary>
        /// The column the identifier is defined.
        /// </summary>
        public int Column
        {
            get { return m_containingFileColumn; }
        }

        /// <summary>
        /// Identifier class constructor
        /// </summary>
        /// <param name="name">The name of the identifier</param>
        /// <param name="type">The identifier type</param>
        public Identifier(string name, eIDENTIFIER_TYPE type)
        {
            //  Store the name and type
            m_name = name;
            m_type = type;
        }

        /// <summary>
        /// Identifier class constructor
        /// </summary>
        /// <param name="name">The name of the identifier</param>
        /// <param name="type">The identifier type</param>
        public Identifier(string name, eIDENTIFIER_TYPE type, string signature)
        {
            //  Store the name and type
            m_name = name;
            m_type = type;
            //  Store the signature
            m_signature = signature;
        }

        /// <summary>
        /// Jump to the containing file, line and column for this identifier
        /// </summary>
        public void GotoDefinition()
        {
            DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;

            //  If the containing file is empty then it should be this file
            if (m_containingFile == string.Empty)
            {
                //  Set containing file to this file
                m_containingFile = dte.ActiveDocument.FullName;
            }
            else
            {
                //  Open the file
                dte.ItemOperations.OpenFile(m_containingFile);
            }

            string fullFilePath = System.IO.Path.GetFullPath(dte.ActiveDocument.FullName);
            string containingFilePath = System.IO.Path.GetFullPath(m_containingFile);

            //  Move to the correct line and column
            if (string.Compare(fullFilePath, containingFilePath, true) == 0)
            {
                TextSelection selection = dte.ActiveDocument.Selection;
                selection.MoveToLineAndOffset(m_containingFileLine, m_containingFileColumn);
            }
        }

        /// <summary>
        /// Sets the filename for the file this identifier was found in
        /// </summary>
        /// <param name="filename">The filename to set</param>
        public void SetContainingFile(string filename, int line, int column)
        {
            m_containingFile = filename;
            //  Line is plus 1, as it is stored zero based but used 1 based (?)
            m_containingFileLine = line + 1;
            m_containingFileColumn = column;
        }

        /// <summary>
        /// Get the identifier name
        /// </summary>
        /// <returns>The identifier name</returns>
        public string GetName()
        {
            return m_name;
        }

        /// <summary>
        /// Get the identifier type
        /// </summary>
        /// <returns>The identifier type</returns>
        public eIDENTIFIER_TYPE GetIdentifierType()
        {
            return m_type;
        }

        /// <summary>
        /// Get the signature string
        /// </summary>
        /// <returns>The signature string</returns>
        public string GetSignature()
        {
            return m_signature;
        }

        /// <summary>
        /// Gets the icon to use in the intellisense completion list
        /// </summary>
        /// <param name="glyphService">The glyph service to use to get the icon</param>
        /// <returns>The image source icon to use for this identifier</returns>
        public ImageSource GetIcon(IGlyphService glyphService)
        {
            switch (m_type)
            {
                case eIDENTIFIER_TYPE.IDENTIFIER_TYPE_VARIABLE:
                {
                    return glyphService.GetGlyph(StandardGlyphGroup.GlyphGroupVariable, StandardGlyphItem.GlyphItemPublic);
                }
                case eIDENTIFIER_TYPE.IDENTIFIER_TYPE_CONST_VARIABLE:
                {
                    return glyphService.GetGlyph(StandardGlyphGroup.GlyphGroupConstant, StandardGlyphItem.GlyphItemPublic);
                }
                case eIDENTIFIER_TYPE.IDENTIFIER_TYPE_FUNCTION:
                {
                    return glyphService.GetGlyph(StandardGlyphGroup.GlyphGroupMethod, StandardGlyphItem.GlyphItemPublic);
                }
                case eIDENTIFIER_TYPE.IDENTIFIER_TYPE_ENUM_TYPE:
                {
                    return glyphService.GetGlyph(StandardGlyphGroup.GlyphGroupEnum, StandardGlyphItem.GlyphItemPublic);
                }
                case eIDENTIFIER_TYPE.IDENTIFIER_TYPE_ENUM_MEMBER:
                {
                    return glyphService.GetGlyph(StandardGlyphGroup.GlyphGroupEnumMember, StandardGlyphItem.GlyphItemPublic);
                }
                case eIDENTIFIER_TYPE.IDENTIFIER_TYPE_USER_TYPE:
                {
                    return glyphService.GetGlyph(StandardGlyphGroup.GlyphGroupTypedef, StandardGlyphItem.GlyphItemPublic);
                }
                case eIDENTIFIER_TYPE.IDENTIFIER_TYPE_STRUCT:
                {
                    return glyphService.GetGlyph(StandardGlyphGroup.GlyphGroupStruct, StandardGlyphItem.GlyphItemPublic);
                }
                default:
                {
                    return glyphService.GetGlyph(StandardGlyphGroup.GlyphGroupUnknown, StandardGlyphItem.GlyphItemPublic);
                }
            }
        }
    }
}
