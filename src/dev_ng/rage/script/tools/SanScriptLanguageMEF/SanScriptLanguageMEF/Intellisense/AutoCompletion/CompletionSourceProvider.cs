﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

using Microsoft.VisualStudio.Utilities;
using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.Text;

namespace SanScriptLanguageMEF
{
    [Export(typeof(ICompletionSourceProvider))]
    [ContentType("SanScript")]
    [Name("SanScriptCompletion")]
    class CompletionSourceProvider : ICompletionSourceProvider
    {
        [Import]
        private IGlyphService GlyphService { get; set; }

        [Import]
        private ILanguagePreferences LangPrefs { get; set; }

        /// <summary>
        /// Create a CompletionSource to use when completing a word
        /// </summary>
        /// <param name="textBuffer">The text containing the word to be completed</param>
        /// <returns>A new CompletionSource instance</returns>
        public ICompletionSource TryCreateCompletionSource(ITextBuffer textBuffer)
        {
            bool createCompletionSource = (LangPrefs == null || (LangPrefs != null && LangPrefs.AutoListMembers));

            if (createCompletionSource)
            {
                return new CompletionSource(textBuffer, GlyphService);
            }
            else
            {
                return null;
            }
        }
    }
}
