﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SanScriptLanguageMEF.Intellisense.ParameterHelp.DefinitionJumper
{
    /// <summary>
    /// Go to definition form.
    /// </summary>
    public partial class GotoDefinitionForm : Form
    {
        #region Constructor 

        /// <summary>
        /// Constructor.
        /// </summary>
        public GotoDefinitionForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Simulate the click of the OK button when the user double clicks an item.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void lstDefinitions_DoubleClick(object sender, EventArgs e)
        {
            btnOK.PerformClick();
        }

        #endregion
    }
}
