﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

using Microsoft.VisualStudio.Editor;
using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.TextManager.Interop;
using Microsoft.VisualStudio.Utilities;
using Microsoft.VisualStudio.OLE.Interop;
using Microsoft.VisualStudio.Text.Operations;

namespace SanScriptLanguageMEF
{
    [Export(typeof(IVsTextViewCreationListener))]
    [ContentType("SanScript")]
    [TextViewRole(PredefinedTextViewRoles.Interactive)]
    internal sealed class TextViewCreationListener : IVsTextViewCreationListener
    {
        [Import]
        internal IVsEditorAdaptersFactoryService AdaptersFactory = null;

        [Import]
        internal ICompletionBroker CompletionBroker = null;

        [Import]
        internal ITextStructureNavigatorSelectorService NavigatorService { get; set; }

        [Import]
        internal ISignatureHelpBroker SignatureHelpBroker;

        [Import]
        internal IDocumentActions DocumentActions { get; set; }

        [Import]
        internal IContextMenuActions ContextMenuActions { get; set; }

        [Import]
        public IParamInfoPopupFactory ParamInfoPopupFactory { get; set; }

        public void VsTextViewCreated(IVsTextView textViewAdapter)
        {
            IWpfTextView view = AdaptersFactory.GetWpfTextView(textViewAdapter);
            Debug.Assert(view != null);

            //  Add the command filter for the auto completion
            CommandFilter filter = new CommandFilter(view, CompletionBroker, ParamInfoPopupFactory.Create(new PopupHelper(view), textViewAdapter));
            DocumentActions.RegisterFilter(view, filter);

            IOleCommandTarget next;
            textViewAdapter.AddCommandFilter(filter, out next);
            filter.Next = next;

            //  Add the parameter help handler
            view.Properties.GetOrCreateSingletonProperty(() => new ParameterHelpCommandHandler(textViewAdapter, view, NavigatorService.GetTextStructureNavigator(view.TextBuffer), SignatureHelpBroker));
        }
    }
}
