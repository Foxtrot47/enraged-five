﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SanScriptLanguageMEF.Intellisense.ParameterHelp.DefinitionJumper
{
    /// <summary>
    /// Controller for the list box.
    /// </summary>
    public class Controller
    {
        #region Private member fields

        private List<Identifier> m_identifiers;
        private ListBox m_listBox;

        #endregion

        #region Public properties

        /// <summary>
        /// The selected identifier.
        /// </summary>
        public Identifier Selected
        {
            get
            {
                return m_identifiers[m_listBox.SelectedIndex];
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="listBox">List box.</param>
        /// <param name="identifiers">Identifiers.</param>
        public Controller(ListBox listBox, List<Identifier> identifiers)
        {
            m_listBox = listBox;
            m_identifiers = identifiers;
            FillList();
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Fill the list and set the first item to be selected.
        /// </summary>
        /// <param name="identifiers">Identifiers.</param>
        private void FillList()
        {
            m_listBox.Items.Clear();

            foreach (var identifier in m_identifiers)
            {
                string ident = identifier.GetName();
                if (!String.IsNullOrEmpty(identifier.ContainingFile))
                {
                    ident += " : " + identifier.ContainingFile + " {" + identifier.Line + ", " + identifier.Column + "}";
                }

                m_listBox.Items.Add(ident);
            }

            m_listBox.SelectedIndex = 0;
        }

        #endregion
    }
}
