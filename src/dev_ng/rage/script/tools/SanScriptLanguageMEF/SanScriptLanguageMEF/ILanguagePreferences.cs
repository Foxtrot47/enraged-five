﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SanScriptLanguageMEF
{
    /// <summary>
    /// Language preferences.
    /// </summary>
    public interface ILanguagePreferences
    {
        /// <summary>
        /// True if Intellisense is to be used.
        /// </summary>
        bool AutoListMembers { get; }
    }
}
