﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.Text.Editor;

namespace SanScriptLanguageMEF
{
    /// <summary>
    /// Reads the SanScript parser and obtains information about the current line's method. 
    /// </summary>
    public class PopupHelper : IPopupHelper
    {
        #region Private member fields

        private IWpfTextView m_wpfTextView;
        private List<ParameterDeclaration> m_paramDecls;

        #endregion

        #region Public properties

        /// <summary>
        /// Method signature.
        /// </summary>
        public string Signature { get; private set; }

        /// <summary>
        /// Method name.
        /// </summary>
        public string MethodName { get; private set; }

        /// <summary>
        /// Current parameter. Zero-based index value of the current parameter.
        /// </summary>
        public int CurrentParameter { get; private set; }

        /// <summary>
        /// The number of method overloads.
        /// </summary>
        public int OverloadCount { get; private set; }

        /// <summary>
        /// The number of parameters.
        /// </summary>
        public int ParameterCount { get; private set; }

        /// <summary>
        /// Caret position.
        /// </summary>
        public int Position { get; private set; }

        /// <summary>
        /// Length.
        /// </summary>
        public int Length { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="wpfTextView"></param>
        public PopupHelper(IWpfTextView wpfTextView)
        {
            m_wpfTextView = wpfTextView;
            m_paramDecls = new List<ParameterDeclaration>();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Update the properties for the current editor line.
        /// </summary>
        public void Update()
        {
            var line = m_wpfTextView.TextSnapshot.GetLineFromPosition(m_wpfTextView.Caret.Position.BufferPosition.Position);
            int caretPos = m_wpfTextView.Caret.Position.BufferPosition.Position - line.Start.Position;
            
            string methodName = String.Empty;
            int paramIdx = -1;

            OverloadCount = 1;
            Position = 0;
            Length = 0;

            if (PopupParser.ParseLine(line.GetText(), caretPos, out methodName, out paramIdx))
            {
                var identifier = SanScriptParser.FindIdentifier(methodName);

                if (identifier != null)
                {
                    MethodName = methodName;
                    Signature = identifier.GetSignature();

                    m_paramDecls.Clear();
                    foreach (var paramDecl in SanScriptParser.MethodCache.GetMethodParams(identifier.GetName()))
                    {
                        m_paramDecls.Add(paramDecl);
                    }
                }

                CurrentParameter = paramIdx;
                ParameterCount = m_paramDecls.Count;
                Position = m_wpfTextView.Caret.Position.BufferPosition.Position;
                Length = MethodName.Length;
            }
        }

        /// <summary>
        /// Gets the parameter name for the supplied parameter index.
        /// </summary>
        /// <param name="paramIndex">Zero based index into the parameter list.</param>
        /// <returns>The parameter name for the supplied parameter index.</returns>
        public string GetParameterName(int paramIndex)
        {
            return m_paramDecls.Count == 0 ? "" : m_paramDecls[paramIndex].Name;
        }

        /// <summary>
        /// Get the parameter type for the supplied parameter index.
        /// </summary>
        /// <param name="paramIndex">Zero based index into the parameter list.</param>
        /// <returns>The parameter type for the supplied parameter index.</returns>
        public string GetParameterType(int paramIndex)
        {
            return m_paramDecls.Count == 0 ? "" : m_paramDecls[paramIndex].DataType + m_paramDecls[paramIndex].DataTypePostFix;
        }

        #endregion
    }
}
