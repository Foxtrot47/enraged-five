﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace SanScriptLanguageMEF
{
    internal static class FileAndContentTypeDefinitions
    {
        [Export]
        [Name("SanScript")]
        [BaseDefinition("code")]
        internal static ContentTypeDefinition sanScriptContentTypeDefinition = null;

        [Export]
        [FileExtension(".sc")]
        [ContentType("SanScript")]
        internal static FileExtensionToContentTypeDefinition sanScriptFileExtensionDefinition = null;

        [Export]
        [FileExtension(".sch")]
        [ContentType("SanScript")]
        internal static FileExtensionToContentTypeDefinition sanScriptHeaderFileExtensionDefinition = null;
    }
}
