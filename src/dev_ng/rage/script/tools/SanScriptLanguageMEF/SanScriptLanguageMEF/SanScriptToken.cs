﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;

namespace SanScriptLanguageMEF
{
    public class SanScriptToken
    {
        //  The type of token
        private TokenTypes m_type = TokenTypes.TOKEN_TYPE_ERROR;
        //  The position and length of the token
        private int m_position = 0;
        private int m_length = 0;

        public SanScriptToken(int position, int length, TokenTypes type)
        {
            //  Store the position, length and type
            m_position = position;
            m_length = length;
            m_type = type;
        }

        public TokenTypes GetTokenType()
        {
            return m_type;
        }

        public int GetPosition()
        {
            return m_position;
        }

        public int GetLength()
        {
            return m_length;
        }
    }
}
