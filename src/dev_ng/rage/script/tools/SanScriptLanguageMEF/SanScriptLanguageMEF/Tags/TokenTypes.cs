﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SanScriptLanguageMEF
{
    /// <summary>
    /// The different types of tokens we can have in the SanScript Language
    /// </summary>
    public enum TokenTypes
    {
        TOKEN_TYPE_DEFAULT,                 //  Basic token
        TOKEN_TYPE_COMMENT,                 //  Line and multiline comments
        TOKEN_TYPE_KEYWORD,                 //  Keywords such as USING, ENUM, VECTOR, etc
        TOKEN_TYPE_METHOD,                  //  Method/Function names
        TOKEN_TYPE_STRING,                  //  Strings
        TOKEN_TYPE_NUMBER,                  //  Number
        TOKEN_TYPE_VECTOR,                  //  Vector value (<<1.0, 0.0, 1.0>>)
        TOKEN_TYPE_IDENTIFIER,              //  Identifier such as variables
        TOKEN_TYPE_NATIVEMETHOD,            //  Native procedure / function
        TOKEN_TYPE_NATIVETYPE,              //  Native type e.g. INT, BOOL etc.
        TOKEN_TYPE_OPERATOR,                //  Operator
        TOKEN_TYPE_PREPROCESSOR,            //  Pre-processor directive
        TOKEN_TYPE_ERROR                    //  Error token (red squiggles underneath words)
    }
}
