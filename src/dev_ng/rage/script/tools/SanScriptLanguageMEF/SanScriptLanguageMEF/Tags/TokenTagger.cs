﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Text;

namespace SanScriptLanguageMEF
{
    internal sealed class TokenTagger : ITagger<TokenTag>
    {
        //  Buffer to store the text we are tagging
        ITextBuffer m_buffer;

        /// <summary>
        /// TokenTagger class constructor
        /// </summary>
        /// <param name="buffer">The text buffer to tag</param>
        internal TokenTagger(ITextBuffer buffer)
        {
            //  Store the text buffer
            m_buffer = buffer;

            //  Reset the stored tag spans and request an immediate reparse
            //SanScriptParser.ClearTagSpans();
            //SanScriptParser.RequestParse(buffer.CurrentSnapshot, 1);
            //  Add our own event handler for when the buffer changes
            m_buffer.Changed += new EventHandler<TextContentChangedEventArgs>(BufferChanged);
        }

        /// <summary>
        /// Event handler to fire when the buffer is changed
        /// </summary>
        /// <param name="sender">The object firing the event</param>
        /// <param name="e">Event arguments</param>
        public void BufferChanged(object sender, TextContentChangedEventArgs e)
        {
            //  Request a reparse, but delay for a second in case of other changes
            SanScriptParser.RequestParse(e.After, 1000);
        }

        /// <summary>
        /// Event handler for when things change
        /// </summary>
        public event EventHandler<SnapshotSpanEventArgs> TagsChanged
        {
            add { }
            remove { }
        }

        /// <summary>
        /// Iterator function to get tags from a span of text
        /// </summary>
        /// <param name="spans">The spans of text to look at</param>
        /// <returns>A new tag span containing the tag span and token type</returns>
        public IEnumerable<ITagSpan<TokenTag>> GetTags(NormalizedSnapshotSpanCollection spans)
        {
            //  Return each tag span to the iterator
            List<ITagSpan<TokenTag>> tagSpans = SanScriptParser.GetTagSpans(this.m_buffer);
            if (tagSpans != null)
            {
                foreach (ITagSpan<TokenTag> tagSpan in tagSpans)
                {
                    yield return tagSpan;
                }
            }
            else
            {
                yield return null;
            }
        }
    }
}
