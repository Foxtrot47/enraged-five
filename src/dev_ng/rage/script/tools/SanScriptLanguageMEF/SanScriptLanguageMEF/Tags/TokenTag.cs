﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.Text.Tagging;

namespace SanScriptLanguageMEF
{
    public class TokenTag : ITag
    {
        //  The type of token
        public TokenTypes m_type { get; private set; }

        /// <summary>
        /// TokenTag class constructor
        /// </summary>
        /// <param name="type">The type of token for this tag</param>
        public TokenTag(TokenTypes type)
        {
            m_type = type;
        }
    }
}
