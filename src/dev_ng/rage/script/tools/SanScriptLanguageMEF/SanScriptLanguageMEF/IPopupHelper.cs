﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SanScriptLanguageMEF
{
    public interface IPopupHelper
    {
        /// <summary>
        /// Method signature.
        /// </summary>
        string Signature { get; }

        /// <summary>
        /// Method name.
        /// </summary>
        string MethodName { get; }

        /// <summary>
        /// Current parameter. Zero based index.
        /// </summary>
        int CurrentParameter { get; }

        /// <summary>
        /// Number of overloaded methods.
        /// </summary>
        int OverloadCount { get; }

        /// <summary>
        /// Parameter count.
        /// </summary>
        int ParameterCount { get; }

        /// <summary>
        /// Caret position.
        /// </summary>
        int Position { get; }

        /// <summary>
        /// Length.
        /// </summary>
        int Length { get; }

        /// <summary>
        /// Update the properties for the current editor line.
        /// </summary>
        void Update();

        /// <summary>
        /// Gets the parameter name for the supplied parameter index.
        /// </summary>
        /// <param name="paramIndex">Zero based index into the parameter list.</param>
        /// <returns>The parameter name for the supplied parameter index.</returns>
        string GetParameterName(int paramIndex);

        /// <summary>
        /// Get the parameter type for the supplied parameter index.
        /// </summary>
        /// <param name="paramIndex">Zero based index into the parameter list.</param>
        /// <returns>The parameter type for the supplied parameter index.</returns>
        string GetParameterType(int paramIndex);
    }
}
