﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Utilities;

namespace SanScriptLanguageMEF
{
    [Export(typeof(ITaggerProvider))]
    [ContentType("SanScript")]
    [TagType(typeof(ErrorTag))]
    internal sealed class ErrorTagProvider : ITaggerProvider
    {
        [Import]
        internal IClassificationTypeRegistryService ClassificationTypeRegistry = null;

        [Import]
        internal IBufferTagAggregatorFactoryService AggregatorFactory = null;

        public ITagger<T> CreateTagger<T>(ITextBuffer buffer) where T : ITag
        {
            ITagAggregator<TokenTag> tagAggregator = AggregatorFactory.CreateTagAggregator<TokenTag>(buffer);
            return new ErrorTagger(buffer, tagAggregator, ClassificationTypeRegistry) as ITagger<T>;
        }
    }
}
