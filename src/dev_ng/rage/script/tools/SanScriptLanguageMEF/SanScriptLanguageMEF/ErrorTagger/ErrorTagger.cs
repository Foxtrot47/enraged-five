﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Text.Classification;

namespace SanScriptLanguageMEF
{
    internal sealed class ErrorTagger : ITagger<ErrorTag>
    {
        //  Buffer to store the text we are tagging
        ITextBuffer m_buffer;
        //  Aggregator to go through text tags
        ITagAggregator<TokenTag> m_aggregator;

        /// <summary>
        /// ErrorTagger class constructor
        /// </summary>
        /// <param name="buffer">The text buffer to get tags from</param>
        /// <param name="aggregator">The aggregator to run through the tags</param>
        /// <param name="typeService">The classification service</param>
        internal ErrorTagger(ITextBuffer buffer, ITagAggregator<TokenTag> aggregator, IClassificationTypeRegistryService typeService)
        {
            //  Store the text buffer and aggregator
            m_buffer = buffer;
            m_aggregator = aggregator;
        }

        /// <summary>
        /// Event handler for when things change
        /// </summary>
        public event EventHandler<SnapshotSpanEventArgs> TagsChanged
        {
            add { }
            remove { }
        }

        /// <summary>
        /// Iterator function to get tags from a span of text
        /// </summary>
        /// <param name="spans">The spans of text to look at</param>
        /// <returns>A new tag span containing the tag span and classification tag</returns>
        public IEnumerable<ITagSpan<ErrorTag>> GetTags(NormalizedSnapshotSpanCollection spans)
        {
            /*
            //  Use the aggregator to iterate through the text spans
            foreach (var tagSpan in m_aggregator.GetTags(spans))
            {
                if (tagSpan.Tag.m_type == TokenTypes.TOKEN_TYPE_ERROR)
                {
                    var tagSpans = tagSpan.Span.GetSpans(spans[0].Snapshot);
                    //  Return the new tag span
                    yield return new TagSpan<ErrorTag>(tagSpans[0], new ErrorTag());
                }
            }
            */
            return null;
        }
    }
}
