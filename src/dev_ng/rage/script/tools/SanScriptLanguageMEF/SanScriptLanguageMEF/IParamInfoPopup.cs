﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.TextManager.Interop;

namespace SanScriptLanguageMEF
{
    /*
     * For more information on the parameter info tooltip window, visit
     * http://msdn.microsoft.com/en-us/library/bb166756(v=vs.100).aspx
     */

    /// <summary>
    /// Parameter information pop-up factory.
    /// </summary>
    public interface IParamInfoPopupFactory
    {
        /// <summary>
        /// Create a new parameter information pop-up.
        /// </summary>
        /// <param name="popupHelper">Pop up helper.</param>
        /// <param name="textView">Text view.</param>
        /// <returns>Parameter information pop-up.</returns>
        IParamInfoPopup Create(IPopupHelper popupHelper, IVsTextView textView);
    }

    /// <summary>
    /// Parameter information pop-up.
    /// </summary>
    public interface IParamInfoPopup
    {
        /// <summary>
        /// The window is currently visible.
        /// </summary>
        bool IsShowing { get; }

        /// <summary>
        /// Refresh the parameter info window.
        /// </summary>
        void Refresh();
    }
}
