﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SanScriptLanguageMEF
{
    /// <summary>
    /// The parameter declaration contains the name and data type of the method's parameter.
    /// </summary>
    internal class ParameterDeclaration
    {
        /// <summary>
        /// Parameter name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Parameter data type.
        /// </summary>
        public string DataType { get; set; }

        /// <summary>
        /// The data type post fix. e.g. & for by reference, [] for array etc. 
        /// </summary>
        public string DataTypePostFix { get; set; }
    }

    /// <summary>
    /// The method cache holds parameter details for each method and function defined.
    /// </summary>
    internal class MethodCache
    {
        #region Private helper class

        class Methods : Dictionary<string, List<ParameterDeclaration>> { }

        #endregion

        #region Private member fields

        Dictionary<string, Methods> m_cache;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public MethodCache()
        {
            m_cache = new Dictionary<string, Methods>();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Add a method to the internal cache.
        /// </summary>
        /// <param name="fileName">Full path to the file that contains the method.</param>
        /// <param name="methodName">Method name.</param>
        /// <param name="methodParams">Parameters.</param>
        public void AddMethod(string fileName, string methodName, List<ParameterDeclaration> methodParams)
        {
            AddFile(fileName);
            string upper = fileName.ToUpper();

            List<ParameterDeclaration> methodParameters = null;
            if(!m_cache[upper].TryGetValue(methodName.ToUpper(), out methodParameters))
            {
                methodParameters = new List<ParameterDeclaration>();
            }

            methodParameters.Clear();
            methodParameters.AddRange(methodParams);
            m_cache[upper][methodName.ToUpper()] = methodParameters;
        }

        /// <summary>
        /// Get the parameter declarations for the given method. Searches through all the files within the cache.
        /// </summary>
        /// <param name="methodName">Method name.</param>
        /// <returns>Enumerated list of parameter declarations.</returns>
        public IEnumerable<ParameterDeclaration> GetMethodParams(string methodName)
        {
            foreach (var filename in m_cache.Keys)
            {
                if (m_cache[filename].ContainsKey(methodName.ToUpper()))
                {
                    var methodParams = GetMethodParams(filename, methodName);
                    foreach (var paramItem in methodParams)
                    {
                        yield return paramItem;
                    }

                    break;
                }
            }

            yield break;
        }

        /// <summary>
        /// Get the parameter declaration for the given method in the supplied file name.
        /// </summary>
        /// <param name="fileName">Full path to the file containing the methodName.</param>
        /// <param name="methodName">The name of the method to get the list of parameters.</param>
        /// <returns>Enumerated list of parameter declarations.</returns>
        public IEnumerable<ParameterDeclaration> GetMethodParams(string fileName, string methodName)
        {
            string lowerFilename = fileName.ToUpper();
            string upperMethodName = methodName.ToUpper();

            Methods methods = null;
            if (m_cache.TryGetValue(fileName, out methods))
            {
                List<ParameterDeclaration> paramList = null;
                if (methods.TryGetValue(upperMethodName, out paramList))
                {
                    foreach (var paramItem in paramList)
                    {
                        yield return paramItem;
                    }
                }
            }

            yield break;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Adds a source code file to the internal list, if it doesn't already exist.
        /// </summary>
        /// <param name="filename">Full path to the file.</param>
        void AddFile(string filename)
        {
            string upper = filename.ToUpper();
            if (!m_cache.ContainsKey(upper))
            {
                m_cache.Add(upper, new Methods());
            }
        }

        #endregion
    }
}
