﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.Windows.Media;

using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace SanScriptLanguageMEF
{
    /// <summary>
    /// Editor format for the default/basic token type
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "SanScript.Default")]
    [Name("SanScript.Default")]
    //  Make visible to the user through the options for Fonts and Colours
    [UserVisible(true)]
    //  Set priority to after default classifiers
    [Order(Before = Priority.Default)]
    internal sealed class TokenTypeFormatDefault : ClassificationFormatDefinition
    {
        public TokenTypeFormatDefault()
        {
            DisplayName = "SanScript Default";
            ForegroundColor = Colors.Black;
        }
    }

    /// <summary>
    /// Editor format for the comment token type
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "SanScript.Comment")]
    [Name("SanScript.Comment")]
    //  Make visible to the user through the options for Fonts and Colours
    [UserVisible(true)]
    //  Set priority to after default classifiers
    [Order(Before = Priority.Default)]
    internal sealed class TokenTypeFormatComment : ClassificationFormatDefinition
    {
        public TokenTypeFormatComment()
        {
            DisplayName = "SanScript Comment";
            ForegroundColor = Colors.Green;
        }
    }

    /// <summary>
    /// Editor format for the keyword token type
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "SanScript.Keyword")]
    [Name("SanScript.Keyword")]
    //  Make visible to the user through the options for Fonts and Colours
    [UserVisible(true)]
    //  Set priority to after default classifiers
    [Order(Before = Priority.Default)]
    internal sealed class TokenTypeFormatKeyword : ClassificationFormatDefinition
    {
        public TokenTypeFormatKeyword()
        {
            DisplayName = "SanScript Keyword";
            ForegroundColor = Colors.Blue;
        }
    }

    /// <summary>
    /// Editor format for the method/function name token type
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "SanScript.Method")]
    [Name("SanScript.Method")]
    //  Make visible to the user through the options for Fonts and Colours
    [UserVisible(true)]
    //  Set priority to after default classifiers
    [Order(Before = Priority.Default)]
    internal sealed class TokenTypeFormatMethod : ClassificationFormatDefinition
    {
        public TokenTypeFormatMethod()
        {
            DisplayName = "SanScript Method";
            ForegroundColor = Colors.Teal;
        }
    }

    /// <summary>
    /// Editor format for the string token type
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "SanScript.String")]
    [Name("SanScript.String")]
    //  Make visible to the user through the options for Fonts and Colours
    [UserVisible(true)]
    //  Set priority to after default classifiers
    [Order(Before = Priority.Default)]
    internal sealed class TokenTypeFormatString : ClassificationFormatDefinition
    {
        public TokenTypeFormatString()
        {
            DisplayName = "SanScript String";
            ForegroundColor = Colors.Gray;
        }
    }

    /// <summary>
    /// Editor format for the vector token type
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "SanScript.VectorValue")]
    [Name("SanScript.VectorValue")]
    //  Make visible to the user through the options for Fonts and Colours
    [UserVisible(true)]
    //  Set priority to after default classifiers
    [Order(Before = Priority.Default)]
    internal sealed class TokenTypeFormatVectorValue : ClassificationFormatDefinition
    {
        public TokenTypeFormatVectorValue()
        {
            DisplayName = "SanScript Vector";
            ForegroundColor = Colors.Magenta;
        }
    }

    /// <summary>
    /// Editor format for the identifier token type
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "SanScript.Identifier")]
    [Name("SanScript.Identifier")]
    //  Make visible to the user through the options for Fonts and Colours
    [UserVisible(true)]
    //  Set priority to after default classifiers
    [Order(Before = Priority.Default)]
    internal sealed class TokenTypeFormatIdentifier : ClassificationFormatDefinition
    {
        public TokenTypeFormatIdentifier()
        {
            DisplayName = "SanScript Identifier";
            ForegroundColor = Colors.DarkCyan;
        }
    }

    /// <summary>
    /// Editor format for the native type token type
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "SanScript.NativeType")]
    [Name("SanScript.NativeType")]
    //  Make visible to the user through the options for Fonts and Colours
    [UserVisible(true)]
    //  Set priority to after default classifiers
    [Order(Before = Priority.Default)]
    internal sealed class TokenTypeFormatNativeType : ClassificationFormatDefinition
    {
        public TokenTypeFormatNativeType()
        {
            DisplayName = "SanScript Native Type";
            ForegroundColor = Colors.Blue;
        }
    }

    /// <summary>
    /// Editor format for the native type token type
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "SanScript.Operator")]
    [Name("SanScript.Operator")]
    //  Make visible to the user through the options for Fonts and Colours
    [UserVisible(true)]
    //  Set priority to after default classifiers
    [Order(Before = Priority.Default)]
    internal sealed class TokenTypeFormatOperator : ClassificationFormatDefinition
    {
        public TokenTypeFormatOperator()
        {
            DisplayName = "SanScript Operator";
            ForegroundColor = Colors.Black;
        }
    }

    /// <summary>
    /// Editor format for the native type token type
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "SanScript.NativeMethod")]
    [Name("SanScript.NativeMethod")]
    //  Make visible to the user through the options for Fonts and Colours
    [UserVisible(true)]
    //  Set priority to after default classifiers
    [Order(Before = Priority.Default)]
    internal sealed class TokenTypeFormatNativeMethod : ClassificationFormatDefinition
    {
        public TokenTypeFormatNativeMethod()
        {
            DisplayName = "SanScript Native Method";
            ForegroundColor = Colors.Brown;
        }
    }

    /// <summary>
    /// Editor format for the native type token type
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "SanScript.Number")]
    [Name("SanScript.Number")]
    //  Make visible to the user through the options for Fonts and Colours
    [UserVisible(true)]
    //  Set priority to after default classifiers
    [Order(Before = Priority.Default)]
    internal sealed class TokenTypeFormatNumber : ClassificationFormatDefinition
    {
        public TokenTypeFormatNumber()
        {
            DisplayName = "SanScript Number";
            ForegroundColor = Colors.Chocolate;
        }
    }

    /// <summary>
    /// Editor format for the pre-processor directive type
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "SanScript.PreProcessor")]
    [Name("SanScript.PreProcessor")]
    //  Make visible to the user through the options for Fonts and Colours
    [UserVisible(true)]
    //  Set priority to after default classifiers
    [Order(Before = Priority.Default)]
    internal sealed class TokenTypeFormatPreProcessor : ClassificationFormatDefinition
    {
        public TokenTypeFormatPreProcessor()
        {
            DisplayName = "SanScript PreProcessor";
            ForegroundColor = Colors.Blue;
        }
    }
}
