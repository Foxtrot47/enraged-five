﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Text.Classification;

namespace SanScriptLanguageMEF
{
    internal sealed class Classifier : ISanScriptClassifier
    {
        //  Buffer to store the text we are tagging
        ITextBuffer m_buffer;
        //  Aggregator to go through text tags
        ITagAggregator<TokenTag> m_aggregator;
        //  Dictionary mapping token type enum to the classification type in TokenTypeClassification.cs
        IDictionary<TokenTypes, IClassificationType> m_types;

        List<ITagSpan<TokenTag>> m_tags;       

        /// <summary>
        /// Gets the text buffer that was used to create this classifier with.
        /// </summary>
        public ITextBuffer Buffer
        {
            get { return this.m_buffer; }
        }

        public List<ITagSpan<TokenTag>> Tags { get { return m_tags; } }


        /// <summary>
        /// Classifier class constructor
        /// </summary>
        /// <param name="buffer">The text buffer to get tags from</param>
        /// <param name="aggregator">The aggregator to run through the tags</param>
        /// <param name="typeService">The classification service</param>
        internal Classifier(ITextBuffer buffer, ITagAggregator<TokenTag> aggregator, IClassificationTypeRegistryService typeService)
        {
            m_tags = new List<ITagSpan<TokenTag>>();
            //  Store the text buffer and aggregator
            m_buffer = buffer;
            m_aggregator = aggregator;
            //  Create the dictionary and add types to it
            m_types = new Dictionary<TokenTypes, IClassificationType>();
            m_types[TokenTypes.TOKEN_TYPE_DEFAULT] = typeService.GetClassificationType("SanScript.Default");
            m_types[TokenTypes.TOKEN_TYPE_COMMENT] = typeService.GetClassificationType("SanScript.Comment");
            m_types[TokenTypes.TOKEN_TYPE_KEYWORD] = typeService.GetClassificationType("SanScript.Keyword");
            m_types[TokenTypes.TOKEN_TYPE_METHOD] = typeService.GetClassificationType("SanScript.Method");
            m_types[TokenTypes.TOKEN_TYPE_STRING] = typeService.GetClassificationType("SanScript.String");
            m_types[TokenTypes.TOKEN_TYPE_VECTOR] = typeService.GetClassificationType("SanScript.VectorValue");
            m_types[TokenTypes.TOKEN_TYPE_IDENTIFIER] = typeService.GetClassificationType("SanScript.Identifier");
            m_types[TokenTypes.TOKEN_TYPE_NATIVETYPE] = typeService.GetClassificationType("SanScript.NativeType");
            m_types[TokenTypes.TOKEN_TYPE_OPERATOR] = typeService.GetClassificationType("SanScript.Operator");
            m_types[TokenTypes.TOKEN_TYPE_NATIVEMETHOD] = typeService.GetClassificationType("SanScript.NativeMethod");
            m_types[TokenTypes.TOKEN_TYPE_NUMBER] = typeService.GetClassificationType("SanScript.Number");
            m_types[TokenTypes.TOKEN_TYPE_ERROR] = typeService.GetClassificationType("SanScript.Default");
            m_types[TokenTypes.TOKEN_TYPE_PREPROCESSOR] = typeService.GetClassificationType("SanScript.PreProcessor");

            //  Store this classifier so we can use it in the parser to fire the TagsChanged event
            if (!buffer.Properties.ContainsProperty(typeof(ISanScriptClassifier)))
            {
                SanScriptParser.SetClassifier(this);
                buffer.Properties.AddProperty(typeof(ISanScriptClassifier), this);
            }
        }

        /// <summary>
        /// Event handler for when things change
        /// </summary>
        public event EventHandler<SnapshotSpanEventArgs> TagsChanged;

        /// <summary>
        /// Fire the tags changed event, so it grabs new tags
        /// </summary>
        /// <param name="span">The SnapshotSpan to use for new tags</param>
        public void FireTagsChanged(SnapshotSpan span)
        {
            if (TagsChanged != null)
            {
                TagsChanged(this, new SnapshotSpanEventArgs(span));
            }
        }

        /// <summary>
        /// Iterator function to get tags from a span of text
        /// </summary>
        /// <param name="spans">The spans of text to look at</param>
        /// <returns>A new tag span containing the tag span and classification tag</returns>
        public IEnumerable<ITagSpan<ClassificationTag>> GetTags(NormalizedSnapshotSpanCollection spans)
        {
            //  Use the aggregator to iterate through the text spans
            foreach (SnapshotSpan snapshot in spans)
            {
                foreach (IMappingTagSpan<TokenTag> tagSpan in m_aggregator.GetTags(snapshot))
                {
                    TokenTag tag = tagSpan.Tag;
                    TokenTypes tagType = tag.m_type;
                    ClassificationTag classification = new ClassificationTag(m_types[tagType]);

                    foreach (SnapshotSpan span in tagSpan.Span.GetSpans(spans[0].Snapshot))
                    {
                        //  Return the new tag span
                        yield return new TagSpan<ClassificationTag>(span, classification);
                    }
                }
            }
        }
    }
}
