﻿using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Tagging;
using System.Collections.Generic;
using System;

namespace SanScriptLanguageMEF
{
    /// <summary>
    /// When implement represents a ITagger that can be used with Classification tags in the
    /// <see cref="SanScriptLanguageMEF.SanScriptParser"/> class.
    /// </summary>
    public interface ISanScriptClassifier : ITagger<ClassificationTag>
    {
        event EventHandler<SnapshotSpanEventArgs> TagsChanged;

        #region Properties
        /// <summary>
        /// Gets the text buffer that was used to create this classifier with.
        /// </summary>
        ITextBuffer Buffer { get; }

        List<ITagSpan<TokenTag>> Tags { get; }

        #endregion
        
        #region Methods
        /// <summary>
        /// Fire the tags changed event, so it grabs new tags
        /// </summary>
        /// <param name="span">The SnapshotSpan to use for new tags</param>
        void FireTagsChanged(SnapshotSpan span);
        #endregion
    }
}
