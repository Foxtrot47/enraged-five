﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace SanScriptLanguageMEF
{
    internal static class TokenTypeClassification
    {
        /// <summary>
        /// The default/basic token type
        /// </summary>
        [Export]
        [Name("SanScript.Default")]
        internal static ClassificationTypeDefinition Default = null;

        /// <summary>
        /// The comment token type
        /// </summary>
        [Export]
        [Name("SanScript.Comment")]
        internal static ClassificationTypeDefinition Comment = null;

        /// <summary>
        /// The keyword token type, e.g. IF, ENUM, etc.
        /// </summary>
        [Export]
        [Name("SanScript.Keyword")]
        internal static ClassificationTypeDefinition Keyword = null;

        /// <summary>
        /// The method/function name token type
        /// </summary>
        [Export]
        [Name("SanScript.Method")]
        internal static ClassificationTypeDefinition Method = null;

        /// <summary>
        /// The string token type
        /// </summary>
        [Export]
        [Name("SanScript.String")]
        internal static ClassificationTypeDefinition String = null;

        /// <summary>
        /// The vector token type
        /// </summary>
        [Export]
        [Name("SanScript.VectorValue")]
        internal static ClassificationTypeDefinition VectorValue = null;

        /// <summary>
        /// The identifier token type
        /// </summary>
        [Export]
        [Name("SanScript.Identifier")]
        internal static ClassificationTypeDefinition Identifier = null;

        /// <summary>
        /// The native type token type
        /// </summary>
        [Export]
        [Name("SanScript.NativeType")]
        internal static ClassificationTypeDefinition NativeType = null;

        /// <summary>
        /// The operator token type
        /// </summary>
        [Export]
        [Name("SanScript.Operator")]
        internal static ClassificationTypeDefinition Operator = null;

        /// <summary>
        /// The native method token type
        /// </summary>
        [Export]
        [Name("SanScript.NativeMethod")]
        internal static ClassificationTypeDefinition NativeMethod = null;

        /// <summary>
        /// The native method token type
        /// </summary>
        [Export]
        [Name("SanScript.Number")]
        internal static ClassificationTypeDefinition Number = null;

        /// <summary>
        /// Pre-processor directive.
        /// </summary>
        [Export]
        [Name("SanScript.PreProcessor")]
        internal static ClassificationTypeDefinition PreProcessor = null;
    }
}
