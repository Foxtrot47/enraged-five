﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace SanScriptLanguageMEF.Popups
{
    class StructViewModel : StructuredViewModel
    {
        static readonly string s_imageSrc = "pack://application:,,,/SanScriptLanguageMEF;component/Resources/field.png";

        public StructViewModel(Identifier identifier)
            : base(identifier)
        {

        }

        protected override void Fill(Identifier identifier)
        {
            this.StructName = identifier.GetName();
            this.StartElement = "STRUCT";
            this.EndElement = "ENDSTRUCT";

            
            foreach (var structMember in SanScriptParser.GetStructMembersRaw(identifier.GetName()))
            {
                string[] split = structMember.Split("!".ToCharArray());
                string dataType = split[1];
                string name = split[0];

                if (split.Length > 2)
                {
                    name = name + "[";
                    name = name + split[2];
                    name = name + "]";
                }

                MemberViewModel mvm = new MemberViewModel(name, dataType, s_imageSrc, Visibility.Visible);
                Members.Add(mvm);
            }
        }
    }
}
