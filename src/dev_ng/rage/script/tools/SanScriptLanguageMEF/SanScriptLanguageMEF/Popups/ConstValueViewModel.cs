﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SanScriptLanguageMEF.Popups
{
    class ConstValueViewModel
    {
        public string Name
        {
            get;
            set;
        }

        public string Value
        {
            get;
            set;
        }

        public ConstValueViewModel(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }
}
