﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace SanScriptLanguageMEF.Popups
{
    class EnumViewModel : StructuredViewModel
    {
        static readonly string s_imageSrc = "pack://application:,,,/SanScriptLanguageMEF;component/Resources/enumvalue.png";

        public EnumViewModel(Identifier identifier)
            : base(identifier)
        {

        }

        protected override void Fill(Identifier identifier)
        {
            this.StructName = identifier.GetName();
            this.StartElement = "ENUM";
            this.EndElement = "ENDENUM";


            foreach (var enumVal in SanScriptParser.FindEnumMembers(identifier.GetName()))
            {

                MemberViewModel mvm = new MemberViewModel(enumVal, "", s_imageSrc, Visibility.Collapsed);
                Members.Add(mvm);
            }
        }
    }

}
