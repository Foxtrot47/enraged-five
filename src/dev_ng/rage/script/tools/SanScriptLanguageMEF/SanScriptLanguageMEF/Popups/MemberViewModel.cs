﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace SanScriptLanguageMEF.Popups
{
    class MemberViewModel
    {
        public string Icon
        {
            get;
            set;
        }

        public string DataType
        {
            get;
            set;
        }

        public Visibility DataTypeVisibility
        {
            get;
            set;
        }

        public string MemberName
        {
            get;
            set;
        }

        public MemberViewModel(string memberName, string dataType, string iconSource, Visibility dataTypeVisibility)
        {
            DataType = dataType;
            MemberName = memberName;
            Icon = iconSource;
            DataTypeVisibility = dataTypeVisibility;
        }
    }
}
