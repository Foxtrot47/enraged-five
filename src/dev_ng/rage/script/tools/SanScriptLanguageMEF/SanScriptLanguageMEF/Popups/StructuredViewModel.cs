﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace SanScriptLanguageMEF.Popups
{
    abstract class StructuredViewModel
    {
        public string StartElement
        {
            get;
            set;
        }

        public string StructName
        {
            get;
            set;
        }

        public string EndElement
        {
            get;
            set;
        }

        public ObservableCollection<MemberViewModel> Members
        {
            get;
            set;
        }

        public StructuredViewModel(Identifier identifier)
        {
            Members = new ObservableCollection<MemberViewModel>();
            Fill(identifier);
        }

        protected abstract void Fill(Identifier identifier);
    }
}
