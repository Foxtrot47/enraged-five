﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading;

using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Tagging;
using EnvDTE;

using Irony.Parsing;
using Microsoft.VisualStudio.Text.Editor;

namespace SanScriptLanguageMEF
{
    static public class SanScriptParser
    {
        //  Keywords
        static private List<string> m_keywordTags = null;
        //  Identifier tags
        static private Dictionary<string, List<Identifier>> m_identifierTags = null;
        static private Dictionary<string, List<Identifier>> m_includedIdentifierTags = null;
        //  Outlining pairs
        static private List<OutliningPair> m_outliningPairs = null;
        //  Brace matching pairs
        static private Dictionary<string, string> m_matchingPairs = null;
        //  Word separator characters
        static private char[] m_wordSeparators = { ' ', '(', ')', ',' };
        //  List of parsed include files
        static private Dictionary<string, ParsedIncludeFile> m_parsedIncludeFiles = null;
        //  Array of include paths
        static private string[] m_storedIncludePaths = null;

        //  Irony grammar file stuff
        static private SanScriptGrammar m_grammar = new SanScriptGrammar();
        static private LanguageData m_language;
        static private Parser m_ironyParser;

        static private List<string> m_stringStack = new List<string>();
        static private List<string> m_filenameStack = new List<string>();

        //  List of parsed tag spans
        static private Dictionary<ITextBuffer, List<ITagSpan<TokenTag>>> m_tagSpans;
        static private object lockObj = new object();

        static private Timer m_parseTimer = null;
        static private ISanScriptClassifier m_currentClassifier = null;
        static private List<WeakReference> m_classifiers = null;

        static private string m_currentlyOpeningFile = string.Empty;

        static private Dictionary<string, List<string>> m_structMembers;
        static private Dictionary<string, Dictionary<string, string>> m_variables;
        static private Dictionary<string, Dictionary<string, List<string>>> m_enumerations;

        //static private bool m_isParsing = false;

        static private List<ITextBuffer> m_currentlyParsing;
        
        /// <summary>
        /// Maps the text buffer to its regions.
        /// </summary>
        internal static Dictionary<ITextBuffer, List<Region>> RegionMap { get; private set; }

        /// <summary>
        /// Maps the text buffer to its snapshot.
        /// </summary>
        internal static Dictionary<ITextBuffer, ITextSnapshot> SnapshotMap { get; private set; }

        internal static MethodCache MethodCache { get; private set; }

        /// <summary>
        /// SanScriptParser class constructor
        /// </summary>
        static SanScriptParser()
        {
            //  Initialise the language and Irony parser
            m_language = new LanguageData(m_grammar);
            m_ironyParser = new Parser(m_language);

            m_variables = new Dictionary<string, Dictionary<string, string>>();
            m_enumerations = new Dictionary<string, Dictionary<string, List<string>>>();
            m_structMembers = new Dictionary<string, List<string>>();
            RegionMap = new Dictionary<ITextBuffer, List<Region>>();
            SnapshotMap = new Dictionary<ITextBuffer, ITextSnapshot>();
            MethodCache = new MethodCache();

            m_keywordTags = new List<string>();
            m_identifierTags = new Dictionary<string, List<Identifier>>();
            m_includedIdentifierTags = new Dictionary<string, List<Identifier>>();
            m_parsedIncludeFiles = new Dictionary<string, ParsedIncludeFile>();
            m_classifiers = new List<WeakReference>();
            m_tagSpans = new Dictionary<ITextBuffer, List<ITagSpan<TokenTag>>>();
            m_currentlyParsing = new List<ITextBuffer>();
            foreach (KeyValuePair<string, KeyTerm> pair in m_grammar.KeyTerms)
            {
                m_keywordTags.Add(pair.Value.ToString());
            }

            //  These are pairs for outlining
            m_outliningPairs = new List<OutliningPair>();
            m_outliningPairs.Add(new OutliningPair("/*", "*/"));
            m_outliningPairs.Add(new OutliningPair("#IF", "#ENDIF"));
            m_outliningPairs.Add(new OutliningPair("#IFDEF", "#ENDIF"));
            m_outliningPairs.Add(new OutliningPair("IF", "ENDIF"));
            m_outliningPairs.Add(new OutliningPair("ENUM", "ENDENUM"));
            m_outliningPairs.Add(new OutliningPair("STRICT_ENUM", "ENDENUM"));
            m_outliningPairs.Add(new OutliningPair("HASH_ENUM", "ENDENUM"));
            m_outliningPairs.Add(new OutliningPair("PROC", "ENDPROC"));
            m_outliningPairs.Add(new OutliningPair("STRUCT", "ENDSTRUCT"));
            m_outliningPairs.Add(new OutliningPair("SCRIPT", "ENDSCRIPT"));
            m_outliningPairs.Add(new OutliningPair("FUNC", "ENDFUNC"));
            m_outliningPairs.Add(new OutliningPair("REPEAT", "ENDREPEAT"));
            m_outliningPairs.Add(new OutliningPair("SWITCH", "ENDSWITCH"));
            m_outliningPairs.Add(new OutliningPair("CASE", "BREAK"));
            m_outliningPairs.Add(new OutliningPair("WHILE", "ENDWHILE"));
            m_outliningPairs.Add(new OutliningPair("GLOBALS", "ENDGLOBALS"));
			//  These are pairs for brace matching (also includes outlining pairs)
            m_matchingPairs = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            foreach (OutliningPair pair in m_outliningPairs)
            {
                m_matchingPairs.Add(pair.GetStart(), pair.GetEnd());
            }
            m_matchingPairs.Add("(", ")");
            m_matchingPairs.Add("<<", ">>");
            m_matchingPairs.Add("[", "]");
        }

        static public void ClearTagSpans()
        {
            if (m_tagSpans != null)
            {
                m_tagSpans.Clear();
            }
        }

        static public List<ITagSpan<TokenTag>> GetTagSpans(ITextBuffer buffer)
        {
            List<ITagSpan<TokenTag>> collection = null;
            m_tagSpans.TryGetValue(buffer, out collection);
            return collection;
        }

        static public void ReParseAsynch(object snapshot)
        {
            //  Non-threaded version
            //ReParse(snapshot);
            //  Threaded version
            ThreadPool.QueueUserWorkItem(SanScriptParser.ReParse, snapshot);
        }

        static public void SetClassifier(ISanScriptClassifier classifier)
        {
            m_currentClassifier = classifier;
            m_classifiers.Add(new WeakReference(classifier));
            SanScriptParser.RequestParse(classifier.Buffer.CurrentSnapshot, 1);
        }

        /// <summary>
        /// Get the variable's data type from its name.
        /// </summary>
        /// <param name="variableName">Variable name.</param>
        /// <returns>Data type.</returns>
        static public string GetVariablesType(string variableName)
        {
            foreach (var key in m_variables.Keys)
            {
                if (m_variables[key].ContainsKey(variableName))
                {
                    return m_variables[key][variableName];
                }
            }

            return String.Empty;
        }

        /// <summary>
        /// Get the structure's members.
        /// </summary>
        /// <param name="structName">Structure name.</param>
        /// <returns>Enumeration of the structure member fields.</returns>
        static public IEnumerable<string> GetStructMembers(string structName)
        {
            if (!m_structMembers.ContainsKey(structName.ToUpper()))
            {
                yield break;
            }

            foreach (string member in m_structMembers[structName.ToUpper()])
            {
                string[] split = member.Split("!".ToCharArray());
                yield return split[0];
            }
        }

        /// <summary>
        /// Get the structure's members.
        /// </summary>
        /// <param name="structName">Structure name.</param>
        /// <returns>Enumeration of the structure member fields.</returns>
        static public IEnumerable<string> GetStructMembersRaw(string structName)
        {
            if (!m_structMembers.ContainsKey(structName.ToUpper()))
            {
                yield break;
            }

            foreach (string member in m_structMembers[structName.ToUpper()])
            {
                yield return member;
            }
        }

        /// <summary>
        /// Get the member fields for a structure data type from a particular file.
        /// </summary>
        /// <param name="fileName">File name.</param>
        /// <param name="variableName">Variable name.</param>
        /// <returns>An enumerator containing the member names.</returns>
        static public IEnumerable<string> GetMemberFields(string variableName)
        {
            foreach(var fileName in m_variables.Keys)
            {
                if (m_variables[fileName].ContainsKey(variableName))
                {
                    string structType = m_variables[fileName][variableName];

                    if (!m_structMembers.ContainsKey(structType.ToUpper()))
                    {
                        yield break;
                    }

                    foreach (string member in m_structMembers[structType.ToUpper()])
                    {
                        yield return member;
                    }

                    yield break;
                }
            }

            yield break;
        }

        static public void RequestParse(ITextSnapshot snapshot, int delay)
        {
            //  Shut down the current timer
            if (m_parseTimer != null)
            {
                m_parseTimer.Dispose();
            }

            //  Put the call to the parser on a timer
            m_parseTimer = new Timer(new TimerCallback(ReParseAsynch), snapshot, delay, Timeout.Infinite);
        }

        /// <summary>
        /// Re-parses the text buffer to get all the tag spans
        /// </summary>
        static public void ReParse(object snapshotObject)
        {
            ITextSnapshot snapshot = (ITextSnapshot)snapshotObject;

            //  Don't reparse if we are already parsing
            if (m_currentlyParsing.Contains(snapshot.TextBuffer))
            {
                return;
            }

            m_currentlyParsing.Add(snapshot.TextBuffer);

            //  Thread safety lock
            lock (lockObj)
            {
                ISanScriptClassifier effectedClassifier = null;
                foreach (WeakReference classifier in m_classifiers)
                {
                    if (!classifier.IsAlive)
                    {
                        continue;
                    }

                    ISanScriptClassifier target = classifier.Target as ISanScriptClassifier;
                    if (target == null)
                    {
                        continue;
                    }

                    if (object.ReferenceEquals(target.Buffer, snapshot.TextBuffer))
                    {
                        effectedClassifier = target;
                        break;
                    }
                }

                if (effectedClassifier == null)
                {
                    m_currentlyParsing.Remove(snapshot.TextBuffer);
                    return;
                }

                if (!m_tagSpans.ContainsKey(effectedClassifier.Buffer))
                {
                    m_tagSpans.Add(effectedClassifier.Buffer, new List<ITagSpan<TokenTag>>());
                }

                //  Reset the tag spans
                List<ITagSpan<TokenTag>> spans = new List<ITagSpan<TokenTag>>();

                //  Get the current snapshot span of the entire text buffer
                SnapshotSpan currentSpan = new SnapshotSpan(snapshot, 0, snapshot.Length);
                //  Split the text into tokens
                List<SanScriptToken> tokens = SplitStringIntoTokens(currentSpan.GetText());

                foreach (SanScriptToken token in tokens)
                {
                    //  Create a snapshot span of the possible token
                    var tokenSpan = new SnapshotSpan(currentSpan.Snapshot, new Span(token.GetPosition(), token.GetLength()));
                    //  If the created span intersects with the current span (is inside it either fully or partially)
                    if (tokenSpan.IntersectsWith(currentSpan))
                    {
                        //  Add a new tag span to the list
                        spans.Add(new TagSpan<TokenTag>(tokenSpan, new TokenTag(token.GetTokenType())));
                    }
                }

                //  Store the new tag spans
                m_tagSpans[effectedClassifier.Buffer] = spans;
                //  Call the tags changed event on the current classifier
                if (effectedClassifier != null)
                {
                    effectedClassifier.Tags.Clear();
                    effectedClassifier.Tags.AddRange(spans);
                    effectedClassifier.FireTagsChanged(currentSpan);
                }
            }

            SnapshotMap[snapshot.TextBuffer] = snapshot;
            m_currentlyParsing.Remove(snapshot.TextBuffer);
            ReParseOutlining(snapshot.TextBuffer);
        }

        static bool TryGetLevel(string text, int startIndex, out int level)
        {
            level = -1;
            if (text.Length > startIndex + 3)
            {
                if (int.TryParse(text.Substring(startIndex + 1), out level))
                {
                    return true;
                }
            }

            return false;
        }


        static SnapshotSpan AsSnapshotSpan(Region region, ITextSnapshot snapshot)
        {
            var startLine = snapshot.GetLineFromLineNumber(region.StartLine);
            var endLine = (region.StartLine == region.EndLine) ? startLine : snapshot.GetLineFromLineNumber(region.EndLine);

            return new SnapshotSpan(startLine.Start + region.StartOffset, endLine.End);
        }


        static void ReParseOutlining(ITextBuffer buffer)
        {
            ITextSnapshot newSnapshot = buffer.CurrentSnapshot;
            List<Region> newRegions = new List<Region>();

            //  Keep the current (deepest) partial region, which will have references
            //  to any parent partial regions
            PartialRegion currentRegion = null;

            foreach (var line in newSnapshot.Lines)
            {
                int regionStart = -1;
                string text = line.GetText();
                OutliningPair pair = null;

                //  Lines that contain a m_startHide denote the start of a new region
                if ((pair = SanScriptParser.IsOutliningStart(text, out regionStart)) != null)
                {
                    int currentLevel = (currentRegion != null) ? currentRegion.Level : 1;
                    int newLevel;

                    if (!TryGetLevel(text, regionStart, out newLevel))
                    {
                        newLevel = currentLevel + 1;
                    }

                    //  Levels are the same and we have an existing region, 
                    //  end the current region and start the next
                    if (currentLevel == newLevel && currentRegion != null)
                    {
                        newRegions.Add(new Region()
                        {
                            Level = currentRegion.Level,
                            StartLine = currentRegion.StartLine,
                            StartOffset = currentRegion.StartOffset,
                            EndLine = line.LineNumber,
                            CollapsedString = currentRegion.CollapsedString
                        });

                        currentRegion = new PartialRegion()
                        {
                            Level = newLevel,
                            StartLine = line.LineNumber,
                            StartOffset = regionStart,
                            PartialParent = currentRegion.PartialParent,
                            CollapsedString = text,
                            RegionPair = pair
                        };
                    }
                    //  This is a new (sub)region
                    else
                    {
                        currentRegion = new PartialRegion()
                        {
                            Level = newLevel,
                            StartLine = line.LineNumber,
                            StartOffset = regionStart,
                            PartialParent = currentRegion,
                            CollapsedString = text,
                            RegionPair = pair
                        };
                    }
                }
                //  Lines that contain m_endHide denote the end of a region
                else if (currentRegion != null &&
                    (pair = SanScriptParser.IsOutliningEnd(text, out regionStart)) != null &&
                    pair == currentRegion.RegionPair)
                {
                    int currentLevel = (currentRegion != null) ? currentRegion.Level : 1;
                    int closingLevel;

                    if (!TryGetLevel(text, regionStart, out closingLevel))
                    {
                        closingLevel = currentLevel;
                    }

                    //  The regions match
                    if (currentRegion != null &&
                        currentLevel == closingLevel)
                    {
                        newRegions.Add(new Region()
                        {
                            Level = currentLevel,
                            StartLine = currentRegion.StartLine,
                            StartOffset = currentRegion.StartOffset,
                            EndLine = line.LineNumber,
                            CollapsedString = currentRegion.CollapsedString
                        });

                        currentRegion = currentRegion.PartialParent;
                    }
                }
            }

            List<Span> snapshotSpans = null;

            if (RegionMap.ContainsKey(buffer) && SnapshotMap.ContainsKey(buffer))
            {
                try
                {
                    snapshotSpans = new List<Span>(RegionMap[buffer].Select(r => AsSnapshotSpan(r, SnapshotMap[buffer]).TranslateTo(newSnapshot, SpanTrackingMode.EdgeExclusive).Span));
                }
                catch (ArgumentOutOfRangeException)
                {
                    // Stability:
                    // ArgumentOutOfRangeException can be thrown in the AsSnapshotSpan. Easier to catch and ignore here 
                    // than inside that method. If an out of range exception occurs, snapshotSpans will be null and an 
                    // empty list will be created below.
                }
            }
            
            //  Determine the changed span and send a changed event with the new spans
            List<Span> oldSpans = snapshotSpans != null && RegionMap.ContainsKey(buffer) && SnapshotMap.ContainsKey(buffer) ?
                oldSpans = snapshotSpans
                : new List<Span>();

            List<Span> newSpans = new List<Span>(newRegions.Select(r => AsSnapshotSpan(r, newSnapshot).Span));

            NormalizedSpanCollection oldSpanCollection = new NormalizedSpanCollection(oldSpans);
            NormalizedSpanCollection newSpanCollection = new NormalizedSpanCollection(newSpans);

            //  the changed regions are regions that appear in one set or the other, but not both
            NormalizedSpanCollection removed = NormalizedSpanCollection.Difference(oldSpanCollection, newSpanCollection);

            int changeStart = int.MaxValue;
            int changeEnd = -1;

            if (removed.Count > 0)
            {
                changeStart = removed[0].Start;
                changeEnd = removed[removed.Count - 1].End;
            }

            if (newSpans.Count > 0)
            {
                changeStart = Math.Min(changeStart, newSpans[0].Start);
                changeEnd = Math.Max(changeEnd, newSpans[newSpans.Count - 1].End);
            }

            SnapshotMap[buffer] = newSnapshot;
            RegionMap[buffer] = newRegions;
        }

        static public bool FindBraceMatchingPairFromKey(string key, int offset, out KeyValuePair<string, string> pair, out int newOffset)
        {
            newOffset = 0;

            if (m_matchingPairs.ContainsKey(key))
            {
                string value;
                m_matchingPairs.TryGetValue(key, out value);
                pair = new KeyValuePair<string, string>(key, value);

                return true;
            }
            else
            {
                //  This could still be an opener but it might be joined to other text, 
                //  such as '<<(' where we could be looking for << or (
                for (int i = 0; i < m_matchingPairs.Count; i++)
                {
                    string currentKey = m_matchingPairs.ElementAt(i).Key;
                    string currentValue = m_matchingPairs.ElementAt(i).Value;
                    int index = key.IndexOf(currentKey, StringComparison.OrdinalIgnoreCase);
                    if (index >= 0 && (offset >= index && offset < index + currentKey.Length))
                    {
                        pair = new KeyValuePair<string, string>(currentKey, currentValue);
                        newOffset = index;

                        return true;
                    }
                }
            }

            //  Create an empty pair if not found
            pair = new KeyValuePair<string, string>();

            return false;
        }

        static public bool FindBraceMatchingPairFromValue(string value, int offset, out KeyValuePair<string, string> pair, out int newOffset)
        {
            newOffset = 0;

            if (m_matchingPairs.ContainsValue(value))
            {
                var key = from n in m_matchingPairs
                          where n.Value.Equals(value)
                          select n.Key;

                pair = new KeyValuePair<string, string>((string)key.ElementAt<string>(0), value);

                return true;
            }
            else
            {
                //  This could still be a closer but it might be joined to other text, 
                //  such as '<<(' where we could be looking for << or (
                for (int i = 0; i < m_matchingPairs.Count; i++)
                {
                    string currentKey = m_matchingPairs.ElementAt(i).Key;
                    string currentValue = m_matchingPairs.ElementAt(i).Value;
                    int index = value.IndexOf(currentValue, StringComparison.OrdinalIgnoreCase);
                    if (index >= 0 && (offset >= index && offset < index + currentValue.Length))
                    {
                        pair = new KeyValuePair<string, string>(currentKey, currentValue);
                        newOffset = index;

                        return true;
                    }
                }
            }

            //  Create an empty pair if not found
            pair = new KeyValuePair<string, string>();

            return false;
        }

        /// <summary>
        /// Checks to see if the text passed in contains a valid outlining start string
        /// </summary>
        /// <param name="text">The text to check</param>
        /// <param name="startPos">The starting position of the text if found</param>
        /// <returns>The matching outlining pair</returns>
        static public OutliningPair IsOutliningStart(string text, out int startPos)
        {
            startPos = -1;
            string textNoWhitespace = text.Trim();
            string[] words = textNoWhitespace.Split(m_wordSeparators);

            foreach (OutliningPair p in m_outliningPairs)
            {
                if (words.Length > 0 && String.Compare(words[0], p.GetStart(), false) == 0)
                {
                    startPos = text.IndexOf(p.GetStart());
                    return p;
                }
            }

            return null;
        }

        /// <summary>
        /// Checks to see if the text passed in contains a valid outlining end string
        /// </summary>
        /// <param name="text">The text to check</param>
        /// <param name="startPos">The starting position of the text if found</param>
        /// <returns>The matching outlining pair</returns>
        static public OutliningPair IsOutliningEnd(string text, out int startPos)
        {
            startPos = -1;
            string textNoWhitespace = text.Trim();
            string[] words = textNoWhitespace.Split(m_wordSeparators);

            foreach (OutliningPair p in m_outliningPairs)
            {
                if (words.Length > 0 && String.Compare(words[0], p.GetEnd(), false) == 0)
                {
                    startPos = text.IndexOf(p.GetEnd());
                    return p;
                }
            }

            return null;
        }

        /// <summary>
        /// Find an identifier by name
        /// </summary>
        /// <param name="name">The name to find</param>
        /// <returns>The identifier if found, or null</returns>
        static public Identifier FindIdentifier(string name)
        {
            Identifier identifier = null;
            List<Identifier> list = FindIdentifierList(name);

            if (list != null)
            {
                //  Pick the first one in the list for now, we may eventually want a pop up if we match more than one
                identifier = list[0];
            }

            return identifier;
        }

        /// <summary>
        /// Find the member names of the supplied enum type.
        /// </summary>
        /// <param name="enumType">Enum type.</param>
        /// <returns>The list of members of the enum type.</returns>
        static public List<string> FindEnumMembers(string enumType)
        {
            foreach (var key in m_enumerations.Keys)
            {
                foreach (var dict in m_enumerations[key])
                {
                    if (dict.Key == enumType)
                    {
                        return dict.Value;
                    }
                }
            }

            return new List<string>();
        }
        
        /// <summary>
        /// Find a list of identifiers by name.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <returns>The list of identifiers matching the name.</returns>
        static public List<Identifier> FindIdentifiers(string name)
        {
            List<Identifier> list = FindIdentifierList(name);

            if (list != null)
            {
                return list;
            }

            return new List<Identifier>();
        }

        /// <summary>
        /// Tries to find an identifier in the lists by name
        /// </summary>
        /// <param name="name">The name to find</param>
        /// <returns>The identifier list if found, or null</returns>
        static private List<Identifier> FindIdentifierList(string name)
        {
            List<Identifier> identifierList = null;

            if (m_includedIdentifierTags.ContainsKey(name.ToUpper()))
            {
                identifierList = m_includedIdentifierTags[name.ToUpper()];
            }
            else if (m_identifierTags.ContainsKey(name.ToUpper()))
            {
                identifierList = m_identifierTags[name.ToUpper()];
            }

            return identifierList;
        }

        /// <summary>
        /// Adds all the tokens contained in the parse tree
        /// </summary>
        /// <param name="tree">The parse tree holding the tokens</param>
        /// <param name="tokenList">The token list to add to</param>
        static private void AddTokensFromParseTree(ParseTree tree, List<SanScriptToken> tokenList)
        {
            if (tree.Tokens != null)
            {
                Token lastToken = null;

                foreach (Token ironyToken in tree.Tokens)
                {
                    if (ironyToken != null)
                    {
                        int position = ironyToken.Location.Position;
                        int length = ironyToken.Length;
                        TokenTypes type = TokenTypes.TOKEN_TYPE_DEFAULT;

                        if (ironyToken.EditorInfo != null)
                        {
                            switch (ironyToken.EditorInfo.Color)
                            {
                                case TokenColor.Identifier:
                                {
                                    //  Find the list of identifiers matching this name
                                    List<Identifier> identifierList = FindIdentifierList(ironyToken.Text);

                                    if (identifierList != null)
                                    {
                                        //  Just grab the first one for now
                                        //  TODO: We will eventually need a more accurate match in case a variable and 
                                        //  function have the same name (shouldn't happen, but *could*)
                                        Identifier identifier = identifierList[0];

                                        switch (identifier.GetIdentifierType())
                                        {
                                            case Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_VARIABLE:
                                            case Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_CONST_VARIABLE:
                                            case Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_ENUM_MEMBER:
                                            {
                                                type = TokenTypes.TOKEN_TYPE_IDENTIFIER;
                                            }
                                            break;
                                            case Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_FUNCTION:
                                            {
                                                type = TokenTypes.TOKEN_TYPE_METHOD;
                                            }
                                            break;
                                            case Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_NATIVEFUNCTION:
                                            {
                                                type = TokenTypes.TOKEN_TYPE_NATIVEMETHOD;
                                            }
                                            break;
                                            case Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_ENUM_TYPE:
                                            case Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_STRUCT:
                                            {
                                                type = TokenTypes.TOKEN_TYPE_KEYWORD;
                                            }
                                            break;
                                            case Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_USER_TYPE:
                                            {
                                                type = TokenTypes.TOKEN_TYPE_NATIVETYPE;
                                            }
                                            break;
                                        }
                                    }
                                    else // This function was defined in this file
                                    {
                                        if (lastToken != null && (lastToken.ValueString == "PROC" || lastToken.ValueString == "FUNC"))
                                        {
                                            type = TokenTypes.TOKEN_TYPE_METHOD;
                                        }
                                    }
                                }
                                break;
                                case TokenColor.Keyword:
                                {
                                    if (ironyToken.Text.StartsWith("#"))
                                    {
                                        type = TokenTypes.TOKEN_TYPE_PREPROCESSOR;
                                    }
                                    else
                                    {
                                        type = TokenTypes.TOKEN_TYPE_KEYWORD;
                                    }
                                }
                                break;
                                case TokenColor.String:
                                {
                                    type = TokenTypes.TOKEN_TYPE_STRING;
                                }
                                break;
                                case TokenColor.Comment:
                                {
                                    type = TokenTypes.TOKEN_TYPE_COMMENT;
                                }
                                break;
                                case TokenColor.Number:
                                {
                                    type = TokenTypes.TOKEN_TYPE_NUMBER;
                                }
                                break;
                            }
                            lastToken = ironyToken;
                        }
                        else
                        {
                            //  Check for error
                            if (ironyToken.Category == TokenCategory.Error)
                            {
                                type = TokenTypes.TOKEN_TYPE_ERROR;
                                //  Set length to end of file
                                length = tree.SourceText.Length - position;
                                lastToken = null;
                            }
                        }

                        //  Add a token to the token list (don't bother with default for now)
                        if (type != TokenTypes.TOKEN_TYPE_DEFAULT)
                        {
                            tokenList.Add(new SanScriptToken(position, length, type));
                        }
                    }
                }
            }
        }

        #region Add Identifiers
        /// <summary>
        /// Adds a variable to the completion list from a parse tree node
        /// </summary>
        /// <param name="node">The parse tree node to look at</param>
        /// <param name="type">The type of variable to add</param>
        /// <param name="identifierTags">The dictionary of identifiers to add to</param>
        /// <param name="lastVariableType">Last variable type.</param>
        static private void AddVariableIdentifier(ParseTreeNode node, Identifier.eIDENTIFIER_TYPE type, Dictionary<string, List<Identifier>> identifierTags, string lastVariableType)
        {
            if (node.ChildNodes.Count > 0)
            {
                //  Identifier should be the first child node
                Token ironyToken = node.ChildNodes[0].Token;

                if (ironyToken != null)
                {
                    if (ironyToken.EditorInfo != null && ironyToken.EditorInfo.Color == TokenColor.Identifier)
                    {
                        if (!String.IsNullOrEmpty(lastVariableType))
                        {
                            if (!m_variables.ContainsKey(m_currentlyOpeningFile))
                            {
                                m_variables[m_currentlyOpeningFile] = new Dictionary<string, string>();
                            }

                            m_variables[m_currentlyOpeningFile][ironyToken.Text] = lastVariableType;
                        }

                        List<Identifier> identifierList = null;
                        Identifier identifier = null;
                        if (type == Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_CONST_VARIABLE)
                        {
                            var literal = FindFirstTerm(node, "literal");
                            if (literal != null)
                            {
                                string signature = String.Empty;

                                switch (literal.ChildNodes[0].Term.Name)
                                {
                                    case "hash_literal":
                                        signature = literal.ChildNodes[0].ChildNodes[1].Token.ValueString;
                                        break;
                                    default:
                                        signature = literal.ChildNodes[0].Token.ValueString;
                                        break;
                                }

                                identifier = new Identifier(ironyToken.Text, type, ironyToken.Text + "=" + signature);
                            }
                        }

                        if (identifier == null)
                        {
                            identifier = new Identifier(ironyToken.Text, type);
                        }
                        //  Set the identifier's containing file
                        identifier.SetContainingFile(m_filenameStack[0], ironyToken.Location.Line, ironyToken.Location.Column);

                        //  Check if the identifier has already been added
                        if (identifierTags.ContainsKey(ironyToken.Text.ToUpper()))
                        {
                            identifierList = identifierTags[ironyToken.Text.ToUpper()];
                            //  Add the identifier to the list
                            identifierList.Add(identifier);
                        }
                        else
                        {
                            //  Add a new list and the first identifier in that list
                            identifierList = new List<Identifier>();
                            identifierList.Add(identifier);

                            identifierTags.Add(ironyToken.Text.ToUpper(), identifierList);
                        }
                    }
                }
                else
                {
                    //  Probably an assigned variable
                    //  Identifier should be the first child node
                    Token assignedVariableToken = node.ChildNodes[0].ChildNodes[0].Token;

                    if (assignedVariableToken != null)
                    {
                        if (assignedVariableToken.EditorInfo != null && assignedVariableToken.EditorInfo.Color == TokenColor.Identifier)
                        {
                            List<Identifier> identifierList = null;
                            Identifier identifier = new Identifier(assignedVariableToken.Text, type);
                            //  Set the identifier's containing file
                            identifier.SetContainingFile(m_filenameStack[0], assignedVariableToken.Location.Line, assignedVariableToken.Location.Column);

                            //  Check if the identifier has already been added
                            if (identifierTags.ContainsKey(assignedVariableToken.Text.ToUpper()))
                            {
                                identifierList = identifierTags[assignedVariableToken.Text.ToUpper()];
                                //  Add the identifier to the list
                                identifierList.Add(identifier);
                            }
                            else
                            {
                                //  Add a new list and the first identifier in that list
                                identifierList = new List<Identifier>();
                                identifierList.Add(identifier);

                                identifierTags.Add(assignedVariableToken.Text.ToUpper(), identifierList);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adds a function to the completion list from a parse tree node
        /// </summary>
        /// <param name="node">The parse tree node to look at</param>
        /// <param name="identifierTags">The dictionary of identifiers to add to</param>
        static private void AddFunctionIdentifier(ParseTreeNode node, Dictionary<string, List<Identifier>> identifierTags)
        {
            if (node.ChildNodes.Count > 0)
            {
                //  Find the identifier position
                int id = 0;

                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    if (node.ChildNodes[i].Token != null && 
                        node.ChildNodes[i].Token.ValueString.Equals("FUNC", StringComparison.OrdinalIgnoreCase))
                    {
                        id = i + 2;
                    }
                }

                Token ironyToken = node.ChildNodes[id].Token;
                //  Get the signature string
                int start = node.ChildNodes[id - 1].Span.Location.Position;
                int end = node.ChildNodes[id + 1].Span.EndPosition - start;
                string signature = m_stringStack[0].Substring(start, end);

                if (node.ChildNodes[0].Comments != null)
                {
                    signature = CreateCommentSignature(signature, node.ChildNodes[0].Comments);
                }
                
                if (ironyToken != null)
                {
                    Identifier.eIDENTIFIER_TYPE functionType = Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_FUNCTION;
                    if (node.Term.Name == "native_function_declaration")
                    {
                        functionType = Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_NATIVEFUNCTION;
                    }

                    Identifier identifier = new Identifier(ironyToken.Text, functionType, signature);
                    //  Set the identifier's containing file
                    identifier.SetContainingFile(m_filenameStack[0], ironyToken.Location.Line, ironyToken.Location.Column);

                    AddToMethodCache(node, ironyToken.Text);

                    if (ironyToken.EditorInfo != null && ironyToken.EditorInfo.Color == TokenColor.Identifier)
                    {
                        List<Identifier> identifierList = null;

                        //  Check if the identifier has already been added
                        if (identifierTags.ContainsKey(ironyToken.Text.ToUpper()))
                        {
                            identifierList = identifierTags[ironyToken.Text.ToUpper()];
                            //  Add the identifier to the list
                            identifierList.Add(identifier);
                        }
                        else
                        {
                            //  Add a new list and the first identifier in that list
                            identifierList = new List<Identifier>();
                            identifierList.Add(identifier);

                            identifierTags.Add(ironyToken.Text.ToUpper(), identifierList);
                        }
                    }
                }
            }
        }

        private static void AddToMethodCache(ParseTreeNode node, string methodName)
        {
            var paramDecls = new List<ParameterDeclaration>();

            var vardecls = FindFirstTerm(node, "cs_variable_declarations");
            if (vardecls != null)
            {
                List<ParseTreeNode> parameters = new List<ParseTreeNode>();
                FindAllTerms(vardecls, "variable_declaration", parameters);
                foreach (var paramValue in parameters)
                {
                    var parameterDataType = FindFirstTerm(paramValue.ChildNodes[0], "Identifier");
                    if (parameterDataType == null)
                    {
                        parameterDataType = FindNodeNoChild(paramValue.ChildNodes[0]);
                    }

                    var assignedVarOp = FindFirstTerm(paramValue, "assigned_variable_opt");
                    var parameterNameNode = FindFirstTerm(assignedVarOp, "Identifier");
                    var byRefOp = NodeContainsChars(paramValue, "&".ToCharArray());
                    var arrayOptNode = FindFirstTerm(paramValue, "array_opt");
                    // There seems to be a bug with the (Irony??) parser that means that all variable
                    // declarations for parameters have an array_opt added. Seems a bit strange. Leaving
                    // it out for now because it would just cause confusion in the UI.

                    var paramDecl = new ParameterDeclaration();
                    paramDecl.DataTypePostFix = "";
                    paramDecl.DataType = parameterDataType == null ? "UNKNOWN" : parameterDataType.Token.Value.ToString();
                    paramDecl.Name = parameterNameNode.Token.Value.ToString();
                    if (byRefOp)
                    {
                        paramDecl.DataTypePostFix = "&";
                    }

                    paramDecls.Add(paramDecl);
                }
            }

            MethodCache.AddMethod(m_filenameStack[0], methodName, paramDecls);
        }

        static ParseTreeNode FindFirstTerm(ParseTreeNode node, string termName)
        {
            if (node.Term.Name == termName)
            {
                return node;
            }
            else
            {
                foreach (var child in node.ChildNodes)
                {
                    var grandChild = FindFirstTerm(child, termName);
                    if (grandChild != null)
                    {
                        return grandChild;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Returns true if a node is found where the token value contains ALL the characters.
        /// </summary>
        /// <param name="node">Node.</param>
        /// <param name="characters">Characters.</param>
        /// <returns>True if a node has all the characters.</returns>
        static bool NodeContainsChars(ParseTreeNode node, char[] characters)
        {
            foreach (var child in node.ChildNodes)
            {
                var found = NodeContainsChars(child, characters);
                if (found)
                {
                    return true;
                }
            }

            if (node.Token == null)
            {
                return false;
            }
            else if (StringContainsChars(node.Token.ValueString, characters))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns true if the string contains all the given characters.
        /// </summary>
        /// <param name="source">Source string.</param>
        /// <param name="characters">Characters.</param>
        /// <returns>True if the string contains all the given characters.</returns>
        static bool StringContainsChars(string source, char[] characters)
        {
            int count = characters.Length;

            foreach (char c in characters)
            {
                if (source.Contains(c))
                {
                    count--;
                }
            }

            return count == 0;
        }

        static ParseTreeNode FindNodeNoChild(ParseTreeNode node)
        {
            if (node.ChildNodes.Count == 0)
            {
                return node;
            }

            foreach (var child in node.ChildNodes)
            {
                var grandChild = FindNodeNoChild(child);
                if (grandChild != null)
                {
                    return grandChild;
                }
            }

            return null;
        }

        static void FindAllTerms(ParseTreeNode node, string termName, List<ParseTreeNode> matches)
        {
            if (node.Term.Name == termName)
            {
                matches.Add(node);
            }
            else
            {
                foreach (var child in node.ChildNodes)
                {
                    FindAllTerms(child, termName, matches);
                }
            }
        }

        /// <summary>
        /// Adds a procedure to the completion list from a parse tree node
        /// </summary>
        /// <param name="node">The parse tree node to look at</param>
        /// <param name="identifierTags">The dictionary of identifiers to add to</param>
        static private void AddProcIdentifier(ParseTreeNode node, Dictionary<string, List<Identifier>> identifierTags)
        {
            if (node.ChildNodes.Count > 0)
            {
                //  Find the identifier position
                int id = 0;

                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    if (node.ChildNodes[i].Token != null &&
                        node.ChildNodes[i].Token.ValueString.Equals("PROC", StringComparison.OrdinalIgnoreCase))
                    {
                        id = i + 1;
                    }
                }

                Token ironyToken = node.ChildNodes[id].Token;
                //  Get the signature string
                int start = node.ChildNodes[id].Span.Location.Position;
                int end = node.ChildNodes[id + 1].Span.EndPosition - start;
                string signature = m_stringStack[0].Substring(start, end);

                if (node.ChildNodes[0].Comments != null)
                {
                    signature = CreateCommentSignature(signature, node.ChildNodes[0].Comments);
                }
        
                if (ironyToken != null)
                {
                    Identifier.eIDENTIFIER_TYPE procType = Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_FUNCTION;
                    if (node.Term.Name == "native_proc_declaration")
                    {
                        procType = Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_NATIVEFUNCTION;
                    }

                    Identifier identifier = new Identifier(ironyToken.Text, procType, signature);
                    //  Set the identifier's containing file
                    identifier.SetContainingFile(m_filenameStack[0], ironyToken.Location.Line, ironyToken.Location.Column);

                    AddToMethodCache(node, ironyToken.Text);

                    if (ironyToken.EditorInfo != null && ironyToken.EditorInfo.Color == TokenColor.Identifier)
                    {
                        List<Identifier> identifierList = null;

                        //  Check if the identifier has already been added
                        if (identifierTags.ContainsKey(ironyToken.Text.ToUpper()))
                        {
                            identifierList = identifierTags[ironyToken.Text.ToUpper()];
                            //  Add the identifier to the list
                            identifierList.Add(identifier);
                        }
                        else
                        {
                            //  Add a new list and the first identifier in that list
                            identifierList = new List<Identifier>();
                            identifierList.Add(identifier);

                            identifierTags.Add(ironyToken.Text.ToUpper(), identifierList);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates the tool tip showing the comments and signature for a function or method.
        /// </summary>
        /// <param name="methodSignature">Method signature.</param>
        /// <param name="commentList">List of comments.</param>
        /// <returns>The combined comment / method signature tool tip text.</returns>
        static private string CreateCommentSignature(string methodSignature, TokenList commentList)
        {
            string comments = String.Empty;
            foreach(var comment in commentList)
            {
                comments += comment.Text + "\r\n";
            }

            if (!String.IsNullOrEmpty(comments))
            {
                methodSignature = comments + methodSignature;
            }

            return methodSignature;
        }

        /// <summary>
        /// Adds an enum type to the completion list from a parse tree node
        /// </summary>
        /// <param name="node">The parse tree node to look at</param>
        /// <param name="identifierTags">The dictionary of identifiers to add to</param>
        static private void AddEnumTypeIdentifier(ParseTreeNode node, Dictionary<string, List<Identifier>> identifierTags)
        {
            if (node.ChildNodes.Count > 0)
            {
                //  The identifier should be the second node
                Token ironyToken = node.ChildNodes[1].Token;
                //  If this is a forward declaration the identifier node is the third one
                if (node.ChildNodes[0].Token != null && node.ChildNodes[0].Token.Text.ToUpper() == "FORWARD")
                {
                    ironyToken = node.ChildNodes[2].Token;
                }

                if (ironyToken != null)
                {
                    Identifier identifier = new Identifier(ironyToken.Text, Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_ENUM_TYPE);
                    //  Set the identifier's containing file
                    identifier.SetContainingFile(m_filenameStack[0], ironyToken.Location.Line, ironyToken.Location.Column);

                    if (ironyToken.EditorInfo != null && ironyToken.EditorInfo.Color == TokenColor.Identifier)
                    {
                        List<Identifier> identifierList = null;

                        List<string> members = new List<string>();
                        GetEnumMembers(node.ChildNodes[2], members); // 2 is the parent node of the member values
                        
                        if (!m_enumerations.ContainsKey(m_filenameStack[0]))
                        {
                            m_enumerations.Add(m_filenameStack[0], new Dictionary<string, List<string>>());
                        }

                        m_enumerations[m_filenameStack[0]][ironyToken.Text] = members;


                        //  Check if the identifier has already been added
                        if (identifierTags.ContainsKey(ironyToken.Text.ToUpper()))
                        {
                            identifierList = identifierTags[ironyToken.Text.ToUpper()];
                            //  Add the identifier to the list
                            identifierList.Add(identifier);
                        }
                        else
                        {
                            //  Add a new list and the first identifier in that list
                            identifierList = new List<Identifier>();
                            identifierList.Add(identifier);

                            identifierTags.Add(ironyToken.Text.ToUpper(), identifierList);
                        }
                    }
                }
            }
        }
        
        /// <summary>
        /// Parses the tree of the enumeration declaration to determine the member field names. These names
        /// are appended to the supplied members list.
        /// </summary>
        /// <param name="node">Parse tree node.</param>
        /// <param name="members">List of member names.</param>
        static private void GetEnumMembers(ParseTreeNode node, List<string> members)
        {
            if (node.Term is IdentifierTerminal)
            {
                members.Add(node.Token.Value.ToString());
            }
            else
            {
                foreach (var child in node.ChildNodes)
                {
                    GetEnumMembers(child, members);
                }
            }
        }

        /// <summary>
        /// Adds an struct to the completion list from a parse tree node
        /// </summary>
        /// <param name="node">The parse tree node to look at</param>
        /// <param name="identifierTags">The dictionary of identifiers to add to</param>
        static private void AddStructIdentifier(ParseTreeNode node, Dictionary<string, List<Identifier>> identifierTags)
        {
            if (node.ChildNodes.Count > 0)
            {
                //  The identifier should be the second node

                int childNodeId = 1;
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    if (node.ChildNodes[i].Token.Text.ToUpper() == "STRUCT")
                    {
                        childNodeId = i + 1;
                        break;
                    }
                }

                Token ironyToken = node.ChildNodes[childNodeId].Token;
                if (ironyToken != null)
                {
                    if (!m_structMembers.ContainsKey(ironyToken.Text.ToUpper()))
                    {
                        m_structMembers[ironyToken.Text.ToUpper()] = new List<string>();
                        FillMembers(node.ChildNodes, m_structMembers[ironyToken.Text.ToUpper()]);
                    }

                    Identifier identifier = new Identifier(ironyToken.Text, Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_STRUCT);
                    //  Set the identifier's containing file
                    identifier.SetContainingFile(m_filenameStack[0], ironyToken.Location.Line, ironyToken.Location.Column);

                    if (ironyToken.EditorInfo != null && ironyToken.EditorInfo.Color == TokenColor.Identifier)
                    {
                        List<Identifier> identifierList = null;

                        //  Check if the identifier has already been added
                        if (identifierTags.ContainsKey(ironyToken.Text.ToUpper()))
                        {
                            identifierList = identifierTags[ironyToken.Text.ToUpper()];
                            //  Add the identifier to the list
                            identifierList.Add(identifier);
                        }
                        else
                        {
                            //  Add a new list and the first identifier in that list
                            identifierList = new List<Identifier>();
                            identifierList.Add(identifier);

                            identifierTags.Add(ironyToken.Text.ToUpper(), identifierList);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Fill in structure member names.
        /// </summary>
        /// <param name="nodeList">Node list.</param>
        /// <param name="memberNameList">Member name list.</param>
        private static void FillMembers(ParseTreeNodeList nodeList, List<string> memberNameList)
        {
            memberNameList.Clear();

            foreach (var node in nodeList)
            {
                if (node.Term.Name == "struct_member_declarations")
                {
                    foreach (var child in node.ChildNodes)
                    {
                        var variableNode = FindAssignedOpt(child);
                        if (variableNode != null)
                        {
                            string identifier = FindIdentifier(variableNode);
                            if (!String.IsNullOrEmpty(identifier))
                            {
                                string structName = FindTypeName(child);
                                string value = identifier + "!" + structName;

                                var arrayInitVal = FindFirstTerm(child, "array_initializer_value");
                                if (arrayInitVal != null)
                                {
                                    value += "!" + arrayInitVal.FindTokenAndGetText();
                                }


                                memberNameList.Add(value);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Find the type name from the supplied parse tree node. The tree is walked until
        /// the 'builtin_or_user_type' terminal is found. 
        /// </summary>
        /// <param name="node">Parse tree node.</param>
        /// <returns>The type name.</returns>
        private static string FindTypeName(ParseTreeNode node)
        {
            string structName = String.Empty;

            foreach (var child in node.ChildNodes)
            {
                if (child.Term.Name == "builtin_or_user_type")
                {
                    if (child.ChildNodes.Count > 0 && child.ChildNodes[0].ChildNodes.Count > 0)
                    {
                        return child.ChildNodes[0].ChildNodes[0].Token.Value.ToString();
                    }
                    else if (child.ChildNodes.Count > 0)
                    {
                        return child.ChildNodes[0].Token.Value.ToString();
                    }
                }

                structName = FindTypeName(child);
                if (String.IsNullOrEmpty(structName))
                {
                    break;
                }
            }

            return structName;
        }

        /// <summary>
        /// Find the parse tree node that contains the variable declaration. This doesn't 
        /// find the actual identifier, but it finds the root node part of the tree that
        /// will lead us to the member variable's name. <seealso cref="FindIdentifier"/>.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private static ParseTreeNode FindAssignedOpt(ParseTreeNode node)
        {
            if (node.Term.Name == "assigned_variable_opt")
            {
                return node;
            }

            foreach(var child in node.ChildNodes)
            {
                var found = FindAssignedOpt(child);
                if (found != null)
                {
                    return found;
                }
            }

            return null;
        }

        /// <summary>
        /// Find the identifier within a structure member's field parse tree structure.
        /// </summary>
        /// <param name="node">Starting node.</param>
        /// <returns>The member field name.</returns>
        private static string FindIdentifier(ParseTreeNode node)
        {
            if (node.Term is IdentifierTerminal)
            {
                return node.Token.ValueString;
            }

            foreach(var child in node.ChildNodes)
            {
                var found = FindIdentifier(child);
                if (!String.IsNullOrEmpty(found))
                {
                    return found;
                }
            }

            return null;
        }

        /// <summary>
        /// Adds a user type to the completion list from a parse tree node
        /// </summary>
        /// <param name="node">The parse tree node to look at</param>
        /// <param name="identifierTags">The dictionary of identifiers to add to</param>
        static private void AddUserTypeIdentifier(ParseTreeNode node, Dictionary<string, List<Identifier>> identifierTags)
        {
            if (node.ChildNodes.Count > 0)
            {
                //  The identifier should be the second node
                Token ironyToken = node.ChildNodes[1].Token;
                if (ironyToken != null)
                {
                    Identifier identifier = new Identifier(ironyToken.Text, Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_USER_TYPE);
                    //  Set the identifier's containing file
                    identifier.SetContainingFile(m_filenameStack[0], ironyToken.Location.Line, ironyToken.Location.Column);

                    if (ironyToken.EditorInfo != null && ironyToken.EditorInfo.Color == TokenColor.Identifier)
                    {
                        List<Identifier> identifierList = null;

                        //  Check if the identifier has already been added
                        if (identifierTags.ContainsKey(ironyToken.Text.ToUpper()))
                        {
                            identifierList = identifierTags[ironyToken.Text.ToUpper()];
                            //  Add the identifier to the list
                            identifierList.Add(identifier);
                        }
                        else
                        {
                            //  Add a new list and the first identifier in that list
                            identifierList = new List<Identifier>();
                            identifierList.Add(identifier);

                            identifierTags.Add(ironyToken.Text.ToUpper(), identifierList);
                        }
                    }
                }
            }
        }
        #endregion

        #region Include file stuff
        /// <summary>
        /// Get the include paths from the project properties
        /// </summary>
        /// <returns>A string array of include paths</returns>
        static private string[] GetIncludePaths()
        {
            //  If they have already been stored then don't try getting them again
            if (m_storedIncludePaths == null)
            {
                DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
                Array activeSolutionProjects = dte.ActiveSolutionProjects as Array;
                Project project = null;
                string[] includePaths = null;

                if (activeSolutionProjects != null && activeSolutionProjects.Length > 0)
                {
                    //  Get the first project in the active solution
                    project = activeSolutionProjects.GetValue(0) as Project;
                    if (project != null)
                    {
                        //  Get the SanScriptProjectNode (using var type to avoid a circular dependency)
                        var projectNode = project.Object;

                        if (projectNode != null)
                        {
                            //  Get the include paths
                            string path = projectNode.GetProjectProperty("IncludePath", true);
                            //  Trim the '-ipath' part at the beginning
                            if (path.StartsWith("-ipath "))
                            {
                                path = path.Substring("-ipath ".Length);
                            }
                            //  Separate the include paths into an array
                            includePaths = path.Split(';');
                        }
                    }
                }

                if (includePaths != null)
                {
                    for (int i = 0; i < includePaths.Length; i++)
                    {
                        if (!includePaths[i].EndsWith("\\"))
                        {
                            includePaths[i] += "\\";
                        }

                        includePaths[i] = Path.GetFullPath(includePaths[i]);
                    }
                    //  Store the include paths
                    m_storedIncludePaths = includePaths;
                }
            }

            return m_storedIncludePaths;
        }

        /// <summary>
        /// Parses the global include file to get tokens for intellisense
        /// </summary>
        /// <param name="tokenList">The token list to add to</param>
        static public void ParseGlobalIncludeFile(List<SanScriptToken> tokenList)
        {
            DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
            Array activeSolutionProjects = dte.ActiveSolutionProjects as Array;
            Project project = null;

            if (activeSolutionProjects != null && activeSolutionProjects.Length > 0)
            {
                //  Get the first project in the active solution
                project = activeSolutionProjects.GetValue(0) as Project;
                if (project != null)
                {
                    //  Get the SanScriptProjectNode (using var type to avoid a circular dependency)
                    var projectNode = project.Object;

                    if (projectNode != null)
                    {
                        //  Get the include paths
                        string path = projectNode.GetProjectProperty("Include", true);

                        //  Trim the '-include' part at the beginning
                        if (path.StartsWith("-include "))
                        {
                            path = path.Substring("-include ".Length);
                        }

                        //  Parse the file
                        ParseIncludeFile(path);
                    }
                }
            }
        }

        /// <summary>
        /// Tries to find a parsed include file in the list by name
        /// </summary>
        /// <param name="filename">The filename to find</param>
        /// <returns>The parsed include file if found, or null</returns>
        static private ParsedIncludeFile FindParsedIncludeFile(string filename)
        {
            ParsedIncludeFile includeFile = null;
            
            if (m_parsedIncludeFiles.ContainsKey(filename))
            {
                includeFile = m_parsedIncludeFiles[filename];
            }

            return includeFile;
        }

        static public string GetIncludeFullPath(string includeFileName)
        {
            if (m_parsedIncludeFiles.ContainsKey(includeFileName))
            {
                return Path.Combine(m_parsedIncludeFiles[includeFileName].GetFilePath(), includeFileName);
            }

            return String.Empty;
        }

        /// <summary>
        /// Parses a file to get tokens for intellisense
        /// </summary>
        /// <param name="filename">The filename to parse</param>
        static private void ParseIncludeFile(string filename)
        {
            ParsedIncludeFile newParsedInclude = FindParsedIncludeFile(filename);
            //  Don't parse the file if it has already been done
            if (newParsedInclude == null)
            {
                string[] includePaths = GetIncludePaths();
                if (includePaths != null)
                {
                    //  Try and find the include file in the include paths
                    foreach (string includePath in includePaths)
                    {
                        //  Get the full path of the include file
                        string fullPath = includePath + filename;

                        string lastVariableType = "";

                        //  Try and open the file
                        if (File.Exists(fullPath))
                        {
                            //  Push the include filename to the top of the filenames stack
                            m_filenameStack.Insert(0, fullPath);

                            //  Add to the already parsed include files
                            newParsedInclude = new ParsedIncludeFile(filename, includePath);
                            m_parsedIncludeFiles.Add(filename, newParsedInclude);

                            StreamReader reader = new StreamReader(fullPath);

                            if (reader != null)
                            {
                                //  Read the whole thing into a string
                                string includeFile = reader.ReadToEnd();

                                //  Create the parse tree from the text passed in
                                ParseTree parseTree = m_ironyParser.Parse(includeFile);
                                ParseTreeNode root = parseTree.Root;

                                //  Push the string onto the stack
                                m_stringStack.Insert(0, includeFile);

                                //  If the root is null then something went wrong in the parse
                                if (root != null)
                                {
                                    //  Look through tokens for auto complete entries
                                    AddTokensFromNode(root, newParsedInclude.GetIdentifiers(), ref lastVariableType);
                                }

                                //  Pop the string off the stack
                                m_stringStack.RemoveAt(0);

                                //  Pop the top entry from the filenames stack
                                m_filenameStack.RemoveAt(0);

                                //  Close the file
                                reader.Close();

                                break;
                            }

                            //  Pop the top entry from the filenames stack
                            m_filenameStack.RemoveAt(0);
                        }
                    }
                }
            }

            //  Add the parsed identifiers to the included list
            if (newParsedInclude != null && newParsedInclude.GetIdentifiersAdded() == false)
            {
                Dictionary<string, List<Identifier>> identifiers = newParsedInclude.GetIdentifiers();
                foreach (KeyValuePair<string, List<Identifier>> identifier in identifiers)
                {
                    if (!m_includedIdentifierTags.ContainsKey(identifier.Key.ToUpper()))
                    {
                        m_includedIdentifierTags.Add(identifier.Key.ToUpper(), identifier.Value);
                    }
                    else
                    {
                        List<Identifier> list = m_includedIdentifierTags[identifier.Key.ToUpper()];
                        list.AddRange(identifier.Value);
                    }
                }
                newParsedInclude.SetIdentifiersAdded(true);
            }
        }

        /// <summary>
        /// Parses an include/using file to get the identifiers in there for intellisense purposes
        /// </summary>
        /// <param name="node">The using statement node to use</param>
        static private void ParseIncludeFile(ParseTreeNode node)
        {
            if (node.ChildNodes.Count > 1)
            {
                //  Include filename should be the second child node
                Token ironyToken = node.ChildNodes[1].Token;

                if (ironyToken != null)
                {
                    if (ironyToken.EditorInfo != null && ironyToken.EditorInfo.Color == TokenColor.String)
                    {
                        ParseIncludeFile(ironyToken.ValueString);
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// Parses the node and it's child nodes and adds any identifiers
        /// </summary>
        /// <param name="node">The node to parse</param>
        /// <param name="identifierTags">The identifier dictionary to add to</param>
        static private void AddTokensFromNode(ParseTreeNode node, Dictionary<string, List<Identifier>> identifierTags, ref string lastVariableType)
        {
            if (node.ChildNodes.Count > 0)
            {
                //  This node has child nodes, so recurse this function for each child node
                foreach (ParseTreeNode childNode in node.ChildNodes)
                {
                    switch (childNode.Term.Name)
                    {
                        case "variable":
                        {
                            switch (node.Term.Name)
                            {
                                case "enum_member_declarations":
                                {
                                    AddVariableIdentifier(childNode, Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_ENUM_MEMBER, identifierTags, null);
                                }
                                break;
                                case "variable_declaration":
                                default:
                                {
                                    AddVariableIdentifier(childNode, Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_VARIABLE, identifierTags, lastVariableType);
                                }
                                break;
                            }
                        }
                        break;
                        case "const_variable":
                        {
                            AddVariableIdentifier(childNode, Identifier.eIDENTIFIER_TYPE.IDENTIFIER_TYPE_CONST_VARIABLE, identifierTags, null);
                        }
                        break;
                        case "function_declaration":
                        case "native_function_declaration":
                        {
                            AddFunctionIdentifier(childNode, identifierTags);
                            //  Add identifiers from here for the parameters and function body
                            AddTokensFromNode(childNode, identifierTags, ref lastVariableType);
                        }
                        break;
                        case "proc_declaration":
                        case "native_proc_declaration":
                        {
                            AddProcIdentifier(childNode, identifierTags);
                            //  Add identifiers from here for the parameters and function body
                            AddTokensFromNode(childNode, identifierTags, ref lastVariableType);
                        }
                        break;
                        case "using_statement":
                        {
                            ParseIncludeFile(childNode);
                        }
                        break;
                        case "enum_declaration":
                        case "forward_dec_enum_declaration":
                        {
                            AddEnumTypeIdentifier(childNode, identifierTags);
                            //  Add identifiers from here for the enum body
                            AddTokensFromNode(childNode, identifierTags, ref lastVariableType);
                        }
                        break;
                        case "native_type":
                        {
                            AddUserTypeIdentifier(childNode, identifierTags);
                        }
                        break;
                        case "struct_declaration":
                        {
                            AddStructIdentifier(childNode, identifierTags);
                            //  Add identifiers from here for the struct body
                            AddTokensFromNode(childNode, identifierTags, ref lastVariableType);
                        }
                        break;
                        case "typedef_function_declaration":
                        {
                            AddFunctionIdentifier(childNode, identifierTags);
                        }
                        break;
                        case "forward_struct_option":
                        {
                            AddStructIdentifier(childNode, identifierTags);
                        }
                        break;
                        case "typedef_proc_declaration":
                        {
                            AddProcIdentifier(childNode, identifierTags);
                        }
                        break;
                        case "builtin_or_user_type":
                        {
                            lastVariableType = node == null || node.ChildNodes.Count == 0 ? null : node.ChildNodes[0].FindTokenAndGetText();
                            AddTokensFromNode(childNode, identifierTags, ref lastVariableType);
                        }
                        break;
                        default:
                        {
                            AddTokensFromNode(childNode, identifierTags, ref lastVariableType);
                        }
                        break;
                    }
                }
            }
        }

        static public void SetCurrentlyOpeningFile(string filename)
        {
            m_currentlyOpeningFile = filename;
        }

        /// <summary>
        /// Split a string into a list of Tokens
        /// </summary>
        /// <param name="inString">The string to split up</param>
        /// <returns>A new list of Tokens</returns>
        static public List<SanScriptToken> SplitStringIntoTokens(string inString)
        {
            //  Create the new list to return
            List<SanScriptToken> tokenList = new List<SanScriptToken>();
            //  Create the parse tree from the text passed in
            ParseTree parseTree = m_ironyParser.Parse(inString);
            ParseTreeNode root = parseTree.Root;

            //  Push the string onto a stack as we may need to use it later for signatures, etc
            m_stringStack.Insert(0, inString);

            //  Reset the filename stack
            m_filenameStack.Clear();
            
            //  Try parsing the global include file first
            ParseGlobalIncludeFile(tokenList);

            //  If the root is null then something went wrong in the parse
            if (root != null)
            {
                string lastVariableType = "";

                //  Get the name of the current file
                string currentFile = String.Empty;
                DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
                {
                    currentFile = dte.ActiveDocument != null ? dte.ActiveDocument.FullName : "";
                }
                
                if (!String.IsNullOrEmpty(currentFile))
                {
                    //  Add it to the filename stack
                    m_filenameStack.Insert(0, currentFile);
                }
                else if (m_currentlyOpeningFile != null)
                {
                    //  This might be opening now, so get the name from the currently opening file
                    m_filenameStack.Insert(0, m_currentlyOpeningFile);
                }
                //  Look through tokens for auto complete entries
                AddTokensFromNode(root, m_identifierTags, ref lastVariableType);
            }
            
            if (parseTree != null)
            {
                //  Add the normal tokens
                AddTokensFromParseTree(parseTree, tokenList);
            }

            //  Clear the string stack
            m_stringStack.Clear();

            //  Clear the filenames stack
            m_filenameStack.Clear();

            //  Return the list of tokens
            return tokenList;
        }

        /// <summary>
        /// Gets the list of keyword strings
        /// </summary>
        /// <returns>The list of keyword strings</returns>
        static public List<string> GetKeywords()
        {
            return m_keywordTags;
        }

        /// <summary>
        /// Gets the list of Identifiers
        /// </summary>
        /// <returns>The dictionary of Identifiers</returns>
        static public Dictionary<string, List<Identifier>> GetIdentifiers()
        {
            return m_identifierTags;
        }

        /// <summary>
        /// Gets the list of included Identifiers
        /// </summary>
        /// <returns>The dictionary of included Identifiers</returns>
        static public Dictionary<string, List<Identifier>> GetIncludedIdentifiers()
        {
            return m_includedIdentifierTags;
        }
    }
}
