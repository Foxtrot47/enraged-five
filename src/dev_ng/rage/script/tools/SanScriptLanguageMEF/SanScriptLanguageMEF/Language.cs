﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SanScriptLanguageMEF
{
    static public class Language
    {
        static public string[] m_keywords = new string[] {
            //  Following taken from scr.y (linked to this project) in the %tokens section
            "IF", "ELSE", "ELIF", "ENDIF", "WHILE", "ENDWHILE", "REPEAT", "ENDREPEAT", "SCRIPT", "ENDSCRIPT", "RETURN", "EXIT", "ENDPROC", "ENDFUNC", 
            "AND", "OR", "NOT", "GE", "LE", "NE", "LSH", "RSH", "FALLTHRU", "FOR", "FORWARD", "ENDFOR", "TO", "STEP", "TYPEDEF", 
            "PLUS_TIME", "MINUS_TIME", "STRLIT", "USING", "CATCH", "THROW", "CALL", 
            "TYPENAME", "FUNCNAME", "PROCNAME", "INTLIT", "FLOATLIT", "NEWSYM", "VAR_REF", "FIELD", "TRUE", "FALSE", "LABEL", 
            "SWITCH", "ENDSWITCH", "CASE", "DEFAULT", "BREAK", "GOTO", "SCOPE", "ENDSCOPE", 
            "FUNC", "PROC", "NATIVE", "GLOBALS", "ENDGLOBALS", "ENUM", "ENDENUM", "ENUMLIT", "STRUCT", "ENDSTRUCT", "NL", 
            "PLUSPLUS", "MINUSMINUS", "PLUS_EQ", "MINUS_EQ", "TIMES_EQ", "DIVIDE_EQ", "COUNT_OF", "ENUM_TO_INT", "INT_TO_ENUM", "SIZE_OF", 
            "AND_EQ", "OR_EQ", "XOR_EQ", 
            "NATIVE_TO_INT", "INT_TO_NATIVE", "HASH", "DEBUGONLY", "VARARGS", "STRICT_ENUM", "HASH_ENUM", "STRICT_HASH_ENUM", "ANDALSO", "ORELSE", 

            //  Following taken from scr.y (linked to this project) in the program section
            "TIMESTEP", "NULL", "INVALID_POOLINDEX", 

            //  Couldn't find these in scr.y, so added them myself
            "#IF", "#IFDEF", "#ENDIF"
        };

        static public string[] m_variableTypes = new string[] {
            //  Following taken from scr.y (linked to this project) in the %tokens section
            "CONST_INT", "CONST_FLOAT", "TWEAK_INT", "TWEAK_FLOAT", 

            //  Following taken from scr.y (linked to this project) in the program section
            "BOOL", "FLOAT", "INT", "STRING", 
            "TEXT_LABEL", "TEXT_LABEL_3", "TEXT_LABEL_7", "TEXT_LABEL_15", "TEXT_LABEL_23", "TEXT_LABEL_31", "TEXT_LABEL_63", 
            "VECTOR", "POOLINDEX", "STRINGHASH", 
        };

        static Language()
        {

        }
    }
}
