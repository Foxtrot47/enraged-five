﻿using Microsoft.VisualStudio.TextManager.Interop;
using Microsoft.VisualStudio.Text.Editor;

namespace SanScriptLanguageMEF
{
    /// <summary>
    /// Interface containing document specific actions.
    /// </summary>
    public interface IDocumentActions
    {
        #region Methods

        /// <summary>
        /// Register a text view with a command filter.
        /// </summary>
        /// <param name="textView">Text view.</param>
        /// <param name="cmdFilter">Command filter.</param>
        void RegisterFilter(IWpfTextView textView, CommandFilter cmdFilter);

        /// <summary>
        /// Get the full path to the given header. The current line (where the system caret is located) will contain a valid USING statement
        /// and this will be used to generate the full path.
        /// </summary>
        /// <param name="view">Text view.</param>
        /// <returns>The string containing the full path to the header file. Will contain an empty string if the full path cannot be determined.</returns>
        string GetHeaderFullPath(IVsTextView view);

        /// <summary>
        /// Checks the line at the system caret and determines whether it contains a USING statement or not.
        /// </summary>
        /// <param name="view">Text view.</param>
        /// <returns>True if the current line at the system caret contains a valid USING statement.</returns>
        bool LineContainsUsing(IVsTextView view);

        #endregion
    }
}
