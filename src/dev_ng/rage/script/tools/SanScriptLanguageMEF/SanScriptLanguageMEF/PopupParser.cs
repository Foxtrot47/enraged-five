﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SanScriptLanguageMEF
{
    /// <summary>
    /// Helper class used by the method pop-up information window (Ctrl+Shift+Space).
    /// </summary>
    internal class PopupParser
    {
        #region Public methods

        /// <summary>
        /// Parse a line of code to determine the method line and parameter based upon the system caret position. 
        /// This can be used to display pop-up information about the method. 
        /// </summary>
        /// <param name="line">Current line.</param>
        /// <param name="caretPos">System caret position.</param>
        /// <param name="methodName">The name of the method.</param>
        /// <param name="paramId">Zero-based parameter index.</param>
        /// <returns>True if the line contains a valid method signature. Signature does not need to be complete, i.e. can have missing brackets etc.</returns>
        public static bool ParseLine(string line, int caretPos, out string methodName, out int paramId)
        {
            paramId = 0;
            methodName = String.Empty;

            if (line.Trim().StartsWith("//") || line.Trim().StartsWith("/*"))
            {
                return false;
            }

            if (line.Length <= caretPos)
            {
                caretPos = line.Length - 1;
            }
            
            int openBracketPos = -1;

            for (int i = caretPos; i >= 0; i--)
            {
                if (line[i] == '(')
                {
                    openBracketPos = i;
                    break;
                }
            }

            if (openBracketPos < 0)
            {
                return false;
            }

            int closeBracket = line.Length;

            for (int i = caretPos; i < line.Length; i++)
            {
                if (line[i] == ')')
                {
                    closeBracket = i;
                    break;
                }
            }

            int commaCount = 0;
            bool insideVector = false;

            for (int i = openBracketPos; i < closeBracket; i++)
            {
                if (IsVectorStart(line, i))
                {
                    insideVector = true;
                }
                else if (IsVectorEnd(line, i))
                {
                    insideVector = false;
                }

                if (line[i] == ',' && !insideVector)
                {
                    commaCount++;
                }

                if (i == caretPos)
                {
                    paramId = commaCount;
                }
            }

            for (int i = openBracketPos - 1; i >= 0; i--)
            {
                if (IsTerminalChar(line[i]))
                {
                    break;
                }

                methodName = line[i] + methodName;
            }

            return true;
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Determines if the character is a terminal.
        /// </summary>
        /// <param name="chr">Character.</param>
        /// <returns>True if the character is terminal.</returns>
        static bool IsTerminalChar(char chr)
        {
            return chr == ' ' || chr == '\t' || chr == '(' || chr == '[';
        }

        /// <summary>
        /// Determines if the position on the line is the start of a vector literal.
        /// </summary>
        /// <param name="line">Line.</param>
        /// <param name="pos">Position.</param>
        /// <returns>True if the position is the start of a vector literal.</returns>
        static bool IsVectorStart(string line, int pos)
        {
            if (pos + 1 < line.Length)
            {
                return line[pos] == '<' && line[pos + 1] == '<';
            }

            return false;
        }

        /// <summary>
        /// Determines if the position on the line is the end of a vector literal.
        /// </summary>
        /// <param name="line">Line.</param>
        /// <param name="pos">Position.</param>
        /// <returns>True if the position is the end of a vector literal.</returns>
        static bool IsVectorEnd(string line, int pos)
        {
            if (pos + 1 < line.Length)
            {
                return line[pos] == '>' && line[pos + 1] == '>';
            }

            return false;
        }

        #endregion
    }
}
