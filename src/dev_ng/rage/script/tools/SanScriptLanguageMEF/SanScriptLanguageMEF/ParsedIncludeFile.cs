﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SanScriptLanguageMEF
{
    public class ParsedIncludeFile
    {
        private Dictionary<string, List<Identifier>> m_identifierTags = new Dictionary<string, List<Identifier>>();
        private string m_filename = null;
        private string m_filePath = null;
        private bool m_identifiersAdded = false;

        public ParsedIncludeFile(string filename, string path)
        {
            //  Store the filename and path to the file
            m_filename = filename;
            m_filePath = path;
        }

        /// <summary>
        /// Returns the filename
        /// </summary>
        /// <returns>The filename of the include file</returns>
        public string GetFileName()
        {
            return m_filename;
        }

        /// <summary>
        /// Returns the path to the include file
        /// </summary>
        /// <returns>The path to the include file</returns>
        public string GetFilePath()
        {
            return m_filePath;
        }

        /// <summary>
        /// Returns the identifier dictionary
        /// </summary>
        /// <returns>The identifier dictionary</returns>
        public Dictionary<string, List<Identifier>> GetIdentifiers()
        {
            return m_identifierTags;
        }

        /// <summary>
        /// Returns the state of the identifiers added flag
        /// </summary>
        /// <returns>The current state of the identifiers added flag</returns>
        public bool GetIdentifiersAdded()
        {
            return m_identifiersAdded;
        }

        /// <summary>
        /// Set identifiers added flag
        /// </summary>
        /// <param name="added">The value to set the flag to</param>
        public void SetIdentifiersAdded(bool added)
        {
            m_identifiersAdded = added;
        }
    }
}
