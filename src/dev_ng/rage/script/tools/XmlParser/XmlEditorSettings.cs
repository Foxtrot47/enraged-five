using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

using ragScriptEditorShared;
using RSG.Platform;

namespace XmlParser
{
    public class XmlCompilingSettings : CompilingSettings
    {
        public XmlCompilingSettings()
            : base( "Xml" )
        {
            m_includeFileOpenFileDialogFilter = string.Empty;
            m_compilerExecutableOpenFileDialogFilter = string.Empty;
        }

        #region Overrides
        public override string BuildCompileScriptCommand(string scriptFilename, Platform? currentPlatform)
        {
            // cannot compile
            return string.Empty;
        }

        public override IEnumerable<string> BuildOutputFilenames(string scriptFilename, Platform platform)
        {
            // cannot compile
            yield break;
        }

        public override SettingsBase Clone()
        {
            XmlCompilingSettings c = new XmlCompilingSettings();
            c.Copy( this );

            return c;
        }

        public override void Copy( SettingsBase t )
        {
            if ( t is XmlCompilingSettings )
            {
                Copy( t as XmlCompilingSettings );
            }
            else if ( t is CompilingSettings )
            {
                base.Copy( t as CompilingSettings );
            }
        }

        public override void Copy( CompilingSettings c )
        {
            if ( c is XmlCompilingSettings )
            {
                Copy( c as XmlCompilingSettings );
            }
            else
            {
                base.Copy( c );
            }
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new CompilingSettingsControl();
        }

        public override bool Equals( object obj )
        {
            XmlCompilingSettings c = obj as XmlCompilingSettings;
            if ( (object)c == null )
            {
                return false;
            }

            return base.Equals( c );
        }

        public override bool Equals( CompilingSettings c )
        {
            if ( (object)c == null )
            {
                return false;
            }

            if ( c is XmlCompilingSettings )
            {
                return Equals( c as XmlCompilingSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion

        #region Public Functions
        public void Copy( XmlCompilingSettings c )
        {
            if ( c == null )
            {
                return;
            }

            base.Copy( c );
        }

        public bool Equals( XmlCompilingSettings c )
        {
            if ( (object)c == null )
            {
                return false;
            }

            return base.Equals( c );
        }
        #endregion
    }

    public class XmlLangaugeSettings : LanguageSettings
    {
        public XmlLangaugeSettings()
            : base( "Xml" )
        {

        }

        #region Overrides
        public override SettingsBase Clone()
        {
            XmlLangaugeSettings l = new XmlLangaugeSettings();
            l.Copy( this );

            return l;
        }

        public override void Copy( SettingsBase t )
        {
            if ( t is XmlLangaugeSettings )
            {
                Copy( t as XmlLangaugeSettings );
            }
            else if ( t is LanguageSettings )
            {
                base.Copy( t as LanguageSettings );
            }
        }

        public override void Copy( LanguageSettings t )
        {
            if ( t is XmlLangaugeSettings )
            {
                Copy( t as XmlLangaugeSettings );
            }
            else
            {
                base.Copy( t );
            }
        }

        public override SettingsControlBase CreateSettingsControl()
        {
            return new LanguageSettingsControl();
        }

        public override bool Equals( object obj )
        {
            XmlLangaugeSettings l = obj as XmlLangaugeSettings;
            if ( (object)l == null )
            {
                return false;
            }

            return base.Equals( l );
        }

        public override bool Equals( LanguageSettings l )
        {
            if ( (object)l == null )
            {
                return false;
            }

            if ( l is XmlLangaugeSettings )
            {
                return Equals( l as XmlLangaugeSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion

        #region Public Functions
        public void Copy( XmlLangaugeSettings l )
        {
            if ( l == null )
            {
                return;
            }

            base.Copy( l );
        }

        public bool Equals( XmlLangaugeSettings l )
        {
            if ( (object)l == null )
            {
                return false;
            }

            return base.Equals( l );
        }
        #endregion
    }

    public class XmlLanguageDefaultEditorSettings : LanguageDefaultEditorSettingsBase
    {
        public XmlLanguageDefaultEditorSettings()
            : base( "Xml", XmlLanguageDefaultEditorSettings.SettingsVersion )
        {

        }

        #region Constants
        public const float SettingsVersion = 1.0f;
        #endregion

        #region Overrides
        public override CompilingSettings CreateCompilingTabSettings()
        {
            return new XmlCompilingSettings();
        }

        public override LanguageSettings CreateLanguageTabSettings()
        {
            return new XmlLangaugeSettings();
        }

        public override Type GetCompilingSettingsDerivedType()
        {
            return typeof( XmlCompilingSettings );
        }

        public override Type GetLanguageSettingsDerivedType()
        {
            return typeof( XmlLangaugeSettings );
        }
        #endregion
    }
}
