using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

using ActiproSoftware.SyntaxEditor;
using ragScriptEditorShared;
using RSG.Platform;

namespace XmlParser
{
    public class XmlProjectResolver : ActiproSoftware.ComponentModel.DisposableObject, IParserProjectResolver
    {
        public XmlProjectResolver( XmlParserPlugin parserPlugin )
        {
            m_parserPlugin = parserPlugin;
        }

        #region Static Variables
        private static XmlDynamicSyntaxLanguage sm_language = null;
        #endregion

        #region Variables
        private XmlParserPlugin m_parserPlugin;
        private List<string> m_projectFiles = new List<string>();
        #endregion

        #region IParserProjectResolver interface
        public ActiproSoftware.SyntaxEditor.SyntaxLanguage Language
        {
            get
            {
                return sm_language;
            }
        }

        public List<string> ProjectFiles
        {
            get
            {
                return m_projectFiles;
            }
            set
            {
                m_projectFiles = value;
            }
        }

        public bool GetImplementationInformation( SyntaxEditor editor, out string[] fileNames, out TextRange[] textRanges )
        {
            fileNames = null;
            textRanges = null;
            return false;
        }

        public void PreCompile()
        {

        }

        public bool NeedsToBeCompiled( string filename, string projectFile, CompilingSettings compilingSettings )
        {
            // we have no integrated compiler for Xml
            return false;
        }

        public bool NeedsToBeResourced(string filename, string projectFile, CompilingSettings compilingSettings)
        {
            // we have no integrated compiler for Xml
            return false;
        }

        public void FileCompiled( string filename )
        {

        }

        /// <summary>
        /// Determines if resourcing is necessary for the file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="platform"></param>
        /// <param name="projectFile">The project we are compiling, if any.</param>
        /// <param name="compilingSettings">The CompilingSettings we will be using for the compile.</param>
        /// <returns>True to recompile</returns>
        public bool NeedsToBeResourced(String filename, Platform platform, String projectFile, CompilingSettings compilingSettings)
        {
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="platform"></param>
        public void FileResourced(String filename, Platform platform)
        {
        }

        public void PostCompile()
        {

        }

        public void SetLanguage( SyntaxEditor editor, InfoTipTextDelegate infoTipTextDel )
        {
            if ( sm_language == null )
            {
                Stream stream = null;
                try
                {
                    stream = GetType().Module.Assembly.GetManifestResourceStream( "XmlParser.ActiproSoftware.XML.xml" );
                }
                catch ( Exception e )
                {
                    ApplicationSettings.ShowMessage( null, e.ToString(), "GetManifestResourceStream Error", 
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation, DialogResult.OK );
                    return;
                }

                sm_language = ActiproSoftware.SyntaxEditor.Addons.Dynamic.DynamicSyntaxLanguage.LoadFromXml( stream, 0 ) as XmlDynamicSyntaxLanguage;
            }

            editor.Document.Language = sm_language;
            editor.Document.LanguageData = this;

            // FIXME: maybe add code snippet folders here
            // FIXME: maybe verify that we are a ProjectFile or a IncludePathFile
        }

        public void ClearLanguage( SyntaxEditor editor )
        {
            editor.Document.Language = ActiproSoftware.SyntaxEditor.Addons.PlainText.PlainTextSyntaxLanguage.PlainText;
            editor.Document.LanguageData = null;
        }

        public IntellisenseSettingsChangedEventHandler IntellisenseSettingsChangedEventHandler
        {
            get
            {
                return new IntellisenseSettingsChangedEventHandler( IntellisenseSettingsChanged );
            }
        }

        public void RefreshIntellisense()
        {
            // do nothing
        }

        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                sm_language.Dispose();
            }

            base.Dispose( disposing );
        }
        #endregion

        #region Event Handlers
        private void IntellisenseSettingsChanged( object sender, IntellisenseSettingsChangedEventArgs e )
        {
            sm_language.AutocompleteEnabled = e.IntellisenseSettings.PerformSemanticParsing && e.IntellisenseSettings.Autocomplete;

            if ( e.Editor.Document.Language.Key == this.Language.Key )
            {
                e.Editor.Document.SemanticParsingEnabled = e.IntellisenseSettings.PerformSemanticParsing;
            }
        }

        public IEnumerable<string> GetIncludesForFile(string filename)
        {
            yield break;
        }
        #endregion
    }
}
