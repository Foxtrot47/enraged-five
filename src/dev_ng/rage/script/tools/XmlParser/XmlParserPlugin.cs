using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using ragScriptEditorShared;

namespace XmlParser
{
    public class XmlParserPlugin : IParserPlugin
    {
        public XmlParserPlugin()
        {
            m_extensions.Add( ".xml" );
        }
        
        #region Variables
        private List<string> m_extensions = new List<string>();

        XmlLangaugeSettings m_currentLanguageSettings = new XmlLangaugeSettings();
        XmlLanguageDefaultEditorSettings m_defaultEditorSettings = new XmlLanguageDefaultEditorSettings();

        List<IParserProjectResolver> m_projectResolvers = new List<IParserProjectResolver>();
        #endregion

        #region IParserPlugin Interface
        /// <summary>
        /// Get:  The name of this language
        /// </summary>
        public string LanguageName
        {
            get
            {
                return "Xml";
            }
        }

        /// <summary>
        /// Retrieves a list of all of the file extensions for this language
        /// </summary>
        public List<string> Extensions 
        { 
            get
            {
                return m_extensions;
            }
        }

        /// <summary>
        /// Retrieves a list of all of the file extensions for this language that can be compiled
        /// </summary>
        public List<string> CompilableExtensions 
        { 
            get
            {
                // nothing can be compiled at this time
                return new List<string>();
            }
        }

        /// <summary>
        /// Get:  Retrieves the Default Editor Settings that were set or loaded by LoadSettings()
        /// </summary>
        public LanguageDefaultEditorSettingsBase DefaultEditorSettings 
        { 
            get
            {
                return m_defaultEditorSettings;
            }
        }

        /// <summary>
        /// Gets or sets the file that is implicitly included in all files in this language
        /// </summary>
        public string IncludeFile 
        { 
            get
            {
                return string.Empty;
            }
            set
            {
                // do nothing
            }
        }

        /// <summary>
        /// Gets or sets the include path.  When set, should resolve IncludePathFiles.
        /// </summary>
        public IEnumerable<string> IncludePaths 
        {
            get
            {
                return new String[0];
            }
            set
            {
                // do nothing
            }
        }

        /// <summary>
        /// Gets the files in the IncludePath that are part of this language.  Should be set when
        /// IncludePaths is set.
        /// </summary>
        public IEnumerable<string> IncludePathFiles 
        { 
            get
            {
                return new String[0];
            }
        }

        /// <summary>
        /// Get:  Gets the current language settings that are used by this plugin
        /// </summary>
        public LanguageSettings CurrentLanguageSettings 
        { 
            get
            {
                return m_currentLanguageSettings;
            }
        }

        /// <summary>
        /// Get a list of all Project Resolvers that have been created by this plugin
        /// </summary>
        public List<IParserProjectResolver> ProjectResolvers 
        { 
            get
            {
                return m_projectResolvers;
            }
        }

        public string Custom
        {
            get { return string.Empty; }
            set {  }    //  Do nothing
        }

        /// <summary>
        /// Initialize the plugin.  Could involve calling LoadSettings()
        /// </summary>
        /// <param name="directory"></param>
        public void InitPlugin( string directory )
        {
            LoadSettings();
        }

        /// <summary>
        /// Shutdown the plugin and dispose of any unmanaged objects
        /// </summary>
        public void ShutdownPlugin()
        {
            foreach ( IParserProjectResolver projectResolver in m_projectResolvers )
            {
                projectResolver.Dispose();
            }

            m_projectResolvers.Clear();
        }

        /// <summary>
        /// Loads the Editor Settings file for this language into DefaultEditorSettings
        /// </summary>
        /// <returns>True on success</returns>
        public bool LoadSettings()
        {
            this.CurrentLanguageSettings.Copy( m_defaultEditorSettings.Language );

            return true;
        }

        /// <summary>
        /// Saves DefaultEditorSettings to the Editor Settings file.  The Script Editor will handle
        /// copying CurrentLanguageSettings to DefaultEditorSettings.LanguageSettings before calling 
        /// this function.
        /// </summary>
        /// <returns>True on success</returns>
        public bool SaveSettings()
        {
            m_defaultEditorSettings.Language.Copy( m_currentLanguageSettings );

            return true;
        }

        /// <summary>
        /// Creates a new Project Resolver.  One of these will be created
        /// for an entire project.
        /// </summary>
        /// <param name="projectFiles"></param>
        /// <returns></returns>
        public IParserProjectResolver CreateProjectResolver( List<string> projectFiles )
        {
            XmlProjectResolver projectResolver = new XmlProjectResolver( this );
            projectResolver.ProjectFiles = projectFiles;

            m_projectResolvers.Add( projectResolver );

            return projectResolver;
        }

        /// <summary>
        /// Creates a new Project Resolver.  One of these will be created
        /// for each file that is not part of a project
        /// </summary>
        /// <param name="includeFile"></param>
        /// <param name="includePaths"></param>
        /// <returns></returns>
        public IParserProjectResolver CreateProjectResolver( string filename )
        {
            List<string> filenames = new List<string>();
            filenames.Add( filename );
            return CreateProjectResolver( filenames );
        }

        /// <summary>
        /// Removes the projectResolver from the list returned by ProjectResolvers and calls Dispose() if necessary
        /// </summary>
        /// <param name="projectResolver"></param>
        public void DestroyProjectResolver( IParserProjectResolver projectResolver )
        {
            m_projectResolvers.Remove( projectResolver );
            projectResolver.Dispose();
        }

        /// <summary>
        /// Called by the main form when the active syntax editor is changed.
        /// </summary>
        /// <param name="editor"></param>
        /// <param name="treeView"></param>
        /// <returns></returns>
        public bool UpdateDocumentOutline( ActiproSoftware.SyntaxEditor.SyntaxEditor editor, TreeView treeView )
        {
            /* FIXME
            if ( editor.Document.SemanticParseData is ICompilationUnit )
            {
                // Load the document outline 
                ICompilationUnit compilationUnit = (ICompilationUnit)editor.Document.SemanticParseData;
                CSharpDocumentOutlineUpdater docOutliner = new CSharpDocumentOutlineUpdater( editor.Document.Guid, treeView );
                docOutliner.Process( compilationUnit );

                // Print the tree of the AST (for debugging only)
                // Trace.WriteLine(compilationUnit.ToStringTree());
                return true;
            }
            */

            return false;
        }

        public void OnLanguageSettingsUpdated()
        {
            // do nothing
        }

        public List<ToolStripMenuItem> HelpMenuItems
        {
            get
            {
                // no help
                return null;
            }
        }
        #endregion
    }
}
