using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using ActiproSoftware.SyntaxEditor;
using ActiproSoftware.SyntaxEditor.Addons.DotNet.Ast;
using ActiproSoftware.SyntaxEditor.Addons.Xml.Ast;

namespace XmlParser
{

    /// <summary>
    /// Provides a class for updating a document outline <see cref="TreeView"/> with nodes from various language ASTs.
    /// </summary>
    public class XmlDocumentOutlineUpdater
    {
        /// <summary>
        /// Initializes a new instance of the <c>XmlDocumentOutlineUpdater</c> class. 
        /// </summary>
        /// <param name="m_guid">The GUID to examine.</param>
        /// <param name="m_documentOutlineTreeView">The <see cref="TreeView"/> to update.</param>
        public XmlDocumentOutlineUpdater( string m_guid, TreeView m_documentOutlineTreeView )
        {
            // Initialize parameters
            this.m_guid = m_guid;
            this.m_documentOutlineTreeView = m_documentOutlineTreeView;
        }

        #region Variables
        private TreeView m_documentOutlineTreeView;
        private string m_guid;
        private TreeNode m_parentTreeNode;
        private List<int> m_treeNodeChildren;
        private List<int> m_treeNodeIndices;
        #endregion

        #region Private Properties
        /// <summary>
        /// Gets or sets the current tree node index.
        /// </summary>
        /// <value>The current tree node index.</value>
        private int CurrentTreeNodeIndex
        {
            get
            {
                return m_treeNodeIndices[0];
            }
            set
            {
                m_treeNodeIndices[0] = value;
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Processes the specified root <see cref="ICompilationUnit"/>.
        /// </summary>
        /// <param name="compilationUnit">The <see cref="ICompilationUnit"/> to examine.</param>
        public void Process( ICompilationUnit compilationUnit )
        {
            this.ProcessNode( compilationUnit, compilationUnit );
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Ends the current node and optionally moves up a level.
        /// </summary>
        /// <param name="moveUp">Whether to move up in the <see cref="TreeNode"/> context.</param>
        private void EndNode( bool moveUp )
        {
            // Remove any inappropriate child nodes
            if ( !moveUp && (m_parentTreeNode != null) && (this.CurrentTreeNodeIndex > 0)
                && (this.CurrentTreeNodeIndex - 1 < m_parentTreeNode.Nodes.Count) )
            {
                this.RemoveChildNodes( m_parentTreeNode.Nodes[this.CurrentTreeNodeIndex - 1], this.ParentTreeNodeChildCount );
            }

            // Remove the child count for the node 
            m_treeNodeChildren.RemoveAt( 0 );

            if ( moveUp )
            {
                if ( m_parentTreeNode != null )
                {
                    // Remove any remaining nodes
                    this.RemoveChildNodes( m_parentTreeNode, this.CurrentTreeNodeIndex );

                    // Remove the current level's index
                    m_treeNodeIndices.RemoveAt( 0 );

                    // Move up a level
                    m_parentTreeNode = m_parentTreeNode.Parent;
                }
                else
                {
                    // Remove any remaining nodes
                    while ( this.CurrentTreeNodeIndex < m_documentOutlineTreeView.Nodes.Count )
                    {
                        m_documentOutlineTreeView.Nodes.RemoveAt( this.CurrentTreeNodeIndex );
                    }
                }
            }
        }

        /// <summary>
        /// Ends the root node.
        /// </summary>
        private void EndRoot()
        {
            this.EndNode( true );
#if !NET11
            if ( (m_documentOutlineTreeView.Tag != (object)m_guid) && (m_documentOutlineTreeView.Nodes.Count > 0) )
            {
                m_documentOutlineTreeView.TopNode = m_documentOutlineTreeView.Nodes[0];
            }
#endif
            m_documentOutlineTreeView.EndUpdate();
        }

        /// <summary>
        /// Gets or sets the parent tree node child count.
        /// </summary>
        /// <value>The parent tree node child count.</value>
        private int ParentTreeNodeChildCount
        {
            get
            {
                return m_treeNodeChildren[0];
            }
            set
            {
                m_treeNodeChildren[0] = value;
            }
        }

        /// <summary>
        /// Processes the child nodes of the specified <see cref="IAstNode"/>.
        /// </summary>
        /// <param name="compilationUnit">The parent <see cref="ICompilationUnit"/>.</param>
        /// <param name="node">The <see cref="IAstNode"/> to examine.</param>
        private void ProcessChildNodes( ICompilationUnit compilationUnit, IAstNode node )
        {
            // Recurse
            if ( node.ChildNodeCount > 0 )
            {
                if ( node is ICompilationUnit )
                {
                    compilationUnit = (ICompilationUnit)node;
                }

                foreach ( IAstNode childNode in node.ChildNodes )
                {
                    this.ProcessNode( compilationUnit, childNode );
                }
            }
        }

        /// <summary>
        /// Processes the specified <see cref="IAstNode"/>.
        /// </summary>
        /// <param name="compilationUnit">The parent <see cref="ICompilationUnit"/>.</param>
        /// <param name="node">The <see cref="IAstNode"/> to examine.</param>
        private void ProcessNode( ICompilationUnit compilationUnit, IAstNode node )
        {
            if ( node is ActiproSoftware.SyntaxEditor.Addons.Xml.Ast.AstNode )
            {
                // Process XML node
                this.ProcessXmlNode( compilationUnit, (ActiproSoftware.SyntaxEditor.Addons.Xml.Ast.AstNode)node );
            }
            else if ( node is LanguageTransitionAstNode )
            {
                // Process child nodes
                this.ProcessChildNodes( compilationUnit, node );
            }
            else
            {
                // Show all nodes for other language
                this.ProcessOtherLanguageNode( compilationUnit, node );
            }
        }

        /// <summary>
        /// Processes the specified <see cref="IAstNode"/>.
        /// </summary>
        /// <param name="compilationUnit">The parent <see cref="ICompilationUnit"/>.</param>
        /// <param name="node">The <see cref="IAstNode"/> to examine.</param>
        private void ProcessOtherLanguageNode( ICompilationUnit compilationUnit, IAstNode node )
        {
            // Process a node start
            if ( node is ICompilationUnit )
            {
                if ( node.ParentNode == null )
                {
                    this.StartRoot();
                }
            }
            else
            {
                this.StartNode( true, node );
            }

            // Process child nodes
            this.ProcessChildNodes( compilationUnit, node );

            // Process a node end
            if ( node is ICompilationUnit )
            {
                if ( node.ParentNode == null )
                {
                    this.EndRoot();
                }
            }
            else
            {
                this.EndNode( true );
            }
        }

        /// <summary>
        /// Processes the specified <see cref="IAstNode"/>.
        /// </summary>
        /// <param name="compilationUnit">The parent <see cref="ICompilationUnit"/>.</param>
        /// <param name="node">The <see cref="IAstNode"/> to examine.</param>
        private void ProcessXmlNode( ICompilationUnit compilationUnit, ActiproSoftware.SyntaxEditor.Addons.Xml.Ast.AstNode node )
        {
            // Process a node start
            switch ( node.NodeType )
            {
                case XmlNodeType.CompilationUnit:
                    if ( node.ParentNode == null )
                    {
                        this.StartRoot();
                    }
                    break;
                case XmlNodeType.Element:
                    this.StartNode( ((((Element)node).HasChildElements) || (node.LanguageTransitions.Count > 0)), node );
                    break;
                default:
                    this.StartNode( false, node );
                    break;
            }

            // Process child nodes
            this.ProcessChildNodes( compilationUnit, node );

            // Process a node end
            switch ( node.NodeType )
            {
                case XmlNodeType.CompilationUnit:
                    if ( node.ParentNode == null )
                    {
                        this.EndRoot();
                    }
                    break;
                case XmlNodeType.Element:
                    this.EndNode( ((((Element)node).HasChildElements) || (node.LanguageTransitions.Count > 0)) );
                    break;
                default:
                    this.EndNode( false );
                    break;
            }
        }

        /// <summary>
        /// Removes any child nodes after the specified count.
        /// </summary>
        /// <param name="node">The parent <see cref="TreeNode"/>.</param>
        /// <param name="count">The target count of child nodes.</param>
        private void RemoveChildNodes( TreeNode node, int count )
        {
            while ( count < node.Nodes.Count )
            {
                node.Nodes.RemoveAt( count );
            }
        }

        /// <summary>
        /// Adds a new <see cref="TreeNode"/> to the <see cref="TreeView"/>.
        /// </summary>
        /// <param name="moveDown">Whether to move down in <see cref="TreeNode"/> context after the new <see cref="TreeNode"/> is added.</param>
        /// <param name="node">The <see cref="IAstNode"/> represented by the <see cref="TreeNode"/>.</param>
        private void StartNode( bool moveDown, IAstNode node )
        {
            this.StartNode( moveDown, node, node.DisplayText, node.ImageIndex );
        }

        /// <summary>
        /// Adds a new <see cref="TreeNode"/> to the <see cref="TreeView"/>.
        /// </summary>
        /// <param name="moveDown">Whether to move down in <see cref="TreeNode"/> context after the new <see cref="TreeNode"/> is added.</param>
        /// <param name="node">The <see cref="IAstNode"/> represented by the <see cref="TreeNode"/>.</param>
        /// <param name="text">The text.</param>
        /// <param name="imageIndex">The image index.</param>
        private void StartNode( bool moveDown, IAstNode node, string text, int imageIndex )
        {
            // Increase the child count
            this.ParentTreeNodeChildCount++;

            // Initialize a child count for the node 
            m_treeNodeChildren.Insert( 0, 0 );

            // Determine whether a new node is needed
            bool createNewNode = false;
            if ( m_parentTreeNode == null )
            {
                if ( this.CurrentTreeNodeIndex >= m_documentOutlineTreeView.Nodes.Count )
                {
                    createNewNode = true;
                }
            }
            else if ( this.CurrentTreeNodeIndex >= m_parentTreeNode.Nodes.Count )
            {
                createNewNode = true;
            }

            TreeNode treeNode;
            if ( createNewNode )
            {
                // Create a new node
                treeNode = new TreeNode();
                treeNode.Text = text;
                treeNode.ImageIndex = imageIndex;
                treeNode.SelectedImageIndex = imageIndex;
                treeNode.Tag = node;

                // Add the new node
                if ( m_parentTreeNode == null )
                {
                    m_documentOutlineTreeView.Nodes.Add( treeNode );
                }
                else
                {
                    m_parentTreeNode.Nodes.Add( treeNode );
                    if ( !m_parentTreeNode.IsExpanded )
                    {
                        m_parentTreeNode.Expand();
                    }
                }
            }
            else
            {
                // Get the existing node
                if ( m_parentTreeNode == null )
                {
                    treeNode = m_documentOutlineTreeView.Nodes[this.CurrentTreeNodeIndex];
                }
                else
                {
                    treeNode = m_parentTreeNode.Nodes[this.CurrentTreeNodeIndex];
                }

                // Update the existing node
                if ( treeNode.Text != text )
                {
                    treeNode.Text = text;
                }

                if ( treeNode.ImageIndex != imageIndex )
                {
                    treeNode.ImageIndex = imageIndex;
                    treeNode.SelectedImageIndex = imageIndex;
                }
                
                treeNode.Tag = node;
            }

            // Increment the index
            this.CurrentTreeNodeIndex++;

            // If we are moving down a level...
            if ( moveDown )
            {
                m_parentTreeNode = treeNode;
                m_treeNodeIndices.Insert( 0, 0 );
            }
        }

        /// <summary>
        /// Starts the root node.
        /// </summary>
        private void StartRoot()
        {
            m_documentOutlineTreeView.BeginUpdate();
            if ( m_documentOutlineTreeView.Tag != (object)m_guid )
            {
                m_documentOutlineTreeView.Nodes.Clear();
            }

            m_parentTreeNode = null;
            m_treeNodeIndices = new List<int>();
            m_treeNodeIndices.Add( 0 );
            m_treeNodeChildren = new List<int>();
            m_treeNodeChildren.Add( 0 );
        }
        #endregion
    }
}