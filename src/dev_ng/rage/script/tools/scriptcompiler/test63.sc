USING "rage_builtins.sch"

INT ga
FLOAT gb
PROC SOME_FUNC(INT a=0, FLOAT b=1.0)
	a = a
	b = b
ENDPROC

PROC SOME_FUNC_2(INT a, FLOAT b)
	a=a
	b=b
ENDPROC

// this exposed a bug when calling a function with no parameters when
// one or more are defaulted.
SCRIPT
	SOME_FUNC()
	WAIT(ga + FLOOR(gb))
ENDSCRIPT

