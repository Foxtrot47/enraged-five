USING "rage_builtins.sch"

STRUCT GWTestStruct
      INT FirstInt
      float FirstFloat
      TEXT_LABEL myTextLabel
ENDSTRUCT

CONST_INT NUMBER_OF_STRUCTS   4

GWTestStruct      ArrayOfStructs[NUMBER_OF_STRUCTS]

STRUCT StructContainingArray
      GWTestStruct      m_ArrayOfStructs[NUMBER_OF_STRUCTS]
ENDSTRUCT

StructContainingArray MyBigStruct

PROC PrintSizeOfArray(GWTestStruct &InArray[])
      PRINTSTRING("PrintSizeOfArray - Size = ")
      // PRINTINT(SIZE_OF(InArray))
      PRINTNL()
ENDPROC

PROC PrintSizeOfBigStuct(StructContainingArray &InStruct)
      PRINTSTRING("PrintSizeOfBigStuct - Size = ")
      PRINTINT(SIZE_OF(InStruct))
      PRINTNL()
ENDPROC

FUNC VECTOR GetPos()
	RETURN <<1,2,3>>
ENDFUNC

// ===========================================================================================================

SCRIPT

      PRINTSTRING("SIZE_OF(GWTestStruct) = ")
      PRINTINT(SIZE_OF(GWTestStruct))
      PRINTNL()

      PRINTSTRING("SIZE_OF(GWTestStruct) * NUMBER_OF_STRUCTS = ")
      PRINTINT(SIZE_OF(GWTestStruct) * NUMBER_OF_STRUCTS)
      PRINTNL()

      PRINTSTRING("SIZE_OF(ArrayOfStructs) = ")
      PRINTINT(SIZE_OF(ArrayOfStructs))
      PRINTNL()
      
      PrintSizeOfArray(ArrayOfStructs)

      PRINTSTRING("SIZE_OF(StructContainingArray) = ")
      PRINTINT(SIZE_OF(StructContainingArray))
      PRINTNL()

      PRINTSTRING("SIZE_OF(MyBigStruct) = ")
      PRINTINT(SIZE_OF(MyBigStruct))
      PRINTNL()
      
      // PRINTLN("VECTOR IS ",GetPos())
      
      PrintSizeOfBigStuct(MyBigStruct)
ENDSCRIPT