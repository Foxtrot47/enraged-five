USING "rage_builtins.sch"

TYPEDEF FUNC INT callback1(INT A,INT B)

TYPEDEF PROC callback2(FLOAT C)

FUNC INT my_callback(INT A,INT B)
	RETURN A + B
ENDFUNC

PROC my_callback2(FLOAT A)
	PRINTFLOAT (A)
ENDPROC
	
SCRIPT
	callback1 cb1 = &my_callback
	
	callback2 cb2 = &my_callback2
	
	INT C = CALL cb1(10,20)
	
	PRINTINT(C)
	PRINTNL()
	
	CALL cb2(12.34)
ENDSCRIPT
