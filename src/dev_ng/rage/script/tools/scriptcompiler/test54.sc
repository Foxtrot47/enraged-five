// This should not compile.
NATIVE FUNC INT foobar(INT a,INT b = 7,INT c)

// Neither should these.
NATIVE FUNC INT foobar2(INT A,INT b = 7.0)
NATIVE FUNC INT foobar3(INT A,FLOAT b = 0)
NATIVE FUNC INT foobar4(INT A,FLOAT b = FALSE)

SCRIPT
ENDSCRIPT
