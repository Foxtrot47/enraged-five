#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc,char **argv) {
	unsigned addr;
	if (argc != 3) {
		printf("%s pc-addr scd-file-name\n",argv[0]);
		return 1;
	}
	addr = strtoul(argv[1],0,0);
	FILE *scd = fopen(argv[2],"rt");
	if (!scd) {
		printf("%s: cannot open scd file '%s'...\n",argv[2]);
		return 1;
	}
	printf("Searching for line info for addr %u(0x%x)...\n",addr,addr);
	char line[512];
	int state = 0;
	int entryCount;
	unsigned matchLn = 0, matchFi = 0;
	unsigned prevPc = 0, prevLn = 0, prevFi = 0;
	while (fgets(line,sizeof(line),scd)) {
		if (!strncmp(line,"[LINEINFO]",10)) {
			state = 1;
			fgets(line,sizeof(line),scd);	 // consume the count
			entryCount = atoi(line);
		}
		else if (state == 1) {
			unsigned pc, ln, fi;
			sscanf(line,"%d:%d:%d",&pc,&ln,&fi);
			if (pc >= prevPc && addr < pc && !matchLn) {
				matchLn = prevLn;
				matchFi = prevFi;
				printf("Line number %u, file index %u\n",matchLn,matchFi);
			}
			prevPc = pc;
			prevLn = ln;
			prevFi = fi;
			--entryCount;
			if (entryCount == 0) {
				fgets(line,sizeof(line),scd);	// skip the file count
				state = 2;
			}
		}
		else if (state == 2) {
			if (matchFi-- == 0) {
				printf("Line %u of file %s",matchLn,line);
				break;
			}
		}
	}
	fclose(scd);
	return matchLn==0? 1 : 0;
}
