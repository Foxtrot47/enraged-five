STRICT_ENUM FOO
	A,
	B,
	C
ENDENUM

PROC BAZ(FOO F)
	IF F = C
	ENDIF
ENDPROC

SCRIPT
	BAZ(A)			// good
	IF A <> B
	ENDIF
	BAZ(A | B)		// bad
ENDSCRIPT
