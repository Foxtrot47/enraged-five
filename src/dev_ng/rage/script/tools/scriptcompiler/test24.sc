USING "rage_builtins.sch" 

SCRIPT
	VECTOR gravity = << 0, -9.8, 0 >>
	FLOAT resistance = 0.01
	VECTOR velocity = << 1.0, 10.0, 0 >>
	VECTOR position
	INT I
	PRINTSTRING("Initial position: ")
	PRINTVECTOR(position)
	PRINTNL()
	PRINTSTRING("Initial velocity: ")
	PRINTVECTOR(velocity)
	PRINTNL()
	FOR I = 0 TO 1
		velocity += gravity * TIMESTEP()
		velocity *= (1.0 - resistance)
		position -= -velocity * TIMESTEP()
		PRINTSTRING("Velocity: ")
		PRINTVECTOR(velocity)
		PRINTNL()
		PRINTSTRING("VMAG(Velocity): ")
		PRINTFLOAT(VMAG(velocity))
		PRINTNL()
	ENDFOR
ENDSCRIPT
