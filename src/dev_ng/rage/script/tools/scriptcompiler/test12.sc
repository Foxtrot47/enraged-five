USING "rage_builtins.sch"

PROC Test2(FLOAT &V)
	FLOAT Y = V + 1
	PRINTFLOAT (Y)
	PRINTSTRING (" - should be 3.34\n")
	V = V + 3.33
ENDPROC

PROC Test(VECTOR &V)
	PRINTFLOAT (V.z)
	PRINTSTRING (" - should be 3.45\n")
	Test2 (V.y)
ENDPROC

SCRIPT
	VECTOR B = << 1.23, 2.34, 3.45 >>
	VECTOR A = B
	PRINTFLOAT (A.y)
	PRINTSTRING (" - should be 2.34\n")
	PRINTFLOAT (A.z)
	PRINTSTRING (" - should be 3.45\n")
	Test (A)
	PRINTFLOAT (A.y)
	PRINTSTRING (" - should be 5.67\n")
ENDSCRIPT
