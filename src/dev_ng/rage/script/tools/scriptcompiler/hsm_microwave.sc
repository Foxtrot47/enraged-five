/*
	A simple microwave oven.
	
	To operate the oven, open the door, place the food inside,
	and press start.  While cooking the oven can be paused, the
	door can be opened, or cooking can be cancelled.
*/

USING "rage_builtins.sch"

//Begin with the door closed
DEFAULTSTATE DoorClosed

//Handles all OpenDoor events regardless of the current state
EVENT OpenDoor()
	GOTOSTATE DoorOpen
ENDEVENT

STATE DoorClosed

	//Begin in the Ready state
	DEFAULTSTATE Ready
	
	//Remember our substates when we're deactivated.  This
	//comes in handy, for example, when the door is opened.
	//On an OpenDoor event we'll transition out of DoorClosed
	//and into DoorOpen.  When the door is closed we'll
	//transition back into DoorClosed and because DoorClosed
	//has history it will remember if it was in Cooking, Ready,
	//or Paused and automatically go back to that state.
	HISTORY
	
	ACTIVATE
		PRINTSTRING("<Door closed>\n")
	ENDACTIVATE
	
	STATE Cooking
		ACTIVATE
			PRINTSTRING("<Cooking>\n")
		ENDACTIVATE
		
		EVENT Done()
			GOTOSTATE Ready
		ENDEVENT

		EVENT Pause()
			GOTOSTATE Paused
		ENDEVENT

		EVENT Cancel()
			GOTOSTATE Ready
		ENDEVENT
		
		EVENT OpenDoor()
			//Turn off the magnetron and call the superstate's handler
			PRINTSTRING("Turning off magnetron...\n")
			SUPER.OpenDoor()
		ENDEVENT
	ENDSTATE
	
	STATE Ready
		ACTIVATE
			PRINTSTRING("<Ready>\n")
		ENDACTIVATE
		
		EVENT Start(INT numSeconds)
			PRINTSTRING("Cooking for ")
			PRINTINT(numSeconds)
			PRINTSTRING(" seconds\n")
			GOTOSTATE Cooking
		ENDEVENT
	ENDSTATE

	STATE Paused
		ACTIVATE
			PRINTSTRING("<Paused>\n")
		ENDACTIVATE
		
		EVENT Unpause()
			GOTOSTATE Cooking
		ENDEVENT
	ENDSTATE
ENDSTATE

STATE DoorOpen
	ACTIVATE
		PRINTSTRING("<Door open>\n")
	ENDACTIVATE
	
	EVENT CloseDoor()
		//Go back to DoorClosed.  DoorClosed will remember
		//what it was doing.
		GOTOSTATE DoorClosed
	ENDEVENT
ENDSTATE

SCRIPT
	PRINTSTRING("Opening door... ")
	OpenDoor()
	
	PRINTSTRING("Closing door... ")
	CloseDoor()
	
	PRINTSTRING("Starting... ")
	Start(30)
	
	PRINTSTRING("Pausing... ")
	Pause()
	
	PRINTSTRING("Opening door... ")
	OpenDoor()
	
	PRINTSTRING("Unpausing... ") PRINTNL()
	//The DoorOpen state doesn't handle Unpause so this
	//will have no effect.
	Unpause()
	
	PRINTSTRING("Closing door... ")
	CloseDoor()
	
	PRINTSTRING("Unpausing... ")
	Unpause()
	
	PRINTSTRING("Opening door... ")
	OpenDoor()
	
	PRINTSTRING("Closing door... ")
	CloseDoor()
	
	PRINTSTRING("Finishing... ")
	Done()
ENDSCRIPT
