USING "Sprite.sc"
USING "rage_builtins.sch"

FUNC INT Factorial (INT N)
	IF N > 1
		RETURN N * Factorial(N-1)
	ELSE
		RETURN N
	ENDIF
ENDFUNC
	
SCRIPT
	SPRITE S = NULL // CREATE_SPRITE("Paddle")
	SPRITE T = NULL
	IF S != T OR TIMERA() < 1000
		HIDE_SPRITE(NULL)
	ENDIF
	// MOVE_SPRITE(S,100,100)
	// SHOW_SPRITE(S)
ENDSCRIPT
