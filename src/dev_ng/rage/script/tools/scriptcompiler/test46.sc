// Intentional error -- code before any CASE or DEFAULT label.

PROC TEST(INT A)
	SWITCH A
		CASE 0
			EXIT
		CASE 1
		CASE 2
			IF A > 1
				EXIT
			ELSE
				EXIT
			ENDIF
			// Unfortunately our checking isn't perfect...
			// This should flag a warning but doesn't becaue
			// the last statement we saw left the block even
			// though it was within an IF statement
			BREAK
		DEFAULT
			EXIT
	ENDSWITCH
ENDPROC

SCRIPT
	INT FOO

	SWITCH FOO
			++FOO
			BREAK
		CASE 0
		CASE 1
			--FOO
			TEST(FOO)
	ENDSWITCH
ENDSCRIPT
