// This test case is for a bug where null handles passed to commands
// did not work properly.

NATIVE Handle
NATIVE PROC NullTest(HANDLE h)

SCRIPT
	NullTest(null)
ENDSCRIPT
