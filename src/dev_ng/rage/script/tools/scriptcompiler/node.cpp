#include "node.h"

#include <stdarg.h>

#include "file/asset.h"
#include "file/stream.h"
#include "string/string.h"
#include "script/program.h"
#include "script/thread.h"
#include "system/param.h"
#include "string/stringhash.h"
#include "atl/queue.h"

using namespace rage;


scrNode* scrNode::Root;
scrValue* scrNode::Statics;
int scrNode::StaticSize;
scrValue* scrNode::Globals;
int scrNode::GlobalSize;
atFixedArray<u8,MAX_LEGIT_OPS> scrNode::Program;
atFixedArray<scrNode::LineInfo,MAX_LINEINFO> scrNode::sm_LineInfo;
atFixedArray<ConstString,MAX_USING_FILES> scrNode::sm_FileInfo;
static atMap<u32,int> s_FileInfoHash;
int scrNode::SourceIndent;
fiStream *c_stream;

char *scrNode::pStringHeap = NULL;
int scrNode::ms_AllocatedSizeOfStringHeap = 0;
int scrNode::StringHeapSize;

const int MAX_STRING_OFFSETS = 131072;
atFixedArray<int,MAX_STRING_OFFSETS> StringOffsets;

extern fiStream *scr_stream;
extern fiStream *scr_debuginfo;
fiStream *scr_globalreffile;

extern bool bAtLeastOneVariableInGlobalBlockIsReferenced[scrProgram::MAX_GLOBAL_BLOCKS];

extern fiStream *scr_global_block_usage_file;
fiStream *scr_structreffile;
fiStream *scr_stringLiteralsFile;
extern int scr_line;
extern char scr_stream_name[];
extern u32 scr_stream_name_hash;
extern bool scr_had_warnings;
extern scrTypeObject s_StructRef;
extern int scr_arg_struct_size;
extern char outname[];

XPARAM(dont_emit_preamble);
XPARAM(emitc_skip_global_variable_names);
XPARAM(emitc_skip_enum_names);


void scrNode::InitClass(int stringHeapSize)
{
	pStringHeap = rage_new char[stringHeapSize];
	ms_AllocatedSizeOfStringHeap = stringHeapSize;
}

void scrNode::ShutdownClass()
{
	delete [] pStringHeap;
	pStringHeap = NULL;

	ms_AllocatedSizeOfStringHeap = 0;
}


int scrNode::AddFile(const char *name,u32 hash)
{
	int *index = s_FileInfoHash.Access(hash);
	if (index)
		return *index;
	s_FileInfoHash[hash] = sm_FileInfo.GetCount();

	if (sm_FileInfo.IsFull())
	{
		Errorf("sm_FileInfo array is full. Current size is %d. You'll need a new script compiler with this number increased", sm_FileInfo.GetMaxCount());
	}
	Assertf(!sm_FileInfo.IsFull(), "sm_FileInfo array is full. Current size is %d. You'll need a new script compiler with this number increased", sm_FileInfo.GetMaxCount());

	sm_FileInfo.Append() = name;
	return sm_FileInfo.GetCount() - 1;
}


scrNode::scrNode(scrNodeType type) : TypeEnum(type), Line(scr_line), File(AddFile(scr_stream_name,scr_stream_name_hash)) { }

bool fatal = true;

extern void dump_using_stack();

void scrNode::node_errorf(const char *fmt,...) {
	va_list(args);
	va_start(args,fmt);
	char buf[256];
	vformatf(buf,sizeof(buf),fmt,args);
	errorf("%s(%d) : %s",GetFilename(),Line,buf);
	dump_using_stack();
	if (fatal) throw "semantic error in compile";
}

void scrNode::node_warningf(const char *fmt,...) {
	va_list(args);
	va_start(args,fmt);
	char buf[256];
	vformatf(buf,sizeof(buf),fmt,args);
	errorf("%s(%d) : %s",GetFilename(),Line,buf);
	dump_using_stack();
	scr_had_warnings = true;
}

int scrNode::LineInfo::cmp(const LineInfo *a,const LineInfo *b)
{
	if (a->m_BasePc != b->m_BasePc)
		return a->m_BasePc - b->m_BasePc;
	return a->m_LineNumber - b->m_LineNumber;
}

void scrNode::UpdateLineInfo() {
	if (sm_LineInfo.GetCount() && sm_LineInfo.Top().m_BasePc == Program.GetCount())
		return;

	if (sm_LineInfo.IsFull())
	{
		Errorf("sm_LineInfo array is full. Current size is %d. You'll need a new script compiler with this number increased", sm_LineInfo.GetMaxCount());
	}
	Assertf(!sm_LineInfo.IsFull(), "sm_LineInfo array is full. Current size is %d. You'll need a new script compiler with this number increased", sm_LineInfo.GetMaxCount());

	LineInfo &li = sm_LineInfo.Append();
	li.m_BasePc = Program.GetCount();
	li.m_LineNumber = Line;
	li.m_FileIndex = u16(File);
	// Displayf("line %d at offset %d",Line,Program.GetCount());
}


int scrNode::GetLineInfo(int offset) {
	// First look for exact match
	int line;
	int bestDist = 99999;
	int bestLine = 0;
	for (line=0; line<sm_LineInfo.GetCount(); line++) {
		if (sm_LineInfo[line].m_BasePc <= offset) {
			int thisDist = offset - sm_LineInfo[line].m_BasePc;
			if (thisDist <= bestDist) {
				bestDist = thisDist;
				bestLine = sm_LineInfo[line].m_LineNumber;
			}
		}
	}

	return bestLine;
}


void scrNode::Emit8(u8 b) { 

	if (Program.IsFull())
	{
		Errorf("Program array is full. Current size is %d. You'll need a new script compiler with this number increased", Program.GetMaxCount());
	}
	Assertf(!Program.IsFull(), "Program array is full. Current size is %d. You'll need a new script compiler with this number increased", Program.GetMaxCount());

	Program.Append() = b;
}

static u8 pOp;
static u8 *pPos;

int InstructionsEmitted;
int InstructionsSaved;
int PaddingEmitted;

PARAM(nopeephole,"Turn off peephole optimizer");
PARAM(final,"Final build, eliminate DEBUGONLY symbols and function names.");
PARAM(noprofile,"Disable profiling strings in subprograms (implied by -final)");
PARAM(strictreferencetypechecking,"Enable stricter type checking on references");
PARAM(warnbigprog,"Warn if any subprogram is larger than NNN kilobytes");

#define REPLACE_PREV(value)	do { *pPos = pOp = (value); InstructionsSaved++; return; } while (0)

void scrNode::EmitOp(scrOpcode op,int sizeOverride) {
	#define OP(a,b,c) b
	static u8 bytesPerOpcode[] = {
	#include "script/opcodes.h"
	};
	#undef OP

	Assert(sizeOverride < scrPageMask);

	int curSize = Program.GetCount();

	if (!PARAM_nopeephole.Get()) {
		// Several opcodes have _LOAD and _STORE variants which do the load and store immediately without hitting TOS.
		if ((pOp == OP_LOCAL_U8 || pOp == OP_LOCAL_U16 || 
			pOp == OP_STATIC_U8 || pOp == OP_STATIC_U16 || pOp == OP_STATIC_U24 || 
			pOp == OP_GLOBAL_U16 || pOp == OP_GLOBAL_U24 || 
				pOp == OP_ARRAY_U8 || pOp == OP_ARRAY_U16 || pOp == OP_IOFFSET_U8 || pOp == OP_IOFFSET_S16)) {
			if (op == OP_LOAD)
				REPLACE_PREV(pOp + 1);
			else if (op == OP_STORE)
				REPLACE_PREV(pOp + 2);
		}
		// Detect immediate adds and multiplies
		if (pOp == OP_PUSH_CONST_U8 && op == OP_IADD)
			REPLACE_PREV(OP_IADD_U8);
		if (pOp == OP_PUSH_CONST_U8 && op == OP_IOFFSET)
			REPLACE_PREV(OP_IOFFSET_U8);
		if (pOp == OP_PUSH_CONST_S16 && op == OP_IADD)
			REPLACE_PREV(OP_IADD_S16);
		if (pOp == OP_PUSH_CONST_S16 && op == OP_IOFFSET)
			REPLACE_PREV(OP_IOFFSET_S16);
		// Expand a one-byte immediate into an IADD_U8; same total size but fewer insns.  Make sure it won't cross or join up against a page boundary.
		if (pOp >= OP_PUSH_CONST_0 && pOp <= OP_PUSH_CONST_7 && op == OP_IADD && (curSize & scrPageMask) < scrPageMask-1) {
			Emit8(u8(pOp - OP_PUSH_CONST_0));
			REPLACE_PREV(OP_IADD_U8);
		}
		if (pOp >= OP_PUSH_CONST_0 && pOp <= OP_PUSH_CONST_7 && op == OP_IOFFSET && (curSize & scrPageMask) < scrPageMask-1) {
			Emit8(u8(pOp - OP_PUSH_CONST_0));
			REPLACE_PREV(OP_IOFFSET_U8);
		}
		if (pOp == OP_PUSH_CONST_U8 && op == OP_IMUL)
			REPLACE_PREV(OP_IMUL_U8);
		if (pOp == OP_PUSH_CONST_S16 && op == OP_IMUL)
			REPLACE_PREV(OP_IMUL_S16);
		// Expand a one-byte immediate into an IMUL_U8; same total size but fewer insns.  Make sure it won't cross or join up against a page boundary.
		if (pOp >= OP_PUSH_CONST_0 && pOp <= OP_PUSH_CONST_7 && op == OP_IMUL && (curSize & scrPageMask) < scrPageMask-1) {
			Emit8(u8(pOp - OP_PUSH_CONST_0));
			REPLACE_PREV(OP_IMUL_U8);
		}
		// Detect multiple constant pushes in a row
		// Make sure that the resulting longer opcode won't cross (or join up against) a page boundary.
		if (pOp == OP_PUSH_CONST_U8 && op == OP_PUSH_CONST_U8 && (curSize & scrPageMask) < scrPageMask-1) // Following Emit8 will put down correct payload byte
			REPLACE_PREV(OP_PUSH_CONST_U8_U8);
		if (pOp == OP_PUSH_CONST_U8_U8 && op == OP_PUSH_CONST_U8 && (curSize & scrPageMask) < scrPageMask-1)
			REPLACE_PREV(OP_PUSH_CONST_U8_U8_U8);
		// Compare-and-jump (again, make sure the payload won't cross a page boundary)
		if (pOp >= OP_IEQ && pOp <= OP_ILE && op == OP_JZ && (curSize & scrPageMask) < scrPageMask - 2)
			REPLACE_PREV(pOp + (OP_IEQ_JZ - OP_IEQ));
	}

	// If we didn't replace a previous insn, check to make sure this insn will fit.
	if (!sizeOverride)
		sizeOverride = bytesPerOpcode[op];
	Assert(sizeOverride);

	// Unconditional flow control can be an exact fit; anything else needs
	// needs at least one byte after it so that we can insert NOP's or
	// a forward jump.
	// We can only cross page boundaries with jump instructions or OP_NOP.
	if (!INSN_THAT_CAN_BE_AT_END_OF_PAGE(op))
		++sizeOverride;

	// See if we have room.
	u32 target = ((curSize + sizeOverride - 1) & ~scrPageMask);
	if ((curSize & ~scrPageMask) != target) {
		int remain =  target - curSize;
		Assert(remain > 0 && remain < scrPageMask);
		PaddingEmitted += remain;
		// Three or more bytes, do a forward jump.
		if (remain >= 3) {
			EmitOp(OP_J);
			EmitS16(remain - 3);
			remain -= 3;
		}
		// Fill everything else with NOP's so the disassembler doesn't choke
		// every though we won't actually execute most of them.
		while (remain--)
			Emit8((u8)OP_NOP);
	}

	Emit8((u8)op);
	InstructionsEmitted++;

	pOp = (u8) op;
	pPos = &Program.Top();
}

typedef atArray<char,0,unsigned> Stream;
atArray<Stream> streams;

void scrNode::EmitSource(const char *fmt,...) {
	va_list args;
	va_start(args,fmt);
	if (fmt[0] == '\t') {		// expand a leading tab to current indent level.
		int i = SourceIndent;

		if (streams.GetCount()) {
			while (i-- > 1)
				streams.Top().Grow(4096) = '\t';
		}
		else if (c_stream) {
			while (i-- > 1)
				fputc('\t',c_stream);
		}
	}

	char buffer[512], *bp = buffer;
	vformatf(buffer,sizeof(buffer),fmt,args);
	if (streams.GetCount()) {
		while (*bp)
			streams.Top().Grow(4096) = *bp++;
	}
	else if (c_stream) {
		while (*bp) {
			if (*bp == '\n')
				fputc('\r',c_stream);
			fputc(*bp++,c_stream);
		}
	}
	va_end(args);
}

atRangeArray< Stream, SCRIPT_CHANNEL_COUNT > other_streams;

XPARAM(emitc);

void scrNode::EmitChannel(int channel,const char *fmt,...) {

	if (!PARAM_emitc.Get())
		return;

	va_list args;
	va_start(args,fmt);

	char buffer[512], *bp = buffer;
	vformatf(buffer,sizeof(buffer),fmt,args);

	while (*bp)
		other_streams[channel].Grow(4096) = *bp++;
	va_end(args);
}


void scrNode::EmitInt(int v) {
	if (v >= 0 && v < 256) {
		if (v >= 0 && v <= 7)
			EmitOp((scrOpcode)(OP_PUSH_CONST_0 + v));
		else {
			EmitOp(OP_PUSH_CONST_U8);
			Emit8((u8)v);
		}
	}
	else if (v >= -32768 && v <= 32767) {
		if (v == -1)
			EmitOp(OP_PUSH_CONST_M1);
		else {
			EmitOp(OP_PUSH_CONST_S16);
			Emit16((u16)v);
		}
	}
	else if (v >= 0 && v < 256 * 65536) {
		EmitOp(OP_PUSH_CONST_U24);
		Emit24(v);
	}
	else {
		EmitOp(OP_PUSH_CONST_U32);
		Emit32(v);
	}
}


void scrNode::EmitFloat(float v) {
	if (v == -1.0f || v == 0.0f || v == 1.0f || v == 2.0f || v == 3.0f || v == 4.0f || v == 5.0f || v == 6.0f || v == 7.0f) {
		EmitOp((scrOpcode)(OP_PUSH_CONST_F0 + int(v)));
	}
	else {
		scrValue value;
		value.Float = v;
		EmitOp(OP_PUSH_CONST_F);
		Emit32(value.Int);
	}
}


//////////////////////////////////////////////////////////////////////////
///////////////////////   E X P R E S S I O N S   ////////////////////////
//////////////////////////////////////////////////////////////////////////

scrNodeExpr::scrNodeExpr(scrNodeType nodeType,const scrType *type) : scrNode(nodeType), Type(type) { }


scrNodeExpr *scrNode::MakeFloat(scrNodeExpr *e) {
	if (e->GetType() == &scrTypeFloat::Instance)
		return e;
	else if (e->GetType() == &scrTypeInt::Instance) {
		int dummy;
		if (e->IsConstant(dummy) /*&& !dummy*/)
			return rage_new scrNodeFloat((float)dummy);
		else
			return rage_new scrNodeUnary(OP_I2F, e, &scrTypeFloat::Instance, "op_i2f");
	}
	else {
		node_errorf("Vector element must be int or float");
		return NULL;
	}
}

void scrNode::LoadAddress(scrNodeExpr *address) {
	if (address->GetType() == &scrTypeNull::Instance) {
		EmitInt(0);
		EmitSource("NULL");
	}
	else {
		address->Emit();
		EmitOp(OP_LOAD);
	}
}

void scrNode::StoreAddress(scrNodeExpr *address) {
	Assert(address->GetType() != &scrTypeNull::Instance);
	address->Emit();
	EmitOp(OP_STORE);
}

scrNodeTuple::scrNodeTuple(scrNodeExpr *x,scrNodeExpr *y,scrNodeExpr *z) : scrNodeExpr(NODE_TUPLE,&scrTypeVector::Instance), X(MakeFloat(x)), Y(MakeFloat(y)), Z(MakeFloat(z)) { }


void scrNodeTuple::Emit() {
	BeginCall("scrVector");
	X->Emit();
	Comma();
	Y->Emit();
	Comma();
	Z->Emit();
	EndCall();
}


scrNodeField::scrNodeField(scrNodeExpr *address,const scrField *field,const scrTypeStruct *parent) : scrNodeExpr(NODE_FIELD,&scrTypeInt::Instance), Address(address), Field(field), Parent(parent) { }


void scrNodeField::Emit() {
	// // scrNodeBinary($1->Address,OP_IADD,rage_new scrNodeInt($4->Offset * sizeof(scrValue)),&scrTypeInt::Instance,"op_iadd");
	Address->Emit();
	EmitSource(".%s/*%d*/",Field->Name,Field->Offset);
	if (Field->Offset) {	// Optimization: Don't bother adding zero.
		EmitInt(Field->Offset);
		EmitOp(OP_IOFFSET);
	}
	if (scr_structreffile && Parent) {
		char sname[128];
		Parent->GetName(sname);
		fprintf(scr_structreffile,"REF,%s,%s\r\n",sname,Field->Name);
	}
}

scrNodeCountOf::scrNodeCountOf(scrNodeExpr *address) : scrNodeExpr(NODE_COUNTOF,&scrTypeInt::Instance), Address(address) { }


void scrNodeCountOf::Emit() {
	// rage_new scrNodeBinary($1->Address,OP_IADD,rage_new scrNodeInt(sizeof(scrValue)),&scrTypeInt::Instance,"+"));
	Address->Emit();
	EmitSource("[0]");
	EmitInt(1);
	EmitOp(OP_IOFFSET);
}

scrNodeBinary::scrNodeBinary(scrNodeExpr *left,scrOpcode op,scrNodeExpr *right,const scrType *type,const char *cop) : scrNodeExpr(NODE_BINARY,type), Op(op), Left(left), Right(right), OpName(cop) { }


void scrNodeBinary::Emit() {
	UpdateLineInfo();
	int value;
	if (IsConstant(value)) {
		EmitSource("%d",value);
		EmitInt(value);
	}
	else {
		if (isalpha(OpName[0])) {
			BeginCall(OpName);
		Left->Emit();
			Comma();
		Right->Emit();
			EndCall();
		}
		else {
			EmitSource("(");
			Left->Emit();
			EmitSource(OpName);
			Right->Emit();
			EmitSource(")");
		}
		EmitOp(Op);
	}
}


bool scrNodeBinary::IsConstant(int &value) {
	int l, r;
	if (Left->IsConstant(l) && Right->IsConstant(r)) {
		switch (Op) {
		case OP_IADD: value = l + r; break;
		case OP_ISUB: value = l - r; break;
		case OP_IMUL: value = l * r; break;
		case OP_IDIV: value = r? l / r : 0; break;
		case OP_IMOD: value = r? l % r : 0; break;
		case OP_INOT: value = !value; break;
		case OP_INEG: value = -value; break;
		case OP_IAND: value = l & r; break;
		case OP_IOR: value = l | r; break;
		case OP_IXOR: value = l ^ r; break;

		case OP_IEQ: value = l == r; break;
		case OP_INE: value = l != r; break;
		case OP_IGT: value = l > r; break;
		case OP_IGE: value = l >= r; break;
		case OP_ILT: value = l < r; break;
		case OP_ILE: value = l <= r; break;

		default: return false;
		}
		return true;
	}
	else
		return false;
}


// If both are float or both are int, does nothing.  Otherwise promotes int to float.
bool scrNodeBinary::Promote(bool isArithmetic) {
	if (Left->GetType()->IsInteger() && Right->GetType()->IsInteger()) {
		if (Left->GetType() != Right->GetType())
			node_warningf("Enumerants must be of same type.");
		return false;
	}
	else if (Left->GetType() == &scrTypeInt::Instance && Right->GetType() == &scrTypeFloat::Instance) {
		int dummy;
		if (Left->IsConstant(dummy))
			Left = rage_new scrNodeFloat((float)dummy);
		else
			Left = rage_new scrNodeUnary(OP_I2F, Left, &scrTypeFloat::Instance, "op_i2f");
		return true;
	}
	else if (Left->GetType() == &scrTypeFloat::Instance && Right->GetType() == &scrTypeInt::Instance) {
		int dummy;
		if (Right->IsConstant(dummy))
			Right = rage_new scrNodeFloat((float)dummy);
		else
			Right = rage_new scrNodeUnary(OP_I2F, Right, &scrTypeFloat::Instance, "op_i2f");
		return true;
	}
	else if (Left->GetType() == &scrTypeFloat::Instance && Right->GetType() == &scrTypeFloat::Instance)
		return false;
	else if (Left->GetType() == &scrTypeBool::Instance && Right->GetType() == &scrTypeBool::Instance)
		return false;
	else if (Left->GetType() == &scrTypeVector::Instance && Right->GetType() == &scrTypeVector::Instance && isArithmetic)
		return false;
	else if (Left->GetType() == &scrTypeVector::Instance && Right->GetType() == &scrTypeFloat::Instance && isArithmetic) {
		Right = rage_new scrNodeUnary(OP_F2V, Right, &scrTypeVector::Instance, "op_f2v");
		return true;
	}
	else if (Left->GetType() == &scrTypeFloat::Instance && Right->GetType() == &scrTypeVector::Instance && isArithmetic) {
		Left = rage_new scrNodeUnary(OP_F2V, Left, &scrTypeVector::Instance, "op_f2v");
		return true;
	}
	else if (!isArithmetic && Left->GetType()->IsEnum() && Right->GetType()->IsEnum() && Left->GetType() == Right->GetType())
		return false;
	else {
		node_warningf(isArithmetic?
			"Binary arithmetic operator expects either integer, floating-point, or vector operands" :
			"Binary comparison operator expects either integer or floating-point operands"
			);
		return true;
	}
}


scrNodeFunctionCall::scrNodeFunctionCall(const scrCommandInfo *info,scrExprList *parameters,scrNodeExpr *addr) : scrNodeExpr(NODE_FUNCTION_CALL,info->ReturnType), Info(info), Parameters(parameters), Addr(addr) { }

void scrNode::CheckTypeDef(const scrCommandInfo *formal,const scrCommandInfo *actual) {
	if (formal->ReturnType && !actual->ReturnType)
		node_warningf("Subprogram signature mismatch: Passing PROC address where FUNC address was expected");
	else if (!formal->ReturnType && actual->ReturnType)
		node_warningf("Subprogram signature mismatch: Passing FUNC address where PROC address was expected");
	else if (formal->ReturnType && !formal->ReturnType->IsCompatibleWith(actual->ReturnType))
		node_warningf("Subprogram signature mismatch: Return types of the functions are not compatible.");
	// TODO: Default parameters won't work yet.
	else if (formal->Parameters && !actual->Parameters)
		node_warningf("Subprogram signature mismatch: Expected parameters, didn't get any.");
	else if (!formal->Parameters && actual->Parameters)
		node_warningf("Subprogram signature mismatch: Didn't expect any parameters, got some.");
	else if (formal->Parameters && actual->Parameters) {
		if (formal->Parameters->Count != actual->Parameters->Count)
			node_warningf("Subprogram signature mismatch: Expected %d parameters but got %d",formal->Parameters->Count,actual->Parameters->Count);
		else {
			const scrTypeList *f = formal->Parameters;
			const scrTypeList *a = actual->Parameters;
			int count = 1;
			while (f && a) {
				if (f->Left->GetReference() && a->Left->GetReference() && f->Left->GetReference()->IsCompatibleWith(a->Left->GetReference()))
					/* okay */;
				else if (!f->Left->IsCompatibleWith(a->Left))
					node_warningf("Subprogram signature mismatch: Parameter #%d is incompatible.",count);
				f = f->Right;
				a = a->Right;
				++count;
			}
		}
	}
}


int scrNode::CollectParameters(const scrTypeList *formal,scrExprList *actual) {
	if (formal == &g_VARARGS || formal == &g_VARARGS1 || formal == &g_VARARGS2 || formal == &g_VARARGS3) {
		int count = 0;
		int mini = formal==&g_VARARGS3?3 : formal==&g_VARARGS2?2 : formal==&g_VARARGS1?1 : 0;
		unsigned types = 0;
		bool bad = false;
		for (scrExprList *j = actual; j; j=j->Right) {
			const scrType *i = j->Left->GetType();
			unsigned thisType = 0;
			if (i->IsString() || i->IsTextLabel())
				thisType = scrValue::VA_STRINGPTR;
			else if (i->IsInteger() || i->GetInnerType() == &scrTypeBool::Instance)
				thisType = scrValue::VA_INT;
			else if (i->GetInnerType() == &scrTypeFloat::Instance)
				thisType = scrValue::VA_FLOAT;
			else if (i->GetInnerType() == &scrTypeVector::Instance)
				thisType = scrValue::VA_VECTOR;
			else {
				node_warningf("COMMAND with VARARGS only accepts integer, float, or vector types");
				bad = true;
			}
			types >>= 2;
			types |= (thisType << 30);
			++count;
		}
		if (count > 16) {
			node_warningf("COMMAND with VARARGS cannot accept more than 16 parameters");
			bad = true;
		}
		if (count < mini) {
			node_warningf("COMMAND with VARARGS expected at least %d parameter but only got %d",mini,count);
			bad = true;
		}
		// Emit a control word containing the number of supplied parameters and their basic types.
		types >>= (16 - count) * 2;
		EmitInt(types);
		EmitSource("%u,0x%08x",count,types);
		if (!bad) {
			while (actual) {
				Comma();
				if (actual->Left->GetType()->IsTextLabel() || actual->Left->GetType()->GetInnerType() == &scrTypeVector::Instance) {
					EmitSource("&");		// pass these by reference in C++, only portable way to do it via ...
					actual->Left->EmitRef();
				}
				else
					actual->Left->Emit();
				actual = actual->Right;
			}
		}
		// Displayf("%d parameters collected, types = %x(%x), extraSize = %d",count,types,count | (types >> ((14 - count)*2)),extraSize);
		return count;
	}

	int minFormal = 0;
	scrDefaultValue dummy;
	const scrTypeList *i = formal;
	while (i && !i->Left->HasDefaultValue(dummy)) {
		++minFormal;
		i = i->Right;
	}
	if (formal && !actual && minFormal)
		node_errorf("COMMAND expects %d parameters but none supplied",formal->Count);
	else if (!formal && actual)
		node_errorf("COMMAND expects no parameters but is given %d",actual->Count);
	else if (!formal && !actual)
		return 0;
	int actualCount = actual? actual->Count : 0;
	if (actualCount < minFormal || actualCount > formal->Count) {
		if (minFormal != formal->Count)
			node_warningf("COMMAND expects between %d and %d parameters but is given %d",minFormal,formal->Count,actualCount);
		else
			node_warningf("COMMAND expects %d parameters but is given %d",formal->Count,actualCount);
	}
	else {
		int pindex = 1;
		while (formal && actual) {
			const scrType *f = formal->Left;
			const scrType *a = actual->Left->GetType();
			if (!f->GetReference())
				while (f->GetInnerType() != a && a->IsObject() && a->GetParent())
					a = a->GetParent();
			if (actual->Left->IsDefaultParam()) {
				if (formal->Left->HasDefaultValue(dummy)) {
					EmitInt(dummy.Value.asInt);		// underlying VM is untyped
					if (dummy.IsFloat)				// ...but C++ is typed!
						EmitSource("%f",dummy.Value.asFloat);
					else
						EmitSource("%d",dummy.Value.asInt);
				}
				else
					node_warningf("DEFAULT specified as parameter #%d when a parameter has no default value",pindex);
			}
			else if (f->GetInnerType() != a) {
				if (f->GetInnerType() == &scrTypeInt::Instance && a == &scrTypeFloat::Instance)
					node_warningf("Cannot pass float to int parameter #%d, use FLOOR, ROUND, or CEIL functions.",pindex); 
				else if (f->GetInnerType() == &scrTypeFloat::Instance && a == &scrTypeInt::Instance) {
					int temp;
					if (!actual->Left->IsConstant(temp))
						node_warningf("Cannot pass int to float parameter #%d, use TO_FLOAT function.",pindex);
					EmitFloat((float)temp);
					EmitSource("%g",(float)temp);
					// actual->Left->Emit();
					// EmitOp(OP_I2F);
				}
				else if (f->GetBasicType() == scrValue::STRING && a->IsTextLabel()) {
					actual->Left->EmitRef();
				}
				else if (f->GetReference() && f->IsCompatibleWith(&s_StructRef)) {
					EmitSource("reinterpret_cast<scr_any_struct*>(&");
					actual->Left->EmitRef();
					EmitSource(")");
				}
				else if (f->GetReference() && f->IsCompatibleWith(a)) {
					if (PARAM_strictreferencetypechecking.Get() && f->GetReference()->IsObject() && a != f->GetReference())
						node_warningf("References to objects must match exact type.");
					else if (!actual->Left->EmitRef())
						node_warningf("Unable to convert to reference in parameter #%d",pindex);
				}
				else if (f == &scrTypeInt::EnumToIntInstance && a->IsInteger()) {
					if (a == &scrTypeInt::Instance)
						node_warningf("ENUM_TO_INT parameter matches any ENUM, but not an INT");
					actual->Left->Emit();
				}
				else if (f->GetReference() == &scrTypeInt::EnumToIntInstance && a->IsInteger()) {
					actual->Left->EmitRef();
				}
				else if (f->IsTypeDef() && a->IsTypeDef()) {
					scrTypeDef *formal_typedef = (scrTypeDef*) f;
					scrTypeDef *actual_typedef = (scrTypeDef*) a;
					CheckTypeDef(formal_typedef->Info,actual_typedef->Info);
					actual->Left->Emit();
				}
				else if (f == &scrTypeStringHash::Instance && actual->Left->IsStringConstant()) {
					int code = atStringHash(actual->Left->IsStringConstant());
					EmitInt(code);
					EmitSource("%d",code);
				}
				else if (f == &scrTypeStringHash::Instance && (a->IsString() || a->IsTextLabel())) {
					EmitSource("atStringHash(");
					actual->Left->EmitRef();
					EmitSource(")");
					EmitOp(OP_STRINGHASH);
				}
				else if (!(f->IsObject() && a == &scrTypeNull::Instance))
					node_warningf("Type mismatch on parameter #%d",pindex);
				else if (a == &scrTypeNull::Instance) {
					EmitSource("0");
					EmitInt(0);
				}
				else if (f->IsCompatibleWith(a)) {
					actual->Left->Emit();
				}
				else {
					Quitf("CollectParameters - Bug in compiler!");
				}
			}
			else
				actual->Left->Emit();
			formal = formal->Right;
			actual = actual->Right;
			if (formal || actual)
				Comma();
			++pindex;
		}
		if (formal) {
			// Displayf("Info: %s(%d) : Supplying default parameter(s).",GetFilename(),Line);
			while (formal) {
				formal->Left->HasDefaultValue(dummy);
				EmitInt(dummy.Value.asInt);		// underlying VM is untyped
				if (dummy.IsFloat)				// ...but C++ is typed!
					EmitSource("%f",dummy.Value.asFloat);
				else
					EmitSource("%d",dummy.Value.asInt);
				formal = formal->Right;
				if (formal)
					Comma();
			}
		}
	}
	return 0;
}


bool scrNodeFunctionCall::IsConstant(int &value) {
	if (PARAM_final.Get() && Info->ProgName && Info->DebugOnly && Info->ReturnType == &scrTypeBool::Instance) {
		value = 0;
		return true;
	}
	else
		return false;
}

static int nextSlot;


void scrNodeFunctionCall::Emit() {
	// FUNC's starting with DEBUG_ get a warning, since we can't eliminate the call if the return value is used.
	if (PARAM_final.Get() && Info->ProgName && Info->DebugOnly)
		node_warningf("Final mode enabled, but return value of a DEBUG_ function is being used so we can't eliminate the call.  Use #ifndef FINAL to fix this code.");

	EmitSource("%s(%s",Info->ProgName?Info->ProgName:"op_indirectcall(",Info->NativeHash?"":Info->Parameters?"statics,":"statics");

	int extraSize = CollectParameters(Info->Parameters,Parameters);
	if (Info->NativeHash) {
		EmitNativeStub(Info,extraSize);
	}
	else {
		Assert(extraSize == 0);
		if (Addr) {
			EmitSource("),");
			LoadAddress(Addr);
			EmitOp(OP_CALLINDIRECT);
		}
		else
			EmitCall(Info,false);
	}
	EndCall();
}


atFixedArray<const scrCommandInfo*, 1024> s_AccumulatedCalls;

void scrNode::EmitCall(const scrCommandInfo *info,bool indirect) {
	if (info->Address) {
		if (indirect)
			EmitInt(info->Address);
		else {
			Assert((info->Address >> 24) == 0);
			EmitOp(OP_CALL);
			Emit24(info->Address);
		}
	}
	else {
		if (!info->FirstRef)
			s_AccumulatedCalls.Push(info);
		scrCommandReference *r = rage_new scrCommandReference;
		r->Next = info->FirstRef;
		info->FirstRef = r;
		if (indirect) {
			EmitOp(OP_PUSH_CONST_U24);
			r->Addr = GetPC();
			Emit24(0);
		}
		else {
			EmitOp(OP_CALL);
			r->Addr = GetPC();
			Emit24(0);
		}
	}
}

void scrNode::EmitAccumulatedCalls() {
	while (s_AccumulatedCalls.GetCount()) {
		const scrCommandInfo *cur = s_AccumulatedCalls.Pop();
		Assert(!cur->Address);
		scrCommandReference *r = cur->FirstRef;
		while (r) {
			u32 ref = r->Addr;
			Program[ref] = (u8) (GetPC() >> 16);
			Program[ref+1] = (u8) (GetPC() >> 8);
			Program[ref+2] = (u8) GetPC();
			r = r->Next;
		}
		cur->Address = Program.GetCount();
		cur->Tree->Emit();
	}
}


bool scrNodeFunctionCall::EmitRef() {
	node_warningf("Invalid implicit string conversion or VECTOR passed by value to VARARGS in function call");
	return false;
}

scrNodeFunctionAddr::scrNodeFunctionAddr(const scrCommandInfo *func) : scrNodeConstant(NODE_FUNCTION_ADDR,rage_new scrTypeDef(func,"UNKNOWN")), Info(func), Value(0) { }

void scrNodeFunctionAddr::Emit() {
	UpdateLineInfo();

	EmitCall(Info,true);
	Value = Info->Address;
	// EmitSource("&script::");
	EmitSource(Info->ProgName);
}


bool scrNodeFunctionAddr::IsConstant(int &value) {
	if (Value) {	// Zero is not a valid function address
		value = Value;
		return true;
	}
	else
		return false;
}


scrNodeArithmetic::scrNodeArithmetic(scrNodeExpr *left,scrOpcode opi,scrOpcode opf,scrOpcode opv,scrNodeExpr *right,const char *copi,const char *copf,const char *copv) : scrNodeBinary(left,opi,right,&scrTypeInt::Instance,copi) {
	Promote(true);
	Type = Left->GetType();
	if (Type == &scrTypeFloat::Instance) {
		if (!opf)
			node_warningf("Cannot apply this arithmetic operation to a FLOAT");
		Op = opf;
		OpName = copf;
	}
	else if (Type == &scrTypeVector::Instance) {
		if (!opv)
			node_warningf("Cannot apply this arithmetic operation to a VECTOR");
		Op = opv;
		OpName = copv;
	}
}


scrNodeLogical::scrNodeLogical(scrNodeExpr *left,scrOpcode op,scrNodeExpr *right,const char *cop) : scrNodeBinary(left,op,right,&scrTypeBool::Instance,cop) {
	if (Left->GetType() != &scrTypeBool::Instance || Right->GetType() != &scrTypeBool::Instance) {
		node_warningf("Logical operator expects boolean operands");
	}
}


scrNodeShortCircuit::scrNodeShortCircuit(scrNodeExpr *left,scrOpcode op,scrNodeExpr *right) : scrNodeExpr(NODE_BOOL,&scrTypeBool::Instance), Left(left), Op(op), Right(right) {
	if (Left->GetType() != &scrTypeBool::Instance || Right->GetType() != &scrTypeBool::Instance)
		node_warningf("Logical operator expects boolean operands");
}

bool scrNodeShortCircuit::IsConstant(int &value) {
	int v1, v2;
	if (Left->IsConstant(v1) && Right->IsConstant(v2)) {
		value = (Op==OP_IAND)? (v1 && v2) : (v1 || v2);
		return true;
	}
	else
		return false;
}

void scrNodeShortCircuit::Emit() {
	// A AND B produces a 0 or 1 on stack appropriately.
	// A ANDALSO B should produce 0 on stack if A is 0 (and not evaluate B at all), otherwise it should leave B on stack.
	// A OR B produces a 0 or 1 on stack appropriately.
	// A ORELSE B should produce 1 on stack if A is 1 (and not evaluate B), otherwise it should leave B on stack.
	Left->Emit();

	EmitSource(Op==OP_IOR?"||":"&&");

	EmitOp(OP_DUP);		// Duplicate result of test since OP_JZ will consume it
	if (Op==OP_IOR)		// Negate the test for the "other" operator
		EmitOp(OP_INOT);
	int offset = scrNodeStmt::EmitForwardJump(OP_JZ);	// skip righthand result if we already know answer (and answer is on top of stack)
	Right->Emit();
	EmitOp(Op);		// This is somewhat redundant but defeats a bad decision in the peephole optimizer.
	scrNodeStmt::PlaceJump(offset);
}


scrNodeConditional::scrNodeConditional(scrNodeExpr *cond,scrNodeExpr *ifTrue,scrNodeExpr *ifFalse) : scrNodeExpr(NODE_BOOL,ifTrue->GetType()), Cond(cond), IfTrue(ifTrue), IfFalse(ifFalse) {
	// Promote int-to-float if necessary.  Intentionally skip all others.
	if (IfTrue->GetType() == &scrTypeInt::Instance && IfFalse->GetType() == &scrTypeFloat::Instance) {
		int dummy;
		if (IfTrue->IsConstant(dummy))
			IfTrue = rage_new scrNodeFloat((float)dummy);
		else
			IfTrue = rage_new scrNodeUnary(OP_I2F, IfTrue, &scrTypeFloat::Instance, "op_i2f");
	}
	else if (IfTrue->GetType() == &scrTypeFloat::Instance && IfFalse->GetType() == &scrTypeInt::Instance) {
		int dummy;
		if (IfFalse->IsConstant(dummy))
			IfFalse = rage_new scrNodeFloat((float)dummy);
		else
			IfFalse = rage_new scrNodeUnary(OP_I2F, IfFalse, &scrTypeFloat::Instance, "op_i2f");
	}

	if (Cond->GetType() != &scrTypeBool::Instance)
		node_warningf("Conditional operator expects boolean operand for first parameter");
	else if (IfTrue->GetType() != IfFalse->GetType())
		node_warningf("Conditional operator expects true and false parameters to have exact same type");
	// Update type in case it changed due to promotion
	Type = IfTrue->GetType();
}

bool scrNodeConditional::IsConstant(int &value) {
	int c;
	if (Cond->IsConstant(c))
		return c? IfTrue->IsConstant(value) : IfFalse->IsConstant(value);
	else
		return false;
}

void scrNodeConditional::Emit() {
	Cond->Emit();
	EmitSource("?");
	int ifFalse = scrNodeStmt::EmitForwardJump(OP_JZ);
	IfTrue->Emit();
	EmitSource(":");
	int whenTrue = scrNodeStmt::EmitForwardJump(OP_J);
	scrNodeStmt::PlaceJump(ifFalse);
	IfFalse->Emit();
	scrNodeStmt::PlaceJump(whenTrue);
}


scrNodeRelational::scrNodeRelational(scrNodeExpr *left,scrOpcode opi,scrOpcode opf,scrNodeExpr *right,const char *copi,const char *copf) : scrNodeBinary(left,opi,right,&scrTypeBool::Instance,copi) {
	Promote(false);
	if (Left->GetType() ==  &scrTypeFloat::Instance) {
		Op = opf;
		OpName = copf;
}
}


static bool IsPoolIndex(scrNodeExpr *expr)
{
	int v;
	return (expr->GetType() == &scrTypePoolIndex::Instance) || (expr->GetType() == &scrTypeInt::Instance && expr->IsConstant(v) && v == -1);
}

static bool IsStringHash(scrNodeExpr *expr)
{
	return (expr->GetType() == &scrTypeStringHash::Instance) || expr->IsStringConstant();
}


scrNodeEquality::scrNodeEquality(scrNodeExpr *left,scrOpcode opi,scrOpcode opf,scrNodeExpr *right,const char *copi,const char *copf) : scrNodeBinary(left,opi,right,&scrTypeBool::Instance,copi) {
	if (left->GetType()->IsTypeDef() && right->GetType() == &scrTypeNull::Instance)
		;
	else if (left->GetType()->IsObject() != right->GetType()->IsObject())
		node_warningf("Mixed object and non-object equality test (try using NULL)");
	else if (left->GetType()->IsObject()) {
		if (left->GetType() != &scrTypeNull::Instance && right->GetType() != &scrTypeNull::Instance && left->GetType() != right->GetType())
			node_warningf("Comparison between incompatible object types.");
	}
	else if (IsPoolIndex(left) && IsPoolIndex(right))
		;
	else if (IsStringHash(left) && IsStringHash(right)) {
		// Replace either or both sides with hash codes
		if (left->IsStringConstant())
			Left = rage_new scrNodeInt(atStringHash(left->IsStringConstant()));
		if (right->IsStringConstant())
			Right = rage_new scrNodeInt(atStringHash(right->IsStringConstant()));
	}
	else {
		Promote(false);
		if (Left->GetType() ==  &scrTypeFloat::Instance) {
			Op = opf;
			OpName = copf;
	}
}
}


scrNodeUnary::scrNodeUnary(scrOpcode op,scrNodeExpr *unary,const scrType *type,const char *cop) : scrNodeExpr(NODE_UNARY,type), Op(op), Unary(unary), OpName(cop) { }


void scrNodeUnary::Emit() {
	UpdateLineInfo();
	int value;
	if (IsConstant(value)) {
		EmitSource("%d",value);
		EmitInt(value);
	}
	else if (Op == OP_FNEG && Unary->GetNodeType() == NODE_FLOAT) {
		// special case, avoid emitting a negated float in two steps.
		float fval = -((scrNodeFloat*)Unary)->GetValue();
		EmitSource("%g",fval);
		EmitFloat(fval);
	}
	else {
		if (Unary) {
			if (!isalpha(OpName[0])) {
				EmitSource("%s",OpName);
			Unary->Emit();
			}
			else {
				BeginCall(OpName);
				Unary->Emit();
				EndCall();
			}
		}
		else
			EmitSource("%s()",OpName);
		if (Op != OP_NOP)
			EmitOp(Op);
	}
}


bool scrNodeUnary::IsConstant(int &value) { 
	if (Unary && Unary->IsConstant(value)) {
		if (Op == OP_INOT)
			value = !value;
		else if (Op == OP_INEG)
			value = -value;
		else
			return false;
		return true;
	}
	else
		return false;
}


scrNodeLogicalNot::scrNodeLogicalNot(scrNodeExpr *expr) : scrNodeUnary(OP_INOT, expr, &scrTypeBool::Instance, "!") {
	if (Unary->GetType() != &scrTypeBool::Instance) {
		node_warningf("NOT operator requires boolean operand");
	}
}


scrNodeNegate::scrNodeNegate(scrNodeExpr *expr) : scrNodeUnary(OP_INEG, expr, &scrTypeInt::Instance, "-") {
	if (Unary->GetType() == &scrTypeInt::Instance)
		;
	else if (Unary->GetType() == &scrTypeFloat::Instance) {
		Op = OP_FNEG;
		OpName = "-";
		Type = &scrTypeFloat::Instance;
	}
	else if (Unary->GetType() == &scrTypeVector::Instance) {
		Op = OP_VNEG;
		OpName = "-";
		Type = &scrTypeVector::Instance;
	}
	else {
		node_warningf("NEGATE operator requires integer or floating point or vector operand");
	}
}


scrNodeVarRef::scrNodeVarRef(const scrType *type,scrNodeExpr *address) : scrNodeExpr(NODE_VARIABLE,type), Address(address) {
}


scrNodeDereference::scrNodeDereference(const scrType *type,scrNodeExpr *address) : scrNodeExpr(NODE_DEREFERENCE,type), Address(address) { }


void scrNodeDereference::Emit() {
	if (Address->GetType() == &scrTypeNull::Instance)
		EmitInt(0);
	else {
		Address->Emit();
		EmitOp(OP_LOAD);
	}
}
	

void scrNodeVarRef::Emit() {
	UpdateLineInfo();
	int size = Type->GetSize();
	if (size > 1) {
		EmitInt(size);
		Address->Emit();
		EmitOp(OP_LOAD_N);
	}
	else
		LoadAddress(Address);
}


bool scrNodeVarRef::EmitRef() {
	UpdateLineInfo();
	Address->Emit();
	return true;
}


scrNodeArrayLookup::scrNodeArrayLookup(scrNodeExpr *baseAddr,int elSize,scrNodeExpr *index) 
	: scrNodeExpr(NODE_ARRAY_LOOKUP,&scrTypeInt::Instance)
	, Base(baseAddr)
	, ElementSize(elSize)
	, Index(index) { 
}


void scrNodeArrayLookup::Emit() {
	BeginCall("op_array");
	Index->Emit();
	Comma();
	// EmitSource("%d",ElementSize);
	Base->Emit();
	EndCall();
	if (ElementSize < 256) {
		EmitOp(OP_ARRAY_U8);
		Emit8((u8)ElementSize);
	}
	else if (ElementSize < 65536) {
		EmitOp(OP_ARRAY_U16);
		Emit16((u16)ElementSize);
	}
	else
		node_errorf("Array element larger than 64k?");
}


scrNodeLocal::scrNodeLocal(const scrType *type,const char* name,int offset) : scrNodeExpr(NODE_LOCAL,type), Name(name), Offset(offset), ReadOnly(false) { }


void scrNodeLocal::Emit() {
	EmitSymbol(Name);
	if (Offset < 256) {
		EmitOp(OP_LOCAL_U8);
		Emit8((u8)Offset);
	}
	else if (Offset < 65536) {
		EmitOp(OP_LOCAL_U16);
		Emit16((u16)Offset);
	}
	else
		node_errorf("OP_LOCAL overflow");
}


scrNodeStatic::scrNodeStatic(const scrType *type,const char* name,int offset) : scrNodeExpr(NODE_STATIC,type), Name(name), Offset(offset) { }


void scrNodeStatic::Emit() {
	EmitSource("statics->%s/*%d*/",Name.c_str(),Offset);
	if (Offset < 256) {
		EmitOp(OP_STATIC_U8);
		Emit8((u8)Offset);
	}
	else if (Offset < 65536) {
		EmitOp(OP_STATIC_U16);
		Emit16((u16)Offset);
	}
	else if (Offset < 65536 * 256) {
		EmitOp(OP_STATIC_U24);
		Emit24(Offset);
	}
	else
		node_errorf("OP_STATIC overflow");
}


scrNodeGlobal::scrNodeGlobal(const scrType *type,const char* name,int offset) : scrNodeExpr(NODE_GLOBAL,type), Name(name), Offset(offset), WasReferenced(false) { }

void scrNodeGlobal::Emit() {
	if (PARAM_emitc_skip_global_variable_names.Get())
	{
		EmitSource("globals->/*%d*/",Offset);
	}
	else
	{
		EmitSource("globals->%s/*%d*/",Name.c_str(), Offset);
	}
	if (Offset < 65536) {
		EmitOp(OP_GLOBAL_U16);
		Emit16((u16)Offset);
	}
	else if (Offset < 65536 * 256) {
		EmitOp(OP_GLOBAL_U24);
		Emit24(Offset);
	}
	else
		node_errorf("OP_GLOBAL overflow");
	if (!WasReferenced) {
		if (scr_globalreffile)
		{
			fprintf(scr_globalreffile,"REF,%s,%s\r\n",Name.c_str(),scr_stream_name);
		}

		int blockIndex = Offset >> scrProgram::MAX_GLOBAL_BLOCKS_SHIFT;

		bAtLeastOneVariableInGlobalBlockIsReferenced[blockIndex] = true;

		if (scr_global_block_usage_file)
		{
			if (blockIndex > 0)
			{
				fprintf(scr_global_block_usage_file,"%d,%s,%s\r\n",blockIndex,Name.c_str(),scr_stream_name);
			}
		}
		WasReferenced = true;
	}
}


scrNodeNull::scrNodeNull(const scrType *type) : scrNodeExpr(NODE_NULL,type) { }


void scrNodeNull::Emit() {
	Quitf("Internal compiler error, should never emit NULL opcode.");
	EmitSource("0");
}




scrNodeConstant::scrNodeConstant(scrNodeType nodetype,const scrType *type) : scrNodeExpr(nodetype,type) { }


scrNodeBool::scrNodeBool(bool value) : scrNodeConstant(NODE_BOOL,&scrTypeBool::Instance), Value(value) { }


void scrNodeBool::Emit() {
	UpdateLineInfo();
	EmitInt(Value? 1 : 0);
	EmitSource(Value? "TRUE":"FALSE");
}


bool scrNodeBool::IsConstant(int &value) {
	value = Value != false;
	return true;
}


scrNodeInt::scrNodeInt(int value) : scrNodeConstant(NODE_INT,&scrTypeInt::Instance), Value(value) { }


void scrNodeInt::Emit() {
	UpdateLineInfo();
	EmitInt(Value);
	EmitSource("%d",Value);
}


bool scrNodeInt::IsConstant(int &value) {
	value = Value;
	return true;
}


scrNodeEnum::scrNodeEnum(scrTypeEnum::Enumerant *e) : scrNodeConstant(NODE_ENUM,e->Parent), Value(e) { }


void scrNodeEnum::Emit() {
	UpdateLineInfo();
	EmitInt(Value->Value);
	if (PARAM_emitc_skip_enum_names.Get())
	{
		EmitSource("%d",Value->Value);
	}
	else
	{
		EmitSource("%d /*%s*/",Value->Value,Value->Name);
	}
}


bool scrNodeEnum::IsConstant(int &value) {
	value = Value->Value;
	return true;
}


scrNodeFloat::scrNodeFloat(float value) : scrNodeConstant(NODE_FLOAT,&scrTypeFloat::Instance), Value(value) { }


void scrNodeFloat::Emit() {
	UpdateLineInfo();
	EmitFloat(Value);
	EmitSource("%g",Value);
}

bool scrNodeFloat::IsConstant(int &) {
	return false;
}


scrNodeString::scrNodeString(const char *value) : scrNodeConstant(NODE_STRING,&scrTypeString::Instance), Value(value), Length((int)strlen(value) + 1), IsDataTable(false) { }

scrNodeString::scrNodeString(scrDataList *list) : scrNodeConstant(NODE_STRING,&scrTypeString::Instance), Value(rage_new char[list->Count * 4 + 2]), Length(list->Count * 4), IsDataTable(true) { 
	char *v = (char*)Value;
	while (list) {
		*v++ = char(list->Data.Unsigned >> 24);
		*v++ = char(list->Data.Unsigned >> 16);
		*v++ = char(list->Data.Unsigned >> 8);
		*v++ = char(list->Data.Unsigned);
		list = list->Next;
	}
}

#if !__BE
static float scrNode_GetFloat(const char *c,unsigned idx)
{
	union {
		char c[4];
		float f;
	} x;
	c += idx * 4;
	x.c[0] = c[3];
	x.c[1] = c[2];
	x.c[2] = c[1];
	x.c[3] = c[0];
	return x.f;
}
#else
static float scrNode_GetFloat(const char *c,unsigned idx)
{
	// Already correct byte swapping.
	float *f = (float*)c;
	return f[idx];
}
#endif

void scrNodeString::Emit() {
	UpdateLineInfo();

	int prevOffset = -1;
	for (int i=0; i<StringOffsets.GetCount(); i++)
		if (!strcmp(pStringHeap + StringOffsets[i],Value)) 
		{
			prevOffset = StringOffsets[i];
			break;
		}

	// Make sure this string doesn't cross a page boundary
	size_t sl = strlen(Value);
	Assert(sl < scrStringSize);

	if (prevOffset == -1)
	{
		if (((size_t)StringHeapSize >> scrStringShift) != ((StringHeapSize + sl) >> scrStringShift))
			StringHeapSize = (StringHeapSize + scrStringMask) & ~scrStringMask;

		if (StringOffsets.IsFull())
		{
			Errorf("StringOffsets array is full. Current size is %d. You'll need a new script compiler with this number increased", StringOffsets.GetMaxCount());
		}
		Assertf(!StringOffsets.IsFull(), "StringOffsets array is full. Current size is %d. You'll need a new script compiler with this number increased", StringOffsets.GetMaxCount());

		StringOffsets.Append() = StringHeapSize;
		EmitInt(StringHeapSize);
		EmitOp(OP_STRING);

		strcpy(pStringHeap + StringHeapSize, Value);
		StringHeapSize += (int)(sl + 1);

		if (StringHeapSize >= ms_AllocatedSizeOfStringHeap)
		{
			Quitf("The size of the string heap needs to be increased. The current size is %d. Try specifying a new size with -stringHeapSize=<new size> in the Custom compiler settings of the .scproj file. In the Script Editor, they can be found in Tools -> Settings -> Compiling -> SanScript -> Custom", ms_AllocatedSizeOfStringHeap);
		}
	}
	else
	{
		EmitInt(prevOffset);
		EmitOp(OP_STRING);
	}

	EmitSource("\"");
	for (const char *c = Value; *c; c++)
		EmitSource((*c>=32 && *c<126 && *c != '\\' && *c != '\"')? "%c" : (*c=='\r'? "\\r" : *c=='\n'? "\\n" : *c=='\t'? "\\t" : *c=='\\'? "\\\\" : "\\x%02X"), unsigned char(*c));
	EmitSource("\"");

	if (scr_stringLiteralsFile)
	{
		fprintf(scr_stringLiteralsFile, "%s\r\n", Value);
	}
}


//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////   S T A T E M E N T S   //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

scrNodeScript::scrNodeScript(const char *progname,int lineNumber,scrNodeStmt *root,int paramCount,int varCount,const scrType *returnType,const scrLocal *firstLocal) 
	: scrNode(NODE_SCRIPT)
	, ProgName(progname)
	, Root(returnType? root : rage_new scrNodeStmts(root,rage_new scrNodeReturn(NULL,paramCount,NULL)))
	, ParamCount(paramCount), VarCount(varCount), ReturnType(returnType), FirstLocal(firstLocal) { 
		Line = lineNumber;
	}

extern bool g_SaveGlobals;

atRangeArray<const scrCommandInfo *,16384> stubs;
int stub_count;
extern char namespace_name[];

int GetInstructionSize(u8 *pc)
{
#define OP(a,b,c) b
	static u8 bytesPerOpcode[] = {
#include "script/opcodes.h"
	};
#undef OP
	int result = bytesPerOpcode[*pc];
	if (!result) switch (*pc) {
		// Weird cases:
		case OP_ENTER: 
			return 5 + pc[4];
		case OP_SWITCH:
			return 2+6*pc[1];
		default:
			Quitf("Bad opcode in GetInstructionSize");
			return -1;
	}
	else
		return result;
}

PARAM(forcename,"Force subprogram names to be emitted even in final scripts (security risk, for local testing only)");

void scrNodeScript::Emit() {
	int pc = scrNode::GetPC();
	bool hasName = (!PARAM_final.Get() && !PARAM_noprofile.Get()) || PARAM_forcename.Get();
	UpdateLineInfo();
	EmitOp(OP_ENTER,5 + (hasName? (int)(strlen(ProgName) + 3) : 0));
	if (ParamCount >= 256)
	{
		if (hasName)
		{
			node_errorf("%s has too many parameters (%d) in script invocation (very large structure passed by value?)", ProgName, ParamCount);
		}
		else
		{
			node_errorf("Too many parameters (%d) in script invocation (very large structure passed by value?)",ParamCount);
		}
	}
	Assert(ParamCount<256);
	Emit8(u8(ParamCount));
	Assert(VarCount<65536);
	Emit16(u16(VarCount));
	if (hasName) {
		int l = (int)(strlen(ProgName) + 1); // include NUL
		Assert(l<254);
		Emit8((u8)(l+2));
		Emit8('_');		// placeholder for profile slot index
		Emit8('_');		// second placeholder for profile slot index
		const char *s = ProgName;
		while (l--)
			Emit8(*s++);
	}
	else
		Emit8(0);

	bool has_script_statics = false;

	if (Root) {
		int selfCount = streams.GetCount();

		if ((selfCount == 0) && (!PARAM_dont_emit_preamble.Get())) {
			if (c_stream) {
				Stream &cur = other_streams[SCRIPT_DECLS];
				for (int i=0; i<cur.GetCount(); i++)
					fprintf(c_stream,cur[i]=='\n'?"\r\n" : "%c", cur[i]);
				cur.Reset();
			}
			if (c_stream) {
				Stream &cur = other_streams[SCRIPT_GLOBALS];
				if (cur.GetCount()) {
					fprintf(c_stream,"\r\n#ifndef script_globals_defined\r\n#define script_globals_defined\r\nstruct script_globals {\r\n");
					for (int i=0; i<cur.GetCount(); i++)
						fprintf(c_stream,cur[i]=='\n'?"\r\n" : "%c", cur[i]);
					fprintf(c_stream,"};\r\n\r\n");
					fprintf(c_stream,"static script_globals *globals;\r\n#endif // script_globals_defined\r\n");
					cur.Reset();
				}
			}
			if (c_stream) {
				Stream &cur = other_streams[SCRIPT_STATICS];
				if (cur.GetCount()) {
					fprintf(c_stream,"\r\n\r\nnamespace %s { struct script_statics {\r\n",namespace_name);
					extern char scr_arg_string[];
					if (scr_arg_string[0])
						fprintf(c_stream,"\t%s; // script args\r\n",scr_arg_string);
					for (int i=0; i<cur.GetCount(); i++)
						fprintf(c_stream,cur[i]=='\n'?"\r\n" : "%c", cur[i]);
					fprintf(c_stream,"}; } // %s\r\n\r\n",namespace_name);
					cur.Reset();
					has_script_statics = true;
				}
			}
		}

		streams.Grow(1);

		EmitSource("\n\n");

		if (!strncmp(ProgName,"DEBUG_",6))
			EmitSource("#ifdef _DEBUG\n");

		EmitSource("namespace %s { static %s %s(script_statics *statics",namespace_name,ReturnType?ReturnType->GetCType():"void",ProgName);
		const scrLocal *l = FirstLocal;
		int paramCount = ParamCount;
		while (paramCount) {	// subprogram parameters are first, followed by local variables.
			EmitSource(",%s %s",l->Type->GetCType(),l->Name);
			paramCount -= l->Type->GetSize();
			l = l->Next;
		}
		// if (!strcmp(ProgName,"SCRIPT"))
		//	EmitSource(",SCRIPT_ARGS");
		EndCall();
		NewLine();

		int lastSourceIndent = SourceIndent;
		SourceIndent = 1;

		EmitSource("{\n");
		Root->Emit();
		Root->Emit2();
		EmitSource("} }\n");

		SourceIndent = lastSourceIndent;

		while (streams.GetCount() > selfCount) {
			if (c_stream) {
				Stream &cur = streams.Top();
				for (int i=0; i<cur.GetCount(); i++)
					fprintf(c_stream,cur[i]=='\n'?"\r\n" : "%c", cur[i]);
			}
			streams.Top().Reset();
			streams.Pop();
		}

		char tn[MAX_TOKEN_LENGTH];
		if (ReturnType)
			ReturnType->GetName(tn);
		else
			strcpy(tn,"NULL");
		if (scr_debuginfo)
		{
			fprintf(scr_debuginfo,"%s,%s,%s,%d,%d\r\n",ReturnType?"FUNC":"PROC",tn,ProgName.m_String,pc,scrNode::GetPC()-1);
			const scrLocal *i = FirstLocal;
			while (i) {
				i->Type->EmitDebugInfo(scr_debuginfo,"LOCAL",i->Name,i->Address);
				i = i->Next;
			}
		}
		int progSize = scrNode::GetPC() - pc;
		int bigSize = 0;
		PARAM_warnbigprog.Get(bigSize);
		if (bigSize && progSize >= (bigSize << 10))
			Displayf("Info: %s(%d) : %s - Large subprogram, %d bytes",GetFilename(),Line,ProgName.m_String,progSize);
	}
	
	if (!strncmp(ProgName,"DEBUG_",6))
		EmitSource("#endif // #ifdef _DEBUG\n");

	EmitAccumulatedCalls();

	if (!strcmp(ProgName,"SCRIPT")) {
		if (stub_count) {
#if 1
			for (int i=0; i<stub_count; i++) {
				fprintf(c_stream,"#ifndef %s_defined\r\n#define %s_defined\r\n",stubs[i]->ProgName,stubs[i]->ProgName);
				fprintf(c_stream,"%s %s(",stubs[i]->ReturnType?stubs[i]->ReturnType->GetCType():"void",stubs[i]->ProgName);
				const scrTypeList *l = stubs[i]->Parameters;
				int siz = 0, num = 0;
				if (l == &g_VARARGS || l == &g_VARARGS1 || l == &g_VARARGS2 || l == &g_VARARGS3) {
					fprintf(c_stream,"unsigned __paramCount,unsigned __paramTypes,...)\r\n{\r\n\tva_list args;\r\n\tva_start(args,__paramTypes);\r\n\tscrValue data[49];\r\n");
					if (stubs[i]->ReturnType)
						fprintf(c_stream,"\tscrValue result[%d];\r\n",stubs[i]->ReturnType->GetSize());
					fprintf(c_stream,"\tInfo info(%s,0,data);\r\n\tinfo.Fill(__paramCount,__paramTypes,args);\r\n",stubs[i]->ReturnType?"result":"NULL");
				}
				else {
					while (l) {
						fprintf(c_stream,"%s P%d%s",l->Left->GetCType(),siz,l->Right?",":"");
						siz += l->Left->GetSize();
						++num;
						l = l->Right;
					}
					fprintf(c_stream,")\r\n{\r\n");
					if (siz)
						fprintf(c_stream,"\tscrValue data[%d];\r\n",siz);
					if (stubs[i]->ReturnType)
						fprintf(c_stream,"\tscrValue result[%d];\r\n",stubs[i]->ReturnType->GetSize());
					fprintf(c_stream,"\tInfo%s info(%s,%d,%s);\r\n",
						stubs[i]->HasVectorReferences?"_with_scrVector_byref":"",
						stubs[i]->ReturnType? "result":"NULL",num,num?"data":"NULL");
					l = stubs[i]->Parameters;
					siz = 0;
					char buf[256];
					while (l) {
						l->Left->EmitCopy(buf,siz);
						fprintf(c_stream,"%s",buf);
						l = l->Right;
					}
				}
				fprintf(c_stream,"\tstatic scrCmd func;\r\n\tnative_call(func,0x%08x,info);\r\n",stubs[i]->NativeHash);
				// \r\n\tif (!func)\r\n\t\tfunc = resolver(0x%08x);\r\n\tfunc(info);\r\n",stubs[i]->NativeHash);
				if (stubs[i]->ReturnType)
					fprintf(c_stream,"\treturn %s;\r\n",stubs[i]->ReturnType->GetValueField());
				fprintf(c_stream,"}\r\n#endif\r\n\r\n");
			}
#else
			fprintf(c_stream,"#ifdef __SNC__\r\nvoid DUMMY_FUNCTION() {\r\n");
			for (int i=0; i<stub_count; i++) {
				fprintf(c_stream,"\tasm(\".%s:\");\r\n",stubs[i]->ProgName);
				fprintf(c_stream,"\tasm(\"\\tli %%r0,%d\");\r\n",i*4);
				fprintf(c_stream,"\tasm(\"\\tb STUBCOMMON\");\r\n");
			}
			fprintf(c_stream,"\tasm(\"STUBCOMMON:\");\r\n");
			fprintf(c_stream,"\tasm(\"\tlis %%r11,SCRIPT_STUBS@ha\");\r\n");
			fprintf(c_stream,"\tasm(\"\taddic %%r11,%%r11,SCRIPT_STUBS@l\");\r\n");
			fprintf(c_stream,"\tasm(\"\tlwzx %%r11,%%r11,%%r0\");\r\n");
			fprintf(c_stream,"\tasm(\"\tmtctr %%r11\");\r\n");
			fprintf(c_stream,"\tasm(\"\tbctr\");\r\n");
			fprintf(c_stream,"}\r\n#else\r\nextern \"C\" {");
			for (int i=0; i<stub_count; i++) {
				fprintf(c_stream,"__declspec(naked) %s %s(",stubs[i]->ReturnType?stubs[i]->ReturnType->GetCType():"void",stubs[i]->ProgName);
				const scrTypeList *l = stubs[i]->Parameters;
				while (l) {
					fprintf(c_stream,"%s%s",l->Left->GetCType(),l->Right?",":"");
					l = l->Right;
				}
				fprintf(c_stream,")\r\n{\r\n");
				fprintf(c_stream,"#ifdef _M_PPC\r\n");
				fprintf(c_stream,"\t__asm   lau r11,SCRIPT_STUBS\r\n");
				fprintf(c_stream,"\t__asm   lal r11,r11,SCRIPT_STUBS\r\n");
				fprintf(c_stream,"\t__asm   lwz r11,%d(r11)\r\n",i*4);
				fprintf(c_stream,"\t__asm   mtctr r11\r\n");
				fprintf(c_stream,"\t__asm   bctr\r\n");
				fprintf(c_stream,"#else\r\n");
				fprintf(c_stream,"\t__asm jmp DWORD PTR [SCRIPT_STUBS+%d]\r\n",i*4);
				fprintf(c_stream,"#endif\r\n");
				fprintf(c_stream,"}\r\n");
			}
			fprintf(c_stream,"}\r\n#endif\r\n\r\n");
#endif
		}
 
		if (c_stream) {
			unsigned globalsHash = scrProgram::CalculateGlobalHash(Globals,GlobalSize);
			if (has_script_statics) {
				fprintf(c_stream,"namespace %s { static void construct_statics(script_statics* that) { ::new(that) script_statics; } }\r\n",namespace_name);
				fprintf(c_stream,"\r\n\r\nscript_def %s_script_def = { %d, sizeof(%s::script_statics), 0x%08x, \"%s\", (void(*)(scrValue*))%s::SCRIPT, (void(*)(scrValue*))%s::construct_statics };\r\n",
					namespace_name,scr_arg_struct_size,namespace_name,globalsHash,namespace_name,namespace_name,namespace_name);
			}
			else {
				fprintf(c_stream,"\r\n\r\nscript_def %s_script_def = { %d, 0, 0x%08x, \"%s\", (void(*)(scrValue*))%s::SCRIPT, NULL };\r\n",
					namespace_name,scr_arg_struct_size,globalsHash,namespace_name,namespace_name);
			}
		}
	}

	/* int finalPc = GetPC();
	for (int i=pc; i<finalPc; i+=GetInstructionSize(&Program[i])) {
		u8 opcode = Program[i];
		if (opcode == OP_JZ || opcode == OP_J) {
			int target = i + 3 + s16((Program[i+1]<<8)|Program[i+2]);
			// It's possible for jumps to leave the function if the jumps themselves are unreachable.
			// This happens when a function containing a switch has a BREAK after a RETURN statement in the switch body.
			while (target < finalPc) {
				// This test is incorrect when short-circuiting an OR because we don't account for the INOT.
				if (opcode==OP_JZ && Program[target]==OP_DUP && Program[target+1]==OP_JZ)
					target = (target+1) + 3 + s16((Program[target+2]<<8)|Program[target+3]);
				else if ((opcode==OP_JZ || opcode==OP_J) && Program[target]==OP_J)
					target = target + 3 + s16((Program[target+1]<<8)|Program[target+2]);
				else
					break;
			}
			if (target < finalPc) {
				int delta = target - (i + 3);
				Program[i+1] = u8(delta >> 8);
				Program[i+2] = u8(delta);
			}
		}
	} */
} 


scrNodeStmt::scrNodeStmt(scrNodeType type) : scrNode(type) { }


int scrNodeStmt::EmitForwardJump(scrOpcode type) {
	EmitOp(type);
	int result = GetPC();
	EmitS16(0);		// placeholder
	return result;
}


void scrNodeStmt::EmitBackwardJump(scrOpcode type,int pc) {
	// Emit the opcode now, so that we get the current program counter if it was *right* before a page break.
	EmitOp(type);
	// Start of jump is (GetPC() + 2) (our next insn) and destination is pc
	//			  dest   -    src
	int distance = pc - (GetPC() + 2);
	Assertf(distance >= -32768 && distance <= 32767, "Jump is %d away from target, too far.", distance);
	EmitS16(distance);
	/* if (distance < 256) -- only saved about 20k out of 36M of scripts, not worth it.
		++Potential; */
}


void scrNodeStmt::PlaceJump(int target) {
	// Start of jump is (target + 2) and destination is GetPC()
	//              dest   -    src
	int distance = GetPC() - (target + 2);
	Assertf(distance >= -32768 && distance <= 32767, "Jump is %d away from target, too far. Start of jump PC is %d. Destination PC is %d", distance, (target + 2), GetPC());
	Program[target] = (u8) (distance  >> 8);
	Program[target+1] = (u8) (distance);
}


scrNodeStmts::scrNodeStmts(scrNodeStmt *a,scrNodeStmt *b) : scrNodeStmt(NODE_STMTS), Head(a), Tail(b) { }


void scrNodeStmts::Emit() {
	// UpdateLineInfo();
	if (Head) {
		Head->Emit();
		// NewLine();
	}
	if (Tail) {
		Tail->Emit();
		// NewLine();
}
}


void scrNodeStmts::Emit2() {
	if (Head)
		Head->Emit2();
	if (Tail)
		Tail->Emit2();
}


scrNodeDecl::scrNodeDecl(const scrType *type,const char *name,bool hasAssignment) : scrNodeStmt(NODE_DECL), Type(type), Name(name), HasAssignment(hasAssignment) { }


void scrNodeDecl::Emit() {
	EmitSource(HasAssignment? "\t%s %s;\n" : "\t%s %s%s;\n",Type->GetCType(),Name.c_str(),Type->GetZeroType());
}


scrNodeAssign::scrNodeAssign(int lineNumber,const scrType *type,scrNodeExpr *address,scrNodeExpr *expr,bool defaultToZero) : scrNodeStmt(NODE_ASSIGN), Type(type), Address(address), Expr(expr), DefaultToZero(defaultToZero) { 
	Line = lineNumber;
	int temp;
	//people do this to explicitly bail out of loops early, can't flag it.
	//if (address->GetReadOnly())
	//	node_warningf("Cannot modify loop control variable.");
	const scrType *innerType = Type->GetInnerType();
	if (innerType == &scrTypeInt::Instance && expr->GetType() == &scrTypeFloat::Instance)
		node_warningf("Cannot assign float to int, use FLOOR, ROUND, or CEIL functions.");
	else if (innerType == &scrTypeFloat::Instance && expr->GetType() == &scrTypeInt::Instance) {
		if (!expr->IsConstant(temp))
			node_warningf("Cannot assign int expression to float, use TO_FLOAT function");
		else
			Expr = rage_new scrNodeFloat((float)temp); // scrNodeUnary(OP_I2F, expr, &scrTypeFloat::Instance, "op_i2f");
	}
	else if (type->IsTextLabel() && (expr->GetType() == &scrTypeString::Instance ||
		expr->GetType()->IsTextLabel() || expr->GetType() == &scrTypeInt::Instance)) {
			/* this is valid */
	}
	else if (type == &scrTypeInt::Instance && expr->GetType() == &scrTypeInt::EnumToIntInstance) {
		/* this is also valid */
	}
	else if (type->IsTypeDef() && expr->GetType()->IsTypeDef()) {
		scrTypeDef *formal = (scrTypeDef*) type;
		scrTypeDef *actual = (scrTypeDef*) expr->GetType();
		CheckTypeDef(formal->Info,actual->Info);
	}
	else if (type->IsObject() && expr->GetType()->IsObject() && type->IsCompatibleWith(expr->GetType())) {
		/* assigning a subclass object to a parent object */
	}
	else if (innerType != expr->GetType() && !((innerType->IsObject() || innerType->IsString() || innerType->IsTypeDef()) && expr->GetType() == &scrTypeNull::Instance))
		node_warningf("Incompatible types in assignment");
}


void scrNodeAssign::Emit() {
	UpdateLineInfo();
	int testValue = 1;
	// If it's an area that already defaults to zero and it's being set to zero, don't bother.
	if (DefaultToZero && ((Expr->IsConstant(testValue) && !testValue) || Expr->GetType() == &scrTypeNull::Instance))
		return;
	if (Type->IsTextLabel() && !Expr->GetType()->IsTextLabel()) {
		BeginCall("\top_store");
		Expr->Emit();
		Comma();
		Address->Emit();
		EndCall();
		EndStmt();
		EmitOp(Expr->GetType() == &scrTypeInt::Instance? OP_TEXT_LABEL_ASSIGN_INT : OP_TEXT_LABEL_ASSIGN_STRING);
		Emit8(u8(Type->GetSize()*4));
	}
	else {
		if (Type->IsTextLabel() && Expr->GetType()->IsTextLabel() && Type->GetSize() != Expr->GetType()->GetSize()) {
			BeginCall("\top_store");
			Expr->Emit();
			Comma();
			EmitInt(Expr->GetType()->GetSize());
			EmitInt(Type->GetSize());
			Address->Emit();
			EndCall();
			EndStmt();
			EmitOp(OP_TEXT_LABEL_COPY);
		}
		else {
			BeginCall("\top_store");
			Expr->Emit();
			Comma();
			int size = Type->GetSize();
			if (size > 1) {
				EmitInt(size);
				Address->Emit();
				EmitOp(OP_STORE_N);
			}
			else
				StoreAddress(Address);
			EndCall();
			EndStmt();
		}
	}
}


scrNodeAppend::scrNodeAppend(scrNodeExpr *address,const scrType *type,scrNodeExpr *expr) : scrNodeStmt(NODE_APPEND), Address(address), Expr(expr), Type(type) { 
	if (Expr->GetType() != &scrTypeInt::Instance && !Expr->GetType()->IsTextLabel() && Expr->GetType() != &scrTypeString::Instance)
		node_warningf("Appending to a TEXT_LABEL requires TEXT_LABEL, STRING, or INT on right-hand side");
}


void scrNodeAppend::Emit() {
	UpdateLineInfo();

	if (Expr->GetType() == &scrTypeInt::Instance) {
		BeginCall("\top_text_label_append_int");
		Expr->Emit();
		Comma();
		Address->Emit();
		EndCall();
		EndStmt();
		EmitOp(OP_TEXT_LABEL_APPEND_INT);
		Emit8(u8(Type->GetSize()*4));
	}
	else {
		BeginCall("\top_text_label_append_string");
		if (Expr->GetType()->IsTextLabel())
			Expr->EmitRef();
		else
			Expr->Emit();
		Comma();
		Address->Emit();
		EndCall();
		EndStmt();
		EmitOp(OP_TEXT_LABEL_APPEND_STRING);
		Emit8(u8(Type->GetSize()*4));
	}
}


scrNodeConstruct::scrNodeConstruct(const scrType *type,scrNodeExpr *address) : scrNodeStmt(NODE_CONSTRUCT), Type(type), Address(address) {
}


void scrNodeConstruct::Emit() {
	if (Type->NeedEmitConstruct()) {
		EmitSource("\t__ignored__(");
		Address->Emit();
		Type->EmitConstruct();
		EmitSource(");\n");
		EmitOp(OP_DROP);
	}
}



scrNodeScope::scrNodeScope(int lineNumber,scrNodeStmt *body) : scrNodeStmt(NODE_SCOPE), Body(body) {
	Line = lineNumber;
}


void scrNodeScope::Emit() {
	UpdateLineInfo();
	if (Body) {
		BeginBlock();
		Body->Emit();
		EndBlock();
	}
}


void scrNodeScope::Emit2() {
	if (Body)
		Body->Emit2();
}


scrNodeIf::scrNodeIf(int lineNumber,scrNodeExpr *expr,scrNodeStmt *ifTrue,scrNodeStmt *ifFalse) : scrNodeStmt(NODE_IF), Expr(expr), IfTrue(ifTrue), IfFalse(ifFalse) { 
	Line = lineNumber;
	if (expr->GetType() != &scrTypeBool::Instance)
		node_warningf("IF expects boolean expression");
}


void scrNodeIf::Emit() {
	UpdateLineInfo();
	int ival;
	if (Expr->IsConstant(ival)) {
		if (!PARAM_final.Get())		// Final builds trigger a lot of these.
			Displayf("Info: %s(%d) : Dead code, eliminating %s branch.",GetFilename(),Line,ival?"FALSE":"TRUE");
		if (ival) {
			if (IfTrue)
				IfTrue->Emit();
		}
		else if (IfFalse)
			IfFalse->Emit();
	}
	else {
		EmitSource("\tif (");
		Expr->Emit();
		EmitSource(")\n");
		BeginBlock();
		int ifFalseLabel = EmitForwardJump(OP_JZ);
		if (IfTrue)
			IfTrue->Emit();
		EndBlock();
		if (IfFalse) {
			EmitSource("\telse\n");
			BeginBlock();
			int ifTrueLabel = EmitForwardJump(OP_J);
			PlaceJump(ifFalseLabel);
			IfFalse->Emit();
			PlaceJump(ifTrueLabel);
			EndBlock();
		}
		else {
			PlaceJump(ifFalseLabel);
		}
	}
}


void scrNodeIf::Emit2() {
	if (IfTrue)
		IfTrue->Emit2();
	if (IfFalse)
		IfFalse->Emit2();
}


struct LabelList {
	int Label;
	LabelList *Next;
};

LabelList *s_BreakLoops, *s_ReLoops;

#define BEGIN_LOOP \
	LabelList *breakStop = s_BreakLoops, *reStop = s_ReLoops

#define END_RE_LOOP \
	while (s_ReLoops != reStop) { \
		PlaceJump(s_ReLoops->Label); \
		s_ReLoops = s_ReLoops->Next; \
	}

#define END_BREAK_LOOP \
	while (s_BreakLoops != breakStop) { \
		PlaceJump(s_BreakLoops->Label); \
		s_BreakLoops = s_BreakLoops->Next; \
	}

scrNodeBreakLoop::scrNodeBreakLoop(int lineNumber) : scrNodeStmt(NODE_BREAKLOOP) {
	Line = lineNumber;
}

void scrNodeBreakLoop::Emit() {
	LabelList *l = rage_new LabelList;
	l->Label = EmitForwardJump(OP_J);
	l->Next = s_BreakLoops;
	s_BreakLoops = l;
}

scrNodeReLoop::scrNodeReLoop(int lineNumber) : scrNodeStmt(NODE_RELOOP) {
	Line = lineNumber;
}

void scrNodeReLoop::Emit() {
	LabelList *l = rage_new LabelList;
	l->Label = EmitForwardJump(OP_J);
	l->Next = s_ReLoops;
	s_ReLoops = l;
}


scrNodeWhile::scrNodeWhile(int lineNumber,scrNodeExpr *expr,scrNodeStmt *body) : scrNodeStmt(NODE_WHILE), Expr(expr), Body(body) { 
	Line = lineNumber;
	if (expr->GetType() != &scrTypeBool::Instance)
		node_warningf("WHILE expects boolean expression");
}


void scrNodeWhile::Emit() {
	UpdateLineInfo();
	int ival;
	if (!Expr->IsConstant(ival) || ival) {
		int top = GetPC();
		EmitSource("\twhile (");
		Expr->Emit();
		EmitSource(")\n");
		BEGIN_LOOP;
		BeginBlock();
		int exitLabel = EmitForwardJump(OP_JZ);
		if (Body)
			Body->Emit();
		EndBlock();
		END_RE_LOOP;
		EmitBackwardJump(OP_J,top);
		END_BREAK_LOOP;
		PlaceJump(exitLabel);
	}
	else
		Displayf("Info: %s(%d) : Dead code in WHILE loop.",GetFilename(),Line);
}


void scrNodeWhile::Emit2() {
	if (Body)
		Body->Emit2();
}


scrNodeFor::scrNodeFor(int lineNumber,scrNodeExpr *address,scrNodeExpr *initialValue,scrNodeExpr *finalValue,int step,scrNodeStmt *body) : scrNodeStmt(NODE_FOR),
	Address(address), InitialValue(initialValue), FinalValue(finalValue), Step(step), Body(body) { 
	Line = lineNumber;
	if (address->GetReadOnly())
		node_warningf("Cannot modify loop control variable (nested loop?).");
}


void scrNodeFor::Emit() {
	UpdateLineInfo();
	// Perform initial assignment
	EmitSource("\top_store(");
	InitialValue->Emit();
	Comma();
	StoreAddress(Address);
	EmitSource(");\n");
	EmitSource("\twhile (");
	// Remember top of loop
	int top = GetPC();
	LoadAddress(Address);
	EmitSource(Step > 0? " <= " : " >=");
	FinalValue->Emit();
	EmitSource(")\n");
	// Check terminating condition and do jump
	EmitOp(Step > 0? OP_ILE : OP_IGE);
	int exitLabel = EmitForwardJump(OP_JZ);
	BEGIN_LOOP;
	// Body of the loop
	BeginBlock();
	if (Body)
		Body->Emit();
	// Apply step
	EmitSource("\top_store(");
	END_RE_LOOP;
	LoadAddress(Address);
	EmitSource("+%d,",Step);
	EmitInt(Step);
	EmitOp(OP_IADD);
	StoreAddress(Address);
	EndCall();
	EndStmt();
	EndBlock();
	EmitBackwardJump(OP_J,top);
	END_BREAK_LOOP;
	PlaceJump(exitLabel);
}


void scrNodeFor::Emit2() {
	if (Body)
		Body->Emit2();
}


scrNodeRepeat::scrNodeRepeat(int lineNumber,scrNodeExpr *count,scrNodeExpr *address,scrNodeStmt *stmt) : scrNodeStmt(NODE_REPEAT), Count(count), Address(address), Body(stmt) { 
	Line = lineNumber;
	if (address->GetReadOnly())
		node_warningf("Cannot modify loop control variable (nested loop?).");
}



void scrNodeRepeat::Emit() {
	UpdateLineInfo();
	// Reset loop count to zero
	EmitSource("\top_store(0,");
	EmitInt(0);
	StoreAddress(Address);
	EmitSource(");\n");
	EmitSource("\twhile (");
	// Remember top of loop
	int top = GetPC();
	LoadAddress(Address);
	EmitSource(" < ");
	Count->Emit();
	EmitOp(OP_ILT);
	EmitSource(")\n");
	int exitLabel = EmitForwardJump(OP_JZ);
	// Body of the loop
	BeginBlock();
	BEGIN_LOOP;
	if (Body)
		Body->Emit();
	// Increment the counter
	EmitSource("\top_store(");
	END_RE_LOOP;
	LoadAddress(Address);
	EmitInt(1);
	EmitOp(OP_IADD);
	EmitSource("+1,");
	StoreAddress(Address);
	EmitSource(");\n");
	EndBlock();
	EmitBackwardJump(OP_J,top);
	END_BREAK_LOOP;
	PlaceJump(exitLabel);
}


void scrNodeRepeat::Emit2() {
	if (Body)
		Body->Emit2();
}


scrNodeReturn::scrNodeReturn(scrNodeExpr *expr,int paramCount,const scrType *returnType) : scrNodeStmt(NODE_RETURN), Expr(expr), ParamCount(paramCount), ReturnType(returnType) { }

void scrNodeReturn::Emit() {
	UpdateLineInfo();
	Assert(Expr || !ReturnType);
	if (Expr) {
		EmitSource("\treturn ");
		if (Expr->GetType() != ReturnType) {
			if (!ReturnType->IsObject() || Expr->GetType() != &scrTypeNull::Instance)
				node_warningf("Unable to convert RETURN expression to FUNC return type");
		}
		Expr->Emit();
	}
	else
		EmitSource("\treturn");
	EndStmt();

	EmitOp(OP_LEAVE);
	Emit8(u8(ParamCount));
	Emit8(ReturnType? u8(ReturnType->GetSize()) : 0);
}


scrNodeCommand::scrNodeCommand(const scrCommandInfo *info,scrExprList *parameters,scrNodeExpr *addr) : scrNodeStmt(NODE_COMMAND), Info(info), Parameters(parameters), Addr(addr) { }


atFixedArray<u64,scrNode::MaxNatives> scrNode::Natives;

extern fiStream *scr_native_file;

void scrNode::EmitNativeStub(const scrCommandInfo *Info,int extraSize) {
	int returnSize = (Info->ReturnType? Info->ReturnType->GetSize() : 0);
	int paramSize = Info->Parameters? Info->Parameters->Size : 0;
	paramSize += extraSize;
	// 2 bits for returnSize, 6 bits for param size, 16 bits for native count.
	Assertf(returnSize <= 3,"Return size is %d for native",returnSize);
	Assertf(paramSize <= 63,"Too many parameters %d for native",Info->Parameters->Size);

	//Note(crolivier): making a special case for natives that get called a lot and instead we
	//					want to turn them into opcodes to reduce on overhead.
	if (!strcmp(Info->ProgName, "IS_BIT_SET"))
 	{
 		EmitOp(OP_IS_BIT_SET);
 		return;
 	}

	EmitOp(OP_NATIVE);
	if (Info->NativeSlot == -1) {
		Info->NativeSlot = Natives.GetCount();
		Assertf(!Natives.IsFull(), "scrNode::EmitNativeStub - too many different native commands used by this .sc file. You'll need a new script compiler with scrNode::MaxNatives increased");
		Natives.Push(Info->NativeHash);
		if (scr_native_file)
			fprintf(scr_native_file,"%016llx (%s) was referenced\r\n",Info->NativeHash,Info->ProgName);
	}
	// 2 : 6
	Emit8(u8(returnSize | (paramSize << 2)));
	Emit8(u8(Info->NativeSlot>>8));
	Emit8(u8(Info->NativeSlot));

	if (Info->WasStubEmitted)
		return;
	Info->WasStubEmitted = true;
	if (PARAM_emitc.Get()) {
		// Special-case some common native calls we implement ourselves (10-15% code size savings in some cases!)
		if (Info->NativeHash != ATSTRINGHASH("IS_BIT_SET",0x902e26ac) && Info->NativeHash != ATSTRINGHASH("CLEAR_BIT",0x8bc9e618)
			&& Info->NativeHash != ATSTRINGHASH("SET_BIT", 0x4efe7e6b))
		stubs[stub_count++] = Info;
	}
}


void scrNodeCommand::Emit() {
	if (PARAM_final.Get() && Info->ProgName && Info->DebugOnly)
		return;

	EmitSource("\t%s(%s",Info->ProgName?Info->ProgName:"op_indirectcall(",Info->NativeHash?"":Info->Parameters?"statics,":"statics");

	UpdateLineInfo();
	int extraSize = CollectParameters(Info->Parameters,Parameters);

	int returnSize = (Info->ReturnType? Info->ReturnType->GetSize() : 0);
	if (Info->NativeHash) {
		EmitNativeStub(Info,extraSize);
	}
	else {
		Assert(extraSize == 0);
		if (Addr) {
			EmitSource("),");
			LoadAddress(Addr);
			EmitOp(OP_CALLINDIRECT);
		}
		else {
			EmitCall(Info,false);
		}
	}
	EndCall();
	EndStmt();
	while (returnSize--)
		EmitOp(OP_DROP);
	
}


scrNodeSwitch::scrNodeSwitch(scrNodeExpr *expr) : scrNodeStmt(NODE_SWITCH), Expr(expr), Body(NULL), Default(0), HadBreak(true), HasDefault(false), HasAtLeastOneBreak(false), LastLabel(0) { }

void scrNodeSwitch::Emit() {
	// Emit the expression
	if (!Cases.GetCount())
		node_errorf("SWITCH statement found with no CASE labels");
	EmitSource("\tswitch (");
	Expr->Emit();
	EmitSource(")\n");
	BeginBlock();
	BeginBlock();
	EmitOp(OP_SWITCH,2 + Cases.GetCount() * 6);
	Assert(Cases.GetCount() < 256);
	Emit8(u8(Cases.GetCount()));
	TableStart = GetPC();
	for (int i=0; i<Cases.GetCount(); i++) {
		Emit32(Cases[i]);
		EmitS16(0);
	}
	// Emit the jump to the DEFAULT label
	Default = EmitForwardJump(OP_J);

	if (Body)
		Body->Emit();

	// Resolve all BREAK statements now
	for (int i=0; i<Breaks.GetCount(); i++)
		PlaceJump(Breaks[i]);

	// If we didn't already have a default label, just jump past the switch
	if (Default > 0)
		PlaceJump(Default);

	EndBlock();	/// extra one
	EndBlock();
}


void scrNodeSwitch::Emit2() {
	if (Body)
		Body->Emit2();
}


void scrNodeSwitch::AddBreak() {
	if (Breaks.GetCount() == Breaks.GetMaxCount())
		node_errorf("Too many BREAK statements in SWITCH");
	else
		Breaks.Push(EmitForwardJump(OP_J));
}


void scrNodeSwitch::AddCase(s32 value) {
	if (Cases.Find(value) != -1)
		node_errorf("Duplicate CASE or DEFAULT label in SWITCH statement");
	else
	{
		if (Cases.GetCount() == Cases.GetMaxCount())
			node_errorf("Too many CASE statements in SWITCH");
		else
			Cases.Push(value);
	}
}


void scrNodeSwitch::EmitCase(s32 value) {
	int idx = Cases.Find(value);
	AssertMsg(idx != -1,"CASE statement disappeared out from under me?");
	// Write the current program counter out into the switch jump table
	PlaceJump(TableStart + 4 + 6 * idx);
}


void scrNodeSwitch::EmitDefault() {
	if (Default == -1)
		node_errorf("Duplicate DEFAULT label in SWITCH statement");
	else {
		PlaceJump(Default);
		Default = -1;
	}
}

void scrNodeSwitch::SetHasDefault()
{
	if(HasDefault)
		node_errorf("Duplicate DEFAULT label in SWITCH statement");
	else
		HasDefault = true;
}

void scrNodeSwitch::SetHasAtLeastOneBreak()
{
	HasAtLeastOneBreak = true;
}

bool scrNodeSwitch::Returns() const
{
	// If there's no default label, we cannot guarantee it will always return
	if (!HasDefault)
		return false;

	// If there were any BREAK statements, we cannot guarantee it will always return
	//	This used to check if (Breaks.GetCount() > 0) but the Breaks array is empty during the parsing stage.
	//	Entries are only Pushed on to the Breaks array later - during the Emit calls.
	if (HasAtLeastOneBreak)
	{
		return false;
	}

	return Body && Body->Returns();
}



scrNodeBreak::scrNodeBreak(scrNodeSwitch *parent) : scrNodeStmt(NODE_BREAK), Parent(parent)
{
	parent->SetHadBreak(true);
	parent->SetHasAtLeastOneBreak();
}


void scrNodeBreak::Emit() {
	Parent->AddBreak();
	EmitSource("\tbreak;\n");
}


scrNodeDefault::scrNodeDefault(scrNodeType type,scrNodeSwitch *parent) : scrNodeStmt(type), Parent(parent), FirstLabel(!parent->GetLastLabel()), LastLabel(false) { 
	if (type == NODE_DEFAULT)
		Parent->SetHasDefault();	// otherwise our single derived class took care of this.  This is ugly.
	Parent->SetLastLabel(this);
}


void scrNodeDefault::Emit() {
	Parent->EmitDefault();
	if (FirstLabel)
		EndBlock();
	EmitSource("\tdefault:\n");
	if (LastLabel)
		BeginBlock();
}


scrNodeCase::scrNodeCase(scrNodeSwitch *parent,int value,const char *enumString) : scrNodeDefault(NODE_CASE,parent), Value(value), EnumString(enumString) { 
	Parent->AddCase(Value);
}


void scrNodeCase::Emit() {
	Parent->EmitCase(Value);
	if (FirstLabel)
		EndBlock();
	if (EnumString)
		EmitSource("\tcase %d: /* %s, 0x%x */\n",Value,EnumString,Value);
	else
		EmitSource("\tcase %d: /* 0x%x */\n",Value,Value);
	if (LastLabel)
		BeginBlock();
}


scrNodeLabel::scrNodeLabel(bool placed) : Placed(placed), scrNodeStmt(NODE_LABEL) {
	Address = 0;		// impossible address for a label.
}


void scrNodeLabel::Emit() {
	UpdateLineInfo();
	Address = GetPC();
}


scrNodeGoto::scrNodeGoto(scrNodeLabel *target) : Target(target), scrNodeStmt(NODE_GOTO) {
	Address = 0;
}


void scrNodeGoto::Emit() {
	UpdateLineInfo();
	// If it's a known address (ie backward jump) then emit it now.
	if (Target->GetAddress()) {
		EmitBackwardJump(OP_J,Target->GetAddress());
	}
	else {
		Address = GetPC() + 1;	// Location of address once we can patch it
		EmitForwardJump(OP_J);
	}
}


void scrNodeGoto::Emit2() {
	if (Address) {
		if (!Target->GetAddress())
			node_warningf("GOTO to unknown label");
		else {
			//                  dest                 src
			int distance = Target->GetAddress() - (Address + 2);
			Assertf(distance >= -32768 && distance <= 32767, "Goto is %d away from target, too far.", distance);
			Program[Address] = u8(distance >> 8);
			Program[Address+1] = u8(distance);
		}
	}
}


scrNodeThrow::scrNodeThrow(scrNodeExpr *expr) : Expr(expr), scrNodeStmt(NODE_THROW) { 
	if (!expr->GetType()->IsInteger())
		node_warningf("THROW expression must be an integer.");
}


void scrNodeThrow::Emit() {
	UpdateLineInfo();
	BeginCall("op_throw");
	Expr->Emit();
	EndCall();
	EndStmt();
	EmitOp(OP_THROW);
	static bool uses_exceptions;
	if (!uses_exceptions) {
		EmitSource("#define USES_EXCEPTIONS\n");
		uses_exceptions = true;
	}
}

