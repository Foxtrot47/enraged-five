USING "rage_builtins.sch"

CONST_INT LoopCount 5
CONST_FLOAT TestNegative -1.5

SCRIPT
	INT I
	INT LC = LoopCount
	FLOAT Array[LoopCount]
	REPEAT LC I
		Array[I] = TO_FLOAT(I * I)
	ENDREPEAT
ENDSCRIPT

