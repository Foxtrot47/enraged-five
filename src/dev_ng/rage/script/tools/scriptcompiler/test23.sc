USING "rage_builtins.sch" 

FUNC INT ADDER(INT &input[])
	INT SUM, I
	FOR I=0 TO COUNT_OF(input)-1
		SUM += input[I]
	ENDFOR
	RETURN SUM
ENDFUNC

SCRIPT
	INT FOO[5]
	FOO[0] = 1
	FOO[1] = 2
	FOO[2] = 3
	FOO[3] = 4
	FOO[4] = 5
	PRINTINT (ADDER(FOO))
ENDSCRIPT
