#include <stdlib.h>
#include <stdio.h>
#include <string.h>


void main(int argc,char **argv)
{
	if (argc == 1) {
		puts(
		"How to use this program:\n"
		"1. add -globalreffile to your script command line.\n"
		"2. You can add a filename to the flag, or it will default to c:\\global-references.txt\n"
		"3. Make sure that file doesn't currently exist\n"
		"4. Do a full rebuild of the script project\n"
		"5. When it finishes, pass the name of that file to this program.");
		exit(0);
	}
	FILE *f = fopen(argv[1],"rt");
	if (!f) {
		fprintf(stderr,"Cannot open '%s'\n",argv[1]);
		exit(1);
	}
	char buf[512];
	const int maxi = 65536;
	static char *globals[maxi];
	static int refCounts[maxi];
	int numGlobals = 0;
	for (int pass=1; pass<=2; pass++) {
		rewind(f);
		while (fgets(buf,sizeof(buf),f)) {
			char *name = strchr(buf,',');
			char *eol = name? strchr(name+1,',') : 0;
			if (name && eol) {
				*name++ = 0;
				*eol = 0;
			}
			if (pass == 1 && !strcmp(buf,"DECL")) {
				if (numGlobals == maxi) {
					fprintf(stderr,"too many globals, raise maxi in source code\n");
					exit(1);
				}
				globals[numGlobals++] = strdup(name);
			}
			else if (pass == 2 && !strcmp(buf,"REF")) {
				for (int j=0; j<numGlobals; j++) {
					if (!strcmp(name,globals[j])) {
						++refCounts[j];
						break;
					}
				}
			}
		}
		if (pass == 1)
			printf("%d total globals\n",numGlobals);
		else {
			for (int k=0; k<3; k++) {
				printf("\n\nGlobals referenced **%d** times:\n",k);
				for (int j=0; j<numGlobals; j++)
					if (refCounts[j] == k)
						printf("%s\n",globals[j]);
			}
		}
	}
	fclose(f);
}
