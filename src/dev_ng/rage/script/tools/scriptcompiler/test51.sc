USING "rage_builtins.sch"

FUNC TEXT_LABEL_15 TIME_TO_STRING(int iTime)
            TEXT_LABEL_15 tTime
            int iMilliSeconds = iTime / 10
            int iSeconds = iTime / 1000
            int iMinutes = iSeconds / 60
            iMilliSeconds -= iSeconds * 100
            iSeconds -=       iMinutes * 60
            
            if iMinutes < 10
                        tTime += 0
            endif
            tTime += iMinutes
            tTime += ":"
            if iSeconds < 10
                        tTime += 0
            endif
            tTime += iSeconds
            tTime += ":"
            if iMilliSeconds < 10
                        tTime += 0
            endif
            tTime += iMilliSeconds
            return tTime
ENDFUNC

SCRIPT
	PRINTSTRING(TIME_TO_STRING(123456))
ENDSCRIPT
