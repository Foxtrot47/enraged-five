USING "rage_builtins.sch"

GLOBALS

STRUCT MyGlobalStruct
STRING MyGlobalString = NULL
ENDSTRUCT


MyGlobalStruct GLOBALStruct

ENDGLOBALS
	
proc inner(int &foo[])
	printnl()
	foo[99] = 0
endproc

proc middle(int &foo[])
	printnl()
	printnl()
	inner(foo)
endproc

script
	int foo[4]
	printnl()
	middle(foo)
endscript