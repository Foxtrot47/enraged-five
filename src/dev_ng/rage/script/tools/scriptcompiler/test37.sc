USING "rage_builtins.sch"

SCRIPT
	WHILE TRUE
		GOTO A

		// You are not allowed to declare any arrays or initialized variables after doing a GOTO.
		// The compiler can't tell if you would be skipping over their initialization
		// (which is particularly important for arrays) and therefore just bans it entirely.
		// INT ARR[3]
		
	B:
		// ARR[0] = 1
		
		PRINTSTRING("B\n")
		IF TRUE
			GOTO C
		ENDIF

	A:
		PRINTSTRING("A\n")
		GOTO B

	ENDWHILE

	C:
	PRINTSTRING("C\n")
ENDSCRIPT
