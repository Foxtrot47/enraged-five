USING "rage_builtins.sch"

SCRIPT

	VECTOR gw_vec1 = << 0.4, 0.5, 0.7>>
	VECTOR gw_vec2 = << 1.4, 16.7, 16.7>>

	IF gw_vec1 >= gw_vec2
		PRINTSTRING("Vectors match")
	ELSE
		PRINTSTRING("Vectors don't match")
	ENDIF

ENDSCRIPT

