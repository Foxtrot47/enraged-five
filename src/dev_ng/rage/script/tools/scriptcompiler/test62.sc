NATIVE A__

NATIVE B__ : A__

PROC TAKE_A_REF(A__ &cool)
    cool = cool
ENDPROC
 
PROC TAKE_B_REF(B__ &notcool)
    notcool = notcool
ENDPROC
 
SCRIPT
    A__ toInitB
    B__ toInitA
 
    TAKE_A_REF(toInitA)
    TAKE_B_REF(toInitB) //<--parse error
ENDSCRIPT

