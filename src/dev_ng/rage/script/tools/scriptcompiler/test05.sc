ENUM TASK_TYPE
	A, B, C, D
ENDENUM

SCRIPT
	TASK_TYPE Task
	INT Result
	REPEAT 5 Task
		SWITCH Task
			CASE A
				Result = Result + 1
				FALLTHRU
			CASE B
				Result = Result + 2
				BREAK
			CASE D
				Result = Result + 3
				BREAK
		ENDSWITCH
	ENDREPEAT
ENDSCRIPT
