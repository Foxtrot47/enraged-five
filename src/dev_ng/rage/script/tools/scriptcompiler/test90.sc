TYPEDEF PROC OPTIONAL_PARAM(INT extra=0)
TYPEDEF PROC OPTIONAL2_PARAM(STRING extra=NULL)

PROC OPTIONAL(INT extra = 0)
    // shuts up a warning
    extra = extra
ENDPROC

PROC OPTIONAL2(STRING extra)
    // shuts up a warning
    extra = extra
ENDPROC

SCRIPT
    OPTIONAL_PARAM fun = &OPTIONAL
    CALL fun()
    OPTIONAL2_PARAM fun2 = &OPTIONAL2
    CALL fun2("bar")
///    TERMINATE_THIS_SCRIPT()
ENDSCRIPT

