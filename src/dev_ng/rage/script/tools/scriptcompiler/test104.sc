NATIVE ENTITY_INDEX

NATIVE PED_INDEX : ENTITY_INDEX

SCRIPT
	PED_INDEX newPed
	
	// This should compile
	ENTITY_INDEX newEntity = newPed
	
	// This should not
	newPed = newEntity
ENDSCRIPT
