@echo off
if "%1"=="" %0 current.txt
if "%2"=="" %0 %1 win32_beta_2008

if not exist %2\sc_%2.exe (
	echo Missing %2 executable, build it first!
	exit/b
)

if exist %1 del %1
for %%I in (test??.sc test1??.sc) do (
	echo === %%I ===
	echo === %%I === >> %1
	%2\sc_%2.exe %%I -dis -run -globals %%~nI.sgv -depfile -emitc %%~nI.cpp -nopopups | grep -v "to execute" >> %1
	IF EXIST %%~nI.cpp_skip (
		cl /nologo /Os /LD /Zi %%~nI.cpp 
		ps3ppusnc -Os -g -Xassumecorrectsign=1 -Xassumecorrectalignment=1 -Xnotocrestore=2 --oformat=fsprx %%~nI.cpp -o %%~nI.sprx
	)
)
pause
echo ****
echo **** Full text:
echo ****
diff current.txt good.txt
echo ****
echo **** Cropped text:
echo ****
cut -c21- current.txt > current19.txt
cut -c21- good.txt > good19.txt
diff current19.txt good19.txt
pause
if NOT ERRORLEVEL 1 IF %2==win32_beta (
	echo **** Updating script compiler exe...
	rem renamed to preserve backwards compatibility
	p4 edit ..\..\..\framework\tools\bin\ScriptEditor\sc_win32_toolbeta.exe
	cp win32_beta\sc_win32_beta.exe ..\..\..\framework\tools\bin\ScriptEditor\sc_win32_toolbeta.exe
)
del *.scd *.sco *.native.txt
del current19.txt good19.txt
