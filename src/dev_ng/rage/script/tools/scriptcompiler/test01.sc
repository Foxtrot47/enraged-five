USING "rage_builtins.sch"	// Necessary for TIMERA/B, SETTIMERA/B.

/*

 

**/

 
SCRIPT
	INT Test /**/ = 10
	BOOL doExit = FALSE
	VECTOR V3 = << 21, 22, 23 >>
	FLOAT __SCALE_VALUE = 1
	WHILE ! doExit
		V3.y = V3.y +@ 3.14
		Test = Test - 3 * 4 / 2
		SETTIMERA (100 )
		__SCALE_VALUE = __SCALE_VALUE * 2.5555
		PRINTSTRING ("Mag = ")
		PRINTFLOAT (VMAG(V3))
		PRINTFLOAT (1)
		PRINTSTRING ("\n")
		doExit = (__Scale_Value > 50)
		WAIT (100)
	ENDWHILE
ENDSCRIPT
