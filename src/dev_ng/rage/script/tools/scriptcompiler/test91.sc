
// Demonstrates difference between SWITCH and IF blocks.

// There may be a good reason why the switch statement below is not permitted to compile, and my contrival may not reflect that.
// I just tried to create logically-equivalent functions.

FUNC INT DO_SOMETHING_SWITCHY(INT iArgument)
	SWITCH iArgument
		CASE 0
			RETURN 16
		CASE 1
			RETURN 32
		DEFAULT
			RETURN 64
	ENDSWITCH
	
	// Need to comment this in for this script to compile.
	//RETURN 128
ENDFUNC

FUNC INT DO_SOMETHING_IFFY(INT iArgument)
	IF iArgument = 0
		RETURN 16
	ELIF iArgument = 1
		RETURN 32
	ELSE
		RETURN 64
	ENDIF
	
	// No need to comment this in; the script will compile without.
	//RETURN 128
ENDFUNC

SCRIPT
	WHILE(TRUE)
		DO_SOMETHING_SWITCHY(6)
		DO_SOMETHING_IFFY(6)
	ENDWHILE
ENDSCRIPT
