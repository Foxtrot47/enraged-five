USING "rage_builtins.sch"

SCRIPT
	INT I
	TEXT_LABEL short_text_label
	TEXT_LABEL_31 long_text_label = "testing"

	PRINTSTRING (long_text_label)
	PRINTNL ()

	FOR I = 0 TO 1000
		short_text_label = long_text_label	
	ENDFOR
	PRINTSTRING (short_text_label)
	PRINTNL ()

	FOR I = 0 TO 1000
		long_text_label = short_text_label	
	ENDFOR

	PRINTSTRING (long_text_label)
	PRINTNL ()
ENDSCRIPT
