USING "rage_builtins.sch"

CONST_INT FooBar 1

#if defined FooBar
PROC FOO()
	PRINTSTRING ("FooBar is defined\n")
ENDPROC
#endif

#if not defined FooBar
PROC FOO()
	PRINTSTRING ("FooBar is NOT defined\n")
ENDPROC
#endif


SCRIPT
// #if NOT FooBar
	FOO()
// #endif

//#if FooBaz
//	BAZ()
//#endif
ENDSCRIPT
