FORWARD STRUCT SPECIFIC_SYNC_DATA

STRUCT SYNC_TEST_DATA
	INT m_test
	// This should not compile:
	SPECIFIC_SYNC_DATA m_SpecificTest
ENDSTRUCT

STRUCT SPECIFIC_SYNC_DATA
	INT m_test
ENDSTRUCT

SCRIPT
	SYNC_TEST_DATA foo
ENDSCRIPT
