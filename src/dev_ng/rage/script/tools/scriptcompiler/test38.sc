USING "rage_builtins.sch"

PROC dump_array(STRING &d[])
	INT I
	FOR I=0 TO COUNT_OF(d)-1
		PRINTSTRING(d[i])
	ENDFOR
ENDPROC

SCRIPT
	STRING S[3]
	S[0] = "one "
	S[1] = "two "
	S[2] = "three"
	dump_array(S)
ENDSCRIPT
