SCRIPT
	INT A
	INT B

	IF 1 = 1
		A = 1
	ELIF 2 = 2
		A = 2
		IF 5 = 5
			B = 1
		ELSE
			B = 2
		ENDIF
	ELSE
		A = 3
	ENDIF

	A = 0

	IF 1 = 1
		A = 4
	ENDIF

	A = 0

	IF 1 = 1
		A = 5
	ELSE
		A = 6
	ENDIF

ENDSCRIPT
