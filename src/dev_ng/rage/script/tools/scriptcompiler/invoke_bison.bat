@echo off

set BISON_PATH=.\usr\local\bin\bison.exe
set BISON_SIMPLE=.\usr\local\lib\bison.simple

if not exist %BISON_PATH% echo Missing bison install at %BISON_PATH%.  See scriptcompiler directory.
echo Translating %1.y to %1.tab.cpp . . .
if not exist \tmp mkdir \tmp
echo %BISON_PATH% -d -p %1_ -dv %1.y
%BISON_PATH% -d -p %1_ -dv %1.y
echo move %1.tab.c %1.tab.cpp
move %1.tab.c %1.tab.cpp
