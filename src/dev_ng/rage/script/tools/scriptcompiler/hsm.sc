USING "rage_builtins.sch"

STRING s_CurState = "ROOT"

PROC PrintTrans( STRING srctate,STRING dststate )
	PRINTSTRING(s_CurState)
	PRINTSTRING(": ")
	PRINTSTRING(srctate)
	PRINTSTRING("->")
	PRINTSTRING(dststate)
	PRINTNL()
	s_CurState = dststate
ENDPROC

PROC PrintActivate( STRING statname )
	PRINTSTRING(statname) PRINTSTRING(": ACTIVATE\n")
	s_CurState = statname
ENDPROC

PROC PrintDeactivate( STRING statname )
	PRINTSTRING(statname) PRINTSTRING(": DEACTIVATE\n")
ENDPROC

ACTIVATE
	PrintActivate("ROOT")
ENDACTIVATE

EVENT GotoS0()
	PrintTrans( "ROOT", "S0" )
	GOTOSTATE S0
ENDEVENT

EVENT GotoS0_S0()
	PrintTrans( "ROOT", "S0_S0" )
	GOTOSTATE S0_S0
ENDEVENT

EVENT GotoS0_S0_S0()
	PrintTrans( "ROOT", "S0_S0_S0" )
	GOTOSTATE S0_S0_S0
ENDEVENT

EVENT GotoS1()
	PrintTrans( "ROOT", "S1" )
	GOTOSTATE S1
ENDEVENT

EVENT GotoS2()
	PrintTrans( "ROOT", "S2" )
	GOTOSTATE S2
ENDEVENT

DEFAULTSTATE S0_S0_S0

STATE S0
	DEFAULTSTATE S0_S0

	ACTIVATE
		PrintActivate("S0")
	ENDACTIVATE

	DEACTIVATE
		PrintDeactivate("S0")
	ENDDEACTIVATE

	EVENT GotoS0()
		PrintTrans( "S0", "S0" )
		GOTOSTATE S0
	ENDEVENT

	EVENT GotoS0_S0_S0()
		PrintTrans( "S0", "S0_S0_S0" )
		GOTOSTATE S0_S0_S0
	ENDEVENT

	STATE S0_S0
		ACTIVATE
			PrintActivate("S0_S0")
		ENDACTIVATE

		DEACTIVATE
			PrintDeactivate("S0_S0")
		ENDDEACTIVATE

		STATE S0_S0_S0
			ACTIVATE
				PrintActivate("S0_S0_S0")
			ENDACTIVATE

			DEACTIVATE
				PrintDeactivate("S0_S0_S0")
			ENDDEACTIVATE

			EVENT GotoS0()
				PrintTrans( "S0_S0_S0", "S0" )
				GOTOSTATE S0
			ENDEVENT
		ENDSTATE
	ENDSTATE
ENDSTATE

STATE S1
	ACTIVATE
		PrintActivate("S1")
	ENDACTIVATE

	DEACTIVATE
		PrintDeactivate("S1")
	ENDDEACTIVATE
ENDSTATE

STATE S2
	ACTIVATE
		PrintActivate("S2")
	ENDACTIVATE

	DEACTIVATE
		PrintDeactivate("S2")
	ENDDEACTIVATE
ENDSTATE

SCRIPT
	PRINTSTRING("Calling GotoS0\n")
	GotoS0()
	PRINTSTRING("Calling GotoS0\n")
	GotoS0()
	PRINTSTRING("Calling GotoS0_S0_S0\n")
	GotoS0_S0_S0()
	PRINTSTRING("Calling GotoS0\n")
	GotoS0()
	PRINTSTRING("Calling GotoS1\n")
	GotoS1()
ENDSCRIPT
