USING "rage_builtins.sch"

INT SEED = 1

STRICT_ENUM BLARG
	ONE = 1,
	TWO = 2,
	THREE = ONE | TWO
ENDENUM
	
	

GLOBALS TRUE

GLOBALS

	INT TOTAL_CALLS = 10

ENDGLOBALS

FUNC INT BOB
	RETURN 1
ENDFUNC

PROC USELESS
	EXIT
ENDPROC

FUNC INT Random (INT N)
	SEED = SEED + 1
	RETURN SEED % N
ENDFUNC

FUNC INT Factorial (INT N)
	IF N > 1
		RETURN N * Factorial(N-1)
	ELSE
		RETURN 1
	ENDIF
ENDFUNC

PROC HelloWorld
	PRINTSTRING("Hello, world!\n")
ENDPROC
	
SCRIPT
	INT A
	PRINTSTRING("Function test is ")
	Factorial(6)
	BOB()
	A = 5
	PRINTINT(Factorial(5))
	Factorial(4)
	PRINTSTRING(".\n")
	PRINTINT(Random(5))
	PRINTINT(Random(5))
	PRINTINT(Random(5))
	PRINTINT(Random(5))
	PRINTINT(Random(5))
	HelloWorld()
ENDSCRIPT
