USING "rage_builtins.sch" 

FUNC VECTOR TEST
	RETURN << 1,2,3 >>
ENDFUNC

SCRIPT
	// Verify that the stack get properly cleaned up in both cases!
	START_NEW_SCRIPT("missing",0)
	TEST()
ENDSCRIPT
