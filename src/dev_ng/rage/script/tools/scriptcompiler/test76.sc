USING "rage_builtins.sch"

STRUCT TEST1
	INT X = 99
	BOOL Y = TRUE
	FLOAT Z = 3.14
ENDSTRUCT

SCRIPT
	TEST1 T[2]
	INT I
	FOR I = 0 TO COUNT_OF (T) - 1
		PRINTINT(T[I].X)
		PRINTNL()
		IF T[I].Y
			PRINTSTRING("TRUE")
		ELSE
			PRINTSTRING("FALSE")
		ENDIF
		PRINTNL()
		PRINTFLOAT(T[I].Z)
		PRINTNL()
	ENDFOR
ENDSCRIPT
