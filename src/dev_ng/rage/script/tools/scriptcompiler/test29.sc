USING "rage_builtins.sch"

ENUM gw_enum
	GW_ONE,
	GW_TWO
ENDENUM

ENUM bad
	worse
ENDENUM

INT  gw_test = -7
FLOAT GW_TEST_FLOAT = -1.23
FLOAT GW_TEST_FLOAT2 = -1

gw_enum gw_test_enum = GW_TWO

VECTOR gw_vec = <<1.23, 1.45, 45.9>>

// STRING gw_string = "asdlajdfliud"


SCRIPT
	PRINTINT(gw_test)
	PRINTNL()
	PRINTFLOAT(gw_test_float)
	PRINTNL()
	PRINTFLOAT(gw_test_float2)
	PRINTNL()
	PRINTVECTOR(gw_vec)
	PRINTNL()
ENDSCRIPT
