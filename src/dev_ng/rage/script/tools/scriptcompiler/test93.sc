// test short circuit
USING "rage_builtins.sch"

PROC BETTER_TEST(INT A)

	IF A <> 3
	ANDALSO A <> 4
	ANDALSO A <> 5
		PRINTLN("Not 3,4,or 5")
	ELSE
		PRINTLN("Is 3,4,or 5")
	ENDIF
ENDPROC

SCRIPT
	BOOL A, B, C
	INT D

	IF A ANDALSO B
		C = TRUE
	ENDIF

	IF A ORELSE B
		C = FALSE
	ENDIF
	
	BETTER_TEST(3)
	BETTER_TEST(4)
	BETTER_TEST(5)
	BETTER_TEST(6)
ENDSCRIPT
