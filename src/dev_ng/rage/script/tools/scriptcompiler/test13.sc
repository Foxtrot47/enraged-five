USING "rage_builtins.sch"

PROC Test(INT a, INT b)
	PRINTINT(a)
	PRINTSTRING(" ")
	PRINTINT(b)
	PRINTNL()
ENDPROC

PROC JoinPosse(STRING foo, INT s)
	Test(33,s)
ENDPROC

SCRIPT
	JoinPosse("aitest_npc_ally1",1+3)
ENDSCRIPT
