USING "rage_builtins.sch"

SCRIPT
	// there was a bug where attempting to ASSIGN_TEXT_LABEL_STRING
	// or APPEND_TEXT_LABEL_STRING a "null" string would cause the
	// instruction stream to be corrupted.
	STRING bad
	TEXT_LABEL_23 foo[3]

	INT X

	REPEAT 3 X
		PRINTINT(x)
		PRINTNL()
		foo[x] = bad
		PRINTSTRING(foo[x])
	ENDREPEAT

	// there was another bug where not referencing the loop counter
	// would throw a warning.  sometimes you just need to do something
	// N times, so it shouldn't warn then
	INT I
	REPEAT 3 I
	ENDREPEAT
	
	// same here (but this already worked properly before)
	INT J
	FOR J = 1 TO 3
	ENDFOR
ENDSCRIPT
