// 
// scriptcompiler/allocator.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SCRIPT_ALLOCATOR_H
#define SCRIPT_ALLOCATOR_H

#include <string.h>

namespace rage {

class scrAllocator {
public:
	static void SetHeap(int heapSize);

	static void* Allocate(size_t size);

	static int GetCurrentHeapSize() { return sm_HeapOffset; }
	static int GetMaxHeapSize() { return sm_HeapSize; }

private:
	static void *sm_Heap;
	static int sm_HeapSize;
	static int sm_HeapOffset;
	static int sm_HeapMapped;
};

class HeapString {
public:
	HeapString(const char *s)
	{
		if (s)
			strcpy(m_String = (char*)scrAllocator::Allocate(strlen(s)+1),s);
		else
			m_String = NULL;
	}
	void operator =(const char *s) 
	{
		if (s == m_String)
			/* do nothing*/;
		else if (s)
			strcpy(m_String = (char*)scrAllocator::Allocate(strlen(s)+1),s);
		else
			m_String = NULL;
	}
	operator const char *() const { return m_String; }
	const char *c_str() const { return m_String; }
	char *m_String;
};

}	// namespace rage

#endif		// SCRIPT_ALLOCATOR_H
