NATIVE ENTITY_INDEX
NATIVE VEHICLE_INDEX : ENTITY_INDEX
NATIVE PED_INDEX : ENTITY_INDEX

NATIVE PROC DELETE_ENTITY(ENTITY_INDEX ent)
NATIVE PROC DELETE_ENTITY_REF(ENTITY_INDEX &ent)
NATIVE PROC DELETE_PED(PED_INDEX ped)
NATIVE PROC DELETE_PED_REF(PED_INDEX &ped)
NATIVE PROC DELETE_VEHICLE(VEHICLE_INDEX veh)
NATIVE PROC DELETE_VEHICLE_REF(VEHICLE_INDEX &veh)


SCRIPT
	PED_INDEX bus_driver

	DELETE_ENTITY(bus_driver)		//	Expect this to compile
	DELETE_ENTITY_REF(bus_driver)	//  DO NOT Expect this to compile now with stricter rules.
	
	DELETE_PED(bus_driver)			//	Expect this to compile
	DELETE_PED_REF(bus_driver)		//	Expect this to compile
	
	DELETE_VEHICLE(bus_driver)		//	Expect this to give "Error:Type mismatch on parameter #1"
	DELETE_VEHICLE_REF(bus_driver)	//	Expect this to give "Error:Type mismatch on parameter #1"
	
ENDSCRIPT
