#define USE_STOCK_ALLOCATOR

#include "atl/string.h"
#include "system/ipc.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timer.h"
#include "file/asset.h"
#include "file/device.h"
#include "system/hangdetect.h"

#include "node.h"
#include "script_platform.h"
#include "script/hash.h"
#include "script/program.h"
#include "script/thread.h"

#include <math.h>
#include <setjmp.h>
#include <stdlib.h>

#if __TOOL
#error This isn't a tool any longer.
#endif

using namespace rage;

CompileTimeAssert(OP_FIRST_INVALID == 131);

eScriptPlatform scriptPlatform = SCRIPT_PLATFORM_UNINITIALIZED;

extern int scr_parse();
extern fiStream *scr_stream, *scr_debuginfo, *scr_globalinfo, *scr_native_file, *scr_dep_file, *c_stream;
fiStream *dis_file_stream = NULL;
extern fiStream *scr_globalreffile, *scr_structreffile, *scr_stringLiteralsFile;

u32 globalHashesToOutputToDebugFile[scrProgram::MAX_GLOBAL_BLOCKS];
bool bAtLeastOneVariableInGlobalBlockIsReferenced[scrProgram::MAX_GLOBAL_BLOCKS];
fiStream *scr_global_block_signature_file = NULL;

fiStream *scr_global_block_usage_file = NULL;
extern char scr_stream_name[RAGE_MAX_PATH], scr_global_name[RAGE_MAX_PATH];
extern u32 scr_stream_name_hash;
extern void scr_using(const char*);
extern void scr_using_close_all(void);
extern void scr_add_path(const char*);
extern bool scr_had_warnings;
extern int scr_arg_struct_size;
extern int g_SaveGlobalsBlock;
extern atString extra_dep;
extern int InstructionsEmitted, InstructionsSaved, PaddingEmitted, Potential, MaxDist, MaxDistPC;

#define __ALLOW_RUNNING_OF_SCRIPTS_WITHIN_SCRIPT_COMPILER	(0)

PARAM(stringHeapSize, "Override the size of the string heap");
PARAM(nodeHeapSize, "Override the size of the node heap");

PARAM(debugparser,"Turn on parser debug spew");
PARAM(dump,"Dump thread state during execution");
PARAM(run, "Run the script after compilation.  Optionally pass number of instructions to execute.");
PARAM(dis, "Print the script disassembly");
PARAM(dis_file, "Output the script disassembly to a text file");
PARAM(shortcircuit,"AND and OR short-circuit by default");
PARAM(include, "Include this file automatically before compilation");
PARAM(ipath, "Specify a semicolon-delimited list of places to look for include files");
PARAM(output, "Specify an output path for compiled script");
PARAM(globals, "Specify output file for global variables (for script console)");
PARAM(depfile, "Generate dependency file to specified name");
PARAM(native, "Generate text file containing native hashcodes (for debugging)");
PARAM(output_path_for_scd_files, "By default, the scd files are written to the same folder as the sco files. Use this to specify a different folder for the scd files");
PARAM(nodebug, "Do not generate debuginfo file (for lack of debugging)");
PARAM(emitc, "Emit C version of code to file");
PARAM(dont_emit_preamble,"When running with -emitc, don't output the list of globals and native commands");
PARAM(emitc_skip_unnecessary_comments, "When running with -emitc, use this to minimize unimportant differences when comparing two C files");
PARAM(emitc_skip_global_variable_names, "When running with -emitc, use this to avoid differences when comparing two C files where we've changed the name of a global variable e.g. MPGlobalsHud to MPGlobalsHud_OLD. The global index is still output as a comment");
PARAM(emitc_skip_enum_names, "When running with -emitc, use this to avoid differences when comparing two C files where we've changed the name of an enum value. The numerical value is still output");
PARAM(compilec, "Compile C version of code to Sony PRX");
PARAM(dumpopcodes, "Dump opcode stream to stdout (for small test cases)");
PARAM(globalreffile, "Append all global references to this file");
PARAM(global_block_usage_file, "Append all globals (that belong to a block other than block 0) to this file");
PARAM(global_block_signature_file, "Output the name of the script followed by the global signatures of the blocks it includes");
PARAM(structreffile, "Append all struct references to this file");
PARAM(literalstringfile, "Append all literal strings to this file");
PARAM(PlatformName, "specifies the target platform e.g. PS5");
namespace rage { XPARAM(aeskey); }
PARAM(rc, "Specify path to resource compiler");
// PARAM(continue,"Continue on errors when possible (may lead to crashes)");
char outname[RAGE_MAX_PATH];
char namespace_name[RAGE_MAX_PATH];

namespace rage {

const TelemetryCallbacks & GetTelemetryCallbacks()
{
	static TelemetryCallbacks stubs;
	return stubs;
}

}

// http://aggregate.org/MAGIC/
static inline u32 CeilPow2(u32 x)
{ 
	--x; x |= x >> 1; x |= x >> 2; x |= x >> 4; x |= x >> 8; x |= x >> 16; return x+1; 
}

// Based on FixPath() in asset.cpp
static void FixPath(char *dest,int destSize,const char *src, bool trailingSlash = true) {
	safecpy(dest,src,destSize);
	if (dest[0] == 0)
		return;

	int sl = StringLength(dest);
	char *path = dest;

	// Convert to expected slash type
	while (*path) {
		if (*path == '\\')
			*path = '/';
		path++;
	}

	// Tack on trailing appropriate slash if necessary
	if (trailingSlash && sl && ((sl+2) < destSize) && dest[--sl] != '/') {
		dest[++sl] = '/';
		dest[++sl] = 0;
	}
}


int Main() {
	if (sysParam::GetArgCount() == 1)
		Quitf("Usage: scriptcompiler script-name [-include filename] [-ipath include-path]");

	HANG_DETECT_SAVEZONE_ENTER();

	// Displayf(sysParam::GetArg(1));

	// Enable this to print the command line; useful when attempting to grab command line for debugging.
	/* for (int i=0; i<sysParam::GetArgCount(); i++)
		printf("%s\n",sysParam::GetArg(i)); */

	int sizeOfStringHeap = 2 * 1024 * 1024;
	int scrNodeHeapSize = 256 * 1024 * 1024;

	PARAM_stringHeapSize.Get(sizeOfStringHeap);
	PARAM_nodeHeapSize.Get(scrNodeHeapSize);

	scrNode::InitClass(sizeOfStringHeap);
#if __ALLOW_RUNNING_OF_SCRIPTS_WITHIN_SCRIPT_COMPILER
	scrThread::InitClass();
	scrThread::RegisterBuiltinCommands();
	scrThread::AllocateThreads(20);
#endif	//	__ALLOW_RUNNING_OF_SCRIPTS_WITHIN_SCRIPT_COMPILER

	scr_stream = fiStream::Open(sysParam::GetArg(1));
	safecpy(scr_stream_name,sysParam::GetArg(1),sizeof(scr_stream_name));
	scr_stream_name_hash = scrComputeHash(scr_stream_name);

	const char* platform_string = NULL;
	if (PARAM_PlatformName.Get(platform_string))
	{
		if (strcmp(platform_string, "Win64") == 0)
		{
			scriptPlatform = SCRIPT_PLATFORM_WIN64;
		}
		else if (strcmp(platform_string, "PS4") == 0)
		{
			scriptPlatform = SCRIPT_PLATFORM_PS4;
		}
		else if (strcmp(platform_string, "PS5") == 0)
		{
			scriptPlatform = SCRIPT_PLATFORM_PS5;
		}
		else if (strcmp(platform_string, "XboxOne") == 0)
		{
			scriptPlatform = SCRIPT_PLATFORM_XB1;
		}
		else if (strcmp(platform_string, "XBSX") == 0)
		{
			scriptPlatform = SCRIPT_PLATFORM_XBSX;
		}
	}

	if (PARAM_globals.Get()) {
		// Don't create the file unless we actually encounter globals.
		// Makes it easier to just leave the switch on all the time.
		const char *globalsName;
		PARAM_globals.Get(globalsName);
		strcpy(scr_global_name,globalsName);
	}

	// get the file path, and add that as the first place to search for includes.
	char filePath[1024];
	ASSET.RemoveNameFromPath(filePath,sizeof(filePath),sysParam::GetArg(1));
	if (strcmp(filePath,sysParam::GetArg(1)))
		scr_add_path(filePath);
	else
		scr_add_path(".");

	// try to deduce the correct AES key to use based on the source directory
	if (!PARAM_aeskey.Get() && sysParam::GetArg(1)[1]==':')
		PARAM_aeskey.Set(sysParam::GetArg(1));

	const char *ipath;
	if (PARAM_ipath.Get(ipath)) {
		char *pathbuf = (char*)alloca(strlen(ipath)+1);
		strcpy(pathbuf,ipath);
		char *pb = pathbuf;
		while ((pb= strtok(pb, ";")) != NULL) {
			scr_add_path(pb);
			pb = NULL;
		}
	}

	const char* suppliedOutname;

	if(PARAM_output.Get(suppliedOutname))
		strcpy(outname, suppliedOutname);
	else
		strcpy(outname,sysParam::GetArg(1));

	char* dotStrPos=strrchr(outname,'.');
	if (dotStrPos==NULL)
	{
		Quitf("Input filename not valid.\nUsage: scriptcompiler script-name [-include filename] [-ipath include-path]");
	}

	*dotStrPos = '\0';

	strcpy(namespace_name, ASSET.FileName(outname));

	for(u32 i = 0; namespace_name[i]; i++)
	{
		if ( (namespace_name[i] >= 'A') && (namespace_name[i] <= 'Z') )
		{
			namespace_name[i] = namespace_name[i] + 32;
		}
	}

	char name_of_c_file[RAGE_MAX_PATH];
	ASSET.FullPath(name_of_c_file,sizeof(name_of_c_file),outname,"cpp");
	const char *cname = name_of_c_file;
	//	const char *cname = "a_out.cpp";
	if (PARAM_emitc.Get()) {
		PARAM_emitc.GetParameter(cname);
		c_stream = fiStream::Create(cname);
	}

	if (c_stream) {
		if (!PARAM_emitc_skip_unnecessary_comments.Get())
		{
			fprintf(c_stream,"/* generated by rage script compiler from '%s', do not edit */\r\n\r\n",sysParam::GetArg(1));
		}
		fprintf(c_stream,"namespace %s { struct script_statics; }\r\n",namespace_name);
	}

	if (PARAM_depfile.Get()) {
		char namebuf[256];
		safecpy(namebuf,scr_stream_name,sizeof(namebuf));
		if (strrchr(namebuf,'.'))
			strcpy(strrchr(namebuf,'.'),".d");
		const char *name = namebuf;
		PARAM_depfile.GetParameter(name);
		ASSET.CreateLeadingPath(name);
		scr_dep_file = fiStream::Create(name);
		if (scr_dep_file)
			fprintf(scr_dep_file, "%s.sco %s: \\\r\n%s \\\r\n",outname,name,scr_stream_name);
	}

	ASSET.CreateLeadingPath(outname);
	if (!PARAM_nodebug.Get())
		scr_debuginfo = ASSET.Create(outname,"scd");

	if (PARAM_native.Get()) {
		const char* name = outname;
		PARAM_native.GetParameter(name);
		ASSET.CreateLeadingPath(name);
		scr_native_file = ASSET.Create(name,"native.txt");
	}

	if (PARAM_globalreffile.Get()) {
		const char *name = "c:\\global-references.txt";
		PARAM_globalreffile.GetParameter(name);
		scr_globalreffile = fiStream::Open(name,false);
		if (scr_globalreffile)
			scr_globalreffile->Seek(scr_globalreffile->Size());
		else
			scr_globalreffile = fiStream::Create(name);
	}

	if (PARAM_global_block_usage_file.Get()) {
		const char *name = "c:\\global-block-usage.txt";
		PARAM_global_block_usage_file.GetParameter(name);
		scr_global_block_usage_file = fiStream::Open(name,false);
		if (scr_global_block_usage_file)
			scr_global_block_usage_file->Seek(scr_global_block_usage_file->Size());
		else
			scr_global_block_usage_file = fiStream::Create(name);
	}

	if (PARAM_global_block_signature_file.Get()) {
		const char* name = outname;
		PARAM_global_block_signature_file.GetParameter(name);
		ASSET.CreateLeadingPath(name);
		scr_global_block_signature_file = ASSET.Create(name,"global_block.txt");

		for (u32 block_init_loop = 0; block_init_loop < scrProgram::MAX_GLOBAL_BLOCKS; block_init_loop++)
		{
			globalHashesToOutputToDebugFile[block_init_loop] = 0;
			bAtLeastOneVariableInGlobalBlockIsReferenced[block_init_loop] = false;
		}

// 		const char *name = "c:\\global-block-signature.txt";
// 		PARAM_global_block_signature_file.GetParameter(name);
// 		scr_global_block_signature_file = fiStream::Open(name,false);
// 		if (scr_global_block_signature_file)
// 			scr_global_block_signature_file->Seek(scr_global_block_signature_file->Size());
// 		else
// 			scr_global_block_signature_file = fiStream::Create(name);
	}

	if (PARAM_dis_file.Get())
	{
		char name_of_dis_file[RAGE_MAX_PATH];
		ASSET.FullPath(name_of_dis_file,sizeof(name_of_dis_file),outname,"dis.txt");
		const char *pDisFilename = name_of_dis_file;
		PARAM_dis_file.GetParameter(pDisFilename);
		if (pDisFilename)
		{
			dis_file_stream = fiStream::Create(pDisFilename);
		}
	}

	if (PARAM_structreffile.Get()) {
		const char *name = "c:\\struct-references.txt";
		PARAM_structreffile.GetParameter(name);
		scr_structreffile = fiStream::Open(name,false);
		if (scr_structreffile)
			scr_structreffile->Seek(scr_structreffile->Size());
		else
			scr_structreffile = fiStream::Create(name);
	}

	if (PARAM_literalstringfile.Get())
	{
		const char *name = "c:\\script-literal-strings.txt";
		PARAM_literalstringfile.GetParameter(name);
		scr_stringLiteralsFile = fiStream::Open(name,false);
		if (scr_stringLiteralsFile)
			scr_stringLiteralsFile->Seek(scr_stringLiteralsFile->Size());
		else
			scr_stringLiteralsFile = fiStream::Create(name);
	}

	extern int scr_debug;
	if (PARAM_debugparser.Get())
		scr_debug = 1;

	bool gotError=false;

	int programSize = 0;

	if (scr_stream) {
		Displayf("compiling '%s'...",sysParam::GetArg(1));

		const char *include;
		if (PARAM_include.Get(include))
			scr_using(include);

		// char *nodeHeap = rage_new char[scrNodeHeapSize];
		scrAllocator::SetHeap(scrNodeHeapSize);

#pragma warning(disable: 4611)

		extern jmp_buf error_jump;
		try {
			if (scr_debuginfo)
			{
				fprintf(scr_debuginfo,"[VERSION]\r\n4\r\n");
				fprintf(scr_debuginfo,"[VARIABLES]\r\n");
			}
			scr_parse();
		
			if (!scr_had_warnings && scrNode::GetRoot()) {
				scrNode::GetRoot()->Emit();

				if (scr_had_warnings)
					throw "semantic error in compile";

				if (scr_debuginfo)
				{
					// fprintf(scr_debuginfo,"PROC,NULL,*END,%d\r\n",scrNode::GetPC());

					fprintf(scr_debuginfo,"[LINEINFO]\r\n");
					fprintf(scr_debuginfo,"%d\r\n",scrNode::sm_LineInfo.GetCount());

					qsort(&scrNode::sm_LineInfo[0],scrNode::sm_LineInfo.GetCount(),sizeof(scrNode::LineInfo),(int(*)(const void*,const void*))scrNode::LineInfo::cmp);
					for (int i=0; i<scrNode::sm_LineInfo.GetCount(); i++) {
						fprintf(scr_debuginfo,"%d:%d:%d\r\n",scrNode::sm_LineInfo[i].m_BasePc,scrNode::sm_LineInfo[i].m_LineNumber,scrNode::sm_LineInfo[i].m_FileIndex);
					}
					fprintf(scr_debuginfo,"%d\r\n",scrNode::sm_FileInfo.GetCount());
					for (int i=0; i<scrNode::sm_FileInfo.GetCount(); i++) {
						char *p, *s = const_cast<char*>(scrNode::sm_FileInfo[i].m_String);
						strlwr(s);
						while ((p = strchr(s,'\\')) != NULL)
							*p = '/';
						fprintf(scr_debuginfo,"%s\r\n",s);
					}
					fprintf(scr_debuginfo,"[EOF]\r\n");
				}

				programSize = scrNode::GetProgramSize();
				if (c_stream) {
					c_stream->Close(); 
					c_stream = NULL;
					if (PARAM_compilec.Get() && getenv("SCE_PS3_ROOT")) {
						const char *outdir = "x:\\gta5\\build\\dev";
						PARAM_compilec.GetParameter(outdir);
						char basename[256];
						ASSET.BaseName(basename,sizeof(basename),ASSET.FileName(cname));
						char cmdline[512];
						formatf(cmdline,"%s\\host-win32\\sn\\bin\\ps3ppusnc -mprx -zgenprx --oformat=fsprx -Os -Wl,--notocrestore -I x:\\gta5\\src\\dev\\rage\\script\\src\\script %s -o %s\\%s.sprx",
							getenv("SCE_PS3_ROOT"),cname,outdir,basename);
						while (strchr(cmdline,'/'))
							*strchr(cmdline,'/') = '\\';
						puts(cmdline);
						system(cmdline);
					}
				}

				if (PARAM_dumpopcodes.Get()) {
					const u8 *op = scrNode::GetProgram();
					u32 siz = scrNode::GetProgramSize();
					printf("static unsigned char opcodes[] = {");
					for (u32 i=0; i<siz; i++)
						printf("%s0x%02x,", (i&15)==0?"\n\t":"", op[i]);
					printf("\n};\n");
				}

				scrProgram::Init(outname,scrNode::GetProgram(),scrNode::GetProgramSize(),scrNode::GetNatives(),scrNode::GetNativeSize(),scrNode::GetStatics(),scrNode::GetStaticSize(),scrNode::GetGlobals(),scrNode::GetGlobalSize(),
					g_SaveGlobalsBlock!=-1?g_SaveGlobalsBlock:0,
					scrNode::GetStringHeap(),scrNode::GetStringHeapSize(),scr_arg_struct_size);

#if 0	// enable this for tracking natives used across all compiles
				fiStream *tmp = fiStream::Open("c:\\nativerefs.txt",false);
				if (!tmp) tmp = fiStream::Create("c:\\nativerefs.txt");
				else tmp->Seek64(tmp->Size64());
				for (u32 i=0; i<scrNode::GetNativeSize(); i++)
					fprintf(tmp,"0x%08x\r\n",scrNode::GetNatives()[i]);
				tmp->Close();
#endif

				scrProgram::Save(outname,g_SaveGlobalsBlock);

				scrNode::Reset();

				scrProgramId progId = scrProgram::Load(outname);

#if __ALLOW_RUNNING_OF_SCRIPTS_WITHIN_SCRIPT_COMPILER
				bool canRun = PARAM_run.Get() && scrProgram::Validate(progId);
#endif	//	__ALLOW_RUNNING_OF_SCRIPTS_WITHIN_SCRIPT_COMPILER


#if __DEV
				int pc = 0;
				int line;
				char buf[128];

				if (PARAM_dis.Get() || dis_file_stream) {
					while ((line = scrNode::GetLineInfo(pc)), ((pc = scrThread::Disassemble(buf,progId,pc)) < programSize))
					{
						if (dis_file_stream)
						{
							fprintf(dis_file_stream,"%s\r\n",buf);
						}
						else
						{
							Displayf("Line %4d: %s",line,buf);
						}
					}

					if (dis_file_stream)
					{
						fprintf(dis_file_stream,"%s\r\n",buf);
					}
					else
					{
						Displayf("Line %4d: %s",line,buf);
					}
				}
#endif

				if (dis_file_stream)
				{
					dis_file_stream->Close();
					dis_file_stream = NULL;
				}

#if __ALLOW_RUNNING_OF_SCRIPTS_WITHIN_SCRIPT_COMPILER
				if (PARAM_run.Get()) {
					if (!canRun) {
						Errorf("(Unable to run program)");
						throw "unable to run program with unknown NATIVE commands";
					}
					DEV_ONLY(scrThreadId tid =) scrThread::CreateThread(outname);
#if __DEV
					if (PARAM_dump.Get()) {
						do {
							scrThread::GetThread(tid)->DumpState(Displayf);
							sysIpcSleep(16);
						} while (scrThread::UpdateAll(1));
					}
					else 
#endif
					{
                        int numInsns;
                        if(!PARAM_run.Get(numInsns))
                        {
                            numInsns = 0;
                        }
						sysTimer T;
						scrThread::UpdateAll(numInsns);
						// Displayf("execution completed in %f ms",T.GetMsTime());
					}
				}
#endif	//	__ALLOW_RUNNING_OF_SCRIPTS_WITHIN_SCRIPT_COMPILER

				const char *rcpath, *aeskey;
				if (PARAM_rc.Get() && PARAM_rc.Get(rcpath) && PARAM_aeskey.Get(aeskey) && *rcpath) {
					char cmd[512];
					formatf(cmd,"%s %s.sco %s.xsc -aeskey %s",rcpath,outname,outname,aeskey);
					int code = system(cmd);
					if (code)
						Errorf("unable to run '%s', code %d",cmd,code);
					formatf(cmd,"%s %s.sco %s.csc -aeskey %s",rcpath,outname,outname,aeskey);
					code = system(cmd);
					if (code)
						Errorf("unable to run '%s', code %d",cmd,code);
					formatf(cmd,"%s %s.sco %s.wsc -aeskey %s",rcpath,outname,outname,aeskey);
					code = system(cmd);
					if (code)
						Errorf("unable to run '%s', code %d",cmd,code);
				}
			}
			else
				gotError=true;

			scrProgram::ShutdownClass();
		}
		catch ( char * ) {
			// ...else some sort of error, don't try to run.
			gotError=true;
			scr_using_close_all();
		}

		if (scr_stream)
			scr_stream->Close();

		// delete[] nodeHeap;
	}
	else {
		Errorf("Unable to open source file '%s'",sysParam::GetArg(1));
		gotError = true;
	}

	if (!gotError) {
		Displayf("compile succeeded, %d insns emitted, %d bytes of code (max = %d bytes), %d static words, %d string bytes (max = %d bytes), %d pad bytes, node heap size = %d bytes (max = %d bytes), number of lines = %d (max = %d), number of includes = %d (max = %d)",
			InstructionsEmitted,
			programSize, scrNode::GetMaxProgramSize(),
			scrNode::GetStaticSize(),
			scrNode::GetStringHeapSize(), scrNode::GetMaxStringHeapSize(),
			PaddingEmitted,
			scrAllocator::GetCurrentHeapSize(), scrAllocator::GetMaxHeapSize(),
			scrNode::sm_LineInfo.GetCount(), scrNode::sm_LineInfo.GetMaxCount(),
			scrNode::sm_FileInfo.GetCount(), scrNode::sm_FileInfo.GetMaxCount()	);
		if (g_SaveGlobalsBlock != -1)
			Displayf("Script '%s' contains globals block %d, %u bytes total (%u consumed, %u wasted)",sysParam::GetArg(1),g_SaveGlobalsBlock,scrNode::GetGlobalSize() * sizeof(scrValue),
			CeilPow2(scrNode::GetGlobalSize() * sizeof(scrValue)),
			CeilPow2(scrNode::GetGlobalSize() * sizeof(scrValue)) - scrNode::GetGlobalSize() * sizeof(scrValue));
	}
	else
	{
		Displayf("COMPILE FAILED, deleting intermediate files");
		char temp[RAGE_MAX_PATH];
		strcpy(temp,outname);
		strcat(temp,".sco");
		fiDevice::GetDevice(temp)->Delete(temp);
	}

	if (scr_debuginfo) {
		scr_debuginfo->Close();

		char filenameOfScdFile[RAGE_MAX_PATH];
		strcpy(filenameOfScdFile, outname);
		strcat(filenameOfScdFile, ".scd");

		char filenameOfScdFileCopy[RAGE_MAX_PATH];
		filenameOfScdFileCopy[0] = '\0';
		const char *pScdPath = NULL;
		if (PARAM_output_path_for_scd_files.Get(pScdPath))
		{
			FixPath(filenameOfScdFileCopy, RAGE_MAX_PATH, pScdPath, true);
			const char *pScdFilename = ASSET.FileName(outname);
			safecat(filenameOfScdFileCopy, pScdFilename);
			ASSET.CreateLeadingPath(filenameOfScdFileCopy);
			safecat(filenameOfScdFileCopy, ".scd");
		}

		if (gotError)
		{
			fiDevice::GetDevice(filenameOfScdFile)->Delete(filenameOfScdFile);

			if (strlen(filenameOfScdFileCopy) > 0)
			{
				// Is this the way to check that the file exists?
				if (fiDevice::GetDevice(filenameOfScdFileCopy)->GetAttributes(filenameOfScdFileCopy) != FILE_ATTRIBUTE_INVALID)
				{
					fiDevice::GetDevice(filenameOfScdFileCopy)->Delete(filenameOfScdFileCopy);
				}
			}
		}
		else
		{
			if (strlen(filenameOfScdFileCopy) > 0)
			{
				char fixedPathOfSourceFile[RAGE_MAX_PATH];
				FixPath(fixedPathOfSourceFile, RAGE_MAX_PATH, filenameOfScdFile, false);

				// Only copy the .scd file if the source and destination are different
				if (stricmp(fixedPathOfSourceFile, filenameOfScdFileCopy) != 0)
				{
					if (!fiDevice::GetDevice(filenameOfScdFile)->CopySingleFile(filenameOfScdFile, filenameOfScdFileCopy))
					{
						Displayf("Failed to copy .scd file contents to %s", filenameOfScdFileCopy);
					}
				}
			}
		}
	}
	if (c_stream) {	// error?
		c_stream->Close();
		fiDevice::GetDevice(cname)->Delete(cname);
	}
	if (scr_globalinfo) {
		scr_globalinfo->Close();
		if (gotError)
			fiDevice::GetDevice(scr_global_name)->Delete(scr_global_name);
	}
	if (scr_native_file)
		scr_native_file->Close();
	if (scr_globalreffile)
		scr_globalreffile->Close();
	if (scr_global_block_usage_file)
		scr_global_block_usage_file->Close();

	if (scr_global_block_signature_file)
	{
		fprintf(scr_global_block_signature_file,"%s",outname);

		for (u32 block_loop=0; block_loop<scrProgram::MAX_GLOBAL_BLOCKS; block_loop++)
		{
			if (bAtLeastOneVariableInGlobalBlockIsReferenced[block_loop])
			{
				fprintf(scr_global_block_signature_file,",%u=%u", block_loop, globalHashesToOutputToDebugFile[block_loop] );
			}
		}

		fprintf(scr_global_block_signature_file,"\r\n");

		scr_global_block_signature_file->Close();
	}

	if (scr_structreffile)
		scr_structreffile->Close();
	if (scr_stringLiteralsFile)
		scr_stringLiteralsFile->Close();
	if (scr_dep_file) {
		fprintf(scr_dep_file,"\r\n");
		scr_dep_file->Write(extra_dep.c_str(), extra_dep.length());
		scr_dep_file->Close();
	}

#if __ALLOW_RUNNING_OF_SCRIPTS_WITHIN_SCRIPT_COMPILER
	scrThread::ShutdownClass();
#endif	//	__ALLOW_RUNNING_OF_SCRIPTS_WITHIN_SCRIPT_COMPILER

	scrNode::ShutdownClass();

	if(gotError)
		return 1;

	return 0;
}
