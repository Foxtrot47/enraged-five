ENUM CONTACTS
	CONTACT_ONE = 0,
	CONTACT_TWO
ENDENUM

NATIVE MyHandle

 
INT contact_flag[COUNT_OF(CONTACTS)]
 
SCRIPT
 
	contact_flag[CONTACT_ONE] = 0
	contact_flag[CONTACT_TWO] = 0

	MyHandle mh = INT_TO_NATIVE(MyHandle,123)

	CONTACTS i
	INT test = ENUM_TO_INT(i)
	INT test2 = NATIVE_TO_INT(mh)
	

	FOR i = CONTACT_ONE TO CONTACT_TWO
		contact_flag[i] = 0
	ENDFOR
 
ENDSCRIPT
