using "rage_builtins.sch"

script
	int table[5][3]
	int i, j

	for i=0 to count_of(table)-1
		for j=0 to count_of(table[])-1
			table[i][j] = i * 10 + j
		endfor
	endfor

	for j=0 to count_of(table[])-1
		for i=0 to count_of(table)-1
			printint (table[i][j])
			printstring ("=")
			printint (i*10+j)
			printstring (" ")
		endfor
		printstring ("\n")
	endfor
endscript
