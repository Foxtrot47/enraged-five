USING "test3435.sch"
USING "rage_builtins.sch"

SCRIPT
	MyArg args
	args.Pos = << 1, 2, 3>>
	args.Name = "Mailbox"
	START_NEW_SCRIPT_WITH_ARGS("test34",args,SIZE_OF(args),0)
ENDSCRIPT
