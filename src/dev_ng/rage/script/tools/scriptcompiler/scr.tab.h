typedef union {
	bool bval;
	int ival;
	float fval;
	const char *sval;
	scrNodeStmt *stmt;
	scrNodeExpr *expr;
	struct {
		struct scrSym **sym;
		u32 hash;
	} newsymhash;
	scrDataList *datalist;
	struct scrSymVar *varref;
	const scrField *field;
	const scrType *type;
	const scrTypeList *typelist;
	scrCommandInfo *cmd;
	scrExprList *exprlist;
	scrValue value;
	scrTypeEnum::Enumerant *eval;
	int lineno;
	class scrNodeLabel *label;
	int enumtype;
} YYSTYPE;
#define	IF	258
#define	ELSE	259
#define	ELIF	260
#define	ENDIF	261
#define	WHILE	262
#define	ENDWHILE	263
#define	REPEAT	264
#define	ENDREPEAT	265
#define	SCRIPT	266
#define	ENDSCRIPT	267
#define	RETURN	268
#define	EXIT	269
#define	ENDPROC	270
#define	ENDFUNC	271
#define	AND	272
#define	OR	273
#define	NOT	274
#define	GE	275
#define	LE	276
#define	NE	277
#define	LSH	278
#define	RSH	279
#define	FALLTHRU	280
#define	FOR	281
#define	FORWARD	282
#define	ENDFOR	283
#define	TO	284
#define	STEP	285
#define	TYPEDEF	286
#define	PLUS_TIME	287
#define	MINUS_TIME	288
#define	STRLIT	289
#define	USING	290
#define	CATCH	291
#define	THROW	292
#define	CALL	293
#define	TYPENAME	294
#define	FUNCNAME	295
#define	PROCNAME	296
#define	INTLIT	297
#define	FLOATLIT	298
#define	NEWSYM	299
#define	VAR_REF	300
#define	FIELD	301
#define	TRUE	302
#define	FALSE	303
#define	LABEL	304
#define	SWITCH	305
#define	ENDSWITCH	306
#define	CASE	307
#define	DEFAULT	308
#define	BREAK	309
#define	GOTO	310
#define	SCOPE	311
#define	ENDSCOPE	312
#define	CONST_INT	313
#define	CONST_FLOAT	314
#define	FUNC	315
#define	PROC	316
#define	NATIVE	317
#define	GLOBALS	318
#define	ENDGLOBALS	319
#define	ENUM	320
#define	ENDENUM	321
#define	ENUMLIT	322
#define	STRUCT	323
#define	ENDSTRUCT	324
#define	NL	325
#define	PLUSPLUS	326
#define	MINUSMINUS	327
#define	PLUS_EQ	328
#define	MINUS_EQ	329
#define	TIMES_EQ	330
#define	DIVIDE_EQ	331
#define	COUNT_OF	332
#define	ENUM_TO_INT	333
#define	INT_TO_ENUM	334
#define	SIZE_OF	335
#define	AND_EQ	336
#define	OR_EQ	337
#define	XOR_EQ	338
#define	NATIVE_TO_INT	339
#define	INT_TO_NATIVE	340
#define	HASH	341
#define	DEBUGONLY	342
#define	VARARGS	343
#define	STRICT_ENUM	344
#define	HASH_ENUM	345
#define	STRICT_HASH_ENUM	346
#define	TWEAK_INT	347
#define	TWEAK_FLOAT	348
#define	ANDALSO	349
#define	ORELSE	350


extern YYSTYPE scr_lval;
