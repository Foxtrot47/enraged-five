NATIVE VOLUME

// This used to cause a type mismatch due to the default parameter
// not properly forwarding IsObject.
PROC DO_STUFF(VOLUME vol = NULL)
	vol = vol
ENDPROC

SCRIPT
	DO_STUFF()

	DO_STUFF(NULL)
ENDSCRIPT
