USING "test3435.sch"
USING "rage_builtins.sch"

SCRIPT(MyArg args)
	PRINTSTRING("Name = ")
	PRINTSTRING(args.Name)
	PRINTSTRING("\nPos.y = ")
	PRINTFLOAT(args.Pos.y)
	PRINTNL()
ENDSCRIPT
