USING "rage_builtins.sch"

ENUM VEHICLE_MODEL_NAMES
      BANSHEE=-1041692462,
      BENSON=2053223216 
ENDENUM

ENUM FRUIT
      FRUIT_APPLE,
      FRUIT_BANANA
ENDENUM

// Without the default parameter, the compiler gives proper Type mismatch errors
// With the default parameter, this file compiles which it shouldn't
PROC PrintVehicle(VEHICLE_MODEL_NAMES myVehicleModel = BANSHEE)
      printint(ENUM_TO_INT(myVehicleModel))
      PRINTNL()
ENDPROC


SCRIPT
      PrintVehicle(23)
      PrintVehicle(FRUIT_APPLE)
ENDSCRIPT

