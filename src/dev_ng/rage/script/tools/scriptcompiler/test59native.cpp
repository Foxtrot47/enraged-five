#include "csupport.h"

scrVector GET_OBJECT_POSITION(void *)
{
	return scrVector(1,2,3);
}

scrVector GET_OBJECT_POSITION_REF(void *&)
{
	return scrVector(2,3,4);
}
