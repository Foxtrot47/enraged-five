

STRUCT GenericStuff
	INT iWhoCares
ENDSTRUCT

TYPEDEF PROC CUSTOM_ROUND_FUNC(GenericStuff & sTheStruct)

PROC UPDATE_FUNC(GenericStuff & sTheStruct)
//	sTheStruct = sTheStruct
	UNUSED_PARAMETER(sTheStruct)
ENDPROC

// Globals:
CUSTOM_ROUND_FUNC fpUpdateFunc


// Script Main.
SCRIPT
	fpUpdateFunc = &UPDATE_FUNC
	
	fpUpdateFunc = NULL
	
	GenericStuff sFoo
	IF fpUpdateFunc <> NULL
		CALL fpUpdateFunc(sFoo)
	ENDIF
ENDSCRIPT
