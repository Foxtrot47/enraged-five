#ifndef SCRIPT_TYPE_H
#define SCRIPT_TYPE_H

#include "string/string.h"
#include "atl/map.h"
#include "atl/array.h"

#include "allocator.h"
#include "script/value.h"

namespace rage {

class scrType;
class fiStream;

struct scrCommandInfo;		// Sigh.

struct scrDefaultValue {
	scrDefaultValue() { Value.asInt = 0; IsFloat = false; }
	explicit scrDefaultValue(int value) { Value.asInt = value; IsFloat = false; }
	explicit scrDefaultValue(float value) { Value.asFloat = value; IsFloat = true; }
	union {
		int asInt;
		float asFloat;
	} Value;
	bool IsFloat;
};

struct scrField {
	const scrType *Type;
	const char *Name;
	int Offset;
	scrDefaultValue Default;
};

const int VariableArraySentinel = 32767;

class scrType {
public:
	scrType(const scrType *parent = NULL);
	virtual ~scrType();
	// virtual bool Assign(scrValue &dest,const scrValue &srcValue) = 0;
	// virtual bool IsReference() const = 0;

#if RAGE_ENABLE_RAGE_NEW
	static void *operator new(size_t size,const char*,int) { return scrAllocator::Allocate(size); }
#else
	static void *operator new(size_t size) { return scrAllocator::Allocate(size); }
#endif

	virtual int GetArrayCount() const { return 0; }
	virtual const scrType* GetArrayType() const { return NULL; }
	virtual const scrType* GetInnerType() const { return this; }
	virtual const scrField* LookupField(const char *) const { return NULL; }
	virtual const scrType* GetReference() const { return NULL; }
	virtual void GetName(char *dest) const = 0;
	virtual int GetBasicType() const { return scrValue::UNKNOWN; }
	// "this" is the formal parameter we're checking against; "inner" is the actual parameter
	virtual bool IsCompatibleWith(const scrType *actual) const { return this == actual; }
	virtual void Construct(scrValue *dest) const {
		int count = GetSize();
		while (count--)
			dest++->Int = 0;
	}
	// This is called with the base address of the object already on TOS
	// Local storage is already guaranteed to be zero-initialized so we only need to
	// fill in nonzero things (array sizes)
	virtual void EmitConstruct() const { }
	virtual bool NeedEmitConstruct() const { return false; }
	virtual void EmitCopy(char *s,int &/*idx*/) const { strcpy(s,"\tINVALID_TYPE_FOR_NATIVE();\r\n"); }
	virtual const char *GetValueField() const { return "result[0].Int"; }

	virtual int GetSize() const;
	virtual bool IsObject() const { return false; }
	virtual bool IsString() const { return false; }
	virtual bool ContainsStringOrFuncPtr() const { return false; }
	virtual bool IsInteger() const { return false; }
	virtual bool IsEnum() const { return false; }
	virtual bool IsTextLabel() const { return false; }
	virtual bool IsTypeDef() const { return false; }
	virtual bool HasDefaultValue(scrDefaultValue&) const { return false; }
	virtual bool IsVector() const { return false; }
	virtual bool IsVectorReference() const { return false; }
	virtual bool IsVariableSizedArray() const { return false; }
	virtual bool IsStruct() const { return false; }

	virtual void EmitDebugInfo(fiStream *S,const char *scope,const char *name,int offset) const;
	virtual void EmitGlobalInfo(fiStream *S,const char *varname) const;
	virtual void DeclareType(fiStream *) const { }

	virtual int GetEnumCount() const { return 0; }

	const scrType *GetParent() const { return Parent; }

	virtual const char *GetCType() const = 0;
	virtual const char *GetZeroType() const = 0;

	const scrType *Parent;
};

class scrTypeBool: public scrType {
public:
	static scrTypeBool Instance;
	virtual void GetName(char *dest) const { strcpy(dest,"BOOL"); }
	virtual int GetBasicType() const { return scrValue::BOOL; }
	virtual const char *GetCType() const { return "BOOL"; }
	virtual const char *GetZeroType() const { return "(FALSE)"; }
	virtual void EmitCopy(char * s,int &idx) const {
		sprintf(s,"\tdata[%d].Bool = P%d;\r\n",idx,idx);
		++idx;
	}
};

class scrTypeInt: public scrType {
public:
	static scrTypeInt Instance;
	static scrTypeInt EnumToIntInstance;
	virtual void GetName(char *dest) const { strcpy(dest,"INT"); }
	virtual int GetBasicType() const { return scrValue::INT; }
	virtual const char *GetCType() const { return "int"; }
	virtual bool IsInteger() const { return true; }
	virtual const char *GetZeroType() const { return "(0)"; }
	virtual void EmitCopy(char * s,int &idx) const {
		sprintf(s,"\tdata[%d].Int = P%d;\r\n",idx,idx);
		++idx;
	}
};

class scrTypePoolIndex: public scrType {
public:
	static scrTypePoolIndex Instance;
	virtual void GetName(char *dest) const { strcpy(dest,"POOLINDEX"); }
	virtual int GetBasicType() const { return scrValue::OBJECT; }
	virtual const char *GetCType() const { return "int"; }
	virtual bool IsInteger() const { return false; }
	virtual const char *GetZeroType() const { return "(-1)"; }
	virtual void EmitCopy(char * s,int &idx) const {
		sprintf(s,"\tdata[%d].Int = P%d;\r\n",idx,idx);
		++idx;
	}
	void Construct(scrValue *dest) const {
		dest->Int = -1;
	}
	virtual void EmitConstruct() const;
	virtual bool NeedEmitConstruct() const { return true; }
};

class scrTypeStringHash: public scrType {
public:
	static scrTypeStringHash Instance;
	virtual void GetName(char *dest) const { strcpy(dest,"STRINGHASH"); }
	virtual int GetBasicType() const { return scrValue::OBJECT; }
	virtual const char *GetCType() const { return "int"; }
	virtual bool IsInteger() const { return false; }
	virtual const char *GetZeroType() const { return "(0)"; }
	virtual void EmitCopy(char * s,int &idx) const {
		sprintf(s,"\tdata[%d].Int = P%d;\r\n",idx,idx);
		++idx;
	}
};

class scrTypeFloat: public scrType {
public:
	static scrTypeFloat Instance;
	virtual void GetName(char *dest) const { strcpy(dest,"FLOAT"); }
	virtual const char *GetCType() const { return "float"; }
	virtual int GetBasicType() const { return scrValue::FLOAT; }
	virtual const char *GetZeroType() const { return "(0.0f)"; }
	virtual void EmitCopy(char * s,int &idx) const {
		sprintf(s,"\tdata[%d].Float = P%d;\r\n",idx,idx);
		++idx;
	}
	virtual const char *GetValueField() const { return "result[0].Float"; }
};

class scrTypeDefaultValue: public scrType {
public:
	scrTypeDefaultValue(const scrType *inner,int defValue) : Inner(inner), Default(defValue) { }
	scrTypeDefaultValue(const scrType *inner,float defValue) : Inner(inner), Default(defValue) { }
	virtual void GetName(char *dest) const { Inner->GetName(dest); }
	virtual int GetBasicType() const { return Inner->GetBasicType(); }
	virtual bool HasDefaultValue(scrDefaultValue &value) const { value = Default; return true; }
	virtual bool IsInteger() const { return Inner->IsInteger(); }
	virtual bool IsEnum() const { return Inner->IsEnum(); }
	virtual bool IsObject() const { return Inner->IsObject(); }
	virtual const scrType* GetInnerType() const { return Inner; }
	virtual void EmitDebugInfo(fiStream *S,const char *scope,const char *name,int offset) const { Inner->EmitDebugInfo(S,scope,name,offset); }
	virtual void DeclareType(fiStream *S) const { Inner->DeclareType(S); }
	virtual const char *GetCType() const { return Inner->GetCType(); }
	virtual const char *GetZeroType() const { return Inner->GetZeroType(); }
	virtual void EmitCopy(char * s,int &idx) const { Inner->EmitCopy(s,idx); }
	virtual bool IsCompatibleWith(const scrType *actual) const { return Inner->GetBasicType() == actual->GetBasicType(); }
	virtual bool ContainsStringOrFuncPtr() const { return Inner->ContainsStringOrFuncPtr(); }

	const scrType *Inner;
	scrDefaultValue Default;
};

class scrTypeArray: public scrType {
public:
	scrTypeArray(const scrType *type,int count);
	~scrTypeArray();

	int GetSize() const { return 1 + Type->GetSize() * Count; }
	int GetArrayCount() const { return Count; }
	bool IsVariableSizedArray() const { return Count == VariableArraySentinel; }
	virtual const scrType* GetArrayType() const { return Type; }
	virtual void GetName(char*) const;
	virtual void EmitGlobalInfo(fiStream *S,const char *varname) const;
	virtual void DeclareType(fiStream *S) const { Type->DeclareType(S); }
	// "this" is formal (which may be variable-sized array).
	bool IsCompatibleWith(const scrType *actual) const {
		return actual->GetArrayType() && 
			(actual->GetArrayCount() == Count || GetArrayCount() == VariableArraySentinel) && GetArrayType()->IsCompatibleWith(actual->GetArrayType());
	}
	void Construct(scrValue *dest) const {
		dest++->Int = Count;
		for (int i=0; i<Count; i++) {
			Type->Construct(dest);
			dest += Type->GetSize();
		}
	}
	virtual void EmitConstruct() const;
	virtual bool NeedEmitConstruct() const { return true; }
	virtual bool ContainsStringOrFuncPtr() const { return Type->ContainsStringOrFuncPtr(); }
	virtual const char *GetCType() const { return CTypeName; }
	virtual const char *GetZeroType() const { return ""; /* already constructed empty */ }

protected:
	const scrType *Type;
	int Count;
	HeapString CTypeName;
};

class scrTypeTextLabel: public scrType {
public:
	scrTypeTextLabel(int charLen) : Count(charLen) { Assert((charLen&3)==0 && charLen <= scrMaxTextLabelSize); }
	int GetSize() const;
	virtual void GetName(char *dest) const { sprintf(dest,"TEXT_LABEL_%d",Count-1); }
	virtual int GetBasicType() const { return scrValue::TEXT_LABEL; }
	virtual bool IsTextLabel() const { return true; }
	virtual bool IsCompatibleWith(const scrType *actual) const { return actual->IsTextLabel() && Count == ((scrTypeTextLabel*)actual)->Count; }
	virtual const char *GetCType() const;
	virtual const char *GetZeroType() const { return "(\"\")"; }

protected:
	int Count;
};

class scrTypeVector: public scrType {
public:
	static scrTypeVector Instance;

	int GetSize() const;
	const scrField* LookupField(const char *name) const;
	virtual void GetName(char *dest) const { strcpy(dest,"VECTOR"); }
	virtual int GetBasicType() const { return scrValue::VECTOR; }
	virtual const char *GetCType() const { return "scrVector"; }
	virtual const char *GetZeroType() const { return ""; }
	virtual void EmitCopy(char * s,int &idx) const {
		sprintf(s,"\tdata[%d].Float = P%d.x;\r\n"
			"\tdata[%d+1].Float = P%d.y;\r\n"
			"\tdata[%d+2].Float = P%d.z;\r\n",
				idx,idx,idx,idx,idx,idx);
		idx += 3;
	}
	virtual const char *GetValueField() const { return "scrVector(result[0].Float,result[1].Float,result[2].Float)"; }
	virtual bool IsVector() const { return true; }
};

class scrTypeString: public scrType {
public:
	static scrTypeString Instance;

	virtual bool IsString() const { return true; }
	virtual bool ContainsStringOrFuncPtr() const { return true; }
	virtual void GetName(char *dest) const { strcpy(dest,"STRING"); }
	virtual int GetBasicType() const { return scrValue::STRING; }
	virtual const char *GetCType() const { return "const char *"; }
	virtual const char *GetZeroType() const { return "(0)"; }
	virtual void EmitCopy(char * s,int &idx) const {
		sprintf(s,"\tdata[%d].String = P%d;\r\n",idx,idx);
		++idx;
	}
	virtual const char *GetValueField() const { return "result[0].String"; }
};

class scrTypeEnum: public scrType {
public:
	struct Enumerant 
	{
#if RAGE_ENABLE_RAGE_NEW
		static void *operator new(size_t size,const char*,int) { return scrAllocator::Allocate(size); }
#else
		static void *operator new(size_t size) { return scrAllocator::Allocate(size); }
#endif
		
		Enumerant *Next;
		int Value;
		const char *Name;
		scrTypeEnum *Parent;
	};
	scrTypeEnum(const char *name,u8 subtype /* 0=normal, 1=strict, 2=hashenum*/) : Name(name), Subtype(subtype), m_Last(NULL), Emitted(false) { }
	virtual void GetName(char *dest) const;
	virtual int GetBasicType() const { return scrValue::INT; }
	virtual bool IsInteger() const { return !(Subtype & 1); }
	virtual bool IsEnum() const { return true; }

	Enumerant* AddEnumerant(const char *name,int value);
	bool LookupEnumerant(char *name,int &value);
	bool HasThisValue(int value) const;
	Enumerant* GetLast() const { return m_Last; }

	int GetEnumCount() const { return m_Last? m_Last->Value + 1 : 0; }
	bool IsHashEnum() const { return (Subtype & 2) != 0; }

	virtual void DeclareType(fiStream *S) const;
	virtual const char *GetCType() const { return "int"; }
	virtual const char *GetZeroType() const { return "(0)"; }
	virtual void EmitCopy(char * s,int &idx) const {
		sprintf(s,"\tdata[%d].Int = P%d;\r\n",idx,idx);
		++idx;
	}

private:
	HeapString Name;
	Enumerant *m_Last;
	u8 Subtype;
	bool Emitted;
};

class scrTypeObject: public scrType {
public:
	scrTypeObject(const char *name,const scrType *parent = NULL);
	virtual bool IsObject() const { return true; }
	virtual void GetName(char *dest) const { strcpy(dest,"OBJECT "); strcat(dest,Name); }
	virtual int GetBasicType() const { return scrValue::OBJECT; }
	virtual bool IsCompatibleWith(const scrType *actual) const { 
		while (actual && actual != this)
			actual = actual->Parent;
		return this == actual;
	}
	virtual const char *GetCType() const { return Name?Name:"void*"; }
	virtual const char *GetZeroType() const { return "(0)"; }
	virtual void EmitCopy(char * s,int &idx) const {
		sprintf(s,"\tdata[%d].Reference = P%d;\r\n",idx,idx);
		++idx;
	}
	virtual const char *GetValueField() const { return "result[0].Reference"; }

	HeapString Name;
};

class scrTypeNull: public scrTypeObject {
public:
	scrTypeNull() : scrTypeObject(NULL) { }
	static scrTypeNull Instance;
	virtual void GetName(char *dest) const { strcpy(dest,"NULL"); }
};

class scrTypeStruct: public scrType {
public:
	scrTypeStruct(const char *name);
	~scrTypeStruct();
	
	static scrTypeStruct* Vector;

	const scrField* AddField(const scrType *type,const char *name);
	int GetSize() const;
	const scrField* LookupField(const char *name) const;
	virtual void GetName(char *dest) const;
	virtual void EmitDebugInfo(fiStream *S,const char *scope,const char *name,int offset) const;
	virtual void EmitGlobalInfo(fiStream *S,const char *varname) const;
	virtual void DeclareType(fiStream *S) const;

	virtual void Construct(scrValue *dest) const {
		for (int i=0; i<Fields.GetCount(); i++) {
			if (Fields[i].Default.Value.asInt)	// Will only be nonzero for simple scalar types.
				dest[Fields[i].Offset].Int = Fields[i].Default.Value.asInt;
			else
				Fields[i].Type->Construct(dest + Fields[i].Offset);
		}
	}
	virtual void EmitConstruct() const;
	virtual bool NeedEmitConstruct() const {
		for (int i=0; i<Fields.GetCount(); i++)
			if (Fields[i].Type->NeedEmitConstruct() || Fields[i].Default.Value.asInt)
				return true;
		return false;
	}
	virtual bool ContainsStringOrFuncPtr() const;
	virtual const char *GetCType() const { return Name; /* struct is optional in C++ */ }
	virtual const char *GetZeroType() const { return ""; }
	void EmitSource() const;
	virtual bool IsStruct() const { return true; }

protected:
	atArray<scrField> Fields;
	int Size;
	bool Emitted;
	HeapString Name;
};

class scrTypeRef: public scrType {
public:
	scrTypeRef(const scrType *reference);
	virtual bool IsCompatibleWith(const scrType *actual) const { return Reference->IsCompatibleWith(actual); }
	virtual const scrType* GetReference() const { return Reference; }
	virtual void GetName(char *dest) const;
	virtual void EmitDebugInfo(fiStream *S,const char *scope,const char *name,int offset) const;
	const char *GetCType() const { return Name; }
	const char *GetZeroType() const { return "REFERENCE-TO-SOMETHING"; /* shouldn't ever get called! */ }
	virtual void EmitCopy(char * s,int &idx) const {
		sprintf(s,"\tdata[%d].Reference = (scrValue*)(&P%d);\r\n",idx,idx);
		++idx;
	}
	virtual const char *GetValueField() const { return "result[0].Reference"; }
	virtual bool IsVectorReference() const { return Reference->IsVector(); }

private:
	const scrType *Reference;
	HeapString Name;
};

struct scrTypeList 
{
#if RAGE_ENABLE_RAGE_NEW
	static void *operator new(size_t size,const char*,int) { return scrAllocator::Allocate(size); }
#else
	static void *operator new(size_t size) { return scrAllocator::Allocate(size); }
#endif
	scrTypeList(const scrType *left,const scrTypeList *right) : Left(left), Right(right), Count(1+(Right?Right->Count:0)), Size(left->GetSize()+(Right?Right->Size:0)) { }
	const scrType *Left;
	const scrTypeList *Right;
	int Count;
	int Size;
};

class scrTypeDef: public scrType {	// somewhat misnamed -- this is really a function pointer
public:
	scrTypeDef(const scrCommandInfo *info,const char *name);
	virtual void GetName(char *dest) const { strcpy(dest,"TYPEDEF"); }
	virtual bool IsTypeDef() const { return true; }

	virtual const char *GetCType() const { return CTypeName; }
	virtual const char *GetZeroType() const { return "(0)"; }
	virtual bool ContainsStringOrFuncPtr() const { return true; }

	const scrCommandInfo *Info;
	HeapString CTypeName;
};

}	// namespace rage

#endif
