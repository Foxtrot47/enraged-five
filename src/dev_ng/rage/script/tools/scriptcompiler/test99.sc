SCRIPT
  INT iDoCauseAnError
  BOOL dummy = TRUE
  IF dummy
    INT iShouldCauseAnError
  ENDIF
  WHILE dummy
     INT iShouldAlsoCauseAnError
  ENDWHILE
ENDSCRIPT
