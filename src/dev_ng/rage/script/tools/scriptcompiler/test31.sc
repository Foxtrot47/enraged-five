CONST_INT my_array_size 2

STRUCT a
	int pad
	INT aa[my_array_size]
ENDSTRUCT

STRUCT b
	int pad
	a bb[my_array_size]
ENDSTRUCT

SCRIPT
	b foo

    INT index
	FOR index = 0 TO my_array_size-1
	   INT embedded_index
       FOR embedded_index = 0 TO my_array_size-1
	       foo.bb[index].aa[embedded_index] = index
       ENDFOR
	ENDFOR
ENDSCRIPT


