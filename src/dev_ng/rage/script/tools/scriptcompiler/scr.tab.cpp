
/*  A Bison parser, made from scr.y with Bison version GNU Bison version 1.21
  */

#define YYBISON 1  /* Identify Bison output.  */

#define yyparse scr_parse
#define yylex scr_lex
#define yyerror scr_error
#define yylval scr_lval
#define yychar scr_char
#define yydebug scr_debug
#define yynerrs scr_nerrs
#define	IF	258
#define	ELSE	259
#define	ELIF	260
#define	ENDIF	261
#define	WHILE	262
#define	ENDWHILE	263
#define	REPEAT	264
#define	ENDREPEAT	265
#define	SCRIPT	266
#define	ENDSCRIPT	267
#define	RETURN	268
#define	EXIT	269
#define	ENDPROC	270
#define	ENDFUNC	271
#define	AND	272
#define	OR	273
#define	NOT	274
#define	GE	275
#define	LE	276
#define	NE	277
#define	LSH	278
#define	RSH	279
#define	FALLTHRU	280
#define	FOR	281
#define	FORWARD	282
#define	ENDFOR	283
#define	TO	284
#define	STEP	285
#define	TYPEDEF	286
#define	PLUS_TIME	287
#define	MINUS_TIME	288
#define	STRLIT	289
#define	USING	290
#define	CATCH	291
#define	THROW	292
#define	CALL	293
#define	TYPENAME	294
#define	FUNCNAME	295
#define	PROCNAME	296
#define	INTLIT	297
#define	FLOATLIT	298
#define	NEWSYM	299
#define	VAR_REF	300
#define	FIELD	301
#define	TRUE	302
#define	FALSE	303
#define	LABEL	304
#define	SWITCH	305
#define	ENDSWITCH	306
#define	CASE	307
#define	DEFAULT	308
#define	BREAK	309
#define	GOTO	310
#define	SCOPE	311
#define	ENDSCOPE	312
#define	CONST_INT	313
#define	CONST_FLOAT	314
#define	FUNC	315
#define	PROC	316
#define	NATIVE	317
#define	GLOBALS	318
#define	ENDGLOBALS	319
#define	ENUM	320
#define	ENDENUM	321
#define	ENUMLIT	322
#define	STRUCT	323
#define	ENDSTRUCT	324
#define	NL	325
#define	PLUSPLUS	326
#define	MINUSMINUS	327
#define	PLUS_EQ	328
#define	MINUS_EQ	329
#define	TIMES_EQ	330
#define	DIVIDE_EQ	331
#define	COUNT_OF	332
#define	ENUM_TO_INT	333
#define	INT_TO_ENUM	334
#define	SIZE_OF	335
#define	AND_EQ	336
#define	OR_EQ	337
#define	XOR_EQ	338
#define	NATIVE_TO_INT	339
#define	INT_TO_NATIVE	340
#define	HASH	341
#define	DEBUGONLY	342
#define	VARARGS	343
#define	STRICT_ENUM	344
#define	HASH_ENUM	345
#define	STRICT_HASH_ENUM	346
#define	TWEAK_INT	347
#define	TWEAK_FLOAT	348
#define	ANDALSO	349
#define	ORELSE	350

#line 35 "scr.y"

#include "node.h"
#include "atl/ownedptr.h"
#include "script/opcode.h"
#include "file/stream.h"
#include "system/magicnumber.h"
#include "system/param.h"

XPARAM(final);
XPARAM(shortcircuit);
PARAM(dumpallglobals,"Dump all known globals to standard output");
PARAM(dumpusedglobals,"Dump all globals used by the script to standard output");
PARAM(dumpincludes,"Dump all include files referenced to standard output");
PARAM(werror,"Warnings as errors");
PARAM(warnnestedunused,"Warn about unused variables in nested blocks");
#define scr_error scr_errorf /* for bison internal use */
#define YYERROR_VERBOSE

extern void scr_errorf(const char*,...);
extern void scr_warningf(const char*,...);
void scr_duplicate(const char *what,int line,const char *file) {
	Errorf("%s(%d) : Location of original %s definition",file,line,what);
	scr_errorf("Duplicate %s name",what);
}
void scr_using(const char*filename);
static char s_LastToken[MAX_TOKEN_LENGTH];
static char s_LastNative[MAX_TOKEN_LENGTH];
static char s_CurrentProg[MAX_TOKEN_LENGTH];
static char s_CurrentTypeDef[MAX_TOKEN_LENGTH];
extern const char *namespace_name;

using namespace rage;

static bool s_HadGoto;
static bool s_AllowGotos = true;
static bool s_AllowNativeToInt = true;
static bool s_AllowIntToNative = true;
static bool s_NoEscape;
static bool s_IgnoreDebugStuff;
bool g_SaveGlobals;
bool scr_had_warnings;

int scr_arg_struct_size;
char scr_arg_string[128];
int scr_line = 1;
fiStream *scr_stream, *scr_globalinfo, *scr_dep_file;
char scr_stream_name[256], scr_global_name[256];
u32 scr_stream_name_hash;
scrTypeObject s_StructRef(NULL);


#line 87 "scr.y"
typedef union {
	bool bval;
	int ival;
	float fval;
	const char *sval;
	scrNodeStmt *stmt;
	scrNodeExpr *expr;
	struct {
		struct scrSym **sym;
		u32 hash;
	} newsymhash;
	scrDataList *datalist;
	struct scrSymVar *varref;
	const scrField *field;
	const scrType *type;
	const scrTypeList *typelist;
	scrCommandInfo *cmd;
	scrExprList *exprlist;
	scrValue value;
	scrTypeEnum::Enumerant *eval;
	int lineno;
	class scrNodeLabel *label;
	int enumtype;
} YYSTYPE;
#line 112 "scr.y"


#define YYMAXDEPTH	2048

#if __WIN32
#pragma warning(disable:4244)	// bison.simple(253) : warning C4244: '=' : conversion from 'int' to 'short', possible loss of data
#pragma warning(disable:4701)	// bison.simple(573) : warning C4701: local variable 'yychar1' may be used without having been initialized
#pragma warning(disable:4668)	// bison.simple(148) : error C4668: '__GNUC__' is not defined as a preprocessor macro, replacing with '0' for '#if/#elif'
#pragma warning(disable:4345)	// behavior change: an object of POD type constructed with () will be default-initialized
#endif

#define YYDEBUG !__PS2

#include "script/hash.h"
#include "script/thread.h"
#include "file/asset.h"
#include "string/string.h"
#include <malloc.h>
#include <string.h>
#include <stdarg.h>

void scr_errorf(const char*,...);
int scr_lex();


const int MaxGlobalsOrStatics = 160*1024;

const int MAX_NESTING = 256;

extern fiStream *scr_structreffile;
extern fiStream *scr_globalreffile;
static atFixedArray<const scrType*,MAX_NESTING> s_Structs;
static atFixedArray<bool,MAX_NESTING> s_ReadOnly;
static atFixedArray<scrNodeSwitch*,MAX_NESTING> s_Switches;
static atFixedArray<scrValue,MaxGlobalsOrStatics> s_Globals;
static atFixedArray<scrValue,MaxGlobalsOrStatics> s_Statics;
static int s_LocalOffset, s_StaticOffset, s_GlobalOffset;
static int s_ScopeLevel;
static const scrType *s_BaseType;
static const scrType *s_ReturnType;

fiStream *scr_debuginfo, *scr_native_file;

static void CheckSwitch() {
	if (s_Switches.GetCount()) {
		if (!s_Switches.Top()->HaveLabels())
			scr_warningf("Code in SWITCH before any CASE or DEFAULT label.");
		s_Switches.Top()->SetHadBreak(false);
		if (s_Switches.Top()->GetLastLabel()) {
			s_Switches.Top()->GetLastLabel()->SetWasLastLabel();
			s_Switches.Top()->SetLastLabel(0);
		}
	}
}

static const char *string_duplicate(const char *str) {
	Assert(str);
	char *s = (char*) scrAllocator::Allocate(strlen(str)+1);
	strcpy(s,str);
	return s;
}

void emit_parameter_source(int script_channel,const scrTypeList *list,bool isNative) {
	if (!isNative)
		scrNode::EmitChannel(script_channel,"struct script_statics*");
	else if (isNative && list == &g_VARARGS) {
		scrNode::EmitChannel(script_channel,"unsigned __paramCount,unsigned __paramTypes,...");
		return;
	}
	
	while (list) {
		if (!isNative)
			scrNode::EmitChannel(script_channel,",");
		scrNode::EmitChannel(script_channel,list->Left->GetCType());
		if (isNative && list->Right)
			scrNode::EmitChannel(script_channel,",");
		list = list->Right;
	}
}

struct scrSym 
{
#if RAGE_ENABLE_RAGE_NEW
	static void *operator new(size_t size,const char*,int) { return scrAllocator::Allocate(size); }
#else
	static void *operator new(size_t size) { return scrAllocator::Allocate(size); }
#endif
	scrSym(int lexVal) : LexVal(lexVal) { 
		Filename = string_duplicate(scr_stream_name);
		LineNumber = scr_line;
	}
	virtual ~scrSym() { }
	virtual void CheckUnreferenced() const { }
	virtual void WasReferenced() { }
	virtual void Init(YYSTYPE &yylval) = 0;
	int LexVal;
	const char *Filename;
	int LineNumber;
};

struct scrSymLabel: public scrSym {
	scrSymLabel(scrNodeLabel *target) : scrSym(LABEL) {
		Target = target;
	}
	void Init(YYSTYPE &yylval) { yylval.label = Target; }
	scrNodeLabel *Target;
};

static bool s_InStackFrame;

static scrLocal *s_FirstLocal, **s_NextLocal;

static int s_InLoop;		// True if we're within a loop of some kind

struct scrSymVar: public scrSym {
	scrSymVar(const scrType	*type,scrNodeExpr *address) : scrSym(VAR_REF), Type(type), Address(address), Referenced(true), Reinitialized(true) { Name = NULL; }
	scrSymVar(const scrType *type) : scrSym(VAR_REF), Type(type), Referenced(false), Reinitialized(s_InLoop == 0) {
		Name = string_duplicate(s_LastToken);
		Address = rage_new scrNodeLocal(type,s_LastToken,s_LocalOffset);
		if (s_InStackFrame) {
			scrLocal *local = rage_new scrLocal(type,s_LastToken,s_LocalOffset);
			*s_NextLocal = local;
			s_NextLocal = &local->Next;
		}
		s_LocalOffset += type->GetSize();
		//if (s_LocalOffset > scrThread::c_DefaultStackSize/2 /* || s_LocalOffset > 255 */)
		//	scr_errorf("Too many local variables in one function");
	}
	void WasReferenced() { Referenced = true; }
	static void InitializeVar(atFixedArray<scrValue,MaxGlobalsOrStatics> &array,scrValue *initValue,const scrType *type) {
		int size = type->GetSize();
		int count = array.GetCount();
		if (count+size > MaxGlobalsOrStatics)
			scr_errorf("Too many global or static variables (limit is %d)",MaxGlobalsOrStatics);
		array.Resize(count + size);
		scrValue *base = &array[count];
		if(type->GetArrayCount() || !initValue) {
			type->Construct(base);
		}
		if (initValue) {
			//If it's an array don't overwrite the array length.
			base = base + (type->GetArrayCount() ? 1 : 0);
			for (int i=0; i<size; i++)
				base[i] = initValue[i];
		}
	}
	scrSymVar(const scrType *type,scrValue *initValue,bool isGlobal) : scrSym(VAR_REF), Type(type), Referenced(isGlobal && !PARAM_dumpusedglobals.Get()), Reinitialized(true) {
		if (isGlobal && type->ContainsString())
			scr_warningf("Cannot safely declare a STRING variable in GLOBALS block.");
		Name = string_duplicate(s_LastToken);
		Address = isGlobal? (scrNodeExpr*)(rage_new scrNodeGlobal(type,Name,s_GlobalOffset)) : (scrNodeExpr*)(rage_new scrNodeStatic(type,Name,s_StaticOffset));
		Assert(!s_InStackFrame);
		if (scr_debuginfo)
			type->EmitDebugInfo(scr_debuginfo,isGlobal?"GLOBAL":"STATIC",s_LastToken,isGlobal?s_GlobalOffset:s_StaticOffset);
		IsGlobal = isGlobal;
		if (isGlobal) {
			if (scr_global_name[0] && !scr_globalinfo) {
				scr_globalinfo = fiStream::Create(scr_global_name);
				if (scr_globalinfo) {
					int magic = MAKE_MAGIC_NUMBER('s','g','v','0');
					scr_globalinfo->WriteInt(&magic,1);
				}
			}
			if (scr_globalreffile && g_SaveGlobals)
				fprintf(scr_globalreffile,"DECL,%s,%u\r\n",s_LastToken,s_GlobalOffset);
			if (scr_globalinfo)
				type->EmitGlobalInfo(scr_globalinfo,s_LastToken,s_GlobalOffset);
			s_GlobalOffset += type->GetSize();
			InitializeVar(s_Globals,initValue,type);
		}
		else {
			s_StaticOffset += type->GetSize();
			InitializeVar(s_Statics,initValue,type);
		}
	}
	void CheckUnreferenced() const {
		if (IsGlobal && ((PARAM_dumpusedglobals.Get() && Referenced) || PARAM_dumpallglobals.Get()))
			Displayf("GLOBAL:%s",Name);
		else if (!Referenced) {
			if (PARAM_werror.Get())
			{
				Errorf("%s(%d) : Unreferenced variable '%s'.",Filename,LineNumber,Name);
				scr_had_warnings = true;
			}
			else
				Displayf("Info: %s(%d) : Unreferenced variable '%s'.",Filename,LineNumber,Name);
		}
	}
	void SetReadOnly(bool flag) { if (Address) Address->SetReadOnly(flag); }
	bool GetReadOnly() { return Address? Address->GetReadOnly() : false; }
	const scrType *Type;
	virtual void Init(YYSTYPE &yylval) { yylval.varref = this; }
	scrNodeExpr *Address;
	const char *Name;
	bool Referenced, IsGlobal, Reinitialized;
};

struct scrSymInt: public scrSym {
	scrSymInt(int ival) : scrSym(INTLIT), Value(ival) { }
	void Init(YYSTYPE &yylval) { yylval.ival = Value; }
	int Value;
};

struct scrSymEnum: public scrSym {
	scrSymEnum(scrTypeEnum::Enumerant *e) : scrSym(ENUMLIT), Enum(e) { }
	void Init(YYSTYPE &yylval) { yylval.eval = Enum; }
	scrTypeEnum::Enumerant *Enum;
};

struct scrSymFloat: public scrSym {
	scrSymFloat(float fval) : scrSym(FLOATLIT), Value(fval) { }
	void Init(YYSTYPE &yylval) { yylval.fval = Value; }
	float Value;
};

struct scrSymTypename: public scrSym {
	scrSymTypename(const scrType *type) : scrSym(TYPENAME), Type(type) { }
	void Init(YYSTYPE &yylval) { yylval.type = Type; }
	const scrType *Type;
};

struct scrSymField: public scrSym {
	scrSymField(const scrField *field) : scrSym(FIELD), Field(field) { }
	void Init(YYSTYPE &yylval) { yylval.field = Field; }
	const scrField *Field;
};

struct scrSymCommand: public scrSym {
	scrSymCommand(const char *progname,const scrType *returnType,const scrTypeList *params,u32 nativeHash,bool debugOnly) : scrSym(returnType? FUNCNAME : PROCNAME) { 
		Info.ReturnType = returnType;
		Info.Parameters = params;
		Info.NativeHash = nativeHash;
		Info.NativeSlot = -1;
		Info.Address = 0;
		Info.Tree = NULL;
		Info.Filename = string_duplicate(scr_stream_name);
		Info.LineNumber = scr_line;
		Info.ProgName = HeapString(progname);
		Info.WasStubEmitted = false;
		Info.FirstRef = NULL;
		Info.DebugOnly = debugOnly;
		if (nativeHash && scr_native_file)
			fprintf(scr_native_file,"%08x %s %d %s\r\n",nativeHash,s_LastNative,scr_line,scr_stream_name);
		Info.HasVectorReferences = false;
		while (params) {
			if (params->Left->IsVectorReference())
				Info.HasVectorReferences = true;
			params = params->Right;
		}
	}
	void Init(YYSTYPE &yylval) { yylval.cmd = &Info; }
	scrCommandInfo Info;
};

typedef atMap<ConstString,scrSym*> scrSymTab;
static atFixedArray<scrSymTab*, MAX_NESTING> s_Symbols;
static scrSymTab *s_CurrentSubprogram;		// for GOTO's
static bool s_HadDebugOnlyCalls;

scrSymTab* CheckForUnreferenced(scrSymTab *tab) {
	if (s_HadDebugOnlyCalls)
		s_HadDebugOnlyCalls = false;
	else
	{
		scrSymTab::Iterator entry = tab->CreateIterator();
		for (entry.Start(); !entry.AtEnd(); entry.Next())
		{
			if (entry.GetData())
				entry.GetData()->CheckUnreferenced();
		}
	}
	return tab;
}


static scrSymCommand *s_TimeStep;

static scrNodeExpr* TimeStep(scrNodeExpr* inner) {
	// The timestep is stored as the first global variable.
	return rage_new scrNodeArithmetic(inner,OP_IMUL,OP_FMUL,OP_VMUL,
		rage_new scrNodeFunctionCall(&s_TimeStep->Info,NULL,NULL),"*","*","*");
}

static int s_ParamSize;

static bool s_InGlobals, s_HadGlobals;

static scrTypeEnum *s_Enum;

static scrTypeStruct *s_Struct;

//Statements that initialize statics and globals.
static scrNodeStmt* s_GlobalsInit;
static scrNodeStmt* s_StaticsInit;

static bool s_NeedNewline;

static void BeginStackFrame(const scrType * /*rtype*/) {
	Assert(!s_InStackFrame);
	s_InStackFrame = true;
	s_NextLocal = &s_FirstLocal;
}
	
static void EndStackFrame() {
	Assert(s_InStackFrame);
	s_InStackFrame = false;
	s_FirstLocal = NULL;
}

static void PromoteToSubprogram(struct scrSym **&symRef)
{
	// force the symbol into a higher symbol table
	if (s_Symbols.Top() != s_CurrentSubprogram)
	{
		// printf("haha fixing label %s\n",s_LastToken);
		Assert(s_CurrentSubprogram);
		s_Symbols.Top()->Delete(s_LastToken);
		symRef = &s_CurrentSubprogram->operator[](ConstString(s_LastToken));
	}
}


#ifndef YYLTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YYLTYPE yyltype
#endif

#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		516
#define	YYFLAG		-32768
#define	YYNTBASE	114

#define YYTRANSLATE(x) ((unsigned)(x) <= 350 ? yytranslate[x] : 209)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,   109,   102,     2,    97,
    98,   107,   103,   101,   100,   106,   108,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,    99,     2,   110,
    96,   111,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
   104,     2,   105,   112,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,   113,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
    26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
    36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
    46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
    56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
    66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
    76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
    86,    87,    88,    89,    90,    91,    92,    93,    94,    95
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     1,     2,    10,    15,    16,    18,    19,    22,    24,
    27,    29,    32,    34,    35,    40,    42,    44,    48,    52,
    56,    60,    61,    67,    68,    74,    75,    81,    82,    88,
    89,    96,    97,   104,   105,   111,   112,   118,   123,   126,
   129,   134,   135,   136,   145,   146,   154,   155,   156,   164,
   165,   172,   173,   179,   183,   184,   191,   192,   198,   199,
   204,   208,   212,   213,   218,   221,   224,   227,   230,   232,
   234,   236,   238,   242,   246,   250,   254,   258,   262,   266,
   270,   274,   277,   280,   283,   286,   292,   293,   294,   301,
   302,   303,   311,   312,   313,   325,   326,   332,   335,   338,
   340,   342,   344,   345,   351,   357,   362,   363,   368,   369,
   375,   379,   381,   384,   387,   390,   393,   396,   398,   403,
   405,   408,   413,   415,   418,   422,   425,   426,   428,   432,
   436,   438,   439,   445,   446,   451,   452,   457,   458,   464,
   468,   471,   472,   474,   478,   480,   484,   486,   489,   490,
   494,   498,   500,   503,   505,   507,   510,   514,   519,   523,
   525,   529,   531,   535,   539,   542,   543,   547,   552,   555,
   556,   558,   561,   564,   567,   570,   573,   578,   581,   582,
   585,   586,   589,   594,   595,   596,   599,   600,   602,   605,
   607,   611,   619,   621,   623,   625,   627,   629,   631,   633,
   638,   644,   649,   652,   655,   657,   662,   667,   672,   677,
   682,   687,   694,   701,   706,   708,   709,   713,   715,   717,
   719,   720,   725,   730,   732,   733,   738,   743,   745,   747,
   749,   750,   755,   759,   761,   764,   767,   769,   773,   777,
   781,   783,   787,   791,   795,   799,   801,   805,   809,   813,
   817,   819,   823,   827,   829,   833,   835,   839,   841,   845,
   847,   851,   855,   857,   861,   865,   867
};

static const short yyrhs[] = {    -1,
     0,   115,   118,    11,   117,   116,   186,    12,     0,    97,
    39,    44,    98,     0,     0,   119,     0,     0,   119,   122,
     0,   122,     0,    87,    60,     0,    60,     0,    87,    61,
     0,    61,     0,     0,    39,   123,   175,    70,     0,    63,
     0,    64,     0,    27,   143,    44,     0,    27,   143,    39,
     0,    27,    68,    44,     0,    27,    68,    39,     0,     0,
   143,    44,   124,   166,    66,     0,     0,   143,    39,   125,
   166,    66,     0,     0,    68,    44,   126,   168,    69,     0,
     0,    68,    39,   127,   168,    69,     0,     0,    62,   120,
    39,    44,   128,   158,     0,     0,    62,   120,    39,    40,
   129,   158,     0,     0,    62,   121,    44,   130,   158,     0,
     0,    62,   121,    41,   131,   158,     0,    62,    44,    99,
    39,     0,    62,    44,     0,    62,    39,     0,    62,    39,
    99,    39,     0,     0,     0,   120,    39,    44,   132,   157,
   133,   186,    16,     0,     0,   120,    39,    40,   134,   157,
   186,    16,     0,     0,     0,   121,    44,   135,   157,   136,
   186,    15,     0,     0,   121,    41,   137,   157,   186,    15,
     0,     0,    58,   138,    44,   208,    70,     0,    92,    44,
   208,     0,     0,    31,    60,    39,    44,   139,   157,     0,
     0,    31,    61,    44,   140,   157,     0,     0,    58,    42,
   141,     1,     0,    59,    44,   156,     0,    93,    44,   156,
     0,     0,    59,   156,   142,     1,     0,    55,    48,     0,
    63,    47,     0,    84,    48,     0,    85,    48,     0,    65,
     0,    90,     0,    89,     0,    91,     0,    56,   184,    57,
     0,   191,    96,   208,     0,   191,    73,   208,     0,   191,
    74,   208,     0,   191,    75,   208,     0,   191,    76,   208,
     0,   191,    81,   208,     0,   191,    82,   208,     0,   191,
    83,   208,     0,   191,    71,     0,    71,   191,     0,   191,
    72,     0,    72,   191,     0,     3,   208,   184,   183,     6,
     0,     0,     0,     7,   208,   145,   184,   146,     8,     0,
     0,     0,     9,   208,   191,   147,   184,   148,    10,     0,
     0,     0,    26,   191,    96,   208,    29,   208,   165,   149,
   184,   150,    28,     0,     0,    50,   208,   151,   184,    51,
     0,    52,   155,     0,    52,    67,     0,    54,     0,    25,
     0,    53,     0,     0,    41,    97,   152,   189,    98,     0,
    38,   193,    97,   189,    98,     0,    40,    97,   189,    98,
     0,     0,    39,   153,   174,    70,     0,     0,    58,   154,
    44,   208,    70,     0,    59,    44,   156,     0,    14,     0,
    13,   208,     0,    44,    99,     0,    49,    99,     0,    55,
    44,     0,    55,    49,     0,    37,     0,    37,    97,   208,
    98,     0,    42,     0,   100,    42,     0,    86,    97,    34,
    98,     0,    43,     0,   100,    43,     0,    97,   159,    98,
     0,    97,    98,     0,     0,   157,     0,    97,    88,    98,
     0,   160,   101,   159,     0,   160,     0,     0,    39,   161,
   102,    44,   179,     0,     0,    39,   162,    44,   180,     0,
     0,    78,   163,    44,   180,     0,     0,    78,   164,   102,
    44,   179,     0,    68,   102,    44,     0,    30,   155,     0,
     0,   167,     0,   166,   101,   167,     0,    44,     0,    44,
    96,   173,     0,   169,     0,   168,   169,     0,     0,    39,
   170,   171,     0,   171,   101,   172,     0,   172,     0,    44,
   180,     0,    67,     0,    42,     0,   100,    42,     0,   173,
   103,    42,     0,    86,    97,    34,    98,     0,   174,   101,
   176,     0,   176,     0,   175,   101,   178,     0,   178,     0,
    44,   181,   177,     0,    45,   181,   177,     0,    96,   208,
     0,     0,    44,   181,   182,     0,   104,   208,   105,   181,
     0,   104,   105,     0,     0,   181,     0,    96,   173,     0,
    96,   156,     0,    96,    47,     0,    96,    48,     0,    96,
    45,     0,   104,   208,   105,   181,     0,   104,   105,     0,
     0,    96,   208,     0,     0,     4,   184,     0,     5,   208,
   184,   183,     0,     0,     0,   185,   186,     0,     0,   187,
     0,   187,   144,     0,   144,     0,    97,   208,    98,     0,
    23,   208,   101,   208,   101,   208,    24,     0,   193,     0,
    42,     0,    43,     0,    34,     0,    67,     0,    47,     0,
    48,     0,    40,    97,   189,    98,     0,    38,   193,    97,
   189,    98,     0,    41,    97,   189,    98,     0,   102,    40,
     0,   102,    41,     0,    36,     0,    78,    97,   208,    98,
     0,    84,    97,   208,    98,     0,    77,    97,   195,    98,
     0,    77,    97,    39,    98,     0,    80,    97,    39,    98,
     0,    80,    97,   193,    98,     0,    79,    97,    39,   101,
   208,    98,     0,    85,    97,    39,   101,   208,    98,     0,
    86,    97,    34,    98,     0,   190,     0,     0,   208,   101,
   190,     0,   208,     0,    45,     0,    44,     0,     0,   191,
   106,   192,    46,     0,   191,   104,   208,   105,     0,    45,
     0,     0,   193,   106,   194,    46,     0,   193,   104,   208,
   105,     0,    44,     0,    45,     0,    44,     0,     0,   195,
   106,   196,    46,     0,   195,   104,   105,     0,   188,     0,
    19,   197,     0,   100,   197,     0,   197,     0,   198,   107,
   197,     0,   198,   108,   197,     0,   198,   109,   197,     0,
   198,     0,   199,   103,   198,     0,   199,   100,   198,     0,
   199,    32,   198,     0,   199,    33,   198,     0,   199,     0,
   200,   110,   199,     0,   200,    21,   199,     0,   200,   111,
   199,     0,   200,    20,   199,     0,   200,     0,   201,    96,
   200,     0,   201,    22,   200,     0,   201,     0,   202,   102,
   201,     0,   202,     0,   203,   112,   202,     0,   203,     0,
   204,   113,   203,     0,   204,     0,   205,    17,   204,     0,
   205,    94,   204,     0,   205,     0,   206,    18,   205,     0,
   206,    95,   205,     0,   206,     0,   207,     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
   438,   471,   480,   514,   521,   528,   529,   533,   534,   538,
   539,   543,   544,   548,   548,   549,   554,   558,   562,   567,
   571,   576,   580,   584,   594,   598,   604,   613,   623,   632,
   639,   647,   654,   654,   661,   669,   676,   676,   684,   692,
   697,   704,   718,   723,   739,   745,   746,   757,   762,   776,
   782,   783,   783,   792,   810,   816,   831,   837,   851,   856,
   856,   857,   870,   875,   875,   876,   877,   878,   882,   883,
   884,   885,   889,   890,   891,   894,   895,   896,   897,   898,
   899,   900,   901,   902,   903,   904,   905,   905,   905,   906,
   906,   907,   916,   916,   917,   928,   933,   938,   956,   973,
   981,   990,  1004,  1006,  1008,  1017,  1018,  1018,  1019,  1019,
  1029,  1032,  1041,  1050,  1058,  1067,  1078,  1086,  1090,  1097,
  1098,  1099,  1103,  1104,  1113,  1132,  1133,  1137,  1138,  1142,
  1143,  1147,  1147,  1155,  1155,  1161,  1161,  1167,  1167,  1173,
  1180,  1181,  1185,  1186,  1190,  1194,  1201,  1202,  1206,  1213,
  1216,  1217,  1221,  1235,  1236,  1237,  1238,  1239,  1243,  1244,
  1248,  1249,  1253,  1266,  1275,  1276,  1280,  1312,  1324,  1328,
  1332,  1333,  1334,  1335,  1336,  1337,  1348,  1360,  1365,  1369,
  1370,  1374,  1375,  1376,  1380,  1382,  1394,  1395,  1399,  1400,
  1404,  1405,  1406,  1412,  1413,  1414,  1415,  1416,  1417,  1418,
  1419,  1427,  1428,  1429,  1430,  1431,  1437,  1445,  1451,  1457,
  1463,  1469,  1477,  1487,  1494,  1495,  1499,  1500,  1504,  1513,
  1514,  1519,  1524,  1539,  1548,  1554,  1559,  1573,  1577,  1584,
  1585,  1590,  1594,  1605,  1606,  1607,  1611,  1612,  1613,  1614,
  1618,  1619,  1620,  1621,  1622,  1626,  1627,  1628,  1629,  1630,
  1634,  1635,  1636,  1640,  1641,  1645,  1646,  1650,  1651,  1655,
  1656,  1657,  1661,  1662,  1663,  1667,  1672
};

static const char * const yytname[] = {   "$","error","$illegal.","IF","ELSE",
"ELIF","ENDIF","WHILE","ENDWHILE","REPEAT","ENDREPEAT","SCRIPT","ENDSCRIPT",
"RETURN","EXIT","ENDPROC","ENDFUNC","AND","OR","NOT","GE","LE","NE","LSH","RSH",
"FALLTHRU","FOR","FORWARD","ENDFOR","TO","STEP","TYPEDEF","PLUS_TIME","MINUS_TIME",
"STRLIT","USING","CATCH","THROW","CALL","TYPENAME","FUNCNAME","PROCNAME","INTLIT",
"FLOATLIT","NEWSYM","VAR_REF","FIELD","TRUE","FALSE","LABEL","SWITCH","ENDSWITCH",
"CASE","DEFAULT","BREAK","GOTO","SCOPE","ENDSCOPE","CONST_INT","CONST_FLOAT",
"FUNC","PROC","NATIVE","GLOBALS","ENDGLOBALS","ENUM","ENDENUM","ENUMLIT","STRUCT",
"ENDSTRUCT","NL","PLUSPLUS","MINUSMINUS","PLUS_EQ","MINUS_EQ","TIMES_EQ","DIVIDE_EQ",
"COUNT_OF","ENUM_TO_INT","INT_TO_ENUM","SIZE_OF","AND_EQ","OR_EQ","XOR_EQ","NATIVE_TO_INT",
"INT_TO_NATIVE","HASH","DEBUGONLY","VARARGS","STRICT_ENUM","HASH_ENUM","STRICT_HASH_ENUM",
"TWEAK_INT","TWEAK_FLOAT","ANDALSO","ORELSE","'='","'('","')'","':'","'-'","','",
"'&'","'+'","'['","']'","'.'","'*'","'/'","'%'","'<'","'>'","'^'","'|'","program",
"@1","@2","optStruct","optDeclList","declList","debugFunc","debugProc","decl",
"@3","@4","@5","@6","@7","@8","@9","@10","@11","@12","@13","@14","@15","@16",
"@17","@18","@19","@20","@21","@22","enumOrStrictEnum","stmt","@23","@24","@25",
"@26","@27","@28","@29","@30","@31","@32","intexpr","floatexpr","optFormalList",
"optFormalOrVarargsList","formalList","formal","@33","@34","@35","@36","optStep",
"enumerantList","enumerant","structMemberList","structMember","@37","svarDeclList",
"svarDecl","enumExpr","lvarDeclList","gvarDeclList","lvarDecl","optAssignment",
"gvarDecl","foptArrays","optArraysOrDefaultValue","optArrays","optGlobalOrStaticAssignment",
"optElses","scopedStmtList","@38","optStmtList","stmtList","primaryExpr","argExprList",
"exprList","lvalue","@39","rvalue","@40","arrayReference","@41","unaryExpr",
"multExpr","addExpr","relExpr","equalityExpr","bitAndExpr","bitXorExpr","bitOrExpr",
"logAndExpr","logOrExpr","conditionalExpr","expr",""
};
#endif

static const short yyr1[] = {     0,
   115,   116,   114,   117,   117,   118,   118,   119,   119,   120,
   120,   121,   121,   123,   122,   122,   122,   122,   122,   122,
   122,   124,   122,   125,   122,   126,   122,   127,   122,   128,
   122,   129,   122,   130,   122,   131,   122,   122,   122,   122,
   122,   132,   133,   122,   134,   122,   135,   136,   122,   137,
   122,   138,   122,   122,   139,   122,   140,   122,   141,   122,
   122,   122,   142,   122,   122,   122,   122,   122,   143,   143,
   143,   143,   144,   144,   144,   144,   144,   144,   144,   144,
   144,   144,   144,   144,   144,   144,   145,   146,   144,   147,
   148,   144,   149,   150,   144,   151,   144,   144,   144,   144,
   144,   144,   152,   144,   144,   144,   153,   144,   154,   144,
   144,   144,   144,   144,   144,   144,   144,   144,   144,   155,
   155,   155,   156,   156,   157,   157,   157,   158,   158,   159,
   159,   161,   160,   162,   160,   163,   160,   164,   160,   160,
   165,   165,   166,   166,   167,   167,   168,   168,   170,   169,
   171,   171,   172,   173,   173,   173,   173,   173,   174,   174,
   175,   175,   176,   176,   177,   177,   178,   179,   179,   179,
   180,   180,   180,   180,   180,   180,   181,   181,   181,   182,
   182,   183,   183,   183,   185,   184,   186,   186,   187,   187,
   188,   188,   188,   188,   188,   188,   188,   188,   188,   188,
   188,   188,   188,   188,   188,   188,   188,   188,   188,   188,
   188,   188,   188,   188,   189,   189,   190,   190,   191,   191,
   192,   191,   191,   193,   194,   193,   193,   193,   195,   195,
   196,   195,   195,   197,   197,   197,   198,   198,   198,   198,
   199,   199,   199,   199,   199,   200,   200,   200,   200,   200,
   201,   201,   201,   202,   202,   203,   203,   204,   204,   205,
   205,   205,   206,   206,   206,   207,   208
};

static const short yyr2[] = {     0,
     0,     0,     7,     4,     0,     1,     0,     2,     1,     2,
     1,     2,     1,     0,     4,     1,     1,     3,     3,     3,
     3,     0,     5,     0,     5,     0,     5,     0,     5,     0,
     6,     0,     6,     0,     5,     0,     5,     4,     2,     2,
     4,     0,     0,     8,     0,     7,     0,     0,     7,     0,
     6,     0,     5,     3,     0,     6,     0,     5,     0,     4,
     3,     3,     0,     4,     2,     2,     2,     2,     1,     1,
     1,     1,     3,     3,     3,     3,     3,     3,     3,     3,
     3,     2,     2,     2,     2,     5,     0,     0,     6,     0,
     0,     7,     0,     0,    11,     0,     5,     2,     2,     1,
     1,     1,     0,     5,     5,     4,     0,     4,     0,     5,
     3,     1,     2,     2,     2,     2,     2,     1,     4,     1,
     2,     4,     1,     2,     3,     2,     0,     1,     3,     3,
     1,     0,     5,     0,     4,     0,     4,     0,     5,     3,
     2,     0,     1,     3,     1,     3,     1,     2,     0,     3,
     3,     1,     2,     1,     1,     2,     3,     4,     3,     1,
     3,     1,     3,     3,     2,     0,     3,     4,     2,     0,
     1,     2,     2,     2,     2,     2,     4,     2,     0,     2,
     0,     2,     4,     0,     0,     2,     0,     1,     2,     1,
     3,     7,     1,     1,     1,     1,     1,     1,     1,     4,
     5,     4,     2,     2,     1,     4,     4,     4,     4,     4,
     4,     6,     6,     4,     1,     0,     3,     1,     1,     1,
     0,     4,     4,     1,     0,     4,     4,     1,     1,     1,
     0,     4,     3,     1,     2,     2,     1,     3,     3,     3,
     1,     3,     3,     3,     3,     1,     3,     3,     3,     3,
     1,     3,     3,     1,     3,     1,     3,     1,     3,     1,
     3,     3,     1,     3,     3,     1,     1
};

static const short yydefact[] = {     1,
     7,     0,     0,    14,     0,    52,     0,    11,    13,     0,
    16,    17,    69,     0,     0,     0,     0,    71,    70,    72,
     0,     0,     0,     6,     0,     0,     9,     0,     0,     0,
     0,     0,     0,    65,    59,     0,   123,     0,     0,    63,
    40,    39,     0,     0,    66,    28,    26,    67,    68,    10,
    12,     0,     0,     5,     8,     0,    50,    47,    24,    22,
    21,    20,    19,    18,     0,    57,   179,     0,   162,     0,
     0,    61,   124,     0,     0,     0,     0,    36,    34,     0,
     0,     0,     0,   196,   205,     0,     0,     0,   194,   195,
   228,   224,   198,   199,   197,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,   234,   193,   237,   241,   246,
   251,   254,   256,   258,   260,   263,   266,   267,    54,    62,
     0,     2,    45,    42,   127,   127,     0,     0,    55,   127,
     0,   181,    15,     0,    60,     0,    64,    41,    38,    32,
    30,   127,   127,   149,     0,   147,     0,   235,     0,     0,
   216,   216,     0,     0,     0,     0,     0,     0,     0,     0,
   236,   203,   204,     0,   225,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,   187,   127,   127,     0,
   187,    48,   145,     0,   143,     0,   127,    58,   178,     0,
     0,   167,   161,    53,   127,   127,     0,   128,    37,    35,
     0,    29,   148,    27,     0,   216,     0,   215,   218,     0,
     0,   230,   229,     0,     0,     0,     0,     0,     0,     0,
     0,   191,     0,     0,   238,   239,   240,   244,   245,   243,
   242,   250,   248,   247,   249,   253,   252,   255,   257,   259,
   261,   262,   264,   265,     0,     0,     0,     0,     0,   112,
   101,     0,   118,     0,   107,     0,     0,   220,   219,     0,
     0,     0,   102,   100,     0,   185,   109,     0,     0,     0,
   190,     0,   188,     0,   187,    43,   132,     0,   136,   126,
     0,   131,     0,   187,     0,    25,     0,    23,    56,   179,
   180,    33,    31,     0,   179,   150,   152,     0,     0,   200,
     0,   202,   209,   208,     0,   231,   206,     0,   210,   211,
   207,     0,   214,   227,   226,     4,   185,    87,     0,   113,
   220,     0,     0,     0,     0,   216,   103,   114,   115,    96,
   120,    99,     0,     0,    98,   116,   117,     0,   187,     0,
     0,    83,    85,     3,   189,    82,    84,     0,     0,     0,
     0,     0,     0,     0,     0,     0,   221,     0,   187,     0,
     0,     0,     0,     0,   125,     0,    51,     0,   155,   154,
     0,     0,   146,   144,   177,   129,     0,   153,   171,     0,
     0,   201,   217,   233,     0,     0,     0,   184,   185,    90,
     0,     0,   216,   179,   179,     0,   160,     0,   216,   185,
     0,   121,    73,   186,     0,   111,    75,    76,    77,    78,
    79,    80,    81,    74,     0,     0,    46,     0,     0,   179,
   140,   179,     0,   130,    49,     0,   156,     0,   176,   174,
   175,     0,   173,   172,   151,     0,   232,   212,   213,   185,
     0,     0,    88,   185,     0,   119,     0,   166,   166,   108,
     0,   106,     0,     0,     0,     0,   223,   222,    44,   170,
   135,   137,   170,     0,   157,   192,   182,   185,    86,     0,
    91,     0,   105,     0,   163,   164,   159,   104,    97,   122,
   110,     0,   133,   139,   158,   184,    89,     0,   142,   165,
   169,     0,   183,    92,     0,    93,   179,   141,   185,   168,
    94,     0,    95,     0,     0,     0
};

static const short yydefgoto[] = {   514,
     1,   187,   122,    23,    24,    25,    26,    27,    33,   128,
   127,    81,    80,   206,   205,   143,   142,   189,   369,   188,
   126,   294,   125,    36,   197,   130,    70,    74,    28,   281,
   399,   480,   454,   498,   509,   512,   410,   409,   335,   350,
   345,    40,   208,   209,   291,   292,   370,   371,   373,   374,
   506,   194,   195,   145,   146,   211,   306,   307,   383,   406,
    68,   407,   485,    69,   493,   388,   389,   202,   452,   348,
   349,   282,   283,   106,   217,   218,   284,   426,   107,   234,
   224,   395,   108,   109,   110,   111,   112,   113,   114,   115,
   116,   117,   118,   219
};

static const short yypact[] = {-32768,
   462,   122,    97,-32768,    23,   -12,     1,-32768,-32768,   106,
    -4,-32768,-32768,    25,    66,   134,   177,-32768,-32768,-32768,
   144,   151,    36,   462,   179,    63,-32768,    85,    99,   136,
   190,   191,   213,-32768,-32768,   215,-32768,     3,   217,-32768,
   146,   164,   225,   124,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,   334,     3,   168,-32768,     9,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,   223,-32768,   167,   -47,-32768,   267,
   334,-32768,-32768,   271,   235,   236,    34,-32768,-32768,   237,
   237,   334,   334,-32768,-32768,   195,   180,   181,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,   182,   183,   185,   186,   188,
   192,   203,   334,   334,   201,-32768,     2,-32768,   113,   -18,
   -13,    -6,   194,   175,   189,    -8,    -7,-32768,-32768,-32768,
   260,-32768,-32768,-32768,   204,   204,   259,   259,-32768,   204,
    92,   209,-32768,   213,-32768,   234,-32768,-32768,-32768,-32768,
-32768,   219,   219,-32768,    -3,-32768,    -1,-32768,   218,   -56,
   334,   334,   103,   334,   279,   117,   334,   281,   287,   224,
-32768,-32768,-32768,   334,-32768,   334,   334,   334,   334,   334,
   334,   334,   334,   334,   334,   334,   334,   334,   334,   334,
   334,   334,   334,   334,   334,   280,   432,   204,   204,    -5,
   432,-32768,   227,   -42,-32768,   -40,   204,-32768,-32768,   220,
   334,-32768,-32768,-32768,   219,   219,   -26,-32768,-32768,-32768,
   282,-32768,-32768,-32768,   334,   334,   233,-32768,   231,   239,
   240,-32768,-32768,    48,   245,   232,   246,    75,   247,   241,
   248,-32768,   243,   308,-32768,-32768,-32768,   113,   113,   113,
   113,   -18,   -18,   -18,   -18,   -13,   -13,    -6,   194,   175,
   189,   189,    -8,    -8,   258,   334,   334,   334,   334,-32768,
-32768,   199,   261,   195,-32768,   262,   263,   265,-32768,   266,
   334,   -30,-32768,-32768,   147,-32768,-32768,   317,   199,   199,
-32768,   350,   432,   127,   432,-32768,   322,   269,   278,-32768,
   275,   268,   352,   432,    -9,-32768,   259,-32768,-32768,   167,
-32768,-32768,-32768,   285,   -69,   283,-32768,   284,   288,-32768,
   334,-32768,-32768,-32768,   286,-32768,-32768,   334,-32768,-32768,
-32768,   334,-32768,-32768,-32768,-32768,-32768,-32768,   199,-32768,
-32768,    14,   334,    47,   202,   334,-32768,-32768,-32768,-32768,
-32768,-32768,   290,   346,-32768,-32768,-32768,   332,   432,   348,
     3,    19,    19,-32768,-32768,-32768,-32768,   334,   334,   334,
   334,   334,   334,   334,   334,   334,-32768,   374,   432,   291,
   351,   353,   354,   292,-32768,   -11,-32768,   381,-32768,-32768,
   303,   360,   300,-32768,-32768,-32768,    74,-32768,-32768,   282,
   334,-32768,-32768,-32768,   358,   307,   309,   244,-32768,    19,
   334,   310,   334,   167,   167,   -41,-32768,   311,   334,-32768,
   372,-32768,-32768,-32768,   334,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,   305,   369,-32768,   400,   373,   -69,
-32768,   -69,   377,-32768,-32768,   388,-32768,   382,-32768,-32768,
-32768,   208,-32768,   300,-32768,   399,-32768,-32768,-32768,-32768,
   334,   419,-32768,-32768,   397,-32768,   329,   333,   333,-32768,
   202,-32768,   330,   386,   335,   368,-32768,-32768,-32768,   338,
-32768,-32768,   338,   349,-32768,-32768,-32768,-32768,-32768,   436,
-32768,   334,-32768,   334,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,   250,-32768,-32768,-32768,   244,-32768,   438,   420,-32768,
-32768,   344,-32768,-32768,    13,-32768,   167,-32768,-32768,-32768,
-32768,   423,-32768,   452,   453,-32768
};

static const short yypgoto[] = {-32768,
-32768,-32768,-32768,-32768,-32768,   444,   445,   435,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,   454,   178,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
   -45,   -36,  -105,  -125,    86,-32768,-32768,-32768,-32768,-32768,
-32768,   336,   166,   384,    83,-32768,-32768,    78,    79,-32768,
-32768,     6,    15,   341,     5,  -198,   -64,-32768,   -17,  -323,
-32768,  -185,-32768,-32768,  -151,   169,  -240,-32768,   -81,-32768,
-32768,-32768,   -72,    45,    51,    76,   304,   312,   313,    73,
    77,-32768,-32768,   -52
};


#define	YYLAST		555


static const short yytable[] = {   119,
   220,    72,   132,   398,   150,   293,   173,   174,   182,   148,
   184,   341,   287,   169,   170,   177,   120,   210,   136,   191,
   192,   332,   133,   296,   198,   298,   387,   287,   460,    35,
   149,   161,   379,   287,   131,   144,   342,   144,   352,   353,
   216,   288,    45,    37,    38,    37,    54,   164,   123,   165,
   160,   289,   124,   134,   341,   343,   288,   380,   297,   461,
   297,   304,   288,    46,   309,   212,   289,   214,    47,   344,
    34,   290,   289,   140,   228,   453,   381,   141,   200,   302,
   303,   171,   285,   286,   172,   183,   464,   185,   400,   178,
   382,   299,   290,   235,   236,   237,   175,   176,   343,   368,
    39,   225,    39,    57,   229,   164,    58,   165,   378,   401,
    82,   233,   344,    48,    83,   379,    37,   366,   439,   367,
   440,   441,   366,    59,   367,    84,   477,    85,    60,    86,
   481,    87,    88,    89,    90,    91,    92,    61,    93,    94,
   380,   221,    62,   403,    41,   314,   222,   223,   301,    42,
   164,   315,   165,   316,   496,   227,    31,    32,    95,   381,
    91,    92,   308,   414,    78,     8,     9,    79,    96,    97,
    98,    99,   320,   442,    63,   100,   101,   102,   164,    64,
   165,    49,   334,   428,   408,   511,    13,    52,   103,    29,
   346,   104,    17,   105,    53,   347,   199,   356,   357,   358,
   359,   360,   361,   327,   328,   329,   330,   362,   363,   364,
    18,    19,    20,   238,   239,   240,   241,    56,   340,   166,
   167,   168,   365,   242,   243,   244,   245,   213,    65,   213,
   366,   471,   367,   472,    66,   385,    50,    51,    91,    92,
   162,   163,   331,   269,    75,   404,   405,   450,   451,   437,
    73,   457,   246,   247,   251,   252,    67,   463,    71,    73,
   253,   254,    76,    77,   121,   396,   129,   135,    82,   397,
   131,   137,    83,   138,   139,   144,   151,   152,   153,   154,
   402,   155,   156,    84,   157,    85,   180,    86,   158,    87,
    88,    89,    90,    91,    92,   179,    93,    94,   186,   159,
   190,   181,   193,   204,   201,   417,   418,   419,   420,   421,
   422,   423,   424,   425,   416,   207,    95,   226,   215,   230,
   231,   232,   295,   255,   300,   305,    96,    97,    98,    99,
   310,   311,   318,   100,   101,   102,   312,   313,   446,   458,
   459,   322,   317,   319,   321,   323,   103,   324,   455,   104,
   443,   105,    82,   325,   501,   326,    83,   333,   336,   337,
   351,   354,   466,   338,   339,  -134,   377,    84,   376,    85,
   372,    86,   375,    87,    88,    89,    90,    91,    92,  -138,
    93,    94,   386,   390,   391,   392,   411,   412,   413,   427,
   394,   415,   429,   433,   430,   435,   431,   432,   478,   436,
    95,   437,   438,   447,   448,   465,   449,   456,   462,   467,
    96,    97,    98,    99,   468,   469,   470,   100,   101,   102,
   473,   474,   476,   475,   479,   482,   483,   488,   484,   499,
   103,   500,   490,   104,   256,   105,   489,   491,   257,   502,
   258,   492,   510,   497,   259,   260,   495,   504,   507,   505,
   513,   515,   516,    43,    44,    30,   261,   262,    55,   508,
   355,   434,   384,   196,   147,   444,   487,   445,   263,   264,
   265,   266,   267,   486,   203,   268,   269,   494,   503,   393,
   270,   271,   248,   272,   273,   274,   275,   276,     2,   277,
   278,   249,     3,   250,     0,     0,     0,     0,     0,     0,
     4,     0,   279,   280,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     5,     0,     0,     6,
     7,     8,     9,    10,    11,    12,    13,     0,     0,    14,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,    15,    16,     0,    17,     0,
    18,    19,    20,    21,    22
};

static const short yycheck[] = {    52,
   152,    38,    67,   327,    86,   191,    20,    21,    17,    82,
    18,    42,    39,    32,    33,    22,    53,   143,    71,   125,
   126,   262,    70,    66,   130,    66,    96,    39,    70,    42,
    83,   104,    42,    39,   104,    39,    67,    39,   279,   280,
    97,    68,    47,    43,    44,    43,    11,   104,    40,   106,
   103,    78,    44,   101,    42,    86,    68,    67,   101,   101,
   101,    88,    68,    39,   216,    69,    78,    69,    44,   100,
    48,    98,    78,    40,   156,   399,    86,    44,   131,   205,
   206,   100,   188,   189,   103,    94,   410,    95,   329,    96,
   100,   197,    98,   166,   167,   168,   110,   111,    86,   285,
   100,   154,   100,    41,   157,   104,    44,   106,   294,    96,
    19,   164,   100,    48,    23,    42,    43,   104,    45,   106,
    47,    48,   104,    39,   106,    34,   450,    36,    44,    38,
   454,    40,    41,    42,    43,    44,    45,    39,    47,    48,
    67,    39,    44,    97,    39,    98,    44,    45,   201,    44,
   104,   104,   106,   106,   478,    39,    60,    61,    67,    86,
    44,    45,   215,   349,    41,    60,    61,    44,    77,    78,
    79,    80,    98,   100,    39,    84,    85,    86,   104,    44,
   106,    48,   264,   369,   336,   509,    65,    44,    97,    68,
    44,   100,    87,   102,    44,    49,   105,    71,    72,    73,
    74,    75,    76,   256,   257,   258,   259,    81,    82,    83,
    89,    90,    91,   169,   170,   171,   172,    39,   271,   107,
   108,   109,    96,   173,   174,   175,   176,   145,    39,   147,
   104,   430,   106,   432,    44,   300,    60,    61,    44,    45,
    40,    41,    44,    45,    99,    44,    45,     4,     5,    42,
    43,   403,   177,   178,   182,   183,    44,   409,    44,    43,
   184,   185,    99,    39,    97,   318,    44,     1,    19,   322,
   104,     1,    23,    39,    39,    39,    97,    97,    97,    97,
   333,    97,    97,    34,    97,    36,   112,    38,    97,    40,
    41,    42,    43,    44,    45,   102,    47,    48,    39,    97,
    97,   113,    44,    70,    96,   358,   359,   360,   361,   362,
   363,   364,   365,   366,   351,    97,    67,    39,   101,    39,
    34,    98,    96,    44,   105,    44,    77,    78,    79,    80,
    98,   101,   101,    84,    85,    86,    98,    98,   391,   404,
   405,   101,    98,    98,    98,    98,    97,   105,   401,   100,
   387,   102,    19,    46,   105,    98,    23,    97,    97,    97,
    44,    12,   415,    99,    99,    44,    15,    34,   101,    36,
   102,    38,    98,    40,    41,    42,    43,    44,    45,   102,
    47,    48,    98,   101,   101,    98,    97,    42,    57,    16,
   105,    44,   102,   102,    44,    15,    44,    44,   451,    97,
    67,    42,   103,    46,    98,    34,    98,    98,    98,   105,
    77,    78,    79,    80,    46,    16,    44,    84,    85,    86,
    44,    34,    24,    42,     6,    29,    98,    98,    96,   482,
    97,   484,    98,   100,     3,   102,    51,    70,     7,   492,
     9,   104,   507,     8,    13,    14,    98,    10,   105,    30,
    28,     0,     0,    10,    10,     2,    25,    26,    24,   505,
   283,   376,   297,   128,    81,   387,   461,   390,    37,    38,
    39,    40,    41,   459,   134,    44,    45,   473,   496,   311,
    49,    50,   179,    52,    53,    54,    55,    56,    27,    58,
    59,   180,    31,   181,    -1,    -1,    -1,    -1,    -1,    -1,
    39,    -1,    71,    72,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    55,    -1,    -1,    58,
    59,    60,    61,    62,    63,    64,    65,    -1,    -1,    68,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    84,    85,    -1,    87,    -1,
    89,    90,    91,    92,    93
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "bison.simple"

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Bob Corbett and Richard Stallman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 1, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi)
#include <alloca.h>
#else /* not sparc */
#if defined (_X86_)
#include <malloc.h>
#define alloca _alloca
#else
#if defined (MSDOS) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
 #pragma alloca
#else /* not MSDOS, __TURBOC__, or _AIX */
#ifdef __hpux
#ifdef __cplusplus
extern "C" {
void *alloca (unsigned int);
};
#else /* not __cplusplus */
void *alloca (unsigned int);
#endif /* not __cplusplus */
#endif /* __hpux */
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif /* alloca not defined.  */
#endif

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	return(0)
#define YYABORT 	return(1)
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#define YYLEX		yylex(&yylval, &yylloc)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
int yyparse (void);
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_bcopy(FROM,TO,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_bcopy (from, to, count)
     char *from;
     char *to;
     int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_bcopy (char *from, char *to, int count)
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 184 "bison.simple"
int
yyparse()
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
#ifdef YYLSP_NEEDED
		 &yyls1, size * sizeof (*yylsp),
#endif
		 &yystacksize);

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
      yyss = (short *) alloca (yystacksize * sizeof (*yyssp));
      __yy_bcopy ((char *)yyss1, (char *)yyss, size * sizeof (*yyssp));
      yyvs = (YYSTYPE *) alloca (yystacksize * sizeof (*yyvsp));
      __yy_bcopy ((char *)yyvs1, (char *)yyvs, size * sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) alloca (yystacksize * sizeof (*yylsp));
      __yy_bcopy ((char *)yyls1, (char *)yyls, size * sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 1:
#line 438 "scr.y"
{ 
			s_Symbols.Push(rage_new scrSymTab);
			
			s_LocalOffset = 0;
			s_StaticOffset = 0;
			s_GlobalOffset = 0;

			strcpy(s_LastNative,"TIMESTEP");
			s_Symbols.Top()->Insert(ConstString("TIMESTEP"),s_TimeStep = rage_new scrSymCommand("TIMESTEP",&scrTypeFloat::Instance,NULL,scrComputeHash("TIMESTEP"),false));
			s_Symbols.Top()->Insert(ConstString("NULL"),rage_new scrSymVar(&scrTypeNull::Instance,rage_new scrNodeNull(&scrTypeNull::Instance)));
			s_Symbols.Top()->Insert(ConstString("INVALID_POOLINDEX"),rage_new scrSymInt(-1));

			s_Symbols.Top()->Insert(ConstString("BOOL"),rage_new scrSymTypename(&scrTypeBool::Instance));
			s_Symbols.Top()->Insert(ConstString("FLOAT"),rage_new scrSymTypename(&scrTypeFloat::Instance));
			s_Symbols.Top()->Insert(ConstString("INT"),rage_new scrSymTypename(&scrTypeInt::Instance));
			s_Symbols.Top()->Insert(ConstString("STRING"),rage_new scrSymTypename(&scrTypeString::Instance));
			s_Symbols.Top()->Insert(ConstString("TEXT_LABEL"),rage_new scrSymTypename(rage_new scrTypeTextLabel(16)));
			s_Symbols.Top()->Insert(ConstString("TEXT_LABEL_3"),rage_new scrSymTypename(rage_new scrTypeTextLabel(4)));
			s_Symbols.Top()->Insert(ConstString("TEXT_LABEL_7"),rage_new scrSymTypename(rage_new scrTypeTextLabel(8)));
			s_Symbols.Top()->Insert(ConstString("TEXT_LABEL_15"),rage_new scrSymTypename(rage_new scrTypeTextLabel(16)));
			s_Symbols.Top()->Insert(ConstString("TEXT_LABEL_23"),rage_new scrSymTypename(rage_new scrTypeTextLabel(24)));
			s_Symbols.Top()->Insert(ConstString("TEXT_LABEL_31"),rage_new scrSymTypename(rage_new scrTypeTextLabel(32)));
			s_Symbols.Top()->Insert(ConstString("TEXT_LABEL_63"),rage_new scrSymTypename(rage_new scrTypeTextLabel(64)));
			s_Symbols.Top()->Insert(ConstString("VECTOR"),rage_new scrSymTypename(&scrTypeVector::Instance));
			s_Symbols.Top()->Insert(ConstString("POOLINDEX"),rage_new scrSymTypename(&scrTypePoolIndex::Instance));
			s_Symbols.Top()->Insert(ConstString("STRINGHASH"),rage_new scrSymTypename(&scrTypeStringHash::Instance));
			
			scrNode::LineInfo &li = scrNode::sm_LineInfo.Append();
			li.m_BasePc = 0;
			li.m_LineNumber = 1;
			li.m_FileIndex = 0;

		;
    break;}
case 2:
#line 472 "scr.y"
{
			s_Symbols.Push(s_CurrentSubprogram = rage_new scrSymTab);

			s_LocalOffset = 2;	// Leave room for return address and frame pointer here (never any parameters)
								// Any argStruct is actually implemented as hidden member variables.
			strcpy(s_LastToken,"*SCRIPT");	// pick an impossible symbol name
			BeginStackFrame(NULL);
		;
    break;}
case 3:
#line 481 "scr.y"
{
			delete CheckForUnreferenced(s_Symbols.Pop());

			s_CurrentSubprogram = NULL;

			EndStackFrame();

			//Make sure no code has been emitted at this point.
			Assert(scrNode::GetPC() == 0);

			scrNodeStmt* stmt = yyvsp[-1].stmt;

			//The first few insns of the script initialize statics/globals
			if(s_GlobalsInit && g_SaveGlobals)
			{
				stmt = rage_new scrNodeStmts(s_GlobalsInit,stmt);
			}

			if(s_StaticsInit)
			{
				stmt = rage_new scrNodeStmts(s_StaticsInit,stmt);
			}

			scrNode::SetRoot(rage_new scrNodeScript("SCRIPT",yyvsp[-4].lineno,stmt,0,s_LocalOffset,NULL,s_FirstLocal)); 
			if (s_Statics.GetCount())
				scrNode::SetStatics(&s_Statics[0],s_Statics.GetCount());
			if (s_Globals.GetCount())
				scrNode::SetGlobals(&s_Globals[0],s_Globals.GetCount());
			CheckForUnreferenced(s_Symbols.Pop());
		;
    break;}
case 4:
#line 515 "scr.y"
{
				scr_arg_struct_size = yyvsp[-2].type->GetSize();
				scrSymVar *var = rage_new scrSymVar(yyvsp[-2].type,NULL,false); 
				*yyvsp[-1].newsymhash.sym = var;
				formatf(scr_arg_string,"%s %s",yyvsp[-2].type->GetCType(),s_LastToken);
			;
    break;}
case 5:
#line 522 "scr.y"
{ 
				scr_arg_struct_size = 0; 
			;
    break;}
case 10:
#line 538 "scr.y"
{ yyval.bval = true; ;
    break;}
case 11:
#line 539 "scr.y"
{ yyval.bval = false; ;
    break;}
case 12:
#line 543 "scr.y"
{ yyval.bval = true; ;
    break;}
case 13:
#line 544 "scr.y"
{ yyval.bval = false; ;
    break;}
case 14:
#line 548 "scr.y"
{ s_BaseType = yyvsp[0].type; s_NeedNewline = true; ;
    break;}
case 16:
#line 550 "scr.y"
{ if (s_InGlobals) scr_errorf("GLOBALS block cannot nest");
		  else if (s_HadGlobals) scr_errorf("Only one GLOBALS block allowed");
		  else s_InGlobals=true; 
		;
    break;}
case 17:
#line 555 "scr.y"
{ if (!s_InGlobals) scr_errorf("ENDGLOBALS without GLOBALS");
		  else s_HadGlobals=true, s_InGlobals=false; 
		;
    break;}
case 18:
#line 559 "scr.y"
{
			*yyvsp[0].newsymhash.sym = rage_new scrSymTypename(rage_new scrTypeEnum(s_LastToken,yyvsp[-1].enumtype));
		;
    break;}
case 19:
#line 563 "scr.y"
{
			if (!yyvsp[0].type->IsEnum()) 
				scr_errorf("Trying to forward declare something that wasn't an ENUM");
		;
    break;}
case 20:
#line 568 "scr.y"
{
			*yyvsp[0].newsymhash.sym = rage_new scrSymTypename(rage_new scrTypeStruct(s_LastToken));
		;
    break;}
case 21:
#line 572 "scr.y"
{
			if (!yyvsp[0].type->IsStruct()) 
				scr_errorf("Trying to forward declare something that wasn't an STRUCT");
		;
    break;}
case 22:
#line 577 "scr.y"
{
			*yyvsp[0].newsymhash.sym = rage_new scrSymTypename(s_Enum = rage_new scrTypeEnum(s_LastToken,yyvsp[-1].enumtype));
		;
    break;}
case 23:
#line 581 "scr.y"
{
			s_Enum = NULL;
		;
    break;}
case 24:
#line 585 "scr.y"
{
			if (!yyvsp[0].type->IsEnum())
				scr_errorf("Attempting to redeclare something that wasn't an ENUM");
			else {
				s_Enum = (scrTypeEnum*) yyvsp[0].type;
				if (s_Enum->GetLast())
					scr_errorf("Attempting to redeclare an ENUM that was already fully specified");
			}
		;
    break;}
case 25:
#line 595 "scr.y"
{
			s_Enum = NULL;
		;
    break;}
case 26:
#line 599 "scr.y"
{
			*yyvsp[0].newsymhash.sym = rage_new scrSymTypename(s_Struct = rage_new scrTypeStruct(s_LastToken));
			s_Symbols.Push(rage_new scrSymTab);
			scrNode::EmitChannel(SCRIPT_DECLS,"#ifndef %s_defined\n#define %s_defined\nstruct %s {\n",s_LastToken,s_LastToken,s_LastToken);
		;
    break;}
case 27:
#line 605 "scr.y"
{
			s_Struct->EmitSource();
			scrNode::EmitChannel(SCRIPT_DECLS,"};\n#endif\n\n");
			s_Struct = NULL;
			// Do not delete the symbol table, just close it.
			// We need it around for when the structure is actually used.
			/*delete*/ s_Symbols.Pop();
		;
    break;}
case 28:
#line 614 "scr.y"
{
			if (!yyvsp[0].type->IsStruct())
				scr_errorf("Trying to redefine something that isn't a STRUCT as a STRUCT?");
			s_Struct = (scrTypeStruct*)yyvsp[0].type;
			if (s_Struct->GetSize())
				scr_errorf("Structure already fully defined");
			s_Symbols.Push(rage_new scrSymTab);
			scrNode::EmitChannel(SCRIPT_DECLS,"#ifndef %s_defined\n#define %s_defined\nstruct %s {\n",s_LastToken,s_LastToken,s_LastToken);
		;
    break;}
case 29:
#line 624 "scr.y"
{
			s_Struct->EmitSource();
			scrNode::EmitChannel(SCRIPT_DECLS,"};\n#endif\n\n");
			s_Struct = NULL;
			// Do not delete the symbol table, just close it.
			// We need it around for when the structure is actually used.
			/*delete*/ s_Symbols.Pop();
		;
    break;}
case 30:
#line 633 "scr.y"
{ 
			s_Symbols.Push(rage_new scrSymTab); 
			safecpy(s_LastNative,s_LastToken,sizeof(s_LastNative));
			scrNode::EmitChannel(SCRIPT_DECLS,"extern %s %s(",yyvsp[-1].type->GetCType(),s_LastNative);
		;
    break;}
case 31:
#line 639 "scr.y"
{
			emit_parameter_source(SCRIPT_DECLS,yyvsp[0].typelist,true);
			scrNode::EmitChannel(SCRIPT_DECLS,");\n");
			*(yyvsp[-2].newsymhash.sym) = rage_new scrSymCommand(s_LastNative,yyvsp[-3].type,yyvsp[0].typelist,yyvsp[-2].newsymhash.hash,yyvsp[-4].bval);
			s_LocalOffset = 0;
			s_ParamSize = 0;
			delete s_Symbols.Pop();
		;
    break;}
case 32:
#line 648 "scr.y"
{
			const char *name = yyvsp[0].cmd->Filename;
			int line = yyvsp[0].cmd->LineNumber;
			scr_duplicate("function",line,name);
		;
    break;}
case 34:
#line 655 "scr.y"
{ 
			s_Symbols.Push(rage_new scrSymTab); 
			safecpy(s_LastNative,s_LastToken,sizeof(s_LastNative));
			scrNode::EmitChannel(SCRIPT_DECLS,"extern void %s(",s_LastNative);
		;
    break;}
case 35:
#line 661 "scr.y"
{ 
			emit_parameter_source(SCRIPT_DECLS,yyvsp[0].typelist,true);
			scrNode::EmitChannel(SCRIPT_DECLS,");\n");
			*(yyvsp[-2].newsymhash.sym) = rage_new scrSymCommand(s_LastNative,NULL,yyvsp[0].typelist,yyvsp[-2].newsymhash.hash,yyvsp[-3].bval);
			s_LocalOffset = 0;
			s_ParamSize = 0;
			delete s_Symbols.Pop();
		;
    break;}
case 36:
#line 670 "scr.y"
{
			const char *name = yyvsp[0].cmd->Filename;
			int line = yyvsp[0].cmd->LineNumber;
			scr_duplicate("procedure",line,name);
		;
    break;}
case 38:
#line 677 "scr.y"
{ 
			// The only thing that matters here is that the address is unique.
			scrType *type = rage_new scrTypeObject(s_LastToken,yyvsp[0].type);
			*(yyvsp[-2].newsymhash.sym) = rage_new scrSymTypename(type); 
			const char *myctype = type->GetCType();
			scrNode::EmitChannel(SCRIPT_DECLS,"typedef void *%s;\n",myctype);
		;
    break;}
case 39:
#line 685 "scr.y"
{ 
			// The only thing that matters here is that the address is unique.
			scrType *type = rage_new scrTypeObject(s_LastToken);
			*(yyvsp[0].newsymhash.sym) = rage_new scrSymTypename(type); 
			const char *myctype = type->GetCType();
			scrNode::EmitChannel(SCRIPT_DECLS,"typedef void *%s;\n",myctype);
		;
    break;}
case 40:
#line 693 "scr.y"
{
			if (!yyvsp[0].type->IsObject())
				scr_warningf("Cannot redeclare built-in type as NATIVE (maybe a missing FUNC or PROC here?)");
		;
    break;}
case 41:
#line 698 "scr.y"
{ 
			if (!yyvsp[-2].type->IsObject())
				scr_warningf("Cannot redeclare built-in type as NATIVE (maybe a missing FUNC or PROC here?)");
			else if (yyvsp[-2].type->Parent != yyvsp[0].type)
				scr_warningf("NATIVE derived type redeclared with a different parent.");
		;
    break;}
case 42:
#line 705 "scr.y"
{
			if (yyvsp[-1].type->GetReference())
				scr_warningf("Returning a reference is not safe.");
			s_ReturnType = yyvsp[-1].type;
			BeginStackFrame(yyvsp[-1].type);
			safecpy(s_CurrentProg,s_LastToken,sizeof(s_CurrentProg));
			if (!strncmp(s_LastToken,"DEBUG_",6))
				scrNode::EmitChannel(SCRIPT_DECLS,"#ifndef _DEBUG\n#define %s(...) FALSE\n#else\nnamespace %s { static %s %s(",s_LastToken,namespace_name,yyvsp[-1].type->GetCType(),s_LastToken);
			else
				scrNode::EmitChannel(SCRIPT_DECLS,"namespace %s { static %s %s(",namespace_name,yyvsp[-1].type->GetCType(),s_LastToken);
			s_Symbols.Push(s_CurrentSubprogram = rage_new scrSymTab);
		;
    break;}
case 43:
#line 718 "scr.y"
{
			emit_parameter_source(SCRIPT_DECLS,yyvsp[0].typelist,false);
			*(yyvsp[-2].newsymhash.sym) = rage_new scrSymCommand(s_CurrentProg,yyvsp[-3].type,yyvsp[0].typelist,0,yyvsp[-4].bval);
			s_LocalOffset += 2;	// Leave room for return address and frame pointer here
		;
    break;}
case 44:
#line 724 "scr.y"
{
			scrNode::EmitChannel(SCRIPT_DECLS,"); } // fwd decl\n");
			if (!strncmp(s_CurrentProg,"DEBUG_",6))
				scrNode::EmitChannel(SCRIPT_DECLS,"#endif\n");
			if (!yyvsp[-1].stmt || !yyvsp[-1].stmt->Returns())
				scr_warningf("FUNC is missing a RETURN statement in a code path");
			((scrSymCommand*)(*(yyvsp[-5].newsymhash.sym)))->Info.Tree = rage_new scrNodeScript(s_CurrentProg,yyvsp[-7].bval,yyvsp[-1].stmt,s_ParamSize,s_LocalOffset,s_ReturnType,s_FirstLocal);
			s_HadGoto = false;
			delete CheckForUnreferenced(s_Symbols.Pop());
			s_LocalOffset = 0;
			s_ParamSize = 0;
			s_ReturnType = NULL;
			s_CurrentSubprogram = NULL;
			EndStackFrame();
		;
    break;}
case 45:
#line 740 "scr.y"
{
			const char *name = yyvsp[0].cmd->Filename;
			int line = yyvsp[0].cmd->LineNumber;
			scr_duplicate("function",line,name);
		;
    break;}
case 47:
#line 747 "scr.y"
{
			BeginStackFrame(NULL);
			safecpy(s_CurrentProg,s_LastToken,sizeof(s_CurrentProg));
			if (!strncmp(s_CurrentProg,"DEBUG_",6))
				scrNode::EmitChannel(SCRIPT_DECLS,"#ifndef _DEBUG\n#define %s(...)\n#else\nnamespace %s { static void %s(",s_LastToken,namespace_name,s_LastToken);
			else
				scrNode::EmitChannel(SCRIPT_DECLS,"namespace %s { static void %s(",namespace_name,s_LastToken);
			s_Symbols.Push(s_CurrentSubprogram = rage_new scrSymTab);
		;
    break;}
case 48:
#line 757 "scr.y"
{
			emit_parameter_source(SCRIPT_DECLS,yyvsp[0].typelist,false);
			*(yyvsp[-2].newsymhash.sym) = rage_new scrSymCommand(s_CurrentProg,NULL,yyvsp[0].typelist,0,yyvsp[-3].bval);
			s_LocalOffset += 2;	// Leave room for return address and frame pointer here
		;
    break;}
case 49:
#line 763 "scr.y"
{
			scrNode::EmitChannel(SCRIPT_DECLS,"); } // fwd decl\n");
			if (!strncmp(s_CurrentProg,"DEBUG_",6))
				scrNode::EmitChannel(SCRIPT_DECLS,"#endif\n");
			((scrSymCommand*)(*(yyvsp[-5].newsymhash.sym)))->Info.Tree = rage_new scrNodeScript(s_CurrentProg,yyvsp[-6].bval,yyvsp[-1].stmt,s_ParamSize,s_LocalOffset,NULL,s_FirstLocal);
			s_HadGoto = false;
			// Don't warn about unreferenced variables in a DEBUGONLY PROC
			delete CheckForUnreferenced(s_Symbols.Pop());
			s_LocalOffset = 0;
			s_ParamSize = 0;
			s_CurrentSubprogram = NULL;
			EndStackFrame();
		;
    break;}
case 50:
#line 777 "scr.y"
{
			const char *name = yyvsp[0].cmd->Filename;
			int line = yyvsp[0].cmd->LineNumber;
			scr_duplicate("function",line,name);
		;
    break;}
case 52:
#line 783 "scr.y"
{ s_NeedNewline = true; ;
    break;}
case 53:
#line 784 "scr.y"
{
			int ival;
			if (yyvsp[-1].expr->IsConstant(ival)) {
				*(yyvsp[-2].newsymhash.sym) = rage_new scrSymInt(ival);
			}
			else
				scr_errorf("CONST_INT expression must be constant");
		;
    break;}
case 54:
#line 793 "scr.y"
{
			int ival;
			/* if (s_InGlobals)
				scr_errorf("TWEAK_INT cannot appear in a GLOBALS block");
			else */ if (PARAM_final.Get()) {
				if (yyvsp[0].expr->IsConstant(ival))
					*(yyvsp[-1].newsymhash.sym) = rage_new scrSymInt(ival);
				else
					scr_errorf("TWEAK_INT expression must be constant in final build.");
			}
			else {
				scrSymVar *var = rage_new scrSymVar(&scrTypeInt::Instance,NULL,s_InGlobals);
				*(yyvsp[-1].newsymhash.sym) = var;
				scrNodeStmt* stmt =	rage_new scrNodeAssign(scr_line,&scrTypeInt::Instance,var->Address,yyvsp[0].expr,true);
				s_StaticsInit = s_StaticsInit ? rage_new scrNodeStmts(s_StaticsInit,stmt) : stmt;
			}
		;
    break;}
case 55:
#line 811 "scr.y"
{
			s_Symbols.Push(rage_new scrSymTab);
			safecpy(s_CurrentTypeDef,s_LastToken);
		;
    break;}
case 56:
#line 816 "scr.y"
{
			scrCommandInfo *info = rage_new scrCommandInfo();
			info->ReturnType = yyvsp[-3].type;
			info->Parameters = yyvsp[0].typelist;
			info->WasStubEmitted = false;
			scrTypeDef *td = rage_new scrTypeDef(info,s_CurrentTypeDef);
			*(yyvsp[-2].newsymhash.sym) = rage_new scrSymTypename(td);
			scrNode::EmitChannel(SCRIPT_DECLS,"typedef %s (*%s)(",yyvsp[-3].type->GetCType(),td->GetCType());
			emit_parameter_source(SCRIPT_DECLS,yyvsp[0].typelist,false);
			scrNode::EmitChannel(SCRIPT_DECLS,");\n");
			delete s_Symbols.Pop();
			s_LocalOffset = 0;
			s_ParamSize = 0;
		;
    break;}
case 57:
#line 832 "scr.y"
{
			s_Symbols.Push(rage_new scrSymTab);
			safecpy(s_CurrentTypeDef,s_LastToken);
		;
    break;}
case 58:
#line 837 "scr.y"
{
			scrCommandInfo *info = rage_new scrCommandInfo();
			info->ReturnType = NULL;
			info->Parameters = yyvsp[0].typelist;
			info->WasStubEmitted = false;
			scrTypeDef *td = rage_new scrTypeDef(info,s_CurrentTypeDef);
			*(yyvsp[-2].newsymhash.sym) = rage_new scrSymTypename(td);
			scrNode::EmitChannel(SCRIPT_DECLS,"typedef void (*%s)(",td->GetCType());
			emit_parameter_source(SCRIPT_DECLS,yyvsp[0].typelist,false);
			scrNode::EmitChannel(SCRIPT_DECLS,");\n");
			delete s_Symbols.Pop();
			s_LocalOffset = 0;
			s_ParamSize = 0;
		;
    break;}
case 59:
#line 852 "scr.y"
{
			scr_error("Duplicate CONST_INT name");
		;
    break;}
case 61:
#line 856 "scr.y"
{ *(yyvsp[-1].newsymhash.sym) = rage_new scrSymFloat(yyvsp[0].fval); ;
    break;}
case 62:
#line 858 "scr.y"
{
			/* if (s_InGlobals)
				scr_errorf("TWEAK_FLOAT cannot appear in a GLOBALS block");
			else */ if (PARAM_final.Get())
				*(yyvsp[-1].newsymhash.sym) = rage_new scrSymFloat(yyvsp[0].fval);
			else {
				scrSymVar *var = rage_new scrSymVar(&scrTypeFloat::Instance,NULL,s_InGlobals);
				*(yyvsp[-1].newsymhash.sym) = var;
				scrNodeStmt* stmt =	rage_new scrNodeAssign(scr_line,&scrTypeFloat::Instance,var->Address,rage_new scrNodeFloat(yyvsp[0].fval),true);
				s_StaticsInit = s_StaticsInit ? rage_new scrNodeStmts(s_StaticsInit,stmt) : stmt;
			}
		;
    break;}
case 63:
#line 871 "scr.y"
{
			scr_error("Duplicate CONST_FLOAT name");
		;
    break;}
case 65:
#line 875 "scr.y"
{ s_AllowGotos = false; ;
    break;}
case 66:
#line 876 "scr.y"
{ g_SaveGlobals = true; ;
    break;}
case 67:
#line 877 "scr.y"
{ s_AllowNativeToInt = false; ;
    break;}
case 68:
#line 878 "scr.y"
{ s_AllowIntToNative = false; ;
    break;}
case 69:
#line 882 "scr.y"
{ yyval.enumtype = 0; ;
    break;}
case 70:
#line 883 "scr.y"
{ yyval.enumtype = 2; ;
    break;}
case 71:
#line 884 "scr.y"
{ yyval.enumtype = 1; ;
    break;}
case 72:
#line 885 "scr.y"
{ yyval.enumtype = 3; ;
    break;}
case 73:
#line 889 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeScope(yyvsp[-2].lineno,yyvsp[-1].stmt); ;
    break;}
case 74:
#line 890 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeAssign(yyvsp[-1].lineno,yyvsp[-2].varref->Type,yyvsp[-2].varref->Address,yyvsp[0].expr); yyvsp[-2].varref->Reinitialized = true; ;
    break;}
case 75:
#line 891 "scr.y"
{ CheckSwitch(); yyval.stmt = yyvsp[-2].varref->Type->IsTextLabel()
															? (scrNodeStmt*)(rage_new scrNodeAppend(yyvsp[-2].varref->Address,yyvsp[-2].varref->Type,yyvsp[0].expr))
															: (scrNodeStmt*)(rage_new scrNodeAssign(yyvsp[-1].lineno,yyvsp[-2].varref->Type,yyvsp[-2].varref->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef(yyvsp[-2].varref->Type,yyvsp[-2].varref->Address),OP_IADD,OP_FADD,OP_VADD,yyvsp[0].expr,"+","+","+"))); ;
    break;}
case 76:
#line 894 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeAssign(yyvsp[-1].lineno,yyvsp[-2].varref->Type,yyvsp[-2].varref->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef(yyvsp[-2].varref->Type,yyvsp[-2].varref->Address),OP_ISUB,OP_FSUB,OP_VSUB,yyvsp[0].expr,"-","-","-")); ;
    break;}
case 77:
#line 895 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeAssign(yyvsp[-1].lineno,yyvsp[-2].varref->Type,yyvsp[-2].varref->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef(yyvsp[-2].varref->Type,yyvsp[-2].varref->Address),OP_IMUL,OP_FMUL,OP_VMUL,yyvsp[0].expr,"*","*","*")); ;
    break;}
case 78:
#line 896 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeAssign(yyvsp[-1].lineno,yyvsp[-2].varref->Type,yyvsp[-2].varref->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef(yyvsp[-2].varref->Type,yyvsp[-2].varref->Address),OP_IDIV,OP_FDIV,OP_VDIV,yyvsp[0].expr,"op_idiv","op_fdiv","op_vdiv")); ;
    break;}
case 79:
#line 897 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeAssign(yyvsp[-1].lineno,yyvsp[-2].varref->Type,yyvsp[-2].varref->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef(yyvsp[-2].varref->Type,yyvsp[-2].varref->Address),OP_IAND,OP_NOP,OP_NOP,yyvsp[0].expr,"&",NULL,NULL)); ;
    break;}
case 80:
#line 898 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeAssign(yyvsp[-1].lineno,yyvsp[-2].varref->Type,yyvsp[-2].varref->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef(yyvsp[-2].varref->Type,yyvsp[-2].varref->Address),OP_IOR,OP_NOP,OP_NOP,yyvsp[0].expr,"|",NULL,NULL)); ;
    break;}
case 81:
#line 899 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeAssign(yyvsp[-1].lineno,yyvsp[-2].varref->Type,yyvsp[-2].varref->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef(yyvsp[-2].varref->Type,yyvsp[-2].varref->Address),OP_IXOR,OP_NOP,OP_NOP,yyvsp[0].expr,"^",NULL,NULL)); ;
    break;}
case 82:
#line 900 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeAssign(yyvsp[0].lineno,yyvsp[-1].varref->Type,yyvsp[-1].varref->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef(yyvsp[-1].varref->Type,yyvsp[-1].varref->Address),OP_IADD,OP_FADD,OP_NOP,rage_new scrNodeInt(1),"+","+",NULL)); ;
    break;}
case 83:
#line 901 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeAssign(yyvsp[-1].lineno,yyvsp[0].varref->Type,yyvsp[0].varref->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef(yyvsp[0].varref->Type,yyvsp[0].varref->Address),OP_IADD,OP_FADD,OP_NOP,rage_new scrNodeInt(1),"+","+",NULL)); ;
    break;}
case 84:
#line 902 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeAssign(yyvsp[0].lineno,yyvsp[-1].varref->Type,yyvsp[-1].varref->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef(yyvsp[-1].varref->Type,yyvsp[-1].varref->Address),OP_ISUB,OP_FSUB,OP_NOP,rage_new scrNodeInt(1),"-","-",NULL)); ;
    break;}
case 85:
#line 903 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeAssign(yyvsp[-1].lineno,yyvsp[0].varref->Type,yyvsp[0].varref->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef(yyvsp[0].varref->Type,yyvsp[0].varref->Address),OP_ISUB,OP_FSUB,OP_NOP,rage_new scrNodeInt(1),"-","-",NULL)); ;
    break;}
case 86:
#line 904 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeIf(yyvsp[-4].lineno,yyvsp[-3].expr,yyvsp[-2].stmt,yyvsp[-1].stmt); ;
    break;}
case 87:
#line 905 "scr.y"
{ ++s_InLoop; ;
    break;}
case 88:
#line 905 "scr.y"
{ --s_InLoop; ;
    break;}
case 89:
#line 905 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeWhile(yyvsp[-5].lineno,yyvsp[-4].expr,yyvsp[-2].stmt); ;
    break;}
case 90:
#line 906 "scr.y"
{ ++s_InLoop; yyvsp[0].varref->Reinitialized = true; s_ReadOnly.Push(yyvsp[0].varref->GetReadOnly()); yyvsp[0].varref->SetReadOnly(true); ;
    break;}
case 91:
#line 906 "scr.y"
{ --s_InLoop; yyvsp[-2].varref->SetReadOnly(s_ReadOnly.Pop()); ;
    break;}
case 92:
#line 907 "scr.y"
{ 
			CheckSwitch(); 
			yyvsp[-4].varref->WasReferenced();
			if (!yyvsp[-5].expr->GetType()->IsInteger())
				scr_errorf("REPEAT count must be integer expression");
			if (!yyvsp[-4].varref->Type->IsInteger())
				scr_errorf("REPEAT variable must be integer");
			yyval.stmt = rage_new scrNodeRepeat(yyvsp[-6].lineno,yyvsp[-5].expr,yyvsp[-4].varref->Address,yyvsp[-2].stmt); 
		;
    break;}
case 93:
#line 916 "scr.y"
{ ++s_InLoop; yyvsp[-5].varref->Reinitialized = true; s_ReadOnly.Push(yyvsp[-5].varref->GetReadOnly()); yyvsp[-5].varref->SetReadOnly(true); ;
    break;}
case 94:
#line 916 "scr.y"
{ --s_InLoop; yyvsp[-7].varref->SetReadOnly(s_ReadOnly.Pop()); ;
    break;}
case 95:
#line 917 "scr.y"
{ 
			CheckSwitch(); 
			yyvsp[-9].varref->WasReferenced();
			if (!yyvsp[-9].varref->Type->IsInteger())
				scr_errorf("FOR loop must have integer as loop index");
			if (!yyvsp[-7].expr->GetType()->IsInteger())
				scr_errorf("FOR loop initial value must be an integral expression");
			if (!yyvsp[-5].expr->GetType()->IsInteger())
				scr_errorf("FOR loop final value must be an integral expression");
			yyval.stmt = rage_new scrNodeFor(yyvsp[-10].lineno,yyvsp[-9].varref->Address,yyvsp[-7].expr,yyvsp[-5].expr,yyvsp[-4].ival,yyvsp[-2].stmt); 
		;
    break;}
case 96:
#line 929 "scr.y"
{
			CheckSwitch(); 
			s_Switches.Push(rage_new scrNodeSwitch(yyvsp[0].expr));
		;
    break;}
case 97:
#line 934 "scr.y"
{
			s_Switches.Top()->SetBody(yyvsp[-1].stmt);
			yyval.stmt = s_Switches.Pop();
		;
    break;}
case 98:
#line 939 "scr.y"
{ 
			if (!s_Switches.GetCount()) 
				scr_errorf("CASE outside any SWITCH");
			else {
				if (!s_Switches.Top()->GetHadBreak()) {
					if (PARAM_werror.Get())
						scr_warningf("Missing BREAK or FALLTHRU before CASE");
					else
						Displayf("Info: %s(%d) : Missing BREAK or FALLTHRU before CASE",scr_stream_name,scr_line);
				}

				s_Switches.Top()->SetHadBreak(true);
				if (s_Switches.Top()->GetExpr()->GetType() != &scrTypeInt::Instance)
					scr_warningf("Enclosing SWITCH statement is an ENUM, not an integer");
				yyval.stmt = rage_new scrNodeCase(s_Switches.Top(),yyvsp[0].ival,0);
			}
		;
    break;}
case 99:
#line 957 "scr.y"
{
			if (!s_Switches.GetCount()) 
				scr_errorf("CASE outside any SWITCH");
			else {
				if (!s_Switches.Top()->GetHadBreak()) {
					if (PARAM_werror.Get())
						scr_warningf("Missing BREAK or FALLTHRU before CASE");
					else
						Displayf("Info: %s(%d) : Missing BREAK or FALLTHRU before CASE",scr_stream_name,scr_line);
				}
				s_Switches.Top()->SetHadBreak(true);
				if (s_Switches.Top()->GetExpr()->GetType() != yyvsp[0].eval->Parent)
					scr_warningf("Enclosing SWITCH statement does not match this ENUM");
				yyval.stmt = rage_new scrNodeCase(s_Switches.Top(),yyvsp[0].eval->Value,yyvsp[0].eval->Name);
			}
		;
    break;}
case 100:
#line 974 "scr.y"
{ 
			CheckSwitch();
			if (!s_Switches.GetCount()) 
				scr_errorf("BREAK outside any SWITCH");
			else
				yyval.stmt = rage_new scrNodeBreak(s_Switches.Top());
		;
    break;}
case 101:
#line 982 "scr.y"
{ 
			CheckSwitch();
			if (!s_Switches.GetCount()) 
				scr_errorf("FALLTHRU outside any SWITCH");
			else
				s_Switches.Top()->SetHadBreak(true);
			yyval.stmt = NULL;
		;
    break;}
case 102:
#line 991 "scr.y"
{ 
			if (!s_Switches.GetCount()) 
				scr_errorf("DEFAULT outside any SWITCH");
			else {
				if (!s_Switches.Top()->GetHadBreak()) {
					if (PARAM_werror.Get())
						scr_warningf("Missing BREAK or FALLTHRU before DEFAULT");
					else
						Displayf("Info: %s(%d) : Missing BREAK or FALLTHRU before DEFAULT",scr_stream_name,scr_line);
				}
				yyval.stmt = rage_new scrNodeDefault(NODE_DEFAULT,s_Switches.Top());
			}
		;
    break;}
case 103:
#line 1005 "scr.y"
{ if (PARAM_final.Get() && yyvsp[-1].cmd->DebugOnly) { s_IgnoreDebugStuff = true; s_HadDebugOnlyCalls = true; } ;
    break;}
case 104:
#line 1007 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeCommand(yyvsp[-4].cmd,yyvsp[-1].exprlist,NULL); s_IgnoreDebugStuff=false; ;
    break;}
case 105:
#line 1009 "scr.y"
{ 
			CheckSwitch(); 
			const scrType *type = yyvsp[-3].varref->Type;
			if (type->IsTypeDef()) 
				yyval.stmt = rage_new scrNodeCommand(((scrTypeDef*)type)->Info,yyvsp[-1].exprlist,yyvsp[-3].varref->Address); 
			else
				scr_warningf("Cannot call thru something that isn't a TYPEDEF");
		;
    break;}
case 106:
#line 1017 "scr.y"
{ CheckSwitch(); yyval.stmt = rage_new scrNodeCommand(yyvsp[-3].cmd,yyvsp[-1].exprlist,NULL); ;
    break;}
case 107:
#line 1018 "scr.y"
{ CheckSwitch(); s_BaseType = yyvsp[0].type; s_NeedNewline = true; ;
    break;}
case 108:
#line 1018 "scr.y"
{ yyval.stmt = yyvsp[-1].stmt; ;
    break;}
case 109:
#line 1019 "scr.y"
{ s_NeedNewline = true; ;
    break;}
case 110:
#line 1020 "scr.y"
{
			int ival;
			if (yyvsp[-1].expr->IsConstant(ival)) {
				*(yyvsp[-2].newsymhash.sym) = rage_new scrSymInt(ival);
			}
			else
				scr_errorf("CONST_INT expression must be constant");
			yyval.stmt = NULL;
		;
    break;}
case 111:
#line 1029 "scr.y"
{ *(yyvsp[-1].newsymhash.sym) = rage_new scrSymFloat(yyvsp[0].fval); yyval.stmt = NULL; ;
    break;}
case 112:
#line 1033 "scr.y"
{ 
			CheckSwitch();
			if (s_ReturnType)
				scr_warningf("Cannot use EXIT in a FUNC (use RETURN instead)");
			yyval.stmt = rage_new scrNodeReturn(NULL,s_ParamSize,0); 
			if (s_Switches.GetCount())
				s_Switches.Top()->SetHadBreak(true);
		;
    break;}
case 113:
#line 1042 "scr.y"
{ 
			CheckSwitch(); 
			if (!s_ReturnType)
				scr_warningf("Cannot use RETURN in a PROC or SCRIPT (use EXIT instead)");
			yyval.stmt = rage_new scrNodeReturn(yyvsp[0].expr,s_ParamSize,s_ReturnType); 
			if (s_Switches.GetCount())
				s_Switches.Top()->SetHadBreak(true);
		;
    break;}
case 114:
#line 1051 "scr.y"
{
			CheckSwitch(); 
			scrNodeLabel *label  = rage_new scrNodeLabel(true);
			PromoteToSubprogram(yyvsp[-1].newsymhash.sym);
			*(yyvsp[-1].newsymhash.sym) = rage_new scrSymLabel(label);
			yyval.stmt = label;
		;
    break;}
case 115:
#line 1059 "scr.y"
{
			CheckSwitch(); 
			yyval.stmt = yyvsp[-1].label;
			if (yyvsp[-1].label->IsPlaced())
				scr_warningf("Duplicate LABEL");
			else
				yyvsp[-1].label->SetPlaced();
		;
    break;}
case 116:
#line 1068 "scr.y"
{
			CheckSwitch(); 
			if (!s_AllowGotos)
				scr_warningf("Sorry, GOTO has been disabled for your project");
			s_HadGoto = true;
			scrNodeLabel *label = rage_new scrNodeLabel(false);
			PromoteToSubprogram(yyvsp[0].newsymhash.sym);
			*(yyvsp[0].newsymhash.sym) = rage_new scrSymLabel(label);
			yyval.stmt = rage_new scrNodeGoto(label);
		;
    break;}
case 117:
#line 1079 "scr.y"
{
			CheckSwitch(); 
			if (!s_AllowGotos)
				scr_warningf("Sorry, GOTO has been disabled for your project");
			s_HadGoto = true;
			yyval.stmt = rage_new scrNodeGoto(yyvsp[0].label);
		;
    break;}
case 118:
#line 1087 "scr.y"
{
			yyval.stmt = rage_new scrNodeThrow(rage_new scrNodeInt(-1));
		;
    break;}
case 119:
#line 1091 "scr.y"
{
			yyval.stmt = rage_new scrNodeThrow(yyvsp[-1].expr);
		;
    break;}
case 120:
#line 1097 "scr.y"
{ yyval.ival = yyvsp[0].ival; ;
    break;}
case 121:
#line 1098 "scr.y"
{ yyval.ival = -yyvsp[0].ival; ;
    break;}
case 122:
#line 1099 "scr.y"
{ yyval.ival = scrComputeHash(yyvsp[-1].sval); ;
    break;}
case 123:
#line 1103 "scr.y"
{ yyval.fval = yyvsp[0].fval; ;
    break;}
case 124:
#line 1104 "scr.y"
{ yyval.fval = -yyvsp[0].fval; ;
    break;}
case 125:
#line 1113 "scr.y"
{ 
									yyval.typelist = yyvsp[-1].typelist; 
									const scrTypeList *l = yyvsp[-1].typelist;
									scrDefaultValue dummy;
									while (l) {
										if (l->Left->HasDefaultValue(dummy)) {
											// Make sure everything after this has one as well
											while (l) {
												if (!l->Left->HasDefaultValue(dummy)) {
													scr_warningf("If parameter list has default value, all parameters after it must have defaults too.");
													break;
												}
												l = l->Right;
											}
										}
										else
											l = l->Right;
									}
							;
    break;}
case 126:
#line 1132 "scr.y"
{ yyval.typelist = NULL; ;
    break;}
case 127:
#line 1133 "scr.y"
{ yyval.typelist = NULL; ;
    break;}
case 128:
#line 1137 "scr.y"
{ yyval.typelist = yyvsp[0].typelist; ;
    break;}
case 129:
#line 1138 "scr.y"
{ yyval.typelist = &g_VARARGS; ;
    break;}
case 130:
#line 1142 "scr.y"
{ yyval.typelist = rage_new scrTypeList(yyvsp[-2].type,yyvsp[0].typelist); ;
    break;}
case 131:
#line 1143 "scr.y"
{ yyval.typelist = rage_new scrTypeList(yyvsp[0].type,NULL); ;
    break;}
case 132:
#line 1147 "scr.y"
{ s_BaseType = yyvsp[0].type; ;
    break;}
case 133:
#line 1148 "scr.y"
{ 
			if (yyvsp[0].type->IsString())
				scr_warningf("STRING variables are implicitly passed by reference, '&' is not allowed.");
			yyval.type = rage_new scrTypeRef(yyvsp[0].type); 
			*(yyvsp[-1].newsymhash.sym) = rage_new scrSymVar(yyval.type);
			s_ParamSize += yyval.type->GetSize();
		;
    break;}
case 134:
#line 1155 "scr.y"
{ s_BaseType = yyvsp[0].type; ;
    break;}
case 135:
#line 1156 "scr.y"
{ 
			yyval.type = yyvsp[0].type; 
			*(yyvsp[-1].newsymhash.sym) = rage_new scrSymVar(yyval.type);
			s_ParamSize += yyval.type->GetSize();
		;
    break;}
case 136:
#line 1161 "scr.y"
{ s_BaseType = &scrTypeInt::EnumToIntInstance; ;
    break;}
case 137:
#line 1162 "scr.y"
{
			yyval.type = yyvsp[0].type; 
			*(yyvsp[-1].newsymhash.sym) = rage_new scrSymVar(yyval.type);
			s_ParamSize += yyval.type->GetSize();
		;
    break;}
case 138:
#line 1167 "scr.y"
{ s_BaseType = &scrTypeInt::EnumToIntInstance; ;
    break;}
case 139:
#line 1168 "scr.y"
{
			yyval.type = rage_new scrTypeRef(yyvsp[0].type); 
			*(yyvsp[-1].newsymhash.sym) = rage_new scrSymVar(yyval.type);
			s_ParamSize += yyval.type->GetSize();
		;
    break;}
case 140:
#line 1174 "scr.y"
{
			yyval.type = rage_new scrTypeRef(&s_StructRef);
		;
    break;}
case 141:
#line 1180 "scr.y"
{ yyval.ival = yyvsp[0].ival; if (!yyvsp[0].ival) scr_warningf("STEP of zero is not allowed"); ;
    break;}
case 142:
#line 1181 "scr.y"
{ yyval.ival = 1; ;
    break;}
case 145:
#line 1191 "scr.y"
{ 
			*(yyvsp[0].newsymhash.sym) = rage_new scrSymEnum(s_Enum->AddEnumerant(s_LastToken,s_Enum->IsHashEnum()? atStringHash(s_LastToken) : s_Enum->GetLast()? s_Enum->GetLast()->Value + 1 : 0));
		;
    break;}
case 146:
#line 1195 "scr.y"
{ 
			*(yyvsp[-2].newsymhash.sym) = rage_new scrSymEnum(s_Enum->AddEnumerant(s_LastToken,yyvsp[0].ival));
		;
    break;}
case 149:
#line 1207 "scr.y"
{
			if (yyvsp[0].type == s_Struct)
				scr_warningf("Structure cannot contain itself!  The universe would implode.");
			s_BaseType = yyvsp[0].type;
		;
    break;}
case 153:
#line 1222 "scr.y"
{
			scrNode::EmitChannel(SCRIPT_DECLS,"\t%s %s;\n",yyvsp[0].type->GetCType(),s_LastToken);
			scrSym *sym = rage_new scrSymField(s_Struct->AddField(yyvsp[0].type,s_LastToken));
			*(yyvsp[-1].newsymhash.sym) = sym;
			if (scr_structreffile) {
				char sname[128];
				s_Struct->GetName(sname);
				fprintf(scr_structreffile,"DECL,%s,%s\r\n",sname,s_LastToken);
			}
		;
    break;}
case 154:
#line 1235 "scr.y"
{ yyval.ival = yyvsp[0].eval->Value; ;
    break;}
case 155:
#line 1236 "scr.y"
{ yyval.ival = yyvsp[0].ival; ;
    break;}
case 156:
#line 1237 "scr.y"
{ yyval.ival = -yyvsp[0].ival; ;
    break;}
case 157:
#line 1238 "scr.y"
{ yyval.ival = yyvsp[-2].ival + yyvsp[0].ival; ;
    break;}
case 158:
#line 1239 "scr.y"
{ yyval.ival = scrComputeHash(yyvsp[-1].sval); ;
    break;}
case 159:
#line 1243 "scr.y"
{ yyval.stmt = rage_new scrNodeStmts(yyvsp[-2].stmt,yyvsp[0].stmt); ;
    break;}
case 160:
#line 1244 "scr.y"
{ yyval.stmt = yyvsp[0].stmt; ;
    break;}
case 163:
#line 1254 "scr.y"
{ 
			bool constructed = yyvsp[-1].type->GetArrayCount() || yyvsp[0].expr;
			if (s_HadGoto && constructed)
				scr_errorf("Cannot declare array or initialized variable after seeing a GOTO in this block");
			else if (s_Switches.GetCount() && constructed)
				scr_errorf("Cannot declare array or initialized variable within a SWITCH statement");
			scrSymVar *var = rage_new scrSymVar(yyvsp[-1].type); 
			*(yyvsp[-2].newsymhash.sym) = var;
			yyval.stmt = rage_new scrNodeStmts(rage_new scrNodeDecl(yyvsp[-1].type,s_LastToken,yyvsp[0].expr != 0),yyvsp[0].expr? (scrNodeStmt*)(rage_new scrNodeAssign(scr_line,yyvsp[-1].type,var->Address,yyvsp[0].expr)) : (scrNodeStmt*)(rage_new scrNodeConstruct(yyvsp[-1].type,var->Address)));
			if (yyvsp[0].expr)
				var->Reinitialized = true;
		;
    break;}
case 164:
#line 1267 "scr.y"
{
			const char *name = yyvsp[-2].varref->Filename;
			int line = yyvsp[-2].varref->LineNumber;
			scr_duplicate("variable",line,name);
		;
    break;}
case 165:
#line 1275 "scr.y"
{ yyval.expr = yyvsp[0].expr; ;
    break;}
case 166:
#line 1276 "scr.y"
{ yyval.expr = NULL; ;
    break;}
case 167:
#line 1281 "scr.y"
{ 
			scrNode::EmitChannel(s_InGlobals? SCRIPT_GLOBALS : SCRIPT_STATICS,"\t%s ",yyvsp[-1].type->GetCType());
			scrNode::EmitChannel(s_InGlobals? SCRIPT_GLOBALS : SCRIPT_STATICS,s_LastToken); // HACK
			scrNode::EmitChannel(s_InGlobals? SCRIPT_GLOBALS : SCRIPT_STATICS," /* +%d */;\n",s_InGlobals?s_GlobalOffset:s_StaticOffset);

			if (yyvsp[-1].type->GetArrayCount() && yyvsp[0].expr)
				scr_warningf("Cannot initialize global arrays");
			scrSymVar *var = rage_new scrSymVar(yyvsp[-1].type,NULL,s_InGlobals); 
			*(yyvsp[-2].newsymhash.sym) = var;

			if(yyvsp[0].expr)
			{
				scrNodeStmt* stmt =
					rage_new scrNodeAssign(scr_line,
									yyvsp[-1].type,
									var->Address,
									yyvsp[0].expr,true);
									
				if(s_InGlobals)
				{
					s_GlobalsInit = s_GlobalsInit ? rage_new scrNodeStmts(s_GlobalsInit,stmt) : stmt;
				}
				else
				{
					s_StaticsInit = s_StaticsInit ? rage_new scrNodeStmts(s_StaticsInit,stmt) : stmt;
				}
			}
		;
    break;}
case 168:
#line 1313 "scr.y"
{
			int size;
			if (yyvsp[-2].expr->IsConstant(size)) {
				if (!size)
					scr_errorf("Zero-sized arrays are not allowed.");
					// Displayf("size = %d",size);
				yyval.type = rage_new scrTypeArray(yyvsp[0].type,size);
			}
			else
				scr_errorf("Array dimension is not integer constant");
		;
    break;}
case 169:
#line 1325 "scr.y"
{
			yyval.type = rage_new scrTypeArray(s_BaseType,VariableArraySentinel);
		;
    break;}
case 170:
#line 1328 "scr.y"
{ yyval.type = s_BaseType; ;
    break;}
case 171:
#line 1332 "scr.y"
{ yyval.type = yyvsp[0].type; ;
    break;}
case 172:
#line 1333 "scr.y"
{ if (!s_BaseType->IsInteger()) scr_warningf("Parameter cannot have default of type ENUM or INT"); yyval.type = rage_new scrTypeDefaultValue(s_BaseType, yyvsp[0].ival); ;
    break;}
case 173:
#line 1334 "scr.y"
{ if (s_BaseType != &scrTypeFloat::Instance) scr_warningf("Parameter cannot have default of type FLOAT"); yyval.type = rage_new scrTypeDefaultValue(s_BaseType, yyvsp[0].fval); ;
    break;}
case 174:
#line 1335 "scr.y"
{ if (s_BaseType != &scrTypeBool::Instance) scr_warningf("Parameter cannot have default of type BOOL"); yyval.type = rage_new scrTypeDefaultValue(s_BaseType, 1); ;
    break;}
case 175:
#line 1336 "scr.y"
{ if (s_BaseType != &scrTypeBool::Instance) scr_warningf("Parameter cannot have default of type BOOL"); yyval.type = rage_new scrTypeDefaultValue(s_BaseType, 0); ;
    break;}
case 176:
#line 1338 "scr.y"
{ 
			if (yyvsp[0].varref->Type != &scrTypeNull::Instance)
				scr_warningf("Only NULL is allowed for default parameter, not an arbitrary variable");
			if (s_BaseType != &scrTypeString::Instance && !s_BaseType->IsObject()) 
				scr_warningf("Parameter cannot have default of type NULL"); 
				yyval.type = rage_new scrTypeDefaultValue(s_BaseType, 0); 
		;
    break;}
case 177:
#line 1349 "scr.y"
{
			int size;
			if (yyvsp[-2].expr->IsConstant(size)) {
				if (!size)
					scr_errorf("Zero-sized arrays are not allowed.");
					// Displayf("size = %d",size);
				yyval.type = rage_new scrTypeArray(yyvsp[0].type,size);
			}
			else
				scr_errorf("Array dimension is not integer constant");
		;
    break;}
case 178:
#line 1361 "scr.y"
{
			scr_errorf("Cannot use variable-length arrays here (must be passed by reference)");
			yyval.type = NULL;
		;
    break;}
case 179:
#line 1365 "scr.y"
{ yyval.type = s_BaseType; ;
    break;}
case 180:
#line 1369 "scr.y"
{ yyval.expr = yyvsp[0].expr; ;
    break;}
case 181:
#line 1370 "scr.y"
{ yyval.expr = NULL; ;
    break;}
case 182:
#line 1374 "scr.y"
{ yyval.stmt = yyvsp[0].stmt; ;
    break;}
case 183:
#line 1375 "scr.y"
{ yyval.stmt = rage_new scrNodeIf(yyvsp[-3].lineno,yyvsp[-2].expr,yyvsp[-1].stmt,yyvsp[0].stmt); ;
    break;}
case 184:
#line 1376 "scr.y"
{ yyval.stmt = 0; ;
    break;}
case 185:
#line 1380 "scr.y"
{ ++s_ScopeLevel; s_Symbols.Push(rage_new scrSymTab) ;
    break;}
case 186:
#line 1382 "scr.y"
{
	    // This should eventually become default behavior!
	    if (PARAM_warnnestedunused.Get())
	      delete CheckForUnreferenced(s_Symbols.Pop());
	    else
	      delete s_Symbols.Pop();
		--s_ScopeLevel; 
		yyval.stmt = yyvsp[0].stmt; 
	  ;
    break;}
case 187:
#line 1394 "scr.y"
{ yyval.stmt = NULL; ;
    break;}
case 188:
#line 1395 "scr.y"
{ yyval.stmt = yyvsp[0].stmt; ;
    break;}
case 189:
#line 1399 "scr.y"
{ yyval.stmt = rage_new scrNodeStmts(yyvsp[-1].stmt,yyvsp[0].stmt); ;
    break;}
case 190:
#line 1400 "scr.y"
{ yyval.stmt = yyvsp[0].stmt; ;
    break;}
case 191:
#line 1404 "scr.y"
{ yyval.expr = yyvsp[-1].expr; ;
    break;}
case 192:
#line 1405 "scr.y"
{ yyval.expr = rage_new scrNodeTuple(yyvsp[-5].expr,yyvsp[-3].expr,yyvsp[-1].expr); ;
    break;}
case 193:
#line 1407 "scr.y"
{ 
				yyval.expr = rage_new scrNodeVarRef(yyvsp[0].varref->Type,yyvsp[0].varref->Address);
				if (!yyvsp[0].varref->Reinitialized)
					Displayf("Info: %s(%d) : Variable '%s' declared in loop with no initializer is referenced before being set, value will persist across iterations",scr_stream_name,scr_line,yyvsp[0].varref->Name);
			;
    break;}
case 194:
#line 1412 "scr.y"
{ yyval.expr = rage_new scrNodeInt(yyvsp[0].ival); ;
    break;}
case 195:
#line 1413 "scr.y"
{ yyval.expr = rage_new scrNodeFloat(yyvsp[0].fval); ;
    break;}
case 196:
#line 1414 "scr.y"
{ yyval.expr = rage_new scrNodeString(yyvsp[0].sval); ;
    break;}
case 197:
#line 1415 "scr.y"
{ yyval.expr = rage_new scrNodeEnum(yyvsp[0].eval); ;
    break;}
case 198:
#line 1416 "scr.y"
{ yyval.expr = rage_new scrNodeBool(true); ;
    break;}
case 199:
#line 1417 "scr.y"
{ yyval.expr = rage_new scrNodeBool(false); ;
    break;}
case 200:
#line 1418 "scr.y"
{ yyval.expr = rage_new scrNodeFunctionCall(yyvsp[-3].cmd,yyvsp[-1].exprlist,NULL); ;
    break;}
case 201:
#line 1420 "scr.y"
{ 
			const scrType *type = yyvsp[-3].varref->Type;
			if (type->IsTypeDef()) 
				yyval.expr = rage_new scrNodeFunctionCall(((scrTypeDef*)type)->Info,yyvsp[-1].exprlist,yyvsp[-3].varref->Address); 
			else
				scr_warningf("Cannot call thru something that isn't a TYPEDEF");
		;
    break;}
case 202:
#line 1427 "scr.y"
{ scr_errorf("PROC has no return value, cannot use in expression"); yyval.expr = NULL; ;
    break;}
case 203:
#line 1428 "scr.y"
{ yyval.expr = rage_new scrNodeFunctionAddr(yyvsp[0].cmd); ;
    break;}
case 204:
#line 1429 "scr.y"
{ yyval.expr = rage_new scrNodeFunctionAddr(yyvsp[0].cmd); ;
    break;}
case 205:
#line 1430 "scr.y"
{ yyval.expr = rage_new scrNodeUnary(OP_CATCH,NULL,&scrTypeInt::Instance,"op_catch"); ;
    break;}
case 206:
#line 1432 "scr.y"
{
			if (!yyvsp[-1].expr->GetType()->IsInteger())
				scr_warningf("Can only apply ENUM_TO_INT to an integer");
			yyval.expr = rage_new scrNodeUnary(OP_NOP,yyvsp[-1].expr,&scrTypeInt::Instance,"enum_to_int");
		;
    break;}
case 207:
#line 1438 "scr.y"
{
			if (!s_AllowNativeToInt)
				scr_warningf("NATIVE_TO_INT has been disallowed");
			else if (!yyvsp[-1].expr->GetType()->IsObject())
				scr_warningf("Can only apply NATIVE_TO_INT to a native type object");
			yyval.expr = rage_new scrNodeUnary(OP_NOP,yyvsp[-1].expr,&scrTypeInt::Instance,"native_to_int");
		;
    break;}
case 208:
#line 1446 "scr.y"
{
			if (!yyvsp[-1].varref->Type->GetArrayCount())
				scr_warningf("Cannot apply COUNT_OF to something that isn't an array or ENUM.");
			yyval.expr = rage_new scrNodeUnary(OP_LOAD,yyvsp[-1].varref->Address,&scrTypeInt::Instance,"count_of"); 
		;
    break;}
case 209:
#line 1452 "scr.y"
{
			if (!yyvsp[-1].type->IsEnum())
				scr_warningf("Cannot apply COUNT_OF to something that isn't an array or ENUM.");
			yyval.expr = rage_new scrNodeInt(yyvsp[-1].type->GetEnumCount());
		;
    break;}
case 210:
#line 1458 "scr.y"
{
			if (yyvsp[-1].type->IsVariableSizedArray())
				scr_warningf("Cannot apply SIZE_OF operator to a variable-sized array.");
			yyval.expr = rage_new scrNodeInt(yyvsp[-1].type->GetSize());
		;
    break;}
case 211:
#line 1464 "scr.y"
{
			if (yyvsp[-1].varref->Type->IsVariableSizedArray())
				scr_warningf("Cannot apply SIZE_OF operator to a variable-sized array.");
			yyval.expr = rage_new scrNodeInt(yyvsp[-1].varref->Type->GetSize());
		;
    break;}
case 212:
#line 1470 "scr.y"
{
			if (!yyvsp[-3].type->IsEnum())
				scr_warningf("Can only cast from something to an ENUM at present.");
			if (!yyvsp[-1].expr->GetType()->IsInteger())
				scr_warningf("Can only cast from an integer expression at present.");
			yyval.expr = rage_new scrNodeUnary(OP_NOP,yyvsp[-1].expr,yyvsp[-3].type,"int_to_enum");
		;
    break;}
case 213:
#line 1478 "scr.y"
{
			if (!s_AllowIntToNative)
				scr_warningf("INT_TO_NATIVE has been disallowed");
			if (!yyvsp[-3].type->IsObject())
				scr_warningf("Can only cast from something to NATIVE at present.");
			if (!yyvsp[-1].expr->GetType()->IsInteger())
				scr_warningf("Can only cast from an integer expression at present.");
			yyval.expr = rage_new scrNodeUnary(OP_NOP,yyvsp[-1].expr,yyvsp[-3].type,"int_to_native");
		;
    break;}
case 214:
#line 1488 "scr.y"
{
			yyval.expr = rage_new scrNodeInt( scrComputeHash(yyvsp[-1].sval));
		;
    break;}
case 215:
#line 1494 "scr.y"
{ yyval.exprlist = yyvsp[0].exprlist; ;
    break;}
case 216:
#line 1495 "scr.y"
{ yyval.exprlist = NULL; ;
    break;}
case 217:
#line 1499 "scr.y"
{ yyval.exprlist = rage_new scrExprList(yyvsp[-2].expr,yyvsp[0].exprlist); ;
    break;}
case 218:
#line 1500 "scr.y"
{ yyval.exprlist = rage_new scrExprList(yyvsp[0].expr,NULL); ;
    break;}
case 219:
#line 1505 "scr.y"
{ 
			if (yyvsp[0].varref->Type->GetReference()) {
				yyvsp[0].varref->WasReferenced();
				yyval.varref = rage_new scrSymVar(yyvsp[0].varref->Type->GetReference(),rage_new scrNodeDereference(yyvsp[0].varref->Type->GetReference(),yyvsp[0].varref->Address));
			}
			else
				yyval.varref = yyvsp[0].varref; 
		;
    break;}
case 220:
#line 1513 "scr.y"
{ scr_errorf("Unrecognized variable or command name '%s'",s_LastToken); yyval.varref = NULL; ;
    break;}
case 221:
#line 1515 "scr.y"
{
			s_Structs.Push(yyvsp[-1].varref->Type);
		;
    break;}
case 222:
#line 1519 "scr.y"
{ 
			// Intentionally don't mark an lvalue as referenced
			yyval.varref = rage_new scrSymVar(yyvsp[0].field->Type,rage_new scrNodeField(yyvsp[-3].varref->Address,yyvsp[0].field,NULL /*(scrTypeStruct*)s_Structs.Top()*/));
			s_Structs.Pop(); 
		;
    break;}
case 223:
#line 1525 "scr.y"
{
			int maxCount = yyvsp[-3].varref->Type->GetArrayCount();
			int idx;
			if (!maxCount)
				scr_errorf("Subscript applied to something that isn't an array");
			else if (yyvsp[-1].expr->IsConstant(idx) && (idx < 0 || idx >= maxCount))
				scr_errorf("Array element out of range");
			else if (!yyvsp[-1].expr->GetType()->IsInteger())
				scr_errorf("Array index must be an integer or enumerant");
			yyval.varref = rage_new scrSymVar(yyvsp[-3].varref->Type->GetArrayType(),rage_new scrNodeArrayLookup(yyvsp[-3].varref->Address,yyvsp[-3].varref->Type->GetArrayType()->GetSize(),yyvsp[-1].expr));
		;
    break;}
case 224:
#line 1540 "scr.y"
{ 
			yyvsp[0].varref->WasReferenced();
			if (yyvsp[0].varref->Type->GetReference()) {
				yyval.varref = rage_new scrSymVar(yyvsp[0].varref->Type->GetReference(),rage_new scrNodeDereference(yyvsp[0].varref->Type->GetReference(),yyvsp[0].varref->Address));
			}
			else
				yyval.varref = yyvsp[0].varref;
		;
    break;}
case 225:
#line 1549 "scr.y"
{
			yyvsp[-1].varref->WasReferenced();
			s_Structs.Push(yyvsp[-1].varref->Type);
		;
    break;}
case 226:
#line 1554 "scr.y"
{ 
			yyval.varref = rage_new scrSymVar(yyvsp[0].field->Type,rage_new scrNodeField(yyvsp[-3].varref->Address,yyvsp[0].field,(scrTypeStruct*)s_Structs.Top()));
			s_Structs.Pop(); 
			yyval.varref->Reinitialized = yyvsp[-3].varref->Reinitialized;
		;
    break;}
case 227:
#line 1560 "scr.y"
{
			yyvsp[-3].varref->WasReferenced();
			int maxCount = yyvsp[-3].varref->Type->GetArrayCount();
			int idx;
			if (!maxCount)
				scr_errorf("Subscript applied to something that isn't an array");
			else if (yyvsp[-1].expr->IsConstant(idx) && (idx < 0 || idx >= maxCount))
				scr_errorf("Array element out of range");
			else if (!yyvsp[-1].expr->GetType()->IsInteger())
				scr_errorf("Array index must be an integer or enumerant");
			yyval.varref = rage_new scrSymVar(yyvsp[-3].varref->Type->GetArrayType(),rage_new scrNodeArrayLookup(yyvsp[-3].varref->Address,yyvsp[-3].varref->Type->GetArrayType()->GetSize(),yyvsp[-1].expr));
			yyval.varref->Reinitialized = yyvsp[-3].varref->Reinitialized;
		;
    break;}
case 228:
#line 1573 "scr.y"
{ scr_errorf("Unrecognized variable or command name '%s'",s_LastToken); yyval.varref = NULL; ;
    break;}
case 229:
#line 1578 "scr.y"
{ 
			if (yyvsp[0].varref->Type->GetReference())
				yyval.varref = rage_new scrSymVar(yyvsp[0].varref->Type->GetReference(),rage_new scrNodeDereference(yyvsp[0].varref->Type->GetReference(),yyvsp[0].varref->Address));
			else
				yyval.varref = yyvsp[0].varref; 
		;
    break;}
case 230:
#line 1584 "scr.y"
{ scr_errorf("Unrecognized variable or command name '%s'",s_LastToken); yyval.varref = NULL; ;
    break;}
case 231:
#line 1586 "scr.y"
{
			s_Structs.Push(yyvsp[-1].varref->Type);
		;
    break;}
case 232:
#line 1590 "scr.y"
{ 
			s_Structs.Pop(); 
			yyval.varref = rage_new scrSymVar(yyvsp[0].field->Type,rage_new scrNodeField(yyvsp[-3].varref->Address,yyvsp[0].field,NULL));
		;
    break;}
case 233:
#line 1595 "scr.y"
{
			if (!yyvsp[-2].varref->Type->GetArrayType())
				scr_errorf("Cannot apply COUNT_OF[] to something that isn't an array.");
			yyval.varref = rage_new scrSymVar(yyvsp[-2].varref->Type->GetArrayType(),rage_new scrNodeCountOf(yyvsp[-2].varref->Address));
		;
    break;}
case 234:
#line 1605 "scr.y"
{ yyval.expr = yyvsp[0].expr; ;
    break;}
case 235:
#line 1606 "scr.y"
{ yyval.expr = rage_new scrNodeLogicalNot(yyvsp[0].expr); ;
    break;}
case 236:
#line 1607 "scr.y"
{ yyval.expr = rage_new scrNodeNegate(yyvsp[0].expr); ;
    break;}
case 237:
#line 1611 "scr.y"
{ yyval.expr = yyvsp[0].expr; ;
    break;}
case 238:
#line 1612 "scr.y"
{ yyval.expr = rage_new scrNodeArithmetic(yyvsp[-2].expr,OP_IMUL,OP_FMUL,OP_VMUL,yyvsp[0].expr,"*","*","*"); ;
    break;}
case 239:
#line 1613 "scr.y"
{ yyval.expr = rage_new scrNodeArithmetic(yyvsp[-2].expr,OP_IDIV,OP_FDIV,OP_VDIV,yyvsp[0].expr,"op_idiv","op_fdiv","op_vdiv"); ;
    break;}
case 240:
#line 1614 "scr.y"
{ yyval.expr = rage_new scrNodeArithmetic(yyvsp[-2].expr,OP_IMOD,OP_FMOD,OP_NOP,yyvsp[0].expr,"op_imod","op_fmod",NULL); ;
    break;}
case 241:
#line 1618 "scr.y"
{ yyval.expr = yyvsp[0].expr; ;
    break;}
case 242:
#line 1619 "scr.y"
{ yyval.expr = rage_new scrNodeArithmetic(yyvsp[-2].expr,OP_IADD,OP_FADD,OP_VADD,yyvsp[0].expr,"+","+","+"); ;
    break;}
case 243:
#line 1620 "scr.y"
{ yyval.expr = rage_new scrNodeArithmetic(yyvsp[-2].expr,OP_ISUB,OP_FSUB,OP_VSUB,yyvsp[0].expr,"-","-","-"); ;
    break;}
case 244:
#line 1621 "scr.y"
{ yyval.expr = rage_new scrNodeArithmetic(yyvsp[-2].expr,OP_IADD,OP_FADD,OP_VADD,TimeStep(yyvsp[0].expr),"+","+","+"); ;
    break;}
case 245:
#line 1622 "scr.y"
{ yyval.expr = rage_new scrNodeArithmetic(yyvsp[-2].expr,OP_ISUB,OP_FSUB,OP_VSUB,TimeStep(yyvsp[0].expr),"-","-","-"); ;
    break;}
case 246:
#line 1626 "scr.y"
{ yyval.expr = yyvsp[0].expr; ;
    break;}
case 247:
#line 1627 "scr.y"
{ yyval.expr = rage_new scrNodeRelational(yyvsp[-2].expr,OP_ILT,OP_FLT,yyvsp[0].expr,"<","<"); ;
    break;}
case 248:
#line 1628 "scr.y"
{ yyval.expr = rage_new scrNodeRelational(yyvsp[-2].expr,OP_ILE,OP_FLE,yyvsp[0].expr,"<=","<="); ;
    break;}
case 249:
#line 1629 "scr.y"
{ yyval.expr = rage_new scrNodeRelational(yyvsp[-2].expr,OP_IGT,OP_FGT,yyvsp[0].expr,">",">"); ;
    break;}
case 250:
#line 1630 "scr.y"
{ yyval.expr = rage_new scrNodeRelational(yyvsp[-2].expr,OP_IGE,OP_FGE,yyvsp[0].expr,">=",">="); ;
    break;}
case 251:
#line 1634 "scr.y"
{ yyval.expr = yyvsp[0].expr; ;
    break;}
case 252:
#line 1635 "scr.y"
{ yyval.expr = rage_new scrNodeEquality(yyvsp[-2].expr,OP_IEQ,OP_FEQ,yyvsp[0].expr,"==","=="); ;
    break;}
case 253:
#line 1636 "scr.y"
{ yyval.expr = rage_new scrNodeEquality(yyvsp[-2].expr,OP_INE,OP_FNE,yyvsp[0].expr,"!=","!="); ;
    break;}
case 254:
#line 1640 "scr.y"
{ yyval.expr = yyvsp[0].expr; ;
    break;}
case 255:
#line 1641 "scr.y"
{ yyval.expr = rage_new scrNodeArithmetic(yyvsp[-2].expr,OP_IAND,OP_NOP,OP_NOP,yyvsp[0].expr,"&",NULL,NULL); ;
    break;}
case 256:
#line 1645 "scr.y"
{ yyval.expr = yyvsp[0].expr; ;
    break;}
case 257:
#line 1646 "scr.y"
{ yyval.expr = rage_new scrNodeArithmetic(yyvsp[-2].expr,OP_IXOR,OP_NOP,OP_NOP,yyvsp[0].expr,"^",NULL,NULL); ;
    break;}
case 258:
#line 1650 "scr.y"
{ yyval.expr = yyvsp[0].expr; ;
    break;}
case 259:
#line 1651 "scr.y"
{ yyval.expr = rage_new scrNodeArithmetic(yyvsp[-2].expr,OP_IOR,OP_NOP,OP_NOP,yyvsp[0].expr,"|",NULL,NULL); ;
    break;}
case 260:
#line 1655 "scr.y"
{ yyval.expr = yyvsp[0].expr; ;
    break;}
case 261:
#line 1656 "scr.y"
{ yyval.expr = PARAM_shortcircuit.Get()? static_cast<scrNodeExpr*>(rage_new scrNodeShortCircuit(yyvsp[-2].expr,OP_IAND,yyvsp[0].expr)) : static_cast<scrNodeExpr*>(rage_new scrNodeLogical(yyvsp[-2].expr,OP_IAND,yyvsp[0].expr,"&")); ;
    break;}
case 262:
#line 1657 "scr.y"
{ yyval.expr = rage_new scrNodeShortCircuit(yyvsp[-2].expr,OP_IAND,yyvsp[0].expr); ;
    break;}
case 263:
#line 1661 "scr.y"
{ yyval.expr = yyvsp[0].expr; ;
    break;}
case 264:
#line 1662 "scr.y"
{ yyval.expr = PARAM_shortcircuit.Get()? static_cast<scrNodeExpr*>(rage_new scrNodeShortCircuit(yyvsp[-2].expr,OP_IOR,yyvsp[0].expr)) : static_cast<scrNodeExpr*>(rage_new scrNodeLogical(yyvsp[-2].expr,OP_IOR,yyvsp[0].expr,"|")); ;
    break;}
case 265:
#line 1663 "scr.y"
{ yyval.expr = rage_new scrNodeShortCircuit(yyvsp[-2].expr,OP_IOR,yyvsp[0].expr); ;
    break;}
case 266:
#line 1667 "scr.y"
{ yyval.expr = yyvsp[0].expr; ;
    break;}
case 267:
#line 1672 "scr.y"
{ yyval.expr = yyvsp[0].expr; ;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 457 "bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;
}
#line 1675 "scr.y"


static int nextch = 32;

const int MAX_INCLUDE_PATHS = 96;
struct include { int scr_line; fiStream *scr_stream; char scr_stream_name[256]; };
atFixedArray<include,MAX_NESTING> s_Includes;
atFixedArray<u32,MAX_USING_FILES> s_UsedUsings;
typedef char include_entry[256];
atFixedArray<include_entry,MAX_INCLUDE_PATHS> s_Paths;

void scr_add_path(const char *path) {
	if (s_Paths.IsFull())
		scr_errorf("Too many include paths specified, max is %d",s_Paths.GetMaxCount());
	strcpy(s_Paths.Append(),path);
}

std::string extra_dep;

void scr_using(const char *filename) {
	// look for the file through the entire path first to catch
	// cases where the same file is included through different paths.
	char fullpath[256];
	fiStream *new_scr_stream = NULL;
	for (int p=0; p<s_Paths.GetCount(); p++) {
		formatf(fullpath,sizeof(fullpath),"%s/%s",s_Paths[p],filename);
		new_scr_stream = fiStream::Open(fullpath);
		if (new_scr_stream)
			break;
	}
	
	// never found?
	if (!new_scr_stream) {
		scr_stream = NULL;
		scr_errorf("Unable to find '%s' anywhere in -ipath",filename);
		return;
	}

	// 
	u32 hash = scrComputeHash(fullpath);
	if (s_UsedUsings.Find(hash) != -1) {
		// Warningf("Ignoring duplicate USING file '%s'",fullpath);
		for (int i=0; i<s_Includes.GetCount(); i++)
			if (hash == scrComputeHash(s_Includes[i].scr_stream_name)) {
				Displayf("Info: %s(%d) : Cycle in header include for '%s'.",scr_stream_name,scr_line,fullpath);
				break;
			}
		new_scr_stream->Close();
		return;
	}
	
	if (PARAM_dumpincludes.Get())
		Displayf("Info: %s(%d) : Including '%s'...",scr_stream_name,scr_line,fullpath);

	if (s_UsedUsings.IsFull())
		scr_errorf("Too many include files, max is %d",s_UsedUsings.GetMaxCount());
	
	s_UsedUsings.Append() = hash;
	if (s_Includes.IsFull())
		scr_errorf("Too many nested include files, max limit is %d",s_Includes.GetMaxCount());
		
	include &i = s_Includes.Append();
	i.scr_line = scr_line;
	i.scr_stream = scr_stream;
	safecpy(i.scr_stream_name, scr_stream_name, sizeof(i.scr_stream_name));
	
	if (scr_dep_file) {
		fprintf(scr_dep_file,"%s \\\r\n",fullpath);
		extra_dep += std::string(fullpath) + std::string(":\r\n");
	}
	
	scr_line = 1;

	scr_stream = new_scr_stream;
	safecpy(scr_stream_name, fullpath, sizeof(scr_stream_name));
	scr_stream_name_hash = hash;
}


void scr_using_close_all() {
	while (s_Includes.GetCount())
		s_Includes.Pop().scr_stream->Close();
}


static int ch() {
RESTART:
	bool wasnewline = (nextch == '\n' || nextch == '\r');
	if (nextch == '\n') 
		++scr_line;
	nextch = scr_stream->FastGetCh();
	if (nextch == -1 && s_Includes.GetCount()) {
		if (!wasnewline)
			scr_errorf("Missing newline at end of this file!");
		scr_stream->Close();
		scr_line = s_Includes.Top().scr_line;
		scr_stream = s_Includes.Top().scr_stream;
		safecpy(scr_stream_name, s_Includes.Top().scr_stream_name,sizeof(scr_stream_name));
		scr_stream_name_hash = scrComputeHash(scr_stream_name);
		s_Includes.Pop();
		goto RESTART;
	}
	return nextch; 
}

extern bool fatal;

void dump_using_stack() {
	int top = s_Includes.GetCount();
	while (top) {
		--top;
		Displayf("   %s(%d) : included from here",s_Includes[top].scr_stream_name,s_Includes[top].scr_line);
	}
}

void scr_errorf(const char *fmt,...) {
	va_list(args);
	va_start(args,fmt);
	char buf[256];
	vformatf(buf,sizeof(buf),fmt,args);
	Errorf("%s(%d) : %s",scr_stream_name,scr_line,buf);
	dump_using_stack();
	if (fatal) throw "fatal error during compilation";
}

void scr_warningf(const char *fmt,...) {
	va_list(args);
	va_start(args,fmt);
	char buf[256];
	vformatf(buf,sizeof(buf),fmt,args);
	dump_using_stack();
	Errorf("%s(%d) : %s",scr_stream_name,scr_line,buf);
	
	scr_had_warnings = true;
}



#define W(x)		{ #x, x },

// Keep this is sorted order, we use a binary search. 
struct { char *name; int value; } rw[] = {
	W(AND)
	W(ANDALSO)
	W(BREAK)
	W(CALL)
	W(CASE)
	W(CATCH)
	W(CONST_FLOAT)
	W(CONST_INT)
	W(COUNT_OF)
	W(DEBUGONLY)
	W(DEFAULT)
	W(ELIF)
	W(ELSE)
	W(ENDENUM)
	W(ENDFOR)
	W(ENDFUNC)
	W(ENDGLOBALS)
	W(ENDIF)
	W(ENDPROC)
	W(ENDREPEAT)
	W(ENDSCOPE)
	W(ENDSCRIPT)
	W(ENDSTRUCT)
	W(ENDSWITCH)
	W(ENDWHILE)
	W(ENUM)
	W(ENUM_TO_INT)
	W(EXIT)
	W(FALLTHRU)
	W(FALSE)
	W(FOR)
	W(FORWARD)
	W(FUNC)
	W(GLOBALS)
	W(GOTO)
	W(HASH)
	W(HASH_ENUM)
	W(IF)
	W(INT_TO_ENUM)
	W(INT_TO_NATIVE)
	W(NATIVE)
	W(NATIVE_TO_INT)
	W(NOT) 
	W(OR)
	W(ORELSE)
	W(PROC)
	W(REPEAT)
	W(RETURN)
	W(SCOPE)
	W(SCRIPT)
	W(SIZE_OF)
	W(STEP)
	W(STRICT_ENUM)
	W(STRICT_HASH_ENUM)
	W(STRUCT)
	W(SWITCH)
	W(THROW)
	W(TO)
	W(TRUE) 
	W(TWEAK_FLOAT)
	W(TWEAK_INT)
	W(TYPEDEF)
	W(USING)
	W(VARARGS)
	W(WHILE) 
};

static int s_InDisabledBlock;

int scr_lex_inner() {
	int temp = 1;
RESTART:
	while (nextch != EOF && nextch <= 32) {
		if ((nextch == '\r' || nextch == '\n') && s_NeedNewline) {
			s_NeedNewline = false;
			return NL;
		}
		ch();
	}
	yylval.lineno = scr_line;
	switch (nextch) {
		case EOF: return EOF;

		case '(': case ')': case '%': case '.': case ',': case '#':
		case '{': case '}': case '[': case ']': case ':': case '=': case '?':
			temp = nextch; ch(); return temp;
		
		case '&': case '|': case '^':
			temp = nextch; ch();
			if (nextch == '=') {
				ch();
				return temp=='&'?AND_EQ:temp=='|'?OR_EQ:XOR_EQ;
			}
			else
				return temp;
				
		case '*':
			if (ch() == '=') {
				ch();
				return TIMES_EQ;
			}
			else
				return '*';

		case '/':
			if (ch() == '/') {
				while (ch() != '\n' && nextch != '\r' && nextch != EOF)
					;
				if (s_NeedNewline) {
					s_NeedNewline = false;
					return NL;
				}
				goto RESTART;
			}
			else if (nextch == '*') {
				ch();
				int state = 0;
				while (nextch != EOF) {
					if (nextch == '*')
						state = 1;
					else if (state == 1 && nextch == '/') {
						ch();	// // consume closing slash
						goto RESTART;
					}
					else
						state = 0;
					ch();
				}
				goto RESTART;
			}
			else if (nextch == '=') {
				ch();
				return DIVIDE_EQ;
			}
			else
				return '/';

		case '+':
			if (ch() == '@') { 
				ch(); 
				return PLUS_TIME; 
			}
			else if (nextch == '+') {
				ch();
				return PLUSPLUS;
			}
			else if (nextch == '=') {
				ch();
				return PLUS_EQ;
			}
			else
				return '+';
				
		case '-':
			if (ch() == '@') {
				ch();
				return MINUS_TIME;
			}
			else if (nextch == '-') {
				ch();
				return MINUSMINUS;
			}
			else if (nextch == '=') {
				ch();
				return MINUS_EQ;
			}
			else
				return '-';

		case '<':
			if (ch() == '>') { 
				ch(); 
				return NE; 
			} 
			else if (nextch == '=') { 
				ch(); 
				return LE; 
			} 
			else if (nextch == '<') { 
				ch(); 
				return LSH; 
			} 
			else 
				return '<';

		case '!':
			if (ch() == '=') {
				ch();
				return NE;
			}
			else
				return NOT;

		case '>':
			if (ch() == '=') { 
				ch(); 
				return GE; 
			} 
			else if (nextch == '>') { 
				ch(); 
				return RSH; 
			} 
			else 
				return '>';

		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9': {
			char buf[MAX_TOKEN_LENGTH];
			int stored = 0;
			bool isFloat = false;
			const char *state = "01234567890.eE";
			do {
				if (stored<sizeof(buf)-1)
					buf[stored++] = nextch;
				ch();
				if (nextch=='e'||nextch=='E') {
					isFloat = true;
					state = "01234567890eE+-";
				}
			} while (nextch && strchr(state,nextch));
			buf[stored] = '\0';
			//puts(buf);
			if (isFloat || strchr(buf,'.')) {
				errno = 0;
				yylval.fval = atof(buf);
				if (errno) 
					scr_warningf("Floating-point literal out of range (%s)",buf);
				return FLOATLIT;
			}
			else {
				errno = 0;
				yylval.ival = atoi(buf);
				if (errno) 
					scr_warningf("Integer literal out of range (%s)",buf);
				return INTLIT;
			}
		}

		case '"': {
			char buf[255];
			int stored = 0;
			bool whined = false;
			while (ch() != '"' && nextch != EOF) {
				int tmpch = nextch;
				if (nextch == '\\' && !s_NoEscape) switch(ch()) {
					case 'n': tmpch = '\n'; break;
					case 'r': tmpch = '\r'; break;
					case '\"': tmpch = '\"'; break;
				}
				if (stored < sizeof(buf)-1)
					buf[stored++] = tmpch;
				else if (!whined) {
					scr_warningf("String constant cannot be longer than 254 characters, will be truncated.");
					whined = true;
				}
			}
			ch();
			buf[stored] = 0;
			yylval.sval = string_duplicate(buf);
			return STRLIT;
		}
			

		default: {
			if ((nextch >= 'a' && nextch <= 'z') || (nextch >= 'A' && nextch <= 'Z') || nextch=='_') {
				char buf[MAX_TOKEN_LENGTH];
				int stored = 0;
				bool warned = false;
				do {
					if (stored < (int)sizeof(buf)-1)
						buf[stored++] = (nextch >= 'a' && nextch <= 'z')? nextch - 32 : nextch;
					else if (!warned) {
						buf[stored] = 0;
						scr_warningf("Symbol starting with '%s' truncated to %d characters.",buf,(int)sizeof(buf)-1);
						warned = true;
					}
					ch();
				} while ((nextch >= 'a' && nextch <= 'z') || (nextch >= 'A' && nextch <= 'Z') || (nextch >= '0' && nextch <= '9') || nextch == '_');
				buf[stored] = 0;
				if (s_InDisabledBlock)	// don't spew crap into symbol table if we're skipping a block
					return TYPENAME;
				const int rwCount = sizeof(rw) / sizeof(rw[0]);
				int low = 0;
				int high = rwCount-1;
				while (low <= high) {
					int mid = (low+high)>>1;
					int val = strcmp(rw[mid].name,buf);
					if (val == 0)
						return rw[mid].value;
					else if (val < 0)
						low = mid+1;
					else
						high = mid-1;
				}
				if (s_Structs.GetCount() && s_Structs.Top()) {
					if ((yylval.field = s_Structs.Top()->LookupField(buf)) == 0)
						scr_errorf("Unknown field name, or not a structure");
					return FIELD;
				}
				for (int scope=s_Symbols.GetCount()-1; scope>=0; --scope) {
					scrSym **s = s_Symbols[scope]->Access(buf);
					if (s) {
						if (*s) {
							(*s)->Init(yylval);
							// (*s)->WasReferenced();
							return (*s)->LexVal;
						}
						else {
							if (!s_IgnoreDebugStuff)
								scr_error("Unknown new symbol referenced.");
							return NEWSYM;
						}
					}
				}
				
				if (!s_Symbols.GetCount()) {
					scr_errorf("Found crap after ENDSCRIPT?");
					return ENDSCRIPT;
				}
				
				safecpy(s_LastToken,buf,sizeof(s_LastToken));
				yylval.newsymhash.sym = &s_Symbols.Top()->operator[](ConstString(buf));
				*(yylval.newsymhash.sym) = NULL;
				u32 hash = scrComputeHash(buf);
				hash += !hash;	// Don't allow zero
				yylval.newsymhash.hash = hash;
				return NEWSYM;
			}
			else {
				if (nextch == ';')
					scr_warningf("This isn't C++, do not taunt me with semicolons!");
				else
					scr_warningf("Unknown character '%c' in input.",nextch);
				return 0;
			}
		}
	}
}


bool get_key_word(char *buf,int bufSize) {
	--bufSize;
	bool warned = false;
	while (nextch==32||nextch==9)
		ch();
	if ((nextch >= 'a' && nextch <= 'z') || (nextch >= 'A' && nextch <= 'Z') || nextch=='_') {
		int stored = 0;
		do {
			if (stored < bufSize)
				buf[stored++] = (nextch >= 'a' && nextch <= 'z')? nextch - 32 : nextch;
			else if (!warned) {
				buf[stored] = 0;
				scr_warningf("Keyword starting with '%s' truncated to %d characters.",buf,bufSize);
				warned = true;
			}
			ch();
		} while ((nextch >= 'a' && nextch <= 'z') || (nextch >= 'A' && nextch <= 'Z') || (nextch >= '0' && nextch <= '9') || nextch == '_');
		buf[stored] = 0;
		return true;
	}
	else if (nextch == '(' || nextch == ')') {
		buf[0] = nextch;
		buf[1] = 0;
		ch();
		return true;
	}
	else
		return false;
}

static int s_Endif;

int scr_lex() {
	int token, parenLevel = 1;
	if (s_IgnoreDebugStuff) {
		do {
			token = scr_lex_inner();
			// printf("ignoring token %d %c\n",token,token);
			if (token == '(')
				++parenLevel;
			else if (token == ')')
				--parenLevel;
			// Check a bunch of tokens that we won't see before the closing paren in case it's missing
			else if (token == ENDFUNC || token == ENDPROC || token == ENDFUNC || token == ENDIF || token == ENDWHILE || token == ENDREPEAT || token == ENDSWITCH || token == CASE || token == -1)
				scr_errorf("Error trying to skip DEBUGONLY PROC or FUNC");
		} while (parenLevel);
		return ')';
	}
	for (;;) {
		token = scr_lex_inner();
		if (token == USING) {
			s_NoEscape = true;
			if (scr_lex_inner() != STRLIT)
				scr_errorf("USING directive expected to be followed by a quoted string");
			else if (yylval.sval[0]==' ' || (yylval.sval[0] && yylval.sval[strlen(yylval.sval)-1]==' '))
				scr_errorf("USING directive cannot have spaces between quotes and actual filename");
			else
				scr_using(yylval.sval);
			s_NoEscape = false;
		}
		else if (token != '#')
			break;
		else {
			char buf[MAX_TOKEN_LENGTH];
			if (get_key_word(buf,sizeof(buf))) {
				if (!strcmp(buf,"ENDIF")) {
					if (!s_Endif)
						scr_errorf("#ENDIF without a matching #IF[N]DEF");
					else {
						--s_Endif;
						continue;
					}
				}
				if (/*strcmp(buf,"IFDEF") && strcmp(buf,"IFNDEF") &&*/ strcmp(buf,"IF"))
					scr_errorf("Preprocessor directive must be #if, not #%s",buf);
				bool _if = !strcmp(buf,"IF");
				bool _ifdef = !strcmp(buf,"IFDEF");
				bool _ifndef = !strcmp(buf,"IFNDEF");
				bool _if_inverted = false;
				bool got_key_word = get_key_word(buf,sizeof(buf));
				if (got_key_word && _if && !strcmp(buf,"NOT")) {
					_if_inverted = true;
					got_key_word = get_key_word(buf,sizeof(buf));
				}
				if (got_key_word && _if && !strcmp(buf,"DEFINED")) {
					got_key_word = get_key_word(buf,sizeof(buf));
					if (got_key_word && !strcmp(buf,"(")) {
						got_key_word = get_key_word(buf,sizeof(buf));
						char temp[64];
						if (!got_key_word || !get_key_word(temp,sizeof(temp)) || strcmp(temp,")"))
							scr_errorf("Missing ')' after 'DEFINED('");
					}
					_ifdef = !_if_inverted;
					_ifndef = _if_inverted;
					_if = false;
				}
				if (got_key_word) {
					bool found = false;
					int value = 0;
					if (!strcmp(buf,"FINAL")) {
						scr_errorf("FINAL preprocessor symbol is deprecated, use #IF IS_FINAL_BUILD instead.");
						found = PARAM_final.Get() != 0, value = 0;
					}
					else if (!strcmp(buf,"DEFINE_DEBUG_MODE")) {
						scr_errorf("DEFINE_DEBUG_MODE preprocessor symbol is deprecated, use #IF IS_DEBUG_BUILD instead.");
						found = PARAM_final.Get() == 0, value = 0;
					}
					else if (!strcmp(buf,"IS_FINAL_BUILD"))
						found = true, value = PARAM_final.Get() != 0;
					else if (!strcmp(buf,"IS_DEBUG_BUILD"))
						found = true, value = PARAM_final.Get() == 0;
					else for (int scope=s_Symbols.GetCount()-1; scope>=0; --scope) {
						scrSym **s = s_Symbols[scope]->Access(buf);
						if (s) {
							scrSymInt *intlit = static_cast<scrSymInt*>(*s);
							found = true;
							if (intlit) {
								if (intlit->LexVal == INTLIT)
									value = intlit->Value;
								// code actually does some wacky stuff like #ifdef SOME_FUNCTION_NAME, which is legitimate.
								// else
								//	scr_warningf("Symbol '%s' is not a CONST_INT, will be treated as integer zero",buf);
								break;
							}
							else if (_if)
								scr_warningf("Symbol is tested via #IF too close to its definition, please insert a dummy declaration (like another CONST_INT) in between them");
						}
					}
					if (_if) {
						if (!found)
							scr_warningf("Unknown symbol '%s' in #if",buf);
						else {
							_ifdef = true;
							found = (value != 0);
							if (_if_inverted)
								found = !found;
						}
					}
					if ((_ifdef && found) || (_ifndef && !found)) {
						++s_Endif;
					}
					else {	// skip the block
						++s_InDisabledBlock;
						int EndifsToMatch = 1;
						for (;;) {
							token = scr_lex_inner();
							if (token == EOF) {
								scr_errorf("End of file found before #IF[N]DEF block was completed?");
								return token;
							}
							if (token == '#' && get_key_word(buf,sizeof(buf)))
							{
								if (!strcmp(buf,"ENDIF"))
								{
									EndifsToMatch--;
									if (EndifsToMatch == 0)
									{
										break;
									}
								}
								else if ( /*(!strcmp(buf,"IFDEF")) || (!strcmp(buf,"IFNDEF")) ||*/ (!strcmp(buf,"IF")) )
								{
									EndifsToMatch++;
								}
								else
								{
									scr_errorf("String following # should be IF or ENDIF (this is inside a block of code that has been disabled by an earlier #IF[N]DEF");
								}
							}
						}
						--s_InDisabledBlock;
					}
				}
				else
					scr_errorf("Expected symbol name after #if" /*,_ifdef?"def":_ifndef?"ndef":""*/);
			}
			else
				scr_errorf("Expected keyword after '#' character");
		}
	}	
	return token;
}
/* int scr_lex() {
	int rv = scr_lex_1();
	printf("lex: %d\n",rv);
	return rv;
} */
