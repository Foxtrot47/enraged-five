USING "rage_builtins.sch"

SCRIPT
	INT I
	INT J
	FOR I = 1 TO 10
		IF I < 5
			RELOOP
		ENDIF
		J++
		IF I >= 8
			BREAKLOOP
		ENDIF
		I++
	ENDFOR
	PRINTLN("I = ", I, " and J = ", J)
ENDSCRIPT
