// 
// scriptcompiler/allocator.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "allocator.h"
#include "system/xtl.h"

namespace rage {

void *scrAllocator::sm_Heap;
int scrAllocator::sm_HeapSize = 0;
int scrAllocator::sm_HeapOffset = 0;
int scrAllocator::sm_HeapMapped = 0;

static const int s_pageMask = 4 * 1024 * 1024 - 1;

void scrAllocator::SetHeap(int heapSize) {
	heapSize = (heapSize + s_pageMask) & ~s_pageMask;
	sm_HeapSize = heapSize;
	sm_HeapOffset = 0;
	sm_HeapMapped = 0;
	sm_Heap = VirtualAlloc(NULL, heapSize, MEM_RESERVE, PAGE_READWRITE);
	if (!sm_Heap)
		Quitf("Unable to allocate %d byte virtual heap for script",heapSize);
}


void *scrAllocator::Allocate(size_t size) {
	size = (size + 15) & ~15;
	void *result = (void*)((char*)sm_Heap + sm_HeapOffset);
	sm_HeapOffset += (int)size;
	if (sm_HeapOffset > sm_HeapSize)
		Quitf("Script compiler heap overflow. The current size is %d. Try specifying a new size with -nodeHeapSize=<new size> in the Custom compiler settings of the .scproj file. In the Script Editor, they can be found in Tools -> Settings -> Compiling -> SanScript -> Custom", sm_HeapSize);
	if (sm_HeapOffset > sm_HeapMapped) {
		sm_HeapMapped = (sm_HeapOffset + s_pageMask) & ~s_pageMask;
		if (!VirtualAlloc(sm_Heap, sm_HeapMapped, MEM_COMMIT, PAGE_READWRITE))
			Quitf("Unabled to expand virtual heap to %d bytes",sm_HeapMapped);
	}
		
	return result;
}

}	// namespace rage
