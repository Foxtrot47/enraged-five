USING "rage_builtins.sch"

BOOL b_or_check_1 = TRUE
BOOL b_or_check_2 = FALSE

/* 
expected output:
[Script] i = 0 A
[Script] i = 1 A
[Script] i = 2 A
[Script] i = 3 A  B  C  D
[Script] i = 4 A
[Script] i = 5 A
*/

SCRIPT

		INT i = 0
		REPEAT 6 i
			PRINTSTRING("i = ")
			PRINTINT(i)
		
			IF (b_or_check_1 OR b_or_check_2)
				PRINTSTRING(" A ")
			ENDIF
			
			IF i = 3
				PRINTSTRING(" B ")
			ENDIF
			
			IF ((b_or_check_1 OR b_or_check_2) AND i = 3)
				PRINTSTRING(" C ")
			ENDIF
			
			IF (i = 3 AND (b_or_check_1 OR b_or_check_2))
				PRINTSTRING(" D ")
			ENDIF
			
			PRINTNL()
		ENDREPEAT
ENDSCRIPT
