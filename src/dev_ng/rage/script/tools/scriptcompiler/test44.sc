USING "rage_builtins.sch"

PROC CheckEnum(ENUM_TO_INT A)
	// The only valid conversion from an ENUM_TO_INT parameter to
	// an INT is during assignment.
	INT B = A
	PRINTINT(B)
	PRINTSTRING("\n")
ENDPROC

PROC CheckEnumRef(ENUM_TO_INT &A)
	// The only valid conversion from an ENUM_TO_INT parameter to
	// an INT is during assignment.
	INT B = A
	PRINTINT(B)
	PRINTSTRING("\n")
ENDPROC

ENUM SAMPLE
	ZERO, ONE, TWO, THREE
ENDENUM


SCRIPT
	SAMPLE S = THREE
	// CheckEnum(1)
	CheckEnum(ONE)
	CheckEnum(TWO)
	CheckEnum(S)
	CheckEnumRef(S)
ENDSCRIPT
