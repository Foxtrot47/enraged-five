USING "rage_builtins.sch"

SCRIPT
	INT I
	FLOAT Array[5]
	FOR I = 0 TO 4
		Array[I] = TO_FLOAT(I * I)
	ENDFOR
	FOR I = 0 TO 4
		PRINTFLOAT (Array[I])
		PRINTNL ()
	ENDFOR
ENDSCRIPT

