%expect 1	// ELIF

%token IF ELSE ELIF ENDIF WHILE ENDWHILE REPEAT ENDREPEAT SCRIPT ENDSCRIPT RETURN EXIT ENDPROC ENDFUNC
%token AND OR NOT GE LE NE LSH RSH FALLTHRU FOR FORWARD ENDFOR TO STEP TYPEDEF
%token PLUS_TIME MINUS_TIME STRLIT USING CATCH THROW CALL
%token TYPENAME FUNCNAME PROCNAME INTLIT FLOATLIT NEWSYM VAR_REF FIELD TRUE FALSE LABEL
%token SWITCH ENDSWITCH CASE DEFAULT BREAK GOTO SCOPE ENDSCOPE BREAKLOOP RELOOP
%token CONST_INT CONST_FLOAT FUNC PROC NATIVE GLOBALS ENDGLOBALS ENUM ENDENUM ENUMLIT STRUCT ENDSTRUCT NL
%token PLUSPLUS MINUSMINUS PLUS_EQ MINUS_EQ TIMES_EQ DIVIDE_EQ COUNT_OF ENUM_TO_INT INT_TO_ENUM SIZE_OF
%token AND_EQ OR_EQ XOR_EQ
%token NATIVE_TO_INT INT_TO_NATIVE HASH DEBUGONLY VARARGS VARARGS1 VARARGS2 VARARGS3 STRICT_ENUM HASH_ENUM STRICT_HASH_ENUM TWEAK_INT TWEAK_FLOAT ANDALSO ORELSE
%token UNUSED_PARAMETER

%type <ival> INTLIT optStep enumExpr intexpr optBlockIndex
%type <varref> VAR_REF lvalue rvalue arrayReference
%type <newsymhash> NEWSYM
%type <label> LABEL
%type <field> FIELD
%type <fval> FLOATLIT floatexpr
%type <sval> STRLIT
%type <type> TYPENAME optArrays foptArrays formal optArraysOrDefaultValue
%type <typelist> formalList optFormalList optFormalOrVarargsList
// We "help" the lexer by using a different terminal symbol for funcs and procs that don't
// take any parameters to avoid parsing ambiguities.
%type <cmd> FUNCNAME PROCNAME
%type <stmt> program stmt optStmtList stmtList scopedStmtList optElses lvarDeclList lvarDecl
%type <expr> expr primaryExpr unaryExpr multExpr addExpr optGlobalOrStaticAssignment
%type <expr> relExpr equalityExpr logAndExpr logOrExpr bitAndExpr bitOrExpr bitXorExpr conditionalExpr
%type <expr> optAssignment exprOrDefault
%type <exprlist> argExprList exprOrDefaultList
%type <eval> ENUMLIT
%type <lineno> FUNC PROC SCRIPT IF WHILE REPEAT FOR ELIF '=' PLUS_EQ MINUS_EQ TIMES_EQ DIVIDE_EQ AND_EQ OR_EQ XOR_EQ PLUSPLUS MINUSMINUS SCOPE BREAKLOOP RELOOP
%type <lineno_bval> debugProc debugFunc 
%type <enumtype> enumOrStrictEnum
%type <guid> optGuid

%{
#include "node.h"
#include "atl/ownedptr.h"
#include "atl/string.h"
#include "script/opcode.h"
#include "file/stream.h"
#include "system/magicnumber.h"
#include "system/param.h"

XPARAM(final);
XPARAM(shortcircuit);
PARAM(dumpallglobals,"Dump all known globals to standard output");
PARAM(dumpusedglobals,"Dump all globals used by the script to standard output");
PARAM(dumpincludes,"Dump all include files referenced to standard output");
PARAM(werror,"Warnings as errors");
PARAM(warnnestedunused,"Warn about unused variables in nested blocks");
PARAM(printstaticsinheadersinclusive, "In the .scd file, print the number of static variables declared in each .sch file (and all .sch files that they include)");
PARAM(printstaticsinheadersexclusive, "In the .scd file, print the number of static variables declared in each .sch file");
#define scr_error scr_errorf /* for bison internal use */
#define YYERROR_VERBOSE

extern void scr_errorf(const char*,...);
extern void scr_warningf(const char*,...);
void scr_duplicate(const char *what,int line,const char *file) {
	errorf("%s(%d) : Location of original %s definition",file,line,what);
	scr_errorf("Duplicate %s name",what);
}
void scr_using(const char*filename);
static char s_LastToken[MAX_TOKEN_LENGTH];
static char s_LastNative[MAX_TOKEN_LENGTH];
static char s_CurrentProg[MAX_TOKEN_LENGTH];
static char s_CurrentTypeDef[MAX_TOKEN_LENGTH];
extern char outname[];
extern char namespace_name[];

using namespace rage;

static bool s_HadGoto;
static bool s_AllowGotos = true;
static bool s_AllowNativeToInt = true;
static bool s_AllowIntToNative = true;
static bool s_NoEscape;
static bool s_IgnoreDebugStuff;
int g_SaveGlobalsBlock = -1;
bool scr_had_warnings;

int scr_arg_struct_size;
char scr_arg_string[128];
int scr_line = 1;
fiStream *scr_stream, *scr_globalinfo, *scr_dep_file;
char scr_stream_name[256], scr_global_name[256];
u32 scr_stream_name_hash;
scrTypeObject s_StructRef(NULL);

%}

%union {
	bool bval;
	int ival;
	float fval;
	const char *sval;
	scrNodeStmt *stmt;
	scrNodeExpr *expr;
	struct {
		struct scrSym **sym;
		u32 hash;
	} newsymhash;
	struct {
		int lineno;
		bool bval;
	} lineno_bval;
	scrDataList *datalist;
	struct scrSymVar *varref;
	const scrField *field;
	const scrType *type;
	const scrTypeList *typelist;
	scrCommandInfo *cmd;
	scrExprList *exprlist;
	scrValue value;
	scrTypeEnum::Enumerant *eval;
	int lineno;
	class scrNodeLabel *label;
	int enumtype;
	u64 guid;
}

%{

#define YYMAXDEPTH	2048

#if __WIN32
#pragma warning(disable:4244)	// bison.simple(253) : warning C4244: '=' : conversion from 'int' to 'short', possible loss of data
#pragma warning(disable:4701)	// bison.simple(573) : warning C4701: local variable 'yychar1' may be used without having been initialized
#pragma warning(disable:4668)	// bison.simple(148) : error C4668: '__GNUC__' is not defined as a preprocessor macro, replacing with '0' for '#if/#elif'
#pragma warning(disable:4345)	// behavior change: an object of POD type constructed with () will be default-initialized
#endif

#define YYDEBUG !__PS2

#include "script/hash.h"
#include "script/thread.h"
#include "file/asset.h"
#include "string/string.h"
#include "script_platform.h"
#include <malloc.h>
#include <string.h>
#include <stdarg.h>

void scr_errorf(const char*,...);
int scr_lex();


const int MaxGlobalsOrStatics = 256*1024;

const int MAX_NESTING = 256;

extern eScriptPlatform scriptPlatform;

extern fiStream *scr_structreffile;
extern fiStream *scr_globalreffile;

extern u32 globalHashesToOutputToDebugFile[scrProgram::MAX_GLOBAL_BLOCKS];

static atFixedArray<const scrType*,MAX_NESTING> s_Structs;
static atFixedArray<bool,MAX_NESTING> s_ReadOnly;
static atFixedArray<scrNodeSwitch*,MAX_NESTING> s_Switches;
static atFixedArray<scrValue,MaxGlobalsOrStatics> s_Globals[scrProgram::MAX_GLOBAL_BLOCKS];
static atFixedArray<scrValue,MaxGlobalsOrStatics> s_Statics;
static int s_LocalOffset, s_StaticOffset, s_GlobalOffsets[scrProgram::MAX_GLOBAL_BLOCKS];
static int s_ScopeLevel;
static const scrType *s_BaseType;
static const scrType *s_ReturnType;

fiStream *scr_debuginfo, *scr_native_file;

static void CheckSwitch() {
	if (s_Switches.GetCount()) {
		if (!s_Switches.Top()->HaveLabels())
			scr_warningf("Code in SWITCH before any CASE or DEFAULT label.");
		s_Switches.Top()->SetHadBreak(false);
		if (s_Switches.Top()->GetLastLabel()) {
			s_Switches.Top()->GetLastLabel()->SetWasLastLabel();
			s_Switches.Top()->SetLastLabel(0);
		}
	}
}

static const char *string_duplicate(const char *str) {
	Assert(str);
	char *s = (char*) scrAllocator::Allocate(strlen(str)+1);
	strcpy(s,str);
	return s;
}

void emit_parameter_source(int script_channel,const scrTypeList *list,bool isNative) {
	if (!isNative)
		scrNode::EmitChannel(script_channel,"struct script_statics*");
	else if (isNative && (list == &g_VARARGS || list == &g_VARARGS1 || list == &g_VARARGS2 || list == &g_VARARGS3)) {
		scrNode::EmitChannel(script_channel,"unsigned __paramCount,unsigned __paramTypes,...");
		return;
	}
	
	while (list) {
		if (!isNative)
			scrNode::EmitChannel(script_channel,",");
		scrNode::EmitChannel(script_channel,list->Left->GetCType());
		if (isNative && list->Right)
			scrNode::EmitChannel(script_channel,",");
		list = list->Right;
	}
}

struct scrSym 
{
#if RAGE_ENABLE_RAGE_NEW
	static void *operator new(size_t size,const char*,int) { return scrAllocator::Allocate(size); }
#else
	static void *operator new(size_t size) { return scrAllocator::Allocate(size); }
#endif
	scrSym(int lexVal) : LexVal(lexVal) { 
		Filename = string_duplicate(scr_stream_name);
		LineNumber = scr_line;
	}
	virtual ~scrSym() { }
	virtual void CheckUnreferenced() const { }
	virtual void WasReferenced() { }
	virtual void Init(YYSTYPE &yylval) = 0;
	int LexVal;
	const char *Filename;
	int LineNumber;
};

struct scrSymLabel: public scrSym {
	scrSymLabel(scrNodeLabel *target) : scrSym(LABEL) {
		Target = target;
	}
	void Init(YYSTYPE &yylval) { yylval.label = Target; }
	scrNodeLabel *Target;
};

static bool s_InStackFrame;

static scrLocal *s_FirstLocal, **s_NextLocal;

static int s_InLoop;		// True if we're within a loop of some kind

struct scrSymVar: public scrSym {
	scrSymVar(const scrType	*type,scrNodeExpr *address) : scrSym(VAR_REF), Type(type), Address(address), Referenced(true), Reinitialized(true) { Name = NULL; }
	scrSymVar(const scrType *type) : scrSym(VAR_REF), Type(type), Referenced(false), Reinitialized(s_InLoop == 0) {
		Name = string_duplicate(s_LastToken);
		Address = rage_new scrNodeLocal(type,s_LastToken,s_LocalOffset);
		if (s_InStackFrame) {
			scrLocal *local = rage_new scrLocal(type,s_LastToken,s_LocalOffset);
			*s_NextLocal = local;
			s_NextLocal = &local->Next;
		}
		s_LocalOffset += type->GetSize();
		//if (s_LocalOffset > scrThread::c_DefaultStackSize/2 /* || s_LocalOffset > 255 */)
		//	scr_errorf("Too many local variables in one function");
	}
	void WasReferenced() { Referenced = true; }
	static void InitializeVar(atFixedArray<scrValue,MaxGlobalsOrStatics> &array,scrValue *initValue,const scrType *type) {
		int size = type->GetSize();
		int count = array.GetCount();
		if (count+size > MaxGlobalsOrStatics)
			scr_errorf("Too many global or static variables (limit is %d)",MaxGlobalsOrStatics);
		array.Resize(count + size);
		scrValue *base = &array[count];
		if(type->GetArrayCount() || !initValue) {
			type->Construct(base);
		}
		if (initValue) {
			//If it's an array don't overwrite the array length.
			base = base + (type->GetArrayCount() ? 1 : 0);
			for (int i=0; i<size; i++)
				base[i] = initValue[i];
		}
	}
	scrSymVar(const scrType *type,scrValue *initValue,int globalBlock) : scrSym(VAR_REF), Type(type), Referenced(globalBlock != -1 && !PARAM_dumpusedglobals.Get()), Reinitialized(true) {
		if (globalBlock != -1 && type->ContainsStringOrFuncPtr())
			scr_warningf("Cannot safely declare a STRING or TYPEDEF variable '%s' in GLOBALS block %d.",s_LastToken,globalBlock);
		Name = string_duplicate(s_LastToken);
		int offset = globalBlock != -1? (globalBlock << scrProgram::MAX_GLOBAL_BLOCKS_SHIFT) | s_GlobalOffsets[globalBlock] : s_StaticOffset;
		Address = globalBlock != -1? (scrNodeExpr*)(rage_new scrNodeGlobal(type,Name,offset)) : (scrNodeExpr*)(rage_new scrNodeStatic(type,Name,s_StaticOffset));
		Assert(!s_InStackFrame);
		if (scr_debuginfo)
			type->EmitDebugInfo(scr_debuginfo,globalBlock != -1?"GLOBAL":"STATIC",s_LastToken,offset);
		IsGlobal = globalBlock != -1;
		if (globalBlock != -1) {
			if (scr_global_name[0] && !scr_globalinfo && g_SaveGlobalsBlock != -1) {
				scr_globalinfo = fiStream::Create(scr_global_name);
				if (scr_globalinfo)
					fprintf(scr_globalinfo,"size,count,totalSize,name\r\n");
			}
			if (scr_globalreffile && g_SaveGlobalsBlock != -1)
				fprintf(scr_globalreffile,"DECL,%s,%u\r\n",s_LastToken,offset);
			if (scr_globalinfo)
				type->EmitGlobalInfo(scr_globalinfo,s_LastToken);
			s_GlobalOffsets[globalBlock] += type->GetSize();
			if (s_GlobalOffsets[globalBlock] > scrProgram::GLOBAL_SIZE_MASK)
				scr_errorf("Too many global variables in a block.");
			InitializeVar(s_Globals[globalBlock],initValue,type);
		}
		else {
			s_StaticOffset += type->GetSize();
			InitializeVar(s_Statics,initValue,type);
		}
	}
	void CheckUnreferenced() const {
		if (IsGlobal && ((PARAM_dumpusedglobals.Get() && Referenced) || PARAM_dumpallglobals.Get()))
			Displayf("GLOBAL:%s",Name);
		else if (!Referenced) {
			if (PARAM_werror.Get())
			{
				errorf("%s(%d) : Unreferenced variable '%s'.",Filename,LineNumber,Name);
				scr_had_warnings = true;
			}
			else
				Displayf("Info: %s(%d) : Unreferenced variable '%s'.",Filename,LineNumber,Name);
		}
	}
	void SetReadOnly(bool flag) { if (Address) Address->SetReadOnly(flag); }
	bool GetReadOnly() { return Address? Address->GetReadOnly() : false; }
	const scrType *Type;
	virtual void Init(YYSTYPE &yylval) { yylval.varref = this; }
	scrNodeExpr *Address;
	const char *Name;
	bool Referenced, IsGlobal, Reinitialized;
};

struct scrSymInt: public scrSym {
	scrSymInt(int ival) : scrSym(INTLIT), Value(ival) { }
	void Init(YYSTYPE &yylval) { yylval.ival = Value; }
	int Value;
};

struct scrSymEnum: public scrSym {
	scrSymEnum(scrTypeEnum::Enumerant *e) : scrSym(ENUMLIT), Enum(e) { }
	void Init(YYSTYPE &yylval) { yylval.eval = Enum; }
	scrTypeEnum::Enumerant *Enum;
};

struct scrSymFloat: public scrSym {
	scrSymFloat(float fval) : scrSym(FLOATLIT), Value(fval) { }
	void Init(YYSTYPE &yylval) { yylval.fval = Value; }
	float Value;
};

struct scrSymTypename: public scrSym {
	scrSymTypename(const scrType *type) : scrSym(TYPENAME), Type(type) { }
	void Init(YYSTYPE &yylval) { yylval.type = Type; }
	const scrType *Type;
};

struct scrSymField: public scrSym {
	scrSymField(const scrField *field) : scrSym(FIELD), Field(field) { }
	void Init(YYSTYPE &yylval) { yylval.field = Field; }
	const scrField *Field;
};

struct scrSymCommand: public scrSym {
	scrSymCommand(const char *progname,const scrType *returnType,const scrTypeList *params,u64 nativeHash,bool debugOnly) : scrSym(returnType? FUNCNAME : PROCNAME) { 
		Info.ReturnType = returnType;
		Info.Parameters = params;
		Info.NativeHash = nativeHash;
		Info.NativeSlot = -1;
		Info.Address = 0;
		Info.Tree = NULL;
		Info.Filename = string_duplicate(scr_stream_name);
		Info.LineNumber = scr_line;
		Info.ProgName = HeapString(progname);
		Info.WasStubEmitted = false;
		Info.FirstRef = NULL;
		Info.DebugOnly = debugOnly;
		if (nativeHash && scr_native_file)
			fprintf(scr_native_file,"%016llx %s %d %s\r\n",nativeHash,s_LastNative,scr_line,scr_stream_name);
		Info.HasVectorReferences = false;
		while (params) {
			if (params->Left->IsVectorReference())
				Info.HasVectorReferences = true;
			params = params->Right;
		}
	}
	void Init(YYSTYPE &yylval) { yylval.cmd = &Info; }
	scrCommandInfo Info;
};

typedef atMap<ConstString,scrSym*> scrSymTab;
static atFixedArray<scrSymTab*, MAX_NESTING> s_Symbols;
static scrSymTab *s_CurrentSubprogram;		// for GOTO's
static bool s_HadDebugOnlyCalls;

scrSymTab* CheckForUnreferenced(scrSymTab *tab) {
	if (s_HadDebugOnlyCalls)
		s_HadDebugOnlyCalls = false;
	else
	{
		scrSymTab::Iterator entry = tab->CreateIterator();
		for (entry.Start(); !entry.AtEnd(); entry.Next())
		{
			if (entry.GetData())
				entry.GetData()->CheckUnreferenced();
		}
	}
	return tab;
}


static scrSymCommand *s_TimeStep;

static scrNodeExpr* TimeStep(scrNodeExpr* inner) {
	// The timestep is stored as the first global variable.
	return rage_new scrNodeArithmetic(inner,OP_IMUL,OP_FMUL,OP_VMUL,
		rage_new scrNodeFunctionCall(&s_TimeStep->Info,NULL,NULL),"*","*","*");
}

static int s_ParamSize;

static int s_InGlobalsBlock = -1;
static u64 s_HadGlobals;

static scrTypeEnum *s_Enum;

static scrTypeStruct *s_Struct;

//Statements that initialize statics and globals.
static scrNodeStmt* s_GlobalsInit;
static scrNodeStmt* s_StaticsInit;

static bool s_NeedNewline;

static void BeginStackFrame(const scrType * /*rtype*/) {
	Assert(!s_InStackFrame);
	s_InStackFrame = true;
	s_NextLocal = &s_FirstLocal;
}
	
static void EndStackFrame() {
	Assert(s_InStackFrame);
	s_InStackFrame = false;
	s_FirstLocal = NULL;
}

static void PromoteToSubprogram(struct scrSym **&symRef)
{
	// force the symbol into a higher symbol table
	if (s_Symbols.Top() != s_CurrentSubprogram)
	{
		// printf("haha fixing label %s\n",s_LastToken);
		Assert(s_CurrentSubprogram);
		s_Symbols.Top()->Delete(s_LastToken);
		symRef = &s_CurrentSubprogram->operator[](ConstString(s_LastToken));
	}
}

%}

%%

program
	: { 
			s_Symbols.Push(rage_new scrSymTab);

			s_Symbols.Top()->Create();
			s_Symbols.Top()->Recompute(65147);			//	To avoid an assert in atHashNextSize() (map.cpp) when compiling cops_and_crooks.sc
			s_Symbols.Top()->SetAllowRecompute(false);	//	I'll set the top-level s_Symbols map to have a size that is the last prime number before 65167
			
			s_LocalOffset = 0;
			s_StaticOffset = 0;
			memset(s_GlobalOffsets,0,sizeof(s_GlobalOffsets));

			strcpy(s_LastNative,"TIMESTEP");
			s_Symbols.Top()->Insert(ConstString("TIMESTEP"),s_TimeStep = rage_new scrSymCommand("TIMESTEP",&scrTypeFloat::Instance,NULL,scrComputeHash("TIMESTEP"),false));
			s_Symbols.Top()->Insert(ConstString("NULL"),rage_new scrSymVar(&scrTypeNull::Instance,rage_new scrNodeNull(&scrTypeNull::Instance)));
			s_Symbols.Top()->Insert(ConstString("INVALID_POOLINDEX"),rage_new scrSymInt(-1));

			s_Symbols.Top()->Insert(ConstString("BOOL"),rage_new scrSymTypename(&scrTypeBool::Instance));
			s_Symbols.Top()->Insert(ConstString("FLOAT"),rage_new scrSymTypename(&scrTypeFloat::Instance));
			s_Symbols.Top()->Insert(ConstString("INT"),rage_new scrSymTypename(&scrTypeInt::Instance));
			s_Symbols.Top()->Insert(ConstString("STRING"),rage_new scrSymTypename(&scrTypeString::Instance));
			s_Symbols.Top()->Insert(ConstString("TEXT_LABEL"),rage_new scrSymTypename(rage_new scrTypeTextLabel(16)));
			s_Symbols.Top()->Insert(ConstString("TEXT_LABEL_3"),rage_new scrSymTypename(rage_new scrTypeTextLabel(4)));
			s_Symbols.Top()->Insert(ConstString("TEXT_LABEL_7"),rage_new scrSymTypename(rage_new scrTypeTextLabel(8)));
			s_Symbols.Top()->Insert(ConstString("TEXT_LABEL_15"),rage_new scrSymTypename(rage_new scrTypeTextLabel(16)));
			s_Symbols.Top()->Insert(ConstString("TEXT_LABEL_23"),rage_new scrSymTypename(rage_new scrTypeTextLabel(24)));
			s_Symbols.Top()->Insert(ConstString("TEXT_LABEL_31"),rage_new scrSymTypename(rage_new scrTypeTextLabel(32)));
			s_Symbols.Top()->Insert(ConstString("TEXT_LABEL_63"),rage_new scrSymTypename(rage_new scrTypeTextLabel(64)));
			s_Symbols.Top()->Insert(ConstString("VECTOR"),rage_new scrSymTypename(&scrTypeVector::Instance));
			s_Symbols.Top()->Insert(ConstString("POOLINDEX"),rage_new scrSymTypename(&scrTypePoolIndex::Instance));
			s_Symbols.Top()->Insert(ConstString("STRINGHASH"),rage_new scrSymTypename(&scrTypeStringHash::Instance));
			
			scrNode::LineInfo &li = scrNode::sm_LineInfo.Append();
			li.m_BasePc = 0;
			li.m_LineNumber = 1;
			li.m_FileIndex = 0;

		}
			optDeclList SCRIPT optStruct
		{
			s_Symbols.Push(s_CurrentSubprogram = rage_new scrSymTab);
			
			s_LocalOffset = 2;	// Leave room for return address and frame pointer here (never any parameters)
								// Any argStruct is actually implemented as hidden member variables.
			strcpy(s_LastToken,"*SCRIPT");	// pick an impossible symbol name
			BeginStackFrame(NULL);
		}
			optStmtList ENDSCRIPT 
		{
			delete CheckForUnreferenced(s_Symbols.Pop());

			s_CurrentSubprogram = NULL;

			EndStackFrame();

			//Make sure no code has been emitted at this point.
			Assert(scrNode::GetPC() == 0);
			
			scrNodeStmt* stmt = $6;

			//The first few insns of the script initialize statics/globals
			if(s_GlobalsInit && g_SaveGlobalsBlock != -1)
			{
				stmt = rage_new scrNodeStmts(s_GlobalsInit,stmt);
			}

			if(s_StaticsInit)
			{
				stmt = rage_new scrNodeStmts(s_StaticsInit,stmt);
			}

			scrNode::SetRoot(rage_new scrNodeScript("SCRIPT",$3,stmt,0,s_LocalOffset,NULL,s_FirstLocal)); 
			if (s_Statics.GetCount())
				scrNode::SetStatics(&s_Statics[0],s_Statics.GetCount());
			if (s_Globals[g_SaveGlobalsBlock != -1? g_SaveGlobalsBlock : 0].GetCount())
				scrNode::SetGlobals(&s_Globals[g_SaveGlobalsBlock != -1? g_SaveGlobalsBlock : 0][0],s_Globals[g_SaveGlobalsBlock != -1? g_SaveGlobalsBlock : 0].GetCount());
				
			for (u32 block_loop=0; block_loop<scrProgram::MAX_GLOBAL_BLOCKS; block_loop++)
			{
				if (s_Globals[block_loop].GetCount() > 0)
				{
					globalHashesToOutputToDebugFile[block_loop] = scrProgram::CalculateGlobalHash(&s_Globals[block_loop][0], s_Globals[block_loop].GetCount());
				}
				else
				{
					globalHashesToOutputToDebugFile[block_loop] = 0;
				}
			}
				
			CheckForUnreferenced(s_Symbols.Pop());
		}
	;
	
optStruct
	: '(' TYPENAME NEWSYM ')'
			{
				scr_arg_struct_size = $2->GetSize();
				scrSymVar *var = rage_new scrSymVar($2,NULL,-1); 
				*$3.sym = var;
				formatf(scr_arg_string,"%s %s",$2->GetCType(),s_LastToken);
			}
	| /* empty */ 
			{ 
				scr_arg_struct_size = 0; 
			}
	;
	
optDeclList
	: declList
	|
	;
	
declList
	: declList decl
	| decl
	;

debugFunc
	: DEBUGONLY FUNC	{ $$.bval = true; $$.lineno = $2; }
	| FUNC				{ $$.bval = false; $$.lineno = $1; }
	;
	
debugProc
	: DEBUGONLY PROC	{ $$.bval = true; $$.lineno = $2; }
	| PROC				{ $$.bval = false; $$.lineno = $1; }
	;
	
decl
	: TYPENAME { s_BaseType = $1; s_NeedNewline = true; } gvarDeclList NL
	| GLOBALS optBlockIndex
		{ 
			if (s_InGlobalsBlock != -1) 
				scr_errorf("GLOBALS block cannot nest");
			else if ((1ULL << $2) & s_HadGlobals) 
				scr_errorf("This globals block already defined");
			else 
				s_InGlobalsBlock = $2; 
		}
	| ENDGLOBALS
		{ 
			if (s_InGlobalsBlock == -1) 
				scr_errorf("ENDGLOBALS without GLOBALS");
			else {
				s_HadGlobals |= (1ULL << s_InGlobalsBlock);
				s_InGlobalsBlock = -1; 
			}
		}
	| FORWARD enumOrStrictEnum NEWSYM
		{
			*$3.sym = rage_new scrSymTypename(rage_new scrTypeEnum(s_LastToken,$2));
		}
	| FORWARD enumOrStrictEnum TYPENAME 
		{
			if (!$3->IsEnum()) 
				scr_errorf("Trying to forward declare something that wasn't an ENUM");
		}
	| FORWARD STRUCT NEWSYM
		{
			*$3.sym = rage_new scrSymTypename(rage_new scrTypeStruct(s_LastToken));
		}
	| FORWARD STRUCT TYPENAME 
		{
			if (!$3->IsStruct()) 
				scr_errorf("Trying to forward declare something that wasn't an STRUCT");
		}
	| enumOrStrictEnum NEWSYM 
		{
			*$2.sym = rage_new scrSymTypename(s_Enum = rage_new scrTypeEnum(s_LastToken,$1));
		}
		enumerantList ENDENUM
		{
			s_Enum = NULL;
		}
	| enumOrStrictEnum TYPENAME
		{
			if (!$2->IsEnum())
				scr_errorf("Attempting to redeclare something that wasn't an ENUM");
			else {
				s_Enum = (scrTypeEnum*) $2;
				if (s_Enum->GetLast())
					scr_errorf("Attempting to redeclare an ENUM that was already fully specified");
			}
		}
		enumerantList ENDENUM
		{
			s_Enum = NULL;
		}
	| STRUCT NEWSYM
		{
			*$2.sym = rage_new scrSymTypename(s_Struct = rage_new scrTypeStruct(s_LastToken));
			s_Symbols.Push(rage_new scrSymTab);
			scrNode::EmitChannel(SCRIPT_DECLS,"#ifndef %s_defined\n#define %s_defined\nstruct %s {\n",s_LastToken,s_LastToken,s_LastToken);
		}
		structMemberList ENDSTRUCT
		{
			s_Struct->EmitSource();
			scrNode::EmitChannel(SCRIPT_DECLS,"};\n#endif\n\n");
			s_Struct = NULL;
			// Do not delete the symbol table, just close it.
			// We need it around for when the structure is actually used.
			/*delete*/ s_Symbols.Pop();
		}
	| STRUCT TYPENAME
		{
			if (!$2->IsStruct())
				scr_errorf("Trying to redefine something that isn't a STRUCT as a STRUCT?");
			s_Struct = (scrTypeStruct*)$2;
			if (s_Struct->GetSize())
				scr_errorf("Structure already fully defined");
			s_Symbols.Push(rage_new scrSymTab);
			scrNode::EmitChannel(SCRIPT_DECLS,"#ifndef %s_defined\n#define %s_defined\nstruct %s {\n",s_LastToken,s_LastToken,s_LastToken);
		}
		structMemberList ENDSTRUCT
		{
			s_Struct->EmitSource();
			scrNode::EmitChannel(SCRIPT_DECLS,"};\n#endif\n\n");
			s_Struct = NULL;
			// Do not delete the symbol table, just close it.
			// We need it around for when the structure is actually used.
			/*delete*/ s_Symbols.Pop();
		}
	| NATIVE debugFunc TYPENAME NEWSYM 
		{ 
			s_Symbols.Push(rage_new scrSymTab); 
			safecpy(s_LastNative,s_LastToken,sizeof(s_LastNative));
			scrNode::EmitChannel(SCRIPT_DECLS,"extern %s %s(",$3->GetCType(),s_LastNative);
		} 
	  optFormalOrVarargsList optGuid
		{
			emit_parameter_source(SCRIPT_DECLS,$6,true);
			scrNode::EmitChannel(SCRIPT_DECLS,");\n");
			*($4.sym) = rage_new scrSymCommand(s_LastNative,$3,$6,$7? $7 : $4.hash,$2.bval);
			s_LocalOffset = 0;
			s_ParamSize = 0;
			delete s_Symbols.Pop();
		}
	| NATIVE debugFunc TYPENAME FUNCNAME
		{
			const char *name = $4->Filename;
			int line = $4->LineNumber;
			scr_duplicate("function",line,name);
		}
		optFormalOrVarargsList optGuid
	| NATIVE debugProc NEWSYM 
		{ 
			s_Symbols.Push(rage_new scrSymTab); 
			safecpy(s_LastNative,s_LastToken,sizeof(s_LastNative));
			scrNode::EmitChannel(SCRIPT_DECLS,"extern void %s(",s_LastNative);
		} 
	  optFormalOrVarargsList optGuid
		{ 
			emit_parameter_source(SCRIPT_DECLS,$5,true);
			scrNode::EmitChannel(SCRIPT_DECLS,");\n");
			*($3.sym) = rage_new scrSymCommand(s_LastNative,NULL,$5,$6? $6 : $3.hash,$2.bval);
			s_LocalOffset = 0;
			s_ParamSize = 0;
			delete s_Symbols.Pop();
		}
	| NATIVE debugProc PROCNAME
		{
			const char *name = $3->Filename;
			int line = $3->LineNumber;
			scr_duplicate("procedure",line,name);
		}
		optFormalOrVarargsList optGuid
	| NATIVE NEWSYM ':' TYPENAME
		{ 
			// The only thing that matters here is that the address is unique.
			scrType *type = rage_new scrTypeObject(s_LastToken,$4);
			*($2.sym) = rage_new scrSymTypename(type); 
			const char *myctype = type->GetCType();
			scrNode::EmitChannel(SCRIPT_DECLS,"typedef void *%s;\n",myctype);
		}
	| NATIVE NEWSYM
		{ 
			// The only thing that matters here is that the address is unique.
			scrType *type = rage_new scrTypeObject(s_LastToken);
			*($2.sym) = rage_new scrSymTypename(type); 
			const char *myctype = type->GetCType();
			scrNode::EmitChannel(SCRIPT_DECLS,"typedef void *%s;\n",myctype);
		}
	| NATIVE TYPENAME
		{
			if (!$2->IsObject())
				scr_warningf("Cannot redeclare built-in type as NATIVE (maybe a missing FUNC or PROC here?)");
		}
	| NATIVE TYPENAME ':' TYPENAME
		{ 
			if (!$2->IsObject())
				scr_warningf("Cannot redeclare built-in type as NATIVE (maybe a missing FUNC or PROC here?)");
			else if ($2->Parent != $4)
				scr_warningf("NATIVE derived type redeclared with a different parent.");
		}
	| debugFunc TYPENAME NEWSYM 
		{
			if ($2->GetReference())
				scr_warningf("Returning a reference is not safe.");
			s_ReturnType = $2;
			BeginStackFrame($2);
			safecpy(s_CurrentProg,s_LastToken,sizeof(s_CurrentProg));
			if (!strncmp(s_LastToken,"DEBUG_",6))
				scrNode::EmitChannel(SCRIPT_DECLS,"#ifndef _DEBUG\n#define %s(...) FALSE\n#else\nnamespace %s { static %s %s(",s_LastToken,namespace_name,$2->GetCType(),s_LastToken);
			else
				scrNode::EmitChannel(SCRIPT_DECLS,"namespace %s { static %s %s(",namespace_name,$2->GetCType(),s_LastToken);
			s_Symbols.Push(s_CurrentSubprogram = rage_new scrSymTab);
		}
	  optFormalList 
		{
			emit_parameter_source(SCRIPT_DECLS,$5,false);
			*($3.sym) = rage_new scrSymCommand(s_CurrentProg,$2,$5,0,$1.bval);
			s_LocalOffset += 2;	// Leave room for return address and frame pointer here
		}
	  optStmtList ENDFUNC
		{
			scrNode::EmitChannel(SCRIPT_DECLS,"); } // fwd decl\n");
			if (!strncmp(s_CurrentProg,"DEBUG_",6))
				scrNode::EmitChannel(SCRIPT_DECLS,"#endif\n");
			if (!$7 || !$7->Returns())
				scr_warningf("FUNC is missing a RETURN statement in a code path");
			((scrSymCommand*)(*($3.sym)))->Info.Tree = rage_new scrNodeScript(s_CurrentProg,$1.lineno,$7,s_ParamSize,s_LocalOffset,s_ReturnType,s_FirstLocal);
			s_HadGoto = false;
			delete CheckForUnreferenced(s_Symbols.Pop());
			s_LocalOffset = 0;
			s_ParamSize = 0;
			s_ReturnType = NULL;
			s_CurrentSubprogram = NULL;
			EndStackFrame();
		}
	| debugFunc TYPENAME FUNCNAME
		{
			const char *name = $3->Filename;
			int line = $3->LineNumber;
			scr_duplicate("function",line,name);
		}
		optFormalList optStmtList ENDFUNC
	| debugProc NEWSYM 
		{
			BeginStackFrame(NULL);
			safecpy(s_CurrentProg,s_LastToken,sizeof(s_CurrentProg));
			if (!strncmp(s_CurrentProg,"DEBUG_",6))
				scrNode::EmitChannel(SCRIPT_DECLS,"#ifndef _DEBUG\n#define %s(...)\n#else\nnamespace %s { static void %s(",s_LastToken,namespace_name,s_LastToken);
			else
				scrNode::EmitChannel(SCRIPT_DECLS,"namespace %s { static void %s(",namespace_name,s_LastToken);
			s_Symbols.Push(s_CurrentSubprogram = rage_new scrSymTab);
		}
	  optFormalList 
		{
			emit_parameter_source(SCRIPT_DECLS,$4,false);
			*($2.sym) = rage_new scrSymCommand(s_CurrentProg,NULL,$4,0,$1.bval);
			s_LocalOffset += 2;	// Leave room for return address and frame pointer here
		}
	  optStmtList ENDPROC
		{
			scrNode::EmitChannel(SCRIPT_DECLS,"); } // fwd decl\n");
			if (!strncmp(s_CurrentProg,"DEBUG_",6))
				scrNode::EmitChannel(SCRIPT_DECLS,"#endif\n");
			((scrSymCommand*)(*($2.sym)))->Info.Tree = rage_new scrNodeScript(s_CurrentProg,$1.lineno,$6,s_ParamSize,s_LocalOffset,NULL,s_FirstLocal);
			s_HadGoto = false;
			// Don't warn about unreferenced variables in a DEBUGONLY PROC
			delete CheckForUnreferenced(s_Symbols.Pop());
			s_LocalOffset = 0;
			s_ParamSize = 0;
			s_CurrentSubprogram = NULL;
			EndStackFrame();
		}
	| debugProc PROCNAME
		{
			const char *name = $2->Filename;
			int line = $2->LineNumber;
			scr_duplicate("function",line,name);
		}
		optFormalList optStmtList ENDPROC
	| CONST_INT { s_NeedNewline = true; } NEWSYM expr NL
		{
			int ival;
			if ($4->IsConstant(ival)) {
				*($3.sym) = rage_new scrSymInt(ival);
			}
			else
				scr_errorf("CONST_INT expression must be constant");
		}
	| TWEAK_INT NEWSYM expr
		{
			int ival;
			/* if (s_InGlobalsBlock)
				scr_errorf("TWEAK_INT cannot appear in a GLOBALS block");
			else */ if (PARAM_final.Get()) {
				if ($3->IsConstant(ival))
					*($2.sym) = rage_new scrSymInt(ival);
				else
					scr_errorf("TWEAK_INT expression must be constant in final build.");
			}
			else {
				scrSymVar *var = rage_new scrSymVar(&scrTypeInt::Instance,NULL,s_InGlobalsBlock);
				*($2.sym) = var;
				if (s_InGlobalsBlock == -1 || g_SaveGlobalsBlock == s_InGlobalsBlock) {
					scrNodeStmt* stmt =	rage_new scrNodeAssign(scr_line,&scrTypeInt::Instance,var->Address,$3,true);
					if (s_InGlobalsBlock != -1)
						s_GlobalsInit = s_GlobalsInit ? rage_new scrNodeStmts(s_GlobalsInit,stmt) : stmt;
					else
						s_StaticsInit = s_StaticsInit ? rage_new scrNodeStmts(s_StaticsInit,stmt) : stmt;
				}
			}
		}
	| TYPEDEF FUNC TYPENAME NEWSYM 
		{
			s_Symbols.Push(rage_new scrSymTab);
			safecpy(s_CurrentTypeDef,s_LastToken);
		}
		optFormalList
		{
			scrCommandInfo *info = rage_new scrCommandInfo();
			info->ReturnType = $3;
			info->Parameters = $6;
			info->WasStubEmitted = false;
			scrTypeDef *td = rage_new scrTypeDef(info,s_CurrentTypeDef);
			*($4.sym) = rage_new scrSymTypename(td);
			scrNode::EmitChannel(SCRIPT_DECLS,"typedef %s (*%s)(",$3->GetCType(),td->GetCType());
			emit_parameter_source(SCRIPT_DECLS,$6,false);
			scrNode::EmitChannel(SCRIPT_DECLS,");\n");
			delete s_Symbols.Pop();
			s_LocalOffset = 0;
			s_ParamSize = 0;
		}

	| TYPEDEF PROC NEWSYM 
		{
			s_Symbols.Push(rage_new scrSymTab);
			safecpy(s_CurrentTypeDef,s_LastToken);
		}
		optFormalList
		{
			scrCommandInfo *info = rage_new scrCommandInfo();
			info->ReturnType = NULL;
			info->Parameters = $5;
			info->WasStubEmitted = false;
			scrTypeDef *td = rage_new scrTypeDef(info,s_CurrentTypeDef);
			*($3.sym) = rage_new scrSymTypename(td);
			scrNode::EmitChannel(SCRIPT_DECLS,"typedef void (*%s)(",td->GetCType());
			emit_parameter_source(SCRIPT_DECLS,$5,false);
			scrNode::EmitChannel(SCRIPT_DECLS,");\n");
			delete s_Symbols.Pop();
			s_LocalOffset = 0;
			s_ParamSize = 0;
		}
	| CONST_INT INTLIT
		{
			scr_error("Duplicate CONST_INT name");
		}
		error
	| CONST_FLOAT NEWSYM floatexpr { *($2.sym) = rage_new scrSymFloat($3); }
	| TWEAK_FLOAT NEWSYM floatexpr
		{
			/* if (s_InGlobalsBlock)
				scr_errorf("TWEAK_FLOAT cannot appear in a GLOBALS block");
			else */ if (PARAM_final.Get())
				*($2.sym) = rage_new scrSymFloat($3);
			else {
				scrSymVar *var = rage_new scrSymVar(&scrTypeFloat::Instance,NULL,s_InGlobalsBlock);
				*($2.sym) = var;
				if (s_InGlobalsBlock==-1 || s_InGlobalsBlock==g_SaveGlobalsBlock) {
					scrNodeStmt* stmt =	rage_new scrNodeAssign(scr_line,&scrTypeFloat::Instance,var->Address,rage_new scrNodeFloat($3),true);
					if (s_InGlobalsBlock==-1)
						s_StaticsInit = s_StaticsInit ? rage_new scrNodeStmts(s_StaticsInit,stmt) : stmt;
					else
						s_GlobalsInit = s_GlobalsInit ? rage_new scrNodeStmts(s_GlobalsInit,stmt) : stmt;
				}
			}
		}
	| CONST_FLOAT floatexpr
		{
			scr_error("Duplicate CONST_FLOAT name");
		}
		error
	| GOTO FALSE { s_AllowGotos = false; }
	| GLOBALS optBlockIndex TRUE { g_SaveGlobalsBlock = $2; }
	| NATIVE_TO_INT FALSE { s_AllowNativeToInt = false; }
	| INT_TO_NATIVE FALSE { s_AllowIntToNative = false; }
	;

optBlockIndex
	: INTLIT	{ $$ = $1; if ($1 < 0 || $1 >= scrProgram::MAX_GLOBAL_BLOCKS) scr_errorf("Invalid globals block index."); }
	|			{ $$ = 0; }
	;
	
enumOrStrictEnum
	: ENUM { $$ = 0; }
	| HASH_ENUM { $$ = 2; }
	| STRICT_ENUM { $$ = 1; }
	| STRICT_HASH_ENUM { $$ = 3; }
	;
	
stmt
	: SCOPE scopedStmtList ENDSCOPE					{ CheckSwitch(); $$ = rage_new scrNodeScope($1,$2); }
	| lvalue '=' expr								{ CheckSwitch(); 
													  // This isn't exhaustive.
													  scrNodeExpr *l = $1->Address; scrNodeExpr *r = $3;
													  l = l? l->GetAddressExpr() : l;
													  r = r? r->GetAddressExpr() : r;
													  if (l && r && l->IsSameAs(r)) Displayf("Info: %s(%d) : Assigning variable to itself, use UNUSED_PARAMETER(...) instead.",scr_stream_name,$2);
													  $$ = rage_new scrNodeAssign($2,$1->Type,$1->Address,$3); $1->Reinitialized = true; }
	| lvalue PLUS_EQ expr							{ CheckSwitch(); $$ = $1->Type->IsTextLabel()
															? (scrNodeStmt*)(rage_new scrNodeAppend($1->Address,$1->Type,$3))
															: (scrNodeStmt*)(rage_new scrNodeAssign($2,$1->Type,$1->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef($1->Type,$1->Address),OP_IADD,OP_FADD,OP_VADD,$3,"+","+","+"))); }
	| lvalue MINUS_EQ expr							{ CheckSwitch(); $$ = rage_new scrNodeAssign($2,$1->Type,$1->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef($1->Type,$1->Address),OP_ISUB,OP_FSUB,OP_VSUB,$3,"-","-","-")); }
	| lvalue TIMES_EQ expr							{ CheckSwitch(); $$ = rage_new scrNodeAssign($2,$1->Type,$1->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef($1->Type,$1->Address),OP_IMUL,OP_FMUL,OP_VMUL,$3,"*","*","*")); }
	| lvalue DIVIDE_EQ expr							{ CheckSwitch(); $$ = rage_new scrNodeAssign($2,$1->Type,$1->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef($1->Type,$1->Address),OP_IDIV,OP_FDIV,OP_VDIV,$3,"op_idiv","op_fdiv","op_vdiv")); }
	| lvalue AND_EQ expr							{ CheckSwitch(); $$ = rage_new scrNodeAssign($2,$1->Type,$1->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef($1->Type,$1->Address),OP_IAND,OP_NOP,OP_NOP,$3,"&",NULL,NULL)); }
	| lvalue OR_EQ expr								{ CheckSwitch(); $$ = rage_new scrNodeAssign($2,$1->Type,$1->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef($1->Type,$1->Address),OP_IOR,OP_NOP,OP_NOP,$3,"|",NULL,NULL)); }
	| lvalue XOR_EQ expr							{ CheckSwitch(); $$ = rage_new scrNodeAssign($2,$1->Type,$1->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef($1->Type,$1->Address),OP_IXOR,OP_NOP,OP_NOP,$3,"^",NULL,NULL)); }
	| lvalue PLUSPLUS								{ CheckSwitch(); $$ = rage_new scrNodeAssign($2,$1->Type,$1->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef($1->Type,$1->Address),OP_IADD,OP_FADD,OP_NOP,rage_new scrNodeInt(1),"+","+",NULL)); }
	| PLUSPLUS lvalue								{ CheckSwitch(); $$ = rage_new scrNodeAssign($1,$2->Type,$2->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef($2->Type,$2->Address),OP_IADD,OP_FADD,OP_NOP,rage_new scrNodeInt(1),"+","+",NULL)); }
	| lvalue MINUSMINUS								{ CheckSwitch(); $$ = rage_new scrNodeAssign($2,$1->Type,$1->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef($1->Type,$1->Address),OP_ISUB,OP_FSUB,OP_NOP,rage_new scrNodeInt(1),"-","-",NULL)); }
	| MINUSMINUS lvalue								{ CheckSwitch(); $$ = rage_new scrNodeAssign($1,$2->Type,$2->Address,rage_new scrNodeArithmetic(rage_new scrNodeVarRef($2->Type,$2->Address),OP_ISUB,OP_FSUB,OP_NOP,rage_new scrNodeInt(1),"-","-",NULL)); }
	| IF expr scopedStmtList optElses ENDIF			{ CheckSwitch(); $$ = rage_new scrNodeIf($1,$2,$3,$4); }
	| WHILE expr { ++s_InLoop; } scopedStmtList { --s_InLoop; } ENDWHILE			{ CheckSwitch(); $$ = rage_new scrNodeWhile($1,$2,$4); }
	| REPEAT expr lvalue { ++s_InLoop; $3->Reinitialized = true; s_ReadOnly.Push($3->GetReadOnly()); $3->SetReadOnly(true); } scopedStmtList { --s_InLoop; $3->SetReadOnly(s_ReadOnly.Pop()); } ENDREPEAT	
		{ 
			CheckSwitch(); 
			$3->WasReferenced();
			if (!$2->GetType()->IsInteger())
				scr_errorf("REPEAT count must be integer expression");
			if (!$3->Type->IsInteger())
				scr_errorf("REPEAT variable must be integer");
			$$ = rage_new scrNodeRepeat($1,$2,$3->Address,$5); 
		}
	| FOR lvalue '=' expr TO expr optStep { ++s_InLoop; $2->Reinitialized = true; s_ReadOnly.Push($2->GetReadOnly()); $2->SetReadOnly(true); } scopedStmtList { --s_InLoop; $2->SetReadOnly(s_ReadOnly.Pop()); }ENDFOR		
		{ 
			CheckSwitch(); 
			$2->WasReferenced();
			if (!$2->Type->IsInteger())
				scr_errorf("FOR loop must have integer as loop index");
			if (!$4->GetType()->IsInteger())
				scr_errorf("FOR loop initial value must be an integral expression");
			if (!$6->GetType()->IsInteger())
				scr_errorf("FOR loop final value must be an integral expression");
			$$ = rage_new scrNodeFor($1,$2->Address,$4,$6,$7,$9); 
		}
	| BREAKLOOP { if (!s_InLoop) scr_errorf("BREAKLOOP outside of any loop"); CheckSwitch(); $$ = rage_new scrNodeBreakLoop($1); }
	| RELOOP { if (!s_InLoop) scr_errorf("RELOOP outside of any loop"); CheckSwitch(); $$ = rage_new scrNodeReLoop($1); }
	| SWITCH expr 
		{
			CheckSwitch(); 
			s_Switches.Push(rage_new scrNodeSwitch($2));
		}
	  scopedStmtList ENDSWITCH
		{
			s_Switches.Top()->SetBody($4);
			$$ = s_Switches.Pop();
		}
	| CASE intexpr
		{ 
			if (!s_Switches.GetCount()) 
				scr_errorf("CASE outside any SWITCH");
			else {
				if (!s_Switches.Top()->GetHadBreak()) {
					if (PARAM_werror.Get())
						scr_warningf("Missing BREAK or FALLTHRU before CASE");
					else
						Displayf("Info: %s(%d) : Missing BREAK or FALLTHRU before CASE",scr_stream_name,scr_line);
				}

				s_Switches.Top()->SetHadBreak(true);
				if (s_Switches.Top()->GetExpr()->GetType() != &scrTypeInt::Instance)
					scr_warningf("Enclosing SWITCH statement is an ENUM, not an integer");
				$$ = rage_new scrNodeCase(s_Switches.Top(),$2,0);
			}
		}
	| CASE ENUMLIT
		{
			if (!s_Switches.GetCount()) 
				scr_errorf("CASE outside any SWITCH");
			else {
				if (!s_Switches.Top()->GetHadBreak()) {
					if (PARAM_werror.Get())
						scr_warningf("Missing BREAK or FALLTHRU before CASE");
					else
						Displayf("Info: %s(%d) : Missing BREAK or FALLTHRU before CASE",scr_stream_name,scr_line);
				}
				s_Switches.Top()->SetHadBreak(true);
				if (s_Switches.Top()->GetExpr()->GetType() != $2->Parent)
					scr_warningf("Enclosing SWITCH statement does not match this ENUM");
				$$ = rage_new scrNodeCase(s_Switches.Top(),$2->Value,$2->Name);
			}
		}
	| BREAK 
		{ 
			CheckSwitch();
			if (!s_Switches.GetCount()) 
				scr_errorf("BREAK outside any SWITCH");
			else
				$$ = rage_new scrNodeBreak(s_Switches.Top());
		}
	| FALLTHRU
		{ 
			CheckSwitch();
			if (!s_Switches.GetCount()) 
				scr_errorf("FALLTHRU outside any SWITCH");
			else
				s_Switches.Top()->SetHadBreak(true);
			$$ = NULL;
		}
	| DEFAULT
		{ 
			if (!s_Switches.GetCount()) 
				scr_errorf("DEFAULT outside any SWITCH");
			else {
				if (!s_Switches.Top()->GetHadBreak()) {
					if (PARAM_werror.Get())
						scr_warningf("Missing BREAK or FALLTHRU before DEFAULT");
					else
						Displayf("Info: %s(%d) : Missing BREAK or FALLTHRU before DEFAULT",scr_stream_name,scr_line);
				}
				$$ = rage_new scrNodeDefault(NODE_DEFAULT,s_Switches.Top());
			}
		}
	| PROCNAME '('
		{ if (PARAM_final.Get() && $1->DebugOnly) { s_IgnoreDebugStuff = true; s_HadDebugOnlyCalls = true; } } 
		argExprList ')'
		{ CheckSwitch(); $$ = rage_new scrNodeCommand($1,$4,NULL); s_IgnoreDebugStuff=false; }
	| CALL rvalue '(' argExprList ')'
		{ 
			CheckSwitch(); 
			const scrType *type = $2->Type;
			if (type->IsTypeDef()) 
				$$ = rage_new scrNodeCommand(((scrTypeDef*)type)->Info,$4,$2->Address); 
			else
				scr_warningf("Cannot call thru something that isn't a TYPEDEF");
		}
	| FUNCNAME '(' argExprList ')'							{ CheckSwitch(); $$ = rage_new scrNodeCommand($1,$3,NULL); }
	| TYPENAME { CheckSwitch(); s_BaseType = $1; s_NeedNewline = true; } lvarDeclList NL { $$ = $3; }
	| CONST_INT { s_NeedNewline = true; } NEWSYM expr NL
		{
			int ival;
			if ($4->IsConstant(ival)) {
				*($3.sym) = rage_new scrSymInt(ival);
			}
			else
				scr_errorf("CONST_INT expression must be constant");
			$$ = NULL;
		}
	| CONST_FLOAT NEWSYM floatexpr { *($2.sym) = rage_new scrSymFloat($3); $$ = NULL; }
	/* We now have separate statements for EXIT and RETURN to avoid a parsing ambiguity that allows
		us to eliminate the requirement for the SET command */
	| EXIT					
		{ 
			CheckSwitch();
			if (s_ReturnType)
				scr_warningf("Cannot use EXIT in a FUNC (use RETURN instead)");
			$$ = rage_new scrNodeReturn(NULL,s_ParamSize,0); 
			if (s_Switches.GetCount())
				s_Switches.Top()->SetHadBreak(true);
		}
	| RETURN expr
		{ 
			CheckSwitch(); 
			if (!s_ReturnType)
				scr_warningf("Cannot use RETURN in a PROC or SCRIPT (use EXIT instead)");
			$$ = rage_new scrNodeReturn($2,s_ParamSize,s_ReturnType); 
			if (s_Switches.GetCount())
				s_Switches.Top()->SetHadBreak(true);
		}
	| NEWSYM ':'
		{
			CheckSwitch(); 
			scrNodeLabel *label  = rage_new scrNodeLabel(true);
			PromoteToSubprogram($1.sym);
			*($1.sym) = rage_new scrSymLabel(label);
			$$ = label;
		}
	| LABEL ':'
		{
			CheckSwitch(); 
			$$ = $1;
			if ($1->IsPlaced())
				scr_warningf("Duplicate LABEL");
			else
				$1->SetPlaced();
		}
	| GOTO NEWSYM
		{
			CheckSwitch(); 
			if (!s_AllowGotos)
				scr_warningf("Sorry, GOTO has been disabled for your project");
			s_HadGoto = true;
			scrNodeLabel *label = rage_new scrNodeLabel(false);
			PromoteToSubprogram($2.sym);
			*($2.sym) = rage_new scrSymLabel(label);
			$$ = rage_new scrNodeGoto(label);
		}
	| GOTO LABEL
		{
			CheckSwitch(); 
			if (!s_AllowGotos)
				scr_warningf("Sorry, GOTO has been disabled for your project");
			s_HadGoto = true;
			$$ = rage_new scrNodeGoto($2);
		}
	| THROW
		{
			$$ = rage_new scrNodeThrow(rage_new scrNodeInt(-1));
		}
	| THROW '(' expr ')'
		{
			$$ = rage_new scrNodeThrow($3);
		}
	| UNUSED_PARAMETER '(' VAR_REF ')' { $3->WasReferenced(); $$ = NULL; }
	;
	
intexpr
	: INTLIT		{ $$ = $1; }
	| '-' INTLIT	{ $$ = -$2; }
	| HASH '(' STRLIT ')' { $$ = scrComputeHash($3); }
	;
	
floatexpr
	: FLOATLIT		{ $$ = $1; }
	| '-' FLOATLIT	{ $$ = -$2; }
	;

/* boolexpr
	: TRUE			{ $$ = 1; }
	| FALSE			{ $$ = 0; }
	; */
	
optFormalList
	: '(' formalList ')'	{ 
									$$ = $2; 
									const scrTypeList *l = $2;
									scrDefaultValue dummy;
									while (l) {
										if (l->Left->HasDefaultValue(dummy)) {
											// Make sure everything after this has one as well
											while (l) {
												if (!l->Left->HasDefaultValue(dummy)) {
													scr_warningf("If parameter list has default value, all parameters after it must have defaults too.");
													break;
												}
												l = l->Right;
											}
										}
										else
											l = l->Right;
									}
							}
	| '(' ')'				{ $$ = NULL; }
	|						{ $$ = NULL; }
	;

optFormalOrVarargsList
	: optFormalList			{ $$ = $1; }
	| '(' VARARGS ')'		{ $$ = &g_VARARGS; }
	| '(' VARARGS1 ')'		{ $$ = &g_VARARGS1; }
	| '(' VARARGS2 ')'		{ $$ = &g_VARARGS2; }
	| '(' VARARGS3 ')'		{ $$ = &g_VARARGS3; }
	;
	
optGuid
	:				{ 
							$$ = 0; 
							scr_warningf("NATIVE declartions must now include = \"0x0123456789abcdef\" which is a 64-bit random number (use uuid64)"); 
					}
	| '=' STRLIT	{ 
							char *endptr; 
							$$ = _strtoui64($2,&endptr,16); 
							if (!endptr || *endptr) 
								scr_warningf("Error in GUID string, should be 64-bit random hex number",*endptr);
							if (!($$ >> 32))
								scr_warningf("GUID random number must be a full 64-bit random hex number, not only 32 bits");
					}
	;
	
formalList
	: formal ',' formalList	{ $$ = rage_new scrTypeList($1,$3); }
	| formal				{ $$ = rage_new scrTypeList($1,NULL); }
	;
	
formal
	: TYPENAME { s_BaseType = $1; } '&' NEWSYM foptArrays
		{ 
			if ($5->IsString())
				scr_warningf("STRING variables are implicitly passed by reference, '&' is not allowed.");
			$$ = rage_new scrTypeRef($5); 
			*($4.sym) = rage_new scrSymVar($$);
			s_ParamSize += $$->GetSize();
		}
	| TYPENAME { s_BaseType = $1; } NEWSYM optArraysOrDefaultValue
		{ 
			$$ = $4; 
			*($3.sym) = rage_new scrSymVar($$);
			s_ParamSize += $$->GetSize();
		}
	| ENUM_TO_INT { s_BaseType = &scrTypeInt::EnumToIntInstance; } NEWSYM optArraysOrDefaultValue
		{
			$$ = $4; 
			*($3.sym) = rage_new scrSymVar($$);
			s_ParamSize += $$->GetSize();
		}
	| ENUM_TO_INT { s_BaseType = &scrTypeInt::EnumToIntInstance; } '&' NEWSYM foptArrays
		{
			$$ = rage_new scrTypeRef($5); 
			*($4.sym) = rage_new scrSymVar($$);
			s_ParamSize += $$->GetSize();
		}
	| STRUCT '&' NEWSYM
		{
			$$ = rage_new scrTypeRef(&s_StructRef);
		}
	;
	
optStep
	: STEP intexpr		{ $$ = $2; if (!$2) scr_warningf("STEP of zero is not allowed"); }
	|					{ $$ = 1; }
	;
	
enumerantList
	: enumerant
	| enumerantList ',' enumerant
	;
	
enumerant
	: NEWSYM 
		{ 
			int value = s_Enum->IsHashEnum()? atStringHash(s_LastToken) : s_Enum->GetLast()? s_Enum->GetLast()->Value + 1 : 0;
			if (s_Enum->IsHashEnum() && s_Enum->HasThisValue(value))
				scr_warningf("Value %d already used in this hash enumerant",value);
			*($1.sym) = rage_new scrSymEnum(s_Enum->AddEnumerant(s_LastToken,value));
		}
	| NEWSYM '=' enumExpr
		{ 
			if (s_Enum->IsHashEnum() && s_Enum->HasThisValue($3))
				scr_warningf("Value %d already used in this hash enumerant",$3);
			*($1.sym) = rage_new scrSymEnum(s_Enum->AddEnumerant(s_LastToken,$3));
		}
	;
	
structMemberList
	: structMember
	| structMemberList structMember
	;
	
structMember
	: TYPENAME 
		{
			if ($1 == s_Struct)
				scr_warningf("Structure cannot contain itself!  The universe would implode.");
			s_BaseType = $1;
		}
		svarDeclList
	;
	
svarDeclList
	: svarDeclList ',' svarDecl
	| svarDecl
	;
	
svarDecl
	: NEWSYM optArraysOrDefaultValue
		{
			scrNode::EmitChannel(SCRIPT_DECLS,"\t%s %s;\n",$2->GetCType(),s_LastToken);
			if (!$2->GetSize())
				scr_warningf("Structure cannot contain a zero-sized (FORWARD'ed?) structure?");
			scrSym *sym = rage_new scrSymField(s_Struct->AddField($2,s_LastToken));
			*($1.sym) = sym;
			if (scr_structreffile) {
				char sname[128];
				s_Struct->GetName(sname);
				fprintf(scr_structreffile,"DECL,%s,%s\r\n",sname,s_LastToken);
			}
		}
	;
	
enumExpr
	: ENUMLIT					{ $$ = $1->Value; }
	| INTLIT					{ $$ = $1; }
	| '-' INTLIT				{ $$ = -$2; }
	| enumExpr '+' INTLIT		{ $$ = $1 + $3; }
	| enumExpr '+' ENUMLIT		{ $$ = $1 + $3->Value; }
	| HASH '(' STRLIT ')'		{ $$ = scrComputeHash($3); }
	;
	
lvarDeclList
	: lvarDeclList ',' lvarDecl		{ $$ = rage_new scrNodeStmts($1,$3); }
	| lvarDecl						{ $$ = $1; }
	;

gvarDeclList
	: gvarDeclList ',' gvarDecl
	| gvarDecl
	;

lvarDecl
	: NEWSYM optArrays optAssignment			
		{ 
			bool constructed = $2->GetArrayCount() || $3;
			if (s_HadGoto && constructed)
				scr_errorf("Cannot declare array or initialized variable after seeing a GOTO in this block");
			else if (s_Switches.GetCount() && constructed)
				scr_errorf("Cannot declare array or initialized variable within a SWITCH statement");
			scrSymVar *var = rage_new scrSymVar($2); 
			*($1.sym) = var;
			$$ = rage_new scrNodeStmts(rage_new scrNodeDecl($2,s_LastToken,$3 != 0),$3? (scrNodeStmt*)(rage_new scrNodeAssign(scr_line,$2,var->Address,$3)) : (scrNodeStmt*)(rage_new scrNodeConstruct($2,var->Address)));
			if ($3)
				var->Reinitialized = true;
		}
	| VAR_REF optArrays optAssignment
		{
			const char *name = $1->Filename;
			int line = $1->LineNumber;
			scr_duplicate("variable",line,name);
		}
	;
	
optAssignment
	: '=' expr		{ $$ = $2; }
	|				{ $$ = NULL; }
	;
	
gvarDecl
	: NEWSYM optArrays optGlobalOrStaticAssignment
		{ 
			scrNode::EmitChannel(s_InGlobalsBlock != -1? SCRIPT_GLOBALS : SCRIPT_STATICS,"\t%s ",$2->GetCType());
			scrNode::EmitChannel(s_InGlobalsBlock != -1? SCRIPT_GLOBALS : SCRIPT_STATICS,s_LastToken); // HACK
			scrNode::EmitChannel(s_InGlobalsBlock != -1? SCRIPT_GLOBALS : SCRIPT_STATICS," /* +%d */;\n",s_InGlobalsBlock != -1?s_GlobalOffsets[s_InGlobalsBlock]:s_StaticOffset);

			if ($2->GetArrayCount() && $3)
				scr_warningf("Cannot initialize global arrays");
			scrSymVar *var = rage_new scrSymVar($2,NULL,s_InGlobalsBlock); 
			*($1.sym) = var;

			if($3)
			{
				scrNodeStmt* stmt =
					rage_new scrNodeAssign(scr_line,
									$2,
									var->Address,
									$3,true);
									
				if (s_InGlobalsBlock != -1)
				{
					// Only initialize the globals that are actually resident
					if (s_InGlobalsBlock == g_SaveGlobalsBlock)
						s_GlobalsInit = s_GlobalsInit ? rage_new scrNodeStmts(s_GlobalsInit,stmt) : stmt;
				}
				else
				{
					s_StaticsInit = s_StaticsInit ? rage_new scrNodeStmts(s_StaticsInit,stmt) : stmt;
				}
			}
		}
	;

foptArrays
	: '[' expr ']' optArrays
		{
			int size;
			if ($2->IsConstant(size)) {
				if (!size)
					scr_errorf("Zero-sized arrays are not allowed.");
					// Displayf("size = %d",size);
				$$ = rage_new scrTypeArray($4,size);
			}
			else
				scr_errorf("Array dimension is not integer constant");
		}
	| '[' ']'
		{
			$$ = rage_new scrTypeArray(s_BaseType,VariableArraySentinel);
		}
	|	{ $$ = s_BaseType; }
	;
	
optArraysOrDefaultValue
	: optArrays		{ $$ = $1; }
	| '=' enumExpr	{ if (!s_BaseType->IsInteger()) scr_warningf("Parameter cannot have default of type ENUM or INT"); $$ = rage_new scrTypeDefaultValue(s_BaseType, $2); }
	| '=' floatexpr	{ if (s_BaseType != &scrTypeFloat::Instance) scr_warningf("Parameter cannot have default of type FLOAT"); $$ = rage_new scrTypeDefaultValue(s_BaseType, $2); }
	| '=' TRUE		{ if (s_BaseType != &scrTypeBool::Instance) scr_warningf("Parameter cannot have default of type BOOL"); $$ = rage_new scrTypeDefaultValue(s_BaseType, 1); }
	| '=' FALSE		{ if (s_BaseType != &scrTypeBool::Instance) scr_warningf("Parameter cannot have default of type BOOL"); $$ = rage_new scrTypeDefaultValue(s_BaseType, 0); }
	| '=' VAR_REF	
		{ 
			if ($2->Type != &scrTypeNull::Instance)
				scr_warningf("Only NULL is allowed for default parameter, not an arbitrary variable");
			if (s_BaseType != &scrTypeString::Instance && !s_BaseType->IsObject()) 
				scr_warningf("Parameter cannot have default of type NULL"); 
				$$ = rage_new scrTypeDefaultValue(s_BaseType, 0); 
		}
	;
	
optArrays
	: '[' expr ']' optArrays
		{
			int size;
			if ($2->IsConstant(size)) {
				if (!size)
					scr_errorf("Zero-sized arrays are not allowed.");
					// Displayf("size = %d",size);
				$$ = rage_new scrTypeArray($4,size);
			}
			else
				scr_errorf("Array dimension is not integer constant");
		}
	|	'[' ']'
		{
			scr_errorf("Cannot use variable-length arrays here (must be passed by reference)");
			$$ = NULL;
		}
	|	{ $$ = s_BaseType; }
	;
	
optGlobalOrStaticAssignment
	: '=' expr	{ $$ = $2; }
	|			{ $$ = NULL; }
	;
	
optElses
	: ELSE scopedStmtList							{ $$ = $2; }
	| ELIF expr scopedStmtList optElses				{ $$ = rage_new scrNodeIf($1,$2,$3,$4); }
	|												{ $$ = 0; }
	;

scopedStmtList
	: { ++s_ScopeLevel; s_Symbols.Push(rage_new scrSymTab) } 
	  optStmtList 
	  {
	    // This should eventually become default behavior!
	    if (PARAM_warnnestedunused.Get())
	      delete CheckForUnreferenced(s_Symbols.Pop());
	    else
	      delete s_Symbols.Pop();
		--s_ScopeLevel; 
		$$ = $2; 
	  }
	;
		  
optStmtList
	:					{ $$ = NULL; }
	| stmtList			{ $$ = $1; }
	;
	
stmtList
	: stmtList stmt		{ $$ = rage_new scrNodeStmts($1,$2); }
	| stmt				{ $$ = $1; }
	;

primaryExpr
	: '(' expr ')'						{ $$ = $2; }
	| LSH expr ',' expr ',' expr RSH	{ $$ = rage_new scrNodeTuple($2,$4,$6); }
	| rvalue							
			{ 
				$$ = rage_new scrNodeVarRef($1->Type,$1->Address);
				if (!$1->Reinitialized)
					Displayf("Info: %s(%d) : Variable '%s' declared in loop with no initializer is referenced before being set, value will persist across iterations",scr_stream_name,scr_line,$1->Name);
			}
	| INTLIT							{ $$ = rage_new scrNodeInt($1); }
	| FLOATLIT							{ $$ = rage_new scrNodeFloat($1); }
	| STRLIT							{ $$ = rage_new scrNodeString($1); }
	| ENUMLIT							{ $$ = rage_new scrNodeEnum($1); }
	| TRUE								{ $$ = rage_new scrNodeBool(true); }
	| FALSE								{ $$ = rage_new scrNodeBool(false); }
	| FUNCNAME '(' argExprList ')'		{ $$ = rage_new scrNodeFunctionCall($1,$3,NULL); }
	| CALL rvalue '(' argExprList ')'
		{ 
			const scrType *type = $2->Type;
			if (type->IsTypeDef()) 
				$$ = rage_new scrNodeFunctionCall(((scrTypeDef*)type)->Info,$4,$2->Address); 
			else
				scr_warningf("Cannot call thru something that isn't a TYPEDEF");
		}
	| PROCNAME '(' argExprList ')'		{ scr_errorf("PROC has no return value, cannot use in expression"); $$ = NULL; }
	| '&' FUNCNAME						{ $$ = rage_new scrNodeFunctionAddr($2); }
	| '&' PROCNAME						{ $$ = rage_new scrNodeFunctionAddr($2); }
	| CATCH								{ $$ = rage_new scrNodeUnary(OP_CATCH,NULL,&scrTypeInt::Instance,"op_catch"); }
	| ENUM_TO_INT '(' expr ')'
		{
			if (!$3->GetType()->IsInteger())
				scr_warningf("Can only apply ENUM_TO_INT to an integer");
			$$ = rage_new scrNodeUnary(OP_NOP,$3,&scrTypeInt::Instance,"enum_to_int");
		}
	| NATIVE_TO_INT '(' expr ')'
		{
			if (!s_AllowNativeToInt)
				scr_warningf("NATIVE_TO_INT has been disallowed");
			else if (!$3->GetType()->IsObject())
				scr_warningf("Can only apply NATIVE_TO_INT to a native type object");
			$$ = rage_new scrNodeUnary(OP_NOP,$3,&scrTypeInt::Instance,"native_to_int");
		}
	| COUNT_OF '(' arrayReference ')'	
		{
			if (!$3->Type->GetArrayCount())
				scr_warningf("Cannot apply COUNT_OF to something that isn't an array or ENUM.");
			$$ = rage_new scrNodeUnary(OP_LOAD,$3->Address,&scrTypeInt::Instance,"count_of"); 
		}
	| COUNT_OF '(' TYPENAME ')'	
		{
			if (!$3->IsEnum())
				scr_warningf("Cannot apply COUNT_OF to something that isn't an array or ENUM.");
			$$ = rage_new scrNodeInt($3->GetEnumCount());
		}
	| SIZE_OF '(' TYPENAME ')'	
		{
			if ($3->IsVariableSizedArray())
				scr_warningf("Cannot apply SIZE_OF operator to a variable-sized array.");
			$$ = rage_new scrNodeInt($3->GetSize());
		}
	| SIZE_OF '(' rvalue ')'	
		{
			if ($3->Type->IsVariableSizedArray())
				scr_warningf("Cannot apply SIZE_OF operator to a variable-sized array.");
			$$ = rage_new scrNodeInt($3->Type->GetSize());
		}
	| INT_TO_ENUM '(' TYPENAME ',' expr ')'
		{
			if (!$3->IsEnum())
				scr_warningf("Can only cast from something to an ENUM at present.");
			if (!$5->GetType()->IsInteger())
				scr_warningf("Can only cast from an integer expression at present.");
			$$ = rage_new scrNodeUnary(OP_NOP,$5,$3,"int_to_enum");
		}
	| INT_TO_NATIVE '(' TYPENAME ',' expr ')'
		{
			if (!s_AllowIntToNative)
				scr_warningf("INT_TO_NATIVE has been disallowed");
			if (!$3->IsObject())
				scr_warningf("Can only cast from something to NATIVE at present.");
			if (!$5->GetType()->IsInteger())
				scr_warningf("Can only cast from an integer expression at present.");
			$$ = rage_new scrNodeUnary(OP_NOP,$5,$3,"int_to_native");
		}
	| HASH '(' STRLIT ')'
		{
			$$ = rage_new scrNodeInt( scrComputeHash($3));
		}
	;
	
argExprList
	: exprOrDefaultList	{ $$ = $1; }
	|					{ $$ = NULL; }
	;
	
exprOrDefaultList
	: exprOrDefault ',' exprOrDefaultList		{ $$ = rage_new scrExprList($1,$3); }
	| exprOrDefault								{ $$ = rage_new scrExprList($1,NULL); }
	;
	
exprOrDefault
	: expr				{ $$ = $1; }
	| DEFAULT			{ $$ = rage_new scrNodeDefaultParam(); }
	;

lvalue
	: VAR_REF
		{ 
			if ($1->Type->GetReference()) {
				$1->WasReferenced();
				$$ = rage_new scrSymVar($1->Type->GetReference(),rage_new scrNodeDereference($1->Type->GetReference(),$1->Address));
			}
			else
				$$ = $1; 
		}
	| NEWSYM				{ scr_errorf("Unrecognized variable or command name '%s'",s_LastToken); $$ = NULL; }
	| lvalue '.'
		{
			s_Structs.Push($1->Type);
		}
	  FIELD 
		{ 
			// Intentionally don't mark an lvalue as referenced
			$$ = rage_new scrSymVar($4->Type,rage_new scrNodeField($1->Address,$4,NULL /*(scrTypeStruct*)s_Structs.Top()*/));
			s_Structs.Pop(); 
		}
	| lvalue '[' expr ']'
		{
			int maxCount = $1->Type->GetArrayCount();
			int idx;
			if (!maxCount)
				scr_errorf("Subscript applied to something that isn't an array");
			else if ($3->IsConstant(idx) && (idx < 0 || idx >= maxCount))
				scr_errorf("Array element out of range");
			else if (!$3->GetType()->IsInteger())
				scr_errorf("Array index must be an integer or enumerant");
			$$ = rage_new scrSymVar($1->Type->GetArrayType(),rage_new scrNodeArrayLookup($1->Address,$1->Type->GetArrayType()->GetSize(),$3));
		}
	;

rvalue
	: VAR_REF
		{ 
			$1->WasReferenced();
			if ($1->Type->GetReference()) {
				$$ = rage_new scrSymVar($1->Type->GetReference(),rage_new scrNodeDereference($1->Type->GetReference(),$1->Address));
			}
			else
				$$ = $1;
		}
	| rvalue '.'
		{
			$1->WasReferenced();
			s_Structs.Push($1->Type);
		}
	  FIELD 
		{ 
			$$ = rage_new scrSymVar($4->Type,rage_new scrNodeField($1->Address,$4,(scrTypeStruct*)s_Structs.Top()));
			s_Structs.Pop(); 
			$$->Reinitialized = $1->Reinitialized;
		}
	| rvalue '[' expr ']'
		{
			$1->WasReferenced();
			int maxCount = $1->Type->GetArrayCount();
			int idx;
			if (!maxCount)
				scr_errorf("Subscript applied to something that isn't an array");
			else if ($3->IsConstant(idx) && (idx < 0 || idx >= maxCount))
				scr_errorf("Array element out of range");
			else if (!$3->GetType()->IsInteger())
				scr_errorf("Array index must be an integer or enumerant");
			$$ = rage_new scrSymVar($1->Type->GetArrayType(),rage_new scrNodeArrayLookup($1->Address,$1->Type->GetArrayType()->GetSize(),$3));
			$$->Reinitialized = $1->Reinitialized;
		}
	| NEWSYM				{ scr_errorf("Unrecognized variable or command name '%s'",s_LastToken); $$ = NULL; }
	;

arrayReference
	: VAR_REF							
		{ 
			if ($1->Type->GetReference())
				$$ = rage_new scrSymVar($1->Type->GetReference(),rage_new scrNodeDereference($1->Type->GetReference(),$1->Address));
			else
				$$ = $1; 
		}
	| NEWSYM				{ scr_errorf("Unrecognized variable or command name '%s'",s_LastToken); $$ = NULL; }
	| arrayReference '.'
		{
			s_Structs.Push($1->Type);
		}
	  FIELD 
		{ 
			s_Structs.Pop(); 
			$$ = rage_new scrSymVar($4->Type,rage_new scrNodeField($1->Address,$4,NULL));
		}
	| arrayReference '[' ']'
		{
			if (!$1->Type->GetArrayType())
				scr_errorf("Cannot apply COUNT_OF[] to something that isn't an array.");
			$$ = rage_new scrSymVar($1->Type->GetArrayType(),rage_new scrNodeCountOf($1->Address));
		}
	;



unaryExpr
	: primaryExpr						{ $$ = $1; }
	| NOT unaryExpr						{ $$ = rage_new scrNodeLogicalNot($2); }
	| '-' unaryExpr						{ $$ = rage_new scrNodeNegate($2); }
	;

multExpr
	: unaryExpr							{ $$ = $1; }
	| multExpr '*' unaryExpr			{ $$ = rage_new scrNodeArithmetic($1,OP_IMUL,OP_FMUL,OP_VMUL,$3,"*","*","*"); }
	| multExpr '/' unaryExpr			{ $$ = rage_new scrNodeArithmetic($1,OP_IDIV,OP_FDIV,OP_VDIV,$3,"op_idiv","op_fdiv","op_vdiv"); }
	| multExpr '%' unaryExpr			{ $$ = rage_new scrNodeArithmetic($1,OP_IMOD,OP_FMOD,OP_NOP,$3,"op_imod","op_fmod",NULL); }
	;

addExpr
	: multExpr							{ $$ = $1; }
	| addExpr '+' multExpr				{ $$ = rage_new scrNodeArithmetic($1,OP_IADD,OP_FADD,OP_VADD,$3,"+","+","+"); }
	| addExpr '-' multExpr				{ $$ = rage_new scrNodeArithmetic($1,OP_ISUB,OP_FSUB,OP_VSUB,$3,"-","-","-"); }
	| addExpr PLUS_TIME multExpr		{ $$ = rage_new scrNodeArithmetic($1,OP_IADD,OP_FADD,OP_VADD,TimeStep($3),"+","+","+"); }
	| addExpr MINUS_TIME multExpr		{ $$ = rage_new scrNodeArithmetic($1,OP_ISUB,OP_FSUB,OP_VSUB,TimeStep($3),"-","-","-"); }
	;

relExpr
	: addExpr							{ $$ = $1; }
	| relExpr '<' addExpr				{ $$ = rage_new scrNodeRelational($1,OP_ILT,OP_FLT,$3,"<","<"); }
	| relExpr LE addExpr				{ $$ = rage_new scrNodeRelational($1,OP_ILE,OP_FLE,$3,"<=","<="); }
	| relExpr '>' addExpr				{ $$ = rage_new scrNodeRelational($1,OP_IGT,OP_FGT,$3,">",">"); }
	| relExpr GE addExpr				{ $$ = rage_new scrNodeRelational($1,OP_IGE,OP_FGE,$3,">=",">="); }
	;

equalityExpr
	: relExpr							{ $$ = $1; }
	| equalityExpr '=' relExpr			{ $$ = rage_new scrNodeEquality($1,OP_IEQ,OP_FEQ,$3,"==","=="); }
	| equalityExpr NE relExpr			{ $$ = rage_new scrNodeEquality($1,OP_INE,OP_FNE,$3,"!=","!="); }
	;

bitAndExpr
	: equalityExpr						{ $$ = $1; }
	| bitAndExpr '&' equalityExpr		{ $$ = rage_new scrNodeArithmetic($1,OP_IAND,OP_NOP,OP_NOP,$3,"&",NULL,NULL); }
	;
	
bitXorExpr
	: bitAndExpr						{ $$ = $1; }
	| bitXorExpr '^' bitAndExpr			{ $$ = rage_new scrNodeArithmetic($1,OP_IXOR,OP_NOP,OP_NOP,$3,"^",NULL,NULL); }
	;

bitOrExpr
	: bitXorExpr						{ $$ = $1; }
	| bitOrExpr '|' bitXorExpr			{ $$ = rage_new scrNodeArithmetic($1,OP_IOR,OP_NOP,OP_NOP,$3,"|",NULL,NULL); }
	;
	
logAndExpr
	: bitOrExpr							{ $$ = $1; }
	| logAndExpr AND bitOrExpr			{ $$ = PARAM_shortcircuit.Get()? static_cast<scrNodeExpr*>(rage_new scrNodeShortCircuit($1,OP_IAND,$3)) : static_cast<scrNodeExpr*>(rage_new scrNodeLogical($1,OP_IAND,$3,"&")); }
	| logAndExpr ANDALSO bitOrExpr		{ $$ = rage_new scrNodeShortCircuit($1,OP_IAND,$3); }
	;

logOrExpr
	: logAndExpr						{ $$ = $1; }
	| logOrExpr OR logAndExpr			{ $$ = PARAM_shortcircuit.Get()? static_cast<scrNodeExpr*>(rage_new scrNodeShortCircuit($1,OP_IOR,$3)) : static_cast<scrNodeExpr*>(rage_new scrNodeLogical($1,OP_IOR,$3,"|")); }
	| logOrExpr ORELSE logAndExpr		{ $$ = rage_new scrNodeShortCircuit($1,OP_IOR,$3); }
	;

conditionalExpr
	: logOrExpr									{ $$ = $1; }
	// | logOrExpr '?' expr ':' conditionalExpr	{ $$ = rage_new scrNodeConditional($1,$3,$5); }
	;
	
expr
	: conditionalExpr						{ $$ = $1; }
	;

%%

static int nextch = 32;

const int MAX_INCLUDE_PATHS = 256;
static int s_NumberOfStaticsWhenIncludeWasLastAppendedOrPopped = 0;
struct include { int scr_line; fiStream *scr_stream; char scr_stream_name[256]; int numberOfStaticsWhenAppended; int numberOfExclusiveStatics; };
atFixedArray<include,MAX_NESTING> s_Includes;
atFixedArray<u32,MAX_USING_FILES> s_UsedUsings;
typedef char include_entry[256];
atFixedArray<include_entry,MAX_INCLUDE_PATHS> s_Paths;

void scr_add_path(const char *path) {
	if (s_Paths.IsFull())
		scr_errorf("Too many include paths specified, max is %d",s_Paths.GetMaxCount());
	strcpy(s_Paths.Append(),path);
}

atString extra_dep;

void scr_using(const char *filename) {
	// look for the file through the entire path first to catch
	// cases where the same file is included through different paths.
	char fullpath[256];
	fiStream *new_scr_stream = NULL;
	for (int p=0; p<s_Paths.GetCount(); p++) {
		formatf(fullpath,sizeof(fullpath),"%s/%s",s_Paths[p],filename);
		new_scr_stream = fiStream::Open(fullpath);
		if (new_scr_stream)
			break;
	}
	
	// never found?
	if (!new_scr_stream) {
		scr_stream = NULL;
		scr_errorf("Unable to find '%s' anywhere in -ipath",filename);
		return;
	}

	// 
	u32 hash = scrComputeHash(fullpath);
	if (s_UsedUsings.Find(hash) != -1) {
		// Warningf("Ignoring duplicate USING file '%s'",fullpath);
		for (int i=0; i<s_Includes.GetCount(); i++)
			if (hash == scrComputeHash(s_Includes[i].scr_stream_name)) {
				Displayf("Info: %s(%d) : Cycle in header include for '%s'.",scr_stream_name,scr_line,fullpath);
				break;
			}
		new_scr_stream->Close();
		return;
	}
	
	if (PARAM_dumpincludes.Get())
		Displayf("Info: %s(%d) : Including '%s'...",scr_stream_name,scr_line,fullpath);

	if (s_UsedUsings.IsFull())
		scr_errorf("Too many include files, max is %d",s_UsedUsings.GetMaxCount());
	
	s_UsedUsings.Append() = hash;
	if (s_Includes.IsFull())
		scr_errorf("Too many nested include files, max limit is %d",s_Includes.GetMaxCount());
		
	if (s_Includes.GetCount())
	{
		s_Includes.Top().numberOfExclusiveStatics += s_Statics.GetCount() - s_NumberOfStaticsWhenIncludeWasLastAppendedOrPopped;
	}

	s_NumberOfStaticsWhenIncludeWasLastAppendedOrPopped = s_Statics.GetCount();
		
	include &i = s_Includes.Append();
	i.scr_line = scr_line;
	i.scr_stream = scr_stream;
	i.numberOfStaticsWhenAppended = s_Statics.GetCount();
	i.numberOfExclusiveStatics = 0;
	safecpy(i.scr_stream_name, scr_stream_name, sizeof(i.scr_stream_name));
	
	if (scr_dep_file) {
		fprintf(scr_dep_file,"%s \\\r\n",fullpath);
		extra_dep += fullpath;
		extra_dep += ":\r\n";
	}
	
	scr_line = 1;

	scr_stream = new_scr_stream;
	safecpy(scr_stream_name, fullpath, sizeof(scr_stream_name));
	scr_stream_name_hash = hash;
}


void scr_using_close_all() {
	while (s_Includes.GetCount())
		s_Includes.Pop().scr_stream->Close();
}


static int ch() {
RESTART:
	bool wasnewline = (nextch == '\n' || nextch == '\r');
	if (nextch == '\n') 
		++scr_line;
	nextch = scr_stream->FastGetCh();
	if (nextch == -1 && s_Includes.GetCount()) {
		if (!wasnewline)
			scr_errorf("Missing newline at end of this file!");
		scr_stream->Close();
		
		if (scr_debuginfo)
		{
			if (PARAM_printstaticsinheadersexclusive.Get())
			{
				s32 numberOfStaticsExclusive = s_Includes.Top().numberOfExclusiveStatics + s_Statics.GetCount() - s_NumberOfStaticsWhenIncludeWasLastAppendedOrPopped;
				s32 exclusiveThreshold = 0;
				PARAM_printstaticsinheadersexclusive.Get(exclusiveThreshold);
				if (numberOfStaticsExclusive > exclusiveThreshold)
				{
					fprintf(scr_debuginfo,"%05d exclusive statics in %s. Number of statics is currently %d\r\n", numberOfStaticsExclusive, scr_stream_name, s_Statics.GetCount());
				}
			}
			
			if (PARAM_printstaticsinheadersinclusive.Get())
			{
				s32 numberOfStaticsInclusive = s_Statics.GetCount() - s_Includes.Top().numberOfStaticsWhenAppended;
				s32 inclusiveThreshold = 0;
				PARAM_printstaticsinheadersinclusive.Get(inclusiveThreshold);
				if (numberOfStaticsInclusive > inclusiveThreshold)
				{
					fprintf(scr_debuginfo,"%05d inclusive statics in %s. Number of statics is currently %d\r\n", numberOfStaticsInclusive, scr_stream_name, s_Statics.GetCount());
				}
			}
			
			s_NumberOfStaticsWhenIncludeWasLastAppendedOrPopped = s_Statics.GetCount();
		}
		
		scr_line = s_Includes.Top().scr_line;
		scr_stream = s_Includes.Top().scr_stream;
		safecpy(scr_stream_name, s_Includes.Top().scr_stream_name,sizeof(scr_stream_name));
		scr_stream_name_hash = scrComputeHash(scr_stream_name);
		s_Includes.Pop();
		goto RESTART;
	}
	return nextch; 
}

extern bool fatal;

void dump_using_stack() {
	int top = s_Includes.GetCount();
	while (top) {
		--top;
		Displayf("   %s(%d) : included from here",s_Includes[top].scr_stream_name,s_Includes[top].scr_line);
	}
}

void scr_errorf(const char *fmt,...) {
	va_list(args);
	va_start(args,fmt);
	char buf[256];
	vformatf(buf,sizeof(buf),fmt,args);
	errorf("%s(%d) : %s",scr_stream_name,scr_line,buf);
	dump_using_stack();
	if (fatal) throw "fatal error during compilation";
}

void scr_warningf(const char *fmt,...) {
	va_list(args);
	va_start(args,fmt);
	char buf[256];
	vformatf(buf,sizeof(buf),fmt,args);
	dump_using_stack();
	errorf("%s(%d) : %s",scr_stream_name,scr_line,buf);
	
	scr_had_warnings = true;
}



#define W(x)		{ #x, x },

// Keep this is sorted order, we use a binary search. 
struct { char *name; int value; } rw[] = {
	W(AND)
	W(ANDALSO)
	W(BREAK)
	W(BREAKLOOP)
	W(CALL)
	W(CASE)
	W(CATCH)
	W(CONST_FLOAT)
	W(CONST_INT)
	W(COUNT_OF)
	W(DEBUGONLY)
	W(DEFAULT)
	W(ELIF)
	W(ELSE)
	W(ENDENUM)
	W(ENDFOR)
	W(ENDFUNC)
	W(ENDGLOBALS)
	W(ENDIF)
	W(ENDPROC)
	W(ENDREPEAT)
	W(ENDSCOPE)
	W(ENDSCRIPT)
	W(ENDSTRUCT)
	W(ENDSWITCH)
	W(ENDWHILE)
	W(ENUM)
	W(ENUM_TO_INT)
	W(EXIT)
	W(FALLTHRU)
	W(FALSE)
	W(FOR)
	W(FORWARD)
	W(FUNC)
	W(GLOBALS)
	W(GOTO)
	W(HASH)
	W(HASH_ENUM)
	W(IF)
	W(INT_TO_ENUM)
	W(INT_TO_NATIVE)
	W(NATIVE)
	W(NATIVE_TO_INT)
	W(NOT) 
	W(OR)
	W(ORELSE)
	W(PROC)
	W(RELOOP)
	W(REPEAT)
	W(RETURN)
	W(SCOPE)
	W(SCRIPT)
	W(SIZE_OF)
	W(STEP)
	W(STRICT_ENUM)
	W(STRICT_HASH_ENUM)
	W(STRUCT)
	W(SWITCH)
	W(THROW)
	W(TO)
	W(TRUE) 
	W(TWEAK_FLOAT)
	W(TWEAK_INT)
	W(TYPEDEF)
	W(UNUSED_PARAMETER)
	W(USING)
	W(VARARGS)
	W(VARARGS1)
	W(VARARGS2)
	W(VARARGS3)
	W(WHILE) 
};

static int s_InDisabledBlock;

int scr_lex_inner() {
	int temp = 1;
RESTART:
	while (nextch != EOF && nextch <= 32) {
		if ((nextch == '\r' || nextch == '\n') && s_NeedNewline) {
			s_NeedNewline = false;
			return NL;
		}
		ch();
	}
	yylval.lineno = scr_line;
	switch (nextch) {
		case EOF: return EOF;

		case '(': case ')': case '%': case '.': case ',': case '#':
		case '{': case '}': case '[': case ']': case ':': case '=': case '?':
			temp = nextch; ch(); return temp;
		
		case '&': case '|': case '^':
			temp = nextch; ch();
			if (nextch == '=') {
				ch();
				return temp=='&'?AND_EQ:temp=='|'?OR_EQ:XOR_EQ;
			}
			else
				return temp;
				
		case '*':
			if (ch() == '=') {
				ch();
				return TIMES_EQ;
			}
			else
				return '*';

		case '/':
			if (ch() == '/') {
				while (ch() != '\n' && nextch != '\r' && nextch != EOF)
					;
				if (s_NeedNewline) {
					s_NeedNewline = false;
					return NL;
				}
				goto RESTART;
			}
			else if (nextch == '*') {
				ch();
				int state = 0;
				while (nextch != EOF) {
					if (nextch == '*')
						state = 1;
					else if (state == 1 && nextch == '/') {
						ch();	// // consume closing slash
						goto RESTART;
					}
					else
						state = 0;
					ch();
				}
				goto RESTART;
			}
			else if (nextch == '=') {
				ch();
				return DIVIDE_EQ;
			}
			else
				return '/';

		case '+':
			if (ch() == '@') { 
				ch(); 
				return PLUS_TIME; 
			}
			else if (nextch == '+') {
				ch();
				return PLUSPLUS;
			}
			else if (nextch == '=') {
				ch();
				return PLUS_EQ;
			}
			else
				return '+';
				
		case '-':
			if (ch() == '@') {
				ch();
				return MINUS_TIME;
			}
			else if (nextch == '-') {
				ch();
				return MINUSMINUS;
			}
			else if (nextch == '=') {
				ch();
				return MINUS_EQ;
			}
			else
				return '-';

		case '<':
			if (ch() == '>') { 
				ch(); 
				return NE; 
			} 
			else if (nextch == '=') { 
				ch(); 
				return LE; 
			} 
			else if (nextch == '<') { 
				ch(); 
				return LSH; 
			} 
			else 
				return '<';

		case '!':
			if (ch() == '=') {
				ch();
				return NE;
			}
			else
				return NOT;

		case '>':
			if (ch() == '=') { 
				ch(); 
				return GE; 
			} 
			else if (nextch == '>') { 
				ch(); 
				return RSH; 
			} 
			else 
				return '>';

		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9': {
			char buf[MAX_TOKEN_LENGTH];
			int stored = 0;
			bool isFloat = false;
			const char *state = "01234567890.eE";
			do {
				if (stored<sizeof(buf)-1)
					buf[stored++] = nextch;
				ch();
				if (nextch=='e'||nextch=='E') {
					isFloat = true;
					state = "01234567890eE+-";
				}
			} while (nextch && strchr(state,nextch));
			buf[stored] = '\0';
			//puts(buf);
			if (isFloat || strchr(buf,'.')) {
				errno = 0;
				yylval.fval = atof(buf);
				if (errno) 
					scr_warningf("Floating-point literal out of range (%s)",buf);
				return FLOATLIT;
			}
			else {
				errno = 0;
				yylval.ival = atoi(buf);
				if (errno) 
					scr_warningf("Integer literal out of range (%s)",buf);
				return INTLIT;
			}
		}

		case '"': {
			char buf[255];
			int stored = 0;
			bool whined = false;
			while (ch() != '"' && nextch != EOF) {
				int tmpch = nextch;
				if (nextch == '\\' && !s_NoEscape) switch(ch()) {
					case 'n': tmpch = '\n'; break;
					case 'r': tmpch = '\r'; break;
					case '\"': tmpch = '\"'; break;
				}
				if (stored < sizeof(buf)-1)
					buf[stored++] = tmpch;
				else if (!whined) {
					scr_warningf("String constant cannot be longer than 254 characters, will be truncated.");
					whined = true;
				}
			}
			ch();
			buf[stored] = 0;
			yylval.sval = string_duplicate(buf);
			return STRLIT;
		}
			

		default: {
			if ((nextch >= 'a' && nextch <= 'z') || (nextch >= 'A' && nextch <= 'Z') || nextch=='_') {
				char buf[MAX_TOKEN_LENGTH];
				int stored = 0;
				bool warned = false;
				do {
					if (stored < (int)sizeof(buf)-1)
						buf[stored++] = (nextch >= 'a' && nextch <= 'z')? nextch - 32 : nextch;
					else if (!warned) {
						buf[stored] = 0;
						scr_warningf("Symbol starting with '%s' truncated to %d characters.",buf,(int)sizeof(buf)-1);
						warned = true;
					}
					ch();
				} while ((nextch >= 'a' && nextch <= 'z') || (nextch >= 'A' && nextch <= 'Z') || (nextch >= '0' && nextch <= '9') || nextch == '_');
				buf[stored] = 0;
				if (s_InDisabledBlock)	// don't spew crap into symbol table if we're skipping a block
					return TYPENAME;
				const int rwCount = sizeof(rw) / sizeof(rw[0]);
				int low = 0;
				int high = rwCount-1;
				while (low <= high) {
					int mid = (low+high)>>1;
					int val = strcmp(rw[mid].name,buf);
					if (val == 0)
						return rw[mid].value;
					else if (val < 0)
						low = mid+1;
					else
						high = mid-1;
				}
				if (s_Structs.GetCount() && s_Structs.Top()) {
					if ((yylval.field = s_Structs.Top()->LookupField(buf)) == 0)
						scr_errorf("Unknown field name, or not a structure");
					return FIELD;
				}
				for (int scope=s_Symbols.GetCount()-1; scope>=0; --scope) {
					scrSym **s = s_Symbols[scope]->Access(buf);
					if (s) {
						if (*s) {
							(*s)->Init(yylval);
							// (*s)->WasReferenced();
							return (*s)->LexVal;
						}
						else {
							if (!s_IgnoreDebugStuff)
								scr_error("Unknown new symbol referenced.");
							return NEWSYM;
						}
					}
				}
				
				if (!s_Symbols.GetCount()) {
					scr_errorf("Found crap after ENDSCRIPT?");
					return ENDSCRIPT;
				}
				
				safecpy(s_LastToken,buf,sizeof(s_LastToken));
				yylval.newsymhash.sym = &s_Symbols.Top()->operator[](ConstString(buf));
				*(yylval.newsymhash.sym) = NULL;
				u32 hash = scrComputeHash(buf);
				hash += !hash;	// Don't allow zero
				yylval.newsymhash.hash = hash;
				return NEWSYM;
			}
			else {
				if (nextch == ';')
					scr_warningf("This isn't C++, do not taunt me with semicolons!");
				else
					scr_warningf("Unknown character '%c' in input.",nextch);
				return 0;
			}
		}
	}
}


bool get_key_word(char *buf,int bufSize) {
	--bufSize;
	bool warned = false;
	while (nextch==32||nextch==9)
		ch();
	if ((nextch >= 'a' && nextch <= 'z') || (nextch >= 'A' && nextch <= 'Z') || nextch=='_') {
		int stored = 0;
		do {
			if (stored < bufSize)
				buf[stored++] = (nextch >= 'a' && nextch <= 'z')? nextch - 32 : nextch;
			else if (!warned) {
				buf[stored] = 0;
				scr_warningf("Keyword starting with '%s' truncated to %d characters.",buf,bufSize);
				warned = true;
			}
			ch();
		} while ((nextch >= 'a' && nextch <= 'z') || (nextch >= 'A' && nextch <= 'Z') || (nextch >= '0' && nextch <= '9') || nextch == '_');
		buf[stored] = 0;
		return true;
	}
	else if (nextch == '(' || nextch == ')') {
		buf[0] = nextch;
		buf[1] = 0;
		ch();
		return true;
	}
	else
		return false;
}

static int s_Endif;

int scr_lex() {
	int token, parenLevel = 1;
	if (s_IgnoreDebugStuff) {
		do {
			token = scr_lex_inner();
			// printf("ignoring token %d %c\n",token,token);
			if (token == '(')
				++parenLevel;
			else if (token == ')')
				--parenLevel;
			// Check a bunch of tokens that we won't see before the closing paren in case it's missing
			else if (token == ENDFUNC || token == ENDPROC || token == ENDFUNC || token == ENDIF || token == ENDWHILE || token == ENDREPEAT || token == ENDSWITCH || token == CASE || token == -1)
				scr_errorf("Error trying to skip DEBUGONLY PROC or FUNC");
		} while (parenLevel);
		return ')';
	}
	for (;;) {
		token = scr_lex_inner();
		if (token == USING) {
			s_NoEscape = true;
			if (scr_lex_inner() != STRLIT)
				scr_errorf("USING directive expected to be followed by a quoted string");
			else if (yylval.sval[0]==' ' || (yylval.sval[0] && yylval.sval[strlen(yylval.sval)-1]==' '))
				scr_errorf("USING directive cannot have spaces between quotes and actual filename");
			else
				scr_using(yylval.sval);
			s_NoEscape = false;
		}
		else if (token != '#')
			break;
		else {
			char buf[MAX_TOKEN_LENGTH];
			if (get_key_word(buf,sizeof(buf))) {
				if (!strcmp(buf,"ENDIF")) {
					if (!s_Endif)
						scr_errorf("#ENDIF without a matching #IF[N]DEF");
					else {
						--s_Endif;
						continue;
					}
				}
				if (/*strcmp(buf,"IFDEF") && strcmp(buf,"IFNDEF") &&*/ strcmp(buf,"IF"))
					scr_errorf("Preprocessor directive must be #if, not #%s",buf);
				bool _if = !strcmp(buf,"IF");
				bool _ifdef = !strcmp(buf,"IFDEF");
				bool _ifndef = !strcmp(buf,"IFNDEF");
				bool _if_inverted = false;
				bool got_key_word = get_key_word(buf,sizeof(buf));
				if (got_key_word && _if && !strcmp(buf,"NOT")) {
					_if_inverted = true;
					got_key_word = get_key_word(buf,sizeof(buf));
				}
				if (got_key_word && _if && !strcmp(buf,"DEFINED")) {
					got_key_word = get_key_word(buf,sizeof(buf));
					if (got_key_word && !strcmp(buf,"(")) {
						got_key_word = get_key_word(buf,sizeof(buf));
						char temp[64];
						if (!got_key_word || !get_key_word(temp,sizeof(temp)) || strcmp(temp,")"))
							scr_errorf("Missing ')' after 'DEFINED('");
					}
					_ifdef = !_if_inverted;
					_ifndef = _if_inverted;
					_if = false;
				}
				if (got_key_word) {
					bool found = false;
					int value = 0;
					if (!strcmp(buf,"FINAL")) {
						scr_errorf("FINAL preprocessor symbol is deprecated, use #IF IS_FINAL_BUILD instead.");
						found = PARAM_final.Get() != 0, value = 0;
					}
					else if (!strcmp(buf,"DEFINE_DEBUG_MODE")) {
						scr_errorf("DEFINE_DEBUG_MODE preprocessor symbol is deprecated, use #IF IS_DEBUG_BUILD instead.");
						found = PARAM_final.Get() == 0, value = 0;
					}
					else if (!strcmp(buf,"IS_FINAL_BUILD"))
						found = true, value = PARAM_final.Get() != 0;
					else if (!strcmp(buf,"IS_DEBUG_BUILD"))
						found = true, value = PARAM_final.Get() == 0;
					else if (!strcmp(buf,"IS_PC_BUILD"))
						found = true, value = (scriptPlatform == SCRIPT_PLATFORM_WIN64);
					else if (!strcmp(buf,"IS_PS4_BUILD"))
						found = true, value = (scriptPlatform == SCRIPT_PLATFORM_PS4);
					else if (!strcmp(buf,"IS_PS5_BUILD"))
						found = true, value = (scriptPlatform == SCRIPT_PLATFORM_PS5);
					else if (!strcmp(buf,"IS_XB1_BUILD"))
						found = true, value = (scriptPlatform == SCRIPT_PLATFORM_XB1);
					else if (!strcmp(buf,"IS_XBSX_BUILD"))
						found = true, value = (scriptPlatform == SCRIPT_PLATFORM_XBSX);
					else for (int scope=s_Symbols.GetCount()-1; scope>=0; --scope) {
						scrSym **s = s_Symbols[scope]->Access(buf);
						if (s) {
							scrSymInt *intlit = static_cast<scrSymInt*>(*s);
							found = true;
							if (intlit) {
								if (intlit->LexVal == INTLIT)
									value = intlit->Value;
								// code actually does some wacky stuff like #ifdef SOME_FUNCTION_NAME, which is legitimate.
								// else
								//	scr_warningf("Symbol '%s' is not a CONST_INT, will be treated as integer zero",buf);
								break;
							}
							else if (_if)
								scr_warningf("Symbol is tested via #IF too close to its definition, please insert a dummy declaration (like another CONST_INT) in between them");
						}
					}
					if (_if) {
						if (!found)
							scr_warningf("Unknown symbol '%s' in #if",buf);
						else {
							_ifdef = true;
							found = (value != 0);
							if (_if_inverted)
								found = !found;
						}
					}
					if ((_ifdef && found) || (_ifndef && !found)) {
						++s_Endif;
					}
					else {	// skip the block
						++s_InDisabledBlock;
						int EndifsToMatch = 1;
						for (;;) {
							token = scr_lex_inner();
							if (token == EOF) {
								scr_errorf("End of file found before #IF[N]DEF block was completed?");
								return token;
							}
							if (token == '#' && get_key_word(buf,sizeof(buf)))
							{
								if (!strcmp(buf,"ENDIF"))
								{
									EndifsToMatch--;
									if (EndifsToMatch == 0)
									{
										break;
									}
								}
								else if ( /*(!strcmp(buf,"IFDEF")) || (!strcmp(buf,"IFNDEF")) ||*/ (!strcmp(buf,"IF")) )
								{
									EndifsToMatch++;
								}
								else
								{
									scr_errorf("String following # should be IF or ENDIF (this is inside a block of code that has been disabled by an earlier #IF[N]DEF");
								}
							}
						}
						--s_InDisabledBlock;
					}
				}
				else
					scr_errorf("Expected symbol name after #if" /*,_ifdef?"def":_ifndef?"ndef":""*/);
			}
			else
				scr_errorf("Expected keyword after '#' character");
		}
	}	
	return token;
}
/* int scr_lex() {
	int rv = scr_lex_1();
	printf("lex: %d\n",rv);
	return rv;
} */
