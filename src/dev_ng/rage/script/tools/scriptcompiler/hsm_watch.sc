USING "rage_builtins.sch"

STRUCT WTime
	INT Hour
	INT Minute
	INT Second
	INT Millisecond
ENDSTRUCT

STRUCT WDate
	INT Month
	INT Day
	INT Year
ENDSTRUCT

WTime m_Time
WTime m_Stopwatch
WDate m_Date

PROC IncrementHours(WTime& time)
	time.Hour = time.Hour + 1
	IF time.Hour >= 24
		time.Hour = 0
	ENDIF
ENDPROC

PROC IncrementMinutes(WTime& time)
	time.Minute = time.Minute + 1
	IF time.Minute >= 60
		time.Minute = 0
	ENDIF
ENDPROC

PROC IncrementSeconds(WTime& time)
	time.Second = time.Second + 1
	IF time.Second >= 60
		time.Second = 0
	ENDIF
ENDPROC

PROC IncrementMonth(WDate& date)
	date.Month = date.Month + 1
	IF date.Month > 12
		date.Month = 1
	ENDIF
ENDPROC

PROC IncrementDay(WDate& date)
	date.Day = date.Day + 1
	IF date.Day > 31
		date.Day = 1
	ENDIF
ENDPROC

PROC IncrementYear(WDate& date)
	date.Year = date.Year + 1
	IF date.Year > 99
		date.Year = 0
	ENDIF
ENDPROC

PROC UpdateTime(WTime& time, INT milliseconds)
	time.Millisecond = time.Millisecond + milliseconds
	WHILE time.Millisecond >= 1000
		time.Millisecond -= 1000
		time.Second += 1
	ENDWHILE
	WHILE time.Second >= 60
		time.Second -= 60
		time.Minute += 1
	ENDWHILE
	WHILE time.Minute >= 60
		time.Minute -= 60
		time.Hour += 1
	ENDWHILE
	WHILE time.Hour >= 24
		time.Hour -= 24
	ENDWHILE
ENDPROC

ACTIVATE
	m_Time.Hour = 0
	m_Time.Minute = 0
	m_Time.Second = 0
	m_Time.Millisecond = 0
	m_Stopwatch.Hour = 0
	m_Stopwatch.Minute = 0
	m_Stopwatch.Second = 0
	m_Time.Millisecond = 0
	m_Date.Month = 1
	m_Date.Day = 1
	m_Date.Year = 0
ENDACTIVATE

DEFAULTSTATE Main

STATE Main
	DEFAULTSTATE ShowTime
	
	EVENT Update()
		UpdateTime(m_Time, FLOOR(TIMESTEP() * 1000))
		PRINTINT(m_Time.Hour) PRINTSTRING(":")
		PRINTINT(m_Time.Minute) PRINTSTRING(":")
		PRINTINT(m_Time.Second) PRINTSTRING(":")
		PRINTINT(m_Time.Millisecond) PRINTNL()
	ENDEVENT
		
	STATE ShowTime
		ACTIVATE
			PRINTSTRING("Showing Time\n")
		ENDACTIVATE
		
		EVENT ButtonA()
			GOTOSTATE ShowDate
		ENDEVENT
		
		EVENT ButtonB()
			GOTOSTATE SettingTime
		ENDEVENT
	ENDSTATE	//Time
	
	STATE ShowDate
		ACTIVATE
			PRINTSTRING("Showing Date\n")
		ENDACTIVATE
		
		EVENT ButtonA()
			GOTOSTATE ShowStopwatch
		ENDEVENT
		
		EVENT ButtonB()
			GOTOSTATE SettingDate
		ENDEVENT
	ENDSTATE	//Date
	
	STATE ShowStopwatch
		HISTORY
		
		DEFAULTSTATE Stopped
		
		ACTIVATE
			PRINTSTRING("Showing Stopwatch\n")
		ENDACTIVATE
		
		EVENT ButtonA()
			GOTOSTATE ShowTime
		ENDEVENT
		
		STATE Stopped
			EVENT ButtonB()
				GOTOSTATE Running
			ENDEVENT
		ENDSTATE
		
		STATE Running
			EVENT Update()
				UpdateTime(m_Stopwatch, FLOOR(TIMESTEP() * 1000))
			ENDEVENT
			
			EVENT ButtonB()
				GOTOSTATE Stopped
			ENDEVENT
		ENDSTATE
	ENDSTATE	//Stopwatch
ENDSTATE
	
STATE SettingTime
	DEFAULTSTATE SettingHours
	
	ACTIVATE
		PRINTSTRING("Setting Time\n")
	ENDACTIVATE
	
	STATE SettingHours
		EVENT ButtonA()
			IncrementHours(m_Time)
		ENDEVENT
		
		EVENT ButtonB()
			GOTOSTATE SettingMinutes
		ENDEVENT
	ENDSTATE
	
	STATE SettingMinutes
		EVENT ButtonA()
			IncrementMinutes(m_Time)
		ENDEVENT
		
		EVENT ButtonB()
			GOTOSTATE SettingSeconds
		ENDEVENT
	ENDSTATE
	
	STATE SettingSeconds
		EVENT ButtonA()
			IncrementSeconds(m_Time)
		ENDEVENT
		
		EVENT ButtonB()
			GOTOSTATE ShowTime
		ENDEVENT
	ENDSTATE
ENDSTATE	//SettingTime

STATE SettingDate
	DEFAULTSTATE SettingMonth
	
	ACTIVATE
		PRINTSTRING("Setting Date\n")
	ENDACTIVATE
	
	STATE SettingMonth
		EVENT ButtonA()
			IncrementMonth(m_Date)
		ENDEVENT
		
		EVENT ButtonB()
			GOTOSTATE SettingDay
		ENDEVENT
	ENDSTATE
	
	STATE SettingDay
		EVENT ButtonA()
			IncrementDay(m_Date)
		ENDEVENT
		
		EVENT ButtonB()
			GOTOSTATE SettingYear
		ENDEVENT
	ENDSTATE
	
	STATE SettingYear
		EVENT ButtonA()
			IncrementYear(m_Date)
		ENDEVENT
		
		EVENT ButtonB()
			GOTOSTATE ShowDate
		ENDEVENT
	ENDSTATE
ENDSTATE	//SettingDate

SCRIPT
	WHILE TRUE
		//WAIT(0)
		Update()
	ENDWHILE
ENDSCRIPT
