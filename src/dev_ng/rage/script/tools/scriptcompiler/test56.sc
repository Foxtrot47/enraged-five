USING "rage_builtins.sch"

SCRIPT
	INT iFinishTime = 57123
	FLOAT fFinishTime = iFinishTime / 1000.0
	PRINTSTRING("Finish time is ")
	PRINTFLOAT2(7,3,fFinishTime)
	PRINTSTRING(" seconds!\n")
ENDSCRIPT
