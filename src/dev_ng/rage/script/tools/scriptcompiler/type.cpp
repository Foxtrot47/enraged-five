#include "type.h"
#include "node.h"
#include "script/thread.h"	// for scrTextLabelSize
#include "script/hash.h"

#include <stdio.h>

#include "file/stream.h"

using namespace rage;

extern scrTypeObject s_StructRef;

/*
	scrType instances are compared by address, so don't ever instantiate more
	than one of any given scrType subclass.
*/


scrType::scrType(const scrType *parent) : Parent(parent) { }


scrType::~scrType() { }


void scrType::EmitDebugInfo(fiStream *S,const char *scope,const char *name,int offset) const {
	DeclareType(S);

	char buf[MAX_TOKEN_LENGTH];
	GetName(buf);
	fprintf(S,"%s,%s,%s,%d\r\n",scope,buf,name,offset);
}


void scrType::EmitGlobalInfo(fiStream *S,const char *varname) const {
	fprintf(S,"%u,%s\r\n",GetSize(),varname);
}


scrTypeBool scrTypeBool::Instance;

scrTypeInt scrTypeInt::Instance;
scrTypeInt scrTypeInt::EnumToIntInstance;

scrTypeFloat scrTypeFloat::Instance;

scrTypeStringHash scrTypeStringHash::Instance;

scrTypeString scrTypeString::Instance;

scrTypeNull scrTypeNull::Instance;

// Only its address is significant, not its contents.
scrTypeList rage::g_VARARGS(&scrTypeInt::Instance,NULL);
scrTypeList rage::g_VARARGS1(&scrTypeInt::Instance,NULL);
scrTypeList rage::g_VARARGS2(&scrTypeInt::Instance,NULL);
scrTypeList rage::g_VARARGS3(&scrTypeInt::Instance,NULL);

scrTypeEnum::Enumerant* scrTypeEnum::AddEnumerant(const char *name,int value) {
	int sl = (int)strlen(name);
	Enumerant *e = rage_new Enumerant;
	e->Name = (char*)scrAllocator::Allocate(sl+1);
	strcpy((char*)e->Name,name);
	e->Value = value;


	e->Next = m_Last;
	e->Parent = this;
	m_Last = e;
	return e;
}

bool scrTypeEnum::HasThisValue(int value) const {
	for (Enumerant *i = m_Last; i; i=i->Next)
		if (i->Value == value)
			return true;
	return false;
}


void scrTypeEnum::GetName(char *dest) const {
	strcpy(dest,"ENUM "); 
	strcat(dest,Name);
}


void scrTypeEnum::DeclareType(fiStream *S) const {
	if (!Emitted) {
		const int maxEnum = 32;
		int count = 0;
		Enumerant *e = m_Last;
		while (e) {
			++count;
			e = e->Next;
		}
		const_cast<bool&>(Emitted) = true;
		fprintf(S,"ENUM,%s,%d\r\n",Name.m_String,Min(count,maxEnum));
		e = m_Last;
		// Could do this more simply with recursion
		// but that likes to randomly fail on large datasets.
		Enumerant **list = rage_new Enumerant*[count];
		Enumerant **top = list + count;
		while (e) {
			*(--top) = e;
			e = e->Next;
		}
		for (int i=0; i<Min(count,maxEnum); i++)
			fprintf(S,"  FIELD,%s,%d\r\n",list[i]->Name,list[i]->Value);
		delete[] list;
	}
}



int scrType::GetSize() const {
	return 1;
}


scrTypeArray::scrTypeArray(const scrType *type,int count) : Type(type), Count(count), CTypeName(NULL) { 
	char buf[128];
	formatf(buf,count == VariableArraySentinel? "scrArrayBase<%s>" : "scrArray<%s,%d>",Type->GetCType(),Count);
	CTypeName = buf;
}

scrTypeArray::~scrTypeArray() { }

void scrTypeArray::EmitGlobalInfo(fiStream *S,const char *varname) const {
	fprintf(S,"%u,%s[%u]\r\n",GetSize(),varname,Count);
	char buf[256];
	safecpy(buf,",");
	safecat(buf,varname);
	Type->EmitGlobalInfo(S,buf);
}

void scrTypeArray::GetName(char *dest) const {
	sprintf(dest,Count==VariableArraySentinel? "[]" : "[%d]",Count);
	Type->GetName(dest+strlen(dest));
}

void scrTypeArray::EmitConstruct() const {		// addr
	scrNode::EmitInt(Count);					// addr Count
	scrNode::EmitOp(OP_STORE_REV);				// addr  (NOTE - OP_STORE_REV leaves its address behind)
	if (Type->NeedEmitConstruct()) {
		scrNode::EmitOp(OP_DUP);				// addr addr
		scrNode::EmitInt(1);					// addr addr 4
		scrNode::EmitOp(OP_IOFFSET);				// addr addr+4
		for (int i=0; i<Count; i++) {
			Type->EmitConstruct();
			scrNode::EmitInt(Type->GetSize());	// addr addr+4 elSize4 -
			scrNode::EmitOp(OP_IOFFSET);		// addr addr+4+elSize4
		}
		scrNode::EmitOp(OP_DROP);				// addr
	}
}


scrTypePoolIndex scrTypePoolIndex::Instance;

void scrTypePoolIndex::EmitConstruct() const {	// addr
	scrNode::EmitOp(OP_PUSH_CONST_M1);			// addr -1
	scrNode::EmitOp(OP_STORE_REV);				// addr
}


scrTypeStruct::scrTypeStruct(const char *name) : Size(0), Emitted(false), Name(name) {}


scrTypeStruct::~scrTypeStruct() { }


void scrTypeStruct::GetName(char *dest) const {
	strcpy(dest,"STRUCT "); 
	strcat(dest,Name);
}


const scrField* scrTypeStruct::AddField(const scrType *type,const char *name) {
	scrField &f = Fields.Grow();
	f.Type = type;
	char *n = (char*) scrAllocator::Allocate(strlen(name)+1);
	strcpy(n,name);
	f.Name = n;
	f.Offset = Size;
	type->HasDefaultValue(f.Default);		// pick up any default value
	Size += type->GetSize();
	return &f;
}


int scrTypeStruct::GetSize() const {
	return Size;
}


const scrField* scrTypeStruct::LookupField(const char *name) const {
	int i;
	for (i=0; i<Fields.GetCount(); i++)
		if (!strcmp(name,Fields[i].Name))
			return &Fields[i];
	return 0;
}


void scrTypeStruct::DeclareType(fiStream *S) const {
	if (!Emitted) {
		const_cast<bool&>(Emitted) = true;
		for (int i=0; i<Fields.GetCount(); i++)
			Fields[i].Type->DeclareType(S);
		fprintf(S,"STRUCT,%s,%d\r\n",Name.m_String,Fields.GetCount());
		for (int i=0; i<Fields.GetCount(); i++) {
			Fields[i].Type->EmitDebugInfo(S,"  FIELD",Fields[i].Name,Fields[i].Offset);
		}
	}
}

void scrTypeStruct::EmitDebugInfo(fiStream *S,const char *scope,const char *name,int offset) const {
	for (int i=0; i<Fields.GetCount(); i++)
		Fields[i].Type->DeclareType(S);
	scrType::EmitDebugInfo(S,scope,name,offset);
}


void scrTypeStruct::EmitGlobalInfo(fiStream *S,const char *varname) const {
	scrType::EmitGlobalInfo(S,varname);
	for (int i=0; i<Fields.GetCount(); i++) {
		char buf[512];
		safecpy(buf,",");
		safecat(buf,varname);
		safecat(buf,".");
		safecat(buf,Fields[i].Name);
		Fields[i].Type->EmitGlobalInfo(S,buf);
	}
}

void scrTypeStruct::EmitConstruct() const { 
	for (int i=0; i<Fields.GetCount(); i++) {
		if (Fields[i].Type->NeedEmitConstruct()) {
			scrNode::EmitOp(OP_DUP);		// addr addr
			if (Fields[i].Offset) {
				scrNode::EmitInt(Fields[i].Offset);	// addr addr offset
				scrNode::EmitOp(OP_IOFFSET);	// addr addr+offset
			}
			Fields[i].Type->EmitConstruct();
			scrNode::EmitOp(OP_DROP);		// addr
		}
		else if (Fields[i].Default.Value.asInt != 0) {
			scrNode::EmitOp(OP_DUP);		// addr addr
			if (Fields[i].Offset) {
				scrNode::EmitInt(Fields[i].Offset);	// addr addr offset
				scrNode::EmitOp(OP_IOFFSET);	// addr addr+offset
			}
			scrNode::EmitInt(Fields[i].Default.Value.asInt);	// addr addr+offset value
			scrNode::EmitOp(OP_STORE_REV);		// addr addr+offset (NOTE - OP_STORE_REV leaves its address behind)
			scrNode::EmitOp(OP_DROP);			// addr
		}
	}
}


bool scrTypeStruct::ContainsStringOrFuncPtr() const {
	for (int i=0; i<Fields.GetCount(); i++)
		if (Fields[i].Type->ContainsStringOrFuncPtr())
			return true;
	return false;
}

void scrTypeStruct::EmitSource() const {
	int neededInit = 0;
	for (int i=0; i<Fields.GetCount(); i++) {
		const char *zt = Fields[i].Type->GetZeroType();
		if (zt[0]) {
			if (!neededInit)
				scrNode::EmitChannel(SCRIPT_DECLS,"\t%s() : %s",Name.c_str(),Fields[i].Name);
			else
				scrNode::EmitChannel(SCRIPT_DECLS,"\t\t, %s",Fields[i].Name);
			if (Fields[i].Default.IsFloat)
				scrNode::EmitChannel(SCRIPT_DECLS,"(%f)\n",Fields[i].Default.Value.asFloat);
			else
				scrNode::EmitChannel(SCRIPT_DECLS,"(%d)\n",Fields[i].Default.Value.asInt);
			++neededInit;
		}
	}
	if (neededInit)
		scrNode::EmitChannel(SCRIPT_DECLS," { }\n");
}


scrTypeVector scrTypeVector::Instance;

int scrTypeVector::GetSize() const {
	return 3;
}

const scrField* scrTypeVector::LookupField(const char *name) const {
	if (name[0] >= 'X' && name[0] <= 'Z' && name[1]==0) {
		static scrField xyz[3] = {
			{ &scrTypeFloat::Instance, "x", 0 },
			{ &scrTypeFloat::Instance, "y", 1 },
			{ &scrTypeFloat::Instance, "z", 2 }
		};
		return &xyz[name[0] - 'X'];
	}
	else
		return NULL;
}


int scrTypeTextLabel::GetSize() const {
	return Count>>2;
}

const char *scrTypeTextLabel::GetCType() const {
	switch (Count) {
		case 4: return "scrTextLabel3";
		case 8: return "scrTextLabel7";
		case 16: return "scrTextLabel15";
		case 24: return "scrTextLabel23";
		case 32: return "scrTextLabel31";
		case 64: return "scrTextLabel63";
		default: return "scrTextLabelUNKNOWN";
	}
}


scrTypeObject::scrTypeObject(const char *name,const scrType *parent) : scrType(parent), Name(name){ 
}

scrTypeRef::scrTypeRef(const scrType *reference) : Reference(reference), Name(NULL) { 
	char ctypename[MAX_TOKEN_LENGTH];
	if (reference == &s_StructRef)
		safecpy(ctypename,"struct scr_any_struct *");
	else
		formatf(ctypename,"%s&",reference->GetCType());
	Name = ctypename;
}


void scrTypeRef::GetName(char *dest) const {
	Reference->GetName(dest);
	strcat(dest,"&");
}


void scrTypeRef::EmitDebugInfo(fiStream *S,const char *scope,const char *name,int offset) const {
	// The 512 is a magic flag indicating this is a reference 
	// (we don't support references to references so a single bit is enough)
	// We shift the original offset leftward to make room for the sub-fields.
	Reference->EmitDebugInfo(S,scope,name,512 | (offset<<10));
}


scrTypeDef::scrTypeDef(const scrCommandInfo *info,const char *name) : Info(info), CTypeName(name) { }
 
