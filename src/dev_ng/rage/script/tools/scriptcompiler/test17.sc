USING "rage_builtins.sch" 

PROC TEST_PROC(STRING S)
	TEXT_LABEL A, B, C
	A = S
	B = A
	C = "TEST_STRING"
	PRINTSTRING(A)
	PRINTNL()
	PRINTSTRING(B)
	PRINTNL()
	PRINTSTRING(C)
	PRINTNL()
ENDPROC

SCRIPT
	TEXT_LABEL SomeText
	TEXT_LABEL_23 SomeText2
	INT Number = 7

	// CLEAR_TEXT_LABEL (SomeText)
	// APPEND_STRING (SomeText,"Blah ")
	// APPEND_INT (SomeText,Number)
	SomeText = "Blah "
	SomeText += Number
	
	PRINTSTRING (SomeText)
	PRINTNL ()
	
	SomeText2 += SomeText
	SomeText2 += " ---- "
	SomeText2 += 10000000
	
	// Note how the text gets truncated:
	PRINTSTRING (SomeText2)
	PRINTNL ()

	TEST_PROC("String")
ENDSCRIPT
