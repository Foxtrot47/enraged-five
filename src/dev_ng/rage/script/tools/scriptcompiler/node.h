#ifndef SCRIPT_NODE_H
#define SCRIPT_NODE_H

const int MAX_TOKEN_LENGTH = 96;

#include "type.h"

#include "script/opcode.h"
#include "script/program.h"
#include "script/value.h"
#include "atl/array.h"

#include "allocator.h"
#include "diag/output.h"

namespace rage {

// #define errorf(fmt,...) Displayf(TRed fmt TNorm,__VA_ARGS__)
#define errorf Errorf

#if __WIN32
#pragma warning(disable: 4514)
#endif

const int MAX_USING_FILES = 1374;
const int MAX_LINEINFO = 1350000;

/*
	This is used only during compilation.
*/
enum scrNodeType {
		NODE_VAR_DECL, NODE_ASSIGN, NODE_STMTS, NODE_IF, NODE_WHILE, NODE_REPEAT, NODE_RETURN, NODE_BINARY, NODE_UNARY,
		NODE_VARIABLE, NODE_BOOL, NODE_INT, NODE_FLOAT, NODE_STRING, NODE_VECTOR, NODE_SCRIPT, NODE_TUPLE, NODE_EXPRLIST, NODE_COMMAND,
		NODE_FUNCTION_CALL, NODE_FOR, NODE_SWITCH, NODE_BREAK, NODE_CASE, NODE_DEFAULT, NODE_DEREFERENCE, NODE_LOCAL, NODE_STATIC, 
		NODE_GLOBAL, NODE_ENUM, NODE_NULL, NODE_ARRAY_LOOKUP, NODE_CONSTRUCT, NODE_APPEND, NODE_GOTO, NODE_LABEL, NODE_THROW,
        NODE_FUNCTION_ADDR, NODE_DECL, NODE_FIELD, NODE_COUNTOF, NODE_SCOPE,
		NODE_DATATABLE, NODE_BREAKLOOP, NODE_RELOOP
};


class scrNodeScript;

struct scrCommandReference {
	u32 Addr;
	scrCommandReference *Next;
};

// DO NOT PUT ANY COMPLEX TYPES IN THIS CLASS
// We're lame and count on new foo() to zero-init this as a POD.
struct scrCommandInfo {
	const scrType *ReturnType;
	const scrTypeList *Parameters;
	u64 NativeHash;		// For C++ commands
	mutable int NativeSlot;
	mutable u32 Address;		// For user-defined commands (zero of it has not been Emitted yet)
	scrNodeScript *Tree;
	const char *Filename;
	int LineNumber;
	const char *ProgName;
	mutable bool WasStubEmitted;
	mutable scrCommandReference *FirstRef;
	bool DebugOnly;
	bool HasVectorReferences;
};

struct scrDataList
{
	scrDataList(int i,scrDataList *n) : Next(n) { Data.Int = i; Count = n? n->Count + 1 : 1; }
	scrDataList(float f,scrDataList *n) : Next(n) { Data.Float = f; Count = n? n->Count + 1 : 1; }
	union {
		float Float;
		unsigned Unsigned;
		int Int;
	} Data;
	scrDataList *Next;
	int Count;
};

struct scrLocal 
{
#if RAGE_ENABLE_RAGE_NEW
	static void *operator new(size_t size,const char*,int) { return scrAllocator::Allocate(size); }
#else
	static void *operator new(size_t size) { return scrAllocator::Allocate(size); }
#endif
	scrLocal(const scrType *type,const char *name,int address) : Type(type), Name(name), Address(address), Next(0) { }
	const scrType *Type;
	HeapString Name;
	int Address;
	scrLocal *Next;
};

enum { SCRIPT_DECLS, SCRIPT_GLOBALS, SCRIPT_STATICS, SCRIPT_STUBS, SCRIPT_CHANNEL_COUNT };

class scrNode {
public:
	scrNode(scrNodeType type);
	virtual ~scrNode() { }

#if RAGE_ENABLE_RAGE_NEW
	static void *operator new(size_t size,const char*,int) { return scrAllocator::Allocate(size); }
#else
	static void *operator new(size_t size) { return scrAllocator::Allocate(size); }
#endif

	static void InitClass(int stringHeapSize);
	static void ShutdownClass();

	virtual void Emit() = 0;
	virtual bool EmitRef() { return false; }

	static int GetPC() { return Program.GetCount(); }
	static void Emit8(u8 b);

	static void EmitOp(scrOpcode op,int sizeOverride = 0);
	static void Emit16(u16 w) { Emit8((u8)(w>>8)); Emit8((u8)w); }
	static void EmitS16(int w) { Assert(w>=-32768&&w<=32767); Emit8((u8)(w>>8)); Emit8((u8)w); }
	static void Emit24(u32 w) { Emit8((u8)(w>>16)); Emit8((u8)(w>>8)); Emit8((u8)w); }
	static void Emit32(u32 w) { Emit8((u8)(w>>24)); Emit8((u8)(w>>16)); Emit8((u8)(w>>8)); Emit8((u8)w); }
	static void EmitCall(const scrCommandInfo *info,bool indirect);
	static void EmitAccumulatedCalls();
	static void EmitInt(int v);
	static void EmitAdd(int v);
	static void EmitFloat(float v);
	static void EmitSource(const char *fmt,...);
	static void BeginCall(const char *name) { EmitSource(name); EmitSource("("); }
	static void Comma() { EmitSource(","); }
	static void NewLine() { EmitSource("\n"); }
	static void EndCall() { EmitSource(")"); }
	static void EndStmt() { EmitSource(";\n"); }
	static void BeginBlock() { EmitSource("\t{\n"); ++SourceIndent; }
	static void EndBlock() { --SourceIndent; EmitSource("\t}\n"); }
	static void EmitSymbol(const char *sym) { EmitSource("%s",sym); /* TODO: mangling? */ }
	static void EmitChannel(int /*channel*/,const char * /*fmt*/,...);

	static void SetRoot(scrNode *root) { Root = root; }
	static scrNode* GetRoot() { return Root; } 
	static void EmitNativeStub(const scrCommandInfo *info,int extraSize);

	static void SetStatics(scrValue *statics,int staticSize) {
		Statics = statics;
		StaticSize = staticSize;
	}

	static scrValue* GetStatics() { return Statics; }
	static int GetStaticSize() { return StaticSize; }

	static void SetGlobals(scrValue *globals,int globalSize) {
		Globals = globals;
		GlobalSize = globalSize;
	}

	static scrValue* GetGlobals() { return Globals; }
	static int GetGlobalSize() { return GlobalSize; }

	static const char *GetStringHeap() { return pStringHeap; }
	static int GetStringHeapSize() { return StringHeapSize; }
	static int GetMaxStringHeapSize() { return ms_AllocatedSizeOfStringHeap; }

	static void Reset() {
		Program.Reset();
	}

	static const u8* GetProgram() { return Program.GetCount()? &Program[0] : 0; }
	static int GetProgramSize() { return Program.GetCount(); }
	static int GetMaxProgramSize() { return Program.GetMaxCount(); }

	static u64 *GetNatives() { return Natives.GetCount()? &Natives[0] : 0; }
	static u32 GetNativeSize() { return Natives.GetCount(); }

	static int GetLineInfo(int offset);

	struct LineInfo {
		int m_BasePc;
		int m_LineNumber;
		u16 m_FileIndex;
		static int cmp(const LineInfo *a,const LineInfo *b);
	};

	static atFixedArray<LineInfo,MAX_LINEINFO> sm_LineInfo;
	static atFixedArray<ConstString,MAX_USING_FILES> sm_FileInfo;

	scrNodeType GetNodeType() const { return TypeEnum; }

protected:
	static const u32 MaxNatives = 4096;	// can raise this to 65536 now if necessary...
	static atFixedArray<u64,MaxNatives> Natives;

	static int AddFile(const char *name,u32 hash);
	void node_errorf(const char *,...);
	void node_warningf(const char *,...);
	void UpdateLineInfo();
	static void LoadAddress(class scrNodeExpr *address);
	static void StoreAddress(class scrNodeExpr *address);
	int CollectParameters(const scrTypeList *formal,struct scrExprList *actual);
	void CheckTypeDef(const scrCommandInfo *formal,const scrCommandInfo *actual);
	scrNodeExpr *MakeFloat(scrNodeExpr *e);
	
	scrNodeType TypeEnum;
	int Line;
	int File;
	const char *GetFilename() const { return sm_FileInfo[File]; }

	static scrNode *Root;
	static int StaticSize;
	static int GlobalSize;
	static scrValue *Statics, *Globals;
	static atFixedArray<u8,MAX_LEGIT_OPS> Program;
	static int SourceIndent;
	static char *pStringHeap;
	static int ms_AllocatedSizeOfStringHeap;
	static int StringHeapSize;
};


//////////////////////////////////////////////////////////////////////////
///////////////////////   E X P R E S S I O N S   ////////////////////////
//////////////////////////////////////////////////////////////////////////

class scrNodeExpr: public scrNode {
public:
	scrNodeExpr(scrNodeType nodeType,const scrType *type);

	virtual bool IsConstant(int &) { return false; }
	virtual const char *IsStringConstant() const { return NULL; }
	const scrType *GetType() { return Type?Type->GetInnerType():NULL; }
	virtual void SetReadOnly(bool) { }
	virtual bool GetReadOnly() const { return false; }
	virtual bool IsDefaultParam() const { return false; }
	virtual bool IsSameAs(scrNodeExpr *) { return false; }
	virtual scrNodeExpr *GetAddressExpr() const { return NULL; }
protected:
	bool IsSameAsBase(scrNodeExpr *right) { 
		return TypeEnum == right->TypeEnum && Type==right->Type; 
	}
	const scrType *Type;
};

class scrNodeTuple: public scrNodeExpr {
public:
	scrNodeTuple(scrNodeExpr *x,scrNodeExpr *y,scrNodeExpr *z);
	void Emit();
protected:
	scrNodeExpr *X, *Y, *Z;
};


class scrNodeBinary: public scrNodeExpr {
public:
	scrNodeBinary(scrNodeExpr *left,scrOpcode op,scrNodeExpr *right,const scrType *type,const char *cOp);
	bool IsConstant(int &value);
	void Emit();
	bool Promote(bool isArithmetica);
protected:
	scrOpcode Op;
	scrNodeExpr *Left, *Right;
	const char *OpName;
};

class scrNodeArrayLookup: public scrNodeExpr {
public:
	scrNodeArrayLookup(scrNodeExpr *baseAddr,int elSize,scrNodeExpr *index);
	void Emit();
private:
	scrNodeExpr *Base;
	int ElementSize;
	scrNodeExpr *Index;
};

class scrNodeField: public scrNodeExpr {
public:
	scrNodeField(scrNodeExpr *address,const scrField *field,const scrTypeStruct *parent);
	void Emit();
private:
	scrNodeExpr *Address;
	const scrField *Field;
	const scrTypeStruct *Parent;
};

class scrNodeCountOf: public scrNodeExpr {
public:
	scrNodeCountOf(scrNodeExpr *address);
	void Emit();
private:
	scrNodeExpr *Address;
};

struct scrExprList 
{
#if RAGE_ENABLE_RAGE_NEW
	static void *operator new(size_t size,const char*,int) { return scrAllocator::Allocate(size); }
#else
	static void *operator new(size_t size) { return scrAllocator::Allocate(size); }
#endif
	scrExprList(scrNodeExpr *left,scrExprList *right) : Left(left), Right(right), Count(1+(Right?Right->Count:0)) { }
	scrNodeExpr *Left;
	scrExprList *Right;
	int Count;
};

class scrNodeFunctionCall: public scrNodeExpr {
public:
	scrNodeFunctionCall(const scrCommandInfo *ci,scrExprList *parameters,scrNodeExpr *addr /* NULL to use data in scrCommandInfo */);
	void Emit();
	bool EmitRef();
	virtual bool IsConstant(int &);
protected:
	const scrCommandInfo *Info;
	scrExprList *Parameters;
	scrNodeExpr *Addr;
	const char *Operator;
};


class scrNodeArithmetic: public scrNodeBinary {
public:
	scrNodeArithmetic(scrNodeExpr *left,scrOpcode opi,scrOpcode opf,scrOpcode opv,scrNodeExpr *right,const char *copi,const char *copf,const char *copv);
};


class scrNodeRelational: public scrNodeBinary {
public:
	scrNodeRelational(scrNodeExpr *left,scrOpcode opi,scrOpcode opf,scrNodeExpr *right,const char *copi,const char *copf);
};


class scrNodeEquality: public scrNodeBinary {
public:
	scrNodeEquality(scrNodeExpr *left,scrOpcode opi,scrOpcode opf,scrNodeExpr *right,const char *copi,const char *copf);
};


class scrNodeLogical: public scrNodeBinary {
public:
	scrNodeLogical(scrNodeExpr *left,scrOpcode op,scrNodeExpr *right,const char *copi);
};


class scrNodeShortCircuit: public scrNodeExpr {
public:
	scrNodeShortCircuit(scrNodeExpr *left,scrOpcode op,scrNodeExpr *right);
	bool IsConstant(int &value);
	void Emit();
protected:
	scrOpcode Op;
	scrNodeExpr *Left, *Right;
};


class scrNodeConditional: public scrNodeExpr {
public:
	scrNodeConditional(scrNodeExpr *cond,scrNodeExpr *ifTrue,scrNodeExpr *ifFalse);
	bool IsConstant(int &value);
	void Emit();
protected:
	scrNodeExpr *Cond, *IfTrue, *IfFalse;
};


class scrNodeUnary: public scrNodeExpr {
public:
	scrNodeUnary(scrOpcode op,scrNodeExpr *unary,const scrType *type,const char *cop);
	void Emit();
	bool IsConstant(int &value);
protected:
	scrOpcode Op;
	scrNodeExpr *Unary;
	const char *OpName;
};


class scrNodeDereference: public scrNodeExpr {
public:
	scrNodeDereference(const scrType *type,scrNodeExpr *address);
	void Emit();
	scrNodeExpr *GetAddressExpr() const { return Address; }
protected:
	scrNodeExpr *Address;
};


class scrNodeLogicalNot: public scrNodeUnary {
public:
	scrNodeLogicalNot(scrNodeExpr *expr);
};


class scrNodeNegate: public scrNodeUnary {
public:
	scrNodeNegate(scrNodeExpr *expr);
};


class scrNodeVarRef : public scrNodeExpr {
public:
	scrNodeVarRef(const scrType *type,scrNodeExpr *address);
	void Emit();
	bool EmitRef();
    scrNodeExpr* GetAddress() { return Address; }
	scrNodeExpr* GetAddressExpr() const { return Address->GetAddressExpr(); }
protected:
	scrNodeExpr *Address;
};


class scrNodeLocal: public scrNodeExpr {
public:
	scrNodeLocal(const scrType *type,const char *name,int offset);
	void Emit();
	virtual void SetReadOnly(bool value) { ReadOnly = value; }
	virtual bool GetReadOnly() const { return ReadOnly; }
	virtual bool IsSameAs(scrNodeExpr *rhs)  { return scrNodeExpr::IsSameAsBase(rhs) && Offset==((scrNodeLocal*)rhs)->Offset; }
protected:
	HeapString Name;
	int Offset;
	bool ReadOnly;
};


class scrNodeGlobal: public scrNodeExpr {
public:
	scrNodeGlobal(const scrType *type,const char *name,int offset);
	void Emit();
protected:
	HeapString Name;
	int Offset;
	bool WasReferenced;
};


class scrNodeStatic: public scrNodeExpr {
public:
	scrNodeStatic(const scrType *type,const char *name,int offset);
	void Emit();
protected:
	HeapString Name;
	int Offset;
};


class scrNodeNull: public scrNodeExpr {
public:
	scrNodeNull(const scrType *type);
	void Emit();
};


class scrNodeConstant: public scrNodeExpr {
public:
	scrNodeConstant(scrNodeType nodetype,const scrType *type);
};


class scrNodeBool: public scrNodeConstant {
public:
	scrNodeBool(bool value);
	void Emit();
	bool IsConstant(int &value);
protected:
	bool Value;
};

class scrNodeInt: public scrNodeConstant {
public:
	scrNodeInt(int value);
	void Emit();
	bool IsConstant(int &value);
protected:
	int Value;
};

class scrNodeDefaultParam: public scrNodeInt {
public:
	scrNodeDefaultParam() : scrNodeInt(0) { }
	virtual bool IsDefaultParam() const { return true; }
};


class scrNodeEnum: public scrNodeConstant {
public:
	scrNodeEnum(scrTypeEnum::Enumerant *value);
	void Emit();
	bool IsConstant(int &value);
protected:
	scrTypeEnum::Enumerant *Value;
};


class scrNodeFloat: public scrNodeConstant {
public:
	scrNodeFloat(float value);
	void Emit();
	bool IsConstant(int &value);
	float GetValue() const { return Value; }
protected:
	float Value;
};


class scrNodeString: public scrNodeConstant {
public:
	scrNodeString(const char *string);
	scrNodeString(scrDataList *list);
	void Emit();
	const char *IsStringConstant() const { return Value; }
protected:
	const char *Value;
	int Length;
	bool IsDataTable;
};

class scrNodeFunctionAddr: public scrNodeConstant {
public:
	scrNodeFunctionAddr(const scrCommandInfo *func);
	void Emit();
	bool IsConstant(int &value);

	const scrCommandInfo *Info;
	int Value;
};




//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////   S T A T E M E N T S   //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

class scrNodeStmt: public scrNode {
public:
	scrNodeStmt(scrNodeType type);

	virtual bool Returns() const { return false; }
	virtual void Emit2() = 0;			// second pass, for GOTO

	static int EmitForwardJump(scrOpcode type);
	static void EmitBackwardJump(scrOpcode type,int pc);
	static void PlaceJump(int target);
};


class scrNodeScript: public scrNode {
public:
	scrNodeScript(const char *progname,int lineNumber,scrNodeStmt *root,int paramCount,int varCount,const scrType *returnType,const scrLocal *firstLocal);
	void Emit();
//private:
	HeapString ProgName;
	scrNodeStmt *Root;
	int ParamCount;
	int VarCount;
	const scrType *ReturnType;
	const scrLocal *FirstLocal;
};

class scrNodeStmts: public scrNodeStmt {
public:
	scrNodeStmts(scrNodeStmt*,scrNodeStmt*);
	void Emit();
	void Emit2();
	bool Returns() const { return Tail? Tail->Returns() : (Head? Head->Returns() : false); }
protected:
	scrNodeStmt *Head;
	scrNodeStmt *Tail;
};


class scrNodeCommand: public scrNodeStmt {
public:
	scrNodeCommand(const scrCommandInfo *ci,scrExprList *parameters,scrNodeExpr *addr);
	void Emit();
	void Emit2() { }
protected:
	const scrCommandInfo *Info;
	scrExprList *Parameters;
	scrNodeExpr *Addr;
};


class scrNodeAssign: public scrNodeStmt {
public:
	scrNodeAssign(int lineNumber,const scrType *type,scrNodeExpr *address,scrNodeExpr *expr,bool defaultToZero = false);
	void Emit();
	void Emit2() { }
protected:
	const scrType *Type;
	scrNodeExpr *Address, *Expr;
	bool DefaultToZero;
};


class scrNodeAppend: public scrNodeStmt {
public:
	scrNodeAppend(scrNodeExpr *address,const scrType *Type,scrNodeExpr *expr);
	void Emit();
	void Emit2() { }
protected:
	scrNodeExpr *Address, *Expr;
	const scrType *Type;
};


class scrNodeConstruct: public scrNodeStmt {
public:
	scrNodeConstruct(const scrType *type,scrNodeExpr *address);
	void Emit();
	void Emit2() { }
private:
	const scrType *Type;
	scrNodeExpr *Address;
};



class scrNodeScope: public scrNodeStmt {
public:
	scrNodeScope(int lineNumber,scrNodeStmt *body);
	void Emit();
	void Emit2();
protected:
	scrNodeStmt *Body;
};


class scrNodeIf: public scrNodeStmt {
public:
	scrNodeIf(int lineNumber,scrNodeExpr *expr,scrNodeStmt *ifTrue,scrNodeStmt *ifFalse);
	void Emit();
	void Emit2();
	bool Returns() const {
		return IfFalse? IfTrue && IfTrue->Returns() && IfFalse->Returns() : false;
	}
protected:
	scrNodeExpr *Expr;
	scrNodeStmt *IfTrue, *IfFalse;
};


class scrNodeWhile: public scrNodeStmt {
public:
	scrNodeWhile(int lineNumber,scrNodeExpr *expr,scrNodeStmt *stmt);
	void Emit();
	void Emit2();
protected:
	scrNodeExpr *Expr;
	scrNodeStmt *Body;
};


class scrNodeFor: public scrNodeStmt {
public:
	scrNodeFor(int lineNumber,scrNodeExpr *address,scrNodeExpr *initialValue,scrNodeExpr *finalValue,int step,scrNodeStmt *body);
	void Emit();
	void Emit2();
protected:
	scrNodeExpr *Address;
	scrNodeExpr *InitialValue;
	scrNodeExpr *FinalValue;
	int Step;
	scrNodeStmt *Body;
};


class scrNodeRepeat: public scrNodeStmt {
public:
	scrNodeRepeat(int lineNumber,scrNodeExpr *count,scrNodeExpr *address,scrNodeStmt *stmt);
	void Emit();
	void Emit2();
protected:
	scrNodeExpr *Count;
	scrNodeExpr *Address;
	scrNodeStmt *Body;
};


class scrNodeReturn: public scrNodeStmt {
public:
	scrNodeReturn(scrNodeExpr *expr,int paramCount,const scrType *returnType);
	void Emit();
	void Emit2() { }
	bool Returns() const { return true; }
private:
	scrNodeExpr *Expr;
	int ParamCount;
	const scrType *ReturnType;
};

class scrNodeDefault;

class scrNodeSwitch: public scrNodeStmt {
public:
	static const int c_DefaultCase = 0x7FFFFFFF;

	scrNodeSwitch(scrNodeExpr *expr);
	~scrNodeSwitch() { }
    bool HasCase(const s32 value) const { return Cases.Find(value) != -1; }
	void Emit();
	void Emit2();
	void AddBreak();
	void AddCase(s32 value);
	void EmitCase(s32 value);
	void EmitDefault();
	void SetBody(scrNodeStmt *body) { Body = body; }
    scrNodeStmt* GetBody() { return Body; }
	bool HaveLabels() const { return Default!=0 || Cases.GetCount()!=0 || HasDefault; }
	void SetHadBreak(bool flag) { HadBreak = flag; }
	bool GetHadBreak() const { return HadBreak; }
	void SetHasDefault();
	void SetHasAtLeastOneBreak();
	void SetLastLabel(scrNodeDefault* label) { LastLabel = label; }
	scrNodeDefault* GetLastLabel() const { return LastLabel; }
	bool Returns() const;

	scrNodeExpr* GetExpr() const { return Expr; }
private:
	scrNodeExpr *Expr;
	scrNodeStmt *Body;
	atFixedArray<int,192> Breaks;
	scrNodeDefault *LastLabel;
	atFixedArray<s32,192> Cases;
	int Default;
	int TableStart;
	bool HadBreak;
	bool HasDefault;
	bool HasAtLeastOneBreak;
};

class scrNodeDefault: public scrNodeStmt {
public:
	scrNodeDefault(scrNodeType type,scrNodeSwitch *parent);
	void Emit();
	void Emit2() { }
	void SetWasLastLabel() { LastLabel = true; }
protected:
	scrNodeSwitch *Parent;
	bool FirstLabel, LastLabel;
};

class scrNodeCase: public scrNodeDefault {
public:
	scrNodeCase(scrNodeSwitch *parent,int value,const char *enumString);
	void Emit();
private:
	int Value;
	const char *EnumString;
};

class scrNodeBreak: public scrNodeStmt {
public:
	scrNodeBreak(scrNodeSwitch *parent);
	void Emit();
	void Emit2() { }
private:
	scrNodeSwitch *Parent;
};

class scrNodeBreakLoop: public scrNodeStmt {
public:
	scrNodeBreakLoop(int ln);
	void Emit();
	void Emit2() { }
};

class scrNodeReLoop: public scrNodeStmt {
public:
	scrNodeReLoop(int ln);
	void Emit();
	void Emit2() { }
};

class scrNodeLabel: public scrNodeStmt {
public:
	scrNodeLabel(bool placed);
	void Emit();
	int GetAddress() const { return Address; }
	void Emit2() { Assert(Address); }
	bool IsPlaced() const { return Placed; }
	void SetPlaced() { Placed = true; }
private:
	int Address;
	bool Placed;
};

class scrNodeGoto: public scrNodeStmt {
public:
	scrNodeGoto(scrNodeLabel *target);
	void Emit();
	void Emit2();
private:
	scrNodeLabel *Target;
	int Address;
};

class scrNodeThrow: public scrNodeStmt {
public:
	scrNodeThrow(scrNodeExpr *expr);
	void Emit();
	void Emit2() { }
private:
	scrNodeExpr *Expr;
};

class scrNodeDecl: public scrNodeStmt {
public:
	scrNodeDecl(const scrType *type,const char *name,bool hasAssignment);
	void Emit();
	void Emit2() { }
private:
	const scrType *Type;
	HeapString Name;
	bool HasAssignment;
};

extern scrTypeList g_VARARGS, g_VARARGS1, g_VARARGS2, g_VARARGS3;

}	// namespace rage


#endif
