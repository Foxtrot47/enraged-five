USING "rage_builtins.sch"	// Necessary for TIMERA/B, SETTIMERA/B.

FUNC STRING Foo()
	RETURN "Foo"
ENDFUNC

FUNC INT BAR(INT i)
	RETURN i + 2
ENDFUNC

GLOBALS TRUE

GLOBALS
	//STRING GlobalStr0 = "GlobalStr0"
	//STRING GlobalStr1 = Foo()
	INT GlobalInt0 = 0
	INT GlobalInt1 = BAR(11)
	INT GlobalInt2 = 22 + BAR(22)
	INT GlobalInt3 = GlobalInt0 + GlobalInt1 + GlobalInt2
ENDGLOBALS

STRING StaticStr0 = "StaticStr0"
STRING StaticStr1 = Foo()
INT StaticInt0 = 0
INT StaticInt1 = BAR(1)
INT StaticInt2 = 2 + BAR(2)
INT StaticInt3 = StaticInt0 + StaticInt1 + StaticInt2

SCRIPT
	//PRINTSTRING(GlobalStr0) PRINTNL()
	//PRINTSTRING(GlobalStr1) PRINTNL()
	PRINTINT(GlobalInt0) PRINTNL()
	PRINTINT(GlobalInt1) PRINTNL()
	PRINTINT(GlobalInt2) PRINTNL()
	PRINTINT(GlobalInt3) PRINTNL()

	PRINTSTRING(StaticStr0) PRINTNL()
	PRINTSTRING(StaticStr1) PRINTNL()
	PRINTINT(StaticInt0) PRINTNL()
	PRINTINT(StaticInt1) PRINTNL()
	PRINTINT(StaticInt2) PRINTNL()
	PRINTINT(StaticInt3) PRINTNL()
ENDSCRIPT
