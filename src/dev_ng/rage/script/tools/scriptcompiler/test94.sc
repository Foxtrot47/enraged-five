SCRIPT
	BOOL A
	INT B, C

	INT D = A? B : C
	INT E = A? 1 : 2
	FLOAT F = A? 1.0 : 2
	FLOAT G = A? 1 : 2.0
ENDSCRIPT
