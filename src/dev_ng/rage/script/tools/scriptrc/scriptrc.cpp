// 
// scriptcompiler/scriptrc.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 
#include "system/main.h"
#include "system/param.h"
#include "system/platform.h"

#include "script/program.h"

#if !__RESOURCECOMPILER
#error "Only Rsc platform will will work properly."
#endif

// How to build the x64 version of this -- 
//	1. Load rage/script/tools/scriptrc/scriptrc_2012.sln
//	2. Pick Beta x64 config
//	3. Temporarily replace the contents of rage/base/src/forceinclude/win64_beta.h with that of win64_rscbeta.h
//	4. Skip the compilation of service_pc.cpp
//		The easiest way to do that is by changing line 14 of service_pc.cpp from 
//		#if RSG_PC && !__TOOL
//		to 
//		#if RSG_PC && !__TOOL && 0
//	5. Change line 50 of diagerrorcodes.cpp from 
//		sysLanguage language = g_SysService.GetSystemLanguage();
//		to 
//		sysLanguage language = LANGUAGE_ENGLISH;
//	6. In thread.cpp around line 4385, skip compilation of this line 
//			extern "C" void ApiCheckPlugin_StartNativeHash();
//		and the entire scrThread::RegisterBuiltinCommands() function by wrapping them in #if 0
//	7. In program.cpp, change __RAGE_SCRIPT_COMPILER to 1. This disables some debug output.
//  8. Do a rebuild all
//	9. Run rage/script/tools/scriptrc/install_north.bat
//	10. Revert the changes you made in steps 3 to 7

namespace rage 
{
	XPARAM(aeskey);
	extern int g_rscDebug;
}

int Main()
{
	using namespace rage;

	/* for (int i=0; i<sysParam::GetArgCount(); i++)
		printf("%s ",sysParam::GetArg(i));
	printf("\n"); */

	const char *input = sysParam::GetArg(1);
	const char *output = sysParam::GetArg(2);

	const char *outputExt = strrchr(output, '.');
	AssertMsg(outputExt,"Output file must have extension");
	g_sysPlatform = outputExt[1];
	g_ByteSwap = sysGetByteSwap(g_sysPlatform);

	scrProgramId pid = scrProgram::Load(input);
	if (!pid) {
		Errorf("Unable to load script object '%s'",input);
		return 1;
	}
#if __ASSERT
	scrProgram *prog = scrProgram::GetProgram(pid);
	Assert(prog);
#endif
	g_rscDebug = 0;

	if (!PARAM_aeskey.Get() && output[1]==':')
		PARAM_aeskey.Set(output);

	for (int pass=0; pass<=pgRscBuilder::LAST_PASS; pass++) {
		pgRscBuilder::BeginBuild(pass==pgRscBuilder::LAST_PASS?true:false,16*1024*1024);
		scrProgram *pt = rage_new scrProgram(*scrProgram::GetProgram(pid));
		if (pass == pgRscBuilder::LAST_PASS) {
			pgRscBuilder::ReadyBuild(pt);
			datTypeStruct Structure;
			pt->DeclareStruct(Structure);
			pgRscBuilder::SaveBuild(output,"#sc",scrProgram::RORC_VERSION,NULL,true);
		}
		pgRscBuilder::EndBuild(pt);
	}
	scrProgram::GetProgram(pid)->Release();
	return 0;
}