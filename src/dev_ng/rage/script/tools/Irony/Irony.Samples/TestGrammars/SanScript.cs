﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Irony.Parsing;

namespace Irony.Samples.SanScript
{
    [Language("SanScript", "1.0", "SanScript scripting language")]
    class SanScriptGrammar : Grammar
    {
        public SanScriptGrammar()
            : base(false)   //  Passing false means it's not a case sensitive language
        {
            //  Strings, numbers and identifiers
            StringLiteral StringLiteral = new StringLiteral("StringLiteral", "\"");
            NumberLiteral NumberLiteral = new NumberLiteral("NumberLiteral");
            IdentifierTerminal identifier = new IdentifierTerminal("Identifier");

            #region Comments
            //  Comments
            CommentTerminal SingleLineComment = new CommentTerminal("SingleLineComment", "//", "\r", "\n", "\u2085", "\u2028", "\u2029");
            CommentTerminal MultiLineComment = new CommentTerminal("MultiLineComment", "/*", "*/");
            //  These are not used in grammar rules but they are valid terminals, so they go on 
            //  the NonGrammarTerminals list
            NonGrammarTerminals.Add(SingleLineComment);
            NonGrammarTerminals.Add(MultiLineComment);
            #endregion

            #region Symbols
            //  Symbols (done as KeyTerm's rather than ToTerm() so they don't 
            //  show up in the keyword intellisense list)
            KeyTerm comma = new KeyTerm(",", "comma");
            comma.SetFlag(TermFlags.IsPunctuation, true);
            KeyTerm equals = new KeyTerm("=", "equals");
            KeyTerm greater_than = new KeyTerm(">", "greater_than");
            KeyTerm greater_than_equal = new KeyTerm(">=", "greater_than_equal");
            KeyTerm less_than = new KeyTerm("<", "less_than");
            KeyTerm less_than_equal = new KeyTerm("<=", "less_than_equal");
            KeyTerm not_equal_to = new KeyTerm("<>", "not_equal_to");
            KeyTerm not_equal_to2 = new KeyTerm("!=", "not_equal_to2");
            KeyTerm not = new KeyTerm("!", "not");
            KeyTerm plus = new KeyTerm("+", "plus");
            KeyTerm plus_plus = new KeyTerm("++", "plus_plus");
            KeyTerm plus_equals = new KeyTerm("+=", "plus_equals");
            KeyTerm minus = new KeyTerm("-", "minus");
            KeyTerm minus_minus = new KeyTerm("--", "minus_minus");
            KeyTerm minus_equals = new KeyTerm("-=", "minus_equals");
            KeyTerm multiply = new KeyTerm("*", "multiply");
            KeyTerm multiply_equals = new KeyTerm("*=", "multiply_equals");
            KeyTerm divide = new KeyTerm("/", "divide");
            KeyTerm divide_equals = new KeyTerm("/=", "divide_equals");
            KeyTerm percent = new KeyTerm("%", "percent");
            KeyTerm dot = new KeyTerm(".", "dot");
            KeyTerm amp = new KeyTerm("&", "amp");
            KeyTerm left_par = new KeyTerm("(", "left_par");
            left_par.SetFlag(TermFlags.IsPunctuation, true);
            KeyTerm right_par = new KeyTerm(")", "right_par");
            right_par.SetFlag(TermFlags.IsPunctuation, true);
            KeyTerm left_vector_bracket = new KeyTerm("<<", "left_vector_bracket");
            left_vector_bracket.SetFlag(TermFlags.IsPunctuation, true);
            KeyTerm right_vector_bracket = new KeyTerm(">>", "right_vector_bracket");
            right_vector_bracket.SetFlag(TermFlags.IsPunctuation, true);
            KeyTerm left_square_bracket = new KeyTerm("[", "left_square_bracket");
            left_square_bracket.SetFlag(TermFlags.IsPunctuation, true);
            KeyTerm right_square_bracket = new KeyTerm("]", "right_square_bracket");
            right_square_bracket.SetFlag(TermFlags.IsPunctuation, true);
            KeyTerm colon = new KeyTerm(":", "colon");
            KeyTerm or = new KeyTerm("|", "or");
            KeyTerm or_equals = new KeyTerm("|=", "or_equals");
            #endregion

            #region Pre Processor
            //  Preprocessor keywords are not marked as keywords by default because they 
            //  do not start with a letter.  We have to set the flag for IsKeyword ourselves.
            KeyTerm hashIf = ToTerm("#IF");
            hashIf.SetFlag(TermFlags.IsKeyword, true);
            KeyTerm hashEndIf = ToTerm("#ENDIF");
            hashEndIf.SetFlag(TermFlags.IsKeyword, true);
            #endregion

            //  I'm defining certain non terminals here, as they may be used in other sections, like between a FOR and ENDFOR or for initializer values
            var script_block = new NonTerminal("script_block");
            var function_call = new NonTerminal("function_call", "function call");
            var vector_value = new NonTerminal("vector_value");
            var variable = new NonTerminal("variable");

            //  The using statement is basically the keyword USING then a string for the include file name
            var using_statement = new NonTerminal("using_statement", "using statement");
            using_statement.Rule = ToTerm("USING") + StringLiteral;

            //  The built in types are for things like int, float and bool that are defined by the language
            var builtin_type = new NonTerminal("builtin_type", "built-in type");
            builtin_type.Rule = ToTerm("INT") | ToTerm("FLOAT") | ToTerm("STRING") | ToTerm("BOOL") |
                                ToTerm("TEXT_LABEL") | ToTerm("TEXT_LABEL_3") | ToTerm("TEXT_LABEL_7") |
                                ToTerm("TEXT_LABEL_15") | ToTerm("TEXT_LABEL_23") |
                                ToTerm("TEXT_LABEL_31") | ToTerm("TEXT_LABEL_63") |
                                ToTerm("VECTOR") | ToTerm("POOLINDEX") | ToTerm("STRINGHASH") |
                                ToTerm("ENUM_TO_INT");
            //  Built in const types
            var builtin_const_type = new NonTerminal("builtin_const_type", "built-in const type");
            builtin_const_type.Rule = ToTerm("CONST_INT") | ToTerm("CONST_FLOAT") |
                                      ToTerm("TWEAK_INT") | ToTerm("TWEAK_FLOAT");
            //  Built in or user type
            var builtin_or_user_type = new NonTerminal("builtin_or_user_type");
            builtin_or_user_type.Rule = builtin_type | identifier;
            //  Base type option
            var base_type_opt = new NonTerminal("base_type_opt", "base type");
            base_type_opt.Rule = Empty | colon + builtin_or_user_type;
            //  Native type, for types not contained in the language
            var native_type = new NonTerminal("native_type");
            native_type.Rule = ToTerm("NATIVE") + identifier + base_type_opt;

            //  Literal values to be used as initial values
            var hash_literal = new NonTerminal("hash_literal");
            hash_literal.Rule = ToTerm("HASH") + left_par + StringLiteral + right_par;
            var literal = new NonTerminal("literal");
            literal.Rule = NumberLiteral | StringLiteral | ToTerm("TRUE") | ToTerm("FALSE") | hash_literal | ToTerm("NULL");

            //  NOT modifier
            var not_modifier = new NonTerminal("not_modifier");
            not_modifier.Rule = ToTerm("NOT") | not;

            //  Unary operators
            var unary_op = new NonTerminal("unary_op");
            unary_op.Rule = minus | amp;

            //  Unary value
            var unary_value = new NonTerminal("unary_value");
            unary_value.Rule = unary_op + NumberLiteral | unary_op + variable | unary_op + function_call;

            //  Different operations used for expressions (have to prefer shift on all of these separately, can't do it on combination for some reason)
            var logic_op = new NonTerminal("logic_op");
            logic_op.Rule = PreferShiftHere() + ToTerm("AND") | PreferShiftHere() + ToTerm("OR");
            var relation_op = new NonTerminal("relation_op");
            relation_op.Rule = PreferShiftHere() + equals | PreferShiftHere() + less_than | PreferShiftHere() + greater_than |
                                PreferShiftHere() + less_than_equal | PreferShiftHere() + greater_than_equal |
                                PreferShiftHere() + not_equal_to | PreferShiftHere() + not_equal_to2;
            var arithmetic_op = new NonTerminal("arithmetic_op");
            arithmetic_op.Rule = PreferShiftHere() + plus | PreferShiftHere() + minus | PreferShiftHere() + multiply |
                                    PreferShiftHere() + divide | PreferShiftHere() + or | PreferShiftHere() + percent |
                                    PreferShiftHere() + amp;

            var expression_op = new NonTerminal("expression_op");
            expression_op.Rule = logic_op | relation_op | arithmetic_op;

            //  Increment (++) or decrement (--)
            var incdec = new NonTerminal("incdec");
            incdec.Rule = plus_plus + variable | variable + plus_plus |
                            minus_minus + variable | variable + minus_minus;

            //  Permitted variable initializer values
            var variable_initializer_value = new NonTerminal("variable_initializer_value");
            variable_initializer_value.Rule = literal | function_call | variable | vector_value | unary_value;

            //  Expression
            var expression = new NonTerminal("expression");
            expression.Rule = variable_initializer_value | expression + expression_op + expression | left_par + expression + right_par |
                                not_modifier + expression;

            //  Permitted array initializer values, can also be empty
            var array_initializer_value = new NonTerminal("array_initializer_value");
            array_initializer_value.Rule = Empty | expression;

            //  Array brackets
            var array_brackets = new NonTerminal("array_brackets");
            array_brackets.Rule = left_square_bracket + array_initializer_value + right_square_bracket;
            //  Dimensional array
            var dimensional_array = new NonTerminal("dimensional_array");
            dimensional_array.Rule = MakePlusRule(dimensional_array, null, array_brackets);
            //  Array option to turn a normal variable into an array
            var array_opt = new NonTerminal("array_opt");
            array_opt.Rule = Empty | dimensional_array;

            //  Variable
            variable.Rule = PreferShiftHere() + identifier + array_opt | variable + PreferShiftHere() + dot + variable;

            //  Vector value
            vector_value.Rule = left_vector_bracket + expression + comma + expression + comma + expression + right_vector_bracket;

            //  Assignment symbols
            var assignment_symbol = new NonTerminal("assignment_symbol");
            assignment_symbol.Rule = equals | plus_equals | minus_equals | multiply_equals | divide_equals | or_equals;

            //  Initialized variable
            var assigned_variable = new NonTerminal("assigned_variable");
            assigned_variable.Rule = variable + assignment_symbol + expression;

            //  Variable with optional assignment
            var assigned_variable_opt = new NonTerminal("assigned_variable_opt");
            assigned_variable_opt.Rule = variable | assigned_variable;

            //  Const variable
            var const_variable = new NonTerminal("const_variable");
            const_variable.Rule = identifier + expression;

            //  Comma separated variable name list
            var cs_variables = new NonTerminal("cs_variables");
            cs_variables.Rule = MakePlusRule(cs_variables, comma, assigned_variable_opt);

            //  Multiple variable declaration with the same type (such as 'INT myVar, myVar2, myVar3')
            var multiple_variable_declaration = new NonTerminal("multiple_variable_declaration");
            multiple_variable_declaration.Rule = builtin_or_user_type + assigned_variable_opt + comma + cs_variables;

            //  A variable declaration is just a type and a variable name, or a type and a comma separated list of variable names
            var variable_declaration = new NonTerminal("variable_declaration");
            variable_declaration.Rule = builtin_or_user_type + assigned_variable_opt | builtin_or_user_type + amp + assigned_variable_opt | builtin_const_type + const_variable;

            //  Return statement
            var return_value_opt = new NonTerminal("return_value_opt");
            return_value_opt.Rule = Empty | expression;
            var return_statement = new NonTerminal("return_statement");
            return_statement.Rule = PreferShiftHere() + ToTerm("RETURN") + return_value_opt | PreferShiftHere() + ToTerm("EXIT");

            #region Sections
            //  GLOBALS/ENDGLOBALS section
            var globals_section = new NonTerminal("globals_section");
            globals_section.Rule = ToTerm("GLOBALS") + script_block + ToTerm("ENDGLOBALS");
            //  SCRIPT/ENDSCRIPT section
            var script_section = new NonTerminal("script_section");
            script_section.Rule = ToTerm("SCRIPT") + script_block + ToTerm("ENDSCRIPT");
            #endregion

            #region Conditional statements
            //  Preprocessor stuff
            var preproc_hash_if = new NonTerminal("preproc_hash_if");
            preproc_hash_if.Rule = hashIf + expression;
            var preproc_hash_endif = new NonTerminal("preproc_hash_endif");
            preproc_hash_endif.Rule = hashEndIf;
            var preproc_statement = new NonTerminal("preproc_statement");
            preproc_statement.Rule = preproc_hash_if | preproc_hash_endif;

            //  Break statement
            var break_statement = new NonTerminal("break_statement");
            break_statement.Rule = ToTerm("BREAK");
            var break_statement_opt = new NonTerminal("break_statement_opt");
            break_statement_opt.Rule = Empty | break_statement;

            //  If statement
            var if_statement_elif = new NonTerminal("if_statement_elif");
            if_statement_elif.Rule = ToTerm("ELIF") + expression + script_block + break_statement_opt;
            var if_statement_elif_opt = new NonTerminal("if_statement_elif_opt");
            if_statement_elif_opt.Rule = MakeStarRule(if_statement_elif_opt, null, if_statement_elif);

            var if_statement_else_opt = new NonTerminal("if_statement_else_opt");
            if_statement_else_opt.Rule = Empty | ToTerm("ELSE") + script_block + break_statement_opt;

            var if_statement = new NonTerminal("if_statement");
            if_statement.Rule = ToTerm("IF") + expression +
                script_block + break_statement_opt +
                if_statement_elif_opt +
                if_statement_else_opt +
                ToTerm("ENDIF");

            //  Switch/case statements
            //  Optional case fall through statement
            var case_fallthrough_opt = new NonTerminal("case_fallthrough_opt");
            case_fallthrough_opt.Rule = Empty | ToTerm("FALLTHRU");
            //  Optional case body
            var case_body_opt = new NonTerminal("case_body_opt");
            case_body_opt.Rule = script_block + break_statement_opt;
            //  Default statement
            var default_statement = new NonTerminal("default_statement");
            default_statement.Rule = ToTerm("DEFAULT") + script_block + break_statement;
            //  Case statement
            var case_statement = new NonTerminal("case_statement");
            case_statement.Rule = ToTerm("CASE") + expression + case_fallthrough_opt + case_body_opt |
                                    default_statement;
            //  List of case statements
            var case_statement_list = new NonTerminal("case_statement_list");
            case_statement_list.Rule = MakePlusRule(case_statement_list, null, case_statement);
            //  Switch statement
            var switch_statement = new NonTerminal("switch_statement");
            switch_statement.Rule = ToTerm("SWITCH") + expression + case_statement_list + ToTerm("ENDSWITCH");
            #endregion

            #region Functions
            //  ***** HACK: as preprocessor stuff not working properly in Irony yet *****
            //  Variable declaration hack
            var variable_declaration_hack = new NonTerminal("variable_declaration_hack");
            variable_declaration_hack.Rule = variable_declaration | variable_declaration + preproc_statement;
            //  *************************************************************************
            //  Comma separated variable declarations
            var cs_variable_declarations = new NonTerminal("cs_variable_declarations");
            cs_variable_declarations.Rule = MakePlusRule(cs_variable_declarations, comma, variable_declaration_hack/*variable_declaration*/);
            //  Optional comma separated variable declarations
            var cs_variable_declarations_opt = new NonTerminal("cs_variable_declarations_opt");
            cs_variable_declarations_opt.Rule = Empty | cs_variable_declarations | ToTerm("VARARGS");
            //  Optional fixed parameter list
            var fixed_parameter_list_opt = new NonTerminal("fixed_parameter_list_opt", "fixed parameter list");
            fixed_parameter_list_opt.Rule = Empty | left_par + cs_variable_declarations_opt + right_par;
            //  ***** HACK: as preprocessor stuff not working properly in Irony yet *****
            //  Parameter
            var parameter = new NonTerminal("parameter");
            parameter.Rule = expression | expression + preproc_statement;
            //  *************************************************************************
            //  Parameter list
            var parameter_list = new NonTerminal("parameter_list");
            parameter_list.Rule = MakePlusRule(parameter_list, comma, parameter/*expression*/);
            //  Optional parameter list
            var parameter_list_opt = new NonTerminal("parameter_list_opt");
            parameter_list_opt.Rule = Empty | parameter_list;

            //  Function declaration
            var function_declaration = new NonTerminal("function_declaration", "function declaration");
            function_declaration.Rule = ToTerm("FUNC") + builtin_or_user_type + identifier + fixed_parameter_list_opt +
                                        script_block +
                                        ToTerm("ENDFUNC");

            //  Debug only option
            var debug_only_opt = new NonTerminal("debug_only_opt");
            debug_only_opt.Rule = Empty | ToTerm("DEBUGONLY");

            //  Native function declaration
            var native_function_declaration = new NonTerminal("native_function_declaration");
            native_function_declaration.Rule = ToTerm("NATIVE") + debug_only_opt + ToTerm("FUNC") + builtin_or_user_type + identifier + fixed_parameter_list_opt;

            //  Proc declaration
            var proc_declaration = new NonTerminal("proc_declaration");
            proc_declaration.Rule = debug_only_opt + ToTerm("PROC") + identifier + fixed_parameter_list_opt +
                                    script_block +
                                    ToTerm("ENDPROC");

            var native_proc_declaration = new NonTerminal("native_proc_declaration");
            native_proc_declaration.Rule = ToTerm("NATIVE") + debug_only_opt + ToTerm("PROC") + identifier + fixed_parameter_list_opt;

            //  Built in functions which are part of the language
            var builtin_function = new NonTerminal("builtin_function");
            builtin_function.Rule = PreferShiftHere() + ToTerm("ENUM_TO_INT") | PreferShiftHere() + ToTerm("INT_TO_ENUM") |
                                    PreferShiftHere() + ToTerm("DEFINED") | PreferShiftHere() + ToTerm("SIZE_OF") |
                                    PreferShiftHere() + ToTerm("NATIVE_TO_INT") | PreferShiftHere() + ToTerm("COUNT_OF");

            //  Function call
            function_call.Rule = identifier + left_par + parameter_list_opt + right_par | builtin_function + left_par + parameter_list_opt + right_par;

            #endregion

            #region Structs and Enums
            //  ***** HACK: Another hack because preprocessor not working in Irony yet *****
            var struct_member_opt = new NonTerminal("struct_member_opt");
            struct_member_opt.Rule = variable_declaration | preproc_statement | multiple_variable_declaration;
            //  ****************************************************************************
            //  Struct member declarations
            var struct_member_declarations = new NonTerminal("struct_member_declarations");
            struct_member_declarations.Rule = MakePlusRule(struct_member_declarations, null, struct_member_opt/*variable_declaration*/);
            //  Struct type
            var struct_declaration = new NonTerminal("struct_declaration");
            struct_declaration.Rule = ToTerm("STRUCT") + identifier + struct_member_declarations + ToTerm("ENDSTRUCT");

            //  Enum member declaration
            var enum_member_declaration = new NonTerminal("enum_member_declaration");
            enum_member_declaration.Rule = assigned_variable_opt | assigned_variable_opt + comma | preproc_statement;
            //  Enum member declarations
            var enum_member_declarations = new NonTerminal("enum_member_declarations");
            enum_member_declarations.Rule = MakePlusRule(enum_member_declarations, null, enum_member_declaration);
            //  Enum type
            var enum_type = new NonTerminal("enum_type");
            enum_type.Rule = ToTerm("ENUM") | ToTerm("STRICT_ENUM") | ToTerm("HASH_ENUM");
            //  Enum declaration
            var enum_declaration = new NonTerminal("enum_declaration");
            enum_declaration.Rule = enum_type + identifier + enum_member_declarations + ToTerm("ENDENUM");
            //  Forward declared enum declaration
            var forward_dec_enum_declaration = new NonTerminal("forward_dec_enum_declaration");
            forward_dec_enum_declaration.Rule = ToTerm("FORWARD") + enum_type + identifier;
            #endregion

            #region Loop statements
            //  Step option for loops
            var step_opt = new NonTerminal("step_opt");
            step_opt.Rule = Empty | ToTerm("STEP") + expression;

            //  Repeat loop
            var repeat_loop = new NonTerminal("repeat_loop", "repeat loop");
            repeat_loop.Rule = ToTerm("REPEAT") + expression + variable +
                                script_block +
                                ToTerm("ENDREPEAT");
            //  While loop
            var while_loop = new NonTerminal("while_loop");
            while_loop.Rule = ToTerm("WHILE") + expression + script_block + ToTerm("ENDWHILE");

            //  For loop
            var for_loop = new NonTerminal("for_loop", "for loop");
            for_loop.Rule = ToTerm("FOR") + assigned_variable + ToTerm("TO") + expression + step_opt +
                script_block +
                ToTerm("ENDFOR");
            #endregion

            //  A script statement can be a single line or a group of lines containing different script commands
            var script_statement = new NonTerminal("script_statement");
            script_statement.Rule = using_statement |
                                    variable_declaration |
                                    assigned_variable |
                                    incdec | // Not sure about this one, should be in the assigned variable rule, but gives conflicts?
                                    multiple_variable_declaration |
                                    native_type |
                                    globals_section |
                                    script_section |
                                    function_declaration |
                                    native_function_declaration |
                                    proc_declaration |
                                    native_proc_declaration |
                                    function_call |
                                    return_statement |
                                    struct_declaration |
                                    enum_declaration |
                                    forward_dec_enum_declaration |
                                    repeat_loop |
                                    preproc_statement |
                                    switch_statement |
                                    if_statement |
                                    while_loop |
                                    for_loop;
            script_statement.ErrorRule = SyntaxError + script_statement;

            //  A script block is made up of zero or more script statements
            script_block.Rule = MakeStarRule(script_block, null, script_statement);

            //  Set grammar root as the script block
            this.Root = script_block;
        }
    }
}
