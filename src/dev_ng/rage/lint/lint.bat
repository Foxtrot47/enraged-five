@echo off

REM ________________________________________________________________________________________________
REM DESCRIPTION
REM	runs lint on the specified module on the specified configuration.
REM
REM USAGE
REM	lint.bat <module name> <Developer Studio config name> <.lnt for configuration and platform>
REM
REM EXAMPLE
REM	lint.bat atl win32_beta Win32 Beta
REM 
REM NOTES
REM 		* must be run in the module's directory
REM 		* lint-nt.exe must be in your %PATH%!
REM ________________________________________________________________________________________________

setlocal
set include=
set LINT_PATH=../../../lint
lint-nt.exe +ffn +fcp -ffb -b -i"%LINT_PATH%/gimpel" -i"%LINT_PATH%" -i..\ +d"CFG=%1 - %3 %4 %5"   %2.lnt %1.vcproj >%1.lnt
lint-nt.exe +ffn +fcp -ffb -b -i"%LINT_PATH%/gimpel" -i"%LINT_PATH%" -i..\ +libclass(angle) +libdir(../src/*) -u %2.lnt env-vc7.lnt %1.lnt
rem del %1.lnt
REM if NOT %errorlevel%==0 echo "found errors" > ..\linterrors.txt