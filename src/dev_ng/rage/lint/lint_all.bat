@echo off

REM ________________________________________________________________________________________________
REM DESCRIPTION
REM	runs lint on all modules in ..\src\*.  Will run lint on directories that have 
REM   makefile.bat only.
REM
REM USAGE
REM	lint_all.bat
REM
REM EXAMPLE
REM	lint_all.bat
REM 
REM NOTES
REM 		* must be run in the lint directory
REM 		* lint-nt.exe must be in your %PATH%!
REM ________________________________________________________________________________________________

setlocal
set LINT_PATH=..\..\..\lint
del lint_errors.txt
chdir ..\engine\src\
for /D %%d in (*) do (
	chdir %%d
	if EXIST makefile.bat (
		echo running lint on module '%%d' ...
		call %LINT_PATH%\lint.bat %%d xenon_beta Xenon Beta >> %LINT_PATH%\lint_errors.txt
	)
	chdir ..
)