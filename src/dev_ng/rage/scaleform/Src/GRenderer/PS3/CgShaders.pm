package Shaders::PS3;

@ISA = qw/Shaders/;

sub new { bless {} }

sub DefaultOutMode { 'f'; }

sub OpenFiles
{
	my $f = "$_[0]->{dir}/$_[0]->{name}Shaders.cpp";
	open (Shaders::OUTC,">$f") || die "$f: $!";
	$f = "$_[0]->{dir}/$_[0]->{name}Shaders.h";
	open (Shaders::OUTH,">$f") || die "$f: $!";
}
sub CloseFiles
{
	close(Shaders::OUTH);
	close(Shaders::OUTC);
}

sub Param
{
  $_ = $_[1];

  my ($vt,$vn,$vs) = ($_->{type},$_->{name},$_->{sem});
  Shaders::ParamD3D($vt);
  if (exists $_->{dim}) {
    $vn .= "[$_->{dim}]";
  }

  if ($vs eq '') {
    if ($vn =~ /^tc([0-9]*)/) { $vs = 'TEXCOORD'.(0+$1); }
    elsif ($vn eq 'color') { $vs = 'COLOR0'; }
    elsif ($vn eq 'factor') { $vs = 'COLOR1'; }
    elsif ($vn eq 'pos') { $vs = 'POSITION'; }
  }
  else { $vs = uc($vs); }

  if ($_[0]->{stage} eq 'v' && $_->{vary} eq 'varying') { $vt = "out $vt"; }
  elsif ($_[0]->{stage} eq 'f' && $_->{vary} eq 'fragout') { $vt = "out $vt"; }
  my $s = ($_->{vary} eq 'uniform') ? $_->{vary}.' ' : '';
  $s .= "$vt $vn";
  $s.= " : $vs" if $vs;

  $s .= (($_->{vary} eq 'uniform') ? ';' : ',') if $_[2];

  return $s, (($_->{vary} eq 'uniform') ? 1 : 0);
}

sub ParamCategory
{
  if ($_[1]->{vary} eq 'uniform') {
    return 1;
  }
  else {
    return 0;
  }
}

sub BuiltinParams
{
  return ($Shaders::stage eq 'f' ? ({vary => 'fragout', type => 'float4', name => 'fcolor', sem => 'COLOR'}) : ());
}

sub Block
{
  my ($this, $stage, $argsr, $b) = @_;
  my %args = %{$argsr};

  $b = Shaders::BlockD3D($this, $stage, $argsr, $b);
  $b =~ s/(\Wmul) *\($Shaders::subexpr,$Shaders::subexpr\)/\1(\3,\2)/g;

  return $b;
}

sub FileExt
{
  return 'cg';
}

sub WriteHeaders
{
    Shaders::WriteHeaders();
	my %stages = qw/v Vertex f Frag/;
	my @init = ();
	foreach my $stage ('v','f') {
		foreach my $g (keys %Shaders::groups) {
			my $Group = $Shaders::groups{$g};
			my @out = @{$Group->{$stage.'out'}};
			foreach (@out) {
				if (ref($_)) {
					my $fn = Shaders::GetFName($stage, @{$_->{blocks}});
					my $sym = "_binary_${fn}_${stage}po_start";
					push @init, [$stages{$stage}, $_->{enum}, $sym];
					print Shaders::OUTH "extern int $sym;\n";
				}
			}
		}
	}
	
	print Shaders::OUTH "\nstatic void $_[0]->{name}InitShaders(GRenderer$_[0]->{name}Impl* pRenderer);\n";
	print Shaders::OUTC "static void $_[0]->{name}InitShaders(GRenderer$_[0]->{name}Impl* pRenderer)\n{\n";
	foreach (@init) {
		print Shaders::OUTC "    pRenderer->Init$$_[0]Shader($$_[1], &$$_[2]);\n";
	}
	print Shaders::OUTC "}\n";
}

sub Output
{
  my @base = @{$Shaders::Outputs{$_[1]}};
  my $dir = $_[0]->{dir};
  my $name = $_[0]->{name};

  return [$base[0], $base[1],
	  sub { &{$base[2]}(@_);
		my $f = "../../Projects/PS3/Shaders.mk0";
		open (OUTDEP,">$f") || die "$f :$!"; },
	  sub { &{$base[3]}(@_);

		my @objs = ();
		foreach my $stage ('v','f') {
			foreach my $g (keys %Shaders::groups) {
				my $Group = $Shaders::groups{$g};
				my @out = @{$Group->{$stage.'out'}};
				foreach (@out) {
					if (ref($_)) {
						my $fn = Shaders::GetFName($stage, @{$_->{blocks}});
						push @objs, "\$(OBJDIRP)/$fn.${stage}po";
					}
				}
			}
		}
	  
		print OUTDEP "GCM_SHADERS += \\\n";
		for (my $i = 0; $i <= $#objs; $i++) {
		  print OUTDEP '    '.$objs[$i]. (($i <= $#objs-1) ? " \\\n" : "\n\n");
		}
		close(OUTDEP); }];
}

new();
