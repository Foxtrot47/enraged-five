/**********************************************************************

Filename    :   hlsl.cpp
Content     :   Direct3D9 hlsl shaders
Created     :   
Authors     :   Michael Antonov & Andrew Reisse

Copyright   :   (c) 2001-2006 Scaleform Corp. All Rights Reserved.

Notes       :   

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#define GRENDERER_VSHADER_PROFILE "vs_2_0"
#define GRENDERER_PSHADER_PROFILE "ps_2_0"

#define XFORMPOS \
   "  opos = mul(pos, mvp);\n"

static const char* pStripVShaderText =
"float4x4 mvp : register(c0);\n"
"float4 texgen[2] : register(c4);\n"
"void main(float4 pos      : POSITION,\n"
"          out float4 opos : POSITION,\n"
"          out float2 otc0 : TEXCOORD0)\n"
"{\n"
XFORMPOS
"  otc0.x = dot(pos, texgen[0]);\n"
"  otc0.y = dot(pos, texgen[1]);\n"
"}\n"
;

static const char* pGlyphVShaderText =
"float4x4 mvp : register(c0);\n"
"void main(float4 pos      : POSITION,\n"
"          float2 tc0      : TEXCOORD0,\n"
"          float4 color    : COLOR0,\n"
"          out float4 opos : POSITION,\n"
"          out float2 otc0 : TEXCOORD0,\n"
"          out float4 ocolor : COLOR0)\n"
"{\n"
XFORMPOS
"  otc0 = tc0;\n"
"  ocolor = color.bgra;\n"
"}\n"
;

// Edge AA VShaders (pass along color channels)
static const char* pStripVShaderXY16iC32Text =
"float4x4 mvp : register(c0);\n"
"float4 texgen[2] : register(c4);\n"
"void main(float4 pos        : POSITION,\n"
"          float4 color      : COLOR,\n"
"          out float4 opos   : POSITION,\n"
"          out float4 ocolor : COLOR,\n"
"          out float2 otc0   : TEXCOORD0)\n"
"{\n"
XFORMPOS
"  otc0.x = dot(pos, texgen[0]);\n"
"  otc0.y = dot(pos, texgen[1]);\n"
"  ocolor = color;\n"
"}\n"
;
static const char* pStripVShaderXY16iCF32Text =
"float4x4 mvp : register(c0);\n"
"float4 texgen[2] : register(c4);\n"
"void main(float4 pos        : POSITION,\n"
"          float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          out float4 opos   : POSITION,\n"
"          out float4 ocolor : COLOR,\n"
"          out float4 ofactor: COLOR1,\n"
"          out float2 otc0   : TEXCOORD0)\n"
"{\n"
XFORMPOS
"  otc0.x = dot(pos, texgen[0]);\n"
"  otc0.y = dot(pos, texgen[1]);\n"
"  ocolor = color;\n"
"  ofactor = factor;\n"
"}\n"
;
// Two-texture shader version
static const char* pStripVShaderXY16iCF32_T2Text =
"float4x4 mvp : register(c0);\n"
"float4 texgen[4] : register(c4);\n"
"void main(float4 pos        : POSITION,\n"
"          float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          out float4 opos   : POSITION,\n"
"          out float4 ocolor : COLOR,\n"
"          out float4 ofactor: COLOR1,\n"
"          out float2 otc0   : TEXCOORD0,\n"
"          out float2 otc1   : TEXCOORD1)\n"
"{\n"
XFORMPOS
"  otc0.x = dot(pos, texgen[0]);\n"
"  otc0.y = dot(pos, texgen[1]);\n"
"  otc1.x = dot(pos, texgen[2]);\n"
"  otc1.y = dot(pos, texgen[3]);\n"
"  ocolor = color;\n"
"  ofactor = factor;\n"
"}\n"
;

static const char* pGlyphVShaderSzcText =
"float4x4 mvp : register(c0);\n"
"void main(float4 pos      : POSITION,\n"
"          float2 tc0      : TEXCOORD0,\n"
"          float4 color    : COLOR0,\n"
"          out float4 opos : POSITION,\n"
"          out float2 otc0 : TEXCOORD0,\n"
"          out float4 ocolor : COLOR0)\n"
"{\n"
XFORMPOS
"  otc0 = tc0;\n"
"  ocolor = color;\n"
"}\n"
;

static const char* pStripVShaderXY16iC32SzcText =
"float4x4 mvp : register(c0);\n"
"float4 texgen[2] : register(c4);\n"
"void main(float4 pos        : POSITION,\n"
"          float4 color      : COLOR,\n"
"          out float4 opos   : POSITION,\n"
"          out float4 ocolor : COLOR,\n"
"          out float2 otc0   : TEXCOORD0)\n"
"{\n"
XFORMPOS
"  otc0.x = dot(pos, texgen[0]);\n"
"  otc0.y = dot(pos, texgen[1]);\n"
"  ocolor = color.bgra;\n"
"}\n"
;
static const char* pStripVShaderXY16iCF32SzcText =
"float4x4 mvp : register(c0);\n"
"float4 texgen[2] : register(c4);\n"
"void main(float4 pos        : POSITION,\n"
"          float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          out float4 opos   : POSITION,\n"
"          out float4 ocolor : COLOR,\n"
"          out float4 ofactor: COLOR1,\n"
"          out float2 otc0   : TEXCOORD0)\n"
"{\n"
XFORMPOS
"  otc0.x = dot(pos, texgen[0]);\n"
"  otc0.y = dot(pos, texgen[1]);\n"
"  ocolor = color.bgra;\n"
"  ofactor = factor.bgra;\n"
"}\n"
;
// Two-texture shader version
static const char* pStripVShaderXY16iCF32Szc_T2Text =
"float4x4 mvp : register(c0);\n"
"float4 texgen[4] : register(c4);\n"
"void main(float4 pos        : POSITION,\n"
"          float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          out float4 opos   : POSITION,\n"
"          out float4 ocolor : COLOR,\n"
"          out float4 ofactor: COLOR1,\n"
"          out float2 otc0   : TEXCOORD0,\n"
"          out float2 otc1   : TEXCOORD1)\n"
"{\n"
XFORMPOS
"  otc0.x = dot(pos, texgen[0]);\n"
"  otc0.y = dot(pos, texgen[1]);\n"
"  otc1.x = dot(pos, texgen[2]);\n"
"  otc1.y = dot(pos, texgen[3]);\n"
"  ocolor = color.bgra;\n"
"  ofactor = factor.bgra;\n"
"}\n"
;

#if 0
///////////////////////////////////////////
//////////////// DPOPT

// Batching
static const char* pStripVShaderXY16iIText =
"float4 matrices[84] : register(c0);\n"
"void main(float4 pos      : POSITION,\n"
"          short2 instance : BLENDINDICES,\n"
"          out float4 opos : POSITION,\n"
"          out float2 otc0 : TEXCOORD0)\n"
"{\n"
"  opos = pos;\n"
"  opos.x = dot(pos, matrices[instance.x*4+0]);\n"
"  opos.y = dot(pos, matrices[instance.x*4+1]);\n"
"  otc0.x = dot(pos, matrices[instance.x*4+2]);\n"
"  otc0.y = dot(pos, matrices[instance.x*4+3]);\n"
"}\n"
;

static const char* pGlyphIVShaderText =
"float4 matrices[84] : register(c0);\n"
"void main(float4 pos      : POSITION,\n"
"          float2 tc0      : TEXCOORD0,\n"
"          float4 color    : COLOR0,\n"
"          short2 instance : BLENDINDICES,\n"
"          out float4 opos : POSITION,\n"
"          out float2 otc0 : TEXCOORD0,\n"
"          out float4 ocolor : COLOR0)\n"
"{\n"
"  opos = pos;\n"
"  opos.x = dot(pos, matrices[instance.x*2+0]);\n"
"  opos.y = dot(pos, matrices[instance.x*2+1]);\n"
"  otc0 = tc0;\n"
"  ocolor = color.bgra;\n"
"}\n"
;

static const char* pStripVShaderXY16iC32IText =
"float4 matrices[84] : register(c0);\n"
"void main(float4 pos        : POSITION,\n"
"          float4 color      : COLOR,\n"
"          short2 instance   : BLENDINDICES,\n"
"          out float4 opos   : POSITION,\n"
"          out float4 ocolor : COLOR,\n"
"          out float2 otc0   : TEXCOORD0)\n"
"{\n"
"  opos = pos;\n"
"  opos.x = dot(pos, matrices[instance.x*4+0]);\n"
"  opos.y = dot(pos, matrices[instance.x*4+1]);\n"
"  otc0.x = dot(pos, matrices[instance.x*4+2]);\n"
"  otc0.y = dot(pos, matrices[instance.x*4+3]);\n"
"  ocolor = color;\n"
"}\n"
;

static const char* pStripVShaderXY16iCF32IText =
"float4 matrices[84] : register(c0);\n"
"void main(float4 pos        : POSITION,\n"
"          float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          short2 instance   : BLENDINDICES,\n"
"          out float4 opos   : POSITION,\n"
"          out float4 ocolor : COLOR,\n"
"          out float4 ofactor: COLOR1,\n"
"          out float2 otc0   : TEXCOORD0)\n"
"{\n"
"  opos = pos;\n"
"  opos.x = dot(pos, matrices[instance.x*4+0]);\n"
"  opos.y = dot(pos, matrices[instance.x*4+1]);\n"
"  otc0.x = dot(pos, matrices[instance.x*4+2]);\n"
"  otc0.y = dot(pos, matrices[instance.x*4+3]);\n"
"  ocolor = color;\n"
"  ofactor = factor;\n"
"}\n"
;

static const char* pStripVShaderXY16iCF32_T2IText =
"float4 matrices[84] : register(c0);\n"
"void main(float4 pos        : POSITION,\n"
"          float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          short2 instance   : BLENDINDICES,\n"
"          out float4 opos   : POSITION,\n"
"          out float4 ocolor : COLOR,\n"
"          out float4 ofactor: COLOR1,\n"
"          out float2 otc0   : TEXCOORD0,\n"
"          out float2 otc1   : TEXCOORD1)\n"
"{\n"
"  opos = pos;\n"
"  opos.x = dot(pos, matrices[instance.x*6+0]);\n"
"  opos.y = dot(pos, matrices[instance.x*6+1]);\n"
"  otc0.x = dot(pos, matrices[instance.x*6+2]);\n"
"  otc0.y = dot(pos, matrices[instance.x*6+3]);\n"
"  otc1.x = dot(pos, matrices[instance.x*6+4]);\n"
"  otc1.y = dot(pos, matrices[instance.x*6+5]);\n"
"  ocolor = color;\n"
"  ofactor = factor;\n"
"}\n"
;

static const char* pGlyphIVShaderSzcText =
"float4 matrices[84] : register(c0);\n"
"void main(float4 pos      : POSITION,\n"
"          float2 tc0      : TEXCOORD0,\n"
"          float4 color    : COLOR0,\n"
"          short2 instance : BLENDINDICES,\n"
"          out float4 opos : POSITION,\n"
"          out float2 otc0 : TEXCOORD0,\n"
"          out float4 ocolor : COLOR0)\n"
"{\n"
"  opos = pos;\n"
"  opos.x = dot(pos, matrices[instance.x*2+0]);\n"
"  opos.y = dot(pos, matrices[instance.x*2+1]);\n"
"  otc0 = tc0;\n"
"  ocolor = color;\n"
"}\n"
;

static const char* pStripVShaderXY16iC32ISzcText =
"float4 matrices[84] : register(c0);\n"
"void main(float4 pos        : POSITION,\n"
"          float4 color      : COLOR,\n"
"          short2 instance   : BLENDINDICES,\n"
"          out float4 opos   : POSITION,\n"
"          out float4 ocolor : COLOR,\n"
"          out float2 otc0   : TEXCOORD0)\n"
"{\n"
"  opos = pos;\n"
"  opos.x = dot(pos, matrices[instance.x*4+0]);\n"
"  opos.y = dot(pos, matrices[instance.x*4+1]);\n"
"  otc0.x = dot(pos, matrices[instance.x*4+2]);\n"
"  otc0.y = dot(pos, matrices[instance.x*4+3]);\n"
"  ocolor = color.bgra;\n"
"}\n"
;

static const char* pStripVShaderXY16iCF32ISzcText =
"float4 matrices[84] : register(c0);\n"
"void main(float4 pos        : POSITION,\n"
"          float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          short2 instance   : BLENDINDICES,\n"
"          out float4 opos   : POSITION,\n"
"          out float4 ocolor : COLOR,\n"
"          out float4 ofactor: COLOR1,\n"
"          out float2 otc0   : TEXCOORD0)\n"
"{\n"
"  opos = pos;\n"
"  opos.x = dot(pos, matrices[instance.x*4+0]);\n"
"  opos.y = dot(pos, matrices[instance.x*4+1]);\n"
"  otc0.x = dot(pos, matrices[instance.x*4+2]);\n"
"  otc0.y = dot(pos, matrices[instance.x*4+3]);\n"
"  ocolor = color.bgra;\n"
"  ofactor = factor.bgra;\n"
"}\n"
;

static const char* pStripVShaderXY16iCF32Szc_T2IText =
"float4 matrices[84] : register(c0);\n"
"void main(float4 pos        : POSITION,\n"
"          float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          short2 instance   : BLENDINDICES,\n"
"          out float4 opos   : POSITION,\n"
"          out float4 ocolor : COLOR,\n"
"          out float4 ofactor: COLOR1,\n"
"          out float2 otc0   : TEXCOORD0,\n"
"          out float2 otc1   : TEXCOORD1)\n"
"{\n"
"  opos = pos;\n"
"  opos.x = dot(pos, matrices[instance.x*6+0]);\n"
"  opos.y = dot(pos, matrices[instance.x*6+1]);\n"
"  otc0.x = dot(pos, matrices[instance.x*6+2]);\n"
"  otc0.y = dot(pos, matrices[instance.x*6+3]);\n"
"  otc1.x = dot(pos, matrices[instance.x*6+4]);\n"
"  otc1.y = dot(pos, matrices[instance.x*6+5]);\n"
"  ocolor = color.bgra;\n"
"  ofactor = factor.bgra;\n"
"}\n"
;
#endif	// DPOPT

#define GRENDERER_SHADER_VERSION    0x0101

static const char* pSource_PS_SolidColor =
"float4 color : register(c0);\n"
"void main(out float4 ocolor : COLOR)\n"
"{ ocolor = color;\n}\n";

static const char* pSource_PS_CxformTexture =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"sampler tex0 : register(s0);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 color = tex2D(tex0, tc0);\n"
"  ocolor = color * cxmul + cxadd;\n"
"}\n"
;

static const char* pSource_PS_CxformTextureMultiply =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"sampler tex0 : register(s0);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 color = tex2D(tex0, tc0);\n"
"  color = color * cxmul + cxadd;\n"
"  ocolor = lerp (1, color, color.a);\n"
"}\n"
;

static const char* pSource_PS_TextTextureAlpha =
"sampler tex0 : register(s0);\n"
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          float4 color      : COLOR,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  color = color * cxmul + cxadd;\n"
"  color.a = color.a * tex2D(tex0, tc0).a;\n"
"  ocolor = color;\n"
"}\n"
;

static const char* pSource_PS_TextTextureColor =
"sampler tex0 : register(s0);\n"
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 color = tex2D(tex0, tc0);\n"
"  color = color * cxmul + cxadd;\n"
"  ocolor = color;\n"
"}\n"
;

static const char* pSource_PS_TextTextureColorMultiply =
"sampler tex0 : register(s0);\n"
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 color = tex2D(tex0, tc0);\n"
"  color = color * cxmul + cxadd;\n"
"  ocolor = lerp (1, color, color.a);\n"
"}\n"
;

static const char* pSource_PS_TextTextureYUV =
"sampler tex_y : register(s0);\n"
"sampler tex_u : register(s1);\n"
"sampler tex_v : register(s2);\n"
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float  Y = (float)tex2D(tex_y, tc0).a;\n"
"  float  U = (float)tex2D(tex_u, tc0).a - 128.f/255.f;\n"
"  float  V = (float)tex2D(tex_v, tc0).a - 128.f/255.f;\n"
"  float3 color = (float3(Y, Y, Y) - float3(16.f/255.f, 16.f/255.f, 16.f/255.f)) * 1.164;\n"
"  color.r +=  V * 1.596f;\n"
"  color.g += -U * 0.392f - V * 0.813f;\n"
"  color.b +=  U * 2.017f;\n"
"  ocolor = cxmul * float4(color,1) + cxadd;\n"
"}\n";

static const char* pSource_PS_TextTextureYUVMultiply =
"sampler tex_y : register(s0);\n"
"sampler tex_u : register(s1);\n"
"sampler tex_v : register(s2);\n"
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float  Y = (float)tex2D(tex_y, tc0).a;\n"
"  float  U = (float)tex2D(tex_u, tc0).a - 128.f/255.f;\n"
"  float  V = (float)tex2D(tex_v, tc0).a - 128.f/255.f;\n"
"  float4 color = float4( float3((float3(Y, Y, Y) - float3(16.f/255.f, 16.f/255.f, 16.f/255.f)) * 1.164), 1);\n"
"  color.r +=  V * 1.596f;\n"
"  color.g += -U * 0.392f - V * 0.813f;\n"
"  color.b +=  U * 2.017f;\n"
"  color = cxmul * color + cxadd;\n"
"  ocolor = lerp (1, color, color.a);\n"
"}\n";

static const char* pSource_PS_TextTextureYUVA =
"sampler tex_y : register(s0);\n"
"sampler tex_u : register(s1);\n"
"sampler tex_v : register(s2);\n"
"sampler tex_a : register(s3);\n"
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float  Y = (float)tex2D(tex_y, tc0).a;\n"
"  float  U = (float)tex2D(tex_u, tc0).a - 128.f/255.f;\n"
"  float  V = (float)tex2D(tex_v, tc0).a - 128.f/255.f;\n"
"  float3 color = (float3(Y, Y, Y) - float3(16.f/255.f, 16.f/255.f, 16.f/255.f)) * 1.164;\n"
"  color.r +=  V * 1.596f;\n"
"  color.g += -U * 0.392f - V * 0.813f;\n"
"  color.b +=  U * 2.017f;\n"
"  ocolor = cxmul * float4(color,tex2D(tex_a, tc0).a) + cxadd;\n"
"}\n";

static const char* pSource_PS_TextTextureYUVAMultiply =
"sampler tex_y : register(s0);\n"
"sampler tex_u : register(s1);\n"
"sampler tex_v : register(s2);\n"
"sampler tex_a : register(s3);\n"
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float  Y = (float)tex2D(tex_y, tc0).a;\n"
"  float  U = (float)tex2D(tex_u, tc0).a - 128.f/255.f;\n"
"  float  V = (float)tex2D(tex_v, tc0).a - 128.f/255.f;\n"
"  float4 color = float4( float3((float3(Y, Y, Y) - float3(16.f/255.f, 16.f/255.f, 16.f/255.f)) * 1.164), tex2D(tex_a, tc0).a);\n"
"  color.r +=  V * 1.596f;\n"
"  color.g += -U * 0.392f - V * 0.813f;\n"
"  color.b +=  U * 2.017f;\n"
"  color = cxmul * color + cxadd;\n"
"  ocolor = lerp (1, color, color.a);\n"
"}\n";

static const char* pSource_PS_CxformGauraud =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  color = color * cxmul + cxadd;\n"
"  color.a = color.a * factor.a;\n"
"  ocolor = color;\n"
"}\n"
;

// Same, for Multiply blend version.
static const char* pSource_PS_CxformGauraudMultiply =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  color = color * cxmul + cxadd;\n"
"  color.a = color.a * factor.a;\n"
"  ocolor = lerp (1, color, color.a);\n"
"}\n"
;

// The difference from above is that we don't have separate EdgeAA alpha channel;
// it is instead pre-multiplied into the color alpha (VertexXY16iC32). So we
// don't do an EdgeAA multiply in the end.
static const char* pSource_PS_CxformGauraudNoAddAlpha =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float4 color      : COLOR,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  ocolor = color * cxmul + cxadd;\n"
"}\n"
;

static const char* pSource_PS_CxformGauraudMultiplyNoAddAlpha =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float4 color      : COLOR,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  color = color * cxmul + cxadd;\n"
"  ocolor = lerp (1, color, color.a);\n"
"}\n"
;

static const char* pSource_PS_CxformGauraudTexture =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"sampler tex0 : register(s0);\n"
"void main(float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  color = lerp (color, tex2D(tex0, tc0), factor.b);\n"
"  color = color * cxmul + cxadd;\n"
"  color.a = color.a * factor.a;\n"
"  ocolor = color;\n"
"}\n"
;

static const char* pSource_PS_CxformGauraudMultiplyTexture =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"sampler tex0 : register(s0);\n"
"void main(float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  color = lerp (color, tex2D(tex0, tc0), factor.b);\n"
"  color = color * cxmul + cxadd;\n"
"  color.a = color.a * factor.a;\n"
"  ocolor = lerp (1, color, color.a);\n"
"}\n"
;

static const char* pSource_PS_Cxform2Texture =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"sampler tex0 : register(s0);\n"
"sampler tex1 : register(s1);\n"
"void main(float4 factor     : COLOR1,\n"
"          float2 tc0        : TEXCOORD0,\n"
"          float2 tc1        : TEXCOORD1,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 color = lerp (tex2D(tex1, tc1), tex2D(tex0, tc0), factor.b);\n"
"  ocolor = color * cxmul + cxadd;\n"
"}\n"
;

static const char* pSource_PS_CxformMultiply2Texture =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"sampler tex0 : register(s0);\n"
"sampler tex1 : register(s1);\n"
"void main(float4 factor     : COLOR1,\n"
"          float2 tc0        : TEXCOORD0,\n"
"          float2 tc1        : TEXCOORD1,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 color = lerp (tex2D(tex1, tc1), tex2D(tex0, tc0), factor.b);\n"
"  color = color * cxmul + cxadd;\n"
"  ocolor = lerp (1, color, color.a);\n"
"}\n"
;


static const char* pSource_PS_AcSolidColor =
"float4 color : register(c0);\n"
"void main(out float4 ocolor : COLOR)\n"
"{\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n"
;

static const char* pSource_PS_AcCxformTexture =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"sampler tex0 : register(s0);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 color = tex2D(tex0, tc0);\n"
"  color = color * cxmul + cxadd;\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n"
;

static const char* pSource_PS_AcCxformTextureMultiply =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"sampler tex0 : register(s0);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 color = tex2D(tex0, tc0);\n"
"  color = color * cxmul + cxadd;\n"
"  color = lerp (1, color, color.a);\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n"
;

static const char* pSource_PS_AcTextTextureAlpha =
"sampler tex0 : register(s0);\n"
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          float4 color      : COLOR,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  color = color * cxmul + cxadd;\n"
"  color.a = color.a * tex2D(tex0, tc0).a;\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n"
;

static const char* pSource_PS_AcTextTextureColor =
"sampler tex0 : register(s0);\n"
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 color = cxmul * tex2D(tex0, tc0) + cxadd;\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n"
;

static const char* pSource_PS_AcTextTextureColorMultiply =
"sampler tex0 : register(s0);\n"
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 color = cxmul * tex2D(tex0, tc0) + cxadd;\n"
"  color = lerp (1, color, color.a);\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n"
;

static const char* pSource_PS_AcTextTextureYUV =
"sampler tex_y : register(s0);\n"
"sampler tex_u : register(s1);\n"
"sampler tex_v : register(s2);\n"
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float  Y = (float)tex2D(tex_y, tc0).a;\n"
"  float  U = (float)tex2D(tex_u, tc0).a - 128.f/255.f;\n"
"  float  V = (float)tex2D(tex_v, tc0).a - 128.f/255.f;\n"
"  float4 color = float4( float3((float3(Y, Y, Y) - float3(16.f/255.f, 16.f/255.f, 16.f/255.f)) * 1.164), 1);\n"
"  color.r +=  V * 1.596f;\n"
"  color.g += -U * 0.392f - V * 0.813f;\n"
"  color.b +=  U * 2.017f;\n"
"  color = cxmul * color + cxadd;\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n";

static const char* pSource_PS_AcTextTextureYUVMultiply =
"sampler tex_y : register(s0);\n"
"sampler tex_u : register(s1);\n"
"sampler tex_v : register(s2);\n"
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float  Y = (float)tex2D(tex_y, tc0).a;\n"
"  float  U = (float)tex2D(tex_u, tc0).a - 128.f/255.f;\n"
"  float  V = (float)tex2D(tex_v, tc0).a - 128.f/255.f;\n"
"  float4 color = float4( float3((float3(Y, Y, Y) - float3(16.f/255.f, 16.f/255.f, 16.f/255.f)) * 1.164), 1);\n"
"  color.r +=  V * 1.596f;\n"
"  color.g += -U * 0.392f - V * 0.813f;\n"
"  color.b +=  U * 2.017f;\n"
"  color = cxmul * color + cxadd;\n"
"  color = lerp (1, color, color.a);\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n";

static const char* pSource_PS_AcTextTextureYUVA =
"sampler tex_y : register(s0);\n"
"sampler tex_u : register(s1);\n"
"sampler tex_v : register(s2);\n"
"sampler tex_a : register(s3);\n"
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float  Y = (float)tex2D(tex_y, tc0).a;\n"
"  float  U = (float)tex2D(tex_u, tc0).a - 128.f/255.f;\n"
"  float  V = (float)tex2D(tex_v, tc0).a - 128.f/255.f;\n"
"  float4 color = float4( float3((float3(Y, Y, Y) - float3(16.f/255.f, 16.f/255.f, 16.f/255.f)) * 1.164), tex2D(tex_a, tc0).a);\n"
"  color.r +=  V * 1.596f;\n"
"  color.g += -U * 0.392f - V * 0.813f;\n"
"  color.b +=  U * 2.017f;\n"
"  color = cxmul * color + cxadd;\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n";

static const char* pSource_PS_AcTextTextureYUVAMultiply =
"sampler tex_y : register(s0);\n"
"sampler tex_u : register(s1);\n"
"sampler tex_v : register(s2);\n"
"sampler tex_a : register(s3);\n"
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float  Y = (float)tex2D(tex_y, tc0).a;\n"
"  float  U = (float)tex2D(tex_u, tc0).a - 128.f/255.f;\n"
"  float  V = (float)tex2D(tex_v, tc0).a - 128.f/255.f;\n"
"  float4 color = float4( float3((float3(Y, Y, Y) - float3(16.f/255.f, 16.f/255.f, 16.f/255.f)) * 1.164), tex2D(tex_a, tc0).a);\n"
"  color.r +=  V * 1.596f;\n"
"  color.g += -U * 0.392f - V * 0.813f;\n"
"  color.b +=  U * 2.017f;\n"
"  color = cxmul * color + cxadd;\n"
"  color = lerp (1, color, color.a);\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n";

static const char* pSource_PS_AcCxformGauraud =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  color = color * cxmul + cxadd;\n"
"  color.a = color.a * factor.a;\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n"
;

// Same, for Multiply blend version.
static const char* pSource_PS_AcCxformGauraudMultiply =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  color = color * cxmul + cxadd;\n"
"  color.a = color.a * factor.a;\n"
"  color = lerp (1, color, color.a);\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n"
;

static const char* pSource_PS_AcCxformGauraudNoAddAlpha =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float4 color      : COLOR,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  color = color * cxmul + cxadd;\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n"
;

static const char* pSource_PS_AcCxformGauraudMultiplyNoAddAlpha =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"void main(float4 color      : COLOR,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  color = color * cxmul + cxadd;\n"
"  color = lerp (1, color, color.a);\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n"
;

static const char* pSource_PS_AcCxformGauraudTexture =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"sampler tex0 : register(s0);\n"
"void main(float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  color = lerp (color, tex2D(tex0, tc0), factor.b);\n"
"  color = color * cxmul + cxadd;\n"
"  color.a = color.a * factor.a;\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n"
;

static const char* pSource_PS_AcCxformGauraudMultiplyTexture =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"sampler tex0 : register(s0);\n"
"void main(float4 color      : COLOR,\n"
"          float4 factor     : COLOR1,\n"
"          float2 tc0        : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  color = lerp (color, tex2D(tex0, tc0), factor.b);\n"
"  color = color * cxmul + cxadd;\n"
"  color.a = color.a * factor.a;\n"
"  color = lerp (1, color, color.a);\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n"
;

static const char* pSource_PS_AcCxform2Texture =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"sampler tex0 : register(s0);\n"
"sampler tex1 : register(s1);\n"
"void main(float4 factor     : COLOR1,\n"
"          float2 tc0        : TEXCOORD0,\n"
"          float2 tc1        : TEXCOORD1,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 color = lerp (tex2D(tex1, tc1), tex2D(tex0, tc0), factor.b);\n"
"  color = color * cxmul + cxadd;\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n"
;

static const char* pSource_PS_AcCxformMultiply2Texture =
"float4 cxmul : register(c2);\n"
"float4 cxadd : register(c3);\n"
"sampler tex0 : register(s0);\n"
"sampler tex1 : register(s1);\n"
"void main(float4 factor     : COLOR1,\n"
"          float2 tc0        : TEXCOORD0,\n"
"          float2 tc1        : TEXCOORD1,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 color = lerp (tex2D(tex1, tc1), tex2D(tex0, tc0), factor.b);\n"
"  color = color * cxmul + cxadd;\n"
"  color = lerp (1, color, color.a);\n"
"  ocolor.rgb = color * color.a;\n"
"  ocolor.a = color.a;\n"
"}\n"
;

static const char* pSource_PS_CmatrixTexture =
"float4x4 cxmul : register(c0);\n"
"float4   cxadd : register(c4);\n"
"sampler  tex0  : register(s0);\n"
"void main(float2 vtc0       : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 c = tex2D(tex0, vtc0);\n"
"  ocolor = mul(c,cxmul) + cxadd * (c.a + cxadd.a);\n"
"}\n"
;

static const char* pSource_PS_CmatrixTextureMultiply =
"float4x4 cxmul : register(c0);\n"
"float4   cxadd : register(c4);\n"
"sampler  tex0  : register(s0);\n"
"void main(float2 vtc0       : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 c = tex2D(tex0, vtc0);\n"
"  ocolor = mul(c,cxmul) + cxadd * (c.a + cxadd.a);\n"
"  ocolor = lerp (1, ocolor, ocolor.a);\n"
"}\n"
;

static const char* pSource_PS_AcCmatrixTexture =
"float4x4 cxmul : register(c0);\n"
"float4   cxadd : register(c4);\n"
"sampler  tex0  : register(s0);\n"
"void main(float2 vtc0       : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 c = tex2D(tex0, vtc0);\n"
"  ocolor = mul(c,cxmul) + cxadd * (c.a + cxadd.a);\n"
"  ocolor.rgb = ocolor * ocolor.a;\n"
"}\n"
;

static const char* pSource_PS_AcCmatrixTextureMultiply =
"float4x4 cxmul : register(c0);\n"
"float4   cxadd : register(c4);\n"
"sampler  tex0  : register(s0);\n"
"void main(float2 vtc0       : TEXCOORD0,\n"
"          out float4 ocolor : COLOR)\n"
"{\n"
"  float4 c = tex2D(tex0, vtc0);\n"
"  ocolor = mul(c,cxmul) + cxadd * (c.a + cxadd.a);\n"
"  ocolor = lerp (1, ocolor, ocolor.a);\n"
"  ocolor.rgb = ocolor * ocolor.a;\n"
"}\n"
;
