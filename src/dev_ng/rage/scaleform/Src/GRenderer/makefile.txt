Project GRenderer
IncludePath ../../Include;../GRenderer;../GKernel;$(RAGE_DIR)/base/src/jpeg;$(RAGE_DIR)/base/src/zlib
Files 
{
    GCompoundShape.h
    GEdgeAA.h
    GRasterizer.h
    GRectPacker.h
    GResizeImage.h
    GStroker.h
    GStrokerAA.h
    GTessellator.h

    GCompoundShape.cpp
    GEdgeAA.cpp
    GImageInfo.cpp
    GRasterizer.cpp
    GRectPacker.cpp
    GRenderer.cpp
    GResizeImage.cpp
    GStroker.cpp
    GStrokerAA.cpp
    GTessellator.cpp
}