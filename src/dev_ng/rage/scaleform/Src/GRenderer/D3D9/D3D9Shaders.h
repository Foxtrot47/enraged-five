enum VertexShaderType2
{
    VS2_None = 0,
    VS2_start_shadows,
    VS2_VVatc = 1,
    VS2_end_shadows = 1,
    VS2_start_blurs,
    VS2_end_blurs = 1,
    VS2_start_cmatrix,
    VS2_end_cmatrix = 1,
    VS2_Count,

};

static const void* VShaderBinaries2[VS2_Count] = {
    NULL,
    pBinary_VVatc,
};

enum VertexShader2Uniform
{
    VSU_mvp,
    VSU_Count
};

static const int VUniforms_VVatc[] = {0};

static const int* VShaderUniforms[VS2_Count] = {
    NULL,
    VUniforms_VVatc,
};

enum FragShaderType2
{
    FS2_None = 0,
    FS2_start_shadows,
    FS2_FBox2InnerShadow = 1,
    FS2_FBox2InnerShadowHighlight,
    FS2_FBox2InnerShadowMul,
    FS2_FBox2InnerShadowMulHighlight,
    FS2_FBox2InnerShadowKnockout,
    FS2_FBox2InnerShadowHighlightKnockout,
    FS2_FBox2InnerShadowMulKnockout,
    FS2_FBox2InnerShadowMulHighlightKnockout,
    FS2_FBox2Shadow,
    FS2_FBox2ShadowHighlight,
    FS2_FBox2ShadowMul,
    FS2_FBox2ShadowMulHighlight,
    FS2_FBox2ShadowKnockout,
    FS2_FBox2ShadowHighlightKnockout,
    FS2_FBox2ShadowMulKnockout,
    FS2_FBox2ShadowMulHighlightKnockout,
    FS2_FBox2Shadowonly,
    FS2_FBox2ShadowonlyHighlight,
    FS2_FBox2ShadowonlyMul,
    FS2_FBox2ShadowonlyMulHighlight,
    FS2_end_shadows = 20,
    FS2_start_blurs,
    FS2_FBox1Blur = 21,
    FS2_FBox2Blur,
    FS2_FBox1BlurMul,
    FS2_FBox2BlurMul,
    FS2_end_blurs = 24,
    FS2_start_cmatrix,
    FS2_FCMatrix = 25,
    FS2_FCMatrixMul,
    FS2_end_cmatrix = 26,
    FS2_Count,

    FS2_shadows_Highlight            = 0x00000001,
    FS2_shadows_Mul                  = 0x00000002,
    FS2_shadows_Knockout             = 0x00000004,
    FS2_blurs_Box2                 = 0x00000001,
    FS2_blurs_Mul                  = 0x00000002,
    FS2_cmatrix_Mul                  = 0x00000001,
};

static const void* FShaderBinaries2[FS2_Count] = {
    NULL,
    pBinary_FBox2InnerShadow,
    pBinary_FBox2InnerShadowHighlight,
    pBinary_FBox2InnerShadowMul,
    pBinary_FBox2InnerShadowMulHighlight,
    pBinary_FBox2InnerShadowKnockout,
    pBinary_FBox2InnerShadowHighlightKnockout,
    pBinary_FBox2InnerShadowMulKnockout,
    pBinary_FBox2InnerShadowMulHighlightKnockout,
    pBinary_FBox2Shadow,
    pBinary_FBox2ShadowHighlight,
    pBinary_FBox2ShadowMul,
    pBinary_FBox2ShadowMulHighlight,
    pBinary_FBox2ShadowKnockout,
    pBinary_FBox2ShadowHighlightKnockout,
    pBinary_FBox2ShadowMulKnockout,
    pBinary_FBox2ShadowMulHighlightKnockout,
    pBinary_FBox2Shadowonly,
    pBinary_FBox2ShadowonlyHighlight,
    pBinary_FBox2ShadowonlyMul,
    pBinary_FBox2ShadowonlyMulHighlight,
    pBinary_FBox1Blur,
    pBinary_FBox2Blur,
    pBinary_FBox1BlurMul,
    pBinary_FBox2BlurMul,
    pBinary_FCMatrix,
    pBinary_FCMatrixMul,
};

enum FragShader2Uniform
{
    FSU_cxadd,
    FSU_cxmul,
    FSU_fsize,
    FSU_offset,
    FSU_scolor,
    FSU_scolor2,
    FSU_srctex,
    FSU_srctexscale,
    FSU_tex,
    FSU_texscale,
    FSU_Count
};

static const int FUniforms_FBox2InnerShadow[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2InnerShadowHighlight[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2InnerShadowMul[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2InnerShadowMulHighlight[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2InnerShadowKnockout[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2InnerShadowHighlightKnockout[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2InnerShadowMulKnockout[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2InnerShadowMulHighlightKnockout[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2Shadow[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2ShadowHighlight[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2ShadowMul[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2ShadowMulHighlight[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2ShadowKnockout[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2ShadowHighlightKnockout[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2ShadowMulKnockout[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2ShadowMulHighlightKnockout[] = {0, 1, 2, 3, 4, 5, 0, 6, 1, 7};
static const int FUniforms_FBox2Shadowonly[] = {0, 1, 2, 3, 4, -1, -1, -1, 0, 5};
static const int FUniforms_FBox2ShadowonlyHighlight[] = {0, 1, 2, 3, 4, -1, -1, -1, 0, 5};
static const int FUniforms_FBox2ShadowonlyMul[] = {0, 1, 2, 3, 4, -1, -1, -1, 0, 5};
static const int FUniforms_FBox2ShadowonlyMulHighlight[] = {0, 1, 2, 3, 4, -1, -1, -1, 0, 5};
static const int FUniforms_FBox1Blur[] = {0, 1, 2, -1, -1, -1, -1, -1, 0, 3};
static const int FUniforms_FBox2Blur[] = {0, 1, 2, -1, -1, -1, -1, -1, 0, 3};
static const int FUniforms_FBox1BlurMul[] = {0, 1, 2, -1, -1, -1, -1, -1, 0, 3};
static const int FUniforms_FBox2BlurMul[] = {0, 1, 2, -1, -1, -1, -1, -1, 0, 3};
static const int FUniforms_FCMatrix[] = {0, 1, -1, -1, -1, -1, -1, -1, 0, -1};
static const int FUniforms_FCMatrixMul[] = {0, 1, -1, -1, -1, -1, -1, -1, 0, -1};

static const int* FShaderUniforms[FS2_Count] = {
    NULL,
    FUniforms_FBox2InnerShadow,
    FUniforms_FBox2InnerShadowHighlight,
    FUniforms_FBox2InnerShadowMul,
    FUniforms_FBox2InnerShadowMulHighlight,
    FUniforms_FBox2InnerShadowKnockout,
    FUniforms_FBox2InnerShadowHighlightKnockout,
    FUniforms_FBox2InnerShadowMulKnockout,
    FUniforms_FBox2InnerShadowMulHighlightKnockout,
    FUniforms_FBox2Shadow,
    FUniforms_FBox2ShadowHighlight,
    FUniforms_FBox2ShadowMul,
    FUniforms_FBox2ShadowMulHighlight,
    FUniforms_FBox2ShadowKnockout,
    FUniforms_FBox2ShadowHighlightKnockout,
    FUniforms_FBox2ShadowMulKnockout,
    FUniforms_FBox2ShadowMulHighlightKnockout,
    FUniforms_FBox2Shadowonly,
    FUniforms_FBox2ShadowonlyHighlight,
    FUniforms_FBox2ShadowonlyMul,
    FUniforms_FBox2ShadowonlyMulHighlight,
    FUniforms_FBox1Blur,
    FUniforms_FBox2Blur,
    FUniforms_FBox1BlurMul,
    FUniforms_FBox2BlurMul,
    FUniforms_FCMatrix,
    FUniforms_FCMatrixMul,
};

static VertexShaderType2 VShaderForFShader[FS2_Count] = {
    VS2_None,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
    VS2_VVatc,
};

