/**********************************************************************

Filename    :   GRendererPS3.h
Content     :   Vector graphics 2D renderer implementation for PS3 gcm
Created     :   February 2, 2007
Authors     :   Andrew Reisse

Notes       :   
History     :   

Copyright   :   (c) 1998-2006 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/


#ifndef INC_GRENDERERPS3_H
#define INC_GRENDERERPS3_H

#include "GTypes.h"
#include "GRefCount.h"

#include "GRenderer.h"
#include "GRendererCommonImpl.h"

#include <cell/gcm.h>

class GTexturePS3 : public GTextureImplNode
{
public:
    GTexturePS3 (GRendererNode *plistRoot) : GTextureImplNode(plistRoot) { }
    GTexturePS3 () {}

    virtual bool        InitTexture(CellGcmTexture *pTex, bool unused = 0)                  = 0;
    virtual bool        InitTexture(GImageBase* pim, UInt usage = Usage_Wrap)               = 0;

    virtual CellGcmTexture* GetNativeTexture() const                                        = 0;
};

class GRenderTargetPS3 : public GRenderTargetImplNode
{
public:
    struct PS3RenderTargetParams
    {
        CellGcmSurface* Surface;
        UInt            ActualHeight; // if 3d frame packing, otherwise zero
        bool            Tiled;

        PS3RenderTargetParams() : Surface(0), ActualHeight(0), Tiled(0) {}
    };
    GRenderTargetPS3 (GRendererNode *plistRoot) : GRenderTargetImplNode(plistRoot) { }
    GRenderTargetPS3 () { }

    virtual bool       InitRenderTarget(GTexture* ptarget, GTexture* pdepth = 0, GTexture* pstencil = 0) = 0;
    virtual bool       InitRenderTarget(PS3RenderTargetParams params) = 0;
};

class GAllocatorPS3
{
 public:
    typedef void* Handle;

    virtual ~GAllocatorPS3 () { }

    virtual Handle   Allocate(UInt32 location, UInt32 size, UInt32 align) = 0;
    virtual void     Free(Handle h) = 0;
    virtual void*    Map(Handle h) = 0;
    virtual void     Unmap(Handle h, void *m) = 0;
    virtual UInt32   GetLocation(Handle h) = 0;
    virtual UInt32   GetOffset(Handle h) = 0;
};


// Base class for PS3 gcm Renderer implementation.
// This class will be replaced with a new shape-centric version in the near future.

class GRendererPS3 : public GRenderer
{
public:
    typedef GTexturePS3     TextureType;
    typedef CellGcmTexture* NativeTextureType;

    // Creates a new renderer object
    static GRendererPS3* GSTDCALL CreateRenderer();

    // Returns created objects with a refCount of 1, must be user-released.
    virtual GTexturePS3* CreateTexture()                                                                = 0;    
    virtual GTexturePS3* CreateTextureYUV()                                                             = 0;
    virtual GRenderTarget* CreateRenderTarget()                                                      = 0;

    virtual GTexturePS3* PushTempRenderTarget(const GRectF& frameRect, UInt targetW, UInt targetH, bool wantStencil = 0)      = 0;

    virtual GAllocatorPS3* GetAllocator()
    {
        GASSERT(0);
        return 0;
    }

    // *** Implement Dependent Video Mode configuration

    enum VMConfigFlags
    {
        // No-wait texture updates. Application must call BeginFrame/EndFrame.
        // Glyph cache must be large enough for entire frame's usage.
        VMConfig_FastTexUpdate = 0x00000001,
    };

    // BaseLabel:        Specify a gcm label address for GFx to use
    // VertexArraySize:  Size of vertex array on main memory to allocate.
    // MaxCmdSize:       Max amount of extra data (e.g. indices) for one call
    struct DependentVideoModeParams
    {
        CellGcmContextData *Ctx;
        GAllocatorPS3      *Alloc;
        UInt32              BaseLabel;
        UInt32              VertexArraySize;
        UInt32              MaxCmdSize;
        UInt32              vmConfigFlags;

        DependentVideoModeParams (CellGcmContextData *ctx, GAllocatorPS3 *alloc,
                                  UInt32 vmconfigflags = 0,
                                  UInt32 baseLabel = 250, UInt32 vertexArraySize = 48 * 65536,
                                  SInt32 maxCmdSize = 24000)
        {
            Ctx = ctx; Alloc = alloc;
            BaseLabel = baseLabel;
            VertexArraySize = vertexArraySize;
            MaxCmdSize = maxCmdSize;
            vmConfigFlags = vmconfigflags;
        }
    };

    virtual bool                SetDependentVideoMode(const DependentVideoModeParams& params)
    {
        GUNUSED(params);
        GASSERT(0);
        return 0;
    }

    bool                        SetDependentVideoMode(CellGcmContextData *ctx, GAllocatorPS3 *alloc,
                                                      UInt32 BaseLabel = 250, UInt32 VertexArraySize = 48 * 65536,
                                                      SInt32 MaxCmdSize = 24000)
    {
        return SetDependentVideoMode(DependentVideoModeParams (ctx, alloc, 0, BaseLabel, VertexArraySize, MaxCmdSize));
    }


    // Returns back to original mode (cleanup)                                                      
    virtual bool                ResetVideoMode()
    {
        GASSERT(0);
        return 0;
    }


    virtual void                RemoveAllTextures()
    {
        GASSERT(0);
    }

    // Query display status
    enum DisplayStatus
    {
        DisplayStatus_Ok            = 0,
        DisplayStatus_NoModeSet     = 1,    // Video mode 
        DisplayStatus_Unavailable   = 2,    // Display is unavailable for rendering; check status again later
        DisplayStatus_NeedsReset    = 3     // May be returned in Dependent mode to indicate external action being required
    };

    virtual DisplayStatus       CheckDisplayStatus() const
    {
        return DisplayStatus_NoModeSet;
    }
};
    


#endif 
