/**********************************************************************

Filename    :   GFxSound.cpp
Content     :   
Created     :   
Authors     :   

Copyright   :   (c) 2001-2006 Scaleform Corp. All Rights Reserved.

Notes       :   

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#include "GFxSound.h"
#ifndef GFC_NO_SOUND

#include "GStd.h"
#include "GFxStream.h"
#include "GFxLog.h"
#include "GFxLoadProcess.h"
#include "GFxSprite.h"
#include "GFxSoundTagsReader.h"
#include "GFxSound.h"
#include "GFxButton.h"


GFxButtonSoundDef* GFxSoundTagsReader::ReadButtonSoundDef(GFxLoadProcess* p)
{
    GFxButtonSoundDefImpl* psound = GHEAP_NEW(p->GetLoadHeap()) GFxButtonSoundDefImpl;
    p->LogParse("button sound options:\n");
    for (int i = 0; i < 4; i++)
    {
        GFxSoundInfo& bs = psound->ButtonSounds[i];
        bs.SoundId = GFxResourceId(p->ReadU16());
        if (bs.SoundId == 0)
            continue;
        GFxResourceHandle rh;
        if (p->GetResourceHandle(&rh, bs.SoundId))
        {
            bs.pSample.SetFromHandle(rh);
        }
        else
        {
            p->LogParse("sound tag not found, SoundId=%d, button state #=%i", (int)bs.SoundId.GetIdValue(), i);
        }
        p->LogParse("\n  SoundId = %d\n", (int)bs.SoundId.GetIdValue());
        bs.Style.Read(p->GetStream());
    }
    return psound;
}

void GFxSoundTagsReader::ReadDefineSoundTag(GFxLoadProcess* p, const GFxTagInfo& tagInfo)
{
    GUNUSED(tagInfo); 
    GASSERT(tagInfo.TagType == GFxTag_DefineSound);

    GSoundRenderer* pSoundRenderer = p->GetLoadStates()->GetAudio()->GetRenderer();
    UInt32 caps = 0;
    if (pSoundRenderer)
        pSoundRenderer->GetRenderCaps(&caps);

    GFxStream* in = p->GetStream();
    UInt16  characterId = in->ReadU16();
    GPtr<GSoundData> psound = *GFx_ReadSoundData(p, characterId);

    if (pSoundRenderer && (psound->GetFormat() & GSoundDataBase::Sample_MP3) && (caps & GSoundRenderer::Cap_NoMP3))
        return;

    GFxResourceData resData = GFxSoundResourceCreator::CreateSoundResourceData(psound);
    p->AddDataResource(GFxResourceId(characterId), resData);
}

class GFxStartSoundTag : public GASExecuteTag
{
public:
    GFxSoundInfo           Info;
    // Initialize this StartSound tag from the GFxStream & given sample.
    // Insert ourself into the GFxMovieDefSub.
    void    Read(GFxLoadProcess* p, const GFxTagInfo& tagInfo, UInt16 soundID, GFxResourceHandle rh)      
    {
        GUNUSED(tagInfo);    
        GASSERT(tagInfo.TagType == GFxTag_StartSound);

        Info.SoundId = GFxResourceId(soundID);
        Info.Style.Read(p->GetStream());
        Info.pSample.SetFromHandle(rh);
    }

    void    Execute(GFxSprite* m)
    {
        Info.Play(m);
    }
};

void GFxSoundTagsReader::ReadStartSoundTag(GFxLoadProcess* p, const GFxTagInfo& tagInfo)
{
    GUNUSED(tagInfo);    
    GASSERT(tagInfo.TagType == GFxTag_StartSound);

    UInt16  soundId = p->ReadU16();

    GFxResourceHandle   rh;

    if (p->GetResourceHandle(&rh, GFxResourceId(soundId)))
    {
        GFxStartSoundTag*   sst = p->AllocTag<GFxStartSoundTag>();
        sst->Read(p, tagInfo, soundId, rh);

        p->LogParse("StartSound tag: id=%d, stop = %d, loop ct = %d\n",
            soundId, int(sst->Info.Style.StopPlayback), sst->Info.Style.LoopCount);
        p->AddExecuteTag(sst);
    }
    else
    {
        p->LogError("StartSoundLoader: SoundId %d is not defined\n", soundId);
    }
}

void GFxSoundTagsReader::ReadButtonSoundTag(GFxLoadProcess* p, const GFxTagInfo& tagInfo)
{
    GASSERT(tagInfo.TagType == GFxTag_ButtonSound);

    int                 buttonCharacterId = p->ReadU16();
    GFxResourceHandle   rh;
    GFxResource*        pres = 0;

    if (p->GetResourceHandle(&rh, GFxResourceId(buttonCharacterId)))
        pres = rh.GetResourcePtr();

    if (pres)
    {
        if (pres->GetResourceType() == GFxResource::RT_ButtonDef)
        {
            GFxButtonCharacterDef* ch = (GFxButtonCharacterDef*) pres;
            GASSERT(ch != NULL);
            ch->Read(p, tagInfo.TagType);
        }
    }
    else
    {
        p->LogError("Error: ButtonDef %d referenced in ButtonSound tag not found", buttonCharacterId);
    }
}


void GFxSoundTagsReader::ReadDefineExternalSoundTag(GFxLoadProcess* p, const GFxTagInfo& tagInfo)
{
    GUNUSED(tagInfo);
    GASSERT(tagInfo.TagType == GFxTag_DefineExternalSound);

    // Utilizes the tag 1006 (unused in normal SWF): the format is as follows:
    // Header           RECORDHEADER    1006
    // CharacterID      UI16
    // SoundFormat      UI16            0 - WAV
    // Bits             UI16
    // Channels         UI16
    // SampleRate       UI32
    // SampleCount      UI32
    // SeekSample       UI32
    // ExportNameLen    UI8
    // ExportName       UI8[ExportNameLen]
    // FileNameLen      UI8             with extension
    // FileName         UI8[FileNameLen]

    GFxStream* pin = p->GetStream();

    UInt    soundResourceId    = p->ReadU16();
    UInt    soundFormat        = p->ReadU16();
    UInt    bits               = p->ReadU16();
    UInt    channels           = p->ReadU16();
    UInt    sample_rate        = p->ReadU32();
    UInt    sample_count       = p->ReadU32();
    UInt    seek_sample        = p->ReadU32();
    GString soundExportName, soundFileName;
    pin->ReadStringWithLength(&soundExportName);
    pin->ReadStringWithLength(&soundFileName);

    pin->LogParse("  DefineExternalSound: id = 0x%X, fmt = %d, name = '%s', exp = '%s'\n",
        soundResourceId,
        soundFormat,
        soundFileName.ToCStr(),
        soundExportName.ToCStr());

    // Fill in file information.
    GPtr<GFxSoundFileInfo> pfi = *new GFxSoundFileInfo;
    if (pfi)
    {
        pfi->FileName       = soundFileName;
        pfi->ExportName     = soundExportName;
        pfi->pExporterInfo  = p->GetExporterInfo();
        pfi->Format         = (GFxFileConstants::FileFormatType)soundFormat;
        pfi->Bits           = bits;
        pfi->Channels       = channels;
        pfi->SampleRate     = sample_rate;
        pfi->SampleCount    = sample_count;
        pfi->SeekSample     = seek_sample;
        pfi->Use = GFxResource::Use_SoundSample;

        // Add resource id and data.      
        p->AddDataResource(GFxResourceId(soundResourceId),
            GFxSoundFileResourceCreator::CreateSoundFileResourceData(pfi));
    }

}
void GFxSoundTagsReader::ReadDefineExternalStreamSoundTag(GFxLoadProcess* p, const GFxTagInfo& tagInfo)
{
    GUNUSED(tagInfo);
    GASSERT(tagInfo.TagType == GFxTag_DefineExternalStreamSound);
    // utilizes the tag 1007 (unused in normal SWF): the format is as follows:
    // Header           RECORDHEADER    1007
    // SoundFormat      UI16            0 - WAV
    // Bits             UI16
    // Channels         UI16
    // SampleRate       UI32
    // SampleCount      UI32
    // SeekSample       UI32
    // StartFrame       UI32
    // LastFrame        UI32
    // FileNameLen      UI8             with extension
    // FileName         UI8[FileNameLen]
    GFxStream* pin = p->GetStream();
    GPtr<GFxSoundStreamDefImpl> pdef = *new GFxSoundStreamDefImpl;

    UInt    soundFormat        = p->ReadU16();
    UInt    bits               = p->ReadU16();
    UInt    channels           = p->ReadU16();
    pdef->StreamSoundRate      = p->ReadU32();
    pdef->StreamSampleCount    = p->ReadU32();
    pdef->LatencySeek          = p->ReadU32();
    pdef->StartFrame           = p->ReadU32();
    pdef->LastFrame            = p->ReadU32();
    GUNUSED2(bits,channels);
    GString soundFileName;
    pin->ReadStringWithLength(&soundFileName);

    pin->LogParse("  DefineExternalStreamSound: fmt = %d, name = '%s'\n",
        soundFormat,
        soundFileName.ToCStr());

    // Translate filename.
    GString fname;
    GFxURLBuilder::LocationInfo loc(GFxURLBuilder::File_Sound,
        soundFileName, p->GetLoadStates()->GetRelativePath());
    p->GetLoadStates()->BuildURL(&fname, loc);

    GPtr<GSoundFile> sound = *new GSoundFile(fname.ToCStr(),pdef->StreamSoundRate, pdef->StreamSampleCount, true);
    sound->SetSeekSample(pdef->LatencySeek);
    pdef->pSoundInfo = *new GSoundInfo(sound);
    p->SetSoundStream(pdef);

}
void GFxSoundTagsReader::ReadSoundStreamHeadTag(GFxLoadProcess* p, const GFxTagInfo& tagInfo)
{
    GUNUSED(tagInfo);    
    GASSERT(tagInfo.TagType == GFxTag_SoundStreamHead || tagInfo.TagType == GFxTag_SoundStreamHead2);

    p->LogParse("SoundStreamHead%c\n", (tagInfo.TagType == GFxTag_SoundStreamHead? ' ' : '2'));

    GSoundRenderer* pSoundRenderer = p->GetLoadStates()->GetAudio()->GetRenderer();
    UInt32 caps = 0;
    if (pSoundRenderer)
        pSoundRenderer->GetRenderCaps(&caps);

    if (pSoundRenderer && (caps & GSoundRenderer::Cap_NoStreaming))
        return;

    GFxStream* in = p->GetStream();
    GPtr<GFxSoundStreamDefImpl> pdef = *new GFxSoundStreamDefImpl;   
    pdef->ReadHeadTag(in);

    if (pdef->StreamSampleCount > 0)
        p->SetSoundStream(pdef);
}

void GFxSoundTagsReader::ReadSoundStreamBlockTag(GFxLoadProcess* p, const GFxTagInfo& tagInfo)
{
    GUNUSED(tagInfo);
    GASSERT(tagInfo.TagType == GFxTag_SoundStreamBlock);

    GSoundRenderer* pSoundRenderer = p->GetLoadStates()->GetAudio()->GetRenderer();
    UInt32 caps = 0;
    if (pSoundRenderer)
        pSoundRenderer->GetRenderCaps(&caps);

    if (pSoundRenderer && (caps & GSoundRenderer::Cap_NoStreaming))
        return;

    GFxSoundStreamDefImpl* pdef = (GFxSoundStreamDefImpl*)p->GetSoundStream();
    GASSERT(pdef);

    if(pSoundRenderer && pdef->SoundCompression == 2 && (caps & GSoundRenderer::Cap_NoMP3))
        return;

    pdef->ReadBlockTag(p);
}

#endif // GFC_NO_SOUND
