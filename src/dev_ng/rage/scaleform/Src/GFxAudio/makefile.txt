Project GFxAudio
IncludePath ../../Include;../GKernel;$(RAGE_DIR)/base/src/jpeg;$(RAGE_DIR)/base/src/zlib
Files 
{
    GASSoundObject.cpp
    GASSoundObject.h
    GFxAudio.cpp
    GFxSound.cpp
    GFxSound.h
    GFxSoundResource.cpp
    GFxSoundResource.h
    GFxSoundTagsReader.cpp
    GFxSoundTagsReader.h
}
