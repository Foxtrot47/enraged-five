/**********************************************************************

Filename    :   GFxSound.h
Content     :   SWF (Shockwave Flash) player library
Created     :   July 7, 2005
Authors     :   

Notes       :   
History     :   

Copyright   :   (c) 1998-2006 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#ifndef INC_GFXSOUND_H
#define INC_GFXSOUND_H

#include "GConfig.h"
#ifndef GFC_NO_SOUND

#include "GTypes.h"
#include "GArray.h"
#include "GFxPlayerStats.h"

#include "GFxResource.h"
#include "GFxString.h"
#include "GSoundRenderer.h"
#include "GFxResourceHandle.h"
#include "GFxSoundResource.h"
#include "GFxAudio.h"

class GFxStream;
class GFxSoundResource;
class GFxASCharacter;
class GFxAudio;
class GFxLoadProcess;
class GFxMovieRoot;
class GFxSprite;

class GFxSoundEnvelope
{
public:
    UInt32 Mark44;
    UInt16 Level0;
    UInt16 Level1;
};

class GFxSoundStyle
{
public:
    bool NoMultiple;
    bool StopPlayback;
    bool HasEnvelope;
    bool HasLoops;
    bool HasOutPoint;
    bool HasInPoint;
    UInt32 InPoint;
    UInt32 OutPoint;
    UInt16 LoopCount;
    GArray<GFxSoundEnvelope> Envelopes;

    void Read(GFxStream* in);
};

GSoundData* GFx_ReadSoundData(GFxLoadProcess* p, UInt16  characterId);

class GFxSoundInfo
{
public:
    GFxResourcePtr<GFxSoundResource> pSample;
    GFxSoundStyle          Style;
    GFxResourceId          SoundId;

    GFxSoundInfo();
    void Play(GFxASCharacter *pchar);
};

class GFxASCharacter;
class GFxButtonSoundDef : public GNewOverrideBase<GFxStatMD_CharDefs_Mem>
{
public:
    virtual ~GFxButtonSoundDef() {};

    virtual void    Play(GFxASCharacter *pchar, int buttonIdx) = 0;
};

class GFxSoundStreamDef : public GRefCountBaseNTS<GFxSoundStreamDef,GFxStatMD_Sounds_Mem>
{
public:
    virtual ~GFxSoundStreamDef() {}
    virtual bool ProcessSwfFrame(GFxMovieRoot* proot, UInt frame, GFxSprite* pchar) = 0;
};

class GFxButtonSoundDefImpl : public GFxButtonSoundDef
{
public:
    virtual ~GFxButtonSoundDefImpl() {};

    virtual void    Play(GFxASCharacter *pchar, int buttonIdx);

    GFxSoundInfo    ButtonSounds[4];
};

class GFxSoundStreamDefImpl : public  GFxSoundStreamDef
{
public:

    UInt PlaybackSoundRate;   // multiples of 5512.5
    UInt PlaybackSound16bit;
    UInt PlaybackStereo;
    UInt SoundCompression;
    UInt StreamSoundRate;   // multiples of 5512.5
    UInt StreamSound16bit;
    UInt StreamStereo;
    UInt StreamSampleCount;
    UInt LatencySeek;

    UInt StartFrame;
    UInt LastFrame;


    GPtr<GSoundInfo>      pSoundInfo;

    GFxSoundStreamDefImpl();
    ~GFxSoundStreamDefImpl();

    void ReadHeadTag(GFxStream* in);
    void ReadBlockTag(GFxLoadProcess* p);
    
    virtual bool ProcessSwfFrame(GFxMovieRoot* proot, UInt frame, GFxSprite* pchar);

};

#endif // GFC_NO_SOUND


#endif // INC_GFXSOUND_H

