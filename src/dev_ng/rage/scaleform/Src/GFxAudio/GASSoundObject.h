/**********************************************************************

Filename    :   AS/GASSoundObject.h
Content     :   Number object functinality
Created     :   October 20, 2006
Authors     :   Artyom Bolgar

Notes       :   
History     :   

Copyright   :   (c) 1998-2006 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#ifndef INC_GFXSOUNDOBJECT_H
#define INC_GFXSOUNDOBJECT_H

#include "GConfig.h"
#ifndef GFC_NO_SOUND


#include "GFxAction.h"
#include "AS/GASObjectProto.h"
#include "GSoundRenderer.h"
#include "GFxCharacter.h"
#include "GFxSoundResource.h"
//
// sound object
//


class GASSoundObject : public GASObject
{
protected:
    GASSoundObject(GASStringContext *psc, GASObject* pprototype)
        : GASObject(psc)
    { 
        commonInit();
        Set__proto__(psc, pprototype); // this ctor is used for prototype obj only
    }
    void commonInit()
    {
        pMovieRoot = NULL;
    }

public:
    ~GASSoundObject() 
    {
        DetachFromTarget();
    }
    GPtr<GSoundSample>       pSample;
    GPtr<GFxSoundResource>   pResource;      // resource which was used to create this sound
    GPtr<GFxCharacterHandle> pTargetHandle;  // movieclip which sound object is connected to
    GFxMovieRoot*            pMovieRoot;

    ObjectType      GetObjectType() const   { return Object_Sound; }

    GASSoundObject(GASEnvironment* penv) : GASObject(penv)
    {        
        commonInit();
        GASStringContext* psc = penv->GetSC();
        Set__proto__(psc, penv->GetPrototype(GASBuiltin_Sound));
        pMovieRoot = penv->GetMovieRoot();
    }
    
    virtual bool        GetMember(GASEnvironment* penv, const GASString& name, GASValue* val);

    void                AttachToTarget(GFxSprite* psprite);
    void                DetachFromTarget();
    void                ReleaseTarget();

    GFxSprite*          GetSprite();
    void                ExecuteOnSoundComplete();

#ifndef GFC_NO_GC
protected:
    virtual void Finalize_GC()
    {
        DetachFromTarget();
        pSample.~GPtr<GSoundSample>();
        pTargetHandle.~GPtr<GFxCharacterHandle>();
        pResource.~GPtr<GFxSoundResource>();
        GASObject::Finalize_GC();
    }
#endif //GFC_NO_GC
};

class GASSoundProto : public GASPrototype<GASSoundObject>
{
public:
    GASSoundProto(GASStringContext *psc, GASObject* pprototype, const GASFunctionRef& constructor);

    static void Attach(const GASFnCall& fn);
    static void GetBytesLoaded(const GASFnCall& fn);
    static void GetBytesTotal(const GASFnCall& fn);
    static void GetPan(const GASFnCall& fn);
    static void GetTransform(const GASFnCall& fn);
    static void GetVolume(const GASFnCall& fn);
    static void LoadSound(const GASFnCall& fn);
    static void SetPan(const GASFnCall& fn);
    static void SetTransform(const GASFnCall& fn);
    static void SetVolume(const GASFnCall& fn);
    static void Start(const GASFnCall& fn);
    static void Stop(const GASFnCall& fn);
};

class GASSoundCtorFunction : public GASCFunctionObject
{
public:
    GASSoundCtorFunction(GASStringContext *psc) : GASCFunctionObject(psc, GlobalCtor) {}

    virtual GASObject* CreateNewObject(GASEnvironment* penv) const;

    static void GlobalCtor(const GASFnCall& fn);

    static GASFunctionRef Register(GASGlobalContext* pgc);
};

#endif // GFC_NO_SOUND


#endif // INC_GFXSOUNDOBJECT_H
