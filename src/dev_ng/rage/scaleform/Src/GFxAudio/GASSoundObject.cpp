/**********************************************************************

Filename    :   GFxSoundObject.cpp
Content     :   Number object functinality
Created     :   October 20, 2006
Authors     :   Artyom Bolgar

Notes       :   
History     :   

Copyright   :   (c) 1998-2006 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#include "GASSoundObject.h"
#ifndef GFC_NO_SOUND

#include "GFxMovieDef.h"
#include "GFxCharacter.h"
#include "GFxSound.h"
#include "GFxPlayerImpl.h"
#include "GFxSprite.h"
#include "GFxAudio.h"
#include "GFxPlayerImpl.h"

#include "AS/GASAsBroadcaster.h"
#include "GFxASString.h"

#include <stdio.h>
#include <stdlib.h>

void GASSoundObject::AttachToTarget(GFxSprite *pcharacter)
{
    GASSERT(pcharacter);
    pTargetHandle = pcharacter->GetCharacterHandle();
    pcharacter->AttachSoundObject(this);
}

GFxSprite* GASSoundObject::GetSprite()
{
    //GASSERT(pTargetHandle);
    if (!pTargetHandle)
        return NULL;
    if (!pMovieRoot)
        return NULL;

    GFxASCharacter* ptarget = pTargetHandle->ResolveCharacter(pMovieRoot);
    if (!ptarget) return NULL;

    GASSERT(ptarget->IsSprite());
    return ptarget->ToSprite();
}
void GASSoundObject::DetachFromTarget()
{
    if(!pTargetHandle) return;
    GFxSprite* psprite = GetSprite();
    if (psprite)
        psprite->DetachSoundObject(this);
}

void GASSoundObject::ReleaseTarget()
{
    pTargetHandle = NULL;
}

void GASSoundObject::ExecuteOnSoundComplete()
{
    GFxSprite* psprite = GetSprite();
    if (!psprite) return;
    GASEnvironment* penv = psprite->GetASEnvironment();
    GASValue callback;
    if (GetMemberRaw(penv->GetSC(),penv->CreateConstString("onSoundComplete"), &callback))
    {
        GASFunctionRef method = callback.ToFunction(penv);
        GASValue result;
        method.Invoke(GASFnCall(&result, this, penv, 0, penv->GetTopIndex()));
    }
}

bool GASSoundObject::GetMember(GASEnvironment* penv, const GASString& name, GASValue* val)
{
    if(name == "position")
    {
        GFxSprite* psprite = GetSprite();
        if (psprite)
            *val = psprite->GetActiveSoundPosition(this)*1000;
        else
            *val = GASValue(0.0f);
        return true;
    }
    else if(name == "duration")
    {
        if (pSample)
            *val = pSample->GetDuration()*1000;
        else
            *val = GASValue(0.0f);
        return true;
    }
    return GASObject::GetMember(penv, name, val);
}

//////////////////////////////////////////////////////////////////////////
//
void    GASSoundProto::Start(const GASFnCall& fn)
{

    GFxMovieRoot* proot = fn.Env->GetMovieRoot();
    if (!proot) return;
    GSoundRenderer* pplayer = proot->GetSoundRenderer();
    if (!pplayer) return;

    if (!fn.ThisPtr || fn.ThisPtr->GetObjectType() != Object_Sound)
        return;

    GASSoundObject* pobj = static_cast<GASSoundObject*>(fn.ThisPtr);
    GASSERT(pobj);
    if (!pobj) return;

    GFxSprite* psprite = pobj->GetSprite();
    if (!psprite) return;

    SInt32 secondOffset = 0;
    SInt32 loops = 0;

    if (fn.NArgs > 0)
        secondOffset = fn.Arg(0).ToInt32(fn.Env);

    if (fn.NArgs > 1)
        loops = fn.Arg(1).ToInt32(fn.Env);

    if (loops == 0)
        loops = 1;
    
    GPtr<GSoundChannel> pchan;
    if (!pobj->pResource)
    {
        // this means that this sound was created by Sound.loadSound method.
        // we need to check if it is already playing and if it is the dont 
        // start it again.
        if (psprite->IsSoundPlaying(pobj))
            return;
    }
    pchan = *pplayer->PlaySample(pobj->pSample,true);
    if (pchan)
    {
        if (secondOffset > 0 || loops > 0)
            pchan->Loop(loops, secondOffset/1.f);
        pchan->SetVolume(psprite->GetRealSoundVolume());
        pchan->SetPan(psprite->GetRealSoundPan());
        pchan->Pause(false);
        psprite->AddActiveSound(pchan, pobj, pobj->pResource);
    }
}


void    GASSoundProto::Stop(const GASFnCall& fn)
{
    GASSoundObject* pobj = static_cast<GASSoundObject*>(fn.ThisPtr);
    GASSERT(pobj);
    if (!pobj) return;

    GFxSprite* psprite = pobj->GetSprite();
    if (!psprite) return;

    if (fn.NArgs > 0)
    {
        GASString id(fn.Arg(0).ToString(fn.Env));
        GASSoundObject* pobj = static_cast<GASSoundObject*>(fn.ThisPtr);
        GASSERT(pobj);
        GPtr<GFxMovieDefImpl> md = fn.Env->GetTarget()->GetResourceMovieDef();
        if (pobj && md) // can it be NULL?
        {
            // Get exported resource for linkageId and verify that it is a sound.
            GFxResourceBindData resBindData;
            if (!fn.Env->GetMovieRoot()->FindExportedResource(md, &resBindData, id.ToCStr()))
                return;
            GASSERT(resBindData.pResource.GetPtr() != 0);
            // Must check resource type, since users can theoretically pass other resource ids.
            if (resBindData.pResource->GetResourceType() != GFxResource::RT_SoundSample)
                return;

            GFxSoundResource* psound = (GFxSoundResource*)resBindData.pResource.GetPtr();
            if (psound)
            {
                psprite->StopActiveSounds(psound);
            }
        }
    }
    else
        psprite->StopActiveSounds();

}

void    GASSoundProto::Attach(const GASFnCall& fn)
{
    GFxMovieRoot* proot = fn.Env->GetMovieRoot();
    if (!proot) return;
    GSoundRenderer* pplayer = proot->GetSoundRenderer();
    if (!pplayer) return;

    if (fn.NArgs < 1)
    {
        fn.Env->LogScriptError("Error: Sound.AttachSound needs one Argument (the file name)\n");
        return;
    }
    GASString id(fn.Arg(0).ToString(fn.Env));
    GASSoundObject* pobj = static_cast<GASSoundObject*>(fn.ThisPtr);
    GASSERT(pobj);

    GPtr<GFxMovieDefImpl> md = fn.Env->GetTarget()->GetResourceMovieDef();
    if (pobj && md) // can it be NULL?
    {
        // Get exported resource for linkageId and verify that it is a sound.
        GFxResourceBindData resBindData;
        if (!fn.Env->GetMovieRoot()->FindExportedResource(md, &resBindData, id.ToCStr()))
            return;
        GASSERT(resBindData.pResource.GetPtr() != 0);
        // Must check resource type, since users can theoretically pass other resource ids.
        if (resBindData.pResource->GetResourceType() != GFxResource::RT_SoundSample)
            return;

        GFxSoundResource* psound = (GFxSoundResource*)resBindData.pResource.GetPtr();
        if (psound)
        {
            pobj->pSample = psound->GetSoundInfo()->GetSoundSample(pplayer);
            pobj->pResource = psound;
        }
    }
}
void    GASSoundProto::GetBytesLoaded(const GASFnCall& fn) 
{ 
    fn.Result->SetUndefined();
    GASSoundObject* pobj = static_cast<GASSoundObject*>(fn.ThisPtr);
    if (pobj && !pobj->pResource)
    {
        // BytesTotal is only defined of sounds loaded with Sound.loadSound method
        fn.Result->SetInt(pobj->pSample->GetBytesLoaded());
    }
}
void    GASSoundProto::GetBytesTotal(const GASFnCall& fn)  
{ 
    fn.Result->SetUndefined();
    GASSoundObject* pobj = static_cast<GASSoundObject*>(fn.ThisPtr);
    if (pobj && !pobj->pResource)
    {
        // BytesTotal is only defined of sounds loaded with Sound.loadSound method
        fn.Result->SetInt(pobj->pSample->GetBytesTotal());
    }
}
void    GASSoundProto::GetPan(const GASFnCall& fn)         
{ 
    GASSoundObject* pobj = static_cast<GASSoundObject*>(fn.ThisPtr);
    GASSERT(pobj);
    if (!pobj) return;
    GFxSprite* psprite = pobj->GetSprite();
    if (!psprite) return;

    fn.Result->SetInt(psprite->GetSoundPan()); 
}
void    GASSoundProto::GetTransform(const GASFnCall& fn)   
{ 
    fn.Result->SetUndefined(); 
}
void    GASSoundProto::GetVolume(const GASFnCall& fn)      
{ 
    GASSoundObject* pobj = static_cast<GASSoundObject*>(fn.ThisPtr);
    fn.Result->SetUndefined();
    GASSERT(pobj);
    if (!pobj) return;
    GFxSprite* psprite = pobj->GetSprite();
    if (!psprite) return;

    fn.Result->SetInt(psprite->GetSoundVolume()); 
}
void    GASSoundProto::LoadSound(const GASFnCall& fn)
{ 
    GFxMovieRoot* proot = fn.Env->GetMovieRoot();
    if (!proot) return;
    GSoundRenderer* pplayer = proot->GetSoundRenderer();
    if (!pplayer) return;

    GASSoundObject* pobj = static_cast<GASSoundObject*>(fn.ThisPtr);
    GASSERT(pobj);
    GFxSprite* psprite = pobj->GetSprite();
    if (!psprite) return;

    if (fn.NArgs < 1)
    {
        fn.Env->LogScriptError("Error: Sound.AttachSound needs one Argument (the file name)\n");
        return;
    }

    GASString url(fn.Arg(0).ToString(fn.Env));

    bool streamflag = false;
    if (fn.NArgs > 1)
        streamflag = fn.Arg(1).ToBool(fn.Env);

    GPtr<GFxLoadStates>  pls = *new GFxLoadStates(proot->pLevel0Def->pLoaderImpl, proot->pStateBag);

    // Obtain level0 path before it has a chance to be unloaded below.
    GString level0Path;
    proot->GetLevel0Path(&level0Path);

    GFxURLBuilder::LocationInfo loc(GFxURLBuilder::File_Regular, GString(url.ToCStr()), level0Path);
    GString                   fileName;
    pls->BuildURL(&fileName, loc);

    GPtr<GSoundSample> psample = *pplayer->CreateSampleFromFile(fileName.ToCStr(), streamflag);
    if (!psample)
    {
        GASValue callback;
        if (pobj->GetMemberRaw(fn.Env->GetSC(),fn.Env->CreateConstString("onLoad"), &callback))
        {
            GASFunctionRef method = callback.ToFunction(fn.Env);
            GASValue result;
            fn.Env->Push(false);
            method.Invoke(GASFnCall(&result, pobj, fn.Env, 1, fn.Env->GetTopIndex()));
            fn.Env->Drop(1);
        }
        return;
    }
    pobj->pResource = NULL;
    pobj->pSample = psample;
    if (streamflag)
    {
        GPtr<GSoundChannel> pchan;
        pchan = *pplayer->PlaySample(pobj->pSample,true);
        if (pchan)
        {
            pchan->SetVolume(psprite->GetRealSoundVolume());
            pchan->SetPan(psprite->GetRealSoundPan());
            pchan->Pause(false);
            psprite->AddActiveSound(pchan, pobj, pobj->pResource);
        }
    }
    GASValue callback;
    if (pobj->GetMemberRaw(fn.Env->GetSC(),fn.Env->CreateConstString("onLoad"), &callback))
    {
        GASFunctionRef method = callback.ToFunction(fn.Env);
        GASValue result;
        fn.Env->Push(true);
        method.Invoke(GASFnCall(&result, pobj, fn.Env, 1, fn.Env->GetTopIndex()));
        fn.Env->Drop(1);
    }
    fn.Env->Drop(1);
}
void    GASSoundProto::SetPan(const GASFnCall& fn)         
{ 
    if (fn.NArgs < 1)
    {
        fn.Env->LogScriptError("Error: Sound.SetPan needs one Argument\n");
        return;
    }

    SInt pan = fn.Arg(0).ToInt32(fn.Env);

    GASSoundObject* pobj = static_cast<GASSoundObject*>(fn.ThisPtr);
    GASSERT(pobj);
    if (!pobj) return;
    GFxSprite* psprite = pobj->GetSprite();
    if (!psprite) return;
    psprite->SetSoundPan(pan);
}
void    GASSoundProto::SetTransform(const GASFnCall& fn)   
{ 
    GUNUSED(fn); 
}
void    GASSoundProto::SetVolume(const GASFnCall& fn)      
{ 
    if (fn.NArgs < 1)
    {
        fn.Env->LogScriptError("Error: Sound.SetVolume needs, at least, one Argument\n");
        return;
    }

    SInt volume = fn.Arg(0).ToInt32(fn.Env);
    SInt subvol = 100;
    if (fn.NArgs > 1)
        subvol = fn.Arg(1).ToInt32(fn.Env);

    GASSoundObject* pobj = static_cast<GASSoundObject*>(fn.ThisPtr);
    GASSERT(pobj);
    if (!pobj) return;
    GFxSprite* psprite = pobj->GetSprite();
    if (!psprite) return;
    psprite->SetSoundVolume(volume, subvol);
}

static const GASNameFunction GAS_SoundFunctionTable[] = 
{
    { "attachSound",    &GASSoundProto::Attach  },
    { "getBytesLoaded", &GASSoundProto::GetBytesLoaded },
    { "getBytesTotal",  &GASSoundProto::GetBytesTotal },
    { "getPan",         &GASSoundProto::GetPan },
    { "getTransform",   &GASSoundProto::GetTransform },
    { "getVolume",      &GASSoundProto::GetVolume },
    { "loadSound",      &GASSoundProto::LoadSound },
    { "setPan",         &GASSoundProto::SetPan },
    { "setTransform",   &GASSoundProto::SetTransform },
    { "setVolume",      &GASSoundProto::SetVolume },
    { "start",          &GASSoundProto::Start },
    { "stop",           &GASSoundProto::Stop },
    { 0, 0 }
};

GASSoundProto::GASSoundProto(GASStringContext *psc, GASObject* pprototype, const GASFunctionRef& constructor) :
GASPrototype<GASSoundObject>(psc, pprototype, constructor)
{
    InitFunctionMembers(psc, GAS_SoundFunctionTable);
}

// Constructor for ActionScript class Sound.
void    GASSoundCtorFunction::GlobalCtor(const GASFnCall& fn)
{
    GPtr<GASSoundObject> psoundObj;
    if (fn.ThisPtr && fn.ThisPtr->GetObjectType() == Object_Sound && !fn.ThisPtr->IsBuiltinPrototype())
        psoundObj = static_cast<GASSoundObject*>(fn.ThisPtr);
    else
        psoundObj = *GHEAP_NEW(fn.Env->GetHeap()) GASSoundObject(fn.Env);

    GFxASCharacter* mc = NULL;
    if (fn.NArgs > 0)
    {
        mc = fn.Arg(0).ToASCharacter(fn.Env);
        if (mc && !mc->IsSprite())
            mc = NULL;
    }
    // if we were not provided with the correct sprite we link this object to movie root
    if (!mc)
        mc = fn.Env->GetMovieRoot()->GetLevelMovie(0);
    GASSERT(mc);
    GASSERT(mc->IsSprite());
    psoundObj->AttachToTarget(mc->ToSprite());
    fn.Result->SetAsObject(psoundObj.GetPtr());
}

GASObject* GASSoundCtorFunction::CreateNewObject(GASEnvironment* penv) const 
{
    return GHEAP_NEW(penv->GetHeap()) GASSoundObject(penv);
}

GASFunctionRef GASSoundCtorFunction::Register(GASGlobalContext* pgc)
{
    GASStringContext sc(pgc, 8);
    GASFunctionRef  ctor(*GHEAP_NEW(pgc->GetHeap())  GASSoundCtorFunction(&sc));
    GPtr<GASObject> proto = *GHEAP_NEW(pgc->GetHeap())  GASSoundProto(&sc, pgc->GetPrototype(GASBuiltin_Object), ctor);
    pgc->SetPrototype(GASBuiltin_Sound, proto);
    pgc->pGlobal->SetMemberRaw(&sc, pgc->GetBuiltin(GASBuiltin_Sound), GASValue(ctor));
    return ctor;
}

#endif // GFC_NO_SOUND
