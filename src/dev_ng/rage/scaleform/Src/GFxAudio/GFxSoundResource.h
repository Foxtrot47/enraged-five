/**********************************************************************

Filename    :   GFxSound.h
Content     :   SWF (Shockwave Flash) player library
Created     :   September, 2008
Authors     :   Maxim Didenko

Notes       :   
History     :   

Copyright   :   (c) 1998-2006 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#ifndef INC_GFXSOUNDRESOURCE_H
#define INC_GFXSOUNDRESOURCE_H

#include "GConfig.h"
#ifndef GFC_NO_SOUND

#include "GTypes.h"

#include "GFxResource.h"
#include "GFxResourceHandle.h"
#include "GSound.h"
#include "GSoundRenderer.h"

class GFxFileOpener;

struct GFxSoundFileInfo : public GFxResourceFileInfo
{
    GFxResource::ResourceUse    Use;
    GString                     ExportName;
    UInt                        Bits;
    UInt                        Channels;
    UInt                        SampleRate;
    UInt                        SampleCount;
    UInt                        SeekSample;

    GFxSoundFileInfo()
    {
        Use = GFxResource::Use_SoundSample;
    }
    GFxSoundFileInfo(const GFxSoundFileInfo& other) : GFxResourceFileInfo(other)
    {
        Use = other.Use;
        ExportName = other.ExportName;
        Bits = other.Bits;
        Channels = other.Channels;
        SampleRate = other.SampleRate;
        SampleCount = other.SampleCount;
        SeekSample = other.SeekSample;
    }
};

// ***** GFxSoundResource
class GFxSoundResource : public GFxResource
{
protected:

    GPtr<GSoundInfoBase>    pSoundInfo;
    // Key used to look up / resolve this object (contains file data).
    GFxResourceKey          Key;
    // What the resource is used for
    ResourceUse             UseType;

    SInt                    PlayingCount;
public:

    GFxSoundResource(GSoundInfoBase* psound, ResourceUse use = Use_SoundSample)
    {
        pSoundInfo   = psound;
        UseType      = use;
        PlayingCount = 0;
    }
    GFxSoundResource(GSoundInfoBase* psound, const GFxResourceKey& key, ResourceUse use = Use_SoundSample)
    {
        pSoundInfo   = psound;
        Key          = key;
        UseType      = use;
        PlayingCount = 0;
    }

    bool                    IsPlaying() const  { return PlayingCount > 0; }
    void                    IncPlayingCount()  { PlayingCount++; }
    void                    DecPlayingCount()  { PlayingCount--; GASSERT(PlayingCount>=0); }

    // Referenced image accessors.
    inline GSoundInfoBase*  GetSoundInfo() const
    {
        return pSoundInfo.GetPtr();
    }
    inline void             SetSoundInfo(GSoundInfoBase* psoundInfo)
    {
        pSoundInfo = psoundInfo;
    }

    // GImplement GFxResource interface.
    virtual GFxResourceKey  GetKey()                        { return Key; }
    virtual UInt            GetResourceTypeCode() const     { return MakeTypeCode(RT_SoundSample, UseType); }

    // Create a key for an image file.
    static  GFxResourceKey  CreateSoundFileKey(GFxSoundFileInfo* pfileInfo, GFxFileOpener* pfileOpener);

};

// ***** Sound resource creators

class GFxSoundResourceCreator : public GFxResourceData::DataInterface
{
    typedef GFxResourceData::DataHandle DataHandle;

    // Creates/Loads resource based on data and loading process
    virtual bool    CreateResource(DataHandle hdata, GFxResourceBindData *pbindData,
        GFxLoadStates *plp, GMemoryHeap* pbindHeap) const;

public:
    static  GFxResourceData CreateSoundResourceData(GSoundData* pimage);    

};

class GFxSoundFileResourceCreator : public GFxResourceData::DataInterface
{
    typedef GFxResourceData::DataHandle DataHandle;

    // Creates/Loads resource based on data and loading process
    virtual bool    CreateResource(DataHandle hdata, GFxResourceBindData *pbindData,
        GFxLoadStates *plp, GMemoryHeap* pbindHeap) const;
public:

    static  GFxResourceData CreateSoundFileResourceData(GFxSoundFileInfo * prfi);    
};

#endif // GFC_NO_SOUND

#endif // INC_GFXSOUNDRESOURCE_H
