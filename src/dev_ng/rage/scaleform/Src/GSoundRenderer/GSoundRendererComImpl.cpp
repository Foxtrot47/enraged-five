/**********************************************************************

Filename    :   GSoundRenderer.h
Content     :   Sound Player/Driver interface
Created     :   November, 2008
Authors     :   Maxim Didenko, Vladislav Merker

Copyright   :   (c) 1998-2009 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#include "GSoundRendererComImpl.h"
#ifndef GFC_NO_SOUND

GSoundSampleImplNode::GSoundSampleImplNode(GDListNode* plistRoot)
: GDListNode(plistRoot)
{
}

GSoundSampleImplNode::~GSoundSampleImplNode()
{
    if (pFirst)
        RemoveNode();
}

GSoundChannelImplNode::GSoundChannelImplNode(GDListNode* plistRoot)
: GDListNode(plistRoot)
{
}

GSoundChannelImplNode::~GSoundChannelImplNode()
{
    if (pFirst)
        RemoveNode();
}

#endif // GFC_NO_SOUND
