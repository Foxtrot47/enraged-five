/**********************************************************************

Filename    :   GSoundRenderer.cpp
Content     :   Sound Player/Driver interface
Created     :   January 23, 2007
Authors     :   Andrew Reisse

Notes       :   
History     :   

Copyright   :   (c) 1998-2006 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#include "GSoundRenderer.h"
#ifndef GFC_NO_SOUND

GSoundInfo::~GSoundInfo()
{
}

GSoundSample*   GSoundInfo::GetSoundSample(GSoundRenderer *pplayer)
{
    if (pSoundSample && pSoundSample->IsDataValid())
    {
        GASSERT(pSoundSample->GetSoundRenderer() == pplayer);
        return pSoundSample;
    }

    if (pSound)
    {
        if ((pSoundSample = *pplayer->CreateSampleFromData(pSound)))
        {
//            pTexture->AddChangeHandler(this);
        }
    }
    else
    {
        GFC_DEBUG_WARNING(1, "GSoundInfo::GetSoundSample failed, data not available. Please check GFxLoader::LoadSoundData flag.");
    }
    return pSoundSample;
}

bool    GSoundInfo::SetSoundData(GSoundDataBase* ps)
{
    pSound = ps;
    pSoundSample = NULL;
    return 1;
}

UInt    GSoundInfo::GetRate() const
{
    if (pSound)
        return pSound->GetRate();
    return 0;
}

void    GSoundInfo::ReleaseResource()
{
    pSoundSample = NULL;
}

#endif // GFC_NO_SOUND
