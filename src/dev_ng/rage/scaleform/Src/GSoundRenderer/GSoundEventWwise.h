/**********************************************************************

Filename    :   GSoundEventWwise.h
Content     :   AK Wwise/SoundFrame implementation of GSoundEvent
Created     :   March 2010
Authors     :   Vladislav Merker

Copyright   :   (c) 1998-2010 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/

#ifndef INC_GSOUNDEVENTWWISE_H
#define INC_GSOUNDEVENTWWISE_H

#include "GConfig.h"
#ifndef GFC_NO_SOUND

#include "GSoundEvent.h"

#if defined(GFC_SOUND_WWISE)

#include <AK/SoundEngine/Common/AkSoundEngine.h>

#if defined(GFC_OS_WIN32)

#include <AK/SoundFrame/SF.h>
#include "GUTF8Util.h"

//////////////////////////////////////////////////////////////////////////
//

class GSoundEventWwiseSF : public GSoundEvent, public AK::SoundFrame::IClient
{
public:
    GSoundEventWwiseSF() : pSoundFrame(NULL)
    {
        GASSERT(AK::SoundEngine::IsInitialized());

        AK::SoundFrame::Create(this, &pSoundFrame);
        GASSERT(pSoundFrame);
    }
    virtual ~GSoundEventWwiseSF()
    {
        if(pSoundFrame)
            pSoundFrame->Release();
    }

    virtual void PostEvent(GString event, GString eventId = "")
    {
        GUNUSED(eventId);

        if(pSoundFrame && pSoundFrame->IsConnected())
        {
            GUTF8Util::DecodeString(pWcharBuf, event.ToCStr(), WcharBufSize);
            LPCWSTR eventNameW = pWcharBuf;
            pSoundFrame->PlayEvents(&eventNameW, 1);
        }
    }

    virtual void SetParam(GString param, Float paramValue, GString eventId = "")
    {
        GUNUSED(eventId);

        if(pSoundFrame && pSoundFrame->IsConnected())
        {
            GUTF8Util::DecodeString(pWcharBuf, param.ToCStr(), WcharBufSize);
            LPCWSTR paramNameW = pWcharBuf;
            pSoundFrame->SetRTPCValue(
                paramNameW, (AkRtpcValue)paramValue,
                AK::SoundFrame::IGameObject::s_InvalidGameObject);
        }
    }

    virtual void OnConnect            (bool) {}
    virtual void OnEventNotif         (Notif, AkUniqueID) {}
    virtual void OnSoundObjectNotif   (Notif, AkUniqueID) {}
    virtual void OnStatesNotif        (Notif, AkUniqueID) {}
    virtual void OnSwitchesNotif      (Notif, AkUniqueID) {}
    virtual void OnGameParametersNotif(Notif, AkUniqueID) {}
    virtual void OnTriggersNotif      (Notif, AkUniqueID) {}
    virtual void OnEnvironmentsNotif  (Notif, AkUniqueID) {}
    virtual void OnGameObjectsNotif   (Notif, AkGameObjectID) {}

private:
    static const UInt8 WcharBufSize = 128;
    wchar_t pWcharBuf[WcharBufSize];

    AK::SoundFrame::ISoundFrame* pSoundFrame;
};
#endif

//////////////////////////////////////////////////////////////////////////
//

class GSoundEventWwise : public GSoundEvent
{
public:
    GSoundEventWwise() : ObjectID(AK_INVALID_GAME_OBJECT), PlayingID(AK_INVALID_PLAYING_ID)
    {
        GASSERT(AK::SoundEngine::IsInitialized());

        ObjectID = (AkGameObjectID)this;
        AKRESULT res = AK::SoundEngine::RegisterGameObj(ObjectID);
        GASSERT(res == AK_Success);
        GUNUSED(res);
    }
    virtual ~GSoundEventWwise()
    {
        if(ObjectID != AK_INVALID_GAME_OBJECT) {
            AKRESULT res = AK::SoundEngine::UnregisterGameObj(ObjectID);
            GASSERT(res == AK_Success);
            GUNUSED(res);
        }
    }

    virtual void PostEvent(GString event, GString eventId = "")
    {
        GUNUSED(eventId);
        PlayingID = AK::SoundEngine::PostEvent(event.ToCStr(), ObjectID);
        GASSERT(PlayingID != AK_INVALID_PLAYING_ID);
    }

    virtual void SetParam(GString param, Float paramValue, GString eventId = "")
    {
        GUNUSED(eventId);
        AK::SoundEngine::SetRTPCValue(param.ToCStr(), (AkRtpcValue)paramValue, ObjectID);
    }

private:
    AkGameObjectID ObjectID;
    AkPlayingID    PlayingID;
};


#endif // GFC_SOUND_WWISE
#endif // GFC_NO_SOUND

#endif // INC_GSOUNDEVENTWWISE_H
