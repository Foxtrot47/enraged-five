#if __WIN32
#pragma warning(disable: 4263)
#pragma warning(disable: 4100)
#pragma warning(disable: 4189)
#endif

#ifdef __SNC__
// member function doesn't override virtual function
#pragma diag_suppress 1011
// pointless comparison of signed and unsigned
#pragma diag_suppress 187
// parameter "something" was never references
#pragma diag_suppress 828
#endif


#if RSG_ORBIS
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif

#if __DEV
#define GFC_BUILD_DEBUG
#endif 

// Need to make sure winsock2.h gets #included before <windows.h> - otherwise we grab the winsock.h header file which conflicts.
#if __WIN32PC || RSG_DURANGO
# define STRICT
# pragma warning(push)
# pragma warning(disable: 4668)
# include <winsock2.h>
# include "../../../base/src/system/xtl.h"
# pragma warning(pop)
#elif __XENON
# include "../../../base/src/system/xtl.h"
#endif
