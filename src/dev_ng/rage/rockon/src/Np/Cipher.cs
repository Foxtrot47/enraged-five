﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.GlobalDb;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Np
{
    public class Cipher
    {
        //PURPOSE
        //  Gets the table name where ciphers are stored.
        public static string GetTableName()
        {
            return "NpCiphers";
        }

        //PURPOSE
        //  Creates table for storing ciphers.
        public static Error CreateTable()
        {
            Context ctx = NpContext.Create();

            try
            {
                string tableName = GetTableName();

                using (SqlConnectionHelper con = GlobalDbHelper.Open())
                {
                    if (!con.CheckTableExists(tableName))
                    {
                        string cmd = "CREATE TABLE " + tableName + Environment.NewLine;

                        cmd += string.Format(@"([Name] [varchar](255) NOT NULL,
                                                [CipherData] [varbinary](MAX) NOT NULL,
                                                [CreateDateTime] [datetime] NOT NULL DEFAULT(GETUTCDATE()),
                                               CONSTRAINT [PK_{0}] PRIMARY KEY CLUSTERED 
                                               ( [Name] ASC ) 
                                               WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                                               ) ON [PRIMARY]", tableName);

                        con.AdHocExecuteNonQuery(cmd);

                        if (!con.CheckTableExists(tableName))
                        {
                            return ctx.Error(ErrorCode.DbCommandFailed);
                        }
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Drops the cipher table.
        public static Error DropTable()
        {
            Context ctx = NpContext.Create();

            try
            {
                using (SqlConnectionHelper con = GlobalDbHelper.Open())
                {
                    string tableName = GetTableName();
                    if (con.CheckTableExists(tableName))
                    {
                        con.AdHocExecuteNonQuery("DROP TABLE " + tableName);

                        if (con.CheckTableExists(tableName))
                        {
                            return ctx.Error(ErrorCode.DbCommandFailed);
                        }
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Reads info for specified cipher, or all ciphers if name is null.
        public static Error QueryCipherInfo(string name,
                                            uint maxResults,
                                            out List<CipherInfo> infos)
        {
            Context ctx = NpContext.Create(name);
            infos = new List<CipherInfo>();

            try
            {
                string cmd = "SELECT * FROM " + GetTableName();

                SqlCmdParams cmdParams = new SqlCmdParams();

                if (name != null && name.Length > 0)
                {
                    cmd += " WHERE Name=@Name";
                    cmdParams.Add("@Name", name);
                }

                using (SqlConnectionHelper con = GlobalDbHelper.Open())
                {
                    using (DbDataReader reader = con.ExecuteReader(cmd, cmdParams))
                    {
                        while (reader.Read())
                        {
                            CipherInfo info = new CipherInfo();

                            info.Name = reader["Name"].ToString();
                            info.CreateDateTime = (DateTime)reader["CreateDateTime"];

                            long bufSize = reader.GetBytes(reader.GetOrdinal("CipherData"), 0, null, 0, 0);                            
                            byte[] buf = new byte[bufSize];
                            Int64 bytesRead = reader.GetBytes(reader.GetOrdinal("CipherData"), 0, buf, 0, buf.Length);
                            info.CipherData = (bytesRead > 0) ? buf : null;

                            infos.Add(info);

                            if ((maxResults != 0) && (infos.Count >= maxResults))
                            {
                                break;
                            }
                        }
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Creates a new cipher record in the table.
        public static Error CreateCipherFromFile(string cipherName,
                                                 string cipherDataFileName)
        {
            Context ctx = NpContext.Create();

            try
            {
                FileInfo fi = new FileInfo(cipherDataFileName);
                if (!fi.Exists)
                {
                    return ctx.Error(ErrorCode.DoesNotExist, "Could not find file " + cipherDataFileName);
                }

                FileStream fs = fi.OpenRead();
                byte[] cipherData = new byte[fs.Length];
                fs.Read(cipherData, 0, cipherData.Length);

                return CreateCipher(cipherName, cipherData);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Creates a new cipher record in the table.
        public static Error CreateCipher(string name,
                                         byte[] cipherData)
        {
            Context ctx = NpContext.Create();

            try
            {
                StringBuilder sb = new StringBuilder(string.Format("INSERT INTO {0}", GetTableName()));
                sb.Append(" (Name, CipherData)");
                sb.Append(" VALUES (@Name, @CipherData)");

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@Name", name);
                cmdParams.Add("@CipherData", cipherData);

                using (SqlConnectionHelper con = GlobalDbHelper.Open())
                {
                    con.ExecuteScalar(sb.ToString(), cmdParams);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Deletes the specified cipher from the table.
        public static Error DeleteCipher(string name,
                                         out uint numDeleted)
        {
            Context ctx = NpContext.Create();
            numDeleted = 0;

            try
            {
                string cmd = string.Format("DELETE FROM {0} WHERE Name=@Name",
                                           GetTableName());

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@Name", name);

                using (SqlConnectionHelper con = GlobalDbHelper.Open())
                {
                    numDeleted = (uint)con.ExecuteNonQuery(cmd, cmdParams);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }
}
