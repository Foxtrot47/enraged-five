﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Rockstar.Rockon.Np
{
    //PURPOSE
    //  Interface for using NP's Ticket Checking Module, which
    //  authenticates tickets sent from client consoles.  It lets
    //  us know that they're legitimate NP users.
    public class Tcm
    {
        #region ErrorCodes
        //Error codes
        public const int SCE_NET_NP_TCM_RET_OK = 0;                 //Normal termination
        public const int SCE_NET_NP_TCM_RET_OK_PARTIAL = 1;         //Normal termination, but other entitlements exist in addition to those in this ticket
        public const int SCE_NET_NP_TCM_RET_UNKNOWN = -1;           //Error of unknown cause
        public const int SCE_NET_NP_TCM_RET_BROKENTICKET = -2;      //Corrupt ticket information
        public const int SCE_NET_NP_TCM_RET_INVALIDCIPHER = -3;     //Corrupt Cipher Information
        public const int SCE_NET_NP_TCM_RET_BADSIGNATURE = -4;      //Invalid ticket signature
        public const int SCE_NET_NP_TCM_RET_BUFTOOSMALL = -5;       //Specified buffer is too small
        public const int SCE_NET_NP_TCM_RET_BADPASSPHRASE = -6;     //Invalid pass phrase
        public const int SCE_NET_NP_TCM_RET_NOSUITABLECIPHER = -7;  //Appropriate Cipher Information corresponding to ticket was not specified
        #endregion

        #region Constants
        //Secret value used by NP.
        public static byte[] PassPhrase = Encoding.UTF8.GetBytes("Ru1DAqKh");

        //Size constants
        public const int SCE_NET_NP_CIPHER_INFORMATION_SIZE = 1024;
        public const int SCE_NET_NP_COOKIE_MAX_SIZE = 1024;
        public const int SCE_NET_NP_ENTITLEMENT_ID_SIZE = 32;
        public const int SCE_NET_NP_ONLINEID_MAX_LENGTH = 16;
        public const int SCE_NET_NP_PREPARED_CIPHER_INFORMATION_SIZE = 1024;
        public const int SCE_NET_NP_ROLE_DOMAIN_SIZE = 24;
        public const int SCE_NET_NP_SERVICE_ID_SIZE = 24;
        public const int SCE_NET_NP_SUBJECT_DOMAIN_SIZE = 4;
        public const int SCE_NET_NP_SUBJECT_ONLINE_ID_SIZE = 32;
        public const int SCE_NET_NP_SUBJECT_REGION_SIZE = 4;
        public const int SCE_NET_NP_TCM_NPID_RESOURCE_MAX_LENGTH = 4;
        public const int SCE_NET_NP_TICKET_SERIAL_ID_SIZE = 20;
        #endregion

        #region Structures
        public struct SceNetNpCipherInformation
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = SCE_NET_NP_CIPHER_INFORMATION_SIZE)]
            public byte[] data;
        }

        public struct SceNetNpCookie
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = SCE_NET_NP_COOKIE_MAX_SIZE)]
            public byte[] data;
            
            public uint size;
        };

        public struct SceNetNpEntitlement
        {
            public SceNetNpEntitlementId id;
            public Int64 created_date;
            public Int64 expired_date;
            public uint type;
            public int remaining_count;
            public uint consumed_count;
        };
        
        public struct SceNetNpEntitlementId
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = SCE_NET_NP_ENTITLEMENT_ID_SIZE)]
            public byte[] data;
        };

        public struct SceNetNpPreparedCipherInformation
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = SCE_NET_NP_PREPARED_CIPHER_INFORMATION_SIZE)]
            public byte[] data;
        }

        public struct SceNetNpRole
        {
            public int id;
            public SceNetNpRoleDomain domain;
        }

        public struct SceNetNpRoleDomain
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = SCE_NET_NP_ROLE_DOMAIN_SIZE)]
            public byte[] data;
        }

        public struct SceNetNpServiceId
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = SCE_NET_NP_SERVICE_ID_SIZE)]
            public byte[] data;
        }

        public struct SceNetNpSubject
        {
            public UInt64 id;
            public SceNetNpSubjectOnlineId handle;
            public SceNetNpSubjectRegion region;
            public SceNetNpSubjectDomain domain;
            public uint status;
            public Int64 duration;
        };

        public struct SceNetNpSubjectDomain
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = SCE_NET_NP_SUBJECT_DOMAIN_SIZE)]
            public byte[] data;
        }

        public struct SceNetNpSubjectOnlineId
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = SCE_NET_NP_SUBJECT_ONLINE_ID_SIZE)]
            public byte[] data;
        };

        public struct SceNetNpSubjectRegion
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = SCE_NET_NP_SUBJECT_REGION_SIZE)]
            public byte[] data;
        }

        public struct SceNetNpTcmNpIdResource
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = SCE_NET_NP_TCM_NPID_RESOURCE_MAX_LENGTH)]
            public byte[] data;
        }

        public struct SceNetNpTicketSerialId
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = SCE_NET_NP_TICKET_SERIAL_ID_SIZE)]
            public byte[] data;
        }

        public struct SceNpId
        {
            public SceNpOnlineId handle;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public byte[] opt;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public byte[] reserved;
        }

        public struct SceNpOnlineId
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = SCE_NET_NP_ONLINEID_MAX_LENGTH)]
            public byte[] data;

            public char term;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public byte[] dummy;
        }
        #endregion

        #region Functions
        [DllImport("np_tcm.dll")]
        public static extern int SceNetNpTcmGetInformationWithoutVerify(
            byte[] ticket_data,                     //const unsigned char *ticket_data
            uint ticket_size,                       //size_t ticket_size
            ref byte major_version,                 //SceNetNpMajorVersion *major_version
            ref byte minor_version,                 //SceNetNpMinorVersion *minor_version
            ref SceNetNpTicketSerialId serial_id,   //SceNetNpTicketSerialId *serial_id
            ref Int64 issued_date,                  //SceNetNpTime *issued_date
            ref Int64 not_on_or_after_date,         //SceNetNpTime *not_on_or_after_date
            ref SceNetNpServiceId service_id,       //SceNetNpServiceId *service_id
            ref uint entitlement_count,             //uint32_t *entitlement_count
            ref uint role_count                     //uint32_t *role_count
            );

        [DllImport("np_tcm.dll")]
        public static extern int SceNetNpTcmGetNpId(
            byte[] ticket_data,                     //const unsigned char *ticket_data,
            uint ticket_size,                       //size_t ticket_size,
            ref SceNetNpTcmNpIdResource resource,   //const SceNetNpTcmNpIdResource *resource,
            ref SceNpId npid                        //SceNpId *npid
            );

        [DllImport("np_tcm.dll")]
        public static extern int SceNetNpTcmPrepareCipherInformation(
            byte[] pass_phrase,                                                 //const unsigned char *pass_phrase,
            ref SceNetNpCipherInformation cipher_information,                   //const SceNetNpCipherInformation *cipher_information,
            ref SceNetNpPreparedCipherInformation prepared_cipher_information   //SceNetNpPreparedCipherInformation *prepared_cipher_information
            );

        [DllImport("np_tcm.dll")]
        public static extern int SceNetNpTcmVerifySignature(
            SceNetNpPreparedCipherInformation[] prepared_cipher_information,    //const SceNetNpPreparedCipherInformation *prepared_cipher_information,
            uint prepared_cipher_information_count,                             //uint32_t prepared_cipher_information_count,
            byte[] ticket_data,                                                 //const unsigned char *ticket_data,
            uint ticket_size                                                    //size_t ticket_size
            );

        [DllImport("np_tcm.dll")]
        public static extern int SceNetNpTcmGetInformation(
            SceNetNpPreparedCipherInformation[] prepared_cipher_information,    //SceNetNpPreparedCipherInformation *prepared_cipher_information
            uint prepared_cipher_information_count,                             //uint32_t prepared_cipher_information_count,
            byte[] ticket_data,                                                 //const unsigned char *ticket_data,
            uint ticket_size,                                                   //size_t ticket_size
            ref byte major_version,                 //SceNetNpMajorVersion *major_version
            ref byte minor_version,                 //SceNetNpMinorVersion *minor_version
            ref uint issuer_id,                     //SceNetNpIssuerId *issuer_id,
            ref SceNetNpTicketSerialId serial_id,   //SceNetNpTicketSerialId *serial_id
            ref Int64 issued_date,                  //SceNetNpTime *issued_date
            ref Int64 not_on_or_after_date,         //SceNetNpTime *not_on_or_after_date
            ref SceNetNpServiceId service_id,       //SceNetNpServiceId *service_id
            ref SceNetNpSubject subject,            //SceNetNpSubject *subject,
            SceNetNpEntitlement[] entitlements,     //SceNetNpEntitlement* entitlements,
            ref uint entitlement_count,             //uint32_t *entitlement_count
            SceNetNpRole[] roles,                   //SceNetNpRole *roles,
            ref uint role_count,                    //uint32_t *role_count
            ref SceNetNpCookie cookie               //SceNetNpCookie* cookie
            );
        #endregion
    }
}
