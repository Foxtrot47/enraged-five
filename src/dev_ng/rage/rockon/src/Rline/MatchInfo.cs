using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Rockstar.Rockon.Rline
{
    //PURPOSE
    //  Describes an entry in the matches DB.
    public class MatchInfo
    {
        public int Id;
        public bool IsArbitrated;
        public DateTime CreateDateTime;
        public DateTime StartDateTime = DateTime.MinValue;    //Default value indicates NULL in DB column
        public DateTime EndDateTime = DateTime.MinValue;      //Default value indicates NULL in DB column

        public bool Started() { return (StartDateTime != DateTime.MinValue); }
        public bool Ended() { return (EndDateTime != DateTime.MinValue); }
    }
}
