using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Rockstar.Rockon.Diag;

namespace Rockstar.Rockon.Rline
{
    public class Telemetry
    {
        //PURPOSE
        //  Stub function for processing a telemetry submission.  In the future,
        //  we'll likely decide what to do with it based on the title ID and 
        //  possibly the submission header.
        public static Error ProcessSubmission(int titleId,
                                              Int64 accountId,
                                              string rson)
        {
            Context ctx = RlineContext.Create("titleId={0} acctId={1}", titleId, accountId);

            return ctx.Success2("Received: " + rson);
        }
    }
}
