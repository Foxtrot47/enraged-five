using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

//FIXME: For all queries that can return more than one result, add a maxResults param.
//FIXME: Change all SELECT * queries to reference exact columns we want, apparently using *
//       is a slight performance hit.  For LBs, this means creating SELECT statements
//       for the various query methods.
//FIXME: How do we make it so the views only refresh periodically, and as needed?

namespace Rockstar.Rockon.Rline
{
    public class Leaderboard
    {
        public static string GetLbMetadataTableName()
        {
            //string s = SqlHelper.UseSqlServer ? "dbo." : "";
            string s = "";

            s += "Lb_Metadata"; //NOTE: Underscore is important, so it will be dropped by DropAll()

            return s;
        }

        public static string GetLbTableName(uint lbId)
        {
            //string s = SqlHelper.UseSqlServer ? "dbo." : "";
            string s = "";

            s += string.Format("Lb_0x{0}", lbId.ToString("X8"));

            return s;
        }

        public static string GetLbViewName(uint lbId)
        {
            //string s = SqlHelper.UseSqlServer ? "dbo." : "";
            string s = "";

            s += "View_" + GetLbTableName(lbId);

            return s;
        }

        public static string GetColName(uint colId)
        {
            return string.Format("Col_0x{0}", colId.ToString("X8"));
        }

        public static string GetRatingColName()
        {
            return string.Format("Col_0x0000FFFE");
        }

        public static bool CreateTables(SqlConnectionHelper con,
                                        IEnumerable<LeaderboardInfo> lbInfos)
        {
            if (!DropAll(con))
            {
                return false;
            }

            foreach (LeaderboardInfo lbInfo in lbInfos)
            {
                if (!CreateTable(con, lbInfo))
                {
                    return false;
                }
            }

            return true;
        }

        public static bool DropAll(SqlConnectionHelper con)
        {
            //Delete leaderboard views
            {
                List<string> names = new List<string>();

                string cmd = "SELECT TABLE_SCHEMA,TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME LIKE 'View_Lb_%'";
                using (DbDataReader reader = con.AdHocExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        names.Add(reader.GetString(1));
                    }
                }

                foreach (string name in names)
                {
                    con.AdHocExecuteNonQuery("DROP VIEW " + name);
                }
            }

            //Delete leaderboard tables
            {
                List<string> names = new List<string>();

                string cmd = "SELECT TABLE_SCHEMA,TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'Lb_%'";
                using (DbDataReader reader = con.AdHocExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        names.Add(reader.GetString(1));
                        //names.Add(reader.GetString(0) + "." + reader.GetString(1));
                    }
                }

                foreach (string name in names)
                {
                    con.AdHocExecuteNonQuery("DROP TABLE " + name);
                }
            }

            //Delete LB metadata table
            if (con.CheckTableExists(GetLbMetadataTableName()))
            {
                con.AdHocExecuteNonQuery("DROP TABLE " + GetLbMetadataTableName());
            }

            return true;
        }

        public static bool CreateTable(SqlConnectionHelper con,
                                       LeaderboardInfo lbInfo)
        {
            if (SqlHelper.UseSqlServer)
            {
                //These are required in order to create indexed views.
                con.AdHocExecuteNonQuery("SET ANSI_NULLS ON");
                con.AdHocExecuteNonQuery("SET QUOTED_IDENTIFIER ON");
            }

            string cmd;

            try
            {
                //Create the LB metadata table if it doesn't exist.
                if (!con.CheckTableExists(GetLbMetadataTableName()))
                {
                    cmd = GetCreateMetaCommand();
                    con.AdHocExecuteNonQuery(cmd);
                }

                //Create the leaderboard table if it doesn't exist.
                cmd = GetCreateCommand(lbInfo);
                cmd += ";" + GetMetaEntryCommand(lbInfo);
                con.AdHocExecuteNonQuery(cmd);

                cmd = GetViewCreateCommand(lbInfo);
                con.AdHocExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return true;
        }

        static string GetCreateMetaCommand()
        {
            string cmd = "CREATE TABLE " + GetLbMetadataTableName() + Environment.NewLine;

            cmd += @"([LbId] [int] NOT NULL,
                      [Name] [varchar](50) NOT NULL,
                      [IsArbitrated] [bit] NOT NULL,
                      [ColumnSchema] [varchar](3000) NOT NULL,
                      [ResetMethod] char(32) NOT NULL,
                      CONSTRAINT PK_" + GetLbMetadataTableName() + " PRIMARY KEY ([LbId]))";

            return cmd;
        }

        static string GetMetaEntryCommand(LeaderboardInfo lbInfo)
        {
            string cmd = "INSERT INTO " + GetLbMetadataTableName() + Environment.NewLine;

            cmd += "(LbId,Name,IsArbitrated,ColumnSchema,ResetMethod)" + Environment.NewLine;
            cmd += "VALUES({0},'{1}',{2},'{3}','{4}')";
            cmd = String.Format(cmd,
                                lbInfo.Id,
                                lbInfo.Name,
                                lbInfo.IsArbitrated ? 1 : 0,
                                lbInfo.GetColumnSchema(),
                                lbInfo.ResetMethod);

            return cmd;
        }

        static string GetCreateCommand(LeaderboardInfo lbInfo)
        {
            string tblName = GetLbTableName(lbInfo.Id);

            string cmd = "CREATE TABLE " + tblName + " " + Environment.NewLine;

            cmd += "(AccountId bigint NOT NULL, DisplayName varchar(20) NOT NULL";

            foreach (LeaderboardColumn lbc in lbInfo.Cols)
            {
                cmd += String.Format(",{0} {1} NOT NULL DEFAULT(0)",
                                     GetColName(lbc.Id),
                                     Xbl.GetSqlDataType(XblProperty.GetTypeFromId(lbc.PropId)));
            }

            cmd += ", CONSTRAINT PK_" + tblName + " PRIMARY KEY (AccountId))";

            cmd = String.Format(cmd, tblName);

            return cmd;
        }

        static string GetViewCreateCommand(LeaderboardInfo lbInfo)
        {
            string viewName = GetLbViewName(lbInfo.Id);
            string tblName = GetLbTableName(lbInfo.Id);

            string cmd = string.Format("CREATE VIEW {0} AS SELECT ROW_NUMBER() OVER(ORDER BY {1} DESC) AS Row, AccountId, DisplayName", viewName, GetRatingColName());

            foreach (LeaderboardColumn lbc in lbInfo.Cols)
            {
                cmd += String.Format(", {0}", GetColName(lbc.Id));
            }

            cmd += string.Format(" FROM {0}", tblName);

            return cmd;
        }

        //PURPOSE
        //  Get info for specified LB, or all LBs if ID is zero.
        public static Error QueryLbInfo(SqlConnectionHelper con,
                                        UInt32 lbId,
                                        out List<LeaderboardInfo> lbInfos)
        {
            Context ctx = RlineContext.Create("{0}", lbId);
            lbInfos = null;

            try
            {
                string s = "SELECT * FROM " + Leaderboard.GetLbMetadataTableName();

                if (lbId != 0)
                {
                    s += String.Format(" WHERE LbId={0}", lbId);
                }

                lbInfos = new List<LeaderboardInfo>();

                using(DbDataReader reader = con.AdHocExecuteReader(s))
                {
                    while (reader.Read())
                    {
                        LeaderboardInfo lbInfo = new LeaderboardInfo();

                        lbInfo.Id = (uint)((int)reader["LbId"]);
                        Trace.WriteLine("Reading leaderboard " + lbInfo.Id);

                        lbInfo.Name = reader["Name"].ToString();
                        lbInfo.IsArbitrated = (bool)reader["IsArbitrated"];

                        if(lbInfo.CreateColumnsFromSchema(reader["ColumnSchema"].ToString()))
                        {
                            lbInfos.Add(lbInfo);
                        }
                        else
                        {
                            throw (new Exception(string.Format("Lb_0x{0} has invalid ColumnSchema string; discarding", Leaderboard.GetLbTableName(lbInfo.Id))));
                        }
                    }

                    reader.Close();
                }

                return ctx.Success2("Read {0} LB infos", lbInfos.Count);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Reads rows by rank.
        public static Error ReadByRank(SqlConnectionHelper con,
                                       uint lbId,
                                       uint startingRank,
                                       uint maxRows,
                                       out List<LeaderboardRow> rows)
        {
            Context ctx = RlineContext.Create("lbId={0}, startingRank={1}, maxRows={2}", lbId, startingRank, maxRows);
            rows = null;

            try
            {
                if (startingRank == 0 || maxRows == 0)
                {
                    return ctx.Error(ErrorCode.InvalidArguments);
                }

                string s = String.Format("SELECT * FROM {0} WHERE Row >= {1} AND Row < {2}",
                                         Leaderboard.GetLbViewName(lbId),
                                         startingRank,
                                         startingRank + maxRows);

                Error err = CommonReadRows(con, s, maxRows, out rows);
                if (err != null)
                {
                    return ctx.Error(err);
                }

                return ctx.Success2("Read {0} rows", rows.Count);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Reads row for a specific gamer.
        public static Error ReadByGamer(SqlConnectionHelper con,
                                        uint lbId,
                                        Int64 accountId,
                                        out LeaderboardRow row)
        {
            Context ctx = RlineContext.Create("lbId={0}, accountId={1}", lbId, accountId);
            row = null;

            try
            {
                string s = String.Format("SELECT * FROM {0} WHERE AccountId={1}",
                                         Leaderboard.GetLbViewName(lbId),
                                         accountId);

                List<LeaderboardRow> rows = new List<LeaderboardRow>();

                Error err = CommonReadRows(con, s, 1, out rows);
                if(err != null)
                {
                    return ctx.Error(err);
                }

                if (rows.Count > 0)
                {
                    row = rows[0];
                }

                return ctx.Success2((row == null) ? "Found row" : "No row found");
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Reads gamer's row and rows around it.
        public static Error ReadByGamerRadius(SqlConnectionHelper con,
                                              uint lbId,
                                              Int64 accountId,
                                              uint radius,
                                              out List<LeaderboardRow> rows)
        {
            Context ctx = RlineContext.Create("lbId={0}, accountId={1}, radius={2}", lbId, accountId, radius);
            rows = null;

            try
            {
                UInt64 gamerRank = 0;

                {
                    string s = String.Format("SELECT [Row] FROM {0} WHERE AccountId={1}",
                                             Leaderboard.GetLbViewName(lbId),
                                             accountId);

                    using(DbDataReader reader = con.AdHocExecuteReader(s))
                    {
                        while (reader.Read())
                        {
                            gamerRank = (UInt64)((Int64)reader["Row"]);
                            break;
                        }

                        reader.Close();
                    }
                }

                {
                    string s = String.Format("SELECT * FROM {0} WHERE Row >= {1} AND Row <= {2}",
                                             Leaderboard.GetLbViewName(lbId),
                                             (gamerRank > radius) ? (gamerRank - radius) : 1,
                                             gamerRank + radius);

                    Error err = CommonReadRows(con, s, 1 + (radius*2), out rows);
                    if (err != null)
                    {
                        return ctx.Error(err);
                    }
                }

                return ctx.Success2("Read {0} rows", rows.Count);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }      

        //PURPOSE
        //  Accepts stat submissions from a gamer.  The submission can be to multiple
        //  leaderboards, and may be for either the submitting gamer or other gamers
        //  in the match.
        public static Error WriteStats(SqlConnectionHelper con,
                                       int matchId,
                                       Int64 accountId,
                                       List<LeaderboardUpdate> updates)
        {
            Context ctx = RlineContext.Create("matchId={0}, accountId={1}, numUpdates={2}", matchId, accountId, updates.Count);

            try
            {
                Error err = null;

                //Get the match info.
                MatchInfo matchInfo;

                if (null != (err = Match.QueryMatch(con, matchId, out matchInfo)))
                {
                    return ctx.Error(err);
                }
                else if (matchInfo == null)
                {
                    return ctx.Error(ErrorCode.InvalidArguments);
                }

                //If the match hasn't started, ignore the submission.
                if (!matchInfo.Started())
                {
                    return ctx.Error(RlineErrorCode.MatchNotStarted);
                }

                //Get the match gamers. In a nonarbitrated match, the gamer should be submitting only for 
                //themselves.  In an arbitrated match, they should be submitting for all match gamers.
                List<MatchMemberInfo> matchMemberInfos;

                if (null != (err = MatchMember.QueryMatchMember(con, matchId, 0, out matchMemberInfos)))
                {
                    return ctx.Error(err);
                }

                //Make sure the submitting gamer is part of the match
                if (-1 == MatchMember.GetMemberIndex(accountId, matchMemberInfos))
                {
                    return ctx.Error(RlineErrorCode.NotInMatch, "Account {0} is not in the match", accountId);
                }

                //Determine the table we're writing to.
                string tableName = matchInfo.IsArbitrated ? "ArbSubmissions" : "Submissions";

                //For each LB being updated, create a record in the submissions table.
                //The background arbitration and commit processes will process these later.
                foreach (LeaderboardUpdate u in updates)
                {
                    int memberIndex = MatchMember.GetMemberIndex(u.AccountId, matchMemberInfos);

                    if (memberIndex == -1)
                    {
                        return ctx.Error(RlineErrorCode.NotInMatch, "Account {0} is not in the match", u.AccountId);
                    }

                    //Verify the gamer doesn't already have an entry for this combination of lbId+matchId+submitAccountId+targetAccountId
                    //NOTE: This should never happen in practice, so we may have to drop this for performance reasons.
                    {
                        string s = String.Format("SELECT COUNT(1) FROM {0} WHERE LbId={1} AND MatchId={2} AND SubmitterAccountId={3} AND TargetAccountId={4}",
                                                 tableName, u.LbId, matchId, accountId, u.AccountId);

                        if ((int)con.AdHocExecuteScalar(s) != 0)
                        {
                            return ctx.Error(ErrorCode.AlreadyExists, 
                                             "Already have submission for SubAcct={0}  TargAcct={1}  MatchId={2}  LbId={3}",
                                             accountId, u.AccountId, matchId, u.LbId);
                        }
                    }

                    err = Submission.CreateRawSubmission(con,
                                                            matchInfo.IsArbitrated,
                                                            matchId,
                                                            accountId,
                                                            u.AccountId,
                                                            matchMemberInfos[memberIndex].DisplayName,
                                                            u.LbId,
                                                            u.Props);

                    if (err != null)
                    {
                        err.Msg += "(CreateRawSubmission)";
                        return ctx.Error(err);
                    }

                    //If the match is arbitrated and hasn't ended, end it now.
                    //TODO: When do nonarbitrated matches get ended?  Do we need a cleanup process?  When do GS matchless sessions get ended?
                    if (matchInfo.IsArbitrated && !matchInfo.Ended())
                    {
                        ctx.Debug2("Ending match...");

                        if (null != (err = Match.EndMatch(con, matchId)))
                        {
                            return ctx.Error(err);
                        }
                    }
                }

                return ctx.Success2();
            }
            catch(Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Convenience method for row-reading methods.
        private static Error CommonReadRows(SqlConnectionHelper con,
                                            string cmd,
                                            uint maxRows,
                                            out List<LeaderboardRow> rows)
        {
            Context ctx = RlineContext.Create();
            rows = null;

            try
            {
                rows = new List<LeaderboardRow>();

                using(DbDataReader reader = con.AdHocExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        LeaderboardRow r = new LeaderboardRow();

                        r.Rank = (UInt64)((Int64)reader["Row"]);
                        r.AccountId = (Int64)reader["AccountId"];
                        r.DisplayName = reader["DisplayName"].ToString();

                        for (int i = 3; i < reader.FieldCount; i++)
                        {
                            r.ColumnValues.Add(reader[i]);
                        }

                        rows.Add(r);

                        if ((maxRows > 0) && (rows.Count >= maxRows))
                        {
                            break;
                        }
                    }

                    reader.Close();
                }

                return ctx.Success2("Read {0} rows}", rows.Count);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }
}
