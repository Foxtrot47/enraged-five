using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.Caching;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Rline
{
    public class Mailbox
    {
        //TODO: Make configuarable
        private const int CACHE_ABSOLUTE_EXPIRATION_MINUTES = 60 * 24; //Daily

        public const Int64 SYSTEM_ID = 0;       //Reserved for System account, which can inherently do anything.
        public const Int64 BROADCAST_ID = -1;   //Msgs sent to this ID are considered broadcasts.
        public const Int64 ANY_ID = -1;         //Used for methods where you want to read records
                                                //to/from a specific ID, or any ID.

        public const string MAILBOX_TABLE_PREFIX = "rlMailbox_";

        //PURPOSE
        //  Gets the table name for specified mailbox.
        public static string GetTableName(string mailboxName)
        {
            return MAILBOX_TABLE_PREFIX + mailboxName;
        }

        //PURPOSE
        //  Gets role override mode enum from string.
        public static MailboxRoleOverrideMode MailboxRoleOverrideModeFromString(string name)
        {
            if (name == null || name.Length == 0)
            {
                return MailboxRoleOverrideMode.None;
            }

            switch (name.ToLower())
            {
                case "none": return MailboxRoleOverrideMode.None;
                case "replace": return MailboxRoleOverrideMode.Replace;
                case "add": return MailboxRoleOverrideMode.Add;
                case "remove": return MailboxRoleOverrideMode.Remove;
                default: throw (new Exception(string.Format("Unknown role override mode: {0}", name)));
            }
        }

        //PURPOSE
        //  Gets role override mode enum from string.
        public static string MailboxRoleOverrideModeToString(MailboxRoleOverrideMode mode)
        {
            return mode.ToString();
        }

        //PURPOSE
        //  Creates a table for a mailbox, and an entry for it in the metadata table.
        public static Error CreateMailbox(TitleInfo titleInfo,
                                          MailboxInfo mailboxInfo)

        {
            Context ctx = RlineContext.Create(mailboxInfo.Name);
            Error err = null;

            try
            {
                using (SqlTransactionScope ts = new SqlTransactionScope())
                {
                    using (SqlConnectionHelper con = Title.Open(titleInfo))
                    {
                        if (SqlHelper.UseSqlServer)
                        {
                            //These are required in order to create indexed views.
                            //TODO: Can we get rid of these?
                            con.AdHocExecuteNonQuery("SET ANSI_NULLS ON");
                            con.AdHocExecuteNonQuery("SET QUOTED_IDENTIFIER ON");
                        }

                        //Create the table
                        string tableName = GetTableName(mailboxInfo.Name);
                        string s = "CREATE TABLE " + tableName;

                        s += string.Format(@"([RecordId] [bigint] IDENTITY(1,1) NOT NULL,
                                           [SenderId] [bigint] NOT NULL,
                                           [SenderDisplayName] [varchar](50) NOT NULL,
                                           [ReceiverId] [bigint] NOT NULL,
                                           [SendDateTime] [datetime] NOT NULL DEFAULT(GETUTCDATE()),
                                           [UserData] [varbinary](MAX) NULL,
                                           [MsgData] [varbinary](MAX) NOT NULL,
                                           [ExpirationDateTime] [datetime] NOT NULL DEFAULT(GETUTCDATE()),
                                           CONSTRAINT [PK_{0}] PRIMARY KEY CLUSTERED 
                                           (
                                            [RecordId] ASC
                                           )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                                           ) ON [PRIMARY]", tableName);
                        con.AdHocExecuteNonQuery(s);

                        //Create additional indices
                        s = string.Format(@"CREATE NONCLUSTERED INDEX [IX_{0}_ReceiverId] ON [dbo].[{0}] 
                                          ( [ReceiverId] ASC )
                                          WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]",
                                          tableName);
                        con.AdHocExecuteNonQuery(s);

                        s = string.Format(@"CREATE NONCLUSTERED INDEX [IX_{0}_SenderId] ON [dbo].[{0}] 
                                          ( [SenderId] ASC )
                                          WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]",
                                          tableName);
                        con.AdHocExecuteNonQuery(s);


                        //Make sure support tables exist.
                        if (null != (err = MailboxMetadata.CreateTable(con)))
                        {
                            return ctx.Error(err);
                        }

                        if (null != (err = MailboxRoles.CreateTable(con)))
                        {
                            return ctx.Error(err);
                        }

                        if (mailboxInfo.RoleOverrideMode != MailboxRoleOverrideMode.None)
                        {
                            if (null != (err = MailboxRoleOverrides.CreateTable(con, mailboxInfo.Name)))
                            {
                                return ctx.Error(err);
                            }
                        }


                        //Create an entry in the metadata table.
                        if (null != (err = MailboxMetadata.CreateMailboxInfo(con, mailboxInfo)))
                        {
                            return ctx.Error(err);
                        }

                        ts.Complete();
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Drops a specific mailbox, removing its entry from the metadata table as well.
        public static Error DeleteMailbox(TitleInfo titleInfo,
                                          string mailboxName)
        {
            Context ctx = RlineContext.Create(mailboxName);
            Error err = null;

            try
            {
                using (SqlTransactionScope ts = new SqlTransactionScope())
                {
                    using (SqlConnectionHelper con = Title.Open(titleInfo))
                    {
                        //Drop table
                        string tableName = GetTableName(mailboxName);
                        if (con.CheckTableExists(tableName))
                        {
                            con.AdHocExecuteNonQuery("DROP TABLE " + tableName);
                        }

                        //Drop its role override table, if any
                        if (null != (err = MailboxRoleOverrides.DropTable(con, mailboxName)))
                        {
                            return ctx.Error(err);
                        }

                        //Delete its record in the metadata table. We do this even if the table didn't 
                        //exist, just in case they somehow got out of sync.
                        if (null != (err = MailboxMetadata.DeleteMailboxInfo(con, mailboxName)))
                        {
                            return ctx.Error(err);
                        }
                    }

                    ts.Complete();
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Drops all mailbox tables, including metadata, roles, and role overrides.
        public static void DropAll(SqlConnectionHelper con)
        {
            List<string> names = new List<string>();

            string cmd = string.Format("SELECT TABLE_SCHEMA,TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE '{0}%'", MAILBOX_TABLE_PREFIX);
            using (DbDataReader reader = con.AdHocExecuteReader(cmd))
            {
                while (reader.Read())
                {
                    names.Add(reader.GetString(1));
                }
            }

            foreach (string name in names)
            {
                con.AdHocExecuteNonQuery("DROP TABLE " + name);
            }
        }
             
        //PURPOSE
        //  Queries the number of records in a mailbox matching specified 
        //  filters.  A -1 sender or receiver ID means "any".
        public static Error QueryNumRecords(int titleId,
                                            Int64 accountId,
                                            string mailboxName,
                                            Int64 senderId,
                                            Int64 receiverId,
                                            Int64 startRecordId,
                                            Int64 endRecordId,
                                            bool includeExpired,
                                            out int numRecords)
        {
            Context ctx = RlineContext.Create("name=\"{0}\", sndr={1}, rcvr={2}",
                                           mailboxName, senderId, receiverId);
            Error err = null;
            numRecords = 0;

            try
            {
                MailboxInfo mailboxInfo = MailboxMetadata.GetCachedMailboxInfo(titleId, mailboxName);
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                //Check permissions
                err = CommonCheckReadPermissions(titleInfo,
                                                 mailboxInfo,
                                                 accountId,
                                                 senderId,
                                                 receiverId);
                if (err != null)
                {
                    return ctx.Error(err);
                }

                //Compose filter string and parameters
                SqlFilterStringBuilder fsb = new SqlFilterStringBuilder();
                SqlCmdParams cmdParams = new SqlCmdParams();

                err = CommonBuildReadFilter(mailboxInfo,
                                            accountId,
                                            senderId,
                                            receiverId,
                                            startRecordId,
                                            endRecordId,
                                            includeExpired,
                                            ref fsb,
                                            ref cmdParams);
                if (err != null)
                {
                    return ctx.Error(err);
                }

                //Do the query
                string cmd = string.Format("SELECT COUNT(1) FROM {0}{1}",
                                           GetTableName(mailboxName),
                                           fsb.ToString());

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    numRecords = (int)con.ExecuteScalar(cmd, cmdParams);

                    if (numRecords < 0)
                    {
                        numRecords = 0;
                        return ctx.Error(ErrorCode.UnknownError);
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Reads messages matching the specified conditions.  The user can 
        //  specify whether or not to download the user and msg data, what
        //  order to receive the messages in, and whether to include expired
        //  messages.
        public static Error QueryRecords(int titleId,
                                         Int64 accountId,
                                         string mailboxName,
                                         Int64 senderId,
                                         Int64 receiverId,
                                         Int64 startRecordId,
                                         Int64 endRecordId,
                                         uint maxRecords,
                                         bool readUserData,
                                         bool readMsgData,
                                         bool oldestFirst,
                                         bool includeExpired,
                                         out List<MailboxRecord> records)
        {
            Context ctx = RlineContext.Create();
            Error err = null;
            records = null;

            try
            {
                MailboxInfo mailboxInfo = MailboxMetadata.GetCachedMailboxInfo(titleId, mailboxName);
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                //Check permissions
                err = CommonCheckReadPermissions(titleInfo,
                                                 mailboxInfo,
                                                 accountId,
                                                 senderId,
                                                 receiverId);
                if (err != null)
                {
                    return ctx.Error(err);
                }

                //Compose filter string and parameters
                SqlFilterStringBuilder fsb = new SqlFilterStringBuilder();
                SqlCmdParams cmdParams = new SqlCmdParams();

                err = CommonBuildReadFilter(mailboxInfo,
                                            accountId,
                                            senderId,
                                            receiverId,
                                            startRecordId,
                                            endRecordId,
                                            includeExpired,
                                            ref fsb,
                                            ref cmdParams);
                if (err != null)
                {
                    return ctx.Error(err);
                }

                //Do the query
                err = CommonQueryRecords(titleInfo,
                                         mailboxInfo,
                                         fsb,
                                         cmdParams,
                                         maxRecords,
                                         readUserData,
                                         readMsgData,
                                         oldestFirst,
                                         out records);
                if (err != null)
                {
                    return ctx.Error(err);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Reads only the message data for a particular record.  It's main
        //  use is for apps that query records without fetching the message data,
        //  and then download the data for a specific chosen record.
        //TODO: Need to rewrite this to verify user has permission to read the data.
        //      Fetch the record's senderId and receiverId as well to test.
        public static Error QueryMsgData(int titleId,
                                         Int64 accountId,
                                         string mailboxName,
                                         Int64 senderId,
                                         Int64 receiverId,
                                         Int64 recordId,
                                         out byte[] msgData)
        {
            Context ctx = RlineContext.Create();
            Error err = null;
            msgData = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);
                MailboxInfo mailboxInfo = MailboxMetadata.GetCachedMailboxInfo(titleId, mailboxName);

                //Check permissions
                err = CommonCheckReadPermissions(titleInfo, mailboxInfo, accountId, senderId, receiverId);
                if (err != null)
                {
                    return ctx.Error(err);
                }

                //Create filter and cmd params.  Note that we filter on sender/receiver as
                //well just as a sanity check, in case those params are not what is actually
                //in the record.  It shouldn't affect performance much.
                SqlFilterStringBuilder fsb = new SqlFilterStringBuilder();
                SqlCmdParams cmdParams = new SqlCmdParams();

                fsb.AddCondition("RecordId=@RecordId");
                cmdParams.Add("@RecordId", recordId);

                if (ANY_ID != receiverId)
                {
                    fsb.AddCondition(string.Format("(ReceiverId=@ReceiverId OR ReceiverId={0})", BROADCAST_ID));
                    cmdParams.Add("@ReceiverId", receiverId);
                }

                if (ANY_ID != senderId)
                {
                    fsb.AddCondition("SenderId=@SenderId");
                    cmdParams.Add("@SenderId", senderId);
                }

                string cmd = string.Format("SELECT MsgData FROM {0} {1}",
                                           GetTableName(mailboxName),
                                           fsb.ToString());

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    using (DbDataReader reader = con.ExecuteReader(cmd, cmdParams))
                    {
                        if (reader.Read())
                        {
                            long bufSize = reader.GetBytes(reader.GetOrdinal("MsgData"), 0, null, 0, 0);
                            byte[] buf = new byte[bufSize];
                            Int64 bytesRead = reader.GetBytes(reader.GetOrdinal("MsgData"), 0, buf, 0, buf.Length);
                            msgData = (bytesRead > 0) ? buf : null;
                        }
                        else
                        {
                            return ctx.Error(ErrorCode.InvalidArguments, "Could not find matching record");
                        }
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Creates a new message in a mailbox.
        public static Error CreateRecord(int titleId,
                                         Int64 senderId,
                                         string mailboxName,
                                         string senderDisplayName,
                                         Int64 receiverId,
                                         byte[] userData,
                                         byte[] msgData,
                                         uint expirationMinutes,
                                         out Int64 recordId)
        {
            Context ctx = RlineContext.Create();
            Error err = null;
            recordId = 0;

            try
            {
                MailboxInfo mailboxInfo = MailboxMetadata.GetCachedMailboxInfo(titleId, mailboxName);

                //Broadcast only check
                if (mailboxInfo.BroadcastOnly && (receiverId != BROADCAST_ID))
                {
                    return ctx.Error(ErrorCode.NotAllowed, "Cannot send to specific ID in a broadcast-only mailbox");
                }

                //Enforce data size limits
                if (mailboxInfo.UserDataMaxSize > 0
                   && userData != null
                   && userData.Length > mailboxInfo.UserDataMaxSize)
                {
                    return ctx.Error(ErrorCode.NotAllowed,
                                     "User data too large ({0} bytes, limit is {1})",
                                     userData.Length,
                                     mailboxInfo.UserDataMaxSize);
                }

                if (mailboxInfo.MsgDataMaxSize > 0
                   && msgData != null
                   && msgData.Length > mailboxInfo.MsgDataMaxSize)
                {
                    return ctx.Error(ErrorCode.NotAllowed,
                                     "Msg data too large ({0} bytes, limit is {1})",
                                     msgData.Length,
                                     mailboxInfo.MsgDataMaxSize);
                }

                //Check the sender's mailbox permissions.
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                uint permissionsRequired = 0;
                
                if (receiverId == BROADCAST_ID)
                {
                    permissionsRequired |= MailboxPermission.SendBroadcast;
                }
                else
                {
                    permissionsRequired |= MailboxPermission.SendPersonal;
                }

                if (null != (err = CheckAccountPermissions(titleInfo, senderId, mailboxInfo, permissionsRequired)))
                {
                    return ctx.Error(err);
                }

                //Make sure we're below our send limit
                if (senderId != Mailbox.SYSTEM_ID && mailboxInfo.SendLimit > 0)
                {
                    int num;
                    if (null != (err = QueryNumRecords(titleId,
                                                       Mailbox.SYSTEM_ID,
                                                       mailboxInfo.Name,
                                                       senderId,
                                                       ANY_ID,
                                                       0,
                                                       0,
                                                       false,
                                                       out num)))
                    {
                        return ctx.Error(err);
                    }

                    if (num >= mailboxInfo.SendLimit)
                    {
                        return ctx.Error(ErrorCode.NotAllowed, "Sender at max allowed ({0})", num);
                    }
                }

                //Make sure our receiver is below their receive limit
                if (!mailboxInfo.BroadcastOnly
                    && receiverId != BROADCAST_ID
                    && (mailboxInfo.ReceiveLimit > 0))
                {
                    int num;
                    if (null != (err = QueryNumRecords(titleId,
                                                       Mailbox.SYSTEM_ID,
                                                       mailboxInfo.Name,
                                                       ANY_ID,
                                                       receiverId,
                                                       0,
                                                       0,
                                                       false,
                                                       out num)))
                    {
                        return ctx.Error(err);
                    }

                    if (num >= mailboxInfo.ReceiveLimit)
                    {
                        return ctx.Error(ErrorCode.NotAllowed, "Receiver at max allowed ({0})", num);
                    }
                }

                //Create the new record
                expirationMinutes = Math.Max(expirationMinutes, mailboxInfo.MinExpirationMinutes);
                expirationMinutes = Math.Min(expirationMinutes, mailboxInfo.MaxExpirationMinutes);

                StringBuilder sb = new StringBuilder(string.Format("INSERT INTO {0}", GetTableName(mailboxInfo.Name)));
                sb.Append(" (SenderId, SenderDisplayName, ReceiverId");
                if (userData != null)
                {
                    sb.Append(", UserData");
                }
                sb.Append(", MsgData, ExpirationDateTime)");

                sb.Append(" VALUES (@SenderId, @SenderDisplayName, @ReceiverId");
                if (userData != null)
                {
                    sb.Append(", @UserData");
                }
                sb.Append(", @MsgData, @ExpirationDateTime)");
                
                sb.Append("; SELECT CAST(scope_identity() AS bigint)");

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@SenderId", senderId);
                cmdParams.Add("@SenderDisplayName", senderDisplayName);
                cmdParams.Add("@ReceiverId", receiverId);
                if (userData != null)
                {
                    cmdParams.Add("@UserData", userData);
                }
                cmdParams.Add("@MsgData", msgData);
                cmdParams.Add("@ExpirationDateTime", DateTime.UtcNow.AddMinutes(expirationMinutes));

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    recordId = Int64.Parse(con.ExecuteScalar(sb.ToString(), cmdParams).ToString());
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }



        //PURPOSE
        //  Deletes the specified record in a mailbox, if the account has
        //  the necessary permissions.
        public static Error DeleteRecords(int titleId,
                                          Int64 accountId,
                                          string mailboxName,
                                          Int64 senderId,
                                          Int64 receiverId,
                                          Int64 startRecordId,
                                          Int64 endRecordId,
                                          bool expiredOnly,
                                          out uint numDeleted)
        {
            Context ctx = RlineContext.Create();
            Error err = null;
            numDeleted = 0;

            try
            {
                MailboxInfo mailboxInfo = MailboxMetadata.GetCachedMailboxInfo(titleId, mailboxName);
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                //Check permissions
                err = CommonCheckDeletePermissions(titleInfo,
                                                   mailboxInfo,
                                                   accountId,
                                                   senderId,
                                                   receiverId);
                if (err != null)
                {
                    return ctx.Error(err);
                }

                //Compose filter string and parameters
                SqlFilterStringBuilder fsb = new SqlFilterStringBuilder();
                SqlCmdParams cmdParams = new SqlCmdParams();

                err = CommonBuildDeleteFilter(mailboxInfo,
                                              accountId,
                                              senderId,
                                              receiverId,
                                              startRecordId,
                                              endRecordId,
                                              expiredOnly,
                                              ref fsb,
                                              ref cmdParams);
                if (err != null)
                {
                    return ctx.Error(err);
                }

                //Do the query
                string cmd = string.Format("DELETE FROM {0} {1}",
                                           GetTableName(mailboxName),
                                           fsb.ToString());

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    numDeleted = (uint)con.ExecuteNonQuery(cmd, cmdParams);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Deletes all expired records in the specified mailbox.
        public static Error DeleteExpiredRecords(int titleId,
                                                 Int64 accountId,
                                                 string mailboxName)
        {
            Context ctx = RlineContext.Create();
            Error err = null;

            try
            {
                MailboxInfo mailboxInfo = MailboxMetadata.GetCachedMailboxInfo(titleId, mailboxName);
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                //Get the account's permissions within this mailbox.
                if (null != (err = CheckAccountPermissions(titleInfo, 
                                                           accountId, 
                                                           mailboxInfo, 
                                                           MailboxPermission.DeleteAll)))
                {
                    return ctx.Error(err);
                }

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@UtcNow", DateTime.UtcNow);

                string cmd = string.Format("DELETE FROM {0} WHERE ExpirationDateTime<@UtcNow",
                                           GetTableName(mailboxName));

                int numDeleted = 0;

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    numDeleted = con.ExecuteNonQuery(cmd, cmdParams);
                }

                return ctx.Success2("Deleted {0} records", numDeleted);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Checks read permissions for account to read messages from a mailbox.
        private static Error CommonCheckReadPermissions(TitleInfo titleInfo,
                                                        MailboxInfo mailboxInfo,
                                                        Int64 accountId,
                                                        Int64 senderId,
                                                        Int64 receiverId)
        {
            Context ctx = RlineContext.Create();
            Error err = null;

            try
            {
                uint permissionsRequired = 0;

                if ((receiverId == accountId) || (receiverId == BROADCAST_ID))
                {
                    permissionsRequired |= MailboxPermission.ReadMyReceived;
                }
                else if (senderId == accountId)
                {
                    permissionsRequired |= MailboxPermission.ReadMySent;
                }
                else
                {
                    permissionsRequired |= MailboxPermission.ReadAll;
                }

                if (null != (err = CheckAccountPermissions(titleInfo,
                                                           accountId,
                                                           mailboxInfo,
                                                           permissionsRequired)))
                {
                    return ctx.Error(err);
                }

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Builds a filter usable when SELECTing mailbox records.
        private static Error CommonBuildReadFilter(MailboxInfo mailboxInfo,
                                                   Int64 accountId,
                                                   Int64 senderId,
                                                   Int64 receiverId,
                                                   Int64 startRecordId,
                                                   Int64 endRecordId,
                                                   bool includeExpired,
                                                   ref SqlFilterStringBuilder fsb,
                                                   ref SqlCmdParams cmdParams)
        {
            Context ctx = RlineContext.Create();

            try
            {
                //Filter by record ID range
                if (startRecordId == endRecordId)
                {
                    if (startRecordId > 0)
                    {
                        fsb.AddCondition("RecordId=@StartRecordId");
                        cmdParams.Add("@StartRecordId", startRecordId);
                    }
                }
                else
                {
                    if (startRecordId > 0)
                    {
                        fsb.AddCondition("RecordId>=@StartRecordId");
                        cmdParams.Add("@StartRecordId", startRecordId);
                    }
                 
                    if (endRecordId > 0)
                    {
                        fsb.AddCondition("RecordId<=@EndRecordId");
                        cmdParams.Add("@EndRecordId", endRecordId);
                    }
                }

                //Filter by sender
                if (senderId != ANY_ID)
                {
                    fsb.AddCondition("SenderId=@SenderId");
                    cmdParams.Add("@SenderId", senderId);
                }

                //Filter by receiver
                if (receiverId != ANY_ID && !mailboxInfo.BroadcastOnly)
                {
                    fsb.AddCondition(string.Format("(ReceiverId=@ReceiverId OR ReceiverId={0})", BROADCAST_ID));
                    cmdParams.Add("@ReceiverId", receiverId);
                }

                //Filter out expired records
                if (!includeExpired)
                {
                    fsb.AddCondition("ExpirationDateTime>@UtcNow");
                    cmdParams.Add("@UtcNow", DateTime.UtcNow);
                }

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        private static Error CommonQueryRecords(TitleInfo titleInfo,
                                                MailboxInfo mailboxInfo,
                                                SqlFilterStringBuilder fsb,
                                                SqlCmdParams cmdParams,
                                                uint maxRecords,
                                                bool readUserData,
                                                bool readMsgData,
                                                bool oldestFirst,
                                                out List<MailboxRecord> records)
        {
            Context ctx = RlineContext.Create();
            Error err = null;
            records = null;

            try
            {
                string cols = "RecordId, SenderId, SenderDisplayName, ReceiverId, SendDateTime, ExpirationDateTime, DATALENGTH(UserData) as UserDataLength, DATALENGTH(MsgData) as MsgDataLength";

                if (readUserData)
                {
                    cols += ", UserData";
                }

                if (readMsgData)
                {
                    cols += ", MsgData";
                }

                string cmd = string.Format("SELECT {0} FROM {1}{2}",
                                           cols,
                                           GetTableName(mailboxInfo.Name),
                                           fsb.ToString());

                if (oldestFirst)
                {
                    cmd += " ORDER BY RecordId ASC";
                }
                else
                {
                    cmd += " ORDER BY RecordId DESC";
                }

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    using (DbDataReader reader = con.ExecuteReader(cmd, cmdParams))
                    {
                        records = new List<MailboxRecord>();

                        while (reader.Read())
                        {
                            MailboxRecord r;
                            if (null != (err = CommonReadRecord(reader, mailboxInfo, readUserData, readMsgData, out r)))
                            {
                                return ctx.Error(err);
                            }

                            records.Add(r);

                            if ((maxRecords > 0) && (records.Count >= maxRecords))
                            {
                                break;
                            }
                        }
                    }
                }

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        private static Error CommonReadRecord(DbDataReader reader,
                                              MailboxInfo info,
                                              bool readUserData,
                                              bool readMsgData,
                                              out MailboxRecord r)
        {
            Context ctx = RlineContext.Create();
            r = null;

            try
            {
                r = new MailboxRecord();

                r.RecordId = (Int64)reader["RecordId"];
                r.SenderId = (Int64)reader["SenderId"];
                r.SenderDisplayName = reader["SenderDisplayName"].ToString();
                r.ReceiverId = (Int64)reader["ReceiverId"];
                r.SendDateTime = (DateTime)reader["SendDateTime"];

                object obj = reader["UserDataLength"];

                if (!(obj is System.DBNull))
                {
                    r.UserDataLength = (uint)((Int64)obj);
                }
                else
                {
                    r.UserDataLength = 0;
                    readUserData = false;
                }

                r.MsgDataLength = (uint)((Int64)reader["MsgDataLength"]);
                r.ExpirationDateTime = (DateTime)reader["ExpirationDateTime"];

                if (readUserData || readMsgData)
                {
                    byte[] buf;
                    long bufSize = 0;
                    Int64 bytesRead;

                    if (readUserData)
                    {
                        bufSize = reader.GetBytes(reader.GetOrdinal("UserData"), 0, null, 0, 0);

                        if (bufSize != r.UserDataLength)
                        {
                            return ctx.Error(ErrorCode.DbInvalidData, "UserDataLength({0}) does not match DATALENGTH({1})", bufSize, r.UserDataLength);
                        }

                        buf = new byte[bufSize];
                        bytesRead = reader.GetBytes(reader.GetOrdinal("UserData"), 0, buf, 0, buf.Length);
                        r.UserData = (bytesRead > 0) ? buf : null;
                    }

                    if (readMsgData)
                    {
                        bufSize = reader.GetBytes(reader.GetOrdinal("MsgData"), 0, null, 0, 0);

                        if (bufSize != r.MsgDataLength)
                        {
                            return ctx.Error(ErrorCode.DbInvalidData, "MsgDataLength({0}) does not match DATALENGTH({1})", bufSize, r.MsgDataLength);
                        }

                        buf = new byte[bufSize];
                        bytesRead = reader.GetBytes(reader.GetOrdinal("MsgData"), 0, buf, 0, buf.Length);
                        r.MsgData = (bytesRead > 0) ? buf : null;
                    }
                }

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Checks permissions for account to delete messages from a mailbox.
        private static Error CommonCheckDeletePermissions(TitleInfo titleInfo,
                                                          MailboxInfo mailboxInfo,
                                                          Int64 accountId,
                                                          Int64 senderId,
                                                          Int64 receiverId)
        {
            Context ctx = RlineContext.Create();
            Error err = null;

            try
            {
                uint permissionsRequired = 0;

                if (receiverId == accountId)
                {
                    permissionsRequired |= MailboxPermission.DeleteMyReceived;
                }
                else if (senderId == accountId)
                {
                    permissionsRequired |= MailboxPermission.DeleteMySent;
                }
                else
                {
                    permissionsRequired |= MailboxPermission.DeleteAll;
                }

                if (null != (err = CheckAccountPermissions(titleInfo,
                                                           accountId,
                                                           mailboxInfo,
                                                           permissionsRequired)))
                {
                    return ctx.Error(err);
                }

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Builds a filter usable when DELETEing mailbox records.
        private static Error CommonBuildDeleteFilter(MailboxInfo mailboxInfo,
                                                     Int64 accountId,
                                                     Int64 senderId,
                                                     Int64 receiverId,
                                                     Int64 startRecordId,
                                                     Int64 endRecordId,
                                                     bool expiredOnly,
                                                     ref SqlFilterStringBuilder fsb,
                                                     ref SqlCmdParams cmdParams)
        {
            Context ctx = RlineContext.Create();

            try
            {
                //Filter by record ID range
                if (startRecordId == endRecordId)
                {
                    if (startRecordId > 0)
                    {
                        fsb.AddCondition("RecordId=@StartRecordId");
                        cmdParams.Add("@StartRecordId", startRecordId);
                    }
                }
                else
                {
                    if (startRecordId > 0)
                    {
                        fsb.AddCondition("RecordId>=@StartRecordId");
                        cmdParams.Add("@StartRecordId", startRecordId);
                    }

                    if (endRecordId > 0)
                    {
                        fsb.AddCondition("RecordId<=@EndRecordId");
                        cmdParams.Add("@EndRecordId", endRecordId);
                    }
                }

                //Filter by sender
                if (senderId != ANY_ID)
                {
                    fsb.AddCondition("SenderId=@SenderId");
                    cmdParams.Add("@SenderId", senderId);
                }

                //Filter by receiver
                if (receiverId != ANY_ID && !mailboxInfo.BroadcastOnly)
                {
                    fsb.AddCondition(string.Format("(ReceiverId=@ReceiverId OR ReceiverId={0})", BROADCAST_ID));
                    cmdParams.Add("@ReceiverId", receiverId);
                }

                //Filter on expiration
                if (expiredOnly)
                {
                    fsb.AddCondition("ExpirationDateTime<=@UtcNow");
                    cmdParams.Add("@UtcNow", DateTime.UtcNow);
                }

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Checks to see if account has all specified permissions.
        private static Error CheckAccountPermissions(TitleInfo titleInfo,
                                                     Int64 accountId,
                                                     MailboxInfo mailboxInfo,
                                                     uint permissionsRequired)
        {
            Context ctx = RlineContext.Create();

            try
            {
                //Check for invalid accounts.
                if (accountId < 0)
                {
                    return ctx.Error(ErrorCode.InvalidArguments, "Invalid account ({0})", accountId);
                }

                //System account has all permissions.
                if (accountId == SYSTEM_ID)
                {
                    return ctx.Success3();
                }
            
                //No permissions required, yay.
                if (permissionsRequired == 0)
                {
                    return ctx.Success3();
                }

                //Start with the permissions of the mailbox's default role.
                MailboxRoleInfo roleInfo = MailboxRoles.GetCachedMailboxRoleInfo(titleInfo.Id, 
                                                                                 mailboxInfo.DefaultRoleName);
                uint permissionsOwned = roleInfo.Permissions;

                //Now modify by any role override
                if (mailboxInfo.RoleOverrideMode != MailboxRoleOverrideMode.None)
                {
                    if (mailboxInfo.RoleOverrideMode == MailboxRoleOverrideMode.Add)
                    {
                        if ((permissionsRequired & permissionsOwned) == permissionsRequired)
                        {
                            //No need to check overrides, we have enough already.
                            return ctx.Success3();
                        }
                    }
                    else if (mailboxInfo.RoleOverrideMode == MailboxRoleOverrideMode.Remove)
                    {
                        if ((permissionsRequired & permissionsOwned) != permissionsRequired)
                        {
                            //No chance of getting required, so no need to check override.
                            return ctx.Error(ErrorCode.NotAllowed);
                        }
                    }

                    //Get the role override for this account, then modify permissions by it.                    
                    using (SqlConnectionHelper con = Title.Open(titleInfo))
                    {
                        List<MailboxRoleOverrideInfo> overrideInfos;

                        Error err = MailboxRoleOverrides.QueryMailboxRoleOverrideInfo(
                                        con, 
                                        accountId,
                                        mailboxInfo.Name,
                                        1,
                                        out overrideInfos);
                        if (null != err)
                        {
                            return ctx.Error(err);
                        }

                        if (overrideInfos != null && overrideInfos.Count > 0)
                        {
                            roleInfo = MailboxRoles.GetCachedMailboxRoleInfo(titleInfo.Id, overrideInfos[0].RoleName);

                            switch (mailboxInfo.RoleOverrideMode)
                            {
                                case MailboxRoleOverrideMode.Replace: permissionsOwned = roleInfo.Permissions; break;
                                case MailboxRoleOverrideMode.Add: permissionsOwned |= roleInfo.Permissions; break;
                                case MailboxRoleOverrideMode.Remove: permissionsOwned &= ~roleInfo.Permissions; break;
                                default:
                                    return ctx.Error(ErrorCode.UnknownError,
                                                     "Invalid role override mode: {0}",
                                                     mailboxInfo.RoleOverrideMode.ToString());
                            }
                        }
                    }
                }

                if ((permissionsRequired & permissionsOwned) != permissionsRequired)
                {
                    return ctx.Error(ErrorCode.NotAllowed);
                }

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }
}
