using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Rline
{
    public class Match
    {
        private const string TABLE_NAME = "rlMatches";

        //PURPOSE
        //  Creates the match members table
        //PARAMS
        //  con     - Open connection using DB that table should be created in.
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        public static Error CreateTable(SqlConnectionHelper con)
        {
            Context ctx = RlineContext.Create();

            try
            {
                //Drop the table if it already existed.  This is a transient table, so no real harm here.
                if (con.CheckTableExists(TABLE_NAME))
                {
                    Error err = DropTable(con);
                    if (err != null)
                    {
                        return ctx.Error(err);
                    }
                }

                string s = "CREATE TABLE " + TABLE_NAME;

                s += @"([MatchId] [int] IDENTITY(1,1) NOT NULL,
                              [IsArbitrated] [bit] NOT NULL,
                              [CreateDateTime] [datetime] NOT NULL DEFAULT(GETUTCDATE()),
                              [StartDateTime] [datetime] NULL,
                              [EndDateTime] [datetime] NULL)";

                con.AdHocExecuteNonQuery(s);

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Drops the matches table
        //PARAMS
        //  con     - Open connection using DB that table resides in
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        public static Error DropTable(SqlConnectionHelper con)
        {
            Context ctx = RlineContext.Create();

            try
            {
                if (!con.CheckTableExists(TABLE_NAME))
                {
                    return ctx.Error(ErrorCode.DoesNotExist, "Table {0} not found", TABLE_NAME);
                }

                con.AdHocExecuteNonQuery("DROP TABLE " + TABLE_NAME);

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        static bool GetMatches(SqlConnectionHelper con,
                               string cmd,
                               uint maxResults,
                               out List<MatchInfo> matchInfos)
        {
            matchInfos = null;

            try
            {
                using(DbDataReader reader = con.AdHocExecuteReader(cmd))
                {
                    matchInfos = new List<MatchInfo>();

                    while (reader.Read())
                    {
                        MatchInfo matchInfo = new MatchInfo();
                        Object col = null;

                        matchInfo.Id = (int)reader["MatchId"];
                        matchInfo.IsArbitrated = (bool)reader["IsArbitrated"];
                        matchInfo.CreateDateTime = (DateTime)reader["CreateDateTime"];

                        col = reader["StartDateTime"];
                        matchInfo.StartDateTime = (col is DBNull) ? DateTime.MinValue : (DateTime)col;

                        col = reader["EndDateTime"];
                        matchInfo.EndDateTime = (col is DBNull) ? DateTime.MinValue : (DateTime)col;

                        matchInfos.Add(matchInfo);

                        if ((maxResults != 0) && (matchInfos.Count >= maxResults))
                        {
                            break;
                        }
                    }

                    reader.Close();
                }
            }
            catch
            {
                matchInfos.Clear();
                return false;
            }

            return true;
        }                             

        public static Error GetMatchesReadyForArbitration(SqlConnectionHelper con,
                                                          uint maxResults,
                                                          out List<MatchInfo> matchInfos)
        {
            Context ctx = RlineContext.Create();

            //A match is ready for arbitration if:
            //  - It is arbitrated, and has not yet been processed AND
            //  - It has ended, and all its members have submitted stats for arbitration OR
            //  - It has ended, and it is past some cutoff (X minutes) OR
            //  - It has started, but not ended for longer than some cutoff (X minutes)
            string s = string.Format("SELECT * FROM {0} WHERE IsArbitrated=1", Match.TABLE_NAME);

            if (!GetMatches(con, s, maxResults, out matchInfos))
            {
                return ctx.Error(ErrorCode.DbCommandFailed);
            }

            return ctx.Success2("Found {0} matches", matchInfos.Count);
        }

        //PURPOSE
        //  Returns info on a single match.
        public static Error QueryMatch(SqlConnectionHelper con,
                                       int matchId,
                                       out MatchInfo matchInfo)
        {
            Context ctx = RlineContext.Create();
            matchInfo = null;

            string s = string.Format("SELECT * FROM {0} WHERE MatchId={1}", Match.TABLE_NAME, matchId);

            List<MatchInfo> matchInfos = new List<MatchInfo>();

            if (GetMatches(con, s, 1, out matchInfos))
            {
                if (matchInfos.Count > 0)
                {
                    matchInfo = matchInfos[0];
                    return ctx.Success2();
                }
                else
                {
                    return ctx.Error(ErrorCode.InvalidArguments);
                }
            }
            else
            {
                return ctx.Error(ErrorCode.DbCommandFailed);
            }
        }

        //PURPOSE
        //  Returns info on multiple matches, ordered by most recently created.
        //PARAMS
        //  con             - Connection to server USEing the title DB
        //  maxResult       - Max results to return (0 means infinite)
        //  matchInfos      - Holds results
        public static Error QueryMatches(SqlConnectionHelper con,
                                         uint maxResults,
                                         out List<MatchInfo> matchInfos)
        {
            Context ctx = RlineContext.Create();

            string s = string.Format("SELECT * FROM {0} ORDER BY CreateDateTime DESC", Match.TABLE_NAME);

            if (GetMatches(con, s, maxResults, out matchInfos))
            {
                return ctx.Success2("Found {0} matches", matchInfos.Count);
            }
            else
            {
                return ctx.Error(ErrorCode.DbCommandFailed);
            }
        }


        //PURPOSE
        //  Creates a match record for specified title.
        public static Error CreateMatch(SqlConnectionHelper con, 
                                        bool isArbitrated,
                                        out int matchId)
        {
            Context ctx = RlineContext.Create();
            matchId = 0;

            try
            {
                ctx.Debug2("isArbitrated={0}", isArbitrated);

                string s = string.Format("INSERT INTO {0} (IsArbitrated) VALUES ({1})", 
                                         Match.TABLE_NAME,
                                         isArbitrated ? 1 : 0);
                s += "; SELECT CAST(scope_identity() AS int)";

                matchId = int.Parse(con.AdHocExecuteScalar(s).ToString());

                return ctx.Success2("matchId={0}", matchId);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Deletes match on the backend.
        public static Error DeleteMatch(SqlConnectionHelper con, 
                                        int matchId)
        {
            Context ctx = RlineContext.Create();

            try
            {
                ctx.Debug2("matchId={0}", matchId);

                if (con.AdHocExecuteNonQuery(String.Format("DELETE FROM {0} WHERE MatchId={1}",
                                                      Match.TABLE_NAME,
                                                      matchId)) == 0)
                {
                    return ctx.Error(ErrorCode.DbCommandFailed);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Starts a match.
        public static Error StartMatch(SqlConnectionHelper con,
                                       int matchId)
        {
            Context ctx = RlineContext.Create();

            try
            {
                ctx.Debug2("matchId={0}", matchId);

                if (matchId == 0)
                {
                    return ctx.Error(ErrorCode.InvalidArguments);
                }

                //Get the match info.
                MatchInfo matchInfo;
                Error err = QueryMatch(con, matchId, out matchInfo);

                if (err != null)
                {
                    return ctx.Error(err);
                }

                //Make sure the match hasn't started or ended yet.
                if (matchInfo.Ended())
                {
                    return ctx.Error(RlineErrorCode.MatchAlreadyEnded);
                }
                else if (matchInfo.Started())
                {
                    return ctx.Error(RlineErrorCode.MatchAlreadyStarted);
                }

                //Update the match to be started
                string s = String.Format("UPDATE {0} SET StartDateTime=GETUTCDATE() WHERE MatchId={1}", Match.TABLE_NAME, matchId);

                if (con.AdHocExecuteNonQuery(s) == 0)
                {
                    return ctx.Error(ErrorCode.DbCommandFailed);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Ends a match.
        public static Error EndMatch(SqlConnectionHelper con,
                                     int matchId)
        {
            Context ctx = RlineContext.Create();

            try
            {
                ctx.Debug2("matchId={0}", matchId);

                if (matchId == 0)
                {
                    return ctx.Error(ErrorCode.InvalidArguments);
                }

                //Get the match info.
                MatchInfo matchInfo;
                Error err = QueryMatch(con, matchId, out matchInfo);

                if (err != null)
                {
                    return err;
                }

                //Make sure the match has started but hasn't ended yet.
                if (!matchInfo.Started())
                {
                    return ctx.Error(RlineErrorCode.MatchNotStarted);
                }
                else if (matchInfo.Ended())
                {
                    return ctx.Error(RlineErrorCode.MatchAlreadyEnded);
                }

                //Update the match to be ended
                string s = string.Format("UPDATE {0} SET EndDateTime=GETUTCDATE() WHERE MatchId={1}", Match.TABLE_NAME, matchId);

                if (con.AdHocExecuteNonQuery(s) == 0)
                {
                    return ctx.Error(ErrorCode.DbCommandFailed);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }
}
