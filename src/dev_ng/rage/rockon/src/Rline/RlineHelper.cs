using System;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Rline
{
    public class RlineHelper
    {
        //PURPOSE
        //  Given a connection to a title's DB, this creates all the tables
        //  required by rline from an XLAST file.
        //RETURNS
        //  True on success, false on failure.
        public static Error BootstrapFromXlast(SqlConnectionHelper con,
                                               XlastInfo xlastInfo)
        {
            Context ctx = RlineContext.Create();

            try
            {
                Error err;
                if (null != (err = Match.CreateTable(con)))
                {
                    return ctx.Error(err);
                }

                if (null != (err = MatchMember.CreateTable(con)))
                {
                    return ctx.Error(err);
                }

                if (!Submission.CreateTables(con, xlastInfo.Properties.Values))
                {
                    return ctx.Error(ErrorCode.UnknownError, "Error creating submission tables");
                }

                if (!Leaderboard.CreateTables(con, xlastInfo.Leaderboards))
                {
                    return ctx.Error(ErrorCode.UnknownError, "Error creating leaderboard tables");
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }
}
