using System;
using System.Collections.Generic;

namespace Rockstar.Rockon.Rline
{
    public enum LeaderboardResetMethod
    {
        Never,
        Weekly,
        Monthly,
        Bimonthly,
        Annually
    }

    public enum LeaderboardAggregationType
    {
        Invalid = -1,
        Last,
        Max,
        Min,
        Sum
    }

    public class LeaderboardColumn
    {
        public int Index;
        public string FriendlyName;
        public uint Id;
        public uint PropId; //Can tell type from this
        public LeaderboardAggregationType AggregationType;

        public LeaderboardColumn()
        {
        }

        public LeaderboardColumn(int index,
                                 string friendlyName,
                                 uint id,
                                 uint propId,
                                 LeaderboardAggregationType aggrType)
        {
            Index = index;
            FriendlyName = friendlyName;
            Id = id;
            PropId = propId;
            AggregationType = aggrType;
        }
    }

    public class LeaderboardRow
    {
        public UInt64 Rank;
        public Int64 AccountId;
        public string DisplayName;
        public List<object> ColumnValues = new List<object>();
    }

    public class LeaderboardUpdate
    {
        public uint LbId;
        public Int64 AccountId;
        public List<XblProperty> Props = new List<XblProperty>();
    }

    public class LeaderboardInfo
    {
        public uint Id;
        public string Name;
        public bool IsArbitrated;
        public bool IsHidden;
        public LeaderboardResetMethod ResetMethod = LeaderboardResetMethod.Never;
        public List<LeaderboardColumn> Cols = new List<LeaderboardColumn>();

        public LeaderboardInfo()
        {
        }

        public LeaderboardInfo(uint id,
                               string name,
                               bool isArbitrated,
                               bool isHidden,
                               LeaderboardResetMethod resetMethod)
        {
            Id = id;
            Name = name;
            IsArbitrated = isArbitrated;
            IsHidden = isHidden;
            ResetMethod = resetMethod;
        }

        public string GetColumnSchema()
        {
            string s = "";

            foreach (LeaderboardColumn lbc in Cols)
            {
                if (Cols.IndexOf(lbc) > 0)
                {
                    s += ", ";
                }

                s += string.Format("{0},{1},0x{2},0x{3},{4}",
                                   lbc.Index,
                                   lbc.FriendlyName,
                                   lbc.Id.ToString("X8"),
                                   lbc.PropId.ToString("X8"),
                                   lbc.AggregationType);
            }

            return s;
        }

        public bool CreateColumnsFromSchema(string s)
        {
            Cols = new List<LeaderboardColumn>();

            string[] tokens = s.Split(',');

            int i = 0;

            while ((i + 4) < tokens.Length)
            {
                LeaderboardColumn c = new LeaderboardColumn();

                c.Index = int.Parse(tokens[i++]);
                c.FriendlyName = tokens[i++];
                c.Id = uint.Parse(tokens[i++].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                c.PropId = uint.Parse(tokens[i++].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                //Aggregation type
                {
                    string aggrTypeStr = tokens[i++].Replace(" ", "");

                    c.AggregationType = LeaderboardAggregationType.Invalid;
                    foreach (LeaderboardAggregationType t in Enum.GetValues(typeof(LeaderboardAggregationType)))
                    {
                        if (aggrTypeStr == t.ToString())
                        {
                            c.AggregationType = t;
                            break;
                        }
                    }
                    if (c.AggregationType == LeaderboardAggregationType.Invalid)
                    {
                        Cols.Clear();
                        return false;
                    }
                }

                Cols.Add(c);
            }

            return true;
        }
    }
}
