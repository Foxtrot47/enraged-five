using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Rline
{
    public class Cleaner
    {
        //PURPOSE
        //  Cleans all titles in the environment, and returns milliseconds 
        //  until next cleaning is required.
        public static Error CleanEnvironment(out int delayMs)
        {
            Context ctx = RlineContext.Create();
            Error err = null;

            //Set delay until next cleaning
            delayMs = 1000 * 60 * 5;

            try
            {
                using (SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    //Get all the titles in this environment
                    List<TitleInfo> titleInfos;
                    if (null != (err = Title.QueryTitle(con, 0, 0, out titleInfos)))
                    {
                        throw new Exception(err.ToString());
                    }

                    //Clean each of them
                    foreach (TitleInfo titleInfo in titleInfos)
                    {
                        if (null != (err = CleanTitle(titleInfo)))
                        {
                            throw new Exception(err.ToString());
                        }
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Cleans the specified title.
        private static Error CleanTitle(TitleInfo titleInfo)
        {
            Context ctx = RlineContext.Create("titleId={0}", titleInfo.Id);
            Error err = null;

            try
            {
                if (null != (err = CleanMailboxes(titleInfo)))
                {
                    return ctx.Error(err);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Cleans the mailboxes for a title.
        private static Error CleanMailboxes(TitleInfo titleInfo)
        {
            Context ctx = RlineContext.Create();
            Error err = null;

            try
            {
                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    //Gather all mailboxes for the title
                    List<MailboxInfo> infos;
                    if (null != (err = MailboxMetadata.QueryMailboxInfo(con, null, out infos)))
                    {
                        return ctx.Error(err);
                    }

                    //Delete the expired records in each
                    foreach (MailboxInfo mailboxInfo in infos)
                    {
                        if (null != (err = Mailbox.DeleteExpiredRecords(titleInfo.Id, 0, mailboxInfo.Name)))
                        {
                            return ctx.Error(err);
                        }
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }
}
