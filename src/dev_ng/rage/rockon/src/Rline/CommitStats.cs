using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Rline
{
    public class CommitStats
    {
        //FIXME: This is begging for a generic, but don't know how to do it right now.
        //PURPOSE
        //  Determines what the new value of a column should be after aggregation.
        //  Returns null if no change, or object if value is different than current.
        static object Aggregate(LeaderboardAggregationType aggrType,
                                object propValue,
                                object colValue)
        {
            if(colValue == null)
            {
                return propValue;
            }

            try
            {
                if (propValue != null)
                {
                    if (aggrType == LeaderboardAggregationType.Last)
                    {
                        if (colValue is int && ((int)colValue != Convert.ToInt32(propValue)))
                        {
                            return propValue;
                        }
                        else if (colValue is float && ((float)colValue != Convert.ToSingle(propValue)))
                        {
                            return propValue;
                        }
                        else if (colValue is Int64 && ((Int64)colValue != Convert.ToInt64(propValue)))
                        {
                            return propValue;
                        }
                        else if (colValue is DateTime && ((DateTime)colValue != Convert.ToDateTime(propValue)))
                        {
                            return propValue;
                        }
                    }
                    else if (aggrType == LeaderboardAggregationType.Max)
                    {
                        if (colValue is int && ((int)colValue < Convert.ToInt32(propValue)))
                        {
                            return propValue;
                        }
                        else if (colValue is float && ((float)colValue < Convert.ToSingle(propValue)))
                        {
                            return propValue;
                        }
                        else if (colValue is Int64 && ((Int64)colValue < Convert.ToInt64(propValue)))
                        {
                            return propValue;
                        }
                        else if (colValue is DateTime && ((DateTime)colValue < Convert.ToDateTime(propValue)))
                        {
                            return propValue;
                        }
                    }
                    else if (aggrType == LeaderboardAggregationType.Min)
                    {
                        if (colValue is int && ((int)colValue > Convert.ToInt32(propValue)))
                        {
                            return propValue;
                        }
                        else if (colValue is float && ((float)colValue > Convert.ToSingle(propValue)))
                        {
                            return propValue;
                        }
                        else if (colValue is Int64 && ((Int64)colValue > Convert.ToInt64(propValue)))
                        {
                            return propValue;
                        }
                        else if (colValue is DateTime && ((DateTime)colValue > Convert.ToDateTime(propValue)))
                        {
                            return propValue;
                        }
                    }
                    else if (aggrType == LeaderboardAggregationType.Sum)
                    {
                        if (colValue is Int64) //Sum columns always have to be Int64 per XBL rules
                        {
                            return (Int64)colValue + Convert.ToInt64(propValue);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
                throw (ex);
            }

            return null;
        }

        //PURPOSE
        //  Commits a single submission for a title.
        public static Error CommitSubmission(SqlConnectionHelper con,
                                             SubmissionInfo sub)
        {
            Context ctx = RlineContext.Create("lbId={0}  matchId={1}  subAcct={2}  targAcct={3}  targName=\"{4}\"",
                                            sub.LbId,
                                            sub.MatchId,
                                            sub.SubmitterAccountId,
                                            sub.TargetAccountId,
                                            sub.TargetDisplayName);

            try
            {
                //Get info for LB being updated
                List<LeaderboardInfo> lbInfos;
                Error err = Leaderboard.QueryLbInfo(con, sub.LbId, out lbInfos);
                if (err != null)
                {
                    return ctx.Error(err);
                }

                LeaderboardInfo lbInfo = lbInfos[0];

                //Get the gamer's current LB entry, and INSERT or UPDATE based on existence.
                //TODO: Sort the rows read by last written date desc.
                LeaderboardRow row;
               
                if (null != (err = Leaderboard.ReadByGamer(con, sub.LbId, sub.TargetAccountId, out row)))
                {
                    return ctx.Error(err);
                }

                if (row == null)
                {
                    //User doesn't have a row, so we do an INSERT.
                    string cols = "AccountId, DisplayName";
                    string vals = string.Format("{0}, '{1}'", sub.TargetAccountId, sub.TargetDisplayName);

                    foreach (LeaderboardColumn c in lbInfo.Cols)
                    {
                        //NOTE: Linear property search is lame, but because number of props submitted
                        //      is usually very small it's probably acceptable here.
                        foreach (XblProperty p in sub.Props)
                        {
                            if (p.Id == c.PropId)
                            {
                                cols += string.Format("{0}{1}", (cols.Length > 0) ? ", " : "", Leaderboard.GetColName(c.Id));
                                vals += string.Format("{0}{1}", (vals.Length > 0) ? ", " : "", p.Value.ToString());
                                break;
                            }
                        }
                    }

                    if (cols.Length > 0)
                    {
                        string s = String.Format("INSERT INTO {0} ({1}) VALUES ({2})",
                                                 Leaderboard.GetLbTableName(sub.LbId), cols, vals);

                        try
                        {
                            if (con.AdHocExecuteNonQuery(s) != 1)
                            {
                                return ctx.Error(ErrorCode.DbCommandFailed);
                            }
                        }
                        catch (Exception ex)
                        {
                            return ctx.Error(ex);
                        }
                    }
                }
                else
                {
                    //UPDATE the user's entry.
                    string setStr = "";

                    foreach (LeaderboardColumn c in lbInfo.Cols)
                    {
                        //NOTE: Linear property search is lame, but because number of props submitted
                        //      is usually very small it's probably acceptable here.
                        foreach (XblProperty p in sub.Props)
                        {
                            if (p.Id == c.PropId)
                            {
                                object v = Aggregate(c.AggregationType, p.Value, row.ColumnValues[c.Index]);

                                if (v != null)
                                {
                                    if (setStr.Length > 0)
                                    {
                                        setStr += ", ";
                                    }

                                    setStr += string.Format("{0}={1}", Leaderboard.GetColName(c.Id), v);
                                }
                                break;
                            }
                        }
                    }

                    if (setStr.Length > 0)
                    {
                        string s = String.Format("UPDATE {0} SET {1} WHERE AccountId={2}",
                                                 Leaderboard.GetLbTableName(sub.LbId), setStr, sub.TargetAccountId);

                        try
                        {
                            if (con.AdHocExecuteNonQuery(s) != 1)
                            {
                                return ctx.Error(ErrorCode.DbCommandFailed);
                            }
                        }
                        catch (Exception ex)
                        {
                            return ctx.Error(ex);
                        }
                    }
                }

                //Delete the submission so it can't be processed twice.
                {
                    //FIXME: Do we need a timestamp as well to identify it?
                    string s = string.Format("DELETE FROM Submissions WHERE MatchId={0} AND LbId={1} AND SubmitterAccountId={2} AND TargetAccountId={3}",
                                             sub.MatchId, sub.LbId, sub.SubmitterAccountId, sub.TargetAccountId);

                    try
                    {
                        if (con.AdHocExecuteNonQuery(s) != 1)
                        {
                            return ctx.Error(ErrorCode.DbCommandFailed);
                        }
                    }
                    catch (Exception ex)
                    {
                        return ctx.Error(ex);
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
        
        //PURPOSE 
        //  Commits submissions (aggregates them to leaderboards) for a title.
        //FIXME: Since this can succeed for many submissions before failing,
        //       perhaps don't count a single failure as overall failure.
        //       Maybe need a way to report individual failures, dunno right now.
        public static Error CommitTitleSubmissions(SqlConnectionHelper con,
                                                   int titleId)
        {
            Context ctx = RlineContext.Create("titleId={0}", titleId);

            try
            {
                //NOTE: Submissions MUST be processed in order from oldest to newest, since we are
                //      not preprocessing all submissions at once.
                List<SubmissionInfo> submissionInfos;
                
                Error err = Submission.QuerySubmission(con, false, 0, true, 0, out submissionInfos);
                if (err != null)
                {
                    return ctx.Error(err);
                }

                foreach (SubmissionInfo sub in submissionInfos)
                {
                    if (null != (err = CommitSubmission(con, sub)))
                    {
                        return ctx.Error(err);
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }
}
