using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Rline
{
    public class XlastInfo
    {
        public uint TitleId = 0;
        public Dictionary<uint, XblProperty> Properties = new Dictionary<uint, XblProperty>();
        public List<LeaderboardInfo> Leaderboards = new List<LeaderboardInfo>();
    }

    public class Xlast
    {
        public static bool ParseStream(Stream schemaStream, 
                                       out XlastInfo info)
        {
            info = new XlastInfo();

            XmlDocument doc = new XmlDocument();
            doc.Load(schemaStream);

            XmlElement projEl = doc.DocumentElement["GameConfigProject"];
            XmlElement statsViewsEl = (null != projEl) ? projEl["StatsViews"] : null;
            XmlElement viewEl = (null != statsViewsEl) ? (XmlElement) statsViewsEl.FirstChild : null;
            string titleIdStr = (null != projEl) ? projEl.Attributes.GetNamedItem("titleId").Value.Substring(2) : null;

            if(null == projEl)
            {
                throw(new Exception("No GameConfigProject element"));
            }
            else if(null == titleIdStr)
            {
                throw(new Exception("No title id"));
            }
            else if(null == statsViewsEl)
            {
                throw(new Exception("No StatsViews element"));
            }
            else if(null == viewEl)
            {
                throw(new Exception("No leaderboard views"));
            }

            //Parse the title id retrieved from the xlast.
            NumberFormatInfo nfi = new NumberFormatInfo();
            uint titleId;
            
            if(!UInt32.TryParse(titleIdStr, NumberStyles.HexNumber, nfi, out titleId))
            {
                throw(new Exception("Error parsing title id"));
            }

            info.TitleId = titleId;

            //Read leaderboards from the xlast, collecting unique properties as we go.
            for(; null != viewEl; viewEl = (XmlElement) viewEl.NextSibling)
            {
                string lbName = viewEl.Attributes.GetNamedItem("friendlyName").Value;
                string strLbId = viewEl.Attributes.GetNamedItem("id").Value;
                XmlNode hidden = viewEl.Attributes.GetNamedItem("hidden");
                XmlNode arbitrated = viewEl.Attributes.GetNamedItem("arbitrated");
                XmlNode resetType = viewEl.Attributes.GetNamedItem("resetType");

                if(null == lbName)
                {
                    throw(new Exception("No friendlyName in leaderboard view"));
                }
                else if(null == strLbId)
                {
                    throw(new Exception("No id in view:" + lbName));
                }
                else if(null == hidden)
                {
                    throw(new Exception("No hidden attribute in view:" + lbName));
                }
                else if(null == arbitrated)
                {
                    throw(new Exception("No arbitrated attribute in view:" + lbName));
                }
                else if(null == resetType)
                {
                    throw(new Exception("No resetType attribute in view:" + lbName));
                }

                bool isHidden = (hidden.Value.ToLower() == "true");
                bool isArbitrated = (arbitrated.Value.ToLower() == "true");
                string resetMethodStr = resetType.Value.ToLower();

                LeaderboardResetMethod resetMethod = LeaderboardResetMethod.Never;

                if("never" == resetMethodStr)
                {
                    resetMethod = LeaderboardResetMethod.Never;
                }
                else if("weekly" == resetMethodStr)
                {
                    resetMethod = LeaderboardResetMethod.Weekly;
                }
                else if("monthly" == resetMethodStr)
                {
                    resetMethod = LeaderboardResetMethod.Monthly;
                }
                else if("bimonthly" == resetMethodStr)
                {
                    resetMethod = LeaderboardResetMethod.Bimonthly;
                }
                else if("annually" == resetMethodStr)
                {
                    resetMethod = LeaderboardResetMethod.Annually;
                }
                else
                {
                    throw(new Exception("Unknown reset type:" + resetType + " in view:" + lbName));
                }

                XmlElement colEl = (XmlElement) viewEl["Columns"].FirstChild;
                if(null == colEl)
                {
                    throw(new Exception("No Columns element in view:" + lbName));
                }

                //Create the leaderboard instance for this view.
                LeaderboardInfo lbInfo = new LeaderboardInfo(UInt32.Parse(strLbId),
                                                             lbName,
                                                             isArbitrated,
                                                             isHidden,
                                                             resetMethod);

                //Add the columns
                for(; null != colEl; colEl = (XmlElement) colEl.NextSibling)
                {
                    string colName = colEl.Attributes.GetNamedItem("friendlyName").Value;

                    if(null == colName)
                    {
                        throw(new Exception("No friendlyName attribute in view:" + lbName + " column:" + lbInfo.Cols.Count));
                    }

                    XmlAttribute propIdAttr;
                    XmlElement propertyEl = (XmlElement) colEl["Property"];

                    if(null != propertyEl)
                    {
                        propIdAttr = (XmlAttribute) propertyEl.Attributes.GetNamedItem("id");
                    }
                    else
                    {
                        XmlElement ctxEl = (XmlElement) colEl["Context"];
                        propIdAttr = (XmlAttribute) ctxEl.Attributes.GetNamedItem("id");
                    }

                    if(null == propIdAttr)
                    {
                        throw (new Exception("No property/context attribute in view:" + lbName + " column:" + lbInfo.Cols.Count));
                    }

                    string propIdStr = propIdAttr.Value.Substring(2);
                    uint propId;
                    
                    if(!UInt32.TryParse(propIdStr, NumberStyles.HexNumber, nfi, out propId))
                    {
                        throw (new Exception("Error parsing property id in view:" + lbName + " column:" + lbInfo.Cols.Count));
                    }

                    //Rank and gamer name columns and do not appear as actual
                    //columns in a leaderboard.  They appear instead as
                    //ancillary data in the structure returned from a query.
                    if (propId != Xbl.RANK_FIELD_ID && propId != Xbl.GAMER_NAME_FIELD_ID)
                    {
                        XblProperty prop;

                        //Do we have this property in our collection?
                        if(!info.Properties.TryGetValue(propId, out prop))
                        {
                            prop = new XblProperty(propId, null);
                            info.Properties.Add(propId, prop);
                        }

                        string colIdStr = colEl.Attributes.GetNamedItem("attributeId").Value;
                        string aggMethodStr = colEl["Property"]["Aggregation"].Attributes.GetNamedItem("type").Value;

                        if(null == colIdStr)
                        {
                            throw (new Exception("No attributeId attribute in view:" + lbName + " column:" + lbInfo.Cols.Count));
                        }

                        if(null == aggMethodStr)
                        {
                            throw (new Exception("No aggregation method in view:" + lbName + " column:" + lbInfo.Cols.Count));
                        }

                        //NOTE: Column IDs are in integer format, unlike most other IDs.
                        uint colId;
                        if(!UInt32.TryParse(colIdStr, NumberStyles.Integer, nfi, out colId))
                        {
                            throw (new Exception("Error parsing column id in view:" + lbName + " column:" + lbInfo.Cols.Count));
                        }

                        LeaderboardAggregationType aggrType = LeaderboardAggregationType.Invalid;

                        aggMethodStr = (null != aggMethodStr) ? aggMethodStr.ToLower() : "";

                        if ("last" == aggMethodStr)
                        {
                            aggrType = LeaderboardAggregationType.Last;
                        }
                        else if ("sum" == aggMethodStr)
                        {
                            aggrType = LeaderboardAggregationType.Sum;
                        }
                        else if ("min" == aggMethodStr)
                        {
                            aggrType = LeaderboardAggregationType.Min;
                        }
                        else if ("max" == aggMethodStr)
                        {
                            aggrType = LeaderboardAggregationType.Max;
                        }
                        else
                        {
                            throw (new Exception("Invalid aggregation method: " + aggMethodStr));
                        }

                        LeaderboardColumn lbc =
                            new LeaderboardColumn(lbInfo.Cols.Count, colName, colId, prop.Id, aggrType);

                        lbInfo.Cols.Add(lbc);
                    }
                }

                info.Leaderboards.Add(lbInfo);
            }

            return true;
        }
    }
}
