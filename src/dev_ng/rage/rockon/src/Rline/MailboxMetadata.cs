using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.Caching;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Rline
{
    public class MailboxMetadata
    {
        //TODO: Make configurable
        private const int CACHE_ABSOLUTE_EXPIRATION_MINUTES = 60 * 24; //Daily

        //PURPOSE
        //  Returns name of mailbox metadata table.
        public static string GetTableName()
        {
            return Mailbox.MAILBOX_TABLE_PREFIX + "Metadata";
        }

        //PURPOSE
        //  Creates the mailbox metadata table.
        public static Error CreateTable(SqlConnectionHelper con)
        {
            Context ctx = RlineContext.Create();

            try
            {
                string tableName = GetTableName();

                if (!con.CheckTableExists(tableName))
                {
                    string cmd = "CREATE TABLE " + tableName + Environment.NewLine;

                    cmd += string.Format(@"([Name] [varchar](255) NOT NULL,
                                           [CreateDateTime] [datetime] NOT NULL DEFAULT(GETUTCDATE()),
                                           [BroadcastOnly] [bit] NOT NULL,
                                           [DefaultRoleName] [varchar](50) NOT NULL,
                                           [RoleOverrideMode] [varchar](50) NOT NULL,
                                           [UserDataMaxSize] [int] NOT NULL,
                                           [MsgDataMaxSize] [int] NOT NULL,
                                           [SendLimit] [int] NOT NULL,
                                           [ReceiveLimit] [int] NOT NULL,
                                           [MinExpirationMinutes] [int] NOT NULL DEFAULT(1),
                                           [MaxExpirationMinutes] [int] NOT NULL DEFAULT(1440),
                                           CONSTRAINT [PK_{0}] PRIMARY KEY CLUSTERED 
                                           ( [Name] ASC ) 
                                           WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                                           ) ON [PRIMARY]", tableName);

                    con.AdHocExecuteNonQuery(cmd);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        // Drops the metadata table.  Note that this doesn't delete the roles or
        // mailbox tables, as those can exist without the metadata (though you 
        // won't be able to do anything with the mailboxes until the metadata
        // table is re-created and the mailboxes re-added).
        public static Error DropTable(SqlConnectionHelper con)
        {
            Context ctx = RlineContext.Create();

            try
            {
                string tableName = GetTableName();

                if (con.CheckTableExists(tableName))
                {
                    con.AdHocExecuteNonQuery("DROP TABLE " + tableName);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Adds an entry for a mailbox to the metadata table.
        public static Error CreateMailboxInfo(SqlConnectionHelper con,
                                              MailboxInfo mailboxInfo)
        {
            Context ctx = RlineContext.Create(mailboxInfo.Name);

            try
            {
                SqlCmdParams cmdParams = new SqlCmdParams();
                string cols = "Name, BroadcastOnly, DefaultRoleName, RoleOverrideMode, UserDataMaxSize, MsgDataMaxSize, SendLimit, ReceiveLimit, MinExpirationMinutes, MaxExpirationMinutes";
                string vals = "@Name, @BroadcastOnly, @DefaultRoleName, @RoleOverrideMode, @UserDataMaxSize, @MsgDataMaxSize, @SendLimit, @ReceiveLimit, @MinExpirationMinutes, @MaxExpirationMinutes";

                string s = string.Format("INSERT INTO {0} ({1}) VALUES({2})", MailboxMetadata.GetTableName(), cols, vals);

                cmdParams.Add("@Name", mailboxInfo.Name);
                cmdParams.Add("@BroadcastOnly", mailboxInfo.BroadcastOnly);
                cmdParams.Add("@DefaultRoleName", mailboxInfo.DefaultRoleName);
                cmdParams.Add("@RoleOverrideMode", Mailbox.MailboxRoleOverrideModeToString(mailboxInfo.RoleOverrideMode));
                cmdParams.Add("@UserDataMaxSize", (int)mailboxInfo.UserDataMaxSize);
                cmdParams.Add("@MsgDataMaxSize", (int)mailboxInfo.MsgDataMaxSize);
                cmdParams.Add("@SendLimit", (int)mailboxInfo.SendLimit);
                cmdParams.Add("@ReceiveLimit", (int)mailboxInfo.ReceiveLimit);
                cmdParams.Add("@MinExpirationMinutes", (int)mailboxInfo.MinExpirationMinutes);
                cmdParams.Add("@MaxExpirationMinutes", (int)mailboxInfo.MaxExpirationMinutes);

                if (con.ExecuteNonQuery(s, cmdParams) != 1)
                {
                    return ctx.Error(ErrorCode.DbCommandFailed);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Removes a mailbox entry in the metadata table.
        public static Error DeleteMailboxInfo(SqlConnectionHelper con,
                                              string mailboxName)
        {
            Context ctx = RlineContext.Create(mailboxName);

            try
            {
                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@Name", mailboxName);

                con.ExecuteNonQuery("DELETE FROM " + GetTableName() + " WHERE Name=@Name", cmdParams);

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Get info for specified mailbox, or all mailboxes if mailboxName is null or empty.
        public static Error QueryMailboxInfo(SqlConnectionHelper con,
                                             string mailboxName,
                                             out List<MailboxInfo> infos)
        {
            Context ctx = RlineContext.Create(mailboxName);
            infos = null;

            try
            {
                //If the title hasn't been setup for mailboxes, it's ok.
                if (!con.CheckTableExists(GetTableName()))
                {
                    infos = new List<MailboxInfo>();
                    return ctx.Success2("This title is not configured for mailboxes.");
                }

                string s = "SELECT * FROM " + GetTableName();
                SqlCmdParams cmdParams = new SqlCmdParams();

                if ((mailboxName != null) && (mailboxName.Length > 0))
                {
                    s += string.Format(" WHERE Name=@Name", mailboxName);
                    cmdParams.Add("@Name", mailboxName);
                }

                using (DbDataReader reader = con.ExecuteReader(s, cmdParams))
                {
                    infos = new List<MailboxInfo>();

                    while (reader.Read())
                    {
                        MailboxInfo info = new MailboxInfo();

                        info.Name = reader["Name"].ToString();
                        info.BroadcastOnly = (bool)reader["BroadcastOnly"];
                        info.DefaultRoleName = reader["DefaultRoleName"].ToString();
                        info.RoleOverrideMode = Mailbox.MailboxRoleOverrideModeFromString(reader["RoleOverrideMode"].ToString());
                        info.UserDataMaxSize = (uint)((int)reader["UserDataMaxSize"]);
                        info.MsgDataMaxSize = (uint)((int)reader["MsgDataMaxSize"]);
                        info.SendLimit = (uint)((int)reader["SendLimit"]);
                        info.ReceiveLimit = (uint)((int)reader["ReceiveLimit"]);
                        info.CreateDateTime = (DateTime)reader["CreateDateTime"];
                        info.MinExpirationMinutes = (uint)((int)reader["MinExpirationMinutes"]);
                        info.MaxExpirationMinutes = (uint)((int)reader["MaxExpirationMinutes"]);

                        infos.Add(info);
                    }
                }

                return ctx.Success2("Found {0} mailboxes", infos.Count);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Helper class for GetCachedMailboxInfo().
        private class CachedMailboxInfoUserData
        {
            public int TitleId;
            public string MailboxName;
        }

        //PURPOSE
        //  Retrieves MailboxInfo from a cache, adding it to the
        //  cache if not found.
        //RETURNS
        //  Like all GetCachedXXX() methods, it is assumed that this will 
        //  succeed and return a non-null instance.  
        //  Errors are communicated as exceptions, to make the calling code less verbose.
        public static MailboxInfo GetCachedMailboxInfo(int titleId,
                                                       string mailboxName)
        {
            CachedMailboxInfoUserData userdata = new CachedMailboxInfoUserData();
            userdata.TitleId = titleId;
            userdata.MailboxName = mailboxName;

            MailboxInfo info = CacheHelper<MailboxInfo>.Get(string.Format("MailboxInfo_{0}_{1}", titleId, mailboxName),
                                                            CreateMailboxInfoDelegate,
                                                            userdata);

            if (info == null)
            {
                throw (new Exception(string.Format("Could not get mailbox {0} from cache", mailboxName)));
            }

            return info;
        }

        //PURPOSE
        //  Object creation callback for GetCachedMailboxInfo().
        public static void CreateMailboxInfoDelegate(object userdata,
                                                     out MailboxInfo obj,
                                                     out DateTime absoluteExpirationTime)
        {
            obj = null;
            absoluteExpirationTime = DateTime.Now;

            CachedMailboxInfoUserData cmiUserData = userdata as CachedMailboxInfoUserData;

            if (cmiUserData == null)
            {
                throw (new InvalidCastException("userdata is not CachedMailboxInfoUserData"));
            }

            TitleInfo titleInfo = Title.GetCachedTitleInfo(cmiUserData.TitleId);

            using (SqlConnectionHelper con = Title.Open(titleInfo))
            {
                List<MailboxInfo> mailboxInfos = null;

                MailboxMetadata.QueryMailboxInfo(con, cmiUserData.MailboxName, out mailboxInfos);

                if (mailboxInfos.Count > 0)
                {
                    obj = mailboxInfos[0];
                    absoluteExpirationTime = DateTime.Now.AddMinutes(CACHE_ABSOLUTE_EXPIRATION_MINUTES);
                }
            }
        }
    }
}
