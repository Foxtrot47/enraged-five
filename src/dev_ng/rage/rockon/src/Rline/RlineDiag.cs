using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using Rockstar.Rockon.Diag;

namespace Rockstar.Rockon.Rline
{
    //PURPOSE
    //  Error codes specific to rline.
    public enum RlineErrorCode
    {
        AlreadyInMatch,
        MatchAlreadyStarted,
        MatchAlreadyEnded,
        MatchNotStarted,
        NoJoinInProgress,
        NotInMatch,
    }

    public class RlineChannel
    {
        private static Channel m_Channel = new Channel("Rline");

        public static Channel Channel
        {
            get { return m_Channel; }
        }
    }

    public class RlineContext
    {
        public static Context Create()
        {
            return new Context(RlineChannel.Channel);
        }

        public static Context Create(string desc)
        {
            return new Context(RlineChannel.Channel, desc);
        }

        public static Context Create(string format, params object[] args)
        {
            return new Context(RlineChannel.Channel, format, args);
        }
    }
}
