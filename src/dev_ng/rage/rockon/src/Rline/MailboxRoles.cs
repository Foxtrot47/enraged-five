using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.Caching;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Rline
{
    public class MailboxRoles
    {
        //TODO: Make configurable
        private const int CACHE_ABSOLUTE_EXPIRATION_MINUTES = 60 * 24; //Daily

        //PURPOSE
        //  These roles are automatically added to the Roles table when it is created.
        private static string[,] m_StockRoles = 
        {
            //Format: {RoleName, PermissionsString}
            { "Admin", "All" },
            { "Banned", "None" },
            { "ReadOnly", "ReadMyReceived" },
            { "Personal", "SendPersonal,ReadMyReceived,ReadMySent,DeleteMyReceived,DeleteMySent" }
        };

        //PURPOSE
        //  Returns name of mailbox roles table.
        public static string GetTableName()
        {
            return Mailbox.MAILBOX_TABLE_PREFIX + "Roles";
        }

        //PURPOSE
        //  Creates the mailbox roles table.
        public static Error CreateTable(SqlConnectionHelper con)
        {
            Context ctx = RlineContext.Create();

            try
            {
                string tableName = GetTableName();

                if (!con.CheckTableExists(tableName))
                {
                    //Create the table
                    string cmd = "CREATE TABLE " + tableName + Environment.NewLine;

                    cmd += string.Format(@"([Name] [varchar](255) NOT NULL,
                                          [CreateDateTime] [datetime] NOT NULL DEFAULT(GETUTCDATE()),
                                          [Permissions] [varchar](512) NOT NULL,
                                          CONSTRAINT [PK_{0}] PRIMARY KEY CLUSTERED 
                                          ([Name] ASC)
                                          WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                                         ) ON [PRIMARY]", tableName);

                    con.AdHocExecuteNonQuery(cmd);

                    //Populate with stock roles
                    for (int i = 0; i < m_StockRoles.Length/2; i++)
                    {
                        Error err = CreateRole(con, m_StockRoles[i, 0], m_StockRoles[i, 1]);
                        if(err != null)
                        {
                            return ctx.Error(err);
                        }
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        // Drops the role override table, if it exists
        public static Error DropTable(SqlConnectionHelper con)
        {
            Context ctx = RlineContext.Create();

            try
            {
                string tableName = GetTableName();

                if (con.CheckTableExists(tableName))
                {
                    con.AdHocExecuteNonQuery("DROP TABLE " + tableName);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Adds a role to the roles table.
        public static Error CreateRole(SqlConnectionHelper con,
                                       string name,
                                       string permissions)
        {
            Context ctx = RlineContext.Create(name);

            try
            {
                string s = string.Format("INSERT INTO {0} (Name, Permissions) VALUES(@Name, @Permissions)", GetTableName());

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@Name", name);
                cmdParams.Add("@Permissions", permissions);

                if (con.ExecuteNonQuery(s, cmdParams) != 1)
                {
                    return ctx.Error(ErrorCode.DbCommandFailed,
                                     "Failed to add role {0} (permissions={1})",
                                     name,
                                     permissions);
                }
                
                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Deletes a role from the table.
        public static Error DeleteRole(SqlConnectionHelper con, 
                                       string name)
        {
            Context ctx = RlineContext.Create(name);

            try
            {
                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@Name", name);

                con.ExecuteNonQuery(string.Format("DELETE FROM {0} WHERE Name=@Name", GetTableName()), cmdParams);

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Queries mailbox role info by name (or all if name is null).
        public static Error QueryMailboxRoleInfo(SqlConnectionHelper con,
                                                 string roleName,
                                                 uint maxResults,
                                                 out List<MailboxRoleInfo> infos)
        {
            Context ctx = RlineContext.Create("rolename={0}", roleName);
            infos = null;

            try
            {
                string s = string.Format("SELECT * FROM {0}", GetTableName());
                SqlCmdParams cmdParams = new SqlCmdParams();

                if (roleName != null && roleName.Length > 0)
                {
                    s += " WHERE Name=@RoleName";
                    cmdParams.Add("@RoleName", roleName);
                }

                using (DbDataReader reader = con.ExecuteReader(s, cmdParams))
                {
                    infos = new List<MailboxRoleInfo>();

                    while (reader.Read())
                    {
                        MailboxRoleInfo info = new MailboxRoleInfo();
                        info.Name = reader["Name"].ToString();
                        info.Permissions = 0;

                        if (!(reader["Permissions"] is System.DBNull))
                        {
                            info.Permissions = MailboxPermission.FromString(reader["Permissions"].ToString());
                        }

                        infos.Add(info);

                        if ((maxResults > 0) && (infos.Count >= maxResults))
                        {
                            break;
                        }
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Helper class for GetCachedMailboxInfo().  Contains data needed to
        //  regenerate the cached item.
        private class CachedMailboxRoleInfoUserData
        {
            public int TitleId;
            public string RoleName;
        }

        //PURPOSE
        //  Retrieves MailboxRoleInfo from a cache, adding it to the
        public static MailboxRoleInfo GetCachedMailboxRoleInfo(int titleId,
                                                               string roleName)
        {
            CachedMailboxRoleInfoUserData userdata = new CachedMailboxRoleInfoUserData();
            userdata.TitleId = titleId;
            userdata.RoleName = roleName;

            MailboxRoleInfo info = CacheHelper<MailboxRoleInfo>.Get(string.Format("MailboxRoleInfo{0}_{1}", titleId, roleName),
                                                                    CreateMailboxRoleInfoDelegate,
                                                                    userdata);

            if (info == null)
            {
                throw (new Exception(string.Format("Could not get mailbox role {0} from cache", roleName)));
            }

            return info;
        }

        //PURPOSE
        //  Object creation callback for GetCachedMailboxRoleInfo().
        public static void CreateMailboxRoleInfoDelegate(object userdata,
                                                         out MailboxRoleInfo obj,
                                                         out DateTime absoluteExpirationTime)
        {
            obj = null;
            absoluteExpirationTime = DateTime.Now;

            CachedMailboxRoleInfoUserData cmiUserData = userdata as CachedMailboxRoleInfoUserData;

            if (cmiUserData == null)
            {
                throw (new InvalidCastException("userdata is not CachedMailboxRoleInfoUserData"));
            }

            TitleInfo titleInfo = Title.GetCachedTitleInfo(cmiUserData.TitleId);

            using (SqlConnectionHelper con = Title.Open(titleInfo))
            {
                List<MailboxRoleInfo> infos = null;

                MailboxRoles.QueryMailboxRoleInfo(con, cmiUserData.RoleName, 1, out infos);

                if (infos.Count > 0)
                {
                    obj = infos[0];
                    absoluteExpirationTime = DateTime.Now.AddMinutes(CACHE_ABSOLUTE_EXPIRATION_MINUTES);
                }
            }
        }

    }
}
