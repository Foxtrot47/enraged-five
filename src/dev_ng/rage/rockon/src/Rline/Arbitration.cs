using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Rline
{
    public class Arbitration
    {       
        //PURPOSE
        //  Helper class for tracking most often reported value for a property.
        class PropValueCounter
        {
            public XblProperty Prop;
            public int HighestCount = 0;
            public DateTime SubmitTime = DateTime.MaxValue;
            public Hashtable ValueCounts = new Hashtable();

            public void Add(XblProperty p,
                            DateTime submitTime)
            {
                object val = p.Value;

                if (ValueCounts[val] == null)
                {
                    ValueCounts.Add(val, (int)0);
                }

                int count = (int)ValueCounts[val] + 1;
                ValueCounts[val] = count;

                if ((count > HighestCount) || ((count == HighestCount) && (submitTime < SubmitTime)))
                {
                    if ((count > HighestCount) || (submitTime < SubmitTime))
                    {
                        SubmitTime = submitTime;
                    }

                    HighestCount = count;
                    Prop = p;
                }
            }
        }

        static PropValueCounter GetPropValueCounter(ref Dictionary<Int64, Dictionary<uint, Dictionary<uint, PropValueCounter>>> dict,
                                                    Int64 accountId,
                                                    uint lbId,
                                                    XblProperty p)
        {
            Dictionary<uint, Dictionary<uint, PropValueCounter>> lbDict = null;
            if (dict.ContainsKey(accountId))
            {
                lbDict = dict[accountId] as Dictionary<uint, Dictionary<uint, PropValueCounter>>;
            }
            else
            {
                lbDict = new Dictionary<uint, Dictionary<uint, PropValueCounter>>();
                dict.Add(accountId, lbDict);
            }

            Dictionary<uint, PropValueCounter> propDict = null;
            if (lbDict.ContainsKey(lbId))
            {
                propDict = lbDict[lbId] as Dictionary<uint, PropValueCounter>;
            }
            else
            {
                propDict = new Dictionary<uint, PropValueCounter>();
                lbDict.Add(lbId, propDict);
            }

            PropValueCounter pvc = null;
            if (propDict.ContainsKey(p.Id))
            {
                pvc = propDict[p.Id] as PropValueCounter;
            }
            else
            {
                pvc = new PropValueCounter();
                propDict.Add(p.Id, pvc);
            }

            return pvc;
        }

        //PURPOSE
        //  Given a list of submissions for the same match, arbitrates the final values 
        //  for each combination of lb+targetgamer+prop.
        private static Error ArbitrateSubmissions(int matchId,
                                                  List<SubmissionInfo> subList,
                                                  out List<SubmissionInfo> arbList)
        {
            Context ctx = RlineContext.Create("match={0}", matchId);
            arbList = null;

            try
            {
                //Figure out which prop values were submitted most to each LB for each gamer.
                //Dict lookup goes: accountId->lbId->propId->PropValueCounter.
                Dictionary<Int64, Dictionary<uint, Dictionary<uint, PropValueCounter>>> dict =
                    new Dictionary<Int64, Dictionary<uint, Dictionary<uint, PropValueCounter>>>();

                Dictionary<Int64, string> accountIdToName = new Dictionary<Int64, string>();

                foreach (SubmissionInfo sub in subList)
                {
                    if (sub.MatchId != matchId)
                    {
                        return ctx.Error(ErrorCode.UnknownError, "Sub match ID ({0}) is not match's ({1})", sub.MatchId, matchId);
                    }

                    if (!accountIdToName.ContainsKey(sub.TargetAccountId))
                    {
                        accountIdToName.Add(sub.TargetAccountId, sub.TargetDisplayName);
                    }

                    foreach (XblProperty p in sub.Props)
                    {
                        PropValueCounter pvc = GetPropValueCounter(ref dict,
                                                                   sub.TargetAccountId,
                                                                   sub.LbId,
                                                                   p);
                        pvc.Add(p, sub.DateTime);
                    }
                }

                //Compose list of final submissions
                arbList = new List<SubmissionInfo>();

                foreach (KeyValuePair<Int64, Dictionary<uint, Dictionary<uint, PropValueCounter>>> kvpGamer in dict)
                {
                    foreach (KeyValuePair<uint, Dictionary<uint, PropValueCounter>> kvpLb in kvpGamer.Value)
                    {
                        SubmissionInfo sub = new SubmissionInfo();

                        sub.DateTime = DateTime.UtcNow;
                        sub.MatchId = matchId;
                        sub.LbId = kvpLb.Key;
                        sub.SubmitterAccountId = 0; //0 indicates an arbitrated submission
                        sub.TargetAccountId = kvpGamer.Key;
                        sub.TargetDisplayName = (string)accountIdToName[sub.TargetAccountId];

                        sub.Props = new List<XblProperty>();

                        foreach (KeyValuePair<uint, PropValueCounter> kvp in kvpLb.Value)
                        {
                            sub.Props.Add(kvp.Value.Prop);
                        }

                        arbList.Add(sub);
                    }
                }

                return ctx.Success2("{0} submissions after arb", arbList.Count);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE 
        //  Performs arbitration for all matches for specified title.
        public static Error ArbitrateMatches(SqlConnectionHelper con)
        {
            Context ctx = RlineContext.Create();

            try
            {
                Error err = null;

                List<MatchInfo> matches;

                if (null != (err = Match.GetMatchesReadyForArbitration(con, 0, out matches)))
                {
                    return ctx.Error(err);
                }

                foreach (MatchInfo m in matches)
                {
                    if (!m.IsArbitrated)
                    {
                        //This should never happen, but we shouldn't allow it to stop processing
                        ctx.Error(ErrorCode.UnknownError, "Match {0} is not actually arbitrated!", m.Id);
                        continue;
                    }

                    //Get all submissions for the match
                    List<SubmissionInfo> submissionInfos;
                    if (null != (err = Submission.QuerySubmission(con, true, m.Id, false, 0, out submissionInfos)))
                    {
                        return ctx.Error(err);
                    }

                    //Arbitrate the match submissions into a final list
                    List<SubmissionInfo> finalSubList;
                    if (null != (err = ArbitrateSubmissions(m.Id, submissionInfos, out finalSubList)))
                    {
                        return ctx.Error(err);
                    }

                    //Commit the arbitrated submissions to the submissions table
                    foreach (SubmissionInfo s in finalSubList)
                    {
                        err = Submission.CreateRawSubmission(con,
                                                                false,
                                                                s.MatchId,
                                                                s.SubmitterAccountId,
                                                                s.TargetAccountId,
                                                                s.TargetDisplayName,
                                                                s.LbId,
                                                                s.Props);

                        if (null != err)
                        {
                            return ctx.Error(err);
                        }
                    }

                    //Delete all the arbitrated submissions we just processed
                    if (null != (err = Submission.DeleteSubmission(con, m.Id, true, 0)))
                    {
                        return ctx.Error(err);
                    }

                    //Delete all the match's gamers
                    if (null != (err = MatchMember.RemoveMatchMember(con, m.Id, 0)))
                    {
                        return ctx.Error(err);
                    }

                    //Delete the match itself
                    if (null != (err = Match.DeleteMatch(con, m.Id)))
                    {
                        return ctx.Error(err);
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }
}
