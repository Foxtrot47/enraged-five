using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Rline
{
    public class Submission
    {
        public static string GetSubmissionsTableName(bool arbitrated)
        {
            //string s = SqlHelper.UseSqlServer ? "dbo." : "";
            string s = "";

            s += arbitrated ? "ArbSubmissions" : "Submissions";

            return s;
        }

        public static bool CreateTables(SqlConnectionHelper con,
                                        IEnumerable<XblProperty> properties)
        {
            return CreateSubmissionsTable(con, false, properties) &&
                   CreateSubmissionsTable(con, true, properties);
        }

        public static bool CreateSubmissionsTable(SqlConnectionHelper con,
                                                  bool arbitrated,
                                                  IEnumerable<XblProperty> properties)
        {
            List<SqlColumn> columns = new List<SqlColumn>();

            columns.Add(new SqlColumn("MatchId",
                                      new SqlDataType(SqlDataType.DataType.Int),
                                      false,
                                      true,
                                      null));
            columns.Add(new SqlColumn("LbId",
                                      new SqlDataType(SqlDataType.DataType.Int),
                                      false,
                                      true,
                                      null));
            columns.Add(new SqlColumn("SubmitterAccountId",
                                      new SqlDataType(SqlDataType.DataType.BigInt),
                                      false,
                                      true,
                                      null));
            columns.Add(new SqlColumn("TargetAccountId",
                                      new SqlDataType(SqlDataType.DataType.BigInt),
                                      false,
                                      true,
                                      null));
            columns.Add(new SqlColumn("TargetDisplayName",
                                      new SqlDataType(SqlDataType.DataType.VarChar, 20),
                                      false,
                                      false,
                                      null));
            columns.Add(new SqlColumn("CreateDateTime",
                                      new SqlDataType(SqlDataType.DataType.DateTime),
                                      false,
                                      false,
                                      "GETUTCDATE()"));

            foreach (XblProperty p in properties)
            {
                columns.Add(new SqlColumn(string.Format("Prop_0x{0}", p.Id.ToString("X8")),
                                          Xbl.GetSqlDataType(p.GetXblDataType()),
                                          true,
                                          false,
                                          "NULL"));
            }

            return SqlTableHelper.Create(con,
                                         GetSubmissionsTableName(arbitrated),
                                         columns,
                                         SqlTableHelper.CreateMode.Replace);
        }

        public static Error QuerySubmission(SqlConnectionHelper con,                                                                 
                                            bool arbitrated,
                                            int matchId,
                                            bool sort,
                                            uint maxResults,
                                            out List<SubmissionInfo> submissionInfos)
        {
            Context ctx = RlineContext.Create("arb={0}, matchId={1}, sort={2}, max={3}",
                                           arbitrated, matchId, sort, maxResults);
            submissionInfos = null;

            try
            {
                string s = string.Format("SELECT * FROM {0}", arbitrated ? "ArbSubmissions" : "Submissions");

                if (matchId != 0)
                {
                    s += string.Format(" WHERE MatchId = {0}", matchId);
                }

                if (sort)
                {
                    s += string.Format(" ORDER BY CreateDateTime ASC");
                }

                using(DbDataReader reader = con.AdHocExecuteReader(s))
                {
                    submissionInfos = new List<SubmissionInfo>();

                    List<uint> propIds = null;

                    while (reader.Read())
                    {
                        //Cache the IDs of property columns.
                        if (propIds == null)
                        {
                            propIds = new List<uint>();

                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i).StartsWith("Prop_0x"))
                                {
                                    string propName = reader.GetName(i).Replace("Prop_0x", "");
                                    propIds.Add(uint.Parse(propName, System.Globalization.NumberStyles.HexNumber));
                                }
                            }
                        }

                        SubmissionInfo sub = new SubmissionInfo();

                        sub.MatchId = (int)reader["MatchId"];
                        sub.LbId = (uint)((int)reader["LbId"]);
                        sub.SubmitterAccountId = (Int64)reader["SubmitterAccountId"];
                        sub.TargetAccountId = (Int64)reader["TargetAccountId"];
                        sub.TargetDisplayName = reader["TargetDisplayName"].ToString();
                        sub.DateTime = (DateTime)reader["CreateDateTime"];

                        foreach(uint propId in propIds)
                        {
                            object col = reader[String.Format("Prop_0x{0}", propId.ToString("X8"))];

                            if ((col != null) && !(col is System.DBNull))
                            {
                                sub.Props.Add(new XblProperty(propId, col));
                            }
                        }

                        submissionInfos.Add(sub);

                        if ((maxResults != 0) && (submissionInfos.Count >= maxResults))
                        {
                            break;
                        }
                    }

                    reader.Close();
                }

                return ctx.Success2("Found {0}", submissionInfos.Count);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Creates an entry in the submissions (or arbitrated submissions) table.
        //  This is "raw" because it doesn't have any sanity checks.  It is assumed that the 
        //  caller has verified the parameters are valid.
        //NOTE
        //  The caller MUST have done sanity checking, or the DB could potentially get 
        //  out of sync.
        public static Error CreateRawSubmission(SqlConnectionHelper con,
                                                bool isArbitrated,
                                                int matchId,
                                                Int64 submitterAccountId,
                                                Int64 targetAccountId,
                                                string targetDisplayName,
                                                uint lbId,
                                                List<XblProperty> props)
        {
            Context ctx = RlineContext.Create("arb={0}, match={1}, sub={2}, targ={3}, name={4}, lb={5}",
                                           isArbitrated, matchId, submitterAccountId, targetAccountId, targetDisplayName, lbId);

            try
            {
                //Determine the table we're writing to.
                string tableName = isArbitrated ? "ArbSubmissions" : "Submissions";

                //Build submission and write to table.
                string cols = String.Format("LbId, MatchId, SubmitterAccountId, TargetAccountId, TargetDisplayName");
                string vals = String.Format("{0}, {1}, {2}, {3}, '{4}'", lbId, matchId, submitterAccountId, targetAccountId, targetDisplayName);

                foreach (XblProperty p in props)
                {
                    cols += String.Format(", Prop_0x{0}", p.Id.ToString("X8"));
                    vals += String.Format(", {0}", p.Value.ToString());
                }

                string s = String.Format("INSERT INTO {0} ({1}) VALUES ({2})", tableName, cols, vals);

                if (con.AdHocExecuteNonQuery(s) != 1)
                {
                    return ctx.Error(ErrorCode.DbCommandFailed);
                }

                return ctx.Success2();
            }
            catch(Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Deletes one or all submissions for specified match.
        public static Error DeleteSubmission(SqlConnectionHelper con,
                                             int matchId,
                                             bool isSubArbitrated,
                                             int submissionId)
        {
            Context ctx = RlineContext.Create(
                                      "Submission.DeleteSubmission(match={0}, arb={1}, sub={2})",
                                      matchId, isSubArbitrated, submissionId);

            try
            {
                string s = String.Format("DELETE FROM {0} WHERE MatchId={1}",
                                         isSubArbitrated ? "ArbSubmissions" : "Submissions",
                                         matchId);

                if (submissionId != 0)
                {
                    s += String.Format(" AND SubmissionId={0}", submissionId);
                }

                if (con.AdHocExecuteNonQuery(s) == 0)
                {
                    return ctx.Error(ErrorCode.DbCommandFailed);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }
}
