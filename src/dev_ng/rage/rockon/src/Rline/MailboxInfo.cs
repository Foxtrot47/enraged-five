using System;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Rockstar.Rockon.Rline
{
    //PURPOSE
    //  Describes a mail message sent from one account directly to another.
    public class MailboxRecord
    {
        public Int64 RecordId;                  //Unique ID of record
        public Int64 SenderId;                  //Sender Rockon account ID
        public string SenderDisplayName;        //Sender display name
        public Int64 ReceiverId;                //Receiver Rockon account ID
        public DateTime SendDateTime;           //Time message was sent
        public uint UserDataLength;             //Size of user data.  This is stored separately, because the user might not request the data.
        public uint MsgDataLength;              //Size of msg data.  This is stored separately, because the user might not request the data.
        public byte[] UserData;                 //Data that both sender and receiver can change at anytime. 
        public byte[] MsgData;                  //Data that only sender can set, and only at time of sending.
        public DateTime ExpirationDateTime;     //Time message expires
    }

    //PURPOSE
    //  Describes a mailbox table.
    public class MailboxInfo
    {
        public string Name;                     //Mailbox name
        public bool BroadcastOnly;              //Broadcast only?
        public string DefaultRoleName;          //Default role for the mailbox
        public MailboxRoleOverrideMode RoleOverrideMode;    //How to apply permissions in role override table
        public uint UserDataMaxSize;            //Max size of UserData
        public uint MsgDataMaxSize;             //MaxSize of MsgData
        public uint SendLimit;                  //Number of msgs an account can have sent in the table
        public uint ReceiveLimit;               //Number of msgs an account can have to receive in the table 
        public uint MinExpirationMinutes;       //Min minutes before message is deleted automatically
        public uint MaxExpirationMinutes;       //Max minutes before message is deleted automatically
        public DateTime CreateDateTime;         //Time mailbox was created
    }

    //PURPOSE
    //  Each mailbox action requires one or more permissions, represented by 
    //  bitflags.
    //IMPORTANT
    //  If you add or remove any permissions, you MUST update Mailbox.QueryMailboxRoleInfo
    //  to match, or they will fail to read in from the DB.
    public class MailboxPermission
    {
        public const uint All = 0xFFFFFF;
        public const uint None = 0;

        public const uint SendPersonal = 1 << 0;        //Can send to individuals
        public const uint SendBroadcast = 1 << 1;       //Can send broadcasts
        public const uint SendAll = SendPersonal | SendBroadcast;

        public const uint ReadMyReceived = 1 << 2;      //Can read msgs addressed to you, and broadcasts
        public const uint ReadMySent = 1 << 3;          //Can read msgs addressed to others if sent by you
        public const uint ReadAll = 1 << 4;             //Can read msgs sent to/from anyone

        public const uint DeleteMyReceived = 1 << 5;    //Can delete msgs addressed to you (but not broadcasts)
        public const uint DeleteMySent = 1 << 6;        //Can delete msgs sent by you
        public const uint DeleteAll = 1 << 7;           //Can delete any msg

        public static bool CanSendAll(uint permissions)
        {
            return (permissions & SendAll) == SendAll;
        }

        public static bool CanSendNone(uint permissions)
        {
            return (permissions & SendAll) == 0;
        }

        public static bool CanReadAll(uint permissions)
        {
            return (permissions & ReadAll) == ReadAll;
        }

        public static bool CanReadNone(uint permissions)
        {
            return (permissions & ReadAll) == 0;
        }

        public static bool CanDeleteAll(uint permissions)
        {
            return (permissions & DeleteAll) == DeleteAll;
        }

        public static bool CanDeleteNone(uint permissions)
        {
            return (permissions & DeleteAll) == 0;
        }

        public static bool Check(uint permissions, uint toCheck)
        {
            return (permissions & toCheck) == toCheck;
        }

        public static string ToString(uint permissions)
        {
            if (All == permissions) return "All";
            if (None == permissions) return "None";

            StringBuilder sb = new StringBuilder();

            if (Check(permissions, SendAll))
            {
                sb.Append("SendAll,");
            }
            else
            {
                if (Check(permissions, SendPersonal)) sb.Append("SendPersonal,");
                if (Check(permissions, SendBroadcast)) sb.Append("SendBroadcast,");
            }

            if (Check(permissions, ReadAll))
            {
                sb.Append("ReadAll,");
            }
            else
            {
                if (Check(permissions, ReadMyReceived)) sb.Append("ReadMyReceived,");
                if (Check(permissions, ReadMySent)) sb.Append("ReadMySent,");
            }

            if (Check(permissions, DeleteAll))
            {
                sb.Append("DeleteAll,");
            }
            else
            {
                if (Check(permissions, DeleteMyReceived)) sb.Append("DeleteMyReceived,");
                if (Check(permissions, DeleteMySent)) sb.Append("DeleteMySent,");
            }

            char[] delimiters = new char[] { ',' };
            return sb.ToString().TrimEnd(delimiters);
        }

        public static uint FromString(string permissions)
        {
            char[] delimiters = new char[] { ',' };
            string[] permissionNames = permissions.Split(delimiters);
            uint p = 0;

            for (int i = 0; i < permissionNames.Length; i++)
            {
                switch (permissionNames[i].Trim().ToLower())
                {
                    case "none": p = 0; break;
                    case "all": p |= All; break;
                    case "sendpersonal": p |= SendPersonal; break;
                    case "sendbroadcast": p |= SendBroadcast; break;
                    case "sendall": p |= SendAll; break;
                    case "readmyreceived": p |= ReadMyReceived; break;
                    case "readmysent": p |= ReadMySent; break;
                    case "readall": p |= ReadAll; break;
                    case "deletemyreceived": p |= DeleteMyReceived; break;
                    case "deletemysent": p |= DeleteMySent; break;
                    case "deleteall": p |= DeleteAll; break;
                    default: throw new ArithmeticException("Unknown permission name: " + permissionNames[i]);
                }
            }

            return p;
        }
    }

    //PURPOSE
    //  A role is a named set of permissions.
    public class MailboxRoleInfo
    {
        public string Name;
        public uint Permissions;
    }

    //PURPOSE
    //  Modes for how to combine a mailbox's default role permissions 
    //  with the override permissions specified for an account.
    public enum MailboxRoleOverrideMode
    {
        None,       //No role overrides for this mailbox.
        Replace,    //Use account's role override instead of default role.
        Add,        //Add (OR) account's role override permissions with default role's.
        Remove      //Remove account's role override permissions from default role's.
    }

    //PURPOSE
    //  Describes an entry in a role overrides table.  The account will
    //  have the specified role's permissions combined with the mailbox
    //  default role according the mailbox's role override mode.
    public class MailboxRoleOverrideInfo
    {
        public Int64 AccountId;
        public string RoleName;
    }
}
