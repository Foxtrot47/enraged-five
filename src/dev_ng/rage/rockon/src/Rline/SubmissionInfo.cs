using System;
using System.Collections;
using System.Collections.Generic;

namespace Rockstar.Rockon.Rline
{
    //PURPOSE
    //  A single submission of properties for a specific leaderboard and gamer.
    public class SubmissionInfo
    {
        public uint LbId;
        public int MatchId;
        public Int64 SubmitterAccountId;
        public Int64 TargetAccountId;
        public string TargetDisplayName;
        public DateTime DateTime = DateTime.MaxValue;
        public List<XblProperty> Props = new List<XblProperty>();
    }
}
