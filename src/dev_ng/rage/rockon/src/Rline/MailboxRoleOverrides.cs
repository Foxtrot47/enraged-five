using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.Caching;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Rline
{
    public class MailboxRoleOverrides
    {
        //PURPOSE
        //  Returns the name of the role overrides table for a given mailbox.
        public static string GetTableName(string mailboxName)
        {
            return Mailbox.GetTableName(mailboxName) + "_RoleOverrides";
        }

        //PURPOSE
        //  Creates the role overrides table for a mailbox.
        public static Error CreateTable(SqlConnectionHelper con,
                                        string mailboxName)
        {
            Context ctx = RlineContext.Create(mailboxName);

            try
            {
                string tableName = GetTableName(mailboxName);

                if (!con.CheckTableExists(tableName))
                {
                    string cmd = "CREATE TABLE " + tableName + Environment.NewLine;

                    cmd += string.Format(
                           @"([AccountId] [bigint] NOT NULL,
                              [RoleName] [varchar](50) NOT NULL,
                              [CreateDateTime] [datetime] NOT NULL DEFAULT(GETUTCDATE()),
                           CONSTRAINT [PK_{0}] PRIMARY KEY CLUSTERED 
                           (
                            [AccountId] ASC
                           )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                           ) ON [PRIMARY]", tableName);

                    con.AdHocExecuteNonQuery(cmd);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        // Drops the role override table for a mailbox, if one exists.
        public static Error DropTable(SqlConnectionHelper con,
                                      string mailboxName)
        {
            Context ctx = RlineContext.Create(mailboxName);

            try
            {
                string tableName = GetTableName(mailboxName);

                if (con.CheckTableExists(tableName))
                {
                    con.AdHocExecuteNonQuery("DROP TABLE " + tableName);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Creates a role override for specified account and mailbox.
        public static Error CreateRoleOverride(SqlConnectionHelper con,
                                               Int64 accountId,
                                               string mailboxName,
                                               string roleName)
        {
            Context ctx = RlineContext.Create("id={0}, mailbox={1}", accountId, mailboxName);

            try
            {
                if (accountId == 0 || roleName == null || roleName.Length == 0)
                {
                    return ctx.Error(ErrorCode.InvalidArguments);
                }

                string s = string.Format("INSERT INTO {0} (AccountId, RoleName) VALUES(@AccountId, @RoleName)", 
                                         GetTableName(mailboxName));

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@AccountId", accountId);
                cmdParams.Add("@RoleName", roleName);

                if (con.ExecuteNonQuery(s, cmdParams) != 1)
                {
                    return ctx.Error(ErrorCode.DbCommandFailed,
                                     "Failed to add role override ({0})",
                                     roleName);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Deletes an account's role override, or all role overrides if accountId is zero.
        public static Error DeleteRoleOverride(SqlConnectionHelper con,
                                               Int64 accountId,
                                               string mailboxName)
        {
            Context ctx = RlineContext.Create("id={0}, mailbox={1}", accountId, mailboxName);

            try
            {
                SqlCmdParams cmdParams = new SqlCmdParams();
                string s = "DELETE FROM " + GetTableName(mailboxName);

                if (accountId != 0)
                {
                    s += " WHERE AccountId=@AccountId";
                    cmdParams.Add("@AccountId", accountId);
                }

                con.ExecuteNonQuery(s, cmdParams);

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Queries role override info for a mailbox, for either a single account
        //  or all accounts (if accountId is zero).  If the mailbox does not have
        //  role override table, returns an empty list.
        public static Error QueryMailboxRoleOverrideInfo(SqlConnectionHelper con,
                                                         Int64 accountId,
                                                         string mailboxName,
                                                         uint maxResults,
                                                         out List<MailboxRoleOverrideInfo> infos)
        {
            Context ctx = RlineContext.Create(accountId.ToString());
            infos = new List<MailboxRoleOverrideInfo>();

            try
            {
                string tableName = GetTableName(mailboxName);

                if (!con.CheckTableExists(tableName))
                {
                    return ctx.Success2("No role override table");
                }

                string s = string.Format("SELECT * FROM {0}", tableName);
                SqlCmdParams cmdParams = new SqlCmdParams();

                if (accountId != 0)
                {
                    s += " WHERE AccountId=@AccountId";
                    cmdParams.Add("@AccountId", accountId);
                }

                using (DbDataReader reader = con.ExecuteReader(s, cmdParams))
                {
                    while (reader.Read())
                    {
                        MailboxRoleOverrideInfo info = new MailboxRoleOverrideInfo();
                        info.AccountId = (Int64)reader["AccountId"];
                        info.RoleName = reader["RoleName"].ToString();

                        infos.Add(info);

                        if ((maxResults > 0) && (infos.Count >= maxResults))
                        {
                            break;
                        }
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //TODO: Add methods for creating, deleting, updating role overrides

    }
}
