using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Rline
{
    public class MatchMember
    {
        private const string TABLE_NAME = "rlMatchMembers";

        //PURPOSE
        //  Creates the match members table
        //PARAMS
        //  con     - Open connection using DB that table should be created in.
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        public static Error CreateTable(SqlConnectionHelper con)
        {
            Context ctx = RlineContext.Create();

            try
            {
                //Drop the table if it already existed.  This is a transient table, so no real harm here.
                if (con.CheckTableExists(TABLE_NAME))
                {
                    Error err = DropTable(con);
                    if (err != null)
                    {
                        return ctx.Error(err);
                    }
                }

                string s = "CREATE TABLE " + TABLE_NAME;

                s += @"([MatchId] [int] NOT NULL,
                      [AccountId] [bigint] NOT NULL,
                      [DisplayName] [varchar](50) NOT NULL,
                      [JoinDateTime] [datetime] NOT NULL DEFAULT(GETUTCDATE()),
                      CONSTRAINT PK_" + TABLE_NAME + " PRIMARY KEY ([MatchId], [AccountId]))";

                con.AdHocExecuteNonQuery(s);

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Drops the match members table
        //PARAMS
        //  con     - Open connection using DB that table resides in
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        public static Error DropTable(SqlConnectionHelper con)
        {
            Context ctx = RlineContext.Create();

            try
            {
                if (!con.CheckTableExists(TABLE_NAME))
                {
                    return ctx.Error(ErrorCode.DoesNotExist, "Table {0} not found", TABLE_NAME);
                }

                con.AdHocExecuteNonQuery("DROP TABLE " + TABLE_NAME);

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Returns info for a single match gamer, or all gamers for a match if accountId is 0.
        public static Error QueryMatchMember(SqlConnectionHelper con,
                                             int matchId,
                                             Int64 accountId,
                                             out List<MatchMemberInfo> infos)
        {
            Context ctx = RlineContext.Create("match={0}, acct={1}", matchId, accountId);
            infos = null;

            try
            {
                string s = String.Format("SELECT * FROM {0} WHERE MatchId={1}", TABLE_NAME, matchId);

                if (accountId != 0)
                {
                    s += String.Format(" AND AccountId={0}", accountId);
                }

                using(DbDataReader reader = con.AdHocExecuteReader(s))
                {
                    infos = new List<MatchMemberInfo>();

                    while (reader.Read())
                    {
                        MatchMemberInfo info = new MatchMemberInfo();

                        info.AccountId = (Int64)reader["AccountId"];
                        info.MatchId = (int)reader["MatchId"];
                        info.DisplayName = reader["DisplayName"].ToString();
                        info.JoinDateTime = (DateTime)reader["JoinDateTime"];

                        infos.Add(info);
                    }

                    reader.Close();
                }

                return ctx.Success2("Found {0} match members", infos.Count);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Adds a match member.
        public static Error AddMatchMember(SqlConnectionHelper con,
                                           int matchId,
                                           Int64 accountId,
                                           string displayName)
        {
            Context ctx = RlineContext.Create("match={0}, acct={1}, name={2}", 
                                            matchId, accountId, displayName);

            try
            {
                if ((matchId == 0) || (accountId == 0))
                {
                    return ctx.Error(ErrorCode.InvalidArguments);
                }

                Error err;

                //TODO: Add in some concurrency checks (like a timestamp) to make sure the
                //      match doesn't start in between reading it's info and adding the gamer.

                //Get the match info.
                MatchInfo matchInfo;
                if (null != (err = Match.QueryMatch(con, matchId, out matchInfo)))
                {
                    return ctx.Error(err);
                }

                //Make sure the match is joinable.
                if (matchInfo.Ended())
                {
                    return ctx.Error(RlineErrorCode.MatchAlreadyEnded);
                }
                else if (matchInfo.Started() && matchInfo.IsArbitrated) //FIXME: Add AllowJip flag to match, since nonarb can disallow as well
                {
                    return ctx.Error(RlineErrorCode.MatchAlreadyEnded);
                }

                //Make sure the gamer isn't already in the match.
                List<MatchMemberInfo> infos;              
                if (null != (err = QueryMatchMember(con, matchId, accountId, out infos)))
                {
                    return ctx.Error(err);
                }
                else if (infos.Count != 0)
                {
                    return ctx.Error(RlineErrorCode.AlreadyInMatch);
                }
    
                //Create the gamer record
                string s = String.Format("INSERT INTO {0} (MatchId, AccountId, DisplayName) VALUES ({1}, {2}, '{3}')",
                                         TABLE_NAME, matchId, accountId, displayName);

                if (con.AdHocExecuteNonQuery(s) == 0)
                {
                    return ctx.Error(ErrorCode.DbCommandFailed);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Removes a member from a match.
        public static Error RemoveMatchMember(SqlConnectionHelper con,
                                              int matchId,
                                              Int64 accountId)
        {
            Context ctx = RlineContext.Create("match={0}, acct={1}", matchId, accountId);

            try
            {
                string s = String.Format("DELETE FROM {0} WHERE MatchId={1}", TABLE_NAME, matchId);

                if (accountId != 0)
                {
                    s += String.Format(" AND AccountId={0}", accountId);
                }

                if (con.AdHocExecuteNonQuery(s) == 0)
                {
                    return ctx.Error(ErrorCode.DbCommandFailed);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PUBLIC
        //  Helper that returns the index of an account in list of members.
        public static int GetMemberIndex(Int64 accountId,
                                         List<MatchMemberInfo> infos)
        {
            for (int i = 0; i < infos.Count; i++ )
            {
                if (infos[i].AccountId == accountId)
                {
                    return i;
                }
            }

            return -1;
        }
    }
}
