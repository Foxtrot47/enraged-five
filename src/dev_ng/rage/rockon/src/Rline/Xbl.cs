using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Rline
{
    //PURPOSE
    //  Type identifiers as defined by Xenon Live (see xonline.h).
    public enum XblDataType
    {
        Invalid = -1,
        Context = 0,
        Int32 = 1,
        Int64 = 2,
        Double = 3,
        Unicode = 4,
        Float = 5,
        Binary = 6,
        DateTime = 7,
    }

    //PURPOSE
    //  Class that provides miscellaneous XBL functions.
    public class Xbl
    {
        public const uint RANK_FIELD_ID = 0x10008001;
        public const uint GAMER_NAME_FIELD_ID = 0x40008002;

        //const uint X_CONTEXT_GAME_TYPE_ID    = 0x0000800A;
        public const uint X_CONTEXT_GAME_MODE_ID = 0x0000800B;

        public static SqlDataType GetSqlDataType(XblDataType xdt)
        {
            switch (xdt)
            {
                case XblDataType.DateTime: return new SqlDataType(SqlDataType.DataType.DateTime);
                case XblDataType.Float: return new SqlDataType(SqlDataType.DataType.Float);
                case XblDataType.Int32: return new SqlDataType(SqlDataType.DataType.Int);
                case XblDataType.Int64: return new SqlDataType(SqlDataType.DataType.BigInt);
                default: return new SqlDataType(SqlDataType.DataType.Invalid);
            }
        }
    }

    //PURPOSE
    //  Encapsulates an XBL property, which consists of an ID and value.
    //  The ID also encodes the value's type.
    public class XblProperty
    {
        public uint Id;
        public object Value;

        const uint X_PROPERTY_TYPE_MASK = 0xF0000000;
        const uint X_PROPERTY_SCOPE_MASK = 0x00008000;
        const uint X_PROPERTY_ID_MASK = 0x00007FFF;

        //PURPOSE
        //  Extracts the type id from the item id.
        public static XblDataType GetTypeFromId(uint id)
        {
            if (id == 0)
            {
                return XblDataType.Invalid;
            }
            else
            {
                return (XblDataType)((uint)(id & X_PROPERTY_TYPE_MASK) >> 28);
            }
        }

        //PURPOSE
        //  Generates an XBL property id.
        //PARAMS
        //  type        - Property data type.
        //  id          - Property id.
        public static uint MakePropertyId(XblDataType type, uint id)
        {
            return (((uint)type << 28) & X_PROPERTY_TYPE_MASK) | (id & X_PROPERTY_ID_MASK);
        }

        public XblProperty()
        {
        }

        public XblProperty(uint id, object value)
        {
            Id = id;
            Value = value;
        }

        public XblDataType GetXblDataType()
        {
            return XblProperty.GetTypeFromId(Id);
        }
    }
}
