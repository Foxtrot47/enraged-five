using System;

namespace Rockstar.Rockon.Rline
{
    //PURPOSE
    //  Describes an entry in the match members DB.
    public class MatchMemberInfo
    {
        public Int64 AccountId;
        public int MatchId;
        public string DisplayName;
        public DateTime JoinDateTime;
    }
}
