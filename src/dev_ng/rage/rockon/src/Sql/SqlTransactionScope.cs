using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Transactions;
using MySql.Data.MySqlClient;

namespace Rockstar.Rockon.Sql
{
    //PURPOSE
    //  The transaction isolation level determines whether other operations
    //  can read, modify, or add rows to volatile data (data the transaction
    //  is working on).
    //
    //  In discussing an isolation level, we refer to problems it addresses:
    //  - Non-repeatable read:  If two transactions are both capable of 
    //    modifying data, a tx that reads a row more than once may receive
    //    two different values.
    //  - Dirty read: Similar to non-repeatable reads, but these are when the
    //    inconsistent read (the second read) occurs during the processing of
    //    another tx that is subsequently rolled back.  So the other tx reads
    //    a value that was never actually committed.
    //  - Phantom read:  A row is deleted during one tx, but is read just before
    //    it is deleted by another tx.  This means that if the second tx attempts
    //    to modify the row, there will be an error because it no longer exists.
    //
    public enum SqlTransactionIsolationLevel
    {
        //The most restrictive level.  Allows volatile data to be read by 
        //other transactions, but not modified.  Also, no new data can be
        //added by other transactions.  Prevents dirty, non-repeatable, and
        //phatom reads, but may block other transactions until completed.
        Serializable,
      
        //Almost the same as Serializable, but other txs are allowed to add
        //new data, so phantom reads are possible.
        RepeatableRead,
        
        //Other txs are not permitted to read volatile data, but can modify it.
        //Prevents dirty reads, but can still get non-repeatable and phantom
        //reads.
        ReadCommitted,
        
        //Other txs can read and modify volatile data.  Least restrictive 
        //option (and best performance), but allows non-repeatable, dirty,
        //and phantom reads.
        ReadUncommitted
    }

    //PURPOSE
    //  Convenience class for using TransactionScopes in a consistent way.
    public class SqlTransactionScope : IDisposable
    {
        public const SqlTransactionIsolationLevel DEFAULT_ISO_LEVEL = SqlTransactionIsolationLevel.Serializable;

        private TransactionScope m_TxScope;
        private bool m_Disposed = false;    

        //PURPOSE
        //  Returns true if a transaction scope exists.  Code that must operate
        //  within a tx but cannot enlist in the current tx (for whatever
        //  reason) can assert on this to force the caller to create a tx.
        //  This might be necessary for methods that are passed an existing 
        //  SQL connection.
        public static bool IsTransactionActive()
        {
            return Transaction.Current != null;
        }

        //PURPOSE
        //  Ctor that uses the default iso level.
        public SqlTransactionScope()
        {
            Init(DEFAULT_ISO_LEVEL);
        }

        //PURPOSE
        //  Ctor that allows specifying the iso level. Use only when
        //  you understand the performance and concurrency ramifications.
        public SqlTransactionScope(SqlTransactionIsolationLevel isolationLevel)
        {
            Init(isolationLevel);
        }

        //PURPOSE
        //  Disposes of the object.
        public void Dispose()
        {
            Dispose(true);

            // Use SupressFinalize in case a subclass of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }

        //PURPOSE
        //  Helper for non-virtual Dispose().
        protected virtual void Dispose(bool disposing)
        {
            // If you need thread safety, use a lock around these 
            // operations, as well as in your methods that use the resource.
            if (!m_Disposed)
            {
                if (disposing)
                {
                    if (m_TxScope != null)
                    {
                        m_TxScope.Dispose();
                    }
                }

                // Indicate that the instance has been disposed.
                m_TxScope = null;
                m_Disposed = true;
            }
        }

        //PURPOSE
        //  Completes the transaction.  If this is not called, the tx will be
        //  rolled back automatically when it is disposed.
        public void Complete()
        {
            m_TxScope.Complete();
        }

        //PURPOSE
        //  Creates our internal TxScope object at the specified iso level.
        private void Init(SqlTransactionIsolationLevel isolationLevel)
        {
            TransactionOptions txOptions = new TransactionOptions();
            
            switch(isolationLevel)
            {
                case SqlTransactionIsolationLevel.Serializable:
                    txOptions.IsolationLevel = System.Transactions.IsolationLevel.Serializable;
                    break;

                case SqlTransactionIsolationLevel.RepeatableRead:
                    txOptions.IsolationLevel = System.Transactions.IsolationLevel.RepeatableRead;
                    break;

                case SqlTransactionIsolationLevel.ReadCommitted:
                    txOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                    break;

                case SqlTransactionIsolationLevel.ReadUncommitted:
                    txOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;
                    break;

                default:
                    throw(new Exception("Unhandled isolation level"));
            }

            m_TxScope = new TransactionScope(TransactionScopeOption.Required, txOptions);
        }
    }
}
