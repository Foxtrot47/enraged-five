using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace Rockstar.Rockon.Sql
{    
    public enum DatabaseVendor
    {
        MYSQL,
        SQLSERVER
    }

    //PURPOSE
    //  Convenience class for running SQL commands.
    public class SqlHelper
    {
        //public const DatabaseVendor DATABASE_VENDOR = DatabaseVendor.MYSQL;
        public const DatabaseVendor DATABASE_VENDOR = DatabaseVendor.SQLSERVER;
        
        public static bool UseMySql
        {
            get{return DatabaseVendor.MYSQL == DATABASE_VENDOR;}
        }

        public static bool UseSqlServer
        {
            get{return DatabaseVendor.SQLSERVER == DATABASE_VENDOR;}
        }

        //PURPOSE
        //  Generates a database connection string.
        public static string MakeConnectionString(string dbServer,
                                                  string username,
                                                  string password)
        {
            string connStr = "server=" + dbServer + ";uid=" + username + ";";
            
            if (null != password)
            {
                connStr += "password=" + password + ";";
            }

            return connStr;
        }

        //PURPOSE
        //  Returns a DataSet instance containing the results of the
        //  sql command.
        //PARAMS
        //  connectionString    - Database connection string.
        //  commandText         - SQL command string
        public static DataSet AdHocExecuteDataset(string connectionString, string commandText)
        {
            DataSet ds = null;

            if(SqlHelper.UseMySql)
            {
                ds = MySqlHelper.ExecuteDataset(connectionString, commandText);
            }
            else
            {
                SqlConnection conn = new SqlConnection(connectionString);

                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = commandText;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    ds = new DataSet();
                    da.Fill(ds);
                }
                catch (System.Exception e)
                {
                    throw(e);
                }
                finally
                {
                    conn.Close();
                }
            }

            return ds;
        }

        //PURPOSE
        //  Executes a non-query SQL command.  Returns the result of the
        //  command.
        //PARAMS
        //  connectionString    - Database connection string.
        //  commandText         - SQL command string
        public static int AdHocExecuteNonQuery(string connectionString, string commandText)
        {
            int result = 0;

            if(SqlHelper.UseMySql)
            {
                result = MySqlHelper.ExecuteNonQuery(connectionString, commandText);
            }
            else
            {
                SqlConnection conn = new SqlConnection(connectionString);

                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = commandText;
                    result = cmd.ExecuteNonQuery();
                }
                catch (System.Exception e)
                {
                    throw(e);
                }
                finally
                {
                    conn.Close();
                }
            }

            return result;
        }

        //PURPOSE
        //  Executes a SQL command that results in a scalar value.
        //  The type of the result depends on the command.  It could be int,
        //  long, float, double, etc.
        //PARAMS
        //  connectionString    - Database connection string.
        //  commandText         - SQL command string
        public static object AdHocExecuteScalar(string connectionString, string commandText)
        {
            object result = null;

            if(SqlHelper.UseMySql)
            {
                result = MySqlHelper.ExecuteScalar(connectionString, commandText);
            }
            else
            {
                SqlConnection conn = new SqlConnection(connectionString);

                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = commandText;
                    result = cmd.ExecuteScalar();
                }
                catch (System.Exception e)
                {
                    throw(e);
                }
                finally
                {
                    conn.Close();
                }
            }

            return result;
        }

        //PURPOSE
        //  Returns true if the given database exists.
        //PARAMS
        //  connectionString    - Database server connection string.
        //  dbName              - Name of the database.
        public static bool CheckDatabaseExists(string connectionString, string dbName)
        {
            string cmd = @"SELECT COUNT(name)
                        FROM sys.databases
                        WHERE name = '{0}';";
            cmd = String.Format(cmd, dbName);

            object result = SqlHelper.AdHocExecuteScalar(connectionString, cmd);

            return (1 == (int) result);
        }

        public static bool CheckDatabaseExists(string connectionString)
        {
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(connectionString);

            if (sb.Database == null)
            {
                throw new ArgumentException("Connection string does not contain database name");
            }

            return CheckDatabaseExists(connectionString, sb.Database);
        }

        //PURPOSE
        //  Returns true if the given table exists.
        //PARAMS
        //  connectionString    - Database connection string.
        //  tblName             - Name of the table.
        //NOTES
        //  The connection string must contain a database specifier
        //  (database=) or a prior USE statement must have been executed
        //  to specify the database.
        public static bool CheckTableExists(string connectionString,string tblName)
        {
            string baseTblName = tblName;
            int dboIdx = baseTblName.IndexOf("dbo.");
            if(0 == dboIdx)
            {
                baseTblName = baseTblName.Substring(4);
            }

            string s = @"SELECT COUNT(table_name)
                        FROM information_schema.tables
                        WHERE table_name = '{0}';";
            s = String.Format(s, baseTblName);

            object result = SqlHelper.AdHocExecuteScalar(connectionString, s);

            return (1 == (int) result);
        }
    }

}
