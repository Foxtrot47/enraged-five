using System;
using System.Data;
using System.Data.Common;

namespace Rockstar.Rockon.Sql
{
    public class SqlDataType
    {
        public enum DataType
        {
            Invalid = -1,
            BigInt,
            Bit,
            DateTime,
            Float,
            Int,
            VarChar
        };

        public DataType m_Type = DataType.Invalid;
        public int m_Size; //Used with variable-sized types

        public SqlDataType()
        {
        }

        public SqlDataType(DataType t)
        {
            if (IsVarSize(t))
            {
                throw (new Exception(string.Format("{0} is variable sized type, but no size specified", t)));
            }

            m_Type = t;
        }

        public SqlDataType(DataType t, int size)
        {
            if (!IsVarSize(t))
            {
                throw (new Exception(string.Format("{0} is not a variable sized type", t)));
            }

            m_Type = t;
            m_Size = size;
        }

        public SqlDataType(string sqlTypeStr)
        {
            if (sqlTypeStr == "bigint")
            {
                m_Type = DataType.BigInt;
            }
            else if (sqlTypeStr == "bit")
            {
                m_Type = DataType.Bit;
            }
            else if (sqlTypeStr == "datetime")
            {
                m_Type = DataType.DateTime;
            }
            else if (sqlTypeStr == "float")
            {
                m_Type = DataType.Float;
            }
            else if (sqlTypeStr == "int")
            {
                m_Type = DataType.Int;
            }
            else if (sqlTypeStr.StartsWith("varchar("))
            {
                m_Size = int.Parse(sqlTypeStr.Replace("varchar(", "").Replace(")", ""));
                m_Type = DataType.VarChar;
            }
        }

        public static bool IsVarSize(DataType t)
        {
            switch (t)
            {
                case DataType.VarChar:
                    return true;
            }
            return false;
        }

        public override string ToString()
        {
            switch (m_Type)
            {
                case DataType.BigInt: return "bigint";
                case DataType.Bit: return "bit";
                case DataType.DateTime: return "datetime";
                case DataType.Float: return "float";
                case DataType.Int: return "int";
                case DataType.VarChar: return string.Format("varchar({0})", m_Size);
                default:
                    throw (new Exception("Invalid data type:" + m_Type.ToString()));
            }
        }
    };
}
