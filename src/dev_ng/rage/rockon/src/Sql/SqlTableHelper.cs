using System;
using System.Collections.Generic;
using System.Data.Common;

namespace Rockstar.Rockon.Sql
{
    public class SqlColumn
    {
        public string m_Name;
        public SqlDataType m_Type;
        public bool m_CanBeNull;
        public bool m_IsPrimaryKey;
        public string m_DefaultValue;

        public SqlColumn(string name,
                         SqlDataType type,
                         bool canBeNull,
                         bool isPrimaryKey,
                         string defaultValue)
        {
            m_Name = name;
            m_Type = type;
            m_CanBeNull = canBeNull;
            m_IsPrimaryKey = isPrimaryKey;
            m_DefaultValue = defaultValue;
        }

        public override string ToString()
        {
            string s = String.Format("{0} {1}{2}{3}",
                                    this.m_Name,
                                    this.m_Type.ToString(),
                                    !this.m_CanBeNull ? " NOT NULL" : "",
                                    (null != this.m_DefaultValue) ? " DEFAULT " + this.m_DefaultValue : "");

            return s;
        }
    }

    public class SqlTableHelper
    {
        public enum CreateMode
        {
            NoReplace,      //Fail if table already exists
            Replace,        //Delete existing table before creating new one
            Union,          //Final table will have all columns in existing table as well as new
            CopyNewOnly,    //Existing table columns will only be copied if they match new columns
        };

        //PURPOSE
        //  Creates a table of given name in a DB.
        public static bool Create(SqlConnectionHelper con,
                                  string tableName,
                                  IEnumerable<SqlColumn> columns,
                                  CreateMode mode)
        {
            string tempTableName = "tmp_" + tableName;

            if(SqlHelper.UseSqlServer)
            {
                tableName = "dbo." + tableName;
                tempTableName = "dbo." + tempTableName;

                //These are required in order to create indexed views of this table.
                con.AdHocExecuteNonQuery("SET ANSI_NULLS ON");
                con.AdHocExecuteNonQuery("SET QUOTED_IDENTIFIER ON");
            }

            //Delete the existing table.
            if (con.CheckTableExists(tableName))
            {
                con.AdHocExecuteNonQuery("DROP TABLE " + tableName);
            }

            //Create the new table.
            con.AdHocExecuteNonQuery(ComposeCreateCmd(tableName, columns));


            //FIXME: Eventually reimplement, but for now all we need is ability to recreate
            //       a table.
/*
            //Retrieve columns of the existing table.
            Dictionary<string, SqlColumn> oldColumns = new Dictionary<string,SqlColumn>();
            RetrieveOldColumns(con, tableName, ref oldColumns);

            if (oldColumns.Count == 0)
            {
                string cmd = ComposeCreateCmd(tableName, columns);
                con.AdHocExecuteNonQuery(cmd);
            }
            else
            {
                //Create a tmp table with the new schema.
                string cmd = ComposeCreateCmd(tempTableName, columns);
                con.AdHocExecuteNonQuery(cmd);

                //Copy columns from the old table to the new table.
                //Columns are copied only if they have the same name and type.
                cmd = GetCopyCommand(tableName, 
                                     oldColumns,
                                     tempTableName,
                                     columns);
                con.AdHocExecuteNonQuery(cmd);

                //Drop the old table.
                cmd = "DROP TABLE " + tableName;
                con.AdHocExecuteNonQuery(cmd);

                //Create the final table with the new schema.
                cmd = ComposeCreateCmd(tableName, columns);
                con.AdHocExecuteNonQuery(cmd);

                //Copy columns from the tmp table to the final table.
                cmd = GetCopyCommand(tempTableName, 
                                     columns,
                                     tableName,
                                     columns);
                con.AdHocExecuteNonQuery(cmd);

                //Drop the tmp table.
                cmd = "DROP TABLE " + tempTableName;
                con.AdHocExecuteNonQuery(cmd);
            }
 */

            return true;
        }

        private static string ComposeCreateCmd(string tableName,
                                               IEnumerable<SqlColumn> columns)
        {
            string cmd = "CREATE TABLE " + tableName + Environment.NewLine;

            string tableDef = "";
            string priKey = "";
            int count = 0;

            foreach(SqlColumn tc in columns)
            {
                if(count > 0)
                {
                    tableDef += "," + Environment.NewLine + tc.ToString();
                }
                else
                {
                    tableDef += tc.ToString();
                }

                if(tc.m_IsPrimaryKey)
                {
                    if(priKey.Length > 0)
                    {
                        priKey += ",";
                    }

                    priKey += tc.m_Name;
                }

                ++count;
            }

            if(priKey.Length > 0)
            {
                string baseTblName = tableName;
                int dboIdx = baseTblName.IndexOf("dbo.");
                if(0 == dboIdx)
                {
                    baseTblName = baseTblName.Substring(4);
                }

                priKey =
                    String.Format("CONSTRAINT PK_{0} PRIMARY KEY ({1})",
                                  baseTblName,
                                  priKey);

                tableDef += "," + Environment.NewLine + priKey;
            }

            cmd += "(" + tableDef + ")";

            return cmd;
        }

        //PURPOSE
        //  Retrieves columns of the existing table.
        private static void RetrieveOldColumns(SqlConnectionHelper con,
                                               string tableName, 
                                               ref Dictionary<string, SqlColumn> columns)
        {
            columns = new Dictionary<string,SqlColumn>();

            string baseTblName = tableName;
            int dboIdx = baseTblName.IndexOf("dbo.");
            if(0 == dboIdx)
            {
                baseTblName = baseTblName.Substring(4);
            }

            string cmd =
                @"SELECT COLUMN_NAME,
                DATA_TYPE
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_NAME='" + baseTblName + "'";
            using(DbDataReader reader = con.AdHocExecuteReader(cmd))
            {
                while(reader.Read())
                {
                    string name = reader.GetValue(0) as string;
                    string dataTypeStr = reader.GetValue(1) as string;

                    SqlColumn tc = new SqlColumn(name,
                                                 new SqlDataType(dataTypeStr),
                                                 true,
                                                 false,
                                                 "NULL");

                    columns.Add(tc.m_Name, tc);
                }

                reader.Close();
            }
        }

        //PURPOSE
        //  Generates a SQL command to copy rows from the source
        //  table to the destination table.
        //NOTES
        //  Column values are copied only if the type of the column
        //  is the same in the source table and the dest table.
        private static string GetCopyCommand(string srcTbl, 
                                             string dstTbl,
                                             IEnumerable<SqlColumn> newColumns,
                                             Dictionary<string, SqlColumn> oldColumns)
        {
            string cmd = "INSERT INTO " + dstTbl + Environment.NewLine;

            string columnsStr = "";
            int count = 0;

            foreach(SqlColumn tc in newColumns)
            {
                if(!oldColumns.ContainsKey(tc.m_Name))
                {
                    continue;
                }

                SqlColumn oldTc = oldColumns[tc.m_Name];

                if(oldTc.m_Type != tc.m_Type)
                {
                    continue;
                }

                if(count > 0)
                {
                    columnsStr += ",";
                }

                columnsStr += tc.m_Name;

                ++count;
            }

            cmd += "(" + columnsStr + ")" + Environment.NewLine;
            cmd += "SELECT " + columnsStr + " FROM " + srcTbl + ";";

            return cmd;
        }
    }
}
