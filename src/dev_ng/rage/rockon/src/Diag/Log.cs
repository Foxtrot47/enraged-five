﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace Rockstar.Rockon.Diag
{
    //PURPOSE
    //  Writes (optionally) timestamped text to Trace.
    //  You can direct how the trace is output by adding a section to
    //  your App.config file.  For example, to output to the console,
    //  add this section:
    //
    //  <system.diagnostics>
    //      <trace autoflush="true">
    //          <listeners>
    //              <add name="Console" type="System.Diagnostics.ConsoleTraceListener" />
    //          </listeners>
    //      </trace>
    //  </system.diagnostics>
    //
    //  You can also add trace listeners programmatically, of course.
    //  Keep in mind, though, that if you have a console listener in the app.config
    //  and one added programmatically, you'll get double output to the console.
    public class Log
    {
        private static bool m_WriteTimestamp = false;
        private static bool m_WriteThreadId = true;

        public static void WriteLine(string format, params object[] args)
        {
            WriteLine(string.Format(format, args));
        }

        public static void WriteLine(string s)
        {
            Trace.WriteLine(ComposeText(s));
        }

        private static string ComposeText(string s)
        {
            StringBuilder sb = new StringBuilder();

            if (m_WriteTimestamp)
            {
                DateTime dt = DateTime.UtcNow;

                sb.Append(string.Format("{0}/{1}/{2} {3}:{4}:{5}.{6} ",
                                      dt.Month.ToString("D2"),
                                      dt.Day.ToString("D2"),
                                      (dt.Year % 1000).ToString("D2"),
                                      dt.Hour.ToString("D2"),
                                      dt.Minute.ToString("D2"),
                                      dt.Second.ToString("D2"),
                                      dt.Millisecond.ToString("D3")));
            }
            
            if(m_WriteThreadId)
            {
                sb.Append(string.Format("[{0}] ", Thread.CurrentThread.ManagedThreadId));
            }

            sb.Append(s);

            return sb.ToString();
        }
    }
}
