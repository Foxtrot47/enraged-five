﻿using System;
using System.Diagnostics;
using System.Text;

namespace Rockstar.Rockon.Diag
{
    //PURPOSE
    //  Convenience class from writing to the Windows event log.
    public class EventLogHelper
    {
        public string SourceName { get; private set; }
        public string LogName { get; private set; }

        //PURPOSE
        //  Ctor that takes a source name (ex. "MyService") and writes to the
        //  Application log.  This is the most common scenario.
        public EventLogHelper(string sourceName)
        {
            Init(sourceName, "Application");
        }

        //PURPOSE
        //  Ctor that allows specifying a log other than Application.
        public EventLogHelper(string sourceName, string logName)
        {
            Init(sourceName, logName);
        }

        //PURPOSE
        //  Writes an information entry to the log.
        public void Write(string format, params object[] args)
        {
            EventLog.WriteEntry(SourceName, 
                                string.Format(format, args),
                                EventLogEntryType.Information);
        }

        //PURPOSE
        //  Writes a warning entry to the log.
        public void WriteWarning(string format, params object[] args)
        {
            EventLog.WriteEntry(SourceName, 
                                string.Format(format, args),
                                EventLogEntryType.Warning);
        }

        //PURPOSE
        //  Writes an error entry to the log.
        public void WriteError(string format, params object[] args)
        {
            EventLog.WriteEntry(SourceName, 
                                string.Format(format, args),
                                EventLogEntryType.Error);
        }

        private void Init(string sourceName, string logName)
        {
            if (!EventLog.Exists(logName) && !EventLog.SourceExists(sourceName))
            {
                EventLog.CreateEventSource(sourceName, logName);
            }

            SourceName = sourceName;
            LogName = logName;
        }
    }
}
