﻿using System;
using System.Diagnostics;
using System.Text;

namespace Rockstar.Rockon.Diag
{
    //PURPOSE
    //  A channel is a filtering mechanism for log output, based on the severity
    //  of the text being logged.  If the text's severity is below the current
    //  threshold of the channel it will not be output.
    //
    //  Channels can be created on the fly, but subsystems may find it useful
    //  to declare a static instance somewhere so that output for the subsystem
    //  can be filtered in one spot.
    //
    //  Channels can be passed the ctor of a Context object.  All output from the
    //  context will then be filtered by the channel.

    public class Channel
    {
        //Severity used by Contexts created without a Channel.
        public static Severity DefaultSeverity = Severity.Debug2;
        
        //Max severity applied globally. Disables all output below this level.
        public static Severity GlobalMaxSeverity = Severity.Debug3;

        //Set whether the channel's abbreviation
        public static bool WriteSeverityAbbrev = true;

        public string Name { get; private set; }
        public Severity MaxSeverity { get; private set; }

        public enum Severity
        {
            None = 0,
            Fatal,
            Error,
            Warning,
            Debug1,
            Debug2,
            Debug3
        }

        private static string[] SeverityAbbrev =
        {
            "NONE",
            "FATAL",
            "err",
            "warn",
            "d1",
            "d2",
            "d3"
        };

        public Channel()
        {
            Name = "UNNAMED";
            MaxSeverity = DefaultSeverity;
        }

        public Channel(string name)
        {
            if (name == null || name.Length == 0)
            {
                throw (new ArgumentNullException());
            }

            Name = name;
            MaxSeverity = Severity.Debug2;
        }

        public Channel(string name, Severity maxSeverity)
        {
            if (name == null || name.Length == 0)
            {
                throw (new ArgumentNullException());
            }

            Name = name;
            MaxSeverity = maxSeverity;
        }

        public void SetSeverity(Severity s)
        {
            MaxSeverity = s;
        }

        public void Fatal(string format, params object[] args)
        {
            Fatal(string.Format(format, args));
        }

        public void Fatal(string s)
        {
            WriteLine(Severity.Fatal, s);
        }
        
        public void Error(string format, params object[] args)
        {
            Error(string.Format(format, args));
        }

        public void Error(string s)
        {
            WriteLine(Severity.Error, s);
        }
        
        public void Warning(string format, params object[] args)
        {
            Warning(string.Format(format, args));
        }

        public void Warning(string s)
        {
            WriteLine(Severity.Warning, s);
        }

        public void Debug1(string format, params object[] args)
        {
            Debug1(string.Format(format, args));
        }

        public void Debug1(string s)
        {
            WriteLine(Severity.Debug1, s);
        }

        public void Debug2(string format, params object[] args)
        {
            Debug2(string.Format(format, args));
        }

        public void Debug2(string s)
        {
            WriteLine(Severity.Debug2, s);
        }

        public void Debug3(string format, params object[] args)
        {
            Debug3(string.Format(format, args));
        }

        public void Debug3(string s)
        {
            WriteLine(Severity.Debug3, s);
        }

        public bool CanWrite(Severity severity)
        {
            return severity <= GlobalMaxSeverity && severity <= MaxSeverity;
        }

        public void WriteLine(Severity severity, string format, params object[] args)
        {
            WriteLine(severity, string.Format(format, args));
        }

        public void WriteLine(Severity severity, string s)
        {
            if (CanWrite(severity))
            {
                string prefix;

                if (WriteSeverityAbbrev)
                {
                    prefix = string.Format("[{0}:{1}] ", Name, SeverityAbbrev[(int)severity]);
                }
                else
                {
                    prefix = string.Format("[{0}] ", Name);
                }

                Diag.Log.WriteLine(prefix + s);
                
                //On fatals, write the stack trace.
                if(Severity.Fatal == severity)
                {
                    Diag.Log.WriteLine(Environment.StackTrace);
                }
            }
        }
    }
}
