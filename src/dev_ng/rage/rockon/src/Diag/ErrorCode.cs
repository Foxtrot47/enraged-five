using System;
using System.Collections;
using System.Collections.Generic;

namespace Rockstar.Rockon.Diag
{
    //PURPOSE
    //  Commonly used error codes that can be used to create Error instances.
    public enum ErrorCode
    {
        AlreadyExists,      //Object already exists
        DbConnectFailed,    //Failed to connect to the DB server
        DbCommandFailed,    //A DB command failed
        DbInvalidData,      //Data stored in DB is invalid
        DoesNotExist,       //Desired object does not exist
        Duplicate,          
        Exception,          //An exception occurred
        InvalidArguments,   //One or more arguments are invalid
        InvalidData,        //The data being processed is invalid
        InvalidState,
        NotAllowed,         //The action is prohibited for some reason
        UnknownError,
    }
}
