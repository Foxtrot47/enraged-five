﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Reflection;

namespace Rockstar.Rockon.Diag
{
    //PURPOSE
    //  Convenience struct to allow auto-prefixing of diagnostic text for a particular
    //  context.  Can save a lot of typing.
    //FIXME: Reverted to class, because ran into odd case where m_Prefix would be initialized
    //       on the object *before the object was even created*.  That doesn't happen
    //       when Context is a class. 
    public class Context
    {
        private Channel m_Channel;
        private string m_Desc;
        private string m_Prefix;

        //PURPOSE
        //  Contexts can be created with an optional Channel to output to, 
        //  and an optional description that prefixes all their output.
        public Context()
        {
            m_Channel = null;
            m_Desc = null;
            m_Prefix = null;
        }

        public Context(string desc)
        {
            m_Channel = null;
            m_Desc = desc;
            m_Prefix = null;
        }

        public Context(string format, params object[] args)
        {
            if (format == null || format.Length == 0)
            {
                throw (new ArgumentNullException("format"));
            }

            m_Channel = null;
            m_Desc = string.Format(format, args);
            m_Prefix = null;
        }

        public Context(Channel channel)
        {
            if (channel == null)
            {
                throw (new ArgumentNullException("channel"));
            }

            m_Channel = channel;
            m_Desc = null;
            m_Prefix = null;
        }

        public Context(Channel channel, string desc)
        {
            if (channel == null)
            {
                throw (new ArgumentNullException("channel"));
            }

            m_Channel = channel;
            m_Desc = desc;
            m_Prefix = null;
        }

        public Context(Channel channel, string format, params object[] args)
        {
            if (channel == null || format == null || format.Length == 0)
            {
                throw (new ArgumentNullException("channel or format"));
            }

            m_Channel = channel;
            m_Desc = string.Format(format, args);
            m_Prefix = null;
        }
/*        
        //PURPOSE
        //  Returns a prefixed string that contains this Context's description
        //  and that of all Contexts before it in the chain.
        public override string ToString()
        {
            return ToString(null);
        }

        //PURPOSE
        //  Returns prefixed strings with additional message.
        //  This is useful for providing context information to an exception,
        //  for instance.
        public string ToString(string format, params object[] args)
        {
            if (format == null)
            {
                return GetPrefix(1);
            }
            else
            {
                return GetPrefix(1) + ": " + string.Format(format, args);
            }
        }
*/
        //PURPOSE
        //  The SuccessX() methods write a "SUCCESS" line, and return null.
        //  While this sounds silly, it leads to cleaner code in methods that
        //  return an Error.
        //  The X in SuccessX() is the debug severity it displays at.
        //
        //NOTE
        //  We have multiple versions of calls partially to avoid overhead of 
        //  unnecessary string.Format, but also because embedded curly braces
        //  (like in RSON messages) cause havoc with string.Format.
        //
        public Error Success1()
        {
            WriteLine(Channel.Severity.Debug1, "SUCCESS", 1);
            return null;
        }

        public Error Success1(string s)
        {
            WriteLine(Channel.Severity.Debug1, "SUCCESS: " + s, 1);
            return null;
        }

        public Error Success1(string format, params object[] args)
        {
            WriteLine(Channel.Severity.Debug1, "SUCCESS: " + string.Format(format, args), 1);
            return null;
        }

        public Error Success2()
        {
            WriteLine(Channel.Severity.Debug2, "SUCCESS", 1);
            return null;
        }

        public Error Success2(string s)
        {
            WriteLine(Channel.Severity.Debug2, "SUCCESS: " + s, 1);
            return null;
        }

        public Error Success2(string format, params object[] args)
        {
            WriteLine(Channel.Severity.Debug2, "SUCCESS: " + string.Format(format, args), 1);
            return null;
        }

        public Error Success3()
        {
            WriteLine(Channel.Severity.Debug3, "SUCCESS", 1);
            return null;
        }

        public Error Success3(string s)
        {
            WriteLine(Channel.Severity.Debug3, "SUCCESS: " + s, 1);
            return null; //null error indicates success
        }

        public Error Success3(string format, params object[] args)
        {
            WriteLine(Channel.Severity.Debug3, "SUCCESS: " + string.Format(format, args), 1);
            return null; //null error indicates success
        }

        //PURPOSE
        //  Output methods that mirror those in Channel.  If the context
        //  was created with a Channel, the output goes through it.
        //  Otherwise, it goes straight to Log.
        public Error Error(object code)
        {
            return this.WriteError(new Error(code), 1);
        }

        public Error Error(object code, string format, params object[] args)
        {
            return this.WriteError(new Error(code, string.Format(format, args)), 1);
        }

        public Error Error(Exception ex)
        {
            return this.WriteError(new Error(ex), 1);
        }

        public Error Error(Error err)
        {
            return this.WriteError(err, 1);
        }

        private Error WriteError(Error err, int callDepth)
        {
            ++callDepth; //To account for this call

            WriteLine(Channel.Severity.Error, err.ToString(), callDepth);

            //We add contextual information so that when returning 
            //this error via web response, the user has some idea 
            //where the error occurred.
            if (!err.m_ContextSet)
            {
                if (err.Msg != null)
                {
                    err.Msg = GetPrefix(callDepth) + ": " + err.Msg;
                }
                else
                {
                    err.Msg = GetPrefix(callDepth);
                }

                err.m_ContextSet = true;
            }

            return err;
        }

        public void Warning(string s)
        {
            WriteLine(Channel.Severity.Warning, s, 1);
        }

        public void Warning(string format, params object[] args)
        {
            WriteLine(Channel.Severity.Warning, string.Format(format, args), 1);
        }

        public void Debug1(string s)
        {
            WriteLine(Channel.Severity.Debug1, s, 1);
        }

        public void Debug1(string format, params object[] args)
        {
            WriteLine(Channel.Severity.Debug1, string.Format(format, args), 1);
        }

        public void Debug2(string s)
        {
            WriteLine(Channel.Severity.Debug2, s, 1);
        }

        public void Debug2(string format, params object[] args)
        {
            WriteLine(Channel.Severity.Debug2, string.Format(format, args), 1);
        }

        public void Debug3(string s)
        {
            WriteLine(Channel.Severity.Debug3, s, 1);
        }

        public void Debug3(string format, params object[] args)
        {
            WriteLine(Channel.Severity.Debug3, string.Format(format, args), 1);
        }

        private string GetPrefix(int callDepth)
        {
            ++callDepth; //To account for this call

            if (m_Prefix == null)
            {
                //Get class and method name the context lives in
                StackFrame sf = new StackFrame(callDepth);
                MethodBase mb = sf.GetMethod();
                Type type = mb.DeclaringType;

                //For brevity, remove the Rockstar.Rockon namespace
                string className = type.FullName.Replace("Rockstar.Rockon.", "");
                string methodName = mb.Name;

                //Build the prefix string
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("{0}.{1}", className, methodName);

                if (m_Desc != null)
                {
                    sb.AppendFormat("({0})", m_Desc);
                }

                m_Prefix = sb.ToString();
            }

            return m_Prefix;
        }

        //PURPOSE
        //  Central writer for all output methods.
        private void WriteLine(Channel.Severity severity, string s, int callDepth)
        {
            ++callDepth; //To account for this call

            if (m_Channel != null)
            {
                if (m_Channel.CanWrite(severity))
                {
                    m_Channel.WriteLine(severity, "{0}: {1}", GetPrefix(callDepth), s);
                }
            }
            else if (severity <= Channel.DefaultSeverity)
            {
                Diag.Log.WriteLine("{0}: {1}", GetPrefix(callDepth), s);
            }
        }
    }
}
