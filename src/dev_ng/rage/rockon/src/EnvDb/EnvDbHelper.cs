using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.EnvDb
{
    public class EnvDbHelper
    {
        //PURPOSE
        //  Creates the environment DB and minimum required tables
        //PARAMS
        //  connectionString    - Conn str for server to create DB on. 
        //                        This must include the DB name.
        //RETURNS
        //  Null on success, Error instance on failure.
        public static Error Bootstrap(string connectionString)
        {
            Context ctx = EnvDbContext.Create();

            try
            {
                SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(connectionString);

                if (sb.Database == null)
                {
                    return ctx.Error(ErrorCode.InvalidArguments, "Connection string must specify the database");
                }

                //Create DB
                if (SqlHelper.CheckDatabaseExists(connectionString, sb.Database))
                {
                    ctx.Debug2("DB '{0}' already exists on server '{1}'", sb.Database, sb.Server);
                }
                else
                {
                    SqlHelper.AdHocExecuteNonQuery(connectionString, "CREATE DATABASE " + sb.Database);
                }

                using (SqlConnectionHelper con = new SqlConnectionHelper(connectionString))
                {
                    if (!con.Open())
                    {
                        return ctx.Error("Failed to open connection to '{0}'", connectionString);
                    }

                    Error err = null;

                    //Create required tables
                    ctx.Debug2("Creating accounts table...");
                    if (null != (err = Account.CreateTables(con)))
                    {
                        return ctx.Error(err);
                    }

                    ctx.Debug2("Creating account-NP link table...");
                    if (null != (err = AccountNpLink.CreateTable(con)))
                    {
                        return ctx.Error(err);
                    }

                    ctx.Debug2("Creating account-XBL link table...");
                    if (null != (err = AccountXblLink.CreateTable(con)))
                    {
                        return ctx.Error(err);
                    }

                    ctx.Debug2("Creating titles table...");
                    if (null != (err = Title.CreateTable(con)))
                    {
                        return ctx.Error(err);
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Connects to DB server and USEs the environment DB within.  It assumes
        //  the connection string is specified in configuration data, and that the 
        //  connection string includes the database name to use.
        //RETURNS
        //  Connection object on success, null on failure.
        private static string sm_ConnectionString;

        public static SqlConnectionHelper Open()
        {
            SqlConnectionHelper con = null;

            try
            {
                if (sm_ConnectionString == null)
                {
                    const string SETTING_CONNECTIONSTRING = "RockonEnvDbConnectionString";

                    sm_ConnectionString = ConfigurationManager.AppSettings[SETTING_CONNECTIONSTRING];
                    if ((sm_ConnectionString == null) || (sm_ConnectionString.Length == 0))
                    {
                        throw (new Exception(string.Format("Could not find config setting [{0}]", SETTING_CONNECTIONSTRING)));
                    }
                }

                con = new SqlConnectionHelper(sm_ConnectionString);
                con.Open();
            }
            catch (Exception ex)
            {
                if (con != null)
                {
                    con.Dispose();
                }
                throw (ex);
            }

            return con;
        }
    }
}
