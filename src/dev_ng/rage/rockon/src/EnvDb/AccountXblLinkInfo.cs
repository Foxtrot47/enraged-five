using System;

namespace Rockstar.Rockon.EnvDb
{
    //PURPOSE
    //  Describes a link between a RockOn account and an Xbox Live account.
    public class AccountXblLinkInfo
    {
        public Int64 AccountId = 0;
        public UInt64 Xuid = 0;
        public string Gamertag = null;
        public DateTime CreateDateTime = DateTime.MinValue;   //Create datetime of link, not account.
    }
}
