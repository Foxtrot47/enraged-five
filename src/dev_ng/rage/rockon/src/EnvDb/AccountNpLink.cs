using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.EnvDb
{
    //PURPOSE
    //  Manages links between NP and Rockon accounts.
    public class AccountNpLink
    {
        public const string TABLE_NAME = "AccountNpLinks";

        //PURPOSE
        //  Creates the link table.
        //PARAMS
        //  con     - Open connection using DB that table should be created in.
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        public static Error CreateTable(SqlConnectionHelper con)
        {
            Context ctx = EnvDbContext.Create();

            if (con.CheckTableExists(TABLE_NAME))
            {
                ctx.Warning("Table already exists. It must be manually dropped before it can be recreated (this is to avoid accidental deletion).");
            }
            else
            {
                //Create the table
                string s = string.Format("; CREATE TABLE {0} ", TABLE_NAME);
                s += @"([OnlineIdHash] [int] NOT NULL,
                        [OnlineId] [varchar](17) NOT NULL,
                        [AccountId] [bigint] NOT NULL,                        
                        [CreateDateTime] [datetime] NOT NULL DEFAULT(GETUTCDATE()),
                        CONSTRAINT [PK_AccountNpLinks] PRIMARY KEY CLUSTERED 
                        (
	                        [OnlineIdHash] ASC
                        )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                        ) ON [PRIMARY]";
                con.AdHocExecuteNonQuery(s);

                //Add foreign key
                s = string.Format("ALTER TABLE {0} ", TABLE_NAME);
                s += @"  WITH CHECK ADD  CONSTRAINT [FK_AccountNpLinks_Accounts] FOREIGN KEY([AccountId])
                       REFERENCES [dbo].[Accounts] ([AccountId])";
                con.AdHocExecuteNonQuery(s);

                s = string.Format("ALTER TABLE {0} ", TABLE_NAME);
                s += @"CHECK CONSTRAINT [FK_AccountNpLinks_Accounts]";
                con.AdHocExecuteNonQuery(s);
            }

            return ctx.Success2();
        }

        public static Error QueryCreate(string npOnlineId,
                                        out Int64 accountId)
        {
            Context ctx = EnvDbContext.Create("{0}", npOnlineId);
            
            Error err;
            accountId = 0;

            if (npOnlineId.Length == 0)
            {
                return ctx.Error(ErrorCode.InvalidArguments);
            }

            using (SqlTransactionScope ts = new SqlTransactionScope())
            {
                using (SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    //First, query to see if a link already exists.
                    AccountNpLinkInfo info;
                    if (null != (err = QueryLink(con, npOnlineId, out info)))
                    {
                        return ctx.Error(err);
                    }

                    if (info == null)
                    {
                        //Otherwise, create an account.
                        if (null != (err = Account.CreateAccount(con, out accountId)))
                        {
                            return ctx.Error(err);
                        }

                        //Now create a link to the account.
                        if (null != (err = CreateLink(con, accountId, npOnlineId)))
                        {
                            return ctx.Error(err);
                        }
                    }
                    else
                    {
                        accountId = info.AccountId;
                    }

                    ts.Complete();
                }
            }

            return ctx.Success2("accountId={0}", accountId);
        }

        //PURPOSE
        //  Returns RockOn account ID associated with an NP account.
        //PARAMS
        //  con         - Open connection using DB that table resides in.
        //  npOnlineId  - ID to lookup link info by
        //  out info    - Link info
        //RETURNS
        //  On success, returns null and info param is non-null if a link was found, or null
        //  if none was found.  On failure, returns Error instance.
        public static Error QueryLink(SqlConnectionHelper con,
                                      string npOnlineId,
                                      out AccountNpLinkInfo info)
        {
            Context ctx = EnvDbContext.Create("{0}", npOnlineId);
            
            info = null;

            List<AccountNpLinkInfo> infos = new List<AccountNpLinkInfo>();

            Error err = QueryLinks(con, npOnlineId, 1, out infos);
            if (err != null)
            {
                return ctx.Error(err);
            }

            if (infos.Count == 0)
            {
                info = null;
                return ctx.Success2();
            }

            info = infos[0];
            
            return ctx.Success2("Found account ({0})", info.AccountId);
        }

        public static Error QueryLinks(SqlConnectionHelper con,
                                       string npOnlineId,
                                       uint maxResults,
                                       out List<AccountNpLinkInfo> infos)
        {
            Context ctx = EnvDbContext.Create("{0}", npOnlineId);
            
            infos = new List<AccountNpLinkInfo>();

            SqlCmdParams cmdParams = new SqlCmdParams();

            string s = string.Format("SELECT * FROM {0}", TABLE_NAME);
            
            if (npOnlineId != null && npOnlineId.Length > 0)
            {
                //NOTE: We lookup by both hash and string ID because we can't 
                //      be sure the hash will be unique, but the string will be.
                s += string.Format(" WHERE OnlineIdHash=@OnlineIdHash AND OnlineId=@OnlineId");
                cmdParams.Add("@OnlineIdHash", npOnlineId.GetHashCode());
                cmdParams.Add("@OnlineId", npOnlineId);
            }

            using(DbDataReader reader = con.ExecuteReader(s, cmdParams))
            {
                while (reader.Read())
                {
                    AccountNpLinkInfo info = new AccountNpLinkInfo();

                    info.AccountId = (Int64)reader["AccountId"];
                    info.CreateDateTime = (DateTime)reader["CreateDateTime"];
                    info.OnlineId = reader["OnlineId"].ToString();
                    info.OnlineIdHash = (int)reader["OnlineIdHash"];

                    if (info.OnlineIdHash != info.OnlineId.GetHashCode())
                    {
                        //This breaks a core assumption, so treat as error.
                        return ctx.Error(ErrorCode.InvalidData,
                                         "Hash code mismatch for {0}: {1}, expected {2}",
                                         info.OnlineId,
                                         info.OnlineIdHash,
                                         info.OnlineId.GetHashCode());
                    }

                    infos.Add(info);

                    if ((maxResults != 0) && (infos.Count >= maxResults))
                    {
                        break;
                    }
                }

                reader.Close();
            }

            return ctx.Success2("Found {0} account npAccountId links", infos.Count);
        }

        //PURPOSE
        //  Creates a link between RockOn account ID and NP online ID.
        //PARAMS
        //  con         - Open connection using DB that table resides in.
        //  accountId   - RockOn account ID to link to
        //  npOnlineId  - NP online ID (eg. gamertag)
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        public static Error CreateLink(SqlConnectionHelper con,
                                       Int64 accountId,                                   
                                       string npOnlineId)
        {
            Context ctx = EnvDbContext.Create("acct={0}, npOnlineId={1}",
                                           accountId,
                                           npOnlineId);
            
            if (accountId == 0 || npOnlineId.Length == 0)
            {
                return ctx.Error(ErrorCode.InvalidArguments);
            }

            string s = String.Format("INSERT INTO {0} (OnlineIdHash, AccountId, OnlineId) VALUES (@OnlineIdHash, @AccountId, @OnlineId)",
                                     TABLE_NAME);

            SqlCmdParams cmdParams = new SqlCmdParams();
            cmdParams.Add("@OnlineIdHash", npOnlineId.GetHashCode());
            cmdParams.Add("@AccountId", accountId);
            cmdParams.Add("@OnlineId", npOnlineId);

            if (1 != con.ExecuteNonQuery(s, cmdParams))
            {
                return ctx.Error(ErrorCode.DbCommandFailed, "INSERT failed; perhaps Rockon account does not exist?");
            }

            return ctx.Success2();
        }

        //PURPOSE
        //  Deletes a link to a RockOn account.
        //PARAMS
        //  con         - Open connection using DB that table resides in.
        //  npOnlineId  - ID of link to remove
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        public static Error DeleteLink(SqlConnectionHelper con,
                                       string npOnlineId)
        {
            Context ctx = EnvDbContext.Create("{0}", npOnlineId);

            string s = String.Format("DELETE FROM {0} WHERE OnlineIdHash=@OnlineIdHash AND OnlineId=@OnlineId", TABLE_NAME);

            SqlCmdParams cmdParams = new SqlCmdParams();
            cmdParams.Add("@OnlineIdHash", npOnlineId.GetHashCode());
            cmdParams.Add("@OnlineId", npOnlineId);

            if (con.ExecuteNonQuery(s, cmdParams) != 1)
            {
                return ctx.Error(ErrorCode.DbCommandFailed, "DELETE failed; perhaps already deleted?");
            }

            return ctx.Success2();
        }
    }
}
