using System;

namespace Rockstar.Rockon.EnvDb
{
    //PURPOSE
    //  Describes an entry in the Titles table of the environment DB.
    public class TitleInfo
    {
        public int Id;                      //Well-known ID of title
        public string ConnectionStr;        //Connection string for DB server title resides in
        public DateTime CreateDateTime;     //Time entry was created
    }
}
