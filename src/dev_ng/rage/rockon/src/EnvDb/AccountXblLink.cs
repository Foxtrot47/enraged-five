using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.EnvDb
{
    //PURPOSE
    //  Manages links between Xuids and RockOn account IDs.
    public class AccountXblLink
    {
        public const string TABLE_NAME = "AccountXblLinks";

        //PURPOSE
        //  Creates the Xuid link table.
        //PARAMS
        //  con     - Open connection using DB that table should be created in.
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        public static Error CreateTable(SqlConnectionHelper con)
        {
            Context ctx = EnvDbContext.Create();

            if (con.CheckTableExists(TABLE_NAME))
            {
                ctx.Warning("Table exists. It must be manually dropped before it can be recreated (this is to avoid accidental deletion).");
            }
            else
            {
                string s = string.Format("CREATE TABLE {0} ", TABLE_NAME);
                s += @"([Xuid] [bigint] NOT NULL,
                        [AccountId] [bigint] NOT NULL,
                        [Gamertag] [varchar](20) NOT NULL,
                        [CreateDateTime] [datetime] NOT NULL DEFAULT(GETUTCDATE()),
                        CONSTRAINT [PK_AccountXblLinks] PRIMARY KEY CLUSTERED 
                        (
	                        [Xuid] ASC
                        )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                        ) ON [PRIMARY]";
                con.AdHocExecuteNonQuery(s);

                //Add foreign key
                s = string.Format("ALTER TABLE {0} ", TABLE_NAME);
                s += @"  WITH CHECK ADD  CONSTRAINT [FK_AccountXblLinks_Accounts] FOREIGN KEY([AccountId])
                       REFERENCES [dbo].[Accounts] ([AccountId])";
                con.AdHocExecuteNonQuery(s);

                s = string.Format("ALTER TABLE {0} ", TABLE_NAME);
                s += @"CHECK CONSTRAINT [FK_AccountXblLinks_Accounts]";
                con.AdHocExecuteNonQuery(s);
            }

            return ctx.Success2();
        }

        public static Error QueryCreate(Int64 xuid,
                                        string gamertag,
                                        out Int64 accountId)
        {
            Context ctx = EnvDbContext.Create("xuid={0}, gamertag={1}", xuid, gamertag);
            
            Error err;
            accountId = 0;

            using (SqlTransactionScope ts = new SqlTransactionScope())
            {
                using (SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    if (xuid == 0)
                    {
                        return ctx.Error(ErrorCode.InvalidArguments);
                    }

                    //First, query to see if a link already exists.
                    AccountXblLinkInfo info;
                    if (null != (err = QueryLink(con, xuid, out info)))
                    {
                        return ctx.Error(err);
                    }

                    if (info == null)
                    {
                        //Otherwise, create an account.
                        if (null != (err = Account.CreateAccount(con, out accountId)))
                        {
                            return ctx.Error(err);
                        }

                        //Now create a link to the account.
                        if (null != (err = CreateLink(con, accountId, xuid, gamertag)))
                        {
                            return ctx.Error(err);
                        }
                    }
                    else
                    {
                        accountId = info.AccountId;
                    }

                    ts.Complete();
                }

                return ctx.Success2("accountId={0}", accountId);
            }
        }

        //PURPOSE
        //  Returns RockOn account ID assoicated with a XUID.
        //PARAMS
        //  con         - Open connection using DB that table resides in.
        //  xuid        - XUID to lookup link info by
        //  out info    - Link info
        //RETURNS
        //  On success, returns null and info param is non-null if a link was found, or null
        //  if none was found.  On failure, returns Error instance.
        public static Error QueryLink(SqlConnectionHelper con,
                                      Int64 xuid,
                                      out AccountXblLinkInfo info)
        {
            Context ctx = EnvDbContext.Create("xuid=0x{0}", xuid.ToString("X16"));

            info = null;

            List<AccountXblLinkInfo> infos = new List<AccountXblLinkInfo>();

            Error err = QueryLinks(con, xuid, 1, out infos);
            if (err != null)
            {
                return ctx.Error(err);
            }

            if (infos.Count == 0)
            {
                info = null;
                return ctx.Success2();
            }

            info = infos[0];
            
            return ctx.Success2("Found account ({0})",
                                info.AccountId);
        }

        public static Error QueryLinks(SqlConnectionHelper con,
                                       Int64 xuid,
                                       uint maxResults,
                                       out List<AccountXblLinkInfo> infos)
        {
            Context ctx = EnvDbContext.Create("xuid=0x{0}", xuid.ToString("X16"));
            
            infos = new List<AccountXblLinkInfo>();

            string s = string.Format("SELECT * FROM {0}", TABLE_NAME);

            SqlCmdParams cmdParams = new SqlCmdParams();

            if (xuid != 0)
            {
                s += string.Format(" WHERE Xuid=@Xuid", xuid);
                cmdParams.Add("@Xuid", xuid);
            }

            using(DbDataReader reader = con.ExecuteReader(s, cmdParams))
            {
                while (reader.Read())
                {
                    AccountXblLinkInfo info = new AccountXblLinkInfo();

                    info.AccountId = (Int64)reader["AccountId"];
                    info.CreateDateTime = (DateTime)reader["CreateDateTime"];
                    info.Xuid = (UInt64)((Int64)reader["Xuid"]);
                    info.Gamertag = reader["Gamertag"].ToString();

                    infos.Add(info);

                    if ((maxResults != 0) && (infos.Count >= maxResults))
                    {
                        break;
                    }
                }

                reader.Close();
            }

            return ctx.Success2("Found {0} account xuid links", infos.Count);
        }

        //PURPOSE
        //  Creates a link between RockOn account ID and XUID.
        //PARAMS
        //  con         - Open connection using DB that table resides in.
        //  accountId   - RockOn account ID to link to
        //  xuid        - XUID to link with
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        public static Error CreateLink(SqlConnectionHelper con,
                                       Int64 accountId,                                   
                                       Int64 xuid,
                                       string gamertag)
        {
            Context ctx = EnvDbContext.Create("acct={0}, xuid={1}, gamertag={2}",
                                           accountId,
                                           xuid.ToString("X16"),
                                           gamertag);
            
            if (accountId == 0 || xuid == 0)
            {
                return ctx.Error(ErrorCode.InvalidArguments);
            }

            string s = String.Format("INSERT INTO {0} (Xuid, AccountId, Gamertag) VALUES (@Xuid, @AccountId, @Gamertag)", 
                                     TABLE_NAME);

            SqlCmdParams cmdParams = new SqlCmdParams();
            cmdParams.Add("@Xuid", xuid);
            cmdParams.Add("@AccountId", accountId);
            cmdParams.Add("@Gamertag", gamertag);

            if (1 != con.ExecuteNonQuery(s, cmdParams))
            {
                return ctx.Error(ErrorCode.DbCommandFailed, "INSERT failed; perhaps account does not exist?");
            }

            return ctx.Success2();
        }

        //PURPOSE
        //  Deletes a Xuid link to a RockOn account.
        //PARAMS
        //  con         - Open connection using DB that table resides in.
        //  xuid        - XUID of link to remove
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        public static Error DeleteLink(SqlConnectionHelper con,
                                       Int64 xuid)
        {
            Context ctx = EnvDbContext.Create("xuid=0x{0}", xuid.ToString("X16"));
            
            string s = String.Format("DELETE FROM {0} WHERE Xuid=@Xuid", TABLE_NAME);

            SqlCmdParams cmdParams = new SqlCmdParams();
            cmdParams.Add("@Xuid", xuid);

            if (con.ExecuteNonQuery(s, cmdParams) != 1)
            {
                return ctx.Error(ErrorCode.DbCommandFailed, "DELETE failed; perhaps already deleted?");
            }

            return ctx.Success2();
        }
    }
}
