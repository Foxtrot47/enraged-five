using System;

namespace Rockstar.Rockon.EnvDb
{
    //PURPOSE
    //  Describes an entry in the Accounts table of the environment DB.
    public class AccountInfo
    {
        public Int64 Id = 0;
        public DateTime CreateDateTime = DateTime.MinValue;
    }
}
