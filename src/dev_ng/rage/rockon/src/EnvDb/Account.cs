using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Net;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.EnvDb
{
    public class Account
    {
        private const string ACCOUNTS_TABLE_NAME        = "Accounts";
        private const string LOGIN_STATUS_TABLE_NAME    = "LoginStatus";

        //PURPOSE
        //  Creates the accounts and logged-in users tables
        //PARAMS
        //  con     - Open connection using DB that table should be created in.
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        public static Error CreateTables(SqlConnectionHelper con)
        {
            Context ctx = EnvDbContext.Create();

            try
            {
                ctx.Debug2("Creating table: " + ACCOUNTS_TABLE_NAME + "...");

                if (con.CheckTableExists(ACCOUNTS_TABLE_NAME))
                {
                    ctx.Warning("Table {0} exists.  It must be manually dropped before it can be recreated (this is to avoid accidental deletion).", ACCOUNTS_TABLE_NAME);
                }
                else
                {
                    string s = "CREATE TABLE " + ACCOUNTS_TABLE_NAME;
                    s += @"([AccountId] [bigint] IDENTITY(1,1) NOT NULL,
                            [CreateDateTime] [datetime] NOT NULL DEFAULT(GETUTCDATE()),
                            CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
                            (
	                            [AccountId] ASC
                            )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                            ) ON [PRIMARY]";
                    con.AdHocExecuteNonQuery(s);
                }

                ctx.Debug2("Creating table: " + LOGIN_STATUS_TABLE_NAME + "...");

                if (con.CheckTableExists(LOGIN_STATUS_TABLE_NAME))
                {
                    ctx.Warning("Table {0} exists.  It must be manually dropped before it can be recreated (this is to avoid accidental deletion).", LOGIN_STATUS_TABLE_NAME);
                }
                else
                {
                    string s = "CREATE TABLE " + LOGIN_STATUS_TABLE_NAME;
                    s += @"([AccountId] [bigint] NOT NULL,
                                [LoginTitleId] [int] NULL,
                                [LoginSgRelayAddr] [varchar](50) NULL,
                                [LoginDateTime] [datetime] NULL,
                                [LogoutTitleId] [int] NULL,
                                [LogoutSgRelayAddr] [varchar](50) NULL,
                                [LogoutSessionDurationMs] [bigint] NULL,
                                [LogoutDateTime] [datetime] NULL,
                                CONSTRAINT [PK_LoginStatus] PRIMARY KEY CLUSTERED 
                                (
                                    [AccountId] ASC
                                )) ON [PRIMARY]";

                    con.AdHocExecuteNonQuery(s);

                    //This trigger creates an entry in the LoginStatus table
                    //for each entry created in the Accounts table.
                    s = @"
                    CREATE TRIGGER AddAccountTrigger 
                        ON {0} 
                        AFTER INSERT
                    AS 
                    BEGIN
                        -- SET NOCOUNT ON added to prevent extra result sets from
                        -- interfering with SELECT statements.
                        SET NOCOUNT ON;

                        INSERT INTO {1} (AccountId) SELECT AccountId FROM inserted;
                    END";

                    s = string.Format(s, ACCOUNTS_TABLE_NAME, LOGIN_STATUS_TABLE_NAME);
                    con.AdHocExecuteNonQuery(s);

                    //This trigger deletes an entry in the LoginStatus table
                    //for each entry deleted in the Accounts table.
                    s = @"
                    CREATE TRIGGER RemoveAccountTrigger 
                        ON {0} 
                        AFTER DELETE
                    AS 
                    BEGIN
                        -- SET NOCOUNT ON added to prevent extra result sets from
                        -- interfering with SELECT statements.
                        SET NOCOUNT ON;

                        DELETE FROM {1} WHERE AccountId IN (SELECT AccountId FROM deleted);
                        DELETE FROM {2} WHERE AccountId IN (SELECT AccountId FROM deleted);
                        DELETE FROM {3} WHERE AccountId IN (SELECT AccountId FROM deleted);
                    END";

                    s = string.Format(s,
                                      ACCOUNTS_TABLE_NAME,
                                      LOGIN_STATUS_TABLE_NAME,
                                      AccountXblLink.TABLE_NAME,
                                      AccountNpLink.TABLE_NAME);
                    con.AdHocExecuteNonQuery(s);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //TODO: Need a corresponding DropTables()

        //PURPOSE
        //  Convenience method for getting a single account by ID.
        //PARAMS
        //  con             - Open connection using DB that table resides in.
        //  accountId       - Account to query.
        //  out info        - Account info.
        //RETURNS
        //  On success, returns null and info will be non-null if account found.  
        //  On failure, returns Error instance.
        public static Error QueryAccount(SqlConnectionHelper con,
                                         Int64 accountId,
                                         out AccountInfo info)
        {
            Context ctx = EnvDbContext.Create("{0}", accountId);
            
            info = null;

            try
            {
                List<AccountInfo> infos;
                Error err = QueryAccount(con, accountId, 1, out infos);

                if (err != null)
                {
                    return ctx.Error(err);
                }

                info = infos[0];

                if (info.Id != accountId)
                {
                    return ctx.Error(ErrorCode.UnknownError,
                                     "ID of retrieved account ({0}) does not match requested ({1})",
                                     info.Id, accountId);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Returns info on a single account, or up to maxResults accounts if accountId is zero.
        //PARAMS
        //  con             - Open connection using DB that table resides in.
        //  accountId       - Account to query.
        //  maxResults      - If accountId is zero, query will return up to this many accounts,
        //                    or all accounts if maxResults is zero.
        //  out infos       - List of found accounts.
        //RETURNS
        //  On success, returns null and infos is non-null.  On failure, returns Error instance.
        public static Error QueryAccount(SqlConnectionHelper con,
                                         Int64 accountId,
                                         uint maxResults,
                                         out List<AccountInfo> infos)
        {
            Context ctx = EnvDbContext.Create("id={0}, max={1}", accountId, maxResults);
            
            infos = new List<AccountInfo>();

            try
            {
                SqlCmdParams cmdParams = new SqlCmdParams();

                string s = "SELECT * FROM " + ACCOUNTS_TABLE_NAME;

                if (accountId != 0)
                {
                    s += String.Format(" WHERE AccountId=@AccountId");
                    cmdParams.Add("@AccountId", accountId);
                }

                using (DbDataReader reader = con.ExecuteReader(s, cmdParams))
                {
                    while (reader.Read())
                    {
                        AccountInfo info = new AccountInfo();

                        info.Id = (Int64)reader["AccountId"];
                        info.CreateDateTime = (DateTime)reader["CreateDateTime"];

                        infos.Add(info);

                        if ((maxResults != 0) && (infos.Count >= maxResults))
                        {
                            break;
                        }
                    }

                    reader.Close();
                }

                return ctx.Success2("Found {0} accounts)", infos.Count);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Creates an account record.
        //PARAMS
        //  con             - Open connection using DB that table resides in.
        //  out accountId   - ID of new account
        //RETURNS
        //  On success, returns null and account ID is nonzero.  On failure, returns Error instance.
        public static Error CreateAccount(SqlConnectionHelper con,
                                          out Int64 accountId)
        {
            Context ctx = EnvDbContext.Create();
            
            accountId = 0;

            try
            {
                string s = string.Format("INSERT INTO {0} (CreateDateTime) VALUES (GETUTCDATE())", ACCOUNTS_TABLE_NAME);
                s += "; SELECT CAST(scope_identity() AS bigint)";

                accountId = Int64.Parse(con.AdHocExecuteScalar(s).ToString());

                return ctx.Success2("Created account {0}", accountId);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Deletes an account record.
        //PARAMS
        //  con             - Open connection using DB that table resides in.
        //  accountId       - ID of account to delete.
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        //NOTES
        //  There may be many references to this account ID, prohibiting its removal.
        //  Those references need to be removed first by the user.
        public static Error DeleteAccount(SqlConnectionHelper con,
                                          Int64 accountId)
        {
            Context ctx = EnvDbContext.Create("{0}", accountId);
            
            try
            {
                if (accountId == 0)
                {
                    return ctx.Error(ErrorCode.InvalidArguments);
                }

                string s = String.Format("DELETE FROM {0} WHERE AccountId=@AccountId", ACCOUNTS_TABLE_NAME);

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@AccountId", accountId);

                if (con.ExecuteNonQuery(s, cmdParams) != 1)
                {
                    return ctx.Error(ErrorCode.DbCommandFailed);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        public static Error LoginUser(SqlConnectionHelper con,
                                    int titleId,
                                    Int64 accountId,
                                    //uint clientId,
                                    IPEndPoint sgRelayAddr)
        {
            Context ctx = EnvDbContext.Create();
            
            try
            {
                string cmd =
                    string.Format("UPDATE {0} SET LoginTitleId=@LoginTitleId, LoginSgRelayAddr=@LoginSgRelayAddr, LoginDateTime=GETUTCDATE() WHERE AccountId=@AccountId",
                                  LOGIN_STATUS_TABLE_NAME);

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@LoginTitleId", titleId);
                cmdParams.Add("@LoginSgRelayAddr", sgRelayAddr.Address.ToString() + ":" + sgRelayAddr.Port.ToString());
                cmdParams.Add("@AccountId", accountId);

                con.ExecuteNonQuery(cmd, cmdParams);

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        public static Error LogoutUser(SqlConnectionHelper con,
                                       int titleId, 
                                       Int64 accountId,
                                       IPEndPoint sgRelayEndPoint,
                                       Int64 sessionDurationMs)
        {
            Context ctx = EnvDbContext.Create("title={0}, acct={1}", titleId, accountId);
            
            try
            {
                string cmd =
                string.Format("UPDATE {0} SET LogoutTitleId=@LogoutTitleId, LogoutSgRelayAddr=@LogoutSgRelayAddr, LogoutSessionDurationMs=@LogoutSessionDurationMs, LogoutDateTime=GETUTCDATE() WHERE AccountId=@AccountId",
                              LOGIN_STATUS_TABLE_NAME);

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@AccountId", accountId);
                cmdParams.Add("@LogoutTitleId", titleId);
                cmdParams.Add("@LogoutSgRelayAddr", string.Format("{0}:{1}", sgRelayEndPoint.Address, sgRelayEndPoint.Port));
                cmdParams.Add("@LogoutSessionDurationMs", sessionDurationMs);
                
                con.ExecuteNonQuery(cmd, cmdParams);

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        public static Error QueryLoginStatus(SqlConnectionHelper con,
                                             int titleId,
                                             Int64 accountId,
                                             bool loggedInOnly,
                                             out List<LoginStatusInfo> infos)
        {
            Context ctx = EnvDbContext.Create(accountId.ToString());
            infos = new List<LoginStatusInfo>();

            try
            {
                SqlFilterStringBuilder fsb = new SqlFilterStringBuilder();
                SqlCmdParams cmdParams = new SqlCmdParams();
                string cmd = string.Format("SELECT * FROM LoginStatus");

                if (titleId != 0)
                {
                    fsb.AddCondition("(LoginTitleId=@TitleId OR LogoutTitleId=@TitleId)");
                    cmdParams.Add("@TitleId", titleId);
                }

                if (accountId != 0)
                {
                    fsb.AddCondition("AccountId=@AccountId");
                    cmdParams.Add("@AccountId", accountId);
                }

                if (loggedInOnly)
                {
                    fsb.AddCondition("LoginDateTime > LogoutDateTime");
                }

                cmd += fsb.ToString();

                using (DbDataReader reader = con.ExecuteReader(cmd, cmdParams))
                {
                    while (reader.Read())
                    {
                        LoginStatusInfo info = new LoginStatusInfo();

                        info.AccountId = (Int64)reader["AccountId"];

                        object tmp = reader["LoginTitleId"];
                        if (!(tmp is System.DBNull))
                        {
                            info.LoginTitleId = (int)tmp;
                        }

                        tmp = reader["LoginSgRelayAddr"];
                        if (!(tmp is System.DBNull))
                        {
                            string[] parts = tmp.ToString().Split(':');
                            info.LoginSgRelayEndPoint = new IPEndPoint(IPAddress.Parse(parts[0]), int.Parse(parts[1]));
                        }

                        tmp = reader["LoginDateTime"];
                        if (!(tmp is System.DBNull))
                        {
                            info.LoginDateTime = (DateTime)tmp;
                        }

                        tmp = reader["LogoutTitleId"];
                        if (!(tmp is System.DBNull))
                        {
                            info.LogoutTitleId = (int)tmp;
                        }

                        tmp = reader["LogoutSgRelayAddr"];
                        if (!(tmp is System.DBNull))
                        {
                            string[] parts = tmp.ToString().Split(':');
                            info.LogoutSgRelayEndPoint = new IPEndPoint(IPAddress.Parse(parts[0]), int.Parse(parts[1]));
                        }

                        tmp = reader["LogoutSessionDurationMs"];
                        if (!(tmp is System.DBNull))
                        {
                            info.LogoutSessionDurationMs = (Int64)tmp;
                        }

                        tmp = reader["LogoutDateTime"];
                        if (!(tmp is System.DBNull))
                        {
                            info.LogoutDateTime = (DateTime)tmp;
                        }

                        infos.Add(info);
                    }
                }

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
    }
}
