using System;

namespace Rockstar.Rockon.EnvDb
{
    //PURPOSE
    //  Describes a link between a RockOn account and an NP account.
    public class AccountNpLinkInfo
    {
        public Int64 AccountId = 0;
        public string OnlineId = null;
        public int OnlineIdHash = 0;
        public DateTime CreateDateTime = DateTime.MinValue;   //Create datetime of link, not account.
    }
}
