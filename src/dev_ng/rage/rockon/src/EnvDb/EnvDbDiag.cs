using System;
using Rockstar.Rockon.Diag;

namespace Rockstar.Rockon.EnvDb
{
    public class EnvDbChannel
    {
        private static Channel m_Channel = new Channel("EnvDb");

        public static Channel Channel
        {
            get { return m_Channel; }
        }
    }

    public class EnvDbContext
    {
        public static Context Create()
        {
            return new Context(EnvDbChannel.Channel);
        }

        public static Context Create(string desc)
        {
            return new Context(EnvDbChannel.Channel, desc);
        }

        public static Context Create(string format, params object[] args)
        {
            return new Context(EnvDbChannel.Channel, format, args);
        }
    }
}
