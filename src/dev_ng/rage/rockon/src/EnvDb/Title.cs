using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Web.Caching;
using System.Collections;
using System.Collections.Generic;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.EnvDb
{
    public class Title
    {
        private const string TABLE_NAME = "Titles";
        private const int CACHE_ABSOLUTE_EXPIRATION_MINUTES = 60 * 24; //Daily

        //PURPOSE
        //  Creates the titles table
        //PARAMS
        //  con     - Open connection using DB that table should be created in.
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        public static Error CreateTable(SqlConnectionHelper con)
        {
            Context ctx = EnvDbContext.Create();

            try
            {
                if (con.CheckTableExists(TABLE_NAME))
                {
                    ctx.Warning("Title.CreateTable: Table exists.  It must be manually dropped before it can be recreated (this is to avoid accidental deletion).");
                }
                else
                {
                    string s = "CREATE TABLE " + TABLE_NAME;
                    s += @"([TitleId] [int] NOT NULL,
                           [ConnectionStr] [varchar](255) NOT NULL,
                           [DbName] [varchar](255) NOT NULL,
                           [CreateDateTime] [datetime] NOT NULL DEFAULT(GETUTCDATE()))";

                    con.AdHocExecuteNonQuery(s);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return new Error(ex);
            }
        }

        //PURPOSE
        //  Connects to the title DB server, and USEs the title DB.
        //RETURNS
        //  Connection object on success, null on failure.
        public static SqlConnectionHelper Open(int titleId)
        {
            return Open(Title.GetCachedTitleInfo(titleId));
        }       
        
        public static SqlConnectionHelper Open(TitleInfo info)
        {
            SqlConnectionHelper con = null;
            
            try
            {
                con = new SqlConnectionHelper(info.ConnectionStr);
                con.Open();
            }
            catch (Exception ex)
            {
                if (con != null)
                {
                    con.Dispose();
                }
                throw (ex);
            }

            return con;
        }

        //PURPOSE
        //  Returns info on a single title, or up to maxResults titles if titleId is zero.
        //PARAMS
        //  con             - Open connection using DB that table resides in.
        //  titleId         - Title to query.
        //  maxResults      - If titleId is zero, query will return up to this many titles,
        //                    or all titles if maxResults is zero.
        //  out infos       - List of found titles.
        //RETURNS
        //  On success, returns null and infos is non-null.  On failure, returns Error instance.
        public static Error QueryTitle(SqlConnectionHelper con,
                                       int titleId,
                                       uint maxResults,
                                       out List<TitleInfo> infos)
        {
            Context ctx = EnvDbContext.Create("id={0}, maxResults={1}", titleId, maxResults);
            infos = null;

            try
            {
                infos = new List<TitleInfo>();

                SqlCmdParams cmdParams = new SqlCmdParams();
                string s = "SELECT * FROM " + TABLE_NAME;

                if (titleId != 0)
                {
                    s += String.Format(" WHERE TitleId=@TitleId", titleId);
                    cmdParams.Add("@TitleId", titleId);
                }

                using(DbDataReader reader = con.ExecuteReader(s, cmdParams))
                {
                    while (reader.Read())
                    {
                        TitleInfo info = new TitleInfo();

                        info.Id = (int)reader["TitleId"];
                        info.ConnectionStr = reader["ConnectionStr"].ToString();
                        info.CreateDateTime = (DateTime)reader["CreateDateTime"];

                        infos.Add(info);

                        if ((maxResults != 0) && (infos.Count >= maxResults))
                        {
                            break;
                        }
                    }

                    reader.Close();
                }

                return ctx.Success2("Read {0} titles", infos.Count);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Creates a title record, and it's DB (if doesn't already exist).
        //PARAMS
        //  con             - Open connection using DB that table resides in.
        //  name            - Internal name for title (ex. mc4_ps3)
        //  platform        - Platform
        //  connectionStr   - Connection string to server where title's DB lives
        //  out titleId     - ID of new title
        //RETURNS
        //  On success, returns null and title ID is zero.  On failure, returns Error instance.
        public static Error CreateTitle(SqlConnectionHelper con,
                                        int titleId,                                        
                                        string connectionStr)
        {
            Context ctx = EnvDbContext.Create("id={0}", titleId);
            bool createdEntry = false;

            try
            {
                if (titleId <= 0
                    || connectionStr.Length == 0)
                {
                    return ctx.Error(ErrorCode.InvalidArguments);
                }

                SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(connectionStr);
                if (sb.Database == null)
                {
                    return ctx.Error(ErrorCode.InvalidArguments, "Database not specified in connection string");
                }

                //Create the title record
                string s = "INSERT INTO " + TABLE_NAME;
                
                s += @"([TitleId]
                        ,[ConnectionStr])";

                s += string.Format(@"VALUES(@TitleId, @ConnectionStr");

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@TitleId", titleId);
                cmdParams.Add("@ConnectionStr", connectionStr);

                if (1 != con.ExecuteNonQuery(s, cmdParams))
                {
                    return ctx.Error(ErrorCode.DbCommandFailed);
                }

                createdEntry = true;

                //Create the title DB
                if (SqlHelper.CheckDatabaseExists(connectionStr, sb.Database))
                {
                    return ctx.Success2("Title DB already exists, no need to create");
                }

                SqlHelper.AdHocExecuteNonQuery(connectionStr, "CREATE DATABASE " + sb.Database);

                return ctx.Success2("Created title DB (\"{0}\")", sb.Database);
            }
            catch (Exception ex)
            {
                //We may have failed creating the database after the entry was put in the titles
                //table, so delete the title if it known.
                if (createdEntry)
                {
                    ctx.Debug2("Exception after creating title entry {0}, deleting entry", titleId);
                    DeleteTitle(con, titleId);
                }

                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Deletes a title record.  Note that this does NOT drop the title's DB;
        //  that is deemed too dangerous to do except manually.
        //PARAMS
        //  con           - Open connection using DB that table resides in.
        //  titleId       - ID of title to delete.
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        //NOTES
        //  There may be many references to this title ID, prohibiting its removal.
        //  Those references need to be removed first by the user.
        public static Error DeleteTitle(SqlConnectionHelper con,
                                        int titleId)
        {
            Context ctx = EnvDbContext.Create("id={0}", titleId); 

            try
            {
                if (titleId <= 0)
                {
                    return ctx.Error(ErrorCode.InvalidArguments);
                }

                string s = String.Format("DELETE FROM {0} WHERE TitleId=@TitleId", TABLE_NAME);

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@TitleId", titleId);

                if (con.ExecuteNonQuery(s, cmdParams) != 1)
                {
                    return ctx.Error(ErrorCode.DbCommandFailed);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Retrieves TitleInfo from the cache, adding it if not found.
        //RETURNS
        //  Like all GetCachedXXX() methods, it is assumed that this will 
        //  succeed and return a non-null instance.  
        //  Errors are communicated as exceptions, to make the calling code
        //  (almost always a web service) less verbose.
        public static TitleInfo GetCachedTitleInfo(int titleId)
        {
            TitleInfo info = CacheHelper<TitleInfo>.Get(string.Format("EnvDb_TitleInfo_{0}", titleId),
                                                        CreateTitleInfoDelegate,
                                                        titleId);

            if (info == null)
            {
                throw (new Exception(string.Format("Failed to get title {0} from cache", titleId)));
            }

            return info;
        }

        //PURPOSE
        //  Object creation callback for GetCachedTitleInfo().
        public static void CreateTitleInfoDelegate(object userdata,
                                                   out TitleInfo obj,
                                                   out DateTime absoluteExpirationTime)
        {
            obj = null;
            absoluteExpirationTime = DateTime.Now;

            using (SqlConnectionHelper con = EnvDbHelper.Open())
            {
                List<TitleInfo> titleInfos = null;

                Title.QueryTitle(con, (int)userdata, 1, out titleInfos);

                if (titleInfos.Count > 0)
                {
                    obj = titleInfos[0];
                    absoluteExpirationTime = DateTime.Now.AddMinutes(CACHE_ABSOLUTE_EXPIRATION_MINUTES);
                }
            }
        }
    }
}
