using System;
using System.Net;
using System.Xml;
using System.Xml.Serialization;

namespace Rockstar.Rockon.EnvDb
{
    //PURPOSE
    //  Describes an entry in the LoginStatus table of the environment DB.
    //NOTE
    //  The little bit of ugliness with relay addresses is due to the fact that
    //  IPAddress doesn't have a default ctor, making it unserializable.
    public class LoginStatusInfo
    {
        public Int64 AccountId;
        
        //Last login
        public int LoginTitleId;
        public DateTime LoginDateTime;

        [XmlIgnore]
        public IPEndPoint LoginSgRelayEndPoint = new IPEndPoint(0, 0);
        
        public string LoginSgRelayAddr
        {
            get { return string.Format("{0}:{1}", LoginSgRelayEndPoint.Address, LoginSgRelayEndPoint.Port); }
        }

        //Last logout info
        public int LogoutTitleId;
        public DateTime LogoutDateTime;
        public Int64 LogoutSessionDurationMs;

        [XmlIgnore]
        public IPEndPoint LogoutSgRelayEndPoint = new IPEndPoint(0, 0);

        public string LogoutSgRelayAddr
        {
            get { return string.Format("{0}:{1}", LogoutSgRelayEndPoint.Address, LogoutSgRelayEndPoint.Port); }
        }
    }
}
