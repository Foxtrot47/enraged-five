using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.GlobalDb
{
    public class GlobalDbHelper
    {
        //PURPOSE
        //  Creates the global DB and minimum required tables
        //PARAMS
        //  connectionString    - Conn str for server to create DB on.
        //                        This must include the DB name for the global DB.
        //RETURNS
        //  Null on success, Error instance on failure.
        public static Error Bootstrap(string connectionString)
        {
            Context ctx = GlobalDbContext.Create();

            try
            {
                SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(connectionString);

                if (sb.Database == null)
                {
                    return ctx.Error(ErrorCode.InvalidArguments, "Connection string must specify the database");
                }

                //Create DB
                if (SqlHelper.CheckDatabaseExists(connectionString, sb.Database))
                {
                    ctx.Debug2("DB '{0}' already exists on server '{1}'", sb.Database, sb.Server);
                }
                else
                {
                    SqlHelper.AdHocExecuteNonQuery(connectionString, "CREATE DATABASE " + sb.Database);
                }

                using (SqlConnectionHelper con = new SqlConnectionHelper(connectionString))
                {
                    if (!con.Open())
                    {
                        return ctx.Error("Failed to open connection to '{0}'", connectionString);
                    }

                    Error err = null;

                    //Create required tables
                    ctx.Debug2("Creating platforms table...");
                    if (null != (err = Platform.CreateTable(con)))
                    {
                        return ctx.Error(err);
                    }

                    ctx.Debug2("Creating titles table...");
                    if (null != (err = Title.CreateTable(con)))
                    {
                        return ctx.Error(err);
                    }

                    //Populate the platforms table, if not already done (ignore errors).
                    Platform.CreatePlatform(con, (int)PlatformId.Xbox360, "xbox360", "Xbox 360", true);
                    Platform.CreatePlatform(con, (int)PlatformId.PS3, "ps3", "Playstation 3", false);
                    Platform.CreatePlatform(con, (int)PlatformId.PC_GFWL, "pcgfwl", "PC (GWFL)", true);
                    Platform.CreatePlatform(con, (int)PlatformId.PSP, "psp", "PSP", false);
                    Platform.CreatePlatform(con, (int)PlatformId.DS, "ds", "Nintendo DS", false);
                    Platform.CreatePlatform(con, (int)PlatformId.Wii, "wii", "Wii", false);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Connects to the global DB server, and USEs the global DB.  It assumes
        //  the connectionstring and dbName are specified in the configuration data.
        //RETURNS
        //  Connection object on success, null on failure.
        private static string sm_ConnectionString;

        public static SqlConnectionHelper Open()
        {
            SqlConnectionHelper con = null;

            try
            {
                if (sm_ConnectionString == null)
                {
                    const string SETTING_CONNECTIONSTRING = "RockonGlobalDbConnectionString";

                    sm_ConnectionString = ConfigurationManager.AppSettings[SETTING_CONNECTIONSTRING];
                    if ((sm_ConnectionString == null) || (sm_ConnectionString.Length == 0))
                    {
                        throw (new Exception(string.Format("Could not find config setting [{0}]", SETTING_CONNECTIONSTRING)));
                    }
                }

                con = new SqlConnectionHelper(sm_ConnectionString);
                con.Open();
            }
            catch (Exception ex)
            {
                if (con != null)
                {
                    con.Dispose();
                }
                throw (ex);
            }

            return con;
        }
    }
}
