using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.GlobalDb
{
    public class Platform
    {
        private const string TABLE_NAME = "Platforms";
        private const int CACHE_ABSOLUTE_EXPIRATION_MINUTES = 60 * 24; //Daily

        //PURPOSE
        //  Creates the platforms table.
        //PARAMS
        //  con     - Open connection using DB that table should be created in.
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        public static Error CreateTable(SqlConnectionHelper con)
        {
            Context ctx = GlobalDbContext.Create();

            try
            {
                if (con.CheckTableExists(TABLE_NAME))
                {
                    return ctx.Error(ErrorCode.AlreadyExists, "Table exists.  It must be manually dropped before it can be recreated (this is to avoid accidental deletion).");
                }

                string s = "CREATE TABLE " + TABLE_NAME;
                s += @"([PlatformId] [int] NOT NULL,
                        [InternalName] [varchar](255) NOT NULL,
                        [FullName] [varchar](255) NOT NULL,
                        [IsSecure] [bit] NOT NULL DEFAULT(0),
                        [CreateDateTime] [datetime] NOT NULL DEFAULT(GETUTCDATE()))";

                con.AdHocExecuteNonQuery(s);

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Returns info on a single platform, or up to maxResults if platformId is zero.
        //PARAMS
        //  con             - Open connection using DB that table resides in.
        //  platformId      - Platform to query.
        //  maxResults      - If platformId is zero, query will return up to this many platforms,
        //                    or all platforms if maxResults is zero.
        //  out infos       - List of found platforms.
        //RETURNS
        //  On success, returns null and infos is non-null.  On failure, returns Error instance.
        public static Error QueryPlatform(SqlConnectionHelper con,
                                          int platformId,
                                          uint maxResults,
                                          out List<PlatformInfo> infos)
        {
            Context ctx = GlobalDbContext.Create();
            infos = null;

            try
            {
                infos = new List<PlatformInfo>();

                string s = "SELECT * FROM " + TABLE_NAME;
                SqlCmdParams cmdParams = new SqlCmdParams();

                if (platformId != 0)
                {
                    s += String.Format(" WHERE PlatformId=@PlatformId");
                    cmdParams.Add("@PlatformId", platformId);
                }

                using (DbDataReader reader = con.ExecuteReader(s, cmdParams))
                {
                    while (reader.Read())
                    {
                        PlatformInfo info = new PlatformInfo();

                        info.Id = (int)reader["PlatformId"];
                        info.InternalName = reader["InternalName"].ToString();
                        info.FullName = reader["FullName"].ToString();
                        info.CreateDateTime = (DateTime)reader["CreateDateTime"];
                        info.IsSecure = (bool)reader["IsSecure"];

                        infos.Add(info);

                        if ((maxResults != 0) && (infos.Count >= maxResults))
                        {
                            break;
                        }
                    }
                }

                return ctx.Success2("Found {0} platforms (maxResults={1})", infos.Count, maxResults);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Creates a platform record.
        //PARAMS
        //  con             - Open connection using DB that table resides in.
        //RETURNS
        //  On success, returns null and platform ID in non-null.  On failure, returns Error instance.
        public static Error CreatePlatform(SqlConnectionHelper con,
                                           int platformId,
                                           string internalName,
                                           string fullName,
                                           bool isSecure)
        {
            Context ctx = GlobalDbContext.Create("id={0}, iname={1}, fname={2}, isSecure={3}", isSecure, 
                                      platformId, internalName, fullName);
            try
            {
                if (platformId <= 0
                    || internalName.Length == 0
                    || fullName.Length == 0)
                {
                    return ctx.Error(ErrorCode.InvalidArguments);
                }

                string s = string.Format("INSERT INTO {0} (PlatformId, InternalName, FullName, IsSecure) VALUES (@PlatformId, @InternalName, @FullName, @IsSecure)",
                                         TABLE_NAME);

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@PlatformId", platformId);
                cmdParams.Add("@InternalName", internalName);
                cmdParams.Add("@FullName", fullName);
                cmdParams.Add("@IsSecure", isSecure ? 1 : 0);

                if (1 != con.ExecuteNonQuery(s, cmdParams))
                {
                    return ctx.Error(ErrorCode.DbCommandFailed);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Deletes a platform record.
        //PARAMS
        //  con             - Open connection using DB that table resides in.
        //  platformId      - ID of platform to delete.
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        //NOTES
        //  There may be many references to this platform ID, prohibiting its removal.
        //  Those references need to be removed first by the user.
        public static Error DeletePlatform(SqlConnectionHelper con,
                                           int platformId)
        {
            Context ctx = GlobalDbContext.Create("id={0}", platformId);

            try
            {
                if (platformId == 0)
                {
                    return ctx.Error(ErrorCode.InvalidArguments);
                }

                string s = String.Format("DELETE FROM {0} WHERE PlatformId=@Platformid", TABLE_NAME);

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@PlatformId", platformId);

                if (con.ExecuteNonQuery(s, cmdParams) != 1)
                {
                    return ctx.Error(ErrorCode.DbCommandFailed);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Retrieves PlatformInfo from the cache, adding it if not found.
        //RETURNS
        //  Like all GetCachedXXX() methods, it is assumed that this will 
        //  succeed and return a non-null instance.  
        //  Errors are communicated as exceptions, to make the calling code
        //  (almost always a web service) less verbose.
        public static PlatformInfo GetCachedPlatformInfo(int platformId)
        {
            PlatformInfo info = CacheHelper<PlatformInfo>.Get(string.Format("GlobalDb_PlatformInfo_{0}", platformId),
                                                        CreatePlatformInfoDelegate,
                                                        platformId);

            if (info == null)
            {
                throw (new Exception(string.Format("Failed to get title {0} from cache", platformId)));
            }

            return info;
        }

        //PURPOSE
        //  Object creation callback for GetCachedPlatformInfo().
        public static void CreatePlatformInfoDelegate(object userdata,
                                                   out PlatformInfo obj,
                                                   out DateTime absoluteExpirationTime)
        {
            obj = null;
            absoluteExpirationTime = DateTime.Now;

            using (SqlConnectionHelper con = GlobalDbHelper.Open())
            {
                List<PlatformInfo> PlatformInfos = null;

                Platform.QueryPlatform(con, (int)userdata, 1, out PlatformInfos);

                if (PlatformInfos.Count > 0)
                {
                    obj = PlatformInfos[0];
                    absoluteExpirationTime = DateTime.Now.AddMinutes(CACHE_ABSOLUTE_EXPIRATION_MINUTES);
                }
            }
        }
    }
}
