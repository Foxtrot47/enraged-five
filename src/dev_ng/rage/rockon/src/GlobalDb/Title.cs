using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.GlobalDb
{
    public class Title
    {
        private const string TABLE_NAME = "Titles";
        private const int CACHE_ABSOLUTE_EXPIRATION_MINUTES = 60 * 24; //Daily

        //PURPOSE
        //  Creates the titles table
        //PARAMS
        //  con     - Open connection using DB that table should be created in.
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        public static Error CreateTable(SqlConnectionHelper con)
        {
            Context ctx = GlobalDbContext.Create();

            try
            {
                if (con.CheckTableExists(TABLE_NAME))
                {
                    return ctx.Error(ErrorCode.AlreadyExists, "Table exists.  It must be manually dropped before it can be recreated (this is to avoid accidental deletion).");
                }

                string s = "CREATE TABLE " + TABLE_NAME;
                s += @"([TitleId] [int] NOT NULL,
                       [PlatformId] [int] NOT NULL,
                       [InternalName] [varchar](255) NOT NULL,
                       [FullName] [varchar](255) NOT NULL,
                       [CreateDateTime] [datetime] NOT NULL DEFAULT(GETUTCDATE()))";

                con.AdHocExecuteNonQuery(s);

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Returns info on a single title, or up to maxResults titles if titleId is zero.
        //PARAMS
        //  con             - Open connection using DB that table resides in.
        //  titleId         - Title to query.
        //  maxResults      - If titleId is zero, query will return up to this many titles,
        //                    or all titles if maxResults is zero.
        //  out infos       - List of found titles.
        //RETURNS
        //  On success, returns null and infos is non-null.  On failure, returns Error instance.
        public static Error QueryTitle(SqlConnectionHelper con,
                                       int titleId,
                                       uint maxResults,
                                       out List<TitleInfo> infos)
        {
            Context ctx = GlobalDbContext.Create("id={0}, maxResults={1}", titleId, maxResults);
            infos = null;

            try
            {
                infos = new List<TitleInfo>();

                string s = "SELECT * FROM " + TABLE_NAME;
                SqlCmdParams cmdParams = new SqlCmdParams();

                if (titleId != 0)
                {
                    s += String.Format(" WHERE TitleId={0}", titleId);
                    cmdParams.Add("@TitleId", titleId);
                }

                using (DbDataReader reader = con.ExecuteReader(s, cmdParams))
                {
                    while (reader.Read())
                    {
                        TitleInfo info = new TitleInfo();

                        info.Id = (int)reader["TitleId"];
                        info.PlatformId = (int)reader["PlatformId"];
                        info.InternalName = reader["InternalName"].ToString();
                        info.FullName = reader["FullName"].ToString();
                        info.CreateDateTime = (DateTime)reader["CreateDateTime"];

                        infos.Add(info);

                        if ((maxResults != 0) && (infos.Count >= maxResults))
                        {
                            break;
                        }
                    }
                }

                return ctx.Success2("Read {0} titles", infos.Count);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Creates a title record, and it's DB (if doesn't already exist).
        //PARAMS
        //  con             - Open connection using DB that table resides in.
        //  name            - Internal name for title (ex. mc4_ps3)
        //  platform        - Platform
        //  connectionStr   - Connection string to server where title's DB lives
        //  dbName          - Name of title's DB
        //  out titleId     - ID of new title
        //RETURNS
        //  On success, returns null and title ID is zero.  On failure, returns Error instance.
        public static Error CreateTitle(SqlConnectionHelper con,
                                        int titleId,
                                        int platformId,
                                        string internalName,
                                        string fullName)
        {
            Context ctx = GlobalDbContext.Create("id={0}, plat={1}, iname={2}", 
                                      titleId, platformId, internalName); 

            try
            {
                if (titleId <= 0 
                    || platformId <= 0
                    || internalName.Length == 0
                    || fullName.Length == 0)
                {
                    return ctx.Error(ErrorCode.InvalidArguments);
                }

                //Create the title record
                string s = "INSERT INTO " + TABLE_NAME;
                
                s += @"([TitleId]
                        ,[PlatformId]
                        ,[InternalName]
                        ,[FullName])";

                s += "VALUES(@TitleId, @PlatformId, @InternalName, @FullName)";

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@TitleId", titleId);
                cmdParams.Add("@PlatformId", platformId);
                cmdParams.Add("@InternalName", internalName);
                cmdParams.Add("@FullName", fullName);

                if (1 != con.ExecuteNonQuery(s, cmdParams))
                {
                    return ctx.Error(ErrorCode.DbCommandFailed, "Failed to insert title record");
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Deletes a title record.  Note that this does NOT drop the title's DB;
        //  that is deemed too dangerous to do except manually.
        //PARAMS
        //  con           - Open connection using DB that table resides in.
        //  titleId       - ID of title to delete.
        //RETURNS
        //  On success, returns null.  On failure, returns Error instance.
        //NOTES
        //  There may be many references to this title ID, prohibiting its removal.
        //  Those references need to be removed first by the user.
        public static Error DeleteTitle(SqlConnectionHelper con,
                                        int titleId)
        {
            Context ctx = GlobalDbContext.Create("name={0}", titleId); 

            try
            {
                if (titleId <= 0)
                {
                    return ctx.Error(ErrorCode.InvalidArguments);
                }

                string s = String.Format("DELETE FROM {0} WHERE TitleId=@TitleId", TABLE_NAME);

                SqlCmdParams cmdParams = new SqlCmdParams();
                cmdParams.Add("@TitleId", titleId);

                if (con.ExecuteNonQuery(s, cmdParams) != 1)
                {
                    return ctx.Error(ErrorCode.DbCommandFailed);
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }
/*
        //PURPOSE
        //  Updates a title record.
        //PARAMS
        //  con             - Open connection using DB that table resides in.
        //  titleId         - ID of title to update
        //  name            - New name for title
        //  platformId      - New platform ID
        //  connectionStr   - New connection string
        //  dbName          - New DB name
        //RETURNS
        //  Returns null on success, Error on failure.
        public static Error UpdateTitle(SqlConnectionHelper con,
                                        int titleId,
                                        string name,
                                        int platformId,
                                        string connectionStr,
                                        string dbName)
        {
            if (titleId == 0
                || name.Length == 0
                || platformId == 0
                || connectionStr.Length == 0
                || dbName.Length == 0)
            {
                return new Error("Title.UpdateTitle: Invalid arguments");
            }

            try
            {
                string s = String.Format("UPDATE {0} SET Name='{1}', PlatformId={2}, ConnectionStr='{3}', DbName='{4}' WHERE TitleId={5}",
                                         TABLE_NAME,
                                         name.Replace("'", "''"),
                                         platformId,
                                         connectionStr,
                                         dbName,
                                         titleId);

                if (con.AdHocExecuteNonQuery(s) != 1)
                {
                    return new Error("Title.UpdateTitle: UPDATE failed");
                }

                Log.WriteLine("Title.UpdateTitle: Updated title {0}", titleId);

                return null;
            }
            catch (Exception ex)
            {
                return new Error(ex);
            }
        }
 */

        //PURPOSE
        //  Retrieves TitleInfo from the cache, adding it if not found.
        //RETURNS
        //  Like all GetCachedXXX() methods, it is assumed that this will 
        //  succeed and return a non-null instance.  
        //  Errors are communicated as exceptions, to make the calling code
        //  (almost always a web service) less verbose.
        public static TitleInfo GetCachedTitleInfo(int titleId)
        {
            TitleInfo info = CacheHelper<TitleInfo>.Get(string.Format("GlobalDb_TitleInfo_{0}", titleId),
                                                        CreateTitleInfoDelegate,
                                                        titleId);

            if (info == null)
            {
                throw (new Exception(string.Format("Failed to get title {0} from cache", titleId)));
            }

            return info;
        }

        //PURPOSE
        //  Object creation callback for GetCachedTitleInfo().
        public static void CreateTitleInfoDelegate(object userdata,
                                                   out TitleInfo obj,
                                                   out DateTime absoluteExpirationTime)
        {
            obj = null;
            absoluteExpirationTime = DateTime.Now;

            using (SqlConnectionHelper con = GlobalDbHelper.Open())
            {
                List<TitleInfo> titleInfos = null;

                Title.QueryTitle(con, (int)userdata, 1, out titleInfos);

                if (titleInfos.Count > 0)
                {
                    obj = titleInfos[0];
                    absoluteExpirationTime = DateTime.Now.AddMinutes(CACHE_ABSOLUTE_EXPIRATION_MINUTES);
                }
            }
        }
    }
}
