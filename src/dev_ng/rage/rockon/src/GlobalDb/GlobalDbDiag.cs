using System;
using Rockstar.Rockon.Diag;

namespace Rockstar.Rockon.GlobalDb
{
    public class GlobalDbChannel
    {
        private static Channel m_Channel = new Channel("GlobalDb");

        public static Channel Channel
        {
            get { return m_Channel; }
        }
    }

    public class GlobalDbContext
    {
        public static Context Create()
        {
            return new Context(GlobalDbChannel.Channel);
        }

        public static Context Create(string desc)
        {
            return new Context(GlobalDbChannel.Channel, desc);
        }

        public static Context Create(string format, params object[] args)
        {
            return new Context(GlobalDbChannel.Channel, format, args);
        }
    }
}
