using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.GlobalDb
{
    //IMPORTANT: The values in the GlobalDb Platforms table must match this enum.
    public enum PlatformId
    {
        Invalid = -1,

        Xbox360 = 1,
        PS3,
        PC_GFWL,
        PSP,
        DS,
        Wii,
    }
}
