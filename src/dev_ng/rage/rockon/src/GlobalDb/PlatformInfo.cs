using System;

namespace Rockstar.Rockon.GlobalDb
{
    //PURPOSE
    //  Describes an entry in the Platforms table of the global DB.
    public class PlatformInfo
    {
        public int Id;                      //Well-known ID
        public string InternalName;         //Short internal name (ex. xbox360)
        public string FullName;             //Full name (ex. Nintendo DS)
        public bool IsSecure;               //False if SG must secure comm with clients
        public DateTime CreateDateTime;     //Time entry was created
    }
}
