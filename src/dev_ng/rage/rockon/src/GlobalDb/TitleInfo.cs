using System;

namespace Rockstar.Rockon.GlobalDb
{
    //PURPOSE
    //  Describes an entry in the Titles table of the global DB.
    public class TitleInfo
    {
        public int Id;                      //Well-known ID
        public int PlatformId;              //Platform title is on
        public string InternalName;         //Internal name of title (ex. rdr2_xbox360)
        public string FullName;             //Full name of title (ex. "Red Dead Redemption")
        public DateTime CreateDateTime;     //Time entry was created
    }
}
