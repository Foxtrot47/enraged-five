﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;

using Rockstar.Rockon.Diag;

namespace Rockstar.Rockon.Net
{
    public class TextMsgReader : IMsgReader
    {
        private StringBuilder m_Sb;
        private string[] m_MsgTerminators = { "\r\n", "\n\r" };

        public TextMsgReader(string[] msgTerminators)
        {
            m_MsgTerminators = msgTerminators;
            Reset();
        }

        //PURPOSE
        //  Takes data, and stores each byte until the end of a text message
        //  is found, at which time it parses the message and returns it, leaving
        //  bytes past that unprocessed.
        //PARAMS
        //  src                 - Buffer to read from
        //  offset              - Offset to begin reading from
        //  size                - Number of bytes to read from offset
        //  bytesProcessed(out) - Number of bytes actually processed. If the 
        //                        end of a msg was not found this will equal size.
        //                        If the end of a msg was found, though, it may be
        //                        less, as processing stops at msg boundaries.
        //  msg(out)            - Text msg parsed from the data; null if end of
        //                        msg hasn't been reached yet.
        //RETURNS
        //  True on success, false if error.  If error, the caller should assume
        //  the stream is unreadable, and should disconnect.
        public Error Read(byte[] src,
                          int offset,
                          int size,
                          out int bytesProcessed,
                          out object msg)
        {
            Context ctx = NetContext.Create("offset={0}, size={1}", offset, size);
            bytesProcessed = 0;
            msg = null;

            try
            {
                while (bytesProcessed < size)
                {
                    char c = (char)src[offset + bytesProcessed];
                    ++bytesProcessed;

                    m_Sb.Append(c);

                    string text = m_Sb.ToString();

                    foreach (string s in m_MsgTerminators)
                    {
                        if (text.EndsWith(s))
                        {
                            text = text.Remove(text.LastIndexOf(s));                            
                            msg = text;
                            Reset();
                            break;
                        }
                    }
                }

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                Reset();
                return ctx.Error(ex);
            }
        }

        private void Reset()
        {
            m_Sb = new StringBuilder();
        }
    }

    public class TextMsgWriter : IMsgWriter
    {
        private string[] m_MsgTerminators = { "\r\n", "\n\r" };

        public TextMsgWriter(string[] msgTerminators)
        {
            m_MsgTerminators = msgTerminators;
        }

        public override void Begin(object msg)
        {
            string text = msg as string;

            bool found = false;

            foreach (string s in m_MsgTerminators)
            {
                if (text.EndsWith(s))
                {
                    found = true;
                    break;
                }
            }

            if(!found)
            {
                text += m_MsgTerminators[0];
            }

            byte[] bytes = Encoding.UTF8.GetBytes(text);

            base.Begin(bytes, 0, bytes.Length);
        }
    }
}
