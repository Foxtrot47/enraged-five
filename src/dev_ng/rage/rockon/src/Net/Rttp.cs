﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Xml;

using Rockstar.Rockon.Diag;

/*
    RTTP is Rockstar Text Transport Protocol.  RTTP message content is
    text, but the header is binary, so technically it's not a 100%
    text protocol.

    RTTP message content is expected to be in RSON format.

    Each RTTP request has a unique transaction id.  When a response is
    received it is matched up with the request based on the transaction id.
    This allows multiple requests to be outstanding.

    RTTP can not only be used for request/response, but also for "push".
    Pushed messages are unsolicited messages, i.e. they are not triggered
    by a request.  Push messages have a transaction id of zero.

    RTTP Format
    ===========

     --------------------------------------------------
    |76543210|76543210765432107654321076543210|76543210|
    |  Mixer |           Signature            | Flags  |
     --------------------------------------------------
    |7654321076543210|765432107654321076543210|76543210....
    | Transaction Id |     Content Size       | Content
     --------------------------------------------------

    Mixer:
    The mixer is an 8-bit value, randomly selected for every message,
    that is used to "mix up" all bytes after the mixer by xoring them
    with the mixer.

    Upon receipt all bytes after the mixer are unmixed by xoring them
    with the mixer.

    Signature:
    32-bit value that identifies the message as an RTTP message.  The
    signature will change when the version changes.

    Flag Bits:
    0   :   1 if content is compressed

    Transaction Id:
    16-bit value that uniquely identifies the message.

    Messages with a non-zero transaction id are considered "request" messages
    and require a response.

    Messages with a transaction id of zero are considered "push" messages
    and require no response.

    Content Size:
    24-bit value indicating the *uncompressed* size of the content.

    Content:
    Message content.  Deflate (RFC 1951) is used for compression.
*/

namespace Rockstar.Rockon.Net
{
    //Determines whether we handle text-only rttp messages, i.e. those
    //with no header. This should be set to true only for debugging.
    struct TextOnly
    {
        //public const bool Value     = true;
        public const bool Value     = false;
    }

    public enum RttpVersion
    {
        Version0 = 0,         //Original version
        Version1,
        Version2,
        NumVersions,

        Invalid = 255,
        Current = Version2
    }

    //Signature used in the RTTP header to recognize RTTP messages.
    public struct RttpSignature
    {
        //A simple hash from "The Art Of Computer Programming" to create the
        //signature and make it unique for the current version.
        static readonly uint SEED = 5381;
        static readonly uint SIG0 = ((SEED << 5) ^ (SEED >> 27)) ^ (uint) RttpVersion.Current;
        static readonly uint SIG1 = ((SIG0 << 5) ^ (SIG0 >> 27)) ^ 'R';
        static readonly uint SIG2 = ((SIG1 << 5) ^ (SIG1 >> 27)) ^ 'T';
        static readonly uint SIG3 = ((SIG2 << 5) ^ (SIG2 >> 27)) ^ 'T';

        public static readonly uint Value = ((SIG3 << 5) ^ (SIG3 >> 27)) ^ 'P';
    }

    //PURPOSE
    //  Transaction ID. A valid txid means an RTTP message is part of
    //  a request-reply transaction.  An invalid one means it is just
    //  a message.
    public struct RttpTxId
    {
        public static readonly UInt16 Invalid   = 0;
    }

    //PURPOSE
    //  Flags in the RTTP header.
    public struct RttpFlags
    {
        public static readonly byte Compressed  = 1 << 0;   //Content is compressed
    }

    //PURPOSE
    //  Describes the header for an RTTP message.
    public class RttpHeader
    {
        static object sm_RngLock = new object();
        static Random sm_Rng = new Random((int) DateTime.Today.Ticks);

        public RttpVersion Version {get; private set;}
        public uint ContentSize {get; private set;}
        public UInt16 TxId {get; private set;}
        public byte Mixer {get; private set;}
        public byte Flags {get; private set;}

        public static readonly int SIZEOF_HEADER    = 11;

        public RttpHeader()
        {
            Version = RttpVersion.Invalid;
            ContentSize = 0;
            TxId = RttpTxId.Invalid;
            Mixer = 0;
            Flags = 0;
        }

        public void ResetForKeepAlive()
        {
            Init(RttpVersion.Current, 0, RttpTxId.Invalid, RttpFlags.Compressed);
        }

        public void ResetForPush(int contentSize, byte flags)
        {
            Init(RttpVersion.Current, contentSize, RttpTxId.Invalid, flags);
        }

        public void ResetForTransaction(int contentSize, UInt16 txId, byte flags)
        {
            Init(RttpVersion.Current, contentSize, txId, flags);
        }

        public bool IsCompressed
        {
            get
            {
                return (Flags & RttpFlags.Compressed) != 0;
            }
        }

        public bool IsTransaction
        {
            get
            {
                return RttpTxId.Invalid != TxId;
            }
        }

        public void Write(MemoryStream ms)
        {
            long pos = ms.Position;

            ms.WriteByte(Mixer);
            ms.WriteByte((byte) ((RttpSignature.Value >> 24) & 0xFF));
            ms.WriteByte((byte) ((RttpSignature.Value >> 16) & 0xFF));
            ms.WriteByte((byte) ((RttpSignature.Value >> 8) & 0xFF));
            ms.WriteByte((byte) ((RttpSignature.Value >> 0) & 0xFF));
            ms.WriteByte((byte) Flags);
            ms.WriteByte((byte) ((TxId >> 8) & 0xFF));
            ms.WriteByte((byte) ((TxId >> 0) & 0xFF));
            ms.WriteByte((byte) ((ContentSize >> 16) & 0xFF));
            ms.WriteByte((byte) ((ContentSize >> 8) & 0xFF));
            ms.WriteByte((byte) ((ContentSize >> 0) & 0xFF));

            System.Diagnostics.Debug.Assert(ms.Position - pos == SIZEOF_HEADER);
        }
        
        private void Init(RttpVersion version, int contentSize, UInt16 txId, byte flags)
        {
            Version = version;
            ContentSize = (uint)contentSize;
            TxId = txId;
            Flags = flags;
            lock(sm_RngLock)
            {
                Mixer = (byte) sm_Rng.Next();
            }
        }
    }

    //PURPOSE
    //  Encapsulates a RTTP message, which can be a request, response,
    //  or "pushed" message.  Pushed messages are messages sent without
    //  a triggering request.
    public class RttpMessage
    {
        //If true, the message is plain text, and the header should be ignored.
        public bool IsPlainText { get; private set; }

        //Content bytes.  This will be null if Header.ContentSize is zero.
        public byte[] Content { get; private set; }

        public UInt16 TxId { get; private set; }

        public RttpMessage()
        {
            IsPlainText = true;
            Content = null;
        }

        public void ResetForPlainText(string text)
        {
            IsPlainText = true;
            Content = Encoding.UTF8.GetBytes(text);
        }

        public void ResetForKeepAlive()
        {
            IsPlainText = false;
            Content = null;
        }

        public void ResetForPush(byte[] content)
        {
            IsPlainText = false;
            Content = content;
        }

        public void ResetForTransaction(byte[] content, UInt16 txId)
        {
            IsPlainText = false;
            TxId = txId;
            Content = content;
        }

        public int GetSize()
        {
            int size = 0;

            if (!IsPlainText)       size += RttpHeader.SIZEOF_HEADER;
            if (Content != null)    size += Content.Length;
            
            return size;
        }

        public string GetRsonString()
        {
            if (Content != null)
            {
                return Encoding.UTF8.GetString(Content);
            }

            return null;
        }

        public void Write(MemoryStream ms)
        {
            if(IsPlainText)
            {
                if(Content != null)
                {
                    ms.Write(Content, 0, Content.Length);
                }
            }
            else
            {
                RttpHeader header = new RttpHeader();
                byte flags = RttpFlags.Compressed;

                if(null == Content)
                {
                    header.ResetForKeepAlive();
                }
                else if(RttpTxId.Invalid == TxId)
                {
                    header.ResetForPush(Content.Length, flags);
                }
                else
                {
                    header.ResetForTransaction(Content.Length, TxId, flags);
                }

                header.Write(ms);
            
                if(Content != null)
                {
                    long pos = ms.Position;
                    DeflateStream ds = new DeflateStream(ms, CompressionMode.Compress, true);
                    ds.Write(Content, 0, Content.Length);
                    ds.Close();

                    if(ms.Length - pos > Content.Length)
                    {
                        //Compressed data is larger than raw data.
                        //Just send the raw data.

                        //Re-write the header with the compressed but cleared.
                        ms.Position = 0;
                        flags &= (byte) ~RttpFlags.Compressed;

                        if(RttpTxId.Invalid == TxId)
                        {
                            header.ResetForPush(Content.Length, flags);
                        }
                        else
                        {
                            header.ResetForTransaction(Content.Length, TxId, flags);
                        }

                        header.Write(ms);

                        ms.Write(Content, 0, Content.Length);
                    }
                }

                ms.SetLength(ms.Position);

                //Mix up the bytes.  Start at index 1 so as not to mix
                //the mixer.
                byte[] bytes = ms.GetBuffer();
                for(int i = 1; i < bytes.Length; ++i)
                {
                    bytes[i] ^= header.Mixer;
                }
            }
        }
    }

    //PURPOSE
    //  Reads RTTP messages from a stream of bytes.  Like all IMsgReaders,
    //  each call to Read() processes bytes up to the end of a message, 
    //  at which point it returns with a msg object (RsonReader in this case).
    //
    //  Because TCP messages can be spread over multiple receive calls,
    //  it is possible that a msg object won't be returned for multiple
    //  Read() calls.
    public class RttpMsgReader : IMsgReader
    {
        private enum State
        {
            STATE_AWAITING_MIXER,
            STATE_AWAITING_SIGNATURE,
            STATE_AWAITING_FLAGS,
            STATE_AWAITING_TXID,
            STATE_AWAITING_SIZE,
            STATE_RECEIVING_CONTENT
        }

        State m_State = State.STATE_AWAITING_MIXER;

        bool m_IsPlainText = false;
        int m_OpenBraces;

        uint m_Signature = 0;
        uint m_ContentSize;
        UInt16 m_TxId;
        byte m_Mixer;
        byte m_Flags;

        MemoryStream m_MemStrm, m_TmpMemStrm;
        DeflateStream m_DeflateStrm;
        byte[] m_ScratchBuf = new byte[256];
        uint m_BytesRcvd;

        public RttpMsgReader()
        {
            Reset();
        }

        //PURPOSE
        //  Takes data, and stores each byte until the end of an RSON message
        //  is found, at which time it parses the message and returns it, leaving
        //  bytes past that unprocessed.
        //PARAMS
        //  src                 - Buffer to read from
        //  offset              - Offset to begin reading from
        //  size                - Number of bytes to read from offset
        //  bytesProcessed(out) - Number of bytes actually processed. If the 
        //                        end of a msg was not found this will equal size.
        //                        If the end of a msg was found, though, it may be
        //                        less, as processing stops at msg boundaries.
        //  msg(out)            - RSON msg parsed from the data; null if end of
        //                        msg hasn't been reached yet.
        //RETURNS
        //  True on success, false if error.  If error, the caller should assume
        //  the stream is unreadable, and should disconnect.
        public Error Read(byte[] src,
                          int offset,
                          int size,
                          out int bytesProcessed,
                          out object msg)
        {
            Context ctx = NetContext.Create("offset={0}, size={1}", offset, size);
            bytesProcessed = 0;
            msg = null;

            try
            {
                while (bytesProcessed < size)
                {
                    switch(m_State)
                    {
                    case State.STATE_AWAITING_MIXER:
                        if(TextOnly.Value)
                        {
                            m_State = State.STATE_AWAITING_SIGNATURE;
                        }
                        else
                        {
                            m_Mixer = src[offset + bytesProcessed];
                            m_BytesRcvd = 0;
                            ++bytesProcessed;
                            m_State = State.STATE_AWAITING_SIGNATURE;
                        }
                        break;
                    case State.STATE_AWAITING_SIGNATURE:
                        {
                            //ctx.Debug2("STATE_AWAITING_SIGNATURE");
                            if (TextOnly.Value && '{' == src[offset + bytesProcessed])
                            {
                                m_IsPlainText = true;
                                m_MemStrm.Write(src, offset + bytesProcessed, 1);
                                m_OpenBraces = 1;

                                m_BytesRcvd = 0;
                                m_State = State.STATE_RECEIVING_CONTENT;
                            }
                            else
                            {
                                m_Signature = (m_Signature << 8) | (uint) (src[offset + bytesProcessed] ^ m_Mixer);
                                ++m_BytesRcvd;

                                if(4 == m_BytesRcvd)
                                {
                                    if(RttpSignature.Value == m_Signature)
                                    {
                                        m_BytesRcvd = 0;
                                        m_State = State.STATE_AWAITING_FLAGS;
                                    }
                                    else
                                    {
                                        ctx.Debug3("Invalid signature:" + m_Signature + " - expected: " + RttpSignature.Value);
                                        this.Reset();
                                    }
                                }
                            }

                            ++bytesProcessed;
                        }
                        break;

                    case State.STATE_AWAITING_FLAGS:
                        {
                            //ctx.Debug2("STATE_AWAITING_FLAGS");

                            if (m_IsPlainText) throw new Exception("Plain text msg reading like binary");

                            m_Flags = (byte)(src[offset + bytesProcessed] ^ m_Mixer);

                            m_BytesRcvd = 0;
                            m_State = State.STATE_AWAITING_TXID;

                            ++bytesProcessed;
                        }
                        break;

                    case State.STATE_AWAITING_TXID:
                        {
                            //ctx.Debug2("STATE_AWAITING_TXID");

                            if (m_IsPlainText) throw new Exception("Plain text msg reading like binary");

                            m_TxId = (UInt16)((m_TxId << 8) | (byte) (src[offset + bytesProcessed] ^ m_Mixer));
                            ++m_BytesRcvd;
                            if (2 == m_BytesRcvd)
                            {
                                m_BytesRcvd = 0;
                                m_State = State.STATE_AWAITING_SIZE;
                            }

                            ++bytesProcessed;
                        }
                        break;

                    case State.STATE_AWAITING_SIZE:
                        {
                            //ctx.Debug2("STATE_AWAITING_SIZE");

                            if (m_IsPlainText) throw new Exception("Plain text msg reading like binary");

                            //Read up to the number of content bytes
                            m_ContentSize = (m_ContentSize << 8) | (byte) (src[offset + bytesProcessed] ^ m_Mixer);
                            ++m_BytesRcvd;

                            if (m_BytesRcvd == 3)
                            {
                                if(m_ContentSize > 0)
                                {
                                    if(0 != (m_Flags & RttpFlags.Compressed))
                                    {
                                        m_TmpMemStrm = new MemoryStream();
                                        m_DeflateStrm = new DeflateStream(m_TmpMemStrm, CompressionMode.Decompress, true);
                                    }

                                    m_BytesRcvd = 0;

                                    m_State = State.STATE_RECEIVING_CONTENT;
                                }
                                else
                                {
                                    ctx.Debug3("Received keepalive; discarding");
                                    Reset();
                                }
                            }

                            ++bytesProcessed;
                        }
                        break;

                    case State.STATE_RECEIVING_CONTENT:
                        {
                            //ctx.Debug2("STATE_RECEIVING_CONTENT");
                            bool makeMsg = false;

                            if (m_IsPlainText)
                            {
                                //Parse to find the end of the message.
                                //FIXME: This doesn't account for braces inside of quotes
                                m_MemStrm.Write(src, offset + bytesProcessed, 1);
                                if ('{' == src[offset + bytesProcessed])
                                {
                                    ++m_OpenBraces;
                                }
                                else if ('}' == src[offset + bytesProcessed])
                                {
                                    --m_OpenBraces;
                                    Debug.Assert(m_OpenBraces >= 0);

                                    if(0 == m_OpenBraces)
                                    {
                                        ++bytesProcessed; //To account for null?
                                        makeMsg = true;
                                    }
                                }

                                ++bytesProcessed;
                            }
                            else
                            {
                                //Unmix the content bytes.
                                int numUnMixedBytes = 0;

                                for(int i = bytesProcessed; i < size && numUnMixedBytes < m_ScratchBuf.Length; ++i, ++numUnMixedBytes)
                                {
                                    m_ScratchBuf[numUnMixedBytes] = (byte)(src[i] ^ m_Mixer);
                                }

                                if(0 != (m_Flags & RttpFlags.Compressed))
                                {
                                    m_TmpMemStrm.Position = 0;
                                    m_TmpMemStrm.Write(m_ScratchBuf, 0, numUnMixedBytes);
                                    m_TmpMemStrm.Position = 0;
                                    int numRead;
                                    while(0 != (numRead = m_DeflateStrm.Read(m_ScratchBuf, 0, m_ScratchBuf.Length)))
                                    {
                                        m_MemStrm.Write(m_ScratchBuf, 0, numRead);
                                        m_BytesRcvd += (uint) numRead;
                                        bytesProcessed += numRead;
                                    }
                                }
                                else
                                {
                                    int contentBytesRemaining = (int)(m_ContentSize - m_BytesRcvd);
                                    int numBytesToCopy = 0;

                                    if(contentBytesRemaining <= numUnMixedBytes)
                                    {
                                        numBytesToCopy = contentBytesRemaining;
                                    }
                                    else
                                    {
                                        numBytesToCopy = numUnMixedBytes;
                                    }

                                    m_MemStrm.Write(m_ScratchBuf, 0, numBytesToCopy);

                                    m_BytesRcvd += (uint)numBytesToCopy;
                                    
                                    bytesProcessed += numBytesToCopy;
                                }

                                Debug.Assert(m_BytesRcvd <= m_ContentSize);

                                makeMsg = (m_BytesRcvd == m_ContentSize);
                            }

                            if(makeMsg)
                            {
                                RttpMessage rttpMsg = new RttpMessage();

                                if (m_IsPlainText)
                                {
                                    rttpMsg.ResetForPlainText(Encoding.UTF8.GetString(m_MemStrm.ToArray()));
                                }
                                else if(m_ContentSize == 0)
                                {
                                    rttpMsg.ResetForKeepAlive();
                                }
                                else if (m_TxId == RttpTxId.Invalid)
                                {
                                    rttpMsg.ResetForPush(m_MemStrm.ToArray());
                                }
                                else
                                {
                                    rttpMsg.ResetForTransaction(m_MemStrm.ToArray(), m_TxId);
                                }

                                msg = rttpMsg;

                                //Reset for parsing next message from stream.
                                Reset();

                                return ctx.Success3();
                            }
                        }
                        break;
                    }
                }

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                //TODO: Should Reset() ourselves on error.
                bytesProcessed = 0;
                return ctx.Error(ex);
            }
        }

        private void Reset()
        {
            m_State = State.STATE_AWAITING_MIXER;
            m_IsPlainText = false;
            m_Signature = 0;
            m_ContentSize = 0;
            m_TxId = RttpTxId.Invalid;
            m_Mixer = 0;
            m_Flags = 0;
            if(null != m_MemStrm)
            {
                m_MemStrm.Dispose();
            }
            m_MemStrm = new MemoryStream();
            if(null != m_TmpMemStrm)
            {
                m_TmpMemStrm.Dispose();
            }
            m_TmpMemStrm = null;
            if(null != m_DeflateStrm)
            {
                m_DeflateStrm.Dispose();
            }
            m_DeflateStrm = null;
            m_BytesRcvd = 0;
            m_OpenBraces = 0;
        }
    }

    //PURPOSE
    //  Writes RTTP messages to a stream of bytes.
    public class RttpMsgWriter : IMsgWriter
    {
        //Generates the bytes that will be sent.
        public override void Begin(object msg)
        {
            RttpMessage rttpMsg = (RttpMessage)msg;

            MemoryStream ms = new MemoryStream(rttpMsg.GetSize());

            rttpMsg.Write(ms);
            
            base.Begin(ms.GetBuffer(), 0, (int)ms.Length);
        }
    }
}
