﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Rockstar.Rockon.Diag;

namespace Rockstar.Rockon.Net
{
    //TODO: Move message reading/writing to a combined MsgProtocol class?  Might help
    //      ensure reader/writer for each protocol versions stay together.
    public interface IMsgReader
    {
        //PURPOSE
        //  Reads messages from a stream of bytes. Each call to Read() processes 
        //  bytes up to the end of a message (determined by whatever protocol the
        //  reader is for), at which point it returns with a msg object.
        //
        //  Because TCP messages can be spread over multiple receive calls,
        //  it is possible that a msg object won't be returned for multiple
        //  Read() calls.
        //
        //PARAMS
        //  src                     - buffer to read from
        //  offset                  - offset into buffer to begin reading
        //  size                    - bytes to read
        //  numBytesRead (out)      - number of bytes read this call 
        //                            (can be less than size if msg produced)
        //  msg (out)               - protocol-specific msg object
        //
        //RETURNS
        //  Null on success; Error instance on error.
        Error Read(byte[] src,
                   int offset,
                   int size,
                   out int numBytesRead,
                   out object msg);
    };

    public abstract class IMsgWriter
    {
        byte[] m_Bytes = null;
        int m_NumLeft = 0;
        int m_Offset = 0;

        //Subclasses must override.  The overridden method must generate
        //a byte buffer and call the variant of Begin() that takes a
        //byte buffer as a parameter.
        public abstract void Begin(object msg);

        //Begins a write operation with the given bytes.
        //The Write() method will be called by the socket helper
        public void Begin(byte[] bytes, int offset, int size)
        {
            Debug.Assert(bytes.Length >= size);

            m_Bytes = bytes;
            m_NumLeft = size;
            m_Offset = offset;
        }

        //PURPOSE
        //  Writes messages to a stream of bytes. Each call to Write() writes 
        //  bytes to the destination buffer, up to the number of bytes passed to
        //  Begin().
        //
        //  Because TCP messages can be spread over multiple send calls,
        //  it is possible that multiple calls will be made to Write().
        //
        //PARAMS
        //  dst                     - buffer to write to
        //  offset                  - offset into buffer to begin writing
        //  size                    - bytes to write
        //  numBytesWritten (out)   - number of bytes written this call 
        //                            (can be less than size if the source
        //                             buffer is exhausted)
        internal Error Write(byte[] dst,
                             int offset,
                             int size,
                             out int numBytesWritten)
        {
            Context ctx = NetContext.Create();

            numBytesWritten = 0;

            if(m_NumLeft > 0)
            {
                numBytesWritten = size > m_NumLeft ? m_NumLeft : size;

                Buffer.BlockCopy(m_Bytes, m_Offset, dst, offset, numBytesWritten);
                m_NumLeft -= numBytesWritten;
                m_Offset += numBytesWritten;

                Debug.Assert(m_NumLeft >= 0);
                Debug.Assert(m_Offset <= m_Bytes.Length);
            }

            return ctx.Success3();
        }
    }

    //PURPOSE
    //  Aggregate stats for all SocketHelpers.
    public class SocketHelperStats
    {
        public static int NumConnectionsClosed;
        public static int NumInvalidSockets;
        public static int NumMsgReadErrors;
        public static int NumMsgWriteErrors;
        public static int NumOtherExceptions;
        public static int NumReceiveCallbacks;
        public static int NumReceives;
        public static int NumSendCallbacks;
        public static int NumSends;
        public static int NumSocketExceptions;
        public static int NumTimeouts;

        public static string ToString()
        {
            StringBuilder sb = new StringBuilder();
            int num = 0;

            sb.Append("Sh[");
            if (NumReceiveCallbacks > 0)
            {
                sb.Append("rcb=" + NumReceiveCallbacks);
                ++num;
            }
            if (NumReceives > 0)
            {
                if (num > 0) sb.Append(" ");
                sb.Append("r=" + NumReceives);
                ++num;
            }
            if (NumMsgReadErrors > 0)
            {
                if (num > 0) sb.Append(" ");
                sb.Append("mr=" + NumMsgReadErrors);
                ++num;
            }
            if (NumSendCallbacks > 0)
            {
                if (num > 0) sb.Append(" ");
                sb.Append("scb=" + NumSendCallbacks);
                ++num;
            }
            if (NumSends > 0)
            {
                if (num > 0) sb.Append(" ");
                sb.Append("s=" + NumSends);
                ++num;
            }
            if (NumMsgWriteErrors > 0)
            {
                if (num > 0) sb.Append(" ");
                sb.Append("mw=" + NumMsgWriteErrors);
                ++num;
            }
            if (NumInvalidSockets > 0)
            {
                if (num > 0) sb.Append(" ");
                sb.Append("inv=" + NumInvalidSockets);
                ++num;
            }
            if (NumOtherExceptions > 0)
            {
                if (num > 0) sb.Append(" ");
                sb.Append("oex=" + NumOtherExceptions);
                ++num;
            }
            if (NumSocketExceptions > 0)
            {
                if (num > 0) sb.Append(" ");
                sb.Append("sex=" + NumSocketExceptions);
                ++num;
            }
            if (NumConnectionsClosed > 0)
            {
                if (num > 0) sb.Append(" ");
                sb.Append("cl=" + NumConnectionsClosed);
                ++num;
            }
            if (NumTimeouts > 0)
            {
                if (num > 0) sb.Append(" ");
                sb.Append("to=" + NumTimeouts);
                ++num;
            }
            sb.Append("]");

            return sb.ToString();
        }
    }

    //PURPOSE
    //  Helper class for sending, receiving, and getting disconnect callbacks
    //  for an already opened stream (TCP) socket.  This manages queuing outgoing
    //  data, parsing incoming data for messages and delivering them to the user,
    //  and alerting user when connection has been closed.
    public class SocketHelper
    {
        public enum Reason
        {
            MsgReadingError,
            MsgWritingError,        
            OtherException,         //Non-socket exception
            RemoteHostDisconnected,
            SocketException,        //Will have an accompanying socket error
            Timeout,
            Unknown,
        };

        private readonly object m_Lock = new object();       
        private Socket m_Skt = null;
        private bool m_IsDead = true;
        private WaitCallback m_RcvdMsgDlgt = null;
        private WaitCallback m_ConClosedDlgt = null;
        private IMsgReader m_MsgReader = null;
        private IMsgWriter m_MsgWriter = null;
        private uint m_RcvTimeoutMs = 0;
        private byte[] m_RcvBuf = null;
        private byte[] m_SndBuf = null;
        private int m_NumBytesToSend = 0;
        private Timer m_RcvTimer;

        //Used to prevent race conditions with the timer timing
        //out simultaneously with receiving data.
        private DateTime m_LastRcvTime;

        const int MTU   = 1400;     //Maximum transmission unit

        private class OutgoingMsg
        {
            public byte[] Buf { get; private set; }
            public object Obj {get; private set;}
            public int Offset { get; private set; }
            public int Size { get; private set; }

            public OutgoingMsg(byte[] buf,
                               int offset,
                               int size)
            {
                Buf = buf;
                Obj = null;
                Offset = offset;
                Size = size;
            }

            public OutgoingMsg(object obj)
            {
                Buf = null;
                Obj = obj;
                Offset = 0;
                Size = 0;
            }
        }

        private List<OutgoingMsg> m_OutgoingMsgs = new List<OutgoingMsg>();

        private AsyncCallback m_ReceiveCallback;
        private AsyncCallback m_SendCallback;

        public Error Init(Socket s, 
                          WaitCallback rcvdDlgt,
                          WaitCallback closedDlgt,
                          IMsgReader msgReader,
                          IMsgWriter msgWriter,
                          uint timeoutMs)
        {
            lock (m_Lock)        
            {
                Context ctx = NetContext.Create();

                try
                {
                    Shutdown();

                    m_Skt = s;
                    m_RcvdMsgDlgt = rcvdDlgt;
                    m_ConClosedDlgt = closedDlgt;
                    m_MsgReader = msgReader;
                    m_MsgWriter = msgWriter;
                    m_RcvTimeoutMs = timeoutMs;

                    m_IsDead = false;
                    m_RcvBuf = new byte[MTU];
                    m_SndBuf = new byte[MTU];
                    m_NumBytesToSend = 0;

                    m_LastRcvTime = DateTime.Now;

                    SetReceiveTimeoutMs(m_RcvTimeoutMs);

                    m_ReceiveCallback = new AsyncCallback(ReceiveCallback);
                    m_SendCallback = new AsyncCallback(SendCallback);

                    try
                    {
                        m_Skt.BeginReceive(m_RcvBuf,
                           0,
                           m_RcvBuf.Length,
                           0,
                           m_ReceiveCallback,
                           null);
                    }
                    catch (SocketException ex)
                    {
                        Interlocked.Increment(ref SocketHelperStats.NumSocketExceptions);
                        Trace.WriteLine("BeginReceive SocketException: " + ex.SocketErrorCode);
                        return ctx.Error(ex.SocketErrorCode, "BeginReceive SocketException");
                    }
                    catch (Exception ex)
                    {
                        Interlocked.Increment(ref SocketHelperStats.NumOtherExceptions);
                        return ctx.Error(ex);
                    }

                    return ctx.Success3();
                }
                catch (Exception ex)
                {
                    return ctx.Error(ex);
                }
            }
        }

        public void Shutdown()
        {
            lock (m_Lock)
            {
                Context ctx = NetContext.Create();

                m_IsDead = true;

                this.CloseSocket();

                m_RcvdMsgDlgt = null;
                m_ConClosedDlgt = null;
                m_MsgReader = null;
                m_MsgWriter = null;
                m_RcvTimeoutMs = 0;
                m_RcvBuf = null;
                m_SndBuf = null;
                m_NumBytesToSend = 0;
                m_OutgoingMsgs.Clear();

                ctx.Success3();
            }
        }

        //PURPOSE
        //  Sets the timeout used when receiving messages.  If nothing is 
        //  received before this timeout, the socket will be closed.
        //  This can be changed at any time.
        public void SetReceiveTimeoutMs(uint ms)
        {
            lock (m_Lock)
            {
                m_RcvTimeoutMs = ms;

                if (m_RcvTimeoutMs == 0)
                {
                    //throw new ArgumentException("Timeout must be greater than zero");
                }
                else if (m_RcvTimer == null)
                {
                    TimerCallback timerCb = new TimerCallback(this.OnTimeout);
                    m_RcvTimer = new Timer(timerCb, this, m_RcvTimeoutMs, Timeout.Infinite);
                }
                else
                {
                    m_RcvTimer.Change(m_RcvTimeoutMs, Timeout.Infinite);
                }
            }
        }

        //PURPOSE
        //  Send a message object.
        public void Send(object msg)
        {
            lock (m_Lock)
            {
                try
                {
                    if (m_IsDead)
                    {
                        return;
                    }

                    if (m_NumBytesToSend > 0)
                    {
                        OutgoingMsg m = new OutgoingMsg(msg);
                        m_OutgoingMsgs.Add(m);
                    }
                    else
                    {
                        m_NumBytesToSend = 0;
                        m_MsgWriter.Begin(msg);

                        Error err;
                        if (null == (err = m_MsgWriter.Write(m_SndBuf, 0, m_SndBuf.Length, out m_NumBytesToSend)))
                        {
                            try
                            {
                                m_Skt.BeginSend(m_SndBuf,
                                                0,
                                                m_NumBytesToSend,
                                                0,
                                                m_SendCallback,
                                                null);
                            }
                            catch (SocketException ex)
                            {
                                Interlocked.Increment(ref SocketHelperStats.NumSocketExceptions);
                                Trace.WriteLine("BeginSend SocketException: " + ex.SocketErrorCode);
                                Die(Reason.SocketException);
                            }
                            catch (Exception ex)
                            {
                                Interlocked.Increment(ref SocketHelperStats.NumOtherExceptions);
                                Trace.WriteLine(ex);
                                Die(Reason.SocketException);
                            }
                        }
                        else
                        {
                            Trace.WriteLine(err);
                            Die(Reason.MsgWritingError);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Interlocked.Increment(ref SocketHelperStats.NumOtherExceptions);
                    Context ctx = NetContext.Create();
                    ctx.Error(ex);
                    Die(Reason.OtherException);
                }
            }
        }

        //PURPOSE
        //  Send a buffer.
        private void Send(byte[] buf, int offset, int size)
        {
            lock (m_Lock)
            {
                try
                {
                    if (m_IsDead)
                    {
                        return;
                    }

                    if(m_NumBytesToSend > 0)
                    {
                        OutgoingMsg m = new OutgoingMsg(buf, offset, size);
                        m_OutgoingMsgs.Add(m);
                    }
                    else
                    {
                        m_NumBytesToSend = 0;
                        m_MsgWriter.Begin(buf, offset, size);

                        Error err;
                        if (null == (err = m_MsgWriter.Write(m_SndBuf, 0, m_SndBuf.Length, out m_NumBytesToSend)))
                        {
                            try
                            {
                                m_Skt.BeginSend(m_SndBuf,
                                                0,
                                                m_NumBytesToSend,
                                                0,
                                                m_SendCallback,
                                                null);
                            }
                            catch (SocketException ex)
                            {
                                Interlocked.Increment(ref SocketHelperStats.NumSocketExceptions);
                                Trace.WriteLine("BeginSend SocketException: " + ex.SocketErrorCode);
                                Die(Reason.SocketException);
                            }
                            catch (Exception ex)
                            {
                                Interlocked.Increment(ref SocketHelperStats.NumOtherExceptions);
                                Trace.WriteLine(ex);
                                Die(Reason.SocketException);
                            }
                        }
                        else
                        {
                            Trace.WriteLine(err);
                            Die(Reason.MsgWritingError);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Interlocked.Increment(ref SocketHelperStats.NumOtherExceptions);
                    Context ctx = NetContext.Create();
                    ctx.Error(ex);
                    Die(Reason.OtherException);
                }
            }
        }

        //PURPOSE
        //  Called when an error has occurred rendering the socket unusable.
        private void Die(Reason reason)
        {
            lock (m_Lock)
            {
                if (m_IsDead)
                {
                    throw (new Exception("SocketHelper.Die: Already dead"));
                }

                m_IsDead = true;

                this.CloseSocket();

                ThreadPool.QueueUserWorkItem(m_ConClosedDlgt, reason);
            }
        }

        //PURPOSE
        //  Closes the socket.  The socket will not be usable until Init() is called again.
        private void CloseSocket()
        {
            lock (m_Lock)
            {
                try
                {
                    Debug.Assert(this.m_IsDead);

                    if (m_Skt != null)
                    {
                        if (m_Skt.Connected)
                        {
                            m_Skt.Shutdown(SocketShutdown.Both);
                        }
                        m_Skt.Close();
                        m_Skt = null;
                    }

                    if (null != m_RcvTimer)
                    {
                        m_RcvTimer.Dispose();
                        m_RcvTimer = null;
                    }
                }
                catch (SocketException ex)
                {
                    Interlocked.Increment(ref SocketHelperStats.NumSocketExceptions);
                    Trace.WriteLine("Shutdown/Close SocketException: " + ex.SocketErrorCode);
                }
                catch (Exception ex)
                {
                    Interlocked.Increment(ref SocketHelperStats.NumOtherExceptions);
                    Trace.WriteLine(ex);
                }
            }
        }

        //PURPOSE
        //  Called when the receive timeout expires.
        private void OnTimeout(object state)
        {
            lock (m_Lock)
            {
                Context ctx = NetContext.Create();

                //If the timeout callback blocked on m_Lock because
                //we were in the process of receiving (i.e. a race condition
                //occurred between receiving and timing out) then by
                //checking the last receive time we can avoid incorrectly
                //timing out.
                if (!m_IsDead)
                {
                    TimeSpan ts = DateTime.Now - m_LastRcvTime;

                    if (ts.TotalMilliseconds >= m_RcvTimeoutMs)
                    {
                        //ctx.Warning("Dying from timeout ({0}ms total, {1}ms timeout)", ts.TotalMilliseconds, m_RcvTimeoutMs);
                        Interlocked.Increment(ref SocketHelperStats.NumTimeouts);
                        this.Die(Reason.Timeout);
                    }
                    else
                    {
                        ctx.Warning("Restarting timeout timer ({0}ms total, {1}ms timeout)", ts.TotalMilliseconds, m_RcvTimeoutMs);
                        SetReceiveTimeoutMs(m_RcvTimeoutMs);
                    }
                }
                else
                {
                    ctx.Warning("Timeout when dead, ignoring");
                }
            }
        }

        //PURPOSE
        //  Called when async send completes.
        //FIXME: I kinda wonder about using the send buffer, and the copies it (may) require,
        //       when we could have direct access to the msg's buffer.
        //       Hmm, but what about cases where truly huge things are being streamed in by the writer?
        //       Those could possibly be handled by overriding the properties of the class?
        private void SendCallback(IAsyncResult ar)
        {
            lock (m_Lock)
            {
                Interlocked.Increment(ref SocketHelperStats.NumSendCallbacks);
                
                Context ctx = new Context();

                try
                {
                    if (m_IsDead)
                    {
                        return;
                    }

                    if (m_Skt == null)
                    {
                        Interlocked.Increment(ref SocketHelperStats.NumInvalidSockets);
                        Trace.WriteLine("Null socket");
                        return;
                    }

                    int numBytes;

                    try
                    {
                        numBytes = m_Skt.EndSend(ar);
                        Interlocked.Increment(ref SocketHelperStats.NumSends);
                    }
                    catch (SocketException ex)
                    {
                        Interlocked.Increment(ref SocketHelperStats.NumSocketExceptions);
                        Trace.WriteLine(string.Format("EndSend SocketException: {0} ({1})", ex.SocketErrorCode, (int)ex.SocketErrorCode));
                        Die(Reason.SocketException);
                        return;
                    }
                    catch (Exception ex)
                    {
                        Interlocked.Increment(ref SocketHelperStats.NumOtherExceptions);
                        Trace.WriteLine(ex);
                        Die(Reason.OtherException);
                        return;
                    }

                    if (numBytes <= 0)
                    {
                        throw new Exception("Sent " + numBytes + " bytes; shouldn't be possible");
                    }

                    //ctx.Debug3("SendCallback: Sent {0} bytes", numBytes);

                    m_NumBytesToSend -= numBytes;
                    Debug.Assert(m_NumBytesToSend >= 0);

                    if (m_NumBytesToSend > 0)
                    {
                        //Backfill the send buffer, eliminating bytes that have already been sent.
                        Buffer.BlockCopy(m_SndBuf, numBytes, m_SndBuf, 0, m_NumBytesToSend);
                    }

                    int numWritten = 0;
                    if (null == m_MsgWriter.Write(m_SndBuf, m_NumBytesToSend, (m_SndBuf.Length - m_NumBytesToSend), out numWritten))
                    {
                        m_NumBytesToSend += numWritten;
                    }

                    if (0 == m_NumBytesToSend)
                    {
                        //NetChannel.Channel.Debug3("SendCallback: Done sending message");

                        if (m_OutgoingMsgs.Count > 0)
                        {
                            OutgoingMsg m = m_OutgoingMsgs[0];
                            m_OutgoingMsgs.Remove(m);

                            if (null != m.Obj)
                            {
                                m_MsgWriter.Begin(m.Obj);
                            }
                            else
                            {
                                m_MsgWriter.Begin(m.Buf, m.Offset, m.Size);
                            }

                            numWritten = 0;
                            Error err;
                            if (null == (err = m_MsgWriter.Write(m_SndBuf, 0, m_SndBuf.Length, out numWritten)))
                            {
                                m_NumBytesToSend = numWritten;
                            }
                            else
                            {
                                Interlocked.Increment(ref SocketHelperStats.NumMsgWriteErrors);
                                Trace.WriteLine(err);
                            }
                        }
                    }

                    if (m_NumBytesToSend > 0)
                    {
                        try
                        {
                            m_Skt.BeginSend(m_SndBuf,
                                            0,
                                            m_NumBytesToSend,
                                            0,
                                            m_SendCallback,
                                            null);
                        }
                        catch (SocketException ex)
                        {
                            Interlocked.Increment(ref SocketHelperStats.NumSocketExceptions);
                            Trace.WriteLine(string.Format("BeginSend SocketException: {0} ({1})", ex.SocketErrorCode, (int)ex.SocketErrorCode));
                            Die(Reason.SocketException);
                        }
                        catch (Exception ex)
                        {
                            Interlocked.Increment(ref SocketHelperStats.NumSocketExceptions);
                            Trace.WriteLine(ex);
                            Die(Reason.OtherException);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //ctx.Error(ex);
                    Trace.WriteLine(ex);
                    Die(Reason.OtherException);
                }
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            lock (m_Lock)
            {
                Interlocked.Increment(ref SocketHelperStats.NumReceiveCallbacks);

                Context ctx = NetContext.Create();

                try
                {
                    if (m_IsDead)
                    {
                        return;
                    }

                    if (m_Skt == null)
                    {
                        Interlocked.Increment(ref SocketHelperStats.NumInvalidSockets);
                        Trace.WriteLine("Null socket");
                        return;
                    }

                    int numBytes = 0;

                    try
                    {
                        numBytes = m_Skt.EndReceive(ar);
                        Interlocked.Increment(ref SocketHelperStats.NumReceives);
                    }
                    catch (SocketException ex)
                    {
                        Interlocked.Increment(ref SocketHelperStats.NumSocketExceptions);

                        if (ex.SocketErrorCode == SocketError.ConnectionReset)
                        {
                            Interlocked.Increment(ref SocketHelperStats.NumConnectionsClosed);
                        }
                        else
                        {
                            Trace.WriteLine(string.Format("EndReceive SocketException: {0} ({1})", ex.SocketErrorCode, (int)ex.SocketErrorCode));
                        }

                        Die(Reason.SocketException);
                        return;
                    }
                    catch (Exception ex)
                    {
                        Interlocked.Increment(ref SocketHelperStats.NumOtherExceptions);
                        Trace.WriteLine(ex);

                        Die(Reason.Unknown);
                        return;
                    }

                    if (numBytes > 0)
                    {
                        //ctx.Debug3("Recv {0} bytes from {1}", numBytes, m_Skt.RemoteEndPoint);

                        //Keep reading until the message reader stops
                        //producing message objects.  At that point we should
                        //have read all the bytes in the receive buffer.
                        int offset = 0;

                        while (offset < numBytes)
                        {
                            int bytesRead;
                            object msg;
                            Error err = m_MsgReader.Read(m_RcvBuf,
                                                         offset,
                                                         (numBytes - offset),
                                                         out bytesRead,
                                                         out msg);

                            if (err != null)
                            {
                                ctx.Error(err);
                                Interlocked.Increment(ref SocketHelperStats.NumMsgReadErrors);
                                Die(Reason.MsgReadingError);
                                return;
                            }

                            offset += bytesRead;

                            if (msg != null)
                            {
                                ThreadPool.QueueUserWorkItem(m_RcvdMsgDlgt, msg);
                            }
                        }

                        //Reset receive timeout.
                        m_LastRcvTime = DateTime.Now;
                        SetReceiveTimeoutMs(m_RcvTimeoutMs);

                        //Start the next receive.
                        try
                        {
                            m_Skt.BeginReceive(m_RcvBuf,
                                               0,
                                               m_RcvBuf.Length,
                                               0,
                                               m_ReceiveCallback,
                                               ar.AsyncState);
                        }
                        catch (SocketException ex)
                        {
                            Interlocked.Increment(ref SocketHelperStats.NumSocketExceptions);
                            Trace.WriteLine(string.Format("BeginReceive SocketException: {0} ({1})", ex.SocketErrorCode, (int)ex.SocketErrorCode));
                            Die(Reason.SocketException);
                        }
                        catch (Exception ex)
                        {
                            Interlocked.Increment(ref SocketHelperStats.NumSocketExceptions);
                            Trace.WriteLine(ex);
                            Die(Reason.OtherException);
                        }
                    }
                    else if (numBytes == 0)
                    {
                        //Normal disconnect.
                        Interlocked.Increment(ref SocketHelperStats.NumConnectionsClosed);
                        Die(Reason.RemoteHostDisconnected);
                    }
                    else
                    {
                        Die(Reason.Unknown);
                        throw new Exception("Negative numBytes: " + numBytes + "... should this even be possible?");
                    }
                }
                catch (Exception ex)
                {
                    Interlocked.Increment(ref SocketHelperStats.NumOtherExceptions);
                    Trace.WriteLine(ex);
                    Die(Reason.Unknown);
                }
            }
        }
    }
}
