using System;
using System.Xml;
using System.Xml.Serialization;

using Rockstar.Rockon.Diag;

namespace Rockstar.Rockon.Net
{
    public class XmlHelper
    {
        //PURPOSE
        //   Serializes an object, optionally without the XML declaration
        //   and without namespaces.
        public static Error Serialize(System.IO.TextWriter textWriter, 
                                      object obj,
                                      bool includeXmlDeclaration,
                                      bool includeNamespaces)
        {
            Context ctx = NetContext.Create();

            try
            {
                XmlWriterSettings xws = new XmlWriterSettings();
                xws.OmitXmlDeclaration = !includeXmlDeclaration;

                XmlWriter xw = XmlWriter.Create(textWriter, xws);

                XmlSerializer xs = new XmlSerializer(obj.GetType());

                if (includeNamespaces)
                {
                    xs.Serialize(xw, obj);
                }
                else
                {
                    XmlSerializerNamespaces xsns = new XmlSerializerNamespaces();
                    xsns.Add("", "");

                    xs.Serialize(xw, obj, xsns);
                }

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Writes an XML node and its children to an RSON document.
        public static bool WriteXmlToRson(XmlNode node, RsonWriter rw)
        {
            if (node != null)
            {
                //For nodes that only contain text, we want to write them as name=innertext,
                //so we have to do this bit if hackery.
                XmlNode firstChild = node.FirstChild;

                if (firstChild != null && firstChild.NodeType == XmlNodeType.Text)
                {
                    if (node.InnerText != null && node.InnerText.Length > 0)
                    {
                        if (!rw.WriteString(node.Name, node.InnerText))
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    //For nodes that contain actual elements, we want to write them
                    //as name={children}.
                    if (!rw.Begin(node.Name))
                    {
                        return false;
                    }

                    foreach (XmlNode child in node.ChildNodes)
                    {
                        if (!WriteXmlToRson(child, rw))
                        {
                            return false;
                        }
                    }

                    if (!rw.End())
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        //PURPOSE
        //  Finds a particular named node in an XML hierarchy.
        //FIXME
        //  I'm sure there's some built-in way to do this easily...
        public static XmlNode FindXmlNode(string name, XmlNode node)
        {
            if (node.Name == name)
            {
                return node;
            }

            foreach (XmlNode child in node.ChildNodes)
            {
                XmlNode n = FindXmlNode(name, child);

                if (n != null)
                {
                    return n;
                }
            }

            return null;
        }
    }
}
