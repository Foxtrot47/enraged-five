using System;
using Rockstar.Rockon.Diag;

namespace Rockstar.Rockon.Net
{
    public class NetChannel
    {
        private static Channel m_Channel = new Channel("Net");

        public static Channel Channel
        {
            get { return m_Channel; }
        }
    }

    public class NetContext
    {
        public static Context Create()
        {
            return new Context(NetChannel.Channel);
        }

        public static Context Create(string desc)
        {
            return new Context(NetChannel.Channel, desc);
        }

        public static Context Create(string format, params object[] args)
        {
            return new Context(NetChannel.Channel, format, args);
        }
    }
}
