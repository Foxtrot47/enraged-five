﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;

using Rockstar.Rockon.Diag;

namespace Rockstar.Rockon.Net
{
    //PURPOSE
    //  Holds stats about an HTTP requests.  Instances can be passed to 
    //  HttpWebRequestHelper methods to collect stats.
    public class HttpWebRequestStats
    {
        public uint RequestId;
        public double GetRequestStreamMs;
        public double WriteRequestMs;
        public double GetResponseMs;
        public double ReadResponseMs;

        private DateTime m_Dt;

        public void StartTimer()
        {
            m_Dt = DateTime.Now;
        }

        public double GetDeltaMs()
        {
            double deltaMs = DateTime.Now.Subtract(m_Dt).TotalMilliseconds;
            m_Dt = DateTime.Now;
            return deltaMs;
        }

        public double TotalMs
        {
            get
            {
                return GetRequestStreamMs + WriteRequestMs + GetResponseMs + ReadResponseMs;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (GetRequestStreamMs != 0)
            {
                sb.AppendFormat("GReq={0}  ", (int)GetRequestStreamMs);
            }
            if (WriteRequestMs != 0)
            {
                sb.AppendFormat("WReq={0}  ", (int)WriteRequestMs);
            }
            if (GetResponseMs != 0)
            {
                sb.AppendFormat("GRes={0}  ", (int)GetResponseMs);
            }
            if (ReadResponseMs != 0)
            {
                sb.AppendFormat("RRes={0}  ", (int)ReadResponseMs);
            }

            sb.AppendFormat("Total={0}", (int)TotalMs);

            return sb.ToString();
        }
    }

    //PURPOSE
    //  Helper class that automates the execution of requests sent via HTTP,
    //  such as POST and SOAP.
    //
    public class HttpWebRequestHelper
    {
        //PURPOSE
        //  Callbacks passed to the ExecuteXXX() methods.
        public delegate void ErrorDelegate(Error err, object userdata);
        public delegate void SuccessDelegate(string content, object userdata);

        //PURPOSE
        //  Synchronously executes an HTTP request, returning the response content.
        //  upon success.
        public static Error ExecuteHttpRequest(HttpWebRequest request,
                                               byte[] contentBytes,                                               
                                               out string responseContent,
                                               HttpWebRequestStats stats)
        {
            Context ctx = NetContext.Create();
            responseContent = null;

            try
            {
                Debug.Assert(request.ContentLength == contentBytes.Length);

                if (stats != null) stats.StartTimer();

                using (Stream s = request.GetRequestStream())
                {
                    if (s == null)
                    {
                        //return new Error(ErrorCode.UnknownError, "Failed to get request stream");
                        return ctx.Error(ErrorCode.UnknownError, "Failed to get request stream");
                    }

                    if (stats != null) stats.GetRequestStreamMs = stats.GetDeltaMs();

                    s.Write(contentBytes, 0, contentBytes.Length);
                    s.Flush();

                    if (stats != null) stats.WriteRequestMs = stats.GetDeltaMs();
                }

                using (HttpWebResponse resp = request.GetResponse() as HttpWebResponse)
                {
                    if (stats != null) stats.GetResponseMs = stats.GetDeltaMs();

                    //If there was an HTTP error, return an error reply to the client.
                    if (resp.StatusCode != HttpStatusCode.OK)
                    {
                        string errStr = string.Format("HttpStatusCode={0} '{1'}",
                                                      resp.StatusCode,
                                                      resp.StatusDescription);
                        //return new Error(ErrorCode.UnknownError, errStr);
                        return ctx.Error(ErrorCode.UnknownError, errStr);
                    }

                    //Read the response.
                    using (Stream s = resp.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(s))
                        {
                            responseContent = reader.ReadToEnd();
                            ctx.Debug3(responseContent);
                        }
                    }

                    if (stats != null) stats.ReadResponseMs = stats.GetDeltaMs();
                }

                //return null;
                return ctx.Success3();
            }
            catch (Exception ex)
            {
                //return new Error(ex);
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Synchronous SOAP request.
        public static Error ExecuteSoapRequest(string uri,
                                               SoapEnvelopeEx env,
                                               out string responseContent,
                                               HttpWebRequestStats stats)
        {
            //Context ctx = NetContext.Create();
            responseContent = null;

            try
            {
                byte[] contentBytes = Encoding.UTF8.GetBytes(env.Content);

                //Create an HTTP request to carry the SOAP content.
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Method = "POST";
                request.Accept = "text/xml";
                request.ContentType = "text/xml; charset=utf-8";
                request.ContentLength = contentBytes.Length;

                if (env.Version == SoapVersion.Soap1_1)
                {
                    request.Headers.Add(string.Format("SOAPAction: \"{0}{1}\"",
                                                      env.MethodNamespace, env.MethodName));
                }

                return ExecuteHttpRequest(request,
                                          contentBytes,                                          
                                          out responseContent,
                                          stats);
            }
            catch (Exception ex)
            {
                return  new Error(ex);
                //return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Begins executing an HttpWebRequest, calling back to user when finished.
        public static Error ExecuteHttpRequest(HttpWebRequest request,
                                               byte[] contentBytes,
                                               SuccessDelegate successDlgt,
                                               ErrorDelegate errorDlgt,
                                               object userdata,
                                               out uint requestId,
                                               HttpWebRequestStats stats)
        {
            Context ctx = NetContext.Create();
            requestId = 0;

            try
            {
                Debug.Assert(request.ContentLength == contentBytes.Length);

                AsyncRequestInfo requestInfo = new AsyncRequestInfo(request,
                                                                  contentBytes,
                                                                  successDlgt,
                                                                  errorDlgt,
                                                                  userdata,
                                                                  stats);

                requestId = requestInfo.Id;

                if (stats != null) stats.StartTimer();

                request.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), requestInfo);

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  Constructs an HTTP request from SOAP, and executes it.
        public static Error ExecuteSoapRequest(string uri,
                                               SoapEnvelopeEx env,
                                               SuccessDelegate successDlgt,
                                               ErrorDelegate errorDlgt,
                                               int timeoutMs,
                                               object userdata,
                                               out uint requestId,
                                               HttpWebRequestStats stats)
        {
            Context ctx = NetContext.Create();
            requestId = 0;

            try
            {
                byte[] contentBytes = Encoding.UTF8.GetBytes(env.Content);

                //Create an HTTP request to carry the SOAP content.
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Method = "POST";
                request.Accept = "text/xml";
                request.ContentType = "text/xml; charset=utf-8";
                request.ContentLength = contentBytes.Length;
                request.Timeout = timeoutMs;

                if (env.Version == SoapVersion.Soap1_1)
                {
                    request.Headers.Add(string.Format("SOAPAction: \"{0}{1}\"", 
                                                      env.MethodNamespace, env.MethodName));
                }

                return ExecuteHttpRequest(request, 
                                          contentBytes, 
                                          successDlgt, 
                                          errorDlgt, 
                                          userdata, 
                                          out requestId,
                                          stats);
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        //PURPOSE
        //  This is called after the HTTP request has successfully (or unsuccessfully) 
        //  connected to the remote host, and the HTTP request body can now be written.
        private static void GetRequestStreamCallback(IAsyncResult ar)
        {
            AsyncRequestInfo requestInfo = ar.AsyncState as AsyncRequestInfo;
            Context ctx = NetContext.Create(requestInfo.Id.ToString());

            try
            {
                //ctx.Debug3("Sending HTTP request content ({0} bytes)", requestInfo.Content.Length);

                if (requestInfo.Stats != null) requestInfo.Stats.GetRequestStreamMs = requestInfo.Stats.GetDeltaMs();

                using (Stream strm = requestInfo.HttpWebRequest.EndGetRequestStream(ar))
                {
                    strm.Write(requestInfo.Content, 0, requestInfo.Content.Length);
                    strm.Flush();
                }

                if (requestInfo.Stats != null) requestInfo.Stats.WriteRequestMs = requestInfo.Stats.GetDeltaMs();

                requestInfo.HttpWebRequest.BeginGetResponse(new AsyncCallback(GetResponseCallback), 
                                                            requestInfo);
            }
            catch (Exception ex)
            {
                Error err = ctx.Error(ex);

                if (requestInfo.ErrorCallback != null)
                {
                    requestInfo.ErrorCallback(err, requestInfo.UserData);
                }
            }
        }

        //PURPOSE
        //  This is called when the HTTP response is received.
        private static void GetResponseCallback(IAsyncResult ar)
        {
            AsyncRequestInfo requestInfo = ar.AsyncState as AsyncRequestInfo;
            Context ctx = NetContext.Create(requestInfo.Id.ToString());
            string content = null;

            try
            {
                if (requestInfo.Stats != null) requestInfo.Stats.GetResponseMs = requestInfo.Stats.GetDeltaMs();

                using(HttpWebResponse resp = requestInfo.HttpWebRequest.EndGetResponse(ar) as HttpWebResponse)
                {
                    //If there was an HTTP error, return an error reply to the client.
                    if (resp.StatusCode != HttpStatusCode.OK)
                    {
                        string errStr = string.Format("HttpStatusCode={0} '{1'}",
                                                      resp.StatusCode,
                                                      resp.StatusDescription);
                        ctx.Warning(errStr);

                        if (requestInfo.ErrorCallback != null)
                        {
                            requestInfo.ErrorCallback(new Error(ErrorCode.UnknownError, errStr), 
                                                      requestInfo.UserData);
                        }
                        return;
                    }

                    //Read the response.
                    using (Stream s = resp.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(s))
                        {
                            content = reader.ReadToEnd();
                            ctx.Debug3("Content={0}", content);
                        }
                    }
                }

                if (requestInfo.Stats != null) requestInfo.Stats.ReadResponseMs = requestInfo.Stats.GetDeltaMs();

                if (requestInfo.SuccessCallback != null)
                {
                    requestInfo.SuccessCallback(content, requestInfo.UserData);
                }
            }
            catch (Exception ex)
            {
                Error err = ctx.Error(ex);

                if (requestInfo.ErrorCallback != null)
                {
                    requestInfo.ErrorCallback(err, requestInfo.UserData);
                }
            }
        }

        //PURPOSE
        //  Helper class that maintains state of request being executed.
        private class AsyncRequestInfo
        {
            public HttpWebRequestStats Stats;

            public uint Id { get; private set; }
            public HttpWebRequest HttpWebRequest { get; private set; }
            public byte[] Content { get; private set; }
            public ErrorDelegate ErrorCallback { get; private set; }
            public SuccessDelegate SuccessCallback { get; private set; }
            public object UserData { get; private set; }

            public double RequestMs = 0;
            public double ResponseMs = 0;

            private static readonly object m_Lock = new object();
            private static uint m_NextRequestId = 1;
            private DateTime m_StartTime = DateTime.Now;

            public AsyncRequestInfo(HttpWebRequest httpRequest,
                               byte[] content,
                               SuccessDelegate successDlgt,
                               ErrorDelegate errorDlgt,
                               object userdata,
                               HttpWebRequestStats stats)
            {
                lock (m_Lock)
                {
                    Id = ++m_NextRequestId;
                }

                HttpWebRequest = httpRequest;
                Content = content;
                SuccessCallback = successDlgt;
                ErrorCallback = errorDlgt;
                UserData = userdata;
                Stats = stats;
            }

            public void ResetTimer()
            {
                m_StartTime = DateTime.Now;
            }

            public double GetElapsedMs()
            {
                return DateTime.Now.Subtract(m_StartTime).TotalMilliseconds;
            }
        }
    }
}
