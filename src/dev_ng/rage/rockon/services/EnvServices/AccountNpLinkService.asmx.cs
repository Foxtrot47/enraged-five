using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Transactions;
using System.Web;
using System.Web.Caching;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;

using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Env
{
    /// <summary>
    /// Summary description for AccountNpLinkService
    /// </summary>
    [WebService(Namespace = "http://env.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class AccountNpLinkService : System.Web.Services.WebService
    {
        //PURPOSE
        //  Returns info on a link.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryCreateResult")]
        public TResult<Int64> QueryCreate(string npOnlineId)
        {
            TResult<Int64> result = new TResult<Int64>();
            Error err = null;

            try
            {
                Int64 accountId = 0;
                if (null == (err = AccountNpLink.QueryCreate(npOnlineId, out accountId)))
                {
                    return result.SetSucceeded(accountId);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Returns info on a link.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryLinkResult")]
        public TResult<AccountNpLinkInfo> QueryLink(string npOnlineId)
        {
            TResult<AccountNpLinkInfo> result = new TResult<AccountNpLinkInfo>();
            Error err = null;

            try
            {
                using(SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    AccountNpLinkInfo info = null;

                    if (null == (err = AccountNpLink.QueryLink(con, npOnlineId, out info)))
                    {
                        return result.SetSucceeded(info);
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Returns info on multiple links.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryLinksResult")]
        public TResult<List<AccountNpLinkInfo>> QueryLinks(string npOnlineId,
                                                           uint maxResults)
        {
            TResult<List<AccountNpLinkInfo>> result = new TResult<List<AccountNpLinkInfo>>();
            Error err = null;

            try
            {
                using(SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    List<AccountNpLinkInfo> infos = null;

                    if (null == (err = AccountNpLink.QueryLinks(con, npOnlineId, maxResults, out infos)))
                    {
                        return result.SetSucceeded(infos);
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Creates a link between xuid and account ID.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "CreateLinkResult")]
        public Result CreateLink(Int64 accountId,
                                 string npOnlineId)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                using(SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    if (null == (err = AccountNpLink.CreateLink(con, accountId, npOnlineId)))
                    {
                        return result.SetSucceeded();
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Delets a link.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "DeleteLinkResult")]
        public Result DeleteLink(string npOnlineId)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                using(SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    if (null == (err = AccountNpLink.DeleteLink(con, npOnlineId)))
                    {
                        return result.SetSucceeded();
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
    }
}
