using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Transactions;
using System.Web;
using System.Web.Caching;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;

using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Env
{
    /// <summary>
    /// Summary description for AccountXblLinkService
    /// </summary>
    [WebService(Namespace = "http://env.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class AccountXblLinkService : System.Web.Services.WebService
    {
        //PURPOSE
        //  Returns info on a link.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryCreateResult")]
        public TResult<Int64> QueryCreate(UInt64 xuid,
                                          string gamertag)
        {
            TResult<Int64> result = new TResult<Int64>();
            Error err = null;

            try
            {
                Int64 accountId = 0;
                if (null == (err = AccountXblLink.QueryCreate((Int64)xuid, gamertag, out accountId)))
                {
                    return result.SetSucceeded(accountId);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Returns info on a link.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryLinkResult")]
        public TResult<AccountXblLinkInfo> QueryLink(UInt64 xuid)
        {
            TResult<AccountXblLinkInfo> result = new TResult<AccountXblLinkInfo>();
            Error err = null;

            try
            {
                using(SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    AccountXblLinkInfo info = null;

                    if (null == (err = AccountXblLink.QueryLink(con, (Int64)xuid, out info)))
                    {
                        return result.SetSucceeded(info);
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Returns info on multiple links.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryLinksResult")]
        public TResult<List<AccountXblLinkInfo>> QueryLinks(UInt64 xuid,
                                                             uint maxResults)
        {
            TResult<List<AccountXblLinkInfo>> result = new TResult<List<AccountXblLinkInfo>>();
            Error err = null;

            try
            {
                using(SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    List<AccountXblLinkInfo> infos = null;

                    if (null == (err = AccountXblLink.QueryLinks(con, (Int64)xuid, maxResults, out infos)))
                    {
                        return result.SetSucceeded(infos);
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Creates a link between xuid and account ID.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "CreateLinkResult")]
        public Result CreateLink(Int64 accountId,                                   
                                 UInt64 xuid,
                                 string gamertag)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                using(SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    if (null == (err = AccountXblLink.CreateLink(con, accountId, (Int64)xuid, gamertag)))
                    {
                        return result.SetSucceeded();
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Deletes a link.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "DeleteLinkResult")]
        public Result DeleteLink(UInt64 xuid)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                using(SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    if (null == (err = AccountXblLink.DeleteLink(con, (Int64)xuid)))
                    {
                        return result.SetSucceeded();
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
    }
}
