using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.Caching;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Env
{
    /// <summary>
    /// Summary description for GlobalDbService
    /// </summary>
    [WebService(Namespace = "http://env.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class EnvDbService : System.Web.Services.WebService
    {
        //PURPOSE
        //  Bootstraps the global DB on specified server.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "BootstrapEnvDbResult")]
        public Result BootstrapEnvDb(string connectionStr)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                if (null == (err = EnvDbHelper.Bootstrap(connectionStr)))
                {
                    return result.SetSucceeded();
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
    }
}
