using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.Caching;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;

using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Net;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Env
{
    /// <summary>
    /// Summary description for TitleService
    /// </summary>
    [WebService(Namespace = "http://core.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class TitleService : System.Web.Services.WebService
    {
        //PURPOSE
        //  Returns info on a single title, or all titles if titleId is 0.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryTitleResult")]
        public TResult<List<EnvDb.TitleInfo>> QueryTitle(int titleId)
        {
            TResult<List<EnvDb.TitleInfo>> result = new TResult<List<EnvDb.TitleInfo>>();
            Error err = null;

            try
            {
                if (titleId != 0)
                {
                    List<EnvDb.TitleInfo> titleInfos = new List<TitleInfo>();
                    titleInfos.Add(Title.GetCachedTitleInfo(titleId));

                    return result.SetSucceeded(titleInfos);
                }
                else
                {
                    using (SqlConnectionHelper con = EnvDbHelper.Open())
                    {
                        List<EnvDb.TitleInfo> titleInfos;

                        if (null == (err = Title.QueryTitle(con, titleId, 0, out titleInfos)))
                        {
                            return result.SetSucceeded(titleInfos);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Creates an entry for a title.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "CreateTitleResult")]
        public Result CreateTitle(int titleId,
                                  string connectionStr)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                using(SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    if (null == (err = EnvDb.Title.CreateTitle(con,
                                                               titleId,
                                                               connectionStr)))
                    {
                        return result.SetSucceeded();
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Deletes the record for a title.
        [return: XmlRootAttribute(ElementName = "DeleteTitleResult")]
        [WebMethod]
        public Result DeleteTitle(int titleId)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                using(SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    if (null == (err = EnvDb.Title.DeleteTitle(con, titleId)))
                    {
                        return result.SetSucceeded();
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        /*
                //PURPOSE
                //  Updates the record for a title.
                [WebMethod]
                public ResultCode UpdateTitle(int titleId,
                                              int platformId,
                                              string name,
                                              string connectionStr,
                                              string dbName)
                {
                    ResultCode result = ResultCode.UnknownError;
                    SqlConnectionHelper con = EnvDbHelper.Open();

                    if(con != null)
                    {
                        result = GlobalDb.UpdateTitle(con, 
                                                      titleId, 
                                                      platformId, 
                                                      name, 
                                                      connectionStr, 
                                                      dbName);
                        con.Close();
                    }

                    return result;
                }
         */
    }
}
