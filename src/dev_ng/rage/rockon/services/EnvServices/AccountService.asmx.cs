using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.Caching;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;

using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Env
{
    /// <summary>
    /// Summary description for AccountService
    /// </summary>
    [WebService(Namespace = "http://core.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class AccountService : System.Web.Services.WebService
    {
        public class QueryAccountResult
        {
            public List<AccountInfo> m_AccountInfos = new List<AccountInfo>();

            public QueryAccountResult()
            {
            }

            public QueryAccountResult(List<AccountInfo> accountInfos)
            {
                m_AccountInfos = accountInfos;
            }
        }

        //PURPOSE
        //  Returns info on a single account, or all accounts if accountId is 0.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryAccountResult")]
        public TResult<QueryAccountResult> QueryAccount(Int64 accountId)
        {
            TResult<QueryAccountResult> result = new TResult<QueryAccountResult>();
            Error err = null;

            try
            {
                using(SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    QueryAccountResult r = new QueryAccountResult();

                    if (null == (err = Account.QueryAccount(con, accountId, 0, out r.m_AccountInfos)))
                    {
                        return result.SetSucceeded(r);
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Creates an entry for an account.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "CreateAccountResult")]
        public TResult<Int64> CreateAccount()
        {
            TResult<Int64> result = new TResult<Int64>();
            Error err = null;

            try
            {
                using(SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    Int64 accountId = 0;

                    if (null == (err = Account.CreateAccount(con, out accountId)))
                    {
                        return result.SetSucceeded(accountId);
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Deletes the record for an account.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "DeleteAccountResult")]
        public Result DeleteAccount(Int64 accountId)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                using(SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    if (null == (err = Account.DeleteAccount(con, accountId)))
                    {
                        return result.SetSucceeded();
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Queries the login status for one or more accounts.
        public class QueryLoginStatusResult
        {
            public List<LoginStatusInfo> m_LoginStatusInfos = new List<LoginStatusInfo>();

            public QueryLoginStatusResult()
            {
            }

            public QueryLoginStatusResult(List<LoginStatusInfo> loginStatusInfos)
            {
                m_LoginStatusInfos = loginStatusInfos;
            }
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryLoginStatusResult")]
        public TResult<QueryLoginStatusResult> QueryLoginStatus(int titleId,
                                                                Int64 accountId,
                                                                bool loggedInOnly)
        {
            TResult<QueryLoginStatusResult> result = new TResult<QueryLoginStatusResult>();
            Error err = null;

            try
            {
                using (SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    QueryLoginStatusResult r = new QueryLoginStatusResult();

                    if (null == (err = Account.QueryLoginStatus(con, 
                                                                titleId, 
                                                                accountId, 
                                                                loggedInOnly, 
                                                                out r.m_LoginStatusInfos)))
                    {
                        return result.SetSucceeded(r);
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
    }
}
