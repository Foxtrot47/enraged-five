using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Rline;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Admin
{
    /// <summary>
    /// Summary description for AdminMailboxService
    /// </summary>
    [WebService(Namespace = "http://rline.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class AdminMailboxService : System.Web.Services.WebService
    {
        //----------------------------------------------------------------------
        //  Mailbox management / bootstrapping
        //----------------------------------------------------------------------
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryMailboxInfoResult")]
        public TResult<List<MailboxInfo>> QueryMailboxInfo(int titleId,
                                                           string mailboxName)
        {
            TResult<List<MailboxInfo>> result = new TResult<List<MailboxInfo>>();
            Error err = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                if (mailboxName != null && mailboxName.Length > 0)
                {
                    List<MailboxInfo> infos = new List<MailboxInfo>();
                    infos.Add(MailboxMetadata.GetCachedMailboxInfo(titleId, mailboxName));
                    return result.SetSucceeded(infos);
                }
                else
                {
                    using (SqlConnectionHelper con = Title.Open(titleInfo))
                    {
                        List<MailboxInfo> infos;

                        if (null == (err = MailboxMetadata.QueryMailboxInfo(con, mailboxName, out infos)))
                        {
                            return result.SetSucceeded(infos);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "CreateMailboxResult")]
        public Result CreateMailbox(int titleId,
                                    string name,
                                    bool broadcastOnly,
                                    string defaultRoleName,
                                    string roleOverrideMode,
                                    uint userDataMaxSize,
                                    uint msgDataMaxSize,
                                    uint sendLimit,
                                    uint receiveLimit,
                                    uint minExpirationMinutes,
                                    uint maxExpirationMinutes)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                MailboxInfo mailboxInfo = new MailboxInfo();
                mailboxInfo.Name = name;
                mailboxInfo.BroadcastOnly = broadcastOnly;
                mailboxInfo.DefaultRoleName = defaultRoleName;
                mailboxInfo.RoleOverrideMode = Mailbox.MailboxRoleOverrideModeFromString(roleOverrideMode);
                mailboxInfo.UserDataMaxSize = userDataMaxSize;
                mailboxInfo.MsgDataMaxSize = msgDataMaxSize;
                mailboxInfo.SendLimit = sendLimit;
                mailboxInfo.ReceiveLimit = receiveLimit;
                mailboxInfo.MinExpirationMinutes = minExpirationMinutes;
                mailboxInfo.MaxExpirationMinutes = maxExpirationMinutes;

                if (null == (err = Mailbox.CreateMailbox(titleInfo, mailboxInfo)))
                {
                    return result.SetSucceeded();
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "DeleteMailboxResult")]
        public Result DeleteMailbox(int titleId,
                                    string name)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                if (null == (err = Mailbox.DeleteMailbox(titleInfo, name)))
                {
                    return result.SetSucceeded();
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "DropAllResult")]
        public Result DropAll(int titleId)
        {
            Result result = new Result();

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    Mailbox.DropAll(con);
                    return result.SetSucceeded();
                }
            }
            catch (System.Exception ex)
            {
                return result.SetFailed(new Error(ex));
            }
        }

        //----------------------------------------------------------------------
        //  Mailbox Role Management
        //----------------------------------------------------------------------
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryMailboxRoleInfoResult")]
        public TResult<List<MailboxRoleInfo>> QueryMailboxRoleInfo(int titleId,
                                                                   string roleName,
                                                                   uint maxResults)
        {
            TResult<List<MailboxRoleInfo>> result = new TResult<List<MailboxRoleInfo>>();
            Error err = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    List<MailboxRoleInfo> infos;

                    if (null == (err = MailboxRoles.QueryMailboxRoleInfo(con, roleName, maxResults, out infos)))
                    {
                        return result.SetSucceeded(infos);
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "CreateMailboxRoleInfoResult")]
        public Result CreateMailboxRoleInfo(int titleId,
                                            string name,
                                            string permissions)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    if (null == (err = MailboxRoles.CreateRole(con, name, permissions)))
                    {
                        return result.SetSucceeded();
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "DeleteMailboxRoleInfoResult")]
        public Result DeleteMailboxRoleInfo(int titleId,
                                            string roleName)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    if (null == (err = MailboxRoles.DeleteRole(con, roleName)))
                    {
                        return result.SetSucceeded();
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //----------------------------------------------------------------------
        //  Mailbox Role Override Management
        //----------------------------------------------------------------------
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryMailboxRoleOverrideInfoResult")]
        public TResult<List<MailboxRoleOverrideInfo>> QueryMailboxRoleOverrideInfo(int titleId,
                                                                                   Int64 accountId,
                                                                                   string mailboxName,
                                                                                   uint maxResults)
        {
            TResult<List<MailboxRoleOverrideInfo>> result = new TResult<List<MailboxRoleOverrideInfo>>();
            Error err = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    List<MailboxRoleOverrideInfo> infos;

                    if (null == (err = MailboxRoleOverrides.QueryMailboxRoleOverrideInfo(con, accountId, mailboxName, maxResults, out infos)))
                    {
                        return result.SetSucceeded(infos);
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "CreateMailboxRoleOverrideInfoResult")]
        public Result CreateMailboxRoleOverrideInfo(int titleId,
                                                    Int64 accountId,
                                                    string mailboxName,
                                                    string roleName)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    if (null == (err = MailboxRoleOverrides.CreateRoleOverride(con, accountId, mailboxName, roleName)))
                    {
                        return result.SetSucceeded();
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "DeleteMailboxRoleOverrideInfoResult")]
        public Result DeleteMailboxRoleOverrideInfo(int titleId,
                                                    Int64 accountId,
                                                    string mailboxName)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    if (null == (err = MailboxRoleOverrides.DeleteRoleOverride(con, accountId, mailboxName)))
                    {
                        return result.SetSucceeded();
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //----------------------------------------------------------------------
        //  Mailbox message-related functions
        //----------------------------------------------------------------------       
        //PURPOSE
        //  Broadcasts a binary message, using the account ID as sender.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "BroadcastMessageResult")]
        public TResult<Int64> BroadcastMessage(int titleId,
                                               Int64 accountId,
                                               string mailboxName,
                                               string senderDisplayName,
                                               byte[] userData,
                                               byte[] msgData,
                                               uint expirationMinutes)
        {
            TResult<Int64> result = new TResult<Int64>();
            Error err = null;

            try
            {
                Int64 recordId = 0;
                if (null == (err = Mailbox.CreateRecord(titleId,
                                                         accountId,
                                                         mailboxName,
                                                         senderDisplayName,
                                                         Mailbox.BROADCAST_ID,
                                                         userData,
                                                         msgData,
                                                         expirationMinutes,
                                                         out recordId)))
                {
                    return result.SetSucceeded(recordId);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Version of BroadcastMessage that takes strings, for ease of testing.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "BroadcastMessageTextResult")]
        public TResult<Int64> BroadcastMessageText(int titleId,
                                                   Int64 accountId,
                                                   string mailboxName,
                                                   string senderDisplayName,
                                                   string userDataStr,
                                                   string msgDataStr,
                                                   uint expirationMinutes)
        {
            byte[] userData = (userDataStr == null) ? null : Encoding.UTF8.GetBytes(userDataStr);
            byte[] msgData = (msgDataStr == null) ? null : Encoding.UTF8.GetBytes(msgDataStr);

            return BroadcastMessage(titleId,
                                    accountId,
                                    mailboxName,
                                    senderDisplayName,
                                    userData,
                                    msgData,
                                    expirationMinutes);
        }

        //PURPOSE
        //  Sends a binary message to specified receiver, using the account ID as sender.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "SendMessageResult")]
        public TResult<Int64> SendMessage(int titleId,
                                          Int64 accountId,
                                          string mailboxName,
                                          string senderDisplayName,
                                          Int64 receiverId,
                                          byte[] userData,
                                          byte[] msgData,
                                          uint expirationMinutes)
        {
            TResult<Int64> result = new TResult<Int64>();
            Error err = null;

            try
            {
                Int64 recordId = 0;
                if (null == (err = Mailbox.CreateRecord(titleId,
                                                         accountId,
                                                         mailboxName,
                                                         senderDisplayName,
                                                         receiverId,
                                                         userData,
                                                         msgData,
                                                         expirationMinutes,
                                                         out recordId)))
                {
                    return result.SetSucceeded(recordId);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Version of SendMessage that takes strings, for ease of testing.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "SendMessageTextResult")]
        public TResult<Int64> SendMessageText(int titleId,
                                              Int64 accountId,
                                              string mailboxName,
                                              string senderDisplayName,
                                              Int64 receiverId,
                                              string userDataStr,
                                              string msgDataStr,
                                              uint expirationMinutes)
        {
            byte[] userData = (userDataStr == null) ? null : Encoding.UTF8.GetBytes(userDataStr);
            byte[] msgData = (msgDataStr == null) ? null : Encoding.UTF8.GetBytes(msgDataStr);

            return SendMessage(titleId,
                               accountId,
                               mailboxName,
                               senderDisplayName,
                               receiverId,
                               userData,
                               msgData,
                               expirationMinutes);
        }

        //PURPOSE
        //  Queries number of records matching specified filters.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryNumRecordsResult")]
        public TResult<int> QueryNumRecords(int titleId,
                                            Int64 accountId,
                                            string mailboxName,
                                            Int64 senderId,
                                            Int64 receiverId,
                                            Int64 startRecordId,
                                            Int64 endRecordId,
                                            bool includeExpired)
        {
            TResult<int> result = new TResult<int>();
            Error err = null;

            try
            {
                int numRecords = 0;
                if (null == (err = Mailbox.QueryNumRecords(titleId,
                                                           accountId,
                                                           mailboxName,
                                                           senderId,
                                                           receiverId,
                                                           startRecordId,
                                                           endRecordId,
                                                           includeExpired,
                                                           out numRecords)))
                {
                    return result.SetSucceeded(numRecords);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        public class QueryRecordsResult
        {
            public List<MailboxRecord> m_Records = null;
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryRecordsResult")]
        public TResult<QueryRecordsResult> QueryRecords(int titleId,
                                                        Int64 accountId,
                                                        string mailboxName,
                                                        Int64 senderId,
                                                        Int64 receiverId,
                                                        Int64 startRecordId,
                                                        Int64 endRecordId,
                                                        uint maxRecords,
                                                        bool readUserData,
                                                        bool readMsgData,
                                                        bool oldestFirst,
                                                        bool includeExpired)
        {
            TResult<QueryRecordsResult> result = new TResult<QueryRecordsResult>();            
            Error err = null;

            try
            {
                QueryRecordsResult r = new QueryRecordsResult();

                if (null == (err = Mailbox.QueryRecords(titleId,
                                                         accountId,
                                                         mailboxName,
                                                         senderId,
                                                         receiverId,
                                                         startRecordId,
                                                         endRecordId,
                                                         maxRecords,
                                                         readUserData,
                                                         readMsgData,
                                                         oldestFirst,
                                                         includeExpired,
                                                         out r.m_Records)))
                {
                    return result.SetSucceeded(r);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryMsgDataResult")]
        public TResult<byte[]> QueryMsgData(int titleId,
                                            Int64 accountId,
                                            string mailboxName,
                                            Int64 recordId)
        {
            TResult<byte[]> result = new TResult<byte[]>();
            Error err = null;

            try
            {
                byte[] data;

                if (null == (err = Mailbox.QueryMsgData(titleId,
                                                        accountId,
                                                        mailboxName,
                                                        Mailbox.ANY_ID,
                                                        accountId,
                                                        recordId, 
                                                        out data)))
                {
                    return result.SetSucceeded(data);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
       
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "CreateRecordResult")]
        public TResult<Int64> CreateRecord(int titleId,
                                           string mailboxName,
                                           Int64 senderId,
                                           string senderDisplayName,
                                           Int64 receiverId,
                                           string userDataStr,
                                           string msgDataStr,
                                           uint expirationMinutes)
        { 
            TResult<Int64> result = new TResult<Int64>();
            Error err = null;

            try
            {
                Int64 recordId;
                byte[] userData = Encoding.UTF8.GetBytes(userDataStr);
                byte[] msgData = Encoding.UTF8.GetBytes(msgDataStr);

                if (null == (err = Mailbox.CreateRecord(titleId,
                                                         senderId,
                                                         mailboxName,
                                                         senderDisplayName,
                                                         receiverId,
                                                         userData,
                                                         msgData,
                                                         expirationMinutes,
                                                         out recordId)))
                {
                    return result.SetSucceeded(recordId);
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }


        [WebMethod]
        [return: XmlRootAttribute(ElementName = "DeleteRecordsResult")]
        public TResult<uint> DeleteRecords(int titleId,
                                           Int64 accountId,
                                           string mailboxName,
                                           Int64 senderId,
                                           Int64 receiverId,
                                           Int64 startRecordId,
                                           Int64 endRecordId,
                                           bool expiredOnly)
        {
            TResult<uint> result = new TResult<uint>();
            Error err = null;

            try
            {
                uint numDeleted;

                if (null == (err = Mailbox.DeleteRecords(titleId,
                                                         accountId,
                                                         mailboxName,
                                                         senderId,
                                                         receiverId,
                                                         startRecordId,
                                                         endRecordId,
                                                         expiredOnly,
                                                         out numDeleted)))
                {
                    return result.SetSucceeded(numDeleted);
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Deletes all expired records in a mailbox.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "DeleteExpiredRecordsResult")]
        public Result DeleteExpiredRecords(int titleId,
                                           Int64 accountId,
                                           string mailboxName)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                if (null == (err = Mailbox.DeleteExpiredRecords(titleId, accountId, mailboxName)))
                {
                    return result.SetSucceeded();
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
    }
}
