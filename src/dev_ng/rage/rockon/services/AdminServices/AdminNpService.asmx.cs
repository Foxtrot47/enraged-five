using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Np;
using Rockstar.Rockon.Rline;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Admin
{
    /// <summary>
    /// Summary description for AdminNpService
    /// </summary>
    [WebService(Namespace = "http://rline.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class AdminNpService : System.Web.Services.WebService
    {
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "CreateCipherTableResult")]
        public Result CreateCipherTable()
        {
            Result result = new Result();
            Error err = null;

            try
            {
                if (null == (err = Cipher.CreateTable()))
                {
                    return result.SetSucceeded();
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "DropCipherTableResult")]
        public Result DropCipherTable()
        {
            Result result = new Result();
            Error err = null;

            try
            {
                if (null == (err = Cipher.DropTable()))
                {
                    return result.SetSucceeded();
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "CreateCipherFromFileResult")]
        public Result CreateCipherFromFile(string cipherName,
                                           string fileName)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                if (null == (err = Cipher.CreateCipherFromFile(cipherName, fileName)))
                {
                    return result.SetSucceeded();
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "DeleteCipherResult")]
        public Result DeleteCipher(string name)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                uint numDeleted;
                if (null == (err = Cipher.DeleteCipher(name, out numDeleted)))
                {
                    return result.SetSucceeded();
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryCipherInfoResult")]
        public TResult<List<CipherInfo>> QueryCipherInfo(string name,
                                                         uint maxResults)
        {
            TResult<List<CipherInfo>> result = new TResult<List<CipherInfo>>();
            Error err = null;

            try
            {
                List<CipherInfo> infos;

                if (null == (err = Cipher.QueryCipherInfo(name, maxResults, out infos)))
                {
                    return result.SetSucceeded(infos);
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
    }
}
