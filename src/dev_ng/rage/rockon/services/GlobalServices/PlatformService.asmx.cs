using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

using Rockstar.Rockon.GlobalDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Global
{
    /// <summary>
    /// Summary description for PlatformService
    /// </summary>
    [WebService(Namespace = "http://core.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class PlatformService : System.Web.Services.WebService
    {
        public class QueryPlatformResult
        {
            public List<PlatformInfo> m_PlatformInfos = new List<PlatformInfo>();

            public QueryPlatformResult()
            {
            }

            public QueryPlatformResult(List<PlatformInfo> infos)
            {
                m_PlatformInfos = infos;
            }
        }

        //PURPOSE
        //  Returns info on a single title, or all titles if platformId is 0.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryPlatformResult")]
        public TResult<QueryPlatformResult> QueryPlatform(int platformId)
        {
            TResult<QueryPlatformResult> result = new TResult<QueryPlatformResult>();
            Error err = null;

            try
            {
                using (SqlConnectionHelper con = GlobalDbHelper.Open())
                {
                    QueryPlatformResult qpr = new QueryPlatformResult();

                    if (null == (err = Platform.QueryPlatform(con, platformId, 0, out qpr.m_PlatformInfos)))
                    {
                        return result.SetSucceeded(qpr);
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
    }
}
