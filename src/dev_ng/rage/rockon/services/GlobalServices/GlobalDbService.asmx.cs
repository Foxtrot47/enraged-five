using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

using Rockstar.Rockon.GlobalDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Global
{
    /// <summary>
    /// Summary description for GlobalDbService
    /// </summary>
    [WebService(Namespace = "http://core.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class GlobalDbService : System.Web.Services.WebService
    {
        //PURPOSE
        //  Bootstraps the global DB on specified server.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "BootstrapGlobalDbResult")]
        public Result BootstrapGlobalDb(string connectionStr)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                if (null == (err = GlobalDbHelper.Bootstrap(connectionStr)))
                {
                    return result.SetSucceeded();
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
    }
}
