using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

using Rockstar.Rockon.GlobalDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Global
{
    /// <summary>
    /// Summary description for TitleService
    /// </summary>
    [WebService(Namespace = "http://core.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class TitleService : System.Web.Services.WebService
    {
        public class QueryTitleResult
        {
            public List<TitleInfo> m_TitleInfos = new List<TitleInfo>();

            public QueryTitleResult()
            {
            }

            public QueryTitleResult(List<TitleInfo> titleInfos)
            {
                m_TitleInfos = titleInfos;
            }
        }

        //PURPOSE
        //  Returns info on a single title, or all titles if titleId is 0.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryTitleResult")]
        public TResult<QueryTitleResult> QueryTitle(int titleId)
        {
            TResult<QueryTitleResult> result = new TResult<QueryTitleResult>();
            Error err = null;

            try
            {
                using (SqlConnectionHelper con = GlobalDbHelper.Open())
                {
                    QueryTitleResult qtr = new QueryTitleResult();

                    if (null == (err = Title.QueryTitle(con, titleId, 0, out qtr.m_TitleInfos)))
                    {
                        return result.SetSucceeded(qtr);
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Creates an entry for a title.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "CreateTitleResult")]
        public Result CreateTitle(int titleId,
                                          int platformId,
                                          string internalName, 
                                          string fullName)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                using (SqlConnectionHelper con = GlobalDbHelper.Open())
                {
                    if (null == (err = Title.CreateTitle(con, titleId, platformId, internalName, fullName)))
                    {
                        return result.SetSucceeded();
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Deletes the record for a title.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "DeleteTitleResult")]
        public Result DeleteTitle(int titleId)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                using (SqlConnectionHelper con = GlobalDbHelper.Open())
                {
                    if (null == (err = Title.DeleteTitle(con, titleId)))
                    {
                        return result.SetSucceeded();
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
/*
        //PURPOSE
        //  Updates the record for a title.
        [WebMethod]
        public ResultCode UpdateTitle(int titleId,
                                      int platformId,
                                      string name,
                                      string connectionStr,
                                      string dbName)
        {
            ResultCode result = ResultCode.UnknownError;
            SqlConnectionHelper con = GlobalDbHelper.Open();

            if(con != null)
            {
                result = GlobalDb.UpdateTitle(con, 
                                              titleId, 
                                              platformId, 
                                              name, 
                                              connectionStr, 
                                              dbName);
                con.Close();
            }

            return result;
        }
 */ 
    }
}
