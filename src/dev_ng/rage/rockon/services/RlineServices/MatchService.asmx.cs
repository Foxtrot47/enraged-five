using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;        

using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Rline;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Rline
{
    /// <summary>
    /// Summary description for MatchService
    /// </summary>
    [WebService(Namespace = "http://rline.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class MatchService : System.Web.Services.WebService
    {
        public class CreateMatchResult
        {
            public int m_MatchId;

            public CreateMatchResult() { }
        }

        public class QueryMatchInfoResult
        {
            public List<MatchInfo> m_MatchInfos;

            public QueryMatchInfoResult() { }
        }

        public class QueryMatchMemberResult
        {
            public List<MatchMemberInfo> m_MatchMemberInfos;

            public QueryMatchMemberResult() { }

            public int GetMemberIndex(Int64 id)
            {
                for (int i = 0; i < m_MatchMemberInfos.Count; i++)
                {
                    if (m_MatchMemberInfos[i].m_AccountId == id)
                    {
                        return i;
                    }
                }
                return -1;
            }
        }

        //PURPOSE
        //  Returns info on a single match.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryMatchResult")]
        public TResult<QueryMatchInfoResult> QueryMatch(int titleId,
                                                       int matchId)
        {
            TResult<QueryMatchInfoResult> result = new TResult<QueryMatchInfoResult>();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        MatchInfo matchInfo;

                        if (null == (err = Match.QueryMatch(con, matchId, out matchInfo)))
                        {
                            QueryMatchInfoResult r = new QueryMatchInfoResult();
                            r.m_MatchInfos.Add(matchInfo);

                            return result.SetSucceeded(r);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Returns info on multiple matches, ordered by most recently created.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryMatchesResult")]
        public TResult<QueryMatchInfoResult> QueryMatches(int titleId,
                                                         uint maxResults)
        {
            TResult<QueryMatchInfoResult> result = new TResult<QueryMatchInfoResult>();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        QueryMatchInfoResult r = new QueryMatchInfoResult();

                        if (null == (err = Match.QueryMatches(con, maxResults, out r.m_MatchInfos)))
                        {
                            return result.SetSucceeded(r);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Returns info for a single match gamer, or all gamers for a match if accountId is 0.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryMatchMemberResult")]
        public TResult<QueryMatchMemberResult> QueryMatchMember(int titleId,
                                                               int matchId,
                                                               Int64 accountId)
        {
            TResult<QueryMatchMemberResult> result = new TResult<QueryMatchMemberResult>();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        QueryMatchMemberResult r = new QueryMatchMemberResult();

                        if (null == (err = MatchMember.QueryMatchMember(con,
                                                       matchId,
                                                       accountId,
                                                       out r.m_MatchMemberInfos)))
                        {
                            return result.SetSucceeded(r);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Creates a match record for specified title.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "CreateMatchResult")]
        public TResult<CreateMatchResult> CreateMatch(int titleId,
                                                     bool isArbitrated)
        {
            TResult<CreateMatchResult> result = new TResult<CreateMatchResult>();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        CreateMatchResult r = new CreateMatchResult();

                        if (null == (err = Match.CreateMatch(con, isArbitrated, out r.m_MatchId)))
                        {
                            return result.SetSucceeded(r);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Deletes match on the backend.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "DeleteMatchResult")]
        public Result DeleteMatch(int titleId,
                                          int matchId)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        if (null == (err = Match.DeleteMatch(con, matchId)))
                        {
                            return result.SetSucceeded();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Adds a gamer to a match.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "AddMatchMemberResult")]
        public Result AddMatchMember(int titleId,
                                             int matchId,
                                             Int64 accountId,
                                             string displayName)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        if (null == (err = MatchMember.AddMatchMember(con, matchId, accountId, displayName)))
                        {
                            return result.SetSucceeded();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Removes a gamer from a match.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "RemoveMatchMemberResult")]
        public Result RemoveMatchMember(int titleId,
                                                int matchId,
                                                Int64 accountId)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        if (null == (err = MatchMember.RemoveMatchMember(con, matchId, accountId)))
                        {
                            return result.SetSucceeded();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Starts a match.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "StartMatchResult")]
        public Result StartMatch(int titleId,
                                         int matchId)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        if (null == (err = Match.StartMatch(con, matchId)))
                        {
                            return result.SetSucceeded();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Ends a match.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "EndMatchResult")]
        public Result EndMatch(int titleId,
                                       int matchId)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        if (null == (err = Match.EndMatch(con, matchId)))
                        {
                            return result.SetSucceeded();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
    }
}
