using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Rline;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Rline
{
    /// <summary>
    /// This service provides methods used by rlTelemetry.  Since this is 
    /// a client-callable service, it should only have methods we are
    /// ok with clients seeing.
    /// </summary>
    [WebService(Namespace = "http://rline.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class TelemetryService : System.Web.Services.WebService
    {
        //PURPOSE
        //  Process a telemetry submission from the client.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "ProcessSubmissionResult")]
        public Result ProcessSubmission(int titleId,
                                        Int64 accountId,
                                        string rson)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                if (null == (err = Telemetry.ProcessSubmission(titleId,
                                                               accountId,
                                                               rson)))
                {
                    return result.SetSucceeded();
                }
            }
            catch (System.Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
    }
}
