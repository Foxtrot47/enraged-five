using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;        

using Rockstar.Rockon.EnvDb;
using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Rline;
using Rockstar.Rockon.Sql;

namespace Rockstar.Rockon.Services.Rline
{
    /// <summary>
    /// Summary description for LeaderboardService
    /// </summary>
    [WebService(Namespace = "http://rline.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class LeaderboardService : System.Web.Services.WebService
    {
        public class QueryLbInfoResult
        {
            public List<LeaderboardInfo> m_LbInfos;

            public QueryLbInfoResult() {}
        }

        public class ReadRowsResult
        {
            public List<LeaderboardRow> m_Rows;

            public ReadRowsResult() {}
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "QueryLbInfoResult")]
        public TResult<QueryLbInfoResult> QueryLbInfo(int titleId,
                                                     UInt32 lbId)
        {
            TResult<QueryLbInfoResult> result = new TResult<QueryLbInfoResult>();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        QueryLbInfoResult qir = new QueryLbInfoResult();

                        if (null == (err = Leaderboard.QueryLbInfo(con, lbId, out qir.m_LbInfos)))
                        {
                            return result.SetSucceeded(qir);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        [WebMethod]
        [return: XmlRootAttribute(ElementName = "ReadByRankResult")]
        public TResult<ReadRowsResult> ReadByRank(int titleId,
                                                  uint lbId,
                                                  uint startingRank,
                                                  uint maxRows)
        {
            TResult<ReadRowsResult> result = new TResult<ReadRowsResult>();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        ReadRowsResult r = new ReadRowsResult();

                        if (null == (err = Leaderboard.ReadByRank(con, lbId, startingRank, maxRows, out r.m_Rows)))
                        {
                            return result.SetSucceeded(r);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Reads LB record for gamer.  List will contain max of 1 record.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "ReadByGamerResult")]
        public TResult<ReadRowsResult> ReadByGamer(int titleId,
                                                  uint lbId,
                                                  Int64 accountId)
        {
            TResult<ReadRowsResult> result = new TResult<ReadRowsResult>();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        LeaderboardRow row;

                        if (null == (err = Leaderboard.ReadByGamer(con, lbId, accountId, out row)))
                        {
                            ReadRowsResult r = new ReadRowsResult();
                            r.m_Rows.Add(row);

                            return result.SetSucceeded(r);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Reads LB records within specified radius of gamer's row.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "ReadByGamerRadiusResult")]
        public TResult<ReadRowsResult> ReadByGamerRadius(int titleId,
                                                        uint lbId,
                                                        Int64 accountId,
                                                        uint radius)
        {
            TResult<ReadRowsResult> result = new TResult<ReadRowsResult>();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        ReadRowsResult r = new ReadRowsResult();

                        if (null == (err = Leaderboard.ReadByGamerRadius(con, lbId, accountId, radius, out r.m_Rows)))
                        {
                            return result.SetSucceeded(r);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }

        //PURPOSE
        //  Accepts stat submissions from a gamer.  The submission can be to multiple
        //  leaderboards, and may be for either the submitting gamer or other gamers
        //  in the match.
        [WebMethod]
        [return: XmlRootAttribute(ElementName = "WriteStatsResult")]
        public Result WriteStats(int titleId,
                                         int matchId,
                                         Int64 accountId,
                                         List<LeaderboardUpdate> updates)
        {
            Result result = new Result();
            Error err = null;

            try
            {
                SqlConnectionHelper con;

                if (null == (err = Title.Open(titleId, out con)))
                {
                    using (con)
                    {
                        if (null == (err = Leaderboard.WriteStats(con, matchId, accountId, updates)))
                        {
                            return result.SetSucceeded();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return result.SetFailed(err);
        }
    }
}
