﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;

using Rockstar.Rockon.Diag;
using Rockstar.Rockon.Sql;
using Rockstar.Rockon.Rline;
using Rockstar.Rockon.EnvDb;

namespace Rockstar.Rockon.Services.Rline
{
    public class RoomInfo
    {
        public int NumMembers = 0;
        public int MaxSlots = 0;
        public Int64 RoomId = 0;
        public int SecKeyId = 0;
        public Int64 SecKey = 0;

        public RoomInfo()
        {
        }

        public RoomInfo(Int64 id, int secKeyId, Int64 secKey, int maxSlots)
        {
            RoomId = id;
            MaxSlots = maxSlots;
            SecKeyId = secKeyId;
            SecKey = secKey;
        }
    }

    public class Room
    {
        public static bool CreateTables(SqlConnectionHelper con)
        {
            string cmd;
            
            //The Rooms table.
            cmd =@"
            CREATE TABLE [dbo].[Rooms](
	            [RoomId] [bigint] IDENTITY(1,1) NOT NULL,
	            [MaxSlots] [int] NOT NULL,
	            [NumMembers] [int] NOT NULL,
	            [OwnerId] [bigint] NOT NULL,
	            [SecKeyId] [int] NOT NULL,
	            [SecKey] [bigint] NOT NULL,
             CONSTRAINT [PK_Rooms] PRIMARY KEY CLUSTERED 
            (
	            [RoomId] ASC
            ))";

            con.AdHocExecuteNonQuery(cmd);

            //Rooms.NumMembers defaults to zero.
            cmd =@"
            ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF_Rooms_NumMembers]  DEFAULT ((0)) FOR [NumMembers]";

            con.AdHocExecuteNonQuery(cmd);

            //The RoomMembers table.
            cmd = @"
            CREATE TABLE [dbo].[RoomMembers](
	            [RoomId] [bigint] NOT NULL,
	            [AccountId] [bigint] NOT NULL,
             CONSTRAINT [PK_RoomMembers] PRIMARY KEY CLUSTERED 
            (
	            [RoomId] ASC,
	            [AccountId] ASC
            ))";

            con.AdHocExecuteNonQuery(cmd);

            return true;
        }

        public static bool CreateIndexes(SqlConnectionHelper con)
        {
            //Index RoomMembers on AccountId.
            string cmd = @"
            CREATE NONCLUSTERED INDEX [AccountId] ON [dbo].[RoomMembers] 
            (
	            [AccountId] ASC
            )";

            con.AdHocExecuteNonQuery(cmd);

            //Index Rooms on OwnerId.
            cmd = @"
            CREATE NONCLUSTERED INDEX [OwnerId] ON [dbo].[Rooms] 
            (
	            [OwnerId] ASC
            )";


            con.AdHocExecuteNonQuery(cmd);

            return true;
        }

        public static bool CreateTriggers(SqlConnectionHelper con)
        {
            string cmd;
            
            //JoinRoomTrigger
            //  Updates Rooms.NumMembers when a player joins a room,
            //  or rolls back the transaction if Rooms.MaxSlots is exceeded.
            cmd = @"
            CREATE TRIGGER [dbo].[JoinRoomTrigger] ON [dbo].[RoomMembers]
            AFTER INSERT
            AS
            BEGIN
              SET NOCOUNT ON;
              UPDATE Rooms SET NumMembers = (NumMembers + 1) WHERE RoomId IN (SELECT RoomId FROM inserted) AND NumMembers < MaxSlots;
              IF 0 = @@ROWCOUNT
                ROLLBACK TRANSACTION;
            END";

            con.AdHocExecuteNonQuery(cmd);

            //LeaveRoomTrigger
            //  Updates Rooms.NumMembers when a player leaves a room.
            cmd = @"
            CREATE TRIGGER [dbo].[LeaveRoomTrigger] ON [dbo].[RoomMembers]
            AFTER DELETE
            AS
            BEGIN
              SET NOCOUNT ON;
              UPDATE Rooms SET NumMembers = (NumMembers - 1) WHERE RoomId IN (SELECT RoomId FROM deleted) AND NumMembers < MaxSlots;
              IF 0 = @@ROWCOUNT
                ROLLBACK TRANSACTION;
            END";

            con.AdHocExecuteNonQuery(cmd);

            return true;
        }

        public static bool CreateStoredProcedures(SqlConnectionHelper con)
        {
            bool success = false;
            try
            {
                string cmd;

                //CreateJoinRoom
                cmd =   "IF OBJECT_ID('dbo.CreateJoinRoom', 'P' ) IS NOT NULL\n"
                        + "  DROP PROCEDURE dbo.CreateJoinRoom;\n";

                con.AdHocExecuteNonQuery(cmd);

                cmd =   "CREATE PROCEDURE [dbo].[CreateJoinRoom] @accountId bigint, @secKeyId int, @secKey bigint, @maxSlots int\n"
                        + "AS\n"
                        + "BEGIN\n"
                        + "  DECLARE @roomId bigint;\n"
                        + "  SET @roomId = 0;\n"
                        + "  SET NOCOUNT ON;\n"
                        + "  BEGIN TRANSACTION\n"
                        + "    INSERT INTO Rooms (MaxSlots, OwnerId, SecKeyId, SecKey) VALUES (@maxSlots, @accountId, @secKeyId, @secKey);\n"
                        + "    SET @roomId = SCOPE_IDENTITY();\n"
                        + "    DECLARE @result int;\n"
                        + "    EXECUTE @result = dbo.JoinRoom @roomId, @accountId;\n"
                        + "    IF 0 = @result\n"
                        + "    BEGIN\n"
                        + "      SET @roomId = 0;\n"
                        + "      ROLLBACK TRANSACTION;\n"
                        + "    END\n"
                        + "    ELSE\n"
                        + "      COMMIT TRANSACTION;\n"
                        + "  RETURN @roomId;\n"
                        + "END\n";

                con.AdHocExecuteNonQuery(cmd);

                //JoinRoom
                cmd =   "IF OBJECT_ID('dbo.JoinRoom', 'P' ) IS NOT NULL\n"
                        + "  DROP PROCEDURE dbo.JoinRoom;\n";

                con.AdHocExecuteNonQuery(cmd);

                cmd =   "CREATE PROCEDURE [dbo].[JoinRoom] @roomId bigint, @accountId bigint\n"
                        + "AS\n"
                        + "BEGIN\n"
                        + "  DECLARE @success int;\n"
                        + "  SET NOCOUNT ON;\n"
                        + "  BEGIN TRANSACTION\n"
                        + "    INSERT INTO RoomMembers (RoomId, AccountId) VALUES (@roomId, @accountId);\n"
                        + "    SET @success = @@ROWCOUNT;\n"
                        + "    IF 0 = @success\n"
                        + "      ROLLBACK TRANSACTION;\n"
                        + "    ELSE\n"
                        + "      COMMIT TRANSACTION;\n"
                        + "    RETURN @success;\n"
                        + "END\n";

                con.AdHocExecuteNonQuery(cmd);

                //LeaveRoom
                cmd =   "IF OBJECT_ID('dbo.LeaveRoom', 'P' ) IS NOT NULL\n"
                        + "  DROP PROCEDURE dbo.LeaveRoom;\n";

                con.AdHocExecuteNonQuery(cmd);

                cmd =   "CREATE PROCEDURE [dbo].[LeaveRoom] @roomId bigint, @accountId bigint\n"
                        + "AS\n"
                        + "BEGIN\n"
                        + "  DECLARE @success int;\n"
                        + "  SET @success = 0;\n"
                        + "  SET NOCOUNT ON;\n"
                        + "  BEGIN TRANSACTION\n"
                        + "    DELETE FROM RoomMembers WHERE RoomId=@roomId AND AccountId=@accountId;\n"
                        + "    SET @success = @@ROWCOUNT;\n"
                        + "    COMMIT TRANSACTION;\n"
                        + "    RETURN @success;\n"
                        + "END\n";

                con.AdHocExecuteNonQuery(cmd);

                //LeaveAllRooms
                cmd =   "IF OBJECT_ID('dbo.LeaveAllRooms', 'P' ) IS NOT NULL\n"
                        + "  DROP PROCEDURE dbo.LeaveAllRooms;\n";

                con.AdHocExecuteNonQuery(cmd);

                cmd =   "CREATE PROCEDURE [dbo].[LeaveAllRooms] @accountId bigint\n"
                        + "AS\n"
                        + "BEGIN\n"
                        + "  DECLARE @numLeft int;\n"
                        + "  SET @numLeft = 0;\n"
                        + "  SET NOCOUNT ON;\n"
                        + "  BEGIN TRANSACTION\n"
                        + "    DELETE FROM RoomMembers WHERE AccountId=@accountId;\n"
                        + "    SET @numLeft = @@ROWCOUNT;\n"
                        + "    COMMIT TRANSACTION;\n"
                        + "    RETURN @numLeft;\n"
                        + "END\n";

                con.AdHocExecuteNonQuery(cmd);

                success = true;
            }
            catch (System.Exception ex)
            {
                Trace.WriteLine(ex);
            }

            return success;
        }

        public static void CreateAll(SqlConnectionHelper con)
        {
            CreateTables(con);
            CreateIndexes(con);
            CreateTriggers(con);
            CreateStoredProcedures(con);

        }

        public static Error CreateJoinRoom(SqlConnectionHelper con,
                                            Int64 accountId,
                                            int secKeyId,
                                            Int64 secKey,
                                            int maxSlots,
                                            out Int64 roomId)
        {
            Context ctx = RlineContext.Create();
            roomId = 0;

            try
            {
                if(0 == accountId)
                {
                    return ctx.Error(ErrorCode.InvalidArguments, "Invalid accountId:" + accountId);
                }
                else if(0 == maxSlots)
                {
                    return ctx.Error(ErrorCode.InvalidArguments, "Invalid maxSlots:" + maxSlots);
                }

                string cmd = "DECLARE @result bigint;\n";
                cmd += "EXECUTE @result = dbo.CreateJoinRoom @accountId,@secKeyId,@secKey,@maxSlots;\n";
                cmd += "SELECT @result;";

                SqlCmdParams args = new SqlCmdParams();
                args.Add("accountId", accountId);
                args.Add("secKeyId", secKeyId);
                args.Add("secKey", secKey);
                args.Add("maxSlots", maxSlots);

                roomId = (Int64) con.ExecuteScalar(cmd, args);

                if(0 != roomId)
                {
                    return ctx.Success2();
                }
                else
                {
                    return ctx.Error(ErrorCode.UnknownError, "Unable to create room");
                }
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        public static Error DestroyRoom(SqlConnectionHelper con, Int64 roomId)
        {
            Context ctx = RlineContext.Create();

            try
            {
                string cmd = "DELETE FROM Rooms WHERE RoomId='@roomId' AND NumMembers = 0";
                SqlCmdParams args = new SqlCmdParams();
                args.Add("roomId", roomId);

                int roomsDestroyed = con.ExecuteNonQuery(cmd, args);

                if(1 == roomsDestroyed)
                {
                    return ctx.Success2();
                }
                else
                {
                    Debug.Assert(0 == roomsDestroyed);
                    return ctx.Error(ErrorCode.DoesNotExist, "Room " + roomId + " does not exist");
                }
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        public static Error JoinRoom(SqlConnectionHelper con, Int64 roomId, Int64 accountId)
        {
            Context ctx = RlineContext.Create();

            try
            {
                string cmd = "DECLARE @result int;\n";
                cmd += "EXECUTE @result = dbo.JoinRoom @roomId, @accountId;\n";
                cmd += "SELECT @result;";

                SqlCmdParams args = new SqlCmdParams();
                args.Add("roomId", roomId);
                args.Add("accountId", accountId);

                int success = (int) con.ExecuteScalar(cmd, args);

                if(1 == success)
                {
                    string msg = string.Format("{{m=RoomService.JoinRoom,roomId={0},accountId={1}}}",
                                                roomId,
                                                accountId);
                    Room.SendMessageToMembers(con, roomId, msg);
                    return ctx.Success2();
                }
                else
                {
                    Debug.Assert(0 == success);
                    return ctx.Error(ErrorCode.UnknownError, "Unable to add user " + accountId + " to room " + roomId);
                }
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        public static Error LeaveRoom(SqlConnectionHelper con, Int64 roomId, Int64 accountId)
        {
            Context ctx = RlineContext.Create();

            try
            {
                string cmd = "DECLARE @result int;\n";
                cmd += "EXECUTE @result = dbo.LeaveRoom @roomId, @accountId;\n";
                cmd += "SELECT @result;";

                SqlCmdParams args = new SqlCmdParams();
                args.Add("roomId", roomId);
                args.Add("accountId", accountId);

                int success = (int) con.ExecuteScalar(cmd, args);

                if(1 == success)
                {
                    string msg = string.Format("{{m=RoomService.LeaveRoom,roomId={0},accountId={1}}}",
                                                roomId,
                                                accountId);
                    Room.SendMessageToMembers(con, roomId, msg);
                    return ctx.Success2();
                }
                else
                {
                    Debug.Assert(0 == success);
                    return ctx.Error(ErrorCode.DoesNotExist, "User " + accountId + " does not exist in room " + roomId);
                }
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        public static Error LeaveAllRooms(SqlConnectionHelper con, Int64 accountId)
        {
            Context ctx = RlineContext.Create();

            try
            {
                string cmd = "SELECT RoomId FROM RoomMembers WHERE AccountId=@accountId";

                SqlCmdParams args = new SqlCmdParams();
                args.Add("accountId", accountId);

                using(DbDataReader reader = con.ExecuteReader(cmd, args))
                {
                    while (reader.Read())
                    {
                        Int64 roomId = (Int64) reader["RoomId"];
                        Room.LeaveRoom(con, (Int64) reader["RoomId"], accountId);
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        public static Error FindRooms(SqlConnectionHelper con,
                                        List<RoomInfo> rooms)
        {
            Context ctx = RlineContext.Create();
            rooms.Clear();

            try
            {
                string cmd = "SELECT * FROM Rooms WHERE NumMembers < MaxSlots";

                using (DbDataReader reader = con.ExecuteReader(cmd, null))
                {
                    while(reader.Read())
                    {
                        RoomInfo roomInfo = new RoomInfo();

                        roomInfo.MaxSlots = int.Parse(reader["MaxSlots"].ToString());
                        roomInfo.RoomId = Int64.Parse(reader["RoomId"].ToString());
                        roomInfo.NumMembers = int.Parse(reader["NumMembers"].ToString());

                        rooms.Add(roomInfo);
                    }
                }

                return ctx.Success2();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        public static Error SendMessageToMembers(SqlConnectionHelper con,
                                                Int64 roomId,
                                                string msg)
        {
            Context ctx = RlineContext.Create();

            try
            {
                string cmd = "SELECT AccountId FROM RoomMembers WHERE RoomId=@roomId";

                SqlCmdParams args = new SqlCmdParams();
                args.Add("roomId", roomId);

                using(DbDataReader reader = con.ExecuteReader(cmd, args))
                {
                    while(reader.Read())
                    {
                        Int64 accountId = (Int64) reader["AccountId"];

                        Room.SendMessageToMember(accountId, msg);
                    }

                    reader.Close();
                }

                return ctx.Success2();
            }
            catch (System.Exception ex)
            {
            	return ctx.Error(ex);
            }
        }

        public static Error SendMessageToMember(Int64 accountId, string msg)
        {
            Context ctx = RlineContext.Create();

            try
            {
                using(SqlConnectionHelper con = EnvDbHelper.Open())
                {
                    string cmd = "SELECT ClientId,SgAddr FROM LoginStatus WHERE AccountId=@accountId";

                    SqlCmdParams args = new SqlCmdParams();
                    args.Add("accountId", accountId);

                    using(DbDataReader reader = con.ExecuteReader(cmd, args))
                    {
                        if(reader.Read())
                        {
                            int clientId = (int) ((Int64)reader["ClientId"]);
                            string sgAddr = (string) reader["SgAddr"];

                            Socket skt = new Socket(AddressFamily.InterNetwork,
                                                     SocketType.Stream,
                                                     ProtocolType.Tcp);

                            string[] ipAndPort = sgAddr.Split(':');
                            Debug.Assert(2 == ipAndPort.Length);

                            string packet = "{{clientId={0},accountId={1},msg={2}}}";
                            packet = string.Format(packet, clientId, accountId, msg);

                            skt.Connect(ipAndPort[0], int.Parse(ipAndPort[1]));
                            skt.Send(Encoding.UTF8.GetBytes(packet));
                            skt.Shutdown(SocketShutdown.Both);
                            skt.Close();
                        }

                        reader.Close();
                    }
                }

                return ctx.Success2();
            }
            catch (System.Exception ex)
            {
            	return ctx.Error(ex);
            }
        }
    }

    /// <summary>
    /// Summary description for RoomService
    /// </summary>
    [WebService(Namespace="http://rline.services.rockon.rockstar.com/")]
    [WebServiceBinding(ConformsTo=WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class RoomService:System.Web.Services.WebService
    {
        [WebMethod]
        public TResult<RoomInfo> CreateJoinRoom(int titleId,
                                                Int64 accountId,
                                                int secKeyId,
                                                Int64 secKey,
                                                int maxSlots)
        {
            RoomInfo roomInfo = null;
            Error err;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    Int64 roomId = 0;
                    err = Room.CreateJoinRoom(con, accountId, secKeyId, secKey, maxSlots, out roomId);

                    if(null == err)
                    {
                        roomInfo = new RoomInfo(roomId, secKeyId, secKey, maxSlots);
                        roomInfo.NumMembers = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            TResult<RoomInfo> result = new TResult<RoomInfo>();
            if(null == err)
            {
                result.SetSucceeded(roomInfo);
            }
            else
            {
                result.SetFailed(err);
            }

            return result;
        }

        [WebMethod]
        public Result JoinRoom(int titleId, Int64 roomId, Int64 accountId)
        {
            Error err = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    err = Room.JoinRoom(con, roomId, accountId);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return (null == err)
                    ? (new Result()).SetSucceeded()
                    : (new Result()).SetFailed(err);
        }

        [WebMethod]
        public Result LeaveRoom(int titleId, Int64 roomId, Int64 accountId)
        {
            Error err = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    err = Room.LeaveRoom(con, roomId, accountId);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return (null == err)
                    ? (new Result()).SetSucceeded()
                    : (new Result()).SetFailed(err);
        }

        [WebMethod]
        public TResult<List<RoomInfo>> FindRooms(int titleId)
        {
            List<RoomInfo> rooms = new List<RoomInfo>();

            Error err = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    err = Room.FindRooms(con, rooms);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            TResult<List<RoomInfo>> result = new TResult<List<RoomInfo>>();
            if(null == err)
            {
                result.SetSucceeded(rooms);
            }
            else
            {
                result.SetFailed(err);
            }

            return result;
        }

        [WebMethod]
        public Result LeaveAllRooms(int titleId, Int64 accountId)
        {
            Error err = null;

            try
            {
                TitleInfo titleInfo = Title.GetCachedTitleInfo(titleId);

                using (SqlConnectionHelper con = Title.Open(titleInfo))
                {
                    err = Room.LeaveAllRooms(con, accountId);
                }
            }
            catch (Exception ex)
            {
                err = new Error(ex);
            }

            return (null == err)
                    ? (new Result()).SetSucceeded()
                    : (new Result()).SetFailed(err);
        }
    }
}
